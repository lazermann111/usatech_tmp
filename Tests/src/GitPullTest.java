import java.awt.Window;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;

import com.jcraft.jsch.JSchException;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.TransportProtocolException;
import com.sshtools.j2ssh.transport.publickey.SshPublicKey;
import com.usatech.tools.deploy.CvsPullTask;
import com.usatech.tools.deploy.DefaultSshClientCache;
import com.usatech.tools.deploy.DeployTaskInfo;
import com.usatech.tools.deploy.GitPullTask;
import com.usatech.tools.deploy.Host;
import com.usatech.tools.deploy.HostImpl;
import com.usatech.tools.deploy.MapPasswordCache;
import com.usatech.tools.deploy.SshClientCache;
import simple.event.ProgressListener;
import simple.io.GuiInteraction;
import simple.io.IOUtils;
import simple.io.Interaction;
import simple.io.versioning.GitClient;
import simple.test.UnitTest;

public class GitPullTest extends UnitTest {

	private MapPasswordCache cache = new MapPasswordCache();
	private Interaction interaction = new Interaction() {
		@Override
		public char[]readPassword(String fmt,Object...args){
	// TODO Auto-generated method stub
	return null;}

	@Override public String readLine(String fmt,Object...args){
	// TODO Auto-generated method stub
	return null;}

	@Override public void printf(String format,Object...args){
	// TODO Auto-generated method stub

	}

	@Override public PrintWriter getWriter(){
	// TODO Auto-generated method stub
	return null;}

	@Override public Future<char[]>getPasswordFuture(String fmt,Object...args){
	// TODO Auto-generated method stub
	return null;}

	@Override public Future<String>getLineFuture(String fmt,Object...args){
	// TODO Auto-generated method stub
	return null;}

	@Override public ProgressListener createProgressMeter(){
	// TODO Auto-generated method stub
	return null;}

	@Override public Future<File>browseForFile(String caption,String baseDir,String fmt,Object...args){

	return null;}

	@Override public int startSection(String sectionDescription){
	// TODO Auto-generated method stub
	return 0;}

	@Override public void endSection(int sectionId,boolean success,String msg){
	// TODO Auto-generated method stub

	}

	@Override public void clear(){
	// TODO Auto-generated method stub

	}};
	private Host gitHost = new HostImpl("LOCAL", "git.usatech.com");
	private Host cvsHost = new HostImpl("LOCAL", "cvs.usatech.com");
	private String user = "darkhipov";

	private DeployTaskInfo deployTaskInfo;
	private SshClientCache sshClientCache = null;

	private HostKeyVerification hostKeyVerification=new HostKeyVerification(){

	@Override public boolean verifyHost(String host,SshPublicKey pk)throws TransportProtocolException{

	return true; /* , what? */
	}};

	@Before
	public void setup() throws IOException {
		setupLog();
		
		Interaction interaction = new GuiInteraction((Window) null);
		char[] pwd = interaction.readPassword("Enter your password for VCS:");
		if(pwd == null)
		return;

		cache.setPassword(gitHost, "ssh", pwd);
		cache.setPassword(cvsHost, "ssh", pwd);

		sshClientCache = new DefaultSshClientCache(interaction, hostKeyVerification, cache);

	}

	@Test
	public void testGitPullTask() throws IOException, InterruptedException {

		deployTaskInfo = new DeployTaskInfo(interaction, gitHost, user, cache, sshClientCache);

		GitPullTask gitPullTask = new GitPullTask("/opt/USAT/git/NetworkServices.git", "DMS/version.txt", "HEAD");

		gitPullTask.perform(deployTaskInfo);

		
	}

	@Test
	public void testCVSPullTask() throws IOException, InterruptedException {

		deployTaskInfo = new DeployTaskInfo(interaction, cvsHost, user, cache, sshClientCache);

		CvsPullTask cvsPullTask = new CvsPullTask("/usr/local/cvsroot/NetworkServices", "DMS/version.txt", "HEAD");

		cvsPullTask.perform(deployTaskInfo);
		
	}

	@Test
	public void testExport() throws Exception, JSchException {
		// String path = "DatabaseScripts/USADBP01/PSS/PKG_TRAN.pbk";
		String path = "Simple1.5/dist/simple-all.jar";
		String revision = "HEAD"; // "1.167";
		GitClient git = new GitClient("git.usatech.com", "/opt/USAT/git/NetworkServices.git");
		char[] pwd = cache.getPassword(gitHost, "ssh");
		if(pwd == null)
			return;
		git.connect("darkhipov", String.valueOf(pwd));
		InputStream in = git.export(path, revision);
		// IOUtils.saveToFile("test.txt", git.export(path, revision));
		long size = IOUtils.readForLength(in);
		log.info("Read " + size + " bytes for " + path + " (" + revision + ")");
		git.close();
	}	

}
