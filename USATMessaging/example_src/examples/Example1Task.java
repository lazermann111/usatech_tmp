package examples;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.event.TaskListener;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;

public class Example1Task implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final Map<String,Object> emptyMap = Collections.emptyMap();
	protected String publishTo = "example-queue";
	protected int limit = 100;
	protected int[] resultCodes;
	protected TaskListener taskListener;
	protected ResourceFolder resourceFolder;

	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();

		if(taskListener != null) taskListener.taskStarted("Example 1", attributes.toString());
		int n;
		try {
			n = ConvertUtils.getInt(attributes.get("count"), 0);
		} catch(ConvertException e) {
			throw new ServiceException("Could not convert 'count'", e);
		}
		log.debug("Got number " + n + " from " + taskInfo.getServiceName());
		String key = ConvertUtils.getStringSafely(attributes.get("file-key"));
		if(key != null) {
			log.debug("Got file-key '" + key + "' from " + taskInfo.getServiceName() +"; grabbing resource contents...");
			Resource res;
			try {
				res = getResourceFolder().getResource(key, ResourceMode.READ);
				String content = IOUtils.readFully(res.getInputStream());
				log.debug("Got " + content.length() + " bytes of content '" + content + "'");
			} catch(IOException e) {
				throw new ServiceException(e);
			}
		}
		int rc;
		if(n >= limit) {
			log.debug("Reached limit; ending...");
			rc = 0;
		} else {
			if(getResultCodes() != null && getResultCodes().length > 0) {
				rc = resultCodes[(int)(resultCodes.length * Math.random())];
			} else
				rc = 0;
			switch(rc) {
				case 0:
					log.debug("Publishing number " + (n + 1) + " to " + getPublishTo());
					MessageChain mc = new MessageChainV11();
					MessageChainStep step = mc.addStep(getPublishTo());
					step.addLiteralAttribute("count", String.valueOf(n+1));
					MessageChainService.publish(mc, taskInfo.getPublisher());
					break;
				case 1:
					Resource res;
					try {
						res = getResourceFolder().getResource("example_file_" + n + ".txt", ResourceMode.CREATE);
						OutputStream out = res.getOutputStream();
						out.write(("This is the content for USAT Messaging Example 1 where count equals " + n + ".\n").getBytes());
						out.flush();
						out.close();
					} catch(IOException e) {
						throw new ServiceException(e);
					}
					log.debug("Publishing number " + (n + 1) + " to " + getPublishTo() + " with file-key '" + res.getKey() + "'");
					mc = new MessageChainV11();
					step = mc.addStep(getPublishTo());
					step.addLiteralAttribute("count", String.valueOf(n+1));
					step.addLiteralAttribute("file-key", res.getKey());
					MessageChainService.publish(mc, taskInfo.getPublisher());
					break;
			}
		}
		if(taskListener != null) taskListener.taskEnded("Example 1");
		return rc;

	}
	public String getPublishTo() {
		return publishTo;
	}
	public void setPublishTo(String publishTo) {
		this.publishTo = publishTo;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int[] getResultCodes() {
		return resultCodes;
	}
	public void setResultCodes(int[] resultCodes) {
		this.resultCodes = resultCodes;
	}
	public TaskListener getTaskListener() {
		return taskListener;
	}
	public void setTaskListener(TaskListener taskListener) {
		this.taskListener = taskListener;
	}
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}
	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
}
