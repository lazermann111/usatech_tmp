package examples;


import java.io.Closeable;
import java.io.IOException;
import java.util.Properties;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.util.CollectionUtils;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

public class Example1Start {
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		int times = args == null || args.length < 1 ? 1 : Integer.parseInt(args[0]);
		Properties properties = new Properties();
		properties.load(Example1Start.class.getResourceAsStream("/example1.properties"));
		Properties subProperties = CollectionUtils.getSubProperties(properties, "com.usatech.app.JMSPublisher.");
		Publisher<ByteInput> publisher = simple.app.Main.configure(Publisher.class, subProperties, properties, null, true);
    	MessageChain mc = buildMessageChain(properties.getProperty("simple.app.Service.A.queue.queueKey"));
    	for(int i = 0; i < times; i++)
    		MessageChainService.publish(mc, publisher);
    	if(publisher instanceof Closeable) {
    		((Closeable)publisher).close();
    	}
    	System.exit(0);
	}

	protected static MessageChain buildMessageChain(String queueName) throws ServiceException, IOException, InterruptedException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(queueName);
		step.addLiteralAttribute("count", "1");
		MessageChainStep retryStep = mc.addStep(queueName);
		retryStep.addLiteralAttribute("count", "-1");
		step.setNextSteps(255, retryStep);
		return mc;
	}
}
