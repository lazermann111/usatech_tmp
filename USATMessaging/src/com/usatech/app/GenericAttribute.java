package com.usatech.app;

public class GenericAttribute implements Attribute {
	protected String value;
	
	public GenericAttribute(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
