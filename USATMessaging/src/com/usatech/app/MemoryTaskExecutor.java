package com.usatech.app;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import simple.io.Log;
import simple.security.SecurityUtils;
import simple.util.concurrent.CustomThreadFactory;

public class MemoryTaskExecutor implements AsyncTaskExecutor {
	private static final Log log = Log.getLog();
	protected int threads = 3;
	protected int queueSize = 300;
	protected ThreadPoolExecutor executor;
	
	@Override
	public void init() {
		executor = new ThreadPoolExecutor(threads, threads, Long.MAX_VALUE, TimeUnit.NANOSECONDS, new ArrayBlockingQueue<Runnable>(queueSize, false),
				new CustomThreadFactory("MemoryTask-", true),
                new ThreadPoolExecutor.DiscardOldestPolicy());
		
	}
	
	@Override
	public void process() {
		executor.execute(new MemoryTask());
	}
	
	protected class MemoryTask implements Runnable {		
		public void run() {
			long startTimeMs = System.currentTimeMillis();
			SecureRandom rand;
			try {
				rand = SecurityUtils.getSecureRandom();
			} catch (Exception e) {
				rand = new SecureRandom();
			}
			int vi = 73 + rand.nextInt(5);
			int mc = 22 + rand.nextInt(4);
			int ax1 = 2 + rand.nextInt(3);
			int ax2 = 2 + rand.nextInt(3);
			int di1 = 1 + rand.nextInt(3);
			int di2 = 1 + rand.nextInt(3);
			int i;
			
			ArrayList<Integer> list = new ArrayList<Integer>(vi + mc + ax1 + ax2 + di1 + di2);
			for (i = 0; i < vi; i++)
				list.add(1);
			for (i = 0; i < mc; i++)
				list.add(2);
			for (i = 0; i < ax1; i++)
				list.add(3);
			for (i = 0; i < ax2; i++)
				list.add(4);
			for (i = 0; i < di1; i++)
				list.add(5);
			for (i = 0; i < di2; i++)
				list.add(6);
			Collections.shuffle(list);
			int count = 0;
			for (Integer item : list) {
				switch (item) {
					case 1:
						count += generateData(rand, "4", 16);
						break;
					case 2:
						count += generateData(rand, new StringBuilder("5").append(1 + rand.nextInt(5)).toString(), 16);
						break;
					case 3:
						count += generateData(rand, "34", 15);
						break;
					case 4:
						count += generateData(rand, "37", 15);
						break;
					case 5:
						count += generateData(rand, "6011", 16);
						break;
					case 6: 
						count += generateData(rand, "65", 16);
						break;
				}
			}
			
			long durationMs = System.currentTimeMillis() - startTimeMs;
			log.info("Processed {0} items in {1} ms", count, durationMs);
		}
		
		protected int generateData(SecureRandom rand, String prefix, int len) {
			String track2 = SecurityUtils.fillInTrackData(rand, SecurityUtils.generateCard(rand, prefix, len, null));
			String[] cardParts = track2.split("=", 2);
			String cardNumber, expirationDate;
			if (cardParts.length > 1) {
				if (track2.startsWith(";"))
					cardNumber = cardParts[0].substring(1);
				else
					cardNumber = cardParts[0];
				if (cardParts[1].length() > 4)
					expirationDate = cardParts[1].substring(0, 4);
				else
					expirationDate = cardParts[1];
			} else {
				cardNumber = track2;
				expirationDate = "";
			}
			if (log.isDebugEnabled())
				log.debug("Length track2: {0}, card: {1}, expiration: {2}", track2.length(), cardNumber.length(), expirationDate.length());
			return 1;
		}
	}
		
	public int getThreads() {
		return threads;
	}
	
	public void setThreads(int threads) {
		this.threads = threads;
	}
	
	public int getQueueSize() {
		return queueSize;
	}
	
	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}
}
