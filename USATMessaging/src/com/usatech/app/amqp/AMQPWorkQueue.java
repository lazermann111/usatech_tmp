package com.usatech.app.amqp;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

import simple.app.PrerequisiteManager;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.event.ProgressListener;
import simple.io.ByteArrayByteInput;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.Initializer;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.rabbitmq.client.ShutdownSignalException;
import com.rabbitmq.utility.ValueOrException;

public class AMQPWorkQueue implements WorkQueue<ByteInput> {
	private static final Log log = Log.getLog();
	protected String host;
	protected int port;
	protected String realm;

	protected String queueKey;
	protected String exchange = "";
	protected boolean multicast = false;

	protected Channel channel;
	protected int ticket;
	protected String consumerTag;
	protected final PrerequisiteManager prerequisiteManager = new PrerequisiteManager();

	//protected final ReentrantLock channelLock = new ReentrantLock();
	protected final BlockingQueue<ValueOrException<Delivery, ShutdownSignalException>> queue = new LinkedBlockingQueue<ValueOrException<Delivery, ShutdownSignalException>>();
	protected final Consumer consumer = new Consumer() {
		public void handleCancelOk(String consumerTag) {
		}
		public void handleConsumeOk(String consumerTag) {
		}
		public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
				throws IOException {
			try {
				queue.put(ValueOrException. <Delivery, ShutdownSignalException> makeValue(new Delivery(envelope, properties, body)));
			} catch(InterruptedException e) {
				IOException ioe = new IOException("Interrupted while adding to consumer queue");
				ioe.initCause(e);
				throw ioe;
			}
		}
		public void handleShutdownSignal(String consumerTag, ShutdownSignalException sig) {
			queue.add(ValueOrException. <Delivery, ShutdownSignalException> makeException(sig));
		}
	};

	protected final Initializer<IOException> initializer = new Initializer<IOException>() {
		@Override
		protected void doInitialize() throws IOException {
			Connection conn = new ConnectionFactory().newConnection(getHost(), getPort());
			channel = conn.createChannel();
			ticket = channel.accessRequest(getRealm());
			channel.exchangeDeclare(ticket, getExchange(), "direct", true);
			String queueName;
			if(isMulticast()) {
				queueName = channel.queueDeclare(ticket).getQueue();
			} else {
				queueName = channel.queueDeclare(ticket, queueKey, true).getQueue();
			}
			channel.queueBind(ticket, queueName, getExchange(), queueKey);
			consumerTag = channel.basicConsume(ticket, queueKey, false, nextConsumerTag(), consumer);
			publisher = new AMQPPublisher();
			publisher.setHost(getHost());
			publisher.setPort(getPort());
			publisher.setRealm(getRealm());
			publisher.setExchange(getExchange());
		}
		@Override
		protected void doReset() {
			try {
				channel.close(200, "Complete");
			} catch(IOException e) {
				//ignore
			}
			try {
				channel.getConnection().close(200, "Complete");
			} catch(IOException e) {
				//ignore
			}
		}
	};

	protected static final Random random = new Random();
	protected AMQPPublisher publisher;
	protected Publisher<ByteInput> byteInputPublisher = new Publisher<ByteInput>() {
		public void publish(String queueKey, ByteInput content)
				throws ServiceException {
			publish(queueKey, false, false, content);
		}
		public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content)
				throws ServiceException {
			publish(queueKey, multicast, temporary, content, null, null);
		}
		@Override
		public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content, Long correlation, simple.app.QoS qos, String... blocks) throws ServiceException {
			if(qos != null && qos.getAvailable() > 0 && qos.getAvailable() > System.currentTimeMillis()) {
				throw new ServiceException("Delayed execute is not supported");
			}
			byte[] buffer;
			try {
				buffer = new byte[content.length()];
				content.readBytes(buffer, 0, buffer.length);
			} catch(IOException e) {
				throw new ServiceException(e);
			}
			publisher.publish(queueKey, buffer); //TODO: Support QoS settings
		}
		/**
		 * @see simple.app.Publisher#close()
		 */
		@Override
		public void close() {
			publisher.close();
		}

		@Override
		public int getConsumerCount(String queueName, boolean temporary) {
			return 1;
		}

		public String[] getSubscriptions(String queueKey) throws ServiceException {
			return null;
		}

		@Override
		public void block(String... blocks) throws ServiceException {
			throw new ServiceException("Blocking is not supported in this publisher implementation");
		}
	};

	protected class AMQPWork implements Work<ByteInput> {
		protected final Delivery delivery;
		protected boolean complete;
		public AMQPWork(Delivery delivery) {
			this.delivery = delivery;
		}
		@Override
		public QoS getQoS() {
			//TODO: Implement this - delivery.getProperties().expiration
			return null;
		}
		@Override
		public Map<String, ?> getWorkProperties() {
			BasicProperties p = delivery.getProperties();
			if(p == null)
				return null;
			Map<String,Object> map = new HashMap<String, Object>();
			for(Field field : p.getClass().getFields()) {
	    		if(!Modifier.isStatic(field.getModifiers())) {
	    			try {
						map.put(field.getName(), field.get(p));
					} catch(IllegalArgumentException e) {
						// Ignore
					} catch(IllegalAccessException e) {
						// Ignore
					}
	    		}
	    	}
			return map;
		}
		public long getEnqueueTime() {
			Date d = delivery.getProperties().timestamp;
			return d == null ? 0 : d.getTime();
		}
		public boolean isRedelivered() {
			return delivery.getEnvelope().isRedeliver();
		}
		public boolean isUnguardedRetryAllowed() {
			return false;
		}
		public int getRetryCount() {
			return -1;
		}
		public boolean isComplete() {
			return complete;
		}
		public ByteInput getBody() {
			return new ByteArrayByteInput(delivery.getBody());
		}

		public void workComplete() throws WorkQueueException {
			if(complete) throw new WorkQueueException("Work is already complete; workComplete() may not be called");
			handleWorkComplete(delivery);
			complete = true;
		}
		public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
			if(complete) throw new WorkQueueException("Work is already complete; workAborted() may not be called");
			//XXX: For now we will not support retries
			prerequisiteManager.resetAvailability();
			handleWorkAborted(delivery, cause);
			complete = true;
		}
		public Publisher<ByteInput> getPublisher() throws WorkQueueException {
			return byteInputPublisher;
		}
		public String getOriginalQueueName() {
			return delivery.getProperties().replyTo;
		}
		public String getQueueName() {
			return getQueueKey();
		}

		public String getGuid() {
			return delivery.getEnvelope().getRoutingKey();
		}
		public void unblock(String... blocksToClear) {
			// TODO Auto-generated method stub			
		}
		public String getSubscription() {
			return null;
		}
	}
	public AMQPWorkQueue() {
	}

	protected static String nextConsumerTag() {
		return "c"+String.valueOf(random.nextLong());
	}

	protected void handleWorkComplete(Delivery delivery) throws WorkQueueException {
		/* Is locking necessary for ack? Let's try it without
		 * 04/28/2008 (BSK) : Analysis of source code indicates that single frame methods (like basicAck) are thread-safe,
		 *                    but multi-frame methods like deliver and publish are not. But since basicAck is outgoing and
		 *                    deliver is incoming we can use the same channel (I think).
		 * 05/02/2008 (BSK) : Let's use a separate channel so that this does not interfere with pause / unpause or cancel / consume
		 * 05/08/2008 (BSK) : AMQP 0-8 does not allow ack'ing on a different channel than we receive delivery
		 * 05/08/2008 (BSK) : It also looks like RabbitMQ 1.2.0 does not support cancelling on a different channel
		 *
		 */
		//channelLock.lock();
		try {
			channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
		} catch(IOException ioe){
			throw new WorkQueueException("While acknowledging message with delivery  tag " + String.valueOf(delivery.getEnvelope().getDeliveryTag()), ioe);
		} finally {
			//channelLock.unlock();
		}
	}

	protected void handleWorkAborted(Delivery delivery, Throwable cause) throws WorkQueueException {
		log.error("Could not process message: " + delivery.getBody(), cause);
	}
	public Class<ByteInput> getWorkBodyType() {
		return ByteInput.class;
	}
	public boolean isAutonomous() {
		return false;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getRealm() {
		return realm;
	}
	public void setRealm(String realm) {
		this.realm = realm;
	}
	public String getQueueKey() {
		return queueKey;
	}
	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public boolean isMulticast() {
		return multicast;
	}
	public void setMulticast(boolean multicast) {
		this.multicast = multicast;
	}
	public PrerequisiteManager getPrerequisiteManager() {
		return prerequisiteManager;
	}

	public WorkQueue.Retriever<ByteInput> getRetriever(ProgressListener listener) {
		//TODO: we could use a common retriever since the next() method is thread-safe
		return new WorkQueue.Retriever<ByteInput>() {
			/**
			 * @see simple.app.WorkQueue.Retriever#arePrequisitesAvailable()
			 */
			@Override
			public boolean arePrequisitesAvailable() {
				return prerequisiteManager.arePrequisitesAvailable();
			}
			/**
			 * @see simple.app.WorkQueue.Retriever#cancelPrequisiteCheck(java.lang.Thread)
			 */
			@Override
			public void cancelPrequisiteCheck(Thread thread) {
				prerequisiteManager.cancelPrequisiteCheck(thread);
			}

			public void resetAvailability() {
				prerequisiteManager.resetAvailability();
			}

			public Work<ByteInput> next() throws WorkQueueException, InterruptedException {
				try {
					initializer.initialize();
				} catch(IOException e) {
					throw new WorkQueueException("Could not initialize queue consumer", e);
				}/*
				if(log.isDebugEnabled()) {
					StringBuilder sb = new StringBuilder("Queue = [");
					for(ValueOrException<Delivery, ShutdownSignalException> voe : queue) {
						if(voe == null)
							sb.append("NULL");
						try {
							sb.append(voe.getValue().getEnvelope().getDeliveryTag());
							sb.append('=');
							MessageChain mc = ComponentService.deserializeMessageChain_v10(voe.getValue().getBody(), queueKey);
							sb.append(mc.getAttributes());
						} catch(ShutdownSignalException e) {
							sb.append("SHUTDOWN");
						} catch(ServiceException e) {
							sb.append("INVALID");
						}
						sb.append(',');
					}
					if(sb.charAt(sb.length()-1) == ',')
						sb.setLength(sb.length()-1);
					sb.append(']');
					log.debug(sb.toString());
				}*/
				try {
					return new AMQPWork(queue.take().getValue());
				} catch(ShutdownSignalException e) {
					return null;
				}
			}
			public boolean cancel(Thread thread) {
				if(thread == null) return false;
				thread.interrupt();
				return true;
			}
			public void close() {
				//Do nothing b/c there are no retriever-specific resources
			}
		};
	}

	public void initialize() throws InterruptedException, IOException {
		initializer.initialize();
	}
	/**
	 * @see simple.app.WorkQueue#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() {
		initializer.reset();
		return TRUE_FUTURE;
	}
}
