package com.usatech.app.amqp;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.ByteInput;
import simple.io.Log;
import simple.text.StringUtils;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ReturnListener;

public abstract class AbstractAMQPPublisher<Q> implements Publisher<Q>, Closeable {
	private static final Log log = Log.getLog();
	protected String host;
	protected int port = -1;
	protected String exchange = "";
	protected String realm;
	protected boolean mandatory = true;

	protected Channel channel;
	protected final ReentrantLock channelLock = new ReentrantLock();
	protected int ticket;
	protected ServiceException exception;

	public void initialize() throws ServiceException {
		channelLock.lock();
		try {
			initializeProtected();
		} catch(IOException e) {
			throw new ServiceException("Could not initialize", e);
		} finally {
			channelLock.unlock();
		}
	}
	//TODO: We should consider using a singleton connection, per host/port (such as BeanFactoryByProperties provides)
	protected void initializeProtected() throws IOException {
		if(channel == null) {
			if(getHost() == null) throw new IOException("Host is not set");
			if(getRealm() == null) throw new IOException("Realm is not set");
			Connection conn = new ConnectionFactory().newConnection(getHost(), getPort());
			Channel channel0 = conn.createChannel();
			channel0.setReturnListener(new ReturnListener() {
            	public void handleBasicReturn(int replyCode,
                        String replyText,
                        String exchange,
                        String routingKey,
                        AMQP.BasicProperties properties,
                        byte[] body) throws IOException {
            		ServiceException e = new ServiceException("Message was not able to be sent to exchange '" + exchange + "' with routing key '"
    				+ routingKey + "' because " + replyText + " (" + replyCode + ")");
            		log.error("Could not publish: " + StringUtils.toHex(body), e);
            		channelLock.lock();
            		try {
            			exception = e;
            		} finally {
            			channelLock.unlock();
            		}
            		//shutdown exchange/routingKey
            		//close(404, msg);
            	}
            });
			ticket = channel0.accessRequest(getRealm());
            channel0.exchangeDeclare(ticket, getExchange(), "direct", true);
            channel = channel0;
		}
	}
	/** This is one implementation of publish (using locking to ensure thread-safety. Another possibility is
	 *  using a channel per thread (Using ThreadLocal to store the channel).
	 * @see simple.app.Publisher#publish(java.lang.String, byte[])
	 */
	public void publish(String routingKey, byte[] content) throws ServiceException {
	}
	/** This is one implementation of publish (using locking to ensure thread-safety. Another possibility is
	 *  using a channel per thread (Using ThreadLocal to store the channel).
	 * @see simple.app.Publisher#publish(java.lang.String, byte[])
	 */
	public void publish(String routingKey, boolean multicast, boolean temporary, byte[] content) throws ServiceException {
		publish(routingKey, multicast, temporary, content, null, null);
	}

	/** This is one implementation of publish (using locking to ensure thread-safety. Another possibility is
	 *  using a channel per thread (Using ThreadLocal to store the channel).
	 * @see simple.app.Publisher#publish(java.lang.String, byte[])
	 */
	public void publish(String routingKey, boolean multicast, boolean temporary, byte[] content, Long correlation, QoS qos, String... blocks) throws ServiceException {
		//XXX: for AMQP the consumer determines singlecase or multicast not the publisher
		if(multicast)
			log.info("AMQP does not support multicast at the publisher");
		if(qos != null && qos.getAvailable() > 0 && qos.getAvailable() > System.currentTimeMillis()) {
			throw new ServiceException("Delayed execute is not supported");
		}
		channelLock.lock();
		try {
			if(exception != null) {
				ServiceException se = exception;
				exception = null;
				throw se;
			}
			initialize();
			//TODO: Support QoS settings
			channel.basicPublish(ticket, getExchange(), routingKey, isMandatory(), false, null, content);
		} catch(IOException e) {
			throw new ServiceException("Could not publish", e);
		} finally {
			channelLock.unlock();
		}
	}

	public void publish(String queueKey, ByteInput content) throws ServiceException {
		publish(queueKey, false, false, content);
	}
	public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content) throws ServiceException {
		publish(queueKey, multicast, temporary, content, null, null);
	}

	public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content, Long correlation, QoS qos, String... blocks) throws ServiceException {
		byte[] buffer;
		try {
			buffer = new byte[content.length()];
			content.readBytes(buffer, 0, buffer.length);
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		publish(queueKey, multicast, temporary, buffer, correlation, qos);
	}

	/**
	 * @see simple.app.Publisher#close(int, java.lang.String)
	 */
	public void close(int closeCode, String closeMessage) throws IOException {
		channelLock.lock();
		try {
			//channel.close(closeCode, closeMessage);
			channel.getConnection().close(closeCode, closeMessage);
			channel = null;
		} finally {
			channelLock.unlock();
		}
	}
	@Override
	protected void finalize() throws Throwable {
		try {
			close(200, "Exiting JVM");
		} catch(Throwable t){}
		super.finalize();
	}
	public String getHost() {
		return host;
	}

	public void setHost(String host) throws IOException {
		String oldHost = this.host;
		this.host = host;
		if(!ConvertUtils.areEqual(oldHost, host) && channel != null) {
			close(200, "Switching hosts");
		}

	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) throws IOException {
		int oldPort = this.port;
		this.port = port;
		if(oldPort != port && channel != null) {
			close(200, "Switching ports");
		}
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) throws IOException {
		String oldExchange = this.exchange;
		//exchange cannot be null
		if(exchange == null) exchange = "";
		this.exchange = exchange;
		if(!ConvertUtils.areEqual(oldExchange, exchange) && channel != null) {
			channelLock.lock();
			try {
				channel.exchangeDeclare(ticket, exchange, "direct", true);
			} finally {
				channelLock.unlock();
			}
		}
	}

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) throws IOException {
		String oldRealm = this.realm;
		this.realm = realm;
		if(!ConvertUtils.areEqual(oldRealm, realm) && channel != null) {
			channelLock.lock();
			try {
				ticket = channel.accessRequest(getRealm());
				// do we also need the following?
				//channel.exchangeDeclare(ticket, exchange, "direct", true);
			} finally {
				channelLock.unlock();
			}
		}
	}
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	public void close() {
		try {
			close(200, "All done");
		} catch(IOException e) {
			log.info("Could not close amqp connection", e);
		}
	}

	@Override
	public int getConsumerCount(String queueName, boolean temporary) {
		return 1;
	}

	public String[] getSubscriptions(String queueKey) throws ServiceException {
		return null;
	}

	@Override
	public void block(String... blocks) throws ServiceException {
		throw new ServiceException("Blocking is not supported in this publisher implementation");
	}
}
