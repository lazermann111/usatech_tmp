package com.usatech.app.tools;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.swt.AppAction;
import simple.swt.BeanTableModel;
import simple.swt.SortedTrackableListModel;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

public class InteractiveQueueLayerProducer extends BaseWithConfig {
	private static final Log log = Log.getLog();

	public static class MessageAttribute {
		protected String name;
		protected Object value;

		public MessageAttribute() {
		}

		public MessageAttribute(String name, Object value) {
			setName(name);
			setValue(value);
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new InteractiveQueueLayerProducer().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
		// registerCommandLineSwitch('d', "searchPath", true, true,
		// "searchPath", "The path in which to look for ChangeSets");
	}

	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		final Publisher<ByteInput> publisher;
		try {
			BaseWithConfig.configureDataSourceFactory(config, null); // this must come first
			publisher = BaseWithConfig.configure(Publisher.class, config.subset("simple.app.Publisher"), null, true);
		} catch(Exception e) {
			finishAndExit("Could not configure", 200, true, e);
			return;
		}
		final CountDownLatch exit = new CountDownLatch(1);
		final JFrame frame = new JFrame("USAT QueueLayer Producer");
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				exit.countDown();
			}
		});
		final JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		frame.getContentPane().add(mainPanel);
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 1;
		constraints.gridy = 1;
		mainPanel.add(new JLabel("Queue Key: "), constraints);
		final JTextField queueKeyField = new JTextField();
		constraints.gridx = 2;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1.0;
		mainPanel.add(queueKeyField, constraints);
		final JPanel attributesPanel = new JPanel(new BorderLayout());
		attributesPanel.setBorder(new TitledBorder("Attributes:"));
		constraints.gridx = 1;
		constraints.gridy = 2;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridwidth = 2;
		constraints.weighty = 1.0;
		mainPanel.add(attributesPanel, constraints);
		final SortedTrackableListModel<MessageAttribute> attributes = new SortedTrackableListModel<MessageAttribute>(false, new Comparator<MessageAttribute>() {
			@Override
			public int compare(MessageAttribute o1, MessageAttribute o2) {
				if(o1.getName() == null) {
					if(o2.getName() == null)
						return 0;
					return 1;
				} else if(o2.getName() == null)
					return -1;
				int i = o1.getName().compareToIgnoreCase(o2.getName());
				if(i != 0)
					return i;
				return o1.getName().compareTo(o2.getName());
			}
		});
		final JTable attributesTable = new JTable(new BeanTableModel(MessageAttribute.class, attributes, true, "Name", "Value"));
		attributesPanel.add(new JScrollPane(attributesTable), BorderLayout.CENTER);
		JPanel attributesButtonPanel = new JPanel(new FlowLayout());
		attributesPanel.add(attributesButtonPanel, BorderLayout.EAST);
		attributesButtonPanel.add(new JButton(new AppAction("Add Attribute", "Add an attribute to the message", null, (String) null) {
			private static final long serialVersionUID = 29773421414346991L;

			public void actionPerformed(ActionEvent event) {
				attributes.add(new MessageAttribute());
				attributesTable.editCellAt(attributes.getSize() - 1, 0);
			}

			protected boolean canPerform() {
				return true;
			}
		}));
		final AppAction deleteAttributeAction = new AppAction("Delete Attribute", "Deletes currently selected attribute from the message", null, (String) null) {
			private static final long serialVersionUID = 114298514346L;

			public void actionPerformed(ActionEvent event) {
				int row = attributesTable.getSelectedRow();
				if(row >= 0)
					attributes.remove(row);
			}

			protected boolean canPerform() {
				return attributesTable.getSelectedRow() >= 0;
			}
		};
		attributesTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				deleteAttributeAction.checkEnabled();
			}
		});
		attributesButtonPanel.add(new JButton(deleteAttributeAction));
		final DefaultStyledDocument consoleContent = new DefaultStyledDocument();
		constraints.gridx = 1;
		constraints.gridy = 4;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridwidth = 2;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		final JScrollPane consoleScrollPane = new JScrollPane(new JTextPane(consoleContent), JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		mainPanel.add(consoleScrollPane, constraints);

		JPanel buttonPanel = new JPanel(new FlowLayout());
		constraints.gridx = 1;
		constraints.gridy = 3;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridwidth = 2;
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		mainPanel.add(buttonPanel, constraints);
		final SimpleAttributeSet consoleInfoAttributeSet = new SimpleAttributeSet();
		buttonPanel.add(new JButton(new AppAction("Add Message", "Add the message into the QueueLayer", null, (String) null) {
			private static final long serialVersionUID = 2977851544186446991L;

			public void actionPerformed(ActionEvent event) {
				StringBuilder sb = new StringBuilder();
				String queueKey = queueKeyField.getText();
				if(queueKey == null || (queueKey = queueKey.trim()).length() == 0) {
					JOptionPane.showMessageDialog(frame, "Queue Key is blank. Please enter the Queue Key");
					return;
				}
				MessageChain mc = new MessageChainV11();
				MessageChainStep step = mc.addStep(queueKey);
				sb.append("Are you sure you wish to publish a new message to queue '").append(queueKey).append("' with attributes [");
				boolean first = true;
				for(MessageAttribute ma : attributes) {
					step.addLiteralAttribute(ma.getName(), ma.getValue());
					if(first)
						first = false;
					else
						sb.append("; ");
					sb.append(ma.getName()).append("=").append(ma.getValue());
				}
				sb.append("]?");
				if(JOptionPane.showConfirmDialog(frame, sb.toString()) == JOptionPane.OK_OPTION) {
					try {
						MessageChainService.publish(mc, publisher);
					} catch(ServiceException e) {
						log.warn("Could not publish message", e);
						JOptionPane.showMessageDialog(frame, "Could not publish this message because " + e.getMessage());
						return;
					}
					sb.replace(0, 32, "Published");
					sb.setCharAt(sb.length() - 1, '\n');
					try {
						consoleContent.insertString(consoleContent.getLength(), sb.toString(), consoleInfoAttributeSet);
					} catch(BadLocationException e) {
						log.warn("Could not write to console", e);
					}
				}
			}
			protected boolean canPerform() {
				return true;
			}
		}));

		frame.setVisible(true);
		try {
			exit.await();
		} catch(InterruptedException e) {
			// ignore
		}
	}
}
