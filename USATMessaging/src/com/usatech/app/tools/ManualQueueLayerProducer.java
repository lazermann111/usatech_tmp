package com.usatech.app.tools;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ByteInput;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.sheet.SheetUtils;
import simple.util.sheet.SheetUtils.RowValuesIterator;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

public class ManualQueueLayerProducer extends BaseWithConfig {
	private static final Log log = Log.getLog();

	public static void main(String[] args) {
		new ManualQueueLayerProducer().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
		registerCommandLineSwitch('q', "queueKey", false, true, "queueKey", "The Queue Key into which to add the messages");
		registerCommandLineSwitch('f', "attributesFile", true, true, "attributesFile",
				"The CSV or Excel file that contains the attributes for the messages (one row for each message and one column with a header for each attribute)");
		registerCommandLineSwitch('c', "classesSpecified", true, false, "classesSpecified",
				"The first row after the header in the CSV or Excel file contains the class names of the data");
		registerCommandLineSwitch('s', "attributesString", true, true, "attributesString", "The string that contains the CSV formatted attributes for the messages (one row for each message and one column with a header for each attribute)");
	}

	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		final Publisher<ByteInput> publisher;
		try {
			BaseWithConfig.configureDataSourceFactory(config, null); // this must come first
			publisher = BaseWithConfig.configure(Publisher.class, config.subset("simple.app.Publisher"), null, true);
		} catch(Exception e) {
			finishAndExit("Could not configure", 200, true, e);
			return;
		}
		String queueKey;
		try {
			queueKey = ConvertUtils.getString(argMap.get("queueKey"), true);
		} catch(ConvertException e) {
			finishAndExit("Queue Key must be provided", 201, true, e);
			return;
		}
		if((queueKey = queueKey.trim()).length() == 0) {
			finishAndExit("Queue Key must be provided", 201, true, null);
			return;
		}
		InputStream attributesStream;
		String attributesFile;
		try {
			attributesFile = ConvertUtils.getString(argMap.get("attributesFile"), false);
		} catch(ConvertException e) {
			finishAndExit("Attributes File must be provided", 202, true, e);
			return;
		}
		if(attributesFile == null || (attributesFile = attributesFile.trim()).length() == 0) {
			String attributesString;
			try {
				attributesString = ConvertUtils.getString(argMap.get("attributesString"), true);
			} catch(ConvertException e) {
				finishAndExit("Attributes File or Attribute String must be provided", 202, true, null);
				return;
			}
			if(attributesString.trim().length() == 0) {
				finishAndExit("Attributes String must be provided", 202, true, null);
				return;
			}
			attributesStream = new ByteArrayInputStream(attributesString.getBytes());
		} else
			try {
				attributesStream = new FileInputStream(attributesFile);
			} catch(FileNotFoundException e) {
				finishAndExit("Attributes File '" + attributesFile + " does not exist or is not readable", 203, true, e);
				return;
			}
		boolean classesSpecified = ConvertUtils.getBooleanSafely(argMap.get("classesSpecified"), false);
		RowValuesIterator iter;
		try {
			iter = SheetUtils.getExcelRowValuesIterator(attributesStream, true);
		} catch(IOException e) {
			finishAndExit("Attributes File '" + attributesFile + " is not formated as a CSV or Excel file", 204, true, e);
			return;
		}
		Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
		StringBuilder sb = new StringBuilder();
		while(iter.hasNext()) {
			sb.setLength(0);
			Map<String, Object> attributes = iter.next();
			if(classesSpecified && classes.isEmpty()) {
				for(Map.Entry<String, Object> ma : attributes.entrySet()) {
					if(StringUtils.isBlank(ma.getKey()) || ma.getValue() == null || StringUtils.isBlank(ConvertUtils.getStringSafely(ma.getValue())))
						continue;
					try {
						classes.put(ma.getKey(), ConvertUtils.convert(Class.class, ma.getValue()));
					} catch(ConvertException e) {
						log.warn("Could not convert class name to a java class", e);
						return;
					}
				}
			} else {
				MessageChain mc = new MessageChainV11();
				MessageChainStep step = mc.addStep(queueKey);
				sb.append("Published message ").append(mc.getGuid()).append(" to queue '").append(queueKey).append("' with attributes [");
				boolean first = true;
				for(Map.Entry<String, Object> ma : attributes.entrySet()) {
					if(StringUtils.isBlank(ma.getKey()) || ma.getValue() == null || StringUtils.isBlank(ConvertUtils.getStringSafely(ma.getValue())))
						continue;
					Object value = ma.getValue();
					if(classesSpecified) {
						Class<?> target = classes.get(ma.getKey());
						if(target != null)
							try {
								value = ConvertUtils.convert(target, value);
							} catch(ConvertException e) {
								log.warn("Could not convert value " + target.getName() + " for attribute '" + ma.getKey() + "' row " + iter.getCurrentRowNum(), e);
								return;
							}
					}
					step.addLiteralAttribute(ma.getKey(), value);
					if(first)
						first = false;
					else
						sb.append("; ");
					sb.append(ma.getKey()).append("=").append(value);
				}
				sb.append("]");
				try {
					MessageChainService.publish(mc, publisher);
				} catch(ServiceException e) {
					log.warn("Could not publish message", e);
					return;
				}
				log.info(sb.toString());
			}
		}
	}
}
