/**
 *
 */
package com.usatech.app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.DataFormatException;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Convertible;
import simple.bean.Enumerator;
import simple.bean.Getter;
import simple.bean.MaskedString;
import simple.io.ByteArrayByteInput;
import simple.io.ByteArrayByteOutput;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.io.IOUtils;
import simple.io.Log;
import simple.lang.Decision;
import simple.security.crypt.CryptUtils;
import simple.security.crypt.EncryptionInfo;
import simple.security.crypt.KeySupply;
import simple.text.StringUtils;
import simple.util.Bitmap;
import simple.util.CollectionUtils;
import simple.util.ConversionMap;
import simple.util.FilterMap;
import simple.util.PrefetchIterator;

/** An encapulsation of a chain of processing. Based on the results of processing the first message in the chain,
 *  subsequent messages are chosen using the resultCode returned from {@link MessageChainTask#process(MessageChainTaskInfo)}
 * @author Brian S. Krug
 *
 */
public class MessageChainV11 implements MessageChain {
	protected static final byte MESSAGE_CHAIN_VERSION_1 = 0x11;
	protected static final byte MESSAGE_CHAIN_VERSION_2 = 0x12;
	protected static final byte MESSAGE_CHAIN_VERSION_2_ENCRYPTED = 0x52;
	protected static final byte MESSAGE_CHAIN_VERSION_2_ENCRYPTED_LONG = 0x62;
	//protected static final String ENCODING = "UTF-8";
	protected static final Charset ENCODING = Charset.forName("UTF-8");
	protected final static String EXECUTOR = ManagementFactory.getRuntimeMXBean().getName();
	protected final static String GUID_PREFIX = EXECUTOR + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':';
	protected final static AtomicLong guidGenerator = new AtomicLong(1);
	private static final Log log = Log.getLog();

	protected class MessageChainStepV11 implements MessageChainStep {
		//protected boolean multicast = false;
		//protected boolean temporary = false;
		protected final Bitmap bitmap = new Bitmap(); // bit 7=multicast;6=temporary;5=hasCorrelation; 4=hasWaitFors; 3=joined
		protected String queueKey;
		protected long startTime;
		protected long endTime;
		protected String executor;
		protected int resultCode = -1;
		protected Long correlation;
		protected final Map<String,AttributeValue> avpMap = new HashMap<String,AttributeValue>();
		protected final Map<String,Object> resultAttributes = new HashMap<String,Object>() {
			private static final long serialVersionUID = 4992812854633748469L;

			@Override
			public Object put(String key, Object value) {
				if(key != null && key.length() > MAX_NAME_LENGTH)
					throw new IllegalArgumentException("Result Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
				/*
				String vs = ConvertUtils.getStringSafely(value);
				if(vs != null && vs.length() > MAX_VALUE_LENGTH)
					throw new IllegalArgumentException("Result Attribute Value is too long. It may not exceed " + MAX_VALUE_LENGTH + " bytes");
				*/
				return super.put(key, value);
			}
		};
		protected final Map<Integer, MessageChainStepV11[]> resultMap = new HashMap<Integer, MessageChainStepV11[]>();
		protected int resultStepCount = 0;
		protected final Set<String> referencedResultAttributes = new HashSet<String>();
		protected transient Set<Integer> resultCodes;
		protected transient Map<String, Object> attributes;
		protected final int stepIndex;
		protected final Set<MessageChainStep> waitFors = new LinkedHashSet<MessageChainStep>();
		protected Set<MessageChainStep> waitForsUnmod;
		
		protected class AttributeMap extends ConversionMap<String, String, AttributeValue, Object> {
			public AttributeMap(Map<String,AttributeValue> delegate) {
				super(delegate);
			}
			@Override
			protected boolean isValueReversible() {
				return true;
			}
			@Override
			protected String convertKeyFrom(String key0) {
				return key0;
			}
			@Override
			protected String convertKeyTo(String key1) {
				return key1;
			}
			@Override
			protected Object convertValueFrom(AttributeValue value0) {
				if(value0 == null)
					return null;
				return value0.getValue();
			}
			@Override
			protected AttributeValue convertValueTo(Object value1) {
				return new LiteralAttributeValue(value1);
			}
		}

		protected MessageChainStepV11() {
			if(steps.size() >= MAX_REFERENCE_INDEX)
				throw new IllegalArgumentException("Too many steps are defined for this Message Chain. Only " + MAX_REFERENCE_INDEX + " are allowed");
			steps.add(this);
			stepIndex = steps.size();
		}

		public MessageChainStepV11(String queueKey) {
			this();
			this.queueKey = queueKey;
		}

		@Override
		public MessageChain getMessageChain() {
			return MessageChainV11.this;
		}
		/**
		 * @see com.usatech.app.MessageChainStep#addStep(java.lang.String)
		 */
		public MessageChainStepV11 addStep(String queueKey) {
			return MessageChainV11.this.addStep(queueKey);
		}
		/**
		 * @see com.usatech.app.MessageChainStep#addStep(java.lang.String, boolean)
		 */
		public MessageChainStepV11 addStep(String queueKey, boolean multicast) {
			return MessageChainV11.this.addStep(queueKey, multicast);
		}
		/**
		 * @see com.usatech.app.MessageChainStep#addStep(java.lang.String, boolean, boolean)
		 */
		public MessageChainStepV11 addStep(String queueKey, boolean multicast, boolean copyAttributes) {
			MessageChainStepV11 step = addStep(queueKey, multicast);
			if (copyAttributes)
				step.avpMap.putAll(avpMap);
			return step;
		}
		/**
		 * @see com.usatech.app.MessageChainStep#copyStep()
		 */
		public MessageChainStep copyStep() {
			MessageChainStepV11 step = addStep(getQueueKey(), isMulticast());
			step.setTemporary(isTemporary());
			step.avpMap.putAll(avpMap);
			step.resultMap.putAll(resultMap);
			step.resultStepCount = resultStepCount;
			step.correlation = step.correlation;
			return step;
		}

		public void copyAttributes(MessageChainStep step) {
			copyAttributes(step, null);
		}

		public void copyAttributes(MessageChainStep step, Decision<String> filter) {
			if(step instanceof MessageChainStepV11) {
				MessageChainStepV11 stepV11 = (MessageChainStepV11) step;
				if(stepV11.getMessageChain() == getMessageChain()) {
					for(Map.Entry<String, AttributeValue> entry : stepV11.avpMap.entrySet())
						if(filter == null || filter.decide(entry.getKey()))
							avpMap.put(entry.getKey(), entry.getValue());
				} else
					for(Map.Entry<String, AttributeValue> entry : stepV11.avpMap.entrySet())
						if(filter == null || filter.decide(entry.getKey())) {
							if(entry.getValue() instanceof ReferencingValue)
								avpMap.put(entry.getKey(), new LiteralAttributeValue(entry.getValue().getValue()));
							else
								avpMap.put(entry.getKey(), entry.getValue());
						}
			} else {
				for(Map.Entry<String, Object> entry : step.getAttributes().entrySet())
					if(filter == null || filter.decide(entry.getKey()))
						setAttribute(entry.getKey(), entry.getValue());
			}
		}
		public <T> T getAttribute(Attribute attribute, Class<T> convertTo, boolean required) throws AttributeConversionException {
			return getAttribute(attribute.getValue(), convertTo, required);
		}
		public <T> T getAttribute(String attributeName, Class<T> convertTo, boolean required) throws AttributeConversionException {
			AttributeValue attributeValue = avpMap.get(attributeName);
			if(attributeValue == null) {
				if(required)
					throw new AttributeConversionException(attributeName, new ConvertException("Value not provided", convertTo, null));
				return null;
			}
			Object value = attributeValue.getValue();
			if(required && value == null)
				throw new AttributeConversionException(attributeName, new ConvertException("Value is null", convertTo, null));
			T ret;
			try {
				ret = ConvertUtils.convert(convertTo, value);
			} catch(ConvertException e) {
				throw new AttributeConversionException(attributeName, e);
			}
			if(required && ret == null)
				throw new AttributeConversionException(attributeName, new ConvertException("Value is null after conversion", convertTo, null));
			return ret;
		}
		public <T> T getAttributeSafely(Attribute attribute, Class<T> convertTo, T defaultValue) {
			try {
				return getAttributeDefault(attribute.getValue(), convertTo, defaultValue);
			} catch(AttributeConversionException e) {
				return defaultValue;
			}
		}
		public <T> T getAttributeDefault(Attribute attribute, Class<T> convertTo, T defaultValue) throws AttributeConversionException {
			return getAttributeDefault(attribute.getValue(), convertTo, defaultValue);
		}
		protected <T> T getAttributeDefault(String attributeName, Class<T> convertTo, T defaultValue) throws AttributeConversionException {
			AttributeValue attributeValue = avpMap.get(attributeName);
			if(attributeValue == null)
				return defaultValue;
			Object value = attributeValue.getValue();
			if(value == null)
				return defaultValue;
			T ret;
			try {
				ret = ConvertUtils.convert(convertTo, value);
			} catch(ConvertException e) {
				throw new AttributeConversionException(attributeName, e);
			}
			if(ret == null)
				return defaultValue;
			return ret;
		}
		public void setAttribute(Attribute attribute, Object value) {
			setAttribute(attribute.getValue(), value);
		}
		public void setAttribute(String attributeName, Object value) {
			if(attributeName != null && attributeName.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(value == null)
				avpMap.remove(attributeName);
			else if(value instanceof Map)
				for(Map.Entry<?, ?> entry : ((Map<?, ?>) value).entrySet()) {
					String subAttributeName = ConvertUtils.getStringSafely(entry.getKey(), "");
					setAttribute(attributeName + '.' + subAttributeName, entry.getValue());
				}
			else
				avpMap.put(attributeName, new LiteralAttributeValue(value));
		}
		public <T> T getIndexedAttribute(Attribute attribute, int index, Class<T> convertTo, boolean required) throws AttributeConversionException {
			return getAttribute(attribute.getValue() + '.' + index, convertTo, required);
		}
		public <T> T getIndexedAttributeDefault(Attribute attribute, int index, java.lang.Class<T> convertTo, T defaultValue) throws AttributeConversionException {
			return getAttributeDefault(attribute.getValue() + '.' + index, convertTo, defaultValue);
		}
		public void setIndexedAttribute(Attribute attribute, int index, Object value) {
			setAttribute(attribute.getValue() + '.' + index, value);
		}

		public void setIndexedSecretAttribute(Attribute attribute, int index, String value) {
			setSecretAttribute(attribute.getValue() + '.' + index, value);
		}

		public <T> T getOptionallyIndexedAttribute(Attribute attribute, int index, Class<T> convertTo, boolean required) throws AttributeConversionException {
			if(index < 1)
				return getAttribute(attribute, convertTo, required);
			T value = getIndexedAttribute(attribute, index, convertTo, required);
			if (value == null)
				return getAttribute(attribute, convertTo, required);
			return value;
		}

		public <T> T getOptionallyIndexedAttributeDefault(Attribute attribute, int index, java.lang.Class<T> convertTo, T defaultValue) throws AttributeConversionException {
			if(index < 1)
				return getAttributeDefault(attribute, convertTo, defaultValue);
			T value = getIndexedAttributeDefault(attribute, index, convertTo, defaultValue);
			if (value == null)
				return getAttributeDefault(attribute, convertTo, defaultValue);
			return value;
		}

		public void setOptionallyIndexedAttribute(Attribute attribute, int index, Object value) {
			if(index < 1)
				setAttribute(attribute, value);
			else
				setIndexedAttribute(attribute, index, value);
		}

		public void setOptionallyIndexedSecretAttribute(Attribute attribute, int index, String value) {
			if(index < 1)
				setSecretAttribute(attribute, value);
			else
				setIndexedSecretAttribute(attribute, index, value);
		}
		@Override
		public void setReferenceIndexedAttribute(Attribute attribute, int index, MessageChainStep referencedStep, Attribute resultAttribute, int resultIndex) {
			setReferenceAttribute(attribute.getValue() + '.' + index, referencedStep, resultAttribute.getValue() + '.' + resultIndex);
		}

		@Override
		public void setReferenceIndexedAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute, int resultIndex) {
			setReferenceAttribute(attribute.getValue(), referencedStep, resultAttribute.getValue() + '.' + resultIndex);
		}

		@Override
		public void setReferenceIndexedAttribute(Attribute attribute, int index, MessageChainStep referencedStep, Attribute resultAttribute) {
			setReferenceAttribute(attribute.getValue() + '.' + index, referencedStep, resultAttribute.getValue());
		}

		@Override
		public void setReferenceOptionallyIndexedAttribute(Attribute attribute, int index, MessageChainStep referencedStep, Attribute resultAttribute, int resultIndex) {
			if(index < 1) {
				if(resultIndex < 1)
					setReferenceAttribute(attribute, referencedStep, resultAttribute);
				else
					setReferenceIndexedAttribute(attribute, referencedStep, resultAttribute, resultIndex);
			} else {
				if(resultIndex < 1)
					setReferenceIndexedAttribute(attribute, index, referencedStep, resultAttribute);
				else
					setReferenceIndexedAttribute(attribute, index, referencedStep, resultAttribute, resultIndex);
			}
		}

		@Override
		public void setReferenceOptionallyIndexedAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute, int resultIndex) {
			if(resultIndex < 1)
				setReferenceAttribute(attribute, referencedStep, resultAttribute);
			else
				setReferenceIndexedAttribute(attribute, referencedStep, resultAttribute, resultIndex);
		}

		@Override
		public void setReferenceOptionallyIndexedAttribute(Attribute attribute, int index, MessageChainStep referencedStep, Attribute resultAttribute) {
			if(index < 1)
				setReferenceAttribute(attribute, referencedStep, resultAttribute);
			else
				setReferenceIndexedAttribute(attribute, index, referencedStep, resultAttribute);
		}

		/**
		 * @see com.usatech.app.MessageChainStep#setSecretAttribute(com.usatech.app.Attribute, java.lang.String)
		 */
		@Override
		public void setSecretAttribute(Attribute attribute, String value) {
			setSecretAttribute(attribute.getValue(), value);
		}

		protected void setSecretAttribute(String attributeName, String value) {
			if(attributeName != null && attributeName.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(value == null)
				avpMap.remove(attributeName);
			else
				avpMap.put(attributeName, new SecretAttributeValue(value));
		}
		/**
		 * @see com.usatech.app.MessageChainStep#setCompressedAttribute(com.usatech.app.Attribute, java.lang.Object)
		 */
		@Override
		public void setCompressedAttribute(Attribute attribute, Object value) throws IOException {
			String name = attribute.getValue();
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			byte[] bytes;
			boolean raw;
			if(value == null) {
				avpMap.remove(name);
				return;
			} else if(value instanceof byte[]) {
				bytes = (byte[])value;
				raw = true;
			} else if(value instanceof String) {
				bytes = ((String)value).getBytes(ENCODING);
				raw = false;
			} else {
				String s = ConvertUtils.getStringSafely(value);
				if(s == null) {
					avpMap.remove(name);
					return;
				}
				bytes = s.getBytes(ENCODING);
				raw = false;
			}
			byte[] compressedValue = IOUtils.compressByteArray(bytes);
			if(compressedValue != null && compressedValue.length > MAX_LITERAL_LENGTH)
				throw new IllegalArgumentException("Data is too long. Its compressed bytes may not exceed " + MAX_LITERAL_LENGTH + " bytes");
			avpMap.put(name, raw ? new CompressedByteArrayAttributeValue(compressedValue) : new CompressedStringAttributeValue(compressedValue));
		}
		/**
		 * @see com.usatech.app.MessageChainStep#setReferenceAttribute(com.usatech.app.Attribute, com.usatech.app.MessageChainStep, com.usatech.app.Attribute)
		 */
		@Override
		public void setReferenceAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute) {
			setReferenceAttribute(attribute.getValue(), referencedStep, resultAttribute.getValue());
		}

		protected void setReferenceAttribute(String attributeName, MessageChainStep referencedStep, String resultAttributeName) {
			if(attributeName != null && attributeName.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(resultAttributeName != null && resultAttributeName.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Result Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			avpMap.put(attributeName, new ReferenceAttributeValuePart(referencedStep, resultAttributeName));
		}
		/**
		 * @see com.usatech.app.MessageChainStep#setReferenceMapAttribute(com.usatech.app.Attribute, com.usatech.app.MessageChainStep, java.lang.String)
		 */
		@Override
		public void setReferenceMapAttribute(Attribute attribute, MessageChainStep referencedStep, String resultAttributesPrefix) {
			String name = attribute.getValue();
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(resultAttributesPrefix != null && resultAttributesPrefix.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Result Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			avpMap.put(name, new ReferenceMapAttributeValue(referencedStep, resultAttributesPrefix));
		}

		/**
		 * @see com.usatech.app.MessageChainStep#createMixedAttribute(com.usatech.app.Attribute)
		 */
		@Override
		public MixedValue createMixedAttribute(Attribute attribute) {
			return createMixedAttribute(attribute.getValue());
		}

		protected MixedValue createMixedAttribute(String attributeName) {
			if(attributeName != null && attributeName.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			MixedAttributeValue mav = new MixedAttributeValue();
			avpMap.put(attributeName, mav);
			return mav;
		}

		public boolean swapReferences(Attribute originalAttribute, MessageChainStep newReferencedStep, Attribute newAttribute) {
			return MessageChainV11.this.swapReferences(this, originalAttribute.getValue(), newReferencedStep, newAttribute.getValue());
		}

		public boolean swapIndexedReferences(Attribute originalAttribute, int originalIndex, MessageChainStep newReferencedStep, Attribute newAttribute, int newIndex) {
			return MessageChainV11.this.swapReferences(this, originalAttribute.getValue() + '.' + originalIndex, newReferencedStep, newAttribute.getValue() + '.' + newIndex);
		}
		public void setResultAttribute(Attribute attribute, Object value) {
			setResultAttribute(attribute.getValue(), value);
		}

		public void setResultAttribute(String attributeName, Object value) {
			if(attributeName != null && attributeName.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(value == null)
				getResultAttributes().remove(attributeName);
			else
				getResultAttributes().put(attributeName, value);
		}

		public void setIndexedResultAttribute(Attribute attribute, int index, Object value) {
			setResultAttribute(attribute.getValue() + '.' + index, value);
		}

		public void setIndexedSecretResultAttribute(Attribute attribute, int index, String value) {
			setResultAttribute(attribute.getValue() + '.' + index, new MaskedString(value));
		}

		public void setOptionallyIndexedResultAttribute(Attribute attribute, int index, Object value) {
			if(index < 1)
				setResultAttribute(attribute, value);
			else
				setIndexedResultAttribute(attribute, index, value);
		}
		/**
		 * @see com.usatech.app.MessageChainStep#setSecretResultAttribute(com.usatech.app.Attribute, java.lang.String)
		 */
		@Override
		public void setSecretResultAttribute(Attribute attribute, String value) {
			setResultAttribute(attribute, new MaskedString(value));
		}

		@Override
		public void setSecretResultAttribute(String attribute, String value) {
			setResultAttribute(attribute, new MaskedString(value));
		}

		public void addPassThruReferenceAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute) {
			addPassThruReferenceAttribute(attribute.getValue(), referencedStep, resultAttribute.getValue());
		}

		/**
		 * @see com.usatech.app.MessageChainStep#getAttributes()
		 */
		public Map<String, Object> getAttributes() {
			if(attributes == null)
				attributes = new AttributeMap(avpMap);
			return attributes;
		}
		/**
		 * @see com.usatech.app.MessageChainStep#getQueueKey()
		 */
		public String getQueueKey() {
			return queueKey;
		}
		/**
		 * @see com.usatech.app.MessageChainStep#getResultAttributes()
		 */
		public Map<String, Object> getResultAttributes() {
			return resultAttributes;
		}

		/**
		 * @see com.usatech.app.MessageChainStep#getResultCode()
		 */
		public int getResultCode() {
			return resultCode;
		}
		/**
		 * @see com.usatech.app.MessageChainStep#removeAttribute(java.lang.String)
		 */
		@Override
		public void removeAttribute(String name) {
			avpMap.remove(name);
		}
		/**
		 * @see com.usatech.app.MessageChainStep#addLiteralAttribute(java.lang.String, java.lang.Object)
		 */
		@Override
		public void addLiteralAttribute(String name, Object value) {
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(value == null)
				avpMap.remove(name);
			else
				avpMap.put(name, new LiteralAttributeValue(value));
		}
		/**
		 * @see com.usatech.app.MessageChainStep#addStringAttribute(java.lang.String, java.lang.String)
		 */
		public void addStringAttribute(String name, String value) {
			addLiteralAttribute(name, value);
		}
		/**
		 * @see com.usatech.app.MessageChainStep#addSecretAttribute(java.lang.String, java.lang.String)
		 */
		public void addSecretAttribute(String name, String value) {
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(value == null)
				avpMap.remove(name);
			else {
				if(value.length() > MAX_LITERAL_LENGTH)
					throw new IllegalArgumentException("Literal is too long. It may not exceed " + MAX_LITERAL_LENGTH + " bytes");
				avpMap.put(name, new SecretAttributeValue(value));
			}
		}
		/*
		public void addEncryptedAttribute(String name, ByteBuffer unencryptedValue, String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAliasPrefix) throws GeneralSecurityException {
			byte[] bytes = new byte[unencryptedValue.remaining()];
			unencryptedValue.get(bytes);
			addEncryptedAttribute(name, bytes, cipherName, secondaryCipherName, keyGenerator, keyAliasPrefix);
		}
		public void addEncryptedAttribute(String name, byte[] unencryptedValue, String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAliasPrefix) throws GeneralSecurityException {
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(unencryptedValue == null)
				avpMap.remove(name);
			else {
				String keyAlias = SecurityUtils.findLatestPublicKeyAlias(keyAliasPrefix);
				if(keyAlias == null)
					throw new IllegalArgumentException("Key Alias Prefix '" + keyAliasPrefix + "' does not match any keys in the store");
				avpMap.put(name, new EncryptedAttributeValue(new EncryptedBytes(cipherName, secondaryCipherName, keyGenerator, keyAlias, unencryptedValue)));
			}
		}
		public void addEncryptedAttribute(String name, String unencryptedValue, String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAliasPrefix) throws GeneralSecurityException {
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(unencryptedValue == null)
				avpMap.remove(name);
			else {
				String keyAlias = SecurityUtils.findLatestPublicKeyAlias(keyAliasPrefix);
				if(keyAlias == null)
					throw new IllegalArgumentException("Key Alias Prefix '" + keyAliasPrefix + "' does not match any keys in the store");
				avpMap.put(name, new EncryptedAttributeValue(new EncryptedString(cipherName, secondaryCipherName, keyGenerator, keyAlias, unencryptedValue, ENCODING)));
			}
		}
		*/
		public void addReferenceAttribute(String name, MessageChainStep referencedStep, String resultAttributeName) {
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(resultAttributeName != null && resultAttributeName.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Result Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			avpMap.put(name, new ReferenceAttributeValuePart(referencedStep, resultAttributeName));
		}
		public void addReferenceMapAttribute(String name, MessageChainStep referencedStep, String resultAttributesPrefix) {
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(resultAttributesPrefix != null && resultAttributesPrefix.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Result Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			avpMap.put(name, new ReferenceMapAttributeValue(referencedStep, resultAttributesPrefix));
		}

		public void addPassThruReferenceAttribute(String name, MessageChainStep referencedStep, String resultAttributeName) {
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			ReferenceAttributeValuePart rv = new ReferenceAttributeValuePart(referencedStep, resultAttributeName);
			AttributeValue av = avpMap.get(name);
			PassThruReferenceAttributeValue ptrav;
			if(av instanceof PassThruReferenceAttributeValue) 
				ptrav = (PassThruReferenceAttributeValue) av;
			else {
				ptrav = new PassThruReferenceAttributeValue(new ArrayList<ReferenceAttributeValuePart>(3));
				avpMap.put(name, ptrav);
			}
			ptrav.addReferencingValue(rv);
		}

		public void addByteArrayAttribute(String name, byte[] value) {
			addLiteralAttribute(name, value);
		}
		public void addCompressedStringAttribute(String name, String value) throws IOException{
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(value != null){
				byte[] compressedValue = IOUtils.compressByteArray(value.getBytes(ENCODING));
				if(compressedValue != null && compressedValue.length > MAX_LITERAL_LENGTH)
					throw new IllegalArgumentException("String is too long. Its compressed bytes may not exceed " + MAX_LITERAL_LENGTH + " bytes");
				avpMap.put(name, new CompressedStringAttributeValue(compressedValue));
			} else {
				avpMap.remove(name);
			}

		}
		public void addCompressedByteArrayAttribute(String name, byte[] value) throws IOException{
			if(name != null && name.length() > MAX_NAME_LENGTH)
				throw new IllegalArgumentException("Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
			if(value != null){
				byte[] compressedValue = IOUtils.compressByteArray(value);
				if(compressedValue != null && compressedValue.length > MAX_LITERAL_LENGTH)
					throw new IllegalArgumentException("String is too long. Its compressed bytes may not exceed " + MAX_LITERAL_LENGTH + " bytes");
				avpMap.put(name, new CompressedByteArrayAttributeValue(compressedValue));
			} else {
				avpMap.remove(name);
			}
		}
		public void addBooleanAttribute(String name, boolean value) {
			addLiteralAttribute(name, value);
		}
		public void addByteAttribute(String name, byte value) {
			addLiteralAttribute(name, value);
		}
		public void addIntAttribute(String name, int value) {
			addLiteralAttribute(name, value);
		}
		public void addLongAttribute(String name, long value) {
			addLiteralAttribute(name, value);
		}
		public void addShortAttribute(String name, short value) {
			addLiteralAttribute(name, value);
		}
		public void write(ByteOutput output) throws IOException {
			if(queueKey == null || queueKey.length() == 0) {
				output.writeShort(0);
			} else {
				byte[] bytes = queueKey.getBytes(ENCODING);
				output.writeShort(bytes.length);
				output.write(bytes);
			}
			output.writeByte(bitmap.getByte(0));
			output.writeShort(avpMap.size());
			for(Map.Entry<String, AttributeValue> entry : avpMap.entrySet()) {
				writeSmallString(output, entry.getKey());
				entry.getValue().writeValue(output, getResultCode() != -1);
			}
			output.writeByte(resultStepCount);
			for(Map.Entry<Integer, MessageChainStepV11[]> entry : resultMap.entrySet()) {
				if(entry.getValue() != null)
					for(MessageChainStep resultStep : entry.getValue()) {
						output.writeByte(entry.getKey());
						int stepIndex = resultStep.getStepIndex();
						if(stepIndex < 1 || stepIndex > steps.size())
							throw new IOException("This step was not found in the message chain; message chain may not be written");
						output.writeByte(stepIndex);
					}
			}
			output.writeShort(resultAttributes.size());
			for(Map.Entry<String,Object> entry : resultAttributes.entrySet()) {
				writeSmallString(output, entry.getKey());
				writeLiteralValue(output, entry.getValue(), !referencedResultAttributes.contains(entry.getKey()));
			}
			switch(version) {
				case 2:
					output.writeShort(resultCode);
					output.writeLong(startTime);
					output.writeLong(endTime);
					writeSmallString(output, executor);
					break;
			}
			if(bitmap.getBit(5))
				output.writeLong(correlation);
			if(bitmap.getBit(4)) {
				output.writeByte(waitFors.size());
				for(MessageChainStep wf : waitFors)
					output.writeByte(wf.getStepIndex());
			}
		}
		public boolean isMulticast() {
			return bitmap.getBit(7);
		}
		public void setMulticast(boolean multicast) {
			bitmap.setBit(7, multicast);
		}

		public boolean isForked() {
			if(isMulticast())
				return true;
			if(isJoined())
				return false;
			MessageChainStepV11 prev = getPrevStep();
			if(prev == null)
				return false;
			return prev.isForked();
		}

		public boolean isJoined() {
			return bitmap.getBit(3);
		}

		public void setJoined(boolean joined) {
			bitmap.setBit(3, joined);
		}
		/**
		 * @see com.usatech.app.MessageChainStep#getNextSteps(int)
		 */
		@Override
		public MessageChainStep[] getNextSteps(int resultCode) {
			MessageChainStep[] nextSteps = resultMap.get(resultCode);
			return nextSteps == null || nextSteps.length == 0 ? nextSteps : nextSteps.clone();
		}

		protected MessageChainStepV11 getPrevStep() {
			for(MessageChainStepV11 checkStep : steps) {
				if(checkStep.getResultCode() >= 0) {
					MessageChainStep[] nextSteps = checkStep.getNextSteps(checkStep.getResultCode());
					if(nextSteps != null && CollectionUtils.iterativeSearch(nextSteps, this) >= 0) {
						return checkStep;
					}
				}
			}
			return null;
		}

		/**
		 * @see com.usatech.app.MessageChainStep#setNextSteps(int, MessageChainStep...)
		 */
		public void setNextSteps(int resultCode, MessageChainStep... nextSteps) {
			if(resultCode < 0 || resultCode > MAX_RESULT_CODE)
				throw new IllegalArgumentException("Invalid Result Code. It must be between 0 and " + MAX_RESULT_CODE);
			MessageChainStepV11[] newNextSteps;
			if(nextSteps != null && nextSteps.length > 0) {
				newNextSteps = new MessageChainStepV11[nextSteps.length]; // protect against changes after this method is called
				for(int i = 0; i < nextSteps.length; i++) {
					if(nextSteps[i] instanceof MessageChainStepV11) {
						if(nextSteps[i].getStepIndex() > steps.size() || steps.get(nextSteps[i].getStepIndex() - 1) != nextSteps[i]) {
							throw new IllegalArgumentException("This Step does not belong to this Message Chain (" + getMessageChain().getGuid() + "). It belongs to " + nextSteps[i].getMessageChain().getGuid());
						}
						newNextSteps[i] = (MessageChainStepV11) nextSteps[i];
					} else
						throw new IllegalArgumentException("Message Chain is not of type '" + MessageChainStepV11.class.getName() + "'");
				}
			} else
				newNextSteps = null;
			MessageChainStep[] oldNextSteps = resultMap.remove(resultCode);
			if(oldNextSteps != null && oldNextSteps.length > 0)
				resultStepCount -= oldNextSteps.length;
			if(newNextSteps != null && newNextSteps.length > 0) {
				resultMap.put(resultCode, newNextSteps);
				resultStepCount += newNextSteps.length;
			}
		}

		public void addNextSteps(int resultCode, MessageChainStep... nextSteps) {
			if(resultCode < 0 || resultCode > MAX_RESULT_CODE)
				throw new IllegalArgumentException("Invalid Result Code. It must be between 0 and " + MAX_RESULT_CODE);
			if(nextSteps == null || nextSteps.length == 0)
				return;
			MessageChainStepV11[] oldNextSteps = resultMap.get(resultCode);
			MessageChainStepV11[] newNextSteps;
			int offset;
			if(oldNextSteps != null) {
				offset = oldNextSteps.length;
				newNextSteps = new MessageChainStepV11[nextSteps.length + offset]; // protect against changes after this method is called
				for(int i = 0; i < offset; i++)
					newNextSteps[i] = oldNextSteps[i];
			} else {
				offset = 0;
				newNextSteps = new MessageChainStepV11[nextSteps.length]; // protect against changes after this method is called
			}
			for(int i = 0; i < nextSteps.length; i++) {
				if(nextSteps[i] instanceof MessageChainStepV11) {
					if(nextSteps[i].getStepIndex() > steps.size() || steps.get(nextSteps[i].getStepIndex() - 1) != nextSteps[i]) {
						throw new IllegalArgumentException("This Step does not belong to this Message Chain (" + getMessageChain().getGuid() + "). It belongs to " + nextSteps[i].getMessageChain().getGuid());
					}
					newNextSteps[i + offset] = (MessageChainStepV11) nextSteps[i];
				} else
					throw new IllegalArgumentException("Message Chain is not of type '" + MessageChainStepV11.class.getName() + "'");
			}
			resultMap.put(resultCode, newNextSteps);
			resultStepCount += nextSteps.length;
		}

		public void removeNextSteps(int resultCode, MessageChainStep... nextSteps) {
			if(resultCode < 0 || resultCode > MAX_RESULT_CODE)
				throw new IllegalArgumentException("Invalid Result Code. It must be between 0 and " + MAX_RESULT_CODE);
			if(nextSteps == null || nextSteps.length == 0)
				return;
			MessageChainStepV11[] oldNextSteps = resultMap.get(resultCode);
			if(oldNextSteps != null && oldNextSteps.length > 0) {
				Set<MessageChainStepV11> nextStepSet = new LinkedHashSet<MessageChainStepV11>();
				for(int i = 0; i < oldNextSteps.length; i++)
					nextStepSet.add(oldNextSteps[i]);
				for(int i = 0; i < nextSteps.length; i++)
					nextStepSet.remove(nextSteps[i]);
				MessageChainStepV11[] newNextSteps = nextStepSet.toArray(new MessageChainStepV11[nextStepSet.size()]);
				resultMap.put(resultCode, newNextSteps);
				resultStepCount -= oldNextSteps.length;
				resultStepCount += newNextSteps.length;
			}
		}

		public void removeDownstreamJoinedSteps() {
			Set<MessageChainStepV11> checked = new HashSet<MessageChainStepV11>();
			removeDownstreamJoinedSteps(checked);
		}

		protected void removeDownstreamJoinedSteps(Set<MessageChainStepV11> checked) {
			Iterator<Map.Entry<Integer, MessageChainStepV11[]>> entryIter;
			if(resultCode < 0)
				entryIter = resultMap.entrySet().iterator();
			else {
				final Integer rc = resultCode;
				if(!resultMap.containsKey(rc))
					return;
				entryIter = new Iterator<Map.Entry<Integer, MessageChainStepV11[]>>() {
					protected boolean next = true;

					public boolean hasNext() {
						return next;
					}

					public Map.Entry<Integer, MessageChainStepV11[]> next() {
						if(!hasNext())
							throw new NoSuchElementException();
						next = false;
						return new Map.Entry<Integer, MessageChainStepV11[]>() {
							public Integer getKey() {
								return rc;
							}

							public MessageChainStepV11[] getValue() {
								return resultMap.get(rc);
							}

							public MessageChainStepV11[] setValue(MessageChainStepV11[] value) {
								return resultMap.put(rc, value);
							}
						};
					}

					public void remove() {
						if(!next)
							resultMap.remove(rc);
					}
				};
			}

			while(entryIter.hasNext()) {
				Map.Entry<Integer, MessageChainStepV11[]> entry = entryIter.next();
				if(entry.getValue() != null) {
					Set<MessageChainStepV11> newNextSteps = null;
					for(MessageChainStepV11 nextStep : entry.getValue())
						if(checked.add(nextStep)) {
							if(nextStep.isJoined()) {
								stepChain = null;
								// remove from resultMap, stepChain
								if(entry.getValue().length == 1)
									entryIter.remove();
								else {
									if(newNextSteps == null) {
										newNextSteps = new LinkedHashSet<MessageChainStepV11>();
										for(MessageChainStepV11 nextStep2 : entry.getValue())
											if(nextStep2 != nextStep)
												newNextSteps.add(nextStep2);
									} else
										newNextSteps.remove(nextStep);
								}
							} else {
								nextStep.removeDownstreamJoinedSteps(checked);
							}
						}

					if(newNextSteps != null) {
						if(newNextSteps.isEmpty())
							entryIter.remove();
						else
							entry.setValue(newNextSteps.toArray(new MessageChainStepV11[newNextSteps.size()]));
					}
				}
			}
		}
		/**
		 * @see com.usatech.app.MessageChainStep#getResultCodes()
		 */
		@Override
		public Set<Integer> getResultCodes() {
			if(resultCodes == null)
				resultCodes = Collections.unmodifiableSet(resultMap.keySet());
			return resultCodes;
		}
		public boolean isTemporary() {
			return bitmap.getBit(6);
		}
		public void setTemporary(boolean temporary) {
			bitmap.setBit(6, temporary);
		}
		/**
		 * @see com.usatech.app.MessageChainStep#serialize(simple.io.ByteOutput,simple.security.crypt.EncryptionInfo)
		 */
		@Override
		public void serialize(ByteOutput output, EncryptionInfo encryptionInfo) throws IOException {
			if(stepIndex < 1 || stepIndex > steps.size())
				throw new IOException("This step was not found in the message chain; message chain may not be written");
			switch(version) {
				case 1:
					output.writeByte(MESSAGE_CHAIN_VERSION_1);
					writeChain(output, stepIndex);
					break;
				case 2:
					if(encryptionInfo == null || encryptionInfo.getCipherName() == null) {
						output.writeByte(MESSAGE_CHAIN_VERSION_2);
						writeSmallString(output, guid);
						output.writeShort(instance);
						writeChain(output, stepIndex);
					} else {
						ByteArrayByteOutput chainOutput = new ByteArrayByteOutput(256);
						writeChain(chainOutput, stepIndex);

						try {
							String keyAlias = CryptUtils.findLatestPublicKeyAlias(encryptionInfo.getKeyAliasPrefix());
							if(keyAlias == null) {
								KeySupply ks = CryptUtils.getDefaultTrustSupply();
								throw new GeneralSecurityException("Could not find key in truststore beginning with '" + encryptionInfo.getKeyAliasPrefix() + "' from " + ks + " with aliases " + ks.getAliases());
							}
							String cipherName = encryptionInfo.getCipherName();
							Key key = CryptUtils.getDefaultTrustSupply().getPublicKey(keyAlias);
							int max = CryptUtils.getMaxDataSize(cipherName, key);
							String secondaryCipherName = encryptionInfo.getSecondaryCipherName();
							byte[] encrypted;
							byte[] encryptedKey;
							if(max > 0 && chainOutput.getPosition() > max && secondaryCipherName != null) {
								SecretKey skey = encryptionInfo.getKeyGenerator().generateKey();
								encryptedKey = CryptUtils.encrypt(cipherName, keyAlias, skey.getEncoded());
								encrypted = CryptUtils.encrypt(secondaryCipherName, skey, chainOutput.getBytes(), 0, chainOutput.getPosition());
							} else {
								encryptedKey = null;
								encrypted = CryptUtils.encrypt(cipherName, keyAlias, chainOutput.getBytes(), 0, chainOutput.getPosition());
							}
							if(encrypted.length > TWO_BYTE_LENGTH)
								output.writeByte(MESSAGE_CHAIN_VERSION_2_ENCRYPTED_LONG);
							else
								output.writeByte(MESSAGE_CHAIN_VERSION_2_ENCRYPTED);
							writeSmallString(output, guid);
							output.writeShort(instance);
							writeSmallString(output, keyAlias);
							writeSmallString(output, cipherName);
							if(encrypted.length > TWO_BYTE_LENGTH)
								writeLargeByteArray(output, encrypted);
							else
								writeMediumByteArray(output, encrypted);
							if(encryptedKey != null) {
								writeSmallString(output, secondaryCipherName);
								writeMediumByteArray(output, encryptedKey);
							} else {
								writeSmallString(output, null);
							}
						} catch(GeneralSecurityException e) {
							throw new IOException(e);
						}
					}
					break;
				default:
					throw new IOException("Version " + version + " not supported");
			}
		}

		public void setQueueKey(String queueKey) {
			this.queueKey = queueKey;
			resultCode = -1; // reset this
		}

		public Long getCorrelation() {
			return bitmap.getBit(5) ? correlation : null;
		}

		public void setCorrelation(Long correlation) {
			this.correlation = correlation;
			bitmap.setBit(5, correlation != null);
		}

		public int getStepIndex() {
			return stepIndex;
		}

		public Set<MessageChainStep> getWaitFors() {
			if(bitmap.getBit(4)) {
				if(waitForsUnmod == null)
					waitForsUnmod = Collections.unmodifiableSet(waitFors);
				return waitForsUnmod;
			}
			return null;
		}

		public void addWaitFor(MessageChainStep waitFor) {
			waitFors.add(waitFor);
			bitmap.setBit(4, true);
		}

		public void removeWaitFor(MessageChainStep waitFor) {
			if(waitFors.remove(waitFor) && waitFors.isEmpty())
				bitmap.setBit(4, false);
		}

		@Override
		public MessageChainStep getPreviousStep() {
			return getPrevStep();
		}
	}
	protected interface AttributeValue {
		public Object getValue() ;
		public void writeValue(ByteOutput output, boolean maskData) throws IOException ;
	}

	protected interface ReferencingValue extends AttributeValue {
		public void addReferencedAttributes();
	}
	protected static final int ONE_BYTE_LENGTH = (1 << (8 * 1)) - 1;
	protected static final int TWO_BYTE_LENGTH = (1 << (8 * 2)) - 1;
	protected static final int THREE_BYTE_LENGTH = (1 << (8 * 3)) - 1;
	protected static final int FOUR_BYTE_LENGTH = Integer.MAX_VALUE; //(1 << (8 * 4)) - 1;

	protected static final int MAX_NAME_LENGTH = ONE_BYTE_LENGTH;
	protected static final int MAX_VALUE_LENGTH = TWO_BYTE_LENGTH;
	//protected static final int MAX_LITERAL_LENGTH = 255 * 128;
	protected static final int MAX_LITERAL_LENGTH = Integer.MAX_VALUE;
	protected static final int MAX_REFERENCE_LENGTH = ONE_BYTE_LENGTH;
	protected static final int MAX_REFERENCE_INDEX = ONE_BYTE_LENGTH;
	protected static final int MAX_RESULT_CODE = ONE_BYTE_LENGTH;
	protected static final int MAX_QUEUE_KEY_LENGTH = TWO_BYTE_LENGTH;
	protected static final int NULL_ATTRIBUTE_VALUE_TYPE = 0;
	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE = 1;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE = 2;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE = 4;
	protected static final int BOOLEAN_TRUE_ATTRIBUTE_VALUE_TYPE = 5;
	protected static final int BOOLEAN_FALSE_ATTRIBUTE_VALUE_TYPE = 6;
	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE = 65;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE = 66;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE = 68;
	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE = 73;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE = 74;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE = 76;
	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE = 77;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE = 78;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE = 80;
	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE = 81;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE = 82;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE = 84;
	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE = 85;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE = 86;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE = 88;
	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE = 89;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE = 90;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE = 92;
	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_LIST_TYPE = 11;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_LIST_TYPE = 12;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_LIST_TYPE = 14;

	protected static final int REFERENCE_ATTRIBUTE_VALUE_TYPE = 16;
	protected static final int REFERENCE_MAP_VALUE_TYPE = 17;
	protected static final int PASS_THRU_REFERENCE_ATTRIBUTE_VALUE_TYPE = 18;
	protected static final int ALTERNATING_ATTRIBUTE_VALUE_TYPE = 32;
	protected static final int BYTE_ATTRIBUTE_VALUE_TYPE = 129;
	protected static final int SHORT_ATTRIBUTE_VALUE_TYPE = 130;
	protected static final int CHAR_ATTRIBUTE_VALUE_TYPE = 131;
	protected static final int INT_ATTRIBUTE_VALUE_TYPE = 132;
	protected static final int LONG_ATTRIBUTE_VALUE_TYPE = 136;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE = 33;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE = 34;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE = 35;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE = 36;

	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE = 96;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE = 97;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE = 99;

	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_MASKED_VALUE_TYPE = 41;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_MASKED_VALUE_TYPE = 42;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_MASKED_VALUE_TYPE = 44;
	protected static final char[] SMALL_MASK = createMask(256);

	protected static char[] createMask(int len) {
		char[] mask = new char[len];
		Arrays.fill(mask, '*');
		return mask;
	}

	protected static String getMaskedString(int len) {
		if(len == 0)
			return "";
		if(len <= SMALL_MASK.length)
			return new String(SMALL_MASK, 0, len);
		return new String(createMask(len));
	}
	/*
	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_BYTE_ARRAY_VALUE_TYPE = 105;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_BYTE_ARRAY_VALUE_TYPE = 106;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_BYTE_ARRAY_VALUE_TYPE = 108;

	protected static final int ONE_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_STRING_VALUE_TYPE = 110;
	protected static final int TWO_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_STRING_VALUE_TYPE = 111;
	protected static final int FOUR_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_STRING_VALUE_TYPE = 113;
	*/
	protected interface AttributeValuePart {
		public Object getValue() ;
		public void writePart(ByteOutput output) throws IOException ;
	}
	protected class StringAttributeValuePart implements AttributeValuePart, AttributeValue {
		protected final String value;
		public StringAttributeValuePart(String value) {
			this.value = value;
		}
		public String getValue() {
			return value;
		}
		public void writePart(ByteOutput output) throws IOException {
			writeSmallString(output, value);
		}
		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			writeStringValue(output, value);
		}
	}
	/*
	protected class EncryptedAttributeValue implements AttributeValue {
		protected final AbstractEncryptedObject<?> encryptedObject;
		protected final boolean asString;
		public EncryptedAttributeValue(EncryptedString encryptedString) {
			this.encryptedObject = encryptedString;
			this.asString = true;
			encryptedString.setEncoding(ENCODING);
		}
		public EncryptedAttributeValue(EncryptedBytes encryptedBytes) {
			this.encryptedObject = encryptedBytes;
			this.asString = false;
		}

		public Object getValue() {
			return encryptedObject;
		}
		public void writeValue(ByteOutput output) throws IOException {
			try {
				writeEncryptedValue(output, encryptedObject, asString);
			} catch(GeneralSecurityException e) {
				throw new IOException("Could not encrypt value", e);
			}
		}
	}
	*/
	
	protected class ReferenceAttributeValuePart implements AttributeValuePart, ReferencingValue {
		protected final String name;
		protected final MessageChainStep step;
		public ReferenceAttributeValuePart(MessageChainStep step, String name) {
			this.name = name;
			this.step = step;
		}
		public Object getValue() {
			return step == null ? null : step.getResultAttributes().get(name);
		}
		public void writePart(ByteOutput output) throws IOException {
			if(step == null)
				output.writeByte(0);
			else {
				int stepIndex = step.getStepIndex();
				if(stepIndex < 1 || stepIndex > steps.size())
					throw new IOException("This step was not found in the message chain; message chain may not be written");
				output.writeByte(stepIndex);
				writeSmallString(output, name);
			}
		}
		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			output.writeByte(REFERENCE_ATTRIBUTE_VALUE_TYPE);
			writePart(output);
		}

		public void addReferencedAttributes() {
			if(name != null && step instanceof MessageChainStepV11) {
				MessageChainStepV11 stepV11 = (MessageChainStepV11) step;
				stepV11.referencedResultAttributes.add(name);
			}
		}
	}

	protected class ReferenceMapAttributeValue implements ReferencingValue {
		protected final String prefix;
		protected final MessageChainStep step;
		public ReferenceMapAttributeValue(MessageChainStep step, String prefix) {
			this.prefix = prefix;
			this.step = step;
		}
		public Object getValue() {
			return step == null ? null : Collections.unmodifiableMap(new FilterMap<Object>(step.getResultAttributes(), prefix, null));
		}

		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			output.writeByte(REFERENCE_MAP_VALUE_TYPE);
			if(step == null)
				output.writeByte(0);
			else {
				int stepIndex = step.getStepIndex();
				if(stepIndex < 1 || stepIndex > steps.size())
					throw new IOException("This step was not found in the message chain; message chain may not be written");
				output.writeByte(stepIndex);
				writeSmallString(output, prefix);
			}
		}

		@Override
		public void addReferencedAttributes() {
			if(step instanceof MessageChainStepV11) {
				MessageChainStepV11 stepV11 = (MessageChainStepV11) step;
				for(String name : step.getResultAttributes().keySet())
					if(prefix == null || name.startsWith(prefix))
						stepV11.referencedResultAttributes.add(name);
			}
		}
	}

	protected class MixedAttributeValue implements ReferencingValue, MixedValue {
		protected final List<AttributeValuePart> parts = new ArrayList<AttributeValuePart>();
		public MixedAttributeValue() {
		}

		public Object getValue() {
			StringBuilder sb = new StringBuilder();
			StringBuilder unmasked = null;
			int last = 0;
			for(AttributeValuePart part : parts) {
				Object value = part.getValue();
				if(value instanceof MaskedString) {
					MaskedString ms = (MaskedString) value;
					if(unmasked == null)
						unmasked = new StringBuilder(sb);
					else
						unmasked.append(sb, last, sb.length());
					if(ms.getValue() != null)
						unmasked.append(ms.getValue());
					if(ms.toString() != null)
						sb.append(ms.toString());
					last = sb.length();
				} else if(value != null)
					sb.append(value);
			}
			if(unmasked != null) {
				unmasked.append(sb, last, sb.length());
				return new MaskedString(unmasked.toString(), sb.toString());
			}
			return sb.toString();
		}
		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			output.writeByte(ALTERNATING_ATTRIBUTE_VALUE_TYPE);
			output.writeByte(parts.size());
			for(AttributeValuePart part : parts) {
				part.writePart(output);
			}
		}

		@Override
		public void addReferencePart(MessageChainStep referencedStep, Attribute resultAttribute) {
			addReferencePart(referencedStep, resultAttribute.getValue());
		}

		public void addReferencePart(MessageChainStep referencedStep, String resultAttributeName) {
			if(parts.size() % 2 == 0)
				parts.add(new StringAttributeValuePart(null));
			parts.add(new ReferenceAttributeValuePart(referencedStep, resultAttributeName));
		}

		public void addIndexedReferencePart(MessageChainStep referencedStep, Attribute resultAttribute, int index) {
			addReferencePart(referencedStep, resultAttribute.getValue() + '.' + index);
		}

		@Override
		public void addLiteralPart(String literal) {
			if(parts.size() % 2 == 1)
				parts.add(new ReferenceAttributeValuePart(null, null));
			parts.add(new StringAttributeValuePart(literal));
		}

		@Override
		public void clear() {
			parts.clear();
		}

		@Override
		public void addReferencedAttributes() {
			for(AttributeValuePart part : parts) {
				if(part instanceof ReferencingValue)
					((ReferencingValue) part).addReferencedAttributes();
			}
		}
	}

	protected class PassThruReferenceAttributeValue implements ReferencingValue {
		protected final List<ReferenceAttributeValuePart> referencingValues;

		public PassThruReferenceAttributeValue(List<ReferenceAttributeValuePart> referencingValues) {
			this.referencingValues = referencingValues;
		}

		@Override
		public Object getValue() {
			ListIterator<ReferenceAttributeValuePart> iter = referencingValues.listIterator(referencingValues.size());
			while(iter.hasPrevious()) {
				Object o = iter.previous().getValue();
				if(o != null)
					return o;
			}
			return null;
		}

		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			output.writeByte(PASS_THRU_REFERENCE_ATTRIBUTE_VALUE_TYPE);
			output.writeByte(referencingValues.size());
			for(ReferenceAttributeValuePart rv : referencingValues)
				rv.writePart(output);
		}

		@Override
		public void addReferencedAttributes() {
			for(ReferencingValue rv : referencingValues)
				rv.addReferencedAttributes();
		}

		public void addReferencingValue(ReferenceAttributeValuePart rv) {
			referencingValues.add(rv);
		}
	}

	protected int currentStepIndex;
	protected int resultCode = -1;
	protected final List<MessageChainStepV11> steps = new ArrayList<MessageChainStepV11>();
	protected String guid;
	protected int instance = 1;
	protected int version;
	protected List<MessageChainStepV11> stepChain;
	protected Iterable<MessageChainStep> allWaitFors;

	public MessageChainV11() {
		this.version = 2;
	}

	public String getGuid() {
		if(guid == null) {
			guid = GUID_PREFIX + guidGenerator.getAndIncrement();
		}
		return guid;
	}

	public int getInstance() {
		return instance;
	}

	/**
	 * @see com.usatech.app.MessageChain#getExecutionInfo(int)
	 */
	@Override
	public String getExecutionInfo(int level) {
		if(level <= 0) {
			return "";
		} else if(level == 1) {
			if(currentStepIndex >= 1 && currentStepIndex <= steps.size()) {
				MessageChainStepV11 step = steps.get(currentStepIndex-1);
				return step.queueKey;
			}
			return "";
		}
		if(steps.size() == 0)
			return "[No Steps]";
		if(currentStepIndex < 1 || currentStepIndex > steps.size())
			return "[NO CURRENT STEP]";
		StringBuilder sb = new StringBuilder();
		for(MessageChainStepV11 step : getStepChain()) {
			if(step == null)
				sb.append("[UNKNOWN");
			else {
				sb.append("[\"").append(step.getQueueKey());
				if(step.executor != null && step.executor.length() > 0)
					sb.append("\" by ").append(step.executor);
				else
					sb.append('"');
				if(level > 2 && step.startTime > 0 && step.endTime >= step.startTime) {
					sb.append(" in ").append(step.endTime-step.startTime).append("ms");
				}
				if(level > 3) {
					sb.append(": attributes=").append(step.getAttributes());
					if(step.getResultCode() >= 0)
						sb.append("; resultAttributes=").append(step.getResultAttributes()).append("; resultCode=").append(step.getResultCode());
				}
			}
			sb.append("]<--");
		}
		return sb.substring(0, sb.length() - 3);
	}

	protected List<MessageChainStepV11> getStepChain() {
		if(stepChain == null) {
			if(currentStepIndex < 1 || currentStepIndex > steps.size())
				stepChain = Collections.emptyList();
			else {
				MessageChainStepV11 step = steps.get(currentStepIndex-1);
				Set<MessageChainStepV11> remaining = new HashSet<MessageChainStepV11>(steps);
				List<MessageChainStepV11> chain = new ArrayList<MessageChainStepV11>();
				chain.add(step);
				remaining.remove(step);
				do {
					MessageChainStepV11 prevStep = null;
					for(MessageChainStepV11 checkStep : remaining) {						
						if(checkStep.getResultCode() >= 0) {
							MessageChainStep[] nextSteps = checkStep.getNextSteps(checkStep.getResultCode());
							if(nextSteps != null && CollectionUtils.iterativeSearch(nextSteps, step) >= 0) {
								if(prevStep == null || prevStep.startTime < checkStep.startTime) {
									prevStep = checkStep;
								} else if(prevStep.startTime == checkStep.startTime) {
									// indeterminant
									prevStep = null;
									break;
								}
							}
						}
					}
					if(prevStep == null && step == steps.get(0)) {
						break;
					}
					step = prevStep;
					chain.add(step);
					remaining.remove(step);
				} while(step != null) ;
				stepChain = chain;
			}
		}
		return stepChain;
	}

	/**
	 * @see com.usatech.app.MessageChain#getCurrentStep()
	 */
	public MessageChainStepV11 getCurrentStep() {
		if(currentStepIndex < 1 || currentStepIndex > steps.size())
			throw new IndexOutOfBoundsException("Current Step Index '" + currentStepIndex + "' is invalid. It must be between 1 and " + (steps.size()));
		MessageChainStepV11 step = steps.get(currentStepIndex-1);
		if(step.startTime == 0)
			step.startTime = System.currentTimeMillis();
		return step;
	}

	@Override
	public void setCurrentStep(MessageChainStep step) {
		currentStepIndex = step.getStepIndex();
	}
	
	// Results methods
	/**
	 * @see com.usatech.app.MessageChain#setResultCode(int)
	 */
	public boolean setResultCode(int resultCode) {
		if(resultCode < 0 || resultCode > MAX_RESULT_CODE)
			throw new IllegalArgumentException("Result Code " + resultCode + " is not valid. It must be between 0 and " + MAX_RESULT_CODE);
		if(currentStepIndex >= 1 && currentStepIndex <= steps.size()) {
			MessageChainStepV11 step = steps.get(currentStepIndex-1);
			step.resultCode = resultCode;
			step.endTime = System.currentTimeMillis();
			step.executor = EXECUTOR;
		}
		this.resultCode = resultCode;
		stepChain = null;
		return isComplete();
	}

	protected void recalculateReferencedAttributes(int stepIndex) {
		// calculate referencedResultAttributes
		for(MessageChainStepV11 step : steps) {
			step.referencedResultAttributes.clear();
		}
		if(stepIndex >= 1 && stepIndex <= steps.size()) {
			MessageChainStepV11 step = steps.get(stepIndex - 1);
			Set<MessageChainStepV11> processed = new HashSet<MessageChainStepV11>();
			recalculateReferencedAttributes(step, processed);
		}
	}

	protected void recalculateReferencedAttributes(MessageChainStepV11 step, Set<MessageChainStepV11> processed) {
		if(processed.add(step)) {
			for(AttributeValue av : step.avpMap.values()) {
				if(av instanceof ReferencingValue)
					((ReferencingValue) av).addReferencedAttributes();
			}
			if(step.resultCode < 0) {
				for(MessageChainStepV11[] nextSteps : step.resultMap.values())
					if(nextSteps != null)
						for(MessageChainStepV11 nextStep : nextSteps)
							recalculateReferencedAttributes(nextStep, processed);

			} else {
				MessageChainStepV11[] nextSteps = step.resultMap.get(step.resultCode);
				if(nextSteps != null)
					for(MessageChainStepV11 nextStep : nextSteps)
						recalculateReferencedAttributes(nextStep, processed);
			}
		}
	}

	//construction methods
	/**
	 * @see com.usatech.app.MessageChain#addStep(java.lang.String)
	 */
	public MessageChainStepV11 addStep(String queueKey) {
		if(queueKey != null && queueKey.length() > MAX_QUEUE_KEY_LENGTH)
			throw new IllegalArgumentException("Queue Key is too long. It must be shorter than " + MAX_QUEUE_KEY_LENGTH + " bytes");
		MessageChainStepV11 step = new MessageChainStepV11(queueKey);
		if(currentStepIndex == 0)
			currentStepIndex = step.getStepIndex();
		return step;
	}

	public MessageChainStepV11 addStep(String queueKey, boolean multicast) {
		MessageChainStepV11 step = addStep(queueKey);
		step.setMulticast(multicast);
		return step;
	}

	public Iterable<MessageChainStep> getAllWaitFors() {
		if(allWaitFors == null)
			allWaitFors = new Iterable<MessageChainStep>() {
				public Iterator<MessageChainStep> iterator() {
					final Iterator<MessageChainStepV11> stepIter = steps.iterator();
					return new PrefetchIterator<MessageChainStep>() {
						protected Iterator<MessageChainStep> waitForIter = null;

						protected boolean fetchNext() {
							while(waitForIter == null || !waitForIter.hasNext()) {
								if(!stepIter.hasNext())
									return false;
								Set<MessageChainStep> waitFors = stepIter.next().getWaitFors();
								waitForIter = waitFors == null ? null : waitFors.iterator();
							}
							nextEntry = waitForIter.next();
							return true;
						}
					};
				}
			};
		return allWaitFors;
	}
	/**
	 * @see com.usatech.app.MessageChain#isComplete()
	 */
	public boolean isComplete() {
		if(resultCode == -1)
			return false;
		MessageChainStep currentStep = getCurrentStep();
		if(currentStep == null)
			return false;
		MessageChainStep[] nextSteps = currentStep.getNextSteps(resultCode);
		if(nextSteps == null || nextSteps.length == 0)
			return false;
		return true;
	}

	/**
	 * @see com.usatech.app.MessageChain#clear()
	 */
	public void clear() {
		currentStepIndex = 0;
		resultCode = -1;
		steps.clear();
		instance++;
		stepChain = null;
	}

	// serialization / deserialization methods
	/**
	 * @see com.usatech.app.MessageChain#read(simple.io.ByteInput)
	 */
	public void read(ByteInput input, boolean encryptionRequired) throws IOException {
		long len;
		if(input == null || (len=input.length()) == 0) throw new IOException("Content not provided");
		if(len < 4) throw new IOException("Invalid content; not long enough");
		byte first = input.readByte();
		//clear current or warn if not new
		clear();
		boolean large = false;
		switch(first) {
			case MESSAGE_CHAIN_VERSION_1:
				if(encryptionRequired)
					throw new IOException("Calling program requires encryption, but message was not encrypted");
				version = 1;
				break;
			case MESSAGE_CHAIN_VERSION_2:
				if(encryptionRequired)
					throw new IOException("Calling program requires encryption, but message was not encrypted");
				version = 2;
				guid = readSmallString(input);
				instance = input.readUnsignedShort();
				break;
			case MESSAGE_CHAIN_VERSION_2_ENCRYPTED_LONG:
				large = true;
			case MESSAGE_CHAIN_VERSION_2_ENCRYPTED:
				version = 2;
				guid = readSmallString(input);
				instance = input.readUnsignedShort();
				String keyAlias = readSmallString(input);
				String cipher = readSmallString(input);
				byte[] encrypted = (large ? readLargeByteArray(input) : readMediumByteArray(input));
				String secondaryCipher = readSmallString(input);
				byte[] unencryptedBytes;
				try {
					if(secondaryCipher != null && secondaryCipher.length() > 0) {
						byte[] encryptedKey = readMediumByteArray(input);
						// first decrypt the key
						byte[] unencryptedKey = CryptUtils.decrypt(cipher, keyAlias, encryptedKey);
						unencryptedBytes = CryptUtils.decrypt(secondaryCipher, new SecretKeySpec(unencryptedKey, StringUtils.substringBefore(secondaryCipher, "/")), encrypted);
					} else {
						unencryptedBytes = CryptUtils.decrypt(cipher, keyAlias, encrypted);
					}
				} catch(GeneralSecurityException e) {
					throw new IOException(e);
				}
				input = new ByteArrayByteInput(unencryptedBytes);
				break;
			default:
				throw new IOException("Data Type " + Integer.toHexString(first) + "h not supported");
		}

		currentStepIndex = input.readUnsignedByte();
		int n = input.readUnsignedByte();
		for(int i = 1; i <= n; i++) {
			MessageChainStepV11 step = getOrCreateStep(i);
			readStep(step, input);
			if(i == currentStepIndex) {
				step.startTime = 0;
				step.endTime = 0;
				step.executor = null;
				step.resultCode = -1;
			}
		}
	}

	public int getStepCount() {
		return steps.size();
	}

	public MessageChainStepV11 getStep(int index) {
		if(index < 1 || index > steps.size())
			return null;
		return steps.get(index - 1); // stepIndex is 1-based;
	}

	protected MessageChainStepV11 getOrCreateStep(int stepIndex) {
		MessageChainStepV11 step = null;
		while(stepIndex > steps.size())
			step = new MessageChainStepV11();
		if(step == null)
			step = steps.get(stepIndex - 1); // stepIndex is 1-based
		return step;
	}

	protected void readStep(MessageChainStepV11 step, ByteInput input) throws IOException {
		step.queueKey = readMediumString(input);
		step.bitmap.setByte(0, input.readByte());
		int n = input.readUnsignedShort();
		for(int i = 0; i < n; i++) {
			String name = readSmallString(input);
			int p = input.readUnsignedByte();
			AttributeValue av;
			switch(p) {
				case REFERENCE_ATTRIBUTE_VALUE_TYPE:
				case ALTERNATING_ATTRIBUTE_VALUE_TYPE:
				case REFERENCE_MAP_VALUE_TYPE:
				case PASS_THRU_REFERENCE_ATTRIBUTE_VALUE_TYPE:
					av = (AttributeValue)readValue(input, p);
					break;
				case TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE:
				case FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE:
					av = new CompressedStringAttributeValue((byte[]) readValue(input, p));
					break;
				case TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE:
				case FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE:
					av = new CompressedByteArrayAttributeValue((byte[]) readValue(input, p));
					break;
				case ONE_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
				case TWO_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
				case FOUR_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
					av = new SecretAttributeValue((String) readValue(input, p));
					break;
				/*case ONE_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_STRING_VALUE_TYPE:
					av = readEncryptedValue(input, 1, true);
					break;
				case TWO_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_STRING_VALUE_TYPE:
					av = readEncryptedValue(input, 2, true);
					break;
				case FOUR_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_STRING_VALUE_TYPE:
					av = readEncryptedValue(input, 4, true);
					break;
				case ONE_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_BYTE_ARRAY_VALUE_TYPE:
					av = readEncryptedValue(input, 1, false);
					break;
				case TWO_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_BYTE_ARRAY_VALUE_TYPE:
					av = readEncryptedValue(input, 2, false);
					break;
				case FOUR_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_BYTE_ARRAY_VALUE_TYPE:
					av = readEncryptedValue(input, 4, false);
					break;*/
				case ONE_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				case TWO_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				case FOUR_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
					av = readArrayAttributeValue(input, p);
					break;
				default:
					Object value = readValue(input, p);
					av = value instanceof AttributeValue ? (AttributeValue) value : new LiteralAttributeValue(value);
			}
			step.avpMap.put(name, av);
		}
		step.resultStepCount = input.readUnsignedByte();
		for(int i = 0; i < step.resultStepCount; i++) {
			int rc = input.readUnsignedByte();
			int stepIndex = input.readUnsignedByte();
			MessageChainStepV11 resultStep = getOrCreateStep(stepIndex);
			MessageChainStepV11[] resultArray = step.resultMap.get(rc);
			if(resultArray == null)
				resultArray = new MessageChainStepV11[] { resultStep };
			else {
				MessageChainStepV11[] tmp = new MessageChainStepV11[resultArray.length + 1];
				System.arraycopy(resultArray, 0, tmp, 0, resultArray.length);
				tmp[resultArray.length] = resultStep;
				resultArray = tmp;
			}
			step.resultMap.put(rc, resultArray);
		}
		n = input.readUnsignedShort();
		for(int i = 0; i < n; i++) {
			String name = readSmallString(input);
			int p = input.readUnsignedByte();
			Object value;
			switch(p) {
				case REFERENCE_ATTRIBUTE_VALUE_TYPE:
				case ALTERNATING_ATTRIBUTE_VALUE_TYPE:
				case REFERENCE_MAP_VALUE_TYPE:
					value = ((AttributeValue)readValue(input, p)).getValue();
					break;
				case TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE:
				case FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE:
					value = new CompressedStringAttributeValue((byte[]) readValue(input, p)).getValue();
					break;
				case TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE:
				case FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE:
					value = new CompressedByteArrayAttributeValue((byte[]) readValue(input, p)).getValue();
					break;
				case ONE_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
				case TWO_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
				case FOUR_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
					value = new MaskedString((String) readValue(input, p));
					break;
				case ONE_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				case TWO_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				case FOUR_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
					value = readArrayAttributeValue(input, p).getValue();
					break;
				default:
					value = readValue(input, p);
					if(value instanceof AttributeValue)
						value = ((AttributeValue) value).getValue();
			}
			step.resultAttributes.put(name, value);
		}
		switch(version) {
			case 2:
				step.resultCode = input.readShort();
				step.startTime = input.readLong();
				step.endTime = input.readLong();
				step.executor = readSmallString(input);
				break;
		}

		if(step.bitmap.getBit(5))
			step.correlation = input.readLong();
		if(step.bitmap.getBit(4)) {
			int len = input.readUnsignedByte();
			for(int i = 0; i < len; i++)
				step.addWaitFor(getOrCreateStep(input.readUnsignedByte()));
		}
	}

	protected Object readValue(ByteInput input, int valueType) throws IOException {
		switch(valueType) {
			case NULL_ATTRIBUTE_VALUE_TYPE:
				return null;
			case ONE_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE:
			case ONE_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
				return readSmallString(input);
			case TWO_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
				return readMediumString(input);
			case FOUR_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
				return readLargeString(input);
			case REFERENCE_ATTRIBUTE_VALUE_TYPE:
				int stepIndex = input.readUnsignedByte();
				if(stepIndex > 0)
					return new ReferenceAttributeValuePart(getOrCreateStep(stepIndex), readSmallString(input));
				return new ReferenceAttributeValuePart(null, null);
			case REFERENCE_MAP_VALUE_TYPE:
				stepIndex = input.readUnsignedByte();
				if(stepIndex > 0)
					return new ReferenceMapAttributeValue(getOrCreateStep(stepIndex), readSmallString(input));
				return new ReferenceMapAttributeValue(null, null);
			case ALTERNATING_ATTRIBUTE_VALUE_TYPE:
				MixedAttributeValue mav = new MixedAttributeValue();
				int t = input.readUnsignedByte();
				for(int k = 0; k < t; k++) {
					if(k % 2 == 0) {
						mav.addLiteralPart(readSmallString(input));
					} else {
						stepIndex = input.readUnsignedByte();
						if(stepIndex > 0)
							mav.addReferencePart(getOrCreateStep(stepIndex), readSmallString(input));
					}
				}
				return mav;
			case PASS_THRU_REFERENCE_ATTRIBUTE_VALUE_TYPE:
				t = input.readUnsignedByte();
				List<ReferenceAttributeValuePart> referencingValues = new ArrayList<ReferenceAttributeValuePart>(t);
				PassThruReferenceAttributeValue ptrav = new PassThruReferenceAttributeValue(referencingValues);
				for(int k = 0; k < t; k++) {
					stepIndex = input.readUnsignedByte();
					if(stepIndex > 0)
						referencingValues.add(new ReferenceAttributeValuePart(getOrCreateStep(stepIndex), readSmallString(input)));
				}
				return ptrav;
			case ONE_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE:
				return readSmallByteArray(input);
			case TWO_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE:
				return readMediumByteArray(input);
			case FOUR_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE:
				return readLargeByteArray(input);
			case ONE_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE:
				return readSmallIntArray(input);
			case TWO_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE:
				return readMediumIntArray(input);
			case FOUR_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE:
				return readLargeIntArray(input);
			case ONE_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE:
				return readSmallLongArray(input);
			case TWO_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE:
				return readMediumLongArray(input);
			case FOUR_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE:
				return readLargeLongArray(input);
			case BYTE_ATTRIBUTE_VALUE_TYPE:
				return input.readByte();
			case CHAR_ATTRIBUTE_VALUE_TYPE:
				return input.readChar();
			case SHORT_ATTRIBUTE_VALUE_TYPE:
				return input.readShort();
			case INT_ATTRIBUTE_VALUE_TYPE:
				return input.readInt();
			case LONG_ATTRIBUTE_VALUE_TYPE:
				return input.readLong();
			case BOOLEAN_TRUE_ATTRIBUTE_VALUE_TYPE:
				return true;
			case BOOLEAN_FALSE_ATTRIBUTE_VALUE_TYPE:
				return false;
			case ONE_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE:
				return readSmallNode(input);
			case TWO_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE:
				return readMediumNode(input);
			case FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE:
				return readLargeNode(input);
			case ONE_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE:
				return readSmallNodeList(input);
			case TWO_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE:
				return readMediumNodeList(input);
			case FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE:
				return readLargeNodeList(input);
			case ONE_BYTE_LENGTH_ATTRIBUTE_MASKED_VALUE_TYPE:
				return readSmallMask(input);
			case TWO_BYTE_LENGTH_ATTRIBUTE_MASKED_VALUE_TYPE:
				return readMediumMask(input);
			case FOUR_BYTE_LENGTH_ATTRIBUTE_MASKED_VALUE_TYPE:
				return readLargeMask(input);
			case ONE_BYTE_LENGTH_ATTRIBUTE_LIST_TYPE:
				return readMixedArray(input, input.readUnsignedByte());
			case TWO_BYTE_LENGTH_ATTRIBUTE_LIST_TYPE:
				return readMixedArray(input, input.readUnsignedShort());
			case FOUR_BYTE_LENGTH_ATTRIBUTE_LIST_TYPE:
				return readMixedArray(input, (int) input.readUnsignedInt());
			default:
				int pos = input.position();
				String s;
				if(input.isResettable()) {
					input.reset();
					byte[] b = new byte[pos];
					input.readBytes(b, 0, pos);
					s = "Unrecognized Attribute Value Type " + valueType + " from:\n\t" + StringUtils.toHex(b) + "...";
				} else
					s = "Unrecognized Attribute Value Type " + valueType + " at position " + pos;
				throw new IOException(s);
		}
	}/*
	protected AttributeValue readEncryptedValue(ByteInput input, int bytesOfLength, boolean asString) throws IOException {
		int len;
		switch(bytesOfLength) {
			case 1: len = input.readUnsignedByte(); break;
			case 2: len = input.readUnsignedShort(); break;
			case 4: len = (int)input.readUnsignedInt(); break;
			default:
				throw new IllegalArgumentException(bytesOfLength + " bytes of length is not supported");
		}
		byte[] encrypted = new byte[len];
		input.readBytes(encrypted, 0, len);
		String cipherName = readSmallString(input);
		String secondaryCipherName = readSmallString(input);
		String keyAlias = readSmallString(input);
		byte[] encryptedKey = readMediumByteArray(input);
		if(asString)
			return new EncryptedAttributeValue(new EncryptedString(encrypted, encryptedKey, cipherName, secondaryCipherName, keyAlias, ENCODING));
		else
			return new EncryptedAttributeValue(new EncryptedBytes(encrypted, encryptedKey, cipherName, secondaryCipherName, keyAlias));
	}*/

	protected Object readMixedArray(ByteInput input, int length) throws IOException {
		final Set<ReferencingValue> referencingValues = new LinkedHashSet<ReferencingValue>();
		Object[] array = new Object[length];	
		for(int i = 0; i < array.length; i++) {
			array[i] = readArrayValue(input, input.readUnsignedByte(), referencingValues);
		}
		if(!referencingValues.isEmpty())
			return new ReferencingDelegatesAttributeValue(array, referencingValues);
		return array;
	}

	protected void writeChain(ByteOutput output, int stepIndex) throws IOException {
		recalculateReferencedAttributes(stepIndex);
		output.writeByte(stepIndex);
		output.writeByte(steps.size());
		for(MessageChainStepV11 step : steps) {
			step.write(output);
		}
	}

	public int getResultCode() {
		return resultCode;
	}

	protected void writeLiteralValue(ByteOutput output, Object value, boolean maskData) throws IOException {
		if(value == null) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else if(value instanceof String) {
			writeStringValue(output, (String)value);
		} else if(value instanceof MaskedString) {
			String s = ((MaskedString) value).getValue();
			if(maskData)
				writeStringValue(output, value.toString()); //writeMaskedStringValue(output, s == null ? 0 : s.length());
			else
				writeSecretStringValue(output, s);
		} else if(value instanceof Byte) {
			writeByteValue(output, (Byte)value);
		} else if(value instanceof Short) {
			writeShortValue(output, (Short)value);
		} else if(value instanceof Character) {
			writeCharValue(output, (Character)value);
		} else if(value instanceof Integer) {
			writeIntValue(output, (Integer)value);
		} else if(value instanceof Long) {
			writeLongValue(output, (Long)value);
		} else if(value instanceof Date) {
			writeLongValue(output, ((Date)value).getTime());
		} else if(value instanceof byte[]) {
			if(maskData)
				writeMaskedArrayValueLegacy(output, ((byte[]) value).length); // writeMaskedArrayValue(output, ((byte[]) value).length);
			else
				writeByteArrayValue(output, (byte[])value);
		} else if(value instanceof int[]) {
			if(maskData)
				writeMaskedArrayValue(output, ((int[]) value).length);
			else
				writeIntArrayValue(output, (int[])value);
		} else if(value instanceof long[]) {
			if(maskData)
				writeMaskedArrayValue(output, ((long[]) value).length);
			else
				writeLongArrayValue(output, (long[])value);
		} else if(value instanceof ByteBuffer) {
			if(maskData)
				writeMaskedArrayValue(output, ((ByteBuffer) value).remaining());
			else
				writeByteArrayValue(output, (ByteBuffer) value);
		} else if(value instanceof Boolean) {
			writeBooleanValue(output, (Boolean)value);
		/*} else if(value instanceof EncryptedBytes) {
			EncryptedBytes eb = (EncryptedBytes)value;
			try {
				writeEncryptedValue(output, eb, false);
			} catch(GeneralSecurityException e) {
				throw new IOException("Could not encrypt value", e);
			}
		} else if(value instanceof EncryptedString) {
			EncryptedString eb = (EncryptedString)value;
			try {
				writeEncryptedValue(output, eb, true);
			} catch(GeneralSecurityException e) {
				throw new IOException("Could not encrypt value", e);
			}
		*/} else if(value instanceof Node) {
			writeNodeValue(output, (Node)value);
		} else if(value instanceof NodeList) {
			writeNodeListValue(output, (NodeList)value);
		} else if(value instanceof Object[]) {
			writeArrayValue(output, (Object[]) value, maskData);
		} else {
			Number n = ConvertUtils.enumerate(value);
	        if(n != null) {
	        	writeLiteralValue(output, n, maskData);
	        } else if(value instanceof Convertible) {
			try {
				Object innerValue = ((Convertible)value).convert(Object.class);
				if(innerValue != value && (innerValue == null || !innerValue.getClass().equals(value.getClass())))
					writeLiteralValue(output, innerValue, maskData);
				else
					writeStringValue(output, ConvertUtils.getStringSafely(value));
			} catch(ConvertException e) {
				writeStringValue(output, ConvertUtils.getStringSafely(value));
			}
			} else if(value instanceof Getter<?>) {
				Object innerValue = ((Getter<?>)value).get();
				if(innerValue != value && (innerValue == null || !innerValue.getClass().equals(value.getClass())))
					writeLiteralValue(output, innerValue, maskData);
				else
					writeStringValue(output, ConvertUtils.getStringSafely(value));
			} else {
				writeStringValue(output, ConvertUtils.getStringSafely(value));
			}
		}
	}

	protected void writeArrayLength(ByteOutput output, int length) throws IOException {
		if(length <= ONE_BYTE_LENGTH) {
			output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE);
			output.writeByte(length);
		} else if(length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE);
			output.writeShort(length);
		} else if(length <= FOUR_BYTE_LENGTH) {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE);
			output.writeInt(length);
		} else {
			throw new IOException("Length of array is invalid: " + length);
		}
	}

	protected void writeArrayArrayValue(ByteOutput output, Object[][] value, boolean maskData) throws IOException {
		int maxLength = 0;
		for(int i = 0; i < value.length; i++)
			if(value[i] != null && value[i].length > maxLength)
				maxLength = value[i].length;
		writeArrayLength(output, maxLength);
		for(int i = 0; i < value.length; i++)
			writeArrayValue(output, value[i], maskData);
	}

	protected static abstract class StringConverter<E> implements WriteSupport<E> {
		public abstract String convert(E value);

		public int getLength(E value) {
			if(value == null)
				return 0;
			String s = convert(value);
			if(s == null)
				return 0;
			return s.getBytes(ENCODING).length;
		}

		@Override
		public byte getValueType(int lengthBytes) {
			switch(lengthBytes) {
				case 1:
					return ONE_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE;
				case 2:
					return TWO_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE;
				case 4:
					return FOUR_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE;
			}
			return 0;
		}

		public void writeValue(ByteOutput output, E value) throws IOException {
			String s = convert(value);
			if(s != null) {
				byte[] bytes = s.getBytes(ENCODING);
				output.write(bytes);
			}
		}
	}

	protected static final StringConverter<String> passThruStringConverter = new StringConverter<String>() {
		@Override
		public String convert(String value) {
			return value;
		}
	};
	protected static final StringConverter<Object> toStringStringConverter = new StringConverter<Object>() {
		@Override
		public String convert(Object value) {
			return value == null ? null : value.toString();
		}
	};
	protected static final StringConverter<Convertible> convertibleStringConverter = new StringConverter<Convertible>() {
		@Override
		public String convert(Convertible value) {
			try {
				return value == null ? null : value.convert(String.class);
			} catch(ConvertException e) {
				return value.toString();
			}
		}
	};
	protected static final StringConverter<Object> convertingStringConverter = new StringConverter<Object>() {
		@Override
		public String convert(Object value) {
			return ConvertUtils.getStringSafely(value);
		}
	};
	protected static final StringConverter<MaskedString> maskedStringStringConverter = new StringConverter<MaskedString>() {
		@Override
		public String convert(MaskedString value) {
			return value == null ? null : value.getValue();
		}
	};
	protected static final StringConverter<Getter<?>> getterStringConverter = new StringConverter<Getter<?>>() {
		@Override
		public String convert(Getter<?> value) {
			return value == null ? null : ConvertUtils.getStringSafely(value.get());
		}
	};

	protected static interface WriteSupport<E> {
		public int getLength(E value);

		public byte getValueType(int lengthBytes);

		public void writeValue(ByteOutput output, E value) throws IOException;
	}

	protected class LiteralAttributeValue implements AttributeValue {
		protected final Object value;

		public LiteralAttributeValue(Object value) {
			this.value = value;
		}

		public Object getValue() {
			return value;
		}

		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			writeLiteralValue(output, value, maskData);
		}
	}

	protected class ReferencingDelegatesAttributeValue implements ReferencingValue {
		protected final Set<ReferencingValue> referencingValues;
		protected final Object[] values;
		protected Object[] convertedValues;

		public ReferencingDelegatesAttributeValue(Object[] values, Set<ReferencingValue> referencingValues) {
			this.values = values;
			this.referencingValues = referencingValues;
		}

		protected Object[] convertValue(Object[] value) {
			Object[] convertedValue = new Object[value.length];
			for(int i = 0; i < value.length; i++)
				convertedValue[i] = convertValue(value[i]);
			return convertedValue;
		}

		protected Object convertValue(Object value) {
			if(value instanceof AttributeValue)
				return ((AttributeValue) value).getValue();
			if(value instanceof Object[])
				return convertValue((Object[]) value);
			return value;
		}

		@Override
		public Object getValue() {
			// must recurse and get value of sub-attributevalues
			if(convertedValues == null)
				convertedValues = convertValue(values);
			return convertedValues;
		}

		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			writeLiteralValue(output, values, maskData);
		}

		@Override
		public void addReferencedAttributes() {
			for(ReferencingValue rv : referencingValues)
				rv.addReferencedAttributes();
		}

		public void addDelegate(ReferencingValue rv) {
			referencingValues.add(rv);
		}
	}
	protected class CompressedByteArrayAttributeValue implements AttributeValue {
		protected final byte[] value;
		public CompressedByteArrayAttributeValue(byte[] value){
			this.value = value;
		}
		public byte[] getValue(){
			try {
				return IOUtils.decompressByteArray(value);
			} catch(DataFormatException e){
				log.warn(e.getMessage(), e);
			} catch(IOException e){
				log.warn(e.getMessage(), e);
			}
			return null;
		}
		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			if(!maskData || value == null)
				writeCompressedByteArrayValue(output, value);
			else
				writeStringValue(output, value.toString());
		}
	}
	protected class CompressedStringAttributeValue implements AttributeValue {
		protected final byte[] value;
		public CompressedStringAttributeValue(byte[] value){
			this.value = value;
		}
		public String getValue(){
			try{
				byte[] temp=IOUtils.decompressByteArray(value);
				return new String(temp, ENCODING);
			} catch(DataFormatException e){
				log.warn(e.getMessage(), e);
			} catch(IOException e){
				log.warn(e.getMessage(), e);
			}
			return null;
		}
		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			if(!maskData || value == null)
				writeCompressedStringValue(output, value);
			else
				writeStringValue(output, value.toString());
		}
	}
	protected class SecretAttributeValue implements AttributeValue {
		protected final MaskedString value;
		public SecretAttributeValue(String secret) {
			this.value = new MaskedString(secret);
		}
		public MaskedString getValue() {
			return value;
		}
		public void writeValue(ByteOutput output, boolean maskData) throws IOException {
			if(!maskData)
				writeSecretStringValue(output, value.getValue());
			else
				writeStringValue(output, value.toString());
		}
	}
	protected static final WriteSupport<byte[]> byteArraySupport = new WriteSupport<byte[]>() {
		@Override
		public int getLength(byte[] value) {
			return value == null ? 0 : value.length;
		}

		@Override
		public byte getValueType(int lengthBytes) {
			switch(lengthBytes) {
				case 1:
					return ONE_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE;
				case 2:
					return TWO_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE;
				case 4:
					return FOUR_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE;
			}
			return 0;
		}

		@Override
		public void writeValue(ByteOutput output, byte[] value) throws IOException {
			output.write(value);
		}
	};
	protected static final WriteSupport<int[]> intArraySupport = new WriteSupport<int[]>() {
		@Override
		public int getLength(int[] value) {
			return value == null ? 0 : value.length;
		}

		@Override
		public byte getValueType(int lengthBytes) {
			switch(lengthBytes) {
				case 1:
					return ONE_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE;
				case 2:
					return TWO_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE;
				case 4:
					return FOUR_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE;
			}
			return 0;
		}

		@Override
		public void writeValue(ByteOutput output, int[] value) throws IOException {
			for(int i = 0; i < value.length; i++)
					output.writeInt(value[i]);
		}
	};
	protected static final WriteSupport<long[]> longArraySupport = new WriteSupport<long[]>() {
		@Override
		public int getLength(long[] value) {
			return value == null ? 0 : value.length;
		}

		@Override
		public byte getValueType(int lengthBytes) {
			switch(lengthBytes) {
				case 1:
					return ONE_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE;
				case 2:
					return TWO_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE;
				case 4:
					return FOUR_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE;
			}
			return 0;
		}

		@Override
		public void writeValue(ByteOutput output, long[] value) throws IOException {
			for(int i = 0; i < value.length; i++)
				output.writeLong(value[i]);
		}
	};
	protected static final WriteSupport<ByteBuffer> byteBufferArraySupport = new WriteSupport<ByteBuffer>() {
		@Override
		public int getLength(ByteBuffer value) {
			return value == null ? 0 : value.remaining();
		}

		@Override
		public byte getValueType(int lengthBytes) {
			switch(lengthBytes) {
				case 1:
					return ONE_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE;
				case 2:
					return TWO_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE;
				case 4:
					return FOUR_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE;
			}
			return 0;
		}

		@Override
		public void writeValue(ByteOutput output, ByteBuffer value) throws IOException {
			output.write(value);
		}
	};
	protected static final WriteSupport<Node> nodeSupport = new StringConverter<Node>() {
		@Override
		public String convert(Node value) {
			return ConvertUtils.getStringSafely(value);
		}

		@Override
		public byte getValueType(int lengthBytes) {
			switch(lengthBytes) {
				case 1:
					return ONE_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE;
				case 2:
					return TWO_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE;
				case 4:
					return FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE;
			}
			return 0;
		}
	};
	protected static final WriteSupport<NodeList> nodeListSupport = new StringConverter<NodeList>() {
		@Override
		public String convert(NodeList value) {
			return ConvertUtils.getStringSafely(value);
		}

		@Override
		public byte getValueType(int lengthBytes) {
			switch(lengthBytes) {
				case 1:
					return ONE_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE;
				case 2:
					return TWO_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE;
				case 4:
					return FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE;
			}
			return 0;
		}
	};
	protected <E> void writeGenericArrayValue(ByteOutput output, E[] value, WriteSupport<E> support, boolean maskData) throws IOException {
		int maxLength = 0;
		int[] lengths = new int[value.length];
		for(int i = 0; i < value.length; i++)
			if((lengths[i] = support.getLength(value[i])) > maxLength)
				maxLength = lengths[i];
		if(maxLength == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else {
			int lengthBytes;
			if(maxLength <= ONE_BYTE_LENGTH)
				lengthBytes = 1;
			else if(maxLength <= TWO_BYTE_LENGTH)
				lengthBytes = 2;
			else if(maxLength <= FOUR_BYTE_LENGTH)
				lengthBytes = 4;
			else
				throw new IOException("Length of value is invalid: " + maxLength);
			output.writeByte(support.getValueType(lengthBytes));
			for(int i = 0; i < value.length; i++) {
				switch(lengthBytes) {
					case 1:
						output.writeByte(lengths[i]);
						break;
					case 2:
						output.writeShort(lengths[i]);
						break;
					case 4:
						output.writeInt(lengths[i]);
						break;
				}
				if(lengths[i] == 0)
					continue;
				if(maskData)
					for(int k = 0; k < lengths[i]; k++)
						output.writeByte('*');
				else
					support.writeValue(output, value[i]);
			}
		}
	}

	protected void writeMixedArray(ByteOutput output, Object[] value, boolean maskData) throws IOException {
		if(value.length <= ONE_BYTE_LENGTH) {
			output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_LIST_TYPE);
			output.writeByte(value.length);
		} else if(value.length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_LIST_TYPE);
			output.writeShort(value.length);
		} else {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_LIST_TYPE);
			output.writeInt(value.length);
		}
		for(int i = 0; i < value.length; i++) {
			writeLiteralValue(output, value[i], maskData);
		}
	}

	protected void writeByteArrayValue(ByteOutput output, Byte[] value) throws IOException {
		output.writeByte(BYTE_ATTRIBUTE_VALUE_TYPE);
		for(int i = 0; i < value.length; i++) {
			if(value[i] == null) {
				output.writeByte(Byte.MIN_VALUE);
				output.writeByte(0);
			} else if(value[i] == Byte.MIN_VALUE) {
				output.writeByte(Byte.MIN_VALUE);
				output.writeByte(Byte.MIN_VALUE);
			} else
				output.writeByte(value[i]);
		}
	}

	protected void writeShortArrayValue(ByteOutput output, Short[] value) throws IOException {
		output.writeByte(SHORT_ATTRIBUTE_VALUE_TYPE);
		for(int i = 0; i < value.length; i++) {
			if(value[i] == null) {
				output.writeShort(Short.MIN_VALUE);
				output.writeByte(0);
			} else if(value[i] == Short.MIN_VALUE) {
				output.writeShort(Short.MIN_VALUE);
				output.writeByte(Byte.MIN_VALUE);
			} else
				output.writeShort(value[i]);
		}
	}

	protected void writeIntArrayValue(ByteOutput output, Integer[] value) throws IOException {
		output.writeByte(INT_ATTRIBUTE_VALUE_TYPE);
		for(int i = 0; i < value.length; i++) {
			if(value[i] == null) {
				output.writeInt(Integer.MIN_VALUE);
				output.writeByte(0);
			} else if(value[i] == Integer.MIN_VALUE) {
				output.writeInt(Integer.MIN_VALUE);
				output.writeByte(Byte.MIN_VALUE);
			} else
				output.writeInt(value[i]);
		}
	}

	protected void writeLongArrayValue(ByteOutput output, Long[] value) throws IOException {
		output.writeByte(LONG_ATTRIBUTE_VALUE_TYPE);
		for(int i = 0; i < value.length; i++) {
			if(value[i] == null) {
				output.writeLong(Long.MIN_VALUE);
				output.writeByte(0);
			} else if(value[i] == Long.MIN_VALUE) {
				output.writeLong(Long.MIN_VALUE);
				output.writeByte(Byte.MIN_VALUE);
			} else
				output.writeLong(value[i]);
		}
	}

	protected void writeCharArrayValue(ByteOutput output, Character[] value) throws IOException {
		output.writeByte(CHAR_ATTRIBUTE_VALUE_TYPE);
		for(int i = 0; i < value.length; i++) {
			if(value[i] == null) {
				output.writeChar(Character.MIN_VALUE);
				output.writeByte(0);
			} else if(value[i] == Character.MIN_VALUE) {
				output.writeChar(Character.MIN_VALUE);
				output.writeByte(Byte.MIN_VALUE);
			} else
				output.writeChar(value[i]);
		}
	}

	protected void writeDateArrayValue(ByteOutput output, Date[] value) throws IOException {
		output.writeByte(LONG_ATTRIBUTE_VALUE_TYPE);
		for(int i = 0; i < value.length; i++) {
			long time;
			if(value[i] == null) {
				output.writeLong(Long.MIN_VALUE);
				output.writeByte(0);
			} else if((time = value[i].getTime()) == Long.MIN_VALUE) {
				output.writeLong(Long.MIN_VALUE);
				output.writeByte(Byte.MIN_VALUE);
			} else
				output.writeLong(time);
		}
	}

	protected void writeBooleanArrayValue(ByteOutput output, Boolean[] value) throws IOException {
		output.writeByte(BOOLEAN_TRUE_ATTRIBUTE_VALUE_TYPE);
		for(int i = 0; i < value.length; i++) {
			if(value[i] == null)
				output.writeByte(Byte.MIN_VALUE);
			else if(value[i])
				output.writeByte(1);
			else
				output.writeByte(0);
		}
	}

	protected void writeArrayValue(ByteOutput output, Object[] value, boolean maskData) throws IOException {
		if(value instanceof String[]) {
			writeArrayLength(output, value.length);
			writeGenericArrayValue(output, (String[]) value, passThruStringConverter, false);
		} else if(value instanceof MaskedString[]) {
			writeArrayLength(output, value.length);
			if(maskData)
				writeGenericArrayValue(output, value, toStringStringConverter, false);
			else
				writeGenericArrayValue(output, (MaskedString[]) value, maskedStringStringConverter, false);
		} else if(value instanceof Byte[]) {
			writeArrayLength(output, value.length);
			writeByteArrayValue(output, (Byte[])value);
		} else if(value instanceof Short[]) {
			writeArrayLength(output, value.length);
			writeShortArrayValue(output, (Short[])value);
		} else if(value instanceof Character[]) {
			writeArrayLength(output, value.length);
			writeCharArrayValue(output, (Character[])value);
		} else if(value instanceof Integer[]) {
			writeArrayLength(output, value.length);
			writeIntArrayValue(output, (Integer[])value);
		} else if(value instanceof Long[]) {
			writeArrayLength(output, value.length);
			writeLongArrayValue(output, (Long[])value);
		} else if(value instanceof Date[]) {
			writeArrayLength(output, value.length);
			writeDateArrayValue(output, (Date[])value);
		} else if(value instanceof byte[][]) {
			writeArrayLength(output, value.length);
			writeGenericArrayValue(output, (byte[][]) value, byteArraySupport, maskData);
		} else if(value instanceof int[][]) {
			writeArrayLength(output, value.length);
			writeGenericArrayValue(output, (int[][]) value, intArraySupport, maskData);
		} else if(value instanceof long[][]) {
			writeArrayLength(output, value.length);
			writeGenericArrayValue(output, (long[][]) value, longArraySupport, maskData);
		} else if(value instanceof ByteBuffer[]) {
			writeArrayLength(output, value.length);
			writeGenericArrayValue(output, (ByteBuffer[]) value, byteBufferArraySupport, maskData);
		} else if(value instanceof Boolean[]) {
			writeArrayLength(output, value.length);
			writeBooleanArrayValue(output, (Boolean[])value);
		} else if(value instanceof Node[]) {
			writeArrayLength(output, value.length);
			writeGenericArrayValue(output, (Node[]) value, nodeSupport, false);
		} else if(value instanceof NodeList[]) {
			writeArrayLength(output, value.length);
			writeGenericArrayValue(output, (NodeList[]) value, nodeListSupport, false);
		} else if(value instanceof Object[][]) {
			writeArrayLength(output, value.length);
			writeArrayArrayValue(output, (Object[][])value, maskData);	
		} else {
			Enumerator<? super Object> en = (Enumerator<? super Object>) ConvertUtils.lookupEnumerator(value.getClass().getComponentType());
			if(en != null && en.getTargetClass() != null && !value.getClass().getComponentType().equals(en.getTargetClass())) {
				Object[] converted = CollectionUtils.createArray(en.getTargetClass(), value.length); 
				for(int i = 0; i < value.length; i++)
					converted[i] = en.enumerate(value[i]);
				writeArrayValue(output, converted, maskData);
			} else {
				writeMixedArray(output, value, maskData);
			}
		}
	}

	protected void writeMaskedArrayValueLegacy(ByteOutput output, int length) throws IOException {
		if(length <= ONE_BYTE_LENGTH) {
			output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE);
			output.writeByte(length);
			for(int i = 0; i < length; i++)
				output.writeByte('*');
		} else if(length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE);
			output.writeShort(length);
			for(int i = 0; i < length; i++)
				output.writeByte('*');
		} else if(length <= FOUR_BYTE_LENGTH) {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE);
			output.writeInt(length);
			for(int i = 0; i < length; i++)
				output.writeByte('*');
		} else {
			throw new IOException("Length of value is invalid: " + length);
		}
	}

	protected void writeMaskedArrayValue(ByteOutput output, int length) throws IOException {
		writeMaskedStringValue(output, length);
	}

	protected void writeMaskedStringValue(ByteOutput output, int length) throws IOException {
		if(length <= ONE_BYTE_LENGTH) {
			output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_MASKED_VALUE_TYPE);
			output.writeByte(length);
		} else if(length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_MASKED_VALUE_TYPE);
			output.writeShort(length);
		} else if(length <= FOUR_BYTE_LENGTH) {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_MASKED_VALUE_TYPE);
			output.writeInt(length);
		} else {
			throw new IOException("Length of value is invalid: " + length);
		}
	}

	protected void writeStringValue(ByteOutput output, String value) throws IOException {
		if(value == null || value.length() == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else {
			byte[] bytes = value.getBytes(ENCODING);
			if(bytes.length <= ONE_BYTE_LENGTH) {
				output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE);
				output.writeByte(bytes.length);
				output.write(bytes);
			} else if(bytes.length <= TWO_BYTE_LENGTH) {
				output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE);
				output.writeShort(bytes.length);
				output.write(bytes);
			} else if(bytes.length <= FOUR_BYTE_LENGTH) {
				output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE);
				output.writeInt(bytes.length);
				output.write(bytes);
			} else {
				throw new IOException("Length of value is invalid: " + bytes.length);
			}
		}
	}

	protected void writeSecretStringValue(ByteOutput output, String value) throws IOException {
		if(value == null || value.length() == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else {
			byte[] bytes = value.getBytes(ENCODING);
			if(bytes.length <= ONE_BYTE_LENGTH) {
				output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE);
				output.writeByte(bytes.length);
				output.write(bytes);
			} else if(bytes.length <= TWO_BYTE_LENGTH) {
				output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE);
				output.writeShort(bytes.length);
				output.write(bytes);
			} else if(bytes.length <= FOUR_BYTE_LENGTH) {
				output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE);
				output.writeInt(bytes.length);
				output.write(bytes);
			} else {
				throw new IOException("Length of value is invalid: " + bytes.length);
			}
		}
	}

	protected void writeNodeValue(ByteOutput output, Node value) throws IOException {
		if(value == null) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else {
			String xml;
			try {
				xml = ConvertUtils.getString(value, true);
			} catch(ConvertException e) {
				throw new IOException(e);
			}
			byte[] bytes = xml.getBytes(ENCODING);
			if(bytes.length <= ONE_BYTE_LENGTH) {
				output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE);
				output.writeByte(bytes.length);
				output.write(bytes);
			} else if(bytes.length <= TWO_BYTE_LENGTH) {
				output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE);
				output.writeShort(bytes.length);
				output.write(bytes);
			} else if(bytes.length <= FOUR_BYTE_LENGTH) {
				output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE);
				output.writeInt(bytes.length);
				output.write(bytes);
			} else {
				throw new IOException("Length of value is invalid: " + bytes.length);
			}
		}
	}
	protected void writeNodeListValue(ByteOutput output, NodeList value) throws IOException {
		if(value == null || value.getLength() == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else {
			String xml;
			try {
				xml = ConvertUtils.getString(value, true);
			} catch(ConvertException e) {
				throw new IOException(e);
			}
			byte[] bytes = xml.getBytes(ENCODING);
			if(bytes.length <= ONE_BYTE_LENGTH) {
				output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE);
				output.writeByte(bytes.length);
				output.write(bytes);
			} else if(bytes.length <= TWO_BYTE_LENGTH) {
				output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE);
				output.writeShort(bytes.length);
				output.write(bytes);
			} else if(bytes.length <= FOUR_BYTE_LENGTH) {
				output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE);
				output.writeInt(bytes.length);
				output.write(bytes);
			} else {
				throw new IOException("Length of value is invalid: " + bytes.length);
			}
		}
	}
	/**
	 * @param output
	 * @param cipherName
	 * @param keyAlias
	 * @param unencrypted
	 * @param b
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
/*	protected void writeEncryptedValue(ByteOutput output, AbstractEncryptedObject<?> encryptedObject, boolean asString) throws IOException, GeneralSecurityException {
		byte[] encrypted = encryptedObject.getEncrypted();
		if(encrypted == null || encrypted.length == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else {
			if(encrypted.length <= ONE_BYTE_LENGTH) {
				output.writeByte(asString ? ONE_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_STRING_VALUE_TYPE : ONE_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_BYTE_ARRAY_VALUE_TYPE);
				output.writeByte(encrypted.length);
				output.write(encrypted);
			} else if(encrypted.length <= TWO_BYTE_LENGTH) {
				output.writeByte(asString ? TWO_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_STRING_VALUE_TYPE : TWO_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_BYTE_ARRAY_VALUE_TYPE);
				output.writeShort(encrypted.length);
				output.write(encrypted);
			} else if(encrypted.length <= FOUR_BYTE_LENGTH) {
				output.writeByte(asString ? FOUR_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_STRING_VALUE_TYPE : FOUR_BYTE_LENGTH_ATTRIBUTE_ENCRYPTED_BYTE_ARRAY_VALUE_TYPE);
				output.writeInt(encrypted.length);
				output.write(encrypted);
			} else {
				throw new IOException("Length of value is invalid: " + encrypted.length);
			}
			writeSmallString(output, encryptedObject.getCipherName());
			writeSmallString(output, encryptedObject.getSecondaryCipherName());
			writeSmallString(output, encryptedObject.getKeyAlias());
			writeMediumByteArray(output, encryptedObject.getEncryptedKey());
		}
	}
*/
	protected void writeByteArrayValue(ByteOutput output, byte[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else if(value.length <= ONE_BYTE_LENGTH) {
			output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE);
			output.writeByte(value.length);
			output.write(value);
		} else if(value.length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE);
			output.writeShort(value.length);
			output.write(value);
		} else if(value.length <= FOUR_BYTE_LENGTH) {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE);
			output.writeInt(value.length);
			output.write(value);
		} else {
			throw new IOException("Length of value is invalid: " + value.length);
		}
	}

	protected void writeByteArrayValue(ByteOutput output, ByteBuffer value) throws IOException {
		int length;
		if(value == null || (length = value.remaining()) == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else if(length <= ONE_BYTE_LENGTH) {
			output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE);
			output.writeByte(length);
			int pos = value.position();
			output.write(value);
			value.position(pos);
		} else if(length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE);
			output.writeShort(length);
			int pos = value.position();
			output.write(value);
			value.position(pos);
		} else if(length <= FOUR_BYTE_LENGTH) {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE);
			output.writeInt(length);
			int pos = value.position();
			output.write(value);
			value.position(pos);
		} else {
			throw new IOException("Length of value is invalid: " + length);
		}
	}
	protected void writeIntArrayValue(ByteOutput output, int[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else if(value.length <= ONE_BYTE_LENGTH) {
			output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE);
			output.writeByte(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeInt(value[i]);
		} else if(value.length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE);
			output.writeShort(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeInt(value[i]);
		} else if(value.length <= FOUR_BYTE_LENGTH) {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE);
			output.writeInt(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeInt(value[i]);
		} else {
			throw new IOException("Length of value is invalid: " + value.length);
		}
	}
	protected void writeLongArrayValue(ByteOutput output, long[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else if(value.length <= ONE_BYTE_LENGTH) {
			output.writeByte(ONE_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE);
			output.writeByte(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeLong(value[i]);
		} else if(value.length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE);
			output.writeShort(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeLong(value[i]);
		} else if(value.length <= FOUR_BYTE_LENGTH) {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE);
			output.writeInt(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeLong(value[i]);
		} else {
			throw new IOException("Length of value is invalid: " + value.length);
		}
	}
	protected void writeCompressedStringValue(ByteOutput output, byte[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else if(value.length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE);
			output.writeShort(value.length);
			output.write(value);
		} else if(value.length <= FOUR_BYTE_LENGTH) {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE);
			output.writeInt(value.length);
			output.write(value);
		} else {
			throw new IOException("Length of value is invalid: " + value.length);
		}
	}
	protected void writeCompressedByteArrayValue(ByteOutput output, byte[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeByte(NULL_ATTRIBUTE_VALUE_TYPE);
		} else if(value.length <= TWO_BYTE_LENGTH) {
			output.writeByte(TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE);
			output.writeShort(value.length);
			output.write(value);
		} else if(value.length <= FOUR_BYTE_LENGTH) {
			output.writeByte(FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE);
			output.writeInt(value.length);
			output.write(value);
		} else {
			throw new IOException("Length of value is invalid: " + value.length);
		}
	}

	protected void writeBooleanValue(ByteOutput output, boolean value) throws IOException {
		output.writeByte(value ? BOOLEAN_TRUE_ATTRIBUTE_VALUE_TYPE : BOOLEAN_FALSE_ATTRIBUTE_VALUE_TYPE);
	}
	protected void writeByteValue(ByteOutput output, byte value) throws IOException {
		output.writeByte(BYTE_ATTRIBUTE_VALUE_TYPE);
		output.writeByte(value);
	}
	protected void writeCharValue(ByteOutput output, char value) throws IOException {
		output.writeByte(CHAR_ATTRIBUTE_VALUE_TYPE);
		output.writeChar(value);
	}
	protected void writeShortValue(ByteOutput output, short value) throws IOException {
		output.writeByte(SHORT_ATTRIBUTE_VALUE_TYPE);
		output.writeShort(value);
	}
	protected void writeIntValue(ByteOutput output, int value) throws IOException {
		output.writeByte(INT_ATTRIBUTE_VALUE_TYPE);
		output.writeInt(value);
	}
	protected void writeLongValue(ByteOutput output, long value) throws IOException {
		output.writeByte(LONG_ATTRIBUTE_VALUE_TYPE);
		output.writeLong(value);
	}
	protected void writeSmallString(ByteOutput output, String value) throws IOException {
		if(value == null || value.length() == 0) {
			output.writeByte(0);
		} else {
			byte[] bytes = value.getBytes(ENCODING);
			if(bytes.length > ONE_BYTE_LENGTH)
				throw new IOException("Length of string byte array is " + bytes.length + "; max supported is " + ONE_BYTE_LENGTH);
			output.writeByte(bytes.length);
			output.write(bytes);
		}
	}
	protected void writeSmallByteArray(ByteOutput output, byte[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeByte(0);
		} else if(value.length > ONE_BYTE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + ONE_BYTE_LENGTH);
		} else {
			output.writeByte(value.length);
			output.write(value);
		}
	}
	protected void writeSmallIntArray(ByteOutput output, int[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeByte(0);
		} else if(value.length > ONE_BYTE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + ONE_BYTE_LENGTH);
		} else {
			output.writeByte(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeInt(value[i]);
		}
	}
	protected void writeSmallLongArray(ByteOutput output, long[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeByte(0);
		} else if(value.length > ONE_BYTE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + ONE_BYTE_LENGTH);
		} else {
			output.writeByte(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeLong(value[i]);
		}
	}
	protected void writeMediumString(ByteOutput output, String value) throws IOException {
		if(value == null || value.length() == 0) {
			output.writeShort(0);
		} else {
			byte[] bytes = value.getBytes(ENCODING);
			if(bytes.length > TWO_BYTE_LENGTH)
				throw new IOException("Length of string byte array is " + bytes.length + "; max supported is " + TWO_BYTE_LENGTH);
			output.writeShort(bytes.length);
			output.write(bytes);
		}
	}
	protected void writeMediumByteArray(ByteOutput output, byte[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeShort(0);
		} else if(value.length > TWO_BYTE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + TWO_BYTE_LENGTH);
		} else {
			output.writeShort(value.length);
			output.write(value);
		}
	}
	protected void writeMediumIntArray(ByteOutput output, int[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeShort(0);
		} else if(value.length > TWO_BYTE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + TWO_BYTE_LENGTH);
		} else {
			output.writeShort(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeInt(value[i]);
		}
	}
	protected void writeMediumLongArray(ByteOutput output, long[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeShort(0);
		} else if(value.length > TWO_BYTE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + TWO_BYTE_LENGTH);
		} else {
			output.writeShort(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeLong(value[i]);
		}
	}
	protected void writeLargeString(ByteOutput output, String value) throws IOException {
		if(value == null || value.length() == 0) {
			output.writeInt(0);
		} else {
			byte[] bytes = value.getBytes(ENCODING);
			if(bytes.length > FOUR_BYTE_LENGTH)
				throw new IOException("Length of string byte array is " + bytes.length + "; max supported is " + FOUR_BYTE_LENGTH);
			output.writeInt(bytes.length);
			output.write(bytes);
		}
	}
	protected void writeLargeByteArray(ByteOutput output, byte[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeInt(0);
		} else if(value.length > FOUR_BYTE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + FOUR_BYTE_LENGTH);
		} else {
			output.writeInt(value.length);
			output.write(value);
		}
	}
	protected void writeLargeIntArray(ByteOutput output, int[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeInt(0);
		} else if(value.length > FOUR_BYTE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + FOUR_BYTE_LENGTH);
		} else {
			output.writeInt(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeInt(value[i]);
		}
	}
	protected void writeLargeLongArray(ByteOutput output, long[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeInt(0);
		} else if(value.length > FOUR_BYTE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + FOUR_BYTE_LENGTH);
		} else {
			output.writeInt(value.length);
			for(int i = 0; i < value.length; i++)
				output.writeLong(value[i]);
		}
	}
	protected String readSmallString(ByteInput input) throws UnsupportedEncodingException, IOException {
		byte[] bytes = readSmallByteArray(input);
		return bytes.length == 0 ? null : new String(bytes, ENCODING);
	}
	protected byte[] readSmallByteArray(ByteInput input) throws IOException {
		int len = input.readUnsignedByte();
		byte[] bytes = new byte[len];
		input.readBytes(bytes, 0, len);
		return bytes;
	}
	protected int[] readSmallIntArray(ByteInput input) throws IOException {
		int len = input.readUnsignedByte();
		int[] array = new int[len];
		for(int i = 0; i < len; i++)
			array[i] = input.readInt();
		return array;
	}
	protected long[] readSmallLongArray(ByteInput input) throws IOException {
		int len = input.readUnsignedByte();
		long[] array = new long[len];
		for(int i = 0; i < len; i++)
			array[i] = input.readLong();
		return array;
	}
	protected String readSmallMask(ByteInput input) throws UnsupportedEncodingException, IOException {
		int len = input.readUnsignedByte();
		return getMaskedString(len);
	}

	protected String readMediumString(ByteInput input) throws UnsupportedEncodingException, IOException {
		byte[] bytes = readMediumByteArray(input);
		return bytes.length == 0 ? null : new String(bytes, ENCODING);
	}
	protected byte[] readMediumByteArray(ByteInput input) throws IOException {
		int len = input.readUnsignedShort();
		byte[] bytes = new byte[len];
		input.readBytes(bytes, 0, len);
		return bytes;
	}
	protected int[] readMediumIntArray(ByteInput input) throws IOException {
		int len = input.readUnsignedShort();
		int[] array = new int[len];
		for(int i = 0; i < len; i++)
			array[i] = input.readInt();
		return array;
	}
	protected long[] readMediumLongArray(ByteInput input) throws IOException {
		int len = input.readUnsignedShort();
		long[] array = new long[len];
		for(int i = 0; i < len; i++)
			array[i] = input.readLong();
		return array;
	}

	protected String readMediumMask(ByteInput input) throws UnsupportedEncodingException, IOException {
		int len = input.readUnsignedShort();
		return getMaskedString(len);
	}

	protected String readLargeString(ByteInput input) throws UnsupportedEncodingException, IOException {
		byte[] bytes = readLargeByteArray(input);
		return bytes.length == 0 ? null : new String(bytes, ENCODING);
	}
	protected byte[] readLargeByteArray(ByteInput input) throws IOException {
		int len = (int)input.readUnsignedInt();
		byte[] bytes = new byte[len];
		input.readBytes(bytes, 0, len);
		return bytes;
	}
	protected int[] readLargeIntArray(ByteInput input) throws IOException {
		int len = (int)input.readUnsignedInt();
		int[] array = new int[len];
		for(int i = 0; i < len; i++)
			array[i] = input.readInt();
		return array;
	}
	protected long[] readLargeLongArray(ByteInput input) throws IOException {
		int len = (int)input.readUnsignedInt();
		long[] array = new long[len];
		for(int i = 0; i < len; i++)
			array[i] = input.readLong();
		return array;
	}

	protected String readLargeMask(ByteInput input) throws UnsupportedEncodingException, IOException {
		int len = (int) input.readUnsignedInt();
		return getMaskedString(len);
	}

	protected Object[] readArray(ByteInput input, int valueType, int length, Set<ReferencingValue> referencingValues) throws IOException {
		Class<?> componentType = getTypeClass(valueType);
		Object[] array = CollectionUtils.createArray(componentType, length);
		for(int i = 0; i < length; i++) {
			array[i] = readArrayValue(input, valueType, referencingValues);
			if(referencingValues != null && array[i] instanceof ReferencingValue) {
				referencingValues.add((ReferencingValue) array[i]);
			}
		}
		return array;
	}

	protected <N extends Number> N checkNumberForNull(ByteInput input, N value, N minValue) throws IOException {
		if(value == minValue) {
			switch(input.readByte()) {
				case 0:
					return null;
				case Byte.MIN_VALUE:
					return minValue;
				default:
					throw new IOException("Invalid byte sequence - expecting 0 or " + Byte.MIN_VALUE);
			}
		}
		return value;
	}

	protected Object readArrayValue(ByteInput input, int valueType, Set<ReferencingValue> referencingValues) throws IOException {
		switch(valueType) {
			case ONE_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				return readArray(input, input.readUnsignedByte(), input.readUnsignedByte(), referencingValues);
			case TWO_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				return readArray(input, input.readUnsignedByte(), input.readUnsignedShort(), referencingValues);
			case FOUR_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				return readArray(input, input.readUnsignedByte(), (int) input.readUnsignedInt(), referencingValues);
			case ONE_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
				return new MaskedString((String) readValue(input, valueType));
			case BYTE_ATTRIBUTE_VALUE_TYPE:
				return checkNumberForNull(input, input.readByte(), Byte.MIN_VALUE);
			case SHORT_ATTRIBUTE_VALUE_TYPE:
				return checkNumberForNull(input, input.readShort(), Short.MIN_VALUE);
			case INT_ATTRIBUTE_VALUE_TYPE:
				return checkNumberForNull(input, input.readInt(), Integer.MIN_VALUE);
			case LONG_ATTRIBUTE_VALUE_TYPE:
				return checkNumberForNull(input, input.readLong(), Long.MIN_VALUE);
			case BOOLEAN_FALSE_ATTRIBUTE_VALUE_TYPE:
			case BOOLEAN_TRUE_ATTRIBUTE_VALUE_TYPE:
				switch(input.readByte()) {
					case 0:
						return Boolean.FALSE;
					case 1:
						return Boolean.TRUE;
					default:
						return null;
				}
			default:
				return readValue(input, valueType);
		}
	}

	protected AttributeValue readArrayAttributeValue(ByteInput input, int valueType) throws IOException {
		final int length;
		switch(valueType) {
			case ONE_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				length = input.readUnsignedByte();
				break;
			case TWO_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				length = input.readUnsignedShort();
				break;
			case FOUR_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				length = (int) input.readUnsignedInt();
				break;
			default:
				throw new IOException("Invalid value type " + valueType);
		}
		int componentValueType = input.readUnsignedByte();
		final Set<ReferencingValue> referencingValues;
		switch(componentValueType) {
			case REFERENCE_ATTRIBUTE_VALUE_TYPE:
			case REFERENCE_MAP_VALUE_TYPE:
			case ALTERNATING_ATTRIBUTE_VALUE_TYPE:
			case ONE_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				// nested -- use referencingDelegate
				referencingValues = new LinkedHashSet<ReferencingValue>();
				break;
			default:
				referencingValues = null;
		}
		Object[] array = readArray(input, componentValueType, length, referencingValues);
		if(referencingValues != null && !referencingValues.isEmpty())
			return new ReferencingDelegatesAttributeValue(array, referencingValues);
		return new LiteralAttributeValue(array);
	}
	protected Class<?> getTypeClass(int valueType) {
		switch(valueType) {
			case NULL_ATTRIBUTE_VALUE_TYPE:
				return Object.class;
			case ONE_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE:
			case ONE_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_SECRET_VALUE_TYPE:
				return String.class;
			case REFERENCE_ATTRIBUTE_VALUE_TYPE:
			case REFERENCE_MAP_VALUE_TYPE:
			case ALTERNATING_ATTRIBUTE_VALUE_TYPE:
				return ReferencingValue.class;
			case ONE_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_BYTE_ARRAY_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_STRING_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_COMPRESSED_BYTE_ARRAY_VALUE_TYPE:
				return byte[].class;
			case ONE_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_INT_ARRAY_VALUE_TYPE:
				return int[].class;
			case ONE_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_LONG_ARRAY_VALUE_TYPE:
				return long[].class;
			case BYTE_ATTRIBUTE_VALUE_TYPE:
				return Byte.class;
			case CHAR_ATTRIBUTE_VALUE_TYPE:
				return Character.class;
			case SHORT_ATTRIBUTE_VALUE_TYPE:
				return Short.class;
			case INT_ATTRIBUTE_VALUE_TYPE:
				return Integer.class;
			case LONG_ATTRIBUTE_VALUE_TYPE:
				return Long.class;
			case BOOLEAN_TRUE_ATTRIBUTE_VALUE_TYPE:
			case BOOLEAN_FALSE_ATTRIBUTE_VALUE_TYPE:
				return Boolean.class;
			case ONE_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_VALUE_TYPE:
				return Node.class;
			case ONE_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_NODE_LIST_VALUE_TYPE:
				return NodeList.class;
			case ONE_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
			case TWO_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
			case FOUR_BYTE_LENGTH_ATTRIBUTE_ARRAY_TYPE:
				return Object[].class;
			default:
				return Object.class;
		}
	}

	protected Node readSmallNode(ByteInput input) throws UnsupportedEncodingException, IOException {
		try {
			return ConvertUtils.convert(Node.class, readSmallString(input));
		} catch(ConvertException e) {
			throw new IOException(e);
		}	
	}
	protected Node readMediumNode(ByteInput input) throws UnsupportedEncodingException, IOException {
		try {
			return ConvertUtils.convert(Node.class, readMediumString(input));
		} catch(ConvertException e) {
			throw new IOException(e);
		}	
	}
	protected Node readLargeNode(ByteInput input) throws UnsupportedEncodingException, IOException {
		try {
			return ConvertUtils.convert(Node.class, readLargeString(input));
		} catch(ConvertException e) {
			throw new IOException(e);
		}	
	}
	protected NodeList readSmallNodeList(ByteInput input) throws UnsupportedEncodingException, IOException {
		try {
			return ConvertUtils.convert(NodeList.class, readSmallString(input));
		} catch(ConvertException e) {
			throw new IOException(e);
		}	
	}
	protected NodeList readMediumNodeList(ByteInput input) throws UnsupportedEncodingException, IOException {
		try {
			return ConvertUtils.convert(NodeList.class, readMediumString(input));
		} catch(ConvertException e) {
			throw new IOException(e);
		}	
	}
	protected NodeList readLargeNodeList(ByteInput input) throws UnsupportedEncodingException, IOException {
		try {
			return ConvertUtils.convert(NodeList.class, readLargeString(input));
		} catch(ConvertException e) {
			throw new IOException(e);
		}	
	}

	protected boolean swapReferences(MessageChainStepV11 originalReferencedStep, String originalAttributeName, MessageChainStep newReferencedStep, String newAttributeName) {
		if(newAttributeName != null && newAttributeName.length() > MAX_NAME_LENGTH)
			throw new IllegalArgumentException("New Attribute Name is too long. It may not exceed " + MAX_NAME_LENGTH + " bytes");
		Set<MessageChainStep> processed = new HashSet<MessageChainStep>();
		return swapReferences(processed, originalReferencedStep, originalReferencedStep, originalAttributeName, newReferencedStep, newAttributeName);
	}

	protected boolean swapReferences(Set<MessageChainStep> processed, MessageChainStepV11 step, MessageChainStepV11 originalReferencedStep, String originalAttributeName, MessageChainStep newReferencedStep, String newAttributeName) {
		boolean replaced = false;
		// search all steps downstream from this for references to the mentioned attribute and replace
		for(MessageChainStep[] substeps : step.resultMap.values()) {
			if(substeps != null) {
				for(MessageChainStep substep : substeps) {
					if(processed.add(substep)) {
						MessageChainStepV11 substepV11 = (MessageChainStepV11) substep;
						for(Map.Entry<String, AttributeValue> entry : substepV11.avpMap.entrySet()) {
							if(entry.getValue() instanceof ReferenceAttributeValuePart) {
								ReferenceAttributeValuePart rav = (ReferenceAttributeValuePart) entry.getValue();
								if(originalReferencedStep.equals(rav.step) && rav.name.equals(originalAttributeName)) {
									entry.setValue(new ReferenceAttributeValuePart(newReferencedStep, newAttributeName));
									replaced = true;
								}
							} else if(entry.getValue() instanceof ReferenceMapAttributeValue) {
								ReferenceMapAttributeValue rmav = (ReferenceMapAttributeValue) entry.getValue();
								if(originalReferencedStep.equals(rmav.step) && rmav.prefix.equals(originalAttributeName)) {
									entry.setValue(new ReferenceMapAttributeValue(newReferencedStep, newAttributeName));
									replaced = true;
								}
							}
						}
						replaced |= swapReferences(processed, substepV11, originalReferencedStep, originalAttributeName, newReferencedStep, newAttributeName);
					}
				}
			}
		}
		return replaced;
	}

	@Override
	public boolean isWaitingFor(MessageChainStep step) {
		for(MessageChainStepV11 other : steps) {
			Set<MessageChainStep> waitFors = other.getWaitFors();
			if(waitFors != null)
				for(MessageChainStep waitFor : waitFors)
					if(waitFor == step)
						return true;
		}
		return false;
	}
}