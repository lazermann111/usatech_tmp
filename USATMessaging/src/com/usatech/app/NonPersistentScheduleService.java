package com.usatech.app;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueueException;
import simple.io.Log;


/** This schedules messages to be enqueued at a later time. It does not acknowledge messages from the queue until the message is run. This
 *  relies on the queue to guarantee the running of the job but results in  long-unacknowledged messages in the queue.
 * @author Brian S. Krug
 *
 */
public class NonPersistentScheduleService extends AbstractScheduleService {
	private static final Log log = Log.getLog();
	protected static final String[] JOB_DATA_NAMES = new String[] {"queue", "message", "work"};

	public NonPersistentScheduleService(String serviceName) {
		super(serviceName);
	}

	@Override
	protected Class<? extends Job> getJobClass() {
		return NonPersistentEnqueueJob.class;
	}

	@Override
	protected boolean isJobPersistent() {
		return false;
	}

	@Override
	protected void schedule(long time, String queueKey, byte[] chainBody, Work<?> work) throws ServiceException, WorkQueueException {
		try {
			scheduleTrigger(time, JOB_DATA_NAMES, queueKey, chainBody, work);
		} catch(SchedulerException e) {
			throw new ServiceException("Could not schedule enqueue", e);
		}
	}

	public static class NonPersistentEnqueueJob extends EnqueueJob {
		public void execute(JobExecutionContext context) throws JobExecutionException {
			JobDataMap jobDataMap = context.getMergedJobDataMap();
			Work<?> work = (Work<?>)jobDataMap.get("work");
			try {
				publish(jobDataMap);
				work.workComplete();
			} catch(WorkQueueException e) {
				log.error("Could not complete work. Throwing Job Exception.", e);
				throw new JobExecutionException(e);
			}
		}
	}
}
