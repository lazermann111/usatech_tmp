/**
 *
 */
package com.usatech.app;

import simple.app.Publisher;
import simple.app.QoS;
import simple.io.ByteInput;
import simple.lang.InterruptGuard;
import simple.security.crypt.EncryptionInfoMapping;

/**
 * @author Brian S. Krug
 *
 */
public interface MessageChainTaskInfo extends InterruptGuard {
	/**
	 * @return The name of the service that called this component. For informational purposes.
	 */
	public String getServiceName() ;
	/**
	 * @return The publisher that will post a message to the queue-system that produced the message being processed.
	 */
	public Publisher<ByteInput> getPublisher() ;
	/**
	 * @return The current step to be processed
	 */
	public MessageChainStep getStep() ;
	/** Whether this step has potentially been redelivered or not
	 * @return <code>true</code> if and only if there is a possibility that this step was partially processed already
	 */
	public boolean isRedelivered() ;

	/** The number of times this Work was previously re-tried or -1 if not known
	 * @return
	 */
	public int getRetryCount() ;
	/**
	 * @return The globally unique id of the message chain
	 */
	public String getMessageChainGuid() ;
	
	/** Sets the QoS to use for the next message in the chain
	 * @param qos
	 */
	public void setNextQos(QoS qos) ;
	
	/** Gets the qos of the current message being processed
	 * @param qos
	 * @return
	 */
	public QoS getCurrentQos() ;
	
	/** A Runnable to execute after next step in Message Chain is published
	 * @param afterComplete
	 */
	public void setAfterComplete(Runnable afterComplete) ;	

	public EncryptionInfoMapping getEncryptionInfoMapping();

	public long getEnqueueTime();
}
