package com.usatech.app;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.db.Call.BatchUpdate;
import simple.db.CallNotFoundException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;

public class BatchUpdateDatasetHandler extends AbstractAttributeDatasetHandler<ServiceException> {
	protected final BatchUpdate batch;
	protected final Connection conn;
	protected final int maxBatchSize;
	protected final Map<String, Object> defaultData;
	public BatchUpdateDatasetHandler(Connection conn, String callId, int maxBatchSize) throws ParameterException, CallNotFoundException {
		this(conn, callId, maxBatchSize, null);
	}

	public BatchUpdateDatasetHandler(Connection conn, String callId, int maxBatchSize, Map<String, Object> defaultData) throws ParameterException, CallNotFoundException {
		this(conn, DataLayerMgr.createBatchUpdate(callId, false, 0.0f), maxBatchSize, defaultData);
	}

	public BatchUpdateDatasetHandler(Connection conn, BatchUpdate batch, int maxBatchSize, Map<String, Object> defaultData) {
		this.batch = batch;
		this.conn = conn;
		this.maxBatchSize = maxBatchSize;
		this.defaultData = defaultData;
	}

	public void handleDatasetEnd() throws ServiceException {
		while(batch.getPendingCount() > 0)
			executeBatch();
	}

	public void handleDatasetStart(String[] columnNames) throws ServiceException {
	}

	@Override
	public void handleRowStart() {
		super.handleRowStart();
		if(defaultData != null)
			data.putAll(defaultData);

	}
	public void handleRowEnd() throws ServiceException {
		try {
			batch.addBatch(data);
		} catch(ParameterException e) {
			throw new ServiceException(e);
		}
		data.clear();
		if(batch.getPendingCount() >= maxBatchSize)
			executeBatch();
	}

	protected void executeBatch() throws ServiceException {
		try {
			batch.executeBatch(conn, maxBatchSize);
			if(!conn.getAutoCommit())
				conn.commit();
		} catch(ParameterException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		}
	}
}