package com.usatech.app;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.beans.IntrospectionException;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import simple.app.AbstractWorkQueueService;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.bean.BeanFactoryByProperties;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ByteArrayByteInput;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.Initializer;
import simple.util.CollectionUtils;

/** This schedules messages to be enqueued at a later time.
 * @author Brian S. Krug
 *
 */
public abstract class AbstractScheduleService extends AbstractWorkQueueService<ByteInput> implements AbstractWorkQueueService.WorkProcessor<ByteInput> {
	private static final Log log = Log.getLog();
	protected AtomicLong jobNameId=new AtomicLong(0);
	protected static Publisher<ByteInput> publisher;
	public static abstract class EnqueueJob implements Job {
		protected void publish(JobDataMap jobDataMap) throws JobExecutionException {
			String queueKey  = jobDataMap.getString("queue");
			byte[] content = (byte[])jobDataMap.get("message");
			publish(jobDataMap, queueKey, content);
		}
		protected void publish(JobDataMap jobDataMap, String queueKey, byte[] content) throws JobExecutionException {
			try {
				getPublisher(jobDataMap).publish(queueKey, new ByteArrayByteInput(content));
			} catch(ServiceException e) {
				log.warn("Could not publish scheduled message. Throwing Job Exception.", e);
				throw new JobExecutionException(e);
			}
		}
		protected Publisher<ByteInput> getPublisher(JobDataMap jobDataMap) throws JobExecutionException {
			if(publisher != null)
				return publisher;
			// @Todo need to optimize the part of publisherClass so it will not create every time
			String className = jobDataMap.getString("publisherClass");
			if(className == null || (className=className.trim()).length() == 0)
				throw new JobExecutionException("A value for 'publisherClass' was not provided in the job data map");
			Class<? extends Publisher<ByteInput>> cls;
			try {
				cls = (Class<? extends Publisher<ByteInput>>)Class.forName(className).asSubclass(Publisher.class);
				return BeanFactoryByProperties.getInstance(cls, CollectionUtils.uncheckedMap(jobDataMap, String.class, Object.class));
			} catch(ClassNotFoundException e) {
				throw new JobExecutionException(e);
			} catch(IntrospectionException e) {
				throw new JobExecutionException(e);
			} catch(IllegalAccessException e) {
				throw new JobExecutionException(e);
			} catch(InvocationTargetException e) {
				throw new JobExecutionException(e);
			} catch(InstantiationException e) {
				throw new JobExecutionException(e);
			} catch(ConvertException e) {
				throw new JobExecutionException(e);
			} catch(ParseException e) {
				throw new JobExecutionException(e);
			}
		}
	}

	protected Scheduler scheduler;
	protected int defaultRetryResultCode = 255;
	protected int defaultErrorResultCode = 254;
	protected long retryDelayMs = 10 * 60 * 1000; /*10 minutes*/
	protected int retryTries = 9;
	protected String host;
	protected int port = -1;
	protected String exchange = "";
	protected String realm;
	protected JobDetail enqueueJobDetail;
	protected final JobDataMap jobDataMap = new JobDataMap();
	protected final Properties schedulerProps = new Properties();
	protected Object quartzInstanceId;
	public Object getQuartzInstanceId() {
		return quartzInstanceId;
	}

	public void setQuartzInstanceId(Object quartzInstanceId) {
		this.quartzInstanceId = quartzInstanceId;
	}

	public String getJobName(){
		return ManagementFactory.getRuntimeMXBean().getName()+":"+System.currentTimeMillis()+":"+jobNameId.getAndIncrement();
	}
	protected final Initializer<SchedulerException> initializer = new Initializer<SchedulerException>() {
		@Override
		protected void doInitialize() throws SchedulerException {
			enqueueJobDetail = newJob(getJobClass())
					.withIdentity(getJobName(), "USAT Messaging :: Scheduled Publish")
					.storeDurably()
					.requestRecovery(isJobPersistent()) 
					.build();
					
			if(getQuartzInstanceId()!=null){
				schedulerProps.setProperty(StdSchedulerFactory.PROP_SCHED_INSTANCE_ID, getQuartzInstanceId().toString());
			}
			scheduler = new StdSchedulerFactory(schedulerProps).getScheduler();
			scheduler.start();
			scheduler.addJob(enqueueJobDetail, true);
		}
		@Override
		protected void doReset(){
			try {
				scheduler.shutdown();
			} catch(SchedulerException e){
				log.warn("Failed to shutdown scheduler.", e);
			}
			scheduler = null;
			enqueueJobDetail = null;
		}
	};
	public AbstractScheduleService(String serviceName) {
		super(serviceName);
	}

	protected abstract boolean isJobPersistent() ;

	protected abstract Class<? extends Job> getJobClass() ;

	protected abstract void schedule(long time, String queueKey, byte[] chainBody, Work<?> work) throws ServiceException, WorkQueueException ;

	@Override
	protected WorkProcessor<ByteInput> getWorkProcessor() {
		return this;
	}

	public boolean interruptProcessing(Thread thread) {
		return false;
	}

	public void processWork(final Work<ByteInput> work) throws ServiceException, WorkQueueException {	
		MessageChain mc = deserialize(work.getBody());
		MessageChainStep step = mc.getCurrentStep();
		if(log.isDebugEnabled()) log.debug("Processing queue " + step.getQueueKey() + "; Attributes " + step.getAttributes());
		String timeVal = ConvertUtils.getStringSafely(step.getAttributes().get("time"));
		long time;
		if(timeVal != null) {
			try {
				time = ConvertUtils.getLong(timeVal);
			} catch(ConvertException e) {
				throw new ServiceException("Value for attribute 'time' is invalid", e);
			}
		} else {
			try {
				time = System.currentTimeMillis() + ConvertUtils.getLong(step.getAttributes().get("retryDelay"), retryDelayMs);
			} catch(ConvertException e) {
				throw new ServiceException("Value for attribute 'retryDelay' is invalid", e);
			}
		}
		mc.setResultCode(0);
		MessageChainStep[] nextSteps = step.getNextSteps(0);
		if(nextSteps == null || nextSteps.length == 0)
			throw new ServiceException("No message chain provided for result code = 0");
		for(MessageChainStep nextStep : nextSteps)
			schedule(time, nextStep.getQueueKey(), MessageChainService.serialize(nextStep), work);
	}

	protected MessageChain deserialize(ByteInput input) throws ServiceException {
		return MessageChainService.deserializeMessageChain_v11(input, queue.getQueueKey(), false);
	}

	protected void scheduleTrigger(long time, String[] extraDataNames, Object... extraDataValues) throws SchedulerException {
		try {
			initializer.initialize();
		} catch(InterruptedException e) {
			throw new SchedulerException("Could not startup Quartz scheduler", e);
		}
		
		Trigger trigger = makeOneTimeTrigger(enqueueJobDetail.getKey(), time);
		if(extraDataValues != null && extraDataValues.length > 0 && extraDataNames != null && extraDataNames.length > 0) {
			JobDataMap jobDataMap = trigger.getJobDataMap();
			for(int i = 0; i < extraDataNames.length && i < extraDataValues.length; i++) {
				jobDataMap.put(extraDataNames[i], extraDataValues[i]);
			}
		}
		
		scheduler.scheduleJob(trigger);
	}

	/**
	 * Added to include the job at build time for the trigger.
	 * @param jobKey identity of the job being scheduled
	 * @param time Time at which to run the job
	 * @return a Quartz Trigger configured with the passed parameters.
	 */
	protected Trigger makeOneTimeTrigger(JobKey jobKey, long time) {
		
		Trigger trigger = newTrigger()
					// no identity gives us the unique behavior evident in the Math.random() call used previously
					.forJob(jobKey)
					.startAt(new Date(time))
					.usingJobData(getJobDataMap())
					.build();
		
		return trigger;
	}

	/**
	 * This is kept for <R29 versions.
	 * Use the other API, which takes the name and group, which are 
	 * build time parameters for creating a Trigger
	 * @param time
	 * @return
	 */
	@Deprecated
	protected Trigger makeOneTimeTrigger(long time) {
		
		Trigger trigger = newTrigger()
					.withIdentity("Once@" + time + "["+Math.random() + "]", "RetryTrigger")
					.startAt(new Date(time))
					.usingJobData(getJobDataMap())
					.build();
		
		return trigger;
	}

	@Override
	public void setQueue(WorkQueue<ByteInput> queue) {
		if(queue != null && !ByteInput.class.isAssignableFrom(queue.getWorkBodyType()))
			throw new IllegalArgumentException("The queue work body type '" + queue.getWorkBodyType().getName() + "' is not an instance of ByteInput");
		if(!isJobPersistent() && queue != null && !queue.isAutonomous())
			throw new IllegalArgumentException("Queue is not autonomous but this type of ScheduleService requires autonomy");
		super.setQueue(queue);
	}

	public JobDataMap getJobDataMap() {
		return jobDataMap;
	}

	public Properties getSchedulerProps() {
		return schedulerProps;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		AbstractScheduleService.publisher = publisher;
	}
}
