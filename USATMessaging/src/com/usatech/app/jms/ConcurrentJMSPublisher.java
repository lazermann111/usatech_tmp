package com.usatech.app.jms;

import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.QoS;
import simple.app.ServiceException;
import simple.io.ByteInput;


/** A <b>thread-safe</b> implementation of Publisher for JMS system.
 * @author Brian S. Krug
 *
 */
public class ConcurrentJMSPublisher extends StandaloneJMSPublisher<ByteInput> {
	protected final ReentrantLock lock =  new ReentrantLock();

	/**
	 * @see com.usatech.app.jms.StandaloneJMSPublisher#isTransacted()
	 */
	@Override
	public boolean isTransacted() {
		return true;
	}
	@Override
	public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content, Long correlation, QoS qos, String... blocks) throws ServiceException {
		lock.lock();
		try {
			super.publish(queueKey, multicast, temporary, content, correlation, qos, blocks);
		} finally {
			lock.unlock();
		}
	}
	@Override
	protected void writeContent(MessageOutputStream<?> out, ByteInput content) throws IOException {
		content.copyInto(out);
	}
}
