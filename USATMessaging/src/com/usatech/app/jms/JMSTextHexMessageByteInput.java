package com.usatech.app.jms;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import simple.io.HexStringByteInput;

public class JMSTextHexMessageByteInput extends HexStringByteInput {
	public JMSTextHexMessageByteInput(TextMessage message) throws JMSException {
		super(message.getText());
	}
}
