package com.usatech.app.jms;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.NamingException;

import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.event.ProgressListener;
import simple.io.Log;
import simple.util.concurrent.RunOnGetFuture;

public abstract class AbstractJMSSubscription<J> extends AbstractJMSWorkBase<J> {
	private static final Log log = Log.getLog();
	protected String subscriber;
	protected boolean noLocal;

	protected final RunOnGetFuture<WorkQueue.Retriever<J>> retrieverFuture = new RunOnGetFuture<WorkQueue.Retriever<J>>() {
		@Override
		protected WorkQueue.Retriever<J> call(Object... params) throws Exception {
			return createRetriever();
		}
	};

	protected class JMSRetriever extends AbstractJMSRetriever {
		protected volatile Message nextMsg;
		protected long availableAfter;
		protected final ReentrantLock lock = new ReentrantLock();
		protected Condition signal;

		public JMSRetriever(Session session, MessageConsumer consumer) {
			super(session, consumer);
		}
		
		@Override
		protected Message nextMessage() throws WorkQueueException, InterruptedException {
			Message msg;
			if(nextMsg != null) {
				long timeout;
				while(availableAfter > 0 && (timeout=availableAfter-System.currentTimeMillis()) > 0) {
					if(signal == null)
						signal = lock.newCondition();
					signal.await(timeout, TimeUnit.MILLISECONDS);
				}
				msg = nextMsg;
				nextMsg = null;
			} else {
				try {
					msg = consumer.receive();
				} catch(JMSException e) {
					if(e.getCause() instanceof InterruptedException)
						throw (InterruptedException)e.getCause();
					throw new WorkQueueException("Could not receive message", e);
				}
			}
			return msg;
		}
		
		public Work<J> next() throws WorkQueueException, InterruptedException {
			lock.lock();
			try {
				return next();
			} catch(WorkQueueException e) {
				lock.unlock();
				throw e;
			} catch(InterruptedException e) {
				lock.unlock();
				throw e;
			} catch(RuntimeException e) {
				lock.unlock();
				throw e;
			} catch(Error e) {
				lock.unlock();
				throw e;
			}
		}
		
		protected void flushPublisher() throws WorkQueueException {
			boolean okay = false;
			try {
				super.flushPublisher();
				okay = true;
			} finally {
				if(!okay)
					lock.unlock();
			}
		}
		protected void handleWorkAborted(Message msg, String desc, WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
			try {
				//Do not use JMS system for retry dead-letter queues
				switch(retryType) {
					case BLOCKING_RETRY:
						prerequisiteManager.resetAvailability();
					case NONBLOCKING_RETRY: // single threaded, so these are the same
						//getPublisher();
						try {
							long aa = JMSBase.prepareNextAvailable(msg, null, session, getRetryPolicy());
							if(aa < Long.MAX_VALUE) {
								availableAfter = aa;
								nextMsg = msg;
							}
						} catch(JMSException e) {
							throw new WorkQueueException("Could not republish " + desc, e);
						}
						break;
					case IMMEDIATE_RETRY:
						try {
							JMSBase.incrementRetryCount(msg);
						} catch(JMSException e) {
							throw new WorkQueueException("Could not increment retry count on " + desc, e);
						}
						availableAfter = 0;
						nextMsg = msg;
						break;
					case NO_RETRY:
						try {
							session.commit();
						} catch(JMSException e) {
							throw new WorkQueueException("Could not disgard " + desc, e);
						}
						break;
				}
			} finally {
				lock.unlock();
			}
		}
		
		protected void handleWorkComplete(Message msg, String desc) throws WorkQueueException {
			try {
				session.commit();
			} catch(JMSException e) {
				throw new WorkQueueException("Could not commit " + desc, e);
			} finally {
				lock.unlock();
			}
		}

		public boolean cancel(Thread thread) {
			try {
				consumer.close();
			} catch(JMSException e) {
				//ignore
				return false;
			}
			if(nextMsg != null) {
				availableAfter = 0;
				nextMsg = null;
				lock.lock();
				try {
					if(signal != null)
						signal.signalAll();
				} finally {
					lock.unlock();
				}
			}
			return true;
		}
		public void close() {
			nextMsg = null;
			super.close();
			retrieverFuture.reset();
		}
	};

	protected JMSRetriever createRetriever() throws WorkQueueException {
		String qk = getQueueKey();
		if(qk == null)
			throw new WorkQueueException("QueueKey is not set");
		String sub = getSubscriber();
		if(sub == null)
			throw new WorkQueueException("Subscriber is not set");
		try {
			Session session = JMSBase.createSession(getUsername(), getPassword(), true);
			Topic topic = (Topic)JMSBase.getDestination(qk, true, session);
			JMSRetriever retriever = new JMSRetriever(session, session.createDurableSubscriber(topic, sub, null, isNoLocal()));
			if(log.isDebugEnabled())
				log.debug("Listening for messages on jms topic '" + topic + "' with subscriber '" + sub + "'");
			return retriever;
		} catch(JMSException e) {
			throw new WorkQueueException("Could not create JMS Session or Consumer", e);
		} catch(NamingException e) {
			throw new WorkQueueException("Could not lookup ConnectionFactory or Destination", e);
		} catch(InterruptedException e) {
			throw new WorkQueueException("While initializaling", e);
		}
	}

	public boolean isAutonomous() {
		return false;
	}

	@Override
	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
		this.retrieverFuture.reset();
	}

	public WorkQueue.Retriever<J> getRetriever(ProgressListener listener) throws WorkQueueException {
		try {
			return retrieverFuture.get();
		} catch(InterruptedException e) {
			throw new WorkQueueException("While initializaling retriever", e);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof WorkQueueException)
				throw (WorkQueueException)e.getCause();
			throw new WorkQueueException("Could not create retriever", e);
		}
	}
	public String getSubscriber() {
		return subscriber;
	}
	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
		this.retrieverFuture.reset();
	}
	public boolean isNoLocal() {
		return noLocal;
	}
	public void setNoLocal(boolean noLocal) {
		this.noLocal = noLocal;
	}
}
