package com.usatech.app.jms;

import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.NamingException;

import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.event.ProgressListener;
import simple.io.Log;
import simple.util.concurrent.RunOnGetFuture;

public abstract class AbstractJMSWorkTopic<J> extends AbstractJMSWorkBase<J> {
	private static final Log log = Log.getLog();
	protected boolean noLocal;
	protected boolean transacted;

	protected final RunOnGetFuture<WorkQueue.Retriever<J>> retrieverFuture = new RunOnGetFuture<WorkQueue.Retriever<J>>() {
		@Override
		protected WorkQueue.Retriever<J> call(Object... params) throws Exception {
			return createRetriever();
		}
	};

	protected static class MessageDelayed implements Delayed {
		protected final Message message;
		protected final long availableAfter;
		protected final int order;

		public MessageDelayed(Message message, long availableAfter, int order) {
			this.message = message;
			this.availableAfter = availableAfter;
			this.order = order;
		}
		/**
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(Delayed o) {
			MessageDelayed md = (MessageDelayed)o;
			if(availableAfter < md.availableAfter)
				return -1;
			if(availableAfter > md.availableAfter)
				return 1;
			return order - md.order;
		}
		/**
		 * @see java.util.concurrent.Delayed#getDelay(java.util.concurrent.TimeUnit)
		 */
		@Override
		public long getDelay(TimeUnit unit) {
			return unit.convert(availableAfter - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
		}
	}
	protected class JMSRetriever extends AbstractJMSRetriever implements MessageListener {
		protected final DelayQueue<MessageDelayed> messages = new DelayQueue<MessageDelayed>();
		protected final AtomicInteger orderGenerator = new AtomicInteger(Integer.MIN_VALUE);
		public JMSRetriever(Session session, MessageConsumer consumer) throws JMSException {
			super(session, consumer);
			consumer.setMessageListener(this);
		}
		/**
		 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
		 */
		@Override
		public void onMessage(Message message) {
			messages.offer(new MessageDelayed(message, 0, orderGenerator.getAndIncrement()));
			try {
				if(session.getTransacted())
					session.commit();
				else
					message.acknowledge();
			} catch(JMSException e) {
				log.warn("Could not acknowledge message " + message, e);
			}
		}
		@Override
		protected Message nextMessage() throws WorkQueueException, InterruptedException {
			return messages.take().message;
		}
		
		@Override
		protected void handleWorkAborted(Message msg, String desc, WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
			//Do not use JMS system for retry dead-letter queues
			switch(retryType) {
				case BLOCKING_RETRY: // this is not supported - just use nonblocking retry
					prerequisiteManager.resetAvailability();
				case NONBLOCKING_RETRY: // single threaded, so these are the same
					//getPublisher();
					try {
						long aa = JMSBase.prepareNextAvailable(msg, null, session, getRetryPolicy());
						if(aa < Long.MAX_VALUE) {
							messages.offer(new MessageDelayed(msg, aa, orderGenerator.getAndIncrement()));
						} else {
							log.warn("Discarding " + desc + " after failing on " + JMSBase.getRetryCount(msg) + " attempts");
						}
					} catch(JMSException e) {
						throw new WorkQueueException("Could not republish " + desc, e);
					}
					break;
				case IMMEDIATE_RETRY:
					try {
						JMSBase.incrementRetryCount(msg);
					} catch(JMSException e) {
						throw new WorkQueueException("Could not increment retry count on " + desc, e);
					}
					messages.offer(new MessageDelayed(msg, System.currentTimeMillis(), orderGenerator.getAndIncrement()));
					break;
				case NO_RETRY:
					try {
						log.warn("Discarding " + desc + " after failing on " + JMSBase.getRetryCount(msg) + " attempts");
					} catch(JMSException e) {
						log.warn("Discarding " + desc);
					}
					break;
			}
		}
		
		@Override
		protected void handleWorkComplete(Message msg, String desc) throws WorkQueueException {
			// don't do anything			
		}
		
		public boolean cancel(Thread thread) {
			thread.interrupt();
			return true;
		}
		public void close() {
			super.close();
			retrieverFuture.reset();
		}
	};

	protected JMSRetriever createRetriever() throws WorkQueueException {
		String qk = getQueueKey();
		if(qk == null)
			throw new WorkQueueException("QueueKey is not set");
		try {
			Session session = JMSBase.createSession(getUsername(), getPassword(), isTransacted());
			Topic topic = (Topic)JMSBase.getDestination(qk, true, session);
			JMSRetriever retriever = new JMSRetriever(session, session.createConsumer(topic, null, isNoLocal()));
			if(log.isDebugEnabled())
				log.debug("Listening for messages on jms topic '" + topic + "''");
			return retriever;
		} catch(JMSException e) {
			throw new WorkQueueException("Could not create JMS Session or Consumer", e);
		} catch(NamingException e) {
			throw new WorkQueueException("Could not lookup ConnectionFactory or Destination", e);
		} catch(InterruptedException e) {
			throw new WorkQueueException("While initializaling", e);
		}
	}

	public boolean isAutonomous() {
		return true;
	}

	@Override
	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
		this.retrieverFuture.reset();
	}

	public WorkQueue.Retriever<J> getRetriever(ProgressListener listener) throws WorkQueueException {
		try {
			return retrieverFuture.get();
		} catch(InterruptedException e) {
			throw new WorkQueueException("While initializaling retriever", e);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof WorkQueueException)
				throw (WorkQueueException)e.getCause();
			throw new WorkQueueException("Could not create retriever", e);
		}
	}

	public boolean isNoLocal() {
		return noLocal;
	}
	public void setNoLocal(boolean noLocal) {
		this.noLocal = noLocal;
	}

	public boolean isTransacted() {
		return transacted;
	}

	public void setTransacted(boolean transacted) {
		this.transacted = transacted;
	}
}
