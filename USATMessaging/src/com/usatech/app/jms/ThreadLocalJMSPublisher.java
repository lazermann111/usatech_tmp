package com.usatech.app.jms;

import java.io.IOException;

import simple.io.ByteInput;


/** Thread-safe implementation of Publisher for JMS system.
 * @author Brian S. Krug
 *
 */
public class ThreadLocalJMSPublisher extends AbstractThreadLocalJMSPublisher<ByteInput> {
	@Override
	protected void writeContent(MessageOutputStream<?> out, ByteInput content) throws IOException {
		content.copyInto(out);
	}
	@Override
	public boolean isTransacted() {
		return true;
	}

	@Override
	public int getConsumerCount(String queueName, boolean temporary) {
		return 1;
	}
}
