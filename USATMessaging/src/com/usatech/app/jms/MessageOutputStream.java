/**
 * 
 */
package com.usatech.app.jms;

import java.io.IOException;
import java.io.OutputStream;

import javax.jms.JMSException;
import javax.jms.Message;

public abstract class MessageOutputStream<M extends Message> extends OutputStream {
	protected final M message;
	public MessageOutputStream(M msg) {
		this.message = msg;
	}
	public void clear() throws JMSException {
		message.clearProperties();
		message.clearBody();
	}
	public M getMessage() {
		return message;
	}
	protected IOException newIOException(String reason, Throwable cause) {
		IOException ioe = new IOException(reason);
		ioe.initCause(cause);
		return ioe;
	}
}