package com.usatech.app.jms;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;

import javax.jms.BytesMessage;
import javax.jms.JMSException;

import simple.io.ByteInput;
import simple.io.ByteOutput;

public class JMSByteMessageByteInput implements ByteInput {
	protected final byte readBuffer[] = new byte[1024];
	protected final BytesMessage message;
	protected int position;
	public JMSByteMessageByteInput(BytesMessage message) {
		super();
		this.message = message;
	}

	protected IOException newIOException(Throwable cause)  {
		IOException ioe = new IOException();
		ioe.initCause(cause);
		return ioe;
	}
	public int length() throws IOException {
		try {
			return (int)message.getBodyLength();
		} catch(JMSException e) {
			throw newIOException(e);
		}
	}

	public int position() {
		return position;
	}

	public byte readByte() throws IOException {
		byte b;
		try {
			b = message.readByte();
		} catch(JMSException e) {
			throw newIOException(e);
		}
		position++;
		return b;
	}

	public char readByteAsChar() throws IOException {
		return (char) readUnsignedByte();
	}

	public String readBytes(int length) throws IOException {
		byte[] buf;
		if(readBuffer.length < length) {
			buf = new byte[length];
		} else
			buf = readBuffer;
		int len;
		try {
			len = message.readBytes(buf, length);
		} catch(JMSException e) {
			throw newIOException(e);
		}
		if(len > 0)
			position += len;
		if(len < length)
			throw new EOFException();
		return new String(buf, 0, length);
	}

	public char readChar() throws IOException {
		char c;
		try {
			c = message.readChar();
		} catch(JMSException e) {
			throw newIOException(e);
		}
		position+=2;
		return c;
	}

	public String readChars(int length) throws IOException {
		char[] buf = new char[length];
		//TODO: could be more efficient by using byte array
		for(int i = 0; i < length; i++)
			buf[i] = readChar();
		return new String(buf, 0, length);
	}

	public double readDouble() throws IOException {
		double d;
		try {
			d = message.readDouble();
		} catch(JMSException e) {
			throw newIOException(e);
		}
		position+=8;
		return d;
	}

	public float readFloat() throws IOException {
		float f;
		try {
			f = message.readFloat();
		} catch(JMSException e) {
			throw newIOException(e);
		}
		position+=4;
		return f;
	}

	public int readInt() throws IOException {
		int i;
		try {
			i = message.readInt();
		} catch(JMSException e) {
			throw newIOException(e);
		}
		position+=4;
		return i;
	}

	public long readLong() throws IOException {
		long l;
		try {
			l = message.readLong();
		} catch(JMSException e) {
			throw newIOException(e);
		}
		position+=8;
		return l;
	}

	public short readShort() throws IOException {
		short s;
		try {
			s = message.readShort();
		} catch(JMSException e) {
			throw newIOException(e);
		}
		position+=2;
		return s;
	}

	public int readUnsignedByte() throws IOException {
		int i;
		try {
			i = message.readUnsignedByte();
		} catch(JMSException e) {
			throw newIOException(e);
		}
		position++;
		return i;
	}

	public long readUnsignedInt() throws IOException {
		int ch1 = readUnsignedByte();
		int ch2 = readUnsignedByte();
		int ch3 = readUnsignedByte();
		int ch4 = readUnsignedByte();
		return (((long) ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0));
	}

	public int readUnsignedShort() throws IOException {
		int i;
		try {
			i = message.readUnsignedShort();
		} catch(JMSException e) {
			throw newIOException(e);
		}
		position++;
		return i;
	}

	public void reset() throws IOException {
		try {
			message.reset();
		} catch(JMSException e) {
			throw newIOException(e);
		}
	}

	public boolean isResettable() {
		return true;
	}

	public int copyInto(OutputStream output) throws IOException {
		int tot = 0;
		try {
			for(int len = 0; (len=message.readBytes(readBuffer)) >= 0; tot += len)
				output.write(readBuffer, 0, len);
		} catch(JMSException e) {
			throw newIOException(e);
		}
        output.flush();
        return tot;
	}

	public void readBytes(byte[] buffer, int offset, int length) throws IOException {
		if(offset == 0) {
			try {
				int len = message.readBytes(buffer, length);
				position += len;
				if(len < length)
					throw new EOFException();
			} catch(JMSException e) {
				throw newIOException(e);
			}
		} else {
			byte[] b = new byte[length];
			int len;
			try {
				len = message.readBytes(b, length);
				position += len;
				if(len < length)
					throw new EOFException();
			} catch(JMSException e) {
				throw newIOException(e);
			}
			System.arraycopy(b, 0, buffer, offset, len);
		}
	}

	@Override
	public int copyInto(ByteOutput output) throws IOException {
		int tot = 0;
		try {
			for(int len = 0; (len = message.readBytes(readBuffer)) >= 0; tot += len)
				output.write(readBuffer, 0, len);
		} catch(JMSException e) {
			throw newIOException(e);
		}
		output.flush();
		return tot;
	}
}
