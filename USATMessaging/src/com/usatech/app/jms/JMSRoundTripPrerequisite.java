package com.usatech.app.jms;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;

import simple.app.Prerequisite;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;

public class JMSRoundTripPrerequisite extends StandaloneJMSPublisher<byte[]> implements Prerequisite {
	private static final Log log = Log.getLog();
	protected static final String jvmNamePrefix = ManagementFactory.getRuntimeMXBean().getName().replaceAll("\\W", "_") + '.';
	protected static final Random random = new Random();
	protected static final AtomicInteger idGenerator = new AtomicInteger((int)Double.doubleToLongBits(Math.random()));
	protected String queueNamePrefix = "round-trip-test.";
	protected int contentSize = 8;
	protected long timeout;
	
	@Override
	public boolean isAvailable() {
		//generate random queue name
		String queueName = getQueueNamePrefix() + jvmNamePrefix + Integer.toHexString(idGenerator.getAndIncrement());
		int sz = getContentSize();
		if(sz < 1)
			sz = 1;
		byte[] content = new byte[sz];
		random.nextBytes(content);
		log.info("Testing JMS Round trip with new queue '" + queueName + "'");
		// publish random bytes to that queue
		try {
			publish(queueName, content);
		} catch(ServiceException e) {
			log.warn("Could not publish message for round trip JMS test on queue '" + queueName + "'", e);
			return false;
		}
		
		//try to consume that message
		Destination dest;
		try {
			dest = JMSBase.getDestination(queueName, false, session);
		} catch(JMSException e) {
			log.warn("Could not get JMS destination for round trip JMS test on queue '" + queueName + "'", e);
			return false;
		}
		MessageConsumer consumer;
		try {
			consumer = session.createConsumer(dest);
		} catch(JMSException e) {
			log.warn("Could not create a consumer for round trip JMS test on queue '" + queueName + "'", e);
			return false;
		}
		long t = getTimeout();
		Message msg;
		try {
			if(t > 0)
				msg = consumer.receive(t);
			else
				msg = consumer.receiveNoWait();
		} catch(JMSException e) {
			log.warn("Could not receive a message for round trip JMS test on queue '" + queueName + "'", e);
			return false;
		}
		if(msg == null) {
			log.info("Queue contained no messages - JMS round trip test failed on queue '" + queueName + "'");
			return false;
		}
		ByteInput bi;
		if(msg instanceof BytesMessage) {
			bi = new JMSByteMessageByteInput((BytesMessage)msg);
		} else if(msg instanceof StreamMessage) {
			bi = new JMSStreamMessageByteInput((StreamMessage)msg);
		} else if(msg instanceof TextMessage) {
			try {
				bi = new JMSTextHexMessageByteInput((TextMessage)msg);
			} catch(JMSException e) {
				log.warn("Could not get content of JMS Text Message", e);
				return false;
			}
		} else {
			log.warn("JMS Message Type " + JMSBase.getMessageType(msg) + " not supported on message ID " + JMSBase.getMessageId(msg));
			return false;
		}
		
		// check random bytes
		byte[] bytes;
		try {
			bytes = new byte[bi.length()];
			bi.readBytes(bytes, 0, bytes.length);
		} catch(IOException e) {
			log.warn("Could not read message content for round trip JMS test on queue '" + queueName + "'", e);
			return false;
		}
		if(!Arrays.equals(content, bytes)) {
			log.warn("Content does not match for round trip JMS test on queue '" + queueName + "'");
			return false;
		}
		return true;
	}
	@Override
	protected boolean isTransacted() {
		return true;
	}
	@Override
	protected void writeContent(MessageOutputStream<?> out, byte[] content) throws IOException {
		out.write(content);
	}
	public String getQueueNamePrefix() {
		return queueNamePrefix;
	}
	public void setQueueNamePrefix(String queueNamePrefix) {
		this.queueNamePrefix = queueNamePrefix;
	}
	public int getContentSize() {
		return contentSize;
	}
	public void setContentSize(int contentSize) {
		this.contentSize = contentSize;
	}
	public long getTimeout() {
		return timeout;
	}
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
}
