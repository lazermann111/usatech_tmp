package com.usatech.app.jms;

import java.io.Serializable;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageFormatException;
import javax.jms.MessageListener;
import javax.jms.MessageNotReadableException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;
import javax.naming.TimeLimitExceededException;

import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.ConfigurationException;

import simple.app.AppPropertiesConfiguration;
import simple.app.RetryPolicy;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.Initializer;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;
import simple.util.concurrent.RunOnGetFuture;

public abstract class JMSBase  {
	private static final Log log = Log.getLog();
	protected static String JNDI_PROPERTIES_FILE_PROPERTY = "com.usatech.app.jms.JNDIPropertiesFile";
	protected static String JNDI_PROPERTIES_FILE = "jms_jndi.properties";
	protected static String RETRY_COUNT_PROPERTY = "reattemptCount";
	protected static String UNGUARDED_RETRY_PROPERTY = "unguardedRetry";
	protected static String AVAILABLE_AFTER_PROPERTY = "availableAfter";
	protected static final String DLQ_KEY_PROPERTY = "deadLetterQueueKey";
	protected static final String RETRY_KEY_PROPERTY = "retryQueueKey";
	protected static final String MAX_BYTES_COPY_PROPERTY = "maxBytesCopy";
	protected static final String CLIENT_ID_CLASS_PROPERTY = "clientIdClass";
	protected static final String MAX_SESSIONS_PROPERTY = "maxSessions";
	protected static final String REUSE_MESSAGE_PROPERTY = "reuseMessageForRetry";
	protected static long maxBytesMessageCopyLength = 256 * 1024;
	protected static ConnectionFactory connectionFactory;
	protected static Context jndiContext;
	protected static String deadLetterQueueKey = "_DLQ";
	protected static String retryQueueKeyPrefix = "_RETRY.";
	protected static Object clientId;
	protected static int maxSessions;
	protected static boolean reuseMessageForRetry;

	protected static final Initializer<NamingException> initializer = new Initializer<NamingException>() {
		@Override
		protected void doInitialize() throws NamingException {
			String path = System.getProperty(JNDI_PROPERTIES_FILE_PROPERTY, JNDI_PROPERTIES_FILE);
			try {
				AppPropertiesConfiguration pc = new AppPropertiesConfiguration(path);
				jndiContext = new InitialContext(ConfigurationConverter.getProperties(pc));
				String retry = pc.getString(RETRY_KEY_PROPERTY);
				if(retry != null && (retry=retry.trim()).length() > 0)
					retryQueueKeyPrefix = retry;
				String dlq = pc.getString(DLQ_KEY_PROPERTY);
				if(dlq != null && (dlq=dlq.trim()).length() > 0)
					deadLetterQueueKey = dlq;
				maxBytesMessageCopyLength = pc.getLong(MAX_BYTES_COPY_PROPERTY, maxBytesMessageCopyLength);
				String clientIdClassName = pc.getString(CLIENT_ID_CLASS_PROPERTY);
				if(clientIdClassName != null) {
					try {
						clientId = Class.forName(clientIdClassName).newInstance();
					} catch(InstantiationException e) {
						log.warn("Could not instantiate an object of type '" + clientIdClassName + "'", e);
					} catch(IllegalAccessException e) {
						log.warn("Could not instantiate an object of type '" + clientIdClassName + "'", e);
					} catch(ClassNotFoundException e) {
						log.warn("Class '" + clientIdClassName + "' not found", e);
					}
				}
				maxSessions = pc.getInt(MAX_SESSIONS_PROPERTY, 0);
				reuseMessageForRetry = pc.getBoolean(REUSE_MESSAGE_PROPERTY, false);
			} catch(ConfigurationException e) {
				log.info("Could not load JMS JNDI Properties File '" + path + "': " + e.getMessage() + "; using default JNDI");
				jndiContext = new InitialContext();
			}
			connectionFactory = (ConnectionFactory)jndiContext.lookup("ConnectionFactory");
		}
		@Override
		protected void doReset() {
			// never called so do nothing
		}
	};
	protected static class ConnectionInfo {
		protected final String username;
		protected final String password;
		protected final List<ConnectionMaker> makers = new ArrayList<ConnectionMaker>();
		protected int makerIndex = 0;
		protected final ReentrantLock lock = new ReentrantLock();
		
		protected class ConnectionMaker extends RunOnGetFuture<Connection> implements Runnable  {
			protected final AtomicInteger sessionCount = new AtomicInteger(0);
			@Override
			protected Connection call(Object... params) throws JMSException, NamingException, InterruptedException {
				Connection conn;
				if(username == null || username.length() == 0)
					conn = connectionFactory.createConnection();
				else
					conn = connectionFactory.createConnection(username, password);
				if(clientId != null)
					conn.setClientID(clientId.toString());
				log.info("Created new JMS connection " + conn);
				conn.start();
				return conn;
			}
			@Override
			protected void resettingRunResult(Connection result) {
				try {
					log.info("Closing JMS connection " + result);
					result.stop();
					result.close();
				} catch(JMSException e) {
					log.info("Could not close connection; Continuing...", e);
				}
			}
			@Override
			protected void finalize() throws Throwable {
				reset();
				super.finalize();
			}
			@Override
			public void run() {
				if(sessionCount.decrementAndGet() == 0)
					reset();
			}
		}
		public ConnectionInfo(String username, String password) {
			this.username = username;
			this.password = password;
		}

		protected ConnectionMaker getAvailableMaker() {
			ConnectionMaker maker;
			lock.lock();
			try {
				if(makers.isEmpty()) {
					maker = new ConnectionMaker();
					maker.sessionCount.incrementAndGet();
					makers.add(maker);
					makerIndex = 0;
					return maker;
				} else {
					int max = getMaxSessions();
					if(max <= 0) {
						maker = makers.get(makers.size() - 1);
						maker.sessionCount.incrementAndGet();
						return maker;
					}
					int startIndex = makerIndex;
					do {
						maker = makers.get(makerIndex);
						if(maker.sessionCount.get() < max) {
							maker.sessionCount.incrementAndGet();
							return maker;
						}
						if(++makerIndex >= makers.size())
							makerIndex = 0;
					} while(makerIndex != startIndex) ;
					// all makers are full, create a new one
					log.debug("All current JMS connections are full; creating a new one");
					maker = new ConnectionMaker();
					maker.sessionCount.incrementAndGet();
					makers.add(maker);
					makerIndex = makers.size() - 1;
					return maker;
				}
			} finally {
				lock.unlock();
			}
		}
		public ExtendedSession createSession(boolean transacted) throws JMSException, NamingException, InterruptedException {
			initializer.initialize();
			ConnectionMaker maker = getAvailableMaker();
			Connection conn;
			boolean okay = false;
			try {
				conn = maker.get(30, TimeUnit.SECONDS);
				okay = true;
			} catch(ExecutionException e) {
				if(e.getCause() instanceof JMSException)
					throw (JMSException)e.getCause();
				else if(e.getCause() instanceof NamingException)
					throw (NamingException)e.getCause();
				else
					throw new UndeclaredThrowableException(e);
			} catch(TimeoutException e) {
				TimeLimitExceededException te = new TimeLimitExceededException("While getting JMS Connection");
				te.initCause(e);
				throw te;
			} finally {
				if(!okay)
					maker.sessionCount.decrementAndGet();
			}
			Session session = conn.createSession(transacted, Session.CLIENT_ACKNOWLEDGE);
			return new DelegatingSession(session, maker, conn.getClientID()) ;
			
		}
	}
	protected static class DelegatingConsumer implements MessageConsumer {
		protected final MessageConsumer consumer;
		protected final Session session;

		public DelegatingConsumer(MessageConsumer consumer, Session session) {
			this.consumer = consumer;
			this.session = session;
		}

		@Override
		public void close() throws JMSException {
			try {
				consumer.close();
			} finally {
				session.close();
			}
		}

		public MessageListener getMessageListener() throws JMSException {
			return consumer.getMessageListener();
		}

		public String getMessageSelector() throws JMSException {
			return consumer.getMessageSelector();
		}

		public Message receive() throws JMSException {
			return consumer.receive();
		}

		public Message receive(long timeout) throws JMSException {
			return consumer.receive(timeout);
		}

		public Message receiveNoWait() throws JMSException {
			return consumer.receiveNoWait();
		}

		public void setMessageListener(MessageListener listener) throws JMSException {
			consumer.setMessageListener(listener);
		}
	}

	protected static class DelegatingSession implements ExtendedSession {
		protected final Session session;
		protected final String clientId;
		protected final Runnable onClose;
		protected final AtomicBoolean closed = new AtomicBoolean(false);

		public DelegatingSession(Session session, Runnable onClose, String clientId) {
			this.session = session;
			this.clientId = clientId;
			this.onClose = onClose;
		}

		@Override
		public void close() throws JMSException {
			try {
				session.close();
			} finally {
				if(closed.compareAndSet(false, true))
					onClose.run();
			}
		}

		public void commit() throws JMSException {
			if(log.isDebugEnabled())
				log.debug("Committing JMS Session " + session);
			session.commit();
			if(log.isDebugEnabled())
				log.debug("Committed JMS Session " + session);
		}

		public QueueBrowser createBrowser(Queue queue, String messageSelector) throws JMSException {
			return session.createBrowser(queue, messageSelector);
		}

		public QueueBrowser createBrowser(Queue queue) throws JMSException {
			return session.createBrowser(queue);
		}

		public BytesMessage createBytesMessage() throws JMSException {
			return session.createBytesMessage();
		}

		public MessageConsumer createConsumer(Destination destination, String messageSelector,
				boolean NoLocal) throws JMSException {
			return session.createConsumer(destination, messageSelector, NoLocal);
		}

		public MessageConsumer createConsumer(Destination destination, String messageSelector)
				throws JMSException {
			return session.createConsumer(destination, messageSelector);
		}

		public MessageConsumer createConsumer(Destination destination) throws JMSException {
			return session.createConsumer(destination);
		}

		public TopicSubscriber createDurableSubscriber(Topic topic, String name,
				String messageSelector, boolean noLocal) throws JMSException {
			return session.createDurableSubscriber(topic, name, messageSelector, noLocal);
		}

		public TopicSubscriber createDurableSubscriber(Topic topic, String name)
				throws JMSException {
			return session.createDurableSubscriber(topic, name);
		}

		public MapMessage createMapMessage() throws JMSException {
			return session.createMapMessage();
		}

		public Message createMessage() throws JMSException {
			return session.createMessage();
		}

		public ObjectMessage createObjectMessage() throws JMSException {
			return session.createObjectMessage();
		}

		public ObjectMessage createObjectMessage(Serializable object) throws JMSException {
			return session.createObjectMessage(object);
		}

		public MessageProducer createProducer(Destination destination) throws JMSException {
			return session.createProducer(destination);
		}

		public Queue createQueue(String queueName) throws JMSException {
			return session.createQueue(queueName);
		}

		public StreamMessage createStreamMessage() throws JMSException {
			return session.createStreamMessage();
		}

		public TemporaryQueue createTemporaryQueue() throws JMSException {
			return session.createTemporaryQueue();
		}

		public TemporaryTopic createTemporaryTopic() throws JMSException {
			return session.createTemporaryTopic();
		}

		public TextMessage createTextMessage() throws JMSException {
			return session.createTextMessage();
		}

		public TextMessage createTextMessage(String text) throws JMSException {
			return session.createTextMessage(text);
		}

		public Topic createTopic(String topicName) throws JMSException {
			return session.createTopic(topicName);
		}

		public int getAcknowledgeMode() throws JMSException {
			return session.getAcknowledgeMode();
		}

		public MessageListener getMessageListener() throws JMSException {
			return session.getMessageListener();
		}

		public boolean getTransacted() throws JMSException {
			return session.getTransacted();
		}

		public void recover() throws JMSException {
			session.recover();
		}

		public void rollback() throws JMSException {
			if(log.isDebugEnabled())
				log.debug("Rolling back JMS Session " + session);
			session.rollback();
			if(log.isDebugEnabled())
				log.debug("Rolled back JMS Session " + session);
		}

		public void run() {
			session.run();
		}

		public void setMessageListener(MessageListener listener) throws JMSException {
			session.setMessageListener(listener);
		}

		public void unsubscribe(String name) throws JMSException {
			session.unsubscribe(name);
		}
		/**
		 * @see com.usatech.app.jms.ExtendedSession#getClientId()
		 */
		@Override
		public String getClientId() {
			return clientId;
		}
	}

	protected static Cache<String,ConnectionInfo,RuntimeException> connCache = new LockSegmentCache<String,ConnectionInfo,RuntimeException>() {
		@Override
		protected ConnectionInfo createValue(String key, Object... additionalInfo) {
			return new ConnectionInfo(key, (additionalInfo != null && additionalInfo.length > 0 ? ConvertUtils.getStringSafely(additionalInfo[0]) : null));
		}
		@Override
		protected boolean keyEquals(String key1, String key2) {
			return ConvertUtils.areEqual(key1, key2);
		}
		@Override
		protected boolean valueEquals(ConnectionInfo value1, ConnectionInfo value2) {
			return ConvertUtils.areEqual(value1, value2);
		}
	};

	protected static JMSException newJMSException(String reason, Exception cause) {
		JMSException e = new JMSException(reason);
		e.initCause(cause);
		e.setLinkedException(cause);
		return e;
	}
	public static ExtendedSession createSession(boolean transacted) throws JMSException, NamingException, InterruptedException {
		return createSession(null, null, transacted);
	}
	public static ExtendedSession createSession(String username, String password, boolean transacted) throws JMSException, NamingException, InterruptedException {
		ConnectionInfo info = connCache.getOrCreate(username, password);
		return info.createSession(transacted);

	}
	public static MessageConsumer createConsumer(String queueKey, String username, String password, boolean transacted) throws JMSException, NamingException, InterruptedException {
		Destination destination = (Destination)jndiContext.lookup(queueKey);
		ConnectionInfo info = connCache.getOrCreate(username, password);
		Session session = info.createSession(transacted);
		MessageConsumer consumer = session.createConsumer(destination);
		return new DelegatingConsumer(consumer, session);
	}
	public static Destination getDestinationFromJNDI(String queueKey, boolean multicast, Session session) throws JMSException, NamingException, InterruptedException {
		initializer.initialize();
		Destination destination;
		try {
			destination = (Destination)jndiContext.lookup(queueKey);
		} catch(NameNotFoundException nnfe) {
			log.info("Could not find queue or topic '" + queueKey + "'; Trying dynamic folder");
			destination = null;
		}
		if(destination == null) {
			// This should but doesn't work b/c org.apache.activemq.jndi.ReadOnlyContext wraps its result
			// which eliminates the "dynamic" behavior of dynamicTopics and dynamicQueue
			/*
			Context ctx = (Context)jndiContext.lookup(multicast ? "dynamicTopics" : "dynamicQueues");
			destination = (Destination)ctx.lookup(queueKey);
			*/
			// We create the Queue (single-cast) or Topic (multicast) ourselves
			// but the JMS API docs caution against this
			if(destination == null) {
				if(multicast)
					destination = session.createTopic(queueKey);
				else
					destination = session.createQueue(queueKey);
			}
		}
		return destination;
	}
	public static Destination getDestination(String queueKey, boolean multicast, Session session) throws JMSException {
		// We create the Queue (single-cast) or Topic (multicast) ourselves
		// but the JMS API docs caution against this
		if(multicast)
			return session.createTopic(queueKey);
		else
			return session.createQueue(queueKey);
	}
	public static Destination getTemporaryDestination(boolean multicast, Session session) throws JMSException {
		// We create the Queue (single-cast) or Topic (multicast) ourselves
		// but the JMS API docs caution against this
		if(multicast)
			return session.createTemporaryTopic();
		else
			return session.createTemporaryQueue();
	}
	public static int getRetryCount(Message msg) throws JMSException {
		try {
			return msg.getIntProperty(RETRY_COUNT_PROPERTY);
		} catch(NumberFormatException e) {
			return 0;
		} catch(MessageFormatException e) {
			return 0;
		}
	}
	public static boolean getUnguardedRetry(Message msg) throws JMSException {
		try {
			return msg.getBooleanProperty(UNGUARDED_RETRY_PROPERTY);
		} catch(MessageFormatException e) {
			return false;
		}
	}
	public static long getAvailableAfter(Message msg) throws JMSException {
		try {
			return msg.getLongProperty(AVAILABLE_AFTER_PROPERTY);
		} catch(NumberFormatException e) {
			return 0;
		} catch(MessageFormatException e) {
			return 0;
		}
	}
	public static void incrementRetryCount(Message msg) throws JMSException {
		setRetryCount(msg, getRetryCount(msg) + 1);
	}
	protected static void setRetryCount(Message msg, int retryCount) throws JMSException {
		msg.setIntProperty(RETRY_COUNT_PROPERTY, retryCount);
	}
	protected static void setUnguardedRetry(Message msg, boolean unguardedRetry) throws JMSException {
		msg.setBooleanProperty(UNGUARDED_RETRY_PROPERTY, unguardedRetry);
	}
	public static void handleRetry(Message msg, MessageProducer producer, Session session, RetryPolicy retryPolicy, boolean unguardedRetryAllowed) throws JMSException {
		if(reuseMessageForRetry)
			handleRetryByReuse(msg, producer, session, retryPolicy, unguardedRetryAllowed);
		else
			handleRetryByCopy(msg, producer, session, retryPolicy, unguardedRetryAllowed);
	}
	
	protected static void handleRetryByReuse(Message msg, MessageProducer producer, Session session, RetryPolicy retryPolicy, boolean unguardedRetryAllowed) throws JMSException {
		long ttl;
		if(msg.getJMSExpiration() > 0) {
			ttl = msg.getJMSExpiration() - System.currentTimeMillis();
			if(ttl <= 0) {
				log.info("Message " + getMessageId(msg) + " has expired; not enqueuing in JMS");
				return;
			}
		} else {
			ttl = 0;
		}
		if(retryPolicy == null) {
			msg.setJMSReplyTo(msg.getJMSDestination());
			msg.setJMSDestination(getDeadLetterQueue(session));
			producer.send(msg.getJMSDestination(), msg);
			session.commit();
		} else {
			int reattemptCount = getRetryCount(msg);
			makePropertiesWritable(msg);
			setRetryCount(msg, reattemptCount + 1);
			setUnguardedRetry(msg, unguardedRetryAllowed);
			if(reattemptCount >= retryPolicy.getMaximumRedeliveries()) {
				msg.setJMSReplyTo(msg.getJMSDestination());
				msg.setJMSDestination(getDeadLetterQueue(session));
			} else if(reattemptCount > 0) { // on the first reattempt just put the message into the same queue at the back
				msg.setJMSReplyTo(msg.getJMSDestination());
				msg.setJMSDestination(getRetryQueue(session, reattemptCount));
				msg.setLongProperty(AVAILABLE_AFTER_PROPERTY, System.currentTimeMillis() + retryPolicy.calculateDelay(reattemptCount));
			}
			producer.send(msg.getJMSDestination(), msg, msg.getJMSDeliveryMode(), msg.getJMSPriority() > 0 ? msg.getJMSPriority() - 1 : 0, ttl);
			session.commit();
		}
	}

	public static Message copyMessage(Message originalMsg, Session session) throws JMSException {
		Message msg;
		if(originalMsg instanceof StreamMessage) {
			StreamMessage originalStreamMsg = (StreamMessage)originalMsg;
			StreamMessage streamMsg = session.createStreamMessage();
			byte[] b = new byte[512];
	        int tot = 0;
	        originalStreamMsg.reset();
	        for(int len = 0; (len=originalStreamMsg.readBytes(b)) >= 0; tot += len) streamMsg.writeBytes(b, 0, len);
	        msg = streamMsg;
		} else if(originalMsg instanceof BytesMessage) {
			BytesMessage originalBytesMsg = (BytesMessage)originalMsg;
			BytesMessage bytesMsg = session.createBytesMessage();
			byte[] b = new byte[512];
	        int tot = 0;
	        originalBytesMsg.reset();
	        for(int len = 0; (len=originalBytesMsg.readBytes(b)) >= 0; tot += len) bytesMsg.writeBytes(b, 0, len);
	        msg = bytesMsg;
		} else if(originalMsg instanceof TextMessage) {
			TextMessage originalTextMsg = (TextMessage)originalMsg;
			TextMessage textMsg = session.createTextMessage();
			textMsg.setText(originalTextMsg.getText());
	        msg = textMsg;
		} else {
			throw new JMSException("JMS Message Type " + JMSBase.getMessageType(originalMsg) + " not supported on message ID " + JMSBase.getMessageId(originalMsg));
		}
		for(Enumeration<?> en = originalMsg.getPropertyNames(); en.hasMoreElements(); ) {
			String name = (String)en.nextElement();
			msg.setObjectProperty(name, originalMsg.getObjectProperty(name));
		}
		return msg;
	}
	protected static void handleRetryByCopy(Message originalMsg, MessageProducer producer, Session session, RetryPolicy retryPolicy, boolean unguardedRetryAllowed) throws JMSException {
		long ttl;
		if(originalMsg.getJMSExpiration() > 0) {
			ttl = originalMsg.getJMSExpiration() - System.currentTimeMillis();
			if(ttl <= 0) {
				log.info("Message " + getMessageId(originalMsg) + " has expired; not enqueuing in JMS");
				return;
			}
		} else {
			ttl = 0;
		}
		
		Message msg = copyMessage(originalMsg, session);
		msg.setJMSReplyTo(originalMsg.getJMSDestination());
		if(retryPolicy == null) {
			msg.setJMSDestination(getDeadLetterQueue(session));
			producer.send(msg.getJMSDestination(), msg);
			session.commit();
		} else {
			int reattemptCount = getRetryCount(originalMsg);
			setRetryCount(msg, reattemptCount + 1);
			setUnguardedRetry(msg, unguardedRetryAllowed);
			if(reattemptCount >= retryPolicy.getMaximumRedeliveries()) {
				msg.setJMSDestination(getDeadLetterQueue(session));
			} else if(reattemptCount > 0) { // on the first reattempt just put the message into the same queue at the back
				msg.setJMSDestination(getRetryQueue(session, reattemptCount));
				msg.setLongProperty(AVAILABLE_AFTER_PROPERTY, System.currentTimeMillis() + retryPolicy.calculateDelay(reattemptCount));
			} else {
				msg.setJMSDestination(originalMsg.getJMSDestination());
			}
			producer.send(msg.getJMSDestination(), msg, originalMsg.getJMSDeliveryMode(), originalMsg.getJMSPriority() > 0 ? originalMsg.getJMSPriority() - 1 : 0, ttl);
			session.commit();
		}
	}
	public static long prepareNextAvailable(Message msg, MessageProducer producer, Session session, RetryPolicy retryPolicy) throws JMSException {
		int reattemptCount = getRetryCount(msg);
		makePropertiesWritable(msg);
		setRetryCount(msg, reattemptCount + 1);
		if(reattemptCount >= retryPolicy.getMaximumRedeliveries()) {
			if(producer != null) {
				msg.setJMSReplyTo(msg.getJMSDestination());
				msg.setJMSDestination(JMSBase.getDeadLetterQueue(session));
				producer.send(msg.getJMSDestination(), msg, msg.getJMSDeliveryMode(), msg.getJMSPriority() > 0 ? msg.getJMSPriority() - 1 : 0, 0);
				session.commit();
			}
			makeReadable(msg);
			return Long.MAX_VALUE; // never available
		} else {
			makeReadable(msg);
			return System.currentTimeMillis() + retryPolicy.calculateDelay(reattemptCount);
		}
	}
	public static boolean resurrectMessage(Message msg, MessageProducer producer) throws JMSException {
		Destination originalDest = msg.getJMSReplyTo();
		if(originalDest == null || originalDest.equals(msg.getJMSDestination())) {
			return false;
		}
		makePropertiesWritable(msg);
		setRetryCount(msg, 0);
		msg.setJMSDestination(originalDest);
		producer.send(msg.getJMSDestination(), msg, msg.getJMSDeliveryMode(), msg.getJMSPriority() > 0 ? msg.getJMSPriority() - 1 : 0, 0);
		return true;
	}
	/**
	 * @param msg
	 */
	protected static void makeReadable(Message msg) {
		if(msg instanceof BytesMessage)
			try {
				((BytesMessage)msg).reset();
			} catch(JMSException e) {
				log.warn("Could not make message readable", e);
			}
	}
	protected static void makePropertiesWritable(Message message) throws JMSException {
		if(message instanceof BytesMessage) {
			// Unfortunately, BytesMessages do not separate their properties from their body (at least for ActiveMQ) so we must do this
			BytesMessage bm = (BytesMessage)message;
			try {
				long length = bm.getBodyLength();
				if(length > maxBytesMessageCopyLength)
					throw new JMSException("BytesMessage is too large to retry. Length " + length + " is greater than maximum of " + maxBytesMessageCopyLength);
				byte[] bytes = new byte[(int)length];
				bm.reset();
				bm.readBytes(bytes);
				bm.clearBody();
				bm.writeBytes(bytes);
			} catch(MessageNotReadableException e) {
				log.debug("Message is already writeable");
			}
		}
		message.clearProperties();
	}
	public static Queue getDeadLetterQueue(Session session) throws JMSException {
		return session.createQueue(deadLetterQueueKey);
	}
	protected static Queue getRetryQueue(Session session, int reattemptCount) throws JMSException {
		return session.createQueue(retryQueueKeyPrefix + reattemptCount);
	}
	public static int getMaxSessions() {
		return maxSessions;
	}
	public static void setMaxSessions(int maxSessions) {
		JMSBase.maxSessions = maxSessions;
	}
	public static String getMessageId(Message msg) {
		try {
			return msg.getJMSMessageID();
		} catch(JMSException e) {
			return "<UNKNOWN>";
		}
	}
	public static String getMessageInfo(Message msg) {
		try {
			return msg.getJMSMessageID() + " on " + msg.getJMSDestination();
		} catch(JMSException e) {
			return "<UNKNOWN>";
		}
	}
	public static int getRetryCountSafely(Message msg) {
		try {
			return getRetryCount(msg);
		} catch(JMSException e) {
			log.warn(e);
			return -1;
		}
	}
	public static boolean getUnguardedRetrySafely(Message msg) {
		try {
			return getUnguardedRetry(msg);
		} catch(JMSException e) {
			log.warn(e);
			return false; // err on the side of caution
		}
	}
	public static long getAvailableAfterSafely(Message msg) {
		try {
			return getAvailableAfter(msg);
		} catch(JMSException e) {
			log.warn(e);
			return -1;
		}
	}
	public static boolean getRedeliveredSafely(Message msg) {
		try {
			return msg.getJMSRedelivered();
		} catch(JMSException e) {
			log.warn(e);
			return true; // err on the side of caution
		}
	}
	public static String getMessageType(Message msg) {
		try {
			return msg.getJMSType();
		} catch(JMSException e) {
			return "<UNKNOWN>";
		}
	}
}
