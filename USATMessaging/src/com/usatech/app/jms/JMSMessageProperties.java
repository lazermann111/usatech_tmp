package com.usatech.app.jms;

import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.jms.JMSException;
import javax.jms.Message;

public class JMSMessageProperties extends AbstractMap<String, Object> {
	protected final Message msg;
	protected final Set<String> keySet;
	protected final Set<Entry<String, Object>> entrySet = new AbstractSet<Entry<String, Object>>() {
		@Override
		public Iterator<Entry<String, Object>> iterator() {
			final Iterator<String> keyIter = keySet().iterator();
			return new Iterator<Entry<String, Object>>() {
				protected String key;
				public boolean hasNext() {
					return keyIter.hasNext();
				}
				public Entry<String, Object> next() {
					key = keyIter.next();
					return new Entry<String, Object>() {
						public String getKey() {
							return key;
						}
						public Object getValue() {
							return get(key);
						}
						public Object setValue(Object value) {
							return put(key, value);					
						}						
					};
				}
				public void remove() {
					JMSMessageProperties.this.remove(key);					
				}
			};
		}

		@Override
		public int size() {
			return keySet().size();
		}		
	};
	protected final Collection<Object> values = new AbstractCollection<Object>() {
		@Override
		public Iterator<Object> iterator() {
			final Iterator<String> keyIter = keySet().iterator();
			return new Iterator<Object>() {
				protected String key;
				public boolean hasNext() {
					return keyIter.hasNext();
				}
				public Object next() {
					key = keyIter.next();
					return get(key);
				}
				public void remove() {
					JMSMessageProperties.this.remove(key);					
				}
			};
		}

		@Override
		public int size() {
			return keySet().size();
		}		
	};
	
	public JMSMessageProperties(Message msg) throws JMSException {
		this.msg = msg;
		keySet = new HashSet<String>();
		for(Enumeration<?> en = msg.getPropertyNames(); en.hasMoreElements(); )
			keySet.add((String)en.nextElement());

	}
	@Override
	public Set<String> keySet() {
		return keySet;
	}
	public void clear() {
		try {
			msg.clearProperties();
		} catch(JMSException e) {
			throw new UnsupportedOperationException("JMS Exception", e);
		}
		keySet().clear();
	}
	public boolean containsKey(Object key) {
		return keySet().contains(key);
	}
	public boolean containsValue(Object value) {
		return values().contains(value);
	}
	public Set<Entry<String, Object>> entrySet() {
		return entrySet;
	}
	public boolean isEmpty() {
		return size() == 0;
	}
	@Override
	public Object get(Object key) {
		if(key instanceof String)
			try {
				return msg.getObjectProperty((String)key);
			} catch(JMSException e) {
				throw new IllegalStateException("JMS Exception", e);
			}
		else
			return null;
	}
	public Object put(String key, Object value) {
		Object old;
		try {
			old = msg.getObjectProperty(key);
			msg.setObjectProperty(key, value);
		} catch(JMSException e) {
			throw new UnsupportedOperationException("JMS Exception", e);
		}
		return old;
	}
	
	public Object remove(Object key) {
		if(keySet.remove(key)) {
			return put((String)key, null);
		} else
			return null;
	}
	public int size() {
		return keySet().size();
	}
	public Collection<Object> values() {
		return values;
	}	
}
