package com.usatech.app.jms;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.naming.NamingException;

import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.event.ProgressListener;
import simple.io.Log;
import simple.util.concurrent.RunOnGetFuture;

public abstract class AbstractJMSWorkQueue<J> extends AbstractJMSWorkBase<J> {
	private static final Log log = Log.getLog();
	protected boolean temporary = false;
	protected ExtendedSession initSession;
	protected boolean blockingRetryAllowed;

	protected RunOnGetFuture<Destination> destinationFuture = new RunOnGetFuture<Destination>() {
		@Override
		protected Destination call(Object... params) throws Exception {
			if(isTemporary()) {
				boolean closeSession = false;
				ExtendedSession session = initSession;
				if(session == null) {
					closeSession = true;
					session = JMSBase.createSession(true);
				}
				Destination dest;
				try {
					dest = createTemporaryDestination(session);
				} finally {
					if(closeSession)
						try {
							session.close();
						} catch(JMSException e) {
							log.debug("While closing session: " + e.getMessage());
						}
				}
				queueKey = getQueueName(dest);
				return dest;
			} else {
				return createDestination(initSession);
			}
		}
	};
	protected class JMSRetriever extends AbstractJMSRetriever {
		protected volatile Message nextMsg;
		protected long availableAfter;
		protected final ReentrantLock lock = new ReentrantLock();
		protected Condition signal;
		
		public JMSRetriever(Session session, MessageConsumer consumer) {
			super(session, consumer);
		}
		@Override
		protected Message nextMessage() throws WorkQueueException, InterruptedException {
			Message msg;
			if(nextMsg != null) {
				long timeout;
				while(availableAfter > 0 && (timeout=availableAfter-System.currentTimeMillis()) > 0) {
					lock.lock();
					try {
						if(signal == null)
							signal = lock.newCondition();
						signal.await(timeout, TimeUnit.MILLISECONDS);
					} finally {
						lock.unlock();
					}
				}
				msg = nextMsg;
				nextMsg = null;
			} else {
				try {
					msg = consumer.receive();
				} catch(JMSException e) {
					if(e.getCause() instanceof InterruptedException)
						throw (InterruptedException)e.getCause();
					throw new WorkQueueException("Could not receive message", e);
				}
			}
			return msg;
		}
		@Override
		protected void handleWorkAborted(Message msg, String desc, WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
			switch(retryType) {
				case IMMEDIATE_RETRY:
					try {
						JMSBase.incrementRetryCount(msg);
					} catch(JMSException e) {
						throw new WorkQueueException("Could not increment retry count on " + desc, e);
					}
					availableAfter = 0;
					nextMsg = msg;
					break;
				case NO_RETRY:
					try {
						JMSBase.handleRetry(msg, getProducer(), session, null, unguardedRetryAllowed);
					} catch(JMSException e) {
						throw new WorkQueueException("Could not republish " + desc, e);
					}
					break;
				case BLOCKING_RETRY:
					//Re-check prerequisites
					//XXX: In the future, we may allow the exception to hold info on which prerequisite is unavailable
					prerequisiteManager.resetAvailability();
					if(isBlockingRetryAllowed()) {
						try {
							long aa = JMSBase.prepareNextAvailable(msg, getProducer(), session, getRetryPolicy());
							if(aa < Long.MAX_VALUE) {
								availableAfter = aa;
								nextMsg = msg;
							}
						} catch(JMSException e) {
							throw new WorkQueueException("Could not republish " + desc, e);
						}
						break;
					}
					log.debug("Blocking retry is not allowed; Re-checking prerequisites and using non-blocking retry");
				case NONBLOCKING_RETRY:
					// The session.rollback() method does NOT do what we want! It stops receiving other messages until
					// the redelivery delay and then tries to process the same message again!
					try {
						JMSBase.handleRetry(msg, getProducer(), session, getRetryPolicy(), unguardedRetryAllowed);
					} catch(JMSException e) {
						throw new WorkQueueException("Could not republish " + desc, e);
					}
					break;
			}
		}
		@Override
		protected void handleWorkComplete(Message msg, String desc) throws WorkQueueException {
			long start;
			if(log.isInfoEnabled())
				start = System.currentTimeMillis();
			else
				start = 0;
			try {
				session.commit();
			} catch(JMSException e) {
				throw new WorkQueueException("Could not commit " + desc, e);
			}
			if(log.isInfoEnabled()) {
				long committed = System.currentTimeMillis();
				log.info("Committed " + desc + " [in " + (committed-start) + " ms]");	
			}
		}

		public boolean cancel(Thread thread) {
			if(nextMsg != null) {
				availableAfter = 0;
				nextMsg = null;
				lock.lock();
				try {
					if(signal != null)
						signal.signalAll();
				} finally {
					lock.unlock();
				}
			}
			try {
				consumer.close();
			} catch(JMSException e) {
				//ignore
				return false;
			}
			return true;
		}
		public void close() {
			nextMsg = null;
			super.close();
		}
	};
	protected Destination createDestination(ExtendedSession session) throws JMSException {
		String qk = getQueueKey();
		if(qk == null)
			throw new JMSException("QueueKey is not set");
		return JMSBase.getDestination(qk, false, session);
	}
	protected Destination createTemporaryDestination(ExtendedSession session) throws JMSException {
		return JMSBase.getTemporaryDestination(false, session);
	}

	public boolean isAutonomous() {
		return false;
	}

	@Override
	public String getQueueKey() {
		if(isTemporary())
			try {
				destinationFuture.get();
			} catch(InterruptedException e) {
				log.warn("Could not get destination", e);
			} catch(ExecutionException e) {
				log.warn("Could not get destination", e);
			}
		return queueKey;
	}

	@Override
	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
		this.destinationFuture.reset();
	}

	public WorkQueue.Retriever<J> getRetriever(ProgressListener listener) throws WorkQueueException {
		try {
			ExtendedSession session = JMSBase.createSession(getUsername(), getPassword(), true);
			initSession = session;
			Destination dest = destinationFuture.get();
			JMSRetriever retriever = new JMSRetriever(session, session.createConsumer(dest));
			if(log.isDebugEnabled())
				log.debug("Listening for messages on jms destination '" + dest + "'");
			return retriever;
		} catch(JMSException e) {
			throw new WorkQueueException("Could not create JMS Session or Consumer", e);
		} catch(NamingException e) {
			throw new WorkQueueException("Could not lookup ConnectionFactory or Destination", e);
		} catch(InterruptedException e) {
			throw new WorkQueueException("While initializaling", e);
		} catch(ExecutionException e) {
			throw new WorkQueueException("Could not create destination", e);
		}
	}
	public boolean isTemporary() {
		return temporary;
	}
	public void setTemporary(boolean temporary) {
		this.temporary = temporary;
		this.destinationFuture.reset();
	}
	public boolean isBlockingRetryAllowed() {
		return blockingRetryAllowed;
	}
	public void setBlockingRetryAllowed(boolean blockingRetryAllowed) {
		this.blockingRetryAllowed = blockingRetryAllowed;
	}
}
