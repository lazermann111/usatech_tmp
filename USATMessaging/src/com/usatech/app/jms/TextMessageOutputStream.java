/**
 * 
 */
package com.usatech.app.jms;

import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

import simple.text.StringUtils;

public class TextMessageOutputStream extends MessageOutputStream<TextMessage> {
	protected final StringBuilder sb = new StringBuilder();
	public TextMessageOutputStream(Session session) throws JMSException {
		super(session.createTextMessage());
	}
	@Override
	public void write(int b) throws IOException {
		StringUtils.appendHex(sb, (byte)b);
		try {
			message.setText(sb.toString());
		} catch(JMSException e) {
			throw newIOException("Could not write to message", e);
		}
	}
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		StringUtils.appendHex(sb, b, off, len);
		try {
			message.setText(sb.toString());
		} catch(JMSException e) {
			throw newIOException("Could not write to message", e);
		}
	}
	@Override
	public void clear() throws JMSException {
		sb.setLength(0);
		super.clear();
	}
}