package com.usatech.app.jms;

import java.io.Closeable;
import java.io.IOException;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;

import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.io.Log;


public abstract class AbstractJMSPublisher<Q> implements Publisher<Q>, Closeable {
	private static final Log log = Log.getLog();
	protected MessageProducer producer;
	protected Class<? extends Message> messageType = BytesMessage.class;

	protected MessageProducer initProducer(Session session) throws JMSException {
		if(producer == null) {
			log.debug("Creating JMS producer");
			producer = session.createProducer(null);
			if(log.isInfoEnabled())
				log.info("Created JMS producer " + producer);
		}
		return producer;
	}
	
	public void publish(String queueKey, Q content) throws simple.app.ServiceException {
		publish(queueKey, false, false, content, null, null);
	}
	public void publish(String queueKey, boolean multicast, boolean temporary, Q content) throws simple.app.ServiceException {
		publish(queueKey, multicast, temporary, content, null, null);
	}

	protected void sendMessage(Session session, String queueKey, boolean multicast, boolean temporary, Q content, QoS qos, String... blocks) throws JMSException {
		MessageProducer producer = initProducer(session);
		long prep;
		if(log.isInfoEnabled())
			prep = System.currentTimeMillis();
		else
			prep = 0;
		Destination dest = JMSBase.getDestination(queueKey, multicast, session);
		int deliverMode;
		int priority;
		long ttl;
		if(qos == null) {
			deliverMode = temporary ? DeliveryMode.NON_PERSISTENT : DeliveryMode.PERSISTENT;
			priority = Message.DEFAULT_PRIORITY;
			ttl = Message.DEFAULT_TIME_TO_LIVE;
		} else if(qos.getAvailable() > 0 && qos.getAvailable() > System.currentTimeMillis()) {
			throw new JMSException("Delayed execute is not supported");
		} else {
			deliverMode = (qos.isPersistent() && !temporary ? DeliveryMode.PERSISTENT : DeliveryMode.NON_PERSISTENT);
			if(qos.getPriority() > 0)
				priority = qos.getPriority();
			else
				priority = Message.DEFAULT_PRIORITY;
			if(qos.getTimeToLive() > 0)
				ttl = qos.getTimeToLive();
			else if(qos.getExpiration() > 0) {
				if(prep == 0)
					prep = System.currentTimeMillis();
				ttl = qos.getExpiration() - prep;
				if(ttl <= 0) { // message expired
					if(log.isInfoEnabled())
						log.info("JMS Message expired before it was sent for " + dest);
					return; // do no further processing
				}
			} else
				ttl = Message.DEFAULT_TIME_TO_LIVE;
		}
		MessageOutputStream<?> out;
		if(getMessageType() == BytesMessage.class) {
			out = new BytesMessageOutputStream(session);
		} else if(getMessageType() == TextMessage.class) {
			out = new TextMessageOutputStream(session);
		} else if(getMessageType() == StreamMessage.class) {
			out = new StreamMessageOutputStream(session);
		} else {
			throw new JMSException("Unsupported messageType " + getMessageType());
		}
		Message msg = out.getMessage();
		Map<String,?> messageProperties;
		if(qos != null && (messageProperties=qos.getMessageProperties()) != null)
			for(Map.Entry<String, ?> entry : messageProperties.entrySet()) {
				msg.setObjectProperty(entry.getKey(), entry.getValue());
			}
		try {
			writeContent(out, content);
		} catch(IOException e) {
			JMSException jmse;
			if(e.getCause() instanceof JMSException)
				jmse = (JMSException)e.getCause();
			else {
				jmse = new JMSException("Could not write content to JMS Message");
				jmse.initCause(e);
			}
			throw jmse;	
		}
		long start;
		if(log.isInfoEnabled())
			start = System.currentTimeMillis();
		else
			start = 0;
		producer.send(dest, msg, deliverMode, priority, ttl);
		if(log.isInfoEnabled()) {
			long sent = System.currentTimeMillis();
			log.info("Published JMS Message " + msg.getJMSMessageID() + " on " + dest + " [in " + (sent-start) + " ms; prepared in " + (start-prep) + " ms]");	
		}
	}

	protected abstract void writeContent(MessageOutputStream<?> out, Q content) throws IOException ;
	
	public void close() {
		if(producer != null)
			try {
				producer.close();
			} catch(JMSException e) {
				//ignore
			}
		producer = null;
	}

	public Class<? extends Message> getMessageType() {
		return messageType;
	}

	public void setMessageType(Class<? extends Message> messageType) {
		this.messageType = messageType;
	}

	@Override
	public int getConsumerCount(String queueName, boolean temporary) {
		return 1;
	}

	public String[] getSubscriptions(String queueKey) throws ServiceException {
		return null;
	}

	@Override
	public void block(String... blocks) throws ServiceException {
		throw new ServiceException("Blocking is not supported in this publisher implementation");
	}
}
