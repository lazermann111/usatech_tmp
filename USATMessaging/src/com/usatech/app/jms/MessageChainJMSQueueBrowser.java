/**
 *
 */
package com.usatech.app.jms;

import java.util.Enumeration;
import java.util.Iterator;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.Initializer;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;

/**
 * @author Brian S. Krug
 *
 */
public class MessageChainJMSQueueBrowser implements Iterable<MessageChain> {
	private static final Log log = Log.getLog();

	protected static class QueueIterator implements Iterator<MessageChain> {
		protected final Enumeration<?> browserEnumeration;
		protected final String queueKey;

		public QueueIterator(QueueBrowser browser) throws JMSException {
			super();
			this.browserEnumeration = browser.getEnumeration();
			this.queueKey = browser.getQueue().getQueueName();
		}
		/**
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return browserEnumeration.hasMoreElements();
		}

		/**
		 * @see java.util.Iterator#next()
		 */
		@Override
		public MessageChain next() {
			Message message = (Message)browserEnumeration.nextElement();
			ByteInput bi;
			if(message instanceof BytesMessage) {
				bi = new JMSByteMessageByteInput((BytesMessage)message);
			} else if(message instanceof StreamMessage) {
				bi = new JMSStreamMessageByteInput((StreamMessage)message);
			} else if(message instanceof TextMessage) {
				try {
					bi = new JMSTextHexMessageByteInput((TextMessage)message);
				} catch(JMSException e) {
					throw new RuntimeException("Could not get content of JMS Text Message", e);
				}
			} else if(message == null) {
				return null;
			} else {
				throw new RuntimeException("JMS Message Type " + JMSBase.getMessageType(message) + " not supported on message ID " + JMSBase.getMessageId(message));
			}

			try {
				return MessageChainService.deserializeMessageChain_v11(bi, queueKey, false);
			} catch(ServiceException e) {
				throw new RuntimeException("Could not deserialize bytes", e);
			}
		}

		/**
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	protected String queueKey;
	protected QueueBrowser queueBrowser;
	protected ExtendedSession session;
	protected final Initializer<ServiceException> initializer = new Initializer<ServiceException>() {
		/**
		 * @see simple.lang.Initializer#doInitialize()
		 */
		@Override
		protected void doInitialize() throws ServiceException {
			String qk = getQueueKey();
			if(qk == null)
				throw new ServiceException("QueueKey is not set");
			try {
				session = JMSBase.createSession(false);
			} catch(NamingException e) {
				throw new ServiceException(e);
			} catch(InterruptedException e) {
				throw new ServiceException(e);
			} catch(JMSException e) {
				throw new ServiceException(e);
			}
			try {
				queueBrowser = session.createBrowser((Queue)JMSBase.getDestination(qk, false, session));
			} catch(JMSException e) {
				throw new ServiceException(e);
			}
		}
		/**
		 * @see simple.lang.Initializer#doReset()
		 */
		@Override
		protected void doReset() {
			if(queueBrowser != null) {
				try {
					queueBrowser.close();
				} catch(JMSException e) {
					log.debug("Could not close browser", e);
				}
				queueBrowser = null;
			}
			if(session != null) {
				try {
					session.close();
				} catch(JMSException e) {
					log.warn("Could not close session", e);
				}
				session = null;
			}
		}
	};
	/**
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<MessageChain> iterator() {
		try {
			initializer.initialize();
		} catch(InterruptedException e) {
			throw new RuntimeException(e);
		} catch(ServiceException e) {
			throw new RuntimeException(e);
		}
		try {
			return new QueueIterator(queueBrowser);
		} catch(JMSException e) {
			throw new RuntimeException(e);
		}
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}
}
