package com.usatech.app.jms;

import java.io.IOException;

import simple.io.ByteInput;


/** Implementation of Publisher for JMS system. NOT thread-safe.
 * @author Brian S. Krug
 *
 */
public class JMSPublisher extends StandaloneJMSPublisher<ByteInput> {
	protected boolean transacted;

	@Override
	protected void writeContent(MessageOutputStream<?> out, ByteInput content) throws IOException {
		content.copyInto(out);
	}
	@Override
	public boolean isTransacted() {
		return transacted;
	}

	public void setTransacted(boolean transacted) {
		this.transacted = transacted;
		close(); //XXX: should we commit here?
	}
}
