/**
 *
 */
package com.usatech.app.jms;

import javax.jms.Session;

/**
 * @author Brian S. Krug
 *
 */
public interface ExtendedSession extends Session {
	public String getClientId() ;
}
