package com.usatech.app.jms;

import java.util.Map;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;

import simple.app.QoS;

public class JMSQoS implements QoS {
	protected final Message msg;
	
	public JMSQoS(Message msg) {
		this.msg = msg;
	}

	@Override
	public long getTimeToLive() {
		return 0;
	}
	
	@Override
	public long getExpiration() {
		try {
			return msg.getJMSExpiration();
		} catch(JMSException e) {
			return 0;
		}
	}

	@Override
	public Map<String, ?> getMessageProperties() {
		try {
			return new JMSMessageProperties(msg);
		} catch(JMSException e) {
			return null;
		}
	}

	@Override
	public int getPriority() {
		try {
			return msg.getJMSPriority() + 1;
		} catch(JMSException e) {
			return 0;
		}
	}

	@Override
	public boolean isPersistent() {
		try {
			return msg.getJMSDeliveryMode() != DeliveryMode.NON_PERSISTENT;
		} catch(JMSException e) {
			return true;
		}
	}

	@Override
	public long getAvailable() {
		return 0;
	}
}
