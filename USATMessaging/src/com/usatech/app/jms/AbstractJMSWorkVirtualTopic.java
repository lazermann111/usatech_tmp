/**
 *
 */
package com.usatech.app.jms;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Destination;
import javax.jms.JMSException;

import simple.lang.SystemUtils;
import simple.text.MessageFormat;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractJMSWorkVirtualTopic<J> extends AbstractJMSWorkQueue<J> {
	protected MessageFormat consumerKeyFormat;

	/**
	 * @see com.usatech.app.jms.AbstractJMSWorkQueue#createDestination(com.usatech.app.jms.ExtendedSession)
	 */
	@Override
	protected Destination createDestination(ExtendedSession session) throws JMSException {
		String qk = getQueueKey();
		if(qk == null)
			throw new JMSException("QueueKey is not set");
		MessageFormat mf = consumerKeyFormat;
		if(mf == null)
			throw new JMSException("QueueKeyFormat is not set");
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("systemInfo", SystemUtils.getSystemInfo());
		params.put("queueKey", qk);
		String consumerKey = mf.format(params);
		return JMSBase.getDestination(consumerKey, false, session);
	}
	public MessageFormat getConsumerKeyFormat() {
		return consumerKeyFormat;
	}

	public void setConsumerKeyFormat(MessageFormat consumerKeyFormat) {
		this.consumerKeyFormat = consumerKeyFormat;
	}
}
