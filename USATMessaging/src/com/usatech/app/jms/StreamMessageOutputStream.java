/**
 * 
 */
package com.usatech.app.jms;

import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.StreamMessage;

public class StreamMessageOutputStream extends MessageOutputStream<StreamMessage> {
	public StreamMessageOutputStream(Session session) throws JMSException {
		super(session.createStreamMessage());
	}
	@Override
	public void write(int b) throws IOException {
		try {
			message.writeByte((byte)b);
		} catch(JMSException e) {
			throw newIOException("Could not write to message", e);
		}
	}
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		try {
			message.writeBytes(b, off, len);
		} catch(JMSException e) {
			throw newIOException("Could not write to message", e);
		}
	}
	@Override
	public void write(byte[] b) throws IOException {
		try {
			message.writeBytes(b);
		} catch(JMSException e) {
			throw newIOException("Could not write to message", e);
		}
	}
}