package com.usatech.app.jms;

import java.io.IOException;
import java.io.OutputStream;


import simple.io.ByteInput;

public class JMSSubscription extends AbstractJMSSubscription<ByteInput> {
	/**
	 * @see com.usatech.app.jms.AbstractJMSWorkQueue#createBody(simple.io.ByteInput)
	 */
	@Override
	protected ByteInput createBody(ByteInput bi) {
		return bi;
	}
	/**
	 * @see com.usatech.app.jms.AbstractJMSWorkQueue#writeBody(java.io.OutputStream, java.lang.Object)
	 */
	@Override
	protected void writeBody(OutputStream out, ByteInput content)
			throws IOException {
		content.copyInto(out);
	}
	/**
	 * @see simple.app.WorkQueue#getWorkBodyType()
	 */
	@Override
	public Class<ByteInput> getWorkBodyType() {
		return ByteInput.class;
	}
}
