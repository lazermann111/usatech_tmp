package com.usatech.app.jms;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.NamingException;

import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.event.ProgressListener;
import simple.io.Log;
import simple.util.concurrent.RunOnGetFuture;

public class JMSRetryQueue implements WorkQueue<Message> {
	private static final Log log = Log.getLog();
	protected ExtendedSession initSession;
	protected long retryDelay = 5000;
	protected String queueKey;
	protected String username;
	protected String password;
	protected boolean delayAfterConsume = false; // delay before consume or after consume

	protected RunOnGetFuture<Destination> destinationFuture = new RunOnGetFuture<Destination>() {
		@Override
		protected Destination call(Object... params) throws Exception {
			return createDestination(initSession);
		}
	};
	protected class JMSRetriever implements WorkQueue.Retriever<Message> {
		protected final Session session;
		protected final MessageConsumer consumer;
		protected Publisher<Message> publisher;
		protected MessageProducer producer;
		protected final ReentrantLock lock = new ReentrantLock();
		protected final Condition signal = lock.newCondition();
		protected long delay;
		
		public JMSRetriever(Session session, MessageConsumer consumer) {
			super();
			this.session = session;
			this.consumer = consumer;
		}
		/**
		 * @see simple.app.WorkQueue.Retriever#arePrequisitesAvailable()
		 */
		@Override
		public boolean arePrequisitesAvailable() {
			return true;
		}
		/**
		 * @see simple.app.WorkQueue.Retriever#cancelPrequisiteCheck(java.lang.Thread)
		 */
		@Override
		public void cancelPrequisiteCheck(Thread thread) {
		}

		public void resetAvailability() {
		}

		protected Publisher<Message> getRetrieverPublisher() throws JMSException {
			if(publisher == null) {
				producer = session.createProducer(null);
				publisher = new Publisher<Message>() {
					public void publish(String queueKey, Message content)
							throws ServiceException {
						publish(queueKey, false, false, content);
					}
					public void publish(String queueKey, boolean multicast, boolean temporary, Message content)
							throws ServiceException {
						publish(queueKey, multicast, temporary, content, null, null);
					}
					@Override
					public void publish(String queueKey, boolean multicast, boolean temporary, Message content, Long correlation, simple.app.QoS qos, String... blocks) throws ServiceException {
						int deliverMode;
						int priority;
						long ttl;
						if(qos == null) {
							deliverMode = temporary ? DeliveryMode.NON_PERSISTENT : DeliveryMode.PERSISTENT;
							priority = Message.DEFAULT_PRIORITY;
							ttl = Message.DEFAULT_TIME_TO_LIVE;
						} else if(qos.getAvailable() > 0 && qos.getAvailable() > System.currentTimeMillis()) {
							throw new ServiceException("Delayed execute is not supported");
						} else {
							deliverMode = (qos.isPersistent() && !temporary ? DeliveryMode.PERSISTENT : DeliveryMode.NON_PERSISTENT);
							if(qos.getPriority() > 0 && qos.getPriority() < 11)
								priority = qos.getPriority() - 1;
							else
								priority = Message.DEFAULT_PRIORITY;
							if(qos.getTimeToLive() > 0)
								ttl = qos.getTimeToLive();
							else if(qos.getExpiration() > 0) {
								long start = System.currentTimeMillis();
								ttl = qos.getExpiration() - start;
							} else
								ttl = Message.DEFAULT_TIME_TO_LIVE;
						}
						try {
							content = JMSBase.copyMessage(content, session);
							producer.send(JMSBase.getDestination(queueKey, multicast, session), content, deliverMode, priority, ttl);
						} catch(JMSException e) {
							throw new ServiceException("Could not publish message", e);
						}
					}
					/**
					 * @see simple.app.Publisher#close()
					 */
					@Override
					public void close() {
						try {
							producer.close();
						} catch(JMSException e) {
							log.info("Could not close producer", e);
						}
						producer = null;
						publisher = null;
					}

					@Override
					public int getConsumerCount(String queueName, boolean temporary) {
						return 1;
					}

					public String[] getSubscriptions(String queueKey) throws ServiceException {
						return null;
					}

					@Override
					public void block(String... blocks) throws ServiceException {
						throw new ServiceException("Blocking is not supported in this publisher implementation");
					}
				};
			}
			return publisher;
		}
		
		protected void resend(Message originalMsg) throws JMSException {
			getRetrieverPublisher(); // ensure producer is not null
			Message msg = JMSBase.copyMessage(originalMsg, session);
			msg.setJMSReplyTo(originalMsg.getJMSReplyTo());
			msg.setJMSDestination(originalMsg.getJMSDestination());
			long ttl;
			if(originalMsg.getJMSExpiration() > 0) {
				ttl = originalMsg.getJMSExpiration() - System.currentTimeMillis();
				if(ttl <= 0) {
					log.info("Message " + JMSBase.getMessageId(originalMsg) + " has expired; not enqueuing in JMS");
					return;
				}
			} else {
				ttl = 0;
			}
			producer.send(msg.getJMSDestination(), msg, originalMsg.getJMSDeliveryMode(), originalMsg.getJMSPriority() > 0 ? originalMsg.getJMSPriority() - 1 : 0, ttl);
			session.commit();
		}
		public Work<Message> next() throws WorkQueueException, InterruptedException {
			if(!delayAfterConsume && delay > 0) {
				if(log.isDebugEnabled())
					log.debug("Last message was not available for " + delay + " ms. Waiting until it would be available");
				lock.lock();
				try {
					signal.await(delay, TimeUnit.MILLISECONDS);
				} finally {
					lock.unlock();
				}
			}
			final Message msg;
			try {
				msg = consumer.receive();
			} catch(JMSException e) {
				if(e.getCause() instanceof InterruptedException)
					throw (InterruptedException)e.getCause();
				throw new WorkQueueException("Could not receive message", e);
			}
			if(msg == null)
				return null;
			String msgId = JMSBase.getMessageId(msg);
			try {
				delay = JMSBase.getAvailableAfter(msg) - System.currentTimeMillis();
			} catch(JMSException e) {
				log.warn("Could not get AvailableAfter property - using no delay", e);
				delay = 0;
			}
			if(delay > 0) {
				if(delayAfterConsume) {
					if(log.isDebugEnabled())
						log.debug("Message " + msgId + " is not available for " + delay + " ms. Waiting until it is available");
					lock.lock();
					try {
						signal.await(delay, TimeUnit.MILLISECONDS);
					} finally {
						lock.unlock();
					}
				} else {
					//stick back in retry queue
					if(log.isDebugEnabled())
						log.debug("Message " + msgId + " is not available for " + delay + " ms. Re-queuing it");
					try {
						resend(msg);
					} catch(JMSException e) {
						throw new WorkQueueException("Could not re-publish message " + msgId, e);
					}
					return null;
				}
			} else {
				if(log.isDebugEnabled())
					log.debug("Message " + msgId + " is available immediately. Serving it for processing");				
			}
			
			return new Work<Message>() {
				protected boolean complete;

				public String getGuid() {
					try {
						return msg.getJMSMessageID();
					} catch(JMSException e) {
						return null;
					}
				}
				@Override
				public QoS getQoS() {
					return new JMSQoS(msg);
				}
				public Map<String, ?> getWorkProperties() {
					try {
						return new JMSMessageProperties(msg);
					} catch(JMSException e) {
						return null;
					}
				}
				public long getEnqueueTime() {
					try {
						return msg.getJMSTimestamp();
					} catch(JMSException e) {
						log.warn(e);
						return 0;
					}
				}
				public boolean isRedelivered() {
					try {
						return msg.getJMSRedelivered();
					} catch(JMSException e) {
						log.warn(e);
						return true; // err on the side of caution
					}
				}
				public boolean isUnguardedRetryAllowed() {
					return false;
				}
				public int getRetryCount() {
					return -1;
				}
				public boolean isComplete() {
					return complete;
				}
				public Message getBody() {
					return msg;
				}
				public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
					if(complete) throw new WorkQueueException("Work is already complete; workAborted() may not be called");
					try {
						session.rollback();
					} catch(JMSException e) {
						throw new WorkQueueException("Could not rollback message ID " + JMSBase.getMessageId(msg), e);
					}
					complete = true;
				}

				public void workComplete() throws WorkQueueException {
					if(complete) throw new WorkQueueException("Work is already complete; workComplete() may not be called");
					try {
						session.commit();
					} catch(JMSException e) {
						throw new WorkQueueException("Could not commit message ID " + JMSBase.getMessageId(msg), e);
					}
					complete = true;
				}
				public Publisher<Message> getPublisher() throws WorkQueueException {
					try {
						return getRetrieverPublisher();
					} catch(JMSException e) {
						throw new WorkQueueException("Could not create MessageProducer", e);
					}
				}
				/**
				 * @see java.lang.Object#toString()
				 */
				@Override
				public String toString() {
					return "JMS Message " + JMSBase.getMessageInfo(msg);
				}
				public String getOriginalQueueName() {
					try {
						return AbstractJMSWorkBase.getQueueName(msg.getJMSReplyTo());
					} catch(JMSException e) {
						log.warn(e);
					}
					return null;
				}
				public String getQueueName() {
					try {
						return AbstractJMSWorkBase.getQueueName(msg.getJMSDestination());
					} catch(JMSException e) {
						log.warn(e);
					}
					return getQueueKey();
				}
				public void unblock(String... blocksToClear) {
					// TODO Auto-generated method stub			
				}
				public String getSubscription() {
					return null;
				}
			};
		}
		public boolean cancel(Thread thread) {
			boolean cancelled = true;
			try {
				consumer.close();
			} catch(JMSException e) {
				//ignore
				cancelled = false;
			}
			lock.lock();
			try {
				signal.signalAll();
			} finally {
				lock.unlock();
			}
			return cancelled;
		}
		public void close() {
			if(producer != null) try {
				producer.close();
			} catch(JMSException e) {
				//ignore
			}
			try {
				session.close();
			} catch(JMSException e) {
				//ignore
			}
		}
	};

	protected Destination createDestination(ExtendedSession session) throws JMSException {
		String qk = getQueueKey();
		if(qk == null)
			throw new JMSException("QueueKey is not set");
		return JMSBase.getDestination(qk, false, session);
	}
	protected Destination createTemporaryDestination(ExtendedSession session) throws JMSException {
		return JMSBase.getTemporaryDestination(false, session);
	}

	public boolean isAutonomous() {
		return false;
	}

	@Override
	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
		this.destinationFuture.reset();
	}

	public WorkQueue.Retriever<Message> getRetriever(ProgressListener listener) throws WorkQueueException {
		try {
			ExtendedSession session = JMSBase.createSession(getUsername(), getPassword(), true);
			initSession = session;
			Destination dest = destinationFuture.get();
			//TODO: should we maybe create the destination each time?
			JMSRetriever retriever = new JMSRetriever(session, session.createConsumer(dest));
			if(log.isDebugEnabled())
				log.debug("Listening for messages on jms destination '" + dest + "'");
			return retriever;
		} catch(JMSException e) {
			throw new WorkQueueException("Could not create JMS Session or Consumer", e);
		} catch(NamingException e) {
			throw new WorkQueueException("Could not lookup ConnectionFactory or Destination", e);
		} catch(InterruptedException e) {
			throw new WorkQueueException("While initializaling", e);
		} catch(ExecutionException e) {
			throw new WorkQueueException("Could not create destination", e);
		}
	}
	/**
	 * @see simple.app.WorkQueue#getWorkBodyType()
	 */
	@Override
	public Class<Message> getWorkBodyType() {
		return Message.class;
	}
	/**
	 * @see simple.app.WorkQueue#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() {
		return TRUE_FUTURE;
	}
	public long getRetryDelay() {
		return retryDelay;
	}
	public void setRetryDelay(long retryDelay) {
		this.retryDelay = retryDelay;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isDelayAfterConsume() {
		return delayAfterConsume;
	}
	public void setDelayAfterConsume(boolean delayAfterConsume) {
		this.delayAfterConsume = delayAfterConsume;
	}
}
