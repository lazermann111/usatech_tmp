package com.usatech.app.jms;


import java.io.IOException;


public class JMSBytesPublisher extends StandaloneJMSPublisher<byte[]> {
	protected boolean transacted;
	@Override
	public boolean isTransacted() {
		return transacted;
	}
	@Override
	protected void writeContent(MessageOutputStream<?> out, byte[] content) throws IOException {
		out.write(content);
	}
	public void setTransacted(boolean transacted) {
		this.transacted = transacted;
		close(); //XXX: should we commit here?
	}
}
