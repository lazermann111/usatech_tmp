/**
 * 
 */
package com.usatech.app.jms;

import java.io.IOException;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Session;

public class BytesMessageOutputStream extends MessageOutputStream<BytesMessage> {
	public BytesMessageOutputStream(Session session) throws JMSException {
		super(session.createBytesMessage());
	}
	@Override
	public void write(int b) throws IOException {
		try {
			message.writeByte((byte)b);
		} catch(JMSException e) {
			throw newIOException("Could not write to message", e);
		}
	}
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		try {
			message.writeBytes(b, off, len);
		} catch(JMSException e) {
			throw newIOException("Could not write to message", e);
		}
	}
	@Override
	public void write(byte[] b) throws IOException {
		try {
			message.writeBytes(b);
		} catch(JMSException e) {
			throw newIOException("Could not write to message", e);
		}
	}
}