package com.usatech.app.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.Topic;

import simple.app.AbstractWorkQueueService;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueueException;
import simple.db.StatementMonitor;
import simple.io.Log;

public class JMSRetryService extends AbstractWorkQueueService<Message> {
	private static final Log log = Log.getLog();
	protected String deadLetterQueueKey = "_DLQ";
	protected final WorkProcessor<Message> workProcessor = new WorkProcessor<Message>() {
		/**
		 * @see simple.app.AbstractWorkQueueService.WorkProcessor#interruptProcessing(java.lang.Thread)
		 */
		@Override
		public boolean interruptProcessing(Thread thread) {
			StatementMonitor.cancelStatements(thread);
			thread.interrupt();
			return true;
		}
		/**
		 * @see simple.app.AbstractWorkQueueService.WorkProcessor#processWork(simple.app.Work)
		 */
		@Override
		public void processWork(Work<Message> work) throws ServiceException, WorkQueueException {
			Message msg = work.getBody();
			String queueKey;
			boolean multicast;
			try {
				Destination originalDest = msg.getJMSReplyTo();
				if(originalDest == null || originalDest.equals(msg.getJMSDestination())) {
					queueKey = getDeadLetterQueueKey();
					multicast = false;
				} else if(originalDest instanceof Queue) {
					queueKey = ((Queue)originalDest).getQueueName();
					multicast = false;
				} else if(originalDest instanceof Topic) {
					queueKey = ((Topic)originalDest).getTopicName();
					multicast = true;
				} else {
					log.warn("Invalid original destination class '" + originalDest.getClass().getName() + "'. Putting in dead letter queue");
					queueKey = getDeadLetterQueueKey();
					multicast = false;
				}
			} catch(JMSException e) {
				throw new ServiceException(e); //TODO: should we specify blocking retry?
			}
			work.getPublisher().publish(queueKey, multicast, false, msg, null, work.getQoS());
		}
	};
	/**
	 * @param serviceName
	 */
	public JMSRetryService(String serviceName) {
		super(serviceName);
	}
	/**
	 * @see simple.app.AbstractWorkQueueService#getWorkProcessor()
	 */
	@Override
	protected WorkProcessor<Message> getWorkProcessor() {
		return workProcessor;
	}
	public String getDeadLetterQueueKey() {
		return deadLetterQueueKey;
	}
	public void setDeadLetterQueueKey(String deadLetterQueueKey) {
		this.deadLetterQueueKey = deadLetterQueueKey;
	}
}
