package com.usatech.app.jms;

import java.io.IOException;

import javax.jms.BytesMessage;
import javax.jms.Message;

import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;


/** A <b>thread-safe</b> implementation of Publisher for JMS system that uses a JMSPublisher per thread.
 * @author Brian S. Krug
 *
 */
public abstract class AbstractThreadLocalJMSPublisher<J> implements Publisher<J> {
	protected Class<? extends Message> messageType = BytesMessage.class;
	protected String username;
	protected String password;
	protected final ThreadLocal<StandaloneJMSPublisher<J>> locals = new ThreadLocal<StandaloneJMSPublisher<J>>() {
		@Override
		protected StandaloneJMSPublisher<J> initialValue() {
			StandaloneJMSPublisher<J> pub = new StandaloneJMSPublisher<J>() {
				@Override
				protected boolean isTransacted() {
					return AbstractThreadLocalJMSPublisher.this.isTransacted();
				}
				protected void writeContent(MessageOutputStream<?> out, J content) throws IOException {
					AbstractThreadLocalJMSPublisher.this.writeContent(out, content);
				}
			};
			pub.setMessageType(messageType);
			pub.setUsername(username);
			pub.setPassword(password);
			return pub;
		}
	};
	protected abstract boolean isTransacted() ;
	protected abstract void writeContent(MessageOutputStream<?> out, J content) throws IOException ;
	
	@Override
	public void close() {
		getLocalPublisher().close();
	}
	public void publish(String queueKey, J content) throws ServiceException {
		getLocalPublisher().publish(queueKey, content);
	}
	public void publish(String queueKey, boolean multicast, boolean temporary, J content) throws ServiceException {
		getLocalPublisher().publish(queueKey, multicast, temporary, content);
	}

	public void publish(String queueKey, boolean multicast, boolean temporary, J content, Long correlation, QoS qos, String... blocks) throws ServiceException {
		getLocalPublisher().publish(queueKey, multicast, temporary, content, null, qos, blocks);
	}
	protected StandaloneJMSPublisher<J> getLocalPublisher() {
		StandaloneJMSPublisher<J> pub = locals.get();
		if(!ConvertUtils.areEqual(pub.getMessageType(), messageType))
			pub.setMessageType(messageType);
		if(!ConvertUtils.areEqual(pub.getUsername(), username))
			pub.setUsername(username);
		if(!ConvertUtils.areEqual(pub.getPassword(), password))
			pub.setPassword(password);
		
		return pub;
	}
	public Class<? extends Message> getMessageType() {
		return messageType;
	}
	public void setMessageType(Class<? extends Message> messageType) {
		this.messageType = messageType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String[] getSubscriptions(String queueKey) throws ServiceException {
		return null;
	}

	@Override
	public void block(String... blocks) throws ServiceException {
		throw new ServiceException("Blocking is not supported in this publisher implementation");
	}
}
