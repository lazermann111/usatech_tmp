package com.usatech.app.jms;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;
import javax.jms.Topic;

import simple.app.AbstractWorkQueue;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.io.ByteInput;
import simple.io.Log;

public abstract class AbstractJMSWorkBase<J> extends AbstractWorkQueue<J> {
	private static final Log log = Log.getLog();
	protected String username;
	protected String password;

	protected abstract class AbstractJMSRetriever implements Retriever<J> {
		protected final Session session;
		protected final MessageConsumer consumer;
		protected InnerPublisher publisher;
		
		protected class PendingMessage {
			protected final String queueKey;
			protected final boolean multicast;
			protected final boolean temporary;
			protected final J content;
			protected final QoS qos;
			public PendingMessage(String queueKey, boolean multicast, boolean temporary, J content, QoS qos) {
				super();
				this.queueKey = queueKey;
				this.multicast = multicast;
				this.content = content;
				this.qos = qos;
				this.temporary = temporary;
			}
		}
		protected class InnerPublisher extends AbstractJMSPublisher<J> {
			protected List<PendingMessage> pendingMessages;

			public void publish(String queueKey, boolean multicast, boolean temporary, J content, Long correlation, QoS qos, String... blocks) throws ServiceException {
				if(pendingMessages == null) {
					pendingMessages = new ArrayList<PendingMessage>(3);
				}
				pendingMessages.add(new PendingMessage(queueKey, multicast, temporary, content, qos));
			}
			protected void writeContent(MessageOutputStream<?> out, J content) throws IOException {
				writeBody(out, content);
			}
			protected void flush() throws WorkQueueException {
				if(pendingMessages != null) {
					try {						
						for(PendingMessage pm : pendingMessages)
							sendMessage(session, pm.queueKey, pm.multicast, pm.temporary, pm.content, pm.qos);
					} catch(JMSException e) {
						throw new WorkQueueException("Could not publish JMS message", e);
					} finally {
						pendingMessages.clear();
					}
				}
			}
			protected void clear() {
				if(pendingMessages != null)
					pendingMessages.clear();
			}
		}
		public AbstractJMSRetriever(Session session, MessageConsumer consumer) {
			super();
			this.session = session;
			this.consumer = consumer;
		}
		/**
		 * @see simple.app.WorkQueue.Retriever#arePrequisitesAvailable()
		 */
		@Override
		public boolean arePrequisitesAvailable() {
			return prerequisiteManager.arePrequisitesAvailable();
		}
		/**
		 * @see simple.app.WorkQueue.Retriever#cancelPrequisiteCheck(java.lang.Thread)
		 */
		@Override
		public void cancelPrequisiteCheck(Thread thread) {
			prerequisiteManager.cancelPrequisiteCheck(thread);
		}

		public void resetAvailability() {
			prerequisiteManager.resetAvailability();
		}

		protected InnerPublisher initPublisher() {
			if(publisher == null) {
				log.debug("Creating new JMS publisher");
				publisher = new InnerPublisher();
				if(log.isDebugEnabled())
					log.debug("Created new publisher");	
			}
			return publisher;
		}
		protected MessageProducer getProducer() throws JMSException {
			return initPublisher().initProducer(session);
		}
		protected abstract Message nextMessage() throws WorkQueueException, InterruptedException ;
		protected abstract void handleWorkAborted(Message msg, String desc, WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException ;
		protected abstract void handleWorkComplete(Message msg, String desc) throws WorkQueueException ;
		protected void flushPublisher() throws WorkQueueException {
			if(publisher != null)
				publisher.flush();
		}
		protected void clearPublisher() {
			if(publisher != null)
				publisher.clear();
		}
		public Work<J> next() throws WorkQueueException, InterruptedException {
			final Message msg = nextMessage();
			ByteInput bi;
			if(msg instanceof BytesMessage) {
				bi = new JMSByteMessageByteInput((BytesMessage)msg);
			} else if(msg instanceof StreamMessage) {
				bi = new JMSStreamMessageByteInput((StreamMessage)msg);
			} else if(msg instanceof TextMessage) {
				try {
					bi = new JMSTextHexMessageByteInput((TextMessage)msg);
				} catch(JMSException e) {
					throw new WorkQueueException("Could not get content of JMS Text Message", e);
				}
			} else if(msg == null) {
				return null;
			} else {
				throw new WorkQueueException("JMS Message Type " + JMSBase.getMessageType(msg) + " not supported on message ID " + JMSBase.getMessageId(msg));
			}
			final J body = createBody(bi);
			final boolean redelivered = JMSBase.getRedeliveredSafely(msg);
			final int retryCount = JMSBase.getRetryCountSafely(msg);
			final boolean unguardedRetryAllowed = JMSBase.getUnguardedRetrySafely(msg);
			final String desc = "JMS Message " + JMSBase.getMessageInfo(msg) + " [" + retryCount + " retries; " + (redelivered ? "redelivered" : "first delivery") + "]";
			if(log.isDebugEnabled())
				log.debug("Created Work '" + desc + "'");
			return new Work<J>() {
				protected boolean complete;

				public String getGuid() {
					try {
						return msg.getJMSMessageID();
					} catch(JMSException e) {
						return null;
					}
				}
				@Override
				public QoS getQoS() {
					return new JMSQoS(msg);
				}
				public Map<String, ?> getWorkProperties() {
					try {
						return new JMSMessageProperties(msg);
					} catch(JMSException e) {
						return null;
					}
				}
				public long getEnqueueTime() {
					try {
						return msg.getJMSTimestamp();
					} catch(JMSException e) {
						log.warn(e);
						return 0;
					}
				}
				public boolean isRedelivered() {
					return retryCount != 0 || redelivered;
				}
				public boolean isUnguardedRetryAllowed() {
					return !redelivered && unguardedRetryAllowed;
				}
				public int getRetryCount() {
					return retryCount;
				}
				public boolean isComplete() {
					return complete;
				}
				public J getBody() {
					return body;
				}
				public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
					if(complete) throw new WorkQueueException("Work is already complete; workAborted() may not be called");
					clearPublisher();
					try {
						handleWorkAborted(msg, desc, retryType, unguardedRetryAllowed, cause);
					} catch(WorkQueueException e) {
						try {
							session.rollback();
						} catch(JMSException e1) {
							log.warn("Could not rollback JMS transaction", e1);
						} catch(RuntimeException e1) {
							log.warn("Could not rollback JMS transaction", e1);
						} catch(Error e1) {
							log.warn("Could not rollback JMS transaction", e1);
						}
						throw e;
					}
					complete = true;
				}

				public void workComplete() throws WorkQueueException {
					if(complete) throw new WorkQueueException("Work is already complete; workComplete() may not be called");				
					try {
						flushPublisher();
						handleWorkComplete(msg, desc);
					} catch(WorkQueueException e) {
						try {
							session.rollback();
						} catch(JMSException e1) {
							log.warn("Could not rollback JMS transaction", e1);
						} catch(RuntimeException e1) {
							log.warn("Could not rollback JMS transaction", e1);
						} catch(Error e1) {
							log.warn("Could not rollback JMS transaction", e1);
						}
						throw e;
					}
					complete = true;
				}
				public Publisher<J> getPublisher() throws WorkQueueException {
					return initPublisher();
				}
				/**
				 * @see java.lang.Object#toString()
				 */
				@Override
				public String toString() {
					return desc;
				}
				public String getOriginalQueueName() {
					try {
						return AbstractJMSWorkBase.getQueueName(msg.getJMSReplyTo());
					} catch(JMSException e) {
						log.warn(e);
					}
					return null;
				}
				public String getQueueName() {
					try {
						return AbstractJMSWorkBase.getQueueName(msg.getJMSDestination());
					} catch(JMSException e) {
						log.warn(e);
					}
					return getQueueKey();
				}
				public void unblock(String... blocksToClear) {
					// TODO Auto-generated method stub			
				}
				public String getSubscription() {
					return null;
				}
			};
		}
		public boolean cancel(Thread thread) {
			try {
				consumer.close();
			} catch(JMSException e) {
				//ignore
				return false;
			}
			return true;
		}
		public void close() {
			if(publisher != null) 
				publisher.close();
			try {
				session.close();
			} catch(JMSException e) {
				//ignore
			}
		}
	}

	/**
	 * @param bi
	 * @return
	 */
	protected abstract J createBody(ByteInput bi) ;

	protected abstract void writeBody(OutputStream out, J content) throws IOException;

	protected static String getQueueName(Destination destination) throws JMSException {	
		if(destination instanceof Queue)
			return ((Queue)destination).getQueueName();
		else if(destination instanceof Topic)
			return ((Topic)destination).getTopicName();
		else if(destination != null)
			return destination.toString();
		else
			return null;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @see simple.app.WorkQueue#shutdown()
	 */
	public Future<Boolean> shutdown() {
		return TRUE_FUTURE; //We don't need to do anything else, because Retriever.cancel() and Retriever.close() will do the rest
	}

}
