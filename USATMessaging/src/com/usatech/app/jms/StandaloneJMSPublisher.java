package com.usatech.app.jms;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.NamingException;

import simple.app.QoS;
import simple.app.ServiceException;
import simple.io.Log;


public abstract class StandaloneJMSPublisher<Q> extends AbstractJMSPublisher<Q> {
	private static final Log log = Log.getLog();
	protected String username;
	protected String password;
	protected Session session;
	
	protected Session initSession() throws ServiceException {
		if(session == null) {
			log.debug("Obtaining JMS session");
			try {
				session = JMSBase.createSession(getUsername(), getPassword(), isTransacted());
			} catch(NamingException e) {
				throw new ServiceException("Could not create JMS Session", e);
			} catch(InterruptedException e) {
				throw new ServiceException("Could not create JMS Session", e);
			} catch(JMSException e) {
				throw new ServiceException("Could not create JMS Session", e);
			}
			if(log.isInfoEnabled())
				log.info("Obtained JMS session " + session);
		}
		return session;
	}
	
	public void close() {
		super.close();
		if(session != null)
			try {
				session.close();
			} catch(JMSException e) {
				//ignore
			}
		session = null;
	}
	
	public void publish(String queueKey, boolean multicast, boolean temporary, Q content, Long correlation, QoS qos, String... blocks) throws ServiceException {
		Session session = initSession();
		boolean transacted;
		try {
			transacted = session.getTransacted();
		} catch(JMSException e) {
			close();
			throw new ServiceException("Could not get 'transacted' property from session", e);
		}
		try {
			sendMessage(session, queueKey, multicast, temporary, content, qos);
		} catch(JMSException e) {
			if(transacted)
				try {
					session.rollback();
				} catch(JMSException e1) {
					log.warn("Could not rollback JMS Session", e1);
				}
			throw new ServiceException("Could not publish message", e);
		}
		if(transacted) {
			long start;
			if(log.isInfoEnabled())
				start = System.currentTimeMillis();
			else
				start = 0;
			try {
				session.commit();
			} catch(JMSException e) {
				throw new ServiceException("Could not commit JMS session", e);
			}
			if(log.isInfoEnabled()) {
				long committed = System.currentTimeMillis();
				log.info("Committed JMS Session " + session + " [in " + (committed-start) + " ms]");	
			}
		}
	}


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		close();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
		close();
	}

	protected abstract boolean isTransacted() ;
}
