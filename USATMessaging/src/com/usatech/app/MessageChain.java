package com.usatech.app;

import java.io.IOException;

import simple.io.ByteInput;

/** An encapulsation of a chain of processing. Based on the results of processing the first message in the chain,
 *  subsequent messages are chosen using the resultCode returned from {@link MessageChainTask#process(MessageChainTaskInfo)}
 * @author Brian S. Krug
 *
 */
public interface MessageChain {
	/** Returns a globally unique identifier for this message chain
	 * @return The guid
	 */
	public String getGuid() ;

	/** Returns the instance number of this message chain (or one more than the number of times clear() has been called)
	 * @return
	 */
	public int getInstance() ;

	/** Returns a string containing information about who executed each step, how long it took, etc. The level is the
	 * amount of detail to use: 4 shows all information, 3 shows who and how long, 2 shows who,
	 * 1 show the current step queue name, 0 shows nothing
	 * @param level
	 * @return
	 */
	public String getExecutionInfo(int level) ;

	/** Returns the current step which is either the first step added (for Message Chain's not yet processed) or
	 *  the step specified in the serialization
	 * @return the current step
	 */
	public MessageChainStep getCurrentStep();

	/** Sets the current step for this message chain
	 * @param step
	 */
	public void setCurrentStep(MessageChainStep step) ;
	
	/** Sets the result code after processing the current step. Returns whether the message chain is complete.
	 * @see #isComplete()
	 * @param resultCode
	 * @return <code>true</code> if and only if the message chain is complete.
	 */
	public boolean setResultCode(int resultCode);

	/** Getter for resultCode. Returns -1 if resultCode has not yet been set using {@link #setResultCode(int)}
	 * @return the result code
	 */
	public int getResultCode() ;

	/** Creates a new MessageChainStep and adds it to this MessageChain, then returns the new MessageChainStep
	 * @param queueKey the queue key of the new step
	 * @return the new MessageChainStep
	 */
	public MessageChainStep addStep(String queueKey) ;
	
	/** Creates a new MessageChainStep and adds it to this MessageChain, then returns the new MessageChainStep
	 * @param queueKey the queue key of the new step
	 * @param multicast whether to send to all consumers or just one
	 * @return the new MessageChainStep
	 */
	public MessageChainStep addStep(String queueKey, boolean multicast);
	
	/** Returns whether the result code that was set points to another step to process or not
	 * @return <code>true</code> if and only if the message chain is complete.
	 */
	public boolean isComplete();

	/** Clears all MessageChainSteps and the resultCode from this MessageChain.
	 *  It may be re-used after clear() is called
	 *
	 */
	public void clear();

	/** Reads the MessageChain from the provided input
	 * @param input the ByteInput that contains the values that will be read into this MessageChain
	 * @param encryptionRequired Whether to throw an IOException if message is not encrypted 
	 * @throws IOException
	 */
	public void read(ByteInput input, boolean encryptionRequired) throws IOException;

	public boolean isWaitingFor(MessageChainStep step);

	public Iterable<MessageChainStep> getAllWaitFors();

	public int getStepCount();

	public MessageChainStep getStep(int index);

}