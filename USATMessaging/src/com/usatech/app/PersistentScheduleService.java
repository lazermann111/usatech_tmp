package com.usatech.app;

import java.util.concurrent.Future;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueueException;


/** This schedules messages to be enqueued at a later time. It assumes that the Scheduler persists Jobs (i.e. - stored to a fault-tolerant data store.)
 * @author Brian S. Krug
 *
 */
public class PersistentScheduleService extends AbstractScheduleService {
	protected static final String[] JOB_DATA_NAMES = new String[] {"queue", "message"};
	public static class PersistentEnqueueJob extends EnqueueJob {
		public void execute(JobExecutionContext context) throws JobExecutionException {
			JobDataMap jobDataMap = context.getMergedJobDataMap();
			publish(jobDataMap);
		}
	}

	public PersistentScheduleService(String serviceName) {
		super(serviceName);
	}
	@Override
	protected Class<? extends Job> getJobClass() {
		return PersistentEnqueueJob.class;
	}
	@Override
	protected boolean isJobPersistent() {
		return true;
	}
	@Override
	protected void schedule(long time, String queueKey, byte[] chainBody, Work<?> work) throws ServiceException, WorkQueueException {
		try {
			scheduleTrigger(time, JOB_DATA_NAMES, queueKey, chainBody);
		} catch(SchedulerException e) {
			throw new ServiceException("Could not schedule enqueue", e);
		}
		if(!work.isComplete())
			work.workComplete();
	}
	
	@Override
	public Future<Boolean> shutdown() {
		initializer.reset();
		return super.shutdown();
	}
}
