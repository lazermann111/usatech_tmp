package com.usatech.app;

import java.util.LinkedHashMap;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.results.DatasetHandler;

public abstract class AbstractAttributeDatasetHandler<I extends Exception> implements DatasetHandler<I> {
	protected final Map<String, Object> data = new LinkedHashMap<String, Object>();
	
	public void handleRowStart() {
		data.clear();
	}

	public void handleValue(String columnName, Object value) {
		data.put(columnName, value);
	}
	
	protected <T> T getDetailIndexedAttribute(Attribute attribute, int index, Class<T> convertTo, boolean required) throws AttributeConversionException {
		return getDetailAttribute(attribute.getValue() + '.' + index, convertTo, required);
	}
	protected <T> T getDetailAttribute(Attribute attribute, Class<T> convertTo, boolean required) throws AttributeConversionException {
		return getDetailAttribute(attribute.getValue(), convertTo, required);
	}
	protected <T> T getDetailAttribute(String attributeName, Class<T> convertTo, boolean required) throws AttributeConversionException {
		try {
			if(required)
				return ConvertUtils.convertRequired(convertTo, data.get(attributeName));
			return ConvertUtils.convert(convertTo, data.get(attributeName));
		} catch(ConvertException e) {
			throw new AttributeConversionException(attributeName, e);
		}
	}

	protected <T> T getDetailAttributeDefault(Attribute attribute, Class<T> convertTo, T defaultValue) throws AttributeConversionException {
		return getDetailAttributeDefault(attribute.getValue(), convertTo, defaultValue);
	}

	protected <T> T getDetailAttributeDefault(String attributeName, Class<T> convertTo, T defaultValue) throws AttributeConversionException {
		try {
			return ConvertUtils.convertDefault(convertTo, data.get(attributeName), defaultValue);
		} catch(ConvertException e) {
			throw new AttributeConversionException(attributeName, e);
		}
	}

}
