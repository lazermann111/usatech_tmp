/**
 * 
 */
package com.usatech.app.task;

import java.sql.SQLException;

import simple.app.ServiceException;

public interface SQLExceptionHandler {
	public void handleSQLException(String callId, Object parameters, SQLException e) throws ServiceException ;
}