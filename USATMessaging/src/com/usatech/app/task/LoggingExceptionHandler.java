/**
 *
 */
package com.usatech.app.task;

import java.sql.SQLException;
import java.text.Format;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.LiteralFormat;

/**
 * @author Brian S. Krug
 *
 */
public class LoggingExceptionHandler implements SQLExceptionHandler {
	private static final Log log = Log.getLog();
	protected String message;
	//protected int logLevel;
	/**
	 * @see com.usatech.app.task.SQLExceptionHandler#handleSQLException(java.lang.String, java.lang.Object, java.sql.SQLException)
	 */
	@Override
	public void handleSQLException(String callId, Object parameters, SQLException e)
			throws ServiceException {
		String msg = getMessage();
		String text;
		if(msg != null) {
			Format fm = ConvertUtils.getFormat(getMessage());
			if(fm instanceof LiteralFormat)
				text = fm.format(null);
			else
				text = fm.format(new Object[] {callId, parameters, e});
		} else
			text = e.getMessage();
		log.error(text);
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
