/**
 *
 */
package com.usatech.app.task;

import java.sql.SQLException;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;


/**
 * @author Brian S. Krug
 *
 */
public class SQLStateSQLExceptionHandler implements SQLExceptionHandler {
	/**
	 * @see com.usatech.app.task.SQLExceptionHandler#handleSQLException(java.lang.String, java.lang.Object, java.sql.SQLException)
	 */
	@Override
	public void handleSQLException(String callId, Object parameters, SQLException e) throws ServiceException {
		throw DatabasePrerequisite.createServiceException("For '" + callId + "' with parameters " + parameters, e);
	}
}
