/**
 *
 */
package com.usatech.app.task;

import java.sql.SQLException;
import java.text.Format;


import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertUtils;
import simple.text.LiteralFormat;

/**
 * @author Brian S. Krug
 *
 */
public class RetrySQLExceptionHandler implements SQLExceptionHandler {
	protected String message;
	protected WorkRetryType retryType = WorkRetryType.NONBLOCKING_RETRY;
	/**
	 * @see com.usatech.app.task.SQLExceptionHandler#handleSQLException(java.lang.String, java.lang.Object, java.sql.SQLException)
	 */
	@Override
	public void handleSQLException(String callId, Object parameters, SQLException e)
			throws ServiceException {
		String msg = getMessage();
		String text;
		if(msg != null) {
			Format fm = ConvertUtils.getFormat(getMessage());
			if(fm instanceof LiteralFormat)
				text = fm.format(null);
			else
				text = fm.format(new Object[] {callId, parameters, e});
		} else
			text = e.getMessage();
		throw new RetrySpecifiedServiceException(text, getRetryType());
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public WorkRetryType getRetryType() {
		return retryType;
	}
	public void setRetryType(WorkRetryType retryType) {
		this.retryType = retryType;
	}

}
