package com.usatech.app.task;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.util.CompositeMap;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

public class ExecuteMultiDbCallTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	public static enum CommitStrategy { AFTER_ALL, AFTER_EACH, NEVER }
	protected String[] callIds;
	protected SQLExceptionHandler sqlExceptionHandler;
	protected CommitStrategy commitStrategy = CommitStrategy.AFTER_ALL;
	
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();		
		CompositeMap<String, Object> merged = new CompositeMap<String, Object>();
		merged.merge(resultAttributes);
		merged.merge(attributes);
		return execute(merged, resultAttributes);
	}
	protected int execute(Object input, Object output) throws ServiceException {
		String[] ids = getCallIds();
		if(ids == null || ids.length == 0)
			throw new ServiceException("CallIds property is not set on " + this);
		CommitStrategy commitStrategy = getCommitStrategy();
		String id = ids[0];
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall(id);
		} catch(SQLException e) {
			throw new RetrySpecifiedServiceException("Could not get connection", e, WorkRetryType.BLOCKING_RETRY);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException("Could not get connection", e, WorkRetryType.BLOCKING_RETRY);
		}
		boolean inTransaction = false;
		try {
			for(int i = 0; i < ids.length; i++) {
				id = ids[i];
				inTransaction = true;
				DataLayerMgr.executeCall(conn, id, input, output);
				if(commitStrategy == CommitStrategy.AFTER_EACH) {
					conn.commit();
					inTransaction = false;
				}
			}
			if(commitStrategy == CommitStrategy.AFTER_ALL) {
				conn.commit();
				inTransaction = false;
			}
		} catch(SQLException e) {
			if(inTransaction)
				try {
					conn.rollback();
				} catch(SQLException e1) {
					log.warn("Could not rollback connection: " + e1.getMessage());
				}
			SQLExceptionHandler handler = getSqlExceptionHandler();
			if(handler != null)
				handler.handleSQLException(id, input, e);
			else
				throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			if(inTransaction)
				try {
					conn.rollback();
				} catch(SQLException e1) {
					log.warn("Could not rollback connection: " + e1.getMessage());
				}
			throw new ServiceException(e);
		} finally {
			try {
				conn.close();
			} catch(SQLException e) {
				log.warn("Could not close connection: " + e.getMessage());
			}
		}
		return 0;
	}
	public String[] getCallIds() {
		return callIds;
	}
	public void setCallIds(String[] callIds) {
		this.callIds = callIds;
	}
	public SQLExceptionHandler getSqlExceptionHandler() {
		return sqlExceptionHandler;
	}
	public void setSqlExceptionHandler(SQLExceptionHandler sqlExceptionHandler) {
		this.sqlExceptionHandler = sqlExceptionHandler;
	}
	public CommitStrategy getCommitStrategy() {
		return commitStrategy;
	}
	public void setCommitStrategy(CommitStrategy commitStrategy) {
		this.commitStrategy = commitStrategy;
	}
}
