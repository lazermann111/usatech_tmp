package com.usatech.app.task;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.event.AbstractTaskListener;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.app.Attribute;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

public class EnqueueMessageTaskListener extends AbstractTaskListener {
	public static enum EnqueueMessageAttribute implements Attribute {
		START_TIME("startTime"),
		END_TIME("endTime"),
		TASK_NAME("taskName"),
		TASK_DETAILS("taskDetails"),
		THREAD_ID("threadId"),
		THREAD_NAME("threadName"),
		APP_NAME("appName"),
		;
		private final String value;	    
	    protected final static EnumStringValueLookup<EnqueueMessageAttribute> lookup = new EnumStringValueLookup<EnqueueMessageAttribute>(EnqueueMessageAttribute.class);
	    public static EnqueueMessageAttribute getByValue(String value) throws InvalidValueException {
	    	return lookup.getByValue(value);
	    }
		private EnqueueMessageAttribute(String value) {
			this.value = value;
		}
		public String getValue() {
			return value;
		}
	}
	private static final Log log = Log.getLog();
	protected Publisher<ByteInput> publisher;
	protected String queueKey;
	protected boolean multicast;
	protected String appName;
	
	@Override
	protected void taskRan(String taskName, String taskDetails, long threadId, String threadName, long startTime, long endTime) {
		Publisher<ByteInput> pub = getPublisher();
		if(pub == null) {
			log.warn("Publisher is not configured on " + this + "; NOT publishing task info");
			return;
		}
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(getQueueKey(), isMulticast());
		step.setAttribute(EnqueueMessageAttribute.APP_NAME, getAppName());
		step.setAttribute(EnqueueMessageAttribute.TASK_NAME, taskName);
		step.setAttribute(EnqueueMessageAttribute.TASK_DETAILS, taskDetails);
		step.setAttribute(EnqueueMessageAttribute.THREAD_ID, threadId);
		step.setAttribute(EnqueueMessageAttribute.THREAD_NAME, threadName);
		step.setAttribute(EnqueueMessageAttribute.START_TIME, startTime);
		step.setAttribute(EnqueueMessageAttribute.END_TIME, endTime);
		try {
			MessageChainService.publish(mc, pub);
		} catch(ServiceException e) {
			log.warn("Could not publisher task info message", e);
		}	
	}

	@Override
	public void flush() {
		// do nothing
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}

	public boolean isMulticast() {
		return multicast;
	}

	public void setMulticast(boolean multicast) {
		this.multicast = multicast;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

}
