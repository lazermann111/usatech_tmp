package com.usatech.app.task;

import java.util.Map;

import simple.app.ServiceException;
import simple.db.BasicBatchRecorder;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

public class ExecuteDbBatchTask implements MessageChainTask {
	protected final BasicBatchRecorder<Map<String,Object>> recorder = new BasicBatchRecorder<Map<String,Object>>();
	
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		recorder.record(taskInfo.getStep().getAttributes());
		return 0;
	}
	@Override
	protected void finalize() throws Throwable {
		recorder.finish();
		super.finalize();
	}

	public void flush() {
		recorder.flush();
	}

	public int getBatchSize() {
		return recorder.getBatchSize();
	}

	public String getCallId() {
		return recorder.getCallId();
	}

	public int getPollFrequency() {
		return recorder.getPollFrequency();
	}
	public boolean isUseThreadForExecute() {
		return isAsynchronous();
	}
	public void setUseThreadForExecute(boolean useThreadForExecute) {
		setAsynchronous(useThreadForExecute);
	}
	public boolean isAsynchronous() {
		return recorder.isAsynchronous();
	}

	public void setAsynchronous(boolean asynchronous) {
		recorder.setAsynchronous(asynchronous);
	}

	public void setBatchSize(int batchSize) {
		recorder.setBatchSize(batchSize);
	}

	public void setCallId(String callId) {
		recorder.setCallId(callId);
	}

	public void setPollFrequency(int pollFrequency) {
		recorder.setPollFrequency(pollFrequency);
	}
}
