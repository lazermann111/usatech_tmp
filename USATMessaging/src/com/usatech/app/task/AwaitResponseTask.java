package com.usatech.app.task;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;

import simple.app.Publisher;
import simple.app.QoS;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.ByteInput;
import simple.io.Log;
import simple.security.crypt.EncryptionInfoMapping;
import simple.util.concurrent.WaitForUpdateResult;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

public class AwaitResponseTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected final ConcurrentMap<Long, WaitForUpdateResult<MessageChainStep>> registry = new ConcurrentHashMap<Long, WaitForUpdateResult<MessageChainStep>>();
	protected static final Attribute REQUEST_ID_ATTRIBUTE = new Attribute() {
		public String getValue() {
			return "_requestId";
		}

		@Override
		public String toString() {
			return getValue();
		}
	};
	protected static final AtomicLong REQUEST_ID_GENERATOR = new AtomicLong(Double.doubleToLongBits(Math.random()));
	protected String responseQueueKey;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		long requestId;
		try {
			requestId = taskInfo.getStep().getAttribute(REQUEST_ID_ATTRIBUTE, Long.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Could not get request id", e, WorkRetryType.NO_RETRY);
		}
		WaitForUpdateResult<MessageChainStep> result = registry.get(requestId);
		if(result == null)
			log.warn("Request " + requestId + " is already expired");
		else
			result.setResult(taskInfo.getStep());
		return 0;
	}

	public Map<Attribute, Object> awaitResponse(MessageChain messageChain, Map<Attribute, MessageChainStep> referenceAttributes, final long timeout, Publisher<ByteInput> publisher, EncryptionInfoMapping encryptionInfoMapping) throws InterruptedException, TimeoutException, ServiceException {
		long requestId = REQUEST_ID_GENERATOR.getAndIncrement();
		WaitForUpdateResult<MessageChainStep> result = new WaitForUpdateResult<MessageChainStep>();
		registry.put(requestId, result);
		try {
			MessageChainStep responseStep = messageChain.addStep(getResponseQueueKey());
			responseStep.setAttribute(REQUEST_ID_ATTRIBUTE, requestId);
			for(Map.Entry<Attribute, MessageChainStep> entry : referenceAttributes.entrySet())
				responseStep.setReferenceAttribute(entry.getKey(), entry.getValue(), entry.getKey());
			MessageChainService.publish(messageChain, publisher, encryptionInfoMapping, new QoS() {
				public long getAvailable() {
					return 0;
				}

				public long getExpiration() {
					return 0;
				}

				public Map<String, ?> getMessageProperties() {
					return null;
				}

				public int getPriority() {
					return 5;
				}

				public long getTimeToLive() {
					return timeout;
				}

				public boolean isPersistent() {
					return false;
				}
			});
			MessageChainStep processedResponseStep = result.awaitResult(timeout);
			Map<Attribute, Object> responseMap = new HashMap<Attribute, Object>(referenceAttributes.size());
			for(Attribute a : referenceAttributes.keySet())
				responseMap.put(a, processedResponseStep.getAttributeSafely(a, Object.class, null));
			return responseMap;
		} finally {
			registry.remove(requestId);
		}
	}

	public String getResponseQueueKey() {
		return responseQueueKey;
	}

	public void setResponseQueueKey(String responseQueueKey) {
		this.responseQueueKey = responseQueueKey;
	}
}
