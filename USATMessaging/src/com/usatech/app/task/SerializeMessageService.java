package com.usatech.app.task;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.app.AbstractWorkQueueService;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueueException;
import simple.io.ByteInput;
import simple.io.Log;

public class SerializeMessageService extends AbstractWorkQueueService<ByteInput> implements AbstractWorkQueueService.WorkProcessor<ByteInput> {
	private static final Log log = Log.getLog();
	protected File directory;
	protected String fileSuffix = ".msg";
	protected String filePrefix = "q-";
	protected static final AtomicLong nameGenerator = new AtomicLong(System.currentTimeMillis());
	protected static final Pattern sanitizeQueueName = Pattern.compile("[^\\w-.]+");
	
	public SerializeMessageService(String serviceName) {
		super(serviceName);
	}

	@Override
	protected AbstractWorkQueueService.WorkProcessor<ByteInput> getWorkProcessor() {
		return this;
	}

	@Override
	public boolean interruptProcessing(Thread thread) {
		thread.interrupt();
		return true;
	}

	@Override
	public void processWork(Work<ByteInput> work) throws ServiceException, WorkQueueException {
		StringBuilder sb = new StringBuilder();
		String prefix = getFilePrefix();
		if(prefix != null)
			sb.append(prefix);
		Matcher matcher = sanitizeQueueName.matcher(work.getQueueName());
		String qn = matcher.replaceAll("");
		sb.append(qn).append('-').append(Long.toHexString(nameGenerator.incrementAndGet())).append('-');
		File d = getDirectory();
		try {
			File file = TaskUtils.serializeMessage(work, sb.toString(), getFileSuffix(), d);
			log.info("Wrote work to file '" + file.getAbsolutePath() + "'");
		} catch(IOException e) {
			throw new ServiceException("Could not serialize queue message in directory '" + (d == null ? "" : d.getAbsolutePath()) + "'", e);
		}		
	}

	public File getDirectory() {
		return directory;
	}

	public void setDirectory(File directory) {
		this.directory = directory;
	}
	public void setDirectoryPath(String directoryPath) throws IOException {
		File dir = new File(directoryPath);
		if(!dir.exists()) {
			if(!dir.mkdirs()) {
				throw new IOException("Could not create directory '" + directoryPath + "' for serializing queue messages");
			}
		}
		if(!dir.canWrite()) 
			throw new IOException("Cannot write to directory '" + directoryPath + "' for serializing queue messages");
		setDirectory(dir);
	}

	public String getFileSuffix() {
		return fileSuffix;
	}

	public void setFileSuffix(String fileSuffix) {
		this.fileSuffix = fileSuffix;
	}

	public String getFilePrefix() {
		return filePrefix;
	}

	public void setFilePrefix(String filePrefix) {
		this.filePrefix = filePrefix;
	}

}
