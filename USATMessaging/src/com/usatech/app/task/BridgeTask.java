/**
 *
 */
package com.usatech.app.task;

import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.ByteInput;
import simple.io.Log;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.GenericAttribute;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

/**
 * @author Brian S. Krug
 *
 */
public class BridgeTask implements MessageChainTask {
	private static Log log = Log.getLog();
	public static final Attribute BRIDGE_TO_QUEUE_KEY = new GenericAttribute("bridgeTo");
	protected Publisher<ByteInput> bridgePublisher;
	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String queueKey;
		try {
			queueKey = step.getAttribute(BRIDGE_TO_QUEUE_KEY, String.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		step.setQueueKey(queueKey);
		step.setAttribute(BRIDGE_TO_QUEUE_KEY, null);
		MessageChainService.publish(step.getMessageChain(), getBridgePublisher());
		log.info("Bridged MessageChain to " + queueKey);
		step.setNextSteps(0); // subsequent steps will be processed via the bridged message chain
		return 0;
	}

	public Publisher<ByteInput> getBridgePublisher() {
		return bridgePublisher;
	}

	public void setBridgePublisher(Publisher<ByteInput> bridgePublisher) {
		this.bridgePublisher = bridgePublisher;
	}
}
