package com.usatech.app.task;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


import simple.app.ServiceException;

public class ErrorCodeSQLExceptionHandler implements SQLExceptionHandler {
	protected final Map<Integer,SQLExceptionHandler> sqlExceptionHandlers = new HashMap<Integer, SQLExceptionHandler>();
	protected SQLExceptionHandler fallbackExceptionHandler;

	/**
	 * @see com.usatech.app.task.SQLExceptionHandler#handleSQLException(java.lang.String, java.lang.Object, java.sql.SQLException)
	 */
	@Override
	public void handleSQLException(String callId, Object parameters, SQLException e)
			throws ServiceException {
		SQLExceptionHandler handler = getSqlExceptionHandler(e.getErrorCode());
		if(handler != null)
			handler.handleSQLException(callId, parameters, e);
		else if(fallbackExceptionHandler != null)
			fallbackExceptionHandler.handleSQLException(callId, parameters, e);
		else
			throw new ServiceException(e);
	}
	public SQLExceptionHandler getSqlExceptionHandler(int errorCode) {
		return sqlExceptionHandlers.get(errorCode);
	}
	public void setSqlExceptionHandler(int errorCode, SQLExceptionHandler sqlExceptionHandler) {
		sqlExceptionHandlers.put(errorCode, sqlExceptionHandler);
	}
	public SQLExceptionHandler getFallbackExceptionHandler() {
		return fallbackExceptionHandler;
	}
	public void setFallbackExceptionHandler(SQLExceptionHandler fallbackExceptionHandler) {
		this.fallbackExceptionHandler = fallbackExceptionHandler;
	}
}
