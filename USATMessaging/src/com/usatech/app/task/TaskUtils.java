package com.usatech.app.task;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import simple.app.Publisher;
import simple.app.QoS;
import simple.app.Work;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.io.ByteArrayByteInput;
import simple.io.ByteInput;

public class TaskUtils {
	public static File serializeMessage(Work<ByteInput> work, String filePrefix, String fileSuffix, File directory) throws IOException {
		File file = File.createTempFile(filePrefix, fileSuffix, directory);
		serializeMessage(work, file);
		return file;
	}
	public static void serializeMessage(Work<ByteInput> work, File file) throws FileNotFoundException, IOException {
		serializeMessage(work, new FileOutputStream(file));
	}
	public static void serializeMessage(Work<ByteInput> work, OutputStream out) throws IOException {
		DataOutputStream dos = new DataOutputStream(out);
		String tmp;
		tmp = work.getOriginalQueueName();
		dos.writeUTF(tmp == null ? "" : tmp);
		dos.writeLong(work.getEnqueueTime());
		ByteInput bi = work.getBody();
		if(bi != null) {
			byte[] bytes = new byte[bi.length()];
			bi.readBytes(bytes, 0, bytes.length);
			dos.writeInt(bytes.length);
			dos.write(bytes);
		} else {
			dos.writeInt(0);
		}
		dos.flush();
		dos.close();	
	}
	public static Work<ByteInput> deserializeMessage(File file) throws FileNotFoundException, IOException {
		return deserializeMessage(new FileInputStream(file));
	}
	public static Work<ByteInput> deserializeMessage(InputStream in) throws IOException {
		DataInputStream dis = new DataInputStream(in);
		final String origQueueName = dis.readUTF();
		final long enqueueTime = dis.readLong();
		byte[] bytes = new byte[dis.readInt()];
		int tot = 0;
		while(tot < bytes.length) {
			int r = dis.read(bytes, tot, bytes.length - tot);
			if(r < 0)
				throw new IOException("Could not read all the bytes. Read " + tot + " of " + bytes.length);
			tot += r;
		}
		dis.close();
		final ByteInput body = new ByteArrayByteInput(bytes);
		return new Work<ByteInput>() {
			public String getGuid() {
				return null;
			}
			public QoS getQoS() {
				// TODO Implements this
				return null;
			}
			public Map<String, ?> getWorkProperties() {
				return null;
			}
			public ByteInput getBody() {
				return body;
			}
			public long getEnqueueTime() {
				return enqueueTime;
			}
			public String getOriginalQueueName() {
				return origQueueName;
			}
			public Publisher<ByteInput> getPublisher() throws WorkQueueException {
				return null;
			}
			public String getQueueName() {
				return null;
			}
			public boolean isComplete() {
				return false;
			}
			public boolean isRedelivered() {
				return true;
			}
			public int getRetryCount() {
				return -1;
			}
			public boolean isUnguardedRetryAllowed() {
				return false;
			}
			public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
				throw new WorkQueueException("Processing this message is not supported");
			}
			public void workComplete() throws WorkQueueException {
				//TODO: We could delete the file I suppose
				throw new WorkQueueException("Processing this message is not supported");
			}
			public void unblock(String... blocksToClear) {
				// TODO Auto-generated method stub			
			}
			public String getSubscription() {
				return null;
			}
		};
	}
}
