package com.usatech.app.task;

import java.sql.SQLException;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

public class ExecuteDbCallTask implements MessageChainTask {
	protected String callId;
	protected SQLExceptionHandler sqlExceptionHandler;
	protected boolean retryOnDisconnect = false;

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		return execute(taskInfo.getStep().getAttributes(), taskInfo.getStep().getResultAttributes());
	}
	protected int execute(Object input, Object output) throws ServiceException {
		String id = getCallId();
		if(id == null || id.trim().length() == 0)
			throw new ServiceException("CallId property is not set on " + this);

		try {
			DataLayerMgr.executeCall(id, input, output, true, isRetryOnDisconnect() ? DataLayer.DEFAULT_RETRY : null);
		} catch(SQLException e) {
			SQLExceptionHandler handler = getSqlExceptionHandler();
			if(handler != null)
				handler.handleSQLException(id, input, e);
			else
				throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
		return 0;
	}
	public String getCallId() {
		return callId;
	}
	public void setCallId(String callId) {
		this.callId = callId;
	}
	public SQLExceptionHandler getSqlExceptionHandler() {
		return sqlExceptionHandler;
	}
	public void setSqlExceptionHandler(SQLExceptionHandler sqlExceptionHandler) {
		this.sqlExceptionHandler = sqlExceptionHandler;
	}

	public boolean isRetryOnDisconnect() {
		return retryOnDisconnect;
	}

	public void setRetryOnDisconnect(boolean retryOnDisconnect) {
		this.retryOnDisconnect = retryOnDisconnect;
	}
}
