/**
 *
 */
package com.usatech.app.task;

import simple.app.ServiceException;
import simple.io.Log;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

/**
 * @author Brian S. Krug
 *
 */
public class DoNothingTask implements MessageChainTask {
	private static Log log = Log.getLog();
	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		log.warn("Received MessageChain with step attributes " + taskInfo.getStep().getAttributes());
		return 0;
	}
}
