package com.usatech.app;

public interface MixedValue {
	public void addReferencePart(MessageChainStep referencedStep, Attribute resultAttribute);

	public void addIndexedReferencePart(MessageChainStep referencedStep, Attribute resultAttribute, int index);

	public void addLiteralPart(String literal);

	public void clear();

}
