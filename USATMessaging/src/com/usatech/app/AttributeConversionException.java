/**
 *
 */
package com.usatech.app;

import simple.bean.ConvertException;

/**
 * @author Brian S. Krug
 *
 */
public class AttributeConversionException extends Exception {
	private static final long serialVersionUID = 1919942141878270241L;

	protected final String attributeKey;
	/**
	 * @param attribute
	 * @param cause
	 */
	public AttributeConversionException(Attribute attribute, ConvertException cause) {
		this(attribute.getValue(), cause);
	}

	/**
	 * @param attribute
	 * @param cause
	 */
	public AttributeConversionException(String attributeName, ConvertException cause) {
		super(cause.getMessage() + " for attribute '" + attributeName + "'", cause);
		this.attributeKey = attributeName;
	}
	
	/**
	 * @see java.lang.Throwable#getCause()
	 */
	@Override
	public ConvertException getCause() {
		return (ConvertException)super.getCause();
	}

	public String getAttributeKey() {
		return attributeKey;
	}
}
