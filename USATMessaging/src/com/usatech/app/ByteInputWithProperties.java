package com.usatech.app;

import java.util.Map;

import simple.io.ByteInput;

public class ByteInputWithProperties {
	protected final ByteInput byteInput;
	protected final Map<String,?> properties;
	public ByteInputWithProperties(ByteInput byteInput, Map<String, ?> properties) {
		super();
		this.byteInput = byteInput;
		this.properties = properties;
	}
	public ByteInput getByteInput() {
		return byteInput;
	}
	public Map<String, ?> getProperties() {
		return properties;
	}
}
