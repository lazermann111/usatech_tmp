package com.usatech.app;

public interface AsyncTaskExecutor {
	public void init();
	public void process();	
}
