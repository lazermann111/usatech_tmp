/**
 *
 */
package com.usatech.app;

/**
 * @author Brian S. Krug
 *
 */
public interface Attribute {
	public String getValue() ;
}
