package com.usatech.app;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.AbstractWorkQueue;
import simple.app.AbstractWorkQueueService;
import simple.app.Prerequisite;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.db.StatementMonitor;
import simple.event.TaskListener;
import simple.io.ByteArrayByteInput;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.lang.EnumStringValueLookup;
import simple.lang.Holder;
import simple.lang.InvalidValueException;
import simple.security.crypt.EncryptionInfo;
import simple.security.crypt.EncryptionInfoMapping;
import simple.text.StringUtils;


/** Implementation of {@link simple.app.Service} that pulls Work from a {@link simple.app.WorkQueue&lt;ByteInput&gt;}, transforms
 *  the work body into a {@link MessageChain} and then sends it to the configured {@link MessageChainTask} to process. It then handles
 *  the result of that processing.
 * @author Brian S. Krug
 *
 */
public class MessageChainService extends AbstractWorkQueueService<ByteInput> {
	private static final Log log = Log.getLog();
	protected MessageChainTask messageChainTask;
	protected AsyncTaskExecutor asyncTaskExecutor;
	protected static byte MESSAGE_CHAIN_VERSION = 0x11;
	protected boolean timingEnabled = false;
	protected EncryptionInfoMapping encryptionInfoMapping;
	protected TaskListener taskListener;
	protected boolean defaultResultEncryptionRequired;
	protected final Map<String, Boolean> resultEncryptionRequiredPerQueue = new HashMap<String, Boolean>();
	protected boolean incomingEncryptionRequired;
	protected boolean publisherGuardedFromInterrupt = false;
	
	public static enum MessageChainAttribute implements Attribute {
		START_TIME("startTime"),
		END_TIME("endTime"),
		PING_ONLY("pingOnly"),
		PING_EXPIRATION("pingExpiration"),
		;
		private final String value;	    
	    protected final static EnumStringValueLookup<MessageChainAttribute> lookup = new EnumStringValueLookup<MessageChainAttribute>(MessageChainAttribute.class);
	    public static MessageChainAttribute getByValue(String value) throws InvalidValueException {
	    	return lookup.getByValue(value);
	    }
		private MessageChainAttribute(String value) {
			this.value = value;
		}
		public String getValue() {
			return value;
		}
	}
	
	protected class MessageChainWorkProcessor implements WorkProcessor<ByteInput> {
		protected final ReentrantLock lock = new ReentrantLock();
		protected boolean interruptible = true;
		public boolean interruptProcessing(Thread thread) {
			StatementMonitor.cancelStatements(thread);
			lock.lock();
			try {
				if(interruptible) {
					thread.interrupt();
					return true;
				}
				return false;
			} finally {
				lock.unlock();
			}
		}
		public boolean getInterruptible() {
			lock.lock();
			try {
				return interruptible;
			} finally {
				lock.unlock();
			}
		}
		public boolean setInterruptible(boolean interruptible) {
			lock.lock();
			try {
				boolean old = this.interruptible;
				this.interruptible = interruptible;
				if(!interruptible)
					Thread.interrupted();
				return old;
			} finally {
				lock.unlock();
			}
		}
		public void processWork(final Work<ByteInput> work) throws ServiceException,
				WorkQueueException {
			//process(work.getBody(), work.getPublisher(), work.isRedelivered(), work.getEnqueueTime(), work.getQoS());
			final MessageChain mc = deserialize(work.getBody());
			if(messageChainTask != null) {
				final MessageChainStep step = mc.getCurrentStep();
				String queueName = step.getQueueKey();
				if(!queueMatches(queueName, getQueue().getQueueKey()))
					throw new RetrySpecifiedServiceException("Queue key mis-match: this service handles queue '" + getQueue().getQueueKey() + "' but was given a message on queue '" + step.getQueueKey() + "'", WorkRetryType.NO_RETRY);
				long start, end;
				if(isTimingEnabled())
					start = System.currentTimeMillis();
				else
					start = 0;
				boolean pingOnly;
				try {
					pingOnly = step.getAttributeDefault(MessageChainAttribute.PING_ONLY, Boolean.class, false);
				} catch(AttributeConversionException e) {
					log.warn("Could not convert pingOnly attribute", e);
					pingOnly = false;
				}
				if(log.isInfoEnabled()) {
					StringBuilder sb = new StringBuilder();
					sb.append("STARTED ");
					if(pingOnly)
						sb.append("(PING ONLY) ");
					sb.append("step '").append(mc.getGuid()).append(':').append(mc.getInstance()).append("': ");
					if(log.isDebugEnabled())
						sb.append(mc.getExecutionInfo(4));
					else
						sb.append(mc.getExecutionInfo(3)).append(": attributes=").append(step.getAttributes());
					if(work.getEnqueueTime() > 0 && start > 0)
						sb.append(" [Published ").append(start - work.getEnqueueTime()).append(" ms ago]");
					log.info(sb.toString());
				}
				int resultCode;
				final Holder<QoS> nextQoS = new Holder<QoS>();
				final Holder<Runnable> afterCompleteHolder = new Holder<Runnable>();
				final Publisher<ByteInput> publisher = work.getPublisher();
				if(pingOnly) {
					long pingExpiration;
					try {
						pingExpiration = step.getAttributeDefault(MessageChainAttribute.PING_EXPIRATION, Long.class, 0L);
					} catch(AttributeConversionException e) {
						log.warn("Could not convert pingExpiration attribute", e);
						pingExpiration = 0L;
					}
					if(pingExpiration > 0) {
						long time;
						if(start > 0) 
							time = start;
						else 
							time = System.currentTimeMillis();
						if(pingExpiration < time)
							resultCode = 1;
						else
							resultCode = 0;
					} else
						resultCode = 0;
					nextQoS.setValue(work.getQoS());
				} else {
					setInterruptible(true);
					MessageChainTaskInfo taskInfo = new MessageChainTaskInfo() {
						public Publisher<ByteInput> getPublisher() {
							return publisher;
						}
						public String getServiceName() {
							return MessageChainService.this.getServiceName();
						}
						public MessageChainStep getStep() {
							return step;
						}
						public boolean isRedelivered() {
							return work.isRedelivered() && !work.isUnguardedRetryAllowed();
						}
						public int getRetryCount() {
							return work.getRetryCount();
						}
						public String getMessageChainGuid() {
							return mc.getGuid();
						}
						@Override
						public void setNextQos(QoS qos) {
							nextQoS.setValue(qos);
						}
						@Override
						public QoS getCurrentQos() {
							return work.getQoS();
						}
						@Override
						public void setAfterComplete(Runnable afterComplete) {
							afterCompleteHolder.setValue(afterComplete);
						}
						@Override
						public boolean getInterruptible() {
							return MessageChainWorkProcessor.this.getInterruptible();
						}
						@Override
						public boolean setInterruptible(boolean interruptible) {
							return MessageChainWorkProcessor.this.setInterruptible(interruptible);
						}
						@Override
						public EncryptionInfoMapping getEncryptionInfoMapping() {
							return MessageChainService.this.getEncryptionInfoMapping();
						}

						@Override
						public long getEnqueueTime() {
							return work.getEnqueueTime();
						}
					};
					resultCode = messageChainTask.process(taskInfo);
					if (asyncTaskExecutor != null)
						asyncTaskExecutor.process();
				}
				if(isTimingEnabled()) {
					end = System.currentTimeMillis();
					TaskListener tl = getTaskListener();
					if(tl != null)
						tl.taskRan("Service " + getServiceName() + (pingOnly ? " (PING ONLY)" : ""), step.getAttributes().toString(), start, end);
				} else
					end = 0;
				if(log.isInfoEnabled())  {
					StringBuilder sb = new StringBuilder();
					sb.append("FINISHED ");
					if(pingOnly)
						sb.append("(PING ONLY) ");
					sb.append("step '").append(mc.getGuid()).append(':').append(mc.getInstance()).append("': ");
					if(log.isDebugEnabled())
						sb.append(mc.getExecutionInfo(4));
					else
						sb.append(mc.getExecutionInfo(3)).append(": attributes=").append(step.getAttributes());
					sb.append("; resultCode=").append(resultCode).append("; resultAttributes=").append(step.getResultAttributes());
					if(start > 0 && end > 0) {
						sb.append(" [in ").append(end - start).append(" ms");
						if(work.getEnqueueTime() > 0 && start > 0)
							sb.append("; published ").append(end - work.getEnqueueTime()).append(" ms ago");
						sb.append(']');
					}
					log.info(sb.toString());
				}
				if(mc.isWaitingFor(step)) {
					StringBuilder sb = new StringBuilder(mc.getGuid()).append(':').append(step.getStepIndex());
					if(step.isMulticast())
						sb.append('-').append(work.getSubscription());
					work.unblock(sb.toString());
				}
				mc.setResultCode(resultCode);
				// If this step is multicast, check if any step down the line is joined and if so only include it if this is the first subscription
				if(step.isMulticast()) {
					String[] subs = publisher.getSubscriptions(step.getQueueKey());
					if(subs != null && subs.length > 1 && !subs[0].equals(work.getSubscription())) {
						step.removeDownstreamJoinedSteps();
					}
				}
				MessageChainStep[] nextSteps = step.getNextSteps(resultCode);
				try {
					if(nextSteps == null || nextSteps.length == 0) {
						if(resultCode == 0) {
							if(log.isDebugEnabled())
								log.debug("No next step found for result code " + String.valueOf(resultCode) + " for step '" + mc.getGuid() + ':' + mc.getInstance() + "'. Doing nothing more.");
						} else if(log.isWarnEnabled())
							log.warn("No next step found for result code " + String.valueOf(resultCode) + " for step '" + mc.getGuid() + ':' + mc.getInstance() + "'. Doing nothing more.");
					} else {
						if(isTimingEnabled()) {
							step.getResultAttributes().put(MessageChainAttribute.START_TIME.getValue(), start);
							step.getResultAttributes().put(MessageChainAttribute.END_TIME.getValue(), end);								
						}
						for(MessageChainStep nextStep : nextSteps) {
							if(nextStep.getResultCode() != -1) {
								if(nextStep != step || nextStep.getQueueKey().equals(queueName)) {
									log.error("Message Chain recursion detected. Aborting message chain '" + mc.getGuid() + "'");
									throw new RetrySpecifiedServiceException("Message Chain recursion detected. Aborting message chain '" + mc.getGuid() + "'", WorkRetryType.NO_RETRY);
								}
								nextStep.setQueueKey(nextStep.getQueueKey());
							}
							EncryptionInfoMapping eim = getEncryptionInfoMapping();
							EncryptionInfo encryptionInfo;
							if(eim != null) {
								encryptionInfo = eim.findEncryptionInfo(nextStep.getQueueKey());
								if(isResultEncryptionRequired(nextStep.getQueueKey()) && encryptionInfo == null)
									throw new ServiceException("No EncryptionInfo found for queue '" + nextStep.getQueueKey() + "' on Message Chain Service '" + getServiceName() + "' but result encryption is required");
							} else if(isResultEncryptionRequired(nextStep.getQueueKey()))
								throw new ServiceException("EncryptionInfoMapping is not set for Message Chain Service '" + getServiceName() + "' but result encryption is required");
							else
								encryptionInfo = null;
							if(log.isInfoEnabled())
								log.info("Enqueuing step '" + mc.getGuid() + ':' + mc.getInstance() + "' in queue " + nextStep.getQueueKey() + (encryptionInfo == null || encryptionInfo.getCipherName() == null ? "" : " with " + encryptionInfo.getCipherName() + " encryption"));
							byte[] chainBody = serialize(nextStep, encryptionInfo);
							if(log.isDebugEnabled()) {
								StringWriter sw = new StringWriter();
								MessageChainService.printStepChain(nextStep, new PrintWriter(sw));
								log.debug("Step Chain is now:\n" + sw.toString());
							}
							Set<MessageChainStep> waitFors = nextStep.getWaitFors();
							String[] blocks;
							if(waitFors != null && !waitFors.isEmpty()) {
								List<String> blockList = new ArrayList<String>();
								for(MessageChainStep waitFor : waitFors) {
									StringBuilder sb = new StringBuilder(mc.getGuid()).append(':').append(waitFor.getStepIndex());
									if(waitFor.isMulticast()) {
										String[] subs = publisher.getSubscriptions(waitFor.getQueueKey());
										sb.append('-');
										int len = sb.length();
										for(String sub : subs) {
											if(waitFor != step || !sub.equals(work.getSubscription())) {
												sb.setLength(len);
												sb.append(sub);
												blockList.add(sb.toString());
											}
										}
									} else if(waitFor != step)
										blockList.add(sb.toString());
								}
								blocks = blockList.toArray(new String[blockList.size()]);
							} else
								blocks = null;
							if(isPublisherGuardedFromInterrupt()) {
								// We don't want to interrupt while publishing next message in chain as this causes issues
								setInterruptible(false);
							}
							// ensure that interrupted status is cleared
							boolean interrupted = Thread.interrupted();
							if(interrupted && log.isInfoEnabled())
								log.info("Thread " + Thread.currentThread().getName() + " was interrupted, but task did not throw an exception. Processing step normally");
							try {
								publisher.publish(nextStep.getQueueKey(), nextStep.isMulticast(), nextStep.isTemporary(), new ByteArrayByteInput(chainBody), nextStep.getCorrelation(), nextQoS.getValue(), blocks);
							} catch(ServiceException e) {
								if(e.getCause() != null && e.getCause().getClass().getName().equals("javax.jms.InvalidDestinationException")) {// invalid destination - no point in keeping
									log.error("Next Step could not be queued because queueKey '" + nextStep.getQueueKey() + "' is invalid. Discarding step '" + mc.getGuid() + ':' + mc.getInstance() + "' with content '" + StringUtils.toHex(chainBody) + "'");
								} else
									throw e;
							}
							if(isPublisherGuardedFromInterrupt()) {
								setInterruptible(true);
							}
						}
						if(!work.isComplete())
							work.workComplete();
					}
				} finally {
					if(afterCompleteHolder.getValue() != null)
						afterCompleteHolder.getValue().run();
				}
			}
			else throw new ServiceException("No processor set");
		}
	}
	/** Creates a new ComponentService with the specified service name
	 * @param serviceName
	 */
	public MessageChainService(String serviceName) {
		super(serviceName);
	}

	protected boolean queueMatches(String stepQueueName, String serviceQueueName) {
		if(stepQueueName == null)
			return true;
		if(serviceQueueName == null)
			return true;
		if(stepQueueName.equals(serviceQueueName))
			return true;
		if(stepQueueName.endsWith('.' + serviceQueueName))
			return true;
		if(stepQueueName.startsWith(serviceQueueName + '#'))
			return true;
		return false;
	}
	public static void addAllAttributes(MessageChainStep step, Map<String, ? extends Object> attributes) {
		if(attributes != null) {
			for(Map.Entry<String, ? extends Object> entry : attributes.entrySet())
				step.addLiteralAttribute(entry.getKey(), entry.getValue());
		}
	}
	public static void printStepChain(MessageChainStep step, PrintWriter out) {
		printStepChain(step, out, 0, new HashSet<MessageChainStep>());
		out.flush();
	}

	protected static void printStepChain(MessageChainStep step, PrintWriter out, int depth, Set<MessageChainStep> recursionProtection) {
		out.print(step.getQueueKey());
		out.print(' ');
		out.print(step.getAttributes());
		out.println();
		char[] indent = new char[depth * 5];
		Arrays.fill(indent, ' ');
		if(recursionProtection.add(step)) {
			for(int rc : step.getResultCodes()) {
				out.print(indent);
				out.print('-');
				String s = String.valueOf(rc);
				switch(s.length()) {
					case 1:
						out.print('-');
					case 2:
						out.print(' ');
					case 3:
						out.print(s);
				}
				out.print('=');
				MessageChainStep[] nextSteps = step.getNextSteps(rc);
				if(nextSteps == null || nextSteps.length == 0)
					out.print("none");
				else if(nextSteps.length == 1) {
					printStepChain(nextSteps[0], out, depth + 1, recursionProtection);
				} else {
					for(int i = 0; i < nextSteps.length; i++) {
						if(i > 0) {
							out.println();
							out.print(indent);
							out.print("    =");
						}
						printStepChain(nextSteps[i], out, depth + 1, recursionProtection);
					}
				}
			}
			recursionProtection.remove(step);
		} else {
			out.print(indent);
			out.print("...recursion...");
			out.println();
		}
	}

	/** Converts the specified MessageChain to a ByteInput and then publishes it using the provided Publisher
	 * @param messageChain
	 * @param publisher
	 * @throws ServiceException
	 */
	public static void publish(MessageChain messageChain, Publisher<ByteInput> publisher) throws ServiceException {
		publishInternal(messageChain, publisher, null, null, null);
	}
	public static void publish(MessageChain messageChain, Publisher<ByteInput> publisher, EncryptionInfoMapping encryptionInfoMapping) throws ServiceException {
		publishInternal(messageChain, publisher, null, encryptionInfoMapping, null);
	}
	public static void publish(MessageChain messageChain, Publisher<ByteInput> publisher, EncryptionInfoMapping encryptionInfoMapping, QoS qos) throws ServiceException {
		publishInternal(messageChain, publisher, null, encryptionInfoMapping, qos);
	}
	protected static void publishInternal(MessageChain messageChain, Publisher<ByteInput> publisher, Boolean multicastOverride, EncryptionInfoMapping encryptionInfoMapping, QoS qos) throws ServiceException {
		MessageChainStep step = messageChain.getCurrentStep();
		int rc = messageChain.getResultCode();
		MessageChainStep[] nextSteps;
		if(rc == -1) {
			nextSteps = new MessageChainStep[] {step};
		} else {
			nextSteps = step.getNextSteps(rc);
		}
		if(nextSteps == null || nextSteps.length == 0) {
			throw new ServiceException("No nextSteps specified for resultCode " + rc + "; Could not publish message chain");
		}
		// Add any blocks
		List<String> blockList = new ArrayList<String>();
		for(MessageChainStep waitFor : messageChain.getAllWaitFors()) {
			StringBuilder sb = new StringBuilder(messageChain.getGuid()).append(':').append(waitFor.getStepIndex());
			if(waitFor.isMulticast()) {
				String[] subs = publisher.getSubscriptions(waitFor.getQueueKey());
				sb.append('-');
				int len = sb.length();
				for(String sub : subs) {
					sb.setLength(len);
					sb.append(sub);
					blockList.add(sb.toString());
				}
			} else
				blockList.add(sb.toString());
		}
		if(!blockList.isEmpty()) {
			publisher.block(blockList.toArray(new String[blockList.size()]));
		}
		for(MessageChainStep nextStep : nextSteps) {
			EncryptionInfo encryptionInfo = (encryptionInfoMapping == null ? null : encryptionInfoMapping.findEncryptionInfo(nextStep.getQueueKey()));
			if(log.isInfoEnabled()) {
				StringBuilder sb = new StringBuilder();
				sb.append("Publishing message '").append(messageChain.getGuid()).append(':').append(messageChain.getInstance()).append("' to '").append(nextStep.getQueueKey());
				if(encryptionInfo != null && encryptionInfo.getCipherName() != null)
					sb.append(" with ").append(encryptionInfo.getCipherName()).append(" encryption");
				sb.append('\'');
				if(nextStep.getCorrelation() != null)
					sb.append(" (correlation=").append(nextStep.getCorrelation()).append(')');
				sb.append(": attributes=").append(nextStep.getAttributes());
				log.info(sb.toString());
			}
			byte[] chainBody = serialize(nextStep, encryptionInfo);
			if(log.isDebugEnabled())
				log.debug("Serialized Message Chain into " + chainBody.length + " bytes");
			Set<MessageChainStep> waitFors = nextStep.getWaitFors();
			String[] blocks;
			if(waitFors != null && !waitFors.isEmpty()) {
				blockList.clear();
				for(MessageChainStep waitFor : waitFors) {
					StringBuilder sb = new StringBuilder(messageChain.getGuid()).append(':').append(waitFor.getStepIndex());
					if(waitFor.isMulticast()) {
						String[] subs = publisher.getSubscriptions(waitFor.getQueueKey());
						sb.append('-');
						int len = sb.length();
						for(String sub : subs) {
							sb.setLength(len);
							sb.append(sub);
							blockList.add(sb.toString());
						}
					} else
						blockList.add(sb.toString());
				}
				blocks = blockList.toArray(new String[blockList.size()]);
			} else
				blocks = null;
			publisher.publish(nextStep.getQueueKey(), multicastOverride == null ? nextStep.isMulticast() : multicastOverride, nextStep.isTemporary(), new ByteArrayByteInput(chainBody), nextStep.getCorrelation(), qos, blocks);
		}
	}
	@Override
	protected WorkProcessor<ByteInput> getWorkProcessor() {
		return new MessageChainWorkProcessor();
	}

	/** Converts a MessageChain into a byte array representation
	 * @param mc The MessageChain to convert
	 * @return The byte array
	 * @throws ServiceException If the message chain could not be converted
	 */
	public static byte[] serialize(MessageChainStep step) throws ServiceException {
		return serialize(step, null);
	}

	/** Converts a MessageChain into a byte array representation
	 * @param mc The MessageChain to convert
	 * @return The byte array
	 * @throws ServiceException If the message chain could not be converted
	 */
	public static byte[] serialize(MessageChainStep step, EncryptionInfo encryptionInfo) throws ServiceException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			step.serialize(new OutputStreamByteOutput(baos), encryptionInfo);
		} catch(IOException e) {
			throw new ServiceException("Could not write MessageChain", e);
		}
		return baos.toByteArray();
	}

	protected MessageChain deserialize(ByteInput input) throws ServiceException {
		return deserializeMessageChain_v11(input, queue.getQueueKey(), isIncomingEncryptionRequired());
	}

	/** Converts ByteInput into a version 1.1 MessageChain
	 * @param input the ByteInput to convert
	 * @param queueKey the name of the queue from which the message was pulled
	 * @return the resultant MessageChain
	 * @throws ServiceException If the input could not be converted to a MessageChain
	 */
	public static MessageChain deserializeMessageChain_v11(ByteInput input, String queueKey, boolean encryptionRequired) throws ServiceException {
		try {
			MessageChain mc = new MessageChainV11();
			mc.read(input, encryptionRequired);
			return mc;
		} catch(IOException e) {
			throw new ServiceException("Could not read input as a MessageChain: " + input.toString(), e);
		}
	}

	/** Gets the {@link MessageChainTask} that will process messages obtained by this service
	 * @return
	 */
	public MessageChainTask getMessageChainTask() {
		return messageChainTask;
	}

	/** Sets the {@link MessageChainTask} that will process messages obtained by this service
	 * @param messageChainTask the new MessageChainTask to use
	 */
	public void setMessageChainTask(MessageChainTask messageChainTask) {
		this.messageChainTask = messageChainTask;
		updatePrerequisites();
	}		

	public AsyncTaskExecutor getAsyncTaskExecutor() {
		return asyncTaskExecutor;
	}

	public void setAsyncTaskExecutor(AsyncTaskExecutor asyncTaskExecutor) {
		this.asyncTaskExecutor = asyncTaskExecutor;
		asyncTaskExecutor.init();
	}

	@Override
	public void setQueue(WorkQueue<ByteInput> queue) {
		if(queue != null && !ByteInput.class.isAssignableFrom(queue.getWorkBodyType()))
			throw new IllegalArgumentException("The queue work body type '" + queue.getWorkBodyType().getName() + "' is not an instance of ByteInput");
		super.setQueue(queue);
		updatePrerequisites();
	}

	protected void updatePrerequisites() {
		MessageChainTask t = getMessageChainTask();
		WorkQueue<ByteInput> q = getQueue();
		if(t instanceof Prerequisite && q instanceof AbstractWorkQueue) {
			((AbstractWorkQueue<?>) q).getPrerequisiteManager().addPrerequisite((Prerequisite) t);
		}
	}

	public boolean isTimingEnabled() {
		return timingEnabled;
	}

	public void setTimingEnabled(boolean timingEnabled) {
		this.timingEnabled = timingEnabled;
	}

	public EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		this.encryptionInfoMapping = encryptionInfoMapping;
	}

	public TaskListener getTaskListener() {
		return taskListener;
	}

	public void setTaskListener(TaskListener taskListener) {
		this.taskListener = taskListener;
	}

	public boolean isDefaultResultEncryptionRequired() {
		return defaultResultEncryptionRequired;
	}

	public void setDefaultResultEncryptionRequired(boolean resultEncryptionRequired) {
		this.defaultResultEncryptionRequired = resultEncryptionRequired;
	}

	public boolean isResultEncryptionRequired(String queueKey) {
		Boolean override = resultEncryptionRequiredPerQueue.get(queueKey);
		return override != null ? override.booleanValue() : isDefaultResultEncryptionRequired();
	}

	public void setResultEncryptionRequired(String queueKey, boolean resultEncryptionRequired) {
		resultEncryptionRequiredPerQueue.put(queueKey, resultEncryptionRequired);
	}

	public boolean isIncomingEncryptionRequired() {
		return incomingEncryptionRequired;
	}

	public void setIncomingEncryptionRequired(boolean incomingEncryptionRequired) {
		this.incomingEncryptionRequired = incomingEncryptionRequired;
	}

	public boolean isPublisherGuardedFromInterrupt() {
		return publisherGuardedFromInterrupt;
	}

	public void setPublisherGuardedFromInterrupt(boolean publisherGuardedFromInterrupt) {
		this.publisherGuardedFromInterrupt = publisherGuardedFromInterrupt;
	}
}
