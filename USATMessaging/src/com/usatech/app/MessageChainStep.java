package com.usatech.app;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import simple.io.ByteOutput;
import simple.lang.Decision;
import simple.security.crypt.EncryptionInfo;

/** One step in a MessageChain. Typically, a component will process one step and then publish back to the queue system
 * the message chain which will now point to the next step.
 * @author Brian S. Krug
 *
 */
public interface MessageChainStep {

	public MessageChain getMessageChain();

	/** Whether this step should be sent to all consumers or just one
	 * @return
	 */
	public boolean isMulticast() ;
	/** The Map of name-value pairs of attributes for this step. This Map is read-only and will throw {@link java.lang.UnsupportedOperationException}
	 * if map modification methods are used
	 * @return
	 */
	public Map<String, Object> getAttributes();

	/**
	 * Copy the attributes from the given step to this step
	 * 
	 * @param step
	 */
	public void copyAttributes(MessageChainStep step, Decision<String> filter);

	/**
	 * Copy the attributes from the given step to this step
	 * 
	 * @param step
	 */
	public void copyAttributes(MessageChainStep step);

	/** The name of the queue from which this Step came or to which this step will be published
	 * @return
	 */
	public String getQueueKey();

	/** The Map of name-value pairs of result attributes from processing this Step. The component that processes the step should
	 *  update this Map with any result attributes from processing.
	 * @return
	 */
	public Map<String, Object> getResultAttributes();

	/** Returns the result code set for this step or -1 if it is not yet set
	 * @return
	 */
	public int getResultCode() ;

	/**
	 * Adds a boolean attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setAttribute(Attribute attribute, Object value) instead
	 */
	public void addBooleanAttribute(String name, boolean value);

	/**
	 * Adds a string attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setAttribute(Attribute attribute, Object value) instead
	 */
	public void addStringAttribute(String name, String value);

	/**
	 * Adds a literal attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * The message chain implementation chooses how to serialize the object based on its type.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setAttribute(Attribute attribute, Object value) instead
	 */
	public void addLiteralAttribute(String name, Object value);

	/** Adds an encrypted string attribute to the Step. The decrypted value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * The message chain implementation chooses how to serialize the object based on its type.
	 * @param name the unique name of the attribute
	 * @param unencryptedValue the unencrypted value of the attribute
	 * @param cipherName The name of the cipher to use for encryption
	 * @param keyAliasPrefix The prefix of the keyAlias to use (the alias in the truststore that matches this prefix and is sorted last is used)
	 */
	//public void addEncryptedAttribute(String name, String unencryptedValue, String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAliasPrefix) throws GeneralSecurityException ;

	/** Adds an encrypted byte array attribute to the Step. The decrypted value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * The message chain implementation chooses how to serialize the object based on its type.
	 * @param name the unique name of the attribute
	 * @param unencryptedValue the unencrypted value of the attribute
	 * @param cipherName The name of the cipher to use for encryption
	 * @param keyAliasPrefix The prefix of the keyAlias to use (the alias in the truststore that matches this prefix and is sorted last is used)
	 */
	//public void addEncryptedAttribute(String name, byte[] unencryptedValue, String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAliasPrefix) throws GeneralSecurityException ;

	/** Adds an encrypted byte array attribute to the Step. The decrypted value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * The message chain implementation chooses how to serialize the object based on its type.
	 * @param name the unique name of the attribute
	 * @param unencryptedValue a byte buffer containing the unencrypted value of the attribute. The position must be at the start of unencrypted data and the limit at the end.
	 * @param cipherName The name of the cipher to use for encryption
	 * @param keyAliasPrefix The prefix of the keyAlias to use (the alias in the truststore that matches this prefix and is sorted last is used)
	 */
	//public void addEncryptedAttribute(String name, ByteBuffer unencryptedValue, String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAliasPrefix) throws GeneralSecurityException ;

	/**
	 * Adds a secret attribute to the Step. A simple.bean.MaskedString of the value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setSecretAttribute(Attribute attribute, String value) instead
	 */
	public void addSecretAttribute(String name, String value) ;

	/**
	 * Adds a byte[] attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setAttribute(Attribute attribute, Object value) instead
	 */
	public void addByteArrayAttribute(String name, byte[] value);

	/**
	 * Adds a literal attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * The message chain handles the compress and decompress of the value provided.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setCompressedAttribute(Attribute attribute, Object value) instead
	 */
	public void addCompressedStringAttribute(String name, String value) throws IOException;

	/**
	 * Adds a byte[] attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * The message chain handles the compress and decompress of the value provided.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setCompressedAttribute(Attribute attribute, Object value) instead
	 */
	public void addCompressedByteArrayAttribute(String name, byte[] value) throws IOException;


	/**
	 * Adds a byte attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setAttribute(Attribute attribute, Object value) instead
	 */
	public void addByteAttribute(String name, byte value);

	/**
	 * Adds a short attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setAttribute(Attribute attribute, Object value) instead
	 */
	public void addShortAttribute(String name, short value);

	/**
	 * Adds a int attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setAttribute(Attribute attribute, Object value) instead
	 */
	public void addIntAttribute(String name, int value);

	/**
	 * Adds a long attribute to the Step. The value provided is what is obtained through <code>getAttributes().get(name)</code>.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param value
	 *            the value of the attribute
	 * @deprecated Use setAttribute(Attribute attribute, Object value) instead
	 */
	public void addLongAttribute(String name, long value);

	/**
	 * Adds a reference attribute to the Step. The value of the attribute obtained through <code>getAttributes().get(name)</code> will
	 * be the value of the result attribute of the specified Step (i.e. - <code>referencedStep.getAttributes().get(resultAttributeName)</code>).
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param referencedStep
	 *            the step that has (or will have) the resultAttribute
	 * @param resultAttributeName
	 *            the unique name of the referenced result attribute
	 * @deprecated Use setReferenceAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute) instead
	 */
	public void addReferenceAttribute(String name, MessageChainStep referencedStep, String resultAttributeName);

	/**
	 * Adds a reference attribute to the Step. The value of the attribute obtained through <code>getAttributes().get(name)</code> will
	 * be the last not null value of the result attribute of all the pass thru attributes added with this method
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param referencedStep
	 *            the step that has (or will have) the resultAttribute
	 * @param resultAttributeName
	 *            the unique name of the referenced result attribute
	 * @deprecated Use addPassThruReferenceAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute instead
	 */
	public void addPassThruReferenceAttribute(String name, MessageChainStep referencedStep, String resultAttributeName);

	/**
	 * Adds a reference map attribute to the Step. The value of the attribute obtained through <code>getAttributes().get(name)</code> will
	 * be a Map<String,Object> of all the values of the result attributes of the specified Step (i.e. - <code>referencedStep.getAttributes().get(resultAttributeName)</code>)
	 * that start with the given prefix. Care must be taken to choose a prefix that will not be used in any result attribute name.
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param referencedStep
	 *            the step that has (or will have) the resultAttribute
	 * @param resultAttributesPrefix
	 *            the prefix used to find the referenced result attributes
	 * @deprecated Use setReferenceMapAttribute(Attribute attribute, MessageChainStep referencedStep, String resultAttributesPrefix) instead
	 */
	public void addReferenceMapAttribute(String name, MessageChainStep referencedStep, String resultAttributesPrefix);

	/**
	 * Adds a reference attribute to the Step. The value of the attribute obtained through <code>getAttributes().get(name)</code> will
	 * be the last not null value of the result attribute of all the pass thru attributes added with this method
	 * 
	 * @param name
	 *            the unique name of the attribute
	 * @param referencedStep
	 *            the step that has (or will have) the resultAttribute
	 * @param resultAttributeName
	 *            the unique name of the referenced result attribute
	 */
	public void addPassThruReferenceAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute);

	/** Removes the specified attribute from the Step.
	 * @param name the unique name of the attribute
	 */
	public void removeAttribute(String name) ;

	/** Sets a mapping for the given resultCode to the Step that should be processed when this resultCode is returned. The <code>nextStep</code>
	 * should be obtained by using {@link MessageChain#addStep(String)} prior to calling this method.
	 * @param resultCode the result that will pass processing to the indicated step
	 * @param nextSteps the step which will be processed next if the specified result code is returned from processing this MessageChainStep
	 */
	public void setNextSteps(int resultCode, MessageChainStep... nextSteps);

	/**
	 * Adds to the mapping for the given resultCode to the Step that should be processed when this resultCode is returned. The <code>nextStep</code> should be obtained by using
	 * {@link MessageChain#addStep(String)} prior to calling this method.
	 * 
	 * @param resultCode
	 *            the result that will pass processing to the indicated step
	 * @param nextSteps
	 *            the step which will be processed next if the specified result code is returned from processing this MessageChainStep
	 */
	public void addNextSteps(int resultCode, MessageChainStep... nextSteps);

	/** Finds the step that will be next if the resultCode is set to the specified value
	 * @param resultCode
	 * @return
	 */
	public MessageChainStep[] getNextSteps(int resultCode) ;

	/**
	 * Removes from the mapping for the given resultCode to the Step that should be processed when this resultCode is returned. The <code>nextStep</code> should be obtained by using
	 * {@link MessageChain#addStep(String)} prior to calling this method.
	 * 
	 * @param resultCode
	 *            the result that will pass processing to the indicated step
	 * @param nextSteps
	 *            the steps which will be no longer be processed next if the specified result code is returned from processing this MessageChainStep
	 */
	public void removeNextSteps(int resultCode, MessageChainStep... nextSteps);

	/** The set of configured resultCodes for this Step
	 * @return an unmodifiable set of the resultCodes defined for this Step
	 */
	public Set<Integer> getResultCodes() ;
	/** Creates a new MessageChainStep and adds it to this MessageChain, then returns the new MessageChainStep
	 * @param queueKey the queue key of the new step
	 * @return the new MessageChainStep
	 */
	public MessageChainStep addStep(String queueKey);
	/** Creates a new MessageChainStep and adds it to this MessageChain, then returns the new MessageChainStep
	 * @param queueKey the queue key of the new step
	 * @param multicast whether to send to all consumers or just one
	 * @return the new MessageChainStep
	 */
	public MessageChainStep addStep(String queueKey, boolean multicast);
	/** Creates a new MessageChainStep and adds it to this MessageChain, then returns the new MessageChainStep
	 * @param queueKey the queue key of the new step
	 * @param multicast whether to send to all consumers or just one
	 * @param copyAttributes whether to copy attributes
	 * @return the new MessageChainStep
	 */
	public MessageChainStep addStep(String queueKey, boolean multicast, boolean copyAttributes);
	
	/** Makes a deep copy of the <code>this</code> step and adds it to the message chain
	 * @return
	 */
	public MessageChainStep copyStep() ;

	/** Retrieves an attribute from this step and converts it to the requested class. If required = true, this method also ensures that the
	 *  returned value is not null.
	 * @param <T>
	 * @param attribute The attribute to lookup (recommended practice is to create an enum that implements this interface such that getValue()
	 * returns a unique String for each enum member)
	 * @param convertTo The class to which the attribute value is converted
	 * @param required Whether to enforce that the returned value is not null
	 * @return The attribute value
	 * @throws AttributeConversionException If the attribute value cannot be converted to the request class or if it is required and not provided
	 */
	public <T> T getAttribute(Attribute attribute, Class<T> convertTo, boolean required) throws AttributeConversionException ;

	<T> T getAttribute(String attributeName, Class<T> convertTo, boolean required) throws AttributeConversionException;

	/** Retrieves an attribute from this step and converts it to the requested class. If the attribute value is null, the defaultValue is returned instead.
	 * @param <T>
	 * @param attribute The attribute to lookup (recommended practice is to create an enum that implements this interface such that getValue()
	 * returns a unique String for each enum member)
	 * @param convertTo The class to which the attribute value is converted
	 * @param defaultValue The object to return if the attribute value is null
	 * @return The attribute value (or the defaultValue if attribute value is null)
	 * @throws AttributeConversionException If the attribute value cannot be converted to the request class
	 */
	public <T> T getAttributeDefault(Attribute attribute, Class<T> convertTo, T defaultValue) throws AttributeConversionException ;

	/** Retrieves an attribute from this step and converts it to the requested class. If the attribute value is null or a conversion exception occurs, the defaultValue is returned instead.
	 * @param <T>
	 * @param attribute The attribute to lookup (recommended practice is to create an enum that implements this interface such that getValue()
	 * returns a unique String for each enum member)
	 * @param convertTo The class to which the attribute value is converted
	 * @param defaultValue The object to return if the attribute value is null
	 * @return The attribute value (or the defaultValue if attribute value is null)
	 */
	public <T> T getAttributeSafely(Attribute attribute, Class<T> convertTo, T defaultValue) ;

	/** Sets the attribute to the specified value. If value is null, the attribute is removed.
	 *  This method is a replacement for addLiteralAttribute().
	 * @param attribute The attribute to set
	 * @param value The value to set it to.
	 */
	public void setAttribute(Attribute attribute, Object value) ;

    void setAttribute(String attributeName, Object value);

	/** Retrieves an attribute from this step and converts it to the requested class. If required = true, this method also ensures that the
	 *  returned value is not null.
	 * @param <T>
	 * @param attribute The attribute to lookup (recommended practice is to create an enum that implements this interface such that getValue()
	 * returns a unique String for each enum member)
	 * @param convertTo The class to which the attribute value is converted
	 * @param required Whether to enforce that the returned value is not null
	 * @return The attribute value
	 * @throws AttributeConversionException If the attribute value cannot be converted to the request class or if it is required and not provided
	 */
	public <T> T getIndexedAttribute(Attribute attribute, int index, Class<T> convertTo, boolean required) throws AttributeConversionException ;

	/** Retrieves an attribute from this step and converts it to the requested class. If the attribute value is null, the defaultValue is returned instead.
	 * @param <T>
	 * @param attribute The attribute to lookup (recommended practice is to create an enum that implements this interface such that getValue()
	 * returns a unique String for each enum member)
	 * @param convertTo The class to which the attribute value is converted
	 * @param defaultValue The object to return if the attribute value is null
	 * @return The attribute value (or the defaultValue if attribute value is null)
	 * @throws AttributeConversionException If the attribute value cannot be converted to the request class
	 */
	public <T> T getIndexedAttributeDefault(Attribute attribute, int index, Class<T> convertTo, T defaultValue) throws AttributeConversionException ;

	/** Sets the attribute to the specified value. If value is null, the attribute is removed.
	 *  This method is a replacement for addLiteralAttribute().
	 * @param attribute The attribute to set
	 * @param value The value to set it to.
	 */
	public void setIndexedAttribute(Attribute attribute, int index, Object value) ;

	/**
	 * Sets the attribute to the specified value as a MaskedString. This will mask the value in the toString() of the attributes Map. If value is null, the attribute is removed.
	 * 
	 * @param attribute
	 *            The attribute to set
	 * @param value
	 *            The value to set it to.
	 */
	public void setIndexedSecretAttribute(Attribute attribute, int index, String value);

	public <T> T getOptionallyIndexedAttribute(Attribute attribute, int index, Class<T> convertTo, boolean required) throws AttributeConversionException;

	public <T> T getOptionallyIndexedAttributeDefault(Attribute attribute, int index, java.lang.Class<T> convertTo, T defaultValue) throws AttributeConversionException;

	public void setOptionallyIndexedAttribute(Attribute attribute, int index, Object value);

	public void setOptionallyIndexedSecretAttribute(Attribute attribute, int index, String value);

	/**
	 * Sets the attribute to the specified value as a MaskedString. This will mask the value in the toString() of the attributes Map. If value is null, the attribute is removed.
	 * This method is a replacement for addSecretAttribute().
	 * 
	 * @param attribute
	 *            The attribute to set
	 * @param value
	 *            The value to set it to.
	 */
	public void setSecretAttribute(Attribute attribute, String value) ;

	/** Sets the attribute to the specified value and compresses it when writing. The value is converted to a byte[] or a String for compression.
	 *  If value is null, the attribute is removed.
	 *  This method is a replacement for addCompressedByteArrayAttribute() and addCompressedStringAttribute().
	 * @param attribute The attribute to set
	 * @param value The value to set it to.
	 */
	public void setCompressedAttribute(Attribute attribute, Object value) throws IOException ;

	/** Sets a reference attribute to the Step. The value of the attribute obtained through <code>getAttributes().get(name)</code> will
	 * be the value of the result attribute of the specified Step (i.e. - <code>referencedStep.getAttributes().get(resultAttributeName)</code>).
	 * @param attribute The attribute to set
	 * @param referencedStep the step that has (or will have) the resultAttribute
	 * @param resultAttribute  The attribute to reference in the result attributes of the referencedStep
	 */
	public void setReferenceAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute);

	/**
	 * Sets a reference attribute to the Step. The value of the attribute obtained through <code>getAttributes().get(name)</code> will
	 * be the value of the result attribute of the specified Step (i.e. - <code>referencedStep.getAttributes().get(resultAttributeName)</code>).
	 * 
	 * @param attribute
	 *            The attribute to set
	 * @param referencedStep
	 *            the step that has (or will have) the resultAttribute
	 * @param resultAttribute
	 *            The attribute to reference in the result attributes of the referencedStep
	 */
	public void setReferenceIndexedAttribute(Attribute attribute, int index, MessageChainStep referencedStep, Attribute resultAttribute, int resultIndex);

	/**
	 * Sets a reference attribute to the Step. The value of the attribute obtained through <code>getAttributes().get(name)</code> will
	 * be the value of the result attribute of the specified Step (i.e. - <code>referencedStep.getAttributes().get(resultAttributeName)</code>).
	 * 
	 * @param attribute
	 *            The attribute to set
	 * @param referencedStep
	 *            the step that has (or will have) the resultAttribute
	 * @param resultAttribute
	 *            The attribute to reference in the result attributes of the referencedStep
	 */
	public void setReferenceIndexedAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute, int resultIndex);

	/**
	 * Sets a reference attribute to the Step. The value of the attribute obtained through <code>getAttributes().get(name)</code> will
	 * be the value of the result attribute of the specified Step (i.e. - <code>referencedStep.getAttributes().get(resultAttributeName)</code>).
	 * 
	 * @param attribute
	 *            The attribute to set
	 * @param referencedStep
	 *            the step that has (or will have) the resultAttribute
	 * @param resultAttribute
	 *            The attribute to reference in the result attributes of the referencedStep
	 */
	public void setReferenceIndexedAttribute(Attribute attribute, int index, MessageChainStep referencedStep, Attribute resultAttribute);

	public void setReferenceOptionallyIndexedAttribute(Attribute attribute, int index, MessageChainStep referencedStep, Attribute resultAttribute, int resultIndex);

	public void setReferenceOptionallyIndexedAttribute(Attribute attribute, MessageChainStep referencedStep, Attribute resultAttribute, int resultIndex);

	public void setReferenceOptionallyIndexedAttribute(Attribute attribute, int index, MessageChainStep referencedStep, Attribute resultAttribute);

	/** Sets a reference map attribute to the Step. The value of the attribute obtained through <code>getAttributes().get(name)</code> will
	 * be a Map<String,Object> of all the values of the result attributes of the specified Step (i.e. - <code>referencedStep.getAttributes().get(resultAttributeName)</code>)
	 * that start with the given prefix. Care must be taken to choose a prefix that will not be used in any result attribute name.
	 * @param attribute The attribute to set
	 * @param referencedStep the step that has (or will have) the resultAttribute
	 * @param resultAttributesPrefix the prefix used to find the referenced result attributes
	 */
	public void setReferenceMapAttribute(Attribute attribute, MessageChainStep referencedStep, String resultAttributesPrefix);

	public void setResultAttribute(Attribute attribute, Object value);

    void setResultAttribute(String attributeName, Object value);

	public void setIndexedResultAttribute(Attribute attribute, int index, Object value);

	public void setOptionallyIndexedResultAttribute(Attribute attribute, int index, Object value);

	public void setSecretResultAttribute(Attribute attribute, String value);

    void setSecretResultAttribute(String attribute, String value);

	public MixedValue createMixedAttribute(Attribute attribute);

	public boolean swapReferences(Attribute originalAttribute, MessageChainStep newReferencedStep, Attribute newAttribute);

	public boolean swapIndexedReferences(Attribute originalAttribute, int originalIndex, MessageChainStep newReferencedStep, Attribute newAttribute, int newIndex);

	public void setQueueKey(String queueKey);
	/**
	 * Sets the result attribute to the specified value as a MaskedString. This will mask the value in the toString() of the attributes Map. If value is null, the attribute is removed.
	 * 
	 * @param attribute
	 *            The attribute to set
	 * @param value
	 *            The value to set it to.
	 */
	public void setIndexedSecretResultAttribute(Attribute attribute, int index, String value);

	public boolean isTemporary() ;
	public void setTemporary(boolean temporary) ;
	
	/** Writes this MessageChain to the provided output. If resultCode is set, the nextStep is used as the currentStep
	 * in the serialization (so that when is it read again, the nextStep of this MessageChain will be the currentStep of
	 * the MessageChain that invokes read() on the same stream of bytes).
	 * @param output the ByteOuput to which to write
	 * @throws IOException
	 */
	public void serialize(ByteOutput output, EncryptionInfo encryptionInfo) throws IOException;

	public Long getCorrelation();

	public void setCorrelation(Long correlation);

	public Set<MessageChainStep> getWaitFors();

	public void addWaitFor(MessageChainStep waitFor);

	public void removeWaitFor(MessageChainStep waitFor);

	public int getStepIndex();

	public void removeDownstreamJoinedSteps();

	public boolean isJoined();

	public void setJoined(boolean joined);
	
	public MessageChainStep getPreviousStep();
}