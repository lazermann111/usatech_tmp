package com.usatech.app;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import simple.app.PrerequisiteManager;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.event.ProgressListener;
import simple.io.ByteInput;
import simple.io.IOUtils;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.server.NIORequestResponse;
import simple.io.server.RequestResponseNIOServer;
import simple.util.concurrent.CreationException;

public class SocketWorkQueue extends RequestResponseNIOServer implements WorkQueue<ByteInput> {
	private static final Log log = Log.getLog();
	protected final PrerequisiteManager prerequisiteManager = new PrerequisiteManager();

	protected class SocketConnectionRetriever implements Retriever<ByteInput>, Publisher<ByteInput> {
		//defined here so it doesn't need to be sychronized
		protected final Map<String,Map<InetSocketAddress,Boolean>> sameAddressCache = new HashMap<String, Map<InetSocketAddress,Boolean>>();
		protected NIORequestResponse rr;
		public boolean cancel(Thread thread) {
			SocketWorkQueue.this.cancel(thread);
			return true;
		}
		public void close() {
			try {
				invalidateObject(rr);
				rr = null;
			} catch(NullPointerException e) {
			}
		}
		/**
		 * @see simple.app.WorkQueue.Retriever#arePrequisitesAvailable()
		 */
		@Override
		public boolean arePrequisitesAvailable() {
			return prerequisiteManager.arePrequisitesAvailable();
		}
		/**
		 * @see simple.app.WorkQueue.Retriever#cancelPrequisiteCheck(java.lang.Thread)
		 */
		@Override
		public void cancelPrequisiteCheck(Thread thread) {
			prerequisiteManager.cancelPrequisiteCheck(thread);
		}
		
		public void resetAvailability() {
			prerequisiteManager.resetAvailability();
		}

		public Work<ByteInput> next() throws WorkQueueException, InterruptedException {
			try {
				rr = borrowObject();
			} catch(TimeoutException e) {
				log.info("Timed out getting next connection");
				return null;
			} catch(CreationException e) {
				throw new WorkQueueException(e.getCause());
			}
			if(rr == null) return null;
			final ByteInput body;
			try {
				body = new InputStreamByteInput(rr.getInputStream());
			} catch(IOException e) {
				throw new WorkQueueException(e);
			}
			final String guid = "" + rr.getSocket().getLocalPort() + ":" + System.currentTimeMillis();
			return new Work<ByteInput>() {
				public String getGuid() {
					return guid;
				}
				@Override
				public QoS getQoS() {
					// TODO Implement this
					return null;
				}
				@Override
				public Map<String, ?> getWorkProperties() {
					return null;
				}
				public long getEnqueueTime() {
					return 0;
				}
				public boolean isRedelivered() {
					return false;
				}
				public boolean isUnguardedRetryAllowed() {
					return true;
				}
				public int getRetryCount() {
					return -1;
				}
				public ByteInput getBody() {
					return body;
				}
				public Publisher<ByteInput> getPublisher() throws WorkQueueException {
					return SocketConnectionRetriever.this;
				}
				public boolean isComplete() {
					return rr == null;
				}
				public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
					if(rr == null)
						throw new WorkQueueException("Work is already complete; workAborted() may not be called");
					//XXX: For now we will not support retries
					prerequisiteManager.resetAvailability();
					invalidateObject(rr);
					rr = null;
				}

				public void workComplete() throws WorkQueueException {
					if(rr == null)
						throw new WorkQueueException("Work is already complete; workComplete() may not be called");
					returnObject(rr);
					rr = null;
				}
				/**
				 * @see java.lang.Object#toString()
				 */
				@Override
				public String toString() {
					return "Socket Request " + rr;
				}
				public String getOriginalQueueName() {
					return null;
				}
				public String getQueueName() {
					return getQueueKey();
				}
				public void unblock(String... blocksToClear) {
				}
				public String getSubscription() {
					return null;
				}
			};
		}
		public void publish(String queueKey, ByteInput content) throws ServiceException {
			publish(queueKey, false, false, content);
		}
		public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content) throws ServiceException {
			publish(queueKey, multicast, temporary, content, null, null);
		}
		@Override
		public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content, Long correlation, simple.app.QoS qos, String... blocks) throws ServiceException {
			//TODO: Support QoS settings
			if(multicast)
				log.info("Sockets do not support multicast at the publisher");
			if(qos != null && qos.getAvailable() > 0 && qos.getAvailable() > System.currentTimeMillis()) {
				throw new ServiceException("Delayed execute is not supported");
			}
			if(queueKey == null || isInetAddressSame(queueKey, (InetSocketAddress)rr.getSocket().getLocalSocketAddress())) {
				try {
					content.copyInto(rr.getOutputStream());
				} catch(IOException e) {
					throw new ServiceException(e);
				}
			} else {
				//do we handle this?
				throw new ServiceException("Publish to different SocketAddress is not supported");
			}
		}

		protected boolean isInetAddressSame(String addressString, InetSocketAddress address) {
			Map<InetSocketAddress, Boolean> m = sameAddressCache.get(addressString);
			if(m == null) {
				m = new HashMap<InetSocketAddress, Boolean>();
				sameAddressCache.put(addressString, m);
			} else {
				Boolean b = m.get(address);
				if(b != null)
					return b;
			}
			boolean same = IOUtils.isInetAddressSame(addressString, address);
			m.put(address, same);
			return same;
		}

		@Override
		public int getConsumerCount(String queueName, boolean temporary) {
			Socket socket = rr.getSocket();
			return socket == null || !socket.isConnected() ? 0 : 1;
		}

		public String[] getSubscriptions(String queueKey) throws ServiceException {
			return null;
		}

		@Override
		public void block(String... blocks) throws ServiceException {
			throw new ServiceException("Blocking is not supported in this publisher implementation");
		}
	}

	public String getQueueKey() {
		return String.valueOf(getSocketAddress());
	}

	public Retriever<ByteInput> getRetriever(ProgressListener listener) throws WorkQueueException {
		return new SocketConnectionRetriever();
	}

	public Class<ByteInput> getWorkBodyType() {
		return ByteInput.class;
	}

	public boolean isAutonomous() {
		return true;
	}
	public PrerequisiteManager getPrerequisiteManager() {
		return prerequisiteManager;
	}

	/**
	 * @see simple.app.WorkQueue#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() {
		close();
		return TRUE_FUTURE;
	}

}
