package com.usatech.app;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.Call;
import simple.db.Call.BatchUpdate;
import simple.db.CallNotFoundException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;
import simple.io.Log;

public class ParallelBatchUpdateDatasetHandler extends AbstractAttributeDatasetHandler<ServiceException> {
	private final static Log log = Log.getLog();
	protected final ExecutorService executor;
	protected final Call call;
	protected BatchUpdate batch;
	protected final int maxBatchSize;
	protected final Map<String, Object> defaultData;
	protected final Queue<Future<int[]>> results = new ConcurrentLinkedQueue<>();
	protected int batchCount = 0;
	protected int retries = 3;
	protected String callId="";

	public ParallelBatchUpdateDatasetHandler(String callId, int maxBatchSize, ExecutorService executor) throws ParameterException, CallNotFoundException {
		this(callId, maxBatchSize, executor, null);
		this.callId=callId;
	}

	public ParallelBatchUpdateDatasetHandler(String callId, int maxBatchSize, ExecutorService executor, Map<String, Object> defaultData) throws ParameterException, CallNotFoundException {
		this(DataLayerMgr.getGlobalDataLayer().findCall(callId), maxBatchSize, executor, defaultData);
		this.callId=callId;
	}

	public ParallelBatchUpdateDatasetHandler(Call call, int maxBatchSize, ExecutorService executor, Map<String, Object> defaultData) throws ParameterException {
		this.call = call;
		this.maxBatchSize = maxBatchSize;
		this.executor = executor;
		this.defaultData = defaultData;
		this.batch = call.createBatchUpdate(false, 0.0f);
	}

	public void handleDatasetEnd() throws ServiceException {
		while(batch.getPendingCount() > 0)
			executeBatch();
		// await completion and detect and throw exceptions
		log.info("Waiting on " + results.size() + " batch sets for completion callId="+callId);
		try {
			for(Future<int[]> result : results) {
				try {
					result.get();
				} catch(InterruptedException e) {
					throw new ServiceException(e);
				} catch(ExecutionException e) {
					if(e.getCause() instanceof ServiceException)
						throw (ServiceException) e.getCause();
					throw new ServiceException(e);
				}
			}
		} finally {
			results.clear();
		}
		log.info("All batch sets (" + batchCount + ") are complete callId="+callId);
	}

	public void handleDatasetStart(String[] columnNames) throws ServiceException {
		log.info("Starting processing of batch sets callId="+callId);
	}

	@Override
	public void handleRowStart() {
		super.handleRowStart();
		if(defaultData != null)
			data.putAll(defaultData);

	}
	public void handleRowEnd() throws ServiceException {
		try {
			batch.addBatch(data);
		} catch(ParameterException e) {
			throw new ServiceException(e);
		}
		data.clear();
		if(batch.getPendingCount() >= maxBatchSize)
			executeBatch();
	}

	protected void executeBatch() throws ServiceException {
		Iterator<Future<int[]>> iter = results.iterator();
		int waiting = 0;
		int finished = 0;
		while(iter.hasNext()) {
			Future<int[]> result = iter.next();
			if(result.isDone()) {
				try {
					result.get();
				} catch(InterruptedException e) {
					throw new ServiceException(e); // this really should never happen
				} catch(ExecutionException e) {
					iter.remove();
					if(e.getCause() instanceof ServiceException)
						throw (ServiceException) e.getCause();
					throw new ServiceException(e);
				}
				iter.remove();
				finished++;
			} else
				waiting++;
		}
		final BatchUpdate currentBatch = this.batch;
		final int n = ++this.batchCount;
		handleBatchPrepare(n);
		if(n % 10 == 0)
			log.info("Cleared " + finished + " completed batch sets, " + waiting + " pending batch sets, " + (n - 1) + " total batch sets submitted callId="+callId);
		results.add(executor.submit(new Callable<int[]>() {
			@Override
			public int[] call() throws ServiceException {
				int[] result;
				Connection conn;
				int i = 0;
				while(true) {
					try {
						conn = DataLayerMgr.getConnection(call.getDataSourceName());
					} catch(SQLException | DataLayerException e) {
						if(i++ < getRetries()) {
							log.info("Could not get connection; retrying...callId="+callId, e);
							continue;
						}
						log.warn("Failed to execute batch set " + n+" callId="+callId, e);
						throw new ServiceException(e);
					}
					try {
						result = currentBatch.executeBatch(conn, maxBatchSize);
						if(!conn.getAutoCommit())
							conn.commit();
						handleBatchComplete(n);
					} catch(ParameterException e) {
						log.warn("Failed to execute batch set " + n+" callId="+callId, e);
						throw new ServiceException(e);
					} catch(SQLException e) {
						if(DatabasePrerequisite.determineRetryType(e) != WorkRetryType.NO_RETRY && i++ < getRetries()) {
							log.info("Could not execute batch; retrying...callId="+callId, e);
							continue;
						}
						log.warn("Failed to execute batch set " + n+" callId="+callId, e);
						throw DatabasePrerequisite.createServiceException(e);
					} finally {
						try {
							conn.close();
						} catch(SQLException e) {
							log.warn("Could not close connection callId="+callId, e);
						}
					}
					return result;
				}
			}
		}));

		try {
			this.batch = call.createBatchUpdate(false, 0.0f);
		} catch(ParameterException e) {
			throw new ServiceException(e);
		}
	}

	protected void handleBatchPrepare(int batchNumber) {
		// Hook for sub-classes
	}

	protected void handleBatchComplete(int batchNumber) {
		// Hook for sub-classes
	}

	public int getRetries() {
		return retries;
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}
}