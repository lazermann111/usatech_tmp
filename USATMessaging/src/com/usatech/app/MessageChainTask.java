package com.usatech.app;


import simple.app.ServiceException;

/** The processor of one piece of the USAT message chain. This is the basic unit of the processing system.
 *  An implementation of this interface should be created for each task that needs to be performed.
 * @author Brian S. Krug
 *
 */
public interface MessageChainTask {
	/** Processes a message with the specified attributes, updates the resultAttributes, and returns a resultCode
	 * that is used to determine what to do next.
	 * @param taskInfo The information on the task to be processed
	 * @return The result of processing
	 * @throws ServiceException If processing could not complete as expected
	 */
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException ;
}
