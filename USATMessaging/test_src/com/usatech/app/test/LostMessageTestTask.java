/**
 *
 */
package com.usatech.app.test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.util.MapBackedSet;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;

/**
 * @author Brian S. Krug
 *
 */
public class LostMessageTestTask implements MessageChainTask {
	private static Log log = Log.getLog();
	protected static final Attribute ATTR_SEQUENCE = new Attribute() {
		@Override
		public String getValue() {
			return "sequence";
		}		
	};
	protected final Set<Long> inflight = new MapBackedSet<Long>(new ConcurrentHashMap<Long, Object>());
	protected Publisher<ByteInput> publisher;
	protected int requests = 1000;
	protected String requestQueue;
	protected String responseQueue;
	protected boolean temporary = true;
	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		final long n;
		try {
			n = taskInfo.getStep().getAttribute(ATTR_SEQUENCE, Long.class, true);
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		}
		if(inflight.remove(n)) {
			log.info("Received response for n=" + n + "; waiting for " + inflight.size());
			if(inflight.isEmpty()) {
				log.info("TEST COMPLETE WITHOUT ERRORS");
			}
		} else
			log.warn("Response for n=" + n + " already received!");
		return 0;
	}
	public void initialize() {
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(5000);
				} catch(InterruptedException e) {
					//
				}
				final int r = getRequests();
				for(int i = 1; i <= r; i++) {
					final MessageChain mc = new MessageChainV11();
					final MessageChainStep step = mc.addStep(getRequestQueue());
					final MessageChainStep responseStep = mc.addStep(getResponseQueue());
					step.setNextSteps(0, responseStep);
					step.setAttribute(ATTR_SEQUENCE, i);
					step.setTemporary(isTemporary());
					responseStep.setAttribute(ATTR_SEQUENCE, i);
					responseStep.setTemporary(isTemporary());
					inflight.add((long)i);
					try {
						MessageChainService.publish(mc, getPublisher());
					} catch(ServiceException e) {
						log.error("Could not publish message chain", e);
						return;
					}
				}
				log.info("Published " + r + " requests");
			}
		});
		t.start();
	}
	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
	public int getRequests() {
		return requests;
	}
	public void setRequests(int requests) {
		this.requests = requests;
	}
	public String getRequestQueue() {
		return requestQueue;
	}
	public void setRequestQueue(String requestQueue) {
		this.requestQueue = requestQueue;
	}
	public String getResponseQueue() {
		return responseQueue;
	}
	public void setResponseQueue(String responseQueue) {
		this.responseQueue = responseQueue;
	}
	public boolean isTemporary() {
		return temporary;
	}
	public void setTemporary(boolean temporary) {
		this.temporary = temporary;
	}

}
