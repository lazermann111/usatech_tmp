package com.usatech.app.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.event.TaskListener;
import simple.io.Log;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;

public class IncrementingComponent implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final Map<String,Object> emptyMap = Collections.emptyMap();
	protected String publishTo = "test.increment.all";
	protected int limit = 100;
	protected int[] resultCodes;
	protected TaskListener taskListener;
	protected static final List<Long> times = new ArrayList<Long>();
	protected Integer correlationDivider;
	protected long processingSleep;

	/*
	public ComponentResult process(String serviceName, Map<String, Object> attributes, Publisher<ByteInput> publisher) throws ServiceException {
		int n;
		try {
			n = ConvertUtils.getInt(attributes.get("N"), 0);
		} catch(ConvertException e) {
			throw new ServiceException("Could not convert N", e);
		}
		log.debug("Got number " + n + " from " + serviceName);
		if(n >= limit) {
			log.debug("Reached limit; ending...");
		} else {
			log.debug("Publishing number " + (n + 1) + " to " + getPublishTo());
			MessageChain mc = new MessageChain(getPublishTo(), Collections.singletonMap("N", (Object)(n+1)));
			ComponentService.publish(mc, publisher);
		}
		if(getResultCodes() != null && getResultCodes().length > 0) {
			int rc = resultCodes[(int)(resultCodes.length * Math.random())];
			return new ComponentResult(rc, emptyMap);
		} else
			return RESULT_OK;
	}*/
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String,Object> attributes = taskInfo.getStep().getAttributes();
		if(taskListener != null) taskListener.taskStarted("Incrementing Test", attributes.toString());
		int n;
		try {
			n = ConvertUtils.getInt(attributes.get("N"), 0);
		} catch(ConvertException e) {
			throw new ServiceException("Could not convert N", e);
		}
		log.debug("Got number " + n + " from " + taskInfo.getServiceName());
		Long start;
		try {
			start = ConvertUtils.convert(Long.class, attributes.get("start"));
		} catch(ConvertException e) {
			throw new ServiceException("Could not convert start", e);
		}
		if(start == null)
			start = System.currentTimeMillis();
		if(n >= limit) {
			long time = System.currentTimeMillis() - start;
			long total = 0;
			long count = 0;
			synchronized(times) {
				times.add(time);
				for(Long l : times) {
					total += l;
					count++;
				}
			}
			log.info("Processed " + n + " hops in " + time + " ms for an average of " + (total/count) + " over " + count + " instances");
			log.debug("Reached limit; ending...");
		} else {
			long ps = getProcessingSleep();
			if(ps > 0)
				try {
					Thread.sleep(ps);
				} catch(InterruptedException e) {
					// ignore
				}
			Long corr;
			if(correlationDivider == null)
				corr = taskInfo.getStep().getCorrelation();
			else
				corr = (long) (n + 1) / correlationDivider;
			log.debug("Publishing number " + (n + 1) + (corr == null ? "" : " (corr=" + corr + ")") + " to " + getPublishTo());
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep(getPublishTo());
			step.addStringAttribute("N", String.valueOf(n+1));
			step.addLongAttribute("start", start);
			MessageChainStep retryStep = mc.addStep(getPublishTo());
			retryStep.addStringAttribute("N", String.valueOf(n+1));
			if(corr != null) {
				step.setCorrelation(corr);
				retryStep.setCorrelation(corr);
			}
			for(Map.Entry<String, Object> entry : taskInfo.getStep().getAttributes().entrySet()) {
				if("N".equals(entry.getKey()) || "start".equals(entry.getKey()))
					continue;
				step.addLiteralAttribute(entry.getKey(), entry.getValue());
				retryStep.addLiteralAttribute(entry.getKey(), entry.getValue());
			}
			step.setNextSteps(255, retryStep);
			MessageChainService.publish(mc, taskInfo.getPublisher(), null, taskInfo.getCurrentQos());
		}
		if(taskListener != null) taskListener.taskEnded("Incrementing Test");
		if(getResultCodes() != null && getResultCodes().length > 0) {
			int rc = resultCodes[(int)(resultCodes.length * Math.random())];
			return rc;
		} else
			return 0;
	}
	public String getPublishTo() {
		return publishTo;
	}
	public void setPublishTo(String publishTo) {
		this.publishTo = publishTo;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int[] getResultCodes() {
		return resultCodes;
	}
	public void setResultCodes(int[] resultCodes) {
		this.resultCodes = resultCodes;
	}
	public TaskListener getTaskListener() {
		return taskListener;
	}
	public void setTaskListener(TaskListener taskListener) {
		this.taskListener = taskListener;
	}

	public Integer getCorrelationDivider() {
		return correlationDivider;
	}

	public void setCorrelationDivider(Integer correlationDivider) {
		this.correlationDivider = correlationDivider;
	}

	public long getProcessingSleep() {
		return processingSleep;
	}

	public void setProcessingSleep(long processingSleep) {
		this.processingSleep = processingSleep;
	}
}
