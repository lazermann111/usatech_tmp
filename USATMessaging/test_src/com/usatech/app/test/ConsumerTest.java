package com.usatech.app.test;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class ConsumerTest {

	public static void scenario1()throws Exception{
		System.out.println("in s1: ");
		Connection conn = new ConnectionFactory().newConnection("localhost", 5672);
		Channel ch = conn.createChannel();
		String realm="/data";
		String queueName="testqueue";
		String consumerTag="testconsumerTag";
		int ticket = ch.accessRequest("/data");
        ch.queueDeclare(ticket, queueName);
		QueueingConsumer consumer = new QueueingConsumer(ch);
		ch.basicConsume(ticket, queueName, false, consumerTag, consumer);
        int count=0;
        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            System.out.println("Message: " + new String(delivery.getBody()));
            ch.basicCancel(consumerTag);
            //Thread.sleep(1000*60);
            System.out.println("after cancel: ");
            ch.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println("after ack: ");
         	//return;
        }
	}
	
	public static void scenario2()throws Exception{
		System.out.println("in s2: ");
		Connection conn = new ConnectionFactory().newConnection("localhost", 5672);
		Channel ch = conn.createChannel();
		String realm="/data";
		String queueName="testqueue";
		String consumerTag="testconsumerTag";
		int ticket = ch.accessRequest("/data");
        ch.queueDeclare(ticket, queueName);
		QueueingConsumer consumer = new QueueingConsumer(ch);
		ch.basicConsume(ticket, queueName, false, consumerTag, consumer);
        int count=0;
        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            System.out.println("Message: " + new String(delivery.getBody()));
            ch.basicCancel(consumerTag);
            ch.basicConsume(ticket, queueName, false, consumerTag, consumer);
            //Thread.sleep(1000*60);
            System.out.println("after cancel: ");
            ch.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println("after ack: ");
         	//return;
        }
	}
	
	public static void scenario3()throws Exception{
		System.out.println("in s2: ");
		Connection conn = new ConnectionFactory().newConnection("localhost", 5672);
		Channel ch = conn.createChannel();
		String realm="/data";
		String queueName="testqueue";
		String consumerTag="testconsumerTag";
		int ticket = ch.accessRequest("/data");
        ch.queueDeclare(ticket, queueName);
		QueueingConsumer consumer = new QueueingConsumer(ch);
		ch.basicConsume(ticket, queueName, false, consumerTag, consumer);
		long[] deliveryTags=new long[10];
		int count=0;
        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            System.out.println("Message: " + new String(delivery.getBody()));
            ch.basicCancel(consumerTag);
            ch.basicConsume(ticket, queueName, false, consumerTag, consumer);
            //Thread.sleep(1000*60);
            System.out.println("after cancel: ");
            deliveryTags[count++]=delivery.getEnvelope().getDeliveryTag();
         	if(count==9){
         		break;
         	}
        }
        for (int i=deliveryTags.length-1; i>-1; i--){
        ch.basicAck(deliveryTags[i], false);
        System.out.println("after ack: ");
        }
	}
	public static void main(String args[]) throws Exception{
		//scenario1();
		scenario1();
		System.out.println("returned: ");
	}
}
