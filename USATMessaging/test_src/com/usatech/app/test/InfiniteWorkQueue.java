/**
 *
 */
package com.usatech.app.test;

import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

import simple.app.PrerequisiteManager;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.event.ProgressListener;
import simple.io.ByteArrayByteInput;
import simple.io.ByteInput;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

/**
 * @author Brian S. Krug
 *
 */
public class InfiniteWorkQueue implements WorkQueue<ByteInput> {
	protected String queueKey;
	protected final PrerequisiteManager prerequisiteManager = new PrerequisiteManager();
	protected final AtomicLong idGenerator = new AtomicLong();
	protected long limit = -1;
	protected final Retriever<ByteInput> retriever = new Retriever<ByteInput>() {
		@Override
		public boolean cancel(Thread thread) {
			return false;
		}

		@Override
		public void close() {
		}

		@Override
		public Work<ByteInput> next() throws WorkQueueException, InterruptedException {
			final long id = idGenerator.incrementAndGet();
			if(limit > 0 && id > limit) {
				idGenerator.compareAndSet(id, 0);
				throw new WorkQueueException("Reached limit");
			}
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep(queueKey);
			step.addLongAttribute("id", id);
			final ByteInput body;
			try {
				body = new ByteArrayByteInput(MessageChainService.serialize(step));
			} catch(ServiceException e) {
				throw new WorkQueueException(e);
			}
			return new Work<ByteInput>() {
				protected boolean complete = false;

				public String getGuid() {
					return String.valueOf(id);
				}
				public long getEnqueueTime() {
					return 0;
				}
				@Override
				public ByteInput getBody() {
					return body;
				}

				@Override
				public Publisher<ByteInput> getPublisher() throws WorkQueueException {
					return null;
				}

				@Override
				public boolean isComplete() {
					return complete;
				}

				@Override
				public boolean isRedelivered() {
					return false;
				}

				@Override
				public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
					complete = true;
					if(retryType == WorkRetryType.BLOCKING_RETRY)
						prerequisiteManager.resetAvailability();
				}

				@Override
				public void workComplete() throws WorkQueueException {
					complete = true;
				}
				@Override
				public boolean isUnguardedRetryAllowed() {
					return true;
				}
				@Override
				public String getOriginalQueueName() {
					return queueKey;
				}
				@Override
				public QoS getQoS() {
					return null;
				}
				@Override
				public String getQueueName() {
					return queueKey;
				}
				@Override
				public int getRetryCount() {
					return 0;
				}
				@Override
				public Map<String, ?> getWorkProperties() {
					return null;
				}

				public void unblock(String... blocksToClear) {
					// TODO Auto-generated method stub
				}

				public String getSubscription() {
					return null;
				}
			};
		}

		@Override
		public boolean arePrequisitesAvailable() {
			return prerequisiteManager.arePrequisitesAvailable();
		}

		@Override
		public void cancelPrequisiteCheck(Thread thread) {
			prerequisiteManager.cancelPrequisiteCheck(thread);
		}

		public void resetAvailability() {
			prerequisiteManager.resetAvailability();
		}
	};

	/**
	 * @see simple.app.WorkQueue#getQueueKey()
	 */
	@Override
	public String getQueueKey() {
		return queueKey;
	}

	/**
	 * @see simple.app.WorkQueue#getRetriever()
	 */
	@Override
	public Retriever<ByteInput> getRetriever(ProgressListener listener) throws WorkQueueException {
		return retriever;
	}

	/**
	 * @see simple.app.WorkQueue#getWorkBodyType()
	 */
	@Override
	public Class<ByteInput> getWorkBodyType() {
		return ByteInput.class;
	}

	/**
	 * @see simple.app.WorkQueue#isAutonomous()
	 */
	@Override
	public boolean isAutonomous() {
		return false;
	}

	/**
	 * @see simple.app.WorkQueue#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() {
		return TRUE_FUTURE;
	}

	public PrerequisiteManager getPrerequisiteManager() {
		return prerequisiteManager;
	}

	public long getLimit() {
		return limit;
	}

	public void setLimit(long limit) {
		this.limit = limit;
	}
}
