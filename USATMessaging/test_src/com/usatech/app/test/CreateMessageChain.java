package com.usatech.app.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import simple.app.Base;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.OutputStreamByteOutput;
import simple.io.ToHexOutputStream;
import simple.text.StringUtils;
import simple.util.sheet.SheetUtils;
import simple.util.sheet.SheetUtils.RowValuesIterator;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

public class CreateMessageChain extends Base {

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('f', "data-file", false, true, "dataFile", "Spreadsheet containing name-value pairs (first row = names, subsequent rows = values)");
		registerCommandLineSwitch('q', "queue", false, true, "queue", "The queue of the message chain step");
		registerCommandLineSwitch('c', "classesSpecified", true, false, "classesSpecified", "The first row after the header in the spreadsheet contains the class names of the data");
	}
	@Override
	protected void execute(Map<String, Object> argMap, Properties properties) {
		try {
			PrintWriter out = new PrintWriter(System.out);
			String queueName = ConvertUtils.convertRequired(String.class, argMap.get("queue"));
			String dataFile = ConvertUtils.convertRequired(String.class, argMap.get("dataFile"));
			boolean classesSpecified = ConvertUtils.getBooleanSafely(argMap.get("classesSpecified"), false);
			Map<String, Class<?>> classes = classesSpecified ? new HashMap<String, Class<?>>() : null;
			RowValuesIterator rvIter = SheetUtils.getExcelRowValuesIterator(new FileInputStream(dataFile), true);
			for(int i = 1; rvIter.hasNext(); i++) {
				Map<String, Object> attributes = rvIter.next();
				if(classesSpecified && classes.isEmpty()) {
					for(Map.Entry<String, Object> ma : attributes.entrySet()) {
						if(StringUtils.isBlank(ma.getKey()) || ma.getValue() == null || StringUtils.isBlank(ConvertUtils.getStringSafely(ma.getValue())))
							continue;
						try {
							classes.put(ma.getKey(), ConvertUtils.convert(Class.class, ma.getValue()));
						} catch(ConvertException e) {
							out.println("Could not convert class name to a java class");
							e.printStackTrace(out);
							return;
						}
					}
				} else {
					out.println("Message Chain #" + i + ":");
					if(classesSpecified && !classes.isEmpty()) {
						for(Map.Entry<String, Class<?>> entry : classes.entrySet())
							attributes.put(entry.getKey(), ConvertUtils.convert(entry.getValue(), attributes.get(entry.getKey())));
					}
					writeMessageChain(queueName, attributes, out);
					out.println();
				}
			}
			out.flush();
		} catch(IOException e) {
			finishAndExit("Could not print message chains in file '" + argMap.get("dataFile") + "'", 201, e);
		} catch(ConvertException e) {
			finishAndExit("Could not print message chains in file '" + argMap.get("dataFile") + "'", 202, e);
		}
	}

	protected void writeMessageChain(String queueName, Map<String, Object> attributes, Writer writer) throws IOException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(queueName);
		for(Map.Entry<String,Object> entry : attributes.entrySet()) {
			step.addLiteralAttribute(entry.getKey(), entry.getValue());
		}
		step.serialize(new OutputStreamByteOutput(new ToHexOutputStream(writer)), null);
	}
	@Override
	protected boolean hasPropertiesFile() {
		return false;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new CreateMessageChain().run(args);
	}

}
