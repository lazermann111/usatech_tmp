/**
 *
 */
package com.usatech.app.test;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.Prerequisite;
import simple.app.PrerequisiteCheck;
import simple.io.Log;
import simple.util.MapBackedSet;

/**
 * @author Brian S. Krug
 *
 */
public class PrerequisiteManager implements PrerequisiteCheck {
	private static final Log log = Log.getLog();
	protected static final Prerequisite[] EMPTY_PREREQUISITES = new Prerequisite[0];
	//NOTE: The only places where we need concurrent access are in getPrerequisites() and setPrerequisites()
	protected final ConcurrentMap<Prerequisite,AvailableState> prerequisites = new ConcurrentHashMap<Prerequisite,AvailableState>();
	protected final Set<LocalAvailability> availabilities = new MapBackedSet<LocalAvailability>(new ConcurrentHashMap<LocalAvailability, Object>());
	protected static enum AvailableState { UNKNOWN, AVAILABLE, UNAVAILABLE, CANCELLED };
	protected class LocalAvailability {
		protected final AtomicReference<AvailableState> state = new AtomicReference<AvailableState>(AvailableState.UNKNOWN);
		/**
		 *
		 */
		public LocalAvailability() {
			availabilities.add(this);
		}

		public boolean isAvailable(boolean wait) {
			if(state.get() == AvailableState.AVAILABLE)
				return true;
			if(!wait)
				return false;
			lock.lock();
			try {
				while(true) {
					switch(state.get()) {
						case AVAILABLE:
							log.info("Prerequisites are now available");
							return true;
						case CANCELLED:
							if(state.compareAndSet(AvailableState.CANCELLED, AvailableState.UNKNOWN)) {
								log.info("Prerequisite check is cancelled; returning unavailable");
								return false;
							}
							break;
						case UNKNOWN:
							state.compareAndSet(AvailableState.UNKNOWN, checkState());
							break;
						case UNAVAILABLE:
							if(log.isInfoEnabled())
								log.info("State is " + state + "; Waiting " + getInterval() + " milliseconds before checking prerequisites");
							try {
								signal.await(getInterval(), TimeUnit.MILLISECONDS);
							} catch(InterruptedException e) {
							}
							// state could be CANCELLED
							if(state.get() == AvailableState.UNAVAILABLE)
								state.compareAndSet(AvailableState.UNAVAILABLE, checkState());
					}
				}
			} finally {
				lock.unlock();
			}
		}
		public void setAvailable(AvailableState state) {
			lock.lock();
			try {
				this.state.set(state);
				signal.signalAll();
			} finally {
				lock.unlock();
			}
		}
	}
	protected final ThreadLocal<LocalAvailability> availableLocal = new ThreadLocal<LocalAvailability>() {
		/**
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected LocalAvailability initialValue() {
			return new LocalAvailability();
		}
	};
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition signal = lock.newCondition();
	protected long interval = 5000;

	protected AvailableState checkState() {
		log.info("Checking prerequisites");
		for(Map.Entry<Prerequisite,AvailableState> entry : prerequisites.entrySet()) {
			AvailableState prevState = entry.getValue();
			if(prevState != AvailableState.AVAILABLE) {
				if(entry.getKey().isAvailable()) {
					entry.setValue(AvailableState.AVAILABLE);
				} else {
					if(prevState != AvailableState.UNAVAILABLE) {
						entry.setValue(AvailableState.UNAVAILABLE);
					}
					return AvailableState.UNAVAILABLE;
				}
			}
		}
		return AvailableState.AVAILABLE;
	}
	/**
	 * @see simple.app.WorkQueue.Retriever#arePrequisitesAvailable()
	 */
	public boolean arePrequisitesAvailable() {
		//return availableLocal.get().isAvailable(wait);
		return availableLocal.get().isAvailable(true);
	}
	/**
	 * @see simple.app.WorkQueue.Retriever#cancelPrequisiteCheck(java.lang.Thread)
	 */
	public void cancelPrequisiteCheck(Thread thread) {
		availableLocal.get().setAvailable(AvailableState.CANCELLED);
	}

	public void resetAvailability() {
		lock.lock();
		try {
			for(Map.Entry<Prerequisite,AvailableState> entry : prerequisites.entrySet()) {
				entry.setValue(AvailableState.UNKNOWN);
			}
			setAllLocalAvailability(AvailableState.UNKNOWN);
			signal.signalAll();
		} finally {
			lock.unlock();
		}
	}
	public void resetAvailability(Prerequisite prerequisite) {
		lock.lock();
		try {
			prerequisites.replace(prerequisite, AvailableState.UNKNOWN);
			setAllLocalAvailability(AvailableState.UNKNOWN);
			signal.signalAll();
		} finally {
			lock.unlock();
		}
	}
	protected void setAllLocalAvailability(AvailableState state) {
		log.info("Setting availability to " + state);
		for(LocalAvailability la : availabilities) {
			la.setAvailable(state);
		}
	}
	public Prerequisite[] getPrerequisites() {
		return prerequisites.keySet().toArray(EMPTY_PREREQUISITES);
	}
	public void setPrerequisites(Prerequisite[] prereqs) {
		for(Prerequisite pr : prereqs)
			prerequisites.put(pr, AvailableState.UNKNOWN);
	}
	public long getInterval() {
		return interval;
	}
	public void setInterval(long interval) {
		this.interval = interval;
	}
}
