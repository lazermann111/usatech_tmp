/* 
 * Test class showing rollback behaviour for KahaDB persistence failing after sessions, connections
 * are closed, and reopened
 */

package com.usatech.app.test;

import java.io.IOException;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import simple.io.ByteInput;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.jms.ExtendedSession;
import com.usatech.app.jms.JMSBase;
import com.usatech.app.jms.JMSByteMessageByteInput;
import com.usatech.app.jms.JMSStreamMessageByteInput;
import com.usatech.app.jms.JMSTextHexMessageByteInput;

public class ConsumeMessageUtil {
	private static final Log log = Log.getLog();
	
    public static String CONNECTION = "failover://(ssl://localhost:61616?soTimeout=5000&keepAlive=true&connectionTimeout=20000)?randomize=false&maxReconnectAttempts=10"; //$NON-NLS-1$ 
    
    
    protected byte[][] consumeMessage(String queueName, int num, boolean rollback, boolean wait) throws IOException, JMSException, NamingException, InterruptedException {
    	ExtendedSession session = JMSBase.createSession(true);
		Destination dest = session.createQueue(queueName);
		MessageConsumer consumer = session.createConsumer(dest);
		byte[][] contents = new byte[num][];
		for(int i = 0; i < num; i++) {
			Message msg = (wait ? consumer.receive() : consumer.receive(2000));
			ByteInput bi;
			if(msg instanceof BytesMessage) {
				bi = new JMSByteMessageByteInput((BytesMessage)msg);
			} else if(msg instanceof StreamMessage) {
				bi = new JMSStreamMessageByteInput((StreamMessage)msg);
			} else if(msg instanceof TextMessage) {
				bi = new JMSTextHexMessageByteInput((TextMessage)msg);	
			} else if(msg == null) {
				break;
			} else {
				throw new JMSException("JMS Message Type " + JMSBase.getMessageType(msg) + " not supported on message ID " + JMSBase.getMessageId(msg));
			}
			byte[] content = new byte[bi.length()];
			bi.readBytes(content, 0, content.length);
			final boolean redelivered = JMSBase.getRedeliveredSafely(msg);
			final String desc = "JMS Message " + JMSBase.getMessageInfo(msg) + " [" + (redelivered ? "redelivered" : "first delivery") + "]";
			log.info("Received " + desc + ": " + StringUtils.toHex(content));
	    	contents[i] = content;
	    }
		// rollback a message and close the connection
    	if(rollback)
    		session.rollback();
    	else
    		session.commit();
    	session.close();
    	return contents;
    }
    
    public static void main(String[] args) {
    	try {
    		ConsumeMessageUtil cmu = new ConsumeMessageUtil();
    		String queueName = args[0];
    		int num;
    		try {
    			num = Integer.parseInt(args[1]);
    		} catch(NullPointerException e) {
    			num = 1;
    		} catch(ArrayIndexOutOfBoundsException e) {
    			num = 1;
    		} catch(NumberFormatException e) {
    			num = 1;
    		}
    		cmu.consumeMessage(queueName, num, false, false);
		} catch(IOException e) {
			log.error("", e);
		} catch(JMSException e) {
			log.error("", e);
		} catch(NamingException e) {
			log.error("", e);
		} catch(InterruptedException e) {
			log.error("", e);
		} 
		System.exit(0);
    }
}
