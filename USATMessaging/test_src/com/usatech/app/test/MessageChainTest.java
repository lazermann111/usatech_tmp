package com.usatech.app.test;

import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.bean.ConvertUtils;
import simple.io.ByteArrayByteInput;
import simple.io.Log;
import simple.test.UnitTest;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

public class MessageChainTest extends UnitTest{
	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}

	public Map<String, Object> getCompressedXMLAtt(String xml)throws Exception{
		MessageChainV11 mc=new MessageChainV11();
		MessageChainStep generateStep=mc.addStep("usat.report.generate");

		generateStep.addCompressedStringAttribute("report.folioXML",xml);
		byte[] chainBody = MessageChainService.serialize(generateStep);
		ByteArrayByteInput body=new ByteArrayByteInput(chainBody);
		MessageChainV11 mc2 = new MessageChainV11();
		mc2.read(body, false);
		MessageChainStep step = mc2.getCurrentStep();
		Map<String, Object> att=step.getAttributes();
		return att;
	}

	@Test
	public void compressStringTest() throws Exception{
		String folioXML="<?xml version=\"1.0\" encoding=\"UTF-8\"?><folio><pillars></pillars></folio>";
		Map<String, Object> att=getCompressedXMLAtt(folioXML);
		String xmlResult=att.get("report.folioXML").toString();
		Assert.assertTrue(folioXML.equals(xmlResult));

		folioXML=null;
		att=getCompressedXMLAtt(folioXML);
		xmlResult=ConvertUtils.getStringSafely(att.get("report.folioXML"));
		Assert.assertTrue(xmlResult==null);

		folioXML="";
		att=getCompressedXMLAtt(folioXML);
		xmlResult=ConvertUtils.getStringSafely(att.get("report.folioXML"));
		Assert.assertTrue(folioXML.equals(xmlResult));
	}

	@Test
	public void compressByteArrayTest() throws Exception{
		String folioXML="<?xml version=\"1.0\" encoding=\"UTF-8\"?><folio><pillars></pillars></folio>";
		Map<String, Object> att=getCompressedXMLAtt(folioXML);
		String xmlResult=new String(ConvertUtils.convert(byte[].class, att.get("report.folioXML")));
		Assert.assertTrue(folioXML.equals(xmlResult));

		folioXML=null;
		att=getCompressedXMLAtt(folioXML);
		xmlResult=ConvertUtils.getStringSafely(att.get("report.folioXML"));
		Assert.assertTrue(xmlResult==null);

		folioXML="";
		att=getCompressedXMLAtt(folioXML);
		xmlResult=ConvertUtils.getStringSafely(att.get("report.folioXML"));
		Assert.assertTrue(folioXML.equals(xmlResult));

	}

}
