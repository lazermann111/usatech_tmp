package com.usatech.app.test;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.ByteArrayByteInput;
import simple.io.HexStringByteInput;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.io.OutputStreamByteOutput;
import simple.security.crypt.EncryptionInfo;
import simple.test.UnitTest;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

public class MessageChainSerializationTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	@Test
	public void testMessageChainDeserialize() throws Throwable {
		// String s =
		// "52223130353533407573616e657432313a31333138374244424243393a3133353132373100021a6d63652e6170706c617965722e646174612e32303039303931370352534103b0f9675d227c447bf4f07f7d5859e3d9e4bec25d3c87c640d7e0a210ef8116dd465bd57776a10add54151e2123bd732730a131ad703178e2c5e1e27421b59b09b9bdc24880ca8a7d9c8b660a8e81f04015f1ca488e73626b30648e4ff8c00a3fe25f6dc01c200d1d5ac239fdad72ffbd431eba2be3cba3489edfdbac37d05c6d150222116f4a8faf424968d721397414a32e84d3a0772ada4aa653950b323c5398b4ddf714b34c4ff2021ef604d28d8e0796b8e80764dde19d01259f85f330a6a7486e448d51abae49123856baea2d436b625309119c39e8b39aedb0f3424fcf2f885374ff714c0a44d98669a1795b96a3c15cded547e1dd84d7886e09c00fa76d52ff1f911d732acb91803c0eb4b5ec71b461f5d4631cc1cd93d850e6b94384028b3d6031f6b9851358838de3bc72f7a2715ea1fcabbd31b145c206c6821465e582de8b3c08bfe03d28308ee67c85bdddba7fc4f44c7a7c91de8f051711298c0994708c4628cbb7841a80af1a0a161a5ab958111513d9ac61db9b99d4031f2807bda58b0664d24e76117d29454a4a852660bef88dd65fef00481c8387f77b96e1caa085525bd78015fa07d48e2486898c9e53bb0ee1d72284c3b2916fa0033d129065aea7c363c38e03ac375b705da8810fa3488f977982fa8e9cbf12c931fbe0b32a3a6aa90f96ff01aecc09eeca9068756a731c2e8c69573727fb4631868fc1ddc5ec23626a546190b869a18274e78b360f050d9abaa82e2aa25cd2103286c8af8f53b0b892fc13a33a5ec444b439c413a87abff577785149cfbc50a96bc15d4dd0f4f57bc69ff8aa97f7c7595ffc4fbaab5ba6f964d7fad844ed17b7cf4bd244a02f68ddff0162547e03208b5e901a6805e505b46a04d01b50401994252276365f740d77d7fe6c8d2abf46e3dde9269a7531b74860847d70ba88b45449ba61f4cf27dcf9e6dc1e68d8aea4776f99c3471323b5326c5a20278e228b773dcf38b9399152dab2584c5d168594f5196c907a69c84d44d5f27fc1d1e428af15fb873780db7c22e697acde9c492250b7945275ca2829dad0deae68cb4a44ed347eae100e0133442dbe2dc961470a0f902574e4ef4b903eee93676c7191fec56d733b7daf7fd5fb521f33a501684ed723a519d8fbc365537523300e6238964c5af3db57f14a07519f7581cd6f46056728d8a44497e4d04bc24ff581c986cd7d9ef37b96e5e9ef491bd1557a350f491ef0e13b5ba20469c156d8c797418f141eb6899112c5aa00b7450f00349970655e2df359c4bd4f4556678431ae30865bc90610c3ec9645105a7fa4300b0eecc65b737e6b03414553010013ce45955ee15e25de041de41eddb1f7b97975c8a674d1ffe9c033633bd186e2cf549cc43f0d38be7b679f6fa7f339f56afe1be4337c4703679ea268b00336708538142653aa6c4ae1a7ca4404b614f1384058013acfb5ae419aa9b348fae16f0696714954e7000c695eb3b6b76c4f0e33f6e1cf39a7982f29f4782c255be663c0a03cd61d468257dad7268b1f1ea592615bf373ff9c6692062eb56183bd9c0d5239b14cf8ab4816f08e9382fbab3f257c83a8ee528f9aac79918c95e77065bf0487cd5f092d5a656c65bf620d77d2082fbb1273b1e95692f891cb4d065612a12b8cde570973df3d6745ccf5701f412eb55c7c79d1150a6b9c486dda5d6a1a61";
		String[] hexArray = {
 "122f34383035407573616e657433342e74726f6f7065722e757361746563682e636f6d3a31343531383931304537313a35000101010021706f736d2e7465726d696e616c2e736574746c656d656e742e666565646261636b0000020c666565646261636b547970650105424154434810666565646261636b5265736f7572636501575b46494c455f5245504f5f332c46494c455f5245504f5f345d34383035407573616e657433342e74726f6f7065722e757361746563682e636f6d3a31343531383846344241383a33464143313536333636323236303832000000ffff0000014518b09d79000000000000000000" };
		for(String s : hexArray) {
			MessageChain mc = MessageChainService.deserializeMessageChain_v11(new HexStringByteInput(s), "test", false);
			MessageChainStep step = mc.getCurrentStep();
			mc.setResultCode(0);
			MessageChainStep[] nextSteps = step.getNextSteps(0);
			if(nextSteps != null)
				log.info("Got " + nextSteps.length + " next steps");
			StringBuilder sb = new StringBuilder();
			sb.append("Message Chain '").append(mc.getGuid()).append(':').append(mc.getInstance()).append("':\n");
			appendStepInfo(step, sb);
			log.info(sb.toString());
		}
	}

	@Test
	public void testMessageChainDeserialize2() throws Throwable {
		byte[] bytes = new byte[] { 82, 26, 49, 57, 56, 52, 64, 66, 75, 82, 85, 71, 73, 55, 58, 49, 51, 67, 50, 53, 54, 66, 56, 67, 53, 68, 58, 50, 0, 2, 26, 109, 99, 101, 46, 110, 101, 116, 108, 97, 121, 101, 114, 46, 100, 97, 116, 97, 46, 50, 48, 48, 57, 48, 56, 48, 51, 3, 82, 83, 65, 23, 80, -96, 25, -128, 50, -45, -122, -99, -92, -13, 14, -6, 2, 74, 8, -42, 13, 42, -22, 3, 56, 43, -91, -73,
				80, -5, -99, 121, -90, 82, -20, -81, -54, 80, 25, 86, 126, 17, -116, -50, -123, 22, -1, -57, -112, 121, 80, -91, 126, 72, -2, -112, -59, 47, 86, 109, -46, -23, 115, -58, -33, -111, 81, 117, 2, 100, 77, -114, -45, 38, 42, 121, -81, 121, 20, -118, 9, 93, -10, -117, -106, 63, 74, 108, 72, 2, -41, 77, -22, -92, -80, 18, 111, 75, 110, -17, 73, 39, 78, 23, 76, -79, -66, -78, 89, 120,
				-19, 125, -108, 12, -117, -122, -98, -21, -113, 122, 58, -4, 49, 36, -1, 123, 70, -45, -52, -25, 5, -64, 56, 41, 39, -108, -104, 124, 37, 24, 20, 60, 26, 63, 90, -38, 80, -12, 117, -25, -84, 73, -59, -81, 40, 116, 43, -30, 83, 115, -80, 55, -25, -14, 38, 19, 23, 34, -2, 18, 117, 55, -128, 67, -20, 122, 100, -114, 81, -60, 82, -118, 105, 65, -43, -107, -64, -28, 121, -92, 113, -36,
				-56, 120, 58, -43, 85, 14, 28, -6, 125, 96, -81, 116, 97, 87, -109, 22, 86, 59, 24, 99, -101, -117, -7, 27, 7, -106, 83, -41, 89, -57, -11, -112, 27, 23, -121, 49, -69, 116, 116, 105, -99, -58, -19, -77, 3, 123, -93, -76, -123, -14, 86, 57, -109, 45, -78, 50, -70, -112, 24, 68, -39, -9, -86, 30, 52, 125, 94, -24, -73, 63, 63, -50, 102, -28, -25, -13, 67, -65, -23, 20, 99, -92,
				-84, 109, 38, -75, -22, -40, 73, 101, 86, 117, -20, 30, -48, -109, 74, -94, -38, 94, -96, -97, -105, 108, -18, -45, -23, -54, -31, -124, -71, 24, 1, -88, -88, 105, -75, 22, 68, -15, -10, 118, 34, 4, -122, -80, -127, -122, -87, 119, 23, -19, 14, -100, -105, 78, 79, -62, -62, 58, 40, 83, -85, -18, -85, -80, 33, 25, 8, -99, 2, -69, 51, -59, 42, 124, 19, -41, -56, 35, -45, -9, 7, -77,
				-86, -76, 70, -15, -43, 74, -25, 102, -60, 116, -127, 97, -108, 1, -120, 33, 46, 125, 30, -13, -99, 123, 116, 113, 53, -71, -57, 17, -116, 38, 29, 44, -57, 70, -11, -15, 25, -50, 68, -36, -101, 94, 60, -34, -13, -120, -70, 106, 90, 115, 126, -16, 2, -111, -101, -54, 102, -77, -68, -114, -115, -7, 68, 31, 99, 70, -11, -15, 25, -50, 68, -36, -101, 94, 60, -34, -13, -120, -70, 106,
				90, -92, -124, 28, 68, 103, -101, -6, -27, -122, 82, -54, -20, 108, 82, -35, -77, -35, 32, -85, 93, -114, -33, -121, -121, 57, -28, -3, 94, -105, 98, 89, -77, 56, -85, -88, -76, 84, 98, 95, 30, 88, 16, 85, -12, 12, -60, -7, 0, -25, 10, -112, -70, 89, -83, -6, -102, -40, 75, -10, -3, 127, 81, 127, 126, -19, 82, 52, -114, -78, -45, -42, 43, -92, 31, -33, 2, -6, -100, -46, -121, 62,
				-53, 28, -36, -8, 52, 55, -4, -56, -78, 64, -50, 80, 44, -92, -127, 23, 115, -92, 94, -45, 66, 64, 99, -75, 70, 46, 14, -2, 67, -124, 21, -118, -87, 46, 118, -18, -27, 10, 70, 30, 19, 30, 91, 46, -21, -82, 15, -25, -29, -101, -52, -85, 96, 72, -3, -112, 13, -73, 91, -92, -89, 57, -2, -67, -83, -43, 29, 73, -113, 64, -68, -107, -49, -37, 30, 118, -123, -50, 32, -23, 68, 80, -104,
				24, 64, 26, 107, 75, 115, 21, 3, -96, 43, 1, 125, 38, -53, 32, 22, -17, 108, -73, 16, -38, -87, 10, 76, -65, 109, 43, 117, -60, 125, -2, 55, -113, 76, -67, 28, -15, 84, 123, 106, 13, 60, -85, 113, 85, 119, 26, -122, 19, -97, -42, 101, 79, 123, 21, 20, 126, 111, 5, -23, 119, 75, 44, -82, -74, 23, -72, -123, 51, -73, 62, 93, 104, 113, -50, 60, 5, 114, 89, -63, 41, 111, -105, 61,
				-15, -23, -96, 81, -119, 90, 105, 72, 118, -18, 3, 103, -111, -122, 2, -84, 32, -61, -81, 11, -14, -90, 113, -62, -77, 86, -40, 44, 99, 93, -44, -90, -119, -42, -104, -30, -48, 120, 41, -57, 124, -61, 43, -117, 15, 46, -52, -108, 119, 40, 65, 119, -72, -97, 107, -28, -95, 41, -24, -43, 76, -4, -62, 51, -8, -25, -35, 68, -116, 70, 100, -121, 62, -110, 54, -23, 91, -50, -68, -88,
				110, -15, 114, 14, 112, -88, -120, -30, -16, 27, -71, 98, -127, 19, -119, -66, 35, -111, 92, -115, 102, -62, 56, 22, -58, 64, 55, 124, -75, 26, -96, 34, -45, 47, -85, -58, 15, -16, -91, -75, -63, -32, -86, 26, -16, 39, -44, 80, 99, -40, 2, -16, -99, 85, -104, -96, -108, 101, 108, 52, -24, -21, -8, -66, -104, 12, 52, -121, -37, 93, 33, 78, -63, 11, 121, 17, -79, -55, -30, 29, -48,
				89, 102, 69, 74, -5, 2, -106, -4, 26, -30, 107, 58, -26, 17, 68, 47, 98, 92, 66, -94, -17, 32, 93, -15, 119, 58, 56, -74, -2, 4, 12, 77, -52, 121, 62, 59, 25, -102, -26, -119, -30, -12, -38, -128, -87, 46, -38, 30, -29, 110, 100, -33, -109, -28, -68, -114, 70, -114, 109, -14, -20, -29, -56, -92, 60, -71, 19, 53, -121, -117, -126, 33, -106, 103, 78, -35, -44, -82, -13, 22, -28, 87,
				-23, -71, -21, 39, 35, -33, 93, -18, 71, 61, -123, 56, -124, -5, -116, -74, -98, 56, 49, 97, -119, -39, -80, 57, -55, -75, 25, -97, -56, 30, -12, -112, -22, -22, 51, 84, -85, 23, 52, 100, -52, -76, -1, -56, -125, -120, -70, -107, -14, 52, 4, -128, -67, -23, 11, 105, -80, -25, -38, -60, 10, 12, 1, -38, 7, -91, 73, -80, 74, 116, 23, -107, -36, 126, -128, -40, 55, 24, -79, -121, -13,
				43, 58, 93, 9, -9, 35, -75, 13, -62, 46, -67, 121, -113, 89, -112, 0, -105, 8, -32, 107, 113, 71, 12, -65, -42, 21, 123, -61, 105, 106, 11, 58, -68, 2, -32, -102, 89, -121, -39, 72, 8, -6, 26, -9, 37, 30, 127, -4, 65, 42, 2, 118, -63, 13, -59, 126, 55, 92, -88, 37, 2, 25, 50, -126, -118, -67, -14, 116, -72, 87, 86, 41, -5, 54, -43, -96, 93, 102, 102, 101, -128, -95, 125, -127, 20,
				-69, -66, -45, -24, -32, -120, -124, 46, 111, -73, -89, 20, -7, -56, -120, -23, 23, -90, -127, -25, -1, 8, 48, 82, 48, 88, 67, 36, 56, 35, -75, 94, 89, -36, 74, -119, -101, 79, 89, -80, -113, 127, -37, -31, 62, 14, 30, 100, 7, -33, 115, 46, -32, 30, 25, -11, 121, -98, -51, -80, 32, 25, -111, 51, -37, -3, -58, 85, 17, -43, -7, -6, 13, -8, -46, 30, -80, -61, -127, 117, -61, 72, -47,
				126, 70, -100, 11, 112, -12, -120, -100, -21, -97, 59, -55, -80, 85, -75, -61, 82, -63, -42, -72, 43, -35, 4, -11, -90, 27, -104, 50, 116, 97, -93, -42, -120, 41, -66, 125, 118, 4, 115, 31, 42, 61, -102, -32, 14, -44, -76, 36, -93, -79, -29, -106, 56, -89, 52, 13, -5, 91, 43, 116, 42, 44, -74, 66, 9, 94, 78, -34, 65, 124, -75, 92, -39, -86, 58, -89, 111, -110, -37, 102, 60, 55,
				88, -22, 41, 99, -123, 105, -98, -14, 126, 86, 94, 85, 118, 3, 104, -44, -66, -22, 65, 45, 31, -10, -118, -28, 51, 72, 19, -73, 53, -49, 65, 44, -120, 108, -107, -116, 65, 87, 5, 28, -35, 53, -43, 4, 16, 110, 38, 52, 41, 92, 80, 104, 8, -119, -96, 69, 112, 117, 39, -76, 29, 15, -76, 78, 59, -98, -69, 107, 75, 77, 19, 73, 62, 67, 82, 99, -113, -120, -107, 38, -15, 103, -101, 45,
				70, 105, 40, 18, -81, 77, 84, 3, 44, 86, 87, 46, -109, -6, 70, 95, -58, -34, -16, -100, 17, 37, 106, 57, -18, 34, -77, -120, -97, 102, 51, 98, -26, -1, -99, -38, 72, -60, 93, 50, 74, 124, -37, 82, -64, -14, -47, -11, -68, -8, 60, -4, 1, 57, 101, 87, -114, -10, 90, -103, -37, -23, -59, -40, 107, 0, 49, -98, -70, 48, 77, 19, 122, 17, -61, 72, 78, -113, -13, 42, 105, 66, 118, -56,
				-86, 92, -26, 75, 81, 72, -3, -34, 95, 102, 125, -91, 40, -48, -13, -33, -91, 63, -126, 59, -76, 105, 20, 18, 74, 47, -23, -93, 93, 40, 1, -22, 9, 54, -57, 101, -50, -7, -116, 77, 57, -72, 84, -102, -70, 60, 36, -84, -77, -71, 77, 122, -18, -13, -122, -80, -6, -98, -17, -48, 109, 35, 25, -19, -60, 45, -103, -73, -5, 83, -19, 39, 88, -78, -126, 36, 40, -126, 31, -30, 88, 4, -16,
				-57, 8, 92, -80, -31, 108, 70, 108, 12, 96, -123, 122, -96, -79, 107, -12, 75, -123, 24, 34, 31, 71, -103, -12, 114, -17, -63, 35, 99, 73, 121, -22, 108, 76, 76, -23, -91, -98, -44, 108, -17, -45, 18, 104, 43, -116, 107, 125, -96, -17, 74, -7, 120, 38, -96, -17, 86, -108, 46, -55, 116, 5, 126, 73, 82, -71, 12, -46, 97, -109, 117, 59, 112, -82, 59, -9, 67, -18, 62, 26, 35, 96, -18,
				60, 84, -37, 14, 2, 58, -36, 73, -81, 11, -74, -12, 67, 62, -5, -67, -55, 88, 62, -62, 104, -118, -19, -22, 91, 28, -5, 122, 1, -84, -87, 12, -44, 123, 122, 22, 38, 23, -37, -93, -113, 38, 37, 14, -3, -106, -6, -97, -87, -61, 3, 25, -106, 23, -33, -19, -53, -42, 36, 126, -73, -52, 110, 7, 52, 19, -49, -53, 94, 109, -36, -59, 32, 43, 48, -22, 32, 19, 60, -113, 125, 59, 2, 79, 52,
				-12, 42, -38, -84, -52, 19, 29, -28, -27, -32, -55, 90, -34, 37, -121, -34, 6, -98, 53, 55, 83, -84, 91, 121, -95, -21, 103, 98, 41, 121, 22, -39, 119, 17, -13, -37, 121, 15, 54, 46, -128, 75, 37, -115, -63, 54, 85, 2, 45, -99, -116, 108, -71, -82, 62, -87, -61, -9, -42, -12, 122, -126, 90, 95, -56, -10, -59, 112, -83, -25, -76, -97, 40, 10, 9, -50, 31, -41, 23, 51, 29, 8, -29,
				-72, -121, 50, 97, -67, 65, 71, -121, 10, -114, 26, -75, 43, 81, -88, 88, 43, -76, 55, -39, -79, 72, -81, 34, -46, -66, -36, 125, -7, 61, 92, 42, 42, -18, 54, -100, -110, -1, 32, 79, 68, 96, -82, -99, 72, -88, 56, -89, -119, -125, 83, 22, 124, 63, 16, 3, -74, 51, -86, 68, 33, 28, 121, -56, 72, -126, -22, 76, -94, -23, -85, -35, 28, -76, 94, -124, 101, -86, 23, -126, -112, -29,
				-107, 20, 87, 58, 101, 64, -125, 94, -87, -13, -70, -51, -3, 46, 52, 1, -102, -54, -113, 35, -83, 3, -113, -82, -100, 69, -127, 1, -97, -76, 127, 109, 17, -109, 34, 76, 113, -109, 120, -109, -102, 85, -36, -49, 79, 46, 101, 29, -50, -122, -41, 69, 89, 53, -27, -78, -50, -5, 125, -76, 37, 29, -82, 126, -105, -82, 73, 106, 110, 47, -107, -79, -42, -16, -119, 16, -68, -57, 82, -110,
				2, -34, -128, -71, -111, -124, -25, 81, -84, 105, -36, 73, -86, 63, 53, -128, 73, -100, 96, -83, -108, 103, 21, -41, -101, -38, 119, 27, -54, -22, -21, 22, -123, 76, 125, -41, 75, -70, -52, 35, 91, 51, -21, 105, 18, 49, 89, 17, -69, -30, 81, 35, 19, -106, 124, -97, -3, -22, -89, -20, 20, 124, -43, -4, -1, -17, 71, -101, -71, -98, -13, -46, -61, -30, 43, 38, 88, 65, -27, 73, 17,
				58, -89, 104, -105, -104, -49, -3, -60, 110, 22, 61, 21, 112, 92, -101, 21, 86, 82, -59, 105, -36, -108, 5, 81, -9, 5, 109, -59, -95, -7, 31, -2, -59, 37, -79, 2, 118, -89, 88, 91, 32, -112, -73, -42, 114, -24, -12, 29, 65, 73, 20, 121, -66, -43, 38, -52, -123, 25, 68, -85, 7, 43, -34, -51, 24, -118, -27, -90, -81, 37, 2, 77, -70, -32, -87, -46, -75, -54, 44, 111, 69, 71, 106,
				-43, -83, 82, -81, -92, -72, -59, 126, -38, 23, 102, -108, 3, 28, -60, -47, 93, 119, -60, -85, -15, -55, 58, 20, 51, -22, 65, 106, -117, 11, -3, -96, 69, -95, -78, -93, -41, -118, 83, 63, 79, 47, -31, -52, -99, -3, -114, 31, 50, 79, 84, -16, -99, 92, -61, -113, 105, -52, 117, 11, 101, 111, 3, 4, 38, -4, 114, -6, -61, 103, 31, -32, 46, 25, 120, 127, -102, 126, 111, -118, -97, -69,
				-96, -50, 127, -125, 54, 75, 54, 89, 102, -79, 44, 77, 58, 79, 47, -9, 89, -11, -2, 51, -24, 46, 1, 95, -25, -16, 111, 117, -17, -3, 94, 66, -61, -113, -59, 111, 85, -105, 17, -28, 74, -58, -32, 23, 48, 13, -36, -5, -112, 92, -127, 67, -111, -68, 124, 32, 43, -30, -8, 60, 64, -42, 34, -112, 92, -1, -122, -95, 55, -13, -81, 122, -38, -118, 91, 94, -19, -8, 90, 122, -87, -107, -33,
				-120, 102, 58, 77, 113, 3, 91, 28, 87, 31, -67, -83, -65, 88, 41, 121, -83, 17, 112, -113, -109, 41, -33, 119, -15, 108, 46, -122, 43, -24, -104, 85, 84, -25, 29, -50, -61, -123, 64, 1, -90, 93, -103, -40, -44, 64, -20, 88, -59, -85, 33, -98, -74, -48, -16, -22, 35, 108, 46, -122, 43, -24, -104, 85, 84, -25, 29, -50, -61, -123, 64, 1, -90, -44, -67, -44, 4, 22, 50, -13, -65, -74,
				2, 102, 0, -41, 86, 59, 119, -73, 9, -91, 60, 87, 86, -101, 92, 96, -33, -37, 16, -103, -31, -100, 1, 45, 126, 49, -59, -49, 12, -3, 93, 127, 0, -87, -29, 56, -32, -40, 115, -59, 17, -116, -56, -68, 59, -52, 95, 119, 0, 16, 20, 90, -4, -79, -38, -41, -5, -70, 118, 71, 122, -67, -9, 12, -110, 123, 91, 1, -62, 17, 22, -72, 45, 101, -106, -46, 74, -91, 100, 106, 125, -109, 89, -13,
				5, -48, -67, 107, -68, 116, 35, 59, -55, 77, -117, -16, 120, 113, 4, -38, -12, -84, -76, -111, 127, 21, 21, -26, 74, 53, 0, 112, -106, -5, -116, 87, 109, -126, -123, -74, 70, -125, -98, 16, 12, -63, -31, 81, 97, 26, 59, 116, -75, 104, 60, 66, -70, 91, 10, 26, 28, 35, -29, -2, 46, 39, -115, -74, -85, 52, -18, -88, -120, 99, -25, 55, -41, 100, 54, 68, 18, 98, 108, 65, -97, -98, -74,
				-115, -98, -44, -48, -42, -96, -86, 36, 33, 124, 14, -95, -120, 87, 6, -33, 72, 39, -44, -87, 15, -7, -77, -47, 38, 113, 117, -128, 99, -113, -23, 38, 126, 49, 116, 85, 59, 18, -111, 18, -34, 106, 103, -97, -8, 101, 22, -64, -108, 27, 1, -71, -91, 107, -81, -53, -55, -79, -34, 6, 121, 66, 116, 80, -39, -58, 113, -36, -80, -22, -83, -104, -119, 73, 23, 65, -66, -49, -57, 110, 56,
				-94, 82, 21, -93, -111, -47, -117, -116, -83, -25, -75, 45, 87, -123, 74, 28, 55, -17, -63, -75, -33, -31, 94, -115, 34, -69, -79, 68, 71, -30, 95, 57, 71, -2, -4, -121, -60, 69, 44, -25, -105, 42, 29, 65, -51, -15, -117, -12, 15, 50, -127, -54, -109, -11, -66, 109, -4, 33, 102, -79, -48, 28, -66, -90, 105, -123, 1, -87, 24, -126, 95, -70, -116, 38, 75, 37, 122, 97, -93, -43, -41,
				59, -85, -69, 40, -119, 19, -76, 104, -24, 1, 1, 43, -4, 43, 33, 69, -22, 46, -57, 93, 105, 18, 89, -127, -6, -28, 125, 9, 68, -41, -104, -69, 59, -111, -14, -84, 74, 45, 37, 14, -73, -79, 76, 127, 28, 85, 95, 28, 89, 65, 91, 82, -99, 82, 57, 8, -8, -28, 78, 21, 46, 33, -99, -32, -25, 115, -62, -90, 97, 114, 56, 118, 42, 59, -58, -22, 34, 29, -62, -93, 6, -95, 69, -39, -12, 106,
				37, 50, -98, 23, -110, -81, 17, 112, -120, 124, 93, -17, -93, -63, 49, 3, -36, -31, 105, -97, -58, -121, -57, 125, -118, 9, -60, -113, 31, 53, 68, -21, 66, 53, -91, -90, 37, 27, 51, -32, -82, -3, 33, -10, 25, -34, -30, -66, 105, -112, 92, -101, -126, -110, -11, 113, -77, 13, -18, 69, 44, 52, 11, 103, -104, -61, -120, 5, -126, 117, -18, -73, 78, -49, -58, 75, -81, -30, 65, -94,
				-119, 93, -9, 83, -55, -105, 122, 4, 96, 15, 80, -22, -44, -1, 25, 34, 89, 65, 82, -19, 28, 112, 0, 30, 19, -64, -120, 109, -89, 1, -30, 8, 73, -120, 18, 69, -111, 50, 48, -9, 10, -4, 28, -66, -3, 100, 5, 105, -7, -29, 43, 115, 12, 74, -70, 114, -46, 53, 18, -82, 38, 74, 53, 37, 59, 0, 106, -78, 109, 102, -121, 25, 40, -67, -48, 91, -107, 43, 70, 54, -43, -96, -88, 87, -19, 85,
				-67, -39, 37, 114, -8, 45, 115, 94, -70, 127, -114, -54, 53, -33, -71, 114, 57, 78, 89, 119, 103, -74, 115, -101, -86, -48, -40, 11, 102, -121, 5, 58, -80, 16, 41, 55, 56, -53, -42, -122, 100, -43, 26, -35, -13, 94, 1, -117, -20, 27, -101, 31, -128, 117, -36, -27, 32, -77, 69, -50, 97, -43, 8, 127, 78, 46, -118, 21, -118, -14, 5, 110, 15, -26, 40, 63, -96, 110, -36, -59, -13, 108,
				64, 93, -109, 89, 72, 62, -97, -70, -49, -7, -13, -78, 117, -107, 121, 18, 61, 68, 49, -3, 44, -33, 43, 42, 79, -43, 74, 29, -95, -79, 114, 7, -54, -82, -38, 49, 67, -98, -38, -63, 116, 30, -23, 127, -73, 89, 78, -103, -38, 91, -99, 116, -1, 18, -127, -14, 99, -110, -8, -14, -45, 15, -17, -53, -101, -88, -82, -45, 45, -118, -19, -106, -85, -34, 67, 12, -93, 125, -115, 25, 40, 18,
				93, -67, 62, 112, -57, 58, 73, -82, -83, -104, -57, 51, 3, 44, 110, 76, -50, -20, 56, -72, -118, -83, 50, 115, -108, -96, 52, -26, -57, -6, 127, 57, 4, 59, -114, 42, 26, -53, -52, -82, -60, 102, 26, 28, 104, -29, 94, 17, 0, -22, -45, -90, 69, -43, -105, 95, -67, -36, -14, -60, 106, 71, 116, -117, -52, -59, -78, 8, 82, 35, -68, -108, -40, 31, -28, -31, 70, -69, 62, -72, -51, -71,
				88, -73, -108, -126, 108, -34, 57, -76, -28, -96, 7, -21, 62, -99, 118, -14, 80, -94, -43, 71, 121, -109, 34, -32, 100, -54, -113, -93, -91, 122, 76, 84, -112, 56, -124, 70, 126, 114, -114, 63, -80, 8, -97, -100, -46, -64, 16, -70, -45, 90, 108, -77, 8, -36, -75, -77, 6, -3, -43, -120, 116, 103, -35, -123, -20, 103, 63, 64, -105, -117, -95, 101, -6, -88, 38, 20, -78, 114, -103,
				-74, -18, 10, 31, 31, -98, 100, -55, 55, -106, -91, 94, -59, 8, 30, -75, -120, -122, 93, -15, 6, 111, 66, 55, 2, 65, -118, -100, 1, -37, -5, -128, -60, -112, -53, 67, 108, -122, 49, -28, 116, 96, 62, 24, -76, -75, -35, -108, -74, 10, 15, -80, 118, 55, 119, -52, 38, 63, -78, 71, -7, -36, -4, 107, 3, -103, -69, -34, 75, 26, 52, 19, 40, -103, 42, -50, -28, 65, -48, -96, -69, -36,
				-90, -38, 42, 81, -64, -47, -94, 39, -96, 25, 41, -23, 75, -22, 71, -116, -12, 126, -86, -40, -112, 82, 72, -4, 70, 111, -40, -70, 32, 87, -103, -65, 97, -68, -108, 33, -94, -52, -39, 92, -21, -17, -36, -88, 126, 12, 18, 38, 61, -32, 110, 53, 81, -19, -112, -119, 91, 58, 37, -7, 41, -103, -33, -83, -37, -35, 91, 78, 11, -25, 97, -45, 31, 54, -34, 115, -128, 110, 1, 28, -127, -68,
				37, 110, 82, 97, -115, 49, 24, 59, 101, 60, -82, 88, -30, -83, 42, 23, 27, 43, 96, 125, -27, -72, -80, 13, 8, 23, -38, -72, -103, -39, -57, -3, -118, -15, 41, 6, 48, 65, -99, -67, 111, -69, 127, -4, -54, -1, 66, -90, 1, 119, -91, 100, 30, -59, -102, 67, 43, -103, -41, -31, 73, -77, -15, 35, 64, 67, -50, -44, 0, 45, 62, -19, -97, 35, 15, -112, -35, 51, 9, 40, -84, -11, 64, 43, -1,
				-56, 127, 97, 101, -9, 123, -1, 0, 39, 23, 89, -81, -44, 34, -74, -49, -22, -103, -8, -50, 36, 127, -14, 27, -56, -116, -67, -11, 79, -125, -113, -21, 11, -31, 3, -60, -34, -37, -84, 79, -90, -99, 6, 47, -8, 80, 34, 77, -96, 49, -78, -111, -32, 37, 59, 55, -12, -98, -49, -51, -20, 102, 33, -67, -42, -122, -74, -48, 22, -18, -104, 118, -20, -11, -21, -25, -66, 84, -97, 51, 55, 33,
				-53, 25, 99, 35, -97, -52, 71, -34, 78, -98, 62, -94, 117, 85, -34, -19, -19, 85, 124, 48, -10, 97, 31, 115, -95, 95, -9, 97, -73, -58, -128, -82, -119, 123, -32, -5, -15, 105, -72, -116, -128, 118, -58, -31, -40, -43, 60, -70, -6, 74, 70, -126, 21, -40, -42, 34, 83, -61, -72, 100, 50, -75, -95, -81, 32, -95, 57, -116, -55, -65, -16, -46, 40, 75, 23, 40, 78, 99, -27, -81, 10, -98,
				-68, -57, -27, -101, -117, -114, 63, -104, -37, 24, -5, 81, -44, 4, 32, -12, 109, -95, -41, 91, -32, 79, -23, 66, -23, -55, 116, 14, -95, -111, -110, 75, -63, -112, 79, 99, -97, -79, 7, 99, -59, -107, 89, 19, -126, -96, -114, -85, 82, -89, -71, -59, -36, -120, -105, 15, -2, -112, 33, 12, 112, 119, 83, 74, 92, -119, 117, 35, -95, 51, -101, 120, 74, 51, -39, -100, 117, 4, 120, -109,
				61, 124, -71, 68, 41, -7, 96, 106, 45, -43, 92, 76, 107, -45, -116, 97, -123, 27, -83, 109, 115, 101, -18, -1, 116, 123, -34, 6, 48, 118, -85, -124, -116, -110, 11, -50, 127, -45, 0, 83, -74, 35, 66, 54, -8, 21, -104, -46, 122, -121, 1, 63, -111, -116, -80, 42, -73, 115, 46, -37, 96, 102, 109, 109, -14, -98, 59, 63, -73, 75, 112, 107, -23, 4, 33, -93, 64, -102, 13, 47, -105, 71,
				75, -82, 33, -87, 78, -26, 84, 86, 112, 50, -54, 78, 117, 45, -26, -113, 10, 65, -46, -48, -55, -51, 37, 31, -110, -90, 90, 87, 115, -16, 70, 116, 55, 62, -111, 67, 86, 63, -59, 84, -32, 45, 107, -81, -28, -95, -81, -118, -94, 83, -40, -23, 34, 22, 41, -75, 116, 40, 66, 96, -114, 27, -99, 0, 111, -74, -24, 116, 0, 69, 95, -99, 28, 24, 94, 34, -49, -67, 110, 65, 71, -16, 47, -107,
				65, -68, -67, -105, 102, 125, -21, 60, 8, 35, 4, 86, -126, 90, 9, 12, -124, 96, -122, -114, 40, 95, 76, -52, 49, -31, 46, -20, 21, -113, 37, 95, 3, 66, 84, 22, -33, -33, -97, -14, -70, 121, -36, 100, -109, -56, -24, 36, -5, 53, 106, 120, 69, -118, 80, -27, 92, 92, -47, 107, 85, 0, 118, -105, -115, 66, -8, 38, 22, -67, 89, 54, -58, -95, 2, -40, -126, -33, -5, 0, 23, 56, -71, -62,
				-91, 6, 54, 110, 94, -84, -35, 39, -124, 25, -74, 24, 31, 26, 45, 47, -60, -80, -62, -95, -61, -124, 46, -84, 19, -122, 24, -69, -54, 32, 116, -84, 103, 14, 106, -76, -72, -24, -87, 118, -31, -60, 49, 14, 82, -17, -30, 30, -88, 100, -90, 54, -53, -15, 51, -47, 117, -91, 81, -114, 122, 76, -2, -22, -36, -55, -119, 110, 31, 22, -112, 76, -6, -42, 33, 13, -123, 126, -108, 24, -25, 7,
				-122, -120, 38, -100, 120, -13, -84, -127, 82, -49, -73, 98, 52, 102, -67, 16, 43, 10, 23, -115, 82, 51, 44, -124, 7, -44, -47, -28, -103, 36, 93, 70, 5, 32, 21, -8, -72, 95, -55, -106, -55, -127, 68, -5, -117, -29, 87, 13, -86, -121, 69, 36, -84, 38, 78, 112, 125, -21, -14, 31, 28, 68, 64, 29, -57, -15, 95, -26, -13, 43, -39, -22, 114, 97, 92, 22, 84, 91, -100, -93, -90, -30, 88,
				92, -94, -43, 30, -23, 32, -13, -9, -97, -15, 112, -124, 48, 29, -114, -63, -28, -109, -32, 17, 125, -122, -75, 109, 54, 91, -26, 1, 11, 19, 105, 56, -3, -76, 79, -22, 120, -67, 15, -44, -26, 67, -3, -117, -82, 63, -93, -31, 81, 111, 82, -126, 124, 111, -35, 53, -2, 110, 74, 117, 122, 120, -25, -46, 63, -68, 51, 43, -60, -15, 23, 33, 68, 35, -97, -55, 39, 5, -90, -105, 59, 20, 64,
				-48, 117, 60, 12, 123, -2, -65, 25, 85, -14, 68, -47, -92, 92, -59, -121, -61, 87, 114, -90, -35, 74, -40, 110, 13, -29, 27, -14, 35, -34, -83, -124, -69, 121, 127, -67, -57, -26, -116, -126, -33, -107, -11, 76, 100, -11, 30, 69, -110, -38, 68, -32, -18, -44, -48, -30, -72, 53, -43, 116, -29, 104, -121, -50, 10, -8, -12, -10, -100, 110, 87, 106, 71, -45, 1, 125, -37, -32, 15, -49,
				24, 109, 58, 103, -69, 2, -48, 53, -99, 52, -116, 103, 45, -109, -95, 71, -57, -64, -74, -1, 120, -74, 113, -30, -28, 38, 39, -17, -97, -60, -48, -2, -18, -93, 120, 112, -20, -99, -73, 112, -68, -40, 63, -9, -50, 54, -84, -74, -53, -91, 104, 36, 61, 106, 59, 75, 122, -54, -22, -61, -110, -118, -48, -38, -62, -111, 80, 53, -124, 91, -42, -74, 44, 103, 12, 18, 108, -63, -117, 32,
				64, -58, -46, -123, 2, 119, -67, 75, -24, -113, -32, -123, 120, -79, 36, -47, -75, 75, -127, -81, 98, -73, 18, -123, -70, -76, 5, 1, -14, -109, 0, 89, -55, -91, 120, -8, -54, -22, -33, 62, -95, 85, -10, 107, -43, -127, -66, -110, -95, 55, 1, -82, 68, -114, -74, 117, 43, 44, 62, -69, 3, -30, -111, 56, -99, -53, -61, -109, 50, 53, -126, 27, -2, -96, 115, 6, -76, 47, 3, -81, 21, 122,
				-2, -79, -28, 116, 112, 107, 126, 88, 52, 27, 123, 46, -27, 52, -58, 37, -113, 52, -126, 126, 92, 79, -23, 11, 30, 104, 7, 107, 32, -2, -39, -26, -25, -98, 120, 66, -86, -28, 91, 52, 5, -15, 78, 105, 59, -61, -39, 55, 68, 2, -83, -18, 26, 108, -48, 1, -128, -85, -35, 122, 17, -31, 61, -90, 88, 14, -31, 98, 38, 33, -57, 2, -110, -40, -83, -111, 25, -87, -73, 38, -103, 76, -88, 90,
				125, -74, -58, -48, 49, -18, 105, -74, -103, 35, 93, 15, 115, 25, 103, -44, 121, -27, 71, 27, -38, 110, 41, -117, -90, -30, 63, -20, 26, -7, -106, -47, -119, -122, -85, -76, 111, 28, 99, 38, 108, -84, -64, 80, 41, 22, 16, 124, -50, 104, 26, -124, -99, 114, -69, -81, 67, 29, 120, 41, -32, -22, -26, 65, 121, -125, -43, 110, 54, 101, 126, -27, -27, 36, 1, 53, -16, -46, -78, 23, 102,
				80, 97, 26, -72, 37, 110, 34, -33, 25, 7, 102, -25, 76, -53, 47, 71, -18, 57, 59, -46, 122, -61, 78, -86, 122, -60, -102, -21, -88, -23, -68, 25, 79, -51, -33, 127, -84, 18, 62, 61, -79, 4, 33, -54, 26, -36, 93, -87, -24, 95, 20, 48, 17, 98, 48, -33, 76, 67, 19, -89, 85, -80, -120, -128, -43, -103, -9, -127, -46, 27, 13, -10, -84, -57, 94, 114, 68, -9, 31, -12, 23, -126, -77, 80,
				103, 126, -96, -107, 17, -45, 86, -24, -54, 37, 82, -88, 80, 73, -84, -12, -98, 47, -23, 68, -47, -95, 123, -89, 60, 24, -6, 59, -4, 76, -72, -65, -50, 12, 80, -19, -23, 87, -77, 13, -90, 117, 91, 123, 36, -22, 66, 58, -16, -53, 88, -16, 117, 88, -38, 28, -13, 124, 86, -110, -57, -40, 107, 76, 45, 58, -68, -64, 112, -74, 57, -74, 57, -20, -9, 123, 52, -103, 23, -103, -10, -114,
				53, -57, -98, 87, 102, 71, -87, -71, -108, 9, -83, -65, 58, -73, -112, 61, 34, -48, -92, 80, -15, -49, 100, -84, -35, -67, 111, 84, 124, -34, 92, 88, 49, 34, -45, 104, 4, -17, -42, 9, -32, -97, 114, -44, 127, -108, 110, 78, -19, 26, 87, -19, -10, -114, 70, 21, -101, -80, 117, 19, -3, 14, -112, -97, 59, -64, -58, -102, 73, -18, 65, -67, 33, 69, 10, 98, 65, -30, 1, 23, -83, 67, -69,
				125, 46, 78, 100, -60, -8, 12, 18, 73, 87, -20, -94, 118, -119, -8, -31, -62, 98, -89, 125, -4, 65, -77, 64, -64, 16, -90, -98, -73, 76, 28, 104, 29, -82, 104, -128, -57, -67, 91, -72, 43, 60, -33, -81, 83, -76, -60, 113, -7, 63, 18, -64, -58, 58, -23, 13, -33, -128, -74, 118, -58, -52, 91, 49, -54, 85, 11, 65, 47, 36, 103, -78, -111, -55, 78, 45, -20, 39, -15, 17, -48, 62, 127,
				17, 98, 75, -114, 9, -119, -5, 98, -112, -80, 94, -127, 58, -24, 66, -119, -43, 126, 35, -128, 105, 62, -28, -128, 13, 85, -106, -1, -88, -128, 110, 100, -22, 73, -63, -47, -113, 118, 100, 100, 119, -81, 30, -56, 89, 101, 93, 69, 55, 18, 40, 87, -104, 0, 60, 20, 54, -103, -67, -42, -126, 34, -82, -17, 83, -52, 127, -20, -53, -123, 94, 117, 123, 69, -112, 23, 3, 77, 13, -121, -118,
				-68, -56, -75, -106, 49, -25, -88, 83, -95, -95, 125, 125, -7, -40, 94, -5, 70, -79, -8, -96, -125, -52, -32, 92, 98, -39, 105, -101, -62, 118, -26, 42, 13, -37, 10, 125, -10, 115, 126, -52, -20, 36, 83, 2, 122, 84, 22, 4, 4, -91, -2, 32, -3, 51, -128, 32, 112, -84, -53, 77, -11, 45, 102, 46, -49, 20, 79, 64, 74, -87, 42, 122, -117, 27, 113, 46, 30, -107, 92, 56, 69, -111, -30,
				91, -48, 16, 77, -29, 52, 43, 96, 2, -21, 60, 49, 115, 8, 39, 94, -14, -84, 7, -107, 109, 5, 123, 68, 113, -18, -73, -85, -123, -66, 124, -46, -38, -44, 63, -12, -120, -51, 95, 81, -108, 120, -41, 99, -120, -4, -49, 89, 16, -91, -17, -22, 98, -126, 120, 84, -126, 116, 45, 43, -102, 45, 4, 120, 106, -41, -7, 10, 35, 80, 54, -38, 34, 116, 106, -39, -124, -49, -17, 32, 89, -20, 52,
				-58, 78, 8, -38, 39, 2, -101, -84, 17, 66, 41, -17, 58, 119, -11, -14, 107, -82, -120, 36, -90, -95, 108, 52, -29, 63, 80, 126, -5, -113, 35, 47, 121, 14, -6, 46, 69, 23, -95, -45, -84, 77, 37, 76, 127, 115, 16, -18, 98, -7, 11, -33, -103, 61, 123, 45, 39, 111, 87, -14, -102, 33, -79, 102, -122, -75, -84, -63, -54, 3, -38, -59, -111, -28, -110, 117, 112, -7, -61, 33, -98, 39, 2,
				-103, 15, -67, 20, 58, -94, -127, -103, 2, 12, 115, -119, 24, 94, -13, -28, -4, -100, 108, 0, -124, 76, -60, -92, -125, -82, 70, -62, -76, -116, -115, -31, 126, -79, 53, 118, 117, -82, 55, -43, 120, -7, -32, 126, 120, 24, -54, 115, 41, -8, 44, -67, 113, 3, 44, -2, 99, -114, 102, 111, -46, 77, -50, 4, -21, 14, 71, 19, 81, -69, 69, -34, 65, 56, 88, 16, 14, -102, -49, 96, -109, 87,
				121, 50, 126, -104, -37, 120, -41, -109, -123, -97, -105, 72, -99, -81, 51, 33, -29, -76, -56, -95, 87, 80, -121, 37, 12, -124, 107, 41, 107, 103, -100, 19, -4, 15, 87, 102, 123, 47, 37, -88, -45, -4, -1, -5, -87, -52, -83, -24, -73, -24, -81, 126, 50, -65, 28, -70, -83, 68, 106, -104, -101, 7, 18, 38, 57, -122, 117, 40, -26, 102, -94, 116, -68, 98, 13, 62, 31, -17, 60, -20, -7,
				106, 99, -18, 89, 112, -51, -115, -8, 123, 28, 6, -59, 99, 52, 99, -84, -94, 13, -76, 70, -62, -38, 60, 97, 100, 109, 83, -85, -79, 99, -60, -34, -114, -30, -4, 0, 40, -86, -21, 22, 78, 29, 37, -53, 76, 66, 96, 20, -104, -38, 109, -72, 35, 92, 31, -75, -102, 102, -96, 53, 14, 55, -19, -104, -116, -41, 65, -7, -60, 47, 20, 67, 123, -101, -27, -125, 79, 93, 20, 104, 91, 60, -29, 65,
				-126, 5, 97, 19, 68, -51, -76, -34, -111, 110, 94, -47, -39, -128, 101, 23, -88, 13, 37, 127, 7, 42, -97, -61, -91, -40, -14, -98, 59, 63, -73, 75, 112, 107, -23, 4, 33, -93, 64, -102, 13, 47, 46, -16, -8, 83, -47, -103, 18, 75, -14, 73, -58, 94, 119, 3, -103, 65, -31, 47, 107, -77, -114, 120, 79, 65, 92, -87, -118, 49, 40, -71, 85, 113, -60, -28, -80, 113, 25, -36, -72, 36, 21,
				-92, 114, 4, -58, -25, 69, 47, 70, 69, 54, 51, -83, 111, 40, -121, 63, 43, -101, -103, -2, 30, -11, -42, 40, 66, -116, -112, -6, 69, 97, -70, 121, -45, -96, 42, 92, 7, -28, -75, -99, -37, -17, -103, 34, -118, 122, -18, -47, 92, 80, -87, -73, 108, -67, -47, 95, 16, 20, 118, -31, -126, 100, 50, -61, -23, 48, -56, 15, 1, 31, 14, -16, -44, -114, 67, -98, 9, -84, -76, -7, -24, -124,
				15, 1, -115, 51, -55, 73, -78, 125, 26, 53, 12, -103, 81, 121, -122, 17, 101, 92, -64, 77, 120, 42, -70, 57, -100, 97, -92, -125, -63, 27, -83, -89, 38, 120, 53, -111, 76, 109, 45, 0, 51, 119, -3, -95, -109, -66, -27, -74, 41, 24, 47, 91, -106, -40, 83, 71, 53, 30, -106, -71, -10, 61, 36, 46, 57, 60, -86, -8, -73, 74, -19, 64, -74, 55, 50, -18, -115, 43, 24, 21, -88, -120, -124,
				-14, 97, -59, 112, 65, 89, -51, 24, 71, -110, -14, 59, -119, -70, -55, -27, -39, -32, 50, -71, 0, -15, 41, -112, -28, -22, -73, 96, -81, 59, 9, -89, -89, -45, 96, 96, 78, -63, -27, 67, 122, -13, 81, -20, -2, 24, -121, -77, -34, -75, 88, -98, 52, 15, -26, -24, 60, -8, 61, -93, -36, -95, -115, -12, -5, -5, -53, -2, -13, -46, 65, -4, -49, -94, 47, 78, 106, -113, -24, -78, -42, 94,
				-125, 4, 40, -51, -7, 111, -15, 46, -123, -1, 127, -9, -18, 80, 32, 17, -107, -95, 20, 72, -43, -59, -2, 107, -118, 63, -112, -103, -85, 4, -123, -128, -87, 116, -53, -33, 107, -27, -125, -30, 31, 7, 15, -14, 86, 0, 9, -11, 47, -92, -105, -78, 110, -48, 77, -85, -90, -110, 54, -71, -20, 5, -24, -70, 20, 126, 113, -126, 57, 10, 32, -45, -37, -82, 64, -125, 103, 19, 0, 57, 34, -125,
				-99, -106, -60, -45, -18, -54, -86, -111, -1, -124, 55, -62, -95, -53, 3, 65, 69, 83, 1, 0, 59, -112, -123, -35, -60, -116, 25, -103, 75, 7, 75, 113, -84, 47, 91, 5, -115, -63, 106, 18, 118, -100, -31, -57, -63, 33, -47, -113, 15, -29, -71, 21, -95, 126, -48, -37, -93, -85, 25, -114, 29, -43, 96, -17, -48, 66, 7, -125, 53, 108, 98, 80, 76, 69, 116, -40, -23, -101, 56, -110, -50,
				-45, -126, -94, -3, -3, 119, 34, 50, 90, -17, 114, 53, 85, -82, -6, 14, 20, -15, 10, -88, 96, 102, -19, 72, -14, 113, 119, -29, 120, -101, -86, 97, -17, 36, 77, 95, -37, 96, 14, -109, -48, -124, 80, -39, -56, 78, 110, -68, 70, -6, -30, 123, -27, 4, -82, -69, 0, 2, 51, 86, 42, -72, -43, -14, 61, -78, -20, -98, 62, 12, -41, 12, -121, -29, -128, -101, -10, -70, -74, 52, -89, -6, -13,
				-117, -89, -40, 22, -128, 116, -52, 64, -13, 97, -76, -1, -124, -110, -22, -39, 41, 20, -120, -54, -54, -64, 55, 110, 118, -95, 52, -23, -43, -9, -116, -79, 54, 45, 20, 38, -74, -104, -69, 48, 117, -113, -96, 109, 104, 19, 8, -67, -101, -8, -35, 84, -85, 78, 122, -102, -24, -18, -87, -119, 8, -15, 77, -37, 83, 59, -59, 53, 83, -93, 83, -107, -24, 23, -118, 27, -122, -20, 95, -68,
				-5, 110, 89, 24, 72, 50, 9, -118, -30, 50, -44, 122, 110, 82, -78, -56, -68, -124, 15, 54, 88, 26, -124, -100, 46, 45, 114, 97, -87, -106, 78, -124 };
		MessageChain mc = MessageChainService.deserializeMessageChain_v11(new ByteArrayByteInput(bytes), "test", false);
		MessageChainStep step = mc.getCurrentStep();
		mc.setResultCode(0);
		MessageChainStep[] nextSteps = step.getNextSteps(0);
		log.info("Got " + nextSteps.length + " next steps");
		StringBuilder sb = new StringBuilder();
		sb.append("Message Chain '").append(mc.getGuid()).append(':').append(mc.getInstance()).append("':\n");
		appendStepInfo(step, sb);
		log.info(sb.toString());
	}
	protected static void appendStepInfo(MessageChainStep step, StringBuilder sb) {
		sb.append("Queue '").append(step.getQueueKey()).append("': attributes=").append(step.getAttributes());
		for(Integer rc : step.getResultCodes()) {
			for(MessageChainStep resultStep : step.getNextSteps(rc)) {
				sb.append("\n\t").append(rc).append(" --> ");
				appendStepInfo(resultStep, sb);
			}
		}
	}
	@Test
	public void testMessageChain2() throws Throwable {
		System.setProperty("javax.net.ssl.keyStore", "D:\\Development\\Java Projects\\LayersCommon\\conf\\apr\\keystore.ks");
		System.setProperty("javax.net.ssl.keyStorePassword", "usatech");
		System.setProperty("javax.net.ssl.trustStore", "D:\\Development\\Java Projects\\LayersCommon\\conf\\net\\truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		boolean useEncryption = true;
		//assert false : " testing assertions on";
		com.usatech.app.MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("queue");
		step.addStringAttribute("literal1", "abc");
		MessageChainStep nextStep = mc.addStep("next");
		nextStep.addStringAttribute("foo", "bar");
		nextStep.addStringAttribute("boo", "far");
		nextStep.setNextSteps(255, step);
		// *
		char[] chs = new char[70000];
		chs[0] = '[';
		chs[69999] = ']';
		step.addLiteralAttribute("verylong", new String(chs));
		// */
		MessageChainStep referencedStep = mc.addStep("reference");
		referencedStep.getResultAttributes().put("def", "you should get this!");
		referencedStep.getResultAttributes().put("ghi", "you should NOT get this!");
		step.addReferenceAttribute("reference1", referencedStep, "def");
		step.setNextSteps(0, nextStep);
		step.setNextSteps(2, referencedStep);
		OutputStream out;
		InputStream in;
		//BufferStream bs =  new BufferStream();
		//out = bs.getOutputStream();
		//in = bs.getInputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		out = baos;
		EncryptionInfo encryptionInfo;
		if(useEncryption) {
			encryptionInfo = new EncryptionInfo();
			encryptionInfo.setCipherName("RSA");
			encryptionInfo.setSecondaryCipherName("AES");
			encryptionInfo.setKeyAliasPrefix("mce.applayer.data.");
			encryptionInfo.setKeySize(256);
		} else {
			encryptionInfo = null;
		}

		mc.getCurrentStep().serialize(new OutputStreamByteOutput(out), encryptionInfo);
		byte[] bytes = baos.toByteArray();
		log.debug("Message = " + StringUtils.toHex(bytes));
		in = new ByteArrayInputStream(bytes);
		com.usatech.app.MessageChain mc2 = new MessageChainV11();
		mc2.read(new InputStreamByteInput(in), encryptionInfo != null);
		log.debug("Attributes = " + mc2.getCurrentStep().getAttributes());
		ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
		mc2.getCurrentStep().serialize(new OutputStreamByteOutput(baos2), encryptionInfo);
		byte[] bytes2 = baos2.toByteArray();
		assert CollectionUtils.deepEquals(bytes, bytes2) : "serialization is not the same!";
	}

	@Test
	public void testMessageChain3() throws Throwable {
		// assert false : " testing assertions on";
		com.usatech.app.MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("queue");
		step.addStringAttribute("literal1", "abc");
		MessageChainStep nextStep = mc.addStep("next");
		nextStep.addStringAttribute("foo", "bar");
		nextStep.addStringAttribute("boo", "far");
		nextStep.setNextSteps(255, step);
		// *
		char[] chs = new char[70000];
		chs[0] = '[';
		chs[69999] = ']';
		for(int i = 1; i < 69999; i++)
			chs[i] = '-';
		step.addLiteralAttribute("verylong", new String(chs));
		// */
		MessageChainStep referencedStep = mc.addStep("reference");
		referencedStep.getResultAttributes().put("def", "you should get this!");
		referencedStep.getResultAttributes().put("ghi", "you should NOT get this!");
		step.addReferenceAttribute("reference1", referencedStep, "def");
		step.addPassThruReferenceAttribute("reference2", referencedStep, "def");
		step.addPassThruReferenceAttribute("reference2", referencedStep, "ghi");
		step.setNextSteps(0, nextStep);
		step.setNextSteps(2, referencedStep);
		OutputStream out;
		InputStream in;
		// BufferStream bs = new BufferStream();
		// out = bs.getOutputStream();
		// in = bs.getInputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		out = baos;
		mc.getCurrentStep().serialize(new OutputStreamByteOutput(out), null);
		byte[] bytes = baos.toByteArray();
		log.debug("Message = " + StringUtils.toHex(bytes));
		in = new ByteArrayInputStream(bytes);
		com.usatech.app.MessageChain mc2 = new MessageChainV11();
		mc2.read(new InputStreamByteInput(in), null != null);
		log.debug("Attributes = " + mc2.getCurrentStep().getAttributes());
		log.debug("Chain = " + mc2.getExecutionInfo(4));
		ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
		mc2.getCurrentStep().serialize(new OutputStreamByteOutput(baos2), null);
		byte[] bytes2 = baos2.toByteArray();
		assert CollectionUtils.deepEquals(bytes, bytes2) : "serialization is not the same!";
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}

