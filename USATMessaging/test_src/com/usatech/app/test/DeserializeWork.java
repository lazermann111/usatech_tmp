package com.usatech.app.test;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import simple.app.Base;
import simple.app.ServiceException;
import simple.app.Work;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.ByteInput;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveComparator;
import simple.util.CollectionUtils;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.task.TaskUtils;

public class DeserializeWork extends Base {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws IntrospectionException 
	 */
	public static void main(String[] args) {
		new DeserializeWork().run(args);
	}
	@Override
	protected void execute(Map<String, Object> argMap, Properties properties) {
		PrintStream out = System.out;
		String outputFile = ConvertUtils.getStringSafely(argMap.get("outputFile"), null);
		if(outputFile != null && (outputFile=outputFile.trim()).length() > 0 && !"-".equals(outputFile)) {
			try {
				out = new PrintStream(outputFile);
			} catch(FileNotFoundException e) {
				out.println("Cannot write to file '" + outputFile + "'; using standard out instead");
			}
		}
		try {
			File dir = new File(ConvertUtils.getString(argMap.get("directory"), true));
			if(!dir.exists()) {
				out.println("Directory '" + dir.getCanonicalPath() + "' does not exist");
				printUsage(out);
				return;
			}
			String filePattern = ConvertUtils.getString(argMap.get("filePattern"), true);
			final Pattern pattern = Pattern.compile(filePattern);
			File[] files = dir.listFiles(new FilenameFilter() {			
				@Override
				public boolean accept(File dir, String name) {
					return pattern.matcher(name).matches();
				}
			});
			if(files == null || files.length == 0) {
				out.println("No files found in directory '" + dir.getCanonicalPath() + "' matching regular expression '" + filePattern + "'");
				printUsage(out);
				return;
			}
			out.println("Found " + files.length + " files:");
			out.println();
			boolean summary = ConvertUtils.getBooleanSafely(argMap.get("summary"), false);
			if(summary) {
				Map<String,List<File>> filesByQueue = new HashMap<String,List<File>>();			
				int length1 = 5;
				int length2 = 7;
				for(File f : files) {
					Work<ByteInput> work = TaskUtils.deserializeMessage(f);
					String queue = work.getOriginalQueueName();
					if(queue == null || (queue=queue.trim()).length() == 0)
						queue = "<NULL>";
					List<File> list = filesByQueue.get(queue);
					if(list == null) {
						list = new ArrayList<File>();
						filesByQueue.put(queue, list);
						length1 = Math.max(length1, queue.length());
					}
					length2 = Math.max(length2, f.getName().length());
					list.add(f);
				}
				out.print("Queue");
				for(int i = 0; i < length1 - 3; i++)
					out.print(' ');
				out.println("File(s)");
				for(int i = 0; i < length1; i++)
					out.print('-');
				out.print("  ");
				for(int i = 0; i < length2; i++)
					out.print('-');				
				out.println();
				String[] sorted = filesByQueue.keySet().toArray(new String[filesByQueue.size()]);
				Arrays.sort(sorted, new CaseInsensitiveComparator<String>());
				for(String s : sorted) {
					for(File f : filesByQueue.get(s)) {
						out.print(s);
						for(int i = 0; i < length1 + 2 - s.length(); i++)
							out.print(' ');
						out.println(f.getName());						
					}
				}
				for(int i = 0; i < length1 + 2 + length2; i++)
					out.print('-');
				out.println();
			} else {
				boolean verbose = ConvertUtils.getBooleanSafely(argMap.get("verbose"), false);
				for(File f : files) {
					Work<ByteInput> work = TaskUtils.deserializeMessage(f);
					out.print("File: ");
					out.println(f.getCanonicalPath());
					out.print("Work: ");
					out.println(work.toString());
					out.println(ReflectionUtils.toPropertyMap(work));
					if(verbose) {
						out.print("MessageChain: ");
						try {
							MessageChain mc = MessageChainService.deserializeMessageChain_v11(work.getBody(), work.getQueueName(), false);
							printMessageChain(mc, out);
						} catch(ServiceException e) {
							e.printStackTrace(out);
						}
					}
					out.println("-------------------------------------");
				}
			}
		} catch(IOException e) {
			out.println("Exception during processing:");
			e.printStackTrace(out);
			printUsage(out);
			return;
		} catch(IntrospectionException e) {
			out.println("Exception during processing:");
			e.printStackTrace(out);
			printUsage(out);
			return;
		} catch(ConvertException e) {
			out.println("Exception during processing:");
			e.printStackTrace(out);
			printUsage(out);
			return;
		}
	}
	protected void printMessageChain(final MessageChain mc, PrintStream out) {
		MessageChainStep step = mc.getCurrentStep();
		if(step == null) {
			out.println("[NO CURRENT STEP]");
		} else {
			out.print("[\"");
			out.print(step.getQueueKey());
			out.print("\": attributes={");
			boolean first = true;
			for(Map.Entry<String,Object> entry : step.getAttributes().entrySet()) {
				if(first)
					first = false;
				else
					out.print(", ");
				out.print(entry.getKey());
				out.print('=');
				Object value = entry.getValue();
				if(value instanceof byte[]) {
					out.print(StringUtils.toHex((byte[])value));
				} else if(value != null && value.getClass().isArray()) {
					out.print(CollectionUtils.deepToString(value));
				} else {
					out.print(value);
				}
			}
			out.println("}]");
		}
	}
	@Override
	public Properties getProperties(String propertiesFile, Class<?> appClass, Properties defaults) throws IOException {
		return new Properties(defaults);
	}
	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineArgument(false, "directory", "The directory in which to look for files to parse");
		registerCommandLineArgument(false, "filePattern", "The regular expression pattern for files to parse");
		registerCommandLineSwitch('s', "summary", true, false, "summary", "Prints a summary by queue of the files that have messages for each queue");
		registerCommandLineSwitch('v', "verbose", true, false, "verbose", "Prints all information for a message including parsing of the content");
	}
}
