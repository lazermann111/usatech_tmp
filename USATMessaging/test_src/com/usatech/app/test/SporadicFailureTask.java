/**
 *
 */
package com.usatech.app.test;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

/**
 * @author Brian S. Krug
 *
 */
public class SporadicFailureTask implements MessageChainTask {
	protected float failureChance = 0.1f;
	protected WorkRetryType retryType = null;
	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		if(Math.random() < failureChance) {
			if(retryType == null)
				throw new ServiceException("Planned failure");
			else
				throw new RetrySpecifiedServiceException("Planned failure", retryType);
		}
		return 0;
	}
	public float getFailureChance() {
		return failureChance;
	}
	public void setFailureChance(float failureChance) {
		this.failureChance = failureChance;
	}
	public WorkRetryType getRetryType() {
		return retryType;
	}
	public void setRetryType(WorkRetryType retryType) {
		this.retryType = retryType;
	}

}
