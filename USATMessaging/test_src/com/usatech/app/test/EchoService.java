package com.usatech.app.test;

import java.io.IOException;

import simple.app.AbstractWorkQueueService;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueueException;
import simple.io.ByteArrayByteInput;
import simple.io.ByteInput;
import simple.io.Log;

public class EchoService extends AbstractWorkQueueService<ByteInput> implements AbstractWorkQueueService.WorkProcessor<ByteInput> {
	private static final Log log = Log.getLog();
	private static final ByteInput NEWLINE_BYTEINPUT = new ByteArrayByteInput("\n\r".getBytes());
	
	public EchoService(String serviceName) {
		super(serviceName);
	}
	@Override
	protected AbstractWorkQueueService.WorkProcessor<ByteInput> getWorkProcessor() {
		return this;
	}
	@Override
	public void processWork(Work<ByteInput> work) throws ServiceException,
			WorkQueueException {
		process(work.getBody(), work.getPublisher(), work.isRedelivered());
	}
	protected void process(ByteInput body, Publisher<ByteInput> publisher, boolean redelivered) throws ServiceException {
		if(body.isResettable() && log.isDebugEnabled()) {
			String s;
			try {
				s = body.readBytes(body.length());
				body.reset();
			} catch(IOException e) {
				log.warn("Could not get bytes", e);
				throw new ServiceException(e);
			}
			log.debug("Got Bytes: " + s);
		}
		publisher.publish(queue.getQueueKey(), body);
		try {
			NEWLINE_BYTEINPUT.reset();
		} catch(IOException e) {
			log.warn("", e);
		}
		publisher.publish(queue.getQueueKey(), NEWLINE_BYTEINPUT);
	}
	@Override
	public boolean interruptProcessing(Thread thread) {
		return false;
	}

}
