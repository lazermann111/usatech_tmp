package com.usatech.app.test;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.*;
import simple.event.ProgressListener;
import simple.io.Log;
import simple.lang.Initializer;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.rabbitmq.client.ShutdownSignalException;
import com.rabbitmq.utility.ValueOrException;

public class FaultyAMQPWorkQueue implements WorkQueue<byte[]> {
	private static final Log log = Log.getLog();
	protected String host;
	protected int port;
	protected String realm;

	protected String queueKey;
	protected String exchange = "";
	protected boolean multicast = false;

	protected int consumerMinSize = 5;
	protected int consumerMaxSize = 100;

	protected Channel channel;
	protected int ticket;
	protected String consumerTag;

	protected final static int STATE_INITIAL = 0;
	protected final static int STATE_STARTING = 1;
	protected final static int STATE_CONSUMING = 2;
	protected final static int STATE_PAUSING = 3;
	protected final static int STATE_PAUSED = 4;

	protected final AtomicInteger consumerState = new AtomicInteger(STATE_INITIAL);
	protected final ReentrantLock channelLock = new ReentrantLock();
	protected final Condition cancelSignal = channelLock.newCondition();
	protected final BlockingQueue<ValueOrException<Delivery, ShutdownSignalException>> queue = new LinkedBlockingQueue<ValueOrException<Delivery, ShutdownSignalException>>();
	protected Thread cancelThread;
	protected final Consumer consumer = new Consumer() {
		public void handleCancelOk(String consumerTag) {
			if(!consumerState.compareAndSet(STATE_PAUSING, STATE_PAUSED))
				log.warn("Consumer was not in PAUSING state when handleCancelOk was received");
		}
		public void handleConsumeOk(String consumerTag) {
			if(!consumerState.compareAndSet(STATE_STARTING, STATE_CONSUMING))
				log.warn("Consumer was not in STARTING state when handleConsumeOk was received");
		}
		public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
				throws IOException {
			try {
				queue.put(ValueOrException. <Delivery, ShutdownSignalException> makeValue(new Delivery(envelope, properties, body)));
			} catch(InterruptedException e) {
				IOException ioe = new IOException("Interrupted while adding to consumer queue");
				ioe.initCause(e);
				throw ioe;
			}
		    if(queue.size() >= getConsumerMaxSize()) {
			    try {
					pauseConsumer();
				} catch(WorkQueueException e) {
					log.warn("Could not pause consumer", e);
				}
		    }
		}
		public void handleShutdownSignal(String consumerTag, ShutdownSignalException sig) {
			queue.add(ValueOrException. <Delivery, ShutdownSignalException> makeException(sig));
		}
	};

	protected final Initializer<IOException> initializer = new Initializer<IOException>() {
		@Override
		protected void doInitialize() throws IOException {
			Connection conn = new ConnectionFactory().newConnection(getHost(), getPort());
			channel = conn.createChannel();
			ticket = channel.accessRequest(getRealm());
			channel.exchangeDeclare(ticket, getExchange(), "direct", true);
			String queueName;
			if(isMulticast()) {
				queueName = channel.queueDeclare(ticket).getQueue();
			} else {
				queueName = channel.queueDeclare(ticket, queueKey, true).getQueue();
			}
			channel.queueBind(ticket, queueName, getExchange(), queueKey);
			consumerState.set(STATE_STARTING);
			cancelThread = new Thread("AMQP Cancel Thread") {
				@Override
				public void run() {
					channelLock.lock();
					try {
						while(true) {
							try {
								cancelSignal.await();
							} catch(InterruptedException e) {
							}
							try {
								if(consumerState.compareAndSet(STATE_CONSUMING, STATE_PAUSING))
									channel.basicCancel(consumerTag);
							} catch(IOException e) {
								consumerState.compareAndSet(STATE_PAUSING, STATE_CONSUMING);
							}
						}
					} finally {
						channelLock.unlock();
					}
				}
			};
			cancelThread.setDaemon(true);
			cancelThread.setPriority(3);
			consumerTag = channel.basicConsume(ticket, queueKey, false, nextConsumerTag(), consumer);
		}
		@Override
		protected void doReset() {
			try {
				channel.close(200, "Complete");
			} catch(IOException e) {
				//ignore
			}
			try {
				channel.getConnection().close(200, "Complete");
			} catch(IOException e) {
				//ignore
			}
		}
	};

	protected static final Random random = new Random();
	protected class AMQPWork implements Work<byte[]> {
		protected final Delivery delivery;

		public AMQPWork(Delivery delivery) {
			this.delivery = delivery;
		}
		public long getEnqueueTime() {
			Date d = delivery.getProperties().timestamp;
			return d == null ? 0 : d.getTime();
		}
		public byte[] getBody() {
			return delivery.getBody();
		}

		public void workComplete() throws WorkQueueException {
			handleWorkComplete(delivery);
		}
		public void workAborted(WorkRetryType retryType, Throwable cause) throws WorkQueueException {
			handleWorkAborted(delivery, cause);
		}
		public Publisher<byte[]> getPublisher() throws WorkQueueException {
			// TODO Auto-generated method stub
			return null;
		}
		public boolean isComplete() {
			// TODO Auto-generated method stub
			return false;
		}
		public boolean isRedelivered() {
			// TODO Auto-generated method stub
			return false;
		}
		public String getOriginalQueueName() {
			return delivery.getProperties().replyTo;
		}
		public String getQueueName() {
			return getQueueKey();
		}

		public String getGuid() {
			return delivery.getEnvelope().getRoutingKey();
		}
		public void unblock(String... blocksToClear) {
			// TODO Auto-generated method stub			
		}
		public String getSubscription() {
			return null;
		}
		@Override
		public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException
		{
			// TODO Auto-generated method stub
			
		}
		@Override
		public boolean isUnguardedRetryAllowed()
		{
			// TODO Auto-generated method stub
			return false;
		}
		@Override
		public int getRetryCount()
		{
			// TODO Auto-generated method stub
			return 0;
		}
		@Override
		public Map<String, ?> getWorkProperties()
		{
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public QoS getQoS()
		{
			// TODO Auto-generated method stub
			return null;
		}
	}
	public FaultyAMQPWorkQueue() {
	}

	protected static String nextConsumerTag() {
		return "c"+String.valueOf(random.nextLong());
	}

	protected void handleWorkComplete(Delivery delivery) throws WorkQueueException {
		/* Is locking necessary for ack? Let's try it without
		 * 04/28/2008 (BSK) : Analysis of source code indicates that single frame methods (like basicAck) are thread-safe,
		 *                    but multi-frame methods like deliver and publish are not. But since basicAck is outgoing and
		 *                    deliver is incoming we can use the same channel (I think).
		 * 05/02/2008 (BSK) : Let's use a separate channel so that this does not interfere with pause / unpause or cancel / consume
		 * 05/08/2008 (BSK) : AMQP 0-8 does not allow ack'ing on a different channel than we receive delivery
		 * 05/08/2008 (BSK) : It also looks like RabbitMQ 1.2.0 does not support cancelling on a different channel
		 *
		 */
		//channelLock.lock();
		try {
			channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
		} catch(IOException ioe){
			throw new WorkQueueException("While acknowledging message with delivery  tag " + String.valueOf(delivery.getEnvelope().getDeliveryTag()), ioe);
		} finally {
			//channelLock.unlock();
		}
	}

	protected void handleWorkAborted(Delivery delivery, Throwable cause) throws WorkQueueException {
		log.error("Could not process message: " + delivery.getBody(), cause);
	}

	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getRealm() {
		return realm;
	}
	public void setRealm(String realm) {
		this.realm = realm;
	}
	public String getQueueKey() {
		return queueKey;
	}
	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}
	public int getConsumerMinSize() {
		return consumerMinSize;
	}
	public void setConsumerMinSize(int consumerMinSize) {
		this.consumerMinSize = consumerMinSize;
	}
	public int getConsumerMaxSize() {
		return consumerMaxSize;
	}
	public void setConsumerMaxSize(int consumerMaxSize) {
		this.consumerMaxSize = consumerMaxSize;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public boolean isMulticast() {
		return multicast;
	}
	public void setMulticast(boolean multicast) {
		this.multicast = multicast;
	}
	public WorkQueue.Retriever<byte[]> getRetriever() {
		return new WorkQueue.Retriever<byte[]>() {
			public Work<byte[]> next() throws WorkQueueException, InterruptedException {
				try {
					initializer.initialize();
				} catch(IOException e) {
					throw new WorkQueueException("Could not initialize queue consumer", e);
				}
				if(queue.size() < getConsumerMinSize()) {
					unpauseConsumer();
				}
				try {
					return new AMQPWork(queue.take().getValue());
				} catch(ShutdownSignalException e) {
					return null;
				}
			}
			public boolean cancel(Thread thread) {
				if(thread == null) return false;
				thread.interrupt();
				return true;
			}
			public void close() {
			}
			@Override
			public boolean arePrequisitesAvailable() {
				// TODO Auto-generated method stub
				return false;
			}
			@Override
			public void cancelPrequisiteCheck(Thread thread) {
				// TODO Auto-generated method stub
				
			}

			public void resetAvailability() {

			}
		};
	}

	protected void pauseConsumer() throws WorkQueueException {
		if(consumerState.get() == STATE_CONSUMING) {
		    channelLock.lock();
		    try {
		    	cancelSignal.signal();
		    } finally {
		    	channelLock.unlock();
		    }
		}
	}

	protected void unpauseConsumer() throws WorkQueueException {
		try {
			if(consumerState.compareAndSet(STATE_PAUSED, STATE_STARTING)) {
				//channelLock.lock();
				try {
					consumerTag = channel.basicConsume(ticket, queueKey, false, consumerTag, consumer);
				} finally {
					//channelLock.unlock();
				}
			}
		} catch(IOException e) {
			consumerState.compareAndSet(STATE_STARTING, STATE_PAUSED);
			throw new WorkQueueException("Could not start queue consumer", e);
		}
	}

	/**
	 * @see simple.app.WorkQueue#getWorkBodyType()
	 */
	@Override
	public Class<byte[]> getWorkBodyType() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see simple.app.WorkQueue#isAutonomous()
	 */
	@Override
	public boolean isAutonomous() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Future<Boolean> shutdown() {
		// TODO Auto-generated method stub
		return TRUE_FUTURE;
	}

	@Override
	public simple.app.WorkQueue.Retriever<byte[]> getRetriever(ProgressListener listener) throws WorkQueueException
	{
		// TODO Auto-generated method stub
		return null;
	}
}
