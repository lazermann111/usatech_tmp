/**
 *
 */
package com.usatech.app.test;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.io.Log;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

/**
 * @author Brian S. Krug
 *
 */
public class RedeliveryTestTask implements MessageChainTask {
	private static Log log = Log.getLog();

	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		if(taskInfo.isRedelivered()) {
			log.info("Task " + taskInfo.getStep().getAttributes() + " is redelivered");
			if(Math.random() > 0.75) {
				log.info("Requesting subsequent retry of task " + taskInfo.getStep().getAttributes() );
				throw new RetrySpecifiedServiceException("Retrying b/c of 1/4 check", WorkRetryType.NONBLOCKING_RETRY);
			}
			log.info("Processing task " + taskInfo.getStep().getAttributes());
			return 1;
		}
		log.info("Task " + taskInfo.getStep().getAttributes() + " is NOT redelivered");
		log.info("Requesting first retry of task " + taskInfo.getStep().getAttributes());
		throw new RetrySpecifiedServiceException("Retrying b/c not redelivered", WorkRetryType.NONBLOCKING_RETRY);
		//return 0;
	}

}
