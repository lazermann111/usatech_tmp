package com.usatech.app.test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import simple.app.AbstractService;
import simple.app.ServiceException;
import simple.app.ServiceStatus;
import simple.app.ServiceStatusInfo;
import simple.app.ServiceStatusListener;
import simple.util.MapBackedSet;
import simple.util.concurrent.ResultFuture;

public class NadaService extends AbstractService {
	protected final AtomicInteger threadCount = new AtomicInteger();
	protected final AtomicBoolean paused = new AtomicBoolean();
	protected final Set<ServiceStatusListener> serviceStatusListeners = new MapBackedSet<ServiceStatusListener>(new ConcurrentHashMap<ServiceStatusListener, Object>()); //new LinkedHashSet<ServiceStatusListener>();
	protected Thread[] threadCache = new Thread[16];

	public NadaService(String serviceName) {
		super(serviceName);
	}

	public boolean addServiceStatusListener(ServiceStatusListener listener) {
		boolean changed = serviceStatusListeners.add(listener);
		if(changed) {
			int end = getNumThreads();
			fireServiceStatusChanged(0, end, end, paused.get());
		}
		return changed;
	}

	public boolean removeServiceStatusListener(ServiceStatusListener listener) {
		return serviceStatusListeners.remove(listener);
	}

	protected void fireServiceStatusChanged(int start, int end, int aliveIndex, boolean paused) {
		Thread[] tc = threadCache;
		if(tc.length < end) {
			tc = new Thread[end+16];
			int len = Math.min(threadCache.length, tc.length);
			System.arraycopy(threadCache, 0, tc, 0, len);
			if(threadCache.length > tc.length)
				threadCache = tc;
		}
		for(int i = start; i < end; i++) {
			if(tc[i] == null)
				tc[i] = new Thread("Nada Thread #" + (i+1));
			ServiceStatus status;
			if(i >= aliveIndex)
				status = ServiceStatus.STOPPED;
			else if(paused)
				status = ServiceStatus.PAUSED;
			else
				status = ServiceStatus.STARTED;
			ServiceStatusInfo ssi = new ServiceStatusInfo();
			ssi.setIterationCount(1);
			ssi.setService(this);
			ssi.setServiceStatus(status);
			ssi.setThread(tc[i]);
			for(ServiceStatusListener l : serviceStatusListeners) {
				l.serviceStatusChanged(ssi);
			}
		}
	}
	public int getNumThreads() {
		return threadCount.get();
	}

	public String getServiceName() {
		return serviceName;
	}

	public int pauseThreads() throws ServiceException {
		if(paused.compareAndSet(false, true)) {
			int end = getNumThreads();
			fireServiceStatusChanged(0, end, end, true);
			return end;
		} else
			return 0;
	}

	public int restartThreads(long timeout) throws ServiceException {
		int end = getNumThreads();
		fireServiceStatusChanged(0, end, end, false);
		return end;
	}

	public int startThreads(int numThreads) throws ServiceException {
		int end = threadCount.addAndGet(numThreads);
		fireServiceStatusChanged(end - numThreads, end, end, false);
		return numThreads;
	}

	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		int end = threadCount.getAndSet(0);
		fireServiceStatusChanged(0, end, 0, false);
		return end;
	}

	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		int orig = threadCount.getAndSet(0);
		int n = Math.min(orig, numThreads);
		int end = threadCount.addAndGet(orig - n);
		fireServiceStatusChanged(end, orig, end, false);
		return n;
	}

	public int unpauseThreads() throws ServiceException {
		if(paused.compareAndSet(true, false)) {
			int end = getNumThreads();
			fireServiceStatusChanged(0, end, end, false);
			return end;
		} else
			return 0;
	}

	@Override
	public Future<Integer> stopAllThreads(final boolean force) throws ServiceException {
		return new ResultFuture<Integer>() {
			@Override
			protected Integer produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				try {
					return stopAllThreads(force, TimeUnit.MILLISECONDS.convert(timeout, unit));
				} catch(ServiceException e) {
					throw new ExecutionException(e);
				}
			}
		};
	}

	@Override
	public Future<Integer> stopThreads(final int numThreads, final boolean force) throws ServiceException {
		return new ResultFuture<Integer>() {
			@Override
			protected Integer produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				try {
					return stopThreads(numThreads, force, TimeUnit.MILLISECONDS.convert(timeout, unit));
				} catch(ServiceException e) {
					throw new ExecutionException(e);
				}
			}
		};
	}
}
