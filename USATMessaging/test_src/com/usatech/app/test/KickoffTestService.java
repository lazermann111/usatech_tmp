/**
 *
 */
package com.usatech.app.test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

import simple.app.AbstractService;
import simple.app.BasicQoS;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.app.ServiceStatusListener;
import simple.io.ByteInput;
import simple.io.Log;
import simple.util.concurrent.TrivialFuture;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

/**
 * @author Brian S. Krug
 *
 */
public class KickoffTestService extends AbstractService {
	private static final Log log = Log.getLog();
	protected Publisher<ByteInput> publisher;
	protected String queueKey;
	protected static final AtomicLong idGenerator = new AtomicLong();
	protected final Map<String,Object> attributes = new HashMap<String,Object>();
	protected long timeToLive;
	protected boolean multicast;
	protected boolean temporary;
	protected boolean first = true;
	protected int correlations;

	public boolean isTemporary() {
		return temporary;
	}

	public void setTemporary(boolean temporary) {
		this.temporary = temporary;
	}

	public KickoffTestService(String serviceName) {
		super(serviceName);
	}

	/**
	 * @see simple.app.Service#getNumThreads()
	 */
	@Override
	public int getNumThreads() {
		return 0;
	}

	/**
	 * @see simple.app.Service#pauseThreads()
	 */
	@Override
	public int pauseThreads() throws ServiceException {
		return 0;
	}

	/**
	 * @see simple.app.Service#removeServiceStatusListener(simple.app.ServiceStatusListener)
	 */
	@Override
	public boolean removeServiceStatusListener(ServiceStatusListener listener) {
		return false;
	}

	/**
	 * @see simple.app.Service#restartThreads(long)
	 */
	@Override
	public int restartThreads(long timeout) throws ServiceException {
		return 0;
	}

	/**
	 * @see simple.app.Service#startThreads(int)
	 */
	@Override
	public int startThreads(final int numThreads) throws ServiceException {
		long ttl = getTimeToLive();
		final QoS qos;
		if(ttl > 0 || isTemporary()) {
			BasicQoS bqos = new BasicQoS();
			bqos.setPersistent(!isTemporary());
			if(ttl > 0)
				bqos.setTimeToLive(ttl);
			qos = bqos;
		} else {
			qos = null;
		}
		if(first) {
			first = false;
			Thread t = new Thread() {
				public void run() {
					try {
						Thread.sleep(5000);
					} catch(InterruptedException e1) {
						//
					}
					try {
						publishMessage(numThreads, qos);
					} catch(ServiceException e) {
						log.warn("Could not publish messages", e);
					}
				}
			};
			t.setDaemon(true);
			t.start();
		} else {
			publishMessage(numThreads, qos);
		}
		return numThreads;
	}
	protected void publishMessage(int n, QoS qos) throws ServiceException {
		for(int i = 0; i < n; i++) {
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep(getQueueKey(), isMulticast());
			step.addStringAttribute("From", getClass().getName());
			step.addLongAttribute("ID", idGenerator.getAndIncrement());
			step.addIntAttribute("Thread #", i+1);
			for(Map.Entry<String,Object> entry : attributes.entrySet())
				step.addLiteralAttribute(entry.getKey(), entry.getValue());
			if(getCorrelations() > 0)
				step.setCorrelation((i % getCorrelations()) + 1L);
			MessageChainService.publish(mc, getPublisher(), null, qos);
		}
	}
	/**
	 * @see simple.app.Service#stopAllThreads(boolean, long)
	 */
	@Override
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		return 0;
	}

	/**
	 * @see simple.app.Service#stopThreads(int, boolean, long)
	 */
	@Override
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		return 0;
	}

	/**
	 * @see simple.app.Service#unpauseThreads()
	 */
	@Override
	public int unpauseThreads() throws ServiceException {
		return 0;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}

	@Override
	public Future<Integer> stopAllThreads(boolean force) throws ServiceException {
		return new TrivialFuture<Integer>(new Integer(0));
	}

	@Override
	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException {
		return new TrivialFuture<Integer>(new Integer(0));
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public long getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}

	public boolean isMulticast() {
		return multicast;
	}

	public void setMulticast(boolean multicast) {
		this.multicast = multicast;
	}

	public int getCorrelations() {
		return correlations;
	}

	public void setCorrelations(int correlations) {
		this.correlations = correlations;
	}
}
