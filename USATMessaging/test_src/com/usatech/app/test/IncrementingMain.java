package com.usatech.app.test;

import java.beans.IntrospectionException;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.BeanFactoryByProperties;
import simple.bean.ConvertException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.Holder;
import simple.util.CollectionUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.GetResponse;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.app.amqp.AMQPBytesPublisher;

public class IncrementingMain {
	private static final Log log = Log.getLog();
	/**
	 * @param args
	 * @throws IOException
	 * @throws ServiceException
	 * @throws InterruptedException
	 * @throws ConvertException
	 * @throws ParseException
	 * @throws InstantiationException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IntrospectionException
	 */
	public static void main(String[] args) throws Exception {
		Properties properties = new Properties();
		properties.load(IncrementingMain.class.getResourceAsStream("incrementingJMS.properties"));
		//Properties subProperties = CollectionUtils.getSubProperties(properties, "com.usatech.app.AMQPPublisher.");
		Properties subProperties = CollectionUtils.getSubProperties(properties, "com.usatech.app.JMSPublisher.");
		/*
		testBeanFactory(subProperties.getProperty("host"),
				simple.bean.ConvertUtils.getInt(subProperties.getProperty("port")),
				subProperties.getProperty("realm"),
				subProperties.getProperty("exchange", ""),
				"test.beanfactory");
		/* /
		cleanQueue(subProperties.getProperty("host"),
				simple.bean.ConvertUtils.getInt(subProperties.getProperty("port")),
				subProperties.getProperty("realm"),
				subProperties.getProperty("exchange", ""),
				"test.schedule");
		/* /
		deleteQueue(subProperties.getProperty("host"),
				simple.bean.ConvertUtils.getInt(subProperties.getProperty("port")),
				subProperties.getProperty("realm"),
				subProperties.getProperty("exchange", ""),
				"even");

    	/*/
		Publisher<ByteInput> publisher = simple.app.Main.configure(Publisher.class, subProperties, properties, null, true);
    	publishN(publisher, 1);
    	if(publisher instanceof Closeable) {
    		((Closeable)publisher).close();
    	}
    	// */
	}

	protected static void testBeanFactory(String host, int port, String realm, String exchange, String queueName) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException, ServiceException, InterruptedException {
		Map<String,Object> properties = new HashMap<String, Object>();
		properties.put("value", new Object());
		Holder<?> h1 = BeanFactoryByProperties.getInstance(Holder.class, properties);
		System.out.println("First object hash = " + h1.getValue().hashCode() + "; BeanFactory Size = " + BeanFactoryByProperties.size());
		h1 = null;
		//BeanFactoryByProperties.beanCache.
		properties.put("value", new Object());
		Holder<?> h2 = BeanFactoryByProperties.getInstance(Holder.class, properties);
		System.out.println("First object hash = " + h2.getValue().hashCode()+ "; BeanFactory Size = " + BeanFactoryByProperties.size());
		h2 = null;

		//*
		properties.put("host", host);
		properties.put("port", port);
		properties.put("realm", realm);
		properties.put("exchange", exchange);
		Publisher<byte[]> p = BeanFactoryByProperties.getInstance(AMQPBytesPublisher.class, properties);
		p.publish(queueName, "test".getBytes());
		p = null; // allow gc
		//*/
		System.out.println("Before first clean size = " +	BeanFactoryByProperties.size());
		//BeanFactoryByProperties.clean();
		System.out.println("Before gc size = " +	BeanFactoryByProperties.size());
		System.gc();
		Thread.sleep(5000);
		System.out.println("Before clean size = " +	BeanFactoryByProperties.size());
		//BeanFactoryByProperties.clean();
		System.out.println("After clean size = " +	BeanFactoryByProperties.size());
		/*
		Object dummy = BeanFactoryByProperties.getInstance(Object.class, properties);
		System.out.println("Work complete. Waiting forever to see if cleanup occurs");

		synchronized(properties) {
			properties.wait();
		}*/
	}
	protected static void publishN(Publisher<ByteInput> publisher, int n) throws ServiceException, IOException, InterruptedException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("odd");
		step.addStringAttribute("N", String.valueOf(n));
		MessageChainStep retryStep = mc.addStep("odd");
		retryStep.addStringAttribute("N", String.valueOf(n));
		step.setNextSteps(255, retryStep);
		MessageChainService.publish(mc, publisher);
	}

	protected static void cleanQueue(String host, int port, String realm, String exchange, String queueName) throws IOException {
		Connection conn = new ConnectionFactory().newConnection(host, port);
		try {
			Channel channel = conn.createChannel();
			int ticket = channel.accessRequest(realm);
			channel.exchangeDeclare(ticket, exchange, "direct", true);
			channel.queueDeclare(ticket, queueName, true);
			channel.queueBind(ticket, queueName, exchange, queueName);
			GetResponse resp;
			while((resp = channel.basicGet(ticket, queueName, true)) != null) {
				log.debug("" + resp.getMessageCount() + " messages left on queue " + queueName);
				if(resp.getMessageCount() == 0) break;
			}
		} finally {
			conn.close(200, "All Done");
		}
	}
	protected static void deleteQueue(String host, int port, String realm, String exchange, String queueName) throws IOException {
		Connection conn = new ConnectionFactory().newConnection(host, port);
		try {
			Channel channel = conn.createChannel();
			int ticket = channel.accessRequest(realm);
			channel.queueDelete(ticket, queueName);
		} finally {
			conn.close(200, "All Done");
		}
	}
}
