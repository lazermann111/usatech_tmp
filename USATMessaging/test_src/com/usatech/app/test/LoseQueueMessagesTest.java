/* 
 * Test class showing rollback behaviour for KahaDB persistence failing after sessions, connections
 * are closed, and reopened
 */

package com.usatech.app.test;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.InOutInteraction;
import simple.io.Interaction;
import simple.io.Log;
import simple.text.StringUtils;

import com.usatech.app.jms.ExtendedSession;
import com.usatech.app.jms.JMSBase;
import com.usatech.app.jms.JMSByteMessageByteInput;
import com.usatech.app.jms.JMSStreamMessageByteInput;
import com.usatech.app.jms.JMSTextHexMessageByteInput;

public class LoseQueueMessagesTest {
	private static final Log log = Log.getLog();
	
    public static String CONNECTION = "failover://(ssl://localhost:61616?soTimeout=5000&keepAlive=true&connectionTimeout=20000,ssl://localhost:61618?soTimeout=5000&keepAlive=true&connectionTimeout=20000)?randomize=false&maxReconnectAttempts=10"; //$NON-NLS-1$ 
    public static String QUEUENAME = "test.losemessageonrollback.kahadb"; //$NON-NLS-1$
    protected static enum FailType { ROLLBACK, INTERRUPT, FAILOVER, ABORT };
    protected FailType failType = FailType.INTERRUPT;
    	
    protected void run() throws IOException, JMSException, NamingException, InterruptedException, ServiceException {
    	String queueName = QUEUENAME + ".1";
    	final String otherQueueName = QUEUENAME + ".2";
    	final AtomicBoolean committed = new AtomicBoolean();
    	// consume all messages in queue to clear it out
    	ExtendedSession session = JMSBase.createSession(true);
    	try {
    		while(consumeMessageAndCommit(session, queueName, false) != null) ;
    		while(consumeMessageAndCommit(session, otherQueueName, false) != null) ;
    	} finally {
    		session.close();
    	}
    	log.info("Cleared out message queue by consume all messages");
    	final AtomicBoolean otherReceived = new AtomicBoolean();
    	final AtomicBoolean otherDone = new AtomicBoolean();
    	final byte[] otherContent = new byte[] {0x33, 0x44, 0x01};
    	Thread consumeOtherThread = new Thread() {
    		public void run() {
    			try {
	    			ExtendedSession session = JMSBase.createSession(true);
	    	    	try {
	    	    		byte[] content;
	    	    		while(!otherDone.get()) {
	    	    			content = consumeMessageAndCommit(session, otherQueueName, false);
	    	    			if(content != null && Arrays.equals(content, otherContent)) {
	    	    				log.info("Got the other message");
	    	    				otherReceived.set(true);
	    	    				return;
	    	    			}
	    	    		}
	    	    		log.warn("Did not get the other message");
	    	    	} finally {
	    	    		session.close();
	    	    	}	
    			} catch(Exception e) {
    				log.error("In Other Thread", e);
    			}
    		}
    	};
    	consumeOtherThread.setDaemon(true);
    	consumeOtherThread.setName("ConsumeOtherThread");
    	consumeOtherThread.start();
    	// produce 2 messages
    	byte[][] contents = new byte[][] {
    		new byte[] { (byte) 0xAA, (byte) 0xBB, (byte) 0xCC, 0x01 },
    		new byte[] { (byte) 0xAA, (byte) 0xBB, (byte) 0xCC, 0x02 },
    	};
    	session = JMSBase.createSession(true);
    	try {
			Destination dest = session.createQueue(queueName);		
	    	sendMessages(session, dest, contents);
	    	session.commit();
    	} finally {
    		session.close();
    	}
    	
    	session = JMSBase.createSession(true);
    	try {
	    	// consume a message and rollback
	    	byte[] content1 = consumeMessageAndFail(session, queueName, otherQueueName, otherContent, committed);
	    	boolean firstCommitted = committed.get();	
			// re-connect
	    	session.close();
	    	session = JMSBase.createSession(true);
	    	// consume a message 
	    	byte[] content2 = consumeMessageAndCommit(session, queueName, true);
	    	
	    	// is the message the first or the second?
	    	if(Arrays.equals(contents[0], content2)) {
	    		log.info("Same message was consumed. It was not lost!");
	    		byte[] content3 = consumeMessageAndCommit(session, queueName, false);
	    		if(content3 == null) {
	    			log.warn("No messages left. Second one was lost!");
	    		} else if(Arrays.equals(contents[1], content3)) {
	    			log.info("Second message consumed");
	    		} else {
	    			log.warn("Message does not match second message. Not sure what happened");
	    		}
	    	} else if(firstCommitted && Arrays.equals(contents[1], content2)) {
	    		log.info("First message was commited and then second was consumed. Message was not lost");
	    		
	    	} else {
	    		log.warn("Different message was consumed. Consuming again to see if message is lost");
	    	   	// if second, consume again with no wait to see if message is lost
	    		byte[] content3;
	    		int cnt = 0;
	    		boolean found = false;
	    		while((content3 = consumeMessageAndCommit(session, queueName, false)) != null) {
		    		if(Arrays.equals(contents[0], content3)) {
		    			log.info("Message was not lost. It was redelivered later");
		    			found = true;
		    		}
		    		cnt++;
	    		}
	    		if(cnt == 0) {
	    			log.warn("No messages left. First one was lost!");
	    		} else if(!found) {
	    			log.warn("Consumed " + cnt + " messages but none matched the first. Message was lost!");	
	    		}
	    	}
	    	otherDone.set(true);
	    	consumeOtherThread.join();
	    	if(firstCommitted && !otherReceived.get()) {
	    		log.warn("First message was committed, but other message was not received. Message was lost!");
	    	}
	    	session.close();
	    	log.info("Clearing out message queues by consuming all messages");
	    	session = JMSBase.createSession(true);
	    	try {
	    		while(consumeMessageAndCommit(session, queueName, false) != null) ;
	    		while(consumeMessageAndCommit(session, otherQueueName, false) != null) ;
	    	} finally {
	    		//session.close();
	    	}
	    	
    	} finally {
    		session.close();
    	}
    }
    protected byte[] consumeMessageAndFail(Session session, String queueName, String publishQueueName, byte[] publishContent, final AtomicBoolean committed) throws IOException, JMSException, NamingException, InterruptedException {
    	committed.set(false);
    	boolean okay = false;
    	byte[] content = null;
    	try {
			content = consumeMessage(session, queueName, true);
    		Destination publishDest = session.createQueue(publishQueueName);
			if(content == null) {
				return null;
			}
			// rollback a message and close the connection		
    		log.info("Failing consumption of message with " + failType);
    		switch(failType) {
    			case ROLLBACK:
    				sendMessages(session, publishDest, publishContent);
    				session.rollback();
    				break;
    			case INTERRUPT:
		    		ThreadGroup tg = Thread.currentThread().getThreadGroup();
		    		Thread[] threads = new Thread[tg.activeCount() + 1];
		    		int n = tg.enumerate(threads, true);
		    		for(int i = 0; i < n; i++) {
		    			if(threads[i] != null && threads[i].getName().startsWith("ActiveMQ Transport:")) {
		    				log.debug("Found transport threads to interrupt: " + threads[i].getName());
		    				threads[i].interrupt();
		    				break;
		    			}
		    		}
		    		sendMessages(session, publishDest, new byte[] {0x33, 0x44, 0x01});
    				break;
    			case FAILOVER:
    				Interaction in = new InOutInteraction();
    				in.readLine("Restart ActiveMQ then press enter to continue");
    				sendMessages(session, publishDest, new byte[] {0x33, 0x44, 0x01});
    				break;
    			case ABORT:
    				log.info("Look in ActiveMQ to see if it has the messages");
    				sendMessages(session, publishDest, new byte[] {0x33, 0x44, 0x01});
    				Runtime.getRuntime().halt(0);
    				break;
    		}
	    	okay = true;
        } catch(JMSException e) {
    		log.warn("Got error", e);
    	} finally {	
    		if(okay) {
    			try {
    				session.commit();
    				committed.set(true);
    			} catch(JMSException e) {
    	    		log.warn("Got error", e);
    	    	} 
    		} else
    			session.rollback();
    	}
    	return content;
    }
    protected byte[] consumeMessageAndCommit(Session session, String queueName, boolean wait) throws IOException, JMSException, NamingException, InterruptedException {
    	try {
    		return consumeMessage(session, queueName, wait);
    	} finally {	
    		session.commit();
    	}
    }
    protected byte[] consumeMessage(Session session, String queueName, boolean wait) throws IOException, JMSException, NamingException, InterruptedException {
		Destination dest = session.createQueue(queueName);
		MessageConsumer consumer = session.createConsumer(dest);
		Message msg = (wait ? consumer.receive() : consumer.receive(2000));
		ByteInput bi;
		if(msg instanceof BytesMessage) {
			bi = new JMSByteMessageByteInput((BytesMessage)msg);
		} else if(msg instanceof StreamMessage) {
			bi = new JMSStreamMessageByteInput((StreamMessage)msg);
		} else if(msg instanceof TextMessage) {
			bi = new JMSTextHexMessageByteInput((TextMessage)msg);	
		} else if(msg == null) {
			return null;
		} else {
			throw new JMSException("JMS Message Type " + JMSBase.getMessageType(msg) + " not supported on message ID " + JMSBase.getMessageId(msg));
		}
		byte[] content = new byte[bi.length()];
		bi.readBytes(content, 0, content.length);
		final boolean redelivered = JMSBase.getRedeliveredSafely(msg);
		final String desc = "JMS Message " + JMSBase.getMessageInfo(msg) + " [" + (redelivered ? "redelivered" : "first delivery") + "]";
		log.info("Received " + desc + ": " + StringUtils.toHex(content));
    	
    	return content;
    }
    
    protected void sendMessages(Session session, Destination dest, byte[]... contents) throws IOException, JMSException {
    	MessageProducer producer = session.createProducer(dest);
		try {
	    	for(byte[] content : contents) {
				BytesMessage msg = session.createBytesMessage();
				msg.writeBytes(content);
				producer.send(dest, msg);
				log.info("Published JMS Message " + JMSBase.getMessageInfo(msg) + ": " + StringUtils.toHex(content));		    	
	    	}
	    } finally {
			producer.close();
		}
    }
    public static void main(String[] args) {
    	try {
			new LoseQueueMessagesTest().run();
		} catch(IOException e) {
			log.error("", e);
		} catch(JMSException e) {
			log.error("", e);
		} catch(NamingException e) {
			log.error("", e);
		} catch(InterruptedException e) {
			log.error("", e);
		} catch(ServiceException e) {
			log.error("", e);
		}
		System.exit(0);
    }
}
