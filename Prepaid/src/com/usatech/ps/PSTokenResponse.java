package com.usatech.ps;

import java.io.Serializable;

public class PSTokenResponse extends PSResponse implements Serializable {
	private static final long serialVersionUID = -4454140598346171417L;
	
	protected String tokenHex = "";
	protected long cardId = 0;
	protected long globalAccountId = 0;

	public String getTokenHex() {
		return tokenHex;
	}

	public void setTokenHex(String tokenHex) {
		this.tokenHex = tokenHex;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public long getGlobalAccountId() {
		return globalAccountId;
	}

	public void setGlobalAccountId(long globalAccountId) {
		this.globalAccountId = globalAccountId;
	}

}
