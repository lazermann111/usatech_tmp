package com.usatech.ps;

import java.io.Serializable;
import java.util.Date;

public class PSUser extends PSResponse implements Serializable {
	private static final long serialVersionUID = -769396722070107231L;
	
	protected String userName;
	protected String firstname;
	protected String lastname;
	protected String address1;
	protected String city;
	protected String state;
	protected String postal;
	protected String country;
	protected Date created;
	protected long consumerId;
	protected long consumerTypeId;
	protected boolean registered;
	protected String emailAddress;
	protected String email;
	protected String mobile;
	protected int carrierId;
	protected int preferredCommType;
	

	public boolean isRegistered() {
		return registered;
	}
	public void setRegistered(boolean registered) {
		this.registered = registered;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public long getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getConsumerTypeId() {
		return consumerTypeId;
	}
	public void setConsumerTypeId(long consumerTypeId) {
		this.consumerTypeId = consumerTypeId;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getCarrierId() {
		return carrierId;
	}
	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}
	public int getPreferredCommType() {
		return preferredCommType;
	}
	public void setPreferredCommType(int preferredCommType) {
		this.preferredCommType = preferredCommType;
	}
	
}
