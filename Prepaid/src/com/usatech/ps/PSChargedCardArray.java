package com.usatech.ps;

import java.io.Serializable;

public class PSChargedCardArray extends PSResponse implements Serializable {
	private static final long serialVersionUID = -2244671018786521434L;
	protected PSChargedCard[] chargedCardArray;
	public PSChargedCard[] getChargedCardArray() {
		return chargedCardArray;
	}
	public void setChargedCardArray(PSChargedCard[] chargedCardArray) {
		this.chargedCardArray = chargedCardArray;
	}

}