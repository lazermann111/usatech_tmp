package com.usatech.ps;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PSPrepaidActivity implements Serializable {
	
	private static final long serialVersionUID = -2644408056890515767L;
	protected String id;
	protected String card;
	protected Date date;
	protected BigDecimal purchase;
	protected BigDecimal replenish;
	protected BigDecimal discount; 
	protected String location;
	protected String address1;
	protected String address2;
	protected String city;
	protected String state;
	protected String postal;
	protected String country;
	protected String currencyCd;
	protected String type;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCard() {
		return card;
	}
	public void setCard(String card) {
		this.card = card;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public BigDecimal getPurchase() {
		return purchase;
	}
	public void setPurchase(BigDecimal purchase) {
		this.purchase = purchase;
	}
	public BigDecimal getReplenish() {
		return replenish;
	}
	public void setReplenish(BigDecimal replenish) {
		this.replenish = replenish;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
}
