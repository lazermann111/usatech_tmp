package com.usatech.ps;

import java.io.Serializable;
import java.math.BigDecimal;

public class PSPrepaidActivities extends PSResponse implements Serializable {

	private static final long serialVersionUID = 2291498331464258676L;
	protected PSPrepaidActivity[] prepaidActivities;
	protected BigDecimal purchaseTotal;
	protected BigDecimal replenishTotal;
	protected BigDecimal discountTotal; 
	
	public PSPrepaidActivity[] getPrepaidActivities() {
		return prepaidActivities;
	}
	public void setPrepaidActivities(PSPrepaidActivity[] prepaidActivities) {
		this.prepaidActivities = prepaidActivities;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public BigDecimal getPurchaseTotal() {
		return purchaseTotal;
	}
	public void setPurchaseTotal(BigDecimal purchaseTotal) {
		this.purchaseTotal = purchaseTotal;
	}
	public BigDecimal getReplenishTotal() {
		return replenishTotal;
	}
	public void setReplenishTotal(BigDecimal replenishTotal) {
		this.replenishTotal = replenishTotal;
	}
	public BigDecimal getDiscountTotal() {
		return discountTotal;
	}
	public void setDiscountTotal(BigDecimal discountTotal) {
		this.discountTotal = discountTotal;
	}
	
}
