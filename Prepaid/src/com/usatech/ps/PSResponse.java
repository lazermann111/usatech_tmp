package com.usatech.ps;

import java.io.Serializable;

public class PSResponse implements Serializable {
	private static final long serialVersionUID = 4004879548311642953L;
	
	protected int returnCode = 0;
	protected String returnMessage = "Failed";
	
	public int getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}		
}
