package com.usatech.ps;

public class PSConsumerAccountBase {
	protected long consumerAcctId;
	protected long globalAccountId;
	protected String currencyCd;
	protected String deactivationYYMM;
	protected long consumerTypeId;
	
	public long getConsumerAcctId() {
		return consumerAcctId;
	}
	public void setConsumerAcctId(long consumerAcctId) {
		this.consumerAcctId = consumerAcctId;
	}
	public long getGlobalAccountId() {
		return globalAccountId;
	}
	public void setGlobalAccountId(long globalAccountId) {
		this.globalAccountId = globalAccountId;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	public String getDeactivationYYMM() {
		return deactivationYYMM;
	}
	public void setDeactivationYYMM(String deactivationYYMM) {
		this.deactivationYYMM = deactivationYYMM;
	}
	public long getConsumerTypeId() {
		return consumerTypeId;
	}
	public void setConsumerTypeId(long consumerTypeId) {
		this.consumerTypeId = consumerTypeId;
	}
	
}
