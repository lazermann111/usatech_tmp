package com.usatech.ps;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PSReplenishInfo extends PSResponse implements Serializable {

	private static final long serialVersionUID = 7944681862427063529L;
	
	protected long replenishId;
	protected int priority;
	protected Date replenishDate;
	protected BigDecimal replenishAmount;
	protected BigDecimal replenishThreshhold;
	protected String replenishCardNum;
	protected String replenishCardKey;
	protected int replenishType;
	protected String currencyCd;
	public long getReplenishId() {
		return replenishId;
	}
	public void setReplenishId(long replenishId) {
		this.replenishId = replenishId;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public Date getReplenishDate() {
		return replenishDate;
	}
	public void setReplenishDate(Date replenishDate) {
		this.replenishDate = replenishDate;
	}
	public BigDecimal getReplenishAmount() {
		return replenishAmount;
	}
	public void setReplenishAmount(BigDecimal replenishAmount) {
		this.replenishAmount = replenishAmount;
	}
	public BigDecimal getReplenishThreshhold() {
		return replenishThreshhold;
	}
	public void setReplenishThreshhold(BigDecimal replenishThreshhold) {
		this.replenishThreshhold = replenishThreshhold;
	}
	public String getReplenishCardNum() {
		return replenishCardNum;
	}
	public void setReplenishCardNum(String replenishCardNum) {
		this.replenishCardNum = replenishCardNum;
	}
	public String getReplenishCardKey() {
		return replenishCardKey;
	}
	public void setReplenishCardKey(String replenishCardKey) {
		this.replenishCardKey = replenishCardKey;
	}
	public int getReplenishType() {
		return replenishType;
	}
	public void setReplenishType(int replenishType) {
		this.replenishType = replenishType;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	
}
