/**
 * PsSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package com.usatech.ps;

import com.usatech.prepaid.PSRequestHandler;
import com.usatech.prepaid.PrepaidAxisServlet;

import simple.bean.ReflectionUtils;
import simple.io.Log;

/**
 * PsSkeleton java skeleton for the axisService
 */
public class PsSkeleton {
	private static final Log log = Log.getLog();

	/**
	 * Auto generated method signature
	 * 
	 * @param updateUser
	 * @return updateUserResponse
	 */

	public com.usatech.ps.UpdateUserResponse updateUser(
			com.usatech.ps.UpdateUser updateUser) {
		PSConsumerSetting[] consumerSetting = null;
		com.usatech.ps.xsd.PSConsumerSetting[] settings = updateUser
				.getConsumerSettings();
		if (settings != null) {
			consumerSetting = new PSConsumerSetting[settings.length];
			for (int i = 0; i < settings.length; i++) {
				consumerSetting[i] = new PSConsumerSetting(
						settings[i].getSettingId(), settings[i].getValue());
			}
		}
		PSResponse response = com.usatech.prepaid.PSRequestHandler.updateUser(
				PrepaidAxisServlet.SOAP_PROTOCOL, updateUser.getOldUsername(),
				updateUser.getUsername(), updateUser.getPassword(),
				updateUser.getFirstname(), updateUser.getLastname(),
				updateUser.getAddress1(), updateUser.getCity(),
				updateUser.getState(), updateUser.getPostal(),
				updateUser.getCountry(), consumerSetting);
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.UpdateUserResponse msgResponse = new com.usatech.ps.UpdateUserResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

         
	/**
	 * Auto generated method signature
	 * 
	 * @param updateUserWithMobile
	 * @return updateUserWithMobileResponse
	 */

	public com.usatech.ps.UpdateUserWithMobileResponse updateUserWithMobile(
			com.usatech.ps.UpdateUserWithMobile updateUserWithMobile) {
		
		PSConsumerSetting[] consumerSetting = null;
		com.usatech.ps.xsd.PSConsumerSetting[] settings = updateUserWithMobile
				.getConsumerSettings();
		if (settings != null) {
			consumerSetting = new PSConsumerSetting[settings.length];
			for (int i = 0; i < settings.length; i++) {
				consumerSetting[i] = new PSConsumerSetting(
						settings[i].getSettingId(), settings[i].getValue());
			}
		}
		PSResponse response = com.usatech.prepaid.PSRequestHandler.updateUser(
				PrepaidAxisServlet.SOAP_PROTOCOL, updateUserWithMobile.getOldUsername(),
				updateUserWithMobile.getPreferredCommType(), updateUserWithMobile.getEmail(),
				updateUserWithMobile.getMobile(), updateUserWithMobile.getCarrierId(),
				updateUserWithMobile.getPassword(),
				updateUserWithMobile.getFirstname(), updateUserWithMobile.getLastname(),
				updateUserWithMobile.getAddress1(), updateUserWithMobile.getCity(),
				updateUserWithMobile.getState(), updateUserWithMobile.getPostal(),
				updateUserWithMobile.getCountry(), consumerSetting);
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.UpdateUserWithMobileResponse msgResponse = new com.usatech.ps.UpdateUserWithMobileResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param createUser
	 * @return createUserResponse
	 */

	public com.usatech.ps.CreateUserResponse createUser(
			com.usatech.ps.CreateUser createUser) {
		PSConsumerSetting[] consumerSetting = null;
		com.usatech.ps.xsd.PSConsumerSetting[] settings = createUser
				.getConsumerSettings();
		if (settings != null) {
			consumerSetting = new PSConsumerSetting[settings.length];
			for (int i = 0; i < settings.length; i++) {
				consumerSetting[i] = new PSConsumerSetting(
						settings[i].getSettingId(), settings[i].getValue());
			}
		}
		PSTokenResponse response = com.usatech.prepaid.PSRequestHandler.createUser(
				PrepaidAxisServlet.SOAP_PROTOCOL,
				createUser.getPrepaidCardNum(),
				createUser.getPrepaidSecurityCode(), createUser.getUsername(),
				createUser.getFirstname(), createUser.getLastname(),
				createUser.getAddress1(), createUser.getCity(),
				createUser.getState(), createUser.getPostal(),
				createUser.getCountry(), createUser.getPassword(),
				createUser.getConfirmPassword(), consumerSetting);
		com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.CreateUserResponse msgResponse = new com.usatech.ps.CreateUserResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}
	
        /**
         * Auto generated method signature
         * 
                                     * @param createUserWithMobile 
             * @return createUserWithMobileResponse 
         */
        
                 public com.usatech.ps.CreateUserWithMobileResponse createUserWithMobile
                  (
                  com.usatech.ps.CreateUserWithMobile createUserWithMobile
                  )
            {
                	 PSConsumerSetting[] consumerSetting = null;
             		com.usatech.ps.xsd.PSConsumerSetting[] settings = createUserWithMobile
             				.getConsumerSettings();
             		if (settings != null) {
             			consumerSetting = new PSConsumerSetting[settings.length];
             			for (int i = 0; i < settings.length; i++) {
             				consumerSetting[i] = new PSConsumerSetting(
             						settings[i].getSettingId(), settings[i].getValue());
             			}
             		}
             		PSTokenResponse response = com.usatech.prepaid.PSRequestHandler.createUser(
             				PrepaidAxisServlet.SOAP_PROTOCOL,
             				createUserWithMobile.getPrepaidCardNum(),
             				createUserWithMobile.getPrepaidSecurityCode(), createUserWithMobile.getPreferredCommType(),
             				createUserWithMobile.getEmail(),createUserWithMobile.getMobile(),createUserWithMobile.getCarrierId(),
             				createUserWithMobile.getFirstname(), createUserWithMobile.getLastname(),
             				createUserWithMobile.getAddress1(), createUserWithMobile.getCity(),
             				createUserWithMobile.getState(), createUserWithMobile.getPostal(),
             				createUserWithMobile.getCountry(), createUserWithMobile.getPassword(),
             				createUserWithMobile.getConfirmPassword(), consumerSetting);
             		com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
             		try {
             			ReflectionUtils.copyProperties(webResponse, response);
             		} catch (Exception e) {
             			log.error("", e);
             		}
             		com.usatech.ps.CreateUserWithMobileResponse msgResponse = new com.usatech.ps.CreateUserWithMobileResponse();
             		msgResponse.set_return(webResponse);
             		return msgResponse;
        }
		

	/**
	 * Auto generated method signature
	 * 
	 * @param getUser
	 * @return getUserResponse
	 */

	public com.usatech.ps.GetUserResponse getUser(com.usatech.ps.GetUser getUser) {
		PSUser response = com.usatech.prepaid.PSRequestHandler.getUser(
				PrepaidAxisServlet.SOAP_PROTOCOL, getUser.getUsername(),
				getUser.getPassword());
		com.usatech.ps.xsd.PSUser webResponse = new com.usatech.ps.xsd.PSUser();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.GetUserResponse msgResponse = new com.usatech.ps.GetUserResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getPrepaidAccounts
	 * @return getPrepaidAccountsResponse
	 */

	public com.usatech.ps.GetPrepaidAccountsResponse getPrepaidAccounts(
			com.usatech.ps.GetPrepaidAccounts getPrepaidAccounts) {
		PSPrepaidAccounts response = com.usatech.prepaid.PSRequestHandler
				.getPrepaidAccounts(PrepaidAxisServlet.SOAP_PROTOCOL,
						getPrepaidAccounts.getUsername(),
						getPrepaidAccounts.getPassword());
		com.usatech.ps.xsd.PSPrepaidAccounts webResponse = new com.usatech.ps.xsd.PSPrepaidAccounts();
		webResponse.setReturnCode(response.getReturnCode());
		webResponse.setReturnMessage(response.getReturnMessage());
		int count = response.getPrepaidAccounts()==null?0:response.getPrepaidAccounts().length;
		com.usatech.ps.xsd.PSPrepaidAccount[] prepaidAccounts = new com.usatech.ps.xsd.PSPrepaidAccount[count];
		try {
			for (int i = 0; i < count; i++) {
				com.usatech.ps.xsd.PSPrepaidAccount prepaidAccount = new com.usatech.ps.xsd.PSPrepaidAccount();
				ReflectionUtils.copyProperties(prepaidAccount,
						response.getPrepaidAccounts()[i]);
				prepaidAccounts[i] = prepaidAccount;
			}
		} catch (Exception e) {
			log.error("Error processing getPrepaidAccounts", e);
		}
		webResponse.setPrepaidAccounts(prepaidAccounts);
		com.usatech.ps.GetPrepaidAccountsResponse msgResponse = new com.usatech.ps.GetPrepaidAccountsResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param registerAnotherCard
	 * @return registerAnotherCardResponse
	 */

	public com.usatech.ps.RegisterAnotherCardResponse registerAnotherCard(
			com.usatech.ps.RegisterAnotherCard registerAnotherCard) {
		PSTokenResponse response = com.usatech.prepaid.PSRequestHandler
				.registerAnotherCard(PrepaidAxisServlet.SOAP_PROTOCOL,
						registerAnotherCard.getUsername(),
						registerAnotherCard.getPassword(),
						registerAnotherCard.getPrepaidCardNum(),
						registerAnotherCard.getPrepaidSecurityCode());
		com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.RegisterAnotherCardResponse msgResponse = new com.usatech.ps.RegisterAnotherCardResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param changePassword
	 * @return changePasswordResponse
	 */

	public com.usatech.ps.ChangePasswordResponse changePassword(
			com.usatech.ps.ChangePassword changePassword) {
		PSResponse response = com.usatech.prepaid.PSRequestHandler
				.changePassword(PrepaidAxisServlet.SOAP_PROTOCOL,
						changePassword.getUsername(),
						changePassword.getPassword(),
						changePassword.getNewPassword(),
						changePassword.getNewPasswordConfirm());
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.ChangePasswordResponse msgResponse = new com.usatech.ps.ChangePasswordResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getPrepaidTransactions
	 * @return getPrepaidTransactionsResponse
	 */

	public com.usatech.ps.GetPrepaidActivitiesResponse getPrepaidActivities(
			com.usatech.ps.GetPrepaidActivities getPrepaidActivities) {
		PSPrepaidActivities response = com.usatech.prepaid.PSRequestHandler
				.getPrepaidActivities(PrepaidAxisServlet.SOAP_PROTOCOL,
						getPrepaidActivities.getUsername(),
						getPrepaidActivities.getPassword(),
						getPrepaidActivities.getCardId(),
						getPrepaidActivities.getStartTime(),
						getPrepaidActivities.getEndTime(),
						getPrepaidActivities.getMaxRows());
		com.usatech.ps.xsd.PSPrepaidActivities webResponse = new com.usatech.ps.xsd.PSPrepaidActivities();
		webResponse.setReturnCode(response.getReturnCode());
		webResponse.setReturnMessage(response.getReturnMessage());
		int count = response.getPrepaidActivities()==null?0:response.getPrepaidActivities().length;
		com.usatech.ps.xsd.PSPrepaidActivity[] prepaidTransactions = new com.usatech.ps.xsd.PSPrepaidActivity[count];
		try {
			com.usatech.ps.PSPrepaidActivity[] responsePrepaidTransactions = response
					.getPrepaidActivities();
			for (int i = 0; i < count; i++) {
				com.usatech.ps.xsd.PSPrepaidActivity prepaidActivity = new com.usatech.ps.xsd.PSPrepaidActivity();
				ReflectionUtils.copyProperties(prepaidActivity,
						responsePrepaidTransactions[i]);
				prepaidTransactions[i] = prepaidActivity;
			}
		} catch (Exception e) {
			log.error("Error processing getPrepaidTransactions", e);
		}
		webResponse.setPrepaidActivities(prepaidTransactions);
		webResponse.setDiscountTotal(response.getDiscountTotal());
		webResponse.setPurchaseTotal(response.getPurchaseTotal());
		webResponse.setReplenishTotal(response.getReplenishTotal());
		com.usatech.ps.GetPrepaidActivitiesResponse msgResponse = new com.usatech.ps.GetPrepaidActivitiesResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param setupReplenish
	 * @return setupReplenishResponse
	 */

	public com.usatech.ps.SetupReplenishResponse setupReplenish(
			com.usatech.ps.SetupReplenish setupReplenish) {
		PSResponse response = com.usatech.prepaid.PSRequestHandler
				.setupReplenish(PrepaidAxisServlet.SOAP_PROTOCOL,
						setupReplenish.getUsername(),
						setupReplenish.getPassword(),
						setupReplenish.getCardId(),
						setupReplenish.getReplenishCardNum(),
						setupReplenish.getReplenishExpMonth(),
						setupReplenish.getReplenishExpYear(),
						setupReplenish.getReplenishSecurityCode(),
						setupReplenish.getAddress1(), setupReplenish.getCity(),
						setupReplenish.getState(), setupReplenish.getPostal(),
						setupReplenish.getCountry(),
						setupReplenish.getAmount(),
						setupReplenish.getReplenishType(),
						setupReplenish.getThreshhold());
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.SetupReplenishResponse msgResponse = new com.usatech.ps.SetupReplenishResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getReplenishInfo
	 * @return getReplenishInfoResponse
	 */

	public com.usatech.ps.GetReplenishInfoResponse getReplenishInfo(
			com.usatech.ps.GetReplenishInfo getReplenishInfo) {
		PSReplenishInfoArray response = com.usatech.prepaid.PSRequestHandler
				.getReplenishInfo(PrepaidAxisServlet.SOAP_PROTOCOL,
						getReplenishInfo.getUsername(),
						getReplenishInfo.getPassword(),
						getReplenishInfo.getCardId(),
						getReplenishInfo.getReplenishId());
		com.usatech.ps.xsd.PSReplenishInfoArray webResponse = new com.usatech.ps.xsd.PSReplenishInfoArray();
		webResponse.setReturnCode(response.getReturnCode());
		webResponse.setReturnMessage(response.getReturnMessage());
		int count = response.getReplenishInfoArray()==null?0:response.getReplenishInfoArray().length;
		com.usatech.ps.xsd.PSReplenishInfo[] replenishInfoArray = new com.usatech.ps.xsd.PSReplenishInfo[count];
		try {
			for (int i = 0; i < count; i++) {
				com.usatech.ps.xsd.PSReplenishInfo replenishInfo = new com.usatech.ps.xsd.PSReplenishInfo();
				ReflectionUtils.copyProperties(replenishInfo,
						response.getReplenishInfoArray()[i]);
				replenishInfoArray[i] = replenishInfo;
			}
		} catch (Exception e) {
			log.error("Error processing getPrepaidAccounts", e);
		}
		webResponse.setReplenishInfoArray(replenishInfoArray);
		com.usatech.ps.GetReplenishInfoResponse msgResponse = new com.usatech.ps.GetReplenishInfoResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param updateReplenish
	 * @return updateReplenishResponse
	 */

	public com.usatech.ps.UpdateReplenishResponse updateReplenish(
			com.usatech.ps.UpdateReplenish updateReplenish) {
		PSResponse response = com.usatech.prepaid.PSRequestHandler
				.updateReplenish(PrepaidAxisServlet.SOAP_PROTOCOL,
						updateReplenish.getUsername(),
						updateReplenish.getPassword(),
						updateReplenish.getCardId(),
						updateReplenish.getReplenishId(),
						updateReplenish.getAmount(),
						updateReplenish.getReplenishType(),
						updateReplenish.getThreshhold(),
						updateReplenish.getReplenCount());
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.UpdateReplenishResponse msgResponse = new com.usatech.ps.UpdateReplenishResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param requestReplenish
	 * @return requestReplenishResponse
	 */

	public com.usatech.ps.RequestReplenishResponse requestReplenish(
			com.usatech.ps.RequestReplenish requestReplenish) {
		PSResponse response = com.usatech.prepaid.PSRequestHandler
				.requestReplenish(PrepaidAxisServlet.SOAP_PROTOCOL,
						requestReplenish.getUsername(),
						requestReplenish.getPassword(),
						requestReplenish.getCardId(),
						requestReplenish.getReplenishId(),
						requestReplenish.getAmount());
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error(e);
		}
		com.usatech.ps.RequestReplenishResponse msgResponse = new com.usatech.ps.RequestReplenishResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getPromoCampaigns
	 * @return getPromoCampaignsResponse
	 */

	public com.usatech.ps.GetPromoCampaignsResponse getPromoCampaigns(
			com.usatech.ps.GetPromoCampaigns getPromoCampaigns) {
		PSPromoCampaigns response = com.usatech.prepaid.PSRequestHandler
				.getPromoCampaigns(PrepaidAxisServlet.SOAP_PROTOCOL,
						getPromoCampaigns.getUsername(),
						getPromoCampaigns.getPassword());
		com.usatech.ps.xsd.PSPromoCampaigns webResponse = new com.usatech.ps.xsd.PSPromoCampaigns();
		webResponse.setReturnCode(response.getReturnCode());
		webResponse.setReturnMessage(response.getReturnMessage());
		int count = response.getPromoCampaigns()==null?0:response.getPromoCampaigns().length;
		com.usatech.ps.xsd.PSPromoCampaign[] promoCampaigns = new com.usatech.ps.xsd.PSPromoCampaign[count];
		try {
			for (int i = 0; i < count; i++) {
				com.usatech.ps.xsd.PSPromoCampaign promoCampaign = new com.usatech.ps.xsd.PSPromoCampaign();
				ReflectionUtils.copyProperties(promoCampaign,
						response.getPromoCampaigns()[i]);
				promoCampaigns[i] = promoCampaign;
			}
		} catch (Exception e) {
			log.error("Error processing getPromoCampaigns", e);
		}
		webResponse.setPromoCampaigns(promoCampaigns);
		com.usatech.ps.GetPromoCampaignsResponse msgResponse = new com.usatech.ps.GetPromoCampaignsResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getLocations
	 * @return getLocationsResponse
	 */

	public com.usatech.ps.GetLocationsResponse getLocations(
			com.usatech.ps.GetLocations getLocations) {
		PSPromoCampaigns response = com.usatech.prepaid.PSRequestHandler
				.getLocations(PrepaidAxisServlet.SOAP_PROTOCOL,
						getLocations.getUsername(), getLocations.getPassword(),
						getLocations.getLongitude(), getLocations.getLatitude());
		com.usatech.ps.xsd.PSPromoCampaigns webResponse = new com.usatech.ps.xsd.PSPromoCampaigns();
		webResponse.setReturnCode(response.getReturnCode());
		webResponse.setReturnMessage(response.getReturnMessage());
		int count = response.getPromoCampaigns()==null?0:response.getPromoCampaigns().length;
		com.usatech.ps.xsd.PSPromoCampaign[] promoCampaigns = new com.usatech.ps.xsd.PSPromoCampaign[count];
		try {
			for (int i = 0; i < count; i++) {
				com.usatech.ps.xsd.PSPromoCampaign promoCampaign = new com.usatech.ps.xsd.PSPromoCampaign();
				ReflectionUtils.copyProperties(promoCampaign,
						response.getPromoCampaigns()[i]);
				promoCampaigns[i] = promoCampaign;
			}
		} catch (Exception e) {
			log.error("Error processing getLocations", e);
		}
		webResponse.setPromoCampaigns(promoCampaigns);
		com.usatech.ps.GetLocationsResponse msgResponse = new com.usatech.ps.GetLocationsResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param resetPassword
	 * @return resetPasswordResponse
	 */

	public com.usatech.ps.ResetPasswordResponse resetPassword(
			com.usatech.ps.ResetPassword resetPassword) {
		PSResponse response = com.usatech.prepaid.PSRequestHandler
				.resetPassword(PrepaidAxisServlet.SOAP_PROTOCOL,
						resetPassword.getUsername(),
						resetPassword.getPassword());
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.ResetPasswordResponse msgResponse = new com.usatech.ps.ResetPasswordResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getConsumerSettings
	 * @return getConsumerSettingsResponse
	 */

	public com.usatech.ps.GetConsumerSettingsResponse getConsumerSettings(
			com.usatech.ps.GetConsumerSettings getConsumerSettings) {
		PSConsumerSettings response = com.usatech.prepaid.PSRequestHandler
				.getConsumerSettings(PrepaidAxisServlet.SOAP_PROTOCOL,
						getConsumerSettings.getUsername(),
						getConsumerSettings.getPassword());
		com.usatech.ps.xsd.PSConsumerSettings webResponse = new com.usatech.ps.xsd.PSConsumerSettings();
		webResponse.setReturnCode(response.getReturnCode());
		webResponse.setReturnMessage(response.getReturnMessage());
		int count = response.getConsumerSetting()==null?0:response.getConsumerSetting().length;
		com.usatech.ps.xsd.PSConsumerSetting[] consumerSettings = new com.usatech.ps.xsd.PSConsumerSetting[count];
		try {
			for (int i = 0; i < count; i++) {
				com.usatech.ps.xsd.PSConsumerSetting consumerSetting = new com.usatech.ps.xsd.PSConsumerSetting();
				ReflectionUtils.copyProperties(consumerSetting,
						response.getConsumerSetting()[i]);
				consumerSettings[i] = consumerSetting;
			}
		} catch (Exception e) {
			log.error("Error processing getConsumerSettings", e);
		}
		webResponse.setConsumerSetting(consumerSettings);
		com.usatech.ps.GetConsumerSettingsResponse msgResponse = new com.usatech.ps.GetConsumerSettingsResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param updateConsumerSettings
	 * @return updateConsumerSettingsResponse
	 */

	public com.usatech.ps.UpdateConsumerSettingsResponse updateConsumerSettings(
			com.usatech.ps.UpdateConsumerSettings updateConsumerSettings) {
		PSConsumerSetting[] consumerSetting = null;
		com.usatech.ps.xsd.PSConsumerSetting[] settings = updateConsumerSettings
				.getConsumerSettings();
		if (settings != null) {
			consumerSetting = new PSConsumerSetting[settings.length];
			for (int i = 0; i < settings.length; i++) {
				consumerSetting[i] = new PSConsumerSetting(
						settings[i].getSettingId(), settings[i].getValue());
			}
		}
		PSResponse response = com.usatech.prepaid.PSRequestHandler
				.updateConsumerSettings(PrepaidAxisServlet.SOAP_PROTOCOL,
						updateConsumerSettings.getUsername(),
						updateConsumerSettings.getPassword(), consumerSetting);
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.UpdateConsumerSettingsResponse msgResponse = new com.usatech.ps.UpdateConsumerSettingsResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getPostalInfo
	 * @return getPostalInfoResponse
	 */

	public com.usatech.ps.GetPostalInfoResponse getPostalInfo(
			com.usatech.ps.GetPostalInfo getPostalInfo) {
		PSPostalInfo response = com.usatech.prepaid.PSRequestHandler
				.getPostalInfo(PrepaidAxisServlet.SOAP_PROTOCOL,
						getPostalInfo.getPostalCd(),
						getPostalInfo.getCountryCd());
		com.usatech.ps.xsd.PSPostalInfo webResponse = new com.usatech.ps.xsd.PSPostalInfo();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.GetPostalInfoResponse msgResponse = new com.usatech.ps.GetPostalInfoResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param getLocationsByUsage
	 * @return getLocationsByUsageResponse
	 */

	public com.usatech.ps.GetLocationsByUsageResponse getLocationsByUsage(
			com.usatech.ps.GetLocationsByUsage getLocationsByUsage) {
		PSPromoCampaigns response = com.usatech.prepaid.PSRequestHandler
				.getLocationsByUsage(PrepaidAxisServlet.SOAP_PROTOCOL,
						getLocationsByUsage.getUsername(),
						getLocationsByUsage.getPassword());
		com.usatech.ps.xsd.PSPromoCampaigns webResponse = new com.usatech.ps.xsd.PSPromoCampaigns();
		webResponse.setReturnCode(response.getReturnCode());
		webResponse.setReturnMessage(response.getReturnMessage());
		int count = response.getPromoCampaigns()==null?0:response.getPromoCampaigns().length;
		com.usatech.ps.xsd.PSPromoCampaign[] promoCampaigns = new com.usatech.ps.xsd.PSPromoCampaign[count];
		try {
			for (int i = 0; i < count; i++) {
				com.usatech.ps.xsd.PSPromoCampaign promoCampaign = new com.usatech.ps.xsd.PSPromoCampaign();
				ReflectionUtils.copyProperties(promoCampaign,
						response.getPromoCampaigns()[i]);
				promoCampaigns[i] = promoCampaign;
			}
		} catch (Exception e) {
			log.error("Error processing getLocationsByUsage", e);
		}
		webResponse.setPromoCampaigns(promoCampaigns);
		com.usatech.ps.GetLocationsByUsageResponse msgResponse = new com.usatech.ps.GetLocationsByUsageResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

        /**
     * Auto generated method signature
     * 
                                 * @param getReplenishments 
         * @return getReplenishmentsResponse 
     */
    
             public com.usatech.ps.GetReplenishmentsResponse getReplenishments
              (
              com.usatech.ps.GetReplenishments getReplenishments
              )
        {
            	 PSReplenishmentArray response = com.usatech.prepaid.PSRequestHandler
         				.getReplenishments(PrepaidAxisServlet.SOAP_PROTOCOL,
         						getReplenishments.getUsername(),
         						getReplenishments.getPassword(),
         						getReplenishments.getCardId(),
         						getReplenishments.getStartTime(),
         						getReplenishments.getEndTime(),
         						getReplenishments.getMaxRows());
         		com.usatech.ps.xsd.PSReplenishmentArray webResponse = new com.usatech.ps.xsd.PSReplenishmentArray();
         		webResponse.setReturnCode(response.getReturnCode());
         		webResponse.setReturnMessage(response.getReturnMessage());
         		int count = response.getReplenishmentArray()==null?0:response.getReplenishmentArray().length;
         		com.usatech.ps.xsd.PSReplenishment[] prepaidReplenishments = new com.usatech.ps.xsd.PSReplenishment[count];
         		try {
         			com.usatech.ps.PSReplenishment[] responsePrepaidReplenishments = response
         					.getReplenishmentArray();
         			for (int i = 0; i < count; i++) {
         				com.usatech.ps.xsd.PSReplenishment prepaidReplenishment = new com.usatech.ps.xsd.PSReplenishment();
         				ReflectionUtils.copyProperties(prepaidReplenishment,
         						responsePrepaidReplenishments[i]);
         				prepaidReplenishments[i] = prepaidReplenishment;
         			}
         		} catch (Exception e) {
         			log.error("Error processing getReplenishments", e);
         		}
         		webResponse.setReplenishmentArray(prepaidReplenishments);
         		com.usatech.ps.GetReplenishmentsResponse msgResponse = new com.usatech.ps.GetReplenishmentsResponse();
        		msgResponse.set_return(webResponse);
        		return msgResponse;
    }
        /**
         * Auto generated method signature
         * 
                                     * @param getChargedCards 
             * @return getChargedCardsResponse 
         */
        
                 public com.usatech.ps.GetChargedCardsResponse getChargedCards
                  (
                  com.usatech.ps.GetChargedCards getChargedCards
                  )
            {
                	 PSChargedCardArray response = com.usatech.prepaid.PSRequestHandler
             				.getChargedCards(PrepaidAxisServlet.SOAP_PROTOCOL,
             						getChargedCards.getUsername(),
             						getChargedCards.getPassword());
             		com.usatech.ps.xsd.PSChargedCardArray webResponse = new com.usatech.ps.xsd.PSChargedCardArray();
             		webResponse.setReturnCode(response.getReturnCode());
             		webResponse.setReturnMessage(response.getReturnMessage());
             		int count = response.getChargedCardArray()==null?0:response.getChargedCardArray().length;
             		com.usatech.ps.xsd.PSChargedCard[] chargedCardArray = new com.usatech.ps.xsd.PSChargedCard[count];
             		try {
             			for (int i = 0; i < count; i++) {
             				com.usatech.ps.xsd.PSChargedCard chargedCard = new com.usatech.ps.xsd.PSChargedCard();
             				ReflectionUtils.copyProperties(chargedCard,
             						response.getChargedCardArray()[i]);
             				chargedCardArray[i] = chargedCard;
             			}
             		} catch (Exception e) {
             			log.error("Error processing getPrepaidAccounts", e);
             		}
             		webResponse.setChargedCardArray(chargedCardArray);
             		com.usatech.ps.GetChargedCardsResponse msgResponse = new com.usatech.ps.GetChargedCardsResponse();
             		msgResponse.set_return(webResponse);
             		return msgResponse;
        }
        /**
         * Auto generated method signature
         * 
                                     * @param getAuthHolds 
             * @return getAuthHoldsResponse 
         */
        
                 public com.usatech.ps.GetAuthHoldsResponse getAuthHolds
                  (
                  com.usatech.ps.GetAuthHolds getAuthHolds
                  )
            {
                	 PSAuthHoldArray response = com.usatech.prepaid.PSRequestHandler
              				.getAuthHolds(PrepaidAxisServlet.SOAP_PROTOCOL,
              						getAuthHolds.getUsername(),
              						getAuthHolds.getPassword(),
              						getAuthHolds.getCardId());
              		com.usatech.ps.xsd.PSAuthHoldArray webResponse = new com.usatech.ps.xsd.PSAuthHoldArray();
              		webResponse.setReturnCode(response.getReturnCode());
              		webResponse.setReturnMessage(response.getReturnMessage());
              		int count = response.getAuthHoldArray()==null?0:response.getAuthHoldArray().length;
              		com.usatech.ps.xsd.PSAuthHold[] authHoldArray = new com.usatech.ps.xsd.PSAuthHold[count];
              		try {
              			for (int i = 0; i < count; i++) {
              				com.usatech.ps.xsd.PSAuthHold authHold = new com.usatech.ps.xsd.PSAuthHold();
              				ReflectionUtils.copyProperties(authHold,
              						response.getAuthHoldArray()[i]);
              				authHoldArray[i] = authHold;
              			}
              		} catch (Exception e) {
              			log.error("Error processing getAuthHolds", e);
              		}
              		webResponse.setAuthHoldArray(authHoldArray);
              		com.usatech.ps.GetAuthHoldsResponse msgResponse = new com.usatech.ps.GetAuthHoldsResponse();
              		msgResponse.set_return(webResponse);
              		return msgResponse;
        }
        /**
         * Auto generated method signature
         * 
                                     * @param getLocationsByLocation 
             * @return getLocationsByLocationResponse 
         */
        
                 public com.usatech.ps.GetLocationsByLocationResponse getLocationsByLocation
                  (
                  com.usatech.ps.GetLocationsByLocation getLocationsByLocation
                  )
            {
                	 PSPromoCampaigns response = com.usatech.prepaid.PSRequestHandler
             				.getLocationsByLocation(PrepaidAxisServlet.SOAP_PROTOCOL,
             						getLocationsByLocation.getUsername(),
             						getLocationsByLocation.getPassword(),
             						getLocationsByLocation.getPostal(),
             						getLocationsByLocation.getCountry(),
             						getLocationsByLocation.getMaxProximityMiles(),
             						getLocationsByLocation.getDiscountsOnly());
             		com.usatech.ps.xsd.PSPromoCampaigns webResponse = new com.usatech.ps.xsd.PSPromoCampaigns();
             		webResponse.setReturnCode(response.getReturnCode());
             		webResponse.setReturnMessage(response.getReturnMessage());
             		int count = response.getPromoCampaigns()==null?0:response.getPromoCampaigns().length;
             		com.usatech.ps.xsd.PSPromoCampaign[] promoCampaigns = new com.usatech.ps.xsd.PSPromoCampaign[count];
             		try {
             			for (int i = 0; i < count; i++) {
             				com.usatech.ps.xsd.PSPromoCampaign promoCampaign = new com.usatech.ps.xsd.PSPromoCampaign();
             				ReflectionUtils.copyProperties(promoCampaign,
             						response.getPromoCampaigns()[i]);
             				promoCampaigns[i] = promoCampaign;
             			}
             		} catch (Exception e) {
             			log.error("Error processing getLocationsByUsage", e);
             		}
             		webResponse.setPromoCampaigns(promoCampaigns);
             		com.usatech.ps.GetLocationsByLocationResponse msgResponse = new com.usatech.ps.GetLocationsByLocationResponse();
             		msgResponse.set_return(webResponse);
             		return msgResponse;
        }
		
		        /**
         * Auto generated method signature
         * 
                                     * @param retokenize 
             * @return retokenizeResponse 
         */
        
                 public com.usatech.ps.RetokenizeResponse retokenize
                  (
                  com.usatech.ps.Retokenize retokenize
                  )
            {
                	 PSTokenResponse  response=PSRequestHandler.retokenize(PrepaidAxisServlet.SOAP_PROTOCOL, 
                			 retokenize.getUsername(), retokenize.getPassword(), retokenize.getCardId(), 
                			 retokenize.getSecurityCode(), retokenize.getAttributes());
                	 com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
             		try {
             			ReflectionUtils.copyProperties(webResponse, response);
             		} catch (Exception e) {
             			log.error("", e);
             		}
             		com.usatech.ps.RetokenizeResponse msgResponse = new com.usatech.ps.RetokenizeResponse();
             		msgResponse.set_return(webResponse);
             		return msgResponse;
        }
		
	/**
	 * Auto generated method signature
	 * 
	 * @param verifyUser
	 * @return verifyUserResponse
	 */

	public com.usatech.ps.VerifyUserResponse verifyUser(
			com.usatech.ps.VerifyUser verifyUser) {
		PSResponse response = com.usatech.prepaid.PSRequestHandler
				.verifyUser(PrepaidAxisServlet.SOAP_PROTOCOL,
						verifyUser.getUsername(),
						verifyUser.getPassword(), verifyUser.getPasscode());
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.VerifyUserResponse msgResponse = new com.usatech.ps.VerifyUserResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}
	
	/**
	 * Auto generated method signature
	 * 
	 * @param getRegions
	 * @return getRegionsResponse
	 */

	public com.usatech.ps.GetRegionsResponse getRegions(
			com.usatech.ps.GetRegions getRegions) {
		PSRegions response = com.usatech.prepaid.PSRequestHandler
				.getRegions(PrepaidAxisServlet.SOAP_PROTOCOL,
						getRegions.getUsername(),
						getRegions.getPassword());
		com.usatech.ps.xsd.PSRegions webResponse = new com.usatech.ps.xsd.PSRegions();
		webResponse.setReturnCode(response.getReturnCode());
		webResponse.setReturnMessage(response.getReturnMessage());
		int count = response.getRegions()==null?0:response.getRegions().length;
		com.usatech.ps.xsd.PSRegion[] regionArray = new com.usatech.ps.xsd.PSRegion[count];
		try {
			for (int i = 0; i < count; i++) {
				com.usatech.ps.xsd.PSRegion region = new com.usatech.ps.xsd.PSRegion();
				ReflectionUtils.copyProperties(region,
						response.getRegions()[i]);
				regionArray[i] = region;
			}
		} catch (Exception e) {
			log.error("Error processing getRegions", e);
		}
		webResponse.setRegions(regionArray);
		com.usatech.ps.GetRegionsResponse msgResponse = new com.usatech.ps.GetRegionsResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param updateRegion
	 * @return updateRegionResponse
	 */

	public com.usatech.ps.UpdateRegionResponse updateRegion(
			com.usatech.ps.UpdateRegion updateRegion) {
		PSResponse response = com.usatech.prepaid.PSRequestHandler
				.updateRegion(PrepaidAxisServlet.SOAP_PROTOCOL,
						updateRegion.getUsername(),
						updateRegion.getPassword(),
						updateRegion.getRegionId());
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.UpdateRegionResponse msgResponse = new com.usatech.ps.UpdateRegionResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}	
	
	/**
	 * Auto generated method signature
	 * 
	 * @param addPromoCode
	 * @return addPromoCodeResponse
	 */

	public com.usatech.ps.AddPromoCodeResponse addPromoCode(
			com.usatech.ps.AddPromoCode addPromoCode) {
		PSResponse response = com.usatech.prepaid.PSRequestHandler
				.addPromoCode(PrepaidAxisServlet.SOAP_PROTOCOL,
						addPromoCode.getUsername(), addPromoCode.getPassword(),
						addPromoCode.getPromoCode());
		com.usatech.ps.xsd.PSResponse webResponse = new com.usatech.ps.xsd.PSResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.AddPromoCodeResponse msgResponse = new com.usatech.ps.AddPromoCodeResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}
	
	/**
	 * Auto generated method signature
	 * 
	 * @param createUserEncrypted
	 * @return createUserEncryptedResponse
	 */

	public com.usatech.ps.CreateUserEncryptedResponse createUserEncrypted(
			com.usatech.ps.CreateUserEncrypted createUserEncrypted) {
		PSConsumerSetting[] consumerSetting = null;
 		com.usatech.ps.xsd.PSConsumerSetting[] settings = createUserEncrypted
 				.getConsumerSettings();
 		if (settings != null) {
 			consumerSetting = new PSConsumerSetting[settings.length];
 			for (int i = 0; i < settings.length; i++) {
 				consumerSetting[i] = new PSConsumerSetting(
 						settings[i].getSettingId(), settings[i].getValue());
 			}
 		}
 		PSTokenResponse response = com.usatech.prepaid.PSRequestHandler.createUserEncrypted(
 				PrepaidAxisServlet.SOAP_PROTOCOL,
 				createUserEncrypted.getEntryType(),
 				createUserEncrypted.getCardReaderType(),
 				createUserEncrypted.getDecryptedCardDataLen(), 
 				createUserEncrypted.getEncryptedCardDataHex(),
 				createUserEncrypted.getKsnHex(),
 				createUserEncrypted.getBillingCountry(),
 				createUserEncrypted.getBillingAddress(),
 				createUserEncrypted.getBillingPostalCode(),
 				createUserEncrypted.getPreferredCommType(),
 				createUserEncrypted.getEmail(),createUserEncrypted.getMobile(),createUserEncrypted.getCarrierId(),
 				createUserEncrypted.getFirstname(), createUserEncrypted.getLastname(),
 				createUserEncrypted.getAddress1(), createUserEncrypted.getCity(),
 				createUserEncrypted.getState(), createUserEncrypted.getPostal(),
 				createUserEncrypted.getCountry(), createUserEncrypted.getPassword(),
 				createUserEncrypted.getConfirmPassword(), 
 				consumerSetting,
 				createUserEncrypted.getPromoCode());
 		com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
 		try {
 			ReflectionUtils.copyProperties(webResponse, response);
 		} catch (Exception e) {
 			log.error("", e);
 		}
 		com.usatech.ps.CreateUserEncryptedResponse msgResponse = new com.usatech.ps.CreateUserEncryptedResponse();
 		msgResponse.set_return(webResponse);
 		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param registerAnotherCardEncrypted
	 * @return registerAnotherCardEncryptedResponse
	 */

	public com.usatech.ps.RegisterAnotherCardEncryptedResponse registerAnotherCardEncrypted(
			com.usatech.ps.RegisterAnotherCardEncrypted registerAnotherCardEncrypted) {
		PSTokenResponse response = com.usatech.prepaid.PSRequestHandler
				.registerAnotherCardEncrypted(PrepaidAxisServlet.SOAP_PROTOCOL,
						registerAnotherCardEncrypted.getUsername(),
						registerAnotherCardEncrypted.getPassword(),
						registerAnotherCardEncrypted.getEntryType(),
						registerAnotherCardEncrypted.getCardReaderType(),
						registerAnotherCardEncrypted.getDecryptedCardDataLen(),
						registerAnotherCardEncrypted.getEncryptedCardDataHex(),
						registerAnotherCardEncrypted.getKsnHex(),
						registerAnotherCardEncrypted.getBillingCountry(),
						registerAnotherCardEncrypted.getBillingAddress(),
						registerAnotherCardEncrypted.getBillingPostalCode()
						);
		com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.RegisterAnotherCardEncryptedResponse msgResponse = new com.usatech.ps.RegisterAnotherCardEncryptedResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param createUserKeyed
	 * @return createUserKeyedResponse
	 */

	public com.usatech.ps.CreateUserKeyedResponse createUserKeyed(
			com.usatech.ps.CreateUserKeyed createUserKeyed) {
		PSConsumerSetting[] consumerSetting = null;
 		com.usatech.ps.xsd.PSConsumerSetting[] settings = createUserKeyed
 				.getConsumerSettings();
 		if (settings != null) {
 			consumerSetting = new PSConsumerSetting[settings.length];
 			for (int i = 0; i < settings.length; i++) {
 				consumerSetting[i] = new PSConsumerSetting(
 						settings[i].getSettingId(), settings[i].getValue());
 			}
 		}
 		PSTokenResponse response = com.usatech.prepaid.PSRequestHandler.createUserKeyed(
 				PrepaidAxisServlet.SOAP_PROTOCOL,
 				createUserKeyed.getCardNum(),
 				createUserKeyed.getSecurityCode(), 
 				createUserKeyed.getExpMonth(),
 				createUserKeyed.getExpYear(),
 				createUserKeyed.getBillingCountry(),
 				createUserKeyed.getBillingAddress(),
 				createUserKeyed.getBillingPostalCode(),
 				createUserKeyed.getPreferredCommType(),
 				createUserKeyed.getEmail(),createUserKeyed.getMobile(),createUserKeyed.getCarrierId(),
 				createUserKeyed.getFirstname(), createUserKeyed.getLastname(),
 				createUserKeyed.getAddress1(), createUserKeyed.getCity(),
 				createUserKeyed.getState(), createUserKeyed.getPostal(),
 				createUserKeyed.getCountry(), createUserKeyed.getPassword(),
 				createUserKeyed.getConfirmPassword(), 
 				consumerSetting,
 				createUserKeyed.getPromoCode());
 		com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
 		try {
 			ReflectionUtils.copyProperties(webResponse, response);
 		} catch (Exception e) {
 			log.error("", e);
 		}
 		com.usatech.ps.CreateUserKeyedResponse msgResponse = new com.usatech.ps.CreateUserKeyedResponse();
 		msgResponse.set_return(webResponse);
 		return msgResponse;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param registerAnotherCardKeyed
	 * @return registerAnotherCardKeyedResponse
	 */

	public com.usatech.ps.RegisterAnotherCardKeyedResponse registerAnotherCardKeyed(
			com.usatech.ps.RegisterAnotherCardKeyed registerAnotherCardKeyed) {
		PSTokenResponse response = com.usatech.prepaid.PSRequestHandler
				.registerAnotherCardKeyed(PrepaidAxisServlet.SOAP_PROTOCOL,
						registerAnotherCardKeyed.getUsername(),
						registerAnotherCardKeyed.getPassword(),
						registerAnotherCardKeyed.getCardNum(),
						registerAnotherCardKeyed.getSecurityCode(),
						registerAnotherCardKeyed.getExpMonth(),
						registerAnotherCardKeyed.getExpYear(),
						registerAnotherCardKeyed.getBillingCountry(),
						registerAnotherCardKeyed.getBillingAddress(),
						registerAnotherCardKeyed.getBillingPostalCode()
						);
		com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
		try {
			ReflectionUtils.copyProperties(webResponse, response);
		} catch (Exception e) {
			log.error("", e);
		}
		com.usatech.ps.RegisterAnotherCardKeyedResponse msgResponse = new com.usatech.ps.RegisterAnotherCardKeyedResponse();
		msgResponse.set_return(webResponse);
		return msgResponse;
	}        
	
    /**
     * Auto generated method signature
     * 
                                 * @param registerAnotherCreditCardEncrypted 
         * @return registerAnotherCreditCardEncryptedResponse 
     */
    
             public com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse registerAnotherCreditCardEncrypted
              (
              com.usatech.ps.RegisterAnotherCreditCardEncrypted registerAnotherCreditCardEncrypted
              )
        {
            	 PSTokenResponse response = com.usatech.prepaid.PSRequestHandler
         				.registerAnotherCardEncrypted(PrepaidAxisServlet.SOAP_PROTOCOL,
         						registerAnotherCreditCardEncrypted.getUsername(),
         						registerAnotherCreditCardEncrypted.getPassword(),
         						registerAnotherCreditCardEncrypted.getEntryType(),
         						registerAnotherCreditCardEncrypted.getCardReaderType(),
         						registerAnotherCreditCardEncrypted.getDecryptedCardDataLen(),
         						registerAnotherCreditCardEncrypted.getEncryptedCardDataHex(),
         						registerAnotherCreditCardEncrypted.getKsnHex(),
         						registerAnotherCreditCardEncrypted.getBillingCountry(),
         						registerAnotherCreditCardEncrypted.getBillingAddress(),
         						registerAnotherCreditCardEncrypted.getBillingPostalCode(),
         						registerAnotherCreditCardEncrypted.getPromoCode()
         						);
         		com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
         		try {
         			ReflectionUtils.copyProperties(webResponse, response);
         		} catch (Exception e) {
         			log.error("", e);
         		}
         		com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse msgResponse = new com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse();
         		msgResponse.set_return(webResponse);
         		return msgResponse;
    }

    /**
     * Auto generated method signature
     * 
                                 * @param registerAnotherCreditCardKeyed 
         * @return registerAnotherCreditCardKeyedResponse 
     */
    
             public com.usatech.ps.RegisterAnotherCreditCardKeyedResponse registerAnotherCreditCardKeyed
              (
              com.usatech.ps.RegisterAnotherCreditCardKeyed registerAnotherCreditCardKeyed
              )
        {
            	 PSTokenResponse response = com.usatech.prepaid.PSRequestHandler
         				.registerAnotherCardKeyed(PrepaidAxisServlet.SOAP_PROTOCOL,
         						registerAnotherCreditCardKeyed.getUsername(),
         						registerAnotherCreditCardKeyed.getPassword(),
         						registerAnotherCreditCardKeyed.getCardNum(),
         						registerAnotherCreditCardKeyed.getSecurityCode(),
         						registerAnotherCreditCardKeyed.getExpMonth(),
         						registerAnotherCreditCardKeyed.getExpYear(),
         						registerAnotherCreditCardKeyed.getBillingCountry(),
         						registerAnotherCreditCardKeyed.getBillingAddress(),
         						registerAnotherCreditCardKeyed.getBillingPostalCode(),
         						registerAnotherCreditCardKeyed.getPromoCode()
         						);
         		com.usatech.ps.xsd.PSTokenResponse webResponse = new com.usatech.ps.xsd.PSTokenResponse();
         		try {
         			ReflectionUtils.copyProperties(webResponse, response);
         		} catch (Exception e) {
         			log.error("", e);
         		}
         		com.usatech.ps.RegisterAnotherCreditCardKeyedResponse msgResponse = new com.usatech.ps.RegisterAnotherCreditCardKeyedResponse();
         		msgResponse.set_return(webResponse);
         		return msgResponse;
    }
}
