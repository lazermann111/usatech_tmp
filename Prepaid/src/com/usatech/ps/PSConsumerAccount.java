package com.usatech.ps;

import java.io.Serializable;
import java.math.BigDecimal;

public class PSConsumerAccount extends PSResponse implements Serializable {
	private static final long serialVersionUID = -1360584297687480522L;
	
	protected long cardId;
	protected String cardNum;
	protected BigDecimal balance;
	protected String currencyCd;
	
	public long getCardId() {
		return cardId;
	}
	public void setCardId(long cardId) {
		this.cardId = cardId;
	}
	public String getCardNum() {
		return cardNum;
	}
	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
}
