package com.usatech.ps;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PSPromoCampaign extends PSResponse implements Serializable {
	private static final long serialVersionUID=-642819649371142122L;
    protected int campaignPriority;
    protected int locationPriority;
    protected Date  locationLastUsedDate;
    protected int locationUsedCount;
    protected int locationProximityMiles;       
    protected String deviceSerialCd;
    protected String locationDescription;
    protected String locationName;
    protected String locationAddress1;
    protected String locationAddress2;
    protected String locationCity;
    protected String locationState;
    protected String locationPostal;
    protected String locationCountry;
    protected String locationProductType;
    protected String campaignName;
    protected String campaignDescription;
    protected Date campaignStartDate;
    protected Date campaignEndDate;
    protected BigDecimal campaignDiscountPercent;
    protected String campaignType;
	protected long campaignId; 
    protected int campaignTypeId; 
    protected String campaignRecurSchedule;
    protected BigDecimal campaignThreshold;
    protected String attentionType;
    protected long cardId;
    protected String cardNum;
    protected String details;// this is constructed dynamically
    protected String title;// this is constructed dynamically
    protected String promoCode;
    
    
    public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getLocationCountry() {
		return locationCountry;
	}
	public void setLocationCountry(String locationCountry) {
		this.locationCountry = locationCountry;
	}
	public int getCampaignTypeId() {
		return campaignTypeId;
	}
	public void setCampaignTypeId(int campaignTypeId) {
		this.campaignTypeId = campaignTypeId;
	}
	public String getCampaignRecurSchedule() {
		return campaignRecurSchedule;
	}
	public void setCampaignRecurSchedule(String campaignRecurSchedule) {
		this.campaignRecurSchedule = campaignRecurSchedule;
	}
	public BigDecimal getCampaignThreshold() {
		return campaignThreshold;
	}
	public void setCampaignThreshold(BigDecimal campaignThreshold) {
		this.campaignThreshold = campaignThreshold;
	}
	public String getAttentionType() {
		return attentionType;
	}
	public void setAttentionType(String attentionType) {
		this.attentionType = attentionType;
	}
	public int getCampaignPriority() {
		return campaignPriority;
	}
	public void setCampaignPriority(int campaignPriority) {
		this.campaignPriority = campaignPriority;
	}
	public int getLocationPriority() {
		return locationPriority;
	}
	public void setLocationPriority(int locationPriority) {
		this.locationPriority = locationPriority;
	}
	public Date getLocationLastUsedDate() {
		return locationLastUsedDate;
	}
	public void setLocationLastUsedDate(Date locationLastUsedDate) {
		this.locationLastUsedDate = locationLastUsedDate;
	}
	public int getLocationUsedCount() {
		return locationUsedCount;
	}
	public void setLocationUsedCount(int locationUsedCount) {
		this.locationUsedCount = locationUsedCount;
	}
	public int getLocationProximityMiles() {
		return locationProximityMiles;
	}
	public void setLocationProximityMiles(int locationProximityMiles) {
		this.locationProximityMiles = locationProximityMiles;
	}
	public String getDeviceSerialCd() {
		return deviceSerialCd;
	}
	public void setDeviceSerialCd(String deviceSerialCd) {
		this.deviceSerialCd = deviceSerialCd;
	}
	public String getLocationDescription() {
		return locationDescription;
	}
	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getLocationAddress1() {
		return locationAddress1;
	}
	public void setLocationAddress1(String locationAddress1) {
		this.locationAddress1 = locationAddress1;
	}
	public String getLocationAddress2() {
		return locationAddress2;
	}
	public void setLocationAddress2(String locationAddress2) {
		this.locationAddress2 = locationAddress2;
	}
	public String getLocationCity() {
		return locationCity;
	}
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}
	public String getLocationState() {
		return locationState;
	}
	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}
	public String getLocationPostal() {
		return locationPostal;
	}
	public void setLocationPostal(String locationPostal) {
		this.locationPostal = locationPostal;
	}
	public String getLocationProductType() {
		return locationProductType;
	}
	public void setLocationProductType(String locationProductType) {
		this.locationProductType = locationProductType;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getCampaignDescription() {
		return campaignDescription;
	}
	public void setCampaignDescription(String campaignDescription) {
		this.campaignDescription = campaignDescription;
	}
	public Date getCampaignStartDate() {
		return campaignStartDate;
	}
	public void setCampaignStartDate(Date campaignStartDate) {
		this.campaignStartDate = campaignStartDate;
	}
	public Date getCampaignEndDate() {
		return campaignEndDate;
	}
	public void setCampaignEndDate(Date campaignEndDate) {
		this.campaignEndDate = campaignEndDate;
	}
	public BigDecimal getCampaignDiscountPercent() {
		return campaignDiscountPercent;
	}
	public void setCampaignDiscountPercent(BigDecimal campaignDiscountPercent) {
		this.campaignDiscountPercent = campaignDiscountPercent;
	}
	public String getCampaignType() {
		return campaignType;
	}
	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	public long getCardId() {
		return cardId;
	}
	public void setCardId(long cardId) {
		this.cardId = cardId;
	}
	public String getCardNum() {
		return cardNum;
	}
	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
    
}
