package com.usatech.ps;

import java.io.Serializable;

public class PSRegion extends PSResponse implements Serializable {
	private static final long serialVersionUID = -4737638740529580492L;
	protected int regionId;
	protected String regionName;
	public int getRegionId() {
		return regionId;
	}
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	
}
