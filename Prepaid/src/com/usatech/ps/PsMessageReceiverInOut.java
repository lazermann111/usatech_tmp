
/**
 * PsMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
        package com.usatech.ps;

        /**
        *  PsMessageReceiverInOut message receiver
        */

        public class PsMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        PsSkeleton skel = (PsSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("getLocations".equals(methodName)){
                
                com.usatech.ps.GetLocationsResponse getLocationsResponse141 = null;
	                        com.usatech.ps.GetLocations wrappedParam =
                                                             (com.usatech.ps.GetLocations)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetLocations.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getLocationsResponse141 =
                                                   
                                                   
                                                         skel.getLocations(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getLocationsResponse141, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getLocations"));
                                    } else 

            if("registerAnotherCreditCardEncrypted".equals(methodName)){
                
                com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse registerAnotherCreditCardEncryptedResponse143 = null;
	                        com.usatech.ps.RegisterAnotherCreditCardEncrypted wrappedParam =
                                                             (com.usatech.ps.RegisterAnotherCreditCardEncrypted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.RegisterAnotherCreditCardEncrypted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               registerAnotherCreditCardEncryptedResponse143 =
                                                   
                                                   
                                                         skel.registerAnotherCreditCardEncrypted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), registerAnotherCreditCardEncryptedResponse143, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "registerAnotherCreditCardEncrypted"));
                                    } else 

            if("createUser".equals(methodName)){
                
                com.usatech.ps.CreateUserResponse createUserResponse145 = null;
	                        com.usatech.ps.CreateUser wrappedParam =
                                                             (com.usatech.ps.CreateUser)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.CreateUser.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               createUserResponse145 =
                                                   
                                                   
                                                         skel.createUser(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), createUserResponse145, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "createUser"));
                                    } else 

            if("updateRegion".equals(methodName)){
                
                com.usatech.ps.UpdateRegionResponse updateRegionResponse147 = null;
	                        com.usatech.ps.UpdateRegion wrappedParam =
                                                             (com.usatech.ps.UpdateRegion)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.UpdateRegion.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateRegionResponse147 =
                                                   
                                                   
                                                         skel.updateRegion(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateRegionResponse147, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "updateRegion"));
                                    } else 

            if("createUserWithMobile".equals(methodName)){
                
                com.usatech.ps.CreateUserWithMobileResponse createUserWithMobileResponse149 = null;
	                        com.usatech.ps.CreateUserWithMobile wrappedParam =
                                                             (com.usatech.ps.CreateUserWithMobile)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.CreateUserWithMobile.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               createUserWithMobileResponse149 =
                                                   
                                                   
                                                         skel.createUserWithMobile(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), createUserWithMobileResponse149, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "createUserWithMobile"));
                                    } else 

            if("addPromoCode".equals(methodName)){
                
                com.usatech.ps.AddPromoCodeResponse addPromoCodeResponse151 = null;
	                        com.usatech.ps.AddPromoCode wrappedParam =
                                                             (com.usatech.ps.AddPromoCode)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.AddPromoCode.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               addPromoCodeResponse151 =
                                                   
                                                   
                                                         skel.addPromoCode(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), addPromoCodeResponse151, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "addPromoCode"));
                                    } else 

            if("getReplenishments".equals(methodName)){
                
                com.usatech.ps.GetReplenishmentsResponse getReplenishmentsResponse153 = null;
	                        com.usatech.ps.GetReplenishments wrappedParam =
                                                             (com.usatech.ps.GetReplenishments)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetReplenishments.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getReplenishmentsResponse153 =
                                                   
                                                   
                                                         skel.getReplenishments(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getReplenishmentsResponse153, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getReplenishments"));
                                    } else 

            if("getPrepaidAccounts".equals(methodName)){
                
                com.usatech.ps.GetPrepaidAccountsResponse getPrepaidAccountsResponse155 = null;
	                        com.usatech.ps.GetPrepaidAccounts wrappedParam =
                                                             (com.usatech.ps.GetPrepaidAccounts)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetPrepaidAccounts.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getPrepaidAccountsResponse155 =
                                                   
                                                   
                                                         skel.getPrepaidAccounts(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getPrepaidAccountsResponse155, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getPrepaidAccounts"));
                                    } else 

            if("updateUser".equals(methodName)){
                
                com.usatech.ps.UpdateUserResponse updateUserResponse157 = null;
	                        com.usatech.ps.UpdateUser wrappedParam =
                                                             (com.usatech.ps.UpdateUser)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.UpdateUser.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateUserResponse157 =
                                                   
                                                   
                                                         skel.updateUser(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateUserResponse157, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "updateUser"));
                                    } else 

            if("getPromoCampaigns".equals(methodName)){
                
                com.usatech.ps.GetPromoCampaignsResponse getPromoCampaignsResponse159 = null;
	                        com.usatech.ps.GetPromoCampaigns wrappedParam =
                                                             (com.usatech.ps.GetPromoCampaigns)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetPromoCampaigns.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getPromoCampaignsResponse159 =
                                                   
                                                   
                                                         skel.getPromoCampaigns(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getPromoCampaignsResponse159, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getPromoCampaigns"));
                                    } else 

            if("updateConsumerSettings".equals(methodName)){
                
                com.usatech.ps.UpdateConsumerSettingsResponse updateConsumerSettingsResponse161 = null;
	                        com.usatech.ps.UpdateConsumerSettings wrappedParam =
                                                             (com.usatech.ps.UpdateConsumerSettings)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.UpdateConsumerSettings.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateConsumerSettingsResponse161 =
                                                   
                                                   
                                                         skel.updateConsumerSettings(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateConsumerSettingsResponse161, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "updateConsumerSettings"));
                                    } else 

            if("registerAnotherCardEncrypted".equals(methodName)){
                
                com.usatech.ps.RegisterAnotherCardEncryptedResponse registerAnotherCardEncryptedResponse163 = null;
	                        com.usatech.ps.RegisterAnotherCardEncrypted wrappedParam =
                                                             (com.usatech.ps.RegisterAnotherCardEncrypted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.RegisterAnotherCardEncrypted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               registerAnotherCardEncryptedResponse163 =
                                                   
                                                   
                                                         skel.registerAnotherCardEncrypted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), registerAnotherCardEncryptedResponse163, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "registerAnotherCardEncrypted"));
                                    } else 

            if("setupReplenish".equals(methodName)){
                
                com.usatech.ps.SetupReplenishResponse setupReplenishResponse165 = null;
	                        com.usatech.ps.SetupReplenish wrappedParam =
                                                             (com.usatech.ps.SetupReplenish)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.SetupReplenish.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               setupReplenishResponse165 =
                                                   
                                                   
                                                         skel.setupReplenish(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), setupReplenishResponse165, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "setupReplenish"));
                                    } else 

            if("createUserEncrypted".equals(methodName)){
                
                com.usatech.ps.CreateUserEncryptedResponse createUserEncryptedResponse167 = null;
	                        com.usatech.ps.CreateUserEncrypted wrappedParam =
                                                             (com.usatech.ps.CreateUserEncrypted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.CreateUserEncrypted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               createUserEncryptedResponse167 =
                                                   
                                                   
                                                         skel.createUserEncrypted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), createUserEncryptedResponse167, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "createUserEncrypted"));
                                    } else 

            if("getAuthHolds".equals(methodName)){
                
                com.usatech.ps.GetAuthHoldsResponse getAuthHoldsResponse169 = null;
	                        com.usatech.ps.GetAuthHolds wrappedParam =
                                                             (com.usatech.ps.GetAuthHolds)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetAuthHolds.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getAuthHoldsResponse169 =
                                                   
                                                   
                                                         skel.getAuthHolds(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getAuthHoldsResponse169, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getAuthHolds"));
                                    } else 

            if("getPostalInfo".equals(methodName)){
                
                com.usatech.ps.GetPostalInfoResponse getPostalInfoResponse171 = null;
	                        com.usatech.ps.GetPostalInfo wrappedParam =
                                                             (com.usatech.ps.GetPostalInfo)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetPostalInfo.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getPostalInfoResponse171 =
                                                   
                                                   
                                                         skel.getPostalInfo(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getPostalInfoResponse171, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getPostalInfo"));
                                    } else 

            if("changePassword".equals(methodName)){
                
                com.usatech.ps.ChangePasswordResponse changePasswordResponse173 = null;
	                        com.usatech.ps.ChangePassword wrappedParam =
                                                             (com.usatech.ps.ChangePassword)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.ChangePassword.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               changePasswordResponse173 =
                                                   
                                                   
                                                         skel.changePassword(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), changePasswordResponse173, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "changePassword"));
                                    } else 

            if("updateReplenish".equals(methodName)){
                
                com.usatech.ps.UpdateReplenishResponse updateReplenishResponse175 = null;
	                        com.usatech.ps.UpdateReplenish wrappedParam =
                                                             (com.usatech.ps.UpdateReplenish)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.UpdateReplenish.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateReplenishResponse175 =
                                                   
                                                   
                                                         skel.updateReplenish(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateReplenishResponse175, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "updateReplenish"));
                                    } else 

            if("updateUserWithMobile".equals(methodName)){
                
                com.usatech.ps.UpdateUserWithMobileResponse updateUserWithMobileResponse177 = null;
	                        com.usatech.ps.UpdateUserWithMobile wrappedParam =
                                                             (com.usatech.ps.UpdateUserWithMobile)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.UpdateUserWithMobile.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateUserWithMobileResponse177 =
                                                   
                                                   
                                                         skel.updateUserWithMobile(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateUserWithMobileResponse177, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "updateUserWithMobile"));
                                    } else 

            if("requestReplenish".equals(methodName)){
                
                com.usatech.ps.RequestReplenishResponse requestReplenishResponse179 = null;
	                        com.usatech.ps.RequestReplenish wrappedParam =
                                                             (com.usatech.ps.RequestReplenish)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.RequestReplenish.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               requestReplenishResponse179 =
                                                   
                                                   
                                                         skel.requestReplenish(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), requestReplenishResponse179, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "requestReplenish"));
                                    } else 

            if("getLocationsByUsage".equals(methodName)){
                
                com.usatech.ps.GetLocationsByUsageResponse getLocationsByUsageResponse181 = null;
	                        com.usatech.ps.GetLocationsByUsage wrappedParam =
                                                             (com.usatech.ps.GetLocationsByUsage)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetLocationsByUsage.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getLocationsByUsageResponse181 =
                                                   
                                                   
                                                         skel.getLocationsByUsage(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getLocationsByUsageResponse181, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getLocationsByUsage"));
                                    } else 

            if("registerAnotherCardKeyed".equals(methodName)){
                
                com.usatech.ps.RegisterAnotherCardKeyedResponse registerAnotherCardKeyedResponse183 = null;
	                        com.usatech.ps.RegisterAnotherCardKeyed wrappedParam =
                                                             (com.usatech.ps.RegisterAnotherCardKeyed)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.RegisterAnotherCardKeyed.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               registerAnotherCardKeyedResponse183 =
                                                   
                                                   
                                                         skel.registerAnotherCardKeyed(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), registerAnotherCardKeyedResponse183, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "registerAnotherCardKeyed"));
                                    } else 

            if("getReplenishInfo".equals(methodName)){
                
                com.usatech.ps.GetReplenishInfoResponse getReplenishInfoResponse185 = null;
	                        com.usatech.ps.GetReplenishInfo wrappedParam =
                                                             (com.usatech.ps.GetReplenishInfo)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetReplenishInfo.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getReplenishInfoResponse185 =
                                                   
                                                   
                                                         skel.getReplenishInfo(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getReplenishInfoResponse185, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getReplenishInfo"));
                                    } else 

            if("createUserKeyed".equals(methodName)){
                
                com.usatech.ps.CreateUserKeyedResponse createUserKeyedResponse187 = null;
	                        com.usatech.ps.CreateUserKeyed wrappedParam =
                                                             (com.usatech.ps.CreateUserKeyed)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.CreateUserKeyed.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               createUserKeyedResponse187 =
                                                   
                                                   
                                                         skel.createUserKeyed(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), createUserKeyedResponse187, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "createUserKeyed"));
                                    } else 

            if("getPrepaidActivities".equals(methodName)){
                
                com.usatech.ps.GetPrepaidActivitiesResponse getPrepaidActivitiesResponse189 = null;
	                        com.usatech.ps.GetPrepaidActivities wrappedParam =
                                                             (com.usatech.ps.GetPrepaidActivities)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetPrepaidActivities.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getPrepaidActivitiesResponse189 =
                                                   
                                                   
                                                         skel.getPrepaidActivities(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getPrepaidActivitiesResponse189, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getPrepaidActivities"));
                                    } else 

            if("verifyUser".equals(methodName)){
                
                com.usatech.ps.VerifyUserResponse verifyUserResponse191 = null;
	                        com.usatech.ps.VerifyUser wrappedParam =
                                                             (com.usatech.ps.VerifyUser)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.VerifyUser.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               verifyUserResponse191 =
                                                   
                                                   
                                                         skel.verifyUser(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), verifyUserResponse191, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "verifyUser"));
                                    } else 

            if("getLocationsByLocation".equals(methodName)){
                
                com.usatech.ps.GetLocationsByLocationResponse getLocationsByLocationResponse193 = null;
	                        com.usatech.ps.GetLocationsByLocation wrappedParam =
                                                             (com.usatech.ps.GetLocationsByLocation)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetLocationsByLocation.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getLocationsByLocationResponse193 =
                                                   
                                                   
                                                         skel.getLocationsByLocation(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getLocationsByLocationResponse193, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getLocationsByLocation"));
                                    } else 

            if("resetPassword".equals(methodName)){
                
                com.usatech.ps.ResetPasswordResponse resetPasswordResponse195 = null;
	                        com.usatech.ps.ResetPassword wrappedParam =
                                                             (com.usatech.ps.ResetPassword)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.ResetPassword.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               resetPasswordResponse195 =
                                                   
                                                   
                                                         skel.resetPassword(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), resetPasswordResponse195, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "resetPassword"));
                                    } else 

            if("getRegions".equals(methodName)){
                
                com.usatech.ps.GetRegionsResponse getRegionsResponse197 = null;
	                        com.usatech.ps.GetRegions wrappedParam =
                                                             (com.usatech.ps.GetRegions)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetRegions.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getRegionsResponse197 =
                                                   
                                                   
                                                         skel.getRegions(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getRegionsResponse197, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getRegions"));
                                    } else 

            if("registerAnotherCard".equals(methodName)){
                
                com.usatech.ps.RegisterAnotherCardResponse registerAnotherCardResponse199 = null;
	                        com.usatech.ps.RegisterAnotherCard wrappedParam =
                                                             (com.usatech.ps.RegisterAnotherCard)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.RegisterAnotherCard.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               registerAnotherCardResponse199 =
                                                   
                                                   
                                                         skel.registerAnotherCard(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), registerAnotherCardResponse199, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "registerAnotherCard"));
                                    } else 

            if("getConsumerSettings".equals(methodName)){
                
                com.usatech.ps.GetConsumerSettingsResponse getConsumerSettingsResponse201 = null;
	                        com.usatech.ps.GetConsumerSettings wrappedParam =
                                                             (com.usatech.ps.GetConsumerSettings)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetConsumerSettings.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getConsumerSettingsResponse201 =
                                                   
                                                   
                                                         skel.getConsumerSettings(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getConsumerSettingsResponse201, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getConsumerSettings"));
                                    } else 

            if("registerAnotherCreditCardKeyed".equals(methodName)){
                
                com.usatech.ps.RegisterAnotherCreditCardKeyedResponse registerAnotherCreditCardKeyedResponse203 = null;
	                        com.usatech.ps.RegisterAnotherCreditCardKeyed wrappedParam =
                                                             (com.usatech.ps.RegisterAnotherCreditCardKeyed)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.RegisterAnotherCreditCardKeyed.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               registerAnotherCreditCardKeyedResponse203 =
                                                   
                                                   
                                                         skel.registerAnotherCreditCardKeyed(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), registerAnotherCreditCardKeyedResponse203, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "registerAnotherCreditCardKeyed"));
                                    } else 

            if("getChargedCards".equals(methodName)){
                
                com.usatech.ps.GetChargedCardsResponse getChargedCardsResponse205 = null;
	                        com.usatech.ps.GetChargedCards wrappedParam =
                                                             (com.usatech.ps.GetChargedCards)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetChargedCards.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getChargedCardsResponse205 =
                                                   
                                                   
                                                         skel.getChargedCards(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getChargedCardsResponse205, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getChargedCards"));
                                    } else 

            if("getUser".equals(methodName)){
                
                com.usatech.ps.GetUserResponse getUserResponse207 = null;
	                        com.usatech.ps.GetUser wrappedParam =
                                                             (com.usatech.ps.GetUser)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.GetUser.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getUserResponse207 =
                                                   
                                                   
                                                         skel.getUser(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getUserResponse207, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "getUser"));
                                    } else 

            if("retokenize".equals(methodName)){
                
                com.usatech.ps.RetokenizeResponse retokenizeResponse209 = null;
	                        com.usatech.ps.Retokenize wrappedParam =
                                                             (com.usatech.ps.Retokenize)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.ps.Retokenize.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               retokenizeResponse209 =
                                                   
                                                   
                                                         skel.retokenize(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), retokenizeResponse209, false, new javax.xml.namespace.QName("urn:ps.usatech.com",
                                                    "retokenize"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetLocations param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetLocations.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetLocationsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetLocationsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCreditCardEncrypted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCreditCardEncrypted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.CreateUser param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.CreateUser.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.CreateUserResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.CreateUserResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateRegion param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateRegion.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateRegionResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateRegionResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.CreateUserWithMobile param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.CreateUserWithMobile.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.CreateUserWithMobileResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.CreateUserWithMobileResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.AddPromoCode param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.AddPromoCode.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.AddPromoCodeResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.AddPromoCodeResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetReplenishments param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetReplenishments.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetReplenishmentsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetReplenishmentsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetPrepaidAccounts param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetPrepaidAccounts.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetPrepaidAccountsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetPrepaidAccountsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateUser param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateUser.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateUserResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateUserResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetPromoCampaigns param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetPromoCampaigns.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetPromoCampaignsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetPromoCampaignsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateConsumerSettings param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateConsumerSettings.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateConsumerSettingsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateConsumerSettingsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCardEncrypted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCardEncrypted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCardEncryptedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCardEncryptedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.SetupReplenish param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.SetupReplenish.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.SetupReplenishResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.SetupReplenishResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.CreateUserEncrypted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.CreateUserEncrypted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.CreateUserEncryptedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.CreateUserEncryptedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetAuthHolds param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetAuthHolds.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetAuthHoldsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetAuthHoldsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetPostalInfo param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetPostalInfo.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetPostalInfoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetPostalInfoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.ChangePassword param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.ChangePassword.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.ChangePasswordResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.ChangePasswordResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateReplenish param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateReplenish.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateReplenishResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateReplenishResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateUserWithMobile param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateUserWithMobile.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.UpdateUserWithMobileResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.UpdateUserWithMobileResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RequestReplenish param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RequestReplenish.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RequestReplenishResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RequestReplenishResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetLocationsByUsage param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetLocationsByUsage.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetLocationsByUsageResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetLocationsByUsageResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCardKeyed param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCardKeyed.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCardKeyedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCardKeyedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetReplenishInfo param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetReplenishInfo.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetReplenishInfoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetReplenishInfoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.CreateUserKeyed param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.CreateUserKeyed.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.CreateUserKeyedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.CreateUserKeyedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetPrepaidActivities param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetPrepaidActivities.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetPrepaidActivitiesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetPrepaidActivitiesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.VerifyUser param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.VerifyUser.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.VerifyUserResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.VerifyUserResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetLocationsByLocation param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetLocationsByLocation.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetLocationsByLocationResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetLocationsByLocationResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.ResetPassword param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.ResetPassword.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.ResetPasswordResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.ResetPasswordResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetRegions param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetRegions.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetRegionsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetRegionsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCard param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCard.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCardResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCardResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetConsumerSettings param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetConsumerSettings.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetConsumerSettingsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetConsumerSettingsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCreditCardKeyed param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCreditCardKeyed.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RegisterAnotherCreditCardKeyedResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RegisterAnotherCreditCardKeyedResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetChargedCards param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetChargedCards.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetChargedCardsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetChargedCardsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetUser param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetUser.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.GetUserResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.GetUserResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.Retokenize param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.Retokenize.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.ps.RetokenizeResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.ps.RetokenizeResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetLocationsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetLocationsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetLocationsResponse wrapgetLocations(){
                                com.usatech.ps.GetLocationsResponse wrappedElement = new com.usatech.ps.GetLocationsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse wrapregisterAnotherCreditCardEncrypted(){
                                com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse wrappedElement = new com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.CreateUserResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.CreateUserResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.CreateUserResponse wrapcreateUser(){
                                com.usatech.ps.CreateUserResponse wrappedElement = new com.usatech.ps.CreateUserResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.UpdateRegionResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.UpdateRegionResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.UpdateRegionResponse wrapupdateRegion(){
                                com.usatech.ps.UpdateRegionResponse wrappedElement = new com.usatech.ps.UpdateRegionResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.CreateUserWithMobileResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.CreateUserWithMobileResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.CreateUserWithMobileResponse wrapcreateUserWithMobile(){
                                com.usatech.ps.CreateUserWithMobileResponse wrappedElement = new com.usatech.ps.CreateUserWithMobileResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.AddPromoCodeResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.AddPromoCodeResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.AddPromoCodeResponse wrapaddPromoCode(){
                                com.usatech.ps.AddPromoCodeResponse wrappedElement = new com.usatech.ps.AddPromoCodeResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetReplenishmentsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetReplenishmentsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetReplenishmentsResponse wrapgetReplenishments(){
                                com.usatech.ps.GetReplenishmentsResponse wrappedElement = new com.usatech.ps.GetReplenishmentsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetPrepaidAccountsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetPrepaidAccountsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetPrepaidAccountsResponse wrapgetPrepaidAccounts(){
                                com.usatech.ps.GetPrepaidAccountsResponse wrappedElement = new com.usatech.ps.GetPrepaidAccountsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.UpdateUserResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.UpdateUserResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.UpdateUserResponse wrapupdateUser(){
                                com.usatech.ps.UpdateUserResponse wrappedElement = new com.usatech.ps.UpdateUserResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetPromoCampaignsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetPromoCampaignsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetPromoCampaignsResponse wrapgetPromoCampaigns(){
                                com.usatech.ps.GetPromoCampaignsResponse wrappedElement = new com.usatech.ps.GetPromoCampaignsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.UpdateConsumerSettingsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.UpdateConsumerSettingsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.UpdateConsumerSettingsResponse wrapupdateConsumerSettings(){
                                com.usatech.ps.UpdateConsumerSettingsResponse wrappedElement = new com.usatech.ps.UpdateConsumerSettingsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.RegisterAnotherCardEncryptedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.RegisterAnotherCardEncryptedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.RegisterAnotherCardEncryptedResponse wrapregisterAnotherCardEncrypted(){
                                com.usatech.ps.RegisterAnotherCardEncryptedResponse wrappedElement = new com.usatech.ps.RegisterAnotherCardEncryptedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.SetupReplenishResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.SetupReplenishResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.SetupReplenishResponse wrapsetupReplenish(){
                                com.usatech.ps.SetupReplenishResponse wrappedElement = new com.usatech.ps.SetupReplenishResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.CreateUserEncryptedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.CreateUserEncryptedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.CreateUserEncryptedResponse wrapcreateUserEncrypted(){
                                com.usatech.ps.CreateUserEncryptedResponse wrappedElement = new com.usatech.ps.CreateUserEncryptedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetAuthHoldsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetAuthHoldsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetAuthHoldsResponse wrapgetAuthHolds(){
                                com.usatech.ps.GetAuthHoldsResponse wrappedElement = new com.usatech.ps.GetAuthHoldsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetPostalInfoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetPostalInfoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetPostalInfoResponse wrapgetPostalInfo(){
                                com.usatech.ps.GetPostalInfoResponse wrappedElement = new com.usatech.ps.GetPostalInfoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.ChangePasswordResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.ChangePasswordResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.ChangePasswordResponse wrapchangePassword(){
                                com.usatech.ps.ChangePasswordResponse wrappedElement = new com.usatech.ps.ChangePasswordResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.UpdateReplenishResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.UpdateReplenishResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.UpdateReplenishResponse wrapupdateReplenish(){
                                com.usatech.ps.UpdateReplenishResponse wrappedElement = new com.usatech.ps.UpdateReplenishResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.UpdateUserWithMobileResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.UpdateUserWithMobileResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.UpdateUserWithMobileResponse wrapupdateUserWithMobile(){
                                com.usatech.ps.UpdateUserWithMobileResponse wrappedElement = new com.usatech.ps.UpdateUserWithMobileResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.RequestReplenishResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.RequestReplenishResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.RequestReplenishResponse wraprequestReplenish(){
                                com.usatech.ps.RequestReplenishResponse wrappedElement = new com.usatech.ps.RequestReplenishResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetLocationsByUsageResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetLocationsByUsageResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetLocationsByUsageResponse wrapgetLocationsByUsage(){
                                com.usatech.ps.GetLocationsByUsageResponse wrappedElement = new com.usatech.ps.GetLocationsByUsageResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.RegisterAnotherCardKeyedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.RegisterAnotherCardKeyedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.RegisterAnotherCardKeyedResponse wrapregisterAnotherCardKeyed(){
                                com.usatech.ps.RegisterAnotherCardKeyedResponse wrappedElement = new com.usatech.ps.RegisterAnotherCardKeyedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetReplenishInfoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetReplenishInfoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetReplenishInfoResponse wrapgetReplenishInfo(){
                                com.usatech.ps.GetReplenishInfoResponse wrappedElement = new com.usatech.ps.GetReplenishInfoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.CreateUserKeyedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.CreateUserKeyedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.CreateUserKeyedResponse wrapcreateUserKeyed(){
                                com.usatech.ps.CreateUserKeyedResponse wrappedElement = new com.usatech.ps.CreateUserKeyedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetPrepaidActivitiesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetPrepaidActivitiesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetPrepaidActivitiesResponse wrapgetPrepaidActivities(){
                                com.usatech.ps.GetPrepaidActivitiesResponse wrappedElement = new com.usatech.ps.GetPrepaidActivitiesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetLocationsByLocationResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetLocationsByLocationResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetLocationsByLocationResponse wrapgetLocationsByLocation(){
                                com.usatech.ps.GetLocationsByLocationResponse wrappedElement = new com.usatech.ps.GetLocationsByLocationResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.VerifyUserResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.VerifyUserResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.VerifyUserResponse wrapverifyUser(){
                                com.usatech.ps.VerifyUserResponse wrappedElement = new com.usatech.ps.VerifyUserResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.ResetPasswordResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.ResetPasswordResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.ResetPasswordResponse wrapresetPassword(){
                                com.usatech.ps.ResetPasswordResponse wrappedElement = new com.usatech.ps.ResetPasswordResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetRegionsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetRegionsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetRegionsResponse wrapgetRegions(){
                                com.usatech.ps.GetRegionsResponse wrappedElement = new com.usatech.ps.GetRegionsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.RegisterAnotherCardResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.RegisterAnotherCardResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.RegisterAnotherCardResponse wrapregisterAnotherCard(){
                                com.usatech.ps.RegisterAnotherCardResponse wrappedElement = new com.usatech.ps.RegisterAnotherCardResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetConsumerSettingsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetConsumerSettingsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetConsumerSettingsResponse wrapgetConsumerSettings(){
                                com.usatech.ps.GetConsumerSettingsResponse wrappedElement = new com.usatech.ps.GetConsumerSettingsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.RegisterAnotherCreditCardKeyedResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.RegisterAnotherCreditCardKeyedResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.RegisterAnotherCreditCardKeyedResponse wrapregisterAnotherCreditCardKeyed(){
                                com.usatech.ps.RegisterAnotherCreditCardKeyedResponse wrappedElement = new com.usatech.ps.RegisterAnotherCreditCardKeyedResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetChargedCardsResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetChargedCardsResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetChargedCardsResponse wrapgetChargedCards(){
                                com.usatech.ps.GetChargedCardsResponse wrappedElement = new com.usatech.ps.GetChargedCardsResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.GetUserResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.GetUserResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.GetUserResponse wrapgetUser(){
                                com.usatech.ps.GetUserResponse wrappedElement = new com.usatech.ps.GetUserResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.ps.RetokenizeResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.ps.RetokenizeResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.ps.RetokenizeResponse wrapretokenize(){
                                com.usatech.ps.RetokenizeResponse wrappedElement = new com.usatech.ps.RetokenizeResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (com.usatech.ps.GetLocations.class.equals(type)){
                
                           return com.usatech.ps.GetLocations.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetLocationsResponse.class.equals(type)){
                
                           return com.usatech.ps.GetLocationsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCreditCardEncrypted.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCreditCardEncrypted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCreditCardEncryptedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.CreateUser.class.equals(type)){
                
                           return com.usatech.ps.CreateUser.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.CreateUserResponse.class.equals(type)){
                
                           return com.usatech.ps.CreateUserResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateRegion.class.equals(type)){
                
                           return com.usatech.ps.UpdateRegion.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateRegionResponse.class.equals(type)){
                
                           return com.usatech.ps.UpdateRegionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.CreateUserWithMobile.class.equals(type)){
                
                           return com.usatech.ps.CreateUserWithMobile.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.CreateUserWithMobileResponse.class.equals(type)){
                
                           return com.usatech.ps.CreateUserWithMobileResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.AddPromoCode.class.equals(type)){
                
                           return com.usatech.ps.AddPromoCode.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.AddPromoCodeResponse.class.equals(type)){
                
                           return com.usatech.ps.AddPromoCodeResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetReplenishments.class.equals(type)){
                
                           return com.usatech.ps.GetReplenishments.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetReplenishmentsResponse.class.equals(type)){
                
                           return com.usatech.ps.GetReplenishmentsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetPrepaidAccounts.class.equals(type)){
                
                           return com.usatech.ps.GetPrepaidAccounts.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetPrepaidAccountsResponse.class.equals(type)){
                
                           return com.usatech.ps.GetPrepaidAccountsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateUser.class.equals(type)){
                
                           return com.usatech.ps.UpdateUser.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateUserResponse.class.equals(type)){
                
                           return com.usatech.ps.UpdateUserResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetPromoCampaigns.class.equals(type)){
                
                           return com.usatech.ps.GetPromoCampaigns.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetPromoCampaignsResponse.class.equals(type)){
                
                           return com.usatech.ps.GetPromoCampaignsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateConsumerSettings.class.equals(type)){
                
                           return com.usatech.ps.UpdateConsumerSettings.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateConsumerSettingsResponse.class.equals(type)){
                
                           return com.usatech.ps.UpdateConsumerSettingsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCardEncrypted.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCardEncrypted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCardEncryptedResponse.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCardEncryptedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.SetupReplenish.class.equals(type)){
                
                           return com.usatech.ps.SetupReplenish.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.SetupReplenishResponse.class.equals(type)){
                
                           return com.usatech.ps.SetupReplenishResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.CreateUserEncrypted.class.equals(type)){
                
                           return com.usatech.ps.CreateUserEncrypted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.CreateUserEncryptedResponse.class.equals(type)){
                
                           return com.usatech.ps.CreateUserEncryptedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetAuthHolds.class.equals(type)){
                
                           return com.usatech.ps.GetAuthHolds.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetAuthHoldsResponse.class.equals(type)){
                
                           return com.usatech.ps.GetAuthHoldsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetPostalInfo.class.equals(type)){
                
                           return com.usatech.ps.GetPostalInfo.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetPostalInfoResponse.class.equals(type)){
                
                           return com.usatech.ps.GetPostalInfoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.ChangePassword.class.equals(type)){
                
                           return com.usatech.ps.ChangePassword.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.ChangePasswordResponse.class.equals(type)){
                
                           return com.usatech.ps.ChangePasswordResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateReplenish.class.equals(type)){
                
                           return com.usatech.ps.UpdateReplenish.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateReplenishResponse.class.equals(type)){
                
                           return com.usatech.ps.UpdateReplenishResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateUserWithMobile.class.equals(type)){
                
                           return com.usatech.ps.UpdateUserWithMobile.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.UpdateUserWithMobileResponse.class.equals(type)){
                
                           return com.usatech.ps.UpdateUserWithMobileResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RequestReplenish.class.equals(type)){
                
                           return com.usatech.ps.RequestReplenish.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RequestReplenishResponse.class.equals(type)){
                
                           return com.usatech.ps.RequestReplenishResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetLocationsByUsage.class.equals(type)){
                
                           return com.usatech.ps.GetLocationsByUsage.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetLocationsByUsageResponse.class.equals(type)){
                
                           return com.usatech.ps.GetLocationsByUsageResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCardKeyed.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCardKeyed.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCardKeyedResponse.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCardKeyedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetReplenishInfo.class.equals(type)){
                
                           return com.usatech.ps.GetReplenishInfo.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetReplenishInfoResponse.class.equals(type)){
                
                           return com.usatech.ps.GetReplenishInfoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.CreateUserKeyed.class.equals(type)){
                
                           return com.usatech.ps.CreateUserKeyed.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.CreateUserKeyedResponse.class.equals(type)){
                
                           return com.usatech.ps.CreateUserKeyedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetPrepaidActivities.class.equals(type)){
                
                           return com.usatech.ps.GetPrepaidActivities.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetPrepaidActivitiesResponse.class.equals(type)){
                
                           return com.usatech.ps.GetPrepaidActivitiesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.VerifyUser.class.equals(type)){
                
                           return com.usatech.ps.VerifyUser.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.VerifyUserResponse.class.equals(type)){
                
                           return com.usatech.ps.VerifyUserResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetLocationsByLocation.class.equals(type)){
                
                           return com.usatech.ps.GetLocationsByLocation.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetLocationsByLocationResponse.class.equals(type)){
                
                           return com.usatech.ps.GetLocationsByLocationResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.ResetPassword.class.equals(type)){
                
                           return com.usatech.ps.ResetPassword.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.ResetPasswordResponse.class.equals(type)){
                
                           return com.usatech.ps.ResetPasswordResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetRegions.class.equals(type)){
                
                           return com.usatech.ps.GetRegions.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetRegionsResponse.class.equals(type)){
                
                           return com.usatech.ps.GetRegionsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCard.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCard.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCardResponse.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCardResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetConsumerSettings.class.equals(type)){
                
                           return com.usatech.ps.GetConsumerSettings.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetConsumerSettingsResponse.class.equals(type)){
                
                           return com.usatech.ps.GetConsumerSettingsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCreditCardKeyed.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCreditCardKeyed.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RegisterAnotherCreditCardKeyedResponse.class.equals(type)){
                
                           return com.usatech.ps.RegisterAnotherCreditCardKeyedResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetChargedCards.class.equals(type)){
                
                           return com.usatech.ps.GetChargedCards.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetChargedCardsResponse.class.equals(type)){
                
                           return com.usatech.ps.GetChargedCardsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetUser.class.equals(type)){
                
                           return com.usatech.ps.GetUser.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.GetUserResponse.class.equals(type)){
                
                           return com.usatech.ps.GetUserResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.Retokenize.class.equals(type)){
                
                           return com.usatech.ps.Retokenize.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.usatech.ps.RetokenizeResponse.class.equals(type)){
                
                           return com.usatech.ps.RetokenizeResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    