package com.usatech.ps;

import java.io.Serializable;

public class PSPrepaidAccounts extends PSResponse implements Serializable {
	private static final long serialVersionUID = 6719499436381649707L;
	
	protected PSPrepaidAccount[] prepaidAccounts;

	public PSPrepaidAccount[] getPrepaidAccounts() {
		return prepaidAccounts;
	}

	public void setPrepaidAccounts(PSPrepaidAccount[] prepaidAccounts) {
		this.prepaidAccounts = prepaidAccounts;
	}
}
