package com.usatech.ps;

import java.io.Serializable;

public class PSConsumerSettings extends PSResponse implements Serializable {

	private static final long serialVersionUID = 222984516077517814L;
	protected PSConsumerSetting[] consumerSetting;
	public PSConsumerSetting[] getConsumerSetting() {
		return consumerSetting;
	}
	public void setConsumerSetting(PSConsumerSetting[] consumerSetting) {
		this.consumerSetting = consumerSetting;
	}

}
