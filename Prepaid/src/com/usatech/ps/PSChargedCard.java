package com.usatech.ps;

import java.io.Serializable;
import java.util.Date;

public class PSChargedCard extends PSResponse implements Serializable {
	private static final long serialVersionUID = 7905057209618101191L;
	protected long replenishId;
	protected Date replenishDate;
	protected String replenishCardNum;
	protected String currencyCd;
	public long getReplenishId() {
		return replenishId;
	}
	public void setReplenishId(long replenishId) {
		this.replenishId = replenishId;
	}
	public Date getReplenishDate() {
		return replenishDate;
	}
	public void setReplenishDate(Date replenishDate) {
		this.replenishDate = replenishDate;
	}
	public String getReplenishCardNum() {
		return replenishCardNum;
	}
	public void setReplenishCardNum(String replenishCardNum) {
		this.replenishCardNum = replenishCardNum;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}

}
