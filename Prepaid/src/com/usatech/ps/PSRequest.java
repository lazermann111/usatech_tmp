package com.usatech.ps;

import java.math.BigDecimal;
import java.util.Date;

public class PSRequest implements PrepaidServiceAPI {
	public PSUser getUser(String username, String password) {
		return null;
	}
	
	public PSResponse verifyUser(String username, String password, String passcode){
		return null;
	}
	
	public PSRegions getRegions(String username, String password) {
		return null;
	}

	public PSResponse updateRegion(String username, String password, int regionId) {
		return null;
	}
	
	public PSPrepaidAccounts getPrepaidAccounts(String username, String password) {
		return null;
	}
	
	
	public PSResponse updateUser(String oldUsername, String username, String password, String firstname, String lastname, String address1, String city, String state, String postal, String country,PSConsumerSetting[] consumerSettings){
		return null;
	}
	
	public PSResponse updateUserWithMobile(String oldUsername, int preferredCommType, String email, String mobile, int carrierId, String password, String firstname, String lastname, String address1, String city, String state, String postal, String country,PSConsumerSetting[] consumerSettings){
		return null;
	}
	
	public PSTokenResponse createUser(String prepaidCardNum, String prepaidSecurityCode, String username, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword,PSConsumerSetting[] consumerSettings){
		return null;
	}
	
	public PSTokenResponse createUserWithMobile(String prepaidCardNum, String prepaidSecurityCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings){
		return null;
	}
	
	public PSTokenResponse registerAnotherCard(String username, String password, String prepaidCardNum, String prepaidSecurityCode){
		return null;
	}
	
	public PSResponse changePassword(String username, String password, String newPassword, String newPasswordConfirm){
		return null;
	}
	
	public PSPrepaidActivities getPrepaidActivities(String username, String password, Long cardId, Date startTime, Date endTime, Integer maxRows){
		return null;
	}
	
	public PSResponse setupReplenish(String username, String password,Long cardId, String replenishCardNum, Integer replenishExpMonth, Integer replenishExpYear, String replenishSecurityCode, String address1, String city, String state, String postal, String country, BigDecimal amount, Integer replenishType,BigDecimal threshhold){
		return null;
	}
	
	public PSResponse updateReplenish(String username, String password, Long cardId, Long replenishId, BigDecimal amount, Integer replenishType,BigDecimal threshhold, Integer replenCount){
		return null;
	}
	
	public PSReplenishInfoArray getReplenishInfo(String username, String password, Long cardId, Long replenishId){
		return null;
	}
	
	public PSResponse requestReplenish(String username, String password, Long cardId, Long replenishId, BigDecimal amount){
		return null;
	}
	
	public PSPromoCampaigns getPromoCampaigns(String username, String password){
		return null;
	}
	
	public PSPromoCampaigns getLocations(String username, String password, BigDecimal longitude, BigDecimal latitude){
		return null;
	}
	
	public PSResponse resetPassword(String username, String password){
		return null;
	}
	
	public PSConsumerSettings getConsumerSettings(String username, String password){
		return null;
	}
	
	public PSResponse updateConsumerSettings(String username, String password, PSConsumerSetting[] consumerSettings){
		return null;
	}
	
	public PSPostalInfo getPostalInfo(String postalCd, String countryCd){
		return null;
	}
	
	public PSPromoCampaigns getLocationsByUsage(String username, String password){
		return null;
	}
	
	public PSReplenishmentArray getReplenishments(String username, String password, Long cardId, Date startTime, Date endTime, Integer maxRows){
		return null;
	}
	
	public PSChargedCardArray getChargedCards(String username, String password){
		return null;
	}
	
	public PSAuthHoldArray getAuthHolds(String username, String password, Long cardId){
		return null;
	}
	
	public PSPromoCampaigns getLocationsByLocation(String username, String password, String postal, String country, float maxProximityMiles, boolean discountsOnly){
		return null;
	}

	@Override
	public PSTokenResponse retokenize(String username, String password, long cardId, String securityCode, String attributes) {
		return null;
	}
	
	public PSTokenResponse createUserEncrypted(String entryType,int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings, String promoCode){
		return null;
	}

	public PSTokenResponse registerAnotherCardEncrypted(String username, String password, String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode){
		return null;
	}

	public PSTokenResponse createUserKeyed(String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings, String promoCode){
		return null;
	}

	public PSTokenResponse registerAnotherCardKeyed(String username, String password, String cardNum, String securityCode, Integer expMonth, Integer expYear,String billingCountry, String billingAddress, String billingPostalCode){
		return null;
	}

	public PSResponse addPromoCode(String username, String password, String promoCode){
		return null;
	}
	
	public PSTokenResponse registerAnotherCreditCardEncrypted(String username, String password, String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode, String promoCode){
		return null;
	}
	
	public PSTokenResponse registerAnotherCreditCardKeyed(String username, String password, String cardNum, String securityCode, Integer expMonth, Integer expYear,String billingCountry, String billingAddress, String billingPostalCode, String promoCode){
		return null;
	}
}
