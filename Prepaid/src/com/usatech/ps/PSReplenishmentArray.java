package com.usatech.ps;

import java.io.Serializable;

public class PSReplenishmentArray extends PSResponse implements Serializable{
	private static final long serialVersionUID = 4895234104005735237L;
	protected PSReplenishment[] replenishmentArray;
	public PSReplenishment[] getReplenishmentArray() {
		return replenishmentArray;
	}
	public void setReplenishmentArray(PSReplenishment[] replenishmentArray) {
		this.replenishmentArray = replenishmentArray;
	}

}
