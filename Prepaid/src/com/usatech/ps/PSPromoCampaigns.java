package com.usatech.ps;

import java.io.Serializable;

public class PSPromoCampaigns extends PSResponse implements Serializable {
	private static final long serialVersionUID = -7221826956537587745L;
	
	protected PSPromoCampaign[] promoCampaigns;

	public PSPromoCampaign[] getPromoCampaigns() {
		return promoCampaigns;
	}

	public void setPromoCampaigns(PSPromoCampaign[] promoCampaigns) {
		this.promoCampaigns = promoCampaigns;
	}

}
