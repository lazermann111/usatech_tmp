package com.usatech.ps;

import java.io.Serializable;

public class PSPostalInfo extends PSResponse implements Serializable {

	private static final long serialVersionUID = 6710207617982613939L;
	
	protected String city;
	protected String state;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	

}
