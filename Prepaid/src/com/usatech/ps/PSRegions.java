package com.usatech.ps;

import java.io.Serializable;

public class PSRegions extends PSResponse implements Serializable {

	private static final long serialVersionUID = -7909065833735748220L;
	protected PSRegion[] regions;
	public PSRegion[] getRegions() {
		return regions;
	}
	public void setRegions(PSRegion[] regions) {
		this.regions = regions;
	}
	
}
