package com.usatech.ps;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PSPrepaidAccount extends PSConsumerAccount implements Serializable {
	private static final long serialVersionUID = 2582401130508334333L;
	
	protected BigDecimal promoTotal;
	protected BigDecimal replenishTotal;
	protected BigDecimal discount;
	protected long points;
	protected BigDecimal pointsTotal;
	protected BigDecimal onHold;
	protected BigDecimal pendingReplenish;
	
	public BigDecimal getPromoTotal() {
		return promoTotal;
	}
	public void setPromoTotal(BigDecimal promoTotal) {
		this.promoTotal = promoTotal;
	}
	public BigDecimal getReplenishTotal() {
		return replenishTotal;
	}
	public void setReplenishTotal(BigDecimal replenishTotal) {
		this.replenishTotal = replenishTotal;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public long getPoints() {
		return points;
	}
	public void setPoints(long points) {
		this.points = points;
	}
	public BigDecimal getPointsTotal() {
		return pointsTotal;
	}
	public void setPointsTotal(BigDecimal pointsTotal) {
		this.pointsTotal = pointsTotal;
	}
	
	public BigDecimal getOnHold() {
		return onHold;
	}
	public void setOnHold(BigDecimal onHold) {
		this.onHold = onHold;
	}
	public BigDecimal getPendingReplenish() {
		return pendingReplenish;
	}
	public void setPendingReplenish(BigDecimal pendingReplenish) {
		this.pendingReplenish = pendingReplenish;
	}
}
