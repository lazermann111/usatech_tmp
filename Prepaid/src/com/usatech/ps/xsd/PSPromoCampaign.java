
/**
 * PSPromoCampaign.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.usatech.ps.xsd;
            

            /**
            *  PSPromoCampaign bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class PSPromoCampaign extends com.usatech.ps.xsd.PSResponse
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = PSPromoCampaign
                Namespace URI = http://ps.usatech.com/xsd
                Namespace Prefix = ns1
                */
            

                        /**
                        * field for AttentionType
                        */

                        
                                    protected java.lang.String localAttentionType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAttentionTypeTracker = false ;

                           public boolean isAttentionTypeSpecified(){
                               return localAttentionTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAttentionType(){
                               return localAttentionType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AttentionType
                               */
                               public void setAttentionType(java.lang.String param){
                            localAttentionTypeTracker = true;
                                   
                                            this.localAttentionType=param;
                                    

                               }
                            

                        /**
                        * field for CampaignDescription
                        */

                        
                                    protected java.lang.String localCampaignDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignDescriptionTracker = false ;

                           public boolean isCampaignDescriptionSpecified(){
                               return localCampaignDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCampaignDescription(){
                               return localCampaignDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignDescription
                               */
                               public void setCampaignDescription(java.lang.String param){
                            localCampaignDescriptionTracker = true;
                                   
                                            this.localCampaignDescription=param;
                                    

                               }
                            

                        /**
                        * field for CampaignDiscountPercent
                        */

                        
                                    protected java.math.BigDecimal localCampaignDiscountPercent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignDiscountPercentTracker = false ;

                           public boolean isCampaignDiscountPercentSpecified(){
                               return localCampaignDiscountPercentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getCampaignDiscountPercent(){
                               return localCampaignDiscountPercent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignDiscountPercent
                               */
                               public void setCampaignDiscountPercent(java.math.BigDecimal param){
                            localCampaignDiscountPercentTracker = true;
                                   
                                            this.localCampaignDiscountPercent=param;
                                    

                               }
                            

                        /**
                        * field for CampaignEndDate
                        */

                        
                                    protected java.util.Date localCampaignEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignEndDateTracker = false ;

                           public boolean isCampaignEndDateSpecified(){
                               return localCampaignEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Date
                           */
                           public  java.util.Date getCampaignEndDate(){
                               return localCampaignEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignEndDate
                               */
                               public void setCampaignEndDate(java.util.Date param){
                            localCampaignEndDateTracker = true;
                                   
                                            this.localCampaignEndDate=param;
                                    

                               }
                            

                        /**
                        * field for CampaignId
                        */

                        
                                    protected long localCampaignId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignIdTracker = false ;

                           public boolean isCampaignIdSpecified(){
                               return localCampaignIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getCampaignId(){
                               return localCampaignId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignId
                               */
                               public void setCampaignId(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localCampaignIdTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localCampaignId=param;
                                    

                               }
                            

                        /**
                        * field for CampaignName
                        */

                        
                                    protected java.lang.String localCampaignName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignNameTracker = false ;

                           public boolean isCampaignNameSpecified(){
                               return localCampaignNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCampaignName(){
                               return localCampaignName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignName
                               */
                               public void setCampaignName(java.lang.String param){
                            localCampaignNameTracker = true;
                                   
                                            this.localCampaignName=param;
                                    

                               }
                            

                        /**
                        * field for CampaignPriority
                        */

                        
                                    protected int localCampaignPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignPriorityTracker = false ;

                           public boolean isCampaignPrioritySpecified(){
                               return localCampaignPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getCampaignPriority(){
                               return localCampaignPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignPriority
                               */
                               public void setCampaignPriority(int param){
                            
                                       // setting primitive attribute tracker to true
                                       localCampaignPriorityTracker =
                                       param != java.lang.Integer.MIN_VALUE;
                                   
                                            this.localCampaignPriority=param;
                                    

                               }
                            

                        /**
                        * field for CampaignRecurSchedule
                        */

                        
                                    protected java.lang.String localCampaignRecurSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignRecurScheduleTracker = false ;

                           public boolean isCampaignRecurScheduleSpecified(){
                               return localCampaignRecurScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCampaignRecurSchedule(){
                               return localCampaignRecurSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignRecurSchedule
                               */
                               public void setCampaignRecurSchedule(java.lang.String param){
                            localCampaignRecurScheduleTracker = true;
                                   
                                            this.localCampaignRecurSchedule=param;
                                    

                               }
                            

                        /**
                        * field for CampaignStartDate
                        */

                        
                                    protected java.util.Date localCampaignStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignStartDateTracker = false ;

                           public boolean isCampaignStartDateSpecified(){
                               return localCampaignStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Date
                           */
                           public  java.util.Date getCampaignStartDate(){
                               return localCampaignStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignStartDate
                               */
                               public void setCampaignStartDate(java.util.Date param){
                            localCampaignStartDateTracker = true;
                                   
                                            this.localCampaignStartDate=param;
                                    

                               }
                            

                        /**
                        * field for CampaignThreshold
                        */

                        
                                    protected java.math.BigDecimal localCampaignThreshold ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignThresholdTracker = false ;

                           public boolean isCampaignThresholdSpecified(){
                               return localCampaignThresholdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getCampaignThreshold(){
                               return localCampaignThreshold;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignThreshold
                               */
                               public void setCampaignThreshold(java.math.BigDecimal param){
                            localCampaignThresholdTracker = true;
                                   
                                            this.localCampaignThreshold=param;
                                    

                               }
                            

                        /**
                        * field for CampaignType
                        */

                        
                                    protected java.lang.String localCampaignType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignTypeTracker = false ;

                           public boolean isCampaignTypeSpecified(){
                               return localCampaignTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCampaignType(){
                               return localCampaignType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignType
                               */
                               public void setCampaignType(java.lang.String param){
                            localCampaignTypeTracker = true;
                                   
                                            this.localCampaignType=param;
                                    

                               }
                            

                        /**
                        * field for CampaignTypeId
                        */

                        
                                    protected int localCampaignTypeId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignTypeIdTracker = false ;

                           public boolean isCampaignTypeIdSpecified(){
                               return localCampaignTypeIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getCampaignTypeId(){
                               return localCampaignTypeId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignTypeId
                               */
                               public void setCampaignTypeId(int param){
                            
                                       // setting primitive attribute tracker to true
                                       localCampaignTypeIdTracker =
                                       param != java.lang.Integer.MIN_VALUE;
                                   
                                            this.localCampaignTypeId=param;
                                    

                               }
                            

                        /**
                        * field for CardId
                        */

                        
                                    protected long localCardId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCardIdTracker = false ;

                           public boolean isCardIdSpecified(){
                               return localCardIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getCardId(){
                               return localCardId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CardId
                               */
                               public void setCardId(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localCardIdTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localCardId=param;
                                    

                               }
                            

                        /**
                        * field for CardNum
                        */

                        
                                    protected java.lang.String localCardNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCardNumTracker = false ;

                           public boolean isCardNumSpecified(){
                               return localCardNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCardNum(){
                               return localCardNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CardNum
                               */
                               public void setCardNum(java.lang.String param){
                            localCardNumTracker = true;
                                   
                                            this.localCardNum=param;
                                    

                               }
                            

                        /**
                        * field for Details
                        */

                        
                                    protected java.lang.String localDetails ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDetailsTracker = false ;

                           public boolean isDetailsSpecified(){
                               return localDetailsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDetails(){
                               return localDetails;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Details
                               */
                               public void setDetails(java.lang.String param){
                            localDetailsTracker = true;
                                   
                                            this.localDetails=param;
                                    

                               }
                            

                        /**
                        * field for DeviceSerialCd
                        */

                        
                                    protected java.lang.String localDeviceSerialCd ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeviceSerialCdTracker = false ;

                           public boolean isDeviceSerialCdSpecified(){
                               return localDeviceSerialCdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDeviceSerialCd(){
                               return localDeviceSerialCd;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeviceSerialCd
                               */
                               public void setDeviceSerialCd(java.lang.String param){
                            localDeviceSerialCdTracker = true;
                                   
                                            this.localDeviceSerialCd=param;
                                    

                               }
                            

                        /**
                        * field for LocationAddress1
                        */

                        
                                    protected java.lang.String localLocationAddress1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationAddress1Tracker = false ;

                           public boolean isLocationAddress1Specified(){
                               return localLocationAddress1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocationAddress1(){
                               return localLocationAddress1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationAddress1
                               */
                               public void setLocationAddress1(java.lang.String param){
                            localLocationAddress1Tracker = true;
                                   
                                            this.localLocationAddress1=param;
                                    

                               }
                            

                        /**
                        * field for LocationAddress2
                        */

                        
                                    protected java.lang.String localLocationAddress2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationAddress2Tracker = false ;

                           public boolean isLocationAddress2Specified(){
                               return localLocationAddress2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocationAddress2(){
                               return localLocationAddress2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationAddress2
                               */
                               public void setLocationAddress2(java.lang.String param){
                            localLocationAddress2Tracker = true;
                                   
                                            this.localLocationAddress2=param;
                                    

                               }
                            

                        /**
                        * field for LocationCity
                        */

                        
                                    protected java.lang.String localLocationCity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationCityTracker = false ;

                           public boolean isLocationCitySpecified(){
                               return localLocationCityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocationCity(){
                               return localLocationCity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationCity
                               */
                               public void setLocationCity(java.lang.String param){
                            localLocationCityTracker = true;
                                   
                                            this.localLocationCity=param;
                                    

                               }
                            

                        /**
                        * field for LocationCountry
                        */

                        
                                    protected java.lang.String localLocationCountry ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationCountryTracker = false ;

                           public boolean isLocationCountrySpecified(){
                               return localLocationCountryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocationCountry(){
                               return localLocationCountry;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationCountry
                               */
                               public void setLocationCountry(java.lang.String param){
                            localLocationCountryTracker = true;
                                   
                                            this.localLocationCountry=param;
                                    

                               }
                            

                        /**
                        * field for LocationDescription
                        */

                        
                                    protected java.lang.String localLocationDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationDescriptionTracker = false ;

                           public boolean isLocationDescriptionSpecified(){
                               return localLocationDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocationDescription(){
                               return localLocationDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationDescription
                               */
                               public void setLocationDescription(java.lang.String param){
                            localLocationDescriptionTracker = true;
                                   
                                            this.localLocationDescription=param;
                                    

                               }
                            

                        /**
                        * field for LocationLastUsedDate
                        */

                        
                                    protected java.util.Date localLocationLastUsedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationLastUsedDateTracker = false ;

                           public boolean isLocationLastUsedDateSpecified(){
                               return localLocationLastUsedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Date
                           */
                           public  java.util.Date getLocationLastUsedDate(){
                               return localLocationLastUsedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationLastUsedDate
                               */
                               public void setLocationLastUsedDate(java.util.Date param){
                            localLocationLastUsedDateTracker = true;
                                   
                                            this.localLocationLastUsedDate=param;
                                    

                               }
                            

                        /**
                        * field for LocationName
                        */

                        
                                    protected java.lang.String localLocationName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationNameTracker = false ;

                           public boolean isLocationNameSpecified(){
                               return localLocationNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocationName(){
                               return localLocationName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationName
                               */
                               public void setLocationName(java.lang.String param){
                            localLocationNameTracker = true;
                                   
                                            this.localLocationName=param;
                                    

                               }
                            

                        /**
                        * field for LocationPostal
                        */

                        
                                    protected java.lang.String localLocationPostal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationPostalTracker = false ;

                           public boolean isLocationPostalSpecified(){
                               return localLocationPostalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocationPostal(){
                               return localLocationPostal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationPostal
                               */
                               public void setLocationPostal(java.lang.String param){
                            localLocationPostalTracker = true;
                                   
                                            this.localLocationPostal=param;
                                    

                               }
                            

                        /**
                        * field for LocationPriority
                        */

                        
                                    protected int localLocationPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationPriorityTracker = false ;

                           public boolean isLocationPrioritySpecified(){
                               return localLocationPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getLocationPriority(){
                               return localLocationPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationPriority
                               */
                               public void setLocationPriority(int param){
                            
                                       // setting primitive attribute tracker to true
                                       localLocationPriorityTracker =
                                       param != java.lang.Integer.MIN_VALUE;
                                   
                                            this.localLocationPriority=param;
                                    

                               }
                            

                        /**
                        * field for LocationProductType
                        */

                        
                                    protected java.lang.String localLocationProductType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationProductTypeTracker = false ;

                           public boolean isLocationProductTypeSpecified(){
                               return localLocationProductTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocationProductType(){
                               return localLocationProductType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationProductType
                               */
                               public void setLocationProductType(java.lang.String param){
                            localLocationProductTypeTracker = true;
                                   
                                            this.localLocationProductType=param;
                                    

                               }
                            

                        /**
                        * field for LocationProximityMiles
                        */

                        
                                    protected int localLocationProximityMiles ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationProximityMilesTracker = false ;

                           public boolean isLocationProximityMilesSpecified(){
                               return localLocationProximityMilesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getLocationProximityMiles(){
                               return localLocationProximityMiles;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationProximityMiles
                               */
                               public void setLocationProximityMiles(int param){
                            
                                       // setting primitive attribute tracker to true
                                       localLocationProximityMilesTracker =
                                       param != java.lang.Integer.MIN_VALUE;
                                   
                                            this.localLocationProximityMiles=param;
                                    

                               }
                            

                        /**
                        * field for LocationState
                        */

                        
                                    protected java.lang.String localLocationState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationStateTracker = false ;

                           public boolean isLocationStateSpecified(){
                               return localLocationStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocationState(){
                               return localLocationState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationState
                               */
                               public void setLocationState(java.lang.String param){
                            localLocationStateTracker = true;
                                   
                                            this.localLocationState=param;
                                    

                               }
                            

                        /**
                        * field for LocationUsedCount
                        */

                        
                                    protected int localLocationUsedCount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationUsedCountTracker = false ;

                           public boolean isLocationUsedCountSpecified(){
                               return localLocationUsedCountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getLocationUsedCount(){
                               return localLocationUsedCount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationUsedCount
                               */
                               public void setLocationUsedCount(int param){
                            
                                       // setting primitive attribute tracker to true
                                       localLocationUsedCountTracker =
                                       param != java.lang.Integer.MIN_VALUE;
                                   
                                            this.localLocationUsedCount=param;
                                    

                               }
                            

                        /**
                        * field for PromoCode
                        */

                        
                                    protected java.lang.String localPromoCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPromoCodeTracker = false ;

                           public boolean isPromoCodeSpecified(){
                               return localPromoCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPromoCode(){
                               return localPromoCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PromoCode
                               */
                               public void setPromoCode(java.lang.String param){
                            localPromoCodeTracker = true;
                                   
                                            this.localPromoCode=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected java.lang.String localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(java.lang.String param){
                            localTitleTracker = true;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://ps.usatech.com/xsd");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":PSPromoCampaign",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "PSPromoCampaign",
                           xmlWriter);
                   }

                if (localReturnCodeTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "returnCode", xmlWriter);
                             
                                               if (localReturnCode==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("returnCode cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReturnCode));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReturnMessageTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "returnMessage", xmlWriter);
                             

                                          if (localReturnMessage==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localReturnMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAttentionTypeTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "attentionType", xmlWriter);
                             

                                          if (localAttentionType==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAttentionType);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignDescriptionTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignDescription", xmlWriter);
                             

                                          if (localCampaignDescription==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCampaignDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignDiscountPercentTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignDiscountPercent", xmlWriter);
                             

                                          if (localCampaignDiscountPercent==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignDiscountPercent));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignEndDateTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignEndDate", xmlWriter);
                             

                                          if (localCampaignEndDate==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignIdTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignId", xmlWriter);
                             
                                               if (localCampaignId==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("campaignId cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignId));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignNameTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignName", xmlWriter);
                             

                                          if (localCampaignName==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCampaignName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignPriorityTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignPriority", xmlWriter);
                             
                                               if (localCampaignPriority==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("campaignPriority cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignPriority));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignRecurScheduleTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignRecurSchedule", xmlWriter);
                             

                                          if (localCampaignRecurSchedule==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCampaignRecurSchedule);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignStartDateTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignStartDate", xmlWriter);
                             

                                          if (localCampaignStartDate==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignThresholdTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignThreshold", xmlWriter);
                             

                                          if (localCampaignThreshold==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignThreshold));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignTypeTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignType", xmlWriter);
                             

                                          if (localCampaignType==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCampaignType);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCampaignTypeIdTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "campaignTypeId", xmlWriter);
                             
                                               if (localCampaignTypeId==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("campaignTypeId cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignTypeId));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCardIdTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "cardId", xmlWriter);
                             
                                               if (localCardId==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("cardId cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCardId));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCardNumTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "cardNum", xmlWriter);
                             

                                          if (localCardNum==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCardNum);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDetailsTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "details", xmlWriter);
                             

                                          if (localDetails==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDetails);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDeviceSerialCdTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "deviceSerialCd", xmlWriter);
                             

                                          if (localDeviceSerialCd==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDeviceSerialCd);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationAddress1Tracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationAddress1", xmlWriter);
                             

                                          if (localLocationAddress1==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocationAddress1);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationAddress2Tracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationAddress2", xmlWriter);
                             

                                          if (localLocationAddress2==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocationAddress2);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationCityTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationCity", xmlWriter);
                             

                                          if (localLocationCity==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocationCity);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationCountryTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationCountry", xmlWriter);
                             

                                          if (localLocationCountry==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocationCountry);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationDescriptionTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationDescription", xmlWriter);
                             

                                          if (localLocationDescription==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocationDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationLastUsedDateTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationLastUsedDate", xmlWriter);
                             

                                          if (localLocationLastUsedDate==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationLastUsedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationNameTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationName", xmlWriter);
                             

                                          if (localLocationName==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocationName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationPostalTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationPostal", xmlWriter);
                             

                                          if (localLocationPostal==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocationPostal);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationPriorityTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationPriority", xmlWriter);
                             
                                               if (localLocationPriority==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("locationPriority cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationPriority));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationProductTypeTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationProductType", xmlWriter);
                             

                                          if (localLocationProductType==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocationProductType);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationProximityMilesTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationProximityMiles", xmlWriter);
                             
                                               if (localLocationProximityMiles==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("locationProximityMiles cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationProximityMiles));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationStateTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationState", xmlWriter);
                             

                                          if (localLocationState==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocationState);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationUsedCountTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "locationUsedCount", xmlWriter);
                             
                                               if (localLocationUsedCount==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("locationUsedCount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationUsedCount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPromoCodeTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "promoCode", xmlWriter);
                             

                                          if (localPromoCode==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPromoCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTitleTracker){
                                    namespace = "http://ps.usatech.com/xsd";
                                    writeStartElement(null, namespace, "title", xmlWriter);
                             

                                          if (localTitle==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://ps.usatech.com/xsd")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd","PSPromoCampaign"));
                 if (localReturnCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "returnCode"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReturnCode));
                            } if (localReturnMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "returnMessage"));
                                 
                                         elementList.add(localReturnMessage==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReturnMessage));
                                    } if (localAttentionTypeTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "attentionType"));
                                 
                                         elementList.add(localAttentionType==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAttentionType));
                                    } if (localCampaignDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignDescription"));
                                 
                                         elementList.add(localCampaignDescription==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignDescription));
                                    } if (localCampaignDiscountPercentTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignDiscountPercent"));
                                 
                                         elementList.add(localCampaignDiscountPercent==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignDiscountPercent));
                                    } if (localCampaignEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignEndDate"));
                                 
                                         elementList.add(localCampaignEndDate==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignEndDate));
                                    } if (localCampaignIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignId"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignId));
                            } if (localCampaignNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignName"));
                                 
                                         elementList.add(localCampaignName==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignName));
                                    } if (localCampaignPriorityTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignPriority"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignPriority));
                            } if (localCampaignRecurScheduleTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignRecurSchedule"));
                                 
                                         elementList.add(localCampaignRecurSchedule==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignRecurSchedule));
                                    } if (localCampaignStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignStartDate"));
                                 
                                         elementList.add(localCampaignStartDate==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignStartDate));
                                    } if (localCampaignThresholdTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignThreshold"));
                                 
                                         elementList.add(localCampaignThreshold==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignThreshold));
                                    } if (localCampaignTypeTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignType"));
                                 
                                         elementList.add(localCampaignType==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignType));
                                    } if (localCampaignTypeIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "campaignTypeId"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignTypeId));
                            } if (localCardIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "cardId"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCardId));
                            } if (localCardNumTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "cardNum"));
                                 
                                         elementList.add(localCardNum==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCardNum));
                                    } if (localDetailsTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "details"));
                                 
                                         elementList.add(localDetails==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDetails));
                                    } if (localDeviceSerialCdTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "deviceSerialCd"));
                                 
                                         elementList.add(localDeviceSerialCd==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeviceSerialCd));
                                    } if (localLocationAddress1Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationAddress1"));
                                 
                                         elementList.add(localLocationAddress1==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationAddress1));
                                    } if (localLocationAddress2Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationAddress2"));
                                 
                                         elementList.add(localLocationAddress2==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationAddress2));
                                    } if (localLocationCityTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationCity"));
                                 
                                         elementList.add(localLocationCity==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationCity));
                                    } if (localLocationCountryTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationCountry"));
                                 
                                         elementList.add(localLocationCountry==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationCountry));
                                    } if (localLocationDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationDescription"));
                                 
                                         elementList.add(localLocationDescription==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationDescription));
                                    } if (localLocationLastUsedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationLastUsedDate"));
                                 
                                         elementList.add(localLocationLastUsedDate==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationLastUsedDate));
                                    } if (localLocationNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationName"));
                                 
                                         elementList.add(localLocationName==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationName));
                                    } if (localLocationPostalTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationPostal"));
                                 
                                         elementList.add(localLocationPostal==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationPostal));
                                    } if (localLocationPriorityTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationPriority"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationPriority));
                            } if (localLocationProductTypeTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationProductType"));
                                 
                                         elementList.add(localLocationProductType==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationProductType));
                                    } if (localLocationProximityMilesTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationProximityMiles"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationProximityMiles));
                            } if (localLocationStateTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationState"));
                                 
                                         elementList.add(localLocationState==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationState));
                                    } if (localLocationUsedCountTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "locationUsedCount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationUsedCount));
                            } if (localPromoCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "promoCode"));
                                 
                                         elementList.add(localPromoCode==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPromoCode));
                                    } if (localTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://ps.usatech.com/xsd",
                                                                      "title"));
                                 
                                         elementList.add(localTitle==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitle));
                                    }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static PSPromoCampaign parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            PSPromoCampaign object =
                new PSPromoCampaign();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"PSPromoCampaign".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (PSPromoCampaign)com.usatech.ps.xsd.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","returnCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"returnCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReturnCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setReturnCode(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","returnMessage").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReturnMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","attentionType").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAttentionType(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignDescription").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignDiscountPercent").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignDiscountPercent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignEndDate").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"campaignId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCampaignId(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignName").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignPriority").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"campaignPriority" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignPriority(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCampaignPriority(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignRecurSchedule").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignRecurSchedule(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignStartDate").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignThreshold").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignThreshold(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignType").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignType(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","campaignTypeId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"campaignTypeId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCampaignTypeId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCampaignTypeId(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","cardId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"cardId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCardId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCardId(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","cardNum").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCardNum(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","details").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDetails(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","deviceSerialCd").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeviceSerialCd(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationAddress1").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationAddress1(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationAddress2").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationAddress2(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationCity").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationCity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationCountry").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationCountry(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationDescription").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationLastUsedDate").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationLastUsedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationName").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationPostal").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationPostal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationPriority").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"locationPriority" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationPriority(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLocationPriority(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationProductType").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationProductType(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationProximityMiles").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"locationProximityMiles" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationProximityMiles(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLocationProximityMiles(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationState").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationState(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","locationUsedCount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"locationUsedCount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationUsedCount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLocationUsedCount(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","promoCode").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPromoCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://ps.usatech.com/xsd","title").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    