
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

        
            package com.usatech.ps.xsd;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSRegions".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSRegions.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSRegion".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSRegion.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSPostalInfo".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSPostalInfo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSPrepaidAccount".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSPrepaidAccount.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSConsumerSetting".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSConsumerSetting.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSReplenishInfoArray".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSReplenishInfoArray.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSReplenishmentArray".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSReplenishmentArray.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSReplenishInfo".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSReplenishInfo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSPrepaidAccounts".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSPrepaidAccounts.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSReplenishment".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSReplenishment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSPrepaidActivities".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSPrepaidActivities.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSChargedCard".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSChargedCard.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSResponse".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSTokenResponse".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSTokenResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSChargedCardArray".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSChargedCardArray.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSPrepaidActivity".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSPrepaidActivity.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSUser".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSUser.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSAuthHold".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSAuthHold.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSPromoCampaigns".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSPromoCampaigns.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSConsumerSettings".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSConsumerSettings.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSPromoCampaign".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSPromoCampaign.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSAuthHoldArray".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSAuthHoldArray.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://ps.usatech.com/xsd".equals(namespaceURI) &&
                  "PSConsumerAccount".equals(typeName)){
                   
                            return  com.usatech.ps.xsd.PSConsumerAccount.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    