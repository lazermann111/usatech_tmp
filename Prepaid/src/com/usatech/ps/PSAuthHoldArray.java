package com.usatech.ps;

import java.io.Serializable;

public class PSAuthHoldArray extends PSResponse implements Serializable {
	private static final long serialVersionUID = 8334105406387412370L;
	protected PSAuthHold[] authHoldArray;
	public PSAuthHold[] getAuthHoldArray() {
		return authHoldArray;
	}
	public void setAuthHoldArray(PSAuthHold[] authHoldArray) {
		this.authHoldArray = authHoldArray;
	}

}
