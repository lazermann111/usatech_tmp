package com.usatech.ps;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PSReplenishment extends PSResponse implements Serializable{
	private static final long serialVersionUID = 164675401268721963L;
	protected long id;
	protected String replenishedCard;
	protected Date date;
	protected String chargedCard;
	protected BigDecimal amount;
	protected String currencyCd;
	protected String type;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getReplenishedCard() {
		return replenishedCard;
	}
	public void setReplenishedCard(String replenishedCard) {
		this.replenishedCard = replenishedCard;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getChargedCard() {
		return chargedCard;
	}
	public void setChargedCard(String chargedCard) {
		this.chargedCard = chargedCard;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
