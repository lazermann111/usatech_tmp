package com.usatech.ps;

import java.io.Serializable;

public class PSConsumerSetting implements Serializable {

	private static final long serialVersionUID = 1537626504626528521L;
	
	protected String settingId;
	protected Object value;
	protected String label;
	protected String group;
	protected String editor;
	protected String converter;
	
	public PSConsumerSetting() {
		super();
	}
	public PSConsumerSetting(String settingId, Object value) {
		super();
		this.settingId = settingId;
		this.value = value;
	}
	public String getSettingId() {
		return settingId;
	}
	public void setSettingId(String settingId) {
		this.settingId = settingId;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public String getConverter() {
		return converter;
	}
	public void setConverter(String converter) {
		this.converter = converter;
	}

}
