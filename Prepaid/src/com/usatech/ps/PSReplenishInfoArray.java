package com.usatech.ps;

import java.io.Serializable;

public class PSReplenishInfoArray extends PSResponse implements Serializable {
	private static final long serialVersionUID = -4880946459566980442L;
	protected PSReplenishInfo[] replenishInfoArray;
	public PSReplenishInfo[] getReplenishInfoArray() {
		return replenishInfoArray;
	}
	public void setReplenishInfoArray(PSReplenishInfo[] replenishInfoArray) {
		this.replenishInfoArray = replenishInfoArray;
	}
}
