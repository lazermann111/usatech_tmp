package com.usatech.ps;

import java.math.BigDecimal;
import java.util.Date;

public interface PrepaidServiceAPI {
	// Get user information 
	public PSUser getUser(String username, String password);
	
	public PSResponse verifyUser(String username, String password, String passcode);
	
	public PSRegions getRegions(String username, String password);
	
	public PSResponse updateRegion(String username, String password, int regionId);
	
	// Get user prepaid accounts
	public PSPrepaidAccounts getPrepaidAccounts(String username, String password);
	
	public PSResponse updateUser(String oldUsername, String username, String password, String firstname, String lastname, String address1, String city, String state, String postal, String country, PSConsumerSetting[] consumerSettings);
	
	public PSResponse updateUserWithMobile(String oldUsername, int preferredCommType, String email, String mobile, int carrierId, String password, String firstname, String lastname, String address1, String city, String state, String postal, String country, PSConsumerSetting[] consumerSettings);
	
	public PSTokenResponse createUser(String prepaidCardNum, String prepaidSecurityCode, String username, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings);
	
	public PSTokenResponse createUserWithMobile(String prepaidCardNum, String prepaidSecurityCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings);
	
	public PSTokenResponse registerAnotherCard(String username, String password, String prepaidCardNum, String prepaidSecurityCode);
	
	public PSResponse changePassword(String username, String password, String newPassword, String newPasswordConfirm);
	
	public PSPrepaidActivities getPrepaidActivities(String username, String password, Long cardId, Date startTime, Date endTime, Integer maxRows);
	
	public PSResponse setupReplenish(String username, String password,Long cardId, String replenishCardNum, Integer replenishExpMonth, Integer replenishExpYear, String replenishSecurityCode, String address1, String city, String state, String postal, String country, BigDecimal amount, Integer replenishType,BigDecimal threshhold);
	
	public PSReplenishInfoArray getReplenishInfo(String username, String password, Long cardId, Long replenishId);
	
	public PSResponse updateReplenish(String username, String password, Long cardId, Long replenishId, BigDecimal amount, Integer replenishType,BigDecimal threshhold, Integer replenCount);
	
	public PSResponse requestReplenish(String username, String password, Long cardId, Long replenishId, BigDecimal amount);
	
	public PSPromoCampaigns getPromoCampaigns(String username, String password);
	
	public PSPromoCampaigns getLocations(String username, String password, BigDecimal longitude, BigDecimal latitude);
	
	public PSResponse resetPassword(String username, String password);
	
	public PSConsumerSettings getConsumerSettings(String username, String password);
	
	public PSResponse updateConsumerSettings(String username, String password, PSConsumerSetting[] consumerSettings);
	
	public PSPostalInfo getPostalInfo(String postalCd, String countryCd);
	
	public PSPromoCampaigns getLocationsByUsage(String username, String password);
	
	public PSReplenishmentArray getReplenishments(String username, String password, Long cardId, Date startTime, Date endTime, Integer maxRows);
	
	public PSChargedCardArray getChargedCards(String username, String password);
	
	public PSAuthHoldArray getAuthHolds(String username, String password, Long cardId);
	
	public PSPromoCampaigns getLocationsByLocation(String username, String password, String postal, String country, float maxProximityMiles, boolean discountsOnly);
	
	public PSTokenResponse retokenize(String username, String password, long cardId, String securityCode, String attributes);
	
	public PSTokenResponse createUserEncrypted(String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings, String promoCode);

	public PSTokenResponse registerAnotherCardEncrypted(String username, String password, String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode);

	public PSTokenResponse createUserKeyed(String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings, String promoCode);

	public PSTokenResponse registerAnotherCardKeyed(String username, String password, String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode);
	
	public PSResponse addPromoCode(String username, String password, String promoCode);
	
	public PSTokenResponse registerAnotherCreditCardEncrypted(String username, String password, String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode, String promoCode);
	
	public PSTokenResponse registerAnotherCreditCardKeyed(String username, String password, String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode, String promoCode);

}
