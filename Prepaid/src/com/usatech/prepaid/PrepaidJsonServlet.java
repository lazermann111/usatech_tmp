package com.usatech.prepaid;

import com.caucho.services.server.ServiceContext;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.usatech.prepaid.web.PrepaidUtils;
import com.usatech.ps.*;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrepaidJsonServlet extends GenericServlet implements PrepaidServiceAPI {

    /**
     *
     */
    private static final long serialVersionUID = 2032066418919591454L;

    public static final String JSON_PROTOCOL = "JSON";

    private final Gson gson;

    private final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

    public PrepaidJsonServlet() {
        gson = new Gson();
    }

    @Override
    public void service(final ServletRequest request, final ServletResponse response) throws ServletException,
            IOException {

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;
        final PrintWriter writer = res.getWriter();

        if ("POST".equalsIgnoreCase(req.getMethod())) {

            final ErrorHolder errorHolder = new ErrorHolder();

            if (req.getPathInfo() == null) {
                showError(res, writer);
                writer.flush();
                return;
            }

            final String function = req.getPathInfo().substring(1);

            final String reqBody = readBody(req);
            JsonParser parser = new JsonParser();
            JsonObject jsonObject;
            try {
                ServiceContext.begin(request, req.getPathInfo(), null);
                jsonObject = parser.parse(reqBody).getAsJsonObject();
                PSResponse psResponse = null;
                if (req.getQueryString() == null) {
                    switch (function) {
                        case "createUser": {
                            // String prepaidCardNum,
                            // String prepaidSecurityCode,
                            // String username, String firstname,
                            // String lastname, String address1,
                            // String city, String state,
                            // String postal, String country,
                            // String password, String confirmPassword,
                            // PSConsumerSetting[] consumerSettings

                            String prepaidCardNum = getStringValueFromJson(jsonObject, "prepaidCardNum");
                            String prepaidSecurityCode = getStringValueFromJson(jsonObject, "prepaidSecurityCode");
                            String username = getStringValueFromJson(jsonObject, "username");
                            String firstname = getStringValueFromJson(jsonObject, "firstname");
                            String lastname = getStringValueFromJson(jsonObject, "lastname");
                            String address1 = getStringValueFromJson(jsonObject, "address1");
                            String city = getStringValueFromJson(jsonObject, "city");
                            String state = getStringValueFromJson(jsonObject, "state");
                            String postal = getStringValueFromJson(jsonObject, "postal");
                            String country = getStringValueFromJson(jsonObject, "country");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String confirmPassword = getStringValueFromJson(jsonObject, "confirmPassword");
                            PSConsumerSetting[] consumerSettings = getPsConsumerSettings(jsonObject);

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = createUser(prepaidCardNum,
                                        prepaidSecurityCode, username, firstname, lastname, address1,
                                        city, state, postal, country, password, confirmPassword, consumerSettings);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "createUserWithMobile": {
                            //String prepaidCardNum,
                            //String prepaidSecurityCode,
                            //int preferredCommType,
                            //String email,
                            //String mobile,
                            //int carrierId,
                            //String firstname,
                            //String lastname,
                            //String address1,
                            //String city,
                            //String state,
                            //String postal,
                            //String country,
                            //String password,
                            //String confirmPassword,
                            //PSConsumerSetting[] consumerSettings

                            String prepaidCardNum = getStringValueFromJson(jsonObject, "prepaidCardNum");
                            String prepaidSecurityCode = getStringValueFromJson(jsonObject, "prepaidSecurityCode");

                            int preferredCommType = 0;
                            try {
                                preferredCommType = Integer
                                        .parseInt(getStringValueFromJson(jsonObject, "preferredCommType"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String email = getStringValueFromJson(jsonObject, "email");
                            String mobile = getStringValueFromJson(jsonObject, "mobile");
                            int carrierId = 0;
                            try {
                                carrierId = Integer.parseInt(getStringValueFromJson(jsonObject, "carrierId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String firstname = getStringValueFromJson(jsonObject, "firstname");
                            String lastname = getStringValueFromJson(jsonObject, "lastname");
                            String address1 = getStringValueFromJson(jsonObject, "address1");
                            String city = getStringValueFromJson(jsonObject, "city");
                            String state = getStringValueFromJson(jsonObject, "state");
                            String postal = getStringValueFromJson(jsonObject, "postal");
                            String country = getStringValueFromJson(jsonObject, "country");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String confirmPassword = getStringValueFromJson(jsonObject, "confirmPassword");
                            PSConsumerSetting[] consumerSettings = getPsConsumerSettings(jsonObject);

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = createUserWithMobile(prepaidCardNum, prepaidSecurityCode,
                                        preferredCommType, email, mobile, carrierId, firstname, lastname, address1,
                                        city, state, postal, country, password, confirmPassword, consumerSettings);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getUser": {
                            // String protocol
                            // String username
                            // String password
                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getUser(username, password);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "updateUser": {
                            // String oldUsername,
                            // String username,
                            // String password,
                            // String firstname,
                            // String lastname,
                            // String address1,
                            // String city,
                            // String state,
                            // String postal,
                            // String country,
                            // PSConsumerSetting[] consumerSettings

                            String oldUsername = getStringValueFromJson(jsonObject, "oldUsername");
                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String firstname = getStringValueFromJson(jsonObject, "firstname");
                            String lastname = getStringValueFromJson(jsonObject, "lastname");
                            String address1 = getStringValueFromJson(jsonObject, "address1");
                            String city = getStringValueFromJson(jsonObject, "city");
                            String state = getStringValueFromJson(jsonObject, "state");
                            String postal = getStringValueFromJson(jsonObject, "postal");
                            String country = getStringValueFromJson(jsonObject, "country");
                            PSConsumerSetting[] consumerSettings = getPsConsumerSettings(jsonObject);

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = updateUser(oldUsername, username, password,
                                        firstname, lastname, address1, city, state, postal, country,
                                        consumerSettings);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "updateUserWithMobile": {
                            // String oldUsername,
                            // int preferredCommType,
                            // String email,
                            // String mobile,
                            // int carrierId,
                            // String password,
                            // String firstname,
                            // String lastname,
                            // String address1,
                            // String city,
                            // String state,
                            // String postal,
                            // String country,
                            // PSConsumerSetting[] consumerSettings

                            String oldUsername = getStringValueFromJson(jsonObject, "oldUsername");
                            int preferredCommType = 0;
                            try {
                                preferredCommType = Integer
                                        .parseInt(getStringValueFromJson(jsonObject, "preferredCommType"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String email = getStringValueFromJson(jsonObject, "email");
                            String mobile = getStringValueFromJson(jsonObject, "mobile");
                            int carrierId = 0;
                            try {
                                carrierId = Integer.parseInt(getStringValueFromJson(jsonObject, "carrierId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String password = getStringValueFromJson(jsonObject, "password");
                            String firstname = getStringValueFromJson(jsonObject, "firstname");
                            String lastname = getStringValueFromJson(jsonObject, "lastname");
                            String address1 = getStringValueFromJson(jsonObject, "address1");
                            String city = getStringValueFromJson(jsonObject, "city");
                            String state = getStringValueFromJson(jsonObject, "state");
                            String postal = getStringValueFromJson(jsonObject, "postal");
                            String country = getStringValueFromJson(jsonObject, "country");
                            PSConsumerSetting[] consumerSettings = getPsConsumerSettings(jsonObject);

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = updateUserWithMobile(oldUsername, preferredCommType, email,
                                        mobile, carrierId, password, firstname, lastname, address1, city, state,
                                        postal, country, consumerSettings);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "verifyUser": {
                            // String username,
                            // String password,
                            // String passcode

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String passcode = getStringValueFromJson(jsonObject, "passcode");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = verifyUser(username, password, passcode);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getRegions": {
                            // String username,
                            // String password,
                            // String passcode

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = PSRequestHandler
                                        .getRegions(PrepaidJsonServlet.JSON_PROTOCOL, username, password);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "updateRegion": {
                            // String username,
                            // String password,
                            // int regionId

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            int regionId = 0;
                            try {
                                regionId = Integer.parseInt(getStringValueFromJson(jsonObject, "regionId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = updateRegion(username, password, regionId);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getPrepaidAccounts": {
                            //String username,
                            //String password

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getPrepaidAccounts(username, password);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "registerAnotherCard": {
                            //String username,
                            //String password,
                            //String prepaidCardNum,
                            //String prepaidSecurityCode

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String prepaidCardNum = getStringValueFromJson(jsonObject, "prepaidCardNum");
                            String prepaidSecurityCode = getStringValueFromJson(jsonObject, "prepaidSecurityCode");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = registerAnotherCard(username, password,
                                        prepaidCardNum, prepaidSecurityCode);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "changePassword": {
                            // String protocol,
                            // String username,
                            // String password,
                            // String newPassword,
                            // String newPasswordConfirm

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String newPassword = getStringValueFromJson(jsonObject, "newPassword");
                            String newPasswordConfirm = getStringValueFromJson(jsonObject, "newPasswordConfirm");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = changePassword(username, password, newPassword,
                                        newPasswordConfirm);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getPrepaidActivities": {
                            // String username,
                            // String password,
                            // Long cardId,
                            // Date startTime,
                            // Date endTime,
                            // Integer maxRows

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            Long cardId = null;
                            try {
                            	String stringCardId = getStringValueFromJson(jsonObject, "cardId");
                                if (stringCardId != null) {
                                	cardId = Long.parseLong(stringCardId);
                                }
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Date startTime = null;
                            try {
                                startTime = formatter.parse(getStringValueFromJson(jsonObject, "startTime"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Date endTime = null;
                            try {
                                endTime = formatter.parse(getStringValueFromJson(jsonObject, "endTime"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer maxRows = null;
                            try {
                            	String stringMaxRows = getStringValueFromJson(jsonObject, "maxRows");
                                if (stringMaxRows != null) {
                                	maxRows = Integer.parseInt(stringMaxRows);
                                }
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getPrepaidActivities(username, password, cardId,
                                        startTime, endTime, maxRows);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "setupReplenish": {
                            // String username,
                            // String password,
                            // Long cardId,
                            // String replenishCardNum,
                            // Integer replenishExpMonth,
                            // Integer replenishExpYear,
                            // String replenishSecurityCode,
                            // String address1,
                            // String city,
                            // String state,
                            // String postal,
                            // String country,
                            // BigDecimal amount,
                            // Integer replenishType,
                            // BigDecimal threshhold

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            Long cardId = null;
                            try {
                                cardId = Long.parseLong(getStringValueFromJson(jsonObject, "cardId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String replenishCardNum = getStringValueFromJson(jsonObject, "replenishCardNum");
                            Integer replenishExpMonth = null;
                            try {
                                replenishExpMonth = Integer
                                        .parseInt(getStringValueFromJson(jsonObject, "replenishExpMonth"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer replenishExpYear = null;
                            try {
                                replenishExpYear = Integer
                                        .parseInt(getStringValueFromJson(jsonObject, "replenishExpYear"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String replenishSecurityCode = getStringValueFromJson(jsonObject, "replenishSecurityCode");
                            String address1 = getStringValueFromJson(jsonObject, "address1");
                            String city = getStringValueFromJson(jsonObject, "city");
                            String state = getStringValueFromJson(jsonObject, "state");
                            String postal = getStringValueFromJson(jsonObject, "postal");
                            String country = getStringValueFromJson(jsonObject, "country");
                            BigDecimal amount = null;
                            try {
                                amount = new BigDecimal(
                                        getStringValueFromJson(jsonObject, "amount") != null ? getStringValueFromJson(
                                                jsonObject, "amount") : "0");
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer replenishType = null;
                            try {
                                replenishType = Integer.parseInt(getStringValueFromJson(jsonObject, "replenishType"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            BigDecimal threshold = null;
                            try {
                                threshold = new BigDecimal(
                                        getStringValueFromJson(jsonObject,
                                                "threshold") != null ? getStringValueFromJson(
                                                jsonObject, "threshold") : "0");
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = setupReplenish(username, password, cardId,
                                        replenishCardNum, replenishExpMonth, replenishExpYear,
                                        replenishSecurityCode, address1, city, state, postal, country, amount,
                                        replenishType, threshold);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getReplenishInfo": {
                            // String username,
                            // String password,
                            // Long cardId,
                            // Long replenishId

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            Long cardId = null;
                            try {
                                cardId = Long.parseLong(getStringValueFromJson(jsonObject, "cardId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Long replenishId = null;
                            try {
                                replenishId = Long.parseLong(getStringValueFromJson(jsonObject, "replenishId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getReplenishInfo(username, password, cardId,
                                        replenishId);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "updateReplenish": {
                            // String username,
                            // String password,
                            // Long cardId,
                            // Long replenishId,
                            // BigDecimal amount,
                            // Integer replenishType,
                            // BigDecimal threshold

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            Long cardId = null;
                            try {
                                cardId = Long.parseLong(getStringValueFromJson(jsonObject, "cardId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Long replenishId = null;
                            try {
                                replenishId = Long.parseLong(getStringValueFromJson(jsonObject, "replenishId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            BigDecimal amount = null;
                            try {
                                amount = new BigDecimal(
                                        getStringValueFromJson(jsonObject, "amount") != null ? getStringValueFromJson(
                                                jsonObject, "amount") : "0");
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer replenishType = null;
                            try {
                                replenishType = Integer.parseInt(getStringValueFromJson(jsonObject, "replenishType"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            BigDecimal threshold = null;
                            try {
                                threshold = new BigDecimal(
                                        getStringValueFromJson(jsonObject,
                                                "threshold") != null ? getStringValueFromJson(
                                                jsonObject, "threshold") : "0");
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer replenCount = 10;
                            try {
                                replenCount = Integer.parseInt(getStringValueFromJson(jsonObject, "replenCount"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            if (!errorHolder.isContainsInvalidData())
                                psResponse = updateReplenish(username, password, cardId,
                                        replenishId, amount, replenishType, threshold, replenCount);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "requestReplenish": {
                            // String username,
                            // String password,
                            // Long cardId,
                            // Long replenishId,
                            // BigDecimal amount

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            Long cardId = null;
                            try {
                                cardId = Long.parseLong(getStringValueFromJson(jsonObject, "cardId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Long replenishId = null;
                            try {
                                replenishId = Long.parseLong(getStringValueFromJson(jsonObject, "replenishId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            BigDecimal amount = null;
                            try {
                                amount = new BigDecimal(
                                        getStringValueFromJson(jsonObject, "amount") != null ? getStringValueFromJson(
                                                jsonObject, "amount") : "0");
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = requestReplenish(username, password, cardId,
                                        replenishId, amount);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getPromoCampaigns": {
                            // String username,
                            // String password,

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getPromoCampaigns(username, password);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getLocations": {
                            // String username,
                            // String password,
                            // BigDecimal longitude,
                            // BigDecimal latitude

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            BigDecimal longitude = null;
                            try {
                                longitude = new BigDecimal(
                                        getStringValueFromJson(jsonObject,
                                                "longitude") != null ? getStringValueFromJson(
                                                jsonObject, "longitude") : "0");
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            BigDecimal latitude = null;
                            try {
                                latitude = new BigDecimal(
                                        getStringValueFromJson(jsonObject, "latitude") != null ? getStringValueFromJson(
                                                jsonObject, "latitude") : "0");
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getLocations(username, password, longitude,
                                        latitude);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "resetPassword": {
                            // String username,
                            // String password

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = resetPassword(username, password);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getConsumerSettings": {
                            // String username,
                            // String password

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getConsumerSettings(username, password);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "updateConsumerSettings": {
                            // String username,
                            // String password,
                            // PSConsumerSetting[] consumerSettings

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            PSConsumerSetting[] consumerSettings = getPsConsumerSettings(jsonObject);

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = updateConsumerSettings(username, password,
                                        consumerSettings);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getPostalInfo": {
                            // String postalCd,
                            // String countryCd

                            String postalCd = getStringValueFromJson(jsonObject, "postalCd");
                            String countryCd = getStringValueFromJson(jsonObject, "countryCd");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getPostalInfo(postalCd, countryCd);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getLocationsByUsage": {
                            // String username,
                            // String password

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getLocationsByUsage(username, password);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getReplenishments": {
                            // String username,
                            // String password,
                            // Long cardId,
                            // Date startTime,
                            // Date endTime,
                            // Integer maxRows

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            Long cardId = null;
                            try {
                                cardId = Long.parseLong(getStringValueFromJson(jsonObject, "cardId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Date startTime = null;
                            try {
                                startTime = formatter.parse(getStringValueFromJson(jsonObject, "startTime"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Date endTime = null;
                            try {
                                endTime = formatter.parse(getStringValueFromJson(jsonObject, "endTime"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer maxRows = null;
                            try {
                                maxRows = Integer.parseInt(getStringValueFromJson(jsonObject, "maxRows"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getReplenishments(username, password, cardId,
                                        startTime, endTime, maxRows);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getChargedCards": {
                            // String username,
                            // String password,

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getChargedCards(username, password);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getAuthHolds": {
                            // String username,
                            // String password,
                            // Long cardId

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            Long cardId = null;
                            try {
                                cardId = Long.parseLong(getStringValueFromJson(jsonObject, "cardId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getAuthHolds(username, password, cardId);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "getLocationsByLocation": {
                            // String username,
                            // String password,
                            // String postal,
                            // String country,
                            // float maxProximityMiles,
                            // boolean discountsOnly

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String postal = getStringValueFromJson(jsonObject, "postal");
                            String country = getStringValueFromJson(jsonObject, "country");
                            Float maxProximityMiles = null;
                            try {
                                maxProximityMiles = Float
                                        .parseFloat(getStringValueFromJson(jsonObject, "maxProximityMiles"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            boolean discountsOnly = false;
                            try {
                                discountsOnly = Boolean
                                        .parseBoolean(getStringValueFromJson(jsonObject, "discountsOnly"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = getLocationsByLocation(username, password,
                                        postal, country, maxProximityMiles, discountsOnly);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "retokenize": {
                            // String username,
                            // String password,
                            // long cardId,
                            // String securityCode,
                            // String attributes

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            Long cardId = null;
                            try {
                                cardId = Long.parseLong(getStringValueFromJson(jsonObject, "cardId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String securityCode = getStringValueFromJson(jsonObject, "securityCode");
                            String attributes = getStringValueFromJson(jsonObject, "attributes");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = retokenize(username, password, cardId,
                                        securityCode, attributes);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "createUserEncrypted": {
                            // String entryType,
                            // int cardReaderType,
                            // int decryptedCardDataLen,
                            // String encryptedCardDataHex,
                            // String ksnHex,
                            // String billingCountry,
                            // String billingAddress,
                            // String billingPostalCode,
                            // int preferredCommType,
                            // String email,
                            // String mobile,
                            // int carrierId,
                            // String firstname,
                            // String lastname,
                            // String address1,
                            // String city,
                            // String state,
                            // String postal,
                            // String country,
                            // String password,
                            // String confirmPassword,
                            // PSConsumerSetting[] consumerSettings,
                            // String promoCode

                            String entryType = getStringValueFromJson(jsonObject, "entryType");
                            Integer cardReaderType = null;
                            try {
                                cardReaderType = Integer.parseInt(getStringValueFromJson(jsonObject, "cardReaderType"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer decryptedCardDataLen = null;
                            try {
                                decryptedCardDataLen = Integer
                                        .parseInt(getStringValueFromJson(jsonObject, "decryptedCardDataLen"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String encryptedCardDataHex = getStringValueFromJson(jsonObject, "encryptedCardDataHex");
                            String ksnHex = getStringValueFromJson(jsonObject, "ksnHex");
                            String billingCountry = getStringValueFromJson(jsonObject, "billingCountry");
                            String billingAddress = getStringValueFromJson(jsonObject, "billingAddress");
                            String billingPostalCode = getStringValueFromJson(jsonObject, "billingPostalCode");
                            Integer preferredCommType = null;
                            try {
                                preferredCommType = Integer
                                        .parseInt(getStringValueFromJson(jsonObject, "preferredCommType"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String email = getStringValueFromJson(jsonObject, "email");
                            String mobile = getStringValueFromJson(jsonObject, "mobile");
                            Integer carrierId = null;
                            try {
                                carrierId = Integer.parseInt(getStringValueFromJson(jsonObject, "carrierId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String firstname = getStringValueFromJson(jsonObject, "firstname");
                            String lastname = getStringValueFromJson(jsonObject, "lastname");
                            String address1 = getStringValueFromJson(jsonObject, "address1");
                            String city = getStringValueFromJson(jsonObject, "city");
                            String state = getStringValueFromJson(jsonObject, "state");
                            String postal = getStringValueFromJson(jsonObject, "postal");
                            String country = getStringValueFromJson(jsonObject, "country");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String confirmPassword = getStringValueFromJson(jsonObject, "confirmPassword");
                            PSConsumerSetting[] consumerSettings = getPsConsumerSettings(jsonObject);
                            String promoCode = getStringValueFromJson(jsonObject, "promoCode");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = createUserEncrypted(entryType, cardReaderType,
                                        decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingCountry,
                                        billingAddress, billingPostalCode, preferredCommType, email, mobile,
                                        carrierId, firstname, lastname, address1, city, state, postal, country,
                                        password, confirmPassword, consumerSettings, promoCode);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "registerAnotherCardEncrypted": {
                            // String username,
                            // String password,
                            // String entryType,
                            // int cardReaderType,
                            // int decryptedCardDataLen,
                            // String encryptedCardDataHex,
                            // String ksnHex,
                            // String billingCountry,
                            // String billingAddress,
                            // String billingPostalCode,

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String entryType = getStringValueFromJson(jsonObject, "entryType");
                            Integer cardReaderType = null;
                            try {
                                cardReaderType = Integer.parseInt(getStringValueFromJson(jsonObject, "cardReaderType"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer decryptedCardDataLen = null;
                            try {
                                decryptedCardDataLen = Integer
                                        .parseInt(getStringValueFromJson(jsonObject, "decryptedCardDataLen"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String encryptedCardDataHex = getStringValueFromJson(jsonObject, "encryptedCardDataHex");
                            String ksnHex = getStringValueFromJson(jsonObject, "ksnHex");
                            String billingCountry = getStringValueFromJson(jsonObject, "billingCountry");
                            String billingAddress = getStringValueFromJson(jsonObject, "billingAddress");
                            String billingPostalCode = getStringValueFromJson(jsonObject, "billingPostalCode");
                            String promoCode = getStringValueFromJson(jsonObject, "promoCode");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = registerAnotherCardEncrypted(username, password,
                                        entryType, cardReaderType, decryptedCardDataLen, encryptedCardDataHex,
                                        ksnHex, billingCountry, billingAddress, billingPostalCode);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "createUserKeyed": {
                            // String cardNum,
                            // String securityCode,
                            // Integer expMonth,
                            // Integer expYear,
                            // String billingCountry,
                            // String billingAddress,
                            // String billingPostalCode,
                            // int preferredCommType,
                            // String email,
                            // String mobile,
                            // int carrierId,
                            // String firstname,
                            // String lastname,
                            // String address1,
                            // String city,
                            // String state,
                            // String postal,
                            // String country,
                            // String password,
                            // String confirmPassword,
                            // PSConsumerSetting[] consumerSettings,
                            // String promoCode

                            String cardNum = getStringValueFromJson(jsonObject, "cardNum");
                            String securityCode = getStringValueFromJson(jsonObject, "securityCode");
                            Integer expMonth = null;
                            try {
                                expMonth = Integer.parseInt(getStringValueFromJson(jsonObject, "expMonth"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer expYear = null;
                            try {
                                expMonth = Integer.parseInt(getStringValueFromJson(jsonObject, "expYear"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String billingCountry = getStringValueFromJson(jsonObject, "billingCountry");
                            String billingAddress = getStringValueFromJson(jsonObject, "billingAddress");
                            String billingPostalCode = getStringValueFromJson(jsonObject, "billingPostalCode");
                            Integer preferredCommType = null;
                            try {
                                expMonth = Integer.parseInt(getStringValueFromJson(jsonObject, "preferredCommType"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String email = getStringValueFromJson(jsonObject, "email");
                            String mobile = getStringValueFromJson(jsonObject, "mobile");
                            Integer carrierId = null;
                            try {
                                expMonth = Integer.parseInt(getStringValueFromJson(jsonObject, "carrierId"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String firstname = getStringValueFromJson(jsonObject, "firstname");
                            String lastname = getStringValueFromJson(jsonObject, "lastname");
                            String address1 = getStringValueFromJson(jsonObject, "address1");
                            String city = getStringValueFromJson(jsonObject, "city");
                            String state = getStringValueFromJson(jsonObject, "state");
                            String postal = getStringValueFromJson(jsonObject, "postal");
                            String country = getStringValueFromJson(jsonObject, "country");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String confirmPassword = getStringValueFromJson(jsonObject, "confirmPassword");
                            PSConsumerSetting[] consumerSettings = getPsConsumerSettings(jsonObject);
                            String promoCode = getStringValueFromJson(jsonObject, "promoCode");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = createUserKeyed(cardNum, securityCode, expMonth,
                                        expYear, billingCountry, billingAddress, billingPostalCode,
                                        preferredCommType, email, mobile, carrierId, firstname, lastname, address1,
                                        city, state, postal, country, password, confirmPassword, consumerSettings,
                                        promoCode);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "registerAnotherCardKeyed": {
                            // String username,
                            // String password,
                            // String cardNum,
                            // String securityCode,
                            // Integer expMonth,
                            // Integer expYear,
                            // String billingCountry,
                            // String billingAddress,
                            // String billingPostalCode,
                            // String promoCode

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String cardNum = getStringValueFromJson(jsonObject, "cardNum");
                            String securityCode = getStringValueFromJson(jsonObject, "securityCode");
                            Integer expMonth = null;
                            try {
                                expMonth = Integer.parseInt(getStringValueFromJson(jsonObject, "expMonth"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer expYear = null;
                            try {
                                expMonth = Integer.parseInt(getStringValueFromJson(jsonObject, "expYear"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String billingCountry = getStringValueFromJson(jsonObject, "billingCountry");
                            String billingAddress = getStringValueFromJson(jsonObject, "billingAddress");
                            String billingPostalCode = getStringValueFromJson(jsonObject, "billingPostalCode");
                            String promoCode = getStringValueFromJson(jsonObject, "promoCode");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = registerAnotherCardKeyed(username, password,
                                        cardNum, securityCode, expMonth, expYear, billingCountry, billingAddress,
                                        billingPostalCode);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "addPromoCode": {
                            // String username,
                            // String password,
                            // String promoCode

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String promoCode = getStringValueFromJson(jsonObject, "promoCode");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = addPromoCode(username, password, promoCode);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "registerAnotherCreditCardEncrypted": {
                            // String username,
                            // String password,
                            // String entryType,
                            // int cardReaderType,
                            // int decryptedCardDataLen,
                            // String encryptedCardDataHex,
                            // String ksnHex,
                            // String billingCountry,
                            // String billingAddress,
                            // String billingPostalCode,

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String entryType = getStringValueFromJson(jsonObject, "entryType");
                            Integer cardReaderType = null;
                            try {
                                cardReaderType = Integer.parseInt(getStringValueFromJson(jsonObject, "cardReaderType"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer decryptedCardDataLen = null;
                            try {
                                decryptedCardDataLen = Integer
                                        .parseInt(getStringValueFromJson(jsonObject, "decryptedCardDataLen"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String encryptedCardDataHex = getStringValueFromJson(jsonObject, "encryptedCardDataHex");
                            String ksnHex = getStringValueFromJson(jsonObject, "ksnHex");
                            String billingCountry = getStringValueFromJson(jsonObject, "billingCountry");
                            String billingAddress = getStringValueFromJson(jsonObject, "billingAddress");
                            String billingPostalCode = getStringValueFromJson(jsonObject, "billingPostalCode");
                            String promoCode = getStringValueFromJson(jsonObject, "promoCode");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = registerAnotherCardEncrypted(username, password,
                                        entryType, cardReaderType, decryptedCardDataLen, encryptedCardDataHex,
                                        ksnHex, billingCountry, billingAddress, billingPostalCode);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        case "registerAnotherCreditCardKeyed": {
                            // String username,
                            // String password,
                            // String cardNum,
                            // String securityCode,
                            // Integer expMonth,
                            // Integer expYear,
                            // String billingCountry,
                            // String billingAddress,
                            // String billingPostalCode,
                            // String promoCode

                            String username = getStringValueFromJson(jsonObject, "username");
                            String password = getStringValueFromJson(jsonObject, "password");
                            String cardNum = getStringValueFromJson(jsonObject, "cardNum");
                            String securityCode = getStringValueFromJson(jsonObject, "securityCode");
                            Integer expMonth = null;
                            try {
                                expMonth = Integer.parseInt(getStringValueFromJson(jsonObject, "expMonth"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            Integer expYear = null;
                            try {
                                expMonth = Integer.parseInt(getStringValueFromJson(jsonObject, "expYear"));
                            } catch (NumberFormatException ex) {
                                errorHolder.add(ex.getMessage());
                                errorHolder.setContainsInvalidData(true);
                            }
                            String billingCountry = getStringValueFromJson(jsonObject, "billingCountry");
                            String billingAddress = getStringValueFromJson(jsonObject, "billingAddress");
                            String billingPostalCode = getStringValueFromJson(jsonObject, "billingPostalCode");
                            String promoCode = getStringValueFromJson(jsonObject, "promoCode");

                            if (!errorHolder.isContainsInvalidData())
                                psResponse = registerAnotherCardKeyed(username, password,
                                        cardNum, securityCode, expMonth, expYear, billingCountry, billingAddress,
                                        billingPostalCode);
                            psResponse = checkAndFillResponse(res, psResponse, errorHolder);
                            writer.print(gson.toJson(psResponse));
                        }
                        break;
                        default:
                            showError(res, writer);
                    }
                }
            } catch (Exception ex) {
                showError(res, writer);
            } finally {
                writer.flush();
                ServiceContext.end();
            }
        } else if ("GET".equalsIgnoreCase(req.getMethod())) {
            PrepaidUtils.writeUsageTerms(res, writer);
        }
    }

    private String getStringValueFromJson(JsonObject object, String elementName) {
        String value = null;

        if (object != null && elementName != null) {
            if (object.has(elementName)) {
                value = object.get(elementName).getAsString();
            }
        }
        return value;
    }

    private List<String> getStringArrayValueFromJson(JsonObject object, String elementName) {
        List<String> value = new ArrayList<>();

        if (object != null && elementName != null) {
            if (object.has(elementName)) {
                JsonArray jsonArray = object.get(elementName).getAsJsonArray();
                for (JsonElement jsonElement : jsonArray) {
                    value.add(jsonElement.getAsString());
                }
            }
        }
        return value;
    }

    private PSConsumerSetting[] getPsConsumerSettings(final JsonObject jsonObject) {

        List<PSConsumerSetting> result = new ArrayList<>();

        if (jsonObject.has("consumerSettings")) {
            result.add(getPsConsumerSetting(jsonObject));
        }
        PSConsumerSetting[] retValue = new PSConsumerSetting[result.size()];

        for (int i = 0; i < result.size(); i++) {
            retValue[i] = result.get(i);
        }
        return retValue;
    }

    private PSConsumerSetting getPsConsumerSetting(final JsonObject jsonObject) {
        PSConsumerSetting result = new PSConsumerSetting();

        if (jsonObject.has("settingId")) {
            result.setSettingId(jsonObject.get("settingId").getAsString());
        }
        if (jsonObject.has("value")) {
            result.setValue(jsonObject.get("value").getAsString());
        }
        if (jsonObject.has("label")) {
            result.setLabel(jsonObject.get("label").getAsString());
        }
        if (jsonObject.has("group")) {
            result.setGroup(jsonObject.get("group").getAsString());
        }
        if (jsonObject.has("editor")) {
            result.setEditor(jsonObject.get("editor").getAsString());
        }
        if (jsonObject.has("converter")) {
            result.setConverter(jsonObject.get("converter").getAsString());
        }

        return result;
    }

    private void showError(HttpServletResponse res, final PrintWriter writer) {
        PSResponse response = new PSResponse();
        res.setStatus(400);
        response.setReturnMessage("Fail. Please, refer to the documentation for the proper usage.");
        writer.print(gson.toJson(response));
    }

    private String readBody(HttpServletRequest request) throws IOException {
        StringBuilder ret = new StringBuilder();
        BufferedReader reader = null;
        char[] buffer = new char[128];
        int bytesRead;
        try {
            reader = request.getReader();
            while ((bytesRead = reader.read(buffer)) > 0) {
                ret.append(buffer, 0, bytesRead);
            }
        } finally {
            if (reader != null)
                reader.close();
        }
        return ret.toString();
    }

    private PSResponse checkAndFillResponse(final HttpServletResponse res, PSResponse psResponse,
                                            final ErrorHolder holder) {
        if (psResponse == null)
            psResponse = new PSResponse();
        if (holder != null) {
            if (holder.isContainsInvalidData()) {
                res.setStatus(400);
                psResponse.setReturnMessage("Fail. Please, refer to the documentation for the proper usage.");
                for (String message : holder.getMessages())
                    psResponse.setReturnMessage(psResponse.getReturnMessage() + "; Invalid value: " + message);
            }
        } else throw new NullPointerException();
        return psResponse;
    }

    public PSUser getUser(String username, String password) {
        return PSRequestHandler.getUser(JSON_PROTOCOL, username, password);
    }

    public PSResponse verifyUser(String username, String password, String passcode) {
        return PSRequestHandler.verifyUser(JSON_PROTOCOL, username, password, passcode);
    }

    public PSRegions getRegions(String username, String password) {
        return PSRequestHandler.getRegions(JSON_PROTOCOL, username, password);
    }

    public PSResponse updateRegion(String username, String password, int regionId) {
        return PSRequestHandler.updateRegion(JSON_PROTOCOL, username, password, regionId);
    }

    public PSPrepaidAccounts getPrepaidAccounts(String username, String password) {
        return PSRequestHandler.getPrepaidAccounts(JSON_PROTOCOL, username, password);
    }

    public PSResponse updateUser(String oldUsername, String username, String password, String firstname,
                                 String lastname, String address1, String city, String state, String postal,
                                 String country, PSConsumerSetting[] consumerSettings) {
        return PSRequestHandler
                .updateUser(JSON_PROTOCOL, oldUsername, username, password, firstname, lastname, address1, city, state,
                        postal, country, consumerSettings);
    }

    public PSResponse updateUserWithMobile(String oldUsername, int preferredCommType, String email, String mobile,
                                           int carrierId, String password, String firstname, String lastname,
                                           String address1, String city, String state, String postal, String country,
                                           PSConsumerSetting[] consumerSettings) {
        return PSRequestHandler
                .updateUser(JSON_PROTOCOL, oldUsername, preferredCommType, email, mobile, carrierId, password,
                        firstname,
                        lastname, address1, city, state, postal, country, consumerSettings);
    }

    public PSTokenResponse createUser(String prepaidCardNum, String prepaidSecurityCode, String username,
                                      String firstname, String lastname, String address1, String city, String state,
                                      String postal, String country, String password, String confirmPassword,
                                      PSConsumerSetting[] consumerSettings) {
        return PSRequestHandler
                .createUser(JSON_PROTOCOL, prepaidCardNum, prepaidSecurityCode, username, firstname, lastname, address1,
                        city, state, postal, country, password, confirmPassword, consumerSettings);
    }

    public PSTokenResponse createUserWithMobile(String prepaidCardNum, String prepaidSecurityCode,
                                                int preferredCommType, String email, String mobile, int carrierId,
                                                String firstname, String lastname, String address1, String city,
                                                String state, String postal, String country, String password,
                                                String confirmPassword, PSConsumerSetting[] consumerSettings) {
        return PSRequestHandler
                .createUser(JSON_PROTOCOL, prepaidCardNum, prepaidSecurityCode, preferredCommType, email, mobile,
                        carrierId,
                        firstname, lastname, address1, city, state, postal, country, password, confirmPassword,
                        consumerSettings);
    }

    public PSTokenResponse registerAnotherCard(String username, String password, String prepaidCardNum,
                                               String prepaidSecurityCode) {
        return PSRequestHandler
                .registerAnotherCard(JSON_PROTOCOL, username, password, prepaidCardNum, prepaidSecurityCode);
    }

    public PSResponse changePassword(String username, String password, String newPassword, String newPasswordConfirm) {
        return PSRequestHandler.changePassword(JSON_PROTOCOL, username, password, newPassword, newPasswordConfirm);
    }

    public PSPrepaidActivities getPrepaidActivities(String username, String password, Long cardId, Date startTime,
                                                    Date endTime, Integer maxRows) {
        return PSRequestHandler
                .getPrepaidActivities(JSON_PROTOCOL, username, password, cardId, startTime, endTime, maxRows);
    }

    public PSResponse setupReplenish(String username, String password, Long cardId, String replenishCardNum,
                                     Integer replenishExpMonth, Integer replenishExpYear, String replenishSecurityCode,
                                     String address1, String city, String state, String postal, String country,
                                     BigDecimal amount, Integer replenishType, BigDecimal threshhold) {
        return PSRequestHandler
                .setupReplenish(JSON_PROTOCOL, username, password, cardId, replenishCardNum, replenishExpMonth,
                        replenishExpYear, replenishSecurityCode, address1, city, state, postal, country, amount,
                        replenishType, threshhold);
    }

    public PSReplenishInfoArray getReplenishInfo(String username, String password, Long cardId, Long replenishId) {
        return PSRequestHandler.getReplenishInfo(JSON_PROTOCOL, username, password, cardId, replenishId);
    }

    public PSResponse updateReplenish(String username, String password, Long cardId, Long replenishId,
                                      BigDecimal amount, Integer replenishType, BigDecimal threshhold, Integer replenCount) {
        return PSRequestHandler
                .updateReplenish(JSON_PROTOCOL, username, password, cardId, replenishId, amount, replenishType,
                        threshhold, replenCount);
    }

    public PSResponse requestReplenish(String username, String password, Long cardId, Long replenishId,
                                       BigDecimal amount) {
        return PSRequestHandler.requestReplenish(JSON_PROTOCOL, username, password, cardId, replenishId, amount);
    }

    public PSPromoCampaigns getPromoCampaigns(String username, String password) {
        return PSRequestHandler.getPromoCampaigns(JSON_PROTOCOL, username, password);
    }

    public PSPromoCampaigns getLocations(String username, String password, BigDecimal longitude, BigDecimal latitude) {
        return PSRequestHandler.getLocations(JSON_PROTOCOL, username, password, longitude, latitude);
    }

    public PSResponse resetPassword(String username, String password) {
        return PSRequestHandler.resetPassword(JSON_PROTOCOL, username, password);
    }

    public PSConsumerSettings getConsumerSettings(String username, String password) {
        return PSRequestHandler.getConsumerSettings(JSON_PROTOCOL, username, password);
    }

    public PSResponse updateConsumerSettings(String username, String password, PSConsumerSetting[] consumerSettings) {
        return PSRequestHandler.updateConsumerSettings(JSON_PROTOCOL, username, password, consumerSettings);
    }

    public PSPostalInfo getPostalInfo(String postalCd, String countryCd) {
        return PSRequestHandler.getPostalInfo(JSON_PROTOCOL, postalCd, countryCd);
    }

    public PSPromoCampaigns getLocationsByUsage(String username, String password) {
        return PSRequestHandler.getLocationsByUsage(JSON_PROTOCOL, username, password);
    }

    public PSReplenishmentArray getReplenishments(String username, String password, Long cardId, Date startTime,
                                                  Date endTime, Integer maxRows) {
        return PSRequestHandler
                .getReplenishments(JSON_PROTOCOL, username, password, cardId, startTime, endTime, maxRows);
    }

    public PSChargedCardArray getChargedCards(String username, String password) {
        return PSRequestHandler.getChargedCards(JSON_PROTOCOL, username, password);
    }

    public PSAuthHoldArray getAuthHolds(String username, String password, Long cardId) {
        return PSRequestHandler.getAuthHolds(JSON_PROTOCOL, username, password, cardId);
    }

    public PSPromoCampaigns getLocationsByLocation(String username, String password, String postal, String country,
                                                   float maxProximityMiles, boolean discountsOnly) {
        return PSRequestHandler
                .getLocationsByLocation(JSON_PROTOCOL, username, password, postal, country, maxProximityMiles,
                        discountsOnly);
    }

    public PSTokenResponse retokenize(String username, String password, long cardId, String securityCode,
                                      String attributes) {
        return PSRequestHandler.retokenize(JSON_PROTOCOL, username, password, cardId, securityCode, attributes);
    }

    public PSTokenResponse createUserEncrypted(String entryType, int cardReaderType, int decryptedCardDataLen,
                                               String encryptedCardDataHex, String ksnHex, String billingCountry,
                                               String billingAddress, String billingPostalCode, int preferredCommType,
                                               String email, String mobile, int carrierId, String firstname,
                                               String lastname, String address1, String city, String state,
                                               String postal, String country, String password, String confirmPassword,
                                               PSConsumerSetting[] consumerSettings, String promoCode) {
        return PSRequestHandler
                .createUserEncrypted(JSON_PROTOCOL, entryType, cardReaderType, decryptedCardDataLen,
                        encryptedCardDataHex,
                        ksnHex, billingCountry, billingAddress, billingPostalCode, preferredCommType, email, mobile,
                        carrierId, firstname, lastname, address1, city, state, postal, country, password,
                        confirmPassword, consumerSettings, promoCode);
    }

    public PSTokenResponse registerAnotherCardEncrypted(String username, String password, String entryType,
                                                        int cardReaderType, int decryptedCardDataLen,
                                                        String encryptedCardDataHex, String ksnHex,
                                                        String billingCountry, String billingAddress,
                                                        String billingPostalCode) {
        return PSRequestHandler
                .registerAnotherCardEncrypted(JSON_PROTOCOL, username, password, entryType, cardReaderType,
                        decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingCountry, billingAddress,
                        billingPostalCode);
    }

    public PSTokenResponse createUserKeyed(String cardNum, String securityCode, Integer expMonth, Integer expYear,
                                           String billingCountry, String billingAddress, String billingPostalCode,
                                           int preferredCommType, String email, String mobile, int carrierId,
                                           String firstname, String lastname, String address1, String city,
                                           String state, String postal, String country, String password,
                                           String confirmPassword, PSConsumerSetting[] consumerSettings,
                                           String promoCode) {
        return PSRequestHandler
                .createUserKeyed(JSON_PROTOCOL, cardNum, securityCode, expMonth, expYear, billingCountry,
                        billingAddress,
                        billingPostalCode, preferredCommType, email, mobile, carrierId, firstname, lastname, address1,
                        city, state, postal, country, password, confirmPassword, consumerSettings, promoCode);
    }

    public PSTokenResponse registerAnotherCardKeyed(String username, String password, String cardNum,
                                                    String securityCode, Integer expMonth, Integer expYear,
                                                    String billingCountry, String billingAddress,
                                                    String billingPostalCode) {
        return PSRequestHandler
                .registerAnotherCardKeyed(JSON_PROTOCOL, username, password, cardNum, securityCode, expMonth, expYear,
                        billingCountry, billingAddress, billingPostalCode);
    }

    public PSResponse addPromoCode(String username, String password, String promoCode) {
        return PSRequestHandler.addPromoCode(JSON_PROTOCOL, username, password, promoCode);
    }

    public PSTokenResponse registerAnotherCreditCardEncrypted(String username, String password, String entryType,
                                                              int cardReaderType, int decryptedCardDataLen,
                                                              String encryptedCardDataHex, String ksnHex,
                                                              String billingCountry, String billingAddress,
                                                              String billingPostalCode, String promoCode) {
        return PSRequestHandler
                .registerAnotherCardEncrypted(JSON_PROTOCOL, username, password, entryType, cardReaderType,
                        decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingCountry, billingAddress,
                        billingPostalCode,
                        promoCode);
    }

    public PSTokenResponse registerAnotherCreditCardKeyed(String username, String password, String cardNum,
                                                          String securityCode, Integer expMonth, Integer expYear,
                                                          String billingCountry, String billingAddress,
                                                          String billingPostalCode, String promoCode) {
        return PSRequestHandler
                .registerAnotherCardKeyed(JSON_PROTOCOL, username, password, cardNum, securityCode, expMonth, expYear,
                        billingCountry, billingAddress, billingPostalCode, promoCode);
    }

    private static class ErrorHolder {
        private List<String> messages;
        private boolean containsInvalidData;

        public ErrorHolder() {
            messages = new ArrayList<>();
            containsInvalidData = false;
        }

        public void add(String message) {
            if (message != null && !"".equals(message)) {
                messages.add(message);
            }
        }

        public List<String> getMessages() {
            return messages;
        }

        public boolean isContainsInvalidData() {
            return containsInvalidData;
        }

        public void setContainsInvalidData(boolean value) {
            containsInvalidData = value;
        }
    }
}
