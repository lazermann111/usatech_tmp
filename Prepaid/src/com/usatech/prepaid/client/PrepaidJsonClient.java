package com.usatech.prepaid.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.usatech.ps.PSAuthHoldArray;
import com.usatech.ps.PSChargedCardArray;
import com.usatech.ps.PSConsumerSetting;
import com.usatech.ps.PSConsumerSettings;
import com.usatech.ps.PSPostalInfo;
import com.usatech.ps.PSPrepaidAccounts;
import com.usatech.ps.PSPrepaidActivities;
import com.usatech.ps.PSPromoCampaigns;
import com.usatech.ps.PSRegions;
import com.usatech.ps.PSReplenishInfoArray;
import com.usatech.ps.PSReplenishmentArray;
import com.usatech.ps.PSResponse;
import com.usatech.ps.PSTokenResponse;
import com.usatech.ps.PSUser;
import com.usatech.ps.PrepaidServiceAPI;

public class PrepaidJsonClient implements PrepaidServiceAPI {

    private String fullPath;
    private Gson gson;
    private final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

    public PrepaidJsonClient(String serverAddress) {
        fullPath = addSuffixIfAbsent(serverAddress, "/");;
        gson = new Gson();
    }
    
	private String addSuffixIfAbsent(String object, String suffix) {
		if (!object.endsWith(suffix)) {
			return object + suffix;
		}
		return object;
	}

	public String post(String function, String request) {
		HttpURLConnection conn;
		BufferedReader bufferedReader = null;
		StringBuilder response = new StringBuilder();
		try {
			URL url = new URL(this.fullPath + function);
			conn = (HttpURLConnection)url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestMethod("POST");

			OutputStream outputStream = conn.getOutputStream();
			outputStream.write(request == null ? "".getBytes() : request.getBytes());
			outputStream.flush();
			bufferedReader = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			while ((output = bufferedReader.readLine()) != null) {
				if (response.length() > 0) {
					response.append("\n");
				}
				response.append(output);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException ignored) {
				}
			}
			// Commented to allow Java to use connection pooling when call to openConnection(..)
			//if (conn != null) {
			//conn.disconnect();
			//}
		}
		return response.toString();
	}

    @Override
    public PSUser getUser(String username, String password) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        return gson.fromJson(post("getUser", gson.toJson(request)), PSUser.class);
    }

    @Override
    public PSResponse verifyUser(String username, String password, String passcode) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("passcode", passcode);
        return gson.fromJson(post("verifyUser", gson.toJson(request)), PSResponse.class);
    }

    @Override
    public PSRegions getRegions(String username, String password) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        return gson.fromJson(post("getRegions", gson.toJson(request)), PSRegions.class);
    }

    @Override
    public PSResponse updateRegion(String username, String password, int regionId) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("regionId", regionId);
        return gson.fromJson(post("updateRegion", gson.toJson(request)), PSRegions.class);
    }

    @Override
    public PSPrepaidAccounts getPrepaidAccounts(String username, String password) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        return gson.fromJson(post("getPrepaidAccounts", gson.toJson(request)), PSPrepaidAccounts.class);
    }

    @Override
    public PSResponse updateUser(String oldUsername, String username, String password, String firstname,
                                 String lastname, String address1, String city, String state, String postal,
                                 String country, PSConsumerSetting[] consumerSettings) {
        JsonObject request = new JsonObject();
        request.addProperty("oldUsername", oldUsername);
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("firstname", firstname);
        request.addProperty("lastname", lastname);
        request.addProperty("address1", address1);
        request.addProperty("city", city);
        request.addProperty("state", state);
        request.addProperty("postal", postal);
        request.addProperty("country", country);
        request.addProperty("consumerSettings", gson.toJson(consumerSettings));
        return gson.fromJson(post("updateUser", gson.toJson(request)), PSUser.class);
    }

    @Override
    public PSResponse updateUserWithMobile(String oldUsername, int preferredCommType, String email, String mobile,
                                           int carrierId, String password, String firstname, String lastname,
                                           String address1, String city, String state, String postal, String country,
                                           PSConsumerSetting[] consumerSettings) {
        JsonObject request = new JsonObject();
        request.addProperty("oldUsername", oldUsername);
        request.addProperty("preferredCommType", preferredCommType);
        request.addProperty("email", email);
        request.addProperty("mobile", mobile);
        request.addProperty("carrierId", carrierId);
        request.addProperty("password", password);
        request.addProperty("firstname", firstname);
        request.addProperty("lastname", lastname);
        request.addProperty("address1", address1);
        request.addProperty("city", city);
        request.addProperty("state", state);
        request.addProperty("postal", postal);
        request.addProperty("country", country);
        request.addProperty("consumerSettings", gson.toJson(consumerSettings));
        return gson.fromJson(post("updateUserWithMobile", gson.toJson(request)), PSUser.class);
    }

    @Override
    public PSTokenResponse createUser(String prepaidCardNum, String prepaidSecurityCode, String username,
                                      String firstname, String lastname, String address1, String city, String state,
                                      String postal, String country, String password, String confirmPassword,
                                      PSConsumerSetting[] consumerSettings) {
        JsonObject request = new JsonObject();
        request.addProperty("prepaidCardNum", prepaidCardNum);
        request.addProperty("prepaidSecurityCode", prepaidSecurityCode);
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("firstname", firstname);
        request.addProperty("lastname", lastname);
        request.addProperty("address1", address1);
        request.addProperty("city", city);
        request.addProperty("state", state);
        request.addProperty("postal", postal);
        request.addProperty("country", country);
        request.addProperty("consumerSettings", gson.toJson(consumerSettings));
        return gson.fromJson(post("createUser", gson.toJson(request)), PSTokenResponse.class);
    }

    @Override
    public PSTokenResponse createUserWithMobile(String prepaidCardNum, String prepaidSecurityCode,
                                                int preferredCommType, String email, String mobile, int carrierId,
                                                String firstname, String lastname, String address1, String city,
                                                String state, String postal, String country, String password,
                                                String confirmPassword, PSConsumerSetting[] consumerSettings) {
        JsonObject request = new JsonObject();
        request.addProperty("prepaidCardNum", prepaidCardNum);
        request.addProperty("prepaidSecurityCode", prepaidSecurityCode);
        request.addProperty("preferredCommType", preferredCommType);
        request.addProperty("email", email);
        request.addProperty("mobile", mobile);
        request.addProperty("carrierId", carrierId);
        request.addProperty("firstname", firstname);
        request.addProperty("lastname", lastname);
        request.addProperty("address1", address1);
        request.addProperty("city", city);
        request.addProperty("state", state);
        request.addProperty("postal", postal);
        request.addProperty("country", country);
        request.addProperty("password", password);
        request.addProperty("confirmPassword", confirmPassword);
        request.addProperty("consumerSettings", gson.toJson(consumerSettings));
        return gson.fromJson(post("createUserWithMobile", gson.toJson(request)), PSTokenResponse.class);
    }

    @Override
    public PSTokenResponse registerAnotherCard(String username, String password, String prepaidCardNum,
                                               String prepaidSecurityCode) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("prepaidCardNum", prepaidCardNum);
        request.addProperty("prepaidSecurityCode", prepaidSecurityCode);
        return gson.fromJson(post("registerAnotherCard", gson.toJson(request)), PSTokenResponse.class);
    }

    @Override
    public PSResponse changePassword(String username, String password, String newPassword, String newPasswordConfirm) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("newPassword", newPassword);
        request.addProperty("newPasswordConfirm", newPasswordConfirm);
        return gson.fromJson(post("changePassword", gson.toJson(request)), PSResponse.class);
    }

    @Override
    public PSPrepaidActivities getPrepaidActivities(String username, String password, Long cardId, Date startTime,
                                                    Date endTime, Integer maxRows) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("cardId", cardId);
        request.addProperty("startTime", formatter.format(startTime));
        request.addProperty("endTime", formatter.format(endTime));
        request.addProperty("maxRows", maxRows);
        return gson.fromJson(post("getPrepaidActivities", gson.toJson(request)), PSPrepaidActivities.class);
    }

    @Override
    public PSResponse setupReplenish(String username, String password, Long cardId, String replenishCardNum,
                                     Integer replenishExpMonth, Integer replenishExpYear, String replenishSecurityCode,
                                     String address1, String city, String state, String postal, String country,
                                     BigDecimal amount, Integer replenishType, BigDecimal threshhold) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("cardId", cardId);
        request.addProperty("replenishCardNum", replenishCardNum);
        request.addProperty("replenishExpMonth", replenishExpMonth);
        request.addProperty("replenishExpYear", replenishExpYear);
        request.addProperty("replenishSecurityCode", replenishSecurityCode);
        request.addProperty("address1", address1);
        request.addProperty("city", city);
        request.addProperty("state", state);
        request.addProperty("postal", postal);
        request.addProperty("country", country);
        request.addProperty("amount", amount.toString());
        request.addProperty("replenishType", replenishType);
        request.addProperty("threshhold", threshhold);
        return gson.fromJson(post("setupReplenish", gson.toJson(request)), PSResponse.class);
    }

    @Override
    public PSReplenishInfoArray getReplenishInfo(String username, String password, Long cardId, Long replenishId) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("cardId", cardId);
        request.addProperty("replenishId", replenishId);
        return gson.fromJson(post("getReplenishInfo", gson.toJson(request)), PSReplenishInfoArray.class);
    }

    @Override
    public PSResponse updateReplenish(String username, String password, Long cardId, Long replenishId,
                                      BigDecimal amount, Integer replenishType, BigDecimal threshhold, Integer replenCount) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("cardId", cardId);
        request.addProperty("replenishId", replenishId);
        request.addProperty("amount", amount.toString());
        request.addProperty("replenishType", replenishType);
        request.addProperty("threshhold", threshhold);
        request.addProperty("replenCount", replenCount);
        return gson.fromJson(post("updateReplenish", gson.toJson(request)), PSResponse.class);
    }

    @Override
    public PSResponse requestReplenish(String username, String password, Long cardId, Long replenishId,
                                       BigDecimal amount) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("cardId", cardId);
        request.addProperty("replenishId", replenishId);
        request.addProperty("amount", amount.toString());
        return gson.fromJson(post("requestReplenish", gson.toJson(request)), PSResponse.class);
    }

    @Override
    public PSPromoCampaigns getPromoCampaigns(String username, String password) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        return gson.fromJson(post("getPromoCampaigns", gson.toJson(request)), PSPromoCampaigns.class);
    }

    @Override
    public PSPromoCampaigns getLocations(String username, String password, BigDecimal longitude, BigDecimal latitude) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("longitude", longitude);
        request.addProperty("latitude", latitude);
        return gson.fromJson(post("getLocations", gson.toJson(request)), PSPromoCampaigns.class);
    }

    @Override
    public PSResponse resetPassword(String username, String password) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        return gson.fromJson(post("resetPassword", gson.toJson(request)), PSResponse.class);
    }

    @Override
    public PSConsumerSettings getConsumerSettings(String username, String password) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        return gson.fromJson(post("getConsumerSettings", gson.toJson(request)), PSConsumerSettings.class);
    }

    @Override
    public PSResponse updateConsumerSettings(String username, String password, PSConsumerSetting[] consumerSettings) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("consumerSettings", gson.toJson(consumerSettings));
        return gson.fromJson(post("updateConsumerSettings", gson.toJson(request)), PSResponse.class);
    }

    @Override
    public PSPostalInfo getPostalInfo(String postalCd, String countryCd) {
        JsonObject request = new JsonObject();
        request.addProperty("postalCd", postalCd);
        request.addProperty("countryCd", countryCd);
        return gson.fromJson(post("getPostalInfo", gson.toJson(request)), PSPostalInfo.class);
    }

    @Override
    public PSPromoCampaigns getLocationsByUsage(String username, String password) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        return gson.fromJson(post("getLocationsByUsage", gson.toJson(request)), PSPromoCampaigns.class);
    }

    @Override
    public PSReplenishmentArray getReplenishments(String username, String password, Long cardId, Date startTime,
                                                  Date endTime, Integer maxRows) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("cardId", cardId);
        request.addProperty("startTime", formatter.format(startTime));
        request.addProperty("endTime", formatter.format(endTime));
        request.addProperty("maxRows", maxRows);
        return gson.fromJson(post("getReplenishments", gson.toJson(request)), PSReplenishmentArray.class);
    }

    @Override
    public PSChargedCardArray getChargedCards(String username, String password) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        return gson.fromJson(post("getChargedCards", gson.toJson(request)), PSChargedCardArray.class);
    }

    @Override
    public PSAuthHoldArray getAuthHolds(String username, String password, Long cardId) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("password", cardId);
        return gson.fromJson(post("getAuthHolds", gson.toJson(request)), PSAuthHoldArray.class);
    }

    @Override
    public PSPromoCampaigns getLocationsByLocation(String username, String password, String postal, String country,
                                                   float maxProximityMiles, boolean discountsOnly) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("postal", postal);
        request.addProperty("country", country);
        request.addProperty("maxProximityMiles", maxProximityMiles);
        request.addProperty("discountsOnly", discountsOnly);
        return gson.fromJson(post("getLocationsByLocation", gson.toJson(request)), PSPromoCampaigns.class);
    }

    @Override
    public PSTokenResponse retokenize(String username, String password, long cardId, String securityCode,
                                      String attributes) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("cardId", cardId);
        request.addProperty("securityCode", securityCode);
        request.addProperty("attributes", attributes);
        return gson.fromJson(post("retokenize", gson.toJson(request)), PSTokenResponse.class);
    }

    @Override
    public PSTokenResponse createUserEncrypted(String entryType, int cardReaderType, int decryptedCardDataLen,
                                               String encryptedCardDataHex, String ksnHex, String billingCountry,
                                               String billingAddress, String billingPostalCode, int preferredCommType,
                                               String email, String mobile, int carrierId, String firstname,
                                               String lastname, String address1, String city, String state,
                                               String postal, String country, String password, String confirmPassword,
                                               PSConsumerSetting[] consumerSettings, String promoCode) {
        JsonObject request = new JsonObject();
        request.addProperty("entryType", entryType);
        request.addProperty("cardReaderType", cardReaderType);
        request.addProperty("decryptedCardDataLen", decryptedCardDataLen);
        request.addProperty("encryptedCardDataHex", encryptedCardDataHex);
        request.addProperty("ksnHex", ksnHex);
        request.addProperty("billingCountry", billingCountry);
        request.addProperty("billingAddress", billingAddress);
        request.addProperty("billingPostalCode", billingPostalCode);
        request.addProperty("preferredCommType", preferredCommType);
        request.addProperty("email", email);
        request.addProperty("mobile", mobile);
        request.addProperty("carrierId", carrierId);
        request.addProperty("password", password);
        request.addProperty("firstname", firstname);
        request.addProperty("lastname", lastname);
        request.addProperty("address1", address1);
        request.addProperty("city", city);
        request.addProperty("state", state);
        request.addProperty("postal", postal);
        request.addProperty("country", country);
        request.addProperty("consumerSettings", gson.toJson(consumerSettings));
        request.addProperty("promoCode", promoCode);
        return gson.fromJson(post("createUserEncrypted", gson.toJson(request)), PSTokenResponse.class);
    }

    @Override
    public PSTokenResponse registerAnotherCardEncrypted(String username, String password, String entryType,
                                                        int cardReaderType, int decryptedCardDataLen,
                                                        String encryptedCardDataHex, String ksnHex,
                                                        String billingCountry, String billingAddress,
                                                        String billingPostalCode) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("entryType", entryType);
        request.addProperty("cardReaderType", cardReaderType);
        request.addProperty("decryptedCardDataLen", decryptedCardDataLen);
        request.addProperty("ksnHex", ksnHex);
        request.addProperty("billingCountry", billingCountry);
        request.addProperty("billingAddress", billingAddress);
        request.addProperty("billingPostalCode", billingPostalCode);
        return gson.fromJson(post("registerAnotherCardEncrypted", gson.toJson(request)), PSTokenResponse.class);
    }

    @Override
    public PSTokenResponse createUserKeyed(String cardNum, String securityCode, Integer expMonth, Integer expYear,
                                           String billingCountry, String billingAddress, String billingPostalCode,
                                           int preferredCommType, String email, String mobile, int carrierId,
                                           String firstname, String lastname, String address1, String city,
                                           String state, String postal, String country, String password,
                                           String confirmPassword, PSConsumerSetting[] consumerSettings,
                                           String promoCode) {
        JsonObject request = new JsonObject();
        request.addProperty("cardNum", cardNum);
        request.addProperty("securityCode", securityCode);
        request.addProperty("expMonth", expMonth);
        request.addProperty("expYear", expYear);
        request.addProperty("billingCountry", billingCountry);
        request.addProperty("billingAddress", billingAddress);
        request.addProperty("billingPostalCode", billingPostalCode);
        request.addProperty("preferredCommType", preferredCommType);
        request.addProperty("email", email);
        request.addProperty("mobile", mobile);
        request.addProperty("carrierId", carrierId);
        request.addProperty("firstname", firstname);
        request.addProperty("lastname", lastname);
        request.addProperty("address1", address1);
        request.addProperty("city", city);
        request.addProperty("state", state);
        request.addProperty("postal", postal);
        request.addProperty("country", country);
        request.addProperty("password", password);
        request.addProperty("confirmPassword", confirmPassword);
        request.addProperty("consumerSettings", gson.toJson(consumerSettings));
        request.addProperty("promoCode", promoCode);
        return gson.fromJson(post("createUserKeyed", gson.toJson(request)), PSTokenResponse.class);
    }

    @Override
    public PSTokenResponse registerAnotherCardKeyed(String username, String password, String cardNum,
                                                    String securityCode, Integer expMonth, Integer expYear,
                                                    String billingCountry, String billingAddress,
                                                    String billingPostalCode) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("cardNum", cardNum);
        request.addProperty("securityCode", securityCode);
        request.addProperty("expMonth", expMonth);
        request.addProperty("expYear", expYear);
        request.addProperty("billingCountry", billingCountry);
        request.addProperty("billingAddress", billingAddress);
        request.addProperty("billingPostalCode", billingPostalCode);
        return gson.fromJson(post("registerAnotherCardKeyed", gson.toJson(request)), PSTokenResponse.class);
    }

    @Override
    public PSResponse addPromoCode(String username, String password, String promoCode) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("promoCode", promoCode);
        return gson.fromJson(post("addPromoCode", gson.toJson(request)), PSResponse.class);
    }

    @Override
    public PSTokenResponse registerAnotherCreditCardEncrypted(String username, String password, String entryType,
                                                              int cardReaderType, int decryptedCardDataLen,
                                                              String encryptedCardDataHex, String ksnHex,
                                                              String billingCountry, String billingAddress,
                                                              String billingPostalCode, String promoCode) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("entryType", entryType);
        request.addProperty("cardReaderType", cardReaderType);
        request.addProperty("decryptedCardDataLen", decryptedCardDataLen);
        request.addProperty("encryptedCardDataHex", encryptedCardDataHex);
        request.addProperty("ksnHex", ksnHex);
        request.addProperty("billingCountry", billingCountry);
        request.addProperty("billingAddress", billingAddress);
        request.addProperty("billingPostalCode", billingPostalCode);
        request.addProperty("promoCode", promoCode);
        return gson.fromJson(post("registerAnotherCreditCardEncrypted", gson.toJson(request)), PSTokenResponse.class);
    }

    @Override
    public PSTokenResponse registerAnotherCreditCardKeyed(String username, String password, String cardNum,
                                                          String securityCode, Integer expMonth, Integer expYear,
                                                          String billingCountry, String billingAddress,
                                                          String billingPostalCode, String promoCode) {
        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);
        request.addProperty("cardNum", cardNum);
        request.addProperty("securityCode", securityCode);
        request.addProperty("expMonth", expMonth);
        request.addProperty("expYear", expYear);
        request.addProperty("billingCountry", billingCountry);
        request.addProperty("billingAddress", billingAddress);
        request.addProperty("billingPostalCode", billingPostalCode);
        request.addProperty("promoCode", promoCode);
        return gson.fromJson(post("registerAnotherCreditCardKeyed", gson.toJson(request)), PSTokenResponse.class);
    }
}
