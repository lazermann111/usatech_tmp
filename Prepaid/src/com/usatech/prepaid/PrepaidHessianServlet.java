package com.usatech.prepaid;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.caucho.hessian.server.HessianServlet;
import com.usatech.prepaid.web.PrepaidUtils;
import com.usatech.ps.PSAuthHoldArray;
import com.usatech.ps.PSChargedCardArray;
import com.usatech.ps.PSConsumerSetting;
import com.usatech.ps.PSConsumerSettings;
import com.usatech.ps.PSPostalInfo;
import com.usatech.ps.PSPrepaidAccounts;
import com.usatech.ps.PSPrepaidActivities;
import com.usatech.ps.PSPromoCampaigns;
import com.usatech.ps.PSRegions;
import com.usatech.ps.PSReplenishInfoArray;
import com.usatech.ps.PSReplenishmentArray;
import com.usatech.ps.PSResponse;
import com.usatech.ps.PSUser;
import com.usatech.ps.PrepaidServiceAPI;
import com.usatech.ps.PSTokenResponse;

public class PrepaidHessianServlet extends HessianServlet implements PrepaidServiceAPI {
	private static final long serialVersionUID = -8275431224969529107L;
	
	public static final String HESSIAN_PROTOCOL = "Hessian";
	public static final String HESSIAN_URI = "/hessian/ps";

	public PSUser getUser(String username, String password) {
		return PSRequestHandler.getUser(HESSIAN_PROTOCOL,username, password);
	}	
	
	public PSResponse verifyUser(String username, String password, String passcode){
		return PSRequestHandler.verifyUser(HESSIAN_PROTOCOL,username, password, passcode);
	}
	
	public PSRegions getRegions(String username, String password) {
		return PSRequestHandler.getRegions(HESSIAN_PROTOCOL,username, password);
	}

	public PSResponse updateRegion(String username, String password, int regionId) {
		return PSRequestHandler.updateRegion(HESSIAN_PROTOCOL,username, password, regionId);
	}
	
	public PSPrepaidAccounts getPrepaidAccounts(String username, String password) {
		return PSRequestHandler.getPrepaidAccounts(HESSIAN_PROTOCOL,username, password);
	}
	
	public PSResponse updateUser(String oldUsername,String username, String password, String firstname, String lastname, String address1, String city, String state, String postal, String country,PSConsumerSetting[] consumerSettings){
		return PSRequestHandler.updateUser(HESSIAN_PROTOCOL,oldUsername, username, password, firstname, lastname, address1, city, state, postal, country, consumerSettings);
	}
	
	public PSResponse updateUserWithMobile(String oldUsername,int preferredCommType, String email, String mobile, int carrierId, String password, String firstname, String lastname, String address1, String city, String state, String postal, String country,PSConsumerSetting[] consumerSettings){
		return PSRequestHandler.updateUser(HESSIAN_PROTOCOL,oldUsername, preferredCommType, email, mobile, carrierId, password, firstname, lastname, address1, city, state, postal, country, consumerSettings);
	}
	
	public PSTokenResponse createUser(String prepaidCardNum, String prepaidSecurityCode, String username, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword,PSConsumerSetting[] consumerSettings){
		return PSRequestHandler.createUser(HESSIAN_PROTOCOL,prepaidCardNum, prepaidSecurityCode, username, firstname, lastname, address1, city, state, postal, country, password, confirmPassword, consumerSettings);
	}
	
	public PSTokenResponse createUserWithMobile(String prepaidCardNum, String prepaidSecurityCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings){
		return PSRequestHandler.createUser(HESSIAN_PROTOCOL,prepaidCardNum, prepaidSecurityCode, preferredCommType, email, mobile, carrierId, firstname, lastname, address1, city, state, postal, country, password, confirmPassword, consumerSettings);
	}
	
	public PSTokenResponse registerAnotherCard(String username, String password, String prepaidCardNum, String prepaidSecurityCode){
		return PSRequestHandler.registerAnotherCard(HESSIAN_PROTOCOL,username, password, prepaidCardNum, prepaidSecurityCode); 
	}
	
	public PSResponse changePassword(String username, String password, String newPassword, String newPasswordConfirm){
		return PSRequestHandler.changePassword(HESSIAN_PROTOCOL,username, password, newPassword, newPasswordConfirm); 
	}
	
	public PSPrepaidActivities getPrepaidActivities(String username, String password, Long cardId, Date startTime, Date endTime, Integer maxRows){
		return PSRequestHandler.getPrepaidActivities(HESSIAN_PROTOCOL,username, password, cardId, startTime, endTime, maxRows); 
	}
	
	public PSResponse setupReplenish(String username, String password,Long cardId, String replenishCardNum, Integer replenishExpMonth, Integer replenishExpYear, String replenishSecurityCode, String address1, String city, String state, String postal, String country, BigDecimal amount, Integer replenishType,BigDecimal threshhold){
		return PSRequestHandler.setupReplenish(HESSIAN_PROTOCOL,username, password, cardId, replenishCardNum, replenishExpMonth, replenishExpYear, replenishSecurityCode, address1, city, state, postal, country, amount, replenishType, threshhold);
	}
	
	public PSReplenishInfoArray getReplenishInfo(String username, String password, Long cardId, Long replenishId){
		return PSRequestHandler.getReplenishInfo(HESSIAN_PROTOCOL,username, password, cardId, replenishId);
	}
	
	public PSResponse updateReplenish(String username, String password, Long cardId, Long replenishId, BigDecimal amount, Integer replenishType,BigDecimal threshhold, Integer replenCount){
		return PSRequestHandler.updateReplenish(HESSIAN_PROTOCOL,username, password, cardId, replenishId, amount, replenishType, threshhold, replenCount);
	}
	
	public PSResponse requestReplenish(String username, String password, Long cardId, Long replenishId, BigDecimal amount){
		return PSRequestHandler.requestReplenish(HESSIAN_PROTOCOL,username, password, cardId, replenishId, amount);
	}
	
	public PSPromoCampaigns getPromoCampaigns(String username, String password){
		return PSRequestHandler.getPromoCampaigns(HESSIAN_PROTOCOL,username, password);
	}
	
	public PSPromoCampaigns getLocations(String username, String password, BigDecimal longitude, BigDecimal latitude){
		return PSRequestHandler.getLocations(HESSIAN_PROTOCOL,username, password, longitude, latitude);
	}
	
	public PSResponse resetPassword(String username, String password){
		return PSRequestHandler.resetPassword(HESSIAN_PROTOCOL,username, password);
	}
	
	public PSConsumerSettings getConsumerSettings(String username, String password){
		return PSRequestHandler.getConsumerSettings(HESSIAN_PROTOCOL,username, password);
	}
	
	public PSResponse updateConsumerSettings(String username, String password, PSConsumerSetting[] consumerSettings){
		return PSRequestHandler.updateConsumerSettings(HESSIAN_PROTOCOL,username, password,consumerSettings);
	}
	
	public PSPostalInfo getPostalInfo(String postalCd, String countryCd){
		return PSRequestHandler.getPostalInfo(HESSIAN_PROTOCOL, postalCd, countryCd);
	}
	
	public PSPromoCampaigns getLocationsByUsage(String username, String password){
		return PSRequestHandler.getLocationsByUsage(HESSIAN_PROTOCOL, username, password);
	}
	
	public PSReplenishmentArray getReplenishments(String username, String password, Long cardId, Date startTime, Date endTime, Integer maxRows){
		return PSRequestHandler.getReplenishments(HESSIAN_PROTOCOL,username, password, cardId, startTime, endTime, maxRows); 
	}
	
	public PSChargedCardArray getChargedCards(String username, String password){
		return PSRequestHandler.getChargedCards(HESSIAN_PROTOCOL, username, password);
	}
	
	public PSAuthHoldArray getAuthHolds(String username, String password, Long cardId){
		return PSRequestHandler.getAuthHolds(HESSIAN_PROTOCOL, username, password, cardId);
	}
	
	public PSPromoCampaigns getLocationsByLocation(String username, String password, String postal, String country, float maxProximityMiles, boolean discountsOnly){
		return PSRequestHandler.getLocationsByLocation(HESSIAN_PROTOCOL, username, password, postal, country, maxProximityMiles, discountsOnly);
	}
	public PSTokenResponse retokenize(String username, String password, long cardId, String securityCode, String attributes) {
		return PSRequestHandler.retokenize(HESSIAN_PROTOCOL, username, password, cardId, securityCode, attributes);
	}
	
	public PSTokenResponse createUserEncrypted(String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings, String promoCode){
		return PSRequestHandler.createUserEncrypted(HESSIAN_PROTOCOL, entryType, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingCountry, billingAddress, billingPostalCode, preferredCommType, email, mobile, carrierId, firstname, lastname, address1, city, state, postal, country, password, confirmPassword, consumerSettings, promoCode);
	}

	public PSTokenResponse registerAnotherCardEncrypted(String username, String password, String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode){
		return PSRequestHandler.registerAnotherCardEncrypted(HESSIAN_PROTOCOL, username, password, entryType, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingCountry, billingAddress, billingPostalCode);
	}

	public PSTokenResponse createUserKeyed(String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings, String promoCode){
		return PSRequestHandler.createUserKeyed(HESSIAN_PROTOCOL, cardNum, securityCode, expMonth, expYear, billingCountry, billingAddress, billingPostalCode, preferredCommType, email, mobile, carrierId, firstname, lastname, address1, city, state, postal, country, password, confirmPassword, consumerSettings, promoCode);
	}

	public PSTokenResponse registerAnotherCardKeyed(String username, String password, String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode){
		return PSRequestHandler.registerAnotherCardKeyed(HESSIAN_PROTOCOL, username, password, cardNum, securityCode, expMonth, expYear,billingCountry, billingAddress, billingPostalCode);
	}

	public PSResponse addPromoCode(String username, String password, String promoCode){
		return PSRequestHandler.addPromoCode(HESSIAN_PROTOCOL, username, password, promoCode);
	}
	
	public PSTokenResponse registerAnotherCreditCardEncrypted(String username, String password, String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode, String promoCode){
		return PSRequestHandler.registerAnotherCardEncrypted(HESSIAN_PROTOCOL, username, password, entryType, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingCountry, billingAddress, billingPostalCode, promoCode);
	}
	
	public PSTokenResponse registerAnotherCreditCardKeyed(String username, String password, String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode, String promoCode){
		return PSRequestHandler.registerAnotherCardKeyed(HESSIAN_PROTOCOL, username, password, cardNum, securityCode, expMonth, expYear,billingCountry, billingAddress, billingPostalCode, promoCode);
	}
    
    public void service(ServletRequest request, ServletResponse response) throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest) request;
    	HttpServletResponse res = (HttpServletResponse) response;
        if ("POST".equalsIgnoreCase(req.getMethod())) {
        	if (req.getQueryString() == null)
        		super.service(request, response);
        } else if ("GET".equalsIgnoreCase(req.getMethod())) {
        	if (req.getQueryString() == null)
        		PrepaidUtils.writeUsageTerms(res, res.getWriter());
        	else
        		res.sendRedirect(HESSIAN_URI);
        }
    }
}
