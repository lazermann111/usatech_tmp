package com.usatech.prepaid;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis2.transport.http.AxisServlet;

import com.usatech.prepaid.web.PrepaidUtils;

@SuppressWarnings("serial")
public class PrepaidAxisServlet extends AxisServlet {
	public static final String SOAP_PROTOCOL = "SOAP";
	public static final String SOAP_URI = "/soap/ps";
	protected static final Set<String> ALLOWED_SOAP_URIS = new TreeSet<String>();
	protected static final Set<String> ALLOWED_QUERY_STRINGS = new TreeSet<String>();
	
	static {
		ALLOWED_SOAP_URIS.add(SOAP_URI);
		ALLOWED_QUERY_STRINGS.add("wsdl");
		ALLOWED_QUERY_STRINGS.add("wsdl2");
		ALLOWED_QUERY_STRINGS.add("xsd");
	}

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if ("POST".equalsIgnoreCase(req.getMethod())) {
        	if (ALLOWED_SOAP_URIS.contains(req.getRequestURI()) && req.getQueryString() == null)
    			super.doPost(req, resp);
        } else if ("GET".equalsIgnoreCase(req.getMethod())) {
        	if (ALLOWED_SOAP_URIS.contains(req.getRequestURI())) {
        		if (req.getQueryString() == null)
        			PrepaidUtils.writeUsageTerms(resp, resp.getWriter());
        		else if (ALLOWED_QUERY_STRINGS.contains(req.getQueryString()))
        			super.doGet(req, resp);
        		else        			
        			resp.sendRedirect(SOAP_URI);
        	} else
    			resp.sendRedirect(SOAP_URI);
        }
    }
}
