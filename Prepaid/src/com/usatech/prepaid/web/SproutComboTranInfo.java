package com.usatech.prepaid.web;

import java.util.List;

public class SproutComboTranInfo {
	 boolean hasDiscount;
	 List<SproutComboTran> sproutComboTran;
	public boolean hasDiscount() {
		return hasDiscount;
	}
	public void setHasDiscount(boolean hasDiscount) {
		this.hasDiscount = hasDiscount;
	}
	public List<SproutComboTran> getSproutComboTran() {
		return sproutComboTran;
	}
	public void setSproutComboTran(List<SproutComboTran> sproutComboTran) {
		this.sproutComboTran = sproutComboTran;
	}
	@Override
	public String toString() {
		return "SproutComboTranInfo [hasDiscount=" + hasDiscount
				+ ", sproutComboTran=" + sproutComboTran + "]";
	}
}
