package com.usatech.prepaid.web;

public class ConsumerAccount {
	protected long consumerAcctId;
	protected long consumerId;
	protected String username;
	public long getConsumerAcctId() {
		return consumerAcctId;
	}
	public void setConsumerAcctId(long consumerAcctId) {
		this.consumerAcctId = consumerAcctId;
	}
	public long getConsumerId() {
		return consumerId;
	}
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
