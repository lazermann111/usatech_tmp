package com.usatech.prepaid.web;

public class CreditPromo {
	protected long campaignId;
	protected String campaignName;
	protected String promoCode;
	
	public CreditPromo(long campaignId, String campaignName, String promoCode) {
		super();
		this.campaignId = campaignId;
		this.campaignName = campaignName;
		this.promoCode = promoCode;
	}

	public long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (campaignId ^ (campaignId >>> 32));
		result = prime * result
				+ ((campaignName == null) ? 0 : campaignName.hashCode());
		result = prime * result
				+ ((promoCode == null) ? 0 : promoCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CreditPromo other = (CreditPromo) obj;
		if (campaignId != other.campaignId)
			return false;
		if (campaignName == null) {
			if (other.campaignName != null)
				return false;
		} else if (!campaignName.equals(other.campaignName))
			return false;
		if (promoCode == null) {
			if (other.promoCode != null)
				return false;
		} else if (!promoCode.equals(other.promoCode))
			return false;
		return true;
	}

}
