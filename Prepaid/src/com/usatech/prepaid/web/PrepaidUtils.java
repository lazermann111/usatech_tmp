package com.usatech.prepaid.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.CoteCache;
import simple.io.Log;
import simple.lang.Holder;
import simple.results.BeanException;
import simple.results.Results;
import simple.security.SecureHash;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.ToJspSimpleServlet;
import simple.text.Censor;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;
import simple.xml.sax.MessagesInputSource;

import com.usatech.ec.ECResponse;
import com.usatech.ec2.EC2AuthResponse;
import com.usatech.ec2.EC2ClientUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CommonConstants;
import com.usatech.layers.common.constants.Patterns;
//import com.usatech.layers.common.sprout.SproutUtils;
import com.usatech.layers.common.util.CampaignUtils;
import com.usatech.prepaid.PSRequestHandler;
import com.usatech.ps.PSConsumerAccountBase;
import com.usatech.ps.PSResponse;

import oracle.sql.CHAR;

public class PrepaidUtils {
	private static final Log log = Log.getLog();
	public static final int COMM_TYPE_EMAIL = 1;
	public static final int COMM_TYPE_SMS = 2;
	protected static final Pattern hasUpperCheck = Pattern.compile("[A-Z]");
	protected static final Pattern hasLowerCheck = Pattern.compile("[a-z]");
	protected static final Pattern hasNonAlphaCheck = Pattern.compile("[!-@\\[-^`{-~]");
	protected static final Pattern BASE_URL_PATTERN = Pattern.compile("(?:([^:]+):)?(?://)?([^/?#:]*)(?:\\:(\\d+))?(/[^/#?;.]+)?(?:/(?:.+\\.[^.]+)?)?");
	protected static final Pattern sproutPattern = Pattern.compile("^627722[0-9]{10}$");
	// 1 = protocol, 2 = subdomain, 3 = port, 4 = subdirectory, 5 = filename
	protected static String tranResultSuccess = "Q";
	protected static String tranResultCancel = "C";
	protected static int replenishItemId = 550;
	protected static boolean passwordUpperCaseRequired = true;
	protected static boolean passwordLowerCaseRequired = true;
	protected static boolean passwordNonAlphaRequired = true;
	protected static int passwordMinLength = 8;
	protected static BigDecimal minReplenishAmount = new BigDecimal("20.00");
	protected static BigDecimal maxReplenishAmount = new BigDecimal("200.00");
	protected static BigDecimal minThreshholdAmount = new BigDecimal("20.00");
	protected static BigDecimal initialReplenishAmount = new BigDecimal("0.01");
	protected static BigDecimal[] replenishAmounts;
	protected static BigDecimal[] threshholdAmounts;
	protected static String homePage = "dashboard.html";
	protected static String usaliveRootUrl;
	protected static final CoteCache coteCache = new CoteCache();

	protected static float promoMaxUsedDays = 365.0f;
	protected static float promoNewDays = 5.0f;
	protected static float promoSoonDays = 5.0f;
	protected static float promoMaxProximityMiles = 30.0f;
	protected static int promoMaxTotalRows = 100;
	protected static int promoMaxCampaigns = 10;
	protected static int promoMaxDevicesPerCampaign = 10;
	// STATIC.com.usatech.prepaid.web.PrepaidUtils.virtualPrepaidDeviceSerialCdPrefix=V3-1-
	protected static String virtualPrepaidDeviceSerialCdPrefix = "V3-1-";
	public static final SimpleDateFormat expMonthOnlyFormat = new SimpleDateFormat("yyMM");
	public static final ThreadSafeDateFormat expDateFormat = new ThreadSafeDateFormat(expMonthOnlyFormat);
	protected static PrepaidLogonStep logonStep=new PrepaidLogonStep();
	public static final String PROMO_CODE_CHECK="^[a-zA-Z0-9]{1,20}$";
	//public static SproutUtils sproutUtils;
	public static boolean allowSproutSignUp=false;
	protected static String usaliveBaseUrl;
	protected static String getMoreBaseUrl;
	
	protected static String passDemoUrl;
	private static String passConsumerEmailPostfix;
	protected static String passTypeIdPrefix="pass.com.usatech.more.";
	
	public static final int CARD_TYPE_CREDIT = 1;
	public static final int CARD_TYPE_PREPAID = 2;
	public static final int CARD_TYPE_SPROUT = 3;
	public static final int CARD_TYPE_PAYROLL = 4;
	
	public static String getPassDemoUrl() {
		return passDemoUrl;
	}

	public static void setPassDemoUrl(String passDemoUrl) {
		PrepaidUtils.passDemoUrl = passDemoUrl;
	}


	static{
		logonStep.setRememberUserNameProperty("remember");
		logonStep.setUsingHash(true);
		logonStep.setPrivilegesCall(null);
		logonStep.setCalcTimeZone(true);
	}
	

	protected static final Set<String> dontForwardPaths = new HashSet<String>(Arrays.asList(new String[] {
			"/signin.html",
			"/signout.html",
			"/register_user.html"
		})); 
	
	public static BigDecimal getInitialReplenishAmount() {
		return initialReplenishAmount;
	}
	

	public static void setInitialReplenishAmount(BigDecimal initialReplenishAmount) {
		PrepaidUtils.initialReplenishAmount = initialReplenishAmount;
	}
	/**
	public static SproutUtils getSproutUtils() {
		return sproutUtils;
	}

	public static void setSproutUtils(SproutUtils sproutUtils) {
		PrepaidUtils.sproutUtils = sproutUtils;
	}
	*/

	public static int updateUserPreferences(long consumerId, Connection conn, InputForm form) throws ServletException, SQLException, DataLayerException {
		int[] settingIds = form.getIntArray("settings", false);
		if(settingIds == null)
			return 0;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		int cnt = 0;
		for(int settingId : settingIds) {
			String value = form.getString("setting_" + settingId, false);
			params.put("settingId", settingId);
					params.put("value", value);
			DataLayerMgr.executeUpdate(conn, "UPDATE_CONSUMER_SETTING", params);
			cnt++;
		}
		return cnt;
	}

	public static int updateUserPreferences(long consumerId, Connection conn, Map<? extends Object, ? extends Object> settings) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		int cnt = 0;
		for(Map.Entry<? extends Object, ? extends Object> setting : settings.entrySet()) {
			params.put("settingId", setting.getKey());
			params.put("value", setting.getValue());
			DataLayerMgr.executeUpdate(conn, "UPDATE_CONSUMER_SETTING", params);
			cnt++;
		}
		return cnt;
	}

	public static Results getPromoCampaigns(PrepaidUser user) throws SQLException, DataLayerException {
		return getPromoCampaigns(user.getConsumerId());
	}
	
	public static Results getPromoCampaigns(long consumerId) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("newDays", getPromoNewDays());
		params.put("soonDays", getPromoSoonDays());
		params.put("maxUsedDays", getPromoMaxUsedDays());
		params.put("consumerId", consumerId);
		params.put("maxProximityMiles", getPromoMaxProximityMiles());
		params.put("maxRows", getPromoMaxTotalRows());
		Results results = DataLayerMgr.executeQuery("GET_PROMOS", params);
		return results;
	}
	
	public static CampaignDetails getPromoCampaignDetails(Results r, Translator translator) throws ConvertException{
		String details=null;
		r.setFormat("campaignDiscountPercent", "PERCENT");
		BigDecimal percent = r.getValue("campaignDiscountPercent", BigDecimal.class);
		BigDecimal threshold = r.getValue("campaignThreshold", BigDecimal.class); 
		String title=r.getValue("campaignDescription", String.class);
		Integer nthVendFreeNum = r.getValue("nthVendFreeNum", Integer.class);
		BigDecimal freeVendMaxAmount = r.getValue("freeVendMaxAmount", BigDecimal.class);
		int freeVendMaxCount = r.getValue("freeVendMaxCount", Integer.class);
		BigDecimal purchaseDiscount = r.getValue("purchaseDiscount", BigDecimal.class);
		int campaignTypeId=r.getValue("campaignTypeId", Integer.class);
		String productCampaign = r.getValue("productCampaign", String.class);
		String productsOnly = "";
		String detailsUpdate = "";
		if (productCampaign.equals("Y")){
			productsOnly = "Applied some products only.";
			detailsUpdate = "product";
		}else{
				detailsUpdate = "purchase";
		}
		switch(campaignTypeId) {
			case 2:// Replenish Reward
			case 6:
				if(StringUtils.isBlank(title))
			        title = "Replenish Bonus";
				if(campaignTypeId==2){
					details = translator.translate("prepaid-promo-replenish-reward-detail", "Earn {5,CURRENCY} for every {4,CURRENCY} you add to your card.", 
						title, percent, r.getValue("campaignEndDate"), r.getValue("attentionType"), threshold, threshold.multiply(percent));   
				}else{
					details = translator.translate("prepaid-flat-replenish-reward-detail", "Earn {1,CURRENCY} when you add {0,CURRENCY} to your card.", 
							threshold, purchaseDiscount);   
				}
				break;
			case 3: // Spend Reward
				if(StringUtils.isBlank(title))
	                title = "Bonus Cash";
				details = translator.translate("prepaid-promo-spend-reward-detail", "Earn {5,CURRENCY} Bonus Cash for every {4,CURRENCY} you spend.", 
	            		title, percent, r.getValue("campaignEndDate"), r.getValue("attentionType"), threshold, threshold.multiply(percent));    
				break;
			case 4: // Nth vend free
				if(StringUtils.isBlank(title))
	                title = "Free Vend";
				nthVendFreeNum=nthVendFreeNum-1;
				String promoCodeNthVend=ConvertUtils.getStringSafely(r.getValue("promoCode"), "");
				String freeMaxCount = "";
				if (freeVendMaxCount >=0) {
					freeMaxCount=". Free " + detailsUpdate + " max limit is " + freeVendMaxCount;
				}
				details = translator.translate("prepaid-promo-free-reward-detail", "Earn a free " + detailsUpdate + " up to {1,CURRENCY} for every {2} " + detailsUpdate + freeMaxCount + ". Promo Code: {3}.", 
	            		title, freeVendMaxAmount,nthVendFreeNum,promoCodeNthVend);    
				break;
			default:
				String campaignRecurSchedule=r.getFormattedValue("campaignRecurSchedule");
				String recurSchedText;
	            if(StringUtils.isBlank(campaignRecurSchedule) || campaignRecurSchedule.trim().equalsIgnoreCase("D"))
	                recurSchedText = "";
	            else try {
	                recurSchedText = CampaignUtils.readRecurSchedule(campaignRecurSchedule);
	            } catch(IllegalArgumentException e) {
	            	recurSchedText = ""; //ignore this for now
	            }
	      String promoCode=ConvertUtils.getStringSafely(r.getValue("promoCode"), "");
	      if(campaignTypeId==1){//loyalty discount
	      	if(StringUtils.isBlank(title))
            title = r.getFormattedValue("campaignDiscountPercent") + " off Sale";
					if(!StringUtils.isBlank(promoCode)){
						details = translator.translate("prepaid-promo-campaign-detail", "Every " + detailsUpdate + " is {1,PERCENT} off {4}{3,MATCH,SOON= until {2,DATE,MM/dd/yyyy}} at all participating locations. Promo Code: {5}.", 
			            		title, percent,  r.getValue("campaignEndDate"), r.getValue("attentionType"), recurSchedText, promoCode);  
					}else{
			            details = translator.translate("prepaid-promo-campaign-detail", "Every " + detailsUpdate + " is {1,PERCENT} off {4}{3,MATCH,SOON= until {2,DATE,MM/dd/yyyy}} at all participating locations.", 
			            		title, percent,  r.getValue("campaignEndDate"), r.getValue("attentionType"), recurSchedText);  
					}
	      }else{//5 is purchase discount
	      	if(StringUtils.isBlank(title)){
            title = "Purchase Discount";
	      	}
	      	if (productCampaign.equals("N")){
	      		detailsUpdate = "line item";
	      	}
	      	details = translator.translate("prepaid-purchase-discount-detail", "Every " + detailsUpdate + " in the sale has up to {1,CURRENCY} off {4}{3,MATCH,SOON= until {2,DATE,MM/dd/yyyy}} at all participating locations. Promo Code: {5}.", 
          		title, purchaseDiscount,  r.getValue("campaignEndDate"), r.getValue("attentionType"), recurSchedText, promoCode);  
	      }
	          break;
			}
		return new CampaignDetails(details, title, productsOnly);
	}

	public static Results getLocations(PrepaidUser user, String address1, String city, String state, String postal, String country) throws SQLException, DataLayerException {
		return getLocations(user.getConsumerId(), address1, city, state, postal, country, getPromoMaxProximityMiles(), false);
	}

	public static Results getLocations(long consumerId, String address1, String city, String state, String postal, String country, float maxProximityMiles, boolean discountsOnly) throws SQLException, DataLayerException {
		
		return getLocations(consumerId, address1, city, state, postal, country, maxProximityMiles, discountsOnly, null);
	}
	
	public static Results getLocations(long consumerId, String address1, String city, String state, String postal, String country, float maxProximityMiles, boolean discountsOnly, Long campaignId) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		params.put("maxProximityMiles", maxProximityMiles);
		params.put("address1", address1);
		params.put("city", city);
		params.put("state", state);
		params.put("postal", postal);
		params.put("country", country);
		params.put("maxRows", getPromoMaxTotalRows());
		params.put("newDays", getPromoNewDays());
		params.put("soonDays", getPromoSoonDays());
		if(campaignId!=null){
			params.put("campaignId", campaignId);
		}
		Results results=null;
		if(discountsOnly){
			results = DataLayerMgr.executeQuery("GET_LOCATIONS", params);
		}else{
			results = DataLayerMgr.executeQuery("GET_LOCATIONS_ALL", params);
		}
		return results;
	}
	
	public static Results getRecentLocations(PrepaidUser user) throws SQLException, DataLayerException {
		return getRecentLocations(user.getConsumerId());
	}
	
	public static Results getRecentLocations(long consumerId) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("maxUsedDays", getPromoMaxUsedDays());
		params.put("consumerId", consumerId);
		params.put("maxRows", getPromoMaxTotalRows());
		params.put("newDays", getPromoNewDays());
		params.put("soonDays", getPromoSoonDays());
		Results results = DataLayerMgr.executeQuery("GET_LOCATIONS_BY_USAGE", params);
		return results;
	}

	public static Results getLocationsByCoordinates(long consumerId, BigDecimal longitude, BigDecimal latitude) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		params.put("maxProximityMiles", getPromoMaxProximityMiles());
		params.put("longitude", longitude);
		params.put("latitude", latitude);
		params.put("maxRows", getPromoMaxTotalRows());
		params.put("newDays", getPromoNewDays());
		params.put("soonDays", getPromoSoonDays());
		Results results = DataLayerMgr.executeQuery("GET_LOCATIONS_BY_COORDINATES", params);
		return results;
	}

	public static boolean shouldForwardAfterLogin(String contentPath) {
		if(StringUtils.isBlank(contentPath))
			return false;
		for(String suffix : dontForwardPaths)
			if(contentPath.endsWith(suffix))
				return false;
		return true;
	}

	public static ConsumerAccount createUser(Translator translator, long globalAccountId, String prepaidSecurityCode, String email, String mobile, Integer carrierId, int preferredCommType, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, Long region, String baseUrl, Map<? extends Object, ? extends Object> preferences,
			HttpServletRequest request, int cardType, String expDate, String promoCode, String currencyCd, Long sproutAccountId)
			throws DataLayerException,
			NoSuchAlgorithmException, SQLException, NoSuchProviderException, ConvertException, ServletException, BeanException, ServiceException, ParseException {
		return createUser(translator, globalAccountId, prepaidSecurityCode, email, mobile, carrierId, preferredCommType, firstname, lastname, address1, city, state, postal, country, password, region, baseUrl, preferences, request, cardType, expDate, promoCode, currencyCd,sproutAccountId, null);
	}
	
	public static ConsumerAccount createUser(Translator translator, long globalAccountId, String prepaidSecurityCode, String email, String mobile, Integer carrierId, int preferredCommType, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, Long region, String baseUrl, Map<? extends Object, ? extends Object> preferences,
			HttpServletRequest request, int cardType, String expDate, String promoCode, String currencyCd, Long sproutAccountId, String passSerialNumber)
			throws DataLayerException,
			NoSuchAlgorithmException, SQLException, NoSuchProviderException, ConvertException, ServletException, BeanException, ServiceException, ParseException {
		ConsumerAccount consumerAcct = new ConsumerAccount();
		String credentialAlg = "SHA-256/1000";
		byte[] credentialSalt = SecureHash.getSalt(32);
		byte[] credentialHash = SecureHash.getHash(password.getBytes(), credentialSalt, credentialAlg);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("email", email);
		params.put("mobile", mobile);
		if(carrierId!=null&&carrierId>0){
			params.put("carrierId", carrierId);
		}
		params.put("preferredCommType", preferredCommType);
		params.put("firstname", firstname);
		params.put("lastname", lastname);
		params.put("address1", address1);
		params.put("city", city);
		params.put("state", state);
		params.put("postal", postal);
		params.put("country", country);
		params.put("region", region);

		params.put("passSerialNumber", passSerialNumber);
		params.put("globalAccountId", globalAccountId);
		params.put("securityCode", prepaidSecurityCode);
		params.put("credentialAlg", credentialAlg);
		params.put("credentialSalt", credentialSalt);
		params.put("credentialHash", credentialHash);
		if(preferences == null || preferences.isEmpty())
			preferences = Collections.singletonMap(new Integer(1), "Y");
		if(sproutAccountId!=null){
			params.put("isCredit", "S");
			params.put("currencyCd", "USD");
			params.put("sproutAccountId", sproutAccountId);
		}else{
			if(cardType == CARD_TYPE_CREDIT){
				params.put("isCredit", "Y");
				if(!StringUtils.isBlank(expDate)){
					params.put("expDate", expDateFormat.parse(expDate));
				}
				params.put("currencyCd", currencyCd);
			}
			else if (cardType == CARD_TYPE_PAYROLL)
				params.put("isCredit", "P");
		}
		if(promoCode!=null){
			params.put("promoCode", promoCode);
		}
		Connection conn = DataLayerMgr.getConnectionForCall("CREATE_USER");
		try {
			long consumerId;
			try {
				DataLayerMgr.executeCall(conn, "CREATE_USER", params);
				consumerId = ConvertUtils.getLong(params.get("consumerId"));
				updateUserPreferences(consumerId, conn, preferences);
			} catch(SQLException e) {
				try {
					conn.rollback();
				} catch(SQLException e2) {
					// ignore
				}
				if(log.isWarnEnabled())
					log.warn("Could not register prepaid card", e);
				throw e;
			}
			long consumerAcctId = ConvertUtils.getLong(params.get("consumerAcctId"));
			consumerAcct.setConsumerId(consumerId);
			consumerAcct.setConsumerAcctId(consumerAcctId);
			String passcode = ConvertUtils.getString(params.get("passcode"), true);
			String username = ConvertUtils.getString(params.get("username"), true);
			String communicationEmail = ConvertUtils.getString(params.get("communicationEmail"), true);
			consumerAcct.setUsername(username);
			sendCompleteRegistrationEmail(conn, communicationEmail, preferredCommType, firstname, lastname, consumerId, passcode, baseUrl, request);
			conn.commit();
			if(log.isInfoEnabled())
				log.info("Created new prepaid account for user '" + username + ": consumer_id=" + consumerId + "; consumer_acct_id=" + params.get("consumerAcctId"));
		} finally {
			conn.close();
		}
		return consumerAcct;
	}

	public static ConsumerAccount createUser(String email, String mobile, Integer carrierId, int preferredCommType, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, Long region, String baseUrl, Map<? extends Object, ? extends Object> preferences,
			HttpServletRequest request, String passSerialNumber)
			throws DataLayerException,
			NoSuchAlgorithmException, SQLException, NoSuchProviderException, ConvertException, ServletException, BeanException, ServiceException, ParseException {
		ConsumerAccount consumerAcct = new ConsumerAccount();
		String credentialAlg = "SHA-256/1000";
		byte[] credentialSalt = SecureHash.getSalt(32);
		byte[] credentialHash = SecureHash.getHash(password.getBytes(), credentialSalt, credentialAlg);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("email", email);
		params.put("mobile", mobile);
		if(carrierId!=null&&carrierId>0){
			params.put("carrierId", carrierId);
		}
		params.put("preferredCommType", preferredCommType);
		params.put("firstname", firstname);
		params.put("lastname", lastname);
		params.put("address1", address1);
		params.put("city", city);
		params.put("state", state);
		params.put("postal", postal);
		params.put("country", country);
		params.put("region", region);

		params.put("passSerialNumber", passSerialNumber);
		params.put("credentialAlg", credentialAlg);
		params.put("credentialSalt", credentialSalt);
		params.put("credentialHash", credentialHash);
		if(preferences == null || preferences.isEmpty())
			preferences = Collections.singletonMap(new Integer(1), "Y");
		
		Connection conn = DataLayerMgr.getConnectionForCall("CREATE_USER_NO_CARDS");
		try {
			long consumerId;
			try {
				DataLayerMgr.executeCall(conn, "CREATE_USER_NO_CARDS", params);
				consumerId = ConvertUtils.getLong(params.get("consumerId"));
				updateUserPreferences(consumerId, conn, preferences);
			} catch(SQLException e) {
				try {
					conn.rollback();
				} catch(SQLException e2) {
					// ignore
				}
				if(log.isWarnEnabled())
					log.warn("Could not create user account", e);
				throw e;
			}
			consumerAcct.setConsumerId(consumerId);
			String passcode = ConvertUtils.getString(params.get("passcode"), true);
			String username = ConvertUtils.getString(params.get("username"), true);
			String communicationEmail = ConvertUtils.getString(params.get("communicationEmail"), true);
			consumerAcct.setUsername(username);
			sendCompleteRegistrationEmail(conn, communicationEmail, preferredCommType, firstname, lastname, consumerId, passcode, baseUrl, request);
			conn.commit();
			if(log.isInfoEnabled())
				log.info("Created new user account for '" + username + ": consumer_id=" + consumerId);
		} finally {
			conn.close();
		}
		return consumerAcct;
	}
	
	public static String updateUser(long consumerId,
									String email,
									String mobile,
									Integer carrierId,
									int preferredCommType,
									String firstname,
									String lastname,
									String address1,
									String city,
									String state,
									String postal,
									String country,
									String password,
									Long region,
									Map<?, ?> preferences)
			throws 	DataLayerException,
					NoSuchAlgorithmException,
					SQLException,
					NoSuchProviderException,
					ConvertException,
					ServletException,
					BeanException,
					ServiceException,
					ParseException {
		if(preferences == null || preferences.isEmpty()) {
			preferences = Collections.singletonMap(1, "Y");
		}
		String credentialAlg = "SHA-256/1000";
		byte[] credentialSalt = SecureHash.getSalt(32);
		byte[] credentialHash = SecureHash.getHash(password.getBytes(), credentialSalt, credentialAlg);
		Map<String, Object> params = new HashMap<>();
		params.put("consumerId", consumerId);
		params.put("email", email);
		params.put("mobile", mobile);
		if (carrierId != null && carrierId > 0) {
			params.put("carrierId", carrierId);
		}
		params.put("preferredCommType", preferredCommType);
		params.put("firstname", firstname);
		params.put("lastname", lastname);
		params.put("address1", address1);
		params.put("city", city);
		params.put("state", state);
		params.put("postal", postal);
		params.put("country", country);
		params.put("region", region);

		params.put("credentialAlg", credentialAlg);
		params.put("credentialSalt", credentialSalt);
		params.put("credentialHash", credentialHash);

		Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_USER");
		String passcode = null;
		try {
			try {
				DataLayerMgr.executeCall(conn, "UPDATE_USER", params);
				passcode = ConvertUtils.getString(params.get("passcode"), false);
				if (StringUtils.isBlank(passcode)) {
					DataLayerMgr.executeCall(conn, "GET_PASSCODE", params);
					passcode = ConvertUtils.getString(params.get("passcode"), true);
				}
				updateUserPreferences(consumerId, conn, preferences);
			} catch (SQLException e) {
				try {
					conn.rollback();
				} catch (SQLException e2) {
					// ignore
				}
				if (log.isWarnEnabled())
					log.warn("Could not register prepaid card", e);
				throw e;
			}
			conn.commit();
		} finally {
			conn.close();
		}
		return passcode;
	}
	
	public static ConsumerAccount createUserSimple(Translator translator, long globalAccountId, String prepaidSecurityCode, String firstname, String lastname, Long region, String baseUrl, HttpServletRequest request)
			throws DataLayerException, SQLException, ConvertException {
		ConsumerAccount consumerAcct = new ConsumerAccount();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("firstname", firstname);
		params.put("lastname", lastname);
		params.put("region", region);

		params.put("globalAccountId", globalAccountId);
		params.put("securityCode", prepaidSecurityCode);
		Map<? extends Object, ? extends Object> preferences= Collections.singletonMap(new Integer(1), "Y");
		Connection conn = DataLayerMgr.getConnectionForCall("CREATE_USER_SIMPLE");
		try {
			long consumerId;
			try {
				DataLayerMgr.executeCall(conn, "CREATE_USER_SIMPLE", params);
				consumerId = ConvertUtils.getLong(params.get("consumerId"));
				updateUserPreferences(consumerId, conn, preferences);
			} catch(SQLException e) {
				try {
					conn.rollback();
				} catch(SQLException e2) {
					// ignore
				}
				if(log.isWarnEnabled())
					log.warn("Could not register prepaid card", e);
				throw e;
			}
			long consumerAcctId = ConvertUtils.getLong(params.get("consumerAcctId"));
			consumerAcct.setConsumerId(consumerId);
			consumerAcct.setConsumerAcctId(consumerAcctId);
			conn.commit();
			if(log.isInfoEnabled())
				log.info("Created new prepaid account for consumer_id=" + consumerId + "; consumer_acct_id=" + params.get("consumerAcctId"));
		} finally {
			conn.close();
		}
		return consumerAcct;
	}

	public static void addCard(PrepaidUser user, long globalAccountId, String prepaidSecurityCode) throws DataLayerException, SQLException {
		addCard(user.getConsumerId(), globalAccountId, prepaidSecurityCode);
	}
	
	public static long addCard(long consumerId, long globalAccountId, String prepaidSecurityCode) throws DataLayerException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		params.put("globalAccountId", globalAccountId);
		params.put("securityCode", prepaidSecurityCode);
		DataLayerMgr.executeCall("ADD_CARD", params, true);
		if(log.isInfoEnabled())
			log.info("Added another prepaid account to consumer '" + consumerId + ": consumer_acct_id=" + params.get("consumerAcctId"));
		return ConvertUtils.getLongSafely(params.get("consumerAcctId"), -1);
	}
	
	public static void addCard(long consumerId, long globalAccountId, String prepaidSecurityCode, String nickname) throws DataLayerException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		params.put("globalAccountId", globalAccountId);
		params.put("securityCode", prepaidSecurityCode);
		if(!StringUtils.isBlank(nickname)){
			params.put("nickname", nickname.trim());
		}
		DataLayerMgr.executeCall("ADD_CARD", params, true);
		if(log.isInfoEnabled())
			log.info("Added another prepaid account to consumer '" + consumerId + ": consumer_acct_id=" + params.get("consumerAcctId"));
	}
	
	public static void addCardByPass(String passSerialNumber, long consumerId) throws DataLayerException, SQLException{
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("passSerialNumber", passSerialNumber);
		params.put("consumerId", consumerId);
		DataLayerMgr.executeCall("ADD_CARD_BY_PASS", params, true);
		if(log.isInfoEnabled())
			log.info("Added another prepaid account to consumer by Pass'" + consumerId + ": consumer_acct_id=" + params.get("consumerAcctId"));
	}
	
	public static void addSproutCard(long consumerId, long globalAccountId, String prepaidSecurityCode, String nickname, long sproutAccountId) throws DataLayerException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		params.put("globalAccountId", globalAccountId);
		params.put("securityCode", prepaidSecurityCode);
		if(!StringUtils.isBlank(nickname)){
			params.put("nickname", nickname.trim());
		}
		params.put("sproutAccountId", sproutAccountId);
		DataLayerMgr.executeCall("ADD_SPROUT_ACCT", params, true);
		if(log.isInfoEnabled())
			log.info("Added another sprout account to consumer '" + consumerId + ": consumer_acct_id=" + params.get("consumerAcctId"));
	}
	
	public static void addPayrollDeductCard(long consumerId, long globalAccountId, String prepaidSecurityCode, String nickname) throws DataLayerException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		params.put("globalAccountId", globalAccountId);
		params.put("securityCode", prepaidSecurityCode);
		if(!StringUtils.isBlank(nickname)){
			params.put("nickname", nickname.trim());
		}
		DataLayerMgr.executeCall("ADD_PAYROLL_DEDUCT_CARD", params, true);
		if(log.isInfoEnabled())
			log.info("Added another payroll deduct account to consumer '" + consumerId + ": consumer_acct_id=" + params.get("consumerAcctId"));
	}
	
	public static long addCreditCard(long consumerId, long globalAccountId, String currencyCd, String expDate, String nickname) throws DataLayerException, SQLException, ParseException {
		return addCreditCard(consumerId,globalAccountId,currencyCd, expDate, nickname, null);
	}
	public static long addCreditCard(long consumerId, long globalAccountId, String currencyCd, String expDate, String nickname, String promoCode) throws DataLayerException, SQLException, ParseException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		params.put("globalAccountId", globalAccountId);
		params.put("currencyCd", currencyCd);
		if(!StringUtils.isBlank(expDate)){
			params.put("expDate", expDateFormat.parse(expDate));
		}
		if(!StringUtils.isBlank(nickname)){
			params.put("nickname", nickname.trim());
		}
		if(!StringUtils.isBlank(promoCode)){
			params.put("promoCode", promoCode);
		}
		DataLayerMgr.executeCall("ADD_CREDIT_ACCT", params, true);
		if(log.isInfoEnabled())
			log.info("Added another credit card to consumer '" + consumerId + ": consumer_acct_id=" + params.get("consumerAcctId"));
		return ConvertUtils.getLongSafely(params.get("consumerAcctId"),-1);
	}

	public static void updatePassword(long userId, String passcode, String password) throws DataLayerException, NoSuchAlgorithmException, SQLException, NoSuchProviderException {
		String credentialAlg = "SHA-256/1000";
		byte[] credentialSalt = SecureHash.getSalt(32);
		byte[] credentialHash = SecureHash.getHash(password.getBytes(), credentialSalt, credentialAlg);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		params.put("passcode", passcode);
		params.put("credentialAlg", credentialAlg);
		params.put("credentialSalt", credentialSalt);
		params.put("credentialHash", credentialHash);
		DataLayerMgr.executeCall("UPDATE_PASSWORD_BY_PASSCODE", params, true);
	}

	public static void updatePassword(PrepaidUser user, String password) throws DataLayerException, NoSuchAlgorithmException, SQLException, NoSuchProviderException {
		updatePassword(user.getConsumerId(), password);
	}
	
	public static void updatePassword(long consumerId, String password) throws DataLayerException, NoSuchAlgorithmException, SQLException, NoSuchProviderException {
		String credentialAlg = "SHA-256/1000";
		byte[] credentialSalt = SecureHash.getSalt(32);
		byte[] credentialHash = SecureHash.getHash(password.getBytes(), credentialSalt, credentialAlg);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		params.put("credentialAlg", credentialAlg);
		params.put("credentialSalt", credentialSalt);
		params.put("credentialHash", credentialHash);
		DataLayerMgr.executeCall("UPDATE_PASSWORD", params, true);
	}

	public static void cancelPasscode(long userId, String passcode) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		params.put("passcode", passcode);
		DataLayerMgr.executeCall("CANCEL_PASSCODE", params, true);
	}

	protected static Pattern MOBILE_PATTERN = Pattern.compile("\\s*\\(?\\s*(\\d{3})\\s*\\)?\\s*[-.]?\\s*(\\d{3})\\s*[-.]?\\s*(\\d{4})\\s*");

	public static String checkMobile(String mobile, int carrierId) throws MessagingException {
		// all domestic mobile numbers require 10 digits. Check that first
		Matcher matcher = MOBILE_PATTERN.matcher(mobile);
		if(!matcher.matches())
			throw new AddressException("10 digits required");
		StringBuilder sb = new StringBuilder();
		for(int i = 1; i <= matcher.groupCount(); i++)
			sb.append(matcher.group(i));
		/*
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("carrierId", carrierId);
		DataLayerMgr.selectInto("GET_SMS_SUFFIX", params);
		sb.append(params.get("suffix"));*/
		return sb.toString();
	}

	public static boolean checkPassword(MessagesInputSource mis, String password, String confirm) {
		if(!password.equals(confirm)) {
			mis.addMessage("error", "prepaid-password-not-confirmed", "The password does not match the confirmation. Please re-type both.");
			return false;
		}
		if(password.length() < passwordMinLength) {
			mis.addMessage("error", "prepaid-password-too-short", "The password must be at least " + passwordMinLength + " digits. Please choose another.", passwordMinLength);
			return false;
		}
		if(passwordUpperCaseRequired && !hasUpperCheck.matcher(password).find()) {
			mis.addMessage("error", "prepaid-password-no-uppercase", "The password must contain at least 1 uppercase letter.");
			return false;
		}
		if(passwordLowerCaseRequired && !hasLowerCheck.matcher(password).find()) {
			mis.addMessage("error", "prepaid-password-no-lowercase", "The password must contain at least 1 lowercase letter.");
			return false;
		}
		if(passwordNonAlphaRequired && !hasNonAlphaCheck.matcher(password).find()) {
			mis.addMessage("error", "prepaid-password-no-nonalpha", "The password must contain at least 1 number or 1 punctuation symbol.");
			return false;
		}
		return true;
	}
	
	public static boolean checkPassword(ServletRequest request,PSResponse response, String password, String confirm) {
		if(!password.equals(confirm)) {
			PSRequestHandler.setReturnMessage(request,response, PSRequestHandler.RES_FAILED,"prepaid-password-not-confirmed", "The password does not match the confirmation. Please re-type both.");
			return false;
		}
		if(password.length() < PrepaidUtils.getPasswordMinLength()) {
			PSRequestHandler.setReturnMessage(request,response, PSRequestHandler.RES_FAILED,"prepaid-password-too-short", "The password must be at least " + passwordMinLength + " digits. Please choose another.", passwordMinLength);
			return false;
		}
		if(PrepaidUtils.isPasswordUpperCaseRequired() && !PrepaidUtils.hasUpperCheck.matcher(password).find()) {
			PSRequestHandler.setReturnMessage(request,response, PSRequestHandler.RES_FAILED,"prepaid-password-no-uppercase", "The password must contain at least 1 uppercase letter.");
			return false;
		}
		if(passwordLowerCaseRequired && !hasLowerCheck.matcher(password).find()) {
			PSRequestHandler.setReturnMessage(request,response, PSRequestHandler.RES_FAILED,"prepaid-password-no-lowercase", "The password must contain at least 1 lowercase letter.");
			return false;
		}
		if(passwordNonAlphaRequired && !hasNonAlphaCheck.matcher(password).find()) {
			PSRequestHandler.setReturnMessage(request,response, PSRequestHandler.RES_FAILED,"prepaid-password-no-nonalpha", "The password must contain at least 1 number or 1 punctuation symbol.");
			return false;
		}
		return true;
	}

	public static void setEmailParams(Connection conn, Translator translator, String username, Map<String, Object> params) throws SQLException, DataLayerException {
		String fromEmail = translator.translate("prepaid-email-from-addr", "prepaid@usatech.com");
		String fromName = translator.translate("prepaid-email-from-name", "MORE by USA Technologies");
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("consumerEmailAddr1", username);
		Results results = DataLayerMgr.executeQuery(conn, "GET_CONSUMER_CORP_CUSTOMER", queryParams);
		if (results.next()) {
			String customerName = results.getFormattedValue("customerName").trim();
			if (!StringUtils.isBlank(customerName)) {
				fromEmail = fromEmail.replaceAll("prepaid", customerName.toLowerCase().replaceAll("\\W", "_"));
				fromName = fromName.replaceAll("USA Technologies", customerName);
			}
		}
		params.put("fromEmail", fromEmail);
		params.put("fromName", fromName);
	}
	
	public static Map<String, Object> getCommParams(Connection conn, long consumerId, String baseUrl) throws SQLException, DataLayerException, BeanException, ConvertException, ServiceException  {
		//get the translateUrl, and other things from the database
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		Matcher matcher = BASE_URL_PATTERN.matcher(baseUrl);
		String protocol;
		int port;
		if(matcher.matches()) {// 1 = protocol, 2 = subdomain, 3 = port, 4 = subdirectory, 5 = filename
			params.put("subdomainUrl", matcher.group(2));
			params.put("subdirectory", matcher.group(4));
			if(!StringUtils.isBlank(matcher.group(1)))
				protocol = matcher.group(1);
			else
				protocol = "https";
			if(!StringUtils.isBlank(matcher.group(3)))
				port = ConvertUtils.getIntSafely(matcher.group(3), 0);
			else
				port = 0;			
		} else {
			protocol = "https";
			port = 0;	
		}
		try {
			if (conn == null)
				DataLayerMgr.selectInto("GET_CONSUMER_COMM_DETAILS", params);
			else
				DataLayerMgr.selectInto(conn, "GET_CONSUMER_COMM_DETAILS", params);
		} catch(NotEnoughRowsException e) {
			log.info("Consumer " + consumerId + " is not found");
			throw e;
		}
		String subdomainUrl = ConvertUtils.convertRequired(String.class, params.get("subdomainUrl"));
		String subdirectory = ConvertUtils.convert(String.class, params.get("subdirectory"));
		params.put(CoteCache.class.getName() + ".Retriever", coteCache.createRetriever(subdomainUrl, subdirectory, consumerId));
		Locale locale = ConvertUtils.convertRequired(Locale.class, params.get("locale"));
		StringBuilder baseUrlBuilder = new StringBuilder();
		baseUrlBuilder.append(protocol).append("://").append(subdomainUrl);
		if(port > 0)
			baseUrlBuilder.append(':').append(port);
		if(!StringUtils.isBlank(subdirectory))
			baseUrlBuilder.append(subdirectory);
		baseUrlBuilder.append('/');
		params.put("baseUrl", baseUrlBuilder.toString());

		String translatorUrl = subdomainUrl;
		if(!StringUtils.isBlank(subdirectory))
			translatorUrl = translatorUrl + subdirectory;

		String domain = subdomainUrl;
		String sub = "communication";
		int pos = subdomainUrl.lastIndexOf('.');
		if(pos > 0) {
			pos = subdomainUrl.lastIndexOf('.', pos - 1);
			if(pos > 0) {
				domain = subdomainUrl.substring(pos + 1);
				sub = subdomainUrl.substring(0, pos);
			}
		}
		String customerName = ConvertUtils.convert(String.class, params.get("customerName"));
		Translator translator = TranslatorFactory.getDefaultFactory().getTranslator(locale, translatorUrl);
		String fromEmail = translator.translate("prepaid-email-from-email", "{0,NULL,{0},{1}}@{2}", StringUtils.isBlank(customerName) ? null : customerName.toLowerCase().replaceAll("\\W", "_"), sub, domain);
		String fromName = translator.translate("prepaid-email-from-name", "Prepaid by {0}", StringUtils.isBlank(customerName) ? "USA Technologies" : customerName);

		params.put("fromEmail", fromEmail);
		params.put("fromName", fromName);
		params.put("translator", translator);
		return params;
	}
	
	public static void sendCommunication(long consumerId, String subject, String body, HttpServletRequest request, Connection conn) throws SQLException, DataLayerException, BeanException, ConvertException, ServiceException  {
		String baseUrl = RequestUtils.getBaseUrl(request);
		Map<String, Object> params = getCommParams(conn, consumerId, baseUrl);
		params.put("body", body);
		params.put("subject", subject);
		if (conn == null)
			DataLayerMgr.executeCall("SEND_EMAIL", params, true);
		else
			DataLayerMgr.executeCall(conn, "SEND_EMAIL", params);
	}
	
	public static void sendEmail(String subject, String body, String fromEmail, String fromName, String toEmail, String toName, Connection conn) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("subject", subject);
		params.put("body", body);
		params.put("fromEmail", fromEmail);
		params.put("fromName", fromName);
		params.put("toEmail", toEmail);
		params.put("toName", toName);
		if (conn == null)
			DataLayerMgr.executeCall("SEND_EMAIL", params, true);
		else
			DataLayerMgr.executeCall(conn, "SEND_EMAIL", params);
	}

	public static void sendCompleteRegistrationEmail(Connection conn, String email, int preferredCommType, String firstname, String lastname, long consumerId, String passcode, String baseUrl, HttpServletRequest request) throws SQLException, DataLayerException, ServletException, BeanException, ConvertException, ServiceException {
		Map<String, Object> params = getCommParams(conn, consumerId, baseUrl);
		Translator translator = (Translator) params.get("translator");
		String body;
		if(passcode==null){
			try {
				DataLayerMgr.executeCall(conn, "GET_PASSCODE", params);
			} catch(NotEnoughRowsException e) {
				log.info("Consumer " + consumerId + " is not found");
				throw e;
			}
			passcode=ConvertUtils.getStringSafely(params.get("passcode"));
		}
		params.put("passcode", passcode);
		if(preferredCommType<0){
			preferredCommType=ConvertUtils.getInt(params.get("preferredCommType"), 1);
		}
		switch(preferredCommType) {
			case 2: // SMS
				body = translator.translate("prepaid-registration-sms-body", "To validate your mobile number, please use passcode {0.passcode} or visit {0.baseUrl}register_user.html?userId={0.consumerId}&passcode={0.passcode,PREPARE,URL}", params);
				break;
			default:
				body = translator.translate("prepaid-registration-email-body", "To validate your email address, please use passcode {0.passcode} or visit {0.baseUrl}register_user.html?userId={0.consumerId}&passcode={0.passcode,PREPARE,URL}", params);
		}
		params.put("body", body);
		params.put("subject", translator.translate("prepaid-registration-email-subject", "MORE Card Registration"));
		if(email==null){
			email=ConvertUtils.getStringSafely(params.get("toEmail"));
		}
		if(firstname==null||lastname==null){
			firstname=ConvertUtils.getStringSafely(params.get("firstName"));
			lastname=ConvertUtils.getStringSafely(params.get("lastName"));
		}
		params.put("toEmail", email);
		params.put("toName", firstname + ' ' + lastname);
		DataLayerMgr.executeCall(conn, "SEND_EMAIL", params);
	}
	
	public static void sendPasswordResetEmail(String username, String baseUrl, HttpServletRequest request) throws SQLException, DataLayerException, ConvertException, ServletException, BeanException, ServiceException {
		sendPasswordResetEmail(username, baseUrl, request, 1);
	}

	public static void sendPasswordResetEmail(String username, String baseUrl, HttpServletRequest request, int preferredCommType) throws SQLException, DataLayerException, ConvertException, ServletException, BeanException, ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", username);
		Connection conn = DataLayerMgr.getConnectionForCall("CREATE_PASSCODE_FOR_PASSWORD");
		try {
			try {
				DataLayerMgr.executeCall(conn, "CREATE_PASSCODE_FOR_PASSWORD", params);
			} catch(SQLException e) {
				switch(e.getErrorCode()) {
					case 20320: // Should we send a welcome email?
						log.warn("User requested password reset on an invalid email '" + username + "'; Telling user okay, but not sending an email", e);
						return;
				}
				throw e;
			}
			long consumerId = ConvertUtils.getLong(params.get("userId"));
			String firstname = ConvertUtils.getString(params.get("firstname"), true);
			String lastname = ConvertUtils.getString(params.get("lastname"), true);
			String passcode = ConvertUtils.getString(params.get("passcode"), true);
			params.clear();
			params = getCommParams(conn, consumerId, baseUrl);
			params.put("passcode", passcode);
			Translator translator = (Translator) params.get("translator");
			String body;
			request.setAttribute("consumerId", consumerId);
			params.put("toEmail", params.get("toEmail"));
			switch(preferredCommType) {
				case 2: // SMS
					String fromName=ConvertUtils.getStringSafely(params.get("fromName"));
					fromName=fromName.substring(0, fromName.indexOf("by"));
					params.put("fromName", fromName);
					body = translator.translate("prepaid-reset-password-sms-body", "Please use passcode: {0.passcode} to reset password", params);
					break;
				default:
						body = translator.translate("prepaid-reset-password-sms-body", "Please use passcode: {0.passcode} to reset password (or visit in a browser: {0.baseUrl}edit_password.html?userId={0.consumerId}&passcode={0.passcode,PREPARE,URL}&username={0.userName})", params);
			}
			
			params.put("body", body);
			params.put("subject", translator.translate("prepaid-change-password-email-subject", "Password Reset"));
			params.put("toName", firstname + ' ' + lastname);
			DataLayerMgr.executeCall(conn, "SEND_EMAIL", params);
		} finally {
			try {
				conn.close();
			} catch(SQLException e) {
				//ignore
			}
		}
	}

	public static CardInfo checkCardPermission(PrepaidUser user, long cardId) throws ConvertException, SQLException, DataLayerException, BeanException {
		if(user == null)
			return null;
		return checkCardPermission(user.getConsumerId(), cardId);
	}
	
	public static class CardInfo {
		public final long globalAccountId;
		public final long consumerId;
		public final BigDecimal balance;
		public final String currency;

		public CardInfo(long globalAccountId, long consumerId, BigDecimal balance, String currency) {
			this.globalAccountId = globalAccountId;
			this.consumerId = consumerId;
			this.balance = balance;
			this.currency = currency;
		}
	}

	public static CardInfo checkCardPermission(long consumerId, long cardId) throws ConvertException, SQLException, DataLayerException, BeanException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cardId", cardId);
		params.put("consumerId", consumerId);
		try {
			DataLayerMgr.selectInto("CHECK_CONSUMER_ACCT", params);
		} catch(NotEnoughRowsException e) {
			return null;
		}
		boolean allowed = ConvertUtils.getBoolean(params.get("allowed"), false);
		if(allowed)
			return new CardInfo(ConvertUtils.convert(long.class, params.get("globalAccountId")), ConvertUtils.convert(long.class, params.get("consumerId")), ConvertUtils.convert(BigDecimal.class, params.get("balance")), ConvertUtils.convert(String.class, params.get("currencyCd")));

		return null;
	}

	public static boolean checkLuhn(String accountNumberWithCheckDigit) {
		int[] a = new int[accountNumberWithCheckDigit.length()];
		for(int i = 0; i < a.length; i++)
			a[i] = Character.getNumericValue(accountNumberWithCheckDigit.charAt(i));

		int len = a.length;
		int checksum = 0;
		int temp;

		for(int i = 1; i < len; i++) { // don't include last digit
			temp = a[len - i - 1] * (1 + (i % 2));
			if(temp < 10)
				checksum += temp;
			else
				checksum += temp - 9;
		}
		checksum = (10 - (checksum % 10)) % 10;

		return (a[a.length - 1] == checksum);
	}

	protected static Pattern CVV_PATTERN = Pattern.compile("^\\d{3,4}$");

	public static boolean checkCardNum(String cardNum, Holder<String> cardNumHolder, boolean prepaidCard) {
		if(cardNum != null && (cardNum = cardNum.trim()).length() > 0) {
			StringBuilder sb = new StringBuilder(19);
			for(int i = 0; i < cardNum.length(); i++) {
				char ch = cardNum.charAt(i);
				if(Character.isDigit(ch)) {
					sb.append(ch);
				} else if(Character.isLetter(ch)) {
					return false;
				}
			}
			if(sb.length() < 15 && sb.length() > 19)
				return false;
			
			cardNum = sb.toString();
			if (prepaidCard) {
				if (!cardNum.startsWith(CommonConstants.USAT_IIN))
					return false;
			} else {
				if (cardNum.startsWith(CommonConstants.USAT_IIN))
					return false;
			}				
			cardNumHolder.setValue(cardNum);
			
			// check Luhn
			return checkLuhn(cardNum);
		}
		return true;
	}

	public static boolean checkCvv(String cvv) {
		if(cvv != null && (cvv = cvv.trim()).length() > 0)
			return CVV_PATTERN.matcher(cvv).matches();
		return true;
	}

	public static EC2AuthResponse setupReplenish(long cardId, final Long replenishId, long globalAccountId, long consumerId, int replenishType, BigDecimal amount, BigDecimal threshhold, String pan, String expDate, String cvv, String address1, String postal) throws SQLException, DataLayerException, ConvertException,ParseException {
		int replenishCount = 10; 
		Map<String, Object> replenCountParams = new HashMap<String, Object>(); 
		Results replenCountResult = DataLayerMgr.executeQuery("GET_DAILY_REPLENISH_COUNT", replenCountParams);
		if (replenCountResult.next()){
		    replenishCount = ConvertUtils.getIntSafely(replenCountResult.getFormattedValue("dailyCount"), 0); 
		}
		return setupReplenish(cardId, replenishId, globalAccountId, consumerId, replenishType, amount, threshhold, pan, expDate, cvv, address1, postal, replenishCount); 
	}
	
	public static EC2AuthResponse setupReplenish(long cardId, final Long replenishId, long globalAccountId, long consumerId, int replenishType, BigDecimal amount, BigDecimal threshhold, String pan, String expDate, String cvv, String address1, String postal, int replenishCount) throws SQLException, DataLayerException, ConvertException,ParseException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerAcctId", cardId);
		params.put("replenishType", replenishType);
		params.put("replenishAmount", amount);
		params.put("replenishCount", replenishCount);
		if(replenishId != null)
			params.put("replenishId", replenishId);
		if(replenishType == 1)
			params.put("replenishThreshhold", threshhold);
		params.put("replenishCardNum", maskCardNumber(pan));
		DataLayerMgr.executeCall("SETUP_REPLENISH", params, true);
		boolean replenish = ConvertUtils.getBoolean(params.get("replenishFlag"), false);
		if(!replenish)
			return null;
		params.put("submitted", "F");
		params.put("initial", replenishId == null ? "Y" : "N");
		params.put("expDate", expDateFormat.parse(expDate));
		try {
			BigDecimal saleAmount = ConvertUtils.convertRequired(BigDecimal.class, params.get("replenishAmount"));
			boolean authOnly;
			if(saleAmount.compareTo(BigDecimal.ZERO) <= 0) {
				authOnly = true;
				amount = initialReplenishAmount; 
			} else {
				authOnly = false;
				amount = saleAmount;
			}
			long tranId = ConvertUtils.getLong(params.get("replenishDeviceTranCd"));
			String deviceSerialCd = ConvertUtils.getString(params.get("replenishDeviceSerialCd"), true);
			// DO REPLENISH (AUTH and Queue Sale)
			EC2AuthResponse resp = PrepaidUtils.replenishCard(deviceSerialCd, tranId, globalAccountId, consumerId, pan, expDate, cvv, address1, postal, amount.movePointRight(2).longValue(), authOnly);
			String authResultCd;
			switch(resp.getReturnCode()) {
				case ECResponse.RES_APPROVED: /*APPROVED*/
					authResultCd = "Y";
					break;
				case ECResponse.RES_DECLINED:
				case ECResponse.RES_RESTRICTED_PAYMENT_METHOD:
				case ECResponse.RES_RESTRICTED_DEBIT_CARD_TYPE:
					authResultCd = "N";
					break;
				case ECResponse.RES_PARTIALLY_APPROVED:
					authResultCd = "P";
					break;
				case 0:
				default:
					authResultCd = "F";
					break;
			}
			params.put("submitted", authResultCd);
			if(authOnly)
				resp.setApprovedAmount(0);
			return resp;
		} finally {
			DataLayerMgr.executeCall("UPDATE_PENDING_REPLENISH", params, true);
		}
	}

	public static EC2AuthResponse replenishCard(String deviceSerialCd, long tranId, long globalAccountId, long consumerId, String pan, String expDate, String cvv, String address1, String postal, long amount, boolean authOnly) {
		// USE web service (ePort Connect) to charge card
		// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
		StringBuilder sb = new StringBuilder();
		appendEscapedCardData(sb, pan);
		appendEscapedCardData(sb, expDate);
		appendEscapedCardData(sb, cvv);
		appendEscapedCardData(sb, null);
		appendEscapedCardData(sb, postal);
		appendEscapedCardData(sb, address1, 200);// max 200
		String cardData = sb.toString();

		EC2AuthResponse resp = EC2ClientUtils.getEportConnect().replenishPlain(null, null, deviceSerialCd, tranId, amount, cardData, "N", globalAccountId, consumerId, authOnly ? "authOnly=true\nrequireCVVMatch=true\nrequireAVSMatch=true\n" : "requireCVVMatch=true\nrequireAVSMatch=true\n");
		switch(resp.getReturnCode()) {
			case 2: /*APPROVED*/
				log.info("Replenish approved for " + resp.getApprovedAmount() + ": " + resp.getReturnMessage());
				break;
			case 3:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
				log.info("Replenish was declined: " + resp.getReturnMessage());
				break;
			case 0:
				log.info("Replenish failed: " + resp.getReturnMessage());
				break;
			default:
				log.info("Replenish return unrecognized status (" + resp.getReturnCode() + "): " + resp.getReturnMessage());
				break;
		}
		return resp;
	}

	public static void redirectDialogWithCarryOver(HttpServletRequest request, HttpServletResponse response, String location) throws IOException {
		Object obj = RequestUtils.getMessagesSource(request);
		if(obj != null)
			RequestUtils.storeForNextRequest(request.getSession(), RequestUtils.ATTRIBUTE_MESSAGES, obj);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.print("<script type=\"text/javascript\">window.location.href='");
		out.print(StringUtils.prepareScript(location));
		out.print("';</script>");
		out.println();
		out.flush();
	}

	protected static void appendEscapedCardData(StringBuilder cardDataBuilder, Object piece) {
		appendEscapedCardData(cardDataBuilder, piece, -1);
	}

	protected static void appendEscapedCardData(StringBuilder cardDataBuilder, Object piece, int maxlength) {
		if(cardDataBuilder.length() > 0)
			cardDataBuilder.append('|');
		if(piece != null) {
			if(piece instanceof Number)
				cardDataBuilder.append(piece);
			else {
				String s = piece.toString();
				if(maxlength >= 0 && s.length() > maxlength)
					s = s.substring(0, maxlength);
				cardDataBuilder.append(s.replace('|', ' '));
			}
		}
	}
	protected static final Pattern cardNumberPattern = Pattern.compile("([0-9]{13,})");

	public static String maskCardNumber(String card) {
		if(card == null || (card = card.trim()).length() == 0)
			return "";
		if(card.length() < 13)
			return card;
		Matcher maskPanMatcher = cardNumberPattern.matcher(card);
		if(maskPanMatcher.find()) {
			String unmasked = maskPanMatcher.group(1);
			char[] masked = unmasked.toCharArray();
			Arrays.fill(masked, 6, unmasked.length() - 4, '*');
			return new String(masked);
		}
		return card;
	}
	
	public static void writeUsageTerms(HttpServletResponse response, PrintWriter writer) {
		writeHeader(response, writer);
		writer.println("NOTICE TO USERS<br/><br/>");
		
		writer.println("This computer system is the private property of USA Technologies, Inc.<br/>");   
		writer.println("*** It Is For Authorized Use Only. ***<br/><br/>");

		writer.println("Any or all uses of this system may be intercepted,<br/>");
		writer.println("audited, inspected, and monitored, recorded, disclosed<br/>");
		writer.println("to authorized site, government, and law enforcement personnel, as well<br/>");
		writer.println("as authorized officials of government agencies, both domestic and foreign.<br/>");
		writer.println("By using this system, the user consents to such interception, monitoring,<br/>"); 
		writer.println("recording, auditing, inspection, and disclosure at the discretion of<br/>");
		writer.println("such personnel or officials. Unauthorized or improper use of this system may<br/>");
		writer.println("result in civil and criminal penalties, and administrative or disciplinary<br/>");
		writer.println("action, as appropriate. By continuing to use this system you indicate your<br/>");
		writer.println("awareness of and consent to these terms and conditions of use.<br/><br/>");

		writer.println("LOG OFF IMMEDIATELY if you do not agree to the conditions stated in this warning.<br/><br/>");
		
		writeFooter(response, writer);
	}

	public static void writeHeader(HttpServletResponse response, PrintWriter writer) {
		response.addHeader("Expires", "-1");
		response.setContentType("text/html; charset=utf-8");

		writer.println("<html>");
		writer.println("<title>USA Technologies Prepaid Web Service</title>");
		writer.println("<body>");
		writer.println("<div align=\"center\" style=\"font-family: Arial, Verdana, Tahoma, Sans-Serif;\">");
		writer.println("<h3>USA Technologies Prepaid Web Service</h3>");
	}

	public static void writeFooter(HttpServletResponse response, PrintWriter writer) {
		writer.print("USA Technologies, Inc. - &copy; Copyright 2003-");
		writer.print(Calendar.getInstance().get(Calendar.YEAR));
		writer.println(" - Confidential");

		writer.println("</div>");
		writer.println("</body>");
		writer.println("</html>");

		writer.flush();
	}

	public static int getReplenishItemId() {
		return replenishItemId;
	}

	public static void setReplenishItemId(int replenishItemId) {
		PrepaidUtils.replenishItemId = replenishItemId;
	}

	public static boolean isPasswordUpperCaseRequired() {
		return passwordUpperCaseRequired;
	}

	public static void setPasswordUpperCaseRequired(boolean passwordUpperCaseRequired) {
		PrepaidUtils.passwordUpperCaseRequired = passwordUpperCaseRequired;
	}

	public static boolean isPasswordLowerCaseRequired() {
		return passwordLowerCaseRequired;
	}

	public static void setPasswordLowerCaseRequired(boolean passwordLowerCaseRequired) {
		PrepaidUtils.passwordLowerCaseRequired = passwordLowerCaseRequired;
	}

	public static boolean isPasswordNonAlphaRequired() {
		return passwordNonAlphaRequired;
	}

	public static void setPasswordNonAlphaRequired(boolean passwordNonAlphaRequired) {
		PrepaidUtils.passwordNonAlphaRequired = passwordNonAlphaRequired;
	}

	public static int getPasswordMinLength() {
		return passwordMinLength;
	}

	public static void setPasswordMinLength(int passwordMinLength) {
		PrepaidUtils.passwordMinLength = passwordMinLength;
	}

	public static BigDecimal getMinReplenishAmount() {
		return minReplenishAmount;
	}

	public static void setMinReplenishAmount(BigDecimal minReplenishAmount) {
		PrepaidUtils.minReplenishAmount = minReplenishAmount;
	}

	public static String getTranResultSuccess() {
		return tranResultSuccess;
	}

	public static void setTranResultSuccess(String tranResultSuccess) {
		PrepaidUtils.tranResultSuccess = tranResultSuccess;
	}

	public static String getTranResultCancel() {
		return tranResultCancel;
	}

	public static void setTranResultCancel(String tranResultCancel) {
		PrepaidUtils.tranResultCancel = tranResultCancel;
	}

	public static BigDecimal getMinThreshholdAmount() {
		return minThreshholdAmount;
	}

	public static void setMinThreshholdAmount(BigDecimal minThreshholdAmount) {
		PrepaidUtils.minThreshholdAmount = minThreshholdAmount;
	}

	public static BigDecimal[] getReplenishAmounts() {
		return replenishAmounts;
	}

	public static void setReplenishAmounts(BigDecimal[] replenishAmounts) {
		PrepaidUtils.replenishAmounts = replenishAmounts;
	}

	public static BigDecimal[] getThreshholdAmounts() {
		return threshholdAmounts;
	}

	public static void setThreshholdAmounts(BigDecimal[] threshholdAmounts) {
		PrepaidUtils.threshholdAmounts = threshholdAmounts;
	}

	public static float getPromoMaxUsedDays() {
		return promoMaxUsedDays;
	}

	public static void setPromoMaxUsedDays(float promoMaxUsedDays) {
		PrepaidUtils.promoMaxUsedDays = promoMaxUsedDays;
	}

	public static int getPromoMaxTotalRows() {
		return promoMaxTotalRows;
	}

	public static void setPromoMaxTotalRows(int promoMaxTotalRows) {
		PrepaidUtils.promoMaxTotalRows = promoMaxTotalRows;
	}

	public static int getPromoMaxCampaigns() {
		return promoMaxCampaigns;
	}

	public static void setPromoMaxCampaigns(int promoMaxCampaigns) {
		PrepaidUtils.promoMaxCampaigns = promoMaxCampaigns;
	}

	public static int getPromoMaxDevicesPerCampaign() {
		return promoMaxDevicesPerCampaign;
	}

	public static void setPromoMaxDevicesPerCampaign(int promoMaxDevicesPerCampaign) {
		PrepaidUtils.promoMaxDevicesPerCampaign = promoMaxDevicesPerCampaign;
	}

	public static float getPromoMaxProximityMiles() {
		return promoMaxProximityMiles;
	}

	public static void setPromoMaxProximityMiles(float promoMaxProximityMiles) {
		PrepaidUtils.promoMaxProximityMiles = promoMaxProximityMiles;
	}

	public static String getHomePage() {
		return homePage;
	}

	public static void setHomePage(String defaultPage) {
		PrepaidUtils.homePage = defaultPage;
	}

	public static String getUsaliveRootUrl() {
		return usaliveRootUrl;
	}

	public static void setUsaliveRootUrl(String usaliveRootUrl) {
		PrepaidUtils.usaliveRootUrl = usaliveRootUrl;
	}

	public static BigDecimal getMaxReplenishAmount() {
		return maxReplenishAmount;
	}

	public static void setMaxReplenishAmount(BigDecimal maxReplenishAmount) {
		PrepaidUtils.maxReplenishAmount = maxReplenishAmount;
	}

	public static float getPromoNewDays() {
		return promoNewDays;
	}

	public static void setPromoNewDays(float promoNewDays) {
		PrepaidUtils.promoNewDays = promoNewDays;
	}

	public static float getPromoSoonDays() {
		return promoSoonDays;
	}

	public static void setPromoSoonDays(float promoSoonDays) {
		PrepaidUtils.promoSoonDays = promoSoonDays;
	}
	
	public static String getVirtualPrepaidDeviceSerialCdPrefix() {
		return virtualPrepaidDeviceSerialCdPrefix;
	}
	
	public static String currencyCdToPrepaidDeviceSerialCd(String currencyCd) {
		return getVirtualPrepaidDeviceSerialCdPrefix() + currencyCd;
	}
	
	public static PSConsumerAccountBase consumerAccountBaseForCardNum(String cardNum) throws NoSuchAlgorithmException, SQLException, DataLayerException, BeanException {
		PSConsumerAccountBase consumerAccountBase = new PSConsumerAccountBase();
		byte [] consumerAcctCdHash = SecureHash.getUnsaltedHash(cardNum.getBytes());
		HashMap<String, Object>params = new HashMap<String, Object>();
		params.put("consumerAcctCdHash", consumerAcctCdHash);
		Results results = DataLayerMgr.executeQuery("GET_CONSUMER_ACCT_BASE_BY_CONSUMER_ACCT_CD_HASH", params);
		if (results.next()) {
			results.fillBean(consumerAccountBase);
		}
		return consumerAccountBase;
	}
	
	public static long nextTransactionId(String deviceSerial) throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceSerialCd", deviceSerial);
		DataLayerMgr.executeCall("NEXT_MASTER_ID_BY_SERIAL", params, true);
		long transactionId = ConvertUtils.getLong(params.get("masterId"));
		return transactionId;
	}
	
	protected static final Cache<String, DateFormat[], RuntimeException> standardDateFormatCache = new LockSegmentCache<String, DateFormat[], RuntimeException>() {
		protected DateFormat[] createValue(String key, Object... additionalInfo) throws RuntimeException {
			Locale locale = additionalInfo.length > 0 && additionalInfo[0] != null ? (Locale) additionalInfo[0] : Locale.getDefault();
			TimeZone timeZone = additionalInfo.length > 1 ? (TimeZone) additionalInfo[1] : null;
			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("M/d/yy @ h:mma z", locale);
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy", locale);
			if(timeZone != null)
				dateFormat.setTimeZone(timeZone);
			DateFormatSymbols dfs = new DateFormatSymbols(locale);
			String[] amPmStrings = dfs.getAmPmStrings();
			for(int i = 0; i < amPmStrings.length; i++)
				amPmStrings[i] = amPmStrings[i].toLowerCase();
			dfs.setAmPmStrings(amPmStrings);
			dateTimeFormat.setDateFormatSymbols(dfs);
			return new DateFormat[] { new ThreadSafeDateFormat(dateTimeFormat), new ThreadSafeDateFormat(dateFormat) };
		}
	};

	public static DateFormat[] getStandardDateFormats(Locale locale, TimeZone timeZone) {
		StringBuilder sb = new StringBuilder();
		if(locale != null)
			sb.append(locale);
		sb.append('@');
		if(timeZone != null)
			sb.append(timeZone.getID());
		return standardDateFormatCache.getOrCreate(sb.toString(), locale, timeZone);
	}

	protected static final Comparator<? super Integer> nonPositiveLastComparator = new Comparator<Integer>() {
		public int compare(Integer o1, Integer o2) {
			int i1 = o1.intValue();
			int i2 = o2.intValue();
			if(i1 == i2)
				return 0;
			if(i1 > 0 && i2 <= 0)
				return -1;
			if(i1 <= 0 && i2 > 0)
				return 1;
			if(i1 < i2)
				return -1;
			return 1;
		}
	};

	public static SortedSet<Integer> getRowOptions(Integer selectedRows) {
		SortedSet<Integer> rowsOptions = new TreeSet<Integer>(nonPositiveLastComparator);
		rowsOptions.add(0);
		rowsOptions.add(10);
		rowsOptions.add(20);
		rowsOptions.add(30);
		if(selectedRows != null)
			rowsOptions.add(selectedRows);
		return rowsOptions;
	}

	public static Integer getRowSelection(HttpServletRequest request) throws ServletException {
		Object rows = RequestUtils.getAttribute(request, "rows", false);
		if(rows == null)
			return null;
		if("ALL".equalsIgnoreCase(rows.toString().trim()))
			return 0;
		return ConvertUtils.convertSafely(Integer.class, rows, null);
	}
	
	public static boolean recordSurveyResponse(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
		String surveyDevice = form.getString("survey_device", true);
		if (StringUtils.isBlank(surveyDevice))
			throw new ServletException("Survey device is not specified");
		if (!surveyDevice.matches("[\\w\\-]+"))
			throw new ServletException("Wrong survey device!");
		log.info("Good Survey device: "+surveyDevice);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("surveyId", form.getLong("survey_id", true, 0));		
		params.put("remoteAddr", request.getRemoteAddr());
		params.put("httpUserAgent", request.getHeader("User-Agent"));
		params.put("eportSerialNum", surveyDevice);
		String consumerEmailAddr = form.getString("consumer_email", false);
		if (!StringUtils.isBlank(consumerEmailAddr)) {
			if (!Patterns.EMAIL_ADDRESS_PATTERN.matcher(consumerEmailAddr).matches() 
					|| !consumerEmailAddr.equalsIgnoreCase(Censor.sanitizeText(consumerEmailAddr))) {
				request.setAttribute("error-msg", "Please enter a valid email address");
				return false;
			}
			params.put("consumerEmailAddr", consumerEmailAddr);
		}
		
		Connection conn = null;
		try {
			int answerCount = 0;
			conn = DataLayerMgr.getConnection("report");
			DataLayerMgr.executeUpdate(conn, "RECORD_SURVEY_RESPONSE", params);
			
			String errorMessage = (String) params.get("errorMessage");
			if (errorMessage != null) {
				request.setAttribute("error-msg", errorMessage);
				return false;
			}
			
			Results questions = DataLayerMgr.executeQuery(conn, "GET_SURVEY_QUESTIONS", params);			
			while (questions.next()) {
				String questionResponse = form.getString(new StringBuilder("question").append(questions.getValue("surveyQuestionId")).toString(), false); 
				if (!StringUtils.isBlank(questionResponse)) {
					params.put("surveyQuestionId", questions.getValue("surveyQuestionId"));
					params.put("surveyQuestionResponseValue", Censor.sanitizeText(questionResponse));
					DataLayerMgr.executeUpdate(conn, "RECORD_SURVEY_QUESTION_RESPONSE", params);
					answerCount++;
				}
			}
			
			if (answerCount > 0) {
				params.put("answerCount", answerCount);
				DataLayerMgr.executeUpdate(conn, "UPDATE_SURVEY_RESPONSE", params);
			}
			conn.commit();
		} catch(Exception e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw new ServletException(e);
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}
		
		String redirectStr = new StringBuilder("/survey.html?survey_device=").append(surveyDevice).append("&msg=Thank+you+for+your+feedback!").toString();
		response.sendRedirect(redirectStr);
		return true;
	}

	public static CoteCache getCotecache() {
		return coteCache;
	}

	public static String namePartsToCardHolderName(String first, String middle, String last) {
	    String firstTrimmed = StringUtils.isBlank(first) ? "" : first.trim();
	    String middleTrimmed = StringUtils.isBlank(middle) ? "" : middle.trim();
	    String lastTrimmed = StringUtils.isBlank(last) ? "" : last.trim();
		StringBuffer cardHolder = new StringBuffer(firstTrimmed);
		cardHolder.append(" ");
		cardHolder.append(middleTrimmed);
		if (middleTrimmed.length() > 0) {
			cardHolder.append(" ");
		}
		cardHolder.append(lastTrimmed);
		return cardHolder.toString().trim();
	}

	public static String getSubdirectory(HttpServletRequest request) {
		String replaced = (String) request.getAttribute(ToJspSimpleServlet.REPLACED_PATH_ATTRIBUTE);
		if(StringUtils.isBlank(replaced))
			return null;
		char lastCh = replaced.charAt(replaced.length() - 1);
		switch(lastCh) {
			case '/':
			case '\\':
				return replaced.substring(0, replaced.length() - 1);
			default:
				return replaced;
		}
	}

	public static Results getRegionsForCard(String cardNum) {
		if(StringUtils.isBlank(cardNum))
			return null;
		/*
		long globalAccountId;
		try {
			globalAccountId = EC2ClientUtils.getGlobalAccountId(cardNum);
		} catch(EC2ExecutionException e) {
			log.error("Could not get global account id for card " + maskCardNumber(cardNum), e);
			return null;
		}
		if(globalAccountId <= 0)
			return null;
		*/
		byte[] hash;
		try {
			hash = SecureHash.getUnsaltedHash(cardNum.getBytes());
		} catch(NoSuchAlgorithmException e) {
			log.error("Could not hash card " + maskCardNumber(cardNum), e);
			return null;
		}
		try {
			return DataLayerMgr.executeQuery("GET_REGIONS_FOR_CARD", Collections.singletonMap("cardNumHash", (Object) hash));
		} catch(SQLException  | DataLayerException e) {
			log.error("Could not retrieve regions for card " + maskCardNumber(cardNum), e);
			return null;
		}
	}

	public static Results getRegionsForConsumer(Long consumerId) {
		if(consumerId == null)
			return null;
		try {
			return DataLayerMgr.executeQuery("GET_REGIONS_FOR_CONSUMER", Collections.singletonMap("consumerId", consumerId));
		} catch(SQLException  | DataLayerException e) {
			log.error("Could not retrieve regions for by consumerId =  " + consumerId, e);
			return null;
		}
	}
	
	public static Results getRegionsForPass(String passSerialNumber) {
		if(passSerialNumber == null)
			return null;
		try {
			return DataLayerMgr.executeQuery("GET_REGIONS_FOR_PASS", Collections.singletonMap("passSerialNumber", passSerialNumber));
		} catch(SQLException  | DataLayerException e) {
			log.error("Could not retrieve regions for by passSerialNumber =  " + passSerialNumber, e);
			return null;
		}
	}
	
	public static boolean loginUser(InputForm form, HttpServletRequest request, HttpServletResponse response){
		try {
			logonStep.loginUser(form, request, response);
			return true;
		} catch(ServletException e) {
			log.info("Logon failed");
			return false;
		}
	}
	
	public static void sendDemoPass(String demoEmail) throws SQLException, DataLayerException {
		String body = "Please visit this link to install your MORE loyalty card pass: " + passDemoUrl;
		sendEmail("MORE Loyalty Card", body, "more@usatech.com", "MORE Loyalty", demoEmail, demoEmail, null);
	}
	
	public static String getTrack(String creditCardNum, String creditCardCvv, Integer creditCardExpYear, Integer creditCardExpMonth,String firstname, String lastname, String zip ){
		return getTrack(creditCardNum, creditCardCvv, creditCardExpYear, creditCardExpMonth,firstname+" "+lastname, zip);
	}
	
	public static String getTrack(String creditCardNum, String creditCardCvv, Integer creditCardExpYear, Integer creditCardExpMonth,String fullName, String zip ){
		Format expFormat = ConvertUtils.getFormat("NUMBER:00");
		String expDate=expFormat.format(creditCardExpYear % 100) + expFormat.format(creditCardExpMonth);
		return getTrack(creditCardNum, creditCardCvv, expDate,fullName, zip);
	}
	
	public static String getTrack(String creditCardNum, String creditCardCvv, String expDate,String fullName, String zip ){
		StringBuilder sb = new StringBuilder();
		PrepaidUtils.appendEscapedCardData(sb, creditCardNum);
		PrepaidUtils.appendEscapedCardData(sb, expDate);
		PrepaidUtils.appendEscapedCardData(sb, creditCardCvv);
		appendEscapedCardData(sb, fullName);
		appendEscapedCardData(sb, zip);
		sb.append("|");
		return sb.toString();
	}
	
	public static boolean isSproutCard(String cardNumber){
		Matcher matcher = sproutPattern.matcher(cardNumber);
		if(matcher.matches()){
			return true;
		}else{
			return false;
		}
	}
	/**
	public static SproutComboTranInfo getSproutActivity(long cardId, long consumerId, Date startTime, Date endTime,String sproutAccountId) throws SproutExcpetion, DataLayerException, BeanException, SQLException{
		SproutComboTranInfo sproutTranInfo=new SproutComboTranInfo();
		List<SproutComboTran> result=new ArrayList<SproutComboTran>();
		Date startMonth=DateUtils.truncate(startTime, Calendar.MONTH);
		Date compareEndTime=DateUtils.addDays(endTime, 1);
		Map<String, Object> params=new HashMap<String, Object>();
        params.put("cardId", cardId);
        params.put("consumerId", consumerId);
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        params.put("types", "XFABQY");
		Results results=DataLayerMgr.executeQuery("GET_ACTIVITY_SPROUT", params);
		results.trackAggregate("purchase", Aggregate.SUM);
		results.trackAggregate("replenish", Aggregate.SUM);
		results.trackAggregate("discount", Aggregate.SUM);
		boolean hasDiscounts = (results.getAggregate("discount", null, Aggregate.SUM) != null);
		sproutTranInfo.setHasDiscount(hasDiscounts);
        HashMap<String, SproutUSATTran> sproutTranMap=new HashMap<String, SproutUSATTran>();
        while(results.next()){
        	SproutUSATTran tranInfo=new SproutUSATTran();
        	results.fillBean(tranInfo);
        	sproutTranMap.put(tranInfo.getExternalReferenceId(), tranInfo);
        }
		while(startMonth.before(endTime)){
			System.out.println("getting startMonth:"+startMonth);
			SproutAccountTransactions trans=sproutUtils.getAccountTransactions(sproutAccountId, SproutUtils.sproutMonthFormat.format(startMonth));
			List<SproutTransaction> tranList=trans.getTransactions();
			if(tranList!=null&&tranList.size()>0){
				for(SproutTransaction tran: tranList){
					if(tran.getCreatedDate().before(compareEndTime)&&(tran.getCreatedDate().after(startTime))){
						result.add(new SproutComboTran(tran, sproutTranMap.get(tran.getExternal_reference_id())));
					}
				}
			}
			startMonth=DateUtils.truncate(DateUtils.addMonths(startMonth, 1), Calendar.MONTH);
		}
		sproutTranInfo.setSproutComboTran(result);
		return sproutTranInfo;
	}
	*/
	
	
	public static void createUserCredential(long consumerId, String password, String address1, String birthday, Integer carrierId)
			throws DataLayerException,
			NoSuchAlgorithmException, SQLException, NoSuchProviderException, ConvertException, ServletException, BeanException, ServiceException, ParseException {
		String credentialAlg = "SHA-256/1000";
		byte[] credentialSalt = SecureHash.getSalt(32);
		byte[] credentialHash = SecureHash.getHash(password.getBytes(), credentialSalt, credentialAlg);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		if(carrierId!=null&&carrierId>0){
			params.put("carrierId", carrierId);
		}
		params.put("address1", address1);
		params.put("birthday", birthday);
		params.put("credentialAlg", credentialAlg);
		params.put("credentialSalt", credentialSalt);
		params.put("credentialHash", credentialHash);
		DataLayerMgr.executeCall("CREATE_USER_CREDENTIAL", params, true);
	}

	public static boolean isAllowSproutSignUp() {
		return allowSproutSignUp;
	}

	public static void setAllowSproutSignUp(boolean allowSproutSignUp) {
		PrepaidUtils.allowSproutSignUp = allowSproutSignUp;
	}

	public static String getUsaliveBaseUrl() {
		return usaliveBaseUrl;
	}

	public static void setUsaliveBaseUrl(String usaliveBaseUrl) {
		PrepaidUtils.usaliveBaseUrl = usaliveBaseUrl;
	}

	public static String getGetMoreBaseUrl() {
		return getMoreBaseUrl;
	}

	public static void setGetMoreBaseUrl(String getMoreBaseUrl) {
		PrepaidUtils.getMoreBaseUrl = getMoreBaseUrl;
	}

	public static String getPassConsumerEmailPostfix() {
		return passConsumerEmailPostfix;
	}

	public static void setPassConsumerEmailPostfix(String defaultPassConsumerEmail) {
		PrepaidUtils.passConsumerEmailPostfix = defaultPassConsumerEmail;
	}
	
	public static boolean isRemoteClientPermitted(String remoteIP) {
		Results results = null;
		try{
			Map<String, Object> params = new HashMap<>();
			params.put("ipAddress", remoteIP);
			results=DataLayerMgr.executeQuery("IS_IP_PERMITTED", params);
			if(results.next()){
				return results.get("lockoutUntilTS") == null;
			}
		}catch(Exception e){
				log.error(String.format("Failed to find out if the IP-address '%s' is permitted", remoteIP), e);
		}
		return true;
	}

	public static void registerFailedRequest(String remoteIP) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("ipAddress", remoteIP);
			DataLayerMgr.executeCall("RECORD_FAILED_LOGIN_BY_IP", params, true);
		} catch(Exception e) {
			log.warn(String.format("Could not record login failure for IP '%s'", remoteIP), e);
		}
	}
	
	public static void registerSuccessRequest(String remoteIP) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("ipAddress", remoteIP);
			DataLayerMgr.executeCall("RECORD_LOGIN_BY_IP", params, true);
		} catch(Exception e) {
			log.warn(String.format("Could not record login for IP '%s'", remoteIP), e);
		}
	}

}
