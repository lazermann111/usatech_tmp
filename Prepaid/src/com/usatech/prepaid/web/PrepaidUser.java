/*
 * Created on Jan 21, 2005
 *
 */
package com.usatech.prepaid.web;

import simple.servlet.BasicServletUser;

/**
 * @author bkrug
 *
 */
public class PrepaidUser extends BasicServletUser {
	private static final long serialVersionUID = -95432316442941L;
    protected long consumerId;
	protected int consumerTypeId;
	protected long customerId;
	protected String companyLogoFileName;
	protected String brandingSubdirectory;
    /**
     * 
     */
    public PrepaidUser() {
        super();
    }

	/**
	 * @return Returns the userId.
	 */
    public long getConsumerId() {
        return consumerId;
    }
    /**
     * @param userId The userId to set.
     */
    public void setConsumerId(long userId) {
        this.consumerId = userId;
    }

	public int getConsumerTypeId() {
		return consumerTypeId;
	}

	public void setConsumerTypeId(int consumerTypeId) {
		this.consumerTypeId = consumerTypeId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}


	public String getBrandingSubdirectory() {
		return brandingSubdirectory;
	}

	public void setBrandingSubdirectory(String brandingSubdirectory) {
		this.brandingSubdirectory = brandingSubdirectory;
	}

	public String getCompanyLogoFileName() {
		return companyLogoFileName;
	}

	public void setCompanyLogoFileName(String companyLogoFileName) {
		this.companyLogoFileName = companyLogoFileName;
	}
	
	
}
