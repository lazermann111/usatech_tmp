package com.usatech.prepaid.web;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.security.SecureHash;
import simple.servlet.BasicServletUser;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.ServletUser;
import simple.servlet.steps.DbLogonStep;
import simple.servlet.steps.LoginFailureException;
import simple.text.StringUtils;

public class PrepaidLogonStep extends DbLogonStep {
	private static Log log = Log.getLog();
	
	public PrepaidLogonStep() {
	}
	
	@Override
	public ServletUser loginUser(InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		final String username = form.getString("username", false);
		if (!isUserPermitted(username)) {
			throw new LoginFailureException(String.format("User %s tried to login but was soft-locked or did not exist.", username), LoginFailureException.INVALID_PASSWORD);
		}
		String remoteIP = request.getRemoteAddr();
		if (StringUtils.isBlank(username) && !PrepaidUtils.isRemoteClientPermitted(remoteIP)) {
			throw new LoginFailureException(String.format("A client from %s tried to login but was soft-locked.", remoteIP), LoginFailureException.INVALID_PASSWORD);
		}
		ServletUser user = null;
		try {
			user = super.loginUser(form, request, response);
		} catch (LoginFailureException e) {
			PrepaidUtils.registerFailedRequest(remoteIP);
			throw e;
		}
		PrepaidUtils.registerSuccessRequest(remoteIP);
		if(StringUtils.isBlank(username)){
			if(ConvertUtils.getBooleanSafely(form.getAttribute("rememberCard"), false)) {
				Cookie usernameCookie = new Cookie("card", form.getString("card", true));
				RequestUtils.setHttpOnlyCookieHeader(request, response, usernameCookie);
			} else {
				RequestUtils.removeCookies(request, response, "card");
			}
		}
		return user;
	}
	
	@Override
	public ServletUser authenticateUser(simple.servlet.InputForm form) throws ServletException {
		if(!StringUtils.isBlank(form.getString("username", false))){
			ServletUser user = null;
			try {
				user = super.authenticateUser(form);
				updateLogin(form.getString("username", false));
			} catch (LoginFailureException e) {
				updateLoginFail(form.getString("username", false));
				throw e;
			}
			return user;
		}else{
			String card = form.getString("card", true);
			String securityCode = form.getString("securityCode", true);
			try {
				Map<String, Object> params=new HashMap<String, Object>();
				byte[] cardHash = SecureHash.getUnsaltedHash(card.getBytes());
				params.put("cardHash", cardHash);
				params.put("securityCode", securityCode);
	            Results results = simple.db.DataLayerMgr.executeQuery("GET_USER_SIMPLE", params, false);
	            if(results.next()) {
	                try {
	                    log.debug("Creating user of class " + getUserClass().getName());
	                    ServletUser user = getUserClass().newInstance();
	                    results.fillBean(user);
	                    if(user instanceof BasicServletUser && getPrivilegesCall() != null && getPrivilegesCall().trim().length() > 0) {
	                        results = simple.db.DataLayerMgr.executeQuery(getPrivilegesCall(), user, false);
	                        BasicServletUser bsu = (BasicServletUser) user;
	                        while(results.next()) {
	                            bsu.addPrivilege(ConvertUtils.convert(String.class, results.getValue(1)));
	                        }
	                    }
	                    log.debug("User " + user.getUserName() + " successfully logged on");
	                    return user;
	                } catch(InstantiationException e) {
	                    //log.warn("Could not create user of class '" + getUserClass().getName() + "'", e);
	                    throw new ServletException("Could not create user of class '" + getUserClass().getName() + "'", e);
	                } catch (IllegalAccessException e) {
	                    //log.warn("Could not create user of class '" + getUserClass().getName() + "'", e);
	                    throw new ServletException("Could not create user of class '" + getUserClass().getName() + "'", e);
	                } catch (BeanException e) {
	                    //log.warn("Could not create user of class '" + getUserClass().getName() + "'", e);
	                    throw new ServletException("Could not create user of class '" + getUserClass().getName() + "'", e);
	                }
	            } else { // user doesn't exist!
	                throw new LoginFailureException("User does not exist", LoginFailureException.INVALID_USERNAME);
	            }  
			} catch(SQLException sqle) {
	            throw new ServletException("Could not run the query '" + getUserCall() + "'", sqle);
			} catch(DataLayerException dle) {
	            throw new ServletException("Could not process the query '" + getUserCall() + "'", dle);
			} catch(ConvertException ce) {
	            throw new ServletException("Could not convert values for the query '" + getUserCall() + "'", ce);            
			} catch(NoSuchAlgorithmException e) {
				throw new ServletException("Could not hash consumer card", e);
			}
		}
	}

	@Override
	public Class<? extends ServletUser> getUserClass() {
		Class<? extends ServletUser> uc = super.getUserClass();
		if(!PrepaidUser.class.isAssignableFrom(uc))
			return PrepaidUser.class;
		return uc;
	}
	
	private void updateLogin(String username) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("username", username);
			DataLayerMgr.executeCall("RECORD_LOGIN", params, true);
		} catch(Exception e) {
			log.warn(String.format("Could not record login for for user '%s'", username), e);
		}
	}

	private void updateLoginFail(String username) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("username", username);
			DataLayerMgr.executeCall("RECORD_FAILED_LOGIN", params, true);
		} catch(Exception e) {
			log.warn(String.format("Could not record login failure for user '%s'", username), e);
		}
	}
	
	private boolean isUserPermitted(String username) {
		if (username == null || "".equals(username)) {
			return true;
		}
		Results results = null;
		try{
			Map<String, Object> params = new HashMap<>();
			params.put("username", username);
			results=DataLayerMgr.executeQuery("IS_USER_PERMITTED", params);
			if(results.next()){
				return ConvertUtils.getInt(results.get("cnt"), 0) == 1;
			}
		}catch(Exception e){
			log.error(String.format("Failed to find out if the user %s is permitted", username), e);
			return true;
		}
		return false;
	}

}
