/*
 * NotLoggedOnException.java
 *
 * Created on January 27, 2003, 9:31 AM
 */

package com.usatech.prepaid.web;

import javax.servlet.ServletException;

/**
 * Signifies that a user has not yet been registered
 * 
 * @author Brian S. Krug
 */
public class NotRegisteredException extends ServletException {
	protected final String username;
	protected final long userId;
	private static final long serialVersionUID = 3987014146767L;

	public NotRegisteredException(String username, long userId) {
		super("'" + username + "' is not registered");
		this.username = username;
		this.userId = userId;
    }

	public String getUsername() {
		return username;
	}

	public long getUserId() {
		return userId;
	}
}


