package com.usatech.prepaid.web;

public class CampaignDetails {
	protected String details;
	protected String title;
	protected String productsDetails;
	public CampaignDetails(String details, String title) {
		super();
		this.details = details;
		this.title = title;
	}
	public CampaignDetails(String details, String title, String productsDetails) {
		super();
		this.details = details;
		this.title = title;
		this.productsDetails = productsDetails;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getProductsDetails() {
		return productsDetails;
	}
	public void setproductsDetails(String productsDetails) {
		this.productsDetails = productsDetails;
	}

}
