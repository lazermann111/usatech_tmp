package com.usatech.prepaid.web;

import java.util.Set;

public class CardOnFile {
	protected long replenConsumerAcctId;
	protected String replenishCardNum;
	protected String currencyCd;
	public CardOnFile(long replenConsumerAcctId,
			String replenishCardNum, String currencyCd) {
		super();
		this.replenConsumerAcctId = replenConsumerAcctId;
		this.replenishCardNum = replenishCardNum;
		this.currencyCd = currencyCd;
	}
	
	public long getReplenConsumerAcctId() {
		return replenConsumerAcctId;
	}
	public void setReplenConsumerAcctId(long replenConsumerAcctId) {
		this.replenConsumerAcctId = replenConsumerAcctId;
	}
	public String getReplenishCardNum() {
		return replenishCardNum;
	}
	public void setReplenishCardNum(String replenishCardNum) {
		this.replenishCardNum = replenishCardNum;
	}
	public String getCurrencyCd() {
		return currencyCd;
	}
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((currencyCd == null) ? 0 : currencyCd.hashCode());
		result = prime * result
				+ (int) (replenConsumerAcctId ^ (replenConsumerAcctId >>> 32));
		result = prime
				* result
				+ ((replenishCardNum == null) ? 0 : replenishCardNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardOnFile other = (CardOnFile) obj;
		if (currencyCd == null) {
			if (other.currencyCd != null)
				return false;
		} else if (!currencyCd.equals(other.currencyCd))
			return false;
		if (replenConsumerAcctId != other.replenConsumerAcctId)
			return false;
		if (replenishCardNum == null) {
			if (other.replenishCardNum != null)
				return false;
		} else if (!replenishCardNum.equals(other.replenishCardNum))
			return false;
		return true;
	}

	public static boolean containsCurrencyCdCards(Set<CardOnFile> cards, String currencyCd){
		for(CardOnFile card: cards){
			if(card.getCurrencyCd()!=null&&card.getCurrencyCd().equalsIgnoreCase(currencyCd)){
				return true;
			}
		}
		return false;
	}
	

}
