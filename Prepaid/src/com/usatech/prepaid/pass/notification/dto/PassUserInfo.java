package com.usatech.prepaid.pass.notification.dto;

import java.io.Serializable;

/**
 * Apple Pass user info
 */
public class PassUserInfo implements Serializable {
    private static final long serialVersionUID = 7309376330520213401L;

    private String cardNumber;
    private String cardSecurityCode;
    private String userEmail;
    private String userPhone;
    private String firstName;
    private String lastName;
    private String postalCode;

    public PassUserInfo() {
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardSecurityCode() {
        return cardSecurityCode;
    }

    public void setCardSecurityCode(String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        PassUserInfo that = (PassUserInfo) o;

        if (cardNumber != null ? !cardNumber.equals(that.cardNumber) : that.cardNumber != null)
            return false;
        if (cardSecurityCode != null ? !cardSecurityCode.equals(that.cardSecurityCode) : that.cardSecurityCode != null)
            return false;
        if (userEmail != null ? !userEmail.equals(that.userEmail) : that.userEmail != null)
            return false;
        if (userPhone != null ? !userPhone.equals(that.userPhone) : that.userPhone != null)
            return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null)
            return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null)
            return false;
        return postalCode != null ? postalCode.equals(that.postalCode) : that.postalCode == null;
    }

    @Override
    public int hashCode() {
        int result = cardNumber != null ? cardNumber.hashCode() : 0;
        result = 31 * result + (cardSecurityCode != null ? cardSecurityCode.hashCode() : 0);
        result = 31 * result + (userEmail != null ? userEmail.hashCode() : 0);
        result = 31 * result + (userPhone != null ? userPhone.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (postalCode != null ? postalCode.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PassUserInfo{" + "cardNumber='" + cardNumber + '\'' + ", cardSecurityCode='" + cardSecurityCode + '\'' + ", userEmail='" + userEmail + '\'' + ", userPhone='" + userPhone + '\'' + ", firstName='" + firstName + '\'' + ", lastName='" + lastName + '\'' + ", postalCode='" + postalCode + '\'' + '}';
    }
}
