package com.usatech.prepaid.pass.notification;

import java.io.Serializable;

/**
 * globalAccountId, consumerAccountId, consumerId
 */
public class ConsumerIds implements Serializable{
    private static final long serialVersionUID = -9165562761306576473L;

    private Long globalAccountId;
    private Long consumerAccountId;
    private Long consumerId;
    private String passcode;

    public ConsumerIds() {
    }

    public Long getGlobalAccountId() {
        return globalAccountId;
    }

    public void setGlobalAccountId(Long globalAccountId) {
        this.globalAccountId = globalAccountId;
    }

    public Long getConsumerAccountId() {
        return consumerAccountId;
    }

    public void setConsumerAccountId(Long consumerAccountId) {
        this.consumerAccountId = consumerAccountId;
    }

    public Long getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(Long consumerId) {
        this.consumerId = consumerId;
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ConsumerIds that = (ConsumerIds) o;

        if (globalAccountId != null ? !globalAccountId.equals(that.globalAccountId) : that.globalAccountId != null)
            return false;
        if (consumerAccountId != null ? !consumerAccountId.equals(that.consumerAccountId) : that.consumerAccountId != null)
            return false;
        return consumerId != null ? consumerId.equals(that.consumerId) : that.consumerId == null;
    }

    @Override
    public int hashCode() {
        int result = globalAccountId != null ? globalAccountId.hashCode() : 0;
        result = 31 * result + (consumerAccountId != null ? consumerAccountId.hashCode() : 0);
        result = 31 * result + (consumerId != null ? consumerId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ConsumerIds{" + "globalAccountId=" + globalAccountId + ", consumerAccountId=" + consumerAccountId + ", consumerId=" + consumerId + '}';
    }
}
