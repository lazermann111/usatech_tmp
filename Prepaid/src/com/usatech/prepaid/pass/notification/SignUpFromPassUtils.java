package com.usatech.prepaid.pass.notification;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;

import com.usatech.prepaid.pass.notification.dto.PassUserInfo;

/**
 * Util class to sign up new user from email link after accepting Apple Wallet's Pass
 */
public final class SignUpFromPassUtils {
    private static final Log LOG = simple.io.Log.getLog();

    private SignUpFromPassUtils() {
    }

    public static PassUserInfo getNewUserInfo(@Nonnull String passSerialNumber) throws SQLException, DataLayerException {
        Map<String, Object> params = new HashMap<>();
        params.put("passSerialNumber", passSerialNumber);
        Results newConsumerDetails = DataLayerMgr.executeQuery("GET_NEW_CONSUMER_INFO_BY_PASS_SN", params, false);
        if (!newConsumerDetails.next()) {
            LOG.error("Failed to find consumer info by pass serial number: " + passSerialNumber);
            return null;
        }
    	PassUserInfo pui = new PassUserInfo();
    	pui.setCardNumber(null);
        pui.setCardSecurityCode(null);
        String flName = (String) newConsumerDetails.get("fullName");
        String[] firstLastName;
        if (flName == null) {
            firstLastName = new String[] {"", ""};
        } else {
            firstLastName = flName.split("\\s");
        }

        pui.setFirstName(firstLastName[0]);
        pui.setLastName(firstLastName.length > 1 ? firstLastName[1] : "");
        pui.setFirstName(firstLastName[0]);
        pui.setLastName(firstLastName.length > 1 ? firstLastName[1] : "");
        pui.setPostalCode((String) newConsumerDetails.get("postal"));
        pui.setUserEmail((String) newConsumerDetails.get("email"));
        pui.setUserPhone((String) newConsumerDetails.get("cellPhone"));
    	return pui;
    }

    public static Map<String, Object> getConsumerDetails(long consumerId) {
        Map<String, Object> paramsAndResults = new HashMap<>();
        paramsAndResults.put("consumerId", consumerId);
        try {
            DataLayerMgr.selectInto("GET_CONSUMER_DETAILS_BY_ID", paramsAndResults);
        } catch(NotEnoughRowsException e) {
            LOG.error("Consumer " + consumerId + " is not found");
        } catch (BeanException | SQLException | DataLayerException e) {
            LOG.error("Error getting consumer details", e);
        }
        return paramsAndResults;
    }

    public static Long getConsumerIdBySerialNumber(@Nonnull String serialNumber) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("passSerialNumber", serialNumber);
            Results results = DataLayerMgr.executeQuery("GET_CONSUMER_ID_BY_PASS_SN", params, false);
            if (!results.next()) {
                LOG.error("Failed to find consumer id by pass serial number: " + serialNumber);
                return null;
            }
            return ConvertUtils.getLong(results.get("consumerId"));
        } catch (Exception e) {
            LOG.error("Failed to getConsumerIdBySerialNumber", e);
            return null;
        }
    }

    public static ConsumerIds findConsumerIdsByPassSerialNumber(@Nonnull String passSerialNumber) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("passSerialNumber", passSerialNumber);
            Results results = DataLayerMgr.executeQuery("GET_GLOBAL_ACCT_ID_BY_PASS_SN", params, false);
            if (!results.next()) {
                LOG.error("Failed to find GlobalAccountId by pass serial number: " + passSerialNumber);
                return null;
            }
            ConsumerIds rv = new ConsumerIds();
            rv.setConsumerId(ConvertUtils.getLong(results.get("consumerId")));
            rv.setConsumerAccountId(ConvertUtils.getLong(results.get("consumerAccountId")));
            rv.setGlobalAccountId(ConvertUtils.getLong(results.get("globalAccountId")));
            return rv;
        } catch (Exception e) {
            LOG.error("Failed to findGlobalAccountIdBySerialNumber", e);
            return null;
        }
    }

    public static void completeUserRegistration(long consumerId, @Nonnull String passCode) {
        Connection conn = null;
        try {
            conn = DataLayerMgr.getConnectionForCall("REGISTER_CONSUMER");
            Map<String, Object> params = new HashMap<>();
            params.put("userId", consumerId);
            params.put("passcode", passCode);
            DataLayerMgr.executeCall(conn, "REGISTER_CONSUMER", params);
            conn.commit();
        } catch(SQLException e) {
            LOG.error("Failed to completeUserRegistration", e);
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException e2) {
                LOG.error("Failed to completeUserRegistration2", e2);
                //ignore
            }
        } catch (DataLayerException e) {
            LOG.error("Failed to completeUserRegistration3", e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    //ignore
                    LOG.error("Failed to completeUserRegistration4", e);
                }
            }
        }
    }
}
