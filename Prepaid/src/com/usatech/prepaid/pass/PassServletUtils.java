package com.usatech.prepaid.pass;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.usatech.cardnumbergenerator.CardFormat2Simple;
import com.usatech.cardnumbergenerator.CardFormat3Simple;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CommonConstants;
import com.usatech.layers.common.constants.ConsumerCommAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.prepaid.web.PrepaidUtils;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.sun.misc.CRC16;
import simple.results.Results;
import simple.security.SecureHash;
import simple.security.SecurityUtils;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

public class PassServletUtils {

	public static final String PUBLIC_KEY_TYPE_APPLE_VAS = "A";
	public static final String PARTNER_PROGRAM_APPLE_VAS = "A";

	private static final String DEVICE_SERIAL_CD_PARAM = "d";
	private static final String PASS_TYPE_CD_PARAM = "p";
	private static final String MODE_PARAM = "m";
	
	private static final String PASS_URL_STUB_MODE = "s";
	private static final String PASS_CARD_STUB_MODE = "c";
	
	protected final static AtomicLong callIdGenerator = new AtomicLong(System.currentTimeMillis());
	protected static String callIdPrefix;

	private static final Log log = simple.io.Log.getLog();

	public static String createPassUrlByCardId(long consumerAcctId) throws Exception {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("consumerAcctId", consumerAcctId);
			Results results = DataLayerMgr.executeQuery("GET_CONSUMER_ACCT_INFO", params);
			if (!results.next())
				throw new ServletException("consumerAcctId " + consumerAcctId + " not found");

			if (!StringUtils.isBlank(results.getFormattedValue("passUrl")))
				throw new ServletException("Pass for consumerAcctId " + consumerAcctId + " already exists");

			long consumerId = results.getValue("consumerId", long.class);
			String consumerAcctCd = results.getFormattedValue("consumerAcctCd");
			String consumerAcctFmtRegex = results.getFormattedValue("consumerAcctFmtRegex");
			int fmtIndex = consumerAcctFmtRegex.indexOf(CommonConstants.USAT_IIN) + 6;
			String fmtDigit = consumerAcctFmtRegex.substring(fmtIndex, fmtIndex + 1);
			String cardNumber = consumerAcctCd.replace(CommonConstants.USAT_IIN + "*", CommonConstants.USAT_IIN + fmtDigit).replace("*", "0");
			
			params.put("partnerProgramCd", PARTNER_PROGRAM_APPLE_VAS);
			DataLayerMgr.executeCall("GET_NEW_PASS_INFO_FOR_CARD", params, true);
			String deviceName = ConvertUtils.getString(params.get("deviceName"), true);
			String passTemplateCd = ConvertUtils.getString(params.get("passTemplateCd"), true);
			long globalAccountId = ConvertUtils.getLong(params.get("globalAccountId"));
			long cardId = ConvertUtils.getLong(params.get("consumerAcctIdentifier"));
			int consumerAcctTypeId = ConvertUtils.getInt(params.get("consumerAcctTypeId"));
			long consumerPassIdentifier = ConvertUtils.getLong(params.get("consumerPassIdentifier"));

			String cardExpirationDate = getCardExpirationDate();
			// get card token
			String token = getCardTokenByGlobalAcctId(globalAccountId, cardNumber, cardExpirationDate, deviceName);
			String callId = newCallId();
			String passPublicKey = getPublicKey(PUBLIC_KEY_TYPE_APPLE_VAS);

			// generate pass
			Map<String, Object> pass = PassApiUtils.generatePass(callId, consumerPassIdentifier, consumerAcctId, token, cardId, passTemplateCd, passPublicKey);
			String passUrl = (String) pass.get("passUrl");

			if (StringUtils.isBlank(passUrl))
				throw new ServletException("Pass URL is missing");

			params.put("consumerId", consumerId);
			params.put("consumerAcctTypeId", consumerAcctTypeId);
			createPass(passUrl, params);
			return passUrl;
		} catch (Exception exception) {
			log.error("Failed to create pass URL", exception);
			throw exception;
		}
	}

	public static String createPassUrlByConsumerId(long consumerId) throws Exception {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("consumerId", consumerId);
			Results results = DataLayerMgr.executeQuery("GET_CONSUMER_PASS_BY_ID", params);
			
			if (results.next() && !StringUtils.isBlank(results.getFormattedValue("passUrl")))
				throw new ServletException("Pass for consumerId " + consumerId + " already exists");

			params.put("partnerProgramCd", PARTNER_PROGRAM_APPLE_VAS);
			DataLayerMgr.executeCall("GET_NEW_PASS_INFO_FOR_CONSUMER", params, true);
			String passTemplateCd = ConvertUtils.getString(params.get("passTemplateCd"), true);
			int consumerAcctTypeId = ConvertUtils.getInt(params.get("consumerAcctTypeId"));
			long consumerPassIdentifier = ConvertUtils.getLong(params.get("consumerPassIdentifier"));

			String callId = newCallId();
			String passPublicKey = getPublicKey(PUBLIC_KEY_TYPE_APPLE_VAS);

			// generate pass
			Map<String, Object> pass = PassApiUtils.generatePass(callId, consumerPassIdentifier, passTemplateCd, passPublicKey);
			String passUrl = (String) pass.get("passUrl");

			if (StringUtils.isBlank(passUrl))
				throw new ServletException("Pass URL is missing");

			params.put("consumerId", consumerId);
			params.put("consumerAcctTypeId", consumerAcctTypeId);
			params.put("consumerPassIdentifier", consumerPassIdentifier);
			createPass(passUrl, params);
			return passUrl;
		} catch (Exception exception) {
			log.error("Failed to create pass URL", exception);
			throw exception;
		}
	}
	
	private static String getCardTokenByGlobalAcctId(long globalAcctId, String cardNumber, String cardExpirationDate, String deviceName) throws Exception {
		Map<String, Object> tokenResult = PassApiUtils.getCardToken(globalAcctId, cardNumber, cardExpirationDate, deviceName);
		byte[] tokenBytes = (byte[]) tokenResult.get("authToken");
		String token = StringUtils.toHex(tokenBytes);
		if (!StringUtils.isBlank(token))
			return token;
		throw new ServletException("Failed to create card token. Error code: "
				+ tokenResult.get(MessageAttrEnum.ATTR_RESPONSE_CODE.getValue()) + " Error message:"
				+ tokenResult.get(MessageAttrEnum.ATTR_RESPONSE_MESSAGE.getValue()));
	}

	public static String  getPassUrlByCardId(long cardId) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cardId", cardId);
		Results results = DataLayerMgr.executeQuery("GET_CONSUMER_PASS_BY_CARD_ID", params, true);
		if (!results.next())
			return null;
		return results.getFormattedValue("passUrl");
	}
	
	public static String getPassUrlByConsumerId(long consumerId) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerId", consumerId);
		Results results = DataLayerMgr.executeQuery("GET_CONSUMER_PASS_BY_ID", params, true);
		if (!results.next())
			return null;
		return results.getFormattedValue("passUrl");
	}
	
	public static String createPassUrl(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			final String remoteAddr = req.getRemoteAddr();
			if (!isIPAllowed(remoteAddr)) {
				throw new ServletException(String.format("IP-address %s is soft-locked", remoteAddr));
			}
			String deviceSerialCd = RequestUtils.getAttribute(req, DEVICE_SERIAL_CD_PARAM, String.class, false);
			String passTypeCd = RequestUtils.getAttribute(req, PASS_TYPE_CD_PARAM, String.class, false);
			if (StringUtils.isBlank(deviceSerialCd) || StringUtils.isBlank(passTypeCd)) {
				log.warn(new StringBuilder("Did not receive required parameter device serial ").append(DEVICE_SERIAL_CD_PARAM).append(" or pass type ").append(PASS_TYPE_CD_PARAM).toString());
				return "";
			}
			String mode = RequestUtils.getAttribute(req, MODE_PARAM, String.class, false);

			if ((PASS_URL_STUB_MODE.equals(mode) || PASS_CARD_STUB_MODE.equals(mode)) && !StringUtils.isBlank(PrepaidUtils.getPassDemoUrl())) {
				log.info("Return demo pass URL for device: " + deviceSerialCd);
				return buildPassUrlResponse(PrepaidUtils.getPassDemoUrl(), resp);
			}

			String publicKey = getPublicKey(PUBLIC_KEY_TYPE_APPLE_VAS);

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("deviceSerialCd", deviceSerialCd);
			params.put("passTypeCd", passTypeCd);
			DataLayerMgr.executeCall("GET_NEW_PASS_INFO", params, true);
			long deviceId = ConvertUtils.getLong(params.get("deviceId"));
			long customerId = ConvertUtils.getLong(params.get("customerId"));
			long corpCustomerId = ConvertUtils.getLong(params.get("corpCustomerId"));
			long rootLocationId = ConvertUtils.getLong(params.get("locationId"));
			String passTemplateCd = ConvertUtils.getString(params.get("passTemplateCd"), true);
			long merchantId = ConvertUtils.getLong(params.get("merchantId"));
			String merchantCd = ConvertUtils.getString(params.get("merchantCd"), true);
			long consumerId = ConvertUtils.getLong(params.get("consumerId"));
			String lastConsumerAcctCd = ConvertUtils.getString(params.get("lastConsumerAcctCd"), false);
			int consumerAcctTypeId = ConvertUtils.getInt(params.get("consumerAcctTypeId"));

			registerSuccessfulRequestFromIP(remoteAddr);

			String url = "";
			long consumerAcctId = 0;
			long passIdentifier = ConvertUtils.getLong(params.get("consumerPassIdentifier"));
			
			if (consumerAcctTypeId == 3) {
				// create card data
				long cardId = getNextCardId();

				log.info("Received request to generate pass for deviceSerialCd: " + deviceSerialCd + ", corpCustomerId: "
						+ corpCustomerId + ", cardId: " + cardId);
				// create prepaid card
				String cardExpirationDate = getCardExpirationDate();
				
				CardFormat2Simple generator = new CardFormat2Simple(cardExpirationDate, lastConsumerAcctCd, buildGroupId(merchantCd), 0, 1);

				PassPrepaidCardDto card = createPrepaidCard(corpCustomerId, customerId, consumerId,
					merchantId, rootLocationId, generator, cardId, passTemplateCd, deviceId, cardExpirationDate);
				consumerAcctId = card.getConsumerAcctId();
				Map<String, Object> result = PassApiUtils.generatePass(newCallId(), passIdentifier, consumerAcctId,
						card.getHexToken(), cardId, passTemplateCd, publicKey);
				// prepare response
				url = result.get(ConsumerCommAttrEnum.ATTR_PASS_URL.getValue()).toString();
				params.put("consumerAcctId", consumerAcctId);
			} else if (consumerAcctTypeId == 7){
				// create card data
				long cardId = getNextCardId();

				log.info("Received request to generate pass for deviceSerialCd: " + deviceSerialCd + ", corpCustomerId: "
						+ corpCustomerId + ", cardId: " + cardId);
				// create payroll deduct card
				String cardExpirationDate = getCardExpirationDate();
				
				CardFormat3Simple generator = new CardFormat3Simple(cardExpirationDate, lastConsumerAcctCd, buildGroupId(merchantCd), 0, 1);

				PassPayrollDeductCardDto card = createPayrollDeductCard(corpCustomerId, customerId, consumerId,
					merchantId, rootLocationId, generator, cardId, passTemplateCd, deviceId, cardExpirationDate);
				consumerAcctId = card.getConsumerAcctId();
				Map<String, Object> result = PassApiUtils.generatePass(newCallId(), passIdentifier, consumerAcctId,
						card.getHexToken(), cardId, passTemplateCd, publicKey);
				// prepare response
				url = result.get(ConsumerCommAttrEnum.ATTR_PASS_URL.getValue()).toString();
				params.put("consumerAcctId", consumerAcctId);
			} else if (consumerAcctTypeId == 5){
				log.info("Received request to generate pass for deviceSerialCd: " + deviceSerialCd + ", corpCustomerId: "
						+ corpCustomerId);
				Map<String, Object> result = PassApiUtils.generatePass(newCallId(), passIdentifier,
						passTemplateCd, publicKey);
				// prepare response
				url = result.get(ConsumerCommAttrEnum.ATTR_PASS_URL.getValue()).toString();
				params.put("consumerAcctId", null);
				params.put("consumerId", null);
			}
			
			params.put("consumerAcctTypeId", consumerAcctTypeId);
			params.put("consumerPassIdentifier", passIdentifier);
			createPass(url, params);

			log.info("Successfully generated pass for device: " + deviceSerialCd);
			return buildPassUrlResponse(url, resp);
		} catch (Exception e) {
			log.error("Failed to generate pass", e);
			return buildErrorResponse(resp, 500, "Failed to generate pass");
		}
	}
	
	public static String newCallId() {
		return new StringBuilder(callIdPrefix).append(callIdGenerator.incrementAndGet()).toString();
	}

	public static String getPublicKey(String publicKeyTypeCd) throws SQLException, DataLayerException {
		String publicKey;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("publicKeyTypeCd", publicKeyTypeCd);
		Results results = null;
		try {
			results = DataLayerMgr.executeQuery("GET_PUBLIC_KEY", params);
			if (results.next()) {
				publicKey = results.getFormattedValue("publicKeyBase64");
				log.info(new StringBuilder("Using public key with identifier ")
						.append(results.getFormattedValue("publicKeyIdentifier").toString()));
			} else {
				throw new SQLException("No public key found in database");
			}
		} finally {
			if (results != null)
				results.close();
		}
		return publicKey;
	}

	private static String createPass(String url, Map<String, Object> params)
			throws SQLException, DataLayerException, ServletException {
		int startIdx = url.lastIndexOf('/') + 1;
		if (startIdx < 1 || startIdx == url.length()) {
			throw new ServletException("Incorrect pass URL; Failed to extract pass serial number");
		}
		String passSerialNumber = url.substring(startIdx, url.length());
		params.put("passSerialNumber", passSerialNumber);
		params.put("passUrl", url);
		DataLayerMgr.executeCall("CREATE_PASS", params, true);
		return passSerialNumber;
	}

	protected static long getNextCardId() throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("numberOfAccounts", 1);
		Object[] results = DataLayerMgr.executeCall("NEW_LAST_CONSUMER_ACCT_IDENTIFIER", params, true);
		return ConvertUtils.getLong(results[1]);
	}

	private static String getOrCreatePrepaidDevice(long customerId, long locationId, long corpCustomerId,
			Connection conn) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<>();
		params.put("currency_cd", "USD");
		params.put("location_id", locationId);
		params.put("customerId", customerId);
		params.put("corp_customer_id", corpCustomerId);
		DataLayerMgr.executeCall(conn, "GET_OR_CREATE_PREPAID_DEVICE", params);
		return (String) params.get("prepaid_device_name");
	}

	private static PassPrepaidCardDto createPrepaidCard(long corpCustomerId, long customerId, long consumerId,
			long merchantId, long locationId, CardFormat2Simple generator, long cardId, String passTemplateCd,
			long deviceId, String cardExpirationDate) throws Exception {
		Map<String, Object> params = new HashMap<>();
		params.put("consumer_acct_active_yn_flag", "Y");
		params.put("consumer_acct_balance", 0);
		params.put("consumer_id", consumerId);
		params.put("location_id", locationId);
		params.put("consumer_acct_issue_num", 1);
		params.put("payment_subtype_id", 1);
		Calendar activationDate = Calendar.getInstance();
		activationDate.add(Calendar.MINUTE, -1);
		params.put("consumer_acct_activation_ts", activationDate.getTime());
		params.put("consumer_acct_deactivation_ts", getCardDeactivationDate());
		params.put("currency_cd", "USD");
		params.put("consumer_acct_fmt_id", getPrepaidCardType());
		params.put("corp_customer_id", corpCustomerId);
		params.put("consumer_acct_promo_total", 0);
		params.put("consumer_acct_type_id", 3);
		params.put("consumer_acct_identifier", cardId);
		params.put("consumer_acct_sub_type_id", 2);

		Connection conn = DataLayerMgr.getConnection("REPORT");
		try {
			PassPrepaidCardDto card = createConsumerAcct(generator, params, conn);

			int rowCount = DataLayerMgr.executeUpdate(conn, "INSERT_MERCHANT_CONSUMER_ACCT", new Object[] { merchantId, card.getConsumerAcctId() });
			if (rowCount != 1) {
				ProcessingUtils.rollbackDbConnection(log, conn);
				return null;
			}

			String prepaidDeviceName = getOrCreatePrepaidDevice(customerId, locationId, corpCustomerId, conn);
			conn.commit();

			CRC16 crc = buildCrc(card.getMagstripe());
			String hexToken = PassApiUtils.sendCardDataToKeyMgr(card.getAccountCdHash(),
					cardId,
					crc.value,
					getCardExpirationDateCalendar().getTimeInMillis(),
					prepaidDeviceName,
					card.getMagstripe().substring(0, card.getMagstripe().indexOf("=")),
					cardExpirationDate
				);
			card.setHexToken(hexToken);
			return card;
		} catch (Exception e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw e;
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}

	}
	
	private static PassPayrollDeductCardDto createPayrollDeductCard(long corpCustomerId, long customerId, long consumerId,
			long merchantId, long locationId, CardFormat3Simple generator, long cardId, String passTemplateCd,
			long deviceId, String cardExpirationDate) throws Exception {
		Map<String, Object> params = new HashMap<>();
		params.put("consumer_acct_active_yn_flag", "Y");
		params.put("consumer_acct_balance", 0);
		params.put("consumer_id", consumerId);
		params.put("location_id", locationId);
		params.put("consumer_acct_issue_num", 1);
		params.put("payment_subtype_id", 1);
		Calendar activationDate = Calendar.getInstance();
		activationDate.add(Calendar.MINUTE, -1);
		params.put("consumer_acct_activation_ts", activationDate.getTime());
		params.put("consumer_acct_deactivation_ts", getCardDeactivationDate());
		params.put("currency_cd", "USD");
		params.put("consumer_acct_fmt_id", getPayrollDeductCardType());
		params.put("corp_customer_id", corpCustomerId);
		params.put("consumer_acct_promo_total", 0);
		params.put("consumer_acct_type_id", 7);
		params.put("consumer_acct_identifier", cardId);
		params.put("consumer_acct_sub_type_id", 3);

		Connection conn = DataLayerMgr.getConnection("REPORT");
		try {
			PassPayrollDeductCardDto card = createConsumerAcct(generator, params, conn);

			int rowCount = DataLayerMgr.executeUpdate(conn, "INSERT_MERCHANT_CONSUMER_ACCT", new Object[] { merchantId, card.getConsumerAcctId() });
			if (rowCount != 1) {
				ProcessingUtils.rollbackDbConnection(log, conn);
				return null;
			}

			String prepaidDeviceName = getOrCreatePrepaidDevice(customerId, locationId, corpCustomerId, conn);
			conn.commit();

			CRC16 crc = buildCrc(card.getMagstripe());
			String hexToken = PassApiUtils.sendCardDataToKeyMgr(card.getAccountCdHash(),
					cardId,
					crc.value,
					getCardExpirationDateCalendar().getTimeInMillis(),
					prepaidDeviceName,
					card.getMagstripe().substring(0, card.getMagstripe().indexOf("=")),
					cardExpirationDate
				);
			card.setHexToken(hexToken);
			return card;
		} catch (Exception e) {
			ProcessingUtils.rollbackDbConnection(log, conn);
			throw e;
		} finally {
			ProcessingUtils.closeDbConnection(log, conn);
		}

	}
	
	private static PassPrepaidCardDto createConsumerAcct(CardFormat2Simple generator, Map<String, Object> params, Connection conn) throws Exception {
		while (true) {
			String card = generator.getNextCardData();
			SecureRandom random = SecurityUtils.getSecureRandom();
			PassPrepaidCardDto ret = parsePrepaidCardNumber(card, random);
			byte[] accountCdHash = SecureHash.getUnsaltedHash(ret.getMagstripe().substring(0, ret.getMagstripe().indexOf('=')).getBytes());
			ret.setAccountCdHash(accountCdHash);
			params.put("truncated_card_num", MessageResponseUtils.maskCardNumber(ret.getMagstripe()));
			params.put("last_consumer_acct_cd", ret.getConsumerAcctCd());
			params.put("consumer_acct_cd_hash", accountCdHash);
			byte[] salt = SecureHash.getSalt(random, SecureHash.SALT_LENGTH);
			params.put("consumer_acct_raw_hash", SecureHash.getHash(ret.getMagstripe().getBytes(), salt));
			params.put("consumer_acct_raw_salt", salt);
			salt = SecureHash.getSalt(random, SecureHash.SALT_LENGTH);
			params.put("security_cd_hash", SecureHash.getHash(ret.getSecurityCd().getBytes(), salt));
			params.put("security_cd_salt", salt);
			params.put("security_cd_hash_alg", SecureHash.HASH_ALGORITHM_ITERS);
			Object[] result = DataLayerMgr.executeCall(conn, "INSERT_CONSUMER_ACCT", params);
			long consumerAcctId = extractConsumerAcctId(result);
			if (consumerAcctId == 0) {
				log.warn("Duplicate consumer account...");
				continue;
			}
			if (consumerAcctId < 1) {
				throw new ServletException("Failed to create new consumer account");
			}
			ret.setConsumerAcctId(consumerAcctId);
			log.info("Consumer account created, consumerAcctId = " + consumerAcctId);
			return ret;
		}
	}
	
	private static PassPayrollDeductCardDto createConsumerAcct(CardFormat3Simple generator, Map<String, Object> params, Connection conn) throws Exception {
		while (true) {
			String card = generator.getNextCardData();
			SecureRandom random = SecurityUtils.getSecureRandom();
			PassPayrollDeductCardDto ret = parsePayrollDeductCardNumber(card, random);
			byte[] accountCdHash = SecureHash.getUnsaltedHash(ret.getMagstripe().substring(0, ret.getMagstripe().indexOf('=')).getBytes());
			ret.setAccountCdHash(accountCdHash);
			params.put("truncated_card_num", MessageResponseUtils.maskCardNumber(ret.getMagstripe()));
			params.put("last_consumer_acct_cd", ret.getConsumerAcctCd());
			params.put("consumer_acct_cd_hash", accountCdHash);
			byte[] salt = SecureHash.getSalt(random, SecureHash.SALT_LENGTH);
			params.put("consumer_acct_raw_hash", SecureHash.getHash(ret.getMagstripe().getBytes(), salt));
			params.put("consumer_acct_raw_salt", salt);
			salt = SecureHash.getSalt(random, SecureHash.SALT_LENGTH);
			params.put("security_cd_hash", SecureHash.getHash(ret.getSecurityCd().getBytes(), salt));
			params.put("security_cd_salt", salt);
			params.put("security_cd_hash_alg", SecureHash.HASH_ALGORITHM_ITERS);
			Object[] result = DataLayerMgr.executeCall(conn, "INSERT_CONSUMER_ACCT", params);
			long consumerAcctId = extractConsumerAcctId(result);
			if (consumerAcctId == 0) {
				log.warn("Duplicate consumer account...");
				continue;
			}
			if (consumerAcctId < 1) {
				throw new ServletException("Failed to create new consumer account");
			}
			ret.setConsumerAcctId(consumerAcctId);
			log.info("Consumer account created, consumerAcctId = " + consumerAcctId);
			return ret;
		}
	}
	
	private static CRC16 buildCrc(String magstripe) {
		CRC16 crc = new CRC16();
		for (byte b : magstripe.getBytes()) {
			crc.update(b);
		}
		return crc;
	}

	private static long extractConsumerAcctId(Object[] result) {
		if (result != null && result[1] != null) {
			return ((BigDecimal) result[1]).longValue();
		}
		return -1;
	}

	private static long getPrepaidCardType() {
		return 2;
	}
	
	private static long getPayrollDeductCardType() {
		return 3;
	}
	
	private static int buildGroupId(String merchantCd) {
		return Integer.parseInt(merchantCd.substring(merchantCd.length() - 3));
	}

	private static String getCardExpirationDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYMM");
		return sdf.format(getCardExpirationDateCalendar().getTime());
	}

	private static Calendar getCardExpirationDateCalendar() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, 50);
		return calendar;
	}

	private static Timestamp getCardDeactivationDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, 50);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		return new Timestamp(calendar.getTimeInMillis());
	}

	public static String buildPassUrlResponse(String url, HttpServletResponse httpResponse) throws IOException {
		httpResponse.setContentType("application/json");
		httpResponse.setStatus(HttpServletResponse.SC_OK);
		JsonObject rootJsonObject = new JsonObject();
		JsonObject urlJsonObject = new JsonObject();
		rootJsonObject.add("URLs", urlJsonObject);
		urlJsonObject.addProperty("passDownload", url);

		Gson gson = new Gson();
		return gson.toJson(rootJsonObject);
	}

	private static String buildErrorResponse(HttpServletResponse resp, int errorCode, String errorMessage)
			throws IOException {
		resp.setContentType("application/json");
		JsonObject rootJsonObject = new JsonObject();
		JsonObject errorObject = new JsonObject();
		rootJsonObject.add("error", errorObject);
		errorObject.addProperty("code", errorCode);
		errorObject.addProperty("message", errorMessage);
		Gson gson = new Gson();
		return gson.toJson(rootJsonObject);
	}

	public static PassPrepaidCardDto parsePrepaidCardNumber(String card, SecureRandom random)
			throws NoSuchAlgorithmException, NoSuchProviderException {
		String[] fields = card.split(",");
		return new PassPrepaidCardDto(fields[0], fields[1], SecurityUtils.getRandomString(random, "0123456789", 4));
	}

	public static PassPayrollDeductCardDto parsePayrollDeductCardNumber(String card, SecureRandom random)
			throws NoSuchAlgorithmException, NoSuchProviderException {
		String[] fields = card.split(",");
		return new PassPayrollDeductCardDto(fields[0], fields[1], SecurityUtils.getRandomString(random, "0123456789", 4));
	}
	
	private static boolean isIPAllowed(String ipAddress) {
		Results results = null;
		try{
			Map<String, Object> params = new HashMap<>();
			params.put("ipAddress", ipAddress);
			results=DataLayerMgr.executeQuery("IS_PASS_REQ_IP_PERMITTED", params);
			if(results.next()){
				return results.get("lockoutUntilTS") == null;
			}
		}catch(Exception e){
				log.error(String.format("Failed to find out if the IP-address '%s' is permitted", ipAddress), e);
		}
		return true;
	}

	private static void registerSuccessfulRequestFromIP(String ipAddress) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("ipAddress", ipAddress);
			DataLayerMgr.executeCall("RECORD_PASS_REQ_BY_IP", params, true);
		} catch(Exception e) {
			log.warn(String.format("Could not record login for IP '%s'", ipAddress), e);
		}
	}

	private static void registerWrongDeviceSerialCodeUsage(String ipAddress) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("ipAddress", ipAddress);
			DataLayerMgr.executeCall("RECORD_FAILED_PASS_REQ_BY_IP", params, true);
		} catch(Exception e) {
			log.warn(String.format("Could not record login failure for IP '%s'", ipAddress), e);
		}
	}

	private static final class PassPrepaidCardDto {

		private long consumerAcctId;
		private String magstripe;
		private String consumerAcctCd;
		private String securityCd;
		private byte[] accountCdHash;
		private String hexToken;

		private PassPrepaidCardDto(String magstripe, String consumerAcctCd, String securityCd) {
			this.magstripe = magstripe;
			this.consumerAcctCd = consumerAcctCd;
			this.securityCd = securityCd;
		}

		public String getMagstripe() {
			return magstripe;
		}

		@SuppressWarnings("unused")
		public void setMagstripe(String magstripe) {
			this.magstripe = magstripe;
		}

		public String getConsumerAcctCd() {
			return consumerAcctCd;
		}

		@SuppressWarnings("unused")
		public void setConsumerAcctCd(String consumerAcctCd) {
			this.consumerAcctCd = consumerAcctCd;
		}

		public String getSecurityCd() {
			return securityCd;
		}

		@SuppressWarnings("unused")
		public void setSecurityCd(String securityCd) {
			this.securityCd = securityCd;
		}

		public long getConsumerAcctId() {
			return consumerAcctId;
		}

		public void setConsumerAcctId(long consumerAcctId) {
			this.consumerAcctId = consumerAcctId;
		}

		public byte[] getAccountCdHash() {
			return accountCdHash;
		}

		public void setAccountCdHash(byte[] accountCdHash) {
			this.accountCdHash = accountCdHash;
		}

		public String getHexToken() {
			return hexToken;
		}

		public void setHexToken(String token) {
			hexToken = token;
		}
	}

	private static final class PassPayrollDeductCardDto {

		private long consumerAcctId;
		private String magstripe;
		private String consumerAcctCd;
		private String securityCd;
		private byte[] accountCdHash;
		private String hexToken;

		private PassPayrollDeductCardDto(String magstripe, String consumerAcctCd, String securityCd) {
			this.magstripe = magstripe;
			this.consumerAcctCd = consumerAcctCd;
			this.securityCd = securityCd;
		}

		public String getMagstripe() {
			return magstripe;
		}

		@SuppressWarnings("unused")
		public void setMagstripe(String magstripe) {
			this.magstripe = magstripe;
		}

		public String getConsumerAcctCd() {
			return consumerAcctCd;
		}

		@SuppressWarnings("unused")
		public void setConsumerAcctCd(String consumerAcctCd) {
			this.consumerAcctCd = consumerAcctCd;
		}

		public String getSecurityCd() {
			return securityCd;
		}

		@SuppressWarnings("unused")
		public void setSecurityCd(String securityCd) {
			this.securityCd = securityCd;
		}

		public long getConsumerAcctId() {
			return consumerAcctId;
		}

		public void setConsumerAcctId(long consumerAcctId) {
			this.consumerAcctId = consumerAcctId;
		}

		public byte[] getAccountCdHash() {
			return accountCdHash;
		}

		public void setAccountCdHash(byte[] accountCdHash) {
			this.accountCdHash = accountCdHash;
		}

		public String getHexToken() {
			return hexToken;
		}

		public void setHexToken(String token) {
			hexToken = token;
		}
	}
	
	public static String getCallIdPrefix() {
		return callIdPrefix;
	}

	public static void setCallIdPrefix(String callIdPrefix) {
		PassServletUtils.callIdPrefix = callIdPrefix;
	}

}
