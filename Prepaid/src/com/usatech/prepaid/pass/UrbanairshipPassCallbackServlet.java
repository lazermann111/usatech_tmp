package com.usatech.prepaid.pass;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.prepaid.web.PrepaidUtils;

public class UrbanairshipPassCallbackServlet extends HttpServlet {

	private static final long serialVersionUID = 8732278210996907677L;

	private static final Log log = simple.io.Log.getLog();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String url = req.getRequestURI();
		if (url.endsWith("personalize")){
			Connection conn = null;
			try {
				Gson gson = new Gson();
				PassCallbackRequest passCallbackRequest = gson.fromJson(req.getReader(), PassCallbackRequest.class);
				validateRequest(passCallbackRequest);

				long consumerPassId = getConsumerPassIdBySerialNumber(passCallbackRequest.getSerialNumber());
				int acctTypeId = getAcctTypeIdByPassId(consumerPassId);
				String accType = "cardType=";
				switch  (acctTypeId){
					case 3: accType="cardType=2";
							break;
					case 5: accType="cardType=0";
							break;
					case 7: accType="cardType=4";
							break;
				}
				Map<String, String> personalizationInfo = passCallbackRequest.getPersonalizationInfo();
				String body = new StringBuilder("Please visit this link to complete your MORE loyalty card registration: ")
						.append(PrepaidUtils.getGetMoreBaseUrl()).append("signup.html?").append(accType).append("&uuid=")
						.append(passCallbackRequest.getSerialNumber()).toString();

				conn = DataLayerMgr.getConnection("REPORT");
				Map<String, Object> params = new HashMap<String, Object>();
				String consumerEmail = extractPersonalizationField(personalizationInfo, PassCallbackRequest.EMAIL_ADDRESS_FIELD);
				String consumerName = extractPersonalizationField(personalizationInfo, PassCallbackRequest.NAME_FIELD);
				String postalCode = extractPersonalizationField(personalizationInfo, PassCallbackRequest.POSTAL_CODE_FIELD);
				String phoneNumber = extractPersonalizationField(personalizationInfo, PassCallbackRequest.PHONE_NUMBER_FIELD);
				params.put("toEmail", consumerEmail);
				params.put("toName", consumerName);
				params.put("body", body);
				params.put("subject", "MORE Loyalty Card");
				updateConsumerPass(conn, consumerEmail, consumerName, postalCode, phoneNumber, consumerPassId);
				String fromEmail = "prepaid@usatech.com";
				String fromName = "MORE by USA Technologies";
				params.put("fromEmail", fromEmail);
				params.put("fromName", fromName);
				DataLayerMgr.executeCall(conn, "SEND_EMAIL", params);
				conn.commit();
				log.info("Personalization request received" + personalizationInfo.toString());
			} catch (Exception exception) {
				log.error("Failed to personalize pass", exception);
				ProcessingUtils.rollbackDbConnection(log, conn);
				sendErrorReponse(exception, resp);
			} finally {
				ProcessingUtils.closeDbConnection(log, conn);
			}
		}

	}

	protected static String getBaseUrl(HttpServletRequest req) {
		return RequestUtils.getBaseUrl(req);
	}

	//Update Consumer Pass with personalization Info.
	private void updateConsumerPass(Connection connection, String consumerEmail, String consumerName, String postalCode, String phoneNumber, long consumerPassId) 
			throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerEmail", consumerEmail);
		params.put("consumerName", consumerName);
		params.put("postalCode", postalCode);
		params.put("phoneNumber", phoneNumber);
		params.put("consumerPassId", consumerPassId);
		DataLayerMgr.executeCall(connection, "UPDATE_CONSUMER_PASS", params);	
	}
	
	private String extractPersonalizationField(Map<String, String> personalizationInfo, String fieldName) {
		return personalizationInfo.get(fieldName);
	}

	private void validateRequest(PassCallbackRequest passCallbackRequest) {
		if (passCallbackRequest == null) {
			throw new IllegalArgumentException("Pass personalization info is required");
		}

		if (StringUtils.isBlank(passCallbackRequest.getSerialNumber())) {
			throw new IllegalArgumentException("Pass pass serial number is required");
		}

		Map<String, String> personalizationInfo = passCallbackRequest.getPersonalizationInfo();
		if (personalizationInfo == null) {
			throw new IllegalArgumentException("Pass personalization info is required");
		}

		String consumerEmail = personalizationInfo.get(PassCallbackRequest.EMAIL_ADDRESS_FIELD);
		if (StringUtils.isBlank(consumerEmail)) {
			throw new IllegalArgumentException("Consumer email is required");
		}

		String consumerName = personalizationInfo.get(PassCallbackRequest.NAME_FIELD);
		if (StringUtils.isBlank(consumerName)) {
			throw new IllegalArgumentException("Consumer name is required");
		}
	}

	private long getConsumerPassIdBySerialNumber(String serialNumber)
			throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("passSerialNumber", serialNumber);
		Results results = DataLayerMgr.executeQuery("GET_CONSUMER_PASS_ID_BY_SN", params, true);
		if (!results.next()) {
			throw new IllegalArgumentException("Failed to find consumer pass by serial number: " + serialNumber);
		}
		return ConvertUtils.getLong(results.get("consumerPassId"));
	}

	private int getAcctTypeIdByPassId(long consumerPassId) throws SQLException, DataLayerException{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("consumerPassId", consumerPassId);
		int acctTypeId = 0;
		Results results = DataLayerMgr.executeQuery("GET_ACCT_TYPE_BY_PASS_ID", params, true);
		if (results.next()) {
			try {
				acctTypeId = ConvertUtils.getInt(results.get("acctTypeId"));
			} catch (ConvertException e) {
				acctTypeId = 0;
			}
		}
		return acctTypeId;
	}
	
	private void sendErrorReponse(Exception exception, HttpServletResponse resp) {

	}

	public static final class PassCallbackRequest implements Serializable {

		private static final long serialVersionUID = 3943723591931863210L;

		public static final String EMAIL_ADDRESS_FIELD = "emailAddress";
		public static final String PHONE_NUMBER_FIELD = "phoneNumber";
		public static final String NAME_FIELD = "fullName";
		public static final String POSTAL_CODE_FIELD = "postalCode";

		private String templateId;

		private String serialNumber;

		private Date createdAt;

		private Map<String, String> personalizationInfo;

		public String getTemplateId() {
			return templateId;
		}

		public void setTemplateId(String templateId) {
			this.templateId = templateId;
		}

		public String getSerialNumber() {
			return serialNumber;
		}

		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}

		public Date getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}

		public Map<String, String> getPersonalizationInfo() {
			return personalizationInfo;
		}

		public void setPersonalizationInfo(Map<String, String> personalizationInfo) {
			this.personalizationInfo = personalizationInfo;
		}

		@Override
		public String toString() {
			return "PassCallbackRequest [templateId=" + templateId + ", serialNumber=" + serialNumber + ", createdAt="
					+ createdAt + ", personalizationInfo=" + personalizationInfo + "]";
		}

	}

}
