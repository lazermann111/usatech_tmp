package com.usatech.prepaid.pass;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.ConsumerCommAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.security.crypt.EncryptionInfoMapping;
import simple.text.StringUtils;

public class PassApiUtils {

	private static Publisher<ByteInput> publisher;
	private static String resultQueue;
	private static long reponseTimeoutInMls = TimeUnit.SECONDS.toMillis(120);
	private static EncryptionInfoMapping encryptionInfoMapping;

	public static Map<String, Object> generatePass(String callId, long consumerPassIdentifier, long consumerAcctId, String token, long cardId,
			String passTemplateId, String publicKey) throws Exception {
		MessageChainV11 messageChainV11 = new MessageChainV11();
		MessageChainStep transportStep = messageChainV11.addStep("usat.pass.api.generate");
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_CARD_ID, cardId);
		transportStep.setSecretAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, token);
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_PASS_PUBLIC_KEY, publicKey);
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_PASS_TEMPLATE_ID, passTemplateId);
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_CALL_ID, callId);
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, consumerAcctId);
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_PASS_IDENTIFIER, consumerPassIdentifier);
		SyncMessagePublisher syncMessagePublisher = new SyncMessagePublisher(reponseTimeoutInMls);
		Map<String, Object> ret = syncMessagePublisher.publishMessage("usat.pass.api.generate_" + callId,
				transportStep, messageChainV11, resultQueue, getPublisher(), null);
		if (ret.containsKey(CommonAttrEnum.ATTR_ERROR_CODE.getValue())) {
			throw new IllegalStateException("Error occurred while executing PassApiTask. CODE: " +
					ret.get(CommonAttrEnum.ATTR_ERROR_CODE.getValue()) + ", MESSAGE: " +
					ret.get(CommonAttrEnum.ATTR_ERROR_MESSAGE.getValue()));
		}
		if (!ret.containsKey(ConsumerCommAttrEnum.ATTR_PASS_URL.getValue())) {
			throw new IllegalArgumentException("Pass URL is missing");
		}
		return syncMessagePublisher.publishMessage("usat.pass.api.generate_" + callId, transportStep,
				messageChainV11, resultQueue, getPublisher(), null);
	}

	public static Map<String, Object> generatePass(String callId, long consumerPassIdentifier, 
			String passTemplateId, String publicKey) throws Exception {
		MessageChainV11 messageChainV11 = new MessageChainV11();
		MessageChainStep transportStep = messageChainV11.addStep("usat.pass.api.generate");
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_PASS_PUBLIC_KEY, publicKey);
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_PASS_TEMPLATE_ID, passTemplateId);
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_CALL_ID, callId);
		transportStep.setSecretAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, "0");
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, "0");
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_CARD_ID, "0");
		transportStep.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_PASS_IDENTIFIER, consumerPassIdentifier);
		SyncMessagePublisher syncMessagePublisher = new SyncMessagePublisher(reponseTimeoutInMls);
		Map<String, Object> ret = syncMessagePublisher.publishMessage("usat.pass.api.generate_" + callId,
				transportStep, messageChainV11, resultQueue, getPublisher(), null);
		if (ret.containsKey(CommonAttrEnum.ATTR_ERROR_CODE.getValue())) {
			throw new IllegalStateException("Error occurred while executing PassApiTask. CODE: " +
					ret.get(CommonAttrEnum.ATTR_ERROR_CODE.getValue()) + ", MESSAGE: " +
					ret.get(CommonAttrEnum.ATTR_ERROR_MESSAGE.getValue()));
		}
		if (!ret.containsKey(ConsumerCommAttrEnum.ATTR_PASS_URL.getValue())) {
			throw new IllegalArgumentException("Pass URL is missing");
		}
		return syncMessagePublisher.publishMessage("usat.pass.api.generate_" + callId, transportStep,
				messageChainV11, resultQueue, getPublisher(), null);
	}
	
	public static Map<String, Object> getCardToken(long globalAccountId, String cardNumber, String expirationDate, String deviceName)
			throws Exception {
		String callId = "usat.keymanager.account.token.lookup_" + globalAccountId;
		MessageChainV11 messageChainV11 = new MessageChainV11();
		MessageChainStep tokenLookupStep = messageChainV11.addStep("usat.keymanager.account.token.lookup");
		tokenLookupStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
		tokenLookupStep.setAttribute(CommonAttrEnum.ATTR_INTERNAL_MESSAGE, true);
		tokenLookupStep.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, deviceName);
		tokenLookupStep.setAttribute(CommonAttrEnum.ATTR_CREATE, true);
		tokenLookupStep.setIndexedSecretAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, 1, cardNumber);
		tokenLookupStep.setIndexedSecretAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, 2, expirationDate);
		tokenLookupStep.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, 2);
		tokenLookupStep.setTemporary(true);

		MessageChainStep resultStep = messageChainV11.addStep(resultQueue);

		// result attributes
		resultStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, tokenLookupStep,
				AuthorityAttrEnum.ATTR_AUTH_TOKEN);
		resultStep.setAttribute(ConsumerCommAttrEnum.ATTR_CALL_ID, callId);

		tokenLookupStep.setNextSteps(0, resultStep);
		SyncMessagePublisher syncMessagePublisher = new SyncMessagePublisher(reponseTimeoutInMls);
		Map<String, Object> ret = syncMessagePublisher.publishMessage(callId, tokenLookupStep, messageChainV11,
				resultQueue, getPublisher(), getEncryptionInfoMapping());
		return ret;
	}

	public static String sendCardDataToKeyMgr(@Nonnull byte[] accountCdHash,
											  long globalAccountId,
											  @Nonnull Integer trackCrc,
											  long cardExpirationTime,
											  @Nonnull String deviceName,
											  String cardNumber,
											  String expirationDate)
			throws Exception {
		String callId = "usat.keymanager.pass.account.update_" + globalAccountId;
		MessageChainV11 messageChainV11 = new MessageChainV11();
		MessageChainStep keymanagerStep = messageChainV11.addStep("usat.keymanager.pass.account.update");
		keymanagerStep.setAttribute(AuthorityAttrEnum.ATTR_GLOBAL_ACCOUNT_ID, globalAccountId);
		keymanagerStep.setAttribute(AuthorityAttrEnum.ATTR_ACCOUNT_CD_HASH, accountCdHash);
		keymanagerStep.setAttribute(AuthorityAttrEnum.ATTR_TRACK_CRC, trackCrc);
		keymanagerStep.setAttribute(AuthorityAttrEnum.ATTR_INSTANCE, (short) 0);
		keymanagerStep.setAttribute(AuthorityAttrEnum.ATTR_STORE_EXPIRATION_TIME, cardExpirationTime);
		keymanagerStep.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, deviceName);
		keymanagerStep.setAttribute(AuthorityAttrEnum.ATTR_FORCE_TOKENIZATION, true);
		keymanagerStep.setIndexedSecretAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, 1, cardNumber);
		keymanagerStep.setIndexedSecretAttribute(CommonAttrEnum.ATTR_DECRYPTED_DATA, 2, expirationDate);
		keymanagerStep.setAttribute(CommonAttrEnum.ATTR_ENCRYPTED_DATA_COUNT, 2);
		MessageChainStep resultStep = messageChainV11.addStep(resultQueue);
		resultStep.setReferenceAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, keymanagerStep, AuthorityAttrEnum.ATTR_AUTH_TOKEN);

		// result attributes
		resultStep.setAttribute(ConsumerCommAttrEnum.ATTR_CALL_ID, callId);

		keymanagerStep.setNextSteps(0, resultStep);
		SyncMessagePublisher syncMessagePublisher = new SyncMessagePublisher(reponseTimeoutInMls);
		Map<String, Object> rv = syncMessagePublisher.publishMessage(callId, keymanagerStep, messageChainV11, resultQueue, getPublisher(),
				getEncryptionInfoMapping());
		return StringUtils.toHex((byte[]) rv.get(AuthorityAttrEnum.ATTR_AUTH_TOKEN.getValue()));
	}

	public static final class SyncMessagePublisher implements MessageChainTask {

		private static final Log log = simple.io.Log.getLog();

		private static final Map<String, SynchronousQueue<Map<String, Object>>> calls = new ConcurrentHashMap<>();

		private long reponseTimeoutInMls;

		public SyncMessagePublisher(long reponseTimeoutInMls) {
			this.reponseTimeoutInMls = reponseTimeoutInMls;
		}

		public SyncMessagePublisher() {

		}

		public Map<String, Object> publishMessage(String callId, MessageChainStep requestStep,
				MessageChainV11 messageChain, String callbackQueueKey, Publisher<ByteInput> publisher,
				EncryptionInfoMapping encryptionInfoMapping) throws Exception {
			if (calls.containsKey(callId)) {
				throw new ServiceException("Call with ID " + callId + " already in progress");
			}

			if (requestStep != null) {
				addRequiredParamsForReply(callId, requestStep, callbackQueueKey);
			}

			SynchronousQueue<Map<String, Object>> queue = new SynchronousQueue<>();
			calls.put(callId, queue);
			log.info("Put call id " + callId + " in queue");
			try {
				MessageChainService.publish(messageChain, publisher, encryptionInfoMapping);
				Map<String, Object> ret = queue.poll(reponseTimeoutInMls, TimeUnit.MILLISECONDS);
				if (ret == null) {
					throw new RuntimeException(
							"Failed getting response for call ID: " + callId + ". Wait time is elapsed");
				}
				return ret;
			} finally {
				calls.remove(callId);
			}
		}

		private void addRequiredParamsForReply(String callId, MessageChainStep requestStep, String callbackQueueKey) {
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_CALL_ID, callId);
			requestStep.setAttribute(ConsumerCommAttrEnum.ATTR_MORE_PASS_QUEUENAME, callbackQueueKey);
		}

		@Override
		public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
			MessageChainStep step = taskInfo.getStep();
			String callId = null;
			try {
				callId = step.getAttribute(ConsumerCommAttrEnum.ATTR_CALL_ID, String.class, true);
				log.info("Trying to find call id " + callId + " in queue");
				SynchronousQueue<Map<String, Object>> queue = calls.get(callId);
				if (queue == null) {
					throw new ServiceException("Callback is missing for call " + callId);
				}
				if (!queue.offer(step.getAttributes(), reponseTimeoutInMls, TimeUnit.MILLISECONDS)) {
					throw new RuntimeException(
							"Failed to send response to consumer for call ID: " + callId + ". Waiting time is elapsed");
				}
			} catch (Exception exception) {
				log.error("Failed to execute call " + callId, exception);
			}
			taskInfo.setNextQos(taskInfo.getCurrentQos());
			return 0;
		}

	}

	public static Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public static void setPublisher(Publisher<ByteInput> publisher) {
		PassApiUtils.publisher = publisher;
	}

	public static String getResultQueue() {
		return resultQueue;
	}

	public static void setResultQueue(String resultQueue) {
		PassApiUtils.resultQueue = resultQueue;
	}

	public static EncryptionInfoMapping getEncryptionInfoMapping() {
		return encryptionInfoMapping;
	}

	public static void setEncryptionInfoMapping(EncryptionInfoMapping encryptionInfoMapping) {
		PassApiUtils.encryptionInfoMapping = encryptionInfoMapping;
	}
}
