package com.usatech.prepaid;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.Format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;

import com.caucho.services.server.ServiceContext;
import com.usatech.ec.ECResponse;
import com.usatech.ec2.EC2AuthResponse;
import com.usatech.ec2.EC2CardInfo;
import com.usatech.ec2.EC2ClientUtils;
import com.usatech.ec2.EC2TokenResponse;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.CardReaderType;
import com.usatech.layers.common.constants.CommonConstants;
import com.usatech.layers.common.util.WebHelper;
import com.usatech.prepaid.web.CampaignDetails;
import com.usatech.prepaid.web.ConsumerAccount;
import com.usatech.prepaid.web.PrepaidUtils;
import com.usatech.prepaid.web.PrepaidUtils.CardInfo;
import com.usatech.ps.PSAuthHold;
import com.usatech.ps.PSAuthHoldArray;
import com.usatech.ps.PSChargedCard;
import com.usatech.ps.PSChargedCardArray;
import com.usatech.ps.PSConsumerAccountBase;
import com.usatech.ps.PSConsumerSetting;
import com.usatech.ps.PSConsumerSettings;
import com.usatech.ps.PSPostalInfo;
import com.usatech.ps.PSPrepaidAccount;
import com.usatech.ps.PSPrepaidAccounts;
import com.usatech.ps.PSPrepaidActivities;
import com.usatech.ps.PSPrepaidActivity;
import com.usatech.ps.PSPromoCampaign;
import com.usatech.ps.PSPromoCampaigns;
import com.usatech.ps.PSRegion;
import com.usatech.ps.PSRegions;
import com.usatech.ps.PSReplenishInfo;
import com.usatech.ps.PSReplenishInfoArray;
import com.usatech.ps.PSReplenishment;
import com.usatech.ps.PSReplenishmentArray;
import com.usatech.ps.PSResponse;
import com.usatech.ps.PSTokenResponse;
import com.usatech.ps.PSUser;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.Holder;
import simple.lang.InvalidByteValueException;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.security.SecureHash;
import simple.servlet.GenerateUtils;
import simple.servlet.RequestUtils;
import simple.text.MessageFormat;
import simple.text.StringUtils;
import simple.translator.Translator;

public class PSRequestHandler {
	private static final Log log = Log.getLog();
	
	public static final int RES_FAILED = 0;
	public static final int RES_OK = 1;
	
	public static final int CONSUMER_TYPE_VIRTUAL = 4;
	public static final int CONSUMER_TYPE_PREPAID = 5;
	public static final int CONSUMER_TYPE_REGISTRATION = 6;
	public static final int CONSUMER_TYPE_LOYALTY = 7;
	
	public static final int CONSUMER_ACCT_TYPE_LOYALTY_ACCOUNT = 5;
	
	protected static final String AUTHENTICATION_ERROR = "Authentication failure";
	
	protected static final String cardFormat = "simple.text.PadFormat:        *";
	
			
	protected static String getBaseUrl(ServletRequest request){
		String baseUrl=RequestUtils.getBaseUrl(request);
		log.info("baseUrl="+baseUrl);
		return baseUrl;
	}
	
	protected static ServletRequest getRequest(String protocol){
		ServletRequest request = null;
		if(PrepaidAxisServlet.SOAP_PROTOCOL.equals(protocol)) {
			MessageContext messageContext = MessageContext.getCurrentMessageContext();
			if(messageContext != null)
				request = (ServletRequest) messageContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		} else if(PrepaidHessianServlet.HESSIAN_PROTOCOL.equals(protocol)) {
			request = ServiceContext.getContextRequest();
		} else if(PrepaidJsonServlet.JSON_PROTOCOL.equals(protocol)){
			request = ServiceContext.getContextRequest();
		}
		return request;
	}
	
	public static void setReturnMessage(ServletRequest request, PSResponse response, int returnCode, String messageKey, String defaultText, Object... parameters) {
		String returnMessage;
		Translator translator=RequestUtils.getTranslator((HttpServletRequest)request);
		if(translator != null) {
			returnMessage = translator.translate(messageKey, defaultText, parameters);
		} else {
			returnMessage = new MessageFormat(messageKey).format(parameters);
		}
		response.setReturnCode(returnCode);
		response.setReturnMessage(returnMessage);
	}
	
	protected static PSUser login(ServletRequest request, String username, String password, Integer consumerTypeId, PSResponse response) {
		try {
			final boolean match;
			final Results results=DataLayerMgr.executeQuery("GET_USER", new Object[] { username,username,username,username });
			if (!results.next()){
				match = false;
			}else {
				byte[] enteredPasswordHash = SecureHash.getHash(password.getBytes(), results.getValue("passwordSalt", byte[].class), results.getValue("passwordAlg", String.class));
				match = Arrays.equals(enteredPasswordHash, results.getValue("passwordHash", byte[].class));
			}
			if (!match) {
				setReturnMessage(request,response, RES_FAILED,"prepaid-ws-auth-failure", AUTHENTICATION_ERROR);
				log.warn(AUTHENTICATION_ERROR + ", username: " + username);
				return null;
			}
			PSUser user;
			if(response instanceof PSUser)
				user = (PSUser) response;
			else
				user = new PSUser();
			results.fillBean(user);
			if(results.getFormattedValue("privilegeArray")!=null&&results.getFormattedValue("privilegeArray").equals("REGISTERED")){
				user.setRegistered(true);
			}else{
				user.setRegistered(false);
			}
			return user;
		} catch (Exception e) {
			setReturnMessage(request,response, RES_FAILED,"prepaid-ws-auth-failure", AUTHENTICATION_ERROR);
			log.error("Error checking credentials, username: " + username, e);			
		}
		return null;
	}
	
	
	public static PSUser getUser(String protocol, String username, String password) {
		log.info(new StringBuilder("Received getUser request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSUser response = new PSUser();
		ServletRequest request=getRequest(protocol);
		try {
			PSUser user = login(request, username, password, null, response);
			if(user == null) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-ws-no-user-info", "Unable to retrieve user info.");
			} else {
				setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			}
			return response;
		} catch (Exception e) {
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		} finally {
			log.info(new StringBuilder("Sending getUser response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSResponse verifyUser(String protocol, String username, String password, String passcode) {
		log.info(new StringBuilder("Received verifyUser request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user =  login(request, username, password, null, response);
			if(user == null){
				return response;
			}else if(user.isRegistered()){
	        	setReturnMessage(request,response, RES_FAILED, "prepaid-registration-already-complete-prompt", "Your account is already verified");
                return response;       
			}
			if(StringUtils.isBlank(passcode)){
				setReturnMessage(request,response, RES_FAILED,"prepaid-invalid-passcode", "Your passcode is invalid. Please try again.");
				return response;
			}
			Connection conn = DataLayerMgr.getConnectionForCall("REGISTER_CONSUMER");
			try {
				DataLayerMgr.executeCall(conn, "REGISTER_CONSUMER", new Object[]{user.getConsumerId(), passcode});
				conn.commit();
			} catch(SQLException e) {
				try {
		            conn.rollback();
		        } catch(SQLException e2) {
		            //ignore
		        }
			    log.error("Could not complete registration of prepaid card for consumer " + user.getConsumerId(), e);
			   	switch(e.getErrorCode()) {
			   		case 20320:
			   			setReturnMessage(request,response, RES_FAILED, "prepaid-consumer-not-found", "User is not found. Please contact Customer Service for assistance");
			   			return response;
			        case 20321:
			        	setReturnMessage(request,response, RES_FAILED, "prepaid-passcode-invalid", "Passcode is invalid. Please contact Customer Service for assistance");
			        	return response;
			        case 20323:
			        	setReturnMessage(request,response, RES_FAILED,  "prepaid-consumer-credentials-not-found", "This account is not set up correctly. Please contact Customer Service for assistance");
		                return response;
			        case 20324:
			        	setReturnMessage(request,response, RES_FAILED, "prepaid-registration-already-complete-prompt", "Your account is already verified");
		                return response;          
			        default:
			        	setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");
			        	return response;
			   	}
			}finally {
				conn.close();
			}
	        setReturnMessage(request,response, RES_OK,"prepaid-registration-complete-prompt", "Your account is now verified");
	        return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending verifyUser response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSRegions getRegions(String protocol, String username, String password) {
		log.info(new StringBuilder("Received getRegions request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSRegions response = new PSRegions();
		ArrayList<PSRegion> regions = new ArrayList<PSRegion>();
		ServletRequest request=getRequest(protocol);
		try {
			PSUser user = login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			Results results = DataLayerMgr.executeQuery("GET_REGIONS_FOR_CONSUMER", new Object[]{user.getConsumerId()});
			while (results.next()) {
				PSRegion region = new PSRegion();
				results.fillBean(region);
				setReturnMessage(request,region, RES_OK,"prepaid-ws-ok", "OK");
				regions.add(region);
			} 
			PSRegion[] regionArray = new PSRegion[regions.size()];
			regions.toArray(regionArray);
			response.setRegions(regionArray);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		} catch (Exception e) {
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		} finally {
			log.info(new StringBuilder("Sending getRegions response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSResponse updateRegion(String protocol, String username, String password, int regionId){
		log.info(new StringBuilder("Received updateRegion request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user =  login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			try{
				if(regionId>0){
					Results regionResults=DataLayerMgr.executeQuery("GET_REGION_FOR_CONSUMER_BY_ID", new Object[]{user.getConsumerId(),regionId});
					if(!regionResults.next()){
						setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-regionId", "You entered an invalid regionId: {0}. Please enter a valid one", regionId);
						return response;
					}
				}else {
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-regionId", "You entered an invalid regionId: {0}. Please enter a valid one", regionId);
					return response;
				}
			}catch(Exception e) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-regionId", "You entered an invalid regionId: {0}. Please enter a valid one", regionId);
				return response;
			}
			DataLayerMgr.executeCall("UPDATE_REGION", new Object[]{regionId, user.getConsumerId()}, true);
	        setReturnMessage(request,response, RES_OK,"prepaid-update-region-success", "Successfully updated the region for the user.");
	        return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending updateRegion response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSPrepaidAccounts getPrepaidAccounts(String protocol, String username, String password) {
		log.info(new StringBuilder("Received getPrepaidAccounts request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSPrepaidAccounts response = new PSPrepaidAccounts();
		ArrayList<PSPrepaidAccount> accounts = new ArrayList<PSPrepaidAccount>();
		ServletRequest request=getRequest(protocol);
		try {
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			Results results = DataLayerMgr.executeQuery("GET_ACCOUNTS", user);
			while (results.next()) {
				PSPrepaidAccount account = new PSPrepaidAccount();
				results.fillBean(account);
				setReturnMessage(request,account, RES_OK,"prepaid-ws-ok", "OK");
				accounts.add(account);
			} 
			PSPrepaidAccount[] prepaidAccounts = new PSPrepaidAccount[accounts.size()];
			accounts.toArray(prepaidAccounts);
			response.setPrepaidAccounts(prepaidAccounts);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		} catch (Exception e) {
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		} finally {
			log.info(new StringBuilder("Sending getPrepaidAccounts response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
		
	}
	
	public static PSResponse updateUser(String protocol, String oldUsername, String username, String password, String firstname, String lastname, String address1, String city, String state, String postal, String country, PSConsumerSetting[] consumerSettings){
		return updateUser(protocol, oldUsername, 1, username, null, -1, password, firstname, lastname, address1, city, state, postal, country, consumerSettings);
	}
	
	public static PSResponse updateUser(String protocol, String oldUsername, int preferredCommType, String email, String mobile, int carrierId, String password, String firstname, String lastname, String address1, String city, String state, String postal, String country, PSConsumerSetting[] consumerSettings){
		String username=null;
		if(preferredCommType==1){
			username=email;
		}else{
			username=mobile;
		}
		log.info(new StringBuilder("Received updateUser request, oldUsername: ").append(oldUsername).append(", username: ").append(username)
				.append(", consumerSettings: ").append(Arrays.toString(consumerSettings))
				.toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try {
			PSUser user = login(request, oldUsername, password, null, response);
			if(user == null)
				return response;
			if(preferredCommType!=1&&preferredCommType!=2){
				setReturnMessage(request,response, RES_FAILED,"prepaid-invalid-preferred-comm-type", "You preferred communication type id {0} is invalid. Please try again.", preferredCommType);
				return response;
			}
			if(StringUtils.isBlank(oldUsername) ||
			        StringUtils.isBlank(username) ||
			        StringUtils.isBlank(firstname) ||
			        StringUtils.isBlank(lastname) ||
			        StringUtils.isBlank(address1) ||
			        StringUtils.isBlank(postal) ||
			        StringUtils.isBlank(country)) {
				setReturnMessage(request,response, RES_FAILED,"prepaid-missing-input", "You did not provide all required fields. Please try again.", username);
				return response;
			}
			if(preferredCommType==1||!StringUtils.isBlank(email)){
				try {
					WebHelper.checkEmail(email);
				} catch(AddressException e) {
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-username", "You entered an invalid email: {0}. Please enter a valid one", email);
					return response;
				}
			}
			if(preferredCommType==2||!StringUtils.isBlank(mobile)){
				try{
					PrepaidUtils.checkMobile(mobile, carrierId);
				} catch(MessagingException e) {
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-mobile", "You entered an invalid mobile number: {0}. Please enter a valid one", mobile);
					return response;
				}
			}
			try{
				if(carrierId>0){
					Results carrierResults=DataLayerMgr.executeQuery("GET_SMS_SUFFIX", new Object[]{carrierId});
					if(!carrierResults.next()){
						setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-carrierId", "You entered an invalid mobile carrierId: {0}. Please enter a valid one", carrierId);
						return response;
					}
				}else if(preferredCommType==2){
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-carrierId", "You entered an invalid mobile carrierId: {0}. Please enter a valid one", carrierId);
					return response;
				}
			}catch(Exception e) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-carrierId", "You entered an invalid mobile carrierId: {0}. Please enter a valid one", carrierId);
				return response;
			}
			Holder<String> formattedPostal = new Holder<String>();
			if(!GenerateUtils.checkPostal(country, postal, true, formattedPostal)) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-postal", "You entered an invalid postal code for the country {1}. Please try again.", postal, country);
				return response;
			}
			String credentialAlg = "SHA-256/1000";
			byte[] credentialSalt = SecureHash.getSalt(32);
			byte[] credentialHash = SecureHash.getHash(password.getBytes(), credentialSalt, credentialAlg);
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("consumerId", user.getConsumerId());
			params.put("email", email);
			params.put("preferredCommType", preferredCommType);
			params.put("mobile", mobile);
			if(carrierId>0){
				params.put("carrierId", carrierId);
			}
			params.put("firstname", firstname);
			params.put("lastname", lastname);
			params.put("address1", address1);
			params.put("city", city);
			params.put("state", state);
			params.put("postal", formattedPostal.getValue());
			params.put("country", country);
			params.put("credentialAlg", credentialAlg);
			params.put("credentialSalt", credentialSalt);
			params.put("credentialHash", credentialHash);
			Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_USER");
			try {
				try {
					DataLayerMgr.executeCall(conn, "UPDATE_USER", params);
					Map<String, Object> additionalParams;
					if(consumerSettings == null) {
						additionalParams = Collections.emptyMap();
					} else {
						additionalParams = new HashMap<String, Object>();
						for(PSConsumerSetting setting : consumerSettings) {
							additionalParams.put(setting.getSettingId(), setting.getValue());
						}
					}
					PrepaidUtils.updateUserPreferences(user.getConsumerId(), conn, additionalParams);
				} catch(SQLException e) {
					try {
						conn.rollback();
					} catch(SQLException e2) {
						// ignore
					}
					log.error("Could not update user '" + user.getUserName() + "'", e);
					switch(e.getErrorCode()) {
						case 20303:
							setReturnMessage(request, response, RES_FAILED, "prepaid-username-not-unique", "You have already registered a different card for this email or mobile number. Please log on and add the card there to your existing account", username);
							break;
						case 20309:
							setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-state", "The state you entered is invalid. Please re-enter it.", state);
							break;
						default:
							setReturnMessage(request, response, RES_FAILED, "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");

					}
					return response;
			    }
				if(!user.getUserName().equals(username)) {
					PrepaidUtils.sendCompleteRegistrationEmail(conn, username, 1, firstname, lastname, user.getConsumerId(), ConvertUtils.getString(params.get("passcode"), true), getBaseUrl(request), (HttpServletRequest) request);
					conn.commit();
					setReturnMessage(request, response, RES_OK, "prepaid-registration-email-prompt", "To receive email messages, please click the link in the email that has been sent to you.");
					if(log.isInfoEnabled())
						log.info("Updated prepaid account for user '" + user.getUserName() + ": consumer_id=" + params.get("consumerId"));

				} else {
					conn.commit();
					if(log.isInfoEnabled())
						log.info("Updated prepaid account for user '" + user.getUserName() + ": consumer_id=" + params.get("consumerId"));
				}
			} finally {
				conn.close();
			}
			setReturnMessage(request, response, RES_OK, "prepaid-user-updated", "Successfully updated this account", username);
			return response;
		} catch (Exception e) {
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		} finally {
			log.info(new StringBuilder("Sending updateUser response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static boolean validateCreateUserFields(ServletRequest request, PSResponse response, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword){
		if(preferredCommType!=1&&preferredCommType!=2){
			setReturnMessage(request,response, RES_FAILED,"prepaid-invalid-preferred-comm-type", "You preferred communication type id {0} is invalid. Please try again.", preferredCommType);
			return false;
		}
		String username=null;
		if(preferredCommType==1){
			username=email;
		}else{
			username=mobile;
		}
		if(StringUtils.isBlank(username) ||
				StringUtils.isBlank(firstname) ||
				StringUtils.isBlank(lastname) ||
				StringUtils.isBlank(address1) ||
				StringUtils.isBlank(postal) ||
				StringUtils.isBlank(country) ||
				StringUtils.isBlank(password)) {
			setReturnMessage(request,response, RES_FAILED,"prepaid-missing-input", "You did not provide all required fields. Please try again.", username);
			return false;
		} 
		if(preferredCommType==1||!StringUtils.isBlank(email)){
			try {
				WebHelper.checkEmail(email);
			} catch(MessagingException e) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-username", "You entered an invalid email: {0}. Please enter a valid one", email);
				return false;
			}
		}
		if(preferredCommType==2||!StringUtils.isBlank(mobile)){
			try{
				PrepaidUtils.checkMobile(mobile, carrierId);
			} catch(MessagingException e) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-mobile", "You entered an invalid mobile number: {0}. Please enter a valid one", mobile);
				return false;
			}
		}
		try{
			if(carrierId>0){
				Results carrierResults=DataLayerMgr.executeQuery("GET_SMS_SUFFIX", new Object[]{carrierId});
				if(!carrierResults.next()){
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-carrierId", "You entered an invalid mobile carrierId: {0}. Please enter a valid one", carrierId);
					return false;
				}
			}else if(preferredCommType==2){
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-carrierId", "You entered an invalid mobile carrierId: {0}. Please enter a valid one", carrierId);
				return false;
			}
		}catch(Exception e) {
			setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-carrierId", "You entered an invalid mobile carrierId: {0}. Please enter a valid one", carrierId);
			return false;
		}
		
		if(!PrepaidUtils.checkPassword(request,response, password, confirmPassword)) {
			return false;
		}
		
		Holder<String> formattedPostal = new Holder<String>();
		try{
		    if(!GenerateUtils.checkPostal(country, postal, true, formattedPostal)) {
		    	setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-postal", "You entered an invalid postal code for the country {1}. Please try again.", postal, country);
				return false;
		    }
	    }catch(SQLException e){
	    	setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return false;
	    }
		return true;
	}
	
	public static boolean validateCreditFields(ServletRequest request, PSResponse response,String billingCountry, String billingAddress, String billingPostalCode, String promoCode, boolean checkPromoCode){
		if(checkPromoCode){
			if(	StringUtils.isBlank(promoCode)||
					!promoCode.trim().matches(PrepaidUtils.PROMO_CODE_CHECK)) {
					setReturnMessage(request,response, RES_FAILED,"prepaid-missing-input", "You did not provide all required fields. Please try again.");
					return false;
			} 
		}
		if(StringUtils.isBlank(billingCountry)||
				StringUtils.isBlank(billingAddress)||
				StringUtils.isBlank(billingPostalCode)) {
				setReturnMessage(request,response, RES_FAILED,"prepaid-missing-input", "You did not provide all required fields. Please try again.");
				return false;
		} 
		try{   
		    Holder<String> formattedBillingPostal = new Holder<String>();
			if(!GenerateUtils.checkPostal(billingCountry, billingPostalCode, true, formattedBillingPostal)) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-postal", "You entered an invalid postal code for the billing country {1}. Please try again.", billingPostalCode, billingCountry);
				return false;
			}
		}catch(SQLException e){
	    	setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return false;
	    }
		return true;
	}
	
	public static boolean validateEncryptedFields(ServletRequest request, PSResponse response,String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex,String ksnHex, String billingCountry){
		
		if(StringUtils.isBlank(entryType)||StringUtils.isBlank(encryptedCardDataHex) || StringUtils.isBlank(ksnHex)||StringUtils.isBlank(billingCountry)) {
			setReturnMessage(request, response, RES_FAILED, "prepaid-missing-input", "You did not provide all required fields. Please try again.");
			return false;
		} 
		
		if(entryType.trim().length()!=1) {
			setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-entry-type", "Entry type is invalid. Please try again.");
			return false;
		} 
		try{
			CardReaderType.getByValue((byte)cardReaderType);
		}catch(InvalidByteValueException e){
			setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-card-reader-type", "Card reader type is invalid. Please try again.");
			return false;
		}
		//@Todo what should be the valid value
		if(decryptedCardDataLen<1) {
			setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-decrypted-carddata-length", "Decrypted card data length is invalid. Please try again.");
			return false;
		} 
		return true;
	}
	
	public static boolean validateExpirationDateFields(ServletRequest request, PSResponse response, Integer expMonth, Integer expYear){
		if(expMonth==null || expYear==null) {
			setReturnMessage(request, response, RES_FAILED, "prepaid-missing-input", "You did not provide expiration month or year. Please try again.");
			return false;
		} 
		if(expMonth > 12 || expMonth < 1) {
		    setReturnMessage(request,response, RES_FAILED,"prepaid-invalid-month", "You specified an invalid month. Please enter a valid one.");
			return false;
		}
		Calendar now = Calendar.getInstance();
		int currYear = now.get(Calendar.YEAR); 
		int currMonth = now.get(Calendar.MONTH) + 1;
		if(expYear < currYear || (expYear == currYear && expMonth < currMonth)) {
		    setReturnMessage(request,response, RES_FAILED,"prepaid-card-expired", "This card has already expired. Please use a different one.");
			return false;
		}
		return true;
	}
	public static boolean validateCardFields(ServletRequest request, PSResponse response, String cardNum, String securityCode, boolean isPrepaid){
		if(StringUtils.isBlank(cardNum) ||
				StringUtils.isBlank(securityCode)) {
			setReturnMessage(request,response, RES_FAILED,"prepaid-missing-input", "You did not provide all required fields. Please try again.");
			return false;
		} 
		
		Holder<String> cardNumHolder = new Holder<String>();
		if(!PrepaidUtils.checkCardNum(cardNum, cardNumHolder, isPrepaid)) {
			setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-prepaid-card", "You entered an invalid card number. Please enter a valid one");
		    return false;
		}
		return true;
	}
	
	public static boolean validateConsumerTypePrepaid(ServletRequest request, PSResponse response, long globalAccountId, String currencyCd) throws ConvertException, DataLayerException, SQLException{
		HashMap<String, Object>params = new HashMap<String, Object>();
		params.put("globalAccountId", globalAccountId);
		params.put("currencyCd", currencyCd);
		Results results = DataLayerMgr.executeQuery("GET_CONSUMER_TYPE_PREPAID", params);
		if (results.next()) {
			int consumerTypeId=ConvertUtils.getInt(results.getValue("consumerTypeId"));
			if(consumerTypeId!=CONSUMER_TYPE_VIRTUAL){
				setReturnMessage(request,response, RES_FAILED,"prepaid-registered-already", "This card has already been registered. Please try another card");
	            return false;
			}else{
				return true;
			}
		}else{
			setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-prepaid-card", "You entered an invalid card number. Please enter a valid one");
		    return false;
		}
	}
	public static PSTokenResponse createUser(String protocol, String prepaidCardNum, String prepaidSecurityCode, String username, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings){
		return  createUser(protocol, prepaidCardNum, prepaidSecurityCode, 1, username, null, -1, firstname, lastname, address1, city, state, postal, country, password, confirmPassword, consumerSettings);
	}
	
	public static PSTokenResponse createUser(String protocol, String prepaidCardNum, String prepaidSecurityCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings){
		String username=null;
		if(preferredCommType==1){
			username=email;
		}else{
			username=mobile;
		}
		log.info(new StringBuilder("Received createUser request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", prepaidCardNum: ").append(MessageResponseUtils.maskCardNumber(prepaidCardNum))
				.append(", consumerSettings: ").append(Arrays.toString(consumerSettings))
				.toString());
			PSTokenResponse response=new PSTokenResponse();
			ServletRequest request = getRequest(protocol);
			if(!validateCreateUserFields(request, response, preferredCommType, email,mobile, carrierId, firstname, lastname, address1, city, state, postal, country, password, confirmPassword)){
				return response;
			}
			
			if(!validateCardFields(request, response, prepaidCardNum, prepaidSecurityCode, true)){
				return response;
			}
			
			try {
			    Map<String, Object> additionalParams;
			    if(consumerSettings==null){
			    	additionalParams = Collections.emptyMap();
		    	}else{
		    		additionalParams=new HashMap<String, Object>();
		    		for(PSConsumerSetting setting:consumerSettings){
		    			additionalParams.put(setting.getSettingId(), setting.getValue());
		    		}
		    	}
			    
				String cardHolder = PrepaidUtils.namePartsToCardHolderName(firstname, "", lastname);
				PSConsumerAccountBase consumerAcctBase1 = PrepaidUtils.consumerAccountBaseForCardNum(prepaidCardNum);
				if(consumerAcctBase1.getConsumerTypeId()!=CONSUMER_TYPE_VIRTUAL){
					setReturnMessage(request,response, RES_FAILED,"prepaid-registered-already", "This card has already been registered. Please try another card");
		            return response;
				}
				String virtualDeviceSerialNumber = PrepaidUtils.currencyCdToPrepaidDeviceSerialCd(consumerAcctBase1.getCurrencyCd());
				long tranId = PrepaidUtils.nextTransactionId(virtualDeviceSerialNumber);
				EC2TokenResponse tokenResponse = EC2ClientUtils.getEportConnect().tokenizePlain(username, password, virtualDeviceSerialNumber,
															   tranId, prepaidCardNum, consumerAcctBase1.getDeactivationYYMM(), prepaidSecurityCode, cardHolder, postal, address1, "");
				if (tokenResponse.getReturnCode() != ECResponse.RES_APPROVED) {
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-token", "Unable to create security token for card. {1}", tokenResponse.getReturnMessage());
				} else {
					long globalAccountId = tokenResponse.getCardId();
					ConsumerAccount consumerAccount = PrepaidUtils.createUser(RequestUtils.getTranslator((HttpServletRequest) request), globalAccountId, prepaidSecurityCode, email, mobile, carrierId, preferredCommType, firstname, lastname, address1, city, state, postal, country, password, null, getBaseUrl(request), additionalParams, (HttpServletRequest) request, 2,null, null,null, null);
					response.setCardId(consumerAccount.getConsumerAcctId());
					response.setGlobalAccountId(globalAccountId);
					response.setTokenHex(tokenResponse.getTokenHex());
					if(preferredCommType==1){
						setReturnMessage(request, response, RES_OK, "prepaid-registration-email-prompt", "To receive email messages, please click the link in the email that has been sent to you.");
					}else{
						setReturnMessage(request, response, RES_OK, "prepaid-registration-sms-prompt", "To receive text messages, please enter the passcode found in the text message that has been sent to you.");
					}
				}
		        return response;
			} catch(SQLException e) {
				log.error("Error processing request", e);
				response.setReturnCode(RES_FAILED);
			   	switch(e.getErrorCode()) {
			   		case 20300:
			   			   setReturnMessage(request,response, RES_FAILED,"prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it", username);
			               break;
			           case 20301:
			        	   setReturnMessage(request,response, RES_FAILED,"prepaid-already-registered", "You have already registered this card. Please enter a different card", username);
			               break;
			           case 20302:
			        	   setReturnMessage(request,response, RES_FAILED,"prepaid-registered-to-other", "This card has been registered under a different email or mobile number. Please contact Customer Service for assistance", username);
			               break;
			           case 20303:
			        	   setReturnMessage(request,response, RES_FAILED, "prepaid-username-not-unique", "You have already registered a different card for this email or mobile number. Please log on and add the card there to your existing account", username);
			               break;
			           case 20309:
			        	   setReturnMessage(request,response, RES_FAILED,"prepaid-invalid-state", "The state you entered is invalid. Please re-enter it.", state);
			               break;
			           case 20310:
						   setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-promo-code", "The promotion code you entered is invalid.", "");
						   break;
			           default:
			        	   setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			   	}
			   	return response;
			} catch(Exception e){
				log.error("Error processing request", e);
				setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
				return response;
			} finally {
				log.info(new StringBuilder("Sending createUser response, username: ").append(username)
						.append(StringUtils.objectToString(response, null)).toString());
			}
	}
	
	public static PSTokenResponse registerAnotherCard(String protocol, String username, String password, String prepaidCardNum, String prepaidSecurityCode){
		log.info(new StringBuilder("Received registerAnotherCard request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", prepaidCardNum: ").append(MessageResponseUtils.maskCardNumber(prepaidCardNum))
				.toString());
		PSTokenResponse response=new PSTokenResponse();
		ServletRequest request=getRequest(protocol);
		try {
			PSUser user = login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			
			if(!validateCardFields(request, response, prepaidCardNum, prepaidSecurityCode, true)){
				return response;
			}
			
			String cardHolder = PrepaidUtils.namePartsToCardHolderName(user.getFirstname(), "", user.getLastname());
			PSConsumerAccountBase consumerAccountBase = PrepaidUtils.consumerAccountBaseForCardNum(prepaidCardNum);
			response.setCardId(consumerAccountBase.getConsumerAcctId());
			if(consumerAccountBase.getConsumerTypeId()!=CONSUMER_TYPE_VIRTUAL){
				setReturnMessage(request,response, RES_FAILED,"prepaid-registered-already", "This card has already been registered. Please try another card");
	            return response;
			}
			String virtualDeviceSerialNumber = PrepaidUtils.currencyCdToPrepaidDeviceSerialCd(consumerAccountBase.getCurrencyCd());
			long tranId = PrepaidUtils.nextTransactionId(virtualDeviceSerialNumber);
			EC2TokenResponse tokenResponse = EC2ClientUtils.getEportConnect().tokenizePlain(username, password, virtualDeviceSerialNumber, 
														   tranId, prepaidCardNum, consumerAccountBase.getDeactivationYYMM(), prepaidSecurityCode, cardHolder, user.getPostal(), user.getAddress1(), "");
			if (tokenResponse.getReturnCode() != ECResponse.RES_APPROVED) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-token", "Unable to create security token for card. {1}", tokenResponse.getReturnMessage());
			} else {
				long globalAccountId = tokenResponse.getCardId();
				PrepaidUtils.addCard(user.getConsumerId(), globalAccountId, prepaidSecurityCode);
				response.setGlobalAccountId(globalAccountId);
				response.setTokenHex(tokenResponse.getTokenHex());
				setReturnMessage(request,response, RES_OK,"prepaid-card-registered-prompt", "This prepaid card ({0}) has been linked to your account.", PrepaidUtils.maskCardNumber(prepaidCardNum));
			}
	        return response;
		} catch(SQLException e) {
			log.error("Error processing request", e);
			response.setReturnCode(RES_FAILED);
		   	switch(e.getErrorCode()) {
		   	case 20300: case 20305:
	            setReturnMessage(request,response, RES_FAILED,"prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it");
	            break;
	        case 20301:
	   			setReturnMessage(request,response, RES_FAILED,"prepaid-already-registered", "You have already registered this card. Please enter a different card");
	            break;
	        case 20302:
	            setReturnMessage(request,response, RES_FAILED,"prepaid-registered-to-other", "This card has been registered under a different email or mobile number. Please contact Customer Service for assistance");
	            break;
	        case 20304:
	            setReturnMessage(request,response, RES_FAILED,"prepaid-not-active", "Your account is not active. Please contact Customer Service for assistance");
	            break;
	        default:
	           	setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
		   	}
		   	return response;
		} catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending registerAnotherCard response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSResponse changePassword(String protocol, String username, String password, String newPassword, String newPasswordConfirm){
		log.info(new StringBuilder("Received changePassword request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			if(!PrepaidUtils.checkPassword(request,response, newPassword, newPasswordConfirm)) {
				return response;
			} else {
				PrepaidUtils.updatePassword(user.getConsumerId(), newPassword);
		        setReturnMessage(request,response, RES_OK,"prepaid-password-changed", "Your password has been successfully changed.");
		        return response;
			}
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending changePassword response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSPrepaidActivities getPrepaidActivities(String protocol, String username, String password, Long cardId, Date startTime, Date endTime, Integer maxRows){
		log.info(new StringBuilder("Received getPrepaidActivities request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", cardId: ").append(cardId).append(", startTime: ").append(startTime).append(", endTime: ").append(endTime)
				.append(", maxRows: ").append(maxRows).toString());
		PSPrepaidActivities response=new PSPrepaidActivities();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			if(cardId != null && cardId > 0) {
				CardInfo cardInfo = PrepaidUtils.checkCardPermission(user.getConsumerId(), cardId);
				if(cardInfo == null) {
					setReturnMessage(request, response, RES_FAILED, "prepaid-not-authorized", "You are not authorized to view this card. Please try logging in again.");
					return response;
				}
			}
			HashMap<String, Object> params=new HashMap<String,Object>();
			params.put("simple.servlet.ServletUser", user);
			if(cardId!=null && cardId>0){
				params.put("cardId", cardId);
			}
			params.put("startTime", startTime);
			params.put("endTime", endTime);
			if(maxRows!=null && maxRows>0){
				params.put("maxRows", maxRows);
			}
			Results results = DataLayerMgr.executeQuery("GET_ACTIVITY", params);
			results.trackAggregate("purchase", Aggregate.SUM);
			results.trackAggregate("replenish", Aggregate.SUM);
			results.trackAggregate("discount", Aggregate.SUM);
			ArrayList<PSPrepaidActivity> prepaidActivities= new ArrayList<PSPrepaidActivity>();
			while(results.next()) {
				PSPrepaidActivity prepaidActivity=new PSPrepaidActivity();
				results.fillBean(prepaidActivity);
				prepaidActivities.add(prepaidActivity);
			}
			if(prepaidActivities.size()>0){
				response.setReplenishTotal(ConvertUtils.convert(BigDecimal.class, results.getAggregate("replenish", null, Aggregate.SUM)));
				response.setPurchaseTotal(ConvertUtils.convert(BigDecimal.class, results.getAggregate("purchase", null, Aggregate.SUM)));
				response.setDiscountTotal(ConvertUtils.convert(BigDecimal.class, results.getAggregate("discount", null, Aggregate.SUM)));
			}
			
			PSPrepaidActivity[] prepaidActivityArray = new PSPrepaidActivity[prepaidActivities.size()];
			prepaidActivities.toArray(prepaidActivityArray);
			response.setPrepaidActivities(prepaidActivityArray);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending getPrepaidActivities response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSResponse setupReplenish(String protocol, String username, String password, Long cardId, String replenishCardNum, Integer replenishExpMonth, Integer replenishExpYear, String replenishSecurityCode, String address1, String city, String state, String postal, String country, BigDecimal amount, Integer replenishType, BigDecimal threshhold){
		log.info(new StringBuilder("Received setupReplenish request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", cardId: ").append(cardId).append(", replenishCardNum: ").append(MessageResponseUtils.maskCardNumber(replenishCardNum))
				.append(", amount: ").append(amount).append(", replenishType: ").append(replenishType).append(", threshhold: ").append(threshhold)
				.toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			if(cardId == null || cardId < 0) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-card-id", "Card id was not provided or is invalid. Please include a valid card id.");
				return response;
			}
			
			CardInfo cardInfo = PrepaidUtils.checkCardPermission(user.getConsumerId(), cardId);
			if(cardInfo == null) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
			    return response;
			}
			
			if(amount != null && amount.compareTo(PrepaidUtils.getMinReplenishAmount()) < 0) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-replenish-amt-too-small", "The amount you entered is too small. Please enter at least {0}.", PrepaidUtils.getMinReplenishAmount());
				return response;
			} else if(amount.compareTo(PrepaidUtils.getMaxReplenishAmount()) > 0) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-replenish-amt-too-big", "The amount you entered is too much. Please enter at most {0}.", PrepaidUtils.getMaxReplenishAmount());
				return response;
			}
			
			if(replenishType == 1) {
				if(threshhold!=null&&threshhold.compareTo(PrepaidUtils.getMinThreshholdAmount()) < 0) {
				    setReturnMessage(request,response, RES_FAILED, "prepaid-threshhold-amt-too-small", "The minimum balance you entered is too small. Please enter at least {0}.", PrepaidUtils.getMinThreshholdAmount());
				    return response;
				}
			} else if(replenishType == 2){ 
				threshhold = null;
			}else{
				setReturnMessage(request,response, RES_FAILED, "prepaid-replenish-type-not-allowed", "The replenishType {0} you entered is invalid. Please enter a valid one", replenishType);
			    return response;
			}
			Holder<String> replenishCardNumHolder = new Holder<String>();
			if(!PrepaidUtils.checkCardNum(replenishCardNum, replenishCardNumHolder, false)) {
			    setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-credit-card", "You entered an invalid credit card number. Please enter a valid one");
			    return response;
			}
			replenishCardNum = replenishCardNumHolder.getValue();
			
			if(replenishExpMonth > 12 || replenishExpMonth < 1) {
			    setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-month", "You specified an invalid month. Please enter a valid one");
			    return response;
			}
			Calendar now = Calendar.getInstance();
			int currYear = now.get(Calendar.YEAR); 
			int currMonth = now.get(Calendar.MONTH) + 1;
			if(replenishExpYear < currYear || (replenishExpYear == currYear && replenishExpMonth < currMonth)) {
			    setReturnMessage(request,response, RES_FAILED, "prepaid-replenish-card-expired", "This card has already expired. Please use a different one.");
			    return response;
			}
			if(!PrepaidUtils.checkCvv(replenishSecurityCode)) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-cvv", "You entered an invalid security code. Please enter a valid one");
			    return response;
			}
			
			if(StringUtils.isBlank(address1)) {
				setReturnMessage(request,response, RES_FAILED,"prepaid-missing-address1", "You did not provide the required field address1. Please try again.");
				return response;
			}
			Holder<String> formattedPostal = new Holder<String>();
			if(!GenerateUtils.checkPostal(country, postal, true, formattedPostal)) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-postal", "You entered an invalid postal code for the country {1}. Please try again.", postal, country);
			    return response;
			}
			postal = formattedPostal.getValue();
			Format expFormat = ConvertUtils.getFormat("NUMBER:00");
			EC2AuthResponse resp = PrepaidUtils.setupReplenish(cardId, null, cardInfo.globalAccountId, cardInfo.consumerId, replenishType, amount, threshhold, replenishCardNum, expFormat.format(replenishExpYear % 100) + expFormat.format(replenishExpMonth), replenishSecurityCode, address1, postal);
			//@Todo will this be null?
			if(resp == null) {
				setReturnMessage(request,response, RES_OK, "prepaid-card-replenish-updated", "The replenishment amount settings have been updated.");
				return response;
			}
			String masked = PrepaidUtils.maskCardNumber(replenishCardNum);
			switch(resp.getReturnCode()) {
				case 2: /*APPROVED*/
			    	if(resp.getApprovedAmount() == 0)
			            setReturnMessage(request,response, RES_OK, "prepaid-card-replenished-authonly-success", "We have verified your credit card {1} and setup automatic replenishment to occur whenever your prepaid account balance falls below {2,CURRENCY}.", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked, threshhold);
			        else if(replenishType == 2)	
				        setReturnMessage(request,response, RES_OK, "prepaid-card-replenished-success", "We have scheduled a transaction to pull {0,CURRENCY} from your credit card {1} and to add that same amount to your prepaid account. Your statements will reflect this in next few minutes", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked);
				    else 
				        setReturnMessage(request,response, RES_OK, "prepaid-card-replenished-auto-success", "We have scheduled a transaction to pull {0,CURRENCY} from your credit card {1} and have setup automatic replenishment to occur whenever your prepaid account balance falls below {2,CURRENCY}. Your statements will reflect this in next few minutes", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked, threshhold);
			        break;
			    case 3:
			    	setReturnMessage(request,response, RES_FAILED, "prepaid-card-replenished-declined", "Your credit card {1} was declined. Please try another card.", masked);
			    	break;
			    case 0:
			    	setReturnMessage(request,response, RES_FAILED, "prepaid-card-replenished-failed", "Sorry for the inconvenience. We could not process your credit card {1} at this time. Please try again.", masked);
			    	break;
			    default:
			    	setReturnMessage(request,response, RES_FAILED, "prepaid-card-replenished-failed", "Sorry for the inconvenience. We could not process your credit card {1} at this time. Please try again.", masked);
			        break;
			}
			return response;
			
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending setupReplenish response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSReplenishInfoArray getReplenishInfo(String protocol, String username, String password, Long cardId, Long replenishId){
		log.info(new StringBuilder("Received getReplenishInfo request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", cardId: ").append(cardId).append(", replenishId: ").append(replenishId)
				.toString());
		PSReplenishInfoArray response = new PSReplenishInfoArray();
		ArrayList<PSReplenishInfo> replenishInfoList = new ArrayList<PSReplenishInfo>();
		ServletRequest request=getRequest(protocol);
		try {
			PSUser user = login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("cardId", cardId);
			if(replenishId!=null&&replenishId>0){
				params.put("replenishId", replenishId);
			}
			Results results = DataLayerMgr.executeQuery("GET_REPLENISH_INFO", params);
			while (results.next()) {
				PSReplenishInfo replenishInfo = new PSReplenishInfo();
				results.fillBean(replenishInfo);
				replenishInfoList.add(replenishInfo);
			} 
			PSReplenishInfo[] replenishInfoArray = new PSReplenishInfo[replenishInfoList.size()];
			replenishInfoList.toArray(replenishInfoArray);
			response.setReplenishInfoArray(replenishInfoArray);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		} catch (Exception e) {
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		} finally {
			log.info(new StringBuilder("Sending getReplenishInfo response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSResponse updateReplenish(String protocol, String username, String password, Long cardId, Long replenishId, BigDecimal amount, Integer replenishType, BigDecimal threshhold, Integer replenCount){
		log.info(new StringBuilder("Received updateReplenish request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", cardId: ").append(cardId).append(", replenishId: ").append(replenishId).append(", amount: ").append(amount)
				.append(", replenishType: ").append(replenishType).append(", threshhold: ").append(threshhold).append(", replenCount: ").append(replenCount)
				.toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			if(cardId == null || cardId < 0) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-card-id", "Card id was not provided or is invalid. Please include a valid card id.");
				return response;
			}
			CardInfo cardInfo = PrepaidUtils.checkCardPermission(user.getConsumerId(), cardId);
			if(cardInfo == null) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
			    return response;
			}
			
			if(amount != null && amount.compareTo(PrepaidUtils.getMinReplenishAmount()) < 0) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-replenish-amt-too-small", "The amount you entered is too small. Please enter at least {0}.", PrepaidUtils.getMinReplenishAmount());
				return response;
			} else if(amount.compareTo(PrepaidUtils.getMaxReplenishAmount()) > 0) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-replenish-amt-too-big", "The amount you entered is too much. Please enter at most {0}.", PrepaidUtils.getMaxReplenishAmount());
				return response;
			}
			if(replenishType == 1) {
				if(threshhold!=null&&threshhold.compareTo(PrepaidUtils.getMinThreshholdAmount()) < 0) {
					setReturnMessage(request,response, RES_FAILED, "prepaid-threshhold-amt-too-small", "The minimum balance you entered is too small. Please enter at least {0}.", PrepaidUtils.getMinThreshholdAmount());
				    return response;
				}
			}else if(replenishType == 2){ 
				threshhold = null;
			}else{
				setReturnMessage(request,response, RES_FAILED, "prepaid-replenish-type-not-allowed", "The replenishType {0} you entered is invalid. Please enter a valid one", replenishType);
			    return response;
			}
			Results results = DataLayerMgr.executeQuery("GET_REPLENISH_INFO", new Object[]{cardId});
			long existingReplenConsumerAcctId=-1;
			if(results.next()){
				existingReplenConsumerAcctId=ConvertUtils.getLongSafely(results.get("replenConsumerAcctId"), -1);
			}else{
				setReturnMessage(request,response, RES_FAILED, "prepaid-replenish-not-exists", "The replenishment for the card does not exist.");
			    return response;
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("replenishId", replenishId);
		    params.put("cardId", cardId);
			params.put("user.consumerId", user.getConsumerId());
		    params.put("replenishType", replenishType);
		    params.put("replenishAmount", amount);
		    params.put("replenishThreshhold", threshhold);
		    params.put("existingReplenConsumerAcctId", existingReplenConsumerAcctId);
		    params.put("replenConsumerAcctId", existingReplenConsumerAcctId);
		    params.put("replenishCount", replenCount);
		    DataLayerMgr.executeCall("UPDATE_REPLENISH", params, true);
		    boolean allowed = ConvertUtils.getBoolean(params.get("allowed"), false);
		    if(!allowed) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-card-not-users", "Your account is not registered with this card.");
				return response;
		    }else{
				setReturnMessage(request,response, RES_OK,"prepaid-card-replenish-updated", "The replenishment amount settings have been updated.");
		        return response;
		    }
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending updateReplenish response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSResponse requestReplenish(String protocol, String username, String password, Long cardId, Long replenishId, BigDecimal amount){
		log.info(new StringBuilder("Received requestReplenish request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", cardId: ").append(cardId).append(", replenishId: ").append(replenishId).append(", amount: ").append(amount)
				.toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			if(cardId == null || cardId < 0) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-card-id", "Card id was not provided or is invalid. Please include a valid card id.");
				return response;
			}
			CardInfo cardInfo = PrepaidUtils.checkCardPermission(user.getConsumerId(), cardId);
			if(cardInfo == null) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
			    return response;
			}
			
			if(amount != null && amount.compareTo(PrepaidUtils.getMinReplenishAmount()) < 0) {
				setReturnMessage(request,response, RES_FAILED, "prepaid-replenish-amt-too-small", "The amount you entered is too small. Please enter at least {0}.", PrepaidUtils.getMinReplenishAmount());
				return response;
			} else if(amount.compareTo(PrepaidUtils.getMaxReplenishAmount()) > 0) {
				setReturnMessage(request, response, RES_FAILED, "prepaid-replenish-amt-too-big", "The amount you entered is too much. Please enter at most {0}.", PrepaidUtils.getMaxReplenishAmount());
				return response;
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("replenishId", replenishId);
		    params.put("cardId", cardId);
			params.put("user.consumerId", user.getConsumerId());
		    params.put("replenishAmount", amount);
		    DataLayerMgr.executeCall("REQUEST_REPLENISH", params, true);
		    boolean allowed = ConvertUtils.getBoolean(params.get("allowed"), false);
		    if(!allowed) {
		    	setReturnMessage(request,response, RES_FAILED, "prepaid-card-not-users", "Your account is not registered with this card.");
				return response;
		    }else{
		    	String cardNum = ConvertUtils.getString(params.get("cardNum"), true);
				setReturnMessage(request,response, RES_OK, "prepaid-card-replenish-requested", "We have scheduled a transaction to pull {0,CURRENCY} from your credit card {1} and to add that same amount to your prepaid account. Your statements will reflect this in next few minutes", amount, cardNum);
		        return response;
		    }
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending requestReplenish response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSPromoCampaigns getPromoCampaigns(String protocol, String username, String password){
		log.info(new StringBuilder("Received getPromoCampaigns request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSPromoCampaigns response=new PSPromoCampaigns();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			ArrayList<PSPromoCampaign> promoCampaignList = new ArrayList<PSPromoCampaign>();
			Results results = PrepaidUtils.getPromoCampaigns(user.getConsumerId());
			while (results.next()) {
				PSPromoCampaign promoCampaign = new PSPromoCampaign();
				results.fillBean(promoCampaign);
				CampaignDetails detail=PrepaidUtils.getPromoCampaignDetails(results, RequestUtils.getTranslator((HttpServletRequest) request));
				promoCampaign.setDetails(detail.getDetails());
				promoCampaign.setTitle(detail.getTitle());
				setReturnMessage(request,promoCampaign, RES_OK,"prepaid-ws-ok", "OK");
				promoCampaignList.add(promoCampaign);
			} 
			PSPromoCampaign[] promoCampaigns = new PSPromoCampaign[promoCampaignList.size()];
			promoCampaignList.toArray(promoCampaigns);
			response.setPromoCampaigns(promoCampaigns);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending getPromoCampaigns response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSPromoCampaigns getLocations(String protocol, String username, String password, BigDecimal longitude, BigDecimal latitude){
		log.info(new StringBuilder("Received getLocations request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", longitude: ").append(longitude).append(", latitude: ").append(latitude)
				.toString());
		PSPromoCampaigns response=new PSPromoCampaigns();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			ArrayList<PSPromoCampaign> promoCampaignList = new ArrayList<PSPromoCampaign>();
			Results results = PrepaidUtils.getLocationsByCoordinates(user.getConsumerId(), longitude, latitude);
			while (results.next()) {
				PSPromoCampaign promoCampaign = new PSPromoCampaign();
				results.fillBean(promoCampaign);
				setReturnMessage(request,promoCampaign, RES_OK,"prepaid-ws-ok", "OK");
				promoCampaignList.add(promoCampaign);
			} 
			PSPromoCampaign[] promoCampaigns = new PSPromoCampaign[promoCampaignList.size()];
			promoCampaignList.toArray(promoCampaigns);
			response.setPromoCampaigns(promoCampaigns);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending getLocations response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSResponse resetPassword(String protocol, String username, String password){
		log.info(new StringBuilder("Received resetPassword request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try{
			/*PSUser user =  login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			*/
			PrepaidUtils.sendPasswordResetEmail(username, getBaseUrl(request), (HttpServletRequest) request);
	        setReturnMessage(request,response, RES_OK,"prepaid-reset-password-email-prompt", "Thank you. An email has been sent to you with a link to change your password.");
	        return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending resetPassword response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSConsumerSettings getConsumerSettings(String protocol, String username, String password) {
		log.info(new StringBuilder("Received getConsumerSettings request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSConsumerSettings response = new PSConsumerSettings();
		ServletRequest request=getRequest(protocol);
		ArrayList<PSConsumerSetting> settingList = new ArrayList<PSConsumerSetting>();
		try {
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			Results results = DataLayerMgr.executeQuery("GET_CONSUMER_SETTINGS", user);
			while (results.next()) {
				PSConsumerSetting setting=new PSConsumerSetting();
				results.fillBean(setting);
				settingList.add(setting);
				setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			} 
			PSConsumerSetting[] consumerSetting = new PSConsumerSetting[settingList.size()];
			settingList.toArray(consumerSetting);
			response.setConsumerSetting(consumerSetting);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		} catch (Exception e) {
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		} finally {
			log.info(new StringBuilder("Sending getConsumerSettings response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSResponse updateConsumerSettings(String protocol, String username, String password, PSConsumerSetting[] consumerSettings){
		log.info(new StringBuilder("Received updateConsumerSettings request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", consumerSettings: ").append(Arrays.toString(consumerSettings))
				.toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			
			ArrayList<Integer> allowedList=new ArrayList<Integer>();
			Results results=DataLayerMgr.executeQuery("GET_ALLOWED_CONSUMER_SETTING", new Object[]{user.getConsumerId()});
			while(results.next()){
				allowedList.add(ConvertUtils.getInt(results.get("consumerSettingParamId")));
			}
			Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_CONSUMER_SETTING");
			Map<String, Object> additionalParams;
		    if(consumerSettings==null){
		    	additionalParams = Collections.emptyMap();
	    	}else{
	    		additionalParams=new HashMap<String, Object>();
	    		for(PSConsumerSetting setting:consumerSettings){
	    			Integer settingId=ConvertUtils.getInt(setting.getSettingId());
	    			if(!allowedList.contains(settingId)){
	    				setReturnMessage(request,response, RES_FAILED,"prepaid-setting-not-allowed", "Consumer setting id {0} is allowed to modify for your account.", settingId);
	    				return response;
	    			}
	    			additionalParams.put(setting.getSettingId(), setting.getValue());
	    		}
	    	}
		    try{
				PrepaidUtils.updateUserPreferences(user.getConsumerId(), conn, additionalParams);
		    	conn.commit();
		    	setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			    return response;
		    }catch(SQLException e) {
		    	try {
		    		conn.rollback();
		    	} catch(SQLException e2) {
		    		//ignore
		    	}
		    	log.error("Error processing request", e);
				setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
				return response;
		    }
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending updateConsumerSettings response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSPostalInfo getPostalInfo(String protocol, String postalCd, String countryCd) {
		log.info(new StringBuilder("Received getPostalInfo request, protocol: ").append(protocol)
				.append(", postalCd: ").append(postalCd).append(", countryCd: ").append(countryCd)
				.toString());
		PSPostalInfo response = new PSPostalInfo();
		ServletRequest request=getRequest(protocol);
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("postalCd", postalCd);
			params.put("countryCd", countryCd);
			Results results = DataLayerMgr.executeQuery("LOOKUP_POSTAL", params);
			if (results.next()) {
				results.fillBean(response);
				setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			} else{
				log.error("Unable to retrieve user info");
				setReturnMessage(request,response, RES_FAILED,"prepaid-ws-no-user-info", "Unable to retrieve postal info.");
			}
			return response;
		} catch (Exception e) {
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		} finally {
			log.info(new StringBuilder("Sending getPostalInfo response ")
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSPromoCampaigns getLocationsByUsage(String protocol, String username, String password){
		log.info(new StringBuilder("Received getLocationsByUsage request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSPromoCampaigns response=new PSPromoCampaigns();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			ArrayList<PSPromoCampaign> promoCampaignList = new ArrayList<PSPromoCampaign>();
			Results results = PrepaidUtils.getRecentLocations(user.getConsumerId());
			while (results.next()) {
				PSPromoCampaign promoCampaign = new PSPromoCampaign();
				results.fillBean(promoCampaign);
				setReturnMessage(request,promoCampaign, RES_OK,"prepaid-ws-ok", "OK");
				promoCampaignList.add(promoCampaign);
			} 
			PSPromoCampaign[] promoCampaigns = new PSPromoCampaign[promoCampaignList.size()];
			promoCampaignList.toArray(promoCampaigns);
			response.setPromoCampaigns(promoCampaigns);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending getLocationsByUsage response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSReplenishmentArray getReplenishments(String protocol, String username, String password, Long cardId, Date startTime, Date endTime, Integer maxRows){
		log.info(new StringBuilder("Received getReplenishments request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", cardId: ").append(cardId).append(", startTime: ").append(startTime).append(", endTime: ").append(endTime)
				.append(", maxRows: ").append(maxRows).toString());
		PSReplenishmentArray response=new PSReplenishmentArray();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			if(cardId != null && cardId > 0) {
				CardInfo cardInfo = PrepaidUtils.checkCardPermission(user.getConsumerId(), cardId);
				if(cardInfo == null) {
					setReturnMessage(request, response, RES_FAILED, "prepaid-not-authorized", "You are not authorized to view this card. Please try logging in again.");
					return response;
				}
			}
			HashMap<String, Object> params=new HashMap<String,Object>();
			params.put("simple.servlet.ServletUser", user);
			if(cardId!=null && cardId>0){
				params.put("cardId", cardId);
			}
			params.put("startTime", startTime);
			params.put("endTime", endTime);
			if(maxRows!=null && maxRows>0){
				params.put("maxRows", maxRows);
			}else{
				params.put("maxRows", 10);
			}
			Results results = DataLayerMgr.executeQuery("GET_REPLENISHMENTS", params);
			results.trackAggregate("amount", Aggregate.SUM);
			results.trackAggregate("date", Aggregate.MAX);
			results.setFormat("replenishedCard", cardFormat);
			ArrayList<PSReplenishment> replenishments= new ArrayList<PSReplenishment>();
			while(results.next()) {
				PSReplenishment replenishment=new PSReplenishment();
				results.fillBean(replenishment);
				replenishments.add(replenishment);
			}
			PSReplenishment[] replenishmentArray = new PSReplenishment[replenishments.size()];
			replenishments.toArray(replenishmentArray);
			response.setReplenishmentArray(replenishmentArray);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending getReplenishments response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSChargedCardArray getChargedCards(String protocol, String username, String password) {
		log.info(new StringBuilder("Received getChargedCards request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSChargedCardArray response = new PSChargedCardArray();
		ArrayList<PSChargedCard> cards = new ArrayList<PSChargedCard>();
		ServletRequest request=getRequest(protocol);
		try {
			PSUser user = login(request, username, password, CONSUMER_TYPE_PREPAID, response);
			if(user == null)
				return response;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("consumerId", user.getConsumerId());
			Results results = DataLayerMgr.executeQuery("GET_CHARGE_CARDS", params);
			while (results.next()) {
				PSChargedCard card = new PSChargedCard();
				results.fillBean(card);
				setReturnMessage(request,card, RES_OK,"prepaid-ws-ok", "OK");
				cards.add(card);
			} 
			PSChargedCard[] chargedCards = new PSChargedCard[cards.size()];
			cards.toArray(chargedCards);
			response.setChargedCardArray(chargedCards);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		} catch (Exception e) {
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		} finally {
			log.info(new StringBuilder("Sending getChargedCards response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
		
	}
	
	public static PSAuthHoldArray getAuthHolds(String protocol, String username, String password, Long cardId){
		log.info(new StringBuilder("Received getAuthHolds request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", cardId: ").append(cardId).toString());
		PSAuthHoldArray response=new PSAuthHoldArray();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			if(cardId != null && cardId > 0) {
				CardInfo cardInfo = PrepaidUtils.checkCardPermission(user.getConsumerId(), cardId);
				if(cardInfo == null) {
					setReturnMessage(request, response, RES_FAILED, "prepaid-not-authorized", "You are not authorized to view this card. Please try logging in again.");
					return response;
				}
			}
			HashMap<String, Object> params=new HashMap<String,Object>();
			params.put("simple.servlet.ServletUser", user);
			if(cardId!=null && cardId>0){
				params.put("cardId", cardId);
			}
			Results results = DataLayerMgr.executeQuery("GET_AUTH_HOLDS", params);
			results.setFormat("card", cardFormat);
			ArrayList<PSAuthHold> authHolds= new ArrayList<PSAuthHold>();
			while(results.next()) {
				PSAuthHold authHold=new PSAuthHold();
				results.fillBean(authHold);
				authHolds.add(authHold);
			}
			PSAuthHold[] authHoldArray = new PSAuthHold[authHolds.size()];
			authHolds.toArray(authHoldArray);
			response.setAuthHoldArray(authHoldArray);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending getAuthHolds response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSPromoCampaigns getLocationsByLocation(String protocol, String username, String password,String postal, String country, float maxProximityMiles, boolean discountsOnly){
		log.info(new StringBuilder("Received getLocationsByLocation request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", postal: ").append(postal).append(", country: ").append(country).append(", maxProximityMiles: ").append(maxProximityMiles)
				.append(", discountsOnly: ").append(discountsOnly).toString());
		PSPromoCampaigns response=new PSPromoCampaigns();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user = login(request, username, password, null, response);
			if(user == null)
				return response;
			if(postal==null||country==null||Float.isNaN(maxProximityMiles)||maxProximityMiles<0){
				setReturnMessage(request, response, RES_FAILED, "prepaid-could-not-process", "You need to provide valid postal, country and maxProximityMiles. Please try again.");
				return response;
			}
			ArrayList<PSPromoCampaign> promoCampaignList = new ArrayList<PSPromoCampaign>();
			Results results = PrepaidUtils.getLocations(user.getConsumerId(), null, null, null, postal, country, maxProximityMiles, discountsOnly);
			while (results.next()) {
				PSPromoCampaign promoCampaign = new PSPromoCampaign();
				results.fillBean(promoCampaign);
				setReturnMessage(request,promoCampaign, RES_OK,"prepaid-ws-ok", "OK");
				promoCampaignList.add(promoCampaign);
			} 
			PSPromoCampaign[] promoCampaigns = new PSPromoCampaign[promoCampaignList.size()];
			promoCampaignList.toArray(promoCampaigns);
			response.setPromoCampaigns(promoCampaigns);
			setReturnMessage(request,response, RES_OK,"prepaid-ws-ok", "OK");
			return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending getLocationsByLocation response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSTokenResponse retokenize(String protocol, String username, String password, long cardId, String securityCode, String attributes) {
		log.info(new StringBuilder("Received retokenize request, protocol: ").append(protocol).append(", username: ").append(username).toString());

		PSTokenResponse response = new PSTokenResponse();
		ServletRequest request = getRequest(protocol);
		try{
			PSUser user = login(request, username, password, null, response);
			if(user != null) {
				// create the cardHolder
				String cardHolder = PrepaidUtils.namePartsToCardHolderName(user.getFirstname(), "", user.getLastname());
				
				// turn the cardId (consumerAcctId) into a globalAccountId
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("consumerAcctId", cardId);
				Results results = DataLayerMgr.executeQuery("GET_CONSUMER_ACCT_BASE", params);
				PSConsumerAccountBase consumerAccountBase = new PSConsumerAccountBase();
				if (results.next()) {
					results.fillBean(consumerAccountBase);
					response.setCardId(consumerAccountBase.getConsumerAcctId());
					response.setGlobalAccountId(consumerAccountBase.getGlobalAccountId());
					if(StringUtils.isBlank(securityCode)) {
						if(user.getConsumerTypeId() == CONSUMER_TYPE_LOYALTY)
							securityCode = "----";
						else {
							setReturnMessage(request, response, RES_FAILED, "prepaid-empty-security-code", "Security Code is required to validate this card");
							return response;
						}
					}

					EC2TokenResponse tokenResponse = EC2ClientUtils.getEportConnect().retokenize(username, 
																								 password, 
																								 PrepaidUtils.currencyCdToPrepaidDeviceSerialCd(consumerAccountBase.getCurrencyCd()), 
																								 consumerAccountBase.getGlobalAccountId(), 
																								 securityCode, 
																								 cardHolder, 
																								 user.getPostal(), 
																								 user.getAddress1(), 
																								 attributes);
					
					if (tokenResponse.getReturnCode() != ECResponse.RES_APPROVED) {
						setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-token", "Unable to fetch security token for card. {1}", tokenResponse.getReturnMessage());
					} else {
						response.setTokenHex(tokenResponse.getTokenHex());
						setReturnMessage(request, response, RES_OK, "prepaid-ws-ok", "Token retreived for cardId {0}", cardId);
					}
				} else {
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-token", "Unable to find consumer account.");
				}
			}
		} catch (Exception e) {
			log.error("retokenize exception: " + e);
		}finally {
			log.info(new StringBuilder("Sending retokenize response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
		
		return response;
	}
	
	public static PSResponse addPromoCode(String protocol, String username, String password, String promoCode){
		log.info(new StringBuilder("Received addPromoCode request, protocol: ").append(protocol).append(", username: ").append(username).toString());
		PSResponse response=new PSResponse();
		ServletRequest request=getRequest(protocol);
		try{
			PSUser user =  login(request, username, password, CONSUMER_TYPE_LOYALTY, response);
			if(user == null)
				return response;
			try{
				if(!StringUtils.isBlank(promoCode)&&promoCode.trim().matches(PrepaidUtils.PROMO_CODE_CHECK)){
					promoCode=promoCode.trim();
			    	DataLayerMgr.executeCall("ADD_PROMO", new Object[]{promoCode, user.getConsumerId()}, true);
				}else {
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-promoCode", "You entered an invalid promoCode: {0}. Please enter a valid one", promoCode);
					return response;
				}
			}catch(SQLException e) {
				log.error("Error processing request", e);
				switch(e.getErrorCode()) {
			        case 20310:
			        	setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-promo-code", "You entered an invalid promoCode: {0}. Please enter a valid one", promoCode);
			            break;
			        case 20311:
			        	setReturnMessage(request, response, RES_FAILED, "prepaid-promo-code-exists", "The promotion code you entered is already associated with the card. Please enter a new one.");
			            break;
			        default:
			        	setReturnMessage(request, response, RES_FAILED, "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");
				}
				return response;
			}
	        setReturnMessage(request,response, RES_OK,"prepaid-add-promoCode-success", "Successfully added the promoCode for the user.");
	        return response;
		}catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending addPromoCode response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSTokenResponse registerAnotherCardKeyed(String protocol, String username, String password, String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode, String promoCode){
		log.info(new StringBuilder("Received registerAnotherCardKeyed request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", cardNum: ").append(MessageResponseUtils.maskCardNumber(cardNum))
				.toString());
		if(!StringUtils.isBlank(cardNum)&&cardNum.startsWith(CommonConstants.USAT_IIN)){
			//prepaid card
			return registerAnotherCard(protocol, username, password, cardNum, securityCode);
		}else{
			PSTokenResponse response=new PSTokenResponse();
			ServletRequest request=getRequest(protocol);
			try {
				PSUser user = login(request, username, password, CONSUMER_TYPE_LOYALTY, response);
				if(user == null)
					return response;
				if(!validateCreditFields(request, response,billingCountry, billingAddress, billingPostalCode, null, false)){
					return response;
				} 
				if(!validateExpirationDateFields(request, response, expMonth, expYear)) {
					return response;
				}
				Format expFormat = ConvertUtils.getFormat("NUMBER:00");
				String expDate=expFormat.format(expYear % 100) + expFormat.format(expMonth);
				
				if(!validateCardFields(request, response, cardNum, securityCode, false)){
					return response;
				}
				String virtualAllCardDeviceSerialCd=null;
				String currencyCd;
				if(billingCountry.equals("US")){
					currencyCd="USD";
					virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-USD";
				}else if(billingCountry.equals("CA")){
					currencyCd="CAD";
					virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-CAD";
				}else{
					setReturnMessage(request,response, RES_FAILED,"all-card-invalid-country", "Billing country is invalid. Please try again.");
					return response;
				}
				String track = PrepaidUtils.getTrack(cardNum,securityCode,expDate, user.getFirstname()+" "+user.getLastname(),billingPostalCode);
				EC2CardInfo infoResp = EC2ClientUtils.getEportConnect().getCardInfoPlain(null,null, virtualAllCardDeviceSerialCd, track, "N", null);
				if(infoResp == null) {
					setReturnMessage(request,response, RES_FAILED,"all-card-auth-failed", "Sorry for the inconvenience. We could not process your credit card at this time. Please try again.");
					return response;
				}
				String masked = PrepaidUtils.maskCardNumber(cardNum);
				switch(infoResp.getReturnCode()) {
					case 1: //APPROVED
					case 2: //APPROVED
				        break;
				    case 3:
				    	setReturnMessage(request,response, RES_FAILED,"all-card-info-declined", "Your credit card {0} was declined. Please try another card.", masked);
						return response;
				    case 0:
				    	setReturnMessage(request,response, RES_FAILED,"all-card-info-failed",  "Your credit card {0} was declined. Please try another card.", masked);
						return response;
				    default:
				    	setReturnMessage(request,response, RES_FAILED,"all-card-info-failed",  "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
						return response;
				}
				long globalAccountId=infoResp.getCardId();
				long consumerAcctId=PrepaidUtils.addCreditCard(user.getConsumerId(), globalAccountId, currencyCd, expDate, "", promoCode);
				response.setCardId(consumerAcctId);
				response.setGlobalAccountId(globalAccountId);
				setReturnMessage(request,response, RES_OK,"prepaid-card-registered-prompt", "This credit card ({0}) has been linked to your account.", masked);
		        return response;
			} catch(SQLException e) {
				log.error("Error processing request", e);
				response.setReturnCode(RES_FAILED);
			   	switch(e.getErrorCode()) {
			   	case 20300: case 20305:
		            setReturnMessage(request,response, RES_FAILED,"prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it");
		            break;
		        case 20301:
		   			setReturnMessage(request,response, RES_FAILED,"prepaid-already-registered", "You have already registered this card. Please enter a different card");
		            break;
		        case 20302:
		            setReturnMessage(request,response, RES_FAILED,"prepaid-registered-to-other", "This card has been registered under a different email or mobile number. Please contact Customer Service for assistance");
		            break;
		        case 20304:
		        	setReturnMessage(request,response, RES_FAILED,"prepaid-registered-to-other", "prepaid-not-active", "Your account is not active. Please contact Customer Service for assistance");
		        	break;
		        case 1:
		        	setReturnMessage(request,response, RES_FAILED,"prepaid-nickname-unique", "The nickname needs to be unique. Please try with a different nickname.");
		        	break;
		        default:
		           	setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
			   	}
			   	return response;
			} catch(Exception e){
				log.error("Error processing request", e);
				setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
				return response;
			}finally {
				log.info(new StringBuilder("Sending registerAnotherCardKeyed response, username: ").append(username)
						.append(StringUtils.objectToString(response, null)).toString());
			}
		}
	}
	
	public static PSTokenResponse registerAnotherCardKeyed(String protocol, String username, String password, String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode){
		return registerAnotherCardKeyed(protocol, username, password, cardNum, securityCode, expMonth, expYear, billingCountry, billingAddress, billingPostalCode, null);
	}
	
	public static PSTokenResponse createUserEncrypted(String protocol, String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings, String promoCode){
		String username=null;
		if(preferredCommType==1){
			username=email;
		}else{
			username=mobile;
		}
		log.info(new StringBuilder("Received createUserEncrypted request, protocol: ").append(protocol).append(", username: ").append(username)
				.toString());
		PSTokenResponse response=new PSTokenResponse();
		ServletRequest request=getRequest(protocol);
		if(!validateEncryptedFields(request, response, entryType, cardReaderType, decryptedCardDataLen, encryptedCardDataHex,ksnHex, billingCountry)) {
			return response;
		} 
		if(!validateCreateUserFields(request, response, preferredCommType, email,mobile, carrierId, firstname, lastname, address1, city, state, postal, country, password, confirmPassword)){
			return response;
		}
		String currencyCd;
		String virtualAllCardDeviceSerialCd;
		
		if(billingCountry.equals("US")){
			currencyCd="USD";
			virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-USD";
		}else if(billingCountry.equals("CA")){
			currencyCd="CAD";
			virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-CAD";
		}else{
			setReturnMessage(request,response, RES_FAILED,"all-card-invalid-country", "Billing country is invalid. Please try again.");
			return response;
		}
		
		EC2CardInfo infoResp = EC2ClientUtils.getEportConnect().getCardInfoEncrypted(null, null, virtualAllCardDeviceSerialCd, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, null);
		if(infoResp == null) {
			setReturnMessage(request,response, RES_FAILED,"all-card-auth-failed", "Sorry for the inconvenience. We could not process your credit card at this time. Please try again.");
			return response;
		}
		log.info("getCardInfoEncrypted returnCode="+infoResp.getReturnCode());
		boolean isMoreCard=false;
		if(infoResp.getCardType()!=null&& infoResp.getCardType().equals("MORE Prepaid")){
			isMoreCard=true;
		}
		switch(infoResp.getReturnCode()) {
			case 1: //APPROVED
			case 2: //APPROVED
				 break;
			case 0: // for prepaid location not allowed 
				if(isMoreCard){
					break;
				}else{
					setReturnMessage(request,response, RES_FAILED,"all-card-info-declined", "Your card was declined. Please try another card.");
					return response;
				}
		    case 3:
		    	setReturnMessage(request,response, RES_FAILED,"all-card-info-declined", "Your card was declined. Please try another card.");
				return response;
		    default:
		    	setReturnMessage(request,response, RES_FAILED,"all-card-info-failed",  "Sorry for the inconvenience. We could not process your credit card at this time. Please try again.");
				return response;
		}
		long globalAccountId=infoResp.getCardId();
		if(globalAccountId==0){
			setReturnMessage(request,response, RES_FAILED,"all-card-info-failed",  "Sorry for the inconvenience. We could not process your card at this time. Please try again.");
			return response;
		}
		try{
			if(isMoreCard){
				if(!validateConsumerTypePrepaid(request, response, globalAccountId,currencyCd )){
					return response;
				}
				String virtualDeviceSerialNumber = PrepaidUtils.currencyCdToPrepaidDeviceSerialCd(currencyCd);
				long tranId = PrepaidUtils.nextTransactionId(virtualDeviceSerialNumber);
				EC2TokenResponse tokenResponse = EC2ClientUtils.getEportConnect().tokenizeEncrypted(username, password, virtualDeviceSerialNumber, tranId, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingPostalCode, billingAddress, null);
				if (tokenResponse.getReturnCode() != ECResponse.RES_APPROVED) {
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-token", "Unable to create security token for card. {1}", tokenResponse.getReturnMessage());
					return response;
				} else {
					    Map<String, Object> additionalParams;
					    if(consumerSettings==null){
					    	additionalParams = Collections.emptyMap();
				    	}else{
				    		additionalParams=new HashMap<String, Object>();
				    		for(PSConsumerSetting setting:consumerSettings){
				    			additionalParams.put(setting.getSettingId(), setting.getValue());
				    		}
				    	}
						ConsumerAccount consumerAccount = PrepaidUtils.createUser(RequestUtils.getTranslator((HttpServletRequest) request), globalAccountId, null, email, mobile, carrierId, preferredCommType, firstname, lastname, address1, city, state, postal, country, password, null, getBaseUrl(request), additionalParams, (HttpServletRequest) request, 2,null, null,null, null);
						response.setCardId(consumerAccount.getConsumerAcctId());
						response.setGlobalAccountId(globalAccountId);
						response.setTokenHex(tokenResponse.getTokenHex());
						if(preferredCommType==1){
							setReturnMessage(request, response, RES_OK, "prepaid-registration-email-prompt", "To receive email messages, please click the link in the email that has been sent to you.");
						}else{
							setReturnMessage(request, response, RES_OK, "prepaid-registration-sms-prompt", "To receive text messages, please enter the passcode found in the text message that has been sent to you.");
						}
				        return response;
				}
			}else{
				
				if(!validateCreditFields(request, response,billingCountry, billingAddress, billingPostalCode, promoCode, true)){
					return response;
				} 
			    
			    Map<String, Object> additionalParams;
			    if(consumerSettings==null){
			    	additionalParams = Collections.emptyMap();
		    	}else{
		    		additionalParams=new HashMap<String, Object>();
		    		for(PSConsumerSetting setting:consumerSettings){
		    			additionalParams.put(setting.getSettingId(), setting.getValue());
		    		}
		    	}
			    promoCode=promoCode.trim();
				ConsumerAccount consumerAcct = PrepaidUtils.createUser(RequestUtils.getTranslator((HttpServletRequest) request), globalAccountId, null, email, mobile, carrierId, preferredCommType, firstname, lastname, address1, city, state, postal, country, password, null, RequestUtils.getBaseUrl(request), null, (HttpServletRequest)request, 1, null, promoCode, currencyCd, null);
				response.setCardId(consumerAcct.getConsumerAcctId());
				response.setGlobalAccountId(globalAccountId);
				if(preferredCommType==1){
					setReturnMessage(request, response, RES_OK, "prepaid-registration-email-prompt", "To receive email messages, please click the link in the email that has been sent to you.");
				}else{
					setReturnMessage(request, response, RES_OK, "prepaid-registration-sms-prompt", "To receive text messages, please enter the passcode found in the text message that has been sent to you.");
				}
		        return response;
			}
		}catch(SQLException e) {
			log.error("Error processing request", e);
			response.setReturnCode(RES_FAILED);
		   	switch(e.getErrorCode()) {
		   		case 20300:
	   			   setReturnMessage(request,response, RES_FAILED,"prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it", username);
	               break;
			   case 20301:
				   setReturnMessage(request,response, RES_FAILED,"prepaid-already-registered", "You have already registered this card. Please enter a different card", username);
			       break;
			   case 20302:
				   setReturnMessage(request,response, RES_FAILED,"prepaid-registered-to-other", "This card has been registered under a different email or mobile number. Please contact Customer Service for assistance", username);
			       break;
			   case 20303:
				   setReturnMessage(request,response, RES_FAILED, "prepaid-username-not-unique", "You have already registered a different card for this email or mobile number. Please log on and add the card there to your existing account", username);
			       break;
			   case 20309:
				   setReturnMessage(request,response, RES_FAILED,"prepaid-invalid-state", "The state you entered is invalid. Please re-enter it.", state);
			       break;
			   case 20310:
				   setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-promo-code", "The promotion code you entered is invalid.", promoCode);
			       break;
			   default:
				   setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
		   	}
		   	return response;
		} catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending createUserEncrypted response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
	}
	
	public static PSTokenResponse registerAnotherCardEncrypted(String protocol, String username, String password, String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode, String promoCode){
		log.info(new StringBuilder("Received registerAnotherCardEncrypted request, protocol: ").append(protocol).append(", username: ").append(username)
				.toString());
		PSTokenResponse response=new PSTokenResponse();
		ServletRequest request=getRequest(protocol);
		PSUser user = login(request, username, password, null, response);
		if(user == null)
			return response;
		if(!validateEncryptedFields(request, response, entryType, cardReaderType, decryptedCardDataLen, encryptedCardDataHex,ksnHex, billingCountry)) {
			return response;
		} 
		String currencyCd;
		String virtualAllCardDeviceSerialCd;
		
		if(billingCountry.equals("US")){
			currencyCd="USD";
			virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-USD";
		}else if(billingCountry.equals("CA")){
			currencyCd="CAD";
			virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-CAD";
		}else{
			setReturnMessage(request,response, RES_FAILED,"all-card-invalid-country", "Billing country is invalid. Please try again.");
			return response;
		}
		EC2CardInfo infoResp = EC2ClientUtils.getEportConnect().getCardInfoEncrypted(null, null, virtualAllCardDeviceSerialCd, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, entryType, null);
		if(infoResp == null) {
			setReturnMessage(request,response, RES_FAILED,"all-card-auth-failed", "Sorry for the inconvenience. We could not process your credit card at this time. Please try again.");
			return response;
		}
		log.info("getCardInfoEncrypted returnCode="+infoResp.getReturnCode());
		boolean isMoreCard=false;
		if(infoResp.getCardType()!=null&& infoResp.getCardType().equals("MORE Prepaid")){
			isMoreCard=true;
		}
		
		switch(infoResp.getReturnCode()) {
			case 1: //APPROVED
			case 2: //APPROVED
			case 0: // for prepaid location not allowed 
				if(isMoreCard){
					break;
				}else{
					setReturnMessage(request,response, RES_FAILED,"all-card-info-declined", "Your card was declined. Please try another card.");
					return response;
				}
		    case 3:
		    	setReturnMessage(request,response, RES_FAILED,"all-card-info-declined", "Your card was declined. Please try another card.");
				return response;
		    default:
		    	setReturnMessage(request,response, RES_FAILED,"all-card-info-failed",  "Sorry for the inconvenience. We could not process your card at this time. Please try again.");
				return response;
		}
		long globalAccountId=infoResp.getCardId();
		if(globalAccountId==0){
			setReturnMessage(request,response, RES_FAILED,"all-card-info-failed",  "Sorry for the inconvenience. We could not process your card at this time. Please try again.");
			return response;
		}
		try{
			if(isMoreCard){
				if(user.getConsumerTypeId()!=CONSUMER_TYPE_PREPAID){
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-card-type", "The user credential used is not valid to register this card type {0}", "MORE Prepaid");
				}
				if(!validateConsumerTypePrepaid(request, response, globalAccountId,currencyCd )){
					return response;
				}
				String virtualDeviceSerialNumber = PrepaidUtils.currencyCdToPrepaidDeviceSerialCd(currencyCd);
				long tranId = PrepaidUtils.nextTransactionId(virtualDeviceSerialNumber);
				EC2TokenResponse tokenResponse = EC2ClientUtils.getEportConnect().tokenizeEncrypted(username, password, virtualDeviceSerialNumber, tranId, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingPostalCode, billingAddress, null);
				if (tokenResponse.getReturnCode() != ECResponse.RES_APPROVED) {
					setReturnMessage(request, response, RES_FAILED, "prepaid-invalid-token", "Unable to create security token for card. {1}", tokenResponse.getReturnMessage());
				} else {
					long consumerAcctId=PrepaidUtils.addCard(user.getConsumerId(), globalAccountId, null);
					response.setCardId(consumerAcctId);
					response.setGlobalAccountId(globalAccountId);
					response.setTokenHex(tokenResponse.getTokenHex());
					setReturnMessage(request,response, RES_OK,"prepaid-card-registered-prompt", "This prepaid card has been linked to your account.");
				}
		        return response;
			}else{
				
				long consumerAcctId=PrepaidUtils.addCreditCard(user.getConsumerId(), globalAccountId, currencyCd, null, "", promoCode);
				response.setCardId(consumerAcctId);
				response.setGlobalAccountId(globalAccountId);
				setReturnMessage(request,response, RES_OK,"prepaid-card-registered-prompt", "This credit card has been linked to your account.");
		        return response;
			}
		} catch (SQLException e){
			log.error("Error processing request", e);
			switch(e.getErrorCode()) {
				case 20300: case 20305:
		   			setReturnMessage(request,response, RES_FAILED,"prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it");
		   			break;
		        case 20301:
		        	setReturnMessage(request,response, RES_FAILED, "prepaid-already-registered", "You have already registered this card. Please enter a different card");
		        	break;
		        case 20302:
		        	setReturnMessage(request,response, RES_FAILED,"prepaid-registered-to-other", "This card has been registered under a different email or mobile number. Please enter a different card");
		        	break;
		        case 20304:
		        	setReturnMessage(request,response, RES_FAILED,"prepaid-registered-to-other", "prepaid-not-active", "Your account is not active. Please contact Customer Service for assistance");
		        	break;
		        case 1:
		        	setReturnMessage(request,response, RES_FAILED,"prepaid-nickname-unique", "The nickname needs to be unique. Please try with a different nickname.");
		        	break;
		        default:
		        	setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");
			}
			return response;
		} catch(Exception e){
			log.error("Error processing request", e);
			setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			return response;
		}finally {
			log.info(new StringBuilder("Sending registerAnotherCardEncrypted response, username: ").append(username)
					.append(StringUtils.objectToString(response, null)).toString());
		}
		
	}
	
	public static PSTokenResponse registerAnotherCardEncrypted(String protocol, String username, String password, String entryType, int cardReaderType, int decryptedCardDataLen, String encryptedCardDataHex, String ksnHex, String billingCountry, String billingAddress, String billingPostalCode){
		return registerAnotherCardEncrypted(protocol, username,  password, entryType, cardReaderType, decryptedCardDataLen, encryptedCardDataHex, ksnHex, billingCountry, billingAddress, billingPostalCode, null);
	}
	
	public static PSTokenResponse createUserKeyed(String protocol, String cardNum, String securityCode, Integer expMonth, Integer expYear, String billingCountry, String billingAddress, String billingPostalCode, int preferredCommType, String email, String mobile, int carrierId, String firstname, String lastname, String address1, String city, String state, String postal, String country, String password, String confirmPassword, PSConsumerSetting[] consumerSettings, String promoCode){
		String username=null;
		if(preferredCommType==1){
			username=email;
		}else{
			username=mobile;
		}
		log.info(new StringBuilder("Received createUserKeyed request, protocol: ").append(protocol).append(", username: ").append(username)
				.append(", cardNum: ").append(MessageResponseUtils.maskCardNumber(cardNum))
				.append(", consumerSettings: ").append(Arrays.toString(consumerSettings))
				.toString());
		if(!StringUtils.isBlank(cardNum)&&cardNum.startsWith(CommonConstants.USAT_IIN)){
			//prepaid card
			return createUser(protocol, cardNum, securityCode, preferredCommType, email, mobile, carrierId, firstname, lastname, address1, city, state, postal, country, password, confirmPassword, consumerSettings);
		}else{
			PSTokenResponse response=new PSTokenResponse();
			ServletRequest request = getRequest(protocol);
			if(!validateCreateUserFields(request, response, preferredCommType, email,mobile, carrierId, firstname, lastname, address1, city, state, postal, country, password, confirmPassword)){
				return response;
			}
			if(!validateCreditFields(request, response,billingCountry, billingAddress, billingPostalCode, promoCode, true)){
				return response;
			} 
			if(!validateExpirationDateFields(request, response, expMonth, expYear)) {
				return response;
			}
			Format expFormat = ConvertUtils.getFormat("NUMBER:00");
			String expDate=expFormat.format(expYear % 100) + expFormat.format(expMonth);
			
			if(!validateCardFields(request, response, cardNum, securityCode, false)){
				return response;
			}
			
			try {
				String currencyCd=null;
				String virtualAllCardDeviceSerialCd=null;
				if(billingCountry.equals("US")){
					currencyCd="USD";
					virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-USD";
				}else if(billingCountry.equals("CA")){
					currencyCd="CAD";
					virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-CAD";
				}else{
					setReturnMessage(request,response, RES_FAILED,"all-card-invalid-country", "Billing country is invalid. Please try again.");
					return response;
				}
			    Map<String, Object> additionalParams;
			    if(consumerSettings==null){
			    	additionalParams = Collections.emptyMap();
		    	}else{
		    		additionalParams=new HashMap<String, Object>();
		    		for(PSConsumerSetting setting:consumerSettings){
		    			additionalParams.put(setting.getSettingId(), setting.getValue());
		    		}
		    	}
			    
			    String track = PrepaidUtils.getTrack(cardNum,securityCode,expDate, firstname+" "+lastname,billingPostalCode);
				EC2CardInfo infoResp = EC2ClientUtils.getEportConnect().getCardInfoPlain(null,null, virtualAllCardDeviceSerialCd, track, "N", null);
				if(infoResp == null) {
					setReturnMessage(request,response, RES_FAILED,"all-card-auth-failed", "Sorry for the inconvenience. We could not process your credit card at this time. Please try again.");
					return response;
				}
				String masked = PrepaidUtils.maskCardNumber(cardNum);
				switch(infoResp.getReturnCode()) {
					case 1: //APPROVED
					case 2: //APPROVED
				        break;
				    case 3:
				    	setReturnMessage(request,response, RES_FAILED,"all-card-info-declined", "Your credit card {0} was declined. Please try another card.", masked);
						return response;
				    case 0:
				    	setReturnMessage(request,response, RES_FAILED,"all-card-info-failed",  "Your credit card {0} was declined. Please try another card.", masked);
						return response;
				    default:
				    	setReturnMessage(request,response, RES_FAILED,"all-card-info-failed",  "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
						return response;
				}
				long globalAccountId=infoResp.getCardId();
				ConsumerAccount consumerAcct = PrepaidUtils.createUser(RequestUtils.getTranslator((HttpServletRequest) request), globalAccountId, securityCode, email, mobile, carrierId, preferredCommType, firstname, lastname, address1, city, state, postal, country, password, null, RequestUtils.getBaseUrl(request), null, (HttpServletRequest)request, 1, expDate, promoCode, currencyCd, null);
				response.setCardId(consumerAcct.getConsumerAcctId());
				response.setGlobalAccountId(globalAccountId);
				if(preferredCommType==1){
					setReturnMessage(request, response, RES_OK, "prepaid-registration-email-prompt", "To receive email messages, please click the link in the email that has been sent to you.");
				}else{
					setReturnMessage(request, response, RES_OK, "prepaid-registration-sms-prompt", "To receive text messages, please enter the passcode found in the text message that has been sent to you.");
				}
		        return response;
			} catch(SQLException e) {
				log.error("Error processing request", e);
				response.setReturnCode(RES_FAILED);
			   	switch(e.getErrorCode()) {
			   		case 20300:
			   			   setReturnMessage(request,response, RES_FAILED,"prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it", username);
			               break;
			           case 20301:
			        	   setReturnMessage(request,response, RES_FAILED,"prepaid-already-registered", "You have already registered this card. Please enter a different card", username);
			               break;
			           case 20302:
			        	   setReturnMessage(request,response, RES_FAILED,"prepaid-registered-to-other", "This card has been registered under a different email or mobile number. Please contact Customer Service for assistance", username);
			               break;
			           case 20303:
			        	   setReturnMessage(request,response, RES_FAILED, "prepaid-username-not-unique", "You have already registered a different card for this email or mobile number. Please log on and add the card there to your existing account", username);
			               break;
			           case 20309:
			        	   setReturnMessage(request,response, RES_FAILED,"prepaid-invalid-state", "The state you entered is invalid. Please re-enter it.", state);
			               break;
			           case 20310:
						   setReturnMessage(request,response, RES_FAILED, "prepaid-invalid-promo-code", "The promotion code you entered is invalid.", promoCode);
					       break;
			           default:
			        	   setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
			   	}
			   	return response;
			} catch(Exception e){
				log.error("Error processing request", e);
				setReturnMessage(request,response, RES_FAILED,"prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance.");
				return response;
			} finally {
				log.info(new StringBuilder("Sending createUser response, username: ").append(username)
						.append(StringUtils.objectToString(response, null)).toString());
			}
		}
	}
}