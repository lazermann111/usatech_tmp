package com.usatech.prepaid.test;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import com.usatech.prepaid.client.PrepaidJsonClient;
import com.usatech.ps.PSConsumerSetting;
import com.usatech.ps.PSPrepaidActivities;
import com.usatech.ps.PSReplenishmentArray;
import com.usatech.ps.PSResponse;
import com.usatech.ps.PSUser;
import com.usatech.ps.PrepaidServiceAPI;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

public class PrepaidServiceJsonTest {
	public static final String PS_JSON_URL = "http://localhost:8580/json/ps";
	//public static final String PS_JSON_URL = "https://getmore-dev.usatech.com/json/ps";
	//public static final String PS_JSON_URL = "https://getmore-int.usatech.com/json/ps";
	//public static final String PS_JSON_URL = "https://getmore-ecc.usatech.com/json/ps";

    private static PrepaidServiceAPI service;

    private static final String TEST_USERNAME = "yhe888@usatech.com";
    private static final String TEST_PASSWORD = "Password1";
    private static final String TEST_PASSCODE = "ZwUHqseaDB";

    private static final String PREPAID_CARD_NUM_1 = "6396212009701971403";
    //private static final String PREPAID_CARD_NUM_1 = "6396212009816772702";
    private static final String PREPAID_SECURITY_CODE = "9901";

    private static final int PREFERRED_COMM_TYPE = 2;
    private static final String MOBILE = "1111111111";
    private static final int CARRIER_ID = 4;

    private static final String ADDRESS_1 = "100 Deerfield Lane";
    private static final String CITY = "Malvern";
    private static final String COUNTRY = "US";
    private static final String FIRSTNAME = "PrepaidTest";
    private static final String LASTNAME = "Test3";
    private static final String POSTAL = "19355";
    private static final String STATE = "PA";
    private static final PSConsumerSetting[] CONSUMER_SETTINGS = new PSConsumerSetting[1];

    static {
        CONSUMER_SETTINGS[0] = new PSConsumerSetting("1", "N");
    }

    @Rule
    public Timeout globalTimeout = Timeout.seconds(60); // 30 seconds max per method tested

    static {
        System.setProperty("app.servicename", "PrepaidServiceJsonTest");
        //Uncomment trustStore properties below for Dev that uses a certificate signed by USAT internal Certification Authority
      	/*System.setProperty("javax.net.ssl.trustStore", "../LayersCommon/conf/net/truststore.ts");
      	System.setProperty("javax.net.ssl.trustStorePassword", "usatech");*/
        System.setProperty("log4j.configuration", "log4j-dev.properties");
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
        System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
        System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");
    }

    @Test
    public void getUser() {
        PSUser user = getService().getUser(TEST_USERNAME, TEST_PASSWORD);
        Assert.assertNotNull("Failed to get user", user);
        Assert.assertEquals("Failed to get user", 1, user.getReturnCode());
    }

    @Test
    public void verifyUser() {
        PSResponse response = getService().verifyUser(TEST_USERNAME, TEST_PASSWORD, TEST_PASSCODE);
        Assert.assertNotNull("Failed to verify user", response);
        Assert.assertEquals("Failed to verify user", 1, response.getReturnCode());
    }

    @Test
    public void getRegions() {
        PSResponse response = getService().getRegions(TEST_USERNAME, TEST_PASSWORD);
        Assert.assertNotNull("Failed to get regions", response);
        Assert.assertEquals("Failed to get regions", 1, response.getReturnCode());
    }

    @Test
    public void updateRegion() {
        PSResponse response = getService().updateRegion(TEST_USERNAME, TEST_PASSWORD, 296);
        Assert.assertNotNull("Failed to update regions", response);
        Assert.assertEquals("Failed to update regions", 1, response.getReturnCode());
    }

    @Test
    public void getPrepaidAccounts() {
        PSResponse response = getService()
                .getPrepaidAccounts(TEST_USERNAME, TEST_PASSWORD);
        Assert.assertNotNull("Failed to get prepaid accounts", response);
        Assert.assertEquals("Failed to get prepaid accounts", 1, response.getReturnCode());
    }

    @Test
    public void updateUser() {
        PSResponse response = getService().updateUser(TEST_USERNAME, TEST_USERNAME,
                TEST_PASSWORD, FIRSTNAME, LASTNAME, ADDRESS_1, CITY, STATE, POSTAL, COUNTRY,
                CONSUMER_SETTINGS);
        Assert.assertNotNull("Failed to update user", response);
        Assert.assertEquals("Failed to update user", 1, response.getReturnCode());
    }

    @Test
    public void updateUserWithMobile() {
        PSResponse response =
                getService().updateUserWithMobile(MOBILE, PREFERRED_COMM_TYPE, TEST_USERNAME, MOBILE, CARRIER_ID,
                        TEST_PASSWORD, FIRSTNAME, LASTNAME, ADDRESS_1, CITY, STATE, POSTAL, COUNTRY, CONSUMER_SETTINGS);
        Assert.assertNotNull("Failed to update user with mobile", response);
        Assert.assertEquals("Failed to update user with mobile", 1, response.getReturnCode());
    }

    @Test
    public void createUser() {
        PSResponse response =
                getService().createUserWithMobile(PREPAID_CARD_NUM_1, PREPAID_SECURITY_CODE, PREFERRED_COMM_TYPE,
                        TEST_USERNAME, MOBILE,
                        CARRIER_ID, FIRSTNAME, LASTNAME, ADDRESS_1, CITY, STATE, POSTAL, COUNTRY, TEST_PASSWORD,
                        TEST_PASSWORD, null);
        Assert.assertNotNull("Failed to create user with mobile", response);
        Assert.assertEquals("Failed to create user with mobile", 1, response.getReturnCode());
    }

    @Test
    public void createUserWithMobile() {
        PSResponse response =
                getService().createUserWithMobile(PREPAID_CARD_NUM_1, PREPAID_SECURITY_CODE, PREFERRED_COMM_TYPE,
                        "1_" + TEST_USERNAME, MOBILE,
                        CARRIER_ID, FIRSTNAME, LASTNAME, ADDRESS_1, CITY, STATE, POSTAL, COUNTRY, TEST_PASSWORD,
                        TEST_PASSWORD, null);
        Assert.assertNotNull("Can't create user with mobile", response);
        Assert.assertEquals("Can't create user with mobile", 1, response.getReturnCode());
    }

    @Test
    public void registerAnotherCard() {
        String prepaidCardNum = "6396212009858401517";
        String prepaidSecurityCode = "9701";
        PSResponse response =
                getService().registerAnotherCard(TEST_USERNAME, TEST_PASSWORD, prepaidCardNum, prepaidSecurityCode);
        Assert.assertNotNull("Can't create card", response);
        Assert.assertEquals("Can't create card", 1, response.getReturnCode());
    }

    @Test
    public void changePassword() {
        String username = TEST_USERNAME;
        String password = "Password2";
        String newPassword = "Password1";
        PSResponse response = getService().changePassword(username, password, newPassword, newPassword);
        Assert.assertNotNull("Can't change password", response);
        Assert.assertEquals("Can't change password", 1, response.getReturnCode());
        response = getService().changePassword(username, newPassword, password, password);
        Assert.assertNotNull("Can't change password", response);
        Assert.assertEquals("Can't change password", 1, response.getReturnCode());
    }

    @Test
    public void getPrepaidActivities() throws ConvertException {
        Date startTime = ConvertUtils.convertRequired(Date.class, "02/05/2015");
        Date endTime = ConvertUtils.convertRequired(Date.class, "05/07/2015");
        PSPrepaidActivities response =
                getService().getPrepaidActivities(TEST_USERNAME, TEST_PASSWORD, null, startTime, endTime, null);
        Assert.assertNotNull("Can't get prepaid activities", response);
        Assert.assertEquals("Can't get prepaid activities", 1, response.getReturnCode());
    }

    @Test
    public void setupReplenish() {
        String username = "yhe66@usatech.com";
        String password = "Password1";
        String replenishCardNum = "5454545454545454";
        String replenishSecurityCode = "3339";
        Long cardId = 140401L;
        int replenishExpMonth = 2;
        int replenishExpYear = 2299;
        String address1 = "100 Deerfield Lane";
        String city = "Malvern";
        String country = "US";
        String postal = "19355";
        String state = "PA";
        BigDecimal amount = new BigDecimal(20);
        int replenishType = 0;// auto=1, manual=2
        BigDecimal threshhold = new BigDecimal(20);
        PSResponse response = getService()
                .setupReplenish(username, password, cardId, replenishCardNum, replenishExpMonth, replenishExpYear,
                        replenishSecurityCode, address1, city, state, postal, country, amount, replenishType,
                        threshhold);
        Assert.assertNotNull("Can't get replenish", response);
        Assert.assertEquals("Can't get replenish", 1, response.getReturnCode());
    }

    @Test
    public void getReplenishInfo() {
        Long cardId = 136186L;
        Long replenishId = 18L;
        PSResponse response = getService().getReplenishInfo(TEST_USERNAME, TEST_PASSWORD, cardId, replenishId);
        Assert.assertNotNull("Can't get replenish info", response);
        Assert.assertEquals("Can't get replenish info", 1, response.getReturnCode());
    }

    @Test
    public void updateReplenish() {
        Long cardId = 136186L;
        Long replenishId = 21L;
        BigDecimal amount = new BigDecimal(20);
        int replenishType = 1;
        BigDecimal threshhold = new BigDecimal(20);
        int replenCount = 10;
        PSResponse response =
                getService().updateReplenish(TEST_USERNAME, TEST_PASSWORD, cardId, replenishId, amount, replenishType,
                        threshhold, replenCount);
        Assert.assertNotNull("Can't update replenish", response);
        Assert.assertEquals("Can't update replenish", 1, response.getReturnCode());
    }

    @Test
    public void requestReplenish() {
        Long cardId = 136186L;
        Long replenishId = 21L;
        BigDecimal amount = new BigDecimal(20);
        PSResponse response = getService().requestReplenish(TEST_USERNAME, TEST_PASSWORD, cardId, replenishId, amount);
        Assert.assertNotNull("Can't request replenish", response);
        Assert.assertEquals("Can't request replenish", 1, response.getReturnCode());
    }

    @Test
    public void getPromoCampaigns() {
        PSResponse response = getService().getPromoCampaigns(TEST_USERNAME, TEST_PASSWORD);
        Assert.assertNotNull("Can't get promo campaigns", response);
        Assert.assertEquals("Can't get promo campaigns", 1, response.getReturnCode());
    }

    @Test
    public void getLocations() {
        BigDecimal longitude = new BigDecimal("-75.53221");
        BigDecimal latitude = new BigDecimal("40.042957");
        PSResponse response = getService().getLocations(TEST_USERNAME, TEST_PASSWORD, longitude, latitude);
        Assert.assertNotNull("Can't get locations", response);
        Assert.assertEquals("Can't get locations", 1, response.getReturnCode());
    }

    @Test
    public void resetPassword() {
        PSResponse response = getService().resetPassword(TEST_USERNAME, TEST_PASSWORD);
        Assert.assertNotNull("Can't reset password", response);
        Assert.assertEquals("Can't reset password", 1, response.getReturnCode());
    }

    @Test
    public void getConsumerSettings() {
        PSResponse response = getService().getConsumerSettings(TEST_USERNAME, TEST_PASSWORD);
        Assert.assertNotNull("Can't get consumer settings", response);
        Assert.assertEquals("Can't get consumer settings", 1, response.getReturnCode());
    }

    @Test
    public void updateConsumerSettings() {
        PSConsumerSetting[] consumerSettings = new PSConsumerSetting[1];
        consumerSettings[0] = new PSConsumerSetting("1", "Y");
        PSResponse response = getService().updateConsumerSettings(TEST_USERNAME, TEST_PASSWORD, consumerSettings);
        Assert.assertNotNull("Can't update consumer settings", response);
        Assert.assertEquals("Can't update consumer settings", 1, response.getReturnCode());
    }

    @Test
    public void getPostalInfo() {
        PSResponse response = getService().getPostalInfo("19087", "US");
        Assert.assertNotNull("Can't get postal info", response);
        Assert.assertEquals("Can't get postal info", 1, response.getReturnCode());
    }

    @Test
    public void getLocationsByUsage() {
        PSResponse response = getService().getLocationsByUsage(TEST_USERNAME, TEST_PASSWORD);
        Assert.assertNotNull("Can't get locations by usage", response);
        Assert.assertEquals("Can't get locations by usage", 1, response.getReturnCode());
    }

    @Test
    public void getReplenishments() throws ConvertException {
        Date startTime = ConvertUtils.convertRequired(Date.class, "02/01/2014");
        Date endTime = ConvertUtils.convertRequired(Date.class, "03/03/2014");
        PSReplenishmentArray response =
                getService().getReplenishments(TEST_USERNAME, TEST_PASSWORD, null, startTime, endTime, null);
        Assert.assertNotNull("Can't get replenishments", response);
        Assert.assertEquals("Can't get replenishments", 1, response.getReturnCode());
    }

    @Test
    public void getChargedCards() {
        PSResponse response = getService().getChargedCards(TEST_USERNAME, TEST_PASSWORD);
        Assert.assertNotNull("Can't get charged cards", response);
        Assert.assertEquals("Can't get charged cards", 1, response.getReturnCode());
    }

    @Test
    public void getAuthHolds() {
        PSResponse response = getService().getAuthHolds(TEST_USERNAME, TEST_PASSWORD, null);
        Assert.assertNotNull("Can't get AuthHolds", response);
        Assert.assertEquals("Can't get AuthHolds", 1, response.getReturnCode());
    }

    @Test
    public void getLocationsByLocation() {
        PSResponse response =
                getService().getLocationsByLocation(TEST_USERNAME, TEST_PASSWORD, "19355", "US", 1f, false);
        Assert.assertNotNull("Can't get locations by location", response);
        Assert.assertEquals("Can't get locations by location", 1, response.getReturnCode());
    }

    @Test
    public void retokenize() {
        long consumerAcctId = 1403631;
        String securityCode = "6924";
        PSResponse response = getService().retokenize(TEST_USERNAME, TEST_PASSWORD, consumerAcctId, securityCode, null);
        Assert.assertNotNull("Can't retokenize", response);
        Assert.assertEquals("Can't retokenize", 1, response.getReturnCode());
    }

    @Test
    public void createUserEncrypted() {
        String username = "test_json_encrypted@usatech.com";
        String password = "Password1";
        String address1 = "100 Deerfield Lane";
        String city = "Malvern";
        String country = "US";
        String firstname = "Ying";
        String lastname = "He";
        String postal = "19355";
        String state = "PA";
        String promoCode = "nTtcTA";
        int carrierId = 4;
        int cardReaderType = 1;
        String cardDataPlain = ";4788250000028291=10121015432112345601?";
        String encryptedCardDataHex =
                "AC08923C2EB0B7385DAA56EE23B4DEC88616D8FFF62542298F5ADAD31EBC4B9BF44F0D5865DFBC47";
        String ksnHex = "62994996340010200003";
        String billingCountry = "US";
        int preferredCommType = 1;
        String entryType = "S";
        PSResponse response =
                getService()
                        .createUserEncrypted(entryType, cardReaderType, cardDataPlain.length(), encryptedCardDataHex,
                                ksnHex,
                                billingCountry,
                                address1, postal, preferredCommType, username, null, carrierId, firstname, lastname,
                                address1,
                                city, state, postal, country, password, password, null, promoCode);
        Assert.assertNotNull("Can't create user encrypted", response);
        Assert.assertEquals("Can't create user encrypted", 1, response.getReturnCode());
    }

    @Test
    public void registerAnotherCardEncrypted() {
        int cardReaderType = 1;
        String cardDataPlain = ";371449635398431=10121015432112345678?";
        String encryptedCardDataHex =
                "1153448955A65615CA60A6CAA3CEAC844A75BB1A25F95159386AEAC8F2ED9154CEF236F4CAD8F663";
        String ksnHex = "62994996340010200003";
        String billingCountry = "CA";
        String billingAddress = "100 Deerfield Lane";
        String billingPostalCode = "19355";
        String entryType = "S";
        PSResponse response = getService()
                .registerAnotherCardEncrypted(TEST_USERNAME, TEST_PASSWORD, entryType, cardReaderType,
                        cardDataPlain.length(), encryptedCardDataHex, ksnHex, billingCountry, billingAddress,
                        billingPostalCode);
        Assert.assertNotNull("Can't register another card encrypted", response);
        Assert.assertEquals("Can't register another card encrypted", 1, response.getReturnCode());
    }

    @Test
    public void createUserKeyed() {
        String username = "yhe888888@usatech.com";
        String password = "Password1";
        String cardNum = "4055011111111129";
        String securityCode = "123";
        String address1 = "100 Deerfield Lane";
        String city = "Malvern";
        String country = "US";
        String firstname = "Ying";
        String lastname = "He";
        String postal = "19355";
        String state = "PA";
        String promoCode = "nTtcTA";
        int carrierId = 4;
        PSResponse response = getService()
                .createUserKeyed(cardNum, securityCode, 2, 2018, "US", address1, postal, 1, username, "1111111111",
                        carrierId, firstname, lastname, address1, city, state, postal, country, password, password,
                        null, promoCode);
        Assert.assertNotNull("Can't create user keyed", response);
        Assert.assertEquals("Can't create user keyed", 1, response.getReturnCode());
    }

    @Test
    public void registerAnotherCardKeyed() {
        String cardNum = "4055011111111129";
        String securityCode = "123";
        String billingCountry = "US";
        String billingAddress = "100 Deerfield Lane";
        String billingPostalCode = "19355";
        PSResponse response = getService()
                .registerAnotherCardKeyed(TEST_USERNAME, TEST_PASSWORD, cardNum, securityCode, 2, 2018, billingCountry,
                        billingAddress, billingPostalCode);
        Assert.assertNotNull("Can't register another card keyed", response);
        Assert.assertEquals("Can't register another card keyed", 1, response.getReturnCode());
    }

    @Test
    public void addPromoCode() {
        String promoCode = "nTtCTA";
        PSResponse response = getService().addPromoCode(TEST_USERNAME, TEST_PASSWORD, promoCode);
        Assert.assertNotNull("Can't add promo code", response);
        Assert.assertEquals("Can't add promo code", 1, response.getReturnCode());
    }

    @Test
    public void registerAnotherCreditCardEncrypted() {
        int cardReaderType = 1;
        String cardDataPlain = ";371449635398431=10121015432112345678?";
        String encryptedCardDataHex =
                "1153448955A65615CA60A6CAA3CEAC844A75BB1A25F95159386AEAC8F2ED9154CEF236F4CAD8F663";
        String ksnHex = "62994996340010200003";
        String billingCountry = "CA";
        String billingAddress = "100 Deerfield Lane";
        String billingPostalCode = "19355";
        String entryType = "S";
        PSResponse response =
                getService().registerAnotherCardEncrypted(TEST_USERNAME, TEST_PASSWORD, entryType, cardReaderType,
                        cardDataPlain.length(), encryptedCardDataHex, ksnHex, billingCountry, billingAddress,
                        billingPostalCode);
        Assert.assertNotNull("Can't register another credit card encrypted", response);
        Assert.assertEquals("Can't register another credit card encrypted", 1, response.getReturnCode());
    }

    @Test
    public void registerAnotherCreditCardKeyed() {
        String cardNum = "4055011111111129";
        String securityCode = "123";
        String billingCountry = "US";
        String billingAddress = "100 Deerfield Lane";
        String billingPostalCode = "19355";
        PSResponse response =
                getService().registerAnotherCardKeyed(TEST_USERNAME, TEST_PASSWORD, cardNum, securityCode, 2, 2018,
                        billingCountry, billingAddress, billingPostalCode);
        Assert.assertNotNull("Can't register another credit card keyed", response);
        Assert.assertEquals("Can't register another credit card keyed", 1, response.getReturnCode());
    }

    private PrepaidServiceAPI getService() {
        if (service == null)
            service = new PrepaidJsonClient(PS_JSON_URL);
        return service;
    }

}
