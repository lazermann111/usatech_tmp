package com.usatech.prepaid.test;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.Properties;

import org.junit.Test;

import com.caucho.hessian.client.HessianProxyFactory;
import com.usatech.prepaid.web.PrepaidUtils;
import com.usatech.ps.PSConsumerAccountBase;
import com.usatech.ps.PSConsumerSetting;
import com.usatech.ps.PSPrepaidActivities;
import com.usatech.ps.PSReplenishmentArray;
import com.usatech.ps.PSResponse;
import com.usatech.ps.PrepaidServiceAPI;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.bean.ConvertUtils;
import simple.db.config.ConfigLoader;
import simple.io.ConfigSource;
import simple.text.StringUtils;

public class PrepaidServiceHessianTest {
	public static final String PS_HESSIAN_URL = "http://localhost:8580/hessian/ps";
	//public static final String PS_HESSIAN_URL = "https://getmore-dev.usatech.com/hessian/ps";
	//public static final String PS_HESSIAN_URL = "https://getmore-int.usatech.com/hessian/ps";
	//public static final String PS_HESSIAN_URL = "https://getmore-ecc.usatech.com/hessian/ps";
	
	protected static HessianProxyFactory hessianProxyFactory = null;	
	protected static PrepaidServiceAPI ps = null;
	
	static {
		System.setProperty("app.servicename", "PrepaidServiceHessianTest");
		//Uncomment trustStore properties below for Dev that uses a certificate signed by USAT internal Certification Authority
		/*System.setProperty("javax.net.ssl.trustStore", "../LayersCommon/conf/net/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");*/
		System.setProperty("log4j.configuration", "log4j-dev.properties");
	}
	
	protected void init() throws MalformedURLException {
		if (hessianProxyFactory == null)
			hessianProxyFactory = new HessianProxyFactory();
		if (ps == null)
			ps = (PrepaidServiceAPI) hessianProxyFactory.create(PrepaidServiceAPI.class, PS_HESSIAN_URL);
	}
	
	protected void parseResponse(PSResponse response) throws Exception {
		if (response == null)
			throw new Exception("Received null response");
		else {
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == PrepaidServiceSOAPTest.RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}

	
	@Test
	public void testGetUser() throws Exception {
		init();
		PSResponse response = ps.getUser(PrepaidServiceSOAPTest.TESTUSERNAME, PrepaidServiceSOAPTest.TESTPASSWORD);
		parseResponse(response);
	}
	
	@Test
	public void testVerifyUser() throws Exception {
		init();
		//PSResponse response = ps.verifyUser(PrepaidServiceSOAPTest.USERNAME, PrepaidServiceSOAPTest.PASSWORD, "");
		PSResponse response = ps.verifyUser("1111111111", "Password1", "ZwUHqseaDB");
		parseResponse(response);
	}
	
	
//	@Test
//	public void testGetUserWithConsumerId() throws Exception {
//		init();
//		PSResponse response = ps.getUser(String.valueOf(PrepaidServiceSOAPTest.CONSUMER_ID), PrepaidServiceSOAPTest.PASSWORD);
//		parseResponse(response);
//	}

	@Test
	public void testGetPrepaidAccounts() throws Exception {
		init();
		PSResponse response = ps.getPrepaidAccounts(PrepaidServiceSOAPTest.TESTUSERNAME, PrepaidServiceSOAPTest.TESTPASSWORD);
//		PSResponse response = ps.getPrepaidAccounts("jscarpaci@usatech.com", "Foobar21");
		parseResponse(response);
	}

//	@Test
//	public void testGetPrepaidAccountsWithConsumerId() throws Exception {
//		init();
//		PSResponse response = ps.getPrepaidAccounts(String.valueOf(PrepaidServiceSOAPTest.CONSUMER_ID), PrepaidServiceSOAPTest.PASSWORD);
//		parseResponse(response);
//	}
	
	@Test
	public void testUpdateUser() throws Exception {
		init();
		String address1="100 Deerfield Lane";
		String city="Malvern";
		String country="US";
		String firstname="PrepaidTest";
		String lastname="Test3";
		String postal="19355";
		String state="PA";
		PSConsumerSetting[] consumerSettings=new PSConsumerSetting[1];
		consumerSettings[0]=new PSConsumerSetting("1","N");
		PSResponse response = ps.updateUser(PrepaidServiceSOAPTest.USERNAME, PrepaidServiceSOAPTest.USERNAME, PrepaidServiceSOAPTest.PASSWORD, firstname, lastname, address1, city, state, postal, country,consumerSettings);
		parseResponse(response);
	}
	
	@Test
	public void testCreateUser() throws Exception {
		init();
		String username = "jscarpaci.usa17@gmail.com";
		String password="Password1";
		String prepaidCardNum = "6396212013515961186";
		String prepaidSecurityCode = "7460";
		String address1="100 Deerfield Lane";
		String city="Malvern";
		String country="US";
		String firstname="John";
		String lastname="ScarpaciJunit";
		String postal="19355";
		String state="PA";
		PSResponse response = ps.createUser(prepaidCardNum, prepaidSecurityCode, username, firstname, lastname, address1, city, state, postal, country, password, password,null);
		parseResponse(response);
	}
	
	@Test
	public void testUpdateUserWithMobile() throws Exception {
		init();
		String address1="100 Deerfield Lane";
		String city="Malvern";
		String country="US";
		String firstname="PrepaidTest";
		String lastname="Test";
		String postal="19355";
		String state="PA";
		PSConsumerSetting[] consumerSettings=new PSConsumerSetting[1];
		consumerSettings[0]=new PSConsumerSetting("1","N");
		int preferredCommType=2;
		String mobile="1111111111";
		int carrierId=4;
		String email="yhe11@usatech.com";
		PSResponse response = ps.updateUserWithMobile(mobile, preferredCommType, email, mobile, carrierId, PrepaidServiceSOAPTest.PASSWORD, firstname, lastname, address1, city, state, postal, country,consumerSettings);
		parseResponse(response);
	}
	
	@Test
	public void testGetDeactivationYYMM() throws Exception {
		init();
		Properties props = MainWithConfig.loadPropertiesWithConfig("prepaid.properties", PrepaidServiceHessianTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:prepaid-data-layer.xml"));
		String prepaidCardNum="6396212009816772702";
		PSConsumerAccountBase consumerAcctBase1 = PrepaidUtils.consumerAccountBaseForCardNum(prepaidCardNum);
		System.out.println(consumerAcctBase1.getDeactivationYYMM());
	}
	
	@Test
	public void testCreateUserWithMobile() throws Exception {
		init();
		String email = "yhe66@usatech.com";
		String password="Password1";
		//String prepaidCardNum = "6396212009816772702";
		//String prepaidSecurityCode = "9385";
		//String prepaidCardNum = "6396212001129218036";
		//String prepaidSecurityCode = "3339";
		String prepaidCardNum = "6396212009701971401";
		String prepaidSecurityCode = "9901";
		String address1="100 Deerfield Lane";
		String city="Malvern";
		String country="US";
		String firstname="Ying";
		String lastname="He";
		String postal="19355";
		String state="PA";
		int preferredCommType=1;
		String mobile="1111111110";
		int carrierId=4;
		PSResponse response = ps.createUserWithMobile(prepaidCardNum, prepaidSecurityCode, preferredCommType,email, mobile,carrierId, firstname, lastname, address1, city, state, postal, country, password, password,null);
		parseResponse(response);
	}
	
	@Test
	public void testRetokenize() throws Exception {
		init();
		//String username="yhe66@usatech.com";
		//String password="Password1";
		//long consumerAcctId=140401;
		//String securityCode= "9901";
		String username="yhe@usatech.com";
		String password="Password1";
		long consumerAcctId=1403631;
		String securityCode= "6924";
		PSResponse response = ps.retokenize(username, password, consumerAcctId,securityCode, null);
		parseResponse(response);
	}
	@Test
	public void testRetokenizeOne() throws Exception {
		init();
		String username = "bkrug_one@usatech.com";
		String password = "Gobblygook1";
		String securityCd = null; // "111";
		String postalCd = "19355";
		String expDate = "2008";
		long consumerAcctId = 140420;
		PSResponse response = ps.retokenize(username, password, consumerAcctId, securityCd, null);
		parseResponse(response);
	}

	@Test
	public void testRegisterAnotherCard() throws Exception {
		init();
		String username=PrepaidServiceSOAPTest.USERNAME;
		String password=PrepaidServiceSOAPTest.PASSWORD;
		String prepaidCardNum="6396212009858401517";
		String prepaidSecurityCode="9701";
		PSResponse response = ps.registerAnotherCard(username, password, prepaidCardNum, prepaidSecurityCode);
		parseResponse(response);
	}
	
	@Test
	public void testChangePassword() throws Exception {		
		init();
		String username=PrepaidServiceSOAPTest.TESTUSERNAME;
		String password="Password2";
		String newPassword="Password1";
		String newPasswordConfirm="Password1";
		PSResponse response = ps.changePassword(username, password, newPassword, newPasswordConfirm);
		parseResponse(response);
	}
	
	@Test
	public void testGetPrepaidActivities() throws Exception {		
		init();
		String username=PrepaidServiceSOAPTest.TESTUSERNAME;
		String password=PrepaidServiceSOAPTest.TESTPASSWORD;
		Date startTime=ConvertUtils.convertRequired(Date.class, "02/05/2015");
		Date endTime=ConvertUtils.convertRequired(Date.class, "05/07/2015");
		PSPrepaidActivities response = ps.getPrepaidActivities(username, password, null, startTime, endTime, null);
		parseResponse(response);
	}
	
	@Test
	public void testSetupReplenish() throws Exception {		
		init();
		//String username=PrepaidServiceSOAPTest.USERNAME;
		//String password=PrepaidServiceSOAPTest.PASSWORD;
		String username="yhe66@usatech.com";
		String password="Password1";
		String replenishCardNum="5454545454545454";
		String replenishSecurityCode="3339";
		Long cardId=140401L;
		int replenishExpMonth=2;
		int replenishExpYear=2299;
		String address1="100 Deerfield Lane";
		String city="Malvern";
		String country="US";
		String postal="19355";
		String state="PA";
		BigDecimal amount=new BigDecimal(20);
		int replenishType=0;// auto=1, manual=2
		BigDecimal threshhold=new BigDecimal(20);
		PSResponse response = ps.setupReplenish(username, password, cardId, replenishCardNum, replenishExpMonth, replenishExpYear, replenishSecurityCode, address1, city, state, postal, country, amount, replenishType, threshhold);
		parseResponse(response);
	}
	
	@Test
	public void testGetReplenishInfo() throws Exception {	
		init();
		String username=PrepaidServiceSOAPTest.USERNAME;
		String password=PrepaidServiceSOAPTest.PASSWORD;
		Long cardId=136186L;
		Long replenishId=18L;
		PSResponse response = ps.getReplenishInfo(username, password, cardId, replenishId);
		parseResponse(response);
	}
	
	@Test
	public void testUpdateReplenish() throws Exception {
		init();
		String username=PrepaidServiceSOAPTest.USERNAME;
		String password=PrepaidServiceSOAPTest.PASSWORD;
		Long cardId=136186L;
		Long replenishId=21L;
		BigDecimal amount=new BigDecimal(20);
		int replenishType=1;
		BigDecimal threshhold=new BigDecimal(20);
		int replenCount = 10;
		PSResponse response = ps.updateReplenish(username, password, cardId, replenishId, amount, replenishType, threshhold, replenCount);
		parseResponse(response);
	}
	
	@Test
	public void testRequestReplenish() throws Exception {
		init();
		String username=PrepaidServiceSOAPTest.USERNAME;
		String password=PrepaidServiceSOAPTest.PASSWORD;
		Long cardId=136186L;
		Long replenishId=21L;
		BigDecimal amount=new BigDecimal(20);
		PSResponse response = ps.requestReplenish(username, password, cardId, replenishId, amount);
		parseResponse(response);
//		String username="jscarpaci@usatech.com";
//		String password="Foobar21";
//		Long cardId=140131L;
//		Long replenishId=21L;
//		BigDecimal amount=new BigDecimal(20);
//		PSResponse response = ps.requestReplenish(username, password, cardId, replenishId, amount);
//		parseResponse(response);
	}
	
	@Test
	public void testGetPromoCampaigns() throws Exception {
		init();
		PSResponse response = ps.getPromoCampaigns(PrepaidServiceSOAPTest.TESTUSERNAME, PrepaidServiceSOAPTest.TESTPASSWORD);
		parseResponse(response);
	}
	
	@Test
	public void testGetLocations() throws Exception {
		init();
		BigDecimal longitude=new BigDecimal("-75.53221");
		BigDecimal latitude=new BigDecimal("40.042957");
		PSResponse response = ps.getLocations(PrepaidServiceSOAPTest.TESTUSERNAME, PrepaidServiceSOAPTest.TESTPASSWORD,longitude,latitude);
		parseResponse(response);
	}
	
	@Test
	public void testGetLocationsNoResults() throws Exception {
		init();
		BigDecimal longitude = new BigDecimal("75.53221");
		BigDecimal latitude = new BigDecimal("40.042957");
		PSResponse response = ps.getLocations(PrepaidServiceSOAPTest.TESTUSERNAME, PrepaidServiceSOAPTest.TESTPASSWORD, longitude, latitude);
		parseResponse(response);
	}

	@Test
	public void testResetPassword() throws Exception {		
		init();
		String username = PrepaidServiceSOAPTest.TESTUSERNAME;
		String password=PrepaidServiceSOAPTest.TESTPASSWORD;
		PSResponse response = ps.resetPassword(username, password);
		parseResponse(response);
	}
	
	@Test
	public void testGetConsumerSettings() throws Exception {		
		init();
		String username=PrepaidServiceSOAPTest.TESTUSERNAME;
		String password=PrepaidServiceSOAPTest.TESTPASSWORD;
		PSResponse response = ps.getConsumerSettings(username, password);
		parseResponse(response);
	}
	
	@Test
	public void testUpdateConsumerSettings() throws Exception {		
		init();
		String username=PrepaidServiceSOAPTest.TESTUSERNAME;
		String password=PrepaidServiceSOAPTest.TESTPASSWORD;
		PSConsumerSetting[] consumerSettings=new PSConsumerSetting[1];
		consumerSettings[0]=new PSConsumerSetting("1","Y");
		PSResponse response = ps.updateConsumerSettings(username, password, consumerSettings);
		parseResponse(response);
	}
	
	@Test
	public void testGetPostalInfo() throws Exception {
		init();
		PSResponse response = ps.getPostalInfo("19087", "US");
		parseResponse(response);
	}
	
	@Test
	public void testGetLocationsByUsage() throws Exception {
		init();
		PSResponse response = ps.getLocationsByUsage(PrepaidServiceSOAPTest.TESTUSERNAME, PrepaidServiceSOAPTest.TESTPASSWORD);
		parseResponse(response);
	}
	
	@Test
	public void testGetReplenishments() throws Exception {		
		init();
		String username=PrepaidServiceSOAPTest.USERNAME;
		String password=PrepaidServiceSOAPTest.PASSWORD;
		Date startTime=ConvertUtils.convertRequired(Date.class, "02/01/2014");
		Date endTime=ConvertUtils.convertRequired(Date.class, "03/03/2014");
		PSReplenishmentArray response = ps.getReplenishments(username, password, null, startTime, endTime, null);
		parseResponse(response);
	}
	
	@Test
	public void testGetChargedCards() throws Exception {
		init();
		PSResponse response = ps.getChargedCards(PrepaidServiceSOAPTest.USERNAME, PrepaidServiceSOAPTest.PASSWORD);
		parseResponse(response);
	}
	
	@Test
	public void testAuthHolds() throws Exception {
		init();
		PSResponse response = ps.getAuthHolds(PrepaidServiceSOAPTest.TESTUSERNAME, PrepaidServiceSOAPTest.TESTPASSWORD, null);
		parseResponse(response);
	}
	
	@Test
	public void testGetLocationsByLocation() throws Exception {
		init();
		PSResponse response = ps.getLocationsByLocation(PrepaidServiceSOAPTest.TESTUSERNAME, PrepaidServiceSOAPTest.TESTPASSWORD, "19355", "US", 1f, false);
		parseResponse(response);
	}
	
	@Test
	public void testGetRegions() throws Exception {
		init();
		//PSResponse response = ps.getRegions("1111111111", "Password1");
		PSResponse response = ps.getRegions(PrepaidServiceSOAPTest.TESTUSERNAME, PrepaidServiceSOAPTest.TESTPASSWORD);
		parseResponse(response);
	}
	
	@Test
	public void testUpdateRegion() throws Exception {		
		init();
		String username = "1111111111";
		String password="Password1";
		PSResponse response = ps.updateRegion(username, password,296);
		parseResponse(response);
	}
	
	@Test
	public void testAddPromoCode() throws Exception {		
		init();
		//String username =PrepaidServiceSOAPTest.TESTUSERNAME;
		//String password=PrepaidServiceSOAPTest.TESTPASSWORD;
		String username =PrepaidServiceSOAPTest.USERNAME;
		String password=PrepaidServiceSOAPTest.PASSWORD;
		String promoCode="nTtcTA";
		PSResponse response = ps.addPromoCode(username, password,promoCode);
		parseResponse(response);
	}
	
	@Test
	public void testRegisterAnotherCardKeyed() throws Exception {
		init();
		String username=PrepaidServiceSOAPTest.TESTUSERNAME;
		String password=PrepaidServiceSOAPTest.TESTPASSWORD;
		String cardNum="4055011111111129";
		String securityCode="123";
	    String billingCountry="US";
		String billingAddress="100 Deerfield Lane";
		String billingPostalCode="19355";
		PSResponse response = ps.registerAnotherCardKeyed(username, password, cardNum, securityCode, 2,2018, billingCountry, billingAddress, billingPostalCode);
		parseResponse(response);
	}
	
	@Test
	public void testCreateUserKeyed() throws Exception {
		init();
		String username = "yhe888888@usatech.com";
		String password="Password1";
		String cardNum = "4055011111111129";
		String securityCode = "123";
		String address1="100 Deerfield Lane";
		String city="Malvern";
		String country="US";
		String firstname="Ying";
		String lastname="He";
		String postal="19355";
		String state="PA";
		String promoCode="nTtcTA";
		int carrierId=4;
		PSResponse response = ps.createUserKeyed(cardNum, securityCode, 2,2018, "US", address1, postal, 1, username, "1111111111", carrierId,firstname, lastname, address1, city, state, postal, country, password, password,null,promoCode);
		parseResponse(response);
	}
	
	@Test
	public void testCreateUserEncrypted() throws Exception {
		init();
		String username = "test_hessian_encrypted@usatech.com";
		String password="Password1";
		String address1="100 Deerfield Lane";
		String city="Malvern";
		String country="US";
		String firstname="Ying";
		String lastname="He";
		String postal="19355";
		String state="PA";
		String promoCode="nTtcTA";
		int carrierId=4;
		int cardReaderType=1;
		String cardDataPlain=";4788250000028291=10121015432112345601?";
		String encryptedCardDataHex= "AC08923C2EB0B7385DAA56EE23B4DEC88616D8FFF62542298F5ADAD31EBC4B9BF44F0D5865DFBC47";
		String ksnHex="62994996340010200003";
		String billingCountry="US";
		String billingAddress=address1;
		String billingPostalCode=postal;
		int preferredCommType=1;
		String entryType="S";
		PSResponse response = ps.createUserEncrypted(entryType, cardReaderType, cardDataPlain.length(), encryptedCardDataHex, ksnHex, billingCountry, billingAddress, billingPostalCode, preferredCommType, username, null, carrierId, firstname, lastname, address1, city, state, postal, country, password, password, null, promoCode);
		parseResponse(response);
	}
	
	@Test
	public void testCreateUserEncryptedPrepaid() throws Exception {
		init();
		String username = "test_hessian_encrypted_prepaid@usatech.com";
		String password="Password1";
		String address1="100 Deerfield Lane";
		String city="Malvern";
		String country="US";
		String firstname="Test";
		String lastname="User";
		String postal="19355";
		String state="PA";
		int carrierId=4;
		int cardReaderType=1;
		String cardDataPlain=";6396000003000008972=9912000000000?";
		String encryptedCardDataHex= "32DA7673E1528F948B0374A3A9F3D98E86811D7B6F7112644827A725A96F3ADCE6DBF9F7E472DE1E";
		String ksnHex="9011040B005165000080";
		String billingCountry="US";
		String billingAddress=address1;
		String billingPostalCode=postal;
		int preferredCommType=1;
		String entryType="S";
		PSResponse response = ps.createUserEncrypted(entryType, cardReaderType, cardDataPlain.length(), encryptedCardDataHex, ksnHex, billingCountry, billingAddress, billingPostalCode, preferredCommType, username, null, carrierId, firstname, lastname, address1, city, state, postal, country, password, password, null, "");
		parseResponse(response);
	}	
	
	@Test
	public void testRegisterAnotherCardEncrypted() throws Exception {
		init();
		String username=PrepaidServiceSOAPTest.TESTUSERNAME;
		String password=PrepaidServiceSOAPTest.TESTPASSWORD;
		int cardReaderType=1;
		String cardDataPlain=";371449635398431=10121015432112345678?";
		String encryptedCardDataHex= "1153448955A65615CA60A6CAA3CEAC844A75BB1A25F95159386AEAC8F2ED9154CEF236F4CAD8F663";
		String ksnHex="62994996340010200003";
	    String billingCountry="CA";
		String billingAddress="100 Deerfield Lane";
		String billingPostalCode="19355";
		String entryType="S";
		PSResponse response = ps.registerAnotherCardEncrypted(username, password, entryType, cardReaderType, cardDataPlain.length(), encryptedCardDataHex, ksnHex, billingCountry, billingAddress, billingPostalCode);
		parseResponse(response);
	}
}
