package com.usatech.prepaid.test;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.junit.Test;

import com.usatech.ps.PsStub;

import simple.bean.ConvertUtils;
import simple.text.StringUtils;

public class PrepaidServiceSOAPTest {
	//public static final String PS_SOAP_URL = "http://localhost:8580/soap/ps";
	//public static final String PS_SOAP_URL = "https://getmore-dev.usatech.com/soap/ps";
	//public static final String PS_SOAP_URL = "https://getmore-int.usatech.com/soap/ps";
	public static final String PS_SOAP_URL = "https://getmore-ecc.usatech.com/soap/ps";
	//public static final String USERNAME = "SelfCareApp@usatech.com";
	//public static final String PASSWORD = "VZSelfCare123";
	//public static final String USERNAME = "PrepaidTest@usatech.com";
	//public static final String PASSWORD = "PSTest123";
    public static final String USERNAME = "yhe@usatech.com";
    
    public static final String DEV_USERNAME="1111111111";
	//public static final String PASSWORD = "Password2";
    public static final String PASSWORD = "Password1";
	public static final String CARD_NUMBER = "4055011111111111";
	public static final String CURRENCY_CODE = "USD";
	public static final long TRANSACTION_ID = 1000010059585L;
	
	//public static final String TESTUSERNAME="bkrug@usatech.com";
	//public static final String TESTPASSWORD="Gobblygook1";
	public static final String TESTUSERNAME = "yhe888@usatech.com";
	public static final String TESTPASSWORD = "Password1";
	
	public static final int RES_FAILED = 0; //Operation failed
	public static final int RES_OK = 1; //Operation was successful   
	
	protected static PsStub ps = null;
	
	static {
		System.setProperty("app.servicename", "PrepaidServiceSOAPTest");
		//Uncomment trustStore properties below for Dev that uses a certificate signed by USAT internal Certification Authority
		/*System.setProperty("javax.net.ssl.trustStore", "../LayersCommon/conf/net/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");*/
		System.setProperty("log4j.configuration", "log4j-dev.properties");
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");
	}
	
	protected void init() throws AxisFault {
		if (ps == null) {
			HttpConnectionManagerParams connectionManagerParams = new HttpConnectionManagerParams();
			connectionManagerParams.setDefaultMaxConnectionsPerHost(100);
			connectionManagerParams.setMaxTotalConnections(100);
			connectionManagerParams.setConnectionTimeout(3000);
			connectionManagerParams.setSoTimeout(30000);
			connectionManagerParams.setLinger(1000);
			connectionManagerParams.setTcpNoDelay(true);
			connectionManagerParams.setStaleCheckingEnabled(true);
			
			MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
			connectionManager.setParams(connectionManagerParams);
			HttpClient httpClient = new HttpClient(connectionManager);

			ps = new PsStub(PS_SOAP_URL);
			ps._getServiceClient().getOptions().setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Boolean.TRUE);
			ps._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, httpClient);
		}
	}
	
	protected void parseResponse(PsStub.PSResponse response) throws Exception {
		String message = StringUtils.objectToString(response, "Response: ");
		if (response.getReturnCode() == RES_OK)
			System.out.println(message);
		else
			throw new Exception(message);
	}

	@Test
	public void testGetUser() throws Exception {		
		init();
		PsStub.GetUser request = new PsStub.GetUser();
		request.setUsername(DEV_USERNAME);
		request.setPassword(PASSWORD);
		PsStub.GetUserResponse responseMessage;
		try {
			responseMessage = ps.getUser(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSUser response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	@Test
	public void testVerifyUser() throws Exception {		
		init();
		PsStub.VerifyUser request = new PsStub.VerifyUser();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		request.setPasscode("xyz");
		PsStub.VerifyUserResponse responseMessage;
		try {
			responseMessage = ps.verifyUser(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	@Test
	public void testGetPrepaidAccounts() throws Exception {		
		init();
		PsStub.GetPrepaidAccounts request = new PsStub.GetPrepaidAccounts();
		request.setUsername(DEV_USERNAME);
		request.setPassword(PASSWORD);
		PsStub.GetPrepaidAccountsResponse responseMessage;
		try {
			responseMessage = ps.getPrepaidAccounts(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSPrepaidAccounts response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testCreateUser() throws Exception {		
		init();
		PsStub.CreateUser request = new PsStub.CreateUser();
		request.setUsername("testws5@usatech.com");
		request.setPassword(PASSWORD);
		request.setConfirmPassword(PASSWORD);
		request.setAddress1("100 Deerfield Lane");
		request.setCity("Malvern");
		request.setCountry("US");
		request.setFirstname("PrepaidTest");
		request.setLastname("Test");
		request.setPostal("19355");
		request.setState("PA");
		//request.setPrepaidCardNum("6396212008348539217");
		//request.setPrepaidSecurityCode("4758");
		PsStub.PSConsumerSetting[] setting=new PsStub.PSConsumerSetting[1];
		setting[0]=new PsStub.PSConsumerSetting();
		setting[0].setSettingId("1");
		setting[0].setValue("N");
		request.setConsumerSettings(setting);
		request.setPrepaidCardNum("6396212009190994260");
		request.setPrepaidSecurityCode("1435");
		PsStub.CreateUserResponse responseMessage;
		try {
			responseMessage = ps.createUser(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testCreateUserWithMobile() throws Exception {		
		init();
		PsStub.CreateUserWithMobile request = new PsStub.CreateUserWithMobile();
		request.setEmail("testws4@usatech.com");
		request.setPreferredCommType(2);
		request.setMobile("2222222222");
		request.setCarrierId(74);
		request.setPassword(PASSWORD);
		request.setConfirmPassword(PASSWORD);
		request.setAddress1("100 Deerfield Lane");
		request.setCity("Malvern");
		request.setCountry("US");
		request.setFirstname("Ying");
		request.setLastname("He");
		request.setPostal("19355");
		request.setState("PA");
		//request.setPrepaidCardNum("6396212008348539217");
		//request.setPrepaidSecurityCode("4758");
		PsStub.PSConsumerSetting[] setting=new PsStub.PSConsumerSetting[1];
		setting[0]=new PsStub.PSConsumerSetting();
		setting[0].setSettingId("1");
		setting[0].setValue("N");
		request.setConsumerSettings(setting);
		request.setPrepaidCardNum("6396212009381988535");
		request.setPrepaidSecurityCode("2418");
		PsStub.CreateUserWithMobileResponse responseMessage;
		try {
			responseMessage = ps.createUserWithMobile(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testUpdateUserWithMobile() throws Exception {		
		init();
		PsStub.UpdateUserWithMobile request = new PsStub.UpdateUserWithMobile();
		request.setPreferredCommType(1);
		request.setEmail("testws4@usatech.com");
		request.setMobile("2222222222");
		request.setCarrierId(74);
		request.setPassword(PASSWORD);
		request.setAddress1("100 Deerfield Lane");
		request.setCity("Malvern");
		request.setCountry("US");
		request.setFirstname("Ying");
		request.setLastname("He");
		request.setOldUsername("2222222222");
		request.setPostal("19355");
		request.setState("PA");
		PsStub.PSConsumerSetting[] setting=new PsStub.PSConsumerSetting[1];
		setting[0]=new PsStub.PSConsumerSetting();
		setting[0].setSettingId("1");
		setting[0].setValue("Y");
		request.setConsumerSettings(setting);
		PsStub.UpdateUserWithMobileResponse responseMessage;
		try {
			responseMessage = ps.updateUserWithMobile(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testUpdateUser() throws Exception {		
		init();
		PsStub.UpdateUser request = new PsStub.UpdateUser();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		request.setAddress1("100 Deerfield Lane");
		request.setCity("Malvern");
		request.setCountry("US");
		request.setFirstname("PrepaidTest");
		request.setLastname("Test");
		request.setOldUsername(USERNAME);
		request.setPostal("19355");
		request.setState("PA");
		PsStub.PSConsumerSetting[] setting=new PsStub.PSConsumerSetting[1];
		setting[0]=new PsStub.PSConsumerSetting();
		setting[0].setSettingId("1");
		setting[0].setValue("N");
		request.setConsumerSettings(setting);
		PsStub.UpdateUserResponse responseMessage;
		try {
			responseMessage = ps.updateUser(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testRegisterAnotherCard() throws Exception {		
		init();
		PsStub.RegisterAnotherCard request = new PsStub.RegisterAnotherCard();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		//request.setPrepaidCardNum("6396212008850409858");
		//request.setPrepaidSecurityCode("3231");
		request.setPrepaidCardNum("6396212001081040709");
		request.setPrepaidSecurityCode("5660");
		PsStub.RegisterAnotherCardResponse responseMessage;
		try {
			responseMessage = ps.registerAnotherCard(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testChangePassword() throws Exception {		
		init();
		PsStub.ChangePassword request = new PsStub.ChangePassword();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		request.setNewPassword("PSTest1234");
		request.setNewPasswordConfirm("PSTest1234");
		//request.setNewPassword("Password1");
		//request.setNewPasswordConfirm("Password1");
		PsStub.ChangePasswordResponse responseMessage;
		try {
			responseMessage = ps.changePassword(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetPrepaidActivities() throws Exception {		
		init();
		PsStub.GetPrepaidActivities request = new PsStub.GetPrepaidActivities();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		Date startTime=ConvertUtils.convertRequired(Date.class, "02/09/2013");
		Date endTime=ConvertUtils.convertRequired(Date.class, "03/20/2013");
		request.setStartTime(startTime);
		request.setEndTime(endTime);
		PsStub.GetPrepaidActivitiesResponse responseMessage;
		try {
			responseMessage = ps.getPrepaidActivities(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSPrepaidActivities response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testSetupReplenish() throws Exception {
		String testCardNum="5454545454545454";//6011111111111117=decline 5454545454545454=success
		String replenishSecurityCode="123";
		init();
		PsStub.SetupReplenish request = new PsStub.SetupReplenish();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		//request.setCardId(136186);//dev
		request.setCardId(123774);//integration
		request.setReplenishCardNum(testCardNum);
		request.setReplenishSecurityCode(replenishSecurityCode);
		request.setReplenishExpMonth(3);
		request.setReplenishExpYear(2015);
		request.setAddress1("100 Deerfield Lane");
		request.setCity("Malvern");
		request.setCountry("US");
		request.setPostal("19355");
		request.setState("PA");
		request.setAmount(new BigDecimal(40));
		request.setReplenishType(1);// auto=1, manual=2
		request.setThreshhold(new BigDecimal(20));
		PsStub.SetupReplenishResponse responseMessage;
		try {
			responseMessage = ps.setupReplenish(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetReplenishInfo() throws Exception {		
		init();
		PsStub.GetReplenishInfo request = new PsStub.GetReplenishInfo();
		request.setUsername(DEV_USERNAME);
		request.setPassword(PASSWORD);
		//request.setCardId(136186);
		request.setCardId(124936);
		PsStub.GetReplenishInfoResponse responseMessage;
		try {
			responseMessage = ps.getReplenishInfo(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSReplenishInfoArray response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testUpdateReplenish() throws Exception {		
		init();
		PsStub.UpdateReplenish request = new PsStub.UpdateReplenish();
		request.setUsername(DEV_USERNAME);
		request.setPassword(PASSWORD);
		//request.setCardId(136186);
		//request.setReplenishId(21);
		request.setCardId(124936);
		request.setReplenishId(141);
		request.setAmount(new BigDecimal(20));
		request.setReplenishType(2);
		request.setThreshhold(new BigDecimal(10));
		request.setReplenCount(13);
		PsStub.UpdateReplenishResponse responseMessage;
		try {
			responseMessage = ps.updateReplenish(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testRequestReplenish() throws Exception {
		init();
		PsStub.RequestReplenish request = new PsStub.RequestReplenish();
		request.setUsername(DEV_USERNAME);
		request.setPassword(PASSWORD);
		request.setCardId(124936);
		request.setReplenishId(141);
		request.setAmount(new BigDecimal(40));
		PsStub.RequestReplenishResponse responseMessage;
		try {
			responseMessage = ps.requestReplenish(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetPromoCampaigns() throws Exception {		
		init();
		PsStub.GetPromoCampaigns request = new PsStub.GetPromoCampaigns();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		PsStub.GetPromoCampaignsResponse responseMessage;
		try {
			responseMessage = ps.getPromoCampaigns(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSPromoCampaigns response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetLocations() throws Exception {		
		init();
		PsStub.GetLocations request = new PsStub.GetLocations();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		request.setLongitude(new BigDecimal("-75.53221"));
		request.setLatitude(new BigDecimal("40.042957"));
		PsStub.GetLocationsResponse responseMessage;
		try {
			responseMessage = ps.getLocations(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSPromoCampaigns response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testResetPassword() throws Exception {		
		init();
		PsStub.ResetPassword request = new PsStub.ResetPassword();
		request.setUsername("yhe@usatech.com");
		request.setPassword("Password1");
		PsStub.ResetPasswordResponse responseMessage;
		try {
			responseMessage = ps.resetPassword(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetConsumerSettings() throws Exception {		
		init();
		PsStub.GetConsumerSettings request = new PsStub.GetConsumerSettings();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		PsStub.GetConsumerSettingsResponse responseMessage;
		try {
			responseMessage = ps.getConsumerSettings(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSConsumerSettings response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testUpdateConsumerSettings() throws Exception {		
		init();
		PsStub.UpdateConsumerSettings request = new PsStub.UpdateConsumerSettings();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		PsStub.PSConsumerSetting[] setting=new PsStub.PSConsumerSetting[1];
		setting[0]=new PsStub.PSConsumerSetting();
		setting[0].setSettingId("1");
		setting[0].setValue("N");
		request.setConsumerSettings(setting);
		PsStub.UpdateConsumerSettingsResponse responseMessage;
		try {
			responseMessage = ps.updateConsumerSettings(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetPostalInfo() throws Exception {		
		init();
		PsStub.GetPostalInfo request = new PsStub.GetPostalInfo();
		request.setPostalCd("19355");
		request.setCountryCd("US");
		PsStub.GetPostalInfoResponse responseMessage;
		try {
			responseMessage = ps.getPostalInfo(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSPostalInfo response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetLocationsByUsage() throws Exception {		
		init();
		PsStub.GetLocationsByUsage request = new PsStub.GetLocationsByUsage();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		PsStub.GetLocationsByUsageResponse responseMessage;
		try {
			responseMessage = ps.getLocationsByUsage(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSPromoCampaigns response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetReplenishments() throws Exception {		
		init();
		PsStub.GetReplenishments request = new PsStub.GetReplenishments();
		request.setUsername(TESTUSERNAME);
		request.setPassword(TESTPASSWORD);
		Date startTime=ConvertUtils.convertRequired(Date.class, "02/01/2014");
		Date endTime=ConvertUtils.convertRequired(Date.class, "03/07/2014");
		request.setStartTime(startTime);
		request.setEndTime(endTime);
		//request.setCardId(136385L);
		PsStub.GetReplenishmentsResponse responseMessage;
		try {
			responseMessage = ps.getReplenishments(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSReplenishmentArray response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetChargedCards() throws Exception {		
		init();
		PsStub.GetChargedCards request = new PsStub.GetChargedCards();
		request.setUsername(TESTUSERNAME);
		request.setPassword(TESTPASSWORD);
		PsStub.GetChargedCardsResponse responseMessage;
		try {
			responseMessage = ps.getChargedCards(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSChargedCardArray response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testAuthHolds() throws Exception {		
		init();
		PsStub.GetAuthHolds request = new PsStub.GetAuthHolds();
		request.setUsername(TESTUSERNAME);
		request.setPassword(TESTPASSWORD);
		PsStub.GetAuthHoldsResponse responseMessage;
		try {
			responseMessage = ps.getAuthHolds(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSAuthHoldArray response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetLocationsByLocation() throws Exception {		
		init();
		PsStub.GetLocationsByLocation request = new PsStub.GetLocationsByLocation();
		request.setUsername(TESTUSERNAME);
		request.setPassword(TESTPASSWORD);
		
		request.setPostal("19355");
		request.setCountry("US");
		request.setMaxProximityMiles(1f);
		request.setDiscountsOnly(true);
		PsStub.GetLocationsByLocationResponse responseMessage;
		try {
			responseMessage = ps.getLocationsByLocation(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSPromoCampaigns response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testRetokenize() throws Exception {		
		init();
		PsStub.Retokenize request= new PsStub.Retokenize();
		request.setUsername("1111111111");
		request.setPassword("Password1");
		request.setCardId(140308);
		request.setSecurityCode("9385");
		PsStub.RetokenizeResponse responseMessage;
		try {
			responseMessage = ps.retokenize(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSTokenResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testGetRegions() throws Exception {		
		init();
		PsStub.GetRegions request = new PsStub.GetRegions();
		request.setUsername("1111111111");
		request.setPassword("Password1");
		PsStub.GetRegionsResponse responseMessage;
		try {
			responseMessage = ps.getRegions(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSRegions response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testUpdateRegion() throws Exception {		
		init();
		PsStub.UpdateRegion request = new PsStub.UpdateRegion();
		request.setUsername("1111111111");
		request.setPassword("Password1");
		request.setRegionId(290);
		PsStub.UpdateRegionResponse responseMessage;
		try {
			responseMessage = ps.updateRegion(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testAddPromoCode() throws Exception {		
		init();
		PsStub.AddPromoCode request = new PsStub.AddPromoCode();
		request.setUsername(TESTUSERNAME);
		request.setPassword(TESTPASSWORD);
		request.setPromoCode("xrJQMX");
		PsStub.AddPromoCodeResponse responseMessage;
		try {
			responseMessage = ps.addPromoCode(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testRegisterAnotherCardKeyed() throws Exception {		
		init();
		PsStub.RegisterAnotherCardKeyed request = new PsStub.RegisterAnotherCardKeyed();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		String cardNum="4055011111111111";
		String securityCode="123";
	    String billingCountry="US";
		String billingAddress="100 Deerfield Lane";
		String billingPostalCode="19355";
		request.setCardNum(cardNum);
		request.setSecurityCode(securityCode);
		request.setExpMonth(2);
		request.setExpYear(2018);
		request.setBillingCountry(billingCountry);
		request.setBillingAddress(billingAddress);
		request.setBillingPostalCode(billingPostalCode);
		PsStub.RegisterAnotherCardKeyedResponse responseMessage;
		try {
			responseMessage = ps.registerAnotherCardKeyed(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testCreateUserKeyed() throws Exception {		
		init();
		PsStub.CreateUserKeyed request = new PsStub.CreateUserKeyed();
		request.setBillingAddress("100 Deerfield Lane");
		request.setBillingCountry("US");
		request.setBillingPostalCode("19355");
		request.setExpMonth(2);
		request.setExpYear(2018);
		String promoCode="nTtcTA";
		request.setPromoCode(promoCode);
		request.setEmail("yhe888888@usatech.com");
		request.setPreferredCommType(1);
		request.setMobile("2222222222");
		request.setCarrierId(4);
		request.setPassword(PASSWORD);
		request.setConfirmPassword(PASSWORD);
		request.setAddress1("100 Deerfield Lane");
		request.setCity("Malvern");
		request.setCountry("US");
		request.setFirstname("Ying");
		request.setLastname("He");
		request.setPostal("19355");
		request.setState("PA");
		PsStub.PSConsumerSetting[] setting=new PsStub.PSConsumerSetting[1];
		setting[0]=new PsStub.PSConsumerSetting();
		setting[0].setSettingId("1");
		setting[0].setValue("N");
		request.setConsumerSettings(setting);
		request.setCardNum("4118960420454783");
		request.setSecurityCode("2418");
		PsStub.CreateUserKeyedResponse responseMessage;
		try {
			responseMessage = ps.createUserKeyed(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testCreateUserEncrypted() throws Exception {		
		init();
		PsStub.CreateUserEncrypted request = new PsStub.CreateUserEncrypted();
		int cardReaderType=1;
		String cardDataPlain=";6396212009867237415=1809000511425?";
		String encryptedCardDataHex= "0B4D52DFEC64FC574078815D90CF07D4657EA2D1C662EB278AAE1C2F3C4AB6BE20C011365C7A8757";
		String ksnHex="62994996340010200003";
		String entryType="S";
		request.setEntryType(entryType);
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(cardDataPlain.length());
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
		request.setBillingAddress("100 Deerfield Lane");
		request.setBillingCountry("US");
		request.setBillingPostalCode("19355");
		String promoCode="nTtcTA";
		request.setPromoCode(promoCode);
		request.setEmail("yhe8888888@usatech.com");
		request.setPreferredCommType(1);
		request.setMobile("2222222222");
		request.setCarrierId(4);
		request.setPassword(PASSWORD);
		request.setConfirmPassword(PASSWORD);
		request.setAddress1("100 Deerfield Lane");
		request.setCity("Malvern");
		request.setCountry("US");
		request.setFirstname("Ying");
		request.setLastname("He");
		request.setPostal("19355");
		request.setState("PA");
		PsStub.PSConsumerSetting[] setting=new PsStub.PSConsumerSetting[1];
		setting[0]=new PsStub.PSConsumerSetting();
		setting[0].setSettingId("1");
		setting[0].setValue("N");
		request.setConsumerSettings(setting);
		PsStub.CreateUserEncryptedResponse responseMessage;
		try {
			responseMessage = ps.createUserEncrypted(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
	@Test
	public void testRegisterAnotherCardEncrypted() throws Exception {		
		init();
		PsStub.RegisterAnotherCardEncrypted request = new PsStub.RegisterAnotherCardEncrypted();
		request.setUsername(USERNAME);
		request.setPassword(PASSWORD);
		String entryType="S";
		request.setEntryType(entryType);
		int cardReaderType=1;
		String cardDataPlain=";6396212009476659660=1809003771893?";
		String encryptedCardDataHex = "0B4D52DFEC64FC5784591C5554F8EE96181543D876E720A38438D4D23FBF8000B85305BCAAAD7E1A";
		String ksnHex="62994996340010200003";
		request.setCardReaderType(cardReaderType);
		request.setDecryptedCardDataLen(cardDataPlain.length());
		request.setEncryptedCardDataHex(encryptedCardDataHex);
		request.setKsnHex(ksnHex);
	    String billingCountry="US";
		String billingAddress="100 Deerfield Lane";
		String billingPostalCode="19355";
		request.setBillingCountry(billingCountry);
		request.setBillingAddress(billingAddress);
		request.setBillingPostalCode(billingPostalCode);
		PsStub.RegisterAnotherCardEncryptedResponse responseMessage;
		try {
			responseMessage = ps.registerAnotherCardEncrypted(request);
		} finally {
			ps._getServiceClient().cleanupTransport();
		}
		if (responseMessage == null || responseMessage.get_return() == null)
			throw new Exception("Received null response");
		else {
			PsStub.PSResponse response = responseMessage.get_return();
			String message = StringUtils.objectToString(response, "Response: ");
			if (response.getReturnCode() == RES_OK)
				System.out.println(message);
			else
				throw new Exception(message);
		}
	}
	
}
