<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
</div>	
    <footer class="footer"><%
    Translator translator = RequestUtils.getTranslator(request);
    String footerImageTitle = translator.translate("prepaid-footer-image-title", "");
    PrepaidUser user = (PrepaidUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
    if(!StringUtils.isBlank(footerImageTitle)) {%><img src="<%=ToJspSimpleServlet.untranslatePath(request, "/bottom_logo._")%>" alt="<%=StringUtils.prepareCDATA(footerImageTitle)%>" /><%}
    %>
    <%if (StringUtils.isBlank(RequestUtils.getAttribute(request, "survey_device", String.class, false))) {
    	
    %>
    <div class="footer-links">
      <div class="terms-links">
      	<span>Terms and Conditions   </span><a id="prepaid_termslink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/morecard_terms_conditions.html") %>">Prepaid</a>&nbsp;|&nbsp;<a id="credit_termslink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/allcards_terms_conditions.html") %>">Credit</a>&nbsp;|&nbsp;<a id="payroll_termslink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/payroll_deduct_cards_terms_conditions.html") %>">Payroll</a>
      </div>
      <div class="privacy-links">  
        <span>Privacy and Policy     </span><a id="prepaid_privacylink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/morecard_privacy_policy.html") %>">Prepaid</a>&nbsp;|&nbsp;<a id="privacylink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/allcards_privacy_policy.html") %>">Credit</a>&nbsp;|&nbsp;<a id="privacylink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/payroll_deduct_cards_privacy_policy.html") %>">Payroll</a>
      </div>	    
    </div>
        
    <% 
    	
    }%>
    <div class="footer-site-info">
      <div>
        <span class="more-text">more.</span> v.<%=StringUtils.encodeForHTML(GenerateUtils.getApplicationVersion())%> &ndash; A product of <a href="http://www.usatech.com">USA Technologies, Inc.</a></div>        
        <div>&copy; Copyright 2003-<%=GenerateUtils.getCurrentYear()%></div>
        <!-- 
        <div class="trustwave-seal">
        <img id="trustwaveSealImage" src="/images/trustwave-seal70x35.png" onclick="javascript:window.open('https://sealserver.trustwave.com/cert.php?customerId=w6o8pBiUhhnnGnDB1cfZ1FWOQ66BnG&amp;size=105x54&amp;style=invert', 'c_TW', 'location=no, toolbar=no, resizable=yes, scrollbars=yes, directories=no, status=no, width=615, height=720'); return false;" oncontextmenu="javascript:alert('Copying Prohibited by Law - Trusted Commerce is a Service Mark of TrustWave Holdings, Inc.'); return false;" alt="This site protected by Trustwave's Trusted Commerce program" title="This site protected by Trustwave's Trusted Commerce program"/> 
        </div>  
        --> 
    </div>
    </footer>
    </body>
</html>