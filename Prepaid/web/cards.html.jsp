<%@page import="simple.io.Log"%>
<%@page import="com.usatech.prepaid.web.CardOnFile"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="simple.lang.SystemUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.results.Results.Aggregate"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="java.util.TimeZone"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.prepaid.pass.PassServletUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
if(user == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-logged-in", "You are no longer logged in. Please log in again.");
    RequestUtils.redirectWithCarryOver(request, response, "signin.html");
    return;
}
Long cardId = RequestUtils.getAttribute(request, "cardId", Long.class, false);
if(cardId != null && PrepaidUtils.checkCardPermission(user, cardId) == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
    RequestUtils.redirectWithCarryOver(request, response, PrepaidUtils.getHomePage());
    return;
}
Integer addConsumerAcctTypeId = RequestUtils.getAttribute(request, "addConsumerAcctTypeId", Integer.class, false);
TimeZone timeZone = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_TIME_ZONE, TimeZone.class, false, RequestUtils.SESSION_SCOPE);
Locale locale = RequestUtils.getLocale(request);
RequestUtils.setAttribute(request, "tab", "cards", "request"); 
RequestUtils.setAttribute(request, "extra-stylesheets", "/css/pikaday.css", "request"); 
    		
BigDecimal replenAmount = RequestUtils.getAttribute(request, "amount", BigDecimal.class, false);
%>
<jsp:include page="/account_header.jsp" />
<iframe name="passDownload" style="width:0; height:0; display:none;"></iframe>
<% 
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
HashMap<CardOnFile, ArrayList<String>> cardMap=new HashMap<CardOnFile, ArrayList<String>>();
Results results = DataLayerMgr.executeQuery("GET_CHARGE_CARDS", user);
results.setLocale(locale);
while(results.next()) {
	CardOnFile card=new CardOnFile(ConvertUtils.getLong(results.getFormattedValue("replenConsumerAcctId")),results.getFormattedValue("replenishCardNum"), results.getFormattedValue("currencyCd"));
	int priority=ConvertUtils.getInt(results.getFormattedValue("priority"));
	if(cardMap.containsKey(card)){
		if(priority==1){
			ArrayList<String> prepaidCardList=cardMap.get(card);
			prepaidCardList.add(results.getFormattedValue("cardNum"));
		}
	}else{
		ArrayList<String> prepaidCardList=new ArrayList<String>();
		if(priority==1){
			prepaidCardList.add(results.getFormattedValue("cardNum"));
		}
		cardMap.put(card,prepaidCardList);
	}
}   		
results = DataLayerMgr.executeQuery("GET_ACCOUNTS_PREPAID", user);
results.setLocale(locale);
String cardFormat = "simple.text.PadFormat:        *";
String currencyFormat = "CURRENCY"; 
results.setFormat("cardNum", cardFormat);
results.setFormat("balance", currencyFormat);
results.setFormat("onHold", currencyFormat);
DateFormat[] dateFormats = PrepaidUtils.getStandardDateFormats(locale, timeZone);
String currencyCd = null;
String balanceText = null;
String dateText = null;
String onHoldText = null;
Long consumerAcctIdentifier = null;
%>
        <div>
            <!--    <h5>Replenishment Settings for </h5>  -->
            <div class="account-table-header">
                <form class="transactions-card-select form-inline form-flow">
                    <strong>Replenishment Settings for</strong> <%
int cardCount = results.getRowCount();
int cardOrder = 0;
String cardNum = "";
String currentNickName="";
String sproutAccountId;
if(cardCount == 1) {
    results.next();
    currencyCd = results.getFormattedValue("currencyCd");
   	balanceText = results.getFormattedValue("balance");
   	consumerAcctIdentifier = results.getValue("consumerAcctIdentifier", Long.class);
    onHoldText = results.getFormattedValue("onHold");
    dateText = ConvertUtils.formatObject(results.getValue("lastReplenishDate"), dateFormats[1]);
    cardOrder = 1;
    cardNum = results.getFormattedValue("cardNum");
    cardId = results.getValue("cardId", Long.class);
    form.setAttribute("cardId", cardId);
    %><%=StringUtils.prepareHTML(results.getFormattedValue("activeStatus"))%> Loyalty Card <span class="cardNum"><%=StringUtils.prepareHTML(cardNum)%>
    <%String nickName=results.getFormattedValue("nickName");
    if(!StringUtils.isBlank(nickName)){ %>
    (<%=StringUtils.prepareHTML(nickName)%>)
    <%} %>
    </span><%
} else {%>
    <select name="cardId" onchange="this.form.submit()">
    <option value=""<%if(cardId == null) {
    	balanceText = results.getFormattedAggregate("balance", null, Aggregate.SUM);
    	onHoldText = results.getFormattedAggregate("onHold", null, Aggregate.SUM);
    	dateText = ConvertUtils.formatObject(results.getAggregate("lastReplenishDate", null, Aggregate.MAX), dateFormats[1]);
        %> selected="selected"<%} %>>All</option><%
int i = 1;
while(results.next()) {
	String cc = results.getFormattedValue("currencyCd");
    long id = results.getValue("cardId", Long.class);
    %><option value="<%=id%>"<%
    String nickName=results.getFormattedValue("nickName");
    if(user.getConsumerTypeId()==8){
		if(i==1&&cardId==null){
			cardId=id;
			form.set("cardId", cardId);
    	}
	}
    if(cardId == null) {
        if(currencyCd == null)
            currencyCd = cc;
        else if(!currencyCd.equals(cc)) {
            currencyCd = "(mixed)";
        }
    } else {
        if(cardId.longValue() == id) {
        	cardOrder = i;
        	cardNum = results.getFormattedValue("cardNum");
            currencyCd = cc;
            balanceText = results.getFormattedValue("balance");
            consumerAcctIdentifier = results.getValue("consumerAcctIdentifier", Long.class);
            onHoldText = results.getFormattedValue("onHold");
            dateText = ConvertUtils.formatObject(results.getValue("lastReplenishDate"), dateFormats[1]);
            currentNickName=nickName;
	    %> selected="selected"<% 
	    }
	}%>><%=StringUtils.prepareHTML(results.getFormattedValue("activeStatus"))%> Loyalty Card <%=i++ %>: <%=StringUtils.prepareHTML(results.getFormattedValue("cardNum"))%>
	<%if(!StringUtils.isBlank(nickName)){ %>
    (<%=StringUtils.prepareHTML(nickName)%>)
    <%} %>
	</option><%    
}%> </select><%
}
int maxReplenishments = 10;
Map<String,Object> params = new HashMap<String,Object>();
params.put("cardId", cardId);
params.put("simple.servlet.ServletUser", user);
params.put("maxRows", maxReplenishments); 
Date startTime = RequestUtils.getAttribute(request, "startTime", Date.class, false);
Date endTime = RequestUtils.getAttribute(request, "endTime", Date.class, false);
params.put("startTime", startTime);
params.put("endTime", endTime);
String dateInputFormat = "DATE:yyyy-MM-dd";
Results replenishResults = DataLayerMgr.executeQuery("GET_REPLENISHMENTS", params);
replenishResults.trackAggregate("amount", Aggregate.SUM);
replenishResults.trackAggregate("date", Aggregate.MAX);
replenishResults.setFormat("replenishedCard", cardFormat);
String numberFormat = "NUMBER:#0.00;(#0.00)";
replenishResults.setLocale(locale);
if(StringUtils.isBlank(dateText) && (endTime == null || endTime.getTime() + (5 * 60 * 1000) >= System.currentTimeMillis())) {
	dateText = ConvertUtils.formatObject(replenishResults.getAggregate("date", null, Aggregate.MAX), dateFormats[1]);
}
String expand = RequestUtils.getAttribute(request, "expand", String.class, false);
String cardBrand = RequestUtils.getTranslator(request).translate("prepaid-card-brand", "loyalty");
%><a class="btn-link action-link add-link" data-toggle="expand" data-target="newCardDetails">ADD <%=StringUtils.prepareHTML(cardBrand.toUpperCase())%> CARD</a>
                </form>
<%
String action = form.getStringSafely("action", "");
if ("Add to Apple Wallet".equalsIgnoreCase(action) && cardId != null) {
	String passUrl = PassServletUtils.createPassUrlByCardId(cardId);
%>
	<script type="text/javascript">
	window.open("<%=passUrl%>", "passDownload");
	</script>
<%}%>
                <div class="clearfix"></div>
                <div id="newCardDetails"<%if(!"new".equalsIgnoreCase(expand)) {%> class="hide"<%} %>>
                    <form class="register-card-form" action="register_card.html" method="post">
                    <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
                    <input id="addConsumerAcctTypeId" type="radio" <%if(addConsumerAcctTypeId==null||(addConsumerAcctTypeId!=null&&addConsumerAcctTypeId==3)){ %>checked="checked" <%} %> onclick="onAddConsumerAcctTypeChange(3);" value="3" name="addConsumerAcctTypeId"> MORE Prepaid Card
					<%if(PrepaidUtils.isAllowSproutSignUp()){ %>
					<input id="addConsumerAcctTypeId" type="radio" <%if(addConsumerAcctTypeId!=null&&addConsumerAcctTypeId==6){ %>checked="checked" <%} %> onclick="onAddConsumerAcctTypeChange(6);" value="6" name="addConsumerAcctTypeId"> Sprout Card
                   	<%} %>
                   	<div id="addMorePrepaidInput" <%if(addConsumerAcctTypeId==null||(addConsumerAcctTypeId!=null&&addConsumerAcctTypeId==3)){ %>style="display:block" <%}else{ %> style="display:none"<%} %>>
                    <label><span>Your 19-digit MORE Card Number</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-cardnumber_highlighted.gif' alt='' height='172' width='266'/&gt;">Where is this?</a></label>                      
	                <input type="text" id="morePrepaidCard" <%if(addConsumerAcctTypeId==null||(addConsumerAcctTypeId!=null&&addConsumerAcctTypeId==3)){ %>required="required" <%} %> name="card" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card number" title="The 19-digit <%=StringUtils.prepareCDATA(cardBrand) %> card number"  maxlength="19" autofocus="autofocus" autocomplete="off" pattern="\d{19}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "card", String.class, false))%>"/>
	                <label><span>The 4-digit Card Security Code</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-securitycode_highlighted.gif' alt='' height='172' width='266'/&gt;">Where is this?</a></label>                        
	                <input type="password" id="morePrepaidSecurityCode" <%if(addConsumerAcctTypeId==null||(addConsumerAcctTypeId!=null&&addConsumerAcctTypeId==3)){ %>required="required" <%} %> name="securityCode" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card's security code" title="The 4-digit security code on the back of your <%=StringUtils.prepareCDATA(cardBrand) %> card"  maxlength="4" autocomplete="off" pattern="\d{4}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "securityCode", String.class, false))%>"/>
	                </div>
	                <div id="addSproutInput" <%if(addConsumerAcctTypeId!=null&&addConsumerAcctTypeId==6){ %>style="display:block" <%}else{ %> style="display:none"<%} %> >
                    <label><span>Your 16-digit Sprout Card Number</span></label>                      
	                <input type="text" id="sproutCard" <%if(addConsumerAcctTypeId!=null&&addConsumerAcctTypeId==6){ %> required="required" <%} %> name="sproutCard" placeholder="Enter your Sprout card number" title="The 16-digit Sprout card number" maxlength="16" autofocus="autofocus" autocomplete="off" pattern="\d{16}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "sproutCard", String.class, false))%>"/>
	                <label><span>The last 4-digit of the OAN number at the back of the card</span></label>                      
	                <input type="password" id="sproutSecurityCode" name="sproutSecurityCode" <%if(addConsumerAcctTypeId!=null&&addConsumerAcctTypeId==6){ %> required="required" <%} %> placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card's security code" title="The 4-digit security code on the back of your Sprout card"  maxlength="4" autocomplete="off" pattern="\d{4}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "sproutSecurityCode", String.class, false))%>"/>
	                </div>
                    <label><span>Card Nickname:</span><span class="optional">Optional</span></label>
                    <input type="text" maxLength="60" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "nickname", String.class, false))%>" title="Enter nickname for the card" name="nickname"> 
                    <div class="form-actions text-center">
                        <button type="submit" class="btn btn-savechanges">Add <%=StringUtils.prepareHTML(StringUtils.capitalizeAllWords(cardBrand))%> Card</button>
                    </div>
                    </form>
                 </div>
            </div>
			<%
			if (cardId != null && consumerAcctIdentifier != null) {
				String passUrl = PassServletUtils.getPassUrlByCardId(cardId);
				%>
				<script type="text/javascript">
				function addToAppleWallet() {
					<%if (StringUtils.isBlank(passUrl)) {%>
					document.getElementById("appleWalletForm").submit();
					<%} else {%>
					window.open("<%=passUrl%>", "passDownload");
					<%}%>
				}
				</script>
				<div class="spacer10"></div>
				To add your loyalty card to Apple Wallet, please open this web page on your iPhone or iPod touch.
				<div class="spacer10"></div>
				<form id="appleWalletForm" method="post">
					<input type="hidden" name="action" value="Add to Apple Wallet" />
					<a href="javascript:addToAppleWallet();"><img src="/images/Add_to_Apple_Wallet_rgb_US-UK.svg" style="width: 100%; max-width: 175px;" alt="Add to Apple Wallet" /></a></td>
				</form>
				<hr/>
			<%}%>
            <span class="replenish-announcements">
                <span class="replenish-current-balance"><strong>Current Balance:</strong> <%=StringUtils.prepareHTML(balanceText) %> <%=StringUtils.prepareHTML(currencyCd) %></span>
                <span class="last-replenished"><strong>Last Replenished:</strong> <%=StringUtils.prepareHTML(dateText) %></span><div class="clearfix"></div><%
if(!StringUtils.isBlank(onHoldText)) {
	%><span class="replenish-current-balance"><strong>On Hold:</strong> <%=StringUtils.prepareHTML(onHoldText) %> <%=StringUtils.prepareHTML(currencyCd) %> <span class="action-link"><a class="btn-link" data-toggle="expand" data-target="onHoldDetails">DETAILS</a></span></span><div class="clearfix"></div><%
}%></span>
<%
if(!StringUtils.isBlank(onHoldText)) {
	results = DataLayerMgr.executeQuery("GET_AUTH_HOLDS", form);
	results.setFormat("card", cardFormat);
	%><div id="onHoldDetails"<%if(!"holds".equalsIgnoreCase(expand)) { %>class="hide"<%} %>>
    <table class="table table-striped table-bordered-squarecorners table-hover account-table" id="accountTable">
        <thead>
            <tr>
                <th class="th-date"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Date" class="btn-link">Date</a></th>
                <%if(cardId == null) { %><th class="th-card"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Card" class="btn-link">Card</a></th><%} %>                
                <th class="th-location"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Location" class="btn-link">Location</a></th>
                <th class="th-amt"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by $ Amount" class="btn-link">$ Amount</a></th>
            </tr>
        </thead><tbody><%
        while(results.next()) {
        	String cc = results.getFormattedValue("currencyCd");
            currencyFormat = (StringUtils.isBlank(cc) ? "CURRENCY" : "CURRENCY:" + cc);
            Object date = results.getValue("date");
            BigDecimal amount = results.getValue("amount", BigDecimal.class);
            String city = results.getFormattedValue("city");
            String state = results.getFormattedValue("state");
            String postal = results.getFormattedValue("postal");
            String country = results.getFormattedValue("country");
            StringBuilder sb = new StringBuilder();
            if(city != null && !(city=city.trim()).isEmpty()) {
                sb.append(city);
                if(state != null && !(state=state.trim()).isEmpty())
                    sb.append(", ").append(state);
            } else if(state != null && !(state=state.trim()).isEmpty())
                sb.append(state);
            if(postal != null && !(postal=postal.trim()).isEmpty()) {
                if(sb.length() > 0)
                    sb.append(' ');
                sb.append(postal);
            }
            if(country != null && !(country=country.trim()).isEmpty()) {
                if(sb.length() > 0)
                    sb.append(' ');
                sb.append(country);
            }
            String[] details = {
                results.getFormattedValue("location"),
                results.getFormattedValue("address1"),
                results.getFormattedValue("address2"),
                sb.toString()         
            };
    %><tr class="tran-pending-purchase-row">
                <td data-sort-value="<%=ConvertUtils.convert(Long.class, date) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(date, dateFormats[1])) %></td>
                <%if(cardId == null) { %><td data-sort-value="<%=StringUtils.prepareCDATA(results.getFormattedValue("card")) %>"><%=StringUtils.prepareHTML(results.getFormattedValue("card")) %></td><%} %>
                <td data-sort-value="<%=StringUtils.prepareCDATA(results.getFormattedValue("location")) %>"><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" data-trigger="hover&amp;click" data-content="ID: <%=StringUtils.prepareCDATA(results.getFormattedValue("id")).replace("&", "&amp;") %>&lt;br/&gt;<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(amount, currencyFormat)).replace("&", "&amp;")%><%
   for(String s : details) {
     if(s != null && !(s=s.trim()).isEmpty()) {%>&lt;br/&gt;<%=StringUtils.prepareCDATA(s).replace("&", "&amp;")%><%}
   }%>" data-title="Pending Transaction - <%=StringUtils.prepareCDATA(ConvertUtils.formatObject(date, dateFormats[0])).replace("&", "&amp;")%>"><%=StringUtils.isBlank(details[0]) ? "Unspecified" : StringUtils.prepareCDATA(details[0])%></a></td>
                <td data-sort-value="<%=SystemUtils.nvl(amount, BigDecimal.ZERO) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(amount, numberFormat)) %></td>
            </tr><%
        }%></tbody></table>
    </div><%
}
if(cardId != null) {
    String address1 = RequestUtils.getAttribute(request, "address1", String.class, false);
    String country = RequestUtils.getAttribute(request, "country", String.class, false);
    String postal = RequestUtils.getAttribute(request, "postal", String.class, false);
    if(StringUtils.isBlank(address1) || StringUtils.isBlank(country) || StringUtils.isBlank(postal)) {
        results = DataLayerMgr.executeQuery("GET_USER_INFO", user);
        results.setLocale(locale);
        if(results.next()) {
            if(StringUtils.isBlank(address1))
                address1 = results.getValue("address1", String.class);
            if(StringUtils.isBlank(country))
                country = results.getValue("country", String.class);
            if(StringUtils.isBlank(postal))
                postal = results.getValue("postal", String.class);
        }
    }
    String replenishCardNum = RequestUtils.getAttribute(request, "replenishCardNum", String.class, false);
    String existingCardNum = null;
    BigDecimal threshhold = RequestUtils.getAttribute(request, "threshhold", BigDecimal.class, false);
    BigDecimal amount = RequestUtils.getAttribute(request, "amount", BigDecimal.class, false);
    boolean stopped = false;
    Integer replenishType = RequestUtils.getAttribute(request, "replenishType", Integer.class, false);
    boolean pending = true;
    boolean existing;
    Long replenishId; // = RequestUtils.getAttribute(request, "replenishId", Long.class, false);
    results = DataLayerMgr.executeQuery("GET_REPLENISH_INFO", form);
    results.setLocale(locale);
    Object[] vals;
    int deniedCount;
    int replenDailyCount = 0;
    int successReplenishCount = 0;
    boolean replenishNowAvailable = true;
    long existingReplenConsumerAcctId=-1;
    if(results.next()) {
        existing = true;
        existingReplenConsumerAcctId=ConvertUtils.getIntSafely(results.getValue("replenConsumerAcctId"),-1);
        replenishId = results.getValue("replenishId", Long.class);
        existingCardNum = results.getFormattedValue("replenishCardNum");
        if(replenishType == null)
            replenishType = results.getValue("replenishType", Integer.class);
        BigDecimal configuredAmount = results.getValue("replenishAmount", BigDecimal.class);
        stopped = (configuredAmount != null && configuredAmount.signum() == 0);
        if(threshhold == null || threshhold.signum() <= 0)
            threshhold = results.getValue("replenishThreshhold", BigDecimal.class);
        if(amount == null || amount.signum() <= 0)
            amount = configuredAmount; 
        pending = StringUtils.isBlank(results.getValue("replenishCardKey", String.class));
        vals = new Object[] {results.getValue("replenishType", Integer.class), existingCardNum, results.getFormattedValue("replenishAmount"), results.getFormattedValue("replenishThreshhold")};
        deniedCount = results.getValue("deniedCount", Integer.class);
        replenDailyCount = results.getValue("replenDailyCount", Integer.class);
        successReplenishCount = results.getValue("successReplenishCount", Integer.class);
    } else {
        existing = false;
        if(replenishType == null)
        	replenishType = 1;
        replenishId = null;
        vals = new Object[] {0};
        deniedCount = 0;
        if (amount == null)
        	amount = new BigDecimal(20);
        if (threshhold == null)
        	threshhold = new BigDecimal(5);
    }
    Translator translator = RequestUtils.getTranslator(request);
    String replenishText;
    if(deniedCount <= 0 || pending || replenishType != 1){
    	if((replenDailyCount !=0)&&(successReplenishCount >= replenDailyCount)){
        	replenishText = translator.translate("prepaid-replenish-settings-replen-count-reached-text", "has reached the maximun replenishments count for day with credit card {1}.", successReplenishCount, vals[1], vals[2], vals[3]);
        	replenishNowAvailable = false;
    	}
        else replenishText = translator.translate("prepaid-replenish-settings-text", "{0,CHOICE,0#is not set up to replenish|1#will AUTO replenish by {2,CURRENCY} with credit card {1} when the balance falls below {3,CURRENCY}|2#will MANUALLY replenish by {2,CURRENCY} with credit card {1} only on your request}.", vals);
    }
    else if(deniedCount < 5)
    	replenishText = translator.translate("prepaid-replenish-settings-failing-text", "has failed {0} times to AUTO replenish by {2,CURRENCY} with credit card {1}. Please configure a different credit card.", deniedCount, vals[1], vals[2], vals[3]);
    else 
        replenishText = translator.translate("prepaid-replenish-settings-failed-text", "has failed {0} times to AUTO replenish by {2,CURRENCY} with credit card {1} and is disabled. Please configure a different credit card.", deniedCount, vals[1], vals[2], vals[3]);
    
%>  
<div class="more-accordion">
       <div class="more-accordion-group">
           <span class="replenish-settings-detail">
                <span class="replenish-settings-detail-inner">
                    <strong>Loyalty Card <%=StringUtils.prepareHTML(cardNum)%></strong> <%=StringUtils.prepareHTML(replenishText) %><%
if(existing && !pending) {
    %>&nbsp;
    <form class="form-inline form-flow" name="immediateForm" action="replenish.html" method="post">
<input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
<input type="hidden" name="cardId" value="<%=cardId%>"/>
<input type="hidden" name="replenishId" value="<%=replenishId%>"/>
<input type="hidden" name="operation" value="immediate"/>
<input type="hidden" name="amount" value="<%=amount%>"/>
<input type="hidden" name="replen_count" value="<%=replenDailyCount%>"/>
<%if (replenishNowAvailable){ %>
<button class="btn-link action-link replenish-link" type="submit"><%if(replenishType == 1 && deniedCount >= 5) {%>REATTEMPT REPLENISH<%} else { %>REPLENISH NOW<%} %></button>
<%} %>
</form><%
    }%>
               </span>
<span class="action-link edit-link"><a class="btn-link" data-toggle="expand" data-target="replenishDetails">EDIT CREDIT CARD</a></span></span>
            <div class="clearfix"></div>
	   </div>
	   <div id="replenishDetails"<%if(!"edit".equalsIgnoreCase(expand) && (replenAmount==null)) { %> class="hide"<%} %>>
            <div class="more-accordion-inner">
                <form id="replenish-form" action="replenish.html" method="post" class="form-horizontal replenishment-form">
                    <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
                    <input type="hidden" name="cardId" value="<%=cardId%>"/><%
                    if(replenishId != null) {%>
                    <input type="hidden" name="replenishId" value="<%=replenishId%>"/><%
                    } 
                    if(existing && pending) {%>
<em class="muted"><%=RequestUtils.getTranslator(request).translate("prepaid-replenish-pending-verification-note", "Initial verification is pending; you may not change the replenish setting at this time") %></em>
<%} %><div class="control-group">
                        <label class="control-label">Replenish&nbsp;Setting:</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="replenishType" value="1" onclick="replenishTypeChecked(this)"<%if(replenishType == 1) {%> checked="checked"<%} else if(existing && pending) {%>disabled="disabled"<%} %>/> Automatic
                            </label>
                            <label class="radio inline">
                                <input type="radio" name="replenishType" value="2" onclick="this.form.threshhold.disabled = this.checked"<%if(replenishType == 2) {%> checked="checked"<%} else if(existing && pending) {%>disabled="disabled"<%} %>/> Manual
                            </label><!-- 
                            <label class="radio inline">
                                <input type="radio" name="replenishType" value="0" onclick="this.form.threshhold.disabled = this.checked"<%if(replenishType == 0) {%> checked="checked"<%} else if(existing && pending) {%> disabled="disabled"<%} %>/> None
                            </label> -->
                        </div>
                    </div>
                    <%
                    String replenishmentBonusNote = translator.translate("prepaid-replenishment-note", "");
                    if (!StringUtils.isBlank(replenishmentBonusNote)) {%>
					<div class="control-group">
                        <span class="replenish-note">
                			<span class="replenish-note-inner">
                			<%=replenishmentBonusNote%>
                			</span>
                		</span>
					</div>
					<%} %>
                    <div class="control-group">
                        <label class="control-label">Replenish by:</label>
                        <div class="input-prepend controls">
                            <span class="add-on">$</span><%
BigDecimal[] replenishAmounts = PrepaidUtils.getReplenishAmounts();
if(replenishAmounts == null || replenishAmounts.length == 0) {
%><input type="number" name="amount" min="<%=PrepaidUtils.getMinReplenishAmount() %>" required="required" placeholder="10.00" value="<%=amount == null ? "" : StringUtils.prepareCDATA(amount.toString())%>" title="The amount that is pulled from your credit card to replenish your prepaid account"/><%
} else { 
    boolean selected = false;%><select name="amount" required="required" title="The amount that is pulled from your credit card to replenish your prepaid account">
<option value=""<%if(amount == null || amount.compareTo(PrepaidUtils.getMinReplenishAmount()) < 0) { 
    selected = true; %> selected="selected"<%} %>>--</option><%
for(BigDecimal value : replenishAmounts) {
    %><option value="<%=value%>"<%if(!selected && amount.compareTo(value) <= 0) {
    selected = true; %> selected="selected"<%} %>><%=ConvertUtils.formatObject(value, "NUMBER:#,##0.00", locale) %></option><%
}%>
</select><%} %>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">When balance falls below:</label>
                        <div class="input-prepend controls">
                            <span class="add-on">$</span><%
BigDecimal[] threshholdAmounts = PrepaidUtils.getThreshholdAmounts();
if(threshholdAmounts == null || threshholdAmounts.length == 0) {
%><input type="number" name="threshhold" min="<%=PrepaidUtils.getMinThreshholdAmount() %>" required="required" placeholder="5.00" <%if(replenishType == 2) {%> disabled="disabled"<%} %> value="<%=threshhold == null ? "" : StringUtils.prepareCDATA(threshhold.toString())%>" title="The minimum balance on your prepaid account before we replenish from your credit card"/><%
} else { 
    boolean selected = false;
%><select name="threshhold"<%if(replenishType == 2) {%> disabled="disabled"<%} %> required="required" title="The minimum balance on your prepaid account before we replenish from your credit card">
<option value=""<%if(threshhold == null || threshhold.compareTo(PrepaidUtils.getMinThreshholdAmount()) < 0) { 
    selected = true; %> selected="selected"<%} %>>--</option><%
for(BigDecimal value : threshholdAmounts) {
    %><option value="<%=value%>"<%if(!selected && threshhold.compareTo(value) <= 0) {
    selected = true; %> selected="selected"<%} %>><%=ConvertUtils.formatObject(value, "NUMBER:#,##0.00", locale) %></option><%
}%>
</select><%} %>
                        </div>
                    </div>
<%
Results replenCountResult = DataLayerMgr.executeQuery("GET_DAILY_REPLENISH_COUNT", form);
int replenCountInitial = 0;
if (replenCountResult.next()){
	replenCountInitial = ConvertUtils.getInt(replenCountResult.getFormattedValue("dailyCount"));
	}
%>
<div class="control-group">
	<label class="control-label">Maximum daily replenishment limit (times):</label>
   	<div class="controls"><input type="text" name="replen_count" maxlength="2" required="required" pattern="\d{1,2}" placeholder="Enter max count" value="<%=replenDailyCount < 1 ? replenCountInitial : replenDailyCount%>" title="The count of replenishments, which can be done in a day"/></div>
</div>
<input type="hidden" name="existingReplenConsumerAcctId" value="<%=existingReplenConsumerAcctId%>"/>
<%
String operation = RequestUtils.getAttribute(request, "operation", String.class, false);%>
<div class="control-group">
    <label class="control-label">Using credit card:</label>
    <div class="controls">
<%if(cardMap.size()>0 && CardOnFile.containsCurrencyCdCards(cardMap.keySet(), currencyCd)){ %>
<label class="radio"><input type="radio" name="operation" required="true" <%if(existingReplenConsumerAcctId>0) {%>checked="checked" <%}%>onclick="actionUpdated(this.form)" value="edit"/> Existing credit card:</label>
<select title="The credit card you want to use for replenishment" name="replenConsumerAcctId">
<% for (CardOnFile key : cardMap.keySet())  {%>
	<%if(currencyCd.equals(key.getCurrencyCd())){ %>
	<option <%if(existingReplenConsumerAcctId==key.getReplenConsumerAcctId()){ %>selected="selected" <% }%> value="<%=key.getReplenConsumerAcctId()%>"><%=StringUtils.prepareCDATA(key.getReplenishCardNum()) %></option>
	<%} %>
<%} %>
</select>
<label class="radio"><input type="radio" name="operation" required="true" <%if(existingReplenConsumerAcctId<0) {%>checked="checked" <%}%>onclick="actionUpdated(this.form)" value="add"/> New credit card</label>
<%} else{ %>
	<input type="hidden" name="operation" value="add"/>
<%} %>
</div>
</div>
<div id="creditCardDetails" class="hide">
<div class="more-accordion-inner">    
<div class="control-group">
   <label class="control-label">Bank Credit Card Number:</label>
   <div class="controls"><input type="text" name="replenishCardNum" placeholder="Enter credit card number" maxlength="16" required="required" pattern="\d{15,16}" autocomplete="off" value="<%=StringUtils.prepareCDATA(replenishCardNum)%>" title="The credit card number without spaces or punctuation. Usually 15 or 16 digits"/></div></div>
   <div class="control-group"><label class="control-label">Expiration Date:</label>
   <div class="controls"><select name="replenishExpMonth" class="autosize" required="required"><%
int month = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "replenishExpMonth", false), 0);
int year = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "replenishExpYear", false), 0);
Calendar now = Calendar.getInstance();
int currYear = now.get(Calendar.YEAR);%>
<option value=""<%if(month < 1 || month > 12) {%> selected="selected"<%} %>>--</option><%
for(int m = 1; m <= 12; m++) {%>
    <option<%if(month == m) {%> selected="selected"<%} %> value="<%=m%>"><%=StringUtils.pad(Integer.toString(m), '0', 2, StringUtils.Justification.RIGHT)%></option><%
}%></select>
<select name="replenishExpYear" class="autosize" required="required">
<option value=""<%if(year < currYear) {%> selected="selected"<%} %>>--</option><%
for(int y = currYear; y <= currYear + 10; y++) {%>
    <option<%if(year == y) {%> selected="selected"<%} %> value="<%=y%>"><%=y%></option><%
}%></select></div></div>
<div class="control-group"><label class="control-label">Security Code:</label>
    <div class="controls"><input type="password" name="cvv" maxlength="4" required="required" pattern="\d{3,4}" placeholder="Enter 3 or 4 digit code" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "cvv", String.class, false))%>" title="The 3 or 4 digit security code found on the back of your credit card"/></div></div>
<div class="control-group">
    <label class="control-label">Billing Country:</label>
    <div class="controls">
    <select name="country" title="Enter the country of your credit card billing address" required="required">
    <% for(GenerateUtils.Country c : GenerateUtils.getCountries()) {%>
        <option value="<%=StringUtils.prepareCDATA(c.code)%>" onselect="updatePostalPrompts('<%=("US".equalsIgnoreCase(c.code) ? "zip code" : "postal code") %>', '<%=StringUtils.prepareCDATA(StringUtils.prepareScript(c.postalRegex))%>', '<%=("US".equalsIgnoreCase(c.code) ? "Zip code" : "Postal code")%>:');" <%
        if(c.code.equalsIgnoreCase(country)) { %> selected="selected"<%
        }%>><%=StringUtils.prepareHTML(c.name)%></option><%
     }%></select>
     </div>
</div>
<div class="control-group">
    <label class="control-label">Billing Address:</label>
    <div class="controls">
        <input type="text" name="address1" placeholder="Enter your street address" title="Enter the street address of your credit card billing address" required="required" value="<%=StringUtils.prepareCDATA(address1)%>"/>
    </div>
</div>
<div class="control-group">
    <label class="control-label" id="postalCodeLabel">Billing Postal Code:</label>
    <div class="controls">
        <input type="text" name="postal" placeholder="Enter your postal code" required="required" value="<%=StringUtils.prepareCDATA(postal)%>" title="Enter the postal code of your credit card billing address"/>
    </div>
</div>
           </div>
       </div>

                    <div class="form-actions text-center">
                        <button type="submit" class="btn btn-savechanges">Save changes</button>
                    </div>
                </form>
            </div>
	</div>
	<script type="text/javascript">
var postalEl;
function updatePostalPrompts(desc, pattern, label) {
	if(!postalEl)
		postalEl = $("replenish-form").postal;
    postalEl.placeholder = "Enter your " + desc; 
    postalEl.title = "Your " + desc; 
    postalEl.pattern = pattern;
    $("postalCodeLabel").set("html", label);
}
var creditCardDetailsFx;
window.addEvent("domready", function() {
   creditCardDetailsFx = new Fx.Reveal($("creditCardDetails"));
   Array.each($("replenish-form").replenishType, function(item) { if(item.checked) replenishTypeChecked(item);});
});
function actionUpdated(form) {
    var editcard;
    Array.each($(form).operation, function(item) {
        if(item.checked && undefined == editcard)
            editcard = ("add" != item.value);
    });
    form.replenishCardNum.disabled = editcard;
    form.replenishExpMonth.disabled = editcard;
    form.replenishExpYear.disabled = editcard;
    form.cvv.disabled = editcard;
    form.address1.disabled = editcard;
    form.postal.disabled = editcard;
    form.country.disabled = editcard;<%
    if(existing && pending) {%>
    Array.each(form.replenishType, function(item) { 
    	if(item.value != <%=replenishType%>)
    		item.disabled = editcard;
    	else if(editcard && !item.checked) {
            item.checked = true;    
    		replenishTypeChecked(item);
    	}
    });<%}%>   
    if(!editcard)
    	creditCardDetailsFx.reveal();
    else
    	creditCardDetailsFx.dissolve();
}
function replenishTypeChecked(chk) {
	if(chk.value == 0) {
		chk.form.amount.disabled = true;
		chk.form.threshhold.disabled = true;
		Array.each(chk.form.operation, function(item) { item.disabled = true; });
		creditCardDetailsFx.dissolve();        
	} else if(chk.value == 2) {
		chk.form.amount.disabled = false;
        chk.form.threshhold.disabled = true;
        Array.each(chk.form.operation, function(item) { item.disabled = false; });
        actionUpdated(chk.form);
	} else {
        chk.form.amount.disabled = false;
        chk.form.threshhold.disabled = false;
        Array.each(chk.form.operation, function(item) { item.disabled = false; });
        actionUpdated(chk.form);
    }
}
</script>
</div>
<form name="nickNameForm" action="edit_nickname.html" method="post" class="form-inline">
	<input type="hidden" name="fromConsumerAcctTypeId" value="3"/>
    <input type="hidden" name="cardId" value="<%=cardId%>"/>
    <strong>Card Nickname:</strong> <input type="text" maxLength="60" value="<%=StringUtils.prepareCDATA(currentNickName) %>" title="Enter nickname for the card" name="nickname"> <button type="submit" class="btn btn-savechanges">Update Nickname</button>
</form>
<form class="form-inline" method="post" action="add_promo.html" name="addPromoForm">
<input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
<input type="hidden" name="cardTypeId" value="2"/>
<strong>Promotion Code:</strong>
<input type="text" title="The promotion code. Usually letters and numbers less than or equal to 20 characters" value="" autocomplete="off" pattern="^[a-zA-Z0-9]{1,20}$" required="required" maxlength="20" placeholder="Enter the promotion code" name="promoCode">
<button class="btn btn-savechanges" type="submit">Add Promotion</button>
</form>
<%
} %>
</div>
<div class="replenish-transactions">
<form name="replenishmentsForm" class="form-inline"><%if(cardId != null) {%>
    <input type="hidden" name="cardId" value="<%=cardId%>"/><%} %>
<strong>Replenishments</strong>
        <div class="date-range">
            Date range: <input type="text" data-toggle="calendar" data-format="mm/dd/yyyy" name="startTime" title="Enter the start date" value="<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(startTime, dateInputFormat)) %>"/>
                     to <input type="text" data-toggle="calendar" data-format="mm/dd/yyyy" name="endTime" title="Enter the end date" value="<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(endTime, dateInputFormat)) %>"/>
        </div>
        </form>
    <table class="table table-striped table-bordered-squarecorners table-hover account-table" id="accountTable">
        <thead>
            <tr>
                <th class="th-date"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Date" class="btn-link">Date</a></th>
                <%if(cardId == null) { %><th class="th-card"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Card Replenished" class="btn-link">Card Replenished</a></th><%} %>                
                <th class="th-amt"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by $ Amount" class="btn-link">$ Amount</a></th>
                <th class="th-cardcharged"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Credit Card Charged" class="btn-link">Card Charged</a></th>
            </tr>
        </thead><tbody><%
while(replenishResults.next()) {
	String cc = replenishResults.getFormattedValue("currencyCd");
    currencyFormat = (StringUtils.isBlank(cc) ? "CURRENCY" : "CURRENCY:" + cc);
    Object date = replenishResults.getValue("date");
    String type = replenishResults.getFormattedValue("type").toLowerCase().replaceAll("\\W+", "-");
    BigDecimal amount= replenishResults.getValue("amount", BigDecimal.class);
    %>
            <tr class="tran-<%=type%>-row">
                <td data-sort-value="<%=replenishResults.getValue("date", Long.class) %>"><%
                if(type.startsWith("pending-")){ 
                %><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" data-trigger="hover&amp;click" data-content="" data-title="Submitted <%=StringUtils.prepareHTML(ConvertUtils.formatObject(date, dateFormats[0])).replace("&", "&amp;") %>">Pending</a>
                <%} else { %><%=StringUtils.prepareHTML(ConvertUtils.formatObject(date, dateFormats[0])) %>
                <%} %>
                </td>
                <%if(cardId == null) { %><td data-sort-value="<%=StringUtils.prepareCDATA(replenishResults.getFormattedValue("replenishedCard")) %>"><%=StringUtils.prepareHTML(replenishResults.getFormattedValue("replenishedCard")) %></td><%} %>
                <%if(type.startsWith("failed-")) {%>
                <td data-sort-value="">Failed</td><%
                } else {%>
                <td data-sort-value="<%=SystemUtils.nvl(amount, BigDecimal.ZERO) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(amount, numberFormat)) %></td><%} %>
                <td data-sort-value="<%=StringUtils.prepareCDATA(replenishResults.getFormattedValue("chargedCard")) %>"><%=StringUtils.prepareHTML(replenishResults.getFormattedValue("chargedCard"))%></td>
            </tr><%
}
if(replenishResults.getRowCount() == 0) {
%><tr class="no-rows-found"><td colspan="4">No replenishments have occurred recently</td></tr></tbody><%
} else if(replenishResults.getRowCount() == maxReplenishments) {
%></tbody><tfoot><tr class="rows-limited"><td colspan="4">Showing only the <%=maxReplenishments %> most recent</td></tr></tfoot><%
} else {%></tbody><%} %>
    </table>
    </div>
    <div class="span8"><hr/>
            <h5>Credit Cards on File </h5><%
for (CardOnFile key : cardMap.keySet()) {
    %><div class="more-accordion"><span class="cc-number"><%=key.getReplenishCardNum() %><%
    String cc = key.getCurrencyCd();
    long replenConsumerAcctId=key.getReplenConsumerAcctId();
    String active=cardMap.get(key).size()==0?" not used for replenishment":"used for replenishment";
    		 %> (<%=StringUtils.prepareHTML(cc) %>) <%=active %></span>
    <button id="ccDelete<%=replenConsumerAcctId%>" class="btn btn-savechanges" type="button" onclick="onDeleteCC(<%=replenConsumerAcctId%>)">Delete</button>
    <div id="confirmCCDelete<%=replenConsumerAcctId%>"></div>
   </div><%
}
if(cardMap.size()==0) {
    %><div>No credit cards on file</div><%
}%></div>
<%if(cardMap.size()>0) { %>
<div class="span8">
            <h5>Replenishment Cards Map</h5>
<table id="accountTable" class="table table-striped table-bordered-squarecorners table-hover account-table">
<thead><tr><th><a data-toggle="sort" data-sort-type="STRING" title="Sort by MORE Card" class="btn-link">More Card</a></th>
    <th><a data-toggle="sort" data-sort-type="STRING" title="Sort by Credit Card" class="btn-link">Replenishment Credit Card</a></th></tr></thead>
<tbody>
<%for (CardOnFile key : cardMap.keySet()) { 
	ArrayList<String> prepaidCardList=cardMap.get(key);
	for(String prepaidCard:prepaidCardList)	{
		%>
		<tr><td data-sort-value="<%=StringUtils.prepareCDATA(prepaidCard) %>"><%=StringUtils.prepareHTML(prepaidCard) %></td><td data-sort-value="<%=StringUtils.prepareCDATA(key.getReplenishCardNum()) %>"><%=StringUtils.prepareHTML(key.getReplenishCardNum())%></td></tr>
		<% 
	}
} %></tbody>
</table>
</div>
<%} %>
<script type="text/javascript">
window.addEvent("domready", function() {
    [$(document.replenishmentsForm.startTime), $(document.replenishmentsForm.endTime)].each(function(input) {
    	if(input.pikaday)
	    	input.addEvent("change", function(evt) { 
	    		if(input.pikaday._d != input.originalDate)
	    			input.form.submit();
	    	});
    	input.addEvent("keypress", function(evt) { 
    		if(evt.code == 13) {
    	        evt.preventDefault();
    	        if(evt.target) {
    	            evt.target.blur();
    	        }
    	    }
    	});
    });
});
function onDeleteCC(replenConsumerAcctId){
	$("ccDelete"+replenConsumerAcctId).style.display="none";
	$("confirmCCDelete"+replenConsumerAcctId).set("html", '<h4>Are you sure you want to delete the credit card?</h4> <button  class="btn btn-savechanges" type="button" onclick="onDeleteCCYes('+replenConsumerAcctId+')">Yes</button>  <button  class="btn btn-savechanges" type="button" onclick="onDeleteCCNo('+replenConsumerAcctId+')">No</button>');
}

function onDeleteCCNo(replenConsumerAcctId){
	$("ccDelete"+replenConsumerAcctId).style.display="inline";
	$("confirmCCDelete"+replenConsumerAcctId).set("html","");
}

function onDeleteCCYes(replenConsumerAcctId){
	window.location.href = "delete_cc.html?replenConsumerAcctId="+replenConsumerAcctId;
}

function onAddConsumerAcctTypeChange(consumerAcctTypeId){
	if(consumerAcctTypeId==3){//more prepaid
		$("addMorePrepaidInput").set("style","display:block");
		$("addSproutInput").set("style","display:none");
		$("morePrepaidCard").set("required","required");
		$("morePrepaidSecurityCode").set("required","required");
		$("sproutCard").removeAttribute("required");
		$("sproutSecurityCode").removeAttribute("required");
		$("sproutCard").set("value","");
		$("sproutSecurityCode").set("value","");
	}else{
		$("addMorePrepaidInput").set("style","display:none");
		$("addSproutInput").set("style","display:block");
		$("morePrepaidCard").removeAttribute("required");
		$("morePrepaidSecurityCode").removeAttribute("required");
		$("sproutCard").set("required","required");
		$("sproutSecurityCode").set("required","required");
		$("morePrepaidCard").set("value","");
		$("morePrepaidSecurityCode").set("value","");
	}
}
</script>
<jsp:include page="/account_footer.jsp" />