<%@page import="com.usatech.ec2.EC2CardInfo"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.lang.Holder"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="java.sql.SQLException,simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);

String passSerialNumber = RequestUtils.getAttribute(request, "uuid", String.class, false);
BigDecimal amount = RequestUtils.getAttribute(request, "amount", BigDecimal.class, false);

if (!StringUtils.isBlank(passSerialNumber)){
	Map<String,Object> params = new HashMap<String,Object>();
    params.put("passSerialNumber", passSerialNumber);
    Results consumerResults = DataLayerMgr.executeQuery("GET_CONSUMER_INFO_BY_PASS_SN", params);
    if (consumerResults.next()){
    	String email = (String)consumerResults.get("email");
    	String mobile = (String)consumerResults.get("mobile");
    	if (!user.getUserName().equals(email) && !user.getUserName().equals(mobile)){
    		RequestUtils.redirectWithCarryOver(request, response, "dashboard.html", false, true);
        	return;	
    	}
    }else{
    	RequestUtils.redirectWithCarryOver(request, response, "dashboard.html", false, true);
    	return;
    }
	
	PrepaidUtils.addCardByPass(passSerialNumber, user.getConsumerId());
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-registered-prompt-existing-account-by-pass", "Your account username and password already exist in our system. Prepaid card has been linked to your account. Please complete replenishment setup for this card, if needed.");
	if (amount == null){
	RequestUtils.redirectWithCarryOver(request, response, "cards.html", false, true);
	}
	else
	{
		params.put("passSerialNumber", passSerialNumber);
	    Results results = DataLayerMgr.executeQuery("GET_GLOBAL_ACCT_ID_BY_PASS_SN", params);
		if (results.next()) {
	    	String replenRedirect = "cards.html?cardId=" + results.get("consumerAccountId");
			RequestUtils.redirectWithCarryOver(request, response, replenRedirect, false, true);
		}
	}
	return;
}

Integer addConsumerAcctTypeId = RequestUtils.getAttribute(request, "addConsumerAcctTypeId", Integer.class, false);
if(addConsumerAcctTypeId==null){
	addConsumerAcctTypeId=3;
}
String card = null;
String securityCode = null;
if(addConsumerAcctTypeId==3){
	card = RequestUtils.getAttribute(request, "card", String.class, false);
	securityCode = RequestUtils.getAttribute(request, "securityCode", String.class, false);
}else{
	card = RequestUtils.getAttribute(request, "sproutCard", String.class, false);
	securityCode = RequestUtils.getAttribute(request, "sproutSecurityCode", String.class, false);
}
boolean fromSignup=ConvertUtils.getBoolean(RequestUtils.getAttribute(request, "fromSignup", Boolean.class, false), false);
if(StringUtils.isBlank(card) || StringUtils.isBlank(securityCode)) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
	RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=new", false, true);
    return;
} 
Holder<String> cardNumHolder = new Holder<String>();
if(addConsumerAcctTypeId==3){
	if(!PrepaidUtils.checkCardNum(card, cardNumHolder, true)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-prepaid-card", "You entered an invalid prepaid card number. Please enter a valid one.");
	    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=new", false, true);
	    return;
	}
}
long globalAccountId;
Long sproutAccountId=null;
if(addConsumerAcctTypeId==3){//for adding more prepaid
	try {
	    globalAccountId = EC2ClientUtils.getGlobalAccountId(cardNumHolder.getValue());
	} catch(Throwable e) {
	    Log.getLog().warn("Could not get globalAccountId", e);
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
	    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=new", false, true);
	    return;
	}
}else{//for adding sprout
	String entryType = "N";
	String attributes =null;
	EC2CardInfo infoResp = EC2ClientUtils.getEportConnect().getCardInfoPlain(null,null, EC2ClientUtils.getVirtualDeviceSerialCd()+"-USD", card+"|"+securityCode, entryType, attributes);
	if(infoResp == null) {
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-auth-failed", "Sorry for the inconvenience. We could not process your sprout card registration at this time. Please try again.");
		RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=new", false, true);
	    return;
	}
	String masked = PrepaidUtils.maskCardNumber(card);
	switch(infoResp.getReturnCode()) {
		case 1: //APPROVED
	    case 2: //APPROVED
	    	break;
	    case 3:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-info-declined", "Your sprout card {0} was declined. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=new", false, true);
	    	return;
	    case 0:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-info-failed", "Your sprout card {0} was declined. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=new", false, true);
	    	return;
	    default:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-info-failed", "Sorry for the inconvenience. We could not process your sprout card {0} at this time. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=new", false, true);
	    	return;
	}
	
	globalAccountId=infoResp.getCardId();
	String attributesResp=infoResp.getAttributes();
	if(attributesResp!=null){
		String[] attArray=attributesResp.split("=");
		if(attArray!=null&&attArray.length==2){
			sproutAccountId=ConvertUtils.getLongSafely(attArray[1], -1);
		}
	}
	if(sproutAccountId==null||sproutAccountId<-1){
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-info-failed", "Sorry for the inconvenience. We could not process your sprout card {0} at this time. Please try again.", masked);
		RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=new", false, true);
    	return;
	}
}
try {
	String nickname = RequestUtils.getAttribute(request, "nickname", String.class, false);
	if(addConsumerAcctTypeId==3){
		PrepaidUtils.addCard(user.getConsumerId(), globalAccountId, securityCode, nickname);	
	}else{
		PrepaidUtils.addSproutCard(user.getConsumerId(), globalAccountId, securityCode, nickname, sproutAccountId);	
	}
} catch(SQLException e) {
   	switch(e.getErrorCode()) {
   		case 20300: case 20305:
            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it");
            break;
        case 20301:
   			RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-already-registered", "You have already registered this card. Please enter a different card");
            break;
        case 20302:
            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-registered-to-other", "This card has been registered under a different email. Please contact Customer Service for assistance");
            break;
        case 20304:
            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-active", "Your account is not active. Please contact Customer Service for assistance");
            break;
        case 1:
        	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-nickname-unique", "The nickname needs to be unique. Please try with a different nickname.");
        	break;
        default:
           	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
   	}
   	RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=new", false, true);
   	return;
}
if(fromSignup){
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-registered-prompt-existing-account", "Your account username and password already exist in our system. This prepaid card ({0}) has been linked to your account. Please complete replenishment setup for this card, if needed.", PrepaidUtils.maskCardNumber(cardNumHolder.getValue()));
	if (amount == null){
		RequestUtils.redirectWithCarryOver(request, response, "cards.html", false, true);
		}
		else
		{
			Map<String,Object> params = new HashMap<String,Object>();
		    params.put("globalAccountId", globalAccountId);
		    Results results = DataLayerMgr.executeQuery("GET_ACCT_ID_BY_GLOBAL_ID", params);
			if (results.next()) {
		    	String replenRedirect = "cards.html?cardId=" + results.get("consumerAcctId");
				RequestUtils.redirectWithCarryOver(request, response, replenRedirect, false, true);
			}
		}
}else{
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-registered-prompt", "This prepaid card ({0}) has been linked to your account.", PrepaidUtils.maskCardNumber(cardNumHolder.getValue()));
}
RequestUtils.redirectWithCarryOver(request, response, "cards.html");

%>