<%@page import="java.util.List"%>
<%@page import="java.util.Locale"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.xml.sax.MessagesInputSource,simple.servlet.RequestUtils,simple.text.StringUtils,java.util.Locale,simple.translator.Translator,com.usatech.prepaid.web.PrepaidUser,simple.servlet.SimpleServlet,java.util.List"%>
<%
Results carrierResults = DataLayerMgr.executeQuery("GET_SMS_GATEWAYS",null);
int preferredCommType=2;
%>
<html>
<head>
		<meta charset="<%=StringUtils.prepareCDATA(response.getCharacterEncoding())%>">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Cache-Control" content="no-cache" />
		<meta http-equiv="Pragma" content="no-cache" />
	    <title>Add MORE to your mobile wallet</title>
        <base href="<%=StringUtils.encodeForHTMLAttribute(RequestUtils.getBaseUrl(request)) %>" />
        <link  rel='stylesheet' href='/democss/fonts.css'>
        <link href="<%=ToJspSimpleServlet.untranslatePath(request, "/extra_style._")%>" type="text/css" rel="stylesheet"/>
        <%
String[] stylesheets = StringUtils.split(RequestUtils.getAttribute(request, "extra-stylesheets", String.class, false), StringUtils.STANDARD_DELIMS, false);
if(stylesheets != null)
	for(String ss : stylesheets) {
	    if(!StringUtils.isBlank(ss)) {
	        %><link href="<%=StringUtils.prepareCDATA(RequestUtils.addLastModifiedToUri(request, null, ss.trim())) %>" type="text/css" rel="stylesheet"/><%
	    }
	}
boolean wrapA = Boolean.TRUE.equals(RequestUtils.getAttribute(request, "wrapNarrow", Boolean.class, false)); 
boolean jstimings = Boolean.TRUE.equals(RequestUtils.getAttribute(request, "_jstimings", Boolean.class, false)); 
if(jstimings) {%><script type="text/javascript">var timings = []; 
timings[timings.length] = +(new Date);</script><%}
%>
		<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/modernizr-2.6.2-custom-46341.min.js")%>"></script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date);</script><%}%>
		<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/mootools-core-1.4.5-full-nocompat.js")%>"></script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date);</script><%}%>
		<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/mootools-more-1.4.0.1-selected.js")%>"></script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date);</script><%}%>
        <script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/mootools-bootstrap-plus.js")%>"></script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date);</script><%}%>
        <script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/apply.js")%>"></script>        
        <script type="text/javascript"><%
Locale locale = RequestUtils.getLocale(request);
if(locale != null) {%>          Locale.use("<%=StringUtils.prepareCDATA(StringUtils.prepareScript(locale.toString()))%>");<%}
//GenerateUtils.writeCountriesConfigScript(translator, out, "\t\t\t", SystemUtils.getNewLine());
%>
if(top.frames.length != 0)
    top.location = self.document.location;
</script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date); 
var s = ""; 
for(var i = 0; i < timings.length - 1; i++) { 
	s += "From #" + i + " to #" + (i+1) + ": " + (timings[i+1] - timings[i]) + "\n"; 
}
alert(s);
</script><%}%>
<style type="text/css">
* {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  font-weight: 300;
}
body {
  font-family: 'Source Sans Pro', sans-serif;
  color: #222222;
  font-weight: 300;
  background:  #ffffff;
}
body ::-webkit-input-placeholder {
  /* WebKit browsers */
  font-family: 'Source Sans Pro', sans-serif;
  color: #222222;
  font-weight: 300;
}
body :-moz-placeholder {
  /* Mozilla Firefox 4 to 18 */
  font-family: 'Source Sans Pro', sans-serif;
  color: #222222;
  opacity: 1;
  font-weight: 300;
}
body ::-moz-placeholder {
  /* Mozilla Firefox 19+ */
  font-family: 'Source Sans Pro', sans-serif;
  color: #222222;
  opacity: 1;
  font-weight: 300;
}
body :-ms-input-placeholder {
  /* Internet Explorer 10+ */
  font-family: 'Source Sans Pro', sans-serif;
  color: #222222;
  font-weight: 300;
}


img{margin: 10px auto; z-index: 9999;}

.wrapper {
  background: #ffffff;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  margin-top: 0;
  overflow: hidden;
}

.container {
  max-width: 800px;
  margin: 0 auto;
  padding: 50px 5%;
  /* height: 500px; */
  text-align: center;
}

.container h1 {
  font-size: 40px;
  -webkit-transition-duration: 1s;
          transition-duration: 1s;
  -webkit-transition-timing-function: ease-in-put;
          transition-timing-function: ease-in-put;
  font-weight: 200;
}

.thank-you {display: none; font-size: 120%;}
.thank-you.form-success{display: block; transform: translateY(85px);}
.controls {margin: 5px 0 20px 0;}

form {
  padding: 10px 0;
  position: relative;
  z-index: 2;
}

form input {
  -webkit-appearance: none;
     -moz-appearance: none;
          appearance: none;
  outline: 0;
  border: 1px solid rgba(100,100,100, 0.4);
  background-color: #f5f5f5;
   width:  100%;
  max-width: 425px;
  border-radius: 3px;
  padding: 10px 15px;
  margin: 0 auto 10px auto;
  display: block;
  text-align: center;
  color: #222222;
  -webkit-transition-duration: 0.25s;
          transition-duration: 0.25s;
  font-weight: 300;
}
form input:hover {
  background-color: #ffffff;
}
form input:focus {
  background-color: #ffffff;
  border-left: solid 6px #588fcc;
  color: ##666666;
}

form select{
  -webkit-appearance: none;
     -moz-appearance: none;
          appearance: none;
  outline: 0;
  border: 1px solid rgba(100,100,100, 0.4);
  background: url(/images/downarrow-gray.png) no-repeat right #f5f5f5;
  width:  100%;
  max-width: 425px;
  border-radius: 8px;
  padding: 10px 15px;
  margin: 0 auto 10px auto;
  display: block;
  text-align: center;
  color: #222222;
  -webkit-transition-duration: 0.25s;
          transition-duration: 0.25s;
  font-weight: 300;
}

form select:hover {
  background-color: #ffffff;
}
form select:focus {
  background-color: #ffffff;
  color: ##666666;
}

form button {
  -webkit-appearance: none;
     -moz-appearance: none;
          appearance: none;
  outline: 0;
  background-color: #588fcc ;
  border: 0;
  padding: 10px 15px;
  color: #ffffff;
  border-radius: 3px;
   width:  100%;
  max-width: 425px;
  cursor: pointer;
  -webkit-transition-duration: 0.25s;
  transition-duration: 0.25s;
}
form button:hover {
  background-color: #79ace4;
}

  input[type="radio"] { 
  outline: 0;
   display:  inline-block;
    width: 20px;
    height: 20px;
    vertical-align: middle;
    margin: 0 0 0 10px;
    cursor: pointer;
}


.bg-bubbles {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1;
}
.bg-bubbles li {
  position: absolute;
  opacity:  0.3;
  list-style: none;
  display: block;
  width: 40px;
  height: 40px;
  background: url('/images/m.svg') bottom left no-repeat;
  bottom: -160px;
  -webkit-animation: square 25s infinite;
  animation: square 25s infinite;
  -webkit-transition-timing-function: linear;
  transition-timing-function: linear;
}
.bg-bubbles li:nth-child(1) {
  left: 10%;
}
.bg-bubbles li:nth-child(2) {
  left: 20%;
  width: 80px;
  height: 80px;
  -webkit-animation-delay: 2s;
          animation-delay: 2s;
  -webkit-animation-duration: 17s;
          animation-duration: 17s;
}
.bg-bubbles li:nth-child(3) {
  left: 25%;
  -webkit-animation-delay: 4s;
          animation-delay: 4s;
}
.bg-bubbles li:nth-child(4) {
  left: 40%;
  width: 60px;
  height: 60px;
  -webkit-animation-duration: 22s;
          animation-duration: 22s;
    background: url('/images/m.svg') bottom left no-repeat;
}
.bg-bubbles li:nth-child(5) {
  left: 70%;
}
.bg-bubbles li:nth-child(6) {
    opacity:  .4;
  left: 80%;
  width: 120px;
  height: 120px;
  -webkit-animation-delay: 3s;
          animation-delay: 3s;
    background: url('/images/m.svg') bottom left no-repeat;
}
.bg-bubbles li:nth-child(7) {
  left: 32%;
  width: 160px;
  height: 160px;
  -webkit-animation-delay: 7s;
          animation-delay: 7s;
}
.bg-bubbles li:nth-child(8) {
  left: 55%;
  width: 20px;
  height: 20px;
  -webkit-animation-delay: 15s;
          animation-delay: 15s;
  -webkit-animation-duration: 40s;
          animation-duration: 40s;
}
.bg-bubbles li:nth-child(9) {
  left: 25%;
  width: 35px;
  height: 35px;
  -webkit-animation-delay: 2s;
          animation-delay: 2s;
  -webkit-animation-duration: 40s;
          animation-duration: 40s;
    background: url('/images/m.svg') bottom left no-repeat;
}
.bg-bubbles li:nth-child(10) {
  left: 90%;
  width: 160px;
  height: 160px;
  -webkit-animation-delay: 11s;
          animation-delay: 11s;
}
@-webkit-keyframes square {
  0% {
    -webkit-transform: translateY(0);
            transform: translateY(0);
  }
  100% {
    -webkit-transform: translateY(-1500px) rotate(600deg);
            transform: translateY(-1500px) rotate(600deg);
  }
}
@keyframes square {
  0% {
    -webkit-transform: translateY(0);
            transform: translateY(0);
  }
  100% {
    -webkit-transform: translateY(-1500px) rotate(600deg);
            transform: translateY(-1500px) rotate(600deg);
  }
}

/* =================================================
   ALERT STYLES
   ================================================= */
  
   
.alert {
  width:  94%;
  max-width: 500px;
  padding: 10px;
  margin: 0 auto 0px;
  border: none;
  font-size:  24px;
  text-align: center;
  font-weight:  500;
  -webkit-border-radius: 8px;
  -moz-border-radius: 8px;
  border-radius: 8px;
  position: absolute;
 top: 80px;
  left: 0;
  right: 0;
  z-index: 99;
}

.alert-success {
 background-color: #00cc00;
 color:  #ffffff;
  animation: pulse 2s;
}

.alert-error {
  background-color: #cc0000;
  color:  #ffffff;
  animation: pulse 2s;
}

@keyframes pulse {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}

@import url("http://fonts.googleapis.com/css?family=Open+Sans:400,600,700");
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
html, body {
  height: 100%;
}

body {
  font: 16px/1 'Open Sans', sans-serif;
  color: #222222;
  background: #ffffff;
}

/* =================================================
   For tab
   ================================================= */

#topnavtabsdiv {
  margin:  0 auto;
}
/* Style the list */
ul.topnavtabs {
    list-style-type: none;
    padding: 0 5%;
    overflow: hidden;
}

/* Float the list items side by side */
ul.topnavtabs li {float: left;
border-bottom:  solid 1px #ddd;
}

/* Style the links inside the list items */
ul.topnavtabs li a {
     text-align: center;
    text-decoration: none;
    transition: 0.3s;
      display: inline-block;
  margin: 0;
  padding: 15px 25px;
  font-weight: 600;
  text-align: center;
  color: #888;
  border:  solid 1px transparent;
  border-top: solid 6px transparent;
}

/* Change background color of links on hover */
ul.topnavtabs li a:hover {
  color: #888;
  cursor: pointer;
}

/* Create an active/current tablink class */
ul.topnavtabs li a:focus, .active {
  background-color: #fff;
  color: #555;
  border: 1px solid #ddd;
  border-top: solid 6px #588fcc;
  border-bottom:  solid 1px #fff;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 0;
    height:  100%;
}
p, form input, form button  {
  font-size: 18px;
  line-height: 1.3em;
}

@media (min-width: 1200px) {
  p, form input , form button {
    font-size: 24px;
  }
}

form select  {
  font-size: 16px;
  line-height: 1.3em;
}

@media (min-width: 1200px) {
  form select {
    font-size: 22px;
  }
}

</style>
    </head>
<body>
<div id="topnavtabsdiv">
<form id="navtabsform" style="padding:0px;">
<ul class="topnavtabs">
  <li style="border-bottom:  none;"><a href="javascript:void(0)" style="background-color: #fff;
  color: #555;
  border: 1px solid #ddd;
  border-top: solid 6px #588fcc;
  border-bottom:  none;" onclick="openTab(1)">Add MORE card to mobile wallet</a></li>
  <li><a href="javascript:void(0)" onclick="openTab(2)">MORE account demo</a></li>
  <li><a href="javascript:void(0)" onclick="openTab(3)">Demo</a></li>
</ul>
</form>
</div>

<div class="wrapper">
<%
List<MessagesInputSource.Message> messages = RequestUtils.getMessages(request);
if(!messages.isEmpty()) {%>
            <br/><%
    for(MessagesInputSource.Message m : messages) {
        %><div id="alertMessage" class="alert alert-<%=StringUtils.prepareCDATA(m.getType())%>"><%=m.toHtml()%></div><%
    }%><%
}
%>
    <div class="container">

        <img src="/images/more-card.svg" style="width: 100%; max-width: 425px;"><br>
        <img src="/images/Add_to_Apple_Wallet_rgb_US-UK.svg" style="width: 100%; max-width: 175px;" alt="add to Apple Wallet">
        <form name="passApiForm" action="send_to_pass_demo.html" method="post" class="form">
        <p>Choose your preferred communication method:</p>

        <p class="controls">
             <label class="radio inline">
                  <input type="radio" style="outline: 0; display:  inline-block; width: 20px; height: 20px; vertical-align: middle; margin: 0 0 0 10px; cursor: pointer; -webkit-appearance: radio;  -moz-appearance: radio; appearance: radio;" name="preferredCommType" value="2" onclick="preferredCommTypeChecked(this)" checked="checked"> Text (SMS)
              </label>
              <label class="radio inline">
                  <input type="radio" style="outline: 0; display:  inline-block; width: 20px; height: 20px; vertical-align: middle; margin: 0 0 0 10px; cursor: pointer; -webkit-appearance: radio;  -moz-appearance: radio; appearance: radio;" name="preferredCommType" value="1" onclick="preferredCommTypeChecked(this)"> Email
               </label>
        </p>
        <div id="optional_marker_mobile" >
            <input type="text" placeholder="Enter full phone number" name="mobile" pattern="\(?\s*\d{3}\s*\)?\s*[-.]?\s*\d{3}\s*[-.]?\s*\d{4}" <%if(preferredCommType == 2 ) {%>required="required" <%}%>>
             <select name="carrierId" title="Select your mobile phone carrier" <%if(preferredCommType == 2) {%>required="required" <%}%>>
                    <option value="">Select your cell provider</option><%
while(carrierResults.next()) {
	Integer value = carrierResults.getValue("carrierId", Integer.class);
	%><option value="<%=value %>"><%=StringUtils.prepareHTML(carrierResults.getFormattedValue("carrierName")) %></option><%
}%>                    
                </select>
            <button type="submit" id="send-text-button">Send text message</button>
        </div>
        <div id="optional_marker_email" style="display:none">
            <input type="text" placeholder="Enter email address" name="email" <%if(preferredCommType == 1) {%>required="required" <%}%>>
            <button type="submit" id="send-email-button">Send email</button>
        </div>
        </form>
        <p><br/>Coming Soon:</p>
        <img src="/images/save_to_android_pay_light.png" style="width: 100%; max-width: 220px;"><br>
        <p style="font-size:80%;">
        <span class="more-text">more.</span> v.1.16.0D &mdash; A product of USA Technologies, Inc.
        <br>&copy; Copyright 2003-2017
    </p>
    </div>

    <ul class="bg-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>

</div>
<script type="text/javascript" defer="defer">
function preferredCommTypeChecked(chk) {
    if (chk.value == 1) {
    	chk.form.email.required = true;
        chk.form.mobile.required = false;
        chk.form.carrierId.required = false; 
       $("optional_marker_email").set("style", "display:block");
       $("optional_marker_mobile").set("style", "display:none");
        
    } else {
    	
       chk.form.email.required = false;
       chk.form.mobile.required = true;
       chk.form.carrierId.required = true; 
       $("optional_marker_email").set("style", "display:none");
       $("optional_marker_mobile").set("style", "display:block");
    }
}

function clearAlert(){
	if($("alertMessage")){
		var alertMsg=$("alertMessage").get("html");
		if(alertMsg.length>0){
			$("alertMessage").set("html","");
			$("alertMessage").set("class","");
		}
	}
	setTimeout(clearAlert, 15000);
}
setTimeout(clearAlert, 15000);
function openTab(pageNum){
	if(pageNum==2){
		window.location.href="walletDemo2.html";
	}else if(pageNum==3){
		window.location.href="walletDemo3.html";
	}
}
</script>
</body>
</html>
