<%@page import="java.util.Map"%>
<%@page import="simple.param.ParamEditor.OptionsParamEditor"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Locale"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.param.ParamEditor"%>
<%@page import="simple.param.html.DefaultParamRenderer"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.param.html.DefaultAddressRenderer"%>
<%@page import="simple.servlet.RequestUtils"%>
<%!
DefaultAddressRenderer addressRenderer = new DefaultAddressRenderer();
%>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
if(user == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-logged-in", "You are no longer logged in. Please log in again.");
    RequestUtils.redirectWithCarryOver(request, response, "signin.html");
    return;
}
Long cardId = RequestUtils.getAttribute(request, "cardId", Long.class, false);
if(cardId != null && PrepaidUtils.checkCardPermission(user, cardId) == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
    RequestUtils.redirectWithCarryOver(request, response, PrepaidUtils.getHomePage());
    return;
}
Locale locale = RequestUtils.getLocale(request);
String username = user.getUserName();
String firstname = RequestUtils.getAttribute(request, "firstname", String.class, false);
String lastname = RequestUtils.getAttribute(request, "lastname", String.class, false);
String address1 = RequestUtils.getAttribute(request, "address1", String.class, false);
String country = RequestUtils.getAttribute(request, "country", String.class, false);
String postal = RequestUtils.getAttribute(request, "postal", String.class, false);
Integer preferredCommType=1;
String email="";
String mobile="";
Integer carrierId =null;
Results results = DataLayerMgr.executeQuery("GET_USER_INFO", user);
results.setLocale(locale);
if(results.next()) {
    if(StringUtils.isBlank(firstname))
        firstname = results.getValue("firstname", String.class);
    if(StringUtils.isBlank(lastname))
        lastname = results.getValue("lastname", String.class);
    if(StringUtils.isBlank(address1))
        address1 = results.getValue("address1", String.class);
    if(StringUtils.isBlank(postal))
    	postal = results.getValue("postal", String.class);
    if(StringUtils.isBlank(country))
    	country = results.getValue("country", String.class);
    preferredCommType = results.getValue("preferredCommType", Integer.class);
    email = results.getValue("email", String.class);
    mobile = results.getValue("mobile", String.class);
    carrierId = results.getValue("carrierId", Integer.class);
    if(email==null){email="";};
    if(mobile==null){mobile="";};
} else {
    //user was deleted?
}
RequestUtils.setAttribute(request, "tab", "settings", "request"); 
Results carrierResults = DataLayerMgr.executeQuery("GET_SMS_GATEWAYS",null);
Results regionResults = DataLayerMgr.executeQuery("GET_REGIONS_FOR_CONSUMER", user);
%>
<jsp:include page="/account_header.jsp" />
<form id="profile-form" class="profile-form" action="update_user.html" method="post" onSubmit="onSaveChanges();">
<div class="account-settings-wrap">
    <div class="row-fluid account-settings">
        <div class="span4">
            <h5>Account Info/Profile</h5>
                <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
                <input type="hidden" name="oldUsername" value="<%=StringUtils.prepareCDATA(user.getUserName())%>"/>
                <input type="hidden" name="accountType" <%if(StringUtils.isBlank(username)){%> value="simple" <%}else{ %>value="full"<%} %>/>
                <% if(StringUtils.isBlank(username)){%>
                <label class="checkbox">
                    <input type="checkbox" id="upgradeToFull" name="upgradeToFull" value="true" title="Upgrade to full account" onclick="onUpgradeToFull();"/> Upgrade to full account
				</label>
				<%} %>
                <div id="fullForm1" <% if(StringUtils.isBlank(username)){%> style="display:none" <%} %>>
                <div class="control-group">
                    <label class="control-label">Preferred Communication Method:</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" name="preferredCommType" value="1" onclick="preferredCommTypeChecked(this)"<%if(preferredCommType == 1) {%> checked="checked"<%}%> /> Email
                        </label>
                        <label class="radio inline">
                            <input type="radio" name="preferredCommType" value="2" onclick="preferredCommTypeChecked(this)"<%if(preferredCommType == 2) {%> checked="checked"<%}%> /> SMS (Text)
                        </label> 
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputEmail">Email: <span id="optional_marker_email" class="<%=(preferredCommType == 1 ? "hidden" : "optional") %>">Optional</span></label>
                    <div class="controls">
                        <input type="email" name="email" placeholder="Enter your email address" title="Your email address" <%if(preferredCommType == 1 && !StringUtils.isBlank(username)) {%>required="required" <%}%> value="<%=email%>"/>
                    </div>
                    <label class="control-label" for="inputMobile">Mobile:<span id="optional_marker_mobile" class="<%=(preferredCommType == 2 ? "hidden" : "optional") %>">Optional</span></label>
                    <div class="controls">
                        <input type="tel" name="mobile" placeholder="Enter your 10-digit mobile phone number" title="Your 10-digit mobile phone number" pattern="\(?\s*\d{3}\s*\)?\s*[-.]?\s*\d{3}\s*[-.]?\s*\d{4}" <%if(preferredCommType == 2 && !StringUtils.isBlank(username)) {%>required="required" <%}%> value="<%=mobile%>"/>
                    </div>
                    
                    <label><span>Mobile Phone Carrier:</span><span id="optional_marker_carrierId" class="<%=(preferredCommType == 2 ? "hidden" : "optional") %>">Optional</span></label>
                <select name="carrierId" title="Select your mobile phone carrier" <%if(preferredCommType == 2) {%>required="required" <%}%>>
                    <option value="">-- Please select --</option><%
while(carrierResults.next()) {
	Integer value = carrierResults.getValue("carrierId", Integer.class);
	%><option value="<%=value %>"<%if (value != null && carrierId != null && value.intValue() == carrierId.intValue()) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(carrierResults.getFormattedValue("carrierName")) %></option><%
}%>                    
                </select>
                </div>
                </div> <!--  end of fullForm1-->
                <div class="control-group">
                    <label class="control-label">First Name:</label>
                    <div class="controls">
                        <input type="text" name="firstname" placeholder="Enter your first name" title="Your first name" required="required" value="<%=StringUtils.prepareCDATA(firstname)%>"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Last Name:</label>
                    <div class="controls">
                        <input type="text" name="lastname" placeholder="Enter your last name" title="Your last name" required="required" value="<%=StringUtils.prepareCDATA(lastname)%>"/>
                    </div>
                </div>
                <div id="fullForm2" <% if(StringUtils.isBlank(username)){%> style="display:none" <%} %>>
                <div class="control-group">
                    <label class="control-label">Country:</label>
                    <div class="controls">
	                <select name="country" title="Your country" <% if(!StringUtils.isBlank(username)) {%> required="required" <%} %>>
	                <% for(GenerateUtils.Country c : GenerateUtils.getCountries()) {%>
	                    <option value="<%=StringUtils.prepareCDATA(c.code)%>" onselect="updatePostalPrompts('<%=("US".equalsIgnoreCase(c.code) ? "5-digit zip code" : "postal code") %>', '<%=StringUtils.prepareCDATA(StringUtils.prepareScript(c.postalRegex))%>', '<%=("US".equalsIgnoreCase(c.code) ? "Primary Location Zip Code" : "Primary Location Postal Code")%>:');" <%
	                    if(c.code.equalsIgnoreCase(country)) { %> selected="selected"<%
	                    }%>><%=StringUtils.prepareHTML(c.name)%></option><%
	                 }%></select>
	                 </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Primary Location Street Address:</label>
                    <div class="controls">
                        <input type="text" name="address1" placeholder="Enter your street address" title="Your street address" <% if(!StringUtils.isBlank(username)) {%> required="required" <%} %> value="<%=StringUtils.prepareCDATA(address1)%>"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" id="postalCodeLabel">Primary Location Postal Code:</label>
                    <div class="controls">
                        <input type="text" name="postal" placeholder="Enter your postal code" <% if(!StringUtils.isBlank(username)) {%>required="required" <%} %> value="<%=StringUtils.prepareCDATA(postal)%>" title="Your postal code"/>
                    </div>
                </div>
                </div><!--  end of fullForm2-->
                <%if(!regionResults.isGroupEnding(0)) { 
                    Long selectedRegionId = RequestUtils.getAttribute(request, "region", Long.class, false);
                %>
                <div id="fullForm4">
                <div class="control-group">
                <label><span>Region</span></label>
                <select name="region" title="Please select the most appropriate option" required="required">
                    <option value="">--</option><%
                    while(regionResults.next()) {
                        long regionId = regionResults.getValue("regionId", Long.class);
                        boolean selected;
                        if(selectedRegionId == null)
                        	selected = regionResults.getValue("selected", Boolean.class);
                        else 
                        	selected = (selectedRegionId.longValue() == regionId);
                    %>
                        <option value="<%=regionId %>"<%if(selected){%> selected="selected"<%} %>><%=StringUtils.prepareHTML(regionResults.getValue("regionName", String.class)) %></option><%
                    } %>                    
                </select>               
                </div>
                </div><!--  end of fullForm4-->
                <%} %> 
        </div>
        <!-- /span4  -->
        <div id="fullForm3" <% if(StringUtils.isBlank(username)){%> style="display:none" <%} %>>
        <div class="span4">
            <h5>Change Password</h5>
            <p>Password must be 8 characters or more and contain 1 uppercase
                letter, 1 lowercase letter, and a number or punctuation symbol.</p>
                <div class="control-group">
                    <label class="control-label">New Password:</label>
                    <div class="controls">
                        <input autocomplete="off" type="password" name="newpassword" placeholder="Create password" title="A password with at least 8 characters and 1 uppercase letter, 1 lowercase letter, and a number or punctuation symbol" pattern=".{8,}"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Type New Password Again:</label>
                    <div class="controls">
                        <input autocomplete="off" type="password" name="newconfirm" placeholder="Reenter your password" title="Must match the password entered above"/>
                    </div>
                </div>
        </div>
        <!-- /span6  -->    
            <div class="span4"><%
results = DataLayerMgr.executeQuery("GET_CONSUMER_SETTINGS", user);
results.setLocale(locale);
results.addGroup("group");
DefaultParamRenderer renderer = new DefaultParamRenderer();

String group = null;
while(results.next()) {
    if(results.isGroupBeginning("group")) {
        group = results.getFormattedValue("group");
        if(!StringUtils.isBlank(group)) {
            %><h5><%=StringUtils.prepareHTML(group) %></h5><%
        }
    }
    int settingId = results.getValue("settingId", Integer.class);
    Object settingValue = results.getValue("value");
    ParamEditor paramEditor = results.getValue("editor", ParamEditor.class);
    switch(paramEditor.getEditorType()) {
    	case CHECKBOX:
    	case RADIO:
    		renderer.setBeforeControl("");
            renderer.setAfterControl("</label>");
            renderer.setBeforeLabel("<label class=\"checkbox\">");
            renderer.setAfterLabel("");
            if(settingValue == null)
                settingValue = "Y";
            break;
    	case SELECT:
    		if(settingValue == null && paramEditor instanceof OptionsParamEditor) {
    			Map<String,ParamEditor.Option> options = ((OptionsParamEditor)paramEditor).getOptions();
    			if(options != null && options.size() > 0)
    				settingValue = options.keySet().iterator().next();
    		}
    		//fall-through
        default:
    		renderer.setBeforeControl("");
	    	renderer.setAfterControl("");
	    	renderer.setBeforeLabel("<label class=\"control-label\">");
	    	renderer.setAfterLabel("</label>");
    }
    renderer.render(out, "setting_" + settingId, paramEditor, results.getFormattedValue("label"), settingValue);
    %><input type="hidden" name="settings" value="<%=settingId %>"/><%
}
%>
            </div>
            <!-- /span4  -->
    </div>
    </div><!--  end of fullForm2-->
    <div class="form-actions text-center">
        <button type="submit" class="btn btn-savechanges">Save changes</button>
    </div>
    <!-- row-fluid  -->
</div>
</form>
<!-- /account-settings-wrap  -->
<script type="text/javascript">
var postalEl;
function updatePostalPrompts(desc, pattern, label) {
    if(!postalEl)
        postalEl = $("profile-form").postal;
    postalEl.placeholder = "Enter your " + desc; 
    postalEl.title = "Your " + desc; 
    postalEl.pattern = pattern;
    $("postalCodeLabel").set("html", label);
}
function preferredCommTypeChecked(chk) {
    if (chk.value == 1) {
        chk.form.email.required = true;
        chk.form.mobile.required = false;
        chk.form.carrierId.required = false; 
       $("optional_marker_email").set("class", "hidden");
       $("optional_marker_mobile").set("class", "optional");
       $("optional_marker_carrierId").set("class", "optional");
    } else {
    	chk.form.email.required = false;
        chk.form.mobile.required = true;
        chk.form.carrierId.required = true; 
        $("optional_marker_email").set("class", "optional");
        $("optional_marker_mobile").set("class", "hidden");
        $("optional_marker_carrierId").set("class", "hidden");
    }
}

function onUpgradeToFull(){
	if($('upgradeToFull').checked){
		$("fullForm1").set("style","display:block");
		$("fullForm2").set("style","display:block");
		$("fullForm3").set("style","display:block");
		
		var preferredCommTypeValue;
		$('profile-form').getElements("input[name=preferredCommType]:checked").each(function(el) {
			preferredCommTypeValue=el.value;
		});
		if(preferredCommTypeValue ==1 ){
			$('profile-form').email.required = true;
			$('profile-form').mobile.required = false;
			$('profile-form').carrierId.required = false; 
		}else{
			$('profile-form').email.required = false;
			$('profile-form').mobile.required = true;
			$('profile-form').carrierId.required = true; 
		}
		$('profile-form').country.required=true;
		$('profile-form').address1.required=true;
		$('profile-form').postal.required=true;
		$('profile-form').region.required=true;
		$('profile-form').newpassword.required=true;
		$('profile-form').newconfirm.required=true;
	}else{
		$("fullForm1").set("style","display:none");
		$("fullForm2").set("style","display:none");
		$("fullForm3").set("style","display:none");
		$('profile-form').email.required = false;
		$('profile-form').mobile.required = false;
		$('profile-form').carrierId.required = false; 
		$('profile-form').country.required=false;
		$('profile-form').address1.required=false;
		$('profile-form').postal.required=false;
		$('profile-form').region.required=false;
		$('profile-form').newpassword.required=false;
		$('profile-form').newconfirm.required=false;
	}
}

function onSaveChanges(){
	if($('profile-form').accountType.value=='simple' && !$('upgradeToFull').checked){
		$('profile-form').action="update_user_simple.html";
	}
}
</script>
<jsp:include page="/account_footer.jsp" />