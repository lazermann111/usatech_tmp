<%@page import="com.usatech.prepaid.web.CampaignDetails"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="simple.results.Results.Aggregate"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%!
	protected static final int promoLocationMaxDisplay=2;
%>
<%
PrepaidUser user = (PrepaidUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
Results results = PrepaidUtils.getPromoCampaigns(user);
results.addGroup("attentionType");
results.addGroup("campaignId");
results.addGroup("deviceSerialCd");
results.trackAggregate("campaignId", Aggregate.DISTINCT);
results.setFormat("campaignDiscountPercent", "PERCENT");
Results newresults = null;
Results soonresults = null;
List<Results> otherResultsList = new ArrayList<Results>();
Results subresults;
while((subresults = results.nextSubResult("attentionType")) != null) {
	String attentionType = results.getFormattedValue("attentionType");
	if("NEW".equalsIgnoreCase(attentionType))
		newresults = subresults;
	else if("SOON".equalsIgnoreCase(attentionType))
	    soonresults = subresults;
    else
    	otherResultsList.add(subresults);
}
Translator translator = RequestUtils.getTranslator(request);
Iterator<Results> otherIter = otherResultsList.iterator();
Results otherResults = otherIter.hasNext() ? otherIter.next() : null;
final int n = Math.min(PrepaidUtils.getPromoMaxCampaigns(), results.getAggregate("campaignId", null, Aggregate.DISTINCT, Integer.class));
%>
<p class="promo-announce">Offers for <%=StringUtils.prepareHTML(user.getFullName()) %></p><%
if(n == 0) {
    %><span class="promo-no-offers"><%=translator.translate("prepaid-no-offers", "No offers currently available.") %></span><%
} else {%>
<div id="carousel" class="carousel slide">
    <div class="promo-carousel-controls">
        <a class="carousel-prev pull-left btn-link">&lsaquo;</a>
        <a class="carousel-next pull-right btn-link">&rsaquo;</a>
        <div class="clearfix"></div>
    </div>
    <!-- /promo-carousel-controls -->
    <ol class="carousel-indicators" id="carousel-tabs"><%
for(int i = 0; i < n; i += 5) {
	%><li<%if(i == 0) {%> class="active"<%} %>></li><%
} %>
    </ol>
    <!-- Carousel items -->
<div class="carousel-inner" data-toggle="carousel" data-circular="true" data-active-class="active" data-scroll="1" data-distance="1" data-previous=".carousel-prev" data-next=".carousel-next" data-interval="7" data-delay="7" data-tabs="#carousel-tabs > li" data-link="cancel"><%  
    for(int i = 0; i < n; i++) {
	Results r;
	switch(i%5) {
		case 0:
			if(i == 0) { 
			     %><div class="active item"><%
			} else { 
			     %></div><div class="item"><%
			}
			//fall-through
		case 1:
			if(newresults != null && !newresults.isGroupEnding(0))
			    r = newresults;
			else if(soonresults != null && !soonresults.isGroupEnding(0))
                r = soonresults;
            else {				
				if(otherResults != null && otherResults.isGroupEnding(0))
					otherResults = otherIter.hasNext() ? otherIter.next() : null;
				r = otherResults;
			}
			break;
		case 2 : case 3:
			if(soonresults != null && !soonresults.isGroupEnding(0))
                r = soonresults;
            else if(newresults != null && !newresults.isGroupEnding(0))
                r = newresults;
            else {              
                if(otherResults != null && otherResults.isGroupEnding(0))
                    otherResults = otherIter.hasNext() ? otherIter.next() : null;
                r = otherResults;
            }
            break;
        default:
       	    if(otherResults != null && otherResults.isGroupEnding(0))
               otherResults = otherIter.hasNext() ? otherIter.next() : null;
            if(otherResults != null)    
                r = otherResults;
            else if(newresults != null && !newresults.isGroupEnding(0))
                r = newresults;
            else if(soonresults != null && !soonresults.isGroupEnding(0))
                r = soonresults;
            else
            	r = null;
            break;
	}
	if(r == null || !r.next())
		break;
	String terms;
	String details;
	String productsDetails;
	boolean listLocations;
	String title = r.getValue("campaignDescription", String.class);
	CampaignDetails detail = PrepaidUtils.getPromoCampaignDetails(r, translator);   
	details=detail.getDetails();
	title=detail.getTitle();
	productsDetails = detail.getProductsDetails();
	switch(r.getValue("campaignTypeId", Integer.class)) {
		case 2: // Replenish Reward
			BigDecimal percent = r.getValue("campaignDiscountPercent", BigDecimal.class);
			BigDecimal threshold = r.getValue("campaignThreshold", BigDecimal.class);
			terms = translator.translate("prepaid-promo-replenish-reward-terms", "{0}", 
					title, percent, r.getValue("campaignEndDate"), threshold, threshold.multiply(percent));
			listLocations = false;
			break;
		case 3: // Spend Reward
            percent = r.getValue("campaignDiscountPercent", BigDecimal.class);
            threshold = r.getValue("campaignThreshold", BigDecimal.class);               
            terms = translator.translate("prepaid-promo-spend-reward-terms", "{0}", 
            		title, percent, r.getValue("campaignEndDate"), threshold, threshold.multiply(percent));
            listLocations = false;
            break;
		case 1: //Loyalty Discount
		default:
            terms = translator.translate("prepaid-promo-campaign-terms", "{0}", 
            		title, r.getValue("campaignDiscountPercent"), r.getValue("campaignEndDate"));
            listLocations = true;
            break;
        
	}
	if(terms != null && terms.length() > 21) {
		StringBuilder sb = new StringBuilder(terms.length() + 10);
		int pos = 0;
		while(pos + 21 < terms.length()) {
			int next = StringUtils.indexOf(terms, "\n\r".toCharArray(), pos);
			if(next < 0 || next > pos + 21) {
				//break by space
				next = StringUtils.lastIndexOf(terms, "\t ".toCharArray(), pos + 21);
				if(next < 0 || next <= pos) {
					sb.append(terms, pos, pos + 20).append("-\n");
					pos += 20;
					continue;
				}        
			}
			sb.append(terms, pos, next).append('\n');
            pos = next + 1;
            if(terms.length() > next + 1 && ((terms.charAt(next) == '\n' && terms.charAt(next + 1) == '\r') || (terms.charAt(next) == '\r' && terms.charAt(next + 1) == '\n')))
            	pos++;
		}
		terms = sb.append(terms, pos, terms.length()).toString();
	}
	String attentionType = r.getFormattedValue("attentionType");
	int count=0;
	String campaignId=null;
	%><div class="promo <%if(!StringUtils.isBlank(attentionType)) { %>attention-type-<%=StringUtils.prepareCDATA(attentionType.toLowerCase())%><%}%>">
            <p class="promo-terms"><%=StringUtils.prepareHTML(terms)%></p>
            <p class="promo-details"><%if(listLocations) { %><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" data-trigger="hover&amp;click" data-content="<%               
        do {
        	
        	if(r.isGroupBeginning("deviceSerialCd")) {
        		campaignId = r.getFormattedValue("campaignId");
	        	String location = r.getFormattedValue("locationDescription");
	            if(StringUtils.isBlank(location))
	                location = r.getFormattedValue("locationName");
	            String city = r.getFormattedValue("locationCity");
	            String state = r.getFormattedValue("locationState");
	            String postal = r.getFormattedValue("locationPostal");
	            String country = r.getFormattedValue("locationCountry");
	            StringBuilder sb = new StringBuilder();
	            if(city != null && !(city=city.trim()).isEmpty()) {
	                sb.append(city);
	                if(state != null && !(state=state.trim()).isEmpty())
	                    sb.append(", ").append(state);
	            } else if(state != null && !(state=state.trim()).isEmpty())
	                sb.append(state);
	            if(postal != null && !(postal=postal.trim()).isEmpty()) {
	                if(sb.length() > 0)
	                    sb.append(' ');
	                sb.append(postal);
	            }
	            if(country != null && !(country=country.trim()).isEmpty()) {
	                if(sb.length() > 0)
	                    sb.append(' ');
	                sb.append(country);
	            }
	            String[] locationDetails = {
	            	location,
	                r.getFormattedValue("locationAddress1"),
	                r.getFormattedValue("locationAddress2"),
	                sb.toString()         
	            };
	            boolean first = true;
	            count++;	
	            if(count>=promoLocationMaxDisplay+1){
	            	if(count==promoLocationMaxDisplay+1){
	            		%>for more go to <a href='/locations.html?discountsOnly=true&campaignId=<%=campaignId%>'>Locations</a><% 
	            	}
	            }else{
		            for(String s : locationDetails) {
		                if(s != null && !(s=s.trim()).isEmpty()) {
		                    if(first)
		                        first = false;
		                    else {%>&lt;br/&gt;<%} %><%=StringUtils.prepareCDATA(s).replace("&", "&amp;")%><%}
		            }
	            }
	            
        	}
            if(r.isGroupEnding("campaignId")) 
            	break;
            if(r.isGroupEnding("deviceSerialCd")) {
            	if(count<promoLocationMaxDisplay+1){
                	%>&lt;hr/&gt;<% 
	            }
            }
        } while(r.next()) ;%>" data-classname="promo-popover" data-title="Participating locations"><%=StringUtils.prepareHTML(details)%></a><%
    } else { 
   	    do {
   	        if(r.isGroupEnding("campaignId"))
   	            break;
   	    } while(r.next()) ;    
    %><%=StringUtils.prepareHTML(details)%><%} %></p>
    		<p class="promo-products"><%if(productsDetails.length() >0) { %><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" data-trigger="hover&amp;click" data-classname="promo-popover" data-title="Participating products" style="color:purple" data-content="<%=r.getValue("products")%>"><%=StringUtils.prepareHTML(productsDetails)%></a><%} %>
    		</p>
        </div><%
} %></div>
    </div>
	<div class="promo-carousel-controls">
        <a class="carousel-prev pull-left btn-link">&lsaquo;</a>
        <a class="carousel-next pull-right btn-link">&rsaquo;</a>
	    <div class="clearfix"></div>
	</div>
	<!-- /promo-carousel-controls -->
</div>
<div class="promo-disclaimer"><%=translator.translate("prepaid-offer-disclaimer", "Discounts may not be combined. Best offer available will be used.") %></div>
<%} %>