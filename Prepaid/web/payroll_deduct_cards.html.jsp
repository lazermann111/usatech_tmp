<%@page import="simple.io.Log"%>
<%@page import="com.usatech.prepaid.web.CardOnFile"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="simple.lang.SystemUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.results.Results.Aggregate"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="java.util.TimeZone"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.prepaid.pass.PassServletUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
if(user == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-logged-in", "You are no longer logged in. Please log in again.");
    RequestUtils.redirectWithCarryOver(request, response, "signin.html");
    return;
}
Long cardId = RequestUtils.getAttribute(request, "cardId", Long.class, false);
if(cardId != null && PrepaidUtils.checkCardPermission(user, cardId) == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
    RequestUtils.redirectWithCarryOver(request, response, PrepaidUtils.getHomePage());
    return;
}
Integer addConsumerAcctTypeId = RequestUtils.getAttribute(request, "addConsumerAcctTypeId", Integer.class, false);
TimeZone timeZone = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_TIME_ZONE, TimeZone.class, false, RequestUtils.SESSION_SCOPE);
Locale locale = RequestUtils.getLocale(request);
RequestUtils.setAttribute(request, "tab", "payroll_deduct_cards", "request"); 
RequestUtils.setAttribute(request, "extra-stylesheets", "/css/pikaday.css", "request"); 
%>
<jsp:include page="/account_header.jsp" />
<%
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
Results results = DataLayerMgr.executeQuery("GET_ACCOUNTS_PAYROLLDEDUCT", user);
results.setLocale(locale);
String cardFormat = "simple.text.PadFormat:        *";
String currencyFormat = "CURRENCY"; 
results.setFormat("cardNum", cardFormat);
results.setFormat("balance", currencyFormat);
results.setFormat("onHold", currencyFormat);
DateFormat[] dateFormats = PrepaidUtils.getStandardDateFormats(locale, timeZone);
String currencyCd = null;
String balanceText = null;
String dateText = null;
String onHoldText = null;
Long consumerAcctIdentifier = null;
%>
		<div>
        	<div class="payroll-deduct-cards-wrap">
            <div class="account-table-header">
                <form class="transactions-card-select form-inline form-flow">
                    <strong>Information for</strong>
<%
int cardCount = results.getRowCount();
int cardOrder = 0;
String cardNum = "";
String currentNickName="";
if(cardCount == 1) {
    results.next();
    currencyCd = results.getFormattedValue("currencyCd");
    balanceText = results.getFormattedValue("balance");
    consumerAcctIdentifier = results.getValue("consumerAcctIdentifier", Long.class);
    onHoldText = results.getFormattedValue("onHold");
    cardOrder = 1;
    cardNum = results.getFormattedValue("cardNum");
    cardId = results.getValue("cardId", Long.class);
    form.setAttribute("cardId", cardId);
    %><%=StringUtils.encodeForHTML(results.getFormattedValue("activeStatus"))%> Payroll Deduct Card <span class="cardNum"><%=StringUtils.encodeForHTML(cardNum)%>
    <%String nickName=results.getFormattedValue("nickName");
    if(!StringUtils.isBlank(nickName)){ %>
    (<%=StringUtils.prepareHTML(nickName)%>)
    <%}
    currentNickName=nickName;
} else {%>
                    <select name="cardId" onchange="this.form.submit()" id="cardId">
                        <option value=""<%if(cardId == null) {
    balanceText = results.getFormattedAggregate("balance", null, Aggregate.SUM);
    onHoldText = results.getFormattedAggregate("onHold", null, Aggregate.SUM);
                        %> selected="selected"<%} %>>All</option><%
	int i = 1;
	while(results.next()) {
		String cc = results.getFormattedValue("currencyCd");
    	long id = results.getValue("cardId", Long.class);
    	%><option value="<%=id%>"<%
    	String nickName=results.getFormattedValue("nickName");
    	if(cardId == null) {
        	if(currencyCd == null)
            currencyCd = cc;
        	else if(!currencyCd.equals(cc)) {
            currencyCd = "(mixed)";
        	}
    	} else {
        	if(cardId.longValue() == id) {
        	cardOrder = i;
        	cardNum = results.getFormattedValue("cardNum");
            currencyCd = cc;
            balanceText = results.getFormattedValue("balance");
            consumerAcctIdentifier = results.getValue("consumerAcctIdentifier", Long.class);
            onHoldText = results.getFormattedValue("onHold");
            currentNickName=nickName;
	    	%> selected="selected"<% 
	    	}
		}%>><%=StringUtils.prepareHTML(results.getFormattedValue("activeStatus"))%> Payroll Deduct Card <%=i++ %>: <%=StringUtils.prepareHTML(results.getFormattedValue("cardNum"))%>
	<%if(!StringUtils.isBlank(nickName)){ %>
    (<%=StringUtils.prepareHTML(nickName)%>)
    <%} %>
	</option><%    
}%> </select>
<%
}
String expand = RequestUtils.getAttribute(request, "expand", String.class, false);
String cardBrand = RequestUtils.getTranslator(request).translate("prepaid-card-brand", "payroll deduct");%>
<a class="btn-link action-link add-link" data-toggle="expand" data-target="newCardDetails">ADD <%=StringUtils.prepareHTML(cardBrand.toUpperCase())%> CARD</a>          
</form>
<%
String action = form.getStringSafely("action", "");
if ("Add to Apple Wallet".equalsIgnoreCase(action) && cardId != null) {
	String passUrl = PassServletUtils.createPassUrlByCardId(cardId);
%>
	<script type="text/javascript">
	window.open("<%=passUrl%>", "passDownload");
	</script>
<%}%>
                <div class="clearfix"></div>
                <div id="newCardDetails"<%if(!"new".equalsIgnoreCase(expand)) {%> class="hide"<%} %>>
                    <form class="register-card-form" action="register_payroll_deduct_card.html" method="post">
                    <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
                    <label><span>Your 19-digit Payroll Deduct Card Number</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-cardnumber_highlighted.gif' alt='' height='172' width='266'/&gt;">Where is this?</a></label>                      
	                <input type="text" id="payrollDeductCard" required="required" name="card" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card number" title="The 19-digit <%=StringUtils.prepareCDATA(cardBrand) %> card number"  maxlength="19" autofocus="autofocus" autocomplete="off" pattern="\d{19}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "card", String.class, false))%>"/>
	                <label><span>The 4-digit Card Security Code</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-securitycode_highlighted.gif' alt='' height='172' width='266'/&gt;">Where is this?</a></label>                        
	                <input type="password" id="payrollDeductSecurityCode" required="required" name="securityCode" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card's security code" title="The 4-digit security code on the back of your <%=StringUtils.prepareCDATA(cardBrand) %> card"  maxlength="4" autocomplete="off" pattern="\d{4}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "securityCode", String.class, false))%>"/>
	                <label><span>Card Nickname:</span><span class="optional">Optional</span></label>
                    <input type="text" maxLength="60" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "nickname", String.class, false))%>" title="Enter nickname for the card" name="nickname"> 
                    <div class="form-actions text-center">
                        <button type="submit" class="btn btn-savechanges">Add <%=StringUtils.prepareHTML(StringUtils.capitalizeAllWords(cardBrand))%> Card</button>
                    </div>
                    </form>
                 </div>
            </div>        
            <%
			if (cardId != null && consumerAcctIdentifier != null) {
				String passUrl = PassServletUtils.getPassUrlByCardId(cardId);
				%>
				<script type="text/javascript">
				function addToAppleWallet() {
					<%if (StringUtils.isBlank(passUrl)) {%>
					document.getElementById("appleWalletForm").submit();
					<%} else {%>
					window.open("<%=passUrl%>", "passDownload");
					<%}%>
				}
				</script>
				<div class="spacer10"></div>
				To add your payroll deduct card to Apple Wallet, please open this web page on your iPhone or iPod touch.
				<div class="spacer10"></div>
				<form id="appleWalletForm" method="post">
					<input type="hidden" name="action" value="Add to Apple Wallet" />
					<a href="javascript:addToAppleWallet();"><img src="/images/Add_to_Apple_Wallet_rgb_US-UK.svg" style="width: 100%; max-width: 175px;" alt="Add to Apple Wallet" /></a></td>
				</form>
				<hr/>
			<%}%>	
            <span class="replenish-announcements">
                <span class="replenish-current-balance"><strong>Current Balance:</strong> <%=StringUtils.prepareHTML(balanceText) %> <%=StringUtils.prepareHTML(currencyCd) %></span>
                <div class="clearfix"></div>
            </span>
            </div>
    <%if(cardId!=null) {%>        
	<form name="nickNameForm" action="edit_nickname.html" method="post" class="form-inline">
	<input type="hidden" name="fromConsumerAcctTypeId" value="7"/>
    <input type="hidden" name="cardId" value="<%=cardId%>"/>
    <strong>Card Nickname:</strong> <input type="text" maxLength="60" value="<%=StringUtils.prepareCDATA(currentNickName) %>" title="Enter nickname for the card" name="nickname"> <button type="submit" class="btn btn-savechanges">Update Nickname</button>
	</form>        
	
	<form class="form-inline" method="post" action="add_promo.html" name="addPromoForm">
	<input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
	<input type="hidden" name="cardTypeId" value="4"/>
	<strong>Promotion Code:</strong>
	<input type="text" title="The promotion code. Usually letters and numbers less than or equal to 20 characters" value="" autocomplete="off" pattern="^[a-zA-Z0-9]{1,20}$" required="required" maxlength="20" placeholder="Enter the promotion code" name="promoCode">
	<button class="btn btn-savechanges" type="submit">Add Promotion</button>
	</form> 
	<%} %>
</div>
<jsp:include page="/account_footer.jsp" />