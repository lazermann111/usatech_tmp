<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
long consumerId;
if(user != null)
	consumerId = user.getConsumerId();
else
	consumerId = ConvertUtils.getLong(request.getParameter("consumerId"));

String message = RequestUtils.getAttribute(request, "message", String.class, false);
String buttonText = request.getParameter("buttonText");
String prompt = request.getParameter("prompt");
if(StringUtils.isBlank(prompt)){
	prompt = RequestUtils.getTranslator(request).translate("prepaid-registration-prompt", "To receive email or text messages,\nplease enter the passcode found in the email or text message that has been sent to you.");
}
prompt = StringUtils.prepareHTML(prompt);

%><form method="post" action="register_user.html">
<input type="hidden" name="userId" value="<%=consumerId%>"/>
<% if(message != null) { %>
<div class="alert text-center"><%=message%></div>
<%} else {%><br/><%} %>
<div class="alert alert-info text-center">
<%=StringUtils.prepareHTML(prompt) %>
</div>
<div class="text-center"><input id="passcode" autocomplete="off" type="password" name="passcode" placeholder="Enter the passcode" title="Enter the passcode" required="required"/></div>
<div class="text-center"><input type="checkbox" name="showPasscode" value="" onclick="onClickShowPasscode()"/> Show Passcode</div>
<div class="text-center">
    <button type="submit" class="btn btn-large btn-submit"><%=StringUtils.prepareHTML(buttonText) %></button>
</div>
</form>
<script type="text/javascript">
function onClickShowPasscode(){
    if($("passcode").type == 'password') {
        $("passcode").type = "text";
    } else {
        $("passcode").type = "password";
    }
}
</script>		