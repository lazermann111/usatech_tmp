<%@page import="simple.io.Log"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
if(user == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-logged-in", "You are no longer logged in. Please log in again.");
    RequestUtils.redirectWithCarryOver(request, response, "signin.html");
    return;
}
String promoCode = RequestUtils.getAttribute(request, "promoCode", String.class, false);
int cardTypeId = RequestUtils.getAttribute(request, "cardTypeId", int.class, true);
String redirectPage;
if(cardTypeId==1){
	redirectPage="allcards.html";
}else if (cardTypeId==2){
	redirectPage="cards.html";
}else{
	redirectPage="payroll_deduct_cards.html";
} 

if(StringUtils.isBlank(promoCode)){
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
	RequestUtils.redirectWithCarryOver(request, response, redirectPage);		
}else if(!promoCode.trim().matches(PrepaidUtils.PROMO_CODE_CHECK)){
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-promo-code", "The promotion code you entered is invalid. Please re-enter it.");
	RequestUtils.redirectWithCarryOver(request, response, redirectPage);		
}
Connection conn = DataLayerMgr.getConnectionForCall("ADD_PROMO");
try {
    try {
    	promoCode=promoCode.trim();
    	DataLayerMgr.executeCall(conn, "ADD_PROMO", new Object[]{promoCode, user.getConsumerId()});
    } catch(SQLException e) {
    	try {
    		conn.rollback();
    	} catch(SQLException e2) {
    		//ignore
    	}
    	if(log.isWarnEnabled())
              log.warn("Could not add promo code to consumerId=" + user.getConsumerId()+" promoCode="+promoCode, e);
    	switch(e.getErrorCode()) {
	        case 20310:
	            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-promo-code", "The promotion code you entered is invalid. Please re-enter it.", RequestUtils.getAttribute(request, "promoCode", String.class, false));
	            break;
	        case 20311:
	            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-promo-code-exists", "The promotion code you entered is already associated with the card. Please enter a new one.", RequestUtils.getAttribute(request, "promoCode", String.class, false));
	            break;
	        default:
	        	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
		}
	    RequestUtils.redirectWithCarryOver(request, response, redirectPage);
    	return;
    }
    conn.commit();
    if(log.isInfoEnabled())
          log.info("Add promocode to user '" + user.getUserName() + ": consumerId=" + user.getConsumerId());
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-credit-add-promo", "Successfully added the promotion code", "");
	RequestUtils.redirectWithCarryOver(request, response,redirectPage,  false, true);
} finally {
	conn.close();
}
%>