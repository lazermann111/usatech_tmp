<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.usatech.prepaid.web.NotRegisteredException"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.servlet.steps.LoginFailureException"%>
<%@page import="simple.servlet.SimpleServlet,simple.servlet.InputForm,java.util.Map,simple.bean.ConvertUtils,simple.servlet.InputFile,simple.servlet.RequestUtils,simple.text.StringUtils"%>
<%@page import="java.util.Set,java.util.HashSet,java.util.Arrays,simple.servlet.SimpleAction,simple.servlet.NotLoggedOnException,simple.servlet.NoSessionTokenException,simple.servlet.InvalidSessionTokenException"%>
<%! 
protected static final Set<String> ignoreParameters = new HashSet<String>(Arrays.asList(new String[] {
    "username",
    "password",
    "loginPromptPage",
    "simple.servlet.steps.LogonStep.forward",
    "content",
    "session-token",
    "selectedMenuItem",
    "card",
    "securityCode",
    "signInType"
}));
%><%
RequestUtils.setAttribute(request, "subtitle", "Sign In", "request");
RequestUtils.setAttribute(request, "signUpLink", true, "request");
RequestUtils.setAttribute(request, "wrapNarrow", true, "request"); 
String contentPath = request.getRequestURI();
if(contentPath == null)
	contentPath = PrepaidUtils.getHomePage();
else if (contentPath.toLowerCase().endsWith("/register_user.html"))
	contentPath = PrepaidUtils.getHomePage();
else if (!PrepaidUtils.shouldForwardAfterLogin(contentPath.toLowerCase()))
    contentPath = PrepaidUtils.getHomePage();
Integer signInType = RequestUtils.getAttribute(request, "signInType", Integer.class, false);
if(signInType==null){signInType=1;}
String cardBrand = RequestUtils.getTranslator(request).translate("prepaid-card-brand", "loyalty");
Throwable exception = (Throwable)RequestUtils.getAttribute(request, SimpleAction.ATTRIBUTE_EXCEPTION, false);
InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);

boolean expired;
if(session != null) {
    if(session.isNew())
        expired = false;
    else if(session.getMaxInactiveInterval() > 0)
        expired = (System.currentTimeMillis() - session.getLastAccessedTime())/ 1000 > session.getMaxInactiveInterval();
    else
        expired = false;
} else
    expired = false;
%><jsp:include page="/header.jsp"/>
<div class="form-wrap"><%
if(exception instanceof NotRegisteredException) {
    long userId = ((NotRegisteredException)exception).getUserId();
    form.setAttribute("userId", userId);
    int preferredCommType;
    String communicationEmail;
    String firstname;
    String lastname;
    Connection conn = DataLayerMgr.getConnectionForCall("REREGISTER_CONSUMER");
    try {
        DataLayerMgr.executeCall(conn, "REREGISTER_CONSUMER", form);
        preferredCommType = form.getInt("preferredCommType", true, 0);
        communicationEmail = form.getString("communicationEmail", true);
        firstname = form.getString("firstname", false);
        lastname = form.getString("lastname", false);
        PrepaidUtils.sendCompleteRegistrationEmail(conn, communicationEmail, preferredCommType, firstname, lastname, userId, form.getString("passcode", true), RequestUtils.getBaseUrl(request), request);
        conn.commit();
    } finally {
        conn.close();
    }
    switch(preferredCommType) {
        case 2: //SMS
            String prompt = RequestUtils.getTranslator(request).translate("prepaid-not-registered-sms", "To receive text messages,\nplease enter the passcode found in the text message that has been sent to you.");
        	String includePage = new StringBuilder().append("/verify_user_form.jsp?buttonText=Complete+Verification&prompt=").append(StringUtils.prepareURLPart(prompt)).toString();
            %>
            <jsp:include page="<%=includePage%>"/>
            <%
            break;
        default:
            %><div class="alert alert-error"><%=RequestUtils.getTranslator(request).translate("prepaid-not-registered-email", "A new email has been sent to ''{0,PREPARE,HTML}''. To receive email messages, please click the link in the email.", communicationEmail, firstname, lastname) %></div><% 
    }
    return;
}%>
	<h1>Sign in</h1>
	<div class="signin-form">
		<form id="signin-form" method="post" action="<%=StringUtils.encodeForHTMLAttribute(contentPath)%>" onsubmit="App.updateClientTimestamp(this);clearRemember(this);" autocomplete="off">
           <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/><%
for(Map.Entry<String,Object> param : form.getParameters().entrySet()) {
    if(ignoreParameters.contains(param.getKey())) continue;
    if(param.getValue() instanceof String) {
        %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute((String)param.getValue())%>"/><%
    } else if(param.getValue() instanceof String[]) {
        String[] values = (String[])param.getValue();
        for(int i = 0; i < values.length; i++) {
            %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(values[i])%>"/><%
        }
    } else if(param.getValue() instanceof InputFile || param.getValue() instanceof InputFile[]) {
        RequestUtils.storeForNextRequest(request.getSession(), param.getKey(), param.getValue());
    } else if(param.getValue() != null) {
        String value = ConvertUtils.getStringSafely(param.getValue());
        %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(value)%>"/><%
    }
}
String infoMessage = RequestUtils.getAttribute(request, "onloadMessage", String.class, false);
if(infoMessage != null && infoMessage.trim().length() > 0) { 
%><div class="alert alert-success"><%=infoMessage%></div><% 
}
if(exception instanceof LoginFailureException) { 
%><div class="alert alert-error"><%=RequestUtils.getTranslator(request).translate("prepaid-login-failed-prompt", "Your login information is invalid; please re-enter your login information.") %></div><% 
} else if((exception instanceof NotLoggedOnException && expired)
        || (exception instanceof NoSessionTokenException && (null != RequestUtils.getSessionToken(request) || null != request.getParameter(SimpleServlet.REQUEST_SESSION_TOKEN)))) { 
%><div class="alert alert-error"><%=RequestUtils.getTranslator(request).translate("prepaid-login-expired-prompt", "Your session has expired; please re-enter your login information.") %></div><% 
} else if(exception instanceof InvalidSessionTokenException) { 
%><div class="alert alert-error"><%=RequestUtils.getTranslator(request).translate("prepaid-invalid-request", "The request was invalid, please login again for the protection of your account.") %></div><% 
}
Cookie cookie = form.getCookie("username");
String username = RequestUtils.getAttribute(request, "username", String.class, false);
if(StringUtils.isBlank(username)) {
    if(cookie != null)
        username = cookie.getValue();
}
%>
			<fieldset>
				<div class="controls">
                    <label class="radio inline">
                        <input type="radio" name="signInType" value="1" onclick="signInTypeChanged(this)" <%if(signInType == 1) {%> checked="checked"<%} %>/> Full Sign In
                    </label>
                    <label class="radio inline">
                        <input type="radio" name="signInType" value="2" onclick="signInTypeChanged(this)" <%if(signInType == 2) {%> checked="checked"<%}%>/> Simple Sign In
                    </label>
                </div>
				<div id="fullSignIn" <%if(signInType == 1) {%> class="shown"<%} else{%>class="hidden"<%} %> >
				<label>Email Address or Mobile Phone Number</label>
                <input type="text" name="username" placeholder="Enter your email address or mobile phone number" title="Your email address or mobile phone number" tabindex="1" value="<%=StringUtils.prepareCDATA(username)%>" <%if(signInType == 1) {%> required="required"<%} %> autofocus="autofocus"/>
                <label>Password</label>
                <input type="password" name="password" placeholder="Password" title="Your password" tabindex="2" value="" autocomplete="off" <%if(signInType == 1) {%> required="required"<%} %>/>
                <p>Forgot your password? <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/forgot_password.html") %>">Click here</a>.</p>
                <label class="checkbox">
                    <input type="checkbox" name="remember" value="true" title="Remember your username for the next session" <%if(cookie != null) { %> checked="checked"<%} %>/> Remember Me
				</label>
				</div>
				<div id="simpleSignIn" <%if(signInType == 1) {%> class="hidden"<%} else{%>class="shown"<%} %> >
				<label><span>Your 19-digit Card Number</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-cardnumber_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input type="text" name="card" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card number" title="The 19-digit <%=StringUtils.prepareCDATA(cardBrand) %> card number" <%if(signInType == 2) {%> required="required"<%} %> maxlength="19" autofocus="autofocus" pattern="\d{19}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "card", String.class, false))%>"/>
                <label><span>The 4-digit Card Security Code</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-securitycode_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input autocomplete="off" type="password" name="securityCode" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card's security code" title="The 4-digit security code on the back of your <%=StringUtils.prepareCDATA(cardBrand) %> card" <%if(signInType == 2) {%> required="required"<%} %> maxlength="4" pattern="\d{4}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "securityCode", String.class, false))%>"/>
                <label class="checkbox">
                    <input type="checkbox" name="rememberCard" value="true" title="Remember your card for the next session" <%if(cookie != null) { %> checked="checked"<%} %>/> Remember Me
				</label>
				</div>
				<div style="text-align: center;">
					<button type="submit" class="btn btn-large btn-submit">Login</button>
				</div>
			</fieldset>
		</form>
	</div>
	<!-- /signin-form -->
	<p>Need an account? <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signup_question.html") %>">Create one</a>.</p>
</div>
<hr/>
<!-- /form-wrap -->
<script type="text/javascript" defer="defer">

function signInTypeChanged(chk) {
    if (chk.value == 1) {
    	$("fullSignIn").set("class", "shown");
    	$("simpleSignIn").set("class", "hidden");
        chk.form.username.required = true;
        chk.form.password.required = true; 
        chk.form.card.required = false;
        chk.form.securityCode.required = false; 
        chk.form.card.value="";
        chk.form.securityCode.value="";
    } else {
    	$("fullSignIn").set("class", "hidden");
    	$("simpleSignIn").set("class", "shown");
        chk.form.username.required = false;
        chk.form.password.required = false; 
        chk.form.card.required = true;
        chk.form.securityCode.required = true; 
        chk.form.username.value="";
        chk.form.password.value="";
    }
}

function clearRemember(f){
	if(f.signInType.value==1){
		f.rememberCard.checked="";
	}else{
		f.remember.checked="";
	}
}
</script>
<jsp:include page="/footer.jsp"/>
