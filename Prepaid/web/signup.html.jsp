<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.prepaid.pass.notification.SignUpFromPassUtils"%>
<%@page import="com.usatech.prepaid.pass.notification.dto.PassUserInfo"%>
<%@page import="simple.param.html.DefaultAddressRenderer"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%!
DefaultAddressRenderer addressRenderer = new DefaultAddressRenderer();
%>
<%
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
int cardType = form.getInt("cardType", false, 2);

String passSerialNumber = form.getStringSafely("uuid", null);
if (passSerialNumber == null) {
    passSerialNumber = RequestUtils.getAttribute(request, "passSerialNumber", String.class, false);
}
PassUserInfo passUserInfo = passSerialNumber == null ? null : SignUpFromPassUtils.getNewUserInfo(passSerialNumber);
if (passUserInfo != null) {
    RequestUtils.setAttribute(request, "passSerialNumber", passSerialNumber, "request");
    RequestUtils.setAttribute(request, "email", passUserInfo.getUserEmail(), "request");
    RequestUtils.setAttribute(request, "emailConfirm", passUserInfo.getUserEmail(), "request");
    RequestUtils.setAttribute(request, "mobile", passUserInfo.getUserPhone(), "request");
    RequestUtils.setAttribute(request, "mobileConfirm", passUserInfo.getUserPhone(), "request");
    RequestUtils.setAttribute(request, "firstname", passUserInfo.getFirstName(), "request");
    RequestUtils.setAttribute(request, "lastname", passUserInfo.getLastName(), "request");
    RequestUtils.setAttribute(request, "postal", passUserInfo.getPostalCode(), "request");

    Map<String,Object> params = new HashMap<String,Object>();
    params.put("consumerEmail", passUserInfo.getUserEmail());
    params.put("phoneNumber", passUserInfo.getUserPhone());
    Results results = DataLayerMgr.executeQuery("GET_CONSUMER", params);
  	if (results.next()) {
  		if (cardType == 2){ 
  			RequestUtils.redirectWithCarryOver(request, response, "register_card.html", false, true);
  		}else if (cardType == 4){
  			RequestUtils.redirectWithCarryOver(request, response, "register_payroll_deduct_card.html", false, true);
  		}else if (cardType == 0){
  			RequestUtils.redirectWithCarryOver(request, response, "allcards.html", false, true);
  		}
  	}
}
if (passUserInfo == null && passSerialNumber != null){
	RequestUtils.redirectWithCarryOver(request, response, "signin.html", false, true);
}

String termsConditionPage=null;
String privacyPage=null;
RequestUtils.setAttribute(request, "subtitle", "Sign Up", "request"); 
RequestUtils.setAttribute(request, "wrapNarrow", true, "request"); 
Translator translator = RequestUtils.getTranslator(request);
String prompt = translator.translate("prepaid-new-user-prompt", "Please fill in the form below to create your account. All fields are required except where indicated.");
String imageTitle = translator.translate("prepaid-new-user-image-title", "");
Locale locale = RequestUtils.getLocale(request);
String cardBrand = RequestUtils.getTranslator(request).translate("prepaid-card-brand", "loyalty");
Integer replenishType = RequestUtils.getAttribute(request, "replenishType", Integer.class, false);
if (replenishType == null)
    replenishType = 1;
BigDecimal threshhold = RequestUtils.getAttribute(request, "threshhold", BigDecimal.class, false);
if (threshhold == null)
    threshhold = new BigDecimal(5);
BigDecimal amount = RequestUtils.getAttribute(request, "amount", BigDecimal.class, false);
if (amount == null)
    amount = new BigDecimal(20);
Integer preferredCommType = RequestUtils.getAttribute(request, "preferredCommType", Integer.class, false);
if (preferredCommType == null)
    preferredCommType = 1;
Integer carrierId = RequestUtils.getAttribute(request, "carrierId", Integer.class, false);
Results carrierResults = DataLayerMgr.executeQuery("GET_SMS_GATEWAYS", form);
String cardNum = RequestUtils.getAttribute(request, "card", String.class, false);
Results regionResults = passUserInfo == null ? PrepaidUtils.getRegionsForCard(cardNum) : PrepaidUtils.getRegionsForPass(passSerialNumber);
if(regionResults == null) {
    String subdirectory = PrepaidUtils.getSubdirectory(request);
    if(!StringUtils.isBlank(subdirectory)) {
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("subdirectory", subdirectory);
        params.put("subdomain", request.getServerName());
        regionResults = DataLayerMgr.executeQuery("GET_REGIONS_FOR_SUBDOMAIN", params);
    } 
}
%><jsp:include page="/header.jsp"/>
<div class="form-wrap"><%
if(!StringUtils.isBlank(imageTitle)) {%>
<img class="new-user-image" src="<%=ToJspSimpleServlet.untranslatePath(request, "/new_user_image._") %>" alt="<%=StringUtils.prepareCDATA(imageTitle) %>" /><%
} %>
	<h1>Sign Up</h1>
	<div class="signup-form">
		<form id="signup-form" method="post" action="create_user.html" onsubmit="App.updateClientTimestamp(this)">
		   <input type="hidden" name="cardType" value="<%=cardType%>">
		   <input type="hidden" name="passSerialNumber" value="<%=passSerialNumber == null ? "" : passSerialNumber%>">
		   <input type="hidden" name="fromSignup" value="true">
	       <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
<% if(!StringUtils.isBlank(prompt)) { 
%><h4 class="message-success"><%=prompt%></h4><% 
} %>
			<fieldset>
			<%if(cardType==2){ 
				termsConditionPage=ToJspSimpleServlet.untranslatePath(request, "/morecard_terms_conditions.html");
				privacyPage=ToJspSimpleServlet.untranslatePath(request, "/morecard_privacy_policy.html");
				if (passUserInfo == null) {
				%>
				<label><span>Your 19-digit MORE Card Number</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-cardnumber_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input type="text" name="card" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card number" title="The 19-digit <%=StringUtils.prepareCDATA(cardBrand) %> card number" required="required" maxlength="19" autofocus="autofocus" autocomplete="off" pattern="\d{19}" value="<%=StringUtils.prepareCDATA(cardNum)%>" onchange="cardNumChange()"/>
                <label><span>The 4-digit Card Security Code</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-securitycode_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input type="password" name="securityCode" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card's security code" title="The 4-digit security code on the back of your <%=StringUtils.prepareCDATA(cardBrand) %> card" required="required" maxlength="4" autocomplete="off" pattern="\d{4}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "securityCode", String.class, false))%>"/>
                
                <%}
				}else if (cardType==3){%>
                <label><span>Your 16-digit Sprout Card Number</span></label>
                <input type="text" name="card" placeholder="Enter your Sprout card number" title="The 16-digit Sprout card number" required="required" maxlength="16" autofocus="autofocus" autocomplete="off" pattern="\d{16}" value="<%=StringUtils.prepareCDATA(cardNum)%>" onchange="cardNumChange()"/>
                <label><span>The last 4-digit of the OAN number at the back of the card</span></label>
                <input type="password" name="securityCode" placeholder="Enter your Sprout card's security code" title="The 4-digit security code on the back of your Sprout card" required="required" maxlength="4" autocomplete="off" pattern="\d{4}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "securityCode", String.class, false))%>"/>
                <% 
                }else if (cardType==4){
                	termsConditionPage=ToJspSimpleServlet.untranslatePath(request, "/payroll_deduct_cards_terms_conditions.html");
					privacyPage=ToJspSimpleServlet.untranslatePath(request, "/payroll_deduct_cards_privacy_policy.html");
					if (passUserInfo == null) {
				%>
                <label><span>Your 19-digit Payroll Deduct Card Number</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-cardnumber_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input type="text" name="card" placeholder="Enter your Payroll Deduct card number" title="The 19-digit <%=StringUtils.prepareCDATA(cardBrand) %> card number" required="required" maxlength="19" autofocus="autofocus" autocomplete="off" pattern="\d{19}" value="<%=StringUtils.prepareCDATA(cardNum)%>" onchange="cardNumChange()"/>
                <label><span>The 4-digit Card Security Code</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-securitycode_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input type="password" name="securityCode" placeholder="Enter your Payroll Deduct card's security code" title="The 4-digit security code on the back of your <%=StringUtils.prepareCDATA(cardBrand) %> card" required="required" maxlength="4" autocomplete="off" pattern="\d{4}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "securityCode", String.class, false))%>"/>
                
                <%}
                }else if(cardType==1){ 
                	termsConditionPage=ToJspSimpleServlet.untranslatePath(request, "/allcards_terms_conditions.html");
                	privacyPage=ToJspSimpleServlet.untranslatePath(request, "/allcards_privacy_policy.html");
                %>
                <label><span>Promotion Code:</span></label>
			    <input type="text" name="promoCode" placeholder="Enter the promotion code" maxlength="20" required="required" pattern="^[a-zA-Z0-9]{1,20}$" autocomplete="off" value="<%=StringUtils.prepareCDATA(form.getStringSafely("promoCode", "")) %>" title="The promotion code. Usually letters and numbers less than or equal to 20 characters" />
                <label><span>Credit Card Number:</span></label>
			    <input type="text" name="creditCardNum" placeholder="Enter credit card number" maxlength="16" required="required" pattern="\d{15,16}" autocomplete="off" value="<%=StringUtils.prepareCDATA(form.getStringSafely("creditCardNum", "")) %>" title="The credit card number without spaces or punctuation. Usually 15 or 16 digits." />
			    <div class="control-group"><label class="control-label">Expiration Date:</label>
			    <div class="controls"><select name="creditCardExpMonth" class="autosize" required="required"><%
				int month = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "creditCardExpMonth", false), 0);
				int year = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "creditCardExpYear", false), 0);
				Calendar now = Calendar.getInstance();
				int currYear = now.get(Calendar.YEAR);%>
				<option value=""<%if(month < 1 || month > 12) {%> selected="selected"<%} %>>--</option><%
				for(int m = 1; m <= 12; m++) {%>
				    <option<%if(month == m) {%> selected="selected"<%} %> value="<%=m%>"><%=StringUtils.pad(Integer.toString(m), '0', 2, StringUtils.Justification.RIGHT)%></option><%
				}%></select>
				<select name="creditCardExpYear" class="autosize" required="required">
				<option value=""<%if(year < currYear) {%> selected="selected"<%} %>>--</option><%
				for(int y = currYear; y <= currYear + 10; y++) {%>
				    <option<%if(year == y) {%> selected="selected"<%} %> value="<%=y%>"><%=y%></option><%
				}%></select></div></div>
				<div class="control-group"><label class="control-label">Security Code:</label>
				<div class="controls"><input type="password" name="creditCardCvv" maxlength="4" required="required" pattern="\d{3,4}" placeholder="Enter 3 or 4 digit code" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "creditCardCvv", String.class, false))%>" title="The 3 or 4 digit security code found on the back of your credit card"/></div></div>
                <%} %>
				<hr/>
				<div class="control-group">
                    <label class="control-label">Preferred Communication Method:</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input type="radio" name="preferredCommType" value="1" onclick="preferredCommTypeChecked(this)"<%if(preferredCommType == 1) {%> checked="checked"<%}%> /> Email
                        </label>
                        <label class="radio inline">OR</label>
                        <label class="radio inline">
                            <input type="radio" name="preferredCommType" value="2" onclick="preferredCommTypeChecked(this)"<%if(preferredCommType == 2) {%> checked="checked"<%}%> /> SMS (Text)
                        </label> 
                    </div>
                </div>
				<label><span>Email Address</span><span id="optional_marker_email" class="<%=(preferredCommType == 1 ? "hidden" : "optional") %>">Optional</span></label>
                <input type="email" id="email" name="email" placeholder="Enter your email address" title="Your email address" <%if(preferredCommType == 1) {%>required="required" <%}%>value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "email", String.class, false))%>" autocomplete="off" />
                <label><span>Confirm Email Address</span><span id="optional_marker_email_confirm" class="<%=(preferredCommType == 1 ? "hidden" : "optional") %>">Optional</span></label>
                <input type="email" name="emailConfirm" placeholder="Enter your email address" title="Your email address" <%if(preferredCommType == 1) {%>required="required" <%}%>value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "emailConfirm", String.class, false))%>" autocomplete="off" />
                <label><span>Mobile Phone Number (for SMS)</span><span id="optional_marker_mobile" class="<%=(preferredCommType == 2 ? "hidden" : "optional") %>">Optional</span></label>
                <input type="tel" name="mobile" placeholder="Enter your 10-digit mobile phone number" title="Your 10-digit mobile phone number" pattern="\(?\s*\d{3}\s*\)?\s*[-.]?\s*\d{3}\s*[-.]?\s*\d{4}" <%if(preferredCommType == 2) {%>required="required" <%}%>value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "mobile", String.class, false))%>" autocomplete="off" />
                <label><span>Confirm Mobile Phone Number (for SMS)</span><span id="optional_marker_mobile_confirm" class="<%=(preferredCommType == 2 ? "hidden" : "optional") %>">Optional</span></label>
                <input type="tel" name="mobileConfirm" placeholder="Enter your 10-digit mobile phone number" title="Your 10-digit mobile phone number" pattern="\(?\s*\d{3}\s*\)?\s*[-.]?\s*\d{3}\s*[-.]?\s*\d{4}" <%if(preferredCommType == 2) {%>required="required" <%}%>value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "mobileConfirm", String.class, false))%>" autocomplete="off" />
                <label><span>Mobile Phone Carrier (for SMS)</span><span id="optional_marker_carrierId" class="<%=(preferredCommType == 2 ? "hidden" : "optional") %>">Optional</span></label>
                <select name="carrierId" title="Select your mobile phone carrier" <%if(preferredCommType == 2) {%>required="required" <%}%>>
                    <option value="">-- Please select --</option><%
while(carrierResults.next()) {
	Integer value = carrierResults.getValue("carrierId", Integer.class);
	%><option value="<%=value %>"<%if (value != null && carrierId != null && value.intValue() == carrierId.intValue()) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(carrierResults.getFormattedValue("carrierName")) %></option><%
}%>                    
                </select>
                <label><span>First Name</span></label>
                <input type="text" name="firstname" placeholder="Enter your first name" title="Your first name" required="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "firstname", String.class, false))%>"/>
                <label><span>Last Name</span></label>
                <input type="text" name="lastname" placeholder="Enter your last name" title="Your last name" required="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "lastname", String.class, false))%>"/>
				<label><span>Country</span></label>
                <select name="country" title="Enter your country">
                <% String country = RequestUtils.getAttribute(request, "country", String.class, false); 
                for(GenerateUtils.Country c : GenerateUtils.getCountries()) {%>
                    <option value="<%=StringUtils.prepareCDATA(c.code)%>" onselect="updatePostalPrompts('<%=("US".equalsIgnoreCase(c.code) ? "zip code" : "postal code") %>', '<%=StringUtils.prepareCDATA(StringUtils.prepareScript(c.postalRegex))%>', '<%=("US".equalsIgnoreCase(c.code) ? "Zip code" : "Postal code")%>');onBillingAddressIsTheSame();" <%
                    if(c.code.equalsIgnoreCase(country)) { %> selected="selected"<%
                    }%>><%=StringUtils.prepareHTML(c.name)%></option><%
                 }%></select>
                <label><span>Primary Location Street Address</span></label>
                <input type="text" name="address1" onchange="onBillingAddressIsTheSame();" placeholder="Enter your primary street address" title="Your street address" required="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address1", String.class, false))%>"/>
                <label><span id="postalCodeLabelHome">Primary Location Postal Code</span></label>
                <input type="text" name="postal" onchange="onBillingAddressIsTheSame();" placeholder="Enter your postal code" required="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "postal", String.class, false))%>"/>
                   <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
                   <% if(cardType==2){ %>
                   <div class="control-group">
                       <label class="control-label">Replenish&nbsp;Setting:</label>
                       <div class="controls">
                           <label class="radio inline">
                               <input type="radio" name="replenishType" value="1" onclick="replenishTypeChecked(this)"<%if(replenishType == 1) {%> checked="checked"<%}%> /> Auto
                           </label>
                           <label class="radio inline">
                               <input type="radio" name="replenishType" value="2" onclick="replenishTypeChecked(this)"<%if(replenishType == 2) {%> checked="checked"<%}%> /> Manual
                           </label> 
                           <label class="radio inline">
                               <input type="radio" name="replenishType" value="0" onclick="replenishTypeChecked(this)"<%if(replenishType == 0) {%> checked="checked"<%}%> /> None
                           </label>
                       </div>
                   </div>
                   <%
                   String replenishmentBonusNote = translator.translate("prepaid-replenishment-note", "");
                   if (!StringUtils.isBlank(replenishmentBonusNote)) {%>
					<div>
	                       <span class="replenish-note-signup">
	               			<span class="replenish-note-signup-inner">
	               			<%=replenishmentBonusNote%>
	               			</span>
	               		</span>
					</div>
					<%} %>
                   <div class="control-group">
                       <label class="control-label">Replenish by:</label>
                       <div class="input-prepend controls">
                           <span class="add-on">$</span><%
						BigDecimal[] replenishAmounts = PrepaidUtils.getReplenishAmounts();
						if(replenishAmounts == null || replenishAmounts.length == 0) {
						%><input type="number" name="amount" min="<%=PrepaidUtils.getMinReplenishAmount() %>" required="required" placeholder="10.00" value="" title="The amount that is pulled from your credit card to replenish your prepaid account"/><%
						} else { 
						%><select name="amount" required="required" title="The amount that is pulled from your credit card to replenish your prepaid account">
						<option value="">--</option><%
						for(BigDecimal value : replenishAmounts) {
						    %><option value="<%=value%>"<%if (value.compareTo(amount) == 0) {%> selected="selected"<%}%>><%=ConvertUtils.formatObject(value, "NUMBER:#,##0.00", locale) %></option><%
						}%>
						</select><%} %>
                       </div>
                   </div>
                   <div class="control-group">
                       <label class="control-label">When balance falls below:</label>
                       <div class="input-prepend controls">
                           <span class="add-on">$</span><%
						BigDecimal[] threshholdAmounts = PrepaidUtils.getThreshholdAmounts();
						if(threshholdAmounts == null || threshholdAmounts.length == 0) {
						%><input type="number" name="threshhold" min="<%=PrepaidUtils.getMinThreshholdAmount() %>" required="required" placeholder="5.00" value="" title="The minimum balance on your prepaid account before we replenish from your credit card"/><%
						} else {
						%><select name="threshhold" required="required" title="The minimum balance on your prepaid account before we replenish from your credit card">
						<option value="">--</option><%
						for(BigDecimal value : threshholdAmounts) {
						    %><option value="<%=value%>"<%if (value.compareTo(threshhold) == 0) {%> selected="selected"<%}%>><%=ConvertUtils.formatObject(value, "NUMBER:#,##0.00", locale) %></option><%
						}%>
						</select><%} %>
                       </div>
                   </div>
				<div id="creditCardDetails">
					<div class="more-accordion-inner">
					<div class="control-group">
					   <label class="control-label">Bank Credit Card Number:</label>
					   <div class="controls"><input type="text" name="replenishCardNum" placeholder="Enter credit card number" maxlength="16" required="required" pattern="\d{15,16}" autocomplete="off" value="<%=StringUtils.prepareCDATA(form.getStringSafely("replenishCardNum", "")) %>" title="The credit card number without spaces or punctuation. Usually 15 or 16 digits." /></div></div>
					   <div class="control-group"><label class="control-label">Expiration Date:</label>
					   <div class="controls"><select name="replenishExpMonth" class="autosize" required="required"><%
					int month = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "replenishExpMonth", false), 0);
					int year = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "replenishExpYear", false), 0);
					Calendar now = Calendar.getInstance();
					int currYear = now.get(Calendar.YEAR);%>
					<option value=""<%if(month < 1 || month > 12) {%> selected="selected"<%} %>>--</option><%
					for(int m = 1; m <= 12; m++) {%>
					    <option<%if(month == m) {%> selected="selected"<%} %> value="<%=m%>"><%=StringUtils.pad(Integer.toString(m), '0', 2, StringUtils.Justification.RIGHT)%></option><%
					}%></select>
					<select name="replenishExpYear" class="autosize" required="required">
					<option value=""<%if(year < currYear) {%> selected="selected"<%} %>>--</option><%
					for(int y = currYear; y <= currYear + 10; y++) {%>
					    <option<%if(year == y) {%> selected="selected"<%} %> value="<%=y%>"><%=y%></option><%
					}%></select></div></div>
					<div class="control-group"><label class="control-label">Security Code:</label>
					    <div class="controls"><input type="password" name="cvv" maxlength="4" required="required" pattern="\d{3,4}" placeholder="Enter 3 or 4 digit code" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "cvv", String.class, false))%>" title="The 3 or 4 digit security code found on the back of your credit card"/></div></div>
					<% }//end of cardType==2 %>
					<div class="control-group">
						<input id="isBillingAddressTheSame" type="checkbox" onclick="onBillingAddressIsTheSame();" />
          				Billing address is the same as home address.
					    <label class="control-label">Billing Country:</label>
					    <div class="controls">
					    <select name="billingCountry" title="Enter the country of your credit card billing address" required="required">
					    <% 
					    String billingCountry = RequestUtils.getAttribute(request, "billingCountry", String.class, false);
					    for(GenerateUtils.Country c : GenerateUtils.getCountries()) {%>
					        <option value="<%=StringUtils.prepareCDATA(c.code)%>" onselect="updatePostalPrompts('<%=("US".equalsIgnoreCase(c.code) ? "zip code" : "postal code") %>', '<%=StringUtils.prepareCDATA(StringUtils.prepareScript(c.postalRegex))%>', '<%=("US".equalsIgnoreCase(c.code) ? "Zip code" : "Postal code")%>:', $('signup-form').billingPostal);" <%
					        if(c.code.equalsIgnoreCase(billingCountry)) { %> selected="selected"<%
					        }%>><%=StringUtils.prepareHTML(c.name)%></option><%
					     }%></select>
					     </div>
					</div>
					<div class="control-group">
					    <label class="control-label">Billing Address:</label>
					    <div class="controls">
					        <input type="text" name="billingAddress1" placeholder="Enter your billing street address" title="Enter the street address of your credit card billing address" required="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingAddress1", String.class, false))%>"/>
					    </div>
					</div>
					<div class="control-group">
					    <label class="control-label" id="postalCodeLabelBilling">Billing Postal Code:</label>
					    <div class="controls">
					        <input type="text" name="billingPostal" placeholder="Enter your billing postal code" required="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingPostal", String.class, false))%>" title="Enter the postal code of your credit card billing address"/>
					    </div>
					</div>
				<% if(cardType==2){ %>
				</div>
				</div>
				<%}%>
				<% if((cardType==2)||(cardType==4)){ %>
				<div id="regionDetails"<%if(regionResults == null || regionResults.isGroupEnding(0)) { 
				%> <%} 
				Long selectedRegionId = RequestUtils.getAttribute(request, "region", Long.class, false); %>>
				<label><span>Region</span></label>
                <select name="region" title="Please select the most appropriate option" required="required">
                    <option value="">--</option><%
               		if(regionResults != null)
               			while(regionResults.next()) {
               			long regionId = regionResults.getValue("regionId", Long.class); %>
                        <option value="<%=regionId %>"<%if(selectedRegionId != null && selectedRegionId == regionId) { %> selected="selected"<%} %>><%=StringUtils.prepareHTML(regionResults.getValue("regionName", String.class)) %></option><%
                    } %>                    
                </select>				
                </div>
                <%} %>
                <hr/>             
                <label><h4>Create Password</h4></label><span style="font-weight: 400;">Password must be 8 characters or more and contain 1 uppercase letter, 1 lowercase letter, and a number or punctuation symbol.</span>
                <input type="password" name="newpassword" placeholder="Create password" title="A password with at least 8 characters and 1 uppercase letter, 1 lowercase letter, and a number or punctuation symbol" required="required" pattern=".{8,}" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "newpassword", String.class, false))%>"/>
                <label><span>Type Password Again</span></label>
                <input type="password" name="newconfirm" placeholder="Reenter your password" title="Must match the password entered above" required="required" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "newconfirm", String.class, false))%>"/>
<%/*
Results results = DataLayerMgr.executeQuery("GET_CONSUMER_SETTINGS", null);
results.addGroup("group");
DefaultParamRenderer renderer = new DefaultParamRenderer();
renderer.setBeforeControl("");
renderer.setAfterControl("</label>");
renderer.setBeforeLabel("<label class=\"checkbox\">");
renderer.setAfterLabel("");
String group = null;
while(results.next()) {
    if(results.isGroupBeginning("group")) {
    	%><hr/><%
        group = results.getFormattedValue("group");
        if(!StringUtils.isBlank(group)) {
            %><label><h4><%=StringUtils.prepareHTML(group) %></h4></label><%
        }
    }
    int settingId = results.getValue("settingId", Integer.class);
    renderer.render(out, "setting_" + settingId, results.getValue("editor", ParamEditor.class), results.getFormattedValue("label"), RequestUtils.getAttribute(request, "setting_" + settingId, String.class, false));
    %><input type="hidden" name="settings" value="<%=settingId %>"/><%
} */%><hr/>
                <p style="margin-top: 1.5em;">By clicking the "Create Account" button below, you signify that you agree to the <%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%> <a href="<%=termsConditionPage%>"> Terms and Conditions</a> and <a href="<%=privacyPage%>">Privacy Policy</a>.</p>                
                <div class="text-center">
                    <button type="submit" class="btn btn-large btn-submit">Create Account</button>
                </div>
            </fieldset>
        </form>                
    </div> <!-- /signup-form -->
    <p>Already have an account? <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signin.html") %>">Sign In</a>.</p>
</div>  <!-- /form-wrap -->
<hr/>
<script type="text/javascript" defer="defer">
function updatePostalPrompts(desc, pattern, label, postalEl) {
    if(!postalEl){
        postalEl = $("signup-form").postal;
        $("postalCodeLabelHome").set("html", 'Primary Location '+label);
    }else{
    	$("postalCodeLabelBilling").set("html", label);
    }
    postalEl.placeholder = "Enter your " + desc; 
    postalEl.title = "Your " + desc; 
    postalEl.pattern = pattern;
    
}

function replenishTypeChecked(chk) {
	if (chk.value == 0) {
		chk.form.amount.disabled = true;
		chk.form.threshhold.disabled = true;		
		chk.form.replenishCardNum.disabled = true;
		chk.form.replenishExpMonth.disabled = true;
		chk.form.replenishExpYear.disabled = true;
		chk.form.cvv.disabled = true;
		chk.form.billingCountry.disabled = true;
		chk.form.billingAddress1.disabled = true;
		chk.form.billingPostal.disabled = true;
		creditCardDetailsFx.dissolve();
	} else {
		chk.form.amount.disabled = false;
		chk.form.threshhold.disabled = (chk.value == 2);
		chk.form.replenishCardNum.disabled = false;
		chk.form.replenishExpMonth.disabled = false;
		chk.form.replenishExpYear.disabled = false;
		chk.form.cvv.disabled = false;
		chk.form.billingCountry.disabled = false;
		chk.form.billingAddress1.disabled = false;
		chk.form.billingPostal.disabled = false;
		creditCardDetailsFx.reveal();  
	}
}

function preferredCommTypeChecked(chk) {
    if (chk.value == 1) {
        chk.form.email.required = true;
        chk.form.emailConfirm.required = true;
        chk.form.mobile.required = false;
        chk.form.mobileConfirm.required = false;
        chk.form.carrierId.required = false; 
       $("optional_marker_email").set("class", "hidden");
       $("optional_marker_email_confirm").set("class", "hidden");
       $("optional_marker_mobile").set("class", "optional");
       $("optional_marker_mobile_confirm").set("class", "optional");
       $("optional_marker_carrierId").set("class", "optional");
    } else {
    	chk.form.email.required = false;
    	chk.form.emailConfirm.required = false;
        chk.form.mobile.required = true;
        chk.form.mobileConfirm.required = true;
        chk.form.carrierId.required = true; 
        $("optional_marker_email").set("class", "optional");
        $("optional_marker_email_confirm").set("class", "optional");
        $("optional_marker_mobile").set("class", "hidden");
        $("optional_marker_mobile_confirm").set("class", "hidden");
        $("optional_marker_carrierId").set("class", "hidden");
    }
}

var creditCardDetailsFx;
var regionsRequest;
window.addEvent("domready", function() {
   creditCardDetailsFx = new Fx.Reveal($("creditCardDetails"));
   Array.each($("signup-form").replenishType, function(item) { if(item.checked) replenishTypeChecked(item);});
   regionsRequest = new Request.HTML({
	   url: "regions_by_card.html",
	   update: $("signup-form").region,
	   link: "cancel"
   });
   window.regionDetailsFx = new Fx.Reveal($("regionDetails"));
});

function cardNumChange() {
	regionsRequest.send({data: $("signup-form")});
}

function onBillingAddressIsTheSame(){
	if ($("isBillingAddressTheSame").checked) {
		var country=$('signup-form').country.value;
		$('signup-form').billingCountry.value=country;
		$('signup-form').billingAddress1.value =$('signup-form').address1.value; 
		$('signup-form').billingPostal.value = $('signup-form').postal.value; 
	}
}
</script>
<jsp:include page="/footer.jsp"/>    