<%@page import="com.usatech.ps.PSResponse"%>
<%@page import="com.usatech.prepaid.PSRequestHandler"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="simple.servlet.RequestUtils"%>
<%! Pattern removeAddrPattern = Pattern.compile("^10\\.0\\.0\\.\\d+|192\\.168\\.\\d+\\.\\d+$"); %>
<%
if(!removeAddrPattern.matcher(request.getRemoteAddr()).matches()) {
    response.sendError(403);
    return;
}
String operation = RequestUtils.getAttribute(request, "operation", String.class, false);
if(StringUtils.isBlank(operation)) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-test", "This is a test of the message system");
	%>
	<jsp:forward page="dashboard.html.jsp"/><%
} else if("test_complete_reg".equalsIgnoreCase(operation)) {
	Translator translator = RequestUtils.getTranslator(request);
	Connection conn = DataLayerMgr.getConnectionForCall("CREATE_USER");
	try {
	    PrepaidUtils.sendCompleteRegistrationEmail(conn, "bkrug@usatech.com", 1, "First", "Last", 44211, "ABCDEFG", RequestUtils.getBaseUrl(request), request);
	    conn.commit();
	} finally {
		conn.close();
	}
    %>Email has been sent.<%
} else if ("validate_user".equalsIgnoreCase(operation)) {
	PSResponse psResponse = new PSResponse();
	PSRequestHandler.validateConsumerTypePrepaid(request, psResponse, 322048, "USD");
} else {
    %>ERROR: Unknown operation '<%=StringUtils.prepareHTML(operation) %>'.<%
}
%>