<%@page import="simple.servlet.SimpleServlet,simple.servlet.InputForm,simple.servlet.RequestUtils,simple.text.StringUtils"%>
<%
InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String username = RequestUtils.getAttribute(request, "username", String.class, false);
if(StringUtils.isBlank(username)) {
    Cookie cookie = form.getCookie("username");
    if(cookie != null)
        username = cookie.getValue();
}
RequestUtils.setAttribute(request, "subtitle", "Forgot Password", "request");
RequestUtils.setAttribute(request, "wrapNarrow", true, "request"); 
%>
<jsp:include page="/header.jsp"/>
<script type="text/javascript" defer="defer">
function preferredCommTypeChecked(chk) {
    if (chk.value == 1) {
        chk.form.username.removeAttribute("pattern");
        chk.form.username.type = "email";
        chk.form.username.placeholder = "Enter your email address";
        chk.form.username.title = "Your email address";
    } else {
    	chk.form.username.type = "tel";
        chk.form.username.pattern = "\\(?\\s*\\d{3}\\s*\\)?\\s*[-.]?\\s*\\d{3}\\s*[-.]?\\s*\\d{4}";
        chk.form.username.placeholder = "Your mobile phone number";
    }
}
</script>
<div class="form-wrap">
    <h1>Forgot password</h1>
    <div class="signin-form">
        <form name="info" method="post" action="reset_password.html" autocomplete="off">
            <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.newSessionToken(request))%>"/>
            <fieldset>
	            <label class="radio inline"><input type="radio" name="preferredCommType" value="1" onclick="preferredCommTypeChecked(this);" checked=true>Email Address</label>
	            <label class="radio inline"><input type="radio" name="preferredCommType" value="2" onclick="preferredCommTypeChecked(this);">Mobile Phone Number</label>
	            <input type="email" placeholder="Enter your email address" title="Your email address" name="username" value="" required="required" autofocus="autofocus"/>
             <div style="text-align: center;">
                    <button type="submit" class="btn btn-large btn-submit">Reset Password</button>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<jsp:include page="/footer.jsp"/>
