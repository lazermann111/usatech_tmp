<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.steps.LoginFailureException"%>
<%@page import="simple.results.BeanException"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.io.Log"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>
<%@page import="javax.crypto.SecretKey"%>
<%@page import="simple.security.crypt.CryptUtils"%>
<%@page import="simple.io.Base64"%>
<%@page import="simple.io.ByteArrayUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<%
long current = System.currentTimeMillis();
long passcodeId = RequestUtils.getAttribute(request, "sid", Long.class, true);
String encryptedBase64 = RequestUtils.getAttribute(request, "skey", String.class, true);
Map<String,Object> params = new HashMap<>();
params.put("passcodeId", passcodeId);
DataLayerMgr.selectInto("GET_LOGIN_AS_PASSCODE_BY_ID", params);
long checkConsumerId = ConvertUtils.convertRequired(Long.class, params.get("consumerId"));
String passcodeBase64 = ConvertUtils.convertRequired(String.class, params.get("passcode"));
SecretKey key = new SecretKeySpec(Base64.decode(passcodeBase64), "AES");
byte[] decrypted = CryptUtils.decrypt("AES", key, Base64.decode(encryptedBase64)); // may need to set ivspec
long time = ByteArrayUtils.readLong(decrypted, 8);
long consumerId = ByteArrayUtils.readLong(decrypted, 16);
long usaliveUserId = ByteArrayUtils.readLong(decrypted, 24);
if(checkConsumerId != consumerId) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-signinas-mismatched-consumer-id", "Your request is invalid. Please try again.");
    %><jsp:include page="/header.jsp"/><jsp:include page="/footer.jsp"/><%
	return;
}
if(Math.abs(time - current) > 30000) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-signinas-mismatched-time", "Your request has expired. Please try again.");
    %><jsp:include page="/header.jsp"/><jsp:include page="/footer.jsp"/><%
    return;
}
log.info("USALive User " + usaliveUserId + " is logging in as consumer " + consumerId);
Results results = DataLayerMgr.executeQuery("GET_USER_BY_CONSUMER_ID", params, false);
if(results.next()) {
    //ok, now create the user
    try {
    	PrepaidUser user = new PrepaidUser();
        results.fillBean(user);  
        request.getSession().setAttribute("remoteUser", user.getUserName());
        request.getSession().setAttribute(SimpleServlet.ATTRIBUTE_USER, user);       
    } catch (BeanException e) {
        //log.warn("Could not create user of class '" + getUserClass().getName() + "'", e);
        throw new ServletException("Could not create user of class '" + PrepaidUser.class.getName() + "'", e);
    }
} else { // user doesn't exist!
    throw new LoginFailureException("User does not exist", LoginFailureException.INVALID_USERNAME);
}  
response.sendRedirect("dashboard.html");
%>