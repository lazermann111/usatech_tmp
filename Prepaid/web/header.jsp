<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.xml.sax.MessagesInputSource,simple.servlet.RequestUtils,simple.text.StringUtils,java.util.Locale,simple.translator.Translator,com.usatech.prepaid.web.PrepaidUser,simple.servlet.SimpleServlet,java.util.List"%>
<%@page import="simple.results.Results"%>

<% 
Translator translator = RequestUtils.getTranslator(request); 
String title = translator.translate("prepaid-title", "more.");
PrepaidUser user = (PrepaidUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
%><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="<%=StringUtils.prepareCDATA(response.getCharacterEncoding())%>">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Cache-Control" content="no-cache" />
		<meta http-equiv="Pragma" content="no-cache" />
	    <title><%=StringUtils.prepareHTML(title)%><%
String subtitle = RequestUtils.getAttribute(request, "subtitle", String.class, false); 
if(!StringUtils.isBlank(subtitle)) {
	%> <%=StringUtils.prepareHTML(subtitle) %><%
}%></title>
        <base href="<%=StringUtils.encodeForHTMLAttribute(RequestUtils.getBaseUrl(request)) %>" />
        <link href="<%=RequestUtils.addLastModifiedToUri(request, null, "/css/normalize.css") %>" type="text/css" rel="stylesheet"/>
        <link href="<%=RequestUtils.addLastModifiedToUri(request, null, "/css/main.css") %>" type="text/css" rel="stylesheet"/>
        <link href="<%=ToJspSimpleServlet.untranslatePath(request, "/extra_style._")%>" type="text/css" rel="stylesheet"/>
        <%
String[] stylesheets = StringUtils.split(RequestUtils.getAttribute(request, "extra-stylesheets", String.class, false), StringUtils.STANDARD_DELIMS, false);
if(stylesheets != null)
	for(String ss : stylesheets) {
	    if(!StringUtils.isBlank(ss)) {
	        %><link href="<%=StringUtils.prepareCDATA(RequestUtils.addLastModifiedToUri(request, null, ss.trim())) %>" type="text/css" rel="stylesheet"/><%
	    }
	}
boolean wrapA = Boolean.TRUE.equals(RequestUtils.getAttribute(request, "wrapNarrow", Boolean.class, false)); 
boolean jstimings = Boolean.TRUE.equals(RequestUtils.getAttribute(request, "_jstimings", Boolean.class, false)); 
if(jstimings) {%><script type="text/javascript">var timings = []; 
timings[timings.length] = +(new Date);</script><%}
%>
		<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/modernizr-2.6.2-custom-46341.min.js")%>"></script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date);</script><%}%>
		<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/mootools-core-1.4.5-full-nocompat.js")%>"></script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date);</script><%}%>
		<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/mootools-more-1.4.0.1-selected.js")%>"></script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date);</script><%}%>
        <script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/mootools-bootstrap-plus.js")%>"></script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date);</script><%}%>
        <script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "/scripts/apply.js")%>"></script>        
        <script type="text/javascript"><%
Locale locale = RequestUtils.getLocale(request);
if(locale != null) {%>          Locale.use("<%=StringUtils.prepareCDATA(StringUtils.prepareScript(locale.toString()))%>");<%}
//GenerateUtils.writeCountriesConfigScript(translator, out, "\t\t\t", SystemUtils.getNewLine());
%>
if(top.frames.length != 0)
    top.location = self.document.location;
</script>
<% if(jstimings) {%><script type="text/javascript">timings[timings.length] = +(new Date); 
var s = ""; 
for(var i = 0; i < timings.length - 1; i++) { 
	s += "From #" + i + " to #" + (i+1) + ": " + (timings[i+1] - timings[i]) + "\n"; 
}
alert(s);
</script><%}%>
    </head>
    <body>
    	<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
<%
if(user != null) {%>
        <div class="topbar topbar-blue">
	        <header class="wrap-b">
				<a href="<%=ToJspSimpleServlet.untranslatePath(request, "/dashboard.html") %>">
				<%if(user!=null&&StringUtils.isBlank(user.getBrandingSubdirectory())&&user.getCompanyLogoFileName()!=null){ %>
					<img height="50" onerror='this.style.display = "none"' src="<%=StringUtils.prepareCDATA(PrepaidUtils.getUsaliveBaseUrl()+"get_company_logo.i?customerId="+user.getCustomerId())%>" alt="m." />
				<%}else{ %>
					<img src="<%=ToJspSimpleServlet.untranslatePath(request, "/top_logo._")%>" alt="m." />
				<%} %>
				</a>          
				<ul class="nav more-nav1 nav-pills pull-right">
					<li><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/dashboard.html") %>">Account Home</a></li>
					<li><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/locations.html?discountsOnly=true") %>">Locations</a></li>
					<li><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/support.html") %>">Support</a></li>
				</ul>
	        </header>
        </div> <!-- /topbar  -->
      
        <div class="wrap-<%=wrapA?'a':'b'%>">      
	        <div class="signedin-bar">
              <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signout.html") %>" class="btn btn-signout pull-right" title="Sign out of <%=title%>">Sign Out</a>
	          <span class="signedin-message"><%
	          String welcome = translator.translate("prepaid-welcome", "Signed in as {0}", user.getFullName()); 
              if(!StringUtils.isBlank(welcome)) { out.write(welcome); }
              %>
              </span>	
              <br/>   
              <%if(!user.hasPrivilege("REGISTERED")&& !StringUtils.isBlank(user.getUserName())){
            	  String notRegisteredMessage=translator.translate("prepaid-not-registered", "You have not verified your communication method, please verify");
              %>
              <%=notRegisteredMessage%> <a href="verify_user.html?userId=<%=user.getConsumerId()%>">here</a> or <a href="resend_registration.html?userId=<%=user.getConsumerId()%>">Resend verification</a>
              <%} %>  
              <div class="clearfix"></div>
	        </div>  <!-- /signedin-bar  -->
	    
	    <%
	    Results results = DataLayerMgr.executeQuery("GET_FAILED_REPLENISH_INFO", user);
		results.setLocale(locale);
		while (results.next()){%>
		<div class="alert alert-replen-failed">You have failed replenish for prepaid card <%= results.getFormattedValue("consumerCard")%> with credit card <%= results.getFormattedValue("replenConsumerCard")%>.</div>
		<%}%>
	   
<%} else { 
    Boolean signUp = RequestUtils.getAttribute(request, "signUpLink", Boolean.class, false); 
	if (StringUtils.isBlank(RequestUtils.getAttribute(request, "survey_device", String.class, false))) {
	%>
	<div class="topbar topbar-gray">
		<header class="wrap-a">
		<a href="<%=ToJspSimpleServlet.untranslatePath(request, "/index.html") %>"><img src="<%=ToJspSimpleServlet.untranslatePath(request, "/top_logo_welcome._")%>" alt="m." /></a>
	 	<ul class="nav more-nav1 nav-pills pull-right">          
	    <li><%if(signUp != null && signUp) {%><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signup_question.html") %>">Sign up</a><%} else {%><li> <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signin.html") %>">Sign in</a><% } %></li>
	       <li><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/support.html") %>">Support</a></li>
	    </ul>
	    </header>
	</div> <!-- /topbar  -->          
	<%} else {%>
	<div align="center" class="topbar topbar-gray">
	   	<header class="wrap-a">
	   	<a href="<%=ToJspSimpleServlet.untranslatePath(request, "/index.html") %>"><img src="<%=ToJspSimpleServlet.untranslatePath(request, "/top_logo_welcome._")%>" alt="m." /></a>
	    <span class="more-survey-header">more</span> <span class="survey-header"> feedback</span>
	    </header>
	</div> <!-- /topbar  -->
	<%}%>      
      
      <div class="wrap-<%=wrapA?'a':'b'%>">
<%}
List<MessagesInputSource.Message> messages = RequestUtils.getMessages(request);
if(!messages.isEmpty()) {%>
            <br/><%
    for(MessagesInputSource.Message m : messages) {
        %><div class="alert alert-<%=StringUtils.prepareCDATA(m.getType())%>"><%=m.toHtml()%></div><%
    }%><%
}
%>

