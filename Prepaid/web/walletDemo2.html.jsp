<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>more. - Account Home</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link  rel='stylesheet' href='/democss/fonts.css'>

        <link rel="stylesheet" href="/democss/normalize.css">
        <link rel="stylesheet" href="/democss/main.css">
        <script src="/demojs/vendor/modernizr-2.6.2.min.js"></script>
	<style>
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
html, body {
  height: 100%;
}

body {
  font: 16px/1 'Open Sans', sans-serif;
  color: #222222;
  background: #ffffff;
}
	/* =================================================
   For tab
   ================================================= */

#topnavtabsdiv {
  margin:  0 auto;

}
/* Style the list */
ul.topnavtabs {
    list-style-type: none;
    padding: 0 5%;
    overflow: hidden;
}

/* Float the list items side by side */
ul.topnavtabs li {float: left;
border-bottom:  solid 1px #ddd;
}

/* Style the links inside the list items */
ul.topnavtabs li a {
     text-align: center;
    text-decoration: none;
    transition: 0.3s;
      display: inline-block;
  margin: 0;
  padding: 15px 25px;
  font-weight: 600;
  text-align: center;
  color: #888;
  border:  solid 1px transparent;
  border-top: solid 6px transparent;
}

/* Change background color of links on hover */
ul.topnavtabs li a:hover {
  color: #888;
  cursor: pointer;
}

/* Create an active/current tablink class */
ul.topnavtabs li a:focus {
  background-color: #fff;
  color: #555;
  border: 1px solid #ddd;
  border-top: solid 6px #588fcc;
  border-bottom:  solid 1px #fff;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 0;
    height:  100%;
}
	</style>
	<script>
	function openTab(pageNum){
		if(pageNum==1){
			window.location.href="walletDemo.html";
		}else if(pageNum==3){
			window.location.href="walletDemo3.html";
		}
	}
	</script>
    </head>
    <body >
<div id="topnavtabsdiv">
<form id="navtabsform">
<ul class="topnavtabs">
  <li><a href="javascript:void(0)" onclick="openTab(1)">Add MORE card to mobile wallet</a></li>
  <li style="border-bottom:  none;"><a href="javascript:void(0)" style="background-color: #fff;
  color: #555;
  border: 1px solid #ddd;
  border-top: solid 6px #588fcc;
  border-bottom:  none;" onclick="openTab(2)">MORE account demo</a></li>
  <li><a href="javascript:void(0)" onclick="openTab(3)">Demo</a></li>
</ul>
</form>
</div>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

      <div class="topbar topbar">
        <header class="wrap-b account-header">
          <a href="index.html"><img  style="margin-top: 10px;" src="/images/icon-logo_more.png" alt="m." /></a>

            <!-- <ul class="nav more-nav1 nav-pills pull-right">
          <li><a href="accounthome.html">Account Home</a></li>
            <li><a href="locations.html">Locations</a></li>
            <li><a href="support.html">Support</a></li>
          </ul> -->
        </header>
      </div> <!-- /topbar  -->

      <div class="wrap-b account-container">

        <div class="signedin-bar">
          <span class="signedin-message">Signed in as Jules Smyth.</span>

          <!-- <button type=" " class="btn btn-signout pull-right">Sign Out</button>  -->
        </div>  <!-- /signedin-bar  -->

         <div class="account-content">


          <!-- tabs  -->
          <ul class="nav nav-tabs" id="accountTab">
            <li class="active"><a href="#dashboard" data-toggle="tab">Dashboard</a></li>
            <!-- <li><a class="manage-cards-tabnav-full"href="#cards" data-toggle="tab">Manage Cards</a><a class="manage-cards-tabnav-short" href="#cards" data-toggle="tab">Cards</a></li>  -->
            <li><a class="account-settings-tabnav-full" href="#settings" data-toggle="tab">Account Settings</a><a class="account-settings-tabnav-short" href="#settings" data-toggle="tab">Settings</a></li>
         </ul>

          <div class="tab-content">

            <div id="dashboard" class="tab-pane active">

            <!-- THE DASHBOARD PANE -->

              <div class="transactions-wrap">

              <div class="account-table-header">

              <form class="transactions-card-select form-inline"><strong>Transactions for</strong>
                <select>
                  <option>Card 1: 1234567890</option>
                  <option>Card 2: 0987654321</option>
                  <option>Card 3: 1029384756</option>
                  <option>All</option>
                </select>

              </form>


              </div>  <!-- /account-table-header -->

                <!-- big boxes  -->
                <div class="big-boxes">
                  <div class="current-balance">
        		    <p>MORE Money<br />
        		    <span class="big-number">$22.75</span>
        		    </p>
        		  </div>

                  <div class="current-reward">
                    <p>Lifetime Discount<br />
                    <span class="big-number">$11.50</span>
                    </p>
                  </div>
        		</div> <!-- /big-boxes  -->


              <div class="account-table-controls">

              <div class="date-range">
            Date range: <input data-toggle="calendar" data-format="mm/dd/yyyy" name="startTime" title="Enter the start date" value="" type="text">
                     to <input data-toggle="calendar" data-format="mm/dd/yyyy" name="endTime" title="Enter the end date" value="" type="text">
        </div>
                <!-- <div class="date-range">
                Date range: <a href="#">12/20/12</a> to <a href="#">5/10/13</a>
                </div>  -->

                <div class="page-controls">


                  <div class="per-page pull-left">Per page: 10 | <a href="#">20</a> | <a href="#">30</a></div>


                  <div class="page-number pull-right">Page: 1, <a href="#">2</a>, <a href="#">3</a>, <a href="#">4</a>, <a href="#">5</a>, <a href="#">6</a>, <a href="#">7</a>, <a href="#">8</a>, <a href="#">9</a>, <a href="#">10</a></div>
                </div>
                <div class="clearfix"></div>

              </div>  <!-- /account-table-controls  -->


<!-- TRANSACTIONS TABLE  -->

<table class="table table-striped table-bordered-squarecorners table-hover account-table" id="accountTable">
  <thead>
    <tr>
      <th class="th-date">Date</th>
      <!-- <th class="th-card">Card</th>  -->
      <th class="th-location">Location</th>
      <th class="th-amt">$ Retail</th>
      <th class="th-discount">$ Discount</th>
      <th class="th-discount">$ Purchase</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>12/22/16</td>
      <!-- <td>1</td>  -->
      <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $8.55<br />
      Bob's Gas<br />
      100 Main Street<br />
      Smalltown, PA 19349<br />
      610-555-5555" title="Transaction on 12/22/16 @ 4:15pm">Bob's Gas</a></td>
      <td>8.55</td>
      <td>-</td>
       <td>8.55</td>
    </tr>
     <tr>
      <td>11/28/16</td>
      <!-- <td>1</td>  -->
      <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $9.25<br />
      Oldtown Mall<br />
      Second floor, East<br />
      1 Mall Street<br />
      Oldtown, PA 19555<br />
      610-555-5555" title="Transaction on 11/28/16 @ 1:15pm">Oldtown Mall</a></td>
      <td>9.50</td>
      <td>(.25) loyalty</td>
       <td>9.25</td>
    </tr>
     <tr>
      <td>11/23/16</td>
      <!-- <td>1</td>  -->
      <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $1.40<br />
      BC Inc. lunchroom<br />
      16 Elm Street<br />
      Smalltown, PA 19349<br />
      610-555-5555" title="Transaction on 11/23/16 @ 12:15pm">BC Inc. lunchroom</a></td>
      <td>1.50</td>
      <td>(.10) loyalty</td>
       <td>1.40</td>
    </tr>
     <tr>
      <td>11/16/16</td>
      <!-- <td>1</td>  -->
      <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $2.80<br />
      BC Inc. lunchroom<br />
      16 Elm Street<br />
      Smalltown, PA 19349<br />
      610-555-5555" title="Transaction on 11/16/16 @ 3:35pm">BC Inc. lunchroom</a></td>
      <td>3.00</td>
      <td>(.20) loyalty</td>
       <td>2.80</td>
    </tr>
     <tr>
      <td>11/12/16</td>
      <!-- <td>1</td>  -->
      <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $3.25<br />
      Foodshopper Supermarket<br />
      6 Greenville Mall<br />
      Plain City, PA 19875<br />
      610-555-5555" title="Transaction on 11/12/16 @ 10:15am">Foodshopper Supermarket</a></td>
      <td>3.75</td>
      <td>(.50) discount</td>
       <td>3.25</td>
    </tr>
     <tr>
      <td>10/22/16</td>
      <!-- <td>1</td>  -->
      <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $6.50<br />
      Holly House Rest Area<br />
      95 South<br />
      Pendletown, MD 20002<br />
      201-555-5555" title="Transaction on 10/22/16 @ 3:15pm">Holly House Rest Area</a></td>
      <td>6.50</td>
      <td>-</td>
       <td>6.50</td>
    </tr>
    <tr>
      <td>10/16/16</td>
      <!-- <td>1</td>  -->
     <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $1.40<br />
      BC Inc. lunchroom<br />
      16 Elm Street<br />
      Smalltown, PA 19349<br />
      610-555-5555" title="Transaction on 10/16/16 @ 12:05pm">BC Inc. lunchroom</a></td>
      <td>1.50</td>
      <td>(.10) loyalty</td>
       <td>1.40</td>
    </tr>
     <tr>
      <td>10/12/16</td>
      <!-- <td>1</td>  -->
      <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $3.25<br />
      Foodshopper Supermarket<br />
      6 Greenville Mall<br />
      Plain City, PA 19875<br />
      610-555-5555" title="Transaction on 10/12/16 @ 10:15am">Foodshopper Supermarket</a></td>
      <td>3.75</td>
      <td>(.50) discount</td>
       <td>3.25</td>
    </tr>
     <tr>
      <td>10/10/16</td>
      <!-- <td>1</td>  -->
     <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $2.80<br />
      BC Inc. lunchroom<br />
      16 Elm Street<br />
      Smalltown, PA 19349<br />
      610-555-5555" title="Transaction on 10/10/16 @ 12:25pm">BC Inc. lunchroom</a></td>
      <td>3.00</td>
      <td>(.20) loyalty</td>
       <td>2.80</td>
    </tr>
    <tr>
      <td>10/6/16</td>
      <!-- <td>1</td>  -->
      <td><a href="#" data-toggle="popover" data-html="true" data-placement="top"
      data-content="
      ID: 0000000000000<br />
      $3.50<br />
      City Zoo<br />
      Reptile House<br />
      100 Tiger Way<br />
      Smalltown, PA 19000<br />
      610-555-5555" title="Transaction on 10/6/16 @ 2:35pm">City Zoo</a></td>
      <td>3.50</td>
      <td>-</td>
       <td>3.50</td>
    </tr>

  </tbody>
</table>

              <div class="account-table-controls">

                <div class="page-controls">
                  <div class="per-page pull-left">Per page: 10 | <a href="#">20</a> | <a href="#">30</a></div>
                  <div class="page-number pull-right">Page: 1, <a href="#">2</a>, <a href="#">3</a>, <a href="#">4</a>, <a href="#">5</a>, <a href="#">6</a>, <a href="#">7</a>, <a href="#">8</a>, <a href="#">9</a>, <a href="#">10</a></div>
                </div>
                <div class="clearfix"></div>

              </div>  <!-- /account-table-controls  -->



              </div>  <!-- /transactions-wrap -->


              <div class="promo-column">

                <p class="promo-announce">
                Offers for Jules Smyth</p>



                <div id="myCarousel" class="carousel slide">

                  <div class="promo-carousel-controls">

                    <a class="pull-left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="pull-right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                    <div class="clearfix"></div>

                  </div> <!-- /promo-carousel-controls -->


                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>
                  <!-- Carousel items -->
                  <div class="carousel-inner">
                    <div class="active item">

                  <div class="promo">
                  <p class="promo-terms">5% OFF SALE</span>
                  <p class="promo-details">Every purchase is 5% off this Sunday from 1pm to 4pm at all participating locations.</p>
                </div> <!-- /promo -->

                <div class="promo">
                  <p class="promo-terms">Presidents' DAy</span>
                  <p class="promo-details">All <strong>US COLA</strong> is discounted $.50 on Presidents' Day, February 20th at participating locations.</p>
                </div> <!-- /promo -->

                <div class="promo">
                  <p class="promo-terms">Oldtown Mall</span>
                  <p class="promo-details">All snacks from machines at Oldtown Mall on February 4th are 5% off at all participating locations.</p>
                </div> <!-- /promo -->

                <div class="promo">
                  <p class="promo-terms">$2 off an ultra wash</span>
                  <p class="promo-details">Receive $2 Off an Ultra Wash this weekend when you use your MORE rewards card.</p>
                </div> <!-- /promo -->



                    </div>
                    <div class="item">

                  <div class="promo">
                  <p class="promo-terms">5% OFF SALE</span>
                  <p class="promo-details">Every purchase is 5% off this Sunday from 1pm to 4pm at all participating locations.</p>
                </div> <!-- /promo -->

                <div class="promo">
                  <p class="promo-terms">Presidents' DAy</span>
                  <p class="promo-details">All <strong>US COLA</strong> is discounted $.50 on Presidents' Day, February 20th at participating locations.</p>
                </div> <!-- /promo -->

                <div class="promo">
                  <p class="promo-terms">Oldtown Mall</span>
                  <p class="promo-details">All snacks from machines at Oldtown Mall on February 4th are 5% off at all participating locations.</p>
                </div> <!-- /promo -->

                <div class="promo">
                  <p class="promo-terms">$2 off an ultra wash</span>
                  <p class="promo-details">Receive $2 Off an Ultra Wash this weekend when you use your MORE rewards card.</p>
                </div> <!-- /promo -->


                    </div>
                     <div class="item">

                  <div class="promo">
                  <p class="promo-terms">5% OFF SALE</span>
                  <p class="promo-details">Every purchase is 5% off this Sunday from 1pm to 4pm at all participating locations.</p>
                </div> <!-- /promo -->

                <div class="promo">
                  <p class="promo-terms">Presidents' DAy</span>
                  <p class="promo-details">All <strong>US COLA</strong> is discounted $.50 on Presidents' Day, February 20th at participating locations.</p>
                </div> <!-- /promo -->

                <div class="promo">
                  <p class="promo-terms">Oldtown Mall</span>
                  <p class="promo-details">All snacks from machines at Oldtown Mall on February 4th are 5% off at all participating locations.</p>
                </div> <!-- /promo -->

                <div class="promo">
                  <p class="promo-terms">$2 off an ultra wash</span>
                  <p class="promo-details">Receive $2 Off an Ultra Wash this weekend when you use your MORE rewards card.</p>
                </div> <!-- /promo -->


                    </div>








                  </div>


                  <div class="promo-carousel-controls">

                  <a class="pull-left" href="#myCarousel" data-slide="prev">&lsaquo;</a>




                  <a class="pull-right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                  <div class="clearfix"></div>

                  </div> <!-- /promo-carousel-controls -->



                </div>


             </div>  <!-- /promo-column -->

            </div>  <!-- /#dashboard  -->


            <!-- THE ACCOUNT SETTINGS PANE  -->

            <div class="tab-pane" id="settings">

            <div class="account-settings-wrap">


            <div class="row-fluid account-settings">
        <div class="span4">
            <h5>Account Info/Profile</h5>
                <input name="session-token" value="" type="hidden">
                <input name="oldUsername" value="jsmyth@mailinator.com" type="hidden">
                <input name="accountType" value="full" type="hidden">

                <div id="fullForm1">
                <div class="control-group">
                    <label class="control-label">Preferred Communication Method:</label>
                    <div class="controls">
                        <label class="radio inline">
                            <input name="preferredCommType" value="1" onclick="preferredCommTypeChecked(this)" checked="checked" type="radio"> Email
                        </label>
                        <label class="radio inline">
                            <input name="preferredCommType" value="2" onclick="preferredCommTypeChecked(this)" type="radio"> SMS (Text)
                        </label>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="inputEmail">Email: <span id="optional_marker_email" class="hidden">Optional</span></label>
                    <div class="controls">
                        <input name="email" placeholder="Enter your email address" title="Your email address" required="required" value="jsmyth@mailinator.com" type="email">
                    </div>
                    <label class="control-label" for="inputMobile">Mobile:<span id="optional_marker_mobile" class="optional">Optional</span></label>
                    <div class="controls">
                        <input name="mobile" placeholder="Enter your 10-digit mobile phone number" title="Your 10-digit mobile phone number" pattern="\(?\s*\d{3}\s*\)?\s*[-.]?\s*\d{3}\s*[-.]?\s*\d{4}" value="" type="tel">
                    </div>

                    <label><span>Mobile Phone Carrier:</span><span id="optional_marker_carrierId" class="optional">Optional</span></label>
                <select name="carrierId" title="Select your mobile phone carrier" style="font-size: 14px;">
                    <option value="">-- Please select --</option><option value="1">3 River Wireless</option><option value="2">ACS Wireless</option><option value="3">Alltel</option><option value="4">AT&amp;T</option><option value="5">Bell Canada</option><option value="6">Bell Mobility</option><option value="7">Blue Sky Frog</option><option value="8">Bluegrass Cellular</option><option value="9">Boost Mobile</option><option value="10">BPL Mobile</option><option value="11">Carolina West Wireless</option><option value="12">Cellular One</option><option value="13">Cellular South</option><option value="14">Centennial Wireless</option><option value="15">CenturyTel</option><option value="16">Cingular (Now AT&amp;T)</option><option value="17">Clearnet</option><option value="18">Comcast</option><option value="19">Corr Wireless Communications</option><option value="20">Dobson</option><option value="21">Edge Wireless</option><option value="22">Fido</option><option value="23">Golden Telecom</option><option value="24">Helio</option><option value="25">Houston Cellular</option><option value="26">Idea Cellular</option><option value="27">Illinois Valley Cellular</option><option value="28">Inland Cellular Telephone</option><option value="29">MCI</option><option value="32">Metro PCS</option><option value="30">Metrocall</option><option value="31">Metrocall 2-way</option><option value="33">Microcell</option><option value="34">Midwest Wireless</option><option value="35">Mobilcomm</option><option value="36">MTS</option><option value="37">Nextel</option><option value="38">OnlineBeep</option><option value="39">PCS One</option><option value="40">President's Choice</option><option value="41">Public Service Cellular</option><option value="42">Qwest</option><option value="43">Rogers AT&amp;T Wireless</option><option value="44">Rogers Canada</option><option value="45">Satellink</option><option value="56">Solo Mobile</option><option value="46">Southwestern Bell</option><option value="47">Sprint</option><option value="48">Sumcom</option><option value="49">Surewest Communicaitons</option><option value="50">T-Mobile</option><option value="51">Telus</option><option value="52">Tracfone</option><option value="53">Triton</option><option value="54">Unicel</option><option value="55">US Cellular</option><option value="57">US West</option><option value="58">Verizon</option><option value="59">Virgin Mobile</option><option value="60">Virgin Mobile Canada</option><option value="61">West Central Wireless</option><option value="62">Western Wireless</option>
                </select>
                </div>


                </div> <!--  end of fullForm1-->
                <div class="control-group">
                    <label class="control-label">First Name:</label>
                    <div class="controls">
                        <input name="firstname" placeholder="Enter your first name" title="Your first name" required="required" value="Jules" type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Last Name:</label>
                    <div class="controls">
                        <input name="lastname" placeholder="Enter your last name" title="Your last name" required="required" value="Smyth" type="text">
                    </div>
                </div>
                <div id="fullForm2">
                <div class="control-group">
                    <label class="control-label">Country:</label>
                    <div class="controls">
                  <select name="country" title="Your country" required="required">

                      <option value="US" onselect="updatePostalPrompts('5-digit zip code', '^([0-9]{5})[ -]?([0-9]{4})?$', 'Primary Location Zip Code:');" selected="selected">USA</option>
                      <option value="CA" onselect="updatePostalPrompts('postal code', '^([A-Z][0-9][A-Z]) ?([0-9][A-Z][0-9])$', 'Primary Location Postal Code:');">Canada</option>
                      <option value="BM" onselect="updatePostalPrompts('postal code', '^([A-Z]{2}) ?([0-9]{2})$', 'Primary Location Postal Code:');">Bermuda</option>
                      <option value="GU" onselect="updatePostalPrompts('postal code', '^([0-9]{5})$', 'Primary Location Postal Code:');">Guam</option></select>
                   </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Primary Location Street Address:</label>
                    <div class="controls">
                        <input name="address1" placeholder="Enter your street address" title="Your street address" required="required" value="1165 Coldwater Dr" type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" id="postalCodeLabel">Primary Location Zip Code:</label>
                    <div class="controls">
                        <input name="postal" placeholder="Enter your 5-digit zip code" required="required" value="19349" title="Your 5-digit zip code" pattern="^([0-9]{5})[ -]?([0-9]{4})?$" type="text">
                    </div>
                </div>
                </div><!--  end of fullForm2-->

                <!-- <div id="fullForm4">
                <div class="control-group">
                <label><span>Region</span></label>
                <select name="region" title="Please select the most appropriate option" required="required">
                    <option value="">--</option>
                        <option value="657">Chips</option>
                        <option value="296" selected="selected">Crackerville</option>
                </select>
                </div>
                </div>  -->  <!--  end of fullForm4-->

        </div>
        <!-- /span4  -->
        <div id="fullForm3">
        <div class="span4">
            <h5>Change Password</h5>
            <label class="control-label">Password must be 8 characters or more and contain 1 uppercase
                letter, 1 lowercase letter, and a number or punctuation symbol.</label>
                <div class="control-group">
                    <label class="control-label">New Password:</label>
                    <div class="controls">
                        <input autocomplete="off" name="newpassword" placeholder="Create password" title="A password with at least 8 characters and 1 uppercase letter, 1 lowercase letter, and a number or punctuation symbol" pattern=".{8,}" type="password">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Type New Password Again:</label>
                    <div class="controls">
                        <input autocomplete="off" name="newconfirm" placeholder="Reenter your password" title="Must match the password entered above" type="password">
                    </div>
                </div>
        </div>
        <!-- /span6  -->
            <div class="span4"><h5>Preferences</h5>

            <style>

input[type="checkbox"] {
    display:inline-block; vertical-align:  top;}
    </style>



    <label class="checkbox"><input type="checkbox"> This is the first checkbox</label>
   <label class="checkbox"><input type="checkbox"> Receive Promotions</label>
   <label class="checkbox"><input type="checkbox"> Contact Me When I Earn a Replenish Bonus</label>
<label class="checkbox"><input type="checkbox">Send Me Purchase Receipt</label>
<label class="checkbox"><input type="checkbox">Contact Me When A Replenishment Is Declined</label>
<label class="checkbox"><input type="checkbox">Contact Me When A Card Is Replenished</label>
<label class="control-label">Contact Me When My Balance Falls Below:</label>
<select name="setting_8" data-validators="required" style="font-size: 14px;">
    <option value="0" selected="selected">Do Not Send</option>
    <option value="1">$1</option>
    <option value="5">$5</option>
    <option value="10">$10</option>
    <option value="15">$15</option>
    <option value="20">$20</option>
    <option value="25">$25</option>
    <option value="30">$30</option>
    <option value="40">$40</option>
    <option value="50">$50</option>
    <option value="60">$60</option>
    <option value="70">$70</option>
    <option value="80">$80</option>
    <option value="90">$90</option></select>
<label class="checkbox"><input type="checkbox">Contact Me When Replenish Credit Card is about to expire</label>
<input name="settings" value="10" type="hidden">
            </div>
            <!-- /span4  -->
    </div>
    </div>




            </div>  <!-- /account-settings-wrap  -->

            <div class="form-actions text-center">
        <button type="submit" class="btn btn-savechanges">Save changes</button>
    </div>


        </div>  <!-- /tab-pane #settings -->

        </div> <!-- /tab content  -->

      </div> <!-- /account-content -->

      <footer class="footer">
        <p>&copy; USA Technologies 2017</p>
      </footer>


      </div>  <!-- /home-container -->
		<script src="/demojs/vendor/jquery-1.9.1.min.js"></script>
        <script src="/demojs/plugins.js"></script>
        <script src="/demojs/main.js"></script>

        <script src="/demojs/bootstrap.min.js"></script>

        <script>
        $('#accountTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
        })
        </script>


<script>
/*
        $("[data-toggle=popover]")
        .popover()
        .click(function(e) {
         e.preventDefault()
         }) */
        </script>

<script> /*
        $(document).on('click', function (e) {
    var
        $popover,
        $target = $(e.target);

    //do nothing if there was a click on popover content
    if ($target.hasClass('popover') || $target.closest('.popover').length) {
        return;
    }

    $('[data-toggle="popover"]').each(function () {
        $popover = $(this);

        if (!$popover.is(e.target) &&
            $popover.has(e.target).length === 0 &&
            $('.popover').has(e.target).length === 0)
        {
            $popover.popover('hide');
        } else {
            //fixes issue described above
            $popover.popover('toggle');
        }
    });
}) */
</script>

<script>
$('[data-toggle="popover"]').popover();

$('body').on('click', function (e) {
    //only buttons
    if ($(e.target).data('toggle') !== 'popover'
        && $(e.target).parents('.popover.in').length === 0) { 
        $('[data-toggle="popover"]').popover('hide');
    }
  //buttons and icons within buttons
    /*
    if ($(e.target).data('toggle') !== 'popover'
        && $(e.target).parents('[data-toggle="popover"]').length === 0
        && $(e.target).parents('.popover.in').length === 0) { 
        $('[data-toggle="popover"]').popover('hide');
    }
    */
});
</script>



        <script>
        $(document).ready(function(){
        $('.carousel').carousel({
        interval: 9000
        });
        });
        </script>




        <!-- code for popovers and adding click anywhere to close  -->

  <!--      <script>

     //     $('a[data-toggle=popover]')
     //     .popover();
//
  //          $(':not(#anywhereonthepage)').click(function(e)
  //
  //          {  e.preventDefault()
  //
  //            $('a[data-toggle=popover]').each(function ()
  //
   //           {
  //
  //
  //               if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('a[data-toggle=popover]').has(e.target).length === 0)
                   {
     //               $(this).popover('hide');
       //             return;
      //             }
     //          });
     //        });

        </script>  -->




        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->

    </body>
</html>

