<%@page import="com.usatech.ec2.EC2CardInfo"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="java.text.Format"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.lang.Holder"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="java.sql.SQLException,simple.text.StringUtils,simple.servlet.RequestUtils"%>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
String creditCardNum = RequestUtils.getAttribute(request, "creditCardNum", String.class, false);
String creditCardCvv = RequestUtils.getAttribute(request, "creditCardCvv", String.class, false);
Integer creditCardExpMonth = RequestUtils.getAttribute(request, "creditCardExpMonth", Integer.class, false);
Integer creditCardExpYear = RequestUtils.getAttribute(request, "creditCardExpYear", Integer.class, false);
String promoCode = RequestUtils.getAttribute(request, "promoCode", String.class, false);

if(StringUtils.isBlank(creditCardNum)||StringUtils.isBlank(creditCardCvv)||creditCardExpMonth==null||creditCardExpYear==null){
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
	RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    return;
}
Holder<String> creditCardNumHolder = new Holder<String>();
if(!PrepaidUtils.checkCardNum(creditCardNum, creditCardNumHolder, false)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-credit-card", "You entered an invalid credit card number. Please enter a valid one.");
    RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    return;
}
creditCardNum = creditCardNumHolder.getValue();
if(creditCardExpMonth > 12 || creditCardExpMonth < 1) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-month", "You specified an invalid month. Please enter a valid one.");
    RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    return;
}
Calendar now = Calendar.getInstance();
int currYear = now.get(Calendar.YEAR); 
int currMonth = now.get(Calendar.MONTH) + 1;
if(creditCardExpYear < currYear || (creditCardExpYear == currYear && creditCardExpMonth < currMonth)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-expired", "This card has already expired. Please use a different one.");
    RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    return;
}
Format expFormat = ConvertUtils.getFormat("NUMBER:00");
String expDate= expDate=expFormat.format(creditCardExpYear % 100) + expFormat.format(creditCardExpMonth);
if(!PrepaidUtils.checkCvv(creditCardCvv)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-cvv", "You entered an invalid security code. Please enter a valid one.");
    RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    return;
}
String billingAddress1 = RequestUtils.getAttribute(request, "billingAddress1", String.class, true);
String billingPostal = RequestUtils.getAttribute(request, "billingPostal", String.class, true);
String billingCountry = RequestUtils.getAttribute(request, "billingCountry", String.class, true);
Holder<String> formattedBillingPostal = new Holder<String>();
if(!GenerateUtils.checkPostal(billingCountry, billingPostal, true, formattedBillingPostal)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-postal", "You entered an invalid postal code for the billing country {1}. Please try again.", billingPostal, billingCountry);
    RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    return;
}
billingPostal = formattedBillingPostal.getValue();
    		
Holder<String> cardNumHolder = new Holder<String>();
if(!PrepaidUtils.checkCardNum(creditCardNum, cardNumHolder, false)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-credit-card", "You entered an invalid credit card number. Please enter a valid one.");
    RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    return;
}
String track = PrepaidUtils.getTrack(creditCardNum,creditCardCvv,creditCardExpYear,creditCardExpMonth, user.getFullName(),billingPostal);
	String virtualAllCardDeviceSerialCd=null;
String currencyCd;
if(billingCountry.equals("CA")){
	currencyCd="CAD";
	virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-CAD";
}else{
	currencyCd="USD";
	virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-USD";
}
String entryType = "N";
String attributes=null;
EC2CardInfo infoResp = EC2ClientUtils.getEportConnect().getCardInfoPlain(null,null, virtualAllCardDeviceSerialCd, track, entryType, attributes);
if(infoResp == null) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-auth-failed", "Sorry for the inconvenience. We could not process your credit card at this time. Please try again.");
	RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    return;
}
String masked = PrepaidUtils.maskCardNumber(creditCardNum);
switch(infoResp.getReturnCode()) {
	case 1: //APPROVED
	case 2: //APPROVED
        break;
    case 3:
    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-info-declined", "Your credit card {0} was declined. Please try another card.", masked);
    	RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    	return;
    case 0:
    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-info-failed", "Your credit card {0} was declined. Please try another card.", masked);
    	RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    	return;
    default:
    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-info-failed", "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
    	RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
    	return;
}
long globalAccountId=infoResp.getCardId();

try {
	String nickname = RequestUtils.getAttribute(request, "nickname", String.class, false);
	PrepaidUtils.addCreditCard(user.getConsumerId(), globalAccountId, currencyCd, expDate, nickname, promoCode);	
} catch(SQLException e) {
   	switch(e.getErrorCode()) {
	   	case 20310:
	        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-promo-code", "The promotion code you entered is invalid. Please re-enter it.", RequestUtils.getAttribute(request, "promoCode", String.class, false));
	        break;
   		case 20330:
        	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-no-promotion-exists", "There is no valid credit card promotion associated with your account. Please enter a valid promotion code.");
        	break;
   		case 20300:
            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it");
            break;
        case 20301:
   			RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-already-registered", "You have already registered this card. Please enter a different card");
            break;
        case 20302:
            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-registered-to-other", "This card has been registered under a different email. Please enter a different card");
            break;
        case 1:
        	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-nickname-unique", "The nickname needs to be unique. Please try with a different nickname.");
        	break;
        default:
           	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
   	}
   	RequestUtils.redirectWithCarryOver(request, response, "allcards.html?expand=new", false, true);
   	return;
}
RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "all-card-info-success", "We have verified your credit card {0}. You may now use this card to get discount.", masked);
RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-registered-prompt", "This credit card ({0}) has been linked to your account.", PrepaidUtils.maskCardNumber(cardNumHolder.getValue()));
RequestUtils.redirectWithCarryOver(request, response, "allcards.html");
%>