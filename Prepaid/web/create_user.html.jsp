<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.ec2.EC2CardInfo"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="simple.results.BeanException"%>
<%@page import="simple.db.DataLayerException"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.prepaid.web.ConsumerAccount"%>
<%@page import="java.text.Format"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="javax.mail.internet.AddressException"%>
<%@page import="com.usatech.ec2.EC2AuthResponse"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="simple.lang.Holder"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.io.Log,simple.translator.Translator"%>
<%@page import="java.sql.SQLException,simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%@page import="com.usatech.prepaid.pass.notification.SignUpFromPassUtils"%>
<%@page import="com.usatech.prepaid.pass.notification.ConsumerIds"%>

<%! private static final Log log = Log.getLog(); %>
<%

if ("GET".equalsIgnoreCase(request.getMethod())){
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-request", "The request was invalid, please login again for the protection of your account.");
    RequestUtils.redirectWithCarryOver(request, response, "signin.html", false, true);
    return;
}

int cardType = RequestUtils.getAttribute(request, "cardType", int.class, true);
String passSerialNumber = StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "passSerialNumber", String.class, false));
String signUpUrl = "signup.html" + (StringUtils.isBlank(passSerialNumber) ? "" : ("?uuid=" + passSerialNumber));

if (!PrepaidUtils.isRemoteClientPermitted(request.getRemoteAddr())) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	return;
};

String creditCardNum = RequestUtils.getAttribute(request, "creditCardNum", String.class, false);
String creditCardCvv = RequestUtils.getAttribute(request, "creditCardCvv", String.class, false);
String promoCode = RequestUtils.getAttribute(request, "promoCode", String.class, false);
Integer creditCardExpMonth = RequestUtils.getAttribute(request, "creditCardExpMonth", Integer.class, false);
Integer creditCardExpYear = RequestUtils.getAttribute(request, "creditCardExpYear", Integer.class, false);

String card = RequestUtils.getAttribute(request, "card", String.class, false);
String securityCode = RequestUtils.getAttribute(request, "securityCode", String.class, false);
Long globalAccountId = null;
Long consumerAccountId = null;
Long consumerId = null;
if (!StringUtils.isBlank(passSerialNumber) && (StringUtils.isBlank(card) || StringUtils.isBlank(securityCode))) {
    ConsumerIds cids = SignUpFromPassUtils.findConsumerIdsByPassSerialNumber(passSerialNumber);
    if (cids != null) {
        globalAccountId = cids.getGlobalAccountId();
        consumerAccountId = cids.getConsumerAccountId();
        consumerId = cids.getConsumerId();
    }
}

Integer preferredCommType = RequestUtils.getAttribute(request, "preferredCommType", Integer.class, false);
String email = RequestUtils.getAttribute(request, "email", String.class, false);
String emailConfirm = RequestUtils.getAttribute(request, "emailConfirm", String.class, false);
String mobile = RequestUtils.getAttribute(request, "mobile", String.class, false);
String mobileConfirm = RequestUtils.getAttribute(request, "mobileConfirm", String.class, false);
Integer carrierId = RequestUtils.getAttribute(request, "carrierId", Integer.class, false);
String firstname = RequestUtils.getAttribute(request, "firstname", String.class, false);
String lastname = RequestUtils.getAttribute(request, "lastname", String.class, false);
String address1 = RequestUtils.getAttribute(request, "address1", String.class, false);
String city = RequestUtils.getAttribute(request, "city", String.class, false);
String state = RequestUtils.getAttribute(request, "state", String.class, false);
String postal = RequestUtils.getAttribute(request, "postal", String.class, false);
String country = RequestUtils.getAttribute(request, "country", String.class, false);
String password = RequestUtils.getAttribute(request, "newpassword", String.class, false);
String confirm = RequestUtils.getAttribute(request, "newconfirm", String.class, false);
Long region = RequestUtils.getAttribute(request, "region", Long.class, false);
Translator translator = RequestUtils.getTranslator(request);
String expDate=null;
String currencyCd=null;
String creditCardMasked=null;
if(cardType==1){
	if(StringUtils.isBlank(promoCode)||!promoCode.trim().matches(PrepaidUtils.PROMO_CODE_CHECK)||StringUtils.isBlank(creditCardNum)||StringUtils.isBlank(creditCardCvv)||creditCardExpMonth==null||creditCardExpYear==null){
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
		
}else if (cardType!=0){
	if(globalAccountId == null && (StringUtils.isBlank(card) ||
			StringUtils.isBlank(securityCode)) ||
			preferredCommType == null ||
			StringUtils.isBlank(firstname) ||
			StringUtils.isBlank(lastname) ||
			StringUtils.isBlank(address1) ||
			StringUtils.isBlank(postal) ||
			StringUtils.isBlank(country) ||
			StringUtils.isBlank(password) ||
			StringUtils.isBlank(confirm)) {
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
}
switch(preferredCommType) {
	case 1: //email
		if(StringUtils.isBlank(email)||StringUtils.isBlank(emailConfirm)) {
		    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
		    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
		    return;
		}
		break;
	case 2: //sms
        if(StringUtils.isBlank(mobile) || carrierId == null||StringUtils.isBlank(mobileConfirm)) {
            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
            RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
            return;
        }
        break;
}
if(!StringUtils.isBlank(email))
	try {
		if(!email.equals(emailConfirm)){
			RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-email", "Your email and confirm email does not match. Please try again.");
		    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
		    return;
		}
		WebHelper.checkEmail(email);
	} catch(MessagingException e) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-email", "You entered an invalid email: {0}. Please enter a valid one", e.getMessage());
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
String sms;
if(!StringUtils.isBlank(mobile) && carrierId != null&& !StringUtils.isBlank(mobileConfirm))
	try {
		if(!mobile.equals(mobileConfirm)){
			RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-mobile", "Your mobile phone number and confirm mobile phone number does not match. Please try again.");
	        RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	        return;
		}
	    sms = PrepaidUtils.checkMobile(mobile, carrierId);
	} catch(MessagingException e) {
        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-mobile", "You entered an invalid mobile number: {0}. Please enter a valid one", e.getMessage());
        RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
        return;
    } 
else
	sms = null;
/*
String agree = RequestUtils.getAttribute(request, "agree", String.class, false);
if(StringUtils.isBlank(agree) || !"Y".equalsIgnoreCase(agree)) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-agree", "You must agree to the Terms and Conditions by clicking the checkbox. Please do so and resubmit.");
    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
    return;
}*/
if(!PrepaidUtils.checkPassword(RequestUtils.getOrCreateMessagesSource(request), password, confirm)) {
    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	return;
}
Holder<String> formattedPostal = new Holder<String>();
if(!GenerateUtils.checkPostal(country, postal, true, formattedPostal)) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-postal", "You entered an invalid postal code for the country {1}. Please try again.", postal, country);
    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
    return;
}

Long sproutAccountId=null;
if(cardType==1){//credit card
	String billingAddress1 = null;
	String billingPostal = null;
	String billingCountry = null;
	Holder<String> creditCardNumHolder = new Holder<String>();
	if(!PrepaidUtils.checkCardNum(creditCardNum, creditCardNumHolder, false)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-credit-card", "You entered an invalid credit card number. Please enter a valid one.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    PrepaidUtils.registerFailedRequest(request.getRemoteAddr());
	    return;
	}
	creditCardNum = creditCardNumHolder.getValue();
	if(creditCardExpMonth > 12 || creditCardExpMonth < 1) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-month", "You specified an invalid month. Please enter a valid one.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	Calendar now = Calendar.getInstance();
	int currYear = now.get(Calendar.YEAR); 
	int currMonth = now.get(Calendar.MONTH) + 1;
	if(creditCardExpYear < currYear || (creditCardExpYear == currYear && creditCardExpMonth < currMonth)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-expired", "This card has already expired. Please use a different one.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	Format expFormat = ConvertUtils.getFormat("NUMBER:00");
	expDate=expFormat.format(creditCardExpYear % 100) + expFormat.format(creditCardExpMonth);
	if(!PrepaidUtils.checkCvv(creditCardCvv)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-cvv", "You entered an invalid security code. Please enter a valid one.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	billingAddress1 = RequestUtils.getAttribute(request, "billingAddress1", String.class, true);
	billingPostal = RequestUtils.getAttribute(request, "billingPostal", String.class, true);
	billingCountry = RequestUtils.getAttribute(request, "billingCountry", String.class, true);
	Holder<String> formattedBillingPostal = new Holder<String>();
	if(!GenerateUtils.checkPostal(billingCountry, billingPostal, true, formattedBillingPostal)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-postal", "You entered an invalid postal code for the billing country {1}. Please try again.", billingPostal, billingCountry);
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	billingPostal = formattedBillingPostal.getValue();
	String track = PrepaidUtils.getTrack(creditCardNum,creditCardCvv,creditCardExpYear,creditCardExpMonth, firstname, lastname,billingPostal);
	String virtualAllCardDeviceSerialCd=null;
	if(billingCountry.equals("CA")){
		currencyCd="CAD";
		virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-CAD";
	}else{
		currencyCd="USD";
		virtualAllCardDeviceSerialCd=EC2ClientUtils.getVirtualDeviceSerialCd()+"-USD";
	}
	String entryType = "N";
	String attributes=null;
	EC2CardInfo infoResp = EC2ClientUtils.getEportConnect().getCardInfoPlain(null,null, virtualAllCardDeviceSerialCd, track, entryType, attributes);
	if(infoResp == null) {
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-auth-failed", "Sorry for the inconvenience. We could not process your credit card at this time. Please try again.");
    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	String masked = PrepaidUtils.maskCardNumber(creditCardNum);
	switch(infoResp.getReturnCode()) {
		case 1: //APPROVED
	    case 2: //APPROVED
	    	creditCardMasked=masked;
	    	break;
	    case 3:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-info-declined", "Your credit card {0} was declined. Please try another card.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
		    PrepaidUtils.registerFailedRequest(request.getRemoteAddr());
	    	return;
	    case 0:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-info-failed", "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    	return;
	    case 9:
	    case 11:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-info-failed", "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
		    PrepaidUtils.registerFailedRequest(request.getRemoteAddr());
	    	return;
	    default:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "all-card-info-failed", "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    	return;
	}
	globalAccountId=infoResp.getCardId();
}else if(cardType==3){//Sprout prepaid card
	if(!PrepaidUtils.isSproutCard(card)){
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-not-match", "This is not a sprout card. Please try another card.");
    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	}
	String entryType = "N";
	String attributes = null;
	EC2CardInfo infoResp = EC2ClientUtils.getEportConnect().getCardInfoPlain(null,null, EC2ClientUtils.getVirtualDeviceSerialCd()+"-USD", card+"|"+securityCode, entryType, attributes);
	if(infoResp == null) {
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-auth-failed", "Sorry for the inconvenience. We could not process your sprout card registration at this time. Please try again.");
    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	String masked = PrepaidUtils.maskCardNumber(card);
	switch(infoResp.getReturnCode()) {
		case 1: //APPROVED
	    case 2: //APPROVED
	    	creditCardMasked=masked;
	    	break;
	    case 3:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-info-declined", "Your sprout card {0} was declined. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    	return;
	    case 0:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-info-failed", "Your sprout card {0} was declined. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    	return;
	    default:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-info-failed", "Sorry for the inconvenience. We could not process your sprout card {0} at this time. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    	return;
	}
	
	globalAccountId=infoResp.getCardId();
	String attributesResp=infoResp.getAttributes();
	if(attributesResp!=null){
		String[] attArray=attributesResp.split("=");
		if(attArray!=null&&attArray.length==2){
			sproutAccountId=ConvertUtils.getLongSafely(attArray[1], -1);
		}
	}
	if(sproutAccountId==null||sproutAccountId<-1){
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "sprout-card-info-failed", "Sorry for the inconvenience. We could not process your sprout card {0} at this time. Please try again.", masked);
    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
    	return;
	}
	
} else if(cardType!=0 && globalAccountId == null) {// MORE and Payroll Deduct card
	Holder<String> cardNumHolder = new Holder<String>();
	if(!PrepaidUtils.checkCardNum(card, cardNumHolder, true)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-prepaid-card", "You entered an invalid prepaid card number. Please enter a valid one.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    PrepaidUtils.registerFailedRequest(request.getRemoteAddr());
	    return;
	}
	
	try {
		globalAccountId = EC2ClientUtils.getGlobalAccountId(cardNumHolder.getValue());
	} catch(Throwable e) {
		log.warn("Could not get globalAccountId", e);
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
}
ConsumerAccount consumerAcct = null;
String passcode = null;
try {
    if (StringUtils.isBlank(passSerialNumber)) {
        if (cardType != 0) consumerAcct = PrepaidUtils.createUser(translator, globalAccountId, securityCode, email, mobile, carrierId, preferredCommType, firstname, lastname, address1, city, state, formattedPostal.getValue(), country, password, region, RequestUtils.getBaseUrl(request), null, request, cardType, expDate, promoCode, currencyCd,sproutAccountId, null);
        else consumerAcct = PrepaidUtils.createUser(email, mobile, carrierId, preferredCommType, firstname, lastname, address1, city, state, formattedPostal.getValue(), country, password, region, RequestUtils.getBaseUrl(request), null, request, null);
    } else {
    	if (cardType != 0) consumerAcct = PrepaidUtils.createUser(translator, globalAccountId, securityCode, email, mobile, carrierId, preferredCommType, firstname, lastname, address1, city, state, formattedPostal.getValue(), country, password, region, RequestUtils.getBaseUrl(request), null, request, cardType, expDate, promoCode, currencyCd,sproutAccountId, passSerialNumber);
    	else consumerAcct = PrepaidUtils.createUser(email, mobile, carrierId, preferredCommType, firstname, lastname, address1, city, state, formattedPostal.getValue(), country, password, region, RequestUtils.getBaseUrl(request), null, request, passSerialNumber);
    }
} catch(SQLException e) {
	String username;
    switch(preferredCommType) {
        case 1: // Email
            username = email;
            break;
        case 2:
            username = mobile;
            break;
        default:
            username = email;
            break;
    }
   	switch(e.getErrorCode()) {
   		   case 20300:
               RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it", username);
         	   PrepaidUtils.registerFailedRequest(request.getRemoteAddr());
               break;
           case 20301:
   			   RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-already-registered", "You have already registered this card. Please enter a different card", username);
               break;
           case 20302:
               RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-registered-to-other", "This card has been registered under a different email. Please contact Customer Service for assistance", username);
        	   PrepaidUtils.registerFailedRequest(request.getRemoteAddr());
               break;
           case 20303:
        	   InputForm form = RequestUtils.getInputForm(request);
        	   form.setAttribute("username", username);
        	   form.setAttribute("password", password);
        	   if (PrepaidUtils.loginUser(form, request, response) && cardType == 0) {
        		    RequestUtils.redirectWithCarryOver(request, response, "dashboard.html", false, true);
      		   		return;   
        	   }
        	   if (PrepaidUtils.loginUser(form, request, response) && !StringUtils.isBlank(passSerialNumber)){
        		   String addCardUrl = "register_card.html" + "?uuid=" + passSerialNumber;
        		   RequestUtils.redirectWithCarryOver(request, response, addCardUrl, false, true);
       		   		return;   
        	   }else if(PrepaidUtils.loginUser(form, request, response) && (cardType==2)){
        		   RequestUtils.redirectWithCarryOver(request, response, "register_card.html", false, true);
        		   	return;
        	   }else if (PrepaidUtils.loginUser(form, request, response) && (cardType==4)){
        		   RequestUtils.redirectWithCarryOver(request, response, "register_payroll_deduct_card.html", false, true);
       		   	return;
       	   	   }else{
               		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-username-not-unique", "Your email or mobile number is already used for MORE card account. Please use a different email and mobile number", username);
        	   }
               break;
           case 20309:
               RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-state", "The state you entered is invalid. Please re-enter it.", RequestUtils.getAttribute(request, "state", String.class, false));
               break;
           case 20310:
               RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-promo-code", "The promotion code you entered is invalid. Please re-enter it.", RequestUtils.getAttribute(request, "promoCode", String.class, false));
               break;
           default:
           	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
   	}
   	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
   	return;
}
int replenishType = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "replenishType", Integer.class, false),0);
if (replenishType > 0) {
	String replenishCardNum = null;
	int replenishExpMonth = 0;
	int replenishExpYear = 0;
	String billingAddress1 = null;
	String billingPostal = null;
	String billingCountry = null;
	String cvv = null;
	replenishCardNum = RequestUtils.getAttribute(request, "replenishCardNum", String.class, true);
	Holder<String> replenishCardNumHolder = new Holder<String>();
	if(!PrepaidUtils.checkCardNum(replenishCardNum, replenishCardNumHolder, false)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-credit-card", "You entered an invalid credit card number. Please enter a valid one.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	replenishCardNum = replenishCardNumHolder.getValue();
	replenishExpMonth = RequestUtils.getAttribute(request, "replenishExpMonth", Integer.class, true);
	replenishExpYear = RequestUtils.getAttribute(request, "replenishExpYear", Integer.class, true);
	if(replenishExpMonth > 12 || replenishExpMonth < 1) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-month", "You specified an invalid month. Please enter a valid one.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	Calendar now = Calendar.getInstance();
	int currYear = now.get(Calendar.YEAR); 
	int currMonth = now.get(Calendar.MONTH) + 1;
	if(replenishExpYear < currYear || (replenishExpYear == currYear && replenishExpMonth < currMonth)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-replenish-card-expired", "This card has already expired. Please use a different one.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	cvv = RequestUtils.getAttribute(request, "cvv", String.class, true);
	if(!PrepaidUtils.checkCvv(cvv)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-cvv", "You entered an invalid security code. Please enter a valid one.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	billingAddress1 = RequestUtils.getAttribute(request, "billingAddress1", String.class, true);
	billingPostal = RequestUtils.getAttribute(request, "billingPostal", String.class, true);
	billingCountry = RequestUtils.getAttribute(request, "billingCountry", String.class, false);
	Holder<String> formattedBillingPostal = new Holder<String>();
	if(!GenerateUtils.checkPostal(billingCountry, billingPostal, true, formattedBillingPostal)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-postal", "You entered an invalid postal code for the billing country {1}. Please try again.", billingPostal, billingCountry);
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	billingPostal = formattedBillingPostal.getValue();
	Format expFormat = ConvertUtils.getFormat("NUMBER:00");
	BigDecimal amount;
	BigDecimal threshhold;
	if(replenishType == 1) {
		threshhold = RequestUtils.getAttribute(request, "threshhold", BigDecimal.class, true);
		if(threshhold.compareTo(PrepaidUtils.getMinThreshholdAmount()) < 0) {
		    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-threshhold-amt-too-small", "The minimum balance you entered is too small. Please enter at least {0}.", PrepaidUtils.getMinThreshholdAmount());
		    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
		    return;
		}
	} else
		threshhold = null;
	amount = RequestUtils.getAttribute(request, "amount", BigDecimal.class, true);
	if(amount.compareTo(PrepaidUtils.getMinReplenishAmount()) < 0) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-replenish-amt-too-small", "The amount you entered is too small. Please enter at least {0}.", PrepaidUtils.getMinReplenishAmount());
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	} else if(amount.compareTo(PrepaidUtils.getMaxReplenishAmount()) > 0) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-replenish-amt-too-big", "The amount you entered is too much. Please enter at most {0}.", PrepaidUtils.getMaxReplenishAmount());
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    return;
	}
	EC2AuthResponse resp = PrepaidUtils.setupReplenish(consumerAcct.getConsumerAcctId(), null, globalAccountId, consumerAcct.getConsumerId(), replenishType, amount, threshhold, replenishCardNum, expFormat.format(replenishExpYear % 100) + expFormat.format(replenishExpMonth), cvv, billingAddress1, billingPostal);
	//tell user
	if(resp == null) {
		RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenish-updated", "The replenishment amount settings have been updated.");
	    RequestUtils.redirectWithCarryOver(request, response, signUpUrl);
	    return;
	}
	String masked = PrepaidUtils.maskCardNumber(replenishCardNum);
	switch(resp.getReturnCode()) {
	    case 2: /*APPROVED*/
	    	if(resp.getApprovedAmount() == 0) {
	    		if(replenishType == 2)    
	                RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-authonly-success", "We have verified your credit card {1}. You may now use this card to replenish your account", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked);
	             else 
	                RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-authonly-auto-success", "We have verified your credit card {1} and setup automatic replenishment to occur whenever your prepaid account balance falls below {2,CURRENCY}.", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked, threshhold);
	    	} else {
	    		if(replenishType == 2)	
	   			   RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-success", "We have scheduled a transaction to charge {0,CURRENCY} to your credit card {1} and to add that same amount to your prepaid account. Your statements will reflect this in the next few minutes.", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked);
	    	    else 
	   	    	   RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-auto-success", "We have scheduled a transaction to charge {0,CURRENCY} to your credit card {1} and have set up automatic replenishment to occur whenever your prepaid account balance falls below {2,CURRENCY}. Your statements will reflect this in the next few minutes.", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked, threshhold);
	    	}
	        break;
	    case 3:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-replenished-declined", "Your credit card {0} was declined. Please try another card.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	        break;
	    case 0:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-replenished-failed", "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	    	break;
	    default:
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-replenished-failed", "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
	    	RequestUtils.redirectWithCarryOver(request, response, signUpUrl, false, true);
	        break;
	}
}
if(cardType==1){
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "all-card-info-success", "We have verified your credit card {0}. You may now use this card to get discount.", creditCardMasked);
}
RequestUtils.setAttribute(request, "subtitle", "Almost Done", "request"); 
PrepaidUtils.registerSuccessRequest(request.getRemoteAddr());
%>
<jsp:include page="/header.jsp"/>
<div class="billboard">
 <div class="kilo">Just one <span class="more-text">more</span> thing<span class="more-text">...</span></div>
 
<%
  String replenishmentBonusNote = translator.translate("prepaid-replenishment-note", "");
  if (!StringUtils.isBlank(replenishmentBonusNote)) {%>
	<div class="spacer10"></div>
	<div>
      <span class="replenish-note-signup">
		<span class="replenish-note-signup-inner">
		<%=replenishmentBonusNote%>
		</span>
      </span>
	</div>
<%} %>
  <p class="lead"><%
  if (passcode != null && consumerAcct != null) {
        SignUpFromPassUtils.completeUserRegistration(consumerAcct.getConsumerId(), passcode);
        %>
        <%=translator.translate("prepaid-registration-complete-prompt", "Your account is now verified") %>
        <%
  } else {
switch(preferredCommType) {
	case 2: //SMS
		String prompt = RequestUtils.getTranslator(request).translate("prepaid-registration-sms-prompt", "To receive text messages,\nplease enter the passcode found in the text message that has been sent to you."); 
		String includePage = new StringBuilder().append("/verify_user_form.jsp?buttonText=Complete+Verification&prompt=").append(StringUtils.prepareURLPart(prompt)).append("&consumerId=").append(consumerAcct.getConsumerId()).toString();
		%>
        <jsp:include page="<%=includePage%>"/>
        <%
		break;
	default:
		  %><%=translator.translate("prepaid-registration-email-prompt", "To receive email messages,<br />please click the link in the email that has been sent to you.<br />(Please check Junk Email folder in your email.)", email, firstname, lastname) %><%		
}}%></p>
  <div class="text-center">
    <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/index.html") %>"><button class="btn btn-return">&laquo; Return to the website</button></a>
  </div>
</div> <!-- /billboard -->
<hr/>
<jsp:include page="/footer.jsp"/>