<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
<title>more. Registration Confirmation</title><%
StringBuilder sb = new StringBuilder();
sb.append("register_user.html?userId=").append(RequestUtils.getAttribute(request, "consumerId", Number.class, true))
    .append("&passcode=").append(StringUtils.prepareURLPart(RequestUtils.getAttribute(request, "passcode", String.class, true)));
String url = sb.toString();
String baseUrl = RequestUtils.getAttribute(request, "baseUrl", String.class, true);
Translator translator = RequestUtils.getTranslator(request);
%>
<base href="<%=StringUtils.prepareCDATA(baseUrl)%>" />
<style type="text/css">
body {
  width: 100% !important;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 16px;
  -webkit-text-size-adjust: none;
  -ms-text-size-adjust: none;
  margin: 0;
  padding: 0;
  text-align: center;
  }
img {
  height: auto;
  line-height: 100%;
  outline: none;
  text-decoration: none;
  display: block;
  }
h1, h2, h3, h4, h5, h6 {
  color: #333333;
  line-height: 100% !important;
  margin:0;
  }
  
p {
  margin: 1em 0;
  }
a {
  color:#0088cc;
  text-decoration: none;
  }
  
a:hover,
a:focus {
  color: #005580;
  text-decoration: underline !important;
}
table td {
  border-collapse: collapse;
  } 
  
<% ResponseUtils.writeCoteResource(pageContext, "extra_style");
PrepaidUser user = (PrepaidUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
%> 
</style>
<meta name="robots" content="noindex, nofollow"/>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td bgcolor="#ffffff" align="center" style="border-collapse:collapse;">
      <!-- table containing the nav bar -->
      <table cellpadding="0" cellspacing="0" border="0" width="600" height="40" style="background-color:#578fcf;">
        <tr>
          <td height="40" width="421" style="text-align: left; font-weight: bold; font-style: italic; font-family: Georgia, sans-serif; color: #fff; font-size: 30px;"><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/index.html") %>"><img src="email_logo._" alt="more." style="color: #fff;"/></a></td>
          <td height="40" width="81" valign="middle" style="text-align: center; border-collapse:collapse;" ><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signin.html") %>" style="color:#fff; text-decoration:none; font-size: 16px;">Sign In</a></td>
          <td height="40" width="86" valign="middle" style="text-align: center; border-collapse:collapse;"><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/support.html") %>" style="color:#ffffff; text-decoration:none; font-size: 16px;">Support</a></td>
          <td height="40" width="12"></td>
        </tr>
      </table>
      <table cellpadding="0" cellspacing="0" border="0" width="600" height="5" bgcolor="#ffffff">
        <tr>
          <td width="600" height="5">&nbsp;</td>
        </tr>
      </table>   
      <!-- table containing the body of the email -->
      <table cellpadding="20" cellspacing="0" border="0" width="600" bgcolor="#ffffff">
        <tr>
          <td width="600" valign="middle" style="border-collapse:collapse; text-align: center;" >              
                <h2 style="color: #333333; font-size: 21px; line-height:100% !important;" >Your MORE account is now activated! Please verify your email address by clicking the Verify Email Address link below.</h2>
                <p style="margin-top: 30px;">
                <a href="<%=StringUtils.prepareCDATA(baseUrl)%><%=StringUtils.prepareCDATA(url)%>" style="color: #0088cc; font-size: 24px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; text-decoration: underline; text-align: center; font-weight: bold; font-style: normal;">
                    Verify Email Address
                </a></p>
                <%=translator.translate("prepaid-register-email-extra-html", "")%>
                <p style="font-size: 14px; color: #333333; margin-top: 30px; line-height: 20px;">
                Is the link above not working?<br />Copy and paste this link into the address bar of your web browser:<br /><a href="<%=StringUtils.prepareCDATA(baseUrl)%><%=StringUtils.prepareCDATA(url)%>"><%=StringUtils.prepareHTMLBlank(baseUrl)%><%=StringUtils.prepareHTMLBlank(url)%></a></p>
          </td>
        </tr>
      </table>          
      <table width="600"  cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
            <hr />
          </td>
        </tr>
      </table>        
      <!-- table containing the footer -->
      <table width="600" height="40"  cellpadding="0" cellspacing="0" border="0">
        <tr>
        <td height="40" style="text-align: center; font-size: 14px;" colspan="3">
        <% if(user!=null){
		    %>
		    <div class="footer-links">
		    	<div class="terms-links">
      				<span>Terms and Conditions   </span><a id="prepaid_termslink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/morecard_terms_conditions.html") %>">Prepaid</a>&nbsp;|&nbsp;<a id="credit_termslink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/allcards_terms_conditions.html") %>">Credit</a>&nbsp;|&nbsp;<a id="payroll_termslink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/payroll_deduct_cards_terms_conditions.html") %>">Payroll</a>
      			</div>
     			 <div class="privacy-links">  
     			   <span>Privacy and Policy     </span><a id="prepaid_privacylink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/morecard_privacy_policy.html") %>">Prepaid</a>&nbsp;|&nbsp;<a id="privacylink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/allcards_privacy_policy.html") %>">Credit</a>&nbsp;|&nbsp;<a id="privacylink" href="<%=ToJspSimpleServlet.untranslatePath(request, "/payroll_deduct_cards_privacy_policy.html") %>">Payroll</a>
   			   </div>  
		    </div>		
		<% 
    		}
		 %>
		    <div style="margin-bottom: .3em;">
		      <div style="display: inline-block;">
		        <span style="font-weight: 800; font-style: italic; font-family: 'Georgia', sans-serif; color: <%=translator.translate("prepaid-more-color", "#e74b34")%>; font-size: 105%">more.</span> v.<%=GenerateUtils.getApplicationVersion()%> &ndash; A product of <a href="http://www.usatech.com">USA Technologies, Inc.</a>
		        <span>&copy; Copyright 2003-<%=GenerateUtils.getCurrentYear()%></span>
		      </div>
		    </div>
        </td>
        </tr>
        <tr>
        <td width="265">&nbsp;</td>
        <td width="70" height="50"><img style="text-align: center;" src="/images/trustwave-seal70x35.png" onclick="javascript:window.open('https://sealserver.trustwave.com/cert.php?customerId=w6o8pBiUhhnnGnDB1cfZ1FWOQ66BnG&amp;size=105x54&amp;style=invert', 'c_TW', 'location=no, toolbar=no, resizable=yes, scrollbars=yes, directories=no, status=no, width=615, height=720'); return false;" oncontextmenu="javascript:alert('Copying Prohibited by Law - Trusted Commerce is a Service Mark of TrustWave Holdings, Inc.'); return false;" alt="This site protected by Trustwave's Trusted Commerce program" title="This site protected by Trustwave's Trusted Commerce program"/></td>
        <td width="265">&nbsp;</td>
        </tr>
        </table> 
</td>  <!-- /end the td that centers things  -->
</tr>
</table>  <!-- /end the table that contains everything -->
</body>
</html>