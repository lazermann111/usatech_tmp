        <div class="page-controls">
            <div class="per-page pull-left">
                Per page: <%
first = true;
for(Integer pp : rowsOptions) {
    if(first)
        first = false;
    else {%> | <% }     
    %><button type="submit" name="rows" class="btn btn-link" value="<%=pp %>" onclick="$('static-rows').disabled=true; return true;"<%if(rows.intValue() == pp.intValue()){ %> disabled="disabled"<%} %>><%=(pp > 0 ? String.valueOf(pp) : "All") %></button><%
}%>
            </div><% 
if(rows > 0) {%>
            <div class="page-number pull-right">
                Page: <%
    for(int p = 1; p <= 1 + ((count - 1) / rows); p++) {
        if(p > 1) {%>, <% }     
        %><button type="button" class="page_button_<%=p%> btn btn-link" onclick="viewPage(<%=p%>)" title="View page <%=p%>"<%if(p == pageNum){ %> disabled="disabled"<%} %>><%=p %></button><%
    }%>
            </div><%
} %>
        </div>
        <div class="clearfix"></div>
