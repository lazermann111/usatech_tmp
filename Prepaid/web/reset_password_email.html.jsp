<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.translator.Translator"%>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
<title>more. Reset Password</title><%
Translator translator = RequestUtils.getTranslator(request);
StringBuilder sb = new StringBuilder();
sb.append("edit_password.html?userId=").append(RequestUtils.getAttribute(request, "consumerId", Number.class, true))
    .append("&passcode=").append(StringUtils.prepareURLPart(RequestUtils.getAttribute(request, "passcode", String.class, true)))
    .append("&username=").append(StringUtils.prepareURLPart(RequestUtils.getAttribute(request, "username", String.class, true)));
String editUrl = sb.toString();
sb.setLength(0);
sb.append("cancel_passcode.html?userId=").append(RequestUtils.getAttribute(request, "consumerId", Number.class, true))
.append("&passcode=").append(StringUtils.prepareURLPart(RequestUtils.getAttribute(request, "passcode", String.class, true)))
.append("&username=").append(StringUtils.prepareURLPart(RequestUtils.getAttribute(request, "username", String.class, true)));
String cancelUrl = sb.toString();
String baseUrl = RequestUtils.getAttribute(request, "baseUrl", String.class, true);
String firstname = RequestUtils.getAttribute(request, "firstname", String.class, true);
%>
<base href="<%=StringUtils.prepareCDATA(baseUrl)%>" />
<style type="text/css">
body {
  width: 100% !important;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 16px;
  -webkit-text-size-adjust: none;
  -ms-text-size-adjust: none;
  margin: 0;
  padding: 0;
  text-align: center;
  }

img {
  height: auto;
  line-height: 100%;
  outline: none;
  text-decoration: none;
  display: block;
  }

h1, h2, h3, h4, h5, h6 {
  color: #333333;
  line-height: 100% !important;
  margin:0;
  }
  
p {
  margin: 1em 0;
  }

a {
  color:#0088cc;
  text-decoration: none;
  }
  
a:hover,
a:focus {
  color: #005580;
  text-decoration: underline !important;
}

table td {
  border-collapse: collapse;
  } 

</style>
<meta name="robots" content="noindex, nofollow"/>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td bgcolor="#ffffff" align="center" style="border-collapse:collapse;">
      <!-- table containing the nav bar -->
      <table cellpadding="0" cellspacing="0" border="0" width="680" height="40" style="background-color:#578fcf;">
        <tr>
          <td height="40" width="501" style="text-align: left; font-weight: bold; font-style: italic; font-family: Georgia, sans-serif; color: #fff; font-size: 30px;"><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/index.html") %>"><img src="/images/more-logo315x40.png" alt="more." style="color: #fff;"/></a></td>
          <td height="40" width="81" valign="middle" style="text-align: center; border-collapse:collapse;" ><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signin.html") %>" style="color:#fff; text-decoration:none; font-size: 16px;">Sign In</a></td>
          <td height="40" width="86" valign="middle" style="text-align: center; border-collapse:collapse;"><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/support.html") %>" style="color:#ffffff; text-decoration:none; font-size: 16px;">Support</a></td>
          <td height="40" width="12"></td>
        </tr>
      </table>
      <table cellpadding="0" cellspacing="0" border="0" width="680" height="5" bgcolor="#ffffff">
        <tr>
          <td width="680" height="5">&nbsp;</td>
        </tr>
      </table>   
      <!-- table containing the body of the email -->
      <table cellpadding="0" cellspacing="0" border="0" width="680" bgcolor="#ffffff">
        <tr>
          <td width="20">&nbsp;</td>         
          <td valign="middle" style="border-collapse:collapse; text-align: left;" width="640">
                <div style="width: 560px">
                <h2 style="color: #333333; font-size: 24px; line-height:100% !important;" >Dear <%=StringUtils.prepareHTML(firstname) %>,</h2>
                <br />
                <p style="font-size: 18px; color: #333333; margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0;" >
                Click on the link below to reset your password for your prepaid and loyalty account:</p>
                <p style="margin-top: 30px;">
                <a href="<%=StringUtils.prepareCDATA(editUrl)%>" style="color: #0088cc; font-size: 24px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; text-decoration: underline; text-align: center; font-weight: bold; font-style: normal;">Reset my password.</span></a></p>
                <p style="font-size: 14px; color: #333333; margin-top: 30px; line-height: 20px;">
                Is the link above not working?<br />Copy and paste this link into the address bar of your web browser:<br /><a href="<%=StringUtils.prepareCDATA(baseUrl)%>/<%=StringUtils.prepareCDATA(editUrl)%>"><%=StringUtils.prepareHTMLBlank(baseUrl)%>/<%=StringUtils.prepareHTMLBlank(editUrl)%></a></p>
                
                <p style="font-size: 18px; color: #333333; margin-top: 30px; margin-bottom: 0; margin-right: 0; margin-left: 0;" >
                If you did not request a password reset, please click on the  link below to cancel the password reset request:</p>
                <p style="margin-top: 30px;">
                <a href="<%=StringUtils.prepareCDATA(cancelUrl)%>" style="color: #0088cc; font-size: 24px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; text-decoration: underline; text-align: center; font-weight: bold; font-style: normal;">Cancel password reset request.</span></a></p>
                <p style="font-size: 14px; color: #333333; margin-top: 30px; line-height: 20px;">
                Is the link above not working?<br />Copy and paste this link into the address bar of your web browser to cancel the password reset request:<br /><a href="<%=StringUtils.prepareCDATA(baseUrl)%>/<%=StringUtils.prepareCDATA(cancelUrl)%>"><%=StringUtils.prepareHTMLBlank(baseUrl)%>/<%=StringUtils.prepareHTMLBlank(cancelUrl)%></a></p>
                </div>
                </td>
            <td width="20">&nbsp;</td>
        </tr>
      </table>
      <table width="680" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
            <hr />
          </td>
        </tr>
      </table>        
      <!-- table containing the footer -->
      <table width="680" height="40" cellpadding="0" cellspacing="0" border="0">
        <tr>
        <td height="40" style="text-align: center; font-size: 14px;" colspan="3">
            <div style="margin-bottom: .3em;">
              <div style="display: inline-block;">
                <span style="font-weight: 800; font-style: italic; font-family: 'Georgia', sans-serif; color: <%=translator.translate("prepaid-more-color", "#e74b34")%>; font-size: 105%">more.</span> v.<%=StringUtils.encodeForHTML(GenerateUtils.getApplicationVersion())%> &ndash; A product of <a href="http://www.usatech.com">USA Technologies, Inc.</a>
                <span>&copy; Copyright 2003-<%=GenerateUtils.getCurrentYear()%></span>
              </div>
            </div>
        </td>
        </tr>
        <tr>
        <td width="305">&nbsp;</td>
        <td width="70" height="50"><img style="text-align: center;" src="/images/trustwave-seal70x35.png" onclick="javascript:window.open('https://sealserver.trustwave.com/cert.php?customerId=w6o8pBiUhhnnGnDB1cfZ1FWOQ66BnG&amp;size=105x54&amp;style=invert', 'c_TW', 'location=no, toolbar=no, resizable=yes, scrollbars=yes, directories=no, status=no, width=615, height=720'); return false;" oncontextmenu="javascript:alert('Copying Prohibited by Law - Trusted Commerce is a Service Mark of TrustWave Holdings, Inc.'); return false;" alt="This site protected by Trustwave's Trusted Commerce program" title="This site protected by Trustwave's Trusted Commerce program"/></td>
        <td width="305">&nbsp;</td>
        </tr>
        </table> 
</td>  <!-- /end the td that centers things  -->
</tr>
</table>  <!-- /end the table that contains everything -->
</body>
</html>