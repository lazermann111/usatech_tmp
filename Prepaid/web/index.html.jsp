<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.RequestUtils"%>
<% RequestUtils.setAttribute(request, "subtitle", "Prepaid Program", "request"); 
RequestUtils.setAttribute(request, "wrapNarrow", true, "request");
Translator translator = RequestUtils.getTranslator(request);%>
<jsp:include page="/header.jsp"/>
<div class="billboard">
  <img src="<%=ToJspSimpleServlet.untranslatePath(request, "/home_image._")%>" alt="<%=translator.translate("prepaid-home-image-title", "get more now") %>" />
  <p class="lead"><%=translator.translate("prepaid-home-description", "Start getting <span class=\"more-text\">more</span> with every purchase at participating self-service retail locations like vending machines,  kiosks, car washes, laundromats and much, much <span class=\"more-text\">more</span> with this ultra-convenient Prepaid &amp; Loyalty program. Simply use your card at participating machines to see the benefits add up!")%></p>
  <a class="btn btn-large btn-signin-more" href="<%=ToJspSimpleServlet.untranslatePath(request, "/signin.html") %>">Sign in</a>
  <a class="btn btn-large btn-signup-more" href="<%=ToJspSimpleServlet.untranslatePath(request, "/signup_question.html") %>">Sign up today</a>
</div> <!-- /billboard -->
<hr/>
<div class="row-fluid">
	<div class="span4 text-center">
		<img src="<%=ToJspSimpleServlet.untranslatePath(request, "/cash_icon._")%>" />
		<p><%=translator.translate("prepaid-home-text1", "Earn cash back rewards<br/>on your point of sale purchases.")%></p>
	</div>
	<div class="span4 text-center">
		<img src="<%=ToJspSimpleServlet.untranslatePath(request, "/sale_icon._")%>" />
		<p><%=translator.translate("prepaid-home-text2", "Access exclusive offers &amp; promotions at participating machines.")%></p>
	</div>
	<div class="span4 text-center">
		<img src="<%=ToJspSimpleServlet.untranslatePath(request, "/new_icon._")%>" />
		<p><%=translator.translate("prepaid-home-text3", "Find valuable product information and locate your favorite brands.")%></p>
	</div>
</div>
<hr/>
<jsp:include page="/footer.jsp"/>