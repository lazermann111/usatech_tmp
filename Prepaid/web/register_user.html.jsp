<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils,simple.io.Log,simple.servlet.InputForm,simple.translator.Translator"%>
<%@page import="java.sql.Connection,java.sql.SQLException,simple.db.DataLayerMgr,simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
long consumerId = RequestUtils.getAttribute(request, "userId", Long.class, true);
RequestUtils.getAttribute(request, "passcode", String.class, true);
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
Translator translator = RequestUtils.getTranslator(request);
Connection conn = DataLayerMgr.getConnectionForCall("REGISTER_CONSUMER");
try {
	try {
	   	DataLayerMgr.executeCall(conn, "REGISTER_CONSUMER", form);
	} catch(SQLException e) {
		try {
            conn.rollback();
        } catch(SQLException e2) {
            //ignore
        }
        if(log.isWarnEnabled())
	         log.warn("Could not complete registration of prepaid card for consumer " + consumerId, e);
	   	switch(e.getErrorCode()) {
	   		case 20320:
	            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-consumer-not-found", "The passcode is not correct.");
	            break;
	        case 20321:
				RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-passcode-invalid", "The passcode is not correct.");
	            break;
	        case 20322: //TODO: distinquish between an expired passcode and an already registered account
	        	DataLayerMgr.executeCall(conn, "REREGISTER_CONSUMER", form);
	        	int preferredCommType = form.getInt("preferredCommType", true, 0);
	            String communicationEmail = form.getString("communicationEmail", true);
	            String firstname = form.getString("firstname", false);
	            String lastname = form.getString("lastname", false);
	            PrepaidUtils.sendCompleteRegistrationEmail(conn, communicationEmail, preferredCommType, firstname, lastname, consumerId, form.getString("passcode", true), RequestUtils.getBaseUrl(request), request);
	            conn.commit();
	            switch(preferredCommType) {
	                case 2: //SMS
	                    String prompt = RequestUtils.getTranslator(request).translate("prepaid-passcode-expired-sms", "This passcode is expired. To receive text messages,\nplease enter the new passcode found in the text message that has been sent to you.");
	                    String includePage = new StringBuilder().append("/verify_user_form.jsp?buttonText=Complete+Verification&prompt=").append(StringUtils.prepareURLPart(prompt)).toString();
	                    %>
	                    <jsp:include page="/header.jsp"/>
                        <jsp:include page="<%=includePage%>"/>
	                    <jsp:include page="/footer.jsp"/><%
	                    return;
	                default:
	                     RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-passcode-expired-email", "This passcode is expired. To receive email messages, please click the link in the new email that has been sent to you.");
	            }
	            break;
	        case 20323:
                RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-consumer-credentials-not-found", "This account is not set up correctly. Please contact Customer Service for assistance");
                break;
	        case 20324:
                form.setAttribute("onloadMessage", translator.translate("prepaid-registration-already-complete-prompt", "Your account is already verified"));
                %><jsp:include page="/signin.html.jsp"/><%
                return;            
	        default:
	        	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
	   	} %>
<jsp:include page="/header.jsp"/>
<jsp:include page="/footer.jsp"/><%
        return;
	}
	if(log.isInfoEnabled())
	    log.info("Activated prepaid account for consumer " + consumerId);     
	conn.commit();
} finally {
	conn.close();
}
form.setAttribute("onloadMessage", translator.translate("prepaid-registration-complete-prompt", "Your account is now verified"));
request.getSession().invalidate();
%>
<jsp:include page="/signin.html.jsp"/>
