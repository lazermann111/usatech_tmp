<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%
RequestUtils.setAttribute(request, "subtitle", "Terms & Conditions", "request");
RequestUtils.setAttribute(request, "wrapNarrow", true, "request");
Translator translator = RequestUtils.getTranslator(request);
%>
<jsp:include page="/header.jsp"/>
<h2><%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%> Loyalty &amp; Rewards Program - TERMS AND CONDITIONS.</h2>

<h3>Questions or Concerns.</h3>
<p>
If you have any questions concerning the Program or your Card, please write to us at USA Technologies, Inc., 100 Deerfield Lane, Suite 300, Malvern, PA 19355 USA or visit us at <a href="./index.html">getmore.usatech.com</a>. You may also call us at (888) 561-4748.
</p>
<hr/>
<jsp:include page="/footer.jsp"/>