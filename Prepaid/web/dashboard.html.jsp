<%@page import="java.text.DateFormat"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.lang.SystemUtils"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="java.util.TimeZone"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.*"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results.Aggregate"%>
<%@page import="java.util.Currency"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%!
Map<String,String> typeLabels = getTypeLabels();
Map<String,String> getTypeLabels() {
    Map<String,String> typeLabels = new LinkedHashMap<String,String>();
    typeLabels.put("purchase", "Transaction");
    typeLabels.put("refund", "Refund");
    typeLabels.put("chargeback", "Chargeback");
    typeLabels.put("chargeback-reversal", "Chargeback Reversal");
    typeLabels.put("replenish", "Replenishment");
    typeLabels.put("pending-replenish", "In-process Replenishment");
    typeLabels.put("pending-purchase", "Pending Transaction");
    typeLabels.put("cash-back", "Bonus Cash");
    typeLabels.put("adjustment", "Adjustment");
    typeLabels.put("pending-adjustment", "Pending Adjustment");
    typeLabels.put("free", "Free Transaction");
    return typeLabels;
}
%>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
if(user == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-logged-in", "You are no longer logged in. Please log in again.");
    RequestUtils.redirectWithCarryOver(request, response, "signin.html");
    return;
}
if(user.getConsumerTypeId()==8){
	RequestUtils.redirectWithCarryOver(request, response, "dashboard_sprout.html");
    return;
}
Long cardId = RequestUtils.getAttribute(request, "cardId", Long.class, false);
if(cardId != null && PrepaidUtils.checkCardPermission(user, cardId) == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
    RequestUtils.redirectWithCarryOver(request, response, PrepaidUtils.getHomePage());
    return;
}
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
Locale locale = RequestUtils.getLocale(request);
TimeZone timeZone = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_TIME_ZONE, TimeZone.class, false, RequestUtils.SESSION_SCOPE);
String numberFormat = "NUMBER:#0.00;(#0.00)";
DateFormat[] dateFormats = PrepaidUtils.getStandardDateFormats(locale, timeZone);
String dateInputFormat = "DATE:yyyy-MM-dd";
Date startTime = RequestUtils.getAttribute(request, "startTime", Date.class, false);
if(startTime == null) {
    startTime = new Date(System.currentTimeMillis() - (30*24*60*60*1000L));
    RequestUtils.setAttribute(request, "startTime", startTime, "request");
}
Date endTime = RequestUtils.getAttribute(request, "endTime", Date.class, false);
if(endTime == null) {
    endTime = new Date();
    RequestUtils.setAttribute(request, "endTime", endTime, "request");
}
Integer rows = PrepaidUtils.getRowSelection(request);
Integer pageNum = RequestUtils.getAttribute(request, "page", Integer.class, false);
if(pageNum == null)
    pageNum = 1;
SortedSet<Integer> rowsOptions = PrepaidUtils.getRowOptions(rows);
if(rows == null) 
    rows = rowsOptions.first();
Results results = DataLayerMgr.executeQuery("GET_ACCOUNTS", user);
RequestUtils.setAttribute(request, "tab", "dashboard", "request"); 
RequestUtils.setAttribute(request, "extra-stylesheets", "/css/pikaday.css", "request"); 
String currencyFormat = "CURRENCY"; 
String pointsFormat = "NUMBER:#,##0";
String cardFormat = "simple.text.PadFormat:        *";
results.setFormat("cardNum", cardFormat);
results.setFormat("balance", currencyFormat);
results.setFormat("cashBackTotal", currencyFormat);
results.setFormat("replenishBonusTotal", currencyFormat);
results.setFormat("combinedPromoTotal", currencyFormat);
results.setFormat("points", pointsFormat);
//results.setFormat("onHold", currencyFormat);
//results.setFormat("pointsTotal", pointsFormat);
String currencyCd = null;
String balanceText = null;
String combinedPromoTotalText = null;
String pointsText = null;
String cashBackText = null;
%>
<jsp:include page="/account_header.jsp" />
<div class="transactions-wrap">
    <form name="transactionsForm" class="transactions-card-select form-inline">
    <div class="account-table-header">             
        <strong>Dashboard for</strong><%
int cardCount = results.getRowCount();
if(cardCount == 1) {
    results.next();
    currencyCd = results.getFormattedValue("currencyCd");
    balanceText = results.getFormattedValue("balance");
    combinedPromoTotalText = results.getFormattedValue("combinedPromoTotal");
    if(results.getValue("points") != null)
        pointsText = results.getFormattedValue("points");
    %><span class="cardNum"><%=StringUtils.prepareHTML(results.getFormattedValue("cardNum"))%></span><%
} else { %> 
        <select name="cardId" onchange="this.form.submit()">
            <option value=""<%if(cardId == null) {
                balanceText = results.getFormattedAggregate("balance", null, Aggregate.SUM);
                combinedPromoTotalText = results.getFormattedAggregate("combinedPromoTotal", null, Aggregate.SUM);
                if(results.getAggregate("points", null, Aggregate.SUM) != null)
                    pointsText = results.getFormattedAggregate("points", null, Aggregate.SUM);
                if(results.getAggregate("cashBackTotal", null, Aggregate.SUM) != null)
                	cashBackText = results.getFormattedAggregate("cashBackTotal", null, Aggregate.SUM);
                %> selected="selected"<%
            } %>>All</option><%
int i = 1;
while(results.next()) {
	String cc = results.getFormattedValue("currencyCd");
    long id = results.getValue("cardId", Long.class);
    %><option value="<%=id%>"<%
    if(cardId == null) {
    	if(currencyCd == null)
    		currencyCd = cc;
    	else if(!currencyCd.equals(cc)) {
    		currencyCd = "(mixed)";
    	}
    } else {
		if(cardId.longValue() == id) {
			currencyCd = cc;
			balanceText = results.getFormattedValue("balance");
			combinedPromoTotalText = results.getFormattedValue("combinedPromoTotal");
			if(results.getValue("points") != null)
				pointsText = results.getFormattedValue("points");
			if(results.getValue("cashBackTotal") != null)
				cashBackText = results.getFormattedValue("cashBackTotal");
            %> selected="selected"<%
		}
	}%>><%=StringUtils.prepareHTML(results.getFormattedValue("activeStatus"))%> Card <%=i++ %>: <%=StringUtils.prepareHTML(results.getFormattedValue("cardNum"))%>
	<%
	String nickName=results.getFormattedValue("nickName");
	if(!StringUtils.isBlank(nickName)){ %>
    (<%=StringUtils.prepareHTML(nickName)%>)
    <%} %>
	</option><%	
} %></select><%
}
Currency defaultCurrency = Currency.getInstance(locale);
if(currencyCd==null||currencyCd.equalsIgnoreCase(defaultCurrency.getCurrencyCode()))
    currencyCd = "";    
RequestUtils.setAttribute(request, "types", "XFABQY", "request");
%>
    </div>  <!-- /account-table-header -->
	<!-- big boxes  -->
	<div class="big-boxes">
		<div class="current-balance">
			<p>MORE Money<br /><span class="big-number"><%=StringUtils.prepareHTML(balanceText) %></span><%=StringUtils.prepareHTML(currencyCd) %></p>
		</div>
		<div class="current-reward">
			<p>Promotional Total<br /><span class="big-number"><%=StringUtils.prepareHTML(combinedPromoTotalText) %></span><%=StringUtils.prepareHTML(currencyCd) %></p>
		</div>
		<% if(pointsText != null) { %>
		<div class="current-reward">
            <p>Current Points<br /><span class="big-number"><%=StringUtils.prepareHTML(pointsText) %></span></p>
        </div><%} 
	    /* Cash Bonus is a part of Promotional Total
	    if(cashBackText != null) { %>
        <div class="current-reward">
            <p>Lifetime Cash Bonus<br /><span class="big-number"><%=StringUtils.prepareHTML(cashBackText) %></span><%=StringUtils.prepareHTML(currencyCd) %></p>
        </div><%}*/%>
	</div>
	<!-- /big-boxes  -->
<%
//For now we'll use device local time to filter transactions. In the future we may wish to use the user's timezone
results = DataLayerMgr.executeQuery("GET_ACTIVITY", form);
results.setFormat("card", cardFormat);
results.trackAggregate("purchase", Aggregate.SUM);
results.trackAggregate("replenish", Aggregate.SUM);
results.trackAggregate("discount", Aggregate.SUM);
results.setLocale(locale);
int count = results.getRowCount();
boolean hasDiscounts = (results.getAggregate("discount", null, Aggregate.SUM) != null);

List<Map<String, Object>> resultsList = new ArrayList<>();
List<Object> transIds = new ArrayList<>();

while(results.next()){
	Map<String,Object> tranInfo = new HashMap<>();
	tranInfo.put("currencyCd", results.getFormattedValue("currencyCd"));
	tranInfo.put("location", results.getFormattedValue("location"));
	tranInfo.put("address1", results.getFormattedValue("address1"));
	tranInfo.put("address2", results.getFormattedValue("address2"));
	tranInfo.put("date", results.getValue("date"));
	tranInfo.put("type", results.getFormattedValue("type").toLowerCase().replaceAll("\\W+", "-"));
	tranInfo.put("replenish", results.getValue("replenish", BigDecimal.class));
	tranInfo.put("purchase", results.getValue("purchase", BigDecimal.class));
	tranInfo.put("discount", results.getValue("discount", BigDecimal.class));
	tranInfo.put("city", results.getFormattedValue("city"));
	tranInfo.put("state", results.getFormattedValue("state"));
	tranInfo.put("postal", results.getFormattedValue("postal"));
	tranInfo.put("country", results.getFormattedValue("country"));
	tranInfo.put("tranLineItemTypeId", results.get("tranLineItemTypeId"));
	tranInfo.put("card", results.getFormattedValue("card"));
	tranInfo.put("id", results.getFormattedValue("id"));
	resultsList.add(tranInfo);
	
	BigDecimal purchaseAmount = BigDecimal.ZERO;
	BigDecimal discountAmount = BigDecimal.ZERO;
	if (tranInfo.get("purchase") != null) purchaseAmount = (BigDecimal)tranInfo.get("purchase");
	if (tranInfo.get("discount") != null) discountAmount = (BigDecimal)tranInfo.get("discount");
	if (discountAmount.compareTo(BigDecimal.ZERO)!=0){
		transIds.add(tranInfo.get("id"));	
	}
}

Results discountResults;
List<Map<String,Object>> discounts = new ArrayList<>();
Map<String, Object> params = new HashMap<>();
params.put("transIds", transIds);
	discountResults = DataLayerMgr.executeQuery("GET_DISCOUNTS_BY_TRANS_IDS", params);
	while(discountResults.next()){
		Map<String, Object> discount = new HashMap<>();
		discount.put("id", discountResults.getFormattedValue("id"));
		discount.put("amount", discountResults.getValue("discount", BigDecimal.class));
		discount.put("tranLineItemTypeId", discountResults.getFormattedValue("tranLineItemTypeId"));
		discounts.add(discount);
	}
boolean first;
%>
        <input id="static-rows" type="hidden" name="rows" value="<%=rows%>"/>
        <strong>Transaction History</strong>
    <div class="account-table-controls">
        <div class="date-range">
            Date range: <input type="text" data-toggle="calendar" data-format="mm/dd/yyyy" name="startTime" title="Enter the start date" value="<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(startTime, dateInputFormat)) %>"/>
                     to <input type="text" data-toggle="calendar" data-format="mm/dd/yyyy" name="endTime" title="Enter the end date" value="<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(endTime, dateInputFormat)) %>"/>
        </div>
        <%@ include file="pagination.frag" %>
    </div>
    <!-- /account-table-controls  -->
    <!-- TRANSACTIONS TABLE  -->
    <table class="table table-striped table-bordered-squarecorners table-hover account-table" id="accountTable">
        <thead>
            <tr>
                <th class="th-date"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Date" class="btn-link">Date</a></th>
                <%if(cardId == null && cardCount != 1) { %><th class="th-card"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Card" class="btn-link">Card</a></th><%} %>                
                <th class="th-location"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Location" class="btn-link">Location</a></th>
                <%if(hasDiscounts) { %>
                <th class="th-retail"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Retail Price" class="btn-link">$&nbsp;Retail</a></th>
                <th class="th-discount"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Discount" class="btn-link">$&nbsp;Discount</a></th><%} %>
                <th class="th-amt"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Purchase Price" class="btn-link">$&nbsp;Purchase</a></th>               
            </tr>
        </thead><%
for(int r=1; r<=resultsList.size(); r++) {
	Map<String, Object> tranInfo = resultsList.get(r-1);
	if(rows == 0) {
        if(r == 1) {%><tbody><%}
    } else if(r % rows == 1) {
        if(r > 1) {
            %></tbody><%
        }       
        int p = 1 + ((r - 1) / rows);
        %><tbody id="page_<%=p%>"<%if(p != pageNum) {%> class="hidden"<%} %>><%
    }
    currencyCd = (String)tranInfo.get("currencyCd");
    currencyFormat = (StringUtils.isBlank(currencyCd) ? "CURRENCY" : "CURRENCY:" + currencyCd);
    String[] details = {
    		(String)tranInfo.get("location"),
    		(String)tranInfo.get("address1"),
    		(String)tranInfo.get("address2"),
            null        
    };
    Object date = tranInfo.get("date");
    String type = (String)tranInfo.get("type");
    BigDecimal amount;
    if(type.endsWith("replenish"))
        amount = (BigDecimal)tranInfo.get("replenish");
    else if(type.startsWith("cash-back")) {
    	amount = null;
    	details[0] = typeLabels.get(type);
    	details[1] = "for " + ConvertUtils.formatObject((BigDecimal)tranInfo.get("purchase"), currencyFormat) + " in purchases";
    } else
        amount = (BigDecimal)tranInfo.get("purchase");
    BigDecimal discount = (BigDecimal)tranInfo.get("discount");
    String city = (String)tranInfo.get("city");
    String state = (String)tranInfo.get("state");
    String postal = (String)tranInfo.get("postal");
    String country = (String)tranInfo.get("country");
    
    StringBuilder sb = new StringBuilder();
    if(city != null && !(city=city.trim()).isEmpty()) {
        sb.append(city);
        if(state != null && !(state=state.trim()).isEmpty())
            sb.append(", ").append(state);
    } else if(state != null && !(state=state.trim()).isEmpty())
        sb.append(state);
    if(postal != null && !(postal=postal.trim()).isEmpty()) {
        if(sb.length() > 0)
            sb.append(' ');
        sb.append(postal);
    }
    if(country != null && !(country=country.trim()).isEmpty()) {
        if(sb.length() > 0)
            sb.append(' ');
        sb.append(country);
    }
    details[3] = sb.toString();
    %>
            <tr class="tran-<%=type%>-row">
                <td data-sort-value="<%=ConvertUtils.convert(Long.class, date) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(date, dateFormats[1])) %></td>
                <%if(cardId == null && cardCount != 1) { %><td data-sort-value="<%=StringUtils.prepareCDATA((String)tranInfo.get("card")) %>"><%=StringUtils.prepareHTML((String)tranInfo.get("card")) %></td><%} %>
                <td data-sort-value="<%=StringUtils.prepareCDATA(details[0]) %>"><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" data-trigger="hover&amp;click" data-content="ID: <%=StringUtils.prepareCDATA((String)tranInfo.get("id")).replace("&", "&amp;") %>&lt;br/&gt;<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(amount, currencyFormat)).replace("&", "&amp;")%><%
   for(String s : details) {
     if(s != null && !(s=s.trim()).isEmpty()) {%>&lt;br/&gt;<%=StringUtils.prepareCDATA(s).replace("&", "&amp;")%><%}
   }%>" data-title="<%=typeLabels.get(type) %> - <%=StringUtils.prepareCDATA(ConvertUtils.formatObject(date, dateFormats[0])).replace("&", "&amp;")%>"><%=StringUtils.isBlank(details[0]) ? "Unspecified" : StringUtils.prepareCDATA(details[0])%></a></td>
                <%if(hasDiscounts) { 
   BigDecimal retail = (discount == null || discount.signum() == 0 || amount == null ? amount : amount.subtract(discount));%>
                <td data-sort-value="<%=SystemUtils.nvl(retail, BigDecimal.ZERO) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(retail, numberFormat)) %></td>
                <td data-sort-value="<%=SystemUtils.nvl(discount, BigDecimal.ZERO) %>">
                	<div>
                	<%  int discountCount = 0;
                		for (int i=0; i<discounts.size(); i++) {%>
                			<%if (((String)(discounts.get(i).get("id"))).equals((String)tranInfo.get("id"))){
                				discountCount++;
                				BigDecimal tranDiscount = (BigDecimal)(discounts.get(i).get("amount"));
                				int tranLineTypeId=ConvertUtils.getIntSafely(discounts.get(i).get("tranLineItemTypeId"), -1);
    							String discountType="";
    							if(tranLineTypeId==204||tranLineTypeId==211){
    								discountType="(loyalty)";
    							}else if (tranLineTypeId==213){
    									discountType="(purchase)";
    							}else if (tranLineTypeId==214||tranLineTypeId==215){
    									discountType="(free)";
    							}%>
                				<div data-sort-value="<%=SystemUtils.nvl(tranDiscount, BigDecimal.ZERO) %>"><%=(tranDiscount == null || tranDiscount.signum() == 0 ? "-" : StringUtils.prepareHTML(ConvertUtils.formatObject(tranDiscount, numberFormat))+" "+discountType) %></div>
                			<%} %>
 						<%} %>
 					<%if (discountCount == 0){%>
 						<%=(discount == null || discount.signum() == 0 ? "-" : StringUtils.prepareHTML(ConvertUtils.formatObject(discount, numberFormat))) %>
 					<%} %>
 					</div>	               
                </td><% 
                }%>
                <td data-sort-value="<%=SystemUtils.nvl(amount, BigDecimal.ZERO) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(amount, numberFormat)) %> <%if(type.equalsIgnoreCase("free")){ %>(Free)<%} %></td>
            </tr><%
}
if(results.getRowCount() == 0) {
%><tbody><tr class="no-rows-found"><td colspan="5">No transactions in this time period</td></tr><%
}%>
        </tbody>
    </table>
    <div class="account-table-controls">
        <%@ include file="pagination.frag" %>
    </div>
    </form>
    <!-- /account-table-controls  -->
<script type="text/javascript">  
    var currPage;
    var currBtn;
    window.addEvent("domready", function() {
        currPage = $("page_<%=pageNum%>");
        currBtn = $$(".page_button_<%=pageNum%>");
        [$(document.transactionsForm.startTime), $(document.transactionsForm.endTime)].each(function(input) {
            if(input.pikaday)
                input.addEvent("change", function(evt) { 
                    if(input.pikaday._d != input.originalDate)
                        input.form.submit();
                });
            input.addEvent("keypress", function(evt) { 
                if(evt.code == 13) {
                    evt.preventDefault();
                    if(evt.target) {
                        evt.target.blur();
                    }
                }
            });
        });
    });
    function viewPage(pageNum) {
    	var newPage = $("page_" + pageNum);
    	var newBtn = $$(".page_button_" + pageNum);
    	currPage.addClass("hidden");
        newPage.removeClass("hidden");
        currPage = newPage;
        currBtn.set("disabled", false);
        newBtn.set("disabled", true);
        currBtn = newBtn; 
        $("accountTable").scrollIntoView();
    }
</script>
</div>
<!-- /transactions-wrap -->
<div class="promo-column">
    <jsp:include page="/promo.jsp" />
</div>
<!-- /promo-column -->
<jsp:include page="/account_footer.jsp" />