<%@page import="simple.io.Log"%>
<%@page import="com.usatech.layers.common.sprout.SproutTransaction"%>
<%@page import="com.usatech.prepaid.web.SproutUSATTran"%>
<%@page import="com.usatech.prepaid.web.SproutComboTran"%>
<%@page import="java.util.List"%>
<%@page import="com.usatech.prepaid.web.SproutComboTranInfo"%>
<%@page import="com.usatech.layers.common.sprout.SproutAccountResponse"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.lang.SystemUtils"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="java.util.TimeZone"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results.Aggregate"%>
<%@page import="java.util.Currency"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<%!
Map<String,String> typeLabels = getTypeLabels();
Map<String,String> getTypeLabels() {
    Map<String,String> typeLabels = new LinkedHashMap<String,String>();
    typeLabels.put("purchase", "Transaction");
    typeLabels.put("refund", "Refund");
    typeLabels.put("load", "Replenishment");
    typeLabels.put("pending-replenish", "In-process Replenishment");
    typeLabels.put("pending-purchase", "Pending Transaction");
    typeLabels.put("cash-back", "Bonus Cash");
    typeLabels.put("adjustment", "Adjustment");
    typeLabels.put("pending-adjustment", "Pending Adjustment");
    return typeLabels;
}
%>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
if(user == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-logged-in", "You are no longer logged in. Please log in again.");
    RequestUtils.redirectWithCarryOver(request, response, "signin.html");
    return;
}
Long cardId = RequestUtils.getAttribute(request, "cardId", Long.class, false);
if(cardId != null && PrepaidUtils.checkCardPermission(user, cardId) == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
    RequestUtils.redirectWithCarryOver(request, response, PrepaidUtils.getHomePage());
    return;
}
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
Locale locale = RequestUtils.getLocale(request);
TimeZone timeZone = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_TIME_ZONE, TimeZone.class, false, RequestUtils.SESSION_SCOPE);
String numberFormat = "NUMBER:#0.00;(#0.00)";
DateFormat[] dateFormats = PrepaidUtils.getStandardDateFormats(locale, timeZone);
String dateInputFormat = "DATE:yyyy-MM-dd";
Date startTime = RequestUtils.getAttribute(request, "startTime", Date.class, false);
if(startTime == null) {
    startTime = new Date(System.currentTimeMillis() - (30*24*60*60*1000L));
    RequestUtils.setAttribute(request, "startTime", startTime, "request");
}
Date endTime = RequestUtils.getAttribute(request, "endTime", Date.class, false);
if(endTime == null) {
    endTime = new Date();
    RequestUtils.setAttribute(request, "endTime", endTime, "request");
}
Integer rows = PrepaidUtils.getRowSelection(request);
Integer pageNum = RequestUtils.getAttribute(request, "page", Integer.class, false);
if(pageNum == null)
    pageNum = 1;
SortedSet<Integer> rowsOptions = PrepaidUtils.getRowOptions(rows);
if(rows == null) 
    rows = rowsOptions.first();
Results results = DataLayerMgr.executeQuery("GET_ACCOUNTS", user);
RequestUtils.setAttribute(request, "tab", "dashboard", "request"); 
RequestUtils.setAttribute(request, "extra-stylesheets", "/css/pikaday.css", "request"); 
String currencyFormat = "CURRENCY"; 
String pointsFormat = "NUMBER:#,##0";
String cardFormat = "simple.text.PadFormat:        *";
results.setFormat("cardNum", cardFormat);
results.setFormat("balance", currencyFormat);
results.setFormat("cashBackTotal", currencyFormat);
results.setFormat("replenishBonusTotal", currencyFormat);
results.setFormat("combinedPromoTotal", currencyFormat);
results.setFormat("points", pointsFormat);
String currencyCd = null;
String balanceText = null;
String combinedPromoTotalText = null;
String pointsText = null;
String cashBackText = null;
String sproutAccountId=null;
%>
<jsp:include page="/account_header.jsp" />
<div class="transactions-wrap">
    <form name="transactionsForm" class="transactions-card-select form-inline">
    <div class="account-table-header">             
        <strong>Dashboard for</strong><%
int cardCount = results.getRowCount();
boolean isSproutApiFailed=false;
if(cardCount == 1) {
    results.next();
    currencyCd = results.getFormattedValue("currencyCd");
    sproutAccountId=results.getFormattedValue("consumerAcctIdentifier");
    SproutAccountResponse sproutAcct=null;
    try{
    	sproutAcct=PrepaidUtils.sproutUtils.getAccount(sproutAccountId);
    	balanceText = sproutAcct.getAvailable_balance().toString();
    	pointsText = String.valueOf(sproutAcct.getPoints_balance());
    }catch(Exception e){
    	log.info("Failed to retrieve sprout account info",e);
    	isSproutApiFailed=true;
    }
    combinedPromoTotalText = results.getFormattedValue("combinedPromoTotal");
    cardId=ConvertUtils.getLong(results.get("cardId"));
    %><span class="cardNum"><%=StringUtils.prepareHTML(results.getFormattedValue("cardNum"))%></span><%
} else { %> 
        <select name="cardId" onchange="this.form.submit()">
           <%
int i = 1;
while(results.next()) {
	String cc = results.getFormattedValue("currencyCd");
    long id = results.getValue("cardId", Long.class);
    %><option value="<%=id%>"<%
    if(cardId == null) {
    	currencyCd = cc;
    	if(i==1){
    		cardId=ConvertUtils.getLong(results.get("cardId"));
    		sproutAccountId=results.getFormattedValue("consumerAcctIdentifier");
    	    SproutAccountResponse sproutAcct=null;
    	    try{
    	    	sproutAcct=PrepaidUtils.sproutUtils.getAccount(sproutAccountId);
    	    	balanceText = sproutAcct.getAvailable_balance().toString();
    	    	pointsText = String.valueOf(sproutAcct.getPoints_balance());
    	    }catch(Exception e){
    	    	log.info("Failed to retrieve sprout account info",e);
    	    	isSproutApiFailed=true;
    	    }
    	    combinedPromoTotalText = results.getFormattedValue("combinedPromoTotal");
    	}
    } else {
		if(cardId.longValue() == id) {
			currencyCd = cc;
			sproutAccountId=results.getFormattedValue("consumerAcctIdentifier");
    	    SproutAccountResponse sproutAcct=null;
    	    try{
    	    	sproutAcct=PrepaidUtils.sproutUtils.getAccount(sproutAccountId);
    	    	balanceText = sproutAcct.getAvailable_balance().toString();
    	    	pointsText = String.valueOf(sproutAcct.getPoints_balance());
    	    }catch(Exception e){
    	    	log.info("Failed to retrieve sprout account info",e);
    	    	isSproutApiFailed=true;
    	    }
			combinedPromoTotalText = results.getFormattedValue("combinedPromoTotal");
			if(results.getValue("cashBackTotal") != null)
				cashBackText = results.getFormattedValue("cashBackTotal");
            %> selected="selected"<%
		}
	}%>><%=StringUtils.prepareHTML(results.getFormattedValue("activeStatus"))%> Card <%=i++ %>: <%=StringUtils.prepareHTML(results.getFormattedValue("cardNum"))%>
	<%
	String nickName=results.getFormattedValue("nickName");
	if(!StringUtils.isBlank(nickName)){ %>
    (<%=StringUtils.prepareHTML(nickName)%>)
    <%} %>
	</option><%	
} %></select><%
}
Currency defaultCurrency = Currency.getInstance(locale);
if(currencyCd.equalsIgnoreCase(defaultCurrency.getCurrencyCode()))
    currencyCd = "";    
RequestUtils.setAttribute(request, "types", "XFABQY", "request");
%>
    </div>  <!-- /account-table-header -->
	<!-- big boxes  -->
	<div class="big-boxes">
		<div class="current-balance">
			<p>MORE Money<br /><span class="big-number"><%=StringUtils.prepareHTML(balanceText) %></span><%=StringUtils.prepareHTML(currencyCd) %></p>
		</div>
		<div class="current-reward">
			<p>Promotional Total<br /><span class="big-number"><%=StringUtils.prepareHTML(combinedPromoTotalText) %></span><%=StringUtils.prepareHTML(currencyCd) %></p>
		</div>
		<% if(pointsText != null) { %>
		<div class="current-reward">
            <p>Current Points<br /><span class="big-number"><%=StringUtils.prepareHTML(pointsText) %></span></p>
        </div><%} 
	    /* Cash Bonus is a part of Promotional Total
	    if(cashBackText != null) { %>
        <div class="current-reward">
            <p>Lifetime Cash Bonus<br /><span class="big-number"><%=StringUtils.prepareHTML(cashBackText) %></span><%=StringUtils.prepareHTML(currencyCd) %></p>
        </div><%}*/%>
	</div>
	<!-- /big-boxes  -->
<%
//For now we'll use device local time to filter transactions. In the future we may wish to use the user's timezone
SproutComboTranInfo tranInfo=null;
try{
	tranInfo=PrepaidUtils.getSproutActivity(cardId, user.getConsumerId(), startTime, endTime, sproutAccountId);
List<SproutComboTran> tranList=tranInfo.getSproutComboTran();
int count = tranList.size();
boolean hasDiscounts = tranInfo.hasDiscount();
boolean first;
%>
        <input id="static-rows" type="hidden" name="rows" value="<%=rows%>"/>
        <strong>Transaction History</strong>
    <div class="account-table-controls">
        <div class="date-range">
            Date range: <input type="text" data-toggle="calendar" data-format="mm/dd/yyyy" name="startTime" title="Enter the start date" value="<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(startTime, dateInputFormat)) %>"/>
                     to <input type="text" data-toggle="calendar" data-format="mm/dd/yyyy" name="endTime" title="Enter the end date" value="<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(endTime, dateInputFormat)) %>"/>
        </div>
        <%@ include file="pagination.frag" %>
    </div>
    <!-- /account-table-controls  -->
    <!-- TRANSACTIONS TABLE  -->
    <table class="table table-striped table-bordered-squarecorners table-hover account-table" id="accountTable">
        <thead>
            <tr>
                <th class="th-date"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Date" class="btn-link">Date</a></th>
                <%if(cardId == null && cardCount != 1) { %><th class="th-card"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Card" class="btn-link">Card</a></th><%} %>                
                <th class="th-location"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Location" class="btn-link">Location</a></th>
                <%if(hasDiscounts) { %>
                <th class="th-retail"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Retail Price" class="btn-link">$&nbsp;Retail</a></th>
                <th class="th-discount"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Discount" class="btn-link">$&nbsp;Discount</a></th><%} %>
                <th class="th-amt"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Purchase Price" class="btn-link">$&nbsp;Purchase</a></th> 
            </tr>
        </thead><%
for(int r=1;r<tranList.size()+1;r++) {
    if(rows == 0) {
        if(r == 1) {%><tbody><%}
    } else if(r % rows == 1) {
        if(r > 1) {
            %></tbody><%
        }       
        int p = 1 + ((r - 1) / rows);
        %><tbody id="page_<%=p%>"<%if(p != pageNum) {%> class="hidden"<%} %>><%
    }
    currencyCd = "USD";
    currencyFormat = (StringUtils.isBlank(currencyCd) ? "CURRENCY" : "CURRENCY:" + currencyCd);
    String[] details =new String[4];
    SproutTransaction sproutTran=tranList.get(r-1).getSproutTran();
    SproutUSATTran usatTran=tranList.get(r-1).getUsatTran();
    if(tranList.get(r-1).isSproutOnUSATDevice()){
	    details[0]=usatTran.getLocation();
	    details[1]= usatTran.getAddress1();
	    details[2]=usatTran.getAddress2();
    }
    Object date = sproutTran.getCreatedDate();
    String type = sproutTran.getTransaction_type().toLowerCase().replaceAll("\\W+", "-");
    BigDecimal amount;
    if(type.endsWith("load"))
        amount = sproutTran.getTransaction_amount();
    else if(type.startsWith("cash-back")) {
    	amount = null;
    	details[0] = typeLabels.get(type);
    	details[1] = "for " + ConvertUtils.formatObject(sproutTran.getTransaction_amount(), currencyFormat) + " in purchases";
    } else
        amount = sproutTran.getTransaction_amount();
    BigDecimal discount = null;
    String city = null;
    String state = null;
    String postal = null;
    String country = null;
    String tranId="";
    String externalReferenceId=sproutTran.getExternal_reference_id();
    if(usatTran!=null){
	    discount = usatTran.getDiscount();
	    city = usatTran.getCity();
	    state = usatTran.getState();
	    postal = usatTran.getPostal();
	    country = usatTran.getCountry();
	    tranId=String.valueOf(usatTran.getId());
    }else{
    	tranId=String.valueOf(sproutTran.getTransaction_id());
    }
    
    StringBuilder sb = new StringBuilder();
    if(city != null && !(city=city.trim()).isEmpty()) {
        sb.append(city);
        if(state != null && !(state=state.trim()).isEmpty())
            sb.append(", ").append(state);
    } else if(state != null && !(state=state.trim()).isEmpty())
        sb.append(state);
    if(postal != null && !(postal=postal.trim()).isEmpty()) {
        if(sb.length() > 0)
            sb.append(' ');
        sb.append(postal);
    }
    if(country != null && !(country=country.trim()).isEmpty()) {
        if(sb.length() > 0)
            sb.append(' ');
        sb.append(country);
    }
    details[3] = sb.toString();
    
    %>
            <tr class="tran-<%=type%>-row">
                <td data-sort-value="<%=ConvertUtils.convert(Long.class, date) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(date, dateFormats[1])) %></td>
                <%if(cardId == null && cardCount != 1) { %><td data-sort-value="<%=StringUtils.prepareCDATA(results.getFormattedValue("cardNum")) %>"><%=StringUtils.prepareHTML(results.getFormattedValue("cardNum")) %></td><%} %>
                <td data-sort-value="<%=StringUtils.prepareCDATA(details[0]) %>"><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" data-trigger="hover&amp;click" data-content="ID: <%=StringUtils.prepareCDATA(tranId).replace("&", "&amp;") %>&lt;br/&gt;External Reference ID: <%=StringUtils.prepareCDATA(externalReferenceId).replace("&", "&amp;") %>&lt;br/&gt;<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(amount, currencyFormat)).replace("&", "&amp;")%><%
   for(String s : details) {
     if(s != null && !(s=s.trim()).isEmpty()) {%>&lt;br/&gt;<%=StringUtils.prepareCDATA(s).replace("&", "&amp;")%><%}
   }%>" data-title="<%=typeLabels.get(type) %> - <%=StringUtils.prepareCDATA(ConvertUtils.formatObject(date, dateFormats[0])).replace("&", "&amp;")%>"><%=StringUtils.isBlank(details[0]) ? "Unspecified" : StringUtils.prepareCDATA(details[0])%></a></td>
                <%if(hasDiscounts) { 
   BigDecimal retail = (discount == null || discount.signum() == 0 || amount == null || amount.signum() == 0 ? amount : amount.add(discount));%>
                <td data-sort-value="<%=SystemUtils.nvl(retail, BigDecimal.ZERO) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(retail, numberFormat)) %></td>
                <td data-sort-value="<%=SystemUtils.nvl(discount, BigDecimal.ZERO) %>"><%=(discount == null || discount.signum() == 0 ? "-" : StringUtils.prepareHTML(ConvertUtils.formatObject(discount, numberFormat))) %></td><%} %>
                <td data-sort-value="<%=SystemUtils.nvl(amount, BigDecimal.ZERO) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(amount, numberFormat)) %></td>
            </tr><%
}
if(tranList.size() == 0) {
%><tbody><tr class="no-rows-found"><td colspan="5">No transactions in this time period</td></tr><%
}%>
        </tbody>
    </table>
    <div class="account-table-controls">
        <%@ include file="pagination.frag" %>
    </div>
    <% 
}catch(Exception e){
	%>
	<table><tbody><tr class="no-rows-found"><td colspan="5">Failed to retrieve information from Sprout. Please retry the page.</td></tr></tbody></table>
	<%
	log.info("Failed to retrieve sprout account activity",e);
}
%>
    </form>
    <!-- /account-table-controls  -->
<script type="text/javascript">  
    var currPage;
    var currBtn;
    window.addEvent("domready", function() {
        currPage = $("page_<%=pageNum%>");
        currBtn = $$(".page_button_<%=pageNum%>");
        [$(document.transactionsForm.startTime), $(document.transactionsForm.endTime)].each(function(input) {
            if(input.pikaday)
                input.addEvent("change", function(evt) { 
                    if(input.pikaday._d != input.originalDate)
                        input.form.submit();
                });
            input.addEvent("keypress", function(evt) { 
                if(evt.code == 13) {
                    evt.preventDefault();
                    if(evt.target) {
                        evt.target.blur();
                    }
                }
            });
        });
    });
    function viewPage(pageNum) {
    	var newPage = $("page_" + pageNum);
    	var newBtn = $$(".page_button_" + pageNum);
    	currPage.addClass("hidden");
        newPage.removeClass("hidden");
        currPage = newPage;
        currBtn.set("disabled", false);
        newBtn.set("disabled", true);
        currBtn = newBtn; 
        $("accountTable").scrollIntoView();
    }
</script>
</div>
<!-- /transactions-wrap -->
<div class="promo-column">
    <jsp:include page="/promo.jsp" />
</div>
<!-- /promo-column -->
<jsp:include page="/account_footer.jsp" />