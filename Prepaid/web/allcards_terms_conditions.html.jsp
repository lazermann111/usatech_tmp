<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%
RequestUtils.setAttribute(request, "subtitle", "Terms & Conditions", "request");
RequestUtils.setAttribute(request, "wrapNarrow", true, "request");
Translator translator = RequestUtils.getTranslator(request);
%>
<jsp:include page="/header.jsp"/>
<h2><%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%> Loyalty &amp; Rewards Program - TERMS AND CONDITIONS.</h2>
<h3>EFFECTIVE May 1, 2015</h3>
<p>
There are no fees associated with the Loyalty &amp; Rewards Program (the &quot;Program&quot;). The Program becomes active no later than the end of the next business day after completing registration.</p>
<p>
The following terms and conditions (&quot;Agreement&quot;) govern the use of the Program. In this Agreement, the words &quot;you&quot; and &quot;your&quot; mean the person who enrolls in the Program. The words &quot;we,&quot; &quot;us,&quot; &quot;our&quot; refer to USA Technologies, Inc., a Pennsylvania corporation. &quot;Card&quot; means the credit or debit card used to enroll in the Program. &quot;Vending Operator&quot; means the owner or operator, or the agent of the owner or operator of vending machines, kiosks/micromarkets or point-of-sale dining locations that are participating in the Program. The terms of this Agreement are binding upon us and you, and will be deemed to have been accepted by you if you or any person authorized by you, use(s) or register(s) the Card.</p>
<p>
<strong>Please read this Agreement carefully, keep it and provide it to anyone to whom the Card is given.</strong></p>
<h3>How the Program Works.</h3>
<p>
By enrolling your Card in the Program, you will be able to participate in certain promotions, as determined and managed by the Vending Operator, including, but not limited to, product discounts and promotional incentives for the purchase of certain specific items from specific vending machines, kiosks/micromarkets or point-of-sale dining locations.
</p>
Step 1: Go to the website <a href="./index.html">getmore.usatech.com</a> and enroll your Card in the Program by creating an account and registering any debit or credit card, including a Visa, MasterCard, Discover or American Express card, for use in the Program. Remember to include the Promotional Code provided by the Operator that is displayed on the vending machine, the kiosk or at the register.
<p>
Step 2: Visit any Program participating location, and purchase a product that is eligible for a Program promotion or discount using the Card that you registered at the getmore.usatech.com website. Any discounts will be applied at the time of the purchase.
</p>
<p>
For an up-to-date list of locations participating in the Program and products that are eligible for a Program promotion, call (888) 561-4748 or visit <a href="./index.html">getmore.usatech.com</a>. We reserve the right not to accept any Card or otherwise limit use of a Card if we reasonably believe that the use is unauthorized, fraudulent or otherwise unlawful.
</p>
<p>
You acknowledge and agree that the amount available to make purchases using the Card is limited to the balance or credit line on the Card, as the case may be. Further, you acknowledge that you will continue to be subject to the rules or terms and conditions set forth in the applicable cardholder agreement with the financial institution that has issued the Card to you. The amount and term of any and all promotions are determined solely by the Vending Operator.
</p>
<p>
To obtain a summary of your prior purchases under the Program, log into your account at <a href="./index.html">getmore.usatech.com</a>. You will not receive a statement or written summary of transactions.
</p>
<h4>Arbitration.</h4>
<p>
You must first present any claim or dispute to us by contacting our Customer Service department at (888) 561-4748 to allow us an opportunity to resolve the dispute. You may request arbitration if your claim or dispute cannot be resolved within 60 days. The arbitration of any dispute or claim shall be conducted in accordance with the Rules of the American Arbitration Association (&quot;AAA&quot;). Unless you and we agree otherwise, any arbitration will take place in Philadelphia, Pennsylvania. Any arbitration shall be confidential, and neither you nor we may disclose the existence, content or results of any arbitration, except as may be required by law or for purposes of enforcement of the arbitration award. Judgment on any arbitration award may be entered in any court having proper jurisdiction. All administrative fees and expenses of an arbitration will be divided equally between you and us. In all arbitrations, each party will bear the expense of its own counsel, experts, witnesses and preparation and presentation of evidence at the arbitration.
</p>
<h4>Waiver of Punitive Damage Claims and Class Actions.</h4>
<p>
Both you and we are waiving certain rights to litigate disputes in court. If for any reason this arbitration clause is deemed inapplicable or invalid, you and we both waive, to the fullest extent allowed by law, any claims to recover punitive or exemplary damages and any right to pursue any claims on a class or consolidated basis or in a representative capacity.
</p>
<h4>Returns and Exchanges.</h4>
<p>
USA Technologies, employees and agents are not responsible for the services or merchandise purchased with the Card, and return and refund policies are dependent on the Vending Operator. By enrolling your Card in the Program, you agree that USAT is not liable for any direct or indirect damages, consequential or otherwise,. If you think an error has occurred involving a transaction, the error needs to be adjusted and resolved with the Vending Operator, and/or the financial institution that has issued your Card. 
</p>
<h4>Program.</h4>
<p>
USAT may change the terms of, add new terms to, or discontinue the Program at any time and without giving you prior notice, subject to applicable law. You may check the most recent terms and conditions of the Program by logging onto <a href="./index.html">getmore.usatech.com</a>.
</p>
<h3>Governing Law; Venue; Severability.</h3>
<p>
This Agreement and its performance shall be governed by the laws and regulations of the United States and, to the extent not governed by federal laws and regulations, by the laws and regulations of the Commonwealth of Pennsylvania, notwithstanding its conflict of laws principles. If any term of this Agreement is found by a court or arbitrator to be unenforceable or illegal, all other terms and conditions will still be in effect.
</p>
<h3>Questions or Concerns.</h3>
<p>
If you have any questions concerning the Program or your Card, please write to us at USA Technologies, Inc., 100 Deerfield Lane, Suite 300, Malvern, PA 19355 USA or visit us at <a href="./index.html">getmore.usatech.com</a>. You may also call us at (888) 561-4748.
</p>
<hr/>
<jsp:include page="/footer.jsp"/>