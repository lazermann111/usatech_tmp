<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.io.Log,simple.servlet.InputForm"%>
<%@page import="java.sql.Connection,java.sql.SQLException,simple.db.DataLayerMgr,simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
String firstname = RequestUtils.getAttribute(request, "firstname", String.class, true);
String lastname = RequestUtils.getAttribute(request, "lastname", String.class, true);
if(StringUtils.isBlank(firstname) ||
        StringUtils.isBlank(lastname)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
    RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
} else {
	InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
	form.setAttribute("consumerId", user.getConsumerId());    
	Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_USER_SIMPLE");
	try {
		try {
	    	DataLayerMgr.executeCall(conn, "UPDATE_USER_SIMPLE", form);
	    } catch(SQLException e) {
	    	try {
	    		conn.rollback();
	    	} catch(SQLException e2) {
	    		//ignore
	    	}
	    	if(log.isWarnEnabled())
	              log.warn("Could not update simple user firstname=" + firstname+" lastname="+lastname, e);
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
	    	RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
	    	return;
	    }
		user.setFullName((firstname + ' ' + lastname).trim());        
        conn.commit();
        if(log.isInfoEnabled())
              log.info("Updated prepaid account for user '" + user.getUserName() + ": consumer_id=" + form.getAttribute("consumerId"));
		RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-user-updated", "Successfully updated this account", "");         
		RequestUtils.redirectWithCarryOver(request, response, "settings.html");
	} finally {
		conn.close();
	}
}%>