<%@page import="com.usatech.prepaid.web.ConsumerAccount"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="simple.lang.Holder"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.io.Log,simple.translator.Translator"%>
<%@page import="java.sql.SQLException,simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<%
String card = RequestUtils.getAttribute(request, "card", String.class, false);
String securityCode = RequestUtils.getAttribute(request, "securityCode", String.class, false);
String firstname = RequestUtils.getAttribute(request, "firstname", String.class, false);
String lastname = RequestUtils.getAttribute(request, "lastname", String.class, false);
Long region = RequestUtils.getAttribute(request, "region", Long.class, false);
Translator translator = RequestUtils.getTranslator(request);
if(StringUtils.isBlank(card) ||
		StringUtils.isBlank(securityCode) ||
		StringUtils.isBlank(firstname) ||
		StringUtils.isBlank(lastname)) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
    RequestUtils.redirectWithCarryOver(request, response, "signup_simple.html", false, true);
    return;
}
Holder<String> cardNumHolder = new Holder<String>();
if(!PrepaidUtils.checkCardNum(card, cardNumHolder, true)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-prepaid-card", "You entered an invalid prepaid card number. Please enter a valid one.");
    RequestUtils.redirectWithCarryOver(request, response, "signup_simple.html", false, true);
    return;
}
long globalAccountId;
try {
	globalAccountId = EC2ClientUtils.getGlobalAccountId(cardNumHolder.getValue());
} catch(Throwable e) {
	log.warn("Could not get globalAccountId", e);
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
    RequestUtils.redirectWithCarryOver(request, response, "signup_simple.html", false, true);
    return;
}
ConsumerAccount consumerAcct;
try {
	consumerAcct = PrepaidUtils.createUserSimple(translator, globalAccountId, securityCode, firstname, lastname, region, RequestUtils.getBaseUrl(request), request);		    
} catch(SQLException e) {
   	switch(e.getErrorCode()) {
   			case 20300:
               	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it", firstname);
               	break;
        	case 20301:
   				RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-already-registered", "You have already registered this card. Please enter a different card", firstname);
               	break;
        	case 20302:
                RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-registered-to-other", "This card has been registered under a different first name and last name. Please contact Customer Service for assistance", firstname);
                break;
           default:
           	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
   	}
   	RequestUtils.redirectWithCarryOver(request, response, "signup_simple.html", false, true);
   	return;
}
RequestUtils.setAttribute(request, "subtitle", "Almost Done", "request"); 
%>
<jsp:include page="/header.jsp"/>
<div class="billboard">
  <div class="text-center">
    Your MORE account is now activated! You can <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signin.html") %>">Sign in</a> using your 19-digit Card Number and security code. 
  </div>
</div> <!-- /billboard -->
<hr/>
<jsp:include page="/footer.jsp"/>