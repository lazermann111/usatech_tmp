<%@page import="simple.io.Log"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
Integer fromConsumerAcctTypeId = RequestUtils.getAttribute(request, "fromConsumerAcctTypeId", Integer.class, false);
if(user == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-logged-in", "You are no longer logged in. Please log in again.");
    RequestUtils.redirectWithCarryOver(request, response, "signin.html");
    return;
}
Long cardId = RequestUtils.getAttribute(request, "cardId", Long.class, true);
if(cardId != null && PrepaidUtils.checkCardPermission(user, cardId) == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
    RequestUtils.redirectWithCarryOver(request, response, PrepaidUtils.getHomePage());
    return;
}
String nickname = RequestUtils.getAttribute(request, "nickname", String.class, true);
Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_CARD_NICKNAME");
try {
    try {
    	if(!StringUtils.isBlank(nickname)){
    		nickname=nickname.trim();
    	}
    	DataLayerMgr.executeCall(conn, "UPDATE_CARD_NICKNAME", new Object[]{nickname, cardId});
    } catch(SQLException e) {
    	try {
    		conn.rollback();
    	} catch(SQLException e2) {
    		//ignore
    	}
    	if(log.isWarnEnabled())
              log.warn("Could not update card nickname cardId=" + cardId+" nickname="+nickname, e);
    	if(e.getErrorCode()==1){
    		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-nickname-unique", "The nickname needs to be unique. Please try with a different nickname.");   
    	}else{
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
    	}
    	if(fromConsumerAcctTypeId!=null&&fromConsumerAcctTypeId==5){
    		RequestUtils.redirectWithCarryOver(request, response, "allcards.html?cardId="+cardId);
    	}else if(fromConsumerAcctTypeId!=null&&fromConsumerAcctTypeId==7) {
    		RequestUtils.redirectWithCarryOver(request, response, "payroll_deduct_cards.html?cardId="+cardId);
    	}else {
    		RequestUtils.redirectWithCarryOver(request, response, "cards.html?cardId="+cardId);
    	}
    	return;
    }
    conn.commit();
    if(log.isInfoEnabled())
          log.info("Updated prepaid card nickname user '" + user.getUserName() + ": cardId=" + cardId);
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-nickname-updated", "Successfully updated the card nickname", "");
	if(fromConsumerAcctTypeId!=null&&fromConsumerAcctTypeId==5){
		RequestUtils.redirectWithCarryOver(request, response, "allcards.html?cardId="+cardId);
	}else if(fromConsumerAcctTypeId!=null&&fromConsumerAcctTypeId==7) {
		RequestUtils.redirectWithCarryOver(request, response, "payroll_deduct_cards.html?cardId="+cardId);
	}else {
		RequestUtils.redirectWithCarryOver(request, response, "cards.html?cardId="+cardId);
	}
} finally {
	conn.close();
}
%>