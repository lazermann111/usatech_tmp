<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Add MORE to your mobile wallet</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link  rel='stylesheet' href='/democss/fonts.css'>
        <!-- Place favicon.ico in the root directory -->

<style type="text/css">
* {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  font-weight: 300;
}
body {
  font-family: 'Source Sans Pro', sans-serif;
  color: #222222;
  font-weight: 300;
  background:  #ffffff;
  font: 16px/1 'Open Sans', sans-serif;
}
.css-carousel {
    width: 100%;
    max-width:  700px;
    margin:  50px auto;
     height: 900px;  /* Height of images */
    position: relative;
    overflow: hidden;
    border:  solid 0px red;
    text-align: center;
}
.css-carousel .css-img {
    width: 100%;
    max-width: 700px;
    position: absolute;
    left: 0;
    right:  0;
    opacity: 0;
    -webkit-animation: css-carousel-fade 30s linear infinite;
    -moz-animation: css-carousel-fade 30s linear infinite;
    -ms-animation: css-carousel-fade 30s linear infinite;
    animation: css-carousel-fade 30s linear infinite;
}
.css-carousel .css-img:nth-child(2) {
    -webkit-animation-delay: 6s;
    -moz-animation-delay: 6s;
    -ms-animation-delay: 6s;
    animation-delay: 6s;
}
.css-carousel .css-img:nth-child(3) {
    -webkit-animation-delay: 12s;
    -moz-animation-delay: 12s;
    -ms-animation-delay: 12s;
    animation-delay: 12s;
}
.css-carousel .css-img:nth-child(4) {
    -webkit-animation-delay: 18s;
    -moz-animation-delay: 18s;
    -ms-animation-delay: 18s;
    animation-delay: 18s;
}
.css-carousel .css-img:nth-child(5) {
    -webkit-animation-delay: 24s;
    -moz-animation-delay: 24s;
    -ms-animation-delay: 24s;
    animation-delay: 24s;
}
/* .css-carousel .css-img:nth-child(6) {
    -webkit-animation-delay: 25s;
    -moz-animation-delay: 25s;
    -ms-animation-delay: 25s;
    animation-delay: 25s;
} */
@-webkit-keyframes css-carousel-fade {
    0%, 20%, 100% { opacity: 0; }
    5%, 15% { opacity: 1;}
}
@-moz-keyframes css-carousel-fade {
    0%, 20%, 100% { opacity: 0; }
    5%, 15% { opacity: 1;}
}
@-ms-keyframes css-carousel-fade {
    0%, 20%, 100% { opacity: 0; }
    5%, 15% { opacity: 1;}
}
@keyframes css-carousel-fade {
    0%, 20%, 100% { opacity: 0; }
    5%, 15% { opacity: 1;}
}
	/* =================================================
   For tab
   ================================================= */

#topnavtabsdiv {
  margin:  0 auto;

}
/* Style the list */
ul.topnavtabs {
    list-style-type: none;
    padding: 0 5%;
    overflow: hidden;
}

/* Float the list items side by side */
ul.topnavtabs li {float: left;
border-bottom:  solid 1px #ddd;
}

/* Style the links inside the list items */
ul.topnavtabs li a {
     text-align: center;
    text-decoration: none;
    transition: 0.3s;
      display: inline-block;
  margin: 0;
  padding: 15px 25px;
  font-weight: 600;
  text-align: center;
  color: #888;
  border:  solid 1px transparent;
  border-top: solid 6px transparent;
}

/* Change background color of links on hover */
ul.topnavtabs li a:hover {
  color: #888;
  cursor: pointer;
}

/* Create an active/current tablink class */
ul.topnavtabs li a:focus {
  background-color: #fff;
  color: #555;
  border: 1px solid #ddd;
  border-top: solid 6px #588fcc;
  border-bottom:  solid 1px #fff;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 0;
    height:  100%;
}
	</style>
	<script>
	function openTab(pageNum){
		if(pageNum==1){
			window.location.href="walletDemo.html";
		}else if(pageNum==2){
			window.location.href="walletDemo2.html";
		}
	}
	</script>
  </head>

    <body>
<div id="topnavtabsdiv">
<form id="navtabsform">
<ul class="topnavtabs">
  <li><a href="javascript:void(0)" onclick="openTab(1)" style="font-family: 'Source Sans Pro', sans-serif;">Add MORE card to mobile wallet</a></li>
  <li><a href="javascript:void(0)" onclick="openTab(2)" style="font-family: 'Source Sans Pro', sans-serif;">MORE account demo</a></li>
  <li style="border-bottom:  none;"><a href="javascript:void(0)" style="background-color: #fff;
  color: #555;
  border: 1px solid #ddd;
  border-top: solid 6px #588fcc;
  border-bottom:  none;
  style="font-family: 'Source Sans Pro', sans-serif;"
  " onclick="openTab(3)">Demo</a></li>
</ul>
</form>
</div>
<div class="css-carousel">
<img class="css-img" src="/images/slide0.jpg" />
    <img class="css-img" src="/images/slide1.jpg" />
    <img class="css-img" src="/images/slide2.jpg" />
    <img class="css-img" src="/images/slide3.jpg" />
    <img class="css-img" src="/images/slide4.jpg" />
</div>

  </body>
</html>

