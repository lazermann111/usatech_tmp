<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<% 
long userId = RequestUtils.getAttribute(request, "userId", Long.class, true);
String passcode = RequestUtils.getAttribute(request, "passcode", String.class, true);
PrepaidUtils.cancelPasscode(userId, passcode);
RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-passcode-canceled", "We have canceled this request.");
%>
<jsp:include page="/header.jsp"/>
<jsp:include page="/footer.jsp"/>