<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<jsp:include page="/header.jsp"/>
<p class="lead">
Please choose the type of card you want to sign up with
</p>
<form id="signup-form" action="signup.html" method="post">
<button class="btn btn-large" onclick="$('signup-form').action='signup.html?cardType=1'" type="submit">Credit Card</button>
<button class="btn btn-large" onclick="$('signup-form').action='signup.html?cardType=2'" type="submit">MORE Card</button>
<%if(PrepaidUtils.isAllowSproutSignUp()){ %>
<button class="btn btn-large" onclick="$('signup-form').action='signup.html?cardType=3'" type="submit">Sprout Card</button>
<%} %>
<button class="btn btn-large" onclick="$('signup-form').action='signup.html?cardType=4'" type="submit">Payroll Deduct Card</button>
</form>
<jsp:include page="/footer.jsp"/>