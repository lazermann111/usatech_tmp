<%@page import="simple.servlet.RequestUtils"%>
<% 
RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
%>
<jsp:include page="/header.jsp"/>
<jsp:include page="/footer.jsp"/>