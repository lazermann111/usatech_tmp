<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.CoteServlet"%>
<%@page import="simple.util.Refresher"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="simple.translator.TranslatorFactory"%>
<%@page import="simple.servlet.RequestUtils"%>
<%! Pattern removeAddrPattern = Pattern.compile("^10\\.0\\.0\\.\\d+|192\\.168\\.\\d+\\.\\d+|0:0:0:0:0:0:0:1|127.0.0.1$"); %>
<%
if(!removeAddrPattern.matcher(request.getRemoteAddr()).matches()) {
	response.sendError(403);
	return;
}
String operation = RequestUtils.getAttribute(request, "operation", String.class, true);
if("reload_translations".equalsIgnoreCase(operation)) {
	TranslatorFactory.getDefaultFactory().refreshTranslations();
	%>Translations have been refreshed.<%
} else if("reload_paths".equalsIgnoreCase(operation)) {
	Refresher pathTranslationRefresher = (Refresher)RequestUtils.getAttribute(request, ToJspSimpleServlet.PATH_TRANSLATIONS_REFRESHER_ATTRIBUTE, false);
	if(pathTranslationRefresher != null) {
		pathTranslationRefresher.refresh();
	    %>Paths have been refreshed.<%
	} else {
        %>No path refresher is configured.<%
	}
} else if("reload_cote_cache".equalsIgnoreCase(operation)) {
	Refresher coteCacheRefresher = (Refresher)RequestUtils.getAttribute(request, CoteServlet.CACHE_REFRESHER_ATTRIBUTE, false);
    if(coteCacheRefresher != null) {
    	coteCacheRefresher.refresh();
        %>Cote Cache has been refreshed.<%
    } else {
        %>No Cote Cache refresher is configured.<%
    }
} else{
	%>ERROR: Unknown operation '<%=StringUtils.prepareHTML(operation) %>'.<%
}
%>