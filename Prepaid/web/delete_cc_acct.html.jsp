<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.servlet.RequestUtils"%>
<% 
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
DataLayerMgr.executeUpdate("DELETE_CREDITCARD_ACCT", form, true);
RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "credit-card-delete-requested", "We have successfully delete the requested credit card");
RequestUtils.redirectWithCarryOver(request, response, "allcards.html");
%>