<%@page import="java.util.HashSet"%>
<%@page import="com.usatech.prepaid.web.CreditPromo"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="simple.lang.SystemUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.results.Results.Aggregate"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="java.util.TimeZone"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.prepaid.pass.PassServletUtils"%>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);

if(user == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-logged-in", "You are no longer logged in. Please log in again.");
    RequestUtils.redirectWithCarryOver(request, response, "signin.html");
    return;
}

String passSerialNumber = RequestUtils.getAttribute(request, "uuid", String.class, false);
if (!StringUtils.isBlank(passSerialNumber)){
	Map<String,Object> params = new HashMap<String,Object>();
    params.put("passSerialNumber", passSerialNumber);
    params.put("consumerId", user.getConsumerId());
    try
    {
    	Results consumerResults = DataLayerMgr.executeQuery("UPDATE_PASS_CONSUMER_ID_BY_SN", params);
    }
    catch (Exception e){
    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "pass-not-added", "New Apple Pass can't be assigned to your account. Please check if you already have one.");
    }
}

Long cardId = RequestUtils.getAttribute(request, "cardId", Long.class, false);
if(cardId != null && PrepaidUtils.checkCardPermission(user, cardId) == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
    RequestUtils.redirectWithCarryOver(request, response, PrepaidUtils.getHomePage());
    return;
}
TimeZone timeZone = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_TIME_ZONE, TimeZone.class, false, RequestUtils.SESSION_SCOPE);
Locale locale = RequestUtils.getLocale(request);
RequestUtils.setAttribute(request, "tab", "allcards", "request"); 
RequestUtils.setAttribute(request, "extra-stylesheets", "/css/pikaday.css", "request"); 
%>
<jsp:include page="/account_header.jsp" /><% 
String numberFormat = "NUMBER:#0.00;(#0.00)";
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
Results results = DataLayerMgr.executeQuery("GET_ACCOUNTS_CREDITCARDS", user);
results.setLocale(locale);
String cardFormat = "simple.text.PadFormat:        *";
String currencyFormat = "CURRENCY"; 
results.setFormat("cardNum", cardFormat);
results.setFormat("balance", currencyFormat);
results.setFormat("onHold", currencyFormat);
DateFormat[] dateFormats = PrepaidUtils.getStandardDateFormats(locale, timeZone);
String currencyCd = null;
String balanceText = null;
String onHoldText = null;
%>
        <div>
        	<div class="allcards-wrap">
            <div class="account-table-header">
                <form class="transactions-card-select form-inline form-flow">
                    <strong>Information for</strong> <%
int cardCount = results.getRowCount();
int cardOrder = 0;
String cardNum = "";
String currentNickName="";
String billingAddress1 = RequestUtils.getAttribute(request, "billingAddress1", String.class, false);
String billingCountry = RequestUtils.getAttribute(request, "billingCountry", String.class, false);
String billingPostal = RequestUtils.getAttribute(request, "billingPostal", String.class, false);
if(StringUtils.isBlank(billingAddress1) || StringUtils.isBlank(billingCountry) || StringUtils.isBlank(billingPostal)) {
    Results userInfoResults = DataLayerMgr.executeQuery("GET_USER_INFO", user);
    userInfoResults.setLocale(locale);
    if(userInfoResults.next()) {
        if(StringUtils.isBlank(billingAddress1))
        	billingAddress1 = userInfoResults.getValue("address1", String.class);
        if(StringUtils.isBlank(billingCountry))
        	billingCountry = userInfoResults.getValue("country", String.class);
        if(StringUtils.isBlank(billingPostal))
        	billingPostal = userInfoResults.getValue("postal", String.class);
    }
}
if(cardCount == 1) {
    results.next();
    currencyCd = results.getFormattedValue("currencyCd");
    balanceText = results.getFormattedValue("balance");
    onHoldText = results.getFormattedValue("onHold");
    cardOrder = 1;
    cardNum = results.getFormattedValue("cardNum");
    cardId = results.getValue("cardId", Long.class);
    form.setAttribute("cardId", cardId);
    %><%=StringUtils.prepareHTML(results.getFormattedValue("activeStatus"))%> Credit Card <span class="cardNum"><%=StringUtils.prepareHTML(cardNum)%>
    <%String nickName=results.getFormattedValue("nickName");
    if(!StringUtils.isBlank(nickName)){ %>
    (<%=StringUtils.prepareHTML(nickName)%>)
    <%} 
    currentNickName=nickName;%>
    <button id="ccDelete<%=cardId%>" class="btn btn-savechanges" type="button" onclick="onDeleteCC(<%=cardId%>)">Delete</button>
    </span>
    <div id="confirmCCDelete<%=cardId%>"></div>
    <%
} else {%>
                    <select name="cardId" onchange="this.form.submit()" id="cardId">
                        <option value=""<%if(cardId == null) {
    balanceText = results.getFormattedAggregate("balance", null, Aggregate.SUM);
    onHoldText = results.getFormattedAggregate("onHold", null, Aggregate.SUM);
                        %> selected="selected"<%} %>>All</option><%
int i = 1;
while(results.next()) {
	String cc = results.getFormattedValue("currencyCd");
    long id = results.getValue("cardId", Long.class);
    %><option value="<%=id%>"<%
    String nickName=results.getFormattedValue("nickName");
    if(cardId == null) {
        if(currencyCd == null)
            currencyCd = cc;
        else if(!currencyCd.equals(cc)) {
            currencyCd = "(mixed)";
        }
    } else {
        if(cardId.longValue() == id) {
        	cardOrder = i;
        	cardNum = results.getFormattedValue("cardNum");
            currencyCd = cc;
            balanceText = results.getFormattedValue("balance");
            onHoldText = results.getFormattedValue("onHold");
            currentNickName=nickName;
	    %> selected="selected"<% 
	    }
	}%>><%=StringUtils.prepareHTML(results.getFormattedValue("activeStatus"))%> Credit Card <%=i++ %>: <%=StringUtils.prepareHTML(results.getFormattedValue("cardNum"))%>
	<%if(!StringUtils.isBlank(nickName)){ %>
    (<%=StringUtils.prepareHTML(nickName)%>)
    <%} %>
	</option><%    
}%> </select>
	<button id="ccDelete<%=cardId%>" class="btn btn-savechanges" type="button" onclick="onDeleteCC(<%=cardId%>)">Delete</button>
	<div id="confirmCCDelete<%=cardId%>"></div>
<%
}
String expand = RequestUtils.getAttribute(request, "expand", String.class, false);
String cardBrand = RequestUtils.getTranslator(request).translate("prepaid-card-brand", "credit");
%><a class="btn-link action-link add-link" data-toggle="expand" data-target="newCardDetails">ADD <%=StringUtils.prepareHTML(cardBrand.toUpperCase())%> CARD</a>
                </form>
                <div class="clearfix"></div>
                <div id="newCardDetails"<%if(!"new".equalsIgnoreCase(expand)) {%> class="hide"<%} %>>
                    <form id="registerCardForm" class="register-card-form" action="register_allcard.html" method="post">
                    <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
                <%if(cardCount==0){ %>   
                    <strong>Promotion Code:</strong>
					<input type="text" title="The promotion code. Usually letters and numbers less than or equal to 20 characters" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "promoCode", String.class, false))%>" autocomplete="off" pattern="^[a-zA-Z0-9]{1,20}$" required="required" maxlength="20" placeholder="Enter the promotion code" name="promoCode">
				<%} %>
                    <label><span>Credit Card Number:</span></label>
			    <input type="text" name="creditCardNum" placeholder="Enter credit card number" maxlength="16" required="required" pattern="\d{15,16}" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "creditCardNum", String.class, false))%>" title="The credit card number without spaces or punctuation. Usually 15 or 16 digits." />
			    <div class="control-group"><label class="control-label">Expiration Date:</label>
			    <div class="controls"><select name="creditCardExpMonth" class="autosize" required="required"><%
				int month = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "creditCardExpMonth", false), 0);
				int year = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "creditCardExpYear", false), 0);
				Calendar now = Calendar.getInstance();
				int currYear = now.get(Calendar.YEAR);%>
				<option value=""<%if(month < 1 || month > 12) {%> selected="selected"<%} %>>--</option><%
				for(int m = 1; m <= 12; m++) {%>
				    <option<%if(month == m) {%> selected="selected"<%} %> value="<%=m%>"><%=StringUtils.pad(Integer.toString(m), '0', 2, StringUtils.Justification.RIGHT)%></option><%
				}%></select>
				<select name="creditCardExpYear" class="autosize" required="required">
				<option value=""<%if(year < currYear) {%> selected="selected"<%} %>>--</option><%
				for(int y = currYear; y <= currYear + 10; y++) {%>
				    <option<%if(year == y) {%> selected="selected"<%} %> value="<%=y%>"><%=y%></option><%
				}%></select></div></div>
				<div class="control-group"><label class="control-label">Security Code:</label>
				<div class="controls"><input type="password" name="creditCardCvv" maxlength="4" required="required" pattern="\d{3,4}" placeholder="Enter 3 or 4 digit code" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "creditCardCvv", String.class, false))%>" title="The 3 or 4 digit security code found on the back of your credit card"/></div></div>
				<div class="control-group">
					    <label class="control-label">Billing Country:</label>
					    <div class="controls">
					    <select name="billingCountry" title="Enter the country of your credit card billing address" required="required">
					    <% 
					    for(GenerateUtils.Country c : GenerateUtils.getCountries()) {%>
					        <option value="<%=StringUtils.prepareCDATA(c.code)%>" onselect="updatePostalPrompts('<%=("US".equalsIgnoreCase(c.code) ? "zip code" : "postal code") %>', '<%=StringUtils.prepareCDATA(StringUtils.prepareScript(c.postalRegex))%>', '<%=("US".equalsIgnoreCase(c.code) ? "Zip code" : "Postal code")%>:');" <%
					        if(c.code.equalsIgnoreCase(billingCountry)) { %> selected="selected"<%
					        }%>><%=StringUtils.prepareHTML(c.name)%></option><%
					     }%></select>
					     </div>
					</div>
					<div class="control-group">
					    <label class="control-label">Billing Address:</label>
					    <div class="controls">
					        <input type="text" name="billingAddress1" placeholder="Enter your billing street address" title="Enter the street address of your credit card billing address" required="required" value="<%=StringUtils.prepareCDATA(billingAddress1)%>"/>
					    </div>
					</div>
					<div class="control-group">
					    <label class="control-label" id="postalCodeLabel">Billing Postal Code:</label>
					    <div class="controls">
					        <input type="text" name="billingPostal" placeholder="Enter your billing postal code" required="required" value="<%=StringUtils.prepareCDATA(billingPostal)%>" title="Enter the postal code of your credit card billing address"/>
					    </div>
					</div>
                    <label><span>Card Nickname:</span><span class="optional">Optional</span></label>
                    <input type="text" maxLength="60" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "nickname", String.class, false))%>" title="Enter nickname for the card" name="nickname"> 
                    <div class="form-actions text-center">
                        <button type="submit" class="btn btn-savechanges">Add <%=StringUtils.prepareHTML(StringUtils.capitalizeAllWords(cardBrand))%> Card</button>
                    </div>
                    </form>
                 </div>
            </div>
            <span class="replenish-announcements">
<%            
if(!StringUtils.isBlank(onHoldText)) {
	%><span class="replenish-current-balance"><strong>On Hold:</strong> <%=StringUtils.prepareHTML(onHoldText) %> <%=StringUtils.prepareHTML(currencyCd) %> <span class="action-link"><a class="btn-link" data-toggle="expand" data-target="onHoldDetails">DETAILS</a></span></span><div class="clearfix"></div><%
}%></span>
<%
if(!StringUtils.isBlank(onHoldText)) {
	results = DataLayerMgr.executeQuery("GET_AUTH_HOLDS", form);
	results.setFormat("card", cardFormat);
	%><div id="onHoldDetails"<%if(!"holds".equalsIgnoreCase(expand)) { %>class="hide"<%} %>>
    <table class="table table-striped table-bordered-squarecorners table-hover account-table" id="accountTable">
        <thead>
            <tr>
                <th class="th-date"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Date" class="btn-link">Date</a></th>
                <%if(cardId == null) { %><th class="th-card"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Card" class="btn-link">Card</a></th><%} %>                
                <th class="th-location"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Location" class="btn-link">Location</a></th>
                <th class="th-amt"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by $ Amount" class="btn-link">$ Amount</a></th>
            </tr>
        </thead><tbody><%
        while(results.next()) {
        	String cc = results.getFormattedValue("currencyCd");
            currencyFormat = (StringUtils.isBlank(cc) ? "CURRENCY" : "CURRENCY:" + cc);
            Object date = results.getValue("date");
            BigDecimal amount = results.getValue("amount", BigDecimal.class);
            String city = results.getFormattedValue("city");
            String state = results.getFormattedValue("state");
            String postal = results.getFormattedValue("postal");
            String country = results.getFormattedValue("country");
            StringBuilder sb = new StringBuilder();
            if(city != null && !(city=city.trim()).isEmpty()) {
                sb.append(city);
                if(state != null && !(state=state.trim()).isEmpty())
                    sb.append(", ").append(state);
            } else if(state != null && !(state=state.trim()).isEmpty())
                sb.append(state);
            if(postal != null && !(postal=postal.trim()).isEmpty()) {
                if(sb.length() > 0)
                    sb.append(' ');
                sb.append(postal);
            }
            if(country != null && !(country=country.trim()).isEmpty()) {
                if(sb.length() > 0)
                    sb.append(' ');
                sb.append(country);
            }
            String[] details = {
                results.getFormattedValue("location"),
                results.getFormattedValue("address1"),
                results.getFormattedValue("address2"),
                sb.toString()         
            };
    %><tr class="tran-pending-purchase-row">
                <td data-sort-value="<%=ConvertUtils.convert(Long.class, date) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(date, dateFormats[1])) %></td>
                <%if(cardId == null) { %><td data-sort-value="<%=StringUtils.prepareCDATA(results.getFormattedValue("card")) %>"><%=StringUtils.prepareHTML(results.getFormattedValue("card")) %></td><%} %>
                <td data-sort-value="<%=StringUtils.prepareCDATA(results.getFormattedValue("location")) %>"><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" data-trigger="hover&amp;click" data-content="ID: <%=StringUtils.prepareCDATA(results.getFormattedValue("id")).replace("&", "&amp;") %>&lt;br/&gt;<%=StringUtils.prepareCDATA(ConvertUtils.formatObject(amount, currencyFormat)).replace("&", "&amp;")%><%
   for(String s : details) {
     if(s != null && !(s=s.trim()).isEmpty()) {%>&lt;br/&gt;<%=StringUtils.prepareCDATA(s).replace("&", "&amp;")%><%}
   }%>" data-title="Pending Transaction - <%=StringUtils.prepareCDATA(ConvertUtils.formatObject(date, dateFormats[0])).replace("&", "&amp;")%>"><%=StringUtils.isBlank(details[0]) ? "Unspecified" : StringUtils.prepareCDATA(details[0])%></a></td>
                <td data-sort-value="<%=SystemUtils.nvl(amount, BigDecimal.ZERO) %>"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(amount, numberFormat)) %></td>
            </tr><%
        }%></tbody></table>
    </div><%
}
if(cardId != null) {
%>  
<form name="nickNameForm" action="edit_nickname.html" method="post" class="form-inline">
	<input type="hidden" name="fromConsumerAcctTypeId" value="5"/>
    <input type="hidden" name="cardId" value="<%=cardId%>"/>
    <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
    <strong>Card Nickname:</strong> <input type="text" maxLength="60" value="<%=StringUtils.prepareCDATA(currentNickName) %>" placeholder="Enter nickname for the card" title="Enter nickname for the card" name="nickname"> <button type="submit" class="btn btn-savechanges">Update Nickname</button>
</form>
<%}
if(cardCount==1||cardId==null){%>
<form name="addPromoForm" action="add_promo.html" method="post" class="form-inline">
    <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
    <input type="hidden" name="cardTypeId" value="1"/>
    <strong>Promotion Code:</strong> <input type="text" name="promoCode" placeholder="Enter the promotion code" maxlength="20" required="required" pattern="^[a-zA-Z0-9]{1,20}$" autocomplete="off" value="" title="The promotion code. Usually letters and numbers less than or equal to 20 characters" /> <button type="submit" class="btn btn-savechanges">Add Promotion</button>
</form>
<%} %>
<hr/>
<%String action = form.getStringSafely("action", "");
if ("Add to Apple Wallet".equalsIgnoreCase(action)) {
	String passUrl = PassServletUtils.createPassUrlByConsumerId(user.getConsumerId());
%>
	<script type="text/javascript">
	window.open("<%=passUrl%>", "passDownload");
	</script>
<%}%>
<script type="text/javascript">
<%String passUrl = PassServletUtils.getPassUrlByConsumerId(user.getConsumerId());%>
function addToAppleWallet() {
					<%if (StringUtils.isBlank(passUrl)) {%>
					document.getElementById("appleWalletForm").submit();
					<%} else {%>
					window.open("<%=passUrl%>", "passDownload");
					<%}%>
				}
</script>
<div class="spacer10"></div>
To get your new pass for credit cards and add it to Apple Wallet, please open this web page on your iPhone or iPod touch.
<div class="spacer10"></div>
<form id="appleWalletForm" method="post">
	<input type="hidden" name="action" value="Add to Apple Wallet" />
	<a href="javascript:addToAppleWallet();"><img src="/images/Add_to_Apple_Wallet_rgb_US-UK.svg" style="width: 100%; max-width: 175px;" alt="Add to Apple Wallet" /></a></td>
</form>
<hr/>
</div>
</div> 
<script type="text/javascript">
var postalEl;
function updatePostalPrompts(desc, pattern, label) {
	if(!postalEl)
		postalEl = $("registerCardForm").billingPostal;
    postalEl.placeholder = "Enter your " + desc; 
    postalEl.title = "Your " + desc; 
    postalEl.pattern = pattern;
    $("postalCodeLabel").set("html", label);
}

function onDeleteCC(cardId){
	if (cardId!=null) {
		$("ccDelete"+cardId).style.display="none";
		$("confirmCCDelete"+cardId).set("html", '<h4>Are you sure you want to delete the credit card?</h4> <button  class="btn btn-savechanges" type="button" onclick="onDeleteCCYes('+cardId+')">Yes</button>  <button  class="btn btn-savechanges" type="button" onclick="onDeleteCCNo('+cardId+')">No</button>');
	}
}

function onDeleteCCYes(cardId){
	window.location.href = "delete_cc_acct.html?cardId="+cardId;
}

function onDeleteCCNo(cardId){
	$("ccDelete"+cardId).style.display="inline";
	$("confirmCCDelete"+cardId).set("html","");
}

</script>
<jsp:include page="/account_footer.jsp" />