<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
String username = RequestUtils.getAttribute(request, "username", String.class, user == null);
if (StringUtils.isBlank(username)) {
    username = form.getStringSafely("username", "");
}
boolean loggedIn = (user != null && (StringUtils.isBlank(username) || user.getUserName().equals(username)));
String passcode = RequestUtils.getAttribute(request, "passcode", String.class, false);
if (StringUtils.isBlank(passcode)) {
    passcode = form.getStringSafely("passcode", "");
}
Long userId = RequestUtils.getAttribute(request, "userId", Long.class, false);
if (userId == null) {
    userId = Long.valueOf(form.getStringSafely("userId", ""));
}
%>
<jsp:include page="/header.jsp"/>
<script type="text/javascript">
function onClickShowPasscode() {
    if($("passcode").type == 'password') {
        $("passcode").type = "text";
    } else {
        $("passcode").type = "password";
    }
}
</script>		
<div class="form-wrap">
    <h1>Change Password</h1>
    <p>Password must be 8 characters or more and contain 1 uppercase
        letter, 1 lowercase letter, and a number or punctuation symbol.</p>
    <form autocomplete="off" class="form-horizontal account-settings" action="update_password<%if(!loggedIn) { %>_by_passcode<%} %>.html" method="post">
        <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/><%
if(!loggedIn) { 
    loggedIn = false;%>
<input type="hidden" name="userId" value="<%=userId %>"/>
<input type="hidden" name="username" value="<%=StringUtils.prepareCDATA(username) %>"/>
<%
} %>
		<div class="control-group" style="<%= StringUtils.isBlank(passcode) ? "" : "display: none;" %>">
            <label class="control-label">Passcode:</label>
            <div class="controls">
                <input id="passcode" type="password" name="passcode" title="Passcode that is sent to you" required="required" value="<%=passcode%>"/> <input type="checkbox" name="showPasscode" value="" onclick="onClickShowPasscode()"/> Show Passcode
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">New Password:</label>
            <div class="controls">
                <input type="password" name="newpassword" placeholder="Create password" title="A password with at least 8 characters and 1 uppercase letter, 1 lowercase letter, and a number or punctuation symbol" required="required" pattern=".{8,}"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Type Again:</label>
            <div class="controls">
                <input type="password" name="newconfirm" placeholder="Reenter your password" title="Must match the password entered above" required="required"/>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-savechanges">Save changes</button>
        </div>
    </form>
</div>
<jsp:include page="/footer.jsp"/>