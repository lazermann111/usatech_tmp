<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.param.html.DefaultAddressRenderer"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%!
DefaultAddressRenderer addressRenderer = new DefaultAddressRenderer();
%>
<%
boolean allowSimpleSignup=false;
Results results = DataLayerMgr.executeQuery("GET_ALLOW_SIMPLE_SIGNUP", null);
if(results.next()){
	char settingValue=ConvertUtils.getChar(results.get("allowSimpleSignup"),'N');
	if(settingValue=='Y'){
		allowSimpleSignup=true;
	}else{
		allowSimpleSignup=false;
	}
}
if(!allowSimpleSignup){ 
	RequestUtils.redirectWithCarryOver(request, response, "signup.html");
}
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
RequestUtils.setAttribute(request, "subtitle", "Sign Up", "request"); 
RequestUtils.setAttribute(request, "wrapNarrow", true, "request"); 
Translator translator = RequestUtils.getTranslator(request);
String prompt = translator.translate("prepaid-new-user-prompt", "Please fill in the form below to create your account. All fields are required except where indicated.");
String imageTitle = translator.translate("prepaid-new-user-image-title", "");
Locale locale = RequestUtils.getLocale(request);
String cardBrand = RequestUtils.getTranslator(request).translate("prepaid-card-brand", "loyalty");
String cardNum = RequestUtils.getAttribute(request, "card", String.class, false);
Results regionResults = PrepaidUtils.getRegionsForCard(cardNum);
if(regionResults == null) {
    String subdirectory = PrepaidUtils.getSubdirectory(request);
    if(!StringUtils.isBlank(subdirectory)) {
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("subdirectory", subdirectory);
        params.put("subdomain", request.getServerName());
        regionResults = DataLayerMgr.executeQuery("GET_REGIONS_FOR_SUBDOMAIN", params);
    } 
}
%><jsp:include page="/header.jsp"/>
<div class="form-wrap"><%
if(!StringUtils.isBlank(imageTitle)) {%>
<img class="new-user-image" src="<%=ToJspSimpleServlet.untranslatePath(request, "/new_user_image._") %>" alt="<%=StringUtils.prepareCDATA(imageTitle) %>" /><%
} %>
	<h1>Sign Up</h1>
	<div class="signup-form">
		<form id="signup-form" method="post" action="create_user_simple.html" onsubmit="App.updateClientTimestamp(this)">
	       <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
<% if(!StringUtils.isBlank(prompt)) { 
%><h4 class="message-success"><%=prompt%></h4><% 
} %>
			<fieldset>
				<label><span>Your 19-digit Card Number</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-cardnumber_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input type="text" name="card" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card number" title="The 19-digit <%=StringUtils.prepareCDATA(cardBrand) %> card number" required="required" maxlength="19" autofocus="autofocus" autocomplete="off" pattern="\d{19}" value="<%=StringUtils.prepareCDATA(cardNum)%>" onchange="cardNumChange()"/>
                <label><span>The 4-digit Card Security Code</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-securitycode_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input type="password" name="securityCode" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card's security code" title="The 4-digit security code on the back of your <%=StringUtils.prepareCDATA(cardBrand) %> card" required="required" maxlength="4" autocomplete="off" pattern="\d{4}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "securityCode", String.class, false))%>"/>
				<hr/>
                <label><span>First Name</span></label>
                <input type="text" name="firstname" placeholder="Enter your first name" title="Your first name" required="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "firstname", String.class, false))%>"/>
                <label><span>Last Name</span></label>
                <input type="text" name="lastname" placeholder="Enter your last name" title="Your last name" required="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "lastname", String.class, false))%>"/>
				<div id="regionDetails"<%if(regionResults == null || regionResults.isGroupEnding(0)) { %> class="hide"<%}
				Long selectedRegionId = RequestUtils.getAttribute(request, "region", Long.class, false); %>>
                <label><span>Region</span></label>
                <select name="region" title="Please select the most appropriate option" required="required">
                    <option value="">--</option><%
                    if(regionResults != null)
                        while(regionResults.next()) {
                        long regionId = regionResults.getValue("regionId", Long.class); %>
                        <option value="<%=regionId %>"<%if(selectedRegionId != null && selectedRegionId == regionId) { %> selected="selected"<%} %>><%=StringUtils.prepareHTML(regionResults.getValue("regionName", String.class)) %></option><%
                    } %>                    
                </select>               
                </div>
                <hr/>
                <p style="margin-top: 1.5em;">By clicking the "Create Account" button below, you signify that you agree to the <%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%> <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/terms_conditions.html") %>">Terms and Conditions</a> and <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/privacy_policy.html") %>">Privacy Policy</a>.</p>                
                <div class="text-center">
                    <button type="submit" class="btn btn-large btn-submit">Create Account</button>
                </div>
            </fieldset>
        </form>                
    </div> <!-- /signup-form -->
    <p>Already have an account? <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signin.html") %>">Sign In</a>.</p>
</div>  <!-- /form-wrap -->
<hr/>
<script type="text/javascript">
var regionsRequest;
window.addEvent("domready", function() {
   regionsRequest = new Request.HTML({
       url: "regions_by_card.html",
       update: $("signup-form").region,
       link: "cancel"
   });
   window.regionDetailsFx = new Fx.Reveal($("regionDetails"));
});

function cardNumChange() {
    regionsRequest.send({data: $("signup-form")});
}
</script>
<jsp:include page="/footer.jsp"/>    