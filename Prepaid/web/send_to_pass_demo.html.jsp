<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.io.Log"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
String commText=null;
String commMethod=null;
try{
	String securityCd=RequestUtils.getAttribute(request, "securityCode", String.class, false);
	int preferredCommType=RequestUtils.getAttribute(request, "preferredCommType", int.class, true);
	String email=null;
	if(preferredCommType==1){
		email=RequestUtils.getAttribute(request, "email", String.class, false);
		commText=email;
		commMethod="email";
	}else{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("carrierId", RequestUtils.getAttribute(request, "carrierId", int.class, true));
		DataLayerMgr.selectInto("GET_SMS_SUFFIX", params);
		email=RequestUtils.getAttribute(request, "mobile", String.class, false);
		if(!StringUtils.isBlank(email)){
			email=email.trim();
		}
		commText=email;
		commMethod="sms";
		email=email+params.get("suffix");
	}
	if(!StringUtils.isBlank(email)){
		email=email.trim();
	}
	PrepaidUtils.sendDemoPass(email);
	if(commMethod.equals("sms")){
		RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-save-to-pass-api", "Successfully sent the request to "+commText+" to add the MORE card to Apple Wallet.  Please check your phone for the SMS.", "");
	}else{
		RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-save-to-pass-api", "Successfully sent the request to "+commText+" to add the MORE card to Apple Wallet.  Please check your email.", "");
	}
	RequestUtils.redirectWithCarryOver(request, response, "walletDemo.html");
}catch(Exception e){
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-pass-request-failed", "Your request to save card to applet wallet failed.");
	RequestUtils.redirectWithCarryOver(request, response, "walletDemo.html");
}
%>