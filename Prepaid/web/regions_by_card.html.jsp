<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
String cardNum = RequestUtils.getAttribute(request, "card", String.class, false);
Results regionResults = PrepaidUtils.getRegionsForCard(cardNum);
if(regionResults != null && !regionResults.isGroupEnding(0)) {
	Long selectedRegionId = RequestUtils.getAttribute(request, "region", Long.class, false); %>
   	<option value="">--</option><%
    while(regionResults.next()) {long regionId = regionResults.getValue("regionId", Long.class); %>
         <option value="<%=regionId %>"<%if(selectedRegionId != null && selectedRegionId == regionId) { %> selected="selected"<%} %>><%=StringUtils.prepareHTML(regionResults.getValue("regionName", String.class)) %></option><%
    } %><script type="text/javascript">window.regionDetailsFx.reveal();</script><% 
} else {
%><option value=""></option>
<script type="text/javascript">window.regionDetailsFx.reveal();</script><%}%>