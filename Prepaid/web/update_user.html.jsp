<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.lang.Holder"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.io.Log,simple.servlet.InputForm,simple.security.SecureHash"%>
<%@page import="java.sql.Connection,java.sql.SQLException,simple.db.DataLayerMgr,simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
String oldUsername = RequestUtils.getAttribute(request, "oldUsername", String.class, true);
Boolean upgradeToFull = RequestUtils.getAttribute(request, "upgradeToFull", Boolean.class, false);
Integer preferredCommType = RequestUtils.getAttribute(request, "preferredCommType", Integer.class, false);
String email = RequestUtils.getAttribute(request, "email", String.class, false);
String mobile = RequestUtils.getAttribute(request, "mobile", String.class, false);
Integer carrierId = RequestUtils.getAttribute(request, "carrierId", Integer.class, false);
String firstname = RequestUtils.getAttribute(request, "firstname", String.class, true);
String lastname = RequestUtils.getAttribute(request, "lastname", String.class, true);
String address1 = RequestUtils.getAttribute(request, "address1", String.class, true);
String postal = RequestUtils.getAttribute(request, "postal", String.class, true);
String country = RequestUtils.getAttribute(request, "country", String.class, true);
String password = RequestUtils.getAttribute(request, "newpassword", String.class, false);
String confirm = RequestUtils.getAttribute(request, "newconfirm", String.class, false);
if((StringUtils.isBlank(oldUsername) && !upgradeToFull) ||
		preferredCommType == null ||
        StringUtils.isBlank(firstname) ||
        StringUtils.isBlank(lastname) ||
        StringUtils.isBlank(address1) ||
        StringUtils.isBlank(postal) ||
        StringUtils.isBlank(country)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
    RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
} else if(!oldUsername.equals(user.getUserName()) && !upgradeToFull) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-wrong-user", "You tried to edit another user. Please try again.");
    RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
} else if(!StringUtils.isBlank(password) && !PrepaidUtils.checkPassword(RequestUtils.getOrCreateMessagesSource(request), password, confirm)) {
    RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
} else {
	switch(preferredCommType) {
	    case 1: //email
	        if(StringUtils.isBlank(email)) {
	            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
	            RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
	            return;
	        }
	        break;
	    case 2: //sms
	        if(StringUtils.isBlank(mobile) || carrierId == null) {
	            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
	            RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
	            return;
	        }
	        break;
	}
	if(!StringUtils.isBlank(email))
	    try {
	    	WebHelper.checkEmail(email);
	    } catch(MessagingException e) {
	        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-email", "You entered an invalid email: {0}. Please enter a valid one", e.getMessage());
	        RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
	        return;
	    }
	if(!StringUtils.isBlank(mobile) && carrierId != null)
	    try {
	    	mobile = PrepaidUtils.checkMobile(mobile, carrierId);
	    } catch(MessagingException e) {
	        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-mobile", "You entered an invalid mobile number: {0}. Please enter a valid one", e.getMessage());
	        RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
	        return;
	    } 
	
	Holder<String> formattedPostal = new Holder<String>();
    if(!GenerateUtils.checkPostal(country, postal, true, formattedPostal)) {
        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-postal", "You entered an invalid postal code for the country {1}. Please try again.", postal, country);
        RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
        return;
    }
    InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
	form.setAttribute("postal", formattedPostal.getValue());
	if(!StringUtils.isBlank(password)) {
		if(!PrepaidUtils.checkPassword(RequestUtils.getOrCreateMessagesSource(request), password, confirm)) {
		    RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
    		return;
		}
		String credentialAlg = "SHA-256/1000";
		byte[] credentialSalt = SecureHash.getSalt(32);
		byte[] credentialHash = SecureHash.getHash(password.getBytes(), credentialSalt, credentialAlg);
		form.setAttribute("credentialAlg", credentialAlg);
		form.setAttribute("credentialSalt", credentialSalt);
		form.setAttribute("credentialHash", credentialHash);
	}
	form.setAttribute("consumerId", user.getConsumerId());    
	Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_USER");
	try {
		String username;
        switch(preferredCommType) {
            case 1: // Email
                username = email;
                break;
            case 2:
                username = mobile;
                break;
            default:
                username = email;
                break;
        }
        try {
	    	DataLayerMgr.executeCall(conn, "UPDATE_USER", form);
	    	PrepaidUtils.updateUserPreferences(user.getConsumerId(), conn, form);
	    } catch(SQLException e) {
	    	try {
	    		conn.rollback();
	    	} catch(SQLException e2) {
	    		//ignore
	    	}
	    	if(log.isWarnEnabled())
	              log.warn("Could not update user '" + user.getUserName() + "'", e);
	    	switch(e.getErrorCode()) {
	    		case 20303:
	                RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-username-not-unique", "You have already registered a different card for this email or mobile number. Please log on and add the card there to your existing account", username);
	                break;
	    		case 20309:
                    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-state", "The state you entered is invalid. Please re-enter it.", RequestUtils.getAttribute(request, "state", String.class, false));
                    break;
                default:
	            	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
	    	}
	    	RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
	    	return;
	    }
		user.setFullName((firstname + ' ' + lastname).trim());        
		user.setEmailAddress(form.getString("communicationEmail", true));
		if(user.getUserName()==null||!user.getUserName().equals(username)) {       	
			PrepaidUtils.sendCompleteRegistrationEmail(conn, user.getEmailAddress(), preferredCommType, firstname, lastname, user.getConsumerId(), form.getString("passcode", false), RequestUtils.getBaseUrl(request), request);
	        conn.commit();
	        if(log.isInfoEnabled())
	              log.info("Updated prepaid account for user '" + user.getUserName() + ": consumer_id=" + form.getAttribute("consumerId"));
	        //must log out
	        long consumerId = user.getConsumerId();
            request.getSession().invalidate();%><jsp:include page="/header.jsp"/><%
			switch(preferredCommType) {
			    case 2: //SMS
			    	String prompt = RequestUtils.getTranslator(request).translate("prepaid-registration-sms-prompt", "To receive text messages,\nplease enter the passcode found in the text message that has been sent to you.");
                    String includePage = new StringBuilder().append("/verify_user_form.jsp?buttonText=Complete+Verification&prompt=").append(StringUtils.prepareURLPart(prompt)).append("&consumerId=").append(consumerId).toString();
                    %>
			        <jsp:include page="<%=includePage%>"/>
                    <%
			        break;
			    default:
			          %><%=RequestUtils.getTranslator(request).translate("prepaid-registration-email-prompt", "To receive email messages,<br />please click the link in the email that has been sent to you.<br />(Please check Junk Email folder in your email.)", email, firstname, lastname) %></p><%     
			}
            %><jsp:include page="/footer.jsp"/><%
		} else {
	        conn.commit();
	        if(log.isInfoEnabled())
	              log.info("Updated prepaid account for user '" + user.getUserName() + ": consumer_id=" + form.getAttribute("consumerId"));
			RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-user-updated", "Successfully updated this account", username);         
			RequestUtils.redirectWithCarryOver(request, response, "settings.html");
		}
	} finally {
		conn.close();
	}
}%>