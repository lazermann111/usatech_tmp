<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
RequestUtils.setAttribute(request, "subtitle", "Privacy Policy", "request"); 
RequestUtils.setAttribute(request, "wrapNarrow", true, "request");
Translator translator = RequestUtils.getTranslator(request);
%>
<jsp:include page="/header.jsp"/>
<h2><%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%> Pre-Paid Account Privacy Policy</h2>
<h3>What information do we collect?</h3>
<p>
We collect information from you when you register on our site.<br/><br/>
When registering on our site, as appropriate, you may be asked to enter your:
name, e-mail address, mailing address or credit card information. You may,
however, visit our site anonymously.</p>
<h3>What do we use your information for?</h3>
<p>Any of the information we collect from you may be used in one of the following
ways:</p>
<ul>
<li><h5>To personalize your experience</h5>
(your information helps us to better respond to your individual needs)</li>
<li><h5>To improve our website</h5>
(we continually strive to improve our website offerings based on the
information and feedback we receive from you)</li>
<li><h5>To improve customer service</h5>
(your information helps us to more effectively respond to your customer service
requests and support needs)</li>
<li><h5>To process transactions</h5>
Your information, whether public or private, will not be sold,
exchanged, transferred, or given to any other company for any reason whatsoever,
without your consent, other than for the express purpose of delivering the
purchased product or service requested.</li>
<li><h5>To administer a contest, promotion, survey or other site feature</h5></li>
<li><h5>To send periodic emails</h5>
The email address you provide for order processing, may be used to
send you information and updates pertaining to your order, in addition to
receiving occasional company news, updates, related product or service
information, etc.<br/><br/>
Note: If
at any time you would like to unsubscribe from receiving future emails, we
include detailed unsubscribe instructions at the bottom of each email.</li>
</ul>
<h3>How do we protect your information?</h3>
<p>
We implement a variety of security measures to maintain the safety of your
personal information when you enter, submit, or access your personal
information.</p>
<p>
We offer the use of a secure server. All supplied sensitive/credit information
is transmitted via industry accepted secure communication technology and then encrypted into
our Database to be only accessed by those authorized with special access rights
to our systems, and are required to keep the information confidential.</p>
<p>
After a transaction, your private information (credit cards) will be kept on
file for more than 60 days in order to have auto account replenishment.</p>
<h3>Do we use cookies?</h3>
<p>We use cookies only to remember your user name, if you so choose.</p>
<h3>Do we disclose any information to outside parties?</h3>
<p>
We do not sell, trade, or otherwise transfer to outside parties your personally
identifiable information. This does not include trusted third parties who
assist us in operating our website, conducting our business, or servicing you,
so long as those parties agree to keep this information confidential. We may
also release your information when we believe release is appropriate to comply
with the law, enforce our site policies, or protect ours or others rights,
property, or safety. However, non-personally identifiable visitor information
may be provided to other parties for marketing, advertising, or other uses.</p>
<h3>California Online Privacy Protection Act Compliance</h3>
<p>
Because we value your privacy we have taken the necessary precautions to be in
compliance with the California Online Privacy Protection Act. We therefore will
not distribute your personal information to outside parties without your
consent.</p>
<p>
As part of the California Online Privacy Protection Act, all users of our site
may make any changes to their information at anytime by logging into their
control panel and going to the 'Edit Profile' page.</p>
<h3>Children's Online Privacy Protection Act Compliance</h3>
<p>
We are in compliance with the requirements of COPPA (Children's Online Privacy
Protection Act), we do not collect any information from anyone under 13 years
of age. Our website, products and services are all directed to people who are
at least 13 years old or older.</p>
<h3>Online Privacy Policy Only</h3>
<p>
This online privacy policy applies only to information collected through our
website and not to information collected offline.</p>
<h3>Terms and Conditions</h3>
<p>
Please also visit our Terms and Conditions section establishing the use,
disclaimers, and limitations of liability governing the use of our website at <a
href="<%=ToJspSimpleServlet.untranslatePath(request, "allcards_terms_conditions.html") %>">Terms and Conditions</a>.</p>
<h3>Your Consent</h3>
<p>
By using our site, you consent to this privacy policy.</p>
<h3>Changes to our Privacy Policy</h3>
<p>
If we decide to change our privacy policy, we will post those changes on this
page, and/or update the Privacy Policy modification date below.</p>
<p>
This policy was last modified on 6/1/2013.</p>
<h3>Contacting Us</h3>
<p>
If there are any questions regarding this privacy policy you may contact us
using the information below:</p>
<address>
<strong><%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%></strong><br/>
<%=translator.translate("prepaid-company-address-line1", "100 Deerfield Lane, Suite 300")%><br/>
<%=translator.translate("prepaid-company-address-line2", "Malvern, PA 19355 USA")%><br/>
Phone: <%=translator.translate("prepaid-company-phone-number", "888-561-4748")%><br/>
Email: <a href="mailto:<%=translator.translate("prepaid-company-email-address", "customersupport@usatech.com")%>"><%=translator.translate("prepaid-company-email-address", "customersupport@usatech.com")%></a><br/>
Website: <a href="<%=translator.translate("prepaid-company-website-url", "http://www.usatech.com/")%>"><%=translator.translate("prepaid-company-website-url", "http://www.usatech.com/")%></a></address>

<hr/>
<jsp:include page="/footer.jsp"/>
