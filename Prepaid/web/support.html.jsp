<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.translator.Translator"%>
<%RequestUtils.setAttribute(request, "subtitle", "Support", "request"); 
RequestUtils.setAttribute(request, "wrapNarrow", true, "request");
Translator translator = RequestUtils.getTranslator(request);
PrepaidUser user = (PrepaidUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
%>
<jsp:include page="/header.jsp"/>
<div class="support-wrap">
	<h2>Contact <%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%></h2>
	<address>
	<strong>Customer Care</strong><br/>
	Phone: <%=translator.translate("prepaid-company-phone-number", "888-561-4748")%><br/>
	Email: <a href="mailto:<%=translator.translate("prepaid-company-email-address", "customersupport@usatech.com")%>"><%=translator.translate("prepaid-company-email-address", "customersupport@usatech.com")%></a></address>
</div>  <!-- /terms-wrap -->
<hr/>
<div class="support-wrap">
<h2>FAQs</h2>
<h5>Who is USA Technologies?</h5>
<p>
USA Technologies (USAT) provides end-to-end electronic payment and M2M telemetry solutions for the unattended retail market &mdash; a traditionally cash-only market sector that continues to transition to cashless payments. USAT customers include vending operators, self-service car wash facilities, coin-operated laundromats, kiosks, and other businesses that take advantage of our cashless payment technology.</p>
<h5>Why did USA Technologies create this program?</h5>
<p>
<%=translator.translate("prepaid-faq-more-program-why-text", "Our partners &mdash; vending operators, laundry owners, carwash proprietors &mdash; all want to provide more value to their customers and reward them for their repeat business.")%>
</p>
<h3>Prepaid cards</h3>
<h5>What is the <%=translator.translate("prepaid-faq-more-text", "<span class=\"more-text\">more</span>")%> Prepaid and Loyalty program?</h5>
<p>
<%=translator.translate("prepaid-faq-more-program-text", "<span class=\"more-text\">more</span> is the premiere prepaid and customer loyalty program for the self-service retail marketplace, brought to you by USA Technologies, the industry leader in secure unattended, cashless transactions and consumer engagement services.  The <span class=\"more-text\">more</span> reloadable card gives you another way to pay at some of your favorite self-service retail terminals and can earn you discounts on future purchases, access to product information and <span class=\"more-text\">more</span>.")%>
</p>
<h5>How can I manage my <%=translator.translate("prepaid-faq-more-text", "<span class=\"more-text\">more</span>")%> Prepaid and Loyalty account?</h5>
<p>
After signing up and logging in to your account you should be directed to your "Account Home" page. The "Dashboard" tab reports your current balance, promotional total, and card transactions along offers that are relevant to you.  The "Manage Cards" tab allows you to link a credit or debit card to your <%=translator.translate("prepaid-faq-more-text", "<span class=\"more-text\">more</span>")%> Prepaid and Loyalty account, add funds, and setup an automatic replenishment schedule.  The "Account Settings" tab allows you to edit your account info and profile, change your password, and set your email preferences.</p> 
<h5>Where can I use my <%=translator.translate("prepaid-faq-more-text", "<span class=\"more-text\">more</span>")%> Prepaid and Loyalty card?</h5>
<p>
You can use your <%=translator.translate("prepaid-faq-more-text", "<span class=\"more-text\">more</span>")%> card at participating self-service machines and retail locations.  After signing up and logging in, click "Locations" in the top links to view your current favorite locations and their promotions and search for new ones.</p>
</p>
</div>
<h3>Loyalty cards</h3>
<h5>What is the <span class="more-text">more</span> Loyalty and Rewards program?</h5>
<p>
The <span class="more-text">more</span> Loyalty & Rewards Program (the "Program") is the premiere customer loyalty program for the self-service retail marketplace, brought to you by USA Technologies (USAT), the industry leader in secure unattended, cashless transactions and consumer engagement services..</p>
<p>
The Program is an enhancement to an existing credit or debit card that you currently own. It is a program that you may participate in by enrolling an existing Credit or Debit card (MasterCard�, Visa�, American Express� or Discover�) by visiting <a href="./index.html">getmore.usatech.com</a>.  
</p>
<p>
The Program gives you another way to take advantage of promotions and/or discounts at your favorite self-service retail terminals. You can earn discounts on purchases, access product information, and/or earn free products. 
</p>
<h5>How do I enroll in the MORE Loyalty & Rewards program?</h5>
<p>
First, choose which existing credit or debit card you would like to use to enroll in the Program. Your card must have a Visa, MasterCard American Express or Discover brand on the face of the card.
</p>
<p>
Visit <a href="./index.html">getmore.usatech.com</a> and follow the prompts to enroll your chosen credit or debit card in the Program. 
</p>
<p>
Provide your email address and/or mobile number for communications and updates, and to activate your account.
</p>
<p>
It's that simple!
</p>
<h5>How can I manage my <span class="more-text">more</span> Loyalty & Rewards account?</h5>
<p>
After signing up and logging in to your account, you should be directed to your "Account Home" page. The "Dashboard" tab reports your current card transactions, along with offers that are relevant to you. The "Manage Cards" tab allows you to enroll your existing credit or debit card(s) in the MORE Loyalty & Rewards program. The "Account Settings" tab allows you to edit your account info and profile, change your password, and set your email preferences.
</p> 
<h5>Where can I use the card that I have used to enroll in the Program?</h5>
<p>
You can use the card that you have used to enroll in the Program at participating self-service machines and retail locations. After signing up and logging in, click on the "Locations" link appearing at the top of your screen to view a list of participating locations and their promotions.
</p>
<h3>All cards</h3>
<h5>Who do I contact with questions regarding my account?</h5>
<p>
USAT Customer Service Representatives are available to answer questions regarding your account. They may be reached at:
<address>
    <strong>Customer Care</strong><br/>
    Phone: <%=translator.translate("prepaid-company-phone-number", "888-561-4748")%><br/>
    Email: <a href="mailto:<%=translator.translate("prepaid-company-email-address", "customersupport@usatech.com")%>"><%=translator.translate("prepaid-company-email-address", "customersupport@usatech.com")%></a>
</address>
</p>
</div>
<hr/>

<jsp:include page="/footer.jsp"/>
