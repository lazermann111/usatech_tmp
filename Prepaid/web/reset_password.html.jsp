<%@page import="simple.translator.Translator"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<% String username = RequestUtils.getAttribute(request, "username", String.class, true);
PrepaidUtils.sendPasswordResetEmail(username, RequestUtils.getBaseUrl(request), request, RequestUtils.getAttribute(request, "preferredCommType", Integer.class, true));
RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-reset-password-email-prompt", "Thank you. A passcode has been sent to ''{0}'' to change your password.", username);
if(request.getAttribute("consumerId")!=null){
	RequestUtils.redirectWithCarryOver(request, response, "edit_password.html?userId="+request.getAttribute("consumerId"), false, true);
}else{
	RequestUtils.redirectWithCarryOver(request, response, "edit_password.html", false, true);
}
%>
<jsp:include page="/header.jsp"/>
<jsp:include page="/footer.jsp"/>