<%@page import="com.usatech.layers.common.constants.Patterns"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.text.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>

<%
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
boolean surveyResponse = false;
if ("POST".equalsIgnoreCase(request.getMethod()))
	surveyResponse = PrepaidUtils.recordSurveyResponse(request, response);
if (!surveyResponse) {
%>

<jsp:include page="/header.jsp"/>

<div class="spacer10"></div>
<div align="center">

<%
String surveyDevice = form.getStringSafely("survey_device", "");
String msg = form.getStringSafely("msg", "");
if (!StringUtils.isBlank(msg)) {
%>
	<div class="description"><%=StringUtils.prepareHTML(msg)%></div>
<%
} else {
Map<String, Object> params = new HashMap<String, Object>();
Results survey = null;
if (!StringUtils.isBlank(surveyDevice)) {	
	params.put("deviceSerialCd", surveyDevice);
	survey = DataLayerMgr.executeQuery("GET_SURVEY", params);
}
if (survey == null || !survey.next()) {
%>

<div class="text-error">Parameter survey_device not found</div>

<% } else {
	String errorMsg = form.getStringSafely("error-msg", "");
	if (!StringUtils.isBlank(errorMsg)) {
%>
	<div class="text-error"><%=StringUtils.prepareHTML(errorMsg)%></div>	
<%}%>

<div class="title"><%=StringUtils.prepareHTML(survey.getFormattedValue("surveyName"))%></div>

<span class="description"><%=StringUtils.prepareHTML(survey.getFormattedValue("surveyDesc"))%></span>

<script type="text/javascript">
function setRating(questionId, ratingValue) {
	document.getElementById("question" + questionId).value = ratingValue;
	document.getElementById("ratingControl" + questionId).setAttribute("class", "rating star" + ratingValue);
}
function checkSurveyInput(form) {	
	if (document.getElementById("ratingQuestionId") && document.getElementById("question" + document.getElementById("ratingQuestionId").value).value == "") {
		alert(document.getElementById("ratingQuestion").value);
		return false;
	}
	return validateForm(form);
}
</script>

<form method="post" onsubmit="return checkSurveyInput(this)">	
	<input type="hidden" name="survey_id" value="<%=survey.getValue("surveyId", long.class)%>" />
	<%
	params.put("surveyId", survey.getValue("surveyId"));
	Results questions = DataLayerMgr.executeQuery("GET_SURVEY_QUESTIONS", params);
	int questionNumber = 0;
	long ratingQuestionId = 0;
	while (questions.next()) {
		questionNumber++;
		String editor = questions.getFormattedValue("editor");
		long questionId = questions.getValue("surveyQuestionId", Long.class);
		String question = questions.getFormattedValue("surveyQuestionName");
		String answer = form.getStringSafely(new StringBuilder("question").append(questionId).toString(), "");
		%>
		<div class="spacer5"></div>
		<%=questionNumber%>. <%=StringUtils.prepareHTML(question)%><br/>
		<%
		if (editor.startsWith("RATING")) {
			if (ratingQuestionId == 0) {
				ratingQuestionId = questionId;
		%>
			<input type="hidden" id="ratingQuestionId" value="<%=ratingQuestionId%>" />
			<input type="hidden" id="ratingQuestion" value="<%=StringUtils.prepareCDATA(question)%>" />
			<%} %>
			<input type="hidden" name="question<%=questionId%>" id="question<%=questionId%>" value="<%=StringUtils.prepareCDATA(answer)%>" />
			<ul id="ratingControl<%=questionId%>" class="rating star<%=StringUtils.prepareCDATA(answer)%>">
				<li class="one"><a href="javascript:void(0)" title="1 Star - Poor" onclick="setRating('<%=questionId%>', '1')">1</a></li>
				<li class="two"><a href="javascript:void(0)" title="2 Stars - Fair" onclick="setRating('<%=questionId%>', '2');">2</a></li>
				<li class="three"><a href="javascript:void(0)" title="3 Stars - Average" onclick="setRating('<%=questionId%>', '3');">3</a></li>
				<li class="four"><a href="javascript:void(0)" title="4 Stars - Good" onclick="setRating('<%=questionId%>', '4');">4</a></li>
				<li class="five"><a href="javascript:void(0)" title="5 Stars - Excellent" onclick="setRating('<%=questionId%>', '5');">5</a></li>
			</ul>
		<%} else if (editor.startsWith("RADIO")) {
			String[] options = editor.replace("RADIO:", "").split(";");
			String[] option;
			for (String item: options) {
				option = item.split("=", -1);
				if (option.length < 2)
					continue;				
				%>
				<input type="radio" name="question<%=questionId%>" value="<%=StringUtils.prepareCDATA(option[0])%>"<%if (answer.equalsIgnoreCase(option[0])) out.write(" checked=\"checked\"");%> /><%=StringUtils.prepareHTML(option[1])%>&nbsp;&nbsp;
			<%}
		} else if (editor.startsWith("TEXTAREA")) {
			String[] option = editor.replace("TEXTAREA:", "").split(" to ");
			int to = ConvertUtils.getIntSafely(option[1], 200);			
		%>			
			<textarea name="question<%=questionId%>" maxlength="<%=to%>" class="survey-input" placeholder="Your comments"><%=StringUtils.prepareHTML(answer)%></textarea>
		<%} %>
	<%}%>
	
	<br/>
	<%=++questionNumber%>. Email address (if you want to be contacted)<br/>
	<input type="email" name="consumer_email" id="consumer_email" maxlength="60" class="survey-input" placeholder="Your email address" value="<%=StringUtils.prepareCDATA(form.getStringSafely("consumer_email", ""))%>" valid="<%=Patterns.EMAIL_ADDRESS_PATTERN.pattern()%>" label="Email address" />
	
	<div class="spacer5"></div>
	<span class="description"><%=StringUtils.prepareHTML(survey.getFormattedValue("surveyDesc2"))%></span>
	<div class="spacer5"></div>	
	<input type="submit" class="btn btn-savechanges" value="Submit" />
</form>
	
<%}%>

	<div class="spacer5"></div>
	<a href="<%=PrepaidUtils.getUsaliveRootUrl()%>terminal_details.i?deviceSerialCd=<%=StringUtils.prepareCDATA(StringUtils.prepareURLPart(surveyDevice))%>">Operator Area</a>

<%}%>	

</div>
<div class="spacer10"></div>

<jsp:include page="/footer.jsp"/>

<%}%>