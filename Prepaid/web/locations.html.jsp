<%@page import="com.usatech.prepaid.web.CampaignDetails"%>
<%@page import="com.usatech.layers.common.util.CampaignUtils"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="java.util.SortedSet"%>
<%@page import="simple.results.Results.Aggregate"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.translator.Translator"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<% RequestUtils.setAttribute(request, "subtitle", "Locations", "request"); 
RequestUtils.setAttribute(request, "wrapNarrow", true, "request");  %>
<jsp:include page="/header.jsp" />
<%
PrepaidUser user = (PrepaidUser) RequestUtils.getUser(request);
Translator translator = RequestUtils.getTranslator(request);
int campaignCnt = 0;
String terms = null;
if(user != null) {
    Results results = PrepaidUtils.getRecentLocations(user);
    results.addGroup("deviceSerialCd");
    results.addGroup("campaignId");
    results.trackAggregate("campaignId", Aggregate.DISTINCT);
    results.setFormat("campaignEndDate", "DATE:MM/dd/yyyy");
    results.setFormat("campaignDiscountPercent", "PERCENT");
    if(!results.isGroupEnding(0)) {
    	%>
<h4 class="favorite-locations-table-header">Recently Visited Locations</h4>
<div class="favorite-locations">
    <table class="table table-striped table-hover table-condensed favorite-locations-table" id="favelocationsTable">
        <thead>
            <tr>
                <th class="th-product"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Product" class="btn-link">Product type</a></th>
                <th class="th-location"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Location" class="btn-link">Location</a></th>
                <th class="th-promo"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Promotion" class="btn-link">Promo</a></th>
            </tr>
        </thead>
        <tbody><%
        while(results.next()) {
        	if(results.isGroupBeginning("deviceSerialCd")) {
                String productType = results.getFormattedValue("locationProductType");
                if(productType == null || productType.startsWith("-"))
                    productType = "";
	            String location = results.getFormattedValue("locationDescription");
	        	if(StringUtils.isBlank(location))
	        		location = results.getFormattedValue("locationName");
	        	String city = results.getFormattedValue("locationCity");
	            String state = results.getFormattedValue("locationState");
	            String postal = results.getFormattedValue("locationPostal");
	            String country = results.getFormattedValue("locationCountry");
	            StringBuilder sb = new StringBuilder();
	            if(city != null && !(city=city.trim()).isEmpty()) {
	                sb.append(city);
	                if(state != null && !(state=state.trim()).isEmpty())
	                    sb.append(", ").append(state);
	            } else if(state != null && !(state=state.trim()).isEmpty())
	                sb.append(state);
	            if(postal != null && !(postal=postal.trim()).isEmpty()) {
	                if(sb.length() > 0)
	                    sb.append(' ');
	                sb.append(postal);
	            }
	            if(country != null && !(country=country.trim()).isEmpty()) {
	                if(sb.length() > 0)
	                    sb.append(' ');
	                sb.append(country);
	            }
	            String[] details = {
	            	results.getFormattedValue("locationAddress1"),
	                results.getFormattedValue("locationAddress2"),
	                sb.toString()         
	            };
        	%><tr>
        	    <td data-sort-value="<%=StringUtils.prepareCDATA(productType.toLowerCase()) %>"><%=StringUtils.prepareHTML(productType) %></td>
                <td data-sort-value="<%=StringUtils.prepareCDATA(location)%>"><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" data-trigger="hover&amp;click" data-content="<%
   for(String s : details) {
     if(s != null && !(s=s.trim()).isEmpty()) {%><%=StringUtils.prepareCDATA(s.replace("&", "&amp;"))%>&lt;br/&gt;<%}
   }
   campaignCnt = results.getAggregate("campaignId", "deviceSerialCd", Aggregate.DISTINCT, Integer.class); 
   /* Lifetime Discount is disabled
   BigDecimal totalDiscount = results.getValue("totalDiscount", BigDecimal.class);
   if(totalDiscount != null) {
	   String currencyCd = results.getFormattedValue("currencyCd");
	   String currencyFormat = (StringUtils.isBlank(currencyCd) ? "CURRENCY" : "CURRENCY:" + currencyCd); 
       %>&lt;span class='popover-discount-total'&gt;&lt;strong&gt;Lifetime Discount:&lt;/strong&gt; <%=StringUtils.prepareCDATA(ConvertUtils.formatObject(discount, currencyFormat)).replace("&", "&amp;") %>&lt;/span&gt;<%
   }*/ %>" data-title="<%=StringUtils.prepareCDATA(location).replace("&", "&amp;")%>"><%=StringUtils.isBlank(location) ? "Unspecified" : StringUtils.prepareCDATA(location)%></a></td>
                <td data-sort-value="<%=campaignCnt %>"><ul class="inline"><%
        } // end of isGroupBeginning
        if(results.getValue("campaignId") != null) {
            if(results.isGroupBeginning("campaignId")) {
            	CampaignDetails cDetails=PrepaidUtils.getPromoCampaignDetails(results, translator);
            	String title = cDetails.getTitle();
            	String details = cDetails.getDetails();
            	String products = results.getValue("products", String.class);
            	if (products == null) products ="";
            	terms = translator.translate("prepaid-promo-campaign-terms", "{0}", 
                        title, results.getValue("campaignDiscountPercent"), results.getValue("campaignEndDate"));
                             %><li><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" 
                             				data-trigger="hover&amp;click" data-title="<%=StringUtils.prepareCDATA(terms).replace("&", "&amp;")%>" 
                             				data-content="<%=StringUtils.prepareCDATA(details).replace("&", "&amp;")%>
                             				<%if (products.length() > 0) {%>
                     						&lt;br/&gt;&lt;em&gt;Valid for products: &lt;br/&gt;<%=StringUtils.prepareCDATA(products).replace("&", "&amp;")%>
                     						<%} %>
                             				&lt;br/&gt;&lt;em&gt;Valid for your cards:&lt;br/&gt;<%
            }
            %><%=StringUtils.prepareHTML(results.getFormattedValue("cardNum")) %><%
            if(results.isGroupEnding("campaignId")) {
                %>&lt;/em&gt;"><%=StringUtils.prepareHTML(terms)%></a></li><%
            } else {
                %>&lt;br/&gt;<%
            }
        } 
        if(results.isGroupEnding("deviceSerialCd")) {
        	if(campaignCnt == 0) {
                %><li>--</li><%
            }
            %></ul></td><%                
        }
    }%>
        </tbody>
     </table>
</div><%
    } else {
    	%><h4 class="favorite-locations-table-header"><%=translator.translate("prepaid-no-recent-usage-prompt", "Start using your card today to earn discounts!") %></h4><%
    }
}
Locale locale = RequestUtils.getLocale(request);
String postal = RequestUtils.getAttribute(request, "postal", String.class, false);
String country = RequestUtils.getAttribute(request, "country", String.class, false);
if(StringUtils.isBlank(postal)) {
	Results results = DataLayerMgr.executeQuery("GET_USER_INFO", user);
    results.setLocale(locale);
    if(results.next()) {
        if(StringUtils.isBlank(country))
            country = results.getValue("country", String.class);
        if(StringUtils.isBlank(postal))
            postal = results.getValue("postal", String.class);
    }
}
if(StringUtils.isBlank(country))
	country = "US";
int[] milesOptions = new int[] {1,5,10,15,20,30,50,100,150};
Float miles = RequestUtils.getAttribute(request, "distance", Float.class, false);
if(miles == null)
	miles = PrepaidUtils.getPromoMaxProximityMiles();
Boolean discountsOnly = RequestUtils.getAttribute(request, "discountsOnly", Boolean.class, false);
Results campaignResults = DataLayerMgr.executeQuery("GET_MORE_CAMPAIGN_BY_CONSUMER",new Object[]{user.getConsumerId(), user.getConsumerId()});
Long campaignId = RequestUtils.getAttribute(request, "campaignId", Long.class, false);
%>
<!-- /favorite-locations -->
<div class="find-locations">
    <h4 class="find-locations-table-header">Find Locations</h4>
    <form class="find-locations-form" name="locationsForm">
          <fieldset>
          <div class="locations-zip-miles-wrap">
          <div class="pull-left locations-enter-country">
            <label>Enter country:</label>
              <select class="autosize" name="country" title="A country">
                    <% for(GenerateUtils.Country c : GenerateUtils.getCountries()) {%>
                        <option value="<%=StringUtils.prepareCDATA(c.code)%>" onselect="updatePostalPrompts('<%=("US".equalsIgnoreCase(c.code) ? "zip code" : "postal code") %>', '<%=StringUtils.prepareCDATA(StringUtils.prepareScript(c.postalRegex))%>', '<%=("US".equalsIgnoreCase(c.code) ? "Enter zip code" : "Enter postal code")%>:');" <%
                        if(c.code.equalsIgnoreCase(country)) { %> selected="selected"<%
                        }%>><%=StringUtils.prepareHTML(c.name)%></option><%
                     }%></select>
          </div>
          <div class="pull-left locations-enter-zip">
            <label id="postalCodeLabel">Enter postal Code:</label>
              <input type="text" name="postal" placeholder="Enter postal code" value="<%=StringUtils.prepareCDATA(postal) %>" required="required" title="A postal code"/>
          </div>
          <div class="pull-left locations-enter-miles">   
            <label>Search within:</label>
              <select class="autosize" name="distance" title="Maximum distance between the zip code and the location"><%
boolean found = false;
for(int i : milesOptions) {
	%><option value="<%=i%>"<% if(!found && i >= miles) { found = true; %> selected="selected"<%} %>><%=i %> miles</option><%
}
boolean first;
Integer rows = PrepaidUtils.getRowSelection(request);
SortedSet<Integer> rowsOptions = PrepaidUtils.getRowOptions(rows);
if(rows == null) 
    rows = rowsOptions.first();
%></select>
           </div> 
           <div class="pull-left locations-enter-campaign">
            <label id="campaignLabel">Enter Promotion:</label>
              <select class="autosize" name="campaignId" title="Promotion">
              <option value="" <%if(campaignId==null){ %>selected<%} %>> All </option>
              <%
        while(campaignResults.next()) {
            long resultsCampaignId = campaignResults.getValue("campaignId", long.class);
        %>
        <option value="<%=resultsCampaignId%>" <%if(campaignId!=null&&campaignId==resultsCampaignId){ %>selected<%} %>>
        <%=StringUtils.prepareCDATA(campaignResults.getFormattedValue("title"))%> <%if(campaignResults.get("promoCode")!=null){ %>(<%=StringUtils.prepareCDATA(campaignResults.getFormattedValue("promoCode"))%>)<%} %>
        </option><%
        }%>
              </select>
          </div>
           <div class="clearfix"></div>
           </div>  <!-- /locations-zip-miles-wrap  -->
           <div>
            <label class="checkbox"><input type="checkbox" name="discountsOnly"<%if(discountsOnly != null && discountsOnly.booleanValue()) {%> checked="checked"<%} %>/>Only show locations with discounts</label>
            </div>           
            <button type="submit" class="btn btn-find-locations">Find Locations</button>
          </fieldset>
          <input id="static-rows" type="hidden" name="rows" value="<%=rows%>"/><%
if(!StringUtils.isBlank(postal)) {
	Integer pageNum = RequestUtils.getAttribute(request, "page", Integer.class, false);
	if(pageNum == null)
	    pageNum = 1;
	Results results = PrepaidUtils.getLocations(user.getConsumerId(), null, null, null, postal, country, miles, discountsOnly != null && discountsOnly.booleanValue(),campaignId);
	results.addGroup("deviceSerialCd");
	results.addGroup("campaignId");
	results.trackAggregate("campaignId", Aggregate.DISTINCT);
	results.trackAggregate("deviceSerialCd", Aggregate.DISTINCT);
	results.setFormat("campaignEndDate", "DATE:MM/dd/yyyy");
	results.setFormat("campaignDiscountPercent", "PERCENT");
	int count = results.getAggregate("deviceSerialCd", null, Aggregate.DISTINCT, Integer.class);
%>
    <div class="locations-table-controls">
        <%@ include file="pagination.frag" %>
    </div>
    <!-- /locations-table-controls  -->
    <table class="table table-striped table-hover table-condensed find-locations-table" id="findlocationsTable">
        <thead>
            <tr>
                <th class="th-product"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Product" class="btn-link">Product type</a></th>
                <th class="th-location"><a data-toggle="sort" data-sort-type="STRING" title="Sort by Location" class="btn-link">Location</a></th>
                <th class="th-distance"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Distance" class="btn-link">Distance</a></th>
                <th class="th-promo"><a data-toggle="sort" data-sort-type="NUMBER" title="Sort by Promotion" class="btn-link">Promo</a></th>
            </tr>
        </thead><%
int r = 0;
while(results.next()) {
    if(results.isGroupBeginning("deviceSerialCd")) {
        String productType = results.getFormattedValue("locationProductType");
        if(productType == null || productType.startsWith("-"))
            productType = "";
        String location = results.getFormattedValue("locationDescription");
        if(StringUtils.isBlank(location))
            location = results.getFormattedValue("locationName");
        String city = results.getFormattedValue("locationCity");
        String state = results.getFormattedValue("locationState");
        postal = results.getFormattedValue("locationPostal");
        country = results.getFormattedValue("locationCountry");
        StringBuilder sb = new StringBuilder();
        if(city != null && !(city=city.trim()).isEmpty()) {
            sb.append(city);
            if(state != null && !(state=state.trim()).isEmpty())
                sb.append(", ").append(state);
        } else if(state != null && !(state=state.trim()).isEmpty())
            sb.append(state);
        if(postal != null && !(postal=postal.trim()).isEmpty()) {
            if(sb.length() > 0)
                sb.append(' ');
            sb.append(postal);
        }
        if(country != null && !(country=country.trim()).isEmpty()) {
            if(sb.length() > 0)
                sb.append(' ');
            sb.append(country);
        }
        String[] details = {
            results.getFormattedValue("locationAddress1"),
            results.getFormattedValue("locationAddress2"),
            sb.toString()         
        };
        Number distance = results.getValue("locationProximityMiles", Number.class);
        r++;
        if(rows == 0) {
        	if(r == 1) {%><tbody><%}
        } else if(r % rows == 1) {
            if(r > 1) {
                %></tbody><%
            }       
            int p = 1 + ((r - 1) / rows);
            %><tbody id="page_<%=p%>"<%if(p != pageNum) {%> class="hidden"<%} %>><%
        }        
    %><tr>
        <td data-sort-value="<%=StringUtils.prepareCDATA(productType.toLowerCase()) %>"><%=StringUtils.prepareHTML(productType) %></td>
        <td data-sort-value="<%=StringUtils.prepareCDATA(location)%>"><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" data-trigger="hover&amp;click" data-content="<%
for(String s : details) {
if(s != null && !(s=s.trim()).isEmpty()) {%><%=StringUtils.prepareCDATA(s).replace("&", "&amp;")%>&lt;br/&gt;<%}
}
campaignCnt = results.getAggregate("campaignId", "deviceSerialCd", Aggregate.COUNT, Integer.class); 
/* Lifetime Discount is disabled
BigDecimal totalDiscount = results.getValue("totalDiscount", BigDecimal.class);
if(totalDiscount != null) {
String currencyCd = results.getFormattedValue("currencyCd");
String currencyFormat = (StringUtils.isBlank(currencyCd) ? "CURRENCY" : "CURRENCY:" + currencyCd); 
%>&lt;span class='popover-discount-total'&gt;&lt;strong&gt;Lifetime Discount:&lt;/strong&gt; <%=StringUtils.prepareCDATA(ConvertUtils.formatObject(discount, currencyFormat)) %>&lt;/span&gt;<%
}*/ %>" data-title="<%=StringUtils.prepareCDATA(location).replace("&", "&amp;")%>"><%=StringUtils.isBlank(location) ? "Unspecified" : StringUtils.prepareCDATA(location)%></a></td>
        <td data-sort-value="<%=distance %>"><%=ConvertUtils.formatObject(distance, "NUMBER:#,##0.0 'mi'") %></td>
        <td data-sort-value="<%=campaignCnt %>"><ul class="inline"><%
} // end of isGroupBeginning
if(results.getValue("campaignId") != null) {
    if(results.isGroupBeginning("campaignId")) {
    	CampaignDetails cDetails=PrepaidUtils.getPromoCampaignDetails(results, translator);
    	String title = cDetails.getTitle();
    	String details = cDetails.getDetails();
    	String products = results.getValue("products", String.class);
    	if (products == null) products ="";
        terms = translator.translate("prepaid-promo-campaign-terms", "{0}", 
                title, results.getValue("campaignDiscountPercent"), results.getValue("campaignEndDate"));
                     %><li><a class="btn-link" data-toggle="popover" data-placement="top" data-offset="-10" 
                     			data-trigger="hover&amp;click" data-title="<%=StringUtils.prepareCDATA(terms).replace("&", "&amp;")%>" 
                     			data-content="<%=StringUtils.prepareCDATA(details).replace("&", "&amp;")%>
                     			<%if (products.length() > 0) {%>
                     			&lt;br/&gt;&lt;em&gt;Valid for products: &lt;br/&gt;<%=StringUtils.prepareCDATA(products).replace("&", "&amp;")%>
                     			<%} %>
                     			&lt;br/&gt;&lt;em&gt;Valid for your cards:&lt;br/&gt;<%
    }
    %><%=StringUtils.prepareCDATA(results.getFormattedValue("cardNum")) %><%
    if(results.isGroupEnding("campaignId")) {
        %>&lt;/em&gt;"><%=StringUtils.prepareHTML(terms)%></a></li><%
    } else {
        %>&lt;br/&gt;<%
    }
} 
if(results.isGroupEnding("deviceSerialCd")) {
    if(campaignCnt == 0) {
        %><li>--</li><%
    }
    %></ul></td><%                
}
}
if(count == 0) {
%><tbody><tr class="no-rows-found"><td colspan="4">No locations found</td></tr><%
}
%>
        </tbody>
    </table>
    <div class="locations-table-controls">
        <%@ include file="pagination.frag" %>
    </div>
    <script type="text/javascript">
    var currPage;
    var currBtn;
    window.addEvent("domready", function() {
    	currPage = $("page_<%=pageNum%>");
        currBtn = $$(".page_button_<%=pageNum%>");
        loaded = true;
    });
    function viewPage(pageNum) {
        var newPage = $("page_" + pageNum);
        var newBtn = $$(".page_button_" + pageNum);
        currPage.addClass("hidden");
        newPage.removeClass("hidden");
        currPage = newPage;
        currBtn.set("disabled", false);
        newBtn.set("disabled", true);
        currBtn = newBtn;  
        $("findlocationsTable").scrollIntoView();
    }
    </script>
    <%} %>
    </form>
    <!-- /locations-table-controls  -->
</div>
<!-- /find-locations  -->
<hr />
<script type="text/javascript">
var postalEl;
function updatePostalPrompts(desc, pattern, label) {
    if(!postalEl)
        postalEl = $(document.locationsForm).postal;
    postalEl.placeholder = "Enter " + desc; 
    postalEl.title = "A " + desc; 
    postalEl.pattern = pattern;
    $("postalCodeLabel").set("html", label);
}
</script>
<jsp:include page="/footer.jsp"/>