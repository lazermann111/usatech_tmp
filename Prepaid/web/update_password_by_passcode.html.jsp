<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.io.Log,simple.translator.Translator"%>
<%@page import="java.sql.SQLException,simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
long userId = RequestUtils.getAttribute(request, "userId", Long.class, true);
String passcode = RequestUtils.getAttribute(request, "passcode", String.class, true);
String password = RequestUtils.getAttribute(request, "newpassword", String.class, true);
String confirm = RequestUtils.getAttribute(request, "newconfirm", String.class, true);
if(!PrepaidUtils.checkPassword(RequestUtils.getOrCreateMessagesSource(request), password, confirm)) {
	String username = RequestUtils.getAttribute(request, "username", String.class, true);
    RequestUtils.redirectWithCarryOver(request, response, "edit_password.html?username=" + username);
} else {
	try {
		PrepaidUtils.updatePassword(userId, passcode.trim(), password);
		RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-password-changed-bypasscode", "Your password has been updated. Please log in.");
		RequestUtils.redirectWithCarryOver(request, response, PrepaidUtils.getHomePage());
    } catch(SQLException e) {
    	if(log.isWarnEnabled())
              log.warn("Could not update password for user " + userId , e);
    	switch(e.getErrorCode()) {
            case 20320:
                RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-consumer-not-found", "Failed to update password. Please contact Customer Service for assistance");
                break;
            case 20321:
            case 20322:
            	String username = RequestUtils.getAttribute(request, "username", String.class, true);
            	PrepaidUtils.sendPasswordResetEmail(username, RequestUtils.getBaseUrl(request), request);
            	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-resend-reset-password-email-prompt", "The passcode is expired. A new email has been sent to you. Please use that to change your password.");
                break;
            default:
            	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
    	}%>
    	<jsp:include page="/header.jsp"/>
        <jsp:include page="/footer.jsp"/><%
    }
}%>