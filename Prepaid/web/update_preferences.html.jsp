<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.io.Log,simple.servlet.InputForm"%>
<%@page import="java.sql.Connection,java.sql.SQLException,simple.db.DataLayerMgr,simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_USER");
try {
   	PrepaidUtils.updateUserPreferences(user.getConsumerId(), conn, form);
   	conn.commit();
} catch(SQLException e) {
   	try {
   		conn.rollback();
   	} catch(SQLException e2) {
   		//ignore
   	}
   	if(log.isWarnEnabled())
   	    log.warn("Could not update user '" + user.getUserName() + "'", e);
   	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
   	RequestUtils.redirectWithCarryOver(request, response, "settings.html", false, true);
   	return;
} finally {
    conn.close();
}
if(log.isInfoEnabled())
    log.info("Updated preferences for user '" + user.getUserName() + ": consumer_id=" + form.getAttribute("consumerId"));
RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-preferences-updated", "Successfully updated your preferences", user.getUserName());         
RequestUtils.redirectWithCarryOver(request, response, "settings.html");	
%>