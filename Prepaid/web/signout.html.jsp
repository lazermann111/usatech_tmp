<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.servlet.RequestUtils"%>
<% RequestUtils.setAttribute(request, "subtitle", "Signout", "request"); 
RequestUtils.setAttribute(request, "wrapNarrow", true, "request"); 
if(RequestUtils.getUser(request) != null) { 
	request.getSession().invalidate();%>
<jsp:include page="/header.jsp"/>
<p>Thank you for using <%=RequestUtils.getTranslator(request).translate("prepaid-title", "more") %>.</p><p><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signin.html") %>">Click here to sign in again</a></p>
<jsp:include page="/footer.jsp"/>
<%
} else { 
    request.getSession().invalidate();%>
<jsp:forward page="signin.html"/>
<%}%>