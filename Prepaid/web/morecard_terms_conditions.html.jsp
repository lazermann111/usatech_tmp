<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%
RequestUtils.setAttribute(request, "subtitle", "Terms & Conditions", "request");
RequestUtils.setAttribute(request, "wrapNarrow", true, "request");
Translator translator = RequestUtils.getTranslator(request);
%>
<jsp:include page="/header.jsp"/>
<h2><%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%> Prepaid &amp; Loyalty (P&amp;L) CARD TERMS AND CONDITIONS.</h2>
<h3>EFFECTIVE April 1, 2013</h3>
<p>
There are no Card fees associated with your account. Please be sure to keep this important information and provide it to anyone to whom the P&amp;L Card is given. The P&amp;L Card becomes active no later than the end of the next business day after registration.</p>
<p>
The following terms and conditions (&quot;Agreement&quot;) govern the use of the Card. In this Agreement, the words &quot;you&quot; and &quot;your&quot; mean: (a) the person who was given the P&amp;L Card (sometimes referred to as the Card in this document); and (b) any person who uses the Card. The words &quot;we,&quot; &quot;us,&quot; &quot;our&quot; and &quot;Card&quot; mean Prepaid and Loyalty Card. The terms of this Agreement are binding upon us and you and will be deemed to have been accepted by you if any of the following are done by you or by another person with your consent or authorization: (a) use the Card; (b) authorize any other person to use the Card; (c) retain the Card; (d) activate the Card, or (e) register the Card.</p>
<p>
<strong>Please read this Agreement carefully, keep it and provide it to anyone to whom the Card is given.</strong></p>
<h3>Card Description And How the Card Works.</h3>
<p>
The Card is intended to be given away as a payment option by a Vending Operator to consumers to be used in a specific set of vending machines. The person to whom the Card is given should register the Card immediately or return it to the issuing vending operator. The Card is a proprietary form of a <%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%>, reloadable, stored value Card. The Card is not linked to, nor does it access in any way, any checking or other accounts at any bank. The Card is not a credit card. The Card can have value added to it anytime or after all of the existing value has been used. You acknowledge and agree that the amount available on the Card is limited to the prepaid U.S. dollar value loaded on the Card. The Card is issued by a vending operator, not directly from USA Technologies, Inc. The Card does have a Security Code, but is not enabled for PIN transactions and may not be used to obtain cash at ATM machines. The Card cannot be used for cash advances and cannot be exchanged for cash at stores or other financial institutions.</p>
<h3>Balance on the Card.</h3>
<p>
The value of the funds available on the Card at any given time is referred to in this Agreement as the &quot;Balance.&quot; No interest will be paid on the Card Balance. The balance is not FDIC insured. The Balance is limited $2000 (two thousand). The Balance will decrease each time the Card is used to make an authorized transaction by the full amount of each purchase, or (when and if available) the use of sufficient loyalty points to cover the cost of said transaction. The amount of funds shown on the Consumer Web Portal records will determine the Balance. Once the Balance is depleted and you no longer wish to use the Card, the Card is no longer valid and you agree (a) not to use the Card; and (b) to cut it in half and discard it. Whenever you use the Card to make an authorized transaction, you are authorizing us to reduce the Card Balance by an amount equal to the Transaction Amount. At the time of each purchase, the Merchant where the transaction is made (&quot;Merchant&quot;) will obtain an approval for the Transaction Amount to ensure adequate funds are available. You agree to keep track of the Balance on your Card by using our website.</p>
<h3>Using Your Card.</h3>
<ol type="a"><%String url = RequestUtils.getBaseUrl(request, true, false); if(StringUtils.isBlank(url)) url = "getmore.usatech.com"; %>
<li>
<h5>Registering the Card.</h5>
You should register the Card at <a href="<%=StringUtils.encodeForURL(url) %>"><%=StringUtils.encodeForHTML(url) %></a></li>
<li>
<h5>Record Your Card Number.</h5>
Record your Card Number in a safe place separate from the Card. You will need your Card Number when reporting a lost or stolen Card or unauthorized transactions.</li>
</ol>
<h3>Transaction Limitations.</h3>
<ol type="a">
<li>
<h5>Non-transferable Once Registered.</h5></li>
<li>
<h5>Point of Sale Use and Hold Periods.</h5>
Vending operator transactions require a hold to put on the card for an amount greater than actual vend price. This hold will remain in effect until we receive the transaction from the Merchant or seven (7) business days, whichever occurs first. During the hold period, the funds will be unavailable.</li>
<li>
<h5>Purchases Exceeding the Card Balance.</h5>
If the amount of your purchase is greater than the Card Balance or loyalty point accrual, the card will be declined.</li>
<li>
<h5>International Transactions.</h5>
This card is not valid for international transactions.</li>
<li>
<h5>Additional Transactions.</h5>
You shall not use your Card for any illegal transaction. We may decline authorization for any illegal transaction or Internet gambling transaction.</li>
<li>
<h5>Returns and Exchanges.</h5>
USA Technologies, employees and agents are not responsible for the services or merchandise purchased with the Card and are not responsible for the return or exchange of merchandise purchased with the Card. By use of this Card, you agree that we are not liable for any consequential damages, direct or indirect. If you think an error has occurred involving a transaction, the error needs to be adjusted and resolved with the Vending Operator. Return and refund policies are dependent on the Vending Operator Availability of credits added to the Balance may vary by Vending Operator.</li>
</ol>
<h3>How to Obtain Your Card Balance or Obtain Information on Transactions Previously Made.</h3>
<p>
It is important that you keep track of the Balance on your Card. To check your Card Balance or to get a summary of the transactions you have made, log onto <a href="<%=StringUtils.encodeForURL(url) %>"><%=StringUtils.encodeForHTML(url) %></a>. You are encouraged to use your Card as soon as possible and check the Balance. No receipt will be available for transaction totals of $15.00 or less. You will not receive a statement or written summary of transactions.</p>
<h3>Theft, Unauthorized Use or Loss of Your Card.</h3>
<p>
Call <%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%> at <%=translator.translate("prepaid-company-phone-number", "888-561-4748")%> and ask for customer support.</p>
<ol type="a">
<li>
<h5>Authorized Use of Card.</h5>
You are responsible for all authorized and unauthorized uses of your Card except to the extent that applicable law may protect you from liability for unauthorized purchases.</li>
<li>
<h5>What to Do If You Believe Your Card is Stolen or Unauthorized Transactions Have Occurred.</h5>
Notify us immediately at <strong><%=translator.translate("prepaid-company-phone-number", "888-561-4748")%></strong>. You will need your Card Number to receive information.</li>
<li>
<h5>What to Do If Your Card is Lost.</h5>
If you lose your Card, it is like losing cash and we may not reissue the Card or refund your money. However, we advise you to report the loss of the Card by calling us at <strong><%=translator.translate("prepaid-company-phone-number", "888-561-4748")%></strong> as soon as you discover the loss.</li>
<li>
<h5>Replacement For Damaged Cards.</h5>
A replacement Card can be issued by the vending operator for the Balance on any damaged Card and the original Card will be canceled.</li>
</ol>
<h3>The Following Notice Contains Information About Your Right to Dispute Errors.</h3>
<ol type="a">
<li>
<h5>What to Do About Suspected Errors.</h5>
In case of errors or questions about electronic transactions on the Card, call us at <%=translator.translate("prepaid-company-phone-number", "888-561-4748")%>.</li>
<li>
<h5>Information You Must Give to Us.</h5>
When you make your inquiry about the suspected error, you must supply us the following information:
<ul>
<li>Your name, Card Number, and Security Code.</li>
<li>A description of the error or the transaction you are unsure about and a clear explanation of why you believe it is an error or why you need more information.</li>
<li>The dollar amount of the suspected error.</li>
</ul>
</li>
<li>
<h5>What Happens If We Believe No Error Occurred.</h5>
You may ask for copies of the documents that we used in our investigation.</li>
<li>
<h5>Disputes with Merchant.</h5>
If you have a problem with any goods or services you purchase using the Card, you agree to settle any disputes arising from the problem directly with the Merchant (Vending Operator).</li>
</ol>
<h3>Validity of Your <%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%> Prepaid Card.</h3>
<ol type="a">
<li>
<h5>Expired Cards.</h5>
Your Card does not expire.</li>
<li>
<h5>Card Activity.</h5>
The Card is considered as active if the cardholder conducts an approved transaction for goods and services.</li>
<li>
<h5>Escheated Balances.</h5>
The Card is subject to escheatment laws (property transfer on death of owner) in the state the Card was obtained. If the Card has been registered online, the escheatment is governed by the address indicated in the Card registration. Should your Card exceed the escheatment rules set by state law, we will be required to unload the Card and send any remaining balance to the appropriate state agency without notice to you.</li>
</ol>
<h3>Cancellation.</h3>
<ol type="a">
<li>
<h5>Issuer and Owner.</h5>
Your Card is issued by USA Technologies, Inc. We are the owner of the Card. The Card will remain our property. We may cancel your right to use the Card at any time and have you return the Card to us, although you may have a right to the existing Balance, subject to the terms of this Agreement.</li>
<li>
<h5>Change in the Program.</h5>
We may change the terms of, add new terms to, or discontinue this Agreement or the Card program at any time and without giving you notice, subject to applicable law. Check <a href="<%=StringUtils.encodeForURL(url) %>"><%=StringUtils.encodeForHTML(url) %></a> for posting of the most recent terms.</li>
<li>
<h5>Loyalty Points Value</h5>
Any loyalty points or coupons associated with the Card are surrendered at the time of account cancelation. Loyalty points do not have cash value.</li>
</ol>
<h3>Governing Law; Venue; Severability.</h3>
<p>This Agreement and its performance shall be governed by the laws and regulations of the United States and, to the extent not governed by federal laws and regulations, by the laws and regulations of the Commonwealth of Pennsylvania, notwithstanding any choice of law principles. You and the vending operator each hereby irrevocably consent and submit to the exclusive jurisdiction of the Courts of the Commonwealth of Pennsylvania, sitting in the County of Chester, or the United States District Court for the District of Pennsylvania in any and all actions and proceedings, questions or controversies arising under or related to this Agreement, the Card or its use. If any term of this Agreement is found by a court to be unenforceable or illegal, all other terms and conditions will still be in effect.</p>
<h3>Electronic Funds Transfer Act Does Not Apply.</h3>
<p>
Neither the Electronic Funds Transfer Act nor Regulation E thereunder applies to the Card. Therefore, this Agreement is the only provision that applies and the only disclosure that you will receive from us. Please keep this Agreement with the Card and provide to anyone to whom the Card is given.</p>
<h3>Questions or Concerns.</h3>
<p>
If you have any questions concerning your Card, please write to us at <%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%>, <%=translator.translate("prepaid-company-address-line1", "100 Deerfield Lane, Suite 300")%>, <%=translator.translate("prepaid-company-address-line2", "Malvern, PA 19355 USA")%> or visit us at <a href="<%=translator.translate("prepaid-company-website-url", "http://www.usatech.com/")%>"><%=translator.translate("prepaid-company-website-url", "http://www.usatech.com/")%></a>.</p>
<hr/>
<jsp:include page="/footer.jsp"/>