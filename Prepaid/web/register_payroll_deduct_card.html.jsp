<%@page import="com.usatech.ec2.EC2CardInfo"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.lang.Holder"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="java.sql.SQLException,simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);

String passSerialNumber = RequestUtils.getAttribute(request, "uuid", String.class, false);
if (!StringUtils.isBlank(passSerialNumber)){
	Map<String,Object> params = new HashMap<String,Object>();
    params.put("passSerialNumber", passSerialNumber);
    Results consumerResults = DataLayerMgr.executeQuery("GET_CONSUMER_INFO_BY_PASS_SN", params);
    if (consumerResults.next()){
    	String email = (String)consumerResults.get("email");
    	String mobile = (String)consumerResults.get("mobile");
    	if (!user.getUserName().equals(email) && !user.getUserName().equals(mobile)){
    		RequestUtils.redirectWithCarryOver(request, response, "dashboard.html", false, true);
        	return;	
    	}
    }else{
    	RequestUtils.redirectWithCarryOver(request, response, "dashboard.html", false, true);
    	return;
    }
	
	PrepaidUtils.addCardByPass(passSerialNumber, user.getConsumerId());
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "payroll-deduct-card-registered-prompt-existing-account-by-pass", "Your account username and password already exist in our system. Payroll deduct card has been linked to your account.");
	RequestUtils.redirectWithCarryOver(request, response, "payroll_deduct_cards.html", false, true);
	return;
}

Integer addConsumerAcctTypeId = RequestUtils.getAttribute(request, "addConsumerAcctTypeId", Integer.class, false);
if(addConsumerAcctTypeId==null){
	addConsumerAcctTypeId=7;
}
String card = null;
String securityCode = null;

card = RequestUtils.getAttribute(request, "card", String.class, false);
securityCode = RequestUtils.getAttribute(request, "securityCode", String.class, false);

boolean fromSignup=ConvertUtils.getBoolean(RequestUtils.getAttribute(request, "fromSignup", Boolean.class, false), false);
if(StringUtils.isBlank(card) || StringUtils.isBlank(securityCode)) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "pairoll-deduct-missing-input", "You did not provide all required fields. Please try again.");
	RequestUtils.redirectWithCarryOver(request, response, "payroll_deduct_cards.html?expand=new", false, true);
    return;
} 
Holder<String> cardNumHolder = new Holder<String>();
if(!PrepaidUtils.checkCardNum(card, cardNumHolder, true)) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "pairoll-deduct-invalid-prepaid-card", "You entered an invalid payroll deduct card number. Please enter a valid one.");
	    RequestUtils.redirectWithCarryOver(request, response, "payroll_deduct_cards.html?expand=new", false, true);
	    return;
	}
long globalAccountId;
try {
	    globalAccountId = EC2ClientUtils.getGlobalAccountId(cardNumHolder.getValue());
	} catch(Throwable e) {
	    Log.getLog().warn("Could not get globalAccountId", e);
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "payroll-deduct-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
	    RequestUtils.redirectWithCarryOver(request, response, "payroll_deduct_cards.html?expand=new", false, true);
	    return;
	}
try {
	String nickname = RequestUtils.getAttribute(request, "nickname", String.class, false);
	PrepaidUtils.addPayrollDeductCard(user.getConsumerId(), globalAccountId, securityCode, nickname);	
	}
catch(SQLException e) {
   	switch(e.getErrorCode()) {
   		case 20300: case 20305:
            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-not-found", "The card you entered does not match anything on record; please re-enter it");
            break;
        case 20301:
   			RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-already-registered", "You have already registered this card. Please enter a different card");
            break;
        case 20302:
            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-registered-to-other", "This card has been registered under a different email. Please contact Customer Service for assistance");
            break;
        case 20304:
            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-active", "Your account is not active. Please contact Customer Service for assistance");
            break;
        case 1:
        	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-nickname-unique", "The nickname needs to be unique. Please try with a different nickname.");
        	break;
        default:
           	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
   	}
   	RequestUtils.redirectWithCarryOver(request, response, "payroll_deduct_cards.html?expand=new", false, true);
   	return;
}
if(fromSignup) RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "payroll-deduct-card-registered-prompt-existing-account", "Your account username and password already exist in our system. This payroll deduct card ({0}) has been linked to your account.", PrepaidUtils.maskCardNumber(cardNumHolder.getValue()));
RequestUtils.redirectWithCarryOver(request, response, "payroll_deduct_cards.html");
%>