<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="java.sql.SQLException,simple.servlet.RequestUtils"%>
<%
String password = RequestUtils.getAttribute(request, "newpassword", String.class, true);
String confirm = RequestUtils.getAttribute(request, "newconfirm", String.class, true);
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
if(!PrepaidUtils.checkPassword(RequestUtils.getOrCreateMessagesSource(request), password, confirm)) {
    RequestUtils.redirectWithCarryOver(request, response, "settings.html");
} else {
	try {
		PrepaidUtils.updatePassword(user, password);
		RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-password-changed", "Your password has been successfully changed.");
		RequestUtils.redirectWithCarryOver(request, response, "settings.html");
    } catch(SQLException e) {
    	switch(e.getErrorCode()) {
            default:
            	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-could-not-process", "We are sorry - an error occurred in processing your request. Please try again or contact Customer Service for assistance");              
    	}
%>
    	<jsp:include page="/header.jsp"/>
        <jsp:include page="/footer.jsp"/><%
    }
}%>