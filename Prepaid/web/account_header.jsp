<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
	PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
	Map<String,String> tabs = new LinkedHashMap<String,String>();
	tabs.put("dashboard", "Dashboard");
	tabs.put("cards", "Prepaid Cards");
	tabs.put("allcards", "Credit Cards");
	tabs.put("payroll_deduct_cards", "Payroll Deduct Cards");
    tabs.put("settings", "Account Settings");
%>
<% RequestUtils.setAttribute(request, "subtitle", "Account Home", "request"); 
String activeTab = RequestUtils.getAttribute(request, "tab", String.class, true);
if(!tabs.containsKey(activeTab))
	activeTab = null;
%>
<jsp:include page="/header.jsp"/>
<div class="account-content">
    <!-- tabs  -->
    <ul class="nav nav-tabs" id="accountTab"><%
for(Map.Entry<String,String> entry : tabs.entrySet()) {
	boolean active;
	if(activeTab == null) {
		activeTab = entry.getKey();
		active = true;
	} else
		active = (entry.getKey().equals(activeTab));
%><li<%if(active) { %> class="active"<%} %>><a href="<%=ToJspSimpleServlet.untranslatePath(request, "/" + entry.getKey() + ".html") %>" data-toggle="tab"><%=entry.getValue() %></a></li><% 
}%>
	</ul>
	<div class="tab-content">     
        <div class="tab-pane active"> 