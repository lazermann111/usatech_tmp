<%@page import="simple.results.BeanException"%>
<%@page import="simple.db.DataLayerException"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="javax.mail.internet.AddressException"%>
<%@page import="simple.lang.Holder"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.io.Log,simple.servlet.InputForm,simple.security.SecureHash,simple.translator.Translator"%>
<%@page import="java.sql.Connection,java.sql.SQLException,simple.db.DataLayerMgr,simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
PrepaidUser user = (PrepaidUser)RequestUtils.getUser(request);
Connection conn = DataLayerMgr.getConnectionForCall("GET_CONSUMER_COMM_DETAILS");
try{
	PrepaidUtils.sendCompleteRegistrationEmail(conn, null, -1, null, null, user.getConsumerId(), null, RequestUtils.getBaseUrl(request), request);
	conn.commit();
	RequestUtils.setAttribute(request, "message", "Registration verification passcode has been resent to you.", "request"); 
}catch(SQLException e) {
	try {
		conn.rollback();
	} catch(SQLException e2) {
		//ignore
	}
}finally{
	conn.rollback();
}
%>
<jsp:forward page="verify_user.html?userId=<%=user.getConsumerId()%>"/>		