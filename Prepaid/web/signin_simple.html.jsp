<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.usatech.prepaid.web.NotRegisteredException"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="simple.servlet.steps.LoginFailureException"%>
<%@page import="simple.servlet.SimpleServlet,simple.servlet.InputForm,java.util.Map,simple.bean.ConvertUtils,simple.servlet.InputFile,simple.servlet.RequestUtils,simple.text.StringUtils"%>
<%@page import="java.util.Set,java.util.HashSet,java.util.Arrays,simple.servlet.SimpleAction,simple.servlet.NotLoggedOnException,simple.servlet.NoSessionTokenException,simple.servlet.InvalidSessionTokenException"%>
<%! 
protected static final Set<String> ignoreParameters = new HashSet<String>(Arrays.asList(new String[] {
    "username",
    "password",
    "loginPromptPage",
    "simple.servlet.steps.LogonStep.forward",
    "content",
    "session-token",
    "selectedMenuItem"
}));
%><%
RequestUtils.setAttribute(request, "subtitle", "Sign In", "request");
RequestUtils.setAttribute(request, "signUpLink", true, "request");
RequestUtils.setAttribute(request, "wrapNarrow", true, "request"); 
String cardBrand = RequestUtils.getTranslator(request).translate("prepaid-card-brand", "loyalty");
String contentPath = request.getRequestURI();
if(contentPath == null)
	contentPath = PrepaidUtils.getHomePage();
else if (!PrepaidUtils.shouldForwardAfterLogin(contentPath.toLowerCase()))
    contentPath = PrepaidUtils.getHomePage();
Throwable exception = (Throwable)RequestUtils.getAttribute(request, SimpleAction.ATTRIBUTE_EXCEPTION, false);
InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);

boolean expired;
if(session != null) {
    if(session.isNew())
        expired = false;
    else if(session.getMaxInactiveInterval() > 0)
        expired = (System.currentTimeMillis() - session.getLastAccessedTime())/ 1000 > session.getMaxInactiveInterval();
    else
        expired = false;
} else
    expired = false;
%><jsp:include page="/header.jsp"/>
<div class="form-wrap">
	<h1>Sign in</h1>
	<div class="signin-form">
		<form id="signin-form" method="post" action="<%=StringUtils.encodeForHTMLAttribute(contentPath)%>" onsubmit="App.updateClientTimestamp(this)">
           <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/><%
for(Map.Entry<String,Object> param : form.getParameters().entrySet()) {
    if(ignoreParameters.contains(param.getKey())) continue;
    if(param.getValue() instanceof String) {
        %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute((String)param.getValue())%>"/><%
    } else if(param.getValue() instanceof String[]) {
        String[] values = (String[])param.getValue();
        for(int i = 0; i < values.length; i++) {
            %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(values[i])%>"/><%
        }
    } else if(param.getValue() instanceof InputFile || param.getValue() instanceof InputFile[]) {
        RequestUtils.storeForNextRequest(request.getSession(), param.getKey(), param.getValue());
    } else if(param.getValue() != null) {
        String value = ConvertUtils.getStringSafely(param.getValue());
        %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(value)%>"/><%
    }
}
String infoMessage = RequestUtils.getAttribute(request, "onloadMessage", String.class, false);
if(infoMessage != null && infoMessage.trim().length() > 0) { 
%><div class="alert alert-success"><%=infoMessage%></div><% 
}
if(exception instanceof LoginFailureException) { 
%><div class="alert alert-error"><%=RequestUtils.getTranslator(request).translate("prepaid-login-failed-prompt", "Your 19-digit card number and security code are invalid; please re-enter your login information.") %></div><% 
} else if((exception instanceof NotLoggedOnException && expired)
        || (exception instanceof NoSessionTokenException && (null != RequestUtils.getSessionToken(request) || null != request.getParameter(SimpleServlet.REQUEST_SESSION_TOKEN)))) { 
%><div class="alert alert-error"><%=RequestUtils.getTranslator(request).translate("prepaid-login-expired-prompt", "Your session has expired; please re-enter your login information.") %></div><% 
} else if(exception instanceof InvalidSessionTokenException) { 
%><div class="alert alert-error"><%=RequestUtils.getTranslator(request).translate("prepaid-invalid-request", "The request was invalid, please login again for the protection of your account.") %></div><% 
}
Cookie cookie = form.getCookie("username");
String username = RequestUtils.getAttribute(request, "username", String.class, false);
if(StringUtils.isBlank(username)) {
    if(cookie != null)
        username = cookie.getValue();
}
%>
			<fieldset>
				<label><span>Your 19-digit Card Number</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-cardnumber_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input type="text" name="card" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card number" title="The 19-digit <%=StringUtils.prepareCDATA(cardBrand) %> card number" required="required" maxlength="19" autofocus="autofocus" autocomplete="off" pattern="\d{19}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "card", String.class, false))%>"/>
                <label><span>The 4-digit Card Security Code</span><a class="btn-link pull-right" data-toggle="popover" data-placement="top" data-offset="{-50, -10}" data-trigger="hover&amp;click" data-content="&lt;img src='/images/morecard-securitycode_highlighted.gif' alt=''/&gt;">Where is this?</a></label>
                <input type="password" name="securityCode" placeholder="Enter your <%=StringUtils.prepareCDATA(cardBrand) %> card's security code" title="The 4-digit security code on the back of your <%=StringUtils.prepareCDATA(cardBrand) %> card" required="required" maxlength="4" autocomplete="off" pattern="\d{4}" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "securityCode", String.class, false))%>"/>
                <label class="checkbox">
                    <input type="checkbox" name="remember" value="true" title="Remember your card number for the next session"<%if(cookie != null) { %> checked="checked"<%} %>/> Remember Me
				</label>
				<div style="text-align: center;">
					<button type="submit" class="btn btn-large btn-submit">Login</button>
				</div>
			</fieldset>
		</form>
	</div>
	<!-- /signin-form -->
	<p>Need an account? <a href="<%=ToJspSimpleServlet.untranslatePath(request, "/signup_question.html") %>">Create one</a>.</p>
</div>
<hr/>
<!-- /form-wrap -->
<jsp:include page="/footer.jsp"/>
