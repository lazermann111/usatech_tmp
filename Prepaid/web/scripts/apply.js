/*
 *  This sets up various utility functions mostly for form validation and field edit masks.
 *  Requires MooTools v.1.4 and (optionally) Meio Mask v.2.0.1.0
 */
Element.implement({
	getText: function() { return this.get('text'); },		
	setText: function(text) { return this.set('text', text); },	
	setHTML: function(html) { return this.set('html', html); },		
	getHTML: function() { return this.get('html'); },
	getTag: function() { return this.get('tag'); },			
	getValue: function() { return this.get('value'); },
	setValue: function(value) { return this.set('value', value); },
	fireCustomEvent: function(type, props) {
		var handler = this.get("on" + type); 
		if(!handler) 
			return;
		if(typeOf(handler) != 'function') {
			handler = new Function("event", String.from(handler));
		}
		var evtProps = {type: type, target: this, returnValue: true};
		if(props)
			evtProps = Object.merge(props, evtProps);			
		return handler.apply(this, [ new DOMEvent(evtProps) ]);
	}
});

Function.implement({
	later: function(delay, bind, args) {
		var fn = this;
		return function() {
			fn.delay(delay, bind, args);
		};
	}
});

window.App = {};

(function() {
	Browser.post = function(url, data, target) {
		var frm = new Element("form", { method: "post", action: String.from(url), target: target });
		if(typeOf(data) == 'element' || (typeOf(data) == 'elements')) {
			frm.adopt(data);
		} else if(typeOf(data) != 'null') {
			if(typeOf(data) != 'object')
				data = String.from(data).parseQueryString();
			Object.each(data, function(value, key) {
				frm.adopt(new Element("input", {type: "hidden", name: key, value: value}));
			});
		}
		$(document.documentElement).adopt(frm);
		frm.submit();
	};
	var containsString = function(item) {
		return String.from(item) == String.from(this);
	};
	Element.Properties.value = {
			set: function(value){
				if(this.get("tag") == "select" && (!Browser.ie|| this.multiple)) {
					var values = Array.from(value);
					Array.each(this.options, function(option){
						option.selected = values.some(containsString, option.value);
					});
				} else {
					this.value = value;
				}
			},
			get: function(){
				if(this.get("tag") == "select" && (!Browser.ie|| this.multiple)) {
					var values = [];
					Array.each(this.options, function(option){
						if(option.selected)
							values.push(option.value);
					});
					switch(values.length) {
						case 0: return "";
						case 1: return values[0];
						default: return values;
					}
				} else {
					return this.value;
				}
			}		
	};
	var extractValidatorProps = window.Form.Validator.extractValidatorProps = function(el) {
		var vals = el.get('validators').filter(function(cls){
				return cls.test(':');
			}), 
			props = {};
		if (vals.length){
			vals.each(function(cls){
				var quoteIndex = cls.indexOf(':');
				if(quoteIndex > 0 && cls.length > quoteIndex+1){
					var propValue = cls.substring(quoteIndex+1),
						propName = cls.substring(0,quoteIndex);
					try {
						props[propName] = JSON.decode(propValue);
					} catch(e){}
				}
			});
		}
		el.store('$moo:validatorProps', props);
		return props;
	};
	Element.Properties.validatorProps.get = function(props){
		if(props) this.set(props);
		if((props=this.retrieve('$moo:validatorProps'))) return props;
		if((props=(this.getProperty('validatorProps') || this.getProperty('data-validator-properties')))) {
			try {
				props = JSON.decode(props);
			} catch(e) {
				return {};
			}
			this.store('$moo:validatorProps', props);
			return props;
		} else {
			return extractValidatorProps(this);
		}
	};
	App.updateClientTimestamp = function(form) {
		form = $(form);
		var cts = $(form.elements["clientTimestamp"]);
		if(!cts) {
			cts = new Element("input", { type: "hidden", name: "clientTimestamp"});
			form.adopt(cts);
		}
		var cto = $(form.elements["clientTimezoneOffset"]);
		if(!cto) {
			cto = new Element("input", { type: "hidden", name: "clientTimezoneOffset"});
			form.adopt(cto);
		}
		var d = new Date();
		cts.setValue(d.toString());	
		cto.setValue(d.getTimezoneOffset());
	}
	
})();

// sorting function----
(function() {
	//sortOrder: 0 = none, 1 = ASC, 2 = DESC
	var headerClasses = ["sortASC", "sortDESC"];
	var sortValueAttribute = 'data-sort-value';
	var compareFunctions = {
			number: function(v1,v2) {
				try {
					return parseFloat(v1) - parseFloat(v2);
				} catch(e) {
					return v1.localeCompare(v2);
				}
			},
			date: function(v1,v2) {
				try {
					return new Date(v1).valueOf() - new Date(v2).valueOf();
				} catch(e) {
					return v1.localeCompare(v2);
				}
			},
			string: function(v1,v2) {
				return v1.localeCompare(v2);
			},
			other: function(v1,v2) {
				return v1.localeCompare(v2);
			}			
	};
	var sortMults = [1, -1];
	var sorting = false;

	function styleHeaders(tr, sortedTh) {
		Array.each(tr.cells, function(item) { 
			Array.each(headerClasses, function(cls) {
				$(item).removeClass(cls);
			}); 
		});
		sortedTh.addClass(headerClasses[sortedTh.sortOrder - 1]);
	}
	function getSortFunction(sortType, columnIndex, sortOrder) {
		if(!sortOrder) sortOrder = "ASC";
		return new Function("tr1", "tr2", "return " +
			(sortOrder.toUpperCase() == "DESC" ? "-" : "") + "sort(tr1, tr2, '"
			+ sortType + "', " + columnIndex + ");");
	}

	function sortRaw(tr1, tr2, sortType, columnIndex) {
		var v1 = tr1.cells.item(columnIndex).getAttribute(sortValueAttribute);
		var v2 = tr2.cells.item(columnIndex).getAttribute(sortValueAttribute);
		//alert("Value1=" + v1 + " (1cc=" + v1.charCodeAt(0) + "); Value2=" + v2 + " (1cc=" + v2.charCodeAt(0) + ")");
		if(v1 == null || v1.trim().length == 0) return v2 == null ? 0 : -1;
		else if(v2 == null || v2.trim().length == 0) return 1;
		var comp = compareFunctions[String.from(sortType).trim().toLowerCase()] || compareFunctions.other;
		return comp(v1,v2);
	}
	
	function doSort(sortType, sortHeader) {
		try {
			var sortOrder = 2 - (((sortHeader.sortOrder || 0) + 1) % 2);
			sortHeader.sortOrder = sortOrder;
			var tr = $(sortHeader.parentNode);
			styleHeaders(tr, sortHeader);
			var tbody;
			if(tr.getParent().get("tag") == "table")
				tbody = tr.getNext("tbody");
			else
				tbody = tr.getParent().getNext("tbody");
			if(tbody == null)
				return; // nothing to sort
			while(!tbody.isVisible()) {
				tbody = tbody.getNext("tbody");
				if(tbody == null)
					return; // nothing to sort
			}
			var start = 0;
			if(tr.parentNode == tbody) {
				start = tr.sectionRowIndex + 1;
			}
			if(!tbody.sortingArray) {
				tbody.sortingArray = new Array();
				for(var i = start; i < tbody.rows.length; i++) tbody.sortingArray.push(tbody.rows[i]);
			}
			var arr = tbody.sortingArray;
			arr.sort(function(tr1,tr2) {
				return sortMults[sortOrder - 1] * sortRaw(tr1, tr2, sortType, sortHeader.cellIndex);
			});
			//arrange on table
			var re = /(.*)(?:(?:odd)|(?:even))(.*)/i;
			if(tbody.moveRow) { // for IE - for some reason it is much faster to do this first for IE
				for(var i = start; i - start < arr.length; i++) {
					tbody.removeChild(arr[i-start]);
				}
			}
			for(var i = start; i - start < arr.length; i++) {
				if(arr[i-start].className) {
					var mtch = arr[i-start].className.match(re);
					if(mtch) arr[i-start].className = mtch[1] + ((i-start) % 2 == 1 ? "even" : "odd") + mtch[2];
				}
				tbody.appendChild(arr[i-start]);
			}
		} catch(e) {
			alert("Error while sorting: " + e.message + " - " + (e.number & 0xFFFF) + " - " + e.description);
		} finally {
			$(document.body).setStyle("cursor", "default");
			window.status = "Done";
			sorting = false;
		}
	};
	window.sortData = function(anch, sortType) {
		if(sorting)
			return;
		sorting = true;
		$(document.body).setStyle("cursor", "wait");
		window.status = "Sorting column. Please wait...";
		var sortHeader = $(anch).getParent("th");
		setTimeout(doSort.pass([sortType, sortHeader]), 1);
	}
})();

window.addEvent("domready", function() {
	/*
	options: {
		toggle: "value", // the value of the data-toggle attribute; either this or selector is REQUIRED
		map: {}, // attribute to class options
		getOptions: function(el) { }, // to get the options
		applicator: function(el, ops) { }, // to do the creation; REQUIRED
		selector: "*[" + toggle + "]" // to get the elements to apply it to
	},*/
	var setupToggles = function(mappings) {
		$$("*[data-toggle]").each(function(el) {
			var options = mappings[el.getAttribute("data-toggle")];
			if(!options || !options.applicator)
				return;
			if(!options.getOptions) {
				if(options.map)
					options.getOptions = function(el) {
				    	var ops = {};
				    	Object.each(options.map, function(fn, key) {
				    		var v = fn(el);
				    		if(v != null)
				    			ops[key] = v;
				    	});
				    	return ops;
					};
				else
					options.getOptions = function(el) {
				    	var ops = {};
				    	Array.each(el.attributes, function(att, key) {
				    		if("data-toggle" != att.name && /^data-/.test(att.name) && att.value != null) 
				    			ops[att.name.substr(5).camelCase()] = att.value;
				    	});
				    	return ops;
					};
			}
			var ops = options.getOptions(el);
			options.applicator(el, ops);
		});
	}

	setupToggles({
		popover: {
			applicator: function(el, ops) { 
				if(!ops.location)
					ops.location = ops.placement;
				if(ops.offset) {
					var m;
					if(typeOf(ops.offset) == 'string' && (m=ops.offset.match(/^\s*\{\s*(-?\d+(?:\.\d+)?)\s*,\s*(-?\d+(?:\.\d+)?)\s*\}\s*$/))) {
						ops.offset = {x: parseInt(m[1]), y: parseInt(m[2])};
					} else {
						ops.offset = parseInt(ops.offset);
						if(isNaN(ops.offset))
							delete ops.offset;
					}
				}
				if(ops.title)
					ops.getTitle = function() { return ops.title; };
				delete ops.content;
				new Bootstrap.Popover(el, ops); 
			}
		},
		calendar: {
			applicator: function(el, ops) { 
				ops.field = el;
				el.pikaday = new Pikaday(ops);
				el.originalDate = el.pikaday._d; 
			}
		},
		carousel: {
			applicator: function(el, ops) { 
				ops.container = el;
				["distance","scroll","current"].each(function(item) { if(ops[item]) ops[item] = parseInt(ops[item]);});
				new Carousel.Extra(ops); 
			}
		},
		expand: {
			applicator: function(el, ops) { 
				["duration"].each(function(item) { if(ops[item]) ops[item] = parseInt(ops[item]);});
				var fx = new Fx.Reveal($(ops.target), ops);
				el.addEvent("click", fx.toggle.bind(fx));
			}
		},
		sort: {
			applicator: function(el, ops) {
				el.addEvent("click", sortData.pass([el, ops.sortType]));
			}
		}
	});
	
	var doSelectFn = function(option) {
		return option.fireCustomEvent("select");
	};
	var onSelectFn = function() {
		return this.getSelected().every(doSelectFn);
	};
	$$("select").each(function(select) {
		if(select.options && select.options.length && (select.options[0].onselect || select.options[0].getAttribute("onselect"))) {					
			select.addEvent('change', onSelectFn);
			select.getSelected().each(doSelectFn);
		}
	});
	
	/*
	var interfaceToTag = {
		ulist: 'ul', 
		olist: 'ol',
		dlist: 'dl',
		directory: 'dir',
		paragraph: 'p',
		heading: '(h1|h2|h3|h4|h5|h6)',
		quote: 'q',
		mod: '(ins|del)',
		anchor: 'a',
		image: 'img',
		tablecaption: 'caption',
		tablecol: '(col|colgroup)',
		tablesection: '(thead|tfoot|tbody)',
		tablerow: 'tr',
		tablecell: '(th|td)'
	};
	var elementPrototypeRE = /\[object HTML(\w+)ElementPrototype\]/
	*/
	var trackChanges = function(obj, prop, handler, tag, cover) {
		var setFn = (cover ? function(value) {
				var self = $(this),
					orig = self.retrieve("$prop:" + prop);
				if(orig == value)
					return;
				self.store("$prop:" + prop, value);	
				handler.apply(this, [value, orig]);
			} : function(value) {
				var self = $(this),
					orig = self.getAttribute(prop);
				if(orig == value)
					return;
				self.setAttribute(prop, value);	
				handler.apply(this, [value, orig]);
			}), 
			getFn = (cover ? function() { return $(this).retrieve("$prop:" + prop); } : function() { return $(this).getAttribute(prop); }), 
			defineFn,
			m;
		if(Object.defineProperty) {
			defineFn = function(obj) {
				try {
					Object.defineProperty(obj, prop, { get: getFn, set: setFn });
				} catch(e) {
					//ignore - can't track certain built-in properties on some browsers
					if(obj.__defineSetter__) {
						obj.__defineGetter__(prop, getFn);
						obj.__defineSetter__(prop, setFn);
					} 
				}
			};
		} else if(Browser.ie7) {
			// TODO: to support IE6-7, use: onpropertychange event
		} else if(obj.__defineSetter__) {
			defineFn = function(obj) {
				obj.__defineGetter__(prop, getFn);
				obj.__defineSetter__(prop, setFn);
			};
		} 
		if(obj && defineFn) 
			defineFn(obj);
		if(tag) { // find and apply on existing
			$$(tag + "[" + prop + "]").each(function(el) {
				setFn.apply(el, [el.getAttribute(prop)]);
				if(cover) el.removeAttribute(prop);
				if(defineFn) defineFn(el); // define it on this instance also -- needed for Webkit browsers
			});
		}
	};
	
	var polyfillValidation = function() {
		// add validators
		Locale.define('en-US', 'FormValidator', {
			reqChkByName: "Please select at least one {label}.",
			minimum: "Please enter a value that is at least {minimum}.",
			maximum: "Please enter a value that is not more than {maximum}."
		});
		
		Form.Validator.add('minLength', {
			errorMsg: function(element, props){
				if (typeOf(props.minLength) != 'null')
					return Form.Validator.getMsg('minLength').substitute({minLength:props.minLength,length:element.get('value').length });
				else return '';
			},
			test: function(element, props){
				var value = element.getValue();
		    	if(value == null || value.length == 0)
			    	return true;
				if (typeOf(props.minLength) != 'null') return (value.length >= (props.minLength || 0));
				else return true;
			}
		});
		
		Form.Validator.add('minimum', {
			errorMsg: function(element, props){
				if (typeOf(props.minimum) != 'null')
					return Form.Validator.getMsg('minimum').substitute({minimum:props.minimum,length:element.get('value').length });
				else return '';
			},
			test: function(element, props){
				var value = element.getValue();
		    	if(value == null || value.length == 0)
			    	return true;
				if (typeOf(props.minimum) == 'null') 
					return true;
				var n = new Number(value);
				var min = new Number(props.minimum);
				return n != Number.NaN && min != Number.NaN && n >= min;
			}
		});
		
		Form.Validator.add('maximum', {
			errorMsg: function(element, props){
				if (typeOf(props.maximum) != 'null')
					return Form.Validator.getMsg('maximum').substitute({maximum:props.maximum,length:element.get('value').length });
				else return '';
			},
			test: function(element, props){
				var value = element.getValue();
		    	if(value == null || value.length == 0)
			    	return true;
				if (typeOf(props.maximum) == 'null') 
					return true;
				var n = new Number(value);
				var max = new Number(props.maximum);
				return n != Number.NaN && min != Number.NaN && n <= max;
			}
		});
		
		Form.Validator.add('validate-regex', {
			errorMsg: function(element, props){
				return "The data is formatted incorrectly.";
			},
			test: function(element, props){
				element = $(element);
		    	if(!element)
			    	return false;
		    	var value = element.getValue();		
		    	if(value == null || value.length == 0)
			    	return true;
		    	if(!props["validate-regex"])
		    		return true;
		    	return value.match(props["validate-regex"]);
			}
		});
		
		Form.Validator.add('validate-email', {
		    errorMsg: function(element){
		    	if(element.getProperty("validation-error-message") != null)
			    	return element.getProperty("validation-error-message");
		    	else
			    	return Form.Validator.getMsg.pass('email');
		    },
		    test: function(element){
		    	var value = element.getValue();
		    	if(value == null || value.length == 0)
			    	return true;
				var emailPat = /^(.+)@(.+)$/;
				var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
				var validChars = "\[^\\s" + specialChars + "\]";
				var quotedUser = "(\"[^\"]*\")";
				var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
				var atom = validChars + '+';
				var word = "(" + atom + "|" + quotedUser + ")";
				var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
				var domainPat = new RegExp("^" + atom + "(\\." + atom +")*$");
				var matchArray = value.match(emailPat);
				if(matchArray == null) {
					element.setProperty("validation-error-message", "The email address is not valid (check @ and .'s)");
					return false;
				}
				var user = matchArray[1];
				var domain = matchArray[2];
				if(user.match(userPat) == null) {
					element.setProperty("validation-error-message", "The email address username is not valid.");
					return false;
				}
				var IPArray = domain.match(ipDomainPat)
				if(IPArray != null) {
					for (var i = 1; i <= 4; i++) {
						if (IPArray[i] > 255) {
							element.setProperty("validation-error-message", "The email address destination IP address is invalid.");
							return false;
						}
					}
				} else {
					var domainArray = domain.match(domainPat);
					if (domainArray == null) {
						element.setProperty("validation-error-message", "The email address domain name is not valid.");
					    return false;
					}
					var atomPat = new RegExp(atom, "g");
					var domArr = domain.match(atomPat);
					var len = domArr.length;
					if (domArr[domArr.length-1].length < 2 || domArr[domArr.length-1].length > 6) {
						element.setProperty("validation-error-message", "The email address must end in a two-letter to six-letter domain.");
						return false;
					}
					if (len < 2) {
						element.setProperty("validation-error-message", "The email address is missing a hostname.");
						return false;
					}
				}
				element.setProperty("validation-error-message", null);				
				return true;
		    }
		});
		
		// Update Form.Validator.Inline advice for popover style
		Form.Validator.Inline.implement({
			getErrorMsgHtml: function(field, error, warn) {
				return (warn ? this.warningPrefix : this.errorPrefix) + error;
			},
			makeAdvice: function(className, field, error, warn){
				var errorMsg = this.getErrorMsgHtml(field, error, warn);
				var cssClass = (warn) ? 'warning-advice' : 'validation-advice';
				var advice = this.getAdvice(className, field);
				if (advice && this._content) {
					this._content.set('html', errorMsg);
					this._title.set('html', field.title);
				} else {
					var content = this._content = new Element("div", {html: errorMsg});
					var title = this._title = new Element("div", {html: field.title});
					advice = new Bootstrap.Popover(field, {
						location: "top",
						trigger: 'none', 
						offset: -10,
						getContent: function() {
							return content;
						},
						getTitle: function() {
							return title;
						},
						keepTitle: true
					});
					window.document.addEvent('click', this.hideAdvice.pass([className, field], this));
					advice._makeTip().addEvent('mouseover', this.hideAdvice.later(2000, this, [className, field]));
				}
				field.store('$moo:advice-' + className, advice);
				return advice;
			},
			insertAdvice: function(advice, field){
				//do nothing
			},
			showAdvice: function(className, field){
				var advice = this.getAdvice(className, field);
				if (advice && !field.retrieve('$moo:' + this.getPropName(className))){
					field.store('$moo:' + this.getPropName(className), true);
					advice.show();
					this.fireEvent('showAdvice', [field, advice, className]);
				}
			},
			hideAdvice: function(className, field){
				var advice = this.getAdvice(className, field);
				if (advice && field.retrieve('$moo:' + this.getPropName(className))){
					field.store('$moo:' + this.getPropName(className), false);
					advice.hide();
					this.fireEvent('hideAdvice', [field, advice, className]);
				}
			}
		});
				
		Element.Properties.validators = {
			get: function(){
				return this.retrieve("$moo:validators") || (this.get('data-validators') || this.className).clean().split(' ');
			}
		};
		
		// Handle HTML5 validation attributes / types
		var construct = {
			required: function(el, value) {
				if(value != "required")
					return null;
				if(el.get("tag") == "input" && el.type == "radio")
					return "validate-reqchk-byname";
				return "required";
			},
			minLength: function(el, value) {
				value = parseInt(value);
				return value > 0 ? "minLength:" + value : null;
			},
			maxLength: function(el, value) {
				value = parseInt(value);
				return value > 0 ? "maxLength:" + value : null;
			},
			min: function(el, value) {
				value = parseInt(value);
				return value > 0 ? "minimum:" + value : null;
			},
			max: function(el, value) {
				value = parseInt(value);
				return value > 0 ? "maximum:" + value : null;
			},
			pattern: function(el, value) {
				return value ? "validate-regex:" + JSON.encode(value) : null;
			}
		};
		var types = {
			number:  function(el) {
				return "validate-numeric";
			},
			email:  function(el) {
				return "validate-email";
			}
		};
			
	    var handleChange = function(value, orig, build) {
			var self = $(this),
				oldval = (orig ? build(self, orig) : null),
				val = (value ? build(self, value) : null);
			if(oldval != val) {
				var validators = (self.retrieve("$moo:validators") || []);
				if(oldval)
					validators.erase(oldval);
				if(val)
					validators.push(val);
				var validator = $(self.form).get("validator");
				validator.resetField(self);
			    self.store("$moo:validators", validators);
			    Form.Validator.extractValidatorProps(self);
				validator.resetField(self);
			}
		};
	
		var addPropTrackChanges = function (tag, inter, props) {
			Array.each(props, function(prop) {
				trackChanges(inter, prop, function(value, orig) {
					handleChange.apply(this, [value, orig, construct[prop]]);
				}, tag, true);
			});
		};
		
		addPropTrackChanges("input", (Browser.ie7 ? null : HTMLInputElement.prototype), ["required", "minLength", "maxLength", "min", "max", "pattern"]);
		addPropTrackChanges("textarea", (Browser.ie7 ? null : HTMLTextAreaElement.prototype), ["required", "minLength", "maxLength", "min", "max", "pattern"]);
		addPropTrackChanges("select", (Browser.ie7 ? null : HTMLSelectElement.prototype), ["required"]);
		
		for(var type in types) {
			trackChanges((Browser.ie7 ? null : HTMLInputElement.prototype), "type", function(value, orig) {
				handleChange(value, orig, function(self, value) {
					return (types[value] ? types[value](self) : null);
				});
			});
			$$("input[type=" + type + "]").each(function(el) {
				handleChange.apply(el, [type, null, types[type]]);
			});			
		};

		//TODO: handle forms added via javascript
		Array.each(document.forms, function(form) {			
			new Form.Validator.Inline(form);
		});
	};
	
	var polyfillPartialValidation = function() {
		var messages = {
			customError: "Please enter valid information.",
			patternMismatch: "Please match the requested format.",
			valueMissing:  "Please fill out this field",
			typeMismatch: "Please match the requested type.",
			tooLong: function(el) { return "Please enter only " + el.maxLength + " characters."; },
			rangeUnderflow: function(el) { return "Please enter at a number that is least " + el.min + "."; },
			rangeOverflow: function(el) { return "Please enter at a number that is most " + el.max + "."; },
			stepMismatch: "Please enter a valid value."
		};
		//TODO: handle forms added via javascript
		Array.each(document.forms, function(form) {			
			$(form).addEvent("submit", function() {
				if(form.checkValidity()) return true;
				// find invalid field and display popup
				Array.each(form.elements, function(el) {
					if(el.validity && !el.validity.valid) {
						el = $(el);
						var errorMsg;
						for(var key in messages){
							if(el.validity[key]) {
								var val = messages[key];
								if(typeOf(val) == "function")
									val = val(el);
								if(val) {
									errorMsg = val;
									break;
								}
							}
						}
						if(!errorMsg)
							errorMsg = "Please enter valid information.";
						var advice = el.retrieve('$moo:advice');
						if(advice) {
							advice._content.set('html', errorMsg);
							advice._title.set('html', el.title);
						} else {
							var content = new Element("div", {html: errorMsg});
							var title = new Element("div", {html: el.title});
							advice = new Bootstrap.Popover(el, {
								location: "top",
								trigger: 'none', 
								offset: -10,
								getContent: function() {
									return content;
								},
								getTitle: function() {
									return title;
								},
								keepTitle: true
							});
							advice._content = content;
							advice._title = title;
							window.document.addEvent('click', advice.hide.bind(advice));
							advice._makeTip().addEvent('mouseover', advice.hide.later(2000, advice));
						}
						el.store('$moo:advice', advice);
						advice.show();
					}
				});
				return false;
			});
		});
	};
	var polyfillPlaceholder = function() {
		var createOverText = function(el) {
			var ot = new OverText(el, { textOverride: " " });
			el.store("$overtext", ot);
			return ot;
		};
		var handler = function(value) {
			var ot = this.retrieve("$overtext");
			if(value)
				(ot || createOverText(this)).text.set("text", value);
			else if(ot)
				ot.text.set("text", "");
		};
		trackChanges((Browser.ie7 ? null : HTMLInputElement.prototype), "placeholder", handler, "input", true);
		trackChanges((Browser.ie7 ? null : HTMLTextAreaElement.prototype), "placeholder", handler, "textarea", true);
	};
	
	var supportsPlaceholder = (window.Modernizr ? Modernizr.input.placeholder : (function() {
		try {
			return new Element('div', {html: "<input placeholder='x'/>"}).getFirst().placeholder == 'x';
		} catch (e){
			return false;
		}		
	})());
	var supportsValidation;
	if(Browser.ie7 || !HTMLFormElement.prototype.checkValidity)
		supportsValidation = 0; //No
	else {
		supportsValidation = 1; //Partial
		try {			
			// Taken from "https://github.com/Modernizr/Modernizr/blob/master/feature-detects/forms/validation.js"
			var invalidFired = false,
				input = new Element("input", {name: "modTest", required: "required", events: { invalid: function(e) {
					supportsValidation = 2; // full
					e.preventDefault();
					e.stopPropagation();
		        }}}),
		        button = new Element("button", {type: "submit"}),
		        form = new Element('form', {
					style: { visibility: 'hidden'},
					events: { submit: function(e) {
				        //Opera does not validate form, if submit is prevented
				        if(!window.opera)
				          e.preventDefault();
				        e.stopPropagation();
				    }}
				});
			form.adopt(input, button);
			
			document.body.appendChild(form);
			// Submit form by clicking submit button: Calling form.submit() doesn't trigger interactive validation, use a submit button instead
	        button.click();
	        form.dispose();
		} catch (e){
			supportsValidation = 3; //Error
		}	
	}
		
	if(!supportsPlaceholder) 
		polyfillPlaceholder();
	switch(supportsValidation) {
		case 0: case 3:
			polyfillValidation();
			break;
		case 1:
			polyfillPartialValidation();
			break;
	}		
});

function trim(s) {
	return s.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '');
}

function validateForm(form) {
	for(var i = 0; i < form.elements.length; i++) {
		if(form.elements[i].type=='hidden')
			continue;
		var elem = $(form.elements[i]);
		var label = elem.getAttribute("label");
		if(typeof(label) == "string" && label.length > 0 && !elem.disabled) {
			if(form.getAttribute("bulk") == "true") {
				var cb = document.getElementById("cb_" + elem.id);
				if(cb && !cb.checked)
					continue;
			}
			var value = elem.value;
			var required = elem.getAttribute("usatRequired"); 
			if(typeof(required) == "string" && "true" == required && value.length == 0) {
				alert(label + " is required. Please enter a value.");
				elem.focus();
				elem.addClass("invalidValue");
				elem.select();
				return false;
			}
			if(typeof(value) == "string") {
				/* regex match */
				var valid = elem.getAttribute("valid");
				if(typeof(valid) == "string" && (valid=trim(valid)).length > 0 && value.length > 0) {
					if(!value.match(valid)) {
						alert(label + " is not valid. Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					}
				}
				/*less than or equal to max*/
				var maxValue = elem.getAttribute("maxValue");
				if(value.length > 0 && typeof(maxValue) == "string" && (maxValue=trim(maxValue)).length > 0) {
					if(!value.match(/^\-?\d+(\.\d+)?$/)) {
						alert(label + " is not a number. Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					} else if(parseFloat(value) > parseFloat(maxValue)) {
						alert(label + " must be less than " + maxValue + ". Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					}
				}
				/* greater than or equal to min*/
				var minValue = elem.getAttribute("minValue");
				if(value.length > 0 && typeof(minValue) == "string" && (minValue=trim(minValue)).length > 0) {
					if(!value.match(/^\-?\d+(\.\d+)?$/)) {
						alert(label + " is not a number. Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					} else if(parseFloat(value) < parseFloat(minValue)) {
						alert(label + " must be greater than " + minValue + ". Please correct.");
						elem.focus();
						elem.addClass("invalidValue");
						elem.select();
						return false;
					}
				}
			}
			elem.removeClass("invalidValue");
		}
	}
	return true;
}
