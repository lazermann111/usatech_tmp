
//This library: http://dev.clientcide.com/depender/build?download=true&version=MooTools+Bootstrap&excludeLibs=Core&require=Bootstrap%2FBootstrap.Popover&require=Bootstrap%2FPopup&excludeLibs=More
//Contents: Bootstrap:Source/UI/CSSEvents.js, Bootstrap:Source/UI/Bootstrap.js, Bootstrap:Source/UI/Bootstrap.Popup.js, Behavior:Source/Element.Data.js, Behavior:Source/BehaviorAPI.js, Behavior:Source/Behavior.js, Bootstrap:Source/UI/Bootstrap.Tooltip.js, Bootstrap:Source/UI/Bootstrap.Popover.js

// Begin: Source/UI/CSSEvents.js
/*
---

name: CSSEvents

license: MIT-style

authors: [Aaron Newton]

requires: [Core/DomReady]

provides: CSSEvents
...
*/

Browser.Features.getCSSTransition = function(){
	Browser.Features.cssTransition = (function () {
		var thisBody = document.body || document.documentElement
			, thisStyle = thisBody.style
			, support = thisStyle.transition !== undefined || thisStyle.WebkitTransition !== undefined || thisStyle.MozTransition !== undefined || thisStyle.MsTransition !== undefined || thisStyle.OTransition !== undefined;
		return support;
	})();

	// set CSS transition event type
	if ( Browser.Features.cssTransition ) {
		Browser.Features.transitionEnd = "TransitionEnd";
		if ( Browser.safari || Browser.chrome ) {
			Browser.Features.transitionEnd = "webkitTransitionEnd";
		} else if ( Browser.firefox ) {
			Browser.Features.transitionEnd = "transitionend";
		} else if ( Browser.opera ) {
			Browser.Features.transitionEnd = "oTransitionEnd";
		}
	}
	Browser.Features.getCSSTransition = Function.from(Browser.Features.transitionEnd);
};

window.addEvent("domready", Browser.Features.getCSSTransition);

// Begin: Source/UI/Bootstrap.js
/*
---

name: Bootstrap

description: The BootStrap namespace.

authors: [Aaron Newton]

license: MIT-style license.

provides: [Bootstrap]

...
*/
var Bootstrap = {};

// Begin: Source/UI/Bootstrap.Popup.js
/*
---

name: Popup

description: A simple Popup class for the Twitter Bootstrap CSS framework.

authors: [Aaron Newton]

license: MIT-style license.

requires:
 - Core/Element.Delegation
 - Core/Fx.Tween
 - Core/Fx.Transitions
 - More/Mask
 - More/Elements.From
 - More/Element.Position
 - More/Element.Shortcuts
 - More/Events.Pseudos
 - /CSSEvents
 - /Bootstrap

provides: [Bootstrap.Popup]

...
*/

Bootstrap.Popup = new Class({

	Implements: [Options, Events],

	options: {
		/*
			onShow: function(){},
			onHide: function(){},
			animate: function(){},
			destroy: function(){},
		*/
		persist: true,
		closeOnClickOut: true,
		closeOnEsc: true,
		mask: true,
		animate: true,
		changeDisplayValue: true
	},

	initialize: function(element, options){
		this.element = document.id(element).store('Bootstrap.Popup', this);
		this.setOptions(options);
		this.bound = {
			hide: this.hide.bind(this),
			bodyClick: function(e){
				if (!this.element.contains(e.target)){
					this.hide();
				}
			}.bind(this),
			keyMonitor: function(e){
				if (e.key == 'esc') this.hide();
			}.bind(this),
			animationEnd: this._animationEnd.bind(this)
		};
		if ((this.element.hasClass('fade') && this.element.hasClass('in')) ||
		    (!this.element.hasClass('hide') && !this.element.hasClass('fade'))){
			if (this.element.hasClass('fade')) this.element.removeClass('in');
			this.show();
		}
	},

	toElement: function(){
		return this.element;
	},

	_checkAnimate: function(){
		var check = this.options.animate !== false && Browser.Features.getCSSTransition() && (this.options.animate || this.element.hasClass('fade'));
		if (!check) {
			this.element.removeClass('fade').addClass('hide');
			this._mask.removeClass('fade').addClass('hide');
		} else if (check) {
			this.element.addClass('fade').removeClass('hide');
			this._mask.addClass('fade').removeClass('hide');
		}
		return check;
	},

	show: function(){
		if (this.visible || this.animating) return;
		this.element.addEvent('click:relay(.close, .dismiss)', this.bound.hide);
		if (this.options.closeOnEsc) document.addEvent('keyup', this.bound.keyMonitor);
		this._makeMask();
		this._mask.inject(document.body);
		this.animating = true;
		if (this.options.changeDisplayValue) this.element.show();
		if (this._checkAnimate()){
			this.element.offsetWidth; // force reflow
			this.element.addClass('in');
			this._mask.addClass('in');
		} else {
			this.element.show();
			this._mask.show();
		}
		this.visible = true;
		this._watch();
	},

	_watch: function(){
		if (this._checkAnimate()) this.element.addEventListener(Browser.Features.getCSSTransition(), this.bound.animationEnd);
		else this._animationEnd();
	},

	_animationEnd: function(){
		if (Browser.Features.getCSSTransition()) this.element.removeEventListener(Browser.Features.getCSSTransition(), this.bound.animationEnd);
		this.animating = false;
		if (this.visible){
			this.fireEvent('show', this.element);
		} else {
			this.fireEvent('hide', this.element);
			if (this.options.changeDisplayValue) this.element.hide();
			if (!this.options.persist){
				this.destroy();
			} else {
				this._mask.dispose();
			}
		}
	},

	destroy: function(){
		this._mask.destroy();
		this.fireEvent('destroy', this.element);
		this.element.destroy();
		this._mask = null;
		this.destroyed = true;
	},

	hide: function(event, clicked){
		if (clicked) {
			var immediateParentPopup = clicked.getParent('[data-behavior~=BS.Popup]');
			if (immediateParentPopup && immediateParentPopup != this.element) return;
		}
		if (!this.visible || this.animating) return;
		this.animating = true;
		if (event && clicked && clicked.hasClass('stopEvent')){
			event.preventDefault();
		}
		document.id(document.body).removeEvent('click', this.bound.hide);
		document.removeEvent('keyup', this.bound.keyMonitor);
		this.element.removeEvent('click:relay(.close, .dismiss)', this.bound.hide);

		if (this._checkAnimate()){
			this.element.removeClass('in');
			this._mask.removeClass('in');
		} else {
			this.element.hide();
			this._mask.hide();
		}
		this.visible = false;
		this._watch();
	},

	// PRIVATE

	_makeMask: function(){
		if (this.options.mask){
			if (!this._mask){
				this._mask = new Element('div.modal-backdrop');
				if (this._checkAnimate()) this._mask.addClass('fade');
			}
		}
		if (this.options.closeOnClickOut){
			if (this._mask) this._mask.addEvent('click', this.bound.hide);
			else document.id(document.body).addEvent('click', this.bound.hide);
		}
	}

});

// Begin: Source/Element.Data.js
/*
---
name: Element.Data
description: Stores data in HTML5 data properties
provides: [Element.Data]
requires: [Core/Element, Core/JSON]
script: Element.Data.js

...
*/
(function(){

	JSON.isSecure = function(string){
		//this verifies that the string is parsable JSON and not malicious (borrowed from JSON.js in MooTools, which in turn borrowed it from Crockford)
		//this version is a little more permissive, as it allows single quoted attributes because forcing the use of double quotes
		//is a pain when this stuff is used as HTML properties
		return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(string.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '').replace(/'[^'\\\n\r]*'/g, ''));
	};

	Element.implement({
		/*
			sets an HTML5 data property.
			arguments:
				name - (string) the data name to store; will be automatically prefixed with 'data-'.
				value - (string, number) the value to store.
		*/
		setData: function(name, value){
			return this.set('data-' + name.hyphenate(), value);
		},

		getData: function(name, defaultValue){
			var value = this.get('data-' + name.hyphenate());
			if (value != undefined){
				return value;
			} else if (defaultValue != undefined){
				this.setData(name, defaultValue);
				return defaultValue;
			}
		},

		/* 
			arguments:
				name - (string) the data name to store; will be automatically prefixed with 'data-'
				value - (string, array, or object) if an object or array the object will be JSON encoded; otherwise stored as provided.
		*/
		setJSONData: function(name, value){
			return this.setData(name, JSON.encode(value));
		},

		/*
			retrieves a property from HTML5 data property you specify
		
			arguments:
				name - (retrieve) the data name to store; will be automatically prefixed with 'data-'
				strict - (boolean) if true, will set the JSON.decode's secure flag to true; otherwise the value is still tested but allows single quoted attributes.
				defaultValue - (string, array, or object) the value to set if no value is found (see storeData above)
		*/
		getJSONData: function(name, strict, defaultValue){
			var value = this.get('data-' + name);
			if (value != undefined){
				if (value && JSON.isSecure(value)) {
					return JSON.decode(value, strict);
				} else {
					return value;
				}
			} else if (defaultValue != undefined){
				this.setJSONData(name, defaultValue);
				return defaultValue;
			}
		}

	});

})();

// Begin: Source/BehaviorAPI.js
/*
---
name: BehaviorAPI
description: HTML getters for Behavior's API model.
requires: [Core/Class, /Element.Data]
provides: [BehaviorAPI]
...
*/


(function(){
	//see Docs/BehaviorAPI.md for documentation of public methods.

	var reggy = /[^a-z0-9\-]/gi;

	window.BehaviorAPI = new Class({
		element: null,
		prefix: '',
		defaults: {},

		initialize: function(element, prefix){
			this.element = element;
			this.prefix = prefix.toLowerCase().replace('.', '-', 'g').replace(reggy, '');
		},

		/******************
		 * PUBLIC METHODS
		 ******************/

		get: function(/* name[, name, name, etc] */){
			if (arguments.length > 1) return this._getObj(Array.from(arguments));
			return this._getValue(arguments[0]);
		},

		getAs: function(/*returnType, name, defaultValue OR {name: returnType, name: returnType, etc}*/){
			if (typeOf(arguments[0]) == 'object') return this._getValuesAs.apply(this, arguments);
			return this._getValueAs.apply(this, arguments);
		},

		require: function(/* name[, name, name, etc] */){
			for (var i = 0; i < arguments.length; i++){
				if (this._getValue(arguments[i]) == undefined) throw new Error('Could not retrieve ' + this.prefix + '-' + arguments[i] + ' option from element.');
			}
			return this;
		},

		requireAs: function(returnType, name /* OR {name: returnType, name: returnType, etc}*/){
			var val;
			if (typeOf(arguments[0]) == 'object'){
				for (var objName in arguments[0]){
					val = this._getValueAs(arguments[0][objName], objName);
					if (val === undefined || val === null) throw new Error("Could not retrieve " + this.prefix + '-' + objName + " option from element.");
				}
			} else {
				val = this._getValueAs(returnType, name);
				if (val === undefined || val === null) throw new Error("Could not retrieve " + this.prefix + '-' + name + " option from element.");
			}
			return this;
		},

		setDefault: function(name, value /* OR {name: value, name: value, etc }*/){
			if (typeOf(arguments[0]) == 'object'){
				for (var objName in arguments[0]){
					this.setDefault(objName, arguments[0][objName]);
				}
				return;
			}
			name = name.camelCase();
			this.defaults[name] = value;
			if (this._getValue(name) == null){
				var options = this._getOptions();
				options[name] = value;
			}
			return this;
		},

		refreshAPI: function(){
			delete this.options;
			this.setDefault(this.defaults);
			return;
		},

		/******************
		 * PRIVATE METHODS
		 ******************/

		//given an array of names, returns an object of key/value pairs for each name
		_getObj: function(names){
			var obj = {};
			names.each(function(name){
				var value = this._getValue(name);
				if (value !== undefined) obj[name] = value;
			}, this);
			return obj;
		},
		//gets the data-behaviorname-options object and parses it as JSON
		_getOptions: function(){
			try {
				if (!this.options){
					var options = this.element.getData(this.prefix + '-options', '{}');
					if (options === "") return this.options = {};
					if (options && options.substring(0,1) != '{') options = '{' + options + '}';
					var isSecure = JSON.isSecure(options);
					if (!isSecure) throw new Error('warning, options value for element is not parsable, check your JSON format for quotes, etc.');
					this.options = isSecure ? JSON.decode(options) : {};
					for (option in this.options) {
						this.options[option.camelCase()] = this.options[option];
					}
				}
			} catch (e){
				throw new Error('Could not get options from element; check your syntax. ' + this.prefix + '-options: "' + this.element.getData(this.prefix + '-options', '{}') + '"');
			}
			return this.options;
		},
		//given a name (string) returns the value for it
		_getValue: function(name){
			name = name.camelCase();
			var options = this._getOptions();
			if (!options.hasOwnProperty(name)){
				var inline = this.element.getData(this.prefix + '-' + name.hyphenate());
				if (inline) options[name] = inline;
			}
			return options[name];
		},
		//given a Type and a name (string) returns the value for it coerced to that type if possible
		//else returns the defaultValue or null
		_getValueAs: function(returnType, name, defaultValue){
			var value = this._getValue(name);
			if (value == null || value == undefined) return defaultValue;
			var coerced = this._coerceFromString(returnType, value);
			if (coerced == null) throw new Error("Could not retrieve value '" + name + "' as the specified type. Its value is: " + value);
			return coerced;
		},
		//given an object of name/Type pairs, returns those as an object of name/value (as specified Type) pairs
		_getValuesAs: function(obj){
			var returnObj = {};
			for (var name in obj){
				returnObj[name] = this._getValueAs(obj[name], name);
			}
			return returnObj;
		},
		//attempts to run a value through the JSON parser. If the result is not of that type returns null.
		_coerceFromString: function(toType, value){
			if (typeOf(value) == 'string' && toType != String){
				if (JSON.isSecure(value)) value = JSON.decode(value);
			}
			if (instanceOf(value, toType)) return value;
			return null;
		}
	});

})();

// Begin: Source/Behavior.js
/*
---
name: Behavior
description: Auto-instantiates widgets/classes based on parsed, declarative HTML.
requires: [Core/Class.Extras, Core/Element.Event, Core/Selectors, More/Table, /Element.Data, /BehaviorAPI]
provides: [Behavior]
...
*/

(function(){

	var getLog = function(method){
		return function(){
			if (window.console && console[method]){
				if(console[method].apply) console[method].apply(console, arguments);
				else console[method](Array.from(arguments).join(' '));
			}
		};
	};

	var PassMethods = new Class({
		//pass a method pointer through to a filter
		//by default the methods for add/remove events are passed to the filter
		//pointed to this instance of behavior. you could use this to pass along
		//other methods to your filters. For example, a method to close a popup
		//for filters presented inside popups.
		passMethod: function(method, fn){
			if (this.API.prototype[method]) throw new Error('Cannot overwrite API method ' + method + ' as it already exists');
			this.API.implement(method, fn);
			return this;
		},

		passMethods: function(methods){
			for (method in methods) this.passMethod(method, methods[method]);
			return this;
		}

	});

	var spaceOrCommaRegex = /\s*,\s*|\s+/g;

	BehaviorAPI.implement({
		deprecate: function(deprecated, asJSON){
			var set,
			    values = {};
			Object.each(deprecated, function(prop, key){
				var value = this.element[ asJSON ? 'getJSONData' : 'getData'](prop);
				if (value !== undefined){
					set = true;
					values[key] = value;
				}
			}, this);
			this.setDefault(values);
			return this;
		}
	});

	this.Behavior = new Class({

		Implements: [Options, Events, PassMethods],

		options: {
			//by default, errors thrown by filters are caught; the onError event is fired.
			//set this to *true* to NOT catch these errors to allow them to be handled by the browser.
			// breakOnErrors: false,
			// container: document.body,

			//default error behavior when a filter cannot be applied
			onError: getLog('error'),
			onWarn: getLog('warn'),
			enableDeprecation: true,
			selector: '[data-behavior]'
		},

		initialize: function(options){
			this.setOptions(options);
			this.API = new Class({ Extends: BehaviorAPI });
			this.passMethods({
				getDelegator: this.getDelegator.bind(this),
				addEvent: this.addEvent.bind(this),
				removeEvent: this.removeEvent.bind(this),
				addEvents: this.addEvents.bind(this),
				removeEvents: this.removeEvents.bind(this),
				fireEvent: this.fireEvent.bind(this),
				applyFilters: this.apply.bind(this),
				applyFilter: this.applyFilter.bind(this),
				getContentElement: this.getContentElement.bind(this),
				cleanup: this.cleanup.bind(this),
				getContainerSize: function(){
					return this.getContentElement().measure(function(){
						return this.getSize();
					});
				}.bind(this),
				error: function(){ this.fireEvent('error', arguments); }.bind(this),
				fail: function(){
					var msg = Array.join(arguments, ' ');
					throw new Error(msg);
				},
				warn: function(){
					this.fireEvent('warn', arguments);
				}.bind(this)
			});
		},

		getDelegator: function(){
			return this.delegator;
		},

		setDelegator: function(delegator){
			if (!instanceOf(delegator, Delegator)) throw new Error('Behavior.setDelegator only accepts instances of Delegator.');
			this.delegator = delegator;
			return this;
		},

		getContentElement: function(){
			return this.options.container || document.body;
		},

		//Applies all the behavior filters for an element.
		//container - (element) an element to apply the filters registered with this Behavior instance to.
		//force - (boolean; optional) passed through to applyFilter (see it for docs)
		apply: function(container, force){
		  this._getElements(container).each(function(element){
				var plugins = [];
				element.getBehaviors().each(function(name){
					var filter = this.getFilter(name);
					if (!filter){
						this.fireEvent('error', ['There is no filter registered with this name: ', name, element]);
					} else {
						var config = filter.config;
						if (config.delay !== undefined){
							this.applyFilter.delay(filter.config.delay, this, [element, filter, force]);
						} else if(config.delayUntil){
							this._delayFilterUntil(element, filter, force);
						} else if(config.initializer){
							this._customInit(element, filter, force);
						} else {
							plugins.append(this.applyFilter(element, filter, force, true));
						}
					}
				}, this);
				plugins.each(function(plugin){ plugin(); });
			}, this);
			return this;
		},

		_getElements: function(container){
			if (typeOf(this.options.selector) == 'function') return this.options.selector(container);
			else return document.id(container).getElements(this.options.selector);
		},

		//delays a filter until the event specified in filter.config.delayUntil is fired on the element
		_delayFilterUntil: function(element, filter, force){
			var events = filter.config.delayUntil.split(','),
			    attached = {},
			    inited = false;
			var clear = function(){
				events.each(function(event){
					element.removeEvent(event, attached[event]);
				});
				clear = function(){};
			};
			events.each(function(event){
				var init = function(e){
					clear();
					if (inited) return;
					inited = true;
					var setup = filter.setup;
					filter.setup = function(element, api, _pluginResult){
						api.event = e;
						return setup.apply(filter, [element, api, _pluginResult]);
					};
					this.applyFilter(element, filter, force);
					filter.setup = setup;
				}.bind(this);
				element.addEvent(event, init);
				attached[event] = init;
			}, this);
		},

		//runs custom initiliazer defined in filter.config.initializer
		_customInit: function(element, filter, force){
			var api = new this.API(element, filter.name);
			api.runSetup = this.applyFilter.pass([element, filter, force], this);
			filter.config.initializer(element, api);
		},

		//Applies a specific behavior to a specific element.
		//element - the element to which to apply the behavior
		//filter - (object) a specific behavior filter, typically one registered with this instance or registered globally.
		//force - (boolean; optional) apply the behavior to each element it matches, even if it was previously applied. Defaults to *false*.
		//_returnPlugins - (boolean; optional; internal) if true, plugins are not rendered but instead returned as an array of functions
		//_pluginTargetResult - (obj; optional internal) if this filter is a plugin for another, this is whatever that target filter returned
		//                      (an instance of a class for example)
		applyFilter: function(element, filter, force, _returnPlugins, _pluginTargetResult){
			var pluginsToReturn = [];
			if (this.options.breakOnErrors){
				pluginsToReturn = this._applyFilter.apply(this, arguments);
			} else {
				try {
					pluginsToReturn = this._applyFilter.apply(this, arguments);
				} catch (e){
					this.fireEvent('error', ['Could not apply the behavior ' + filter.name, e]);
				}
			}
			return _returnPlugins ? pluginsToReturn : this;
		},

		//see argument list above for applyFilter
		_applyFilter: function(element, filter, force, _returnPlugins, _pluginTargetResult){
			var pluginsToReturn = [];
			element = document.id(element);
			//get the filters already applied to this element
			var applied = getApplied(element);
			//if this filter is not yet applied to the element, or we are forcing the filter
			if (!applied[filter.name] || force){
				//if it was previously applied, garbage collect it
				if (applied[filter.name]) applied[filter.name].cleanup(element);
				var api = new this.API(element, filter.name);

				//deprecated
				api.markForCleanup = filter.markForCleanup.bind(filter);
				api.onCleanup = function(fn){
					filter.markForCleanup(element, fn);
				};

				if (filter.config.deprecated && this.options.enableDeprecation) api.deprecate(filter.config.deprecated);
				if (filter.config.deprecateAsJSON && this.options.enableDeprecation) api.deprecate(filter.config.deprecatedAsJSON, true);

				//deal with requirements and defaults
				if (filter.config.requireAs){
					api.requireAs(filter.config.requireAs);
				} else if (filter.config.require){
					api.require.apply(api, Array.from(filter.config.require));
				}

				if (filter.config.defaults) api.setDefault(filter.config.defaults);

				//apply the filter
				var result = filter.setup(element, api, _pluginTargetResult);
				if (filter.config.returns && !instanceOf(result, filter.config.returns)){
					throw new Error("Filter " + filter.name + " did not return a valid instance.");
				}
				element.store('Behavior Filter result:' + filter.name, result);
				//and mark it as having been previously applied
				applied[filter.name] = filter;
				//apply all the plugins for this filter
				var plugins = this.getPlugins(filter.name);
				if (plugins){
					for (var name in plugins){
						if (_returnPlugins){
							pluginsToReturn.push(this.applyFilter.pass([element, plugins[name], force, null, result], this));
						} else {
							this.applyFilter(element, plugins[name], force, null, result);
						}
					}
				}
			}
			return pluginsToReturn;
		},

		//given a name, returns a registered behavior
		getFilter: function(name){
			return this._registered[name] || Behavior.getFilter(name);
		},

		getPlugins: function(name){
			return this._plugins[name] || Behavior._plugins[name];
		},

		//Garbage collects all applied filters for an element and its children.
		//element - (*element*) container to cleanup
		//ignoreChildren - (*boolean*; optional) if *true* only the element will be cleaned, otherwise the element and all the
		//	  children with filters applied will be cleaned. Defaults to *false*.
		cleanup: function(element, ignoreChildren){
			element = document.id(element);
			var applied = getApplied(element);
			for (var filter in applied){
				applied[filter].cleanup(element);
				element.eliminate('Behavior Filter result:' + filter);
				delete applied[filter];
			}
			if (!ignoreChildren) this._getElements(element).each(this.cleanup, this);
			return this;
		}

	});

	//Export these for use elsewhere (notabily: Delegator).
	Behavior.getLog = getLog;
	Behavior.PassMethods = PassMethods;


	//Returns the applied behaviors for an element.
	var getApplied = function(el){
		return el.retrieve('_appliedBehaviors', {});
	};

	//Registers a behavior filter.
	//name - the name of the filter
	//fn - a function that applies the filter to the given element
	//overwrite - (boolean) if true, will overwrite existing filter if one exists; defaults to false.
	var addFilter = function(name, fn, overwrite){
		if (!this._registered[name] || overwrite) this._registered[name] = new Behavior.Filter(name, fn);
		else throw new Error('Could not add the Behavior filter "' + name  +'" as a previous trigger by that same name exists.');
	};

	var addFilters = function(obj, overwrite){
		for (var name in obj){
			addFilter.apply(this, [name, obj[name], overwrite]);
		}
	};

	//Registers a behavior plugin
	//filterName - (*string*) the filter (or plugin) this is a plugin for
	//name - (*string*) the name of this plugin
	//setup - a function that applies the filter to the given element
	var addPlugin = function(filterName, name, setup, overwrite){
		if (!this._plugins[filterName]) this._plugins[filterName] = {};
		if (!this._plugins[filterName][name] || overwrite) this._plugins[filterName][name] = new Behavior.Filter(name, setup);
		else throw new Error('Could not add the Behavior filter plugin "' + name  +'" as a previous trigger by that same name exists.');
	};

	var addPlugins = function(obj, overwrite){
		for (var name in obj){
			addPlugin.apply(this, [obj[name].fitlerName, obj[name].name, obj[name].setup], overwrite);
		}
	};

	var setFilterDefaults = function(name, defaults){
		var filter = this.getFilter(name);
		if (!filter.config.defaults) filter.config.defaults = {};
		Object.append(filter.config.defaults, defaults);
	};

	//Add methods to the Behavior namespace for global registration.
	Object.append(Behavior, {
		_registered: {},
		_plugins: {},
		addGlobalFilter: addFilter,
		addGlobalFilters: addFilters,
		addGlobalPlugin: addPlugin,
		addGlobalPlugins: addPlugins,
		setFilterDefaults: setFilterDefaults,
		getFilter: function(name){
			return this._registered[name];
		}
	});
	//Add methods to the Behavior class for instance registration.
	Behavior.implement({
		_registered: {},
		_plugins: {},
		addFilter: addFilter,
		addFilters: addFilters,
		addPlugin: addPlugin,
		addPlugins: addPlugins,
		setFilterDefaults: setFilterDefaults
	});

	//This class is an actual filter that, given an element, alters it with specific behaviors.
	Behavior.Filter = new Class({

		config: {
			/**
				returns: Foo,
				require: ['req1', 'req2'],
				//or
				requireAs: {
					req1: Boolean,
					req2: Number,
					req3: String
				},
				defaults: {
					opt1: false,
					opt2: 2
				},
				//simple example:
				setup: function(element, API){
					var kids = element.getElements(API.get('selector'));
					//some validation still has to occur here
					if (!kids.length) API.fail('there were no child elements found that match ', API.get('selector'));
					if (kids.length < 2) API.warn("there weren't more than 2 kids that match", API.get('selector'));
					var fooInstance = new Foo(kids, API.get('opt1', 'opt2'));
					API.onCleanup(function(){
						fooInstance.destroy();
					});
					return fooInstance;
				},
				delayUntil: 'mouseover',
				//OR
				delay: 100,
				//OR
				initializer: function(element, API){
					element.addEvent('mouseover', API.runSetup); //same as specifying event
					//or
					API.runSetup.delay(100); //same as specifying delay
					//or something completely esoteric
					var timer = (function(){
						if (element.hasClass('foo')){
							clearInterval(timer);
							API.runSetup();
						}
					}).periodical(100);
					//or
					API.addEvent('someBehaviorEvent', API.runSetup);
				});
				*/
		},

		//Pass in an object with the following properties:
		//name - the name of this filter
		//setup - a function that applies the filter to the given element
		initialize: function(name, setup){
			this.name = name;
			if (typeOf(setup) == "function"){
				this.setup = setup;
			} else {
				Object.append(this.config, setup);
				this.setup = this.config.setup;
			}
			this._cleanupFunctions = new Table();
		},

		//Stores a garbage collection pointer for a specific element.
		//Example: if your filter enhances all the inputs in the container
		//you might have a function that removes that enhancement for garbage collection.
		//You would mark each input matched with its own cleanup function.
		//NOTE: this MUST be the element passed to the filter - the element with this filters
		//      name in its data-behavior property. I.E.:
		//<form data-behavior="FormValidator">
		//  <input type="text" name="email"/>
		//</form>
		//If this filter is FormValidator, you can mark the form for cleanup, but not, for example
		//the input. Only elements that match this filter can be marked.
		markForCleanup: function(element, fn){
			var functions = this._cleanupFunctions.get(element);
			if (!functions) functions = [];
			functions.include(fn);
			this._cleanupFunctions.set(element, functions);
			return this;
		},

		//Garbage collect a specific element.
		//NOTE: this should be an element that has a data-behavior property that matches this filter.
		cleanup: function(element){
			var marks = this._cleanupFunctions.get(element);
			if (marks){
				marks.each(function(fn){ fn(); });
				this._cleanupFunctions.erase(element);
			}
			return this;
		}

	});

	Behavior.elementDataProperty = 'behavior';

	Element.implement({

		addBehaviorFilter: function(name){
			return this.setData(Behavior.elementDataProperty, this.getBehaviors().include(name).join(' '));
		},

		removeBehaviorFilter: function(name){
			return this.setData(Behavior.elementDataProperty, this.getBehaviors().erase(name).join(' '));
		},

		getBehaviors: function(){
			var filters = this.getData(Behavior.elementDataProperty);
			if (!filters) return [];
			return filters.trim().split(spaceOrCommaRegex);
		},

		hasBehavior: function(name){
			return this.getBehaviors().contains(name);
		},

		getBehaviorResult: function(name){
			return this.retrieve('Behavior Filter result:' + name);
		}

	});


})();


// Begin: Source/UI/Bootstrap.Tooltip.js
/*
---

name: Bootstrap.Tooltip

description: A simple tooltip implementation that works with the Twitter Bootstrap css framework.

authors: [Aaron Newton]

license: MIT-style license.

requires:
 - /Bootstrap
 - /CSSEvents
 - More/Element.Position
 - More/Element.Shortcuts
 - Behavior/Behavior

provides: [Bootstrap.Twipsy, Bootstrap.Tooltip]

...
*/
(function(){
	var _showing = null;
	var _clickInstalled = false;
	
	Bootstrap.Tooltip = Bootstrap.Twipsy = new Class({

	Implements: [Options, Events],

	options: {
		location: 'above', //below, left, right, bottom, top
		animate: !Browser.ie10,
		delayIn: 200,
		delayOut: 0,
		fallback: '',
		override: '',
		onOverflow: false,
		offset: 0,
		title: 'title', //element property
		trigger: 'hover', //focus, manual
		getContent: function(el){
			return el.get(this.options.title);
		}
	},

	initialize: function(el, options){
		this.element = document.id(el);
		this.setOptions(options);
		var location = this.options.location;
		if (location == 'above') this.options.location = 'top';    //bootstrap 2.0
		if (location == 'below') this.options.location = 'bottom'; //bootstrap 2.0
		this._attach();
	},

	show: function(){
		this._clear();
		this._makeTip();
		var pos, edge, offset = {x: 0, y: 0};
		switch(this.options.location){
			case 'below': case 'bottom':
				pos = 'centerBottom';
				edge = 'centerTop';
				offset.y = this.options.offset;
				break;
			case 'left':
				pos = 'centerLeft';
				edge = 'centerRight';
				offset.x = this.options.offset;
				break;
			case 'right':
				pos = 'centerRight';
				edge = 'centerLeft';
				offset.x = this.options.offset;
				break;
			default: //top
				pos = 'centerTop';
				edge = 'centerBottom';
				offset.y = this.options.offset;
		}
		if (typeOf(this.options.offset) == "object") offset = this.options.offset;
		this.tip.inject(document.body).show().position({
			relativeTo: this.element,
			position: pos,
			edge: edge,
			offset: offset
		}).removeClass('out').addClass('in');
		this.visible = true;
		if (!Browser.Features.cssTransition || !this.options.animate) this._complete();
		this.fireEvent('show');
		return this;
	},

	hide: function(){
		this._makeTip();
		this.tip.removeClass('in').addClass('out');
		this.visible = false;
		if (!Browser.Features.cssTransition || !this.options.animate) this._complete();
		this.fireEvent('hide');
		return this;
	},

	destroy: function(){
		this._detach();
		if (this.tip) this.tip.destroy();
		this.destroyed = true;
		return this;
	},

	// PRIVATE METHODS

	_makeTip: function(){
		if (!this.tip){
			var location = this.options.location;
			if (location == 'above') location = 'top';    //bootstrap 2.0
			if (location == 'below') location = 'bottom'; //bootstrap 2.0
			this.tip = new Element('div.tooltip').addClass(location)
				 .adopt(new Element('div.tooltip-arrow'))
				 .adopt(
				   new Element('div.tooltip-inner', {
				     html: this.options.override || this.options.getContent.apply(this, [this.element]) || this.options.fallback
				   })
				 );
			if (this.options.animate) this.tip.addClass('fade');
			if (Browser.Features.cssTransition && this.tip.addEventListener){
				this.tip.addEventListener(Browser.Features.transitionEnd, this.bound.complete);
			}
			this.element.set('alt', '').set('title', '');
		}
		return this.tip;
	},

	_attach: function(method){
		method = method || 'addEvents';
		this.bound = {
			enter: this._enter.bind(this),
			leave: this._leave.bind(this),
			complete: this._complete.bind(this),
			click: this._click.bind(this)
		};

		if (this.options.trigger == 'focus'){
			this.element[method]({
				focus: this.bound.enter,
				blur: this.bound.leave
			});
		} else {
			if (/(^|.*&)hover($|&.*)/.test(this.options.trigger)) {
				this.element[method]({
					mouseenter: this.bound.enter,
					mouseleave: this.bound.leave
				});
			} 
			if (/(^|.*&)click($|&.*)/.test(this.options.trigger)){
				this.element[method]({
					click: this.bound.click
				});
				this._installClick();
			}
		}
	},

	_detach: function(){
		this._attach('removeEvents');
	},

	_clear: function(){
		clearTimeout(this._inDelay);
		clearTimeout(this._outDelay);
	},

	_enter: function(){
		if (this.options.onOverflow){
			var scroll = this.element.getScrollSize(),
			    size = this.element.getSize();
			if (scroll.x <= size.x && scroll.y <= size.y) return;
		}
		this._clear();
		if (this.options.delayIn){
			this._inDelay = this.show.delay(this.options.delayIn, this);
		} else {
			this.show();
		}
	},

	_leave: function(){
		if(_showing == this)
			return;
		this._clear();
		if (this.options.delayOut){
			this._outDelay = this.hide.delay(this.options.delayOut, this);
		} else {
			this.hide();
		}
	},

	_complete: function(){
		if (!this.visible){
			this.tip.dispose();
		}
		this.fireEvent('complete', this.visible);
	},

	_click: function(){
		this._clear();
		if(_showing)
			_showing.hide();
		if(_showing == this) {
			_showing = null;
			return;
		}
		_showing = this;
		if (this.options.delayIn){
			this._inDelay = this.show.delay(this.options.delayIn, this);
		} else {
			this.show();
		}
	},
	
	_installClick: function(){
		if(_clickInstalled)
			return;
		window.document.addEvent('click', function(e){
			if(_showing == null || !_showing.tip)
				return;
			//Allow a click anywhere on the page to hide the tooltip
			var target = e.target;
			if(target == _showing.tip || _showing.tip.contains(target))
				return; // ignore a click on the tip
			if(target == _showing.element || _showing.element.contains(target))
				return; // ignore a click on the link
			_showing.hide();
			_showing = null;
		});
		_clickInstalled = true;
	}
});
})();

// Begin: Source/UI/Bootstrap.Popover.js
/*
---

name: Bootstrap.Popover

description: A simple tooltip (yet larger than Bootstrap.Tooltip) implementation that works with the Twitter Bootstrap css framework.

authors: [Aaron Newton]

license: MIT-style license.

requires:
 - /Bootstrap.Tooltip

provides: Bootstrap.Popover

...
*/

Bootstrap.Popover = new Class({

	Extends: Bootstrap.Tooltip,

	options: {
		location: 'right',
		offset: 10,
		getTitle: function(el){
			return el.get(this.options.title);
		},
		content: 'data-content',
		getContent: function(el){
			return el.get(this.options.content);
		}
	},

	_makeTip: function(){
		if (!this.tip){
			var title = this.options.getTitle.apply(this, [this.element]) || this.options.fallback;
			var content = this.options.getContent.apply(this, [this.element]);

			var inner = new Element('div.popover-inner');


			if (title) {
				var titleWrapper = new Element('h3.popover-title');
				if (typeOf(title) == "element") titleWrapper.adopt(title);
				else titleWrapper.set('html', title);
				inner.adopt(titleWrapper);
			} else {
				inner.addClass('no-title');
			}

			if (typeOf(content) != "element") content = new Element('div', { html: content});
			inner.adopt(new Element('div.popover-content').adopt(content));
			this.tip = new Element('div.popover').addClass(this.options.location)
				 .adopt(new Element('div.arrow'))
				 .adopt(inner);
			if(this.options.classname) this.tip.addClass(this.options.classname);
			if (this.options.animate) this.tip.addClass('fade');
			if (Browser.Features.cssTransition && this.tip.addEventListener){
				this.tip.addEventListener(Browser.Features.transitionEnd, this.bound.complete);
			}
			if(!this.options.keepTitle) this.element.set('alt', '').set('title', '');
		}
		return this.tip;
	}

});

/*!
 * Pikaday
 *
 * Copyright © 2013 David Bushell | BSD & MIT license | https://github.com/dbushell/Pikaday
 */

(function (root, define, factory)
{
    'use strict';

    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(function (req)
        {
            // Load moment.js as an optional dependency
            var id = 'moment';
            var moment = req.defined && req.defined(id) ? req(id) : undefined;
            return factory(moment || root.moment);
        });
    } else {
        // Browser global
        root.Pikaday = factory(root.moment);
    }
}(window, window.define, function (moment)
{
    'use strict';

    /**
     * feature detection and helper functions
     */
    var hasMoment = typeof moment === 'function',

    hasEventListeners = !!window.addEventListener,

    document = window.document,

    sto = window.setTimeout,

    hasMootools = !!window.MooTools,
    
    addEvent = function(el, e, callback, capture)
    {
        if(hasMootools) {
        	$(el).addEvent(e, callback, !!capture);
        } else if (hasEventListeners) {
            el.addEventListener(e, callback, !!capture);
        } else {
            el.attachEvent('on' + e, callback);
        }
    },

    removeEvent = function(el, e, callback, capture)
    {
    	if(hasMootools) {
    		$(el).removeEvent(e, callback, !!capture);
        } else if (hasEventListeners) {
            el.removeEventListener(e, callback, !!capture);
        } else {
            el.detachEvent('on' + e, callback);
        }
    },

    fireEvent = function(el, eventName, data)
    {
        var ev;

        if(hasMootools) {
        	$(el).fireEvent(eventName, data);
        } else if (document.createEvent) {
            ev = document.createEvent('HTMLEvents');
            ev.initEvent(eventName, true, false);
            ev = extend(ev, data);
            el.dispatchEvent(ev);
        } else if (document.createEventObject) {
            ev = document.createEventObject();
            ev = extend(ev, data);
            el.fireEvent('on' + eventName, ev);
        }
    },

    trim = function(str)
    {
        return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g,'');
    },

    hasClass = function(el, cn)
    {
        return (' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== -1;
    },

    addClass = function(el, cn)
    {
        if (!hasClass(el, cn)) {
            el.className = (el.className === '') ? cn : el.className + ' ' + cn;
        }
    },

    removeClass = function(el, cn)
    {
        el.className = trim((' ' + el.className + ' ').replace(' ' + cn + ' ', ' '));
    },

    isArray = function(obj)
    {
        return (/Array/).test(Object.prototype.toString.call(obj));
    },

    isDate = function(obj)
    {
        return (/Date/).test(Object.prototype.toString.call(obj)) && !isNaN(obj.getTime());
    },

    isLeapYear = function(year)
    {
        // solution by Matti Virkkunen: http://stackoverflow.com/a/4881951
        return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
    },

    getDaysInMonth = function(year, month)
    {
        return [31, isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    },

    setToStartOfDay = function(date)
    {
        if (isDate(date)) date.setHours(0,0,0,0);
    },

    compareDates = function(a,b)
    {
        // weak date comparison (use setToStartOfDay(date) to ensure correct result)
        return a.getTime() === b.getTime();
    },

    extend = function(to, from, overwrite)
    {
        var prop, hasProp;
        for (prop in from) {
            hasProp = to[prop] !== undefined;
            if (hasProp && typeof from[prop] === 'object' && from[prop].nodeName === undefined) {
                if (isDate(from[prop])) {
                    if (overwrite) {
                        to[prop] = new Date(from[prop].getTime());
                    }
                }
                else if (isArray(from[prop])) {
                    if (overwrite) {
                        to[prop] = from[prop].slice(0);
                    }
                } else {
                    to[prop] = extend({}, from[prop], overwrite);
                }
            } else if (overwrite || !hasProp) {
                to[prop] = from[prop];
            }
        }
        return to;
    },


    /**
     * defaults and localisation
     */
    defaults = {

        // bind the picker to a form field
        field: null,

        // automatically show/hide the picker on `field` focus (default `true` if `field` is set)
        bound: undefined,

        // the default output format for `.toString()` and `field` value
        format: 'YYYY-MM-DD',

        // the initial date to view when first opened
        defaultDate: null,

        // make the `defaultDate` the initial selected value
        setDefaultDate: false,

        // first day of week (0: Sunday, 1: Monday etc)
        firstDay: 0,

        // the minimum/earliest date that can be selected
        minDate: null,
        // the maximum/latest date that can be selected
        maxDate: null,

        // number of years either side, or array of upper/lower range
        yearRange: 10,

        // used internally (don't config outside)
        minYear: 0,
        maxYear: 9999,
        minMonth: undefined,
        maxMonth: undefined,

        isRTL: false,

        // how many months are visible (not implemented yet)
        numberOfMonths: 1,

        // internationalization
        i18n: {
            previousMonth : 'Previous Month',
            nextMonth     : 'Next Month',
            months        : ['January','February','March','April','May','June','July','August','September','October','November','December'],
            weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
            weekdaysShort : ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
        },

        // callback function
        onSelect: null,
        onOpen: null,
        onClose: null,
        onDraw: null
    },


    /**
     * templating functions to abstract HTML rendering
     */
    renderDayName = function(opts, day, abbr)
    {
        day += opts.firstDay;
        while (day >= 7) {
            day -= 7;
        }
        return abbr ? opts.i18n.weekdaysShort[day] : opts.i18n.weekdays[day];
    },

    renderDay = function(i, isSelected, isToday, isDisabled, isEmpty)
    {
        if (isEmpty) {
            return '<td class="is-empty"></td>';
        }
        var arr = [];
        if (isDisabled) {
            arr.push('is-disabled');
        }
        if (isToday) {
            arr.push('is-today');
        }
        if (isSelected) {
            arr.push('is-selected');
        }
        return '<td data-day="' + i + '" class="' + arr.join(' ') + '"><button class="pika-button" type="button">' + i + '</button>' + '</td>';
    },

    renderRow = function(days, isRTL)
    {
        return '<tr>' + (isRTL ? days.reverse() : days).join('') + '</tr>';
    },

    renderBody = function(rows)
    {
        return '<tbody>' + rows.join('') + '</tbody>';
    },

    renderHead = function(opts)
    {
        var i, arr = [];
        for (i = 0; i < 7; i++) {
            arr.push('<th scope="col"><abbr title="' + renderDayName(opts, i) + '">' + renderDayName(opts, i, true) + '</abbr></th>');
        }
        return '<thead>' + (opts.isRTL ? arr.reverse() : arr).join('') + '</thead>';
    },

    renderTitle = function(instance)
    {
        var i, j, arr,
            opts = instance._o,
            month = instance._m,
            year  = instance._y,
            isMinYear = year === opts.minYear,
            isMaxYear = year === opts.maxYear,
            html = '<div class="pika-title">',
            prev = true,
            next = true;

        for (arr = [], i = 0; i < 12; i++) {
            arr.push('<option value="' + i + '"' +
                (i === month ? ' selected': '') +
                ((isMinYear && i < opts.minMonth) || (isMaxYear && i > opts.maxMonth) ? 'disabled' : '') + '>' +
                opts.i18n.months[i] + '</option>');
        }
        html += '<div class="pika-label">' + opts.i18n.months[month] + '<select class="pika-select pika-select-month">' + arr.join('') + '</select></div>';

        if (isArray(opts.yearRange)) {
            i = opts.yearRange[0];
            j = opts.yearRange[1] + 1;
        } else {
            i = year - opts.yearRange;
            j = 1 + year + opts.yearRange;
        }

        for (arr = []; i < j && i <= opts.maxYear; i++) {
            if (i >= opts.minYear) {
                arr.push('<option value="' + i + '"' + (i === year ? ' selected': '') + '>' + (i) + '</option>');
            }
        }
        html += '<div class="pika-label">' + year + '<select class="pika-select pika-select-year">' + arr.join('') + '</select></div>';

        if (isMinYear && (month === 0 || opts.minMonth >= month)) {
            prev = false;
        }

        if (isMaxYear && (month === 11 || opts.maxMonth <= month)) {
            next = false;
        }

        html += '<button class="pika-prev' + (prev ? '' : ' is-disabled') + '" type="button">' + opts.i18n.previousMonth + '</button>';
        html += '<button class="pika-next' + (next ? '' : ' is-disabled') + '" type="button">' + opts.i18n.nextMonth + '</button>';

        return html += '</div>';
    },

    renderTable = function(opts, data)
    {
        return '<table cellpadding="0" cellspacing="0" class="pika-table">' + renderHead(opts) + renderBody(data) + '</table>';
    },


    /**
     * Pikaday constructor
     */
    Pikaday = function(options)
    {
        var self = this,
            opts = self.config(options);

        self._onMouseDown = function(e)
        {
            if (!self._v) {
                return;
            }
            e = e || window.event;
            var target = e.target || e.srcElement;
            if (!target) {
                return;
            }

            if (!hasClass(target, 'is-disabled')) {
                if (hasClass(target, 'pika-button') && !hasClass(target, 'is-empty')) {
                    self.setDate(new Date(self._y, self._m, parseInt(target.innerHTML, 10)));
                    if (opts.bound) {
                        sto(function() {
                            self.hide();
                        }, 100);
                    }
                    return;
                }
                else if (hasClass(target, 'pika-prev')) {
                    self.prevMonth();
                }
                else if (hasClass(target, 'pika-next')) {
                    self.nextMonth();
                }
            }
            if (!hasClass(target, 'pika-select')) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    e.returnValue = false;
                    return false;
                }
            } else {
                self._c = true;
            }
        };

        self._onChange = function(e)
        {
            e = e || window.event;
            var target = e.target || e.srcElement;
            if (!target) {
                return;
            }
            if (hasClass(target, 'pika-select-month')) {
                self.gotoMonth(target.value);
            }
            else if (hasClass(target, 'pika-select-year')) {
                self.gotoYear(target.value);
            }
        };

        self._onInputChange = function(e)
        {
            var date;

            if (e.firedBy === self) {
                return;
            }
            if(hasMoment) {
                date = moment(opts.field.value, opts.format);
                date = (date && date.isValid()) ? date.toDate() : null;
            } else if(opts.field.value) {
                date = new Date(Date.parse(opts.field.value));
            } else
            	date = null;
            self.setDate(isDate(date) ? date : null);
            if (!self._v) {
                self.show();
            }
        };

        self._onInputFocus = function()
        {
            self.show();
        };

        self._onInputClick = function()
        {
            self.show();
        };

        self._onInputBlur = function()
        {
            if (!self._c) {
                self._b = sto(function() {
                    self.hide();
                }, 50);
            }
            self._c = false;
        };

        self._onClick = function(e)
        {
            e = e || window.event;
            var target = e.target || e.srcElement,
                pEl = target;
            if (!target) {
                return;
            }
            if (!hasEventListeners && hasClass(target, 'pika-select')) {
                if (!target.onchange) {
                    target.setAttribute('onchange', 'return;');
                    addEvent(target, 'change', self._onChange);
                }
            }
            do {
                if (hasClass(pEl, 'pika-single')) {
                    return;
                }
            }
            while ((pEl = pEl.parentNode));
            if (self._v && target !== opts.field) {
                self.hide();
            }
        };

        self.el = document.createElement('div');
        self.el.className = 'pika-single' + (opts.isRTL ? ' is-rtl' : '');

        addEvent(self.el, 'mousedown', self._onMouseDown, true);
        addEvent(self.el, 'change', self._onChange);

        if (opts.field) {
            if (opts.bound) {
                document.body.appendChild(self.el);
            } else {
                opts.field.parentNode.insertBefore(self.el, opts.field.nextSibling);
            }
            addEvent(opts.field, 'change', self._onInputChange);

            if (!opts.defaultDate) {
                if (hasMoment && opts.field.value) {
                    opts.defaultDate = moment(opts.field.value, opts.format).toDate();
                } else if(opts.field.value) {
                    opts.defaultDate = new Date(Date.parse(opts.field.value));
                }
                opts.setDefaultDate = true;
            }
        }

        var defDate = opts.defaultDate;

        if (isDate(defDate)) {
            if (opts.setDefaultDate) {
                self.setDate(defDate, true);
            } else {
                self.gotoDate(defDate);
            }
        } else {
            self.gotoDate(new Date());
        }

        if (opts.bound) {
            this.hide();
            self.el.className += ' is-bound';
            addEvent(opts.field, 'click', self._onInputClick);
            addEvent(opts.field, 'focus', self._onInputFocus);
            addEvent(opts.field, 'blur', self._onInputBlur);
        } else {
            this.show();
        }

    };


    /**
     * public Pikaday API
     */
    Pikaday.prototype = {


        /**
         * configure functionality
         */
        config: function(options)
        {
            if (!this._o) {
                this._o = extend({}, defaults, true);
            }

            var opts = extend(this._o, options, true);

            opts.isRTL = !!opts.isRTL;

            opts.field = (opts.field && opts.field.nodeName) ? opts.field : null;

            opts.bound = !!(opts.bound !== undefined ? opts.field && opts.bound : opts.field);

            var nom = parseInt(opts.numberOfMonths, 10) || 1;
            opts.numberOfMonths = nom > 4 ? 4 : nom;

            if (!isDate(opts.minDate)) {
                opts.minDate = false;
            }
            if (!isDate(opts.maxDate)) {
                opts.maxDate = false;
            }
            if ((opts.minDate && opts.maxDate) && opts.maxDate < opts.minDate) {
                opts.maxDate = opts.minDate = false;
            }
            if (opts.minDate) {
                setToStartOfDay(opts.minDate);
                opts.minYear  = opts.minDate.getFullYear();
                opts.minMonth = opts.minDate.getMonth();
            }
            if (opts.maxDate) {
                setToStartOfDay(opts.maxDate);
                opts.maxYear  = opts.maxDate.getFullYear();
                opts.maxMonth = opts.maxDate.getMonth();
            }

            if (isArray(opts.yearRange)) {
                var fallback = new Date().getFullYear() - 10;
                opts.yearRange[0] = parseInt(opts.yearRange[0], 10) || fallback;
                opts.yearRange[1] = parseInt(opts.yearRange[1], 10) || fallback;
            } else {
                opts.yearRange = Math.abs(parseInt(opts.yearRange, 10)) || defaults.yearRange;
                if (opts.yearRange > 100) {
                    opts.yearRange = 100;
                }
            }

            return opts;
        },

        /**
         * return a formatted string of the current selection (using Moment.js if available)
         */
        toString: function(format)
        {
            return !isDate(this._d) ? '' : hasMoment ? moment(this._d).format(format || this._o.format) : this._d.toDateString();
        },

        /**
         * return a Moment.js object of the current selection (if available)
         */
        getMoment: function()
        {
            return hasMoment ? moment(this._d) : null;
        },

        /**
         * set the current selection from a Moment.js object (if available)
         */
        setMoment: function(date)
        {
            if (hasMoment && moment.isMoment(date)) {
                this.setDate(date.toDate());
            }
        },

        /**
         * return a Date object of the current selection
         */
        getDate: function()
        {
            return isDate(this._d) ? new Date(this._d.getTime()) : null;
        },

        /**
         * set the current selection
         */
        setDate: function(date, preventOnSelect)
        {
            if (!date) {
                this._d = null;
                if (this._o.field) {
                    this._o.field.value = this.toString();
                    fireEvent(this._o.field, 'change', { firedBy: this });
                }
                return this.draw();
            }
            if (typeof date === 'string') {
                date = new Date(Date.parse(date));
            }
            if (!isDate(date) || this._d == date) {
                return;
            }

            var min = this._o.minDate,
                max = this._o.maxDate;

            if (isDate(min) && date < min) {
                date = min;
            } else if (isDate(max) && date > max) {
                date = max;
            }

            this._d = new Date(date.getTime());
            setToStartOfDay(this._d);
            this.gotoDate(this._d);

            if (this._o.field) {
                this._o.field.value = this.toString();
                fireEvent(this._o.field, 'change', { firedBy: this });
            }
            if (!preventOnSelect && typeof this._o.onSelect === 'function') {
                this._o.onSelect.call(this, this.getDate());
            }
        },

        /**
         * change view to a specific date
         */
        gotoDate: function(date)
        {
            if (!isDate(date)) {
                return;
            }
            this._y = date.getFullYear();
            this._m = date.getMonth();
            this.draw();
        },

        gotoToday: function()
        {
            this.gotoDate(new Date());
        },

        /**
         * change view to a specific month (zero-index, e.g. 0: January)
         */
        gotoMonth: function(month)
        {
            if (!isNaN( (month = parseInt(month, 10)) )) {
                this._m = month < 0 ? 0 : month > 11 ? 11 : month;
                this.draw();
            }
        },

        nextMonth: function()
        {
            if (++this._m > 11) {
                this._m = 0;
                this._y++;
            }
            this.draw();
        },

        prevMonth: function()
        {
            if (--this._m < 0) {
                this._m = 11;
                this._y--;
            }
            this.draw();
        },

        /**
         * change view to a specific full year (e.g. "2012")
         */
        gotoYear: function(year)
        {
            if (!isNaN(year)) {
                this._y = parseInt(year, 10);
                this.draw();
            }
        },

        /**
         * change the minDate
         */
        setMinDate: function(value)
        {
            this._o.minDate = value;
        },

        /**
         * change the maxDate
         */
        setMaxDate: function(value)
        {
            this._o.maxDate = value;
        },

        /**
         * refresh the HTML
         */
        draw: function(force)
        {
            if (!this._v && !force) {
                return;
            }
            var opts = this._o,
                minYear = opts.minYear,
                maxYear = opts.maxYear,
                minMonth = opts.minMonth,
                maxMonth = opts.maxMonth;

            if (this._y <= minYear) {
                this._y = minYear;
                if (!isNaN(minMonth) && this._m < minMonth) {
                    this._m = minMonth;
                }
            }
            if (this._y >= maxYear) {
                this._y = maxYear;
                if (!isNaN(maxMonth) && this._m > maxMonth) {
                    this._m = maxMonth;
                }
            }

            this.el.innerHTML = renderTitle(this) + this.render(this._y, this._m);

            if (opts.bound) {
                var pEl  = opts.field,
                    left = pEl.offsetLeft,
                    top  = pEl.offsetTop + pEl.offsetHeight;
                while((pEl = pEl.offsetParent)) {
                    left += pEl.offsetLeft;
                    top  += pEl.offsetTop;
                }
                this.el.style.cssText = 'position:absolute;left:' + left + 'px;top:' + top + 'px;';
                sto(function() {
                    opts.field.focus();
                }, 1);
            }

            if (typeof this._o.onDraw === 'function') {
                var self = this;
                sto(function() {
                    self._o.onDraw.call(self);
                }, 0);
            }
        },

        /**
         * render HTML for a particular month
         */
        render: function(year, month)
        {
            var opts   = this._o,
                now    = new Date(),
                days   = getDaysInMonth(year, month),
                before = new Date(year, month, 1).getDay(),
                data   = [],
                row    = [];
            setToStartOfDay(now);
            if (opts.firstDay > 0) {
                before -= opts.firstDay;
                if (before < 0) {
                    before += 7;
                }
            }
            var cells = days + before,
                after = cells;
            while(after > 7) {
                after -= 7;
            }
            cells += 7 - after;
            for (var i = 0, r = 0; i < cells; i++)
            {
                var day = new Date(year, month, 1 + (i - before)),
                    isDisabled = (opts.minDate && day < opts.minDate) || (opts.maxDate && day > opts.maxDate),
                    isSelected = isDate(this._d) ? compareDates(day, this._d) : false,
                    isToday = compareDates(day, now),
                    isEmpty = i < before || i >= (days + before);

                row.push(renderDay(1 + (i - before), isSelected, isToday, isDisabled, isEmpty));

                if (++r === 7) {
                    data.push(renderRow(row, opts.isRTL));
                    row = [];
                    r = 0;
                }
            }
            return renderTable(opts, data);
        },

        isVisible: function()
        {
            return this._v;
        },

        show: function()
        {
            if (!this._v) {
                if (this._o.bound) {
                    addEvent(document, 'click', this._onClick);
                }
                removeClass(this.el, 'is-hidden');
                this._v = true;
                this.draw();
                if (typeof this._o.onOpen === 'function') {
                    this._o.onOpen.call(this);
                }
            }
        },

        hide: function()
        {
            var v = this._v;
            if (v !== false) {
                if (this._o.bound) {
                    removeEvent(document, 'click', this._onClick);
                }
                this.el.style.cssText = '';
                addClass(this.el, 'is-hidden');
                this._v = false;
                if (v !== undefined && typeof this._o.onClose === 'function') {
                    this._o.onClose.call(this);
                }
            }
        },

        /**
         * GAME OVER
         */
        destroy: function()
        {
            this.hide();
            removeEvent(this.el, 'mousedown', this._onMouseDown, true);
            removeEvent(this.el, 'change', this._onChange);
            if (this._o.field) {
                removeEvent(this._o.field, 'change', this._onInputChange);
                if (this._o.bound) {
                    removeEvent(this._o.field, 'click', this._onInputClick);
                    removeEvent(this._o.field, 'focus', this._onInputFocus);
                    removeEvent(this._o.field, 'blur', this._onInputBlur);
                }
            }
            if (this.el.parentNode) {
                this.el.parentNode.removeChild(this.el);
            }
        }

    };

    return Pikaday;

}));

(function() {
	//This was copied from http://stevenlevithan.com/assets/misc/date.format.js
	//and a complete listing of formats can be found @ http://blog.stevenlevithan.com/archives/date-time-format
	/*
		Date Format 1.1
		(c) 2007 Steven Levithan <stevenlevithan.com>
		MIT license
		With code by Scott Trenda (Z and o flags, and enhanced brevity)
	*/

	/*** dateFormat
		Accepts a date, a mask, or a date and a mask.
		Returns a formatted version of the given date.
		The date defaults to the current date/time.
		The mask defaults ``"ddd mmm d yyyy HH:MM:ss"``.
	*/
	var dateFormat = function () {
		var	token        = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloZ]|"[^"]*"|'[^']*'/g,
			timezone     = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
			timezoneClip = /[^-+\dA-Z]/g,
			pad = function (value, length) {
				value = String(value);
				length = parseInt(length) || 2;
				while (value.length < length)
					value = "0" + value;
				return value;
			};

		// Regexes and supporting functions are cached through closure
		return function (date, mask) {
			// Treat the first argument as a mask if it doesn't contain any numbers
			if (
				arguments.length == 1 &&
				(typeof date == "string" || date instanceof String) &&
				!/\d/.test(date)
			) {
				mask = date;
				date = undefined;
			}

			date = date ? new Date(date) : new Date();
			if (isNaN(date))
				throw "invalid date";

			var dF = dateFormat;
			mask   = String(dF.masks[mask] || mask || dF.masks["default"]);

			var	d = date.getDate(),
				D = date.getDay(),
				m = date.getMonth(),
				y = date.getFullYear(),
				H = date.getHours(),
				M = date.getMinutes(),
				s = date.getSeconds(),
				L = date.getMilliseconds(),
				o = date.getTimezoneOffset(),
				flags = {
					d:    d,
					dd:   pad(d),
					ddd:  dF.i18n.dayNames[D],
					dddd: dF.i18n.dayNames[D + 7],
					m:    m + 1,
					mm:   pad(m + 1),
					mmm:  dF.i18n.monthNames[m],
					mmmm: dF.i18n.monthNames[m + 12],
					yy:   String(y).slice(2),
					yyyy: y,
					h:    H % 12 || 12,
					hh:   pad(H % 12 || 12),
					H:    H,
					HH:   pad(H),
					M:    M,
					MM:   pad(M),
					s:    s,
					ss:   pad(s),
					l:    pad(L, 3),
					L:    pad(L > 99 ? Math.round(L / 10) : L),
					t:    H < 12 ? "a"  : "p",
					tt:   H < 12 ? "am" : "pm",
					T:    H < 12 ? "A"  : "P",
					TT:   H < 12 ? "AM" : "PM",
					Z:    (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
					o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4)
				};

			return mask.replace(token, function ($0) {
				return ($0 in flags) ? flags[$0] : $0.slice(1, $0.length - 1);
			});
		};
	}();

	//Some common format strings
	dateFormat.masks = {
		"default":       "ddd mmm d yyyy HH:MM:ss",
		shortDate:       "m/d/yy",
		mediumDate:      "mmm d, yyyy",
		longDate:        "mmmm d, yyyy",
		fullDate:        "dddd, mmmm d, yyyy",
		shortTime:       "h:MM TT",
		mediumTime:      "h:MM:ss TT",
		longTime:        "h:MM:ss TT Z",
		isoDate:         "yyyy-mm-dd",
		isoTime:         "HH:MM:ss",
		isoDateTime:     "yyyy-mm-dd'T'HH:MM:ss",
		isoFullDateTime: "yyyy-mm-dd'T'HH:MM:ss.lo"
	};

	//Internationalization strings
	dateFormat.i18n = {
		dayNames: Locale.get("Date.days_abbr").invoke("capitalize").append(Locale.get("Date.days")),
		monthNames: Locale.get("Date.months_abbr").invoke("capitalize").append(Locale.get("Date.months"))
	};

	//For convenience...
	Date.prototype.format = function (mask) {
		return dateFormat(this, mask);
	}

	if(window.Pikaday)
		Pikaday.prototype.toString = function(format) {
			return typeOf(this._d) != 'date' ? '' : dateFormat(this._d, format || this._o.format);
		};
	})();


/*
Script: Reflection.js
	
	License: MIT-style license.
	Copyright: Copyright (c) 2007 Thierry bela <bntfr at yahoo dot fr>

	License:
		MIT-style license.

	Authors:
		Thierry Bela
	Version:
		1.1

*/

/**
 * reflection.js v2.0
 * http://cow.neondragon.net/stuff/reflection/
 * Freely distributable under MIT-style license.
 */
 
!function ($) {
 
	this.Reflection = {
	
		options: {
			opacity : .5,
			height : .5,
			width: .5
		},
		
		add: function(image, options) {
			this.remove($(image));
			
			options = Object.merge(this.options, options);
			
			try {
				
				var d = new Element('div'), p = image,
				newClasses = p.removeClass('reflected').className,
				reflectionHeight = Math.floor(p.height*options['height']), 
				divHeight = Math.floor(p.height*(1+options['height'])),
				reflectionWidth = p.width;
				
				if (Browser.ie && Browser.version < 9) {
					/* Fix hyperlinks */
					//if(p.parentNode.tagName == 'A') d = new Element('a', {href: p.parentNode.href}).inject(p.parentNode, 'after');
						
					/* Copy original image's classes & styles to div */
					if(newClasses) d.className = newClasses;
					p.className = 'reflected';
					
					d.style.cssText = p.style.cssText;
					p.style.cssText = 'vertical-align: bottom';
				
					var reflection = new Element('img', {src: p.src, 
							width: reflectionWidth, 
							height: p.height
						});
						
					reflection.style.display = 'block';
					
					reflection.style.marginBottom = -(p.height-reflectionHeight)+'px';
					reflection.style.filter = 'flipv progid:DXImageTransform.Microsoft.Alpha(opacity='+(options['opacity']*100)+', style=1, finishOpacity=0, startx=0, starty=0, finishx=0, finishy='+(options['height']*100)+')';
					
					d.setStyles({height: divHeight, width: reflectionWidth});
					p.parentNode.replaceChild(d, p);
					
					d.appendChild(p);
					d.appendChild(reflection)
					
				} else {
				
					var canvas = new Element('canvas');
					
					if (canvas.getContext) {
						/* Copy original image's classes & styles to div */
						if(newClasses) d.className = newClasses;
						p.className = 'reflected';
						
						d.style.cssText = p.style.cssText;
						p.style.cssText = 'vertical-align: bottom';
				
						var context = canvas.getContext("2d");
					
						canvas.setProperties({height: reflectionHeight, width: reflectionWidth});
						
						d.setStyles({height: divHeight,width: reflectionWidth});
						
						p.parentNode.replaceChild(d, p);
						
						d.appendChild(p);
						d.appendChild(canvas);
						
						context.save();
						
						context.translate(0,image.height-1);
						context.scale(1,-1);
						
						context.drawImage(image, 0, 0, reflectionWidth, image.height);
		
						context.restore();
						
						context.globalCompositeOperation = "destination-out";
						var gradient = context.createLinearGradient(0, 0, 0, reflectionHeight);
						
						gradient.addColorStop(1, "rgba(255, 255, 255, 1.0)");
						gradient.addColorStop(0, "rgba(255, 255, 255, "+(1-options['opacity'])+")");
			
						context.fillStyle = gradient;
						context.rect(0, 0, reflectionWidth, reflectionHeight*2);
						context.fill()
					}
				}
			}
			
			catch (e) { }
		},
		
		remove : function(image) {
			if (image.className == "reflected") {
				image.className = image.parentNode.className;
				image.parentNode.parentNode.replaceChild(image, image.parentNode);
			}
		}
	}

 }(document.id);
 
 /*
 Script: PeriodicalExecuter.js
 	port of the Prototype.js timer to Mootools

 	License: MIT-style license.
 	Copyright: Copyright (c) 2007 Thierry bela <bntfr at yahoo dot fr>

 	License:
 		MIT-style license.

 	Authors:
 		Thierry Bela

 	TODO: possibility to stop the timer when the window is idle ?
 */			
 	var PeriodicalExecuter = new Class({
 		// name: 'PeriodicalExecuter',
 		initialize: function(callback, frequency) {
 		
 			this.callback = callback;
 			this.frequency = frequency;
 			this.currentlyExecuting = false;

 			this.registerCallback()
 		},

 		registerCallback: function() {
 		
 			this.stop();
 			this.timer = setInterval(this.onTimerEvent.bind(this), this.frequency * 1000);
 			return this
 		},

 		execute: function() {
 		
 			this.callback(this);
 			return this
 		},

 		stop: function() {
 		
 			if (!this.timer) return this;
 			clearInterval(this.timer);
 			this.timer = null;
 			return this
 		},

 		onTimerEvent: function() {
 		
 			if (!this.currentlyExecuting) {
 			
 				try {
 				
 					this.currentlyExecuting = true;
 					this.execute();
 				} finally {
 				
 					this.currentlyExecuting = false;
 				}
 			}
 				
 			return this
 		}
 	});

 	
/*
---
script: Carousel.js
license: MIT-style license.
description: Carousel - Extensible mootools carousel with dynamic elements addition/removal.
copyright: Copyright (c) 2010 Thierry Bela
authors: [Thierry Bela]

requires: 
  core:1.3: 
  - Class.Extras
  - Element.Event
  - Element.Style
  - Element.Dimensions
  - Array
  - Type
provides: [Carousel, Carousel.plugins.Move]
...
*/

!function (context, $, undefined) {

"use strict";

function style(el, style) {

	var mrg = el.getStyle(style);
	
	return mrg == 'auto' ? 0 : mrg.toInt() 
}

context.Carousel = new Class({

		Implements: [Options, Events],
		options: {
		
		/*
			circular: false,
			onChange: function (index) {
			
			},
			previous: element1,
			next: element2,
			container: undefined,
			selector: '',
			tabs: [],
		*/
			activeClass: '',
			inactiveClass: '',
			link: 'cancel',
			mode: 'horizontal',
			animation: 'Move',
			scroll: 4,
			distance: 1,
			fx: {
			
				link: 'cancel',
				transition: 'sine:out',
				duration: 500
			}
		},
		current: 0,
		plugins: {},
		initialize: function (options) {
		
			this.addEvents({
				change: function (current) { 
				
					if(this.tabs[this.current]) this.tabs[this.current].addClass(this.options.inactiveClass).removeClass(this.options.activeClass)
					if(this.tabs[current]) this.tabs[current].addClass(this.options.activeClass).removeClass(this.options.inactiveClass);
					
				},
				complete: function (current, selected) { 
				
					this.current = current;
					this.selected = selected
					this.running = false 
				}
			}).setOptions(options);
			
			['previous', 'next'].each(function (fn) {
				var els = $$(this.options[fn]);
				if(els) els.addEvent('click', function (e) {
					e.stop();
					this[fn]();					
				}.bind(this));				
			}, this);
			
			var current = options.current || 0,
				events = this.events = {

						click: function(e) {

							e.stop();
							
							var target = e.target,
								index = this.tabs.indexOf(target);

							while(target && index == -1) {

								target = target.parentNode;
								index = this.tabs.indexOf(target)
							}
							
							if(index == -1) return;
							this.move(index)

						}.bind(this)
					};
					
			this.tabs = $$(options.tabs).addEvents(events);
			this.elements = $(options.container).getChildren(options.selector);
			this.anim = new this.plugins[this.options.animation](this.elements, this.options, this).addEvents({change: function () { this.fireEvent('change', Array.slice(arguments)) }.bind(this), complete: function () { this.fireEvent('complete', Array.slice(arguments)) }.bind(this)});
			
			this.move(current || 0);
			
			if(Browser.unknown) { // IE11 is this
				var maxHeight = 0;
				var extraHeight = 0;
				this.elements.each(function(el) {
					var h = el.getSize().y;
					if(el.hasClass("item")) {
						if(h > maxHeight)
							maxHeight = h;
					} else
						extraHeight += h;
				});
				if(maxHeight + extraHeight > 0)
					$(options.container).setStyle("height", maxHeight + extraHeight);
			}
		},
		
		isVisible: function (index) {
		
			if(Type.isElement(index)) index = this.elements.indexOf($(index));
			
			var length = this.elements.length,
				current = this.current,
				scroll = this.options.scroll;
			
			if(current <= index && index < current + scroll) return true;
			if(this.options.circular)  while(scroll--) if((scroll + current)  % length == index) return true;
			
			return false
		},
		
		first: function () { return this.current },
		
		previous: function (direction) { return this.move(this.current - this.options.distance, direction) },
		
		next: function (direction) { return this.move(this.current + this.options.distance, direction) },
	
		add: function (panel, tab, index) {

			panel = $(panel);
			tab = $(tab);

			if(tab) tab.addEvents(this.events);

			if(this.elements.indexOf(panel) != -1) return this;

			if(index == undefined) index = this.elements.length;
			index = Math.min(index, this.elements.length);
			
			switch(index) {

				case 0:
						if(this.elements.length > 0) {

							this.elements.unshift(panel.inject(this.elements[0], 'before'));
							if(tab) this.tabs.unshift(tab.inject(this.tabs[0], 'before'));
						}

						else {
						
							this.elements.push(panel.inject(this.options.container));
							if(tab) this.tabs.push(tab);
						}

						break;
				default:
						this.elements.splice(index, 0, panel.inject(this.elements[index - 1], 'after'));
						if(tab) this.tabs.splice(index, 0, tab.inject(this.tabs[index - 1], 'after'));
						break;
			}
			
			if(this.anim.add) this.anim.add(panel);
			this.current = this.elements.indexOf(this.selected);

			return this
		},

		remove: function (index) {

			var panel = this.elements[index],
				tab = this.tabs[index];
				
			//
			if(panel == undefined) return undefined;

			this.elements.splice(index, 1);
			panel.dispose();

			if(tab) {

				tab.removeEvents(this.events).dispose();
				this.tabs.splice(index, 1);
			}

			if(this.anim.remove) this.anim.remove(panel, index);
			
			var current = this.elements.indexOf(this.selected);
			
			if((current == -1 || current != this.current) && this.elements.length > 0) {
			
				current = Math.max(index - 1, 0);
				this.move(current)
			}

			return {panel: panel, tab: tab}
		},

		move: function (index, direction) {
		
			if(this.running) {
			
				switch(this.options.link) {
				
					case 'cancel':
								this.anim.cancel();
								break;
					case 'chain':
								break;
					case 'ignore':
							return this;
				}
			}
			
			var elements = this.elements,
				current = this.current,
				length = elements.length,
				scroll = this.options.scroll;
			
			if(Type.isElement(index)) index = elements.indexOf(index);
			
			if(!this.options.circular) {	        
				if(index > length - scroll) index = length - scroll;
                index = Math.max(index, 0);
                if(direction == undefined) {  				
    				//detect direction. inspired by moostack
    				var forward = current < index ? index - current : elements.length - current + index,
    					backward = current > index ? current - index : current + elements.length - index; 				
    				direction = Math.abs(forward) <= Math.abs(backward) ? 1 : -1
    			}	
			} else {
				if(direction == undefined) 
					direction = (current > index ? 1 : current < index ? -1 : 0);
				if(index < 0) index += length;
				index %= Math.max(length, 1);				
			}			
		
			if(index < 0 || length <= scroll || index >= length) return this;

			this.anim.move(index, direction);
			return this;
		}
	});
	
context.Carousel.prototype.plugins.Move = new Class({
	
		Implements: [Events, Chain],
		initialize: function (elements, options) {
		
			var up = this.up = options.mode == 'vertical',
				parent;

			if(elements.length > 0) {
			
				parent = elements[0].getParent();
				
				parent.setStyles({height: parent.getStyle('height'), position: 'relative', overflow: 'hidden'}).getStyle('padding' + (this.up ? 'Top' : 'Left'));
				
				this.padding = style(parent, up ? 'paddingTop' : 'paddingLeft');
				this.pad = style(parent, 'paddingLeft');
			}
			
			this.options = options;
			this.elements = elements;
			this.property = 'offset' + (up ? 'Top' : 'Left');
			this.margin = up ? ['marginTop', 'marginBottom'] : ['marginLeft', 'marginRight'];
		
			elements.each(this.addElement.bind(this));
			this.direction = 1;
			this.current = elements[0];
			this.reset();
		},
		
		cancel: function () { this.fx.cancel() },
		
		reset: function () {
			this.fx = new Fx.Elements(this.elements, this.options.fx).addEvents({complete: function () {
				this.current = this.elements[this.index];
				this.fireEvent('complete', [this.index, this.current]) 			
			}.bind(this)});
			this.reorder(this.elements.indexOf(this.current), this.direction);			
			return this;
		},
		
		addElement: function (el) {		
			el.setStyles({display: 'block', position: 'absolute'});
			if(this.up)
				el.setStyle('height', el.getStyle('height'));
			else
				el.setStyle('width', el.getStyle('width'));
			
			if(isNaN(this.pad)) {			
				var parent = el.getParent();				
				this.padding = style(parent, this.up ? 'paddingTop' : 'paddingLeft');
				this.pad = style(parent, 'paddingLeft');
			}
			
			return this;
		},
		
		add: function (el) { 		
			this.addElement(el).reset() 
		},
		
		remove: function () { 		
			this.fx.cancel();
			this.reset();
		},
		
		reorder: function (offset, direction) {
		
			var options = this.options,
				panels = this.elements,
				panel,
				prev,
				pos,
				ini,
				pad = this.pad,
				index,
				length = panels.length,
				i = length + 1,
				horizontal = options.mode == 'horizontal',
				side = horizontal ? 'offsetWidth' : 'offsetHeight';
			
			ini = pos = this.padding;
			
			//rtl
			if(direction == -1) {
			
				while(i > options.scroll - 1 && i--) {
			
					index = (i + offset + length) % length;
					prev = panel;
					panel = panels[index];
					
					if(prev) pos -= style(prev, this.margin[0]);
					
					if(horizontal) panel.setStyle('left', pos);
					else panel.setStyles({left: pad, top: pos});
					pos -= (panel[side] + style(panel, this.margin[1]));
				}
				
				pos = ini + panel[side] + style(panel, this.margin[0]);
				
				for(i = 1; i < options.scroll; i++) {
			
					index = (i + offset + length) % length;
					
					prev = panel;
					panel = panels[index];			
					
					if(prev) pos += style(prev, this.margin[1]);
					if(horizontal) panel.setStyle('left', pos);
					else panel.setStyles({left: pad, top: pos});
					pos += panel[side] + style(panel, this.margin[0]);		
				}
				
				//ltr
			} else if(direction == 1) for(i = 0; i < length; i++) {
			
				index = (i + offset + length) % length;
				prev = panel;
				panel = panels[index];				
				
				if(horizontal) panel.setStyle('left', pos);
				else panel.setStyles({left: pad, top: pos});
				pos += panel[side] + style(panel, this.margin[0]);
				if(prev) pos += style(prev, this.margin[1]);
			}
			
			return this;
		},
		
		move: function (current, direction) {
			if(this._running) {
				this.chain(this.move.pass([current,direction], this));
				return;
			}
			this._running = true;
			try {
				if(this.options.circular) {
					//this.reorder(this.elements.indexOf(this.current), direction);
					var former = this.index || 0, pos = this.padding, dim, measure, total = 0, props = {}, n;
					if(this.options.mode == 'horizontal') {
						dim = 'left';
						measure = 'offsetWidth';
					} else {
						dim = 'top';
						measure = 'offsetHeight';
					}
					if(direction != 0 && former == current)
						former = (this.elements.length + current + direction * this.options.scroll) % this.elements.length;
					n = (this.elements.length + former - current) % this.elements.length;
					if(direction == -1) { //rtl
						//calc move total
						for(var i = 0; i < n; i++) {
							var index = (former + i) % this.elements.length;
							props[index] = {};
							props[index][dim] = [total, null];
							total += this.elements[index][measure] + style(this.elements[index], this.margin[1]);
						}
						for(var i = 0; i < n; i++) {
							var index = (former + i) % this.elements.length;
							props[index][dim][1] = props[index][dim][0] - total;
						}
						for(var i = 0; i < this.options.scroll; i++) {
							var index = (current + i) % this.elements.length;
							if(!props[index]) {
								props[index] = {};
								props[index][dim] = [pos + total, pos];	
								pos += this.elements[index][measure] + style(this.elements[index], this.margin[1]);
							}
						}
					} else if(direction == 1) { //ltr
						//calc move total
						for(var i = 1; i <= n; i++) {
							var index = (this.elements.length + former - i) % this.elements.length;
							total += this.elements[index][measure] + style(this.elements[index], this.margin[1]);
							props[index] = {};
							props[index][dim] = [-total, null];
						}
						for(var i = 1; i <= n; i++) {
							var index = (this.elements.length + former - i) % this.elements.length;
							props[index][dim][1] = props[index][dim][0] + total;
						}
						for(var i = 0; i < this.options.scroll; i++) {
							var index = (former + i) % this.elements.length;
							if(!props[index]) {
								props[index] = {};
								props[index][dim] = [pos, pos + total];	
								pos += this.elements[index][measure] + style(this.elements[index], this.margin[1]);
							}
						}
					} else {
						return;
					}	
					this.fireEvent('change', [current, element]).fx.cancel().start(props);
					this.index = current;
					this.direction = direction;
				} else {
					var up = this.up,
					property = this.property,
					offset,
					element = this.elements[current];					
					this.index = current;
					this.direction = direction;
					offset = element[property] - this.padding;
					this.fireEvent('change', [current, element]).fx.cancel().start(Object.map(this.elements, function (el, index) { if(!isNaN(index)) return up ? {top: el[property] - offset} : {left: el[property] - offset} }));
				}
			} finally {
				this._running = false;
				this.callChain();
			}
		}
	})
	
}(this, document.id);

/*
---
script: Carousel.Extra.js
license: MIT-style license.
description: Tab.Extra - Autosliding carousel.
copyright: Copyright (c) 2010 Thierry Bela
authors: [Thierry Bela]

requires: 
  core:1.2.3: 
  - Class.Extras
  - Element.Event
  - Element.Style
  - Element.Dimensions
  - Array
provides: [Carousel]
...
*/

!function (context) {

"use strict";

context.Carousel.Extra = new Class({

	/*
	
		options: {
		
			interval: 10, //interval between 2 executions in seconds
			delay: 10, //delay between the moment a tab is clicked and the auto slide is restarted
			reverse: true, //move backward
			autostart: true
		},
		*/
	
		Extends: context.Carousel,
		Binds: ['update', 'start', 'stop'],
		initialize: function(options) {

			this.parent(Object.append({interval: 10, delay: 10, autostart: true}, options));
			
			var active,
				events = this.events = {

						click: function(e) {

							e.stop();
							
							active = this.active;

							if(active) this.stop();

							var target = e.event.target,
								index = this.tabs.indexOf(target);

							while(target && index == -1) {

								target = target.parentNode;
								index = this.tabs.indexOf(target)
							}
							
							if(index == -1) return;
							
							this.move(index);
							if(active) this.start.delay(this.options.delay * 1000)

						}.bind(this)
					};
					
			this.tabs.each(function (tab) { tab.removeEvents(this.events).addEvents(events) }, this);
			
			this.events = events;
			
			//handle click on tab. wait 10 seconds before we go
			['previous', 'next'].each(function (fn) {
			
				if($(this.options[fn])) $(this.options[fn]).addEvent('click', function (e) {
			
					e.stop();
					
					active = this.active;
					
					if(active) {
					
						this.stop().start.delay(this.options.delay * 1000);
						this.active = active
					}

				}.bind(this))
			}, this);
		
			this.timer = new PeriodicalExecuter(this.update, this.options.interval).stop();
			this[this.options.autostart ? 'start' : 'stop']()
		},
		
		update: function () { return this[this.options.reverse ? 'previous' : 'next']() },
		
		start: function () {
		
			this.timer.registerCallback();
			this.active = true;
			return this
		},
		
		stop: function() { 
		
			this.timer.stop();
			this.active = false;
			return this
		},
		
		toggle: function() { 
		
			return this[this.active ? 'stop' : 'start']()
		}

	})

}(this);
		
