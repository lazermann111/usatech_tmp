<%@page import="simple.servlet.RequestUtils"%>
<%
RequestUtils.setAttribute(request, "subtitle", "Prepaid and Loyalty Program", "request"); 
RequestUtils.setAttribute(request, "wrapNarrow", true, "request"); 
%>
<jsp:include page="/header.jsp" />
<style type="text/css">
@font-face {
  font-family: 'TradeGothic';
  font-weight: bold;
  src: url('TradeGothicBoldCondensed20.eot?') format('embedded-opentype'), /* IE6-IE8 */ 
       url('TradeGothicBoldCondensed20.woff') format('woff'), /* Modern Browsers */
       url('TradeGothicBoldCondensed20.ttf') format('truetype'), /* Safari, Android, iOS */
       url('TradeGothicBoldCondensed20.svg#svgFontName') format('svg'); /* Legacy iOS */
}
.get-ready {
    display: inline-block;
    font-family: 'TradeGothic',serif;
    font-size: 34.7px;
    color: #5891CF;
    font-weight: bold;
}
.gray-line {
    display: inline-block;
    width: 30px;
    height: 5px;
    vertical-align: middle;
    border-width: 5px;
}
.more-enumeration {
    font-family: 'TradeGothic',serif;
    font-size: 22.1px;
    color: #5891CF;
    font-weight: bold;
    text-align: center;
}
/*  ----  VENDOR PAGE STYLES  ----  */
        
.bluelead-box {
  background-color: #5990cc;
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  border-radius: 10px;
  padding: 5px 2em;
  margin-top: 1.4em;
  margin-bottom: 2.4em;
  }
  
.bluelead-box p {
  font-size: 20px;
  font-size: 2.0rem;
  font-weight: 300;
  line-height: 1.5;
  margin-bottom: 1.4em;
  color: #ffffff;
  text-align: left;
  }
  
.work-row {
  margin-top: 2em;
  }
  
.didyouknow-row {
  margin-bottom: 2.4em;
  }
  
.didyouknow-box {
  background-color: #f5f5f5;
  margin-top: 1.4em;
  padding: 0 1.2em 1.2em;
  }
  
.didyouknow-box h5 {
  background-color: #e74b34;
  margin: -15px auto 15px auto;
  text-align: center;
  font-weight: 700;
  color: #ffffff;
  width: 84%;
  }
  
.didyouknow-box p {
  padding: 0;
  margin: 1.2em 0 0 0;
  }
  
.firstcharacter {
  float: left;
  color: #5990cc;
  font-size: 36px;
  font-size: 3.6rem;
  line-height: 1;
  padding-top: 4px;
  padding-right: 8px;
  padding-left: 3px;
  font-family: Georgia;
  font-weight: bold;
  }
  
.specialdidyouknow {
   float: left;
   color: #5991cc;
   font-size: 12px;
   font-size: 1.2rem;
   width: 50%;
   font-family: Georgia, sans-serif;
   font-weight: bold;
   padding: 4px 8px 0 3px;
   line-height: 1.6;
   }
   
.specialthree {
  float:left;
  font-size: 4rem;
  padding: 0 4px 0 0;
  line-height: .6;
  }
.package-box {
  background-color: #f5f5f5;
  margin-top: 2.4em;
  padding: 0 1.2em 1.2em;
  }
  
.package-box h4 {
  margin: -18px 0 18px 0;
  background-color: #5990cc;
  text-align: center;
  color: #ffffff;
  text-transform: uppercase;
  padding: 5px;
  }
  
.package-box ul {
  font-size: 14px;
  font-size: 1.4rem;
  }
  
.package-box p {
  background-color: #ffffff;
  padding: 1.2em;
  margin: 1.4em 0 0 0;
  }
  
.calltoaction-box {
   background-color: #e74b34;
   padding: 1.2em;
   color: #ffffff;
   font-size: 18px;
   font-size: 1.8rem;
   text-align: center;
   font-weight: 700;
   -webkit-border-radius: 10px;
   -moz-border-radius: 10px;
   border-radius: 10px;
   }
</style>


<div class="billboard"> 
  <img src="/images/get-ready-to-get-more.png" alt="get ready to get more" />
  
    <div class="bluelead-box">
    <img src="/images/icons.png" alt="various more program icons" style="width: 100%; max-width: 1200px; margin: 30px auto 10px;">
        <p style="text-align: center;">From vending machines to dining services, micro markets to catering, processing payments through USA Technologies' ePort Connect Service helps you get more from your business. </p>
   
    </div>  <!-- /blueleadbox -->
 <div >
 <p class="lead">
Now <span class="more-text">more</span> is even <span class="more-text">more</span> rewarding than before.  <span class="more-text">more</span> was built to enable operators to connect with customers and gain a competitive advantage with rewards and loyalty.  Now <span class="more-text">more</span> gives vending operators the ability to extend that benefit across their entire business.  Process all of your cashless payments through USA Technologies to improve your economics, increase customer loyalty and account retention, and gain valuable insights that help your business grow. 
 </p>

 </div>
</div> <!-- /billboard -->


  
<img src="/images/how-does-more-work.png" alt="how does more work?" />
<div class="row-fluid work-row">
  <div class="span4 text-center">
    <img src="/images/icon-morecards60x50.png" />
    <p>Customers activate the <span class="more-text">more</span> card at <a href="http://getmore.usatech.com">getmore.usatech.com</a>, which can then be used to earn cash back rewards for products or services purchased at any of the locations you identify via USALive.</p>
  </div>
  <div class="span4 text-center">
    <img src="/images/icon-salecoupon60x50.png" />
    <p>Your business can then push special offers and promotions directly to your customers' email and <span class="more-text">more</span> accounts to drive more  <strong>frequent, repeat sales</strong>.</p>
  </div>
  
  <div class="span4 text-center">
    <img src="/images/icon-people60x50.png" />
    <p>You'll build meaningful customer relationships that create consumer 'stickiness' and referrals though increased communication, product information, special reward and promotions, and <span class="more-text">more</span>.</p>
  </div>
  
</div>  <!-- /row-fluid workrow -->  
  
<p class="calltoaction-box">
Contact your USAT Sales rep for pricing and additional information at 800.633.0340 or <a href="mailto: info@usatech.com" style="color: #ffffff;">info@usatech.com</a></p>
<hr/>
<jsp:include page="/footer.jsp"/>
