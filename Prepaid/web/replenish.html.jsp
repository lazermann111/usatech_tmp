<%@page import="simple.results.Results"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="com.usatech.layers.common.constants.CommonConstants"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils.CardInfo"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.lang.Holder"%>
<%@page import="com.usatech.ec2.EC2AuthResponse"%>
<%@page import="java.text.Format"%>
<%@page import="com.usatech.prepaid.web.PrepaidUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%! private static final Log log = Log.getLog(); %>
<% 
//GATHER data
PrepaidUser user = (PrepaidUser) RequestUtils.getUser(request);
long cardId = RequestUtils.getAttribute(request, "cardId", Long.class, true);
CardInfo cardInfo = PrepaidUtils.checkCardPermission(user, cardId);
if(cardInfo == null) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-not-authorized", "You are not authorized to do this. Please try logging in again.");
    RequestUtils.redirectWithCarryOver(request, response, PrepaidUtils.getHomePage());
	return;
}

int replenishCountMax = 0; 	
Map<String, Object> replenCountParams = new HashMap<String, Object>(); 
Results replenCountResult = DataLayerMgr.executeQuery("GET_DAILY_REPLENISH_COUNT_MAX", replenCountParams); 
if (replenCountResult.next()){
    replenishCountMax = ConvertUtils.getIntSafely(replenCountResult.getFormattedValue("dailyCountMax"), 0); 
}
int replenishCount = RequestUtils.getAttribute(request, "replen_count", Integer.class, true);    		
if((replenishCount > replenishCountMax)||(replenishCount <= 0)){
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-replen-count", "Replenishment count can't be less than 1 or more than max value = "+ replenishCountMax +".");
	RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
	return;
}

RequestUtils.storeAllParamsForNextRequest(request);
Long replenishId = RequestUtils.getAttribute(request, "replenishId", Long.class, false);
String operation = RequestUtils.getAttribute(request, "operation", String.class, true);
BigDecimal amount;
BigDecimal threshhold;
if("REMOVE".equalsIgnoreCase(operation)) {
	if(replenishId == null) {
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
	    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
	    return;
	}		
    Map<String, Object> params = new HashMap<String, Object>();
	params.put("replenishId", replenishId);
    params.put("replenishType", 2);
    params.put("cardId", cardId);
    params.put("user", user);
    DataLayerMgr.executeCall("UPDATE_REPLENISH", params, true);
    boolean allowed = ConvertUtils.getBoolean(params.get("allowed"), false);
    if(!allowed) {
        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-not-users", "Your account is not registered with this card.");
        RequestUtils.redirectWithCarryOver(request, response, "cards.html");
    } else {
        RequestUtils.setAttribute(request, "amount", 0, RequestUtils.REQUEST_SCOPE);
        RequestUtils.setAttribute(request, "threshhold", 0, RequestUtils.REQUEST_SCOPE);
        RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenish-removed", "Automatic replenishment has been stopped on this prepaid account.");
        RequestUtils.redirectWithCarryOver(request, response, "cards.html?cardId=" + cardId);
    }
    return;
} 
amount = RequestUtils.getAttribute(request, "amount", BigDecimal.class, true);
if(amount.compareTo(PrepaidUtils.getMinReplenishAmount()) < 0) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-replenish-amt-too-small", "The amount you entered is too small. Please enter at least {0}.", PrepaidUtils.getMinReplenishAmount());
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    return;
} else if(amount.compareTo(PrepaidUtils.getMaxReplenishAmount()) > 0) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-replenish-amt-too-big", "The amount you entered is too much. Please enter at most {0}.", PrepaidUtils.getMaxReplenishAmount());
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    return;
}
if("IMMEDIATE".equalsIgnoreCase(operation)) {
	if(replenishId == null) {
        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-missing-input", "You did not provide all required fields. Please try again.");
        RequestUtils.redirectWithCarryOver(request, response, "cards.html?cardId=" + cardId);
        return;
    }   
	Map<String, Object> params = new HashMap<String, Object>();
    params.put("replenishId", replenishId);
    params.put("cardId", cardId);
    params.put("user", user);
    params.put("replenishAmount", amount);
    DataLayerMgr.executeCall("REQUEST_REPLENISH", params, true);
    String cardNum = ConvertUtils.getString(params.get("cardNum"), true);
    RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenish-requested", "We have scheduled a transaction to pull {0,CURRENCY} from your credit card {1} and to add that same amount to your prepaid account. Your statements will reflect this in next few minutes", amount, cardNum);
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?cardId=" + cardId);
    return;
}
int replenishType = RequestUtils.getAttribute(request, "replenishType", Integer.class, true);
if(replenishType == 1) {
	threshhold = RequestUtils.getAttribute(request, "threshhold", BigDecimal.class, true);
	if(threshhold.compareTo(PrepaidUtils.getMinThreshholdAmount()) < 0) {
	    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-threshhold-amt-too-small", "The minimum balance you entered is too small. Please enter at least {0}.", PrepaidUtils.getMinThreshholdAmount());
	    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
	    return;
	}
} else
	threshhold = null;
//if edit or convert do update, else do replenish
if("EDIT".equalsIgnoreCase(operation)) {
	long replenConsumerAcctId=RequestUtils.getAttribute(request, "replenConsumerAcctId", Long.class, true);
	Map<String, Object> params = new HashMap<String, Object>();
	params.put("replenishId", replenishId);
    params.put("cardId", cardId);
    params.put("user", user);
    params.put("replenishType", replenishType);
    params.put("replenishAmount", amount);
    params.put("replenishThreshhold", threshhold);
    params.put("replenConsumerAcctId", replenConsumerAcctId);
    params.put("replenishCount", replenishCount);
    
    if(replenishId == null) {
    	DataLayerMgr.executeCall("ADD_REPLENISH", params, true);
    	 boolean allowed = ConvertUtils.getBoolean(params.get("allowed"), false);
    	    if(!allowed) {
    	        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-not-users", "Your account is not registered with this card.");
    	        RequestUtils.redirectWithCarryOver(request, response, "cards.html");
    	    } else {
		    	if(replenishType == 2){ 
		            RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-authonly-success", "You may now use this card to replenish your account");
		    	}
		        else {
		            RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-authonly-auto-success", "We setup automatic replenishment to occur whenever your prepaid account balance falls below {0,CURRENCY}.", threshhold);
		        }
    	    }
    }else{
    	Long existingReplenConsumerAcctId = RequestUtils.getAttribute(request, "existingReplenConsumerAcctId", Long.class, false);
    	params.put("existingReplenConsumerAcctId", existingReplenConsumerAcctId);
    	DataLayerMgr.executeCall("UPDATE_REPLENISH", params, true);
    	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenish-updated", "The replenishment settings have been updated.");
    }
    
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?cardId=" + cardId);
    return;
}
if(!"ADD".equalsIgnoreCase(operation)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-replenish-invalid-action", "The request had an invalid action. Please try again.");
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    return;
}
String replenishCardNum = null;
int replenishExpMonth = 0;
int replenishExpYear = 0;
String address1 = null;
String postal = null;
String country = null;
String cvv = null;
replenishCardNum = RequestUtils.getAttribute(request, "replenishCardNum", String.class, true);
Holder<String> replenishCardNumHolder = new Holder<String>();
if(replenishCardNum != null && replenishCardNum.startsWith(CommonConstants.USAT_IIN)) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-credit-card", "You entered an invalid credit card number. Please enter a valid one.");
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    return;
}
if(!PrepaidUtils.checkCardNum(replenishCardNum, replenishCardNumHolder, false)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-credit-card", "You entered an invalid credit card number. Please enter a valid one.");
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    return;
}
replenishCardNum = replenishCardNumHolder.getValue();
replenishExpMonth = RequestUtils.getAttribute(request, "replenishExpMonth", Integer.class, true);
replenishExpYear = RequestUtils.getAttribute(request, "replenishExpYear", Integer.class, true);
if(replenishExpMonth > 12 || replenishExpMonth < 1) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-month", "You specified an invalid month. Please enter a valid one");
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    return;
}
Calendar now = Calendar.getInstance();
int currYear = now.get(Calendar.YEAR); 
int currMonth = now.get(Calendar.MONTH) + 1;
if(replenishExpYear < currYear || (replenishExpYear == currYear && replenishExpMonth < currMonth)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-replenish-card-expired", "This card has already expired. Please use a different one.");
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    return;
}
cvv = RequestUtils.getAttribute(request, "cvv", String.class, true);
if(!PrepaidUtils.checkCvv(cvv)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-cvv", "You entered an invalid security code. Please enter a valid one");
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    return;
}
address1 = RequestUtils.getAttribute(request, "address1", String.class, true);
postal = RequestUtils.getAttribute(request, "postal", String.class, true);
country = RequestUtils.getAttribute(request, "country", String.class, false);
Holder<String> formattedPostal = new Holder<String>();
if(!GenerateUtils.checkPostal(country, postal, true, formattedPostal)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-invalid-postal", "You entered an invalid postal code for the country {1}. Please try again.", postal, country);
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    return;
}
postal = formattedPostal.getValue();
Format expFormat = ConvertUtils.getFormat("NUMBER:00");
long globalAccountId=-1;
try {
	globalAccountId = EC2ClientUtils.getGlobalAccountId(replenishCardNum);
} catch(Throwable e) {
	log.info("replenishCardNum globalAccountId not exists");
}
if(globalAccountId>0){
	Map<String, Object> params = new HashMap<String, Object>();
	params.put("globalAccountId", globalAccountId);
    params.put("user", user);
	DataLayerMgr.executeCall("GET_CREDIT_CARD_EXISTS", params, true);
	int exists = ConvertUtils.getInt(params.get("exists"));
	if(exists>0){
		 RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-credit-card-exists", "Credit card already exists. Please choose among existing cards or enter new one.");
		 RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
		 return;
	}
}
EC2AuthResponse resp = PrepaidUtils.setupReplenish(cardId, null, cardInfo.globalAccountId, cardInfo.consumerId, replenishType, amount, threshhold, replenishCardNum, expFormat.format(replenishExpYear % 100) + expFormat.format(replenishExpMonth), cvv, address1, postal, replenishCount);
//tell user
if(resp == null) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenish-updated", "The replenishment amount settings have been updated.");
    RequestUtils.redirectWithCarryOver(request, response, "cards.html?cardId=" + cardId);
    return;
}
String masked = PrepaidUtils.maskCardNumber(replenishCardNum);
switch(resp.getReturnCode()) {
    case 2: /*APPROVED*/
    	if(resp.getApprovedAmount() == 0) {
    		if(replenishType == 2)    
                RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-authonly-success", "We have verified your credit card {1}. You may now use this card to replenish your account", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked);
             else 
                RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-authonly-auto-success", "We have verified your credit card {1} and setup automatic replenishment to occur whenever your prepaid account balance falls below {2,CURRENCY}.", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked, threshhold);
    	} else {
    		if(replenishType == 2)	
   			   RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-success", "We have scheduled a transaction to charge {0,CURRENCY} to your credit card {1} and to add that same amount to your prepaid account. Your statements will reflect this in the next few minutes.", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked);
    	    else 
   	    	   RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-replenished-auto-success", "We have scheduled a transaction to charge {0,CURRENCY} to your credit card {1} and have set up automatic replenishment to occur whenever your prepaid account balance falls below {2,CURRENCY}. Your statements will reflect this in the next few minutes.", BigDecimal.valueOf(resp.getApprovedAmount(), 2), masked, threshhold);
    	}
        RequestUtils.redirectWithCarryOver(request, response, "cards.html?cardId=" + cardId);
        break;
    case 3: case 8:
    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-replenished-declined", "Your credit card {0} was declined. Please try another card.", masked);
    	RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    	break;
    case 7:
        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-replenished-no-debit", "Debit cards are not accepted. Please try another card.", masked);
        RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
        break;
    case 9: case 10: case 11:
    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-replenished-mismatch", "Either the security code or the address did not match for {0}. Please enter these again.", masked);
        RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
        break;
    case 0:
    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-replenished-failed", "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
    	RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
    	break;
    default:
    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "prepaid-card-replenished-failed", "Sorry for the inconvenience. We could not process your credit card {0} at this time. Please try again.", masked);
    	RequestUtils.redirectWithCarryOver(request, response, "cards.html?expand=edit", false, true);
        break;
}
%>
