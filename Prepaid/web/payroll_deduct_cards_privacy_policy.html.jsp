<%@page import="simple.translator.Translator"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
RequestUtils.setAttribute(request, "subtitle", "Privacy Policy", "request"); 
RequestUtils.setAttribute(request, "wrapNarrow", true, "request");
Translator translator = RequestUtils.getTranslator(request);
%>
<jsp:include page="/header.jsp"/>
<h2><%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%> Pre-Paid Account Privacy Policy</h2>

<h3>Contacting Us</h3>
<p>
If there are any questions regarding this privacy policy you may contact us
using the information below:</p>
<address>
<strong><%=translator.translate("prepaid-company-name", "USA Technologies, Inc.")%></strong><br/>
<%=translator.translate("prepaid-company-address-line1", "100 Deerfield Lane, Suite 300")%><br/>
<%=translator.translate("prepaid-company-address-line2", "Malvern, PA 19355 USA")%><br/>
Phone: <%=translator.translate("prepaid-company-phone-number", "888-561-4748")%><br/>
Email: <a href="mailto:<%=translator.translate("prepaid-company-email-address", "customersupport@usatech.com")%>"><%=translator.translate("prepaid-company-email-address", "customersupport@usatech.com")%></a><br/>
Website: <a href="<%=translator.translate("prepaid-company-website-url", "http://www.usatech.com/")%>"><%=translator.translate("prepaid-company-website-url", "http://www.usatech.com/")%></a></address>

<hr/>
<jsp:include page="/footer.jsp"/>