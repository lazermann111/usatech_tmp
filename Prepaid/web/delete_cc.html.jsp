<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.prepaid.web.PrepaidUser"%>
<%@page import="simple.servlet.ToJspSimpleServlet"%>
<%@page import="simple.servlet.RequestUtils"%>
<% 
InputForm form = (InputForm)request.getAttribute("simple.servlet.InputForm");
DataLayerMgr.executeUpdate("DELETE_CREDIT_CARD", form, true);
RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "prepaid-card-delete-cc-requested", "We have successfully delete the requested credit card for replenishment");
RequestUtils.redirectWithCarryOver(request, response, "cards.html");
%>