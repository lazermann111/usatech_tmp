import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import sun.misc.CRC16;

import com.usatech.cardnumbergenerator.cd.Luhn;
import com.usatech.cardnumbergenerator.lfsr.MIT864;


public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		String[] cards = new String[] {
				"639621012292487427", "6396210012292487425"// "371449635398431", "6011000995500000", "4788250000028291", "4055011111111111", "5473500000000014",																				// "371449635398449"
		};
		for(String card : cards)
			printCalcs(card);
		for(String card : cards)
			printCheckDigit(card);
		for(String card : cards)
			printNextCard(card);
		*/
		// printNextCards();
		printCrcCheck();
	}

	protected static void printCheck(String card) {
		boolean okay = Luhn.check(card);
		System.out.println("Card '" + card + "' is " + (okay ? "" : "NOT ") + "okay");
	}
	protected static void printCheckDigit(String card) {
		boolean okay = Luhn.check(card);
		int luhn = Luhn.generate(card.substring(0, card.length() - 1));
		boolean match = (card.charAt(card.length() - 1) == '0' + (luhn % 10));
		System.out.println("For '" + card + " (" + (okay ? "OKAY" : "FAIL") + "); CheckDigit=" + luhn + "; which " + (match ? "matches" : " DOES NOT MATCH"));
	}

	protected static void printNextCard(String card) {
		char pu = card.charAt(card.length() - 2);
		if(pu == '9')
			pu = '0';
		else
			pu++;
		int luhn = Luhn.generate(card.substring(0, card.length() - 2) + pu);
		char lc = (char)('0' + luhn);
		System.out.println("Next card after '" + card + "' is '" + card.substring(0, card.length() - 2) + pu + lc + "'");
	}

	protected static void printCalcs(String card) {
		int[] a = new int[card.length()];
		for(int i = 0; i < a.length; i++)
			a[i] = Character.getNumericValue(card.charAt(i));

		int len = a.length;
		int checksum = 0;
		int tot = 0;
		for(int i = 0; i < len; i++) {
			int n = a[i] * (1 + (i % 2));
			tot += n;
			if(n < 10)
				checksum += n;
			else
				checksum += (n - 9);
			System.out.println("Checksum=" + checksum + "; tot=" + tot);
		}
	}

	protected static void printNextCards() {
		int seed = 1;
		MIT864 lfsr = generateLfsrFromGroupId(seed);
		Map<Integer, Integer> accounts = new TreeMap<Integer, Integer>();
		int dups = 0;
		for(int i = MIN_ACCOUNT_ID; i <= MAX_ACCOUNT_ID; i++) {
			int acct = getNextLFSRValue(lfsr);
			Integer oldOrder = accounts.get(acct);
			if(oldOrder != null) {
				dups++;
				System.out.println("LFSR " + acct + " already generated in step " + oldOrder + "; dup on step " + i);
			} else
				accounts.put(acct, i);

			if((i % 1000) == 0)
				lfsr = generateLfsr(acct);
		}
		System.out.println("Found " + dups + " dups for seed " + seed + " when generating " + accounts.size() + " cards");
	}

	private static final int MIN_ACCOUNT_ID = 1;
	private static final int MAX_ACCOUNT_ID = 99999999;

	private static MIT864 generateLfsrFromGroupId(int groupId) {
		MIT864 lfsr = new MIT864(groupId);
		for(int i = 0; i < 10; i++)
			getNextLFSRValue(lfsr);
		return lfsr;
	}
	private static MIT864 generateLfsr(int lastLFSRVal) {
		MIT864 lfsr = new MIT864(lastLFSRVal);
		// roll the lfsr forward until we find the last stopping point
		for(int i = 1; i <= MAX_ACCOUNT_ID; i++) {
			if(getNextLFSRValue(lfsr) == lastLFSRVal) {
				System.out.println("Took " + i + " tries to find last lfsr val");
				return lfsr;
			}
		}
		throw new IllegalArgumentException("Could not get lfsr for last val of " + lastLFSRVal);
	}

	private static int getNextLFSRValue(MIT864 lfsr) {
		int n = -1;
		while(n < MIN_ACCOUNT_ID || n > MAX_ACCOUNT_ID)
			n = lfsr.nextInt();
		return n;
	}

	protected static void printCrcCheck() {
		Map<Integer, Set<String>> crcs = new TreeMap<Integer, Set<String>>();
		char[] value = new char[5];
		int dups = 0;
		for(int i = 0; i < 100000; i++) {
			String s = Integer.toString(i);
			if(s.length() < value.length) {
				s.getChars(0, s.length(), value, value.length - s.length());
				s = new String(value);
			}
			int crc = getCRC16(s);
			Set<String> matches = crcs.get(crc);
			if(matches == null) {
				matches = new HashSet<String>();
				crcs.put(crc, matches);
			} else {
				dups++;
			}
			matches.add(s);
		}
		System.out.println("Found " + dups + " dups and " + crcs.size() + " uniques");
	}

	protected static int getCRC16(String s) {
		CRC16 crc = new CRC16();
		for(byte b : s.getBytes())
			crc.update(b);
		return crc.value;
	}
}
