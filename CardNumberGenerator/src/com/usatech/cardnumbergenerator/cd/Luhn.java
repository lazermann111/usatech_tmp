package com.usatech.cardnumbergenerator.cd;

public class Luhn
{
	
	public static boolean check(String accountNumberWithCheckDigit)
	{
		int[] a = new int[accountNumberWithCheckDigit.length()];
		for(int i=0; i<a.length; i++)
			a[i] = Character.getNumericValue(accountNumberWithCheckDigit.charAt(i));
		
		int len = a.length;
		int checksum = 0;
		int temp;
		
		for (int i = 1; i < len; i++)
		{
			// don't include last digit
			temp = a[len - i - 1] * (1 + (i % 2));
			if (temp < 10)
				checksum += temp;
			else
				checksum += temp - 9;
		}
		
		checksum = (10 - (checksum % 10)) % 10;

		if (a[a.length-1] == checksum)
			return true;
		else
			return false;
	}
	
	public static int generate(String accountNumberWithoutCheckDigit)
	{
		int[] a = new int[accountNumberWithoutCheckDigit.length()+1];
		for(int i=0; i<a.length-1; i++)
			a[i] = Character.getNumericValue(accountNumberWithoutCheckDigit.charAt(i));
		
		int len = a.length;
		int checksum = 0;
		int temp;
		
		for (int i = 1; i < len; i++)
		{
			int n = a[len - i - 1];
			
			// don't include last digit
			temp = a[len - i - 1] * (1 + (i % 2));
			
			if (temp < 10)
				checksum += temp;
			else
				checksum += (temp - 9);
		}
		
		checksum = (10 - (checksum % 10)) % 10;
		
		return checksum;
	}
}
