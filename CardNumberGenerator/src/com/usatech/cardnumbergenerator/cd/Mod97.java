package com.usatech.cardnumbergenerator.cd;

import java.math.BigInteger;
//import com.modp.checkdigits.*;

public class Mod97
{
	private static final BigInteger bi97 = BigInteger.valueOf(97);
	private static final BigInteger bi98 = BigInteger.valueOf(98);
	private static final BigInteger bi100 = BigInteger.valueOf(100);
	
	/*
	public static void main(String args[])
	{
		CheckISO7064Mod97_10 asdf = new CheckISO7064Mod97_10();
		
		for(int i=10; i<999999; i++)
		{
			String s = Integer.toString(i);
			int cd1 = generate(s);
			int cd2 = asdf.computeCheck(s);
			System.out.println("i=" + i + " cd1 = " + cd1 + " cd2 = " + cd2);
			if(cd1 != cd2)
			{
				System.out.println("NOT EQUAL i = " + i + " cd1 = " + cd1 + " cd2 = " + cd2);
				break;
			}
			
			boolean ok1 = check(s + padL0(cd1, 2));
			boolean ok2 = asdf.verify(s + padL0(cd2, 2));
			
			if(!ok1 || !ok2)
			{
				System.out.println("NOT OK i = " + i + " cd1 = " + cd1 + " cd2 = " + cd2 + " ok1 = " + ok1 + " ok2 = " + ok2);
				break;
			}
		}
	}
	*/
	
	public static boolean check(String s)
	{
		BigInteger i = new BigInteger(s);
		return i.mod(bi97).intValue() == 1;
	}
	
	public static int generate(String s)
	{
		BigInteger i = new BigInteger(s);
		return bi98.subtract(i.multiply(bi100).mod(bi97)).mod(bi97).intValue();
	}
	
	private static String padL0(int n, int len)
	{
		StringBuilder sb = new StringBuilder();
		String nStr = Integer.toString(n);
		sb.append(nStr);
		for(int i=0; i<(len-nStr.length()); i++)
			sb.insert(0, "0");
		return sb.toString();
	}
}
