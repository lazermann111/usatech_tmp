package com.usatech.cardnumbergenerator.cd;

import java.util.Arrays;

public class VerhoeffErrorDetection
{
	public static void main(String[] args) throws Exception
	{
		String s = "6123450001584045899012120095473";
		VerhoeffErrorDetection ed = new VerhoeffErrorDetection();
		System.out.println(ed.getVerhoeffChecksum(s));
	}
	
	private static final short[][] ip = 
	{
		{0,1,5,8,9,4,2,7},
		{1,5,8,9,4,2,7,0},
		{2,7,0,1,5,8,9,4},
		{3,6,3,6,3,6,3,6},
		{4,2,7,0,1,5,8,9},
		{5,8,9,4,2,7,0,1},
		{6,3,6,3,6,3,6,3},
		{7,0,1,5,8,9,4,2},
		{8,9,4,2,7,0,1,5},
		{9,4,2,7,0,1,5,8}
	};

	private static final short[][] ij = 
	{
		{0,1,2,3,4,5,6,7,8,9},
		{1,2,3,4,0,6,7,8,9,5},
		{2,3,4,0,1,7,8,9,5,6},
		{3,4,0,1,2,8,9,5,6,7},
		{4,0,1,2,3,9,5,6,7,8},
		{5,9,8,7,6,0,4,3,2,1},
		{6,5,9,8,7,1,0,4,3,2},
		{7,6,5,9,8,2,1,0,4,3},
		{8,7,6,5,9,3,2,1,0,4},
		{9,8,7,6,5,4,3,2,1,0}
	};

	/** Returns the Verhoeff checksum of the entire string. */
	public char getVerhoeffChecksum(String s) throws Exception
	{
		return getVerhoeffChecksum(s, s.length(), 0);
	}

	/**
	 Returns the Verhoeff checksum for the specified string, starting the analysis
	 at the specified index and proceeding through the specified number of characters.
	 The string must not contain any other character exception the digits 0-9 and the decimal point '.'.
	 @throws Exception if the string contains an invalid character
	 */
	public char getVerhoeffChecksum(String s, int nChars, int startIndex) throws Exception
	{
		int k = 0;
		int m = 0;
		// zoom through the string
		for (int j = startIndex; j < nChars; j++)
		{
			char c = s.charAt(j);
			if (c >= '0' && c <= '9')
				k = ij[k][ip[(c + 2) % 10][7 & m++]];
		}
		// find the checksum digit
		for (int j = 0; j <= 9; j++)
		{
			if (ij[k][ip[j][7 & m]] == 0)
				return (char) ('0' + j);
		}
		throw new Exception("Verhoeff internal error");
	}

	/**
	 Returns the Verhoeff checksum for the specified string, starting the analysis
	 at the specified index and ending with the specified termination character.
	 The string must contain the termination character, and must not contain
	 any other character exception the digits 0-9 and the decimal point '.'.
	 @throws VerhoeffException if the string contains an invalid character, or if the 
	 termination character is missing
	 */
	public char getVerhoeffChecksum(String s, char term, int startIndex) throws Exception
	{
		int k = 0;
		int m = 0;
		int slen = s.length();
		// zoom through the string
		// the only valid characters preceding the terminator are the digits 0-9 and '.'
		for (int j = startIndex; j < slen; j++)
		{
			char c = s.charAt(j);
			if (c == term)
				break;
			if (j >= slen - 1)
				throw new Exception("No termination character");
			else if (c >= '0' && c <= '9')
				k = ij[k][ip[(c + 2) % 10][7 & m++]];
			else if (c != '.')
				throw new Exception("Illegal character in expression");
		}

		// find the checksum digit
		for (int j = 0; j <= 9; j++)
		{
			if (ij[k][ip[j][7 & m]] == 0)
				return (char) ('0' + j);
		}
		throw new Exception("Verhoeff internal error");
	}
}
