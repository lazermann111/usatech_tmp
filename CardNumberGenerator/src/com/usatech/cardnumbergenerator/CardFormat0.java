package com.usatech.cardnumbergenerator;

import java.util.*;
import java.util.regex.*;

import cern.jet.random.engine.*;

import org.apache.commons.configuration.*;

import com.usatech.db.*;
import com.usatech.util.*;

import com.usatech.cardnumbergenerator.cd.*;
import com.usatech.cardnumbergenerator.lfsr.*;

/**
 * 
 * 		Card Format 0 ----------- (33 characters) (General Gift/Employee Card Format)
 * 		
 * 		0            1               2             3      
 * 		123456 7   890 12345678 9    0 1234 56  78901 23
 * 		IIN    Fmt Grp AcctID   Luhn = YYMM LCC Verif Mod97
 * 
 * 		6	IIN
 * 		1	Card Format Indicator [0-9] = 0
 * 		3	Card Group Code (Left Zero Padded)
 * 		8	Account ID (Generated using lfsr MIT864 with group code as initial seed) (Left Zero Padded)
 * 		1	Luhn's Algorithm Check Digit (Calculated using all preceding digits)
 * 		1	= Delimiter
 * 		4	Expiration (YYMM)
 * 		2	Lost Card Counter (Left Zero Padded)
 * 		5	Verification Value (Random number with current time as initial seed) (Left Zero Padded)
 *		2	Mod97 Check Digits (Calculated using all preceding digits, subsititute 0 for = sign) (Left Zero Padded)
 * 		---------------
 * 		33 Digits Total
 *
 */

public class CardFormat0 extends Generator
{
	//private static Log	log	= LogFactory.getLog(CardFormat0.class);
	
	public static final int FORMAT_ID = 0;
	
	public static final int PERSISTENCE_INSERT 	= 0;
	public static final int PERSISTENCE_SQL 	= 1;
	public static final int PERSISTENCE_NONE 	= 2;
	
	private static final int MIN_ACCOUNT_ID = 1;
	private static final int MAX_ACCOUNT_ID = 99999999;
	
	private static final int MIN_VV = 1;
	private static final int MAX_VV = 99999;
	
	private static Pattern validationPattern = Pattern.compile("([0-9]{6})([0-9]{1})([0-9]{3})([0-9]{8})([0-9]{1})=([0-9]{4})([0-9]{2})([0-9]{5})([0-9]{2})");

	private String expirationYYMM;
	private int groupID;
	private int lastLFSRVal;
	private int lostCardCD;
	private int numCards;
	private int balance;
	private int consumerID;
	private int locationID;
	private int paymentSubtypeID;
	private int issueNum;
	private String currencyCD;
	private int persistence;
	
	private MIT864 lfsr;
	private MersenneTwister rng;
	
	private List<Card> cards = new ArrayList<Card>();
	
	public static void main(String args[])
	{
		if(args.length != 1)
		{
			System.out.println("Usage: java com.usatech.cardnumbergenerator.CardFormat0 <properties_file>");
			System.exit(1);
		}
		
		try
		{
			new CardFormat0(new PropertiesConfiguration(args[0]));
		}
		catch(ConfigurationException e)
		{
			System.out.println("Usage: java com.usatech.cardnumbergenerator.CardFormat0 <properties_file>");
			System.exit(1);
		}
		catch(Exception e)
		{
			System.out.println("CardFormat0 Failed: "+ e.getMessage());
			e.printStackTrace(System.out);
			System.exit(1);
		}
	}
	
	public CardFormat0(PropertiesConfiguration props) throws Exception
	{
		loadProperties(props);
		
		// pre-load the database
		SQL.getConnectionManager();

		Integer.parseInt(expirationYYMM);
		
		lfsr = new MIT864(groupID);
		if(lastLFSRVal > 0)
		{
			// roll the lfsr forward untill we find the last stoping point
			int n = -1;
			while((n = getNextLFSRValue()) != lastLFSRVal);
		}
		else
		{
			// roll the lfsr forward to burn the first 10 numbers; thet're usually more randomized
			for(int i=0; i<10; i++)
				getNextLFSRValue();
		}
		
		// always burn the first usable lfsr value for each new request
		// for security and because the first generated lfsr value matches the seed
		getNextLFSRValue();
		
		rng = new MersenneTwister((int)System.currentTimeMillis());
		//rng = new MersenneTwister(groupID);
		
		for(int i=0; i<numCards; i++)
		{
			Card card = new Card();
			
			card.accountID = getNextLFSRValue();
			card.vv = getNextRandom();
			
			card.generate();
			card.validate();
			
			cards.add(card);
		}
		
		String sql = "insert into consumer_acct(consumer_acct_cd, consumer_acct_active_yn_flag, consumer_acct_balance, consumer_id, payment_subtype_id, location_id, consumer_acct_issue_num, consumer_acct_activation_ts, currency_cd, consumer_acct_issue_cd,consumer_acct_validation_cd) values (?,?,?,?,?,?,?,?,?,?,?)";
		
		Vector args = new Vector();
		
		int counter = 0;
		for(Card card : cards)
		{
			System.out.println("CARD" + ++counter + ":" + card.getTrack2());
			
			args.clear();
			
			args.add(card.consumerAcctCD);
			args.add("Y");
			args.add(new Integer(balance));
			args.add(new Integer(consumerID));
			args.add(new Integer(paymentSubtypeID));
			args.add(new Integer(locationID));
			args.add(new Integer(issueNum));
			args.add(new Date());
			args.add(currencyCD);
			args.add(padL0(lostCardCD, 2));
			args.add(padL0(card.vv,5));
			
			if(persistence == PERSISTENCE_INSERT)
			{
				Update insert = new Update(sql, args);
				if(!insert.execute())
				{
					System.out.println("Insert Failed!: " + insert.getException());
					insert.getException().printStackTrace(System.out);
					System.exit(1);
				}
				insert.close();
			}
			else if(persistence == PERSISTENCE_SQL)
			{
				System.out.println("SQL:" + SQL.queryToString(sql, args));
			}
			
			if(counter % 100 == 0)
				Runtime.getRuntime().gc();
		}
	}
	
	private void loadProperties(PropertiesConfiguration props) throws Exception
	{
		props.setThrowExceptionOnMissing(true);
		
		expirationYYMM = props.getString("expirationYYMM");
		groupID = props.getInt("groupID");
		lastLFSRVal = props.getInt("lastLFSRVal");
		lostCardCD = props.getInt("lostCardCD");
		numCards = props.getInt("numCards");
		consumerID = props.getInt("consumerID");
		locationID = props.getInt("locationID");
		balance = props.getInt("balance");
		paymentSubtypeID = props.getInt("paymentSubtypeID");
		issueNum = props.getInt("issueNum");
		currencyCD = props.getString("currencyCD");
		String persistenceStr = props.getString("persistence");
		
		if(persistenceStr.equalsIgnoreCase("INSERT"))
			persistence = PERSISTENCE_INSERT;
		else if(persistenceStr.equalsIgnoreCase("SQL"))
			persistence = PERSISTENCE_SQL;
		else if(persistenceStr.equalsIgnoreCase("NONE"))
			persistence = PERSISTENCE_NONE;
		else
			throw new Exception("Invalid persistence!");
		
		if(expirationYYMM.length() != 4)
			throw new Exception("Invalid expirationYYMM length!");
		
		if(groupID < 0 || groupID > 999)
			throw new Exception("groupID out of range!");
		
		if(numCards <= 0)
			throw new Exception("numCards out of range!");
		
		if(consumerID <= 0)
			throw new Exception("consumerID out of range!");

		if(locationID <= 0)
			throw new Exception("locationID out of range!");
	}
	
	private int getNextLFSRValue()
	{
		//int c = 0;
		int n = -1;
		while(n < MIN_ACCOUNT_ID ||  n > MAX_ACCOUNT_ID)
		{
			//c++;
			n = lfsr.nextInt();
		}
		//System.out.println("c="+c);
		return n;
	}
	
	private int getNextRandom()
	{
		return (int) Math.round(Math.floor(((MAX_VV + 1 - MIN_VV) * rng.nextDouble()) + MIN_VV));
	}
	
	private class Card
	{
		private int accountID;
		private int luhnCD;
		private int vv;
		private int mod97;
		
		private StringBuilder sb = new StringBuilder();
		
		private String consumerAcctCD;
		
		private void generate()
		{
			sb.append(IIN);
			sb.append(FORMAT_ID);
			sb.append(padL0(groupID, 3));
			sb.append(padL0(accountID, 8));
			
			consumerAcctCD = padL0(groupID, 3) + padL0(accountID, 8);
			
			luhnCD = Luhn.generate(sb.toString());
			sb.append(luhnCD);
			sb.append("=");
			sb.append(expirationYYMM);
			sb.append(padL0(lostCardCD, 2));
			sb.append(padL0(vv,5));
			mod97 = Mod97.generate(sb.toString().replace('=','0'));
			sb.append(padL0(mod97, 2));
		}
		
		private void validate() throws Exception
		{
			String s = getTrack2();
			
			Matcher matcher = validationPattern.matcher(s);
			if(!matcher.matches())
				throw new Exception("Card does not match format! " + s);
			
			String iin = matcher.group(1);
			String formatID = matcher.group(2);
			String groupID = matcher.group(3);
			String accountID = matcher.group(4);
			String luhnCD = matcher.group(5);
			String exp = matcher.group(6);
			String lostCard = matcher.group(7);
			String vv = matcher.group(8);
			String mod97 = matcher.group(9);
			
			if(!iin.equals(IIN))
				throw new Exception("Fails IIN check! " + s);
			
			if(!formatID.equals(Integer.toString(FORMAT_ID)))
				throw new Exception("Fails Format ID check! " + s);
			
			if(!lostCard.equals("00"))
				throw new Exception("Fails Lost Card check! " + s);

			String pan = iin + formatID + groupID + accountID + luhnCD;
			if(!Luhn.check(pan))
				throw new Exception("Fails Luhn double check! " + s);
			
			if(!Mod97.check(s.replace('=','0')))
				throw new Exception("Fails Mod97 double check! " + s);
		}
		
		private String getTrack2()
		{
			return sb.toString();
		}
	
	}
	
	public String[] getGeneratedCardInfo() throws Exception{
		return null;
	}
}
