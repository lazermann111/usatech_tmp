package com.usatech.cardnumbergenerator;

public abstract class Generator
{
	public static final String IIN = "639621";
	
	public static String padL0(int n, int len)
	{
		StringBuilder sb = new StringBuilder();
		String nStr = Integer.toString(n);
		if (nStr.length() > len)
			return nStr.substring(nStr.length() - len);
		sb.append(nStr);
		for(int i=0; i<(len-nStr.length()); i++)
			sb.insert(0, "0");
		return sb.toString();
	}
	
	/**
	 * Added abstract getGeneratedCardInfo to optionally return card info outside of native invocation.
	 * @return
	 */
	public abstract String[] getGeneratedCardInfo() throws Exception;
}
