package com.usatech.cardnumbergenerator;

import java.util.*;
import java.util.regex.*;
import java.util.concurrent.atomic.*;

import cern.jet.random.engine.*;

import org.apache.commons.configuration.*;

import com.usatech.db.*;
import com.usatech.util.*;

import com.usatech.cardnumbergenerator.cd.*;
import com.usatech.cardnumbergenerator.lfsr.*;

/**
 * 
 * 		Card Format 1 ----------- (28 characters) (General Service Card Format)
 * 		
 * 		0                  1                2        
 * 		123456 7   8      901 234567 8    9 0123 45678 
 * 		IIN    Fmt ProcCd Grp OperID Luhn = YYMM Verif
 * 
 * 		6	IIN
 * 		1	Card Format Indicator [0-9] = 1
 * 		1	Service Card Process Code
 * 		3	Operator Group ID (Left Zero Padded)
 * 		6	Operator ID (Sequentially generated from 1) (Left Zero Padded)
 * 		1	Luhn's Algorithm Check Digit (Calculated using all preceding digits)
 * 		1	= Delimiter
 * 		4	Expiration (YYMM) (Substitute 9999 to ignore)
 * 		5	Verification Value (Random number with current time as initial seed) (Substitute 0000 to ignore) (Left Zero Padded)
 * 		---------------
 * 		28 Digits Total
 *
 */

public class CardFormat1 extends Generator
{
	//private static Log	log	= LogFactory.getLog(CardFormat0.class);
	
	public static final int FORMAT_ID = 1;
	
	public static final int PERSISTENCE_INSERT 	= 0;
	public static final int PERSISTENCE_SQL 	= 1;
	public static final int PERSISTENCE_NONE 	= 2;
	
	private static final int MIN_VV = 1;
	private static final int MAX_VV = 99999;
	
	private static Pattern validationPattern = Pattern.compile("([0-9]{6})([0-9]{1})([0-9]{1})([0-9]{3})([0-9]{6})([0-9]{1})=([0-9]{4})([0-9]{5})");

	private String expirationYYMM;
	private int processCode;
	private int operatorGroupID;
	private int lastOperatorID;
	private int numCards;
	private int balance;
	private int consumerID;
	private int locationID;
	private int paymentSubtypeID;
	private String currencyCD;
	private int persistence;
	
	private MersenneTwister rng;
	private AtomicInteger operatorIDCounter = new AtomicInteger();
	
	private List<Card> cards = new ArrayList<Card>();
	
	public static void main(String args[])
	{
		if(args.length != 1)
		{
			System.out.println("Usage: java com.usatech.cardnumbergenerator.CardFormat1 <properties_file>");
			System.exit(1);
		}
		
		try
		{
			new CardFormat1(new PropertiesConfiguration(args[0]));
		}
		catch(ConfigurationException e)
		{
			System.out.println("Usage: java com.usatech.cardnumbergenerator.CardFormat1 <properties_file>");
			System.exit(1);
		}
		catch(Exception e)
		{
			System.out.println("CardFormat0 Failed: "+ e.getMessage());
			e.printStackTrace(System.out);
			System.exit(1);
		}
	}
	
	public CardFormat1(PropertiesConfiguration props) throws Exception
	{
		loadProperties(props);
		
		// pre-load the database
		SQL.getConnectionManager();

		Integer.parseInt(expirationYYMM);
		
		rng = new MersenneTwister((int)System.currentTimeMillis());
		
		operatorIDCounter.set(lastOperatorID+1);
		
		for(int i=0; i<numCards; i++)
		{
			Card card = new Card();
			
			card.accountID = operatorIDCounter.getAndIncrement();
			card.vv = getNextRandom();
			
			card.generate();
			card.validate();
			
			cards.add(card);
		}
		
		String sql = "insert into consumer_acct(consumer_acct_cd, consumer_acct_active_yn_flag, consumer_acct_balance, consumer_id, payment_subtype_id, location_id, consumer_acct_issue_num, consumer_acct_activation_ts, currency_cd, consumer_acct_issue_cd,consumer_acct_validation_cd) values (?,?,?,?,?,?,?,?,?,?,?)";
		
		Vector args = new Vector();
		
		int counter = 0;
		for(Card card : cards)
		{
			//System.out.println("CARD" + ++counter + ":" + card.getTrack2());
			++counter;
			System.out.println(card.getTrack2());
			
			args.clear();
			
			args.add(card.consumerAcctCD);
			args.add("Y");
			args.add(new Integer(balance));
			args.add(new Integer(consumerID));
			args.add(new Integer(paymentSubtypeID));
			args.add(new Integer(locationID));
			args.add(new Integer(1));
			args.add(new Date());
			args.add(currencyCD);
			args.add(padL0(0, 2));
			args.add(padL0(card.vv,5));
			
			if(persistence == PERSISTENCE_INSERT)
			{
				Update insert = new Update(sql, args);
				if(!insert.execute())
				{
					System.out.println("Insert Failed!: " + insert.getException());
					insert.getException().printStackTrace(System.out);
					System.exit(1);
				}
				insert.close();
			}
			else if(persistence == PERSISTENCE_SQL)
			{
				System.out.println("SQL:" + SQL.queryToString(sql, args));
			}
			
			if(counter % 100 == 0)
				Runtime.getRuntime().gc();
		}
	}
	
	private void loadProperties(PropertiesConfiguration props) throws Exception
	{
		props.setThrowExceptionOnMissing(true);
		
		processCode = props.getInt("processCode");
		operatorGroupID = props.getInt("operatorGroupID");
		lastOperatorID = props.getInt("lastOperatorID");
		expirationYYMM = props.getString("expirationYYMM");
		numCards = props.getInt("numCards");
		consumerID = props.getInt("consumerID");
		locationID = props.getInt("locationID");
		balance = props.getInt("balance");
		paymentSubtypeID = props.getInt("paymentSubtypeID");
		currencyCD = props.getString("currencyCD");
		String persistenceStr = props.getString("persistence");
		
		if(persistenceStr.equalsIgnoreCase("INSERT"))
			persistence = PERSISTENCE_INSERT;
		else if(persistenceStr.equalsIgnoreCase("SQL"))
			persistence = PERSISTENCE_SQL;
		else if(persistenceStr.equalsIgnoreCase("NONE"))
			persistence = PERSISTENCE_NONE;
		else
			throw new Exception("Invalid persistence!");
		
		if(expirationYYMM.length() != 4)
			throw new Exception("Invalid expirationYYMM length!");
		
		if(processCode < 0 || processCode > 9)
			throw new Exception("processCode out of range!");
		
		if(operatorGroupID < 0 || operatorGroupID > 999)
			throw new Exception("operatorGroupID out of range!");
		
		if(numCards <= 0)
			throw new Exception("numCards out of range!");
		
		if(consumerID <= 0)
			throw new Exception("consumerID out of range!");

		if(locationID <= 0)
			throw new Exception("locationID out of range!");
	}
	
	private int getNextRandom()
	{
		return (int) Math.round(Math.floor(((MAX_VV + 1 - MIN_VV) * rng.nextDouble()) + MIN_VV));
	}
	
	private class Card
	{
		private int accountID;
		private int luhnCD;
		private int vv;
		
		private StringBuilder sb = new StringBuilder();
		
		private String consumerAcctCD;
		
		private void generate()
		{
			sb.append(IIN);
			sb.append(FORMAT_ID);
			sb.append(processCode);
			sb.append(padL0(operatorGroupID, 3));
			sb.append(padL0(accountID, 6));
			
			consumerAcctCD = padL0(operatorGroupID, 3) + padL0(accountID, 6);
			
			luhnCD = Luhn.generate(sb.toString());
			sb.append(luhnCD);
			sb.append("=");
			sb.append(expirationYYMM);
			sb.append(padL0(vv,5));
		}
		
		private void validate() throws Exception
		{
			String s = getTrack2();
			
			Matcher matcher = validationPattern.matcher(s);
			if(!matcher.matches())
				throw new Exception("Card does not match format! " + s);
			
			String iin = matcher.group(1);
			String formatID = matcher.group(2);
			String processCode = matcher.group(3);
			String groupID = matcher.group(4);
			String accountID = matcher.group(5);
			String luhnCD = matcher.group(6);
			String exp = matcher.group(7);
			String vv = matcher.group(8);
			
			if(!iin.equals(IIN))
				throw new Exception("Fails IIN check! " + s);
			
			if(!formatID.equals(Integer.toString(FORMAT_ID)))
				throw new Exception("Fails Format ID check! " + s);
			
			String pan = iin + formatID + processCode + groupID + accountID + luhnCD;
			if(!Luhn.check(pan))
				throw new Exception("Fails Luhn double check! " + s);
		}
		
		private String getTrack2()
		{
			return sb.toString();
		}
		
	}
	
	public String[] getGeneratedCardInfo() throws Exception{
		return null;
	}
}
