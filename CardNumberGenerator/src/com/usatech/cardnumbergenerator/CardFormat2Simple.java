package com.usatech.cardnumbergenerator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cern.jet.random.engine.MersenneTwister;

import com.usatech.cardnumbergenerator.cd.Luhn;
import com.usatech.cardnumbergenerator.cd.Mod97;
import com.usatech.cardnumbergenerator.lfsr.MIT864;

/**
 * 
 * 		Card Format 2 ----------- (33 characters) (Prepaid Card Format)
 * 		
 * 		0            1               2             3      
 * 		123456 7   890 12345678 9    0 1234 56  78901 23
 * 		IIN    Fmt Grp AcctID   Luhn = YYMM LCC Verif Mod97
 * 
 * 		6	IIN
 * 		1	Card Format Indicator [0-9] = 0
 * 		3	Card Group Code (Left Zero Padded)
 * 		8	Account ID (Generated using lfsr MIT864 with group code as initial seed) (Left Zero Padded)
 * 		1	Luhn's Algorithm Check Digit (Calculated using all preceding digits)
 * 		1	= Delimiter
 * 		4	Expiration (YYMM)
 * 		2	Lost Card Counter (Left Zero Padded)
 * 		5	Verification Value (Random number with current time as initial seed) (Left Zero Padded)
 *		2	Mod97 Check Digits (Calculated using all preceding digits, subsititute 0 for = sign) (Left Zero Padded)
 * 		---------------
 * 		33 Digits Total
 *
 */

public class CardFormat2Simple extends Generator
{
	public static final int FORMAT_ID = 2;
	
	private static final int MIN_ACCOUNT_ID = 1;
	private static final int MAX_ACCOUNT_ID = 99999999;
	
	private static final int MIN_VV = 1;
	private static final int MAX_VV = 99999;
	
	private static Pattern validationPattern = Pattern.compile("([0-9]{6})([0-9]{1})([0-9]{3})([0-9]{8})([0-9]{1})=([0-9]{4})([0-9]{2})([0-9]{5})([0-9]{2})");

	private String expirationYYMM;
	private String lastConsumerAcctCD;
	private int groupID;
	private int lostCardCD;
	private int numCards;
	
	private MIT864 lfsr;
	private MersenneTwister rng;
	
	public CardFormat2Simple(String expirationYYMM, String lastConsumerAcctCD, int groupID, int lostCardCD, int numCards) throws Exception
	{
		this.expirationYYMM = expirationYYMM;
		this.lastConsumerAcctCD = lastConsumerAcctCD;
		this.groupID = groupID;
		this.lostCardCD = lostCardCD;
		this.numCards = numCards;
		
		//generate the MersenneTwister pseudorandom number, set on rng
		generateMersenneTwister();
		
		//generate the operatorIDCounter, set on operatorIDCounter
		generateLfsr();
	}
	
	public String getNextCardData() throws Exception {
		Card card = new Card();
		
		card.accountID = getNextLFSRValue();
		card.vv = getNextRandom();
		
		card.generate();
		card.validate();
		
		return card.toString();
	}
	
	/**
	 * Returns a String[] of generated card info.
	 * @param lastConsumerAcctCD
	 * @param groupID
	 * @param numCards
	 * @return
	 * @throws Exception
	 */
	public String[] getGeneratedCardInfo() throws Exception {
		
		String[] returnCardInfo = new String[this.numCards];
		for(int i=0; i<this.numCards; i++)				
			returnCardInfo[i] = getNextCardData();
		return returnCardInfo;
	}

	private void generateLfsr() {
		int lastLFSRVal = 0;
		if(lastConsumerAcctCD != null && !lastConsumerAcctCD.equals("0")) {
			try {
				lastLFSRVal = Integer.parseInt(lastConsumerAcctCD.substring(3));
			} catch (Exception e) {
				
			}
		}
		
		if(lastLFSRVal > 0) {
            lfsr = new MIT864(lastLFSRVal);
			// roll the lfsr forward until we find the last stopping point
			for(int i=0; i<1000000; i++) {
				if (getNextLFSRValue() == lastLFSRVal)
					return;
			}
		}

        lfsr = new MIT864(groupID);
		// roll the lfsr forward to burn the first 10 numbers; they're usually more randomized
		for(int i=0; i<10; i++)
			getNextLFSRValue();
	}

	private void generateMersenneTwister() {
		rng = new MersenneTwister((int)System.currentTimeMillis());
	}

	private int getNextLFSRValue()
	{
		int n = -1;
		while(n < MIN_ACCOUNT_ID ||  n > MAX_ACCOUNT_ID)
			n = lfsr.nextInt();
		return n;
	}
	
	private int getNextRandom()
	{
		return (int) Math.round(Math.floor(((MAX_VV + 1 - MIN_VV) * rng.nextDouble()) + MIN_VV));
	}
	
	private class Card
	{
		private String groupIDPadded;
		private String accountIDPadded;
		private String lostCardCDPadded;
		private String vvPadded;
		private String mod97Padded;
		
		private String consumerAcctCD;
		private int accountID;
		private int luhnCD;
		private int vv;
		private int mod97;
		
		private StringBuilder sb = new StringBuilder();
		
		private void generate()
		{
			groupIDPadded = padL0(groupID, 3);
			accountIDPadded = padL0(accountID, 8);
			lostCardCDPadded = padL0(lostCardCD, 2);
			vvPadded = padL0(vv,5);
			
			sb.append(IIN);
			sb.append(FORMAT_ID);
			sb.append(groupIDPadded);
			sb.append(accountIDPadded);

			consumerAcctCD = (groupIDPadded + accountIDPadded);
			luhnCD = Luhn.generate(sb.toString());
			
			sb.append(luhnCD);
			sb.append("=");
			sb.append(expirationYYMM);
			sb.append(lostCardCDPadded);
			sb.append(vvPadded);
			
			mod97 = Mod97.generate(sb.toString().replace('=','0'));
			mod97Padded = padL0(mod97, 2);
			sb.append(mod97Padded);
		}
		
		private void validate() throws Exception
		{
			String s = getTrack2();
			
			Matcher matcher = validationPattern.matcher(s);
			if(!matcher.matches())
				throw new Exception("Card does not match format! " + s);
			
			String iin = matcher.group(1);
			String formatID = matcher.group(2);
			String groupID = matcher.group(3);
			String accountID = matcher.group(4);
			String luhnCD = matcher.group(5);
			String exp = matcher.group(6);
			String lostCard = matcher.group(7);
			String vv = matcher.group(8);
			String mod97 = matcher.group(9);
			
			if(!iin.equals(IIN))
				throw new Exception("Fails IIN check! " + s);
			
			if(!formatID.equals(Integer.toString(FORMAT_ID)))
				throw new Exception("Fails Format ID check! " + s);
			
			if(!lostCard.equals("00"))
				throw new Exception("Fails Lost Card check! " + s);

			String pan = iin + formatID + groupID + accountID + luhnCD;
			if(!Luhn.check(pan))
				throw new Exception("Fails Luhn double check! " + s);
			
			if(!Mod97.check(s.replace('=','0')))
				throw new Exception("Fails Mod97 double check! " + s);
		}
		
		private String getTrack2()
		{
			return sb.toString();
		}
		
		public String toString()
		{
			StringBuilder toStr = new StringBuilder();
			toStr.append(getTrack2());
			sb.append(",");
			sb.append(consumerAcctCD);
			sb.append(",");
			sb.append(accountID);
			sb.append(",");
			sb.append(vvPadded);
			sb.append(",");
			sb.append(lostCardCDPadded);
			return sb.toString();
		}
	}
}
