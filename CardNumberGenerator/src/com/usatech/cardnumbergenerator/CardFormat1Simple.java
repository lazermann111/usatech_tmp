package com.usatech.cardnumbergenerator;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cern.jet.random.engine.MersenneTwister;

import com.usatech.cardnumbergenerator.cd.Luhn;

/**
 * 
 * 		Card Format 1 ----------- (28 characters) (General Service Card Format)
 * 		
 * 		0                  1                2        
 * 		123456 7   8      901 234567 8    9 0123 45678 
 * 		IIN    Fmt ProcCd Grp OperID Luhn = YYMM Verif
 * 
 * 		6	IIN
 * 		1	Card Format Indicator [0-9] = 1
 * 		1	Service Card Process Code
 * 		3	Operator Group ID (Left Zero Padded)
 * 		6	Operator ID (Sequentially generated from 1) (Left Zero Padded)
 * 		1	Luhn's Algorithm Check Digit (Calculated using all preceding digits)
 * 		1	= Delimiter
 * 		4	Expiration (YYMM) (Substitute 9999 to ignore)
 * 		5	Verification Value (Random number with current time as initial seed) (Substitute 0000 to ignore) (Left Zero Padded)
 * 		---------------
 * 		28 Digits Total
 * 
 *  	Author		Date		Bug#		Modification Desc.
 * 		--------------------------------------------------------------------------------------------------------------
 * 		EPC			20100413	DMS			Attemtpt to avoid native process invocation by extracting methods and creating new constructor
 * 											with boolean that determines if calling process would like the results returned in a String[].
 *
 */

public class CardFormat1Simple extends Generator
{
	public static final int FORMAT_ID = 1;
	
	private static final int MIN_VV = 1;
	private static final int MAX_VV = 99999;
	
	private static Pattern validationPattern = Pattern.compile("([0-9]{6})([0-9]{1})([0-9]{1})([0-9]{3})([0-9]{6})([0-9]{1})=([0-9]{4})([0-9]{5})");

	// inputs
	private String 	expirationYYMM;
	private String 	lastConsumerAcctCD;
	private int 	operatorGroupID;
	private int 	processCode;
	private int 	numCards;
	
	private MersenneTwister 	rng;
	private AtomicInteger 		operatorIDCounter;	
	
	public CardFormat1Simple(String expirationYYMM, String lastConsumerAcctCD, int operatorGroupID, int processCode, int numCards) throws Exception
	{		
		this.expirationYYMM = expirationYYMM;
		this.lastConsumerAcctCD = lastConsumerAcctCD;
		this.operatorGroupID = operatorGroupID;
		this.processCode = processCode;
		this.numCards = numCards;
		
		//generate the MersenneTwister pseudorandom number, set on rng
		generateMersenneTwister();
		
		//generate the operatorIDCounter, set on operatorIDCounter
		generateOperatorIDCounter();
	}
	
	public String getNextCardData() throws Exception {
		Card card = new Card();
		
		card.accountID = this.operatorIDCounter.getAndIncrement();
		card.vv = getNextRandom();
		
		card.generate();
		card.validate();
		
		return card.toString();
	}
	
	/**
	 * Returns a String[] of generated card info.
	 * @throws Exception
	 */
	public String[] getGeneratedCardInfo() throws Exception {
		
		String[] returnCardInfo = new String[this.numCards];						
		for(int i=0; i<this.numCards; i++)
			returnCardInfo[i] = getNextCardData();		
		return returnCardInfo;
	
	}

	private void generateOperatorIDCounter() {
		int lastOperatorID = 0;
		if(lastConsumerAcctCD != null && !lastConsumerAcctCD.equals("0")) {
			try {
				lastOperatorID = Integer.parseInt(lastConsumerAcctCD.substring(3));
			} catch (Exception e) {
				
			}
		}
		
		operatorIDCounter = new AtomicInteger();
		operatorIDCounter.set(lastOperatorID);
		operatorIDCounter.incrementAndGet();
	}

	private void generateMersenneTwister() {
		rng = new MersenneTwister((int)System.currentTimeMillis());
	}
	
	private int getNextRandom()
	{
		return (int) Math.round(Math.floor(((MAX_VV + 1 - MIN_VV) * rng.nextDouble()) + MIN_VV));
	}

	private class Card
	{
		private int accountID;
		private String operatorGroupIDPadded;		
		private String accountIDPadded;
		private String consumerAcctCD;
		private int luhnCD;
		private int vv;
		private String vvPadded;
		
		private StringBuilder sb = new StringBuilder();
		
		private void generate()
		{
			operatorGroupIDPadded = padL0(operatorGroupID, 3);
			accountIDPadded = padL0(accountID, 6);
			vvPadded = padL0(vv,5);
			
			sb.append(IIN);
			sb.append(FORMAT_ID);
			sb.append(processCode);
			sb.append(operatorGroupIDPadded);
			sb.append(accountIDPadded);
			
			consumerAcctCD = (operatorGroupIDPadded + accountIDPadded);
			luhnCD = Luhn.generate(sb.toString());
			
			sb.append(luhnCD);
			sb.append("=");
			sb.append(expirationYYMM);
			sb.append(vvPadded);
		}
		
		private void validate() throws Exception
		{
			String s = getTrack2();
			
			Matcher matcher = validationPattern.matcher(s);
			if(!matcher.matches())
				throw new Exception("Card does not match format! " + s);
			
			String iin = matcher.group(1);
			String formatID = matcher.group(2);
			String processCode = matcher.group(3);
			String groupID = matcher.group(4);
			String accountID = matcher.group(5);
			String luhnCD = matcher.group(6);
			String exp = matcher.group(7);
			String vv = matcher.group(8);
			
			if(!iin.equals(IIN))
				throw new Exception("Fails IIN check! " + s);
			
			if(!formatID.equals(Integer.toString(FORMAT_ID)))
				throw new Exception("Fails Format ID check! " + s);
			
			String pan = iin + formatID + processCode + groupID + accountID + luhnCD;
			if(!Luhn.check(pan))
				throw new Exception("Fails Luhn double check! " + s);
			
			if(!exp.equals(expirationYYMM))
				throw new Exception("Fails exp check! " + s);
			
			if(!vv.equals(vvPadded))
				throw new Exception("Fails vv check! " + s);
		}
		
		private String getTrack2()
		{
			return sb.toString();
		}
		
		public String toString()
		{
			StringBuilder toStr = new StringBuilder();
			toStr.append(getTrack2());
			sb.append(",");
			sb.append(consumerAcctCD);
			sb.append(",");
			sb.append(accountID);
			sb.append(",");
			sb.append(vvPadded);
			return sb.toString();
		}
	}
}
