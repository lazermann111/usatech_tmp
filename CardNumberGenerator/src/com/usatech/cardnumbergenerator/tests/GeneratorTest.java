package com.usatech.cardnumbergenerator.tests;

import org.junit.Test;

import com.usatech.cardnumbergenerator.lfsr.MIT864;

import static com.usatech.cardnumbergenerator.cd.Luhn.generate;
import static com.usatech.cardnumbergenerator.cd.Mod97.check;

import java.util.BitSet;

public class GeneratorTest {
	
	@Test
	public void testLuhn() {
		String arg = "4143671352598294";
		System.out.println(generate(arg));
	}
	
	@Test
	public void testMod97() {
		String s = "612345000158404589901212009547361";
		System.out.println(check(s));
	}
	
	@Test
	public void testMIT864() {
		int seed = 3;
		/*
		int want = Integer.parseInt(args[1]);
		int max = Integer.parseInt(args[2]);
		
		int got = 0;
		
		BitSet bitSet = new BitSet();
		
		MIT864 lfsr = new MIT864(seed);
		long st = System.currentTimeMillis();
		while(got < want)
		{
			int n = lfsr.nextInt();
			if(n > 0 && n <= max)
			{
				if(bitSet.get(n))
				{
					System.out.println("GOT DUPLICATE!!! (" + n + ")");
					System.exit(1);
				}
				
				
				got++;
				bitSet.set(n);
			}
		}
		
		long et = (System.currentTimeMillis() - st);
		
		System.out.println("et="+et);
		
		*/

		
		int reps = 10;
		int histoBins = 2;
		int printPoints = (reps/10);
		
		BitSet bitSet = new BitSet();
		int maxBit = 0;
		int[] bins = new int[11];
		
		MIT864 lfsr = new MIT864(seed);
		for (int i = 1; i <= reps; i++)
		{
			int n = -1;
			while(n <= 0)
				n = lfsr.nextInt();
			
			System.out.println("n="+n);
			
			if(bitSet.get(n))
			{
				System.out.println("already set: i=" + i + " n=" + n);
				return;
			}
			
			if(n > maxBit)
				maxBit = n;
			
			if((i % printPoints) == 0)
			{
				double percentDone = ((double)i/(double)reps)*100;
				System.out.println(i + " = " + n + "\t\t\t" + percentDone + "% done");
			}
			
			bitSet.set(n);
			
			bins[Integer.toString(n).length()]++;
		}
		
		for(int i=0; i<bins.length; i++)
			System.out.println("len " + i + "\t = " + bins[i]);
		/*	
		System.out.println();
		System.out.println("Calculating histo...");
		System.out.println();
		
		int[] histo = new int[histoBins];
		
		for (int i = 0; i < maxBit; i++)
		{
			if(bitSet.get(i))
				//System.out.println("histo[" + (i%histoBins) + "] = " + ++histo[(i%histoBins)]);
				++histo[(i%histoBins)];
		}
		
		int max = 100;
		
		for(int i=0; i<histo.length; i++)
		{
			int binVal = histo[i];
			int normalized = max - (int) Math.round(((double)max/(double)binVal)*100);
			System.out.print(i+"\t"+binVal+"\t"+normalized+"\t");
			for(int j=0; j<normalized; j++)
				System.out.print("#");
			System.out.println();
		}
		
		System.out.println();
		*/
	}

}
