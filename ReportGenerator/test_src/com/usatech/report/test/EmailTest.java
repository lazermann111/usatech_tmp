package com.usatech.report.test;

import java.util.Properties;

import simple.mail.Email;
import simple.mail.Emailer;

public class EmailTest {
	
	public static void main(String args[]) throws Exception{
		Properties props=new Properties();
		props.put("mail.smtp.host", "mailhost.usatech.com");
		Emailer emailer = new Emailer(props);
		Email email = emailer.createEmail();
		email.setTo("yhe@usatech.com");
		email.setFrom("yhe@yahoo.com");
		email.setSubject("aTest");
		email.setBody("ok mail is working");
		email.send();
	}

}
