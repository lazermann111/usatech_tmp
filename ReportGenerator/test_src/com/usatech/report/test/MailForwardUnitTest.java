package com.usatech.report.test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.usatech.ccs.MailForwardScheduledThread;

import junit.framework.Assert;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.test.UnitTest;

/**
 * Runs a test to ensure Mail Forward properly sends emails to the queue
 * @author phorsfield
 *
 */
public class MailForwardUnitTest extends UnitTest {

	private static final String SUBJECT_LINE_PREFIX = "Test New MailForward";
	private static final String SQL_TEMPLATE = 
	"insert into ob_email_queue (ob_email_from_email_addr, ob_email_from_name, ob_email_to_email_addr, ob_email_to_name, ob_email_subject, ob_email_msg, ob_email_sent_success_ts, ob_email_scheduled_send_ts) " + 
	"values ('phorsfield@usatech.com', 'Peter', 'phorsfield@usatech.com', 'Peter H', ?, 'This is a test of the new mailforward code', NULL, SYSDATE - 3/1440)";

	private ArrayList<ByteInput> publishedMessages;
	private Connection conn;
	private TaskUnderTest task; 
	
	class TaskUnderTest extends MailForwardScheduledThread {
		public void runTest() { 
			process();  
		}  
	}
	
	@Before
	public void setUp() throws ServiceException, IOException, SQLException, DataLayerException {
		 
		super.setupDSF("TestAppLayerService-dev.properties");
		
		publishedMessages = new ArrayList<ByteInput>();
		
		task = new TaskUnderTest();
		
		task.setCcListLocation("missing");
		task.setMaxItems(100);
		task.setSendMailQueueName("usat.ccs.email");
		task.setAppInstance(3);
		//task.setConfiguredPeriod(10000);

		task.setPublisher(new Publisher<ByteInput>() {
			
			/*@Override
			public void publish(String queueKey, boolean multicast, boolean temporary,
					ByteInput content, QoS qos) throws ServiceException {
				
				publishedMessages.add(content);
				
			}*/
			
			@Override
			public void publish(String queueKey, boolean multicast, boolean temporary,
					ByteInput content) throws ServiceException {
				publishedMessages.add(content);
				
			}
			
			@Override
			public void publish(String queueKey, ByteInput content)
					throws ServiceException {
				publishedMessages.add(content);
				
			}
			
			@Override
			public void close() {}

			@Override
			public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content,
					Long correlation, QoS qos, String... blocks) throws ServiceException {
				// TODO Auto-generated method stub
				
			}

			@Override
			public int getConsumerCount(String queueKey, boolean temporary) {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public String[] getSubscriptions(String queueKey) throws ServiceException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void block(String... blocks) throws ServiceException {
				// TODO Auto-generated method stub
				
			}
		});
		
		after();		
	}
	
	@After public void after() throws SQLException, DataLayerException {
		if(conn == null || conn.isClosed()) 
		{
			conn = DataLayerMgr.getConnection("REPORT");
		}
		Statement s = conn.createStatement();
		s.setQueryTimeout(1);
		s.execute("DELETE from ob_email_queue where UPPER(ob_email_subject) like UPPER('"+SUBJECT_LINE_PREFIX+"%')");
		s.close();
		conn.commit();		
		conn.close();
	}
	
	@Test public void testMailForward() throws SQLException, DataLayerException
	{				
		conn = DataLayerMgr.getConnection("REPORT");		
		int desiredNonDup = 5;
		int desiredDup = 5;
		for(int i = 0; i < desiredDup; ++i )
		{
			PreparedStatement ps = conn.prepareStatement(SQL_TEMPLATE);
			ps.setString(1, SUBJECT_LINE_PREFIX + " Dup");
			ps.execute();
			ps.close();
		}
		for(int i = 0; i < desiredNonDup-1; ++i )
		{
			PreparedStatement ps = conn.prepareStatement(SQL_TEMPLATE);
			ps.setString(1, SUBJECT_LINE_PREFIX + " Unique: " + Math.random());
			ps.execute();
			ps.close();
		}
		conn.commit();		

		task.runTest();
		
		Assert.assertEquals("Wrong number of published messages", desiredNonDup, publishedMessages.size());

		Statement s = conn.createStatement();
		ResultSet results = s.executeQuery("select count(*) from ob_email_queue where ob_email_subject like '"+SUBJECT_LINE_PREFIX+"%' and ob_email_sent_success_ts is not null");
		results.next();
		int count = results.getInt(1);
		s.close();

		Assert.assertEquals("Wrong number of remaining messages", desiredNonDup, count);
	}
}
