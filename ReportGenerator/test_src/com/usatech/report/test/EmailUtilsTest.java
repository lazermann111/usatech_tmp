package com.usatech.report.test;

import java.util.HashMap;
import java.util.HashSet;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import com.usatech.transport.EmailUtils;

import simple.mail.EmailInfo;

public class EmailUtilsTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		String sentToEmail="aaccac@rose.net";
		EmailInfo emailInfo = new EmailInfo();
		emailInfo.setTo(sentToEmail);
		emailInfo.setCc("");
		emailInfo.setFrom("test@usatech.com");
		emailInfo.setSubject("test");
		emailInfo.setBody("this is a test");
		HashSet<String> recipients=new HashSet<String> ();
		recipients.add(sentToEmail);
		HashMap<InternetAddress, MessagingException> failedMap=new HashMap<InternetAddress, MessagingException>();
		EmailUtils.sendEmails(emailInfo, recipients, failedMap);
	}

}
