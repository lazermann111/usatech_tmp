package com.usatech.report.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Clob;
import java.util.Properties;
import java.util.concurrent.Callable;

import com.usatech.report.DexInputStream;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.db.config.ConfigLoader;
import simple.io.App;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.Log;
import simple.results.Results;
import test.ReportGeneratorTest;

public class TestDex {

	public static void testDexFile(long dexFileId) throws Exception{
		Results results = DataLayerMgr.executeQuery("GET_DEX_FILE_PATH", new Object[] { dexFileId }, false);
		if(results.next()) {
			Object content = results.getValue(3);
			final Callable<InputStream> getter;
			if(content instanceof Clob) {
				final Clob clob = (Clob) content;
				getter = new Callable<InputStream>() {
					public InputStream call() throws Exception {
						return clob.getAsciiStream();
					}
				};
			} else {
				final byte[] data = ConvertUtils.convert(byte[].class, content);
				getter = new Callable<InputStream>() {
					public InputStream call() throws Exception {
						return new ByteArrayInputStream(data);
					}
				};
			}
			;
			IOUtils.copy(new DexInputStream(getter.call()), new FileOutputStream("D:\\public\\2011Doc\\troubleshoot\\11052014DEX\\test"+dexFileId+".txt"));
		}
		
		Log.finish();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
		Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/test/test-data-layer.xml"));
        
		Results results = DataLayerMgr.executeQuery("GET_DEX_FILE", null, false);
		while(results.next()) {
			long dexFileId=ConvertUtils.getInt(results.get("dexFileId")) ;
			testDexFile(dexFileId);
		}
		
		

	}

}
