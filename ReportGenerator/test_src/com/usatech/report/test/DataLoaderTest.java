package com.usatech.report.test;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.app.BaseWithConfig;
import simple.app.ServiceException;
import simple.io.Log;
import simple.mq.app.StandaloneSimplePublisher;
import simple.mq.peer.PeerSupervisor;
import simple.mq.peer.PublishType;
import simple.test.UnitTest;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.DataLoaderAction;
import com.usatech.layers.common.constants.DataLoaderAttrEnum;

public class DataLoaderTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}

	protected StandaloneSimplePublisher setupPublisher() throws IOException, ServiceException {
		Configuration config = BaseWithConfig.loadConfig("RDWLoader.properties", DataLoaderTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
		PeerSupervisor sup = new PeerSupervisor();
		sup.setPrimaryDataSourceName("MQ_1");
		sup.setSecondaryDataSourceNames(new String[] { "MQ_2" });
		sup.setDefaultDurablePublishType(PublishType.DB);
		StandaloneSimplePublisher publisher = new StandaloneSimplePublisher();
		publisher.setSupervisor(sup);
		return publisher;
	}

	@Test
	public void testVPIteration() throws Exception {
		class SourceValueIterator {
			protected final String colname;
			protected final Object[] values;
			protected int index;

			public SourceValueIterator(String colname, Object[] values) {
				this.colname = colname;
				this.values = values;
				this.index = -1;
			}

			public boolean next() {
				return ++index < values.length;
			}

			public void reset() {
				index = -1;
			}

			public void set(Map<String, Object> sourceValues) {
				sourceValues.put(colname, values[index]);
			}
		}

		final SourceValueIterator[] svis = new SourceValueIterator[] {
				new SourceValueIterator("#1", new String[] {"a", "b", "c"}),
				new SourceValueIterator("#2", new String[] {"d", "e", "f"}),
				new SourceValueIterator("#3", new String[] {"h", "i", "j", "k"}),
				new SourceValueIterator("#4", new String[] {"l", "m"}),
				
		};
		class ValuePreparer {
			protected int i = -1;

			public boolean prepareNext() {
				if(svis.length == 0)
					return false;
				if(i < 0) {
					i = 0;
					for(int j = 0; j < svis.length; j++)
						if(!svis[j].next()) {
							i = svis.length;
							return false;
						}
					return true;
				}
				if(i < svis.length) {
					if(svis[i].next())
						return true;

					while(++i < svis.length) {
						if(svis[i].next()) {
							for(int j = 0; j < i; j++) {
								svis[j].reset();
								svis[j].next();
							}
							i = 0;
							return true;
						}
					}
					return false;
				}

				return false;
			}
		}
		Map<String, Object> input = new TreeMap<String, Object>();
		ValuePreparer vp = new ValuePreparer();
		while(vp.prepareNext()) {
			input.clear();
			for(int i = 0; i < svis.length; i++)
				svis[i].set(input);
			log.info("INPUT=" + input);
		}
	}

	@Test
	public void testUpsert() throws Exception {
		StandaloneSimplePublisher publisher = setupPublisher();
		MessageChainV11 mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.rdw.load.single");
		step.setAttribute(DataLoaderAttrEnum.ATTR_ACTION, DataLoaderAction.UPSERT);
		step.setAttribute(DataLoaderAttrEnum.ATTR_TABLE, "DEVICE_DIM");
		Map<String, Object> sourceValues = new LinkedHashMap<String, Object>();
		sourceValues.put("DEVICE_SERIAL_CD", "K2MTB000023");
		step.setAttribute(DataLoaderAttrEnum.ATTR_SOURCE_VALUES, sourceValues);
		MessageChainService.publish(mc, publisher);
	}

	@Test
	public void testMetadata() throws Exception {
		StandaloneSimplePublisher publisher = setupPublisher();
		MessageChainV11 mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.rdw.load.single");
		step.setAttribute(DataLoaderAttrEnum.ATTR_ACTION, DataLoaderAction.METADATA);
		String metadataAction = "ADD_COL";
		String xml = "<tab name=\"PAYMENT_BATCH_DIM\"><col name=\"PAYMENT_SCHEDULE_ID\" source=\"PAYMENT_SCHEDULE_ID\" sqltype=\"SMALLINT\" coltype=\"DATA\" required=\"true\"/></tab>";
		step.setAttribute(DataLoaderAttrEnum.ATTR_SOURCE_VALUES, Collections.singletonMap(metadataAction, xml));
		MessageChainService.publish(mc, publisher);
	}

	@Test
	public void testRepopulate() throws Exception {
		StandaloneSimplePublisher publisher = setupPublisher();
		MessageChainV11 mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.rdw.load.single");
		step.setAttribute(DataLoaderAttrEnum.ATTR_ACTION, DataLoaderAction.REPOPULATE);
		step.setAttribute(DataLoaderAttrEnum.ATTR_TABLE, "PAYMENT_BATCH_DIM");
		step.setAttribute(DataLoaderAttrEnum.ATTR_SOURCE_VALUES, Collections.singletonMap("PAYMENT_SCHEDULE_ID", true));
		MessageChainService.publish(mc, publisher);
	}
	
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
