package com.usatech.report.test;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import simple.io.IOUtils;

public class TestServlet extends GenericServlet {

	public TestServlet() {
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		log("Got request '" + ((HttpServletRequest) req).getRequestURI() + "' with params " + req.getParameterMap());
		Reader reader = req.getReader();
		String content = IOUtils.readFully(reader);
		log("with content:\n" + content);
		Writer writer = res.getWriter();
		writer.append("Received file. Thank you.");
		writer.flush();
	}

}
