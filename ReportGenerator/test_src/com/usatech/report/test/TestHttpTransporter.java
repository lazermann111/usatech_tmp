package com.usatech.report.test;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.io.resource.ByteArrayResource;
import simple.io.resource.Resource;
import simple.test.UnitTest;

import com.usatech.transport.RESTTransporter;
import com.usatech.transport.TransportReason;

public class TestHttpTransporter extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	
	@Test
	public void testRestTransporter() throws Exception {
		RESTTransporter t=new RESTTransporter();
		HashMap<String, String> transportProperties=new HashMap<String, String>();
		transportProperties.put("URL", "https://qas-onedatatransfer.allinonesys.com/Settlement");
		transportProperties.put("USERNAME", "compass-usa\\VSA_USAT");
		transportProperties.put("PASSWORD", "T0m@tI110_S@1s@");
		TransportReason transportReason=TransportReason.TEST;
		Resource resource=new ByteArrayResource("This is a test for the transport.".getBytes(), "testTransportKey", "testTransportFileName");
		HashMap<String, String> messageParameters=new HashMap<String, String>();
		t.transport(transportProperties, transportReason, resource, messageParameters);
	}

	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
