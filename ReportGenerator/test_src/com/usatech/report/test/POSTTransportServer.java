/*
 * Copyright 2015 USA Technologies, Inc. All rights reserved.
 * The contents of this file are subject to the terms of USA Technologies Software Tool License Agreement. See LICENSE.rtf.
 * 
 */
package com.usatech.report.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.SSLContext;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsServer;

public class POSTTransportServer {
	protected int backlog = 10;
	protected int port = 443;
	protected String context = "/";
	protected String directory = "./reports/";
	protected HttpsServer server;
	protected final Decoder base64Decoder = Base64.getDecoder();
	protected int bufferSize = 1024;
	protected final HttpsConfigurator httpsConfigurator;
	protected final Map<String, byte[][]> accessMap = new LinkedHashMap<>(); // stores username=[hash,salt]
	protected final String HASH_ALG = "SHA-256";

	public POSTTransportServer() throws NoSuchAlgorithmException {
		SSLContext sslContext = SSLContext.getDefault();
		httpsConfigurator = new HttpsConfigurator(sslContext);
	}

	/**
	 * This is where the real work is done to handle incoming requests
	 * 
	 * @throws IOException
	 */
	protected void handleExchange(HttpExchange exchange) throws IOException {
		// Authenticate
		log("Received " + exchange.getRequestMethod() + " request for " + exchange.getRequestURI());
		String authorization = exchange.getRequestHeaders().getFirst("Authorization");
		if(authorization == null) {
			exchange.getResponseHeaders().set("WWW-Authenticate", "Basic realm=\"Reports\"");
			sendResponse(exchange, 401, "Please include the 'Authorization' header with Basic Authorization");
			return;
		}
		if(!authorization.startsWith("Basic ")) {
			exchange.getResponseHeaders().set("WWW-Authenticate", "Basic realm=\"Reports\"");
			sendResponse(exchange, 401, "Only Basic Authorization is supported");
			return;
		}

		String credential;
		try {
			credential = new String(base64Decoder.decode(authorization.substring(6)));
		} catch(IllegalArgumentException e) {
			exchange.getResponseHeaders().set("WWW-Authenticate", "Basic realm=\"Reports\"");
			sendResponse(exchange, 401, "Credential for Basic Authorization was not Base 64 encoded");
			return;
		}
		int pos = credential.indexOf(':');
		if(pos < 0) {
			exchange.getResponseHeaders().set("WWW-Authenticate", "Basic realm=\"Reports\"");
			sendResponse(exchange, 401, "Invalid format of credential data for Basic Authorization. Please use 'username:password'");
			return;
		}
		String username = credential.substring(0, pos);
		String password = credential.substring(pos + 1);
		if(!validateCredentials(username, password)) {
			exchange.getResponseHeaders().set("WWW-Authenticate", "Basic realm=\"Reports\"");
			sendResponse(exchange, 403, "Credential not valid");
			return;
		}
		if(!"POST".equalsIgnoreCase(exchange.getRequestMethod())) {
			exchange.getResponseHeaders().set("Allow", "POST");
			sendResponse(exchange, 405, "Only POST is allowed");
			return;
		}
		// Check parameters
		Map<String, String[]> parameters = getParameterMap(exchange.getRequestURI().getRawQuery());

		String[] reason = parameters.get("reason");
		if(reason == null || reason.length == 0 || reason[0] == null || reason[0].trim().isEmpty()) {
			sendResponse(exchange, 404, "The 'reason' parameter was not provided in the url");
			return;
		}

		if("TEST".equalsIgnoreCase(reason[0].trim())) {
			sendResponse(exchange, 200, "Test Succeeded");
			return;
		}

		// Determine file name
		String path = exchange.getRequestURI().getPath();
		String context = getContext();
		if(path.startsWith(context)) {
			pos = context.length();
			while(pos < path.length() && (path.charAt(pos) == '/' || path.charAt(pos) == '\\'))
				pos++;
			path = path.substring(pos);
		} else {
			pos = Math.max(path.lastIndexOf('/'), path.lastIndexOf('\\'));
			path = path.substring(pos + 1);
		}

		// Ensure path is safe
		if(path.contains("..")) {
			sendResponse(exchange, 404, "The url path may not contain '..'");
			return;
		}

		// Write contents to file
		byte[] buffer = new byte[bufferSize];
		File file = new File(directory, path);
		long tot = 0;
		try {
			FileOutputStream out = new FileOutputStream(file);
			try {
				InputStream in = exchange.getRequestBody();
				for(int len = 0; (len = in.read(buffer)) >= 0; tot += len)
					out.write(buffer, 0, len);
				out.flush();
			} finally {
				out.close();
			}
		} catch(IOException e) {
			log("Could not write file to '" + file.getAbsolutePath() + "'. You may need to create directories or grant permissions");
			sendResponse(exchange, 500, "Could not write file");
			return;
		}
		sendResponse(exchange, 200, "Received file of " + tot + " bytes");
	}

	protected boolean validateCredentials(String username, String password) {
		byte[][] passwordHashAndSalt = accessMap.get(username);
		if(passwordHashAndSalt == null) {
			log("User '" + username + "' does not have a credential configured for it");
			return false;
		}
		if(passwordHashAndSalt.length == 0) {
			log("User '" + username + "' authenticated without password");
			return true;
		}
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance(HASH_ALG);
		} catch(NoSuchAlgorithmException e) {
			log("Could not create MessageDigest for algorithm '" + HASH_ALG + "'");
			return false;
		}
		digest.reset();
		if(passwordHashAndSalt.length > 1 && passwordHashAndSalt[1] != null && passwordHashAndSalt[1].length > 0)
			digest.update(passwordHashAndSalt[1]);
		byte[] hashed = digest.digest(password.getBytes());
		if(Arrays.equals(passwordHashAndSalt[0], hashed)) {
			log("User '" + username + "' authenticated with password");
			return true;
		}
		log("User '" + username + "' provided invalid password");
		return false;
	}

	protected void setCredential(String username, String hashedPasswordBase64) throws IllegalArgumentException {
		byte[][] passwordHashAndSalt;
		if(hashedPasswordBase64 == null || (hashedPasswordBase64 = hashedPasswordBase64.trim()).isEmpty())
			passwordHashAndSalt = new byte[0][];
		else {
			int pos = hashedPasswordBase64.indexOf(',');
			if(pos < 0)
				passwordHashAndSalt = new byte[][] { base64Decoder.decode(hashedPasswordBase64) };
			else
				passwordHashAndSalt = new byte[][] { base64Decoder.decode(hashedPasswordBase64.substring(0, pos).trim()), base64Decoder.decode(hashedPasswordBase64.substring(pos + 1).trim()) };
		}
		accessMap.put(username, passwordHashAndSalt);
	}

	protected void sendResponse(HttpExchange exchange, int responseCode, String message) throws IOException {
		log("Sending response " + responseCode + ": " + message);
		exchange.sendResponseHeaders(responseCode, message.length());
		OutputStream out = exchange.getResponseBody();
		out.write(message.getBytes());
		out.close();
	}

	protected Map<String, String[]> getParameterMap(String rawQuery) throws IOException {
		Map<String, String[]> parameters = new LinkedHashMap<String, String[]>();
		decodeURL(rawQuery, parameters);
		return parameters;
	}

	protected void decodeURL(String urlEncodedData, Map<String, String[]> parameters) throws IOException {
		if(urlEncodedData != null) {
			int prev = 0;
			while(prev < urlEncodedData.length()) {
				int pos = urlEncodedData.indexOf('=', prev);
				if(pos < 0)
					throw new IOException("Invalid url-encoded data");
				String name = URLDecoder.decode(urlEncodedData.substring(prev, pos), "UTF-8");
				prev = pos + 1;
				String value;
				if(prev < urlEncodedData.length()) {
					pos = urlEncodedData.indexOf('&', prev);
					if(pos < 0)
						pos = urlEncodedData.length();
					value = URLDecoder.decode(urlEncodedData.substring(prev, pos), "UTF-8");
					prev = pos + 1;
				} else
					value = "";
				String[] old = parameters.get(name);
				String[] values;
				if(old == null || old.length == 0)
					values = new String[] { value };
				else {
					values = new String[old.length + 1];
					System.arraycopy(old, 0, values, 0, old.length);
					values[old.length] = value;
				}
				parameters.put(name, values);
			}
		}
	}

	public synchronized boolean startServer() throws IOException {
		if(server != null)
			return false;
		log("Starting POST Server on port " + port + "...");
		server = HttpsServer.create(new InetSocketAddress(port), backlog);
		server.createContext(context, new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				handleExchange(exchange);
			}
		});
		server.setHttpsConfigurator(httpsConfigurator);
		server.start();
		log("POST Server started");
		return true;
	}

	public synchronized boolean stopServer(int maxWaitSeconds) {
		if(server == null)
			return false;
		server.stop(maxWaitSeconds);
		return true;
	}

	public synchronized boolean isRunning() {
		return server != null;
	}

	public synchronized int getBacklog() {
		return backlog;
	}

	public synchronized void setBacklog(int backlog) {
		this.backlog = backlog;
	}

	public synchronized int getPort() {
		return port;
	}

	public synchronized void setPort(int port) throws IOException {
		int old = this.port;
		this.port = port;
		if(server != null && old != port) {
			stopServer(5);
			startServer();
		}
	}

	public synchronized String getContext() {
		return context;
	}

	public synchronized void setContext(String context) {
		this.context = context;
		if(context == null || (context = context.trim()).isEmpty())
			context = "/";
		String old = this.context;
		this.context = context;
		if(server != null && !old.equals(context)) {
			server.removeContext(old);
			server.createContext(context, new HttpHandler() {
				@Override
				public void handle(HttpExchange exchange) throws IOException {
					handleExchange(exchange);
				}
			});
		}
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		if(directory == null || (directory=directory.trim()).isEmpty())
			directory = "./reports/";
		else if(!directory.endsWith("/"))
			directory = directory + "/";
		this.directory = directory;
	}

	protected final ThreadLocal<DateFormat> logDateFormat = new ThreadLocal<DateFormat>() {
		protected DateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		}
	};

	protected void log(String message) {
		System.out.print(logDateFormat.get().format(new Date()));
		System.out.print(' ');
		System.out.println(message);
	}

	public static void main(String[] args) {
		String ks = System.getProperty("javax.net.ssl.keyStore");
		if(ks == null || ks.trim().isEmpty()) {
			System.err.println("WARNING: A keystore was not specified. The server will not be able to find a certificate to use for ssl. Please specify -Djavax.net.ssl.keyStore=<location of keystore> on the commandline to tell java where to find the keystore");
		} else if(!new File(ks).canRead()) {
			System.err.println("WARNING: The specified keystore file '" + ks + "' is not readable. The server will not be able to find a certificate to use for ssl. Please specify a valid keystore file using -Djavax.net.ssl.keyStore=<location of keystore> on the commandline");
		}
		POSTTransportServer server;
		try {
			server = new POSTTransportServer();
		} catch(NoSuchAlgorithmException e) {
			printUsageAndExit("Could not setup an ssl server: " + e.getMessage());
			return;
		}
		for(int i = 0; i < args.length; i += 2) {
			if(args[i] == null || args[i].isEmpty()) {
				printUsageAndExit("Argument " + (i + 1) + " is blank");
				return;
			}
			if(i + 1 >= args.length) {
				printUsageAndExit("Not enough arguments");
				return;
			}
			if(args[i + 1] == null || args[i + 1].isEmpty()) {
				printUsageAndExit("Argument " + (i + 2) + " is blank");
				return;
			}
			switch(args[i].trim()) {
				case "-p":
				case "--port":
					int port;
					try {
						port = Integer.parseInt(args[i + 1].trim());
					} catch(NumberFormatException e) {
						printUsageAndExit("Port value '" + args[i + 1].trim() + "' is not a valid number");
						return;
					}
					try {
						server.setPort(port);
					} catch(IOException e) {
						printUsageAndExit("Could not set port  to '" + port + "': " + e.getMessage());
						return;
					}
					break;
				case "-b":
				case "--backlog":
					int backlog;
					try {
						backlog = Integer.parseInt(args[i + 1].trim());
					} catch(NumberFormatException e) {
						printUsageAndExit("Backlog value '" + args[i + 1].trim() + "' is not a valid number");
						return;
					}
					server.setBacklog(backlog);
					break;
				case "-d":
				case "--directory":
					server.setDirectory(args[i + 1].trim());
					break;
				case "-c":
				case "--context":
					server.setContext(args[i + 1].trim());
					break;
				case "-a":
				case "--access-file":
					Properties p = new Properties();
					try {
						p.load(new FileReader(args[i + 1].trim()));
					} catch(IOException e) {
						printUsageAndExit("Could not load credential file " + args[i + 1].trim() + ": " + e.getMessage());
						return;
					}
					for(Map.Entry<Object, Object> entry : p.entrySet())
						try {
							server.setCredential((String) entry.getKey(), (String) entry.getValue());
						} catch(IllegalArgumentException e) {
							System.err.println("Could not load hashed Base 64 password for user '" + (String) entry.getKey() + "': " + e.getMessage());
						}
					server.log("Loaded " + p.size() + " credentials");
					break;
				default:
					printUsageAndExit("Option '" + args[i].trim() + "' is not recognized");
					return;
			}
		}
		try {
			server.startServer();
		} catch(IOException e) {
			printUsageAndExit("Could not start server on port " + server.getPort() + ": " + e.getMessage());
			return;
		}
		synchronized(server) {
			try {
				server.wait();
			} catch(InterruptedException e) {
				// ignore
			}
		}
	}

	protected static void printUsageAndExit(String error) {
		if(error != null && !error.trim().isEmpty())
			System.err.println(error);
		System.err.println("This commands starts up a new server that listens on a port for https traffic and saves the stream as a file. Use the following arguments:");
		System.err.println("\t-p\tPort on which to listen (443 by default)");
		System.err.println("\t-b\tNumber of backlog requests to allow (10 by default)");
		System.err.println("\t-d\tDirectory into which files are stored ('./reports/' by default)");
		System.err.println("\t-c\tContext (server path) that requests must match ('/' by default)");
		System.err.println("\t-a\tAccess File that lists valid user names and base 64 of the SHA-256 hash and salt of the password separated by a comma (,). Each line in the file should be '<username>=<base 64 of SHA-256 hash of password>,<base 64 of the salt used>'");
	}
}
