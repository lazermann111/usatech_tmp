package com.usatech.report;

import java.io.Serializable;

public class TestObj implements Serializable {
	private static final long serialVersionUID = 610310609811L;
	protected String name;
	public TestObj(String name){
		this.name=name;
	}
	public String toString(){
		return name;
	}
}