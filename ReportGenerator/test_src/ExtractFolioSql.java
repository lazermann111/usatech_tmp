import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.standard.StandardDesignEngine2;
import simple.falcon.engine.standard.StandardExecuteEngine;
import simple.io.LoadingException;
import simple.results.Results;
import simple.text.StringUtils;

public class ExtractFolioSql extends BaseWithConfig {
	public static void main(String[] args) {
		new ExtractFolioSql().run(args);
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
		registerCommandLineArgument(false, "directory", "The output directory");
		registerCommandLineArgument(false, "userGroupIds", "The comma-separated list of user groups");
	}

	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		try {
			BaseWithConfig.configureDataSourceFactory(config, null);
		} catch(ServiceException e) {
			finishAndExit("Could not configure data source factory", 1, true, e);
			return;
		}
		long[] userGroupIds;
		try {
			userGroupIds = ConvertUtils.convert(long[].class, argMap.get("userGroupIds"));
		} catch(ConvertException e) {
			finishAndExit("Could not convert user group ids to array of longs", 2, true, e);
			return;
		}
		File directory = new File(ConvertUtils.getStringSafely(argMap.get("directory"), "~/folio_sql_" + System.currentTimeMillis() + "_" + Arrays.toString(userGroupIds).replace(", ", "-")));
		directory.mkdir();
		try {
			ConfigLoader.loadConfig("simple/falcon/engine/standard/design-data-layer.xml");
		} catch(ConfigException e) {
			finishAndExit("Could not setup design data layer", 3, true, e);
			return;
		} catch(IOException e) {
			finishAndExit("Could not setup design data layer", 3, true, e);
			return;
		} catch(LoadingException e) {
			finishAndExit("Could not setup design data layer", 3, true, e);
			return;
		}
		StandardDesignEngine2 sde = new StandardDesignEngine2();
		ExecuteEngine ee = new StandardExecuteEngine(sde);
		String dsn = config.getString("simple.app.Service.generate_folio.messageChainTask.designDataSourceName", "REPORT");
		ee.setDesignDataSourceName(dsn);
		try {
			ee.setDefaultDataSourceName(dsn);
			ee.setDataSourceFactory(DataLayerMgr.getGlobalDataLayer().getDataSourceFactory());
			Results results = DataLayerMgr.executeSQL(ee.getDesignDataSourceName(), "SELECT FOLIO_ID folioId FROM FOLIO_CONF.FOLIO ORDER BY FOLIO_ID", null, null);
			while(results.next()) {
				long folioId = results.getValue("folioId", long.class);
				System.out.println("Extracting SQL for Folio " + folioId + ": -----------------------");
				Folio folio = ee.getDesignEngine().getFolio(folioId);
				FileOutputStream out = new FileOutputStream(new File(directory, "folio-" + folioId + ".sql"));
				try {
					Call call = ee.getDesignEngine().getCall(folio, userGroupIds);
					out.write(call.getSql().getBytes());
					System.out.println("SQL for Folio " + folioId + ":\n" + call.getSql());
				} catch(DesignException e) {
					out.write(StringUtils.exceptionToString(e).getBytes());
					System.out.println("Could not get call for folio " + folioId);
					e.printStackTrace(System.out);
				} finally {
					out.close();
				}
			}
		} catch(IOException e) {
			finishAndExit("Could not extract folio sql", 5, true, e);
			return;
		} catch(DesignException e) {
			finishAndExit("Could not extract folio sql", 5, true, e);
			return;
		} catch(SQLException e) {
			finishAndExit("Could not extract folio sql", 5, true, e);
			return;
		} catch(DataLayerException e) {
			finishAndExit("Could not extract folio sql", 5, true, e);
			return;
		} catch(ConvertException e) {
			finishAndExit("Could not extract folio sql", 5, true, e);
			return;
		}
	}

}
