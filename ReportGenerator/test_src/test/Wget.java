package test;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import simple.io.Base64;
import simple.text.StringUtils;

public class Wget {
	protected static class EnhancedRequestBuilder {
		protected RequestBuilder req;
		protected MultipartEntityBuilder mpe;
		protected List<NameValuePair> parameters;
		protected List<Header> headers;

		public EnhancedRequestBuilder setUri(String uri) {
			if(req == null)
				req = RequestBuilder.get();
			req.setUri(uri);
			return this;
		}

		/**
		 * Sets the method.
		 * 
		 * @param method
		 * @return
		 */
		public EnhancedRequestBuilder setMethod(String method) {
			if(req == null)
				req = RequestBuilder.create(method);
			else if(!req.getMethod().equalsIgnoreCase(method)) {
				RequestBuilder newReq = RequestBuilder.create(method);
				newReq.setUri(req.getUri());
				req = newReq;
			}
			return this;
		}

		public EnhancedRequestBuilder addHeader(final String name, final String value) {
			if(headers == null)
				headers = new ArrayList<Header>();
			headers.add(new BasicHeader(name, value));
			return this;
		}

		public EnhancedRequestBuilder addParameter(String name, File file) {
			if(mpe == null)
				mpe = MultipartEntityBuilder.create();
			mpe.addPart(name, new FileBody(file));
			return this;
		}

		public EnhancedRequestBuilder addParameter(String name, String value) {
			if(parameters == null)
				parameters = new ArrayList<NameValuePair>();
			parameters.add(new BasicNameValuePair(name, value));
			return this;
		}

		public HttpUriRequest build() {
			if(req == null)
				req = RequestBuilder.get();
			applyHeaders(req);
			if(mpe == null) {
				if(parameters != null)
					for(NameValuePair nvp : parameters)
						req.addParameter(nvp);
			} else {
				if(parameters != null)
					for(NameValuePair nvp : parameters)
						mpe.addPart(nvp.getName(), new StringBody(nvp.getValue(), ContentType.TEXT_PLAIN));
				req.setEntity(mpe.build());
			}
			return req.build();
		}

		public RequestBuilder applyHeaders(RequestBuilder req) {
			if(headers != null)
				for(Header header : headers)
					req.addHeader(header);
			return req;
		}
	}
	public static void main(String[] args) throws IOException {
		final EnhancedRequestBuilder req = new EnhancedRequestBuilder();
		switch(args.length) {
			case 0:
				printUsage();
				return;
			case 1:
				int pos = args[0].indexOf('?');
				if(pos >= 0) {
					req.setUri(args[0].substring(0, pos));
					if(!parseQueryPart(req, args[0].substring(pos + 1)))
						return;
				} else {
					req.setUri(args[0]);
				}
				break;
			default:
				int offset;
				switch(args[0].toUpperCase()) {
					case HttpPost.METHOD_NAME:
					case HttpPut.METHOD_NAME:
					case HttpGet.METHOD_NAME:
					case HttpTrace.METHOD_NAME:
					case HttpPatch.METHOD_NAME:
					case HttpOptions.METHOD_NAME:
					case HttpDelete.METHOD_NAME:
					case HttpHead.METHOD_NAME:
						req.setMethod(args[0].toUpperCase());
						offset = 1;
						break;
					default:
						offset = 0;
				}
				pos = args[offset].indexOf('?');
				if(pos >= offset) {
					req.setUri(args[offset].substring(0, pos));
					if(!parseQueryPart(req, args[offset].substring(pos + 1)))
						return;
				} else {
					req.setUri(args[offset]);
				}
				for(int i = offset + 1; i < args.length; i++)
					if(!parseQueryPart(req, args[i]))
						return;
		}
		HttpUriRequest httpMethod = req.build();
		log("Sending " + httpMethod.getMethod() + " Request for '" + httpMethod.getURI() + "'");
		int returnCode;
		String responseBody;
		try (CloseableHttpClient httpClient = HttpClients.custom().build()) {
			while(true)
				try (CloseableHttpResponse response = httpClient.execute(httpMethod)) {
					returnCode = response.getStatusLine().getStatusCode();
					System.out.println(returnCode);
					switch(returnCode) {
						case 302: // redirect
							Header next = response.getFirstHeader("Location");
							if(next != null) {
								response.close();
								httpMethod = req.applyHeaders(RequestBuilder.get(next.getValue())).build();
								System.out.println("---");
								continue;
							}
					}
					responseBody = copyContent(response.getEntity(), System.out, 200, 1024);
					System.out.println();
					break;
				}
		}
		log("Response = " + returnCode + ": " + responseBody);
	}

	protected static String copyContent(HttpEntity httpEntity, PrintStream copyTo, int summarySize, int bufferSize) throws IOException {
		if(httpEntity == null)
			return "";
		Charset charset;
		try {
			final ContentType contentType = ContentType.get(httpEntity);
			if(contentType != null)
				charset = contentType.getCharset();
			else
				charset = HTTP.DEF_CONTENT_CHARSET;
		} catch(final UnsupportedCharsetException ex) {
			charset = HTTP.DEF_CONTENT_CHARSET;
		}
		if(charset == null) {
			charset = HTTP.DEF_CONTENT_CHARSET;
		}
		if(summarySize > bufferSize)
			bufferSize = summarySize;
		int len;
		if(httpEntity.getContentLength() > bufferSize || httpEntity.getContentLength() < 0)
			len = bufferSize;
		else
			len = (int) httpEntity.getContentLength();
		long total = 0L;
		CharBuffer content = CharBuffer.allocate(len);
		StringBuilder summary = new StringBuilder(summarySize);
		try (Reader in = new InputStreamReader(httpEntity.getContent(), charset)) {
			int zeros = 0;
			while((len = in.read(content)) >= 0) {
				if(len == 0) {
					if(zeros++ > 20)
						break;
					continue;
				}
				zeros = 0;
				total += len;
				content.flip();
				copyTo.append(content);
				content.position(0);
				int need = summarySize - summary.length();
				if(need > 0) {
					if(content.limit() > need)
						content.limit(need);
					summary.append(content);
				}
				content.clear();
			}
		} catch(UnsupportedOperationException e) {
			return "";
		}
		if(total > summarySize)
			summary.append("...");
		summary.append(" (").append(total).append(" bytes)");
		return new StringBuilder(summary.length() + 20).append(summary).toString();
	}

	protected static boolean parseQueryPart(EnhancedRequestBuilder req, String queryPart) throws UnsupportedEncodingException {
		String[] params = StringUtils.split(queryPart, '&');
		String authUser = null;
		String authPass = null;
        for(String param : params) {
        	int pos = param.indexOf('=');
        	if(pos < 0)
        		continue;
        	String value = URLDecoder.decode(param.substring(pos+1), "UTF-8");
        	int sep = param.indexOf(':');
        	if(sep >= 0 && sep < pos) {
				String prefix = param.substring(0, sep);
				String name = URLDecoder.decode(param.substring(sep + 1, pos), "UTF-8");
				switch(prefix) {
        			case "HEADER":
						req.addHeader(name, value);
        				break;
        			case "AUTH":
						switch(name.toLowerCase()) {
        					case "user": case "username":
        						authUser = value;
        						break;
        					case "pass": case "password":
        						authPass = value;
        						break; 
        					default:
								log("ERROR: Parameter 'AUTH:" + name + "' is not recognized; ignoring");
        				}
        				break;
					case "COOKIE":
						// TODO: add cookie
						break;
					case "FILE":
						File file = new File(value);
						if(!file.canRead()) {
							log("ERROR: File '" + file.getAbsolutePath() + "' is not readable; exiting");
							return false;
						}
						req.addParameter(name, file);
						break;
					default:
						log("ERROR: Parameter prefix '" + prefix + "' is not recognized; ignoring");
        		}
			} else {
				String name = URLDecoder.decode(param.substring(0, pos), "UTF-8");
				req.addParameter(name, value);
        	}
        }
		if(authUser != null && authPass != null) {
			String base64Token = Base64.encodeString(authUser + ":" + authPass, true);
			req.addHeader("Authorization", new StringBuilder("Basic ").append(base64Token).toString());
		}
		return true;
	}

	protected static void log(String msg) {
		System.err.println(msg);
	}

	protected static void printUsage() {
		System.err.println("Arguments: (METHOD) url");
	}
}
