/*
 * Created on Feb 8, 2006
 *
 */
package test;

import java.io.ByteArrayInputStream;
import java.io.Console;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.Vector;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.configuration.Configuration;

import com.usatech.layers.common.constants.ConsumerCommType;
import com.usatech.report.DexInputStream;
import com.usatech.report.Frequency;
import com.usatech.report.Frequency.DateRange;
import com.usatech.report.FrequencyManager;
import com.usatech.report.Generator;
import com.usatech.report.ReportInstance;
import com.usatech.report.ReportScheduleType;
import com.usatech.report.rest.CreditData;
import com.usatech.report.rest.CreditTran;
import com.usatech.report.rest.EFT;
import com.usatech.report.rest.EFTData;
import com.usatech.report.rest.EFTTerminal;
import com.usatech.report.rest.EFTTran;
import com.usatech.transport.RESTTransporter;
import com.usatech.transport.SecureFTPGanymedTransporter;
import com.usatech.transport.SecureFTPTransporter;
import com.usatech.transport.TransportErrorType;
import com.usatech.transport.TransportException;
import com.usatech.transport.TransportReason;
import com.usatech.transport.Transporter;
import com.usatech.transport.soap.vdi.VDIDexUploadInput;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SFTPException;
import ch.ethz.ssh2.SFTPv3Client;
import ch.ethz.ssh2.SFTPv3DirectoryEntry;
import ftp.FtpBean;
import ftp.FtpListResult;
import simple.app.Base;
import simple.app.BaseWithConfig;
import simple.app.MainWithConfig;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.db.config.ConfigLoader;
import simple.io.App;
import simple.io.ConfigSource;
import simple.io.ConsoleInteraction;
import simple.io.IOUtils;
import simple.io.InOutInteraction;
import simple.io.Interaction;
import simple.io.LoadingException;
import simple.io.Log;
import simple.io.resource.ByteArrayResource;
import simple.io.resource.FileResource;
import simple.io.resource.Resource;
import simple.results.Results;
import simple.servlet.Interceptor;
import simple.servlet.JspRequestDispatcherFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.xml.TemplatesLoader;
import simple.xml.sax.BeanAdapter1;
import simple.xml.sax.BeanInputSource;

public class ReportGeneratorTest {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        System.setProperty("simple.io.logging.bridge", "simple.io.logging.SimpleBridge");
		// testFreqs();
        //testDailyFreqs();
        //testSFTP();
        //testDexInputStream();
        //testDailyFreqs();
		// testVDITemplate();
		//testJspTemplate();
        //testCreditTran();
        //testCreditTranCount();
        //testCreditTranReport();
        //testEFTTran();
        //testEnum();
        //testEFTSplit();
        //testEFTTran();
        //testConsumer();
        //testPrepaidPurchase();
        //testSMSConsumer();
        testCreditTran();
        Log.finish();
    }
    protected static void testSMSConsumer() throws Exception {
    	Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/rgg-data-layer.xml"));
        ConsumerCommType consumerCommType=ConsumerCommType.CASH_BACK_ON_PURCHASE;
        Map<String, Object> params=new HashMap<String, Object>();
        params.put("consumerAcctId", 140161);
        params.put("communicationType", consumerCommType);
        params.put("cashBackAmount", 10);
        params.put("purchaseAmount", 100);
        params.put("replenishCardMasked", "0090909090909090909");
        params.put("deniedCount", "4");
        params.put("baseUrl", "localhost:8580/");
        DataLayerMgr.selectInto("GET_CONSUMER_COMM_DETAILS", params, params, true);
        int preferredCommType=ConvertUtils.getInt(params.get("preferredCommType"));
        Translator translator = TranslatorFactory.getDefaultFactory().getTranslator(Locale.ENGLISH, "localhost");
        String body;
        body=translator.translate("consumer-comm-cashback-sms-body", "You earned a {0.cashBackAmount,CURRENCY} bonus cash on a purchase of {0.purchaseAmount,CURRENCY}. See {1}settings.html to change settings.", params, params.get("baseUrl"));
        System.out.println("------------");
		System.out.println(body);
		System.out.println("------------");
		body=translator.translate("consumer-comm-replenishbonus-sms-body", "You earned a {0.cashBackAmount,CURRENCY} replenish bonus because of adding {0.purchaseAmount,CURRENCY} to your account. See {1}settings.html to change settings.", params, params.get("baseUrl"));
		System.out.println("------------");
		System.out.println(body);
		System.out.println("------------");
		body=translator.translate("consumer-comm-replenishdisabled-sms-body", "Your credit card {0.replenishCardMasked} has been denied {0.deniedCount} times while replenishing your MORE card. See {1}cards.html to change. See {1}settings.html to change settings.", params, params.get("baseUrl"));
		System.out.println("------------");
		System.out.println(body);
		System.out.println("------------");
    }
    protected static void testPrepaidPurchase() throws Exception {
    	Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/rgg-data-layer.xml"));
        
        Results results = DataLayerMgr.executeQuery("GET_PREPAID_PURCHASE", new Object[] { 1000009871283L}, false);
        Translator translator = TranslatorFactory.getDefaultFactory().getTranslator(Locale.ENGLISH, "localhost");
        if(results.next()) {
        	String body=translator.translate("consumer-comm-purchase-sms-body","Time: {0, DATE,MM/dd/yy @ h:mma z}\nOperator: {1}\nLocation: {2}\nDevice: {3}\nCard: {4}\nTransaction Id: {5}\nItems: {6}\nAmount: {7,CURRENCY}\nProfile: {8,URL}\n", 
					results.get("date"),
					results.get("operatorName"),
					results.get("location"),
					results.get("deviceSerialCd"),
					results.get("cardNum"),
					results.get("tranDeviceTranCd"),
					results.get("vendColumn"),
					results.get("totalAmount"),
					"localhost:8580/settings.html");
			System.out.println(body);
		}
    }
    protected static void testEFTSplit() throws Exception {
    	Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/rgg-data-layer.xml"));
        //int docId=8563;
        int docId=8803;
        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("eftId", docId);
        DataLayerMgr.executeCall("PROCESS_EFT", parameters, true);
    }
    
    protected static void testEnum() throws Exception{
    	ReportScheduleType scheduleType=ReportScheduleType.PAYMENT;
    	Generator generator=Generator.EFT_XML;
    	if(scheduleType.equals(ReportScheduleType.PAYMENT) && generator!=null&&generator.equals(Generator.EFT_XML)){
			System.out.println("yes");
		}
    }
    protected static void testEFTTran() throws Exception {
    	Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/rgg-data-layer.xml"));
        //int docId=8563;
        int docId=6800;
        
        ArrayList<Integer> list=new ArrayList<Integer>();
        list.add(63839);
        Results results = DataLayerMgr.executeQuery("GET_PAYMENT_BATCHES", new Object[] { docId }, false);
        EFT eft=new EFT();
        BigDecimal settledAmount=new BigDecimal("0.00");
        BigDecimal failedAmount=new BigDecimal("0.00");
        BigDecimal feeAmount=new BigDecimal("0.00");
        BigDecimal chargebackAmount=new BigDecimal("0.00");
        BigDecimal refundAmount=new BigDecimal("0.00");
        BigDecimal adjustmentAmount=new BigDecimal("0.00");
        BigDecimal netAmount=new BigDecimal("0.00");
        int customerId=-1;
        Date paymentDate=null;
		while(results.next()) {
			EFTTerminal t=new EFTTerminal();
			if(results.get("settledAmount")!=null){
				settledAmount=settledAmount.add(results.getValue("settledAmount", BigDecimal.class));
			}
			if(results.get("failedAmount")!=null){
				BigDecimal tFailedAmount=results.getValue("failedAmount", BigDecimal.class);
				t.setFailedAmount(tFailedAmount);
				failedAmount=failedAmount.add(tFailedAmount);
				
			}
			if(results.get("feeAmount")!=null){
				BigDecimal tFeeAmount=results.getValue("feeAmount", BigDecimal.class);
				t.setFeeAmount(tFeeAmount);
				feeAmount=feeAmount.add(tFeeAmount);
			}
			if(results.get("chargebackAmount")!=null){
				BigDecimal tChargebackAmount=results.getValue("chargebackAmount", BigDecimal.class);
				t.setChargebackAmount(tChargebackAmount);
				chargebackAmount=chargebackAmount.add(tChargebackAmount);
			}
			if(results.get("refundAmount")!=null){
				BigDecimal tRefundAmount=results.getValue("refundAmount", BigDecimal.class);
				t.setRefundAmount(tRefundAmount);
				refundAmount=refundAmount.add(tRefundAmount);
			}
			if(results.get("adjustmentAmount")!=null){
				BigDecimal tAdjustmentAmount=results.getValue("adjustmentAmount", BigDecimal.class);
				t.setAdjustmentAmount(tAdjustmentAmount);
				adjustmentAmount=adjustmentAmount.add(tAdjustmentAmount);
			}
			if(results.get("netAmount")!=null){
				netAmount=netAmount.add(results.getValue("netAmount", BigDecimal.class));
			}
			eft.addEFTTerminal(t);
			if(customerId==-1){
				customerId=results.getValue("customerCode", int.class);
			}
			if(paymentDate==null){
				paymentDate=results.getValue("paymentDate", Date.class);
			}
			String termnalCode=ConvertUtils.getStringSafely(results.getValue("terminalCode"));
			System.out.println(results.getValue("paymentBatchIds"));
			Results tranResults = DataLayerMgr.executeQuery("GET_PAYMENT_TRAN", new Object[] { docId, results.getValue("paymentBatchIds") }, false);
			while(tranResults.next()){
				EFTTran tran=new EFTTran();
				String machineTransNo=ConvertUtils.getStringSafely(tranResults.getValue("txnCode"));
				tran.setTxnAmount(tranResults.getValue("txnAmount", BigDecimal.class));
				if(machineTransNo!=null&&machineTransNo.lastIndexOf(":")>0){
					tran.setTxnCode(machineTransNo.substring(machineTransNo.lastIndexOf(":")+1));
				}
				t.addEFTTran(tran);
			}
		} 
		eft.setAdjustmentAmount(adjustmentAmount);
		eft.setChargebackAmount(chargebackAmount);
		eft.setFailedAmount(failedAmount);
		eft.setFeeAmount(feeAmount);
		eft.setRefundAmount(refundAmount);
		eft.setCustomerCode(customerId);
		eft.setNetAmount(netAmount);
		eft.setPaymentCode(docId);
		eft.setPaymentDate(paymentDate);
		eft.setSettledAmount(settledAmount);
		
		EFTData data=new EFTData();
		data.addEFT(eft);
		data.setVersion("1.0");
		//String eftXml=EFTData.toXML(data);
		//System.out.println(eftXml);
		RESTTransporter transport=new RESTTransporter();
		//transport.testRest(eftXml);
    }
    protected static void testConsumer() throws Exception {
    	Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/test-data-layer.xml"));
        
        Results results = DataLayerMgr.executeQuery("GET_CONSUMER_ACCT_DETAILS", new Object[] { 1001694200, "639621*********6721","639621*********6721" }, false);
		while(results.next()) {
			System.out.println("sendReceipt="+results.get("sendReceipt"));
			System.out.println("mobileNumber="+results.get("mobileNumber"));
			System.out.println("mobileEmail="+results.get("mobileEmail"));
			System.out.println("commMethod="+results.get("commMethod"));

		} 
    }
    protected static void testCreditTranReport() throws Exception {
    	Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/rgr-data-layer.xml"));
        Date now=new Date();
        Results results = DataLayerMgr.executeQuery("GET_UNSENT_CREDIT_XML_REPORT", new Object[] { now, 20232 }, false);
		while(results.next()) {
			ReportInstance rpt = new ReportInstance();
			results.fillBean(rpt);
			System.out.println(rpt);
		} 
    }

    protected static void testCreditTran() throws Exception {
    	Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/rgg-data-layer.xml"));
        long tranId=1000009871885L;
        Results results = DataLayerMgr.executeQuery("GET_CREDIT_TRAN", new Object[] { tranId, 974 }, false);
		if(results.next()) {
			CreditTran tran=new CreditTran();
			results.fillBean(tran);
			CreditData data=new CreditData();
			data.setVersion("1.0");
			data.add(tran);
			//String xml=CreditData.toXML(data);
			//System.out.println(xml);
			CreditData.toXML(data, System.out);
			RESTTransporter transport=new RESTTransporter();
			//transport.testRest(xml);
		} else{
			throw new SQLException("Credit Tran XML tranId= " + tranId + " not found in database");
		}
    }
    protected static void testCreditTranCount() throws Exception {
    	Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/loader-data-layer.xml"));
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transTypeId", 22);
        Results results = DataLayerMgr.executeQuery("GET_CREDIT_IND", parameters, false);
		if(results.next()) {
			char creditInd=results.getValue("creditInd", char.class);
			System.out.println("creditInd:"+creditInd);
		} else{
			System.out.println("no result");
		}
		
		Date now=new Date();
		parameters.put("startDate", now);
		parameters.put("deviceSerialCd","VJ000000003");
		results = DataLayerMgr.executeQuery("GET_CREDIT_TRAN_REPORTS", parameters, false);
		int creditTranReportCount=0;
		if(results.next()) {
			int[] userReportIds=results.getValue("userReportIds", int[].class);
			creditTranReportCount=userReportIds.length;
			String terminalId=results.getValue("terminalId", String.class);
			System.out.println("creditTranReportCount:"+creditTranReportCount+" terminalId:"+terminalId);
		} else{
			System.out.println("no result");
		}
		
		int tranId=1098957;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tranId", tranId);
		DataLayerMgr.selectInto("GET_TRAN_DATA_FOR_REQUEST", params);
		System.out.println(params);
    }
	protected static void testJspTemplate() throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("firstname", "Sebastian");
		params.put("baseUrl", "http://bkrugi7.usatech.com:8580/");
		params.put("promoTitle", "Best Dealio Ever");
		params.put("promoDetail", "Take 0% off for ever");
		JspRequestDispatcherFactory jrdf = new JspRequestDispatcherFactory();
		String body = Interceptor.intercept(jrdf, jrdf, "com/usatech/campaign/promo-template.jsp", Locale.getDefault(), null, params);
		System.out.println("Got Body:");
		System.out.println(body);

	}
    protected static void testFreqs() throws Exception {
    	Properties props = MainWithConfig.loadPropertiesWithConfig("RequestReport.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("RequestReport");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/rgr-data-layer.xml"));
    	FrequencyManager fm = new FrequencyManager();
    	fm.loadBasicFrequencies("GET_FREQUENCIES");
    	Date now = new Date();
    	for(int fid : fm.getFrequencyIds()) {
    		Frequency freq = fm.getFrequency(fid);
    		System.out.println("Next 5 Ranges from " + now + " for Frequency #" + fid + ":");
    		DateRange dateRange = freq.nextDateRange(now);
    		System.out.println(dateRange.beginDate + " to " + dateRange.endDate);
    		for(int i = 0; i < 5; i++) {
    			dateRange = freq.nextDateRange(dateRange);
    			System.out.println(dateRange.beginDate + " to " + dateRange.endDate);
    		}
    		System.out.println("-------------------------------");
    		System.out.println();
    	}
    }
    protected static void testDailyFreqs() throws Exception {
    	Configuration config = BaseWithConfig.loadConfig("RequestReport.properties", ReportGeneratorTest.class, null);
    	BaseWithConfig.configureDataSourceFactory(config, null);
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:/com/usatech/report/rgr-data-layer.xml"));
    	FrequencyManager fm = new FrequencyManager();
    	fm.loadBasicFrequencies("GET_FREQUENCIES");
    	Calendar cal = Calendar.getInstance();
    	cal.set(2011, 1, 5, 23, 59, 59);
    	Date lastSent = cal.getTime();
    	Frequency freq = fm.getFrequency(1);
	    DateRange dateRange = freq.nextDateRange(lastSent);
		double latency = 1.05; // the number of days to wait to send
		//latency = Math.max(latency,1.0/6.0); // ensure at least a 4 hour leeway
		Date now = new Date(System.currentTimeMillis() - (long)(24 * 60 * 60 * 1000 * latency)); // give specified leeway
		System.out.println("Send Report for last sent " + lastSent + "? " + dateRange.endDate.before(now));

	}

    protected static void testRG() throws Exception {
        /*String appName = "ReportGenerator";
    App.setMasterAppName(appName);
    ReportGenerator rg = new ReportGenerator();
    rg.setProps(App.getProperties(App.getMasterAppName()));
    rg.generateBatchReport(575, 20079);
    rg.generateBatchReport(559, 20079);
    */
    /*
    System.out.println(Arrays.toString(MailcapCommandMap.getDefaultCommandMap().getMimeTypes()));
    CommandInfo[] cis = MailcapCommandMap.getDefaultCommandMap().getAllCommands("application/xls");
    DataContentHandler dch = MailcapCommandMap.getDefaultCommandMap().createDataContentHandler("application/xls");

    System.out.println(dch);
    System.out.println(Arrays.toString(MailcapCommandMap.getDefaultCommandMap().getAllCommands("application/pdf")));
    */
    }
    protected static void testFTP() throws Exception {
        int port = 21;
        String host = "12.107.55.68";
        String username = "usatech$77";
        String password = "cv092006$";
        String remoteDir = "inbox/weekly";
        String filename = "test-ftp-file.txt";
        String contents = "This is a test. Please ignore.";

        // Make a client connection
        FtpBean ftp = new FtpBean();
        ftp.setPort(port);
        try {
            // login
        	ftp.ftpConnect(host, username, password);

            // Change directory
            if(remoteDir != null && remoteDir.trim().length() > 0) ftp.setDirectory(remoteDir);

            // Upload a file
            System.out.println("FileName\tDate\tSize");
            FtpListResult flr = ftp.getDirectoryContent();
            while(flr.next()) {
            	System.out.println(flr.getName() + "\t" + flr.getDate() + "\t" + flr.getSize());
            }
            //ftp.putBinaryFile(filename, contents.getBytes());
        } catch(Exception e) {
        	e.printStackTrace();
		} finally {
            ftp.close();
        }
    }
    protected static void testSFTP() throws Exception {
    	Map<String,Object> transportProperties = new HashMap<String,Object>();
        transportProperties.put("HOST", "sftp.betagrid.gxs.com");
        transportProperties.put("PORT", 22);
        transportProperties.put("USERNAME", "806204301");
        transportProperties.put("PASSWORD", "PASSWORD");
        transportProperties.put("REMOTE PATH", "PEPAMAI/PEPAMCSVITFF");
        Resource ds = new FileResource("X:/data/Payment Reconciliation for Payment #83386.csv");
        /*DataSource ds = new DataSource() {
        	public String getContentType() {
        		return "text/plain";
        	}
        	public InputStream getInputStream() throws IOException {
        		return new ByteArrayInputStream("This is a test. Please ignore.".getBytes());
        	}
        	public String getName() {
        		return "test.txt";
        	}
        	public OutputStream getOutputStream() throws IOException {
        		return null;
        	}
        };*/
        Properties props = new Properties();
        props.put("knownhosts.file", "X:/ReportGenerator/known_hosts");
        Transporter transporter = new SecureFTPTransporter(props);
        Map<String,Object> messageParameters = Collections.emptyMap();

        String result = transporter.transport(transportProperties, TransportReason.BROADCAST, ds, messageParameters);
        System.out.println("RESULT: " + result);
    }
    protected static void testSFTP2() throws Exception {
    	Console console;
    	Interaction interaction;
		if((console = System.console()) != null) {
			interaction = new ConsoleInteraction(console);
		} else {
			interaction = new InOutInteraction();
		}
		//String host = "usadev01.usatech.com"; String user = "bkrug"; String path = "Inbound"; String password = new String(interaction.readPassword("Please enter the password for %1$s@%2$s:", user, host)); String privateKey = null;
		//String host = "ecgitef.pbsg.com"; String user = "usatech"; String path = "Inbound"; String password = "Test@12"; String privateKey = null;
		//String host = "204.136.110.216"; String user = "usatech"; String path = "Inbound"; String password = "Test@12"; String privateKey = null;
		//String host = "204.136.108.216"; String user = "usatech"; String path = "Inbound"; String password = "Test@12"; String privateKey = null;
		String host = "ecgtstf.pbsg.com"; String user = "usatech"; String path = "Inbound"; String password = "Test@12"; String privateKey = null;
		Map<String,Object> transportProperties = new HashMap<String,Object>();
        transportProperties.put("HOST", host);
        transportProperties.put("PORT", 22);
        transportProperties.put("USERNAME", user);
        transportProperties.put("PASSWORD", password);
        transportProperties.put("PRIVATE KEY", privateKey);
        transportProperties.put("REMOTE PATH", path);
        Resource ds = new FileResource("C:\\Users\\Public\\Documents\\Test_SFTP_J.txt");
        /*DataSource ds = new DataSource() {
        	public String getContentType() {
        		return "text/plain";
        	}
        	public InputStream getInputStream() throws IOException {
        		return new ByteArrayInputStream("This is a test. Please ignore.".getBytes());
        	}
        	public String getName() {
        		return "test.txt";
        	}
        	public OutputStream getOutputStream() throws IOException {
        		return null;
        	}
        };*/
        Properties props = new Properties();
        props.put("knownhosts.file", "C:\\Users\\Public\\Documents\\known_hosts");
        Transporter transporter = new SecureFTPTransporter(props);
        Map<String,Object> messageParameters = Collections.emptyMap();

        String result = transporter.transport(transportProperties, TransportReason.BROADCAST, ds, messageParameters);
        System.out.println("RESULT: " + result);
    }
    protected static void testSFTP_G() throws Exception {
    	Console console;
    	Interaction interaction;
		if((console = System.console()) != null) {
			interaction = new ConsoleInteraction(console);
		} else {
			interaction = new InOutInteraction();
		}
		//String host = "usadev01.usatech.com"; String user = "bkrug"; String path = "Inbound"; String password = new String(interaction.readPassword("Please enter the password for %1$s@%2$s:", user, host));
		//String host = "ecgitef.pbsg.com"; String user = "usatech"; String path = "Inbound"; String password = "Test@12";
		//String host = "204.136.110.216"; String user = "usatech"; String path = "Inbound"; String password = "Test@12";
		//String host = "204.136.108.216"; String user = "usatech"; String path = "Inbound"; String password = "Test@12";
		String host = "ecgtstf.pbsg.com"; String user = "usatech"; String path = "Inbound"; String password = "Test@12"; String privateKey = null;
		/*String privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" +
		"MIIEogIBAAKCAQEAs+6MvJRzuZpKWwY+0CU7LMQ38+yDMH60e4SdXNlOoroSC1VI\n" +
		"ITLxXzSWTlU8HDopbzluCYAUYVDezdh8QwbcvPWBYqsBt7OtsFug45pmz3VEV55k\n" +
		"L399Ha0MU6RvlIpkKH3O9tjJSx5aiIw+kl9zjKSJJD9t4BqciC6KBlPq927MB25w\n" +
		"ubqqXXaJQKI1S2TxXi2VgwReIzuH0GKGUUKY8MF9CAuwTCz/+M93Cvf6TrIcQ6M1\n" +
		"8JYwVyEHW9ZYsfd5Ux3KHMNsc5vQ6NFF0SLikojRytcZQWaaXNKzTZE5r5kjkYAt\n" +
		"QIqFJ+giUb8Yy9/6p8V5aFITx/uXdnFiLX50qQIBJQKCAQEAjQcUXHRatBElhZ0c\n" +
		"eaCkAH4eBF9tvjnLusHjH0KJvdAb+wt9uSftnaXB6l57OLf29k+bbzrfiot+MqLC\n" +
		"Ql9TEKS/WywWGlxlijn6pI3GM+ZKUoMQQOdiEFczLMz9gkL0jnBxyGS5cjpiomAV\n" +
		"XffJRLhP0E1j9NaWXOY01Ib2bubPOHD0HIwWsd8zztFqDJCuglLKp8fOozcC4si7\n" +
		"7ThpBw6RtMvsKH6Ph8hKBlU9SgZG0kIW88Fj3otllnM7j8FoOXh7sSBFkjB135gl\n" +
		"ieTkX2zH74ho5wnvwH+KB4GHFfSDhezkkCbsGk+jt97pmYYOkYzF9GclQltLS5oY\n" +
		"M6984QKBgQD4/vWFkd2HSmCZ3/YFNmkASNBC7wAnrg8TpuTXIH8XPcvX9Ij3s80L\n" +
		"sol7jJ9wUgG47wlOpzg6ourEg2zfLDdg+iUbZ0WN0PDyqQd1k6ktBLC/FqLWE9as\n" +
		"nOPbOURdIoYJayvmLzsf4sDELphSA9nhwXD7yehRQs7rovDSHsuzlwKBgQC4/kEU\n" +
		"znnZ+y4gCVpiKENZDfyE+76qZyQPYGvrj1g5fdFmwuh5ooCv6HtoFSmUEQCn3mjk\n" +
		"fEy857lgIUk2W5ReZ+w0NJtQ6CZrK1ckwoHP+Zy7VnSRMu0unjGjjyMwM0URPapy\n" +
		"5qImYWCnvPi3OR1EGvkox7Ex4RB35c2dUSshvwKBgQDkzpxs5uc+DP7SlnNevGB8\n" +
		"zUnA90xAI2fM3o0YthPr1+u/hsMNIcNW22mUIFso8Wld1Lxxyhf+h96mvfVXdL1E\n" +
		"W3wLV/q5qz5GxNZsCyXWV1ZOutrSjsVEq9hM6IQCjm1Uw1jFrtxifVcw0N7/Qc8i\n" +
		"emfSndxmWRErjs90+bREKQKBgG3+9kO5Ci6VWbIvEyWbZlCgiE8SOgR0rahHK2l3\n" +
		"05fORShYNzORDj8UnGdmh2zL1uBauui4nFSldSRm0ZXxQ3ZnTizhAmd1R0akJfou\n" +
		"drMDHuvuN3jt2SKjQBwP62jEie6TXm3VPdGhrxegyzyC0yGTf2RbD2nEF6E8iBhZ\n" +
		"xp5xAoGASsjSw6MiBvqEhDTPNaOmgalvzFeYABFkkf8ssKBBltLa6xuSADLPVdgi\n" +
		"0C+7HgnyrADOu0WWsQYy9tLCJ7kQFUj+VFnrUdc6Gk8TUUJzSvqlOSz5cPjVB5h9\n" +
		"t6gJ6VjFrlR1FEr3cZ+mSNwSxrCNmGM22FDbdJ4QC07KlgMiGeQ=\n" +
		"-----END RSA PRIVATE KEY-----\n"; String host = "usapfs01.usatech.com"; String user = "uccboc"; String path = "out"; String password = null; */
    	Map<String,Object> transportProperties = new HashMap<String,Object>();
        transportProperties.put("HOST", host);
        transportProperties.put("PORT", 22);
        transportProperties.put("USERNAME", user);
        transportProperties.put("PASSWORD", password);
        transportProperties.put("PRIVATE KEY", privateKey);
        transportProperties.put("REMOTE PATH", path);
        Resource ds = new FileResource("C:\\Users\\bkrug\\Documents\\usatech_april30_171494.csv");
        /*DataSource ds = new DataSource() {
        	public String getContentType() {
        		return "text/plain";
        	}
        	public InputStream getInputStream() throws IOException {
        		return new ByteArrayInputStream("This is a test. Please ignore.".getBytes());
        	}
        	public String getName() {
        		return "test.txt";
        	}
        	public OutputStream getOutputStream() throws IOException {
        		return null;
        	}
        };*/
        Properties props = new Properties();
        props.put("knownhosts.file", "C:\\Users\\Public\\Documents\\known_hosts");
        Transporter transporter = new SecureFTPGanymedTransporter(props);
        Map<String,Object> messageParameters = Collections.emptyMap();

        String result = transporter.transport(transportProperties, TransportReason.BROADCAST, ds, messageParameters);
        System.out.println("RESULT: " + result);
    }
    
    protected static void testSFTP_G2() throws Exception {
    	String privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" +
		"MIIEogIBAAKCAQEAs+6MvJRzuZpKWwY+0CU7LMQ38+yDMH60e4SdXNlOoroSC1VI\n" +
		"ITLxXzSWTlU8HDopbzluCYAUYVDezdh8QwbcvPWBYqsBt7OtsFug45pmz3VEV55k\n" +
		"L399Ha0MU6RvlIpkKH3O9tjJSx5aiIw+kl9zjKSJJD9t4BqciC6KBlPq927MB25w\n" +
		"ubqqXXaJQKI1S2TxXi2VgwReIzuH0GKGUUKY8MF9CAuwTCz/+M93Cvf6TrIcQ6M1\n" +
		"8JYwVyEHW9ZYsfd5Ux3KHMNsc5vQ6NFF0SLikojRytcZQWaaXNKzTZE5r5kjkYAt\n" +
		"QIqFJ+giUb8Yy9/6p8V5aFITx/uXdnFiLX50qQIBJQKCAQEAjQcUXHRatBElhZ0c\n" +
		"eaCkAH4eBF9tvjnLusHjH0KJvdAb+wt9uSftnaXB6l57OLf29k+bbzrfiot+MqLC\n" +
		"Ql9TEKS/WywWGlxlijn6pI3GM+ZKUoMQQOdiEFczLMz9gkL0jnBxyGS5cjpiomAV\n" +
		"XffJRLhP0E1j9NaWXOY01Ib2bubPOHD0HIwWsd8zztFqDJCuglLKp8fOozcC4si7\n" +
		"7ThpBw6RtMvsKH6Ph8hKBlU9SgZG0kIW88Fj3otllnM7j8FoOXh7sSBFkjB135gl\n" +
		"ieTkX2zH74ho5wnvwH+KB4GHFfSDhezkkCbsGk+jt97pmYYOkYzF9GclQltLS5oY\n" +
		"M6984QKBgQD4/vWFkd2HSmCZ3/YFNmkASNBC7wAnrg8TpuTXIH8XPcvX9Ij3s80L\n" +
		"sol7jJ9wUgG47wlOpzg6ourEg2zfLDdg+iUbZ0WN0PDyqQd1k6ktBLC/FqLWE9as\n" +
		"nOPbOURdIoYJayvmLzsf4sDELphSA9nhwXD7yehRQs7rovDSHsuzlwKBgQC4/kEU\n" +
		"znnZ+y4gCVpiKENZDfyE+76qZyQPYGvrj1g5fdFmwuh5ooCv6HtoFSmUEQCn3mjk\n" +
		"fEy857lgIUk2W5ReZ+w0NJtQ6CZrK1ckwoHP+Zy7VnSRMu0unjGjjyMwM0URPapy\n" +
		"5qImYWCnvPi3OR1EGvkox7Ex4RB35c2dUSshvwKBgQDkzpxs5uc+DP7SlnNevGB8\n" +
		"zUnA90xAI2fM3o0YthPr1+u/hsMNIcNW22mUIFso8Wld1Lxxyhf+h96mvfVXdL1E\n" +
		"W3wLV/q5qz5GxNZsCyXWV1ZOutrSjsVEq9hM6IQCjm1Uw1jFrtxifVcw0N7/Qc8i\n" +
		"emfSndxmWRErjs90+bREKQKBgG3+9kO5Ci6VWbIvEyWbZlCgiE8SOgR0rahHK2l3\n" +
		"05fORShYNzORDj8UnGdmh2zL1uBauui4nFSldSRm0ZXxQ3ZnTizhAmd1R0akJfou\n" +
		"drMDHuvuN3jt2SKjQBwP62jEie6TXm3VPdGhrxegyzyC0yGTf2RbD2nEF6E8iBhZ\n" +
		"xp5xAoGASsjSw6MiBvqEhDTPNaOmgalvzFeYABFkkf8ssKBBltLa6xuSADLPVdgi\n" +
		"0C+7HgnyrADOu0WWsQYy9tLCJ7kQFUj+VFnrUdc6Gk8TUUJzSvqlOSz5cPjVB5h9\n" +
		"t6gJ6VjFrlR1FEr3cZ+mSNwSxrCNmGM22FDbdJ4QC07KlgMiGeQ=\n" +
		"-----END RSA PRIVATE KEY-----\n"; String host = "usapfs01.usatech.com"; String username = "uccboc"; String remoteDir = "out"; String password = null; int port = 22; String passphrase = null;
    	
    	
    	Connection connection = new Connection(host, port);
		try {
			// Connect to the host
	        //new KnownHostsVerifier(new File("C:\\Users\\Public\\Documents\\known_hosts")));
			connection.connect();
	        // authenticate
	        boolean authenticated = false;
	        StringBuilder methods = new StringBuilder();
	        if(privateKey != null) {
	        	authenticated = connection.authenticateWithPublicKey(username, privateKey.toCharArray(), passphrase);
	        	methods.append("private key, ");
	        }
	        if(!authenticated && password != null) {
	        	authenticated = connection.authenticateWithPassword(username, password);
	        	methods.append("password, ");
	        }
	        if(!authenticated) {
	        	throw new TransportException("Could not log into " + host + ':' + port + " using " + methods.substring(0, methods.length() - 2), TransportErrorType.TRANSPORT_ERR);
	        }
	        
	        // The connection is authenticated we can now do some real work!           
	        SFTPv3Client sftp = new SFTPv3Client(connection);
	        try {
	        	// Change directory
	        	String dirPrefix;
	            if(remoteDir != null && remoteDir.trim().length() > 0) 
	            	try {
	            		dirPrefix = sftp.canonicalPath(remoteDir) + "/";
	            	} catch(SFTPException e) {
	            		StringBuilder sb = new StringBuilder();
	            		sb.append("Could not get canonical path for '").append(remoteDir).append("' because ").append(e.getMessage()).append(". Current Directory list:\n");
	            		Vector<?> list = sftp.ls(".");
	            		for(Object o : list) {
	            			if(o instanceof SFTPv3DirectoryEntry) {
	            				sb.append(((SFTPv3DirectoryEntry)o).longEntry).append('\n');
	            			} else {
	            				sb.append(o).append('\n');
	            			}
	            		}
	            		System.out.println(sb.toString());
	            		throw e;
	            	}
	            else
	            	dirPrefix = "";
	            
	            StringBuilder sb = new StringBuilder();
        		Vector<?> list = sftp.ls(dirPrefix + ".");
        		for(Object o : list) {
        			if(o instanceof SFTPv3DirectoryEntry) {
        				sb.append(((SFTPv3DirectoryEntry)o).longEntry).append('\n');
        			} else {
        				sb.append(o).append('\n');
        			}
        		}
        		System.out.println(sb.toString());
        		
	            // Upload a file
	            /*
	            boolean rename = false;
                StringBuilder sb = new StringBuilder();
                if(tmpPrefix != null && (tmpPrefix=tmpPrefix.trim()).length() > 0) {
                	sb.append(tmpPrefix);
                	rename = true;
                }
                sb.append(resource.getName());
                if(tmpSuffix != null && (tmpSuffix=tmpSuffix.trim()).length() > 0) {
                	sb.append(tmpSuffix);
                	rename = true;
                }
                String tmpName = sb.toString();
                SFTPv3FileHandle handle = sftp.createFileTruncate(dirPrefix + tmpName);
                try {
                    InputStream in = resource.getInputStream();
                    try {
	                    byte[] b = byteBufferLocal.get();
	                    int tot = 0;
	                    for(int len = 0; (len=in.read(b)) >= 0; tot += len) 
	                    	sftp.write(handle, tot, b, 0, len);
	                } finally {
                    	in.close();
                    }
                } finally {
                    sftp.closeFile(handle);                  
                }
                if(rename)
                	sftp.mv(dirPrefix + tmpName, dirPrefix + resource.getName());
                	*/
	        } finally {
	        	sftp.close();
	        }
        } catch(IOException ioe) {
        	System.out.println("While sftping file");
        	ioe.printStackTrace();
        } finally {
        	connection.close();
        }
    }
    protected static void testDexInputStream() throws Exception {
    	String[] contents = new String[] {
    			"junkDXS*h*\nMA5*ERROR*HA\nD*X*E*\nDXE*0000*09876\r\nMoreJunkYeah\r\nokay."
    	};
    	for(String c : contents) {
    		System.out.println(IOUtils.readFully(new DexInputStream(new ByteArrayInputStream(c.getBytes()))));
    	}

    }

	protected static void testVDITemplate() throws Exception {
		System.setProperty("javax.xml.transform.TransformerFactory", "com.jclark.xsl.trax.TransformerFactoryImpl");
    	String[] contents = new String[] {
    			"junkDXS*h*\nDA1*G8400956\u0000\u0001\u0002\u0003\u0103*ePortG8*6040\nMA5*ERROR*HA\nD*X*E*\nDXE*0000*09876\r\nMoreJunkYeah\r\nokay."
    	};
    	VDIDexUploadInput input = new VDIDexUploadInput();
		input.setApplicationId("ReportGenerator");
		input.setApplicationVersion("3");
		input.setCustomerId("1");
		input.setDeviceSerialCd("TT000001");
		input.setDexCompressionParam(null);
		input.setDexCompressionType(null);
		input.setDexDate(new Date());
		input.setDexEncoding(1);
		input.setDexReason(0);
		input.setProviderId("USAT");
		input.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
		input.setTransactionId("12345");
		input.setXmlVersion("1.0");
		for(String c : contents) {
			input.setDexFile(new ByteArrayResource(c.getBytes(), "NONE", "DEX-" + input.getDeviceSerialCd() + "-" + input.getDexLocalTime() + ".log"));
	        StringWriter writer = new StringWriter();
			try {
            	TemplatesLoader.getInstance(null).newTransformer("resource:com/usatech/transport/soap/vdi/vdiDexUpload.xsl", 0).transform(
            			new SAXSource(new BeanAdapter1(), new BeanInputSource(input, "info")), new StreamResult(writer));
            	System.out.println("BEGIN ------------------------------------------------------");
            	System.out.println(writer.toString());
            	System.out.println("END ------------------------------------------------------");            	
            } catch(RemoteException e) {
                throw new TransportException("RemoteException while sending DEX file", e, TransportErrorType.TRANSPORT_ERR);
            } catch(TransformerConfigurationException e) {
            	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
    		} catch(TransformerException e) {
            	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
    		} catch(IOException e) {
            	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
    		} catch(LoadingException e) {
            	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
    		}
    	}

    }

}
