package test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.usatech.report.Frequency;
import com.usatech.report.Frequency.DateRange;
import com.usatech.report.FrequencyManager;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.db.config.ConfigLoader;
import simple.io.App;
import simple.io.ConfigSource;

public class LastSentTest {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Properties props = MainWithConfig.loadPropertiesWithConfig("ReportGeneratorService-dev.properties", ReportGeneratorTest.class, null);
    	Base.configureDataSourceFactory(props, null);
		
    	App.setMasterAppName("ReportGenerator");
        ConfigLoader.loadConfig(ConfigSource.createConfigSource("resource:rgr-data-layer.xml"));
        SimpleDateFormat df=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date lastSent=df.parse("07/18/2016 14:59:00");
		double latency = 0;
		Integer frequencyId = 8;
		FrequencyManager frequencyManager = new FrequencyManager();
        frequencyManager.loadBasicFrequencies("GET_FREQUENCIES");
		Frequency frequency = frequencyManager.getFrequency(frequencyId);
		if(frequency == null) {
			throw new Exception("Unknown Frequency: '" + frequencyId + "' - skipping report");
		}

		//Frequency frequency = new BasicFrequency(Calendar.DAY_OF_WEEK, 1, -(24*60*60*1000));
		//Frequency frequency = new BasicFrequency(Calendar.WEEK_OF_YEAR, 1, -(24*60*60*1000));

		DateRange dateRange = frequency.nextDateRange(lastSent);
		
		//Date now = new Date(System.currentTimeMillis() - (long)(24 * 60 * 60 * 1000 * latency)); // give specified leeway
		
		Date now = df.parse("07/19/2016 09:18:00");; // give specified leeway
		
		int cnt = 0;
		System.out.println("Starting with Last Sent of " + lastSent);
		System.out.println("Now equals " + now);
		while(dateRange.endDate.before(now)) {
			System.out.println("Would generate a report for " + dateRange.beginDate + " to " + dateRange.endDate);
			cnt++;
			dateRange = frequency.nextDateRange(dateRange);
		}
		System.out.println("Would generate " + cnt + " reports");
	}

}
