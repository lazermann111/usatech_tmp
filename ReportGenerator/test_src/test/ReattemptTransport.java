package test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.ConfigurationException;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;

import simple.app.AppPropertiesConfiguration;
import simple.app.Base;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.ByteInput;
import simple.io.ConfigSource;
import simple.io.LoadingException;
import simple.results.BeanException;
import simple.results.Results;
import simple.util.CollectionUtils;

public class ReattemptTransport {
	public static void main(String[] args) throws Exception {
		publishTransportErrorReAttempt(61);
	}
	@SuppressWarnings("unchecked")
	public static int publishTransportErrorReAttempt(int errorId) throws SQLException, BeanException, DataLayerException, ConvertException, ServiceException, ConfigException, LoadingException, IOException, ConfigurationException {
		Properties props = ConfigurationConverter.getProperties(new AppPropertiesConfiguration("ReportGeneratorService.properties"));
		Properties subProperties = CollectionUtils.getSubProperties(props, "com.usatech.app.jms.JMSPublisher.");
		Publisher<ByteInput> publisher = simple.app.Main.configure(Publisher.class, subProperties, props, null, true);
		Base.configureDataSourceFactory(props, null);
		ConfigLoader.loadConfig(ConfigSource.createConfigSource("file:../usalive/xsl/ccs-data-layer.xml"));
		MessageChainV11 mc=new MessageChainV11();
		MessageChainStep updateReportSentStep=mc.addStep(props.getProperty("queue.updateReportSent","usat.report.updateReportSent"));
		MessageChainStep transportErrorStep=mc.addStep(props.getProperty("queue.transportError","usat.report.transportError"));

		Map<String, Object> params=new HashMap<String, Object>(11);
        params.put("errorId", errorId);
		DataLayerMgr.selectInto("GET_TRANSPORT_DETAILS_BY_ERROR_ID", params);
		int transportTypeId = ConvertUtils.getInt(params.get("transportTypeId"));
		MessageChainStep transportStep = mc.addStep(props.getProperty("queue.transport","usat.report.transport") + '.' + transportTypeId);
		//ReportRequestFactory.addTransportPropertiesToStep(transportStep, params, null);

		transportStep.addStringAttribute("transport.transportReason", params.get("batchId")==null?"TIMED_REPORT":"BATCH_REPORT");
		transportStep.addStringAttribute("file.fileName", params.get("fileName").toString());
		transportStep.addStringAttribute("file.fileKey", params.get("fileKey").toString());
		transportStep.addStringAttribute("file.fileContentType", params.get("fileContentType").toString());
		transportStep.addStringAttribute("transport.fileId", ConvertUtils.getStringSafely(params.get("fileId")) );
		transportStep.addStringAttribute("transport.filePasscode",params.get("filePasscode").toString());

		transportStep.setNextSteps(0, updateReportSentStep);
		transportStep.setNextSteps(1, transportErrorStep);

		Results results = DataLayerMgr.executeQuery("GET_CCS_TRANSPORT_ERROR_ATTR", new Object[] { errorId }, false);
		while(results.next()) {
			transportErrorStep.addStringAttribute(ConvertUtils.getStringSafely(results.getValue("attrName")), ConvertUtils.getStringSafely(results.getValue("attrValue")));
			transportStep.addStringAttribute(ConvertUtils.getStringSafely(results.getValue("attrName")), ConvertUtils.getStringSafely(results.getValue("attrValue")));
		}
		transportErrorStep.addReferenceAttribute("error.transportError", transportStep,"error.transportError" );
		transportErrorStep.addReferenceAttribute("error.errorTs", transportStep,"error.errorTs" );
		transportErrorStep.addStringAttribute("reportSentId", ConvertUtils.getStringSafely(params.get("reportSentId")));

		updateReportSentStep.addStringAttribute("reportSentId", ConvertUtils.getStringSafely(params.get("reportSentId")));
		updateReportSentStep.addReferenceAttribute("sentDate", transportStep, "sentDate");
		updateReportSentStep.addReferenceAttribute("sentDetails", transportStep, "sentDetails");

		MessageChainService.publish(mc, publisher);
		return ConvertUtils.getInt(params.get("transportId"));
	}
}
