package test;

import java.io.IOException;
import java.util.Vector;

import org.junit.After;
import org.junit.Before;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.InteractiveCallback;
import ch.ethz.ssh2.SFTPException;
import ch.ethz.ssh2.SFTPv3Client;
import ch.ethz.ssh2.SFTPv3DirectoryEntry;
import ch.ethz.ssh2.SFTPv3FileAttributes;

public class GanymedSFTPTest {
	/*protected SFTPv3Client sftp;
	protected Connection connection;

	@Before
	public void setUp() throws IOException {
		String host = "eccpgs11.usatech.com";
		int port = 22;
		String username = "usalive";
		final String password = "usatech";
		String privateKey = null;
		String passphrase = null;
		connection = new Connection(host, port);
		try {
			// Connect to the host
			connection.connect();
			// authenticate
			boolean authenticated = false;
			StringBuilder methods = new StringBuilder();
			if(privateKey != null) {
				authenticated = connection.authenticateWithPublicKey(username, privateKey.toCharArray(), passphrase);
				methods.append("private key, ");
			}
			if(!authenticated && password != null) {
				if(connection.isAuthMethodAvailable(username, "password")) {
					authenticated = connection.authenticateWithPassword(username, password);
					methods.append("password, ");
				} else if(connection.isAuthMethodAvailable(username, "keyboard-interactive")) {
					authenticated = connection.authenticateWithKeyboardInteractive(username, new InteractiveCallback() {
						@Override
						public String[] replyToChallenge(String name, String instruction, int numPrompts, String[] prompt, boolean[] echo) throws Exception {
							switch(numPrompts) {
								case 1:
									return new String[] { password };
								case 0:
									return new String[0];
								default:
									return new String[numPrompts];
							}

						}
					});
					methods.append("keyboard-interactive, ");
				}
			}
			if(!authenticated) {
				throw new IOException("Could not log into " + host + ':' + port + " using " + methods.substring(0, methods.length() - 2));
			}

			// The connection is authenticated we can now do some real work!
			sftp = new SFTPv3Client(connection);
		} finally {
			if(this.sftp == null)
				connection.close();
		}

	}

	@After
	public void tearDown() {
		if(this.sftp != null) {
			sftp.close();
			sftp = null;
			if(connection != null) {
				connection.close();
				connection = null;
			}
		}
	}

	@org.junit.Test
	public void testLsFile() {
		String[] filePaths = { "reports/testTransportFileName.tmp", "NEVER EXISTS", "reports/testTransportFileName", "reports", "." };
		for(String filePath : filePaths)
			try {
			Vector<?> list = sftp.ls(filePath);
			System.out.println("Listing for '" + filePath + "': (" + list.size() + " items)");
			for(Object item : list) {
				if(item instanceof SFTPv3DirectoryEntry) {
					System.out.println('\t' + ((SFTPv3DirectoryEntry) item).longEntry);
				} else {
					System.out.println('\t' + item.toString());
				}
			}
			} catch(IOException e) {
				System.out.println("Listing for '" + filePath + "': ERROR - " + e.getMessage());
		}
	}

	@org.junit.Test
	public void testStatFile() {
		String[] filePaths = { "reports/testTransportFileName.tmp", "NEVER EXISTS", "reports/testTransportFileName", "reports", "." };
		for(String filePath : filePaths)
			try {
				SFTPv3FileAttributes atts = sftp.stat(filePath);
				System.out.println("Attributes for '" + filePath + "': size=" + atts.size + "; lastMod=" + atts.mtime + "; permissions=" + atts.getOctalPermissions());
			} catch(IOException e) {
				System.out.println("Listing for '" + filePath + "': ERROR - " + e.getMessage());
			}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		String privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" +
			"MIIEogIBAAKCAQEAs+6MvJRzuZpKWwY+0CU7LMQ38+yDMH60e4SdXNlOoroSC1VI\n" +
			"ITLxXzSWTlU8HDopbzluCYAUYVDezdh8QwbcvPWBYqsBt7OtsFug45pmz3VEV55k\n" +
			"L399Ha0MU6RvlIpkKH3O9tjJSx5aiIw+kl9zjKSJJD9t4BqciC6KBlPq927MB25w\n" +
			"ubqqXXaJQKI1S2TxXi2VgwReIzuH0GKGUUKY8MF9CAuwTCz/+M93Cvf6TrIcQ6M1\n" +
			"8JYwVyEHW9ZYsfd5Ux3KHMNsc5vQ6NFF0SLikojRytcZQWaaXNKzTZE5r5kjkYAt\n" +
			"QIqFJ+giUb8Yy9/6p8V5aFITx/uXdnFiLX50qQIBJQKCAQEAjQcUXHRatBElhZ0c\n" +
			"eaCkAH4eBF9tvjnLusHjH0KJvdAb+wt9uSftnaXB6l57OLf29k+bbzrfiot+MqLC\n" +
			"Ql9TEKS/WywWGlxlijn6pI3GM+ZKUoMQQOdiEFczLMz9gkL0jnBxyGS5cjpiomAV\n" +
			"XffJRLhP0E1j9NaWXOY01Ib2bubPOHD0HIwWsd8zztFqDJCuglLKp8fOozcC4si7\n" +
			"7ThpBw6RtMvsKH6Ph8hKBlU9SgZG0kIW88Fj3otllnM7j8FoOXh7sSBFkjB135gl\n" +
			"ieTkX2zH74ho5wnvwH+KB4GHFfSDhezkkCbsGk+jt97pmYYOkYzF9GclQltLS5oY\n" +
			"M6984QKBgQD4/vWFkd2HSmCZ3/YFNmkASNBC7wAnrg8TpuTXIH8XPcvX9Ij3s80L\n" +
			"sol7jJ9wUgG47wlOpzg6ourEg2zfLDdg+iUbZ0WN0PDyqQd1k6ktBLC/FqLWE9as\n" +
			"nOPbOURdIoYJayvmLzsf4sDELphSA9nhwXD7yehRQs7rovDSHsuzlwKBgQC4/kEU\n" +
			"znnZ+y4gCVpiKENZDfyE+76qZyQPYGvrj1g5fdFmwuh5ooCv6HtoFSmUEQCn3mjk\n" +
			"fEy857lgIUk2W5ReZ+w0NJtQ6CZrK1ckwoHP+Zy7VnSRMu0unjGjjyMwM0URPapy\n" +
			"5qImYWCnvPi3OR1EGvkox7Ex4RB35c2dUSshvwKBgQDkzpxs5uc+DP7SlnNevGB8\n" +
			"zUnA90xAI2fM3o0YthPr1+u/hsMNIcNW22mUIFso8Wld1Lxxyhf+h96mvfVXdL1E\n" +
			"W3wLV/q5qz5GxNZsCyXWV1ZOutrSjsVEq9hM6IQCjm1Uw1jFrtxifVcw0N7/Qc8i\n" +
			"emfSndxmWRErjs90+bREKQKBgG3+9kO5Ci6VWbIvEyWbZlCgiE8SOgR0rahHK2l3\n" +
			"05fORShYNzORDj8UnGdmh2zL1uBauui4nFSldSRm0ZXxQ3ZnTizhAmd1R0akJfou\n" +
			"drMDHuvuN3jt2SKjQBwP62jEie6TXm3VPdGhrxegyzyC0yGTf2RbD2nEF6E8iBhZ\n" +
			"xp5xAoGASsjSw6MiBvqEhDTPNaOmgalvzFeYABFkkf8ssKBBltLa6xuSADLPVdgi\n" +
			"0C+7HgnyrADOu0WWsQYy9tLCJ7kQFUj+VFnrUdc6Gk8TUUJzSvqlOSz5cPjVB5h9\n" +
			"t6gJ6VjFrlR1FEr3cZ+mSNwSxrCNmGM22FDbdJ4QC07KlgMiGeQ=\n" +
			"-----END RSA PRIVATE KEY-----\n"; 
		String host = "usapfs01.usatech.com"; String username = "uccboc"; String remoteDir = "out"; String password = null; int port = 22; String passphrase = null;
	    	
	    	
    	Connection connection = new Connection(host, port);
		try {
			// Connect to the host
	        connection.connect();
	        // authenticate
	        boolean authenticated = false;
	        StringBuilder methods = new StringBuilder();
	        if(privateKey != null) {
	        	authenticated = connection.authenticateWithPublicKey(username, privateKey.toCharArray(), passphrase);
	        	methods.append("private key, ");
	        }
	        if(!authenticated && password != null) {
	        	authenticated = connection.authenticateWithPassword(username, password);
	        	methods.append("password, ");
	        }
	        if(!authenticated) {
	        	throw new Exception("Could not log into " + host + ':' + port + " using " + methods.substring(0, methods.length() - 2));
	        }
	        
	        // The connection is authenticated we can now do some real work!           
	        SFTPv3Client sftp = new SFTPv3Client(connection);
	        try {
	        	// Change directory
	        	String dirPrefix;
	            if(remoteDir != null && remoteDir.trim().length() > 0) 
	            	try {
	            		dirPrefix = sftp.canonicalPath(remoteDir) + "/";
	            	} catch(SFTPException e) {
	            		StringBuilder sb = new StringBuilder();
	            		sb.append("Could not get canonical path for '").append(remoteDir).append("' because ").append(e.getMessage()).append(". Current Directory list:\n");
	            		Vector<?> list = sftp.ls(".");
	            		for(Object o : list) {
	            			if(o instanceof SFTPv3DirectoryEntry) {
	            				sb.append(((SFTPv3DirectoryEntry)o).longEntry).append('\n');
	            			} else {
	            				sb.append(o).append('\n');
	            			}
	            		}
	            		System.out.println(sb.toString());
	            		throw e;
	            	}
	            else
	            	dirPrefix = "";
	            
	            StringBuilder sb = new StringBuilder();
        		Vector<?> list = sftp.ls(dirPrefix + ".");
        		for(Object o : list) {
        			if(o instanceof SFTPv3DirectoryEntry) {
        				sb.append(((SFTPv3DirectoryEntry)o).longEntry).append('\n');
        			} else {
        				sb.append(o).append('\n');
        			}
        		}
        		System.out.println(sb.toString());
        		
	            // Upload a file
	            /*
	            boolean rename = false;
                StringBuilder sb = new StringBuilder();
                if(tmpPrefix != null && (tmpPrefix=tmpPrefix.trim()).length() > 0) {
                	sb.append(tmpPrefix);
                	rename = true;
                }
                sb.append(resource.getName());
                if(tmpSuffix != null && (tmpSuffix=tmpSuffix.trim()).length() > 0) {
                	sb.append(tmpSuffix);
                	rename = true;
                }
                String tmpName = sb.toString();
                SFTPv3FileHandle handle = sftp.createFileTruncate(dirPrefix + tmpName);
                try {
                    InputStream in = resource.getInputStream();
                    try {
	                    byte[] b = byteBufferLocal.get();
	                    int tot = 0;
	                    for(int len = 0; (len=in.read(b)) >= 0; tot += len) 
	                    	sftp.write(handle, tot, b, 0, len);
	                } finally {
                    	in.close();
                    }
                } finally {
                    sftp.closeFile(handle);                  
                }
                if(rename)
                	sftp.mv(dirPrefix + tmpName, dirPrefix + resource.getName());
                	*/
	        } finally {
	        	sftp.close();
	        }
        } catch(IOException ioe) {
        	System.out.println("While sftping file");
        	ioe.printStackTrace();
        } finally {
        	connection.close();
        }
    }
}
