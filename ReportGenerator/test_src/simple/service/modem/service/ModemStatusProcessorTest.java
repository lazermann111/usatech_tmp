package simple.service.modem.service;

import org.junit.Test;

import simple.app.ServiceException;
import simple.service.modem.dao.eseye.EsEyeModemApiImpl;
import simple.service.modem.service.dto.DeviceOperationResult;
import simple.service.modem.service.dto.USATDevice;
import simple.service.modem.service.exception.ModemServiceException;

public class ModemStatusProcessorTest {
    @Test
    public void testMe() throws ServiceException, ModemServiceException {
        EsEyeModemService ms = new EsEyeModemService("USATSiamTest", "<Stored in Password Manager>", "<Stored in Password Manager>");
        ms.setDao(new EsEyeModemApiImpl());
        USATDevice md = new USATDevice();
        md.setIccid("8944538523009064331");
        DeviceOperationResult[] res = ms.getCurrentState(new USATDevice[]{md});
        if (res.length == 0) {
            throw new IllegalStateException("ooops");
        }
    }
}
