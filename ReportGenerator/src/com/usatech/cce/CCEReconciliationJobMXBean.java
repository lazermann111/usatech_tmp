package com.usatech.cce;

import javax.management.MXBean;

@MXBean(true)
public interface CCEReconciliationJobMXBean {

	public abstract Float getAverageTransferMilliseconds();

	public abstract void addElapsedTime(long start, long end);
	
	public abstract int getCurrentWindowCount();
	
	public abstract int getCounter();
	
	public abstract int getMaxConcurrentDatabaseTasks();
	
	public abstract int getMaxConcurrentTasks();
	
	public abstract int getQueuedSftpTasks();
	
	public abstract void resetCounts();
	
	public abstract int getConcurrentTasks();
	
	public abstract int getConcurrentDatabaseTasks();
}
