package com.usatech.cce;

// -- database constants from Const.pm
class CCEConstants { 
    public static final int CCE_FILL_STATE__NEW                       = 1;
    public static final int CCE_FILL_STATE__PROCESSING                = 2;
    public static final int CCE_FILL_STATE__PROCESSING_INCOMPLETE     = 3;
    public static final int CCE_FILL_STATE__PROCESSING_EXACT_MATCH    = 4;
    public static final int CCE_FILL_STATE__PROCESSING_BEST_MATCH     = 5;
    public static final int CCE_FILL_STATE__PROCESSING_COMPLETE_ERROR = 6;
    public static final int CCE_FILL_STATE__DUPLICATE                 = 7;
    public static final int CCE_FILL_STATE__MANUALL_RECONCILED        = 8;

    public static final int FILE_TRANSFER_STATE__PENDING_TRANSFER     = 1;
    public static final int FILE_TRANSFER_STATE__TRANSFER_IN_PROGRESS = 2;
    public static final int FILE_TRANSFER_STATE__TRANSFER_SUCCESS     = 3;
    public static final int FILE_TRANSFER_STATE__TRANSFER_ERROR       = 4;

    public static final int CCE_FILL_LOG_TYPE__NO_MATCHING_BATCH_ID   = 1;
    public static final int CCE_FILL_LOG_TYPE__AMOUNTS_DO_NOT_MATCH   = 2;
    public static final int CCE_FILL_LOG_TYPE__NO_MATCHING_BATCH_FINAL_ERROR   = 3;
    public static final int CCE_FILL_LOG_TYPE__USER_COMMENT           = 4;
    public static final int CCE_FILL_LOG_TYPE__AUTOMATED_RECONCILATION_ANALYSIS_RUN      = 5;
    public static final int CCE_FILL_LOG_TYPE__NO_MATCHING_CCE_FILL_ID   = 6;

    public static final int FILE_TRNSFR_CCE_FILL_TYPE__INPUT_DATA_FILE  = 1;
    public static final int FILE_TRNSFR_CCE_FILL_TYPE__OUTPUT_DATA_FILE = 2;
}