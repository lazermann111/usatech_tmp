/*
 * (C) USA Technologies 2011
 */
package com.usatech.cce;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

import simple.io.Log;

import com.trilead.ssh2.Connection;
import com.trilead.ssh2.SFTPv3Client;


/**
 * Tracks connection state. Connection remains open 
 * while we submit sftp requests, which are executed
 * in order, but asynchronously. 
 * 
 * Will close the connection once all scheduled 
 * file operations have completed. 
 * 
 * @author phorsfield
 *
 */
public class CCESFTPConnection { 
	
	private static final Log log = Log.getLog();
	private boolean processedPathExists = false;

	public void setProcessedPathExists(boolean b) {
		processedPathExists = b;
	}
	public boolean isProcessedPathExists() {
		return processedPathExists;
	}
	
	// Resource lock on SFTP connection, initialized when we know how many files need processing.
	private Semaphore fileOperationsStarted;
	private CountDownLatch readyToClose;
	private Semaphore fileTaskSubmission;		
	private int fileOperationCount;
	private SFTPv3Client sftp;
	
	private long connectionStart;
	private Connection connection;

	public Callable<Void> getCloseSftpClientFutureTask() {
		return new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				
				// If I can get this semaphore, I know all tasks have been submitted.
				fileTaskSubmission.acquire();
				fileTaskSubmission.release(); // but don't keep it, because endOperations are also blocking on this 
				long localCount = readyToClose.getCount();
				log.trace("Waiting for " + localCount + " sftp operations to complete.");
				
				readyToClose.await();
				
				CCEReconciliationJob.getJMXBeanImpl().changeQueuedSftpTaskCount(-1);

		        log.debug("Closing SFTP/SSH session after " + localCount + " SFTP operations over " + (System.currentTimeMillis() - connectionStart) + "ms (from SSH connection initiation).");
		        
				// close connection when finished.
			    sftp.close();
			    
			    // close SSH connection when finished
			    connection.close();
			        
		        log.trace("SFTP/SSH session closed after " + localCount + " SFTP operations over " + (System.currentTimeMillis() - connectionStart) + "ms (from SSH connection initiation).");

		        return null;
		}};
	}
	
	public SFTPv3Client getSftp() {
		return sftp;
	}
	public void setSftp(SFTPv3Client sftp) {
		this.sftp = sftp;
	}		
	
	public CCESFTPConnection(Connection connection, SFTPv3Client sftp, long connectionStart)
	{
		this.sftp = sftp;
		this.connectionStart = connectionStart;
		this.connection = connection;
		fileOperationsStarted = new Semaphore(0); // no file operations unless allowed by the main sftp loop
		fileTaskSubmission = new Semaphore(1); // track main file operation submission loop.
	}
	
	public void addFileOperations(int ops) 
	{
		fileOperationCount += ops;
		fileOperationsStarted.release(ops);
	}
	
	public SFTPv3Client getClient() {			
		return sftp;
	}
	
	/*
	 * Use up the license that allows the threads 
	 * to close the connection while we are still 
	 * submitting tasks.
	 */
	public void beginSubmittingTasks() throws InterruptedException {
		fileTaskSubmission.acquire(); 		
	}
	
	// Create the close latch and release threads waiting to count down the latch
	public void endSubmittingTasks() {
		readyToClose = new CountDownLatch(fileOperationCount);
		fileTaskSubmission.release(); 		
	}
	
	// Use up a license to perform a file operation
	public SFTPv3Client beginFileOperation(String operation, CCEPendingFileTransferBean transferBean) throws InterruptedException {
		if(fileOperationsStarted.availablePermits() < 1)
		{
			transferBean.logWarn("Begin sftp '" + operation + "' with no available file operations currently allowed - this is bad because this is an unregistered request.");
		}
		fileOperationsStarted.acquire(1);
		
		return sftp;
	}
	
	// Blocks for all file operations to have been submitted, then decrements the latch.
	public void endFileOperation(String op, CCEPendingFileTransferBean transferBean) throws InterruptedException {			
		if(readyToClose != null) transferBean.logTrace("Ending sftp '" + op + "' operation with " + readyToClose.getCount() + " operations (includes this) in play/completed.");
		fileTaskSubmission.acquire(1);
		readyToClose.countDown();
		fileTaskSubmission.release(1);			
	}
	public void awaitSftpCompletion() {
		if(readyToClose != null)
		{
			try {
				readyToClose.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
				// return immediately.
			}
		}
	}		
}