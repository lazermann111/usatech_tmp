/** 
 * (C) USA Technologies, Inc. 2011
 * CCEReconciliationTask.java by phorsfield, Sep 26, 2011 1:22:30 PM
 */
package com.usatech.cce;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.FileUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteBufferInputStream;
import simple.io.IOUtils;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.security.SecurityUtils;
import simple.security.crypt.CryptUtils;
import com.trilead.ssh2.Connection;
import com.trilead.ssh2.SFTPv3Client;
import com.trilead.ssh2.SFTPv3FileHandle;
import com.trilead.ssh2.ServerHostKeyVerifier;

import com.usatech.layers.common.QuartzCronScheduledJob;
import com.usatech.transport.KnownHostsVerifier;

/**
 * Responsible for reading files over SFTP from CCE
 * Then parsing them into the reporting database
 * Then matching each record against doc batches.
 * For CCE a batch is confirmed ready to send only
 * when all records are matched.
 * 
 * @author phorsfield
 *
 */
@DisallowConcurrentExecution
public class CCEReconciliationJob extends QuartzCronScheduledJob {
	
	private String keyStoreAliasPrefix; 
	private String knownHostsFileName;
	private long cceFillMatchWaitTimeout=1200000;
	protected int connectTimeout = 3000;
	//setting kexTimeout to 10000 caused "java.net.SocketTimeoutException: The kexTimeout (10000 ms) expired." in the middle of file transfer
	protected int kexTimeout = 0;
	
	// -- runtime
	static final Log log = Log.getLog();
	private KeyStore keyStore;	
	protected ServerHostKeyVerifier hostKeyVerification;
	protected int bufferSize = 512;
	private java.sql.Connection conn;
	static final CCEReconciliationJobMXBeanImpl JMX = new CCEReconciliationJobMXBeanImpl();
	
	// Step 1 - Queue files for reading
	ExecutorService sftpRequests = Executors.newSingleThreadScheduledExecutor();
	// Step 2 - Database Blob and CCE FILL row Write writes
	ExecutorService dbWriteRequests = Executors.newFixedThreadPool(5);
	// Step 3 - Parsing 
	ExecutorService freePool = Executors.newCachedThreadPool();
		
	// -- constants
	private static final String PROCESSED_PATH = "processed";
	private static final String DS_NAME = "REPORT";
	private static final Charset US_ASCII = Charset.forName("US-ASCII");

    // -- regular expressions
	private static final String REGEX_MATCH_INCOMING = "^(CCDATA_2|CCBCDATA_2).{13}";	
	private static final Pattern regexMatchIncoming = Pattern.compile(REGEX_MATCH_INCOMING);

	private static final String REGEX_CCE_FILL_STATE__UNPACK_PARSE_LENGTH = "(.{100}?)|(.{64}?)|(.{45}?)\\n";	
	private static final Pattern regexParser = Pattern.compile(REGEX_CCE_FILL_STATE__UNPACK_PARSE_LENGTH, Pattern.MULTILINE);

	private static final String REGEX_CCE_FILL_STATE__UNPACK_PARSE_RULE1  = "(\\p{ASCII}{5})(\\p{ASCII}{7})(\\p{ASCII}{10})(\\p{ASCII}{8})(\\p{ASCII}{8})(\\p{ASCII}{7})";
	private static final String REGEX_CCE_FILL_STATE__UNPACK_PARSE_RULE2  = "(\\p{ASCII}{5})(\\p{ASCII}{7})(\\p{ASCII}{10})(\\p{ASCII}{10})(\\p{ASCII}{8})(\\p{ASCII}{7})(\\p{ASCII}{3})(\\p{ASCII}{4})(\\p{ASCII}{10})";
	private static final String REGEX_CCE_FILL_STATE__UNPACK_PARSE_RULE3  = "(\\p{ASCII}{5})(\\p{ASCII}{7})(\\p{ASCII}{10})(\\p{ASCII}{10})(\\p{ASCII}{8})(\\p{ASCII}{7})(\\p{ASCII}{3})(\\p{ASCII}{4})(\\p{ASCII}{10})(\\p{ASCII}{36})";
	private static final Pattern regexParserRule1 = Pattern.compile(REGEX_CCE_FILL_STATE__UNPACK_PARSE_RULE1);
	private static final Pattern regexParserRule2 = Pattern.compile(REGEX_CCE_FILL_STATE__UNPACK_PARSE_RULE2);
	private static final Pattern regexParserRule3 = Pattern.compile(REGEX_CCE_FILL_STATE__UNPACK_PARSE_RULE3);

	protected class CCEFillMatchTask implements Runnable {
		private CCESourceCfg source;
		
		public CCEFillMatchTask(CCESourceCfg source) {
			super();
			this.source = source;
		}

		public void run(){
			try{
				  if(!source.getCceFileCountLatch().await(cceFillMatchWaitTimeout, TimeUnit.MILLISECONDS)){
					  log.warn("CCE Fill sftp and inserts takes more than "+cceFillMatchWaitTimeout+"ms to finish.");
				  }
				  log.info("Done CCE fill imports. Start match for customerId="+source.getCustomerId()+" cceFillMatchWaitTimeout="+cceFillMatchWaitTimeout);
				  long startMatchTime=System.currentTimeMillis();
				  DataLayerMgr.executeUpdate("CCE_FILL_MATCH", new Object[]{source.getCustomerId()},true);
				  log.info("Done CCE fill match for customerId="+source.getCustomerId()+" takes "+(System.currentTimeMillis()-startMatchTime)+" ms.");
			}
			catch (InterruptedException e) {
				log.warn("Concurrency problem while importing CCE data for customer Id " + source.getCustomerId(), e);					
			}catch (Exception e) {
				log.warn("Fail to do CCE fill match for customerId="+source.getCustomerId(), e);	
			}
		}
	}
	// -- helper class instances
	protected final ThreadLocal<byte[]> byteBufferLocal = new ThreadLocal<byte[]>() {
		@Override
		protected byte[] initialValue() {
			return new byte[bufferSize];
		}
	};
	
	protected final Predicate justMatchedFiles = new Predicate() {

		@Override
		public boolean evaluate(Object o) {
        	return o instanceof com.trilead.ssh2.SFTPv3DirectoryEntry
        			&& ((com.trilead.ssh2.SFTPv3DirectoryEntry)o).attributes.isRegularFile()
        			&& regexMatchIncoming.matcher(((com.trilead.ssh2.SFTPv3DirectoryEntry)o).filename).matches();
		} 
	};

	protected final Predicate justProcessedPath = new Predicate() {

		@Override
		public boolean evaluate(Object o) {
        	return o instanceof com.trilead.ssh2.SFTPv3DirectoryEntry
        			&& ((com.trilead.ssh2.SFTPv3DirectoryEntry)o).attributes.isDirectory()
        			&& ((com.trilead.ssh2.SFTPv3DirectoryEntry)o).filename.equals(PROCESSED_PATH);
		} 
	};

	
	// -- getter/setters

	public void setKeyStoreAliasPrefix(String keyStoreAliasPrefix) 
	{
		this.keyStoreAliasPrefix = keyStoreAliasPrefix;
	}
	
	public void setKnownHostsFile(String knownHostsFile) {
		this.knownHostsFileName = knownHostsFile;
	}	
	
	// -- business logic
	
	public void init(Properties props) throws IOException {
		
		connectTimeout = Integer.parseInt(props.getProperty("connectTimeout", "10000"));
		kexTimeout = Integer.parseInt(props.getProperty("kexTimeout", "10000"));
	}

	
	/* (non-Javadoc)
	 * @see com.usatech.layers.common.QuartzCronScheduledJob#executePostConfigure(org.quartz.JobExecutionContext)
	 */
	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {

		try {
			File knownHostsFile = new File(this.knownHostsFileName);
			if(!knownHostsFile.exists())
			{
				FileUtils.forceMkdir(knownHostsFile.getParentFile());
				knownHostsFile.createNewFile();
			}
			hostKeyVerification = new KnownHostsVerifier(knownHostsFile);
		} catch (IOException e) { // catch (InvalidHostFileException e) {*/
			logAndFailJob("Error creating known Hosts file verification instance.", e);
		}
		
		conn = null;
		HashSet<CCESourceCfg> sourceSet = new HashSet<CCESourceCfg>(2);
		
		try {
			conn = DataLayerMgr.getConnection("report");
			
			Results results = DataLayerMgr.executeQuery(conn, "CCE_FILL_CONFIG", null);
			
			while(results.next())
			{
				CCESourceCfg source = new CCESourceCfg();
				results.fillBean(source);
				sourceSet.add(source);
			}
		} catch (SQLException e) {
			logAndFailJob("Error reading REPORT database for CCE configuration.", e);
		} catch (DataLayerException e) {
			logAndFailJob("Error reading REPORT database for CCE configuration.", e);
		} catch (BeanException e) {
			logAndFailJob("Error populating results from database for CCE configuration.", e);
		}
		finally
		{
			if(conn != null)
			{
				java.sql.Connection lConn = conn;
				conn = null;
				try {
					lConn.close();
				} catch (SQLException e) {
					log.warn("Failed to close database connection.");
				}
			}
		}

		conn = null;

		try {
			keyStore = simple.security.SecurityUtils.getDefaultKeyStore();
		} catch (Exception e) {
			logAndFailJob("Error opening key store at all, for any customer credentials.", e);
		}

		try
		{
			for(CCESourceCfg src : sourceSet)
			{
				try {
					src.setPrivateKey(getPrivateKeyForCustomer(src));
					sftpImport(src);
					if(src.getCceFileCount()>0){
						//only when we actually import files, we will have match process
						sftpRequests.submit(new CCEFillMatchTask(src));
					}else{
						log.info(" No CCE fill file imports for customerId="+src.getCustomerId()+" No CCE fill match job to be submited.");
					}
				} catch (IOException e) {
					logWarningAndFailJob("Could not execute SFTP task for customer Id " + src.getCustomerId(), e);
				} catch (SQLException e) {
					logWarningAndFailJob("Could not access database to import data for customer Id " + src.getCustomerId(), e);
				} catch (DataLayerException e) {
					logWarningAndFailJob( "Could not access database to import data for customer Id " + src.getCustomerId(), e);
				} catch (BeanException e) {
					logWarningAndFailJob("Could not convert data while importing CCE data for customer Id " + src.getCustomerId(), e);
				} catch (InterruptedException e) {
					logWarningAndFailJob("Concurrency problem while importing CCE data for customer Id " + src.getCustomerId(), e);					
				} catch (ExecutionException e) {
					logWarningAndFailJob("Concurrency problem while importing CCE data for customer Id " + src.getCustomerId(), e);					}
			}
			
			
			// prevent re-entry
			for(CCESourceCfg src : sourceSet)
			{
				if(src.getActiveConnection() != null)
				{
					src.getActiveConnection().awaitSftpCompletion();
				}
			}
		}
		finally
		{
			if(conn != null)
			{
				try {
					conn.close();
				} catch (SQLException e) {
					log.warn("Failed to close database connection.");					
				}
			}
		}
	}
	
	private PrivateKey getPrivateKeyForCustomer(CCESourceCfg src) throws JobExecutionException {
		
		String keyStoreCustomerAlias = src.getkeyStoreAliase(keyStoreAliasPrefix);
		
		try
		{
			if(!keyStore.containsAlias(keyStoreCustomerAlias))
			{
				logAndFailJob("Customer key/certificate '"+ keyStoreCustomerAlias + "' not found in keyStore.");
			}
		}
		catch (KeyStoreException e) {
			logAndFailJob("Error checking contents of key store for customer alias '"+keyStoreCustomerAlias+"'.",e);
		}
		
		try 
		{
			if(!keyStore.isKeyEntry(keyStoreCustomerAlias))
			{
				String message = "Customer key/certificate '"+keyStoreCustomerAlias + "' found in keyStore, but has no key.";
				log.fatal(message);
				throw new JobExecutionException(message);
			}
		}
		catch(JobExecutionException e) {
			throw e;
		} 
		catch (Exception e) {
			String message = "Error checking entry in key store for customer alias '"+keyStoreCustomerAlias+"'.";
			log.fatal(message);
			throw new JobExecutionException(message, e);
		}
		
		KeyStore.Entry ke;
		try {
			char[] keyStorePassword = SecurityUtils.getDefaultKeyStoreConfig().getPassword();
			ke = keyStore.getEntry(keyStoreCustomerAlias, new KeyStore.PasswordProtection(keyStorePassword));
		} catch (Exception e) {
			String message = "Error getting entry from key store for customer alias '"+keyStoreCustomerAlias+"'.";
			log.fatal(message);
			throw new JobExecutionException(message, e);
		}

		if(!(ke instanceof KeyStore.PrivateKeyEntry))
		{
			String message = "Customer key/certificate '"+keyStoreCustomerAlias + "' found in keyStore, but is not a private key.";
			log.fatal(message);
			throw new JobExecutionException(message);				
		}

		KeyStore.PrivateKeyEntry pke = (KeyStore.PrivateKeyEntry)ke;
		
		PrivateKey pk = pke.getPrivateKey();
		
		return pk;
	}

	/**
	 * Log and throw
	 * @param message Fatal error to log
	 * @throws JobExecutionException
	 */
	private void logAndFailJob(String message)
			throws JobExecutionException {		
		log.fatal(message);
		throw new JobExecutionException(message);
	}

	/**
	 * Log and throw
	 * @param message Fatal error to log
	 * @param cause Inner exception
	 * @throws JobExecutionException
	 */
	void logAndFailJob(String message, Exception cause)
			throws JobExecutionException {		
		log.fatal(message, cause);
		throw new JobExecutionException(message, cause);
	}

	/**
	 * Log warning and throw
	 * @param message Warning message to log
	 * @param cause Inner exception
	 * @throws JobExecutionException
	 */
	void logWarningAndFailJob(String message, Exception cause)
			throws JobExecutionException {		
		log.warn(message, cause);
		throw new JobExecutionException(message, cause);
	}
	
	/**
	 * This implements the body of sftp_import.pl
	 * To retrieve files from SFTP.
	 * 
	 * Upon return the processor is actively using an SFTP connection.
	 * It is also recorded in the passed CCESourceCfg object.
	 * 
	 * @param source Everything needed to access the remote server
	 * @param customerId customer Id
	 * @return all successfull transfers 
	 * @throws IOException 
	 * @throws DataLayerException 
	 * @throws SQLException 
	 * @throws BeanException 
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 */
	private CCESFTPConnection sftpImport(CCESourceCfg source) throws IOException, SQLException, DataLayerException, BeanException, InterruptedException, ExecutionException {
		
		String host = source.getHost(); 
		int port = 22;
		
		Connection connection = new Connection(host, port);

		// Connect to the host
		long connectionStart = System.currentTimeMillis();
        connection.connect(hostKeyVerification, connectTimeout, kexTimeout);
        log.debug("Connection to " + host + ":" + port + " took " + (System.currentTimeMillis() - connectionStart) + " ms");

        // authenticate
        boolean authenticated = false;
        	        
        String username = source.getUserName();
        String passphrase = source.getKeyPassphrase();
        StringBuilder methods = new StringBuilder();
        if(source.getPrivateKey() != null) {
        	
//        	if(source.getPrivateKey() instanceof sun.security.rsa.RSAPrivateCrtKeyImpl)
//        	{
//        		sun.security.rsa.RSAPrivateCrtKeyImpl sunRsaPk = (sun.security.rsa.RSAPrivateCrtKeyImpl)source.getPrivateKey();
//        		
////        		com.trilead.ssh2.signature.RSAPrivateKey pk = 
////        				new com.trilead.ssh2.signature.RSAPrivateKey(
////        						sunRsaPk.getPrivateExponent(),
////        						sunRsaPk.getPublicExponent(),
////        						sunRsaPk.getModulus()
////        				);
//        		
//        		authenticated = connection.authenticateWithPublicKey(username, sunRsaPk); // no longer supported with ConnectBot SSH2
//        	}
        	
        	if(!authenticated)
        	{	        		
        		CharArrayWriter pem = new CharArrayWriter();
        		CryptUtils.writePEMKey("RSA PRIVATE KEY", source.getPrivateKey().getEncoded(), pem);
        		authenticated = connection.authenticateWithPublicKey(username, pem.toCharArray(), passphrase);
        		methods.append("private key, ");
        	}
        }
        //...
        if(!authenticated) {
        	throw new IOException("Could not log into " + host + ':' + port + " using " + methods.substring(0, methods.length() - 2));
        }
        
        // The connection is authenticated we can now do some real work!           
        SFTPv3Client sftp = new SFTPv3Client(connection);
        CCESFTPConnection sftpConnection = new CCESFTPConnection(connection, sftp, connectionStart);
        source.setActiveConnection(sftpConnection);
        
        String srcPath = source.getSourcePath();
        if(!srcPath.endsWith("/")) srcPath = srcPath + "/";
        
        String processedPath = srcPath + PROCESSED_PATH + "/";
        
        @SuppressWarnings("rawtypes")
		Vector results = sftp.ls(srcPath);
        
		sftpConnection.setProcessedPathExists(CollectionUtils.exists(results, justProcessedPath));

        // Block tasks from waiting for us to complete until we are ready.
        sftpConnection.beginSubmittingTasks();
        int matchedFileCount=0;
		for(Object o : CollectionUtils.select(results, justMatchedFiles))
		{
			com.trilead.ssh2.SFTPv3DirectoryEntry dirEntry = (com.trilead.ssh2.SFTPv3DirectoryEntry)o;
			
			// Package up parameters
			CCEPendingFileTransferBean transferBean = new CCEPendingFileTransferBean(this, sftpConnection, dirEntry, source, srcPath + dirEntry.filename, processedPath);

			// keep connection for the duration, but lazily create it.
			if(conn == null)
				conn = DataLayerMgr.getConnection(DS_NAME);
    				    				
			// do the work
			if(!doesCceFileExist(transferBean,conn))
			{
				sftpConnection.addFileOperations(2); // read/move
				sftpRequests.submit(transferBean.getFileReadFutureTask());
				JMX.changeQueuedSftpTaskCount(1);
				matchedFileCount++;
			}
			else		        				
			{
				transferBean.setAlreadyExists(true);
				sftpConnection.addFileOperations(1); // move				
				sftpRequests.submit(transferBean.getFileMoveFutureTask());
				JMX.changeQueuedSftpTaskCount(1);
			}
        			
        } // for each file
						
		source.setCceFileCount(matchedFileCount);
		source.setCceFileCountLatch(new CountDownLatch(matchedFileCount));
		sftpConnection.endSubmittingTasks();
		
		freePool.submit(sftpConnection.getCloseSftpClientFutureTask());
		JMX.changeQueuedSftpTaskCount(1);		
		return sftpConnection;
	}

	private boolean doesCceFileExist(CCEPendingFileTransferBean transferBean, java.sql.Connection connection) throws SQLException, DataLayerException, BeanException {
		
		transferBean.CCE_FILL_FILE_EXISTS(connection);		
		
		return transferBean.exists();			
	}

	
	/**
	 * Parallel decomposition context: executing in sftpRequests, single threaded for SFTP safety
	 */
	void mtMoveFile(CCESFTPConnection sftpConnection, CCEPendingFileTransferBean transferBean) throws IOException, InterruptedException {
		
		SFTPv3Client sftp = sftpConnection.beginFileOperation("mv", transferBean);
		JMX.changeQueuedSftpTaskCount(-1);
		transferBean.logMove();
		try 
		{
			
			String remoteFile = transferBean.getProcessedPath() + transferBean.getFileName();
	
			if(!transferBean.getSftpConnection().isProcessedPathExists())
			{
				sftp.mkdir(transferBean.getProcessedPath(), 0700);
			}
			sftp.mv(transferBean.getSourcePath(), remoteFile);
		}
		finally
		{
			sftpConnection.endFileOperation("mv", transferBean);
		}
	}

	/**
	 * Parallel decomposition context: executing in sftpRequests, single threaded for SFTP safety
	 */
	ByteBuffer mtReadFile(CCESFTPConnection sftpConnection, CCEPendingFileTransferBean transferBean) throws IOException, InterruptedException, JobExecutionException
	{		
		ByteBuffer byteBuffer;
		SFTPv3Client sftp = sftpConnection.beginFileOperation("read",transferBean);
		JMX.changeQueuedSftpTaskCount(-1);
		transferBean.logImport();		
		
		try 
		{
			// read buffer		
			SFTPv3FileHandle fh = sftp.openFileRO(transferBean.getSourcePath());
	
			// read at most 2Gb.
			byteBuffer = ByteBuffer.allocate(transferBean.getDirEntry().attributes.size.intValue());
			
			int max = 512;
			int pos = 0;
			
			int rx = 0;
			
			do 
			{
				
				rx = sftp.read( fh, pos, byteBuffer.array(), pos, max);
				if(rx == -1) break;
	
				pos += rx;
			}
			while(rx == max); 
	
			sftp.closeFile(fh);
		}
		finally
		{
			sftpConnection.endFileOperation("read", transferBean);
		}
		
			
		try {
			sftpRequests.submit(transferBean.getFileMoveFutureTask());		
			JMX.changeQueuedSftpTaskCount(1);
			
			dbWriteRequests.submit(transferBean.getBlobWriteFutureTask());
			JMX.changeQueuedDatabaseTaskCount(1);
		}
		catch(Exception e)
		{
			transferBean.logWarningAndFail("Unable to schedule next execution... strange",e);
		}

		return byteBuffer.asReadOnlyBuffer();
	}
	
	/**
	 * Parallel decomposition context: executing in database thread pool, guaranteed after buffer is read
	 */
	Integer mtBufferData(CCEPendingFileTransferBean transferBean) throws IOException, SQLException, BeanException, DataLayerException, InterruptedException, ExecutionException, JobExecutionException
	{
		transferBean.logTrace("mtBuffer");
		java.sql.Connection conn = null;
		ByteBuffer byteBuffer = transferBean.getFileReadFutureTask().get().asReadOnlyBuffer(); // block on read	
		JMX.changeQueuedDatabaseTaskCount(-1);
		transferBean.logDebug("Writing BLOB");
		
		try 
		{
			conn = DataLayerMgr.getConnection(DS_NAME);

			Blob blob = conn.createBlob();
			OutputStream os = blob.setBinaryStream(1);			
			long length = IOUtils.readInto(new ByteBufferInputStream(byteBuffer), os, 128);			
			os.close();
			
			if(length != transferBean.getDirEntry().attributes.size)
			{
				throw new IOException("Wrong number of bytes read from file named '" + transferBean.getFileName() + "'.");
			}
			transferBean.setFileContent(blob);
			
			freePool.submit(transferBean.getParseFutureTask());
			JMX.changeQueuedTaskCount(1);
			
			// ensure same thread
			transferBean.CCE_FILL_FILE_CONTENT(conn);	
			
		}
		catch(Exception x)
		{
			logAndFailJob("Error inserting file after succesful read from SFTP", x);
		}
		finally {
			if(conn != null)
			{
				conn.close();
			}
		}		
		
		return transferBean.getFileTransferId();
	}

	// 
	/**
	 * Parallel decomposition context: parseRequests thread pool
	 * 
	 * Parses text strings from the CCE files.
	 * <pre>
	 *   #  data received are in this order,
     *        if(length($data) == CCE_FILL_STATE__UNPACK_PARSE_RULE1_LENGTH ) {
     *           #  5 char blank
     *           #  7 char outlet number
     *           # 10 char invoice number
     *           #  8 char invoice amount
     *           #  8 char delivery date
     *           #  7 char credit to outlet
     *        } elsif(length($data) == CCE_FILL_STATE__UNPACK_PARSE_RULE2_LENGTH ||
     *                length($data) == CCE_FILL_STATE__UNPACK_PARSE_RULE3_LENGTH ) {
     *           #  data received are in this order,
     *           #  5 char branch number
     *           #  7 char outlet number
     *           # 10 char invoice number
     *           # 10 char invoice amount
     *           #  8 char delivery date
     *           #  7 char credit to outlet
     *           #  3 char route number
     *           #  4 char driver number
     *           # 10 char theoretical cash
     *           # if record is 100 chars long add 36 char blank
     *           # 36 char blank for future use </pre>
	 * @param transferBean
	 * 
	 * @return number of parsed rows.
	 */
	void mtParse(CCEPendingFileTransferBean transferBean) throws NumberFormatException, InterruptedException, ExecutionException, ParseException
	{
		SimpleDateFormat cceDateFormatter = new SimpleDateFormat("yyyyMMdd");
		
		int rowCount = 0;
		transferBean.logTrace("mtParse");
		
		ByteBuffer byteBuffer = transferBean.getFileReadFutureTask().get().asReadOnlyBuffer(); // block on read
		transferBean.logDebug("Data received, can now parse.");
		JMX.changeQueuedTaskCount(-1);
		
		// ... do parsing		
		CharBuffer characters = US_ASCII.decode(byteBuffer);
		Matcher matcher = regexParser.matcher(characters);
		Matcher lineMatcher;
		if(matcher.find())
		{
			do
			{
				int length = matcher.end() - matcher.start();
				switch(length)
				{
					case 45: lineMatcher = regexParserRule1.matcher(characters);				
						break;
					case 64: lineMatcher = regexParserRule2.matcher(characters);
						break;
					case 100: lineMatcher = regexParserRule3.matcher(characters);
						break;
					default:
		        		transferBean.logWarn("Wrong length of line, record: '" + matcher.group() + "'");
						continue;
				}
	
				lineMatcher.region(matcher.start(), matcher.end());
				
				if(!lineMatcher.matches())
				{
					transferBean.logWarn("Unpack rule mismatch for record: '" + matcher.group() + "'");
					continue;
				}
				
				CCEFillRowBean row = transferBean.createCCEFillRow();
				
	            row.put("source",transferBean.getSource());			
	            row.put("branchNum",lineMatcher.group(1));
	            row.put("outlet",lineMatcher.group(2));
	            row.put("invoiceNum",lineMatcher.group(3));
	            row.put("amount",lineMatcher.group(4));
	            
	            try {
	            	row.put("date",cceDateFormatter.parse(lineMatcher.group(5)));
	            }
	            catch(NumberFormatException e)
	            {
	        		transferBean.logWarn("Could not parse date record from CCE file: " + lineMatcher.group(0), e);
	        		throw e;
	            } catch (ParseException e) {
	        		transferBean.logWarn("Could not parse date record from CCE file: " + lineMatcher.group(0), e);
	        		throw e;
				}
	            row.put("creditToOutlet",lineMatcher.group(6));
	            
	            if(length >= 64)
	            {
		            row.put("routeNum",lineMatcher.group(7));
		            row.put("driverNum",lineMatcher.group(8));
		            row.put("theoreticalCashAmount",lineMatcher.group(9));
	
		            if(length == 100) {
		            	// 10th group is not used.
		            }
	            }
				// uploaded content to the server
				dbWriteRequests.submit(transferBean.newRowInsertFutureTask(row));
				JMX.changeQueuedDatabaseTaskCount(1);
				
				++rowCount;
			}
			while(matcher.find());
		}

		transferBean.setParsedRows(rowCount);
		freePool.submit(transferBean.getEndOfFileFutureTask());
		JMX.changeQueuedTaskCount(1);
	}
	
	/**
	 * Parallel decomposition context: running in the database thread pool
	 */
	void mtRowInsert(CCEPendingFileTransferBean transferBean, CCEFillRowBean row) throws InterruptedException, ExecutionException, SQLException, DataLayerException, JobExecutionException
	{
		transferBean.logTrace("mtRowInsert");
		int fileTransferId = transferBean.getBlobWriteFutureTask().get();
		JMX.changeQueuedDatabaseTaskCount(-1);
		
		transferBean.logDebug("FileTransferId acquired from BLOB insert, for row insert: " + fileTransferId);
		
		row.put("fileTransferId", fileTransferId);
		row.put("fileTransferCceFillTypeId", CCEConstants.FILE_TRNSFR_CCE_FILL_TYPE__INPUT_DATA_FILE);
		row.put("fillStateId", CCEConstants.CCE_FILL_STATE__NEW);
		
		java.sql.Connection connection = DataLayerMgr.getConnection(DS_NAME);

		try 
		{
			DataLayerMgr.executeCall(connection, "CCE_FILL_INSERT_RECORD", row);
			
			Integer cceFillId = ConvertUtils.convert(Integer.class, row.get("cceFillId"));
			Integer batchId = ConvertUtils.convert(Integer.class, row.get("batchId"));

			// Record insert and match
			transferBean.logInserted((String)row.get("outlet"), (Date)row.get("date"), cceFillId, batchId);

			if(cceFillId != null)
			{
				connection.commit();
			}
		}
		catch(SQLException e)
		{
			connection.rollback();
			transferBean.logWarningAndFail("Database error inserting record from CCE file: " + row.toString(), e);
		}	
		catch(Exception e)
		{
			connection.rollback();
			transferBean.logWarningAndFail("Could not insert record from CCE file: " + row.toString(), e);
		}
		finally 
		{
			connection.close();
		}
		
	}

	/** Package accessible */
	static CCEReconciliationJobMXBeanImpl getJMXBeanImpl() {
		return JMX;
	}

	public void setCceFillMatchWaitTimeout(long cceFillMatchWaitTimeout) {
		this.cceFillMatchWaitTimeout = cceFillMatchWaitTimeout;
	}

}
