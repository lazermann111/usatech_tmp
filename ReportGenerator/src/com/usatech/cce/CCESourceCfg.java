package com.usatech.cce;

import java.security.PrivateKey;
import java.util.concurrent.CountDownLatch;

/**
 * Encapsulate data necessary to talk to the server.
 * 1 per customer, so hash and equals are defined that way.
 */
public class CCESourceCfg { 

	public CCESFTPConnection getActiveConnection() {
		return activeConnection;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + customerId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CCESourceCfg other = (CCESourceCfg) obj;
		if (customerId != other.customerId)
			return false;
		return true;
	}
	/**
	 * @return database record id 
	 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getOldSftpPassword() {
		return oldSftpPassword;
	}
	public void setOldSftpPassword(String sshAuthPassword) {
		this.oldSftpPassword = sshAuthPassword;
	}
	public String getSourcePath() {
		return sourcePath;
	}
	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public PrivateKey getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}
	
	public String getKeyPassphrase() {
		return keyPassphrase;
	}
	public void setKeyPassphrase(String keyPassphrase) {
		this.keyPassphrase = keyPassphrase;
	}

	//total file we need to handle to cce_fill
	private CountDownLatch cceFileCountLatch;
	private int cceFileCount;
	
	// -- database values
	private int id; 
	private int customerId;
	private String userName;
	private String oldSftpPassword;
	private String keyPassphrase; // NOTE: Currently Unused - keys returned from the key store do not have a password.
	private String sourcePath;
	private String host;

	// -- runtime
	private PrivateKey privateKey = null;
	private CCESFTPConnection activeConnection;

	public void setActiveConnection(CCESFTPConnection conn) {
		this.activeConnection = conn;
		
	}
	
	public String getkeyStoreAliase(String keyStoreAliasPrefix){
		return keyStoreAliasPrefix+customerId;
	}
	public CountDownLatch getCceFileCountLatch() {
		return cceFileCountLatch;
	}
	public void setCceFileCountLatch(CountDownLatch cceFileCountLatch) {
		this.cceFileCountLatch = cceFileCountLatch;
	}
	public int getCceFileCount() {
		return cceFileCount;
	}
	public void setCceFileCount(int cceFileCount) {
		this.cceFileCount = cceFileCount;
	}
	
}