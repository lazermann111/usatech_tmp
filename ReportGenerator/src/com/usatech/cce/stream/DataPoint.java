package com.usatech.cce.stream;

public class DataPoint<KEY, VALUE> {
	KEY key;
	VALUE value;
	DataPoint(KEY key, VALUE value) 
	{
		this.key = key;
		this.value = value;
	}
}