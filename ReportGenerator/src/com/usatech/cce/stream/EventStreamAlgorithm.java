package com.usatech.cce.stream;

public interface EventStreamAlgorithm<KEY, VALUE> extends DataPointListener<KEY, VALUE> {		

	String getName();
	
	VALUE getResultant();
}