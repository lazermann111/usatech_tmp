package com.usatech.cce.stream;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

class ConcurrentSkipListEventStream<KEY, VALUE> implements EventStream<KEY, VALUE> { 
	ConcurrentSkipListMap<KEY, DataPoint<KEY,VALUE>> buffer = new ConcurrentSkipListMap<KEY, DataPoint<KEY,VALUE>>();
	
	@Override
	public boolean addDataPoint(DataPoint<KEY, VALUE> dp) {		
		DataPoint<KEY, VALUE> oldDp = buffer.putIfAbsent(dp.key, dp);
		
		// oldDp = value already there, or null. 
		// In any case, if it's not the new version, nothing was inserted.
		return oldDp != dp; 
	}

	@Override
	public DataPoint<KEY,VALUE> getEarliest() {
		
		Map.Entry<KEY,DataPoint<KEY,VALUE>> firstEntry = buffer.firstEntry();
		
		return firstEntry != null ? firstEntry.getValue() : null;
	}

	@Override
	public int getCountBetween(KEY lowerMilli, KEY upperMilli) {
		return buffer.subMap(lowerMilli,  upperMilli).size();			
	}


	@Override
	public DataPoint<KEY, VALUE> removeEarliest() {
		if(!buffer.isEmpty())
		{
			return buffer.remove(buffer.firstKey());
		}
		else
			return null;
	}

	@Override
	public int size() {
		return buffer.size();
	}
	
}