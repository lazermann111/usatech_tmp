package com.usatech.cce.stream;

import java.util.HashMap;
import java.util.Map;

public class EventStreamFactory<KEY, VALUE> { 	
	
	EventStream<KEY, VALUE> es;
	private EventStreamContentGateway<KEY, VALUE> escm;
	Map<String, EventStreamAlgorithm<KEY, VALUE>> registeredStreamAlgorithms = new HashMap<String, EventStreamAlgorithm<KEY, VALUE>>();
	
	public EventStreamFactory(EventStreamContentGateway<KEY, VALUE> escm, EventStream<KEY, VALUE> es) {
		this.es = es;
		this.setContentManager(escm);
	}
	
	public static <VALUE> EventStreamFactory<Long,VALUE> createSlidingTimeSeriesStream() { return createSlidingTimeSeriesStream(5000 /*5 seconds*/); }
	public static <VALUE> EventStreamFactory<Long,VALUE> createSlidingTimeSeriesStream(long maxTimeMs)
	{
		EventStreamContentGateway<Long, VALUE> escm = new RecentTimeEventStreamContentGateway<VALUE>(maxTimeMs, Long.MIN_VALUE, Long.MAX_VALUE);
		EventStream<Long, VALUE> es = new ConcurrentSkipListEventStream<Long, VALUE>();
		
		escm.setEventStream(es);
		
		return new EventStreamFactory<Long, VALUE>(escm, es);
	}

	public void register(String streamAlgoName, EventStreamAlgorithm<KEY, VALUE> streamAlgorithm) {
			
		registeredStreamAlgorithms.put(streamAlgorithm.getName() + ":" + streamAlgoName, streamAlgorithm);
		escm.addDataPointListener(streamAlgorithm);
	}

	public EventStreamContentGateway<KEY, VALUE> getContentManager() {
		return escm;
	}

	public void setContentManager(EventStreamContentGateway<KEY, VALUE> escm) {
		this.escm = escm;
	}		
}