package com.usatech.cce.stream;

public interface DataPointListener<KEY, VALUE> {
	void newDataPoint(EventStream<KEY, VALUE> stream, DataPoint<KEY, VALUE> dp);
	void loseDataPoint(EventStream<KEY, VALUE> stream, DataPoint<KEY, VALUE> dp);
}