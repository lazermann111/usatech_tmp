package com.usatech.cce.stream;

public interface EventStream<KEY, VALUE> { 	
	boolean addDataPoint(DataPoint<KEY, VALUE> dataPoint);
	int getCountBetween(KEY lowerMilli, KEY upperMilli);		
	DataPoint<KEY, VALUE> getEarliest();
	DataPoint<KEY, VALUE> removeEarliest();
	int size();		
}