package com.usatech.cce.stream;

public class MovingAverage implements EventStreamAlgorithm<Long, Float> {

	float total;
	int count;
	Float zero = 0.0f;
	
	@Override
	public String getName() {
		return "MovingAverage";
	}

	@Override
	public void newDataPoint(EventStream<Long, Float> stream,
			DataPoint<Long, Float> dp) {
	
		total += dp.value.floatValue();
		++count;
	}

	@Override
	public void loseDataPoint(EventStream<Long, Float> stream,
			DataPoint<Long, Float> dp) {
		
		total -= dp.value.floatValue();
		--count;			
	}

	@Override
	public Float getResultant() {
		
		if(count > 0) return new Float(total / (float)count);
		else return zero;
	}
	
}