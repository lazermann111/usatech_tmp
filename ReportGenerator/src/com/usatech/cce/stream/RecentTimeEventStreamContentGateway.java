package com.usatech.cce.stream;

import java.util.HashSet;
import java.util.Set;

public class RecentTimeEventStreamContentGateway<VALUE> implements EventStreamContentGateway<Long, VALUE> {

	Set<DataPointListener<Long, VALUE>> listeners = new HashSet<DataPointListener<Long, VALUE>>();
	EventStream<Long, VALUE> stream;
	
	Long minMilli;
	Long maxMilli;
	
	Long  allowedTimeMilli;
	
	RecentTimeEventStreamContentGateway(Long allowedTimeMilli, Long min, Long max)
	{
		this.allowedTimeMilli = allowedTimeMilli;
		this.minMilli = max;
		this.maxMilli = min;
	}
	
	@Override
	public void setEventStream(EventStream<Long, VALUE> stream) {
		this.stream = stream;			
	}

	@Override
	public Long newIncomingDataPoint(Long timeMilli, VALUE value) {
	
		DataPoint<Long, VALUE> dp;
		
		// increase time boundary for new data point.
		if(maxMilli.compareTo(timeMilli) < 0 ) maxMilli = timeMilli;
		
		// check window size
		if(maxMilli - minMilli > allowedTimeMilli)
		{
			minMilli = maxMilli - allowedTimeMilli;			
			do
			{
				dp = stream.removeEarliest();
				
				if(dp == null) break;
				
				for(DataPointListener<Long, VALUE> listener : listeners) 
				{
					listener.loseDataPoint(stream, dp);
				}
				
				dp = stream.getEarliest();					
			}
			while(dp != null && minMilli.compareTo(dp.key) > 0);

			// new min milli
			if(dp != null) 
			{
				minMilli = dp.key;
			}
			else
			{
				minMilli = maxMilli; 
			}
		}

		// now insert into our truncated buffer, the new value.
		dp = new DataPoint<Long, VALUE>(timeMilli, value);

		if(minMilli > dp.key) 
		{
			minMilli = dp.key;
		}
			
		if(stream.addDataPoint(dp)) // returns true if the data point has a unique key, so we don't overwrite.
		{				
			for(DataPointListener<Long, VALUE> listener : listeners) 
			{
				listener.newDataPoint(stream, dp);
			}
		}

		return timeMilli;
	}

	@Override
	public void addDataPointListener(DataPointListener<Long, VALUE> listener) {			
		listeners.add(listener);		
	}

	@Override
	public void removeDataPointListener(
			DataPointListener<Long, VALUE> listener) {			
		listeners.remove(listener);
	}

	@Override
	public int getDataPointCount() {
		return stream.size();
	}
	
}