package com.usatech.cce.stream;

public interface EventStreamContentGateway<KEY, VALUE> { 				
	void setEventStream(EventStream<KEY, VALUE> stream);
	KEY newIncomingDataPoint(KEY timeMilli, VALUE value);
	void addDataPointListener(DataPointListener<KEY, VALUE> listener);
	void removeDataPointListener(DataPointListener<KEY, VALUE> listener);
	int getDataPointCount();
}