/** 
 * (C) USA Technologies, Inc. 2011
 * CCEReconciliationJobMBean.java by phorsfield, Oct 3, 2011 9:45:44 AM
 */
package com.usatech.cce;

import java.lang.management.ManagementFactory;
import java.util.concurrent.atomic.AtomicInteger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import com.usatech.cce.stream.EventStreamFactory;
import com.usatech.cce.stream.MovingAverage;

import simple.io.Log;

/**
 * 
 * This class managements statistics and reporting over JMX
 * for the CCE Reconciliaton job 
 * 
 * @author phorsfield
 *
 */
public class CCEReconciliationJobMXBeanImpl implements CCEReconciliationJobMXBean {
	
	// -- runtime
	private static final Log log = Log.getLog();
	private static int instanceId = 0;
	private EventStreamFactory<Long, Float> eventStreamProcessor;
	private MovingAverage movingAverage;
	private AtomicInteger count = new AtomicInteger(0);
	private AtomicInteger maxConcurrentDatabaseTasks = new AtomicInteger(0);
	private AtomicInteger maxConcurrentTasks = new AtomicInteger(0);
	private AtomicInteger queuedSftpTasks = new AtomicInteger(0);
	private AtomicInteger concurrentDatabaseTasks = new AtomicInteger(0);
	private AtomicInteger concurrentTasks = new AtomicInteger(0);
		
	public CCEReconciliationJobMXBeanImpl() { 
		eventStreamProcessor = EventStreamFactory.createSlidingTimeSeriesStream(5000 /*ms - 5 seconds*/ );
		movingAverage = new MovingAverage();
		
		String instanceStr = Integer.toString(++instanceId);
		eventStreamProcessor.register(instanceStr, movingAverage);

		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		ObjectName self;
		try {
			self = new ObjectName("CCEReconciliation:instance=" + instanceStr);
			mbs.registerMBean(this, self);
		} catch (MalformedObjectNameException e) {
			fail(e);
		} catch (NullPointerException e) {
			fail(e);
		} catch (InstanceAlreadyExistsException e) {
			fail(e);
		} catch (MBeanRegistrationException e) {
			fail(e);
		} catch (NotCompliantMBeanException e) {
			fail(e);
		}
	}
	
	/**
	 * Logs a failure to establish JMX, but does not propagate the failure. 
	 * @param e a JMX exception 
	 */
	private void fail(Exception e) {		
		log.warn("Could not register MBean for CCE Reconciliation job: ", e);
	}

	/**
	 * Update statistic
	 */
	@Override
	public void addElapsedTime(long start, long end)
	{
		count.incrementAndGet();
		eventStreamProcessor.getContentManager().newIncomingDataPoint(System.currentTimeMillis(), (float)(end - start));	
	}

	/** 
	 * Retrieve statistic
	 */
	@Override
	public Float getAverageTransferMilliseconds()
	{
		return movingAverage.getResultant();
	}

	@Override
	public int getCurrentWindowCount() {
		return eventStreamProcessor.getContentManager().getDataPointCount();
	}

	@Override
	public int getCounter() {		
		return count.get();
	}

	@Override
	public int getMaxConcurrentDatabaseTasks() {
		return maxConcurrentDatabaseTasks.get();
	}

	@Override
	public int getMaxConcurrentTasks() {
		return maxConcurrentTasks.get();
	}

	@Override
	public int getQueuedSftpTasks() {		
		return queuedSftpTasks.get();
	}
	
	public void setQueuedSftpTasks(int count) {
		queuedSftpTasks.set(count);
	}
	public void setCurrentConcurrentDatabaseTasks(int count)
	{
		int current;
		
		do 
		{
			current = maxConcurrentDatabaseTasks.get();
			if(count > current)
			{
				if(maxConcurrentDatabaseTasks.compareAndSet(current, count))
				{
					break;
				}
			}
		}		
		while(count > current);
		
		concurrentDatabaseTasks.set(count);		
	}
	
	public void setCurrentConcurrentTasks(int count)
	{
		int current;
		
		do 
		{
			current = maxConcurrentTasks.get();
			if(count > current)
			{
				if(maxConcurrentTasks.compareAndSet(current, count))
				{
					break;
				}
			}
		}		
		while(count > current);
		
		concurrentTasks.set(count);
	}

	@Override
	public void resetCounts() {
		concurrentDatabaseTasks.set(0);
		concurrentTasks.set(0);
		maxConcurrentDatabaseTasks.set(0);
		maxConcurrentTasks.set(0);
		queuedSftpTasks.set(0);
		count.set(0);
	}

	@Override
	public int getConcurrentTasks() {
		return concurrentTasks.get();
	}

	@Override
	public int getConcurrentDatabaseTasks() {
		return concurrentDatabaseTasks.get();
	}

	public void changeQueuedSftpTaskCount(int delta) {
		queuedSftpTasks.addAndGet(delta);	
		changeQueuedTaskCount(delta);
	}

	public void changeQueuedDatabaseTaskCount(int delta) {		
		
		changeQueuedTaskCount(delta);
		
		int count = concurrentDatabaseTasks.addAndGet(delta);
		int current;
		do 
		{
			current = maxConcurrentDatabaseTasks.get();
			if(count > current)
			{
				if(maxConcurrentDatabaseTasks.compareAndSet(current, count))
				{
					break;
				}
			}
		}		
		while(count > current);
	}
	
	public void changeQueuedTaskCount(int delta) {		
		
		int count = concurrentTasks.addAndGet(delta);
		int current;
		do 
		{
			current = maxConcurrentTasks.get();
			if(count > current)
			{
				if(maxConcurrentTasks.compareAndSet(current, count))
				{
					break;
				}
			}
		}		
		while(count > current);		
	}

}
