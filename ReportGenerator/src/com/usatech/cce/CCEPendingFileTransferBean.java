package com.usatech.cce;

import java.nio.ByteBuffer;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.Semaphore;
import java.util.Date;

import org.quartz.JobExecutionException;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.BeanException;
import com.trilead.ssh2.SFTPv3DirectoryEntry;


/**
 * Encapsulates the state of an in progress file transfer.
 * 
 * Lifetime of a file transfer:
 * 
 * 1) Read from SFTP   1=>2
 * 2) Buffer into a Blob (requires DB)  2=>3,4,5
 * 3) Insert Blob 3=>6
 * 4) Parse Rows from file 4=>6
 * 5) Move file to final location 5=>(end)
 * 6) Insert rows 6=>7
 * 7) Indicate completion of all inserts for a file 7=>(end)
 * 
 * @author phorsfield
 *
 */
public class CCEPendingFileTransferBean {

	// -- pending job and environment
	private final CCEReconciliationJob cceReconciliationJob;
	private CCESFTPConnection sftpConnection;
	private CCESourceCfg source;
	private SFTPv3DirectoryEntry dirEntry;
	
	// -- runtime state
	private String processedPath;
	private String sourcePath;
	private boolean alreadyExists;
	private int found;

	// -- database fields
	private Blob fileContent;
	private int mimeTypeId;
	private int fileContentId;
	private int fileTransferStateId;
	private char fileTransferDirectionId;
	private int fileTransferId;
	
	// -- detailed logging fields
	private long startFileTransferTimeMillis;
	private Semaphore rowInsertsCompleted = new Semaphore(0);
	private int parsedRows; 
	private boolean imported;
	
	
	// -- parallel task definitions
	private FutureTask<Void> fileMoveFutureTask = new FutureTask<Void>(new Callable<Void>() {

		@Override
		public Void call() throws Exception {			
			cceReconciliationJob.mtMoveFile(sftpConnection, CCEPendingFileTransferBean.this);
			return null;
		}}); 

	private FutureTask<ByteBuffer> fileReadFutureTask = new FutureTask<ByteBuffer>(new Callable<ByteBuffer>() {

		@Override
		public ByteBuffer call() throws Exception {			
			try 
			{
				return cceReconciliationJob.mtReadFile(sftpConnection, CCEPendingFileTransferBean.this);
			}
			catch(Exception x)
			{
				cceReconciliationJob.logAndFailJob("Error Reading from SFTP", x);
			}
			return null;
		}}); 

	private FutureTask<Integer> blobWriteFutureTask = new FutureTask<Integer>(new Callable<Integer>() {

		@Override
		public Integer call() throws Exception {			
			try 
			{
				return cceReconciliationJob.mtBufferData(CCEPendingFileTransferBean.this);
			}
			catch(Exception x)
			{
				cceReconciliationJob.logAndFailJob("Error Writing Blob", x);
			}
			return null;
		}}); 


	private FutureTask<?> parseFutureTask = new FutureTask<Void>(new Callable<Void>() {

		@Override
		public Void call() throws Exception {
			try 
			{
				cceReconciliationJob.mtParse(CCEPendingFileTransferBean.this);
			}
			catch(Exception x)
			{
				cceReconciliationJob.logAndFailJob("Error Parsing", x);
			}
			return null;
		}}); 

	class IndividualRowInsertCallable implements Callable<Void> {
		private CCEFillRowBean row;

		@Override
		public Void call() throws Exception {			
			try 
			{
			    cceReconciliationJob.mtRowInsert(CCEPendingFileTransferBean.this, row);
			}
			catch(Exception x)
			{
				cceReconciliationJob.logAndFailJob("Error Inserting Row ", x);
			}
			finally
			{
				rowInsertsCompleted.release(1);
			}
			return null;				 
		}
					
		IndividualRowInsertCallable(CCEFillRowBean row)
		{
			this.row = row;
		}			
	}

	private FutureTask<?> endOfFileFutureTask = new FutureTask<Void>(new Callable<Void>() {
		@Override
		public Void call() throws Exception {
			try 
			{
				rowInsertsCompleted.acquire(parsedRows);
				CCEReconciliationJob.getJMXBeanImpl().changeQueuedTaskCount(-1);
				CCEReconciliationJob.getJMXBeanImpl().addElapsedTime(startFileTransferTimeMillis, System.currentTimeMillis());
				source.getCceFileCountLatch().countDown();
			}
			catch(Exception x)
			{
				cceReconciliationJob.logAndFailJob("Error Recording End Of File", x);
			}
			return null;
		}}); 

	public CCEPendingFileTransferBean(CCEReconciliationJob cceReconciliationJob, CCESFTPConnection sftpConnection, SFTPv3DirectoryEntry dirEntry, CCESourceCfg source, String srcPath, String processedPath) {
		this.cceReconciliationJob = cceReconciliationJob;
		this.sftpConnection = sftpConnection;
		this.dirEntry = dirEntry;
		this.source = source;
		this.sourcePath = srcPath;
		this.processedPath = processedPath;
		this.imported = false;
		this.found = -1;
		this.alreadyExists = false;
		this.startFileTransferTimeMillis = System.currentTimeMillis();

	}
	
	// ===== database accessors
	public void CCE_FILL_FILE_EXISTS(java.sql.Connection connection) throws BeanException, SQLException, DataLayerException {
		if(connection == null)
		{
			throw new BeanException("Do not call before connecting to the database.");
		}
		
		DataLayerMgr.selectInto(connection, "CCE_FILL_FILE_EXISTS", this);
	}	
	
	public void CCE_FILL_FILE_CONTENT(java.sql.Connection connection) throws BeanException, SQLException, DataLayerException {
		if(connection == null)
		{
			throw new BeanException("Do not call before connecting to the database.");
		}
	
		this.setMimeTypeId(1);
		this.setFileTransferStateId(3);			
		this.setFileTransferDirectionId('I');
		
		DataLayerMgr.executeCall(connection, "CCE_FILL_FILE_CONTENT", this);
		
		assert this.fileContentId != 0;
	}

	public boolean exists() throws BeanException {
		if(found == -1)
		{
			throw new BeanException("Do not call before querying the database."); 
		}
		return found > 0;
	}
	
	
	//===== PARRALELIZATION
	public FutureTask<Integer> getBlobWriteFutureTask() {
		return blobWriteFutureTask;
	}
	public FutureTask<ByteBuffer> getFileReadFutureTask() {
		return fileReadFutureTask;
	}
	public FutureTask<Void> getFileMoveFutureTask() {
		return fileMoveFutureTask;
	}		
	public FutureTask<Void> newRowInsertFutureTask(CCEFillRowBean row)
	{
		return new FutureTask<Void>(new IndividualRowInsertCallable(row));
	}		
	public FutureTask<?> getEndOfFileFutureTask() {
		return endOfFileFutureTask;
	}		
	public FutureTask<?> getParseFutureTask() {
		return parseFutureTask;
	}
	
	//===== LOGGING
	public void logTrace(String string) {
		CCEReconciliationJob.log.trace(getFileName() + ": " + string);
	}
	private void logBizOp(String string) {
		CCEReconciliationJob.log.info(getFileName() + ": " + string);
	}
	public void logWarn(String string) {
		CCEReconciliationJob.log.warn(getFileName() + ": " + string);			
	}
	public void logWarn(String string, Exception e) {
		CCEReconciliationJob.log.warn(getFileName() + ": " + string, e);
	}
	public void logWarningAndFail(String string, Exception e) throws JobExecutionException {
		this.cceReconciliationJob.logWarningAndFailJob(getFileName() + ": " + string, e);			
	}
	public void logDebug(String string) {
		CCEReconciliationJob.log.debug(getFileName() + ": " + string);			
	}	
	public void logImport() {
		logDebug("Found file, importing.");			
	}
	
	private void logParsedRows(int count) {
		
		if(count > 0)
		{
			logBizOp("Found on SFTP site and parsed " + count + " row(s).");
		}
		else 
		{
			logWarn("No matched content in file.");
		}
	}
	public void logMove() {
		if(alreadyExists)
		{
			logBizOp("Already exists in the database.");
		}
		else
		{
			logDebug("Moving to " + this.processedPath + dirEntry.filename);
		}
	}

	public void logInserted(String outlet, Date date, Integer cceFillId, Integer batchId) {
		if(batchId != null)
		{
			logBizOp("Confirmed batch " + batchId);
		}
		
		if(cceFillId != null)
		{
			logDebug("Inserted CCE FILL record id = " + cceFillId);
		}
		else
		{
			logWarn("Failed to insert CCE FILL record for outlet '" + outlet + "' and date '" + date + "'");
		}
	}
	
	//===== CREATIONAL 
	public CCEFillRowBean createCCEFillRow() {			
		return new CCEFillRowBean();
	}
	
	//===== GETTER/SETTER
	public boolean isAlreadyExists() {
		return alreadyExists;
	}
	public void setAlreadyExists(boolean alreadyExists) {
		this.alreadyExists = alreadyExists;
	}
	public int getFileTransferId() {
		return fileTransferId;
	}
	public void setFileTransferId(int fileTransferId) {
		this.fileTransferId = fileTransferId;
	}
	public CCESFTPConnection getSftpConnection() {
		return sftpConnection;
	}
	public void setSftpConnection(CCESFTPConnection sftpConnection) {
		this.sftpConnection = sftpConnection;
	}
	public int getFileContentId() {
		return fileContentId;
	}
	public void setFileContentId(int fileContentId) {
		this.fileContentId = fileContentId;
	}
	public int getMimeTypeId() {
		return mimeTypeId;
	}
	public void setMimeTypeId(int mimeTypeId) {
		this.mimeTypeId = mimeTypeId;
	}
	public int getFileTransferStateId() {
		return fileTransferStateId;
	}
	public void setFileTransferStateId(int fileTransferStateId) {
		this.fileTransferStateId = fileTransferStateId;
	}
	public char getFileTransferDirectionId() {
		return fileTransferDirectionId;
	}
	public void setFileTransferDirectionId(char fileTransferDirectionId) {
		this.fileTransferDirectionId = fileTransferDirectionId;
	}
	public Blob getFileContent() {
		return fileContent;
	}
	public void setFileContent(Blob fileContent) {
		this.fileContent = fileContent;
	}
	public String getSourcePath() {
		return sourcePath;
	}
	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}
	public SFTPv3DirectoryEntry getDirEntry() {
		return dirEntry;
	}
	public void setDirEntry(SFTPv3DirectoryEntry dirEntry) {
		this.dirEntry = dirEntry;
	}
	public int getFound() {
		return found;
	}
	public void setFound(int found) {
		this.found = found;
	}
	public boolean isImported() {
		return imported;
	}
	public void setImported(boolean imported) {
		this.imported = imported;
	}
	public String getProcessedPath() {
		return processedPath;
	}
	public void setProcessedPath(String remotePath) {
		this.processedPath = remotePath;
	}
	public CCESourceCfg getSource() {
		return source;
	}
	public void setSource(CCESourceCfg source) {
		this.source = source;
	}
	public void setParsedRows(int count) {
		
		this.parsedRows = count;

		logParsedRows(count);
	}

	// clever getters
	public String getFileName() {			
		return dirEntry.filename;
	}
	
	public void setFilename(String fileName) {			
		// n/a
	}
}