package com.usatech.cce;

import java.util.HashMap;

/*
 * This bean represents a record in the table 
 * usat_custom.file_trnsfr_cce_fill
 */
@SuppressWarnings("serial")
public class CCEFillRowBean extends HashMap<String,Object> {

	@Override
	public String toString() {
		return "CCEFillRowBean [entrySet()=" + entrySet() + "]";
	} 		
}