package com.usatech.report;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.NotAuthorizedException;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.text.MessageFormat;
import simple.text.StringUtils;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.sheet.SheetUtils;
import simple.util.sheet.SheetUtils.RowValuesIterator;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.process.ProcessResponse;
import com.usatech.layers.common.process.Processor;
import com.usatech.layers.common.util.WebHelper;

/**
 * Uploads device data from an Excel spreadsheet into the database.
 * Returns the number of devices successfully activated. The Excel spreadsheet should
 * have the following columns at the top with each row below containing the
 * appropriate information (* indicates field is required):
 * ACTION *,
 * SERIAL_NUMBER or EPORT_SERIAL_NUMBER *,
 * ASSET_NUMBER or OUTLET_NUMBER,
 * TELEPHONE,
 * OUTSIDE_LINE,
 * LOCATION_NAME *,
 * ADDRESS_1 or ADDRESS,
 * ADDRESS_2,
 * CITY *,
 * STATE *,
 * ZIP *,
 * LOCATION_DETAILS,
 * LOCATION_TYPE,
 * TIME_ZONE *,
 * MACHINE_MAKE *,
 * MACHINE_MODEL *,
 * PRODUCT_TYPE *,
 * VEND_MODE or VENDS_PER_TRAN *,
 * AUTH_MODE *,
 * RECEIPT,
 * DEX_CAPTURE,
 * REGION,
 * BANK_ACCT_ID,
 * DEALER_NAME,
 * PAY_SCHEDULE,
 * CURRENCY
 *
 * @author bkrug
 *
 */
public class MassDeviceUpdate implements Processor {
	private static final Log log = Log.getLog();
	protected static final Map<String, Character> actionMap = new HashMap<String, Character>();
	protected static final Map<String, String> authModeMap = new HashMap<String, String>();
	protected static final Map<String, Integer> vendModeMap = new HashMap<String, Integer>();
	protected static final Map<String, String> dexModeMap = new HashMap<String, String>();
	protected static final Map<String, String> yesNoMap = new HashMap<String, String>();
	protected static final Map<String, String> signatureCaptureMap = new HashMap<String, String>();
	protected static final Map<String, Integer> timeZoneMap = new HashMap<String, Integer>();
	protected static final Map<String, Integer> paySchedMap = new HashMap<String, Integer>();
	protected static final Map<String, Integer> currencyMap = new HashMap<String, Integer>();
	protected static String timeZoneInstructs;
	protected static final Map<String, String> instructMap = new HashMap<String, String>();

	static {
		authModeMap.put("F", "F");
		authModeMap.put("FULL", "F");
		authModeMap.put("L", "L");
		authModeMap.put("LOCAL", "L");
		authModeMap.put("N", "N");
		authModeMap.put("NONE", "N");

		vendModeMap.put("1", 1);
		vendModeMap.put("S", 1);
		vendModeMap.put("SINGLE", 1);
		vendModeMap.put("10", 10);
		vendModeMap.put("M", 10);
		vendModeMap.put("MULTIPLE", 10);
		vendModeMap.put("2", 2);
		vendModeMap.put("3", 3);
		vendModeMap.put("4", 4);
		vendModeMap.put("5", 5);
		vendModeMap.put("6", 6);
		vendModeMap.put("7", 7);
		vendModeMap.put("8", 8);
		vendModeMap.put("9", 9);

		dexModeMap.put("N", "N");
		dexModeMap.put("NONE", "N");
		dexModeMap.put("NO", "N");
		dexModeMap.put("D", "D");
		dexModeMap.put("DEX", "D");
		dexModeMap.put("YES", "D");
		dexModeMap.put("Y", "D");
		dexModeMap.put("A", "A");
		dexModeMap.put("ALERT", "A");

		yesNoMap.put("Y", "Y");
		yesNoMap.put("YES", "Y");
		yesNoMap.put("NO", "N");
		yesNoMap.put("N", "N");

		signatureCaptureMap.put("E", "E");
		signatureCaptureMap.put("M", "M");
		signatureCaptureMap.put("ELEC", "E");
		signatureCaptureMap.put("MAN", "M");
		signatureCaptureMap.put("ELECTRONIC", "E");
		signatureCaptureMap.put("MANUAL", "M");

		actionMap.put("A", 'A');
		actionMap.put("ACTIVATE", 'A');
		actionMap.put("C", 'C');
		actionMap.put("CHANGE", 'C');
		actionMap.put("D", 'D');
		actionMap.put("DEACTIVATE", 'D');
		actionMap.put("I", 'I');
		actionMap.put("IGNORE", 'I');

		paySchedMap.put("1", 1);
		paySchedMap.put("A", 1);
		paySchedMap.put("AS ACCUMULATED", 1);
		paySchedMap.put("2", 2);
		paySchedMap.put("F", 2);
		paySchedMap.put("FILL", 2);
		paySchedMap.put("FILL TO FILL", 2);
		paySchedMap.put("4", 4);
		paySchedMap.put("8", 8);
		paySchedMap.put("AS EXPORTED", 8);

		currencyMap.put("1", 1);
		currencyMap.put("USD", 1);
		currencyMap.put("840", 1);
		currencyMap.put("2", 2);
		currencyMap.put("CAD", 2);
		currencyMap.put("124", 2);

		timeZoneMap.put("EST", 0);
		timeZoneMap.put("PST", 12);
		timeZoneMap.put("MST", 14);
		timeZoneMap.put("CST", 18);
		timeZoneMap.put("HST", 6);
		timeZoneMap.put("AKST", 10);
		timeZoneMap.put("AST", 25);

		StringBuilder sb = new StringBuilder("Please enter one of the following: ");
		for(Iterator<String> iter = timeZoneMap.keySet().iterator(); iter.hasNext();) {
			String s = iter.next();
			if(iter.hasNext())
				sb.append(", ");
			else sb.append(", or ");
			sb.append(s);
		}

		sb.append('.');
		timeZoneInstructs = sb.toString();

		instructMap.put("AUTH_MODE", "Please specify 'Full' for live authorization, 'Local' for local authorization, or 'None' for no credit cards accepted.");
		instructMap.put("RECEIPT", "Please specify 'Yes' to have the device print a receipt if available, or 'No' to not print a receipt.");
		instructMap.put("DEX_CAPTURE", "Please specify 'Yes' to have the device capture dex data if available, or 'No' to not capture dex data.");
		//instructMap.put("ACTIVATE_FLAG", "Please specify 'Yes' to activate, or 'No' to skip this row.");
		instructMap.put("ACTION", "Please specify 'A' to activate, 'C' to change, 'D' to deactivate, or 'I' to ignore.");
		instructMap.put("VEND_MODE", "Please specify 'S' for Single Vend mode, or 'M' for Multi-Vend mode.");
		instructMap.put("PAY_SCHEDULE", "Please specify '1' for As Accumulated Payment, or '2' for Fill to Fill Payment.");
		instructMap.put("CURRENCY", "Please specify '1' for United States Dollars.");
		instructMap.put("SIGNATURE_CAPTURE", "Please specify 'E' for Electronic or 'M' for Manual.");
	}

	public static enum FileType {
		EXCEL, FIXED_V1
	};

	protected static class ValueMissingException extends Exception {
		private static final long serialVersionUID = 5L;
		protected String name;

		public ValueMissingException(String name) {
			super();
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	protected static class StringTooLongException extends ValueInvalidException {
		private static final long serialVersionUID = 10L;
		protected int maxlength;

		public StringTooLongException(String name, Object value, int maxlength) {
			super(name, value);
			this.maxlength = maxlength;
		}

		public int getMaxlength() {
			return maxlength;
		}
	}

	protected static class StringConversionException extends ValueInvalidException {
		private static final long serialVersionUID = 6L;

		public StringConversionException(String name, Object value) {
			super(name, value);
		}
	}

	protected static class SpecialValueInvalidException extends ValueInvalidException {
		private static final long serialVersionUID = 8L;

		public SpecialValueInvalidException(String message, String name, Object value) {
			super(message, name, value);
		}
	}

	protected static class BankAcctValueInvalidException extends ValueInvalidException {
		private static final long serialVersionUID = 9L;

		public BankAcctValueInvalidException(String name, Object value) {
			super(name, value);
		}
	}

	protected static class ValueInvalidException extends Exception {
		private static final long serialVersionUID = 4L;
		protected String name;
		protected Object value;

		public ValueInvalidException(String message, String name, Object value) {
			super(message);
			this.name = name;
			this.value = value;
		}

		public ValueInvalidException(String name, Object value) {
			super();
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public Object getValue() {
			return value;
		}
	}

	protected static class NumberConversionException extends ValueInvalidException {
		private static final long serialVersionUID = 7L;
		protected String name;
		protected Object value;

		public NumberConversionException(String name, Object value) {
			super(name, value);
		}
	}

	public static class RowInfo {
		public String serialNum;
		public int rowNum;
		public char actionCode;
	}

	public static interface Messages {
		public void processed(RowInfo rowInfo, String messageKey, Object... parameters) throws ServiceException;

		public void skipped(RowInfo rowInfo, String messageKey, Object... parameters) throws ServiceException;

		public void skippedSummary(String messageKey, Object... parameters) throws ServiceException;

		public void failed(RowInfo rowInfo, String field, Object value, String messageKey, Object... parameters) throws ServiceException;

		public void header(String messageKey, Object... parameters) throws ServiceException;

		public void footer(String messageKey, Object... parameters) throws ServiceException;
	}

	protected static final char SPACE_CHAR = ' ';
	protected static final int[] FIXED_WIDTHS_V1 = new int[] { 1, 50, 50, 50, 255, 50, 50, 20, 255, 50, 50, 50, 20, 20, 50, 30, 1, 1, 1, 1, 1, 1, 50 };
	protected static final String[] COLUMN_NAMES_V1 = new String[] { "ACTION", "SERIAL_NUMBER", "DEALER_NAME", "LOCATION_NAME", "ADDRESS", "CITY", "STATE", "ZIP", "LOCATION_DETAILS", "LOCATION_TYPE", "TIME_ZONE", "ASSET_NUMBER", "MACHINE_MAKE", "MACHINE_MODEL", "PRODUCT_TYPE", "BANK_ACCT_ID", /* "VEND_MODE", "AUTH_MODE","RECEIPT", "DEX_CAPTURE",*/"PAY_SCHEDULE", "CURRENCY", "REGION" };
	protected static final String[] RESPONSE_COLUMN_NAMES_V1 = new String[] { "SERIAL_NUM", "ACTION", "RESULT", "ERROR_FIELD", "ERROR_VALUE", "MESSAGE" };

	protected static Translator getTranslator(CallInputs ci) throws MalformedURLException {
		String host = new URL(ci.getRequestURL()).getHost();
		String lang = ci.getHeaders().get("accept-language");
		if(lang != null)
			lang = lang.split("[,]", 2)[0];
		Locale locale = ConvertUtils.convertSafely(Locale.class, lang, null);
		try {
			return TranslatorFactory.getDefaultFactory().getTranslator(locale, host);
		} catch(ServiceException e) {
			log.warn("Could not get translator", e);
			return DefaultTranslatorFactory.getTranslatorInstance();
		}
	}

	public void processRequest(long requestId, long profileId, long userId, CallInputs ci, InputStream content, final ProcessResponse response, long lastProcessedLine, long updatedCount) throws ServiceException {
		ReportingUser user;
		ReportingUser masterUser=null;
		
		try {
			user = ReportUtil.loadReportingUser(profileId);
		} catch(SQLException | DataLayerException | ConvertException e) {
			throw new ServiceException(e);
		}
		if(userId != profileId){
			user.setMasterUserId(userId);
			try{
				masterUser= ReportUtil.loadReportingUser(userId);
			}
			catch(SQLException | DataLayerException | ConvertException e) {
				throw new ServiceException(e);
			}
		}

		ci.setUser(user);
		final Translator translator;
		try {
			translator = getTranslator(ci);
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		Messages messageHandler = new Messages() {
			protected final StringBuilder responseText = new StringBuilder();

			protected String translateMessage(String messageKey, String defaultText, Object... parameters) {
				String message;
				if(translator != null)
					message = translator.translate(messageKey, defaultText, parameters);
				else {
					if(defaultText != null)
						message = defaultText;
					else
						message = messageKey;
					if(parameters != null && message != null && message.indexOf('{') >= 0) {
						try {
							message = new MessageFormat(message).format(parameters);
						} catch(RuntimeException e) {
							log.warn("Could not create Message Format from '" + message + "'", e);
						}
					}
				}
				return message;
			}

			protected void addDetail(String messageType, String messageKey, Object... parameters) throws ServiceException {
				responseText.setLength(0);
				responseText.append("<li class=\"message-").append(messageType).append("\">");
				try {
					StringUtils.appendHTML(responseText, translateMessage(messageKey, null, parameters));
				} catch(IOException e) {
					throw new ServiceException(e);
				}
				responseText.append("</li>");
				response.appendResponseText(responseText.toString());
			}

			public void failed(RowInfo rowInfo, String field, Object value, String messageKey, Object... parameters) throws ServiceException {
				addDetail("error", messageKey, parameters);
			}

			public void skipped(RowInfo rowInfo, String messageKey, Object... parameters) {
				// do nothing
			}

			public void processed(RowInfo rowInfo, String messageKey, Object... parameters) throws ServiceException {
				addDetail("success", messageKey, parameters);
			}

			public void header(String messageKey, Object... parameters) throws ServiceException {
				responseText.setLength(0);
				responseText.append("Mass Device Update: ").append(translateMessage(messageKey, null, parameters));
				response.setEmailSubject(responseText.toString());
			}

			public void footer(String messageKey, Object... parameters) throws ServiceException {
				String message = translateMessage(messageKey, null, parameters);
				response.setResponseSummary(message);
			}

			public void skippedSummary(String messageKey, Object... parameters) throws ServiceException {
				addDetail("skipped", messageKey, parameters);
			}
		};
		response.setEmailFromName("USALive");
		response.setEmailFromAddress("usalive@usatech.com");
		try {
			FileType fileType = ConvertUtils.convertDefault(FileType.class, ci.getRealParameters().get("fileType"), FileType.EXCEL);
			String path = ConvertUtils.getStringSafely(ci.getRealParameters().get("updateFile"));
			RowValuesIterator rvIter = getRowValuesIterator(content, fileType);
			Long customerBankId = ConvertUtils.convert(Long.class, ci.getRealParameters().get("custBankId"));
			if(customerBankId != null)
				checkBankAcct(customerBankId, user.getUserId());
			
			Long dealerId = ConvertUtils.convert(Long.class, ci.getRealParameters().get("dealerId"));
			boolean override = ConvertUtils.getBoolean(ci.getRealParameters().get("override"), false);
			processRows(requestId, ci, rvIter, path, messageHandler, user, dealerId, customerBankId, override, masterUser,lastProcessedLine, updatedCount);
			StringBuilder sb = new StringBuilder();
			sb.append("<html lang=\"en\" xml:lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html charset=UTF-8\" /><title>USALive - Mass Device Update</title><base href=\"");
			sb.append(ci.getRequestURL());
			sb.append("\" /><style type=\"text/css\">body {  width: 100% !important;  font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;  font-size: 16px;  -webkit-text-size-adjust: none;  -ms-text-size-adjust: none;  margin: 0;  padding: 0; } img {  height: auto;  line-height: 100%;  outline: none;  text-decoration: none;  display: block;  }h1, h2, h3, h4, h5, h6 {  color: #333333;  line-height: 100% !important;  margin:0;  }  p {  margin: 1em 0;  }a {  color:#0088cc;  text-decoration: none;  }  a:hover,a:focus {  color: #005580;  text-decoration: underline !important;}table td {  border-collapse: collapse;  } </style><meta name=\"robots\" content=\"noindex, nofollow\"/></head><body bgcolor=\"#ffffff\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");
			sb.append("<h4>");
			response.setEmailHeader(sb.toString());
			response.setEmailMiddle("</h4><ul>");
			response.setEmailSummaryFirst(true);
			response.setEmailFooter("</ul></body></html>");
		} catch(IOException | ConvertException | ServletException e) {
			throw new ServiceException(e);
		} 
	}

	public static void checkBankAcct(long customerBankId, long userId) throws ServletException {
		log.debug("Checking if userId '" + userId + "' can assign locations to bank acct " + customerBankId);
		/* BSK - 04/09/2007 - This check is no longer necessary because it prevented a user who could edit a terminal from editting it if that user could not see the bank account
		*/
		/* WCL - 03/26/2008 - Adding this back into the checking process because the bank account ID for the
		 * terminal is now taken from the database, so it would be used if the bank account ID is not present
		 * in the file. */
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("custBankId", customerBankId);
		params.put("userId", userId);
		
		try {
			DataLayerMgr.selectInto("GET_CUST_BANK_STATUS", params, false);
		} catch(NotEnoughRowsException e) {
			throw new ServletException("Invalid Bank Account");
		} catch(SQLException e) {
			throw new ServletException("Could not retrieve Bank Account", e);
		} catch(DataLayerException e) {
			throw new ServletException("Could not retrieve Bank Account", e);
		} catch(BeanException e) {
			throw new ServletException("Could not retrieve Bank Account", e);
		}
		
		char status = ConvertUtils.getCharSafely(params.get("custBankStatus"), '-');
		char allowEdit = ConvertUtils.getCharSafely(params.get("allowEdit"), '-');
		if(allowEdit != 'N' && allowEdit != 'Y') {
			throw new NotAuthorizedException("User is not authorized to assign a location to this bank account");
		} else if(status != 'A') {
			throw new ServletException("Bank Account is not active");
		}
	}

	public RowValuesIterator getRowValuesIterator(InputStream inputStream, FileType fileType) throws IOException {
		switch(fileType) {
			case EXCEL:
				return SheetUtils.getExcelRowValuesIterator(inputStream);
			case FIXED_V1:
				return SheetUtils.getFixedWidthRowValuesIterator(inputStream, FIXED_WIDTHS_V1, COLUMN_NAMES_V1);
			default:
				throw new IOException("File Type " + fileType + " is not supported");
		}
	}

	protected static String getDeviceType(Integer deviceTypeId) {
		String dType = null;
		switch(deviceTypeId.intValue()) {
			case 4:
				dType = "Sony device";
				break;
			case 5:
				dType = "eSuds room controller";
				break;
			case 0: // G4 ePort
			case 1: // G5 ePort
			case 3: // ePort NG
			case 6: // MEI ePort
			case 7: // EZ80 ePort Development
			case 8: // EZ80 ePort
			case 9: // Legacy G4
				dType = "ePort";
				break;
			case 11: // kiosk
			case 10: // transact
			default:
				dType = "device";
		}
		return dType;
	}

	public void processRows(long requestId, CallInputs ci, RowValuesIterator rvIter, String filePath, Messages messages, ReportingUser user, Long defaultDealerId, Long defaultCustomerBankId, boolean override, ReportingUser masterUser,long lastProcessedLine, long updatedCount) throws ServiceException {
		long processed = updatedCount;
		int ignored = 0;
		int errors = 0;
		int totalIgnored = 0;
		//get all the values for this row
		messages.header("device.update.header", filePath);

		Map<String, String> attributes = new HashMap<String, String>();
		attributes.put("masterUserId", String.valueOf(user.getMasterUserId()));
		attributes.put("userId", String.valueOf(user.getUserId()));
		ci.setAppCd("USALive");
		ci.setActionName("Mass Device Update");
		ci.setObjectTypeCd("terminal");
		long currentLine = 0;
		HashMap<String, Object> requestParams = new HashMap<String, Object>();
		requestParams.put("processRequestId", requestId);
		requestParams.put("profileId", user.getUserId());
		
		boolean isDataSyncUser = user.getUserName().trim().equalsIgnoreCase("USATDataSync");
		
		while(rvIter.hasNext()) {
			currentLine++;
			if (currentLine <= lastProcessedLine){
				rvIter.next(true);
				continue;
			}

			RowInfo rowInfo = new RowInfo();
			String locationName = null;
			Long customerBankId = null;
			Long dealerId = null;
			String subkey = "update";
			Map<String, Object> columnValues = null;
			boolean isActivateAllowed=true;

			try {
				columnValues = rvIter.next(true);
				rowInfo.rowNum = rvIter.getCurrentRowNum();

				if(columnValues.isEmpty()||columnValues.get("ACTION")==null) {
					log.debug("Ignoring empty row at row #" + rowInfo.rowNum);
				} else {
					long startTsMs = System.currentTimeMillis();
					long terminalId = 0;
					
					log.debug("Processing row #" + rowInfo.rowNum + " with values=" + columnValues);
					//validations
					Object actionValue=checkMappedValue(actionMap, columnValues, true, false, 'I', null, "ACTION");
					if(actionValue==null){
						rowInfo.actionCode = 'I';
					}else{
						rowInfo.actionCode = ConvertUtils.convert(char.class, actionValue);
					}
					
					if(rowInfo.actionCode == 'I') {
						messages.skipped(rowInfo, "device.update.skipped", rowInfo.rowNum, processed);
						ignored++;
					} else {
						switch(rowInfo.actionCode) {
							case 'D':
								subkey = "deactivate";
								break;
							case 'A':
								subkey = "activate";
								break;
							default:
								subkey = "change";
						}
						
						if(ignored > 0) {
							messages.skippedSummary("device.update.skippedsummary", ignored, rowInfo.rowNum, processed);
							totalIgnored += ignored;
							ignored = 0;
						}
						
						rowInfo.serialNum = checkStringValue(columnValues, true, true, null, 50, null, "SERIAL_NUMBER", "EPORT_SERIAL_NUMBER").trim().toUpperCase();
						if(rowInfo.actionCode == 'D') {
							if(!user.hasPrivilege("16"))
								throw new NotAuthorizedException("User is not permitted to deactivate devices");
							
							DataLayerMgr.executeCall("GET_TERMINAL_ID", columnValues, false);
							terminalId = ConvertUtils.getLong(columnValues.get("TERMINAL_ID"));
							user.checkPermission(ReportingUser.ResourceType.TERMINAL, ReportingUser.ActionType.DELETE, terminalId);
							DataLayerMgr.executeUpdate("DEACTIVATE_DEVICE", columnValues, true);
						} else if(rowInfo.actionCode=='A'&&!(user.isPartner()||user.isCustomerService())){
							isActivateAllowed=false;
						} else {
							Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_DEVICE");

							try {
								TerminalInfo terminal = new TerminalInfo(rowInfo.serialNum);
								boolean columnRequired = (rowInfo.actionCode == 'A');

								if(rowInfo.actionCode == 'A') {
									if(!user.hasPrivilege(String.valueOf(ReportingPrivilege.PRIV_ACTIVATE_TERMINAL.getValue()))) {
										throw new NotAuthorizedException("User is not permitted to activate devices");
									}
									
									terminal.DEALER_NAME = checkStringValue(columnValues, defaultDealerId == null, (defaultDealerId == null && columnRequired), null, 0, terminal.COLUMNS, "DEALER_NAME");

									if(terminal.DEALER_NAME == null) {
										dealerId = defaultDealerId;
										terminal.DEALER_ID = dealerId;
									} else {
										DataLayerMgr.executeCall(conn, "GET_DEALER_ID", terminal);
									}
									DataLayerMgr.executeCall(conn, "GET_POS_EDITABLE_BY_SERIAL_NUM", terminal);
								} else {
									try {
										terminal.loadFromDatabase(conn);
									} catch(NotEnoughRowsException e) {
										throw new SQLException("Invalid serial # " + terminal.SERIAL_NUMBER, null, 20201);
									}
									if(terminal.DEVICE_TYPE_ID == null)
										throw new SQLException("Invalid serial # " + terminal.SERIAL_NUMBER, null, 20201);
									if(terminal.TERMINAL_ID == null)
										throw new SQLException("Customer trying to update " + getDeviceType(terminal.DEVICE_TYPE_ID) + " serial # " + terminal.SERIAL_NUMBER + " which was never activated", null, 20218);
									user.checkPermission(ReportingUser.ResourceType.TERMINAL, ReportingUser.ActionType.EDIT, terminal.TERMINAL_ID);
								}

								// TODO: need to figure a way that we got the value in the map somewhere, but not here- mmmm.
								locationName = checkStringValue(columnValues, true, columnRequired, terminal.LOCATION_NAME, 50, terminal.COLUMNS, "LOCATION_NAME", "LOCATION");
								terminal.LOCATION_NAME = locationName;
								terminal.ASSET_NUMBER = checkStringValue(columnValues, false, false, terminal.ASSET_NUMBER, 50, terminal.COLUMNS, "ASSET_NUMBER", "OUTLET_NUMBER"); // this sets asset_number ifoutlet_number is																																													// given
								terminal.CLIENT = checkStringValue(columnValues, false, false, terminal.CLIENT, 50, terminal.COLUMNS, "CLIENT");
								terminal.ADDRESS = checkStringValue(columnValues, false, false, terminal.ADDRESS, 255, terminal.COLUMNS, "ADDRESS", "ADDRESS_1"); // this sets ADDRESS if ADDRESS_1 is given
								terminal.LOCATION_DETAILS = checkStringValue(columnValues, false, false, terminal.LOCATION_DETAILS, 255, terminal.COLUMNS, "LOCATION_DETAILS");
								terminal.CITY = checkStringValue(columnValues, true, columnRequired, terminal.CITY, 50, terminal.COLUMNS, "CITY");
								terminal.STATE = checkStringValue(columnValues, true, columnRequired, terminal.STATE, 50, terminal.COLUMNS, "STATE");
								terminal.COUNTRY = checkStringValue(columnValues, false, false, "US", 2, terminal.COLUMNS, "COUNTRY");
								terminal.ZIP = checkStringValue(columnValues, true, columnRequired, terminal.ZIP, 20, terminal.COLUMNS, "POSTAL", "ZIP");
								terminal.TIME_ZONE = checkMappedValue(timeZoneMap, columnValues, true, columnRequired, terminal.TIME_ZONE, terminal.COLUMNS, "TIME_ZONE");
								terminal.MACHINE_MAKE = checkStringValue(columnValues, true, columnRequired, terminal.MACHINE_MAKE, 20, terminal.COLUMNS, "MACHINE_MAKE");
								terminal.MACHINE_MODEL = checkStringValue(columnValues, true, columnRequired, terminal.MACHINE_MODEL, 20, terminal.COLUMNS, "MACHINE_MODEL");
								terminal.TELEPHONE = checkNumberStringValue(columnValues, false, false, terminal.TELEPHONE, 20, ALLOWABLE, terminal.COLUMNS, "TELEPHONE");

								String s = checkStringValue(columnValues, true, columnRequired, getTypeValue(terminal.PRODUCT_TYPE, terminal.PRODUCT_TYPE_SPECIFY), 0, terminal.COLUMNS, "PRODUCT_TYPE");
								int pos = 0;
								if(s != null) {
									pos = s.indexOf(':');
									if(pos >= 0) {
										terminal.PRODUCT_TYPE = s.substring(0, pos).trim();
										terminal.PRODUCT_TYPE_SPECIFY = s.substring(pos + 1).trim();
									} else {
										terminal.PRODUCT_TYPE = s;
										terminal.PRODUCT_TYPE_SPECIFY = null;
									}
								}
								
								s = checkStringValue(columnValues, false, false, getTypeValue(terminal.LOCATION_TYPE, terminal.LOCATION_TYPE_SPECIFY), 0, terminal.COLUMNS, "LOCATION_TYPE", "CHANNEL");
								
								if(s != null) {
									pos = s.indexOf(':');
									if(pos >= 0) {
										terminal.LOCATION_TYPE = s.substring(0, pos).trim();
										terminal.LOCATION_TYPE_SPECIFY = s.substring(pos + 1).trim();
									} else {
										terminal.LOCATION_TYPE = s;
										terminal.LOCATION_TYPE_SPECIFY = null;
									}
								}
								// terminal.VEND_MODE = checkMappedValue(vendModeMap, columnValues, true, columnRequired, terminal.VEND_MODE, "VEND_MODE", "VENDS_PER_TRAN", "VENDS/SWIPE");
								// terminal.AUTH_MODE = checkMappedValue(authModeMap, columnValues, true, columnRequired, terminal.AUTH_MODE, "AUTH_MODE");

								// terminal.RECEIPT = checkMappedValue(yesNoMap, columnValues, true, false, (rowInfo.actionCode == 'A' ? "N" : terminal.RECEIPT), "RECEIPT");
								// terminal.DEX_CAPTURE = checkMappedValue(dexModeMap, columnValues, true, false, (rowInfo.actionCode == 'A' ? "N" : terminal.DEX_CAPTURE), "DEX_CAPTURE");

								terminal.AVG_ITEM_PRICE = checkDecimalValue(columnValues, false, false, terminal.AVG_ITEM_PRICE, terminal.COLUMNS, "AVG_ITEM_PRICE", "HIGHEST VEND PRICE");
								customerBankId = checkLongValue(columnValues, false, false, null, terminal.COLUMNS, "BANK_ACCT_ID");
								if(customerBankId == null) {
									if(override || rowInfo.actionCode == 'A') {
										customerBankId = defaultCustomerBankId;
										terminal.COLUMNS.add("BANK_ACCT_ID");
									}
								} else if(!ConvertUtils.areEqual(customerBankId, defaultCustomerBankId)) {
									checkBankAcct(customerBankId, user.getUserId());
								}
								terminal.CUSTOMER_BANK_ID = customerBankId;

								terminal.PAY_SCHEDULE = checkMappedValue(paySchedMap, columnValues, false, false, null, terminal.COLUMNS, "PAY_SCHEDULE", "PAY_SCHED");
								terminal.CURRENCY_ID = checkMappedValue(currencyMap, columnValues, false, false, null, terminal.COLUMNS, "CURRENCY");

								terminal.USER_ID = user.getUserId();
								terminal.MASTER_USER_ID = user.getMasterUserId();
								terminal.REGION = checkStringValue(columnValues, false, false, terminal.REGION, 50, terminal.COLUMNS, "REGION");
								if(user.isInternal()||(masterUser!=null&&masterUser.isInternal())){
									terminal.BUSINESS_TYPE = checkStringValue(columnValues, true, columnRequired, terminal.BUSINESS_TYPE, 0, terminal.COLUMNS, "BUSINESS_TYPE");
									terminal.DOING_BUSINESS_AS = checkStringValue(columnValues, false, false, terminal.DOING_BUSINESS_AS, 21, terminal.COLUMNS, "DOING_BUSINESS_AS");
								}
								terminal.CUSTOMER_SERVICE_PHONE = checkNumberStringValue(columnValues, false, false, terminal.CUSTOMER_SERVICE_PHONE, 20, ALLOWABLE, terminal.COLUMNS, "CUSTOMER_SERVICE_PHONE");
								terminal.CUSTOMER_SERVICE_EMAIL = checkStringValue(columnValues, false, false, terminal.CUSTOMER_SERVICE_EMAIL, 70, terminal.COLUMNS, "CUSTOMER_SERVICE_EMAIL");

								if(terminal.SERIAL_NUMBER.startsWith("K3") && terminal.posTypeEditable) {
									String mobile = checkMappedValue(yesNoMap, columnValues, true, columnRequired, null, terminal.COLUMNS, "MOBILE");
									String signature = checkMappedValue(signatureCaptureMap, columnValues, true, false, null, terminal.COLUMNS, "SIGNATURE_CAPTURE");
									if(!StringUtils.isBlank(mobile) || !StringUtils.isBlank(signature)) {
										char mobileIndicator = mobile.charAt(0);
										char cardEntryKeyed = (terminal.ENTRY_CAPABILITY != null && terminal.ENTRY_CAPABILITY.indexOf('M') >= 0 ? 'Y': 'N'); //checkMappedValue(yesNoMap, columnValues, true, false, "N", "CARD_ENTRY_KEYED").charAt(0);
										char cardEntrySwipe = (terminal.ENTRY_CAPABILITY != null && terminal.ENTRY_CAPABILITY.indexOf('S') >= 0 ? 'Y': 'N'); //checkMappedValue(yesNoMap, columnValues, true, false, "N", "CARD_ENTRY_SWIPE").charAt(0);
										char cardEntryProximity = (terminal.ENTRY_CAPABILITY != null && terminal.ENTRY_CAPABILITY.indexOf('P') >= 0 ? 'Y': 'N'); //checkMappedValue(yesNoMap, columnValues, true, false, "N", "CARD_ENTRY_PROXIMITY").charAt(0);
										char cardEntryEMV = (terminal.ENTRY_CAPABILITY != null && terminal.ENTRY_CAPABILITY.indexOf('E') >= 0 ? 'Y': 'N'); //checkMappedValue(yesNoMap, columnValues, true, false, "N", "CARD_ENTRY_EMV").charAt(0);
										char signatureCaptured = signature.charAt(0);
										Set<Character> entryList = new HashSet<Character>();
										terminal.POS_ENVIRONMENT = '?';
										switch(mobileIndicator) {
											case 'Y':
												entryList.add('M');
												terminal.POS_ENVIRONMENT = 'C'; // Mobile
												break;
											case 'N':
												char attendedIndicator = checkMappedValue(yesNoMap, columnValues, true, true, null, terminal.COLUMNS, "ATTENDED").charAt(0);
												switch(attendedIndicator) {
													case 'N':
														if(!entryList.contains('M') || entryList.contains('S') || entryList.contains('P') || entryList.contains('E'))
															terminal.POS_ENVIRONMENT = 'U'; // Unattended
														else terminal.POS_ENVIRONMENT = 'E'; // Ecommerce
														break;
													case 'Y':
														if(entryList.contains('S'))
															terminal.POS_ENVIRONMENT = 'A'; // Attended
														else terminal.POS_ENVIRONMENT = 'M'; // MOTO
														break;
													default:
														throw new ValueInvalidException("ATTENDED", attendedIndicator);
												}
												break;
											default:
												throw new ValueInvalidException("MOBILE", mobileIndicator);
										}
										if(cardEntryKeyed == 'Y')
											entryList.add('M');
										if(cardEntrySwipe == 'Y')
											entryList.add('S');
										if(cardEntryProximity == 'Y')
											entryList.add('P');
										if(cardEntryEMV == 'Y')
											entryList.add('E');
										if(signatureCaptured == 'E')
											entryList.add('X');
	
										terminal.PIN_CAPABILITY = (entryList.contains('M') ? 'Y' : 'N');
										if(entryList.isEmpty())
											terminal.ENTRY_CAPABILITY = "-";
										else {
											char[] entryCapability = new char[entryList.size()];
											int i = 0;
											for(Character ec : entryList)
												entryCapability[i++] = ec;
											terminal.ENTRY_CAPABILITY = new String(entryCapability);
										}
									}
								}
								
					
								for(int i = 1; i <= 8; i++) {
									String val = checkStringValue(columnValues, false, false, terminal.CUSTOM.get(String.valueOf(i)), 4000, terminal.COLUMNS, "CUSTOM_" + i);
									terminal.CUSTOM.put(String.valueOf(i), val);
								}

								if(rowInfo.actionCode == 'A') {
									terminal.activatedBy = user.getFirstName() + ' ' + user.getLastName();
									// TODO: handle commissionBank for commissioned licenses?
									DataLayerMgr.executeUpdate(conn, "ACTIVATE_DEVICE", terminal);
									user.incrementTerminalCount();
								} else {
									if(customerBankId != null) {
										Object[] ret = DataLayerMgr.executeCall(conn, "CHECK_TERMINAL_CUST_BANK", new Object[] {customerBankId,terminal.TERMINAL_ID});
										boolean ok = (ret != null && ret.length >= 2 && ret[1] != null && "Y".equals(ret[1]));
										if(!ok)
											throw new BankAcctValueInvalidException("BANK_ACCT_ID", customerBankId);
									}
									DataLayerMgr.executeUpdate(conn, "UPDATE_DEVICE", terminal);
								}
								
								if(terminal.CURRENCY_ID != null) {
									if(rowInfo.actionCode != 'A') {
										DataLayerMgr.executeUpdate(conn, "GET_TERMINAL_CURRENCY", terminal);
										if(terminal.CURRENCY_ID != terminal.CURRENT_CURRENCY_ID)
											throw new ValueInvalidException("CURRENCY", terminal.CURRENCY_ID);
									}
									DataLayerMgr.executeUpdate(conn, "UPDATE_TERMINAL_CURRENCY", terminal);
								}
								
								//USAT-488
								//check for existing values if nothing changed then skip the update
								String columnMapName = checkStringValue(columnValues, false, false, null, 100, terminal.COLUMNS, "COLUMN_MAP_NAME");								
								String columnMapValues = checkStringValue(columnValues, false, false, null, 100000, terminal.COLUMNS, "COLUMN_MAP");
								
								//validate columnMapName not more than 100 chars
								if(columnMapName != null && columnMapName.length()>100)
									throw new ValueInvalidException("COLUMN_MAP_NAME", columnMapName);
								
						//TODO for R57: make changes to avoid USATDataSync user overwriting customer created maps
								//Check to avoid USATDataSync user overwriting customer created maps
								if(isDataSyncUser && terminal.AUTO_UPDATE_COLUMN_MAP.equals("N")){
									
									//log warn can't overwrite customer map
									log.info("MassDeviceUpdate datasync cannot modify customer disabled column map for terminal " + terminal.TERMINAL_ID);
									
								} else {
									//different column map name supplied in update
									if(columnMapName != null && !columnMapName.equalsIgnoreCase(terminal.COLUMN_MAP_NAME)){
										
										if(terminal.CUSTOMER_ID==null)
											DataLayerMgr.selectInto(conn,"GET_CUSTOMER_FOR_TERMINAL", terminal);
																			
										//check for existing map by name
										HashMap<String, Object> columnMapParams = new HashMap<String, Object>();
										columnMapParams.put("columnMapName", columnMapName);
										columnMapParams.put("customerId", terminal.CUSTOMER_ID);
										
										Long columnMapId = null;
										Long customerId = null;
										String existingColumnMapValues = null;
										Results results = DataLayerMgr.executeQuery(conn, "GET_COLUMN_MAP_BY_NAME", columnMapParams);
										if(results.next()){
											columnMapId = results.getValue("columnMapId", Long.class);
											customerId = results.getValue("customerId", Long.class);
											existingColumnMapValues= results.getValue("columnMapValues", String.class);
										}
										
										if(columnMapId==null){
											//to create a new map they need to supply values
											//if they didn't then the map name is probably a typo
											if(columnMapValues==null)
												throw new ValueInvalidException("COLUMN_MAP_NAME", columnMapName);
											//create the new map and values combinations
											validateAndSaveColumnMap(conn, columnMapName, columnMapValues, terminal, null, 'N');
										} else if((columnMapValues==null) || (columnMapValues!=null && columnMapValues.equalsIgnoreCase(existingColumnMapValues))) {
											//attach it to the existing column map
											columnMapParams.put("columnMapId", columnMapId);
											columnMapParams.put("terminalId", terminal.TERMINAL_ID);
											DataLayerMgr.executeUpdate(conn, "UPDATE_TERMINAL_COLUMN_MAP", columnMapParams);
										} else {
											//existing name with new values
											if(customerId!=null){
												//belongs to customer so update values and link map
												validateAndSaveColumnMap(conn, columnMapName, columnMapValues, terminal, columnMapId, 'N');
											} else {
												//The existing map is GLOBAL and cannot be modified by a customer
												//so create a new map and values combinations at the customer level 
												validateAndSaveColumnMap(conn, columnMapName, columnMapValues, terminal, null, 'N');
											}
										}
										//different mdb=column values supplied in update 
									} else if (columnMapValues != null && !columnMapValues.equalsIgnoreCase(terminal.COLUMN_MAP)){
										
										if(terminal.CUSTOMER_ID==null)
											DataLayerMgr.selectInto(conn,"GET_CUSTOMER_FOR_TERMINAL", terminal);
																			
										HashMap<String, Object> columnMapParams = new HashMap<String, Object>();
	
										if(columnMapName != null){
											//At this point the column map name in the row must match the existing column map name
											//so this is an update to the existing values 
											columnMapParams.put("columnMapName", columnMapName);
											columnMapParams.put("customerId", terminal.CUSTOMER_ID);
											
											Long columnMapId = null;
											Long customerId = null;
											Results results = DataLayerMgr.executeQuery(conn, "GET_COLUMN_MAP_BY_NAME", columnMapParams);
											if(results.next()){
												columnMapId = results.getValue("columnMapId", Long.class);
												customerId = results.getValue("customerId", Long.class);
											}
											//check for global map cannot update values
											if(customerId != null){
												//belongs to customer so update values and link map
												validateAndSaveColumnMap(conn, columnMapName, columnMapValues, terminal, columnMapId, 'N');
											} else {
												//The existing map is GLOBAL and cannot be modified by a customer
												//or the map is not status A and did not return a match
												//so create a new map and values combinations at the customer level 
												validateAndSaveColumnMap(conn, columnMapName, columnMapValues, terminal, null, 'N');
											}
										} else if(terminal.COLUMN_MAP_NAME != null && columnMapValues.equals(terminal.COLUMN_MAP)){
											//If they have a column map already but the name in the row was null
											//if column map has the same values as assign to terminal then do nothing. 
										} else {
											//no name so check for existing map by value string
											columnMapParams.put("columnMapValues", columnMapValues);
											columnMapParams.put("customerId", terminal.CUSTOMER_ID);
											
											Long columnMapId = null;
											Results results = DataLayerMgr.executeQuery(conn, "GET_COLUMN_MAP_BY_VALUES", columnMapParams);
											if(results.next()){
												columnMapId = results.getValue("columnMapId", Long.class);
											}
		
											if(columnMapId==null){
												//no existing map
												StringBuffer newMapName = new StringBuffer();
												//name prefix
												if(terminal.MACHINE_MAKE != null && terminal.MACHINE_MODEL!=null){
													newMapName.append(terminal.MACHINE_MAKE).append(" ").append(terminal.MACHINE_MODEL).append(" CM-");
												} else {
													//TODO need better default value 
													newMapName.append("USAT SYSGEN CM-");
												}
												//create the new map and values combinations
												validateAndSaveColumnMap(conn, newMapName.toString(), columnMapValues, terminal, null, 'Y');
											} else {
												//attach it to the existing column map
												columnMapParams.put("terminalId", terminal.TERMINAL_ID);
												columnMapParams.put("columnMapId", columnMapId);
												DataLayerMgr.executeUpdate(conn, "UPDATE_TERMINAL_COLUMN_MAP", columnMapParams);
											}
										}
									}
								}
								if(terminalId < 1)
									terminalId = terminal.TERMINAL_ID;
								requestParams.put("lastProcessedLine", currentLine);
								requestParams.put("updatedCount", isActivateAllowed?processed+1:processed);
								DataLayerMgr.executeUpdate(conn, "UPDATE_PROCESS_REQUEST_LINE", requestParams);
								conn.commit();
							} catch(Exception e) {
								ProcessingUtils.rollbackDbConnection(log, conn);
								throw e;
							} finally {
								ProcessingUtils.closeDbConnection(log, conn);
							}
						}
						if(isActivateAllowed){
							processed++;
							messages.processed(rowInfo, "device." + subkey + ".success", rowInfo.serialNum, locationName, rowInfo.rowNum, processed);
							ci.setDuration(System.currentTimeMillis() - startTsMs);
							ci.setObjectCd(String.valueOf(terminalId));
							ci.setExplicitParameters(columnValues);
							WebHelper.publishAppRequestRecord(ci, attributes, log);
						}else{
							errors++;
							messages.failed(rowInfo, "ACTION", "A", "device." + subkey + ".notallowed", rowInfo.serialNum, rowInfo.rowNum);
						}
					}
				}
			} catch(Exception e) {
				errors++;
				
				if(ignored > 0) {
					messages.skippedSummary("device.update.skippedsummary", ignored, rowInfo.rowNum, processed);
					totalIgnored += ignored;
					ignored = 0;
				}
				
				if(e instanceof SQLException) {
					log.warn("Exception while attemping to " + subkey + " terminal", e);
					switch(((SQLException)e).getErrorCode()) {
						case 20201:
							messages.failed(rowInfo, "SERIAL_NUMBER", rowInfo.serialNum, "device." + subkey + ".error.invalid.serial", rowInfo.serialNum, rowInfo.rowNum, processed);
							break;
						case 20202:
							if(dealerId == null)
								messages.failed(rowInfo, "DEALER_NAME", columnValues.get("DEALER_NAME"), "device." + subkey + ".error.invalid.dealer", dealerId, rowInfo.rowNum, processed);
							else
								messages.failed(rowInfo, "DEALER_ID", dealerId, "device." + subkey + ".error.invalid.dealer", dealerId, rowInfo.rowNum, processed);
							break;
						case 20203:
							messages.failed(rowInfo, "BANK_ACCT_ID", customerBankId, "device." + subkey + ".error.invalid.bankacct", customerBankId, rowInfo.rowNum, processed);
							break;
						case 20204:
							messages.failed(rowInfo, null, null, "device." + subkey + ".error.nolicense", rowInfo.rowNum, processed);
							break;
						case 20205:
							messages.failed(rowInfo, "SERIAL_NUMBER", rowInfo.serialNum, "device." + subkey + ".error.alreadyactive", rowInfo.serialNum, rowInfo.rowNum, processed);
							break;
						case 20206:
							log.error("User, " + user.getUserName() + ", is trying to activate an e-Port that has already been activated by another user.");
							messages.failed(rowInfo, "SERIAL_NUMBER", rowInfo.serialNum, "device." + subkey + ".error.inusebyother", rowInfo.serialNum, rowInfo.rowNum, processed);
							break;
						case 20211:
							messages.failed(rowInfo, "LOCATION_TYPE", columnValues.get("LOCATION_TYPE"), "device." + subkey + ".error.invalid.locationtype", rowInfo.serialNum, columnValues.get("LOCATION_TYPE"), rowInfo.rowNum, processed);
							break;
						case 20212:
							messages.failed(rowInfo, "PRODUCT_TYPE", columnValues.get("PRODUCT_TYPE"), "device." + subkey + ".error.invalid.producttype", rowInfo.serialNum, columnValues.get("PRODUCT_TYPE"), rowInfo.rowNum, processed);
							break;
						case 20213:
							messages.failed(rowInfo, "BUSINESS_TYPE", columnValues.get("BUSINESS_TYPE"), "device." + subkey + ".error.invalid.businesstype", rowInfo.serialNum, columnValues.get("BUSINESS_TYPE"), rowInfo.rowNum, processed);
							break;
						case 20215:
							messages.failed(rowInfo, "SERIAL_NUMBER", rowInfo.serialNum, "device." + subkey + ".error.multilocation", rowInfo.serialNum, rowInfo.rowNum, processed);
							break;
						case 20216:
							messages.failed(rowInfo, "DEALER_NAME", columnValues.get("DEALER_NAME"), "device." + subkey + ".error.invalid.dealername", rowInfo.serialNum, columnValues.get("DEALER_NAME"), rowInfo.rowNum, processed);
							break;
						case 20217:
							messages.failed(rowInfo, "PAY_SCHEDULE", columnValues.get("PAY_SCHEDULE"), "device." + subkey + ".error.invalid.paysched", rowInfo.serialNum, columnValues.get("PAY_SCHEDULE"), rowInfo.rowNum, processed);
							break;
						case 20218:
							messages.failed(rowInfo, "SERIAL_NUMBER", rowInfo.serialNum, "device." + subkey + ".error.notactivated", rowInfo.serialNum, rowInfo.rowNum, processed);
							break;
						case 20220:
							messages.failed(rowInfo, "STATE", rowInfo.serialNum, "device." + subkey + ".error.invalid.state", rowInfo.serialNum, columnValues.get("STATE"), columnValues.get("COUNTRY"), rowInfo.rowNum, processed);
							break;
						default:
							messages.failed(rowInfo, null, null, "device." + subkey + ".error.db.general", rowInfo.serialNum, rowInfo.rowNum, processed);
					}
				} else if(e instanceof ValueMissingException) {
					String field = ((ValueMissingException)e).getName();
					messages.failed(rowInfo, field, null, "device." + subkey + ".error.missing", field, rowInfo.rowNum, processed);
				} else if(e instanceof StringConversionException) {
					String field = ((ValueInvalidException)e).getName();
					Object value = ((ValueInvalidException)e).getValue();
					messages.failed(rowInfo, field, value, "device." + subkey + ".error.conversion.string", field, value, rowInfo.rowNum, processed);
				} else if(e instanceof NumberConversionException) {
					String field = ((ValueInvalidException)e).getName();
					Object value = ((ValueInvalidException)e).getValue();
					messages.failed(rowInfo, field, value, "device." + subkey + ".error.conversion.number", field, value, rowInfo.rowNum, processed);
				} else if(e instanceof StringTooLongException) {
					StringTooLongException stle = (StringTooLongException)e;
					String field = stle.getName();
					Object value = stle.getValue();
					messages.failed(rowInfo, field, value, "device." + subkey + ".error.invalid.toolong", field, value, stle.getMaxlength(), rowInfo.rowNum, processed);
				} else if(e instanceof BankAcctValueInvalidException) {
					String field = ((ValueInvalidException)e).getName();
					Object value = ((ValueInvalidException)e).getValue();
					messages.failed(rowInfo, field, value, "device." + subkey + ".error.incorrect.bankacct", field, value, rowInfo.rowNum, processed);
				} else if(e instanceof SpecialValueInvalidException) {
					String field = ((ValueInvalidException)e).getName();
					Object value = ((ValueInvalidException)e).getValue();
					messages.failed(rowInfo, field, value, "device." + subkey + ".error.invalid.special", e.getMessage(), field, value, rowInfo.rowNum, processed);
				} else if(e instanceof ValueInvalidException) {
					String field = ((ValueInvalidException)e).getName();
					Object value = ((ValueInvalidException)e).getValue();
					String instructs = instructMap.get(field);
					if(instructs == null)
						instructs = "Please correct.";
					messages.failed(rowInfo, field, value, "device." + subkey + ".error.invalid.value", instructs, field, value, rowInfo.rowNum, processed);
				} else if(e instanceof NotAuthorizedException) {
					messages.failed(rowInfo, "user", user.getUserName(), "device." + subkey + ".error.notauthorized", e.getMessage(), rowInfo.rowNum, processed);
				} else {
					log.warn("While processing row #" + rowInfo.rowNum, e);
					throw new ServiceException(e);
				}
			}
		}
		if(ignored > 0) {
			messages.skippedSummary("device.update.skippedsummary", ignored, rvIter.getCurrentRowNum(), processed);
			totalIgnored += ignored;
			ignored = 0;
		}
		
		messages.footer("device.update.footer", filePath, rvIter.getCurrentRowNum(), processed, totalIgnored, errors);
	}

	protected static String checkStringValue(Map<String, Object> columnValues, boolean valueRequired, boolean columnRequired, String defaultValue, int maxlength, Set<String> columns, String... names) throws ValueInvalidException, ValueMissingException {
		for(String nm: names) {
			if(columnValues.containsKey(nm)) {
				if(columns != null)
					columns.add(names[0]);
				Object tmp = columnValues.get(nm);
				if(tmp != null) {
					String s;
					try {
						s = ConvertUtils.convert(String.class, tmp);
					} catch(ConvertException e) {
						throw new StringConversionException(nm, tmp);
					}
					
					if(s != null && (s = s.trim()).length() > 0) {
						columnValues.put(names[0], s);
						if(maxlength > 0 && s.length() > maxlength)
							throw new StringTooLongException(names[0], s, maxlength);
						return s;
					}
				} else if(valueRequired)
					checkRequired(null, names[0]);
				
				return null;
			}
		}
		
		if(columnRequired)
			checkRequired(null, names[0]);
		
		if(valueRequired)
			checkRequired(defaultValue, names[0]);
		return defaultValue;
	}

	protected static final StringUtils.SortedChars DIGITS = new StringUtils.SortedChars("0123456789".toCharArray());
	protected static final StringUtils.SortedChars ALLOWABLE = new StringUtils.SortedChars("- \t\n\r.()".toCharArray());

	protected static String checkNumberStringValue(Map<String, Object> columnValues, boolean valueRequired, boolean columnRequired, String defaultValue, int maxlength, StringUtils.SortedChars allowableExtras, Set<String> columns, String... names) throws ValueInvalidException, ValueMissingException {
		for(String nm: names) {
			if(columnValues.containsKey(nm)) {
				if(columns != null)
					columns.add(names[0]);
				Object tmp = columnValues.get(nm);
				if(tmp != null) {
					String s;
					try {
						s = ConvertUtils.convert(String.class, tmp);
					} catch(ConvertException e) {
						throw new StringConversionException(nm, tmp);
					}
					
					if(s != null && (s = s.trim()).length() > 0) {
						columnValues.put(names[0], s);
						if(maxlength > 0 && s.length() > maxlength)
							throw new StringTooLongException(names[0], s, maxlength);

						try {
							return StringUtils.gather(s, DIGITS, allowableExtras);
						} catch(ParseException e) {
							throw new SpecialValueInvalidException("The value \"" + s + "\" for column " + names[0] + " is invalid. " + e.getMessage(), names[0], s);
						}
					}
				} else if(valueRequired)
					checkRequired(null, names[0]);
				return null;
			}
		}
		
		if(columnRequired)
			checkRequired(defaultValue, names[0]);
		
		return defaultValue;
	}

	protected static Integer checkIntegerValue(Map<String, Object> columnValues, boolean valueRequired, boolean columnRequired, Integer defaultValue, Set<String> columns, String... names) throws ValueInvalidException, ValueMissingException {
		for(String nm: names) {
			if(columnValues.containsKey(nm)) {
				if(columns != null)
					columns.add(names[0]);
				Object tmp = columnValues.get(nm);
				if(tmp != null) {
					Integer i;
					try {
						i = ConvertUtils.convert(Integer.class, tmp);
					} catch(ConvertException e) {
						throw new NumberConversionException(nm, tmp);
					}
					
					if(i != null) {
						columnValues.put(names[0], i);
						return i;
					}
				} else if(valueRequired)
					checkRequired(null, names[0]);
			}
		}
		
		if(columnRequired)
			checkRequired(null, names[0]);
		
		return defaultValue;
	}

	protected static Long checkLongValue(Map<String, Object> columnValues, boolean valueRequired, boolean columnRequired, Long defaultValue, Set<String> columns, String... names) throws ValueInvalidException, ValueMissingException {
		for(String nm : names) {
			if(columnValues.containsKey(nm)) {
				if(columns != null)
					columns.add(names[0]);
				Object tmp = columnValues.get(nm);
				if(tmp != null) {
					Long i;
					try {
						i = ConvertUtils.convert(Long.class, tmp);
					} catch(ConvertException e) {
						throw new NumberConversionException(nm, tmp);
					}

					if(i != null) {
						columnValues.put(names[0], i);
						return i;
					}
				} else if(valueRequired)
					checkRequired(null, names[0]);
			}
		}

		if(columnRequired)
			checkRequired(null, names[0]);

		return defaultValue;
	}

	protected static BigDecimal checkDecimalValue(Map<String, Object> columnValues, boolean valueRequired, boolean columnRequired, BigDecimal defaultValue, Set<String> columns, String... names) throws ValueInvalidException, ValueMissingException {
		for(String nm: names) {
			if(columnValues.containsKey(nm)) {
				if(columns != null)
					columns.add(names[0]);
				Object tmp = columnValues.get(nm);
				if(tmp != null) {
					BigDecimal bd;
					try {
						bd = ConvertUtils.convert(BigDecimal.class, tmp);
					} catch(ConvertException e) {
						throw new NumberConversionException(nm, tmp);
					}
					
					if(bd != null) {
						columnValues.put(names[0], bd);
						return bd;
					}
				} else if(valueRequired)
					checkRequired(null, names[0]);
				return null;
			}
		}
		
		if(columnRequired)
			checkRequired(null, names[0]);
		
		return defaultValue;
	}

	protected static <E> E checkMappedValue(Map<String, E> mappedValues, Map<String, Object> columnValues, boolean valueRequired, boolean columnRequired, E defaultValue, Set<String> columns, String... names) throws ValueInvalidException, ValueMissingException {
		for(String nm: names) {
			if(columnValues.containsKey(nm)) {
				if(columns != null)
					columns.add(names[0]);
				Object tmp = columnValues.get(nm);
				if(tmp != null) {
					String s;
					try {
						s = ConvertUtils.convert(String.class, tmp);
					} catch(ConvertException e) {
						throw new StringConversionException(nm, tmp);
					}
					
					if(s != null && (s = s.trim()).length() > 0) {
						if(mappedValues.containsKey(s.toUpperCase())) {
							E val = mappedValues.get(s.toUpperCase());
							columnValues.put(names[0], val);
							return val;
						}
						throw new ValueInvalidException(nm, s);
					}
				} else if(valueRequired)
					checkRequired(null, names[0]);
				
				return null;
			}
		}
		
		if(columnRequired)
			checkRequired(null, names[0]);
		return defaultValue;
	}

	protected static void checkRequired(Object value, String name) throws ValueMissingException {
		if(value == null)
			throw new ValueMissingException(name);
	}

	protected static String getTypeValue(String typeName, String typeSpecify) {
		if(StringUtils.isBlank(typeSpecify))
			return typeName;
		if(StringUtils.isBlank(typeName))
			return typeSpecify;
		return new StringBuilder().append(typeName).append(':').append(typeSpecify).toString();
	}
	/*
		public static void convertExcelToFixed(File excelFile, File fixedFile) throws IOException {
			convertExcelToFixed(new FileInputStream(excelFile), new FileOutputStream(fixedFile));
		}

		public static void convertExcelToFixed(InputStream excelInputStream, OutputStream fixedOutputStream) throws IOException {
			final RowValuesIterator rvIter = SheetUtils.getExcelRowValuesIterator(excelInputStream);
			final byte[] newline = SystemUtils.getNewLine().getBytes();
			
			while(rvIter.hasNext()) {
				Map<String, Object> row = rvIter.next();
				for(int i = 0; i < COLUMN_NAMES_V1.length; i++) {
					String value = ConvertUtils.getStringSafely(row.get(COLUMN_NAMES_V1[i]));
					String padded = StringUtils.pad(value, SPACE_CHAR, FIXED_WIDTHS_V1[i], Justification.LEFT);
					fixedOutputStream.write(padded.getBytes());
				}
				
				fixedOutputStream.write(newline);
				fixedOutputStream.flush();
			}
		}
	*/
	
	protected static void validateAndSaveColumnMap(Connection conn, String columnMapName, String columnMapValues, TerminalInfo terminal, Long columnMapId, char systemGeneratedName) throws ValueInvalidException, DataLayerException, SQLException {
		ArrayList<String> mdbNumber=new ArrayList<String>();
		ArrayList<String> vendColumn=new ArrayList<String>();
		ArrayList<String> code=new ArrayList<String>();
		String[] keyValueTrio=columnMapValues.split("\\|");
		
		for(String aTrio:keyValueTrio){
			if(aTrio.contains("=")){
				String[] keyValue=aTrio.split("=");
				if(keyValue.length>3){
					throw new ValueInvalidException("COLUMN_MAP", "Invalid value trio: " + aTrio);
				}
				String mdbNumberStr=(keyValue.length>0) ? keyValue[0].trim() : "";
				String vendColumnStr=(keyValue.length>1) ? keyValue[1].trim() : "";
				String codeStr=(keyValue.length==3) ? keyValue[2].trim() : "";
				if(mdbNumberStr.length()>0&&vendColumnStr.length()>0){
					if(mdbNumber.contains(mdbNumberStr)){
						throw new ValueInvalidException("COLUMN_MAP", "MDB Number " + mdbNumberStr + " appears more than once in map");
					}else{
						if(mdbNumberStr.length()>255)
							throw new ValueInvalidException("COLUMN_MAP", mdbNumberStr);
							
						if(vendColumnStr.length()>100)
							throw new ValueInvalidException("COLUMN_MAP", vendColumnStr);
						
						if(codeStr.length()>50)
							throw new ValueInvalidException("COLUMN_MAP", codeStr);
						
						mdbNumber.add(mdbNumberStr);
						vendColumn.add(vendColumnStr);
						code.add(codeStr);
					}
				}else{
					throw new ValueInvalidException("COLUMN_MAP", "Missing value in trio: " + aTrio);
				}
			}else{
				throw new ValueInvalidException("COLUMN_MAP", "Missing equal sign in trio: " + aTrio);
			}
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("terminalId", terminal.TERMINAL_ID);
		params.put("customerId", terminal.CUSTOMER_ID);
		params.put("mdbNumber", mdbNumber.toArray());
		params.put("vendColumn", vendColumn.toArray());
		params.put("code", code.toArray());
		params.put("columnMapName", columnMapName);
		params.put("columnMapId", columnMapId);
		params.put("updateUserId", terminal.USER_ID);
		params.put("systemGenName", systemGeneratedName);
		DataLayerMgr.executeUpdate(conn, "SAVE_COLUMN_MAP_TERMINAL", params);
	}
	
}
