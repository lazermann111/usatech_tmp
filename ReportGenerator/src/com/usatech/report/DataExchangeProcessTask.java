package com.usatech.report;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.usatech.app.AbstractAttributeDatasetHandler;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call;
import simple.db.CallNotFoundException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;
import simple.io.BufferStream;
import simple.io.HeapBufferStream;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.DatasetHandler;
import simple.results.DatasetHandlerSelectorFacade;
import simple.text.StringUtils;

public class DataExchangeProcessTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final Pattern FILE_NAME_PATTERN = Pattern.compile("(?:.*/)?([^/_]+)_[^/]+\\.csv");
	protected ResourceFolder resourceFolder;
	protected float processingThresholdDays = 0.25f;
	// protected static final DateFormat DATE_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"));
	protected String processEmailTo;
	protected String processEmailFrom;

	protected class ProcessDatasetHandler extends AbstractAttributeDatasetHandler<ServiceException> {
		protected final Call call;
		protected final Connection conn;
		protected final long dataExchangeFileId;
		protected final Writer emailContentWriter;
		protected final String identifierProperty;
		protected final AtomicLong lineNumber;

		public ProcessDatasetHandler(Connection conn, String callId, String identifierProperty, long dataExchangeFileId, String fileName, AtomicLong lineNumber, Writer emailContentWriter) throws CallNotFoundException {
			this.call = DataLayerMgr.getGlobalDataLayer().findCall(callId);
			this.conn = conn;
			this.dataExchangeFileId = dataExchangeFileId;
			this.emailContentWriter = emailContentWriter;
			this.identifierProperty = identifierProperty;
			this.lineNumber = lineNumber;
		}

		public void handleDatasetEnd() throws ServiceException {
		}

		public void handleDatasetStart(String[] columnNames) throws ServiceException {
		}

		@Override
		public void handleRowStart() {
			super.handleRowStart();
			data.put("dataExchangeFileId", dataExchangeFileId);
		}

		public void handleRowEnd() throws ServiceException {
			try {
				call.executeCall(conn, data, null);
				if(!conn.getAutoCommit())
					conn.commit();
				String identifier = ConvertUtils.getString(data.get(identifierProperty), false);
				char processStatusCd = ConvertUtils.getChar(data.get("processStatusCd"));
				List<String> actionsTaken = ConvertUtils.convert(List.class, String.class, data.get("actionsTaken"));
				String errorMessage = ConvertUtils.getString(data.get("errorMessage"), false);
				switch(processStatusCd) {
					case 'D': // complete
						emailContentWriter.append("SUCCESS: ").append(identifier).append(" (#").append(lineNumber.toString()).append("): ");
						boolean first = true;
						for(String action : actionsTaken) {
							if(first)
								first = false;
							else
								emailContentWriter.append(", ");
							emailContentWriter.append(action);
						}
						emailContentWriter.append("\n");
						log.info("Processed - " + actionsTaken.toString() + " for " + identifier + " (#" + lineNumber.toString() + ")");
						break;
					case 'S': // Skipped
						emailContentWriter.append("Skipped: ").append(identifier).append(" (#").append(lineNumber.toString()).append("): ").append(errorMessage).append("\n");
						log.info("Skipped - " + errorMessage + " for " + identifier + " (#" + lineNumber.toString() + ")");
						break;
					case 'E': // Error
						emailContentWriter.append("ERROR: ").append(identifier).append(" (#").append(lineNumber.toString()).append("): ").append(errorMessage).append("\n");
						log.warn("Failed to process - " + errorMessage + " for " + identifier + " (#" + lineNumber.toString() + ")");
						break;
					default:
						emailContentWriter.append("INVALID '").append(processStatusCd).append("': ").append(identifier).append(" (#").append(lineNumber.toString()).append("): ").append(errorMessage).append("\n");
						log.warn("Invalid process status '" + processStatusCd + "' - " + errorMessage + " for " + identifier + " (#" + lineNumber.toString() + ")");
				}
				emailContentWriter.flush();
			} catch(SQLException | ParameterException | ConvertException | IOException e) {
				throw new ServiceException("While processing line #" + lineNumber.toString(), e);
			}
			data.clear();
		}
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ResourceFolder rf = getResourceFolder();
		MessageChainStep step = taskInfo.getStep();
		try {
			long dataExchangeFileId = step.getAttribute(CommonAttrEnum.ATTR_FILE_ID, Long.class, true);
			Map<String, Object> params = new HashMap<>();
			try (Connection conn = DataLayerMgr.getConnection("REPORT")) {
				params.put("dataExchangeFileId", dataExchangeFileId);
				params.put("processingThresholdDays", getProcessingThresholdDays());
				DataLayerMgr.executeCall(conn, "START_DATA_EXCHANGE_PROCESSING", params);
				conn.commit();
				boolean proceed = ConvertUtils.getBoolean(params.get("proceed"));
				if(proceed) {
					log.info("Processing Data Exchange File " + dataExchangeFileId);
					String msg;
					boolean success;
					String fileName = ConvertUtils.getString(params.get("fileName"), true);
					Matcher matcher = FILE_NAME_PATTERN.matcher(fileName);
					if(matcher.matches()) {
						try {
							String fileType = matcher.group(1);
							BufferStream emailContentBuffer = new HeapBufferStream();
							Writer emailContentWriter = new OutputStreamWriter(emailContentBuffer.getOutputStream());
							AtomicLong lineNumber = new AtomicLong();
							DatasetHandler<ServiceException> handler;
							switch(fileType.toUpperCase()) {
								case "DEVICEINFO":
									DatasetHandlerSelectorFacade<ServiceException> selector = new DatasetHandlerSelectorFacade<>("Record Type");
									handler = selector;
									selector.addHandler("DI", new ProcessDatasetHandler(conn, "PROCESS_DATA_EXCHANGE_DEVICE_INFO", "Device Serial Number", dataExchangeFileId, fileName, lineNumber, emailContentWriter));
									selector.addHandler("DS", new ProcessDatasetHandler(conn, "PROCESS_DATA_EXCHANGE_DEVICE_SETTING", "Device Serial Number", dataExchangeFileId, fileName, lineNumber, emailContentWriter));
									break;
								default:
									handler = null;
							}
							if(handler != null) {
								int count = 0;
								String resourceKey = ConvertUtils.getString(params.get("resourceKey"), true);
								Resource resource = rf.getResource(resourceKey, ResourceMode.READ);
								try {
									try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
										handler.handleDatasetStart(null);
										String[] headers = null;
										String line;
										while((line = reader.readLine()) != null) {
											lineNumber.incrementAndGet();
											String[] data = StringUtils.split(line, ',', '"', true);
											if(data != null && data.length > 0) {
												if("Record Type".equalsIgnoreCase(data[0]))
													headers = data;
												else {
													handler.handleRowStart();
													for(int i = 0; i < data.length; i++)
														handler.handleValue(headers != null && i < headers.length ? headers[i] : "~" + i, data[i]);
													handler.handleRowEnd();
													count++;
												}
											}
										}
										handler.handleDatasetEnd();
									}
								} finally {
									resource.release();
								}
								msg = "Processed " + count + " items on " + lineNumber + " lines";
								if(!StringUtils.isBlank(getProcessEmailFrom()) && !StringUtils.isBlank(getProcessEmailTo()) && emailContentBuffer.getLength() > 0) {
									emailContentWriter.append("--------------------\n").append(msg).append(".\n");
									Map<String, Object> emailParams = new HashMap<String, Object>();
									emailParams.put("fromEmail", getProcessEmailFrom());
									emailParams.put("fromName", getProcessEmailFrom());
									emailParams.put("toEmail", getProcessEmailTo());
									emailParams.put("toName", getProcessEmailTo());
									emailParams.put("subject", "Processed Data Exchange File '" + fileName + "' (" + dataExchangeFileId + ")");
									emailParams.put("body", emailContentBuffer.getInputStream());
									DataLayerMgr.executeUpdate(conn, "SEND_EMAIL", emailParams);
								}
								success = true;
							} else {
								msg = "File type '" + fileType + "' is not supported";
								success = false;
								log.error(msg + " for inbound Data Exchange File " + dataExchangeFileId);
							}
						} catch(ServiceException | SQLException | DataLayerException | ConvertException | IOException e) {
							params.put("processStatusCd", 'R');
							params.put("processCompleteTs", System.currentTimeMillis());
							params.put("processDetails", e.getMessage());
							DataLayerMgr.executeCall(conn, "UPDATE_DATA_EXCHANGE_FILE", params);
							conn.commit();
							throw e;
						}
					} else {
						msg = "Invalid file name " + fileName;
						success = false;
						log.error(msg + " for inbound Data Exchange File " + dataExchangeFileId);
					}
					params.put("processStatusCd", success ? 'D' : 'R');
					params.put("processCompleteTs", System.currentTimeMillis());
					params.put("processDetails", msg);
					DataLayerMgr.executeCall(conn, "UPDATE_DATA_EXCHANGE_FILE", params);
					conn.commit();
				} else
					log.info("Data Exchange File " + dataExchangeFileId + " is already in process or is complete");
			}
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(AttributeConversionException | DataLayerException | ConvertException | IOException e) {
			throw new ServiceException(e);
		}

		return 0;
	}

	protected void process() {


	}
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public float getProcessingThresholdDays() {
		return processingThresholdDays;
	}

	public void setProcessingThresholdDays(float processingThresholdDays) {
		this.processingThresholdDays = processingThresholdDays;
	}

	public String getProcessEmailTo() {
		return processEmailTo;
	}

	public void setProcessEmailTo(String processEmailTo) {
		this.processEmailTo = processEmailTo;
	}

	public String getProcessEmailFrom() {
		return processEmailFrom;
	}

	public void setProcessEmailFrom(String processEmailFrom) {
		this.processEmailFrom = processEmailFrom;
	}
}
