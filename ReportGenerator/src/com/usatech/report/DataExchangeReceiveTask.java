package com.usatech.report;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.CommonAttrEnum;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;

public class DataExchangeReceiveTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			int dataExchangeType = step.getAttribute(CommonAttrEnum.ATTR_FILE_TYPE, int.class, true);
			String[] resourceKeys = ConvertUtils.convertToStringArrayNoParse(step.getAttribute(CommonAttrEnum.ATTR_RESOURCE, Object.class, true));
			String[] filePaths = ConvertUtils.convertToStringArrayNoParse(step.getAttribute(CommonAttrEnum.ATTR_FILE_PATH, Object.class, true));
			Map<String, Object> params = new HashMap<>();
			ResourceFolder rf = getResourceFolder();
			for(int i = 0; i < resourceKeys.length; i++) {
				Resource resource = rf.getResource(resourceKeys[i], ResourceMode.READ);
				try {
					String filePath = (filePaths.length > i ? filePaths[i] : resource.getName());
					log.info("Recorded file from " + filePath + " for Data Exchange Type " + dataExchangeType);
					params.clear();
					params.put("dataExchangeType", dataExchangeType);
					int pos = filePath.lastIndexOf('/');
					String fileName = filePath.substring(pos + 1);
					params.put("fileName", fileName);
					params.put("resource", resource);
					DataLayerMgr.executeCall("RECORD_DATA_EXCHANGE_FILE", params, true);
					long dataExchangeFileId = ConvertUtils.getLong(params.get("dataExchangeFileId"));
					char processStatus = ConvertUtils.getChar(params.get("processStatus"));
					if(processStatus == 'P') {
						MessageChain mc = new MessageChainV11();
						MessageChainStep processStep = mc.addStep("usat.data.exchange.process");
						processStep.setAttribute(CommonAttrEnum.ATTR_FILE_ID, dataExchangeFileId);
						MessageChainService.publish(mc, taskInfo.getPublisher());
					}
				} finally {
					resource.release();
				}
			}
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(AttributeConversionException | DataLayerException | ConvertException | IOException e) {
			throw new ServiceException(e);
		}

		return 0;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
}
