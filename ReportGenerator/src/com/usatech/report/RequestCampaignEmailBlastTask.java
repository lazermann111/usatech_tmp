package com.usatech.report;


import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.Publisher;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.QuartzCronScheduledJob;

@DisallowConcurrentExecution
public class RequestCampaignEmailBlastTask extends QuartzCronScheduledJob {
	protected static Publisher<ByteInput> publisher;	
	private static final Log log = Log.getLog();
	protected static String queueKey="usat.campaign.blast";
	
	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {
		try {
			Results requestResult = DataLayerMgr.executeQuery("GET_CAMPAIGN_BLAST_REQUEST", null);
			while(requestResult.next()) {
				MessageChain mc = new MessageChainV11();
				MessageChainStep step = mc.addStep(getQueueKey());
				step.addLongAttribute("campaignBlastId", ConvertUtils.getLong(requestResult.get("campaignBlastId")));
				MessageChainService.publish(mc, publisher);
				DataLayerMgr.executeCall("UPDATE_CAMPAIGN_BLAST_REQUEST", new Object[] {requestResult.get("campaignBlastId")}, true);
			}
		} catch(Exception e) {
			log.error("Could not request campaign email blast", e);
		}

	}

	public static Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public static void setPublisher(Publisher<ByteInput> publisher) {
		RequestCampaignEmailBlastTask.publisher = publisher;
	}

	public static String getQueueKey() {
		return queueKey;
	}

	public static void setQueueKey(String queueKey) {
		RequestCampaignEmailBlastTask.queueKey = queueKey;
	}
	
	

}
