package com.usatech.report;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum ReportingPrivilege {
	PRIV_USER_ADMIN(1),
	PRIV_REGION_ADMIN(2),
	PRIV_CREATE_BANK_ACCT(3),
	PRIV_ACTIVATE_TERMINAL(4),
	PRIV_ADMIN_ALL_USERS(5),
	PRIV_VIEW_ALL_TERMS(6),
	PRIV_EDIT_ALL_TERMS(7),
	PRIV_ADMIN_ALL_CUST_USERS(8),
	PRIV_APPROVE_EFT(9),
	PRIV_LOGIN_AS_CUST(10),
	PRIV_VIEW_ALL_BANK_ACCTS(11),
	PRIV_REFUND(12),
	PRIV_CHARGEBACK(13),
	PRIV_OVERRIDE_REFUND(14),
	PRIV_OVERRIDE_CHARGEBACK(15),
	PRIV_CUSTOMER_SERVICE(16),
	PRIV_FOLIO_DESIGN(17),
	PRIV_PAYMENTS_ADMIN(18),
	PRIV_FOLIO_ADMIN(19),
	PRIV_SYSTEM_ADMIN(20),
	PRIV_LOGIN_READONLY_AS_ANY_CUSTOMER(21),
	PRIV_MANAGE_CAMPAIGN(22),
	PRIV_SET_BATCH_CLOSE_TIME(23),
	PRIV_COLUMN_MAPPINGS(24),
	PRIV_ADVANCED_DASHBOARD(25),
	PRIV_BACK_OFFICE(26),
	PRIV_RMA(27),
	PRIV_CONFIGURE_DEVICE(28),
	PRIV_PARTNER(29),
	PRIV_RMA_RENTRAL_FOR_CREDIT(30),
	PRIV_MANAGE_PREPAID_CONSUMERS(31), // PREPAID is a misnomer, this applies to consumers in general, but it was too risky change this name
	PRIV_ADMIN_REFUND(32);
	
	private final int value;
    private ReportingPrivilege(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
    protected final static EnumIntValueLookup<ReportingPrivilege> lookup = new EnumIntValueLookup<ReportingPrivilege>(ReportingPrivilege.class);
    public static ReportingPrivilege getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
}
