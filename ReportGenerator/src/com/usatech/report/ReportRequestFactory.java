/*
 * (C) USA Technologies 2011
 */
package com.usatech.report;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import simple.app.Publisher;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.falcon.engine.Layout;
import simple.falcon.engine.Scene;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidIntValueException;
import simple.lang.InvalidValueException;
import simple.results.BeanException;
import simple.results.Results;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;

import com.usatech.app.Attribute;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.TransportAttrEnum;

/**
 * This class contains all of the functionality of the 
 * original ReportRequestMain with regards to creating
 * Report Requests. It is now also used by the Report 
 * Generator in order to create outgoing alert messages.  
 *
 * @author phorsfield
 *
 */
public class ReportRequestFactory {
	private static final Log log = Log.getLog();

	public static final String QUEUE_KEY_TRANSPORT_PREFIX = "usat.ccs.transport.";
	public static final String QUEUE_KEY_GENERATE_PREFIX = "usat.report.generate.";
	public static final String QUEUE_KEY_TRANSPORT_ERROR = "usat.report.transportError";
	public static final String QUEUE_KEY_UPDATE_REPORT_SENT = "usat.report.updateReportSent";

	// -- Configuration properties
	protected Publisher<ByteInput> publisher;	
	protected FolioProperties folioProperties = new FolioProperties();
	protected static final DateFormat batchDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMdd-HHmmss"));

	public enum Fillin {
		CUSTOMER('c', null, "Customer Id"),
		CUSTOMERS('m', "customerIds", "Customer Ids"),
		USER('u', null, "User Id"),
		TERMINAL('t', null, "All terminals"),
		TERMINALS('l', "terminalIds", "Terminal List"),
		REGIONS('g', "regionIds", "Region List"),
		FREQUENCY('f', "frequencyName", "Frequency Name"),
		REPORT('r', "regionIds", "Report Name"),
		BEGIN('b', "beginDate", "Begin Date"),
		END('e', "endDate", "End Date"),
		BATCH('x', "batchId", "Batch Id"),
		DATE('d', null, "Current Date"),
		DEVICE('s', null, "Device Serial Cd"),
		QUALIFIER('q', "qualifier", "A qualifier of the report (used for the alert source)"),
		SCHEDULE('n', "batchType", "Schedule"),
		;
		protected final static EnumCharValueLookup<Fillin> lookup = new EnumCharValueLookup<Fillin>(Fillin.class);
		protected static Map<Character, Fillin> configurables;
		private final char value;
		private final String paramName;
		private final String replaceString;
		private final String description;

		private Fillin(char value, String paramName, String description) {
			this.value = value;
			this.paramName = paramName;
			this.description = description;
			this.replaceString = new String(new char[] { '{', value, '}' });
		}

		public static Fillin getByValue(char value) throws InvalidValueException {
			return lookup.getByValue(value);
		}

		public static Set<Character> getAllValues() {
			return lookup.getAllValues();
		}

		public static Set<Character> getConfigurableValues() {
			if(configurables == null) {
				Map<Character, Fillin> configurables = new HashMap<>();
				for(Fillin fillin : new Fillin[] { BATCH, BEGIN, END, REGIONS, TERMINALS, QUALIFIER, CUSTOMERS })
					configurables.put(fillin.getValue(), fillin);
				Fillin.configurables = Collections.unmodifiableMap(configurables);
			}
			return configurables.keySet();
		}
		public char getValue() {
			return value;
		}

		public String getDescription() {
			return description;
		}

		public String getReplaceString() {
			return replaceString;
		}
		public String getParamName() {
			return paramName;
		}
	}

	protected static final String TRANSPORT_REASON_BATCH_REPORT = "BATCH_REPORT";
	protected static final String TRANSPORT_REASON_TIMED_REPORT = "TIMED_REPORT";
	
	protected int transportMaxRetryAllowed = 5;
	protected long maxStandbyWait = 900000L;

	public int getTransportMaxRetryAllowed() {
		return transportMaxRetryAllowed;
	}

	public void setTransportMaxRetryAllowed(int transportMaxRetryAllowed) {
		this.transportMaxRetryAllowed = transportMaxRetryAllowed;
	}
	
	// Folio configuration from program properties
	public static class FolioProperties { 
		public String baseUrl;
		public Locale locale;
		public String runReportAction;
		public String getBaseUrl() {
			return baseUrl;
		}
		public void setBaseUrl(String baseUrl) {
			this.baseUrl = baseUrl;
		}
		public Locale getLocale() {
			return locale;
		}
		public void setLocale(Locale locale) {
			this.locale = locale;
		}
		public void setLocale(String locale) throws ConvertException {
			this.locale = ConvertUtils.convert(Locale.class, locale);			
		}
		public String getRunReportAction() {
			return runReportAction;
		}
		public void setRunReportAction(String runReportAction) {
			this.runReportAction = runReportAction;
		}
	}

	/**
	 * Invokes generateReport with the pre-publish callback function set to nothing 
	 */
	public void generateReport(ReportScheduleType scheduleType, int userReportId, long userId, int reportId, Generator generator, String title,
 Long batchId, java.util.Date beginDate, java.util.Date endDate, String frequencyName, Long refreshStoredFileId, String deviceSerialCd) throws ServiceException {
		generateReport(scheduleType, userReportId, userId, reportId, generator, title, batchId, beginDate, endDate, frequencyName, refreshStoredFileId, deviceSerialCd, null);
	}

	public static void generateReportParameters(long profileId, int reportId, ReportScheduleType scheduleType, Long batchId, java.util.Date beginDate, java.util.Date endDate, String frequencyName, String deviceSerialCd, String qualifier, long[] terminalIds, long[] regionIds, Map<String, ? super String> reportParams, String title, Map<String, Object> userParams, long[] customerIds)
			throws SQLException, DataLayerException, ConvertException, BeanException, InvalidIntValueException, ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reportId", reportId);
		params.put("userId", profileId);
		DataLayerMgr.selectInto("GET_BASIC_REPORT", params);
		String reportName = ConvertUtils.getString(params.get("name"), true);
		boolean permitted = ConvertUtils.getBoolean(params.get("permitted"), false);
		if(!permitted)
			throw new RetrySpecifiedServiceException("You do not have permission to run '" + reportName + "'", WorkRetryType.NO_RETRY);
		if(StringUtils.isBlank(title))
			title = ConvertUtils.getString(params.get("title"), false);
		int generatorId = ConvertUtils.getInt(params.get("generatorId"));
		if(scheduleType == null)
			scheduleType = ReportScheduleType.getByValue(ConvertUtils.getInt(params.get("scheduleTypeId")));
		generateReportParameters(scheduleType, profileId, reportId, Generator.getByValue(generatorId), title, batchId, beginDate, endDate, frequencyName, deviceSerialCd, qualifier, terminalIds, regionIds, reportParams, userParams,customerIds);
	}

	/**
	 * Creates a report request chain.
	 * 
	 * @param prePublishCallback
	 *            If set, this callback is invoked just prior to the request chain being published to the message queues
	 * @throws DataLayerException
	 * @throws SQLException
	 * @throws ConvertException
	 */
	public static ReportRequest<BasicReport> generateReportRequest(ReportScheduleType scheduleType, long profileId, long userId, int reportId, Generator generator, String title, Long batchId, java.util.Date beginDate, java.util.Date endDate, String frequencyName, String deviceSerialCd, String qualifier, Scene scene) throws SQLException, DataLayerException,
			ConvertException {
		SortedMap<String, String> params = new TreeMap<String, String>();
		params.put("report.basicReportId", String.valueOf(reportId));
		params.put("report.batchType", String.valueOf(scheduleType.getValue()));
		if(batchId != null)
			params.put("report.batchId", String.valueOf(batchId));
		if(beginDate != null)
			params.put("report.beginDate", new StringBuilder(21).append('{').append(beginDate.getTime()).append('}').toString());
		if(endDate != null)
			params.put("report.endDate", new StringBuilder(21).append('{').append(endDate.getTime()).append('}').toString());
		if(frequencyName != null)
			params.put("report.frequencyName", frequencyName);
		if(deviceSerialCd != null)
			params.put("report.deviceSerialCd", deviceSerialCd);
		if(qualifier != null)
			params.put("report.qualifier", qualifier);
		BasicReport report = new BasicReport();
		report.setTitle(title);
		return new ReportRequest<BasicReport>(generator, report, scene, profileId, userId, params, title);
	}

	/**
	 * Creates parameters
	 * 
	 * @param prePublishCallback
	 *            If set, this callback is invoked just prior to the request chain being published to the message queues
	 * @throws DataLayerException
	 * @throws SQLException
	 * @throws ConvertException
	 */
	public static void generateReportParameters(ReportScheduleType scheduleType, long profileId, int reportId, Generator generator, String title, Long batchId, java.util.Date beginDate, java.util.Date endDate, String frequencyName, String deviceSerialCd, String qualifier, long[] terminalIds, long[] regionIds, Map<String, ? super String> params, Map<String, Object> userParams,long[] customerIds)
			throws SQLException, DataLayerException,
			ConvertException {
		retrieveParams(reportId, params, userParams);
		String dateFormat = ConvertUtils.getString(params.get("report.dateFormat"), false);
		if(dateFormat == null || dateFormat.trim().length() == 0) {
			dateFormat = "MM-dd-yyyy HH:mm:ss";
			params.put("report.dateFormat", dateFormat);
		}
		java.text.DateFormat df = new java.text.SimpleDateFormat(dateFormat);
		
		String beginDateFormat = ConvertUtils.getString(params.get("report.beginDateFormat"), false);
		if(beginDateFormat == null || beginDateFormat.trim().length() == 0) {
			beginDateFormat = dateFormat;
			params.put("report.beginDateFormat", beginDateFormat);
		}
		java.text.DateFormat beginDf = new java.text.SimpleDateFormat(beginDateFormat);
			
		String endDateFormat = ConvertUtils.getString(params.get("report.endDateFormat"), false);
		if(endDateFormat == null || endDateFormat.trim().length() == 0) {
			endDateFormat = dateFormat;
			params.put("report.endDateFormat", endDateFormat);
		}
		java.text.DateFormat endDf = new java.text.SimpleDateFormat(endDateFormat);
		
		Map<String, Object> fillins = new HashMap<String, Object>();
		// fillins.put(Fillin.CUSTOMER.getReplaceString(),"" + customerId); //customer id
		if(!StringUtils.isBlank(deviceSerialCd))
			fillins.put(Fillin.DEVICE.getReplaceString(), deviceSerialCd); // serial number
		else {
			if(batchId!=null){
				Object deviceSerialCdFillin = getDeviceSerialCdFillin(scheduleType, batchId);
				if(deviceSerialCdFillin != null)
					fillins.put(Fillin.DEVICE.getReplaceString(), deviceSerialCdFillin);
			}
		}

		fillins.put(Fillin.USER.getReplaceString(), ConvertUtils.getStringSafely(profileId)); // user id
		fillins.put(Fillin.TERMINAL.getReplaceString(), getTerminalsFillin(profileId)); // terminals
		fillins.put(Fillin.FREQUENCY.getReplaceString(), frequencyName); // frequency
		fillins.put(Fillin.QUALIFIER.getReplaceString(), qualifier);
		fillins.put(Fillin.SCHEDULE.getReplaceString(), scheduleType.toString());
		fillins.put(Fillin.TERMINALS.getReplaceString(), terminalIds);
		fillins.put(Fillin.REGIONS.getReplaceString(), regionIds);
		fillins.put(Fillin.CUSTOMERS.getReplaceString(), customerIds);

		java.text.DateFormat dateNoTimeFormat = null;
		Date today = null;
		Date todayDate = null;
		int timedDateChangeDays = 0;
		String userAgent = "";
		if (userParams != null)
			userAgent = ConvertUtils.getStringSafely(userParams.get("scene.userAgent"), "");
		// apply configured date modifications only if the report is NOT requested interactively in USALive UI 
		if (StringUtils.isBlank(userAgent)) {
			timedDateChangeDays = ConvertUtils.getIntSafely(params.get("report.timedDateChangeDays"), 0);
			if (timedDateChangeDays != 0) {
				today = new Date();
				dateNoTimeFormat = new java.text.SimpleDateFormat("MM/dd/yyyy");
				try {
					todayDate = dateNoTimeFormat.parse(dateNoTimeFormat.format(today));
				} catch (ParseException e) {
					throw new DataLayerException("Error getting todayDate", e);
				}
			}
		}

		switch(scheduleType) {
			case TIMED:
				if(beginDate != null) {
					if (timedDateChangeDays != 0) {
						Date beginDateNoTime;
						try {
							beginDateNoTime = dateNoTimeFormat.parse(dateNoTimeFormat.format(beginDate));
						} catch (ParseException e) {
							throw new DataLayerException("Error getting beginDateNoTime", e);
						}

						if (beginDateNoTime.equals(todayDate)) {
							Calendar cal = Calendar.getInstance();
							cal.setTime(beginDate);
							cal.add(Calendar.DATE, timedDateChangeDays);
							beginDate = cal.getTime();
						}
					}
					fillins.put(Fillin.BEGIN.getReplaceString(), beginDf.format(beginDate));
				}
				if(endDate != null) {
					if (timedDateChangeDays != 0 && beginDate != null) {
						Date beginDateNoTime;
						try {
							beginDateNoTime = dateNoTimeFormat.parse(dateNoTimeFormat.format(beginDate));
						} catch (ParseException e) {
							throw new DataLayerException("Error getting beginDateNoTime", e);
						}

						Calendar cal = Calendar.getInstance();
						cal.setTime(endDate);
						cal.add(Calendar.DATE, timedDateChangeDays);
						if (cal.getTime().compareTo(beginDateNoTime) >= 0)
							endDate = cal.getTime();
					}
					fillins.put(Fillin.END.getReplaceString(), endDf.format(endDate));
				}
				fillins.put(Fillin.DATE.getReplaceString(), df.format(new java.util.Date()));
				break;
			case DAILY_EXPORT:
			case DEX:
			case RDC:
			case PAYMENT:
			case PENDING_PAYMENT:
			case EVENT:
			case FILE_TRANSFER_EVENT:
			case CUSTOM_FILE_UPLOAD:
			case CONDITION:
			case SINGLE_TXN:
				if(batchId != null)
					fillins.put(Fillin.BATCH.getReplaceString(), ConvertUtils.getStringSafely(batchId));
				fillins.put(Fillin.DATE.getReplaceString(), batchDateFormat.format(new java.util.Date()));
				break;
		}

		title = replaceFillins(title, fillins);
		fillins.put(Fillin.REPORT.getReplaceString(), title); // report name
		translateParams(params, fillins);
		params.put("report.title", title);
		if(scheduleType.equals(ReportScheduleType.DEX)) {
			params.put("report.dexId", String.valueOf(batchId));
		} else if(scheduleType.equals(ReportScheduleType.CUSTOM_FILE_UPLOAD)) {
			params.put("report.fileTransferId", String.valueOf(batchId));
		} else if(scheduleType.equals(ReportScheduleType.SINGLE_TXN)) {
			params.put("report.reportTranId", String.valueOf(batchId));
		} else if(scheduleType.equals(ReportScheduleType.PAYMENT) && generator != null && generator.equals(Generator.EFT_XML)) {
			params.put("report.docId", String.valueOf(batchId));
		}
		params.put("report.basicReportId", String.valueOf(reportId));
	}

	public void generateReport(ReportScheduleType scheduleType, int userReportId, long userId, int reportId, Generator generator, String title, Long batchId, java.util.Date beginDate, java.util.Date endDate, String frequencyName, Long refreshStoredFileId, String deviceSerialCd, String qualifier) throws ServiceException {
		try {
			Scene scene = new Scene();
			String baseUrl = folioProperties.baseUrl;
			if(baseUrl != null && baseUrl.length() > 0 && !baseUrl.endsWith("/") && !baseUrl.endsWith("\\"))
				baseUrl += "/";
			scene.setBaseUrl(baseUrl);
			scene.setLocale(folioProperties.locale);
			scene.setRunReportAction(folioProperties.runReportAction);
			scene.setLayout(Layout.OFFLINE);
			// TODO: get and set timezone in scene

			ReportRequest<BasicReport> request = generateReportRequest(scheduleType, userId, userId, reportId, generator, title, batchId, beginDate, endDate, frequencyName, deviceSerialCd, qualifier, scene);
			Connection conn = DataLayerMgr.getConnectionForCall("REGISTER_AUTO_REPORT_REQUEST");
			try {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("request", request);
				if(refreshStoredFileId != null)
					params.put("refreshFileId", refreshStoredFileId);
				params.put("userId", userId);
				params.put("userReportId", userReportId);
				switch(scheduleType) {
					case TIMED:
						params.put("beginDate", beginDate);
						params.put("endDate", endDate);
						break;
					case DAILY_EXPORT:
					case DEX:
					case RDC:
					case PAYMENT:
					case PENDING_PAYMENT:
					case EVENT:
					case FILE_TRANSFER_EVENT:
					case CUSTOM_FILE_UPLOAD:
					case CONDITION:
					case SINGLE_TXN:
						params.put("batchId", batchId);
						break;
				}
				params.put("batchTypeId", scheduleType.getValue());
				
				DataLayerMgr.executeUpdate(conn, "REGISTER_AUTO_REPORT_REQUEST", params);
				conn.commit(); // must commit to release user application lock in database
				long requestId = ConvertUtils.getLong(params.get("requestId"));
				int reportSentId = ConvertUtils.getInt(params.get("reportSentId"));
				Character reusing = ConvertUtils.convert(Character.class, params.get("reusingRequest"));
				Long storedFileId = ConvertUtils.convert(Long.class, params.get("storedFileId"));
				if(storedFileId != null) {
					log.info("Report is already generated");
					//queue to transporter
					publishTransportReport(request.getGenerator(), userReportId, reportSentId, scheduleType, batchId, beginDate, endDate, storedFileId,
							ConvertUtils.getString(params.get("fileName"), true),
							ConvertUtils.getString(params.get("fileKey"), true),
							ConvertUtils.getString(params.get("fileContentType"), false),
							ConvertUtils.getString(params.get("filePasscode"), true), conn);
				} else if(reusing == null || reusing != 'Y') {
					log.info("Publishing report to generation queue reusing="+reusing);
					publishReportRequest(requestId, request, title, userReportId, reportSentId, scheduleType, batchId, beginDate, endDate, conn);
				} else{
					log.info("Report is already requested but not yet generated requestId="+requestId+" reportSentId="+reportSentId);
					// right now we must re-generate anyway to have a continuous message chain
					publishReportRequest(requestId, request, title, userReportId, reportSentId, scheduleType, batchId, beginDate, endDate, conn);
				}
				DataLayerMgr.executeUpdate(conn, "MARK_AUTO_REPORT_REQUESTED", params);
				conn.commit();
			} catch(RuntimeException e) {
				try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } // must rollback to release user application lock in database
				throw e;
			} catch(Error e) {
				try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } // must rollback to release user application lock in database
				throw e;
			} catch(ServiceException e) {
				try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } // must rollback to release user application lock in database
				throw e;
			} catch(ConvertException e) {
				try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } // must rollback to release user application lock in database
				throw new ServiceException(e);
			} catch(DataLayerException e) {
				try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } // must rollback to release user application lock in database
				throw new ServiceException(e);
			} catch(SQLException e) {
				try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } // must rollback to release user application lock in database
				throw new ServiceException(e);
			} catch(BeanException e) {
				try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } // must rollback to release user application lock in database
				throw new ServiceException(e);
			} finally {
				conn.close();
			}
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(ConvertException e) {
			throw new ServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}

	}

	/**
	 * @param requestId
	 * @param request
	 * @param title
	 * @param userReportId
	 * @param reportSentId
	 * @param prePublishCallback 
	 * @throws ServiceException
	 * @throws SQLException
	 * @throws DataLayerException
	 * @throws ConvertException
	 * @throws BeanException
	 */
	protected void publishReportRequest(long requestId, ReportRequest<?> request, String title, long userReportId, long reportSentId, ReportScheduleType scheduleType, Long batchId, Date beginDate, Date endDate, Connection conn) throws ServiceException, ConvertException, DataLayerException, SQLException, BeanException {
		MessageChainV11 mc = new MessageChainV11();
		MessageChainStep generateStep = mc.addStep(QUEUE_KEY_GENERATE_PREFIX + request.getGenerator().getValue());

		generateStep.setAttribute(GenerateAttrEnum.ATTR_REPORT_SENT_ID, reportSentId);
		generateStep.setAttribute(GenerateAttrEnum.ATTR_USE_STANDBY, StandbyAllowance.NONE);
		generateStep.setAttribute(GenerateAttrEnum.ATTR_MAX_STANDBY_WAIT, getMaxStandbyWait());
		generateStep.setCorrelation(request.getUserId());
		request.populateGenerateStep(generateStep, requestId);

		MessageChainStep transportStep = addTransportSteps(request.getGenerator(), mc, scheduleType, batchId, beginDate, endDate, userReportId, reportSentId, conn);
		transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_NAME, generateStep, TransportAttrEnum.ATTR_FILE_NAME);
		transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_KEY, generateStep, TransportAttrEnum.ATTR_FILE_KEY);
		transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_CONTENT_TYPE, generateStep, TransportAttrEnum.ATTR_FILE_CONTENT_TYPE);
		transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_ID, generateStep, TransportAttrEnum.ATTR_FILE_ID);
		transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_PASSCODE, generateStep, TransportAttrEnum.ATTR_FILE_PASSCODE);
		transportStep.setAttribute(TransportAttrEnum.ATTR_TRANSPORT_MAX_RETRY_ALLOWED, getTransportMaxRetryAllowed());

		generateStep.setNextSteps(0, transportStep);

		MessageChainService.publish(mc, publisher);
	}

	public static MessageChainStep createTransportStep(MessageChain mc, long transportId, int transportTypeId, Connection conn) throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> tranParams = new HashMap<String, Object>(6);
		tranParams.put("transportId", transportId);
		tranParams.put("transportTypeId", transportTypeId);
		MessageChainStep transportStep = mc.addStep(QUEUE_KEY_TRANSPORT_PREFIX + transportTypeId);
		transportStep.setCorrelation(transportId);
		final Results propResults;
		if(conn == null)
			propResults = DataLayerMgr.executeQuery("GET_TRANSPORT_PROPERTIES", tranParams, false);
		else
			propResults = DataLayerMgr.executeQuery(conn, "GET_TRANSPORT_PROPERTIES", tranParams);
		try {
			while(propResults.next()) {
				boolean sensitive = propResults.getValue("sensitive", Boolean.class);
				final String name = "transport." + ConvertUtils.convert(String.class, propResults.getValue("name"));
				Attribute a = new Attribute() {
					public String getValue() {
						return name;
					}
				};
				String value = ConvertUtils.convert(String.class, propResults.getValue("value"));
				if(sensitive)
					transportStep.setSecretAttribute(a, value);
				else
					transportStep.setAttribute(a, value);
			}
		} finally {
			propResults.close();
		}
		transportStep.setAttribute(TransportAttrEnum.ATTR_TRANSPORT_TYPE_ID, transportTypeId);
		// step.addStringAttribute("transport.transportClassName", transportClassName); // not needed
		return transportStep;
	}

	private MessageChainStep addTransportSteps(Generator generator, MessageChain mc, ReportScheduleType scheduleType, Long batchId, Date beginDate, Date endDate, long userReportId, long reportSentId, Connection conn) throws ConvertException, DataLayerException, SQLException, BeanException {
		Map<String, Object> tranParams = new HashMap<String, Object>(6);
		tranParams.put("userReportId", userReportId);
		if (conn == null)
			DataLayerMgr.selectInto("GET_TRANSPORT", tranParams);
		else
			DataLayerMgr.selectInto(conn, "GET_TRANSPORT", tranParams);
		long transportId = ConvertUtils.getLong(tranParams.get("transportId"));
		int transportTypeId = ConvertUtils.getInt(tranParams.get("transportTypeId"));

		MessageChainStep transportStep = createTransportStep(mc, transportId, transportTypeId, conn);
		MessageChainStep updateReportSentStep = mc.addStep(QUEUE_KEY_UPDATE_REPORT_SENT);
		MessageChainStep transportErrorStep = mc.addStep(QUEUE_KEY_TRANSPORT_ERROR);

		transportStep.setNextSteps(0, updateReportSentStep);
		transportStep.setNextSteps(1, transportErrorStep);

		switch(scheduleType) {
			case DAILY_EXPORT: case DEX: case RDC: case PAYMENT: case PENDING_PAYMENT:
				transportStep.addLongAttribute("message.batchId", batchId);
				transportErrorStep.addLongAttribute("message.batchId", batchId);
				break;
			case TIMED:
				transportStep.addLongAttribute("message.beginDate", beginDate.getTime());
				transportStep.addLongAttribute("message.endDate", endDate.getTime());
				transportErrorStep.addLongAttribute("message.beginDate", beginDate.getTime());
				transportErrorStep.addLongAttribute("message.endDate", endDate.getTime());
				break;
			case SINGLE_TXN:
				transportStep.addStringAttribute("message.MessageType", "MktCreditTxn");
				transportErrorStep.addStringAttribute("message.MessageType", "MktCreditTxn");
				break;
		}
		if(scheduleType.equals(ReportScheduleType.PAYMENT) && generator!=null&&generator.equals(Generator.EFT_XML)){
			transportStep.addStringAttribute("message.MessageType", "MktEFT");
			transportErrorStep.addStringAttribute("message.MessageType", "MktEFT");
		}
		transportStep.addStringAttribute("transport.transportReason", scheduleType.getTransportReason().toString());
		transportErrorStep.addStringAttribute("transport.transportReason", scheduleType.getTransportReason().toString());

		if(scheduleType == ReportScheduleType.DEX) {
			Results dexResults;
			if (conn == null)
				dexResults = DataLayerMgr.executeQuery("GET_DEX_FILE_INFO", new Object[] { batchId }, false);
			else
				dexResults = DataLayerMgr.executeQuery(conn, "GET_DEX_FILE_INFO", new Object[] { batchId });
			if(dexResults.next()) {
				String deviceSerialCd = ConvertUtils.convert(String.class, dexResults.getValue("deviceSerialCd"));
				int dexType = ConvertUtils.getInt(dexResults.getValue("dexType"));
				String assetNumber = ConvertUtils.convert(String.class, dexResults.getValue("assetNumber"));
				String fillFlag = ConvertUtils.getString(dexResults.getValue("fillFlag"), true);
				Calendar dexTime = ConvertUtils.convert(Calendar.class, dexResults.getValue("dexTime"));
				String dexGMTTime = ConvertUtils.convert(String.class, dexResults.getValue("dexGMTTime"));
				String timeZoneGuid = ConvertUtils.convert(String.class, dexResults.getValue("timeZoneGuid"));
				transportStep.addStringAttribute("message.deviceSerialCd", deviceSerialCd);
				transportStep.addIntAttribute("message.dexType", dexType);
				transportStep.addStringAttribute("message.assetNumber", assetNumber);
				transportStep.addStringAttribute("message.fillFlag", fillFlag);
				transportStep.addStringAttribute("message.dexTime", ConvertUtils.getStringSafely(dexTime));
				transportStep.addStringAttribute("message.dexGMTTime", dexGMTTime);
				transportStep.addStringAttribute("message.timezone", timeZoneGuid);

				// reattemp needed attributes
				transportErrorStep.addStringAttribute("message.deviceSerialCd", deviceSerialCd);
				transportErrorStep.addIntAttribute("message.dexType", dexType);
				transportErrorStep.addStringAttribute("message.assetNumber", assetNumber);
				transportErrorStep.addStringAttribute("message.fillFlag", fillFlag);
				transportErrorStep.addStringAttribute("message.dexTime", ConvertUtils.getStringSafely(dexTime));
				transportErrorStep.addStringAttribute("message.dexGMTTime", dexGMTTime);
				transportErrorStep.addStringAttribute("message.timezone", timeZoneGuid);
			} else {
				log.debug("Resultset of SQL GET_DEX_FILE_INFO is empty for dexId " + batchId);
			}
		}

		updateReportSentStep.addLongAttribute("reportSentId", reportSentId);
		updateReportSentStep.addReferenceAttribute("sentDate", transportStep, "sentDate");
		updateReportSentStep.addReferenceAttribute(TransportAttrEnum.ATTR_SENT_DETAILS.getValue(), transportStep, TransportAttrEnum.ATTR_SENT_DETAILS.getValue());

		transportErrorStep.addLongAttribute("reportSentId", reportSentId);
		transportErrorStep.addReferenceAttribute("error.errorText", transportStep, "error.errorText");
		transportErrorStep.addReferenceAttribute("error.errorTs", transportStep, "error.errorTs");
		transportErrorStep.addReferenceAttribute("error.errorTypeId", transportStep, "error.errorTypeId");
		transportErrorStep.addReferenceAttribute("error.retryProps", transportStep, "error.retryProps");
		return transportStep;
	}

	private void publishTransportReport(Generator generator, long userReportId, long reportSentId, ReportScheduleType scheduleType, Long batchId, Date beginDate, Date endDate, long storedFileId, String fileName, String fileKey, String fileContentType, String filePasscode, Connection conn) throws ConvertException, DataLayerException, SQLException, BeanException, ServiceException {
		MessageChainV11 mc = new MessageChainV11();
		MessageChainStep transportStep = addTransportSteps(generator, mc, scheduleType, batchId, beginDate, endDate, userReportId, reportSentId, conn);
		transportStep.addStringAttribute("file.fileName", fileName);
		transportStep.addStringAttribute("file.fileKey", fileKey);
		transportStep.addStringAttribute("file.fileContentType", fileContentType);
		transportStep.addLongAttribute("transport.fileId", storedFileId);
		transportStep.addStringAttribute("transport.filePasscode", filePasscode);

		MessageChainService.publish(mc, publisher);
	}
	public static void addLiteralAttributeHelper(MessageChainStep step, Map<String, String> params) {
		for(Map.Entry<String, String> entry : params.entrySet()) {
			step.addStringAttribute(entry.getKey(), entry.getValue());
		}
	}

	protected static void retrieveParams(int reportId, Map<String, ? super String> params, Map<String, Object> userParams) throws SQLException, DataLayerException {
		try {
			Results results = DataLayerMgr.executeQuery("GET_PARAMETERS", new Object[] { reportId }, false);
			while(results.next()) {
				String name = results.getFormattedValue("name");
				String value = results.getFormattedValue("value");
				boolean configurable = ConvertUtils.getBooleanSafely(results.getValue("configurable"), false);
				if(StringUtils.isBlank(name))
					continue;
				String userValue;
				if(configurable && !StringUtils.isBlank(userValue = ConvertUtils.getStringSafely(userParams.get(name))))
					params.put("report." + name, userValue);
				else if(!StringUtils.isBlank(value))
					params.put("report." + name, value);
			}
		} catch(SQLException e) {
			log.warn("Exception while retrieving report parameters", e);
			throw e;
		} catch(DataLayerException e) {
			log.warn("Exception while retrieving report parameters", e);
			throw e;
		}
	}

	public static void translateParams(Map<String, ? super String> params, Map<String, Object> fillins) throws ConvertException {
		for(Map.Entry<String, ? super String> entry : params.entrySet()) {
			entry.setValue(replaceFillins(entry.getValue(), fillins));
		}
	}

	public static String replaceFillins(Object string, Map<String, Object> fillins) throws ConvertException {
		StringBuffer value = new StringBuffer(ConvertUtils.getString(string, false));
		boolean inBrace = false;
		StringBuffer replace = new StringBuffer();
		for(int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			switch(c) {
				case '}':
					replace.append(c);
					String fillin = ConvertUtils.getStringSafely(fillins.get(replace.toString()));
					if(fillin == null) {
						if(fillins.containsKey(replace.toString()))
							fillin = "";
						else
							fillin = replace.toString();
					}

					value.replace(i - replace.length() + 1, i + 1, fillin);
					log.debug("Replaced " + replace + " with " + fillin + " at " + i);
					i = i + fillin.length() - replace.length();
					replace.setLength(0);
					inBrace = false;
					break;
				case '{':
					inBrace = true;
				default:
					if(inBrace)
						replace.append(c);

			}
		}
		return value.toString();
	}

	protected static Object getTerminalsFillin(final long userId) {
		return new Object() {
			@Override
			public String toString() {
				StringBuffer sb = new StringBuffer();
				try {
					Results results = DataLayerMgr.executeQuery("GET_TERMINALS", new Object[] { userId }, false);
					while(results.next()) {
						if(sb.length() > 0)
							sb.append(',');
						sb.append(results.getValue(1));
					}
				} catch(SQLException e) {
					log.warn("Exception while attemping to retrieve terminals for user id = " + userId, e);
				} catch(DataLayerException e) {
					log.warn("Exception while attemping to retrieve terminals for user id = " + userId, e);
				}
				return sb.toString();
			}
		};
	}

	protected static Object getDeviceSerialCdFillin(final ReportScheduleType scheduleType, final long batchId) {
		final String callId;
		switch(scheduleType) {
			case FILE_TRANSFER_EVENT:
				callId = "GET_DEVICE_SERIAL_CD_BY_FILE_TRANSFER_ID";
				break;
			case DEX:
				callId = "GET_DEVICE_SERIAL_CD_BY_DEX_ID";
				break;
			case EVENT:
				callId = "GET_DEVICE_SERIAL_CD_BY_EVENT_ID";
				break;
			case SINGLE_TXN:
				callId = "GET_DEVICE_SERIAL_CD_BY_TRAN_ID";
				break;
			default:
				return null;
		}
		return new Object() {
			@Override
			public String toString() {
				Results results;
				try {
					results = DataLayerMgr.executeQuery(callId, new Object[] { batchId });
					if(results.next())
						return ConvertUtils.getStringSafely(results.getValue("deviceSerialCd"));
				} catch(SQLException | DataLayerException e) {
					log.warn("Exception while attemping to retrieve device serial cd for " + scheduleType + " with batch id = " + batchId, e);
				}
				return null;
			}
		};
	}

	public FolioProperties getFolioProperties() {
		return folioProperties;
	}

	public void setFolioProperties(FolioProperties folioProperties) {
		this.folioProperties = folioProperties;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public static boolean isGraphOutputType(int outputType) {
		switch(outputType) {
			case 28:
			case 29:
			case 30:
			case 31:
			case 34:
			case 35:
			case 36:
				return true;
			default:
				return false;
		}
	}

	public long getMaxStandbyWait() {
		return maxStandbyWait;
	}

	public void setMaxStandbyWait(long maxStandbyWait) {
		this.maxStandbyWait = maxStandbyWait;
	}
}
