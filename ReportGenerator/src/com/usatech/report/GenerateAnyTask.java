/**
 *
 */
package com.usatech.report;

import java.util.HashMap;
import java.util.Map;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

import simple.app.ServiceException;

/**
 * @author Brian S. Krug
 *
 */
public class GenerateAnyTask implements MessageChainTask {
	protected final Map<Generator,AbstractGenerateTask> generatorToTask = new HashMap<Generator, AbstractGenerateTask>();
	protected final static AbstractGenerateTask[] EMPTY_ARRAY = new AbstractGenerateTask[0];
	/**
	 * @see com.usatech.app.MessageChainTask#process(com.usatech.app.MessageChainTaskInfo)
	 */
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Generator generator;
		try {
			generator = taskInfo.getStep().getAttribute(GenerateAttrEnum.ATTR_GENERATOR, Generator.class, true);
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		}

		AbstractGenerateTask task = generatorToTask.get(generator);
		if(task == null)
			throw new ServiceException("No task is registered for generator " + generator);
		return task.process(taskInfo);
	}

	public void setTasks(AbstractGenerateTask... tasks) {
		generatorToTask.clear();
		if(tasks != null)
			for(AbstractGenerateTask task : tasks)
				generatorToTask.put(task.getGenerator(), task);
	}

	public AbstractGenerateTask[] getTasks() {
		return generatorToTask.values().toArray(EMPTY_ARRAY);
	}
}
