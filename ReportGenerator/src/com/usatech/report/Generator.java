/**
 * (C) 2011 USA Technologies
 */
package com.usatech.report;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;


/**
 * Enumeration attributes that map to the 
 * defined Generator types in the database
 * and in ReportGenerator.
 * 
 * @author Brian S. Krug
 *
 */
public enum Generator {
	CSV(1, "text/csv"), 
	HTML(2, "application/xhtml+xml"), 
	DEX(3,"text/plain"), 
	DWI(4,"text/plain"), 
	XLS(5,"application/xls"), 
	FOLIO(6, null), 
	CUSTOM(7, null), 
	RDC(8,"text/plain"), 
	ALERT(9,"text/plain"),
	CUSTOM_FILE_UPLOAD(10, null),
	CREDIT_XML(11,"text/xml"),
	EFT_XML(12,"text/xml"),
	BUILD_FOLIO(13, null); /* file content type look up from report.file_ext_to_content_type table default to application/octet-stream */

	private final int value;
	private final String contentType;

	private Generator(int value, String contentType) {
		this.value = value;
		this.contentType=contentType;
	}

	public int getValue() {
        return value;
    }
	
	public String getContentType() {
		return contentType;
	}

	protected final static EnumIntValueLookup<Generator> lookup = new EnumIntValueLookup<Generator>(Generator.class);
    public static Generator getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
}
