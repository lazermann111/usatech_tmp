package com.usatech.report;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

public class TransportErrorTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		Connection conn=null;
		try{
			conn = DataLayerMgr.getConnection("report", false);
			boolean retry=ConvertUtils.getBooleanSafely(attributes.get("retry"), false);
			if(retry){
				//check whether 1 week lapse, if so, disable the transport and notify customer service.
				DataLayerMgr.executeUpdate(conn,"CCS_TRANSPORT_ERROR_UPDATE", attributes);
				int isTransportDisabled=ConvertUtils.getInt(attributes.get("isTransportDisabled"));
				log.info("isTransportDisabled="+isTransportDisabled+" for reportSentId="+attributes.get("reportSentId"));
				if(isTransportDisabled>0){
					log.error("Please take action to resolve the issue or the report will not be generated and resent. Transport failed due to:"+attributes.get("error.errorText"));
				}
			}else{
				Map<String,Object> insertMap=new HashMap<String, Object>(4);
				insertMap.put("reportSentId", ConvertUtils.getInt(attributes.remove("reportSentId")));
				insertMap.put("errorText", attributes.remove("error.errorText"));
				insertMap.put("errorTs", attributes.remove("error.errorTs"));
				insertMap.put("errorTypeId", attributes.remove("error.errorTypeId"));
				insertMap.put("retryProps", attributes.remove("error.retryProps"));
				
				DataLayerMgr.executeUpdate(conn,"CCS_TRANSPORT_ERROR_INS", insertMap);
				int errorId=ConvertUtils.getInt(insertMap.get("errorId"));
				for(Map.Entry<String,Object> entry : attributes.entrySet()) {
					DataLayerMgr.executeUpdate(conn,"CCS_TRANSPORT_ERROR_ATTR_INS", new Object[]{errorId, entry.getKey(), entry.getValue()});
				}
			}
			conn.commit();
			return 0;
		}catch(Exception e){
			try{
				if(conn!=null) conn.rollback();
			}catch(SQLException sqle){log.debug("TransportErrorComponent Failed to rollback.");}
			throw new ServiceException(e);
		} finally {
			try{
				if(conn!=null) conn.close();
			}catch(SQLException sqle){log.debug("TransportErrorComponent Failed to close database connection.");}
		}
	}

}
