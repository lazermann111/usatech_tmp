/**
 * 
 */
package com.usatech.report;

import java.sql.SQLException;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.results.Results;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

/**
 * Request for single transaction report
 * @author yhe
 *
 */
public class RequestSingleTxnTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	
	protected ReportRequestFactory reportRequestFactory;
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ReportScheduleType batchType=ReportScheduleType.SINGLE_TXN;
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		long reportTranId=-1;
		try {
			Results reportResults = DataLayerMgr.executeQuery("GET_UNSENT_SINGLE_TXN_REPORT", attributes, false);
			reportTranId=ConvertUtils.getLong(attributes.get("reportTranId"));
			while(reportResults.next()) {
				ReportInstance rpt = new ReportInstance();
				reportResults.fillBean(rpt);
				log.info("Requesting Single Transaction Report " + rpt.userReportId + " for " +reportTranId);
				reportRequestFactory.generateReport(batchType, rpt.userReportId, rpt.userId, rpt.reportId, Generator.getByValue(rpt.generatorId), rpt.title, reportTranId, null, null, batchType.getDescription(), null, null, null);
			}
		} catch(BeanException e) {
			throw new RetrySpecifiedServiceException("Could not convert report request for tranId " + reportTranId, e, WorkRetryType.NO_RETRY);
		} catch(InvalidIntValueException e) {
			throw new RetrySpecifiedServiceException("Could not convert generator id for tranId " + reportTranId, e, WorkRetryType.NO_RETRY);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException("Could not convert generator id for tranId " + reportTranId, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not generate instant dex batch report request for tranId " + reportTranId, e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not generate instant dex batch report request for tranId " + reportTranId, e);
		}
		return 0;
	}
	
	public ReportRequestFactory getReportRequestFactory() {
		return reportRequestFactory;
	}

	public void setReportRequestFactory(ReportRequestFactory reportRequestFactory) {
		this.reportRequestFactory = reportRequestFactory;
	}
}

