package com.usatech.report;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;

import com.usatech.layers.common.model.PayrollSchedule;

public class PayPeriodFrequency extends AbstractFrequency {
	
	private PayrollSchedule payrollSchedule;
	
	public PayPeriodFrequency(PayrollSchedule payrollSchedule) {
		this.payrollSchedule = payrollSchedule;
	}
	
	public PayrollSchedule getPayrollSchedule() {
		return payrollSchedule;
	}

	@Override
	protected void setNextBeginDate(Calendar calendar) {
		if (payrollSchedule == null || payrollSchedule.isNone()) {
			// if none we don't want to generate so just set the date far out
			calendar.roll(Calendar.YEAR, false);
			return;
		}
		
		LocalDate fromDate = LocalDate.from(calendar.toInstant().atZone(ZoneId.systemDefault()));
		payrollSchedule.setCurrentDate(fromDate);
		LocalDate nextDate = payrollSchedule.getNextPayrollDate();
		nextDate = nextDate.plus(Period.ofDays(1)); // it's asking for begin date, which is the day after the payroll date
		calendar.set(nextDate.getYear(), nextDate.getMonthValue()-1, nextDate.getDayOfMonth(), 0, 0, 0); // calendar month is zero indexed...
	}
	
	public String toString() {
		return String.format("PayPeriodFrequency[%s]", payrollSchedule);
	}
}
