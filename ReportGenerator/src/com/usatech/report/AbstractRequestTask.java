package com.usatech.report;

import java.lang.management.ManagementFactory;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.Initializer;
import simple.results.Results;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.NotifyingBlockingThreadPoolExecutor;

import com.usatech.layers.common.InteractionUtils;

/**
 * Base class - common code
 * 
 * @author bkrug
 * 
 */
public abstract class AbstractRequestTask implements SelfProcessor {
	private static final Log log = Log.getLog();
	protected static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));
	
	protected ReportRequestFactory reportRequestFactory;
	protected int poolSize = 5;
	protected long lockUpdateInterval = 900000;
	protected NotifyingBlockingThreadPoolExecutor threadPoolExecutor;
	protected final Initializer<ServiceException> initializer = new Initializer<ServiceException>() {
		protected void doInitialize() throws ServiceException {
			threadPoolExecutor = new NotifyingBlockingThreadPoolExecutor(getPoolSize(), getPoolSize() * 2, new CustomThreadFactory(Thread.currentThread().getName() + '-', true));
			initializeProcessor();
		}

		protected void doReset() {
			ThreadPoolExecutor tpe = threadPoolExecutor;
			if(tpe != null)
				tpe.shutdown();
			resetProcessor();
		}
	};
	
	@Override
	public void process() {
		try {
			initializer.initialize();
		} catch(InterruptedException e) {
			log.error("Error initializing report request.", e);
			return;
		} catch(ServiceException e) {
			log.error("Error initializing report request.", e);
			return;
		} catch(RuntimeException e) {
			log.error("Error initializing report request.", e);
			return;
		}
		processInternal();
	}
	
	/**
	 * @throws ServiceException
	 */
	protected void initializeProcessor() throws ServiceException {
		// Hook for sub-classes
	}

	protected void resetProcessor() {
		// Hook for sub-classes
	}

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public long getLockUpdateInterval() {
		return lockUpdateInterval;
	}

	public void setLockUpdateInterval(long lockUpdateInterval) {
		this.lockUpdateInterval = lockUpdateInterval;
	}

	protected void processInternal() {
		long startTs = System.currentTimeMillis();
		long totalItemCount = 0;
		final Map<String, Object> params = new HashMap<String, Object>();
		final AtomicBoolean locked = new AtomicBoolean(true);
		final ReentrantLock guard = new ReentrantLock();
		final String processCd = getProcessCd();
		final String processDesc = getProcessDesc();
		try {
			if(!InteractionUtils.lockProcess(processCd, LOCK_PROCESS_TOKEN_CD, params)) {
				log.info(processDesc + " is already locked by instance " + params.get("lockedBy"));
				return;
			}
			final Timer timer = new Timer(processCd + "_Timer");
			try {
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						guard.lock();
						try {
							if(locked.get() && !InteractionUtils.lockProcess(processCd, LOCK_PROCESS_TOKEN_CD, params)) {
								log.info(processDesc + " is already locked by instance " + params.get("lockedBy"));
								threadPoolExecutor.getQueue().clear();
								locked.set(false);
								timer.cancel();
							}
						} catch(ConvertException e) {
							log.warn("Error while renewing lock on " + processDesc, e);
							locked.set(false);
							timer.cancel();
						} catch(DataLayerException e) {
							log.warn("Error while renewing lock on " + processDesc, e);
							locked.set(false);
							timer.cancel();
						} catch(SQLException e) {
							log.warn("Error while renewing lock on " + processDesc, e);
							locked.set(false);
							timer.cancel();
						} catch(RuntimeException e) {
							log.warn("Error while renewing lock on " + processDesc, e);
							locked.set(false);
							timer.cancel();
						} catch(Error e) {
							log.warn("Error while renewing lock on " + processDesc, e);
							locked.set(false);
							timer.cancel();
						} finally {
							guard.unlock();
						}
					}
				}, getLockUpdateInterval(), getLockUpdateInterval());
				// Don't store referrences to the submitted tasks so that when they complete they may be gc'ed.
				Results results = DataLayerMgr.executeQuery(getListCallId(), getListCallParams());
				try {
					while(locked.get() && results.next()) {
						totalItemCount++;
						HandleItemTask item = new HandleItemTask(locked);
						results.fillBean(item);
						threadPoolExecutor.submit(item);
					}
				} finally {
					results.close();
					if(totalItemCount > 0)
						threadPoolExecutor.await();
				}
			} finally {
				timer.cancel();
			}
		} catch(Exception e) {
			log.error("Error processing " + processDesc, e);
		} finally {
			guard.lock();
			try {
				locked.set(false);
				InteractionUtils.unlockProcess(processCd, LOCK_PROCESS_TOKEN_CD, params);
			} catch(Exception e) {
				log.error("Error unlocking " + processCd, e);
			} finally {
				guard.unlock();
			}
			if(totalItemCount > 0)
				log.info("Processed " + totalItemCount + " " + processDesc + " items in " + (System.currentTimeMillis() - startTs) + " ms");
		}
	}

	protected abstract String getProcessCd();

	protected abstract String getProcessDesc();

	protected abstract String getListCallId();

	protected abstract Object getListCallParams();

	protected abstract void handleItem(Map<String, Object> itemParams);

	protected class HandleItemTask extends HashMap<String, Object> implements Runnable {
		private static final long serialVersionUID = -7105131360671970156L;
		protected final AtomicBoolean locked;

		public HandleItemTask(AtomicBoolean locked) {
			super();
			this.locked = locked;
		}

		public void run() {
			if(!locked.get())
				return;
			handleItem(this);
		}
	}

	public ReportRequestFactory getReportRequestFactory() {
		return reportRequestFactory;
	}

	public void setReportRequestFactory(ReportRequestFactory reportRequestFactory) {
		this.reportRequestFactory = reportRequestFactory;
	}
}
