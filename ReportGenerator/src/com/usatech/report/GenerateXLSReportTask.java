package com.usatech.report;

import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.WordUtils;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.sql.SQLType;

public class GenerateXLSReportTask extends AbstractGenerateReportTask {
	private static final Log log = Log.getLog();

	public GenerateXLSReportTask() {
		super(Generator.XLS);
	}
	/**
	 * @see com.usatech.report.AbstractGenerateReportTask#getContentType()
	 */
	@Override
	protected String getContentType() {
		return generator.getContentType();
	}
	/**
	 * @see com.usatech.report.AbstractGenerateReportTask#generateReport(int,
	 *      java.util.Map, java.io.OutputStream)
	 */
	@Override
	protected void generateReport(String title, int userId, Map<String, Object> params, OutputStream out, StandbyAllowance standbyAllowance) throws Exception {
		generateXLSReport(params, new PrintStream(out, true), title, standbyAllowance);
	}

	protected int generateXLSReport(Map<String, Object> params, PrintStream out, String title, StandbyAllowance standbyAllowance) throws SQLException, IllegalArgumentException, DataLayerException, ConvertException {
		int rows = 0;
		String query = ConvertUtils.getStringSafely(params.get("query"));
		if(query == null || query.trim().length() == 0)
			throw new IllegalArgumentException("Necessary parameter, 'query', not found");
		params.remove("query"); // no need to do anything with this below
		String[] parameterNames = ConvertUtils.convertSafely(String[].class, params.get("paramNames"), null);
		SQLType[] parameterTypes = ConvertUtils.convertSafely(SQLType[].class, params.get("paramTypes"), null);
		Object[] parameterValues;
		if(parameterNames == null || parameterNames.length == 0) {
			int n = 0;
			if(parameterTypes != null)
				n = parameterTypes.length;
			while(params.containsKey("params." + (n + 1)))
				n++;
			parameterValues = new Object[n];
			for(int i = 0; i < n; i++)
				parameterValues[i] = params.get("params." + (i + 1));
		} else {
			int n = parameterNames.length;
			if(parameterTypes != null && parameterTypes.length > n)
				n = parameterTypes.length;
			parameterValues = new Object[n];
			for(int i = 0; i < n; i++) {
				if(i < parameterNames.length && parameterNames[i] != null)
					parameterValues[i] = params.get("params." + parameterNames[i]);
			}
		}
		String dsn;
		if(query.indexOf("RDW.") > 0)
			dsn = "MAIN";
		else {
			switch(standbyAllowance) {
				case ALLOW:
				case WAIT_ON_REQUEST:
					dsn = "REPORT_STANDBY";
					break;
				default:
					dsn = "REPORT";
			}
		}
		log.info("Executing SQL '" + query + "' on " + dsn);

		Results results;
		try {
			results = DataLayerMgr.executeSQLx(dsn, query, parameterValues, parameterTypes);
		} catch(SQLException sqle) {
			log.warn("Exception while executing CSV Report Query: " + query);
			throw sqle;
		} catch(DataLayerException e) {
			log.warn("Exception while executing CSV Report Query: " + query);
			throw e;
		}

		boolean header = ConvertUtils.getBoolean(params.get("header"), false);
		Map<Integer, String> colStyles = new HashMap<Integer, String>();
		for(Map.Entry<String, Object> entry : params.entrySet()) {
			if(entry.getKey().startsWith("XLSCSS@")) {
				if(entry.getValue() != null) {
					String cssClass = ConvertUtils.getStringSafely(entry.getValue());
					if(cssClass != null) {
						cssClass = cssClass.toLowerCase();
					}
					String cn = entry.getKey().substring("XLSCSS@".length());
					int col = -1;
					try {
						// try to get the column number directly
						col = Integer.parseInt(cn);
					} catch(Exception e) {
						try {
							// get the column number by name
							col = results.getColumnIndex(cn);
						} catch(Exception e1) {
						}
					}
					// if we were able to resovle a column number add to hash
					if(col != -1) {
						colStyles.put(new Integer(col), cssClass);
					}
				}
			} else if(entry.getKey().startsWith("FORMAT@")) {
				if(entry.getValue() != null) {
					String cn = entry.getKey().substring("FORMAT@".length());
					try {
						int col = Integer.parseInt(cn);
						results.setFormat(col, ConvertUtils.getStringSafely(entry.getValue()));
					} catch(Exception e) {
						try {
							results.setFormat(cn, ConvertUtils.getStringSafely(entry.getValue()));
						} catch(IllegalArgumentException e1) {
							log.warn("Column '" + cn + "' does not exist in this resultset", e1);
						}
					}
				}
			}
		}
		// open the document and head
		out.println("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\">");
		out.println("<head>");

		// we need to pull in the XLS html header
		generateXLSReportHeader(out, "");

		// close the head, open the body, open the table
		out.println("</head>");
		out.println("<body>");
		out.println("<table border=0 cellpadding=0 cellspacing=0>");

		int columns = results.getColumnCount();
		// boolean[] quote = new boolean[columns];
		// for(int i = 1; i <= columns; i++) quote[i-1] =
		// !Number.class.isAssignableFrom(results.getColumnClass(i));

		// do the cols (set the datatype / css class)
		for(int i = 1; i <= columns; i++) {
			String colStyle = colStyles.get(new Integer(i));
			out.print("<col class=");
			if(colStyle != null) {
				out.print(colStyle);
			} else {
				out.print("gen");
			}
			// this will have to be transalated to a xls css class
			// out.print(getCssClass(results.getColumnType(i),
			// results.getColumnScale(i)));
			out.println(">");
		}

		// here we want to format the report header and column names
		if(header) {
			// first do a colspan for the title row
			out.print("<tr><td class=head colspan=");
			out.print(columns);
			out.print(">");
			out.print(title);
			out.println("</td></tr>");
			// then do the header row
			out.print("<tr>");
			for(int i = 1; i <= columns; i++) {
				out.print("<td class=head>");
				// format the report column headers to use Title Case (cleaner
				// appearance)
				out.print(WordUtils.capitalizeFully(results.getColumnName(i).replace('_', ' ')));
				out.print("</td>");
			}
			out.println("</tr>");
		}
		// this is the actual data
		while(results.next()) {
			out.print("<tr>");
			for(int i = 1; i <= columns; i++) {
				out.print("<td>");
				out.print(results.getFormattedValue(i));
				out.print("</td>");
			}
			out.println("</tr>");
			rows++;
		}

		// display a formatted error message for no data
		if(rows == 0) {
			out.print("<tr><td class=text colspan=");
			out.print(columns);
			out.print("><b>");
			out.print(Messages.getInstance().getMessage("NO_ROWS_FOR_XLS"));
			out.println("</b></td></tr>");
		}

		// close the table, close the body, close the document
		out.println("</table>");
		out.println("</body>");
		out.println("</html>");

		return rows;
	}

	protected void generateXLSReportHeader(PrintStream out, String sheetTitle) {
		/*
		 * <style> <!-- table {mso-displayed-decimal-separator:"\.";
		 * mso-displayed-thousand-separator:"\,";} @page {margin:1.0in .75in
		 * 1.0in .75in; mso-header-margin:.5in; mso-footer-margin:.5in;} tr
		 * {mso-height-source:auto;} col {mso-width-source:auto;} br
		 * {mso-data-placement:same-cell;} .base {text-align:general;
		 * vertical-align:bottom; white-space:nowrap; color:windowtext;
		 * font-size:10.0pt; font-weight:400; font-style:normal;
		 * text-decoration:none; font-family:Arial;
		 * mso-generic-font-family:auto; mso-font-charset:0; border:none;
		 * mso-protection:locked visible; mso-style-name:Normal;
		 * mso-style-id:0;} td {mso-style-parent:base; padding-top:1px;
		 * padding-right:1px; padding-left:1px; mso-ignore:padding;
		 * font-size:10.0pt; font-family:Arial; border:.5pt solid;} .gen
		 * {mso-style-parent:base; mso-number-format:General;} .std
		 * {mso-style-parent:base; mso-number-format:Standard;} .int
		 * {mso-style-parent:base; mso-number-format:0;} .float
		 * {mso-style-parent:base; mso-number-format:"0\.0000";} .curr
		 * {mso-style-parent:base;
		 * mso-number-format:"\0022$\0022\#\,\#\#0\.00";} .acc
		 * {mso-style-parent:base; mso-number-format:"_\(\0022$\0022*
		 * \#\,\#\#0\.00_\)\;_\(\0022$\0022*
		 * \\\(\#\,\#\#0\.00\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";}
		 * .date {mso-style-parent:base; mso-number-format:"m\/d\/yyyy\;\@";}
		 * .datetime {mso-style-parent:base; mso-number-format:"m\/d\/yy\\
		 * h\:mm\\ AM\/PM\;\@";} .time {mso-style-parent:base;
		 * mso-number-format:"h\:mm\:ss\\ AM\/PM\;\@";} .text
		 * {mso-style-parent:base; mso-number-format:"\@";} .head
		 * {mso-style-parent:base; mso-number-format:"\@"; font-weight:700;
		 * background:#CCFFFF; text-align:center;} --> </style> <!--[if gte mso
		 * 9]><xml> <x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>
		 * <x:Name>Sheet1</x:Name>
		 * <x:WorksheetOptions><x:Selected/></x:WorksheetOptions>
		 * </x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>
		 * </xml><![endif]-->
		 */
		out.println("<style>");
		out.println("<!--");
		out.println("table {mso-displayed-decimal-separator:\"\\.\"; mso-displayed-thousand-separator:\"\\,\";}");
		out.println("@page {margin:1.0in .75in 1.0in .75in; mso-header-margin:.5in; mso-footer-margin:.5in;}");
		out.println("tr {mso-height-source:auto;}");
		out.println("col {mso-width-source:auto;}");
		out.println("br {mso-data-placement:same-cell;}");
		out
				.println(".base {text-align:general; vertical-align:bottom; white-space:nowrap; color:windowtext; font-size:10.0pt; font-weight:400; font-style:normal; text-decoration:none; font-family:Arial; mso-generic-font-family:auto; mso-font-charset:0; border:none; mso-protection:locked visible; mso-style-name:Normal; mso-style-id:0;}");
		out.println("td {mso-style-parent:base; padding-top:1px; padding-right:1px; padding-left:1px; mso-ignore:padding; font-size:10.0pt; font-family:Arial; border:.5pt solid;}");
		out.println(".gen {mso-style-parent:base; mso-number-format:General;}");
		out.println(".std {mso-style-parent:base; mso-number-format:Standard;}");
		out.println(".int {mso-style-parent:base; mso-number-format:0;}");
		out.println(".float {mso-style-parent:base; mso-number-format:\"0\\.0000\";}");
		out.println(".curr {mso-style-parent:base; mso-number-format:\"\\0022$\\0022\\#\\,\\#\\#0\\.00\";}");
		out
				.println(".acc {mso-style-parent:base; mso-number-format:\"_\\(\\0022$\\0022* \\#\\,\\#\\#0\\.00_\\)\\;_\\(\\0022$\\0022* \\\\\\(\\#\\,\\#\\#0\\.00\\\\\\)\\;_\\(\\0022$\\0022* \\0022-\\0022??_\\)\\;_\\(\\@_\\)\";}");
		out.println(".date {mso-style-parent:base; mso-number-format:\"m\\/d\\/yyyy\\;\\@\";}");
		out.println(".datetime {mso-style-parent:base; mso-number-format:\"m\\/d\\/yy\\\\ h\\:mm\\\\ AM\\/PM\\;\\@\";}");
		out.println(".time {mso-style-parent:base; mso-number-format:\"h\\:mm\\:ss\\\\ AM\\/PM\\;\\@\";}");
		out.println(".text {mso-style-parent:base; mso-number-format:\"\\@\";}");
		out.println(".head {mso-style-parent:base; mso-number-format:\"\\@\"; font-weight:700; background:#CCFFFF; text-align:center;}");
		out.println("-->");
		out.println("</style>");
		out.println("<!--[if gte mso 9]><xml>");
		out.println("<x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>");
		if(sheetTitle.equals("")) {
			out.println("<x:Name>Sheet1</x:Name>");
		} else {
			out.println("<x:Name>" + sheetTitle + "</x:Name>");
		}
		out.println("<x:WorksheetOptions><x:Selected/></x:WorksheetOptions>");
		out.println("</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>");
		out.println("</xml><![endif]-->");
	}
}
