package com.usatech.report.build;

import java.sql.SQLException;
import java.sql.Types;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.FilterItem;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Param;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardFilterGroup;
import simple.falcon.run.FolioUpdateUtils;
import simple.falcon.servlet.FalconServlet;
import simple.falcon.servlet.FolioStepHelper;
import simple.servlet.InputForm;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.util.CollectionUtils;

/**
 * Builds Activity Analysis Detail report
 *
 * @author bkrug
 *
 */
public class BuildActivityDetailExtFolio extends BuildActivityExtFolio {

	public BuildActivityDetailExtFolio() {
		super();
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return true;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			folio.setTitle(ConvertUtils.getFormat("LITERAL", ConvertUtils.getStringSafely(form.get("reportTitle"), "Activity Analysis Detail")));
			int[] sortBy = ConvertUtils.convertRequired(int[].class, form.get("sortBy"));
			boolean hasPreference = false;
			Set<String> addKeepParamNames = new HashSet<String>();
			form.set("additionalKeepParamNames", addKeepParamNames);
			addKeepParamNames.add("params.tranType");
			ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
			StandardDesignEngine designEngine = (StandardDesignEngine) executeEngine.getDesignEngine();
			boolean filtered = false;
			Collection<?> rangeSelection = null;
			boolean itemGranularity = false;
			String tranTypeParamName = "params.tranType";
			for(int i = 0; i < sortBy.length; i++) {
				if(sortBy[i] == 0)
					continue;
				if(i > 0 && CollectionUtils.iterativeSearch(sortBy, sortBy[i], 0, i) >= 0)
					continue;
				String paramName = getLinkFilterLabel(sortBy[i]);
				Object tmp = form.get(paramName);
				if(tmp != null) {
					FilterItem filter = getLinkActionFilter(sortBy[i], designEngine);
					SQLType[] sqlTypes = filter.getSqlTypes();
					Collection<?> values = ConvertUtils.asCollection(tmp, ConvertUtils.convertToWrapperClass(SQLTypeUtils.getJavaType(sqlTypes[0])));
					if(values != null && !values.isEmpty()) {
						addKeepParamNames.add(paramName);
						if(filter.getOperator().getOperatorId() == 1) { // Use arrayOperator
							filter.setOperatorById(arrayOperatorId);
							// NOTE: It appears that Oracle has an issue with MEMBER OF DATE_TABLE (in that it returns no rows), so use IN operator for those cases
							if(arrayOperatorId == 21)
								switch(filter.getField().getSortSqlType().getTypeCode()) {
									case Types.DATE:
									case Types.TIMESTAMP:
										filter.setOperatorById(27);
										break;
								}
							filter.setValues(new Object[] { values });
							FolioStepHelper.addFilter(folio, filter, "AND");
						} else {
							Param[] params = filter.getOperatorParams();
							if(params.length > 0) {
								for(Param p : params)
									p.setName(null);
								StandardFilterGroup group = new StandardFilterGroup();
								group.setSeparator(StandardFilterGroup.SEPARATOR_OR);
								for(Iterator<?> iter = values.iterator(); iter.hasNext();) {
									Object value = iter.next();
									FilterItem fi;
									if(iter.hasNext())
										fi = filter.copy(null);
									else
										fi = filter;
									for(Param p : fi.getOperatorParams())
										p.setValue(value);
									group.addFilter(fi);
								}
								FolioUpdateUtils.addFilter(folio, group, StandardFilterGroup.SEPARATOR_AND);

							}
						}
						switch(sortBy[i]) {
							case 3:
							case 4:
							case 5:
							case 6:
								rangeSelection = values;
								break;
							case 7:
								tranTypeParamName = paramName;
								break;
							default:
								filtered = true;
						}
					}
				}
				switch(sortBy[i]) {
					case 2:
						hasPreference = true;
						break;
					case 11:
						itemGranularity = true;
						break;
					case 7: case 12:
						continue;
					case 13:
						adjustSortByPillars(addPillars(folio, "regionDetail"), 4, null);
						continue;
				}
				addSortByPillar(form, folio, sortBy[i], 4, null);
			}
			if(!hasPreference) {
				addSortByPillar(form, folio, 2, 4, null);
			}
			adjustSortByPillars(addPillars(folio, "tranAndSettleType"), 3, null);
			adjustSortByPillars(addPillars(folio, "device"), 2, null);
			adjustSortByPillars(addPillars(folio, "refNbr"), 2, null);
			adjustSortByPillars(addPillars(folio, "date"), 2, null);
			adjustSortByPillars(addPillars(folio, "cardType"), 2, null);
			adjustSortByPillars(addPillars(folio, "card"), 2, null);
			adjustShowValuePillars(addPillars(folio, itemGranularity ? "itemAmount" : "amount"), 0, false, null);
			adjustSortByPillars(addPillars(folio, "apCode"), 2, null);
			adjustSortByPillars(addPillars(folio, "items"), 2, null);
			adjustShowValuePillars(addPillars(folio, itemGranularity ? "itemQuantity" : "quantity"), 0, false, null);
			adjustSortByPillars(addPillars(folio, "deviceTranId"), 2, null);
			adjustSortByPillars(addPillars(folio, "applyToCardId"), 2, null);

			// add currency filter
			if(addFilter(form, folio, designEngine.getField(fields.get("currencyCd")), designEngine, addKeepParamNames))
				filtered = true;
			// logic for selecting customer, region and terminal
			if(addFiltersForLocation(form, folio, null))
				filtered = true;
						
			String rangeType = form.getString("rangeType", true);
			RangeType rt = rangeTypes.get(rangeType);
			adjustSubtitleForRangeType(form, folio, rt, filtered, tranTypeParamName, rangeSelection);
			addQueryOptimizerHint(folio, rangeType);
			Param tranTypeParam = addFilter(form, folio, "tranType").getOperatorParams()[0];
			tranTypeParam.setValue(ConvertUtils.getString(form.get(tranTypeParamName), false));
			tranTypeParam.setName(null);
			
			if(rangeSelection == null || rangeSelection.isEmpty())
				addFilter(form, folio, "day");
			// fix up tranType
			// fixArrayParam(form);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}
}
