/**
 *
 */
package com.usatech.report.build;

import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.text.Format;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.xml.sax.SAXException;

import simple.app.AppPropertiesConfiguration;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.falcon.engine.*;
import simple.falcon.engine.Pillar.PillarType;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardFilterGroup;
import simple.falcon.engine.standard.StandardFilterItem;
import simple.falcon.engine.util.EngineUtils;
import simple.falcon.servlet.FalconServlet;
import simple.falcon.servlet.FolioStepHelper;
import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.lang.Holder;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.sql.SQLTypeUtils;
import simple.text.LiteralFormat;
import simple.text.MessageFormat;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveComparator;
import simple.util.CaseInsensitiveHashMap;
import simple.xml.ObjectBuilder;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractBuildActivityFolio {
	private static final Log log = Log.getLog();
	protected static Map<String, ObjectBuilder> pillars;
	protected static Map<String, ObjectBuilder> filters;
	protected static Map<String, Long> fields;
	protected static int arrayOperatorId;

	protected static final class LazyHolder {
		protected static final ConfigSource configSource = getConfigSource();
	}

	protected static final String preferencePillarName = "preference";
	protected static boolean usalive_1_5_compatible = true;
	protected static String configFile = "com/usatech/report/build/BuildReportPillars.properties";
	protected int maxRowPerPage = 500;

	protected static class RangeType {
		public final String pillarName;
		public final String displayDateFormat;
		public final String filterName;
		public final String parameterDateFormat;
		public final String selectionFormat;

		public RangeType(String pillarName, String filterName, String dateFormat, String parameterDateFormat,
				String selectionFormat) {
			this.pillarName = pillarName;
			this.displayDateFormat = dateFormat;
			this.filterName = filterName;
			this.parameterDateFormat = parameterDateFormat;
			this.selectionFormat = selectionFormat;
		}
	}

	protected static final Map<String, RangeType> rangeTypes = new HashMap<String, RangeType>();
	protected static final Map<Integer, String> preferenceToPillarMap = new HashMap<Integer, String>();

	static {
		preferenceToPillarMap.put(1, "device");
		preferenceToPillarMap.put(2, "location");
		preferenceToPillarMap.put(3, "asset");
		preferenceToPillarMap.put(4, "model");
		preferenceToPillarMap.put(5, "terminal");
		preferenceToPillarMap.put(6, "customer");
		preferenceToPillarMap.put(7, "client");
		preferenceToPillarMap.put(8, "specifics");

		rangeTypes.put("DAY", new RangeType("day", "day", "MMMM dd, yyyy", "MM-dd-yyyy",
				"simple.text.DateFromDayFormat:MMMM dd, yyyy"));
		rangeTypes.put("DAYROLLUP", new RangeType("dayRollup", "dayRollup", "MMMM dd, yyyy", "MM-dd-yyyy",
				"simple.text.DateFromDayFormat:MMMM dd, yyyy"));
		rangeTypes.put("WEEK", new RangeType("week", "week", "MMMM dd, yyyy", "MM-dd-yyyy", "DATE:MMMM dd, yyyy"));
		rangeTypes.put("WEEKROLLUP",
				new RangeType("weekRollup", "weekRollup", "MMMM dd, yyyy", "MM-dd-yyyy", "DATE:MMMM dd, yyyy"));
		rangeTypes.put("MONTH", new RangeType("month", "month", "MMMM, yyyy", "MM-yyyy", "DATE:MMMM, yyyy"));
		rangeTypes.put("MONTHROLLUP",
				new RangeType("monthRollup", "monthRollup", "MMMM, yyyy", "MM-yyyy", "DATE:MMMM, yyyy"));
		rangeTypes.put("FILL",
				new RangeType("fill", "fill", "MMMM dd, yyyy HH:mm", "MM-dd-yyyy HH:mm:ss", "NUMBER:'Fill #'0"));
		rangeTypes.put("ALL", new RangeType("", "all", "MMMM dd, yyyy HH:mm", "MM-dd-yyyy HH:mm", "LITERAL:"));
		rangeTypes.put("ALLROLLUP",
				new RangeType("", "allRollup", "MMMM dd, yyyy HH:mm", "MM-dd-yyyy HH:mm", "LITERAL:"));
	}

	protected static final String[] DEFAULT_PREFERENCE_PILLARS = { preferenceToPillarMap.get(1),
			preferenceToPillarMap.get(2) };

	protected static Map<String, ObjectBuilder> loadBuilderMap(Configuration subset) throws LoadingException {
		Map<String, ObjectBuilder> tmp = new HashMap<String, ObjectBuilder>();
		for (Iterator<?> iter = subset.getKeys(); iter.hasNext();) {
			String key = (String) iter.next();
			String value = subset.getString(key);
			try {
				tmp.put(key, new ObjectBuilder(new StringReader(value)));
			} catch (IOException e) {
				throw new LoadingException("Could not parse pillar xml '" + value + "'", e);
			} catch (SAXException e) {
				throw new LoadingException("Could not parse pillar xml '" + value + "'", e);
			} catch (ParserConfigurationException e) {
				throw new LoadingException("Could not parse pillar xml '" + value + "'", e);
			}
		}
		return tmp;
	}

	protected static <E> Map<String, E> loadValues(Configuration subset, Class<E> valueType) throws LoadingException {
		Map<String, E> tmp = new HashMap<String, E>();
		for (Iterator<?> iter = subset.getKeys(); iter.hasNext();) {
			String key = (String) iter.next();
			String value = subset.getString(key);
			try {
				tmp.put(key, ConvertUtils.convert(valueType, value));
			} catch (ConvertException e) {
				throw new LoadingException("Could not convert '" + value + "' for '" + key + "'", e);
			}
		}
		return tmp;
	}

	protected static ConfigSource getConfigSource() {
		try {
			ConfigSource src = ConfigSource.createConfigSource(getConfigFile());
			src.registerLoader(new Loader() {
				public void load(ConfigSource src) throws LoadingException {
					AppPropertiesConfiguration pc = new AppPropertiesConfiguration();
					try {
						pc.load(src.getInputStream());
					} catch (ConfigurationException e) {
						throw new LoadingException("Could not load " + src, e);
					} catch (IOException e) {
						throw new LoadingException("Could not load " + src, e);
					}
					pillars = loadBuilderMap(pc.subset("pillars"));
					filters = loadBuilderMap(pc.subset("filters"));
					fields = loadValues(pc.subset("fields"), Long.class);
					arrayOperatorId = ConvertUtils.getIntSafely(pc.getString("arrayOperatorId"), 21);
				}
			}, 15000, 0);
			return src;
		} catch (IOException e) {
			log.warn("Could not configure pillars for Build a Report", e);
			return null;
		}
	}

	protected Map<String, String> queryOptimizerHints = new CaseInsensitiveHashMap<String>();
	public static String ACTIVITY_QUERY_OPTIMIZER_HINT = "INDEX(ar IX_AR_TERMINAL_TRAN_DATE)";

	protected AbstractBuildActivityFolio() {
		queryOptimizerHints.put("DAY", ACTIVITY_QUERY_OPTIMIZER_HINT);
		queryOptimizerHints.put("WEEK", ACTIVITY_QUERY_OPTIMIZER_HINT);
		queryOptimizerHints.put("MONTH", ACTIVITY_QUERY_OPTIMIZER_HINT);
		queryOptimizerHints.put("ALL", ACTIVITY_QUERY_OPTIMIZER_HINT);
		queryOptimizerHints.put("FILL", "INDEX(ar IX_AR_TERMINAL_FILL_DATE)");
	}

	public abstract int getDefaultOutputTypeId();

	public abstract boolean keepAdditionalParamNames();

	public abstract void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException;

	public Folio getFolio(ExecuteEngine executeEngine, InputForm form, Scene scene) throws ServletException {
		try {
			LazyHolder.configSource.reload();
		} catch (LoadingException e) {
			throw new ServletException("Could not load configuration", e);
		}
		try {
			Folio folio = executeEngine.getDesignEngine().newFolio();

			int maxRowPerPageFromForm = ConvertUtils.getIntSafely(form.getAttribute("max-rows-per-page"), -1);
			if (maxRowPerPageFromForm > -1) {
				folio.setDirective("max-rows-per-page", String.valueOf(maxRowPerPageFromForm));
			} else {
				folio.setDirective("max-rows-per-page", String.valueOf(maxRowPerPage));
			}
			return folio;
		} catch (DesignException e) {
			throw new ServletException("Could not create new folio", e);
		}
	}

	protected void fixArrayParam(InputForm form) throws ServletException {
		try {
			form.set("params.tranType", ConvertUtils.getString(form.get("params.tranType"), false));
		} catch (ConvertException e) {
			throw new ServletException(e);
		}
	}

	protected Pillar[] addSortByPillar(InputForm form, Folio folio, int sortByIndex, int groupingLevel,
			PillarType pillarTypeOverride) throws ConvertException, DesignException, SQLException, DataLayerException {
		switch (sortByIndex) {
		case 1:
			// TODO: check privilege
			return adjustSortByPillars(addPillars(folio, "customer"), groupingLevel, pillarTypeOverride);
		case 2:
			String[] pillarNames = getPreferencePillars(form);
			List<Pillar> pillarList = new ArrayList<Pillar>(pillarNames.length + 2);
			for (int i = 0; i < pillarNames.length; i++) {
				Pillar[] pillars = adjustSortByPillars(addPillars(folio, pillarNames[i]), groupingLevel,
						pillarTypeOverride);
				for (Pillar pillar : pillars)
					pillarList.add(pillar);
			}
			return pillarList.toArray(new Pillar[pillarList.size()]);
		case 3:
			return adjustSortByPillars(addPillars(folio, "fill"), groupingLevel, pillarTypeOverride);
		case 4:
			return adjustSortByPillars(addPillars(folio, "month" + form.getStringSafely("rollup", "")), groupingLevel,
					pillarTypeOverride);
		case 5:
			return adjustSortByPillars(addPillars(folio, "week" + form.getStringSafely("rollup", "")), groupingLevel,
					pillarTypeOverride);
		case 6:
			return adjustSortByPillars(addPillars(folio, "day" + form.getStringSafely("rollup", "")), groupingLevel,
					pillarTypeOverride);
		case 7:
			return adjustSortByPillars(addPillars(folio, "tranType" + form.getStringSafely("rollup", "")),
					groupingLevel, pillarTypeOverride);
		case 8:
			return adjustSortByPillars(addPillars(folio, "card"), groupingLevel, pillarTypeOverride);
		case 9:
			return adjustSortByPillars(addPillars(folio, "hourOfDay"), groupingLevel, pillarTypeOverride);
		case 10:
			return adjustSortByPillars(addPillars(folio, "dayOfWeek" + form.getStringSafely("rollup", "")),
					groupingLevel, pillarTypeOverride);
		case 11:
			return adjustSortByPillars(addPillars(folio, "item"), groupingLevel, pillarTypeOverride);
		case 12:
			return adjustSortByPillars(addPillars(folio, "settleState"), groupingLevel, pillarTypeOverride);
		case 13:
			return adjustSortByPillars(addPillars(folio, "region"), groupingLevel, pillarTypeOverride);
		case -13:
			return adjustSortByPillars(addPillars(folio, "parentRegion"), groupingLevel, pillarTypeOverride);
		case 14:
			return adjustSortByPillars(addPillars(folio, "client"), groupingLevel, pillarTypeOverride);
		default:
			throw new ConvertException("Could not convert to sortBy index", String.class, sortByIndex);
		}
	}

	protected Field getLinkFilterField(String pillarName, DesignEngine designEngine) throws DesignException {
		ObjectBuilder pillarsBuilder = pillars.get(pillarName);
		if (pillarsBuilder == null)
			throw new DesignException("Pillar '" + pillarName + "' is not configured");
		if (pillarsBuilder.getSubNodes() == null || pillarsBuilder.getSubNodes().length == 0)
			return null;
		ObjectBuilder[] fieldBuilders;
		if (pillarsBuilder.getTag().equalsIgnoreCase("pillars"))
			fieldBuilders = pillarsBuilder.getSubNodes()[0].getSubNodes("field");
		else
			fieldBuilders = pillarsBuilder.getSubNodes("field");
		if (fieldBuilders == null || fieldBuilders.length == 0)
			return null;
		return designEngine.getField(fieldBuilders[0].get("fieldId", Long.class));
	}

	protected Field getLinkFilterField(int sortByIndex, DesignEngine designEngine)
			throws ConvertException, DesignException {
		switch (sortByIndex) {
		case 1:
			// TODO: check privilege
			return getLinkFilterField("customer", designEngine);
		case 2:
			return getLinkFilterField("locationId", designEngine); // designEngine.getField(fields.get("terminalId"));
		case 3:
			return getLinkFilterField("fill", designEngine);
		case 4:
			return getLinkFilterField("month", designEngine);
		case 5:
			return getLinkFilterField("week", designEngine);
		case 6:
			return getLinkFilterField("day", designEngine);
		case 7:
			return getLinkFilterField("tranType", designEngine);
		case 8:
			return getLinkFilterField("card", designEngine);
		case 9:
			return getLinkFilterField("hourOfDay", designEngine);
		case 10:
			return getLinkFilterField("dayOfWeek", designEngine);
		case 11:
			return getLinkFilterField("item", designEngine);
		case 12:
			return getLinkFilterField("settleState", designEngine);
		case 13:
			return getLinkFilterField("region", designEngine);
		case -13:
			return getLinkFilterField("parentRegion", designEngine);
		case 14:
			return getLinkFilterField("client", designEngine);
		default:
			throw new ConvertException("Could not convert to sortBy index", String.class, sortByIndex);
		}
	}

	protected String getLinkFilterLabel(int sortByIndex) throws ConvertException {
		switch (sortByIndex) {
		case 1:
			return "customer";
		case 2:
			return "terminal";
		case 3:
			return "fill";
		case 4:
			return "month";
		case 5:
			return "week";
		case 6:
			return "day";
		case 7:
			return "tranTypeId";
		case 8:
			return "card";
		case 9:
			return "hourOfDay";
		case 10:
			return "dayOfWeek";
		case 11:
			return "item";
		case 12:
			return "settleState";
		case 13:
			return "region";
		case -13:
			return "parentRegion";
		case 14:
			return "client";
		default:
			throw new ConvertException("Could not convert to sortBy index", String.class, sortByIndex);
		}
	}

	protected Pillar[] addShowValuePillars(InputForm form, Folio folio, int showValueIndex, boolean itemGranularity,
			boolean showPercent, PillarType pillarTypeOverride) throws ConvertException, DesignException {
		switch (showValueIndex) {
		case 1:
			if (itemGranularity)
				throw new DesignException(
						"Sale Amounts cannot be meaningfully displayed when report is grouped by Item");
			return adjustShowValuePillars(addPillars(folio, "amount" + form.getStringSafely("rollup", "")), 0,
					showPercent, pillarTypeOverride);
		case 2:
			return adjustShowValuePillars(addPillars(folio, itemGranularity ? "countWithItems" : "count" + form.getStringSafely("rollup", "")), 0,
					showPercent, pillarTypeOverride);
		case 3:
			return adjustShowValuePillars(addPillars(folio, itemGranularity ? "itemQuantity" : "quantity" + form.getStringSafely("rollup", "")), 0,
					showPercent, pillarTypeOverride);
		default:
			throw new ConvertException("Could not convert to showValue index", String.class, showValueIndex);
		}
	}

	protected void addShowAveragePillar(InputForm form, Folio folio, int showAverageIndex, boolean itemGranularity,
			PillarType pillarTypeOverride) throws ConvertException, DesignException {
		if (itemGranularity)
			throw new DesignException(
					"Averages per transaction cannot be meaningfully displayed when report is grouped by Item");
		switch (showAverageIndex) {
		case 1:
			adjustSortByPillars(addPillars(folio, "avgAmount" + form.getStringSafely("rollup", "")), 0, pillarTypeOverride);
			break;
		case 2:
			adjustSortByPillars(addPillars(folio, "avgQuantity" + form.getStringSafely("rollup", "")), 0, pillarTypeOverride);
			break;
		default:
			throw new ConvertException("Could not convert to showAverage index", String.class, showAverageIndex);
		}
	}

	protected Pillar[] adjustShowValuePillars(Pillar[] pillars, int groupingLevel, boolean showPercent,
			PillarType pillarTypeOverride) {
		for (Pillar pillar : pillars)
			adjustShowValuePillar(pillar, groupingLevel, showPercent, pillarTypeOverride);
		return pillars;
	}

	protected Pillar adjustShowValuePillar(Pillar pillar, int groupingLevel, boolean showPercent,
			PillarType pillarTypeOverride) {
		if (showPercent && pillar.getPercentFormat() == null)
			pillar.setPercentFormat(ConvertUtils.getFormat("NUMBER:##0.00%"));
		else if (!showPercent && pillar.getPercentFormat() != null)
			pillar.setPercentFormat(null);
		return adjustSortByPillar(pillar, groupingLevel, pillarTypeOverride);
	}

	protected Pillar[] adjustSortByPillars(Pillar[] pillars, int groupingLevel, PillarType pillarTypeOverride) {
		for (Pillar pillar : pillars)
			adjustSortByPillar(pillar, groupingLevel, pillarTypeOverride);
		return pillars;
	}

	protected Pillar adjustSortByPillar(Pillar pillar, int groupingLevel, PillarType pillarTypeOverride) {
		pillar.setGroupingLevel(groupingLevel);
		if (pillarTypeOverride != null)
			pillar.setPillarType(pillarTypeOverride);
		return pillar;
	}

	protected Pillar[] addPillars(Folio folio, String pillarName) throws DesignException {
		ObjectBuilder pillarsBuilder = pillars.get(pillarName);
		if (pillarsBuilder == null)
			throw new DesignException("Pillar '" + pillarName + "' is not configured");
		if (pillarsBuilder.getTag().equalsIgnoreCase("pillars")) {
			ObjectBuilder[] pillarBuilders = pillarsBuilder.getSubNodes();
			Pillar[] pillars = new Pillar[pillarBuilders.length];
			for (int i = 0; i < pillars.length; i++) {
				pillars[i] = folio.newPillar();
				pillars[i].readXML(pillarBuilders[i]);
			}
			return pillars;
		}
		Pillar pillar = folio.newPillar();
		pillar.readXML(pillarsBuilder);
		return new Pillar[] { pillar };
	}

	protected String[] getPreferencePillars(InputForm form) throws SQLException, DataLayerException, ConvertException {
		Results results = DataLayerMgr.executeQuery("GET_USER_DISPLAY_PREFERENCE", form);
		ArrayList<String> userPreference = new ArrayList<String>(6);
		while (results.next()) {
			String pillarXml = preferenceToPillarMap.get(results.getValue("displayId", Integer.class));
			if (pillarXml == null)
				throw new SQLException(
						"Preference " + results.getValue("displayId") + " not found in preferenceToPillarMap");
			userPreference.add(pillarXml);
		}
		if (userPreference.isEmpty())
			return DEFAULT_PREFERENCE_PILLARS;
		return userPreference.toArray(new String[userPreference.size()]);
	}

	protected void addFilterForRangeType(InputForm form, Folio folio)
			throws ServletException, DesignException, ConvertException {
		String rangeType = form.getString("rangeType", true) + form.getStringSafely("rollup", "").toUpperCase();
		RangeType rt = rangeTypes.get(rangeType);
		addFilter(form, folio, rt.filterName);
		adjustSubtitleForRangeType(form, folio, rt, false);
		addQueryOptimizerHint(folio, rangeType);
	}

	protected void addQueryOptimizerHint(Folio folio, String rangeType) {
		String hint = getQueryOptimizerHint(rangeType);
		if (!StringUtils.isBlank(hint))
			folio.setDirective("query-optimizer-hint", hint);
	}

	protected void adjustSubtitleForRangeType(InputForm form, Folio folio, RangeType rt, boolean filtered)
			throws ConvertException, DesignException {
		adjustSubtitleForRangeType(form, folio, rt, filtered, "params.tranType", null);
	}

	protected void adjustSubtitleForRangeType(InputForm form, Folio folio, RangeType rt, boolean filtered,
			String tranTypeParamName, Collection<?> rangeSelection) throws ConvertException, DesignException {
		if (folio.getSubtitle() == null && rt != null) {
			StringBuilder sb = new StringBuilder();
			String tranType = ConvertUtils.getString(form.get(tranTypeParamName), false);
			if (StringUtils.isBlank(tranType))
				sb.append("All");
			else {
				int[] tranTypeArray = ConvertUtils.convert(int[].class, tranType);
				SortedSet<String> tranTypeNames = new TreeSet<String>(new CaseInsensitiveComparator<String>());
				for (int tt : tranTypeArray) {
					tranTypeNames.add(getTranTypeName(tt));
				}
				if (tranTypeNames.size() >= 5) {
					sb.append("All");
				} else {
					int i = 0;
					for (String s : tranTypeNames) {
						if (i++ > 0) {
							if (i == tranTypeNames.size())
								sb.append(" and ");
							else
								sb.append(", ");
						}
						sb.append(s);
					}
				}
			}
			if (rangeSelection == null || rangeSelection.isEmpty()) {
				sb.append(" from ");
				Format format = ConvertUtils.getFormat("DATE", rt.displayDateFormat);
				sb.append(ConvertUtils.formatObject(form.get("params.beginDate"), format));
				sb.append(" to ");
				sb.append(ConvertUtils.formatObject(form.get("params.endDate"), format));
			} else {
				sb.append(" in ");
				Format format;
				if (Date.class.isAssignableFrom(
						SQLTypeUtils.getJavaType(getDetailFilter(form, rt.filterName).getField().getDisplaySqlType())))
					format = ConvertUtils.getFormat("DATE", rt.displayDateFormat);
				else
					format = ConvertUtils.getFormat(rt.selectionFormat);
				for (Object value : rangeSelection) {
					sb.append(ConvertUtils.formatObject(value, format)).append(", ");
				}
				sb.setLength(sb.length() - 2);
			}
			if (filtered)
				sb.append(" (Filtered)");

			folio.setSubtitle(ConvertUtils.getFormat("LITERAL", sb.toString()));
		}
	}

	protected String getTranTypeName(int tranTypeId) {
		switch (tranTypeId) {
		case 16:
		case 19:
		case 13:
		case 33:
			return "Credit";
		case 17:
		case 34:
			return "Access";
		case 18:
		case 23:
		case 35:
			return "Pass";
		case 20:
		case 21:
		case 31:
			return "Refunds";
		case 22:
			return "Cash";
		default:
			return "Others";
		}
	}

	protected Filter createFilterItemForLocation(StandardDesignEngine designEngine, long fieldId, String paramName,
			Object paramValue, boolean isArray, InputForm form) throws DesignException {
		StandardFilterItem theFilter = new StandardFilterItem(designEngine);
		Field theField = designEngine.getField(fieldId);
		theFilter.setField(theField);
		if (isArray) {
			theFilter.setOperatorById(arrayOperatorId);
		} else {
			theFilter.setOperatorById(1);
		}
		theFilter.getOperatorParams()[0].setName(paramName);
		form.set(EngineUtils.PARAMS_PREFIX + paramName, paramValue);
		Long posSourceFieldId = fields.get("posSource");
		if (posSourceFieldId == null)
			return theFilter;
		StandardFilterGroup filterGroup = new StandardFilterGroup(designEngine);
		filterGroup.setSeparator("AND");
		filterGroup.addFilter(theFilter);
		theFilter = new StandardFilterItem(designEngine);
		Field posSourceField = designEngine.getField(posSourceFieldId);
		theFilter.setField(posSourceField);
		theFilter.setOperatorById(1);
		Param param = theFilter.getOperatorParams()[0];
		param.setValue("TE");
		filterGroup.addFilter(theFilter);
		return filterGroup;
	}

	protected StandardFilterItem createNullFilterItemForLocation(StandardDesignEngine designEngine, long fieldId)
			throws DesignException {
		StandardFilterItem theFilter = new StandardFilterItem(designEngine);
		Field theField = designEngine.getField(fieldId);
		theField.setId(fieldId);
		theFilter.setField(theField);
		theFilter.setOperatorById(9);
		return theFilter;
	}

	protected boolean addFiltersForLocation(InputForm form, Folio folio, Holder<Boolean> hasSubregionsHolder)
			throws ConvertException, DesignException {
		ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
		StandardDesignEngine designEngine = (StandardDesignEngine) executeEngine.getDesignEngine();

		Object customerIds = form.get("params.customerIds");
		Object terminalIds = form.get("params.terminalIds");
		Object isLastDexFileOnly = form.get("params.isLastDexFileOnly");
		Object customerRegionIds = form.get("customerId_regionId");
		if (usalive_1_5_compatible) {
			if (customerIds == null)
				customerIds = form.get("customerId");
			if (terminalIds == null)
				terminalIds = form.get("terminals");
		}
		// main filter group
		StandardFilterGroup fgMainNew = new StandardFilterGroup();
		fgMainNew.setSeparator("OR");
		if (customerIds != null) {
			Filter customerIdMainFilter = createFilterItemForLocation(designEngine, fields.get("customerId"),
					"customerIds", customerIds, true, form);
			fgMainNew.addFilter(customerIdMainFilter);
		}
		if (terminalIds != null) {
			Filter terminalIdMainFilter = createFilterItemForLocation(designEngine, fields.get("locationId"),
					"terminalIds", terminalIds, true, form);
			fgMainNew.addFilter(terminalIdMainFilter);
		}
		if (isLastDexFileOnly != null) {
			Filter lastDexOnlyMainFilter = createFilterItemForLocation(designEngine, fields.get("isLastDexFileOnly"),
					"isLastDexFileOnly", isLastDexFileOnly, false, form);
			fgMainNew.addFilter(lastDexOnlyMainFilter);
		}
		String[] regionIds;
		if (customerRegionIds != null) {
			String[] customerRegions = ConvertUtils.convert(String[].class, customerRegionIds);
			regionIds = new String[customerRegions.length];
			for (int i = 0; i < customerRegions.length; i++) {
				String[] parts = customerRegions[i].split("\\_", 2);
				Filter customerIdFilter = createFilterItemForLocation(designEngine, fields.get("customerId"),
						"customerId_" + (i + 1), parts[0], false, form);
				Filter regionIdFilter;
				if (parts[1].trim().equals("0")) {
					regionIdFilter = createNullFilterItemForLocation(designEngine, fields.get("regionId"));
				} else {
					regionIdFilter = createFilterItemForLocation(designEngine, fields.get("regionId"),
							"regionId_" + (i + 1), parts[1], false, form);
				}
				StandardFilterGroup fgNew = new StandardFilterGroup();
				fgNew.setSeparator("AND");
				fgNew.addFilter(customerIdFilter);
				fgNew.addFilter(regionIdFilter);
				fgMainNew.addFilter(fgNew);
				regionIds[i] = parts[1];
			}
		} else
			regionIds = null;
		if (hasSubregionsHolder != null) {
			Map<String, Object> params = new HashMap<>();
			params.put(SimpleServlet.ATTRIBUTE_USER, RequestUtils.getUser(form));
			params.put("customerIds", customerIds);
			params.put("regionIds", regionIds);
			params.put("terminalIds", terminalIds);
			try {
				DataLayerMgr.selectInto("HAS_SUBREGIONS", params);
			} catch (SQLException | DataLayerException | BeanException e) {
				throw new DesignException(e);
			}
			hasSubregionsHolder.setValue(ConvertUtils.getBoolean(params.get("value")));
		}

		if (fgMainNew.filterCount() > 0) {
			FolioStepHelper.addFilter(folio, fgMainNew, "AND");
			return true;
		}
		return false;
	}

	protected FilterItem addFilter(InputForm form, Folio folio, String filterName) throws DesignException {
		FilterItem filter = getFilter(form, filterName);
		FolioStepHelper.addFilter(folio, filter, "AND");
		return filter;
	}

	protected FilterItem getDetailFilter(StandardDesignEngine designEngine, String filterName) throws DesignException {
		return getFilter(designEngine, filterName, true);
	}

	protected FilterItem getDetailFilter(InputForm form, String filterName) throws DesignException {
		return getFilter(form, filterName, true);
	}

	protected FilterItem getFilter(InputForm form, String filterName) throws DesignException {
		return getFilter(form, filterName, false);
	}

	protected FilterItem getFilter(InputForm form, String filterName, boolean detail) throws DesignException {
		ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
		StandardDesignEngine designEngine = (StandardDesignEngine) executeEngine.getDesignEngine();
		return getFilter(designEngine, filterName, detail);
	}

	protected FilterItem getFilter(StandardDesignEngine designEngine, String filterName) throws DesignException {
		return getFilter(designEngine, filterName, false);
	}

	protected FilterItem getFilter(StandardDesignEngine designEngine, String filterName, boolean detail)
			throws DesignException {
		ObjectBuilder filterBuilder;
		if (!detail || (filterBuilder = filters.get("detail." + filterName)) == null)
			filterBuilder = filters.get(filterName);
		if (filterBuilder == null)
			throw new DesignException("Filter '" + filterName + " is not configured");
		StandardFilterItem filter = new StandardFilterItem(designEngine);
		filter.readXML(filterBuilder);
		return filter;
	}

	protected Format makeShowValueFormat(Format original) {
		if (original == null)
			return new MessageFormat("{0}");
		else if (original instanceof LiteralFormat)
			return original;
		else
			return new MessageFormat("{0," + ConvertUtils.getFormatString(original).replaceFirst(":", ",") + '}');
	}
	
	protected boolean hasActivityRefField(int[] sortBy) {
		for (int i = 0; i < sortBy.length; i++) {
			if (sortBy[i] == 3 || sortBy[i] == 8 || sortBy[i] == 9 || sortBy[i] == 11 || sortBy[i] == 12) {
				return true;
			}
		} 
		return false;
	}
	
	public String getQueryOptimizerHint(String rangeType) {
		return queryOptimizerHints.get(rangeType);
	}

	public void setQueryOptimizerHint(String rangeType, String queryOptimizerHint) {
		this.queryOptimizerHints.put(rangeType, queryOptimizerHint);
	}

	public void setQueryOptimizerHints(Map<String, String> queryOptimizerHints) {
		this.queryOptimizerHints = queryOptimizerHints;
	}

	public static String getConfigFile() {
		return configFile;
	}

	public static void setConfigFile(String configFile) {
		AbstractBuildActivityFolio.configFile = configFile;
	}
}
