package com.usatech.report.build;

import java.sql.SQLException;
import java.util.Arrays;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Folio;
import simple.lang.Holder;
import simple.servlet.InputForm;

/**
 * Builds Activity Analysis report
 *
 * @author bkrug
 *
 */
public class BuildDiagnosticFolio extends AbstractBuildActivityFolio {
	
	public BuildDiagnosticFolio() {
		super();
	}

	/**
	 * @see AbstractBuildActivityFolio#getDefaultOutputTypeId()
	 */
	@Override
	public int getDefaultOutputTypeId() {
		return 22;
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return false;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			folio.setTitle(ConvertUtils.getFormat("LITERAL", ConvertUtils.getStringSafely(form.get("reportTitle"), "Diagnostics")));
			
			// logic for selecting customer, region and terminal
			Holder<Boolean> hasSubregionsHolder = new Holder<>();
			addFiltersForLocation(form, folio, hasSubregionsHolder);

			// add customer pillar (if user not a customer)
			if(!customerUser)
				adjustSortByPillars(addPillars(folio, "customer"), !Boolean.FALSE.equals(hasSubregionsHolder.getValue()) ? 6 : 5, null);
			
			// add region pillar
			if(!Boolean.FALSE.equals(hasSubregionsHolder.getValue())) {
				folio.setDirective("dynamic-labels", "true");
				adjustSortByPillars(addPillars(folio, "parentRegion"), 5, null);
			}

			// add region pillar
			adjustSortByPillars(addPillars(folio, "region"), 4, null);
			
			// add terminal pillars
			String[] preferencePillars = getPreferencePillars(form);
			adjustSortByPillars(addPillars(folio, preferencePillars[0]), 3, null);
			for(String pillarName : preferencePillars) {
				adjustSortByPillars(addPillars(folio, pillarName), 2, null);
			}
			adjustSortByPillars(addPillars(folio, "alertDate"), 2, null);// alert date
			adjustSortByPillars(addPillars(folio, "alertType"), 2, null);// alert type
			adjustSortByPillars(addPillars(folio, "alertDetail"), 2, null);// alert details
			
			addFilter(form, folio, "alert_all");			
			StringBuilder sb = new StringBuilder();
			RangeType rt = rangeTypes.get("ALL");
			if(rt != null) {
				sb.append("from {,simple.text.DateByTimezoneFormat,params.beginDate,~scene.timeZone,").append(rt.displayDateFormat)
					.append("} to {,simple.text.DateByTimezoneFormat,params.endDate,~scene.timeZone,").append(rt.displayDateFormat).append('}');
			} else {
				sb.append("from {params.beginDate} to {params.endDate}");
			}
				
			folio.setSubtitle(ConvertUtils.getFormat("MESSAGE", sb.toString()));
			// add filter for alert types
			int[] alertTypes = ConvertUtils.convert(int[].class, form.get("params.alertType"));
			if(alertTypes != null) {
				int[] possibleAlertTypes = ConvertUtils.convert(int[].class, form.get("possibleAlertType"));
				if(possibleAlertTypes != null && possibleAlertTypes.length > 0 && possibleAlertTypes.length == alertTypes.length) {
					Arrays.sort(alertTypes);
					Arrays.sort(possibleAlertTypes);
					if(Arrays.equals(alertTypes, possibleAlertTypes))
						alertTypes = null; // all
				}
				if(alertTypes != null) {
					addFilter(form, folio, "alertType");
				}
			}
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}
}
