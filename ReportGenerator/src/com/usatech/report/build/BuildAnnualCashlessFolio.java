package com.usatech.report.build;


import javax.servlet.ServletException;

import simple.bean.ConvertUtils;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Pillar.PillarType;
import simple.servlet.InputForm;

/**
 * The pillar information is in BuildReportPillars.properties file
 * @author yhe
 *
 */
public class BuildAnnualCashlessFolio extends AbstractBuildActivityFolio {
	
	public BuildAnnualCashlessFolio() {
		super();
	}

	/**
	 * @see AbstractBuildActivityFolio#getDefaultOutputTypeId()
	 */
	@Override
	public int getDefaultOutputTypeId() {
		return 28;
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return false;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			folio.setTitle(ConvertUtils.getFormat("LITERAL", ConvertUtils.getStringSafely(form.get("reportTitle"), "Total Annual Cashless Sales")));
			
			// need to add this to make join correct, not via activity_ref
			//if(!isCustomerUser(form))
			adjustSortByPillars(addPillars(folio, "baCustomerName"), 5, null);
			adjustSortByPillars(addPillars(folio, "baCurrencyCode"), 4, PillarType.CLUSTER);
			adjustSortByPillars(addPillars(folio, "baYear"), 3, PillarType.CLUSTER);
			adjustSortByPillars(addPillars(folio, "baAmount"), 0, PillarType.VALUE);
			
			addFilter(form, folio, "baYearRange");	
			addFilter(form, folio, "baTranType");	
			StringBuilder sb = new StringBuilder();
			
			sb.append("from {params.beginDate,DATE,MM/dd/yyyy} to {params.endDate,DATE,MM/dd/yyyy}");
			folio.setSubtitle(ConvertUtils.getFormat("MESSAGE", sb.toString()));
		} catch(DesignException e) {
			throw new ServletException(e);
		}
	}
}
