package com.usatech.report.build;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Folio;
import simple.lang.Holder;
import simple.servlet.InputForm;

/**
 * The pillar information is in BuildReportPillars.properties file
 * @author yhe
 *
 */
public class BuildDexStatusFolio extends AbstractBuildActivityFolio {
	
	public BuildDexStatusFolio() {
		super();
	}

	/**
	 * @see AbstractBuildActivityFolio#getDefaultOutputTypeId()
	 */
	@Override
	public int getDefaultOutputTypeId() {
		return 22;
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return false;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			folio.setTitle(ConvertUtils.getFormat("LITERAL", ConvertUtils.getStringSafely(form.get("reportTitle"), "Dex Status")));
			
			// logic for selecting customer, region and terminal
			Holder<Boolean> hasSubregionsHolder = new Holder<>();
			addFiltersForLocation(form, folio, hasSubregionsHolder);

			// add customer pillar (if user not a customer)
			if(!customerUser)
				adjustSortByPillars(addPillars(folio, "customer"), !Boolean.FALSE.equals(hasSubregionsHolder.getValue()) ? 6 : 5, null);
			
			// add region pillar
			if(!Boolean.FALSE.equals(hasSubregionsHolder.getValue())) {
				folio.setDirective("dynamic-labels", "true");
				adjustSortByPillars(addPillars(folio, "parentRegion"), 5, null);
			}
			adjustSortByPillars(addPillars(folio, "region"), 4, null);
			adjustSortByPillars(addPillars(folio, "location"), 4, null);
			adjustSortByPillars(addPillars(folio, "address1"), 4, null);
			adjustSortByPillars(addPillars(folio, "address2"), 4, null);
			adjustSortByPillars(addPillars(folio, "city"), 4, null);
			adjustSortByPillars(addPillars(folio, "state"), 4, null);
			
			adjustSortByPillars(addPillars(folio, "dexDeviceSerialNum"), 4, null);
			adjustSortByPillars(addPillars(folio, "dexAsset"), 4, null);
			adjustSortByPillars(addPillars(folio, "dexFileSize"), 2, null);
			adjustSortByPillars(addPillars(folio, "dexFileName"), 2, null);
			adjustSortByPillars(addPillars(folio, "dexDate"), 2, null);
			adjustSortByPillars(addPillars(folio, "dexSentDate"), 2, null);
			adjustSortByPillars(addPillars(folio, "dexAlertCount"), 2, null);
			
			
			if(form.getInt("params.isLastDexFileOnly", false, 0)==1){
				adjustSortByPillars(addPillars(folio, "dexExceptionCountWithLastDex"), 2, null);
				adjustSortByPillars(addPillars(folio, "dexLastGoodDex"), 2, null);
			}else{
				adjustSortByPillars(addPillars(folio, "dexExceptionCount"), 2, null);
				addFilter(form, folio, "dex_all");	
				StringBuilder sb = new StringBuilder();
				RangeType rt = rangeTypes.get("ALL");
				if(rt != null) {
					sb.append("from {,simple.text.DateByTimezoneFormat,params.beginDate,~scene.timeZone,").append(rt.displayDateFormat)
						.append("} to {,simple.text.DateByTimezoneFormat,params.endDate,~scene.timeZone,").append(rt.displayDateFormat).append('}');
				} else {
					sb.append("from {params.beginDate} to {params.endDate}");
				}
					
				folio.setSubtitle(ConvertUtils.getFormat("MESSAGE", sb.toString()));
			}
			if(form.getInt("params.isDexExceptionOnly", false, 0)==1){
				addFilter(form, folio, "isDexExceptionOnly");	
				addFilter(form, folio, "dexAlertId");
				adjustSortByPillars(addPillars(folio, "dexExceptionDetails"), 2, null);
			}
			
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		}
	}
}
