package com.usatech.report.build;

import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Field;
import simple.falcon.engine.FilterItem;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.SortOrder;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardFilterItem;
import simple.falcon.servlet.FalconServlet;
import simple.falcon.servlet.FolioStepHelper;
import simple.io.Log;
import simple.lang.Holder;
import simple.results.Results.Aggregate;
import simple.servlet.InputForm;
import simple.sql.SQLTypeUtils;
import simple.text.MessageFormat;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

/**
 * Builds Activity Analysis report
 *
 * @author bkrug
 *
 */
public class BuildActivityExtFolio extends AbstractBuildActivityFolio {
	private static final Log log = Log.getLog();
	// protected static final long[] fieldsForDetailsLink = new long[]{0, FIELD_CUSTOMER_ID, FIELD_TERMINAL_ID, 949, 440, 440, 440, FIELD_TRANS_TYPE_ID, 23, 35, 32, 541, 103, 14};
	// protected static final int[] operatorsForDetailsLink = new int[]{0, 1, 1, 1, 30, 29, 28, 1, 1, 1, 1, 1, 1, 1};

	public BuildActivityExtFolio() {
		super();
	}

	protected FilterItem getLinkActionFilter(int sortByIndex, StandardDesignEngine designEngine) throws ConvertException, DesignException {
		return getDetailFilter(designEngine, getFilterName(sortByIndex));
	}

	protected Field getLinkActionField(int sortByIndex, StandardDesignEngine designEngine, boolean rollup) throws ConvertException, DesignException {
		return getDetailFilter(designEngine, getFilterName(sortByIndex, rollup)).getField();
	}
	
	protected Field getLinkActionField(int sortByIndex, StandardDesignEngine designEngine) throws ConvertException, DesignException {
		return getLinkActionField(sortByIndex, designEngine, false);
	}

	protected String getFilterName(int sortByIndex) throws ConvertException {
		return getFilterName (sortByIndex, false);
	}
	
	protected String getFilterName(int sortByIndex,  boolean rollup) throws ConvertException {
		switch(sortByIndex) {
			case 1:
				return "customer";
			case 2:
				return "location";
			case 3:
				return "fill";
			case 4:
				return "month" + (rollup ? "Rollup" : "");
			case 5:
				return "week" + (rollup ? "Rollup" : "");
			case 6:
				return "day" + (rollup ? "Rollup" : "");
			case 7:
				return "singleTranType" + (rollup ? "Rollup" : "");
			case 8:
				return "card";
			case 9:
				return "tranHour";
			case 10:
				return "tranDayOfWeek" + (rollup ? "Rollup" : "");
			case 11:
				return "item";
			case 12:
				return "settleState";
			case 13:
				return "region" + (rollup ? "Rollup" : "");
			case -13:
				return "parentRegion";
			case 14:
				return "client";
			default:
				throw new ConvertException("Could not convert to sortBy index", String.class, sortByIndex);
		}
	}
	
	/**
	 * @see AbstractBuildActivityFolio#getDefaultOutputTypeId()
	 */
	@Override
	public int getDefaultOutputTypeId() {
		return 22;
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return false;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			folio.setTitle(ConvertUtils.getFormat("LITERAL", ConvertUtils.getStringSafely(form.get("reportTitle"), "Activity Analysis")));
			ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
			StandardDesignEngine designEngine = (StandardDesignEngine) executeEngine.getDesignEngine();
			int[] sortBy = ConvertUtils.convertRequired(int[].class, form.get("sortBy"));
			int[] showValues = ConvertUtils.convertRequired(int[].class, form.get("showValues"));
			int[] showPercents = ConvertUtils.convert(int[].class, form.get("showPercents"));
			int[] showAverages = ConvertUtils.convert(int[].class, form.get("showAverages"));
			if(sortBy.length != 4)
				throw new ServletException("Invalid input: Number of groups specified is not correct. Need 4");
			boolean itemGranularity = false;
			StringBuilder sb = new StringBuilder();
			String rangeType = form.getString("rangeType", true);
			String tranType = ConvertUtils.getString(form.get("params.tranType"), true);
			Date beginDate = ConvertUtils.convertRequired(Date.class, form.get("params.beginDate"));
			Date endDate = ConvertUtils.convertRequired(Date.class, form.get("params.endDate"));
			form.set("params.tranType", tranType);
			RangeType rt = rangeTypes.get(rangeType);
			boolean rollup = !hasActivityRefField(sortBy);
			if (rollup) {
				form.set("rollup", "Rollup");
				rt = rangeTypes.get(rangeType + "ROLLUP");
			} else
				rt = rangeTypes.get(rangeType);
			
			sb.append("activity_detail_ext.i?rangeType=").append(rangeType).append("&params.beginDate=");
			if(rt != null)
				sb.append(ConvertUtils.formatObject(beginDate, "DATE:" + rt.parameterDateFormat));
			else
				sb.append("'{").append(beginDate.getTime()).append("}'");
			sb.append("&params.endDate=");
			if(rt != null)
				sb.append(ConvertUtils.formatObject(endDate, "DATE:" + rt.parameterDateFormat));
			else
				sb.append("'{").append(endDate.getTime()).append("}'");
			sb.append("&params.tranType=").append(StringUtils.prepareURLPart(tranType)).append("&sortBy=").append(ConvertUtils.getString(sortBy, true));
			List<Field> sortByFields = new ArrayList<Field>(4);
			Field field;
			// logic for selecting customer, region and terminal
			Holder<Boolean> hasSubregionsHolder = new Holder<Boolean>();
			addFiltersForLocation(form, folio, hasSubregionsHolder);
			// add currency pillar
			if(CollectionUtils.iterativeSearch(showValues, 1) >= 0 || (showAverages != null && CollectionUtils.iterativeSearch(showAverages, 1) >= 0)) {
				addPillars(folio, "currency");
				field = designEngine.getField(fields.get("currencyCd" + form.getStringSafely("rollup", "")));
				sortByFields.add(field);
				sb.append('&').append(StringUtils.prepareSectionLabel(field.getLabel())).append("={").append(sortByFields.size()).append(",simple.text.SetFormat,,;PREPARE:URL}");
			}
			for(int i = 0; i < 4; i++) {
				if(i > 0 && CollectionUtils.iterativeSearch(sortBy, sortBy[i], 0, i) >= 0)
					continue;
				String sortByRangeType = null;
				int level = 4 - i;
				if(i == 0 && sortBy[1] == 13 && !Boolean.FALSE.equals(hasSubregionsHolder.getValue()))
					level++;
				switch(sortBy[i]) {
					case 0:
						continue;
					case 3: // FILL
						sortByRangeType = "FILL";
						break;
					case 4: // MONTH
						sortByRangeType = "MONTH";
						break;
					case 5: // WEEK
						sortByRangeType = "WEEK";
						break;
					case 6: // DAY
						sortByRangeType = "DAY";
						break;
					case 11:
						itemGranularity = true;
						break;
					case 13:
						if(!Boolean.FALSE.equals(hasSubregionsHolder.getValue())) {
							folio.setDirective("dynamic-labels", "true");
							addSortByPillar(form, folio, -sortBy[i], level + 1, null);
							field = getLinkActionField(-sortBy[i], designEngine, rollup);
							sortByFields.add(field);
							sb.append('&').append(getLinkFilterLabel(-sortBy[i])).append("={").append(sortByFields.size()).append(",simple.text.SetFormat,,;");
							if(Date.class.isAssignableFrom(SQLTypeUtils.getJavaType(field.getDisplaySqlType())))
								sb.append("DATE:").append(rangeTypes.get(sortByRangeType).parameterDateFormat);
							else
								sb.append("PREPARE:URL");
							sb.append('}');
						}
						break;
				}
				addSortByPillar(form, folio, sortBy[i], level, null);
				field = getLinkActionField(sortBy[i], designEngine, rollup);
				sortByFields.add(field);
				sb.append('&').append(getLinkFilterLabel(sortBy[i])).append("={").append(sortByFields.size()).append(",simple.text.SetFormat,,;");
				if(Date.class.isAssignableFrom(SQLTypeUtils.getJavaType(field.getDisplaySqlType())))
					sb.append("DATE:").append(rangeTypes.get(sortByRangeType).parameterDateFormat);
				else
					sb.append("PREPARE:URL");
				sb.append('}');				
			}

			if(showPercents != null)
				Arrays.sort(showPercents);
			for(int sv : showValues) {
				boolean showPercent = (showPercents != null && Arrays.binarySearch(showPercents, sv) >= 0);
				Pillar[] pillars = addShowValuePillars(form, folio, sv, itemGranularity, showPercent, null);
				for(Pillar pillar : pillars) {
					for(Field f : sortByFields)
						pillar.addFieldOrder(f, SortOrder.ASC, Aggregate.SET, 0);
					pillar.setActionFormat(new MessageFormat(sb.toString()));
					if(!sortByFields.isEmpty()) {
						pillar.setDisplayFormat(makeShowValueFormat(pillar.getDisplayFormat()));
						pillar.setSortFormat(makeShowValueFormat(pillar.getSortFormat()));
						if(pillar.getHelpFormat() != null)
							pillar.setHelpFormat(makeShowValueFormat(pillar.getHelpFormat()));
						if(pillar.getLabelFormat() != null)
							pillar.setLabelFormat(makeShowValueFormat(pillar.getLabelFormat()));
						if(pillar.getPercentFormat() != null)
							pillar.setPercentFormat(makeShowValueFormat(pillar.getPercentFormat()));
						if(pillar.getStyleFormat() != null)
							pillar.setStyleFormat(makeShowValueFormat(pillar.getStyleFormat()));
					}
				}
			}
			if(showAverages != null)
				for(int sa : showAverages) {
					addShowAveragePillar(form, folio, sa, itemGranularity, null);
				}
			addFilter(form, folio, "tranType" + form.getStringSafely("rollup", ""));
			addFilterForRangeType(form, folio);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}
	
	protected boolean addFilter(InputForm form, Folio folio, Field field, StandardDesignEngine designEngine, Set<String> addKeepParamNames) throws ConvertException {
		String paramName = StringUtils.prepareSectionLabel(field.getLabel());
		Object tmp = form.get(paramName);
		if(log.isDebugEnabled())
			log.debug("Adding filter '" + paramName + "' on " + tmp + " for field " + field.getId());
		if(tmp != null) {
			Collection<?> value = ConvertUtils.asCollection(tmp, ConvertUtils.convertToWrapperClass(SQLTypeUtils.getJavaType(field.getSortSqlType())));
			if(value != null && !value.isEmpty()) {
				StandardFilterItem filter = new StandardFilterItem(designEngine);
				filter.setField(field);
				if(value.size() == 1) {
					filter.setOperatorById(1);
					filter.setValues(value.toArray());
				} else {
					filter.setOperatorById(arrayOperatorId);
					// NOTE: It appears that Oracle has an issue with MEMBER OF DATE_TABLE (in that it returns no rows), so use IN operator for those cases
					if(arrayOperatorId == 21)
						switch(field.getSortSqlType().getTypeCode()) {
							case Types.DATE:
							case Types.TIMESTAMP:
								filter.setOperatorById(27);
								break;
						}
					filter.setValues(new Object[] {value.toArray()});
				}
				FolioStepHelper.addFilter(folio, filter, "AND");
				addKeepParamNames.add(paramName);
				return true;
			}
		}
		return false;
	}
}
