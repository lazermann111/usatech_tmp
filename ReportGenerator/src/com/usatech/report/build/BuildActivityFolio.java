package com.usatech.report.build;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Field;
import simple.falcon.engine.FieldOrder;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.SortOrder;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.servlet.FalconServlet;
import simple.io.Log;
import simple.lang.Holder;
import simple.results.Results.Aggregate;
import simple.servlet.InputForm;
import simple.sql.SQLTypeUtils;
import simple.text.MessageFormat;
import simple.text.StringUtils;

/**
 * Builds Activity Analysis report
 *
 * @author bkrug
 *
 */
public class BuildActivityFolio extends AbstractBuildActivityFolio {
	private static final Log log = Log.getLog();
	public BuildActivityFolio() {
		super();
	}

	/**
	 * @see AbstractBuildActivityFolio#getDefaultOutputTypeId()
	 */
	@Override
	public int getDefaultOutputTypeId() {
		return 22;
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return false;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			folio.setTitle(ConvertUtils.getFormat("LITERAL", ConvertUtils.getStringSafely(form.get("reportTitle"), "Activity")));
			ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
			StandardDesignEngine designEngine = (StandardDesignEngine) executeEngine.getDesignEngine();
			StringBuilder sb = new StringBuilder();
			String rangeType = form.getString("rangeType", true);
			boolean rollup = !"ALL".equalsIgnoreCase(rangeType) && !"FILL".equalsIgnoreCase(rangeType)&&!configFile.equals("com/usatech/report/build/BuildReportPillars-RDW.properties");
			RangeType rt;
			if (rollup) {
				form.set("rollup", "Rollup");
				rt = rangeTypes.get(rangeType + "ROLLUP");
			} else
				rt = rangeTypes.get(rangeType);
			String tranType = ConvertUtils.getString(form.get("params.tranType"), false);
			Date beginDate = ConvertUtils.convertRequired(Date.class, form.get("params.beginDate"));
			Date endDate = ConvertUtils.convertRequired(Date.class, form.get("params.endDate"));
			if(!StringUtils.isBlank(tranType))
				form.set("params.tranType", tranType);
			sb.append("activity_detail.i?saveAction=save_report_params_prompt.i&rangeType=").append(rangeType).append("&params.beginDate=");
			if(rt != null)
				sb.append(ConvertUtils.formatObject(beginDate, "DATE:" + rt.parameterDateFormat));
			else
				sb.append("'{").append(beginDate.getTime()).append("}'");
			sb.append("&params.endDate=");
			if(rt != null)
				sb.append(ConvertUtils.formatObject(endDate, "DATE:" + rt.parameterDateFormat));
			else
				sb.append("'{").append(endDate.getTime()).append("}'");
			if(!StringUtils.isBlank(tranType))
				sb.append("&params.tranType=").append(tranType);
			List<Field> sortByFields = new ArrayList<Field>(4);
			String[] preferencePillars = getPreferencePillars(form);
			FieldOrder fo;
			Field field;
			int[] sortBy = ConvertUtils.convert(int[].class, form.get("sortBy"));
			int level = 3;
			Set<Integer> sortByClean;
			// logic for selecting customer, region and terminal
			Holder<Boolean> hasSubregionsHolder = new Holder<>();
			addFiltersForLocation(form, folio, hasSubregionsHolder);
			if(sortBy != null && sortBy.length > 0) {
				sortByClean = new LinkedHashSet<>();
				for(int sortByIndex : sortBy) {
					switch(sortByIndex) {
						case 1:
							// add customer pillar (if user not a customer)
							if(!customerUser)
								sortByClean.add(sortByIndex);
							break;
						case 2:
						case 14:
							sortByClean.add(sortByIndex);
							break;
						case 13:
							if(!Boolean.FALSE.equals(hasSubregionsHolder.getValue())) {
								sortByClean.add(-sortByIndex);
								folio.setDirective("dynamic-labels", "true");
							}
							sortByClean.add(sortByIndex);
							break;
						default:
							log.warn("Not adding grouping by " + getLinkFilterLabel(sortByIndex) + " because it is not supported");
					}
				}
			} else
				sortByClean = Collections.emptySet();
			level += sortByClean.size();

			// add currency pillar - at level 6
			adjustSortByPillars(addPillars(folio, "currency"), level--, null);
			field = designEngine.getField(fields.get("currencyCd" + form.getStringSafely("rollup", "")));
			sortByFields.add(field);
			sb.append('&').append(StringUtils.prepareSectionLabel(stripRollup(field.getLabel(), rollup))).append("={").append(sortByFields.size()).append(",simple.text.SetFormat,,;PREPARE:URL}");
			
			// add other grouping pillars
			for(int sortByIndex : sortByClean) {
				String label;
				if(sortByIndex == 2)
					label = preferencePillars[0];
				else
					label = getLinkFilterLabel(sortByIndex);
				Pillar[] sortByPillars = adjustSortByPillars(addPillars(folio, label), level--, null);
				if(sortByIndex != 2) {
					field = getFirstField(sortByPillars);
					if(field != null) {
						sortByFields.add(field);
						sb.append('&').append(StringUtils.prepareSectionLabel(field.getLabel())).append("={").append(sortByFields.size()).append(",simple.text.SetFormat,,;PREPARE:URL}");
					}
				}
			}

			// add terminal pillars
			for(String pillarName : preferencePillars) 
				adjustSortByPillars(addPillars(folio, pillarName), 2, null);
			
			field = designEngine.getField(fields.get("locationId"));
			sortByFields.add(field);
			sb.append('&').append(StringUtils.prepareSectionLabel(field.getLabel())).append("={").append(sortByFields.size()).append(",simple.text.SetFormat,,;PREPARE:URL}");
			// add range type pillar
			if(rt.pillarName != null && rt.pillarName.trim().length() > 0) {
				Pillar[] pillars = adjustSortByPillars(addPillars(folio, rt.pillarName), 2, null);
				for(Pillar pillar : pillars) {
					fo = pillar.getFieldOrders()[0];
					fo.setSortOrder(SortOrder.DESC);
					field = getDetailFilter(designEngine, rt.filterName).getField();
					sortByFields.add(field);
					// TODO: how about Fill Dates?
					sb.append('&').append(StringUtils.prepareSectionLabel(rt.filterName.replaceFirst("Rollup", ""))).append("={").append(sortByFields.size()).append(",simple.text.SetFormat,,;");
					if(Date.class.isAssignableFrom(SQLTypeUtils.getJavaType(field.getDisplaySqlType())))
						sb.append("DATE:").append(rt.parameterDateFormat);
					else
						sb.append("PREPARE:URL");
					sb.append('}');
				}
			}
			
			if("FILL".equalsIgnoreCase(rangeType))
				adjustSortByPillars(addPillars(folio, "convenienceFee"), -3, null);

			//add tran type pillar
			adjustSortByPillars(addPillars(folio, "tranType" + form.getStringSafely("rollup", "")), 1, null);
			field = designEngine.getField(fields.get("tranTypeId" + form.getStringSafely("rollup", "")));
			sortByFields.add(field);
			sb.append('&').append(StringUtils.prepareSectionLabel(stripRollup(field.getLabel(), rollup))).append("={").append(sortByFields.size()).append(",simple.text.SetFormat,,;PREPARE:URL}");
			// add amount pillar
			Pillar[] pillars = adjustShowValuePillars(addPillars(folio, "amount" + form.getStringSafely("rollup", "")), 0, false, null);
			for(Pillar pillar : pillars) {
				pillar.setLabelFormat(null);
				for(Field f : sortByFields)
					pillar.addFieldOrder(f, SortOrder.ASC, Aggregate.SET, 0);
				pillar.setActionFormat(new MessageFormat(sb.toString()));
				if(!sortByFields.isEmpty()) {
					pillar.setDisplayFormat(makeShowValueFormat(pillar.getDisplayFormat()));
					pillar.setSortFormat(makeShowValueFormat(pillar.getSortFormat()));
					if(pillar.getHelpFormat() != null)
						pillar.setHelpFormat(makeShowValueFormat(pillar.getHelpFormat()));
					if(pillar.getLabelFormat() != null)
						pillar.setLabelFormat(makeShowValueFormat(pillar.getLabelFormat()));
					if(pillar.getPercentFormat() != null)
						pillar.setPercentFormat(makeShowValueFormat(pillar.getPercentFormat()));
					if(pillar.getStyleFormat() != null)
						pillar.setStyleFormat(makeShowValueFormat(pillar.getStyleFormat()));
				}
			}
			
			if(!StringUtils.isBlank(tranType))
				addFilter(form, folio, "tranType" + form.getStringSafely("rollup", ""));
			addFilterForRangeType(form, folio);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}

	protected String stripRollup(String fieldLabel, boolean rollup) {
		if(rollup && fieldLabel.startsWith("BA "))
			return fieldLabel.substring(3);
		return fieldLabel;
	}

	protected Field getFirstField(Pillar[] pillars) {
		if(pillars == null || pillars.length == 0)
			return null;
		for(Pillar pillar : pillars) {
			FieldOrder[] fos = pillar.getFieldOrders();
			if(fos == null || fos.length == 0)
				continue;
			for(FieldOrder fo : fos)
				if(fo.getField() != null)
					return fo.getField();
		}
		return null;
	}
}
