package com.usatech.report.build;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.falcon.engine.DesignEngine;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Field;
import simple.falcon.engine.FilterItem;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Param;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardFilterGroup;
import simple.falcon.run.FolioUpdateUtils;
import simple.falcon.servlet.FalconServlet;
import simple.servlet.InputForm;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.StringUtils;

/**
 * Builds Activity Analysis Detail report
 *
 * @author bkrug
 *
 */
public class BuildActivityDetailFolio extends BuildActivityExtFolio {

	public BuildActivityDetailFolio() {
		super();
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return true;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			folio.setTitle(ConvertUtils.getFormat("LITERAL", ConvertUtils.getStringSafely(form.get("reportTitle"), "Activity Detail")));
			addPillars(folio, "currency");
			addSortByPillar(form, folio, 2, 4, null);
			adjustSortByPillars(addPillars(folio, "tranAndSettleType"), 3, null);
			adjustSortByPillars(addPillars(folio, "refNbr"), 2, null);
			adjustSortByPillars(addPillars(folio, "date"), 2, null);
			adjustSortByPillars(addPillars(folio, "cardType"), 2, null);
			adjustSortByPillars(addPillars(folio, "card"), 2, null);
			adjustShowValuePillars(addPillars(folio, "amount"), 0, false, null);
			adjustSortByPillars(addPillars(folio, "apCode"), 2, null);
			adjustSortByPillars(addPillars(folio, "items"), 2, null);
			adjustShowValuePillars(addPillars(folio, "quantity"), 0, false, null);
			adjustSortByPillars(addPillars(folio, "deviceTranId"), 2, null);
			adjustSortByPillars(addPillars(folio, "applyToCardId"), 2, null);

			//add filters
			ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
			StandardDesignEngine designEngine = (StandardDesignEngine) executeEngine.getDesignEngine();
			Set<String> addKeepParamNames = new HashSet<String>();
			form.set("additionalKeepParamNames", addKeepParamNames);
			boolean filtered = false;
			// add currency filter
			if(addFilter(form, folio, designEngine.getField(fields.get("currencyCd")), designEngine, addKeepParamNames))
				filtered = true;
			// add terminal filter
			if(addFilter(form, folio, designEngine.getField(fields.get("locationId")), designEngine, addKeepParamNames))
				filtered = true;
			
			//add tran type filter
			Field field = designEngine.getField(fields.get("tranTypeId"));
			if(addFilter(form, folio, field, designEngine, addKeepParamNames))
				filtered = true;

			// add range type pillar
			addDetailFilterForRangeType(form, folio, designEngine, addKeepParamNames, filtered, StringUtils.prepareSectionLabel(field.getLabel()));

			// fix up tranType
			fixArrayParam(form);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}
	
	protected void addDetailFilterForRangeType(InputForm form, Folio folio, DesignEngine designEngine, Set<String> addKeepParamNames, boolean filtered, String tranTypeParamName) throws ServletException, DesignException, ConvertException {
		String rangeType = form.getString("rangeType", true);
		RangeType rt = rangeTypes.get(rangeType);
		Collection<?> rangeSelection = null;
		boolean doAll;
		if(rt.filterName != null && rt.filterName.trim().length() > 0) {
			if("all".equals(rt.filterName)) {
				doAll = true;
			} else {
				String paramName = StringUtils.prepareSectionLabel(rt.filterName);
				Object tmp = form.get(paramName);
				if(tmp != null) {
					FilterItem filter = getDetailFilter(form, rt.filterName);
					SQLType[] sqlTypes = filter.getSqlTypes();
					rangeSelection = ConvertUtils.asCollection(tmp, ConvertUtils.convertToWrapperClass(SQLTypeUtils.getJavaType(sqlTypes[0])));
					if(rangeSelection != null && !rangeSelection.isEmpty()) {
						addKeepParamNames.add(paramName);
						Param[] params = filter.getOperatorParams();
						if(params.length > 0) {
							for(Param p : params)
								p.setName(null);
							StandardFilterGroup group = new StandardFilterGroup();
							group.setSeparator(StandardFilterGroup.SEPARATOR_OR);
							for(Iterator<?> iter = rangeSelection.iterator(); iter.hasNext();) {
								Object value = iter.next();
								FilterItem fi;
								if(iter.hasNext())
									fi = filter.copy(null);
								else
									fi = filter;
								for(Param p : fi.getOperatorParams())
									p.setValue(value);
								group.addFilter(fi);
							}
							FolioUpdateUtils.addFilter(folio, group, StandardFilterGroup.SEPARATOR_AND);
						}
						doAll = false;
					} else
						doAll = true;
				} else
					doAll = true;
			}
		} else
			doAll = true;
		if(doAll)
			addFilter(form, folio, "all");
		adjustSubtitleForRangeType(form, folio, rt, filtered, tranTypeParamName, rangeSelection);
		addQueryOptimizerHint(folio, rangeType);
	}
}
