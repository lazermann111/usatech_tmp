package com.usatech.report.build;

/**
 * Resolves BuildActivityFolio class by action name
 */
public class BuildActivityFolioResolver {
    public Class<? extends AbstractBuildActivityFolio> resolve(String actionName) {
        switch (actionName) {
            case "dex_status":
                return BuildDexStatusFolio.class;
            case "diagnostic":
                return BuildDiagnosticFolio.class;
            case "activity":
                return BuildActivityFolio.class;
            case "activity_detail":
                return BuildActivityDetailFolio.class;
            case "activity_summary":
                return BuildActivitySummaryFolio.class;
            case "activity_graph":
                return BuildActivityGraphFolio.class;
            case "activity_ext":
                return BuildActivityExtFolio.class;
            case "activity_detail_ext":
                return BuildActivityDetailExtFolio.class;
            case "activity_rollup":
                return BuildActivityRollupFolio.class;
            case "total_annual_cashless_sales":
                return BuildAnnualCashlessFolio.class;
            default:
                throw new IllegalArgumentException("There is no mapping to resolve AbstractBuildActivityFolio class " +
                        "name by actionName = " + actionName);
        }
    }
}
