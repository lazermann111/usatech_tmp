package com.usatech.report.build;

import java.sql.SQLException;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Pillar.PillarType;
import simple.servlet.InputForm;

/**
 * Builds Activity Graph report
 *
 * @author bkrug
 */
public class BuildActivityGraphFolio extends AbstractBuildActivityFolio {
	public BuildActivityGraphFolio() {
		super();
	}

	/**
	 * @see AbstractBuildActivityFolio#getDefaultOutputTypeId()
	 */
	@Override
	public int getDefaultOutputTypeId() {
		return 28;
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return false;
	}

	/**
	  com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			folio.setTitle(ConvertUtils.getFormat("LITERAL", ConvertUtils.getStringSafely(form.get("reportTitle"), "Activity Graph")));
			int[] sortBy = ConvertUtils.convertRequired(int[].class, form.get("sortBy"));
			int[] showValues = ConvertUtils.convertRequired(int[].class, form.get("showValues"));
			int[] showAverages = ConvertUtils.convert(int[].class, form.get("showAverages"));
			if(sortBy.length != 4)
				throw new ServletException("Invalid input: Number of groups specified is not correct. Need 4");
			boolean itemGranularity = false;
			boolean rollup = !hasActivityRefField(sortBy);
			if (rollup) {
				form.set("rollup", "Rollup");
			} ;
			for(int i = 0; i < 4; i++) {
				switch(sortBy[i]) {
					case 0:
						continue;
					case 11:
						itemGranularity = true;
						break;
					case 13:
						adjustSortByPillars(addPillars(folio, "regionDetail"), 4 - i, i > 1 ? PillarType.CLUSTER : null);
						continue;
				}
				addSortByPillar(form, folio, sortBy[i], 4 - i, i > 1 ? PillarType.CLUSTER : null);
			}
			boolean needCurrency = false;
			for(int sv : showValues) {
				addShowValuePillars(form, folio, sv, itemGranularity, false, PillarType.VALUE);
				if(sv == 1)
					needCurrency = true;
			}
			if(showAverages != null)
				for(int sa : showAverages) {
					addShowAveragePillar(form, folio, sa, itemGranularity, PillarType.VALUE);
					if(sa == 1)
						needCurrency = true;
				}
			if(needCurrency)
				addPillars(folio, "currency");
			addFilter(form, folio, "tranType" + form.getStringSafely("rollup", ""));
			addFilterForRangeType(form, folio);
			// logic for selecting customer, region and terminal
			addFiltersForLocation(form, folio, null);
			// fix up tranType
			fixArrayParam(form);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}
}
