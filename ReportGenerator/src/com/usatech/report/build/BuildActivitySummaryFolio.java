package com.usatech.report.build;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.Folio;
import simple.lang.Holder;
import simple.servlet.InputForm;

/**
 * Builds Activity Summary report
 *
 * @author bkrug
 */
public class BuildActivitySummaryFolio extends AbstractBuildActivityFolio {
	public BuildActivitySummaryFolio() {
		super();
	}

	/**
	 * @see AbstractBuildActivityFolio#getDefaultOutputTypeId()
	 */
	@Override
	public int getDefaultOutputTypeId() {
		return 22;
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return false;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			folio.setTitle(ConvertUtils.getFormat("LITERAL", ConvertUtils.getStringSafely(form.get("reportTitle"), "Activity Summary")));
			int[] sortBy = ConvertUtils.convertRequired(int[].class, form.get("sortBy"));
			int[] showValues = ConvertUtils.convertRequired(int[].class, form.get("showValues"));
			int[] showPercents = ConvertUtils.convert(int[].class, form.get("showPercents"));
			int[] showAverages = ConvertUtils.convert(int[].class, form.get("showAverages"));
			// logic for selecting customer, region and terminal
			Holder<Boolean> hasSubregionsHolder = new Holder<Boolean>();
			addFiltersForLocation(form, folio, hasSubregionsHolder);

			Set<Integer> sortByClean = new LinkedHashSet<Integer>();
			for(int sb : sortBy) {
				if(sb == 13 && !Boolean.FALSE.equals(hasSubregionsHolder.getValue())) {
					sortByClean.add(-sb);
					folio.setDirective("dynamic-labels", "true");
				}
				if(sb > 0)
					sortByClean.add(sb);
			}
			boolean itemGranularity = false;
			boolean rollup = !hasActivityRefField(sortBy);
			if (rollup) {
				form.set("rollup", "Rollup");
			} ;
			int i = 0;
			for(int sb : sortByClean) {
				switch(sb) {
					case 11:
						itemGranularity = true;
						break;
				}
				int grouping = 1 + sortByClean.size() - i;
				if(grouping > 5)
					grouping = 5;
				else if(grouping < 2)
					grouping = 2;
				addSortByPillar(form, folio, sb, grouping, null);
				i++;
			}
			if(showPercents != null)
				Arrays.sort(showPercents);
			boolean needCurrency = false;
			for(int sv : showValues) {
				boolean showPercent = (showPercents != null && Arrays.binarySearch(showPercents, sv) >= 0);
				addShowValuePillars(form, folio, sv, itemGranularity, showPercent, null);
				if(sv == 1)
					needCurrency = true;
			}
			if(showAverages != null)
				for(int sa : showAverages) {
					addShowAveragePillar(form, folio, sa, itemGranularity, null);
					if(sa == 1)
						needCurrency = true;
				}
			if(needCurrency)
				addPillars(folio, "currency");
			addFilter(form, folio, "tranType" + form.getStringSafely("rollup", ""));
			addFilterForRangeType(form, folio);
			// fix up tranType
			fixArrayParam(form);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}
}
