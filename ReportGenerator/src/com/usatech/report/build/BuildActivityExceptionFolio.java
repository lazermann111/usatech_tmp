package com.usatech.report.build;
import javax.servlet.ServletException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Scene;
import simple.io.LoadingException;
import simple.lang.Holder;
import simple.servlet.InputForm;
import simple.text.StringUtils;

public class BuildActivityExceptionFolio extends AbstractBuildActivityFolio {
	public BuildActivityExceptionFolio() {
		super();
	}

	@Override
	public Folio getFolio(ExecuteEngine executeEngine, InputForm form, Scene scene) throws ServletException {
		try {
			LazyHolder.configSource.reload();
		} catch(LoadingException e) {
			throw new ServletException("Could not load configuration", e);
		}
		try {
			Folio folio = executeEngine.getDesignEngine().getFolio(2002, form.getTranslator());

			int maxRowPerPageFromForm = ConvertUtils.getIntSafely(form.getAttribute("max-rows-per-page"), -1);
			if(maxRowPerPageFromForm > -1) {
				folio.setDirective("max-rows-per-page", String.valueOf(maxRowPerPageFromForm));
			} else {
				folio.setDirective("max-rows-per-page", String.valueOf(maxRowPerPage));
			}
			return folio;
		} catch(DesignException e) {
			throw new ServletException("Could not create new folio", e);
		}
	}
	/**
	 * @see AbstractBuildActivityFolio#getDefaultOutputTypeId()
	 */
	@Override
	public int getDefaultOutputTypeId() {
		return 22;
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return false;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			Pillar[] pillars = folio.getPillars();
			Pillar regionPillar = pillars[1];
			//Pillar parentRegionPillar = null;
			String tranType = ConvertUtils.getString(form.get("params.tranType"), false);
			// Date beginDate = ConvertUtils.convertRequired(Date.class, form.get("params.beginDate"));
			// Date endDate = ConvertUtils.convertRequired(Date.class, form.get("params.endDate"));
			if(!StringUtils.isBlank(tranType) /* && not all*/)
				form.set("params.tranType", tranType);
			// logic for selecting customer, region and terminal
			Holder<Boolean> hasSubregionsHolder = new Holder<>();
			addFiltersForLocation(form, folio, hasSubregionsHolder);
			if(!Boolean.FALSE.equals(hasSubregionsHolder.getValue())) {
				Pillar[] ps = adjustSortByPillars(addPillars(folio, "parentRegion"), 2, null);
				int insertIndex = regionPillar.getIndex();
				for(int i = ps.length - 1; i >= 0; i--)
					folio.movePillar(ps[i].getIndex(), insertIndex);
				//parentRegionPillar = ps[0];
				folio.setDirective("dynamic-labels", "true");
			}
			
		if(!StringUtils.isBlank(tranType)){
			addFilter(form, folio, "tranType");
		}

		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		} 
	}
}
