package com.usatech.report.build;

import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Pillar;
import simple.falcon.engine.Scene;
import simple.io.LoadingException;
import simple.io.Log;
import simple.lang.Holder;
import simple.servlet.InputForm;
import simple.text.StringUtils;

/**
 * Builds Activity Rollup report
 *
 * @author bkrug
 *
 */
public class BuildActivityRollupFolio extends AbstractBuildActivityFolio {
	private static final Log log = Log.getLog();
	public BuildActivityRollupFolio() {
		super();
	}

	@Override
	public Folio getFolio(ExecuteEngine executeEngine, InputForm form, Scene scene) throws ServletException {
		try {
			LazyHolder.configSource.reload();
		} catch(LoadingException e) {
			throw new ServletException("Could not load configuration", e);
		}
		try {
			Folio folio = executeEngine.getDesignEngine().getFolio(969, form.getTranslator());

			int maxRowPerPageFromForm = ConvertUtils.getIntSafely(form.getAttribute("max-rows-per-page"), -1);
			if(maxRowPerPageFromForm > -1) {
				folio.setDirective("max-rows-per-page", String.valueOf(maxRowPerPageFromForm));
			} else {
				folio.setDirective("max-rows-per-page", String.valueOf(maxRowPerPage));
			}
			return folio;
		} catch(DesignException e) {
			throw new ServletException("Could not create new folio", e);
		}
	}
	/**
	 * @see AbstractBuildActivityFolio#getDefaultOutputTypeId()
	 */
	@Override
	public int getDefaultOutputTypeId() {
		return 22;
	}

	@Override
	public boolean keepAdditionalParamNames() {
		return false;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#updateFolio(simple.falcon.engine.Folio, simple.servlet.InputForm)
	 */
	@Override
	public void updateFolio(Folio folio, InputForm form, boolean customerUser) throws ServletException {
		try {
			Pillar[] pillars = folio.getPillars();
			Pillar customerPillar = pillars[0];
			Pillar regionPillar = pillars[1];
			Set<Pillar> detailPillars = new LinkedHashSet<>();
			for(int i = 2; i < 11; i++)
				detailPillars.add(pillars[i]);
			Pillar makePillar = pillars[6];
			Pillar currencyPillar = pillars[15];
			Pillar parentRegionPillar = null;
			String tranType = ConvertUtils.getString(form.get("params.tranType"), false);
			// Date beginDate = ConvertUtils.convertRequired(Date.class, form.get("params.beginDate"));
			// Date endDate = ConvertUtils.convertRequired(Date.class, form.get("params.endDate"));
			if(!StringUtils.isBlank(tranType) /* && not all*/)
				form.set("params.tranType", tranType);
			String[] preferencePillarLabels = getPreferencePillars(form);
			int[] sortBy = ConvertUtils.convert(int[].class, form.get("sortBy"));
			Set<Integer> sortByClean;
			// logic for selecting customer, region and terminal
			Holder<Boolean> hasSubregionsHolder = new Holder<>();
			addFiltersForLocation(form, folio, hasSubregionsHolder);
			if(!Boolean.FALSE.equals(hasSubregionsHolder.getValue())) {
				Pillar[] ps = adjustSortByPillars(addPillars(folio, "parentRegion"), 2, null);
				int insertIndex = regionPillar.getIndex();
				for(int i = ps.length - 1; i >= 0; i--)
					folio.movePillar(ps[i].getIndex(), insertIndex);
				parentRegionPillar = ps[0];
				folio.setDirective("dynamic-labels", "true");
			}

			if(sortBy != null && sortBy.length > 0) {
				sortByClean = new LinkedHashSet<>();
				for(int sortByIndex : sortBy) {
					switch(sortByIndex) {
						case 1:
							// add customer pillar (if user not a customer)
							if(!customerUser)
								sortByClean.add(sortByIndex);
							break;
						case 14:
						case 2:
							sortByClean.add(sortByIndex);
							break;
						case 13:
							if(!Boolean.FALSE.equals(hasSubregionsHolder.getValue()))
								sortByClean.add(-sortByIndex);
							sortByClean.add(sortByIndex);
							break;
						default:
							log.warn("Not adding grouping by " + getLinkFilterLabel(sortByIndex) + " because it is not supported");
					}
				}
			} else
				sortByClean = Collections.emptySet();
			Pillar clientPillar = null;
			if(preferencePillarLabels != null)
				for(String ppl : preferencePillarLabels) {
					Pillar[] preferencePillars;
					switch(ppl) {
						case "client":
							preferencePillars = adjustSortByPillars(addPillars(folio, ppl), 2, null);
							for(int i = preferencePillars.length - 1; i >= 0; i--) {
								folio.movePillar(preferencePillars[i].getIndex(), makePillar.getIndex());
								if(!sortByClean.contains(14))
									detailPillars.add(preferencePillars[i]);

							}
							clientPillar = preferencePillars[0];
							break;
						case "terminal":
						case "specifics":
							preferencePillars = adjustSortByPillars(addPillars(folio, ppl), 2, null);
							for(int i = preferencePillars.length - 1; i >= 0; i--) {
								folio.movePillar(preferencePillars[i].getIndex(), makePillar.getIndex());
								detailPillars.add(preferencePillars[i]);
							}
							break;
					}
				}
			
			int level = 3 + sortByClean.size();
			currencyPillar.setGroupingLevel(level--);
			// add other grouping pillars
			pillars = folio.getPillars();
			if(sortByClean.contains(1)) {
				customerPillar.setGroupingLevel(level--);
			}
			if(sortByClean.contains(-13)) {
				parentRegionPillar.setGroupingLevel(level--);
			}
			if(sortByClean.contains(13)) {
				regionPillar.setGroupingLevel(level--);
			}
			if(sortByClean.contains(14)) {
				if(clientPillar == null)
					clientPillar = adjustSortByPillars(addPillars(folio, "client"), level, null)[0];
				else
					clientPillar.setGroupingLevel(level);
				level--;
			}
			if(sortByClean.contains(2)) {
				for(Pillar pillar : detailPillars)
					if(pillar.getGroupingLevel() == 2)
						pillar.setGroupingLevel(level);
				if(!sortByClean.contains(1))
					customerPillar.setGroupingLevel(level);
				if(!sortByClean.contains(13))
					regionPillar.setGroupingLevel(level);
				if(parentRegionPillar != null && !sortByClean.contains(-13)) {
					parentRegionPillar.setGroupingLevel(level);
					parentRegionPillar.setLabelFormat(ConvertUtils.getFormat("NULL:Parent Region"));
				}
			}

			// extra filtering
			if(!StringUtils.isBlank(tranType))
				addFilter(form, folio, "tranTypeRollup");
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(DesignException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}
}
