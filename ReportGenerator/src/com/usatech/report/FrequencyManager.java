package com.usatech.report;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.usatech.layers.common.model.PayrollSchedule;
import com.usatech.layers.common.model.PayrollSchedule.PayrollScheduleType;

import simple.bean.ConvertException;
import simple.bean.DynaBean;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class FrequencyManager {
	
	private static final Log log = Log.getLog();

	protected static final Map<String,Integer> dateFieldNames = new HashMap<String, Integer>();
	static {
		dateFieldNames.put("YEAR", Calendar.YEAR);
		dateFieldNames.put("MONTH", Calendar.MONTH);
		dateFieldNames.put("WEEK", Calendar.WEEK_OF_YEAR);
		dateFieldNames.put("DAY", Calendar.DATE);
		dateFieldNames.put("HOUR", Calendar.HOUR_OF_DAY);
		dateFieldNames.put("MINUTE", Calendar.MINUTE);
		dateFieldNames.put("PAY_PERIOD", 0);
	}
	
	protected final Map<Integer,Frequency> frequencies = new HashMap<Integer,Frequency>();
	protected final Set<Integer> frequencyIds = Collections.unmodifiableSet(frequencies.keySet());
	
	public int loadBasicFrequencies(String callId) throws SQLException, DataLayerException, ConvertException {
		Results results = DataLayerMgr.executeQuery(callId, null);
		int cnt = 0;
		while(results.next()) {
			Integer fid = results.getValue("frequencyId", Integer.class);
			String dateFieldName = results.getValue("dateField", String.class);
			Integer amount = results.getValue("amount", Integer.class);
			Integer offset = results.getValue("offsetMs", Integer.class);
			if(dateFieldName == null)
				throw new ConvertException("Date Field not provided", String.class, dateFieldName);
			Integer dateField = dateFieldNames.get(dateFieldName.toUpperCase());
			if(amount == null)
				throw new ConvertException("Amount not provided", Integer.class, amount);
			if(dateField == null)
				throw new ConvertException("Date Field not recognized", Integer.class, dateFieldName);
			if (dateField == 0)
				frequencies.put(fid, new PayPeriodFrequency(PayrollSchedule.create(PayrollScheduleType.NONE)));
			else
				frequencies.put(fid, new BasicFrequency(dateField, amount, offset == null ? 0 : offset));
			cnt++;
		}
		return cnt;
	}
	
	public void clearFrequencies() {
		frequencies.clear();
	}
	
	public Frequency getFrequency(int frequencyId) {
		return frequencies.get(frequencyId);
	}
	
	public Frequency getFrequency(int frequencyId, DynaBean results) {
		Frequency frequency = getFrequency(frequencyId);
		if (frequency instanceof PayPeriodFrequency) {
			PayrollSchedule payrollSchedule = null;
			try {
				payrollSchedule = PayrollSchedule.loadFrom(results);
			} catch (Exception e) {
				log.warn("Failed to load PayrollSchedule from {0}", results, e);
			}
			if (payrollSchedule != null)
				return new PayPeriodFrequency(payrollSchedule);
		}
		
		return frequency;
	}
	
	public Set<Integer> getFrequencyIds() {
		return frequencyIds;
	}
}
