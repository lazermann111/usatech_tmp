package com.usatech.report;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.TransportAttrEnum;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.Results;
import simple.text.StringUtils;

public class DataExchangeGenerateTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			String fileName = step.getAttribute(CommonAttrEnum.ATTR_FILE_PATH, String.class, true);
			long[] itemIds = step.getAttribute(CommonAttrEnum.ATTR_ITEM_ID, long[].class, true);
			int dataExchangeType = step.getAttribute(CommonAttrEnum.ATTR_FILE_TYPE, int.class, true);
			boolean skipEmpty = step.getAttribute(CommonAttrEnum.ATTR_SKIP_EMPTY, boolean.class, true);
			Map<String, Object> params = new HashMap<>();
			params.put("itemIds", itemIds);

			// generate csv
			int dataCount = 0;
			boolean okay = false;
			Resource resource = getResourceFolder().getResource(fileName, ResourceMode.CREATE);
			try {
				try (Connection conn = DataLayerMgr.getConnection("REPORT")) {
					PreparedStatement ps = null;
					int parameterCount = 0;
					boolean header = true;
					try (Results results = DataLayerMgr.executeQuery(conn, "GET_DATA_EXCHANGE_ITEMS", params)) {
						results.addGroup("queryText");
						try (PrintWriter out = new PrintWriter(resource.getOutputStream())) {
							while(results.next()) {
								if(results.isGroupBeginning("queryText")) {
									String queryText = results.getValue("queryText", String.class);
									if(ps != null)
										ps.close();
									ps = conn.prepareStatement(queryText);
									parameterCount = ps.getParameterMetaData().getParameterCount();
									header = true;
								} else
									ps.clearParameters();
								long itemId = results.getValue("itemId", long.class);
								for(int i = 1; i <= parameterCount; i++)
									ps.setLong(i, itemId);
								try (ResultSet rs = ps.executeQuery()) {
									if(rs.isBeforeFirst()) {
										dataCount += StringUtils.writeCSV(rs, out, header);
										header = false;
									}
								}
							}
						}

						resource.setContentType("text/csv");
					} finally {
						if(ps != null)
							ps.close();
					}
					// save info / get unique file name
					params.put("dataExchangeType", dataExchangeType);
					params.put("resource", resource);
					params.put("processStatusCd", skipEmpty && resource.getLength() == 0 ? 'S' : 'P');
					try {
						DataLayerMgr.executeQuery(conn, "CREATE_DATA_EXCHANGE_FILE", params);
						conn.commit();
						okay = true;
					} finally {
						if(!okay)
							conn.rollback();
					}
				}
			} finally {
				if(okay && !(skipEmpty && resource.getLength() == 0))
					resource.release();
				else
					resource.delete();
			}
			log.info("Produced data exchange file " + params.get("dataExchangeFileId") + " with " + dataCount + " data rows and " + resource.getLength() + " bytes");
			if(skipEmpty && resource.getLength() == 0)
				return 1;
			// update result attributes
			step.setResultAttribute(CommonAttrEnum.ATTR_FILE_ID, params.get("dataExchangeFileId"));
			step.setResultAttribute(TransportAttrEnum.ATTR_FILE_KEY, resource.getKey());
			step.setResultAttribute(TransportAttrEnum.ATTR_FILE_NAME, resource.getName());
			step.setResultAttribute(TransportAttrEnum.ATTR_FILE_CONTENT_TYPE, resource.getContentType());
			// step.setResultAttribute(TransportAttrEnum.ATTR_FILE_PASSCODE, null);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(AttributeConversionException | IOException | DataLayerException | ConvertException e) {
			throw new ServiceException(e);
		}

		return 0;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

}
