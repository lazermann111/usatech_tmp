package com.usatech.report;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MapDynaBean;
import simple.db.DataLayerException;
import simple.io.Log;

import com.usatech.report.Frequency.DateRange;
/**
 * Class for unsent timed report.
 * @author yhe
 *
 */
public class RequestUnsentTimedReportTask extends AbstractRequestTask {
	private static final Log log = Log.getLog();
	
	private final FrequencyManager frequencyManager = new FrequencyManager();

	@Override
	protected void initializeProcessor() throws ServiceException {
		try {
			frequencyManager.loadBasicFrequencies("GET_FREQUENCIES");
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(ConvertException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	protected void resetProcessor() {
		frequencyManager.clearFrequencies();
	}

	@Override
	protected String getListCallId() {
		return "GET_TIMED_REPORTS";
	}

	@Override
	protected Object getListCallParams() {
		return null;
	}

	@Override
	protected String getProcessCd() {
		return "TIMED_REPORT_REQUEST";
	}

	@Override
	protected String getProcessDesc() {
		return "Unsent Timed Report Request";
	}

	@Override
	protected void handleItem(Map<String, Object> itemParams) {
		int userReportId;
		try {
			userReportId = ConvertUtils.getInt(itemParams.get("userReportId"));
		} catch(ConvertException e) {
			log.error("Could not convert userReportId", e);
			return;
		}
		Date lastEndDate;
		int frequencyId;
		double latency; // the number of days to wait to send
		int userId;
		String frequencyName;
		int reportId;
		Generator generator;
		String title;
		try {
			lastEndDate = ConvertUtils.convert(Date.class, itemParams.get("lastEndDate"));
			frequencyId = ConvertUtils.getInt(itemParams.get("frequencyId"));
			latency = ConvertUtils.getDouble(itemParams.get("latency"), 0.0);
			userId = ConvertUtils.getInt(itemParams.get("userId"));
			frequencyName = ConvertUtils.convert(String.class, itemParams.get("frequencyName"));
			reportId = ConvertUtils.getInt(itemParams.get("reportId"));
			generator = ConvertUtils.convertRequired(Generator.class, itemParams.get("generatorId"));
			title = ConvertUtils.convert(String.class, itemParams.get("title"));
		} catch(ConvertException e) {
			log.error("Could not set convert properties of user report " + userReportId, e);
			return;
		}
		Frequency frequency = frequencyManager.getFrequency(frequencyId, new MapDynaBean(itemParams));
		if(frequency == null) {
			log.warn("Unknown Frequency: '" + frequencyId + "' - skipping user_report_id = " + userReportId);
		} else {
			DateRange dateRange = null;
			try {
				if(frequency instanceof PayPeriodFrequency){
					dateRange = frequency.nextDateRange(lastEndDate);
				}else{
					dateRange=adjustDateRange(frequencyId, (BasicFrequency) frequency, lastEndDate);
				}
			} catch (Exception e) {
				log.error("Could not get next date range for end date {0} and frequency {1}", lastEndDate, frequency, e);
				return;
			}
			Date now = new Date(System.currentTimeMillis() - (long) (24 * 60 * 60 * 1000 * latency)); // give
			// specified leeway
			while(dateRange.endDate.before(now)) {
				log.info("Generating Timed Report " + userReportId + " for " + dateRange.beginDate + " to " + dateRange.endDate);
				try {
					reportRequestFactory.generateReport(ReportScheduleType.TIMED, userReportId, userId, reportId, generator, title, null, dateRange.beginDate, dateRange.endDate, frequencyName, null, "");
				} catch(ServiceException e) {
					// thrown by reportRequestFactory.generateReport
					log.error("Could not publish timed report request for user report " + userReportId, e);
				}
				dateRange = frequency.nextDateRange(dateRange);
			}
		}
	}
	
	public static DateRange adjustDateRange(int frequencyId, BasicFrequency frequency, Date lastEndDate){
		DateRange dateRange;
		dateRange=frequency.nextDateRange(lastEndDate);
		Date adjustDate=null;
		switch(frequency.getDateField()) {
			//daily
			case Calendar.DATE:
				adjustDate=DateUtils.addDays(lastEndDate, -1);
				break;
			//weekly
			case Calendar.WEEK_OF_YEAR:
				adjustDate=DateUtils.addDays(lastEndDate, -7);
				break;
			//monthly
			case Calendar.MONTH:
				adjustDate=DateUtils.addMonths(lastEndDate, -1);
				break;
			//yearly
			case Calendar.YEAR:
				adjustDate=DateUtils.addYears(lastEndDate, -1);
				break;
		}
		if(adjustDate!=null){
			DateRange dateRangeAdjusted = frequency.nextDateRange(adjustDate);
			if(lastEndDate.before(dateRangeAdjusted.endDate)){
				dateRange=dateRangeAdjusted;
			}
		}
		return dateRange;
	}
}
