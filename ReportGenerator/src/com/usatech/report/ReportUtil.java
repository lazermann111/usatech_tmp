package com.usatech.report;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.text.RegexUtils;

public class ReportUtil {
	public static final SimpleDateFormat todayDateFormat = new SimpleDateFormat("yyyyMMdd");

	public static Date getTimeFromNow(int day) {
		return new Date(System.currentTimeMillis() + day * 24 * 60 * 60 * 1000);
	}

	public static String getCurrentDate() {
		return todayDateFormat.format(new Date());
	}

	public static String appendDateDir(String fileName) {
		return getCurrentDate() + "/" + fileName;
	}

	public static ReportingUser loadReportingUser(long userId) throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<String, Object>();
		ReportingUser user = new ReportingUser();
		user.setUserId(userId);
		params.put("user", user);
		DataLayerMgr.executeCall("POPULATE_USER", params, false);
		if(!user.isInternal()) {
			long customerActiveBankAccountCount = ConvertUtils.getLong(params.get("customerActiveBankAccountCount"), 0);
			user.setMissingBankAccount(customerActiveBankAccountCount <= 0);
		}
		return user;
	}

	/**
	 * This method is taken from com.usatech.usalive.hybrid.HybridServlet.
	 *
	 * @param userPrivs
	 * @param userType
	 * @return userGroupIds
	 */
	public static long[] retrieveUserGroups(int[] userPrivs, int userType) {
        /*
        USER_TYPE_ID  USER_TYPE                      DESCRIPTION
        ------------- ------------------------------ --------------------------------------------------
        5             Application Process            Application process that can update the database
        3             System Admin                   System Administrator
        2             Accounting                     Accounting User
        1             Customer Service               Customer Service User
        4             Management                     Management User
        7             Development                    Systems Analyst/Programmer
        6             System Process                 System process that can update the database
        8             Customer                       A USA Tech Customer (for web reports)
        9             Sales                          Sales Force
        */
    	Set<Long> userGroups = new HashSet<Long>(10);
        switch(userType) {
            case 3: userGroups.add(1L); break;
            case 2: userGroups.add(2L); break;
            case 1: userGroups.add(3L); break;
            case 4: userGroups.add(4L); break;
            case 9: userGroups.add(5L); break;
            case 8: userGroups.add(8L); break;
        }

        userGroups.add(100L); // via report.user_terminal
        userGroups.add(120L); // via report.user_customer_bank
        Arrays.sort(userPrivs);
        if(Arrays.binarySearch(userPrivs, 7) >= 0 || Arrays.binarySearch(userPrivs, 6) >= 0) {
        	switch(userType) {
        		case 8:
        			userGroups.add(101L); break; // all terminals where customer_id is equal
        		case 3:
                case 2:
                case 1:
                case 4:
                case 9:
                	userGroups.add(102L); break; // all terminals
            }
        }
        if(Arrays.binarySearch(userPrivs, 11) >= 0) {
        	switch(userType) {
        		case 8:
        			userGroups.add(121L); break; // all bank accts where customer_id is equal
        		case 3:
                case 2:
                case 1:
                case 4:
                case 9:
                	userGroups.add(122L); break; // all bank accts
            }
        }
        long[] tmp = new long[userGroups.size()];
        Iterator<Long> iter = userGroups.iterator();
        for(int i = 0; i < tmp.length; i++)
        	tmp[i] = iter.next();
        return tmp;
    }
	

	protected static String getFileExtension(String contentType) {
		String[] ct = RegexUtils.match("([^/]+)[/]([^;]+)(?:[;](.+))?", contentType, null);
		if (ct.length < 3)
			return null; // unexpected
		if (ct[1].equals("text") && ct[2].equals("plain"))
			return "txt";
		return ct[2];
	}
	
	public static String getBasicStyle() {
		return new StringBuilder()
			.append("<style>")
			.append("<!--")
			.append("body {margin:0; padding:0; font-family:Verdana, Arial, Helvetica; font-size:12px; color:#333333;} ")
			.append("table {padding:0px; border:0px; border-collapse:collapse;} ")
			.append("th {background:#d6d6d6; font-weight:bold; font-size:12px; text-align:center; padding:3px 5px 3px 5px; border:1px solid #a9a9a9; margin:5px 0px 2px 0px;} ")
			.append("td {padding:3px 5px 3px 5px; border:1px solid #a9a9a9 !important; font-size:12px;} ")
			.append(".row0 {background-color:#EEEEEE;} ")
			.append("-->")
			.append("</style>")
			.toString();
	}
}
