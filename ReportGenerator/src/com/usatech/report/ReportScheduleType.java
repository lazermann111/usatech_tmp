/**
 * (C) USA Technologies 2011
 */
package com.usatech.report;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

import com.usatech.transport.TransportReason;

/**
 * This enumeration matches the EXPORT_TYPE table in RDB, REPORT.REPORTS BATCH_TYPE_ID=EXPORT_TYPE_ID
 *
 */
public enum ReportScheduleType {
	TIMED(0, "Timed", TransportReason.TIMED_REPORT), 
	DAILY_EXPORT(1, "Daily Export", TransportReason.BATCH_REPORT), 
	DEX(2, "DEX", TransportReason.BATCH_REPORT), 
	PAYMENT(3, "Payment", TransportReason.BATCH_REPORT), 
	PENDING_PAYMENT(4, "Pending Payment", TransportReason.BATCH_REPORT),
	RDC(5, "RDC", TransportReason.BATCH_REPORT),
	EVENT(6, "Device Event", TransportReason.ALERT),
	FILE_TRANSFER_EVENT(7, "File Transfer Event", TransportReason.ALERT),
	CUSTOM_FILE_UPLOAD(8, "Custom File Upload", TransportReason.CUSTOM_FILE_UPLOAD),
	CONDITION(9, "Condition based", TransportReason.CONDITION),
	SINGLE_TXN(10, "Single Transaction", TransportReason.SINGLE_TXN);

	protected final static EnumIntValueLookup<ReportScheduleType> lookup = new EnumIntValueLookup<ReportScheduleType>(ReportScheduleType.class);
    public static ReportScheduleType getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
	private final int value;
	private final String description;
	private final TransportReason transportReason;
	private ReportScheduleType(int value, String description, TransportReason transportReason) {
		this.value = value;
		this.description = description;
		this.transportReason = transportReason;
	}
	public String getDescription() {
		return description;
	}
	public int getValue() {
		return value;
	}
	public TransportReason getTransportReason() {
		return transportReason;
	}
}