/**
 * 
 */
package com.usatech.report;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.results.Results;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

/**
 * Request for batchTypeId=8 when custom file comes in from file transfer.
 * @author yhe
 *
 */
public class RequestCustomFileUploadBatchTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	
	// -- configuration fields
	protected ReportRequestFactory reportRequestFactory;
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ReportScheduleType batchType=ReportScheduleType.CUSTOM_FILE_UPLOAD;
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		long fileTransferId=-1;
		try {
			fileTransferId=ConvertUtils.getLong(attributes.get("fileTransferId"));
			params.put("terminalId", attributes.get("terminalId"));
			params.put("batchId", fileTransferId);
			params.put("batchType", batchType);
			params.put("createDate", attributes.get("fileTransferTs")); 
			String deviceSerialCd=ConvertUtils.getStringSafely(attributes.get("deviceSerialCd"));
			Results reportResults = DataLayerMgr.executeQuery("GET_UNSENT_REPORTS_FOR_CUSTOM_FILE_UPLOAD", params, false);
			while(reportResults.next()) {
				ReportInstance rpt = new ReportInstance();
				reportResults.fillBean(rpt);
				log.info("Requesting Custom File Upload Report " + rpt.userReportId + " for " + fileTransferId);
				reportRequestFactory.generateReport(batchType, rpt.userReportId, rpt.userId, rpt.reportId, Generator.getByValue(rpt.generatorId), rpt.title, fileTransferId, null, null, batchType.getDescription(), null, deviceSerialCd, null);
			}
		} catch(BeanException e) {
			throw new RetrySpecifiedServiceException("Could not convert report request for fileTransferId " + fileTransferId, e, WorkRetryType.NO_RETRY);
		} catch(InvalidIntValueException e) {
			throw new RetrySpecifiedServiceException("Could not convert generator id for fileTransferId " + fileTransferId, e, WorkRetryType.NO_RETRY);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException("Could not convert generator id for fileTransferId " + fileTransferId, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not generate custom file upload report request for fileTransferId " + fileTransferId, e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not generate custom file upload report request for fileTransferId " + fileTransferId, e);
		}
		return 0;
	}
	
	public ReportRequestFactory getReportRequestFactory() {
		return reportRequestFactory;
	}

	public void setReportRequestFactory(ReportRequestFactory reportRequestFactory) {
		this.reportRequestFactory = reportRequestFactory;
	}
}

