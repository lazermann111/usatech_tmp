package com.usatech.report;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.activation.DataSource;

import simple.io.IOUtils;
import simple.io.resource.Resource;

public abstract class AbstractRetrieveFileTask extends AbstractGenerateTask {
	protected AbstractRetrieveFileTask(Generator generator) {
		super(generator);
	}
	@Override
	protected Resource writeResource(String title, int userId, Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception {
		DataSource ds = generateDataSource(params, standbyAllowance);
		Resource resource = getResourceInstance(ds.getName());
		resource.setContentType(ds.getContentType());
		OutputStream out;
		try {
			out = resource.getOutputStream();
			IOUtils.copy(ds.getInputStream(), out);
			out.close();
		} catch(IOException e) {
			resource.release();
			throw e;
		} catch(RuntimeException e) {
			resource.release();
			throw e;
		} catch(Error e) {
			resource.release();
			throw e;
		}

		return resource;
	}
	
	/**
	 * @param params
	 * @param standbyAllowance
	 *            TODO
	 * @return
	 */
	protected abstract DataSource generateDataSource(Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception;
}
