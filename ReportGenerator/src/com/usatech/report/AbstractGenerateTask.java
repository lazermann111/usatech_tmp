package com.usatech.report;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

import simple.app.RetryPolicy;
import simple.app.ScheduledRetryServiceException;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.util.FilterMap;

public abstract class AbstractGenerateTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected int daysReportWillExpire = 7;
	protected final Generator generator;


	protected AbstractGenerateTask(Generator generator) {
		super();
		this.generator = generator;
	}

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		
		Long requestId;
		StandbyAllowance standbyAllowance;
		long maxStandbyWait;
		try {
			requestId = taskInfo.getStep().getAttribute(GenerateAttrEnum.ATTR_GEN_REQUEST_ID, Long.class, false);
			standbyAllowance = taskInfo.getStep().getAttributeDefault(GenerateAttrEnum.ATTR_USE_STANDBY, StandbyAllowance.class, StandbyAllowance.NONE);
			maxStandbyWait = taskInfo.getStep().getAttributeDefault(GenerateAttrEnum.ATTR_MAX_STANDBY_WAIT, Long.class, 0L);
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		}

		if(requestId != null && (standbyAllowance == StandbyAllowance.WAIT_ON_REQUEST) || taskInfo.isRedelivered()) {
			Map<String, Object> params = new HashMap<String, Object>();
			if(standbyAllowance == StandbyAllowance.WAIT_ON_REQUEST) {
				try (Connection conn = DataLayerMgr.getConnection("REPORT_STANDBY")) {
					try {
						DataLayerMgr.selectInto(conn, "GET_REQUEST_STATUS", attributes, params);
					} catch(NotEnoughRowsException e) {
						if(maxStandbyWait > 0L) {
							long maxDelay = taskInfo.getEnqueueTime() + maxStandbyWait - System.currentTimeMillis();
							if(maxDelay > 0L) {
								// NOTE: we may want to get the staleness of the standby and use that in calculating the redeliveryDelay
								// DataLayerMgr.selectInto(conn, "GET_STALENESS", attributes, params);
								// long staleness = ConvertUtils.getLongSafely(params.get("staleness"), maxDelay);
								throw new ScheduledRetryServiceException("Request not propagated to standby yet", new RetryPolicy(20, Math.min(maxDelay, maxStandbyWait / 10L), 1));
							}
						}
						standbyAllowance = StandbyAllowance.NONE; // Don't use standby
						try {
							DataLayerMgr.selectInto("GET_REQUEST_STATUS", attributes, params);
						} catch(SQLException | DataLayerException | BeanException e1) {
							log.warn("Could not get status for request " + requestId + "; generating report anyway", e1);
						}
					}
				} catch(SQLException | DataLayerException | BeanException e) {
					throw new ServiceException("Could not get status for request " + requestId + "", e);
				}
			} else
				try {
					DataLayerMgr.selectInto("GET_REQUEST_STATUS", attributes, params);
				} catch(SQLException | DataLayerException | BeanException e) {
					log.warn("Could not get status for request " + requestId + "; generating report anyway", e);
				}
			try {
				int requestStatus = ConvertUtils.getInt(params.get("requestStatus"));
				switch(requestStatus) {
					case -1: // Cancelled
					case 3: // Failed
						log.info("Request " + requestId + " is cancelled; not generating report.");
						return 5;
					case 2:
						log.info("Request " + requestId + " has been refreshed; not generating report.");
						return 5;
					case 1:
						log.info("Request " + requestId + " is already generated. Passing on to next step.");
						params.remove("requestStatus");
						taskInfo.getStep().getResultAttributes().putAll(params);
						return 0;
				}
			} catch(ConvertException e) {
				log.warn("Could not convert status for request " + requestId + "; generating report anyway", e);
			}
		}
		try {
			generateReport(attributes, taskInfo, standbyAllowance);
			return 0;
		} catch(ServiceException e) {
			throw e;
		} catch(Exception e){
			throw new ServiceException("GenerateReportComponent failed for parameters: " + new FilterMap<Object>(attributes, "report.", null),e);
		}
	}

	protected Connection getReportConnection(StandbyAllowance standbyAllowance) throws SQLException, DataLayerException {
		String dsn;
		switch(standbyAllowance) {
			case ALLOW:
			case WAIT_ON_REQUEST:
				dsn = "REPORT_STANDBY";
				break;
			default:
				dsn = "REPORT";
		}
		log.info("Using data source " + dsn);
		return DataLayerMgr.getConnection(dsn);
	}

	protected Resource getResourceInstance(String fileName) throws IOException{
		return resourceFolder.getResource(ReportUtil.appendDateDir(fileName), ResourceMode.CREATE);
	}

	protected abstract Resource writeResource(String title, int userId, Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception;
	
	protected void generateReport(Map<String, Object> attributes, MessageChainTaskInfo taskInfo, StandbyAllowance standbyAllowance) throws Exception {
		generateReport(attributes, taskInfo.getStep().getResultAttributes(), standbyAllowance);
	}

	protected void generateReport(Map<String, Object> attributes, Map<String, Object> resultAttributes, StandbyAllowance standbyAllowance) throws Exception {
		Generator generator = ConvertUtils.convert(Generator.class, attributes.get("request.generatorId"));
		if(generator != this.generator) {
			throw new ConvertException("Report Request for Generator " + generator + " was sent to the queue for " + this.generator, null, generator);
		}
		String title = ConvertUtils.getString(attributes.get("request.title"), "report");
		int userId = ConvertUtils.getInt(attributes.get("request.userId"));
		Integer basicReportId = ConvertUtils.convert(Integer.class, attributes.get("report.basicReportId"));
		Map<String, Object> params = new LinkedHashMap<>();
		for(Map.Entry<String, Object> entry : attributes.entrySet())
			if(entry.getKey() != null && entry.getKey().startsWith("report.") && !GenerateAttrEnum.ATTR_MAX_STANDBY_WAIT.getValue().equals(entry.getKey()) && !GenerateAttrEnum.ATTR_USE_STANDBY.getValue().equals(entry.getKey()))
				params.put(entry.getKey().substring(7), entry.getValue());
		if(basicReportId != null && basicReportId != 0) {
			params = new FilterMap<Object>(readBasicParameters(basicReportId, params, userId, title), "report.", null);
			title = ConvertUtils.getString(params.get("title"), title);
		}
		Resource resource = writeResource(title, userId, params, standbyAllowance);
		store(attributes, resultAttributes, resource, 0, 0);
	}
	
	protected Map<String, Object> readBasicParameters(int basicReportId, Map<String, Object> params, int userId, String title) throws ConvertException, SQLException, DataLayerException, BeanException, InvalidIntValueException, ServiceException {
		Long batchId = ConvertUtils.convert(Long.class, params.get("batchId"));
		Date beginDate = ConvertUtils.convert(Date.class, params.get("beginDate"));
		Date endDate = ConvertUtils.convert(Date.class, params.get("endDate"));
		String frequencyName = ConvertUtils.convert(String.class, params.get("frequencyName"));
		String deviceSerialCd = ConvertUtils.convert(String.class, params.get("deviceSerialCd"));
		String qualifier = ConvertUtils.convert(String.class, params.get("qualifier"));
		ReportScheduleType scheduleType = ConvertUtils.convert(ReportScheduleType.class, params.get("batchType"));
		long[] terminalIds = ConvertUtils.convert(long[].class, params.get("terminalIds"));
		long[] regionIds = ConvertUtils.convert(long[].class, params.get("regionIds"));
		long[] customerIds = ConvertUtils.convert(long[].class, params.get("customerIds"));
		Map<String, Object> reportParams = new HashMap<>();
		ReportRequestFactory.generateReportParameters(userId, basicReportId, scheduleType, batchId, beginDate, endDate, frequencyName, deviceSerialCd, qualifier, terminalIds, regionIds, reportParams, title, params,customerIds);
		return reportParams;
	}

	public void store(Map<String, Object> attributes, Map<String, Object> resultAttributes, Resource resource, int pageIndex, int maxPageId) throws ConvertException,
	SQLException, DataLayerException {
		Map<String, Object> generateReportInsMap = new HashMap<String, Object>(10);
		resultAttributes.put("file.fileName", resource.getName());
		resultAttributes.put("file.fileKey", resource.getKey());
		resultAttributes.put("file.fileContentType", resource.getContentType());
		long expiration = System.currentTimeMillis() + daysReportWillExpire * 24 * 60 * 60 * 1000L;
		resultAttributes.put("file.fileExpiration", expiration);
		generateReportInsMap.putAll(resultAttributes);
		generateReportInsMap.put("file.fileLength", resource.getLength());
		generateReportInsMap.put("pageId", pageIndex);
		generateReportInsMap.put("file.description", "Generated by Report Generator.");
		String filePasscode = RandomStringUtils.randomAlphanumeric(30);
		generateReportInsMap.put("filePasscode", filePasscode);
		generateReportInsMap.put("generateDate", System.currentTimeMillis());
		generateReportInsMap.put("reportSentId", attributes.get("reportSentId"));
		// for usalive requestId
		generateReportInsMap.put("requestId", attributes.get("request.requestId"));
		int fileId=ConvertUtils.getIntSafely(resultAttributes.get("transport.fileId"),-1);
		if(fileId>0){
			generateReportInsMap.put("fileId", fileId);
		}
		generateReportInsMap.put("maxPageId", maxPageId);
		int masterUserId = ConvertUtils.getInt(attributes.get("request.masterUserId"));
		generateReportInsMap.put("masterUserId", masterUserId);
		fileId=generateReportIns(generateReportInsMap);
		resultAttributes.put("transport.fileId", fileId);
		resultAttributes.put("transport.filePasscode", filePasscode);
		resultAttributes.put("file.pageId", pageIndex);
	}
	

	/**
	* Method to insert STORED_FILE info.
	* @return
	* @throws ConvertException
	* @throws SQLException
	* @throws DataLayerException
	*/
	public int generateReportIns(Map<String, Object> paramMap) throws ConvertException, SQLException,DataLayerException{
		DataLayerMgr.executeUpdate("STORED_FILE_INS_REPORT_SENT_UPD", paramMap,true);
		int fileId=ConvertUtils.getInt(paramMap.get("fileId"));
		return fileId;
	}

	public int getDaysReportWillExpire() {
		return daysReportWillExpire;
	}

	public void setDaysReportWillExpire(int daysReportWillExpire) {
		this.daysReportWillExpire = daysReportWillExpire;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public Generator getGenerator() {
		return generator;
	}
}
