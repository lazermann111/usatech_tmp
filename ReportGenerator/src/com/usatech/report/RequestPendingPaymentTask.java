package com.usatech.report;


import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.QuartzCronScheduledJob;

@DisallowConcurrentExecution
public class RequestPendingPaymentTask extends QuartzCronScheduledJob {
	protected static ReportRequestFactory reportRequestFactory;
	private static final Log log = Log.getLog();
	
	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {
		try {
			JobDataMap quartzJobMap = context.getMergedJobDataMap();
			Results reportResults = DataLayerMgr.executeQuery("GET_PENDING_PAYMENT_REPORT_REQUEST", new Object[] { quartzJobMap.get("batchTypeId"),quartzJobMap.get("reportName"),quartzJobMap.get("minAccountBalance")}, false);
			while(reportResults.next()) {
				ReportInstance rpt = new ReportInstance();
				reportResults.fillBean(rpt);
				log.info("Requesting Pending Payment " + rpt.userReportId + " for " + rpt.userId);
				ReportScheduleType batchType=ReportScheduleType.getByValue(ConvertUtils.getInt(quartzJobMap.get("batchTypeId")));
				reportRequestFactory.generateReport(batchType, rpt.userReportId, rpt.userId, rpt.reportId, Generator.getByValue(rpt.generatorId), rpt.title, ConvertUtils.getLong(reportResults.get("customerBankId")), null, null, batchType.getDescription(), null, "");
			}
		} catch(Exception e) {
			log.error("Could not generate pending payment request", e);
		}

	}

	public static ReportRequestFactory getReportRequestFactory() {
		return reportRequestFactory;
	}

	public static void setReportRequestFactory(
			ReportRequestFactory reportRequestFactory) {
		RequestPendingPaymentTask.reportRequestFactory = reportRequestFactory;
	}

}
