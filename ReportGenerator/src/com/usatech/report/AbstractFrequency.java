package com.usatech.report;

import java.util.Calendar;
import java.util.Date;

public abstract class AbstractFrequency implements Frequency {

	public DateRange nextDateRange(Date prevDate) {
		DateRange range = new DateRange();
		range.beginDate = prevDate;
		range.endDate = prevDate;
		return nextDateRange(range);
	}

	public DateRange nextDateRange(DateRange dateRange) {
		dateRange.calendar.setTime(dateRange.beginDate);
		setNextBeginDate(dateRange.calendar);
		dateRange.beginDate = dateRange.calendar.getTime();
		setNextBeginDate(dateRange.calendar);
		dateRange.calendar.add(java.util.Calendar.SECOND,-1); // reports are inclusive so use one second less than full interval
		dateRange.endDate = dateRange.calendar.getTime();
		return dateRange;
	}

	protected abstract void setNextBeginDate(Calendar calendar) ;
}
