/*
 * ReportParameter.java
 *
 * Created on December 11, 2001, 4:35 PM
 */

package com.usatech.report;

/**
 *
 * @author  bkrug
 * @version
 */
public class Parameter extends Choice implements Cloneable {	
	protected java.util.List choices = null;
	protected java.util.Map map = null;
	
	public Parameter(String label, Object value) {
		super(label,value);
	}
	
	public Choice addChoice(String label, Object value) {
		return addChoice(new Choice(label,value));
	}	
	
	public Choice addChoice(String label, Object value, String shortLabel) {
		return addChoice(new Choice(label,value, shortLabel));
	}
	public Choice addChoice(int index,String label, Object value) {
		return addChoice(index,new Choice(label,value));
	}	
	
	public Choice addChoice(int index,String label, Object value, String shortLabel) {
		return addChoice(index,new Choice(label,value, shortLabel));
	}
	public Choice addChoice(Choice c) {
		return addChoice(choices == null ? 0 : choices.size(),c);
	}
	
	public Choice addChoice(int index, Choice c) {
		if(choices == null) {
			choices = new java.util.ArrayList();
			map = new java.util.HashMap();
		}
		if(c.valueIsArray) {
			Object array = c.getValue();
			int len =  java.lang.reflect.Array.getLength(array);
			for(int i = 0; i < len;i++) {
				Object elem = java.lang.reflect.Array.get(array,i);
				if(!map.containsKey(elem)) {
					map.put(elem,c);
				}
			}
		}
		if(!map.containsKey(c.getValue())) { //only add if its unique
			map.put(c.getValue(),c);
			choices.add(index,c);
		}
		return c;
	}
	
	public Choice[] getChoices() {
		Choice[] retVal = new Choice[choices.size()];
		return (Choice[])choices.toArray(retVal);
	}
	
	public Choice findChoice(Object value) {
		if(choices == null || choices.size() == 0) return null;
		Choice c = (Choice) map.get(value);
		if(c == null) System.out.println("Could not find " + value + " in " + map);
		return c;
	}
	
	public String getChoiceLabel(Object value) {
		Choice c = findChoice(value);
		return (c == null ? String.valueOf(value) : c.getLabel());
	}
	
	public String getChoiceShortLabel(Object value) {
		Choice c = findChoice(value);
		return (c == null ? String.valueOf(value) : c.getShortLabel());
	}
	
	public void configureComboBox(javax.swing.JComboBox combo) {
		combo.removeAllItems();
		for(int i = 0; i < choices.size(); i++)	combo.addItem(choices.get(i));
	}
	
	public String generateSelectHTML(String selectAttributes, Object selectedChoice) {
		return generateSelectHTML(selectAttributes, -1, selectedChoice);
	}
	
	public String generateSelectHTML(String selectAttributes, int defaultSelected) {
		return generateSelectHTML(selectAttributes, defaultSelected, null);
	}
	
	public String generateSelectHTML(String name, String selectAttributes, Object selectedChoice) {
		return generateSelectHTML(name, selectAttributes, -1, selectedChoice);
	}
	
	public String generateSelectHTML(String name, String selectAttributes, int defaultSelected) {
		return generateSelectHTML(name, selectAttributes, defaultSelected, null);
	}
	
	protected String generateSelectHTML(String selectAttributes, int defaultSelected, Object selectedChoice) {
		return generateSelectHTML(getValueString(), selectAttributes, defaultSelected, selectedChoice);
	}	
	
	protected String generateSelectHTML(String name, String selectAttributes, int defaultSelected, Object selectedChoice) {
		StringBuffer sb = new StringBuffer();
		sb.append("<SELECT name=\"");
		sb.append(name);
		sb.append('"');
		if(selectAttributes != null && selectAttributes.length() > 0) {
			sb.append(" ");
			sb.append(selectAttributes);
		}
		sb.append(">");
		for(int i = 0; i < choices.size(); i++) {
			Choice c = (Choice) choices.get(i);
			sb.append("<OPTION value=\"");
			sb.append(c.getValueString());
			sb.append('"');
			if(defaultSelected == i + 1 || (defaultSelected == -1 && c.equals(selectedChoice))) sb.append(" selected");
			sb.append(">");
			sb.append(c.getLabel());
			sb.append("</OPTION>");
		}
		sb.append("</SELECT>");
		return sb.toString();
	}
	
	public Object clone() {
		try {
			Parameter p = (Parameter) super.clone();
			p.choices = new java.util.ArrayList(choices);
			p.map = new java.util.HashMap(map);
			return p;
		} catch(CloneNotSupportedException cnse) {
		}
		return null;
	}
	
	public void removeChoice(Choice c) {
		if(c == null) return;
		choices.remove(c);
		for(java.util.Iterator iter = map.values().iterator(); iter.hasNext(); ) {
			if(c.equals(iter.next())) iter.remove();
		}
	}
}
