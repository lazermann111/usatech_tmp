package com.usatech.report;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.activation.DataSource;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class GenerateDEXReportTask extends AbstractRetrieveFileTask {
	private static final Log log = Log.getLog();

	public GenerateDEXReportTask() {
		super(Generator.DEX);
	}

	protected GenerateDEXReportTask(Generator generator) {
		super(generator);
	}

	/**
	 * @throws IOException
	 * @throws ConvertException
	 * @throws DataLayerException
	 * @throws SQLException
	 * @see com.usatech.report.AbstractRetrieveFileTask#generateDataSource(java.util.Map, StandbyAllowance)
	 */
	@Override
	protected DataSource generateDataSource(Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception {
		return generateDEXFile(ConvertUtils.getLong(params.get("dexId")), standbyAllowance);
	}
	
	protected DataSource generateDEXFile(long dexId, StandbyAllowance standbyAllowance) throws SQLException, DataLayerException, ConvertException, IOException {
		if(log.isDebugEnabled())
			log.debug("Generate DEX for dex id = " + dexId);
		DataSource ds = getDexFile(dexId, standbyAllowance);
		if(log.isDebugEnabled())
			log.debug("Successfully retrieved DEX File with dex file id =  " + dexId);
		return ds;
	}

	protected DataSource getDexFile(long dexId, StandbyAllowance standbyAllowance) throws SQLException, DataLayerException, ConvertException, IOException {
		try (Connection conn = getReportConnection(standbyAllowance)) {
			conn.setAutoCommit(false);
			try (Results results = DataLayerMgr.executeQuery(conn, "GET_DEX_FILE_PATH", new Object[] { dexId })) {
				if(results.next()) {
					String filePath = results.getValue(1, String.class);
					final String fileName = results.getValue(2, String.class);
					Object content = results.getValue(3);
					final Callable<InputStream> getter;
					if(content == null) {
						if(filePath == null)
							throw new IOException("The dex file " + dexId + " is neither stored in the database nor in the file system");
						final File file = new File(filePath);
						if(!file.exists())
							throw new IOException("DEX File '" + file.getPath() + "' does not exist on the file system");
						getter = new Callable<InputStream>() {
							public InputStream call() throws Exception {
								return new FileInputStream(file);
							}
						};
					} else if(content instanceof Clob) {
						final Clob clob = (Clob) content;
						getter = new Callable<InputStream>() {
							public InputStream call() throws Exception {
								return clob.getAsciiStream();
							}
						};
					} else {
						final byte[] data = ConvertUtils.convert(byte[].class, content);
						getter = new Callable<InputStream>() {
							public InputStream call() throws Exception {
								return new ByteArrayInputStream(data);
							}
						};
					}
					return new DataSource() {
						public String getContentType() {
							return generator.getContentType();
						}

						public InputStream getInputStream() throws IOException {
							try {
								return new DexInputStream(getter.call());
							} catch(Exception e) {
								if(e instanceof IOException)
									throw (IOException) e;
								IOException ioe = new IOException(e.getMessage());
								ioe.initCause(e);
								throw ioe;
							}
						}

						public String getName() {
							return fileName;
						}

						public OutputStream getOutputStream() throws IOException {
							return null;
						}
					};
				} else
					throw new SQLException("Dex id " + dexId + " not found in database");
			}
		}
	}

}