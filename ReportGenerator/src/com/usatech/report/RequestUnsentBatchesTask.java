package com.usatech.report;

import java.sql.SQLException;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.results.Results;

/**
 * Used for requesting batch report.
 * batchType is configurable for 4 types of batch supported.
 * 
 * @author yhe, bkrug
 * 
 */
public class RequestUnsentBatchesTask extends AbstractRequestTask {
	private static final Log log = Log.getLog();
	protected ReportScheduleType batchType;

	@Override
	protected String getListCallId() {
		return "GET_POTENTIAL_" + batchType.toString() + "_BATCHES";
	}

	@Override
	protected Object getListCallParams() {
		return null;
	}

	@Override
	protected String getProcessCd() {
		return batchType.toString() + "_REPORT_REQUEST";
	}
	
	@Override
	protected String getProcessDesc() {
		return "Unsent " + batchType.getDescription() + " Batch";
	}

	@Override
	protected void handleItem(Map<String, Object> itemParams) {
		long batchId;
		try {
			batchId = ConvertUtils.getLong(itemParams.get("batchId"));
		} catch(ConvertException e) {
			log.error("Could not convert batchId", e);
			return;
		}
		try {
			Results reportResults = DataLayerMgr.executeQuery("GET_UNSENT_REPORTS_FOR_" + batchType.toString() + "_BATCH", itemParams, false);
			while(reportResults.next()) {
				ReportInstance rpt = new ReportInstance();
				reportResults.fillBean(rpt);
				log.info("Generating Batch Report " + rpt.userReportId + " for " + batchId);
				reportRequestFactory.generateReport(batchType, rpt.userReportId, rpt.userId, rpt.reportId, Generator.getByValue(rpt.generatorId), rpt.title, batchId, null, null, batchType.getDescription(), null, "");
			}
			DataLayerMgr.executeQuery("UPDATE_" + batchType.toString() + "_BATCH_TO_SENT", itemParams, true);
		} catch(BeanException e) {
			log.error("Could not convert report request for batch " + batchId, e);
		} catch(ServiceException e) {
			log.error("Could not publish report request for batch " + batchId, e);
		} catch(InvalidIntValueException e) {
			log.error("Could not convert generator id for batch " + batchId, e);
		} catch(SQLException e) {
			log.error("Could not generate batch report request for batch " + batchId, e);
		} catch(DataLayerException e) {
			log.error("Could not generate batch report request for batch " + batchId, e);
		}
	}

	public ReportScheduleType getBatchType() {
		return batchType;
	}

	public void setBatchType(ReportScheduleType batchType) {
		this.batchType = batchType;
	}
}
