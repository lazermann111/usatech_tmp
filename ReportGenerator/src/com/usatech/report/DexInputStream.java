/**
 *
 */
package com.usatech.report;

import java.io.IOException;
import java.io.InputStream;

import simple.io.StartAtPrefixInputStream;

/** This cuts off the stream once the end of line after DXE* is found.
 * @author Brian S. Krug
 *
 */
public class DexInputStream extends StartAtPrefixInputStream {
	protected static final String DEX_PREFIX = "DXS*";
	protected static final String DEX_SUFFIX = "DXE*";

	protected static final int STREAM_DONE = -10;
	protected static final int LAST_LINE = -5;
	protected static final int NONE = -1;
	protected byte[] endingMarker = DEX_SUFFIX.getBytes();
	protected int endingPos = NONE;

	public DexInputStream(InputStream delegate) throws IOException {
		super(delegate, DEX_PREFIX);
	}

	/**
	 * @see simple.io.StartAtPrefixInputStream#read()
	 */
	@Override
	public int read() throws IOException {
		if(endingPos == STREAM_DONE)
			return -1;
		int ch = super.read();
		if(ch < 0)
			throw new IOException("Ending marker was not found");
		checkByte((byte)ch);
		return ch;
	}
	/**
	 * @see simple.io.StartAtPrefixInputStream#read(byte[], int, int)
	 */
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if(endingPos == STREAM_DONE)
			return -1;
		int r = super.read(b, off, len);
		if(r < 0) {
			if(endingPos == LAST_LINE) // handle when last line of dex file does not have newline after it
				return -1;
			throw new IOException("Ending marker was not found");
		}
		for(int i = off; i < off+r; i++) {
			checkByte(b[i]);
			if(endingPos == STREAM_DONE)
				return i-off;
		}
		return r;
	}

	protected void checkByte(byte b) {
		if(endingPos == LAST_LINE) {
			if(b == '\n' || b == '\r') {
				endingPos = STREAM_DONE;
			}
		} else if(endingPos == NONE) {
			if(b == '\n'||b == '\r')
				endingPos = 0;
		} else if(b == endingMarker[endingPos]) {
			endingPos++;
			if(endingPos + 1 > endingMarker.length)
				endingPos = LAST_LINE;
		} else if (endingPos==0 &&(b == '\n'||b=='\r')){
			endingPos=0;
		}else {
			endingPos = NONE;
		}
	}
}
