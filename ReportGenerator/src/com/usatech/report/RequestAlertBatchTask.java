/**
 * 
 */
package com.usatech.report;

import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.results.Results;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.AlertSourceCode;
import com.usatech.layers.common.constants.AlertingAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.messagedata.MessageDataFactory;
import com.usatech.layers.common.messagedata.MessageData_0008;
import com.usatech.layers.common.messagedata.MessageDirection;


/**
 * Requests alert reports by receiving message chains from AlertDistributionTask
 * 
 * @author bkrug
 * 
 */
public class RequestAlertBatchTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	
	// -- configuration fields
	protected ReportRequestFactory reportRequestFactory;
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		//TODO: it would be very nice to create an event for each DEX file transfer and use EVENT_ID as the BATCH_ID (and as the REFERENCE_ID)
		long terminalId;
		String deviceSerialCd;
		long batchId;
		ReportScheduleType batchType;
		String qualifier;
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			deviceSerialCd = step.getAttribute(AlertingAttrEnum.AL_SERIAL_NUM, String.class, true);
			terminalId = step.getAttribute(AlertingAttrEnum.AL_TERMINAL, Long.class, true);
			Long eventId = step.getAttribute(AlertingAttrEnum.AL_EVENT_ID, Long.class, false);
			if(eventId != null) {
				AlertSourceCode eventSource = step.getAttribute(AlertingAttrEnum.AL_EVENT_SOURCE_TYPE, AlertSourceCode.class, true);
				batchId = eventId;
				qualifier = eventSource.toString();
				switch(eventSource) {
					case DEX:
						batchType = ReportScheduleType.FILE_TRANSFER_EVENT;
						break;
					case DEVICE:
					case MDB:
					default:
						batchType = ReportScheduleType.EVENT;
						break;
				}
				params.put("terminalId", terminalId);
				params.put("batchId", batchId);
				params.put("batchType", batchType);
				params.put("alertSourceName", qualifier);
			} else {
				byte[] bytes = step.getAttribute(MessageAttrEnum.ATTR_DATA, byte[].class, true);
				final ByteBuffer readBuffer = ByteBuffer.wrap(bytes);
				MessageData_0008 data = (MessageData_0008) MessageDataFactory.readMessage(readBuffer, MessageDirection.INTRA_SERVER);
				List<MessageData_0008.AlertData> alerts = data.getAlerts();
				// get the "batchId" (REFERENCE_ID) and "batchTYpe" (ALERT_SOURCE) from database record of first alert
				if(alerts.isEmpty()) {
					log.warn("Received Alert with no detail rows for device '" + deviceSerialCd);
					return 1;
				}
				MessageData_0008.AlertData alert = alerts.get(0);
				DataLayerMgr.selectInto("GET_ALERT_BATCH_INFO", alert, params);
				batchId = ConvertUtils.getLong(params.get("batchId"));
				batchType = ConvertUtils.convertRequired(ReportScheduleType.class, params.get("batchType"));
				qualifier = ConvertUtils.convertRequired(String.class, params.get("alertSourceName"));
			}
			params.put("createDate", step.getAttribute(AlertingAttrEnum.AL_NOTIFICATION_TS, Object.class, true)); // TODO: get this
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(ConvertException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(BeanException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		try {
			Results reportResults = DataLayerMgr.executeQuery("GET_UNSENT_REPORTS_FOR_ALERT_BATCH", params, false);
			while(reportResults.next()) {
				ReportInstance rpt = new ReportInstance();
				reportResults.fillBean(rpt);
				log.info("Generating Alert Report " + rpt.userReportId + " for " + batchId);
				reportRequestFactory.generateReport(batchType, rpt.userReportId, rpt.userId, rpt.reportId, Generator.getByValue(rpt.generatorId), rpt.title, batchId, null, null, batchType.getDescription(), null, deviceSerialCd, qualifier);
			}
			DataLayerMgr.executeQuery("UPDATE_ALERT_BATCH_TO_SENT", params, true);
		} catch(BeanException e) {
			throw new RetrySpecifiedServiceException("Could not convert report request for batch " + batchId, e, WorkRetryType.NO_RETRY);
		} catch(InvalidIntValueException e) {
			throw new RetrySpecifiedServiceException("Could not convert generator id for batch " + batchId, e, WorkRetryType.NO_RETRY);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException("Could not convert generator id for batch " + batchId, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not generate batch report request for batch " + batchId, e);
		}
		return 0;
	}
	
	public ReportRequestFactory getReportRequestFactory() {
		return reportRequestFactory;
	}

	public void setReportRequestFactory(ReportRequestFactory reportRequestFactory) {
		this.reportRequestFactory = reportRequestFactory;
	}
}

