package com.usatech.report;

import org.apache.log4j.Logger;

import simple.app.MainWithConfig;
import simple.io.logging.Log4JThreadFilter;

public class ReportGeneratorService {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.setProperty("javax.xml.transform.TransformerFactory",
		"com.jclark.xsl.trax.TransformerFactoryImpl");
		Log4JThreadFilter filter = new Log4JThreadFilter();
		filter.setThreadNamePattern("RecordCurrentServiceStatusThread");
		Logger.getRootLogger().getAppender("file").addFilter(filter);
		new MainWithConfig().run(args);
	}

}
