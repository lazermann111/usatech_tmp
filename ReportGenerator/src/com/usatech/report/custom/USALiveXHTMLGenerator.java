
package com.usatech.report.custom;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Locale;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Layout;
import simple.falcon.engine.Report;
import simple.falcon.engine.standard.DirectXHTMLGenerator;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.text.StringUtils;
import simple.xml.AppendableAttributes;


public class USALiveXHTMLGenerator extends DirectXHTMLGenerator {
	private static final Log log = Log.getLog();
	
    public USALiveXHTMLGenerator(ExecuteEngine engine) {
        super(engine);
    }
   	
	protected void generatePreprocess(GenerateInfo info) throws SAXException {
		super.generatePreprocess(info);
		switch(info.scene.getLayout()) {
			case FRAGMENT:
				info.serializer.processingInstruction("output", "omit-dtd=\"true\"");
		}
	}

	protected void generateBeforeContents(GenerateInfo info) throws SAXException {
		switch(info.scene.getLayout()) {
			case FRAGMENT:
				info.serializer.startElement("div");
				switch(info.targetType) {
					case CHART_HTML:
					case HTML:
						super.generateScripts(info);
				}
				super.generateStyles(info);
				break;
			case UNFRAMED:
			case OFFLINE:
				generateDocumentHeader(info);
				info.serializer.startElement("body", Collections.singletonMap("class", "unframed"));
				break;
			default:
				super.generateBeforeContents(info);
		}
	}

	protected void generateAfterContents(GenerateInfo info) throws SAXException {
		switch(info.scene.getLayout()) {
			case FRAGMENT:
				info.serializer.endElement("div");
				break;
			case UNFRAMED:
			case OFFLINE:
				generateCopyright(info, info.report, null, "copyright");
				info.serializer.endElement("body");
				info.serializer.endElement("html");
				break;
			default:
				super.generateAfterContents(info);
		}
	}

	protected String getTitle(GenerateInfo info) {
		String title = translate(info.scene, "main-title", "USALive");
		String displayName = getDisplayName(info);
		if(displayName != null)
			title = title + " - " + displayName;
		return title;
	}

	protected String getDisplayName(GenerateInfo info) {
		if(info.executor == null)
			return null;
		String displayName = info.executor.getDisplayName();
		if(displayName == null)
			return null;
		displayName = displayName.trim();
		if(displayName.length() == 0)
			return null;
		return displayName;
	}
	protected void generatePageHeader(GenerateInfo info) throws SAXException {
		StringBuilder sb = new StringBuilder();
		sb.append("[if IE]><link href=\"").append(info.scene.addLastModifiedToUri("css/usalive-ie.css")).append("\" type=\"text/css\" rel=\"stylesheet\"/><![endif]");
		char[] ch = new char[sb.length()];
		sb.getChars(0, ch.length, ch, 0);
		info.serializer.comment(ch, 0, ch.length);
	}
	
	protected static final String[] USALIVE_SCRIPTS = new String[] {
		"scripts/mootools-core-1.4.5-full-nocompat.js",
		"scripts/mootools-more-1.4.0.1.js",
		"scripts/meio-mask-2.0.1.0.js",
		"scripts/app.js",
		"scripts/report.js",
		"scripts/site.js"
	};
	
	protected void generateScripts(GenerateInfo info) throws SAXException {
		for(String script : USALIVE_SCRIPTS)
			info.serializer.putElement("script", attr().add("type", "text/javascript").add("src", info.scene.addLastModifiedToUri(script)));
		if(info.scene.isLive()) {
			String sessionToken = ConvertUtils.getStringSafely(info.scene.getProperties().get("session-token"));
			if(!StringUtils.isBlank(sessionToken)) {
				info.serializer.startElement("script", attr().add("type", "text/javascript"));
				info.serializer.characters("Form.injectSessionToken(\"");
				info.serializer.characters(StringUtils.prepareScript(sessionToken));
				info.serializer.characters("\");");
				info.serializer.endElement("script");
			}
		}
		info.serializer.startElement("script", attr().add("type", "text/javascript"));
		info.serializer.characters("\nLocale.use(\"");
		Locale locale = info.scene.getLocale();
		if(locale == null)
			locale = Locale.getDefault();
		info.serializer.characters(locale.toString());
		info.serializer.characters("\");\n");
		try {
			info.serializer.characters(GenerateUtils.getCountriesConfigScript(info.scene));
		} catch(SQLException e) {
			log.warn("Could not generate Countries config script", e);
		}
		if(info.scene.getLayout() == Layout.FULL && info.executor != null) {
			info.serializer
					.characters("\nvar newReportRunner = new SerialTaskRunner(); window.addEvent('domready', function() { var box = $(\"report-ready-box\"); var container = $(\"report-ready-content\"); window.handleNewReport = function(url, reportName) { newReportRunner.add(function() { container.empty(); container.appendText(\"Report \\\"\"); var link = new Element(\"a\", { href: url, text: reportName}); container.adopt(link); container.appendText(\"\\\" is ready for pick-up.\"); }, 1).addFx(new Fx.Tween(box), [\"opacity\", 0, 1], 1).addFx(new Fx.Tween(box, {duration: 5000}), [\"opacity\", 1, 0], 5000); }; new Request.HTML({ url : \"report_ajax_poll.i\", data: {resetAjaxPollCount:true}, update: $(\"ready-reports-box\") }).send(); });\n");
		}
		info.serializer.endElement("script");
		super.generateScripts(info);
	}

	@Override
	public void generateBodyHeader(GenerateInfo info) throws SAXException{
		AppendableAttributes aaContent = attr();
		if(info.scene.getLayout() == Layout.FULL) {
			info.serializer.startElement("div", attr().add("class", "wrapper"));
			info.serializer.startElement("div", attr().add("id", "banner"));
			info.serializer.startElement("a", attr().add("title", "My Homepage").add("href", "/home.i").add("class", "headerLogo"));
			info.serializer.endElement("a");
			info.serializer.startElement("div", attr().add("id", "banner-center"));
			info.serializer.startElement("div", attr().add("id", "banner-welcome"));
			String displayName = getDisplayName(info);
			if(displayName != null)
				info.serializer.characters(displayName);
			info.serializer.endElement("div");
			info.serializer.endElement("div");

			if(info.executor != null) {
				info.serializer.startElement("fieldset", attr().add("id", "report-request-group"));
				info.serializer.startElement("legend");
				info.serializer.characters("Report Requests:");
				info.serializer.endElement("legend");
				info.serializer.startElement("div", attr().add("id", "report-requests"));
				info.serializer.startElement("div", attr().add("id", "reports-pending-count"));
				info.serializer.endElement("div");
				info.serializer.startElement("a", attr().add("title", "View Reports").add("href", "./report_request_history.i?isByProfile=false").add("id", "reports-ready-count"));
				info.serializer.characters(translate(info.scene, "report-request-summary-retrieval", "Loading..."));
				info.serializer.endElement("a");
				info.serializer.endElement("div");
				info.serializer.endElement("fieldset");
				info.serializer.startElement("div", attr().add("id", "report-ready-box").add("class", "request-complete-box"));
				info.serializer.startElement("div", attr().add("class", "box-top-left"));
				info.serializer.endElement("div");
				info.serializer.startElement("div", attr().add("class", "box-top"));
				info.serializer.endElement("div");
				info.serializer.startElement("div", attr().add("class", "box-top-right"));
				info.serializer.endElement("div");
				info.serializer.startElement("div", attr().add("class", "box-left"));
				info.serializer.endElement("div");
				info.serializer.startElement("div", attr().add("class", "box-container"));
				info.serializer.startElement(
						"div",
						attr().add("class", "close-btn").add("onclick", "$('report-ready-box').setStyle('opacity', 0);").add("onmouseenter", "$(this).setStyle('backgroundImage', 'url(images/info_dialog_close_hover.png)');").add("onmouseleave", "$(this).setStyle('backgroundImage', 'url(images/info_dialog_close.png)');")
								.add("onmousedown", "$(this).setStyle('backgroundImage', 'url(images/info_dialog_close_down.png)');").add("onmouseup", "$(this).setStyle('backgroundImage', 'url(images/info_dialog_close.png)');"));
				info.serializer.endElement("div");
				info.serializer.startElement("div", attr().add("id", "report-ready-content"));
				info.serializer.endElement("div");
				info.serializer.endElement("div");
				info.serializer.startElement("div", attr().add("class", "box-right"));
				info.serializer.endElement("div");
				info.serializer.startElement("div", attr().add("class", "box-bottom-left"));
				info.serializer.endElement("div");
				info.serializer.startElement("div", attr().add("class", "box-bottom"));
				info.serializer.endElement("div");
				info.serializer.startElement("div", attr().add("class", "box-bottom-right"));
				info.serializer.endElement("div");
				info.serializer.endElement("div");
			}
			info.serializer.endElement("div");
			if(info.scene.isLive()) {				
				Node linksNode = null;
				try {
					linksNode = ConvertUtils.convert(Node.class, info.scene.getProperties().get("menuLinks"));					
				} catch(ConvertException e) {
					log.warn("Could not convert 'menuLinks' to nodes", e);
				}
				if(linksNode != null)					
					generateMenuLinks(info, linksNode);
			}			
		} else
			aaContent.add("class", "unframed");
		aaContent.add("id", "content");
		info.serializer.startElement("div", aaContent);
	}

	protected void generateBodyFooter(GenerateInfo info) throws SAXException {
		info.serializer.startElement("div", attr().add("class", "push"));
		info.serializer.endElement("div");
		info.serializer.endElement("div");
		info.serializer.endElement("div");
		generateCopyright(info, info.report, "footer", null);
	}

	@Override
	protected int getCopyrightHeight(GenerateInfo info) {
		return 0;
	}

	protected void generateCopyright(GenerateInfo info, Report report, String cssId, String cssClass) throws SAXException {
		AppendableAttributes aa = attr();
		if(!StringUtils.isBlank(cssId))
			aa.add("id", cssId);
		if(!StringUtils.isBlank(cssClass))
			aa.add("class", cssClass);

		info.serializer.startElement("div", aa);
		info.serializer.startElement("a", attr().add("href", "https://www.usatech.com").add("target", "_blank"));
		info.serializer.characters("v.");
		info.serializer.characters(getUSALiveVersion());
		info.serializer.characters(" - USA Technologies, Inc. - \u00A9 Copyright ");
		info.serializer.characters(String.valueOf(GenerateUtils.getCurrentYear()));
		info.serializer.characters(" - Confidential");
		info.serializer.endElement("a");
		info.serializer.endElement("div");
	}

	protected static String getFirstSubElementValue(Element elem, String tagName) {
		String[] values = getSubElementValues(elem, tagName);
		if(values.length > 0)
			return values[0];
		return null;
	}

	protected static String[] getSubElementValues(Element elem, String tagName) {
		NodeList nl = elem.getElementsByTagName(tagName);
		String[] values = new String[nl.getLength()];
		for(int i = 0; i < values.length; i++)
			values[i] = ((Element) nl.item(i)).getTextContent();
		return values;
	}

	protected void generateMenuLinks(GenerateInfo info, Node linksNode) throws SAXException {
		info.serializer.startElement("div", attr().add("id", "menuContainer"));
		info.serializer.startElement("div", attr().add("class", "topMenu"));
		generateMenuLinks(info, linksNode, false, "Reports");
		info.serializer.endElement("div");
		info.serializer.startElement("div", attr().add("class", "submenu"));
		generateMenuLinks(info, linksNode, true, "Reports");
		info.serializer.endElement("div");
		info.serializer.endElement("div");
	}
	
	protected void generateMenuLinks(GenerateInfo info, Node linksNode, boolean subMenu, String reportsCategory) throws SAXException {
		String selectedMenuItem = ConvertUtils.getStringSafely(info.parameters.get("selectedMenuItem"));
		NodeList linkList = linksNode.getChildNodes();
		String prevCategory = null;
		info.serializer.startElement("ul");
		int linkListLength = linkList.getLength();
		boolean foundSelection = false;
		for(int i = 0; i < linkListLength; i++) {
			Node node = linkList.item(i);
			if(node instanceof Element) {
				Element elem = (Element) node;
				String category = getFirstSubElementValue(elem, "category").toLowerCase();
				if (subMenu && !reportsCategory.equalsIgnoreCase(category))
					continue;
				if(!subMenu && !ConvertUtils.areEqual(prevCategory, category)) {
					if (prevCategory != null) {
						info.serializer.endElement("ul");
						info.serializer.endElement("li");
					}
					if (reportsCategory.equalsIgnoreCase(category))
						info.serializer.startElement("li", attr().add("class", "topMenuItem topMenuItemSelected"));
					else
						info.serializer.startElement("li", attr().add("class", "topMenuItem"));					
					info.serializer.startElement("a", attr().add("href", "#"));
					info.serializer.putElement("img", attr().add("src", new StringBuilder("/images/").append(category).append(".png").toString()));
					info.serializer.characters(category);
				    info.serializer.endElement("a");
					info.serializer.startElement("ul");
					prevCategory = category;
				}
				String id = getFirstSubElementValue(elem, "id");
				StringBuilder href = new StringBuilder(getFirstSubElementValue(elem, "href"));
				href.append(href.indexOf("?") >= 0 ? '&' : '?').append("selectedMenuItem=").append(id);
				AppendableAttributes aa = attr();
				String title = getFirstSubElementValue(elem, "title");
				if(!foundSelection && (selectedMenuItem != null && ConvertUtils.areEqual(selectedMenuItem, id) 
						|| selectedMenuItem == null && title.equalsIgnoreCase("Recent Reports") || i == linkListLength - 1)) {
					aa.add("class", "submenuItemSelected");
					foundSelection = true;
				}
				info.serializer.startElement("li", aa);
				String description = getFirstSubElementValue(elem, "description");
				info.serializer.putElement("a", attr().add("title", description).add("href", href.toString()).add("target", "_top"), title.toLowerCase());
				info.serializer.endElement("li");
			}
		}
		info.serializer.endElement("ul");
		if (!subMenu) {
			info.serializer.endElement("li");
			info.serializer.endElement("ul");
		}
	}
	protected static String usaliveVersion;
	/**
	 * In deployed environment, we create in rptgen/classes a symbolic link usalive-usalive.txt to ../../usalive/classes/version.txt. See build.xml
	 * @return
	 */
	public static String getUSALiveVersion() {
		if(usaliveVersion == null) {
			try {
				usaliveVersion = SystemUtils.nvl(IOUtils.readFully(ConfigSource.createConfigSource("usalive-version.txt").getInputStream()), "UNKNOWN").trim();
			} catch(FileNotFoundException e) {
				Log.getLog().info("Could not find usalive-version.txt on the classpath");
				usaliveVersion = "UNKNOWN";
			} catch(IOException e) {
				usaliveVersion = "UNKNOWN";
			}
		}
		return usaliveVersion;
	}
}	
	