package com.usatech.report.custom;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import simple.bean.ConvertException;
import simple.falcon.engine.Scene;
import simple.results.Results;

public class GenerateUtils extends simple.servlet.GenerateUtils {
	static {
		setCountryStore(new CountryStore() {
			@Override
			protected Country createCountry(Results results) throws SQLException {
				Country country = super.createCountry(results);
				boolean terminals;
				boolean customers;
				boolean banks;
				try {
					terminals = results.getValue("terminals", Boolean.class);
					customers = results.getValue("customers", Boolean.class);
					banks = results.getValue("banks", Boolean.class);
				} catch(ConvertException e) {
					throw new SQLException(e);
				}
				if(terminals)
					terminalCountries.add(country);
				if(customers)
					customerCountries.add(country);
				if(banks)
					bankCountries.add(country);
				return country;
			}

			@Override
			protected void resetCountries() {
				super.resetCountries();
				terminalCountries.clear();
				customerCountries.clear();
				bankCountries.clear();
			}
		});
	}
	
	protected static final Collection<Country> terminalCountries = new ArrayList<Country>();
	protected static final Collection<Country> customerCountries = new ArrayList<Country>();
	protected static final Collection<Country> bankCountries = new ArrayList<Country>();
	protected static final Collection<Country> terminalCountriesUnmod = Collections.unmodifiableCollection(terminalCountries);
	protected static final Collection<Country> customerCountriesUnmod = Collections.unmodifiableCollection(customerCountries);
	protected static final Collection<Country> bankCountriesUnmod = Collections.unmodifiableCollection(bankCountries);

	public static Country getCountry(String countryCd) throws SQLException {
		return simple.servlet.GenerateUtils.getCountry(countryCd);
	}

	public static Collection<Country> getAllCountries() throws SQLException {
		return simple.servlet.GenerateUtils.getCountries();
	}

	public static Collection<Country> getTerminalCountries() throws SQLException {
		simple.servlet.GenerateUtils.getCountries();
		return terminalCountriesUnmod;
	}

	public static Collection<Country> getCustomerCountries() throws SQLException {
		simple.servlet.GenerateUtils.getCountries();
		return customerCountriesUnmod;
	}

	public static Collection<Country> getBankCountries() throws SQLException {
		simple.servlet.GenerateUtils.getCountries();
		return bankCountriesUnmod;
	}

	public static String getCountriesConfigScript(Scene scene) throws SQLException {
		return getCountriesConfigScript(scene.getTranslator());
	}
}
