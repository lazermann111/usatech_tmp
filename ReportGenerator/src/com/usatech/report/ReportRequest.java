package com.usatech.report;

import java.beans.IntrospectionException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.CharBuffer;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.zip.DataFormatException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.Scene;
import simple.falcon.engine.util.EngineUtils;
import simple.falcon.run.ReportRunner;
import simple.lang.SystemUtils;
import simple.text.StringUtils;
import simple.xml.XMLBuilder;
import simple.xml.XMLizable;

import com.usatech.app.MessageChainStep;
/**
 * Class that contains the info for a report request.
 * @author yhe, bkrug
 *
 */
public class ReportRequest<R> {
	protected final R report;
	protected final SortedMap<String,String> parameters;
	protected final Scene scene;
	protected final long profileId;
	protected final long userId;
	protected byte[] reportXml;
	protected byte[] reportParams;
	protected final boolean compressing;
	protected final Generator generator;
	protected final String title;
	protected static final String CHARSET_NAME = "UTF-8";
	protected static boolean defaultCompressing = false;
	

	public long getUserId() {
		return userId;
	}

	/**
     * Method to take report parameters and return a urlencoded and compressed byte array
     * @param parameters
     * @return
	 * @throws IOException
     * @throws DataFormatException
     */
	public static byte[] encodeParameters(Map<String,String> parameters, Scene scene, boolean compressing) throws IOException {
    	//sort first
    	SortedMap<String, String> sortedMap;
    	if(parameters instanceof SortedMap)
    		sortedMap = (SortedMap<String, String>)parameters;
    	else
    		sortedMap = new TreeMap<String, String>(parameters);
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	Writer writer = new OutputStreamWriter(compressing ? new DeflaterOutputStream(baos) : baos, ReportRunner.XML_CHARSET);
    	try {
			for(Map.Entry<String, String> entry : sortedMap.entrySet())
				writeNVP(writer, entry.getKey(), entry.getValue());
			if(scene != null) {
				if(scene.getTimeZone() != null)
					writeNVP(writer, "report.scene.timeZone", scene.getTimeZone().getID());
				if(scene.getLocale() != null)
					writeNVP(writer, "report.scene.locale", scene.getLocale().toString());
				if(scene.getBaseUrl() != null)
					writeNVP(writer, "report.scene.baseUrl", scene.getBaseUrl());
				if(scene.getRunReportAction() != null)
					writeNVP(writer, "report.scene.runReportAction", scene.getRunReportAction());
				if(scene.getUserAgent() != null)
					writeNVP(writer, "report.scene.userAgent.dataUrlSupported", String.valueOf(EngineUtils.isDataUriSupported(scene.getUserAgent())));
				if(scene.getLayout() != null)
					writeNVP(writer, "report.scene.layout", scene.getLayout().toString());
				if(scene.getProperties() != null)
					for(Map.Entry<String, Object> entry : scene.getProperties().entrySet()) {
						String value = ConvertUtils.getStringSafely(entry.getValue());
						if(!StringUtils.isBlank(value))
							writeNVP(writer, "report.scene.properties." + entry.getKey(), value);
					}
			}
	    	writer.flush();
    	} finally {
    		writer.close();
    	}
    	return baos.toByteArray();
    }

	protected static void writeNVP(Writer writer, String name, String value) throws IOException {
		if(!StringUtils.isBlank(name))
			writer.write(URLEncoder.encode(name, ReportRunner.XML_CHARSET.name()));
		writer.write('=');
		if(!StringUtils.isBlank(value))
			writer.write(URLEncoder.encode(value, ReportRunner.XML_CHARSET.name()));
		writer.write('&');
	}
    /**
     * Method to take report parameters and return a urlencoded string
     * @param parameters
     * @return
	 * @throws IOException
     * @throws DataFormatException
     */
    public static SortedMap<String,String> decodeParameters(byte[] bytes, boolean compressing) throws IOException {
    	return decodeParameters(new java.io.ByteArrayInputStream(bytes), compressing);
    }
    public static SortedMap<String,String> decodeParameters(InputStream bytes, boolean compressing) throws IOException {
    	final SortedMap<String, String> parameters = new TreeMap<String, String>();
    	final Reader reader = new InputStreamReader(compressing ? new InflaterInputStream(bytes) : bytes, ReportRunner.XML_CHARSET);
    	CharBuffer buffer = CharBuffer.allocate(512);
    	String name = null;
    	int start = 0;
    	while(reader.read(buffer) >= 0) {
    		buffer.flip();
    		while(buffer.hasRemaining()) {
    			if(name != null) {
    				if(buffer.get() == '&') {
    					String value = extractDecodedString(buffer, start);
    					parameters.put(name, value);
    					name = null;
	    				start = buffer.position(); // should be 0
	    			}
    			} else {
	    			if(buffer.get() == '=') {
	    				name = extractDecodedString(buffer, start);
	    				start = buffer.position(); // should be 0
	    			}
    			}
    		}
    		if(buffer.limit() > buffer.capacity() - 32) {
    			// we need a bigger buffer
    			CharBuffer newBuffer = CharBuffer.allocate(buffer.capacity() * 2);
    			buffer.position(start);
    			newBuffer.put(buffer);
    			buffer = newBuffer;
    		} else {
    			buffer.limit(buffer.capacity());
    		}
    	}
    	if(name != null) {
    		String value = extractDecodedString(buffer, start);
			parameters.put(name, value);
    	}
    	return parameters;
    }

    protected static String extractDecodedString(CharBuffer buffer, int start) throws UnsupportedEncodingException {
    	int end = buffer.position() - 1;
		int origLimit = buffer.limit();
		buffer.position(start);
		buffer.limit(end);
		String s = URLDecoder.decode(buffer.toString(), ReportRunner.XML_CHARSET.name());
		buffer.limit(origLimit);
		buffer.position(end + 1);
		buffer.compact();
		buffer.flip();
		return s;
    }

	public ReportRequest(Generator generator, R report, Scene scene, long profileId, long userId, SortedMap<String, String> parameters, String title) {
		this.generator = generator;
		this.scene = scene;
		this.profileId = profileId;
		this.userId = userId;
		this.report = report;
		this.parameters = parameters;
		this.title = title;
		this.compressing = isDefaultCompressing();
	}

	@Override
	public int hashCode() {
		return SystemUtils.addHashCodes(0, generator, scene, getReportHashCode(), parameters, profileId);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ReportRequest<?>))
			return false;
		final ReportRequest<?> rr = (ReportRequest<?>) obj;
		return parameters.equals(rr.parameters)
			&& ConvertUtils.areEqual(report, rr.report)
			&& ConvertUtils.areEqual(scene, rr.scene)
			&& profileId == rr.profileId
			&& generator == rr.generator;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}
	public String getBaseUrl() {
		return (scene == null ? null : scene.getBaseUrl());
	}
	public Locale getLocale() {
		return (scene == null ? null : scene.getLocale());
	}
	public long getProfileId() {
		return profileId;
	}

	public byte[] getReportXml() throws ConvertException {
		if(reportXml == null) {
			if(report instanceof XMLizable)
				try {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					Writer writer = new OutputStreamWriter(isCompressing() ? new DeflaterOutputStream(baos) : baos, ReportRunner.XML_CHARSET);
					try {
						XMLBuilder xmlBuilder = new XMLBuilder(writer);
						xmlBuilder.write(report);
						writer.flush();
					} finally {
						writer.close();
					}
					reportXml = baos.toByteArray();
				} catch(InstantiationException e) {
					throw new ConvertException("Could not create XMLBuilder", byte[].class, report, e);
				} catch(IllegalAccessException e) {
					throw new ConvertException("Could not create XMLBuilder", byte[].class, report, e);
				} catch(ClassNotFoundException e) {
					throw new ConvertException("Could not create XMLBuilder", byte[].class, report, e);
				} catch(IntrospectionException e) {
					throw new ConvertException("Could not create XMLBuilder", byte[].class, report, e);
				} catch(InvocationTargetException e) {
					throw new ConvertException("Could not create XMLBuilder", byte[].class, report, e);
				} catch(IOException e) {
					throw new ConvertException("Could not create XMLBuilder", byte[].class, report, e);
				} catch(ParseException e) {
					throw new ConvertException("Could not create XMLBuilder", byte[].class, report, e);
				} catch(SAXException e) {
					throw new ConvertException("Could not write the report to xml", byte[].class, report, e);
				}
			else
				reportXml = new byte[0];
		}
		return reportXml;
	}

	public byte[] getReportParams() throws IOException {
		if(reportParams == null)
			reportParams = encodeParameters(parameters, scene, isCompressing());
		return reportParams;
	}

	public long getReportAndParamsHash() {
		return ((getReportHashCode() & 0xFFFFFFFFL) << 32) + (getParametersHashCode() & 0xFFFFFFFFL);
	}

	protected int getReportHashCode() {
		R r = getReport();
		return r == null ? 0 : r.hashCode();
	}

	protected int getParametersHashCode() {
		return SystemUtils.addHashCode(parameters.hashCode(), scene);
	}

	public R getReport() {
		return report;
	}

	public Generator getGenerator() {
		return generator;
	}

	public String getRunReportAction() {
		return (scene == null ? null : scene.getRunReportAction());
	}

	public TimeZone getTimeZone() {
		return (scene == null ? null : scene.getTimeZone());
	}

	public Scene getScene() {
		return scene;
	}

	public static boolean isDefaultCompressing() {
		return defaultCompressing;
	}

	public static void setDefaultCompressing(boolean compressing) {
		defaultCompressing = compressing;
	}

	public boolean isCompressing() {
		return compressing;
	}

	public void populateGenerateStep(MessageChainStep generateStep, long requestId) throws ConvertException {
		for(Map.Entry<String, String> entry : getParameters().entrySet()) {
			generateStep.addStringAttribute(entry.getKey(), entry.getValue());
		}
		generateStep.addLongAttribute("request.requestId", requestId);
		generateStep.addLongAttribute("request.userId", getProfileId());
		generateStep.addLongAttribute("request.masterUserId", getUserId());
		generateStep.addStringAttribute("request.title", title);
		generateStep.addIntAttribute("request.generatorId", getGenerator().getValue());
		byte[] xml = getReportXml();
		if(xml != null && xml.length > 0)
			generateStep.addByteArrayAttribute("report.reportXML", xml);
		Scene scene = getScene();
		if(scene != null) {
			if(scene.getTimeZone() != null)
				generateStep.addStringAttribute("report.scene.timeZone", scene.getTimeZone().getID());
			if(scene.getLocale() != null)
				generateStep.addStringAttribute("report.scene.locale", scene.getLocale().toString());
			if(scene.getBaseUrl() != null)
				generateStep.addStringAttribute("report.scene.baseUrl", scene.getBaseUrl());
			if(scene.getRunReportAction() != null)
				generateStep.addStringAttribute("report.scene.runReportAction", scene.getRunReportAction());
			if(scene.isLive())
				generateStep.addBooleanAttribute("report.scene.live", true);
			if(scene.getUserAgent() != null)
				generateStep.addStringAttribute("report.scene.userAgent", scene.getUserAgent());
			if(scene.getLayout() != null)
				generateStep.addLiteralAttribute("report.scene.layout", scene.getLayout());
			for(Map.Entry<String, Object> entry : scene.getProperties().entrySet())
				generateStep.addLiteralAttribute("report.scene.properties." + entry.getKey(), entry.getValue());
		}
	}

	public String getTitle() {
		return title;
	}
}
