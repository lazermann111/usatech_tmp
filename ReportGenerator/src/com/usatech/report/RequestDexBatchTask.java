/**
 * 
 */
package com.usatech.report;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.results.Results;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

/**
 * Request for batchTypeId=2 DEX when DEX file comes in from file transfer.
 * @author yhe
 *
 */
public class RequestDexBatchTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	
	protected ReportRequestFactory reportRequestFactory;
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		ReportScheduleType batchType=ReportScheduleType.DEX;
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		long dexId=-1;
		try {
			dexId=ConvertUtils.getLong(attributes.get("dexId"));
			params.put("terminalId", attributes.get("terminalId"));
			params.put("batchId", dexId);
			params.put("batchType", batchType);
			params.put("createDate", attributes.get("fileTransferTs")); 
			String deviceSerialCd=ConvertUtils.getStringSafely(attributes.get("deviceSerialCd"));
			Results reportResults = DataLayerMgr.executeQuery("GET_UNSENT_REPORTS_FOR_DEX_BATCH", params, false);
			while(reportResults.next()) {
				ReportInstance rpt = new ReportInstance();
				reportResults.fillBean(rpt);
				log.info("Requesting Dex Batch Report " + rpt.userReportId + " for " + dexId);
				reportRequestFactory.generateReport(batchType, rpt.userReportId, rpt.userId, rpt.reportId, Generator.getByValue(rpt.generatorId), rpt.title, dexId, null, null, batchType.getDescription(), null, deviceSerialCd, null);
			}
			DataLayerMgr.executeQuery("UPDATE_DEX_BATCH_TO_SENT", params, true);
		} catch(BeanException e) {
			throw new RetrySpecifiedServiceException("Could not convert report request for dexId " + dexId, e, WorkRetryType.NO_RETRY);
		} catch(InvalidIntValueException e) {
			throw new RetrySpecifiedServiceException("Could not convert generator id for dexId " + dexId, e, WorkRetryType.NO_RETRY);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException("Could not convert generator id for dexId " + dexId, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not generate instant dex batch report request for dexId " + dexId, e);
		} catch(ConvertException e) {
			throw new ServiceException("Could not generate instant dex batch report request for dexId " + dexId, e);
		}
		return 0;
	}
	
	public ReportRequestFactory getReportRequestFactory() {
		return reportRequestFactory;
	}

	public void setReportRequestFactory(ReportRequestFactory reportRequestFactory) {
		this.reportRequestFactory = reportRequestFactory;
	}
}

