package com.usatech.report;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;

import com.usatech.app.Attribute;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.AbstractScheduledService;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.TransportAttrEnum;

/**
 * Controls polling for inbound data exchange items
 * 
 * @author bkrug
 * 
 */
public class DataExchangePollService extends AbstractScheduledService {
	private static final Log log = Log.getLog();
	protected long pollInterval = 5 * 60 * 1000L;
	protected int dataExchangeType = 1;
	protected Publisher<ByteInput> publisher;
	protected long delay;
	public DataExchangePollService(String serviceName) {
		super(serviceName);
	}

	@Override
	protected long getDelay() {
		return delay;
	}

	@Override
	public void process() {
		// try to lock polling
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("pollInterval", getPollInterval());
		params.put("dataExchangeType", getDataExchangeType());
		delay = getPollInterval();
		try {
			DataLayerMgr.executeCall("START_DATA_EXCHANGE_INBOUND_POLL", params, true);
			boolean proceed = ConvertUtils.getBoolean(params.get("proceed"));
			if(proceed) {
				log.info("Firing off Inbound polling for Data Exchange Type " + getDataExchangeType());
				long transportId = ConvertUtils.getLong(params.get("transportId"));
				int transportTypeId = ConvertUtils.getInt(params.get("transportTypeId"));
				long pollStartTime = ConvertUtils.getLong(params.get("pollStartTime"));
				// get transport props
				MessageChain mc = new MessageChainV11();
				MessageChainStep pollStep = createPollStep(mc, transportId, transportTypeId);
				MessageChainStep recordStep = mc.addStep("usat.data.exchange.received");
				recordStep.setAttribute(CommonAttrEnum.ATTR_FILE_TYPE, getDataExchangeType());
				recordStep.setReferenceAttribute(CommonAttrEnum.ATTR_RESOURCE, pollStep, CommonAttrEnum.ATTR_RESOURCE);
				recordStep.setReferenceAttribute(CommonAttrEnum.ATTR_FILE_PATH, pollStep, CommonAttrEnum.ATTR_FILE_PATH);
				recordStep.setReferenceAttribute(CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME, pollStep, CommonAttrEnum.ATTR_RESOURCE_MODIFIED_TIME);
				MessageChainStep completeStep = mc.addStep("usat.data.exchange.poll.complete");
				completeStep.setAttribute(CommonAttrEnum.ATTR_FILE_TYPE, getDataExchangeType());
				completeStep.setAttribute(CommonAttrEnum.ATTR_PROCESS_TIME, pollStartTime);

				pollStep.addNextSteps(0, recordStep, completeStep);
				pollStep.addNextSteps(1, completeStep); // for none found

				MessageChainService.publish(mc, getPublisher());
			} else {
				delay = ConvertUtils.getLong(params.get("delay"));
			}
		} catch(SQLException | DataLayerException | ConvertException | ServiceException e) {
			log.error("Could not start data exchange polling for Data Exchange Type " + getDataExchangeType(), e);
		}

		if (delay > 0)
			log.info("Waiting " + delay + " milliseconds before checking Inbound polling for Data Exchange Type " + getDataExchangeType());
	}

	protected static final Map<String, Attribute> POLL_ATTRIBUTE_MAP = new ConcurrentHashMap<>();
	protected static final String tmpPrefixProperty = "Tmp Prefix";
	protected static final String tmpSuffixProperty = "Tmp Suffix";

	static {
		POLL_ATTRIBUTE_MAP.put("HOST", CommonAttrEnum.ATTR_HOST);
		POLL_ATTRIBUTE_MAP.put("PORT", CommonAttrEnum.ATTR_PORT);
		POLL_ATTRIBUTE_MAP.put("USERNAME", CommonAttrEnum.ATTR_USERNAME);
		POLL_ATTRIBUTE_MAP.put("PASSWORD", CommonAttrEnum.ATTR_PASSWORD);
		POLL_ATTRIBUTE_MAP.put("REMOTE PATH", CommonAttrEnum.ATTR_FILE_PATH);
		POLL_ATTRIBUTE_MAP.put("AS ZIP FILE", CommonAttrEnum.ATTR_COMPRESS);
	}
	protected MessageChainStep createPollStep(MessageChain mc, long transportId, int transportTypeId) throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> tranParams = new HashMap<String, Object>(6);
		tranParams.put("transportId", transportId);
		tranParams.put("transportTypeId", transportTypeId);
		MessageChainStep pollStep = mc.addStep("usat.ccs.poll." + transportTypeId);
		pollStep.setCorrelation(transportId);
		MessageChainStep removeStep = mc.addStep("usat.ccs.remove." + transportTypeId);
		String prefix = null;
		String suffix = null;
		final Results propResults = DataLayerMgr.executeQuery("GET_TRANSPORT_PROPERTIES", tranParams, false);
		try {
			while(propResults.next()) {
				String name = ConvertUtils.convert(String.class, propResults.getValue("name"));
				String value = ConvertUtils.convert(String.class, propResults.getValue("value"));
				Attribute a = POLL_ATTRIBUTE_MAP.get(name.toUpperCase());
				if(a != null) {
					boolean sensitive = propResults.getValue("sensitive", Boolean.class);
					if(sensitive) {
						pollStep.setSecretAttribute(a, value);
						removeStep.setSecretAttribute(a, value);
					} else {
						pollStep.setAttribute(a, value);
						removeStep.setAttribute(a, value);
					}
				}
				if(name.equalsIgnoreCase(tmpPrefixProperty))
					prefix = value;
				else if(name.equalsIgnoreCase(tmpSuffixProperty))
					suffix = value;
			}
		} finally {
			propResults.close();
		}
		pollStep.setAttribute(TransportAttrEnum.ATTR_TRANSPORT_TYPE_ID, transportTypeId);
		pollStep.setAttribute(CommonAttrEnum.ATTR_WILDCARD, true);
		if(!StringUtils.isBlank(prefix) || !StringUtils.isBlank(suffix)) {
			// rename
			removeStep.setQueueKey("usat.ccs.rename." + transportTypeId);
			removeStep.setAttribute(CommonAttrEnum.ATTR_FILE_RENAME_MATCH, "(.*/)?([^/]+)");
			StringBuilder sb = new StringBuilder();
			sb.append("$1");
			if(!StringUtils.isBlank(prefix))
				sb.append(prefix.replaceAll("([\\\\$])", "\\\\$1"));
			sb.append("$2");
			if(!StringUtils.isBlank(suffix))
				sb.append(suffix.replaceAll("([\\\\$])", "\\\\$1"));
			removeStep.setAttribute(CommonAttrEnum.ATTR_FILE_RENAME_REPLACE, sb.toString());
		}
		removeStep.setReferenceAttribute(CommonAttrEnum.ATTR_FILE_PATH, pollStep, CommonAttrEnum.ATTR_FILE_PATH);
		pollStep.setNextSteps(0, removeStep);
		return pollStep;
	}

	public long getPollInterval() {
		return pollInterval;
	}

	public void setPollInterval(long pollInterval) {
		this.pollInterval = pollInterval;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public int getDataExchangeType() {
		return dataExchangeType;
	}

	public void setDataExchangeType(int dataExchangeType) {
		this.dataExchangeType = dataExchangeType;
	}
}
