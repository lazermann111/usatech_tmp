package com.usatech.report;

import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.sql.SQLType;

public class GenerateCSVReportTask extends AbstractGenerateReportTask {
	private static final Log log = Log.getLog();

	public GenerateCSVReportTask() {
		super(Generator.CSV);
	}

	/**
	 * @see com.usatech.report.AbstractGenerateReportTask#getContentType()
	 */
	@Override
	protected String getContentType() {
		return generator.getContentType();
	}
	/**
	 * @see com.usatech.report.AbstractGenerateTask#generateReport(int, java.util.Map, java.io.OutputStream)
	 */
	@Override
	protected void generateReport(String title, int userId, Map<String, Object> params, OutputStream out, StandbyAllowance standbyAllowance) throws Exception {
		generateCSVReport(params, new PrintStream(out, true), standbyAllowance);

	}

	protected int generateCSVReport(Map<String, Object> params, PrintStream out, StandbyAllowance standbyAllowance) throws SQLException, IllegalArgumentException, ConvertException, DataLayerException {
		int rows = 0;
		String query = ConvertUtils.getString(params.get("query"),false);
		if (query == null || query.trim().length() == 0)
			throw new IllegalArgumentException(
					"Necessary parameter, 'query', not found");
		params.remove("query"); // no need to do anything with this below
		String[] parameterNames = ConvertUtils.convertSafely(String[].class, params.get("paramNames"), null);
		SQLType[] parameterTypes = ConvertUtils.convertSafely(SQLType[].class, params.get("paramTypes"), null);
		Object[] parameterValues;
		if(parameterNames == null || parameterNames.length == 0) {
			int n = 0;
			if(parameterTypes != null)
				n = parameterTypes.length;
			while(params.containsKey("params." + (n + 1)))
				n++;
			parameterValues = new Object[n];
			for(int i = 0; i < n; i++)
				parameterValues[i] = params.get("params." + (i + 1));
		} else {
			int n = parameterNames.length;
			if(parameterTypes != null && parameterTypes.length > n)
				n = parameterTypes.length;
			parameterValues = new Object[n];
			for(int i = 0; i < n; i++) {
				if(i < parameterNames.length && parameterNames[i] != null)
					parameterValues[i] = params.get("params." + parameterNames[i]);
			}
		}
		String dsn;
		if(query.indexOf("RDW.") > 0)
			dsn = "MAIN";
		else {
			switch(standbyAllowance) {
				case ALLOW:
				case WAIT_ON_REQUEST:
					dsn = "REPORT_STANDBY";
					break;
				default:
					dsn = "REPORT";
			}
		}
		log.info("Executing SQL '" + query + "' on " + dsn);
		Results results;
		try {
			results = DataLayerMgr.executeSQLx(dsn, query, parameterValues, parameterTypes);
		} catch (SQLException sqle) {
			log.warn("Exception while executing CSV Report Query: " + query);
			throw sqle;
		} catch (DataLayerException e) {
			log.warn("Exception while executing CSV Report Query: " + query);
			throw e;
		}

		boolean header = ConvertUtils.getBoolean(params.get("header"), false);
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			if (!entry.getKey().startsWith("FORMAT@"))
				continue;
			if (entry.getValue() != null) {
				String cn = entry.getKey().substring("FORMAT@".length());
				try {
					int col = Integer.parseInt(cn);
					results.setFormat(col, ConvertUtils.getStringSafely(entry.getValue()));
				} catch (NumberFormatException e) {
					try {
						results.setFormat(cn, ConvertUtils.getStringSafely(entry.getValue()));
					} catch (IllegalArgumentException e1) {
						log.warn("Column '" + cn
								+ "' does not exist in this resultset", e1);
					}
				}
			}
		}

		int columns = results.getColumnCount();
		boolean[] quote = new boolean[columns];
		for (int i = 1; i <= columns; i++)
			quote[i - 1] = !Number.class.isAssignableFrom(results
					.getColumnClass(i));
		if (header) {
			for (int i = 1; i <= columns; i++) {
				if (i > 1)
					out.print(',');
				out.print('"');
				out.print(results.getColumnName(i));
				out.print('"');
			}
			out.println();
		}
		while (results.next()) {
			for (int i = 1; i <= columns; i++) {
				if (i > 1)
					out.print(',');
				if (quote[i - 1])
					out.print('"');
				out.print(results.getFormattedValue(i));
				if (quote[i - 1])
					out.print('"');
			}
			out.println();
			rows++;
		}
		if (rows == 0)
			out.print(Messages.getInstance().getMessage("NO_ROWS_FOR_CSV"));
		return rows;
	}
}
