/**
 *
 */
package com.usatech.report;

import simple.bean.ConvertUtils;
import simple.xml.XMLizable;

/**
 * @author Brian S. Krug
 *
 */
public class BasicReport implements XMLizable {
	protected String title;


	public BasicReport(String title) {
		this.title = title;
	}

	public BasicReport() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public int hashCode() {
		return title == null ? 0 : title.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof BasicReport))
			return false;
		BasicReport other = (BasicReport) obj;
		return ConvertUtils.areEqual(title, other.title);
	}
}
