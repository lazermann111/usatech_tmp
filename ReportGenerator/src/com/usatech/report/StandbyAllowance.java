package com.usatech.report;

public enum StandbyAllowance {
	NONE, WAIT_ON_REQUEST, ALLOW
}
