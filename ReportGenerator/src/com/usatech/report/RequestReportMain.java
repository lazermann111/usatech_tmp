package com.usatech.report;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import simple.app.Base;
import simple.app.MainWithConfig;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.results.Results;
import simple.util.CollectionUtils;

import com.usatech.report.Frequency.DateRange;

/**
 * This class now is single threaded.
 *
 * @author yhe
 *
 */
public class RequestReportMain extends MainWithConfig {
	private static final Log log = Log.getLog();

	protected FrequencyManager frequencyManager = new FrequencyManager();
	protected Publisher<ByteInput> publisher;
	protected Properties props;	
	protected ReportRequestFactory reportRequestFactory;

	// protected static final Map<String,Format> formats = new
	// HashMap<String,Format>();
	protected static final java.text.SimpleDateFormat batchDateFormat = new java.text.SimpleDateFormat("yyyyMMdd-HHmmss");

	public static final Choice CUSTOMER_FILLIN = new Choice("Customer Id", "{c}");
	public static final Choice USER_FILLIN = new Choice("User Id", "{u}");
	public static final Choice TERMINAL_FILLIN = new Choice("All terminals", "{t}");
	public static final Choice FREQUENCY_FILLIN = new Choice("Frequency Name", "{f}");
	public static final Choice REPORT_FILLIN = new Choice("Report Name", "{r}");
	public static final Choice BEGIN_FILLIN = new Choice("Begin Date", "{b}");
	public static final Choice END_FILLIN = new Choice("End Date", "{e}");
	public static final Choice BATCH_FILLIN = new Choice("Batch Id", "{x}");
	public static final Choice DATE_FILLIN = new Choice("Current Date", "{d}");
	public static final Choice SPECIFIC_DEVICE_FILLIN = new Choice("Name of triggering device", "{s}");

	protected static final String TRANSPORT_REASON_BATCH_REPORT = "BATCH_REPORT";
	protected static final String TRANSPORT_REASON_TIMED_REPORT = "TIMED_REPORT";

	protected class BatchReport {
		protected int userReportId;
		protected int userId;
		protected java.sql.Timestamp updateDate;
		protected int reportId;
		protected int generatorId;
		protected String title;
		protected String name;
		protected int batchType;

		public int getBatchType() {
			return batchType;
		}

		public void setBatchType(int batchType) {
			this.batchType = batchType;
		}

		public int getGeneratorId() {
			return generatorId;
		}

		public void setGeneratorId(int generatorId) {
			this.generatorId = generatorId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getReportId() {
			return reportId;
		}

		public void setReportId(int reportId) {
			this.reportId = reportId;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public java.sql.Timestamp getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(java.sql.Timestamp updateDate) {
			this.updateDate = updateDate;
		}

		public int getUserId() {
			return userId;
		}

		public void setUserId(int userId) {
			this.userId = userId;
		}

		public int getUserReportId() {
			return userReportId;
		}

		public void setUserReportId(int userReportId) {
			this.userReportId = userReportId;
		}
	}

	protected class TimedReport {
		protected int userReportId;
		protected Date lastEndDate;
		protected Integer frequencyId;
		protected double latency = 0.0; // the number of days to wait to send

		public Integer getFrequencyId() {
			return frequencyId;
		}

		public void setFrequencyId(Integer frequencyId) {
			this.frequencyId = frequencyId;
		}

		public Date getLastEndDate() {
			return lastEndDate;
		}

		public void setLastEndDate(Date lastEndDate) {
			this.lastEndDate = lastEndDate;
		}

		public double getLatency() {
			return latency;
		}

		public void setLatency(double latency) {
			this.latency = latency;
		}

		public int getUserReportId() {
			return userReportId;
		}

		public void setUserReportId(int userReportId) {
			this.userReportId = userReportId;
		}
	}

	@Override
	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "propertiesFile", true, true, "properties-file", "The properties file to use");
	}

	@Override
	public void run(String[] args) {
		Map<String, Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		try {
			props = getProperties(ConvertUtils.getStringSafely(argMap.get("properties-file")), getClass(), null);
			Properties subProperties = CollectionUtils.getSubProperties(props, "simple.app.Publisher.");
			Base.configureDataSourceFactory(props, null);
			Base.configureDataLayer(props);
			HashMap<String, Object> namedReferences = new HashMap<String, Object>();
			publisher = ConvertUtils.cast(simple.app.Main.configure(Publisher.class, subProperties, props, namedReferences, true));
			reportRequestFactory = ConvertUtils.cast(simple.app.Main.configure(ReportRequestFactory.class, subProperties, props, namedReferences, true));
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		} catch(ServiceException e) {
			finishAndExit("Could not configure publisher", 120, e);
			return;
		}
		for(Map.Entry<String, Object> entry : argMap.entrySet()) {
			props.put(entry.getKey(), entry.getValue());
		}
		
		try {
			frequencyManager.loadBasicFrequencies("GET_FREQUENCIES");
			generateUnsentBatchReports();
			generateUnsentTimedReports();
			generateResendTimedReports();
		} catch(SQLException e) {
			log.warn("Failure in RequestReportMain run method", e);
		} catch(ConvertException e) {
			log.warn("Failure in RequestReportMain run method", e);
		} catch(DataLayerException e) {
			log.warn("Failure in RequestReportMain run method", e);
		} catch(BeanException e) {
			log.warn("Failure in RequestReportMain run method", e);
		} catch(ServiceException e) {
			log.warn("Failure in RequestReportMain run method", e);
		} catch(InvalidIntValueException e) {
			log.warn("Failure in RequestReportMain run method", e);
		}
		Log.finish();
		System.exit(0);
	}

	public void generateResendTimedReports() throws SQLException, DataLayerException, ConvertException, BeanException, ServiceException {
		// handle timed reports that are scheduled for resend
		final Results resendResults = DataLayerMgr.executeQuery("GET_RESENT_TIMED_REPORTS", null, false);
		while(resendResults.next()) {
			int userReportId = ConvertUtils.getInt(resendResults.getValue("userReportId"));
			Date beginDate = ConvertUtils.convert(Date.class, resendResults.getValue("beginDate"));
			Date endDate = ConvertUtils.convert(Date.class, resendResults.getValue("endDate"));
			Long refreshStoredFileId = ConvertUtils.convert(Long.class, resendResults.getValue("refreshStoredFileId"));
			generateTimedReport(userReportId, beginDate, endDate, refreshStoredFileId);
		}
	}

	public void generateUnsentTimedReports() throws SQLException, DataLayerException, ConvertException, BeanException,ServiceException {
		final Results results = DataLayerMgr.executeQuery("GET_TIMED_REPORTS", null, false);
		while(results.next()) {
			TimedReport rpt = new TimedReport();
			try {
				results.fillBean(rpt);
				Frequency frequency = frequencyManager.getFrequency(rpt.getFrequencyId());
				if(frequency == null) {
					log.warn("Unknown Frequency: '" + rpt.getFrequencyId() + "' - skipping user_report_id = " + rpt.getUserReportId());
				} else {
					DateRange dateRange = frequency.nextDateRange(rpt.getLastEndDate());
					Date now = new Date(System.currentTimeMillis() - (long) (24 * 60 * 60 * 1000 * rpt.getLatency())); // give
					// specified leeway
					while(dateRange.endDate.before(now)) {
						generateTimedReport(rpt.getUserReportId(), dateRange.beginDate, dateRange.endDate, null);
						dateRange = frequency.nextDateRange(dateRange);
					}
				}
			} catch(ServiceException e) {
				log.warn("Could not publish report request for user report " + rpt.userReportId, e);
			}
		}
	}

	public void generateTimedReport(int userReportId, java.util.Date beginDate, java.util.Date endDate, Long refreshStoredFileId) throws ServiceException {
		log.info("Generating Timed Report " + userReportId + " for " + beginDate + " to " + endDate);
		int userId;
		String frequencyName;
		int reportId;
		Generator generator;
		String title;
		try {
			Results results = DataLayerMgr.executeQuery("GET_REPORT_INFO", new Object[] { userReportId }, false);
			try {
				if(!results.next())
					throw new SQLException("Zero rows returned");
				userId = ConvertUtils.getInt(results.getValue("userId"));
				frequencyName = ConvertUtils.convert(String.class, results.getValue("frequencyName"));
				reportId = ConvertUtils.getInt(results.getValue("reportId"));
				generator = ConvertUtils.convertRequired(Generator.class, results.getValue("generatorId"));
				title = ConvertUtils.convert(String.class, results.getValue("title"));
			} finally {
				results.close();
			}
		} catch(ConvertException e) {
			throw new ServiceException(e);
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}

		reportRequestFactory.generateReport(ReportScheduleType.TIMED, userReportId, userId, reportId, generator, title, null, beginDate, endDate, frequencyName, refreshStoredFileId, "");
	}

	public void generateUnsentBatchReports() throws SQLException, DataLayerException, ConvertException, BeanException, InvalidIntValueException {
		final Results results = DataLayerMgr.executeQuery("GET_UNSENT_BATCH_REPORTS", null, false);
		while(results.next()) {
			BatchReport rpt = new BatchReport();
			results.fillBean(rpt);
			// now find unsent for that type
			ReportScheduleType batchType = ReportScheduleType.getByValue(rpt.batchType);
			String callId;
			switch(batchType) {
				case DAILY_EXPORT: // trans
					callId = "GET_UNSENT_TRAN_BATCHES";
					break;
				case DEX: // DEX
					callId = "GET_UNSENT_DEX_BATCHES";
					break;
				case RDC: // RDC (Best Vendor group DEX)
					callId = "GET_UNSENT_DEX_RDC_BATCHES";
					break;
				case PAYMENT: // EFT
					callId = "GET_UNSENT_DOC_BATCHES";
					break;
				case PENDING_PAYMENT: // Locked EFT
					callId = "GET_UNSENT_LOCKED_DOC_BATCHES";
					break;
				default:
					callId = null; // should never happen
			}
			Results batchResults = DataLayerMgr.executeQuery(callId, rpt, false);
			while(batchResults.next()) {
				long batchId;
				try {
					batchId = ConvertUtils.getLong(batchResults.getValue(1));
					Long refreshStoredFileId = ConvertUtils.convert(Long.class, batchResults.getValue("refreshStoredFileId"));
					log.info("Generating Batch Report " + rpt.userReportId + " for " + batchId);
					reportRequestFactory.generateReport(batchType, rpt.userReportId, rpt.userId, rpt.reportId, Generator.getByValue(rpt.generatorId), rpt.title, batchId, null, null, batchType.getDescription(), refreshStoredFileId, "");
				} catch(ConvertException e) {
					log.error("Batch Id '" + batchResults.getValue(1) + "' is invalid", e);
					continue;
				} catch(ServiceException e) {
					log.warn("Could not publish report request for user report " + rpt.userReportId, e);
				} catch(InvalidIntValueException e) {
					log.warn("Could not convert generator id for user report " + rpt.userReportId, e);
				}
			}
		}
	}


	public static void main(String[] args) {
		new RequestReportMain().run(args);
	}

	public ReportRequestFactory getReportRequestFactory() {
		return reportRequestFactory;
	}

	public void setReportRequestFactory(ReportRequestFactory reportRequestFactory) {
		this.reportRequestFactory = reportRequestFactory;
	}
}
