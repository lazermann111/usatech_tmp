package com.usatech.report;

import java.sql.SQLException;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
/**
 * Used for requesting batch report. 
 * batchType and batchSqlCallId are configurable for 4 types of batch supported.
 * @author yhe
 *
 */
public class RequestBatchReportTask extends AbstractRequestTask {
	private static final Log log = Log.getLog();
	
	protected ReportScheduleType batchType;
	protected String batchSqlCallId;
	
	@Override
	protected String getListCallId() {
		return "GET_UNSENT_BATCH_REPORTS_BY_BATCH_TYPE";
	}

	@Override
	protected Object getListCallParams() {
		return new Object[] { batchType };
	}

	@Override
	protected String getProcessCd() {
		return batchType + "_REPORT_REQUEST";
	}

	@Override
	protected String getProcessDesc() {
		return "Unsent Batch " + batchType + " Request";
	}

	@Override
	protected void handleItem(Map<String, Object> itemParams) {
		int userReportId;
		try {
			userReportId = ConvertUtils.getInt(itemParams.get("userReportId"));
		} catch(ConvertException e) {
			log.error("Could not convert userReportId", e);
			return;
		}
		int userId;
		int reportId;
		Generator generator;
		String title;
		try {
			userId = ConvertUtils.getInt(itemParams.get("userId"));
			reportId = ConvertUtils.getInt(itemParams.get("reportId"));
			generator = ConvertUtils.convertRequired(Generator.class, itemParams.get("generatorId"));
			title = ConvertUtils.convert(String.class, itemParams.get("title"));
		} catch(ConvertException e) {
			log.error("Could not set convert properties of user report " + userReportId, e);
			return;
		}
		try {
			Results batchResults = DataLayerMgr.executeQuery(batchSqlCallId, itemParams, false);
			while(batchResults.next()) {
				long batchId;
				try {
					batchId = ConvertUtils.getLong(batchResults.getValue(1));
					Long refreshStoredFileId = ConvertUtils.convert(Long.class, batchResults.getValue("refreshStoredFileId"));
					log.info("Generating Batch Report " + userReportId + " for " + batchId);
					reportRequestFactory.generateReport(batchType, userReportId, userId, reportId, generator, title, batchId, null, null, batchType.getDescription(), refreshStoredFileId, "");
				} catch(ConvertException e) {
					log.error("Batch Id '" + batchResults.getValue(1) + "' is invalid", e);
					continue;
				} catch(ServiceException e) {
					log.error("Could not publish report request for user report " + userReportId, e);
				}
			}
		} catch(SQLException e) {
			log.error("Could not generate batch report request for user report " + userReportId, e);
		} catch(DataLayerException e) {
			log.error("Could not generate batch report request for user report " + userReportId, e);
		}
	}

	public ReportScheduleType getBatchType() {
		return batchType;
	}

	public void setBatchType(ReportScheduleType batchType) {
		this.batchType = batchType;
	}

	public String getBatchSqlCallId() {
		return batchSqlCallId;
	}

	public void setBatchSqlCallId(String batchSqlCallId) {
		this.batchSqlCallId = batchSqlCallId;
	}
}
