package com.usatech.report;

import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.ServletException;

import org.quartz.CronExpression;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.ec.ECResponse;
import com.usatech.ec2.EC2AuthResponse;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.util.BackendPayorUtils;
import com.usatech.layers.common.util.DeviceUtils;

import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.results.Results;
import simple.text.MessageFormat;
import simple.text.StringUtils;

/**
 * Finds Recurring Charges that need to be processed and does so
 * 
 * @author bkrug
 * 
 */
public class RecurChargeTask implements SelfProcessor, MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));
	protected static final String PROCESS_CD = "RECUR_CHARGE";
	protected long recheckInterval = 7200000L;
	protected long lockUpdateInterval = 60000L;
	protected int maxItems = 1000;
	protected MessageFormat referenceFormat = new MessageFormat("#{1}");
	protected String baseUrl;
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition signal = lock.newCondition();
	protected static int exceptionRetryIntervalHour=1;
	protected static int maxRetryCount=5;
	
	
	
	public static int getExceptionRetryIntervalHour() {
		return exceptionRetryIntervalHour;
	}

	public static void setExceptionRetryIntervalHour(int exceptionRetryIntervalHour) {
		RecurChargeTask.exceptionRetryIntervalHour = exceptionRetryIntervalHour;
	}

	public static int getMaxRetryCount() {
		return maxRetryCount;
	}

	public static void setMaxRetryCount(int maxRetryCount) {
		RecurChargeTask.maxRetryCount = maxRetryCount;
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		log.info("Waking up recur charge checker");
		lock.lock();
		try {
			signal.signalAll();
		} finally {
			lock.unlock();
		}
		return 0;
	}

	@Override
	public void process() {
		long startTs = System.currentTimeMillis();
		long totalItemCount = 0;
		final Map<String, Object> lockParams = new HashMap<String, Object>();
		final AtomicBoolean locked = new AtomicBoolean(true);
		try {
			if(!InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams)) {
				log.info("Recur Charge is already locked by instance " + lockParams.get("lockedBy"));
				return;
			}
			final Timer timer = new Timer(getClass().getName() + "_Timer");
			try {
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						try {
							if(locked.get() && !InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams)) {
								log.info("Recur Charge is already locked by instance " + lockParams.get("lockedBy"));
								locked.set(false);
								timer.cancel();
							}
						} catch(Throwable e) {
							log.warn("Error while renewing lock on Recur Charge", e);
							locked.set(false);
							timer.cancel();
						}
					}
				}, getLockUpdateInterval(), getLockUpdateInterval());
				final Map<String, Object> params = new HashMap<String, Object>();
				while(locked.get()) {
					params.put("maxItems", getMaxItems());
					params.put("exceptionRetryIntervalHour", getExceptionRetryIntervalHour());
					params.put("maxRetryCount", getMaxRetryCount());
					try (Connection conn = DataLayerMgr.getConnection("REPORT")) {
						log.info("Checking for recur charges");
						Set<Long> ignorePayorIds = null;
						try (Results results = DataLayerMgr.executeQuery(conn, "NEXT_RECUR_CHARGES", params)) {
							while(results.next()) {
								long payorId = results.getValue("payorId", long.class);
								try {
									chargePayorRecur(payorId, conn,params);
								} catch(ServiceException e) {
									log.info("Error processing recurring charge for payor " + payorId, e);
									params.put("payorId", payorId);
									params.put("recurException", StringUtils.exceptionToString(e));
									DataLayerMgr.executeCall(conn, "UPDATE_PAYOR_RECUR_EXCEPTION", params);
									conn.commit();
									if(ignorePayorIds == null)
										ignorePayorIds = new HashSet<>();
									ignorePayorIds.add(payorId);
								}
							}
						}
						params.clear();
						params.put("ignorePayorIds", ignorePayorIds);
						params.put("exceptionRetryIntervalHour", getExceptionRetryIntervalHour());
						params.put("maxRetryCount", getMaxRetryCount());
						DataLayerMgr.selectInto(conn, "EARLIEST_RECUR_TS", params);
					}
					long earliestRecurTs = ConvertUtils.getLong(params.get("earliestRecurTs"), -1L);
					long delay;
					if(earliestRecurTs > 0L) {
						log.info("Next recur charge is scheduled for " + ConvertUtils.formatObject(new Date(earliestRecurTs), "DATE:MM/dd/yyyy HH:mm:ss"));
						delay = earliestRecurTs - System.currentTimeMillis();
						if(delay > getRecheckInterval())
							delay = getRecheckInterval();
					} else {
						log.info("No recur charge is scheduled");
						delay = getRecheckInterval();
					}
					if(delay > 0) {
						lock.lock();
						try {
							signal.await(delay, TimeUnit.MILLISECONDS);
						} finally {
							lock.unlock();
						}
					}
				}
			} finally {
				timer.cancel();
			}
		} catch(Exception e) {
			log.error("Error processing Recur Charge", e);
		} finally {
			try {
				locked.set(false);
				InteractionUtils.unlockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams);
			} catch(Exception e) {
				log.error("Error unlocking " + PROCESS_CD, e);
			}
			if(totalItemCount > 0)
				log.info("Processed " + totalItemCount + " Recur Charge items in " + (System.currentTimeMillis() - startTs) + " ms");
		}
	}

	public boolean chargePayorRecur(long payorId, Connection conn, Map<String, Object> paramsForError) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("payorId", payorId);
		try {
			boolean okay = false;
			try {
				DataLayerMgr.executeCall(conn, "BEGIN_PAYOR_RECUR_CHARGE", params); // get device serial cd, device_id, cardId, token, payor name, card #, timezone
				byte[] token = ByteArrayUtils.fromHex(ConvertUtils.getString(params.get("token"), false));
				if(token == null || token.length == 0) {
					throw new ServiceException("This payor's credentials are not saved. Please edit the payor and add their card info again");
				}
				BigDecimal recurChargeAmount = ConvertUtils.convert(BigDecimal.class, params.get("recurAmount"));
				if(recurChargeAmount == null || recurChargeAmount.signum() == 0) {
					log.info("Skipping recurring charge for payor " + payorId + " because amount is not specified");
					return false;
				}
				if(recurChargeAmount.signum() < 0) {
					log.info("Skipping recurring charge for payor " + payorId + " because amount is negative");
					return false;
				}
				String recurSchedule = ConvertUtils.getString(params.get("recurSchedule"), false);
				if(StringUtils.isBlank(recurSchedule)) {
					log.info("Skipping recurring charge for payor " + payorId + " because schedule is blank");
					return false;
				}
				Date recurStartTs = ConvertUtils.convert(Date.class, params.get("recurStartTs"));
				if(recurStartTs == null) {
					log.info("Skipping recurring charge for payor " + payorId + " because start time is blank");
					return false;
				}
				Date recurEndTs = ConvertUtils.convert(Date.class, params.get("recurEndTs"));
				Date recurLastTs = ConvertUtils.convert(Date.class, params.get("recurLastTs"));

				if(recurLastTs == null || recurLastTs.before(recurStartTs))
					recurLastTs = recurStartTs;
				Date currTs = new Date();
				if(recurEndTs != null && !recurLastTs.before(recurEndTs)) {
					log.info("Skipping recurring charge for payor " + payorId + " because end time is not greater than the last charge time");
					return false;
				}
				if(recurLastTs.after(currTs)) {
					log.info("Skipping recurring charge for payor " + payorId + " because the start time is in the future");
					return false;
				}

				CronExpression ce;
				try {
					ce = new CronExpression(recurSchedule);
				} catch(ParseException e) {
					log.warn("Skipping recurring charge for payor " + payorId + " because the schedule expression '" + recurSchedule + "' is invalid", e);
					return false;
				}
				String timeZoneGuid = ConvertUtils.getString(params.get("timeZoneGuid"), false);
				if(!StringUtils.isBlank(timeZoneGuid))
					ce.setTimeZone(TimeZone.getTimeZone(timeZoneGuid));
				Date nextTs = ce.getTimeAfter(recurLastTs);
				if(recurEndTs != null && !nextTs.before(recurEndTs)) {
					log.info("Skipping recurring charge for payor " + payorId + " because the next time is after the end time");
					params.put("nextTs", nextTs);
					DataLayerMgr.executeCall(conn, "UPDATE_PAYOR_RECUR_NEXT", params);
					okay = true;
					conn.commit();
					return false;
				}
				if(nextTs.after(currTs)) {
					log.info("Skipping recurring charge for payor " + payorId + " because the next time is in the future");
					params.put("nextTs", nextTs);
					DataLayerMgr.executeCall(conn, "UPDATE_PAYOR_RECUR_NEXT", params);
					okay = true;
					conn.commit();
					return false;
				}
				Date futureTs = ce.getTimeAfter(nextTs);
				int nextInstallment = 1 + ConvertUtils.getInt(params.get("recurInstallment"), 0);
				// Do the charge
				String deviceSerialCd = ConvertUtils.getString(params.get("deviceSerialCd"), true);
				long deviceTranCd = DeviceUtils.getNextMasterIdBySerial(deviceSerialCd, conn);
				long cardId = ConvertUtils.getLong(params.get("cardId"));
				String payorName = ConvertUtils.getString(params.get("payorName"), true);
				String cardNumber = ConvertUtils.getString(params.get("cardNumber"), true);
				String doingBusinessAs = ConvertUtils.getString(params.get("doingBusinessAs"), true);
				String userName = ConvertUtils.getString(params.get("userName"), true);
				String payorEmail = ConvertUtils.getString(params.get("payorEmail"), false);
				String recurDescFormat = ConvertUtils.getString(params.get("recurDescFormat"), false);

				Object[] formatParams = new Object[] { nextTs, nextInstallment };
				String chargeReference = referenceFormat == null ? null : referenceFormat.format(formatParams);
				String chargeDesc;
				if(StringUtils.isBlank(recurDescFormat)) {
					if(!StringUtils.isBlank(doingBusinessAs))
						chargeDesc = "Online Charge By " + doingBusinessAs.replace('|', '/') + " - " + ConvertUtils.formatObject(nextTs, "DATE:MM/dd/yyyy");
					else
						chargeDesc = "Online Charge - " + ConvertUtils.formatObject(nextTs, "DATE:MM/dd/yyyy");
				} else
					chargeDesc = new MessageFormat(recurDescFormat).format(formatParams);

				EC2AuthResponse response = BackendPayorUtils.chargePayor(payorId, deviceSerialCd, deviceTranCd, cardId, token, userName, doingBusinessAs, recurChargeAmount, chargeReference, nextTs, chargeDesc);
				String maskedCardNumber = MessageResponseUtils.maskCardNumber(cardNumber);
				switch(response.getReturnCode()) {
					case ECResponse.RES_APPROVED:
					case ECResponse.RES_CVV_MISMATCH:
					case ECResponse.RES_AVS_MISMATCH:
					case ECResponse.RES_CVV_AND_AVS_MISMATCH:
						params.put("chargeAmount", recurChargeAmount);
						params.put("chargeTs", currTs);
						params.put("lastTs", nextTs);
						params.put("nextTs", futureTs);
						params.put("installment", nextInstallment);
						DataLayerMgr.executeCall(conn, "FINISH_PAYOR_RECUR_CHARGE", params);
						okay = true;
						conn.commit();
						log.info("Charged payor {0} on {1} card {2} for {3,CURRENCY} for recur #{4} on {5,DATE,MM/dd/yyyy}", payorId, response.getCardType(), maskedCardNumber, recurChargeAmount, nextInstallment, nextTs);
						if(!StringUtils.isBlank(payorEmail)) {
							try {
								String address = ConvertUtils.getString(params.get("address"), false);
								String city = ConvertUtils.getString(params.get("city"), false);
								String stateCd = ConvertUtils.getString(params.get("stateCd"), false);
								String postalCd = ConvertUtils.getString(params.get("postalCd"), false);
								String countryCd = ConvertUtils.getString(params.get("countryCd"), false);
								String phone = ConvertUtils.getString(params.get("phone"), false);
								String email = ConvertUtils.getString(params.get("email"), false);
								BackendPayorUtils.sendReceipt(payorName, payorEmail, baseUrl, deviceTranCd, deviceSerialCd, maskedCardNumber, response.getCardType(), currTs.getTime(), recurChargeAmount, chargeDesc, nextTs, chargeReference, doingBusinessAs, address, city, stateCd, postalCd, countryCd, phone, email, Locale.getDefault());
							} catch(ServletException | SQLException | DataLayerException | ConvertException e) {
								log.error("Could not send receipt for recurring charge to payor " + payorId, e);
								// XXX: Should we do something else here?
							}
						}
						return true;
					default:
						paramsForError.put("recurErrorMsg", response.getReturnMessage());
						if(params.get("recurNextTs") == null) {
							log.info("Updating recur next time for payor " + payorId + " because it is null");
							params.put("nextTs", nextTs);
							DataLayerMgr.executeCall(conn, "UPDATE_PAYOR_RECUR_NEXT", params);
							okay = true;
							conn.commit();
						}
						switch(response.getReturnCode()) {
							case ECResponse.RES_DECLINED:
								throw new ServiceException("Could not charge Account because the " + response.getCardType() + " card " + maskedCardNumber + " was declined. Please use a different card. Details: " + response.getReturnMessage() + ".");
							case ECResponse.RES_RESTRICTED_DEBIT_CARD_TYPE:
								throw new ServiceException("Could not charge Account because the " + response.getCardType() + " card " + maskedCardNumber + " was declined because debit cards are not accepted. Please use a different card. Details: " + response.getReturnMessage() + ".");
							case ECResponse.RES_RESTRICTED_PAYMENT_METHOD:
								throw new ServiceException("Could not charge Account because the " + response.getCardType() + " card " + maskedCardNumber + " is not a valid card. Please use a different card. Details: " + response.getReturnMessage() + ".");
							default:
								throw new ServiceException("Failed while processing this request: " + response.getReturnMessage());
						}
				}
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
			}
		} catch(SQLException | DataLayerException | ConvertException e) {
			log.warn("Could not charge payor", e);
			return false;
		}
	}
	public long getLockUpdateInterval() {
		return lockUpdateInterval;
	}

	public void setLockUpdateInterval(long lockUpdateInterval) {
		this.lockUpdateInterval = lockUpdateInterval;
	}

	public int getMaxItems() {
		return maxItems;
	}

	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}

	public long getRecheckInterval() {
		return recheckInterval;
	}

	public void setRecheckInterval(long recheckInterval) {
		this.recheckInterval = recheckInterval;
	}

	public String getReferenceFormat() {
		return referenceFormat == null ? null : referenceFormat.toPattern();
	}

	public void setReferenceFormat(String referenceFormat) {
		if(StringUtils.isBlank(referenceFormat))
			this.referenceFormat = null;
		else
			this.referenceFormat = new MessageFormat(referenceFormat);
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
}
