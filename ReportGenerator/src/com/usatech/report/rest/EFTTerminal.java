package com.usatech.report.rest;

import java.math.BigDecimal;
import java.util.ArrayList;

public class EFTTerminal {
	protected String terminalCode=new String("Unknown");
	protected BigDecimal failedAmount=new BigDecimal("0.00");
	protected BigDecimal feeAmount=new BigDecimal("0.00");
	protected BigDecimal chargebackAmount=new BigDecimal("0.00");
	protected BigDecimal refundAmount=new BigDecimal("0.00");
	protected BigDecimal adjustmentAmount=new BigDecimal("0.00");
	protected ArrayList<EFTTran> eftTrans = new ArrayList<EFTTran>();
	public EFTTerminal() {
		super();
	}
	public void addEFTTran(EFTTran eftTran){
		eftTrans.add(eftTran);
	}
	public ArrayList<EFTTran> getEftTrans() {
		return eftTrans;
	}
	public void setEftTrans(ArrayList<EFTTran> eftTrans) {
		this.eftTrans = eftTrans;
	}
	
	public String getTerminalCode() {
		return terminalCode;
	}
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}
	public BigDecimal getFailedAmount() {
		return failedAmount;
	}
	public void setFailedAmount(BigDecimal failedAmount) {
		this.failedAmount = failedAmount;
	}
	public BigDecimal getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}
	public BigDecimal getChargebackAmount() {
		return chargebackAmount;
	}
	public void setChargebackAmount(BigDecimal chargebackAmount) {
		this.chargebackAmount = chargebackAmount;
	}
	public BigDecimal getRefundAmount() {
		return refundAmount;
	}
	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}
	public BigDecimal getAdjustmentAmount() {
		return adjustmentAmount;
	}
	public void setAdjustmentAmount(BigDecimal adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}
	
	
}
