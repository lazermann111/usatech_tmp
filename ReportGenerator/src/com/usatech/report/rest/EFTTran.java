package com.usatech.report.rest;

import java.math.BigDecimal;

public class EFTTran {
	protected String txnCode;
	protected BigDecimal txnAmount;
	public EFTTran(String txnCode, BigDecimal txnAmount) {
		super();
		this.txnCode = txnCode;
		this.txnAmount = txnAmount;
	}
	
	public EFTTran() {
		super();
	}

	public String getTxnCode() {
		return txnCode;
	}
	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}
	public BigDecimal getTxnAmount() {
		return txnAmount;
	}
	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}
	
}
