package com.usatech.report.rest;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class BigDecimalConverter implements Converter {

	protected final static DecimalFormat df = new DecimalFormat("######0.00");

	@Override
	public boolean canConvert(Class clazz) {
		return clazz.equals(BigDecimal.class);
	}

	public void marshal(Object value, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		if (value == null) {
			writer.setValue("0.00");
		} else {
			writer.setValue(df.format((BigDecimal) value));
		}

	}

	@Override
	public Object unmarshal(HierarchicalStreamReader arg0,
			com.thoughtworks.xstream.converters.UnmarshallingContext arg1) {
		Object value = arg0.getValue();
		if (value != null) {
			return new BigDecimal(value.toString());
		}
		return value;
	}

}
