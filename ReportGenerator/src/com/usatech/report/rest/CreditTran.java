package com.usatech.report.rest;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import simple.text.ThreadSafeDateFormat;

public class CreditTran{
	
	public CreditTran() {
		super();
	}
	protected String vendorCode="USAT";
	protected int customerCode;
	protected String terminalCode;
	protected String txnCode;
	protected Date txnDateTime;
	protected String txnDateTimeString;
	protected String txnType;
	protected String txnDescription;
	protected BigDecimal txnAmount;
	protected String statusCode;
	protected String cardNo;
	protected String approvalCode;
	
	protected static final DateFormat tranDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
	
	
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public int getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(int customerCode) {
		this.customerCode = customerCode;
	}
	public String getTerminalCode() {
		return terminalCode;
	}
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}
	public String getTxnCode() {
		return txnCode;
	}
	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}
	
	public Date getTxnDateTime() {
		return txnDateTime;
	}
	public void setTxnDateTime(Date txnDateTime) {
		this.txnDateTime = txnDateTime;
		if(txnDateTime!=null){
			txnDateTimeString=tranDateFormat.format(txnDateTime);
		}
	}
	public String getTxnDateTimeString() {
		return txnDateTimeString;
	}
	public void setTxnDateTimeString(String txnDateTimeString) {
		this.txnDateTimeString = txnDateTimeString;
	}
	public BigDecimal getTxnAmount() {
		return txnAmount;
	}
	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getTxnDescription() {
		return txnDescription;
	}
	public void setTxnDescription(String txnDescription) {
		this.txnDescription = txnDescription;
	}
	
}