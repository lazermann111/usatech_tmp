package com.usatech.report.rest;


import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class CreditData {
	
	protected String version="1.0";
	protected ArrayList<CreditTran> creditTrans = new ArrayList<CreditTran>();
	

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public ArrayList<CreditTran> getCreditTrans() {
		return creditTrans;
	}

	public void setCreditTrans(ArrayList<CreditTran> creditTrans) {
		this.creditTrans = creditTrans;
	}
	
	public void add(CreditTran tran){
		creditTrans.add(tran);
	}
	
	
	protected final static XStream xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("_-", "_"))) ;
	
    static {
    	xstream.registerConverter(new BigDecimalConverter());
		xstream.alias("Credit_Data", CreditData.class);
		xstream.alias("Credit_Transaction", CreditTran.class);
		xstream.aliasField("Credit_Transactions", CreditData.class, "creditTrans");
		xstream.aliasField("Version", CreditData.class, "version");
		xstream.aliasField("VendorCode", CreditTran.class, "vendorCode");
		xstream.aliasField("CustomerCode", CreditTran.class, "customerCode");
		xstream.aliasField("TerminalCode", CreditTran.class, "terminalCode");
		xstream.aliasField("TxnCode ", CreditTran.class, "txnCode");
		xstream.aliasField("TxnDateTime", CreditTran.class, "txnDateTimeString");
		xstream.aliasField("TxnType", CreditTran.class, "txnType");
		xstream.aliasField("TxnDescription", CreditTran.class, "txnDescription");
		xstream.aliasField("TxnAmount", CreditTran.class, "txnAmount");
		xstream.aliasField("StatusCode", CreditTran.class, "statusCode");
		xstream.aliasField("CardNo", CreditTran.class, "cardNo");
		xstream.aliasField("ApprovalCode", CreditTran.class, "approvalCode");
		xstream.omitField(CreditTran.class, "txnDateTime");
    }
    
	public static void toXML(CreditData creditData, OutputStream out) {
		xstream.toXML(creditData, out);
	}
	
	public static void main(String[] args) {
		CreditTran tran=new CreditTran();
		tran.setApprovalCode("123");
		tran.setCardNo("cardNo");
		tran.setCustomerCode(123);
		tran.setStatusCode("approved");
		tran.setTerminalCode("K3KID000001");
		tran.setTxnCode("txnCode");
		tran.setTxnType("CBORD");
		tran.setTxnDescription("MANUAL ENTRY");
		tran.setTxnAmount(new BigDecimal("1.25"));
		tran.setTxnDateTime(new Date());
		tran.setVendorCode("USAT");
		
		CreditTran tran2=new CreditTran();
		tran2.setApprovalCode("123");
		tran2.setCardNo("cardNo");
		tran2.setCustomerCode(123);
		tran2.setStatusCode("approved");
		tran2.setTerminalCode("K3KID000001");
		tran2.setTxnCode("txnCode");
		tran2.setTxnType("CREDIT");
		tran2.setTxnDescription("MANUAL ENTRY");
		tran2.setTxnAmount(new BigDecimal("-1.25"));
		tran2.setTxnDateTime(new Date());
		tran2.setVendorCode("USAT");
		
		
		CreditData data=new CreditData();
		//data.setVersion("1.0");
		data.add(tran);
		data.add(tran2);
		toXML(data, System.out);
	}
}
