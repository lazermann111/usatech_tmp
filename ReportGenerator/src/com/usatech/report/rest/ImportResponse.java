package com.usatech.report.rest;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class ImportResponse {
	
	protected long confirmationNumber;
	protected boolean successInd;
	protected String supportMessage;
	
	
	private ImportResponse() {
		super();
	}

	public long getConfirmationNumber() {
		return confirmationNumber;
	}


	public void setConfirmationNumber(long confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}


	public boolean isSuccessInd() {
		return successInd;
	}


	public void setSuccessInd(boolean successInd) {
		this.successInd = successInd;
	}


	public String getSupportMessage() {
		return supportMessage;
	}

	public void setSupportMessage(String supportMessage) {
		this.supportMessage = supportMessage;
	}

	protected final static XStream xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("_-", "_"))) {
        // ignore unknown XML tags
		protected MapperWrapper wrapMapper(MapperWrapper next) {
            return new MapperWrapper(next) {
                public boolean shouldSerializeMember(@SuppressWarnings("rawtypes") Class definedIn, String fieldName) {
                    try {
                        return definedIn != Object.class || realClass(fieldName) != null;
                    } catch(CannotResolveClassException cnrce) {
                        return false;
                    }
                }
            };
        }
    };		
	
    static {
    	xstream.alias("ImportResponse", ImportResponse.class);
		xstream.aliasField("ConfirmationNumber", ImportResponse.class, "confirmationNumber");
		xstream.aliasField("SuccessInd", ImportResponse.class, "successInd");
		xstream.aliasField("SupportMessage", ImportResponse.class, "supportMessage");
    }
    public String toString(){
    	return "ImportResponse [ConfirmationNumber="+confirmationNumber+",SuccessInd="+successInd+",SupportMessage="+supportMessage+"]";
    }
	public static String toXML(ImportResponse response) {
		return xstream.toXML(response);
	}
	
	public static ImportResponse fromXML(String xml) {
		return (ImportResponse)xstream.fromXML(xml);
	}
	
	public static void main(String[] args) {
		String xml="<ImportResponse xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.datacontract.org/2004/07/CTG.DataImport.DataIntegration\"><ConfirmationNumber>29390310</ConfirmationNumber><SuccessInd>true</SuccessInd><SupportMessage i:nil=\"true\" /></ImportResponse>";
		//String xml="<ImportResponse><ConfirmationNumber>29390310</ConfirmationNumber><SuccessInd>true</SuccessInd><SupportMessage/></ImportResponse>";
		ImportResponse response=ImportResponse.fromXML(xml);
		System.out.println(response);

	}

}
