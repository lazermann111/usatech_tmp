package com.usatech.report.rest;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class EFTData {
	protected String version="1.0";
	protected ArrayList<EFT> efts = new ArrayList<EFT>();
	
	public EFTData() {
		super();
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public ArrayList<EFT> getEfts() {
		return efts;
	}
	public void setEfts(ArrayList<EFT> efts) {
		this.efts = efts;
	}
	public void addEFT(EFT eft){
		efts.add(eft);
	}
	protected final static XStream xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("_-", "_"))) {
        // ignore unknown XML tags
		protected MapperWrapper wrapMapper(MapperWrapper next) {
            return new MapperWrapper(next) {
                public boolean shouldSerializeMember(@SuppressWarnings("rawtypes") Class definedIn, String fieldName) {
                    try {
                        return definedIn != Object.class || realClass(fieldName) != null;
                    } catch(CannotResolveClassException cnrce) {
                        return false;
                    }
                }
            };
        }
    };		
   
	
    static {
    	xstream.registerConverter(new BigDecimalConverter());
		xstream.alias("EFT_Data", EFTData.class);
		xstream.alias("Transaction", EFTTran.class);
		xstream.alias("Terminal", EFTTerminal.class);
		xstream.alias("EFT", EFT.class);
		xstream.aliasField("EFTs", EFTData.class, "efts");
		xstream.aliasField("Version", EFTData.class, "version");
		
		xstream.aliasField("Terminals", EFT.class, "eftTerminals");
		xstream.aliasField("VendorCode", EFT.class, "vendorCode");
		xstream.aliasField("CustomerCode", EFT.class, "customerCode");
		xstream.aliasField("PaymentCode", EFT.class, "paymentCode");
		xstream.aliasField("PaymentDate", EFT.class, "paymentDate");
		xstream.aliasField("SettledAmount", EFT.class, "settledAmount");
		xstream.aliasField("FailedAmount", EFT.class, "failedAmount");
		xstream.aliasField("FeeAmount", EFT.class, "feeAmount");
		xstream.aliasField("ChargebackAmount", EFT.class, "chargebackAmount");
		xstream.aliasField("RefundAmount", EFT.class, "refundAmount");
		xstream.aliasField("AdjustmentAmount", EFT.class, "adjustmentAmount");
		xstream.aliasField("NetAmount", EFT.class, "netAmount");
		
		xstream.aliasField("TerminalCode", EFTTran.class, "terminalCode");
		xstream.aliasField("TxnCode", EFTTran.class, "txnCode");
		xstream.aliasField("TxnAmount", EFTTran.class, "txnAmount");
		
		xstream.aliasField("TerminalCode", EFTTerminal.class, "terminalCode");
		xstream.aliasField("FailedAmount", EFTTerminal.class, "failedAmount");
		xstream.aliasField("FeeAmount", EFTTerminal.class, "feeAmount");
		xstream.aliasField("ChargebackAmount", EFTTerminal.class, "chargebackAmount");
		xstream.aliasField("RefundAmount", EFTTerminal.class, "refundAmount");
		xstream.aliasField("AdjustmentAmount", EFTTerminal.class, "adjustmentAmount");
		xstream.aliasField("Transactions", EFTTerminal.class, "eftTrans");
    }
    
	public static void toXML(EFTData EFTData, OutputStream out) {
		xstream.toXML(EFTData, out);
	}
	public static void main(String[] args) {
		EFTTran tran=new EFTTran();
		tran.setTxnCode("837389292");
		tran.setTxnAmount(new BigDecimal("1.25"));
		
		EFTTran tran2=new EFTTran();
		tran2.setTxnCode("837389293");
		tran2.setTxnAmount(new BigDecimal("0.85"));
		
		EFTTerminal t=new EFTTerminal();
		t.setAdjustmentAmount(new BigDecimal("0.85"));
		t.setChargebackAmount(new BigDecimal("0.85"));
		t.setFailedAmount(new BigDecimal("0.85"));
		t.setFeeAmount(new BigDecimal("0.85"));
		t.setRefundAmount(new BigDecimal("0.85"));
		t.setTerminalCode("K3KID000001");
		t.addEFTTran(tran);
		
		EFTTerminal t2=new EFTTerminal();
		t2.setAdjustmentAmount(new BigDecimal("0.85"));
		t2.setChargebackAmount(new BigDecimal("0.85"));
		t2.setFailedAmount(new BigDecimal("0.85"));
		t2.setFeeAmount(new BigDecimal("0.85"));
		t2.setRefundAmount(new BigDecimal("0.85"));
		t2.setTerminalCode("K3KID000002");
		t2.addEFTTran(tran2);
		
		EFT eft=new EFT();
		eft.addEFTTerminal(t);
		eft.addEFTTerminal(t2);
		eft.setAdjustmentAmount(new BigDecimal("0.85"));
		eft.setChargebackAmount(new BigDecimal("0.85"));
		eft.setFailedAmount(new BigDecimal("0.85"));
		eft.setFeeAmount(new BigDecimal("0.85"));
		eft.setRefundAmount(new BigDecimal("0.85"));
		eft.setCustomerCode(2974);
		eft.setNetAmount(new BigDecimal("0.85"));
		eft.setPaymentCode(1);
		eft.setPaymentDate(new Date());
		eft.setSettledAmount(new BigDecimal("0.85"));
		
		EFTData data=new EFTData();
		data.addEFT(eft);
		data.setVersion("1.0");
		toXML(data, System.out);
	}

}
