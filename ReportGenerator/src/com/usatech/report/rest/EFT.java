package com.usatech.report.rest;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;

public class EFT {
	protected static final java.text.SimpleDateFormat paymentDateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
	public static final MathContext mathContext=new MathContext(2,RoundingMode.HALF_UP);
	protected String vendorCode="USAT";
	protected int customerCode;
	protected int paymentCode;
	protected String paymentDate;
	protected BigDecimal settledAmount=new BigDecimal("0.00");
	protected BigDecimal failedAmount=new BigDecimal("0.00");
	protected BigDecimal feeAmount=new BigDecimal("0.00");
	protected BigDecimal chargebackAmount=new BigDecimal("0.00");
	protected BigDecimal refundAmount=new BigDecimal("0.00");
	protected BigDecimal adjustmentAmount=new BigDecimal("0.00");
	protected BigDecimal netAmount=new BigDecimal("0.00");
	protected ArrayList<EFTTerminal> eftTerminals = new ArrayList<EFTTerminal>();
	public EFT() {
		super();
	}
	
	public void addEFTTerminal(EFTTerminal eftTerminal){
		eftTerminals.add(eftTerminal);
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public int getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(int customerCode) {
		this.customerCode = customerCode;
	}
	public int getPaymentCode() {
		return paymentCode;
	}
	public void setPaymentCode(int paymentCode) {
		this.paymentCode = paymentCode;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		if(paymentDate!=null){
			this.paymentDate = paymentDateFormat.format(paymentDate);
		}
	}
	public BigDecimal getSettledAmount() {
		return settledAmount;
	}
	public void setSettledAmount(BigDecimal settledAmount) {
		this.settledAmount = settledAmount;
	}
	public BigDecimal getFailedAmount() {
		return failedAmount;
	}
	public void setFailedAmount(BigDecimal failedAmount) {
		this.failedAmount = failedAmount;
	}
	public BigDecimal getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}
	public BigDecimal getChargebackAmount() {
		return chargebackAmount;
	}
	public void setChargebackAmount(BigDecimal chargebackAmount) {
		this.chargebackAmount = chargebackAmount;
	}
	public BigDecimal getRefundAmount() {
		return refundAmount;
	}
	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}
	public BigDecimal getAdjustmentAmount() {
		return adjustmentAmount;
	}
	public void setAdjustmentAmount(BigDecimal adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}
	public BigDecimal getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}
	
	public ArrayList<EFTTerminal> getEftTerminals() {
		return eftTerminals;
	}
	public void setEftTerminals(ArrayList<EFTTerminal> eftTerminals) {
		this.eftTerminals = eftTerminals;
	}
	
	
}
