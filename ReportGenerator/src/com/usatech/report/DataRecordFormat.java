/*
 *
 * Created on September 17, 2001, 9:23 AM
 */

package com.usatech.report;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.Format;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Column;
import simple.db.DBHelper;
import simple.db.DBUnwrap;
import simple.db.DataLayerMgr;
import simple.io.IOUtils;
import simple.io.Log;
import simple.results.ListArrayResults;
import simple.results.Results;
import simple.text.CentsFormat;
import simple.text.PadFormat;
import simple.text.StringUtils;

/**
 *
 * @author  bkrug
 * @version
 */

public class DataRecordFormat extends java.text.Format {
	/**
	 *
	 */
	private static final long serialVersionUID = 234141471240471L;
	private static Log log = Log.getLog();
	public static final String TAG_SELECT = "SELECT";
	public static final String TAG_COL = "COL";
	public static final String TAG_SEPARATOR = "SEPARATOR";
	public static final String TAG_LINE_SEPARATOR = "LINESEPARATOR";
	public static final String TAG_NEWLINE = "NEWLINE";
	public static final String TAG_PARAMETER = "PARAMETER";
	protected static final Map<String, Format> formatMap = new HashMap<String, Format>();
	static {
		formatMap.put("DATE", new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss"));
		formatMap.put("CURRENCY",java.text.NumberFormat.getCurrencyInstance());
		formatMap.put("PERCENT",java.text.NumberFormat.getPercentInstance());
	}

	protected Block headBlock = new Block();
	protected Block currentBlock = headBlock;
	protected String separator = null;
	protected String newline = System.getProperty("line.separator");
	protected Connection conn = null;

	protected class NotMatchingException extends Exception {
		public NotMatchingException() {super("Column doesn't match, reset stream to last valid"); }
		public NotMatchingException(String msg) { super(msg); }
	}

	protected class ResultsMap extends AbstractMap {
		protected Results results;
		protected Set entrySet = null;
		public ResultsMap(Results _results) {
			results = _results;
		}
		@Override
		public Set entrySet() {
			if(entrySet == null) entrySet = new AbstractSet() {
				@Override
				public int size() {
					return results.getColumnCount();
				}
				@Override
				public Iterator iterator() {
					return new Iterator() {
						protected int index = 0;
						public Object next() {
							return new Map.Entry() {
								protected int eindex = ++index;
								public Object getKey() {
									return results.getColumnName(eindex);
								}
								public Object getValue() {
									return results.getValue(eindex);
								}
								@Override
								public boolean equals(Object o) {
									Map.Entry e = (Map.Entry) o;
									return 	(getKey()==null ? e.getKey()==null : getKey().equals(e.getKey()))  &&
										(getValue()==null ? e.getValue()==null : getValue().equals(e.getValue()));
								}
								@Override
								public int hashCode() {
									return (getKey()==null   ? 0 : getKey().hashCode()) ^
										(getValue()==null ? 0 : getValue().hashCode());
								}
								public Object setValue(Object value) {
									throw new UnsupportedOperationException("Cannot change value of FormattedResults");
								}
							};
						}
						public boolean hasNext() {
							return index < results.getColumnCount();
						}
						public void remove() {
							throw new UnsupportedOperationException("Cannot remove columns");
						}
					};
				}
			};
			return entrySet;
		}
	}

	protected abstract class Col {
		public boolean write(Results results, Appendable out, boolean separatorPending) throws IOException {
			return write(new ResultsMap(results),out, separatorPending);
		}
		public abstract boolean write(Map results, Appendable out, boolean separatorPending) throws IOException ;
		public abstract void read(Map results, java.io.InputStream in) throws NotMatchingException, IOException ;
	}
	protected class Block extends Col {
		public String name = null;
		public Block parent = null;
		public String sql = null;
		public PreparedStatement ps = null;
		public String[] params = null;
		public List cols = new LinkedList();
		@Override
		public boolean write(Map results, Appendable out, boolean separatorPending) throws IOException {
			if(ps == null && sql == null) {
				return writeCols(results, out, separatorPending);
			} else {
                ResultSet rs = null;
				try {
                    if(ps == null) ps = conn.prepareStatement(sql);
					if(params != null && results != null) {
						for(int i = 0; i < params.length; i++) {
							Object value = results.get(params[i]);
							if(log.isDebugEnabled())
								log.debug("Binding " + value + (value == null ? "" : " (" + value.getClass().getName() + ")") + " to parameter index " + (i+1));
							//Class c = results.getColumnClass(params[i]);
							if(value == null) ps.setNull(i+1, Types.VARCHAR);
							else ps.setObject(i+1,value);
						}
					}
                    rs = ps.executeQuery();

                    ResultSetMetaData rsmd = rs.getMetaData();
                    int count = rsmd.getColumnCount();
                    Column[] columns = new Column[count];
                    for(int i = 1; i <= count; i++) {
                        String name = rsmd.getColumnName(i);
                        Column c = new Column();
                        c.setIndex(i);
                        c.setName(name);
                        c.setPropertyName(name);
                        c.setSqlType(rsmd.getColumnType(i));
                        try {
                            c.setColumnClass(Class.forName(rsmd.getColumnClassName(i)));
                        } catch(ClassCastException e) {
                            // leave it as Object then
                        } catch(ClassNotFoundException e) {
                            // leave it as Object then
                        } catch(IllegalArgumentException e) {
                            // leave it as Object then
                        } catch(NoClassDefFoundError e) {
                            // leave it as Object then
                        }
                        columns[i-1] = c;
                    }
                    DBHelper dbHelper = DataLayerMgr.getDBHelper(DBUnwrap.getRealConnection(conn));
                    List<Object[]> data = new ArrayList<Object[]>();
                    try {
                        while(rs.next()) {
                            Object[] values = dbHelper.getValues(rs, columns, true);
                            data.add(values);
                        }
                    } catch(SQLException sqle) {
                        log.info("Exception while reading row #" + rs.getRow());
                        throw sqle;
                    } catch(ConvertException e) {
                    	log.info("Exception while reading row #" + rs.getRow());
                        SQLException sqle = new SQLException();
                        sqle.initCause(e);
                        throw sqle;
					} finally {
                        try { rs.close(); } catch(SQLException e) {}
                    }
                    Results innerResults = new ListArrayResults(columns, data);
                    while(innerResults.next()) {
                    	separatorPending = writeCols(new ResultsMap(innerResults),out, separatorPending);
					}
                    return separatorPending;
				} catch(SQLException sqle) {
					log.warn("While executing '" + sql + "' in block, " + name,sqle);
					throw new IOException("While writing output: " + sqle);
				} finally {
                    if(rs != null) try { rs.close(); } catch(SQLException e) {}
					if(ps != null) {
						try { ps.close(); } catch(SQLException e) {}
						ps = null; //cannot reuse ps
					}
				}
			}
		}

		protected boolean writeCols(Map results, Appendable out, boolean separatorPending) throws IOException {
			for(Iterator iter = cols.iterator(); iter.hasNext();) {
				Col col = (Col)iter.next();
				separatorPending = col.write(results, out, separatorPending);
			}
			return separatorPending;
		}

		@Override
		public void read(Map results, java.io.InputStream in) throws NotMatchingException, IOException {
			List rows = new ArrayList();
			results.put(name,rows);
			try {
				while(true) { //endless loop until exception
					in.mark(in.available());
					Map innerResults = new HashMap();
					for(Iterator iter = cols.iterator(); iter.hasNext();) {
						Col col = (Col)iter.next();
						try {
							col.read(innerResults,in);
						} catch(NotMatchingException nme0) {
							if(!(col instanceof Block)) throw nme0;
							else if(!iter.hasNext()) {
								rows.add(innerResults); //must add this row in, it's valid
								throw nme0;
							}
						}
						if(separator != null && iter.hasNext()) {
							byte[] b = new byte[separator.length()];
							int len = in.read(b);
							if(len != separator.length() || !separator.equals(new String(b))) throw new NotMatchingException("Separator not found in block '" + name + "'");
						}

					}
					rows.add(innerResults);
				}
			} catch(NotMatchingException nme) {
				log.debug("InputStream did not match the block, '" + name + "'; resetting stream to last mark.", nme);
				in.reset();
				throw nme;
			}
		}

	}
	protected class NewlineCol extends Col {
		public NewlineCol() {
		}
		@Override
		public boolean write(Map results, Appendable out, boolean separatorPending) throws IOException {
			out.append(newline);
			return false;
		}
		@Override
		public void read(Map results, java.io.InputStream in) throws NotMatchingException, IOException {
			byte[] b = new byte[newline.length()];
			int len = in.read(b);
			if(len != newline.length() || !newline.equals(new String(b))) throw new NotMatchingException("Newline not found.");
		}
	}
	protected class LiteralCol extends Col {
		public String literal = "";
		public LiteralCol(String _literal) {
			literal = _literal;
		}
		@Override
		public boolean write(Map results, Appendable out, boolean separatorPending) throws IOException {
			if(separatorPending && separator != null)
				out.append(separator);
			out.append(literal);
			return true;
		}

		@Override
		public void read(Map results, java.io.InputStream in) throws NotMatchingException, IOException {
			byte[] b = new byte[literal.length()];
			int len = in.read(b);
			if(len != literal.length() || !literal.equals(new String(b))) throw new NotMatchingException("Literal '" + literal + "' not found");
		}
	}
	/*protected class ParameterCol extends Col {
		public String name;
		public java.text.Format format = null;
		public int length = -1;
		public ParameterCol(String _name) {
			name = _name;
		}
		public void write(Map results, StringBuffer out) throws org.xml.sax.SAXException {
			Object value = results.get(name);
			String s = null;
			if(format == null) {
				if(value != null)  s = value.toString();
			} else s = format.format(value);
			log.debug("Writing '" + value + "' as '" + s + "'");
			if(s!= null) out.append(s);
		}
		public void read(Map results, java.io.InputStream in) throws NotMatchingException, java.io.IOException {
			//how to handle variable width?i
			byte[] b = new byte[length()];
			int len = in.read(b);
			if(len != length()) throw new NotMatchingException("Length does not match for '" + name + "'");
			try {
				Object value = (format == null ? new String(b) : format.parseObject(new String(b)));
				log.debug("Reading '" +name + "' as '" + value + "'");
				results.put(name,value);
			} catch(java.text.ParseException pe) {
				throw new NotMatchingException("Could not parse '" + new String(b) + "' into format for '" + name + "'");
			}
		}
		protected int length() {
			if(length == -1) {
				if(format == null) length = 0;
				else {
					Object o;
					if(format instanceof java.text.DateFormat) o = new java.util.Date();
					else if(format instanceof java.text.NumberFormat) o = new Integer(0);
					else o = "";
					String s = format.format(o);
					length = (s == null ? 0 : s.length());
				}
			}
			return length;
		}

	}*/
	protected class ValueCol extends Col {
		public ValueCol(String _columnName) {
			columnName = _columnName;
		}
		public String columnName = null;
		public java.text.Format format = null;
		public int length = -1;
		public boolean optional;
		@Override
		public boolean write(Map results, Appendable out, boolean separatorPending) throws IOException {
			Object value = results.get(columnName);
			if(separatorPending && (value != null || !optional)) {
				out.append(separator);
			}
			if(value == null) {
				return !optional || separatorPending;
			}
			String s;
			if(format == null) {
				s = value.toString();
			} else {
				s = format.format(value);
				if(s == null) return !optional || separatorPending;
			}
			if(log.isDebugEnabled())
				log.debug("Writing '" + columnName + "' as '" + s + "'");
			out.append(s);
			return true;
		}

		@Override
		public void read(Map results, java.io.InputStream in) throws NotMatchingException, IOException {
			//how to handle variable width?i
			byte[] b = new byte[length()];
			int len = in.read(b);
			if(len != length()) throw new NotMatchingException("Length does not match for '" + columnName + "'");
			try {
				Object value = (format == null ? new String(b) : format.parseObject(new String(b)));
				if(log.isDebugEnabled())
					log.debug("Reading '" + columnName + "' as '" + value + "'");
				results.put(columnName,value);
			} catch(java.text.ParseException pe) {
				throw new NotMatchingException("Could not parse '" + new String(b) + "' into format for '" + columnName + "'");
			}
		}

		protected int length() {
			if(length == -1) {
				if(format == null) length = 0;
				else {
					Object o;
					if(format instanceof java.text.DateFormat) o = new java.util.Date();
					else if(format instanceof java.text.NumberFormat) o = new Integer(0);
					else o = "";
					String s = format.format(o);
					length = (s == null ? 0 : s.length());
				}
			}
			return length;
		}
	}
	/*
	protected class CountCol extends Col {
		public CountCol(String _columnName) {
			columnName = _columnName;
		}
		public CountCol(String _columnName, String _groupColumnName) {
			columnName = _columnName;
			groupColumnName = _groupColumnName;
		}
		public int length = -1;
		public String groupColumnName = null;
		public String columnName = null;
		public java.text.Format format = null;
		public void write(FormattedResults results, StringBuffer out) throws org.xml.sax.SAXException {
			Object value = results.getAggregate(columnName,groupColumnName, results.AGGREGATE_COUNT);
			if(format == null) if(value != null) out.append(value.toString());
			else out.append(format.format(value));
		}
		public void read(Map results, java.io.InputStream in) throws NotMatchingException, java.io.IOException {
			//how to handle variable width?i
			byte[] b = new byte[length()];
			int len = in.read(b);
			if(len != length()) throw new NotMatchingException("Length does not match for '" + columnName + "'");
			try {
				results.put("*" + columnName,(format == null ? new String(b) : format.parseObject(new String(b))));
			} catch(java.text.ParseException pe) {
				throw new NotMatchingException("Could not parse '" + new String(b) + "' into format for '" + columnName + "'");
			}
		}

		protected int length() {
			if(length == -1) {
				if(format == null) length = 0;
				else {
					Object o;
					if(format instanceof java.text.DateFormat) o = new Date();
					else if(format instanceof java.text.NumberFormat) o = new Integer(0);
					else o = "";
					String s = format.format(o);
					length = (s == null ? 0 : s.length());
				}
			}
			return length;
		}

	}
	//*/

	@Override
	public java.lang.StringBuffer format(java.lang.Object obj, java.lang.StringBuffer stringBuffer, java.text.FieldPosition fieldPosition) {
		try {
			headBlock.write((Map)obj,stringBuffer, false);
		} catch(IOException e) {
			log.warn("Could not format object: " + e);
			throw new RuntimeException(e.getMessage(), e);
		}
		return stringBuffer;
	}

	public void format(Map params, Appendable appendTo) throws IOException {
		headBlock.write(params, appendTo, false);
	}

	@Override
	public java.lang.Object parseObject(java.lang.String str, java.text.ParsePosition parsePosition) {
		java.io.InputStream in = new java.io.ByteArrayInputStream(str.getBytes());
		Map results = new HashMap();
		try {
			if(parsePosition.getIndex() > 0) in.skip(parsePosition.getIndex());
			try {
				headBlock.read(results,in);
			} catch(NotMatchingException nme) {
				int avail = in.available();
				if(avail == 0) parsePosition.setIndex(str.length());
				else {
					parsePosition.setErrorIndex(str.length()-avail);
					log.warn("Could not match string at position " + parsePosition.getErrorIndex() + " of " + str.length());
				}
			} finally {
				in.close();
			}
		} catch(java.io.IOException ioe) {
			log.warn("Could not parse string due to IO Exception: " + ioe);
		}

		return ((List)results.get(null)).get(0);
	}

	protected class ConfigXMLHandler extends org.xml.sax.helpers.DefaultHandler {
		protected Connection connection;

		public ConfigXMLHandler(Connection connection) {
			this.connection = connection;
		}

		@Override
		public void characters(char[] ch, int start,  int length) throws org.xml.sax.SAXException {
			for(int i = start; i < start + length; i++) {
				if(!Character.isWhitespace(ch[i])) throw new org.xml.sax.SAXException("Invalid character, '" + ch[i] + "' found in XML Config file");
			}
		}

		@Override
		public void endDocument() throws org.xml.sax.SAXException {
		}

		@Override
		public void endElement(java.lang.String uri, java.lang.String localName, java.lang.String qName) throws org.xml.sax.SAXException {
			if(log.isDebugEnabled())
				log.debug("END ELEMENT >> URI = " + uri + "; LocalName = " + localName + "; qName = " + qName + ";");
			String tag = qName;
			if(tag.equalsIgnoreCase(TAG_COL)) {
				//do nothing
			} else if(tag.equalsIgnoreCase(TAG_SELECT)) {
				currentBlock = currentBlock.parent;
			}
		}

		@Override
		public void startDocument() throws org.xml.sax.SAXException {
			separator = null;
			currentBlock = headBlock;
		}

		@Override
		public void startElement(java.lang.String uri,  java.lang.String localName, java.lang.String qName, org.xml.sax.Attributes attributes) throws org.xml.sax.SAXException {
			log.debug("START ELEMENT >> URI = " + uri + "; LocalName = " + localName + "; qName = " + qName + ";");
			String tag = qName;
			if(tag.equalsIgnoreCase(TAG_COL)) {
				Col col = null;
				int index = attributes.getIndex("literal");
				if(index >= 0) {
					col = new LiteralCol(attributes.getValue(index));
				/*} else if((index=attributes.getIndex("groupcount")) >= 0) {
					String columnName = attributes.getValue(index);
					index = attributes.getIndex("group");
					if(index >= 0) 	col = new CountCol(columnName, attributes.getValue(index));
					else col =  new CountCol(columnName);
					index = attributes.getIndex("format");
					if(index >= 0) ((CountCol)col).format = getFormat(attributes.getValue(index));
					col.useSeparator = !first;
					first = false;
				*/} else if((index=attributes.getIndex("column")) >= 0) {
					String columnName = attributes.getValue(index);
					ValueCol vc = new ValueCol(columnName);
					col = vc;
					index = attributes.getIndex("format");
					if(log.isDebugEnabled())
						log.debug("Creating value column" + (index >=0 ? " with format = '" + attributes.getValue(index) + "'" : ""));
					if(index >= 0) vc.format = getFormat(attributes.getValue(index));
					try {
						vc.optional = ConvertUtils.getBoolean(attributes.getValue("optional"), false);
					} catch(ConvertException e) {
						log.warn("Could not convert optional to boolean", e);
					}
				}
				if(col != null) currentBlock.cols.add(col);
			} else if(tag.equalsIgnoreCase(TAG_NEWLINE)) {
				currentBlock.cols.add(new NewlineCol());
			} else if(tag.equalsIgnoreCase(TAG_SELECT)) {
				Block block = new Block();
				block.parent = currentBlock;
				if(currentBlock != null) currentBlock.cols.add(block);
				currentBlock = block;
				int index = attributes.getIndex("sql");
				if(index >= 0) {
					String sql = attributes.getValue(index);
					block.sql = sql;
					try {
						block.ps = connection.prepareStatement(sql);
					} catch(java.sql.SQLException e) {
						log.warn("While running query: '" + sql + "'",e);
						throw new org.xml.sax.SAXException("Error while running query: '" + sql + "'");
					}
				}
				if((index=attributes.getIndex("name")) >= 0) block.name = attributes.getValue(index);
				else block.name = getUniqueName(block.parent);
				if((index=attributes.getIndex("parameters")) >= 0) block.params = StringUtils.split(attributes.getValue(index),',');
			} else if(tag.equalsIgnoreCase(TAG_SEPARATOR)) {
				int index = attributes.getIndex("value");
				if(index >= 0) separator = attributes.getValue(index);
			} else if(tag.equalsIgnoreCase(TAG_LINE_SEPARATOR)) {
				int index = attributes.getIndex("value");
				if(index >= 0) newline = attributes.getValue(index);
			}

		}

		@Override
		public void error(SAXParseException exception) throws SAXException {
			log.warn("Error:",exception);
		}

		@Override
		public void fatalError(SAXParseException exception) throws SAXException {
			log.warn("Fatal Error:",exception);
		}

		@Override
		public void warning(SAXParseException exception) throws SAXException {
			log.warn("Warning:",exception);
		}
	};// end of handler inner class

	public DataRecordFormat(String file, Connection connection) throws java.io.IOException, javax.xml.parsers.ParserConfigurationException, org.xml.sax.SAXException {
		this(new java.io.FileInputStream(file),connection);
	}
	public DataRecordFormat(java.io.File file, Connection connection) throws java.io.IOException, javax.xml.parsers.ParserConfigurationException, org.xml.sax.SAXException {
		this(new java.io.FileInputStream(file),connection);
	}
	public DataRecordFormat(java.io.InputStream configStream, Connection connection) throws java.io.IOException, javax.xml.parsers.ParserConfigurationException, org.xml.sax.SAXException {
		conn = connection;
		//System.out.println("Getting SAXParserFactory");
		javax.xml.parsers.SAXParserFactory spf = javax.xml.parsers.SAXParserFactory.newInstance();
		//System.out.println("Getting SAXParser");
		javax.xml.parsers.SAXParser parser = spf.newSAXParser();
		//System.out.println("Created DataRecordFormat. Will use '" + parser + "' as parser");
		parser.parse(configStream, new ConfigXMLHandler(connection));
		//System.out.println("Done parsing XML. Ready to format");
	}

	protected  java.text.Format getFormat(String pattern) {
		int p = pattern.indexOf(',');
		java.text.Format format = null;
		if(p < 0) {
			format = formatMap.get(pattern.toUpperCase());
		} else {
			String init = pattern.substring(p+1);
			String type = pattern.substring(0,p).trim();
			if(type.equalsIgnoreCase("DATE")) {
				format = new java.text.SimpleDateFormat(init);
			} else if(type.equalsIgnoreCase("NUMBER")) {
				format = new java.text.DecimalFormat(init);
			} else if(type.equalsIgnoreCase("STRING")) {
				format = new StringFormat(init);
			} else if(type.equalsIgnoreCase("PAD")) {
				format = new PadFormat(init);
			} else if(type.equalsIgnoreCase("CENTS")) {
				format = new CentsFormat(init);
			}
		}
		if(format == null) log.warn("Format '" + pattern + "' not found.");
		return format;
	}

	public static void main(java.lang.String[] args) {
		//runParse();
		runFormat();
	}

	protected static void runParse() {
		Object results = null;
		try {
			Connection da = DataLayerMgr.getConnection("");
			java.io.File f = new java.io.File("C:\\e-port\\dev\\code\\nacha_format.xml");
			DataRecordFormat drf = new DataRecordFormat(f,da);
			f = new java.io.File("C:\\e-port\\dev\\code\\nacha_file.txt");
			String s = IOUtils.readFully(new java.io.FileInputStream(f));
			results = drf.parseObject(s);
		} catch(Exception e) {
			log.warn("While parsing xml",e);
		}
		log.debug("===============================================");
		log.debug("OUTPUT: \r\n" + results);
		Log.finish();
	}

	protected static void runFormat() {
		String s = null;
		try {
            Connection da = DataLayerMgr.getConnection("");
            java.io.File f = new java.io.File("C:\\e-port\\dev\\code\\nacha_format_test.xml");
			DataRecordFormat drf = new DataRecordFormat(f,da);
			s = drf.format(null);
		} catch(Exception e) {
			log.warn("While parsing xml",e);
		}
		log.debug("===============================================");
		log.debug("OUTPUT: \r\n" + s);
        Log.finish();
	}

	protected void handleException(Exception e) {
		log.warn("",e);
	}

	protected String getUniqueName(Block parent) {
		String s = "ROWS";
		int i = 0;
		for(boolean unique = false; !unique; i++) {
			s = "ROWS_" + i;
			unique = true;
			for(Iterator iter = parent.cols.iterator(); iter.hasNext() && unique; ) {
				Object o = iter.next();
				if(o instanceof ValueCol) unique = !s.equalsIgnoreCase(((ValueCol)o).columnName);
				else if(o instanceof Block) unique = !s.equalsIgnoreCase(((Block)o).name);
			}
		}
		return s;
	}

}
