package com.usatech.report;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.DeleteResourceTask;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.AlertSourceCode;
import com.usatech.layers.common.constants.AlertingAttrEnum;
import com.usatech.layers.common.constants.BooleanType;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.EventState;
import com.usatech.layers.common.constants.EventType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.LoadDataAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;
import com.usatech.layers.common.dex.InboundFileImportTask;
import com.usatech.layers.common.dex.InboundFileImportTask.DexParseResult;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.EnhancedBufferedReader;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.text.StringUtils;

public class ProcessVdiDEXFileTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String resourceKey;
		try {
			resourceKey = step.getAttribute(CommonAttrEnum.ATTR_RESOURCE, String.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Could not get resourceKey", e, WorkRetryType.NO_RETRY);
		}
		Resource resource;
		ResourceFolder rf = getResourceFolder();
		if(rf == null)
			throw new ServiceException("ResourceFolder property is not set on " + this);
		try {
			resource = rf.getResource(resourceKey, ResourceMode.READ);
		} catch(IOException e) {
			if(!rf.isAvailable(ResourceMode.READ)) {
				throw new RetrySpecifiedServiceException("Could not get resource at '" + resourceKey + "' for processing file transfer", e, WorkRetryType.NONBLOCKING_RETRY);
			}
			throw new ServiceException("Could not get resource at '" + resourceKey + "' for processing file transfer", e);
		}
		DexParseResult dexParseResult;
		long fileTransferId;
		long fileTransferTime;
		String deviceSerialCd;
		try {
			FileType fileType = step.getAttribute(CommonAttrEnum.ATTR_FILE_TYPE, FileType.class, true);	
			String globalSessionCd = step.getAttribute(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD, String.class, true);
			String deviceName = step.getAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, String.class, true);
			String fileName = step.getAttribute(CommonAttrEnum.ATTR_FILE_NAME, String.class, true);
			fileTransferTime = step.getAttribute(LoadDataAttrEnum.ATTR_FILE_TRANSFER_TS, Long.class, true);
			long deviceId = step.getAttribute(LoadDataAttrEnum.ATTR_DEVICE_ID, Long.class, true);
			Map<String,Object> params = new HashMap<>();
			try(Connection conn = DataLayerMgr.getConnection("REPORT")) {
				switch(fileType) {
					case DEX_FILE_FOR_FILL_TO_FILL:
						Long deviceEventId = step.getAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, Long.class, false);
						long eventTime =  step.getAttribute(LoadDataAttrEnum.ATTR_EVENT_TIME, Long.class, true);
						String globalEventCd = step.getAttribute(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD, String.class, false);
						String[] parts = StringUtils.split(globalEventCd, ':', 3);
						String prefix;
						String deviceEventCd;
						if(parts != null && parts.length >= 1) {
							prefix = parts[0];
							if(parts.length >= 3)
								deviceEventCd = parts[2];
							else
								deviceEventCd = String.valueOf(deviceEventId);
						} else {
							prefix = EventCodePrefix.UNKNOWN.getValue();
							deviceEventCd = String.valueOf(deviceEventId);
						}
						long eventId;
						try {
							eventId = AppLayerUtils.getOrCreateEvent(log, conn, params, prefix, globalSessionCd, deviceName, 0 /* Base Host*/, EventType.FILL.getValue(), EventType.FILL.getHostNumber(), EventState.COMPLETE_FINAL, deviceEventCd, BooleanType.TRUE, BooleanType.TRUE, eventTime, eventTime); //XXX: do we need to adjust eventTime to Local?
						} catch (Exception e) {
							throw new ServiceException("Error creating fill event for a fill-to-fill DEX file, deviceName: " + deviceName + ", fileName: " + fileName, e);
						}
						params.put("countersDisplayed", "true"); //We assume so
						params.put("createDEX", "false");
						params.put("eventTime", eventTime);
						params.put("eventId", eventId);
						params.put("deviceId", deviceId);
						DataLayerMgr.executeCall(conn, "LOAD_VDI_FILL", params);
						conn.commit();
						params.put("deviceEventCd", deviceEventId);
						params.put("globalEventCd", globalEventCd);
					case DEX_FILE:
						params.put("globalSessionCd", globalSessionCd);
						params.put("deviceName", deviceName);
						params.put("deviceId", deviceId);
						params.put("fileName", fileName);
						params.put("fileType", fileType);
						params.put("overwrite", "Y");
						params.put("fileTransferTs", fileTransferTime);
						DataLayerMgr.executeCall(conn, "RECORD_INBOUND_FILE_TRANSFER", params);
						try (InputStream in = resource.getInputStream()) {
							params.put("fileContent", in);
							DataLayerMgr.executeCall(conn, "UPDATE_FILE_TRANSFER_CONTENT", params);
							conn.commit();
						}
						break;
					default:
						throw new ServiceException("Processing of this fileType is not supported, fileName: " + resource.getName() + ", fileType: " + fileType + ", resourceKey: " + resourceKey);
				}
			} 
			fileTransferId = ConvertUtils.getLong(params.get("fileTransferId"));
			log.info("Uploaded file '" + fileName + "' of " + resource.getLength() + " bytes from device " + deviceName + " to OPER database, fileTransferId: " + fileTransferId);
			deviceSerialCd = step.getAttribute(LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD, String.class, true);
			Calendar fileTransferCal = Calendar.getInstance();
			fileTransferCal.setTimeInMillis(fileTransferTime);
			String deviceCharset = step.getAttribute(LoadDataAttrEnum.ATTR_DEVICE_CHARSET, String.class, true);
			try (EnhancedBufferedReader reader = new EnhancedBufferedReader(new InputStreamReader(resource.getInputStream(), deviceCharset))) {
				dexParseResult = InboundFileImportTask.parseDexFile(fileTransferId, fileName, reader, deviceSerialCd, fileTransferCal, false);
			}
			DeleteResourceTask.requestResourceDeletion(resource, taskInfo.getPublisher());
		} catch(AttributeConversionException | ConvertException e) {
			throw new RetrySpecifiedServiceException("Could not get resourceKey", e, WorkRetryType.NO_RETRY);
		} catch(IOException | SQLException | DataLayerException e) {
			throw new ServiceException(e);
		} finally {
			resource.release();
		}
		if(dexParseResult != null) {
			if(dexParseResult.getTerminalId() != null) {
				// Publish to RequestAlertBatchTask
				if(dexParseResult.getAlertCount() > 0) {
					MessageChain mc = new MessageChainV11();
					MessageChainStep requestStep = mc.addStep("usat.report.request." + 6 /*ReportScheduleType.EVENT.getValue()*/);
					requestStep.setAttribute(AlertingAttrEnum.AL_SERIAL_NUM, deviceSerialCd);
					requestStep.setAttribute(AlertingAttrEnum.AL_TERMINAL, dexParseResult.getTerminalId());
					requestStep.setAttribute(AlertingAttrEnum.AL_EVENT_ID, fileTransferId);
					requestStep.setAttribute(AlertingAttrEnum.AL_EVENT_SOURCE_TYPE, AlertSourceCode.DEX);
					requestStep.setAttribute(AlertingAttrEnum.AL_NOTIFICATION_TS, fileTransferTime);
					MessageChainService.publish(mc, taskInfo.getPublisher());
				}
			}
		}
		return 0;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
}
