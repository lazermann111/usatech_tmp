package com.usatech.report;

import java.rmi.RemoteException;

import javax.management.MXBean;

@MXBean(true)
public interface DataExchangeRetriesMXBean {
	public int kickoffRetries(int dataExchangeType, boolean all) throws RemoteException;
}
