package com.usatech.report;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

public class UpdateReportSentTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> params = taskInfo.getStep().getAttributes();
		Connection conn=null;
		try{
			conn = DataLayerMgr.getConnection("report", false);
			DataLayerMgr.executeUpdate(conn,"REPORT_SENT_UPD", params);
			boolean retry=ConvertUtils.getBooleanSafely(params.get("retry"), false);
			if(retry){
				DataLayerMgr.executeUpdate(conn,"CCS_TRANSPORT_RETRY_SUCCESS", params);
			}
			conn.commit();
			return 0;
		}catch(Exception e){
			try{
				if(conn!=null) conn.rollback();
			}catch(SQLException sqle){log.debug("UpdateReportSentTask Failed to rollback.");}
			throw new ServiceException(e);
		} finally {
			try{
				if(conn!=null) conn.close();
			}catch(SQLException sqle){log.debug("UpdateReportSentTask Failed to close database connection.");}
		}
	}

}
