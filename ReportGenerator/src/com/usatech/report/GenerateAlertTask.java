/**
 * 
 */
package com.usatech.report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.falcon.engine.standard.StandardOutputType;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.results.Results;
import simple.text.MessageFormat;

/**
 * Receives alert messages on usat.report.generate.9
 * 
 * @author phorsfield
 * 
 */
public class GenerateAlertTask extends AbstractGenerateReportTask {

	private static final Log log = Log.getLog();
	private static final String USE_CLASSLOADER_PREFIX = "resource:";
	private static final String TEMPLATE_PREFIX = USE_CLASSLOADER_PREFIX + "alert/email_";
	private static final String alertFileHeaderResourceName = TEMPLATE_PREFIX + "header_template.txt";
	private static final String alertFileFooterResourceName = TEMPLATE_PREFIX + "footer_template.txt";
	private static final String alertFileAlertTemplateResourceName = TEMPLATE_PREFIX + "alert_template.txt";

	private MessageFormat alertFileHeader;
	private MessageFormat alertFileAlertTemplate;
	private MessageFormat alertFileFooter;

	// -- configuration fields
	protected ReportRequestFactory reportRequestFactory;

	public GenerateAlertTask() throws IOException {
		super(Generator.ALERT);

		alertFileHeader = new MessageFormat(loadResource(alertFileHeaderResourceName));
		alertFileFooter = new MessageFormat(loadResource(alertFileFooterResourceName));
		alertFileAlertTemplate = new MessageFormat(loadResource(alertFileAlertTemplateResourceName));
	}

	/**
	 * Generate email format. Format dates and numbers according to locale and dateFormat params.
	 * Reads resources alert/email_header_*.txt to build the alert email.
	 * 
	 * <h2>Some available parameters for all parts of the email:</h2><br />
	 * <dl>
	 * <dt>${terminalId}</dt>
	 * <dd>#####</dd>
	 * <dt>${serialNum}</dt>
	 * <dd>Device serial number if available.</dd>
	 * <dt>${specificDevice}</dt>
	 * <dd>User friendly device identifier</dd>
	 * <dt>${request.title}</dt>
	 * <dd>Title of the notification, includes unreplaced fillins</dt>
	 * <dt>${baseUrl}</dt>
	 * <dd>For hyperlinking</dd>
	 * <dt>${runReportAction}</dt>
	 * <dd>For hyperlinking</dd>
	 * <dt>${locale}</dt>
	 * <dd>ex. en-US</dd>
	 * <dt>${dateFormat}</dt>
	 * <dd>ex. HH:MM:SS</dd>
	 * <dt>${notificationTs}</dt>
	 * <dd>Time at which the alert was received</dd>
	 * </dl>
	 * 
	 * <h2>Available parameters for each individual alert:</h2><br />
	 * <dl>
	 * <dt>${alert.deviceName}</dt>
	 * <dd>Internal device name</dd>
	 * <dt>${alert.component}</dt>
	 * <dd>Host port number, for example</dd>
	 * <dt>${alert.description}</dt>
	 * <dd>Friendly device identifier</dd>
	 * <dt>${alert.eventType}</dt>
	 * <dd>Reporting event type identifier</dt>
	 * <dt>${alert.sourceId}</dt>
	 * <dd>Source subsystem identifier (i.e. Device Event id)</dd>
	 * <dt>${alert.source}</dt>
	 * <dd>Dex Alerts or Device Alerts</dd>
	 * <dt>${alert.timestampMs}</dt>
	 * <dd>Milliseconds since epoch in UTC</dd>
	 * <dt>${alert.timestamp}</dt>
	 * <dd>Formatted timestamp.</dd>
	 * </dl>
	 */
	@Override
	protected void generateReport(String title, int userId, Map<String, Object> params, OutputStream out, StandbyAllowance standbyAllowance) throws Exception {
		/*
		long eventId = ConvertUtils.getLong(params.get("eventId"));
		Locale locale = ConvertUtils.convert(Locale.class, params.get("scene.locale"));
		Calendar localTime = GregorianCalendar.getInstance(locale);
		java.text.DateFormat df = new java.text.SimpleDateFormat(params.get("dateFormat").toString(), locale);
		 */
		ReportScheduleType scheduleType = ConvertUtils.convertRequired(ReportScheduleType.class, params.get("batchType"));
		// int[] alertTypes = ConvertUtils.convert(int[].class, params.get("alertTypes"));
		HashMap<String, Object> alertParams = new HashMap<String, Object>(params);
		alertParams.put("batchType", scheduleType.getValue());
		// alertParams.put("alertTypes", alertTypes);
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
		try (Connection conn = getReportConnection(standbyAllowance)) {
			DataLayerMgr.selectInto(conn, "GET_ALERT_HEADER_INFO", alertParams, alertParams);
			writer.write(alertFileHeader.format(alertParams));
			writer.newLine();
			try (Results results = DataLayerMgr.executeQuery(conn, "GET_ALERT_DETAIL_INFO", alertParams)) {
				while(results.next()) {
					results.fillBean(alertParams);
					writer.write(alertFileAlertTemplate.format(alertParams));
					writer.newLine();
				}
			}
		}
		writer.write(alertFileFooter.format(alertParams));
		writer.flush();
	}

	@Override
	protected String getContentType() {
		return generator.getContentType();
	}

	/**
	 * Loads email template text files from the classpath or filesystem
	 * 
	 * @param path
	 *            file path - prefix with 'resource:' to use classpath
	 * @throws IOException
	 */
	private static String loadResource(String path) throws IOException {
		if(path.startsWith(USE_CLASSLOADER_PREFIX)) {
			return IOUtils.readFully(ConfigSource.createConfigSource(path).getInputStream());
		} else {
			FileInputStream fis = new FileInputStream(new File(path));
			String tmp = IOUtils.readFully(fis);
			fis.close();
			return tmp;
		}
	}

	@Override
	/**
	 * Overridden purely to provide a good subject line (data source name, filename) for emails. 
	 */
	protected Resource writeResource(String title, int userId, Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception {
		String contentType = getContentType();
		String fileName = StandardOutputType.fixFileName(StandardOutputType.appendFileExtention(new StringBuilder(title), contentType)).toString();

		if(ConvertUtils.getIntSafely(params.get("transportTypeId"), -1) == 1) {
			fileName = fileName.replace(".txt", "");
		}

		Resource resource = getResourceInstance(fileName);
		resource.setContentType(contentType);
		try {
			OutputStream out = resource.getOutputStream();
			generateReport(title, userId, params, out, standbyAllowance);
			log.debug("Successfully generated report with params =  " + params);
			out.close();
		} catch(Exception e) {
			resource.release();
			throw e;
		} catch(Error e) {
			resource.release();
			throw e;
		}
		return resource;
	}
}
