/*
 * Messages.java
 *
 * Created on November 28, 2001, 2:07 PM
 */

package com.usatech.report;

/**
 *
 * @author  bkrug
 * @version
 */
import java.sql.*;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.text.MessageFormat;

public class Messages {
	private static final Log log = Log.getLog();
	
	//protected Connection da;
	protected java.util.Map cache;
	protected static final Object EMPTY = new Object(); //if retrieval was already tried and failed for a code
	protected static String defaultSource = "REPORT.MESSAGE"; //table name where the messages are
	protected String source; //table name where the messages are
	protected static java.util.Comparator comparator = new java.util.Comparator() {
		public int compare(Object o1, Object o2) {
			if(o1 == null) return -1;
			if(o2 == null) return 1;
			Connection da1 = (Connection) o1;
			Connection da2 = (Connection) o2;
            try {
                String url1 = da1.getMetaData().getURL();
                String url2 = da2.getMetaData().getURL();
                if(url1 == null) url1 = "";
                if(url2 == null) url2 = "";
                
    			int i = url1.compareTo(url2);
    			if(i == 0) i = da1.getMetaData().getUserName().compareTo(da2.getMetaData().getUserName());
    			return i;
            } catch(SQLException sqle) {
                throw new RuntimeException(sqle);
            }
		}
	};
	//protected static java.util.Map instances = new java.util.TreeMap(comparator);
	protected static Messages instance = new Messages();
    
	/** Creates new Messages */
	protected Messages() {
		cache = new java.util.HashMap();
		source = defaultSource;
	}
	
	public static Messages getInstance() {
		return instance;
	}
	
	public String getMessage(String messageCode) {
		return getMessage(messageCode, null);
	}
	
	public void clearCache() {
		cache.clear();
	}
	
	public String getMessage(String messageCode, Object[] params) {
		String mc = messageCode.trim().toUpperCase();
		Object msg = cache.get(mc);
		if(msg == EMPTY) return messageCode;
		else if(msg == null) msg = retrieveMessage(mc);
		
		if(msg == EMPTY)
			return messageCode;
		else if(msg instanceof MessageFormat) {
			return ((MessageFormat)msg).format(params);
		} else {
			return msg.toString();
		}
	}
	
	protected Object retrieveMessage(String messageCode) {
		String sql = "SELECT MESSAGE_TEXT FROM " + source + " WHERE UPPER(TRIM(MESSAGE_CODE)) = ?";
		PreparedStatement ps = null;
        Connection da = null;
		try {
            da = DataLayerMgr.getConnection("report");
			ps = da.prepareStatement(sql);
			ps.setString(1,messageCode);
			ResultSet rs = ps.executeQuery();
			Object msg;
			if(rs.next()) {
				String pattern = rs.getString(1);
				msg = new MessageFormat(pattern);
			} else {
				msg =  EMPTY;
			}		
			cache.put(messageCode,msg);
			log.debug("Retrieved message for " + messageCode);
			return msg;	
		} catch(SQLException sqle) {
			log.warn("Exception while retrieving message " + messageCode, sqle);
			return "COULD NOT RETRIEVE MESSAGE BECAUSE: " + sqle;
		} catch (DataLayerException e) {
            log.warn("Exception while retrieving message " + messageCode, e);
            return "COULD NOT RETRIEVE MESSAGE BECAUSE: " + e;
        } finally {
            if(da != null) {
                if(ps != null) try { ps.close(); } catch(SQLException e) {}
                try { da.close(); } catch(SQLException e) {}
            }
        }
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public static String getDefaultSource() {
		return defaultSource;
	}
	
	public static void setDefaultSource(String newDefaultSource) {
		defaultSource = newDefaultSource;
	}
	
}
