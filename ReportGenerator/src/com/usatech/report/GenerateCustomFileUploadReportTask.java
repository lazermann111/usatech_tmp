package com.usatech.report;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.activation.DataSource;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
/**
 * 
 * @author yhe
 *
 */
public class GenerateCustomFileUploadReportTask extends AbstractRetrieveFileTask {
	private static final Log log = Log.getLog();

	public GenerateCustomFileUploadReportTask() {
		super(Generator.CUSTOM_FILE_UPLOAD);
	}

	protected GenerateCustomFileUploadReportTask(Generator generator) {
		super(generator);
	}

	/**
	 * @throws IOException
	 * @throws ConvertException
	 * @throws DataLayerException
	 * @throws SQLException
	 * @see com.usatech.report.AbstractRetrieveFileTask#generateDataSource(java.util.Map, StandbyAllowance)
	 */
	@Override
	protected DataSource generateDataSource(Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception {
		return generateCustomFileUpload(ConvertUtils.getLong(params.get("fileTransferId")), standbyAllowance);
	}
	
	protected DataSource generateCustomFileUpload(long fileTransferId, StandbyAllowance standbyAllowance) throws SQLException, DataLayerException, ConvertException, IOException {
		if(log.isDebugEnabled())
			log.debug("Generate Custom File Upload for fileTransferId = " + fileTransferId);
		DataSource ds = getCustomFileUpload(fileTransferId, standbyAllowance);
		if(log.isDebugEnabled())
			log.debug("Successfully retrieved Custom File Upload with fileTransferId =  " + fileTransferId);
		return ds;
	}

	protected DataSource getCustomFileUpload(long fileTransferId, StandbyAllowance standbyAllowance) throws SQLException, DataLayerException, ConvertException, IOException {
		try (Connection conn = getReportConnection(standbyAllowance)) {
			try (Results results = DataLayerMgr.executeQuery(conn, "GET_CUSTOM_FILE_UPLOAD", new Object[] { fileTransferId })) {
				if(results.next()) {
					final String fileName = results.getValue(1, String.class);
					Object content = results.getValue(2);
					final String fileContentType = results.getValue(3, String.class);
					if(content == null) {
						throw new IOException("The fileTransferId " + fileTransferId + " fileTransferContent is not stored in the database.");
					} else {
						final byte[] data = ConvertUtils.convert(byte[].class, content);
						return new DataSource() {
							public String getContentType() {
								return fileContentType;
							}

							public InputStream getInputStream() throws IOException {
								try {
									return new ByteArrayInputStream(data);
								} catch(Exception e) {
									if(e instanceof IOException)
										throw (IOException) e;
									IOException ioe = new IOException(e.getMessage());
									ioe.initCause(e);
									throw ioe;
								}
							}

							public String getName() {
								return fileName;
							}

							public OutputStream getOutputStream() throws IOException {
								return null;
							}
						};
					}

				} else
					throw new SQLException("Custom File Upload " + fileTransferId + " not found in database");
			}
		}
	}

}