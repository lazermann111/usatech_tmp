package com.usatech.report;

import java.beans.IntrospectionException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.zip.InflaterInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.SAXException;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.DynaBean;
import simple.bean.MapDynaBean;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceFactory;
import simple.falcon.engine.DesignEngine;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Layout;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.engine.standard.DirectoryResourceResolver;
import simple.falcon.engine.standard.PagableOutput;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardDesignEngine2;
import simple.falcon.engine.standard.StandardExecuteEngine;
import simple.falcon.engine.standard.StandardFolio;
import simple.falcon.engine.standard.URLResourceResolver;
import simple.falcon.engine.util.EngineUtils;
import simple.falcon.run.BasicExecutor;
import simple.falcon.run.FolioUpdateUtils;
import simple.falcon.run.ReportRunner;
import simple.io.ByteInput;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.lang.Initializer;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.results.Results;
import simple.sql.BuildSQLException;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.util.FilterMap;
import simple.util.SiftedMap;
import simple.xml.ObjectBuilder;

public class GenerateFolioReportTask extends AbstractGenerateTask{
	private static final Log log = Log.getLog();
	protected String baseUrl;
	protected String cssDirectory;
	protected ReportRunner reportRunner;
	protected boolean usalive15Compatible = false;
	protected String reportDataSourceName;
	protected String designDataSourceName;
	protected Publisher<ByteInput> publisher;
	protected int folioMaxRows=100000;
	
	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	protected final Set<String> siftedParameters = new HashSet<String>(Arrays.asList(new String[] {
		"reportXML","reportId","folioIds","folioId","is_custom_report"		
	}));
	protected final DirectoryResourceResolver resourceResolver = new DirectoryResourceResolver(new URLResourceResolver());
	protected final MessageChainTask reloadMetaDataTask = new MessageChainTask() {	
		public int process(MessageChainTaskInfo taskInfo) {
			try {
				reloadMetaData();
			} catch(InterruptedException e) {
				log.warn("Could not reload meta data", e);
				return 5;
			} catch(DesignException e) {
				log.warn("Could not reload meta data", e);
				return 5;
			} catch(ServiceException e) {
				log.warn("Could not reload meta data", e);
				return 5;
			}
			return 0;
		}
	};
	protected final StandardDesignEngine designEngine = new StandardDesignEngine2();
	protected final StandardExecuteEngine executeEngine = new StandardExecuteEngine(designEngine);

	public GenerateFolioReportTask() {
		super(Generator.FOLIO);
	}

	protected final Initializer<ServiceException> initializer = new Initializer<ServiceException>() {
		@Override
		protected void doInitialize() throws ServiceException {
			DataSourceFactory dsf = DataLayerMgr.getGlobalDataLayer().getDataSourceFactory();
			// Engine 2
			executeEngine.setDataSourceFactory(dsf);
			executeEngine.setDefaultDataSourceName(reportDataSourceName);
			executeEngine.setDesignDataSourceName(designDataSourceName);
			executeEngine.setCssDirectory(getCssDirectory());
			resourceResolver.setPrefixMatcher(baseUrl);
			executeEngine.setResourceResolver(resourceResolver);

			reportRunner = new ReportRunner(executeEngine);
		}

		@Override
		protected void doReset() {
			// never called so do nothing
		}
	};
	
	@Override
	protected void generateReport(Map<String, Object> attributes, MessageChainTaskInfo taskInfo, StandbyAllowance standbyAllowance) throws Exception {
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		Generator generator = ConvertUtils.convert(Generator.class, attributes.get("request.generatorId"));
		if(generator != this.generator) {
			throw new ConvertException("Report Request for Generator " + generator + " was sent to the queue for " + this.generator, null, generator);
		}
		String title = ConvertUtils.getString(attributes.get("request.title"), null);
		int userId = ConvertUtils.getInt(attributes.get("request.userId"));
		Map<String, Object> params = new LinkedHashMap<>();
		for(Map.Entry<String, Object> entry : attributes.entrySet())
			if(entry.getKey() != null && entry.getKey().startsWith("report.") && !GenerateAttrEnum.ATTR_MAX_STANDBY_WAIT.getValue().equals(entry.getKey()) && !GenerateAttrEnum.ATTR_USE_STANDBY.getValue().equals(entry.getKey()))
				params.put(entry.getKey().substring(7), entry.getValue());
		long requestId = ConvertUtils.getLong(attributes.get("request.requestId"));
		params.put("scene.properties.requestId", requestId);

		DataLayerMgr.selectInto("NEXT_STORED_FILE_ID", new FilterMap<Object>(resultAttributes, "transport.", null));
		params.put("scene.properties.fileId", resultAttributes.get("transport.fileId"));

		Resource resource = writeResource(title, userId, params, taskInfo, standbyAllowance);
		int pageIndex = ConvertUtils.getIntSafely(resultAttributes.get("file.pageId"), -1);
		if(pageIndex > -1) {
			pageIndex++;
		} else {
			pageIndex = 0;
		}
		store(attributes, resultAttributes, resource, pageIndex, pageIndex);
	}
	
	public static class BasicExecutorPlus extends BasicExecutor {
		protected Long customerId;

		public BasicExecutorPlus() {
		}

		public Long getCustomerId() {
			return customerId;
		}

		public void setCustomerId(Long customerId) {
			this.customerId = customerId;
		}
	}
	protected Resource generateFolioReport(Report report, int userId, Scene scene, Map<String, Object> params, String fileName, String contentType, MessageChainTaskInfo taskInfo) throws Exception{
		params.put("folioMaxRows", getFolioMaxRows());
		Results results = DataLayerMgr.executeQuery("GET_USER_INFO", new Object[] { userId }, false);
		if(!results.next())
			throw new SQLException("Zero rows returned");
		int userType = results.getValue("userType", int.class);
		Long customerId = results.getValue("customerId", Long.class);
		int[] userPrivs = results.getValue("userPrivs", int[].class);
		BasicExecutorPlus executor = new BasicExecutorPlus();
		executor.setUserId(userId);
		executor.setCustomerId(customerId);
		executor.setUserGroupIds(ReportUtil.retrieveUserGroups(userPrivs, userType));// see com.usatech.usalive.hybrid.HybridServlet
		executor.setDisplayName(results.getValue("displayName", String.class));
		if(log.isDebugEnabled())
			log.debug("Generating report");
		
		FolioPagableOutput output=new FolioPagableOutput(resourceFolder, fileName, contentType, taskInfo, publisher);
		try{
			reportRunner.runReport(report, params, executor, scene, output);
		}finally{
			output.close();
		}
		
		ExecuteEngine ee = reportRunner.getExecuteEngine();
		if(ee != null && ee.getTaskListener() != null)
			ee.getTaskListener().flush();
		return output.getCurrentResource();
	}

	protected Resource writeResource(String title, int userId, Map<String, Object> params, MessageChainTaskInfo taskInfo, StandbyAllowance standbyAllowance) throws Exception {
		initializer.initialize();
		TimeZone timeZone = ConvertUtils.convertSafely(TimeZone.class, params.get("scene.timeZone"), null);
		Locale locale = ConvertUtils.convertSafely(Locale.class, params.get("scene.locale"), null);
		String baseUrl = ConvertUtils.getStringSafely(params.get("scene.baseUrl"), this.baseUrl);
		String runReportAction = ConvertUtils.getStringSafely(params.get("scene.runReportAction"), null);
		boolean live = ConvertUtils.getBooleanSafely(params.get("scene.live"), false);
		Layout layout = ConvertUtils.convertSafely(Layout.class, params.get("scene.layout"), null);
		Scene scene = reportRunner.createScene(timeZone, locale, baseUrl, runReportAction, live, layout);
		scene.getProperties().putAll(new FilterMap<Object>(params, "scene.properties.", null));
		scene.setUserAgent(ConvertUtils.getStringSafely(params.get("scene.userAgent")));

		Report report = getReport(scene, params, userId, title, standbyAllowance);
		String contentType = report.getOutputType().getContentType();
		String fileName = report.getOutputType().generateFileName(report, params);

		Map<String, Object> siftedParams = new SiftedMap<String, Object>(params) {
			@Override
			protected boolean siftKey(Object key, boolean update) {
				if(!(key instanceof String))
					return true;
				String s = (String)key;
				return s.startsWith("scene.") || siftedParameters.contains(s);
			}
		};
		
		Resource resource = generateFolioReport(report, userId, scene, siftedParams, fileName, contentType, taskInfo);
		log.debug("Successfully generated report with params =  " + params);
		return resource;
		
	}

	protected Report readReport(Scene scene, Map<String, Object> reportParams, int userId) throws DesignException, ConvertException {
		boolean sqlFolio = ConvertUtils.getBoolean(reportParams.get("isSQLFolio"), false);
		if(sqlFolio)
			return readBasic(scene, reportParams, userId);
		byte[] reportXml = ConvertUtils.convert(byte[].class, reportParams.get("reportXML"));
		if(reportXml != null && reportXml.length > 0) {
			InputStream in = new ByteArrayInputStream(reportXml);
			Reader reader = new InputStreamReader(ReportRequest.isDefaultCompressing() ? new InflaterInputStream(in) : in, ReportRunner.XML_CHARSET);
			
	    	try {
				if(log.isDebugEnabled()) {
					in.mark(reportXml.length);
					log.debug("Building Report Definition from XML: \n" + IOUtils.readFully(reader));
					in.reset();
					reader = new InputStreamReader(ReportRequest.isDefaultCompressing() ? new InflaterInputStream(in) : in, ReportRunner.XML_CHARSET);
				}
	    		ObjectBuilder builder = new ObjectBuilder(reader);
	    		String className = builder.getAttrValue("class");
	    		if(className != null && className.equals(Report.class.getName())) {
	    			Report report = reportRunner.getExecuteEngine().getDesignEngine().newReport(scene.getTranslator());
	    	        report.readXML(builder);
	    	        log.debug("Built Report Definition from XML");	    			
	    	        return report;
	    		}
	    	} catch(IOException e) {
				throw new DesignException("Could not read report definition", e);
			} catch(SAXException e) {
				throw new DesignException("Could not read report definition", e);
			} catch(ParserConfigurationException e) {
				throw new DesignException("Could not read report definition", e);
			} finally {
	    		try {
					reader.close();
				} catch(IOException e) {
					//Ignore
				}
	    	}			
		} 
		Long reportId = ConvertUtils.convert(Long.class, reportParams.get("reportId"));

		if(reportId != null) {
			Report report = reportRunner.getReport(reportId, scene);
			log.debug("Built Report Definition from report id");	    			
	        return report;
		}

		long[] folioIds = ConvertUtils.convert(long[].class, reportParams.get("folioIds"));
		if(folioIds != null && folioIds.length > 0) {
			Report report = reportRunner.getReportFromFolios(folioIds, scene);
			log.debug("Built Report Definition from list of folio ids");	    			
	        return report;
		}

		Long folioId = ConvertUtils.convert(Long.class, reportParams.get("folioId"));
		if(folioId != null) {
			Report report = reportRunner.getReportFromFolio(folioId, scene);
			log.debug("Built Report Definition from folio id");	    			
	        return report;
		}
		throw new DesignException("One of isSQLFolio, reportXML, reportId, folioIds or folioId must have a value.");

	}

	protected Report getReport(Scene scene, Map<String, Object> params, int userId, String title, StandbyAllowance standbyAllowance) throws DesignException, ConvertException, IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ParseException, SQLException, DataLayerException, BeanException, InvalidIntValueException, ServiceException {
		log.debug("Building Report Definition");
		Integer outputType = ConvertUtils.convertSafely(Integer.class, params.get("outputType"), null);
		// this is a param called reportId in report_param table and in folio_conf.report's reportId, not report.reports reportId
		Integer basicReportId = ConvertUtils.convert(Integer.class, params.get("basicReportId"));
		Map<String, Object> reportParams;
		if(basicReportId != null) {
			Map<String, Object> basicParams = readBasicParameters(basicReportId, params, userId, title);
			title = ConvertUtils.getString(basicParams.get("report.title"), title);
			if(outputType == null)
				outputType = ConvertUtils.convertSafely(Integer.class, basicParams.get("report.outputType"), null);
			reportParams = new FilterMap<>(basicParams, "report.", null);
			for(Map.Entry<String, Object> entry : reportParams.entrySet()) {
				if(entry.getKey().startsWith("params."))
					params.put(entry.getKey(), entry.getValue());
			}
		} else {
			reportParams = params;
		}
		Report report = readReport(scene, reportParams, userId);
		if(!StringUtils.isBlank(title)) {
			Format titleFormat = ConvertUtils.getFormat("LITERAL", title);
			Folio folio;
			if(report.getTitle() != null) {
				report.setTitle(titleFormat);
				report.setSubtitle(null);
			} else if(report.getSubtitle() != null)
				report.setSubtitle(titleFormat);
			else if(report.getFolios() == null || report.getFolios().length == 0)
				report.setTitle(titleFormat);
			else if((folio = report.getFolios()[0]).getTitle() != null) {
				folio.setTitle(titleFormat);
				folio.setSubtitle(null);
			} else if(folio.getSubtitle() != null)
				folio.setSubtitle(titleFormat);
			else
				folio.setTitle(titleFormat);
		}

		boolean customReport = ConvertUtils.getBoolean(reportParams.get("is_custom_report"), false);
		if(customReport) {
			DesignEngine designEngine = reportRunner.getExecuteEngine().getDesignEngine();
			DynaBean form = new MapDynaBean(reportParams);
			Folio singleFolio;
			if(report.getFolios() != null && report.getFolios().length == 1 && (singleFolio=report.getFolios()[0]) != null) {
	            log.debug("Updating pillars");
	            FolioUpdateUtils.addBlankPillars(singleFolio, form, designEngine);
	            FolioUpdateUtils.updatePillar(singleFolio, form, designEngine);
	            log.debug("Adding filters");
	            if(designEngine instanceof StandardDesignEngine)
	            	FolioUpdateUtils.addFilters(singleFolio, form, (StandardDesignEngine)designEngine);
	            if(ConvertUtils.getBoolean(form.get("doChartConfigUpdate"), false)) {
	                log.debug("Configuring chart");
	                FolioUpdateUtils.chartConfig(singleFolio,form,designEngine);
	        	}
	            log.debug("Updating titles");
				String subtitle = ConvertUtils.getString(form.get("subtitle"), null);
				if(subtitle != null) {
					if(subtitle.trim().length() > 0)
						singleFolio.setSubtitle(ConvertUtils.getFormat(subtitle));
					else
						singleFolio.setSubtitle(null);
				}
	        	log.debug("Updating directives");
	            FolioUpdateUtils.updateDirectives(singleFolio, form);
			} else {
				log.debug("Updating titles");
				FolioUpdateUtils.updateTitles(report, form, designEngine);
			}
        	log.debug("Updating directives");
            FolioUpdateUtils.updateDirectives(report, form);
		}
		if(outputType != null) {
            log.debug("Setting output type");
            report.setOutputType(reportRunner.getExecuteEngine().getDesignEngine().getOutputType(outputType));
        }
        if(report.getOutputType() == null)
        	report.setOutputType(reportRunner.getExecuteEngine().getDesignEngine().getOutputType(22));
		switch(standbyAllowance) {
			case ALLOW:
			case WAIT_ON_REQUEST:
				for(Folio folio : report.getFolios()) {
					if(folio instanceof StandardFolio)
						try {
							((StandardFolio) folio).setDataSourceName(designEngine.findDataSourceName(folio) + "_STANDBY");
						} catch(BuildSQLException e) {
							throw new DesignException(e);
						}
				}
		}
        return report;
	}

	protected Report readBasic(Scene scene, Map<String, Object> params, int userId) throws DesignException {
		return EngineUtils.getSQLFolioReport(reportRunner.getExecuteEngine(), params, 0);
	}

	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public boolean isUsalive15Compatible() {
		return usalive15Compatible;
	}

	public void setUsalive15Compatible(boolean usalive15Compatible) {
		this.usalive15Compatible = usalive15Compatible;
	}
	
	public void reloadMetaData() throws InterruptedException, ServiceException, DesignException {
		initializer.initialize();
		DesignEngine de = reportRunner.getExecuteEngine().getDesignEngine();
		de.reloadOperators();
		de.reloadOutputTypes();
		de.reloadJoins();
		de.reloadFields();
		de.reloadFilters();
		de.reloadFolios();
		de.reloadReports();
	}
	
	public MessageChainTask getReloadMetaDataTask() {
		return reloadMetaDataTask;
	}

	public File getResourceBaseDirectory() {
		return resourceResolver.getBaseDirectory();
	}

	public void setResourceBaseDirectory(File baseDirectory) {
		resourceResolver.setBaseDirectory(baseDirectory);
	}

	public String getCssDirectory() {
		return cssDirectory;
	}

	public void setCssDirectory(String cssDirectory) {
		this.cssDirectory = cssDirectory;
	}

	public String getReportDataSourceName() {
		return reportDataSourceName;
	}

	public void setReportDataSourceName(String reportDataSourceName) {
		this.reportDataSourceName = reportDataSourceName;
	}

	public String getDesignDataSourceName() {
		return designDataSourceName;
	}

	public void setDesignDataSourceName(String designDataSourceName) {
		this.designDataSourceName = designDataSourceName;
	}

	public StandardDesignEngine getDesignEngine() {
		return designEngine;
	}

	public StandardExecuteEngine getExecuteEngine() {
		return executeEngine;
	}

	@Override
	protected Resource writeResource(String title, int userId, Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception {
		return null;
	}	
	
	public static final ThreadSafeDateFormat todayDateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMdd"));

	public static String getCurrentDate() {
		return todayDateFormat.format(new Date());
	}
    public static String appendDateDir(String fileName) {
		return getCurrentDate() + "/" + fileName;
	}
    
    public static String appendPageIndex(String fileName, int pageIndex) {
		return "p"+pageIndex+"-"+fileName;
	}
    
	protected class FolioPagableOutput implements PagableOutput{
		protected ResourceFolder resourceFolder;
		protected String fileName;
		protected String contentType;
		protected int pageIndex=0;
		protected Writer writer;
		protected OutputStream outputStream;
		protected Resource currentResource;
		protected MessageChainTaskInfo taskInfo;
		protected Publisher<ByteInput> publisher;
		protected int maxPageId;
		
		
		public int getMaxPageId() {
			return maxPageId;
		}

		public void setMaxPageId(int maxPageId) {
			this.maxPageId = maxPageId;
		}

		public FolioPagableOutput(ResourceFolder resourceFolder, String fileName,
				String contentType, MessageChainTaskInfo taskInfo, Publisher<ByteInput> publisher) {
			super();
			this.resourceFolder = resourceFolder;
			this.fileName = fileName;
			this.contentType = contentType;
			this.taskInfo = taskInfo;
			this.publisher=publisher;
		}
	    
	    protected Resource getResourceInstance(ResourceFolder resourceFolder, String fileName) throws IOException{
			return resourceFolder.getResource(appendDateDir(fileName), ResourceMode.CREATE);
		}
		public Writer getWriter() throws IOException {
			if(writer==null){
				Resource resource = getResourceInstance(resourceFolder, fileName);
				resource.setContentType(contentType);
				currentResource=resource;
				outputStream=resource.getOutputStream();
				writer=new OutputStreamWriter(outputStream);
				return writer;
			}
			return writer;
	    }
	    public OutputStream getOutputStream() throws IOException {
	    	if(outputStream==null){
				Resource resource = getResourceInstance(resourceFolder, fileName);
				resource.setContentType(contentType);
				currentResource=resource;
				outputStream=resource.getOutputStream();
				writer=new OutputStreamWriter(outputStream);
				return outputStream;
			}
			return outputStream;
	    }
		public Result getResult() throws IOException {
			if(outputStream==null){
				Resource resource = getResourceInstance(resourceFolder, fileName);
				resource.setContentType(contentType);
				currentResource=resource;
				outputStream=resource.getOutputStream();
				return new StreamResult(outputStream);
			}
			return new StreamResult(outputStream);
		}

		public void close() throws IOException {
			try {
				if(outputStream != null) {
					OutputStream temp = outputStream;
					outputStream = null;
					temp.close();
				}
			} finally {
				if(currentResource != null)
					currentResource.release();
			}
		}
		public void next() throws IOException, ServiceException{
			close();
			try{
				MessageChainStep currentStep=taskInfo.getStep();
				MessageChain mc=currentStep.getMessageChain();
				mc.setResultCode(0);
				store(currentStep.getAttributes(), currentStep.getResultAttributes(), currentResource, pageIndex, maxPageId);
				MessageChainService.publish(mc, publisher);
			}catch(Exception e){
				throw new ServiceException("Failed to store generated report page.", e);
			}
			
			pageIndex++;
			
			currentResource=getResourceInstance(resourceFolder, fileName);
			currentResource.setContentType(contentType);
			outputStream=currentResource.getOutputStream();
			writer=new OutputStreamWriter(outputStream);
		}
		public Resource getCurrentResource() {
			return currentResource;
		}
	}

	public int getFolioMaxRows() {
		return folioMaxRows;
	}

	public void setFolioMaxRows(int folioMaxRows) {
		this.folioMaxRows = folioMaxRows;
	}
	
	
}
