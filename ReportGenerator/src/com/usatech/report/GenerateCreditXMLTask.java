package com.usatech.report;

import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import com.usatech.report.rest.CreditData;
import com.usatech.report.rest.CreditTran;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class GenerateCreditXMLTask extends AbstractGenerateReportTask {
	private static final Log log = Log.getLog();

	public GenerateCreditXMLTask() {
		super(Generator.CREDIT_XML);
	}

	/**
	 * @see com.usatech.report.AbstractGenerateReportTask#getContentType()
	 */
	@Override
	protected String getContentType() {
		return generator.getContentType();
	}
	/**
	 * @see com.usatech.report.AbstractGenerateTask#generateReport(int, java.util.Map, java.io.OutputStream)
	 */
	@Override
	protected void generateReport(String title, int userId, Map<String, Object> params, OutputStream out, StandbyAllowance standbyAllowance) throws Exception {
		long reportTranId=ConvertUtils.getLong(params.get("reportTranId"));
		log.info("generateCreditXMLReport tranId="+reportTranId);
		try (Connection conn = getReportConnection(standbyAllowance)) {
			try (Results results = DataLayerMgr.executeQuery(conn, "GET_CREDIT_TRAN", new Object[] { reportTranId, userId })) {
				if(results.next()) {
					CreditTran tran = new CreditTran();
					results.fillBean(tran);
					CreditData data = new CreditData();
					data.add(tran);
					CreditData.toXML(data, out);
				} else {
					throw new SQLException("Credit Tran XML reportTranId= " + reportTranId + " not found in database");
				}
			}
		}
		out.flush();
	}
}
