package com.usatech.report;

import java.lang.management.ManagementFactory;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.TransportAttrEnum;
import com.usatech.transport.TransportErrorType;
import com.usatech.transport.TransportReason;

import simple.app.DatabasePrerequisite;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;

public class DataExchangeSentTask implements MessageChainTask, DataExchangeRetriesMXBean {
	private static final Log log = Log.getLog();
	protected int retriesLimit = 20;
	protected Publisher<ByteInput> publisher;
	protected ObjectName jmxObjectName;

	public DataExchangeSentTask() {
		registerJMX();
	}

	protected void registerJMX() {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		try {
			jmxObjectName = new ObjectName("AppJobs:Name=" + getClass().getSimpleName());
			mbs.registerMBean(this, jmxObjectName);
		} catch(MalformedObjectNameException | InstanceAlreadyExistsException | MBeanRegistrationException | NotCompliantMBeanException e) {
			log.error(e);
			jmxObjectName = null;
		} catch(NullPointerException e) {
			log.error(e);
			jmxObjectName = null;
		}
	}
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		try {
			Map<String, Object> params = new HashMap<>();
			long dataExchangeFileId = step.getAttribute(CommonAttrEnum.ATTR_FILE_ID, long.class, true);
			params.put("dataExchangeFileId", dataExchangeFileId);
			Boolean success = step.getAttribute(TransportAttrEnum.ATTR_SENT_SUCCESS, Boolean.class, false);
			if(success != null && success.booleanValue()) {
				params.put("processStatusCd", 'D');
				params.put("processCompleteTs", step.getAttribute(TransportAttrEnum.ATTR_SENT_DATE, Date.class, false));
				params.put("processDetails", step.getAttribute(TransportAttrEnum.ATTR_SENT_DETAILS, String.class, false));
			} else {
				Integer transportErrorTypeOrdinal = step.getAttribute(TransportAttrEnum.ATTR_ERROR_TYPE, Integer.class, false);
				if(transportErrorTypeOrdinal != null && transportErrorTypeOrdinal.intValue() == TransportErrorType.TRANSPORT_ERR.ordinal())
					params.put("processStatusCd", 'R');
				else
					params.put("processStatusCd", 'E');
				params.put("processCompleteTs", step.getAttribute(TransportAttrEnum.ATTR_ERROR_TIME, Date.class, false));
				params.put("processDetails", step.getAttribute(TransportAttrEnum.ATTR_ERROR_TEXT, String.class, false));
				// TODO: handle TransportAttrEnum.ATTR_ERROR_RETRY_PROPS
			}
			DataLayerMgr.executeCall("UPDATE_DATA_EXCHANGE_FILE", params, true);
			if(success != null && success.booleanValue())
				kickoffRetries(ConvertUtils.getInt(params.get("dataExchangeType")), true, taskInfo.getPublisher());
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(AttributeConversionException | DataLayerException | ConvertException e) {
			throw new ServiceException(e);
		}

		return 0;
	}

	public int kickoffRetries(int dataExchangeType, boolean all) throws RemoteException {
		try {
			return kickoffRetries(dataExchangeType, all, getPublisher());
		} catch(ServiceException e) {
			log.error("Could not kickoffRetries", e);
			throw new RemoteException(e.getMessage());
		}
	}

	protected int kickoffRetries(int dataExchangeType, boolean all, Publisher<ByteInput> publisher) throws ServiceException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("dataExchangeType", dataExchangeType);
			params.put("limit", all ? getRetriesLimit() : 1);
			try (Results results = DataLayerMgr.executeQuery("GET_DATA_EXCHANGE_RETRIES", params)) {
				while(results.next()) {
					MessageChain mc = new MessageChainV11();
					long transportId = results.getValue("transportId", Long.class);
					int transportTypeId = results.getValue("transportTypeId", Integer.class);

					// send via transport
					MessageChainStep transportStep = ReportRequestFactory.createTransportStep(mc, transportId, transportTypeId, null);
					transportStep.setAttribute(TransportAttrEnum.ATTR_FILE_ID, results.getValue("dataExchangeFileId"));
					transportStep.setAttribute(TransportAttrEnum.ATTR_FILE_KEY, results.getValue("resourceKey"));
					transportStep.setAttribute(TransportAttrEnum.ATTR_FILE_NAME, results.getValue("fileName"));
					// transportStep.setAttribute(TransportAttrEnum.ATTR_FILE_PASSCODE, null);
					// transportStep.setAttribute(TransportAttrEnum.ATTR_ERROR_RETRY_PROPS, null);
					transportStep.setAttribute(TransportAttrEnum.ATTR_TRANSPORT_REASON, TransportReason.CONDITION);

					// update transport details
					MessageChainStep updateStep = mc.addStep("usat.data.exchange.sent");
					updateStep.setAttribute(CommonAttrEnum.ATTR_FILE_ID, results.getValue("dataExchangeFileId"));
					updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_SENT_SUCCESS, transportStep, TransportAttrEnum.ATTR_SENT_SUCCESS);
					updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_SENT_DETAILS, transportStep, TransportAttrEnum.ATTR_SENT_DETAILS);
					updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_SENT_DATE, transportStep, TransportAttrEnum.ATTR_SENT_DATE);
					updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_ERROR_TEXT, transportStep, TransportAttrEnum.ATTR_ERROR_TEXT);
					updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_ERROR_TYPE, transportStep, TransportAttrEnum.ATTR_ERROR_TYPE);
					updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_ERROR_TIME, transportStep, TransportAttrEnum.ATTR_ERROR_TIME);
					updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_ERROR_RETRY_PROPS, transportStep, TransportAttrEnum.ATTR_ERROR_RETRY_PROPS);

					transportStep.setNextSteps(0, updateStep);
					transportStep.setNextSteps(1, updateStep);
					MessageChainService.publish(mc, publisher);
				}
				log.info("Sending " + results.getRowCount() + " files to retry transport");
				return results.getRowCount();
			}
		} catch(SQLException | DataLayerException | ConvertException e) {
			throw new ServiceException(e);
		}
	}

	public int getRetriesLimit() {
		return retriesLimit;
	}

	public void setRetriesLimit(int retriesLimit) {
		this.retriesLimit = retriesLimit;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
}
