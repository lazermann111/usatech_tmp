package com.usatech.report;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.BeanException;

public class TerminalInfo 
{
	public Long USER_ID;
	public Long MASTER_USER_ID;
	public Long TERMINAL_ID;
	public String SERIAL_NUMBER;
	public Long DEALER_ID;
	public String ASSET_NUMBER;
	public String MACHINE_MAKE;
	public String MACHINE_MODEL;
	public String TELEPHONE;
	public String OUTSIDE_LINE;
	public String REGION;
	public String LOCATION_NAME;
	public String LOCATION_DETAILS;
	public String ADDRESS;
	public String CITY;
	public String STATE;
	public String ZIP;
	public String COUNTRY;
	public Long CUSTOMER_BANK_ID;
	public Integer PAY_SCHEDULE;
	public String LOCATION_TYPE;
	public String LOCATION_TYPE_SPECIFY;
	public String PRODUCT_TYPE;
	public String PRODUCT_TYPE_SPECIFY;
	public String AUTH_MODE;
	public BigDecimal AVG_ITEM_PRICE;
	public Integer TIME_ZONE;
	public String DEX_CAPTURE;
	public String RECEIPT;
	public Integer VEND_MODE;
	public final Map<String, String> CUSTOM = new LinkedHashMap<String, String>();
	public String DOING_BUSINESS_AS;
	public Character POS_ENVIRONMENT;
	public String ENTRY_CAPABILITY;
	public Character PIN_CAPABILITY;
	public Long COMMISSION_BANK_ID;
	public String BUSINESS_TYPE;
	public String CUSTOMER_SERVICE_PHONE;
	public String CUSTOMER_SERVICE_EMAIL;
	public String DEALER_NAME;
	public Integer DEVICE_TYPE_ID;
	public Integer LOCATION_TYPE_ID;
	public Integer CURRENT_CURRENCY_ID;
	public Integer CURRENCY_ID;
	public boolean posTypeEditable;
	public String CLIENT;
	public final Set<String> COLUMNS = new LinkedHashSet<>();
	public String COLUMN_MAP_NAME;
	public String COLUMN_MAP;
	public Long CUSTOMER_ID;
	public Long COLUMN_MAP_USER_ID;
	public String AUTO_UPDATE_COLUMN_MAP;
	public Long DISTRIBUTOR_ID;
	public String activatedBy;
	
	public TerminalInfo(String serialNumber)
	{
		this.SERIAL_NUMBER = serialNumber;
	}
	
	public void loadFromDatabase(Connection connection) throws SQLException, DataLayerException, BeanException
	{
		DataLayerMgr.selectInto(connection, "GET_TERMINAL", this);
//		if(DEVICE_TYPE_ID == null)
//			throw new SQLException("Invalid serial # " + SERIAL_NUMBER, null, 20201);
//		if(TERMINAL_ID == null)
//			throw new SQLException("Serial # " + SERIAL_NUMBER + " was never activated", null, 20218);
	}
	
	public void saveToDatabase()
	{
		
	}
}