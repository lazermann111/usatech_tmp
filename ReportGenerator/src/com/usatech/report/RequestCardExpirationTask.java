package com.usatech.report;


import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import simple.app.Publisher;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.QuartzCronScheduledJob;
import com.usatech.layers.common.constants.ConsumerCommAttrEnum;
import com.usatech.layers.common.constants.ConsumerCommType;

@DisallowConcurrentExecution
public class RequestCardExpirationTask extends QuartzCronScheduledJob {
	protected static Publisher<ByteInput> publisher;	
	private static final Log log = Log.getLog();
	
	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {
		try {
			JobDataMap quartzJobMap = context.getMergedJobDataMap();
			//int[] notifyBeforeExpirationDays=ConvertUtils.convert(int[].class, quartzJobMap.get("notifyBeforeExpirationDays"));
			Results cardExpResults = DataLayerMgr.executeQuery("GET_CARD_ABOUT_TO_EXPIRE", new Object[] { quartzJobMap.get("expirationDays"),quartzJobMap.get("notifyBeforeExpirationDays")}, false);
			while(cardExpResults.next()) {
				MessageChain mc = new MessageChainV11();
				MessageChainStep step = mc.addStep("usat.consumer.communicate");
				step.setAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, cardExpResults.get("consumerAcctId"));
				step.setAttribute(ConsumerCommAttrEnum.ATTR_COMMUNICATION_TYPE, ConsumerCommType.CARD_EXPIRATION);
				step.setAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_PRIORITY, cardExpResults.get("replenishPriority"));
				step.setAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_CARD_MASKED, cardExpResults.get("replenishCardMasked"));
				step.setAttribute(ConsumerCommAttrEnum.ATTR_EXPIRATION_DATE, cardExpResults.get("expirationDate"));
				MessageChainService.publish(mc, publisher);
				DataLayerMgr.executeCall("UPDATE_CARD_EXPIRATION_NOTIFY", new Object[] {cardExpResults.get("replenishConsumerAcctId"),cardExpResults.get("expirationDate"),cardExpResults.get("consumerId")}, true);
			}
		} catch(Exception e) {
			log.error("Could not request card expiration notification", e);
		}

	}

	public static Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public static void setPublisher(Publisher<ByteInput> publisher) {
		RequestCardExpirationTask.publisher = publisher;
	}

}
