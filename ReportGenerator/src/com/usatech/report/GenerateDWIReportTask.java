package com.usatech.report;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.activation.DataSource;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ConfigSource;
import simple.io.Log;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class GenerateDWIReportTask extends GenerateDEXReportTask {
	private static final Log log = Log.getLog();
	protected String DWIFormatFile;
	protected DataRecordFormat drf = null;

	public GenerateDWIReportTask() {
		super(Generator.DWI);
	}

	/**
	 * @see com.usatech.report.GenerateDEXReportTask#generateDataSource(java.util.Map, StandbyAllowance)
	 */
	@Override
	protected DataSource generateDataSource(Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception {
		return generateDWIFile(ConvertUtils.getLong(params.get("dexId")), standbyAllowance);
	}

	protected DataSource generateDWIFile(long dexId, StandbyAllowance standbyAllowance) throws Exception {
		log.debug("Generate DWI for dex id = " + dexId);
		DataSource ds = getDexFile(dexId, standbyAllowance);
		StringBuilder sb = new StringBuilder();
		int cnt = 0;
		boolean dxeFound = false;
		String alarm = null;
		String line;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(ds.getInputStream()));
		try {
			// while((line=lnr.readLine()) != null) {
			for(line = lnr.readLine(); line != null; line = lnr.readLine()) {
				if(cnt == 0) {
					int p = line.indexOf("DXS*");
					if(p < 0)
						throw new Exception("No DXS* header found in the file id " + dexId + ", skipping file.");
					else
						line = line.substring(p);
				}
				if(line.length() > 0) {
					sb.append(line);
					sb.append("\r\n");
					cnt++;
					log.debug("Read line " + cnt + " of length " + line.length());
					if(line.startsWith("DXE*"))
						dxeFound = true;
					else if(alarm == null && (line.startsWith("MA") || line.startsWith("MC")))
						alarm = line;
				}
			}
			/*
			 * } catch(Error err) { System.out.println("ERROR!");
			 * err.printStackTrace(); throw new Exception(err.getMessage());
			 */
		} finally {
			log.debug("Closing file input stream");
			try {
				lnr.close();
			} catch(IOException ioe) {
			}
		}
		String fileSeqNum = StringUtils.pad(String.valueOf(dexId), '0', 4, Justification.RIGHT);// between 0001 and 9999
		String provider = "UST";
		String site = "1";

		Map<String, Object> formatParams = new HashMap<String, Object>();
		formatParams.put("DEXID", dexId);
		formatParams.put("DEXBODY", sb.toString().trim());
		formatParams.put("RECORDCOUNT", new Integer(cnt + 4));
		formatParams.put("INTEROG_STATUS", dxeFound ? "GOOD" : "FAIL");
		formatParams.put("FILE_SEQ_NUM", fileSeqNum);
		formatParams.put("SOURCE_DATA_PROVIDER", provider);
		formatParams.put("SOURCE_SITE_NUMBER", site);
		formatParams.put("RUN_DATE", new Date());

		DataLayerMgr.selectInto("GET_DWI_INFO", formatParams);
		// set VIU_NUMBER (3-digit provider + 12-digit unique device id between
		// 6,000,000 and 6,500,000)
		String serial = (String) formatParams.get("serialNumber");
		String viu = provider + "000006" + StringUtils.pad(serial, '0', 6, Justification.RIGHT);
		formatParams.put("VIU_NUMBER", viu);

		/*
		 * Alarm type and subtype should only be non-null when RECORD Type =
		 * 'VIU4' NOTE: MA and MC are the only ones we are supporting (we don't
		 * have documentation on the others) 'SE' = selection threshold alarm
		 * 'SO' = sold out alarm 'BV' = bill validator alarm 'EC' = exact change
		 * alarm 'MC' = machine configuration change 'MA' = Machine error (MA5
		 * errors)
		 */
		if(alarm != null) {
			Number dexType = (Number) formatParams.get("DEX_TYPE");
			if(dexType.intValue() == 4) {
				formatParams.put("ALARM_TYPE", alarm.substring(0, 2));
				formatParams.put("ALARM_SUBTYPE", alarm.substring(10));
			}
		}
		sb.setLength(0);
		log.debug("Formatting DWI Data...");
		DataRecordFormat format = getDataRecordFormat();
		format.format(formatParams, sb);// } catch(Throwable t) {
		// System.out.println("ERROR " +
		// t.getClass().getName() + ": " +
		// t.getMessage()); t.printStackTrace();
		// throw new Exception("BIG PROBLEM");}
		final byte[] content = sb.toString().getBytes();
		final String fileName = provider + site + "." + fileSeqNum;
		return new DataSource() {
			public String getContentType() {
				return generator.getContentType();
			}

			public InputStream getInputStream() throws IOException {
				return new ByteArrayInputStream(content);
			}

			public String getName() {
				return fileName;
			}

			public OutputStream getOutputStream() throws IOException {
				return null;
			}
		};
	}

	protected DataRecordFormat getDataRecordFormat() throws SQLException, java.io.IOException, javax.xml.parsers.ParserConfigurationException, org.xml.sax.SAXException, DataLayerException {
		if(drf == null) {
			drf = new DataRecordFormat(ConfigSource.createConfigSource(DWIFormatFile).getInputStream(), null /*DataLayerMgr.getConnection("report")*/);
		}
		return drf;
	}

	public String getDWIFormatFile() {
		return DWIFormatFile;
	}

	public void setDWIFormatFile(String formatFile) {
		DWIFormatFile = formatFile;
	}
}
