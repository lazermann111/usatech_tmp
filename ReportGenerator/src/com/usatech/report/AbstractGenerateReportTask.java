package com.usatech.report;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Map;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.falcon.engine.standard.StandardOutputType;
import simple.io.Log;
import simple.io.resource.Resource;

public abstract class AbstractGenerateReportTask extends AbstractGenerateTask {
	private static final Log log = Log.getLog();

	protected AbstractGenerateReportTask(Generator generator) {
		super(generator);
	}
	@Override
	protected Resource writeResource(String title, int userId, Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception {
		String contentType = getContentType();
		String fileName = StandardOutputType.fixFileName(StandardOutputType.appendFileExtention(new StringBuilder(title), contentType)).toString();
		Resource resource = getResourceInstance(fileName);
		resource.setContentType(contentType);
		try {
			OutputStream out = resource.getOutputStream();
			generateReport(title, userId, params, out, standbyAllowance);
			log.debug("Successfully generated report with params =  " + params);
			out.close();
		} catch(Exception e) {
			resource.release();
			throw e;
		} catch(Error e) {
			resource.release();
			throw e;
		}
		return resource;
	}

	/** Generates the report
	 * @param userId The userId for whome the report is being generated
	 * @param params A read-only maps of parameters for the report
	 * @param out The OutputStream on which to write the report
	 * @return The contentType (mime) of the report
	 * @throws SQLException
	 * @throws IOException
	 * @throws IllegalArgumentException
	 * @throws ConvertException
	 * @throws DataLayerException
	 */
	protected abstract void generateReport(String title, int userId, Map<String, Object> params, OutputStream out, StandbyAllowance standbyAllowance) throws Exception;

	protected abstract String getContentType() ;
}
