package com.usatech.report;

import java.util.Calendar;

public class BasicFrequency extends AbstractFrequency {
	protected final int dateField;
	protected final int amount;
	protected final int offset;
	
	public BasicFrequency(int dateField, int amount, int offset) {
		super();
		this.dateField = dateField;
		this.amount = amount;
		this.offset = offset;
	}

	@Override
	protected void setNextBeginDate(Calendar calendar) {
		if(offset != 0)
			calendar.add(Calendar.MILLISECOND, -offset);
		truncate(calendar);
		mod(calendar);
		next(calendar);		
	}

	protected void truncate(Calendar calendar) {
		switch(dateField) {
			case Calendar.YEAR: //set month to the first month
				calendar.set(Calendar.MONTH,0); //0 is first month
			case Calendar.MONTH:
				calendar.set(Calendar.DAY_OF_MONTH, 1);
			case Calendar.DATE:
				calendar.set(Calendar.HOUR_OF_DAY, 0);
			case Calendar.HOUR_OF_DAY:
				calendar.set(Calendar.MINUTE,0);
				calendar.set(Calendar.SECOND,0);
				calendar.set(Calendar.MILLISECOND,0);
				break;
			case Calendar.MINUTE:
				calendar.set(Calendar.SECOND,0);
				calendar.set(Calendar.MILLISECOND,0);
				break;
			case Calendar.WEEK_OF_YEAR:
				calendar.set(Calendar.DAY_OF_WEEK, 1);
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE,0);
				calendar.set(Calendar.SECOND,0);
				calendar.set(Calendar.MILLISECOND,0);
				break;
		}
	}
	protected void mod(Calendar calendar) {
		if(amount > 1) {
			int n = calendar.get(dateField);
			n = n - (n % amount);
			calendar.set(dateField, n);
		}
	}
	protected void next(Calendar calendar) {
		calendar.add(dateField, amount);
		if(offset != 0)
			calendar.add(Calendar.MILLISECOND, offset);
	}
	public int getDateField() {
		return dateField;
	}
}
