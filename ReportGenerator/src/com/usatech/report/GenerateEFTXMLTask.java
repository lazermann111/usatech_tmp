package com.usatech.report;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import com.usatech.report.rest.EFT;
import com.usatech.report.rest.EFTData;
import com.usatech.report.rest.EFTTerminal;
import com.usatech.report.rest.EFTTran;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

public class GenerateEFTXMLTask extends AbstractGenerateReportTask {
	private static final Log log = Log.getLog();

	public GenerateEFTXMLTask() {
		super(Generator.EFT_XML);
	}

	/**
	 * @see com.usatech.report.AbstractGenerateReportTask#getContentType()
	 */
	@Override
	protected String getContentType() {
		return generator.getContentType();
	}
	/**
	 * @see com.usatech.report.AbstractGenerateTask#generateReport(int, java.util.Map, java.io.OutputStream)
	 */
	@Override
	protected void generateReport(String title, int userId, Map<String, Object> params, OutputStream out, StandbyAllowance standbyAllowance) throws Exception {
		int docId=ConvertUtils.getInt(params.get("docId"));
		log.info("generateEFTXMLReport docId="+docId);
		try (Connection conn = getReportConnection(standbyAllowance)) {
			try (Results results = DataLayerMgr.executeQuery(conn, "GET_PAYMENT", new Object[] { docId, userId })) {
				if(results.next()) {
					EFT eft = new EFT();
					eft.setCustomerCode(results.getValue("customerCode", int.class));
					eft.setPaymentCode(docId);
					eft.setPaymentDate(results.getValue("paymentDate", Date.class));

					BigDecimal settledAmount = new BigDecimal("0.00");
					BigDecimal failedAmount = new BigDecimal("0.00");
					BigDecimal feeAmount = new BigDecimal("0.00");
					BigDecimal chargebackAmount = new BigDecimal("0.00");
					BigDecimal refundAmount = new BigDecimal("0.00");
					BigDecimal adjustmentAmount = new BigDecimal("0.00");
					BigDecimal netAmount = new BigDecimal("0.00");
					try (Results batchResults = DataLayerMgr.executeQuery(conn, "GET_PAYMENT_BATCHES", new Object[] { docId, userId })) {
						while(batchResults.next()) {
							EFTTerminal t = new EFTTerminal();
							BigDecimal tmp = batchResults.getValue("settledAmount", BigDecimal.class);
							if(tmp != null)
								settledAmount = settledAmount.add(tmp);
							tmp = batchResults.getValue("failedAmount", BigDecimal.class);
							if(tmp != null) {
								t.setFailedAmount(tmp);
								failedAmount = failedAmount.add(tmp);
							}
							tmp = batchResults.getValue("feeAmount", BigDecimal.class);
							if(tmp != null) {
								t.setFeeAmount(tmp);
								feeAmount = feeAmount.add(tmp);
							}
							tmp = batchResults.getValue("chargebackAmount", BigDecimal.class);
							if(tmp != null) {
								t.setChargebackAmount(tmp);
								chargebackAmount = chargebackAmount.add(tmp);
							}
							tmp = batchResults.getValue("refundAmount", BigDecimal.class);
							if(tmp != null) {
								t.setRefundAmount(tmp);
								refundAmount = refundAmount.add(tmp);
							}
							tmp = batchResults.getValue("adjustmentAmount", BigDecimal.class);
							if(tmp != null) {
								t.setAdjustmentAmount(tmp);
								adjustmentAmount = adjustmentAmount.add(tmp);
							}
							tmp = batchResults.getValue("netAmount", BigDecimal.class);
							if(tmp != null)
								netAmount = netAmount.add(tmp);
							t.setTerminalCode(batchResults.getValue("terminalCode", String.class));
							eft.addEFTTerminal(t);
							try (Results tranResults = DataLayerMgr.executeQuery(conn, "GET_PAYMENT_TRAN", new Object[] { docId, batchResults.getValue("paymentBatchIds") })) {
								while(tranResults.next()) {
									EFTTran tran = new EFTTran();
									tran.setTxnCode(tranResults.getValue("txnCode", String.class));
									tran.setTxnAmount(tranResults.getValue("txnAmount", BigDecimal.class));
									t.addEFTTran(tran);
								}
							}
						}
					}
					eft.setAdjustmentAmount(adjustmentAmount);
					eft.setChargebackAmount(chargebackAmount);
					eft.setFailedAmount(failedAmount);
					eft.setFeeAmount(feeAmount);
					eft.setRefundAmount(refundAmount);
					eft.setNetAmount(netAmount);
					eft.setSettledAmount(settledAmount);

					EFTData data = new EFTData();
					data.addEFT(eft);
					EFTData.toXML(data, out);
				} else
					throw new SQLException("EFT XML docId= " + docId + " not found in database");
			}
		}
	}
}
