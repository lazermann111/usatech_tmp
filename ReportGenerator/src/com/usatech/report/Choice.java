/*
 * ReportParameter.java
 *
 * Created on December 11, 2001, 4:35 PM
 */

package com.usatech.report;

/**
 *
 * @author  bkrug
 * @version
 */
import java.lang.reflect.Array;

public class Choice {	
	protected String label;
	protected Object value;
	protected String shortLabel;
	protected java.util.Map attributes = new java.util.HashMap();
	protected boolean valueIsArray = false;
	
	public Choice(String label, Object value) {
		this(label,value,null);
	}
	
	public Choice(String label, Object value, String shortLabel) {
		this.label = label;
		this.value = value;
		this.shortLabel = shortLabel;
		valueIsArray = (value != null && value.getClass().isArray());		
	}
	
	public Object removeAttribute(String name) {
		return attributes.remove(name);
	}

	public Object setAttribute(String name, Object value) {
		return attributes.put(name, value);
	}
	
	public Object getAttribute(String name) {
		return attributes.get(name);
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getShortLabel() {
		return (shortLabel == null ? label : shortLabel);
	}
	
	/** Getter for property value.
	 * @return Value of property value.
	 */
	public Object getValue() {
		return value;
	}
	
	public String toString() {
		return getLabel() + " (" + getValueString() + ")";
	}
	
	public String getValueString() {
		Object v = getValue();
		if(v == null) return "";
		if(valueIsArray) {
			StringBuffer sb = new StringBuffer();
			for(int i = 0; i < Array.getLength(v); i++) {
				if(i > 0) sb.append(", ");
				sb.append(Array.get(v,i));
			}
			return sb.toString();
		} else if(v instanceof Choice) {
			return ((Choice)v).getValueString();
		} else {
			return String.valueOf(v);
		}
	}
	
	public boolean equals(Object o) {
		if(o instanceof Choice) {
			if(value == null) return ((Choice)o).getValue() == null;
			return value.equals(((Choice)o).getValue());
		} else if(value == null) {
			return o == null;
		} else if(o instanceof String) {
			return toString().equals(o) || getValueString().equals(o) || inArray(o);
		} else {
			return getValue().equals(o) || inArray(o);
		}
	}
	
	protected boolean inArray(Object o) {
		if(!valueIsArray) return false;
		for(int i = 0; i < Array.getLength(value); i++) {
			Object elem = Array.get(value,i);
			if(elem == null && o == null) return true;
			if(elem != null && elem.equals(o) || elem.toString().equals(o.toString())) return true;
		}
		return false;
	}
	
	public int hashCode() {
		return getValueString().hashCode();
	}
}
