package com.usatech.report;

import java.util.Date;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
/**
 * Class for resend timed report.
 * @author yhe
 *
 */
public class RequestResendReportTask extends AbstractRequestTask {
	private static final Log log = Log.getLog();
	
	@Override
	protected String getListCallId() {
		return "GET_RESENT_REPORTS";
	}

	@Override
	protected Object getListCallParams() {
		return null;
	}

	@Override
	protected String getProcessCd() {
		return "RESEND_REPORT_REQUEST";
	}

	@Override
	protected String getProcessDesc() {
		return "Resend Report Request";
	}

	@Override
	protected void handleItem(Map<String, Object> itemParams) {
		int userReportId;
		try {
			userReportId = ConvertUtils.getInt(itemParams.get("userReportId"));
		} catch(ConvertException e) {
			log.error("Could not convert userReportId", e);
			return;
		}
		try {
			ReportScheduleType batchType = ConvertUtils.convert(ReportScheduleType.class, itemParams.get("batchType"));
			Long batchId = ConvertUtils.convert(Long.class, itemParams.get("batchId"));
			Date beginDate = ConvertUtils.convert(Date.class, itemParams.get("beginDate"));
			Date endDate = ConvertUtils.convert(Date.class, itemParams.get("endDate"));
			Long refreshStoredFileId = ConvertUtils.convert(Long.class, itemParams.get("refreshStoredFileId"));
			if(log.isInfoEnabled()) {
				if(batchId == null)
					log.info("Generating Timed Report " + userReportId + " for " + beginDate + " to " + endDate);
				else
					log.info("Generating Batch Report " + userReportId + " for " + batchType + " " + batchId);
			}
			int userId = ConvertUtils.getInt(itemParams.get("userId"));
			int reportId = ConvertUtils.getInt(itemParams.get("reportId"));
			Generator generator = ConvertUtils.convertRequired(Generator.class, itemParams.get("generatorId"));
			String title = ConvertUtils.convert(String.class, itemParams.get("title"));
			String frequencyName = (batchId == null ? ConvertUtils.convert(String.class, itemParams.get("frequencyName")) : batchType.getDescription());

			reportRequestFactory.generateReport(batchType, userReportId, userId, reportId, generator, title, batchId, beginDate, endDate, frequencyName, refreshStoredFileId, "");
		} catch(ConvertException e) {
			log.error("Could not generate resend report request for user report " + userReportId, e);
		} catch(ServiceException e) {
			log.error("Could not publish resend report request for user report " + userReportId, e);
		}
	}
}
