package com.usatech.report;

import java.sql.Connection;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.QuartzCronScheduledWithPublisherJob;
import com.usatech.layers.common.constants.TransportAttrEnum;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
/**
 * Class for request transport retry
 * @author yhe
 *
 */
@DisallowConcurrentExecution
public class  RequestRetryTransportTask extends QuartzCronScheduledWithPublisherJob {
	private static final Log log = Log.getLog();

	@Override
	public void executePostConfigure(JobExecutionContext context)
			throws JobExecutionException {
		String errorId=null;
		try {
			JobDataMap quartzJobMap = context.getMergedJobDataMap();
			Results transportResults = DataLayerMgr.executeQuery("GET_RETRY_TRANSPORT", null);
			while(transportResults.next()){
				long transportId = ConvertUtils.getLongSafely(transportResults.get("transportId"), 0L);
				MessageChainV11 mc=new MessageChainV11();
				MessageChainStep transportStep = ReportRequestFactory.createTransportStep(mc, transportId, ConvertUtils.getInt(transportResults.get("transportTypeId")), null);
				MessageChainStep updateReportSentStep = mc.addStep(ReportRequestFactory.QUEUE_KEY_UPDATE_REPORT_SENT);
				MessageChainStep transportErrorStep = mc.addStep(ReportRequestFactory.QUEUE_KEY_TRANSPORT_ERROR);
				transportStep.setNextSteps(0, updateReportSentStep);
				transportStep.setNextSteps(1, transportErrorStep);
	
				transportStep.addStringAttribute("transport.transportReason", transportResults.get("batchId")==null?"TIMED_REPORT":"BATCH_REPORT");
				transportStep.addStringAttribute("file.fileName", ConvertUtils.getStringSafely(transportResults.get("fileName")));
				transportStep.addStringAttribute("file.fileKey", ConvertUtils.getStringSafely(transportResults.get("fileKey")));
				transportStep.addStringAttribute("file.fileContentType", ConvertUtils.getStringSafely(transportResults.get("fileContentType")));
				transportStep.addStringAttribute("transport.fileId", ConvertUtils.getStringSafely(transportResults.get("fileId")) );
				transportStep.addStringAttribute("transport.filePasscode",ConvertUtils.getStringSafely(transportResults.get("filePasscode")));
				transportStep.addStringAttribute("transport.retryProps",ConvertUtils.getStringSafely(transportResults.get("retryProps")));
				errorId= ConvertUtils.getStringSafely(transportResults.get("errorId"));
				Results results = DataLayerMgr.executeQuery("GET_CCS_TRANSPORT_ERROR_ATTR", new Object[] { errorId}, false);
				while(results.next()) {
					transportErrorStep.addStringAttribute(ConvertUtils.getStringSafely(results.getValue("attrName")), ConvertUtils.getStringSafely(results.getValue("attrValue")));
					transportStep.addStringAttribute(ConvertUtils.getStringSafely(results.getValue("attrName")), ConvertUtils.getStringSafely(results.getValue("attrValue")));
				}
				transportErrorStep.addReferenceAttribute("error.transportError", transportStep,"error.transportError" );
				transportErrorStep.addReferenceAttribute("error.errorText", transportStep,"error.errorText" );
				transportErrorStep.addReferenceAttribute("error.errorTs", transportStep,"error.errorTs" );
				transportErrorStep.addReferenceAttribute("error.errorTypeId", transportStep, "error.errorTypeId");
				transportErrorStep.addReferenceAttribute("error.retryProps", transportStep, "error.retryProps");
				transportErrorStep.addStringAttribute("reportSentId", ConvertUtils.getStringSafely(transportResults.get("reportSentId")));
				transportErrorStep.addBooleanAttribute("retry", true);
				transportErrorStep.addStringAttribute("errorId", errorId);
				transportErrorStep.addLongAttribute("transportId", transportId);

				updateReportSentStep.addStringAttribute("reportSentId", ConvertUtils.getStringSafely(transportResults.get("reportSentId")));
				updateReportSentStep.addReferenceAttribute("sentDate", transportStep, "sentDate");
				updateReportSentStep.addReferenceAttribute(TransportAttrEnum.ATTR_SENT_DETAILS.getValue(), transportStep, TransportAttrEnum.ATTR_SENT_DETAILS.getValue());
				updateReportSentStep.addBooleanAttribute("retry", true);
				updateReportSentStep.addStringAttribute("errorId", errorId);
				updateReportSentStep.addLongAttribute("transportId", transportId);
				
				Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_REATTEMPTED_FLAG_RETRY");
				try{
					DataLayerMgr.executeCall(conn, "UPDATE_REATTEMPTED_FLAG_RETRY", new Object[] { errorId});
					MessageChainService.publish(mc, publisher);
					conn.commit();
				}catch(Exception e) {
					log.error("Could not generate retry transport request publish for errorId " + errorId, e);
    				throw e;
    			} finally {
    				conn.close();
    			}
			}
		} catch(Exception e) {
			log.error("Could not generate retry transport request for errorId " + errorId, e);
		} 
	}
}
