/*
 * User.java
 *
 * Created on November 1, 2001, 5:06 PM
 */

package com.usatech.report;

/**
 *
 * @author  bkrug
 * @version
 */
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.falcon.servlet.FalconUser;
import simple.results.BeanException;
import simple.servlet.NotAuthorizedException;
import simple.text.StringUtils;

public class ReportingUser extends FalconUser {
	public static enum ResourceType {
		TERMINAL, BANK_ACCT, BANK_ACCTS, USER, USER_REPORT, ALERT_NOTIF, PROFILE, TERMINALS, PREFERENCES, TRANSPORT, CUSTOMER_SERVICE, CAMPAIGN, COLUMNMAP, PAYOR, REPORT_REGISTER, BASIC_REPORT, DEVICE_CONFIG, CUSTOMER, REGION, PREPAID_CONSUMER, PREPAID_CONSUMER_ACCT
	}

	public static enum ActionType {
		VIEW, EDIT, DELETE, USE, UNLOCK
	}

	private static final long serialVersionUID = 1236616439697722350L;
	
	protected static final Set<String> READ_ONLY_PRIVS = new HashSet<String>(Arrays.asList(new String[] { "1", "6", "11", "21", "4","22" }));
	protected boolean readOnly = false;
	protected Set<String> readOnlyPrivs;
	protected long masterUserId;
	protected String firstName;
	protected String lastName;
	protected int userType;
	protected int customerId;
	protected String customerName;
	protected String email;
	protected String licenseNbr;
	protected char licenseType = 'S';
	protected int activeBankAccountCount;
	protected final AtomicInteger pendingBankAccountCount = new AtomicInteger(0);
	protected final AtomicInteger terminalCount = new AtomicInteger(0);
	protected boolean internal = false;
	protected boolean missingBankAccount = false;
	protected int moreAcctCount = 0;
	    
	/** Creates new User */
	public ReportingUser() {
	}
	
	@Override
	public String getDisplayName() {
		String fn = getFullName();
		switch(getUserType()) {
			case 8:
				String cn = getCustomerName();
				if(!StringUtils.isBlank(cn)) {
					if(!StringUtils.isBlank(fn))
						return cn + " (" + fn + ")";
					return cn;
				}
		}
		return fn;
	}
	/** Getter for property firstName.
	 * @return Value of property firstName.
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/** Setter for property firstName.
	 * @param firstName New value of property firstName.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
		updateFullName();
	}
	
	/** Getter for property lastName.
	 * @return Value of property lastName.
	 */
	public String getLastName() {
		return lastName;
	}
	
	/** Setter for property lastName.
	 * @param lastName New value of property lastName.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
		updateFullName();
	}
	
	/** Getter for property userType.
	 * @return Value of property userType.
	 */
	public int getUserType() {
		return userType;
	}
	
	/** Setter for property userType.
	 * @param userType New value of property userType.
	 */
	public void setUserType(int userType) {
		this.userType = userType;
	}
	
	/** Getter for property customerId.
	 * @return Value of property customerId.
	 */
	public int getCustomerId() {
		return customerId;
	}
	
	/** Setter for property customerId.
	 * @param customerId New value of property customerId.
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	/** Getter for property email.
	 * @return Value of property email.
	 */
	public String getEmail() {
		return email;
	}
	
	/** Setter for property email.
	 * @param email New value of property email.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	public String getLicenseNbr() {
		return licenseNbr;
	}

	public void setLicenseNbr(String licenseNbr) {
		this.licenseNbr = licenseNbr;
	}

	public boolean isMissingBankAccount() {
		return missingBankAccount;
	}	
	
	public boolean isMissingLicense() {
		return getUserType() == 8 && getLicenseNbr() == null && getCustomerId() != 0;
	}

	public int getActiveBankAccountCount() {
		return activeBankAccountCount;
	}

	public void setActiveBankAccountCount(int activeBankAccountCount) {
		this.activeBankAccountCount = activeBankAccountCount;
	}

	public int getPendingBankAccountCount() {
		return pendingBankAccountCount.get();
	}

	public void setPendingBankAccountCount(int pendingBankAccountCount) {
		this.pendingBankAccountCount.set(pendingBankAccountCount);
	}

	public int getTerminalCount() {
		return terminalCount.get();
	}

	public void setTerminalCount(int terminalCount) {
		this.terminalCount.set(terminalCount);
	}
	
	public long getMasterUserId() {
		return masterUserId;
	}

	public void setMasterUserId(long masterUserId) {
		this.masterUserId = masterUserId;
	}

	public void incrementPendingBankAccountCount() {
		pendingBankAccountCount.incrementAndGet();
	}

	public void incrementTerminalCount() {
		terminalCount.incrementAndGet();
	}

	public void checkPermission(ReportingUser.ResourceType resource, ReportingUser.ActionType action, Object resourceId) throws ServletException {
		try {
			if(action == null)
				action = ReportingUser.ActionType.VIEW;
			if(action != ReportingUser.ActionType.VIEW && isReadOnly())
				throw new NotAuthorizedException("User '" + getUserName() + "' is in read-only mode");
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("checkUser", this);
			params.put("action", action);
			String callId;
			switch(resource) {
				case ALERT_NOTIF:
					if(action == ReportingUser.ActionType.VIEW && (resourceId == null || ConvertUtils.getLong(resourceId) == 0))
						return;
					params.put("alertNotifId", resourceId);
					callId = "CHECK_ALERT_NOTIF_PERMISSION";
					break;
				case USER_REPORT:
					if(action == ReportingUser.ActionType.VIEW && (resourceId == null || ConvertUtils.getLong(resourceId) == 0))
						return;
					params.put("userReportId", resourceId);
					callId = "CHECK_USER_REPORT_PERMISSION";
					break;
				case TERMINAL:
					params.put("terminalId", resourceId);
					callId = "CHECK_TERMINAL_PERMISSION";
					break;
				case TERMINALS:
					params.put("terminalIds", resourceId);
					callId = "CHECK_TERMINALS_PERMISSION";
					break;
				case BANK_ACCT:
					params.put("bankAcctId", resourceId);
					callId = "CHECK_BANK_ACCT_PERMISSION";
					break;
				case BANK_ACCTS:
					params.put("bankAcctIds", resourceId);
					callId = "CHECK_BANK_ACCTS_PERMISSION";
					break;
				case USER:
					params.put("userId", resourceId);
					callId = "CHECK_USER_PERMISSION";
					break;
				case PROFILE:
					params.put("userId", resourceId);
					params.remove("checkUser");
					params.put("checkUser.userId", getMasterUserId());
					callId = "CHECK_USER_PERMISSION";
					break;
				case PREFERENCES:
					return; // if read-only and action = EDIT then alright threw exception
				case TRANSPORT:
					params.put("transportId", resourceId);
					callId = "CHECK_TRANSPORT_PERMISSION";
					break;
				case CUSTOMER_SERVICE:
					callId = "CHECK_CUSTOMER_SERVICE_PERMISSION";
					break;
				case CAMPAIGN:
					params.put("campaignId", resourceId);
					callId = "CHECK_CAMPAIGN_PERMISSION";
					break;
				case COLUMNMAP:
					params.put("columnMapId", resourceId);
					if(action == ReportingUser.ActionType.VIEW){
						callId = "CHECK_VIEW_COLUMN_MAP";
					}else {
						callId = "CHECK_EDIT_COLUMN_MAP";
					}
					break;
				case PAYOR:
					params.put("payorId", resourceId);
					callId = "CHECK_PAYOR_PERMISSION";
					break;
				case REPORT_REGISTER:
					callId = "CHECK_REPORT_REGISTER_PERMISSION";
					break;
				case BASIC_REPORT:
					params.put("basicReportId", resourceId);
					callId = "CHECK_BASIC_REPORT_PERMISSION";
					break;
				case DEVICE_CONFIG:
					params.put("terminalId", resourceId);
					callId = "CHECK_DEVICE_CONFIG_PERMISSION";
					break;
				case REGION:
					params.put("regionId", resourceId);
					callId = "CHECK_REGION_PERMISSION";
					break;
				case PREPAID_CONSUMER: // PREPAID is a misnomer, this applies to consumers in general
					params.put("consumerId", resourceId);
					callId = "CHECK_CONSUMER_PERMISSION";
					break;
				case PREPAID_CONSUMER_ACCT:  // PREPAID is a misnomer, this applies to consumer accounts in general
					params.put("consumerAcctId", resourceId);
					if(action == ReportingUser.ActionType.VIEW){
						callId = "CHECK_CONSUMER_ACCT_PERMISSION_VIEW";
					}else{
						callId = "CHECK_CONSUMER_ACCT_PERMISSION_EDIT";
					}
					break;
				case CUSTOMER:
					params.put("customerId", resourceId);
					callId = "CHECK_CUSTOMER_PERMISSION";
					break;
				default:
					throw new ServletException("Resource " + resource + " could not be checked");
			}
			DataLayerMgr.selectInto(callId, params);
			if(!ConvertUtils.getBoolean(params.get("permitted"), false))
				throw new NotAuthorizedException("User '" + getUserName() + "' is not permitted to " + action.toString().toLowerCase() + " this");
		} catch(NotEnoughRowsException e) {
			throw new NotAuthorizedException("User '" + getUserName() + "' is not permitted to " + action.toString().toLowerCase() + " this");
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(BeanException e) {
			throw new ServletException(e);
		}
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Set<String> getPrivileges() {
		if(isReadOnly()) {
			if(readOnlyPrivs == null) {
				Set<String> tmp = new HashSet<String>(unmodPrivs);
				tmp.retainAll(READ_ONLY_PRIVS);
				readOnlyPrivs = Collections.unmodifiableSet(tmp);
			}
			return readOnlyPrivs;
		}
		return unmodPrivs;
	}

	public boolean hasPrivilege(ReportingPrivilege privilege) {
		return hasPrivilege(String.valueOf(privilege.getValue()));
	}
	@Override
	public boolean hasPrivilege(String privilege) {
		if(isReadOnly() && !READ_ONLY_PRIVS.contains(privilege))
			return false;
		return super.hasPrivilege(privilege);
	}

	protected void updateFullName() {
		String firstName, lastName;
		if((firstName = getFirstName()) == null || (firstName = firstName.trim()).length() == 0)
			setFullName((lastName = getLastName()) == null || (lastName = lastName.trim()).length() == 0 ? null : lastName);
		else if((lastName = getLastName()) == null || (lastName = lastName.trim()).length() == 0)
			setFullName(firstName);
		else
			setFullName(firstName + ' ' + lastName);
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public boolean isInternal() {
		return internal;
	}

	public void setInternal(boolean internal) {
		this.internal = internal;
	}

	public void setMissingBankAccount(boolean missingBankAccount) {
		this.missingBankAccount = missingBankAccount;
	}

	public boolean isCustomerService() {
		return isInternal() && hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE);
	}
	
	public boolean isPartner() {
		return hasPrivilege(ReportingPrivilege.PRIV_PARTNER) && !hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE);
	}
	
	public char getLicenseType() {
		return licenseType;
	}
	
	public void setLicenseType(char licenseType) {
		//if (licenseType != null)
			this.licenseType = licenseType;
	}
	
	public boolean isReseller() {
		return licenseType == 'R';
	}

	public int getMoreAcctCount() {
		return moreAcctCount;
	}

	public void setMoreAcctCount(int moreAcctCount) {
		this.moreAcctCount = moreAcctCount;
	}
}