/*
 * (C) USA Technologies 2011
 */
package com.usatech.report;

import com.usatech.app.Attribute;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

/**
 * <p>
 * Used by {@link com.usatech.report.AbstractGenerateTask}.
 * </p>
 * <p>
 * Attributes used by Report Generation should no longer be constant
 * strings, but should be placed in this
 * Enumeration.
 * </p>
 * 
 * @author phorsfield, bkrug
 */
public enum GenerateAttrEnum implements Attribute {
	ATTR_REPORT_SENT_ID("reportSentId", "The REPORT.REPORT_SENT.REPORT_SENT_ID"),
	ATTR_GEN_REQUEST_ID("request.requestId", "Long; The primary key identifying this report request"),
	ATTR_USE_STANDBY("report.useStandby", "Whether the standby database can be used"),
	ATTR_MAX_STANDBY_WAIT("report.maxStandbyWait", "The maximun number of milliseconds to wait for the request to propagate to the standby database"),
	ATTR_GENERATOR("request.generatorId", "The generator id of the report request"),
	;
	private final String value;
	private final String description;

	protected final static EnumStringValueLookup<GenerateAttrEnum> lookup = new EnumStringValueLookup<GenerateAttrEnum>(
			GenerateAttrEnum.class);

	public static GenerateAttrEnum getByValue(String value)
			throws InvalidValueException {
		return lookup.getByValue(value);
	}

	/**
	 * Internal constructor.
	 * 
	 * @param value
	 */
	private GenerateAttrEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * Get the intrinsic value of the type.
	 * 
	 * @return a char
	 */
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
}