package com.usatech.report;

import java.util.Calendar;
import java.util.Date;

public interface Frequency {
	public class DateRange {
		public Date beginDate;
		public Date endDate;
		public final Calendar calendar = Calendar.getInstance();
		
		public String toString() {
			return beginDate + " - " + endDate;
		}
	}
	
	public DateRange nextDateRange(Date prevDate);
	public DateRange nextDateRange(DateRange prevDateRange);
}
