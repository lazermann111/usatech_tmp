package com.usatech.report;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.activation.DataSource;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

public class GenerateCustomReportTask extends AbstractRetrieveFileTask {
	private static final Log log = Log.getLog();

	public GenerateCustomReportTask() {
		super(Generator.CUSTOM);
	}

	protected GenerateCustomReportTask(Generator generator) {
		super(generator);
	}
	
	@Override
	protected DataSource generateDataSource(Map<String, Object> params, StandbyAllowance standbyAllowance) throws Exception {
		return generateDBBlobFile(params, standbyAllowance);
	}

	protected DataSource generateDBBlobFile(Map<String, Object> params, StandbyAllowance standbyAllowance) throws SQLException, DataLayerException, ConvertException, IOException, Exception {
		String callId=ConvertUtils.getStringSafely(params.get("callId"));
		try (Connection conn = getReportConnection(standbyAllowance)) {
			conn.setAutoCommit(false);
			try {
				DataLayerMgr.executeUpdate(conn, callId, params);
				conn.commit();
			} catch(Exception e) {
				try {
					conn.rollback();
				} catch(SQLException e1) {
					log.warn("Could not rollback");
				}
				throw e;
			}
		}
		
		final String fileName=ConvertUtils.getStringSafely(params.get("fileName"));
		final String contentType=ConvertUtils.getStringSafely(params.get("contentType"));
		Object content = params.get("content");
		final Callable<InputStream> getter;
		if(ConvertUtils.getInt(params.get("exists"))>0){
			log.debug("Generate custom report:"+fileName);
		}else{
			log.debug("Retrieve custom report:"+fileName);
		}
		if(content == null) {
			throw new Exception("DB callId:"+callId+" with params:"+params+" returns no content for custom report.");
		} else if(content instanceof Blob) {
			final Blob blob = (Blob) content;
			getter = new Callable<InputStream>() {
				public InputStream call() throws Exception {
					return blob.getBinaryStream();
				}
			};
		} else {
			final byte[] data = ConvertUtils.convert(byte[].class, content);
			getter = new Callable<InputStream>() {
				public InputStream call() throws Exception {
					return new ByteArrayInputStream(data);
				}
			};
		}
		return new DataSource() {
			public String getContentType() {
				return contentType;
			}

			public InputStream getInputStream() throws IOException {
				try {
					return getter.call();
				} catch(Exception e) {
					if(e instanceof IOException)
						throw (IOException) e;
					IOException ioe = new IOException(e.getMessage());
					ioe.initCause(e);
					throw ioe;
				}
			}

			public String getName() {
				return fileName;
			}
			public OutputStream getOutputStream() throws IOException {
				return null;
			}
		};
	}

}
