package com.usatech.report;

import java.lang.management.ManagementFactory;
import java.text.Format;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.TransportAttrEnum;
import com.usatech.transport.TransportReason;

import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;

/**
 * Finds outbound Data Exchange Items that need to be processed and sends them off to do so
 * 
 * @author bkrug
 * 
 */
public class DataExchangeKickoffTask implements SelfProcessor {
	private static final Log log = Log.getLog();
	protected static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));
	protected static final String PROCESS_CD = "OUTBOUND_DATA_EXCHANGE";
	protected static final Format FILE_NAME_DATE_FORMAT = ConvertUtils.getFormat("DATE", "yyyyMMdd_HHmmss");
	protected long lockUpdateInterval = 900000L;
	protected long pollInterval = 15000L;
	protected int maxItems = 1000;
	protected float processingThresholdDays = 0.25f;
	protected int dataExchangeType = 1;
	protected Publisher<ByteInput> publisher;
	
	@Override
	public void process() {
		long startTs = System.currentTimeMillis();
		long totalItemCount = 0;
		final Map<String, Object> lockParams = new HashMap<String, Object>();
		final AtomicBoolean locked = new AtomicBoolean(true);
		try {
			if(!InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams)) {
				log.info("Outbound Data Exchange is already locked by instance " + lockParams.get("lockedBy"));
				return;
			}
			final Timer timer = new Timer(getClass().getName() + "_Timer");
			try {
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						try {
							if(locked.get() && !InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams)) {
								log.info("Outbound Data Exchange is already locked by instance " + lockParams.get("lockedBy"));
								locked.set(false);
								timer.cancel();
							}
						} catch(Throwable e) {
							log.warn("Error while renewing lock on Outbound Data Exchange", e);
							locked.set(false);
							timer.cancel();
						}
					}
				}, getLockUpdateInterval(), getLockUpdateInterval());
				final Map<String, Object> params = new HashMap<String, Object>();
				params.put("maxItems", getMaxItems());
				params.put("processingThresholdDays", getProcessingThresholdDays());
				params.put("dataExchangeType", getDataExchangeType());
				long prevTime = 0L;
				while(locked.get()) {
					DataLayerMgr.executeCall("NEXT_DATA_EXCHANGE_ITEMS", params, true);
					long[] itemIds = ConvertUtils.convert(long[].class, params.get("itemIds"));
					if(itemIds != null && itemIds.length > 0) {
						String fileNamePrefix = ConvertUtils.convert(String.class, params.get("fileNamePrefix"));
						Long transportId = ConvertUtils.getLong(params.get("transportId"));
						int transportTypeId = ConvertUtils.getInt(params.get("transportTypeId"));
						char consolidateFlag = ConvertUtils.getChar(params.get("consolidateFlag"));
						long time = System.currentTimeMillis();
						time = time - (time % 1000);
						if(time <= prevTime)
							time = prevTime + 1000L;
						prevTime = time;
						String fileName = fileNamePrefix + FILE_NAME_DATE_FORMAT.format(new Date(time)) + ".csv";
						MessageChain mc = new MessageChainV11();
						// generate content
						// insert data_exchange_file and update items
						MessageChainStep generateStep = mc.addStep("usat.data.exchange.generate");
						generateStep.setAttribute(CommonAttrEnum.ATTR_FILE_PATH, fileName);
						generateStep.setAttribute(CommonAttrEnum.ATTR_ITEM_ID, itemIds);
						generateStep.setAttribute(CommonAttrEnum.ATTR_FILE_TYPE, getDataExchangeType());
						generateStep.setAttribute(CommonAttrEnum.ATTR_SKIP_EMPTY, (consolidateFlag == 'A'));

						// send via transport
						MessageChainStep transportStep = ReportRequestFactory.createTransportStep(mc, transportId, transportTypeId, null);
						transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_ID, generateStep, CommonAttrEnum.ATTR_FILE_ID);
						transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_KEY, generateStep, TransportAttrEnum.ATTR_FILE_KEY);
						transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_NAME, generateStep, TransportAttrEnum.ATTR_FILE_NAME);
						transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_CONTENT_TYPE, generateStep, TransportAttrEnum.ATTR_FILE_CONTENT_TYPE);
						transportStep.setReferenceAttribute(TransportAttrEnum.ATTR_FILE_PASSCODE, generateStep, TransportAttrEnum.ATTR_FILE_PASSCODE);
						transportStep.setAttribute(TransportAttrEnum.ATTR_TRANSPORT_REASON, TransportReason.CONDITION);

						// update transport details
						MessageChainStep updateStep = mc.addStep("usat.data.exchange.sent");
						updateStep.setReferenceAttribute(CommonAttrEnum.ATTR_FILE_ID, generateStep, CommonAttrEnum.ATTR_FILE_ID);
						updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_SENT_SUCCESS, transportStep, TransportAttrEnum.ATTR_SENT_SUCCESS);
						updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_SENT_DETAILS, transportStep, TransportAttrEnum.ATTR_SENT_DETAILS);
						updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_SENT_DATE, transportStep, TransportAttrEnum.ATTR_SENT_DATE);
						updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_ERROR_TEXT, transportStep, TransportAttrEnum.ATTR_ERROR_TEXT);
						updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_ERROR_TYPE, transportStep, TransportAttrEnum.ATTR_ERROR_TYPE);
						updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_ERROR_TIME, transportStep, TransportAttrEnum.ATTR_ERROR_TIME);
						updateStep.setReferenceAttribute(TransportAttrEnum.ATTR_ERROR_RETRY_PROPS, transportStep, TransportAttrEnum.ATTR_ERROR_RETRY_PROPS);

						generateStep.setNextSteps(0, transportStep);
						transportStep.setNextSteps(0, updateStep);
						transportStep.setNextSteps(1, updateStep);
						MessageChainService.publish(mc, getPublisher());
						totalItemCount += itemIds.length;
						// do again without delay
					} else {
						long delay = getPollInterval();
						if(delay > 0)
							Thread.sleep(delay);
					}
				}
			} finally {
				timer.cancel();
			}
		} catch(Exception e) {
			log.error("Error processing Outbound Data Exchange", e);
		} finally {
			try {
				locked.set(false);
				InteractionUtils.unlockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams);
			} catch(Exception e) {
				log.error("Error unlocking " + PROCESS_CD, e);
			}
			if(totalItemCount > 0)
				log.info("Processed " + totalItemCount + " Outbound Data Exchange items in " + (System.currentTimeMillis() - startTs) + " ms");
		}
	}

	public long getLockUpdateInterval() {
		return lockUpdateInterval;
	}

	public void setLockUpdateInterval(long lockUpdateInterval) {
		this.lockUpdateInterval = lockUpdateInterval;
	}

	public long getPollInterval() {
		return pollInterval;
	}

	public void setPollInterval(long pollInterval) {
		this.pollInterval = pollInterval;
	}

	public int getMaxItems() {
		return maxItems;
	}

	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}

	public float getProcessingThresholdDays() {
		return processingThresholdDays;
	}

	public void setProcessingThresholdDays(float processingThresholdDays) {
		this.processingThresholdDays = processingThresholdDays;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public int getDataExchangeType() {
		return dataExchangeType;
	}

	public void setDataExchangeType(int dataExchangeType) {
		this.dataExchangeType = dataExchangeType;
	}

}
