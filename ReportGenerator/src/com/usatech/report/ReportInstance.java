package com.usatech.report;

public class ReportInstance {
	protected int userReportId;
	protected int userId;
	protected java.sql.Timestamp updateDate;
	protected int reportId;
	protected int generatorId;
	protected String title;
	protected String name;

	public int getGeneratorId() {
		return generatorId;
	}

	public void setGeneratorId(int generatorId) {
		this.generatorId = generatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public java.sql.Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(java.sql.Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserReportId() {
		return userReportId;
	}

	public void setUserReportId(int userReportId) {
		this.userReportId = userReportId;
	}
}