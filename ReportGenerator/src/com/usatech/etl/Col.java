package com.usatech.etl;

import java.sql.Connection;
import java.sql.SQLException;

import org.xml.sax.SAXException;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.Call;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.Parameter;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;


class Col implements Pnt {
	public final Tab tab;
	protected Tab refTab;
	public final ColType colType;
	public final String name;
	public final String source;
	public final SQLType sqlType;
	public final boolean required;
	public final String defaultSql;
	protected Object defaultValue;
	public final Object deleteValue;
	protected final ObjectBuilder colBuilder;
	protected boolean autoGen;
	protected String precalcExpression;

	protected Col(Tab tab, String name, String source, ColType colType, SQLType sqlType, boolean required, String defaultSql, String deleteLiteral, ObjectBuilder colBuilder) {
		this(tab, name, source, colType, sqlType, required, defaultSql, (deleteLiteral == null ? null : ConvertUtils.convertSafely((Class<? super Object>) SQLTypeUtils.getJavaType(sqlType), deleteLiteral, deleteLiteral)), colBuilder);
	}

	public Col(Tab tab, ObjectBuilder colBuilder) throws ConvertException {
		this(tab, colBuilder.getAttrValue("name"), colBuilder.getAttrValue("source"),
				ConvertUtils.convertRequired(ColType.class, colBuilder.getAttrValue("coltype")), 
				ConvertUtils.convertRequired(SQLType.class, colBuilder.getAttrValue("sqltype")), 
				ConvertUtils.getBoolean(colBuilder.getAttrValue("required"), false), colBuilder.getAttrValue("default"), 
				colBuilder.getAttrValue("deleteValue"), colBuilder);
		setAutoGen(ConvertUtils.getBoolean(colBuilder.getAttrValue("autoGen"), false));
		setPrecalcExpression(colBuilder.getAttrValue("precalc"));
	}

	protected Col(Tab tab, String name, String source, ColType colType, SQLType sqlType, boolean required, String defaultSql, Object deleteValue, ObjectBuilder colBuilder) {
		super();
		this.tab = tab;
		this.colType = colType;
		this.name = name;
		this.source = source;
		this.sqlType = sqlType;
		this.required = required;
		this.defaultSql = defaultSql;
		this.deleteValue = deleteValue;
		this.colBuilder = colBuilder;
		tab.addCol(this);
	}

	protected Col(Col template, ColType colType) {
		this.tab = template.tab;
		this.colType = colType;
		this.name = template.name;
		this.source = template.source;
		this.sqlType = template.sqlType;
		this.required = template.required;
		this.defaultSql = template.defaultSql;
		this.deleteValue = template.deleteValue;
		this.colBuilder = template.colBuilder;
		setAutoGen(template.isAutoGen());
		setPrecalcExpression(template.getPrecalcExpression());
		setRefTab(template.getRefTab());
	}

	public Col asInherited() {
		switch(colType) {
			case PRIMARY_KEY:
			case IDENTITY_KEY:
				return new Col(this, ColType.DATA);
			default:
				return this;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Col))
			return false;
		Col other = (Col)obj;
		return name.equals(other.name)
				&& colType == other.colType
				&& ConvertUtils.areEqual(source, other.source)
				&& ConvertUtils.areEqual(sqlType, other.sqlType)
				&& required == other.required
				&& ConvertUtils.areEqual(defaultSql, other.defaultSql)
				&& ConvertUtils.areEqual(defaultValue, other.defaultValue)
				&& tab.name.equals(other.tab.name)
				&& ConvertUtils.areEqual(refTab == null ? null : refTab.name, other.refTab == null ? null : other.refTab.name)
				&& autoGen == other.autoGen
				&& ConvertUtils.areEqual(precalcExpression, other.precalcExpression);
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	public void writeXml(XMLBuilder xmlBuilder) throws SAXException {
		if(colBuilder != null && colBuilder.getXmlExtraNodes() != null)
			colBuilder.getXmlExtraNodes().writeExtraNodes(colBuilder, xmlBuilder.getContentHandler());
		xmlBuilder.attributeForNext("name", name);
		if(!StringUtils.isBlank(source))
			xmlBuilder.attributeForNext("source", source);
		xmlBuilder.attributeForNext("coltype", ConvertUtils.getStringSafely(colType));
		xmlBuilder.attributeForNext("sqltype", ConvertUtils.getStringSafely(sqlType));
		if(required)
			xmlBuilder.attributeForNext("required", "true");
		if(!StringUtils.isBlank(defaultSql))
			xmlBuilder.attributeForNext("default", defaultSql);
		if(deleteValue != null)
			xmlBuilder.attributeForNext("deleteValue", ConvertUtils.getStringSafely(deleteValue));
		if(refTab != null)
			xmlBuilder.attributeForNext("ref", refTab.name);
		if(autoGen)
			xmlBuilder.attributeForNext("autoGen", "true");
		if(!StringUtils.isBlank(precalcExpression))
			xmlBuilder.attributeForNext("precalc", precalcExpression);

		xmlBuilder.element("col", colBuilder != null ? colBuilder.getAttributeNames() : null);
	}

	public Col copyTo(Tab newTab) {
		Col newCol = new Col(newTab, name, source, colType, sqlType, required, defaultSql, deleteValue, colBuilder);
		if(defaultValue != null)
			newCol.defaultValue = defaultValue;
		newCol.setAutoGen(isAutoGen());
		newCol.setPrecalcExpression(getPrecalcExpression());
		newCol.setRefTab(refTab);
		return newCol;
	}

	public Object getDefaultValue(DataLoader dataLoader) throws ServiceException {
		if(defaultValue == null && !StringUtils.isBlank(defaultSql) && !DataLoader.changingDefaultPattern.matcher(defaultSql).matches()) {
			StringBuilder sb = new StringBuilder();
			sb.append("{? = call ").append(defaultSql).append('}');
			Parameter updateCountParameter = new Parameter();
			updateCountParameter.setIn(false);
			updateCountParameter.setOut(true);
			Parameter defaultValueParameter = new Parameter();
			defaultValueParameter.setPropertyName("defaultValue");
			defaultValueParameter.setSqlType(sqlType);
			defaultValueParameter.setOut(true);
			defaultValueParameter.setIn(false);
			Call call = new Call();
			call.setSql(sb.toString());
			call.setArguments(new Argument[] { updateCountParameter, defaultValueParameter });
			call.setLogExecuteTimeThreshhold(dataLoader.getDefaultValueQueryTimeout() / 4);
			call.setQueryTimeout(dataLoader.getDefaultValueQueryTimeout());
			try {
				Connection conn = DataLayerMgr.getConnection(dataLoader.getTargetDsn());
				try {
					Object[] ret = call.executeCall(conn, null, null);
					defaultValue = ret[1];
				} finally {
					conn.close();
				}
			} catch(SQLException e) {
				throw new ServiceException("Could not get default value for column '" + name + "' in table '" + tab.name + "' with sql '" + defaultSql + "'", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not get default value for column '" + name + "' in table '" + tab.name + "' with sql '" + defaultSql + "'", e);
			}
		}
		return defaultValue;
	}

	@Override
	public String toString() {
		return name;
	}

	public Tab getRefTab() {
		return refTab;
	}

	public void setRefTab(Tab refTab) {
		if(this.refTab == refTab)
			return;
		if(this.refTab != null)
			this.refTab.referees.remove(this);
		this.refTab = refTab;
		refTab.referees.add(this);
	}

	public boolean isAutoGen() {
		return autoGen;
	}

	public void setAutoGen(boolean autoGen) {
		this.autoGen = autoGen;
	}

	public String getPrecalcExpression() {
		return precalcExpression;
	}

	public void setPrecalcExpression(String precalcExpression) {
		this.precalcExpression = precalcExpression;
	}

	public String getName() {
		return name;
	}

	public SQLType getSQLType() {
		return sqlType;
	}

	public boolean isRequired() {
		return required;
	}

	public boolean isRef() {
		return false;
	}

	public Col getRefCol() {
		return refTab == null ? null : refTab.getFirstPrimaryKeyCol();
	}

	@Override
	public boolean isInput() {
		return true;
	}

	@Override
	public boolean isOutput() {
		return true;
	}

	@Override
	public Pnt getLocalPnt() {
		return this;
	}
}