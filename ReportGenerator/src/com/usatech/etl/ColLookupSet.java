package com.usatech.etl;

import java.util.Map;
import java.util.Set;

import simple.util.ConversionSet;

public class ColLookupSet extends ConversionSet<String, Col> {
	protected final Map<String, Col> colsByName;

	public ColLookupSet(Set<String> names, Map<String, Col> colsByName) {
		super(names);
		this.colsByName = colsByName;
	}

	@Override
	protected String convertTo(Col object1) {
		return object1 == null ? null : object1.name;
	}

	@Override
	protected Col convertFrom(String object0) {
		return colsByName.get(object0);
	}

	@Override
	protected boolean isReversible() {
		return true;
	}
}
