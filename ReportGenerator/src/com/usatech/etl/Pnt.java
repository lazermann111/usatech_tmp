package com.usatech.etl;

import simple.sql.SQLType;

public interface Pnt {
	public String getName();

	public SQLType getSQLType();

	public boolean isRequired();

	public boolean isRef();

	public Col getRefCol();

	public boolean isInput();

	public boolean isOutput();

	public Pnt getLocalPnt();

}
