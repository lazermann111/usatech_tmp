package com.usatech.etl;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import simple.app.DatabasePrerequisite;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.RetrySpecifiedServiceException;
import simple.app.SelfProcessor;
import simple.app.Work;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.ConnectionGroup;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.PlainConnectionGroup;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;
import simple.util.LRUMap;
import simple.util.MapBackedSet;

import com.usatech.layers.common.constants.DataLoaderAction;

public class DataLoaderDataQueue extends QueryLoadingWorkQueue {
	private static final Log log = Log.getLog();

	protected DataLoader dataLoader;
	protected int dataValueCount = 5;
	protected long nonblockingRetryDelay = 120000L;
	protected long blockingRetryDelay = 60000L;
	protected long nonblockingRetryCount = 40;
	protected int appInstance;
	protected int expireSeconds = 120;
	protected long cleanupInterval = -1;
	protected final AtomicLong lastCleanup = new AtomicLong();

	protected class RenewerThread extends Thread {
		public RenewerThread() {
			super(getQueueKey() + "Renewer");
		}

		public void run() {
			renewLoaderLoop();
		}
	}

	protected final Set<Long> pendingIds = new MapBackedSet<Long>(new ConcurrentHashMap<Long, Object>());
	protected final Set<Long> pendingIdsUnmod = Collections.unmodifiableSet(pendingIds);
	protected final LRUMap<DataUpdateTask, DataUpdateTask> recentlyProcessing = new LRUMap<DataUpdateTask, DataUpdateTask>(1024);
	
	public char getCleanupBeforeLoad() {
		long ci = getCleanupInterval();
		if(ci < 0)
			return 'N';
		long time = System.currentTimeMillis();
		while(true) {
			long last = lastCleanup.get();
			long target = last + ci;
			if(time <= target)
				return 'N';
			if(lastCleanup.compareAndSet(last, time))
				return 'Y';
		}
	}

	@Override
	protected Work<SelfProcessor> createTask(Results results) throws ConvertException {
		long dataUpdateId = results.getValue("id", Long.class);
		String table = results.getValue("table", String.class);
		String source = results.getValue("source", String.class);
		Map<String, Object> sourceValues = new HashMap<String, Object>(getDataValueCount());
		for(int i = 1; i <= getDataValueCount(); i++) {
			String label = results.getValue("label." + i, String.class);
			if(!StringUtils.isBlank(label))
				sourceValues.put(label, results.getValue("value." + i));
		}
		long createTime = results.getValue("createTime", Long.class);
		int retryCount = results.getValue("retryCount", int.class);
		DataLoaderAction action = results.getValue("action", DataLoaderAction.class);
		if(action == null)
			action = DataLoaderAction.UPSERT;
		DataUpdateTask dut = new DataUpdateTask(dataUpdateId, action, table, source, sourceValues, createTime, retryCount);
		// assume single thread access of recentlyProcessing
		DataUpdateTask existing = recentlyProcessing.get(dut);
		if(existing != null && (existing.getProcessedTime() == 0 || createTime <= existing.getProcessedTime())) {
			dut.remove();
			log.info("A data update similiar to " + dut.getDataUpdateId() + " was recently processed; ignoring");
			return null;
		}
		recentlyProcessing.put(dut, dut);
		return dut;
	}

	public void renewLoaderLoop() {
		while(!shutdown.get()) {
			try {
				DataLayerMgr.executeCall("RENEW_EXPIRE_TIME", this, true);
			} catch(SQLException e) {
				log.warn("Could not renew expire time", e);
			} catch(DataLayerException e) {
				log.warn("Could not renew expire time", e);
			} catch(RuntimeException e) {
				log.warn("Could not renew expire time", e);
			} catch(Error e) {
				log.warn("Could not renew expire time", e);
			}
			try {
				Thread.sleep(Math.max(1000L, getExpireSeconds() * 500L));
			} catch(InterruptedException e) {
				// ignore
			}
		}
	}

	protected class DataUpdateTask implements Work<SelfProcessor>, SelfProcessor {
		protected final AtomicBoolean complete = new AtomicBoolean();
		protected final Map<String, Object> sourceValues;
		protected final String table;
		protected final String source;
		protected final long dataUpdateId;
		protected final long createTime;
		protected final int retryCount;
		protected final DataLoaderAction action;
		protected String errorText;
		protected final int hash;
		protected long processedTime;

		public DataUpdateTask(long dataUpdateId, DataLoaderAction action, String table, String source, Map<String, Object> sourceValues, long createTime, int retryCount) {
			this.dataUpdateId = dataUpdateId;
			this.sourceValues = sourceValues;
			this.table = table;
			this.source = source;
			this.createTime = createTime;
			this.retryCount = retryCount;
			this.hash = sourceValues.hashCode() + table.hashCode() * 31;
			this.action = action;
			log.info("Creating DataUpdateTask " + dataUpdateId);
			pendingIds.add(dataUpdateId);
		}

		@Override
		public int hashCode() {
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof DataUpdateTask) || hash != obj.hashCode())
				return false;
			DataUpdateTask dut = (DataUpdateTask) obj;
			return ConvertUtils.areEqual(table, dut.table) && ConvertUtils.areEqual(action, dut.action) && ConvertUtils.areEqual(source, dut.source) && ConvertUtils.areEqual(sourceValues, dut.sourceValues);
		}

		public String getGuid() {
			return String.valueOf(dataUpdateId);
		}

		@Override
		public long getEnqueueTime() {
			return createTime;
		}

		@Override
		public SelfProcessor getBody() {
			return this;
		}

		@Override
		public void workComplete() throws WorkQueueException {
			/* right now we assume the task does this
			if(complete.compareAndSet(false, true)) {
				
			} //*/
		}

		@Override
		public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
			/* right now we assume the task does this
			if(complete.compareAndSet(false, true)) {
				
			} //*/
		}

		@Override
		public Publisher<SelfProcessor> getPublisher() throws WorkQueueException {
			return null;
		}

		@Override
		public boolean isComplete() {
			return complete.get();
		}

		@Override
		public boolean isRedelivered() {
			return retryCount > 0;
		}

		@Override
		public boolean isUnguardedRetryAllowed() {
			return true;
		}

		@Override
		public int getRetryCount() {
			return retryCount;
		}

		@Override
		public String getQueueName() {
			return getQueueKey();
		}

		@Override
		public String getOriginalQueueName() {
			return getQueueKey();
		}

		@Override
		public Map<String, ?> getWorkProperties() {
			return null;
		}

		@Override
		public QoS getQoS() {
			return null;
		}

		public void unblock(String... blocksToClear) {
		}

		public String getSubscription() {
			return null;
		}
		public void process() {
			log.info("Start processing DataUpdateTask " + dataUpdateId);
			try {
				long time;
				RETRY: while(true) {
					time = System.currentTimeMillis();
					try {
						ConnectionGroup connGroup = new PlainConnectionGroup(DataLayerMgr.getDataSourceFactory());
						boolean okay = false;
						try {
							switch(action) {
								case UPSERT:
									dataLoader.loadRow(table, source, sourceValues, connGroup);
									break;
								case PARTIAL:
									final Set<String> targetKeys = new HashSet<String>();
									final Map<String, Object> sourceKeyValues = new HashMap<String, Object>();
									for(Map.Entry<String, Object> entry : sourceValues.entrySet()) {
										if(StringUtils.isBlank(entry.getKey()))
											continue;
										if(entry.getKey().charAt(0) == '@')
											targetKeys.add(entry.getKey().substring(1));
										else
											sourceKeyValues.put(entry.getKey(), entry.getValue());
									}
									// NOTE: updateValues should contain revision
									dataLoader.partialUpdate(table, source, sourceKeyValues, targetKeys, connGroup);
									break;
								case REPOPULATE:
									dataLoader.repopulateColumns(table, source, sourceValues.keySet(), connGroup);
									break;
								default:
									throw new RetrySpecifiedServiceException("Unsupported action " + action, WorkRetryType.NO_RETRY);
							}
							connGroup.commit();
							okay = true;
							processedTime = time;
						} finally {
							connGroup.close(!okay);
						}
						break RETRY; // success - move on
					} catch(Exception e) {
						recentlyProcessing.remove(this); // so that we re-try
						switch(handleError(e)) {
							case BLOCKING_RETRY:
								long delay = getBlockingRetryDelay();
								if(delay > 0) {
									try {
										Thread.sleep(delay);
									} catch(InterruptedException e1) {
										// ignore
									}
								}
								continue RETRY;
							case IMMEDIATE_RETRY:
								continue RETRY;
							case NONBLOCKING_RETRY:
							case SCHEDULED_RETRY:
								nonblockingRetry(e);
								return;
							case NO_RETRY:
								noRetry(e);
								return;
						}
					}
				}
				StringBuilder sb = new StringBuilder("Update of ").append(table).append(" with values: ").append(sourceValues).append(" took ").append(System.currentTimeMillis() - time).append(" ms");
				log.info(sb.toString());
				DataLayerMgr.executeCall("DELETE_DATA_UPDATE", this, true);
			} catch(Exception e) {
				StringBuilder sb = new StringBuilder("Could not process update of ").append(table).append(" with values: ").append(sourceValues);
				log.error(sb.toString(), e);			
			} finally {
				complete.set(true);
				pendingIds.remove(dataUpdateId);
				log.info("Processing complete for DataUpdateTask " + dataUpdateId);
			}
		}

		public void remove() {
			try {
				DataLayerMgr.executeCall("DELETE_DATA_UPDATE", this, true);
			} catch(Exception e) {
				StringBuilder sb = new StringBuilder("Could not remove data update ").append(dataUpdateId).append(" from the database");
				log.error(sb.toString(), e);
			} finally {
				complete.set(true);
				pendingIds.remove(dataUpdateId);
			}
		}

		public long getDataUpdateId() {
			return dataUpdateId;
		}

		protected WorkRetryType handleError(Throwable e) {
			StringBuilder sb = new StringBuilder("Failed to process update of ").append(table).append(" with values: ").append(sourceValues);
			WorkRetryType retryType;
			if(e instanceof SQLException)
				retryType = DatabasePrerequisite.determineRetryType((SQLException) e);
			else if(e instanceof DataLayerException)
				retryType = WorkRetryType.BLOCKING_RETRY;
			else if(e.getCause() instanceof SQLException)
				retryType = DatabasePrerequisite.determineRetryType((SQLException) e.getCause());
			else if(e instanceof RetrySpecifiedServiceException)
				retryType = ((RetrySpecifiedServiceException) e).getRetryType();
			else if(e.getCause() instanceof RetrySpecifiedServiceException)
				retryType = ((RetrySpecifiedServiceException) e.getCause()).getRetryType();
			else
				retryType = WorkRetryType.NONBLOCKING_RETRY;

			sb.append("; using ").append(retryType);
			log.error(sb.toString(), e);
			return retryType == null ? WorkRetryType.NONBLOCKING_RETRY : retryType;
		}

		protected void nonblockingRetry(Throwable e) throws SQLException, DataLayerException {
			errorText = StringUtils.exceptionToString(e);
			try {
				DataLayerMgr.executeCall("DELAY_DATA_UPDATE", this, true);
			} finally {
				errorText = null;
			}
		}

		protected void noRetry(Throwable e) throws SQLException, DataLayerException {
			recentlyProcessing.remove(this);
			errorText = StringUtils.exceptionToString(e);
			try {
				DataLayerMgr.executeCall("HALT_DATA_UPDATE", this, true);
			} finally {
				errorText = null;
			}
		}

		public long getRetryDelay() {
			return getNonblockingRetryDelay();
		}

		public long getRetryMax() {
			return getNonblockingRetryCount();
		}

		public String getErrorText() {
			return errorText;
		}

		public int getAppInstance() {
			return DataLoaderDataQueue.this.getAppInstance();
		}

		public long getProcessedTime() {
			return processedTime;
		}
	}
	
	@Override
	protected void prepareLoad() throws SQLException, DataLayerException {
		super.prepareLoad();
		DataLayerMgr.executeCall("RENEW_EXPIRE_TIME", this, true);
		Thread renewerThread = new RenewerThread();
		renewerThread.setDaemon(true);
		renewerThread.start();
	}

	@Override
	public String getQueueKey() {
		return "DataTriggered";
	}

	public DataLoader getDataLoader() {
		return dataLoader;
	}

	public void setDataLoader(DataLoader dataLoader) {
		this.dataLoader = dataLoader;
	}

	public int getDataValueCount() {
		return dataValueCount;
	}

	public void setDataValueCount(int dataValueCount) {
		this.dataValueCount = dataValueCount;
	}

	public long getNonblockingRetryDelay() {
		return nonblockingRetryDelay;
	}

	public void setNonblockingRetryDelay(long instanceRetryDelay) {
		this.nonblockingRetryDelay = instanceRetryDelay;
	}

	public long getBlockingRetryDelay() {
		return blockingRetryDelay;
	}

	public void setBlockingRetryDelay(long blockingRetryDelay) {
		this.blockingRetryDelay = blockingRetryDelay;
	}

	public long getNonblockingRetryCount() {
		return nonblockingRetryCount;
	}

	public void setNonblockingRetryCount(long nonblockingRetryCount) {
		this.nonblockingRetryCount = nonblockingRetryCount;
	}

	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int instanceNum) {
		this.appInstance = instanceNum;
	}

	public Set<Long> getPendingIds() {
		return pendingIdsUnmod;
	}

	public int getExpireSeconds() {
		return expireSeconds;
	}

	public void setExpireSeconds(int expireSeconds) {
		this.expireSeconds = expireSeconds;
	}

	public long getCleanupInterval() {
		return cleanupInterval;
	}

	public void setCleanupInterval(long cleanupInterval) {
		this.cleanupInterval = cleanupInterval;
	}

	public int getRecentlyProcessingSize() {
		return recentlyProcessing.getMaxSize();
	}

	public void setRecentlyProcessingSize(int recentlyProcessingSize) {
		recentlyProcessing.setMaxSize(recentlyProcessingSize);
	}
}
