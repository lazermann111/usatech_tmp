package com.usatech.etl;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.Call;
import simple.lang.SystemUtils;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;


public class Tab {
	protected final ObjectBuilder tabBuilder;
	protected ObjectBuilder upsertBuilder;
	protected String schema;
	protected String tablespace;
	public final String name;
	protected Ent sourceEnt = Ent.BLANK_ENT;
	protected final Set<String> uniqueKeyNames = new LinkedHashSet<String>();
	protected final Set<String> primaryKeyNames = new LinkedHashSet<String>();
	protected final Map<String, Col> colsByName = new LinkedHashMap<String, Col>();
	protected final Map<String, Col> inheritedColsByName = new LinkedHashMap<String, Col>();
	protected Call customUpsertCall = null;
	protected boolean customUpsertCallCopied = false;
	protected Map<String, Pnt> customUpsertPnts;
	protected SourceType sourceType;
	protected Pnt revisionPnt;
	protected Col firstPrimaryKeyCol;
	protected DeleteType deleteType;
	protected UpsertType upsertType;
	protected Tab parent;
	protected String partitionExpression;
	protected String[] filterExpressions;
	protected String precalcExpression;
	protected String extraSql;
	protected final Set<Ind> indexes = new LinkedHashSet<Ind>();
	protected final Set<Col> referees = new HashSet<Col>();
	protected final Set<Tab> parentReferees = new HashSet<Tab>();
	protected final Set<String> unmodUniqueKeys = Collections.unmodifiableSet(uniqueKeyNames);
	protected final Set<String> unmodPrimaryKeys = Collections.unmodifiableSet(primaryKeyNames);
	protected final Set<Col> primaryKeyCols = new ColLookupSet(unmodPrimaryKeys, colsByName);
	protected final Set<Col> uniqueKeyCols = new ColLookupSet(unmodUniqueKeys, colsByName);
	protected final Collection<Col> unmodCols = Collections.unmodifiableCollection(colsByName.values());
	protected Collection<Pnt> unmodCustomUpsertPnts = null;
	protected final Collection<Col> allCols = new AbstractCollection<Col>() {
		@Override
		public Iterator<Col> iterator() {
			return new Iterator<Col>() {
				protected boolean inherited = false;
				protected Iterator<Col> iter = colsByName.values().iterator();
				protected Col next = moveNext();

				@Override
				public void remove() {
					throw new UnsupportedOperationException();
				}

				public boolean hasNext() {
					return next != null;
				}

				public Col next() {
					if(!hasNext())
						throw new NoSuchElementException("End of iterator");
					Col col = next;
					next = moveNext();
					return col;
				}

				protected Col moveNext() {
					if(iter.hasNext())
						return iter.next();
					if(inherited)
						return null;
					inherited = true;
					iter = inheritedColsByName.values().iterator();
					return moveNext();
				}
			};
		}

		@Override
		public int size() {
			return colsByName.size() + inheritedColsByName.size();
		}

		@Override
		public boolean contains(Object o) {
			if(!(o instanceof Col))
				return false;
			Col col = (Col) o;
			if(col.tab == Tab.this)
				return colsByName.get(col.name) == col; // or colsByName.containsKey(col.name)
			return inheritedColsByName.get(col.name) == col; // or inheritedColsByName.containsKey(col.name)
		}

		@Override
		public boolean isEmpty() {
			return colsByName.isEmpty() && inheritedColsByName.isEmpty();
		}
	};

	public Tab(ObjectBuilder tabBuilder) {
		this.name = tabBuilder.getAttrValue("name");
		this.tabBuilder = tabBuilder;
		setSchema(tabBuilder.getAttrValue("schema"));
		setSource(tabBuilder.getAttrValue("source"));
		setTablespace(tabBuilder.getAttrValue("tablespace"));
		String extra = tabBuilder.getConcatenatedNodeValues("extrasql", SystemUtils.getNewLine());
		if(extra != null)
			setExtraSql(extra);
	}

	protected Tab(String name, ObjectBuilder tabBuilder) {
		this.name = name;
		this.tabBuilder = tabBuilder;
	}

	public Tab copy() {
		Tab newTab = new Tab(name, tabBuilder);
		copy(newTab);
		return newTab;
	}

	protected void copy(Tab newTab) {
		newTab.sourceEnt = sourceEnt;
		newTab.setDeleteType(getDeleteType());
		newTab.setSourceType(getSourceType());
		newTab.setSchema(getSchema());
		if(customUpsertCall != null) {
			newTab.customUpsertCall = customUpsertCall;
			customUpsertCallCopied = true;
		}
		newTab.setParent(getParent());
		if(getFilterExpressions() != null)
			newTab.setFilterExpressions(getFilterExpressions().clone());
		newTab.setPartitionExpression(getPartitionExpression());
		newTab.setPrecalcExpression(getPrecalcExpression());
		newTab.setUpsertType(getUpsertType());
		newTab.setTablespace(getTablespace());
	}

	public void destroy() {
		// do nothing
	}

	protected void addCol(Col col) {
		if(col.tab != this)
			throw new IllegalArgumentException("Col '" + col.name + "' is part of tab '" + (col.tab == null ? "<NULL>" : col.tab.name) + "' and cannot be assigned to '" + name + "'");
		if(colsByName.containsKey(col.name))
			throw new IllegalArgumentException("Col '" + col.name + "' is already added to tab '" + (col.tab == null ? "<NULL>" : col.tab.name) + "'");
		switch(col.colType) {
			case IDENTITY_KEY:
				uniqueKeyNames.add(col.name);
				break;
			case PRIMARY_KEY:
				primaryKeyNames.add(col.name);
				if(firstPrimaryKeyCol == null)
					firstPrimaryKeyCol = col;
				break;
			case REVISION:
				if(revisionPnt instanceof Col && ((Col) revisionPnt).tab == this)
					throw new IllegalArgumentException("Tab '" + name + "' already has a revision column '" + revisionPnt.getName() + "'; Cannot add '" + col.name + "' as a revision column");
				revisionPnt = col;
	
		}
		colsByName.put(col.name, col);
		inheritedColsByName.remove(col.name);
		addInheritedCol(col);
	}

	protected void addInheritedCol(Col col) {
		for(Tab child : parentReferees) {
			if(!child.colsByName.containsKey(col.name)) {
				child.inheritedColsByName.put(col.name, col);
				child.addInheritedCol(col);
			}
		}
	}

	public void updateReferees(Tab newTab) {
		// must use array to avoid concurrent mod exception
		for(Col col : referees.toArray(new Col[referees.size()]))
			col.setRefTab(newTab);
		for(Tab tab : parentReferees.toArray(new Tab[parentReferees.size()]))
			tab.setParent(newTab);
	}

	public Col getCol(String colname) {
		return getCol(colname, false);
	}

	public Col getCol(String colname, boolean ignoreInherited) {
		Col col = colsByName.get(colname);
		if(col != null)
			return col;
		// check ref cols
		String[] parts = StringUtils.split(colname, '$', 1);
		if(parts.length > 1) {
			Col fkCol = colsByName.get(parts[0]);
			if(fkCol != null && fkCol.getRefTab().uniqueKeyNames.contains(parts[1]))
				return fkCol.getRefTab().getCol(parts[1]);
		}
		if(ignoreInherited || parent == null)
			return null;
		col = inheritedColsByName.get(colname);
		if(col != null)
			return col;
		if(parts.length > 1) {
			Col fkCol = inheritedColsByName.get(parts[0]);
			if(fkCol != null && fkCol.getRefTab().uniqueKeyNames.contains(parts[1]))
				return fkCol.getRefTab().getCol(parts[1]);
		}
		return null;
	}

	public Collection<Col> getCols() {
		return getCols(false);
	}

	public Collection<Col> getCols(boolean ignoreInherited) {
		return ignoreInherited ? unmodCols : allCols;
	}

	protected Col getReferingCol(String colname) {
		String[] parts = StringUtils.split(colname, '$', 1);
		if(parts.length > 1)
			return getCol(parts[0]);
		return null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(!StringUtils.isBlank(schema))
			sb.append(schema).append('.');
		sb.append(name).append('(');
		boolean first = true;
		for(Col col : getCols()) {
			if(first)
				first = false;
			else
				sb.append(", ");
			sb.append(col);
		}
		sb.append(')');
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Tab))
			return false;
		Tab other = (Tab) obj;
		return name.equals(other.name)
				&& ConvertUtils.areEqual(schema, other.schema)
				&& ConvertUtils.areEqual(sourceEnt, other.sourceEnt)
				&& colsAreEqual(colsByName, other.colsByName)
				&& ConvertUtils.areEqual(customUpsertCall, other.customUpsertCall)
				&& deleteType == other.deleteType
				&& (sourceType == null || other.sourceType == null || sourceType == other.sourceType)
				&& upsertType == other.upsertType
				&& ConvertUtils.areEqual(parent, other.parent)
				&& ConvertUtils.areEqual(partitionExpression, other.partitionExpression)
				&& ConvertUtils.areEqual(filterExpressions, other.filterExpressions)
				&& ConvertUtils.areEqual(precalcExpression, other.precalcExpression)
				&& ConvertUtils.areEqual(indexes, other.indexes)				
				;			
	}

	protected boolean colsAreEqual(Map<String, Col> colsByName1, Map<String, Col> colsByName2) {
		if(colsByName1.size() != colsByName2.size())
			return false;
		Iterator<Col> colIter1 = colsByName1.values().iterator();
		Iterator<Col> colIter2 = colsByName2.values().iterator();
		while(colIter1.hasNext()) {
			if(!colIter1.next().equals(colIter2.next()))
				return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
		if(sourceType == SourceType.NONE)
			this.sourceEnt = Ent.BLANK_ENT;
	}

	public Call getCustomUpsertCall() {
		return customUpsertCall;
	}

	public void setCustomUpsertCall(Call customUpsertCall, Map<String, Pnt> customUpsertPnts) {
		this.customUpsertCall = customUpsertCall;
		if(customUpsertCall == null) {
			if(getUpsertType() != null)
				switch(getUpsertType()) {
					case BOTH:
						upsertType = UpsertType.STANDARD;
						break;
					case CUSTOM:
						upsertType = null;
						break;
				}
			this.customUpsertPnts = null;
			this.unmodCustomUpsertPnts = Collections.emptySet();
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append("HANDLE_");
			if(!StringUtils.isBlank(schema))
				sb.append(schema).append('_');
			sb.append(name);
			customUpsertCall.setLabel(sb.toString());
			if(getUpsertType() == null || getUpsertType() == UpsertType.STANDARD)
				upsertType = UpsertType.BOTH;
			this.customUpsertPnts = customUpsertPnts;
			this.unmodCustomUpsertPnts = Collections.unmodifiableCollection(customUpsertPnts.values());
			if(revisionPnt == null)
				setRevisionPnt(customUpsertPnts.get("REVISION"));
		}
	}

	public Pnt getCustomUpsertPnt(String propName) {
		return customUpsertPnts.get(propName);
	}

	public Collection<Pnt> getCustomUpsertPnts() {
		return unmodCustomUpsertPnts;
	}
	public DeleteType getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(DeleteType deleteType) {
		this.deleteType = deleteType;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public ObjectBuilder getUpsertBuilder() {
		return upsertBuilder;
	}

	public void setUpsertBuilder(ObjectBuilder upsertBuilder) {
		this.upsertBuilder = upsertBuilder;
	}

	public void writeXml(XMLBuilder xmlBuilder) throws SAXException, ConvertException {
		if(tabBuilder != null && tabBuilder.getXmlExtraNodes() != null)
			tabBuilder.getXmlExtraNodes().writeExtraNodes(tabBuilder, xmlBuilder.getContentHandler());
		xmlBuilder.attributeForNext("name", name);
		if(!StringUtils.isBlank(schema))
			xmlBuilder.attributeForNext("schema", schema);
		SourceType sourceType = getSourceType();
		String source;
		if(sourceType != SourceType.NONE && !StringUtils.isBlank((source = getSource())))
			xmlBuilder.attributeForNext("source", source);
		if(sourceType != null)
			xmlBuilder.attributeForNext("sourceType", getSourceType().toString());
		if(getDeleteType() != null)
			xmlBuilder.attributeForNext("deleteType", getDeleteType().toString());
		if(getUpsertType() == UpsertType.CUSTOM)
			xmlBuilder.attributeForNext("upsertType", getUpsertType().toString());
		
		if(getParent() != null)
			xmlBuilder.attributeForNext("parent", parent.name);
		if(!StringUtils.isBlank(getPartitionExpression()))
			xmlBuilder.attributeForNext("partitionBy", getPartitionExpression());
		if(!StringUtils.isBlank(getPrecalcExpression()))
			xmlBuilder.attributeForNext("precalcBy", getPrecalcExpression());
		if(!StringUtils.isBlank(getTablespace()))
			xmlBuilder.attributeForNext("tablespace", getTablespace());
	
		xmlBuilder.elementStart("tab", tabBuilder != null ? tabBuilder.getAttributeNames() : null);
	
		if(getFilterExpressions() != null)
			for(String fe : getFilterExpressions()) {
				if(!StringUtils.isBlank(fe)) {
					xmlBuilder.attributeForNext("sql", fe);
					xmlBuilder.element("filter");
				}
			}
	
		for(Col col : colsByName.values())
			col.writeXml(xmlBuilder);
	
		if(getCustomUpsertCall() != null) {
			Argument[] arguments = getCustomUpsertCall().getArguments();
			xmlBuilder.attributeForNext("sql", getCustomUpsertCall().getSql());
			if(arguments != null && arguments.length > 0 && arguments[0] != null)
				xmlBuilder.attributeForNext("property", arguments[0].getPropertyName());
			xmlBuilder.elementStart("upsert");
			getCustomUpsertCall().writeArgumentsXml(xmlBuilder);
			xmlBuilder.elementEnd("upsert");
		}
		for(Ind ind : indexes)
			ind.writeXml(xmlBuilder);
	
		xmlBuilder.elementEnd("tab");
	}

	public boolean isPartitioned() {
		return !StringUtils.isBlank(getPartitionExpression());
	}

	public boolean isAncestorOf(Tab other) {
		for(Tab p = other.parent; p != null; p = p.parent)
			if(p == this)
				return true;
		return false;
	}

	public boolean isDescendantOf(Tab other) {
		for(Tab p = this.parent; p != null; p = p.parent)
			if(p == other)
				return true;
		return false;
	}

	public Tab getParent() {
		return parent;
	}

	public void setParent(Tab parent) {
		if(this.parent == parent)
			return;
		if(this.parent != null)
			this.parent.parentReferees.remove(this);
		this.parent = parent;
		inheritedColsByName.clear();
		if(parent != null) {
			parent.parentReferees.add(this);
			if(revisionPnt == null && parent.revisionPnt != null)
				setRevisionPnt(parent.revisionPnt);
			addInheritedCols();
		} else if(revisionPnt instanceof Col && ((Col) revisionPnt).tab != this)
			setRevisionPnt(null);
		// update children
		for(Tab child : parentReferees) {
			child.inheritedColsByName.clear();
			child.addInheritedCols();
		}
	}

	protected void addInheritedCols() {
		for(Col col : parent.colsByName.values()) {
			if(colsByName.containsKey(col.name) || inheritedColsByName.containsKey(col.name))
				continue;
			inheritedColsByName.put(col.name, col.asInherited());
		}
		for(Col col : parent.inheritedColsByName.values()) {
			if(colsByName.containsKey(col.name) || inheritedColsByName.containsKey(col.name))
				continue;
			inheritedColsByName.put(col.name, col);
		}
	}
	
	protected void setRevisionPnt(Pnt revisionPnt) {
		if(this.revisionPnt != revisionPnt) {
			Pnt old = this.revisionPnt;
			this.revisionPnt = revisionPnt;
			for(Tab child : parentReferees)
				if(child.revisionPnt == old)
					child.setRevisionPnt(revisionPnt);
		}
	}

	public String getPartitionExpression() {
		return partitionExpression;
	}

	public void setPartitionExpression(String partitionExpression) {
		this.partitionExpression = partitionExpression;
	}

	public String[] getFilterExpressions() {
		return filterExpressions;
	}

	public void setFilterExpressions(String[] filterExpressions) {
		this.filterExpressions = filterExpressions;
	}

	public String getPrecalcExpression() {
		return precalcExpression;
	}

	public void setPrecalcExpression(String precalcExpression) {
		this.precalcExpression = precalcExpression;
	}

	public Set<Ind> getIndexes() {
		return indexes;
	}

	public Iterable<Col> getIdentityCols() {
		return uniqueKeyNames.isEmpty() ? getPrimaryKeyCols() : getUniqueKeyCols();
	}

	public boolean hasCol(String colname) {
		return colsByName.containsKey(colname);
	}

	public UpsertType getUpsertType() {
		return upsertType;
	}

	public void setUpsertType(UpsertType upsertType) {
		this.upsertType = upsertType;
		switch(upsertType) {
			case STANDARD:
				customUpsertCall = null;
				customUpsertPnts = null;
				unmodCustomUpsertPnts = Collections.emptySet();
				break;
		}
	}

	public String getExtraSql() {
		return extraSql;
	}

	public void setExtraSql(String extraSql) {
		this.extraSql = extraSql;
	}

	public Set<Col> getPrimaryKeyCols() {
		return primaryKeyCols;
	}

	public Set<Col> getUniqueKeyCols() {
		return uniqueKeyCols;
	}

	public Set<String> getUniqueKeyNames() {
		return unmodUniqueKeys;
	}

	public Set<String> getPrimaryKeyNames() {
		return unmodPrimaryKeys;
	}

	public Col getFirstPrimaryKeyCol() {
		return firstPrimaryKeyCol;
	}

	public String getSource() {
		return sourceEnt.toString();
	}

	public boolean hasSource() {
		return sourceType != SourceType.NONE && !sourceEnt.toString().isEmpty();
	}

	public void setSource(String source) {
		if(StringUtils.isBlank(source)) {
			sourceEnt = Ent.BLANK_ENT;
			sourceType = SourceType.NONE;
		} else {
			sourceEnt = new Ent(source);
		}
	}

	public String getSourceDsn() {
		return sourceEnt.getDsn();
	}

	public String getSourceSchema() {
		return sourceEnt.getSchema();
	}

	public String getSourceTable() {
		return sourceEnt.getTable();
	}

	public String getSourceCatalog() {
		return sourceEnt.getCatalog();
	}

	public Pnt getRevisionPnt() {
		return revisionPnt;
	}

	public String getTablespace() {
		return tablespace;
	}

	public void setTablespace(String tablespace) {
		this.tablespace = tablespace;
	}
}