package com.usatech.etl;

import java.io.IOException;

public interface DdlHandler extends Appendable {
	public void start(int buildNumber) throws IOException;

	public void startStatement(String comment) throws IOException;

	public void endStatement() throws IOException;

	public void end() throws IOException;
}
