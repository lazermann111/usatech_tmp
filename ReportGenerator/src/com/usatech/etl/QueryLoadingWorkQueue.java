package com.usatech.etl;

import java.sql.SQLException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import simple.app.AvailabilityChangeListener;
import simple.app.Prerequisite;
import simple.app.SelfProcessor;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.event.ProgressListener;
import simple.io.Log;
import simple.lang.Initializer;
import simple.results.Results;
import simple.text.StringUtils;

public abstract class QueryLoadingWorkQueue extends QueueBufferWorkQueue implements AvailabilityChangeListener {
	private static final Log log = Log.getLog();

	protected String loadCallId = "GET_ITEMS";
	protected String prepareCallId = "RESET_ITEMS";
	protected long waitCheckInterval = 1000L;
	protected final Initializer<WorkQueueException> initializer = new Initializer<WorkQueueException>() {
		protected LoaderThread loadingThread;
		@Override
		protected void doInitialize() throws WorkQueueException {
			if(shutdown.get())
				throw new WorkQueueException("Queue is shutdown");
			loadingThread = new LoaderThread();
			loadingThread.setDaemon(true);
			loadingThread.start();
		}

		@Override
		protected void doReset() {
			if(loadingThread != null) {
				loadingThread.done.set(true);
				loadingThread.interrupt();
				loadingThread = null;
			}
		}
	};
	
	public WorkQueue.Retriever<SelfProcessor> getRetriever(ProgressListener listener) throws WorkQueueException {
		try {
			initializer.initialize();
		} catch(InterruptedException e) {
			throw new WorkQueueException(e);
		}
		return super.getRetriever(listener);
	}

	protected class LoaderThread extends Thread {
		protected final AtomicBoolean done = new AtomicBoolean();

		public LoaderThread() {
			super(getQueueKey() + "Loader");
		}

		public void run() {
			loadWork(done);
		}
	}

	protected void prepareLoad() throws SQLException, DataLayerException {
		if(!StringUtils.isBlank(getPrepareCallId()))
			DataLayerMgr.executeCall(getPrepareCallId(), this, true);
	}
	protected void loadWork(AtomicBoolean shutdown) {
		boolean reset = false;
		while(true) {
			try {
				if(!reset) {
					prepareLoad();
					reset = true;
				}
				while(true) {
					if(shutdown.get())
						return;
					queueBuffer.awaitNotFull();
					if(shutdown.get())
						return;
					Results results = DataLayerMgr.executeQuery(getLoadCallId(), this, true);
					try {
						if(!results.next())
							break;
						do {
							Work<SelfProcessor> work = createTask(results);
							if(work != null)
								addWork(work, false);
						} while(results.next());
						if(log.isInfoEnabled())
							log.info("Added " + results.getRowCount() + " items to the queue");
					} finally {
						results.close();
					}
				}
			} catch(SQLException e) {
				log.error("Could not process data updates", e);
			} catch(DataLayerException e) {
				log.error("Could not process data updates", e);
			} catch(ConvertException e) {
				log.error("Could not process data updates", e);
			} catch(InterruptedException e) {
				log.error("Could not process data updates", e);
			} catch(RuntimeException e) {
				log.error("Could not process data updates", e);
			} catch(Error e) {
				log.error("Could not process data updates", e);
			}
			if(shutdown.get())
				return;
			long wait = getWaitCheckInterval();
			if(wait > 0)
				try {
					Thread.sleep(wait);
				} catch(InterruptedException e) {
					// ignore
				} catch(RuntimeException e) {
					log.error("While sleeping", e);
				} catch(Error e) {
					log.error("While sleeping", e);
				}
		}
	}

	@Override
	public void availibilityChanged(Prerequisite prerequisite, boolean available) {
		if(available)
			try {
				initializer.initialize();
			} catch(InterruptedException e) {
				log.error("Failed to initialize", e);
			} catch(WorkQueueException e) {
				log.error("Failed to initialize", e);
			}
		else
			initializer.reset();
	}

	protected abstract Work<SelfProcessor> createTask(Results results) throws ConvertException;
	
	public long getWaitCheckInterval() {
		return waitCheckInterval;
	}

	public void setWaitCheckInterval(long waitCheckInterval) {
		this.waitCheckInterval = waitCheckInterval;
	}

	public String getLoadCallId() {
		return loadCallId;
	}

	public void setLoadCallId(String itemsCallId) {
		this.loadCallId = itemsCallId;
	}

	public String getPrepareCallId() {
		return prepareCallId;
	}

	public void setPrepareCallId(String resetCallId) {
		this.prepareCallId = resetCallId;
	}

	@Override
	public Future<Boolean> shutdown() {
		Future<Boolean> ret = super.shutdown();
		initializer.reset();
		return ret;
	}
}
