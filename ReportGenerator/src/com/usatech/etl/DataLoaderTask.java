package com.usatech.etl;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.db.ConnectionGroup;
import simple.db.DataLayerMgr;
import simple.db.PlainConnectionGroup;
import simple.text.StringUtils;
import simple.util.FilterMap;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.DataLoaderAction;
import com.usatech.layers.common.constants.DataLoaderAttrEnum;

public class DataLoaderTask implements MessageChainTask {
	protected DataLoader dataLoader;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		final MessageChainStep step = taskInfo.getStep();
		final DataLoaderAction action;
		final String table;
		final String source;
		final InputStream metadata;
		try {
			action = step.getAttribute(DataLoaderAttrEnum.ATTR_ACTION, DataLoaderAction.class, true);
			table = step.getAttribute(DataLoaderAttrEnum.ATTR_TABLE, String.class, false);
			source = step.getAttribute(DataLoaderAttrEnum.ATTR_SOURCE, String.class, false);
			metadata = step.getAttribute(DataLoaderAttrEnum.ATTR_METADATA, InputStream.class, false);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException("Attribute not provided", e, WorkRetryType.NO_RETRY);
		}
		final Map<String, Object> sourceValues = new FilterMap<Object>(step.getAttributes(), "sourceValues.", null);
		final ConnectionGroup connGroup = new PlainConnectionGroup(DataLayerMgr.getDataSourceFactory());
		boolean okay = false;
		try {
			switch(action) {
				case UPSERT:
					if(StringUtils.isBlank(table))
						throw new RetrySpecifiedServiceException("Attribute '" + DataLoaderAttrEnum.ATTR_TABLE + "' not provided", WorkRetryType.NO_RETRY);
					dataLoader.loadRow(table, source, sourceValues, connGroup);
					break;
				case METADATA:
					if(metadata == null)
						throw new RetrySpecifiedServiceException("Attribute '" + DataLoaderAttrEnum.ATTR_METADATA + "' not provided", WorkRetryType.NO_RETRY);
					dataLoader.updateMetadata(metadata, true, true);
					break;
				case REPOPULATE:
					if(StringUtils.isBlank(table))
						throw new RetrySpecifiedServiceException("Attribute '" + DataLoaderAttrEnum.ATTR_TABLE + "' not provided", WorkRetryType.NO_RETRY);
					dataLoader.repopulateColumns(table, source, sourceValues.keySet(), connGroup);
					break;
				case PARTIAL:
					if(StringUtils.isBlank(table))
						throw new RetrySpecifiedServiceException("Attribute '" + DataLoaderAttrEnum.ATTR_TABLE + "' not provided", WorkRetryType.NO_RETRY);
					final Set<String> targetKeys = new HashSet<String>();
					final Map<String, Object> sourceKeyValues = new HashMap<String, Object>();
					for(Map.Entry<String, Object> entry : sourceValues.entrySet()) {
						if(StringUtils.isBlank(entry.getKey()))
							continue;
						if(entry.getKey().charAt(0) == '@')
							targetKeys.add(entry.getKey());
						else
							sourceKeyValues.put(entry.getKey(), entry.getValue());
					}
					dataLoader.partialUpdate(table, source, sourceKeyValues, targetKeys, connGroup);
					break;
				default:
					throw new ServiceException("Unsupported action " + action);
			}
			connGroup.commit();
			okay = true;
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} finally {
			connGroup.close(!okay);
		}
		return 0;
	}

	public DataLoader getDataLoader() {
		return dataLoader;
	}

	public void setDataLoader(DataLoader dataLoader) {
		this.dataLoader = dataLoader;
	}

}
