package com.usatech.etl;

public interface PntLookup {
	public Pnt getPnt(String colname);
}
