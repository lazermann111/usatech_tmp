package com.usatech.etl;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.Hasher;
import simple.text.StringUtils;
import simple.util.CompositeSet;
import simple.xml.ObjectBuilder;

public class GenerateSourceViewStubs {
	private static final Log log = Log.getLog();
	protected static final Pattern FOREIGN_KEY_PATTERN = Pattern.compile("(\\w+)\\((\\w+)\\)");
	protected static final Set<String> precalcedReferences = new HashSet<String>();
	protected static final Set<String> ignoredReferences = new HashSet<String>();
	protected static final Set<String> ignoredColumns = new HashSet<String>();
	protected static String viewSchema = "RDW_LOADER";
	protected static String defaultSourceDsn = "OPER";
	protected static String defaultTargetDsn = "MAIN";
	protected static int maxNameLength = 30;
	protected static final Map<String, String> fkeySqlTypeMapping = new HashMap<String, String>();
	protected static final String newline = SystemUtils.getNewLine();
	protected static int topCategoryId = 500;
	protected static int fieldCategoryId = 500;
	protected static int joinFiilterId = 5000;

	protected static final Hasher hasher;
	static {
		Hasher h;
		try {
			h = new Hasher("MD5", false);
		} catch(NoSuchAlgorithmException e) {
			h = null;
		}
		hasher = h;
		ignoredReferences.add("TIME_SEGMENT_DIM");
		ignoredReferences.add("_");
		ignoredReferences.add("CONFIG_DIM");
		ignoredReferences.add("CONFIG_FACT");
		ignoredReferences.add("PAYMENT_ITEM_FACT");
		ignoredReferences.add("ITEM_FACT");
		ignoredReferences.add("ITEM_DETAIL_DIM");
		precalcedReferences.add("TIME_DIM");
		precalcedReferences.add("DATE_DIM");
		precalcedReferences.add("ITEM_TYPE_DIM");
		fkeySqlTypeMapping.put("SERIAL", "INTEGER");
		fkeySqlTypeMapping.put("BIGSERIAL", "BIGINT");
		fkeySqlTypeMapping.put("SMALLSERIAL", "SMALLINT");
		ignoredColumns.add("CREATED_UTC_TS");
		ignoredColumns.add("CREATED_TS");
		ignoredColumns.add("CREATED_BY");
		ignoredColumns.add("LAST_UPDATED_UTC_TS");
		ignoredColumns.add("LAST_UPDATED_TS");
		ignoredColumns.add("LAST_UPDATED_BY");
		ignoredColumns.add("REVISION");
	}

	public static class Col implements Cloneable {
		protected String name;
		protected SQLType sqlType;
		protected String referenceName;
		protected boolean primaryKey;
		protected boolean required;
		protected boolean uniqueKey;
		protected String defaultExp;
		protected String preCalcValue;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public SQLType getSqlType() {
			return sqlType;
		}

		public void setSqlType(SQLType sqlType) {
			this.sqlType = sqlType;
		}

		public String getReferenceName() {
			return referenceName;
		}

		public void setReferenceName(String referenceName) {
			this.referenceName = referenceName;
		}

		public boolean isPrimaryKey() {
			return primaryKey;
		}

		public void setPrimaryKey(boolean primaryKey) {
			this.primaryKey = primaryKey;
		}

		public boolean isRequired() {
			return required;
		}

		public void setRequired(boolean required) {
			this.required = required;
		}

		public boolean isUniqueKey() {
			return uniqueKey;
		}

		public void setUniqueKey(boolean uniqueKey) {
			this.uniqueKey = uniqueKey;
		}

		public void readXML(ObjectBuilder builder) {
			setName(builder.getAttrValue("name"));
			setSqlType(convertSqlType(builder.getAttrValue("sqlType")));
			String coltypecat = builder.getAttrValue("type");
			Matcher matcher = FOREIGN_KEY_PATTERN.matcher(coltypecat);
			if(matcher.matches()) {
				setReferenceName(matcher.group(1));
			}
			for(ObjectBuilder attribBuilder : builder.getSubNodes("attrib")) {
				String attrib = attribBuilder.getAttrValue("name");
				if("NotN".equals(attrib))
					setRequired(true);
				else if("PrKey".equals(attrib))
					setPrimaryKey(true);
				else if("Udx1".equals(attrib))
					setUniqueKey(true);
				else if("Def".equals(attrib))
					setDefaultExp(attribBuilder.getAttrValue("value"));
				else {
					String value = attribBuilder.getAttrValue("value");
					if(!StringUtils.isBlank(value) && !StringUtils.isBlank(attrib)) {
						if(Character.isUpperCase(attrib.charAt(0)))
							attrib = attrib.substring(0, 1).toLowerCase() + attrib.substring(1);
						try {
							ReflectionUtils.setProperty(this, attrib, value, true);
						} catch(IntrospectionException e) {
							log.warn("Could not set property '" + attrib + "' to '" + value + "' on column " + getName(), e);
						} catch(IllegalAccessException e) {
							log.warn("Could not set property '" + attrib + "' to '" + value + "' on column " + getName(), e);
						} catch(InvocationTargetException e) {
							log.warn("Could not set property '" + attrib + "' to '" + value + "' on column " + getName(), e);
						} catch(InstantiationException e) {
							log.warn("Could not set property '" + attrib + "' to '" + value + "' on column " + getName(), e);
						} catch(ConvertException e) {
							log.warn("Could not set property '" + attrib + "' to '" + value + "' on column " + getName(), e);
						} catch(ParseException e) {
							log.warn("Could not set property '" + attrib + "' to '" + value + "' on column " + getName(), e);
						}
					}
				}					
			}
		}

		public String getDefaultExp() {
			return defaultExp;
		}

		public void setDefaultExp(String defaultExp) {
			this.defaultExp = defaultExp;
		}

		public String getPreCalcValue() {
			return preCalcValue;
		}

		public void setPreCalcValue(String preCalcValue) {
			this.preCalcValue = preCalcValue;
		}

		@Override
		public int hashCode() {
			return name == null ? 0 : name.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return this == obj || (obj instanceof Col && ConvertUtils.areEqual(name, ((Col) obj).name));
		}

		public Col clone() {
			try {
				return (Col) super.clone();
			} catch(CloneNotSupportedException e) {
				return null;
			}
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public static class Tab {
		protected String schema;
		protected String name;
		protected final Set<Col> pkCols = new LinkedHashSet<Col>();
		protected final Set<Col> ukCols = new LinkedHashSet<Col>();
		protected final Set<Col> dataCols = new LinkedHashSet<Col>();
		protected final Set<Col> inheritedCols = new LinkedHashSet<Col>();
		@SuppressWarnings("unchecked")
		protected final CompositeSet<Col> cols = new CompositeSet<Col>(pkCols, ukCols, dataCols);
		protected String parentName;
		protected String parentSchema;
		protected Tab parent;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Set<Col> getCols() {
			return cols;
		}

		public Iterable<Col> getUniqueColIterable() {
			return ukCols;
		}

		public void readXML(ObjectBuilder builder) {
			setName(builder.getAttrValue("name"));
			setSchema(builder.getAttrValue("schema"));
			String par = builder.getAttrValue("parent");
			if(!StringUtils.isBlank(par)) {
				int p = par.indexOf('.');
				if(p >= 0) {
					setParentSchema(par.substring(0, p));
					setParentName(par.substring(p + 1));
				} else {
					setParentSchema(null);
					setParentName(par);
				}
			} else {
				setParentSchema(null);
				setParentName(null);
			}
			for(ObjectBuilder colBuilder : builder.getSubNodes("field")) {
				Col col = new Col();
				col.readXML(colBuilder);
				addCol(col);
			}
		}

		protected void addCol(Col col) {
			if(ignoredColumns.contains(col.getName()))
				return;
			if(col.isPrimaryKey()) {
				pkCols.add(col);
				col = col.clone();
				col.setPrimaryKey(false);
				inheritedCols.add(col);
			} else if(col.isUniqueKey()) {
				ukCols.add(col);
				col = col.clone();
				col.setUniqueKey(false);
				inheritedCols.add(col);
			} else {
				dataCols.add(col);
				inheritedCols.add(col);
			}

		}

		public String getSchema() {
			return schema;
		}

		public void setSchema(String schema) {
			this.schema = schema;
		}

		public String getParentName() {
			return parentName;
		}

		public void setParentName(String parentName) {
			this.parentName = parentName;
		}

		public void resolveParent(Map<String, Tab> tables) {
			if(parent == null && parentName != null) {
				parent = tables.get(parentName);
				Tab anc = parent;
				while(anc != null) {
					cols.merge(anc.inheritedCols);
					anc.resolveParent(tables);
					anc = anc.parent;
				}
			}
		}

		public String getParentSchema() {
			return parentSchema;
		}

		public void setParentSchema(String parentSchema) {
			this.parentSchema = parentSchema;
		}

		@Override
		public int hashCode() {
			return SystemUtils.addHashCodes(0, name, schema);
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(!(obj instanceof Tab))
				return false;
			Tab t = (Tab) obj;
			return ConvertUtils.areEqual(name, t.name) && ConvertUtils.areEqual(schema, t.schema);
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			if(!StringUtils.isBlank(getSchema()))
				sb.append(getSchema()).append('.');
			sb.append(getName()).append('(');
			boolean first = true;
			for(Col col : getCols()) {
				if(first)
					first = false;
				else
					sb.append(", ");
				sb.append(col);
			}
			sb.append(')');
			return sb.toString();
		}
	}
	
	/**
	 * @param args
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws ParseException
	 * @throws ConvertException
	 * @throws InstantiationException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws IntrospectionException
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException, SAXException, ParserConfigurationException, IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
		File summaryFile = new File("D:\\Development\\Database Projects\\DatabaseScripts\\REPTP\\RDW\\Druid_Summary.xml");
		final Map<String, Tab> tables = new LinkedHashMap<String, Tab>();
		ObjectBuilder builder = new ObjectBuilder(new FileReader(summaryFile));
		for(ObjectBuilder tableBuilder : builder.getSubNodes("table")) {
			// if(!tableBuilder.)
			Tab tab = new Tab();
			tab.readXML(tableBuilder);
			tables.put(tab.getName(), tab);
		}

		File viewFile = new File("D:\\Development\\Database Projects\\DatabaseScripts\\REPTP\\RDW\\SourceViewsStubs.sql");
		File metaFile = new File("D:\\Development\\Database Projects\\DatabaseScripts\\REPTP\\RDW\\loader-metadata.xml");
		File sqlFile = new File("D:\\Development\\Database Projects\\DatabaseScripts\\REPTP\\RDW\\SetupFieldsAndJoins_gen.sql");
		Writer writer = new FileWriter(viewFile);
		try {
			Writer metawriter = new FileWriter(metaFile);
			try {
				Writer sqlwriter = new FileWriter(sqlFile);
				try {
					metawriter.append("<db>").append(newline);
					for(Tab tab : tables.values()) {
						if(ignoreReference(tab.getName()))
							continue;
						tab.resolveParent(tables);
						boolean hasUK = tab.getUniqueColIterable().iterator().hasNext();
						boolean precalc = precalcReference(tab.getName());
						if(!precalc) {
							writer.append("CREATE OR REPLACE VIEW ").append(viewSchema).append(".VW_").append(tab.getName()).append('(');
							metawriter.append("<tab ");
							if(!StringUtils.isBlank(tab.getSchema()))
								metawriter.append("schema=\"").append(StringUtils.prepareCDATA(tab.getSchema())).append("\" ");
							metawriter.append("name=\"").append(StringUtils.prepareCDATA(tab.getName())).append("\" source=\"");
							if(!StringUtils.isBlank(defaultSourceDsn))
								metawriter.append(StringUtils.prepareCDATA(defaultSourceDsn)).append('.');
							metawriter.append(StringUtils.prepareCDATA(viewSchema)).append(".VW_").append(StringUtils.prepareCDATA(tab.getName())).append('"');
							// metawriter.append(" sourceType=\"SELECTABLE\"");
							metawriter.append('>').append(newline);
						}
						sqlwriter.append("INSERT INTO FOLIO_CONF.FIELD_CATEGORY(FIELD_CATEGORY_ID,FIELD_CATEGORY_LABEL,PARENT_FIELD_CATEGORY_ID)").append(newline).append("\tVALUES(").append(String.valueOf(++fieldCategoryId)).append(",'");
						appendLabel(tab.getName(), sqlwriter).append("',").append(String.valueOf(topCategoryId)).append(");").append(newline).append(newline);

						boolean first = true;
						for(Col col : tab.getCols()) {
							String colName = getName(col.getName());
							if(!precalc) {
								metawriter.append("<col name=\"").append(StringUtils.prepareCDATA(col.getName())).append("\" source=\"").append(StringUtils.prepareCDATA(colName)).append("\" sqltype=\"").append(StringUtils.prepareCDATA(col.getSqlType().toString())).append("\" coltype=\"");
								if(col.isPrimaryKey()) {
									if(hasUK)
										metawriter.append("PRIMARY_KEY");
									else
										metawriter.append("IDENTITY_KEY");
								} else if(col.isUniqueKey())
									metawriter.append("IDENTITY_KEY");
								else
									metawriter.append("DATA");
								metawriter.append('"');
								if(col.isRequired())
									metawriter.append(" required=\"true\"");

								String def = col.getDefaultExp();
								if(!StringUtils.isBlank(def))
									metawriter.append(" default=\"").append(StringUtils.prepareCDATA(def)).append('"');
								if(StringUtils.isBlank(col.getReferenceName()) || precalcReference(col.getReferenceName())) {
									if(!col.isPrimaryKey() || !hasUK) {
										if(first) {
											writer.append(newline).append('\t');
											first = false;
										} else
											writer.append(",").append(newline).append('\t');
										writer.append(colName);
										if(!colName.equalsIgnoreCase(col.getName())) {
											writer.append(" /* ").append(col.getName()).append(" */");
										}
									}
								} else if(!ignoreReference(col.getReferenceName())) {
									Tab refTab = tables.get(col.getReferenceName());
									if(refTab == null)
										throw new SAXException("Could not find table '" + col.getReferenceName() + "' referenced by '" + tab.getName() + '.' + col.getName() + "'");
									metawriter.append(" ref=\"").append(StringUtils.prepareCDATA(col.getReferenceName())).append('"');
									if(first) {
										writer.append(newline).append('\t');
										first = false;
									} else
										writer.append(",").append(newline).append('\t');
									boolean refFirst = true;
									for(Col refCol : refTab.getUniqueColIterable()) {
										if(refFirst)
											refFirst = false;
										else
											writer.append(",").append(newline).append('\t');
										String colFullName = col.getName() + '$' + refCol.getName();
										colName = getName(colFullName);
										writer.append(colName);
										if(!colName.equalsIgnoreCase(colFullName)) {
											writer.append(" /* ").append(colFullName).append(" */");
										}
									}
									if(refFirst)
										throw new SAXException("Referenced table '" + col.getReferenceName() + "' referenced by '" + tab.getName() + '.' + col.getName() + "' has no unique columns");
								}
								metawriter.append("/>").append(newline);
							}
							if(StringUtils.isBlank(col.getReferenceName())) {
								if(!col.isPrimaryKey() || !hasUK) {
									sqlwriter.append("INSERT INTO FOLIO_CONF.FIELD(FIELD_LABEL,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,FIELD_CATEGORY_ID)").append(newline).append("\tVALUES('");
									appendLabel(col, sqlwriter).append("','");
									if(!StringUtils.isBlank(defaultTargetDsn))
										sqlwriter.append(defaultTargetDsn).append('.');
									if(!StringUtils.isBlank(tab.getSchema()))
										sqlwriter.append(tab.getSchema()).append('.');
									sqlwriter.append(tab.getName()).append('.').append(col.getName()).append("','");
									String func;
									if(String.class.equals(SQLTypeUtils.getJavaType(col.sqlType)))
										func = "UPPER";
									else
										func = null;
									if(func != null)
										sqlwriter.append(func).append('(');
									if(!StringUtils.isBlank(defaultTargetDsn))
										sqlwriter.append(defaultTargetDsn).append('.');
									if(!StringUtils.isBlank(tab.getSchema()))
										sqlwriter.append(tab.getSchema()).append('.');
									sqlwriter.append(tab.getName()).append('.').append(col.getName());
									if(func != null)
										sqlwriter.append(')');
									String sqlTypeStr = col.sqlType.toString().replace("'", "''");
									sqlwriter.append("','").append(sqlTypeStr).append("','").append(sqlTypeStr).append("',").append(String.valueOf(fieldCategoryId)).append(");").append(newline).append(newline);
								}
							} else if(!ignoreReference(col.getReferenceName())) {
								Tab refTab = tables.get(col.getReferenceName());
								if(refTab == null)
									throw new SAXException("Could not find table '" + col.getReferenceName() + "' referenced by '" + tab.getName() + '.' + col.getName() + "'");
								// ADD FILTER_JOIN
								Col refCol = null;
								String prefix = null;
								if(col.getName().startsWith(refTab.getName()))
									prefix = "";
								for(Col pkCol : refTab.pkCols) {
									if(refCol == null)
										refCol = pkCol;
									if(prefix != null)
										break;
									if(col.getName().endsWith(pkCol.getName())) {
										prefix = col.getName().substring(0, col.getName().length() - pkCol.getName().length());
										int pos = prefix.length() - 1;
										while(pos >= 0 && prefix.charAt(pos) == '_')
											pos--;
										prefix = prefix.substring(0, pos + 1);
										break;
									}
								}
								if(refCol != null) {
									sqlwriter.append("INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION,JOIN_TYPE,ACTIVE_FLAG,JOIN_CARDINALITY)").append(newline).append("\tVALUES(").append(String.valueOf(joinFiilterId++)).append(",'");
									if(!StringUtils.isBlank(defaultTargetDsn))
										sqlwriter.append(defaultTargetDsn).append('.');
									if(!StringUtils.isBlank(tab.getSchema()))
										sqlwriter.append(tab.getSchema()).append('.');
									sqlwriter.append(tab.getName()).append("','");
									if(!StringUtils.isBlank(defaultTargetDsn))
										sqlwriter.append(defaultTargetDsn).append('.');
									if(!StringUtils.isBlank(refTab.getSchema()))
										sqlwriter.append(refTab.getSchema()).append('.');
									sqlwriter.append(refTab.getName());
									if(prefix == null)
										sqlwriter.append('[').append(col.getName()).append(']');
									else if(!prefix.isEmpty())
										sqlwriter.append('[').append(prefix).append(']');
									sqlwriter.append("','");

									if(!StringUtils.isBlank(defaultTargetDsn))
										sqlwriter.append(defaultTargetDsn).append('.');
									if(!StringUtils.isBlank(tab.getSchema()))
										sqlwriter.append(tab.getSchema()).append('.');
									sqlwriter.append(tab.getName()).append('.').append(col.getName()).append(" = ");
									if(!StringUtils.isBlank(defaultTargetDsn))
										sqlwriter.append(defaultTargetDsn).append('.');
									if(!StringUtils.isBlank(refTab.getSchema()))
										sqlwriter.append(refTab.getSchema()).append('.');
									sqlwriter.append(refTab.getName());
									if(prefix == null)
										sqlwriter.append('[').append(col.getName()).append(']');
									else if(!prefix.isEmpty())
										sqlwriter.append('[').append(prefix).append(']');
									sqlwriter.append('.').append(refCol.getName()).append("','");
									if(col.isRequired())
										sqlwriter.append("INNER");
									else
										sqlwriter.append("LEFT");
									sqlwriter.append("','Y','");
									if(col.isPrimaryKey() || col.isUniqueKey())
										sqlwriter.append("=");
									else
										sqlwriter.append(">");

									sqlwriter.append("');").append(newline).append(newline);
									if(prefix != null && !prefix.isEmpty()) {
										int parentFieldCategoryId = fieldCategoryId;
										sqlwriter.append("INSERT INTO FOLIO_CONF.FIELD_CATEGORY(FIELD_CATEGORY_ID,FIELD_CATEGORY_LABEL,PARENT_FIELD_CATEGORY_ID)").append(newline).append("\tVALUES(").append(String.valueOf(++fieldCategoryId)).append(",'");
										appendLabel(prefix, sqlwriter).append(' ');
										appendLabel(refTab.getName(), sqlwriter).append("',").append(String.valueOf(parentFieldCategoryId)).append(");").append(newline).append(newline);

										for(Col otherCol : refTab.getCols()) {
											if(otherCol.isPrimaryKey() || otherCol.isUniqueKey())
												continue;
											sqlwriter.append("INSERT INTO FOLIO_CONF.FIELD(FIELD_LABEL,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,FIELD_CATEGORY_ID)").append(newline).append("\tVALUES('");
											appendLabel(prefix, sqlwriter).append(' ');
											appendLabel(otherCol, sqlwriter).append("','");
											if(!StringUtils.isBlank(defaultTargetDsn))
												sqlwriter.append(defaultTargetDsn).append('.');
											if(!StringUtils.isBlank(refTab.getSchema()))
												sqlwriter.append(refTab.getSchema()).append('.');
											sqlwriter.append(refTab.getName());
											sqlwriter.append('[').append(prefix).append(']');
											sqlwriter.append('.').append(otherCol.getName()).append("','");
											String func;
											if(String.class.equals(SQLTypeUtils.getJavaType(otherCol.sqlType)))
												func = "UPPER";
											else
												func = null;
											if(func != null)
												sqlwriter.append(func).append('(');
											if(!StringUtils.isBlank(defaultTargetDsn))
												sqlwriter.append(defaultTargetDsn).append('.');
											if(!StringUtils.isBlank(refTab.getSchema()))
												sqlwriter.append(refTab.getSchema()).append('.');
											sqlwriter.append(refTab.getName());
											sqlwriter.append('[').append(prefix).append(']');
											sqlwriter.append('.').append(otherCol.getName());
											if(func != null)
												sqlwriter.append(')');
											sqlwriter.append("','");
											String sqlTypeStr = otherCol.sqlType.toString().replace("'", "''");
											sqlwriter.append(sqlTypeStr).append("','").append(sqlTypeStr).append("',").append(String.valueOf(fieldCategoryId)).append(");").append(newline).append(newline);
										}
									}
								}
							}
						}
						if(!precalc) {
							metawriter.append("</tab>").append(newline);
							writer.append(") AS").append(newline).append("SELECT");
							first = true;
							for(Col col : tab.getCols()) {
								if(StringUtils.isBlank(col.getReferenceName()) || precalcReference(col.getReferenceName())) {
									if(!col.isPrimaryKey() || !hasUK) {
										if(first) {
											writer.append(newline).append('\t');
											first = false;
										} else
											writer.append(",").append(newline).append('\t');
										writer.append(" /* ").append(col.getName()).append(" */");
									}
								} else if(!ignoreReference(col.getReferenceName())) {
									if(first) {
										writer.append(newline).append('\t');
										first = false;
									} else
										writer.append(",").append(newline).append('\t');
									Tab refTab = tables.get(col.getReferenceName());
									if(refTab == null)
										throw new SAXException("Could not find table '" + col.getReferenceName() + "' referenced by '" + tab.getName() + '.' + col.getName() + "'");
									boolean refFirst = true;
									for(Col refCol : refTab.getUniqueColIterable()) {
										if(refFirst)
											refFirst = false;
										else
											writer.append(",").append(newline).append('\t');
										writer.append(" /* ").append(col.getName()).append('$').append(refCol.getName()).append(" */");
									}
								}
							}
							writer.append(newline).append(newline).append(';').append(newline).append(newline);
						}
					}
					metawriter.append("</db>").append(newline);

					writer.flush();
					metawriter.flush();
					sqlwriter.flush();
				} finally {
					sqlwriter.close();
				}
			} finally {
				metawriter.close();
			}
		} finally {
			writer.close();
		}
	}

	protected static Appendable appendLabel(Col col, Appendable appendTo) throws IOException {
		return appendLabel(col.getName(), appendTo);
	}

	protected static Appendable appendLabel(String name, Appendable appendTo) throws IOException {
		boolean first = true;
		for(int i = 0; i < name.length(); i++) {
			char ch = name.charAt(i);
			if(Character.isLetterOrDigit(ch)) {
				if(first) {
					appendTo.append(Character.toUpperCase(ch));
					first = false;
				} else {
					appendTo.append(Character.toLowerCase(ch));
				}
			} else if(!first) {
				appendTo.append(' ');
				first = true;
			}
		}
		return appendTo;
	}

	private static boolean precalcReference(String referenceName) {
		return precalcedReferences.contains(referenceName);
	}

	private static boolean ignoreReference(String referenceName) {
		return ignoredReferences.contains(referenceName);
	}

	protected static String getName(String name) {
		name = name.toUpperCase();
		if(name.length() > maxNameLength)
			return name.substring(0, maxNameLength - 9) + '#' + StringUtils.toHex(name.substring(maxNameLength - 9).hashCode());
		return name;
	}

	protected static SQLType convertSqlType(String sqlType) {
		String alt = fkeySqlTypeMapping.get(sqlType.toUpperCase());
		if(alt != null)
			sqlType = alt;
		try {
			return ConvertUtils.convert(SQLType.class, sqlType);
		} catch(ConvertException e) {
			log.warn("Could not convert sql type '" + sqlType + "'", e);
			return null;
		}
	}
}
