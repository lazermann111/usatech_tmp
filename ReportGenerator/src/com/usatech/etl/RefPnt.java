package com.usatech.etl;

import simple.sql.SQLType;

public class RefPnt implements Pnt {
	protected final Pnt localPnt;
	protected final Col refCol;

	public RefPnt(Pnt localPnt, Col refCol) {
		this.localPnt = localPnt;
		this.refCol = refCol;
	}

	@Override
	public String getName() {
		return localPnt.getName();
	}

	@Override
	public SQLType getSQLType() {
		return refCol.getSQLType();
	}

	@Override
	public boolean isRequired() {
		return localPnt.isRequired() && refCol.isRequired();
	}

	@Override
	public boolean isRef() {
		return true;
	}

	@Override
	public Col getRefCol() {
		return refCol;
	}

	@Override
	public boolean isInput() {
		return true;
	}

	@Override
	public boolean isOutput() {
		return true;
	}

	public Pnt getLocalPnt() {
		return localPnt;
	}
}
