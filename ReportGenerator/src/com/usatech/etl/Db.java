package com.usatech.etl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.Call;
import simple.db.ConnectionHolder;
import simple.db.DataLayerMgr;
import simple.db.DataSourceConnectionHolder;
import simple.db.Parameter;
import simple.io.LoadingException;
import simple.io.Log;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLExtraNodes;

abstract class Db<E extends Tab> {
	private static final Log log = Log.getLog();
	protected final ConcurrentMap<String, E> tabs;
	protected final Collection<E> unmodTabs;
	protected final Set<String> unmodNames;
	protected String[] preSql;
	protected String[] postSql;
	protected int buildNumber;

	protected static final AtomicInteger refHandlerSeq = new AtomicInteger();

	protected static abstract class RefHandler implements Comparable<RefHandler> {
		protected final int seq = refHandlerSeq.incrementAndGet();

		public int compareTo(RefHandler o) {
			return seq - o.seq;
		}

		public abstract void setRefTab(Tab tab);
	}

	protected static class SetParentRefHandler extends RefHandler {
		protected final Tab tab;

		public SetParentRefHandler(Tab tab) {
			this.tab = tab;
		}

		public int compareTo(RefHandler o) {
			if(o instanceof SetParentRefHandler)
				return super.compareTo(o);
			return 1;
		}

		public void setRefTab(Tab parentTab) {
			tab.setParent(parentTab);
		}
	}

	protected static class SetRefRefHandler extends RefHandler {
		protected final Col col;

		public SetRefRefHandler(Col col) {
			this.col = col;
		}

		public int compareTo(RefHandler o) {
			if(o instanceof SetParentRefHandler) {
				return -1;
			}
			return super.compareTo(o);
		}

		public void setRefTab(Tab refTab) {
			col.setRefTab(refTab);
		}
	}

	protected static class SetPntRefRefHandler extends RefHandler {
		protected final Map<String, Pnt> pnts;
		protected final Pnt pnt;
		protected final String baseName;

		public SetPntRefRefHandler(Map<String, Pnt> pnts, Pnt pnt, String baseName) {
			this.pnts = pnts;
			this.pnt = pnt;
			this.baseName = baseName;
		}
		public int compareTo(RefHandler o) {
			if(o instanceof SetParentRefHandler) {
				return -1;
			}
			return super.compareTo(o);
		}

		public void setRefTab(Tab tab) {
			if(!pnts.containsKey(baseName))
				pnts.put(baseName, new RefPnt(pnt, tab.getFirstPrimaryKeyCol()));
		}
	}

	protected Db(ConcurrentMap<String, E> tabs) {
		this.tabs = tabs;
		this.unmodTabs = Collections.unmodifiableCollection(tabs.values());
		this.unmodNames = Collections.unmodifiableSet(tabs.keySet());
	}

	public boolean clearTabs(MetadataChangeTracker tracker) {
		boolean changed = false;
		Iterator<E> iter = tabs.values().iterator();
		while(iter.hasNext()) {
			Tab old = iter.next();
			if(tracker != null)
				tracker.tabRemoved(old);
			old.updateReferees(null);
			old.destroy();
			iter.remove();
			changed = true;
		}
		return changed;
	}

	public boolean removeTab(String tabname, MetadataChangeTracker tracker) {
		Tab old = tabs.remove(tabname);
		if(old == null)
			return false;
		if(tracker != null)
			tracker.tabRemoved(old);
		old.updateReferees(null);
		old.destroy();
		return true;
	}

	public void addTabs(Iterable<E> tabs, MetadataChangeTracker tracker) {
		for(E tab : tabs)
			addTab(tab, tracker);
	}

	public boolean addTab(E tab, MetadataChangeTracker tracker) {
		Tab old = tabs.putIfAbsent(tab.name, tab);
		if(old == null) {
			if(tracker != null)
				tracker.tabAdded(tab);
			return true;
		}
		return false;
	}

	public boolean replaceAllTabs(Iterable<E> tabs, MetadataChangeTracker tracker) {
		Map<String, E> remove = new HashMap<String, E>(this.tabs);
		boolean changed = false;
		for(E tab : tabs) {
			changed |= replaceTab(tab, tracker);
			remove.remove(tab.name);
		}
		for(E old : remove.values()) {
			if(this.tabs.remove(old.name, old)) {
				if(tracker != null)
					tracker.tabRemoved(old);
				old.updateReferees(null);
				old.destroy();
				changed = true;
			}
		}
		return changed;
	}

	public boolean replaceTab(E tab, MetadataChangeTracker tracker) {
		while(true) {
			E old = tabs.get(tab.name);
			if(old != null) {
				if(tabs.replace(tab.name, old, tab)) {
					boolean changed = !ConvertUtils.areEqual(old, tab);
					if(changed && tracker != null)
						tracker.tabModified(old, tab);
					old.updateReferees(tab);
					old.destroy();
					return changed;
				}
			} else {
				if(tabs.putIfAbsent(tab.name, tab) == null) {
					if(tracker != null)
						tracker.tabAdded(tab);
					return true;
				}
			}

		}
	}

	public E getTab(String name) {
		return tabs.get(name);
	}

	public Collection<E> getTabs() {
		return unmodTabs;
	}

	public Set<String> getTabNames() {
		return unmodNames;
	}

	public String[] getPreSql() {
		return preSql;
	}

	public void setPreSql(String[] preSql) {
		this.preSql = preSql;
	}

	public String[] getPostSql() {
		return postSql;
	}

	public void setPostSql(String[] postSql) {
		this.postSql = postSql;
	}

	public void load(InputStream xml, String targetDsn, MetadataChangeTracker tracker) throws LoadingException {
		XMLExtraNodes xen = new XMLExtraNodes();
		ObjectBuilder builder;
		try {
			builder = new ObjectBuilder(xml, xen);
		} catch(IOException e) {
			throw new LoadingException(e);
		} catch(SAXException e) {
			throw new LoadingException(e);
		} catch(ParserConfigurationException e) {
			throw new LoadingException(e);
		}
		Integer build = builder.getAttr("build", Integer.class);
		if(build != null)
			setBuildNumber(build);
		Map<String, E> addedTabs = new LinkedHashMap<String, E>();
		SortedMap<RefHandler, String> refHandlers = new TreeMap<RefHandler, String>();
		for(ObjectBuilder tabBuilder : builder.getSubNodes("tab")) {
			E tab = readTab(tabBuilder, refHandlers, targetDsn);
			addedTabs.put(tab.name, tab);
		}
		for(ObjectBuilder tabBuilder : builder.getSubNodes("upsert")) {
			E tab = readUpsert(tabBuilder, refHandlers, targetDsn);
			addedTabs.put(tab.name, tab);
		}
		for(Map.Entry<RefHandler, String> entry : refHandlers.entrySet()) {
			entry.getKey().setRefTab(addedTabs.get(entry.getValue()));
		}
		for(E tab : addedTabs.values()) {
			ObjectBuilder[] indBuilders = tab.tabBuilder.getSubNodes("ind");
			if(indBuilders != null)
				for(ObjectBuilder indBuilder : indBuilders)
					addInd(tab, indBuilder);
		}
		setPreSql(builder.getNodeValues("presql"));
		replaceAllTabs(addedTabs.values(), tracker);
		setPostSql(builder.getNodeValues("postsql"));
	}

	protected E readTab(ObjectBuilder tabBuilder, Map<RefHandler, String> refHandlers, String targetDsn) throws LoadingException {
		return readTab(tabBuilder, refHandlers, null, targetDsn);
	}

	protected abstract E createTab(ObjectBuilder tabBuilder);

	protected E readTab(ObjectBuilder tabBuilder, Map<RefHandler, String> refHandlers, Collection<Col> copyCols, String targetDsn) throws LoadingException {
		final E tab = createTab(tabBuilder);
		tab.setSourceType(tabBuilder.get("sourceType", SourceType.class));
		tab.setDeleteType(tabBuilder.get("deleteType", DeleteType.class));
		UpsertType upsertType = tabBuilder.getAttr("upsertType", UpsertType.class);
		String parent = tabBuilder.getAttrValue("parent");
		if(!StringUtils.isBlank(parent))
			refHandlers.put(new SetParentRefHandler(tab), parent);
		tab.setPartitionExpression(tabBuilder.getAttrValue("partitionBy"));
		tab.setPrecalcExpression(tabBuilder.getAttrValue("precalcBy"));

		ObjectBuilder[] filterBuilders = tabBuilder.getSubNodes("filter");
		if(filterBuilders != null) {
			List<String> list = new ArrayList<String>();
			for(ObjectBuilder filterBuilder : filterBuilders) {
				String fe = filterBuilder.getAttrValue("sql");
				if(!StringUtils.isBlank(fe))
					list.add(fe);
			}
			if(!list.isEmpty())
				tab.setFilterExpressions(list.toArray(new String[list.size()]));
		}
		if(copyCols != null)
			for(Col col : copyCols)
				col.copyTo(tab);

		for(ObjectBuilder colBuilder : tabBuilder.getSubNodes("col")) {
			addCol(tab, colBuilder, refHandlers, false);
		}
		ObjectBuilder[] upsertBuilders = tabBuilder.getSubNodes("upsert");
		if(upsertType == null)
			upsertType = upsertBuilders != null && upsertBuilders.length > 0 ? UpsertType.BOTH : UpsertType.STANDARD;
		else
			switch(upsertType) {
				case CUSTOM:
				case BOTH:
					if(upsertBuilders == null || upsertBuilders.length == 0)
						throw new LoadingException("No <upsert> elements specified. Must specify one when upsertType equals " + upsertType + " at " + getBuilderLocation(tabBuilder));

			}
		switch(upsertType) {
			case CUSTOM:
			case BOTH:
				if(upsertBuilders.length == 1) {
					Map<String, Pnt> upsertPnts = new LinkedHashMap<String, Pnt>();
					Call upsertCall = readCall(upsertBuilders[0], targetDsn, refHandlers, upsertPnts);
					tab.setCustomUpsertCall(upsertCall, upsertPnts);
					tab.setUpsertBuilder(upsertBuilders[0]);
				} else
					throw new LoadingException("Too many <upsert> elements specified. Only one per <tab> is allowed" + getBuilderLocation(upsertBuilders[1]));
				break;
		}

		tab.setUpsertType(upsertType);

		return tab;
	}

	protected E readUpsert(ObjectBuilder tabBuilder, Map<RefHandler, String> refHandlers, String targetDsn) throws LoadingException {
		E tab = createTab(tabBuilder);
		tab.setSourceType(SourceType.NONE);
		tab.setUpsertType(UpsertType.CUSTOM);
		Map<String, Pnt> upsertPnts = new LinkedHashMap<String, Pnt>();
		Call upsertCall = readCall(tabBuilder, targetDsn, refHandlers, upsertPnts);
		tab.setCustomUpsertCall(upsertCall, upsertPnts);
		tab.setUpsertBuilder(tabBuilder);
		return tab;
	}

	protected Call readCall(ObjectBuilder builder, String dataSourceName, Map<RefHandler, String> refHandlers, final Map<String, Pnt> pnts) throws LoadingException {
		Call call = new Call();
		call.setSql(builder.getAttrValue("sql"));

		List<Argument> arguments = new ArrayList<Argument>();
		ConnectionHolder ch = new DataSourceConnectionHolder(DataLayerMgr.getGlobalDataLayer(), dataSourceName);
		try {
			Parameter p = new Parameter();
			p.setIn(false);
			p.setOut(true);
			p.setPropertyName(builder.getAttrValue("property"));
			arguments.add(p);
			ObjectBuilder[] pnodes = builder.getSubNodes();
			if(pnodes != null) {
				for(ObjectBuilder node : pnodes) {
					if(node.getTag().equalsIgnoreCase("parameter")) {
						p = Call.readParameter(node, ch);
						arguments.add(p);
						final Pnt pnt = new ParamPnt(p);
						String ref = node.getAttrValue("ref");
						if(!StringUtils.isBlank(ref)) {
							refHandlers.put(new SetPntRefRefHandler(pnts, pnt, p.getPropertyName()), ref);
						} else if(!pnts.containsKey(p.getPropertyName())) {
							pnts.put(p.getPropertyName(), pnt);
						}
					} else if(node.getTag().equalsIgnoreCase("cursor"))
						arguments.add(Call.readCursor(node));
					else
						throw new ConvertException("Unrecognized element '" + node.getTag() + "'", Argument.class, node);
				}
			}
		} catch(ConvertException e) {
			throw new LoadingException(e);
		} catch(SAXException e) {
			throw new LoadingException(e);
		} finally {
			ch.closeConnection();
		}
		call.setArguments(arguments.toArray(new Argument[arguments.size()]));

		return call;
	}

	protected Col addCol(Tab tab, ObjectBuilder colBuilder, Map<RefHandler, String> refHandlers, boolean optional) throws LoadingException {
		if(optional) {
			String colname = colBuilder.getAttrValue("name");
			if(tab.hasCol(colname)) {
				log.info("Col '" + colname + "' already exists in tab '" + tab.name + "'");
				return null;
			}
		}
		final Col col;
		try {
			col = new Col(tab, colBuilder);
		} catch(ConvertException e) {
			throw new LoadingException(e);
		}
		String ref = colBuilder.getAttrValue("ref");
		if(!StringUtils.isBlank(ref))
			refHandlers.put(new SetRefRefHandler(col), ref);
		return col;
	}

	protected Ind addInd(Tab tab, ObjectBuilder indBuilder) throws LoadingException {
		Ind ind = new Ind(tab, indBuilder);
		tab.getIndexes().add(ind);
		return ind;
	}

	protected String getBuilderLocation(ObjectBuilder builder) {
		if(builder.getCurrentLocation() == null)
			return "";
		return " at Line " + builder.getCurrentLocation().getStartLine() + ", Character " + builder.getCurrentLocation().getStartColumn();
	}

	public int getBuildNumber() {
		return buildNumber;
	}

	public void setBuildNumber(int buildNumber) {
		this.buildNumber = buildNumber;
	}

}
