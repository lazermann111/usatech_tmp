package com.usatech.etl;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.db.Argument;
import simple.db.Call;
import simple.db.Column;
import simple.db.ConnectionGroup;
import simple.db.Cursor;
import simple.db.DataLayerException;
import simple.db.Parameter;
import simple.db.ParameterException;
import simple.io.Log;
import simple.results.Results;
import simple.sql.ArraySQLType;
import simple.sql.SQLType;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveHashMap;
import simple.util.CollectionUtils;
import simple.util.IncludingMap;
import simple.util.concurrent.AbstractFutureCache;
import simple.util.concurrent.LockSegmentFutureCache;
import simple.xml.ObjectBuilder;


class DataLoaderTab extends Tab implements PntLookup {
	private static final Log log = Log.getLog();
	final static Object PLACEHOLDER = new Object();
	protected final DataLoader dataLoader;
	protected final ConcurrentMap<Ent, SourceInfo> sources = new ConcurrentHashMap<Ent, SourceInfo>();
	protected final ConcurrentMap<Set<String>, Call> deleteCalls = new ConcurrentHashMap<Set<String>, Call>();
	protected final ConcurrentMap<UpdateCallKey, Call> updateCalls = new ConcurrentHashMap<UpdateCallKey, Call>();
	protected final AtomicReference<Call> standardUpsertCall = new AtomicReference<Call>();
	protected final AtomicReference<Call> lookupCall = new AtomicReference<Call>();
	protected final AtomicReference<Call> statsCall = new AtomicReference<Call>();
	protected final AtomicReference<Call> allExistingCall = new AtomicReference<Call>();
	protected final AbstractFutureCache<Map<String, Object>, Long> lookupCache;

	protected class UpdateCallKey {
		public final Set<String> filterKeys;
		public final Set<String> updateKeys;
		public final boolean returnPk;

		public UpdateCallKey(Set<String> filterKeys, Set<String> updateKeys, boolean returnPk) {
			super();
			this.filterKeys = filterKeys;
			this.updateKeys = updateKeys;
			this.returnPk = returnPk;
		}

		@Override
		public int hashCode() {
			return filterKeys.hashCode() + updateKeys.hashCode() * 31;
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof UpdateCallKey))
				return false;
			UpdateCallKey uck = (UpdateCallKey) obj;
			return uck.filterKeys.equals(filterKeys) && uck.updateKeys.equals(updateKeys) && uck.returnPk == returnPk;
		}
	}

	public class SourceInfo implements PntLookup {
		public final Ent sourceEnt;
		public final SourceType sourceType;
		public final Namer namer;
		public final Map<String, Pnt> colnames;
		public final ConcurrentMap<Set<String>, Call> sourceCalls = new ConcurrentHashMap<Set<String>, Call>();

		public SourceInfo(Ent sourceEnt, SourceType sourceType, Namer namer, Map<String, Pnt> colnames) {
			this.sourceEnt = sourceEnt;
			this.sourceType = sourceType;
			this.namer = namer;
			this.colnames = colnames;
		}

		@Override
		public Pnt getPnt(String colname) {
			return colnames == null ? null : colnames.get(namer.getName(colname, NameType.COLUMN));
		}
	}

	public DataLoaderTab(DataLoader dataLoader, ObjectBuilder tabBuilder) {
		super(tabBuilder);
		this.dataLoader = dataLoader;
		this.lookupCache = createLookupCache(dataLoader);
	}

	protected DataLoaderTab(DataLoader dataLoader, String name, ObjectBuilder tabBuilder) {
		super(name, tabBuilder);
		this.dataLoader = dataLoader;
		this.lookupCache = createLookupCache(dataLoader);
	}

	protected AbstractFutureCache<Map<String, Object>, Long> createLookupCache(DataLoader dataLoader) {
		final AbstractFutureCache<Map<String, Object>, Long> lookupCache = new LockSegmentFutureCache<Map<String, Object>, Long>(dataLoader.getLookupCacheSize(), 8, 0.75f, dataLoader.getLookupCacheConcurrency()) {
			@SuppressWarnings("unchecked")
			@Override
			protected Long createValue(final Map<String, Object> key, Object... additionalInfo) throws ServiceException {
				long revision = (Long) additionalInfo[0];
				String source = (String) additionalInfo[1];
				Map<String, Object> sourceValues = (Map<String, Object>) additionalInfo[2];
				ConnectionGroup connGroup = (ConnectionGroup) additionalInfo[3];
				List<Long> ids = (List<Long>) additionalInfo[4];
				return doLookupRow(revision, source, sourceValues, connGroup, ids);
			}
		};
		lookupCache.setExpireTime(0); // don't expire
		lookupCache.setTimeout(dataLoader.getQueryTimeout() * 2);
		return lookupCache;
	}

	public Call getDeleteCall(Map<String, Object> sourceValues, ConnectionGroup connGroup) throws ServiceException, SQLException {
		DeleteType deleteType = this.deleteType;
		if(deleteType == null)
			return null;
		Map<String, String> keys = checkKeys(sourceValues, true);
		Call deleteCall = deleteCalls.get(keys.keySet());
		if(deleteCall == null) {
			Namer namer = dataLoader.getTargetNamer(connGroup);
			boolean first = true;
			StringBuilder sb = new StringBuilder();
			List<Argument> args = new ArrayList<Argument>();
			Parameter p = new Parameter();
			p.setPropertyName(dataLoader.getRowCountLabel());
			p.setIn(false);
			p.setOut(true);
			args.add(p);
			switch(deleteType) {
				case DELETE:
					sb.append("DELETE FROM ");
					break;
				case UPDATE_FLAG:
					sb.append("UPDATE ");
					break;
			}
			if(!StringUtils.isBlank(schema))
				namer.appendExpression(sb, schema, NameType.SCHEMA).append('.');
			namer.appendExpression(sb, name, NameType.TABLE).append(" U");
			switch(deleteType) {
				case UPDATE_FLAG:
					sb.append(" SET ");
					if(revisionPnt != null) {
						namer.appendExpression(sb, revisionPnt.getName(), NameType.COLUMN).append(" = ?, ");
						p = new Parameter();
						p.setPropertyName(dataLoader.getRevisionLabel());
						p.setSqlType(revisionPnt.getSQLType());
						p.setIn(true);
						args.add(p);
					}
					first = true;
					for(Col col : getCols()) {
						if(col.deleteValue != null) {
							if(first)
								first = false;
							else
								sb.append(", ");
							namer.appendExpression(sb, col.name, NameType.COLUMN).append(" = ?");
							p = new Parameter();
							p.setDefaultValue(col.deleteValue);
							p.setSqlType(col.sqlType);
							p.setIn(true);
							args.add(p);
						}
					}
					if(first)
						throw new RetrySpecifiedServiceException("Tab '" + name + "' has a deleteType of 'UPDATE_FLAG' but no deleteValues are specified on any cols", WorkRetryType.NO_RETRY);
					break;
			}
			first = true;
			if(revisionPnt != null) {
				sb.append(" WHERE ");
				namer.appendExpression(sb, revisionPnt.getName(), NameType.COLUMN).append(" < ? AND ");
				p = new Parameter();
				p.setPropertyName(dataLoader.getRevisionLabel());
				p.setSqlType(revisionPnt.getSQLType());
				p.setIn(true);
				args.add(p);
				first = false;
			}
			appendWhereClause(sb, keys, this, dataLoader.getTargetDsn(), connGroup, dataLoader.getTargetNamer(connGroup), args, first);

			deleteCall = new Call();
			deleteCall.setSql(sb.toString());
			sb.setLength(0);
			deleteCall.setDataSourceName(dataLoader.getTargetDsn());
			sb.append("DELETE_");
			if(!StringUtils.isBlank(schema))
				sb.append(schema).append('_');
			sb.append(name);
			deleteCall.setLabel(sb.toString());
			deleteCall.setArguments(args.toArray(new Argument[args.size()]));
			deleteCall.setLogExecuteTimeThreshhold(dataLoader.getLogExecuteTimeThreshhold());
			deleteCall.setQueryTimeout(dataLoader.getQueryTimeout());
			Call stored = deleteCalls.putIfAbsent(keys.keySet(), deleteCall);
			if(stored != null)
				return stored;
			deleteCall.initialize();
			if(log.isInfoEnabled())
				log.info("Created call '" + deleteCall.getLabel() + "':\n\t" + deleteCall.getSql());
		}
		return deleteCall;
	}

	protected String findLongName(String name, Namer namer) {
		// Try to find the a column in this tab that matches this name exactly or the shortened name
		if(getCol(name) == null) {
			for(Col col : getCols()) {
				if(col.getRefTab() != null) {
					for(Col fkCol : col.getRefTab().getIdentityCols()) {
						String longName = col.getName() + '$' + fkCol.getName();
						if(name.equalsIgnoreCase(namer.getName(longName, NameType.COLUMN)))
							return longName;
					}
				} else if(name.equalsIgnoreCase(namer.getName(col.getName(), NameType.COLUMN))) {
					return col.getName();
				}
			}
		}
		return name;
	}

	protected Map<String, Pnt> getSelectableColNames(Connection conn, Ent sourceEnt, Namer namer) throws SQLException {
		ResultSet sourceCols = conn.getMetaData().getColumns(namer.getName(sourceEnt.getCatalog(), NameType.CATALOG), namer.getName(sourceEnt.getSchema(), NameType.SCHEMA), namer.getName(sourceEnt.getTable(), NameType.TABLE), "%");
		try {
			if(sourceCols.next()) {
				Map<String, Pnt> colnames = new CaseInsensitiveHashMap<Pnt>();
				do {
					String name = sourceCols.getString(4).toUpperCase();
					boolean required = (sourceCols.getInt(11) == DatabaseMetaData.columnNoNulls);
					int typeCode = sourceCols.getInt(5);
					String typeName = sourceCols.getString(6);
					Integer precision = sourceCols.getInt(7);
					if(sourceCols.wasNull())
						precision = null;
					Integer scale = sourceCols.getInt(9);
					if(sourceCols.wasNull())
						scale = null;
					// name = findLongName(name, namer);
					colnames.put(name, new BasicPnt(name, new SQLType(typeCode, typeName, precision, scale), required, true, true));
				} while(sourceCols.next());
				return colnames;
			}
		} finally {
			sourceCols.close();
		}
		return null;
	}

	protected Map<String, Pnt> getExecutableColNames(Connection conn, Ent sourceEnt, Namer namer) throws SQLException {
		ResultSet sourceCols = conn.getMetaData().getProcedureColumns(namer.getName(sourceEnt.getCatalog(), NameType.CATALOG), namer.getName(sourceEnt.getSchema(), NameType.SCHEMA), namer.getName(sourceEnt.getTable(), NameType.PROCEDURE), "%");
		try {
			if(sourceCols.next()) {
				Map<String, Pnt> colnames = new CaseInsensitiveHashMap<Pnt>();
				do {
					String name = sourceCols.getString(4);
					if(!sourceCols.wasNull())
						name = name.toUpperCase();
					boolean required = (sourceCols.getInt(12) == DatabaseMetaData.columnNoNulls);
					int typeCode = sourceCols.getInt(6);
					String typeName = sourceCols.getString(7);
					Integer precision = sourceCols.getInt(8);
					if(sourceCols.wasNull())
						precision = null;
					Integer scale = sourceCols.getInt(10);
					if(sourceCols.wasNull())
						scale = null;
					int direction = sourceCols.getInt(5);
					boolean input;
					boolean output;
					switch(direction) {
						case DatabaseMetaData.procedureColumnIn:
							input = true;
							output = false;
							break;
						case DatabaseMetaData.procedureColumnOut:
						case DatabaseMetaData.procedureColumnResult:
						case DatabaseMetaData.procedureColumnReturn:
							input = false;
							output = true;
							break;
						case DatabaseMetaData.procedureColumnInOut:
						case DatabaseMetaData.procedureColumnUnknown:
						default:
							input = true;
							output = true;
							break;
					}
					// name = findLongName(name, namer);
					colnames.put(name, new BasicPnt(name, new SQLType(typeCode, typeName, precision, scale), required, input, output));
				} while(sourceCols.next());
				return colnames;
			}
		} finally {
			sourceCols.close();
		}
		return null;
	}

	public SourceInfo getSourceInfo(String source, ConnectionGroup connGroup) throws SQLException {
		final Ent sourceEnt;
		SourceType sourceType;
		if(StringUtils.isBlank(source) || (this.sourceEnt != null && this.sourceEnt.toString().equals(source))) {
			sourceEnt = this.sourceEnt;
			sourceType = this.sourceType;
		} else {
			sourceEnt = new Ent(source);
			sourceType = null;
		}
		SourceInfo si = sources.get(sourceEnt);
		if(si != null)
			return si;
		Map<String, Pnt> colnames;
		Namer namer;
		if(sourceType == SourceType.NONE) {
			colnames = null;
			namer = null;
		} else {
			Connection sourceConn;
			try {
				sourceConn = connGroup.getConnection(sourceEnt.getDsn());
			} catch(DataLayerException e) {
				throw new SQLException(e);
			}
			namer = new Namer(sourceConn.getMetaData());
			if(sourceType == null || sourceType == SourceType.UNKNOWN) {
				if((colnames = getSelectableColNames(sourceConn, sourceEnt, namer)) != null)
					sourceType = SourceType.SELECTABLE;
				else if((colnames = getExecutableColNames(sourceConn, sourceEnt, namer)) != null)
					sourceType = SourceType.EXECUTABLE;
				else {
					// sourceType = SourceType.UNKNOWN;
					StringBuilder sb = new StringBuilder();
					sb.append("Could not determine source type for '").append(sourceEnt).append("'");
					throw new SQLException(sb.toString(), "42602");
				}
			} else
				switch(sourceType) {
					case SELECTABLE:
						if((colnames = getSelectableColNames(sourceConn, sourceEnt, namer)) == null)
							throw new SQLException("Could not find table or view '" + sourceEnt + "'", "42602");
						break;
					case EXECUTABLE:
						if((colnames = getExecutableColNames(sourceConn, sourceEnt, namer)) == null)
							throw new SQLException("Could not find procedure or function '" + sourceEnt + "'", "42602");
						break;
					default:
						colnames = null;

				}
		}
		si = new SourceInfo(sourceEnt, sourceType, namer, colnames == null ? null : Collections.unmodifiableMap(colnames));
		SourceInfo old = sources.putIfAbsent(sourceEnt, si);
		if(old != null)
			return old;
		return si;
	}

	public void clearSources() {
		sources.clear();
	}

	protected boolean containsAllColNames(Set<String> colnames, Namer namer) {
		switch(upsertType) {
			case BOTH:
			case CUSTOM:
				// get refs
				Set<String> refnames = new HashSet<String>();
				for(String colname : colnames) {
					int pos = colname.indexOf('$');
					if(pos > 0)
						refnames.add(colname.substring(0, pos));
				}
				// check params
				int k = 0;
				for(int i = 0; k < customUpsertCall.getInCount(); i++) {
					Argument argument = customUpsertCall.getArgument(i);
					if(argument.isIn()) {
						String propname = argument.getPropertyName();
						if(!colnames.contains(propname) && !refnames.contains(propname))
							return false;
						k++;
					}
				}
				if(upsertType == UpsertType.CUSTOM)
					break;
			case STANDARD:
				for(Col col : getCols()) {
					if(col.getRefTab() != null) {
						StringBuilder sb = new StringBuilder(dataLoader.getMaxNameLength());
						namer.appendExpression(sb, col.name, NameType.COLUMN).append('$');
						int pos = sb.length();
						for(Col refCol : col.getRefTab().getIdentityCols()) {
							sb.setLength(pos);
							namer.appendExpression(sb, refCol.name, NameType.COLUMN);
							if(!colnames.contains(namer.getName(sb.toString(), NameType.COLUMN)))
								return false;
						}
					} else if(!colnames.contains(namer.getName(col.name, NameType.COLUMN)))
						return false;
				}
				break;
		}

		return true;
	}

	protected void addColumn(Pnt target, SourceInfo sourceInfo, Map<String, Column> columns, AtomicInteger index) {
		if(columns.containsKey(target.getName()))
			return;
		String colname = sourceInfo.namer.getName(target.getName(), NameType.COLUMN);
		Pnt pnt = sourceInfo.colnames.get(colname);
		if(pnt != null) {
			if(pnt.isOutput()) {
				Column column = new Column();
				column.setPropertyName(target.getName());
				column.setSqlType(target.getSQLType().getTypeCode());
				if(index != null)
					column.setIndex(index.getAndIncrement());
				else
					column.setName(colname);
				columns.put(target.getName(), column);
			}
		} else if(target.getRefCol() != null) {
			for(Col refCol : target.getRefCol().tab.getIdentityCols()) {
				String propName = target.getName() + '$' + refCol.name;
				if(columns.containsKey(propName))
					continue;
				colname = sourceInfo.namer.getName(propName, NameType.COLUMN);
				pnt = sourceInfo.colnames.get(colname);
				if(pnt != null && pnt.isOutput()) {
					Column column = new Column();
					column.setPropertyName(propName);
					column.setSqlType(refCol.sqlType.getTypeCode());
					if(index != null)
						column.setIndex(index.getAndIncrement());
					else
						column.setName(colname);
					columns.put(propName, column);
				}
			}
		}
	}
	public Call getSourceCall(String source, Map<String, Object> sourceValues, ConnectionGroup connGroup) throws SQLException {
		// Create source call based on provided values
		final SourceInfo sourceInfo = getSourceInfo(source, connGroup);
		if(sourceInfo.sourceType == SourceType.NONE 
			 || (sourceInfo.sourceType == SourceType.SELECTABLE && sourceValues.keySet().containsAll(sourceInfo.colnames.keySet()))
			 || containsAllColNames(sourceValues.keySet(), sourceInfo.namer))
			return null; // no call necessary - all data provided
		Map<String, String> keys = checkSourceKeys(sourceValues, sourceInfo, connGroup);

		Call sourceCall = sourceInfo.sourceCalls.get(keys);
		if(sourceCall == null) {
			StringBuilder sb = new StringBuilder();
			List<Argument> args = new ArrayList<Argument>();
			Cursor cur = new Cursor();
			// cur.setAllColumns(true);
			// /*
			Map<String, Column> columns = new LinkedHashMap<String, Column>(sourceInfo.colnames.size());
			AtomicInteger index = dataLoader.isStrictSourceColumnOrder() ? new AtomicInteger(1) : null;
			switch(upsertType) {
				case BOTH:
				case STANDARD:
					for(Col col : getCols()) {
						switch(col.colType) {
							case PRIMARY_KEY:
							case IDENTITY_KEY:
							case DATA:
								addColumn(col, sourceInfo, columns, index);
								break;
						}
					}
					if(upsertType == UpsertType.STANDARD)
						break;
				case CUSTOM:
					for(Pnt target : getCustomUpsertPnts())
						addColumn(target, sourceInfo, columns, index);
					break;
			}
			args.add(cur);
			Col col;
			Parameter p;
			boolean first;
			switch(sourceInfo.sourceType) {
				case SELECTABLE:
					cur.setColumns(columns.values().toArray(new Column[columns.size()]));
					sb.append("SELECT ");
					first = true;
					for(Column column : columns.values()) {
						if(first)
							first = false;
						else
							sb.append(", ");
						sourceInfo.namer.appendExpression(sb, column.getName(), NameType.COLUMN);
					}
					sb.append(" FROM ");
					if(!StringUtils.isBlank(sourceInfo.sourceEnt.getSchema()))
						sb.append(sourceInfo.sourceEnt.getSchema()).append('.');
					if(!StringUtils.isBlank(sourceInfo.sourceEnt.getCatalog()))
						sb.append(sourceInfo.sourceEnt.getCatalog()).append('.');
					sb.append(sourceInfo.sourceEnt.getTable());
					boolean usesRange = appendWhereClause(sb, keys, sourceInfo, sourceInfo.sourceEnt.getDsn(), connGroup, sourceInfo.namer, args, true);
					if(usesRange && dataLoader.isLazyAccessOnRangeSelect())
						cur.setLazyAccess(true);
					break;
				case EXECUTABLE:
					String suffix;
					if(DataLoader.useSelectForCall(sourceInfo.sourceEnt.getDsn(), connGroup)) {
						if(columns.isEmpty()) {
							cur.setAllColumns(true);
							sb.append("SELECT * ");
						} else {
							cur.setColumns(columns.values().toArray(new Column[columns.size()]));
							sb.append("SELECT ");
							first = true;
							for(Column column : columns.values()) {
								if(first)
									first = false;
								else
									sb.append(", ");
								sourceInfo.namer.appendExpression(sb, column.getName(), NameType.COLUMN);
							}
						}
						sb.append(" FROM ");
						suffix = "";
					} else {
						if(columns.isEmpty())
							cur.setAllColumns(true);
						else
							cur.setColumns(columns.values().toArray(new Column[columns.size()]));
						p = new Parameter();
						p.setIn(false);
						p.setOut(true);
						args.add(0, p);
						sb.append("{? = call ");
						suffix = "}";
					}
					if(!StringUtils.isBlank(sourceInfo.sourceEnt.getSchema()))
						sb.append(sourceInfo.sourceEnt.getSchema()).append('.');
					if(!StringUtils.isBlank(sourceInfo.sourceEnt.getCatalog()))
						sb.append(sourceInfo.sourceEnt.getCatalog()).append('.');
					sb.append(sourceInfo.sourceEnt.getTable()).append("(");
					first = true;
					for(String colname : uniqueKeyNames) {
						if(first)
							first = false;
						else
							sb.append(", ");
						switch(colname.charAt(0)) {
							case '*':
							case '\\':
								colname = colname.substring(1);
						}

						col = getCol(colname);
						p = new Parameter();
						p.setPropertyName(colname);
						p.setSqlType(col.sqlType);
						p.setIn(true);
						sb.append("?");
						p.setRequired(col.required);
						args.add(p);
					}
					sb.append(')').append(suffix);
					break;
				default:
					sb.append("Source Type ").append(sourceInfo.sourceType).append(" is not implemented; Cannot get data from '");
					if(!StringUtils.isBlank(sourceInfo.sourceEnt.getSchema()))
						sb.append(sourceInfo.sourceEnt.getSchema()).append('.');
					if(!StringUtils.isBlank(sourceInfo.sourceEnt.getCatalog()))
						sb.append(sourceInfo.sourceEnt.getCatalog()).append('.');
					sb.append(sourceInfo.sourceEnt.getTable()).append("'");
					throw new SQLException(sb.toString());
			}

			sourceCall = new Call();
			sourceCall.setSql(sb.toString());
			sourceCall.setDataSourceName(sourceInfo.sourceEnt.getDsn());
			sb.setLength(0);
			sb.append("SELECT_");
			if(!StringUtils.isBlank(sourceInfo.sourceEnt.getSchema()))
				sb.append(sourceInfo.sourceEnt.getSchema()).append('_');
			if(!StringUtils.isBlank(sourceInfo.sourceEnt.getCatalog()))
				sb.append(sourceInfo.sourceEnt.getCatalog()).append('_');
			sb.append(sourceInfo.sourceEnt.getTable());
			if(!keys.keySet().equals(uniqueKeyNames))
				StringUtils.appendHex(sb.append('_'), keys.keySet().hashCode());

			sourceCall.setLabel(sb.toString());
			sourceCall.setArguments(args.toArray(new Argument[args.size()]));
			sourceCall.setLogExecuteTimeThreshhold(dataLoader.getLogExecuteTimeThreshhold());
			sourceCall.setQueryTimeout(dataLoader.getQueryTimeout());

			Call stored = sourceInfo.sourceCalls.putIfAbsent(keys.keySet(), sourceCall);
			if(stored != null)
				return stored;
			sourceCall.initialize();
			if(log.isInfoEnabled())
				log.info("Created call '" + sourceCall.getLabel() + "':\n\t" + sourceCall.getSql());
		}
		return sourceCall;
	}

	protected boolean checkNotNull(Map<String, Object> sourceValues, Set<String> keyNames) {
		for(String key : keyNames) {
			Col col;
			if(sourceValues.get(key) == null && (col = getCol(key)) != null && !col.required)
				return false;
		}
		return true;
	}

	public Call buildUpsertCall(ConnectionGroup connGroup) throws SQLException {
		Namer namer = dataLoader.getTargetNamer(connGroup);
		StringBuilder sb = new StringBuilder();
		sb.append("{call ");
		if(!StringUtils.isBlank(schema))
			namer.appendExpression(sb, schema, NameType.SCHEMA).append('.');
		namer.appendExpression(sb, "UPSERT_" + name, NameType.PROCEDURE).append('(');
		List<Argument> args = new ArrayList<Argument>();
		// First param is update count
		Parameter p = new Parameter();
		p.setIn(false);
		p.setOut(true);
		args.add(p);
		OUTER: for(Col col : getCols()) {
			switch(col.colType) {
				case PRIMARY_KEY:
					if(!uniqueKeyNames.isEmpty()) {
						p = new Parameter();
						p.setOut(col.getRefTab() == null);
						p.setIn(col.getRefTab() != null);
						break;
					}
				case IDENTITY_KEY:
				case DATA:
					p = new Parameter();
					p.setIn(true);
					break;
				default:
					continue OUTER;
			}
			p.setPropertyName(col.name);
			p.setSqlType(col.sqlType);
			if(col.required && !StringUtils.isBlank(col.defaultSql)) {
				if(dataLoader.isDefaultOnUpsert())
					sb.append("COALESCE(?, ").append(col.defaultSql).append("),");
				else
					sb.append('?').append(',');
				p.setRequired(false);
			} else {
				sb.append('?').append(',');
				p.setRequired(col.required);
			}
			args.add(p);
		}
		if(revisionPnt != null) {
			sb.append("?,");
			p = new Parameter();
			p.setPropertyName(dataLoader.getRevisionLabel());
			p.setSqlType(revisionPnt.getSQLType());
			p.setOut(true);
			p.setIn(true);
			args.add(p);
		}
		sb.append("?)}");
		p = new Parameter();
		p.setPropertyName(dataLoader.getRowCountLabel());
		p.setSqlType(dataLoader.getRowCountSqlType());
		p.setIn(false);
		p.setOut(true);
		args.add(p);
		Call call = new Call();
		call.setSql(sb.toString());
		sb.setLength(0);
		sb.append("UPSERT_");
		if(!StringUtils.isBlank(schema))
			sb.append(schema).append('_');
		sb.append(name);
		call.setLabel(sb.toString());
		call.setArguments(args.toArray(new Argument[args.size()]));
		if(log.isInfoEnabled())
			log.info("Created call '" + call.getLabel() + "':\n\t" + call.getSql());
		return call;
	}

	protected void prepareCall(Call upsertCall) {
		upsertCall.setDataSourceName(dataLoader.getTargetDsn());
		upsertCall.setLogExecuteTimeThreshhold(dataLoader.getLogExecuteTimeThreshhold());
		upsertCall.setQueryTimeout(dataLoader.getQueryTimeout());
		upsertCall.initialize();
	}

	public Call getStandardUpsertCall(ConnectionGroup connGroup) throws SQLException {
		Call call = standardUpsertCall.get();
		switch(getUpsertType()) {
			case STANDARD:
			case BOTH:
				if(call == null) {
					call = buildUpsertCall(connGroup);
					if(!standardUpsertCall.compareAndSet(null, call))
						return getStandardUpsertCall(connGroup);
					prepareCall(call);
					return call;
				}
		}

		return call;
	}

	public Call getLookupCall(ConnectionGroup connGroup) throws SQLException {
		if(primaryKeyNames.isEmpty())
			return null;
		Call call = lookupCall.get();
		if(call == null) {
			Namer namer = dataLoader.getTargetNamer(connGroup);
			StringBuilder sb = new StringBuilder();
			List<Argument> args = new ArrayList<Argument>();
			Cursor cur = new Cursor();
			boolean first = true;
			sb.append("SELECT ");
			List<Column> columns = new ArrayList<Column>();
			for(Col col : getPrimaryKeyCols()) {
				if(first)
					first = false;
				else
					sb.append(", ");
				namer.appendExpression(sb, col.name, NameType.COLUMN);
				Column column = new Column();
				column.setPropertyName(col.name);
				// column.setName(getName(col.name));
				column.setSqlType(col.sqlType.getTypeCode());
				column.setIndex(1);
				columns.add(column);
			}
			cur.setColumns(columns.toArray(new Column[columns.size()]));
			args.add(cur);
			first = true;
			sb.append(" FROM ");
			if(!StringUtils.isBlank(schema))
				namer.appendExpression(sb, schema, NameType.SCHEMA).append('.');
			namer.appendExpression(sb, name, NameType.TABLE).append(" WHERE ");
			for(Col col : getUniqueKeyCols()) {
				if(first)
					first = false;
				else
					sb.append(" AND ");
				int pc;
				if(col.required) {
					namer.appendExpression(sb, col.name, NameType.COLUMN).append(" = ?");
					pc = 1;
				} else
					pc = DataLoader.appendNotDistinctFromExpr(dataLoader.getTargetDsn(), connGroup, sb, col.name);
				Parameter p = new Parameter();
				p.setPropertyName(col.name);
				p.setSqlType(col.sqlType);
				p.setIn(true);
				p.setRequired(col.required);
				for(int i = 0; i < pc; i++)
					args.add(p);
			}

			call = new Call();
			call.setSql(sb.toString());
			sb.setLength(0);
			call.setDataSourceName(dataLoader.getTargetDsn());
			sb.append("LOOKUP_");
			if(!StringUtils.isBlank(schema))
				sb.append(schema).append('_');
			sb.append(name);
			call.setLabel(sb.toString());
			call.setArguments(args.toArray(new Argument[args.size()]));
			call.setLogExecuteTimeThreshhold(dataLoader.getLogExecuteTimeThreshhold());
			call.setQueryTimeout(dataLoader.getQueryTimeout());
			if(!lookupCall.compareAndSet(null, call))
				return getLookupCall(connGroup);
			call.initialize();
			if(log.isInfoEnabled())
				log.info("Created call '" + call.getLabel() + "':\n\t" + call.getSql());
		}
		return call;
	}
	public boolean isDefaultRow(Map<String, Object> sourceValues, DataLoader dataLoader) throws ServiceException {
		if(!sourceValues.keySet().containsAll(uniqueKeyNames))
			return false;
		for(String colname : uniqueKeyNames) {
			Col col = colsByName.get(colname);
			Object value = sourceValues.get(colname);
			if(!dataLoader.isDefaultValue(col, value))
				return false;
		}
		return true;
	}

	public Long lookupRow(long revision, String source, Map<String, Object> sourceValues, ConnectionGroup connGroup, List<Long> ids, DataLoader dataLoader) throws ServiceException {
		Map<String, Object> key;
		if(sourceValues.keySet().equals(uniqueKeyNames))
			key = sourceValues;
		else
			key = new IncludingMap<String, Object>(sourceValues, uniqueKeyNames);
		Long id;
		try {
			id = lookupCache.getOrCreate(key, revision, source, sourceValues, connGroup, ids, dataLoader);
			if(id == null)
				id = lookupCache.expireAndGet(key, revision, source, sourceValues, connGroup, ids, dataLoader);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof ServiceException)
				throw (ServiceException) e.getCause();
			throw new ServiceException(e);
		}
		return id;
	}

	protected Long doLookupRow(long revision, String source, Map<String, Object> sourceValues, ConnectionGroup connGroup, List<Long> ids) throws ServiceException {
		// Try a lookup first to avoid grabbing all source data first
		try {
			Call call = getLookupCall(connGroup);
			if(call != null) {
				boolean okay = false;
				Connection targetConn = connGroup.getConnection(dataLoader.getTargetDsn());
				try {
					Results lookupResults = call.executeQuery(targetConn, sourceValues, null, null);
					if(lookupResults.next())
						try {
							return lookupResults.getValue(1, Long.class);
						} catch(ConvertException e) {
							throw new ServiceException(e);
						}
					okay = true;
				} finally {
					if(!targetConn.getAutoCommit()) {
						if(okay)
							targetConn.commit();
						else
							targetConn.rollback();
					}
				}
			}
		} catch(ParameterException e) {
			throw new ServiceException("While looking up row in target table '" + name + "' with params " + sourceValues, e);
		} catch(SQLException e) {
			throw new ServiceException("While looking up row in target table '" + name + "' with params " + sourceValues, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not obtain target connection  '" + dataLoader.getTargetDsn() + "'", e);
		}
		// resort to getting from source and updating in target if necessary
		return dataLoader.loadRow(revision, this, source, sourceValues, connGroup, ids);
	}

	public void setCustomUpsertCall(Call customUpsertCall, Map<String, Pnt> customUpsertPnts) {
		super.setCustomUpsertCall(customUpsertCall, customUpsertPnts);
		prepareCall(customUpsertCall);
	}

	public Call getTargetStatsCall(int minDistinctForRange, ConnectionGroup connGroup) throws SQLException {
		Call call = statsCall.get();
		if(call == null) {
			Namer namer = dataLoader.getTargetNamer(connGroup);
			StringBuilder sb = new StringBuilder();
			List<Column> columns = new ArrayList<Column>();
			List<Argument> arguments = new ArrayList<Argument>();
			Cursor cur = new Cursor();
			arguments.add(cur);
			Parameter parameter = new Parameter();
			parameter.setDefaultValue(minDistinctForRange);
			parameter.setSqlType(new SQLType(Types.BIGINT));
			sb.append("SELECT COUNT(*)");
			Column column = new Column();
			column.setPropertyName("count");
			int index = 1;
			column.setIndex(index++);
			columns.add(column);
			for(String colname : uniqueKeyNames) {
				sb.append(", COUNT(DISTINCT ").append(colname).append("), CASE WHEN COUNT(DISTINCT ")
					.append(colname).append(") < ? THEN ARRAY_AGG(DISTINCT ").append(colname).append(") END, MAX(")
					.append(colname).append("), MIN(").append(colname).append(')');
				arguments.add(parameter);
				column = new Column();
				column.setPropertyName(colname + ".cardinality");
				column.setIndex(index++);
				columns.add(column);
				column = new Column();
				column.setPropertyName(colname + ".values");
				column.setIndex(index++);
				columns.add(column);
				column = new Column();
				column.setPropertyName(colname + ".max");
				column.setIndex(index++);
				columns.add(column);
				column = new Column();
				column.setPropertyName(colname + ".min");
				column.setIndex(index++);
				columns.add(column);
				
			}
			cur.setColumns(columns.toArray(new Column[columns.size()]));			
			sb.append(" FROM ");
			if(!StringUtils.isBlank(schema))
				namer.appendExpression(sb, schema, NameType.SCHEMA).append('.');
			namer.appendExpression(sb, name, NameType.TABLE);
			
			call = new Call();
			call.setSql(sb.toString());
			sb.setLength(0);
			call.setDataSourceName(dataLoader.getTargetDsn());
			sb.append("STATS_");
			if(!StringUtils.isBlank(schema))
				sb.append(schema).append('_');
			sb.append(name);
			call.setLabel(sb.toString());
			call.setArguments(arguments.toArray(new Argument[arguments.size()]));
			call.setLogExecuteTimeThreshhold(dataLoader.getLogExecuteTimeThreshhold());
			call.setQueryTimeout(dataLoader.getQueryTimeout());
			if(!statsCall.compareAndSet(null, call))
				return getTargetStatsCall(minDistinctForRange, connGroup);
			call.initialize();
			if(log.isInfoEnabled())
				log.info("Created call '" + call.getLabel() + "':\n\t" + call.getSql());
		}
		return call;
	}

	protected Set<String> retainUpdateColNames(Set<String> keys) {
		Set<String> copy = null;
		for(String colname : keys) {
			Col col = getCol(colname);
			if(col == null || (col.colType != ColType.DATA && col.colType != ColType.CUSTOM && (col.colType != ColType.IDENTITY_KEY || col.tab == this || col.tab.isDescendantOf(this)))) {
				if(copy == null)
					copy = new HashSet<String>(keys);
				copy.remove(colname);
			}
		}
		return copy == null ? keys : copy;
	}

	protected Map<String,String> checkSourceKeys(Map<String, Object> filterValues, SourceInfo sourceInfo, ConnectionGroup connGroup) throws SQLException {
		if(!uniqueKeyNames.isEmpty() && filterValues.keySet().containsAll(uniqueKeyNames) && checkNotNull(filterValues, uniqueKeyNames))
			return CollectionUtils.sameValueMap(uniqueKeyNames);
		Map<String,String> keys = new HashMap<String,String>(filterValues.size());
		switch(sourceInfo.sourceType) {
			case SELECTABLE:
				for(Map.Entry<String, Object> entry : filterValues.entrySet()) {
					String colname = entry.getKey();
					char ch = colname.charAt(0);
					if(!Character.isLetterOrDigit(ch))
						colname = colname.substring(1);
					Pnt pnt = sourceInfo.getPnt(colname);
					if(pnt == null)
						continue;
					if(pnt.isInput())
						switch(ch) {
							case '<':
							case '>':
							case '+':
							case '!':
							case '^':
							case '*':
							case '?':
								keys.put(entry.getKey(), entry.getKey());
								break;
							case '=':
							case '\\':
							default:
								if(entry.getValue() == null)
									colname = '*' + colname;
								else if(entry.getValue() == PLACEHOLDER)
									colname = '?' + colname;
								else if(!Character.isLetterOrDigit(colname.charAt(0)))
									colname = '\\' + colname;
								keys.put(colname, entry.getKey());
								break;
						}
				}
				return keys;
			case EXECUTABLE:
				for(Map.Entry<String, Pnt> entry : sourceInfo.colnames.entrySet()) {
					if(entry.getValue().isInput()) {
						if(entry.getValue().isRequired() && !filterValues.containsKey(entry.getKey()))
							throw new SQLException("Only the full set of input keys is valid for filterColumns");
						keys.put(entry.getKey(), entry.getKey());
					}
				}
				return keys;
			default:
				throw new SQLException("Only the full set of unique keys is valid for filterColumns");
		}
	}

	protected Map<String, String> checkKeys(Map<String, Object> filterValues, boolean allowAny) throws SQLException {
		if(!uniqueKeyNames.isEmpty() && filterValues.keySet().containsAll(uniqueKeyNames) && checkNotNull(filterValues, uniqueKeyNames))
			return CollectionUtils.sameValueMap(uniqueKeyNames);
		if(!allowAny)
			throw new SQLException("Only the full set of unique keys is valid for filterColumns");
		Map<String, String> keys = new HashMap<String, String>(filterValues.size());
		for(Map.Entry<String, Object> entry : filterValues.entrySet()) {
			String colname = entry.getKey();
			char ch = colname.charAt(0);
			if(!Character.isLetterOrDigit(ch))
				colname = colname.substring(1);
			Pnt pnt = getPnt(colname);
			if(pnt != null)
				switch(ch) {
					case '<':
					case '>':
					case '+':
					case '!':
					case '^':
					case '*':
					case '?':
						keys.put(entry.getKey(), entry.getKey());
						break;
					case '=':
					case '\\':
					default:
						if(!pnt.isRequired() && entry.getValue() == null)
							colname = '*' + colname;
						else if(!pnt.isRequired() && entry.getValue() == PLACEHOLDER)
							colname = '?' + colname;
						else if(!Character.isLetterOrDigit(colname.charAt(0)))
							colname = '\\' + colname;
						keys.put(colname, entry.getKey());
						break;
				}
		}
		return keys;
	}

	@Override
	public Pnt getPnt(String colname) {
		switch(upsertType) {
			case BOTH:
			case STANDARD:
				Col col = colsByName.get(colname);
				if(col != null)
					return col;
				if(parent != null) {
					col = inheritedColsByName.get(colname);
					if(col != null)
						return col;
				}
				// check ref cols
				String[] parts = StringUtils.split(colname, '$', 1);
				if(parts.length > 1) {
					Col fkCol = colsByName.get(parts[0]);
					if(fkCol != null && fkCol.getRefTab().uniqueKeyNames.contains(parts[1])) {
						Col refCol = fkCol.getRefTab().getCol(parts[1]);
						if(refCol == null)
							return null;
						return new RefPnt(fkCol, refCol);
					}
					fkCol = inheritedColsByName.get(parts[0]);
					if(fkCol != null && fkCol.getRefTab().uniqueKeyNames.contains(parts[1])) {
						Col refCol = fkCol.getRefTab().getCol(parts[1]);
						if(refCol == null)
							return null;
						return new RefPnt(fkCol, refCol);
					}
				}
				if(upsertType == UpsertType.STANDARD)
					break;
			case CUSTOM:
				return getCustomUpsertPnt(colname);
		}
		return null;
	}

	protected boolean isLookup(Col col) {
		if(col.tab == this)
			return false;
		switch(col.colType) {
			case PRIMARY_KEY:
			case IDENTITY_KEY:
				return true;
			case DATA:
				return !isDescendantOf(col.tab);
			default:
				return false;
		}
	}

	protected boolean appendWhereClause(StringBuilder sb, Map<String, String> keys, PntLookup lookup, String dsn, ConnectionGroup connGroup, Namer namer, List<Argument> args, boolean first) throws SQLException {
		boolean usesRange = false;
		Map<String, Map<String, Pnt>> lookups = null;
		Pnt pnt;
		Parameter p;
		for(Map.Entry<String, String> entry : keys.entrySet()) {
			String colname;
			char ch = entry.getKey().charAt(0);
			if(!Character.isLetterOrDigit(ch))
				colname = entry.getKey().substring(1);
			else
				colname = entry.getKey();
			pnt = lookup.getPnt(colname);
			if(pnt == null)
				continue;
			if(pnt.isRef()) {
				if(lookups == null)
					lookups = new LinkedHashMap<String, Map<String, Pnt>>();
				Map<String, Pnt> lookupPnts = lookups.get(pnt.getName());
				if(lookupPnts == null) {
					lookupPnts = new LinkedHashMap<String, Pnt>();
					lookups.put(pnt.getName(), lookupPnts);
				}
				lookupPnts.put(entry.getKey(), pnt);
				continue;
			}
			if(first) {
				first = false;
				sb.append(" WHERE ");
			} else
				sb.append(" AND ");
			switch(ch) {
				case '!': // NOT EQUALS
					if(pnt.isRequired())
						namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" != ?");
					else
						DataLoader.appendDistinctFromExpr(dsn, connGroup, sb, namer.getExpression(pnt.getName(), NameType.COLUMN));
					p = new Parameter();
					p.setPropertyName(entry.getValue());
					p.setSqlType(pnt.getSQLType());
					p.setIn(true);
					p.setRequired(pnt.isRequired());
					args.add(p);
					break;
				case '^': // NOT IN
					namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(' ').append(DataLoader.getNotEqualsAnyExpr(dsn, connGroup));
					p = new Parameter();
					p.setPropertyName(entry.getValue());
					p.setSqlType(new ArraySQLType(pnt.getSQLType()));
					p.setIn(true);
					p.setRequired(pnt.isRequired());
					args.add(p);
					break;
				case '>': // GREATER OR EQUAL
					namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" >= ?");
					p = new Parameter();
					p.setPropertyName(entry.getValue());
					p.setSqlType(pnt.getSQLType());
					p.setIn(true);
					p.setRequired(pnt.isRequired());
					args.add(p);
					usesRange = true;
					break;
				case '<': // LESSER OR EQUAL
					namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" <= ?");
					p = new Parameter();
					p.setPropertyName(entry.getValue());
					p.setSqlType(pnt.getSQLType());
					p.setIn(true);
					p.setRequired(pnt.isRequired());
					args.add(p);
					usesRange = true;
					break;
				case '+': // ARRAY
					namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(' ').append(DataLoader.getEqualsAnyExpr(dsn, connGroup));
					p = new Parameter();
					p.setPropertyName(entry.getValue());
					p.setSqlType(new ArraySQLType(pnt.getSQLType()));
					p.setIn(true);
					p.setRequired(true);
					args.add(p);
					break;
				case '*': // OPTIONAL
					namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" IS NULL");
					break;
				case '?': // MAYBE NULL
					namer.appendExpression(sb.append("((? IS NOT NULL AND "), pnt.getName(), NameType.COLUMN).append(" = ?) OR (? IS NULL AND ");
					namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" IS NULL))");
					p = new Parameter();
					p.setPropertyName(entry.getValue());
					p.setSqlType(pnt.getSQLType());
					p.setIn(true);
					p.setRequired(pnt.isRequired());
					args.add(p);
					args.add(p);
					args.add(p);
					break;
				case '\\':
				default: // NORMAL
					namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" = ?");
					p = new Parameter();
					p.setPropertyName(entry.getValue());
					p.setSqlType(pnt.getSQLType());
					p.setIn(true);
					p.setRequired(pnt.isRequired());
					args.add(p);
					break;
			}
		}
		if(lookups != null) {
			for(Map.Entry<String, Map<String, Pnt>> lookupEntry : lookups.entrySet()) {
				if(first) {
					first = false;
					sb.append(" WHERE ");
				} else
					sb.append(" AND ");
				//Assume 1 PK
				/* FOR more than 1:
				sb.append(" EXISTS(SELECT 1 FROM ");
				if(!StringUtils.isBlank(referCol.getRefTab().schema))
					sb.append(referCol.getRefTab().schema).append('.');
				sb.append(referCol.getRefTab().name);
				sb.append(" WHERE ");
				for(Col fkCol : referCol.getRefTab().getPrimaryKeyCols()) {
					// TODO: handle figuring out the referCol for each PK col
					exp = "U." + referCol;
					if(fkCol.required)
						sb.append(exp).append(" = ?").append(fkCol.name);
					else
						DataLoader.appendNotDistinctFromExpr(dsn, connGroup, sb, exp, fkCol.name);
					sb.append(" AND ");
				}
				*/
				namer.appendExpression(sb, lookupEntry.getKey(), NameType.COLUMN).append(" IN(SELECT ");
				first = true;
				for(Map.Entry<String, Pnt> pntEntry : lookupEntry.getValue().entrySet()) {
					Col refCol = pntEntry.getValue().getRefCol();
					if(first) {
						namer.appendExpression(sb, refCol.tab.getFirstPrimaryKeyCol().name, NameType.COLUMN).append(" FROM ");
						if(!StringUtils.isBlank(refCol.tab.schema))
							namer.appendExpression(sb, refCol.tab.schema, NameType.SCHEMA).append('.');
						namer.appendExpression(sb, refCol.tab.name, NameType.TABLE).append(" WHERE ");
						first = false;
					} else
						sb.append(" AND ");
					String colname = pntEntry.getKey();
					pnt = pntEntry.getValue();
					switch(colname.charAt(0)) {
						case '!': // NOT EQUALS
							if(pnt.isRequired())
								namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" != ?");
							else
								DataLoader.appendDistinctFromExpr(dsn, connGroup, sb, namer.getExpression(pnt.getName(), NameType.COLUMN));
							p = new Parameter();
							p.setPropertyName(keys.get(colname));
							p.setSqlType(pnt.getSQLType());
							p.setIn(true);
							p.setRequired(pnt.isRequired());
							args.add(p);
							break;
						case '^': // NOT IN
							namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(' ').append(DataLoader.getNotEqualsAnyExpr(dsn, connGroup));
							p = new Parameter();
							p.setPropertyName(keys.get(colname));
							p.setSqlType(new ArraySQLType(pnt.getSQLType()));
							p.setIn(true);
							p.setRequired(pnt.isRequired());
							args.add(p);
							break;
						case '>': // GREATER OR EQUAL
							namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" >= ?");
							p = new Parameter();
							p.setPropertyName(keys.get(colname));
							p.setSqlType(pnt.getSQLType());
							p.setIn(true);
							p.setRequired(pnt.isRequired());
							args.add(p);
							usesRange = true;
							break;
						case '<': // LESSER OR EQUAL
							namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" <= ?");
							p = new Parameter();
							p.setPropertyName(keys.get(colname));
							p.setSqlType(pnt.getSQLType());
							p.setIn(true);
							p.setRequired(pnt.isRequired());
							args.add(p);
							usesRange = true;
							break;
						case '*': // OPTIONAL
							namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" IS NULL");
							break;
						case '+': // ARRAY
							namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(' ').append(DataLoader.getEqualsAnyExpr(dsn, connGroup));
							p = new Parameter();
							p.setPropertyName(keys.get(colname));
							p.setSqlType(new ArraySQLType(pnt.getSQLType()));
							p.setIn(true);
							p.setRequired(pnt.isRequired());
							args.add(p);
							break;
						case '?': // MAYBE NULL
							namer.appendExpression(sb.append("((? IS NOT NULL AND "), pnt.getName(), NameType.COLUMN).append(" = ?) OR (? IS NULL AND ");
							namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" IS NULL))");
							p = new Parameter();
							p.setPropertyName(keys.get(colname));
							p.setSqlType(pnt.getSQLType());
							p.setIn(true);
							p.setRequired(false);
							args.add(p);
							args.add(p);
							args.add(p);
							break;
						case '\\':
						default: // NORMAL
							namer.appendExpression(sb, pnt.getName(), NameType.COLUMN).append(" = ?");
							p = new Parameter();
							p.setPropertyName(colname);
							p.setSqlType(pnt.getSQLType());
							p.setIn(true);
							p.setRequired(pnt.isRequired());
							break;
					}
				}
				sb.append(')');
			}
		}
		return usesRange;
	}


	public Call getPartialUpdateCall(Set<String> updateKeys, boolean returnPk, ConnectionGroup connGroup) throws SQLException {
		return getPartialUpdateCall(CollectionUtils.singleValueMap(uniqueKeyNames, PLACEHOLDER), updateKeys, returnPk, connGroup);
	}

	public Call getPartialUpdateCall(Map<String, Object> targetValues, Set<String> updateKeys, boolean returnPk, ConnectionGroup connGroup) throws SQLException {
		if(updateKeys.isEmpty())
			return null; // no call necessary - all data provided
		Map<String, String> keys = checkKeys(targetValues, true);
		updateKeys = retainUpdateColNames(updateKeys);
		UpdateCallKey uck = new UpdateCallKey(keys.keySet(), updateKeys, returnPk);
		Call updateCall = updateCalls.get(uck);
		if(updateCall == null) {
			Namer namer = dataLoader.getTargetNamer(connGroup);
			StringBuilder sb = new StringBuilder();
			List<Argument> args = new ArrayList<Argument>();
			Parameter p;
			boolean first;
			sb.append("UPDATE ");
			if(!StringUtils.isBlank(schema))
				namer.appendExpression(sb, schema, NameType.SCHEMA).append('.');
			namer.appendExpression(sb, name, NameType.TABLE).append(" SET ");
			Map<Col, Map<String,Col>> lookups = new LinkedHashMap<Col, Map<String,Col>>();
			first = true;
			for(String colname : updateKeys) {
				Col col = getCol(colname);
				if(!isLookup(col)) {
					if(first)
						first = false;
					else
						sb.append(", ");
					namer.appendExpression(sb, col.name, NameType.COLUMN).append(" = ?");
					p = new Parameter();
					p.setPropertyName(col.name);
					p.setSqlType(col.sqlType);
					p.setIn(true);
					p.setRequired(col.required);
					args.add(p);
				} else {
					Col referCol = getReferingCol(colname);
					Map<String,Col> lookupCols = lookups.get(referCol);
					if(lookupCols == null) {
						lookupCols = new LinkedHashMap<String,Col>();
						lookups.put(referCol, lookupCols);
					}
					lookupCols.put(colname, col);
				}
			}
			for(Map.Entry<Col, Map<String,Col>> entry : lookups.entrySet()) {
				if(first)
					first = false;
				else
					sb.append(", ");
				sb.append(entry.getKey().name).append(" = (SELECT ");
				Tab refTab = entry.getKey().getRefTab();
				switch(refTab.getPrimaryKeyCols().size()) {
					case 1:
						Col fkCol1 = refTab.getFirstPrimaryKeyCol();
						namer.appendExpression(sb, fkCol1.name, NameType.COLUMN);
						break;
					default:
						throw new SQLException("Cannot lookup primary key value from table '" + refTab + "' for '" + entry.getKey() + "' because it is a multi-column key");
				}
				sb.append(" FROM ");
				if(!StringUtils.isBlank(refTab.schema))
					namer.appendExpression(sb, refTab.schema, NameType.SCHEMA).append('.');
				namer.appendExpression(sb, refTab.name, NameType.TABLE);
				sb.append(" WHERE ");
				first = true;
				for(Map.Entry<String,Col> lookupEntry : entry.getValue().entrySet()) {
					if(first)
						first = false;
					else
						sb.append(" AND ");
					Col lookupCol = lookupEntry.getValue();
					int pc;
					if(lookupCol.required) {
						namer.appendExpression(sb, lookupCol.name, NameType.COLUMN).append(" = ?");
						pc = 1;
					} else
						pc = DataLoader.appendNotDistinctFromExpr(dataLoader.getTargetDsn(), connGroup, sb, lookupCol.name);
					p = new Parameter();
					p.setPropertyName(lookupEntry.getKey());
					p.setSqlType(lookupCol.sqlType);
					p.setIn(true);
					p.setRequired(lookupCol.required);
					for(int i = 0; i < pc; i++)
						args.add(p);
				}
				sb.append(')');
				first = false;
			}
			first = true;
			if(revisionPnt != null) {
				first = false;
				sb.append(" WHERE ");
				namer.appendExpression(sb, revisionPnt.getName(), NameType.COLUMN).append(" < ?");
				p = new Parameter();
				p.setPropertyName(dataLoader.getRevisionLabel());
				p.setSqlType(revisionPnt.getSQLType());
				p.setIn(true);
				args.add(p);
			}
			appendWhereClause(sb, keys, this, dataLoader.getTargetDsn(), connGroup, namer, args, first);
			if(returnPk && getFirstPrimaryKeyCol() != null) {
				sb.append(" RETURNING ");
				namer.appendExpression(sb, getFirstPrimaryKeyCol().name, NameType.COLUMN);
				Column column = new Column();
				column.setPropertyName(getFirstPrimaryKeyCol().name);
				// column.setName(getName(getFirstPrimaryKeyCol().name));
				column.setSqlType(getFirstPrimaryKeyCol().getSQLType().getTypeCode());
				column.setIndex(1);
				Cursor cursor = new Cursor();
				cursor.setColumns(new Column[] { column });
				args.add(0, cursor);
			} else {
				p = new Parameter();
				p.setIn(false);
				p.setOut(true);
				args.add(0, p);
			}

			updateCall = new Call();
			updateCall.setSql(sb.toString());
			updateCall.setDataSourceName(dataLoader.getTargetDsn());
			sb.setLength(0);
			sb.append("UPDATE_PARTIAL_");
			if(!StringUtils.isBlank(schema))
				sb.append(schema).append('.');
			sb.append(name).append('_');
			StringUtils.appendHex(sb.append('_'), updateKeys.hashCode());
			if(!keys.keySet().equals(uniqueKeyNames))
				StringUtils.appendHex(sb.append("_FILTERED_"), keys.keySet().hashCode());

			updateCall.setLabel(sb.toString());
			updateCall.setArguments(args.toArray(new Argument[args.size()]));
			updateCall.setLogExecuteTimeThreshhold(dataLoader.getLogExecuteTimeThreshhold());
			updateCall.setQueryTimeout(dataLoader.getQueryTimeout());
			Call stored = updateCalls.putIfAbsent(uck, updateCall);
			if(stored != null)
				return stored;
			updateCall.initialize();
			if(log.isInfoEnabled())
				log.info("Created call '" + updateCall.getLabel() + "':\n\t" + updateCall.getSql());
		}
		return updateCall;
	}

	public Call getAllExistingCall(ConnectionGroup connGroup) throws SQLException {
		Call call = allExistingCall.get();
		if(call == null) {
			Namer namer = dataLoader.getTargetNamer(connGroup);
			StringBuilder sb = new StringBuilder();
			List<Column> columns = new ArrayList<Column>();
			Cursor cur = new Cursor();
			boolean first = true;
			int index = 1;
			sb.append("SELECT ");
			for(String colname : uniqueKeyNames) {
				if(first)
					first = false;
				else
					sb.append(", ");
				Col col = colsByName.get(colname);
				namer.appendExpression(sb, col.name, NameType.COLUMN);
				Column column = new Column();
				column.setPropertyName(col.name);
				// column.setName(getName(col.name));
				column.setSqlType(col.sqlType.getTypeCode());
				column.setIndex(index++);
				columns.add(column);
			}

			cur.setColumns(columns.toArray(new Column[columns.size()]));
			sb.append(" FROM ");
			if(!StringUtils.isBlank(schema))
				namer.appendExpression(sb, schema, NameType.SCHEMA).append('.');
			namer.appendExpression(sb, name, NameType.TABLE);
			
			call = new Call();
			call.setSql(sb.toString());
			sb.setLength(0);
			call.setDataSourceName(dataLoader.getTargetDsn());
			sb.append("ALL_");
			if(!StringUtils.isBlank(schema))
				sb.append(schema).append('_');
			sb.append(name);
			call.setLabel(sb.toString());
			call.setArguments(new Argument[] {cur});
			call.setLogExecuteTimeThreshhold(dataLoader.getLogExecuteTimeThreshhold());
			call.setQueryTimeout(dataLoader.getQueryTimeout());
			if(!allExistingCall.compareAndSet(null, call))
				return getAllExistingCall(connGroup);
			call.initialize();
			if(log.isInfoEnabled())
				log.info("Created call '" + call.getLabel() + "':\n\t" + call.getSql());
		}
		return call;
	}

	public Tab copy() {
		Tab newTab = new DataLoaderTab(dataLoader, name, tabBuilder);
		copy(newTab);
		return newTab;
	}

	public void destroy() {
		for(Iterator<SourceInfo> siIter = sources.values().iterator(); siIter.hasNext();) {
			SourceInfo si = siIter.next();
			for(Iterator<Call> iter = si.sourceCalls.values().iterator(); iter.hasNext();) {
				iter.next().destroy();
				iter.remove();
			}
			siIter.remove();
		}
		for(Iterator<Call> iter = deleteCalls.values().iterator(); iter.hasNext();) {
			iter.next().destroy();
			iter.remove();
		}
		Call c = standardUpsertCall.getAndSet(null);
		if(c != null)
			c.destroy();
		if(customUpsertCall != null) {
			if(!customUpsertCallCopied)
				customUpsertCall.destroy();
			customUpsertCall = null;
			customUpsertPnts = null;
		}
		c = lookupCall.getAndSet(null);
		if(c != null)
			c.destroy();
		c = statsCall.getAndSet(null);
		if(c != null)
			c.destroy();
		c = allExistingCall.getAndSet(null);
		if(c != null)
			c.destroy();
	}
}