package com.usatech.etl;

import simple.db.Parameter;
import simple.sql.SQLType;

public class ParamPnt implements Pnt {
	protected final Parameter parameter;

	public ParamPnt(Parameter parameter) {
		this.parameter = parameter;
	}

	@Override
	public String getName() {
		return parameter.getPropertyName();
	}

	@Override
	public SQLType getSQLType() {
		return parameter.getSqlType();
	}

	@Override
	public boolean isRequired() {
		return parameter.isRequired();
	}

	@Override
	public boolean isRef() {
		return false;
	}

	@Override
	public Col getRefCol() {
		return null;
	}

	@Override
	public boolean isInput() {
		return parameter.isIn();
	}

	@Override
	public boolean isOutput() {
		return parameter.isOut();
	}

	public Pnt getLocalPnt() {
		return this;
	}
}
