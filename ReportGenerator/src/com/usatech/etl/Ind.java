package com.usatech.etl;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.xml.sax.SAXException;

import simple.bean.ConvertUtils;
import simple.io.LoadingException;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

class Ind {
	protected String name;
	protected boolean unique;
	protected final Set<Col> cols = new LinkedHashSet<Col>();
	protected String where;
	protected String tablespace;
	protected final ObjectBuilder indBuilder;

	protected Ind(ObjectBuilder indBuilder) {
		this.indBuilder = indBuilder;
	}

	public Ind(Tab tab, ObjectBuilder indBuilder) throws LoadingException {
		this(indBuilder);
		for(String colname : indBuilder.getAttr("cols", String[].class)) {
			Col col = tab.getCol(colname);
			if(col == null)
				throw new LoadingException("Col '" + colname + "' does not exist in '" + tab.name + "' and cannot be used in an index");
			addCol(col);
		}
		setName(indBuilder.getAttr("name", String.class));
		setUnique(Boolean.TRUE.equals(indBuilder.getAttr("unique", Boolean.class)));
		setWhere(indBuilder.getAttr("where", String.class));
		setTablespace(indBuilder.getAttr("tablespace", String.class));
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	public String getWhere() {
		return where;
	}

	public void setWhere(String where) {
		this.where = where;
	}

	public Set<Col> getCols() {
		return cols;
	}

	public void addCol(Col col) {
		cols.add(col);
	}

	public Ind copy(Tab tab) {
		Ind ind = new Ind(indBuilder);
		ind.setUnique(isUnique());
		ind.setWhere(getWhere());
		ind.setTablespace(getTablespace());
		for(Col oldCol : getCols()) {
			Col col = tab.getCol(oldCol.name);
			if(col != null)
				ind.addCol(col);
		}
		return ind;
	}

	public void writeXml(XMLBuilder xmlBuilder) throws SAXException {
		if(indBuilder != null && indBuilder.getXmlExtraNodes() != null)
			indBuilder.getXmlExtraNodes().writeExtraNodes(indBuilder, xmlBuilder.getContentHandler());
		if(isUnique())
			xmlBuilder.attributeForNext("unique", "true");
		if(!StringUtils.isBlank(getWhere()))
			xmlBuilder.attributeForNext("where", getWhere());
		if(!StringUtils.isBlank(getTablespace()))
			xmlBuilder.attributeForNext("tablespace", getTablespace());
		switch(getCols().size()) {
			case 0:
				break;
			case 1:
				xmlBuilder.attributeForNext("cols", getCols().iterator().next().name);
				break;
			default:
				StringBuilder sb = new StringBuilder();
				for(Col col : getCols())
					sb.append(',').append(col);
				xmlBuilder.attributeForNext("cols", sb.substring(1));
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Ind))
			return false;
		Ind other = (Ind) obj;
		return unique == other.unique
				&& ConvertUtils.areEqual(where, other.where)
				&& colsAreEqual(cols, other.cols);
	}
	
	protected boolean colsAreEqual(Set<Col> cols1, Set<Col> cols2) {
		if(cols1.size() != cols2.size())
			return false;
		Iterator<Col> colIter1 = cols1.iterator();
		Iterator<Col> colIter2 = cols2.iterator();
		while(colIter1.hasNext()) {
			if(!colIter1.next().equals(colIter2.next()))
				return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return cols.hashCode();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTablespace() {
		return tablespace;
	}

	public void setTablespace(String tablespace) {
		this.tablespace = tablespace;
	}
}
