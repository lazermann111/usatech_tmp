package com.usatech.etl;

import java.io.IOException;
import java.sql.Types;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import simple.io.Log;
import simple.lang.SystemUtils;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;


public class DdlGenerator {
	private static final Log log = Log.getLog();
	protected String rowCountLabel = "ROW_COUNT";
	protected String newline = SystemUtils.getNewLine();
	protected String b4parens = newline + "  ";
	protected String indent = "    ";
	protected String partitionTablePrefix = "_";

	protected static int targetMaxNameLength = 60;
	protected static int sourceMaxNameLength = 30;

	protected static final char[] QUOTES = "'".toCharArray();
	protected static final Pattern pattern = Pattern.compile("\\s*(\\w+)(?:\\s*[(]\\s*(\\d+)\\s*(?:[,]\\s*(\\d+)\\s*)?[)])?(?:(?:\\s*[:]\\s*|\\s+OF\\s+)(\\w+(?:\\s*[(]\\s*\\d+\\s*(?:[,]\\s*\\d+\\s*)?[)])?(?:(?:\\s*[:]\\s*|\\s+OF\\s+)\\w+(?:\\s*[(]\\s*\\d+\\s*(?:[,]\\s*\\d+\\s*)?[)])?)*))?\\s*", Pattern.CASE_INSENSITIVE);

	public DdlGenerator() {
	}

	public void generate(DdlHandler handler, Db<? extends Tab> db) throws IOException {
		if(log.isInfoEnabled())
			log.info("Generating DDL for db");
		handler.start(db.getBuildNumber());
		generateSql(handler, db.getPreSql(), true);
		// gen independent functions

		// gen audit triggers
		Map<Tab, String> auditTabs = new LinkedHashMap<Tab, String>();
		for(Tab tab : db.getTabs()) {
			for(Tab parent = tab.getParent(); parent != null; parent = parent.getParent()) {
				if(auditTabs.containsKey(parent))
					break;
				if(hasAuditCols(parent)) {
					auditTabs.put(parent, determineAuditTriggerSuffix(parent));
					break;
				}
			}
		}
		for(Map.Entry<Tab, String> entry : auditTabs.entrySet()) {
			Tab tab = entry.getKey();
			String triggerSuffix = entry.getValue();
			generateAuditTrigger(handler, tab, tab.getSchema(), triggerSuffix);
		}
		Set<Tab> generated = new HashSet<Tab>();
		while(generated.size() < db.getTabs().size()) {
			int cnt = 0;
			MIDDLE: for(Tab tab : db.getTabs()) {
				if(generated.contains(tab))
					continue;
				if(tab.getParent() != null && !generated.contains(tab.getParent()))
					continue;
				for(Col col : tab.getCols(true)) {
					if(col.getRefTab() != null && !generated.contains(col.getRefTab()))
						continue MIDDLE;
				}
				String auditTriggerNameSuffix = null;
				String auditTriggerSchema = null;
				for(Tab parent = tab.getParent(); parent != null; parent = parent.getParent()) {
					auditTriggerNameSuffix = auditTabs.get(parent);
					if(auditTriggerNameSuffix != null) {
						auditTriggerSchema = parent.getSchema();
						break;
					}
				}
				generate(handler, tab, auditTriggerNameSuffix, auditTriggerSchema);
				cnt++;
				generated.add(tab);
			}
			if(cnt == 0) {
				StringBuilder sb = new StringBuilder();
				sb.append("Circular Reference detected in tables [");
				for(Tab tab : db.getTabs()) {
					if(generated.contains(tab))
						continue;
					sb.append(tab.name).append(", ");
				}
				sb.setLength(sb.length() - 2);
				sb.append(']');
				throw new IOException(sb.toString());
			}
		}
		// gen post-sql
		generateSql(handler, db.getPostSql(), false);
		if(log.isInfoEnabled())
			log.info("Generation complete for db");
	}

	protected void generateSql(DdlHandler handler, String[] sqls, boolean first) throws IOException {
		if(sqls == null)
			return;
		for(String sql : sqls) {
			if(!StringUtils.isBlank(sql)) {
				if(first) {
					handler.startStatement(null);
					first = false;
				} else
					handler.startStatement(" ======================================================================");
				handler.append(sql);
				handler.endStatement();
			}
		}
	}
	protected String determineAuditTriggerSuffix(Tab tab) {
		if(tab.name.equals("_"))
			return "AUDIT";
		return tab.name;
	}

	protected boolean hasAuditCols(Tab tab) {
		for(Col col : tab.getCols(true))
			switch(col.colType) {
				case AUDIT_CREATE:
				case AUDIT_UPDATE:
					return true;
			}
		return false;
	}

	protected String getDbType(SQLType sqlType, boolean autoGen) {
		if(autoGen) {
			switch(sqlType.getTypeCode()) {
				case Types.BIGINT:
					return "BIGSERIAL";
				case Types.INTEGER:
					return "SERIAL";
				case Types.SMALLINT:
					return "SMALLSERIAL";
			}
		}
		return sqlType.toString();
	}

	public void generate(DdlHandler handler, final Tab tab, String auditTriggerNameSuffix, String auditTriggerSchema) throws IOException {
		if(tab.getUpsertType() == UpsertType.CUSTOM)
			return;
		if(log.isInfoEnabled())
			log.info("Generating DDL for table " + tab.name);
		boolean first = true;
		handler.startStatement(" ======================================================================");
		handler.append("CREATE TABLE ");
		if(!StringUtils.isBlank(tab.getSchema()))
			handler.append(tab.getSchema()).append('.');
		if(tab.isPartitioned())
			handler.append(partitionTablePrefix);
		handler.append(tab.name).append(b4parens).append('(').append(newline);
		StringBuilder sb2 = new StringBuilder();
		String[][] layout = new String[tab.getCols(true).size()][3];
		int[] widths = new int[3];
		int i = 0;
		boolean audit = false;
		for(Col col : tab.getCols(true)) {
			layout[i][0] = col.name;
			layout[i][1] = getDbType(col.sqlType, col.isAutoGen());
			sb2.setLength(0);
			if(col.required)
				sb2.append("NOT NULL");
			if(!StringUtils.isBlank(col.defaultSql)) {
				if(sb2.length() > 0)
					sb2.append(' ');
				sb2.append("DEFAULT ").append(col.defaultSql);
			}
			layout[i][2] = sb2.toString();
			for(int k = 0; k < layout[i].length; k++)
				if(layout[i][k].length() > widths[k])
					widths[k] = layout[i][k].length();
			i++;
			switch(col.colType) {
				case AUDIT_CREATE: case AUDIT_UPDATE:
					audit = true;
					break;
			}
		}
		for(i = 0; i < layout.length; i++) {
			if(i > 0)
				handler.append(',').append(newline);
			handler.append(indent);
			for(int k = 0; k < 2; k++) {
				StringUtils.appendPadded(handler, layout[i][k], ' ', widths[k], Justification.LEFT);
				handler.append(' ');
			}
			handler.append(layout[i][2]);
		}
		if(!tab.getPrimaryKeyCols().isEmpty()) {
			handler.append(',').append(newline);
			handler.append(newline);
			handler.append(indent).append("CONSTRAINT PK_").append(tab.name).append(" PRIMARY KEY(");
			first = true;
			for(Col col : tab.getPrimaryKeyCols()) {
				if(first)
					first = false;
				else
					handler.append(", ");
				handler.append(col.name);
			}
			handler.append(')');
			if(!StringUtils.isBlank(tab.getTablespace()))
				handler.append(" USING INDEX TABLESPACE ").append(tab.getTablespace());
		}
		// fks
		for(Col col : tab.getCols()) {
			if(col.getRefTab() != null) {
				switch(col.getRefTab().getPrimaryKeyCols().size()) {
					case 1:
						Col fkCol = col.getRefTab().getFirstPrimaryKeyCol();
						handler.append(',').append(newline);
						handler.append(indent).append("CONSTRAINT ");
						sb2.setLength(0);
						sb2.append("FK_").append(tab.name).append('_').append(col.name);
						appendName(handler, sb2.toString(), targetMaxNameLength);
						handler.append(" FOREIGN KEY(").append(col.name).append(") REFERENCES ");
						if(!StringUtils.isBlank(col.getRefTab().getSchema()))
							handler.append(col.getRefTab().getSchema()).append('.');
						if(col.getRefTab().isPartitioned())
							handler.append(partitionTablePrefix);
						handler.append(col.getRefTab().name).append('(').append(fkCol.name).append(')');
						break;
					default:
						// XXX: This gets nasty so do nothing for now
				}
			}
		}
		handler.append(b4parens).append(')');
		// inherits
		if(tab.getParent() != null) {
			handler.append(b4parens).append("INHERITS(");
			if(!StringUtils.isBlank(tab.getParent().getSchema()))
				handler.append(tab.getParent().getSchema()).append('.');
			if(tab.getParent().isPartitioned())
				handler.append(partitionTablePrefix);
			handler.append(tab.getParent().name).append(')');
			for(Tab p = tab.getParent(); !audit && p != null; p = p.getParent()) {
				audit = hasAuditCols(p);
			}
		}
		if(!StringUtils.isBlank(tab.getTablespace()))
			handler.append(b4parens).append("TABLESPACE ").append(tab.getTablespace());
		handler.append(';');
		handler.endStatement();

		// ---------------------
		if(tab.isPartitioned()) {
			handler.startStatement(null);
			handler.append("CREATE OR REPLACE FUNCTION ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("FRIIUD_").append(tab.name).append("()").append(newline).append("\tRETURNS TRIGGER").append(newline).append("\tSECURITY DEFINER").append(newline).append("AS $$").append(newline).append("DECLARE").append(newline).append("\tlv_old_partition_table VARCHAR(4000);").append(newline).append("\tlv_new_partition_table VARCHAR(4000);").append(newline)
					.append("\tlv_new_suffix VARCHAR(4000);").append(newline).append("\tlv_check_exp VARCHAR(4000);").append(newline).append("\tlv_sql TEXT;").append(newline).append("\tlv_filter TEXT;").append(newline).append("\tln_cnt INTEGER;").append(newline).append("BEGIN").append(newline);
			if(audit) {
				handler.append("\tIF TG_OP = 'INSERT' THEN").append(newline);
				for(Col col : tab.getCols()) {
					switch(col.colType) {
						case AUDIT_CREATE: case AUDIT_UPDATE:
							handler.append("\t\t\tNEW.").append(col.name).append(" := ").append(getAuditValueExpression(col)).append(';').append(newline);
							break;
					}
				}
				handler.append("\tELSIF TG_OP = 'UPDATE' THEN").append(newline);
				for(Col col : tab.getCols()) {
					switch(col.colType) {
						case AUDIT_CREATE:
							handler.append("\t\t\tNEW.").append(col.name).append(" := OLD.").append(col.name).append(';').append(newline);
							break;
						case AUDIT_UPDATE:
							handler.append("\t\t\tNEW.").append(col.name).append(" := ").append(getAuditValueExpression(col)).append(';').append(newline);
							break;
					}
				}
				handler.append("\tEND IF;").append(newline);
			}

			handler.append("\tIF TG_OP IN('INSERT', 'UPDATE') THEN").append(newline).append("\t\tSELECT COALESCE(");
			appendReplacement(handler, tab.getPartitionExpression(), "$1", "NEW", QUOTES).append(",'') INTO lv_new_suffix;").append(newline).append("\t\tSELECT ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("CONSTRUCT_NAME('").append(partitionTablePrefix).append(tab.name).append("', lv_new_suffix) INTO lv_new_partition_table;").append(newline).append("\tEND IF;").append(newline);
			handler.append("\tIF TG_OP IN('DELETE', 'UPDATE') THEN").append(newline).append("\t\tSELECT ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("CONSTRUCT_NAME('").append(partitionTablePrefix).append(tab.name).append("', COALESCE(");
			appendReplacement(handler, tab.getPartitionExpression(), "$1", "OLD", QUOTES).append(", '')) INTO lv_old_partition_table;").append(newline).append("\tEND IF;").append(newline);

			handler.append("\tIF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN").append(newline).append("\t\tlv_sql := 'UPDATE ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("' || lv_new_partition_table || ' SET ';").append(newline).append("\t\tlv_filter := '';").append(newline);
			for(Col col : tab.getPrimaryKeyCols()) {
				handler.append("\t\tIF OLD.").append(col.name).append(col.required ? " != " : " IS DISTINCT FROM ").append("NEW.").append(col.name).append(" THEN").append(newline).append("\t\t\tlv_sql := lv_sql || '").append(col.name).append(" = $2.").append(col.name).append(", ';").append(newline)
					.append("\t\tEND IF;").append(newline);
			}
			for(Col col : tab.getUniqueKeyCols()) {
				handler.append("\t\tIF OLD.").append(col.name).append(col.required ? " != " : " IS DISTINCT FROM ").append("NEW.").append(col.name).append(" THEN").append(newline).append("\t\t\tlv_sql := lv_sql || '")
					.append(col.name).append(" = $2.").append(col.name).append(", ';").append(newline).append("\t\t\tlv_filter := lv_filter || ' AND (").append(col.name).append(col.required ? " = " : " IS NOT DISTINCT FROM ")
					.append("$1.").append(col.name).append(" OR ").append(col.name).append(col.required ? " = " : " IS NOT DISTINCT FROM ").append("$2.").append(col.name).append(")';").append(newline)
					.append("\t\tEND IF;").append(newline);
			}
			sb2.setLength(0);
			for(Col col : tab.getCols()) {
				switch(col.colType) {
					case CUSTOM:
					case REVISION:
					case DATA:
						handler.append("\t\tIF OLD.").append(col.name).append(col.required ? " != " : " IS DISTINCT FROM ").append("NEW.").append(col.name).append(" THEN").append(newline).append("\t\t\tlv_sql := lv_sql || '").append(col.name).append(" = $2.").append(col.name).append(", ';").append(newline).append("\t\t\tlv_filter := lv_filter || ' AND (").append(col.name)
								.append(col.required ? " = " : " IS NOT DISTINCT FROM ").append("$1.").append(col.name).append(" OR ").append(col.name).append(col.required ? " = " : " IS NOT DISTINCT FROM ").append("$2.").append(col.name).append(")';").append(newline).append("\t\tEND IF;").append(newline);
						break;
					case AUDIT_UPDATE:
						sb2.append(col.name).append(" = $2.").append(col.name).append(", ");
						break;

				}
			}
			handler.append("\t\tlv_sql := lv_sql || '").append(sb2).append("';").append(newline);
			handler.append("\t\tlv_sql := LEFT(lv_sql, -2) || ' WHERE ");
			first = true;
			for(Col col : tab.getPrimaryKeyCols()) {
				if(first)
					first = false;
				else
					handler.append(" AND ");
				if(col.required)
					handler.append(col.name).append(" = $1.").append(col.name);
				else
					handler.append(col.name).append(" IS NOT DISTINCT FROM $1.").append(col.name);
			}
			handler.append("' || lv_filter;").append(newline);

			handler.append("\t\tEXECUTE lv_sql USING OLD, NEW;").append(newline).append("\t\tGET DIAGNOSTICS ln_cnt = ROW_COUNT;").append(newline).append("\t\tIF ln_cnt = 0 THEN").append(newline).append("\t\t\tRAISE serialization_failure;").append(newline).append("\t\tEND IF;").append(newline).append("\t\tRETURN NEW;").append(newline).append("\tEND IF;").append(newline)
					.append("\tIF lv_new_partition_table IS NOT NULL THEN").append(newline).append("\t\tLOOP").append(newline).append("\t\t\tBEGIN").append(newline).append("\t\t\t\tEXECUTE 'INSERT INTO ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("' || lv_new_partition_table || ' SELECT $1.*' USING NEW;").append(newline).append("\t\t\t\tEXIT;").append(newline).append("\t\t\tEXCEPTION").append(newline).append("\t\t\tWHEN undefined_table THEN").append(newline).append("\t\t\t\tSELECT '");
			if(tab.getFilterExpressions() != null) {
				i = 0;
				for(String fe : tab.getFilterExpressions()) {
					if(!StringUtils.isBlank(fe)) {
						handler.append("CONSTRAINT CK_' || lv_new_partition_table || '");
						if(i > 0)
							handler.append('_').append(String.valueOf(i));
						handler.append(" CHECK(' ||");
						appendReplacement(handler, fe, "$1", "NEW", QUOTES).append("||'), ");
						i++;
					}
				}
			}
			handler.append("' INTO lv_check_exp;").append(newline);
			handler.append("\t\t\t\tRAISE NOTICE 'Creating new partition table %', lv_new_partition_table;").append(newline).append("\t\t\t\tBEGIN").append(newline).append("\t\t\t\t\tEXECUTE 'CREATE TABLE ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("' || lv_new_partition_table || '(' || lv_check_exp");
			if(!tab.getPrimaryKeyCols().isEmpty()) {
				handler.append(" ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(");
				first = true;
				for(Col col : tab.getPrimaryKeyCols()) {
					if(first)
						first = false;
					else
						handler.append(", ");
					handler.append(col.name);
				}
				handler.append(')');
				if(!StringUtils.isBlank(tab.getTablespace()))
					handler.append(" USING INDEX TABLESPACE ").append(tab.getTablespace());
			}
			for(Col col : tab.getCols()) {
				if(col.getRefTab() != null) {
					// NOTE: for now let's ignore foreign keys because they can get tricky when referencing a partitioned table
					// NOTE: we could add foreign keys for references to partitioned tables where the partition expressions match -- except if we ever move the row to a different partition,
					// so for now do not add those foreign keys
					if(!col.getRefTab().isPartitioned() /*|| ConvertUtils.areEqual(col.getRefTab().getPartitionExpression(), tab.getPartitionExpression()) */) {
						switch(col.getRefTab().getPrimaryKeyCols().size()) {
							case 1:
								if(first)
									first = false;
								else
									handler.append(", ");

								Col fkCol = col.getRefTab().getFirstPrimaryKeyCol();
								handler.append("CONSTRAINT ' || ");
								if(!StringUtils.isBlank(tab.getSchema()))
									handler.append(tab.getSchema()).append('.');

								handler.append("CONSTRUCT_NAME('").append(partitionTablePrefix).append("FK_").append(tab.name).append('_').append(col.name).append("', lv_new_suffix) || ' ");
								handler.append("FOREIGN KEY(").append(col.name).append(") REFERENCES ");
								if(!StringUtils.isBlank(tab.getSchema()))
									handler.append(tab.getSchema()).append('.');
								// Add fk for partitioned parent
								if(!col.getRefTab().isPartitioned())
									handler.append(col.getRefTab().name);
								else {
									handler.append("' || ");
									if(!StringUtils.isBlank(col.getRefTab().getSchema()))
										handler.append(col.getRefTab().getSchema()).append('.');
									handler.append("CONSTRUCT_NAME('").append(partitionTablePrefix).append(col.getRefTab().name).append("', lv_new_suffix) || '");
								}
								handler.append('(').append(fkCol.name).append(')');
								break;
							default:
								// XXX: This gets nasty so do nothing for now
						}
					}
				}
			}
			handler.append(") INHERITS(");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(partitionTablePrefix).append(tab.name).append(')');
			if(!StringUtils.isBlank(tab.getTablespace()))
				handler.append(" TABLESPACE ").append(tab.getTablespace());
			handler.append("';").append(newline).append("\t\t\t\tEXCEPTION").append(newline).append("\t\t\t\t\tWHEN duplicate_table THEN").append(newline).append("\t\t\t\t\t\tCONTINUE;").append(newline).append("\t\t\t\tEND;").append(newline);

			handler.append("\t\t\t\tEXECUTE 'GRANT ALL ON TABLE ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("' || lv_new_partition_table || ' TO ' || CURRENT_USER;").append(newline);
			/*
			handler.append("\t\t\t\tEXECUTE 'GRANT INSERT,UPDATE,DELETE ON TABLE ");
			if(schema != null)
				handler.append(schema).append('.');
			handler.append("' || lv_new_partition_table || ' TO write_rdw';").append(newline);
			handler.append("\t\t\t\tEXECUTE 'GRANT SELECT ON TABLE ");
			if(schema != null)
				handler.append(schema).append('.');
			handler.append("' || lv_new_partition_table || ' TO read_rdw';").append(newline);
			*/
			// Copy foreign keys, check constraints and indexes
			handler.append("\t\t\t\tPERFORM ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("COPY_INDEXES('").append(partitionTablePrefix).append(tab.name).append("', lv_new_suffix);").append(newline);
			handler.append("\t\t\tEND;").append(newline).append("\t\tEND LOOP;").append(newline).append("\tEND IF;").append(newline);
			handler.append("\tIF lv_old_partition_table IS NOT NULL THEN").append(newline).append("\t\tEXECUTE 'DELETE FROM ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("' || lv_old_partition_table || ' WHERE ");
			first = true;
			for(Col col : tab.getPrimaryKeyCols()) {
				if(first)
					first = false;
				else
					handler.append(" AND ");
				if(col.required)
					handler.append(col.name).append(" = $1.").append(col.name);
				else
					handler.append(col.name).append(" IS NOT DISTINCT FROM $1.").append(col.name);
			}

			handler.append("' USING OLD;").append(newline).append("\t\tGET DIAGNOSTICS ln_cnt = ROW_COUNT;").append(newline).append("\t\tIF ln_cnt = 0 THEN").append(newline).append("\t\t\tRAISE serialization_failure;").append(newline).append("\t\tEND IF;").append(newline).append("\tEND IF;").append(newline).append("\tIF TG_OP IN('INSERT', 'UPDATE') THEN").append(newline).append("\t\tRETURN NEW;")
					.append(newline).append("\tELSE").append(newline).append("\t\tRETURN OLD;").append(newline).append("\tEND IF;").append(newline).append("END;").append(newline).append("$$ LANGUAGE plpgsql;").append(newline).append(newline);

			handler.append("CREATE OR REPLACE VIEW ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(tab.name).append(newline).append("\tAS SELECT * FROM ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(partitionTablePrefix).append(tab.name).append(";").append(newline).append(newline);

			first = true;
			for(Col col : tab.getCols()) {
				if(!StringUtils.isBlank(col.defaultSql)) {
					handler.append("ALTER VIEW ");
					if(!StringUtils.isBlank(tab.getSchema()))
						handler.append(tab.getSchema()).append('.');
					handler.append(tab.name).append(" ALTER ").append(col.name).append(" SET DEFAULT ").append(col.defaultSql).append(';').append(newline);
					first = false;
				} else if(col.isAutoGen()) {
					handler.append("ALTER VIEW ");
					if(!StringUtils.isBlank(tab.getSchema()))
						handler.append(tab.getSchema()).append('.');
					handler.append(tab.name).append(" ALTER ").append(col.name).append(" SET DEFAULT nextval('");
					if(!StringUtils.isBlank(tab.getSchema()))
						handler.append(tab.getSchema()).append('.');
					handler.append(partitionTablePrefix).append(tab.name.toLowerCase()).append('_').append(col.name.toLowerCase()).append("_seq'::regclass);").append(newline);
					first = false;
				}
			}
			if(!first)
				handler.append(newline);
			handler.append("GRANT SELECT ON ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(tab.name).append(" TO read_");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema().toLowerCase());
			else
				handler.append("data");
			handler.append(";").append(newline);
			handler.append("GRANT INSERT, UPDATE, DELETE ON ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(tab.name).append(" TO write_");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema().toLowerCase());
			else
				handler.append("data");
			handler.append(";").append(newline);

			handler.append("REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(partitionTablePrefix).append(tab.name).append(" FROM postgres;").append(newline).append(newline);

			handler.append("DROP TRIGGER IF EXISTS TRIIUD_").append(tab.name).append(" ON ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(tab.name).append(';').append(newline);
			handler.append("CREATE TRIGGER TRIIUD_").append(tab.name).append(newline).append("INSTEAD OF INSERT OR UPDATE OR DELETE ON ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(tab.name).append(newline).append("\tFOR EACH ROW").append(newline).append("\tEXECUTE PROCEDURE ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("FRIIUD_").append(tab.name).append("();").append(newline);
			handler.endStatement();
		} else {
			handler.startStatement(null);
			handler.append("GRANT SELECT ON ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(tab.name).append(" TO read_");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema().toLowerCase());
			else
				handler.append("data");
			handler.append(";").append(newline);
			handler.append("GRANT INSERT, UPDATE, DELETE ON ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(tab.name).append(" TO write_");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema().toLowerCase());
			else
				handler.append("data");
			handler.append(";").append(newline);
			handler.endStatement();
			if(audit) {
				if(StringUtils.isBlank(auditTriggerNameSuffix)) {
					auditTriggerNameSuffix = determineAuditTriggerSuffix(tab);
					auditTriggerSchema = tab.getSchema();
					generateAuditTrigger(handler, tab, auditTriggerSchema, auditTriggerNameSuffix);
				}
				handler.startStatement(null);
				handler.append("DROP TRIGGER IF EXISTS TRBI_").append(tab.name).append(" ON ");
				if(!StringUtils.isBlank(tab.getSchema()))
					handler.append(tab.getSchema()).append('.');
				handler.append(tab.name).append(';').append(newline);
				handler.endStatement();
				handler.startStatement(null);
				handler.append("CREATE TRIGGER TRBI_").append(tab.name).append(newline).append("BEFORE INSERT ON ");
				if(!StringUtils.isBlank(tab.getSchema()))
					handler.append(tab.getSchema()).append('.');
				handler.append(tab.name).append(newline).append("\tFOR EACH ROW").append(newline).append("\tEXECUTE PROCEDURE ");
				if(!StringUtils.isBlank(auditTriggerSchema))
					handler.append(auditTriggerSchema).append('.');
				handler.append("FTRBI_").append(auditTriggerNameSuffix).append("();").append(newline).append(newline);
				handler.endStatement();
				handler.startStatement(null);
				handler.append("DROP TRIGGER IF EXISTS TRBU_").append(tab.name).append(" ON ");
				if(!StringUtils.isBlank(tab.getSchema()))
					handler.append(tab.getSchema()).append('.');
				handler.append(tab.name).append(';').append(newline);
				handler.endStatement();
				handler.startStatement(null);
				handler.append("CREATE TRIGGER TRBU_").append(tab.name).append(newline).append("BEFORE UPDATE ON ");
				if(!StringUtils.isBlank(tab.getSchema()))
					handler.append(tab.getSchema()).append('.');
				handler.append(tab.name).append(newline).append("\tFOR EACH ROW").append(newline).append("\tEXECUTE PROCEDURE ");
				if(!StringUtils.isBlank(auditTriggerSchema))
					handler.append(auditTriggerSchema).append('.');
				handler.append("FTRBU_").append(auditTriggerNameSuffix).append("();").append(newline);
				handler.endStatement();
			}
		}

		// gen indexes
		String altKeyIndex;
		if(!tab.getUniqueKeyCols().isEmpty()) {
			sb2.setLength(0);
			altKeyIndex = sb2.append("AK_").append(tab.name).toString();
		} else
			altKeyIndex = null;
		i = 1;
		for(Ind ind : tab.getIndexes()) {
			if(altKeyIndex != null && ind.isUnique() && ind.getCols().equals(tab.getIdentityCols()) && StringUtils.isBlank(ind.getWhere())) {
				generateIndex(handler, tab, StringUtils.isBlank(ind.getName()) ? altKeyIndex : ind.getName(), true, ind.getCols(), null, ind.getTablespace() == null ? tab.getTablespace() : ind.getTablespace());
				altKeyIndex = null;
			} else
				generateIndex(handler, tab, constructIndexName(tab, ind), ind.isUnique(), ind.getCols(), ind.getWhere(), ind.getTablespace() == null ? tab.getTablespace() : ind.getTablespace());
		}
		if(altKeyIndex != null)
			generateIndex(handler, tab, altKeyIndex, true, tab.getIdentityCols(), null, tab.getTablespace());

		// -- generate default row population script
		if(tab.name.endsWith("_DIM")) {
			sb2.setLength(0);
			sb2.append("------------------- DATA PRE-LOAD for ");
			if(!StringUtils.isBlank(tab.getSchema()))
				sb2.append(tab.getSchema()).append('.');
			sb2.append(tab.name).append(" ----------------------------------");
			handler.startStatement(sb2.toString());
			
			handler.append("INSERT INTO ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(tab.name).append('(');
			first = true;
			for(Col col : tab.getCols()) {
				switch(col.colType) {
					case PRIMARY_KEY: case IDENTITY_KEY: case DATA: case REVISION:
						if(first)
							first = false;
						else
							handler.append(',');
						handler.append(newline).append('\t').append(col.name);
						break;
				}
			}
			handler.append(')').append(newline).append("VALUES(");
			first = true;
			for(Col col : tab.getCols()) {
				switch(col.colType) {
					case PRIMARY_KEY: case IDENTITY_KEY: case DATA: case REVISION:
						if(first)
							first = false;
						else
							handler.append(',');
						handler.append(newline).append('\t').append(getUnknownValueExpression(col));
				}
			}
			handler.append(");").append(newline);
			handler.endStatement();
		}
		String updateName = tab.name;
		// --generate precalc population script
		if(!StringUtils.isBlank(tab.getPrecalcExpression())) {
			if(!tab.name.endsWith("_DIM")) {
				sb2.setLength(0);
				sb2.append("--------------------- DATA PRE-LOAD for ");
				if(!StringUtils.isBlank(tab.getSchema()))
					sb2.append(tab.getSchema()).append('.');
				sb2.append(tab.name).append(" ----------------------------------");
				handler.startStatement(sb2.toString());
			} else
				handler.startStatement(null);

			handler.append("INSERT INTO ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(updateName).append('(');
			first = true;
			for(Col col : tab.getCols()) {
				switch(col.colType) {
					case PRIMARY_KEY: case IDENTITY_KEY: case DATA: case REVISION:
						if(first)
							first = false;
						else
							handler.append(',');
						handler.append(newline).append('\t').append(col.name);
						break;
				}
			}
			
			handler.append(')').append(newline).append("SELECT ");
			first = true;
			for(Col col : tab.getCols()) {
				switch(col.colType) {
					case PRIMARY_KEY: case IDENTITY_KEY: case DATA: case REVISION:
						if(first)
							first = false;
						else
							handler.append(',');
						handler.append(newline).append('\t');
						if(!StringUtils.isBlank(col.getPrecalcExpression()))
							handler.append(col.getPrecalcExpression());
						else
							handler.append(getUnknownValueExpression(col));
				}
			}
			handler.append(newline).append("FROM ").append(tab.getPrecalcExpression()).append(';').append(newline);
			handler.endStatement();
		} else if(!tab.getPrimaryKeyCols().isEmpty()) { // don't do this for pre-calc tables
			// Add UPSERT function for table
			handler.startStatement(null);
			handler.append(newline).append("CREATE OR REPLACE FUNCTION ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("UPSERT_").append(tab.name).append('(');
			Col revisionCol = null;
			boolean pkRef = false;
			boolean hasUniqueKeys = !tab.getUniqueKeyCols().isEmpty();
			first = true;
			OUTER: for(Col col : tab.getCols()) {
				String direction;
				boolean usePrecision = true;
				switch(col.colType) {
					case PRIMARY_KEY:
						if(col.getRefTab() != null) {
							direction = null;
							pkRef = hasUniqueKeys;
						} else if(hasUniqueKeys) {
							direction = "OUT";
							usePrecision = false;
						} else
							direction = null;
						break;
					case REVISION:
						direction = "INOUT";
						usePrecision = false; // Not sure on this
						revisionCol = col;
						break;
					case DATA:
					case IDENTITY_KEY:
						direction = null;
						break;
					default:
						continue OUTER;
				}
				if(first)
					first = false;
				else
					handler.append(", ");
				handler.append(newline).append("\tp_").append(col.name.toLowerCase()).append(' ');
				if(direction != null)
					handler.append(direction).append(' ');
				appendSqlType(handler, col.sqlType, usePrecision);
			}
			
			if(first)
				first = false;
			else
				handler.append(',');
			handler.append(newline).append("\tp_").append(getRowCountLabel().toLowerCase()).append(" OUT INTEGER");
			handler.append(')').append(newline).append("\tSECURITY DEFINER").append(newline).append("AS $$").append(newline);
			if(revisionCol != null || pkRef) {
				handler.append("DECLARE").append(newline);
				if(revisionCol != null) {
					handler.append("\tp_old_").append(revisionCol.name.toLowerCase()).append(' ');
					appendSqlType(handler, revisionCol.sqlType, true);
					handler.append(';').append(newline);
				}
				if(pkRef) {
					for(Col col : tab.getPrimaryKeyCols()) {
						if(col.getRefTab() != null) {
							handler.append("\tl_").append(col.name.toLowerCase()).append(' ');
							appendSqlType(handler, col.sqlType, false);
							handler.append(';').append(newline);
						}
					}
				}
			}

			handler.append("BEGIN").append(newline).append("\tFOR i IN 1..20 LOOP").append(newline).append("\t\tBEGIN").append(newline);
			if(revisionCol != null) {
				handler.append("\t\t\tSELECT ");
				first = true;
				if(!tab.getUniqueKeyCols().isEmpty()) {
					for(Col col : tab.getPrimaryKeyCols()) {
						if(first)
							first = false;
						else
							handler.append(", ");
						handler.append(col.name);
					}
				}
				if(first)
					first = false;
				else
					handler.append(", ");
				handler.append(revisionCol.name).append(newline).append("\t\t\t  INTO ");
				first = true;
				if(!tab.getUniqueKeyCols().isEmpty()) {
					for(Col col : tab.getPrimaryKeyCols()) {
						if(first)
							first = false;
						else
							handler.append(", ");
						if(col.getRefTab() != null)
							handler.append('l');
						else
							handler.append('p');
						handler.append('_').append(col.name.toLowerCase());
					}
				}
				if(first)
					first = false;
				else
					handler.append(", ");
				handler.append("p_old_").append(revisionCol.name.toLowerCase()).append(newline).append("\t\t\t  FROM ");
				if(!StringUtils.isBlank(tab.getSchema()))
					handler.append(tab.getSchema()).append('.');
				handler.append(tab.name).append(newline).append("\t\t\t WHERE ");
				first = true;
				for(Col col : tab.getIdentityCols()) {
					if(first)
						first = false;
					else
						handler.append(newline).append("\t\t\t   AND ");
					if(col.required)
						handler.append(col.name).append(" = p_").append(col.name.toLowerCase());
					else
						handler.append("((").append(col.name).append(" IS NOT NULL AND ").append(col.name).append(" = p_").append(col.name.toLowerCase()).append(") OR (").append(col.name).append(" IS NULL AND p_").append(col.name.toLowerCase()).append(" IS NULL))");
				}
					
				handler.append(';').append(newline).append("\t\t\tIF FOUND THEN").append(newline);
				String paramname = revisionCol.name.toLowerCase();
				handler.append("\t\t\tIF NOT(p_").append(paramname).append(" > p_old_").append(paramname).append(") THEN").append(newline).append("\t\t\t\tp_").append(getRowCountLabel().toLowerCase()).append(" := 0;").append(newline).append("\t\t\t\tp_").append(paramname).append(" := p_old_").append(paramname).append(';').append(newline).append("\t\t\t\tRETURN;").append(newline)
						.append("\t\t\tEND IF;").append(newline);
				if(pkRef) {
					handler.append("\t\tIF ");
					first = true;
					for(Col col : tab.getPrimaryKeyCols()) {
						if(first)
							first = false;
						else
							handler.append(" OR ");

						handler.append("l_").append(col.name.toLowerCase());
						if(col.required)
							handler.append(" != p_");
						else
							handler.append(" IS DISTINCT FROM p_");
						handler.append(col.name.toLowerCase());
					}
					handler.append(" THEN").append(newline).append("\t\t\tRAISE SQLSTATE '22000' USING MESSAGE = 'Row does not match primary key';").append(newline).append("\t\tEND IF;").append(newline);
				}
			}
			handler.append("\t\t\tUPDATE ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(updateName).append(newline).append("\t\t\t   SET ");
			first = true;
			for(Col col : tab.getCols()) {
				switch(col.colType) {
					case DATA:
					case REVISION:
						if(first)
							first = false;
						else
							handler.append(',').append(newline).append("\t\t\t       ");
						handler.append(col.name);
						if(col.required && !StringUtils.isBlank(col.defaultSql) && col.colType != ColType.REVISION)
							handler.append(" = COALESCE(p_").append(col.name.toLowerCase()).append(", ").append(col.name).append(')');
						else
							handler.append(" = p_").append(col.name.toLowerCase());
						break;
				}
			}
			handler.append(newline).append("\t\t\t WHERE ");
			first = true;
			for(Col col : tab.getIdentityCols()) {
				if(first)
					first = false;
				else
					handler.append(newline).append("\t\t\t   AND ");
				if(col.required)
					handler.append(col.name).append(" = p_").append(col.name.toLowerCase());
				else
					handler.append("((").append(col.name).append(" IS NOT NULL AND ").append(col.name).append(" = p_").append(col.name.toLowerCase()).append(") OR (").append(col.name).append(" IS NULL AND p_").append(col.name.toLowerCase()).append(" IS NULL))");
			}
			// filter by revision
			if(revisionCol != null) {
				if(first)
					first = false;
				else
					handler.append(newline).append("\t\t\t   AND ");
				handler.append("p_").append(revisionCol.name.toLowerCase()).append(" > ").append(revisionCol.name);
			} else if(!tab.getUniqueKeyCols().isEmpty() && !tab.getPrimaryKeyCols().isEmpty()) {
				handler.append(newline).append("\t\t\t  RETURNING ");
				first = true;
				for(Col col : tab.getPrimaryKeyCols()) {
					if(first)
						first = false;
					else
						handler.append(", ");
					handler.append(col.name);
				}
				handler.append(newline).append("\t\t\t  INTO ");
				first = true;
				for(Col col : tab.getPrimaryKeyCols()) {
					if(first)
						first = false;
					else
						handler.append(", ");
					handler.append("p_").append(col.name.toLowerCase());
				}
			}
			handler.append(';').append(newline).append("\t\t\tIF FOUND THEN").append(newline).append("\t\t\t\tGET DIAGNOSTICS p_").append(getRowCountLabel().toLowerCase()).append(" = ROW_COUNT;").append(newline).append("\t\t\tELSE").append(newline);
			if(revisionCol != null) {
				handler.append("\t\t\t\tp_").append(getRowCountLabel().toLowerCase()).append(" := 0;").append(newline).append("\t\t\t\tp_").append(revisionCol.name.toLowerCase()).append(" := p_old_").append(revisionCol.name.toLowerCase()).append(';').append(newline).append("\t\t\t\tEND IF;").append(newline).append("\t\t\tELSE").append(newline);
			}
			handler.append("\t\t\t\tBEGIN").append(newline);
			handler.append("\t\t\t\t\tINSERT INTO ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append(updateName).append('(');
			first = true;
			if(!tab.getUniqueKeyCols().isEmpty() && pkRef) {
				for(Col col : tab.getPrimaryKeyCols()) {
					if(col.getRefTab() == null)
						continue;
					if(first)
						first = false;
					else
						handler.append(", ");
					handler.append(col.name);
				}
			}
			for(Col col : tab.getIdentityCols()) {
				if(first)
					first = false;
				else
					handler.append(", ");
				handler.append(col.name);
			}
			for(Col col : tab.getCols()) {
				switch(col.colType) {
					case DATA:
					case REVISION:
						if(first)
							first = false;
						else
							handler.append(", ");
						handler.append(col.name);
						break;
				}
			}
			handler.append(')').append(newline).append("\t\t\t\t\t VALUES(");
			first = true;
			if(!tab.getUniqueKeyCols().isEmpty() && pkRef) {
				for(Col col : tab.getPrimaryKeyCols()) {
					if(col.getRefTab() == null)
						continue;
					if(first)
						first = false;
					else
						handler.append(", ");
					handler.append("p_").append(col.name.toLowerCase());
				}
			}
			for(Col col : tab.getIdentityCols()) {
				if(first)
					first = false;
				else
					handler.append(", ");
				if(col.required && !StringUtils.isBlank(col.defaultSql))
					handler.append("COALESCE(p_").append(col.name.toLowerCase()).append(", ").append(col.defaultSql).append(')');
				else
					handler.append("p_").append(col.name.toLowerCase());
			}
			for(Col col : tab.getCols()) {
				switch(col.colType) {
					case DATA:
					case REVISION:
						if(first)
							first = false;
						else
							handler.append(", ");
						if(col.required && !StringUtils.isBlank(col.defaultSql) && col.colType != ColType.REVISION)
							handler.append("COALESCE(p_").append(col.name.toLowerCase()).append(", ").append(col.defaultSql).append(')');
						else
							handler.append("p_").append(col.name.toLowerCase());
				}
			}
			handler.append(')');
			if(!tab.getPrimaryKeyCols().isEmpty() && !tab.getUniqueKeyCols().isEmpty()) {
				for(Col col : tab.getPrimaryKeyCols())
					if(col.getRefTab() == null) {
						handler.append(newline).append("\t\t\t\t\t RETURNING ");
						handler.append(col.name);
						handler.append(newline).append("\t\t\t\t\t      INTO ");
						handler.append("p_").append(col.name.toLowerCase());
					}
			}
			handler.append(';').append(newline).append("\t\t\t\t\tGET DIAGNOSTICS p_").append(getRowCountLabel().toLowerCase()).append(" = ROW_COUNT;").append(newline);
			handler.append("\t\t\t\tEXCEPTION").append(newline).append("\t\t\t\t\tWHEN unique_violation THEN").append(newline).append("\t\t\t\t\t\tCONTINUE;").append(newline);
			handler.append("\t\t\t\tEND;").append(newline);
			handler.append("\t\t\tEND IF;").append(newline).append("\t\t\tRETURN;").append(newline);
			if(tab.isPartitioned())
				handler.append("\t\tEXCEPTION").append(newline).append("\t\t\tWHEN serialization_failure THEN").append(newline).append("\t\t\t\tCONTINUE;").append(newline);
			handler.append("\t\tEND;").append(newline).append("\tEND LOOP;").append(newline);
			handler.append("\tRAISE serialization_failure;").append(newline);
			handler.append("END;").append(newline).append("$$ LANGUAGE plpgsql;").append(newline).append(newline).append("GRANT EXECUTE ON FUNCTION ");
			if(!StringUtils.isBlank(tab.getSchema()))
				handler.append(tab.getSchema()).append('.');
			handler.append("UPSERT_").append(tab.name).append('(');
			first = true;
			OUTER: for(Col col : tab.getCols()) {
				String direction;
				boolean usePrecision = true;
				switch(col.colType) {
					case PRIMARY_KEY:
						if(col.getRefTab() != null) {
							direction = null;
						} else if(hasUniqueKeys) {
							direction = "OUT";
							usePrecision = false;
						} else
							direction = null;
						break;
					case REVISION:
						direction = "INOUT";
						usePrecision = false; // Not sure on this
						revisionCol = col;
						break;
					case DATA:
					case IDENTITY_KEY:
						direction = null;
						break;
					default:
						continue OUTER;
				}
				if(first)
					first = false;
				else
					handler.append(", ");
				handler.append(newline).append("\tp_").append(col.name.toLowerCase()).append(' ');
				if(direction != null)
					handler.append(direction).append(' ');
				appendSqlType(handler, col.sqlType, usePrecision);
			}
			if(first)
				first = false;
			else
				handler.append(',');
			handler.append(newline).append("\tp_").append(getRowCountLabel().toLowerCase()).append(" OUT INTEGER) TO write_");
			if(tab.getSchema() != null)
				handler.append(tab.getSchema().toLowerCase());
			else
				handler.append("data");
			handler.append(";").append(newline);
			handler.endStatement();
		}

		// --------------------
		if(!StringUtils.isBlank(tab.getExtraSql())) {
			handler.startStatement(" ----------------------------------------------------------------------");
			handler.append(tab.getExtraSql());
			handler.endStatement();
		}
		if(log.isInfoEnabled())
			log.info("Generation complete for table " + tab.name);
	}

	protected void generateAuditTrigger(DdlHandler handler, Tab tab, String auditTriggerSchema, String auditTriggerNameSuffix) throws IOException {
		handler.startStatement(null);
		handler.append("CREATE OR REPLACE FUNCTION ");
		if(!StringUtils.isBlank(auditTriggerSchema))
			handler.append(auditTriggerSchema).append('.');
		handler.append("FTRBU_").append(auditTriggerNameSuffix).append("()").append(newline).append("\tRETURNS TRIGGER").append(newline).append("\tSECURITY DEFINER").append(newline).append("AS $$").append(newline).append("BEGIN").append(newline);
		for(Col col : tab.getCols()) {
			switch(col.colType) {
				case AUDIT_CREATE:
					handler.append("\tNEW.").append(col.name).append(" := OLD.").append(col.name).append(';').append(newline);
					break;
				case AUDIT_UPDATE:
					handler.append("\tNEW.").append(col.name).append(" := ").append(getAuditValueExpression(col)).append(';').append(newline);
					break;
			}
		}
		handler.append("\tRETURN NEW;").append(newline).append("END").append(newline).append("$$ LANGUAGE plpgsql;").append(newline);
		handler.endStatement();
		handler.startStatement(null);
		handler.append("CREATE OR REPLACE FUNCTION ");
		if(!StringUtils.isBlank(auditTriggerSchema))
			handler.append(auditTriggerSchema).append('.');
		handler.append("FTRBI_").append(auditTriggerNameSuffix).append("()").append(newline).append("\tRETURNS TRIGGER").append(newline).append("\tSECURITY DEFINER").append(newline).append("AS $$").append(newline).append("BEGIN").append(newline);
		for(Col col : tab.getCols()) {
			switch(col.colType) {
				case AUDIT_UPDATE:
				case AUDIT_CREATE:
					handler.append("\tNEW.").append(col.name).append(" := ").append(getAuditValueExpression(col)).append(';').append(newline);
					break;
			}
		}
		handler.append("\tRETURN NEW;").append(newline).append("END").append(newline).append("$$ LANGUAGE plpgsql;").append(newline);
		handler.endStatement();
	}

	protected String constructName(String name, int maxNameLength) {
		name = name.toUpperCase();
		if(name.length() > maxNameLength)
			return name.substring(0, maxNameLength - 9) + '$' + StringUtils.toHex(name.substring(maxNameLength - 9).hashCode());
		return name;
	}

	protected Appendable appendName(Appendable appendTo, String name, int maxNameLength) throws IOException {
		name = name.toUpperCase();
		if(name.length() > maxNameLength) {
			appendTo.append(name, 0, maxNameLength - 9).append('$');
			appendHex(appendTo, name.substring(maxNameLength - 9).hashCode());
		} else
			appendTo.append(name);
		return appendTo;
	}

	protected String constructIndexName(Tab tab, Ind ind) {
		if(!StringUtils.isBlank(ind.getName()))
			return ind.getName();
		StringBuilder sb = new StringBuilder();
		if(ind.isUnique())
			sb.append('U');
		else
			sb.append('I');
		sb.append("X_").append(tab.name);
		for(Col col : ind.getCols())
			sb.append('_').append(col.name);
		return constructName(sb.toString(), targetMaxNameLength);
	}

	protected void appendHex(Appendable appendTo, int i) throws IOException {
		appendTo.append(StringUtils.hexDigit1((byte) (i >>> 24))).append(StringUtils.hexDigit2((byte) (i >>> 24))).append(StringUtils.hexDigit1((byte) (i >>> 16))).append(StringUtils.hexDigit2((byte) (i >>> 16))).append(StringUtils.hexDigit1((byte) (i >>> 8))).append(StringUtils.hexDigit2((byte) (i >>> 8))).append(StringUtils.hexDigit1((byte) (i >>> 0)))
				.append(StringUtils.hexDigit2((byte) (i >>> 0)));
	}

	protected Appendable appendReplacement(Appendable appendTo, String orig, String find, String replace, char[] quotes) throws IOException {
		int s = 0;
		if(quotes != null) {
			for(int p = find(orig, quotes, s); p > 0; p = find(orig, quotes, s)) {
				appendTo.append(orig.substring(s, p).replace(find, replace));
				char q = orig.charAt(p);
				s = orig.indexOf(q, p + 1);
				if(s < 0)
					throw new IllegalArgumentException("Unbalanced quotes (" + q + ") at position " + (p + 1) + " of \"" + orig + "\"");
				s++;
				appendTo.append(orig, p, s);
			}
		}
		appendTo.append(orig.substring(s).replace(find, replace));
		return appendTo;
	}

	protected int find(String search, char[] find, int start) {
		Arrays.sort(find);
		for(int i = start; i < search.length(); i++)
			if(Arrays.binarySearch(find, search.charAt(i)) >= 0)
				return i;
		return -1;
	}

	protected String getAuditValueExpression(Col col) {
		if(!StringUtils.isBlank(col.defaultSql))
			return col.defaultSql;

		if("timestamptz".equalsIgnoreCase(col.sqlType.getTypeName()))
			return "STATEMENT_TIMESTAMP()";
		else if("timestamp".equalsIgnoreCase(col.sqlType.getTypeName())) {
			if(col.name.toUpperCase().contains("_UTC_"))
				return "STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC'";
			return "STATEMENT_TIMESTAMP()";
		} else if("timestamp".equalsIgnoreCase(col.sqlType.getTypeName())) {
			if(col.name.toUpperCase().contains("_UTC_"))
				return "CAST(STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC' AS DATE)";
			return "CAST(STATEMENT_TIMESTAMP() AS DATE)";
		}
		return "'?' /* Audit Value for " + col.name + " */";
	}

	protected static final Pattern IGNORE_DEFAULT_PATTERN = Pattern.compile("\\s*nextval\\s*\\(.*", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	protected String getUnknownValueExpression(Col col) {
		if(!StringUtils.isBlank(col.defaultSql) && !IGNORE_DEFAULT_PATTERN.matcher(col.defaultSql).matches())
			return col.defaultSql;
		else if(!col.required)
			return "NULL";
		else {
			switch(col.sqlType.getTypeCode()) {
				case Types.BIGINT:
				case Types.BIT:
				case Types.DECIMAL:
				case Types.FLOAT:
				case Types.DOUBLE:
				case Types.INTEGER:
				case Types.NUMERIC:
				case Types.REAL:
				case Types.SMALLINT:
				case Types.TINYINT:
					return "0";
				case Types.CHAR:
				case Types.LONGNVARCHAR:
				case Types.LONGVARCHAR:
				case Types.NCHAR:
				case Types.NVARCHAR:
				case Types.VARCHAR:
					int precision;
					if(col.sqlType.getPrecision() != null)
						precision = col.sqlType.getPrecision();
					else
						precision = 1;
					if(precision >= 7)
						return "'Unknown'";
					char[] chars = new char[precision + 2];
					chars[0] = '\'';
					chars[precision + 1] = '\'';
					for(int p = 1; p <= precision; p++)
						chars[p] = '-';
					return new String(chars);
				default:
					return "'?' /* Type: " + col.sqlType + " */";
			}
		}
	}

	protected Appendable appendSqlType(Appendable appendTo, SQLType sqlType, boolean usePrecision) throws IOException {
		if(sqlType.getTypeName() == null && sqlType.getTypeCode() != 0) {
			String typeName = SQLTypeUtils.getTypeCodeName(sqlType.getTypeCode());
			appendTo.append(typeName != null ? typeName : "<UNKNOWN>");
		} else {
			appendTo.append(sqlType.getTypeName());
		}
		if(usePrecision && sqlType.getPrecision() != null) {
			appendTo.append('(').append(sqlType.getPrecision().toString());
			if(sqlType.getScale() != null) {
				appendTo.append(',').append(sqlType.getScale().toString());
			}
			appendTo.append(')');
		}
		return appendTo;
	}

	protected void generateIndex(DdlHandler handler, Tab tab, String indexName, boolean unique, Iterable<Col> cols, String where, String tablespace) throws IOException {
		// --- build index
		handler.startStatement(null);
		handler.append("CREATE ");
		if(unique)
			handler.append("UNIQUE ");
		handler.append("INDEX ").append(indexName).append(" ON ");
		if(!StringUtils.isBlank(tab.getSchema()))
			handler.append(tab.getSchema()).append('.');
		if(tab.isPartitioned())
			handler.append(partitionTablePrefix);
		handler.append(tab.name);
		handler.append('(');
		boolean first = true;
		for(Col col : cols) {
			if(first)
				first = false;
			else
				handler.append(", ");
			handler.append(col.name);
		}
		handler.append(')');

		if(!StringUtils.isBlank(where))
			handler.append(" WHERE ").append(where);
		if(!StringUtils.isBlank(tablespace))
			handler.append(" TABLESPACE ").append(tablespace);

		handler.append(';');
		handler.endStatement();
	}

	public String getRowCountLabel() {
		return rowCountLabel;
	}

	public void setRowCountLabel(String rowCountLabel) {
		this.rowCountLabel = rowCountLabel;
	}
}
