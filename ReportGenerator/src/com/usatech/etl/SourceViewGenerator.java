package com.usatech.etl;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Types;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import simple.io.Log;
import simple.lang.SystemUtils;
import simple.sql.SQLType;
import simple.text.StringUtils;

public class SourceViewGenerator {
	private static final Log log = Log.getLog();
	protected static final String newline = SystemUtils.getNewLine();
	protected final Map<String, Namer> namers = new HashMap<String, Namer>();
	protected static final Namer DEFAULT_NAMER = new Namer("\"", null, 30, 30, 30, 30, 30, false, false, true);

	public void generate(String filePrefix, String fileSuffix, Db<? extends Tab> db) throws IOException {
		Map<String, Writer> writers = new LinkedHashMap<String, Writer>();
		try {
			for(Tab tab : db.getTabs()) {
				generate(writers, filePrefix, fileSuffix, tab);
			}
		} finally {
			for(Writer writer : writers.values())
				try {
					writer.close();
				} catch(IOException e) {
					log.info("Could not close writer: " + e.getMessage(), e);
				} catch(RuntimeException e) {
					log.info("Could not close writer: " + e.getMessage(), e);
				} catch(Error e) {
					log.info("Could not close writer: " + e.getMessage(), e);
				}
		}
		if(log.isInfoEnabled())
			log.info("Generation of Source Views complete");
	}
	
	protected void generate(Map<String, Writer> writers, String filePrefix, String fileSuffix, Tab tab) throws IOException {
		if(!tab.hasSource())
			return;
		Writer writer = writers.get(tab.getSourceDsn());
		if(writer == null) {
			StringBuilder sb = new StringBuilder();
			if(!StringUtils.isBlank(filePrefix))
				sb.append(filePrefix);
			else
				sb.append("SourceViewStubs_");
			if(!StringUtils.isBlank(tab.getSourceDsn()))
				sb.append(tab.getSourceDsn());
			if(fileSuffix != null)
				sb.append(fileSuffix);
			else
				sb.append(".sql");
			writer = new FileWriter(sb.toString());
			writers.put(tab.getSourceDsn(), writer);
		}
		if(log.isInfoEnabled())
			log.info("Generating Source View for table " + tab.name);
		Namer namer = getNamer(tab.getSourceDsn());
		writer.append("CREATE OR REPLACE ");
		SourceType sourceType = tab.getSourceType();
		if(sourceType == SourceType.EXECUTABLE)
			writer.append("FUNCTION");
		else
			writer.append("VIEW");
		writer.append(' ');
		if(!StringUtils.isBlank(tab.getSourceSchema()))
			writer.append(tab.getSourceSchema()).append('.');
		if(!StringUtils.isBlank(tab.getSourceCatalog()))
			writer.append(tab.getSourceCatalog()).append('.');
		writer.append(tab.getSourceTable()).append('(');
		
		boolean first = true;
		for(Col col : tab.getCols()) {
			if(col.getRefTab() == null || !StringUtils.isBlank(col.getRefTab().getPrecalcExpression())) {
				switch(col.colType) {
					case AUDIT_CREATE:
					case AUDIT_UPDATE:
					case CUSTOM:
					case REVISION:
						break;
					case PRIMARY_KEY:
						if(!tab.getUniqueKeyCols().isEmpty())
							break;
					case DATA:
						if(sourceType == SourceType.EXECUTABLE)
							break;
					default:
						if(first)
							first = false;
						else
							writer.append(',');
						writer.append(newline).append('\t');
						if(sourceType == SourceType.EXECUTABLE) {
							/*
							StringBuilder sb = new StringBuilder();
							sb.append('p').append(getDataTypeChar(col.sqlType)).append('_').append(col.name);
							String paramName = getName(sb.toString()).toLowerCase();*/
							namer.appendExpression(writer, col.name, NameType.COLUMN).append(' ').append(col.sqlType.toString()).append(" /* ").append(col.name).append(" */");
						} else {
							namer.appendExpression(writer, col.name, NameType.COLUMN);
							if(!namer.getName(col.name, NameType.COLUMN).equalsIgnoreCase(col.name))
								writer.append(" /* ").append(col.name).append(" */");
						}
				}
			} else {
				switch(col.colType) {
					case AUDIT_CREATE:
					case AUDIT_UPDATE:
					case CUSTOM:
					case REVISION:
						break;
					case DATA:
						if(sourceType == SourceType.EXECUTABLE)
							break;
					default:
						if(first)
							first = false;
						else
							writer.append(',');
						writer.append(newline).append('\t');
						boolean refFirst = true;
						for(Col refCol : col.getRefTab().getIdentityCols()) {
							if(refFirst)
								refFirst = false;
							else
								writer.append(',').append(newline).append('\t');
							String colFullName = col.name + '$' + refCol.name;
							if(sourceType == SourceType.EXECUTABLE) {
								/*
								StringBuilder sb = new StringBuilder();
								sb.append('p').append(getDataTypeChar(col.sqlType)).append('_').append(colFullName);
								String paramName = getName(sb.toString()).toLowerCase();
								*/
								namer.appendExpression(writer, col.name, NameType.COLUMN).append(' ').append(col.sqlType.toString()).append(" /* ").append(colFullName).append(" */");
							} else {
								namer.appendExpression(writer, colFullName, NameType.COLUMN);
								if(!namer.getName(colFullName, NameType.COLUMN).equalsIgnoreCase(colFullName))
									writer.append(" /* ").append(colFullName).append(" */");
							}
						}
						if(refFirst)
							throw new IOException("Referenced table '" + col.getRefTab().name + "' referenced by '" + tab.name + '.' + col.name + "' has no unique columns");
				}
			}
		}
		if(sourceType == SourceType.EXECUTABLE) {
			/* For postgres:
			 * 
			      RETURNS TABLE(
			        SOURCE_GLOBAL_SESSION_CD MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
			        CALL_START_TS TIMESTAMP WITH TIME ZONE,
			        CALL_END_TS TIMESTAMP WITH TIME ZONE,
			        IMPORT_COMPLETED_TS TIMESTAMP WITH TIME ZONE,
			        CLIENT_IP_ADDR MAIN.DEVICE_SESSION.CLIENT_IP_ADDRESS%TYPE,
			        CLIENT_PORT MAIN.DEVICE_SESSION.CLIENT_PORT%TYPE,
			        NET_LAYER_DESC MAIN.NET_LAYER.NET_LAYER_DESC%TYPE,
					SERVER_NAME MAIN.NET_LAYER.SERVER_NAME%TYPE,
					SERVER_PORT MAIN.NET_LAYER.SERVER_PORT_NUM%TYPE) 
			    SECURITY DEFINER
			AS $$
			BEGIN
			RETURN QUERY 
			SELECT
			    SRC.SOURCE_GLOBAL_SESSION_CD /* SOURCE_GLOBAL_SESSION_CD * /,
			    DS.CALL_IN_START_TS /* CALL_START_TS * /,
			    DS.CALL_IN_FINISH_TS/* CALL_END_TS * /,
			    DS.CREATED_UTC_TS AT TIME ZONE 'GMT'/* IMPORT_COMPLETED_TS * /,
			    COALESCE(DS.CLIENT_IP_ADDRESS, 'Unknown') /* CLIENT_IP_ADDR * /,
			    DS.CLIENT_PORT /* CLIENT_PORT * /,
				COALESCE(NL.NET_LAYER_DESC, 'Unknown') /* NET_LAYER_DESC * /,
				COALESCE(NL.SERVER_NAME, 'Unknown') /* SERVER_NAME * /,
				NL.SERVER_PORT_NUM /* SERVER_PORT * /
			  FROM (SELECT pn_source_global_session_cd SOURCE_GLOBAL_SESSION_CD) SRC
			  LEFT OUTER JOIN MAIN.DEVICE_SESSION DS ON SRC.SOURCE_GLOBAL_SESSION_CD = DS.GLOBAL_SESSION_CD
			  LEFT OUTER JOIN MAIN.NET_LAYER NL ON DS.NET_LAYER_ID = NL.NET_LAYER_ID;
			END;
			$$ LANGUAGE plpgsql;
			 */
			writer.append(')').append(newline).append("\tRETURN PKG_GLOBAL.REF_CURSOR").append(newline).append("IS").append(newline).append("\tl_cur PKG_GLOBAL.REF_CURSOR;")
				.append(newline).append("BEGIN").append(newline).append("\tOPEN l_cur FOR ");
		} else
			writer.append(") AS").append(newline);
		writer.append("SELECT");
		first = true;
		for(Col col : tab.getCols()) {
			if(col.getRefTab() == null || !StringUtils.isBlank(col.getRefTab().getPrecalcExpression())) {
				switch(col.colType) {
					case AUDIT_CREATE:
					case AUDIT_UPDATE:
					case CUSTOM:
					case REVISION:
						break;
					case PRIMARY_KEY:
						if(!tab.getUniqueKeyCols().isEmpty())
							break;
					case IDENTITY_KEY:
						if(sourceType == SourceType.EXECUTABLE) {
							break;
						}
					default:
						if(first)
							first = false;
						else
							writer.append(',');
						writer.append(newline).append('\t');
						if(sourceType == SourceType.EXECUTABLE)
							writer.append('\t');
						writer.append(" /* ").append(col.name).append(" */");
				}
			} else {
				switch(col.colType) {
					case AUDIT_CREATE:
					case AUDIT_UPDATE:
					case CUSTOM:
					case REVISION:
						break;
					default:
						if(first)
							first = false;
						else
							writer.append(',');
						writer.append(newline).append('\t');
						boolean refFirst = true;
						for(Col refCol : col.getRefTab().getIdentityCols()) {
							if(refFirst)
								refFirst = false;
							else
								writer.append(",").append(newline).append('\t');
							if(sourceType == SourceType.EXECUTABLE) {
								writer.append('\t');
								if(col.colType == ColType.IDENTITY_KEY) {
									/*
									StringBuilder sb = new StringBuilder();
									sb.append('p').append(getDataTypeChar(col.sqlType)).append('_').append(col.name + '$' + refCol.name);
									String paramName = getName(sb.toString()).toLowerCase();
									*/
									namer.appendExpression(writer, col.name, NameType.COLUMN);
								}
							}
							writer.append(" /* ").append(col.name).append('$').append(refCol.name).append(" */");
						}
				}
			}
		}
		if(sourceType == SourceType.EXECUTABLE)
			writer.append(newline).append('\t').append(';').append(newline).append("\tRETURN l_cur;").append(newline).append("END;").append(newline).append('/');
		else
			writer.append(newline).append(newline).append(';');
		writer.append(newline).append(newline);
		writer.flush();
	}

	public Namer getNamer(String sourceDsn) {
		Namer namer = namers.get(sourceDsn);
		return namer == null ? DEFAULT_NAMER : namer;
	}

	public void setNamer(String sourceDsn, Namer namer) {
		namers.put(sourceDsn, namer);
	}

	protected char getDataTypeChar(SQLType sqlType) {
		switch(sqlType.getTypeCode()) {
			case Types.ARRAY:
				return 't';
			case Types.BIGINT:
			case Types.DECIMAL:
			case Types.DOUBLE:
			case Types.FLOAT:
			case Types.INTEGER:
			case Types.NUMERIC:
			case Types.SMALLINT:
			case Types.TINYINT:
				return 'n';
			case Types.CHAR:
			case Types.NCHAR:
				return 'c';
			case Types.VARCHAR:
			case Types.LONGNVARCHAR:
			case Types.LONGVARCHAR:
			case Types.NVARCHAR:
				return 'v';
			case Types.DATE:
			case Types.TIME:
			case Types.TIMESTAMP:
				return 'd';
			case Types.BINARY:
			case Types.BIT:
			case Types.BOOLEAN:
			case Types.LONGVARBINARY:
			case Types.VARBINARY:
				return 'b';
			case Types.BLOB:
			case Types.CLOB:
				return 'l';
			default:
				return 'o';
		}
	}

	protected static Appendable appendLabel(Col col, Appendable appendTo) throws IOException {
		return appendLabel(col.name, appendTo);
	}

	protected static Appendable appendLabel(String name, Appendable appendTo) throws IOException {
		boolean first = true;
		for(int i = 0; i < name.length(); i++) {
			char ch = name.charAt(i);
			if(Character.isLetterOrDigit(ch)) {
				if(first) {
					appendTo.append(Character.toUpperCase(ch));
					first = false;
				} else {
					appendTo.append(Character.toLowerCase(ch));
				}
			} else if(!first) {
				appendTo.append(' ');
				first = true;
			}
		}
		return appendTo;
	}
}
