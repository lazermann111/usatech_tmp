package com.usatech.etl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.LinkedHashMap;

import simple.io.ConfigSource;
import simple.io.LoadingException;
import simple.util.CollectionUtils;
import simple.xml.ObjectBuilder;

public class GenerateMain {

	/**
	 * @param args
	 * @throws IOException
	 * @throws LoadingException
	 */
	public static void main(String[] args) throws IOException, LoadingException {
		File baseDir = new File(System.getProperty("rootSourceDir", "D:\\Development\\Database Projects"));
		File scriptsDir = new File(baseDir, "DatabaseScripts/REPTP/RDW");
		boolean create = true;
		boolean views = true;
		// boolean fields = false;
		ConfigSource cfgSrc = ConfigSource.createConfigSource("rdw_metadata.xml");
		Db<Tab> db = new Db<Tab>(CollectionUtils.synchronizedConcurrentMap(new LinkedHashMap<String, Tab>())) {
			@Override
			protected Tab createTab(ObjectBuilder tabBuilder) {
				return new Tab(tabBuilder);
			}
		};
		InputStream in = cfgSrc.getInputStream();
		try {
			db.load(in, "RDW", null);
			if(create) {
				Writer writer = new BufferedWriter(new FileWriter(new File(scriptsDir, "pg_rdw_setup.sql")));
				try {
					DdlHandler handler = new ScriptDdlHandler(writer);
					DdlGenerator gen = new DdlGenerator();
					gen.generate(handler, db);
				} finally {
					writer.close();
				}
			}
			if(views) {
				SourceViewGenerator gen = new SourceViewGenerator();
				gen.setNamer("OPER", new Namer("\"", "$#", 0, 30, 30, 30, 30, true, false, false));
				gen.setNamer("MAIN", new Namer("\"", null, 63, 63, 63, 63, 63, false, true, false));
				gen.generate(scriptsDir.getAbsolutePath() + "/SourceViewsStubs_", ".sql", db);
			}
		} finally {
			in.close();
		}
	}

}
