package com.usatech.etl;

import java.io.IOException;

import simple.lang.SystemUtils;

public class ScriptDdlHandler implements DdlHandler {
	protected final Appendable writer;
	protected final String newline = SystemUtils.getNewLine();
	
	public ScriptDdlHandler(Appendable writer) {
		super();
		this.writer = writer;
	}

	@Override
	public Appendable append(CharSequence csq) throws IOException {
		writer.append(csq);
		return this;
	}

	@Override
	public Appendable append(CharSequence csq, int start, int end) throws IOException {
		writer.append(csq, start, end);
		return this;
	}

	@Override
	public Appendable append(char c) throws IOException {
		writer.append(c);
		return this;
	}

	@Override
	public void start(int buildNumber) throws IOException {
		writer.append("-- ======================================================================").append(newline)
			.append("-- ===   Sql Script for Database : Postgresql").append(newline)
			.append("-- ===").append(newline)
			.append("-- === Build : ").append(String.valueOf(buildNumber)).append(newline)
			.append("-- ======================================================================").append(newline)
			.append(newline);
	}

	@Override
	public void startStatement(String comment) throws IOException {
		if(comment != null)
			writer.append(newline).append("--").append(comment).append(newline);
		writer.append(newline);
	}

	@Override
	public void endStatement() throws IOException {
		writer.append(newline);
	}

	@Override
	public void end() throws IOException {
		writer.append("-- ======================================================================").append(newline)
		.append(newline);
	}
}
