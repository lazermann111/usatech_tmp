package com.usatech.etl;

enum SourceType {
	SELECTABLE, EXECUTABLE, NONE, UNKNOWN
}