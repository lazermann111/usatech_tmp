package com.usatech.etl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call;
import simple.db.Call.BatchUpdate;
import simple.db.CallNotFoundException;
import simple.db.Column;
import simple.db.ConnectionGroup;
import simple.db.ConnectionHolder;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceNotFoundException;
import simple.db.ParameterException;
import simple.db.TooManyRowsException;
import simple.io.Abortable;
import simple.io.ConfigSource;
import simple.io.CopyInputStream;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.results.BeanException;
import simple.results.ListResults;
import simple.results.Results;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.StringUtils;
import simple.util.CompositeMap;
import simple.xml.ObjectBuilder;

import com.usatech.etl.DataLoaderTab.SourceInfo;

public class DataLoader {
	private static final Log log = Log.getLog();

	protected ConfigSource metadataFile;
	protected String revisionLabel = "REVISION";
	protected String rowCountLabel = "ROW_COUNT";
	protected SQLType rowCountSqlType = new SQLType(Types.INTEGER);
	protected long logExecuteTimeThreshhold;
	protected int queryTimeout;
	protected int defaultValueQueryTimeout = 2000;
	protected String targetDsn;
	protected Namer targetNamer;
	protected int maxNameLength = 30;
	protected String revisionGeneratorCallId = "GENERATE_NEXT_REVISION";
	protected Call revisionGeneratorCall;
	protected int lookupCacheSize = 1000;
	protected int lookupCacheConcurrency = 8;
	protected boolean defaultOnUpsert = false;
	protected boolean strictSourceColumnOrder = false;
	protected boolean lazyAccessOnRangeSelect = true;
	protected int minDistinctForRange = 21;
	protected int idealBatchSize = 100;
	protected static final Pattern changingDefaultPattern = Pattern.compile("[^']*(?:(?:'[^']*){2})*nextval\\s*[(].*", Pattern.CASE_INSENSITIVE);
	protected static final char[] SORTED_ASCII_CHARS = new char[95];
	static {
		for(int i = 0; i < SORTED_ASCII_CHARS.length; i++)
			SORTED_ASCII_CHARS[i] = (char) (32 + i);
	}
	protected static class RowNotFoundServiceException extends ServiceException {
		private static final long serialVersionUID = -6517319526139803650L;
	}

	public static Object getEqualsAnyExpr(ConnectionHolder sourceConnHolder) throws SQLException {
		try {
			if(sourceConnHolder != null && DataLayerMgr.getInnermostDBHelper(sourceConnHolder.getConnection()).getClass().getSimpleName().startsWith("Oracle")) {
				return "MEMBER OF ?";
			}
		} catch(DataSourceNotFoundException e) {
			throw new SQLException(e);
		}
		return "= ANY(?)";
	}

	public static Object getNotEqualsAnyExpr(ConnectionHolder sourceConnHolder) throws SQLException {
		try {
			if(sourceConnHolder != null && DataLayerMgr.getInnermostDBHelper(sourceConnHolder.getConnection()).getClass().getSimpleName().startsWith("Oracle")) {
				return "NOT MEMBER OF ?";
			}
		} catch(DataSourceNotFoundException e) {
			throw new SQLException(e);
		}
		return "!= ANY(?)";
	}

	public static StringBuilder appendDistinctFromExpr(ConnectionHolder sourceConnHolder, StringBuilder appendTo, String expression) throws SQLException {
		try {
			if(sourceConnHolder != null && DataLayerMgr.getInnermostDBHelper(sourceConnHolder.getConnection()).getClass().getSimpleName().startsWith("Oracle")) {
				return appendTo.append("DECODE(").append(expression).append(", ?, 'Y', 'N') = 'N'");
			}
		} catch(DataSourceNotFoundException e) {
			throw new SQLException(e);
		}
		return appendTo.append(expression).append(" IS DISTINCT FROM ?");
	}

	public static int appendNotDistinctFromExpr(ConnectionHolder sourceConnHolder, StringBuilder appendTo, String expression) throws SQLException {
		return appendNotDistinctFromExpr(sourceConnHolder, appendTo, expression, "?");
	}

	public static int appendNotDistinctFromExpr(ConnectionHolder sourceConnHolder, StringBuilder appendTo, String expression1, String expression2) throws SQLException {
		try {
			if(sourceConnHolder != null && DataLayerMgr.getInnermostDBHelper(sourceConnHolder.getConnection()).getClass().getSimpleName().startsWith("Oracle")) {
				appendTo.append("DECODE(").append(expression1).append(", ").append(expression2).append(", 'Y', 'N') = 'Y'");
				return 1;
			}
		} catch(DataSourceNotFoundException e) {
			throw new SQLException(e);
		}
		// return appendTo.append(expression1).append(" IS NOT DISTINCT FROM ").append(expression2);
		appendTo.append("((").append(expression1).append(" = ").append(expression2).append(" AND ").append(expression2).append(" IS NOT NULL) OR (").append(expression1).append(" IS NULL AND ").append(expression2).append(" IS NULL))");
		return 3;
	}

	public static boolean useSelectForCall(ConnectionHolder sourceConnHolder) throws SQLException {
		try {
			if(DataLayerMgr.getInnermostDBHelper(sourceConnHolder.getConnection()).getClass().getSimpleName().startsWith("Postgres")) {
				return true;
			}
		} catch(DataSourceNotFoundException e) {
			throw new SQLException(e);
		}
		return false;
	}

	public static Object getEqualsAnyExpr(String dsn, ConnectionGroup connGroup) throws SQLException {
		try {
			if(connGroup != null && DataLayerMgr.getInnermostDBHelper(connGroup.getConnection(dsn)).getClass().getSimpleName().startsWith("Oracle")) {
				return "MEMBER OF ?";
			}
		} catch(DataLayerException e) {
			throw new SQLException(e);
		}
		return "= ANY(?)";
	}

	public static Object getNotEqualsAnyExpr(String dsn, ConnectionGroup connGroup) throws SQLException {
		try {
			if(connGroup != null && DataLayerMgr.getInnermostDBHelper(connGroup.getConnection(dsn)).getClass().getSimpleName().startsWith("Oracle")) {
				return "NOT MEMBER OF ?";
			}
		} catch(DataLayerException e) {
			throw new SQLException(e);
		}
		return "!= ANY(?)";
	}

	public static StringBuilder appendDistinctFromExpr(String dsn, ConnectionGroup connGroup, StringBuilder appendTo, String expression) throws SQLException {
		try {
			if(connGroup != null && DataLayerMgr.getInnermostDBHelper(connGroup.getConnection(dsn)).getClass().getSimpleName().startsWith("Oracle")) {
				return appendTo.append("DECODE(").append(expression).append(", ?, 'Y', 'N') = 'N'");
			}
		} catch(DataLayerException e) {
			throw new SQLException(e);
		}
		return appendTo.append(expression).append(" IS DISTINCT FROM ?");
	}

	public static int appendNotDistinctFromExpr(String dsn, ConnectionGroup connGroup, StringBuilder appendTo, String expression) throws SQLException {
		return appendNotDistinctFromExpr(dsn, connGroup, appendTo, expression, "?");
	}

	public static int appendNotDistinctFromExpr(String dsn, ConnectionGroup connGroup, StringBuilder appendTo, String expression1, String expression2) throws SQLException {
		try {
			if(connGroup != null && DataLayerMgr.getInnermostDBHelper(connGroup.getConnection(dsn)).getClass().getSimpleName().startsWith("Oracle")) {
				appendTo.append("DECODE(").append(expression1).append(", ").append(expression2).append(", 'Y', 'N') = 'Y'");
				return 1;
			}
		} catch(DataLayerException e) {
			throw new SQLException(e);
		}
		// appendTo.append(expression1).append(" IS NOT DISTINCT FROM ").append(expression2);
		appendTo.append("((").append(expression1).append(" = ").append(expression2).append(" AND ").append(expression2).append(" IS NOT NULL) OR (").append(expression1).append(" IS NULL AND ").append(expression2).append(" IS NULL))");
		return 3;
	}

	public static boolean useSelectForCall(String dsn, ConnectionGroup connGroup) throws SQLException {
		try {
			if(DataLayerMgr.getInnermostDBHelper(connGroup.getConnection(dsn)).getClass().getSimpleName().startsWith("Postgres")) {
				return true;
			}
		} catch(DataLayerException e) {
			throw new SQLException(e);
		}
		return false;
	}

	public boolean isDefaultValue(Col col, Object value) throws ServiceException {
		if(!col.required && (value == null || value.toString().isEmpty()))
			return true;
		Class<? extends Object> cls = SQLTypeUtils.getJavaType(col.sqlType);
		Object cvalue = convertSafely(cls, value);
		Object defaultValue = col.getDefaultValue(this);
		if(defaultValue != null)
			return ConvertUtils.areEqual(convertSafely(cls, defaultValue), cvalue);
		int precision = (col.sqlType.getPrecision() != null ? col.sqlType.getPrecision() : -1);
		switch(col.sqlType.getTypeCode()) {
			case Types.BIGINT:
			case Types.BIT:
			case Types.DECIMAL:
			case Types.FLOAT:
			case Types.DOUBLE:
			case Types.INTEGER:
			case Types.NUMERIC:
			case Types.REAL:
			case Types.SMALLINT:
			case Types.TINYINT:
				return ConvertUtils.areEqual(convertSafely(cls, 0), cvalue);
			case Types.CHAR:
			case Types.LONGNVARCHAR:
			case Types.LONGVARCHAR:
			case Types.NCHAR:
			case Types.NVARCHAR:
			case Types.VARCHAR:
				if(precision >= 7)
					return ConvertUtils.areEqual(convertSafely(cls, "Unknown"), cvalue);
				return ConvertUtils.areEqual(convertSafely(cls, "-"), cvalue);
			default:
				return false;
		}
	}

	protected static Object convertSafely(Class<? extends Object> cls, Object value) {
		try {
			return ConvertUtils.convert(cls, value);
		} catch(ConvertException e) {
			return value;
		}
	}

	protected final Db<DataLoaderTab> db = new Db<DataLoaderTab>(new ConcurrentHashMap<String, DataLoaderTab>()) {
		@Override
		protected DataLoaderTab createTab(ObjectBuilder tabBuilder) {
			return new DataLoaderTab(DataLoader.this, tabBuilder);
		}
	};
	protected class MetadataLoader extends ReentrantLock implements Loader {
		private static final long serialVersionUID = 7358683176351766763L;

		@Override
		public void load(ConfigSource src) throws LoadingException {
			// We lock for the benefit of updateMetadata() so it can ensure a consistent state
			lock();
			try {
				db.load(src.getInputStream(), getTargetDsn(), null);
			} catch(IOException e) {
				throw new LoadingException(e);
			} finally {
				unlock();
			}
		}
	};
	protected final MetadataLoader metadataLoader = new MetadataLoader();

	public void initialize() throws IOException, LoadingException, CallNotFoundException {
		readMetadata();
		if(!StringUtils.isBlank(getRevisionGeneratorCallId()))
			revisionGeneratorCall = DataLayerMgr.getGlobalDataLayer().findCall(getRevisionGeneratorCallId());
	}

	public void clearLookups() {
		for(DataLoaderTab tab : db.getTabs())
			tab.lookupCache.clear();
	}
	protected void readMetadata() throws IOException, LoadingException {
		ConfigSource cfg = getMetadataFile();
		if(cfg == null) {
			cfg = ConfigSource.createConfigSource("resource:loader-metadata.xml");
			setMetadataFile(cfg);
		}
		cfg.registerLoader(metadataLoader, 5000, 300000);
		cfg.reload();
	}

	public long repopulateColumns(String table, String source, Set<String> columns, ConnectionGroup connGroup) throws ServiceException {
		ConfigSource cfg = getMetadataFile();
		if(cfg != null)
			try {
				cfg.reload();
			} catch(LoadingException e) {
				log.error("Could not reload metadata file '" + cfg + "'; continuing with old definitions", e);
			}
		DataLoaderTab tab = db.getTab(table);
		if(tab == null)
			throw new ServiceException("Table '" + table + "' is not defined");
		final long revision;
		if(tab.getRevisionPnt() == null || revisionGeneratorCall == null)
			revision = 0L;
		else {
			try {
				Connection conn = connGroup.getConnection(revisionGeneratorCall.getDataSourceName(), null);
				Object[] ret = null;
				try {
					ret = revisionGeneratorCall.executeCall(conn, null, null);
				} finally {
					if(!conn.getAutoCommit()) {
						if(ret != null)
							conn.commit();
						else
							conn.rollback();
					}
				}
				if(ret.length < 2)
					throw new ServiceException("Could not get revision from call '" + getRevisionGeneratorCallId() + "' - it must have one out parameter");
				revision = ConvertUtils.getLong(ret[1]);
			} catch(SQLException e) {
				throw new ServiceException("Could not generate revision", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not generate revision", e);
			} catch(ConvertException e) {
				throw new ServiceException("Could not generate revision", e);
			}
		}
		return repopulateColumns(revision, tab, source, columns, connGroup);
	}

	protected static interface ValuePreparer {
		public boolean prepareNext(Map<String, Object> input);
	}

	protected long repopulateColumns(long revision, DataLoaderTab tab, String source, Set<String> columns, ConnectionGroup connGroup) throws ServiceException {
		long cnt = 0;
		// find unique key columns with low cardinality and get full unique key ranges
		Set<String> filterColumns;
		final ValuePreparer prep;
		try {
			final SourceInfo sourceInfo = tab.getSourceInfo(source, connGroup);
			switch(sourceInfo.sourceType) {
				case SELECTABLE:
					final SourceValueIterator[] svis = new SourceValueIterator[tab.getUniqueKeyNames().size()];
					Call statCall = tab.getTargetStatsCall(getMinDistinctForRange(), connGroup);
					Results statResults = null;
					try {
						statResults = statCall.executeQuery(connGroup.getConnection(statCall.getDataSourceName(), true), null, null);
					} catch(ParameterException e) {
						throw new ServiceException("Could not run stats query for " + tab, e);
					} catch(SQLException e) {
						throw new ServiceException("Could not run stats query for " + tab, e);
					} catch(DataLayerException e) {
						throw new ServiceException("Could not get target connection", e);
					}
					try {
					if(!statResults.next())
						throw new ServiceException("Stats query for " + tab + " returned zero rows");
					try {
						long count = statResults.getValue("count", Long.class);
						if(count == 0)
							return 0L;
						long per = count;
						filterColumns = new LinkedHashSet<String>();
						Iterator<String> iter = tab.uniqueKeyNames.iterator();
						for(int i = 0; i < svis.length; i++) {
							String colname = iter.next();
							Col col = tab.getCol(colname);
							Integer cardinality = statResults.getValue(colname + ".cardinality", Integer.class);
							if(cardinality == null)
								return 0L; // nothing to do, assumably
							if(cardinality < getMinDistinctForRange()) {
								Object[] values = statResults.getValue(colname + ".values", Object[].class);
								svis[i] = new DiscreteSourceValueIterator(colname, values);
								per = per / cardinality;
								filterColumns.add(colname);
							} else {
								int s = getIdealBatchSize();
								Object maxValue = statResults.getValue(colname + ".max");
								Object minValue = statResults.getValue(colname + ".min");
								if(maxValue instanceof Long || maxValue instanceof Integer || maxValue instanceof Short || maxValue instanceof Byte) {
									long max = ConvertUtils.getLong(maxValue);
									long min = ConvertUtils.getLong(minValue);
									long x = (max - min) * s;
									long inc;
									if(x <= 0)
										inc = 1;
									else {
										inc = x / per;
										if(x <= 0)
											inc = 1;
									}
									svis[i] = new LongRangeSourceValueIterator(colname, min, max, inc, !col.required);
								} else if(maxValue instanceof Double || maxValue instanceof Float) {
									double max = ConvertUtils.getDouble(maxValue);
									double min = ConvertUtils.getDouble(minValue);
									double x = (max - min) * s;
									double inc;
									if(x <= 0.0)
										inc = 1.0;
									else {
										inc = x / per;
										if(x <= 0.0)
											inc = 1.0;
									}
									svis[i] = new DoubleRangeSourceValueIterator(colname, min, max, inc, !col.required);
								} else if(maxValue instanceof BigInteger) {
									BigInteger max = ConvertUtils.convert(BigInteger.class, maxValue);
									BigInteger min = ConvertUtils.convert(BigInteger.class, minValue);
									BigInteger x = max.subtract(min).multiply(BigInteger.valueOf(s));
									BigInteger inc;
									if(x.compareTo(BigInteger.ZERO) <= 0)
										inc = BigInteger.ONE;
									else {
										inc = x.divide(BigInteger.valueOf(per));
										if(inc.compareTo(BigInteger.ZERO) <= 0)
											inc = BigInteger.ONE;
									}
									svis[i] = new BigIntegerRangeSourceValueIterator(colname, min, max, inc, !col.required);
								} else if(maxValue instanceof Number) {
									BigDecimal max = ConvertUtils.convert(BigDecimal.class, maxValue);
									BigDecimal min = ConvertUtils.convert(BigDecimal.class, minValue);
									BigDecimal x = max.subtract(min).multiply(BigDecimal.valueOf(s));
									BigDecimal inc;
									if(x.compareTo(BigDecimal.ZERO) <= 0)
										inc = new BigDecimal(1);
									else {
										inc = x.divide(BigDecimal.valueOf(per));
										if(inc.compareTo(BigDecimal.ZERO) <= 0)
											inc = new BigDecimal(1);
									}
									svis[i] = new BigDecimalRangeSourceValueIterator(colname, min, max, inc, !col.required);

								} else if(maxValue instanceof Date || maxValue instanceof Calendar) {
									long max = ConvertUtils.getLong(maxValue);
									long min = ConvertUtils.getLong(minValue);
									long x = (max - min) * s;
									long inc;
									if(x <= 0)
										inc = 1;
									else {
										inc = x / per;
										if(x <= 0)
											inc = 1;
									}
									svis[i] = new LongRangeSourceValueIterator(colname, min, max, inc, !col.required);
								} else {
									char[] max = ConvertUtils.getString(maxValue, true).toCharArray();
									char[] min = ConvertUtils.getString(minValue, true).toCharArray();
									BigInteger diff = StringUtils.getGapBetween(min, max, SORTED_ASCII_CHARS);
									BigInteger x = diff.multiply(BigInteger.valueOf(s));
									BigInteger inc;
									if(x.compareTo(BigInteger.ZERO) <= 0)
										inc = BigInteger.ONE;
									else {
										inc = x.divide(BigInteger.valueOf(per));
										if(inc.compareTo(BigInteger.ZERO) <= 0)
											inc = BigInteger.ONE;
									}
									svis[i] = new CharArrayRangeSourceValueIterator(colname, min, max, inc, !col.required);
								}

								per = per / s;
								filterColumns.add('-' + colname);
							}
						}
					} catch(ConvertException e) {
						throw new ServiceException("Could not convert stats for " + tab, e);
					}
					} finally {
						statResults.close();
					}
					prep = new ValuePreparer() {
						protected int index = -1;
						@Override
						public boolean prepareNext(Map<String, Object> input) {
							if(moveNext()) {
								for(int k = 0; k < svis.length; k++)
									svis[k].set(input);
								return true;
							}
							return false;
						}

						protected boolean moveNext() {
							if(svis.length == 0)
								return false;
							if(index < 0) {
								index = 0;
								for(int k = 0; k < svis.length; k++)
									if(!svis[k].next()) {
										index = svis.length;
										return false;
									}
								return true;
							}
							if(index < svis.length) {
								if(svis[index].next())
									return true;

								while(++index < svis.length) {
									if(svis[index].next()) {
										for(int k = 0; k < index; k++) {
											svis[k].reset();
											svis[k].next();
										}
										index = 0;
										return true;
									}
								}
								return false;
							}

							return false;
						}
					};
					break;
				case EXECUTABLE:
					Call allExistingCall = tab.getAllExistingCall(connGroup);
					final Results existingResults = allExistingCall.executeQuery(connGroup.getConnection(allExistingCall.getDataSourceName(), true), null, null);
					prep = new ValuePreparer() {
						@Override
						public boolean prepareNext(Map<String, Object> input) {
							if(existingResults.next()) {
								try {
									existingResults.fillBean(input);
								} catch(BeanException e) {
									throw new UndeclaredThrowableException(e);
								} finally {
									existingResults.close();
								}
								return true;
							}
							existingResults.close();
							return false;
						}
					};
					filterColumns = tab.getUniqueKeyNames();
					break;
				default:
					throw new RetrySpecifiedServiceException("Cannot process repopulate columns for tab with " + tab.getSourceType() + " source type", WorkRetryType.NO_RETRY);
			}

			// Verify that source has all tab.getUniqueKeyNames()
			for(String key : tab.getUniqueKeyNames()) {
				if(!sourceInfo.colnames.containsKey(key))
					throw new SQLException("Neither filterValues nor updateKeys contains unique key column '" + key + "'");
			}

			Call updateCall = tab.getPartialUpdateCall(columns, false, connGroup);
			BatchUpdate updateBatch = updateCall.createBatchUpdate();
			Map<Col, Map<String, Object>> refInputs = new HashMap<Col, Map<String, Object>>();
			for(String colname : columns) {
				Col col = tab.getCol(colname);
				if(col != null && col.getRefTab() != null && !sourceInfo.colnames.containsKey(colname)) {
					Map<String, Object> refInput = new HashMap<String, Object>();
					refInputs.put(col, refInput);
					for(Col refCol : col.getRefTab().getIdentityCols()) {
						if(sourceInfo.getPnt(colname + '$' + refCol.name) != null)
							refInput.put(refCol.name, null);
					}
				}
			}

			// loop to update
			Map<String, Object> input = new HashMap<String, Object>();
			while(prep.prepareNext(input)) {
				// construct select from source and update calls
				Call selectCall = tab.getSourceCall(source, input, connGroup);
				if(selectCall == null) {
					log.warn("Select columns were not different from unique key columns; doing nothing");
					return 0;
				}
				final Results results = selectCall.executeQuery(connGroup.getConnection(selectCall.getDataSourceName(), true), input, null, null);
				try {
					while(results.next()) {
						input.clear();
						input.put(getRevisionLabel(), revision);
						results.fillBean(input);
						for(Map.Entry<Col, Map<String, Object>> lookupEntry : refInputs.entrySet()) {
							for(Map.Entry<String, Object> refEntry : lookupEntry.getValue().entrySet()) {
								refEntry.setValue(input.get(lookupEntry.getKey().name + '$' + refEntry.getKey()));
							}
							Long refId = lookupRow(revision, (DataLoaderTab) lookupEntry.getKey().getRefTab(), null, lookupEntry.getValue(), connGroup, null);
							if(refId == null)
								throw new ServiceException("Could not lookup row in table '" + lookupEntry.getKey().name + "' with values " + lookupEntry.getValue());
							input.put(lookupEntry.getKey().name, refId);
						}

						updateBatch.addBatch(input);
					}
				} finally {
					results.close();
				}
				int[] ret = updateBatch.executeBatch(connGroup.getConnection(updateCall.getDataSourceName(), true));
				for(int r : ret)
					if(r > 0)
						cnt += r;
				input.clear();
			}
		} catch(SQLException e) {
			throw new ServiceException("Could not repopulateColumns in " + tab, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not repopulateColumns in " + tab, e);
		} catch(BeanException e) {
			throw new ServiceException("Could not repopulateColumns in " + tab, e);
		}

		return cnt;
	}

	protected static interface SourceValueIterator {
		public boolean next();

		public void reset();

		public void set(Map<String, Object> sourceValues);
	}

	protected static class DiscreteSourceValueIterator implements SourceValueIterator {
		protected final String colname;
		protected final Object[] values;
		protected int index;

		public DiscreteSourceValueIterator(String colname, Object[] values) {
			this.colname = colname;
			this.values = values;
			this.index = -1;
		}

		@Override
		public boolean next() {
			return ++index < values.length;
		}

		@Override
		public void reset() {
			index = -1;
		}

		@Override
		public void set(Map<String, Object> sourceValues) {
			sourceValues.put(colname, values[index]);
		}
	}

	protected static abstract class AbstractRangeSourceValueIterator<V, O> implements SourceValueIterator {
		protected final String colname;
		protected O startValue;
		protected O endValue;
		protected final V minValue;
		protected final V maxValue;
		protected final boolean includeNull;

		public AbstractRangeSourceValueIterator(String colname, V minValue, V maxValue, boolean includeNull) {
			this.colname = colname;
			this.minValue = minValue;
			this.maxValue = maxValue;
			this.includeNull = includeNull;
		}

		@Override
		public void reset() {
			startValue = null;
			endValue = null;
		}

		@Override
		public void set(Map<String, Object> sourceValues) {
			if(includeNull && (startValue == null || endValue == null)) {
				sourceValues.put('*' + colname, 1);
				sourceValues.remove('>' + colname);
				sourceValues.remove('<' + colname);
			} else {
				sourceValues.put('>' + colname, startValue);
				sourceValues.put('<' + colname, endValue);
				sourceValues.remove('*' + colname);
			}
		}
	}

	protected static class DoubleRangeSourceValueIterator extends AbstractRangeSourceValueIterator<Double, Double> {
		protected final double increment;

		public DoubleRangeSourceValueIterator(String colname, Double minValue, Double maxValue, double increment, boolean includeNull) {
			super(colname, minValue, maxValue, includeNull);
			this.increment = increment;
		}

		@Override
		public boolean next() {
			if(includeNull && startValue == null && endValue == null) {
				endValue = minValue;
				return true;
			}
			if(startValue == null) {
				startValue = minValue;
				endValue = Math.min(startValue + increment, maxValue);
				return true;
			}
			if(endValue >= maxValue)
				return false;
			startValue = Math.nextUp(endValue);
			endValue = Math.min(startValue + increment, maxValue);
			return true;
		}
	}

	protected static class LongRangeSourceValueIterator extends AbstractRangeSourceValueIterator<Long, Long> {
		protected final long increment;

		public LongRangeSourceValueIterator(String colname, Long minValue, Long maxValue, long increment, boolean includeNull) {
			super(colname, minValue, maxValue, includeNull);
			this.increment = increment;
		}

		@Override
		public boolean next() {
			if(includeNull && startValue == null && endValue == null) {
				endValue = minValue;
				return true;
			}
			if(startValue == null) {
				startValue = minValue;
				endValue = Math.min(startValue + increment, maxValue);
				return true;
			}
			if(endValue >= maxValue)
				return false;
			startValue = endValue + 1;
			endValue = Math.min(startValue + increment, maxValue);
			return true;
		}
	}

	protected static class BigDecimalRangeSourceValueIterator extends AbstractRangeSourceValueIterator<BigDecimal, BigDecimal> {
		protected final BigDecimal increment;

		public BigDecimalRangeSourceValueIterator(String colname, BigDecimal minValue, BigDecimal maxValue, BigDecimal increment, boolean includeNull) {
			super(colname, minValue, maxValue, includeNull);
			this.increment = increment;
		}

		@Override
		public boolean next() {
			if(includeNull && startValue == null && endValue == null) {
				endValue = minValue;
				return true;
			}
			if(startValue == null) {
				startValue = minValue;
				endValue = SystemUtils.least(startValue.add(increment), maxValue);
				return true;
			}
			if(endValue.compareTo(maxValue) >= 0)
				return false;
			startValue = endValue; // Since we can't accurately determine the 'next' value after endValue we use it
			endValue = SystemUtils.least(startValue.add(increment), maxValue);
			return true;
		}
	}

	protected static class BigIntegerRangeSourceValueIterator extends AbstractRangeSourceValueIterator<BigInteger, BigInteger> {
		protected final BigInteger increment;

		public BigIntegerRangeSourceValueIterator(String colname, BigInteger minValue, BigInteger maxValue, BigInteger increment, boolean includeNull) {
			super(colname, minValue, maxValue, includeNull);
			this.increment = increment;
		}

		@Override
		public boolean next() {
			if(includeNull && startValue == null && endValue == null) {
				endValue = minValue;
				return true;
			}
			if(startValue == null) {
				startValue = minValue;
				endValue = SystemUtils.least(startValue.add(increment), maxValue);
				return true;
			}
			if(endValue.compareTo(maxValue) >= 0)
				return false;
			startValue = endValue.add(BigInteger.ONE);
			endValue = SystemUtils.least(startValue.add(increment), maxValue);
			return true;
		}
	}

	protected static class CharArrayRangeSourceValueIterator extends AbstractRangeSourceValueIterator<char[], String> {
		protected final char[] workingValue;
		protected final BigInteger increment;
		protected boolean more = false;

		public CharArrayRangeSourceValueIterator(String colname, char[] minValue, char[] maxValue, BigInteger increment, boolean includeNull) {
			super(colname, minValue, maxValue, includeNull);
			this.increment = increment;
			this.workingValue = new char[Math.max(minValue.length, maxValue.length)];
		}

		public boolean next() {
			if(includeNull && startValue == null && endValue == null) {
				endValue = new String(minValue);
				return true;
			}
			if(startValue == null) {
				Arrays.fill(workingValue, (char) 0);
				System.arraycopy(minValue, 0, workingValue, 0, minValue.length);
				startValue = new String(minValue);
				more = StringUtils.increment(workingValue, increment, SORTED_ASCII_CHARS);
				endValue = new String(workingValue);
				return true;
			}
			if(!more)
				return false;
			more = StringUtils.increment(workingValue, BigInteger.ONE, SORTED_ASCII_CHARS);
			startValue = new String(minValue);
			if(!more) {
				endValue = startValue;
			} else {
				more = StringUtils.increment(workingValue, increment, SORTED_ASCII_CHARS);
				endValue = new String(workingValue);
			}
			return true;
		}
	}
	
	/**
	 * Loads the specified row from source to target. This will ensure that the specified target table is updated, but does not guarantee that any referenced tables are
	 * updated (it does guarantee that referenced rows exist).
	 * 
	 * @param table
	 * @param source
	 *            The source of the data
	 * @param sourceValues
	 * @param connGroup
	 * @return
	 * @throws ServiceException
	 */
	public Collection<Long> loadRow(String table, String source, Map<String, Object> sourceValues, ConnectionGroup connGroup) throws ServiceException {
		ConfigSource cfg = getMetadataFile();
		if(cfg != null)
			try {
				cfg.reload();
			} catch(LoadingException e) {
				log.error("Could not reload metadata file '" + cfg + "'; continuing with old definitions", e);
			}
		DataLoaderTab tab = db.getTab(table);
		if(tab == null)
			throw new ServiceException("Table '" + table + "' is not defined");
		final long revision;
		if(tab.getRevisionPnt() == null || revisionGeneratorCall == null)
			revision = 0L;
		else {
			try {
				Connection conn = connGroup.getConnection(revisionGeneratorCall.getDataSourceName(), null);
				Object[] ret = null;
				try {
					ret = revisionGeneratorCall.executeCall(conn, null, null);
				} finally {
					if(!conn.getAutoCommit()) {
						if(ret != null)
							conn.commit();
						else
							conn.rollback();
					}
				}
				if(ret.length < 2)
					throw new ServiceException("Could not get revision from call '" + getRevisionGeneratorCallId() + "' - it must have one out parameter");
				revision = ConvertUtils.getLong(ret[1]);
			} catch(SQLException e) {
				throw new ServiceException("Could not generate revision", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not generate revision", e);
			} catch(ConvertException e) {
				throw new ServiceException("Could not generate revision", e);
			}
		}
		List<Long> ids = new ArrayList<Long>();
		loadRow(revision, tab, source, sourceValues, connGroup, ids);
		return ids;
	}

	protected Long lookupRow(long revision, DataLoaderTab tab, String source, Map<String, Object> sourceValues, ConnectionGroup connGroup, List<Long> ids) throws ServiceException {
		// If values are for the default (unknown) row then return 0
		if(tab.isDefaultRow(sourceValues, this))
			return 0L;
		// Cache this so it can be much faster than hitting db each time
		return tab.lookupRow(revision, source, sourceValues, connGroup, ids, this);
	}

	protected Long loadRow(long revision, DataLoaderTab tab, String source, Map<String, Object> sourceValues, ConnectionGroup connGroup, List<Long> ids) throws ServiceException {
		Call sourceCall;
		try {
			sourceCall = tab.getSourceCall(source, sourceValues, connGroup);
		} catch(SQLException e) {
			throw new ServiceException("While creating source call for table '" + tab.name + "' with params " + sourceValues, e);
		}
		Call[] loadCalls;
		try {
			if(tab.getStandardUpsertCall(connGroup) != null) {
				if(tab.getCustomUpsertCall() != null)
					loadCalls = new Call[] { tab.getStandardUpsertCall(connGroup), tab.getCustomUpsertCall() };
				else
					loadCalls = new Call[] { tab.getStandardUpsertCall(connGroup) };
			} else if(tab.getCustomUpsertCall() != null)
				loadCalls = new Call[] { tab.getCustomUpsertCall() };
			else
				throw new ServiceException("Neither a standard upsert call nor a custom upsert call is set for table '" + tab.name + "'");
		} catch(SQLException e) {
			throw new ServiceException("While creating upsert call for table '" + tab.name + "' with params " + sourceValues, e);
		}
		return loadRow(revision, tab, sourceCall, sourceValues, connGroup, ids, loadCalls);
	}

	public String getRevisionLabel() {
		return revisionLabel;
	}

	public void setRevisionLabel(String revisionLabel) {
		this.revisionLabel = revisionLabel;
	}
	public String getRowCountLabel() {
		return rowCountLabel;
	}

	public void setRowCountLabel(String rowCountLabel) {
		this.rowCountLabel = rowCountLabel;
	}

	public SQLType getRowCountSqlType() {
		return rowCountSqlType;
	}

	public void setRowCountSqlType(SQLType rowCountSqlType) {
		this.rowCountSqlType = rowCountSqlType;
	}

	public long getLogExecuteTimeThreshhold() {
		return logExecuteTimeThreshhold;
	}

	public void setLogExecuteTimeThreshhold(long logExecuteTimeThreshhold) {
		this.logExecuteTimeThreshhold = logExecuteTimeThreshhold;
	}

	public int getQueryTimeout() {
		return queryTimeout;
	}

	public void setQueryTimeout(int queryTimeout) {
		this.queryTimeout = queryTimeout;
	}

	public ConfigSource getMetadataFile() {
		return metadataFile;
	}

	public void setMetadataFile(ConfigSource metadataFile) {
		this.metadataFile = metadataFile;
	}

	public Namer getTargetNamer(ConnectionGroup connGroup) throws SQLException {
		Namer namer = this.targetNamer;
		if(namer == null)
			try {
				this.targetNamer = namer = new Namer(connGroup.getConnection(getTargetDsn(), true).getMetaData());
			} catch(DataLayerException e) {
				throw new SQLException(e);
			}
		return namer;
	}
	public String getTargetDsn() {
		return targetDsn;
	}

	public void setTargetDsn(String targetDsn) {
		this.targetDsn = targetDsn;
		this.targetNamer = null;
	}

	public String getRevisionGeneratorCallId() {
		return revisionGeneratorCallId;
	}

	public void setRevisionGeneratorCallId(String revisionGeneratorCallId) {
		this.revisionGeneratorCallId = revisionGeneratorCallId;
	}

	public int getLookupCacheSize() {
		return lookupCacheSize;
	}

	public void setLookupCacheSize(int lookupCacheSize) {
		int old = this.lookupCacheSize;
		this.lookupCacheSize = lookupCacheSize;
		if(old != lookupCacheSize)
			for(DataLoaderTab tab : db.getTabs())
				tab.lookupCache.setMaxSize(lookupCacheSize);
	}

	public int getLookupCacheConcurrency() {
		return lookupCacheConcurrency;
	}

	public void setLookupCacheConcurrency(int lookupCacheConcurrency) {
		this.lookupCacheConcurrency = lookupCacheConcurrency;
	}

	public boolean isDefaultOnUpsert() {
		return defaultOnUpsert;
	}

	public void setDefaultOnUpsert(boolean usingDefaultOnUpsert) {
		this.defaultOnUpsert = usingDefaultOnUpsert;
	}

	public boolean isStrictSourceColumnOrder() {
		return strictSourceColumnOrder;
	}

	public void setStrictSourceColumnOrder(boolean strictSourceColumnOrder) {
		this.strictSourceColumnOrder = strictSourceColumnOrder;
	}

	public boolean isLazyAccessOnRangeSelect() {
		return lazyAccessOnRangeSelect;
	}

	public void setLazyAccessOnRangeSelect(boolean lazyAccessOnRangeSelect) {
		this.lazyAccessOnRangeSelect = lazyAccessOnRangeSelect;
	}

	protected static final MetadataChangeTracker NADA_METADATA_CHANGE_TRACKER = new MetadataChangeTracker() {
		@Override
		public void tabAdded(Tab newTab) {
		}

		@Override
		public void tabRemoved(Tab oldTab) {
		}

		@Override
		public void tabModified(Tab oldTab, Tab newTab) {
		}

		@Override
		public void colAdded(Col newCol) {
		}

		@Override
		public void colRemoved(Col oldCol) {
		}

		@Override
		public void colModified(Col oldCol, Col newCol) {
		}

		@Override
		public void commit() throws ServiceException {
		}
	};

	// TODO: implement this
	protected class PopulateDataTracker implements MetadataChangeTracker {
		@Override
		public void tabAdded(Tab newTab) {
		}

		@Override
		public void tabRemoved(Tab oldTab) {
		}

		@Override
		public void tabModified(Tab oldTab, Tab newTab) {
		}

		@Override
		public void colAdded(Col newCol) {
		}

		@Override
		public void colRemoved(Col oldCol) {
		}

		@Override
		public void colModified(Col oldCol, Col newCol) {
		}

		@Override
		public void commit() throws ServiceException {
		}
	};

	public void updateMetadata(InputStream xml, boolean saveToFile, boolean updateData) throws ServiceException {
		OutputStream out;
		if(saveToFile) {
			try {
				out = metadataFile.getOutputStream();
			} catch(IOException e) {
				throw new ServiceException("Could not get output stream for metadata resource", e);
			}
			xml = new CopyInputStream(xml, out);
		} else
			out = null;
		MetadataChangeTracker tracker = (updateData ? new PopulateDataTracker() : NADA_METADATA_CHANGE_TRACKER);
		boolean okay = false;
		try {
			metadataLoader.lock();
			try {
				db.load(xml, getTargetDsn(), tracker);
				okay = true;
			} catch(LoadingException e) {
				throw new ServiceException(e);
			} finally {
				metadataLoader.unlock();
			}
		} finally {
			if(!okay && out instanceof Abortable)
				try {
					((Abortable) out).abort();
				} catch(IOException e) {
					log.warn("Could not clean up after writing part of metadata resource", e);
				}
			else if(out != null)
				try {
					out.close();
				} catch(IOException e) {
					throw new ServiceException("Could not fully write metadata resource", e);
				}
		}
		tracker.commit();
	}

	/*
	protected static final Pattern PARSE_COMMAND_PATTERN = Pattern.compile("(ADD|REMOVE|MODIFY)_(TAB|COL)", Pattern.CASE_INSENSITIVE);

	protected static enum CommandAction {
		ADD, REMOVE, MODIFY, REPLACE
	};

	protected static enum CommandTarget {
		TAB, COL
	};

	public void updateMetadata(Map<String, Object> sourceValues, ConnectionGroup connGroup) throws ServiceException {
		updateMetadata(sourceValues, connGroup, NADA_METADATA_CHANGE_TRACKER);
	}

	protected void updateMetadata(Map<String, Object> sourceValues, ConnectionGroup connGroup, MetadataChangeTracker tracker) throws ServiceException {
		boolean changed = false;
		metadataLoader.lock();
		try {
			Map<String, DataLoaderTab> updateTabs = new HashMap<String, DataLoaderTab>();
			Map<Col, String> refs = new HashMap<Col, String>();			
			Map<Tab, String> parentRefs = new HashMap<Tab, String>();
			for(Map.Entry<String, Object> entry : sourceValues.entrySet()) {
				Matcher matcher = PARSE_COMMAND_PATTERN.matcher(entry.getKey());
				if(!matcher.matches())
					throw new ServiceException("Command '" + entry.getKey() + "' is not recognized; use: ADD_TAB, ADD_COL, REMOVE_TAB, REMOVE_COL, REPLACE_TAB, REPLACE_COL, MODIFY_TAB or MODIFY_COL");
				CommandAction ca;
				CommandTarget ct;
				try {
					ca = ConvertUtils.convertRequired(CommandAction.class, matcher.group(1));
					ct = ConvertUtils.convertRequired(CommandTarget.class, matcher.group(2));
				} catch(ConvertException e) {
					throw new ServiceException("Invalid command '" + entry.getKey() + "'; use: ADD_TAB, ADD_COL, REMOVE_TAB, REMOVE_COL, REPLACE_TAB, REPLACE_COL, MODIFY_TAB or MODIFY_COL", e);
				}
				Reader xml;
				try {
					xml = ConvertUtils.convert(Reader.class, entry.getValue());
				} catch(ConvertException e) {
					throw new ServiceException("Could not convert command data to a Reader", e);
				}
				ObjectBuilder builder;
				try {
					builder = new ObjectBuilder(xml);
				} catch(IOException e) {
					throw new ServiceException("Could not parse command data", e);
				} catch(SAXException e) {
					throw new ServiceException("Could not parse command data", e);
				} catch(ParserConfigurationException e) {
					throw new ServiceException("Could not parse command data", e);
				}
				try {
					if("tab".equals(builder.getTag()))
						updateTab(builder, ca, ct, updateTabs, refs, parentRefs);
					else {
						if(ca == CommandAction.REPLACE && ct == CommandTarget.TAB && "db".equals(builder.getTag())) {
							updateTabs.putAll(CollectionUtils.singleValueMap(db.getTabNames(), (DataLoaderTab) null));
						}
						for(ObjectBuilder tabBuilder : builder.getSubNodes("tab")) {
							updateTab(tabBuilder, ca, ct, updateTabs, refs, parentRefs);
						}
					}
				} catch(LoadingException e) {
					throw new ServiceException("Could not update tab", e);
				}
			}
			for(Map.Entry<Col, String> refsentry : refs.entrySet()) {
				Tab refTab = updateTabs.get(refsentry.getValue());
				if(refTab == null)
					refTab = db.getTab(refsentry.getValue());
				refsentry.getKey().setRefTab(refTab);
			}
			for(Map.Entry<Tab, String> refsentry : parentRefs.entrySet()) {
				Tab refTab = updateTabs.get(refsentry.getValue());
				if(refTab == null)
					refTab = db.getTab(refsentry.getValue());
				refsentry.getKey().setParent(refTab);
			}

			for(Map.Entry<String, DataLoaderTab> updateEntry : updateTabs.entrySet()) {
				if(updateEntry.getValue() == null) {
					changed |= db.removeTab(updateEntry.getKey(), tracker);
				} else {
					changed |= db.replaceTab(updateEntry.getValue(), tracker);
				}
			}
			tracker.commit();
			if(changed) {
				try {
					OutputStream out = metadataFile.getOutputStream();
					try {
						XMLBuilder xmlBuilder = new XMLBuilder(out);
						xmlBuilder.setIndent(true);
						xmlBuilder.comment("Auto-Saved because of metadata change at " + new Date());
						xmlBuilder.elementStart("db");
						for(Tab tab : db.getTabs())
							tab.writeXml(xmlBuilder);
						xmlBuilder.elementEnd("db");
					} catch(InstantiationException e) {
						log.error("Could not save metadata changes; keeping the change but not updating the file", e);
					} catch(IllegalAccessException e) {
						log.error("Could not save metadata changes; keeping the change but not updating the file", e);
					} catch(ClassNotFoundException e) {
						log.error("Could not save metadata changes; keeping the change but not updating the file", e);
					} catch(IntrospectionException e) {
						log.error("Could not save metadata changes; keeping the change but not updating the file", e);
					} catch(InvocationTargetException e) {
						log.error("Could not save metadata changes; keeping the change but not updating the file", e);
					} catch(ConvertException e) {
						log.error("Could not save metadata changes; keeping the change but not updating the file", e);
					} catch(ParseException e) {
						log.error("Could not save metadata changes; keeping the change but not updating the file", e);
					} catch(SAXException e) {
						log.error("Could not save metadata changes; keeping the change but not updating the file", e);
					} finally {
						out.close();
					}
				} catch(IOException e) {
					log.error("Could not save metadata changes; keeping the change but not updating the file", e);
				}
			}
		} finally {
			metadataLoader.unlock();
		}
	}
	
	protected void updateTab(ObjectBuilder tabBuilder, CommandAction ca, CommandTarget ct, Map<String, DataLoaderTab> updateTabs, Map<Col, String> refs, Map<Tab, String> parentRefs) throws LoadingException {
		String tabName = tabBuilder.getAttrValue("name");
		Tab old;
		Tab tab;
		switch(ca) {
			case REMOVE:
				switch(ct) {
					case TAB:
						old = db.getTab(tabName);
						if(!updateTabs.containsKey(tabName)) {
							if(old != null) {
								updateTabs.put(tabName, null);
								log.info("Removing tab '" + tabName + "'");
							} else
								log.info("Tab '" + tabName + "' is already removed");
						} else {
							if(old != null) {
								Tab prev = updateTabs.put(tabName, null);
								if(prev != null) {
									prev.destroy();
									log.info("Removing tab '" + tabName + "' that would have been updated by this change");
								} else
									log.info("Tab '" + tabName + "' was already removed by this change");
							} else {
								Tab prev = updateTabs.remove(tabName);
								if(prev != null)
									prev.destroy();
								log.info("Removing tab '" + tabName + "' that would have been added by this change");
							}
						}
						break;
					case COL:
						if(!updateTabs.containsKey(tabName)) {
							old = db.getTab(tabName);
							if(old == null) {
								log.info("Tab '" + tabName + "' has been removed; can't remove cols on it");
								break;
							}
						} else {
							old = updateTabs.get(tabName);
							if(old == null) {
								log.info("Tab '" + tabName + "' is being removed by this change; can't remove cols on it");
								break;
							}
						}
						Set<String> remove = new HashSet<String>();
						for(ObjectBuilder colBuilder : tabBuilder.getSubNodes("col"))
							remove.add(colBuilder.getAttrValue("name"));
						tab = old.copy();
						Set<String> removed = new HashSet<String>();
						for(Col col : old.getCols(true))
							if(!remove.contains(col.name)) {
								col.copyTo(tab);
							} else {
								removed.add(col.name);
							}
						if(removed.isEmpty()) {
							log.info("None of the cols supplied " + remove + " was found in the table '" + tabName + "'");
							tab.destroy();
						} else {
							copyIndexes(old, tab);
							log.info("Removing cols '" + removed + "' from '" + tabName + "'");
							Tab prev = updateTabs.put(tabName, tab);
							if(prev != null)
								prev.destroy();
						}
						break;
				}
				break;
			case ADD:
				switch(ct) {
					case TAB:
						if(!updateTabs.containsKey(tabName)) {
							if(db.getTab(tabName) != null) {
								log.info("Tab '" + tabName + "' has already been added");
								break;
							}
							log.info("Adding tab '" + tabName + "'");
							old = null;
						} else {
							if(updateTabs.get(tabName) == null) {
								log.info("Updating tab '" + tabName + "' because remove and then add was specified by this change");
								old = db.getTab(tabName);
							} else {
								log.info("Overwriting tab '" + tabName + "' that was already added by this change");
								old = null;
							}
						}
						tab = readTab(tabBuilder, refs, parentRefs);
						Tab prev = updateTabs.put(tabName, tab);
						if(prev != null)
							prev.destroy();
						break;
					case COL:
						if(!updateTabs.containsKey(tabName)) {
							old = db.getTab(tabName);
							if(old == null) {
								log.info("Tab '" + tabName + "' has been removed; can't add cols on it");
								break;
							}
						} else {
							old = updateTabs.get(tabName);
							if(old == null) {
								log.info("Tab '" + tabName + "' is being removed by this change; can't add cols on it");
								break;
							}
						}
						tab = old.copy();
						Set<String> added = new HashSet<String>();
						for(Col col : old.getCols())
							col.copyTo(tab);
						for(ObjectBuilder colBuilder : tabBuilder.getSubNodes("col")) {
							Col col = addCol(tab, colBuilder, refs, true);
							if(col != null) {
								added.add(col.name);
							}
						}
						if(added.isEmpty()) {
							log.info("All of the cols supplied already exist in the tab '" + tabName + "'");
							tab.destroy();
						} else {
							copyIndexes(old, tab);
							log.info("Adding cols '" + added + "' to '" + tabName + "'");
							prev = updateTabs.put(tabName, tab);
							if(prev != null)
								prev.destroy();
						}
						break;
				}
				break;
			case MODIFY:
				switch(ct) {
					case TAB:
						old = db.getTab(tabName);
						Tab prev;
						if(!updateTabs.containsKey(tabName)) {
							if(old != null) {
								log.info("Modifying tab '" + tabName + "'");
								prev = old;
							} else {
								log.info("Tab '" + tabName + "' does not exist; cannot modify");
								break;
							}
						} else {
							prev = updateTabs.get(tabName);
							if(prev != null) {
								log.info("Modifying tab '" + tabName + "' that is already updated by this change");
								prev.destroy();
							} else {
								log.info("Tab '" + tabName + "' was already removed by this change");
								break;
							}
						}
						tab = readTab(tabBuilder, refs, parentRefs, prev.getCols());
						prev = updateTabs.put(tabName, tab);
						if(prev != null)
							prev.destroy();
						break;
					case COL:
						if(!updateTabs.containsKey(tabName)) {
							old = db.getTab(tabName);
							if(old == null) {
								log.info("Tab '" + tabName + "' has been removed; can't modify cols on it");
								break;
							}
						} else {
							old = updateTabs.get(tabName);
							if(old == null) {
								log.info("Tab '" + tabName + "' is being removed by this change; can't modify cols on it");
								break;
							}
						}
						tab = old.copy();
						Map<String, ObjectBuilder> modifying = new HashMap<String, ObjectBuilder>();
						for(ObjectBuilder colBuilder : tabBuilder.getSubNodes("col"))
							modifying.put(colBuilder.getAttrValue("name"), colBuilder);
						int n = modifying.size();
						for(Col col : old.getCols()) {
							ObjectBuilder colBuilder = modifying.remove(col.name);
							if(colBuilder == null)
								col.copyTo(tab);
							else {
								Col newCol = addCol(tab, colBuilder, refs, false);
							}
						}
						if(n == modifying.size()) {
							log.info("None of the cols supplied " + modifying.keySet() + " match any cols already in the tab '" + tabName + "'");
							tab.destroy();
						} else {
							if(!modifying.isEmpty())
								log.info("Could not find cols " + modifying.keySet() + " in tab '" + tabName + "'");
							copyIndexes(old, tab);
							log.info("Modifying " + (n - modifying.size()) + " cols in tab '" + tabName + "'");
							prev = updateTabs.put(tabName, tab);
							if(prev != null)
								prev.destroy();
						}
						break;
				}
				break;
			case REPLACE:
				switch(ct) {
					case TAB:
						old = db.getTab(tabName);
						tab = readTab(tabBuilder, refs, parentRefs);
						if(old != null && old.equals(tab)) {
							log.info("Skipping replace of  tab '" + tabName + "' becuse new definition matches existing one");
							break;
						}

						Tab prev = updateTabs.put(tabName, tab);
						if(prev != null)
							prev.destroy();
						if(old == null) {
							log.info("Adding tab '" + tabName + "'");
						} else {
							log.info("Overwriting tab '" + tabName + "'");
						}
						break;
					case COL:
						if(!updateTabs.containsKey(tabName)) {
							old = db.getTab(tabName);
							if(old == null) {
								log.info("Tab '" + tabName + "' has been removed; can't replace cols on it");
								break;
							}
						} else {
							old = updateTabs.get(tabName);
							if(old == null) {
								log.info("Tab '" + tabName + "' is being removed by this change; can't replace cols on it");
								break;
							}
						}
						ObjectBuilder[] colBuilders = tabBuilder.getSubNodes("col");
						if(colBuilders == null || colBuilders.length == 0) {
							log.info("No cols were supplied for tab '" + tabName + "'");
							break;
						}
						tab = old.copy();
						Map<String, ObjectBuilder> newcols = new LinkedHashMap<String, ObjectBuilder>();
						for(ObjectBuilder colBuilder : tabBuilder.getSubNodes("col"))
							newcols.put(colBuilder.getAttrValue("name"), colBuilder);

						Set<String> replaced = new LinkedHashSet<String>();
						for(Col col : old.getCols()) {
							ObjectBuilder colBuilder = newcols.remove(col.name);
							if(colBuilder != null) {
								Col newCol = addCol(tab, colBuilder, refs, false);
								replaced.add(col.name);
							} else
								col.copyTo(tab);
						}

						for(ObjectBuilder colBuilder : newcols.values()) {
							Col col = addCol(tab, colBuilder, refs, false);
						}
						copyIndexes(old, tab);
						log.info("Adding cols '" + newcols.keySet() + "' to and modifying cols '" + replaced + "' on '" + tabName + "'");
						prev = updateTabs.put(tabName, tab);
						if(prev != null)
							prev.destroy();
						break;
				}
				break;
		}
	}
	protected void copyIndexes(Tab old, Tab tab) {
		for(Ind oldInd : old.getIndexes()) {
			Ind ind = oldInd.copy(tab);
			if(!ind.getCols().isEmpty())
				tab.getIndexes().add(ind);
		}
	}
	*/
	public int getDefaultValueQueryTimeout() {
		return defaultValueQueryTimeout;
	}

	public void setDefaultValueQueryTimeout(int defaultValueQueryTimeout) {
		this.defaultValueQueryTimeout = defaultValueQueryTimeout;
	}

	public int getMinDistinctForRange() {
		return minDistinctForRange;
	}

	public void setMinDistinctForRange(int minDistinctForRange) {
		this.minDistinctForRange = minDistinctForRange;
	}

	public int getIdealBatchSize() {
		return idealBatchSize;
	}

	public void setIdealBatchSize(int idealBatchSize) {
		this.idealBatchSize = idealBatchSize;
	}

	public Collection<Long> partialUpdate(String table, String source, Map<String, Object> sourceValues, Set<String> targetKeys, ConnectionGroup connGroup) throws ServiceException {
		DataLoaderTab tab = db.getTab(table);
		if(tab == null)
			throw new ServiceException("Table '" + table + "' is not defined");
		final long revision;
		if(tab.getRevisionPnt() == null || revisionGeneratorCall == null)
			revision = 0L;
		else {
			try {
				Connection conn = connGroup.getConnection(revisionGeneratorCall.getDataSourceName(), null);
				Object[] ret = null;
				try {
					ret = revisionGeneratorCall.executeCall(conn, null, null);
				} finally {
					if(!conn.getAutoCommit()) {
						if(ret != null)
							conn.commit();
						else
							conn.rollback();
					}
				}
				if(ret.length < 2)
					throw new ServiceException("Could not get revision from call '" + getRevisionGeneratorCallId() + "' - it must have one out parameter");
				revision = ConvertUtils.getLong(ret[1]);
			} catch(SQLException e) {
				throw new ServiceException("Could not generate revision", e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not generate revision", e);
			} catch(ConvertException e) {
				throw new ServiceException("Could not generate revision", e);
			}
		}
		List<Long> ids = new ArrayList<Long>();
		partialUpdate(revision, tab, source, sourceValues, targetKeys, connGroup, ids);
		return ids;
	}

	protected boolean addColNameOnMatch(ColType colType, String longName, SourceInfo sourceInfo, Map<String, Object> sourceValues, Map<String, Object> targetValues, Set<String> targetKeys, Set<String> updateKeys) {
		String shortName;
		switch(colType) {
			case IDENTITY_KEY:
				if(sourceValues.containsKey(longName))
					targetValues.put(longName, sourceValues.get(longName));
				else if(sourceValues.containsKey((shortName = sourceInfo.namer.getName(longName, NameType.COLUMN))))
					targetValues.put(longName, sourceValues.get(shortName));
				else if(sourceInfo.colnames.containsKey(longName))
					targetValues.put(longName, DataLoaderTab.PLACEHOLDER);
				else if(sourceInfo.colnames.containsKey(shortName))
					targetValues.put(longName, DataLoaderTab.PLACEHOLDER);
				else
					return false;
				targetKeys.add(longName);
				return true;
			case DATA:
			case CUSTOM:
				if(targetKeys.contains(longName))
					targetValues.put(longName, sourceValues.containsKey(longName) ? sourceValues.get(longName) : DataLoaderTab.PLACEHOLDER);
				else if(targetKeys.contains((shortName = sourceInfo.namer.getName(longName, NameType.COLUMN))))
					targetValues.put(longName, sourceValues.containsKey(shortName) ? sourceValues.get(shortName) : DataLoaderTab.PLACEHOLDER);
				else if(sourceInfo.colnames.containsKey(longName))
					updateKeys.add(longName);
				else if(sourceInfo.colnames.containsKey(shortName))
					updateKeys.add(longName);
				else
					return false;
				return true;
		}
		return false;
	}
	protected Long partialUpdate(long revision, DataLoaderTab tab, String source, Map<String, Object> sourceValues, Set<String> targetKeys, ConnectionGroup connGroup, List<Long> ids) throws ServiceException {
		Call sourceCall;
		SourceInfo sourceInfo;
		try {
			sourceInfo = tab.getSourceInfo(source, connGroup);
			sourceCall = tab.getSourceCall(source, sourceValues, connGroup);
		} catch(SQLException e) {
			throw new ServiceException("While creating source call for table '" + tab.name + "' with params " + sourceValues, e);
		}
		if(targetKeys == null)
			targetKeys = new HashSet<String>(sourceValues.keySet());
		else if(targetKeys.isEmpty())
			targetKeys.addAll(sourceValues.keySet());

		// figure out targetValues and updateKeys
		Map<String, Object> targetValues = new HashMap<String, Object>(sourceValues.size());
		Set<String> updateKeys = new HashSet<String>(targetKeys.size());
		for(Col col : tab.getCols()) {
			if(col.getRefCol() != null)
				for(Col fkCol : col.getRefTab().getIdentityCols())
					addColNameOnMatch(col.colType, col.getName() + '$' + fkCol.getName(), sourceInfo, sourceValues, targetValues, targetKeys, updateKeys);
			else
				addColNameOnMatch(col.colType, col.getName(), sourceInfo, sourceValues, targetValues, targetKeys, updateKeys);
		}

		if(targetKeys.containsAll(tab.getUniqueKeyNames()))
			targetKeys.retainAll(tab.getUniqueKeyNames());

		Call updateCall;
		try {
			updateCall = tab.getPartialUpdateCall(targetValues, updateKeys, true, connGroup);
		} catch(SQLException e) {
			throw new ServiceException("While creating partial update call for table '" + tab.name + "' with params " + sourceValues, e);
		}
		return loadRow(revision, tab, sourceCall, sourceValues, connGroup, ids, updateCall);
	}

	protected static final Pattern REF_COL_PATTERN = Pattern.compile("([^$]+)\\$([^$]+)");

	protected void addRefInputs(Map<Pnt, Map<String, Object>> refInputs, DataLoaderTab tab, Set<String> keys) {
		for(String colname : keys) {
			Matcher matcher = REF_COL_PATTERN.matcher(colname);
			if(matcher.matches()) {
				String rawColName = matcher.group(1);
				Pnt pnt;
				switch(tab.getUpsertType()) {
					case BOTH:
					case CUSTOM:
						pnt = tab.getPnt(rawColName);
						if(pnt != null)
							break;
						else if(tab.getUpsertType() == UpsertType.CUSTOM)
							continue;
					case STANDARD:
					default:
						Col col = tab.getCol(rawColName);
						if(col == null)
							continue;
						switch(col.colType) {
							case PRIMARY_KEY:
							case IDENTITY_KEY:
							case DATA:
								pnt = col;
								break;
							default:
								continue;
						}
						break;
				}
				String refColName = matcher.group(2);
				// Use copy instead of FilterMap because this map may be cached and values in "sourceResults.getValuesMap()" change with results.next()
				Map<String, Object> refInput = refInputs.get(pnt);
				if(refInput == null) {
					refInput = new HashMap<String, Object>();
					refInputs.put(pnt, refInput);
				}
				refInput.put(refColName, null);
			}
		}
	}
	protected Long loadRow(long revision, DataLoaderTab tab, Call sourceCall, Map<String, Object> sourceValues, ConnectionGroup connGroup, List<Long> ids, Call... loadCalls) throws ServiceException {
		Results sourceResults;
		if(sourceCall == null) {
			Column[] columns = new Column[sourceValues.size()];
			int i = 0;
			for(String name : sourceValues.keySet()) {
				columns[i] = new Column();
				columns[i].setPropertyName(name);
				columns[i].setName(name);
				columns[i].setIndex(i + 1);
				i++;
			}
			sourceResults = new ListResults<Map<String, Object>>(columns, Collections.singletonList(sourceValues)) {
				@Override
				protected Object[] toValueArray(Map<String, Object> rowData) {
					return rowData.values().toArray();
				}

				@Override
				protected Map<String, Object> toValueMap(Map<String, Object> rowData) {
					return rowData;
				}
			};
		} else {
			try {
				sourceResults = sourceCall.executeQuery(connGroup.getConnection(sourceCall.getDataSourceName(), true), sourceValues, null, null);
			} catch(ParameterException e) {
				throw new ServiceException("While selecting from source for table '" + tab.name + "' with params " + sourceValues, e);
			} catch(SQLException e) {
				throw new ServiceException("While selecting from source for table '" + tab.name + "' with params " + sourceValues, e);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not obtain source connection  '" + sourceCall.getDataSourceName() + "'", e);
			}
		}
		try {
			Connection targetConn;
			try {
				targetConn = connGroup.getConnection(getTargetDsn(), loadCalls.length == 1);
			} catch(DataLayerException e) {
				throw new ServiceException("Could not obtain target connection  '" + getTargetDsn() + "'", e);
			}
			Map<String, Object> output = new HashMap<String, Object>();
			CompositeMap<String, Object> input = new CompositeMap<String, Object>();
			input.merge(null);
			input.merge(sourceValues);
			// gather any references
			Map<Pnt, Map<String, Object>> refInputs = new HashMap<Pnt, Map<String, Object>>();
			addRefInputs(refInputs, tab, sourceValues.keySet());
			addRefInputs(refInputs, tab, sourceResults.keySet());
			input.merge(output);

			while(sourceResults.next()) {
				output.clear();
				output.put(getRevisionLabel(), revision);
				input.mergeReplace(0, sourceResults.getValuesMap());
				for(Map.Entry<Pnt, Map<String, Object>> lookupEntry : refInputs.entrySet()) {
					for(Map.Entry<String, Object> refEntry : lookupEntry.getValue().entrySet()) {
						refEntry.setValue(input.get(lookupEntry.getKey().getName() + '$' + refEntry.getKey()));
					}
					Long refId = lookupRow(revision, (DataLoaderTab) lookupEntry.getKey().getRefCol().tab, null, lookupEntry.getValue(), connGroup, null);
					if(refId == null)
						throw new ServiceException("Could not lookup row in table '" + lookupEntry.getKey().getName() + "' with values " + lookupEntry.getValue());
					output.put(lookupEntry.getKey().getName(), refId);
				}
				boolean okay = false;
				try {
					for(Call loadCall : loadCalls) {
						Object[] ret = loadCall.executeCall(targetConn, input, output, null);
						if(ret != null && ret.length > 0 && ret[0] instanceof Results) {
							Results loadResults = (Results) ret[0];
							if(loadResults.next()) {
								loadResults.fillBean(output);
								if(!output.containsKey(getRowCountLabel()))
									output.put(getRowCountLabel(), 1);
							}
						}
					}
					okay = true;
				} catch(ParameterException e) {
					throw new ServiceException("While upserting into target for table '" + tab.name + "' with values " + sourceResults.getValuesMap(), e);
				} catch(SQLException e) {
					throw new ServiceException("While upserting into target for table '" + tab.name + "' with values " + sourceResults.getValuesMap(), e);
				} catch(BeanException e) {
					throw new ServiceException("While upserting into target for table '" + tab.name + "' with values " + sourceResults.getValuesMap(), e);
				} finally {
					if(!targetConn.getAutoCommit()) {// we must commit or else a lookup may return a row that ends up being rolled back
						if(okay)
							targetConn.commit();
						else
							try {
								targetConn.rollback();
							} catch(SQLException e) {
								log.warn("Could not rollback", e);
							}
					}
				}
				int rowCount = ConvertUtils.getIntSafely(output.get(getRowCountLabel()), 0);
				if(rowCount == 0) { // no update
					long newRevision = ConvertUtils.getLongSafely(output.get(getRevisionLabel()), 0);
					if(newRevision != revision)
						log.info("Skipping row with revision " + revision + " in table '" + tab.name + "' because it was updated while selecting from source with params " + sourceValues);
					else
						log.info("Row with revision " + revision + " in table '" + tab.name + "' was already updated with this revision for values " + sourceResults.getValuesMap());
					// if this is a lookup, we still need to get the id
					Long id;
					switch(tab.getPrimaryKeyCols().size()) {
						case 1:
							id = ConvertUtils.convertSafely(Long.class, output.get(tab.getFirstPrimaryKeyCol().name), null);
							if(id == null)
								throw new ServiceException("Could not find row in table '" + tab.name + "' with values " + sourceResults.getValuesMap());
							break;
						case 0:
							id = null;
							break;
						default:
							// not sure what to do here
							id = null;
					}
					if(id == null) {
						if(ids == null)
							return id;
					} else if(ids != null)
						ids.add(id);
					else if(sourceResults.next())
						throw new ServiceException("Lookup of table '" + tab.name + "' with values " + sourceResults.getValuesMap() + " produced more than 1 result", new TooManyRowsException());
					else
						return id;
				} else {
					log.info("Upserted " + rowCount + " row(s) with revision " + revision + " in table '" + tab.name + "' with values " + sourceResults.getValuesMap());
					Long id;
					switch(tab.getPrimaryKeyCols().size()) {
						case 1:
							id = ConvertUtils.convertSafely(Long.class, output.get(tab.getFirstPrimaryKeyCol().name), null);
							if(id == null)
								throw new ServiceException("Could not update row in table '" + tab.name + "' with values " + sourceResults.getValuesMap());
							break;
						case 0:
							id = null;
							break;
						default:
							// not sure what to do here
							id = null;
					}
					if(id == null) {
						if(ids == null)
							return id;
					} else if(ids != null)
						ids.add(id);
					else if(sourceResults.next())
						throw new ServiceException("Lookup of table '" + tab.name + "' with values " + sourceResults.getValuesMap() + " produced more than 1 result", new TooManyRowsException());
					else
						return id;
				}
			}
			if(log.isInfoEnabled())
				log.info("Updated table '" + tab.name + "' from " + sourceResults.getRowCount() + " source rows");
		} catch(SQLException e) {
			throw new ServiceException("Could not commit to target", e);
		} finally {
			sourceResults.close();
		}
		return null;
	}

	public int getMaxNameLength() {
		return maxNameLength;
	}

	/*
	 * public long partialUpdate(String table, Map<String, Object> keyValues, Map<String, Object> updateValues, ConnectionGroup connGroup) throws ServiceException {
		DataLoaderTab tab = db.getTab(table);
		if(tab == null)
			throw new ServiceException("Table '" + table + "' is not defined");
		final long revision;
		if(tab.getRevisionCol() == null)
			revision = 0L;
		else {
			Object rev = updateValues.get(tab.getRevisionCol().name);
			if(rev == null)
				throw new RetrySpecifiedServiceException("Revision value not provided for update to '" + tab.name + "' in parameter '" + tab.getRevisionCol().name + "'", WorkRetryType.NO_RETRY);
			try {
				revision = ConvertUtils.getLong(rev);
			} catch(ConvertException e) {
				throw new RetrySpecifiedServiceException("Could not convert revision value to long for update to '" + tab.name + "' in parameter '" + tab.getRevisionCol().name + "'", e, WorkRetryType.NO_RETRY);
			}
		}
		return partialUpdate(revision, tab, keyValues, updateValues, connGroup);
	}

	protected long partialUpdate(long revision, DataLoaderTab tab, Map<String, Object> keyValues, Map<String, Object> updateValues, ConnectionGroup connGroup) throws ServiceException {
		try {
			Call updateCall = tab.getPartialUpdateCall(keyValues, updateValues.keySet(), connGroup);
			if(updateCall == null) {
				log.warn("Update columns were not different from unique key columns; doing nothing");
				return 0;
			}
			CompositeMap<String, Object> input = new CompositeMap<String, Object>();
			input.merge(Collections.singletonMap(getRevisionLabel(), (Object) revision));
			input.merge(keyValues);
			input.merge(updateValues);
			Connection conn = connGroup.getConnection(getTargetDsn());
			int ret = updateCall.executeUpdate(conn, input, null, null);
			conn.commit();
			return ret;
		} catch(SQLException e) {
			throw new ServiceException("Could not update '" + tab.name + "' where " + keyValues, e);
		} catch(ParameterException e) {
			throw new ServiceException("Could not update '" + tab.name + "' where " + keyValues, e);
		} catch(DataLayerException e) {
			throw new ServiceException("Could not update '" + tab.name + "' where " + keyValues, e);
		}
	}
	 */
}
