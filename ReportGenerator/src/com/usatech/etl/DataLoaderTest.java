package com.usatech.etl;

import java.util.Collections;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.db.ConnectionGroup;
import simple.db.DataLayerMgr;
import simple.db.PlainConnectionGroup;

public class DataLoaderTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Throwable {
		Configuration config = BaseWithConfig.loadConfig("RDWLoader.properties", DataLoaderTest.class, null);
		BaseWithConfig.configureDataSourceFactory(config, null);
		BaseWithConfig.configureDataLayer(config);
		DataLoader loader = new DataLoader();
		loader.setTargetDsn("MAIN");
		loader.initialize();
		ConnectionGroup connGroup = new PlainConnectionGroup(DataLayerMgr.getDataSourceFactory());
		boolean okay = false;
		try {
			// loader.loadRow("ITEM_FACT", Collections.singletonMap("SOURCE_TRAN_LINE_ITEM_ID", (Object) 1177125), connGroup);
			loader.loadRow("ITEM_FACT", null, Collections.singletonMap("SOURCE_EVENT_ID", (Object) 1096405), connGroup);
			loader.loadRow("ITEM_FACT", null, Collections.singletonMap("SOURCE_EVENT_ID", (Object) 1096406), connGroup);
			loader.loadRow("ITEM_FACT", null, Collections.singletonMap("SOURCE_EVENT_ID", (Object) 1096407), connGroup);
			loader.loadRow("ITEM_FACT", null, Collections.singletonMap("SOURCE_EVENT_ID", (Object) 1096408), connGroup);
			connGroup.commit();
			okay = true;
		} finally {
			connGroup.close(!okay);
		}
	}

}
