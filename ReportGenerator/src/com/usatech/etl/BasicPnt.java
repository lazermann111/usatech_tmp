package com.usatech.etl;

import simple.sql.SQLType;

public class BasicPnt implements Pnt {
	protected final String name;
	protected final SQLType sqlType;
	protected final boolean required;
	protected final boolean input;
	protected final boolean output;

	public BasicPnt(String name, SQLType sqlType, boolean required, boolean input, boolean output) {
		this.name = name;
		this.sqlType = sqlType;
		this.required = required;
		this.input = input;
		this.output = output;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public SQLType getSQLType() {
		return sqlType;
	}

	@Override
	public boolean isRequired() {
		return required;
	}

	@Override
	public boolean isRef() {
		return false;
	}
	@Override
	public Col getRefCol() {
		return null;
	}

	public boolean isInput() {
		return input;
	}

	public boolean isOutput() {
		return output;
	}

	@Override
	public Pnt getLocalPnt() {
		return this;
	}
}
