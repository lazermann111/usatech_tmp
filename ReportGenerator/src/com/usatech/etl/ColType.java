package com.usatech.etl;

enum ColType {
	PRIMARY_KEY, IDENTITY_KEY, DATA, AUDIT_CREATE, AUDIT_UPDATE, CUSTOM, REVISION
}