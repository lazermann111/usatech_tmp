package com.usatech.etl;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import simple.app.PrerequisiteManager;
import simple.app.SelfProcessor;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.event.ProgressListener;
import simple.util.concurrent.ConcurrentQueueBuffer;

public class QueueBufferWorkQueue implements WorkQueue<SelfProcessor> {
	protected final ConcurrentQueueBuffer<Work<SelfProcessor>> queueBuffer = new ConcurrentQueueBuffer<Work<SelfProcessor>>(10);
	protected final PrerequisiteManager prerequisiteManager = new PrerequisiteManager();
	protected final AtomicBoolean shutdown = new AtomicBoolean();
	
	public int getQueueSize() {
		return queueBuffer.getCapacity();
	}

	public void setQueueSize(int queueSize) throws InterruptedException {
		queueBuffer.setCapacity(queueSize);
	}

	@Override
	public String getQueueKey() {
		return null;
	}

	@Override
	public WorkQueue.Retriever<SelfProcessor> getRetriever(ProgressListener listener) throws WorkQueueException {
		return new WorkQueue.Retriever<SelfProcessor>() {
			protected final AtomicBoolean taking = new AtomicBoolean();
			
			@Override
			public void cancelPrequisiteCheck(Thread thread) {
				prerequisiteManager.cancelPrequisiteCheck(thread);
			}

			@Override
			public boolean arePrequisitesAvailable() {
				return prerequisiteManager.arePrequisitesAvailable();
			}

			public void resetAvailability() {
				prerequisiteManager.resetAvailability();
			}

			@Override
			public Work<SelfProcessor> next() throws WorkQueueException, InterruptedException {
				if(shutdown.get())
					return null;
				taking.set(true);
				try {
					return queueBuffer.take();
				} finally {
					taking.set(false);
				}
			}

			@Override
			public boolean cancel(Thread thread) {
				// NOTE: this is not perfect, but good enough. A retrieving thread could have returned from queueBuffer.take() 
				//       but not yet set taking.set(false) when this runs in which case the first interruptible command of
				//       processing the work will throw an InterruptedException
				if(taking.get()) {
					thread.interrupt();
					return true;
				}
				return false;
			}

			@Override
			public void close() {
			}		
		};
	}

	@Override
	public Class<SelfProcessor> getWorkBodyType() {
		return SelfProcessor.class;
	}

	@Override
	public boolean isAutonomous() {
		return true;
	}

	@Override
	public Future<Boolean> shutdown() {
		shutdown.set(true);
		return TRUE_FUTURE;
	}

	public PrerequisiteManager getPrerequisiteManager() {
		return prerequisiteManager;
	}	
	
	public void addWork(Work<SelfProcessor> work, boolean awaitNotFull) throws InterruptedException {
		queueBuffer.put(work, awaitNotFull);
	}
	
	public void awaitNotFull() throws InterruptedException {
		queueBuffer.awaitNotFull();
	}

	public int getPendingCount() {
		return queueBuffer.size();
	}
}
