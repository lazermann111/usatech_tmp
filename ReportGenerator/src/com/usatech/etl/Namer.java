package com.usatech.etl;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.EnumMap;

import simple.text.StringUtils;

public class Namer {
	protected final String quotes;
	protected final char[] extras;
	protected final EnumMap<NameType, Integer> maxLengths = new EnumMap<NameType, Integer>(NameType.class);
	protected final NamePrep namePrep;

	protected static interface NamePrep {
		public String prepare(String name);
	}

	protected static final NamePrep TO_LOWER_CASE = new NamePrep() {
		@Override
		public String prepare(String argument) {
			return StringUtils.isBlank(argument) ? argument : argument.toLowerCase();
		}
	};
	protected static final NamePrep TO_UPPER_CASE = new NamePrep() {
		@Override
		public String prepare(String argument) {
			return StringUtils.isBlank(argument) ? argument : argument.toUpperCase();
		}
	};
	protected static final NamePrep TO_SAME_CASE = new NamePrep() {
		@Override
		public String prepare(String argument) {
			return argument;
		}
	};

	public Namer(DatabaseMetaData md) throws SQLException {
		this.quotes = md.getIdentifierQuoteString().trim();
		String s = md.getExtraNameCharacters();
		if(s != null) {
			this.extras = s.toCharArray();
			Arrays.sort(extras);
		} else
			this.extras = new char[0];
		maxLengths.put(NameType.CATALOG, md.getMaxCatalogNameLength());
		maxLengths.put(NameType.SCHEMA, md.getMaxSchemaNameLength());
		maxLengths.put(NameType.TABLE, md.getMaxTableNameLength());
		maxLengths.put(NameType.COLUMN, md.getMaxColumnNameLength());
		maxLengths.put(NameType.PROCEDURE, md.getMaxProcedureNameLength());
		if(!md.storesMixedCaseIdentifiers()) {
			if(md.storesLowerCaseIdentifiers())
				this.namePrep = TO_LOWER_CASE;
			else if(md.storesUpperCaseIdentifiers())
				this.namePrep = TO_UPPER_CASE;
			else
				this.namePrep = TO_SAME_CASE;
		} else
			this.namePrep = TO_SAME_CASE;
	}

	public Namer(String quotes, String extraNameCharacters, int maxCatalogNameLength, int maxSchemaNameLength, int maxTableNameLength, int maxColumnNameLength, int maxProcedureNameLength, boolean mixedIdentifiers, boolean lowerIdentifiers, boolean upperIdentifiers) {
		this.quotes = quotes == null ? "" : quotes.trim();
		if(extraNameCharacters != null) {
			this.extras = extraNameCharacters.toCharArray();
			Arrays.sort(extras);
		} else
			this.extras = new char[0];
		maxLengths.put(NameType.CATALOG, maxCatalogNameLength);
		maxLengths.put(NameType.SCHEMA, maxSchemaNameLength);
		maxLengths.put(NameType.TABLE, maxTableNameLength);
		maxLengths.put(NameType.COLUMN, maxColumnNameLength);
		maxLengths.put(NameType.PROCEDURE, maxProcedureNameLength);
		if(!mixedIdentifiers) {
			if(lowerIdentifiers)
				this.namePrep = TO_LOWER_CASE;
			else if(upperIdentifiers)
				this.namePrep = TO_UPPER_CASE;
			else
				this.namePrep = TO_SAME_CASE;
		} else
			this.namePrep = TO_SAME_CASE;
	}

	public StringBuilder appendExpression(StringBuilder sb, String name, NameType nameType) {
		name = getName(name, nameType);
		if(isQuotesRequired(name))
			sb.append(getQuotes()).append(name).append(getQuotes());
		else
			sb.append(name);
		return sb;
	}

	public Appendable appendExpression(Appendable sb, String name, NameType nameType) throws IOException {
		name = getName(name, nameType);
		if(isQuotesRequired(name))
			sb.append(getQuotes()).append(name).append(getQuotes());
		else
			sb.append(name);
		return sb;
	}

	public String getExpression(String name, NameType nameType) {
		name = getName(name, nameType);
		if(isQuotesRequired(name))
			return new StringBuilder().append(getQuotes()).append(name).append(getQuotes()).toString();
		return name;
	}

	public boolean isQuotesRequired(String name) {
		if(StringUtils.isBlank(name))
			return false;
		for(int i = 0; i < name.length(); i++) {
			char ch = name.charAt(i);
			if(!Character.isLetterOrDigit(ch) && ch != '_' && Arrays.binarySearch(extras, ch) < 0)
				return true;
		}
		return false;
	}
	public String getName(String name, NameType nameType) {
		if(name == null)
			return name;
		name = namePrep.prepare(name);
		Integer max = maxLengths.get(nameType);
		if(max == null || max <= 0 || name.length() <= max)
			return name;
		return name.substring(0, max - 9) + '#' + StringUtils.toHex(name.substring(max - 9).hashCode());
	}

	public String getQuotes() {
		return quotes;
	}
}