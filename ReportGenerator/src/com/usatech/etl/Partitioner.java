package com.usatech.etl;

public interface Partitioner<E> {
	public boolean next();

	public E getStart();

	public E getEnd();

	public void reset();
}
