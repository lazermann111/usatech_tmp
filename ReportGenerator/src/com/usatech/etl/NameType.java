package com.usatech.etl;

public enum NameType {
	CATALOG, SCHEMA, TABLE, COLUMN, PROCEDURE
}
