package com.usatech.etl;

import simple.app.ServiceException;


interface MetadataChangeTracker {
	public void tabAdded(Tab newTab);

	public void tabRemoved(Tab oldTab);

	public void tabModified(Tab oldTab, Tab newTab);

	public void colAdded(Col newCol);

	public void colRemoved(Col oldCol);

	public void colModified(Col oldCol, Col newCol);

	public void commit() throws ServiceException;
}