package com.usatech.etl;

import simple.text.StringUtils;

public class Ent {
	public static final Ent BLANK_ENT = new Ent(null);
	protected final String name;
	protected final String dsn;
	protected final String schema;
	protected final String table;
	protected final String catalog;

	public Ent(String name) {
		if(StringUtils.isBlank(name)) {
			this.name = dsn = schema = table = catalog = "";
		} else {
			this.name = name;
			String[] parts = StringUtils.split(name, '.');
			switch(parts.length) {
				case 1:
					dsn = "";
					schema = "";
					table = name;
					catalog = "";
					break;
				case 2:
					dsn = parts[0];
					schema = "";
					table = parts[1];
					catalog = "";
					break;
				case 3:
					dsn = parts[0];
					schema = parts[1];
					table = parts[2];
					catalog = "";
					break;
				case 4:
					dsn = parts[0];
					schema = parts[1];
					table = parts[3];
					catalog = parts[2];
					break;
				default:
					dsn = parts[0];
					schema = parts[1];
					table = StringUtils.join(parts, ".", 3, parts.length);
					catalog = parts[2];
					break;
			}
		}
	}

	public String getDsn() {
		return dsn;
	}

	public String getSchema() {
		return schema;
	}

	public String getTable() {
		return table;
	}

	public String getCatalog() {
		return catalog;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		return name.equals(obj.toString());
	}
}
