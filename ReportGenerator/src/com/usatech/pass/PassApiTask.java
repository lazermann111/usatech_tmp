package com.usatech.pass;

import java.util.Map;

import javax.net.ssl.HostnameVerifier;

import org.apache.http.HttpException;
import org.apache.http.client.methods.HttpRequestBase;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.HttpRequester;
import com.usatech.layers.common.HttpRequester.Authenticator;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.ConsumerCommAttrEnum;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.text.StringUtils;

public class PassApiTask implements MessageChainTask {
	private static final Log log = Log.getLog();

	protected HostnameVerifier hostNameVerifier;
	protected static int connectionTimeout = 3000;
	protected static int maxHostConnections = 500;
	protected static int maxTotalConnections = 100;
	protected static int socketTimeout = 10000;
	protected String passUrl;
	protected String username;
	protected String password;
	protected String apiVersion = "1.2";
	protected String environmentSuffix;
	protected String urbanAirShipApiKey;

	protected HttpRequester requester;

	protected Publisher<ByteInput> publisher;

	protected final Authenticator authenticator = new Authenticator() {
		@Override
		public void addAuthentication(HttpRequestBase method) throws HttpException {
			// Base64.encodeString(username + ":" + password, true)
			method.addHeader("Authorization", new StringBuilder("Basic ")
					.append(urbanAirShipApiKey).toString());

			method.addHeader("Accept", "application/json;version=1.2");
			method.addHeader("Api-Revision", apiVersion);
			method.addHeader("Content-Type", "application/json;version=1.2");
		}
	};

	public PassApiTask() {
		requester = new HttpRequester();
		if (hostNameVerifier != null)
			requester.setHostNameVerifier(hostNameVerifier);
		if (connectionTimeout >= 0)
			requester.setConnectionTimeout(connectionTimeout);
		if (maxHostConnections >= 0)
			requester.setMaxHostConnections(maxHostConnections);
		if (maxTotalConnections >= 0)
			requester.setMaxTotalConnections(maxTotalConnections);
		if (socketTimeout >= 0)
			requester.setSocketTimeout(socketTimeout);
	}

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String queueReplyKey = null;
		try {
			// extract main input parameters
			queueReplyKey = step.getAttribute(ConsumerCommAttrEnum.ATTR_MORE_PASS_QUEUENAME, String.class, true);
			String consumerAcctId; 
			String cardId; 
			String token = step.getAttribute(AuthorityAttrEnum.ATTR_AUTH_TOKEN, String.class, true);
			String templateId = step.getAttribute(ConsumerCommAttrEnum.ATTR_PASS_TEMPLATE_ID, String.class, true);
			String maskedCard;
			String callId = step.getAttribute(ConsumerCommAttrEnum.ATTR_CALL_ID, String.class, true);
			String publicKey = step.getAttribute(ConsumerCommAttrEnum.ATTR_PASS_PUBLIC_KEY, String.class, true);
			String consumerPassIdentifier = step.getAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_PASS_IDENTIFIER, String.class, true);

			// extract additional parameters depends on pass type
			Pass aPass;
			if (token == null || token.equals("0"))
				aPass = buildPassBody(consumerPassIdentifier, publicKey);
			else{
				consumerAcctId = step.getAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, String.class, true);
				cardId = step.getAttribute(ConsumerCommAttrEnum.ATTR_CARD_ID, String.class, true);
				maskedCard = step.getAttribute(ConsumerCommAttrEnum.ATTR_MASKED_CARD, String.class, false);
				aPass = buildPassBody(consumerPassIdentifier, consumerAcctId, cardId, token, maskedCard, publicKey);
			}
			// execute HTTP request
			String dynamicPassUrl = buildDynamicPassUrl(templateId);
			Map<String, Object> result = jsonPost(dynamicPassUrl, aPass);
			String url = extractUrlFromResult(result);
			if (url == null) {
				throw new RuntimeException("Incorrect response from: " + dynamicPassUrl + "; URL is missing");
			}

			// build and publish result
			publishReplyMessage(taskInfo, queueReplyKey, callId, url);
		} catch (Exception e) {
			publishErrorMessage(taskInfo, queueReplyKey, e.getClass().getName(), e.getMessage());
		}
		return 0;
	}

	private void publishReplyMessage(MessageChainTaskInfo taskInfo, String queueReplyKey, String callId,
			String url) throws ServiceException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep replyStep = mc.addStep(queueReplyKey);
		replyStep.setAttribute(ConsumerCommAttrEnum.ATTR_CALL_ID, callId);
		replyStep.setAttribute(ConsumerCommAttrEnum.ATTR_PASS_URL, url);
		MessageChainService.publish(mc, taskInfo.getPublisher());
	}

	private void publishErrorMessage(MessageChainTaskInfo taskInfo, String queueReplyKey, String errorCode, String errorMessage) throws ServiceException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep replyStep = mc.addStep(queueReplyKey);
		replyStep.setAttribute(CommonAttrEnum.ATTR_ERROR_CODE, errorCode);
		replyStep.setAttribute(CommonAttrEnum.ATTR_ERROR_MESSAGE, errorMessage);
		MessageChainService.publish(mc, taskInfo.getPublisher());
	}

	private String extractUrlFromResult(Map<String, Object> result) {
		if (!result.containsKey("url")) {
			return null;
		}

		String url = (String) result.get("url");
		if (StringUtils.isBlank(url)) {
			return null;
		}

		return url;
	}

	private Pass buildPassBody(String consumerPassIdentifier, String consumerAcctId, String cardId, String token, String maskedCard,
			String publicKey) {
		PassHeaders headers = new PassHeaders(new PassExpirationDate("2020-01-01T9:41-08:00"));
		PassFields fields = new PassFields();
		String nfcMessage = new StringBuilder(cardId).append("|").append(token).append("|").append(consumerPassIdentifier).toString();
		PassNfc nfc = new PassNfc(nfcMessage, publicKey);

		PassCustom custom = new PassCustom("edited custom value", maskedCard, "Custom Label");
		PassText text = new PassText("edited text", "MORE card", "MORE card");
		fields.setCustom(custom);
		fields.setText(text);
		String externalId = consumerAcctId + environmentSuffix;
		Pass aPass = new Pass(headers, fields, nfc, externalId);
		return aPass;
	}

	private Pass buildPassBody(String consumerPassIdentifier, String publicKey) {
		PassHeaders headers = new PassHeaders(new PassExpirationDate("2020-01-01T9:41-08:00"));
		PassFields fields = new PassFields();
		String nfcMessage = consumerPassIdentifier;
		PassNfc nfc = new PassNfc(nfcMessage, publicKey);

		PassCustom custom = new PassCustom("edited custom value", consumerPassIdentifier, "Custom Label");
		PassText text = new PassText("edited text", "MORE card", "MORE card");
		fields.setCustom(custom);
		fields.setText(text);
		String externalId = consumerPassIdentifier + environmentSuffix;
		Pass aPass = new Pass(headers, fields, nfc, externalId);
		return aPass;
	}
	
	private String buildDynamicPassUrl(String templateId) {
		return passUrl + "/pass/" + templateId + "/dynamic";
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> jsonPost(String url, Pass body) {
		try {
			return requester.jsonPost(url, body, Map.class, Map.class, authenticator);
		} catch (Exception e) {
			log.error("Failed to request: " + url, e);
			throw new RuntimeException("Failed to request: " + url, e);
		}
	}

	private void processStub(MessageChainTaskInfo taskInfo, MessageChainStep step)
			throws AttributeConversionException, ServiceException {
		String queueReplyKey = step.getAttribute(ConsumerCommAttrEnum.ATTR_MORE_PASS_QUEUENAME, String.class, false);
		String callId = step.getAttribute(ConsumerCommAttrEnum.ATTR_CALL_ID, String.class, false);
		String passUrl = "https://wallet-api.urbanairship.com/v1/pass/dynamic/f2da6cbc-80f0-4fa9-bd4e-1c9817b2147b";
		publishReplyMessage(taskInfo, queueReplyKey, callId, passUrl);
	}

	public HostnameVerifier getHostNameVerifier() {
		return hostNameVerifier;
	}

	public void setHostNameVerifier(HostnameVerifier hostNameVerifier) {
		this.hostNameVerifier = hostNameVerifier;
	}

	public String getPassUrl() {
		return passUrl;
	}

	public void setPassUrl(String passUrl) {
		this.passUrl = passUrl;
	}

	public String getUrbanAirShipApiKey() {
		return urbanAirShipApiKey;
	}

	public void setUrbanAirShipApiKey(String urbanAirShipApiKey) {
		this.urbanAirShipApiKey = urbanAirShipApiKey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static int getConnectionTimeout() {
		return connectionTimeout;
	}

	public static void setConnectionTimeout(int connectionTimeout) {
		PassApiTask.connectionTimeout = connectionTimeout;
	}

	public static int getMaxHostConnections() {
		return maxHostConnections;
	}

	public static void setMaxHostConnections(int maxHostConnections) {
		PassApiTask.maxHostConnections = maxHostConnections;
	}

	public static int getMaxTotalConnections() {
		return maxTotalConnections;
	}

	public static void setMaxTotalConnections(int maxTotalConnections) {
		PassApiTask.maxTotalConnections = maxTotalConnections;
	}

	public static int getSocketTimeout() {
		return socketTimeout;
	}

	public static void setSocketTimeout(int socketTimeout) {
		PassApiTask.socketTimeout = socketTimeout;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public String getEnvironmentSuffix() {
		return environmentSuffix;
	}

	public void setEnvironmentSuffix(String environmentSuffix) {
		this.environmentSuffix = environmentSuffix;
	}

}
