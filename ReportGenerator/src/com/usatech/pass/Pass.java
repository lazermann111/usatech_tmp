
package com.usatech.pass;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Pass {

    private PassHeaders headers;
    private PassFields fields;
    private PassNfc nfc;
    private String externalId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Pass() {
    }

    public Pass(PassHeaders headers, PassFields fields, PassNfc nfc, String externalId) {
			super();
			this.headers = headers;
			this.fields = fields;
			this.nfc = nfc;
			this.externalId=externalId;
		}
		/**
     * 
     * @return
     *     The headers
     */
    public PassHeaders getHeaders() {
        return headers;
    }

    /**
     * 
     * @param headers
     *     The headers
     */
    public void setHeaders(PassHeaders headers) {
        this.headers = headers;
    }

    /**
     * 
     * @return
     *     The fields
     */
    public PassFields getFields() {
        return fields;
    }

    /**
     * 
     * @param fields
     *     The fields
     */
    public void setFields(PassFields fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

		public PassNfc getNfc() {
			return nfc;
		}

		public void setNfc(PassNfc nfc) {
			this.nfc = nfc;
		}

		public String getExternalId() {
			return externalId;
		}

		public void setExternalId(String externalId) {
			this.externalId = externalId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((externalId == null) ? 0 : externalId.hashCode());
			result = prime * result + ((fields == null) ? 0 : fields.hashCode());
			result = prime * result + ((headers == null) ? 0 : headers.hashCode());
			result = prime * result + ((nfc == null) ? 0 : nfc.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Pass other = (Pass)obj;
			if (externalId == null) {
				if (other.externalId != null)
					return false;
			} else if (!externalId.equals(other.externalId))
				return false;
			if (fields == null) {
				if (other.fields != null)
					return false;
			} else if (!fields.equals(other.fields))
				return false;
			if (headers == null) {
				if (other.headers != null)
					return false;
			} else if (!headers.equals(other.headers))
				return false;
			if (nfc == null) {
				if (other.nfc != null)
					return false;
			} else if (!nfc.equals(other.nfc))
				return false;
			return true;
		}
    

}
