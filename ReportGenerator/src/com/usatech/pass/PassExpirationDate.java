
package com.usatech.pass;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PassExpirationDate {

    private String value;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PassExpirationDate() {
    }

    /**
     * 
     * @param value
     */
    public PassExpirationDate(String value) {
        super();
        this.value = value;
    }

    /**
     * 
     * @return
     *     The value
     */
    public String getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(value).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PassExpirationDate) == false) {
            return false;
        }
        PassExpirationDate rhs = ((PassExpirationDate) other);
        return new EqualsBuilder().append(value, rhs.value).isEquals();
    }

}
