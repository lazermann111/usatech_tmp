
package com.usatech.pass;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PassCustom {

    private String changeMessage;
    private String value;
    private String label;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PassCustom() {
    }

    /**
     * 
     * @param changeMessage
     * @param label
     * @param value
     */
    public PassCustom(String changeMessage, String value, String label) {
        super();
        this.changeMessage = changeMessage;
        this.value = value;
        this.label = label;
    }

    /**
     * 
     * @return
     *     The changeMessage
     */
    public String getChangeMessage() {
        return changeMessage;
    }

    /**
     * 
     * @param changeMessage
     *     The changeMessage
     */
    public void setChangeMessage(String changeMessage) {
        this.changeMessage = changeMessage;
    }

    /**
     * 
     * @return
     *     The value
     */
    public String getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 
     * @return
     *     The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * 
     * @param label
     *     The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(changeMessage).append(value).append(label).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PassCustom) == false) {
            return false;
        }
        PassCustom rhs = ((PassCustom) other);
        return new EqualsBuilder().append(changeMessage, rhs.changeMessage).append(value, rhs.value).append(label, rhs.label).isEquals();
    }

}
