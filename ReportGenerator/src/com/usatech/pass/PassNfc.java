package com.usatech.pass;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class PassNfc implements Serializable {

	private static final long serialVersionUID = -3863044260951294791L;

	private String message;

	private String encryptionPublicKey;

	public PassNfc() {

	}

	public PassNfc(String message, String encryptionPublicKey) {
		this.message = message;
		this.encryptionPublicKey = encryptionPublicKey;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEncryptionPublicKey() {
		return encryptionPublicKey;
	}

	public void setEncryptionPublicKey(String encryptionPublicKey) {
		this.encryptionPublicKey = encryptionPublicKey;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((encryptionPublicKey == null) ? 0 : encryptionPublicKey.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassNfc other = (PassNfc) obj;
		if (encryptionPublicKey == null) {
			if (other.encryptionPublicKey != null)
				return false;
		} else if (!encryptionPublicKey.equals(other.encryptionPublicKey))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}

}
