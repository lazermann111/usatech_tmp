
package com.usatech.pass;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PassFields {

    private PassCustom custom;
    private PassText text;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PassFields() {
    }

    /**
     * 
     * @param custom
     * @param text
     */
    public PassFields(PassCustom custom, PassText text) {
        super();
        this.custom = custom;
        this.text = text;
    }

    /**
     * 
     * @return
     *     The custom
     */
    public PassCustom getCustom() {
        return custom;
    }

    /**
     * 
     * @param custom
     *     The Custom
     */
    public void setCustom(PassCustom custom) {
        this.custom = custom;
    }

    /**
     * 
     * @return
     *     The text
     */
    public PassText getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The Text
     */
    public void setText(PassText text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(custom).append(text).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PassFields) == false) {
            return false;
        }
        PassFields rhs = ((PassFields) other);
        return new EqualsBuilder().append(custom, rhs.custom).append(text, rhs.text).isEquals();
    }

}
