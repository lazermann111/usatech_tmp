
package com.usatech.pass;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PassHeaders {

    private PassExpirationDate expirationDate;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PassHeaders() {
    }

    /**
     * 
     * @param expirationDate
     */
    public PassHeaders(PassExpirationDate expirationDate) {
        super();
        this.expirationDate = expirationDate;
    }

    /**
     * 
     * @return
     *     The expirationDate
     */
    public PassExpirationDate getExpirationDate() {
        return expirationDate;
    }

    /**
     * 
     * @param expirationDate
     *     The expirationDate
     */
    public void setExpirationDate(PassExpirationDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(expirationDate).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PassHeaders) == false) {
            return false;
        }
        PassHeaders rhs = ((PassHeaders) other);
        return new EqualsBuilder().append(expirationDate, rhs.expirationDate).isEquals();
    }

}
