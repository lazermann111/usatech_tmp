package com.usatech.transport;

public enum TransportReason {
	BATCH_REPORT, TIMED_REPORT, ALERT, BROADCAST, TEST, CUSTOM_FILE_UPLOAD, CONDITION, SINGLE_TXN
}
