package com.usatech.transport;

import java.util.Map;

import simple.bean.ConvertUtils;
import simple.ftp.SimpleFtpsBean;

import com.usatech.ftps.EncryptType;
import com.usatech.ftps.FtpsBean;

import ftp.FtpBean;

public class FTPSTransporter extends FTPTransporter {
	protected boolean hostVerifying;

	protected class FtpsKey extends FtpKey {
		protected final EncryptType encryptType;

		public FtpsKey(String host, int port, String username, String password, EncryptType encryptType) {
			super(host, port, username, password);
			this.encryptType = encryptType;
		}

		public EncryptType getEncryptType() {
			return encryptType;
		}

		@Override
		public boolean equals(Object obj) {
			return (obj instanceof FtpsKey) && super.equals(obj) && ((FtpsKey) obj).encryptType == encryptType;
		}

		@Override
		public String toString() {
			return getUsername() + '@' + getHost() + ':' + getPort() + " (" + encryptType + ")";
		}
	}
	public FTPSTransporter() {
		super();
	}

	@Override
	protected FtpKey createKey(String host, int port, String username, String password, Map<String, ? extends Object> transportProperties) {
		EncryptType encryptType = ConvertUtils.convertSafely(EncryptType.class, transportProperties.get("ENCRYPT TYPE"), null);
		if(encryptType == null) {
			if(ConvertUtils.getBooleanSafely(transportProperties.get("IMPLICIT SSL"), false))
				encryptType = EncryptType.SSL_IMPLICIT;
			else
				encryptType = EncryptType.SSL_REQUIRE;
		}
		return new FtpsKey(host, port, username, password, encryptType);
	}

	@Override
	protected FtpBean createFtpBean(FtpKey key) {
		FtpsBean ftpsBean = new SimpleFtpsBean();
		ftpsBean.setEncryptType(((FtpsKey) key).getEncryptType());
		ftpsBean.setHostVerifying(isHostVerifying());
		return ftpsBean;
	}

	/**
	 * @see com.usatech.transport.FTPTransporter#getProtocol()
	 */
	@Override
	protected String getProtocol() {
		return "ftps";
	}

	public boolean isHostVerifying() {
		return hostVerifying;
	}

	public void setHostVerifying(boolean hostVerifying) {
		this.hostVerifying = hostVerifying;
	}
}
