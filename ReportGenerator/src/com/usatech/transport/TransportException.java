package com.usatech.transport;

public class TransportException extends Exception {
    private static final long serialVersionUID = 3256437006405218617L;
    protected TransportErrorType errorType;
    protected String retryProps;
    public TransportException() {
        super();
    }
    
	public TransportException(TransportErrorType errorType) {
        super();
        this.errorType = errorType;
    }
    
	public TransportException(String message, TransportErrorType errorType) {
		super(message);
		this.errorType = errorType;
	}

	public TransportException(Throwable cause, TransportErrorType errorType) {
		super(cause);
		this.errorType = errorType;
	}

	public TransportException(String message, Throwable cause, TransportErrorType errorType) {
		super(message, cause);
		this.errorType = errorType;
	}

	public TransportErrorType getErrorType() {
		return errorType;
	}

	public void setErrorType(TransportErrorType errorType) {
		this.errorType = errorType;
	}

	public String getRetryProps() {
		if(retryProps==null){
			return "";
		}else{
			return retryProps;
		}
	}

	public void setRetryProps(String retryProps) {
		this.retryProps = retryProps;
	}
	
}