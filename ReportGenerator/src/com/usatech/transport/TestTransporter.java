package com.usatech.transport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;

/**
 * This is a test transporter, simply write file to a local file system.
 * @author yhe
 *
 */
public class TestTransporter implements Transporter {
	private static final Log log = Log.getLog();
	protected String transportFolder;

	public TestTransporter(String transportFolder) {
		super();
		this.transportFolder = transportFolder;
	}

	/* (non-Javadoc)
	 * @see com.usatech.transport.Transporter#transport(java.util.Map, com.usatech.transport.TransportReason, javax.activation.DataSource, java.util.Map)
	 */
	public String transport(Map<String, ? extends Object> transportProperties,
			TransportReason transportReason, Resource resource,
			Map<String, ? extends Object> messageParameters)
			throws TransportException {
		try{
			if(!checkDir(transportFolder)){
					log.warn("TestTransporter fails to create transportFolder:"+transportFolder);
			}
			IOUtils.copy(resource.getInputStream(), new FileOutputStream(new File(transportFolder,resource.getName())));
			return "TestTransporter write file:"+resource.getName()+" to folder:"+transportFolder;
		}catch (IOException ioe){
			log.warn("Error using TestTransporter", ioe);
            throw new TransportException(ioe.getMessage(), ioe,TransportErrorType.INSTANCE_ERR);
		}
	}
	public static boolean checkDir(String fileDir){
		File directory=new File(fileDir);
		if(!directory.exists()){
			return directory.mkdirs();
		}
		else return true;
	}
}
