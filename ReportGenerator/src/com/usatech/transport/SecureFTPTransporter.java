package com.usatech.transport;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.sftp.SftpUtils;

import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.InvalidHostFileException;

/**
 * @author Brian S. Krug
 *
 */
public class SecureFTPTransporter implements Transporter {
	private static final Log log = Log.getLog();

	protected HostKeyVerification hostKeyVerification;

	public SecureFTPTransporter(Properties props) throws InvalidHostFileException {
		super();
		init(props);
	}

	public void init(Properties props) throws InvalidHostFileException {
		String knownHostsFile = props.getProperty("knownhosts.file");
		hostKeyVerification = SftpUtils.createHostKeyVerification(knownHostsFile);
	}

	public String transport(Map<String, ? extends Object> transportProperties, TransportReason transportReason, Resource resource, Map<String, ? extends Object> messageParameters) throws TransportException {
		String host, username, password, remoteDir, privateKey, passphrase, dsaPrivateKey, rsaPrivateKey, tmpPrefix, tmpSuffix;
		int port;
		try {
			host= ConvertUtils.getString(transportProperties.get("HOST"), false);
	        port = ConvertUtils.getInt(transportProperties.get("PORT"));
	        username = ConvertUtils.getString(transportProperties.get("USERNAME"), false);
	        password = ConvertUtils.getString(transportProperties.get("PASSWORD"), false);
	        remoteDir = ConvertUtils.getString(transportProperties.get("REMOTE PATH"), false);
	        privateKey = ConvertUtils.getString(transportProperties.get("PRIVATE KEY"), false);
	        passphrase = ConvertUtils.getString(transportProperties.get("PRIVATE KEY PASSPHRASE"), false);
	        dsaPrivateKey = ConvertUtils.getString(transportProperties.get("DSA PRIVATE KEY"), false);
	        rsaPrivateKey = ConvertUtils.getString(transportProperties.get("RSA PRIVATE KEY"), false);
	        tmpPrefix = ConvertUtils.getString(transportProperties.get("TMP PREFIX"), false);
	        tmpSuffix = ConvertUtils.getString(transportProperties.get("TMP SUFFIX"), false);
		} catch(ConvertException e){
			throw new TransportException(
					"ConvertException while converting transport parameters", e, TransportErrorType.INSTANCE_ERR);
		}
		if(host != null) {
            try {
    	        // Make a client connection
    	        SshClient ssh = new SshClient();
    	        // Connect to the host
    	        ssh.connect(host, hostKeyVerification);
    	        SftpClient sftp = null;
    	        try {
    	        	SftpUtils.authenticate(ssh, username, password, privateKey == null ? null : privateKey.getBytes(), passphrase, dsaPrivateKey == null ? null : dsaPrivateKey.getBytes(), rsaPrivateKey == null ? null : rsaPrivateKey.getBytes());

    	            // The connection is authenticated we can now do some real work!
    	            sftp = ssh.openSftpClient();

    	            // Change directory
    	            if(remoteDir != null && remoteDir.trim().length() > 0) sftp.cd(remoteDir);

    	            // Upload a file
    	            boolean rename = false;
                    StringBuilder sb = new StringBuilder();
                    if(tmpPrefix != null && (tmpPrefix=tmpPrefix.trim()).length() > 0) {
                    	sb.append(tmpPrefix);
                    	rename = true;
                    }
                    sb.append(resource.getName());
                    if(tmpSuffix != null && (tmpSuffix=tmpSuffix.trim()).length() > 0) {
                    	sb.append(tmpSuffix);
                    	rename = true;
                    }
                    String tmpName = sb.toString();
                    sftp.put(resource.getInputStream(), tmpName);
                    
                    if(rename)
                    	sftp.rename(tmpName, resource.getName());
    	        } finally {
    	            if (sftp != null)
    	                sftp.quit();
    	            ssh.disconnect();
    	        }
            } catch(IOException ioe) {
                log.warn("While sftping file", ioe);
                throw new TransportException(ioe.getMessage(), ioe, TransportErrorType.TRANSPORT_ERR);
            } /*catch(Exception e) {
                log.warn("While sftping file", e);
                throw new TransportException(e.getMessage());
            }*/
            return "sftp: " + username + "@" + host + ":" + port +
                (remoteDir != null && remoteDir.trim().length() > 0 ? "/" + remoteDir : "") + "/" + resource.getName();
		} else {
            return "NOT SENT";
        }
	}
}
