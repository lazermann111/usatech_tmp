package com.usatech.transport;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeoutException;

import org.apache.commons.io.input.CountingInputStream;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.ftp.SimpleFtpBean;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.lang.SystemUtils;
import simple.util.concurrent.Cabinent;
import simple.util.concurrent.CreationException;
import ftp.FtpBean;
import ftp.FtpException;

public class FTPTransporter implements Transporter {
	private static final Log log = Log.getLog();
	protected static final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
	protected int socketTimeout;
	protected int readTimeout;
	protected long compressionMinimum = 1024 * 10; // must be at least 10K
	protected final Cabinent<FtpKey, FtpBean> cabinent = new Cabinent<FtpKey, FtpBean>() {
		protected FtpBean createItem(FtpKey key) throws Exception {
			return createAndConnect(key);
		}

		protected void configureItem(FtpKey key, FtpBean item, boolean initial) throws Exception {
			if(initial || key.defaultDirectory == null) {
				key.defaultDirectory = item.getDirectory();
				if(key.defaultDirectory == null)
					throw new IOException("Not connected");
			} else
				item.setDirectory(key.defaultDirectory);
		}

		protected void closeItem(FtpBean item) throws Exception {
			item.close();
		}
	};

	protected class FtpKey {
		protected final String host;
		protected final int port;
		protected final String username;
		protected final String password;
		protected String defaultDirectory;

		public FtpKey(String host, int port, String username, String password) {
			super();
			this.host = host;
			this.port = port;
			this.username = username;
			this.password = password;
		}

		public String getHost() {
			return host;
		}

		public int getPort() {
			return port;
		}

		public String getUsername() {
			return username;
		}

		public String getPassword() {
			return password;
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof FtpKey))
				return false;
			FtpKey k = (FtpKey) obj;
			return ConvertUtils.areEqual(host, k.host) && port == k.port && ConvertUtils.areEqual(username, k.username) && ConvertUtils.areEqual(password, k.password);
		}

		@Override
		public int hashCode() {
			return SystemUtils.addHashCodes(port, host, username, password);
		}

		@Override
		public String toString() {
			return getUsername() + '@' + getHost() + ':' + getPort();
		}
	}

	public FTPTransporter() {
		cabinent.schedulePurgeJob(executor, 600000);
	}

	public String transport(Map<String,? extends Object> transportProperties, TransportReason transportReason, Resource resource, Map<String, ? extends Object> messageParameters) throws TransportException {
        String host, username, password, remoteDir, tmpPrefix, tmpSuffix, dateFolderFormat;
        int port;
        long folderDate;
        boolean useDateFolder=false;
        try {
	        host= ConvertUtils.getString(transportProperties.get("HOST"), false);
	        port = ConvertUtils.getInt(transportProperties.get("PORT"));
	        username = ConvertUtils.getString(transportProperties.get("USERNAME"), false);
	        password = ConvertUtils.getString(transportProperties.get("PASSWORD"), false);
	        remoteDir = ConvertUtils.getString(transportProperties.get("REMOTE PATH"), false);
	        tmpPrefix = ConvertUtils.getString(transportProperties.get("TMP PREFIX"), false);
	        tmpSuffix = ConvertUtils.getString(transportProperties.get("TMP SUFFIX"), false);
	        dateFolderFormat = ConvertUtils.getString(transportProperties.get("DATE FORMATTED FOLDER"), false);
	        folderDate=ConvertUtils.getLongSafely(messageParameters.get("endDate"), System.currentTimeMillis());
		} catch(ConvertException e){
			throw new TransportException(
					"ConvertException while converting transport parameters", e, TransportErrorType.INSTANCE_ERR);
		}
		if(host != null) {
            // Make a client connection
			FtpKey key = createKey(host, port, username, password, transportProperties);
			try {
				FtpBean ftp = getFtpBean(key);
				try {
					// Change directory
					if(dateFolderFormat != null && dateFolderFormat.trim().length() > 0) {
						useDateFolder = true;
						SimpleDateFormat dateFormat = new SimpleDateFormat(dateFolderFormat.trim());
						if(remoteDir != null && remoteDir.trim().length() > 0) {
							remoteDir = remoteDir + "/" + dateFormat.format(new Date(folderDate));
						} else {
							remoteDir = dateFormat.format(new Date(folderDate));
						}
						if(ftp.getFileInfoAsString(remoteDir) == null) {
							ftp.makeDirectory(remoteDir);
						}
						ftp.setDirectory(remoteDir);
					} else {
						if(remoteDir != null && remoteDir.trim().length() > 0) {
							ftp.setDirectory(remoteDir);
						}
					}

					boolean rename = false;
					StringBuilder sb = new StringBuilder();
					if(tmpPrefix != null && (tmpPrefix = tmpPrefix.trim()).length() > 0) {
						sb.append(tmpPrefix);
						rename = true;
					}
					sb.append(resource.getName());
					if(tmpSuffix != null && (tmpSuffix = tmpSuffix.trim()).length() > 0) {
						sb.append(tmpSuffix);
						rename = true;
					}
					String tmpName = sb.toString();
					if(getCompressionMinimum() >= 0 && getCompressionMinimum() < Long.MAX_VALUE && getCompressionMinimum() <= resource.getLength())
						ftp.enableCompression();
					try {
						CountingInputStream cis = new CountingInputStream(resource.getInputStream());
						try {
							ftp.putFile(tmpName, cis, -1);
						} finally {
							cis.close();
						}
						log.info("Wrote " + cis.getByteCount() + " bytes to " + tmpName);
					} finally {
						if(ftp.isCompressing())
							ftp.disableCompression();
					}

					if(rename)
						ftp.fileRename(tmpName, resource.getName());
				} catch(IOException ioe) {
					cabinent.invalidateItem(key, ftp);
					log.warn("While ftping file", ioe);
					throw new TransportException(ioe.getMessage(), ioe, TransportErrorType.TRANSPORT_ERR);
				} finally {
					cabinent.returnItem(key, ftp);
				}
            } catch (FtpException e) {
				log.warn("While ftping file",e);
				if(useDateFolder){
					e=new FtpException("Use dateFolder:"+remoteDir+". "+e.getMessage());
				}
                throw new TransportException(e.getMessage(), e, TransportErrorType.TRANSPORT_ERR);
            } catch(IOException ioe) {
                log.warn("While ftping file", ioe);
                throw new TransportException(ioe.getMessage(), ioe, TransportErrorType.TRANSPORT_ERR);
            }
            return getProtocol() + ": " + username + "@" + host + ":" + port +
                (remoteDir != null && remoteDir.trim().length() > 0 ? "/" + remoteDir : "") + "/" + resource.getName();
        }
		return "NOT SENT";
	}

	protected FtpBean getFtpBean(FtpKey key) throws IOException, FtpException {
		try {
			return cabinent.getItem(key);
		} catch(TimeoutException | InterruptedException e) {
			throw new IOException(e);
		} catch(CreationException e) {
			if(e.getCause() instanceof IOException)
				throw (IOException) e.getCause();
			if(e.getCause() instanceof FtpException)
				throw (FtpException) e.getCause();
			throw new IOException(e);
		}
	}

	protected FtpKey createKey(String host, int port, String username, String password, Map<String, ? extends Object> transportProperties) {
		return new FtpKey(host, port, username, password);
	}

	protected FtpBean createFtpBean(FtpKey key) {
		return new SimpleFtpBean();
	}

	protected FtpBean createAndConnect(FtpKey key) throws IOException, FtpException {
		FtpBean ftpBean = createFtpBean(key);
		if(ftpBean.getPort() != key.getPort())
			ftpBean.setPort(key.getPort());
		if(ftpBean.getSocketTimeout() != getSocketTimeout())
			ftpBean.setSocketTimeout(getSocketTimeout());
		if(ftpBean.getReadTimeout() != getReadTimeout())
			ftpBean.setReadTimeout(getReadTimeout());
		// login
		if(log.isInfoEnabled())
			log.info("Attempting to connect to " + key.getUsername() + '@' + key.getHost() + ':' + key.getPort());
		ftpBean.ftpConnect(key.getHost(), key.getUsername(), key.getPassword());
		if(log.isInfoEnabled())
			log.info("Created new FTP Bean for " + key.getUsername() + '@' + key.getHost() + ':' + key.getPort());
		return ftpBean;
	}
	protected String getProtocol() {
		return "ftp";
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}

	public long getMaxWait() {
		return cabinent.getMaxWait();
	}

	public void setMaxWait(long maxWait) {
		cabinent.setMaxWait(maxWait);
	}

	public boolean isTracing() {
		return cabinent.isTracing();
	}

	public void setTracing(boolean tracing) {
		cabinent.setTracing(tracing);
	}

	public int getMaxIdle() {
		return cabinent.getMaxIdle();
	}

	public void setMaxIdle(int maxIdle) {
		cabinent.setMaxIdle(maxIdle);
	}

	public int getMaxActive() {
		return cabinent.getMaxActive();
	}

	public void setMaxActive(int maxActive) {
		cabinent.setMaxActive(maxActive);
	}

	public int getMaxKeys() {
		return cabinent.getMaxKeys();
	}

	public void setMaxKeys(int maxKeys) {
		cabinent.setMaxKeys(maxKeys);
	}

	public long getMaxInactivity() {
		return cabinent.getMaxInactivity();
	}

	public void setMaxInactivity(long maxInactivity) {
		cabinent.setMaxInactivity(maxInactivity);
	}

	public long getCompressionMinimum() {
		return compressionMinimum;
	}

	/**
	 * Sets the minimum size of a file above which compression is used if available. Set to -1 or Long.MAX_VALUE to disable compression
	 * 
	 * @param compressionMinimum
	 */
	public void setCompressionMinimum(long compressionMinimum) {
		this.compressionMinimum = compressionMinimum;
	}

	/** The sun.net.ftp.FtpClient does not correctly handle passive mode, so instead use FtpBean (in method ftp2Report)
     *
     */
 /*           try {
                // Make a client connection
                FtpClient ftp = new FtpClient();
                ftp.openServer(host, port);
                try {
                    // login
                    ftp.login(username, password);

                    // Change directory
                    if(remoteDir != null && remoteDir.trim().length() > 0) ftp.cd(remoteDir);

                    // Upload a file
                    OutputStream out = ftp.put(a.filename);
                    out.write(a.contents.getBytes());
                    out.flush();
                    out.close();
                } finally {
                    ftp.closeServer();
                }
            } catch(IOException ioe) {
                log.warn("While ftping file", ioe);
                throw new TransportException(ioe.getMessage());
            }
        */

}
