package com.usatech.transport;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Base64;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.text.StringUtils;

/**
 * POST the file to the given URL using Multipart HTTP POST
 * 
 */
public class MultipartPOSTTransporter extends AbstractHttpTransporter {
	private static final Log log = Log.getLog();

	protected final Map<String, String> headerFromParameter = new LinkedHashMap<String, String>();

	public MultipartPOSTTransporter() {
		super(new String[] { "TLSv1.1", "TLSv1.2" });
		headerFromParameter.put("batchId", "batchId");
		headerFromParameter.put("beginDate", "beginDate");
		headerFromParameter.put("endDate", "endDate");
	}

	@Override
	public String transport(Map<String, ? extends Object> transportProperties, TransportReason transportReason, Resource resource, Map<String, ? extends Object> messageParameters) throws TransportException {
		try {
			String url = ConvertUtils.getString(transportProperties.get("URL"), true);
			String acceptHeader = ConvertUtils.getString(transportProperties.get("ACCEPT HEADER"), true);
			String fileField = ConvertUtils.getString(transportProperties.get("FILE FIELD"), true);
			String username = ConvertUtils.getString(transportProperties.get("USERNAME"), false);
			String password = ConvertUtils.getString(transportProperties.get("PASSWORD"), false);

			StringBuilder sb = new StringBuilder();
			if(!url.matches("^\\w+://.*"))
				sb.append("https://");
			int pos = 0;
			while(pos < url.length() && url.charAt(pos) == '/')
				pos++;
			sb.append(url, pos, url.length());

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.addTextBody("fileName", StringUtils.prepareURLPart(resource.getName()));
			builder.addTextBody("reason", StringUtils.prepareURLPart(StringUtils.capitalizeAllWords(transportReason.toString().replace('_', ' '))));
			for(Map.Entry<String, String> entry : headerFromParameter.entrySet()) {
				if("reason".equalsIgnoreCase(entry.getKey()))
					continue;
				String value = ConvertUtils.getStringSafely(messageParameters.get(entry.getValue()), null);
				if(!StringUtils.isBlank(value))
					builder.addTextBody(StringUtils.prepareURLPart(entry.getKey()), StringUtils.prepareURLPart(value)); 
			}
			builder.addBinaryBody(fileField, resource.getInputStream());

			HttpPost httpMethod = new HttpPost(sb.toString());
			updateRequestSettings(httpMethod);
			httpMethod.addHeader("Accept", acceptHeader);
			if(username != null) {
				String base64Token = Base64.encodeString(username + ":" + password, true);
				httpMethod.addHeader("Authorization", new StringBuilder("Basic ").append(base64Token).toString());
			}

			HttpEntity entity = builder.build();
			httpMethod.setEntity(entity);
			log.info("Sending file via POST Request for '" + sb.toString() + "'");
			HttpResponse response = httpClient.execute(httpMethod);
			final int returnCode = response.getStatusLine().getStatusCode();
			final String responseBody = readContent(response.getEntity(), 2000);
			String returnInfo = returnCode + ": " + response.getStatusLine().getReasonPhrase();
			if (!StringUtils.isBlank(responseBody))
				returnInfo += ": " + responseBody;
			log.info("POST Response = " + returnInfo);
			switch(returnCode) {
				case 200:
				case 201:
				case 202:
				case 203:
				case 204:
					break;// okay
				default:
					throw new TransportException("POST Request was not successful: " + returnInfo, TransportErrorType.INSTANCE_ERR);
			}
			return returnInfo;
		} catch(ConvertException e) {
			throw new TransportException("ConvertException while converting transport parameters", e, TransportErrorType.INSTANCE_ERR);
		} catch(IOException e) {
			log.warn("Could not send POST request", e);
			throw new TransportException(e.getMessage(), e, TransportErrorType.TRANSPORT_ERR);
		}
	}
}
