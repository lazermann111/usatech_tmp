package com.usatech.transport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Base64;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;

import com.usatech.report.rest.ImportResponse;

public class RESTTransporter extends AbstractHttpTransporter {
	private static final Log log = Log.getLog();
	
	public RESTTransporter() {
		super();
	}

	@Override
	public String transport(Map<String, ? extends Object> transportProperties,
			TransportReason transportReason, Resource resource,
			Map<String, ? extends Object> messageParameters)
			throws TransportException {
		try {
			String url= ConvertUtils.getString(transportProperties.get("URL"), false);
	        String username = ConvertUtils.getString(transportProperties.get("USERNAME"), false);
	        String password = ConvertUtils.getString(transportProperties.get("PASSWORD"), false);
			HttpPost httpMethod = new HttpPost(url);
			httpMethod.addHeader("accept", "text/xml");
			if(username!=null){
				String base64Token=Base64.encodeString(username+":"+password, true);
				httpMethod.addHeader("Authorization", new StringBuilder("Basic ").append(base64Token).toString());
			}
			String messageType=ConvertUtils.getStringSafely(messageParameters.get("MessageType"), "MktCreditTxn");
			List<NameValuePair> parameters = new ArrayList<>(2);
			parameters.add(new BasicNameValuePair("MessageType", messageType));
			parameters.add(new BasicNameValuePair("Payload", IOUtils.readFully(resource.getInputStream())));
			httpMethod.setEntity(new UrlEncodedFormEntity(parameters));
			HttpResponse response = httpClient.execute(httpMethod);

			final int returnCode = response.getStatusLine().getStatusCode();
			log.info("RESTTransporter returnCode"+returnCode);
			if(returnCode!=202){
				throw new TransportException(
						"RESTTransporter has invalid returnCode="+returnCode, TransportErrorType.INSTANCE_ERR);
			}
			String resp = readContent(response.getEntity(), Integer.MAX_VALUE);
			ImportResponse importResponse = ImportResponse.fromXML(resp);
			if(!importResponse.isSuccessInd()) {
				throw new TransportException("RESTTransporter has ImportResponse=" + importResponse, TransportErrorType.INSTANCE_ERR);
			}
			log.info("RESTTransporter response:" + importResponse);
			return importResponse.toString();
		}catch(ConvertException e){
			throw new TransportException(
					"ConvertException while converting transport parameters", e, TransportErrorType.INSTANCE_ERR);
		}catch(IOException e){
			throw new TransportException(e.getMessage(), e, TransportErrorType.TRANSPORT_ERR);
		}
	}
}
