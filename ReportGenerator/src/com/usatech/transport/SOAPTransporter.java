package com.usatech.transport;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.resource.Resource;

public class SOAPTransporter implements Transporter
{
    protected Map<String,Transporter> transporters = new HashMap<String, Transporter>();
    protected Properties props;

    public SOAPTransporter(Properties props) {
        super();
        this.props = props;
    }

    public String transport(Map<String, ? extends Object> transportProperties, TransportReason transportReason, Resource resource, Map<String, ? extends Object> messageParameters) throws TransportException
    {
        String endpointURL;
        String encodedURL;
        try{
        	endpointURL= ConvertUtils.getString(transportProperties.get("URL"), false);
        }catch(ConvertException e){
			throw new TransportException(
					"ConvertException while converting endpointURL", e, TransportErrorType.INSTANCE_ERR);
		}

        if (endpointURL == null)
            throw new TransportException("URL transport property is null", TransportErrorType.INSTANCE_ERR);


        try {
            encodedURL = URLEncoder.encode(endpointURL, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new TransportException("UnsupportedEncodingException while URL-encoding URL " + endpointURL, e, TransportErrorType.PROGRAM_ERR);
        }

        String transporterClassName = null;
        try{
        	transporterClassName=props.getProperty(endpointURL.substring(endpointURL.lastIndexOf('/')+1));
        }catch(Exception e){
        	throw new TransportException(
					"Exception while getting the last part of endpointURL. This should not happen.", e, TransportErrorType.INSTANCE_ERR);
        }
        if (transporterClassName == null)
        {
            transporterClassName = props.getProperty("DefaultSOAPTransporter");
            if (transporterClassName == null)
                throw new TransportException("No SOAP transporter class property is configured for URL " + encodedURL, TransportErrorType.PROGRAM_ERR);
        }

        Transporter transporter = transporters.get(transporterClassName);
        if (transporter == null) {
            Class<?> clazz;
            try {
                clazz = Class.forName(transporterClassName);
            } catch(ClassNotFoundException e) {
                throw new TransportException("SOAP transporter class name '" + transporterClassName + "' could not be found", e, TransportErrorType.PROGRAM_ERR);
            }

            try {
                transporter = (Transporter)ReflectionUtils.createObject(clazz);
            } catch (InstantiationException e) {
                throw new TransportException("SOAP transporter object cannot be instantiated from class " + transporterClassName, e, TransportErrorType.PROGRAM_ERR);
            } catch (Exception e) {
                throw new TransportException("Exception while instantiating SOAP transporter object from class " + transporterClassName, e, TransportErrorType.PROGRAM_ERR);
            }

            transporters.put(transporterClassName, transporter);
        }

        return transporter.transport(transportProperties, transportReason, resource, messageParameters);
    }
}
