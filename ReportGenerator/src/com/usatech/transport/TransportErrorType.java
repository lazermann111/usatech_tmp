package com.usatech.transport;
/**
 * Instance error are not related to transport. They are errors caused by a specific report parameters. We need to notify the developer.
 * For transport error, we need not only notify the developer but also disable the report until it is being resolved.
 * Program error should not happen unless we haven't configured the report generating process properly. We need to notify the developer.
 * @author yhe
 *
 */
public enum TransportErrorType {
	INSTANCE_ERR, TRANSPORT_ERR, PROGRAM_ERR, PARTIAL_SUCCESS
}
