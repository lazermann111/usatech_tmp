package com.usatech.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Map;
import java.util.zip.ZipEntry;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.TransportAttrEnum;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.io.ZippingInputStream;
import simple.io.resource.ByteArrayResource;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.text.StringUtils;
import simple.util.FilterMap;

public class TransportTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected Transporter transporter;
	protected Transporter alternateTransporter = null;
	protected boolean allowTransportErrors = false;
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		Map<String, Object> resultAttributes = taskInfo.getStep().getResultAttributes();
		TransportReason transportReason=null;
		try{
			
			Resource dataSource=null;
			try {
				transportReason=ConvertUtils.convert(TransportReason.class, attributes.get("transport.transportReason"));
				if(transportReason.equals(TransportReason.TEST)){
					dataSource =new ByteArrayResource("This is a test for the transport.".getBytes(), "testTransportKey", "testTransportFileName");
				}else{
					dataSource=resourceFolder.getResource(ConvertUtils.getStringSafely(attributes.get("file.fileKey")),ResourceMode.READ);
					String name = ConvertUtils.getStringSafely(attributes.get("file.fileName"));
					if(!StringUtils.isBlank(name))
						dataSource.setName(name);
					String contentType = ConvertUtils.getStringSafely(attributes.get("file.fileContentType"));
					if(!StringUtils.isBlank(contentType))
						dataSource.setContentType(contentType);
					if(dataSource.exists()==false){
						throw new TransportException("TransportReport: file does not exists with key:"+attributes.get("file.fileKey"),TransportErrorType.INSTANCE_ERR);
					}
				}
				String details = transport(dataSource, attributes);
				resultAttributes.put("sentDate", new Date().toString());
				resultAttributes.put(TransportAttrEnum.ATTR_SENT_DETAILS.getValue(), details);
				resultAttributes.put(TransportAttrEnum.ATTR_SENT_SUCCESS.getValue(), true);
			}catch(ConvertException e){
				throw new TransportException(e.getMessage(), e, TransportErrorType.INSTANCE_ERR);
			}catch(IOException e){
				throw new TransportException(e.getMessage(), e, TransportErrorType.INSTANCE_ERR);
			} finally {
				if(dataSource!=null){
					dataSource.release();
				}
			}
			return 0;
		} catch(TransportException e){
			int maxRetryAllowed=ConvertUtils.getIntSafely(attributes.get(TransportAttrEnum.ATTR_TRANSPORT_MAX_RETRY_ALLOWED.getValue()), 5);
			if(maxRetryAllowed>0){
				if(taskInfo.getRetryCount() >= maxRetryAllowed|| e.errorType.equals(TransportErrorType.PARTIAL_SUCCESS)){
					resultAttributes.put("error.errorText", StringUtils.exceptionToString(e));
					resultAttributes.put("error.errorTs", new Date().toString());
					resultAttributes.put("error.errorTypeId", e.getErrorType().ordinal());
					resultAttributes.put("error.retryProps", e.getRetryProps());
					return 1;
				} else{
					throw new ServiceException(e);
				}
			}else{
				// This is for transport TEST case
				resultAttributes.put(TransportAttrEnum.ATTR_SENT_SUCCESS.getValue(), false);
				Throwable rootCause=ExceptionUtils.getRootCause(e);
				if(rootCause==null){
					rootCause=e;
				}
				String msg=rootCause.getMessage();
				if(msg==null){
					msg="";
				}else{
					msg=msg.trim().replaceAll("[\n\r\t]", " ");
				}
				resultAttributes.put(TransportAttrEnum.ATTR_SENT_DETAILS.getValue(), rootCause.getClass().getSimpleName()+":"+msg);
				return 0;
			}

		}
	}

	protected String transport(Resource dataSource, Map<String, Object> attributes) throws TransportException, ConvertException{
		TransportReason transportReason=ConvertUtils.convert(TransportReason.class, attributes.get("transport.transportReason"));
		Map<String, Object> transportParams=new FilterMap<Object>(attributes, "transport.", null);
		long minBytesToSend = ConvertUtils.getLongSafely(transportParams.get("MIN BYTES TO SEND"), 0);
		if (dataSource.getLength() < minBytesToSend)
			return new StringBuilder("Not Sent: File size ").append(dataSource.getLength()).append(" is less than ").append(minBytesToSend).append(" bytes").toString();		
		String transportTypeName = ConvertUtils.getStringSafely(transportParams.get("TRANSPORT_TYPE_NAME"), "");
		long maxFileLength = ConvertUtils.getLongSafely(transportParams.get("MAX_FILE_LENGTH"), 0);
		long fileLength = dataSource.getLength();
		boolean useAlternateTransporter = false;
		if (maxFileLength > 0 && fileLength > maxFileLength) {
			if (alternateTransporter != null) {
				useAlternateTransporter = true;
				log.info(String.format("File length %s is greater than max file length %s bytes, using alternate transporter %s", fileLength, maxFileLength, alternateTransporter.getClass().getSimpleName()));
			} else
				return new StringBuilder("Not Sent: File size ").append(fileLength).append(" exceeds max file size ").append(maxFileLength).append(" bytes for ").append(transportTypeName).toString();
		}
		parseRetryProps(transportParams);
		Map<String, Object> messageParams=new FilterMap<Object>(attributes,"message.", null);
		if (log.isDebugEnabled())
			log.debug("Sending report via Transport Type #" + transporter + " with properties: " + transportParams);
        if(!transportReason.equals(TransportReason.TEST)&&ConvertUtils.getBoolean(transportParams.get("AS ZIP FILE"), false)){
        	dataSource=new ZippedResource(dataSource);
        }
        if (useAlternateTransporter)
        	return alternateTransporter.transport(transportParams, transportReason, dataSource, messageParams);
        else
        	return transporter.transport(transportParams, transportReason, dataSource, messageParams);
    }
	/**
	 * retryProps is in the format of key1=value1;key2=value2; format
	 */
	protected void parseRetryProps(Map<String, Object> transportParams){
		Object retryProps=transportParams.get("retryProps");
		if(retryProps!=null&&!retryProps.toString().equals("")){
			String[] keyValuePairs=retryProps.toString().split(";");
			for(String keyValue:keyValuePairs){
				String[] pair=keyValue.split("=");
				transportParams.put("retryProps."+pair[0], pair[1]);
			}
		}
	}
	
	

	/**
	 * Class to facilitate transforming a datasource to have a ZippingInputStream
	 * @author yhe
	 *
	 */
	protected class ZippedResource implements Resource {
		protected Resource delegate;
		public ZippedResource(Resource delegate){
			this.delegate = delegate;
		}
		public String getContentType() {
			return delegate.getContentType();
		}

		public InputStream getInputStream() throws IOException {
			return new ZippingInputStream(new ZipEntry(delegate.getName()), delegate.getInputStream());
		}

		public String getName() {
			return delegate.getName()+".zip";
		}

		public OutputStream getOutputStream() throws IOException {
			return delegate.getOutputStream();
		}
		public boolean delete() {
			return delegate.delete();
		}
		public boolean exists() {
			return delegate.exists();
		}
		public String getKey() {
			return delegate.getKey();
		}
		public long getLength() {
			return delegate.getLength();
		}
		public String getPath() {
			return delegate.getPath();
		}
		public boolean isReadable() {
			return delegate.isReadable();
		}
		public boolean isWritable() {
			return delegate.isWritable();
		}
		public void release() {
			delegate.release();
		}
		public void setContentType(String contentType) {
			delegate.setContentType(contentType);
		}
		public void setName(String fileName) {
			delegate.setName(fileName);
		}
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}
	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public boolean isAllowTransportErrors() {
		return allowTransportErrors;
	}

	public void setAllowTransportErrors(boolean allowTransportErrors) {
		this.allowTransportErrors = allowTransportErrors;
	}

	public Transporter getTransporter() {
		return transporter;
	}

	public void setTransporter(Transporter transporter) {
		this.transporter = transporter;
	}

	public Transporter getAlternateTransporter() {
		return alternateTransporter;
	}

	public void setAlternateTransporter(Transporter alternateTransporter) {
		this.alternateTransporter = alternateTransporter;
	}
}

