package com.usatech.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

import org.apache.http.HttpEntity;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

import simple.io.Log;
import simple.io.resource.Resource;
import simple.text.StringUtils;

/**
 * POST the file to the given url + the filename.
 * 
 * @author bkrug
 * 
 */
public abstract class AbstractHttpTransporter implements Transporter {
	private static final Log log = Log.getLog();
	protected final PoolingHttpClientConnectionManager httpConnectionManager;
	protected final HttpClient httpClient;
	protected int connectionTimeout;
	protected int socketTimeout;
	protected int retryCount = 3;

	public AbstractHttpTransporter() {
		this(null);
	}

	public AbstractHttpTransporter(String[] sslProtocols) {
		super();
		this.httpConnectionManager = new PoolingHttpClientConnectionManager(RegistryBuilder.<ConnectionSocketFactory> create().register("http", PlainConnectionSocketFactory.getSocketFactory()).register("https", new SSLConnectionSocketFactory((javax.net.ssl.SSLSocketFactory) javax.net.ssl.SSLSocketFactory.getDefault(), sslProtocols, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier()) {
			@Override
			public Socket createLayeredSocket(Socket socket, String target, int port, HttpContext context) throws IOException {
				Socket s = super.createLayeredSocket(socket, target, port, context);
				socketCreated(s);
				return s;
			}
		}).build());

		HttpClientBuilder builder = HttpClients.custom().setConnectionManager(httpConnectionManager);
		builder.setRetryHandler(new HttpRequestRetryHandler() {
			@Override
			public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
				if (executionCount > retryCount) {
					log.warn("Maximum attempts reached for client http pool");
					return false;
				}
				if (exception instanceof NoHttpResponseException) {
					log.warn(new StringBuilder("No response from server on ").append(executionCount).append(" attempt").toString());
					return true;
				}
				return DefaultHttpRequestRetryHandler.INSTANCE.retryRequest(exception, executionCount, context);
			}
		});
		this.httpClient = builder.build();
	}

	protected void socketCreated(Socket socket) {
		if(socket instanceof SSLSocket && log.isInfoEnabled()) {
			SSLSession ss = ((SSLSocket) socket).getSession();
			log.info(new StringBuilder().append("Created SSL Socket to ").append(socket.getInetAddress()).append(':').append(socket.getPort()).append(" using ").append(ss.getProtocol()).append(" with ").append(ss.getCipherSuite()));
		}
	}

	protected static class ResourceEntity extends AbstractHttpEntity {
		protected final Resource resource;

		public ResourceEntity(Resource resource) {
			super();
			this.resource = resource;
			if(!StringUtils.isBlank(resource.getContentType()))
				setContentType(resource.getContentType());
		}

		@Override
		public boolean isRepeatable() {
			return true;
		}

		@Override
		public long getContentLength() {
			return resource.getLength();
		}

		@Override
		public InputStream getContent() throws IOException, UnsupportedOperationException {
			return resource.getInputStream();
		}

		@Override
		public void writeTo(OutputStream outstream) throws IOException {
			final InputStream instream = getContent();
			try {
				final byte[] tmp = new byte[OUTPUT_BUFFER_SIZE];
				int l;
				while((l = instream.read(tmp)) != -1) {
					outstream.write(tmp, 0, l);
				}
				outstream.flush();
			} finally {
				instream.close();
			}
		}

		@Override
		public boolean isStreaming() {
			return false;
		}
	}
	protected String readContent(HttpEntity httpEntity, int maxSize) throws IOException {
		if(httpEntity == null)
			return "";
		int len;
		if(httpEntity.getContentLength() > maxSize || httpEntity.getContentLength() < 0)
			len = maxSize;
		else
			len = (int) httpEntity.getContentLength();
		byte[] content = new byte[len];
		InputStream in;
		int off=0;
		try {
			in = httpEntity.getContent();
		} catch(UnsupportedOperationException e) {
			return "";
		}
		try {
			int b;
			while(off<len&&(b = in.read(content,off,len-off))>-1){
				off+=b;
            }
		} finally {
			in.close();
		}
		Charset charset;
		try {
			final ContentType contentType = ContentType.get(httpEntity);
			if(contentType != null)
				charset = contentType.getCharset();
			else
				charset = HTTP.DEF_CONTENT_CHARSET;
		} catch(final UnsupportedCharsetException ex) {
			charset = HTTP.DEF_CONTENT_CHARSET;
		}
		if(charset==null){
			charset = HTTP.DEF_CONTENT_CHARSET;
		}
		return new String(content, 0, off, charset);
	}

	protected void updateRequestSettings(HttpRequestBase request) {
		RequestConfig.Builder builder;
		if(request.getConfig() == null)
			builder = RequestConfig.custom();
		else
			builder = RequestConfig.copy(request.getConfig());
		builder.setConnectTimeout(getConnectionTimeout());
		builder.setSocketTimeout(getSocketTimeout());
		request.setConfig(builder.build());
	}

	/*
	public int getSocketTimeout() {
		return httpConnectionManager.getDefaultSocketConfig().getSoTimeout();
	}

	public void setSocketTimeout(int socketTimeout) {
		SocketConfig.Builder builder;
		if(httpConnectionManager.getDefaultSocketConfig() == null)
			builder = SocketConfig.custom();
		else
			builder = SocketConfig.copy(httpConnectionManager.getDefaultSocketConfig());
		httpConnectionManager.setDefaultSocketConfig(builder.setSoTimeout(socketTimeout).build());
	}
	*/
	public int getMaxTotalConnections() {
		return httpConnectionManager.getMaxTotal();
	}

	public void setMaxTotalConnections(int maxTotalConnections) {
		httpConnectionManager.setMaxTotal(maxTotalConnections);
	}

	public int getMaxHostConnections() {
		return httpConnectionManager.getDefaultMaxPerRoute();
	}

	public void setMaxHostConnections(int maxHostConnections) {
		httpConnectionManager.setDefaultMaxPerRoute(maxHostConnections);
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

}
