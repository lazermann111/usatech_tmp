package com.usatech.transport;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeoutException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.lang.SystemUtils;
import simple.security.crypt.CryptUtils;
import simple.util.concurrent.Cabinent;
import simple.util.concurrent.CreationException;
import com.trilead.ssh2.Connection;
import com.trilead.ssh2.InteractiveCallback;
import com.trilead.ssh2.SFTPException;
import com.trilead.ssh2.SFTPv3Client;
import com.trilead.ssh2.SFTPv3DirectoryEntry;
import com.trilead.ssh2.SFTPv3FileHandle;
import com.trilead.ssh2.ServerHostKeyVerifier;
import com.trilead.ssh2.sftp.ErrorCodes;

/**
 * @author Brian S. Krug
 *
 */
public class SecureFTPGanymedTransporter implements Transporter {
	private static final Log log = Log.getLog();
	protected static final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

	protected ServerHostKeyVerifier hostKeyVerification;
	protected int bufferSize = 512;
	protected int connectTimeout = 3000;
	//setting kexTimeout to 10000 caused "java.net.SocketTimeoutException: The kexTimeout (10000 ms) expired." in the middle of file transfer
	protected int kexTimeout = 0;
	protected final ThreadLocal<byte[]> byteBufferLocal = new ThreadLocal<byte[]>() {
		@Override
		protected byte[] initialValue() {
			return new byte[bufferSize];
		}
	};

	protected class SftpKey {
		protected final String host;
		protected final int port;
		protected final String username;
		protected final String password;
		protected final String passphrase, privateKey, dsaPrivateKey, rsaPrivateKey;
		

		public SftpKey(String host, int port, String username, String password, String passphrase, String privateKey,String dsaPrivateKey, String rsaPrivateKey) {
			super();
			this.host = host;
			this.port = port;
			this.username = username;
			this.password = password;
			this.privateKey = privateKey;
			this.dsaPrivateKey = dsaPrivateKey;
			this.rsaPrivateKey = rsaPrivateKey;
			this.passphrase = passphrase;
		}

		public String getHost() {
			return host;
		}

		public int getPort() {
			return port;
		}

		public String getUsername() {
			return username;
		}

		public String getPassword() {
			return password;
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof SftpKey))
				return false;
			SftpKey k = (SftpKey) obj;
			return ConvertUtils.areEqual(host, k.host) && port == k.port && ConvertUtils.areEqual(username, k.username) && ConvertUtils.areEqual(password, k.password)
					&& ConvertUtils.areEqual(privateKey, k.privateKey)
					&& ConvertUtils.areEqual(dsaPrivateKey, k.dsaPrivateKey)
					&& ConvertUtils.areEqual(rsaPrivateKey, k.rsaPrivateKey)
					&& ConvertUtils.areEqual(passphrase, k.passphrase);
		}

		@Override
		public int hashCode() {
			return SystemUtils.addHashCodes(port, host, username);
		}

		public String getPassphrase() {
			return passphrase;
		}

		public String getPrivateKey() {
			return privateKey;
		}

		public String getDsaPrivateKey() {
			return dsaPrivateKey;
		}

		public String getRsaPrivateKey() {
			return rsaPrivateKey;
		}

		@Override
		public String toString() {
			return getUsername() + '@' + getHost() + ':' + getPort();
		}
	}

	protected final Cabinent<SftpKey, SFTPv3Client> cabinent = new Cabinent<SftpKey, SFTPv3Client>() {
		protected SFTPv3Client createItem(SftpKey key) throws Exception {
			return createAndConnect(key);
		}

		protected void configureItem(SftpKey key, SFTPv3Client item, boolean initial) throws Exception {
		}

		protected void closeItem(SFTPv3Client item) throws Exception {
			item.close();
		}
	};

	public SecureFTPGanymedTransporter(Properties props) throws IOException {
		super();
		init(props);
		cabinent.schedulePurgeJob(executor, 600000);
	}

	public void init(Properties props) throws IOException {
		hostKeyVerification = new KnownHostsVerifier(new File(props.getProperty("knownhosts.file")));
		connectTimeout = Integer.parseInt(props.getProperty("connectTimeout"));
		kexTimeout = Integer.parseInt(props.getProperty("kexTimeout"));
	}

	public String transport(Map<String, ? extends Object> transportProperties, TransportReason transportReason, Resource resource, Map<String, ? extends Object> messageParameters) throws TransportException {
		final String host, username, password, remoteDir, privateKey, passphrase, dsaPrivateKey, rsaPrivateKey;
		String tmpPrefix, tmpSuffix;
		int port;
		try {
			host= ConvertUtils.getString(transportProperties.get("HOST"), false);
	        port = ConvertUtils.getInt(transportProperties.get("PORT"));
	        username = ConvertUtils.getString(transportProperties.get("USERNAME"), false);
	        password = ConvertUtils.getString(transportProperties.get("PASSWORD"), false);
	        remoteDir = ConvertUtils.getString(transportProperties.get("REMOTE PATH"), false);
	        privateKey = ConvertUtils.getString(transportProperties.get("PRIVATE KEY"), false);
	        passphrase = ConvertUtils.getString(transportProperties.get("PRIVATE KEY PASSPHRASE"), false);
	        dsaPrivateKey = ConvertUtils.getString(transportProperties.get("DSA PRIVATE KEY"), false);
	        rsaPrivateKey = ConvertUtils.getString(transportProperties.get("RSA PRIVATE KEY"), false);
	        tmpPrefix = ConvertUtils.getString(transportProperties.get("TMP PREFIX"), false);
	        tmpSuffix = ConvertUtils.getString(transportProperties.get("TMP SUFFIX"), false);
		} catch(ConvertException e){
			throw new TransportException(
					"ConvertException while converting transport parameters", e, TransportErrorType.INSTANCE_ERR);
		}
		if(host != null) {
			SftpKey key = createKey(host, port, username, password, passphrase, privateKey, dsaPrivateKey, rsaPrivateKey);
			SFTPv3Client sftp;
			try {
				sftp = getSFTPv3Client(key);
			} catch(IOException e) {
				throw new TransportException(e.getMessage(), e, TransportErrorType.TRANSPORT_ERR);
			}
			try {
				// Change directory
				String dirPrefix;
				if(remoteDir != null && remoteDir.trim().length() > 0)
					try {
						dirPrefix = sftp.canonicalPath(remoteDir) + "/";
					} catch(SFTPException e) {
						StringBuilder sb = new StringBuilder();
						sb.append("Could not get canonical path for '").append(remoteDir).append("' because ").append(e.getMessage()).append(". Current Directory list:\n");
						Vector<?> list = sftp.ls(".");
						for(Object o : list) {
							if(o instanceof SFTPv3DirectoryEntry) {
								sb.append(((SFTPv3DirectoryEntry) o).longEntry).append('\n');
							} else {
								sb.append(o).append('\n');
							}
						}
						log.warn(sb.toString());
						throw e;
					}
				else
					dirPrefix = "";

				// Upload a file
				boolean rename = false;
				StringBuilder sb = new StringBuilder();
				if(tmpPrefix != null && (tmpPrefix = tmpPrefix.trim()).length() > 0) {
					sb.append(tmpPrefix);
					rename = true;
				}
				sb.append(resource.getName());
				if(tmpSuffix != null && (tmpSuffix = tmpSuffix.trim()).length() > 0) {
					sb.append(tmpSuffix);
					rename = true;
				}
				String tmpName = sb.toString();
				SFTPv3FileHandle handle = sftp.createFileTruncate(dirPrefix + tmpName);
				try {
					InputStream in = resource.getInputStream();
                    try {
						byte[] b = byteBufferLocal.get();
						long tot = 0;
						for(int len = 0; (len = in.read(b)) >= 0; tot += len)
							sftp.write(handle, tot, b, 0, len);
						log.info("Wrote " + tot + " bytes to " + dirPrefix + tmpName);
					} finally {
						in.close();
                    }
				} finally {
					sftp.closeFile(handle);
				}
				if(rename) {
					try {
						sftp.rm(dirPrefix + resource.getName());
					} catch(SFTPException e) {
						switch(e.getServerErrorCode()) {
							case ErrorCodes.SSH_FX_NO_SUCH_FILE:
							case ErrorCodes.SSH_FX_NO_SUCH_PATH:
								// Ignore
								break;
							default:
								log.warn("Could not delete file '" + dirPrefix + resource.getName() + "' that is being replaced", e);
						}
					}
					sftp.mv(dirPrefix + tmpName, dirPrefix + resource.getName());
				}
			} catch(IOException e) {
				cabinent.invalidateItem(key, sftp);
				log.warn("While sftping file", e);
				throw new TransportException(e.getMessage(), e, TransportErrorType.TRANSPORT_ERR);
            } finally {
				cabinent.returnItem(key, sftp);
            }
            return "sftp: " + username + "@" + host + ":" + port +
                (remoteDir != null && remoteDir.trim().length() > 0 ? "/" + remoteDir : "") + "/" + resource.getName();
		}
		return "NOT SENT";
	}

	protected SFTPv3Client getSFTPv3Client(SftpKey key) throws IOException {
		try {
			return cabinent.getItem(key);
		} catch(TimeoutException | InterruptedException e) {
			throw new IOException(e);
		} catch(CreationException e) {
			if(e.getCause() instanceof IOException)
				throw (IOException) e.getCause();
			throw new IOException(e);
		}
	}

	protected SftpKey createKey(String host, int port, String username, String password, String passphrase, String privateKey, String dsaPrivateKey, String rsaPrivateKey) {
		return new SftpKey(host, port, username, password, passphrase, privateKey, dsaPrivateKey, rsaPrivateKey);
	}

	protected SFTPv3Client createAndConnect(final SftpKey key) throws IOException {
		boolean okay = false;
		final Connection connection = new Connection(key.getHost(), key.getPort());
		try {
			// Connect to the host
			long connectionStart = System.currentTimeMillis();
	        connection.connect(hostKeyVerification, connectTimeout, kexTimeout);
	        log.info("Connection to " + key.getHost() + ":" + key.getPort() + " took " + (System.currentTimeMillis() - connectionStart) + " ms");
	        // authenticate
	        boolean authenticated = false;
	        StringBuilder methods = new StringBuilder();
			if(key.getPrivateKey() != null) {
				authenticated = connection.authenticateWithPublicKey(key.getUsername(), key.getPrivateKey().toCharArray(), key.getPassphrase());
	        	methods.append("private key, ");
	        }
			if(!authenticated && key.getDsaPrivateKey() != null) {
	        	CharArrayWriter appendTo = new CharArrayWriter();
				CryptUtils.writePEMKey("DSA PRIVATE KEY", key.getDsaPrivateKey().getBytes(), appendTo);
				authenticated = connection.authenticateWithPublicKey(key.getUsername(), appendTo.toCharArray(), key.getPassphrase());
	        	methods.append("dsa private key, ");
	        }
			if(!authenticated && key.getRsaPrivateKey() != null) {
	        	CharArrayWriter appendTo = new CharArrayWriter();
				CryptUtils.writePEMKey("RSA PRIVATE KEY", key.getRsaPrivateKey().getBytes(), appendTo);
				authenticated = connection.authenticateWithPublicKey(key.getUsername(), appendTo.toCharArray(), key.getPassphrase());
	        	methods.append("rsa private key, ");
	        }
			if(!authenticated && key.getPassword() != null) {
				if(connection.isAuthMethodAvailable(key.getUsername(), "password")) {
					authenticated = connection.authenticateWithPassword(key.getUsername(), key.getPassword());
					methods.append("password, ");
				} else if(connection.isAuthMethodAvailable(key.getUsername(), "keyboard-interactive")) {
					authenticated = connection.authenticateWithKeyboardInteractive(key.getUsername(), new InteractiveCallback() {
						@Override
						public String[] replyToChallenge(String name, String instruction, int numPrompts, String[] prompt, boolean[] echo) throws Exception {
							switch(numPrompts) {
								case 1:
									return new String[] { key.getPassword() };
								case 0:
									return new String[0];
								default:
									return new String[numPrompts];
							}

						}
					});
					methods.append("keyboard-interactive, ");
				}
	        }
			if(!authenticated)
				throw new IOException("Could not log into " + key.getHost() + ':' + key.getPort() + " using " + methods.substring(0, methods.length() - 2));

	        // The connection is authenticated we can now do some real work!           
			SFTPv3Client sftp = new SFTPv3Client(connection) {
				@Override
				public void close() {
					super.close();
					connection.close();
				}
			};
			okay = true;
			if(log.isInfoEnabled())
				log.info("Created new SFTP Client for " + key.getUsername() + '@' + key.getHost() + ':' + key.getPort());
			return sftp;
		} finally {
			if(!okay)
				connection.close();
		}
	}
	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		if(bufferSize < 1)
			throw new IllegalArgumentException("BufferSize must be positive");
		this.bufferSize = bufferSize;
	}

	public long getMaxWait() {
		return cabinent.getMaxWait();
	}

	public void setMaxWait(long maxWait) {
		cabinent.setMaxWait(maxWait);
	}

	public int getMaxIdle() {
		return cabinent.getMaxIdle();
	}

	public void setMaxIdle(int maxIdle) {
		cabinent.setMaxIdle(maxIdle);
	}

	public int getMaxActive() {
		return cabinent.getMaxActive();
	}

	public void setMaxActive(int maxActive) {
		cabinent.setMaxActive(maxActive);
	}

	public int getMaxKeys() {
		return cabinent.getMaxKeys();
	}

	public void setMaxKeys(int maxKeys) {
		cabinent.setMaxKeys(maxKeys);
	}

	public long getMaxInactivity() {
		return cabinent.getMaxInactivity();
	}

	public void setMaxInactivity(long maxInactivity) {
		cabinent.setMaxInactivity(maxInactivity);
	}
}
