package com.usatech.transport;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;

import com.trilead.ssh2.KnownHosts;
import com.trilead.ssh2.ServerHostKeyVerifier;
import com.trilead.ssh2.crypto.Base64;
import com.trilead.ssh2.util.StringEncoder;

public class KnownHostsVerifier implements ServerHostKeyVerifier {
	protected final File knownHostsFile;
	protected final KnownHosts knownHosts;
	
	public KnownHostsVerifier(File knownHostsFile) throws IOException {
		this.knownHostsFile = knownHostsFile;
		this.knownHosts = new KnownHosts(knownHostsFile);
	}
	
	public boolean verifyServerHostKey(String hostname, int port, String serverHostKeyAlgorithm, byte[] serverHostKey) throws IOException {
		int result = knownHosts.verifyHostkey(hostname, serverHostKeyAlgorithm, serverHostKey);
		switch(result) {
			case KnownHosts.HOSTKEY_IS_OK:
				return true; // We are happy
			case KnownHosts.HOSTKEY_IS_NEW:
				// Unknown host? Blindly accept the key and put it into the cache.
				String[] hostnames = new String[] { hostname };
				//KnownHosts.addHostkeyToFile(knownHostsFile, hostnames, serverHostKeyAlgorithm, serverHostKey); // There is no locking in this so do it ourselves
				RandomAccessFile raf = new RandomAccessFile(knownHostsFile, "rw");
				try {
					FileLock fileLock = raf.getChannel().lock();
					try {
						long len = raf.length();							
						if (len > 0) {
							raf.seek(len - 1);
							int last = raf.read();
							if (last != '\n')
								raf.write('\n');
						}
						raf.write(StringEncoder.GetBytes(hostname));
					} finally {
						fileLock.release();
					}
					raf.write(' ');
					raf.write(StringEncoder.GetBytes(serverHostKeyAlgorithm));
					raf.write(' ');
					raf.write(StringEncoder.GetBytes(new String(Base64.encode(serverHostKey))));
					raf.write('\n');
				} finally {
					raf.close();
				}
				knownHosts.addHostkey(hostnames, serverHostKeyAlgorithm, serverHostKey);
				return true;
			case KnownHosts.HOSTKEY_HAS_CHANGED:
				// Close the connection if the hostkey has changed.
				return false;
			default:
				throw new IllegalStateException();
		}
	}
}
