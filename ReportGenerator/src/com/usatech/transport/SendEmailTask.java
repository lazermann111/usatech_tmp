package com.usatech.transport;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.MailAttributeEnum;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.mail.EmailInfo;
import simple.text.StringUtils;

/**
 * Use a task to send email so it does not block other services.
 * 
 * @author bkrug
 * 
 */
public class SendEmailTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected static final Pattern HTML_PATTERN = Pattern.compile("\\s*(?:<!DOCTYPE\\s[^>]+>)?\\s*<html[\\s>]", Pattern.CASE_INSENSITIVE);

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		String failed;
		String to;
		String cc;
		String bcc;
		String contentType;
		try {
			failed = step.getAttribute(MailAttributeEnum.FAILED, String.class, false);
			to = step.getAttribute(MailAttributeEnum.TO, String.class, false);
			cc = step.getAttribute(MailAttributeEnum.CC, String.class, false);
			bcc = step.getAttribute(MailAttributeEnum.BCC, String.class, false);
			contentType = step.getAttribute(MailAttributeEnum.BODY_CONTENT_TYPE, String.class, false);
		} catch(AttributeConversionException e) {
			log.info("Could not convert attributes", e);
			step.setResultAttribute(CommonAttrEnum.ATTR_ERROR, SystemUtils.getRootCause(e).getMessage());
			return 1;
		}
		Set<String> recipients = EmailUtils.parseRecipients(to, cc, bcc, failed);
		if(!recipients.isEmpty()) {
			EmailInfo emailInfo = new EmailInfo();
			emailInfo.setTo(to);
			emailInfo.setCc(cc);
			Object body;
			try {
				emailInfo.setFrom(step.getAttribute(MailAttributeEnum.FROM, String.class, false));
				emailInfo.setSubject(step.getAttribute(MailAttributeEnum.SUBJECT, String.class, false));
				body = step.getAttribute(MailAttributeEnum.BODY, Object.class, false);
			} catch(AttributeConversionException e) {
				log.info("Could not convert attributes", e);
				step.setResultAttribute(CommonAttrEnum.ATTR_ERROR, SystemUtils.getRootCause(e).getMessage());
				return 1;
			}
			if(!StringUtils.isBlank(contentType))
				emailInfo.setContentType(contentType);
			else if(!(body instanceof Multipart) && !(body instanceof DataSource)) {
				String bodyText = ConvertUtils.getStringSafely(body);
				if(!StringUtils.isBlank(bodyText) && HTML_PATTERN.matcher(bodyText).lookingAt())
					emailInfo.setContentType("text/html");
			}
			emailInfo.setBody(body);
			Map<InternetAddress, MessagingException> failingRecipients = new LinkedHashMap<InternetAddress, MessagingException>();
			int sent;
			try {
				sent = EmailUtils.sendEmails(emailInfo, recipients, failingRecipients);
			} catch(TransportException e) {
				log.info("Could not send emails", e);
				step.setResultAttribute(CommonAttrEnum.ATTR_ERROR, SystemUtils.getRootCause(e).getMessage());
				return 1;
			}
			if(failingRecipients.size() == 0) {
				log.info("successfully sent " + sent + " emails " + recipients);
				return 0;
			}
			StringBuilder sb = new StringBuilder();
			for(Map.Entry<InternetAddress, MessagingException> entry : failingRecipients.entrySet()) {
				String address = entry.getKey().getAddress();
				recipients.remove(address);
				log.warn("Could not send email to '" + address + "'", entry.getValue());
				if(sb.length() > 0)
					sb.append("; ");
				sb.append(SystemUtils.getRootCause(entry.getValue()).getMessage());
			}
			step.setResultAttribute(CommonAttrEnum.ATTR_ERROR, sb.toString());

			if(sent > 0)
				log.info("Failed to send " + failingRecipients.size() + " emails; but successfully sent " + sent + " emails " + recipients);
			else
				log.info("Failed to send " + failingRecipients.size() + " emails");

			return 1;
		}
		return 0;
	}
}
