package com.usatech.transport;

import java.util.Map;

import simple.io.resource.Resource;



public interface Transporter {
	/** Sends the message using the implemented transport method
	 * @param transportProperties The name-value pairs of properties for
	 * this transporter as defined in the CCS_TRANSPORT_PROPERTY_TYPE table.
	 * These properties are configurable by the user who registers to receive the message.
	 * @param transportReason What caused the message (a batch report, a timed report, an alert, etc)
	 * @param resource The content of the message. This should include the contentType and a name if applicable
	 * @param messageParameters Other message information
	 * @return Details about the destination to which the message is sent. For an email transporter this might be "mailto: youremailaddress@server"
	 * @throws TransportException If the message cannot be sent to the destination for any reason.
	 */
	public String transport(Map<String, ? extends Object> transportProperties, TransportReason transportReason, Resource resource, Map<String, ? extends Object> messageParameters) throws TransportException;
}
