package com.usatech.transport;

import java.util.Map;
import java.util.Properties;

import javax.activation.DataSource;

import simple.bean.ConvertUtils;
import simple.mail.EmailInfo;
import simple.text.StringUtils;
/**
 * This class will send an email to the customer with a link to the generated report for the user to retrieve the report.
 * @author yhe
 *
 */
public class EmailLinkTransporter extends EmailTransporter {
	protected String url;

	public EmailLinkTransporter(Properties props) {
		super(props);
	}

	@Override
	public void init(Properties props) {
		super.init(props);
		this.url = props.getProperty("url");
	}

	@Override
	protected void populateSubjectAndContent(EmailInfo email, TransportReason transportReason, Map<String, ? extends Object> transportProperties, DataSource dataSource) {
		switch(transportReason) {
			case BATCH_REPORT: 
			case CONDITION:
			case SINGLE_TXN: {
				Object param = new Object[] {dataSource.getName()};
    			email.setSubject(ConvertUtils.formatObject(param, batchSubject));
				email.setBody("<p>" + ConvertUtils.formatObject(param, batchBody) + "</p><p>" + ConvertUtils.formatObject(transportProperties, url) + "</p>");
				email.setContentType("text/html");
    			break;
			}
			case TIMED_REPORT: {
				Object param = new Object[] {dataSource.getName()};
    			email.setSubject(ConvertUtils.formatObject(param, timedSubject));
				email.setBody("<p>" + ConvertUtils.formatObject(param, timedBody) + "</p><p>" + ConvertUtils.formatObject(transportProperties, url) + "</p>");
				email.setContentType("text/html");
    			break;
			}
			case CUSTOM_FILE_UPLOAD:
				Object param = new Object[] { dataSource.getName() };
				email.setSubject(ConvertUtils.formatObject(param, customFileUploadSubject));
				email.setBody("<p>" + ConvertUtils.formatObject(param, customFileUploadBody) + "</p><p>" + ConvertUtils.formatObject(transportProperties, url) + "</p>");
				email.setContentType("text/html");
				break;
			case TEST:
				email.setSubject(testSubject);
				email.setBody(testBody);
				if(!StringUtils.isBlank(testContentType))
					email.setContentType(testContentType);
				break;
			case ALERT:
			case BROADCAST:
			default:
				//TODO: need to clarify what this should be doing
				email.setSubject(dataSource.getName());
				email.setBody(dataSource);
		}
	}
}
