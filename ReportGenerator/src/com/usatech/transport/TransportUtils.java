/*
 * Created on May 15, 2007
 */
package com.usatech.transport;

import java.io.IOException;

import javax.activation.DataSource;

import simple.io.IOUtils;

public class TransportUtils {
    public static String getDEXFileData(DataSource dataSource) throws TransportException {
        try {
            return IOUtils.readFully(dataSource.getInputStream());
        } catch(IOException ioe) {
            throw new TransportException("IOException while converting content InputStream to String", ioe, TransportErrorType.INSTANCE_ERR);
        }
    }
}
