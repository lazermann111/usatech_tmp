package com.usatech.transport;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.exception.ExceptionUtils;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.mail.EmailInfo;
import simple.mail.MXRecordMailHost;
import simple.text.StringUtils;

public class EmailTransporter implements Transporter {
	private static final Log log = Log.getLog();
	protected String from;
	protected String batchSubject;
	protected String batchBody;
	protected String batchContentType;
	protected String timedSubject;
	protected String timedBody;
	protected String timedContentType;
	protected String customFileUploadSubject;
	protected String customFileUploadBody;
	protected String customFileUploadContentType;
	protected String testSubject;
	protected String testBody;
	protected String testContentType;
	

	public EmailTransporter(Properties props) {
		super();
		init(props);
	}

	public void init(Properties props) {
		this.from = props.getProperty("from");
		this.batchBody = props.getProperty("batch.body");
		this.batchSubject = props.getProperty("batch.subject");
		this.batchContentType = props.getProperty("batch.contenttype");
		this.timedBody = props.getProperty("timed.body");
		this.timedSubject = props.getProperty("timed.subject");
		this.timedContentType = props.getProperty("timed.contenttype");
		this.customFileUploadBody = props.getProperty("custom_file_upload.body");
		this.customFileUploadSubject = props.getProperty("custom_file_upload.subject");
		this.customFileUploadContentType = props.getProperty("custom_file_upload.contenttype");
		this.testBody = props.getProperty("test.body");
		this.testSubject = props.getProperty("test.subject");
		this.testContentType = props.getProperty("test.contenttype");

	}
	/**
	 * key=hostname value=emailSet
	 * @param to
	 * @param cc
	 * @return
	 */
	public HashMap<String, HashSet<InternetAddress>> getAllRecepient(String to, String cc, String failedEmails) throws TransportException{
		HashMap<String, HashSet<InternetAddress>> allMap=new HashMap<String, HashSet<InternetAddress>>();
		try{
			ArrayList<String> parseList=new ArrayList<String>(2);
			if(to!=null&&!to.trim().equals("")){
				to=to.replace(';', ',');
				parseList.add(to);
			}
			if(cc!=null&&!cc.trim().equals("")){
				cc=cc.replace(';', ',');
				parseList.add(cc);
			}
			for(String sendTo:parseList){
				String[] toEmailArray=sendTo.split(",");
				for(String emailStr:toEmailArray){
					String emailAddress=emailStr.trim();
					if(failedEmails==null||(failedEmails.contains(emailAddress))){
						String hostName=MXRecordMailHost.getHostname(emailAddress);
						if(allMap.containsKey(hostName)){
							allMap.get(hostName).add(new InternetAddress(emailAddress));
						}else{
							HashSet<InternetAddress> emailSet=new HashSet<InternetAddress>();
							emailSet.add(new InternetAddress(emailAddress));
							allMap.put(hostName, emailSet);
						}
					}
				}
			}
		}catch(AddressException e){
			throw new TransportException("Email address list contains invalid items to="+to+" cc="+cc,TransportErrorType.TRANSPORT_ERR);
		}
		return allMap;
	}
	
	public String generateRetryProps(Collection<InternetAddress> emailSet) throws ConvertException{
		StringBuilder sb=new StringBuilder("failedEmails=");
		sb.append(ConvertUtils.convert(String.class, emailSet));
		sb.append(";");
		return sb.toString();
	}

	public String transport(Map<String, ? extends Object> transportProperties,
			TransportReason transportReason, Resource resource,
			Map<String, ? extends Object> messageParameters) throws TransportException {
		String to, cc;
		try {
			to = ConvertUtils.getString(transportProperties.get("EMAIL"), false);
			cc = ConvertUtils.getString(transportProperties.get("CC"), false);
		} catch(ConvertException e) {
			throw new TransportException("ConvertException while converting transport parameters",
					e, TransportErrorType.INSTANCE_ERR);
		}
		if(to != null || cc != null) {
			String failed = ConvertUtils.getStringSafely(transportProperties.get("retryProps.failedEmails"));
			EmailInfo emailInfo = new EmailInfo();
			emailInfo.setTo(to);
			emailInfo.setCc(cc);
			emailInfo.setFrom(from);
			try {
				populateSubjectAndContent(emailInfo, transportReason, transportProperties, resource);
			} catch(MessagingException e) {
				throw new TransportException("Could not add attachment to email", e, TransportErrorType.PROGRAM_ERR);
			}
			Map<InternetAddress, MessagingException> failingRecipients = new LinkedHashMap<InternetAddress, MessagingException>();
			int sent = EmailUtils.sendEmails(emailInfo, EmailUtils.parseRecipients(to, cc, failed), failingRecipients);
			log.info("Sent report to " + sent + " emails");
			if(failingRecipients.size() == 0) {
				log.debug("Sent report to " + to);
				return "mailto: " + to + (cc != null && cc.trim().length() > 0 ? "; cc: " + cc : "");
			}
			TransportErrorType errorType = null;
			MessagingException exception = null;
			Iterator<MessagingException> iter = failingRecipients.values().iterator();
			while(iter.hasNext()) {
				exception = iter.next();
				Throwable rootCause = ExceptionUtils.getRootCause(exception);
				if(rootCause instanceof InterruptedException || rootCause instanceof TimeoutException || rootCause instanceof SQLException) {
					errorType = TransportErrorType.INSTANCE_ERR;
					break;
				}
				errorType = TransportErrorType.PARTIAL_SUCCESS;
			}
			TransportException transportException = new TransportException(exception.getMessage(), exception, errorType);
			try {
				transportException.setRetryProps(generateRetryProps(failingRecipients.keySet()));
			} catch(ConvertException e) {
				transportException = new TransportException(e.getMessage(), e, TransportErrorType.INSTANCE_ERR);
			}
			throw transportException;
		}
		return "NOT SENT";
	}

	protected void populateSubjectAndContent(EmailInfo email, TransportReason transportReason, Map<String, ? extends Object> transportProperties, DataSource dataSource) throws MessagingException {
		switch(transportReason) {
			case BATCH_REPORT:
			case CONDITION:
			case SINGLE_TXN: {
				Object param = new Object[] { dataSource.getName() };
				email.setSubject(ConvertUtils.formatObject(param, batchSubject));
				email.setBody(ConvertUtils.formatObject(param, batchBody));
				email.addAttachment(dataSource, getEncoding(dataSource));
				if(!StringUtils.isBlank(batchContentType))
					email.setContentType(batchContentType);
				break;
			}
			case TIMED_REPORT: {
				Object param = new Object[] { dataSource.getName() };
				email.setSubject(ConvertUtils.formatObject(param, timedSubject));
				email.setBody(ConvertUtils.formatObject(param, timedBody));
				email.addAttachment(dataSource, getEncoding(dataSource));
				if(!StringUtils.isBlank(timedContentType))
					email.setContentType(timedContentType);
				break;
			}
			case CUSTOM_FILE_UPLOAD:
				Object param = new Object[] { dataSource.getName() };
				email.setSubject(ConvertUtils.formatObject(param, customFileUploadSubject));
				email.setBody(ConvertUtils.formatObject(param, customFileUploadBody));
				email.addAttachment(dataSource, getEncoding(dataSource));
				if(!StringUtils.isBlank(customFileUploadContentType))
					email.setContentType(customFileUploadContentType);
				break;
			case TEST:
				email.setSubject(testSubject);
				email.setBody(testBody);
				if(!StringUtils.isBlank(testContentType))
					email.setContentType(testContentType);
				break;
			case ALERT:
			case BROADCAST:
			default:
				email.setSubject(dataSource.getName());
				email.setBody(dataSource);
		}
	}

	protected String getEncoding(DataSource dataSource) {
		return "base64";
		//return dataSource.getContentType().startsWith("text/") ? "base64": "7bit";
	}
}
