package com.usatech.transport;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Base64;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.text.StringUtils;

/**
 * POST the file to the given url + the filename.
 * 
 * @author bkrug
 * 
 */
public class POSTTransporter extends AbstractHttpTransporter {
	private static final Log log = Log.getLog();

	protected final Map<String, String> headerFromParameter = new LinkedHashMap<String, String>();

	public POSTTransporter() {
		super(new String[] { "TLSv1.1", "TLSv1.2" });
		headerFromParameter.put("batchId", "batchId");
		headerFromParameter.put("beginDate", "beginDate");
		headerFromParameter.put("endDate", "endDate");
	}

	@Override
	public String transport(Map<String, ? extends Object> transportProperties, TransportReason transportReason, Resource resource, Map<String, ? extends Object> messageParameters) throws TransportException {
		try {
			String url = ConvertUtils.getString(transportProperties.get("URL"), false);
			String username = ConvertUtils.getString(transportProperties.get("USERNAME"), false);
			String password = ConvertUtils.getString(transportProperties.get("PASSWORD"), false);

			StringBuilder sb = new StringBuilder();
			if(!url.matches("^\\w+://.*"))
				sb.append("https://");
			int pos = 0;
			while(pos < url.length() && url.charAt(pos) == '/')
				pos++;
			sb.append(url, pos, url.length());
			if(url.charAt(url.length() - 1) != '/')
				sb.append('/');
			sb.append(StringUtils.prepareURLPart(resource.getName()));
			// XXX:Can we add query params?
			sb.append("?reason=");
			sb.append(StringUtils.prepareURLPart(StringUtils.capitalizeAllWords(transportReason.toString().replace('_', ' '))));
			for(Map.Entry<String, String> entry : headerFromParameter.entrySet()) {
				if("reason".equalsIgnoreCase(entry.getKey()))
					continue;
				String value = ConvertUtils.getStringSafely(messageParameters.get(entry.getValue()), null);
				if(!StringUtils.isBlank(value))
					sb.append('&').append(StringUtils.prepareURLPart(entry.getKey())).append('=').append(StringUtils.prepareURLPart(value));
			}

			HttpPost httpMethod = new HttpPost(sb.toString());
			updateRequestSettings(httpMethod);
			httpMethod.addHeader("Accept", "text/plain");
			if(username != null) {
				String base64Token = Base64.encodeString(username + ":" + password, true);
				httpMethod.addHeader("Authorization", new StringBuilder("Basic ").append(base64Token).toString());
			}

			httpMethod.setEntity(new ResourceEntity(resource));
			log.info("Sending file via POST Request for '" + sb.toString() + "'");
			HttpResponse response = httpClient.execute(httpMethod);
			final int returnCode = response.getStatusLine().getStatusCode();
			final String responseBody = readContent(response.getEntity(), 2000);
			log.info("POST Response = " + returnCode + ": " + responseBody);
			switch(returnCode) {
				case 200:
				case 201:
				case 202:
				case 203:
				case 204:
					break;// okay
				default:
					throw new TransportException("POST Request was not successful: " + returnCode + " - " + responseBody, TransportErrorType.INSTANCE_ERR);
			}
			return returnCode + ": " + responseBody;
		} catch(ConvertException e) {
			throw new TransportException("ConvertException while converting transport parameters", e, TransportErrorType.INSTANCE_ERR);
		} catch(IOException e) {
			log.warn("Could not send POST request", e);
			throw new TransportException(e.getMessage(), e, TransportErrorType.TRANSPORT_ERR);
		}
	}
}
