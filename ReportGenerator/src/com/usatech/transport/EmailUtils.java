package com.usatech.transport;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import simple.io.Log;
import simple.mail.EmailInfo;
import simple.mail.MXRecordMailHost;
import simple.text.StringUtils;

public class EmailUtils {
	private static final Log log = Log.getLog();

	public static int sendEmails(String to, String cc, String from, String subject, String body, DataSource attachment, String failedRecipients, Map<InternetAddress, MessagingException> failingRecipients) throws TransportException {
		return sendEmails(to, cc, from, subject, body, attachment, parseRecipients(to, cc, failedRecipients), failingRecipients);
	}

	public static int sendEmails(String to, String cc, String from, String subject, String body, DataSource attachment, Set<String> recipients, Map<InternetAddress, MessagingException> failingRecipients) throws TransportException {
		EmailInfo emailInfo = new EmailInfo();
		emailInfo.setTo(to);
		emailInfo.setCc(cc);
		emailInfo.setFrom(from);
		emailInfo.setSubject(subject);
		emailInfo.setBody(body);
		if(attachment != null)
			try {
				emailInfo.addAttachment(attachment, getEncoding(attachment));
			} catch(MessagingException e) {
				throw new TransportException("Could not add attachment to email", e, TransportErrorType.PROGRAM_ERR);
			}
		return sendEmails(emailInfo, recipients, failingRecipients);
	}

	public static int sendEmails(EmailInfo emailInfo, Set<String> recipients, Map<InternetAddress, MessagingException> failingRecipients) throws TransportException {
		int cnt = 0;
		Map<String, Set<InternetAddress>> allMap = organizeRecipients(recipients);
		Properties props = new Properties();
		for(Map.Entry<String, Set<InternetAddress>> entry : allMap.entrySet()) {
			List<String> mailHostList = MXRecordMailHost.getMXRecordsForHost(entry.getKey());
			if(mailHostList.size() == 0) {
				MessagingException exception = new MessagingException("Failed to lookup mailhost through MX record for emailAddress=" + entry.getValue());
				log.warn("While sending an email", exception);
				if(failingRecipients != null) {
					for(InternetAddress address : entry.getValue())
						failingRecipients.put(address, exception);
				}
			} else {
				HashSet<InternetAddress> pendingEmailSet = new HashSet<InternetAddress>();
				pendingEmailSet.addAll(entry.getValue());
				for(int i = 0; i < mailHostList.size(); i++) {
					log.info("Using mailhost=" + mailHostList.get(i) + " to send to=" + entry.getValue());
					props.put("mail.smtp.host", mailHostList.get(i));
					Session session = Session.getInstance(props, null);

					for(Iterator<InternetAddress> it = pendingEmailSet.iterator(); it.hasNext();) {
						InternetAddress address = it.next();
						try {
							emailInfo.send(session, address);
							it.remove();
							cnt++;
							failingRecipients.remove(address);
						} catch(MessagingException e) {
							log.warn("While sending an email", e);
							if(failingRecipients != null)
								failingRecipients.put(address, e);
						}
					}
					// when all succeeded
					if(pendingEmailSet.size() == 0)
						break;
				}
			}
		}
		return cnt;
	}

	public static Set<String> parseRecipients(String to, String cc, String failed) {
		return parseRecipients(to, cc, null, failed);
	}

	public static Set<String> parseRecipients(String to, String cc, String bcc, String failed) {
		Set<String> recipients = new LinkedHashSet<String>();
		if(StringUtils.isBlank(failed)) {
			if(!StringUtils.isBlank(to))
				StringUtils.splitQuotesAnywhere(to, ",;", "\"", false, recipients);
			if(!StringUtils.isBlank(cc))
				StringUtils.splitQuotesAnywhere(cc, ",;", "\"", false, recipients);
			if(!StringUtils.isBlank(bcc))
				StringUtils.splitQuotesAnywhere(bcc, ",;", "\"", false, recipients);
		} else
			StringUtils.splitQuotesAnywhere(failed, ",;", "\"", false, recipients);
		return recipients;
	}

	protected static Map<String, Set<InternetAddress>> organizeRecipients(Set<String> recipients) throws TransportException {
		Map<String, Set<InternetAddress>> allMap = new LinkedHashMap<String, Set<InternetAddress>>();
		for(String r : recipients) {
			r = r.trim();
			try {
				InternetAddress address = new InternetAddress(r);
				String hostName = MXRecordMailHost.getHostname(address.getAddress());
				Set<InternetAddress> hostSet = allMap.get(hostName);
				if(hostSet == null) {
					hostSet = new LinkedHashSet<InternetAddress>();
					allMap.put(hostName, hostSet);
				}
				hostSet.add(address);
			} catch(AddressException e) {
				throw new TransportException("Recipient '" + r + "' is invalid", e, TransportErrorType.TRANSPORT_ERR);
			} catch(IllegalArgumentException e) {
				throw new TransportException("Recipient '" + r + "' is invalid", e, TransportErrorType.TRANSPORT_ERR);
			}
		}
		return allMap;
	}

	protected static String getEncoding(DataSource dataSource) {
		return "base64";
		// return dataSource.getContentType().startsWith("text/") ? "base64": "7bit";
	}
}
