package com.usatech.transport.soap;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;
import org.apache.axis.types.UnsignedInt;

import com.usatech.transport.TransportErrorType;
import com.usatech.transport.TransportException;
import com.usatech.transport.TransportReason;
import com.usatech.transport.TransportUtils;
import com.usatech.transport.Transporter;
import com.usatech.transport.soap.cantaloupe.InteractionLocator;
import com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.resource.Resource;

public class CantaloupeSOAPTransporter implements Transporter {
    /*  Cantaloupe Import function return values:
        "User not found or invalid login" - bad login/password
        "User does not have permission to import dex" - improper permissions - users by default do not have permission to import dex
        "Machine not found" - unit has not been assigned to a machine
        "Import error: <errormsg>" - error message
        "Import successful" - success  */

	public String transport(Map<String,  ? extends Object> transportProperties,
			TransportReason transportReason, Resource resource,
			Map<String, ? extends Object> messageParameters) throws TransportException {

        String endpointURL;
        URL endpoint;
        String username, password;
        String assetNumberString;
		boolean fillFlag;
		Calendar dexCal;
		try{
	        endpointURL = ConvertUtils.getString(transportProperties.get("URL"), false);
			if(endpointURL == null)
				throw new TransportException("URL transport property is null", TransportErrorType.INSTANCE_ERR);

	        try {
	            endpoint = new URL(endpointURL);
	        } catch(MalformedURLException exc) {
	            throw new TransportException("MalformedURLException while creating URL object from "
	                    + endpointURL, exc, TransportErrorType.INSTANCE_ERR);
	        }

			username = ConvertUtils.getString(transportProperties.get("USERNAME"), false);
			if(username == null)
				username = "";

			password = ConvertUtils.getString(transportProperties.get("PASSWORD"), false);
			if(password == null)
				password = "";

			assetNumberString = ConvertUtils.getString(messageParameters.get("assetNumber"), false);
			fillFlag = ConvertUtils.getBoolean(messageParameters.get("fillFlag"));
			dexCal = ConvertUtils.convert(Calendar.class, messageParameters.get("dexTime"));
		}
		catch(ConvertException e){
			throw new TransportException(
					"ConvertException while converting DEX file parameters", e, TransportErrorType.INSTANCE_ERR);
		}

		UnsignedInt assetNumber;
		try {
			assetNumber = new UnsignedInt(assetNumberString);
		} catch(NumberFormatException e) {
			throw new TransportException("NumberFormatException while converting asset number "
					+ assetNumberString + " to UnsignedInt", e, TransportErrorType.INSTANCE_ERR);
		}

        String dexData = TransportUtils.getDEXFileData(resource);
		InteractionLocator locator = new InteractionLocator();

		InteractionSoap_PortType webService;
		try {
			webService = locator.getInteractionSoap12(endpoint);
		} catch(ServiceException exc) {
			throw new TransportException("ServiceException while connecting to " + endpointURL,
					exc, TransportErrorType.TRANSPORT_ERR);
		}
		
		//set timeout to 0 for configuration properties to take effect
		((Stub)webService).setTimeout(0);

		String importResult;
		try {
			importResult = webService._import(username, password, assetNumber, dexData, fillFlag, dexCal);
		} catch(RemoteException re) {
			throw new TransportException("RemoteException while sending DEX file", re, TransportErrorType.TRANSPORT_ERR);
		}

        if (importResult == null)
            importResult = "null";
        else
        {
            int posLF = importResult.indexOf("\n");
            if (posLF > 0)
                importResult = importResult.substring(0, posLF);
            if (importResult.length() > 200)
                importResult = importResult.substring(0, 200);
            importResult.replaceAll("\r", "").trim();
            if (importResult.length() == 0)
                importResult = "empty";
        }

		return endpointURL + "; RESULT=" + importResult;
	}
}
