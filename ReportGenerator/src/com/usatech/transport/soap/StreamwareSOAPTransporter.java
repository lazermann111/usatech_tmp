package com.usatech.transport.soap;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import com.usatech.transport.TransportErrorType;
import com.usatech.transport.TransportException;
import com.usatech.transport.TransportReason;
import com.usatech.transport.TransportUtils;
import com.usatech.transport.Transporter;
import com.usatech.transport.soap.streamware.IvmRemoteDexbindingStub;
import com.usatech.transport.soap.streamware.IvmRemoteDexserviceLocator;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.resource.Resource;

public class StreamwareSOAPTransporter implements Transporter {
    public String transport(Map<String, ? extends Object> transportProperties,
            TransportReason transportReason, Resource resource,
            Map<String, ? extends Object> messageParameters) throws TransportException {

        String endpointURL,assetNumber, dexGMTTime;
        URL endpoint;
        try{
	        endpointURL=ConvertUtils.getString(transportProperties.get("URL"), false);
	        if(endpointURL == null)
	            throw new TransportException("URL transport property is null", TransportErrorType.INSTANCE_ERR);

	        try {
	            endpoint = new URL(endpointURL);
	        } catch(MalformedURLException exc) {
	            throw new TransportException("MalformedURLException while creating URL object from "
	                    + endpointURL, exc, TransportErrorType.INSTANCE_ERR);
	        }
	        assetNumber = ConvertUtils.getString(messageParameters.get("assetNumber"), false);
	        dexGMTTime = ConvertUtils.getString(messageParameters.get("dexGMTTime"), false);
		}
		catch(ConvertException e){
			throw new TransportException(
					"ConvertException while converting DEX file parameters", e, TransportErrorType.INSTANCE_ERR);
		}

        String dexData = TransportUtils.getDEXFileData(resource);
        IvmRemoteDexserviceLocator locator = new IvmRemoteDexserviceLocator();

        IvmRemoteDexbindingStub webService;
        try {
            webService = (IvmRemoteDexbindingStub)locator.getIvmRemoteDexPort(endpoint);
            String username=ConvertUtils.getString(transportProperties.get("USERNAME"), false);
            String password=ConvertUtils.getString(transportProperties.get("PASSWORD"), false);
            if(username!=null&&password!=null){
	            webService.setUsername(username);
	            webService.setPassword(password);
            }
        } catch(ServiceException exc) {
            throw new TransportException("ServiceException while connecting to " + endpointURL,
                    exc, TransportErrorType.TRANSPORT_ERR);
        } catch(ConvertException e){
    		throw new TransportException("ConvertException while converting String", e, TransportErrorType.INSTANCE_ERR);
    	}
        
        //set timeout to 0 for configuration properties to take effect
	    webService.setTimeout(0);

        int importRC;
        try {
            importRC = webService.processDexFile(assetNumber, dexGMTTime, dexData);
        } catch(RemoteException re) {
            throw new TransportException("RemoteException while sending DEX file", re, TransportErrorType.TRANSPORT_ERR);
        }

        String importResult = importRC + ", ";
        switch(importRC)
        {
        case 0:
            importResult += "No Error";
            break;
        case 1:
            importResult += "Dex Web Service Error: Unable to load settings";
            break;
        case 2:
            importResult += "Dex Web Service Error: Unable to open VendMAX Database";
            break;
        case 3:
            importResult += "Dex Web Service Error: Unable to save dex file locally";
            break;
        case 4:
            importResult += "Dex Web Service Error: Failed to send email";
            break;
        case 5:
            importResult += "Dex Web Service Error: Failed to load Dex Parser DLL";
            break;
        case 6:
            importResult += "Dex Web Service Error: Unable to find Matching Point of Sale";
            break;
        case 7:
            importResult += "Dex Web Service Error: Initial Dex Parse Failed";
            break;
        case 8:
            importResult += "Dex Web Service Error: Failure Parsing Debit Card Fields";
            break;
        case 9:
            importResult += "Dex Web Service Error: One or more cumulatives are too high (based on previous DEX)";
            break;
        case 10:
            importResult += "Dex Web Service Error: One or more columns do not exist in previous DEX";
            break;
        case 11:
            importResult += "Dex Web Service Error: Failed to load the Dex Master Parser";
            break;
        case 12:
            importResult += "Dex Web Service Error: No Machine Id Received";
            break;
        case 13:
            importResult += "Dex Web Service Error: Invalid DateTime Received";
            break;
        case 14:
            importResult += "Dex Web Service Error: Invalid Dex File Received";
            break;
        case 15:
            importResult += "Dex Web Service Error: One or more cumulatives are too low (based on previous DEX)";
            break;
        case 16:
            importResult += "Dex Web Service Error: Failed to open/process dex file";
            break;
        case 17:
            importResult += "Dex Web Service Error: Unknown Exception during Parse";
            break;
        default:
            importResult += ", Unknown return value";
        }

        return endpointURL + "; RESULT=" + importResult;
    }
}
