/**
 * GenericWebServiceEndPointService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.compass;

public interface GenericWebServiceEndPointService extends javax.xml.rpc.Service {
    public java.lang.String getsubmitMessageAddress();

    public com.usatech.transport.soap.compass.GenericWebServiceEndPoint getsubmitMessage() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.compass.GenericWebServiceEndPoint getsubmitMessage(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
