/**
 * GenericWebServiceEndPointServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.compass;

public class GenericWebServiceEndPointServiceLocator extends org.apache.axis.client.Service implements com.usatech.transport.soap.compass.GenericWebServiceEndPointService {

    public GenericWebServiceEndPointServiceLocator() {
    }


    public GenericWebServiceEndPointServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GenericWebServiceEndPointServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for submitMessage
    private java.lang.String submitMessage_address = "http://esbdev.compass-usa.com/services/GenericWebServiceEndPoint";

    public java.lang.String getsubmitMessageAddress() {
        return submitMessage_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String submitMessageWSDDServiceName = "submitMessage";

    public java.lang.String getsubmitMessageWSDDServiceName() {
        return submitMessageWSDDServiceName;
    }

    public void setsubmitMessageWSDDServiceName(java.lang.String name) {
        submitMessageWSDDServiceName = name;
    }

    public com.usatech.transport.soap.compass.GenericWebServiceEndPoint getsubmitMessage() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(submitMessage_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsubmitMessage(endpoint);
    }

    public com.usatech.transport.soap.compass.GenericWebServiceEndPoint getsubmitMessage(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.compass.SubmitMessageSoapBindingStub _stub = new com.usatech.transport.soap.compass.SubmitMessageSoapBindingStub(portAddress, this);
            _stub.setPortName(getsubmitMessageWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsubmitMessageEndpointAddress(java.lang.String address) {
        submitMessage_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.transport.soap.compass.GenericWebServiceEndPoint.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.compass.SubmitMessageSoapBindingStub _stub = new com.usatech.transport.soap.compass.SubmitMessageSoapBindingStub(new java.net.URL(submitMessage_address), this);
                _stub.setPortName(getsubmitMessageWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("submitMessage".equals(inputPortName)) {
            return getsubmitMessage();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://esbdev.compass-usa.com/services/GenericWebServiceEndPoint", "GenericWebServiceEndPointService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://esbdev.compass-usa.com/services/GenericWebServiceEndPoint", "submitMessage"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("submitMessage".equals(portName)) {
            setsubmitMessageEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
