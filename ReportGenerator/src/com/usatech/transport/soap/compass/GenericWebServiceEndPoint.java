/**
 * GenericWebServiceEndPoint.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.compass;

public interface GenericWebServiceEndPoint extends java.rmi.Remote {
    public boolean submitMessage(java.lang.String submitterID, java.lang.String messageID, java.lang.String fileType, java.lang.String fileName, java.lang.String message) throws java.rmi.RemoteException;
}
