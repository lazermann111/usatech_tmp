package com.usatech.transport.soap.compass;

public class GenericWebServiceEndPointProxy implements com.usatech.transport.soap.compass.GenericWebServiceEndPoint {
  private String _endpoint = null;
  private com.usatech.transport.soap.compass.GenericWebServiceEndPoint genericWebServiceEndPoint = null;
  
  public GenericWebServiceEndPointProxy() {
    _initGenericWebServiceEndPointProxy();
  }
  
  private void _initGenericWebServiceEndPointProxy() {
    try {
      genericWebServiceEndPoint = (new com.usatech.transport.soap.compass.GenericWebServiceEndPointServiceLocator()).getsubmitMessage();
      if (genericWebServiceEndPoint != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)genericWebServiceEndPoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)genericWebServiceEndPoint)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (genericWebServiceEndPoint != null)
      ((javax.xml.rpc.Stub)genericWebServiceEndPoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.usatech.transport.soap.compass.GenericWebServiceEndPoint getGenericWebServiceEndPoint() {
    if (genericWebServiceEndPoint == null)
      _initGenericWebServiceEndPointProxy();
    return genericWebServiceEndPoint;
  }
  
  public boolean submitMessage(java.lang.String submitterID, java.lang.String messageID, java.lang.String fileType, java.lang.String fileName, java.lang.String message) throws java.rmi.RemoteException{
    if (genericWebServiceEndPoint == null)
      _initGenericWebServiceEndPointProxy();
    return genericWebServiceEndPoint.submitMessage(submitterID, messageID, fileType, fileName, message);
  }
  
  
}