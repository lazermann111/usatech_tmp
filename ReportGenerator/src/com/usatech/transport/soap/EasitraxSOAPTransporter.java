package com.usatech.transport.soap;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axis.client.Stub;
import org.apache.axis.message.SOAPHeaderElement;
import org.xml.sax.SAXException;

import com.usatech.transport.TransportErrorType;
import com.usatech.transport.TransportException;
import com.usatech.transport.TransportReason;
import com.usatech.transport.Transporter;
import com.usatech.transport.soap.easitrax.VDIUploadDEXLocator;
import com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType;
import com.usatech.transport.soap.vdi.VDIDexUploadInput;
import com.usatech.transport.soap.vdi.VDIDexUploadOutput;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Base64;
import simple.io.LoadingException;
import simple.io.resource.Resource;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.TemplatesLoader;
import simple.xml.sax.BeanAdapter1;
import simple.xml.sax.BeanInputSource;

public class EasitraxSOAPTransporter extends VDISOAPTransporter implements Transporter {
    public String transport(Map<String,? extends Object> transportProperties,
			TransportReason transportReason, Resource resource,
			Map<String, ? extends Object> messageParameters) throws TransportException {
    	String endpointURL, customerId, username, password;
    	URL endpoint;
    	VDIDexUploadInput input = new VDIDexUploadInput();
    	try{
	        endpointURL = ConvertUtils.getString(transportProperties.get("URL"), false);
			if(StringUtils.isBlank(endpointURL))
				throw new TransportException("URL transport property is blank", TransportErrorType.INSTANCE_ERR);

	        try {
	            endpoint = new URL(endpointURL);
	        } catch(MalformedURLException exc) {
	            throw new TransportException("MalformedURLException while creating URL object from " + endpointURL, exc, TransportErrorType.INSTANCE_ERR);
	        }
	        
	        customerId = ConvertUtils.getString(transportProperties.get("VDI CUSTOMER ID"), false);
	        if(StringUtils.isBlank(customerId))
				throw new TransportException("VDI Customer Id transport property is blank", TransportErrorType.INSTANCE_ERR);
	        
			username = ConvertUtils.getStringSafely(transportProperties.get("USERNAME"), "");
			password = ConvertUtils.getStringSafely(transportProperties.get("PASSWORD"), "");

	        input.setApplicationId(APPLICATION_ID);
	        input.setApplicationVersion(APPLICATION_VERSION);
	        input.setCustomerId(customerId);
	        if(transportReason.equals(TransportReason.TEST)){
	        	input.setDeviceSerialCd("TEST");
	        	input.setDexDate(new Date());
	        	input.setDexReason(6);
	        	input.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
	 	        input.setTransactionId(String.valueOf(System.currentTimeMillis()));
	 	        
	        }else{
	 	        input.setDeviceSerialCd(ConvertUtils.getString(messageParameters.get("deviceSerialCd"), true));
	        	input.setDexDate(ConvertUtils.convertRequired(Calendar.class, messageParameters.get("dexTime")).getTime());
	        	input.setDexReason(toDexReason(ConvertUtils.getInt(messageParameters.get("dexType"))));
	        	input.setTimeZone(TimeZone.getTimeZone(ConvertUtils.convertRequired(String.class, messageParameters.get("timezone"))));
	 	        input.setTransactionId(ConvertUtils.getString(messageParameters.get("batchId"), true));
	        }
	        input.setDexCompressionParam(null);
	        input.setDexCompressionType(null);
	        input.setDexEncoding(1);
	        input.setDexFile(resource);
	        input.setProviderId(PROVIDER_ID);
	        input.setXmlVersion("1.0");
		} catch (ConvertException e) {
			throw new TransportException("Could not convert transport properties as needed", e, TransportErrorType.INSTANCE_ERR);
		}
		
		VDIUploadDEXLocator locator = new VDIUploadDEXLocator();
		VDIUploadDEXSoap_PortType webService;
		try {
			webService = locator.getVDIUploadDEXSoap(endpoint);
		} catch(ServiceException exc) {
			throw new TransportException("ServiceException while connecting to " + endpointURL,
					exc, TransportErrorType.TRANSPORT_ERR);
		}
		if(!(webService instanceof Stub))
			throw new TransportException("Can not set username and password because webService is not an instance of Stub",TransportErrorType.TRANSPORT_ERR);
		
		((Stub)webService).setUsername(username);
		((Stub)webService).setPassword(password);		
		
		//add SOAP header for credentials
		SOAPHeaderElement credentialsDEXHeader = new SOAPHeaderElement("urn:WANServiceSB", "CredentialsDEXHeader");
		SOAPHeaderElement userElement = new SOAPHeaderElement("urn:WANServiceSB", "Username", Base64.encodeBytes(getExtendedBytes(username), true));
		SOAPHeaderElement passwordElement = new SOAPHeaderElement("urn:WANServiceSB", "Password", Base64.encodeBytes(getExtendedBytes(password), true));
		try {
			credentialsDEXHeader.addChild(userElement);
			credentialsDEXHeader.addChild(passwordElement);
		} catch (SOAPException e) {
			throw new TransportException("SOAPException while adding credentials header", e, TransportErrorType.TRANSPORT_ERR);
		}
		((Stub)webService).setHeader(credentialsDEXHeader);
		
		VDIDexUploadOutput output = uploadDex(webService, input);
	       
        return endpointURL + "; RESULT=" + output.getResultMessage() + " (" + output.getResultCode() + ")";
    }
    
    protected VDIDexUploadOutput uploadDex(VDIUploadDEXSoap_PortType webService, VDIDexUploadInput input) throws TransportException {		
    	//set timeout to 0 for configuration properties to take effect
    	((Stub)webService).setTimeout(0);
    	
    	StringWriter writer = new StringWriter();
    	String resultXml;
        try {
        	TemplatesLoader.getInstance(null).newTransformer("resource:com/usatech/transport/soap/vdi/vdiDexUpload.xsl", refreshInterval).transform(
        			new SAXSource(new BeanAdapter1(), new BeanInputSource(input, "info")), new StreamResult(writer));
        	resultXml = webService.uploadDex(input.getTransactionId(), input.getProviderId(), input.getCustomerId(), input.getApplicationId(),
        			input.getApplicationVersion(), input.getDexEncoding(), input.getDexCompressionType(), input.getDexCompressionParam(),
        			null, input.getXmlVersion(), writer.toString());
        } catch(RemoteException e) {
            throw new TransportException("RemoteException while sending DEX file", e, TransportErrorType.TRANSPORT_ERR);
        } catch(TransformerConfigurationException e) {
        	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(TransformerException e) {
        	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(IOException e) {
        	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(LoadingException e) {
        	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
		}		

        ObjectBuilder builder;
		try {
			builder = new ObjectBuilder(new StringReader(resultXml));
		} catch(IOException e) {
        	throw new TransportException("Could not parse VDI result XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(SAXException e) {
        	throw new TransportException("Could not parse VDI result XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(ParserConfigurationException e) {
        	throw new TransportException("Could not parse VDI result XML", e, TransportErrorType.TRANSPORT_ERR);
		}
		VDIDexUploadOutput output = new VDIDexUploadOutput();
		String[] values = builder.getNodeValues("Code");
		if(values==null||values.length != 1)
        	throw new TransportException("Could not parse VDI result XML: Found " + values==null?"no":values.length + " <Code> elements", TransportErrorType.TRANSPORT_ERR);
		try {
			output.setResultCode(ConvertUtils.getInt(values[0]));
		} catch(ConvertException e) {
			throw new TransportException("Could not parse VDI result XML: Non-numeric Code value", e, TransportErrorType.TRANSPORT_ERR);
		}
		values = builder.getNodeValues("Message");
		if(values==null||values.length != 1)
        	throw new TransportException("Could not parse VDI result XML: Found " + values==null?"no":values.length + " <Message> elements", TransportErrorType.TRANSPORT_ERR);
		output.setResultMessage(values[0]);
		return output;
	}
    
    protected byte[] getExtendedBytes(String input) {
    	byte[] bytes = input.getBytes();
    	byte[] extended = new byte[input.length() * 2];
    	for (int i=0; i < extended.length; i++) {
			if (i % 2 == 0)
				extended[i] = bytes[i / 2];
			else
				extended[i] = 0x00;
		}
    	return extended;
    }
}
