/**
 * IvmRemoteDex.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.streamware;

public interface IvmRemoteDex extends java.rmi.Remote {
    public int processDexFile(java.lang.String machineID, java.lang.String dateTimeRead, java.lang.String dexFile) throws java.rmi.RemoteException;
}
