/**
 * IvmRemoteDexbindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.streamware;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class IvmRemoteDexbindingImpl implements com.usatech.transport.soap.streamware.IvmRemoteDex{
	protected static String saveDirectory = ".";
    public int processDexFile(java.lang.String machineID, java.lang.String dateTimeRead, java.lang.String dexFile) throws java.rmi.RemoteException {
        // this is for testing only
        try {
			FileWriter fw = new FileWriter(new File(saveDirectory, "Streamware test DEX import " + System.currentTimeMillis() + ".log"));
            fw.write("machineID: " + machineID + "\n");
            fw.write("dateTimeRead: " + dateTimeRead + "\n");
            fw.write("dexfile: " + dexFile);
            fw.flush();
            fw.close();
        } catch (IOException e) {
                throw new java.rmi.RemoteException(e.getMessage());
        }
        return 0;
    }

	public static String getSaveDirectory() {
		return saveDirectory;
	}

	public static void setSaveDirectory(String saveDirectory) {
		IvmRemoteDexbindingImpl.saveDirectory = saveDirectory;
	}
}
