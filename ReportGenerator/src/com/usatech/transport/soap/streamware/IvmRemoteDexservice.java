/**
 * IvmRemoteDexservice.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.streamware;

public interface IvmRemoteDexservice extends javax.xml.rpc.Service {
    public java.lang.String getIvmRemoteDexPortAddress();

    public com.usatech.transport.soap.streamware.IvmRemoteDex getIvmRemoteDexPort() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.streamware.IvmRemoteDex getIvmRemoteDexPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
