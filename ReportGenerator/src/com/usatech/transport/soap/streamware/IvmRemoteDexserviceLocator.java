/**
 * IvmRemoteDexserviceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.streamware;

public class IvmRemoteDexserviceLocator extends org.apache.axis.client.Service implements com.usatech.transport.soap.streamware.IvmRemoteDexservice {

    public IvmRemoteDexserviceLocator() {
    }


    public IvmRemoteDexserviceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public IvmRemoteDexserviceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IvmRemoteDexPort
    private java.lang.String IvmRemoteDexPort_address = "http://67.95.134.212/remote/vmremotedexcgi.exe/soap/IvmRemoteDex";

    public java.lang.String getIvmRemoteDexPortAddress() {
        return IvmRemoteDexPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IvmRemoteDexPortWSDDServiceName = "IvmRemoteDexPort";

    public java.lang.String getIvmRemoteDexPortWSDDServiceName() {
        return IvmRemoteDexPortWSDDServiceName;
    }

    public void setIvmRemoteDexPortWSDDServiceName(java.lang.String name) {
        IvmRemoteDexPortWSDDServiceName = name;
    }

    public com.usatech.transport.soap.streamware.IvmRemoteDex getIvmRemoteDexPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IvmRemoteDexPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIvmRemoteDexPort(endpoint);
    }

    public com.usatech.transport.soap.streamware.IvmRemoteDex getIvmRemoteDexPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.streamware.IvmRemoteDexbindingStub _stub = new com.usatech.transport.soap.streamware.IvmRemoteDexbindingStub(portAddress, this);
            _stub.setPortName(getIvmRemoteDexPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIvmRemoteDexPortEndpointAddress(java.lang.String address) {
        IvmRemoteDexPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.transport.soap.streamware.IvmRemoteDex.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.streamware.IvmRemoteDexbindingStub _stub = new com.usatech.transport.soap.streamware.IvmRemoteDexbindingStub(new java.net.URL(IvmRemoteDexPort_address), this);
                _stub.setPortName(getIvmRemoteDexPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IvmRemoteDexPort".equals(inputPortName)) {
            return getIvmRemoteDexPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "IvmRemoteDexservice");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "IvmRemoteDexPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IvmRemoteDexPort".equals(portName)) {
            setIvmRemoteDexPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
