/**
 * WsMainLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.bestvendor;

@SuppressWarnings({ "unchecked", "rawtypes", "serial" })
public class WsMainLocator extends org.apache.axis.client.Service implements com.usatech.transport.soap.bestvendor.WsMain {

    public WsMainLocator() {
    }


    public WsMainLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WsMainLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for wsMainSoap12
    private java.lang.String wsMainSoap12_address = "http://208.82.3.241:85/ws_RDC.asmx";

    public java.lang.String getwsMainSoap12Address() {
        return wsMainSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String wsMainSoap12WSDDServiceName = "wsMainSoap12";

    public java.lang.String getwsMainSoap12WSDDServiceName() {
        return wsMainSoap12WSDDServiceName;
    }

    public void setwsMainSoap12WSDDServiceName(java.lang.String name) {
        wsMainSoap12WSDDServiceName = name;
    }

    public com.usatech.transport.soap.bestvendor.WsMainSoap getwsMainSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(wsMainSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getwsMainSoap12(endpoint);
    }

    public com.usatech.transport.soap.bestvendor.WsMainSoap getwsMainSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.bestvendor.WsMainSoap12Stub _stub = new com.usatech.transport.soap.bestvendor.WsMainSoap12Stub(portAddress, this);
            _stub.setPortName(getwsMainSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setwsMainSoap12EndpointAddress(java.lang.String address) {
        wsMainSoap12_address = address;
    }


    // Use to get a proxy class for wsMainSoap
    private java.lang.String wsMainSoap_address = "http://208.82.3.241:85/ws_RDC.asmx";

    public java.lang.String getwsMainSoapAddress() {
        return wsMainSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String wsMainSoapWSDDServiceName = "wsMainSoap";

    public java.lang.String getwsMainSoapWSDDServiceName() {
        return wsMainSoapWSDDServiceName;
    }

    public void setwsMainSoapWSDDServiceName(java.lang.String name) {
        wsMainSoapWSDDServiceName = name;
    }

    public com.usatech.transport.soap.bestvendor.WsMainSoap getwsMainSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(wsMainSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getwsMainSoap(endpoint);
    }

    public com.usatech.transport.soap.bestvendor.WsMainSoap getwsMainSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.bestvendor.WsMainSoapStub _stub = new com.usatech.transport.soap.bestvendor.WsMainSoapStub(portAddress, this);
            _stub.setPortName(getwsMainSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setwsMainSoapEndpointAddress(java.lang.String address) {
        wsMainSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.transport.soap.bestvendor.WsMainSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.bestvendor.WsMainSoap12Stub _stub = new com.usatech.transport.soap.bestvendor.WsMainSoap12Stub(new java.net.URL(wsMainSoap12_address), this);
                _stub.setPortName(getwsMainSoap12WSDDServiceName());
                return _stub;
            }
            if (com.usatech.transport.soap.bestvendor.WsMainSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.bestvendor.WsMainSoapStub _stub = new com.usatech.transport.soap.bestvendor.WsMainSoapStub(new java.net.URL(wsMainSoap_address), this);
                _stub.setPortName(getwsMainSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("wsMainSoap12".equals(inputPortName)) {
            return getwsMainSoap12();
        }
        else if ("wsMainSoap".equals(inputPortName)) {
            return getwsMainSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.bestvendors.com/", "wsMain");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.bestvendors.com/", "wsMainSoap12"));
            ports.add(new javax.xml.namespace.QName("http://www.bestvendors.com/", "wsMainSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("wsMainSoap12".equals(portName)) {
            setwsMainSoap12EndpointAddress(address);
        }
        else 
if ("wsMainSoap".equals(portName)) {
            setwsMainSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
