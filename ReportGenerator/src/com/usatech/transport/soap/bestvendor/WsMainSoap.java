/**
 * WsMainSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.bestvendor;

public interface WsMainSoap extends java.rmi.Remote {

    /**
     * Web service to receive inbound documents from vendors.  The
     * string parameter is a BASE64 encoded .zip file.
     */
    public java.lang.String inbound_RDC(java.lang.String buffer) throws java.rmi.RemoteException;
}
