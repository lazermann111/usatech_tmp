/**
 * WsMain.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.bestvendor;

public interface WsMain extends javax.xml.rpc.Service {
    public java.lang.String getwsMainSoap12Address();

    public com.usatech.transport.soap.bestvendor.WsMainSoap getwsMainSoap12() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.bestvendor.WsMainSoap getwsMainSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getwsMainSoapAddress();

    public com.usatech.transport.soap.bestvendor.WsMainSoap getwsMainSoap() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.bestvendor.WsMainSoap getwsMainSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
