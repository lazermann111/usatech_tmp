package com.usatech.transport.soap;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.LoadingException;
import simple.io.resource.Resource;
import simple.xml.ObjectBuilder;
import simple.xml.TemplatesLoader;
import simple.xml.sax.BeanAdapter1;
import simple.xml.sax.BeanInputSource;

import com.usatech.transport.TransportErrorType;
import com.usatech.transport.TransportException;
import com.usatech.transport.TransportReason;
import com.usatech.transport.Transporter;
import com.usatech.transport.soap.vdi.HIADWebServiceLocator;
import com.usatech.transport.soap.vdi.HIADWebServiceSoap;
import com.usatech.transport.soap.vdi.HIADWebServiceSoapStub;
import com.usatech.transport.soap.vdi.VDIDexUploadInput;
import com.usatech.transport.soap.vdi.VDIDexUploadOutput;

public class VDISOAPTransporter implements Transporter {
	protected static final String PROVIDER_ID = "USAT";
	protected static final String APPLICATION_ID = "ReportGenerator";
	protected static final String APPLICATION_VERSION = "3";
	protected int refreshInterval = 15000;
    public String transport(Map<String, ? extends Object> transportProperties,
            TransportReason transportReason, Resource resource,
            Map<String, ? extends Object> messageParameters) throws TransportException {

        String endpointURL;
        URL endpoint;
        VDIDexUploadInput input = new VDIDexUploadInput();
        try{
	        endpointURL = ConvertUtils.getString(transportProperties.get("URL"), false);
	        if(endpointURL == null)
	            throw new TransportException("URL transport property is null", TransportErrorType.INSTANCE_ERR);

	        try {
	            endpoint = new URL(endpointURL);
	        } catch(MalformedURLException exc) {
	            throw new TransportException("MalformedURLException while creating URL object from "
	                    + endpointURL, exc, TransportErrorType.INSTANCE_ERR);
	        }
	        input.setApplicationId(APPLICATION_ID);
	        input.setApplicationVersion(APPLICATION_VERSION);
	        input.setCustomerId(ConvertUtils.getString(transportProperties.get("VDI CUSTOMER ID"), true));
	        if(transportReason.equals(TransportReason.TEST)){
	        	input.setDeviceSerialCd("TEST");
	        	input.setDexDate(new Date());
	        	input.setDexReason(99);
	        	input.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
	 	        input.setTransactionId("1");
	 	        
	        }else{
	 	        input.setDeviceSerialCd(ConvertUtils.getString(messageParameters.get("deviceSerialCd"), true));
	        	input.setDexDate(ConvertUtils.convertRequired(Calendar.class, messageParameters.get("dexTime")).getTime());
	        	input.setDexReason(toDexReason(ConvertUtils.getInt(messageParameters.get("dexType"))));
	        	input.setTimeZone(TimeZone.getTimeZone(ConvertUtils.convertRequired(String.class, messageParameters.get("timezone"))));
	 	        input.setTransactionId(ConvertUtils.getString(messageParameters.get("batchId"), true));
	        }
	        input.setDexCompressionParam(null);
	        input.setDexCompressionType(null);
	        input.setDexEncoding(1);
	        input.setDexFile(resource);
	        input.setProviderId(PROVIDER_ID);
	        input.setXmlVersion("1.0");
		} catch(ConvertException e){
			throw new TransportException("Could not convert transport properties as needed", e, TransportErrorType.INSTANCE_ERR);
		}

        HIADWebServiceLocator locator = new HIADWebServiceLocator();
        HIADWebServiceSoapStub webService;
        try {
            webService = (HIADWebServiceSoapStub)locator.getHIADWebServiceSoap(endpoint);
            String username=ConvertUtils.getString(transportProperties.get("USERNAME"), false);
            String password=ConvertUtils.getString(transportProperties.get("PASSWORD"), false);
            if(username!=null&&password!=null){
	            webService.setUsername(username);
	            webService.setPassword(password);
            }
        } catch(ServiceException e) {
            throw new TransportException("ServiceException while connecting to " + endpointURL, e, TransportErrorType.TRANSPORT_ERR);
        }catch(ConvertException e){
    		throw new TransportException("ConvertException while converting String", e, TransportErrorType.INSTANCE_ERR);
    	}
        VDIDexUploadOutput output = uploadDex(webService, input);
       
        return endpointURL + "; RESULT=" + output.getResultMessage() + " (" + output.getResultCode() + ")";
    }

    protected VDIDexUploadOutput uploadDex(HIADWebServiceSoap webService, VDIDexUploadInput input) throws TransportException {
    	//set timeout to 0 for configuration properties to take effect
	    ((HIADWebServiceSoapStub)webService).setTimeout(0);
    	
    	StringWriter writer = new StringWriter();
    	String resultXml;
        try {
        	TemplatesLoader.getInstance(null).newTransformer("resource:com/usatech/transport/soap/vdi/vdiDexUpload.xsl", refreshInterval).transform(
        			new SAXSource(new BeanAdapter1(), new BeanInputSource(input, "info")), new StreamResult(writer));
        	resultXml = webService.uploadDex(input.getTransactionId(), input.getProviderId(), input.getCustomerId(), input.getApplicationId(),
        			input.getApplicationVersion(), input.getDexEncoding(), input.getDexCompressionType(), input.getDexCompressionParam(),
        			null, input.getXmlVersion(), writer.toString());
        } catch(RemoteException e) {
            throw new TransportException("RemoteException while sending DEX file", e, TransportErrorType.TRANSPORT_ERR);
        } catch(TransformerConfigurationException e) {
        	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(TransformerException e) {
        	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(IOException e) {
        	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(LoadingException e) {
        	throw new TransportException("Could not format VDI XML", e, TransportErrorType.TRANSPORT_ERR);
		}
		ObjectBuilder builder;
		try {
			builder = new ObjectBuilder(new StringReader(resultXml));
		} catch(IOException e) {
        	throw new TransportException("Could not parse VDI result XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(SAXException e) {
        	throw new TransportException("Could not parse VDI result XML", e, TransportErrorType.TRANSPORT_ERR);
		} catch(ParserConfigurationException e) {
        	throw new TransportException("Could not parse VDI result XML", e, TransportErrorType.TRANSPORT_ERR);
		}
		VDIDexUploadOutput output = new VDIDexUploadOutput();
		String[] values = builder.getNodeValues("Code");
		if(values==null||values.length != 1)
        	throw new TransportException("Could not parse VDI result XML: Found " + values==null?"no":values.length + " <Code> elements", TransportErrorType.TRANSPORT_ERR);
		try {
			output.setResultCode(ConvertUtils.getInt(values[0]));
		} catch(ConvertException e) {
			throw new TransportException("Could not parse VDI result XML: Non-numeric Code value", e, TransportErrorType.TRANSPORT_ERR);
		}
		values = builder.getNodeValues("Message");
		if(values==null||values.length != 1)
        	throw new TransportException("Could not parse VDI result XML: Found " + values==null?"no":values.length + " <Message> elements", TransportErrorType.TRANSPORT_ERR);
		output.setResultMessage(values[0]);
		return output;
    }
	/**
	 * @param int1
	 * @return
	 */
	protected static int toDexReason(int dexType) {
		/* VDI Mapping:
		 * 0 - Scheduled DEX Read,
		 * 1 - Service Button Pressed,
		 * 2 - Door Open,
		 * 3 - Door Closed,
		 * 4 - Fulfills Specific Request,
		 * 5 - Alert/VMC initiated,
		 * 6 - Dex from external Device(Driver HH or similar),
		 * 99 - Other
		 */

		/* USALive Mapping:
		 * if(info[4].equalsIgnoreCase("SCHEDULED")) dexType = 1;
			else if(info[4].equalsIgnoreCase("INTERVAL")) dexType = 2;
			else if(info[4].equalsIgnoreCase("FILL")) dexType = 3;
			else if(info[4].equalsIgnoreCase("ALARM")) dexType = 4;
			else log.warn("Unknown DEX type: '" + info[4] + "'");
		 */
		switch(dexType) {
			case 1: //SCHEDULED
				return 0;
			case 2: //INTERVAL - None of these exist in production as of 10/26/2009; assume it is unused
				return 0;
			case 3: // FILL
				return 1;
			case 4: // ALARM
				return 5;
			default:
				return 99;
		}
	}

	public int getRefreshInterval() {
		return refreshInterval;
	}

	public void setRefreshInterval(int refreshInterval) {
		this.refreshInterval = refreshInterval;
	}
}
