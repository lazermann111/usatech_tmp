package com.usatech.transport.soap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;

import com.usatech.transport.TransportErrorType;
import com.usatech.transport.TransportException;
import com.usatech.transport.TransportReason;
import com.usatech.transport.Transporter;
import com.usatech.transport.soap.compass.GenericWebServiceEndPoint;
import com.usatech.transport.soap.compass.GenericWebServiceEndPointServiceLocator;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.IOUtils;
import simple.io.resource.Resource;
import simple.text.StringUtils;

public class CompassSOAPTransporter implements Transporter {
	protected static final String SUBMITTER_ID = "USATECH_IVEND_10001";
	protected static final AtomicInteger MESSAGE_ID_SUFFIX = new AtomicInteger();
	protected static final char[] MESSAGE_ID_CS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    public String transport(Map<String,? extends Object> transportProperties,
			TransportReason transportReason, Resource resource,
			Map<String, ? extends Object> messageParameters) throws TransportException {
    	String endpointURL, username, password;
    	URL endpoint;
    	try{
	        endpointURL = ConvertUtils.getString(transportProperties.get("URL"), false);
			if(endpointURL == null)
				throw new TransportException("URL transport property is null", TransportErrorType.INSTANCE_ERR);


	        try {
	            endpoint = new URL(endpointURL);
	        } catch(MalformedURLException exc) {
	            throw new TransportException("MalformedURLException while creating URL object from "
	                    + endpointURL, exc, TransportErrorType.INSTANCE_ERR);
	        }

			username = ConvertUtils.getString(transportProperties.get("USERNAME"), false);
			if(username == null)
				username = "";

			password = ConvertUtils.getString(transportProperties.get("PASSWORD"), false);
			if(password == null)
				password = "";
    	}
    	catch(ConvertException e){
    		throw new TransportException(
					"ConvertException while converting String", e, TransportErrorType.INSTANCE_ERR);
    	}
		String messageId = String.valueOf(System.currentTimeMillis()) + StringUtils.asCharacters(MESSAGE_ID_SUFFIX.incrementAndGet(), MESSAGE_ID_CS);

		GenericWebServiceEndPointServiceLocator locator = new GenericWebServiceEndPointServiceLocator();

		GenericWebServiceEndPoint webService;
		try {
			webService = locator.getsubmitMessage(endpoint);
		} catch(ServiceException exc) {
			throw new TransportException("ServiceException while connecting to " + endpointURL,
					exc, TransportErrorType.TRANSPORT_ERR);
		}
		if(!(webService instanceof Stub))
			throw new TransportException("Can not set username and password because webService is not an instance of Stub",TransportErrorType.TRANSPORT_ERR);
		((Stub)webService).setUsername(username);
		((Stub)webService).setPassword(password);

		//set timeout to 0 for configuration properties to take effect
		((Stub)webService).setTimeout(0);
		
		boolean importResult;
		try {
			importResult = webService.submitMessage(SUBMITTER_ID, messageId, resource.getContentType(), resource.getName(), IOUtils.readFully(resource.getInputStream()));
		} catch(RemoteException re) {
			throw new TransportException("RemoteException while sending file", re, TransportErrorType.TRANSPORT_ERR);
		} catch(IOException e) {
			throw new TransportException("IOException while reading file", e, TransportErrorType.INSTANCE_ERR);
		}

        return endpointURL + "; RESULT=" + (importResult ? "Success" : "Failure");
	}
}
