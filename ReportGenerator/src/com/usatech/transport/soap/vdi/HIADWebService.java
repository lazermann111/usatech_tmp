/**
 * HIADWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.vdi;

public interface HIADWebService extends javax.xml.rpc.Service {
    public java.lang.String getHIADWebServiceSoapAddress();

    public com.usatech.transport.soap.vdi.HIADWebServiceSoap getHIADWebServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.vdi.HIADWebServiceSoap getHIADWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getHIADWebServiceSoap12Address();

    public com.usatech.transport.soap.vdi.HIADWebServiceSoap getHIADWebServiceSoap12() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.vdi.HIADWebServiceSoap getHIADWebServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
