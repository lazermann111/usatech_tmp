package com.usatech.transport.soap.vdi;

public class HIADWebServiceSoapProxy implements com.usatech.transport.soap.vdi.HIADWebServiceSoap {
  private String _endpoint = null;
  private com.usatech.transport.soap.vdi.HIADWebServiceSoap hIADWebServiceSoap = null;
  
  public HIADWebServiceSoapProxy() {
    _initHIADWebServiceSoapProxy();
  }
  
  public HIADWebServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initHIADWebServiceSoapProxy();
  }
  
  private void _initHIADWebServiceSoapProxy() {
    try {
      hIADWebServiceSoap = (new com.usatech.transport.soap.vdi.HIADWebServiceLocator()).getHIADWebServiceSoap();
      if (hIADWebServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)hIADWebServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)hIADWebServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (hIADWebServiceSoap != null)
      ((javax.xml.rpc.Stub)hIADWebServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.usatech.transport.soap.vdi.HIADWebServiceSoap getHIADWebServiceSoap() {
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap;
  }
  
  public java.lang.String getDex(java.lang.String transactionId, java.lang.String providerId, java.lang.String customerId, java.lang.String applicationId, java.lang.String applicationVersion, java.lang.String[] deviceList, java.lang.String[] outletList, java.util.Calendar onOrAfter, java.util.Calendar onOrBefore, java.lang.String returnSet, java.lang.String userData, java.lang.String vdiXmlVersion) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.getDex(transactionId, providerId, customerId, applicationId, applicationVersion, deviceList, outletList, onOrAfter, onOrBefore, returnSet, userData, vdiXmlVersion);
  }
  
  public java.lang.String uploadDex(java.lang.String transactionId, java.lang.String providerId, java.lang.String customerId, java.lang.String applicationId, java.lang.String applicationVersion, int dexEncoding, java.lang.String dexCompressionType, java.lang.String dexCompressionParam, java.lang.String userData, java.lang.String vdiXmlVersion, java.lang.String vdiXml) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.uploadDex(transactionId, providerId, customerId, applicationId, applicationVersion, dexEncoding, dexCompressionType, dexCompressionParam, userData, vdiXmlVersion, vdiXml);
  }
  
  public void testCommStatusReport() throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    hIADWebServiceSoap.testCommStatusReport();
  }
  
  public java.lang.String ping(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.ping(arg0, arg1, arg2);
  }
  
  public java.lang.String dex(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6, java.lang.String arg7) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.dex(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
  }
  
  public java.lang.String getLastDEX(java.lang.String remoteId) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.getLastDEX(remoteId);
  }
  
  public java.lang.String firmwareUpgradeCheck(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.firmwareUpgradeCheck(arg0, arg1, arg2, arg3, arg4);
  }
  
  public java.lang.String fileTransfer(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.fileTransfer(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
  }
  
  public java.lang.String assignRules(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.assignRules(arg0, arg1, arg2, arg3);
  }
  
  public java.lang.String installOptionsRev3(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.installOptionsRev3(arg0, arg1, arg2, arg3, arg4);
  }
  
  public java.lang.String assignValidate(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.assignValidate(arg0, arg1, arg2, arg3, arg4, arg5);
  }
  
  public java.lang.String assignGetPosInfo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.assignGetPosInfo(arg0, arg1, arg2, arg3);
  }
  
  public java.lang.String assign(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.assign(arg0, arg1, arg2, arg3, arg4);
  }
  
  public java.lang.String unassign(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.unassign(arg0, arg1, arg2, arg3, arg4, arg5);
  }
  
  public java.lang.String getTimeZoneOffset(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.getTimeZoneOffset(arg0, arg1, arg2, arg3, arg4);
  }
  
  public java.lang.String config(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.config(arg0, arg1, arg2, arg3);
  }
  
  public java.lang.String HIADReport(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6, java.lang.String arg7, java.lang.String arg8) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.HIADReport(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  }
  
  public java.lang.String cashlessKey() throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.cashlessKey();
  }
  
  public java.lang.String cashlessPayload() throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.cashlessPayload();
  }
  
  public java.lang.String cashlessStatistics() throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.cashlessStatistics();
  }
  
  public java.lang.String cashlessTotalizer() throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.cashlessTotalizer();
  }
  
  public java.lang.String refillSession(java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.refillSession(arg1, arg2, arg3);
  }
  
  public int saveDEXFile(java.lang.String sRemoteId, byte[] byteDEXFile, java.lang.String sReadDateTime, java.lang.String sCompressionType, java.lang.String sCompressionVersion, int iCompressedSize, int iUncompressedSize, int iCrcCompressed, int iCrcUnCompressed) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.saveDEXFile(sRemoteId, byteDEXFile, sReadDateTime, sCompressionType, sCompressionVersion, iCompressedSize, iUncompressedSize, iCrcCompressed, iCrcUnCompressed);
  }
  
  public com.usatech.transport.soap.vdi.RemoteDEXReturn getDeviceData(java.lang.String username, java.lang.String password, java.lang.String[] deviceList, java.util.Calendar currentDateTime, int iSeconds, com.usatech.transport.soap.vdi.holders.ArrayOfDEXDataCompressedHolder devData, java.lang.String encryptionDLLVersion, boolean lastOnly) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.getDeviceData(username, password, deviceList, currentDateTime, iSeconds, devData, encryptionDLLVersion, lastOnly);
  }
  
  public int updateRetrieveStatus(java.lang.String sDexIdArray, int iStatus) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.updateRetrieveStatus(sDexIdArray, iStatus);
  }
  
  public com.usatech.transport.soap.vdi.CMSReturn getAlarmsForCustomer(java.lang.String username, java.lang.String password, java.lang.String[] deviceList, java.util.Calendar currentDateTime, int iSeconds, com.usatech.transport.soap.vdi.holders.ArrayOfEventAlarmExHolder events) throws java.rmi.RemoteException{
    if (hIADWebServiceSoap == null)
      _initHIADWebServiceSoapProxy();
    return hIADWebServiceSoap.getAlarmsForCustomer(username, password, deviceList, currentDateTime, iSeconds, events);
  }
  
  
}