/**
 * ArrayOfEventAlarmExHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.vdi.holders;

public final class ArrayOfEventAlarmExHolder implements javax.xml.rpc.holders.Holder {
    public com.usatech.transport.soap.vdi.EventAlarmEx[] value;

    public ArrayOfEventAlarmExHolder() {
    }

    public ArrayOfEventAlarmExHolder(com.usatech.transport.soap.vdi.EventAlarmEx[] value) {
        this.value = value;
    }

}
