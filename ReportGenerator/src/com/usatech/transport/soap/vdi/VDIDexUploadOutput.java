/**
 *
 */
package com.usatech.transport.soap.vdi;

/**
 * @author Brian S. Krug
 *
 */
public class VDIDexUploadOutput {
	protected int resultCode;
	protected String resultMessage;
	public int getResultCode() {
		return resultCode;
	}
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
}
