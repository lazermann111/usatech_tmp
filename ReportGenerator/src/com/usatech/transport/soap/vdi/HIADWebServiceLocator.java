/**
 * HIADWebServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.vdi;

public class HIADWebServiceLocator extends org.apache.axis.client.Service implements com.usatech.transport.soap.vdi.HIADWebService {

    public HIADWebServiceLocator() {
    }


    public HIADWebServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public HIADWebServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for HIADWebServiceSoap
    private java.lang.String HIADWebServiceSoap_address = "http://streamwareonline.com:8081/WANServiceSB.asmx";

    public java.lang.String getHIADWebServiceSoapAddress() {
        return HIADWebServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String HIADWebServiceSoapWSDDServiceName = "HIADWebServiceSoap";

    public java.lang.String getHIADWebServiceSoapWSDDServiceName() {
        return HIADWebServiceSoapWSDDServiceName;
    }

    public void setHIADWebServiceSoapWSDDServiceName(java.lang.String name) {
        HIADWebServiceSoapWSDDServiceName = name;
    }

    public com.usatech.transport.soap.vdi.HIADWebServiceSoap getHIADWebServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(HIADWebServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getHIADWebServiceSoap(endpoint);
    }

    public com.usatech.transport.soap.vdi.HIADWebServiceSoap getHIADWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.vdi.HIADWebServiceSoapStub _stub = new com.usatech.transport.soap.vdi.HIADWebServiceSoapStub(portAddress, this);
            _stub.setPortName(getHIADWebServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setHIADWebServiceSoapEndpointAddress(java.lang.String address) {
        HIADWebServiceSoap_address = address;
    }


    // Use to get a proxy class for HIADWebServiceSoap12
    private java.lang.String HIADWebServiceSoap12_address = "http://streamwareonline.com:8081/WANServiceSB.asmx";

    public java.lang.String getHIADWebServiceSoap12Address() {
        return HIADWebServiceSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String HIADWebServiceSoap12WSDDServiceName = "HIADWebServiceSoap12";

    public java.lang.String getHIADWebServiceSoap12WSDDServiceName() {
        return HIADWebServiceSoap12WSDDServiceName;
    }

    public void setHIADWebServiceSoap12WSDDServiceName(java.lang.String name) {
        HIADWebServiceSoap12WSDDServiceName = name;
    }

    public com.usatech.transport.soap.vdi.HIADWebServiceSoap getHIADWebServiceSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(HIADWebServiceSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getHIADWebServiceSoap12(endpoint);
    }

    public com.usatech.transport.soap.vdi.HIADWebServiceSoap getHIADWebServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.vdi.HIADWebServiceSoap12Stub _stub = new com.usatech.transport.soap.vdi.HIADWebServiceSoap12Stub(portAddress, this);
            _stub.setPortName(getHIADWebServiceSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setHIADWebServiceSoap12EndpointAddress(java.lang.String address) {
        HIADWebServiceSoap12_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.transport.soap.vdi.HIADWebServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.vdi.HIADWebServiceSoapStub _stub = new com.usatech.transport.soap.vdi.HIADWebServiceSoapStub(new java.net.URL(HIADWebServiceSoap_address), this);
                _stub.setPortName(getHIADWebServiceSoapWSDDServiceName());
                return _stub;
            }
            if (com.usatech.transport.soap.vdi.HIADWebServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.vdi.HIADWebServiceSoap12Stub _stub = new com.usatech.transport.soap.vdi.HIADWebServiceSoap12Stub(new java.net.URL(HIADWebServiceSoap12_address), this);
                _stub.setPortName(getHIADWebServiceSoap12WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("HIADWebServiceSoap".equals(inputPortName)) {
            return getHIADWebServiceSoap();
        }
        else if ("HIADWebServiceSoap12".equals(inputPortName)) {
            return getHIADWebServiceSoap12();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:WANServiceSB", "HIADWebService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:WANServiceSB", "HIADWebServiceSoap"));
            ports.add(new javax.xml.namespace.QName("urn:WANServiceSB", "HIADWebServiceSoap12"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("HIADWebServiceSoap".equals(portName)) {
            setHIADWebServiceSoapEndpointAddress(address);
        }
        else 
if ("HIADWebServiceSoap12".equals(portName)) {
            setHIADWebServiceSoap12EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
