<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:b="http://simple/bean/1.0"
	exclude-result-prefixes="b">
	<xsl:output method="xml"/>
	<xsl:template match="/b:info">
		<VDITransaction VDIXMLVersion="{b:xmlVersion}" TransactionReason="UploadDEX" TransactionID="{b:transactionId}" TransactionTime="{b:currentLocalTime}"
			ProviderID="{b:providerId}" CustomerID="{b:customerId}" ApplicationID="{b:applicationId}" ApplicationVersion="{b:applicationVersion}">
			<DEXList RecordsCount="1" DEXEncoding="{b:dexEncoding}" DEXCompressionType="{b:dexCompressionType}" DEXCompressionParam="{b:dexCompressionParam}">
				<DexTransmission DeviceID="{b:deviceSerialCd}" TransmitTime="{b:currentLocalTime}" GMTOffSet="{b:currentTimeOffset}">
					<DexCollection RecordsCount="1">
						<DEX ReadDateTime="{b:dexLocalTime}" GMTOffSet="{b:dexTimeOffset}" FileSize="{b:dexFileSize}" DexReason="{b:dexReason}" DexType="0"
							ResponseCode="OK">
							<RawDEX>
								<xsl:value-of select="b:dexFileContents" disable-output-escaping="yes" />
							</RawDEX>
							<UserData></UserData>
						</DEX>
						<UserData></UserData>
					</DexCollection>
					<UserData></UserData>
				</DexTransmission>
				<UserData></UserData>
			</DEXList>
			<OtherCollectionsOrLists></OtherCollectionsOrLists>
			<UserData></UserData>
		</VDITransaction>
	</xsl:template>
</xsl:stylesheet>