/**
 *
 */
package com.usatech.transport.soap.vdi;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import simple.io.resource.Resource;
import simple.text.MinimalCDataCharset;
import simple.text.ThreadSafeDateFormat;

/**
 * @author Brian S. Krug
 *
 */
public class VDIDexUploadInput {
	protected static final DateFormat timestampFormat = new ThreadSafeDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
	public static final MinimalCDataCharset cdataCharset = new MinimalCDataCharset();
	protected String customerId;
	protected String transactionId;
	protected String xmlVersion;
	protected String providerId;
	protected String applicationId;
	protected String applicationVersion;
	protected int dexEncoding;
	protected String dexCompressionType;
	protected String dexCompressionParam;
	protected String deviceSerialCd;
	protected int dexReason;

	protected Resource dexFile;
	protected Date dexDate;
	protected TimeZone timeZone;
	protected final Calendar currentCalendar = Calendar.getInstance();

	public String getCustomerId() {
		return customerId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public String getXmlVersion() {
		return xmlVersion;
	}
	public String getProviderId() {
		return providerId;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public String getApplicationVersion() {
		return applicationVersion;
	}
	public int getDexEncoding() {
		return dexEncoding;
	}
	public String getDexCompressionType() {
		return dexCompressionType;
	}
	public String getDexCompressionParam() {
		return dexCompressionParam;
	}
	public String getDeviceSerialCd() {
		return deviceSerialCd;
	}
	public String getCurrentLocalTime() {
		return timestampFormat.format(currentCalendar.getTime());
	}
	public int getCurrentTimeOffset() {
		return currentCalendar.getTimeZone().getOffset(currentCalendar.getTimeInMillis()) / (60*60*1000); // its in hours
	}
	public String getDexLocalTime() {
		if(dexDate!=null){
			return timestampFormat.format(dexDate);
		}else{
			return null;
		}
	}
	public int getDexTimeOffset() {
		if(dexDate!=null){
			return timeZone.getOffset(dexDate.getTime()) / (60*60*1000); // its in hours
		}else{
			return -1;
		}
	}
	public long getDexFileSize() {
		return dexFile.getLength();
	}
	public int getDexReason() {
		return dexReason;
	}
	public Reader getDexFileContents() throws IOException {
		return new InputStreamReader(dexFile.getInputStream(), getCharsetForDexEncoding(dexEncoding));
	}
	/**
	 * @param dexEncoding2
	 * @return
	 */
	public static Charset getCharsetForDexEncoding(int dexEncoding) {
		switch(dexEncoding) {
			case 1: return cdataCharset; //Charset.forName("ISO8859-1");
			case 2: return Charset.forName("UTF-8");
			default: return Charset.defaultCharset();
		}
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public void setXmlVersion(String xmlVersion) {
		this.xmlVersion = xmlVersion;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}
	public void setDexEncoding(int dexEncoding) {
		this.dexEncoding = dexEncoding;
	}
	public void setDexCompressionType(String dexCompressionType) {
		this.dexCompressionType = dexCompressionType;
	}
	public void setDexCompressionParam(String dexCompressionParam) {
		this.dexCompressionParam = dexCompressionParam;
	}
	public void setDeviceSerialCd(String deviceSerialCd) {
		this.deviceSerialCd = deviceSerialCd;
	}
	public void setDexReason(int dexReason) {
		this.dexReason = dexReason;
	}

	// used indirectly
	Resource getDexFile() {
		return dexFile;
	}
	public void setDexFile(Resource dexFile) {
		this.dexFile = dexFile;
	}
	Date getDexDate() {
		return dexDate;
	}
	public void setDexDate(Date dexDate) {
		this.dexDate = dexDate;
	}
	TimeZone getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}
}
