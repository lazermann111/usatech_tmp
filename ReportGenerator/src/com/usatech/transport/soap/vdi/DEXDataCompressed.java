/**
 * DEXDataCompressed.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.vdi;

public class DEXDataCompressed  implements java.io.Serializable {
    private java.util.Calendar dateTime;

    private java.lang.String device;

    private int id;

    private int compType;

    private int sizeCompressed;

    private int CRCCompressed;

    private int sizeUncompressed;

    private int CRCUncompressed;

    private byte[] data;

    public DEXDataCompressed() {
    }

    public DEXDataCompressed(
           java.util.Calendar dateTime,
           java.lang.String device,
           int id,
           int compType,
           int sizeCompressed,
           int CRCCompressed,
           int sizeUncompressed,
           int CRCUncompressed,
           byte[] data) {
           this.dateTime = dateTime;
           this.device = device;
           this.id = id;
           this.compType = compType;
           this.sizeCompressed = sizeCompressed;
           this.CRCCompressed = CRCCompressed;
           this.sizeUncompressed = sizeUncompressed;
           this.CRCUncompressed = CRCUncompressed;
           this.data = data;
    }


    /**
     * Gets the dateTime value for this DEXDataCompressed.
     * 
     * @return dateTime
     */
    public java.util.Calendar getDateTime() {
        return dateTime;
    }


    /**
     * Sets the dateTime value for this DEXDataCompressed.
     * 
     * @param dateTime
     */
    public void setDateTime(java.util.Calendar dateTime) {
        this.dateTime = dateTime;
    }


    /**
     * Gets the device value for this DEXDataCompressed.
     * 
     * @return device
     */
    public java.lang.String getDevice() {
        return device;
    }


    /**
     * Sets the device value for this DEXDataCompressed.
     * 
     * @param device
     */
    public void setDevice(java.lang.String device) {
        this.device = device;
    }


    /**
     * Gets the id value for this DEXDataCompressed.
     * 
     * @return id
     */
    public int getId() {
        return id;
    }


    /**
     * Sets the id value for this DEXDataCompressed.
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Gets the compType value for this DEXDataCompressed.
     * 
     * @return compType
     */
    public int getCompType() {
        return compType;
    }


    /**
     * Sets the compType value for this DEXDataCompressed.
     * 
     * @param compType
     */
    public void setCompType(int compType) {
        this.compType = compType;
    }


    /**
     * Gets the sizeCompressed value for this DEXDataCompressed.
     * 
     * @return sizeCompressed
     */
    public int getSizeCompressed() {
        return sizeCompressed;
    }


    /**
     * Sets the sizeCompressed value for this DEXDataCompressed.
     * 
     * @param sizeCompressed
     */
    public void setSizeCompressed(int sizeCompressed) {
        this.sizeCompressed = sizeCompressed;
    }


    /**
     * Gets the CRCCompressed value for this DEXDataCompressed.
     * 
     * @return CRCCompressed
     */
    public int getCRCCompressed() {
        return CRCCompressed;
    }


    /**
     * Sets the CRCCompressed value for this DEXDataCompressed.
     * 
     * @param CRCCompressed
     */
    public void setCRCCompressed(int CRCCompressed) {
        this.CRCCompressed = CRCCompressed;
    }


    /**
     * Gets the sizeUncompressed value for this DEXDataCompressed.
     * 
     * @return sizeUncompressed
     */
    public int getSizeUncompressed() {
        return sizeUncompressed;
    }


    /**
     * Sets the sizeUncompressed value for this DEXDataCompressed.
     * 
     * @param sizeUncompressed
     */
    public void setSizeUncompressed(int sizeUncompressed) {
        this.sizeUncompressed = sizeUncompressed;
    }


    /**
     * Gets the CRCUncompressed value for this DEXDataCompressed.
     * 
     * @return CRCUncompressed
     */
    public int getCRCUncompressed() {
        return CRCUncompressed;
    }


    /**
     * Sets the CRCUncompressed value for this DEXDataCompressed.
     * 
     * @param CRCUncompressed
     */
    public void setCRCUncompressed(int CRCUncompressed) {
        this.CRCUncompressed = CRCUncompressed;
    }


    /**
     * Gets the data value for this DEXDataCompressed.
     * 
     * @return data
     */
    public byte[] getData() {
        return data;
    }


    /**
     * Sets the data value for this DEXDataCompressed.
     * 
     * @param data
     */
    public void setData(byte[] data) {
        this.data = data;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DEXDataCompressed)) return false;
        DEXDataCompressed other = (DEXDataCompressed) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dateTime==null && other.getDateTime()==null) || 
             (this.dateTime!=null &&
              this.dateTime.equals(other.getDateTime()))) &&
            ((this.device==null && other.getDevice()==null) || 
             (this.device!=null &&
              this.device.equals(other.getDevice()))) &&
            this.id == other.getId() &&
            this.compType == other.getCompType() &&
            this.sizeCompressed == other.getSizeCompressed() &&
            this.CRCCompressed == other.getCRCCompressed() &&
            this.sizeUncompressed == other.getSizeUncompressed() &&
            this.CRCUncompressed == other.getCRCUncompressed() &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              java.util.Arrays.equals(this.data, other.getData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDateTime() != null) {
            _hashCode += getDateTime().hashCode();
        }
        if (getDevice() != null) {
            _hashCode += getDevice().hashCode();
        }
        _hashCode += getId();
        _hashCode += getCompType();
        _hashCode += getSizeCompressed();
        _hashCode += getCRCCompressed();
        _hashCode += getSizeUncompressed();
        _hashCode += getCRCUncompressed();
        if (getData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DEXDataCompressed.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:WANServiceSB", "DEXDataCompressed"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:WANServiceSB", "DateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("device");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:WANServiceSB", "Device"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:WANServiceSB", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("compType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:WANServiceSB", "compType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sizeCompressed");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:WANServiceSB", "SizeCompressed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CRCCompressed");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:WANServiceSB", "CRCCompressed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sizeUncompressed");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:WANServiceSB", "SizeUncompressed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CRCUncompressed");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:WANServiceSB", "CRCUncompressed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:WANServiceSB", "Data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
