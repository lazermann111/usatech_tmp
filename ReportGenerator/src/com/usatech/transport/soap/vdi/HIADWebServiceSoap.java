/**
 * HIADWebServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.vdi;

public interface HIADWebServiceSoap extends java.rmi.Remote {
    public java.lang.String getDex(java.lang.String transactionId, java.lang.String providerId, java.lang.String customerId, java.lang.String applicationId, java.lang.String applicationVersion, java.lang.String[] deviceList, java.lang.String[] outletList, java.util.Calendar onOrAfter, java.util.Calendar onOrBefore, java.lang.String returnSet, java.lang.String userData, java.lang.String vdiXmlVersion) throws java.rmi.RemoteException;
    public java.lang.String uploadDex(java.lang.String transactionId, java.lang.String providerId, java.lang.String customerId, java.lang.String applicationId, java.lang.String applicationVersion, int dexEncoding, java.lang.String dexCompressionType, java.lang.String dexCompressionParam, java.lang.String userData, java.lang.String vdiXmlVersion, java.lang.String vdiXml) throws java.rmi.RemoteException;
    public void testCommStatusReport() throws java.rmi.RemoteException;
    public java.lang.String ping(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public java.lang.String dex(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6, java.lang.String arg7) throws java.rmi.RemoteException;
    public java.lang.String getLastDEX(java.lang.String remoteId) throws java.rmi.RemoteException;
    public java.lang.String firmwareUpgradeCheck(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws java.rmi.RemoteException;
    public java.lang.String fileTransfer(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6) throws java.rmi.RemoteException;
    public java.lang.String assignRules(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public java.lang.String installOptionsRev3(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws java.rmi.RemoteException;
    public java.lang.String assignValidate(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5) throws java.rmi.RemoteException;
    public java.lang.String assignGetPosInfo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public java.lang.String assign(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws java.rmi.RemoteException;
    public java.lang.String unassign(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5) throws java.rmi.RemoteException;
    public java.lang.String getTimeZoneOffset(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4) throws java.rmi.RemoteException;
    public java.lang.String config(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public java.lang.String HIADReport(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6, java.lang.String arg7, java.lang.String arg8) throws java.rmi.RemoteException;
    public java.lang.String cashlessKey() throws java.rmi.RemoteException;
    public java.lang.String cashlessPayload() throws java.rmi.RemoteException;
    public java.lang.String cashlessStatistics() throws java.rmi.RemoteException;
    public java.lang.String cashlessTotalizer() throws java.rmi.RemoteException;
    public java.lang.String refillSession(java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public int saveDEXFile(java.lang.String sRemoteId, byte[] byteDEXFile, java.lang.String sReadDateTime, java.lang.String sCompressionType, java.lang.String sCompressionVersion, int iCompressedSize, int iUncompressedSize, int iCrcCompressed, int iCrcUnCompressed) throws java.rmi.RemoteException;
    public com.usatech.transport.soap.vdi.RemoteDEXReturn getDeviceData(java.lang.String username, java.lang.String password, java.lang.String[] deviceList, java.util.Calendar currentDateTime, int iSeconds, com.usatech.transport.soap.vdi.holders.ArrayOfDEXDataCompressedHolder devData, java.lang.String encryptionDLLVersion, boolean lastOnly) throws java.rmi.RemoteException;
    public int updateRetrieveStatus(java.lang.String sDexIdArray, int iStatus) throws java.rmi.RemoteException;
    public com.usatech.transport.soap.vdi.CMSReturn getAlarmsForCustomer(java.lang.String username, java.lang.String password, java.lang.String[] deviceList, java.util.Calendar currentDateTime, int iSeconds, com.usatech.transport.soap.vdi.holders.ArrayOfEventAlarmExHolder events) throws java.rmi.RemoteException;
}
