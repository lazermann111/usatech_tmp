/**
 * VDIUploadDEX.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.easitrax;

public interface VDIUploadDEX extends javax.xml.rpc.Service {
    public java.lang.String getVDIUploadDEXSoap12Address();

    public com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType getVDIUploadDEXSoap12() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType getVDIUploadDEXSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getVDIUploadDEXSoapAddress();

    public com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType getVDIUploadDEXSoap() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType getVDIUploadDEXSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
