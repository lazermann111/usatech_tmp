/**
 * VDIUploadDEXLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.easitrax;

public class VDIUploadDEXLocator extends org.apache.axis.client.Service implements com.usatech.transport.soap.easitrax.VDIUploadDEX {

    public VDIUploadDEXLocator() {
    }


    public VDIUploadDEXLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public VDIUploadDEXLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for VDIUploadDEXSoap12
    private java.lang.String VDIUploadDEXSoap12_address = "http://199.244.200.60:8006/VDIUploadDEX.asmx";

    public java.lang.String getVDIUploadDEXSoap12Address() {
        return VDIUploadDEXSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String VDIUploadDEXSoap12WSDDServiceName = "VDIUploadDEXSoap12";

    public java.lang.String getVDIUploadDEXSoap12WSDDServiceName() {
        return VDIUploadDEXSoap12WSDDServiceName;
    }

    public void setVDIUploadDEXSoap12WSDDServiceName(java.lang.String name) {
        VDIUploadDEXSoap12WSDDServiceName = name;
    }

    public com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType getVDIUploadDEXSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(VDIUploadDEXSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getVDIUploadDEXSoap12(endpoint);
    }

    public com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType getVDIUploadDEXSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.easitrax.VDIUploadDEXSoap12Stub _stub = new com.usatech.transport.soap.easitrax.VDIUploadDEXSoap12Stub(portAddress, this);
            _stub.setPortName(getVDIUploadDEXSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setVDIUploadDEXSoap12EndpointAddress(java.lang.String address) {
        VDIUploadDEXSoap12_address = address;
    }


    // Use to get a proxy class for VDIUploadDEXSoap
    private java.lang.String VDIUploadDEXSoap_address = "http://199.244.200.60:8006/VDIUploadDEX.asmx";

    public java.lang.String getVDIUploadDEXSoapAddress() {
        return VDIUploadDEXSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String VDIUploadDEXSoapWSDDServiceName = "VDIUploadDEXSoap";

    public java.lang.String getVDIUploadDEXSoapWSDDServiceName() {
        return VDIUploadDEXSoapWSDDServiceName;
    }

    public void setVDIUploadDEXSoapWSDDServiceName(java.lang.String name) {
        VDIUploadDEXSoapWSDDServiceName = name;
    }

    public com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType getVDIUploadDEXSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(VDIUploadDEXSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getVDIUploadDEXSoap(endpoint);
    }

    public com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType getVDIUploadDEXSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_BindingStub _stub = new com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_BindingStub(portAddress, this);
            _stub.setPortName(getVDIUploadDEXSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setVDIUploadDEXSoapEndpointAddress(java.lang.String address) {
        VDIUploadDEXSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.easitrax.VDIUploadDEXSoap12Stub _stub = new com.usatech.transport.soap.easitrax.VDIUploadDEXSoap12Stub(new java.net.URL(VDIUploadDEXSoap12_address), this);
                _stub.setPortName(getVDIUploadDEXSoap12WSDDServiceName());
                return _stub;
            }
            if (com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_BindingStub _stub = new com.usatech.transport.soap.easitrax.VDIUploadDEXSoap_BindingStub(new java.net.URL(VDIUploadDEXSoap_address), this);
                _stub.setPortName(getVDIUploadDEXSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("VDIUploadDEXSoap12".equals(inputPortName)) {
            return getVDIUploadDEXSoap12();
        }
        else if ("VDIUploadDEXSoap".equals(inputPortName)) {
            return getVDIUploadDEXSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:WANServiceSB", "VDIUploadDEX");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:WANServiceSB", "VDIUploadDEXSoap12"));
            ports.add(new javax.xml.namespace.QName("urn:WANServiceSB", "VDIUploadDEXSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("VDIUploadDEXSoap12".equals(portName)) {
            setVDIUploadDEXSoap12EndpointAddress(address);
        }
        else 
if ("VDIUploadDEXSoap".equals(portName)) {
            setVDIUploadDEXSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
