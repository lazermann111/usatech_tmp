/**
 * VDIUploadDEXSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.easitrax;

public interface VDIUploadDEXSoap_PortType extends java.rmi.Remote {

    /**
     * Validate DEX XML, De-serialize it, store into database and
     * Return the ACK
     */
    public java.lang.String uploadDex(java.lang.String transactionId, java.lang.String providerId, java.lang.String customerId, java.lang.String applicationId, java.lang.String applicationVersion, int dexEncoding, java.lang.String dexCompressionType, java.lang.String dexCompressionParam, java.lang.String userData, java.lang.String vdiXmlVersion, java.lang.String vdiXml) throws java.rmi.RemoteException;
}
