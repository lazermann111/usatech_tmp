/**
 * InteractionLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.cantaloupe;

public class InteractionLocator extends org.apache.axis.client.Service implements com.usatech.transport.soap.cantaloupe.Interaction {

    public InteractionLocator() {
    }


    public InteractionLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public InteractionLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for InteractionSoap12
    private java.lang.String InteractionSoap12_address = "http://www.mycantaloupe.com/service/Interaction.asmx";

    public java.lang.String getInteractionSoap12Address() {
        return InteractionSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String InteractionSoap12WSDDServiceName = "InteractionSoap12";

    public java.lang.String getInteractionSoap12WSDDServiceName() {
        return InteractionSoap12WSDDServiceName;
    }

    public void setInteractionSoap12WSDDServiceName(java.lang.String name) {
        InteractionSoap12WSDDServiceName = name;
    }

    public com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType getInteractionSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(InteractionSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getInteractionSoap12(endpoint);
    }

    public com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType getInteractionSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.cantaloupe.InteractionSoap12Stub _stub = new com.usatech.transport.soap.cantaloupe.InteractionSoap12Stub(portAddress, this);
            _stub.setPortName(getInteractionSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setInteractionSoap12EndpointAddress(java.lang.String address) {
        InteractionSoap12_address = address;
    }


    // Use to get a proxy class for InteractionSoap
    private java.lang.String InteractionSoap_address = "http://www.mycantaloupe.com/service/Interaction.asmx";

    public java.lang.String getInteractionSoapAddress() {
        return InteractionSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String InteractionSoapWSDDServiceName = "InteractionSoap";

    public java.lang.String getInteractionSoapWSDDServiceName() {
        return InteractionSoapWSDDServiceName;
    }

    public void setInteractionSoapWSDDServiceName(java.lang.String name) {
        InteractionSoapWSDDServiceName = name;
    }

    public com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType getInteractionSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(InteractionSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getInteractionSoap(endpoint);
    }

    public com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType getInteractionSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.usatech.transport.soap.cantaloupe.InteractionSoap_BindingStub _stub = new com.usatech.transport.soap.cantaloupe.InteractionSoap_BindingStub(portAddress, this);
            _stub.setPortName(getInteractionSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setInteractionSoapEndpointAddress(java.lang.String address) {
        InteractionSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.cantaloupe.InteractionSoap12Stub _stub = new com.usatech.transport.soap.cantaloupe.InteractionSoap12Stub(new java.net.URL(InteractionSoap12_address), this);
                _stub.setPortName(getInteractionSoap12WSDDServiceName());
                return _stub;
            }
            if (com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.usatech.transport.soap.cantaloupe.InteractionSoap_BindingStub _stub = new com.usatech.transport.soap.cantaloupe.InteractionSoap_BindingStub(new java.net.URL(InteractionSoap_address), this);
                _stub.setPortName(getInteractionSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("InteractionSoap12".equals(inputPortName)) {
            return getInteractionSoap12();
        }
        else if ("InteractionSoap".equals(inputPortName)) {
            return getInteractionSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://mycantaloupe.com/service", "Interaction");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://mycantaloupe.com/service", "InteractionSoap12"));
            ports.add(new javax.xml.namespace.QName("http://mycantaloupe.com/service", "InteractionSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("InteractionSoap12".equals(portName)) {
            setInteractionSoap12EndpointAddress(address);
        }
        else 
if ("InteractionSoap".equals(portName)) {
            setInteractionSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
