/**
 * RequestFastResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.cantaloupe;

public class RequestFastResponse  implements java.io.Serializable {
    private com.usatech.transport.soap.cantaloupe.RequestFastResponseRequestFastResult requestFastResult;

    public RequestFastResponse() {
    }

    public RequestFastResponse(
           com.usatech.transport.soap.cantaloupe.RequestFastResponseRequestFastResult requestFastResult) {
           this.requestFastResult = requestFastResult;
    }


    /**
     * Gets the requestFastResult value for this RequestFastResponse.
     * 
     * @return requestFastResult
     */
    public com.usatech.transport.soap.cantaloupe.RequestFastResponseRequestFastResult getRequestFastResult() {
        return requestFastResult;
    }


    /**
     * Sets the requestFastResult value for this RequestFastResponse.
     * 
     * @param requestFastResult
     */
    public void setRequestFastResult(com.usatech.transport.soap.cantaloupe.RequestFastResponseRequestFastResult requestFastResult) {
        this.requestFastResult = requestFastResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestFastResponse)) return false;
        RequestFastResponse other = (RequestFastResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.requestFastResult==null && other.getRequestFastResult()==null) || 
             (this.requestFastResult!=null &&
              this.requestFastResult.equals(other.getRequestFastResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRequestFastResult() != null) {
            _hashCode += getRequestFastResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestFastResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://mycantaloupe.com/service", ">RequestFastResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestFastResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mycantaloupe.com/service", "RequestFastResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://mycantaloupe.com/service", ">>RequestFastResponse>RequestFastResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
