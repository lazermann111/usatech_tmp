/**
 * InteractionSoap_BindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.cantaloupe;

public class InteractionSoap_BindingImpl implements com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType{
    public java.lang.String _import(java.lang.String username, java.lang.String password, org.apache.axis.types.UnsignedInt serialnumber, java.lang.String dexfile, boolean restock, java.util.Calendar dextime) throws java.rmi.RemoteException {
        return null;
    }

    public com.usatech.transport.soap.cantaloupe.RequestResponseRequestResult request(java.lang.String username, java.lang.String password, java.lang.String interactionXml) throws java.rmi.RemoteException {
        return null;
    }

    public com.usatech.transport.soap.cantaloupe.RequestFastResponseRequestFastResult requestFast(java.lang.String username, java.lang.String password, java.lang.String interactionXml) throws java.rmi.RemoteException {
        return null;
    }

}
