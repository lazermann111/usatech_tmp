/**
 * InteractionSoap12Impl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.cantaloupe;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InteractionSoap12Impl implements com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType{
    public java.lang.String _import(java.lang.String username, java.lang.String password, org.apache.axis.types.UnsignedInt serialnumber, java.lang.String dexfile, boolean restock, java.util.Calendar dextime) throws java.rmi.RemoteException {
        // this is for testing only
        try {
            FileWriter fw = new FileWriter(new File("Cantaloupe test DEX import " + System.currentTimeMillis() + ".log"));
            fw.write("username: " + username + "\n");
            fw.write("password: " + password + "\n");
            fw.write("serialnumber: " + serialnumber + "\n");
            fw.write("restock: " + restock + "\n");

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date dexDate = new Date(dextime.getTimeInMillis());

            fw.write("dextime: " + sdf.format(dexDate) + "\n");
            fw.write("dexfile: " + dexfile);
            fw.flush();
            fw.close();
        } catch (IOException e) {
                throw new java.rmi.RemoteException(e.getMessage());
        }
        return "Import successful";
    }

    public com.usatech.transport.soap.cantaloupe.RequestResponseRequestResult request(java.lang.String username, java.lang.String password, java.lang.String interactionXml) throws java.rmi.RemoteException {
        return null;
    }

    public com.usatech.transport.soap.cantaloupe.RequestFastResponseRequestFastResult requestFast(java.lang.String username, java.lang.String password, java.lang.String interactionXml) throws java.rmi.RemoteException {
        return null;
    }

}
