/**
 * Interaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.usatech.transport.soap.cantaloupe;

public interface Interaction extends javax.xml.rpc.Service {
    public java.lang.String getInteractionSoap12Address();

    public com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType getInteractionSoap12() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType getInteractionSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getInteractionSoapAddress();

    public com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType getInteractionSoap() throws javax.xml.rpc.ServiceException;

    public com.usatech.transport.soap.cantaloupe.InteractionSoap_PortType getInteractionSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
