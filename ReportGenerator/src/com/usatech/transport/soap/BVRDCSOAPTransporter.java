/** 
 * (C) USA Technologies, Inc. 2011
 * BVRDCSOAPTransporter.java by phorsfield, Sep 14, 2011 4:46:11 PM
 */
package com.usatech.transport.soap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;

import com.sun.mail.util.BASE64EncoderStream;
import com.usatech.transport.TransportErrorType;
import com.usatech.transport.TransportException;
import com.usatech.transport.TransportReason;
import com.usatech.transport.Transporter;
import com.usatech.transport.soap.bestvendor.WsMainLocator;
import com.usatech.transport.soap.bestvendor.WsMainSoap;
import com.usatech.transport.soap.bestvendor.WsMainSoapStub;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.resource.Resource;

/**
 * This transporter accepts RDC XML messages on usat.ccs.transport.8
 * 
 * It then zips the RDC message, base 64 encodes it, and uploads it to 
 * the remote Best Vendors RDC webservice.
 * 
 * A URL message property is required. The content of the XML is not
 * modified in anyway by this transporter.
 * 
 * @author phorsfield
 *
 */
public class BVRDCSOAPTransporter implements Transporter {
	// ======== Interface implementation

    public String transport(Map<String,? extends Object> transportProperties,
			TransportReason transportReason, Resource resource,
			Map<String, ? extends Object> messageParameters) throws TransportException {
    	String endpointURL, username, password;
    	URL endpoint;
    	try{
	        endpointURL = ConvertUtils.getString(transportProperties.get("URL"), false);
			if(endpointURL == null)
				throw new TransportException("URL transport property is null", TransportErrorType.INSTANCE_ERR);


	        try {
	            endpoint = new URL(endpointURL);
	        } catch(MalformedURLException exc) {
	            throw new TransportException("MalformedURLException while creating URL object from "
	                    + endpointURL, exc, TransportErrorType.INSTANCE_ERR);
	        }

			username = ConvertUtils.getString(transportProperties.get("USERNAME"), false);
			if(username == null)
				username = "";

			password = ConvertUtils.getString(transportProperties.get("PASSWORD"), false);
			if(password == null)
				password = "";
    	}
    	catch(ConvertException e){
    		throw new TransportException(
					"ConvertException while converting String", e, TransportErrorType.INSTANCE_ERR);
    	}
		
		WsMainLocator locator = new WsMainLocator();

		WsMainSoap webService;
		try {
			webService = locator.getwsMainSoap(endpoint);
		} catch(ServiceException exc) {
			throw new TransportException("ServiceException while connecting to " + endpointURL,
					exc, TransportErrorType.TRANSPORT_ERR);
		}
		if(!(webService instanceof WsMainSoapStub))
			throw new TransportException("Can not set username and password because webService is not an instance of Stub",TransportErrorType.TRANSPORT_ERR);
		((WsMainSoapStub)webService).setUsername(username);
		((WsMainSoapStub)webService).setPassword(password);

		//set timeout to 0 for configuration properties to take effect
		((Stub)webService).setTimeout(0);
		
		String importResult;
		try {
			byte[] message = ZipAndBase64( resource.getInputStream() );

			importResult = webService.inbound_RDC(new String(message, "US-ASCII"));
			
		} catch(RemoteException re) {
			throw new TransportException("RemoteException while sending file", re, TransportErrorType.TRANSPORT_ERR);
		} catch(IOException e) {
			throw new TransportException("IOException while reading file", e, TransportErrorType.INSTANCE_ERR);
		}

        return endpointURL + "; RESULT=" + (importResult != null ? "Success" : "Failure");
	}
    
    /** 
     * Modified to compress and base64 the file in memory
     * @param content
     * @return
     * @throws IOException
     */
	public static byte[] ZipAndBase64( InputStream clear ) throws IOException
	{		
		ByteArrayOutputStream buffer = new ByteArrayOutputStream(65536); // 64Kb initial buffer	
		BASE64EncoderStream b64 = new BASE64EncoderStream(buffer);
		ZipOutputStream compressed = new ZipOutputStream(b64);
		compressed.putNextEntry(new ZipEntry("usatrdc.xml"));		
		simple.io.IOUtils.copy(clear, compressed);	        
		compressed.closeEntry();
		compressed.close();
		clear.close();
	    return buffer.toByteArray();
	}
    
}
