package com.usatech.device;

import static simple.service.modem.dto.ModemApiCommand.GET_STATE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.mail.Email;
import simple.mail.Emailer;
import simple.service.modem.dao.eseye.EsEyeModemApi;
import simple.service.modem.dao.eseye.EsEyeModemApiImpl;
import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.dao.verizon.VerizonModemApi;
import simple.service.modem.dao.verizon.VerizonModemApiImpl;
import simple.service.modem.dto.ModemApiCommand;
import simple.service.modem.service.EsEyeModemService;
import simple.service.modem.service.ModemService;
import simple.service.modem.service.VerizonModemService;
import simple.service.modem.service.dto.DeviceOperationResult;
import simple.service.modem.service.dto.OperationStatus;
import simple.service.modem.service.dto.USATDevice;
import simple.util.ClassSerializer;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.CommonAttrEnum;

/**
 * Task for TRANSPORT SERVICE (!!!) to process device sim state update or read
 *
 * @author dlozenko
 */
public class ModemStatusProcessor implements MessageChainTask {
    static final Log log = Log.getLog();
    private String esEyeUsername = null;
    private String esEyePassword = null;
    private String esEyePortfolioId = null;
    private String verizonUsername = null;
    private String verizonPassword = null;
    private String verizonAppKey = null;
    private String verizonAppSecret = null;
    private String verizonBillingAccountName = null;
    private String verizonCallbackUrl = null;
    private String verizonDefaultServicePlan = null;
    private String verizonCDMAIPPool = null;
    private String verizonLTEIPPool = null;
    private final ModemService[] modemServices = new ModemService[2];
    private final ClassSerializer<DeviceOperationResult[]> resultsSerializer = new ClassSerializer<>();
    private final ClassSerializer<USATDevice[]> paramsDeserializer = new ClassSerializer<>();

    @Override
    public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
        ModemApiCommand command = null;
        USATDevice[] devices = null;
        try {
            instantiateServicesIfNeed();
            command = extractCommand(taskInfo);
            devices = extractDevices(taskInfo);
            USATDevice[][] devGroups = groupDevices(devices);
            for (USATDevice[] group : devGroups) {
                ModemService service = findSuitableService(group[0]);
                DeviceOperationResult[] results = null;
                results = processDeviceGroup(command, group, service, results);
             		forwardToResultsProcessor(results, taskInfo.getPublisher(), service, command);
            }
        } catch (Throwable t) {
            log.error("Unexpected error occurred: " + t.getMessage(), t);
            if (command != null && devices != null) {
                // emailErrorsToOwner(command, devices, t);
            }
        }
        return 0;
    }

		private DeviceOperationResult[] processDeviceGroup(ModemApiCommand command, USATDevice[] group,
				ModemService service, DeviceOperationResult[] results) {
			try {
			    switch (command) {
			        case ACTIVATE:
				        	if (service.getProviderType() == ModemApiProvider.VERIZON) {
		                for (USATDevice device : group) {
		                    if (device.getServicePlan() == null) {
		                        device.setServicePlan(verizonDefaultServicePlan);
		                    }
		                }
				        	}
			            results = service.activate(group);
			            break;
			        case DEACTIVATE:
			            results = service.deactivate(group);
			            break;
			        case SUSPEND:
			            results = service.suspend(group);
			            break;
			        case RESUME:
			            results = service.resume(group);
			            break;
			        case GET_STATE:
			            results = service.getCurrentState(group);
			            break;
			        default:
			            throw new IllegalArgumentException("Unknown API command: " + command.name());
			    }
			} catch (Exception e) {
					if (results == null && group != null && group.length > 0) {
						results = new DeviceOperationResult[] {new DeviceOperationResult()};
						results[0].setDevice(group[0]);
						results[0].setOperationStatus(OperationStatus.ERROR);
						results[0].setErrorString(e.getMessage());
					}
			}
			return results;
		}

    ModemApiCommand extractCommand(MessageChainTaskInfo taskInfo) throws AttributeConversionException {
        return taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_ITEM_ID, ModemApiCommand.class, true);
    }

    USATDevice[] extractDevices(MessageChainTaskInfo taskInfo) throws AttributeConversionException {
        return paramsDeserializer.fromBytes(taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_OBJECT, byte[].class, true));
    }

    USATDevice[][] groupDevices(USATDevice[] allDevices) {
        //TODO: this is simple grouping only for EsEye provider, for Verizon we should also group by modem type (2g,3g,4g), servicePlan, ZIP CODE, etc...
        if (allDevices.length == 0) {
            return new USATDevice[0][0];
        }
        int groupSize = 10;
        int lastGroupSize = allDevices.length % groupSize;
        int groups = allDevices.length / groupSize + (lastGroupSize == 0 ? 0 : 1);
        if (lastGroupSize == 0) {
            lastGroupSize = groupSize;
        }
        USATDevice[][] rv = new USATDevice[groups][];
        int group = 0;
        int item = 0;
        rv[group] = new USATDevice[group == groups - 1 ? lastGroupSize : groupSize];
        for (USATDevice dev : allDevices) {
            if (item >= groupSize) {
                item = 0;
                group++;
                rv[group] = new USATDevice[group == groups - 1 ? lastGroupSize : groupSize];
            }
            rv[group][item] = dev;
            item++;
        }
        return rv;
    }

    ModemService findSuitableService(USATDevice device) {
        for (ModemService srv : modemServices) {
            if (srv.isSuitableDevice(device)) {
                return srv;
            }
        }
        throw new IllegalStateException("This device can not be processed by no one of registered services. Device:\n" +
                device.toString() + ".");
    }

    void forwardToResultsProcessor(DeviceOperationResult[] results,
                                   Publisher<ByteInput> publisher,
                                   ModemService service,
                                   ModemApiCommand command) throws ServletException {
        MessageChain mc = new MessageChainV11();
        MessageChainStep resultProcessorStep = mc.addStep("usat.device.modem.result");
        if (service.getProviderType().equals(ModemApiProvider.ESEYE) && command != GET_STATE) {
            // Work around EsEye buggy API.
            // We can't rely on immediate EsEye response and should process it in different way.
            // We postpone any device processing to give EsEye chance to complete their operations
            // But bypass error devices immediately
            // See task USAT-434 for details
            List<DeviceOperationResult> esEyeErrorResults = new ArrayList<>();
            for (DeviceOperationResult result : results) {
                if (!result.isSuccess() || result.getOperationStatus() == OperationStatus.ERROR) {
                    esEyeErrorResults.add(result);
                }
            }
            if (!esEyeErrorResults.isEmpty()) {
                resultProcessorStep.setAttribute(CommonAttrEnum.ATTR_OBJECT, resultsSerializer.toBytes(
                        esEyeErrorResults.toArray(new DeviceOperationResult[esEyeErrorResults.size()])));
                publishMessage(mc, publisher);
            }
        } else {
            resultProcessorStep.setAttribute(CommonAttrEnum.ATTR_OBJECT, resultsSerializer.toBytes(results));
            publishMessage(mc, publisher);
        }
    }

    private void publishMessage(MessageChain mc, Publisher<ByteInput> publisher) throws ServletException {
        try {
            MessageChainService.publish(mc, publisher);
        } catch (ServiceException serviceException) {
            throw new ServletException("Failed to send message", serviceException);
        }
    }

    void emailErrorsToOwner(ModemApiCommand command, USATDevice[] devices, Throwable error) {
        try {
            String body = "This is an automatically generated email to inform you about errors occurred while" +
                    " changing device modem status. Command: " + command.name() + ", Errors: " + error.getMessage() +
                    ", Devices bellow didn't change their states: " + Arrays.toString(devices);
            Properties properties = new Properties();
            properties.put("mail.smtp.host", "mailhost.usatech.com");
            Emailer emailer = new Emailer(properties);
            Email email = emailer.createEmail();
            email.setFrom("customerservice@usatech.com");
            email.setTo(devices[0].getOwnerEmail());
            email.setSubject("DMS: Device modem status change errors");
            email.setBody(body);
            email.send();
        } catch (Exception e) {
            log.error("Failed to send email to user: " + e.getMessage(), e);
        }
    }

    public void setEsEyeUsername(String esEyeUsername) {
        this.esEyeUsername = esEyeUsername;
    }

    public void setEsEyePassword(String esEyePassword) {
        this.esEyePassword = esEyePassword;
    }

    public void setEsEyePortfolioId(String esEyePortfolioId) {
        this.esEyePortfolioId = esEyePortfolioId;
    }

    public void setVerizonUsername(String verizonUsername) {
        this.verizonUsername = verizonUsername;
    }

    public void setVerizonPassword(String verizonPassword) {
        this.verizonPassword = verizonPassword;
    }

    public void setVerizonAppKey(String verizonAppKey) {
        this.verizonAppKey = verizonAppKey;
    }

    public void setVerizonAppSecret(String verizonAppSecret) {
        this.verizonAppSecret = verizonAppSecret;
    }

    public void setVerizonBillingAccountName(String verizonBillingAccountName) {
        this.verizonBillingAccountName = verizonBillingAccountName;
    }

    public void setVerizonCallbackUrl(String verizonCallbackUrl) {
        this.verizonCallbackUrl = verizonCallbackUrl;
    }

    public void setVerizonDefaultServicePlan(String verizonDefaultServicePlan) {
        this.verizonDefaultServicePlan = verizonDefaultServicePlan;
    }

    public void setVerizonCDMAIPPool(String verizonCDMAIPPool) {
			this.verizonCDMAIPPool = verizonCDMAIPPool;
		}

		public void setVerizonLTEIPPool(String verizonLTEIPPool) {
			this.verizonLTEIPPool = verizonLTEIPPool;
		}

		void instantiateServicesIfNeed() {
        if (modemServices[0] != null) {
            return;
        }
        synchronized (modemServices) {
            if (modemServices[0] == null) {
                EsEyeModemApi esEyeDao = new EsEyeModemApiImpl();
                EsEyeModemService esEyeService = new EsEyeModemService(esEyeUsername, esEyePassword, esEyePortfolioId);
                esEyeService.setDao(esEyeDao);
                modemServices[0] = esEyeService;
            }
            if (modemServices[1] == null) {
                VerizonModemApi verizonDao = new VerizonModemApiImpl();
                VerizonModemService verizonService = new VerizonModemService(verizonUsername, verizonPassword, verizonAppKey, verizonAppSecret, verizonBillingAccountName, verizonCallbackUrl, verizonCDMAIPPool, verizonLTEIPPool);
                verizonService.setDao(verizonDao);
                modemServices[1] = verizonService;
            }
        }
    }
}
