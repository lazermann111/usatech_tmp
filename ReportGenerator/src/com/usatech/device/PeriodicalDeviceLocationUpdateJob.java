package com.usatech.device;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.CommonAttrEnum;
import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;
import java.lang.management.ManagementFactory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PeriodicalDeviceLocationUpdateJob implements SelfProcessor {
    static final String PROCESS_CD = "DEVICE_LOCATION_UPDATE_JOB";
    static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));
    static final Log log = Log.getLog();
    int minAllowedUpdateIntervalDays = 1;
    Publisher<ByteInput> publisher;

    @Override
    public void process() throws ServiceException {
        log.debug("PeriodicalDeviceLocationUpdateJob STARTED by schedule..");
        Map<String, Object> lockParams = new HashMap<>();
        boolean lockAcquired = false;
        try {
            lockAcquired = acquireProcessLock(lockParams);
            if (lockAcquired) {
                List<Long> iccidsToUpdate = getIccidsToUpdate();
                log.debug("PeriodicalDeviceLocationUpdateJob wants to update " + iccidsToUpdate.size() + " DEVICES.");
                publishLocationUpdateTask(iccidsToUpdate);
            }
        } catch (Exception e) {
            // We intentionally do not rethrown any exceptions here
            // because we want this job continue running at next scheduled time and do not
            // want to drop this job or immediately repeat it.
            log.error("Could not request device location in Periodical Job", e);
        } finally {
            if (lockAcquired) {
                releaseProcessLock(lockParams);
            }
        }
        log.debug("PeriodicalDeviceLocationUpdateJob FINISHED.");
    }

    boolean acquireProcessLock(Map<String, Object> lockParams) throws DataLayerException, SQLException, ConvertException {
        if(!InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams)) {
            log.info("DEVICE_LOCATION_UPDATE_JOB is already locked by instance " + lockParams.get("lockedBy"));
            return false;
        }
        return true;
    }

    void releaseProcessLock(Map<String, Object> lockParams) {
        try {
            InteractionUtils.unlockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams);
        } catch(Exception e) {
            log.error("Error unlocking " + PROCESS_CD, e);
        }
    }

    List<Long> getIccidsToUpdate() throws SQLException, DataLayerException {
        //if (1 == 2 - 1) {
        //    List<Long> rv = new ArrayList<>();
        //    rv.add(8944538523006819760L);
        //    return rv;
        //}
        Map<String,Object> params = new HashMap<>();
        params.put("minAllowedUpdateIntervalDays", minAllowedUpdateIntervalDays);
        Results res = DataLayerMgr.executeQuery("GET_DEVICES_TO_LOCATION_FIX", params);
        List<Long> rv = new ArrayList<>();
        while(res.next()) {
            rv.add(Long.valueOf(res.getFormattedValue("device_iccid")));
        }
        return rv;
    }

    void publishLocationUpdateTask(List<Long> devicesToUpdate) throws ServiceException {
        for (Long deviceIccId : devicesToUpdate) {
            MessageChain mc = new MessageChainV11();

            MessageChainStep esEyeStep = mc.addStep("usat.location.network.eseye");
            esEyeStep.setAttribute(CommonAttrEnum.ATTR_ITEM_ID, deviceIccId);

            MessageChainStep updateStep = mc.addStep("usat.location.update");
            updateStep.setReferenceAttribute(CommonAttrEnum.ATTR_ITEM_ID, esEyeStep, CommonAttrEnum.ATTR_ITEM_ID);
            updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_LATITUDE, esEyeStep, LocationAttrEnum.ATTR_LATITUDE);
            updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_LONGITUDE, esEyeStep, LocationAttrEnum.ATTR_LONGITUDE);
            updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_COUNTRY_CODE, esEyeStep, LocationAttrEnum.ATTR_COUNTRY_CODE);
            updateStep.setReferenceAttribute(CommonAttrEnum.ATTR_ERROR, esEyeStep, CommonAttrEnum.ATTR_ERROR);

            esEyeStep.setNextSteps(0, updateStep);

            MessageChainService.publish(mc, publisher);
        }
    }

    public int getMinAllowedUpdateIntervalDays() {
        return minAllowedUpdateIntervalDays;
    }

    public void setMinAllowedUpdateIntervalDays(int minAllowedUpdateIntervalDays) {
        this.minAllowedUpdateIntervalDays = minAllowedUpdateIntervalDays;
    }

    public Publisher<ByteInput> getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher<ByteInput> publisher) {
        this.publisher = publisher;
    }
}
