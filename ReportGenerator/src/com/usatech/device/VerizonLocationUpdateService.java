package com.usatech.device;

import com.google.gson.Gson;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.service.modem.dao.generic.exception.UnavailableException;
import simple.service.modem.dao.verizon.VerizonModemApi;
import simple.service.modem.dao.verizon.VerizonModemApiImpl;
import simple.service.modem.dao.verizon.dto.location.VerizonDeviceLocationId;
import simple.service.modem.dao.verizon.dto.location.VerizonDeviceLocationResult;
import simple.service.modem.dao.verizon.dto.location.VerizonPositionData;
import simple.service.modem.dao.verizon.dto.login.VerizonToken;
import simple.service.modem.dao.verizon.exception.VerizonApiException;
import simple.text.StringUtils;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Consumer for TRANSPORT SERVICE(!!!) that handle tasks for updating EsEye device network location
 */
public class VerizonLocationUpdateService implements MessageChainTask {
	private static final Log log = Log.getLog();
	
	private static final int MAX_DEVICES_PER_REQUEST = 20;
	private static final String INVALiD_ACCESS_TOKEN_CODE = "900901";
	
	private String username = null;
	private String password = null;
	private String appKey = null;
	private String appSecret = null;
	private String billingAccountName = null;
	private TokenInfo tokenInfo = new TokenInfo();
	private AtomicBoolean tokenRenew = new AtomicBoolean(false);
	//private VerizonToken token = null;
	//private long tokenTs;
	private VerizonModemApi api = new VerizonModemApiImpl();
	Publisher<ByteInput> publisher;

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		log.debug("VerizonLocationUpdateService STARTED to processing task..");
		VerizonDeviceLocationId deviceId = null;
		VerizonDeviceLocationResult deviceLocation = null;
		try {
			deviceId = getDeviceId(taskInfo);
			if (deviceId == null) {
				log.debug("Empty device id!");
				return -1;
			} else {
				if (tokenisOld()) {
					synchronized(tokenInfo) {
						if (tokenisOld()) {
							log.debug("Receiving new tokens after " + tokenInfo.tokenTs);
							long tokenTs = System.currentTimeMillis();
							String apiToken = api.obtainApiToken(appKey, appSecret);
							tokenInfo.token = api.login(username, password, apiToken);
							tokenInfo.tokenTs = tokenTs;
							log.debug("New token received with TS = " + tokenInfo.tokenTs);
						}
					}
				}
				deviceLocation = getVerizonDeviceLocation(deviceId);
				publishStepResult(taskInfo, deviceLocation);
				log.debug("VerizonLocationUpdateService SUCCESSFULLY processed task: " + deviceId.toString());
			}
		} catch (AttributeConversionException e) {
			log.error("Error converting ITEM_ID attribute to VerizonDeviceLocationId", e);
			publishStepErrorResult(taskInfo, deviceId, deviceLocation, e.getMessage());
		} catch (VerizonApiException e) {
			log.error("Error calling Verizon Location API", e);
			publishStepErrorResult(taskInfo, deviceId, deviceLocation, e.getMessage());
		} catch (UnavailableException e) {
			log.error("Unavailable Verizon Location API", e);
			String msg = e.getMessage();
			if (isInvalidToken(msg)) {
				if (tokenRenew.compareAndSet(false, true)) {
					try {
						synchronized(tokenInfo) {
							log.debug("Receiving new token after Invalid Credentials");
							long tokenTs = System.currentTimeMillis();
							String apiToken;
							apiToken = api.obtainApiToken(appKey, appSecret);
							tokenInfo.token = api.login(username, password, apiToken);
							tokenInfo.tokenTs = tokenTs;
							log.debug("New token received with TS = " + tokenInfo.tokenTs);
						}
						tokenRenew.set(false);
						deviceLocation = getVerizonDeviceLocation(deviceId);
						publishStepResult(taskInfo, deviceLocation);
						log.debug("VerizonLocationUpdateService SUCCESSFULLY processed task: " + deviceId.toString());
					} catch (VerizonApiException e1) {
						log.error("Error calling Verizon Location API after Invalid Credentials", e);
						publishStepErrorResult(taskInfo, deviceId, deviceLocation, e.getMessage());
					} catch (UnavailableException e1) {
						log.error("Unavailable Verizon Location API after Invalid Credentials", e);
						publishStepErrorResult(taskInfo, deviceId, deviceLocation, msg);
					}
				} else {
					try {
						while (tokenRenew.get()) {
							try {
								log.debug("Waiting for token renew after Invalid Credentials");
								Thread.sleep(500);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								//e1.printStackTrace();
							}
						}
						deviceLocation = getVerizonDeviceLocation(deviceId);
						publishStepResult(taskInfo, deviceLocation);
						log.debug("VerizonLocationUpdateService SUCCESSFULLY processed task: " + deviceId.toString());
					} catch (VerizonApiException e1) {
						log.error("Error calling Verizon Location API after Invalid Credentials", e);
						publishStepErrorResult(taskInfo, deviceId, deviceLocation, e.getMessage());
					} catch (UnavailableException e1) {
						log.error("Unavailable Verizon Location API after Invalid Credentials", e);
						publishStepErrorResult(taskInfo, deviceId, deviceLocation, msg);
					}
				}
			} else
				publishStepErrorResult(taskInfo, deviceId, deviceLocation, msg);
		} catch (Exception e) {
			log.error("Unexpected error occurred", e);
			publishStepErrorResult(taskInfo, deviceId, deviceLocation, (e.getMessage() == null ? e.getClass().getName() : e.getMessage()));
		}
		log.debug("VerizonLocationUpdateService FINISHED processing task.");
		return 0;
	}

	void publishStepResult(MessageChainTaskInfo taskInfo, VerizonDeviceLocationResult result) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		if (!StringUtils.isBlank(result.getMsid()))
			step.setResultAttribute(CommonAttrEnum.ATTR_ITEM_ID, Long.valueOf(result.getMsid()));
		if (result.getPositionData() != null) {
			step.setResultAttribute(LocationAttrEnum.ATTR_LATITUDE, Float.valueOf(result.getPositionData().getX()));
			step.setResultAttribute(LocationAttrEnum.ATTR_LONGITUDE, Float.valueOf(result.getPositionData().getY()));
		}
		if (result.getPositionError() != null) {
			step.setResultAttribute(CommonAttrEnum.ATTR_ERROR_MESSAGE, result.getPositionError().toString());
		}
	}

	void publishStepResults(MessageChainTaskInfo taskInfo, VerizonDeviceLocationResult[] deviceLocations) throws ServiceException {
		if (deviceLocations.length == 0)
			return;

		for (VerizonDeviceLocationResult deviceLocation : deviceLocations)
			publishStepResult(taskInfo, deviceLocation);

	}

	void publishStepErrorResult(MessageChainTaskInfo taskInfo, VerizonDeviceLocationId deviceId, VerizonDeviceLocationResult deviceLocation, String errorMessage) {
		MessageChainStep step = taskInfo.getStep();
		step.setResultAttribute(CommonAttrEnum.ATTR_ITEM_ID,  (deviceId == null ? -1 : Long.valueOf(deviceId.getMdn())));
		if (deviceLocation != null && deviceLocation.getPositionData() != null) {
			VerizonPositionData vpd = deviceLocation.getPositionData();
			if (vpd.getX() != null) {
				step.setResultAttribute(LocationAttrEnum.ATTR_LATITUDE, vpd.getX());
			}
			if (vpd.getY() != null) {
				step.setResultAttribute(LocationAttrEnum.ATTR_LONGITUDE, vpd.getY());
			}
		}
		step.setResultAttribute(CommonAttrEnum.ATTR_ERROR_MESSAGE, errorMessage);
	}

	VerizonDeviceLocationId[] getDeviceIdsAttribute(MessageChainTaskInfo taskInfo) throws AttributeConversionException {
		String deviceStr = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_OBJECT, String.class, true);
		Gson gson = new Gson();
		return gson.fromJson(deviceStr, VerizonDeviceLocationId[].class);
	}

	VerizonDeviceLocationId getDeviceId(MessageChainTaskInfo taskInfo) throws AttributeConversionException {
		String deviceStr = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_OBJECT, String.class, true);
		Gson gson = new Gson();
		return gson.fromJson(deviceStr, VerizonDeviceLocationId.class);
	}

	public VerizonDeviceLocationResult getVerizonDeviceLocation(VerizonDeviceLocationId deviceId) throws VerizonApiException, UnavailableException {
		log.debug("VerizonLocationUpdateService getNetworkDeviceLocation for " + deviceId);
		String accuracyMode = "0";
		String cacheMode = "2";
		VerizonDeviceLocationResult[] locResults = api.location(deviceId, billingAccountName, accuracyMode, cacheMode, tokenInfo.token);
		return (locResults == null || locResults.length == 0 ? null : locResults[0]);
	}

	public VerizonDeviceLocationResult[] getVerizonDeviceLocations(VerizonDeviceLocationId[] devices) throws VerizonApiException, UnavailableException {
		int nDev = devices.length;
		log.debug("VerizonLocationUpdateService getNetworkDeviceLocation for " + nDev + "devices");
		if (nDev == 0)
			return null;
		String accuracyMode = "0";
		String cacheMode = "2";
		VerizonDeviceLocationId[] vdlia = null;
		VerizonDeviceLocationResult[] rv = new VerizonDeviceLocationResult[nDev];
		for (int i = 0; i < nDev; i += MAX_DEVICES_PER_REQUEST) {
			int to = i + MAX_DEVICES_PER_REQUEST;
			vdlia = Arrays.copyOfRange(devices, i, (to < nDev ? to : nDev));
			VerizonDeviceLocationResult[] locResults = api.location(vdlia, billingAccountName, accuracyMode, cacheMode, tokenInfo.token);
			int j = 0;
			for (VerizonDeviceLocationResult result: locResults)
				rv[i + (j++)] = result;
		}
		return rv;
	}

	// maximum token life, 19 minutes for now
	private static final long MAX_TOKEN_LIFE = 19 * 60 * 1000;

	private boolean tokenisOld() {
		return (tokenInfo.token == null || tokenInfo.tokenTs + MAX_TOKEN_LIFE < System.currentTimeMillis());
	}

	private boolean isInvalidToken(String message) {
		return message != null && message.contains(INVALiD_ACCESS_TOKEN_CODE);
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public void setBillingAccountName(String billingAccountName) {
		this.billingAccountName = billingAccountName;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	private static class TokenInfo {
		private VerizonToken token = null;
		private long tokenTs;
	}
}
