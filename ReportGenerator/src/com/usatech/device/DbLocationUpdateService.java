package com.usatech.device;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;
import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Consumer for REPORT GENERATOR SERVICE(!!!) that handle tasks for updating EsEye device location in ORACLE DB
 */
public class DbLocationUpdateService implements MessageChainTask {
    static final Log log = Log.getLog();

    @Override
    public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
        log.debug("DbLocationUpdateService STARTED to processing task..");
        try {
            Long deviceIccId = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_ITEM_ID, Long.class, true);
            Long deviceId = getDeviceIdByIccid(deviceIccId);
            DeviceLocation deviceLocation = buildDeviceLocation(deviceId, taskInfo);
            if (StringUtils.isBlank(deviceLocation.getErrorCode())) {
                fillCityStatePostal(deviceLocation);
            }
            updateDeviceLocationInDatabase(deviceLocation);
            log.debug("DbLocationUpdateService SUCCESSFULLY processed task: " + deviceLocation.toString());
        } catch (AttributeConversionException e) {
            log.error("Error converting ITEM_ID attribute to Long", e);
        } catch (SQLException e) {
            log.error("Can't execute DB call", e);
        } catch (DataLayerException e) {
            log.error("Error while executing query in data layer", e);
        } catch (Throwable t) {
            log.error("Unexpected error occurred", t);
        }
        //TODO: for now we always successful, but we should found the way how to fail correctly in MQ to stop further chain processing
        log.debug("DbLocationUpdateService FINISHED processing task.");
        return 0;
    }

    DeviceLocation buildDeviceLocation(Long deviceId, MessageChainTaskInfo taskInfo) throws AttributeConversionException {
        DeviceLocation deviceLocation = new DeviceLocation();
        deviceLocation.setDeviceId(deviceId);
        deviceLocation.setLatitude(taskInfo.getStep().getAttribute(LocationAttrEnum.ATTR_LATITUDE, Float.class, false));
        deviceLocation.setLongitude(taskInfo.getStep().getAttribute(LocationAttrEnum.ATTR_LONGITUDE, Float.class, false));
        deviceLocation.setCountry(taskInfo.getStep().getAttribute(LocationAttrEnum.ATTR_COUNTRY_CODE, String.class, false));
        deviceLocation.setErrorCode(taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_ERROR, String.class, false));
        return deviceLocation;
    }

    long getDeviceIdByIccid(long deviceIccid) throws SQLException, DataLayerException {
        //if (deviceIccid == 8944538523006819760L) {   // 1 == 2 - 1
        //    return 19452;
        //}
        Map<String,Object> params = new HashMap<>();
        params.put("device_iccid", String.valueOf(deviceIccid));
        Results res = DataLayerMgr.executeQuery("GET_DEVICE_ID_BY_ICCID", params);
        if(res.next()) {
            return ((BigDecimal)res.get("device_id")).longValue();
        }
        throw new IllegalStateException("Device with iccid = " + deviceIccid + " should have ID, but it don't.");
    }

	void fillCityStatePostal(DeviceLocation deviceLocation) throws SQLException, DataLayerException {
		Map<String,Object> params = new HashMap<>();
		params.put("latitude", deviceLocation.getLatitude());
		params.put("longitude", deviceLocation.getLongitude());
		DataLayerMgr.executeCall("GET_CITY_STATE_POSTAL_BY_COORD", params, true);
		deviceLocation.setPostal((String)params.get("postal_cd"));
		deviceLocation.setCity((String)params.get("city"));
		deviceLocation.setState((String)params.get("state_cd"));
	}

    void updateDeviceLocationInDatabase(DeviceLocation deviceLocation) throws SQLException, DataLayerException {
        Map<String,Object> params = new HashMap<>();
        params.put("city", deviceLocation.getCity());
        params.put("state_cd", deviceLocation.getState());
        params.put("postal_cd", deviceLocation.getPostal());
        params.put("latitude", deviceLocation.getLatitude());
        params.put("longitude", deviceLocation.getLongitude());
        params.put("error_cd", deviceLocation.getErrorCode());
        params.put("device_id", deviceLocation.getDeviceId());
        DataLayerMgr.executeUpdate("UPDATE_DEVICE_NET_INFO", params, true);
    }
}
