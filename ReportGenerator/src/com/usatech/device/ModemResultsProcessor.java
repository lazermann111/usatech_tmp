package com.usatech.device;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;
import simple.app.ServiceException;
import simple.io.Log;
import simple.service.modem.service.ModemStatusDbResultProcessor;
import simple.service.modem.service.ResultProcessor;
import simple.service.modem.service.dto.DeviceOperationResult;
import simple.util.ClassSerializer;

/**
 * Task for REPORT GENERATOR SERVICE (!!!) to process modem status results
 *
 * @author dlozenko
 */
public class ModemResultsProcessor implements MessageChainTask {
    static final Log log = Log.getLog();
    private final ResultProcessor<DeviceOperationResult> resultProcessor = new ModemStatusDbResultProcessor();
    private final ClassSerializer<DeviceOperationResult[]> paramsDeserializer = new ClassSerializer<>();

    @Override
    public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
        DeviceOperationResult[] results = null;
        try {
            results = extractResults(taskInfo);
            for (DeviceOperationResult result : results) {
                resultProcessor.process(result);
            }
        } catch (Throwable t) {
            log.error("Unexpected error occurred", t);
        }
        return 0;
    }

    DeviceOperationResult[] extractResults(MessageChainTaskInfo taskInfo) throws AttributeConversionException {
        return paramsDeserializer.fromBytes(taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_OBJECT, byte[].class, true));
    }
}
