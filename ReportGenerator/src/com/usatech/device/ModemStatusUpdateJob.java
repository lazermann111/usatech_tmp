package com.usatech.device;

import com.usatech.device.modem.AsyncRgrModemService;
import com.usatech.layers.common.InteractionUtils;
import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.modem.USATDeviceDao;
import simple.modem.USATDeviceDaoImpl;
import simple.results.Results;
import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.service.DeviceModemStatusDao;
import simple.service.modem.service.DeviceModemStatusDaoImpl;
import simple.service.modem.service.dto.DeviceModemState;
import simple.service.modem.service.dto.USATDevice;
import javax.servlet.ServletException;
import java.lang.management.ManagementFactory;
import java.sql.SQLException;
import java.util.*;

/**
 * Job to periodically update modem statuses
 *
 * @author dlozenko
 */
public class ModemStatusUpdateJob implements SelfProcessor {
    static final String PROCESS_CD = "MODEM_STATUS_UPDATE_JOB";
    static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));
    static final Log log = Log.getLog();
    Publisher<ByteInput> publisher;

    @Override
    public void process() throws ServiceException {
        Map<String, Object> lockParams = new HashMap<>();
        boolean lockAcquired = false;
        try {
            lockAcquired = acquireProcessLock(lockParams);
            if (lockAcquired) {
                List<DeviceModemState> devicesToUpdate = getDevicesToUpdate();
                publishModemStatusUpdateTask(devicesToUpdate);
            }
        } catch (Exception e) {
            // We intentionally do not rethrown any exceptions here
            // because we want this job continue running at next scheduled time and do not
            // want to drop this job or immediately repeat it.
            log.error("Could not request device modem status update in Periodical Job", e);
        } finally {
            if (lockAcquired) {
                releaseProcessLock(lockParams);
            }
        }
    }

    boolean acquireProcessLock(Map<String, Object> lockParams) throws DataLayerException, SQLException, ConvertException {
        if(!InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams)) {
            log.info("MODEM_STATUS_UPDATE_JOB is already locked by instance " + lockParams.get("lockedBy"));
            return false;
        }
        return true;
    }

    void releaseProcessLock(Map<String, Object> lockParams) {
        try {
            InteractionUtils.unlockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams);
        } catch(Exception e) {
            log.error("Error unlocking " + PROCESS_CD, e);
        }
    }

    List<DeviceModemState> getDevicesToUpdate() throws SQLException, DataLayerException, ConvertException {
        Results res = DataLayerMgr.executeQuery("GET_DEVICES_TO_MODEM_STATUS_UPDATE", null);
        List<DeviceModemState> rv = new ArrayList<>();
        while(res.next()) {
            DeviceModemState dms = new DeviceModemState();
            dms.setModemSerialCode(res.getFormattedValue("modem_serial_cd"));
            dms.setRequestId(res.getFormattedValue("request_id"));
            dms.setProvider(ModemApiProvider.fromString(res.getFormattedValue("provider")));
            dms.setCreatedTs(res.getValue("created_ts", Date.class));
            rv.add(dms);
        }
        return rv;
    }

    void publishModemStatusUpdateTask(List<DeviceModemState> devicesToUpdate) throws ServiceException, SQLException, ServletException, DataLayerException, ConvertException {
        for (DeviceModemState device : devicesToUpdate) {
            enqueueModemStatusRead(device);
        }
    }

    public void enqueueModemStatusRead(DeviceModemState deviceState) throws DataLayerException, SQLException, ConvertException, ServletException {
        USATDeviceDao modemDeviceDao = new USATDeviceDaoImpl();
        DeviceModemStatusDao statusDao = new DeviceModemStatusDaoImpl();
        Long deviceId = statusDao.findDeviceIdBySerialCode(deviceState.getModemSerialCode());
        if (deviceId != null) {
            USATDevice dev = modemDeviceDao.populateByDeviceId(deviceId);
            if (dev.hasSerialNumberAndProvider()) {
                AsyncRgrModemService rgrModemService = new AsyncRgrModemService();
                rgrModemService.setPublisher(publisher);
                rgrModemService.refreshActivationStatus(new USATDevice[]{dev});
            }
        }
    }

    public Publisher<ByteInput> getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher<ByteInput> publisher) {
        this.publisher = publisher;
    }
}
