package com.usatech.device;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;
import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Consumer for REPORT GENERATOR SERVICE(!!!) that handle tasks for updating Verizon device location in ORACLE DB
 */
public class DbVerizonLocationUpdateService implements MessageChainTask {
	static final Log log = Log.getLog();

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		log.debug("DbVerizonLocationUpdateService STARTED to processing task..");
		try {
			Long deviceMdn = taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_ITEM_ID, Long.class, true);
			Long deviceId = getDeviceIdByMdn(deviceMdn);
			DeviceLocation deviceLocation = buildDeviceLocation(deviceId, taskInfo);
			if (StringUtils.isBlank(deviceLocation.getErrorCode())) {
				fillCityStatePostal(deviceLocation);
			}
			updateDeviceLocationInDatabase(deviceLocation);
			log.debug("DbVerizonLocationUpdateService SUCCESSFULLY processed task: " + deviceLocation.toString());
		} catch (AttributeConversionException e) {
			log.error("Error converting ITEM_ID attribute to Long", e);
		} catch (SQLException e) {
			log.error("Can't execute DB call", e);
		} catch (DataLayerException e) {
			log.error("Error while executing query in data layer", e);
		} catch (Throwable t) {
			log.error("Unexpected error occurred", t);
		}
		//TODO: for now we always successful, but we should found the way how to fail correctly in MQ to stop further chain processing
		log.debug("DbVerizonLocationUpdateService FINISHED processing task.");
		return 0;
	}

	DeviceLocation buildDeviceLocation(Long deviceId, MessageChainTaskInfo taskInfo) throws AttributeConversionException {
		DeviceLocation deviceLocation = new DeviceLocation();
		deviceLocation.setDeviceId(deviceId);
		deviceLocation.setLatitude(taskInfo.getStep().getAttribute(LocationAttrEnum.ATTR_LATITUDE, Float.class, false));
		deviceLocation.setLongitude(taskInfo.getStep().getAttribute(LocationAttrEnum.ATTR_LONGITUDE, Float.class, false));
		//deviceLocation.setCountry(taskInfo.getStep().getAttribute(LocationAttrEnum.ATTR_COUNTRY_CODE, String.class, false));
		//deviceLocation.setErrorCode(taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_ERROR, String.class, false));
		deviceLocation.setErrorCode(taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_ERROR_MESSAGE, String.class, false));
		return deviceLocation;
	}

	long getDeviceIdByMdn(long deviceMdn) throws SQLException, DataLayerException {
		//if (deviceIccid == 8944538523006819760L) {   // 1 == 2 - 1
		//    return 19452;
		//}
		Map<String, Object> params = new HashMap<>();
		String mdnStr = String.valueOf(deviceMdn);
		params.put("device_mdn", mdnStr);
		params.put("device_mdn_quote", mdnStr + '"');
		params.put("device_mdn_vlte", "+1" + mdnStr);
		params.put("device_mdn_cnum", ',' + mdnStr + ",129");
		Results res = DataLayerMgr.executeQuery("GET_DEVICE_ID_BY_MDN", params);
		if (res.next()) {
			return ((BigDecimal)res.get("device_id")).longValue();
		}
		throw new IllegalStateException("Device with mdn = " + deviceMdn + " should have ID, but it doesn't.");
	}

	void fillCityStatePostal(DeviceLocation deviceLocation) throws SQLException, DataLayerException {
		Map<String,Object> params = new HashMap<>();
		params.put("latitude", deviceLocation.getLatitude());
		params.put("longitude", deviceLocation.getLongitude());
		DataLayerMgr.executeCall("GET_CITY_STATE_POSTAL_BY_COORD", params, true);
		deviceLocation.setPostal((String)params.get("postal_cd"));
		deviceLocation.setCity((String)params.get("city"));
		deviceLocation.setState((String)params.get("state_cd"));
	}

	private static final int MAX_ERROR_LEN = 250;

	void updateDeviceLocationInDatabase(DeviceLocation deviceLocation) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<>();
		params.put("city", deviceLocation.getCity());
		params.put("state_cd", deviceLocation.getState());
		params.put("postal_cd", deviceLocation.getPostal());
		params.put("latitude", deviceLocation.getLatitude());
		params.put("longitude", deviceLocation.getLongitude());
		String errorCode = deviceLocation.getErrorCode();
		if (errorCode != null && errorCode.length() > MAX_ERROR_LEN)
			errorCode = errorCode.substring(0, MAX_ERROR_LEN);
		params.put("error_cd", errorCode);
		params.put("device_id", deviceLocation.getDeviceId());
		DataLayerMgr.executeUpdate("UPDATE_DEVICE_NET_INFO", params, true);
	}
}
