package com.usatech.device;

import com.google.gson.Gson;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.CommonAttrEnum;
import simple.app.ServiceException;
import simple.io.Log;
import simple.location.network.api.Credentials;
import simple.location.network.api.Request;
import simple.location.network.dto.EsEyeDeviceLocation;
import simple.location.network.dto.EsEyeStatus;
import simple.location.network.dto.NetworkDeviceLocation;
import simple.location.network.exception.DeviceLocationAccessDeniedException;
import simple.location.network.exception.NetDeviceLocationException;
import simple.location.network.impl.JsonRequest;
import simple.location.network.impl.UsernamePasswordCredentials;
import simple.text.StringUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Consumer for TRANSPORT SERVICE(!!!) that handle tasks for updating EsEye device network location
 */
public class EsEyeLocationUpdateService implements MessageChainTask {
    private static final String ESEYE_REST_API_URL = "https://location.eseye.com/index/locatedevice";
    private static final String OK_STATUS = "ok";
    private static final String ACCESS_DENIED_STATUS = "602";
    private static final Log log = Log.getLog();
    private String esEyeUsername = null;
    private String esEyePassword = null;

    @Override
    public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
        log.debug("EsEyeLocationUpdateService STARTED to processing task..");
        Long deviceIccId = null;
        NetworkDeviceLocation deviceLocation = null;
        try {
            UsernamePasswordCredentials credentials = buildCredentials();
            deviceIccId = getDeviceIccIdAttribute(taskInfo);
            deviceLocation = getNetworkDeviceLocation(deviceIccId, credentials);
            publishStepResult(taskInfo, deviceIccId, deviceLocation);
            log.debug("EsEyeLocationUpdateService SUCCESSFULLY processed task: " + deviceLocation.toString());
        } catch (AttributeConversionException e) {
            log.error("Error converting ITEM_ID attribute to Long", e);
        } catch (NetDeviceLocationException e) {
            log.error("EsEye related error occurred", e);
            publishStepErrorResult(taskInfo, deviceIccId, deviceLocation, e.getErrorCode());
        } catch (Exception e) {
            log.error("Unexpected error occurred", e);
            publishStepErrorResult(taskInfo, deviceIccId, deviceLocation, NetDeviceLocationException.ERR_CODE_INTERNAL_ERROR);
        }
        log.debug("EsEyeLocationUpdateService FINISHED processing task.");
        return 0;
    }

    void publishStepResult(MessageChainTaskInfo taskInfo, Long deviceIccId, NetworkDeviceLocation deviceLocation) {
        taskInfo.getStep().setResultAttribute(CommonAttrEnum.ATTR_ITEM_ID, deviceIccId);
        taskInfo.getStep().setResultAttribute(LocationAttrEnum.ATTR_LATITUDE, deviceLocation.getLatitude());
        taskInfo.getStep().setResultAttribute(LocationAttrEnum.ATTR_LONGITUDE, deviceLocation.getLongitude());
        taskInfo.getStep().setResultAttribute(LocationAttrEnum.ATTR_COUNTRY_CODE, deviceLocation.getCountryCode());
        taskInfo.getStep().setResultAttribute(CommonAttrEnum.ATTR_ERROR, null);
    }

    void publishStepErrorResult(MessageChainTaskInfo taskInfo, Long deviceIccId, NetworkDeviceLocation deviceLocation, String errorCode) {
        if (deviceIccId != null) {
            taskInfo.getStep().setResultAttribute(CommonAttrEnum.ATTR_ITEM_ID, deviceIccId);
        }
        if (deviceLocation != null) {
            if (deviceLocation.getLatitude() != null) {
                taskInfo.getStep().setResultAttribute(LocationAttrEnum.ATTR_LATITUDE, deviceLocation.getLatitude());
            }
            if (deviceLocation.getLongitude() != null) {
                taskInfo.getStep().setResultAttribute(LocationAttrEnum.ATTR_LONGITUDE, deviceLocation.getLongitude());
            }
            if (deviceLocation.getCountryCode() != null) {
                taskInfo.getStep().setResultAttribute(LocationAttrEnum.ATTR_COUNTRY_CODE, deviceLocation.getCountryCode());
            }
        }
        taskInfo.getStep().setResultAttribute(CommonAttrEnum.ATTR_ERROR,
                StringUtils.isBlank(errorCode) ? NetDeviceLocationException.ERR_CODE_INTERNAL_ERROR : errorCode);
    }

    UsernamePasswordCredentials buildCredentials() {
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials();
        credentials.setUsername(esEyeUsername);
        credentials.setPassword(esEyePassword);
        return credentials;
    }

    Long getDeviceIccIdAttribute(MessageChainTaskInfo taskInfo) throws AttributeConversionException {
        return taskInfo.getStep().getAttribute(CommonAttrEnum.ATTR_ITEM_ID, Long.class, true);
    }

    public NetworkDeviceLocation getNetworkDeviceLocation(long iccid, Credentials credentials) throws NetDeviceLocationException {
        String requestId = getRandomRequestId();
        log.debug("EsEyeLocationUpdateService PROCESSING REQUEST_ID: " + requestId);
        String jsonRequest = buildJsonRequest(iccid, credentials, requestId);
        String jsonResponse = callEsEyeLocationApi(jsonRequest);
        EsEyeDeviceLocation esEyeDeviceLocation = fromJson(jsonResponse);
        checkResultStatus(esEyeDeviceLocation, requestId);
        return esEyeDeviceLocation;
    }

    String buildJsonRequest(long iccid, Credentials credentials, String requestId) {
        Request jsonReq = new JsonRequest();
        jsonReq.setParam("request_id", requestId);
        jsonReq.setParam("ICCID", String.valueOf(iccid));
        credentials.prepareRequest(jsonReq);
        return jsonReq.convertToString();
    }

    String getRandomRequestId() {
        return String.valueOf(ThreadLocalRandom.current().nextInt(1, 100));
    }

    private String callEsEyeLocationApi(String json) {
        StringBuilder rv = new StringBuilder("");
        HttpURLConnection conn;
        BufferedReader br = null;
        try {
            URL url = new URL(ESEYE_REST_API_URL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes());
            os.flush();
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new NetDeviceLocationException("EsEye API access problem, HTTP error code : " +
                        conn.getResponseCode(), NetDeviceLocationException.ERR_CODE_BAD_HTTP_RESPONSE);
            }
            br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output;
            while ((output = br.readLine()) != null) {
                if (rv.length() > 0) {
                    rv.append("\n");
                }
                rv.append(output);
            }
        } catch (IOException e) {
            throw new NetDeviceLocationException("I/O problem while connecting to EsEye API", e,
                    NetDeviceLocationException.ERR_CODE_NETWORK_PROBLEM);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ignored) {
                }
            }
            // Commented to allow Java to use connection pooling when call to openConnection(..)
            //if (conn != null) {
            //conn.disconnect();
            //}
        }
        return rv.toString();
    }

    EsEyeDeviceLocation fromJson(String netDevLocJson) {
        Gson gson = new Gson();
        return gson.fromJson(netDevLocJson, EsEyeDeviceLocation.class);
    }

    void checkResultStatus(EsEyeDeviceLocation deviceLocation, String requestId) throws NetDeviceLocationException {
        if (deviceLocation == null) {
            throw new NetDeviceLocationException("null device location returned from EsEye API",
                    NetDeviceLocationException.ERR_CODE_UNEXPECTED_API_RESPONSE);
        }
        EsEyeStatus status = deviceLocation.getStatus();
        if (status == null) {
            throw new NetDeviceLocationException("null device location status returned from EsEye API",
                    NetDeviceLocationException.ERR_CODE_UNEXPECTED_API_RESPONSE);
        }
        if (!OK_STATUS.equalsIgnoreCase(status.getStatus())) {
            if (ACCESS_DENIED_STATUS.equals(status.getStatus())) {
                throw new DeviceLocationAccessDeniedException("EsEye API reported following error: CODE:" + status.getErrorCode() +
                        ", MESSAGE:" + status.getErrorMessage());
            } else {
                throw new NetDeviceLocationException("EsEye API reported following error: CODE:" + status.getErrorCode() +
                        ", MESSAGE:" + status.getErrorMessage(), status.getErrorCode());
            }
        }
        if (deviceLocation.getLatitude() == null) {
            throw new NetDeviceLocationException("null device LATITUDE returned from EsEye API",
                    NetDeviceLocationException.ERR_CODE_UNEXPECTED_API_RESPONSE);
        } else if (deviceLocation.getLongitude() == null) {
            throw new NetDeviceLocationException("null device LONGITUDE returned from EsEye API",
                    NetDeviceLocationException.ERR_CODE_UNEXPECTED_API_RESPONSE);
        } else if (deviceLocation.getIccid() == null) {
            throw new NetDeviceLocationException("null device ICCID returned from EsEye API",
                    NetDeviceLocationException.ERR_CODE_UNEXPECTED_API_RESPONSE);
        } else  if (deviceLocation.getRequestId() == null || !requestId.equals(deviceLocation.getRequestId().toString())) {
            throw new NetDeviceLocationException("Invalid RESPONSE_ID returned from EsEye API, expected: " + requestId +
                    ", actual: " + deviceLocation.getRequestId(),
                    NetDeviceLocationException.ERR_CODE_UNEXPECTED_API_RESPONSE);
        }
    }

    public void setEsEyeUsername(String esEyeUsername) {
        this.esEyeUsername = esEyeUsername;
    }

    public void setEsEyePassword(String esEyePassword) {
        this.esEyePassword = esEyePassword;
    }
}
