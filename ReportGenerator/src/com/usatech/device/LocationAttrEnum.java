package com.usatech.device;

import com.usatech.app.Attribute;
import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

public enum LocationAttrEnum implements Attribute {
    ATTR_LATITUDE("latitude", "Latitude of device location"),
    ATTR_LONGITUDE("longitude", "Longitude of device location"),
    ATTR_COUNTRY_CODE("countryCode", "Country code of device location");

    final static EnumStringValueLookup<LocationAttrEnum> lookup = new EnumStringValueLookup<>(LocationAttrEnum.class);

    private final String value;
    private final String description;

    public static LocationAttrEnum getByValue(String value) throws InvalidValueException {
        return lookup.getByValue(value);
    }

    LocationAttrEnum(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
