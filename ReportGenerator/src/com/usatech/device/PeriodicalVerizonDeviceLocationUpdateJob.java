package com.usatech.device;

import com.google.gson.Gson;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.CommonAttrEnum;
import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.Results;
import simple.service.modem.dao.verizon.dto.location.IdKind;
import simple.service.modem.dao.verizon.dto.location.VerizonDeviceLocationId;

import java.lang.management.ManagementFactory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PeriodicalVerizonDeviceLocationUpdateJob implements SelfProcessor {
	static final String VERIZON_LBS_ENABLED_CD = "VERIZON_LBS_ENABLED";
	static final String PROCESS_CD = "VERIZON_DEVICE_LOCATION_UPDATE_JOB";
	static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));
	static final Log log = Log.getLog();
	static final int DEFAULT_MAX_ROW_NUM = 2000;
	int minAllowedUpdateIntervalDays = 1;
	int maxRowNum = DEFAULT_MAX_ROW_NUM;
	Publisher<ByteInput> publisher;

	@Override
	public void process() throws ServiceException {
		log.debug("PeriodicalVerizonDeviceLocationUpdateJob STARTED by schedule..");
		if (!isVerizonLbsEnabled()) {
			log.debug("Verizon LBS is disabled, nothing to do");
			return;
		}
		String instance = null;
		try {
			Results res = DataLayerMgr.executeQuery("GET_INSTANCE_NAME", null);
			instance = (res.next() ? res.getFormattedValue("instanceName") : null);
			log.debug("Instance: " + (instance != null ? instance : "no instance"));
		} catch (SQLException e1) {
			log.debug("SQL exception: ", e1);
			//e1.printStackTrace();
		} catch (DataLayerException e1) {
			// TODO Auto-generated catch block
			log.debug("Mgr exception: ", e1);
			//e1.printStackTrace();
		}
		Map<String, Object> lockParams = new HashMap<>();
		boolean lockAcquired = false;
		try {
			lockAcquired = acquireProcessLock(lockParams);
			if (lockAcquired) {
				List<VerizonDeviceLocationId> devicesToUpdate = getDevicesToUpdate(instance);
				log.debug("PeriodicalVerizonDeviceLocationUpdateJob wants to update " + devicesToUpdate.size() + " DEVICES.");
				publishLocationUpdateTask(devicesToUpdate);
			}
		} catch (Exception e) {
			// We intentionally do not rethrow any exceptions here
			// because we want this job continue running at next scheduled time and do not
			// want to drop this job or immediately repeat it.
			log.error("Could not request device location in Periodical Verizon Job", e);
		} finally {
			if (lockAcquired) {
				releaseProcessLock(lockParams);
			}
		}
		log.debug("PeriodicalVerizonDeviceLocationUpdateJob FINISHED.");
	}

	private boolean isVerizonLbsEnabled() {
		Object [] param = {VERIZON_LBS_ENABLED_CD};
		try {
			Results res = DataLayerMgr.executeQuery("GET_APP_SETTING", param);
			String verizonLbsEnabled = (res.next() ? res.getFormattedValue("value") : "N");
			return "Y".equals(verizonLbsEnabled);
		} catch (SQLException e1) {
			log.debug("SQL exception: ", e1);
		} catch (DataLayerException e1) {
			log.error("Mgr exception: ", e1);
		}
		return false;
	}

	boolean acquireProcessLock(Map<String, Object> lockParams) throws DataLayerException, SQLException, ConvertException {
		if (!InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams)) {
			log.info(PROCESS_CD + " is already locked by instance " + lockParams.get("lockedBy"));
			return false;
		}
		return true;
	}

	void releaseProcessLock(Map<String, Object> lockParams) {
		try {
			InteractionUtils.unlockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams);
		} catch (Exception e) {
			log.error("Error unlocking " + PROCESS_CD, e);
		}
	}

	List<VerizonDeviceLocationId> getDevicesToUpdate(String instance) throws SQLException, DataLayerException {
		List<VerizonDeviceLocationId> rv = new ArrayList<>();
		/*if (instance != null && instance.equalsIgnoreCase("usaodev04")) {
			rv.add(new VerizonDeviceLocationId(VerizonDeviceKind.MEID, "A1000009D63642", "2065169430"));
			rv.add(new VerizonDeviceLocationId(VerizonDeviceKind.MEID, "A1000009D672FF", "2675627408"));
			rv.add(new VerizonDeviceLocationId(VerizonDeviceKind.MEID, "A1000009D6A261", "2675649798"));
			return rv;
		}*/
		Map<String, Object> params = new HashMap<>();
		params.put("minAllowedUpdateIntervalDays", minAllowedUpdateIntervalDays);
		params.put("maxRowNum", (maxRowNum < 1 ? DEFAULT_MAX_ROW_NUM : maxRowNum));
		log.debug("Params: " + params);
		Results res = DataLayerMgr.executeQuery("GET_VERIZON_DEVICES_TO_LOCATION_FIX", params);
		while (res.next()) {
			IdKind kind = IdKind.getValueOf(res.getFormattedValue("hostTypeDesc"));
			if (kind == null)
				continue;
			String deviceId = kind.checkDeviceId(res.getFormattedValue("hostSerial"), res.getFormattedValue("hostLabel"));
			if (deviceId == null)
				continue;
			String mdn = kind.checkMdn(res.getFormattedValue("hostDialNumber"));
			log.debug("deviceId, mdn: " + deviceId + ',' + res.getFormattedValue("hostDialNumber") + ',' + mdn);
			if (mdn == null)
				continue;
			VerizonDeviceLocationId vdli = new VerizonDeviceLocationId(kind.getVerizonDeviceKind(), deviceId, mdn);
			rv.add(vdli);
		}
		return rv;
	}

	void publishLocationUpdateTask(List<VerizonDeviceLocationId> devicesToUpdate) throws ServiceException {
		if (devicesToUpdate.size() == 0)
			return;

		for (VerizonDeviceLocationId deviceLocationId : devicesToUpdate) {

			MessageChain mc = new MessageChainV11();
			MessageChainStep verizonStep = mc.addStep("usat.location.network.verizon");

			Object attrValue = preareAttributeValue(deviceLocationId);
			verizonStep.setAttribute(CommonAttrEnum.ATTR_OBJECT, attrValue);

			MessageChainStep updateStep = mc.addStep("usat.verizon.location.update");
			updateStep.setReferenceAttribute(CommonAttrEnum.ATTR_ITEM_ID, verizonStep, CommonAttrEnum.ATTR_ITEM_ID);
			updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_LATITUDE, verizonStep, LocationAttrEnum.ATTR_LATITUDE);
			updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_LONGITUDE, verizonStep, LocationAttrEnum.ATTR_LONGITUDE);
			//updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_COUNTRY_CODE, verizonStep, LocationAttrEnum.ATTR_COUNTRY_CODE);
			updateStep.setReferenceAttribute(CommonAttrEnum.ATTR_ERROR_MESSAGE, verizonStep, CommonAttrEnum.ATTR_ERROR_MESSAGE);

			verizonStep.setNextSteps(0, updateStep);

			MessageChainService.publish(mc, publisher);
		}
	}

	private Object preareAttributeValue(VerizonDeviceLocationId deviceLocationId) {
		Gson gson = new Gson();
		return gson.toJson(deviceLocationId);
	}

	public int getMinAllowedUpdateIntervalDays() {
		return minAllowedUpdateIntervalDays;
	}

	public void setMinAllowedUpdateIntervalDays(int minAllowedUpdateIntervalDays) {
		this.minAllowedUpdateIntervalDays = minAllowedUpdateIntervalDays;
	}

	public int getMaxRowNum() {
		return maxRowNum;
	}

	public void setMaxRowNum(int maxRowNum) {
		this.maxRowNum = maxRowNum;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
}
