package com.usatech.device.file;

import java.lang.management.ManagementFactory;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.util.DeviceUtils;

public class PeriodicalEMVCAPKeyFileUpdateJob implements SelfProcessor {

	private static final Log log = Log.getLog();

	private static final String PROCESS_CD = "EMV_CAPK_UPDATE_JOB";
	private static final String LOCK_PROCESS_TOKEN_CD = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random()));

	private static final String EMV_CAP_KEYS_FIRMWARE_CD = "EMV_CAPK_UPDATE_MIN_FIRMWARE_VERSION";
	private static final String EMV_CAP_KEYS_CARD_READERS_CD = "EMV_CAPK_UPDATE_CARD_READERS";

	private static final String CARD_READER_DELIM = ";";

	private static final String CARD_READER_MODEL_DELIM = ",";

	@Override
	public void process() throws ServiceException {
		Map<String, Object> lockParams = new HashMap<>();
		boolean lockAcquired = false;
		try {
			lockAcquired = acquireProcessLock(lockParams);
			if (lockAcquired) {
				String query = getQueryForDevices();
				log.info("Query for devices to update " + query);

				Map<String, Object> params = new HashMap<>();
				params.put("fileTransferTypeCd", FileType.CARD_READER_EMV_PARAMETER_FILE.getValue());
				params.put("packetSize", DeviceUtils.DEFAULT_PACKET_SIZE);
				params.put("deviceQuery", query);
				DataLayerMgr.executeCall("CREATE_PENDING_COMMANDS", params, true);
				log.info("Result: " + params.get("resultMessage"));
			}
		} catch (Exception e) {
			// We intentionally do not rethrow any exceptions here
			// because we want this job continue running at next scheduled time and do not
			// want to drop this job or immediately repeat it.
			log.error("Error in Periodical EMV CAP Keys Update Job", e);
		} finally {
			if (lockAcquired) {
				releaseProcessLock(lockParams);
			}
		}
		log.debug("PeriodicalEMVCAPKeyFileUpdateJob FINISHED.");
	}

	private String getQueryForDevices() throws ServiceException {
		Object [] paramFirmware = {EMV_CAP_KEYS_FIRMWARE_CD};
		Object [] paramCardReaderModels = {EMV_CAP_KEYS_CARD_READERS_CD};
		//Object [] paramFileType = {EMV_CAP_KEYS_FILE_TYPE_ID};
		try {
			Results res = DataLayerMgr.executeQuery("GET_APP_SETTING", paramFirmware);
			if (!res.next())
				throw new ServiceException("Minimum firmware version is not set!");
			String minFirmwareVersion = res.getFormattedValue("value");
			if (minFirmwareVersion == null)
				throw new ServiceException("Minimum firmware version value is not set!");
			minFirmwareVersion = minFirmwareVersion.trim();
			if (minFirmwareVersion.isEmpty())
				throw new ServiceException("Minimum firmware version value is empty!");
			res = DataLayerMgr.executeQuery("GET_APP_SETTING", paramCardReaderModels);
			if (!res.next())
				throw new ServiceException("Card reader models are not set!");
			String cardReaderStr = res.getFormattedValue("value");
			if (cardReaderStr == null)
				throw new ServiceException("Card reader model values are not set!");
			String[] cardReaderModels = cardReaderStr.split(CARD_READER_DELIM);
			StringBuilder sb = new StringBuilder();
			for (String s : cardReaderModels) {
				String[] model = s.split(CARD_READER_MODEL_DELIM);
				if (model.length != 3 || model[0].trim().isEmpty()
						|| model[1].trim().isEmpty() || model[2].trim().isEmpty()) {
					log.warn("Wrong card reader model spec: [" + s + ']');
					continue;
				}
				if (sb.length() > 0)
					sb.append(" OR ");
				sb.append("(he.HOST_EQUIPMENT_MFGR = '").append(model[0].trim())
				.append("' AND he.HOST_EQUIPMENT_MODEL = '").append(model[1].trim())
				.append("' AND hs.HOST_SETTING_VALUE >= '").append(model[2].trim()).append("')");
			}
			if (sb.length() == 0)
				throw new ServiceException("Card reader model values are wrong or empty!");
			
			String query = (new StringBuilder())
					.append("SELECT DISTINCT d.DEVICE_ID, d.DEVICE_NAME, d.DEVICE_TYPE_ID FROM device.host h\r\n")
					.append("  JOIN device.DEVICE d on h.DEVICE_ID = d.DEVICE_ID\r\n")
					.append("  JOIN device.DEVICE_SETTING s on s.DEVICE_ID = d.DEVICE_ID\r\n")
					.append("  JOIN device.HOST_TYPE t on t.HOST_TYPE_ID = h.HOST_TYPE_ID\r\n")
					.append("  JOIN device.HOST_EQUIPMENT he on he.HOST_EQUIPMENT_ID = h.HOST_EQUIPMENT_ID\r\n")
					.append("  JOIN device.HOST_SETTING hs on hs.HOST_ID = h.HOST_ID\r\n")
					.append("  LEFT JOIN (SELECT DEVICE_ID FROM device.DEVICE_FILE_TRANSFER\r\n")
					.append("    WHERE FILE_TRANSFER_ID = :A AND DEVICE_FILE_TRANSFER_STATUS_CD = 1\r\n")
					.append("    ) dft on dft.DEVICE_ID = d.DEVICE_ID\r\n")
					.append("  WHERE h.HOST_ACTIVE_YN_FLAG = 'Y' AND d.DEVICE_ACTIVE_YN_FLAG = 'Y'\r\n")
					.append("    AND dft.DEVICE_ID IS NULL\r\n")
					.append("    AND s.DEVICE_SETTING_PARAMETER_CD = 'Firmware Version'\r\n")
					.append("    AND s.DEVICE_SETTING_VALUE >= '").append(minFirmwareVersion).append("'\r\n")
					.append("    AND t.HOST_TYPE_DESC = 'Bezel'\r\n")
					.append("    AND hs.HOST_SETTING_PARAMETER = 'Application Version'\r\n")
					.append("    AND (").append(sb).append(')').toString();
			return query;
		} catch (SQLException e1) {
			log.debug("SQL exception: ", e1);
			throw new ServiceException("SQL exception: ", e1);
		} catch (DataLayerException e1) {
			log.error("Mgr exception: ", e1);
			throw new ServiceException("Mgr exception: ", e1);
		}
	}

	boolean acquireProcessLock(Map<String, Object> lockParams) throws DataLayerException, SQLException, ConvertException {
		if (!InteractionUtils.lockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams)) {
			log.info(PROCESS_CD + " is already locked by instance " + lockParams.get("lockedBy"));
			return false;
		}
		return true;
	}

	void releaseProcessLock(Map<String, Object> lockParams) {
		try {
			InteractionUtils.unlockProcess(PROCESS_CD, LOCK_PROCESS_TOKEN_CD, lockParams);
		} catch (Exception e) {
			log.error("Error unlocking " + PROCESS_CD, e);
		}
	}

}
