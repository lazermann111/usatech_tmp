package com.usatech.device;

import java.io.Serializable;

/**
 * Device location info DTO
 */
public class DeviceLocation implements Serializable {
    private static final long serialVersionUID = -6060974381964415264L;

    private Long deviceId = null;
    private String country = null;
    private String state = null;
    private String city = null;
    private String postal = null;
    private Float latitude = null;
    private Float longitude = null;
    private String errorCode = null;

    public DeviceLocation() {
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return "DeviceLocation{" +
                "deviceId=" + deviceId +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", postal='" + postal + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", errorCode='" + errorCode + '\'' +
                '}';
    }
}
