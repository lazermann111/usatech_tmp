package com.usatech.ccs;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.CoteCache;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.Interceptor;
import simple.servlet.JspRequestDispatcherFactory;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

public class CampaignApprovedNotifyTask implements MessageChainTask {
	private static final Log log = Log.getLog();
		
	protected int poolSize = 2;
	protected int queueSize = 4;
	protected long waitCheckInterval = 5000;
	protected final Map<String, Integer> subdomainPortMap = new HashMap<String, Integer>();
	protected final CoteCache coteCache = new CoteCache();
	protected final JspRequestDispatcherFactory jrdf = new JspRequestDispatcherFactory();
	protected String marketingEmail;
	protected String usaliveBaseUrl;
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		long campaignId;
		try {
			Map<String, Object> attributes = step.getAttributes();
			campaignId = ConvertUtils.getLong(attributes.get("campaignId"));
			log.info("Processing campaign custom text approval notification for campaignId " + campaignId + "...");
			
			Results results = DataLayerMgr.executeQuery("GET_CAMPAIGN_APPROVED_NOTIFY_INFO", new Object[]{campaignId});
				while (results.next()){
					String email=ConvertUtils.getStringSafely(results.get("email"));
					if(!StringUtils.isBlank(email)){
						try{
							WebHelper.checkEmail(email);
							Map<String, Object> params = new HashMap<String, Object>();
							
							params.put("email", email);
							String firstName=ConvertUtils.getStringSafely(results.get("firstName"));
							String lastName=ConvertUtils.getStringSafely(results.get("lastName"));
							params.put("firstName", results.get("firstName"));
							params.put("promoText", results.get("promoText"));
							params.put("usaliveManagePromoUrl",getUsaliveBaseUrl()+"manage_promo_email.html?campaignId="+campaignId);
							Locale locale = Locale.getDefault(); 
							String body = Interceptor.intercept(jrdf, jrdf, "com/usatech/ccs/promo-approval-template.jsp", locale, null, params);
							params.clear();
							params.put("campaignId", campaignId);
							params.put("fromEmail", getMarketingEmail());
							params.put("fromName", "USA Technologies Marketing Team");
							params.put("body", body);
							params.put("subject", "USA Technologies Campaign Email notification");
							params.put("toEmail", email);
							StringBuilder toName = new StringBuilder();
							if (!StringHelper.isBlank(firstName)) {
								toName.append(firstName);
								if (!StringHelper.isBlank(lastName))
									toName.append(" ").append(lastName);
							} else{
								toName.append(email);
							}
							params.put("toName",toName);
							DataLayerMgr.executeCall("SEND_CAMPAIGN_APPROVED_NOTIFY", params, true);
						}catch(MessagingException e){
							log.info("Cannot notify user due to invalid email address. campaignId="+campaignId);
						}
						
					}
					
				}
				return 1;
		} catch (Exception e) {
			throw new ServiceException("Error processing campaign approval notify", e);
		}
	}
	

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}

	public Integer getSubdomainPort(String subdomain) {
		return subdomainPortMap.get(subdomain);
	}

	public void setSubdomainPort(String subdomain, Integer port) {
		subdomainPortMap.put(subdomain, port);
	}

	public CoteCache getCoteCache() {
		return coteCache;
	}

	public String getMarketingEmail() {
		return marketingEmail;
	}

	public void setMarketingEmail(String marketingEmail) {
		this.marketingEmail = marketingEmail;
	}

	public String getUsaliveBaseUrl() {
		return usaliveBaseUrl;
	}

	public void setUsaliveBaseUrl(String usaliveBaseUrl) {
		this.usaliveBaseUrl = usaliveBaseUrl;
	}
	
	
}
