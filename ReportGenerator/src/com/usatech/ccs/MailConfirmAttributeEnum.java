package com.usatech.ccs;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

import com.usatech.app.Attribute;

public enum MailConfirmAttributeEnum implements Attribute {
	OB_EMAIL_QUEUE_ID("ob_email_queue_id", "Long identifier; Outbound Email Queue Id - attribute key must match data layer query"),
	EMAIL_SENT("EMAIL_SENT","Boolean; Flag whether the identified email was sent ok.");
	
	private final String value;
	private final String description;

	protected final static EnumStringValueLookup<MailConfirmAttributeEnum> lookup = new EnumStringValueLookup<MailConfirmAttributeEnum>(
			MailConfirmAttributeEnum.class);

	public static MailConfirmAttributeEnum getByValue(String value)
			throws InvalidValueException {
		return lookup.getByValue(value);
	}

	/**
	 * Internal constructor.
	 * 
	 * @param value
	 */
	private MailConfirmAttributeEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}

	/**
	 * Get the intrinsic value of the type.
	 * 
	 * @return a char
	 */
	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}
}