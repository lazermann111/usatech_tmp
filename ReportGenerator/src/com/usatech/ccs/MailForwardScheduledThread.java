package com.usatech.ccs;

import static com.usatech.ccs.MailConfirmAttributeEnum.EMAIL_SENT;
import static com.usatech.ccs.MailConfirmAttributeEnum.OB_EMAIL_QUEUE_ID;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.constants.MailAttributeEnum;

import simple.app.BasicQoS;
import simple.app.Publisher;
import simple.app.SelfProcessor;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.text.StringUtils;

/**
 * A version of MailForward.pl for the AppLayer
 * Does duplicate detection
 * Reads the cc list file from the file system if configured
 * Combines Name and Address into To and From field per RFC822
 * Queues persistent 'mail' tasks for the SendMail task to take 
 * care of.
 * Uses the old SQL, but note: does not do retries (left to the
 * Queue Layer).
 * 
 * Messages are ALWAYS marked attempted to send here.
 *  
 * @author phorsfield
 *
 */
public class MailForwardScheduledThread implements SelfProcessor {

	private static final Log log = Log.getLog();

	// === Data Layer constants - see applayer-data-layer.xml ===
	private static final String MAIL_FORWARD_PENDING_QUERY_NAME = "MAIL_FORWARD_PENDING";
	private static final String MAIL_FORWARD_DELETE_DUP = "MAIL_FORWARD_DELETE_DUP";
	private static final String MAIL_FORWARD_MARK_QUEUED = "MAIL_FORWARD_MARK_QUEUED";
	@SuppressWarnings("unused")
	private static final String MAIL_FORWARD_MARK_SENT = "MAIL_FORWARD_MARK_SENT";
	
	/* This lock identifier ties to the following database configuration lines 
	 * INSERT INTO "ENGINE"."APP_SETTING" (APP_SETTING_CD, APP_SETTING_DESC) VALUES ('MAIL_FORWARD_LOCK', 'Record for locking Mail Forward in an AppLayer instance')
	 * INSERT INTO "ENGINE"."APP_SETTING" (APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC) VALUES ('MAIL_FORWARD_MAX_DURATION_SEC', '300', 'Number of seconds to allow the MAIL FORWARD code in App Layer to stay locked')
	 * */	
	private static final String MAIL_FORWARD_LOCK = "MAIL_FORWARD";

	/**
	 * QoS guarantee: Mail Tasks are persistent and never expire.
	 */
	protected final BasicQoS mailForwardSendQos = new BasicQoS(0, 0, true, null);

	// === Configuration Parameters ===
	protected int appInstance;
	protected int maxItems = 100;
	protected String ccListLocation = null;
	protected String ccList = null;
	protected Publisher<ByteInput> publisher;
	protected String sendMailQueueName = "usat.ccs.email";
	protected String tazdbaMailUpdateQueue = "usat.ccs.email.tazdba";
	protected MailForwardFilterParameters filter = new MailForwardFilterParameters(5,1,5);
	// === END Configuration Parameters ===

	// ============== Bean Definitions =================
	public static class MailTask {

		public MailTask() {}
		
		public int getOb_email_queue_id() {
			return ob_email_queue_id;
		}

		public void setOb_email_queue_id(int ob_email_queue_id) {
			this.ob_email_queue_id = ob_email_queue_id;
		}

		public Date getOb_email_sent_success_ts() {
			return ob_email_sent_success_ts;
		}

		public void setOb_email_sent_success_ts(Date ob_email_sent_success_ts) {
			this.ob_email_sent_success_ts = ob_email_sent_success_ts;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime
					* result
					+ ((ob_email_from_email_addr == null) ? 0
							: ob_email_from_email_addr.hashCode());
			result = prime
					* result
					+ ((ob_email_from_name == null) ? 0 : ob_email_from_name
							.hashCode());
			result = prime * result
					+ ((ob_email_msg == null) ? 0 : ob_email_msg.hashCode());
			result = prime
					* result
					+ ((ob_email_subject == null) ? 0 : ob_email_subject
							.hashCode());
			result = prime
					* result
					+ ((ob_email_to_email_addr == null) ? 0
							: ob_email_to_email_addr.hashCode());
			result = prime
					* result
					+ ((ob_email_to_name == null) ? 0 : ob_email_to_name
							.hashCode());
			result = prime * result
					+ ((ob_email_content == null) ? 0 : ob_email_content.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MailTask other = (MailTask) obj;
			if (ob_email_from_email_addr == null) {
				if (other.ob_email_from_email_addr != null)
					return false;
			} else if (!ob_email_from_email_addr
					.equals(other.ob_email_from_email_addr))
				return false;
			if (ob_email_from_name == null) {
				if (other.ob_email_from_name != null)
					return false;
			} else if (!ob_email_from_name.equals(other.ob_email_from_name))
				return false;
			if (ob_email_msg == null) {
				if (other.ob_email_msg != null)
					return false;
			} else if (!ob_email_msg.equals(other.ob_email_msg))
				return false;
			if (ob_email_subject == null) {
				if (other.ob_email_subject != null)
					return false;
			} else if (!ob_email_subject.equals(other.ob_email_subject))
				return false;
			if (ob_email_to_email_addr == null) {
				if (other.ob_email_to_email_addr != null)
					return false;
			} else if (!ob_email_to_email_addr
					.equals(other.ob_email_to_email_addr))
				return false;
			if (ob_email_to_name == null) {
				if (other.ob_email_to_name != null)
					return false;
			} else if (!ob_email_to_name.equals(other.ob_email_to_name))
				return false;
			if (ob_email_content == null) {
				if (other.ob_email_content != null)
					return false;
			} else if (!ob_email_content.equals(other.ob_email_content))
				return false;
			return true;
		}

		public String getOb_email_from_email_addr() {
			return ob_email_from_email_addr;
		}

		public void setOb_email_from_email_addr(String ob_email_from_email_addr) {
			this.ob_email_from_email_addr = ob_email_from_email_addr;
		}

		public String getOb_email_from_name() {
			return ob_email_from_name;
		}

		public void setOb_email_from_name(String ob_email_from_name) {
			this.ob_email_from_name = ob_email_from_name;
		}

		public String getOb_email_to_email_addr() {
			return ob_email_to_email_addr;
		}

		public void setOb_email_to_email_addr(String ob_email_to_email_addr) {
			this.ob_email_to_email_addr = ob_email_to_email_addr;
		}

		public String getOb_email_to_name() {
			return ob_email_to_name;
		}

		public void setOb_email_to_name(String ob_email_to_name) {
			this.ob_email_to_name = ob_email_to_name;
		}

		public String getOb_email_subject() {
			return ob_email_subject;
		}

		public void setOb_email_subject(String ob_email_subject) {
			this.ob_email_subject = ob_email_subject;
		}

		public String getOb_email_msg() {
			return ob_email_msg;
		}

		public void setOb_email_msg(String ob_email_msg) {
			this.ob_email_msg = ob_email_msg;
		}
		
		public String getOb_email_content() {
			return ob_email_content;
		}

		public void setOb_email_content(String ob_email_content) {
			this.ob_email_content = ob_email_content;
		}

		private int ob_email_queue_id;
		private Date ob_email_sent_success_ts;
		private String ob_email_from_email_addr;
		private String ob_email_from_name;
		private String ob_email_to_email_addr;
		private String ob_email_to_name;
		private String ob_email_subject;
		private String ob_email_msg;
		private String ob_email_content;
		
		public String to() {
			StringBuilder sb = new StringBuilder();
			String[] addrs = StringUtils.splitQuotesAnywhere(ob_email_to_email_addr, ",;", "\"", false);
			String[] names = StringUtils.splitQuotesAnywhere(ob_email_to_name, ",;", "\"", false);
			if(addrs != null)
				for(int i = 0; i < addrs.length; i++) {
					if(StringUtils.isBlank(addrs[i]))
						continue;
					if(sb.length() > 0)
						sb.append(';');
					if(names == null || i >= names.length || StringUtils.isBlank(names[i]))
						sb.append(addrs[i]);
					else
						sb.append('"').append(StringUtils.escape(names[i], "\\\"\r\n".toCharArray(), '\\')).append('"');
					sb.append(" <").append(addrs[i]).append('>');
				}
			return sb.toString();
		}

		public String from() {
			StringBuilder sb = new StringBuilder();
			if(StringUtils.isBlank(ob_email_from_name))
				sb.append(ob_email_from_email_addr);
			else
				sb.append('"').append(StringUtils.escape(ob_email_from_name, "\\\"\r\n".toCharArray(), '\\')).append('"');
			sb.append(" <").append(ob_email_from_email_addr).append('>');
			return sb.toString();
		}
	}

	// ============== END Bean Definitions =================
	
	// ============== Getter/Setters =================
	public String getSendMailQueueName() {
		return sendMailQueueName;
	}

	public void setSendMailQueueName(String sendMailQueueName) {
		this.sendMailQueueName = sendMailQueueName;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}


	public int getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(int appInstance) {
		this.appInstance = appInstance;
	}

	public int getMaxItems() {
		return maxItems;
	}

	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}

	public String getCcListLocation() {
		return ccListLocation;
	}

	public void setCcListLocation(String ccListLocation) {
		this.ccListLocation = ccListLocation;
	}

	public String getTazdbaMailUpdateQueue() {
		return tazdbaMailUpdateQueue;
	}

	public void setTazdbaMailUpdateQueue(String tazdbaMailUpdateQueue) {
		this.tazdbaMailUpdateQueue = tazdbaMailUpdateQueue;
	}

	public MailForwardFilterParameters getFilter() {
		return filter;
	}

	public void setFilter(MailForwardFilterParameters filter) {
		this.filter = filter;
	}

	public void initialize() throws ServiceException {
		if (publisher == null) {
			throw new ServiceException(
					"'publisher' not configured; Publisher property for MailForwardScheduledTask has not been configured in app properties.");
		}

		if(ccListLocation == null && ccList == null)
		{
			log.info("Neither 'ccList' nor 'ccListLocation' configured in app properties. Will not cc' on emails.");
		}
		
		if (ccList == null && ccListLocation != null)
		{
			File ccFile = new File(ccListLocation);
			try {
				String lineSeparated = simple.io.IOUtils.readFully(new FileReader(ccFile));
				String[] lines = lineSeparated.split("\r?\n");
				ccList = StringUtils.join(lines, ",");
			} catch (FileNotFoundException e) {
				log.warn("Not cc'ing; File Not Found reading cc list in " + ccListLocation);
			} catch (IOException e) {
				log.warn("Not cc'ing; IOException reading cc list in " + ccListLocation);
			}			
		}		
	}

	// =========== END Getter/Setters =================

	@Override
	public void process() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("maxItems", maxItems);
		try {			
			Connection conn = null;
			try {
				conn = DataLayerMgr.getConnectionForCall(MAIL_FORWARD_PENDING_QUERY_NAME);
				
				try {
					if(!InteractionUtils.lockProcess(conn, MAIL_FORWARD_LOCK, String.valueOf(appInstance), params)) {
						log.info("Mail Forward is already locked by instance " + params.get("lockedBy"));
						return;
					}
				} catch(ConvertException e) {
					throw new RuntimeException("Failed to lock for mail forward", e);
				} catch(DataLayerException e) {
					throw new RuntimeException("Failed to lock for mail forward", e);
				} catch(SQLException e) {
					throw new RuntimeException("Failed to lock for mail forward", e);
				}

				Set<MailTask> uniqueEmails = new HashSet<MailTask>();
				List<MailTask> duplicates = new ArrayList<MailTask>();
				try {

					Results results = DataLayerMgr.executeQuery(conn, MAIL_FORWARD_PENDING_QUERY_NAME, filter);

					List<MailTask> mail = Arrays.asList(results.toBeanArray(MailTask.class));

					for(MailTask m : mail) {
						if(uniqueEmails.contains(m)) {
							duplicates.add(m);
							log.warn("Duplicate Detected! Email " + m.ob_email_queue_id + " is a duplicate of another queued email");
						}
						else
						{
							uniqueEmails.add(m);
						}
					}

					if(duplicates.size() > 0)
					{
						deleteEmails(conn, duplicates);
					}
					if(uniqueEmails.size() > 0) {
						sendEmails(conn, uniqueEmails);
					}
					else
					{
						log.debug("No current emails to send at this time!");
					}
				} catch(SQLException e) {
					throw new RuntimeException("Failed to process mail forward", e);
				} catch(DataLayerException e) {
					throw new RuntimeException("Failed to process mail forward", e);
				} catch(BeanException e) {
					throw new RuntimeException("Failed to process mail forward", e);
				} catch(ServiceException e) {
					throw new RuntimeException("Failed to enqueue mail", e);
				} finally {
					try {
						InteractionUtils.unlockProcess(conn, MAIL_FORWARD_LOCK, String.valueOf(appInstance), params);
					} catch(DataLayerException e) {
						throw new RuntimeException("Failed to unlock after mail forward", e);
					} catch(SQLException e) {
						throw new RuntimeException("Failed to unlock after mail forward", e);
					}
				}
			} finally {
				if(conn != null)
				{
					conn.close();
				}
			}
		} catch (SQLException e) {
			log.warn("Failed to process mail forward", e);
		} catch (DataLayerException e) {
			log.warn("Failed to process mail forward", e);
		}
	}

	/**
	 * Send the good emails to the SendEmail task
	 * @param conn
	 * @param uniqueEmails
	 * @throws ServiceException
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	private void sendEmails(Connection conn, Set<MailTask> uniqueEmails) throws ServiceException, SQLException, DataLayerException {

		for(MailTask m : uniqueEmails)
		{
			log.debug("Attempting to forward email to '"+ m.to() + "' from '" + m.from() + "'");
			
			MessageChain mc = new com.usatech.app.MessageChainV11();
	
			MessageChainStep sendStep = mc.addStep(this.sendMailQueueName, false);
	
			sendStep.setAttribute(MailAttributeEnum.BODY, m.getOb_email_content() == null ? m.getOb_email_msg() : m.getOb_email_content());
			sendStep.setAttribute(MailAttributeEnum.SUBJECT, m.getOb_email_subject());
			sendStep.setAttribute(MailAttributeEnum.TO, m.to());			
			sendStep.setAttribute(MailAttributeEnum.FROM, m.from());
			sendStep.setAttribute(MailAttributeEnum.CC, this.ccList);
			
			MessageChainStep okStep = mc.addStep(this.tazdbaMailUpdateQueue);
			MessageChainStep failStep = mc.addStep(this.tazdbaMailUpdateQueue);

			okStep.setAttribute(OB_EMAIL_QUEUE_ID, m.getOb_email_queue_id());
			okStep.setAttribute(EMAIL_SENT, true);

			failStep.setAttribute(OB_EMAIL_QUEUE_ID, m.getOb_email_queue_id());
			failStep.setAttribute(EMAIL_SENT, false);
			
			sendStep.setNextSteps(0, okStep);
			sendStep.setNextSteps(1, failStep); 

			MessageChainService.publish(mc, publisher, null, mailForwardSendQos);

			DataLayerMgr.executeUpdate(conn, MAIL_FORWARD_MARK_QUEUED, m);		
		}
		conn.commit();
	}

	/**
	 * Delete duplicates directly from the database.
	 * @param conn
	 * @param duplicates
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	private void deleteEmails(Connection conn, List<MailTask> duplicates) throws SQLException, DataLayerException {

		for(MailTask m : duplicates)
		{
			DataLayerMgr.executeUpdate(conn, MAIL_FORWARD_DELETE_DUP, m);
		}
		conn.commit();
	}

}
