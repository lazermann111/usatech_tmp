package com.usatech.ccs;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.CoteCache;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.Interceptor;
import simple.servlet.JspRequestDispatcherFactory;
import simple.text.StringUtils;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.NotifyingBlockingThreadPoolExecutor;

import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.util.CampaignUtils;
import com.usatech.layers.common.util.StringHelper;

public class CampaignBlastTask implements MessageChainTask {
	private static final Log log = Log.getLog();
		
	protected int poolSize = 2;
	protected int queueSize = 4;
	protected long waitCheckInterval = 5000;
	protected int maxUsedDays = 180;
	protected int maxProximityMiles = 30;
	protected int maxDevices = 20;
	protected float promoNewDays = 5.0f;
	protected float promoSoonDays = 5.0f;
	protected NotifyingBlockingThreadPoolExecutor executor = null;
	protected final Map<String, Integer> subdomainPortMap = new HashMap<String, Integer>();
	protected final CoteCache coteCache = new CoteCache();
	protected final JspRequestDispatcherFactory jrdf = new JspRequestDispatcherFactory();
	protected String marketingEmail;
	protected String usaliveBaseUrl;
	
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		if (executor == null)
			executor = new NotifyingBlockingThreadPoolExecutor(poolSize, queueSize, new CustomThreadFactory(Thread.currentThread().getName() + '-', true));
		MessageChainStep step = taskInfo.getStep();
		long campaignBlastId;
		try {
			Map<String, Object> attributes = step.getAttributes();
			campaignBlastId = ConvertUtils.getLong(attributes.get("campaignBlastId"));
			log.info("Processing campaign blast for campaignBlastId " + campaignBlastId + "...");
						
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("newDays", getPromoNewDays());
			params.put("soonDays", getPromoSoonDays());
			params.put("campaignBlastId", campaignBlastId);
			params.put("preferredSubdomains", subdomainPortMap.keySet());
			
			Results productResults = DataLayerMgr.executeQuery("GET_CAMPAIGN_BLAST_PRODUCTS", params);
			String detailsUpdate = "purchase";
			
			String products = "";
			if(productResults.next()) {
				products = "Products participating in campaign: ";
				products = products + productResults.getValue("productName", String.class);
			}
			while (productResults.next()){
				products = products + ", ";
				products = products + productResults.getValue("productName", String.class);
			}
			if (products.length() > 0){
				products = products + ".";
				detailsUpdate = "product";
			}
			
			Results results = DataLayerMgr.executeQuery("GET_CAMPAIGN_BLAST_DETAILS", params);
			String campaignDetails;
			String campaignTerms;
			int campaignTypeId;
			if(results.next()) {
				campaignTypeId = ConvertUtils.getInt(results.getValue("campaignTypeId"));
				String title = results.getValue("campaignDescription", String.class);
				String subdomainUrl = results.getValue("subdomainUrl", String.class);
				String subdirectory = results.getValue("subdirectory", String.class);
				StringBuilder baseUrl = new StringBuilder();
				Integer port = subdomainPortMap.get(subdomainUrl);
				if(port != null && port > 0 && (port % 1000) != 443)
					baseUrl.append("http://");
				else
					baseUrl.append("https://");
				baseUrl.append(subdomainUrl);
				if(port != null && port > 0)
					baseUrl.append(':').append(port);
				if(!StringUtils.isBlank(subdirectory))
					baseUrl.append(subdirectory);
				baseUrl.append('/');
				params.put("baseUrl", baseUrl.toString());

				String translatorUrl = subdomainUrl;
				if(!StringUtils.isBlank(subdirectory))
					translatorUrl = translatorUrl + subdirectory;
				Locale locale = Locale.getDefault(); // TODO: get this from customer preference

				String domain = subdomainUrl;
				String sub = "communication";
				int pos = subdomainUrl.lastIndexOf('.');
				if(pos > 0) {
					pos = subdomainUrl.lastIndexOf('.', pos - 1);
					if(pos > 0) {
						domain = subdomainUrl.substring(pos + 1);
						sub = subdomainUrl.substring(0, pos);
					}
				}
				String customerName = results.getValue("customerName", String.class);
				Translator translator = TranslatorFactory.getDefaultFactory().getTranslator(locale, translatorUrl);
				String fromEmail = translator.translate("consumer-comm-email-from-email", "{0,NULL,{0},{1}}@{2}", StringUtils.isBlank(customerName) ? null : customerName.toLowerCase().replaceAll("\\W", "_"), sub, domain);
				String fromName = translator.translate("campaign-blast-email-from-name", "Promotions by {0}", StringUtils.isBlank(customerName) ? "USA Technologies" : customerName);
				String subject = translator.translate("campaign-blast-email-subject-" + campaignTypeId, "We are running a promotion!");
				String moreColor = translator.translate("prepaid-more-color", "#e74b34");

				switch(campaignTypeId) {
					case 2: // Replenish Reward
					case 6:
						BigDecimal percent = results.getValue("campaignDiscountPercent", BigDecimal.class);
						BigDecimal threshold = results.getValue("campaignThreshold", BigDecimal.class);
						if(StringUtils.isBlank(title))
							title = "Replenish Bonus";
						campaignTerms = translator.translate("prepaid-promo-replenish-reward-terms", "{0}", title);
						if(campaignTypeId==2){
							campaignDetails = translator.translate("prepaid-promo-replenish-reward-detail", "Earn {5,CURRENCY} for every {4,CURRENCY} you add to your card.", title, percent, results.getValue("campaignEndDate"), results.getValue("attentionType"), threshold, threshold.multiply(percent));
						}else{
							campaignDetails = translator.translate("prepaid-flat-replenish-reward-detail", "Earn {1,CURRENCY} when you add {0,CURRENCY} to your card.", threshold, results.getValue("purchaseDiscount"));
						}
						DataLayerMgr.executeCall("PROCESS_CAMPAIGN_BLAST_CASHBACK", new Object[] { campaignBlastId }, true);
						break;
					case 3: // Spend Reward
						if(StringUtils.isBlank(title))
							title = "Bonus Cash";
						percent = results.getValue("campaignDiscountPercent", BigDecimal.class);
						threshold = results.getValue("campaignThreshold", BigDecimal.class);
						campaignTerms = translator.translate("prepaid-promo-spend-reward-terms", "{0}", title, percent, results.getValue("campaignEndDate"), threshold, threshold.multiply(percent));
						campaignDetails = translator.translate("prepaid-promo-spend-reward-detail", "Earn {5,CURRENCY} Bonus Cash for every {4,CURRENCY} you spend. {5}", title, percent, results.getValue("campaignEndDate"), results.getValue("attentionType"), threshold, threshold.multiply(percent), products);
						DataLayerMgr.executeCall("PROCESS_CAMPAIGN_BLAST_CASHBACK", new Object[] { campaignBlastId }, true);
						break;
					case 1: // Loyalty Discount
					case 5: // Purchase Discount
						String recurSchedText;
						String s = results.getFormattedValue("campaignRecurSchedule");
						if(StringUtils.isBlank(s) || s.trim().equalsIgnoreCase("D"))
							recurSchedText = "";
						else
							recurSchedText = CampaignUtils.readRecurSchedule(s);
						if(campaignTypeId==1){//loyalty discount
							if(StringUtils.isBlank(title))
								title = results.getFormattedValue("campaignDiscountPercent") + " off Sale";
							campaignTerms = translator.translate("prepaid-promo-campaign-terms", "{0}", title, results.getValue("campaignDiscountPercent"), results.getValue("campaignEndDate"));
							campaignDetails = translator.translate("prepaid-promo-campaign-detail", "Every " + detailsUpdate + " is {1,PERCENT} off {4}{3,MATCH,SOON= until {2,DATE,MM/dd/yyyy}} at all participating locations. {5}", title, results.getValue("campaignDiscountPercent"), results.getValue("campaignEndDate"), results.getValue("attentionType"), recurSchedText, products);
						}else{//purchase discount
							if(StringUtils.isBlank(title)){
		            title = "Purchase Discount";
			      	}
							if (products.length() == 0) {
								detailsUpdate = "line item";
							}
							campaignTerms = translator.translate("prepaid-purchase-discount-terms", "{0}", title);
							campaignDetails = translator.translate("prepaid-purchase-discount-detail", "Every " + detailsUpdate + " in the sale has up to {1,CURRENCY} off {4}{3,MATCH,SOON= until {2,DATE,MM/dd/yyyy}} at all participating locations.", 
		          		title, results.getValue("purchaseDiscount"), results.getValue("campaignEndDate"), results.getValue("attentionType"));  
						}
						DataLayerMgr.executeCall("PROCESS_CAMPAIGN_BLAST", new Object[] { campaignBlastId }, true);
						break;
					case 4:
						Integer nthVendFreeNum =results.getValue("nthVendFreeNum", Integer.class);
						BigDecimal freeVendMaxAmount = results.getValue("freeVendMaxAmount", BigDecimal.class);
						Integer freeVendMaxCount = results.getValue("freeVendMaxCount", Integer.class);
						if(StringUtils.isBlank(title))
			                title = "Free Vend";
						nthVendFreeNum=nthVendFreeNum-1;
						String freeMaxCount = "";
						if (freeVendMaxCount >=0) {
							freeMaxCount=". Free " + detailsUpdate + " max limit is " + freeVendMaxCount;
						}
						campaignTerms = translator.translate("prepaid-promo-campaign-terms", "{0}", title);
						campaignDetails = translator.translate("prepaid-promo-free-reward-detail", "Earn a free " + detailsUpdate + " up to {1,CURRENCY} for every {2} " + detailsUpdate + freeMaxCount + ". {3}", 
			            		title, freeVendMaxAmount,nthVendFreeNum, products);   
						DataLayerMgr.executeCall("PROCESS_CAMPAIGN_BLAST", new Object[] { campaignBlastId }, true);
						break;
					default:
						log.error("Error unknown campaign type:" + campaignTypeId);
						return 1;
				}
				results = DataLayerMgr.executeQuery("GET_CAMPAIGN_BLAST_CONSUMERS", new Object[]{getMarketingEmail(),campaignBlastId});
				Semaphore semaphore = new Semaphore(0);
				AtomicReference<Exception> exceptionHolder = new AtomicReference<Exception>();
				while (results.next())
					executor.execute(new CampaignBlastConsumerTask(
							campaignTypeId,
							results.getValue("campaignBlastConsumerId", long.class),
							campaignBlastId, 
							results.getValue("consumerId", long.class),
							results.getFormattedValue("consumerEmailAddr1"),
							results.getFormattedValue("consumerFName"),
							results.getFormattedValue("consumerLName"),
							campaignTerms,
							campaignDetails,
							baseUrl.toString(),
							subdomainUrl, 
							subdirectory, 
							fromEmail, 
							fromName, 
							subject,
							locale,
							semaphore,
							exceptionHolder,
							moreColor,
							results.getFormattedValue("isTest").equals("Y"),
							results.getValue("preferredCommTypeId", int.class)
						)
					);
				log.info("Submitted " + results.getRowCount() + " entries for processing campaign blast for campaignBlastId " + campaignBlastId);
				semaphore.acquire(results.getRowCount());
				if(exceptionHolder.get() == null)
					log.info("Finished processing campaign blast for campaignBlastId " + campaignBlastId);
				else
					throw exceptionHolder.get();
			} else {
				log.warn("Error getting campaign blast details");
				return 1;
			}			
		} catch (Exception e) {
			throw new ServiceException("Error processing campaign blast", e);
		}
		return 0;
	}
	
	protected class CampaignBlastConsumerTask implements Runnable {
		protected final long campaignTypeId;
		protected final long campaignBlastConsumerId;
		protected final long campaignBlastId;
		protected final long consumerId;
		protected final String consumerEmailAddr1;
		protected final String consumerFName;
		protected final String consumerLName;
		protected final String campaignTitle;
		protected final String campaignInfo;
		protected final Locale locale;
		protected final String baseUrl;
		protected final String subdomainUrl;
		protected final String subdirectory;
		protected final String fromEmail;
		protected final String fromName;
		protected String subject;
		protected final Semaphore semaphore;
		protected final AtomicReference<Exception> exceptionHolder;
		protected final String moreColor;
		protected final boolean isTest;
		protected int preferredCommTypeId;
		
		public CampaignBlastConsumerTask(long campaignTypeId, long campaignBlastConsumerId, long campaignBlastId, long consumerId, String consumerEmailAddr1, String consumerFName, String consumerLName, String campaignTitle, String campaignInfo, String baseUrl, String subdomainUrl, String subdirectory, String fromEmail, String fromName, String subject, Locale locale, Semaphore semaphore,
				AtomicReference<Exception> exceptionHolder, String moreColor, boolean isTest, int preferredCommTypeId) {
			this.campaignTypeId=campaignTypeId;
			this.campaignBlastConsumerId = campaignBlastConsumerId;
			this.campaignBlastId = campaignBlastId;
			this.consumerId = consumerId;
			this.consumerEmailAddr1 = consumerEmailAddr1;
			this.consumerFName = consumerFName;
			this.consumerLName = consumerLName;
			this.campaignInfo = campaignInfo;
			this.campaignTitle = campaignTitle;
			this.baseUrl = baseUrl;
			this.subdomainUrl = subdomainUrl;
			this.subdirectory = subdirectory;
			this.fromEmail = fromEmail;
			this.fromName = fromName;
			this.subject = subject;
			this.locale = locale;
			this.semaphore = semaphore;
			this.exceptionHolder = exceptionHolder;
			this.moreColor = moreColor;
			this.isTest=isTest;
			this.preferredCommTypeId=preferredCommTypeId;
		}
		
		public void run() {
			try {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("maxUsedDays", maxUsedDays);
				params.put("consumerId", consumerId);
				params.put("campaignBlastId", campaignBlastId);
				params.put("maxProximityMiles", maxProximityMiles);
				params.put("maxDevices", maxDevices);
				Results results = null;
				if(campaignTypeId==1||campaignTypeId==4||campaignTypeId==5){
					if(isTest){
						results=DataLayerMgr.executeQuery("GET_CAMPAIGN_BLAST_CONSUMER_DEVICES_TEST", params);
					}else{
						results=DataLayerMgr.executeQuery("GET_CAMPAIGN_BLAST_CONSUMER_DEVICES", params);
					}
				}else{
					results=DataLayerMgr.executeQuery("GET_CAMPAIGN_BLAST_CARDS", params);
				}
				Results promoTextResults = DataLayerMgr.executeQuery("GET_CAMPAIGN_PROMO_TEXT", params);
				
				if(!results.isGroupEnding(0)) {
					params.put("isTest", isTest);
					params.put("userId", consumerId);
					params.put("userName", consumerEmailAddr1);
					params.put("firstName", consumerFName);
					params.put("lastName", consumerLName);
					params.put("baseUrl", baseUrl);
					params.put("promoTitle", campaignTitle);
					params.put("promoDetail", campaignInfo);
					params.put("campaignTypeId", campaignTypeId);
					if(campaignTypeId==1||campaignTypeId==4||campaignTypeId==5){
						params.put("locations", results);
					}else{
						params.put("cards", results);
					}
					DataLayerMgr.selectInto("GET_CAMPAIGN_BLAST_REQUEST_USER", new Object[]{campaignBlastId}, params);
					int customerId=ConvertUtils.getIntSafely(params.get("customerId"), -1);
					if(customerId>0){
						params.put("companyLogoUrl", getUsaliveBaseUrl()+"get_company_logo.i?customerId="+customerId);
						DataLayerMgr.selectInto("HAS_COMPANY_LOGO", new Object[]{"company_logo_"+customerId}, params);
					}
					params.put("customerId",customerId);
					if(promoTextResults.next()){
						params.put("promoText",promoTextResults.get("promoText"));
						String toMarketing=ConvertUtils.getStringSafely(promoTextResults.get("toMarketing"));
						params.put("toMarketing",toMarketing);
						if(toMarketing!=null&&toMarketing.equalsIgnoreCase("Y")){
							//DataLayerMgr.selectInto("GET_CAMPAIGN_BLAST_REQUEST_USER", new Object[]{campaignBlastId}, params);
							if(StringUtils.isBlank(subject)){
								subject="APPROVAL REQUIRED:";
							}else{
								subject="APPROVAL REQUIRED:"+subject;
							}
						}
						if(promoTextResults.get("passcode")!=null){
							params.put("usalivePromoApproveUrl",getUsaliveBaseUrl()+"approve_promo_text.html"+"?campaignId="+promoTextResults.get("campaignId")+"&passcode="+promoTextResults.get("passcode"));
						}
					}
					
					params.put("version", ConsumerCommunicationTask.getPrepaidVersion());
					params.put(CoteCache.class.getName() + ".Retriever", coteCache.createRetriever(subdomainUrl, subdirectory, consumerId));
					params.put("moreColor", moreColor);

					String body = null;
					
					String fromNameText="";
					if(preferredCommTypeId==1||isTest){
						body=Interceptor.intercept(jrdf, jrdf, "com/usatech/ccs/promo-template.jsp", locale, null, params);
						params.clear();
						params.put("subject", subject);
						fromNameText=fromName;
					}else{
						params.clear();
						params.put("subject", "See "+baseUrl);
						body=campaignInfo;
						if(!StringUtils.isBlank(fromName)){
							fromNameText=fromName.replaceFirst("Promotions by ", "");
						}
					}
					
					params.put("fromEmail", fromEmail);
					params.put("fromName", fromNameText);
					params.put("body", body);
					params.put("toEmail", consumerEmailAddr1);
					StringBuilder toName = new StringBuilder();
					if (!StringHelper.isBlank(consumerFName)) {
						toName.append(consumerFName);
						if (!StringHelper.isBlank(consumerLName))
							toName.append(" ").append(consumerLName);
					} else
						toName.append(consumerEmailAddr1);
					params.put("toName", toName.toString());
					params.put("campaignBlastConsumerId", campaignBlastConsumerId);
					DataLayerMgr.executeCall("SEND_CONSUMER_BLAST_EMAIL", params, true);
				} else {
					params.clear();
					params.put("campaignBlastConsumerId", campaignBlastConsumerId);
					DataLayerMgr.executeCall("MARK_CONSUMER_BLAST_EMAIL_PROCESSED", params, true);
				}
			} catch (Exception e) {
				log.error("Error sending campaign blast email for campaignBlastId: " + campaignBlastId + ", consumerId: " + consumerId, e);
				exceptionHolder.compareAndSet(null, e);
			} finally {
				semaphore.release();
			}
		}
	}

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}

	public int getMaxUsedDays() {
		return maxUsedDays;
	}

	public void setMaxUsedDays(int maxUsedDays) {
		this.maxUsedDays = maxUsedDays;
	}

	public int getMaxProximityMiles() {
		return maxProximityMiles;
	}

	public void setMaxProximityMiles(int maxProximityMiles) {
		this.maxProximityMiles = maxProximityMiles;
	}

	public int getMaxDevices() {
		return maxDevices;
	}

	public void setMaxDevices(int maxDevices) {
		this.maxDevices = maxDevices;
	}

	public float getPromoNewDays() {
		return promoNewDays;
	}

	public void setPromoNewDays(float promoNewDays) {
		this.promoNewDays = promoNewDays;
	}

	public float getPromoSoonDays() {
		return promoSoonDays;
	}

	public void setPromoSoonDays(float promoSoonDays) {
		this.promoSoonDays = promoSoonDays;
	}

	public Integer getSubdomainPort(String subdomain) {
		return subdomainPortMap.get(subdomain);
	}

	public void setSubdomainPort(String subdomain, Integer port) {
		subdomainPortMap.put(subdomain, port);
	}

	public CoteCache getCoteCache() {
		return coteCache;
	}

	public String getMarketingEmail() {
		return marketingEmail;
	}

	public void setMarketingEmail(String marketingEmail) {
		this.marketingEmail = marketingEmail;
	}

	public String getUsaliveBaseUrl() {
		return usaliveBaseUrl;
	}

	public void setUsaliveBaseUrl(String usaliveBaseUrl) {
		this.usaliveBaseUrl = usaliveBaseUrl;
	}
	
	
}
