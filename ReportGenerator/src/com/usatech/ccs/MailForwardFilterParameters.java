package com.usatech.ccs;

/**
 * A version of MailForward.pl for the AppLayer
 * This class contains parameters governing what
 * emails are forwarded.
 * 
 * last_attempt_minutes: Emails that have not been resent in the last # minutes 
 * dupe_filter_time: Emails that have not been idling for more than some # minutes
 * max_send_attempts: Emails that have not had attempts to send more than # times. 
 */
public class MailForwardFilterParameters {
	public int getDupe_filter_time() {
		return dupe_filter_time;
	}

	public void setDupe_filter_time(int dupe_filter_time) {
		this.dupe_filter_time = dupe_filter_time;
	}

	public int getMax_send_attempts() {
		return max_send_attempts;
	}

	public void setMax_send_attempts(int max_send_attempts) {
		this.max_send_attempts = max_send_attempts;
	}

	public int getLast_attempt_time_minutes() {
		return last_attempt_time_minutes;
	}

	public void setLast_attempt_time_minutes(int last_attempt_time_minutes) {
		this.last_attempt_time_minutes = last_attempt_time_minutes;
	}

	private int dupe_filter_time;
	private int max_send_attempts;
	private int last_attempt_time_minutes;


	/**
	 * 
	 * @param dupe_filter_time do not send emails if they have become this many minutes old
	 * @param last_attempt_time_minutes do not resend emails that were attempted this recently
	 * @param max_send_attempts max number of retries to allow
	 */
	public MailForwardFilterParameters(int dupe_filter_time, int last_attempt_time_minutes, int max_send_attempts) {
		this.dupe_filter_time = dupe_filter_time;
		this.max_send_attempts = max_send_attempts;
		this.last_attempt_time_minutes = last_attempt_time_minutes;
	}

	/**
	 * Default 5 minute, 1 minute, 5 attempts.
	 */
	public MailForwardFilterParameters() {
		this(5,1,5);
	}
}