<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.ccs.ConsumerCommunicationTask"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.Format"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.report.custom.GenerateUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
String moreColor = RequestUtils.getAttribute(request, "moreColor", String.class, true);
%>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
<title>more. Bonus Cash</title>
<base href="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "baseUrl", String.class, true))%>" />
<style type="text/css">
body {
  width: 100% !important;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 16px;
  -webkit-text-size-adjust: none;
  -ms-text-size-adjust: none;
  margin: 0;
  padding: 0;
  text-align: center;
  }

img {
  height: auto;
  line-height: 100%;
  outline: none;
  text-decoration: none;
  display: block;
  }

h1, h2, h3, h4, h5, h6 {
  color: #333333;
  line-height: 100% !important;
  margin:0;
  }
  
p {
  margin: 1em 0;
  }

a {
  color:#0088cc;
  text-decoration: none;
  }
  
a:hover,
a:focus {
  color: #005580;
  text-decoration: underline !important;
}

table td {
  border-collapse: collapse;
  } 
<% ResponseUtils.writeCoteResource(pageContext, "extra_style");%>
</style>
<meta name="robots" content="noindex, nofollow"/>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td bgcolor="#ffffff" align="center" style="border-collapse:collapse;">        
      <!-- table containing the nav bar -->
      <table cellpadding="0" cellspacing="0" border="0" width="600" height="40" style="background-color:#578fcf;">
        <tr>
          <td height="40" width="421" style="text-align: left; font-weight: bold; font-style: italic; font-family: Georgia, sans-serif; color: #fff; font-size: 30px;"><a href="index.html"><img src="email_logo._" alt="more." style="color: #fff;" /></a></td>
          <td height="40" width="81" valign="middle" style="text-align: center; border-collapse:collapse;" ><a href="signin.html" style="color:#fff; text-decoration:none; font-size: 16px;">Sign In</a></td>
          <td height="40" width="86" valign="middle" style="text-align: center; border-collapse:collapse;"><a href="support.html" style="color:#ffffff; text-decoration:none; font-size: 16px;">Support</a></td>
          <td height="40" width="12"></td>
        </tr>
      </table>     
      <!-- table containing the body of the email -->
      <table cellpadding="0" cellspacing="0" border="0" width="600">
        <tr>
          <td style="border-collapse:collapse;" >          
           <!-- table for main message  -->
            <table cellpadding="20" cellspacing="0" border="0" width="600" bgcolor="#ffffff">
              <tr>
                <td valign="middle" style="border-collapse:collapse;" >
                <h2 style="color: #333333; font-size: 24px; line-height:100% !important;" >Hey <%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "firstName", String.class, true))%>!</h2>
                <br /><%
Locale locale = RequestUtils.getAttribute(request, "locale", Locale.class, false);
Format currencyFormat = ConvertUtils.getFormat("CURRENCY", RequestUtils.getAttribute(request, "currencyCd", String.class, true), locale);
Results results = DataLayerMgr.executeQuery("GET_CAMPAIGN_INFO", RequestUtils.getInputForm(request));%>
                <p style="font-size: 18px; color: #333333; margin-top: 0; margin-bottom: 0; margin-right: 0; margin-left: 0;" >
                You earned <span style="font-weight: bold; color: <%=moreColor%>;"><%=StringUtils.prepareHTML(currencyFormat.format(RequestUtils.getAttribute(request, "cashBackAmount", Number.class, true))) %></span> in Bonus Cash on your <span style="font-weight: bold; font-style: italic; font-family: Georgia, sans-serif; color: <%=moreColor%>; font-size: 20px;">more</span> account because of <%=StringUtils.prepareHTML(currencyFormat.format(RequestUtils.getAttribute(request, "purchaseAmount", Number.class, true))) %> in purchases<%
if(results.next()) {
    String promo = results.getValue("campaignDescription", String.class);
    if(StringUtils.isBlank(promo))
        promo = results.getValue("campaignName", String.class);
    if(!StringUtils.isBlank(promo)) {
%> during the <%=promo %> promotion<%
    }
} %>!</p>
                </td>
             </tr>
           </table> <!-- end table for main message  -->           
          </td>
        </tr>
      </table>
		<table width="600"  cellpadding="0" cellspacing="0" border="0">
		<tr>
		<td>
		<hr />
		</td>
		</tr>
		</table>	
        <table width="600" height="80"  cellpadding="0" cellspacing="0" border="0">
        <tr><td height="40" style="text-align: center; font-size: 14px;" colspan="3"><a href="settings.html?username=<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "userName", String.class, true)) %>">Click here</a> to change your communication preferences or to opt out of these types of emails</td></tr>
        <tr>
        <td height="40" style="text-align: center; font-size: 14px;" colspan="3">
            <div style="margin-bottom: .3em;">
              <a id="termslink" href="terms_conditions.html">Terms and Conditions</a>&nbsp;|&nbsp;<a id="privacylink" href="privacy_policy.html">Privacy Policy</a>
            </div>
            <div style="margin-bottom: .3em;">
              <div style="display: inline-block;">
                <span style="font-weight: 800; font-style: italic; font-family: 'Georgia', sans-serif; color: <%=moreColor%>; font-size: 105%">more.</span> v.<%=StringUtils.prepareHTML(ConsumerCommunicationTask.getPrepaidVersion())%> &ndash; A product of <a href="http://www.usatech.com">USA Technologies, Inc.</a>
                <span>&copy; Copyright 2003-<%=GenerateUtils.getCurrentYear()%></span>
              </div>
            </div>
        </td>
        </tr>
        <tr>
        <td width="265">&nbsp;</td>
        <td width="70" height="50"><img style="text-align: center;" src="/images/trustwave-seal70x35.png" onclick="javascript:window.open('https://sealserver.trustwave.com/cert.php?customerId=w6o8pBiUhhnnGnDB1cfZ1FWOQ66BnG&amp;size=105x54&amp;style=invert', 'c_TW', 'location=no, toolbar=no, resizable=yes, scrollbars=yes, directories=no, status=no, width=615, height=720'); return false;" oncontextmenu="javascript:alert('Copying Prohibited by Law - Trusted Commerce is a Service Mark of TrustWave Holdings, Inc.'); return false;" alt="This site protected by Trustwave's Trusted Commerce program" title="This site protected by Trustwave's Trusted Commerce program"/></td>
        <td width="265">&nbsp;</td>
        </tr>
        </table>
</td>  <!-- /end the td that centers things  -->
</tr>
</table>  <!-- /end the table that contains everything -->
</body>
</html>