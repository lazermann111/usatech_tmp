<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.report.custom.GenerateUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
	int campaignTypeId=RequestUtils.getAttribute(request, "campaignTypeId", int.class, true);
	String moreColor = RequestUtils.getAttribute(request, "moreColor", String.class, true);
	boolean isTest=  RequestUtils.getAttribute(request, "isTest", boolean.class, true);
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!-- NAME: 1:3:1 COLUMN -->
        <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>more. Promotions</title>
    <base href="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "baseUrl", String.class, true))%>" />
	<meta name="robots" content="noindex, nofollow"/>
    <style type="text/css">
        p{
            margin:10px 0;
            padding:0;
        }
        table{
            border-collapse:collapse;
        }
        h1,h2,h3,h4,h5,h6{
            display:block;
            margin:0;
            padding:0;
        }
        img,a img{
            border:0;
            height:auto;
            outline:none;
            text-decoration:none;
        }
        body,#bodyTable,#bodyCell{
            height:100%;
            margin:0;
            padding:0;
            width:100%;
        }
        #outlook a{
            padding:0;
        }
        img{
            -ms-interpolation-mode:bicubic;
        }
        table{
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }
        .ReadMsgBody{
            width:100%;
        }
        .ExternalClass{
            width:100%;
        }
        p,a,li,td,blockquote{
            mso-line-height-rule:exactly;
        }
        a[href^=tel],a[href^=sms]{
            color:inherit;
            cursor:default;
            text-decoration:none;
        }
        p,a,li,td,body,table,blockquote{
            -ms-text-size-adjust:100%;
            -webkit-text-size-adjust:100%;
        }
        .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
            line-height:100%;
        }
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }
        #bodyCell{
            padding:10px;
        }
        .templateContainer{
            max-width:600px !important;
        }
        a.mcnButton{
            display:block;
        }
        .mcnImage{
            vertical-align:bottom;
        }
        .mcnTextContent{
            word-break:break-word;
        }
        .mcnTextContent img{
            height:auto !important;
        }
        .mcnDividerBlock{
            table-layout:fixed !important;
        }
        body,#bodyTable{
            background-color:#FAFAFA;
        }
        #bodyCell{
            border-top:0;
        }
        .templateContainer{
            border:0;
        }
        h1{
            color:#202020;
            font-family:Helvetica;
            font-size:26px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }
        h2{
            color:#202020;
            font-family:Helvetica;
            font-size:22px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }
        h3{
            color:#202020;
            font-family:Helvetica;
            font-size:20px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }
        h4{
            color:#202020;
            font-family:Helvetica;
            font-size:18px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }
        #templatePreheader{
            background-color:#FAFAFA;
            border-top:0;
            border-bottom:0;
            padding-top:9px;
            padding-bottom:9px;
        }
        #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
            color:#656565;
            font-family:Helvetica;
            font-size:12px;
            line-height:150%;
            text-align:left;
        }
        #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
            color:#656565;
            font-weight:normal;
            text-decoration:underline;
        }
        #templateHeader{
            background-color:#FFFFFF;
            border-top:0;
            border-bottom:0;
            padding-top:9px;
            padding-bottom:0;
        }
        #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:16px;
            line-height:150%;
            text-align:left;
        }
        #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
            color:#2BAADF;
            font-weight:normal;
            text-decoration:underline;
        }
        #templateUpperBody{
            background-color:#FFFFFF;
            border-top:0;
            border-bottom:0;
            padding-top:0;
            padding-bottom:0;
        }
        #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:16px;
            line-height:150%;
            text-align:left;
        }
        #templateUpperBody .mcnTextContent a,#templateUpperBody .mcnTextContent p a{
            color:#2BAADF;
            font-weight:normal;
            text-decoration:underline;
        }
        #templateColumns{
            background-color:#FFFFFF;
            border-top:0;
            border-bottom:0;
            padding-top:0;
            padding-bottom:0;
        }
        #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:16px;
            line-height:150%;
            text-align:left;
        }
        #templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{
            color:#2BAADF;
            font-weight:normal;
            text-decoration:underline;
        }
        #templateLowerBody{
            background-color:#FFFFFF;
            border-top:0;
            border-bottom:2px solid #EAEAEA;
            padding-top:0;
            padding-bottom:9px;
        }
        #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:16px;
            line-height:150%;
            text-align:left;
        }
        #templateLowerBody .mcnTextContent a,#templateLowerBody .mcnTextContent p a{
            color:#2BAADF;
            font-weight:normal;
            text-decoration:underline;
        }
        #templateFooter{
            background-color:#FAFAFA;
            border-top:0;
            border-bottom:0;
            padding-top:9px;
            padding-bottom:9px;
        }
        #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
            color:#656565;
            font-family:Helvetica;
            font-size:12px;
            line-height:150%;
            text-align:center;
        }
        #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
            color:#656565;
            font-weight:normal;
            text-decoration:underline;
        }
    @media only screen and (min-width:768px){
        .templateContainer{
            width:600px !important;
        }

}   @media only screen and (max-width: 480px){
        body,table,td,p,a,li,blockquote{
            -webkit-text-size-adjust:none !important;
        }

}   @media only screen and (max-width: 480px){
        body{
            width:100% !important;
            min-width:100% !important;
        }

}   @media only screen and (max-width: 480px){
        #bodyCell{
            padding-top:10px !important;
        }

}   @media only screen and (max-width: 480px){
        .columnWrapper{
            max-width:100% !important;
            width:100% !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnImage{
            width:100% !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
            max-width:100% !important;
            width:100% !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnBoxedTextContentContainer{
            min-width:100% !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnImageGroupContent{
            padding:9px !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
            padding-top:9px !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
            padding-top:18px !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnImageCardBottomImageContent{
            padding-bottom:9px !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnImageGroupBlockInner{
            padding-top:0 !important;
            padding-bottom:0 !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnImageGroupBlockOuter{
            padding-top:9px !important;
            padding-bottom:9px !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnTextContent,.mcnBoxedTextContentColumn{
            padding-right:18px !important;
            padding-left:18px !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
            padding-right:18px !important;
            padding-bottom:0 !important;
            padding-left:18px !important;
        }

}   @media only screen and (max-width: 480px){
        .mcpreview-image-uploader{
            display:none !important;
            width:100% !important;
        }

}   @media only screen and (max-width: 480px){
        h1{
            font-size:22px !important;
            line-height:125% !important;
        }

}   @media only screen and (max-width: 480px){
        h2{
            font-size:20px !important;
            line-height:125% !important;
        }

}   @media only screen and (max-width: 480px){
        h3{
            font-size:18px !important;
            line-height:125% !important;
        }

}   @media only screen and (max-width: 480px){
        h4{
            font-size:16px !important;
            line-height:150% !important;
        }

}   @media only screen and (max-width: 480px){
        .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
            font-size:14px !important;
            line-height:150% !important;
        }

}   @media only screen and (max-width: 480px){
        #templatePreheader{
            display:block !important;
        }

}   @media only screen and (max-width: 480px){
        #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
            font-size:14px !important;
            line-height:150% !important;
        }

}   @media only screen and (max-width: 480px){
        #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
            font-size:16px !important;
            line-height:150% !important;
        }

}   @media only screen and (max-width: 480px){
        #templateUpperBody .mcnTextContent,#templateUpperBody .mcnTextContent p{
            font-size:16px !important;
            line-height:150% !important;
        }

}   @media only screen and (max-width: 480px){
        #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
            font-size:16px !important;
            line-height:150% !important;
        }

}   @media only screen and (max-width: 480px){
        #templateLowerBody .mcnTextContent,#templateLowerBody .mcnTextContent p{
            font-size:16px !important;
            line-height:150% !important;
        }

}   @media only screen and (max-width: 480px){
        #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
            font-size:14px !important;
            line-height:150% !important;
        }

}</style></head>
    <body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #FAFAFA;">
                <tr>
                <td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;">
                        <!-- BEGIN TEMPLATE // -->
                        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                        <tr>
                        <td align="center" valign="top" width="600" style="width:600px;">
                        <![endif]-->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
                         <tr>
                         <td valign="top" id="templatePreheader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;">
                            <table class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody class="mcnTextBlockOuter">
                                    <tr>
                                    <td class="mcnTextBlockInner" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
<!-- TOP NAV STUFF  -->
                                        <table class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="366" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tbody>
                                                <tr>
                                                <td class="mcnTextContent" style="padding-top: 9px;padding-left: 18px;padding-bottom: 9px;padding-right: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: left;" valign="top"><!-- possible teaser text -->
                                                </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="mcnTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="197" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tbody>
                                                <tr>
                                                 <td class="mcnTextContent" style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: left;" valign="top">
                                                 <div style="text-align: right;"><a href="signin.html" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #656565;font-weight: normal;text-decoration: underline;">SIGN IN</a> &nbsp;&nbsp;&nbsp;&nbsp;<a href="support.html" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #656565;font-weight: normal;text-decoration: underline;">SUPPORT</a></div>
                                                 </td>
                                                 </tr>
                                            </tbody>
                                        </table>
<!--  / END TOP NAV STUFF  -->
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        </tr>
                        <tr>
                        <td valign="top" id="templateHeader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 0;">
                            <table class="mcnImageBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody class="mcnImageBlockOuter">
                                    <tr>
                                    <td style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnImageBlockInner" valign="top">
                                        <table class="mcnImageContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
<!-- MORE LOGO  -->
                                                <tr>
                                                <td class="mcnImageContent" style="padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top"><a href="index.html" title="" class="" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img alt="more." src="email_logo._" style="max-width: 380px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="mcnImage" align="middle" width="380"></a>
                                                </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    </tr>
                                </tbody>
                             </table>
                             <table class="mcnDividerBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;" border="0" cellpadding="0" cellspacing="0" width="100%">
                             <tbody class="mcnDividerBlockOuter">
                                <tr>
                                <td class="mcnDividerBlockInner" style="min-width: 100%;padding: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
<!--  LIGHT GRAY LINE DIVIDER -->
                                    <table class="mcnDividerContent" style="min-width: 100%;border-top: 3px solid #EEEEEE;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                            <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"> <span></span>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    </tr>
                    <tr>
                    <td valign="top" id="templateUpperBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;"><table class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody class="mcnTextBlockOuter">
                            <tr>
                            <td class="mcnTextBlockInner" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                        <td class="mcnTextContent" style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;" valign="top">
<!--  PROMO INTRO TEXT  -->
                                        <h1 style="display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;"><span style="color:#2baadf">Hello <%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "firstName", String.class, true))%>!<%if(isTest){ %> This is a test!<%} %></span></h1>
                                        <span style="color:#444444"><span style="font-size:17px">A new promotion has been posted to your <span style="font-family:georgia,times,times new roman,serif; color: <%=moreColor%>;"><em><strong>more.</strong></em></span> account.</span></span>
                                        </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="mcnBoxedTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
    <!--[if gte mso 9]>
    <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
    <![endif]-->
                        <tbody class="mcnBoxedTextBlockOuter">
                            <tr>
                                <td class="mcnBoxedTextBlockInner" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                <!--[if gte mso 9]>
                <td align="center" valign="top" ">
                <![endif]-->
                                    <table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnBoxedTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
<!-- COUPON CONTENT -->
                                            <tr><!-- % might not work in some email clients  -->
                                            <td style="padding-top: 9px;padding-left: 18px;padding-left: 10%;padding-bottom: 9px;padding-right: 18px;padding-right: 10%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <table class="mcnTextContentContainer" style="min-width: 100% ! important;background-color: #FFFFFF;border: 3px dashed #2BAADF;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="18" cellspacing="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                        <td style="color: #444444;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;line-height: 150%;" class="mcnTextContent" valign="top"><div style="text-align: center;"><span style="color:#444444"><span style="font-size:17px"><strong><%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "promoTitle", String.class, true))%></strong><br/><%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "promoDetail", String.class, true))%></span></span></div>
                                                        </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
                <!--[if gte mso 9]>
                </td>
                <![endif]-->

                <!--[if gte mso 9]>
                </tr>
                </table>
                <![endif]-->
                                </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody class="mcnTextBlockOuter">
                                <tr>
                                <td class="mcnTextBlockInner" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                            <td class="mcnTextContent" style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #444444;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;" valign="top">
                                                <h3 class="null" style="display: block;margin: 0;padding: 0;color: #444444;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;"><span style="color:#444444"><span style="font-size:17px"><strong>Participating <% if(campaignTypeId==1||campaignTypeId==4||campaignTypeId==5){%>Locations<%}else{ %>Cards<%} %></strong></span></span></h3>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                </tr>
                            </tbody>
                        </table>
<!-- LOCATION TABLE  -->
<!-- start locations or cards-->
<% if(campaignTypeId==1||campaignTypeId==4||campaignTypeId==5){ 
Results results = RequestUtils.getAttribute(request, "locations", Results.class, true);
results.addGroup("deviceSerialCd");
int i = 0;
ArrayList<String> locationList=new ArrayList<String>();
while(results.next()) {
    if(results.isGroupBeginning("deviceSerialCd")) {
        i++;
		String location = results.getFormattedValue("locationDescription");
	    if(StringUtils.isBlank(location))
	        location = results.getFormattedValue("locationName");
	    String city = results.getFormattedValue("locationCity");
	    String state = results.getFormattedValue("locationState");
	    String postal = results.getFormattedValue("locationPostal");
	    String country = results.getFormattedValue("locationCountry");
	    StringBuilder sb = new StringBuilder();
	    if(city != null && !(city=city.trim()).isEmpty()) {
	        sb.append(city);
	        if(state != null && !(state=state.trim()).isEmpty())
	            sb.append(", ").append(state);
	    } else if(state != null && !(state=state.trim()).isEmpty())
	        sb.append(state);
	    if(postal != null && !(postal=postal.trim()).isEmpty()) {
	        if(sb.length() > 0)
	            sb.append(' ');
	        sb.append(postal);
	    }
	    if(country != null && !(country=country.trim()).isEmpty()) {
	        if(sb.length() > 0)
	            sb.append(' ');
	        sb.append(country);
	    }
	    String[] locationDetails = {
	   		results.getFormattedValue("locationAddress1"),
	   		results.getFormattedValue("locationAddress2"),
	        sb.toString()         
	    };
	    StringBuilder sbL=new StringBuilder("<table class=\"mcnTextContentContainer\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"282\">");
	    sbL.append("<tbody><tr>");
	    sbL.append("<td class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #444444;font-family: Helvetica;font-size: 15px;line-height: 150%;text-align: left;\" valign=\"top\">");
	    sbL.append("<span style=\"color:#444444\">");
	    sbL.append("<span style=\"font-size:15px\"><strong>");
		sbL.append(StringUtils.prepareHTML(location));
		sbL.append("</strong>");
	    for(String s : locationDetails) {
            if(!StringUtils.isBlank(s)) {
            		sbL.append("<br/>");
            		sbL.append(StringUtils.prepareHTML(s));
            }
        }
	    sbL.append("</span></span>");
	    sbL.append("</td></tr></tbody></table>");
	    locationList.add(sbL.toString());
    }
}
if(locationList.size()>0){
	StringBuilder sbOneRow=new StringBuilder();
	String outTableStart="<table class=\"mcnTextBlock\" style=\"min-width:100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
	outTableStart+="<tbody class=\"mcnTextBlockOuter\"><tr><td class=\"mcnTextBlockInner\" valign=\"top\">";
	
	String outTableEnd="</td></tr></tbody></table>";
	
	for(int j=0;j<locationList.size();j++){
		if(j%2==0){
			sbOneRow.append(outTableStart);
			sbOneRow.append(locationList.get(j));
		}else{
			sbOneRow.append(locationList.get(j));
			sbOneRow.append(outTableEnd);
		}
	}
	%>
	<%=sbOneRow.toString()%>
	<%
}
}else{ %>	
	<!--   participating cards table-->
	<table class="mcnTextBlock" style="min-width:100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody class="mcnTextBlockOuter">
	<% Results results = RequestUtils.getAttribute(request, "cards", Results.class, true);
	while(results.next()) {%>
		<tr>
		<td>
		<table class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="282">
                                        <tbody>
                                            <tr>
                                            <td class="mcnTextContent" style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #444444;font-family: Helvetica;font-size: 15px;line-height: 150%;text-align: left;" valign="top">
		<%=StringUtils.prepareHTML(results.getFormattedValue("cardNumber"))%>
		</td></tr></tbody></table>
		</td>
		</tr>
	<% }%>
	</tbody>
	</table>
	<!--  participating cards table -->
<%} %>
<!--  end -->
                    </td>
                    </tr>

<!--  CUSTOM COMPANY SECTION -->
                    <tr>
                    <td valign="top" id="templateLowerBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 1px solid #EEEEEE;padding-top: 0;padding-bottom: 9px; background-color: #FFFFFF;">

                        <table class="mcnDividerBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;" border="0" cellpadding="0" cellspacing="0" width="100%;">
                            <tbody class="mcnDividerBlockOuter">
                                <tr>
                                <td class="mcnDividerBlockInner" style="min-width: 100%;padding: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
<!--  LIGHT GRAY LINE DIVIDER -->
                                    <table class="mcnDividerContent" style="min-width: 100%;border-top: 3px solid #EEEEEE;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                            <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"> <span></span>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
                                </td>
                                </tr>
                            </tbody>
                        </table>
<!--  COMPANY NEWS BLOCK CONTENT -->
<%
String promoText = RequestUtils.getAttribute(request, "promoText", String.class, false);
if(!StringUtils.isBlank(promoText)) {%>  
                        <table class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody class="mcnTextBlockOuter">
                                <tr>
                                <td class="mcnTextBlockInner" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                            <td class="mcnTextContent" style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;" valign="top">
                                                <h3 class="null" style="display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;"><span style="color:#444444"><span style="font-size:17px">News</span></span></h3>
                                                <span style="color:#444444"><%=StringUtils.prepareHTML(promoText) %></span>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                </tr>
                            </tbody>
                        </table>
<%} %>
<%if(RequestUtils.getAttribute(request, "customerId", int.class, true)>0){ 
	String customerName=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "requestCustomerName", String.class, false),"");
	String customerAddress1=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "customerAddress1", String.class, false),"");
	String customerAddress2=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "customerAddress2", String.class, false),"");
	String city=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "customerCity", String.class, false),"");
	String state=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "customerState", String.class, false),"");
	String zip=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "customerZip", String.class, false),"");
	int companyLogoCount=ConvertUtils.getInt(RequestUtils.getAttribute(request, "companyLogoCount", int.class, true));
%>
					<table class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody class="mcnTextBlockOuter">
                                <tr>
                                <td class="mcnTextBlockInner" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                    <table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                            <td class="mcnTextContent" style="padding: 9px 18px;line-height: 100% !important;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #444444;font-family: Helvetica;font-size: 16px;text-align: left;" valign="top">
                                            <%if(companyLogoCount>0){ %>
                                                <img onerror='this.style.display = "none"' src="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "companyLogoUrl", String.class, true)) %>" alt="company logo" style="height: 75px;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="75" width="100" align="left" /><img src="15px-spacer._" style="height: 75px; width: 15px;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="75" width="15" align="left" />
                                            <%} %>
                                                <span style="color:#444444">
                                                <span style="font-size:12px"><strong><%=StringUtils.prepareHTML(customerName) %></strong><br>
                                                <%if(!StringUtils.isBlank(customerAddress1)){ %>
                                                <%=StringUtils.prepareHTML(customerAddress1) %><br/>
                                                <%} %>
                                                <%if(!StringUtils.isBlank(customerAddress2)){ %>
                                                <%=StringUtils.prepareHTML(customerAddress2) %><br/>
                                                <%} %>
                                                <%if(!StringUtils.isBlank(city)){ %>
                                                <%=StringUtils.prepareHTML(city) %>
                                                <%} %>
                                                <%if(!StringUtils.isBlank(state)){ %>
                                                <%=StringUtils.prepareHTML(state) %>
                                                <%} %><br>
                                                <%if(!StringUtils.isBlank(zip)){ %>
                                                <%=StringUtils.prepareHTML(zip) %>
                                                <%} %>
                                                </span>
                                                </span>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </td>
                                </tr>
                            </tbody>
                        </table>
<%} %>
                    </td>
                    </tr>
<!-- /END COMPANY CUSTOM SECTION  -->
					</table>
                        <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
                <tr>
                    <td valign="top" id="templateFooter" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;">
                        <table class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody class="mcnTextBlockOuter">
                                <tr>
                                <td class="mcnTextBlockInner" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                    <table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                            <td class="mcnTextContent" style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;" valign="top"> Copyright &copy; <%=GenerateUtils.getCurrentYear()%> USA Technologies, Inc., All rights reserved.
                                             <br/>
                                             <a href="settings.html?username=<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "userName", String.class, true)) %>">Click here</a> to change your communication preferences or to opt out of these types of emails.
                                             <br/>
                                             <a id="termslink" href="terms_conditions.html">Terms and Conditions</a>&nbsp;|&nbsp;<a id="privacylink" href="privacy_policy.html">Privacy Policy</a>
                                             <br/>
                                             <span style="font-weight: bold; font-style: italic; font-family: 'Georgia', sans-serif; color: <%=moreColor%>; font-size: 105%">more.</span> v.<%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "version", String.class, true))%> &ndash; A product of <a href="http://www.usatech.com">USA Technologies, Inc.</a>
                                             <br >
                                             <img style="text-align: center;" src="/images/trustwave-seal70x35.png" onclick="javascript:window.open('https://sealserver.trustwave.com/cert.php?customerId=w6o8pBiUhhnnGnDB1cfZ1FWOQ66BnG&amp;size=105x54&amp;style=invert', 'c_TW', 'location=no, toolbar=no, resizable=yes, scrollbars=yes, directories=no, status=no, width=615, height=720'); return false;" oncontextmenu="javascript:alert('Copying Prohibited by Law - Trusted Commerce is a Service Mark of TrustWave Holdings, Inc.'); return false;" alt="This site protected by Trustwave's Trusted Commerce program" title="This site protected by Trustwave's Trusted Commerce program"/>
                                             <br/>
                                             <br/>
                                            <%
String toMarketing = RequestUtils.getAttribute(request, "toMarketing", String.class, false);
if(!StringUtils.isBlank(toMarketing)&&toMarketing.equalsIgnoreCase("Y")) {%>  
For Marketing: Please <a href="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "usalivePromoApproveUrl", String.class, true)) %>">click here</a> to approve the custom text included in the campaign email. For USALive user:<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "requestUserName", String.class, true)) %> email:<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "requestUserEmail", String.class, false)) %> customer:<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "requestCustomerName", String.class, false)) %>

<%} %> 
                                            </td>
                                            </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table></td>
                </tr>
            </table>
        </center>
    </body>
</html>