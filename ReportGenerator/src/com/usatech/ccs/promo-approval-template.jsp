<%@page import="java.util.ArrayList"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
	String firstName = RequestUtils.getAttribute(request, "firstName", String.class, false);
	String promoText = RequestUtils.getAttribute(request, "promoText", String.class, true);
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!-- NAME: 1:3:1 COLUMN -->
        <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>more. Promotions Email Approval Notice</title>
	<meta name="robots" content="noindex, nofollow"/>
	<style>
	.mcnTextContent{
            word-break:break-word;
        }
	</style>
    </head>
    <body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;">
        <center>
			<table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
			<tr><td><%if(!StringUtils.isBlank(firstName)){ %>
            	Hello <%=StringUtils.prepareHTML(firstName)%>!
            <%}else{ %>
            	Hello!
            <%} %><br/><br/></td></tr>
			<tr><td>The custom text shown below has been approved by the USA Technologies Marketing Team.</td></tr>
			</tbody>
			</table>

			<table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                            <td class="mcnTextContent" style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;" valign="top">
                                                <h3 class="null" style="display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;"><span style="color:#444444"><span style="font-size:17px"><%=StringUtils.prepareHTML(promoText)%></span></span></h3>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
			<table style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                            <td >
											If you have already scheduled this email to be sent, USALive will automatically send the email to your Customers.  If you have not scheduled this email to be sent, please do so now by clicking on this <a href="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "usaliveManagePromoUrl", String.class, true)) %>">link</a> 
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
        </center>
    </body>
</html>