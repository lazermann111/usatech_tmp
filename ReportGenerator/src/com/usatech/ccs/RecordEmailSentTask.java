package com.usatech.ccs;

import java.sql.SQLException;
import java.util.Map;

import simple.app.DatabasePrerequisite;
import simple.app.ServiceException;
import simple.db.DataLayerMgr;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

public class RecordEmailSentTask implements MessageChainTask {
	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		try{
			Map<String, Object> params = taskInfo.getStep().getAttributes();
			long currentTime=System.currentTimeMillis();
			params.put("lastAttemptTime", currentTime);
			params.put("attemptNum", 1);
			params.put("successTime", currentTime);
			DataLayerMgr.executeCall("RECORD_EMAIL_SENT", params, true);
		} catch(SQLException e) {
            throw DatabasePrerequisite.createServiceException(e);
		} catch(Exception e){
			throw new ServiceException("Error while recording email sent", e);
		}
		return 0;
	}
}
