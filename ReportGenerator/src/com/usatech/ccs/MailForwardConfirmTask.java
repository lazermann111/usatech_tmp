package com.usatech.ccs;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

import static com.usatech.ccs.MailConfirmAttributeEnum.*;

/**
 * A version of MailForward.pl for the AppLayer
 * 
 * This message acts after SendEmail has completed it's work,
 * in order to flag the message as having been successfully
 * sent or not.
 * 
 * @author phorsfield
 *
 */
public class MailForwardConfirmTask implements MessageChainTask {

	private static final Log log = Log.getLog();

	// === Data Layer constants - see applayer-data-layer.xml ===
	private static final String MAIL_FORWARD_MARK_SENT = "MAIL_FORWARD_MARK_SENT";
	private static final String MAIL_FORWARD_MARK_NOT_SENT = "MAIL_FORWARD_MARK_NOT_SENT";

	// === No configurable properties

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		long id = ConvertUtils.getLongSafely(attributes.get(OB_EMAIL_QUEUE_ID.getValue()),Long.MAX_VALUE);
		boolean success = ConvertUtils.getBooleanSafely(attributes.get(EMAIL_SENT.getValue()), false);
		if(id == Long.MAX_VALUE) return 1;
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(OB_EMAIL_QUEUE_ID.getValue(), id);

		try 
		{
			if(success)
			{
				DataLayerMgr.executeUpdate(MAIL_FORWARD_MARK_SENT, params, true);
			}
			else
			{
				DataLayerMgr.executeUpdate(MAIL_FORWARD_MARK_NOT_SENT, params, true);			
			}
			log.info("Recorded " + (success?"confirmation":"failure") + " of outbound email id " + id);
		}
		catch (DataLayerException e) {
			String msg = "Could not record " + (success?"confirmation":"failure") + " of outbound email id " + id;
			log.warn(msg,e);
			throw new ServiceException(msg, e);
		} catch(SQLException e) {
			String msg = "Could not record " + (success?"confirmation":"failure") + " of outbound email id " + id;
			log.warn(msg,e);
			throw new ServiceException(msg, e);
		}
		
		return 0;
	}
}
