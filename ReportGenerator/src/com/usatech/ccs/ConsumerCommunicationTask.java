package com.usatech.ccs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;

import simple.app.DatabasePrerequisite;
import simple.app.RetrySpecifiedServiceException;
import simple.app.ServiceException;
import simple.app.WorkRetryType;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.ConfigSource;
import simple.io.CoteCache;
import simple.io.IOUtils;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.Interceptor;
import simple.servlet.JspRequestDispatcherFactory;
import simple.text.StringUtils;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.CompositeMap;

import com.usatech.app.Attribute;
import com.usatech.app.AttributeConversionException;
import com.usatech.app.GenericAttribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.ConsumerCommAttrEnum;
import com.usatech.layers.common.constants.ConsumerCommType;

public class ConsumerCommunicationTask implements MessageChainTask {
	private static final Log log = Log.getLog();
	protected final JspRequestDispatcherFactory jrdf = new JspRequestDispatcherFactory();
	protected final String jspPageDirectory = getClass().getPackage().getName().replace('.', '/') + '/';
	protected static String prepaidVersion;
	protected final Map<String, Integer> subdomainPortMap = new HashMap<String, Integer>();
	protected final CoteCache coteCache = new CoteCache();
	protected static final Attribute PREFERRED_SUBDOMAINS = new GenericAttribute("preferredSubdomains");

	@Override
	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		MessageChainStep step = taskInfo.getStep();
		ConsumerCommType consumerCommType;
		long consumerAcctId;
		try {
			consumerCommType = step.getAttribute(ConsumerCommAttrEnum.ATTR_COMMUNICATION_TYPE, ConsumerCommType.class, true);
			consumerAcctId = step.getAttribute(ConsumerCommAttrEnum.ATTR_CONSUMER_ACCT_ID, Long.class, true);
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		if(!subdomainPortMap.isEmpty())
			step.setAttribute(PREFERRED_SUBDOMAINS, subdomainPortMap.keySet());
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			DataLayerMgr.selectInto("GET_CONSUMER_COMM_DETAILS", step.getAttributes(), params, true);
		} catch(NotEnoughRowsException e) {
			log.info("Consumer Acct " + consumerAcctId + " is not configured to received email notification for " + consumerCommType);
			return 0;
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		} catch(BeanException e) {
			throw new ServiceException(e);
		}
		
		String body;
		try {
			String subdomainUrl = ConvertUtils.convertRequired(String.class, params.get("subdomainUrl"));
			String subdirectory = ConvertUtils.convert(String.class, params.get("subdirectory"));
			long consumerId  = ConvertUtils.convertRequired(Long.class, params.get("consumerId"));
			params.put(CoteCache.class.getName() + ".Retriever", coteCache.createRetriever(subdomainUrl, subdirectory, consumerId));
			Locale locale = ConvertUtils.convertRequired(Locale.class, params.get("locale"));
			StringBuilder baseUrl = new StringBuilder();
			Integer port = subdomainPortMap.get(subdomainUrl);
			if(port != null && port > 0 && (port % 1000) != 443)
				baseUrl.append("http://");
			else
				baseUrl.append("https://");
			baseUrl.append(subdomainUrl);
			if(port != null && port > 0)
				baseUrl.append(':').append(port);
			if(!StringUtils.isBlank(subdirectory))
				baseUrl.append(subdirectory);
			baseUrl.append('/');
			params.put("baseUrl", baseUrl.toString());

			String translatorUrl = subdomainUrl;
			if(!StringUtils.isBlank(subdirectory))
				translatorUrl = translatorUrl + subdirectory;

			String domain = subdomainUrl;
			String sub = "communication";
			int pos = subdomainUrl.lastIndexOf('.');
			if(pos > 0) {
				pos = subdomainUrl.lastIndexOf('.', pos - 1);
				if(pos > 0) {
					domain = subdomainUrl.substring(pos + 1);
					sub = subdomainUrl.substring(0, pos);
				}
			}
			String customerName = ConvertUtils.convert(String.class, params.get("customerName"));
			Translator translator = TranslatorFactory.getDefaultFactory().getTranslator(locale, translatorUrl);
			String fromEmail = translator.translate("consumer-comm-email-from-email", "{0,NULL,{0},{1}}@{2}", StringUtils.isBlank(customerName) ? null : customerName.toLowerCase().replaceAll("\\W", "_"), sub, domain);
			String fromName = translator.translate("consumer-comm-email-from-name", "{0}", StringUtils.isBlank(customerName) ? "USA Technologies" : customerName);
			String subject = translator.translate("consumer-comm-subject-" + consumerCommType.toString().toLowerCase().replace('_', '-'), getCommTypeSubject(consumerCommType));
			String moreColor = translator.translate("prepaid-more-color", "#e74b34");

			params.put("fromEmail", fromEmail);
			params.put("fromName", fromName);
			params.put("subject", subject);
			if(consumerCommType==ConsumerCommType.PREPAID_PURCHASE){
				long reportTranId;
				try{
					reportTranId=step.getAttribute(ConsumerCommAttrEnum.ATTR_REPORT_TRAN_ID, Long.class, true);
				} catch(AttributeConversionException e) {
					throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
				}
				Results results = DataLayerMgr.executeQuery("GET_PREPAID_PURCHASE", new Object[] { reportTranId}, false);
				if(results.next()) {
					String freeInd=ConvertUtils.getStringSafely(results.get("freeInd"),"N");
					StringBuilder sb=new StringBuilder("");
					if(freeInd.equals("Y")){
						sb.append(" (Free)");
					}
					body=translator.translate("consumer-comm-purchase-sms-body","Time: {0, DATE,MM/dd/yy @ h:mma z}\nOperator: {1}\nLocation: {2}\nDevice: {3}\nCard: {4}\nTransaction Id: {5}\nItems: {6}\nAmount: {7,CURRENCY}"+sb.toString()+"\n"
							+ (StringUtils.isBlank(results.getFormattedValue("consumerAcctBalance")) ? "" : "Balance: {9,CURRENCY}\n")
							+ "Profile: {8}\n", 
							results.get("date"),
							results.get("operatorName"),
							results.get("location"),
							results.get("deviceSerialCd"),
							results.get("cardNum"),
							results.get("tranDeviceTranCd"),
							results.get("vendColumn"),
							results.get("totalAmount"),
							baseUrl.toString()+"settings.html",
							results.get("consumerAcctBalance"));
				}else{
					log.warn("Failed to find prepaid purchase reportTranId="+reportTranId);
					return 0;
				}
			}else if (consumerCommType==ConsumerCommType.REPLENISH_DECLINED){
				body=translator.translate("consumer-comm-replenishdeclined-sms-body", "Your credit card {0.replenishCardMasked} has been declined while replenishing your MORE card {2} for {0.replenishAmount,CURRENCY}. See {1}cards.html to change. See {1}settings.html to change settings.", step.getAttributes(), params.get("baseUrl"),params.get("consumerAcctCd"));
			}else if (consumerCommType==ConsumerCommType.REPLENISH_SUCCESS){
				char cashInd= step.getAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_CASH_IND, char.class, true);
				String cashOrCredit=cashInd=='Y'?"cash":"credit card {0.replenishCardMasked}";
				body=translator.translate("consumer-comm-replenishsuccess-sms-body", "Your MORE card {2} is replenished {0.replenishAmount,CURRENCY} using "+cashOrCredit+". See {1}settings.html to change settings.", step.getAttributes(), params.get("baseUrl"),params.get("consumerAcctCd"));
			}else if (consumerCommType==ConsumerCommType.BELOW_BALANCE){
				body=translator.translate("consumer-comm-belowbalance-sms-body", "Your MORE card {2} balance {0.consumerAcctBalance,CURRENCY} is below {0.belowBalanceThreshold,CURRENCY}. See {1}settings.html to change settings.", step.getAttributes(), params.get("baseUrl"),params.get("consumerAcctCd"));
			}else if (consumerCommType==ConsumerCommType.CARD_EXPIRATION){
				int replenishPriority= step.getAttribute(ConsumerCommAttrEnum.ATTR_REPLENISH_PRIORITY, Integer.class, true);
				String cardDesc=replenishPriority==1?"Credit card {0.replenishCardMasked} used to replenish MORE card {1}":"Credit card {0.replenishCardMasked} on file for MORE card replenishment";
				body=translator.translate("consumer-comm-card-exp-sms-body", cardDesc+" is about to expire on {0.expirationDate, DATE,MM/yyyy}. Please update the info. See {2}settings.html to change settings.", step.getAttributes(), params.get("consumerAcctCd"), params.get("baseUrl"));
			}else if (consumerCommType==ConsumerCommType.ADD_PASS){
				params.put("fromName", "Get ready to get MORE!");
				params.put("subject", "Download your Mobile Pass");
				String overwriteEmail=step.getAttribute(ConsumerCommAttrEnum.ATTR_PASS_COMMUNICATION_EMAIL, String.class, false);
				if(overwriteEmail!=null){
					params.put("toEmail", overwriteEmail);
				}
				body=translator.translate("consumer-add-pass-sms-body", "Download {0.passUrl}", step.getAttributes());
			}else{
				int preferredCommType=ConvertUtils.getInt(params.get("preferredCommType"));
				if(preferredCommType==1){
					String jsp = jspPageDirectory + "consumer-comm-" + consumerCommType.toString().toLowerCase().replace('_', '-') + ".jsp";
					params.put("moreColor", moreColor);
					CompositeMap<String, Object> merged = new CompositeMap<String, Object>();
					merged.merge(params);
					merged.merge(step.getAttributes());
					body = Interceptor.intercept(jrdf, jrdf, jsp, locale, null, merged);
				}else{//SMS
					switch(consumerCommType) {
						case CASH_BACK_ON_PURCHASE:
						body=translator.translate("consumer-comm-cashback-sms-body", "You earned a {0.cashBackAmount,CURRENCY} bonus cash on a purchase of {0.purchaseAmount,CURRENCY}. See {1}settings.html to change settings.", step.getAttributes(), params.get("baseUrl"));
							break;
						case REPLENISH_BONUS:
						body=translator.translate("consumer-comm-replenishbonus-sms-body", "You earned a {0.cashBackAmount,CURRENCY} replenish bonus because of adding {0.purchaseAmount,CURRENCY} to your account. See {1}settings.html to change settings.", step.getAttributes(), params.get("baseUrl"));
							break;
						case REPLENISH_DISABLED:
						body=translator.translate("consumer-comm-replenishdisabled-sms-body", "Your credit card {0.replenishCardMasked} has been denied {0.deniedCount} times while replenishing your MORE card. See {1}cards.html to change. See {1}settings.html to change settings.", step.getAttributes(), params.get("baseUrl"));
							break;
						default:
							body="";
					}
				}
			}
		} catch(AttributeConversionException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(SQLException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(DataLayerException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(ConvertException e) {
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		} catch(ServletException e) {
			if(e.getRootCause() instanceof ServiceException)
				throw (ServiceException) e.getRootCause();
			throw new RetrySpecifiedServiceException(e, WorkRetryType.NO_RETRY);
		}
		params.put("body", body);
		try {
			DataLayerMgr.executeCall("SEND_EMAIL", params, true);
		} catch(SQLException e) {
			throw DatabasePrerequisite.createServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
		return 0;
	}

	protected String getCommTypeSubject(ConsumerCommType consumerCommType) {
		switch(consumerCommType) {
			case CASH_BACK_ON_PURCHASE:
				return "You earned Bonus Cash!";
			case REPLENISH_BONUS:
				return "You earned a Replenish Bonus!";
			case REPLENISH_DISABLED:
				return "Please correct replenish settings";
			case PREPAID_PURCHASE:
				return "Your purchase receipt";
			case REPLENISH_DECLINED:
				return "Your replenishment is declined";
			case REPLENISH_SUCCESS:
				return "Your replenishment is successful";
			case BELOW_BALANCE:
				return "Your card balance is low";
			case CARD_EXPIRATION:
				return "Your credit card expiration notification";
			default:
				return "Notice";
		}
	}
	/**
	 * In deployed environment, we create in rptgen/classes a symbolic link prepaid-version.txt to ../../prepaid/classes/version.txt. See build.xml
	 * 
	 * @return
	 */
	public static String getPrepaidVersion() {
		if(prepaidVersion == null) {
			try {
				prepaidVersion = SystemUtils.nvl(IOUtils.readFully(ConfigSource.createConfigSource("prepaid-version.txt").getInputStream()), "UNKNOWN").trim();
			} catch(FileNotFoundException e) {
				Log.getLog().info("Could not find prepaid-version.txt on the classpath");
				prepaidVersion = "UNKNOWN";
			} catch(IOException e) {
				prepaidVersion = "UNKNOWN";
			}
		}
		return prepaidVersion;
	}

	public Integer getSubdomainPort(String subdomain) {
		return subdomainPortMap.get(subdomain);
	}

	public void setSubdomainPort(String subdomain, Integer port) {
		subdomainPortMap.put(subdomain, port);
	}

	public CoteCache getCoteCache() {
		return coteCache;
	}
}

