package com.usatech.usalive.ws.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.UUID;

import org.junit.BeforeClass;
import org.junit.Test;

import com.usatech.usalive.ws.*;
import com.usatech.usalive.ws.client.UsaLiveStub;

public class UsaLiveWebServiceUnitTests
{
	private static final String LOC = "http://127.0.0.1:8880/soap/usalive?wsdl";
	private static final String DEV = "https://usalive-dev.usatech.com/soap/usalive?wsdl";
	private static final String INT = "https://usalive-int.usatech.com/soap/usalive?wsdl";
	private static final String ECC = "https://usalive-ecc.usatech.com/soap/usalive?wsdl";
	private static final String PRD = "https://www.usatech.com/soap/usalive?wsdl";

	// you must modify the following 3 values before running the tests
	private static String userName = "abc01";
	private static String password = "Password1";
	private static long commissionBankAccountId = 4133;
	
	private static UsaLiveStub clientStub;

	private static long bankAccountId = 0;

	@BeforeClass
	public static void setup() throws Exception
	{
		clientStub = new UsaLiveStub(userName, password, LOC);
	}

	@Test
	public void verifySetup()
	{
		clientStub.setup();
		assertNotNull(clientStub.getService());
		assertNotNull(clientStub.getWs());
	}

	@Test
	public void testPing()
	{
		assertEquals(clientStub.ping(), "pong");
	}

	@Test
	public void testLogin()
	{
		assertTrue(clientStub.checkCredentials());
	}

	@Test
	public void testCreateCustomer()
	{
		String uuid = UUID.randomUUID().toString().substring(0, 15);

		Location customerLocation = new Location();
		customerLocation.setAddress("123 Main St");
		customerLocation.setCountryCd(Location.COUNTRY_US);
		customerLocation.setPostalCd("12345");

		Customer customer = new Customer();
		customer.setLocation(customerLocation);
		customer.setDba("dba-" + uuid);
		customer.setName("customer-" + uuid);
		customer.setPaymentCycle("D");
		customer.setCustomerServiceEmail("cs@test.com");
		customer.setCustomerServicePhone("1231231234");

		User user = new User();
		user.setEmail("user@email.com");
		user.setFax("1231231234");
		user.setFirstName("First");
		user.setLastName("Last");
		user.setPassword("Password1");
		user.setPhone("1231231234");
		user.setUserName("user-" + uuid);

		BankContact bankContact = new BankContact();
		bankContact.setFax("1231231234");
		bankContact.setName("Bank Contact Name");
		bankContact.setPhone("1231231234");
		bankContact.setTitle("Bank Manager");

		Location bankLocation = new Location();
		bankLocation.setAddress("123 Bank St");
		bankLocation.setCountryCd(Location.COUNTRY_US);
		bankLocation.setPostalCd("94563");

		BankAccount bankAccount = new BankAccount();
		bankAccount.setLocation(bankLocation);
		bankAccount.setAbaNumber("123456789");
		bankAccount.setAccountNumber("12345");
		bankAccount.setAccountTitle("First Last");
		bankAccount.setAccountType(BankAccount.ACCOUNT_TYPE_CHECKING);
		bankAccount.setBankContact(bankContact);
		bankAccount.setBankName("Bank Name");
		bankAccount.setSwiftCode("asdf12345");
		bankAccount.setTaxId("123456789");
		
		Location bankBillingLocation = new Location();
		bankBillingLocation.setAddress("100 Deerfield Lane");
		bankBillingLocation.setCountryCd(Location.COUNTRY_US);
		bankBillingLocation.setPostalCd("19355");
		bankAccount.setBillingLocation(bankBillingLocation);

		ProcessFee cardPresentFee = new ProcessFee();
		cardPresentFee.setAmount(.20);
		cardPresentFee.setPercent(5);
		cardPresentFee.setMinimumAmount(.50);

		ProcessFee cardNotPresentFee = new ProcessFee();
		cardNotPresentFee.setAmount(.40);
		cardNotPresentFee.setPercent(6);
		cardNotPresentFee.setMinimumAmount(.75);

		ServiceFee serviceFee = new ServiceFee();
		serviceFee.setAmount(12.95);

		Fees fees = new Fees();
		fees.setCardPresentFee(cardPresentFee);
		fees.setCardNotPresentFee(cardNotPresentFee);
		fees.setServiceFee(serviceFee);

		CreateCustomerRequest request = new CreateCustomerRequest();
		request.setCustomer(customer);
		request.setPrimaryUser(user);
		request.setBankAccount(bankAccount);
		request.setFees(fees);

		CustomerIds response = clientStub.createCustomer(request);
		assertNotNull(response);
		assertTrue(Arrays.asList(response.getResponseMessages()).toString(), response.getResponseCode() == 1);
		assertTrue("customerId=" + response.getCustomerId(), response.getCustomerId() > 0);
		assertTrue("userId=" + response.getUserId(), response.getUserId() > 0);

		// a valid customer bankAccountId is needed by activateDevice
		bankAccountId = response.getBankAccountId();

		System.out.println(String.format("CreateCustomer passed with customerId=%s, userId=%s, bankAccountId=%s, licenseId=%s", response.getCustomerId(), response.getUserId(), response.getBankAccountId(), response.getFeesId()));
	}

	@Test
	public void testActivateDevice()
	{
		assertTrue("bankAccountId is not set,  createCustomer first", (bankAccountId > 0));
		assertTrue("commissionBankAccountId is not set,  commissionBankAccountId is required", (commissionBankAccountId > 0));

		String uuid = UUID.randomUUID().toString().substring(0, 15);

		String serialNumber = "K3TS" + System.currentTimeMillis();

		Location deviceLocation = new Location();
		deviceLocation.setAddress("123 Device St");
		deviceLocation.setCountryCd(Location.COUNTRY_US);
		deviceLocation.setPostalCd("12345");

		Device device = new Device();
		device.setAttendedIndicator(Device.INDICATOR_NO);
		device.setCustomerAssetNumber("asset-" + uuid.substring(0, 8));
		device.setDba("dba-" + uuid);
		device.setEntryTypeCardSwipeIndicator(Device.INDICATOR_YES);
		device.setEntryTypeKeypadIndicator(Device.INDICATOR_YES);
		device.setEntryTypeEmvIndicator(Device.INDICATOR_NO);
		device.setEntryTypeProximityIndicator(Device.INDICATOR_NO);
		device.setCustomerRegion("#3, Philadelphia Area");
		device.setLocation(deviceLocation);
		device.setLocationSpecify("Front Lobby");
		device.setLocationName("location-" + uuid);
		device.setLocationPhone("1231231234");
		device.setMobileDeviceIndicator(Device.INDICATOR_YES);
		device.setQuickConnectUserName("testaccount1");
		device.setSerialNumber(serialNumber);
		device.setSignatureCapturedIndicator(Device.INDICATOR_YES);

		device.setLocationType("Kiosk");
		device.setLocationTypeSpecify("Awesome-o Dispenser");
		device.setBusinessType("Kiosk");
		device.setProductType("Retail Products");
		device.setProductTypeSpecify("Widgets");
		
		device.setCustomerServiceEmail("asdf@asdf.com");
		device.setCustomerServicePhone("1231231234");

		ProcessFee cardPresentFee = new ProcessFee();
		cardPresentFee.setAmount(.30);
		cardPresentFee.setPercent(6);
		cardPresentFee.setMinimumAmount(.60);

		ProcessFee cardNotPresentFee = new ProcessFee();
		cardNotPresentFee.setAmount(.50);
		cardNotPresentFee.setPercent(7);
		cardNotPresentFee.setMinimumAmount(.85);

		ServiceFee serviceFee = new ServiceFee();
		serviceFee.setAmount(13.95);

		Fees fees = new Fees();
		fees.setCardPresentFee(cardPresentFee);
		fees.setCardNotPresentFee(cardNotPresentFee);
		fees.setServiceFee(serviceFee);

		ActivateDeviceRequest request = new ActivateDeviceRequest();
		request.setBankAccountId(bankAccountId);
		request.setCommissionBankAccountId(commissionBankAccountId);
		request.setDevice(device);
		request.setFees(fees);

		DeviceIds response = clientStub.activateDevice(request);
		assertNotNull(response);
		assertTrue(Arrays.asList(response.getResponseMessages()).toString(), response.getResponseCode() == 1);
		assertTrue("eportId=" + response.getEportId(), response.getEportId() > 0);
		assertTrue("terminalId=" + response.getTerminalId(), response.getTerminalId() > 0);

		System.out.println(String.format("ActivateDevice for serialNumber %s passed with eportId=%s, terminalId=%s", serialNumber, response.getEportId(), response.getTerminalId()));
	}
}
