package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class BankContact implements Serializable
{
	private static final long serialVersionUID = 1L;

	@NotNull(message = "bank contact name is required")
	@Size(min = 1, max = 50, message = "bank contact name must be between {min} and {max} in length")
	private String name;

	@NotNull(message = "bank contact title is required")
	@Size(min = 1, max = 50, message = "bank contact title must be between {min} and {max} in length")
	private String title;

	@NotNull(message = "bank contact phone is required")
	@Pattern(regexp = "[0-9]{10,20}", message = "bank contact phone must be 10-20 digits")
	private String phone;

	@Pattern(regexp = "(^$|[0-9]{10,20})", message = "bank contact fax must be 10-20 digits")
	private String fax;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getFax()
	{
		return fax;
	}

	public void setFax(String fax)
	{
		this.fax = fax;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof BankContact))
			return false;
		BankContact other = (BankContact)obj;
		if(fax == null) {
			if(other.fax != null)
				return false;
		} else if(!fax.equals(other.fax))
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		if(phone == null) {
			if(other.phone != null)
				return false;
		} else if(!phone.equals(other.phone))
			return false;
		if(title == null) {
			if(other.title != null)
				return false;
		} else if(!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("BankContact[name=%s,title=%s,phone=%s,fax=%s]", name, title, phone, fax);
	}

}
