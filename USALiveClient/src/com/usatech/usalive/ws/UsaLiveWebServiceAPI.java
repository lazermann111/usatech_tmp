package com.usatech.usalive.ws;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name="usalive", 
			serviceName="usalive",
			portName="usalive",
			targetNamespace="http://ws.usalive.usatech.com")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT,
			 use = SOAPBinding.Use.ENCODED,
			 parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface UsaLiveWebServiceAPI
{
	@WebMethod
	@WebResult(targetNamespace="http://ws.usalive.usatech.com")
	public String ping();

	@WebMethod
	@WebResult(targetNamespace="http://ws.usalive.usatech.com")
	public boolean checkCredentials(Credentials credentials);

	@WebMethod
	@WebResult(targetNamespace="http://ws.usalive.usatech.com")
	public CustomerIds createCustomer(Credentials credentials, CreateCustomerRequest request);

	@WebMethod
	@WebResult(targetNamespace="http://ws.usalive.usatech.com")
	public DeviceIds activateDevice(Credentials credentials, ActivateDeviceRequest request);
}
