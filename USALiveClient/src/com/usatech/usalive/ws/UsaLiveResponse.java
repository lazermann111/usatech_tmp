package com.usatech.usalive.ws;

import java.io.Serializable;
import java.util.Arrays;

public abstract class UsaLiveResponse implements Serializable
{
	private static final long serialVersionUID = 1L;

	public static final int ERROR = 0;
	public static final int SUCCESS = 1;

	protected int responseCode = 1;
	protected String[] responseMessages;

	public int getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(int responseCode)
	{
		this.responseCode = responseCode;
	}

	public String[] getResponseMessages()
	{
		return responseMessages;
	}

	public void setResponseMessages(String[] responseMessages)
	{
		this.responseMessages = responseMessages;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + responseCode;
		result = prime * result + Arrays.hashCode(responseMessages);
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof UsaLiveResponse))
			return false;
		UsaLiveResponse other = (UsaLiveResponse)obj;
		if(responseCode != other.responseCode)
			return false;
		if(!Arrays.equals(responseMessages, other.responseMessages))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("UsaLiveResponse[responseCode=%s,responseMessages=%s]", responseCode, Arrays.toString(responseMessages));
	}

}
