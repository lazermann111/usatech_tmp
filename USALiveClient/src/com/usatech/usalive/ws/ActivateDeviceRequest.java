package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ActivateDeviceRequest implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Valid
	@NotNull(message = "device is required")
	private Device device;

	@NotNull(message = "bank account is required")
	@Min(value = 1, message = "bank account must be greater than zero")
	private Long bankAccountId;

	@Valid
	private Fees fees;

	@NotNull(message = "commission bank account is required")
	@Min(value = 1, message = "commission bank account must be greater than zero")
	private Long commissionBankAccountId;

	public Device getDevice()
	{
		return device;
	}

	public void setDevice(Device device)
	{
		this.device = device;
	}

	public Long getBankAccountId()
	{
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId)
	{
		this.bankAccountId = bankAccountId;
	}

	public Fees getFees()
	{
		return fees;
	}

	public void setFees(Fees fees)
	{
		this.fees = fees;
	}

	public Long getCommissionBankAccountId()
	{
		return commissionBankAccountId;
	}

	public void setCommissionBankAccountId(Long commissionBankAccountId)
	{
		this.commissionBankAccountId = commissionBankAccountId;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bankAccountId == null) ? 0 : bankAccountId.hashCode());
		result = prime * result + ((commissionBankAccountId == null) ? 0 : commissionBankAccountId.hashCode());
		result = prime * result + ((device == null) ? 0 : device.hashCode());
		result = prime * result + ((fees == null) ? 0 : fees.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof ActivateDeviceRequest))
			return false;
		ActivateDeviceRequest other = (ActivateDeviceRequest)obj;
		if(bankAccountId == null) {
			if(other.bankAccountId != null)
				return false;
		} else if(!bankAccountId.equals(other.bankAccountId))
			return false;
		if(commissionBankAccountId == null) {
			if(other.commissionBankAccountId != null)
				return false;
		} else if(!commissionBankAccountId.equals(other.commissionBankAccountId))
			return false;
		if(device == null) {
			if(other.device != null)
				return false;
		} else if(!device.equals(other.device))
			return false;
		if(fees == null) {
			if(other.fees != null)
				return false;
		} else if(!fees.equals(other.fees))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("ActivateDeviceRequest[device=%s,bankAccountId=%s,fees=%s,commissionBankAccountId=%s]", device, bankAccountId, fees, commissionBankAccountId);
	}

}
