package com.usatech.usalive.ws.client;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;

import com.usatech.usalive.ws.ActivateDeviceRequest;
import com.usatech.usalive.ws.CreateCustomerRequest;
import com.usatech.usalive.ws.Credentials;
import com.usatech.usalive.ws.CustomerIds;
import com.usatech.usalive.ws.DeviceIds;
import com.usatech.usalive.ws.UsaLiveWebServiceAPI;

public class UsaLiveStub
{
	/*static {
		System.setProperty("javax.net.ssl.trustStore", "resources/ssl/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usalive");
	}*/

	private static final String DEFAULT_WSDL_URL = "https://usalive-ecc.usatech.com/soap/usalive?wsdl";
	private static final String DEFAULT_ENDPOINT = "usaliveHttpsSoap11Endpoint";

	private static final String NAMESPACE = "http://ws.usalive.usatech.com";
	private static final String SERVICE_NAME = "usalive";

	private String wsdlUrl;
	private String endpoint;
	private String userName;
	private String password;

	private Credentials credentials;

	private Service service;
	private UsaLiveWebServiceAPI ws;

	public UsaLiveStub(String userName, String password)
	{
		this(userName, password, DEFAULT_WSDL_URL);
	}

	public UsaLiveStub(String userName, String password, String wsdlUrl)
	{
		this(userName, password, wsdlUrl, DEFAULT_ENDPOINT);
	}

	public UsaLiveStub(String userName, String password, String wsdlUrl, String endpoint)
	{
		this.wsdlUrl = wsdlUrl;
		this.endpoint = endpoint;

		credentials = new Credentials(userName, password);
	}

	public static String getNamespace()
	{
		return NAMESPACE;
	}

	public static String getServiceName()
	{
		return SERVICE_NAME;
	}

	public String getWsdlUrl()
	{
		return wsdlUrl;
	}

	public String getEndpoint()
	{
		return endpoint;
	}

	public String getUserName()
	{
		return userName;
	}

	public String getPassword()
	{
		return password;
	}

	public Credentials getCredentials()
	{
		return credentials;
	}

	public Service getService()
	{
		return service;
	}

	public UsaLiveWebServiceAPI getWs()
	{
		return ws;
	}

	public void setup() throws WebServiceException
	{
		if(ws == null) {
			try {
				service = Service.create(new URL(wsdlUrl), new QName(NAMESPACE, SERVICE_NAME));
			}
			catch(MalformedURLException e) {
				throw new WebServiceException(e);
			}
			ws = service.getPort(new QName(NAMESPACE, endpoint), UsaLiveWebServiceAPI.class);
		}
	}

	public boolean isSetup()
	{
		return ws != null;
	}

	public String ping() throws WebServiceException
	{
		setup();
		return ws.ping();
	}
	
	public boolean checkCredentials() throws WebServiceException
	{
		setup();
		return ws.checkCredentials(credentials);
	}

	public CustomerIds createCustomer(CreateCustomerRequest request) throws WebServiceException
	{
		setup();
		return ws.createCustomer(credentials, request);
	}

	public DeviceIds activateDevice(ActivateDeviceRequest request) throws WebServiceException
	{
		setup();
		return ws.activateDevice(credentials, request);
	}
}
