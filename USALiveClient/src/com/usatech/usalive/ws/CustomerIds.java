package com.usatech.usalive.ws;


public class CustomerIds extends UsaLiveResponse
{
	private static final long serialVersionUID = 1L;

	private Long userId;
	private Integer customerId;
	private Long bankAccountId;
	private Long feesId;

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public Integer getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(Integer customerId)
	{
		this.customerId = customerId;
	}

	public Long getBankAccountId()
	{
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId)
	{
		this.bankAccountId = bankAccountId;
	}

	public Long getFeesId()
	{
		return feesId;
	}

	public void setFeesId(Long feesId)
	{
		this.feesId = feesId;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bankAccountId == null) ? 0 : bankAccountId.hashCode());
		result = prime * result + ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result + ((feesId == null) ? 0 : feesId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(!super.equals(obj))
			return false;
		if(!(obj instanceof CustomerIds))
			return false;
		CustomerIds other = (CustomerIds)obj;
		if(bankAccountId == null) {
			if(other.bankAccountId != null)
				return false;
		} else if(!bankAccountId.equals(other.bankAccountId))
			return false;
		if(customerId == null) {
			if(other.customerId != null)
				return false;
		} else if(!customerId.equals(other.customerId))
			return false;
		if(feesId == null) {
			if(other.feesId != null)
				return false;
		} else if(!feesId.equals(other.feesId))
			return false;
		if(userId == null) {
			if(other.userId != null)
				return false;
		} else if(!userId.equals(other.userId))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("CustomerIds[userId=%s,customerId=%s,bankAccountId=%s,feesId=%s]", userId, customerId, bankAccountId, feesId);
	}

}
