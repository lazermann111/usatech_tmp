package com.usatech.usalive.ws;


public class DeviceIds extends UsaLiveResponse
{
	private static final long serialVersionUID = 1L;

	private Long eportId;
	private Long terminalId;
	private Long deviceId;

	public Long getEportId()
	{
		return eportId;
	}

	public void setEportId(Long eportId)
	{
		this.eportId = eportId;
	}

	public Long getTerminalId()
	{
		return terminalId;
	}

	public void setTerminalId(Long terminalId)
	{
		this.terminalId = terminalId;
	}

	public Long getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(Long deviceId)
	{
		this.deviceId = deviceId;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		result = prime * result + ((eportId == null) ? 0 : eportId.hashCode());
		result = prime * result + ((terminalId == null) ? 0 : terminalId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(!super.equals(obj))
			return false;
		if(!(obj instanceof DeviceIds))
			return false;
		DeviceIds other = (DeviceIds)obj;
		if(deviceId == null) {
			if(other.deviceId != null)
				return false;
		} else if(!deviceId.equals(other.deviceId))
			return false;
		if(eportId == null) {
			if(other.eportId != null)
				return false;
		} else if(!eportId.equals(other.eportId))
			return false;
		if(terminalId == null) {
			if(other.terminalId != null)
				return false;
		} else if(!terminalId.equals(other.terminalId))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("DeviceIds[eportId=%s,terminalId=%s,deviceId=%s]", eportId, terminalId, deviceId);
	}
}
