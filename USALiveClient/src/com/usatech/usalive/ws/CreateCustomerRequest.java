package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class CreateCustomerRequest implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Valid
	@NotNull(message = "customer is required")
	private Customer customer;

	@Valid
	@NotNull(message = "primary user is required")
	private User primaryUser;

	@Valid
	@NotNull(message = "bank account is required")
	private BankAccount bankAccount;

	@Valid
	@NotNull(message = "fees is required")
	private Fees fees;

	public Customer getCustomer()
	{
		return customer;
	}

	public void setCustomer(Customer customer)
	{
		this.customer = customer;
	}

	public User getPrimaryUser()
	{
		return primaryUser;
	}

	public void setPrimaryUser(User primaryUser)
	{
		this.primaryUser = primaryUser;
	}

	public BankAccount getBankAccount()
	{
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount)
	{
		this.bankAccount = bankAccount;
	}

	public Fees getFees()
	{
		return fees;
	}

	public void setFees(Fees fees)
	{
		this.fees = fees;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bankAccount == null) ? 0 : bankAccount.hashCode());
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((fees == null) ? 0 : fees.hashCode());
		result = prime * result + ((primaryUser == null) ? 0 : primaryUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof CreateCustomerRequest))
			return false;
		CreateCustomerRequest other = (CreateCustomerRequest)obj;
		if(bankAccount == null) {
			if(other.bankAccount != null)
				return false;
		} else if(!bankAccount.equals(other.bankAccount))
			return false;
		if(customer == null) {
			if(other.customer != null)
				return false;
		} else if(!customer.equals(other.customer))
			return false;
		if(fees == null) {
			if(other.fees != null)
				return false;
		} else if(!fees.equals(other.fees))
			return false;
		if(primaryUser == null) {
			if(other.primaryUser != null)
				return false;
		} else if(!primaryUser.equals(other.primaryUser))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("CreateCustomerRequest[customer=%s,primaryUser=%s,bankAccount=%s,fees=%s]", customer, primaryUser, bankAccount, fees);
	}

}
