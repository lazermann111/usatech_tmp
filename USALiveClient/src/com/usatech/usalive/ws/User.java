package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class User implements Serializable
{
	private static final long serialVersionUID = 1L;

	@NotNull(message = "user name is required")
	@Size(min = 5, max = 20, message = "user name must be between {min} and {max} in length")
	private String userName;

	@NotNull(message = "user first name is required")
	@Size(min = 1, max = 50, message = "user first name must be between {min} and {max} in length")
	private String firstName;

	@NotNull(message = "user last name is required")
	@Size(min = 1, max = 50, message = "user last name must be between {min} and {max} in length")
	private String lastName;

	@Password
	@NotNull(message = "user password is required")
	private String password;

	@NotNull(message = "user email is required")
	@Pattern.List({@Pattern(regexp = "^(?:[a-z0-9!#$%&'*+\\/=?^_`{|}~-]\\.?){0,63}[a-z0-9!#$%&'*+\\/=?^_`{|}~-]@(?:(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\\.)*[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\])$", flags = Pattern.Flag.CASE_INSENSITIVE, message = "invalid user email format"),@Pattern(regexp = ".*(?!@usatech\\.com)", flags = Pattern.Flag.CASE_INSENSITIVE, message = "user email may not contain @usatech.com")})
	private String email;

	@NotNull(message = "user phone is required")
	@Pattern(regexp = "[0-9]{10,20}", message = "user phone must be 10-20 digits")
	private String phone;

	@Pattern(regexp = "(^$|[0-9]{10,20})", message = "user fax must be 10-20 digits")
	private String fax;

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getFax()
	{
		return fax;
	}

	public void setFax(String fax)
	{
		this.fax = fax;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((fax == null) ? 0 : fax.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof User))
			return false;
		User other = (User)obj;
		if(email == null) {
			if(other.email != null)
				return false;
		} else if(!email.equals(other.email))
			return false;
		if(fax == null) {
			if(other.fax != null)
				return false;
		} else if(!fax.equals(other.fax))
			return false;
		if(firstName == null) {
			if(other.firstName != null)
				return false;
		} else if(!firstName.equals(other.firstName))
			return false;
		if(lastName == null) {
			if(other.lastName != null)
				return false;
		} else if(!lastName.equals(other.lastName))
			return false;
		if(password == null) {
			if(other.password != null)
				return false;
		} else if(!password.equals(other.password))
			return false;
		if(phone == null) {
			if(other.phone != null)
				return false;
		} else if(!phone.equals(other.phone))
			return false;
		if(userName == null) {
			if(other.userName != null)
				return false;
		} else if(!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("User[userName=%s,firstName=%s,lastName=%s,password=%s,email=%s,phone=%s,fax=%s]", userName, firstName, lastName, password, email, phone, fax);
	}

}
