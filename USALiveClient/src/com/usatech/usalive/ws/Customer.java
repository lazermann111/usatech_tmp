package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class Customer implements Serializable
{
	private static final long serialVersionUID = 1L;

	@NotNull(message = "customer name is required")
	@Size(min = 1, max = 50, message = "customer name must be between {min} and {max} in length")
	private String name;

	@Valid
	@NotNull(message = "customer location is required")
	private Location location;

	@Size(min = 0, max = 21, message = "customer dba must be between {min} and {max} in length")
	private String dba;

	@Size(min = 0, max = 20, message = "customer service phone must be between {min} and {max} in length")
	private String customerServicePhone;

	@Size(min = 0, max = 70, message = "customer service email must be between {min} and {max} in length")
	private String customerServiceEmail;

	@NotNull(message = "customer payment cycle is required")
	@Pattern(regexp = "[DWM]{1}", message = "customer payment cycle must be (D)aily (W)eekly or (M)onthly")
	private String paymentCycle;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Location getLocation()
	{
		return location;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	public String getDba()
	{
		return dba;
	}

	public void setDba(String dba)
	{
		this.dba = dba;
	}

	public String getPaymentCycle()
	{
		return paymentCycle;
	}

	public void setPaymentCycle(String paymentCycle)
	{
		this.paymentCycle = paymentCycle;
	}

	public String getCustomerServicePhone() 
	{
		return customerServicePhone;
	}

	public void setCustomerServicePhone(String customerServicePhone) 
	{
		this.customerServicePhone = customerServicePhone;
	}

	public String getCustomerServiceEmail() 
	{
		return customerServiceEmail;
	}

	public void setCustomerServiceEmail(String customerServiceEmail) 
	{
		this.customerServiceEmail = customerServiceEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customerServiceEmail == null) ? 0 : customerServiceEmail.hashCode());
		result = prime * result + ((customerServicePhone == null) ? 0 : customerServicePhone.hashCode());
		result = prime * result + ((dba == null) ? 0 : dba.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((paymentCycle == null) ? 0 : paymentCycle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof Customer))
			return false;
		Customer other = (Customer)obj;
		if(customerServiceEmail == null) {
			if(other.customerServiceEmail != null)
				return false;
		} else if(!customerServiceEmail.equals(other.customerServiceEmail))
			return false;
		if(customerServicePhone == null) {
			if(other.customerServicePhone != null)
				return false;
		} else if(!customerServicePhone.equals(other.customerServicePhone))
			return false;
		if(dba == null) {
			if(other.dba != null)
				return false;
		} else if(!dba.equals(other.dba))
			return false;
		if(location == null) {
			if(other.location != null)
				return false;
		} else if(!location.equals(other.location))
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		if(paymentCycle == null) {
			if(other.paymentCycle != null)
				return false;
		} else if(!paymentCycle.equals(other.paymentCycle))
			return false;
		return true;
	}

	@Override
	public String toString() 
	{
		return String.format("Customer[name=%s, location=%s, dba=%s, customerServicePhone=%s, customerServiceEmail=%s, paymentCycle=%s]", name, location, dba, customerServicePhone, customerServiceEmail, paymentCycle);
	}

}
