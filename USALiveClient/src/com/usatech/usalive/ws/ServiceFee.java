package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;

public class ServiceFee implements Serializable
{
	private static final long serialVersionUID = 1L;

	@DecimalMin(value = "0.0", message = "service fee amount must be >= 0.0")
	@Digits(integer = 6, fraction = 2, message = "service fee amount can not exceed 2 decimal places")
	private double amount;

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(double amount)
	{
		this.amount = amount;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(amount);
		result = prime * result + (int)(temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof ServiceFee))
			return false;
		ServiceFee other = (ServiceFee)obj;
		if(Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("ServiceFee[amount=%s]", amount);
	}

}
