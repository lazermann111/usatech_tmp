package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class BankAccount implements Serializable
{
	private static final long serialVersionUID = 1L;

	public static final String ACCOUNT_TYPE_CHECKING = "C";
	public static final String ACCOUNT_TYPE_SAVINGS = "S";

	@NotNull(message = "bank name is required")
	@Size(min = 1, max = 50, message = "bank name must be between {min} and {max} in length")
	private String bankName;

	@Valid
	@NotNull(message = "bank location is required")
	private Location location;

	@NotNull(message = "bank account title is required")
	@Size(min = 1, max = 50, message = "bank account title must be between {min} and {max} in length")
	private String accountTitle;

	@NotNull(message = "bank account type is required")
	@Pattern(regexp = "[SC]{1}", message = "bank account type must be S for savings or C for checking")
	private String accountType;

	@NotNull(message = "bank aba number is required")
	@Pattern(regexp = "\\d{9}", message = "bank aba number must be 9 digits")
	private String abaNumber;

	@NotNull(message = "bank account number is required")
	@Size(min = 1, max = 20, message = "bank account number must be between {min} and {max} in length")
	private String accountNumber;

	@Size(min = 8, max = 12, message = "bank swift code must be between {min} and {max} in length")
	private String swiftCode;

	@Valid
	@NotNull(message = "bank contact is required")
	private BankContact bankContact;
	
	@NotNull(message = "bank tax id is required")
	@Digits(integer = 9, fraction = 0, message = "bank tax id must be at least 9 digits long")
	private String taxId;
	
	@Valid
	@NotNull(message = "bank billing location is required")
	private Location billingLocation;

	public String getBankName()
	{
		return bankName;
	}

	public void setBankName(String bankName)
	{
		this.bankName = bankName;
	}

	public Location getLocation()
	{
		return location;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	public String getAccountTitle()
	{
		return accountTitle;
	}

	public void setAccountTitle(String accountTitle)
	{
		this.accountTitle = accountTitle;
	}

	public String getAccountType()
	{
		return accountType;
	}

	public void setAccountType(String accountType)
	{
		this.accountType = accountType;
	}

	public String getAbaNumber()
	{
		return abaNumber;
	}

	public void setAbaNumber(String abaNumber)
	{
		this.abaNumber = abaNumber;
	}

	public String getAccountNumber()
	{
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}

	public String getSwiftCode()
	{
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode)
	{
		this.swiftCode = swiftCode;
	}

	public BankContact getBankContact()
	{
		return bankContact;
	}

	public void setBankContact(BankContact bankContact)
	{
		this.bankContact = bankContact;
	}
	
	public String getTaxId()
	{
		return taxId;
	}

	public void setTaxId(String taxId)
	{
		this.taxId = taxId;
	}
	
	public Location getBillingLocation() {
		return billingLocation;
	}

	public void setBillingLocation(Location billingLocation) {
		this.billingLocation = billingLocation;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((abaNumber == null) ? 0 : abaNumber.hashCode());
		result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result + ((accountTitle == null) ? 0 : accountTitle.hashCode());
		result = prime * result + ((accountType == null) ? 0 : accountType.hashCode());
		result = prime * result + ((bankContact == null) ? 0 : bankContact.hashCode());
		result = prime * result + ((bankName == null) ? 0 : bankName.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((swiftCode == null) ? 0 : swiftCode.hashCode());
		result = prime * result + ((taxId == null) ? 0 : taxId.hashCode());
		result = prime * result + ((billingLocation == null) ? 0 : billingLocation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof BankAccount))
			return false;
		BankAccount other = (BankAccount)obj;
		if(abaNumber == null) {
			if(other.abaNumber != null)
				return false;
		} else if(!abaNumber.equals(other.abaNumber))
			return false;
		if(accountNumber == null) {
			if(other.accountNumber != null)
				return false;
		} else if(!accountNumber.equals(other.accountNumber))
			return false;
		if(accountTitle == null) {
			if(other.accountTitle != null)
				return false;
		} else if(!accountTitle.equals(other.accountTitle))
			return false;
		if(accountType == null) {
			if(other.accountType != null)
				return false;
		} else if(!accountType.equals(other.accountType))
			return false;
		if(bankContact == null) {
			if(other.bankContact != null)
				return false;
		} else if(!bankContact.equals(other.bankContact))
			return false;
		if(bankName == null) {
			if(other.bankName != null)
				return false;
		} else if(!bankName.equals(other.bankName))
			return false;
		if(location == null) {
			if(other.location != null)
				return false;
		} else if(!location.equals(other.location))
			return false;
		if(swiftCode == null) {
			if(other.swiftCode != null)
				return false;
		} else if(!swiftCode.equals(other.swiftCode))
			return false;
		if(taxId == null) {
			if(other.taxId != null)
				return false;
		} else if(!taxId.equals(other.taxId))
			return false;
		if(billingLocation == null) {
			if(other.billingLocation != null)
				return false;
		} else if(!billingLocation.equals(other.billingLocation))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("BankAccount[bankName=%s,location=%s,accountTitle=%s,accountType=%s,abaNumber=%s,accountNumber=%s,swiftCode=%s,bankContact=%s,taxId=%s,billingLocation=%s]", bankName, location, accountTitle, accountType, abaNumber, accountNumber, swiftCode, bankContact, taxId, billingLocation);
	}

}
