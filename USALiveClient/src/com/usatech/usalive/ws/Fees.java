package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class Fees implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Valid
	@NotNull(message = "card present fee is required")
	private ProcessFee cardPresentFee;

	@Valid
	@NotNull(message = "card not present fee is required")
	private ProcessFee cardNotPresentFee;

	@Valid
	@NotNull(message = "service fee is required")
	private ServiceFee serviceFee;

	public ProcessFee getCardPresentFee()
	{
		return cardPresentFee;
	}

	public void setCardPresentFee(ProcessFee cardPresentFee)
	{
		this.cardPresentFee = cardPresentFee;
	}

	public ProcessFee getCardNotPresentFee()
	{
		return cardNotPresentFee;
	}

	public void setCardNotPresentFee(ProcessFee cardNotPresentFee)
	{
		this.cardNotPresentFee = cardNotPresentFee;
	}

	public ServiceFee getServiceFee()
	{
		return serviceFee;
	}

	public void setServiceFee(ServiceFee serviceFee)
	{
		this.serviceFee = serviceFee;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardNotPresentFee == null) ? 0 : cardNotPresentFee.hashCode());
		result = prime * result + ((cardPresentFee == null) ? 0 : cardPresentFee.hashCode());
		result = prime * result + ((serviceFee == null) ? 0 : serviceFee.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof Fees))
			return false;
		Fees other = (Fees)obj;
		if(cardNotPresentFee == null) {
			if(other.cardNotPresentFee != null)
				return false;
		} else if(!cardNotPresentFee.equals(other.cardNotPresentFee))
			return false;
		if(cardPresentFee == null) {
			if(other.cardPresentFee != null)
				return false;
		} else if(!cardPresentFee.equals(other.cardPresentFee))
			return false;
		if(serviceFee == null) {
			if(other.serviceFee != null)
				return false;
		} else if(!serviceFee.equals(other.serviceFee))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("Fees[cardPresentFee=%s,cardNotPresentFee=%s,serviceFee=%s]", cardPresentFee, cardNotPresentFee, serviceFee);
	}

}
