package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Device implements Serializable
{
	private static final long serialVersionUID = 1L;

	public static final String INDICATOR_YES = "Y";
	public static final String INDICATOR_NO = "N";

	@NotNull(message = "device serial number is required")
	private String serialNumber;

	@Size(min = 0, max = 15, message = "device asset number must be between {min} and {max} in length")
	private String customerAssetNumber;
	
	@Size(min = 0, max = 50, message = "device customer region must be between {min} and {max} in length")
	private String customerRegion;

	@Valid
	private Location location;

	@Size(min = 0, max = 50, message = "device location name must be between {min} and {max} in length")
	private String locationName;

	@NotNull(message = "location specify device indicator is required")
	@Size(min = 0, max = 50, message = "location specify must be between {min} and {max} in length")
	private String locationSpecify;

	@Pattern(regexp = "[0-9]{10,20}", message = "device location phone must be 10-20 digits")
	private String locationPhone;
	
	@NotNull(message = "location type is required")
	@Size(min = 0, max = 50, message = "location type must be between {min} and {max} in length")
	private String locationType;

	@Size(min = 0, max = 50, message = "location type specify must be between {min} and {max} in length")
	private String locationTypeSpecify;
	
	@NotNull(message = "product type is required")
	private String productType;
	
	@Size(min = 0, max = 50, message = "product type specify must be between {min} and {max} in length")
	private String productTypeSpecify;
	
	@NotNull(message = "business type is required")
	private String businessType;

	@NotNull(message = "device mobile device indicator is required")
	@Pattern(regexp = "[YN]{1}", message = "device mobile device indicator must be Y or N")
	private String mobileDeviceIndicator;

	@NotNull(message = "device attended indicator is required")
	@Pattern(regexp = "[YN]{1}", message = "device attended indicator must be Y if there is an attendant or cashier present N otherwise")
	private String attendedIndicator;

	@NotNull(message = "device entry type keypad indicator is required")
	@Pattern(regexp = "[YN]{1}", message = "device entry type keypad indicator must be Y or N")
	private String entryTypeKeypadIndicator;

	@NotNull(message = "device entry type card swipe indicator is required")
	@Pattern(regexp = "[YN]{1}", message = "device entry type card swipe indicator must be Y or N")
	private String entryTypeCardSwipeIndicator;

	@NotNull(message = "device entry type proximity indicator is required")
	@Pattern(regexp = "[YN]{1}", message = "device entry type proximity indicator must be Y or N")
	private String entryTypeProximityIndicator;

	@NotNull(message = "device entry type emv indicator is required")
	@Pattern(regexp = "[YN]{1}", message = "device entry type emv indicator must be Y or N")
	private String entryTypeEmvIndicator;

	@NotNull(message = "device signature captured indicator is required")
	@Pattern(regexp = "[YN]{1}", message = "device signature captured indicator must be Y or N")
	private String signatureCapturedIndicator;

	@Size(min = 0, max = 21, message = "device dba must be between {min} and {max} in length")
	private String dba;

	@Size(min = 0, max = 20, message = "device customer service phone must be between {min} and {max} in length")
	private String customerServicePhone;

	@Size(min = 0, max = 70, message = "device customer service email must be between {min} and {max} in length")
	private String customerServiceEmail;

	@NotNull(message = "device quick connect username is required")
	@Size(min = 5, max = 20, message = "device quick connect username must be between {min} and {max} in length")
	private String quickConnectUserName;

	public String getSerialNumber()
	{
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	public String getCustomerAssetNumber()
	{
		return customerAssetNumber;
	}

	public void setCustomerAssetNumber(String customerAssetNumber)
	{
		this.customerAssetNumber = customerAssetNumber;
	}

	public Location getLocation()
	{
		return location;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	public String getLocationName()
	{
		return locationName;
	}

	public void setLocationName(String locationName)
	{
		this.locationName = locationName;
	}

	public String getLocationSpecify() 
	{
		return locationSpecify;
	}

	public void setLocationSpecify(String locationSpecify) 
	{
		this.locationSpecify = locationSpecify;
	}

	public String getLocationPhone()
	{
		return locationPhone;
	}

	public void setLocationPhone(String locationPhone)
	{
		this.locationPhone = locationPhone;
	}

	public String getLocationType()
	{
		return locationType;
	}

	public void setLocationType(String locationType)
	{
		this.locationType = locationType;
	}

	public String getLocationTypeSpecify() 
	{
		return locationTypeSpecify;
	}

	public void setLocationTypeSpecify(String locationTypeSpecify) 
	{
		this.locationTypeSpecify = locationTypeSpecify;
	}

	public String getProductType()
	{
		return productType;
	}

	public void setProductType(String productType)
	{
		this.productType = productType;
	}

	public String getProductTypeSpecify() 
	{
		return productTypeSpecify;
	}

	public void setProductTypeSpecify(String productTypeSpecify) 
	{
		this.productTypeSpecify = productTypeSpecify;
	}

	public String getBusinessType()
	{
		return businessType;
	}

	public void setBusinessType(String businessType)
	{
		this.businessType = businessType;
	}

	public String getMobileDeviceIndicator()
	{
		return mobileDeviceIndicator;
	}

	public void setMobileDeviceIndicator(String mobileDeviceIndicator)
	{
		this.mobileDeviceIndicator = mobileDeviceIndicator;
	}

	public String getAttendedIndicator()
	{
		return attendedIndicator;
	}

	public void setAttendedIndicator(String attendedIndicator)
	{
		this.attendedIndicator = attendedIndicator;
	}

	public String getEntryTypeKeypadIndicator()
	{
		return entryTypeKeypadIndicator;
	}

	public void setEntryTypeKeypadIndicator(String entryTypeKeypadIndicator)
	{
		this.entryTypeKeypadIndicator = entryTypeKeypadIndicator;
	}

	public String getEntryTypeCardSwipeIndicator()
	{
		return entryTypeCardSwipeIndicator;
	}

	public void setEntryTypeCardSwipeIndicator(String entryTypeCardSwipeIndicator)
	{
		this.entryTypeCardSwipeIndicator = entryTypeCardSwipeIndicator;
	}

	public String getEntryTypeProximityIndicator()
	{
		return entryTypeProximityIndicator;
	}

	public void setEntryTypeProximityIndicator(String entryTypeProximityIndicator)
	{
		this.entryTypeProximityIndicator = entryTypeProximityIndicator;
	}

	public String getEntryTypeEmvIndicator()
	{
		return entryTypeEmvIndicator;
	}

	public void setEntryTypeEmvIndicator(String entryTypeEmvIndicator)
	{
		this.entryTypeEmvIndicator = entryTypeEmvIndicator;
	}

	public String getSignatureCapturedIndicator()
	{
		return signatureCapturedIndicator;
	}

	public void setSignatureCapturedIndicator(String signaturedCapturedIndicator)
	{
		this.signatureCapturedIndicator = signaturedCapturedIndicator;
	}

	public String getDba()
	{
		return dba;
	}

	public void setDba(String dba)
	{
		this.dba = dba;
	}
	
	public String getCustomerServicePhone() 
	{
		return customerServicePhone;
	}

	public void setCustomerServicePhone(String customerServicePhone) 
	{
		this.customerServicePhone = customerServicePhone;
	}

	public String getCustomerServiceEmail() 
	{
		return customerServiceEmail;
	}

	public void setCustomerServiceEmail(String customerServiceEmail) 
	{
		this.customerServiceEmail = customerServiceEmail;
	}

	public String getQuickConnectUserName()
	{
		return quickConnectUserName;
	}

	public void setQuickConnectUserName(String quickConnectUserName)
	{
		this.quickConnectUserName = quickConnectUserName;
	}

	public String getCustomerRegion() 
	{
		return customerRegion;
	}

	public void setCustomerRegion(String customerRegion) 
	{
		this.customerRegion = customerRegion;
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attendedIndicator == null) ? 0 : attendedIndicator.hashCode());
		result = prime * result + ((businessType == null) ? 0 : businessType.hashCode());
		result = prime * result + ((customerAssetNumber == null) ? 0 : customerAssetNumber.hashCode());
		result = prime * result + ((customerRegion == null) ? 0 : customerRegion.hashCode());
		result = prime * result + ((customerServiceEmail == null) ? 0 : customerServiceEmail.hashCode());
		result = prime * result + ((customerServicePhone == null) ? 0 : customerServicePhone.hashCode());
		result = prime * result + ((dba == null) ? 0 : dba.hashCode());
		result = prime * result + ((entryTypeCardSwipeIndicator == null) ? 0 : entryTypeCardSwipeIndicator.hashCode());
		result = prime * result + ((entryTypeEmvIndicator == null) ? 0 : entryTypeEmvIndicator.hashCode());
		result = prime * result + ((entryTypeKeypadIndicator == null) ? 0 : entryTypeKeypadIndicator.hashCode());
		result = prime * result + ((entryTypeProximityIndicator == null) ? 0 : entryTypeProximityIndicator.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((locationName == null) ? 0 : locationName.hashCode());
		result = prime * result + ((locationPhone == null) ? 0 : locationPhone.hashCode());
		result = prime * result + ((locationSpecify == null) ? 0 : locationSpecify.hashCode());
		result = prime * result + ((locationType == null) ? 0 : locationType.hashCode());
		result = prime * result + ((locationTypeSpecify == null) ? 0 : locationTypeSpecify.hashCode());
		result = prime * result + ((mobileDeviceIndicator == null) ? 0 : mobileDeviceIndicator.hashCode());
		result = prime * result + ((productType == null) ? 0 : productType.hashCode());
		result = prime * result + ((productTypeSpecify == null) ? 0 : productTypeSpecify.hashCode());
		result = prime * result + ((quickConnectUserName == null) ? 0 : quickConnectUserName.hashCode());
		result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
		result = prime * result + ((signatureCapturedIndicator == null) ? 0 : signatureCapturedIndicator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof Device))
			return false;
		Device other = (Device)obj;
		if(attendedIndicator == null) {
			if(other.attendedIndicator != null)
				return false;
		} else if(!attendedIndicator.equals(other.attendedIndicator))
			return false;
		if(businessType == null) {
			if(other.businessType != null)
				return false;
		} else if(!businessType.equals(other.businessType))
			return false;
		if(customerAssetNumber == null) {
			if(other.customerAssetNumber != null)
				return false;
		} else if(!customerAssetNumber.equals(other.customerAssetNumber))
			return false;
		if(customerRegion == null) {
			if(other.customerRegion != null)
				return false;
		} else if(!customerRegion.equals(other.customerRegion))
			return false;
		if(customerServiceEmail == null) {
			if(other.customerServiceEmail != null)
				return false;
		} else if(!customerServiceEmail.equals(other.customerServiceEmail))
			return false;
		if(customerServicePhone == null) {
			if(other.customerServicePhone != null)
				return false;
		} else if(!customerServicePhone.equals(other.customerServicePhone))
			return false;
		if(dba == null) {
			if(other.dba != null)
				return false;
		} else if(!dba.equals(other.dba))
			return false;
		if(entryTypeCardSwipeIndicator == null) {
			if(other.entryTypeCardSwipeIndicator != null)
				return false;
		} else if(!entryTypeCardSwipeIndicator.equals(other.entryTypeCardSwipeIndicator))
			return false;
		if(entryTypeEmvIndicator == null) {
			if(other.entryTypeEmvIndicator != null)
				return false;
		} else if(!entryTypeEmvIndicator.equals(other.entryTypeEmvIndicator))
			return false;
		if(entryTypeKeypadIndicator == null) {
			if(other.entryTypeKeypadIndicator != null)
				return false;
		} else if(!entryTypeKeypadIndicator.equals(other.entryTypeKeypadIndicator))
			return false;
		if(entryTypeProximityIndicator == null) {
			if(other.entryTypeProximityIndicator != null)
				return false;
		} else if(!entryTypeProximityIndicator.equals(other.entryTypeProximityIndicator))
			return false;
		if(location == null) {
			if(other.location != null)
				return false;
		} else if(!location.equals(other.location))
			return false;
		if(locationName == null) {
			if(other.locationName != null)
				return false;
		} else if(!locationName.equals(other.locationName))
			return false;
		if(locationPhone == null) {
			if(other.locationPhone != null)
				return false;
		} else if(!locationPhone.equals(other.locationPhone))
			return false;
		if(locationSpecify == null) {
			if(other.locationSpecify != null)
				return false;
		} else if(!locationSpecify.equals(other.locationSpecify))
			return false;
		if(locationType == null) {
			if(other.locationType != null)
				return false;
		} else if(!locationType.equals(other.locationType))
			return false;
		if(locationTypeSpecify == null) {
			if(other.locationTypeSpecify != null)
				return false;
		} else if(!locationTypeSpecify.equals(other.locationTypeSpecify))
			return false;
		if(mobileDeviceIndicator == null) {
			if(other.mobileDeviceIndicator != null)
				return false;
		} else if(!mobileDeviceIndicator.equals(other.mobileDeviceIndicator))
			return false;
		if(productType == null) {
			if(other.productType != null)
				return false;
		} else if(!productType.equals(other.productType))
			return false;
		if(productTypeSpecify == null) {
			if(other.productTypeSpecify != null)
				return false;
		} else if(!productTypeSpecify.equals(other.productTypeSpecify))
			return false;
		if(quickConnectUserName == null) {
			if(other.quickConnectUserName != null)
				return false;
		} else if(!quickConnectUserName.equals(other.quickConnectUserName))
			return false;
		if(serialNumber == null) {
			if(other.serialNumber != null)
				return false;
		} else if(!serialNumber.equals(other.serialNumber))
			return false;
		if(signatureCapturedIndicator == null) {
			if(other.signatureCapturedIndicator != null)
				return false;
		} else if(!signatureCapturedIndicator.equals(other.signatureCapturedIndicator))
			return false;
		return true;
	}

	@Override
	public String toString() 
	{
		return String.format("Device[serialNumber=%s, customerAssetNumber=%s, customerRegion=%s, location=%s, locationName=%s, locationSpecify=%s, locationPhone=%s, locationType=%s, locationTypeSpecify=%s, productType=%s, productTypeSpecify=%s, businessType=%s, mobileDeviceIndicator=%s, attendedIndicator=%s, entryTypeKeypadIndicator=%s, entryTypeCardSwipeIndicator=%s, entryTypeProximityIndicator=%s, entryTypeEmvIndicator=%s, signatureCapturedIndicator=%s, dba=%s, customerServicePhone=%s, customerServiceEmail=%s, quickConnectUserName=%s]", serialNumber, customerAssetNumber, customerRegion, location, locationName, locationSpecify, locationPhone, locationType, locationTypeSpecify, productType, productTypeSpecify, businessType, mobileDeviceIndicator, attendedIndicator, entryTypeKeypadIndicator, entryTypeCardSwipeIndicator, entryTypeProximityIndicator, entryTypeEmvIndicator, signatureCapturedIndicator, dba, customerServicePhone, customerServiceEmail, quickConnectUserName);
	}

}
