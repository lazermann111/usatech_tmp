package com.usatech.usalive.ws;

import java.lang.annotation.*;

import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@javax.validation.Constraint(validatedBy={})
@NotNull(message="password is required")
@Size(min=8, max=50, message="password must be between {min} and {max} in length")
@Pattern.List({
		@Pattern(regexp=".*[A-Z]{1,}.*", message="password must contain at least 1 uppercase letter"),
		@Pattern(regexp=".*[a-z]{1,}.*", message="password must contain at least 1 lowercase letter"),
		@Pattern(regexp=".*[!-@\\[-^`{-~]{1,}.*", message="password must contain at least 1 number or 1 punctuation symbol"),
})
@Target({
	ElementType.ANNOTATION_TYPE, 
	ElementType.METHOD, 
	ElementType.FIELD, 
	ElementType.CONSTRUCTOR, 
	ElementType.PARAMETER
})
@Retention(RetentionPolicy.RUNTIME)
public @interface Password {
	String message() default "invalid password";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
