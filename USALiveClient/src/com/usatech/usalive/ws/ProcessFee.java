package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;

public class ProcessFee implements Serializable
{
	private static final long serialVersionUID = 1L;

	@DecimalMin(value = "0.0", message = "process fee amount must be >= 0.0")
	@Digits(integer = 6, fraction = 2, message = "process fee amount can not exceed 2 decimal places")
	private double amount;

	@DecimalMin(value = "0.0", message = "process fee percent must be >= 0.0")
	@DecimalMax(value = "100.0", message = "process fee percent must be <= 100.0")
	@Digits(integer = 3, fraction = 2, message = "process fee percent can not exceed 2 decimal places")
	private double percent;
	
	@DecimalMin(value = "0.0", message = "minimum amount must be >= 0.0")
	@Digits(integer = 6, fraction = 2, message = "minimum amount can not exceed 2 decimal places")
	private double minimumAmount = 0;

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(double amount)
	{
		this.amount = amount;
	}

	public double getPercent()
	{
		return percent;
	}

	public void setPercent(double percent)
	{
		this.percent = percent;
	}
	
	public double getMinimumAmount()
	{
		return minimumAmount;
	}

	public void setMinimumAmount(double minimumAmount)
	{
		this.minimumAmount = minimumAmount;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(amount);
		result = prime * result + (int)(temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(minimumAmount);
		result = prime * result + (int)(temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(percent);
		result = prime * result + (int)(temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof ProcessFee))
			return false;
		ProcessFee other = (ProcessFee)obj;
		if(Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount))
			return false;
		if(Double.doubleToLongBits(minimumAmount) != Double.doubleToLongBits(other.minimumAmount))
			return false;
		if(Double.doubleToLongBits(percent) != Double.doubleToLongBits(other.percent))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("ProcessFee[amount=%s,percent=%s,minimumAmount=%s]", amount, percent, minimumAmount);
	}

}
