package com.usatech.usalive.ws;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Location implements Serializable
{
	private static final long serialVersionUID = 1L;

	public static final String COUNTRY_US = "US";
	public static final String COUNTRY_CA = "CA";

	@NotNull(message = "customer address is required")
	@Size(min = 1, max = 50, message = "customer address must be between {min} and {max} in length")
	private String address;

	@NotNull(message = "customer postal code is required")
	@Size(min = 1, max = 50, message = "customer postal code must be between {min} and {max} in length")
	private String postalCd;

	@NotNull(message = "customer country code is required")
	@Pattern(regexp = "(US|CA)", flags = Pattern.Flag.CASE_INSENSITIVE, message = "customer country code must be 2 letter ISO format US or CA")
	private String countryCd;

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getPostalCd()
	{
		return postalCd;
	}

	public void setPostalCd(String postalCd)
	{
		this.postalCd = postalCd;
	}

	public String getCountryCd()
	{
		return countryCd;
	}

	public void setCountryCd(String countryCd)
	{
		this.countryCd = countryCd;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((countryCd == null) ? 0 : countryCd.hashCode());
		result = prime * result + ((postalCd == null) ? 0 : postalCd.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(!(obj instanceof Location))
			return false;
		Location other = (Location)obj;
		if(address == null) {
			if(other.address != null)
				return false;
		} else if(!address.equals(other.address))
			return false;
		if(countryCd == null) {
			if(other.countryCd != null)
				return false;
		} else if(!countryCd.equals(other.countryCd))
			return false;
		if(postalCd == null) {
			if(other.postalCd != null)
				return false;
		} else if(!postalCd.equals(other.postalCd))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return String.format("Location[address=%s,postalCd=%s,countryCd=%s]", address, postalCd, countryCd);
	}

}
