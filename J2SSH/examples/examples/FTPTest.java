//package examples;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//
//import sun.net.ftp.FtpClient;
//
///*
// * Created on Mar 30, 2005
// *
// */
//
///**
// * @author bkrug
// *
// */
//public class FTPTest {
//
//	public static void main(String args[]) throws Exception {
//	    System.setProperty("org.apache.commons.logging.simplelog.defaultlog", "trace");
//	    //testFTP("www.idglink.com", 21, "squared", "idg011606", ".", "SFTP-Test1.txt", new StringBufferInputStream("This is a test. Please disregard."));
//        testFTP("www.idglink.com", 21, "squared", "idg011606", ".", "SFTP-Test1.txt", null);
//	}
//
//	protected static void testFTP(String host, int port, String username, String password,
//            String remoteDir, String fileName, InputStream content) throws Exception {
//        // Make a client connection
//        FtpClient ftp = new FtpClient();
//        ftp.openServer(host, port);
//        try {
//            ftp.login(username, password);
//            //ftp.ascii();
//            ftp.cd(remoteDir);
//            // list directory contents
//            byte[] buffer = new byte[1024];
//            int n = 0;
//            try {
//                InputStream in = ftp.list();
//                System.out.println("Files:");
//                System.out.println("-------------------------");
//                while((n=in.read(buffer)) >= 0) {
//                    System.out.write(buffer, 0, n);
//                }
//                System.out.println();
//            } catch(IOException ioe) {
//                System.out.println("Couldn't delete remote copy of file because:");
//                ioe.printStackTrace();
//            }
//            if(content != null) {
//                OutputStream out = ftp.put(fileName);
//                while((n=content.read(buffer)) >= 0) {
//                    out.write(buffer, 0, n);
//                }
//                out.flush();
//                out.close();
//            }
//        } finally {
//            ftp.closeServer();
//        }
//    }
//	
//}
