package examples;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.crypto.Cipher;

import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;
import com.sshtools.j2ssh.authentication.PasswordAuthenticationClient;
import com.sshtools.j2ssh.sftp.SftpFile;
import com.sshtools.j2ssh.transport.IgnoreHostKeyVerification;

/*
 * Created on Mar 30, 2005
 *
 */

/**
 * @author bkrug
 *
 */
public class Test {

	public static void main(String args[]) throws Exception {
	    /*
	    int blockSize = Cipher.getInstance("Blowfish/CBC/NoPadding").getBlockSize();
	    System.out.println("Cipher block size = " + blockSize);
	    /*/
	    //testSFTP("usaeap01", 22, "root", "ro11abzp", "/usr/tmp", "testSFTP.txt", "This is a test of the sftp library from java.\nPlease disregard.\n");
	    System.setProperty("org.apache.commons.logging.simplelog.defaultlog", "trace");
	    //*
	    //File attachment = new File("X:/data/TRANS-77268-20050329-050805.csv");	    
	    //testSFTP("209.248.181.101", 22, "USATech", "Password1", "USATech", attachment.getName(), new FileInputStream(attachment));
        testSFTP("usadbt01", 22, "root", "ro11abzp", "/root", "testSFTP.txt", new StringBufferInputStream("This is a test. Please disregard."));
        
        //testSFTP("209.248.181.101", 22, "USATech", "Password1", "USATech", "SFTP-Test1.txt", "This is a test. Please disregard.");
	    //*/
	}

	protected static void testSFTP(String host, int port, String username, String password,
            String remoteDir, String fileName, InputStream content) throws Exception {
        // Make a client connection
        SshClient ssh = new SshClient();
        // Connect to the host
        ssh.connect(host, new IgnoreHostKeyVerification());
        SftpClient sftp = null;
        try {
            // Create a password authentication instance
            PasswordAuthenticationClient pwd = new PasswordAuthenticationClient();
            pwd.setUsername(username);
            pwd.setPassword(password);
            // Try the authentication
            int result = ssh.authenticate(pwd);
            // Evaluate the result
            if (result != AuthenticationProtocolState.COMPLETE)
                throw new IOException("Authorization not completed");

            // The connection is authenticated we can now do some real work!
            sftp = ssh.openSftpClient();
            // Change directory
            sftp.cd(remoteDir);
            try {
                sftp.rm(fileName);
            } catch(IOException ioe) {
                System.out.println("Couldn't delete remote copy of file because:");
                ioe.printStackTrace();
            }
            //list directory contents
            try {
                List files = sftp.ls();
                System.out.println("Files:");
                System.out.println("-------------------------");
                for(Iterator iter = files.iterator(); iter.hasNext();) {
                    SftpFile file = (SftpFile)iter.next();
                    System.out.println(file.getFilename() + "\t" + file.getAttributes().getModTimeString() + "\t" + file.getAttributes().getSize());
                }
            } catch(IOException ioe) {
                System.out.println("Couldn't delete remote copy of file because:");
                ioe.printStackTrace();
            }
            // Upload a file
            sftp.put(content, fileName);
        } finally {
            if (sftp != null)
                sftp.quit();
            ssh.disconnect();
        }
    }
	
}
