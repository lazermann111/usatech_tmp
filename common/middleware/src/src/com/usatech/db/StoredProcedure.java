package com.usatech.db;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Vector;

public class StoredProcedure extends Query
{
	protected boolean resultStatus;
	
	public static void main(String args[])
	{
		try
		{
			String sql = "{? = pkg_process_transaction.sp_authorize(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
			Vector v = new Vector();
			v.addElement("205603604");
			v.addElement(new Integer(439));
			v.addElement("12345678");
			v.addElement("S");
			v.addElement(new Float(10.00f));
			v.addElement(new java.util.Date());
			v.addElement("Y");
			
			OutParameter approvalParam = new OutParameter(Types.VARCHAR);
			OutParameter resultCodeParam = new OutParameter(Types.NUMERIC);
			OutParameter resultMsgParam = new OutParameter(Types.VARCHAR);
			InOutParameter test = new InOutParameter(Types.VARCHAR, "asdf");
			
			v.addElement(approvalParam);
			v.addElement(resultCodeParam);
			v.addElement(resultMsgParam);
			v.addElement(test);			
			
			StoredProcedure proc = new StoredProcedure(sql, v);
			if(!proc.execute())
			{
				System.out.println("execute() failed!");
				if(proc.getException() != null)
				{
					proc.getException().printStackTrace();
				}
				else
				{
					System.out.println("No exception!");
				}
			}
			else
			{
				System.out.println("approved      = " + approvalParam.getString());
				System.out.println("resultCode    = " + resultCodeParam.getInt());
				System.out.println("resultMessage = " + resultMsgParam.getString());
			}
			
			proc.close();
			
			/*
			Connection connection = SQL.getConnectionManager().getConnection();
			CallableStatement cs = connection.prepareCall("{call pkg_process_transaction.sp_authorize(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
			cs.setString(1, "205603604");
			cs.setInt(2, 439);
			cs.setString(3, "12345678");
			cs.setString(4, "S");
			cs.setFloat(5, 10.00f);
			cs.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			cs.setString(7, "Y");
			cs.registerOutParameter(8, Types.VARCHAR);
			cs.registerOutParameter(9, Types.NUMERIC);
			cs.registerOutParameter(10, Types.VARCHAR);

			boolean result = cs.execute();
			
			String approvedFlag = cs.getString(8);
			int resultCode = cs.getInt(9);
			String resultMsg = cs.getString(10);
			
			System.out.println("approvedFlag = " + approvedFlag);
			System.out.println("resultCode = " + resultCode);
			System.out.println("resultMsg = " + resultMsg);
			
			cs.close();
			SQL.getConnectionManager().freeConnection(connection);
			*/
			 
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public StoredProcedure(String sql, Vector args)
	{
		super(sql, args);
	}

	public StoredProcedure(String sql, Vector args, String dbName)
	{
		super(sql, args, dbName);
	}
	
	public boolean getResultStatus() { return resultStatus; }
	
	public boolean execute()
	{
		if(!connect())
			return false;
		
		try
		{
			stmt = merge(con.prepareCall(sql), args);
			resultStatus = ((CallableStatement)stmt).execute();
			rs = stmt.getResultSet();
			if(rs != null)
				rsmd = rs.getMetaData();
		}
		catch (Exception ex)
		{
			close();
			
			if(ex instanceof SQLException)
				exception = (SQLException) ex;
			else
			{
				exception = new SQLException(ex.getMessage());
				//ex.printStackTrace();
			}
		}
		
		if(failed())
			return false;
		
		try
		{
			for(int i=0; i<args.size(); i++)
			{
				if(args.elementAt(i) instanceof OutParameter)
				{
					((OutParameter)args.elementAt(i)).setOutValue(((CallableStatement)stmt).getObject(i+1));
				}
			}
		}
		catch(SQLException ex)
		{
			close();
			
			if(ex instanceof SQLException)
				exception = (SQLException) ex;
			else
			{
				exception = new SQLException(ex.getMessage());
				//ex.printStackTrace();
			}
			
			return false;
		}
			
		return true;  
	}
}