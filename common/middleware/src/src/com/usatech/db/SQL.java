package com.usatech.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

public abstract class SQL
{
	public static final SimpleDateFormat	DATABASE_FORMATTER	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	protected static DBConnectionManager	connectionManager	= DBConnectionManager.getInstance();

	protected String						sql;
	protected Vector						args;
	protected String						dbName;

	protected Connection					con					= null;
	protected PreparedStatement				stmt				= null;

	protected SQLException					exception;
	protected long							connectionTimeout	= -1;

	protected SQL(String sql, Vector args)
	{
		this(sql, args, connectionManager.getDefault());
	}

	protected SQL(String sql, Vector args, String dbName)
	{
		this.sql = sql;
		this.args = args;
		this.dbName = dbName;

		if (sql.endsWith(";"))
			sql = sql.substring(0, sql.length() - 1);

		if (this.args == null)
			this.args = new Vector();
	}

	public static DBConnectionManager getConnectionManager()
	{
		return connectionManager;
	}

	public String getDBName()
	{
		return dbName;
	}

	public String getSQL()
	{
		return sql;
	}

	public Vector getArgs()
	{
		return args;
	}

	public String getDBCMDefaultDBName()
	{
		return connectionManager.getDefault();
	}

	public String getDBCMSecondaryDBName()
	{
		return connectionManager.getSecondary();
	}

	public int getDBCMDefaultDBType()
	{
		return connectionManager.getDefaultDBType();
	}

	public int getDBCMSecondaryDBType()
	{
		return connectionManager.getSecondaryDBType();
	}

	public Connection getConnection()
	{
		return con;
	}

	public PreparedStatement getStatement()
	{
		return stmt;
	}

	public SQLException getException()
	{
		return exception;
	}

	public boolean failed()
	{
		return (exception != null);
	}

	public void setConnectionTimeout(long ms)
	{
		this.connectionTimeout = ms;
	}

	public long getConnectionTimeout()
	{
		return this.connectionTimeout;
	}

	public boolean isActive()
	{
		return (con != null);
	}

	protected boolean connect()
	{
		if (connectionManager != null)
		{
			if (getConnectionTimeout() > 0)
				con = connectionManager.getConnection(getDBName(), getConnectionTimeout());
			else
				con = connectionManager.getConnection(getDBName());

			if (con == null)
			{
				exception = new SQLException("Failed to get a connection to the database '" + getDBName() + "'");
				return false;
			}
		}
		else
		{
			exception = new SQLException("ConnectionManager is null");
			return false;
		}

		return true;
	}

	public void close()
	{
		if (stmt != null)
		{
			try
			{
				stmt.close();
			}
			catch (Exception e)
			{
			}
		}

		if (con != null)
		{
			if (failed())
				connectionManager.freeConnection(getDBName(), con, true);
			else
				connectionManager.freeConnection(getDBName(), con);
		}

		stmt = null;
		con = null;
	}

	public static PreparedStatement merge(PreparedStatement stmt, Vector args) throws SQLException
	{
		if (args == null)
			args = new Vector();

		for (int i = 0; i < args.size(); i++)
		{
			Object obj = args.elementAt(i);
			if (obj == null)
				stmt.setNull(i + 1, java.sql.Types.VARCHAR);
			else if (obj instanceof Integer)
				stmt.setInt(i + 1, ((Integer) obj).intValue());
			else if (obj instanceof Long)
				stmt.setLong(i + 1, ((Long) obj).longValue());
			else if (obj instanceof Float)
				stmt.setFloat(i + 1, ((Float) obj).floatValue());
			else if (obj instanceof Double)
				stmt.setDouble(i + 1, ((Double) obj).doubleValue());
			else if (obj instanceof Character)
				stmt.setString(i + 1, ((Character) obj).toString());
			else if (obj instanceof String)
				stmt.setString(i + 1, (String) obj);
			else if (obj instanceof java.util.Date)
			{
				//stmt.setTimestamp(i+1, new Timestamp(((java.util.Date)obj).getTime()));
				Timestamp timestamp = new Timestamp(((java.util.Date) obj).getTime());
				stmt.setTimestamp(i + 1, timestamp);
			}
			else if ((obj instanceof OutParameter) && (stmt instanceof CallableStatement))
			{
				((CallableStatement) stmt).registerOutParameter(i + 1, ((OutParameter) obj).getOutSqlType());
			}
			else if ((obj instanceof InOutParameter) && (stmt instanceof CallableStatement))
			{
				((CallableStatement) stmt).setObject(i + 1, ((InOutParameter) obj).getInValue(), ((InOutParameter) obj).getOutSqlType());
				((CallableStatement) stmt).registerOutParameter(i + 1, ((InOutParameter) obj).getOutSqlType());
			}
		}

		return stmt;
	}

	public abstract boolean execute();

	public static String queryToString(String sql, Vector args)
	{
		if (sql == null || args == null)
			return null;
		StringBuffer sb = new StringBuffer();
		StringTokenizer st = new StringTokenizer(sql, "?", true);
		String temp = null;
		String value = null;
		Enumeration values = args.elements();
		while (st.hasMoreTokens())
		{
			temp = st.nextToken();
			if (temp.equals("?"))
			{
				Object obj = values.nextElement();
				if (obj instanceof String)
				{
					value = "'" + (String) obj + "'";
				}
				else if (obj instanceof java.util.Date)
				{
					value = "'" + SQL.DATABASE_FORMATTER.format((java.util.Date) obj) + "'";
				}
				else if (obj != null)
					value = obj.toString();
				else
					value = "null";

				sb.append(value);
			}
			else
			{
				sb.append(temp);
			}
		}
		return sb.toString();
	}
}
