package com.usatech.db;

public class OutParameter
{
	protected int		outSqlType;
	protected Object	outValue;

	public OutParameter(int outSqlType)
	{
		this.outSqlType = outSqlType;
	}

	public int getOutSqlType()
	{
		return outSqlType;
	}

	protected void setOutValue(Object outValue)
	{
		this.outValue = outValue;
	}

	public Object getObject()
	{
		return outValue;
	}

	public int getInt()
	{
		if (outValue == null)
			return -1;

		try
		{
			return Integer.parseInt(outValue.toString());
		}
		catch (Exception e)
		{
			return -1;
		}
	}

	public long getLong()
	{
		if (outValue == null)
			return -1;

		try
		{
			return Long.parseLong(outValue.toString());
		}
		catch (Exception e)
		{
			return -1;
		}
	}

	public float getFloat()
	{
		if (outValue == null)
			return -1;

		try
		{
			return Float.parseFloat(outValue.toString());
		}
		catch (Exception e)
		{
			return -1;
		}
	}

	public double getDouble()
	{
		if (outValue == null)
			return -1;

		try
		{
			return Double.parseDouble(outValue.toString());
		}
		catch (Exception e)
		{
			return -1;
		}
	}

	public String getString()
	{
		if (outValue == null)
			return null;

		return outValue.toString();
	}

	public java.util.Date getDate()
	{
		if (outValue == null)
			return null;

		return (java.util.Date) outValue;
	}

	public byte[] getBytes()
	{
		if (outValue == null)
			return null;

		return (byte[]) outValue;
	}
}
