package com.usatech.db;

import java.sql.SQLException;
import java.util.Vector;

public class Update extends SQL
{
	protected int	resultCode	= -1;

	public Update(String sql, Vector args)
	{
		super(sql, args);
	}

	public Update(String sql, Vector args, String dbName)
	{
		super(sql, args, dbName);
	}

	public int getResultCode()
	{
		return resultCode;
	}

	public boolean execute()
	{
		//Util.output("SQL", SQL.queryToString(sql, args));

		if (!connect())
			return false;

		try
		{
			stmt = merge(con.prepareStatement(sql), args);
			resultCode = stmt.executeUpdate();
		}
		catch (Exception ex)
		{
			if (ex instanceof SQLException)
				exception = (SQLException) ex;
			else
				exception = new SQLException(ex.getMessage());

			close();
		}

		return !failed();
	}
}
