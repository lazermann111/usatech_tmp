package com.usatech.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Query extends SQL
{
	protected ResultSet			rs		= null;
	protected ResultSetMetaData	rsmd	= null;

	public static void main(String args[])
	{
		if (args.length != 2)
			return;

		Query query = new Query(args[0], null, args[1]);
		if (query.execute())
		{
			while (query.next())
			{
				System.out.println(query.getLine(","));
			}
		}
		else
		{
			query.getException().printStackTrace(System.out);
		}

		query.close();
	}

	public Query(String sql, Vector args)
	{
		super(sql, args);
	}

	public Query(String sql, Vector args, String dbName)
	{
		super(sql, args, dbName);
	}

	public ResultSet getResultSet()
	{
		return rs;
	}

	public ResultSetMetaData getMetaData()
	{
		return rsmd;
	}

	public boolean execute()
	{
		//Util.output("SQL", SQL.queryToString(sql, args));

		if (!connect())
			return false;

		try
		{
			stmt = merge(con.prepareStatement(sql), args);
			;
			rs = stmt.executeQuery();
			rsmd = rs.getMetaData();
		}
		catch (Exception ex)
		{
			if (ex instanceof SQLException)
				exception = (SQLException) ex;
			else
			{
				exception = new SQLException(ex.getMessage());
				//ex.printStackTrace();
			}

			close();
		}

		return !failed();
	}

	public void close()
	{
		if (rs != null)
		{
			try
			{
				rs.close();
			}
			catch (Exception e)
			{
			}
		}
		super.close();
		rsmd = null;
		rs = null;
	}

	public boolean next()
	{
		if (rs == null)
			return false;

		try
		{
			return rs.next();
		}
		catch (SQLException e)
		{
			if (e.getMessage().startsWith("Closed Connection: next"))
			{
				return false;
			}
			else
			{
				close();
				exception = e;
				return false;
			}
		}
	}

	public String getLine(String delim)
	{
		StringBuffer sb = new StringBuffer();
		int numCols = getColumnCount();
		for (int i = 1; i <= getColumnCount(); i++)
		{
			sb.append(getString(i));
			if (i < numCols)
				sb.append(delim);
		}

		return sb.toString();
	}

	public int getColumnCount()
	{
		try
		{
			if (rsmd != null)
				return rsmd.getColumnCount();
		}
		catch (SQLException e)
		{
			close();
			exception = e;
		}

		return 0;
	}

	public String getColumnName(int colNum)
	{
		try
		{
			if (rsmd != null)
				return rsmd.getColumnName(colNum);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return null;
	}

	public String getString(int colNum)
	{
		try
		{
			if (rs != null)
				return rs.getString(colNum);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return null;
	}

	public String getString(String colName)
	{
		try
		{
			if (rs != null)
				return rs.getString(colName);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return null;
	}

	public double getFloat(int colNum)
	{
		try
		{
			if (rs != null)
				return rs.getFloat(colNum);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return -1;
	}

	public double getFloat(String colName)
	{
		try
		{
			if (rs != null)
				return rs.getFloat(colName);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return -1;
	}

	public double getDouble(int colNum)
	{
		try
		{
			if (rs != null)
				return rs.getDouble(colNum);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return -1;
	}

	public double getDouble(String colName)
	{
		try
		{
			if (rs != null)
				return rs.getDouble(colName);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return -1;
	}

	public java.util.Date getDate(int colNum)
	{
		try
		{
			if (rs != null)
				return rs.getTimestamp(colNum);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return null;
	}

	public java.util.Date getDate(String colName)
	{
		try
		{
			if (rs != null)
				return rs.getTimestamp(colName);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return null;
	}

	public int getInt(int colNum)
	{
		try
		{
			if (rs != null)
				return rs.getInt(colNum);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return -1;
	}

	public int getInt(String colName)
	{
		try
		{
			if (rs != null)
				return rs.getInt(colName);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return -1;
	}

	public long getLong(int colNum)
	{
		try
		{
			if (rs != null)
				return rs.getLong(colNum);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return -1;
	}

	public long getLong(String colName)
	{
		try
		{
			if (rs != null)
				return rs.getLong(colName);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return -1;
	}

	public byte[] getBytes(int colNum)
	{
		try
		{
			if (rs != null)
				return rs.getBytes(colNum);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return null;
	}

	public byte[] getBytes(String colName)
	{
		try
		{
			if (rs != null)
				return rs.getBytes(colName);
		}
		catch (SQLException e)
		{
			exception = e;
		}
		return null;
	}
}
