package com.usatech.db;

/*
 * Based on DBConnectionManager as described in 
 * http://www.webdevelopersjournal.com/columns/connection_pool.html
 */

import java.io.*;
import java.sql.*;
import java.util.*;
import com.usatech.util.Util;

public class DBConnectionManager
{
	public static final int				DB_UNKNOWN			= 0;
	public static final int				DB_ORACLE			= 1;
	public static final int				DB_POSTGRESQL		= 2;
	public static final int				DB_MYSQL			= 3;

	private static DBConnectionManager	instance;								// The single instance
	private static int					numClients;

	private Vector						drivers				= new Vector();
	private Hashtable					pools				= new Hashtable();
	private PrintWriter					log;
	private String						defaultName			= null;
	private String						secondaryName		= null;
	private int							defaultDBType		= DB_UNKNOWN;
	private int							secondaryDBType		= DB_UNKNOWN;
	private boolean						closing				= false;
	private boolean						releaseOnShutdown	= true;

	/**
	 * A private constructor since this is a Singleton
	 */
	private DBConnectionManager()
	{
		init();
	}

	public static void initialize()
	{
		instance = new DBConnectionManager();
	}

	/**
	 * Returns the single instance, creating one if it's the first time this method is called.
	 * @return DBConnectionManager The single instance.
	 */
	protected static synchronized DBConnectionManager getInstance()
	{
		if (instance == null)
			initialize();
		numClients++;
		return instance;
	}

	/**
	 * Return the name of the default connection, or "unset" if none has been specified.
	 * @return String The name of the default connection.
	 */
	public String getDefault()
	{
		return defaultName;
	}

	/**
	 * Return the name of the secondary connection.
	 * @return String The name of the secondary connection.
	 */
	public String getSecondary()
	{
		return secondaryName;
	}

	/**
	 * Return the database type of the default connection.
	 * @return int The database type of the default connection.
	 */
	public int getDefaultDBType()
	{
		return defaultDBType;
	}

	/**
	 * Return the database type of the secondary connection.
	 * @return int The database type of the secondary connection.
	 */
	public int getSecondaryDBType()
	{
		return secondaryDBType;
	}

	/**
	 * Returns an open connection to the default database. If none is 
	 * available, and the max number of connections has not been reached, 
	 * a new connection is created.
	 *
	 * @return Connection The connection or null
	 */
	public Connection getConnection()
	{
		return getConnection(defaultName);
	}

	/**
	 * Returns an open connection. If no one is available, and the max
	 * number of connections has not been reached, a new connection is
	 * created.
	 *
	 * @param name The pool name as defined in the properties file
	 * @return Connection The connection or null
	 */
	public Connection getConnection(String name)
	{
		DBConnectionPool pool = (DBConnectionPool) pools.get(name);
		if (pool != null)
			return pool.getConnection();
		else
			return null;
	}

	/**
	 * Returns an open connection to the default database. If none is 
	 * available, and the max number of connections has not been reached, 
	 * a new connection is created. If the max number has been reached, 
	 * waits until one is available or the specified time has elapsed.
	 *
	 * @param time The number of milliseconds to wait
	 * @return Connection The connection or null
	 */
	public Connection getConnection(long time)
	{
		return getConnection(defaultName, time);
	}

	/**
	 * Returns an open connection. If none is available, and the max
	 * number of connections has not been reached, a new connection is
	 * created. If the max number has been reached, waits until one
	 * is available or the specified time has elapsed.
	 *
	 * @param name The pool name as defined in the properties file
	 * @param time The number of milliseconds to wait
	 * @return Connection The connection or null
	 */
	public Connection getConnection(String name, long time)
	{
		DBConnectionPool pool = (DBConnectionPool) pools.get(name);
		if (pool != null)
			return pool.getConnection(time);
		else
			return null;
	}

	/**
	 * Returns a connection to the default pool.
	 *
	 * @param name The pool name as defined in the properties file
	 * @param con The Connection
	 */
	public void freeConnection(Connection con)
	{
		freeConnection(defaultName, con);
	}

	/**
	 * Returns a connection to the named pool.
	 *
	 * @param name The pool name as defined in the properties file
	 * @param con The Connection
	 */
	public void freeConnection(String name, Connection con)
	{
		freeConnection(name, con, false);
	}

	/**
	 * Returns a connection to the named pool.
	 *
	 * @param name The pool name as defined in the properties file
	 * @param con The Connection
	 * @param recycle If true the release connection will be close and replaced with a fresh one
	 */
	public void freeConnection(String name, Connection con, boolean recycle)
	{
		DBConnectionPool pool = (DBConnectionPool) pools.get(name);
		if (pool != null)
		{
			if (recycle)
			{
				pool.recycle(con);
			}
			else
			{
				pool.freeConnection(con);
			}
		}
	}

	/**
	 * If true, DBConnectionManager will execute use a Shutdown Hook to release
	 * all connections on shutdown.  If you use a ShutDown Hook in your classes 
	 * you may want set this to false, and do your own database cleanup.
	 * 
	 * @param releaseOnShutdown Default is TRUE
	 */
	public void setReleaseOnShutdown(boolean releaseOnShutdown)
	{
		this.releaseOnShutdown = releaseOnShutdown;
	}

	/**
	 * Closes all open connections and deregisters all drivers if all connections have been released.
	 */
	public synchronized void release()
	{
		release(false);
	}

	/**
	 * Closes all open connections and deregisters all drivers.
	 * 
	 * @param force If true, disregard currently open connections and close immediatly.
	 */
	public synchronized void release(boolean force)
	{
		if (closing)
			return;

		closing = true;

		// Wait until called by the last client
		if ((--numClients != 0) && (!force))
			return;

		Enumeration allPools = pools.elements();
		while (allPools.hasMoreElements())
		{
			DBConnectionPool pool = (DBConnectionPool) allPools.nextElement();
			pool.release();
		}

		Enumeration allDrivers = drivers.elements();
		while (allDrivers.hasMoreElements())
		{
			Driver driver = (Driver) allDrivers.nextElement();
			try
			{
				DriverManager.deregisterDriver(driver);
				log("Deregistered JDBC driver " + driver.getClass().getName());
			}
			catch (SQLException e)
			{
				log(e, "Can't deregister JDBC driver: " + driver.getClass().getName());
			}
		}
	}

	/**
	 * Creates instances of DBConnectionPool based on the properties.
	 * A DBConnectionPool can be defined with the following properties:
	 * <PRE>
	 * &lt;poolname&gt;.url		 The JDBC URL for the database
	 * &lt;poolname&gt;.user		A database user (optional)
	 * &lt;poolname&gt;.password	A database user password (if user specified)
	 * &lt;poolname&gt;.maxconn	 The maximum number of connections (optional)
	 * </PRE>
	 *
	 * @param props The connection pool properties
	 */
	private void createPools(Properties props)
	{
		Enumeration propNames = props.propertyNames();
		while (propNames.hasMoreElements())
		{
			String name = (String) propNames.nextElement();
			if (name.endsWith(".url"))
			{
				String poolName = name.substring(0, name.lastIndexOf("."));
				String url = props.getProperty(poolName + ".url");
				if (url == null)
				{
					log("No URL specified for " + poolName);
					continue;
				}
				String user = props.getProperty(poolName + ".user");
				String password = props.getProperty(poolName + ".password");
				String maxconn = props.getProperty(poolName + ".maxconn", "0");
				String minconn = props.getProperty(poolName + ".minconn", "0");
				long recycleTime = Util.parseLong(props.getProperty(poolName + ".recycletime", "0"), 0);
				boolean isDefault = Boolean.valueOf(props.getProperty(poolName + ".default", "false")).booleanValue();

				int max;
				try
				{
					max = Integer.valueOf(maxconn).intValue();
				}
				catch (NumberFormatException e)
				{
					log("Invalid maxconn value " + maxconn + " for " + poolName);
					max = 0;
				}

				int min;
				try
				{
					min = Integer.valueOf(minconn).intValue();
				}
				catch (NumberFormatException e)
				{
					log("Invalid minconn value " + minconn + " for " + poolName);
					min = 0;
				}

				String defaultTimeoutStr = props.getProperty(poolName + ".conntimeout", "0");
				int defaultTimeout = 0;
				try
				{
					defaultTimeout = Integer.valueOf(defaultTimeoutStr).intValue();
				}
				catch (NumberFormatException e)
				{
					log("Invalid conntimeout value " + defaultTimeoutStr + " for " + poolName);
					defaultTimeout = 0;
				}

				DBConnectionPool pool = new DBConnectionPool(poolName, url, user, password, max, min, recycleTime, defaultTimeout);
				pools.put(poolName, pool);
				log("Initialized pool " + poolName);

				int dbType = DB_UNKNOWN;
				if (url.startsWith("jdbc:postgresql:") == true)
					dbType = DB_POSTGRESQL;
				else if (url.startsWith("jdbc:oracle:") == true)
					dbType = DB_ORACLE;
				else if (url.startsWith("jdbc:mysql:") == true)
					dbType = DB_MYSQL;

				if (isDefault)
				{
					defaultName = poolName;
					defaultDBType = dbType;
				}
				else if (secondaryName == null)
				{
					secondaryName = poolName;
					secondaryDBType = dbType;
				}
			}
		}

		new RecycleThread();

		if (defaultName == null)
		{
			log("No default connection specified.");
		}
	}

	/**
	 * Loads properties and initializes the instance with its values.
	 */
	private void init()
	{
		ShutdownThread shutdownThread = new ShutdownThread();
		Runtime.getRuntime().addShutdownHook(shutdownThread);

		InputStream is = getClass().getResourceAsStream("/db.properties");
		Properties dbProps = new Properties();
		try
		{
			dbProps.load(is);
		}
		catch (Exception e)
		{
			System.err.println("DBConnectionManager can't read the properties file.  Make sure db.properties is in the CLASSPATH");
			System.out.println("DBConnectionManager can't read the properties file.  Make sure db.properties is in the CLASSPATH");
			return;
		}

		//System.out.println("DBConnectionManager Loaded db.properties");

		String logFile = dbProps.getProperty("logfile", "DBConnectionManager.log");

		if (Util.systemIsWindows())
		{
			String temp = dbProps.getProperty("logfile.windows");
			if (temp != null)
				logFile = temp;
		}
		else
		{
			String temp = dbProps.getProperty("logfile.unix");
			if (temp != null)
				logFile = temp;
		}

		try
		{
			log = new PrintWriter(new FileWriter(logFile, true), true);
		}
		catch (IOException e)
		{
			System.err.println("Can't open the log file: " + logFile);
			log = new PrintWriter(System.err);
		}

		log("-------------------------------------------------------------------[INIT]-");

		loadDrivers(dbProps);
		createPools(dbProps);
	}

	/**
	 * Loads and registers all JDBC drivers. This is done by the
	 * DBConnectionManager, as opposed to the DBConnectionPool,
	 * since many pools may share the same driver.
	 *
	 * @param props The connection pool properties
	 */
	private void loadDrivers(Properties props)
	{
		String driverClasses = props.getProperty("drivers");
		StringTokenizer st = new StringTokenizer(driverClasses, ",");
		while (st.hasMoreElements())
		{
			String driverClassName = st.nextToken().trim();
			try
			{
				Driver driver = (Driver) Class.forName(driverClassName).newInstance();
				DriverManager.registerDriver(driver);
				drivers.addElement(driver);
				log("Registered JDBC driver " + driverClassName);
			}
			catch (Exception e)
			{
				log("Can't register JDBC driver: " + driverClassName + ", Exception: " + e);
			}
		}
	}

	/**
	 * Writes a message to the log file.
	 */
	private void log(String msg)
	{
		log.println("[" + Util.getTimestamp() + "] " + msg);
	}

	/**
	 * Writes a message with an Exception to the log file.
	 */
	private void log(Throwable e, String msg)
	{
		log.println("[" + Util.getTimestamp() + "] " + msg);
		e.printStackTrace(log);
	}

	public synchronized String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("DBConnectionManager: " + pools.size() + " pool(s) in use, default is " + defaultName);
		sb.append(Util.LB);
		Enumeration e = pools.elements();
		int counter = 1;
		while (e.hasMoreElements())
		{
			sb.append("Pool " + counter++ + ": ");
			sb.append(e.nextElement());
			sb.append(Util.LB);
		}

		return sb.toString();
	}

	/**
	 * This inner class represents a connection pool. It creates new
	 * connections on demand, up to a max number if specified.
	 * It also makes sure a connection is still open before it is
	 * returned to a client.
	 */
	private class DBConnectionPool
	{
		private int			numConnections	= 0;
		private Vector		freeConnections	= new Vector();
		private Hashtable	usedConnections	= new Hashtable();
		private int			maxConn;
		private int			minConn;
		private String		name;
		private String		password;
		private String		URL;
		private String		user;
		private long		recycleTime;
		private int			defaultTimeout;

		/**
		 * Creates new connection pool.
		 *
		 * @param name The pool name
		 * @param URL The JDBC URL for the database
		 * @param user The database user, or null
		 * @param password The database user password, or null
		 * @param maxConn The maximum number of connections, or 0
		 * @param maxConn The minimum number of connections; will be opened immediatly
		 * @param recylceTime The maximum amount of time a connection should be kept open
		 *   for no limit
		 */
		protected DBConnectionPool(String name, String URL, String user, String password, int maxConn, int minConn, long recycleTime, int defaultTimeout)
		{
			this.name = name;
			this.URL = URL;
			this.user = user;
			this.password = password;
			this.maxConn = maxConn;
			this.minConn = minConn;
			this.recycleTime = recycleTime;
			this.defaultTimeout = defaultTimeout;

			log("Creating new pool: " + name);
			log("Pool " + name + ": User=" + user + ", Password=" + password + ", URL=" + URL);
			log("Pool " + name + ": Max Connections=" + maxConn + ", Min Connections=" + minConn + ", RecycleTime=" + recycleTime + ", DefaultTimeout=" + defaultTimeout);

			// if minConn > 0, initialize new connections now
			for (int i = 0; i < this.minConn; i++)
			{
				freeConnections.addElement(newConnection());
			}
		}

		/**
		 * Checks out a connection from the pool. If no free connection
		 * is available, a new connection is created unless the max
		 * number of connections has been reached. If a free connection
		 * has been closed by the database, it's removed from the pool
		 * and this method is called again recursively.
		 * <P>
		 * If no connection is available and the max number has been 
		 * reached, this method waits the specified time for one to be
		 * checked in.
		 *
		 */
		protected synchronized Connection getConnection()
		{
			return this.getConnection(defaultTimeout);
		}

		protected Enumeration getFreeConnections()
		{
			return freeConnections.elements();
		}

		/**
		 * Checks out a connection from the pool. If no free connection
		 * is available, a new connection is created unless the max
		 * number of connections has been reached. If a free connection
		 * has been closed by the database, it's removed from the pool
		 * and this method is called again recursively.
		 * <P>
		 * If no connection is available and the max number has been 
		 * reached, this method waits the specified time for one to be
		 * checked in.
		 *
		 * @param timeout The timeout value in milliseconds
		 */
		protected synchronized Connection getConnection(long timeout)
		{
			long startTime = System.currentTimeMillis();
			ConnectionWrapper wrapper = null;
			while ((wrapper = getWrapper()) == null)
			{
				if (timeout <= 0 || ((System.currentTimeMillis() - startTime) >= timeout))
					break;

				try
				{
					wait(timeout);
				}
				catch (InterruptedException e)
				{
				}
			}

			if (wrapper == null)
			{
				log("Failed to get a connection, connections exhausted.  Waited " + (System.currentTimeMillis() - startTime) + " ms. before giving up.");
				return null;
			}

			Connection con = wrapper.getConnection();
			wrapper.setState(true);
			usedConnections.put(con, wrapper);

			return con;
		}

		/**
		 * Checks out a connection from the pool. If no free connection
		 * is available, a new connection is created unless the max
		 * number of connections has been reached. If a free connection
		 * has been closed by the database, it's removed from the pool
		 * and this method is called again recursively.
		 */
		protected synchronized ConnectionWrapper getWrapper()
		{
			ConnectionWrapper wrapper = null;
			if (freeConnections.size() > 0)
			{
				// Pick the first Connection in the Vector
				// to get round-robin usage
				wrapper = (ConnectionWrapper) freeConnections.remove(0);
				if (wrapper == null)
				{
					log("Failed to get a free connection: remove(0) returned null!");
					return null;
				}

				try
				{
					if (wrapper.getConnection().isClosed())
					{
						log("Removed bad connection from " + name + ": " + wrapper);
						// Try again recursively
						wrapper = getWrapper();
					}
				}
				catch (SQLException e)
				{
					log("Removed bad connection from " + name + ": " + wrapper);
					// Try again recursively
					wrapper = getWrapper();
				}
			}
			else if (maxConn == 0 || usedConnections.size() < maxConn)
			{
				wrapper = newConnection();
			}

			return wrapper;
		}

		/**
		 * Checks in a connection to the pool. Notify other Threads that
		 * may be waiting for a connection.
		 *
		 * @param con The connection to check in
		 */
		protected synchronized void freeConnection(Connection con)
		{
			ConnectionWrapper wrapper = (ConnectionWrapper) usedConnections.remove(con);
			if (wrapper != null)
			{
				wrapper.setState(false);
				freeConnections.addElement(wrapper);
				checkRecycle(wrapper);
			}
			else
			{
				log("Wrapper does not exist for connection " + con);
			}

			notifyAll();
		}

		protected synchronized void recycle(Connection con)
		{
			ConnectionWrapper wrapper = (ConnectionWrapper) usedConnections.remove(con);
			if (wrapper != null)
			{
				wrapper.setState(false);
				recycle(wrapper);
			}
			else
			{
				log("Wrapper does not exist for connection " + con);
			}

			notifyAll();
		}

		private synchronized void recycle(ConnectionWrapper wrapper)
		{
			log("Recycling connection " + wrapper);
			freeConnections.removeElement(wrapper);
			try
			{
				wrapper.getConnection().close();
			}
			catch (SQLException e)
			{
				log(e, "Failed to close connection in pool " + name + ": " + wrapper);
			}

			wrapper = null;

			if (freeConnections.size() < minConn)
			{
				freeConnections.addElement(newConnection());
			}
		}

		private synchronized void checkRecycle(ConnectionWrapper wrapper)
		{
			if ((recycleTime > 0) && (wrapper.getConnectionAge() >= recycleTime) && !wrapper.isInUse())
			{
				recycle(wrapper);
			}
		}

		/**
		 * Closes all available connections.
		 */
		protected synchronized void release()
		{
			log("Closing all connections not in use...");
			Enumeration allConnections = freeConnections.elements();
			while (allConnections.hasMoreElements())
			{
				ConnectionWrapper wrapper = (ConnectionWrapper) allConnections.nextElement();
				try
				{
					wrapper.getConnection().close();
					log("Closed connection in pool " + name + ": " + wrapper);
				}
				catch (SQLException e)
				{
					log(e, "Failed to close connection in pool " + name + ": " + wrapper);
				}
			}
			freeConnections.removeAllElements();

			log("Closing all connections in use...");
			allConnections = usedConnections.elements();
			while (allConnections.hasMoreElements())
			{
				ConnectionWrapper wrapper = (ConnectionWrapper) allConnections.nextElement();
				try
				{
					wrapper.getConnection().close();
					log("Closed connection in pool " + name + ": " + wrapper);
				}
				catch (SQLException e)
				{
					log(e, "Failed to close connection in pool " + name + ": " + wrapper);
				}
			}
			usedConnections.clear();
		}

		/**
		 * Creates a new connection, using a userid and password if specified.
		 */
		protected ConnectionWrapper newConnection()
		{
			Connection con = null;
			try
			{
				if (user == null)
				{
					con = DriverManager.getConnection(URL);
				}
				else
				{
					con = DriverManager.getConnection(URL, user, password);
				}
			}
			catch (SQLException e)
			{
				log(e, "Failed to create a new connection for " + URL);
				return null;
			}

			ConnectionWrapper wrapper = new ConnectionWrapper(con, numConnections++);
			log("Opened new connection in pool " + name + ": " + wrapper);

			return wrapper;
		}

		public synchronized String toString()
		{
			StringBuffer sb = new StringBuffer();
			sb.append("DBConnectionPool[" + name + "] using " + URL);
			sb.append(Util.LB);
			sb.append("Free connections: ");
			sb.append(Util.LB);
			if (freeConnections.size() > 0)
			{
				Enumeration e = freeConnections.elements();
				while (e.hasMoreElements())
				{
					sb.append("\t" + e.nextElement());
					sb.append(Util.LB);
				}
			}
			else
			{
				sb.append("\tNo free connections.");
				sb.append(Util.LB);
			}
			sb.append("Used connections: ");
			sb.append(Util.LB);
			if (usedConnections.size() > 0)
			{
				Enumeration e = usedConnections.elements();
				while (e.hasMoreElements())
				{
					sb.append("\t" + e.nextElement());
					sb.append(Util.LB);
				}
			}
			else
			{
				sb.append("\tNo used connections");
				sb.append(Util.LB);
			}

			return sb.toString();
		}
	}

	private class RecycleThread extends Thread
	{
		protected RecycleThread()
		{
			this.setDaemon(true);
			this.start();
		}

		public void run()
		{
			while (true)
			{
				if (pools != null)
				{
					Enumeration poolsEnum = pools.elements();
					while (poolsEnum.hasMoreElements())
					{
						DBConnectionPool pool = (DBConnectionPool) poolsEnum.nextElement();
						Enumeration conEnum = pool.getFreeConnections();
						while (conEnum.hasMoreElements())
						{
							ConnectionWrapper wrapper = (ConnectionWrapper) conEnum.nextElement();
							pool.checkRecycle(wrapper);
						}
					}
				}

				try
				{
					Thread.sleep(60000);
				}
				catch (Exception e)
				{
				}
			}
		}
	}

	private class ConnectionWrapper
	{
		private long		createTime	= -1;
		private long		usedTime	= -1;
		private Connection	con			= null;
		private int			id			= -1;
		private long		totalUsage	= -1;

		protected ConnectionWrapper(Connection con, int id)
		{
			this.con = con;
			this.id = id;
			createTime = System.currentTimeMillis();
		}

		protected Connection getConnection()
		{
			return con;
		}

		protected void setState(boolean inUse)
		{
			if (inUse)
				usedTime = System.currentTimeMillis();
			else
			{
				long et = (System.currentTimeMillis() - usedTime);
				totalUsage += et;
				usedTime = -1;
			}
		}

		protected boolean isInUse()
		{
			return (usedTime > 0);
		}

		protected long getConnectionAge()
		{
			return (System.currentTimeMillis() - createTime);
		}

		protected long getUseET()
		{
			if (usedTime <= 0)
				return 0;
			else
				return (System.currentTimeMillis() - usedTime);
		}

		protected long getTotalUsage()
		{
			return totalUsage;
		}

		protected double getPercentUsage()
		{
			if ((getConnectionAge() <= 0) || (getTotalUsage() <= 0))
				return 0;
			else
				return ((getTotalUsage() / getConnectionAge()) * 100);
		}

		protected int getID()
		{
			return id;
		}

		public String toString()
		{
			return "ConnectionWrapper[" + id + "] " + getConnectionAge() + " ms. old, " + getUseET() + " ms. in use, " + getPercentUsage() + "% used";
		}
	}

	private class ShutdownThread extends Thread
	{
		public void run()
		{
			if (instance != null && !closing && releaseOnShutdown)
			{
				log("Forcing release of all connections on shutdown...");
				instance.release(true);
			}
		}
	}
}
