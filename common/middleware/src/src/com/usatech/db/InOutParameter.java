package com.usatech.db;

public class InOutParameter extends OutParameter
{
	protected Object	inValue;

	public InOutParameter(int outSqlType, Object inValue)
	{
		super(outSqlType);
	}

	public Object getInValue()
	{
		return inValue;
	}
}
