package com.usatech.net;

import java.io.*;
import java.net.*;
public class Connection
{
    private Socket socket;
    private InputStream in;
    private OutputStream out;

    /**
     * Create a Connection from a Socket.
     *
     * @param socket a socket
     */
    public Connection(Socket socket) throws IOException
    {
		this.socket = socket;
		this.in = socket.getInputStream();
		this.out = socket.getOutputStream();
    }	    /**
     * Create a Connection from a hostname and port.
     *
     * @param host remote hostname
     * @param port remote port
     */
    public Connection(String host, int port) throws IOException
    {
		this(new Socket(InetAddress.getByName(host), port));
    }	    public Connection(InetAddress host, int port) throws IOException
    {
		this(new Socket(host, port));
    }

    public Socket getSocket() { return socket; }	public InputStream getInputStream() { return in; }
    public OutputStream getOutputStream() { return out; }    public InetAddress getInetAddress() { return socket.getInetAddress(); }
	public int getPort() { return socket.getPort(); }    		public void setInputStream(InputStream in)	{		this.in = in;	}		public void setOutputStream(OutputStream out)	{		this.out = out;	}

    /**
     * Close the connection.
     */
    public boolean close()
    {		if (socket != null)
		{
		    try
		    {
				socket.close();		    }
		    catch (IOException e)
		    {
				return false;
		    }
		}				return true;
    }

    public String toString()
    {
		return getInetAddress().getHostAddress() + ":" + getPort();
    }

    public void setTimeout(int timeout) throws SocketException
    {
		socket.setSoTimeout(timeout);
    }
}
