package com.usatech.net;

import java.util.*;
import java.io.*;
import java.net.*;
import com.usatech.util.Util;

public abstract class TCPServer extends Thread
{
	public static final int BUFFER_LENGTH = 65507;
	
	protected ServerSocket serverSocket;
	protected int port;
	protected boolean listening;
	protected boolean stopFlag = false;
	protected Exception exception;
	
	public TCPServer(int port) throws IOException
	{
		this.port = port;

		serverSocket = new ServerSocket(port);
		start();
	}
	
	/**
	 * Method to be implemented by sub-classes.  Should read the datagram packet and respond to the socket.
	 */
	protected abstract void handleConnection(Socket socket) throws Exception;
	
	public boolean listening() { return listening; }
	
	public int getPort() { return port; }
	public ServerSocket getServerSocket() { return serverSocket; }
	public void exit() { this.stopFlag = true; }
	public Exception getException() { return exception; }
	
	public void run()
	{
		while(!stopFlag)
		{
			Socket socket = null;
			
			try
			{
				socket = serverSocket.accept();
			}
			catch(Exception e)
			{
				exception = e;
				break;
			}
			
			try
			{
				handleConnection(socket);
			}
			catch(Exception e)
			{
				exception = e;
			}
		}
	}
}
