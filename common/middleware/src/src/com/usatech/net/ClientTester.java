package com.usatech.net;

import com.usatech.util.*;
import java.io.*;
import java.net.*;

public class ClientTester extends TCPServer
{
	public static void main(String args[]) throws Exception
	{
		new ClientTester(Util.parseInt(args[0], 80));
	}
	
	public ClientTester(int port) throws Exception
	{
		super(port);
	}
	
	protected void handleConnection(Socket socket) throws Exception
	{
		InputStream is = socket.getInputStream();
		
		byte b = -1;
		
		while(true)
		{
			b = (byte) is.read();
			
			if(b < 0)
				break;
			
			System.out.print("[" + Byte.toString(b) + "]");
		}
		
		is.close();
		socket.close();			
	}
}
