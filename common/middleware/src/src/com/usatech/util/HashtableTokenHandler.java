package com.usatech.util;

import java.util.*;

public class HashtableTokenHandler implements TokenHandler
{
	protected Hashtable tokens = null;
	
	/**
	 * NOTE: Any keys used in the specified Hashtable should be UPPER CASE.  Mixed
	 * or lower case keys will not be matched.
	 */
	public HashtableTokenHandler(Hashtable tokens)
	{
		this.tokens = tokens;
		if (this.tokens == null) tokens = new Hashtable();
	}
	
	public String getValue(String tokenName)
	{
		if (tokenName == null) return "";
		tokenName = tokenName.toUpperCase();
		Object obj = tokens.get(tokenName);
		if (obj == null) return "";
		//if ((obj instanceof String)) return obj.toString();
		//return (String)obj;
		return obj.toString();
	}
}
