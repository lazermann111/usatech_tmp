package com.usatech.util;

import java.io.File;
import java.util.StringTokenizer;

/**
 * Transforms a filename to adhere to the conventions of the host
 * platform, using the following algorithm:
 * <ol>
 * <li>First, forward slashes are converted to underscores.
 * <li>Depending on the contents of the os.name system property,
 *     one of the following is performed:
 *     <ul>
 *     <li>On MacOS Classic, the filename is mid-truncated when it
 *         it larger than 31 characters.
 *     <li>On Windows, the following special characters are
 *         replaced by an underscore: " \ : * | < > ; , ?
 *     </ul>
 * <li>The resulting filename is returned to the caller.
 * </ol>
 */
public class FilenameUtils {

    /**
     * From a given file F.EXT returns a file Fn.EXT
     * where n is a zero or more digit number (prefixed by
     * an underscore if more than zero digits) that is 
     * guaranteed not to exist. This method tries twice
     * to create files with n up to 100000, then fails
     * with an exception.
     * @return non existing file.
     * @throws RuntimeException if a non existing file could
     * not be determined.
     */
    public static File getNextUniqueFile(File f) {

        if(!f.exists())
            return f;
        
        String n = f.getName();            
        String pre = f.getName();
        String post = "";
            
        if(n.indexOf('.') != -1) {
            
            pre = n.substring(0, n.indexOf('.'));
            post = n.substring(n.indexOf('.'));
            
        }
        
        pre += "_";

        /* Arbitrary limit of 100 thousand, after that try again,
           after that just throw an exception and fail. Not sure what
           else to do. */
        
        for(int tries = 0; tries < 2; tries++) {
            
            for(int i = 1; i < 100000; i++) {
                
                f = new File(f.getParent(), pre + i + post);
                
                if(!f.exists())
                    return f;
                
            }

        }

        throw new RuntimeException("could not get unique file for " + f);
        
    }

    public static String qualify(String temp) {
        
        /* Convert all slashes to underscores. */

        String filename = TextUtils.findAndReplace(temp, "/", "_");

        if(System.getProperty("os.name").startsWith("Mac") &&
           !System.getProperty("os.name").equals("Mac OS X")) {

            /* If we're on MacOS Classic, make sure the filename
               never exceeds 31 characters. */

            if(filename.length() > 31)
                filename = filename.substring(0, 14) + "..." + 
                    filename.substring(filename.length() - 14, 
                                       filename.length());

            /* The : is dir. separator on Mac. */

            filename = TextUtils.findAndReplace(filename, ":", "_");
            
        } else if(System.getProperty("os.name").startsWith("Windows")) {

            /* On Windows, a whole bunch of illegal characters need to
               be filtered. */
            
            char[] c = filename.toCharArray();
            char[] c2 = new char[c.length];
            char t;

            for(int i = 0; i < c.length; i++) {

                t = c[i];

                switch(c[i]) {
                    
                case '\\':
                case '\"':
                case ':':
                case '*':
                case '|':
                case '<':
                case '>':
                case ';':
                case ',':
                case '?':
                    t = '_';
                default:                    
                    if(Character.isISOControl(c[i])) 
                        t = '_';
                    
                    c2[i] = t;
                    break;

                }

            }

            filename = new String(c2);

        }

        return filename;
	
    }

}

