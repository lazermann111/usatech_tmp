package com.usatech.util;

import java.util.logging.Logger;

public class LoggerFactory
{
	public static Logger make() 
	{
		return Logger.getLogger((new Throwable()).getStackTrace()[1].getClassName());
	}
}
