package com.usatech.util;

import java.io.Serializable;
import java.util.*;

public class CachableString implements Cachable, Serializable, Comparable
{
	protected String str;
	protected boolean print;
	
	public CachableString(String str)
	{
		this(str, false);
	}
	
	public CachableString(String str, boolean printOnPurge)
	{
		this.str = str;
		this.print = printOnPurge;
	}
	
	public void onPurge()
	{
		if(print)
		{
			System.out.println("[CachableString] Purging " + this.toString());
		}
	}
	
	public String toString()
	{
		return str.toString();
	}
	
	public int compareTo(Object o)
	{
		if(o instanceof String)
			return str.compareTo((String)o);
		return 0;
	}
	
	public boolean equals(Object o)
	{
		return str.equals(o);
	}
	
	public String getString()
	{
		return str;
	}
}
