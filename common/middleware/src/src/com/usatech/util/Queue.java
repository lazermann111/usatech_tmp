package com.usatech.util;

import java.util.Vector;
import java.util.Enumeration;

public class Queue // thread safe
{
    protected Vector queue; // Vector is synchronized - thead safe

    public Queue()
    {
		this(10);
    }

    public Queue(int intialCapacity)
    {
		queue = new Vector(intialCapacity);
    }

    public void addBack(Object item)
	{
		//queue.add(item);
		queue.addElement(item);
	}

    public Object getFront()
	{
		/*
		synchronized(queue)
		{
			if(queue.isEmpty())
				return null;
			else
				return queue.remove(0);
		}
		*/
		
		try
		{
			return queue.remove(0);
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			return null;
		}
		
		//Object item = size() == 0 ? null : queue.elementAt(0);
		//if (item != null) queue.removeElementAt(0);
		//return item;
	}

    public Object peekFront()
	{
		synchronized(queue)
		{
			return queue.isEmpty() ? null : queue.elementAt(0);
		}
	}

    public Object peekBack()
	{
		synchronized(queue)
		{
			return queue.isEmpty() ? null : queue.lastElement();
		}
	}
	
	public void replaceBack(Object item)
	{
		synchronized(queue)
		{
			if(isEmpty()) 
				addBack(item);
			else
				queue.setElementAt(item, (queue.size()-1));
		}
	}
	
	public boolean isEmpty()
	{
		return queue.isEmpty();
	}
	
    public void clear()
    {
        queue.clear();
    }

    public int size()
    {
        return queue.size();
    }

    public boolean contains(Object key)
    {
    	return queue.contains(key);
    }

    public Object elementAt(int index)
    {
    	return queue.elementAt(index);
    }

    public int indexOf(Object key)
    {
    	return queue.indexOf(key);
    }

    public void insertElementAt(Object obj, int index)
    {
    	queue.insertElementAt(obj, index);
    }

    public void removeElementAt(int index)
    {
        queue.removeElementAt(index);
    }

    public void remove(Object o)
    {
        queue.remove(o);
    }
	
	public Enumeration elements()
	{
		return queue.elements();
	}
}

