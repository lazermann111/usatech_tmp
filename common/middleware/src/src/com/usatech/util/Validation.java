package com.usatech.util;

import java.net.*;
import java.util.*;

public class Validation
{
	public static final int INFINITY = Integer.MAX_VALUE;
	
	public static boolean validateEmailAddress(String email)
	{
		if(!validateString(email, 6, INFINITY, false)) // min size is a@b.cd
			return false; 
		
		if (containsIllegalCharacters(email, " \"'[]{};:,<>/\\?`~!#$%^&*()+="))
			return false;	// contains illegal characters
		
		if (email.indexOf('@') <= 0) 
			return false;	// must have something before the @ // must have at least one @
		
		if (email.lastIndexOf('@') > email.indexOf('@')) 
			return false;	// too many @s
		
		if (email.lastIndexOf('.') <= email.indexOf('@')) 
			return false;	// must be at least one . after the @
		
		if (email.indexOf('.', email.indexOf('@')) == email.indexOf('@') + 1) 
			return false;	// must be something between the @ and .
		
		if (email.lastIndexOf('.') > email.length() - 3) 
			return false;	//
		
		return true;
	}
	
	public static boolean validateZIPCode(String zipCode)
	{
		if(!validateString(zipCode, 5, 10, false))
			return false;
		
		if (zipCode.indexOf('-') != -1)
		{
			if (zipCode.indexOf('-') != zipCode.lastIndexOf('-')) 
				return false;	// only one '-' allowed
			
			if(!validateString(zipCode, 10, 10, false)) // make sure it's 10 chars, including '-'
			   return false;

			String zip = zipCode.substring(0, zipCode.indexOf('-'));
			
			if(!validateNumber(zip, 5)) // make sure first part is 5 digits
				return false;
			
			String zipPlus4 = zipCode.substring(zipCode.indexOf('-') + 1, zipCode.length());
			if(!validateNumber(zipPlus4, 4))
				return false;
		}
		else
		{
			if(!validateNumber(zipCode, 5))
				return false;
		}
		
		return true;
	}
	
	public static boolean validateNumber(String number, int min, int max)
	{
		if(!validateNumber(number))
			return false;
		
		int n = Util.parseInt(number, -1);
		if(n < min || n > max)
			return false;
		
		return true;
	}	
	
	public static boolean validateNumber(String number, int numDigits)
	{
		if(!validateString(number, numDigits, numDigits, false))
			return false;
		return validateNumber(number);
	}
	
	public static boolean validateNumber(String number)
	{
		try 
		{ 
			Long.parseLong(number); 
		}
		catch (NumberFormatException e) 
		{ 
			return false;
		}
		
		return true;
	}
	
	public static boolean validateURL(String url)
	{
		if(!validateString(url, 11, INFINITY, false))	// min size is http://a.bc
			return false;
		
		if (!url.startsWith("http://")) 
			return false;
		
		try 
		{ 
			URL u = new URL(url); 
		}
		catch (MalformedURLException e) 
		{ 
			return false;
		}
		
		if (url.indexOf('.') == 0) 
			return false;
		
		if (url.indexOf('.') < url.indexOf('/')) 
			return false;
		
		return true;
	}
	
	public static boolean validateString(String str, int minLength, int maxLength, boolean nullAllowed)
	{
		if (!nullAllowed && str == null) 
			return false;
		
		if (str.length() < minLength) 
			return false;
		
		if (str.length() > maxLength) 
			return false;
		
		return true;
	}
	
	public static boolean containsIllegalCharacters(String str, String illegalChars)
	{
		for (int i = 0; i < illegalChars.length(); i++)
		{
			if (str.indexOf(illegalChars.charAt(i)) != -1) return true;
		}
		return false;
	}
}
