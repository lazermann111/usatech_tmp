package com.usatech.util;

import java.util.*;
import java.io.*;

public class HexConverter
{
	public static void main(String args[]) throws Exception
	{
		if(args.length != 3)
		{
			System.out.println("Usage: com.usatech.util.HexConverter <h|b> <infile> <outfile>");
			System.out.println("Error: Wrong number of args");
			System.exit(1);
		}
		
		char operation = args[0].toLowerCase().charAt(0);
		if(operation != 'h' && operation != 'b')
		{
			System.out.println("Usage: com.usatech.util.HexConverter <h|b> <infile> <outfile>");
			System.out.println("Error: operation must be h or b for hex or binary");
			System.exit(1);
		}
		
		File inFile = new File(args[1]);
		if(!inFile.exists() || !inFile.canRead())
		{
			System.out.println("Usage: com.usatech.util.HexConverter <h|b> <infile> <outfile>");
			System.out.println("Error: infile can not be read or does not exist");
			System.exit(1);
		}

		File outFile = new File(args[2]);
		/*
		if(!outFile.canWrite())
		{
			System.out.println("Usage: com.usatech.util.HexConverter <h|b> <infile> <outfile>");
			System.out.println("Error: outfile can not be written");
			System.exit(1);
		}
		*/
		
		switch(operation)
		{
			case 'h':
			{
				byte[] inData = Util.readContentsToBytes(inFile);
				System.out.println("Read " + inData.length + " bytes from " + inFile);
				String outData = Conversions.bytesToHex(inData);
				Util.writeContentsFromString(outData, outFile);
				System.out.println("Wrote " + outData.length() + " characters to " + outFile);
				break;
			}
			case 'b':
			{
				String inData = Util.readContentsToString(inFile);
				System.out.println("Read " + inData.length() + " characters from " + inFile);
				byte[] outData = Conversions.hexToByteArray(inData);
				Util.writeContentsFromBytes(outFile, outData, true);
				System.out.println("Wrote " + outData.length + " bytes to " + outFile);
				break;
			}
			default:
			{
				System.out.println("Usage: com.usatech.util.HexConverter <h|b> <infile> <outfile>");
				System.out.println("Error: default switch?");
				System.exit(1);
			}
		}
	}
}
