package com.usatech.util;

/**
 * Converts primitive types from and to byte arrays.
 */
public class SignUtils {

    public static int toUnsigned(byte b) 
    {
		return (int) b & 0xFF;
    }
    
	public static String byteToHex(byte b)
	{
		int i = b & 0xFF;
		return Integer.toHexString(i);
	}
	
	public static int[] toUnsigned(byte[] b)
	{
		int[] temp = new int[b.length];
		for(int i=0; i<temp.length; i++)
		{
			temp[i] = toUnsigned(b[i]);
		}
		return temp;
	}
	
	/**
	 * Reads an unsigned short (2 byte bigendian integer) from a
	 * byte array starting at index pos.
	 * @param arr the byte array
	 * @param pos the index into the byte array
	 * @return the unsigned short (expanded to an int to avoid sign problems)
	 */
	public static int readUnsignedShort(byte[] arr, int pos) 
	{
		return ((arr[pos] & 0xff) << 8) + (arr[pos + 1] & 0xff);
	}

	/**
	 * Writes a signed short (2 byte bigendian integer) to a
	 * byte array starting at index pos.
	 * @param s the signed short
	 * @param arr the byte array
	 * @param pos the index into the byte array
	 */
	public static void writeShort(short s, byte[] arr, int pos) 
	{
		arr[pos] = (byte) (s >>> 8);
		arr[pos + 1] = (byte) (s & 0xff);
	}
}








