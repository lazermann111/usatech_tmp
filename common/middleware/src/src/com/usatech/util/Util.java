package com.usatech.util;

import java.util.*;
import java.lang.reflect.*;
import java.text.*;
import java.io.*;
import java.net.*;

public class Util
{
    public static final String LB = System.getProperty("line.separator");
    
    public static final String WINDOWS_LB = "\r\n";
    public static final String UNIX_LB = "\n";
    
    public static final DecimalFormat INT_FORMATTER = new DecimalFormat("###,###,###,###");
    public static final DecimalFormat DECIMAL_FORMATTER = new DecimalFormat("###,###,###,##0.##");
    public static final DecimalFormat CURRENCY_FORMATTER = new DecimalFormat("$###,###,###,##0.00");
    public static final DecimalFormat PERCENT_FORMATTER = new DecimalFormat("###,###,###,##0.##%");
    public static final SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat("MM.dd.yyyy-HH.mm.ss");  
    public static final SimpleDateFormat LONG_TIME_FORMATTER = new SimpleDateFormat("EEEEEEE, MMM d, yyyy 'at' hh:mm:ss a z");
    public static final SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
	public static final SimpleDateFormat SHORT_DATE_FORMATTER = new SimpleDateFormat("MM/dd/yyyy");
    
    public static Object constructNew(String className, Object arg) throws InstantiationException 
    {
        try
        {
            if(arg == null)
                return Class.forName(className).newInstance();
            
            Class c = Class.forName(className);
            Class[] contructorArgs = {arg.getClass()};
            Constructor constructor = c.getConstructor(contructorArgs);
            Object[] args = {arg};
            return constructor.newInstance(args);
        }
        catch(Exception e)
        {
            throw new InstantiationException("Problem constructing new " + className + ". Exception is " + e);
        }
    }

    public static String getStringOrExit(Properties p, String property)
    {
        String s = p.getProperty(property);
        if(s == null)
        {
            System.out.println("Required property not found: " + property);
            System.exit(1);
        }
        
        //System.out.println(property+"="+s);
        
        return s;
    }
    
    public static int getIntOrExit(Properties p, String property)
    {
        String s = p.getProperty(property);
        if(s == null)
        {
            System.out.println("Required property not found: " + property);
            System.exit(1);
        }
        
        //System.out.println(property+"="+s);
        
        try
        {
            return Integer.parseInt(s);
        }
        catch(NumberFormatException e)
        {
            System.out.println("Invalid property format: " + property);
            System.exit(1);
        }
        
        return -1;
    }
    
    public static double getDoubleOrExit(Properties p, String property)
    {
        String s = p.getProperty(property);
        if(s == null)
        {
            System.out.println("Required property not found: " + property);
            System.exit(1);
        }
        
        //System.out.println(property+"="+s);
        
        try
        {
            return Double.parseDouble(s);
        }
        catch(NumberFormatException e)
        {
            System.out.println("Invalid property format: " + property);
            System.exit(1);
        }
        
        return -1;
    }

    public static boolean getBooleanOrExit(Properties p, String property)
    {
        String s = p.getProperty(property);
        if(s == null)
        {
            System.out.println("Required property not found: " + property);
            System.exit(1);
        }
        
        //System.out.println(property+"="+s);
        
        try
        {
            return Boolean.valueOf(s).booleanValue();
        }
        catch(NumberFormatException e)
        {
            System.out.println("Invalid property format: " + property);
            System.exit(1);
        }
        
        return false;
    }
    
    public static String getStackTraceAsString(Throwable e)
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(bytes, true);
        e.printStackTrace(writer);
        String str = bytes.toString();

        try
        {
            writer.close();
            bytes.close();
        }
        catch(IOException ex) { }

        return str;
    }
    
    /**
     * Prints the name of the method that called this method to stdout
     * 
     * @return String the name of the method that called this method.
     */
    public static void printCurrentMethodName()
    {
        //System.out.println(getCurrentMethodName(3));
		Util.output(getCurrentMethodName(3));
    }
    
    /**
     * Gets the name of the method that called this method to stdout
     * 
     * @return String the name of the method that called this method.
     */
    public static String getCurrentMethodName()
    {
        return getCurrentMethodName(3);
    }
    
    /**
     * Prints the name of the method that called this method to stdout
     * 
     * @param numLinesToIgnore number of lines in the stack trace to ignore
     * @return String the name of the method that called this method.
     */
    public static String getCurrentMethodName(int numLinesToIgnore)
    {
        // Exceptions are an easy way to get call stack information.
        // This method takes advantage of that.
        Throwable t = new Throwable();
        
        // Print the stack trace to a byte array
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        t.printStackTrace(new PrintWriter(baos, true));
        byte[] stackTraceBytes = baos.toByteArray();
        
        String line = null;
        
        // Now read from that byte array, line by line
        try
        {
            ByteArrayInputStream bais = new ByteArrayInputStream(stackTraceBytes);
            BufferedReader in = new BufferedReader(new InputStreamReader(bais));
            
            // We need to skip the first couple of lines of the stack trace, because we only
            // want the line that pertains to our calling method.
            in.readLine();  // always ignore the first lines
            for (int i = 0; i < numLinesToIgnore - 1; i++)
            {
                in.readLine();  // clear lines that we don't need
            }
            
            line = in.readLine();
            in.close();
        }
        catch (IOException e)
        {
            // nothing to do
        }
        
        // The stack trace line begins with "at ", so trim it off to give us a clean method name
        String methodName = line.substring(4, line.length());
        
        return methodName;
    }

    public static Properties loadProperties(String filename)
    {
        try
        {
            File file = new File(filename);
            return loadProperties(file);
        }
        catch (Exception e)
        {
            // log some kind of error here
            return null;
        }
    }

    public static Properties loadProperties(File file)
    {
        Properties p = null;
        try
        {
            FileInputStream in = new FileInputStream(file);
            p = new Properties();
            p.load(in);
            in.close();
        }
        catch (Throwable e)
        {
            // log the error
        }
        return p;
    }
    
    /**
     * Efficiently copies everything from an InputStream to an OutputStream
     * @param in The InputStream to read from.
     * @param out The OutputStream to write to.
     * @param bufferSize The size to use for each stream read.
     * @return Elapsed time in milliseconds it took to copy the stream.
     */
    public static long copyStream(InputStream in, OutputStream out, int bufferSize) throws IOException 
    {
        synchronized(in)
        {
            synchronized(out)
            {
                long start = System.currentTimeMillis();
                byte[] buffer = new byte[bufferSize];
                while(true)
                {
                    int bytesRead = in.read(buffer);
                    if(bytesRead == -1)
                        break;
                    out.write(buffer, 0, bytesRead);
                }
                out.flush();
                long end = System.currentTimeMillis();
                return (end - start);
            }
        }
    }
    
    /**
     * Efficiently copies everything from an InputStream to an OutputStream
     * @param in The InputStream to read from.
     * @param out The OutputStream to write to.
     * @return Elapsed time in milliseconds it took to copy the stream.
     */
    public static long copyStream(InputStream in, OutputStream out) throws IOException 
    {
        return copyStream(in, out, 1024);
    }
    
    /**
     * Efficiently copies everything from a Reader to an Writer
     * @param in The Reader to read from.
     * @param out The Writer to write to.
     * @param bufferSize The size to use for each stream read.
     * @return Elapsed time in milliseconds it took to copy the stream.
     */
    public static long copyStream(Reader in, Writer out, int bufferSize) throws IOException 
    {
        synchronized(in)
        {
            synchronized(out)
            {
                long start = System.currentTimeMillis();
                char[] buffer = new char[bufferSize];
                while(true)
                {
                    int bytesRead = in.read(buffer);
                    if(bytesRead == -1)
                        break;
                    out.write(buffer, 0, bytesRead);
                }
                out.flush();
                long end = System.currentTimeMillis();
                return (end - start);
            }
        }
    }
    
    /**
     * Efficiently copies everything from a Reader to an Writer
     * @param in The Reader to read from.
     * @param out The Writer to write to.
     * @return Elapsed time in milliseconds it took to copy the stream.
     */
    public static long copyStream(Reader in, Writer out) throws IOException 
    {
        return copyStream(in, out, 1024);
    }    
    
    /**
     * Reads all input from an InputStream and returns it as a byte array
     * @param in The InputStream to read from
     * @return A byte array of the contents of the stream.
     */
    public static byte[] getStreamAsBytes(InputStream in) throws IOException 
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        copyStream(in, out);
        byte[] bytes = out.toByteArray();
        out.close();
        return bytes;
    }
    
    public static String replaceSubstring(String orig, String searchFor, String replaceWith)
    {
        int index = orig.indexOf(searchFor);
        if(index < 0)
            return orig;
            
        StringBuffer sb = new StringBuffer();
        sb.append(orig.substring(0, index));
        sb.append(replaceWith);
        sb.append(orig.substring((index+searchFor.length())));
        return sb.toString();
    }

    public static String replaceSubstringAll(String orig, String searchFor, String replaceWith)
    {
        int index = 0;
        StringBuffer sb = new StringBuffer(orig);
        while((index = sb.toString().indexOf(searchFor, index)) != -1)
        {
            sb.replace(index, index+searchFor.length(), replaceWith);
            index += replaceWith.length();
        }
        return sb.toString();
    }

    /// Parse an integer, returning a default value on errors.
    public static int parseInt(String str, int def)
    {
        try
        {
            return Integer.parseInt(str);
        }
        catch (Exception e)
        {
            return def;
        }
    }

    /// Parse a long, returning a default value on errors.
    public static long parseLong(String str, long def)
    {
        try
        {
            return Long.parseLong(str);
        }
        catch ( Exception e )
        {
            return def;
        }
    }
    
    public static int getObjectSize(Serializable obj)
    {
        ObjectOutputStream oos = null;
        
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
        
            oos.writeObject(obj);
            oos.flush();
            
            return baos.size();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return -1;
        }
        finally
        {
            try { oos.close(); } catch(Exception e) {}
        }
    }
    
    public static int getRandomInt(int min, int max)
    {
        return (int)Math.round(Math.floor(((max + 1 - min) * Math.random()) + min));
    }

    public static long getRandomLong(long min, long max)
    {
        return Math.round(Math.floor(((max + 1 - min) * Math.random()) + min));
    }

    public static Object[] shuffle(Object[] array)
    {
        if(array == null)
            return null;
        
        Vector v = new Vector(array.length);
        for(int i=0; i<array.length; i++)
            v.addElement(array[i]);
        
        Collections.shuffle(v);
        
        Object[] shuffled = new Object[array.length];
        v.copyInto(shuffled);
        return shuffled;
    }   
    
    public static String getTimestamp()
    {
        //SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        //return format.format(new Date());
		return DATE_TIME_FORMATTER.format(new Date());
    }
    
    public static String getLocalHost()
    {
        try
        {
            InetAddress a = InetAddress.getLocalHost();
            String s = a.getHostName();
            if(s != null)
                return s;
            else
            {
                return a.getHostAddress();
            }
        }
        catch(Exception e)
        {
            return "localhost";
        }
    }
    
    public static String getFormattedPercentage(double part, double whole)
    {
        return PERCENT_FORMATTER.format(getPercentage(part, whole));
    }
    
    public static double getPercentage(double part, double whole)
    {
        return ((part/whole)*100);
    }
    
    public static String getFormattedAverage(double total, double count)
    {
        return DECIMAL_FORMATTER.format(getAverage(total, count));
    }
    
    public static double getAverage(double total, double count)
    {
        return (total/count);
    }

    public static String mergeTemplate(Hashtable tokens, String template, String beginDelim, String endDelim)
    {
        return mergeTemplate(new HashtableTokenHandler(tokens), template, beginDelim, endDelim);
    }
    
    public static String mergeTemplate(TokenHandler tokenHandler, String template, String beginDelim, String endDelim)
    {
        if (tokenHandler == null || template == null || beginDelim == null || endDelim == null) return null;
        
        StringBuffer sb = new StringBuffer();
        int index = -1;
        
        String key = null;
        String value = null;
        
        while((index = template.indexOf(beginDelim)) > -1)
        {
            // Append everything from the beginning of the input string up to the delimiter onto the StringBuffer
            sb.append(template.substring(0, index));
            
            // Chop off everything in the input string up to and including the first delimiter
            template = template.substring(index + beginDelim.length(), template.length());
            
            // Find the ending delimiter
            if ((index = template.indexOf(endDelim)) > -1)
            {
                // Get everything from the start of the input string up to the ending delimiter (the token name)
                key = template.substring(0, index);
                
                // Trim leading and trailing white space, so that both "[mm: TOKEN /]" and "[mm:TOKEN/]" are legal
                key = key.trim();
                
                // All keys in the hashtable are in upper case, so make sure the token we just found is also upper case
                //key = key.toUpperCase();  <- some tokens need to be case-sensitive - nix the toUpperCase()
                
                // Get the value from the TokenHandler.  If it returns null, don't append anything
                //value = tokens.containsKey(key) ? (String)tokens.get(key) : "";
                value = tokenHandler.getValue(key);
                if (value == null) value = "";
                sb.append(value);
                
                // Chop the ending delimiter from the input string
                template = template.substring(index + endDelim.length(), template.length());
            }
        }
        // If there's anything left in the input string (but no more tokens), append it to the StringBuffer
        if (template.length() > 0) sb.append(template);
        return sb.toString();
    }
    
    public static void writeContentsFromString(String contents, File f) throws IOException
    {
        FileOutputStream fos = new FileOutputStream(f);
        PrintWriter out = new PrintWriter(fos);

        out.print(contents);
        out.flush();

        out.close();
        fos.close();
    }

    public static String readContentsToString(File f) throws IOException, FileNotFoundException
    {
        FileInputStream fis = new FileInputStream(f);
        BufferedInputStream bis = new BufferedInputStream(fis);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        byte[] buffer = new byte[bis.available()];
        
        bis.read(buffer);
        bos.write(buffer);
        
        String contents = bos.toString();
        
        bos.close();
        
        bis.close();
        fis.close();

        return contents;
    }
    
    public static byte[] readContentsToBytes(File f) throws FileNotFoundException, IOException
    {
        FileInputStream fis = new FileInputStream(f);
        BufferedInputStream bis = new BufferedInputStream(fis);
        
        byte[] buffer = new byte[bis.available()];
        bis.read(buffer);

        bis.close();
        fis.close();
        
        return buffer;
    }
    
    public static void writeContentsFromBytes(File f, byte[] contents, boolean overwrite) throws IOException
    {
        if(!f.exists() || (f.exists() && overwrite))
        {
            FileOutputStream fos = new FileOutputStream(f);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            
            bos.write(contents);

            bos.close();
            fos.close();
        }
    }
    
    public static String execute(String command) throws IOException
    {
        Process process = Runtime.getRuntime().exec(command);
        InputStream is = process.getInputStream();
        String s = new String(getStreamAsBytes(is));
        is.close();
        return s;
    }
    
    public static boolean arrayContains(int[] array, int searchFor)
    {
        for(int i=0; i<array.length; i++)
        {
            if(array[i] == searchFor)
                return true;
        }
        
        return false;
    }
    
    public static boolean arrayContains(Object[] array, Object searchFor)
    {
        for(int i=0; i<array.length; i++)
        {
            if(array[i].equals(searchFor))
                return true;
        }
        
        return false;
    }
    
    public static boolean arrayEquals(Object[] array1, Object[] array2, boolean ignoreOrder)
    {
        if(array1 == null && array2 == null)
            return true;
            
        if(array1 == null && array2 != null)
            return false;
            
        if(array2 == null && array1 != null)
            return false;                   
            
        if(array1.length != array2.length)
            return false;

        if(ignoreOrder)
        {
            for(int i=0; i<array1.length; i++)
            {
                if(!arrayContains(array2, array1[i]))
                    return false;
            }
            
            for(int i=0; i<array2.length; i++)
            {
                if(!arrayContains(array1, array2[i]))
                    return false;
            }
            
            return true;
        }
        else
        {
            return Arrays.equals(array1, array2);
        }
    }
    
    public static String makeList(Object[] array)
    {
        return makeList(array, ",");
    }
    
    public static String makeList(Object[] array, String delim)
    {
        if(array == null || array.length == 0)
            return "";
        
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<array.length; i++)
        {
            sb.append(array[i]);
            if(i < (array.length -1))
                sb.append(delim);
        }
        
        return sb.toString();
    }
    
    public static boolean isNull(String s)
    {
        if(s == null || s.length() == 0 || s.trim().length() == 0)
            return true;
        return false;
    }
    
    public static boolean equal(Object o1, Object o2)
    {
        if(o1 == null && o2 != null)
            return false;

        if(o1 != null && o2 == null)
            return false;

        if(o1 == null && o2 == null)
            return true;
        
        return o1.equals(o2);
    }
    
    public static void output(String msg)
    {
        System.out.println("[" + Util.getTimestamp() + "] " + msg);
    }
    
    public static void output(String id, String msg)
    {
        System.out.println("[" + Util.getTimestamp() + " " + id + "] " + msg);
    }
    
    public static boolean systemIsWindows()
    {
        if(File.separatorChar == '\\')
           return true;
        return false;
    }
    
    public static String formatCurrency(double price)
    {
        return CURRENCY_FORMATTER.format(price);
    }
    
    public static Vector split(String string, String delimiters)
    {
    	Vector v = new Vector();
    	StringTokenizer st = new StringTokenizer(string, delimiters);
    	while(st.hasMoreTokens())
    	{
    		v.add(st.nextToken());
    	}
    	return v;
    }
    
    public static void overwrite(char[] array)
    {
    	for(int i=0; i<array.length; i++)
    		array[i] = '0';
    }
    
    public static void overwrite(byte[] array)
    {
    	for(int i=0; i<array.length; i++)
    		array[i] = 0x00;
    }
    
    public static boolean startsWith(char[] prefix, char[] array, int pos)
    {
    	if(array == null || prefix == null || (array.length-pos) < prefix.length)
    		return false;
    	
    	for(int i=0; i<prefix.length; i++)
    		if(array[pos+i] != prefix[i])
    			return false;
    	
    	return true;
    }
    
    public static char[] convertToChars(byte[] bytes)
    {
    	char[] chars = new char[bytes.length];
    	for(int i=0; i<chars.length; i++)
    		chars[i] = (char) bytes[i];
    	return chars;
    }

    public static byte[] convertToBytes(char[] chars)
    {
    	byte[] bytes = new byte[chars.length];
    	for(int i=0; i<bytes.length; i++)
    		bytes[i] = (byte) chars[i];
    	return bytes;
    }
    
    public static char[] trim(char[] chars)
    {
    	int start = 0;
    	int end = 0;
    	
    	for(int i=0; i<chars.length; i++)
    	{
    		if(!Character.isWhitespace(chars[i]))
    		{
    			start = i;
    			break;
    		}
    	}
    	
    	for(int i=chars.length; i>0; i--)
    	{
    		if(!Character.isWhitespace(chars[i-1]))
    		{
    			end = i;
    			break;
    		}
    	}
    	
    	char[] trimmed = new char[(end-start)];
    	System.arraycopy(chars, start, trimmed, 0, trimmed.length);
    	return trimmed;
    }

    public static void main(String args[])
    {
        System.out.println(Util.getCurrentMethodName());
    }
}
