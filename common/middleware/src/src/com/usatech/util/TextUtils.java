package com.usatech.util;

import java.util.StringTokenizer;

/**
 * Some text manipulation utility classes.
 */
public class TextUtils {

    /**
     * Prepends a string with zeroes to the specified length.
     * @param s the string
     * @param length the desired length
     * @return the padded string
     */
    public static String prepadCharacter(String s, int length, char c) {
	String s2 = s;

	if(s.length() <= length) 
	    for(int i=length - s.length(); i >= 0; i--)
		s2 = c + s2;

	return s2;

    }

    /**
     * Prepends a string with spaces to the specified length.
     * @param s the string
     * @param length the desired length
     * @return the padded string
     */
    public static String prepad(String s, int length) {
        
        return prepadCharacter(s, length, ' ');

    }

    /**
     * Pads a string with spaces to the specified length.
     * @param s the string
     * @param length the desired length
     * @return the padded string
     */
    public static String pad(String s, int length) {
	String s2 = s;
	if(s.length() <= length) {
	    for(int i=length - s.length(); i >= 0; i--)
		s2 += " ";
	}
	return s2;
    }

    /**
     * Finds and replaces text in a string.
     * @param s the string to search
     * @param f the text to find
     * @param r the text to replace found string by
     * @return string with all instances of f replaced by r
     */
    public static String findAndReplace(String s, String f, String r) {
	String result = "";
        int matchPos, lastMatchPos;

        //        DebuggerOutput.debug("findAndReplace: s = " + s + ", f = " + f + ", r = " + r);
        lastMatchPos = 0;
        matchPos = s.indexOf(f, 0);

        //        DebuggerOutput.debug("findAndReplace: setup: matchPos = " + matchPos);

        while(matchPos != -1) {

            result += (lastMatchPos == matchPos ? 
                       r : (s.substring(lastMatchPos, matchPos) + r));
            //            DebuggerOutput.debug("findAndReplace: loop: lastMatchPos = " + lastMatchPos + ", result = " + result);

            lastMatchPos = matchPos + f.length();


            matchPos = s.indexOf(f, lastMatchPos);
            //            DebuggerOutput.debug("findAndReplace: next: lastMatchPos = " + lastMatchPos + ", matchPos = " + matchPos);
           
        }

        String returnString = result + s.substring(lastMatchPos);
        
        //        DebuggerOutput.debug("findAndReplace: finally: result = " + result + ", returning: '" + returnString + "'");

        return returnString;

    }

    /**
     * Wraps a long line into multiple line by inserting \n at
     * word boundaries.
     * @param s the string to wrap
     * @param columns the maximum number of characters on a single line
     * @return wrapped text
     */
    public static String wordWrap(String s, int columns) {
	char[] space = { ' ', '\t', '-' };
	return textWrap(s, columns, space, '\n');
    }

    /**
     * Generic text wrap method. Rather klunky.
     * @param s the string to wrap
     * @param wrap_columns the maximum number of characters on a single line
     * @param delim_chars an array of chars specifying allowed wrap points
     * @param wrap_char the character to use for wrapping (normally \n).
     * @return wrapped text
     */
    public static String textWrap(String s,
                                  int wrapColumns,
                                  char[] delimChars,
                                  char wrapChar) {

	char[] a = s.toCharArray();
	boolean found;
	int index = 0, prevIndex = 0;

	while(a.length - index > wrapColumns) {

	    found = false;

	    for(index += wrapColumns; index > prevIndex; --index) {

		for(int i = 0; i < delimChars.length; i++) {

		    if(a[index] == delimChars[i]) {

			found = true;
                        break;

                    }

                }
                
                if(found)
                    break;

            }

            if(!found)
                index += wrapColumns;

            a[index] = wrapChar;
            prevIndex = index;

	}

	return new String(a);
	
    }
}




