package com.usatech.util;

import java.util.Calendar;
import java.text.NumberFormat;
import java.text.DateFormat;

/**
 * Rudimentary time formatting tools.
 */
public class TimeFormat {
	
    /**
     * Formats a long representing a number of seconds into
     * human-readable time (e.g. "hh:mm", "mm:ss", "2 days, hh:mm".
     * @return formatted text
     */
    public static String format(long sec) {
	NumberFormat nf = NumberFormat.getInstance();
	nf.setMinimumIntegerDigits(2);
	String s = "";
	long days, hours, minutes, seconds, rest;
	days = sec / (60 * 60 * 24);
	rest = sec % (60 * 60 * 24);
	hours = rest / (60 * 60);
	rest = rest % (60 * 60);
	minutes = rest / 60;
	rest = rest % 60;
	seconds = rest;
	if(days > 0) 
	    s += days + (days > 1 ? " days" :
			 " day") + ", ";
	if(hours > 0) 
	    s += nf.format(hours) + ":";

	s += nf.format(minutes);

	if(hours < 1) 
	    s += ":" + nf.format(seconds);

	return s;
    }

    public static String formatCurrentDateTime(int df, int tf) {
        
        return DateFormat.getDateInstance(df).format(Calendar.getInstance().getTime()) + " " + DateFormat.getTimeInstance(tf).format(Calendar.getInstance().getTime());

    }

}


