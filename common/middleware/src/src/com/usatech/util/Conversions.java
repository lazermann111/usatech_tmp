package com.usatech.util;

/**
 * Converts primitive types from and to byte arrays.
 */
public class Conversions
{
	private static final char[] HEX_CHARS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	//private static final String BASE_TYPES = "char byte int long float double boolean";
	
	public static void main(String args[]) throws Exception
	{
		if(args[0].equalsIgnoreCase("b"))
		{
			int i = Integer.parseInt(args[1]);
			String s = Integer.toBinaryString(i);
			System.out.println(s);
			System.out.println(Conversions.binaryToInt(s));
		}
		else if(args[0].equalsIgnoreCase("i"))
		{
			int i = Conversions.binaryToInt(args[1]);
			System.out.println(i);
			System.out.println(Integer.toBinaryString(i));
		}

	}

	/**
	 * Converts a long to an 8 byte array.
	 * @param l a long
	 * @return an 8 byte array
	 */
	public static byte[] longToByteArray(long l)
	{
		byte[] b = new byte[8];
		b[0] = (byte) ((l >>> 56) & 0xff);
		b[1] = (byte) ((l >>> 48) & 0xff);
		b[2] = (byte) ((l >>> 40) & 0xff);
		b[3] = (byte) ((l >>> 32) & 0xff);
		b[4] = (byte) ((l >>> 24) & 0xff);
		b[5] = (byte) ((l >>> 16) & 0xff);
		b[6] = (byte) ((l >>> 8) & 0xff);
		b[7] = (byte) (l & 0xff);
		return b;
	}

	/**
	 * Converts an integer to a 4 char array.
	 * @param l the integer to convert
	 * @return a 4 char array
	 */
	public static char[] intToCharArray(int l)
	{
		char[] c = new char[4];
		c[0] = (char) ((l >> 24) & 0xff);
		c[1] = (char) ((l >> 16) & 0xff);
		c[2] = (char) ((l >> 8) & 0xff);
		c[3] = (char) (l & 0xff);
		return c;
	}

	/**
	 * Converts an integer to a 4 byte array.
	 * @param l the integer to convert
	 * @return a 4 byte array
	 */
	public static byte[] intToByteArray(int l)
	{
		byte[] b = new byte[4];
		b[0] = (byte) ((l >> 24) & 0xff);
		b[1] = (byte) ((l >> 16) & 0xff);
		b[2] = (byte) ((l >> 8) & 0xff);
		b[3] = (byte) (l & 0xff);
		return b;
	}

	/**
	 * Converts a short to a 2 byte array.
	 * @param l the short to convert
	 * @return a 2 byte array
	 */
	public static byte[] shortToByteArray(int l)
	{
		byte[] b = new byte[2];
		b[0] = (byte) ((l >> 8) & 0xff);
		b[1] = (byte) (l & 0xff);
		return b;
	}

	/**
	 * Converts a char to a 2 byte array.
	 * @param l the char to convert
	 * @return a 2 byte array
	 */
	public static byte[] charToByteArray(char l)
	{
		byte[] b = new byte[2];
		b[0] = (byte) ((l >> 8) & 0xff);
		b[1] = (byte) (l & 0xff);
		return b;
	}

	/**
	 * Converts a byte array to a long.
	 * @param b a byte array
	 * @return long
	 * @exception IllegalArgumentException when b is a byte array
	 * with more than 8 elements
	 */
	public static long byteArrayToLong(byte[] b)
	{
		long mask = 0xff;

		if (b.length < 9)
		{

			long val = 0;
			int shift = 0;
			//int j = b.length;

			for (int i = b.length; i > 0; i--)
			{
				val |= (long) (byte) b[i - 1] << shift & mask;
				shift += 8;
				mask <<= 8;
			}

			return val;
		}
		throw new IllegalArgumentException("byteArrayToLong requires <9 byte arrays");
	}

	/**
	 * Converts a byte array to an integer.
	 * @param b byte array
	 * @return integer
	 * @exception IllegalArgumentException when b is a byte array
	 * with more than 4 elements
	 */
	public static int byteArrayToInt(byte[] b)
	{
		int mask = 0xff;

		if (b.length < 5)
		{

			int val = 0;
			int shift = 0;
			//int j = b.length;

			for (int i = b.length; i > 0; i--)
			{
				val |= (byte) b[i - 1] << shift & mask;
				shift += 8;
				mask <<= 8;
			}

			return val;
		}
		throw new IllegalArgumentException("byteArrayToLong requires <5 byte arrays");
	}

	/**
	 * Converts a 2 byte array into a short.
	 * @param b 2 byte array
	 * @return short
	 * @exception IllegalArgumentException when b is not a 2 byte array
	 */
	public static short byteArrayToShort(byte[] b)
	{
		if (b.length == 2)
		{
			short s;
			s = (short) (((short) b[1] & 0xff) | ((short) (b[0] << 8 & 0xff00)));
			return s;
		}
		throw new IllegalArgumentException("byte_array_to_short requires 2 byte array");
	}

	/**
	 * Swaps the bytes in a byte array.
	 * @param b the byte array
	 * @return byte swapped byte array
	 */
	public static byte[] swapByteArray(byte[] b)
	{
		byte[] b2 = new byte[b.length];
		for (int i = 0; i < b.length; i++)
			b2[(b.length - 1) - i] = b[i];
		return b2;
	}

	/*
		public static String byteToHex(byte b)
		{
			int i = b & 0xFF;
			String s = Integer.toHexString(i);
			if (s.length() == 1)
				s = "0" + s;
			return s;
		}
	
		public static String bytesToHex(byte[] array)
		{
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; i++)
			{
				sb.append(byteToHex(array[i]));
			}
			return sb.toString();
		}
	*/

	/**
	 * Convert a byte to a Hex string.
	 * @param b The byte to convert.
	 * @return a two character Hex representation of the byte.
	 */
	public static String byteToHex(byte b)
	{

		StringBuilder sb = new StringBuilder(2);
		int i = (b & 0xf0) >> 4;
		int j = b & 0xf;

		sb.append(HEX_CHARS[i]);
		sb.append(HEX_CHARS[j]);

		return sb.toString();
	}

	/**
	 * Convert an array of bytes to a String composed of hex representations
	 * of the bytes separated by a given separator character.
	 * @param ba The byte array to convert.
	 * @param sep The separator character.
	 * @return The string representation.
	 */
	public static String bytesToHex(byte[] ba, char sep)
	{

		int len = ba.length;
		int j;
		int k;
		StringBuilder sb = new StringBuilder((len * 3));

		for (int i = 0; i < len; i++)
		{
			j = (ba[i] & 0xf0) >> 4;
			k = ba[i] & 0xf;

			sb.append(HEX_CHARS[j]);
			sb.append(HEX_CHARS[k]);

			if (i < (len - 1))
			{
				sb.append(sep);
			}
		}

		return sb.toString();
	}

	/**
	 * Convert an array of bytes to a String composed of hex representations
	 * of the bytes.
	 * @param ba The byte array to convert.
	 * @return The string representation.
	 */
	public static String bytesToHex(byte[] ba)
	{

		int len = ba.length;
		int j;
		int k;
		StringBuilder sb = new StringBuilder((len * 3));

		for (int i = 0; i < len; i++)
		{
			j = (ba[i] & 0xf0) >> 4;
			k = ba[i] & 0xf;

			sb.append(HEX_CHARS[j]);
			sb.append(HEX_CHARS[k]);
		}

		return sb.toString();
	}

	/**
	 * Convert an array of bytes to a String composed of hex representations
	 * of the bytes.
	 * @param ba The byte array to convert.
	 * @return The string representation.
	 */
	public static String bytesToHex(byte[] ba, int offset, int length)
	{
		int j;
		int k;
		StringBuilder sb = new StringBuilder((length * 3));

		for (int i = offset; i < (length+offset); i++)
		{
			j = (ba[i] & 0xf0) >> 4;
			k = ba[i] & 0xf;

			sb.append(HEX_CHARS[j]);
			sb.append(HEX_CHARS[k]);
		}

		return sb.toString();
	}

	/**
	 * Convert an array of byte to an array of char.
	 * @param ba The byte array to convert.
	 * @return An array of char.
	 */
	public static char[] bytesToChars(byte[] ba)
	{

		char[] ca = new char[ba.length];

		for (int i = 0; i < ba.length; i++)
		{
			ca[i] = (char) ba[i];
		}

		return ca;
	}

	/**
	 * Convert a string to its boolean value. This method checks for "yes" or "true" to
	 * determine if the value is <b>true</b> anything else returns <b>false</b>.
	 * @return the boolean value for a string.
	 * @param val The string to evaluate.
	 */
	public static boolean stringToBool(String val)
	{

		boolean result = false;

		if (val.equalsIgnoreCase("true") || val.equalsIgnoreCase("yes"))
		{
			result = true;
		}

		return result;
	}

	/**
	 * Convert a string to its int value.
	 * @return The int value of the string or <b>Integer.MIN_VALUE</b> if the string is not numeric.
	 * @param str The String to convert.
	 */
	public static int stringToInt(String str)
	{

		int result = Integer.MIN_VALUE;

		try
		{
			Integer temp = new Integer(str);

			result = temp.intValue();
		}
		catch (Exception e)
		{
		}

		return result;
	}

	/**
	 * Convert a string to its long value.
	 * @return The long value of the string or <b>Long.MIN_VALUE</b> if the string is not numeric.
	 * @param str The String to convert.
	 */
	public static long stringToLong(String str)
	{

		long result = Long.MIN_VALUE;

		try
		{
			Long temp = new Long(str);

			result = temp.longValue();
		}
		catch (Exception e)
		{
		}

		return result;
	}

	/**
	 * Convert a string formed of hex byte representations to a byte array.
	 * @param hey The hex string.
	 * @param hex
	 * @return An array of byte.
	 * @exception NumberFormatException If the string cannot be converted.
	 */
	public static byte[] hexToByteArray(String hex) throws NumberFormatException
	{

		if ((hex.length() % 2) != 0)
		{
			throw new NumberFormatException("The string has the wrong length, not pairs of hex representations.");
		}

		int len = hex.length() / 2;
		byte[] ba = new byte[len];
		int pos = 0;

		for (int i = 0; i < len; i++)
		{
			ba[i] = hexToByte(hex.substring(pos, pos + 2).toCharArray());
			pos += 2;
		}

		return ba;
	}

	/**
	 * Convert the two character arrax hex to a byte.
	 * @param hex The character array to parse.
	 * @return The byte array.
	 * @exception NumberFormatException if the first 2 characters of
	 * the array are not hexadecimal characters.
	 */
	public static byte hexToByte(char[] hex) throws NumberFormatException
	{

		int i = 0;
		byte nibble;

		if ((hex[i] >= '0') && (hex[i] <= '9'))
		{
			nibble = (byte) ((hex[i] - '0') << 4);
		}
		else if ((hex[i] >= 'A') && (hex[i] <= 'F'))
		{
			nibble = (byte) ((hex[i] - ('A' - 0x0A)) << 4);
		}
		else if ((hex[i] >= 'a') && (hex[i] <= 'f'))
		{
			nibble = (byte) ((hex[i] - ('a' - 0x0A)) << 4);
		}
		else
		{
			throw new NumberFormatException(hex[i] + " is not a hexadecimal string.");
		}

		if (i == hex.length)
		{
			throw new NumberFormatException("Invalid number of digits in " + new String(hex));
		}

		i++;

		if ((hex[i] >= '0') && (hex[i] <= '9'))
		{
			nibble = (byte) (nibble | (hex[i] - '0'));
		}
		else if ((hex[i] >= 'A') && (hex[i] <= 'F'))
		{
			nibble = (byte) (nibble | (hex[i] - ('A' - 0x0A)));
		}
		else if ((hex[i] >= 'a') && (hex[i] <= 'f'))
		{
			nibble = (byte) (nibble | (hex[i] - ('a' - 0x0A)));
		}
		else
		{
			throw new NumberFormatException(hex[i] + " is not a hexadecimal string.");
		}

		return nibble;
	}

	/**
	 * Convert an array of char to an array of byte.
	 * @param ca The char array to convert.
	 * @return An array of byte.
	 */
	public static byte[] charArrayToBytes(char[] ca)
	{

		byte[] ba = new byte[ca.length];

		for (int i = 0; i < ca.length; i++)
		{
			ba[i] = (byte) ca[i];
		}

		return ba;
	}
	
	public static int binaryToInt(String s)
	{
		String input = s;
		int inputLength = input.length();
		int output = 0; 
		String characterTesting = "";

		for(int i=0;i<inputLength;i++)
		{
			characterTesting = "" + input.charAt(inputLength-1-i);

			if(characterTesting.equals("1"))
				output = output + (int) Math.pow(2,i);
		}

		return output;
	}	
	
	/**
	 * Convert an int into an n-byte array.
	 * @param number the integer to convert.
	 * @param numberOfBytesInArray
	 * @return An array of byte.
	 */
	public static byte[] convertToNByteArray(int number,int numberOfBytesInArray)
	{
		byte[] byteArray = new byte[numberOfBytesInArray];

		String baseBinaryString = "";

		for (int byteCounter = 0;byteCounter < numberOfBytesInArray;byteCounter++)
		{
			baseBinaryString = baseBinaryString + "00000000";
		}

		byte[] binaryArray = baseBinaryString.getBytes();

		if (number != 0)
		{
			String binaryStringOfNumber = Integer.toBinaryString(number);
			
			System.arraycopy(binaryStringOfNumber.getBytes(),0,binaryArray,(numberOfBytesInArray*8)-binaryStringOfNumber.length(),binaryStringOfNumber.length());
		}

		byte[] currByte = new byte[8];

		//int testInt = 0;
		
		for (int byteArrayCounter = 0;byteArrayCounter < numberOfBytesInArray;byteArrayCounter++)
		{
			System.arraycopy(binaryArray,(byteArrayCounter*8),currByte,0,8);		
			byteArray[byteArrayCounter] = (byte)(char)Integer.parseInt(new String(currByte),2);

		}

		return byteArray;

	}
		
	public static byte[] convertToNByteArray(long number,int numberOfBytesInArray)
	{
		byte[] byteArray = new byte[numberOfBytesInArray];

		String baseBinaryString = "";

		for (int byteCounter = 0;byteCounter < numberOfBytesInArray;byteCounter++)
		{
			baseBinaryString = baseBinaryString + "00000000";
		}

		byte[] binaryArray = baseBinaryString.getBytes();

		if (number != 0)
		{
			String binaryStringOfNumber = Long.toBinaryString(number);
		
			System.arraycopy(binaryStringOfNumber.getBytes(),0,binaryArray,(numberOfBytesInArray*8)-binaryStringOfNumber.length(),binaryStringOfNumber.length());
		}

		
		byte[] currByte = new byte[8];

		for (int byteArrayCounter = 0;byteArrayCounter < numberOfBytesInArray;byteArrayCounter++)
		{
			System.arraycopy(binaryArray,(byteArrayCounter*8),currByte,0,8);	
				
			byteArray[byteArrayCounter] = (byte)(char)Integer.parseInt(new String(currByte),2);
		}

		return byteArray;

	}
}
