package com.usatech.util;

/*
 This object was based off of org.ofbiz.core.util.UtilCache.  For more 
 information see the "Open For Business Project" at http://ofbiz.sourceforge.net
 */

import java.util.*;
import java.io.*;

public class Cache
{
	public static void main(String args[])
	{
		Cache myCache = new Cache(10, 30000, 100, 1000);

		try
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

			while (true)
			{
				System.out.println();
				System.out.print("> ");
				String s = br.readLine();
				if (s != null && s.length() > 0)
				{
					StringTokenizer st = new StringTokenizer(s, " ,|-", false);
					if (st.countTokens() != 2)
						break;

					String key = st.nextToken();
					String value = st.nextToken();

					myCache.put(key, new CachableString(value, true));
				}

				System.out.println();
				Runtime runtime = Runtime.getRuntime();
				System.out.println(runtime.freeMemory() + "/" + runtime.totalMemory());
			}
		}
		catch (Exception e)
		{
			System.exit(0);
		}
	}

	/** A list of the elements order by Least Recent Use
	 */
	public LinkedList				keyLRUList	= new LinkedList();

	/** A hashtable containing a value for each element.
	 */
	public Hashtable				valueTable	= new Hashtable();

	/** A count of the number of cache hits
	 */
	protected int					hitCount	= 0;

	/** A count of the number of cache misses
	 */
	protected int					missCount	= 0;

	/** The maximum number of elements in the cache.
	 * If set to 0, there will be no limit on the number of elements in the cache.
	 */
	protected int					maxSize		= 0;

	/** A hashtable containing a Long integer representing the time that the corresponding element was first loaded
	 */
	public Hashtable				expireTable	= new Hashtable();

	/** Specifies the amount of time since initial loading before an element will be reported as expired.
	 * If set to 0, elements will never expire.
	 */
	protected long					expireTime	= 0;

	/** A queue containing Cachable objects that need to be purged
	 */
	protected Queue					purgeList	= new Queue();

	/** The number of threads to allocate for calling onPurge on Cachable objects.  
	 *  If set to 0, no threads will run and onPurge will not be called on Cachable objects.
	 */
	protected int					numThreads	= 0;

	/** The time in ms to wait between checks for Cachable items to purge.
	 */
	protected long					checkTime	= 0;

	protected ExpiredCheckerThread	checkerThread;
	protected Queue					purgeThreads;

	/** Constructor which specifies the maxSize and expireTime.  Cachable objects will not have onPurge called.
	 * @param maxSize The maxSize member is set to this value.
	 * @param expireTime The expireTime member is set to this value.
	 *  If numThreads or checkTime <= 0, Cachable objects will not have onPurge called.
	 */
	public Cache(int maxSize, int expireTime)
	{
		this(maxSize, expireTime, 0, 0);
	}

	/** Constructor which specifies the maxSize, expireTime, numThreads, and checkTime.
	 * @param maxSize The maxSize member is set to this value.
	 * @param expireTime The expireTime member is set to this value.
	 * @param numThreads The number of threads to run that will purge Cachable obects.
	 * @param checkTime The time in ms to wait between checks for Cachable items to purge. 
	 *  If numThreads or checkTime <= 0, cachable objects will not have onPurge called.
	 */
	public Cache(int maxSize, int expireTime, int numThreads, long checkTime)
	{
		this.maxSize = maxSize;
		this.expireTime = expireTime;

		if (numThreads > 0 && checkTime > 0)
		{
			this.numThreads = numThreads;
			this.checkTime = checkTime;

			checkerThread = new ExpiredCheckerThread();
			purgeThreads = new Queue();
			for (int i = 0; i < numThreads; i++)
			{
				purgeThreads.addBack(new PurgeThread(i));
			}
		}
	}

	/** Puts or loads the passed element into the cache
	 * @param key The key for the element, used to reference it in the hastables and LRU linked list
	 * @param value The value of the element
	 */
	public synchronized void put(Object key, Object value)
	{
		//System.out.println("put("+key+","+value+")");

		if (key == null)
			return;

		if (valueTable.containsKey(key))
		{
			//System.out.println("[Cache] keyLRUList.remove("+key+")");
			keyLRUList.remove(key);
			//System.out.println("[Cache] keyLRUList.addFirst("+key+")");
			keyLRUList.addFirst(key);
		}
		else
		{
			//System.out.println("[Cache] keyLRUList.addFirst("+key+")");
			keyLRUList.addFirst(key);
		}

		//System.out.println("[Cache] valueTable.put("+key+","+value+")");
		valueTable.put(key, value);
		expireTable.put(key, new Long(System.currentTimeMillis()));

		if (valueTable.size() > maxSize && maxSize != 0)
		{
			//System.out.println("[Cache] remove(lastKey)");
			Object lastKey = keyLRUList.getLast();
			remove(lastKey, true);
		}
	}

	/** Gets an element from the cache according to the specified key.
	 * If the requested element hasExpired, it is removed before it is looked up which causes the function to return null.
	 * @param key The key for the element, used to reference it in the hastables and LRU linked list
	 * @return The value of the element specified by the key
	 */
	public synchronized Object get(Object key)
	{
		//System.out.println("get("+key+")");

		if (key == null)
		{
			missCount++;
			return null;
		}

		if (hasExpired(key))
		{
			remove(key, true);
		}

		Object value = valueTable.get(key);
		if (value == null)
		{
			missCount++;
			return null;
		}

		hitCount++;
		keyLRUList.remove(key);
		keyLRUList.addFirst(key);

		return value;
	}

	/** Removes an element from the cache according to the specified key.  
	 *  Will not call onPurge for Cachable objects.
	 * @param key The key for the element, used to reference it in the hastables and LRU linked list
	 * @return The value of the removed element specified by the key
	 */
	public synchronized Object remove(Object key)
	{
		return remove(key, false);
	}

	/** Removes an element from the cache according to the specified key
	 * @param key The key for the element, used to reference it in the hastables and LRU linked list
	 * @param purge If true, and the value for the key is a Cachable, onPurge will be called.
	 * @return The value of the removed element specified by the key
	 */
	public synchronized Object remove(Object key, boolean purge)
	{
		if (key != null && valueTable.containsKey(key))
		{
			Object value = valueTable.get(key);

			if (purge && (value instanceof Cachable))
				addForPurging((Cachable) value);

			valueTable.remove(key);
			expireTable.remove(key);
			keyLRUList.remove(key);
			return value;
		}
		else
		{
			missCount++;
			expireTable.remove(key);
			keyLRUList.remove(key);
			return null;
		}
	}

	/** Removes all element from the cache.  
	 *  Does not cause onPurge to be called on Cachable objects.
	 */
	public synchronized void clear()
	{
		clear(false);
	}

	/** Removes all element from the cache.
	 * @param purge If true, onPurge will be called on all Cachable objects.
	 */
	public synchronized void clear(boolean purge)
	{
		if (purge)
		{
			Enumeration e = valueTable.elements();
			while (e.hasMoreElements())
			{
				Object o = e.nextElement();
				if (o instanceof Cachable)
				{
					addForPurging((Cachable) o);
				}
			}
		}

		//Enumeration e;
		//for (e = valueTable.keys(); e.hasMoreElements();) remove(e.nextElement());
		valueTable.clear();
		expireTable.clear();
		keyLRUList.clear();
		clearCounters();
	}

	/** Add a Cachable object to the list of objects that need to be purged.  
	 *  If no threads are allocated for purging, this will no nothing.
	 */
	protected void addForPurging(Cachable cachable)
	{
		if (numThreads > 0)
		{
			//System.out.println("[Cache] purgeList.addBack("+cachable+")");
			if (!purgeList.contains(cachable))
				purgeList.addBack(cachable);
		}
	}

	/** Returns the number of successful hits on the cache
	 * @return The number of successful cache hits
	 */
	public int getHitCount()
	{
		return hitCount;
	}

	/** Returns the number of cache misses
	 * @return The number of cache misses
	 */
	public int getMissCount()
	{
		return missCount;
	}

	/** Clears the hit and miss counters
	 */
	public void clearCounters()
	{
		hitCount = 0;
		missCount = 0;
	}

	/** Sets the maximum number of elements in the cache.
	 * If 0, there is no maximum.
	 * @param maxSize The maximum number of elements in the cache
	 */
	public synchronized void setMaxSize(int maxSize)
	{
		//if the new maxSize is less than the old maxSize, shrink the cache.
		if ((maxSize < this.maxSize || this.maxSize == 0) && maxSize != 0)
		{
			while (valueTable.size() > maxSize)
			{
				Object lastKey = keyLRUList.getLast();
				remove(lastKey);
			}
		}
		this.maxSize = maxSize;
	}

	/** Returns the current maximum number of elements in the cache
	 * @return The maximum number of elements in the cache
	 */
	public int getMaxSize()
	{
		return maxSize;
	}

	/** Sets the expire time for the cache elements.
	 * If 0, elements never expire.
	 * @param expireTime The expire time for the cache elements
	 */
	public void setExpireTime(long expireTime)
	{
		this.expireTime = expireTime;
	}

	/** return the current expire time for the cache elements
	 * @return The expire time for the cache elements
	 */
	public long getExpireTime()
	{
		return expireTime;
	}

	/** Returns the number of elements currently in the cache
	 * @return The number of elements currently in the cache
	 */
	public int size()
	{
		return valueTable.size();
	}

	/** Returns the number of elements currently waiting to be purged.
	 * @return The number of elements currently waiting to be purged.
	 */
	public int purgeListSize()
	{
		return purgeList.size();
	}

	/** Returns a boolean specifying whether or not an element with the specified key is in the cache.
	 * If the requested element hasExpired, it is removed before it is looked up which causes the function to return false.
	 * @param key The key for the element, used to reference it in the hastables and LRU linked list
	 * @return True is the cache contains an element corresponding to the specified key, otherwise false
	 */
	public synchronized boolean containsKey(Object key)
	{
		if (hasExpired(key))
			remove(key, true);

		return valueTable.containsKey(key);
	}

	/** Returns a boolean specifying whether or not the element corresponding to the key has expired.
	 * Only returns true if element is in cache and has expired. Error conditions return false.
	 *
	 * @param key The key for the element, used to reference it in the hastables and LRU linked list
	 * @return True is the element corresponding to the specified key has expired, otherwise false
	 */
	public synchronized boolean hasExpired(Object key)
	{
		if (expireTime == 0)
			return false;
		if (key == null)
			return false;
		Long time = (Long) expireTable.get(key);
		if (time == null)
			return false;
		Long curTime = new Long(System.currentTimeMillis());
		if (time.longValue() + expireTime < curTime.longValue())
			return true;
		return false;
	}

	/** This thread run over so often to check for expired objects and add them to the purge list.
	 */
	protected class ExpiredCheckerThread extends Thread
	{
		protected ExpiredCheckerThread()
		{
			this.setDaemon(true);
			this.start();
		}

		public void run()
		{
			while (true)
			{
				try
				{
					Thread.sleep(checkTime);
				}
				catch (InterruptedException e)
				{
				}

				Object[] array = null;

				try
				{
					array = keyLRUList.toArray();
				}
				catch (Exception e)
				{
				}

				if (array == null || array.length == 0)
					continue;

				int i = 0;
				try
				{
					for (i = (array.length - 1); i >= 0; i--)
					{
						if (hasExpired(array[i]))
						{
							remove(array[i], true);
							/*
							 Object o = valueTable.get(array[i]);
							 if((o != null) && (o instanceof Cachable))
							 {
							 System.out.println("[ExpiredCheckerThread] addForPurging("+o+")");
							 addForPurging((Cachable) o);
							 }
							 */
						}
					}
				}
				catch (ArrayIndexOutOfBoundsException e)
				{
					System.out.println("[ExpiredCheckerThread] Caught exception iterating through array. i=" + i);
					e.printStackTrace(System.out);
					continue;
				}
			}
		}
	}

	/** This thread runs to call onPurge on Cachable objects.
	 */

	protected class PurgeThread extends Thread
	{
		private int	id;

		protected PurgeThread(int id)
		{
			this.id = id;
			this.setDaemon(true);
			this.start();
		}

		protected int getID()
		{
			return id;
		}

		public void run()
		{
			while (true)
			{
				if (purgeList.isEmpty())
				{
					try
					{
						Thread.sleep(checkTime);
					}
					catch (InterruptedException e)
					{
					}
				}

				Object o = null;

				try
				{
					o = purgeList.getFront();
				}
				catch (Exception e)
				{
				}

				if (o == null)
					continue;

				System.out.println("[PurgeThread." + id + "] Calling onPurge on " + o);
				((Cachable) o).onPurge();
			}
		}
	}
}
