package com.usatech.util.mail;

import javax.activation.DataSource;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.io.IOException;

public class StringDataSource implements DataSource
{
	private String	contentType	= null;
	private String	name		= null;
	private String	buf			= null;

	/**
	 * Create a new <tt>StringDataSource</tt> object.
	 * 
	 * @param s
	 *            The string from which to read
	 * @param mimeType
	 *            The MIME type (or content type) to associate with the contents of the string. If this parameter is null, it defaults to "application/octet-stream".
	 * @param name
	 *            The name to associate with the content. For an email attachment, this parameter is typically used to name the attachment. If this parameter is <tt>null</tt>, a name will be generated.
	 */
	public StringDataSource(String s, String mimeType, String name)
	{
		this.buf = s;
		this.contentType = mimeType;
		this.name = name;

		if (this.name == null)
			this.name = System.currentTimeMillis() + ".DAT";

		if (contentType == null)
			contentType = "application/octet-stream";
	}

	/**
	 * Get a new <tt>InputStream</tt> object that will read from this object's string buffer.
	 * 
	 * @return The <tt>InputStream</tt>.
	 * 
	 * @exception java.io.IOException
	 *                On error.
	 */
	public InputStream getInputStream() throws IOException
	{
		return new ByteArrayInputStream(buf.getBytes());
	}

	/**
	 * According to the documentation for the <tt>DataSource</tt> interface, this method returns an OutputStream where the data can be written and throws the appropriate exception if it can not do so. Since there's no output destination associated with a string, this implementation of <tt>getOutputStream()</tt> automatically throws an exception.
	 * 
	 * @return Nothing.
	 * 
	 * @exception java.io.IOException
	 *                Always.
	 */
	public OutputStream getOutputStream() throws IOException
	{
		throw new IOException("Can't have an OutputStream with a StringDataSource object.");
	}

	/**
	 * Get the content type (i.e., the MIME type) associated with the underlying string's content.
	 * 
	 * @return The content type.
	 */
	public String getContentType()
	{
		return contentType;
	}

	/**
	 * Get the name associated with the underlying string's content.
	 * 
	 * @return The name.
	 */
	public String getName()
	{
		return name;
	}
}
