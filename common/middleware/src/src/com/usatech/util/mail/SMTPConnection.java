package com.usatech.util.mail;

import java.util.*;
import java.io.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import com.usatech.util.*;

public class SMTPConnection
{
	public static final int TEXT_FORMAT			= 1;
	public static final int HTML_FORMAT			= 2;
	public static final int MULTIPART_FORMAT	= 3;
	
	public static final String DEFAULT_SERVER = "127.0.0.1";
	
	public static final int maxTries = 3;
	protected String server = null;
	protected int port = 25;
	protected boolean connected = false;
	
	protected Session session;
	protected Transport transport;
	
	protected long createTime = -1;
	protected long sendCount = 0;
	protected long failedCount = 0;
	
	protected PrintStream out;
	
	public static void main(String args[]) throws Exception
	{
		SMTPConnection conn = new SMTPConnection(args[0]);
		conn.send("pwc21@yahoo.com", "pwc21@yahoo.com", null, "This is a test", "Hello?  Hello?", null, TEXT_FORMAT);
	}
	
	public SMTPConnection() throws Exception
	{
		this(DEFAULT_SERVER, 25);
	}
	
	public SMTPConnection(String server) throws Exception
	{
		this(server, 25);
	}

	public SMTPConnection(String server, int port) throws Exception
	{
		this(server, port, System.out);
	}
	
	public SMTPConnection(String server, int port, PrintStream out) throws Exception
	{
		this.server = server;
		this.port = port;
		this.out = out;
		Properties props = System.getProperties();
		props.put("mail.smtp.host", server);
		props.put("mail.smtp.port", port + "");
		this.session = Session.getDefaultInstance(props);
		transport = session.getTransport("smtp");
		createTime = System.currentTimeMillis();
		connect();
	}

	public String getServer() { return server; }
	public Session getSession() { return session; }
	public boolean connected() { return connected; }
	public long getCreateTime() { return createTime; }
	public long getSendCount() { return sendCount; }
	public long getFailedCount() { return failedCount; }
	
	protected long getConnectionAge()
	{
		return (System.currentTimeMillis() - getCreateTime());
	}
		
	protected double getSendRate()
	{
		if((getConnectionAge() <= 0) || (getSendCount() <= 0))
		   return 0;
		else
			return ((getSendCount()/(getConnectionAge())*1000));
	}
		
	public String toString()
	{
		return "SMTPConnection: " + getConnectionAge() + " ms. old, " + getSendCount() + " messages sent, " + getSendRate() + " messages per second";
	}

	protected boolean waitForConnection(long maxWait)
	{
		long RETRY_INTERVAL = 500;
		long timeWaited = 0;
		while (!connected && timeWaited <= maxWait)
		{
			try
			{
				Thread.sleep(RETRY_INTERVAL);
			}
			catch(InterruptedException e) {}
			timeWaited += RETRY_INTERVAL;
		}
		return connected;
	}

	public void connect() throws Exception
	{
		disconnect();
		
		if(transport != null)
		{
			try
			{
				transport.connect();
				connected = true;
				log("Successfully connected to " + server + ":" + port);
			}
			catch(Exception e)
			{
				log("Transport failed to connect.  Exception is " + e);
				connected = false;
				throw e;
			}
		}
	}
	
	public void disconnect()
	{
		if(transport != null)
		{
			try
			{
				if(transport.isConnected())
				{
					transport.close();
					log("Closed transport to " + server + ":" + port);
				}
			}
			catch(Exception e)
			{
				log("Transport failed to close.  Exception is " + e);
			}
		}
	}
	
	public boolean isConnected()
	{
		if(transport != null)
			return transport.isConnected();
		return false;
	}
	
	private void log(String msg) 
	{
		out.println("[" + Util.getTimestamp() + "] " + msg);
	}
	
	public void send(String from, String to, String replyTo, String subject, String textBody, String htmlBody, int mailType)
	{
		MimeMessage msg = null;
		long messageID = -1;
		
		try
		{
			msg = new MimeMessage(session);
			msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to, false));
			msg.setFrom(InternetAddress.parse(from)[0]);
			if(replyTo != null)
				msg.setReplyTo(InternetAddress.parse(replyTo));
			if(subject != null)
				msg.setSubject(subject);
			msg.setSentDate(new Date());
			
						
			if(mailType == MULTIPART_FORMAT)
			{
				MimeMultipart content = new MimeMultipart("alternative");
							
				MimeBodyPart text = new MimeBodyPart();
				text.setContent(textBody, "text/plain");
				content.addBodyPart(text);

				MimeBodyPart html = new MimeBodyPart();
				html.setContent(htmlBody, "text/html");
				content.addBodyPart(html);
				
				msg.setContent(content);
			}
			else if(mailType == HTML_FORMAT)
			{
				msg.setContent(htmlBody, "text/html");
			}
			else
			{
				msg.setText(textBody);
			}
		}
		catch(Exception e)
		{
			log("Error in the message: " + from + ", " + to + ", " + subject);
			log(Util.getStackTraceAsString(e));
		}
				
		int tries = 0;
		while(true)
		{
			try
			{
				if(!transport.isConnected() || tries > 1)
				{
					connect();
				}
				
				msg.saveChanges();
				transport.sendMessage(msg, msg.getAllRecipients());
				sendCount++;
					
				break;
			}
			catch(Exception e)
			{
				if(++tries > maxTries)
				{
					log("Failed " + maxTries + " times to send the message: " + msg);
					log("Exception is " + e);
					log(Util.getStackTraceAsString(e));
					failedCount++;
					return;
				}
			}
		}
	}
	
	public void send(String from, String to, String replyTo, String subject, String textBody, String htmlBody, String attachment, String attachmentMimeType, String attachmentName)
	{
		MimeMessage msg = null;
		long messageID = -1;
		
		try
		{
			msg = new MimeMessage(session);
			msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to, false));
			msg.setFrom(InternetAddress.parse(from)[0]);
			if(replyTo != null)
				msg.setReplyTo(InternetAddress.parse(replyTo));
			if(subject != null)
				msg.setSubject(subject);
			msg.setSentDate(new Date());
			
			//MimeMultipart content = new MimeMultipart("alternative");
			MimeMultipart content = new MimeMultipart();
			
			if(textBody != null)
			{
				MimeBodyPart text = new MimeBodyPart();
				text.setContent(textBody, "text/plain");
				content.addBodyPart(text);
			}

			if(htmlBody != null)
			{
				MimeBodyPart html = new MimeBodyPart();
				html.setContent(htmlBody, "text/html");
				content.addBodyPart(html);
			}
			
			if(attachment != null)
			{
				MimeBodyPart attachmentPart = new MimeBodyPart();
				DataSource stringSource = new StringDataSource(attachment, attachmentMimeType, attachmentName);
				attachmentPart.setDataHandler(new DataHandler(stringSource));
				attachmentPart.setFileName(attachmentName);
				//attachmentPart.setDisposition("attachment");
				content.addBodyPart(attachmentPart);
			}
			
			msg.setContent(content);
		}
		catch(Exception e)
		{
			log("Error in the message: " + from + ", " + to + ", " + subject);
			log(Util.getStackTraceAsString(e));
		}
				
		int tries = 0;
		while(true)
		{
			try
			{
				if(!transport.isConnected() || tries > 1)
				{
					connect();
				}
				
				msg.saveChanges();
				transport.sendMessage(msg, msg.getAllRecipients());
				sendCount++;
					
				break;
			}
			catch(Exception e)
			{
				if(++tries > maxTries)
				{
					log("Failed " + maxTries + " times to send the message: " + msg);
					log("Exception is " + e);
					log(Util.getStackTraceAsString(e));
					failedCount++;
					return;
				}
			}
		}
	}
}
