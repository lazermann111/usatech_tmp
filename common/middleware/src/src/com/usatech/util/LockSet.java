package com.usatech.util;

import java.util.Map;

public class LockSet extends java.util.LinkedHashMap<String, Object>
{
	protected int maxSize;
	
	public LockSet(int maxSize)
	{
		this.maxSize = maxSize;
	}
	
	public synchronized Object acquireLock(String key)
	{
		Object value = get(key);
		if(value != null)
			return value;
		
		value = new Object();
		put(key, value);
		return value;
	}
	
	protected boolean removeEldestEntry(Map.Entry eldest)
	{
		return size() > maxSize;
	}
	
	public int getMaxSize()
	{
		return maxSize;
	}
}
