package com.usatech.util;

public interface TokenHandler
{
	public String getValue(String token);
}
