/*
 * Created on Sep 8, 2003
 *
 */
package com.usatech.util;

/**
 * @author pcowan
 *
 */
public class PortedUtils
{
	public static void main(String args[])
	{
		String action = args[0];
		String in = args[1];
		
		if(action.toUpperCase().startsWith("E"))
		{
			byte[] bytes = Conversions.hexToByteArray(in);
			String encoded = uuencode(bytes);
			System.out.println("encoded: \""+encoded+"\", hex: \""+Conversions.bytesToHex(encoded.getBytes())+"\"");
		}
		else if(action.toUpperCase().startsWith("D"))
		{
			byte[] bytes = uudecode(new String(Conversions.hexToByteArray(in)));
			String decoded = Conversions.bytesToHex(bytes);
			System.out.println("decoded: \""+decoded+"\"");
		}
	}
	
	public static String uuencode(byte[] inbuf) //char *uuencode(unsigned char* inbuf, int inbuflen, int* outbuflen)
	{
		int i=0, cnt=0, outbufsize = (inbuf.length % 3 + (int) (4*inbuf.length / 3)+1);

		//char *outbuf = (char *) malloc(outbufsize);
		char[] outbuf = new char[outbufsize];
		
		//BYTE hex0a = (BYTE)10;

		byte var = 0; //BYTE var = 0;

		for (i=0; i< outbufsize; i++) 
		{
			outbuf[i]^=outbuf[i];
		}
	
		for (i=0; i<inbuf.length; i+=3)
		{
			outbuf[cnt] = (char) ( 0x20 + ((inbuf[i]>>2)  & 0x3F)); //outbuf[cnt] = (char) ( hex20 + ((inbuf[i]>>2)  & hex3f));
			cnt += 1;

			if ((i+1) < inbuf.length)
			{
				outbuf[cnt] = (char) ( 0x20 + (((inbuf[i] <<4) | (((inbuf[i+1] >>4) & 0x0F))) & 0x3F)); //outbuf[cnt] = (char) ( hex20 + (((inbuf[i] <<4) | (((inbuf[i+1] >>4) & hex0f))) & hex3f));

				cnt += 1;

				if ( (i+2) < inbuf.length)
				{
				
					outbuf[cnt] = (char) ( 0x20 + ((((inbuf[i+1] <<2)) | (((inbuf[i+2] >>6) & 0x03))) & 0x3F)); //outbuf[cnt] = (char) ( hex20 + ((((inbuf[i+1] <<2)) | (((inbuf[i+2] >>6) & hex03))) & hex3f));
					cnt += 1;

					var = (byte) (0x20 + ((inbuf[i+2]) & 0x3F)); //var = (hex20 + ((inbuf[i+2]) & hex3f));
					outbuf[cnt] = (char) (0x20 + ((inbuf[i+2]) & 0x3F)); //outbuf[cnt] = (char) (hex20 + ((inbuf[i+2]) & hex3f));
					cnt += 1;
					continue;
				}

				var = (byte) (0x20 + ((inbuf[i+1] <<2)  & 0x3F)); //var = (hex20 + ((inbuf[i+1] <<2)  & hex3f));
				outbuf[cnt] = (char) (0x20 + ((inbuf[i+1] <<2)  & 0x3F)); //outbuf[cnt] = (char) (hex20 + ((inbuf[i+1] <<2)  & hex3f));
			
				cnt += 1;
				break;

			}
	
			outbuf[cnt] = (char) (0x20+ ((inbuf[i] <<4) & 0x3F)); //outbuf[cnt] = (char) (hex20+ ((inbuf[i] <<4) & hex3f));
			cnt += 1;
			break;
		}

		//strcat(outbuf, (const char*) "\x0a");	//eoln char needed for ReRix protocol
		//outbuflen = cnt; //*outbuflen = cnt;
		
		return new String(outbuf, 0, cnt);
	}
	
	public static byte[] uudecode(String inStr) //unsigned char* uudecode(unsigned char* inbuf, int inbuflen, int* outbuflen)
	{
		char[] inbuf = inStr.toCharArray();
		
		int i=0, cnt=0, mod=0, outbufsize = (inbuf.length);

		//unsigned char *outbuf = (unsigned char *) malloc(outbufsize);
		//initBufToBinZeros((char*)outbuf, outbufsize);
		byte[] outbuf = new byte[outbufsize];

		byte var = 0; //BYTE var = 0;

		char info=0, val = 0; //unsigned char info=0, val=0;

		for (i=0; i<inbuf.length; i++)
		{
			mod = i%4;

			info = (char) ((byte)inbuf[i] - (byte) 32); //info = (unsigned char) ((BYTE)inbuf[i] - (BYTE) 32);

			switch (mod)
			{
				case 0:
					val = (char) (((byte)info & 0x3F) << 2); //val = (unsigned char) (((BYTE)info & hex3f) << 2);
					break;
				case 1:
					val |= (char) (((byte)info >> 4) & 0x03); //val |= (unsigned char) (((BYTE)info >> 4) & hex03);
					outbuf[cnt++] = (byte) val;
					val = (char) (((byte)info & 0x0f) <<4); //val = (unsigned char) (((BYTE)info & hex0f) <<4);
					break;
				case 2:
					val |= (char) (((byte)info >> 2) & 0x0f); //val |= (unsigned char) (((BYTE)info >> 2) & hex0f);
					outbuf[cnt++] =(byte) val;
					val = (char) (((byte)info & 0x03) << 6); //val = (unsigned char) (((BYTE)info & hex03) << 6);
					break;
				case 3:
					val |= (char) ((byte)info & 0x3F); //val |= (unsigned char) ((BYTE)info & hex3f);
					outbuf[cnt++] = (byte) val;
					break;
			}	

		} 

		//outbuflen = cnt; //*outbuflen = cnt;
		byte[] outBytes = new byte[cnt];
		System.arraycopy(outbuf, 0, outBytes, 0, cnt);
		return outBytes;
	}
}
