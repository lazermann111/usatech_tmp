package com.usatech.util;

public interface Cachable
{
	public void onPurge();
}
