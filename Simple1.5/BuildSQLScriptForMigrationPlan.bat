set LIB=../ThirdPartyJavaLibraries/lib
set OPTS=-Duser.name=%USERNAME% -Drelease=R26 -Dkb.page.prefix=EdgeServer -Dfile.prefix=D:/Development/workspace/DatabaseScripts/edge_implementation -Djavax.net.ssl.trustStore=truststore.jk -Djavax.net.ssl.trustStorePassword=usatech
java -cp "bin;%LIB%/commons-codec-1.3.jar;%LIB%/commons-httpclient-3.1.jar;%LIB%/commons-logging-1.1.1.jar;%LIB%/log4j-1.2.15.jar" %OPTS% simple.tools.BuildSQLScriptForMigrationPlan %*
