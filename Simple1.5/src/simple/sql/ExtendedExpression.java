package simple.sql;

import java.util.Set;

import simple.results.Results.Aggregate;

public interface ExtendedExpression extends Expression {
	public Set<Expression> getInnerExpressions() ;
	public Set<Aggregate> getAggregates() ;
}
