package simple.sql;

import java.sql.Types;

public class ArraySQLType extends SQLType {
	protected SQLType componentType;

	public ArraySQLType(SQLType componentType) {
		super(Types.ARRAY);
		this.componentType = componentType;
	}

	public ArraySQLType(String typeName, SQLType componentType) {
		super(Types.ARRAY, typeName);
		this.componentType = componentType;
	}

	public ArraySQLType(String typeName, Integer precision, Integer scale, SQLType componentType) {
		super(Types.ARRAY, typeName, precision, scale);
		this.componentType = componentType;
	}

	@Override
	public void setTypeCode(int sqlType) {
		if(sqlType != Types.ARRAY)
			throw new UnsupportedOperationException("The typeCode may not be changed");
		else 
			super.setTypeCode(sqlType);
	}

	public SQLType getComponentType() {
		return componentType;
	}

	public void setComponentType(SQLType componentType) {
		this.componentType = componentType;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(getTypeName() == null || "ARRAY".equalsIgnoreCase(getTypeName())) {
    		sb.append("ARRAY");
	        if(getPrecision() != null) {
	        	sb.append('(').append(getPrecision()).append(')');
	        }
	        if(getComponentType() != null)
	        	sb.append(':').append(getComponentType());
    	} else { // its a UDF
    		sb.append(getTypeName());
    	}
		return sb.toString();
	}	
}
