package simple.sql;

import java.util.Set;

public interface Aliaser {
	public String getAlias(Table table);

	public void reset();

	public void retainOnly(Set<Table> retain);
}
