/*
 * Created on Oct 12, 2005
 *
 */
package simple.sql;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

public class Join implements Cloneable, Serializable {
    private static final long serialVersionUID = 5915730710562346880L;
	protected Table table1;
    protected Table table2;
    protected String expression;
    protected String dataSourceName;
    protected int order;
    protected JoinType joinType = JoinType.INNER;
	protected JoinCardinality joinCardinality = JoinCardinality.ONE_TO_ONE;
    protected Collection<Table> additionalTables;
    protected Expression expressionObject;
    
    public Join() {
        super();
    }
    public String getExpression() {
        return expression;
    }
    public void setExpression(String expression) {
        if(expression != null) expression = expression.trim();
        this.expression = expression;
    }
    public Table getTable1() {
        return table1;
    }
    public void setTable1(Table table1) {
        if(table1 != null && table1.getDataSourceName().length() == 0)
            table1.setDataSourceName(getDataSourceName());
		Table old = this.table1;
        this.table1 = table1;
		if(expressionObject != null && old != null && table1 != null && !old.equals(table1)) {
			expressionObject.replaceTable(old, table1);
			setExpression(expressionObject.toString());
		}
    }
    public Table getTable2() {
        return table2;
    }
    public void setTable2(Table table2) {
        if(table2 != null && table2.getDataSourceName().length() == 0)
            table2.setDataSourceName(getDataSourceName());
		Table old = this.table2;
        this.table2 = table2;
		if(expressionObject != null && old != null && table2 != null && !old.equals(table2)) {
			expressionObject.replaceTable(old, table2);
			setExpression(expressionObject.toString());
		}
    }
    public String getDataSourceName() {
        return dataSourceName;
    }
    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }
    /**
     * @return Returns the order.
     */
    public int getOrder() {
        return order;
    }
    /**
     * @param order The order to set.
     */
    public void setOrder(int order) {
        this.order = order;
    }
    public void readXML(ObjectBuilder builder) throws ConvertException {
        setDataSourceName(builder.get("dataSource", String.class));
        setExpression(builder.get("expression", String.class));
        setJoinType(builder.get("type", JoinType.class));
		setJoinCardinality(builder.get("cardinality", JoinCardinality.class));
        setOrder(builder.getPosition());
        ObjectBuilder[] tabs = builder.getSubNodes("table");
        if(tabs == null || tabs.length == 0) return;
        setTable1(tabs[0].read(Table.class));
        if(tabs.length == 1) return;
        setTable2(tabs[1].read(Table.class));
    }
    
    public void writeXML(String tagName, XMLBuilder builder) throws SAXException {
        Map<String,String> atts = new LinkedHashMap<String, String>();
        if(getDataSourceName() != null && getDataSourceName().trim().length() > 0)
        	atts.put("dataSource", getDataSourceName());
        atts.put("expression", getExpression());
        if(getJoinType() != JoinType.INNER) {
            atts.put("type", getJoinType().toString());
        }
		atts.put("cardinality", getJoinCardinality().toString());
        if(tagName == null) tagName = "join";
        builder.elementStart(tagName, atts);
        if(getTable1() != null) {
            atts.clear();
            getTable1().addXMLAttributes(atts);
            builder.elementStart("table", atts); 
            builder.elementEnd("table");
        }
        if(getTable2() != null) {
            atts.clear();
            getTable2().addXMLAttributes(atts);
            builder.elementStart("table", atts);            
            builder.elementEnd("table");
        }
        builder.elementEnd(tagName);
    }

    public Join clone() {
        try {
			Join clone = (Join) super.clone();
			if(clone.expressionObject != null)
				clone.expressionObject = expressionObject.copy();
			return clone;
        } catch(CloneNotSupportedException e) {
            return null;
        }
    }
    /**
     * @return Returns the joinType.
     */
    public JoinType getJoinType() {
        return joinType;
    }
    /**
     * @param joinType The joinType to set.
     */
    public void setJoinType(JoinType joinType) {
		if(joinType == null)
			joinType = JoinType.INNER;
        this.joinType = joinType;
    }
    
    @Override
    public boolean equals(Object obj) {
    	if(!(obj instanceof Join))
    		return false;
		if(obj == this)
			return true;
    	Join other = (Join)obj;
    	return ConvertUtils.areEqual(getExpression(), other.getExpression())
    			&& ConvertUtils.areEqual(getTable1(), other.getTable1())
    			&& ConvertUtils.areEqual(getTable2(), other.getTable2())
    			&& getJoinType() == other.getJoinType()
    			&& getJoinCardinality() == other.getJoinCardinality();
    }
    
    @Override
    public int hashCode() {
    	return SystemUtils.addHashCodes(0, getTable1(), getTable2(), getExpression());
    }

    @Override
    public String toString() {
        return super.toString() + " (" + getExpression() + ")";
    }
	public Collection<Table> getAdditionalTables() {
		return additionalTables;
	}
	public void setAdditionalTables(Collection<Table> additionalTables) {
		this.additionalTables = additionalTables;
	}
	public Expression getExpressionObject() {
		return expressionObject;
	}
	public void setExpressionObject(Expression expressionObject) {
		this.expressionObject = expressionObject;
	}

	public JoinCardinality getJoinCardinality() {
		return joinCardinality;
	}

	public void setJoinCardinality(JoinCardinality joinCardinality) {
		if(joinCardinality == null)
			joinCardinality = JoinCardinality.ONE_TO_ONE;
		this.joinCardinality = joinCardinality;
	}

	public Join getReverse() {
		Join copy = clone();
		copy.setTable1(getTable2());
		copy.setTable2(getTable1());
		copy.setJoinType(getJoinType().getReverse());
		copy.setJoinCardinality(getJoinCardinality().getReverse());
		return copy;
	}
}
