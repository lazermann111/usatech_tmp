/**
 * 
 */
package simple.sql;

import java.sql.SQLException;

import simple.db.DataLayerException;

public interface ForeignKeyInfo {
	public ColumnInfo[] getForeignColumns() throws SQLException, DataLayerException ;
	public ColumnInfo[] getReferringColumns() ;
	public String getName() ;	
}