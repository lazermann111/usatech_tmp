package simple.sql;

import java.util.Map;

public class InitialsAliaser extends AbstractAliaser {
	public InitialsAliaser(Map<Table, String> tables) {
		super(tables);
	}

	protected String generateAlias(Table table) {
		StringBuilder initials = new StringBuilder();
		boolean first = true;
		String name = table.getTableName();
		for(int i = 0; i < name.length(); i++) {
			char ch = name.charAt(i);
			if(Character.isLetter(ch)) {
				if(first) {
					initials.append(Character.toLowerCase(ch));
					first = false;
				}
			} else if(Character.isDigit(ch)) {
				if(initials.length() > 0) {
					initials.append(ch);
					first = false;
				}
			} else
				first = true;
		}
		String alias = initials.toString();
		if(!aliases.containsKey(alias))
			return alias;
		initials.append("_");
		int pos = initials.length();
		for(int i = 1;; i++) {
			initials.setLength(pos);
			initials.append(i);
			alias = initials.toString();
			if(!aliases.containsKey(alias))
				return alias;
		}
	}
}
