package simple.sql;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public abstract class AbstractAliaser implements Aliaser {
	protected final Map<String, Table> aliases = new HashMap<String, Table>();
	protected final Map<Table, String> tables;

	public AbstractAliaser(Map<Table, String> tables) {
		this.tables = tables;
	}

	@Override
	public String getAlias(Table table) {
		String alias = tables.get(table);
		if(alias == null) {
			alias = generateAlias(table);
			tables.put(table, alias);
			aliases.put(alias, table);
		}
		return alias;
	}

	protected abstract String generateAlias(Table table);

	@Override
	public void reset() {
		aliases.clear();
		for(Map.Entry<Table, String> entry : tables.entrySet())
			entry.setValue(null);
	}

	public void retainOnly(Set<Table> retain) {
		for(Iterator<Map.Entry<Table, String>> iter = tables.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<Table, String> entry = iter.next();
			if(!retain.contains(entry.getKey())) {
				iter.remove();
				aliases.remove(entry.getValue());
			}
		}
	}
}
