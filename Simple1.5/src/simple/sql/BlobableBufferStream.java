package simple.sql;

import java.sql.Blob;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import simple.io.HeapBufferStream;

public class BlobableBufferStream extends HeapBufferStream {
	protected Blob blob;
	public BlobableBufferStream(boolean resettable) {
		super(resettable);
	}

	public BlobableBufferStream() {
		super();
	}

	public BlobableBufferStream(int blockSize, int capacity) {
		super(blockSize, capacity);
	}

	public BlobableBufferStream(int blockSize, int capacity, boolean resettable) {
		super(blockSize, capacity, resettable);
	}

	public Blob getBlob() {
		if(blob == null)
			try {
				blob = new SerialBlob(buffer) {
					private static final long serialVersionUID = 4110816547698153721L;

					@Override
					public void free() throws SQLException {
						// Don't throw exception
					}
				};
			} catch(SerialException e) {
				// Can never happen
			} catch(SQLException e) {
				// Can never happen
			}
		return blob;
	}
}
