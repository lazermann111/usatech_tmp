/**
 *
 */
package simple.sql;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.sql.Blob;
import java.sql.SQLException;

import simple.io.RandomAccessFileInputStream;
import simple.io.RandomAccessFileOutputStream;
import simple.io.SliceInputStream;

/**
 * @author Brian S. Krug
 *
 */
public class FileBlob implements Blob {
	protected final File file;
	protected final boolean deleteOnFree;

    public FileBlob(File file, boolean deleteOnFree) {
    	this.file = file;
    	this.deleteOnFree = deleteOnFree;
    }
    public InputStream getBinaryStream() throws SQLException {
    	try {
            return new FileInputStream(file);
        } catch(FileNotFoundException e) {
            throw new SQLException("File '" + file.getAbsolutePath() + "' does not exist", e);
        }
    }
    public byte[] getBytes(long pos, int length) throws SQLException {
        if(pos < 1)
        	throw new SQLException("Invalid pos " + pos);
        if(length < 0)
        	throw new SQLException("Invalid length " + length);
        int l = (int)Math.min(file.length() - pos + 1, length);
    	byte[] b = new byte[l];
		try {
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			try {
				raf.seek(pos - 1);
				raf.read(b);
			} finally {
				raf.close();
			}
        	return b;
        } catch(IOException ioe) {
            throw new SQLException("Could not read file: " + ioe.getMessage());
        }
    }
    public InputStream getBinaryStream(long pos, long length) throws SQLException {
    	try {
    		RandomAccessFile raf = new RandomAccessFile(file, "r");
        	raf.seek(pos-1);
        	return new SliceInputStream(new RandomAccessFileInputStream(raf), 0, length);
		} catch(IOException e) {
			throw new SQLException("IOException occured: " + e.getMessage(), e);
		}
    }
    public void free() throws SQLException {
    	if(deleteOnFree)
    		file.delete();
    }
    public long length() throws SQLException {
        return file.length();
    }
    public long position(byte[] pattern, long start) throws SQLException {
        return -1; //TODO: implement this
    }
    public long position(Blob pattern, long start) throws SQLException {
        return -1; //TODO: implement this
    }
    public java.io.OutputStream setBinaryStream(long pos) throws SQLException {
    	try {
    		RandomAccessFile raf = new RandomAccessFile(file, "rw");
        	raf.seek(pos-1);
        	return new RandomAccessFileOutputStream(raf);
		} catch(IOException e) {
			throw new SQLException("IOException occured: " + e.getMessage(), e);
		}
    }
    public int setBytes(long pos, byte[] bytes) throws SQLException {
        return setBytes(pos, bytes, 0, bytes.length);
    }
    public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
    	try {
    		RandomAccessFile raf = new RandomAccessFile(file, "rw");
			try {
				raf.seek(pos - 1);
				raf.write(bytes, offset, len);
			} finally {
				raf.close();
			}
        	return len;
		} catch(IOException e) {
			throw new SQLException("IOException occured: " + e.getMessage(), e);
		}
    }
    public void truncate(long len) throws SQLException {
    	try {
    		RandomAccessFile raf = new RandomAccessFile(file, "rw");
			try {
				raf.setLength(len);
			} finally {
				raf.close();
			}
		} catch(IOException e) {
			throw new SQLException("IOException occured: " + e.getMessage(), e);
		}
    }
}
