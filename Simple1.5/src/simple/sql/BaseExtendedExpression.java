package simple.sql;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;

import simple.results.Results.Aggregate;
import simple.util.CollectionUtils;

public class BaseExtendedExpression extends BaseMutableExpression implements ExtendedExpression {
	protected Set<Expression> innerExpressions;
	protected Set<Aggregate> aggregates;
	
	public BaseExtendedExpression(List<Object> parts, Set<Expression> innerExpressions, Set<Aggregate> aggregates) {
		super(parts);
		this.innerExpressions = innerExpressions;
		this.aggregates = aggregates;
	}

	public Set<Aggregate> getAggregates() {
		return aggregates;
	}

	public Set<Expression> getInnerExpressions() {
		return innerExpressions;
	}

	@Override
	public void invalidate() {
		for(Expression exp : innerExpressions)
			exp.invalidate();
		super.invalidate();
	}
	@Override
	public BaseExtendedExpression copy() {
		try {
			return new BaseExtendedExpression(parts, CollectionUtils.deepCopy(innerExpressions, null), CollectionUtils.deepCopy(aggregates, null));
		} catch(InstantiationException e) {
			throw new IllegalStateException("BaseExtendedExpression could not be cloned");
		} catch(IllegalAccessException e) {
			throw new IllegalStateException("BaseExtendedExpression could not be cloned");
		} catch(NoSuchMethodException e) {
			throw new IllegalStateException("BaseExtendedExpression could not be cloned");
		} catch(InvocationTargetException e) {
			throw new IllegalStateException("BaseExtendedExpression could not be cloned");
		}
	}
}
