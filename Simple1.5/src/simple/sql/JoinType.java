/*
 * Created on Nov 15, 2005
 *
 */
package simple.sql;

public enum JoinType {
    INNER,
    LEFT,
    RIGHT,
    FULL,
    CROSS, 
    OPTIONAL;

	public JoinType getReverse() {
		switch(this) {
			case RIGHT:
				return JoinType.LEFT;
			case LEFT:
				return JoinType.RIGHT;
			case FULL:
				return OPTIONAL;
			case OPTIONAL:
				return FULL;
			default:
				return this;

		}
	}

	public JoinType getCombination(JoinType next) {
		if(next == JoinType.CROSS || this == JoinType.CROSS)
			return next;
		if(this == JoinType.INNER)
			return next;
		else if(this == next || next == JoinType.INNER)
			return this;
		else if(this == JoinType.LEFT && next == JoinType.RIGHT)
			return JoinType.OPTIONAL;
		else if(this == JoinType.RIGHT && next == JoinType.LEFT)
			return JoinType.FULL;
		else if(this == JoinType.OPTIONAL || next == JoinType.OPTIONAL)
			return JoinType.OPTIONAL;
		else if(this == JoinType.FULL || next == JoinType.FULL)
			return JoinType.FULL;
		else
			throw new IllegalStateException("Should never get here with joinType=" + this + " and join's type=" + next);
	}
}
