package simple.sql;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

public class ByteArrayBlob implements Blob {
	private byte[] content;

	public ByteArrayBlob(byte[] content) {
		this.content = content;
	}

	public InputStream getBinaryStream() throws SQLException {
		return new ByteArrayInputStream(content);
	}

	public byte[] getBytes(long pos, int length) throws SQLException {
		if(pos > Integer.MAX_VALUE)
			throw new SQLException("pos is out of range");
		byte[] b = new byte[length];
		System.arraycopy(content, (int) pos, b, 0, length);
		return b;
	}

	public InputStream getBinaryStream(long pos, long length) throws SQLException {
		if(pos > Integer.MAX_VALUE)
			throw new SQLException("pos is out of range");
		if(length > Integer.MAX_VALUE)
			throw new SQLException("length is out of range");
		return new ByteArrayInputStream(content, (int) pos, (int) length);
	}

	public void free() throws SQLException {
		content = null;
	}

	public long length() throws SQLException {
		return content.length;
	}

	public long position(byte[] pattern, long start) throws SQLException {
		return -1;
	}

	public long position(Blob pattern, long start) throws SQLException {
		return -1;
	}

	public java.io.OutputStream setBinaryStream(long pos) throws SQLException {
		throw new UnsupportedOperationException("This method is not supported");
	}

	public int setBytes(long pos, byte[] bytes) throws SQLException {
		return setBytes(pos, bytes, 0, bytes.length);
	}

	public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
		if(pos > Integer.MAX_VALUE)
			throw new SQLException("pos is out of range");
		int newlen = ((int) pos) + len;
		if(newlen > content.length) {
			byte[] tmp = new byte[newlen];
			System.arraycopy(content, 0, tmp, 0, (int) pos);
			this.content = tmp;
		}
		System.arraycopy(bytes, offset, content, (int) pos, len);
		return len;
	}

	public void truncate(long len) throws SQLException {
		if(len > content.length)
			throw new SQLException("len is greater than the current length");
		byte[] tmp = new byte[(int) len];
		System.arraycopy(content, 0, tmp, 0, tmp.length);
		content = tmp;
	}
}
