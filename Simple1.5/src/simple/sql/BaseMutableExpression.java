package simple.sql;

import java.util.List;

public class BaseMutableExpression extends BaseExpression implements MutableExpression {

	public BaseMutableExpression() {
		super();
	}

	public BaseMutableExpression(List<Object> parts) {
		super(parts);
	}
	
	public void append(Expression expression) {
		if(expression instanceof BaseExpression) {
			BaseExpression ae = (BaseExpression) expression;
			appendParts(ae.parts.toArray(new Object[ae.parts.size()]));
			sql.append(ae.sql);
		} else {
			appendParts(expression);
			sql.append(expression.toString());
		}
	}
	
	public void insert(Expression expression) {
		if(expression instanceof BaseExpression) {
			BaseExpression ae = (BaseExpression) expression;
			prependParts(ae.parts.toArray(new Object[ae.parts.size()]));
			sql.insert(0,ae.sql);
		} else {
			prependParts(expression);
			sql.insert(0,expression.toString());
		}
	}	

	@Override
	public BaseMutableExpression copy() {
		return new BaseMutableExpression(parts);
	}
}
