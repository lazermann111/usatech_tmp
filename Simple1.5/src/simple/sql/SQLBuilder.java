/*
 * Created on Nov 16, 2004
 *
 */
package simple.sql;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Parameter;
import simple.io.Log;
import simple.lang.Holder;
import simple.results.Results.Aggregate;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveComparator;
import simple.util.CollectionUtils;
import simple.util.IncludingSet;
import simple.util.MapBackedSet;

/**
 * @author bkrug
 *
 */
public class SQLBuilder {
    private static Log log = Log.getLog();

    protected static final Set<String> defaultAggFunctions = new TreeSet<String>(new CaseInsensitiveComparator<String>());
    static {
    	defaultAggFunctions.add("COUNT");
    	defaultAggFunctions.add("SUM");
    	defaultAggFunctions.add("MAX");
    	defaultAggFunctions.add("MIN");
    	defaultAggFunctions.add("AVG");
    }
    //protected Map<Table,Map<Table,Join>> sourceJoins;
    protected Comparator<? super String> comparator;
    protected Comparator<? super Table> tableComparator;
    protected Map<String,Set<String>> aggFunctionsByDB;
    protected boolean caseSensitive;
    protected Map<Table,Map<Table,JoinPath>> joinPaths;
    protected Map<Table,Join> extraJoins;
	protected Map<Table, Expression> uniqueExpressions;

    protected class AggregateFinder implements TableColumnHandler {
        public boolean containsAgg = false;
        protected Set<String> aggFunctions;
        public AggregateFinder(DatabaseMetaData metaData) {
        	aggFunctions = getAggregateFunctions(metaData);
        }
        public void handleTableColumn(String s, int wordStart, int lastDot, int wordEnd) {
        }
        public void handleFunction(String s, int wordStart, int lastDot, int wordEnd, int beginParen) {
            if(wordStart >= 0 && wordEnd > 0) {
                String function = s.substring(wordStart, wordEnd).toUpperCase();
                log.debug("Checking function '" + function + "' to see if it is an aggregate");
                if(aggFunctions.contains(function)) containsAgg = true;
            }
        }
		public void handleBeginParen(String s, int pos) {
		}
		public void handleEndParen(String s, int pos) {
		}

		public void handleParam(String s, int wordStart, int wordEnd, int typeStart, int typeEnd) {
		}
    }

    public static interface AggregateParser {
    	public Aggregate isAggregateFunction(String sql, int functionStart, int functionEnd, int parenStart, Holder<Integer> nextPos) ;
    }
    public static class StraightAggregateParser implements AggregateParser {
    	public Aggregate isAggregateFunction(String sql, int functionStart, int functionEnd, int parenStart, Holder<Integer> nextPos) {
    		String function = sql.substring(functionStart, functionEnd);
    		Aggregate agg;
    		try {
        		agg = ConvertUtils.convert(Aggregate.class, function);
        	} catch(ConvertException e) {
        		agg = null;
        	}
        	if(agg != null && nextPos != null) {
        		nextPos.setValue(parenStart + 1);
        	}
        	return agg;
		}
    }
    public static class RegexAggregateParser implements AggregateParser {
    	protected final Map<String, Map<Pattern, Aggregate>> regexs = new HashMap<String, Map<Pattern,Aggregate>>();
		public Aggregate isAggregateFunction(String sql, int functionStart, int functionEnd, int parenStart, Holder<Integer> nextPos) {
			Map<Pattern,Aggregate> patterns = regexs.get(sql.substring(functionStart, functionEnd).toUpperCase());
			if(patterns == null)
				return null;
			for(Map.Entry<Pattern, Aggregate> entry : patterns.entrySet()) {
				if(entry.getKey() == null) {
					if(nextPos != null) nextPos.setValue(parenStart + 1);
					return entry.getValue();
				}
				Matcher matcher = entry.getKey().matcher(sql);
				matcher.region(functionStart, sql.length());
				if(matcher.lookingAt()) {
					if(nextPos != null) nextPos.setValue(matcher.end());
					return entry.getValue();
				}
			}
			return null;
		}
		public void registerAggregate(Aggregate aggregate, String function, Pattern regex) {
			function = function.toUpperCase();
			Map<Pattern,Aggregate> patterns = regexs.get(function);
			if(patterns == null) {
				patterns = new LinkedHashMap<Pattern, Aggregate>(); //retain order so that first registered are tried first
				regexs.put(function, patterns);
			}
			patterns.put(regex, aggregate);
		}
		public void registerAggregate(Aggregate aggregate) {
			registerAggregate(aggregate, aggregate.toString(), null);
		}
    }

    /** Holds a map of the aliases used and the tables they alias */
    //protected SortedMap aliases = new java.util.TreeMap(comparator);

    /* we need to track the order that joins are added to the Maps at
       each entry of the sourceJoins Map so we will use LinkedHashMap for
       those and a plain HashMap for sourceJoins (since we never iterate on it)
       We will upper case table names if case insensitive
    public SQLBuilder(Comparator comparator) {
        this.comparator = comparator;
        //this.sourceJoins = new java.util.TreeMap(comparator);
        this.sourceJoins = new java.util.TreeMap(comparator);
        this.aggFunctions = new java.util.TreeSet(comparator);
        //add agg functions
        aggFunctions.addAll(Arrays.asList(new String[] {
        	"COUNT", "SUM", "MAX", "MIN", "AVG"
        }));
    }
    */
    public SQLBuilder(Comparator<? super String> comparator) {
        this.comparator = comparator;
        caseSensitive = (comparator.compare("a", "A") != 0);
        this.tableComparator = new Comparator<Table>() {
            public int compare(Table o1, Table o2) {
                if(o1 == null) {
                    if(o2 == null) return 0;
                    else return -1;
                } else if(o2 == null) return 1;
				// return SQLBuilder.this.comparator.compare(o1.toString(), o2.toString());
				return o1.compareTo(o2);
            }
        };
        // we need to track the order that joins are added to the Maps at
        // each entry of the sourceJoins Map so we will use LinkedHashMap for
        // those and a plain HashMap for sourceJoins (since we never iterate on it)
        // We will upper case table names if case insensitive
        //this.sourceJoins = new java.util.TreeMap(comparator);
        //this.sourceJoins = new TreeMap<Table,Map<Table,Join>>(tableComparator);
        this.joinPaths = new TreeMap<Table,Map<Table,JoinPath>>(tableComparator);
        this.extraJoins = new TreeMap<Table,Join>(tableComparator);
		this.uniqueExpressions = new TreeMap<Table, Expression>(tableComparator);
        this.aggFunctionsByDB = new HashMap<String,Set<String>>();//new TreeSet<String>(comparator);
        //add agg functions
        InputStream in = getClass().getResourceAsStream("SQLBuilder.properties");
        if(in == null) {
        	log.error("Could not find SQLBuilder.properties file");
        } else {
	        Properties props = new Properties();
	        try {
				props.load(in);
				for(Map.Entry<Object,Object> entry : props.entrySet()) {
		        	String name = (String)entry.getKey();
		        	String value = (String) entry.getValue();
		        	if(name.startsWith("aggregates.")) {
		        		String dbName = name.substring(11);
		        		Set<String> aggs = new TreeSet<String>(comparator);
		        		aggs.addAll(Arrays.asList(StringUtils.split(value,StringUtils.STANDARD_DELIMS, false)));
		        		aggFunctionsByDB.put(dbName.toUpperCase(), aggs);
		        	}
		        }
			} catch (IOException e) {
				log.error("Could not load SQLBuilder.properties from inputstream", e);
			}
        }
        if(!aggFunctionsByDB.containsKey("default")) {
        	Set<String> aggs = new TreeSet<String>(comparator);
        	aggs.addAll(Arrays.asList(new String[] {
                	"COUNT", "SUM", "MAX", "MIN", "AVG"
                }));
        	aggFunctionsByDB.put("default", aggs);
        }
    }
    public Set<String> getAggregateFunctions(DatabaseMetaData metaData) {
    	Set<String> aggFunctions;
        if(metaData != null) {
    		try {
				String product = metaData.getDatabaseProductName().toUpperCase();
        		//String version = metaData.getDatabaseProductVersion().toUpperCase();
        		int major = metaData.getDatabaseMajorVersion();
        		aggFunctions = aggFunctionsByDB.get(product + "." + major);
        		if(aggFunctions == null)
        			aggFunctions = aggFunctionsByDB.get(product);
        		if(aggFunctions == null)
        			aggFunctions = aggFunctionsByDB.get("default");
			} catch (SQLException e) {
				log.warn("While getting database product name", e);
    			aggFunctions = aggFunctionsByDB.get("default");
			}
    	} else {
			aggFunctions = aggFunctionsByDB.get("default");
    	}
        return aggFunctions;
    }

    /** Finds the shortest join path from one of the tables in the Set of unjoined tables to
     * one of the tables in the Set of anchor tables. If a join is found the tables
     * Map of the XAgg object is modified accordingly with any new tables and the joins
     * are added to the joins Map provided. The number of joins found is returned.
     * @param anchors The Set of anchor tables
     * @param unjoined The Set of unjoined tables
     * @param agg The XAgg object for which joins are sought
     * @param joins The Map into which to add any new joins
     * @return The number of joins found
     */
	protected int findOneJoin(Set<Table> anchors, Set<Table> unjoined, Map<Table, String> tables, Map<String, Join> joins, Aliaser aliaser, DatabaseMetaData metaData) {
        //first find the shortest path
        //Set<Table> excludes = new TreeSet<Table>(tableComparator);
        //XXX: WHY WERE WE DOING THIS???: excludes.addAll(unjoined);
        //List<Join> joinPath = findJoinPath(unjoined, excludes, anchors);
		List<Join> joinPath = findBestJoinList(CollectionUtils.singleValueMap(unjoined, (JoinStrand) null), anchors);

        if(joinPath == null) return 0; //no path found :(

        //then add the new table(s) in and adjust everything accordingly
        //the first one is already added so skip it and the last one is the visible table that it finally links to
        for(Join join : joinPath) {
			if(!tables.containsKey(join.getTable1()) && checkTable(join.getTable1(), metaData)) {
				String alias = aliaser.getAlias(join.getTable1());
				tables.put(join.getTable1(), alias);
            }
			if(!tables.containsKey(join.getTable2()) && checkTable(join.getTable2(), metaData)) {
				String alias = aliaser.getAlias(join.getTable2());
				tables.put(join.getTable2(), alias);
            }
        }

        // then add the joins
        log.info("Found join through " + joinPath);
        for(Join join : joinPath) {
            unjoined.remove(join.getTable1());
            unjoined.remove(join.getTable2());
            anchors.add(join.getTable1());
            anchors.add(join.getTable2());
            String viewName = (join.getTable1().hasViewName() ? join.getTable1().getViewName() : join.getTable2().getViewName());
            //find additional tables
            Collection<Table> addTabs = findTables(join.getExpression());
            for(Table addtab : addTabs) {
            	if(addtab.getDataSourceName() == null || addtab.getDataSourceName().trim().length() == 0)
            		addtab.setDataSourceName(join.getDataSourceName());
				if(isAdditionalTable(addtab, tables, viewName, metaData)) {
                    log.info("WARNING: Additional table " + addtab + " found in join.");
                    //warnings.add("WARNING: Additional table " + addtab + " found in join.");
					String alias = aliaser.getAlias(addtab);
					tables.put(addtab, alias);
                    unjoined.add(addtab);
                }
            }
			joins.put(replaceAliases(join.getExpression(), tables, viewName, join.getDataSourceName()), join);
        }
        return joinPath.size();
    }

	/**
	 * Finds the shortest join path from one of the tables in the Set of unjoined tables to
	 * one of the tables in the Set of anchor tables. If a join is found the tables
	 * Map of the XAgg object is modified accordingly with any new tables and the joins
	 * are added to the joins Map provided. The number of joins found is returned.
	 * 
	 * @param anchors
	 *            The Set of anchor tables
	 * @param unjoined
	 *            The Set of unjoined tables
	 * @param agg
	 *            The XAgg object for which joins are sought
	 * @param joins
	 *            The Map into which to add any new joins
	 * @return The number of joins found
	 */
	protected int findOneJoinStrand(Map<Table, JoinStrand> anchors, Set<Table> unjoined, Set<Table> additional, Map<Table, String> tables, Aliaser aliaser, DatabaseMetaData metaData) {
		// first find the shortest path
		List<Join> joinPath = findBestJoinList(anchors, unjoined);

		if(joinPath == null)
			return 0; // no path found :(

		// then add the new table(s) in and adjust everything accordingly
		// the first one is already added so skip it and the last one is the visible table that it finally links to
		for(Join join : joinPath) {
			if(!tables.containsKey(join.getTable1()) && checkTable(join.getTable1(), metaData)) {
				String alias = aliaser.getAlias(join.getTable1());
				tables.put(join.getTable1(), alias);
			}
			if(!tables.containsKey(join.getTable2()) && checkTable(join.getTable2(), metaData)) {
				String alias = aliaser.getAlias(join.getTable2());
				tables.put(join.getTable2(), alias);
			}
		}

		// then add the joins
		log.info("Found join through " + joinPath);
		for(Join join : joinPath) {
			String viewName = (join.getTable1().hasViewName() ? join.getTable1().getViewName() : join.getTable2().getViewName());
			// find additional tables
			Collection<Table> addTabs = findTables(join.getExpression());
			addTabs.remove(join.getTable1());
			addTabs.remove(join.getTable2());
			if(!anchors.containsKey(join.getTable1()))
				additional.add(join.getTable1());
			if(!anchors.containsKey(join.getTable2()))
				additional.add(join.getTable2());

			boolean hasAdditional = false;
			boolean hasUnjoined = false;
			for(Table addtab : addTabs) {
				if(addtab.getDataSourceName() == null || addtab.getDataSourceName().trim().length() == 0)
					addtab.setDataSourceName(join.getDataSourceName());
				if(isAdditionalTable(addtab, tables, viewName, metaData)) {
					log.info("WARNING: Additional table " + addtab + " found in join.");
					// warnings.add("WARNING: Additional table " + addtab + " found in join.");
					// we must include joins on additional tables first
					String alias = aliaser.getAlias(addtab);
					tables.put(addtab, alias);
					unjoined.add(addtab);
					additional.add(addtab);
					hasAdditional = true;
				} else if(unjoined.contains(addtab)) {
					log.info("Unjoined table " + addtab + " found in join.");
					hasUnjoined = true;
					additional.add(addtab);
				}
			}
			if(hasAdditional)
				return findOneJoinStrand(anchors, unjoined, additional, tables, aliaser, metaData);
			JoinStrand js1 = anchors.get(join.getTable1());
			JoinStrand js2 = anchors.get(join.getTable2());
			JoinStrand parent;
			JoinStrand child;
			if(js1 != null) {
				if(js2 == null) {
					js2 = new JoinStrand(join.getTable2(), join.getJoinType(), join.getJoinCardinality(), join.getExpressionObject());
					anchors.put(join.getTable2(), js2);
					unjoined.remove(join.getTable2());
				} else if(js2.getType() == null) {
					JoinStrand old = js2;
					js2 = new JoinStrand(join.getTable2(), join.getJoinType(), join.getJoinCardinality(), join.getExpressionObject());
					old.moveChildrenTo(js2);
					anchors.put(join.getTable2(), js2);
				} else
					continue; // throw new IllegalStateException("Both tables (" + join.getTable1() + ", " + join.getTable2() + ") in join are already in anchors with a previously defined join");
				parent = js1;
				child = js2;
			} else if(js2 != null) {
				js1 = new JoinStrand(join.getTable1(), join.getJoinType().getReverse(), join.getJoinCardinality().getReverse(), join.getExpressionObject());
				anchors.put(join.getTable1(), js1);
				unjoined.remove(join.getTable1());
				parent = js2;
				child = js1;
			} else {
				js1 = new JoinStrand(join.getTable1(), null, JoinCardinality.ONE_TO_ONE, null);
				js2 = new JoinStrand(join.getTable2(), join.getJoinType(), join.getJoinCardinality(), join.getExpressionObject());
				anchors.put(join.getTable1(), js1);
				unjoined.remove(join.getTable1());
				anchors.put(join.getTable2(), js2);
				unjoined.remove(join.getTable2());
				parent = js1;
				child = js2;
			}
			if(hasUnjoined) {
				Set<Table> addUnjoined = new IncludingSet<Table>(unjoined, addTabs);
				int r;
				do {
					r = findOneJoinStrand(anchors, addUnjoined, additional, tables, aliaser, metaData);
				} while(r > 0 && !addUnjoined.isEmpty());
				// TODO: handle when r == 0
			}
			addJoinStrand(parent, child, anchors, addTabs);
		}
		return joinPath.size();
	}

	protected void addJoinStrand(JoinStrand parent, JoinStrand child, Map<Table, JoinStrand> anchors, Collection<Table> addTabs) {
		parent.appendChild(child);
		// /*
		if(addTabs.isEmpty())
			return;
		for(Table addtab : addTabs) {
			JoinStrand addJs = anchors.get(addtab);
			if(addJs != null)
				child.moveAfter(addJs);
		}// */
	}

	protected JoinStrand fixUnmetPrerequisites(JoinStrand startStrand, Map<Table, JoinStrand> anchors) {
		Map<Table, Set<JoinStrand>> unmet = findUnmetPrerequisites(startStrand);
		log.info("Unmet prereqs: " + unmet);
		if(unmet.isEmpty())
			return startStrand;
		for(Map.Entry<Table, Set<JoinStrand>> entry : unmet.entrySet()) {
			JoinStrand addJs = anchors.get(entry.getKey());
			if(addJs != null) {
				for(JoinStrand child : entry.getValue())
					child.moveAfter(addJs);
			}
		}
		unmet = findUnmetPrerequisites(startStrand);
		log.info("AFTER ADJUSTMENT Unmet prereqs: " + unmet);
		if(!unmet.isEmpty()) {
			log.warn("Could not adjust joins so all prerequisites are satisfied");
			return null;
		}
		return startStrand;
	}

	protected Map<Table, Set<JoinStrand>> findUnmetPrerequisites(JoinStrand js) {
		Map<Table, Set<JoinStrand>> required = new TreeMap<Table, Set<JoinStrand>>(tableComparator);
		Set<Table> provided = new TreeSet<Table>(tableComparator);
		for(JoinStrand parent = js; parent != null; parent = parent.getParent()) {
			for(JoinStrand prev = parent; prev != null; prev = prev.getPrev())
				provided.add(prev.getTable());
		}
		findUnmetPrerequisites(js, required, provided, new TreeSet<Table>(tableComparator));
		return required;
	}

	protected void findUnmetPrerequisites(JoinStrand js, Map<Table, Set<JoinStrand>> required, Set<Table> parentProvided, Set<Table> currentProvided) {
		if(js.getExpression() != null) {
			Set<Column> cols = js.getExpression().getColumns();
			if(cols != null)
				for(Column col : cols) {
					if(!parentProvided.contains(col.getTable()) && !currentProvided.contains(col.getTable())) {
						Set<JoinStrand> dependents = required.get(col.getTable());
						if(dependents == null) {
							dependents = new HashSet<JoinStrand>(2);
							required.put(col.getTable(), dependents);
						}
						dependents.add(js);
					}
				}
		}
		for(JoinStrand sub : js.getStrands()) {
			currentProvided.add(sub.getTable());
			findUnmetPrerequisites(sub, required, parentProvided, currentProvided);
			currentProvided.remove(sub.getTable());
			parentProvided.add(sub.getTable());
		}
	}

    protected boolean isAdditionalTable(Table addtab, Map<Table,String> tables, Map<Table,String> tabsAliases, Map<Table,String> visibleTables, String viewName, DatabaseMetaData metaData) {
        if(visibleTables.containsKey(addtab) || tables.containsKey(addtab) || !checkTable(addtab, metaData)) return false;
        if(addtab.hasViewName()) {
            //additional table -- fall through
            return true;
        } else if(viewName.length() > 0) {
            //addtab.setViewName(viewName); // XXX: Let's see what happens if we don't change the view name here
            if(visibleTables.containsKey(addtab))
                return false;
            //otherwise -- additional table -- fall through
            return true;
        } else { // both join and additional table are generic (no view names)
            //search for table in visible tables with same base table
            Table vtabMatch = null;
            for(Table vtab : visibleTables.keySet()) {
                if(addtab.equalsBaseTable(vtab)) {
                    if(vtabMatch != null) return true; //additional table -- fall through
                    vtabMatch = vtab;
                }
            }
            if(vtabMatch != null) {
                String alias = tables.get(vtabMatch);
                tabsAliases.put(addtab, alias);
                return false;
            }
            return true;
        }
    }

	protected boolean isAdditionalTable(Table addtab, Map<Table, String> tables, String viewName, DatabaseMetaData metaData) {
		if(tables.containsKey(addtab) || !checkTable(addtab, metaData))
			return false;
		if(addtab.hasViewName()) {
			// additional table -- fall through
			return true;
		} else if(viewName.length() > 0) {
			/*addtab.setViewName(viewName); // XXX: Let's see what happens if we don't change the view name here
			if(tables.containsKey(addtab))
			    return false;*/
			// otherwise -- additional table -- fall through
			return true;
		} else { // both join and additional table are generic (no view names)
			// search for table in visible tables with same base table
			Table vtabMatch = null;
			for(Table vtab : tables.keySet()) {
				if(addtab.equalsBaseTable(vtab)) {
					if(vtabMatch != null)
						return true; // additional table -- fall through
					vtabMatch = vtab;
				}
			}
			return vtabMatch == null;
		}
	}
    
	/**
	 * Finds the shortest join path from one of the tables in the Set of start tables to
	 * one of the tables in the Set of end tables and returns the list of tables in
	 * that join path whose first element is the table from the start table and last
	 * element is the end table.
	 * 
	 * @param startTables
	 *            Set of tables with which to start the join path
	 * @param endTables
	 *            Set of tables with which to end the join path
	 * @return The list of tables in shortest join path
	 */
	/*
	public List<Join> findJoinPath(Set<Table> startTables, Set<Table> endTables) {
	 //return findJoinPath(startTables, new HashSet<Table>(), endTables);
	return findBestJoinList(startTables, endTables);
	}*/

    /** Finds the shortest join path from one of the tables in the Set of start tables to
     * one of the tables in the Set of end tables and returns the list of tables in
     * that join path whose first element is the table from the start table and last
     * element is the end table.
     * @param startTables Set of tables with which to start the join path
     * @param excludeTables Set of tables to not use when trying to find a join path. This is primarily used
     * for the recursion on this method. NOTE: this Set is modified in the course of
     * processing.
     * @param endTables Set of tables with which to end the join path
     * @return The list of tables in shortest join path
     */    /*
    protected List<Join> findJoinPath(Set<Table> startTables, Set<Table> excludeTables, Set<Table> endTables) {
        if(startTables.size() == 0) return null;

        //XXX: For now we had better not modify endTables - but maybe after we assess the implications
        Set<Table> filteredEndTables = new TreeSet<Table>(tableComparator);
        filteredEndTables.addAll(endTables);
        filteredEndTables.removeAll(excludeTables);
        //first, find all direct paths
        Join bestJoin = findBestDirectJoin(startTables, filteredEndTables);
        if(bestJoin != null) {
            List<Join> l = new LinkedList<Join>();
            l.add(bestJoin);
            return l;
        }
        //second, check for a third-party path
        Set<Table> newStartTables = new TreeSet<Table>(tableComparator);
        for(Table tab : startTables) {
            Map<Table, Join> ftabJoins = sourceJoins.get(tab);
            if(ftabJoins != null) newStartTables.addAll(ftabJoins.keySet());
            if(tab.hasViewName()) {
                Table stab = new Table(tab.getDataSourceName(), tab.getSchemaName(), tab.getTableName(), tab.isCaseSensitive());
                ftabJoins = sourceJoins.get(stab);
                if(ftabJoins != null) {
                    for(Table ftab : ftabJoins.keySet()) {
                        if(ftab == null) continue;
                        if(!ftab.hasViewName()) newStartTables.add(new Table(ftab.getDataSourceName(), ftab.getSchemaName(), ftab.getTableName(), tab.getViewName(), ftab.isCaseSensitive())); // XXX: can we use ftab + arr?
                        else if(ftab.getViewName().equalsIgnoreCase(tab.getViewName())) newStartTables.add(ftab);
                    }
                }
            }
        }
        excludeTables.addAll(startTables); // avoid circular processing
        newStartTables.removeAll(excludeTables);
        newStartTables.remove(null); // no blank tables!
        List<Join> l = findJoinPath(newStartTables, excludeTables, endTables);
        if(l == null) return l;

        //now figure out which of the start tables joins to the first in the join path list
        Join join1 = l.get(0);// this is the join through which we have found a third party join
        Join j2 = findBestDirectJoin(startTables, Collections.singleton(join1.getTable1())); // must go from start tables to middle table so that join.table1 and table 2 are correct
        if(j2 != null) {
            l.add(0, j2);
            return l;
        }
        //* XXX: needed?
        if(join1.getTable1().hasViewName()) {
            Table stab1 = new Table(join1.getTable1().getDataSourceName(), join1.getTable1().getSchemaName(), join1.getTable1().getTableName(), join1.getTable1().isCaseSensitive());
            j2 = findBestDirectJoin(startTables, Collections.singleton(stab1));
            if(j2 != null) {
                l.add(0, j2);
                return l;
            }
        }// *//*
        throw new RuntimeException("Processing error; this statement should never run - No Join found for '" + join1.getTable1() + "' amoungst " + startTables);
    }*/
/*
    protected Join findBestDirectJoin(Set<Table> startTables, Set<Table> endTables) {
        Join bestJoin = null;
        for(Table tab : startTables) {
            Map<Table,Join> ftabJoins = sourceJoins.get(tab);
            boolean found = false;
            if(ftabJoins != null) {
                for(Table ftab : endTables) {
                    Join join = ftabJoins.get(ftab);
                    if(join != null) {
                        found = true;
                        if(bestJoin == null || bestJoin.getOrder() > join.getOrder())
                            bestJoin = join;
                    } else if(ftab.hasViewName()) {
                        Table sftab = new Table(ftab.getDataSourceName(), ftab.getSchemaName(), ftab.getTableName(), ftab.isCaseSensitive());
                        join = ftabJoins.get(sftab);
                        if(join != null) {
                            found = true;
                            if(bestJoin == null || bestJoin.getOrder() > join.getOrder()) {
                                //I believe we need to clone the join and replace table2 with the table with the view
                                bestJoin = join.clone();
                                bestJoin.setTable2(ftab);
                            }
                        }
                    }
                }
            }
            if(!found && tab.hasViewName()) {
                Table stab = new Table(tab.getDataSourceName(), tab.getSchemaName(), tab.getTableName(), tab.isCaseSensitive());
                ftabJoins = sourceJoins.get(stab);
                if(ftabJoins != null) {
                    for(Table ftab : endTables) {
                        Join join = ftabJoins.get(ftab);
                        if(join != null && (!ftab.hasViewName() || tab.getViewName().equalsIgnoreCase(ftab.getViewName()))) {
                            if(bestJoin == null || bestJoin.getOrder() > join.getOrder()) {
                                //I believe we need to clone the join and replace table2 with the table with the view
                                bestJoin = join.clone();
                                bestJoin.setTable1(tab);
                            }
                        }
                    }
                }
            }
        }
        return bestJoin;
    }
*/
    /** Finds the best join path from one of the tables in the Set of start tables to
     * one of the tables in the Set of end tables and returns the list of joins in that join
     * path whose first element is the table from the start table and last
     * element is the end table.
     * @param startTables Set of tables with which to start the join path
     * @param excludeTables Set of tables to not use when trying to find a join path. This is primarily used
     * for the recursion on this method. NOTE: this Set is modified in the course of
     * processing.
     * @param endTables Set of tables with which to end the join path
     * @return The list of joins in best join path
     */
	protected List<Join> findBestJoinList(Map<Table, JoinStrand> anchors, Set<Table> endTables) {
		if(anchors.size() == 0)
			return null;
		JoinPath jp = findBestJoinPath(anchors, endTables);
        return jp == null ? null : jp.getJoins();
    }

    /** New way of doing this (5-9-2006) that pre-indexes join paths picking the optimal one for each table to each other table
     *
     * @param startTables
     * @param endTables
     * @return
     */
	protected JoinPath findBestJoinPath(Map<Table, JoinStrand> anchors, Set<Table> endTables) {
        JoinPath bestJoinPath = null;
		JoinStrand bestJoinStrand = null;
		for(Map.Entry<Table, JoinStrand> anchor : anchors.entrySet()) {
			Table tab = anchor.getKey();
            Map<Table,JoinPath> ftabJoinPaths = joinPaths.get(tab);
            if(ftabJoinPaths != null) {
                for(Table ftab : endTables) {
                    JoinPath joinPath = ftabJoinPaths.get(ftab);
                    if(joinPath != null) {
						if(isJoinPathBetter(bestJoinPath, joinPath, bestJoinStrand, anchor.getValue())) {
                        	bestJoinPath = joinPath;
							bestJoinStrand = anchor.getValue();
						}
					}
					if(ftab.hasViewName() && tab.getViewName().equalsIgnoreCase(ftab.getViewName())) {
						Table sftab = new Table(ftab.getDataSourceName(), ftab.getSchemaName(), ftab.getTableName(), ftab.isCaseSensitive());
						joinPath = ftabJoinPaths.get(sftab);
						if(joinPath != null && joinPathAllowsView(joinPath, ftab.getViewName())) {
							if(isJoinPathBetter(bestJoinPath, joinPath, bestJoinStrand, anchor.getValue())) {
								bestJoinPath = joinPath.cloneWithViewLast(ftab.getViewName());
								bestJoinStrand = anchor.getValue();
							}
						}
					}
				}
			}
			if(tab.hasViewName()) {
				Table stab = new Table(tab.getDataSourceName(), tab.getSchemaName(), tab.getTableName(), tab.isCaseSensitive());
				ftabJoinPaths = joinPaths.get(stab);
				if(ftabJoinPaths != null) {
					for(Table ftab : endTables) {
						if(tab.getViewName().equalsIgnoreCase(ftab.getViewName())) {
							JoinPath joinPath = ftabJoinPaths.get(ftab);
							if(joinPath != null && joinPathAllowsView(joinPath, tab.getViewName())) {
								if(isJoinPathBetter(bestJoinPath, joinPath, bestJoinStrand, anchor.getValue())) {
									bestJoinPath = joinPath.cloneWithViewFirst(tab.getViewName());
									bestJoinStrand = anchor.getValue();
								}
							}
						}
					}
				}
			}
		}
		if(bestJoinPath == null) {
			for(Map.Entry<Table, JoinStrand> anchor : anchors.entrySet()) {
				Table tab = anchor.getKey();
				Map<Table, JoinPath> ftabJoinPaths = joinPaths.get(tab);
				if(ftabJoinPaths != null) {
					for(Table ftab : endTables) {
						if(ftab.hasViewName() && !tab.hasViewName()) {
							Table sftab = new Table(ftab.getDataSourceName(), ftab.getSchemaName(), ftab.getTableName(), ftab.isCaseSensitive());
							JoinPath joinPath = ftabJoinPaths.get(sftab);
							if(joinPath != null && joinPathAllowsView(joinPath, ftab.getViewName())) {
								if(isJoinPathBetter(bestJoinPath, joinPath, bestJoinStrand, anchor.getValue())) {
									bestJoinPath = joinPath.cloneWithViewLast(ftab.getViewName());
									bestJoinStrand = anchor.getValue();
								}
							}
						}
					}
				}
				if(tab.hasViewName()) {
					Table stab = new Table(tab.getDataSourceName(), tab.getSchemaName(), tab.getTableName(), tab.isCaseSensitive());
					ftabJoinPaths = joinPaths.get(stab);
					if(ftabJoinPaths != null) {
						for(Table ftab : endTables) {
							if(!ftab.hasViewName()) {
								JoinPath joinPath = ftabJoinPaths.get(ftab);
								if(joinPath != null && joinPathAllowsView(joinPath, tab.getViewName())) {
									if(isJoinPathBetter(bestJoinPath, joinPath, bestJoinStrand, anchor.getValue())) {
										bestJoinPath = joinPath.cloneWithViewFirst(tab.getViewName());
										bestJoinStrand = anchor.getValue();
									}
								}
							}
						}
					}
				}
			}
		}
		return bestJoinPath;
	}

	protected boolean joinPathAllowsView(JoinPath joinPath, String viewName) {
		for(Join join : joinPath.getJoins()) {
			if(join.getTable1() != null && join.getTable1().hasViewName() && !viewName.equals(join.getTable1().getViewName()))
				return false;
			if(join.getTable2() != null && join.getTable2().hasViewName() && !viewName.equals(join.getTable2().getViewName()))
				return false;
		}
		return true;
	}

    /** Retrieves all the joins for the specified tables. This method attempts to join
     * all the tables to each other in some way without including addtional unnecessary
     * joins. If this best effort attempt fails, for one or more of the tables a
     * cross-product situation may occur. Also, any extra where clauses
     * (specified by a mapping of a table to a blank in the custom defined joins) are
     * retrieved and included in the returned collection of joins.
     * @return A collection containing all the join strings retrieved for the given tables
     * @param tables The set of tables for which joins must be found (listed in order of join attempt)
     * @param visibleTables A map of all tables available for direct join mapped to their alias
     */
	public Map<String, Join> getJoins(Map<Table, String> tables, Aliaser aliaser, DatabaseMetaData metaData) throws BuildSQLException {
		return getJoins(tables, aliaser, metaData, true);
    }
    /** Retrieves all the joins for the specified tables. This method attempts to join
     * all the tables to each other in some way without including addtional unnecessary
     * joins. If this best effort attempt fails, for one or more of the tables a
     * cross-product situation may occur. Also, any extra where clauses
     * (specified by a mapping of a table to a blank in the custom defined joins) are
     * retrieved and included in the returned collection of joins.
     * @return A collection containing all the join strings retrieved for the given tables
     * @param tables The set of tables for which joins must be found (listed in order of join attempt)
     * @param visibleTables A map of all tables available for direct join mapped to their alias
     * @param allowUnjoined Whether to throw an error if tables cannot all be joined together
     */
	public Map<String, Join> getJoins(Map<Table, String> tables, Aliaser aliaser, DatabaseMetaData metaData, boolean allowUnjoined) throws BuildSQLException {
        Map<String,Join> joins = new LinkedHashMap<String,Join>(); // keep track of order of addition
        if(tables.isEmpty()) return joins;
        //changing this to ensure that each table is joined to another
        Set<Table> unjoined = new MapBackedSet<Table>(new simple.util.ConcurrentTreeMap<Table,Object>(tableComparator)); //allows concurrent modifications
        log.debug("Creating unjoined set");
        unjoined.addAll(tables.keySet());
        log.debug("Creating anchor set");
        Set<Table> anchors = new TreeSet<Table>(tableComparator);
        Set<Table> poss = new LinkedHashSet<Table>(); // keep track of order
        // pick first table to try as the anchor
        poss.addAll(tables.keySet());
        Iterator<Table> iter = poss.iterator();
        Table atab = iter.next();
        log.debug("Adding first table to anchors and removing from unjoined set");
        unjoined.remove(atab);
        anchors.add(atab);

        log.debug("Finding table joins");
        while(unjoined.size() > 0) {
            log.debug("Looking for a join in '" + unjoined + "' amongst + " + anchors);
			if(findOneJoin(anchors, unjoined, tables, joins, aliaser, metaData) == 0) {
                if(!allowUnjoined) throw new BuildSQLException("No joins found for '" + unjoined + "' amongst " + anchors);
                log.info("WARNING: No joins found for '" + unjoined + "' amongst " + anchors);
                //pick a different anchor
                if(!iter.hasNext())
                    atab = unjoined.iterator().next();
                else
                    atab = iter.next();
                unjoined.remove(atab);
                anchors.add(atab);
            }
        }

        //extra joins (where clauses)
        log.debug("Adding extra where clauses");
        for(Table tab : tables.keySet().toArray(new Table[tables.size()])) { // use array to avoid ConcurrentModException
            Join join = findExtraJoin(tab);
            if(join != null) {
            	//Find additional tables
                Collection<Table> addTabs = findTables(join.getExpression());
                for(Table addtab : addTabs) {
                	if(addtab.getDataSourceName() == null || addtab.getDataSourceName().trim().length() == 0)
                    	addtab.setDataSourceName(join.getDataSourceName());
					if(isAdditionalTable(addtab, tables, tab.getViewName(), metaData)) {
                        log.info("WARNING: Additional table " + addtab + " found in extra where clause");
						String alias = aliaser.getAlias(addtab);
						tables.put(addtab, alias);
						unjoined.add(addtab);
                    }
                }
				joins.put(replaceAliases(join.getExpression(), tables, tab.getViewName(), join.getDataSourceName()), join);

                //joins.put(replaceAliases(join.getExpression(), visibleTables, tab.getViewName()), join);
            }
        }
		log.info("Found " + joins.size() + " joins(s) for tables " + tables + ": " + joins);
        return joins;
    }

	/**
	 * Retrieves all the joins for the specified tables. This method attempts to join
	 * all the tables to each other in some way without including addtional unnecessary
	 * joins. If this best effort attempt fails, for one or more of the tables a
	 * cross-product situation may occur. Also, any extra where clauses
	 * (specified by a mapping of a table to a blank in the custom defined joins) are
	 * retrieved and included in the returned collection of joins.
	 * 
	 * @return A collection containing all the join strings retrieved for the given tables
	 * @param tables
	 *            The set of tables for which joins must be found (listed in order of join attempt)
	 * @param visibleTables
	 *            A map of all tables available for direct join mapped to their alias
	 * @param allowUnjoined
	 *            Whether to throw an error if tables cannot all be joined together
	 */
	public JoinStrand getJoinWeb(Map<Table, String> tables, Aliaser aliaser, DatabaseMetaData metaData, boolean allowUnjoined) throws BuildSQLException {
		if(tables.isEmpty())
			return null;
		Set<Table> origTables = new HashSet<Table>(tables.keySet());

		// Map<String, Join> extraJoins = getExtraJoins(tables, aliaser, metaData);
		Set<Join> extraJoins = new LinkedHashSet<Join>();
		// changing this to ensure that each table is joined to another
		Set<Table> unjoined = new MapBackedSet<Table>(new simple.util.ConcurrentTreeMap<Table, Object>(tableComparator)); // allows concurrent modifications
		Set<Table> unjoinedExtra = new MapBackedSet<Table>(new simple.util.ConcurrentTreeMap<Table, Object>(tableComparator)); // allows concurrent modifications
		log.debug("Creating anchor set");
		Map<Table, JoinStrand> anchors = new TreeMap<Table, JoinStrand>(tableComparator);
		List<Table> poss = new ArrayList<Table>(tables.keySet()); // keep track of order

		for(int possIndex = 0; possIndex < poss.size(); possIndex++) {
			log.debug("Creating unjoined set");
			unjoined.addAll(tables.keySet());
			unjoinedExtra.addAll(tables.keySet());
			Table atab = poss.get(possIndex);
			log.info("Picked " + atab + " as first anchored table");
			JoinStrand startStrand = new JoinStrand(atab, null, JoinCardinality.ONE_TO_ONE, null);
			unjoined.remove(atab);
			anchors.put(atab, startStrand);
			log.debug("Finding table joins");
			while(true) {
				log.debug("Adding extra where clauses");
				for(Table tab : unjoinedExtra.toArray(new Table[unjoinedExtra.size()])) { // use array to avoid ConcurrentModException
					Join join = findExtraJoin(tab);
					if(join != null) {
						// Find additional tables
						Collection<Table> addTabs = findTables(join.getExpression());
						for(Table addtab : addTabs) {
							if(addtab.getDataSourceName() == null || addtab.getDataSourceName().trim().length() == 0)
								addtab.setDataSourceName(join.getDataSourceName());
							if(isAdditionalTable(addtab, tables, tab.getViewName(), metaData)) {
								log.info("WARNING: Additional table " + addtab + " found in extra where clause");
								String alias = aliaser.getAlias(addtab);
								tables.put(addtab, alias);
								unjoinedExtra.add(addtab);
								unjoined.add(addtab);
								if(tables.containsKey(tab))
									poss.add(addtab);
							}
						}
						extraJoins.add(join);
					}
					unjoinedExtra.remove(tab);
				}
				if(unjoined.isEmpty())
					break;
				log.debug("Looking for a join in '" + unjoined + "' amongst + " + anchors);
				if(findOneJoinStrand(anchors, unjoined, unjoinedExtra, tables, aliaser, metaData) == 0) {
					if(!allowUnjoined)
						throw new BuildSQLException("No joins found for '" + unjoined + "' amongst " + anchors);
					log.info("WARNING: No joins found for '" + unjoined + "' amongst " + anchors);
					// pick a different anchor
					if(possIndex < poss.size() - 1)
						atab = poss.get(++possIndex);
					else
						atab = unjoined.iterator().next();
					JoinStrand js = new JoinStrand(atab, null, JoinCardinality.ONE_TO_ONE, null);
					unjoined.remove(atab);
					anchors.put(atab, startStrand);
					Set<Table> addTabs = Collections.emptySet();
					addJoinStrand(startStrand, js, anchors, addTabs);
				}
			}

			for(Join join : extraJoins) {
				if(join.getExpression() == null)
					continue;
				Table tab = join.getTable1();
				if(tab == null)
					tab = join.getTable2();
				JoinStrand js = anchors.get(tab);
				if(js == null)
					throw new BuildSQLException("Table " + tab + " is not in anchors: " + anchors);
				if(!join.getAdditionalTables().isEmpty()) {
					// find the last inner join in the chain and use it
					js = findLastInInnerJoinChain(js);
				}
				js.appendExpression(join.getExpressionObject());

				/*
				// find next after js
				JoinStrand extraStrand = new JoinStrand(null, JoinType.INNER, join.getExpressionObject());
				if(js == startStrand) {
					extraStrand.appendChild(startStrand);
					startStrand = extraStrand;
				} else
					js.appendSibling(extraStrand);
					*/
			}
			log.info("Found joins(s) for tables " + tables + ":\n" + startStrand);
			startStrand = fixUnmetPrerequisites(startStrand, anchors);
			if(startStrand != null)
				return startStrand;

			log.info("Joins were not valid (they referenced a table not yet defined). Choosing a different first table and trying again");

			extraJoins.clear();
			unjoined.clear();
			unjoinedExtra.clear();
			anchors.clear();
			aliaser.retainOnly(origTables);
		}
		throw new BuildSQLException("Could not construct a valid join web for " + tables.keySet());
	}

	protected JoinStrand findLastInInnerJoinChain(JoinStrand js) {
		// go over as far as possible and then down
		JoinStrand next = js.getNext();
		if(next != null && next.getType() == JoinType.INNER)
			return findLastInInnerJoinChain(next);
		JoinStrand child = js.getFirst();
		if(child != null && child.getType() == JoinType.INNER)
			return findLastInInnerJoinChain(child);
		return js;
	}

	/**
	 * @throws BuildSQLException
	 */
	public Map<String, Join> getExtraJoins(Map<Table, String> tables, Aliaser aliaser, DatabaseMetaData metaData) throws BuildSQLException {
		Map<String, Join> joins = new LinkedHashMap<String, Join>(); // keep track of order of addition
		if(tables.isEmpty())
			return joins;

		// extra joins (where clauses)
		log.debug("Adding extra where clauses");
		Set<Table> unjoined = new HashSet<Table>(tables.keySet());
		while(unjoined.size() > 0) {
			for(Table tab : unjoined.toArray(new Table[unjoined.size()])) { // use array to avoid ConcurrentModException
				Join join = findExtraJoin(tab);
				if(join != null) {
					// Find additional tables
					Collection<Table> addTabs = findTables(join.getExpression());
					Map<Table, String> tabsAliases = new HashMap<Table, String>();
					for(Table addtab : addTabs) {
						if(addtab.getDataSourceName() == null || addtab.getDataSourceName().trim().length() == 0)
							addtab.setDataSourceName(join.getDataSourceName());
						if(isAdditionalTable(addtab, tables, tab.getViewName(), metaData)) {
							log.info("WARNING: Additional table " + addtab + " found in extra where clause");
							String alias = aliaser.getAlias(addtab);
							tables.put(addtab, alias);
							unjoined.add(addtab);
						}
					}
					tabsAliases.putAll(tables);
					joins.put(replaceAliases(join.getExpression(), tabsAliases, tab.getViewName(), join.getDataSourceName()), join);
				}
				unjoined.remove(tab);
			}
		}
		log.info("Found " + joins.size() + " extra joins(s) for tables " + tables + ": " + joins);
		return joins;
	}

    /*
    protected Join findOneWayJoin(Table table) {
        Map<Table,Join> ftabs = sourceJoins.get(table);
        if(ftabs != null) {
            Join join = ftabs.get(null);
            if(join != null) return join;
        }
        if(table.hasViewName()) {
            Table stab = new Table(table.getDataSourceName(), table.getSchemaName(), table.getTableName(), table.isCaseSensitive());
            ftabs = sourceJoins.get(stab);
            if(ftabs != null) {
                Join join = ftabs.get(null);
                if(join != null) {
                    Join copy = join.clone();
                    copy.setTable1(table);
                    return copy;
                }
            }
        }
        return null;
    }*/
    protected Join findExtraJoin(Table table) {
        Join join = extraJoins.get(table);
        if(join == null && table.hasViewName()) {
            Table stab = new Table(table.getDataSourceName(), table.getSchemaName(), table.getTableName(), table.isCaseSensitive());
            join = extraJoins.get(stab);
            if(join != null) {
                Join copy = join.clone();
                copy.setTable1(table);
                return copy;
            }
        }
        return join;
    }
    /** Loads any custom defined joins from the specified input stream. Joins are
     * specified one on each line in the form: {table1}/{table2}={join}; Lines that
     * start with '#' are ignored as comments.
     * @param in The InputStream containing the joins
     * @throws IOException If the InputStream cannot be read
     */  /*
    protected void loadJoinsFromStream(InputStream in) throws IOException {
        java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(in));
        String line;
        for(int i = 1; (line=br.readLine()) != null; i++) {
            if(line.trim().length() == 0 || line.trim().charAt(0) == '#') continue;
            java.util.StringTokenizer token = new java.util.StringTokenizer(line);
            try {
                String t1 = token.nextToken("&/-");
                String t2 = token.nextToken(":=").substring(1); // b/c the previous token is returned with this call
                String join = token.nextToken("").substring(1);// b/c the previous token is returned with this call
                loadJoin(t1, t2, join);
                loadJoin(t2, t1, join);
                log.info("Loaded join '" + join + "' for tables [" + t1 + "] and [" + t2 + "]");
            } catch(java.util.NoSuchElementException nsee) {
                log.info("Invalid join specified on line number " + i + "; Must be of the format {table 1}&{table 2}:{join expression}");
            }
        }
    }
    */
    /** If the table name has an array suffix, it is removed and the base table name is
     * returned; otherwise, the original table name is returned.
     * @param s The table name
     * @return The table name with any array suffix strip off of it
     */
    public static String stripArraySuffix(String s) {
        if(s.endsWith("]")) {
            int p = s.lastIndexOf('[');
            if(p == 0) {// if whole thing is bracketted then strip off brackets and return middle
                return s.substring(1, s.length() - 1);
            } else if(p > 0) {
                return s.substring(0, p);
            }
        }
        return s;
    }

    /** Finds joins for the specified tables using the Source Joins data and failing that
     * creates joins for the specified tables using the foreign keys found in the
     * database
     * @param table1 The name of the first table
     * @param table2 The name of the second table
     * @return A collection of any joins found or created (may be empty if no source join is specified
     * and no foreign key relationship exists between the two tables)
     */
    /*
    protected Collection<String> findJoins(Table table1, Table table2, DatabaseMetaData metaData) {
        return null;
        if(metaData == null || table1 == "" || table2 == "") return Collections.EMPTY_LIST;
        String[] st1 = splitSchemaTable(table1);
        String[] st2 = splitSchemaTable(table2);
        List list = new java.util.LinkedList();
        try {
            ResultSet rs = metaData.getCrossReference(null, st1[0], st1[1], null, st2[0], st2[1]);
            try {
                while(rs.next()) {
                    //TODO: if foreign key column is nullable then use outer join
                    String join = table1 + "." + rs.getString("PKCOLUMN_NAME") + " = " + table2 + "." + rs.getString("FKCOLUMN_NAME");
                    list.add(join);
                }
            } finally {
                try { rs.close(); } catch(SQLException e1) {}
            }
            //try the other way
            rs = metaData.getCrossReference(null, st2[0], st2[1], null, st1[0], st1[1]);
            try {
                while(rs.next()) {
                    //TODO: if foreign key column is nullable then use outer join
                    String join = table2 + "." + rs.getString("PKCOLUMN_NAME") + " = " + table1 + "." + rs.getString("FKCOLUMN_NAME");
                    list.add(join);
                }
            } finally {
                try { rs.close(); } catch(SQLException e1) {}
            }
         } catch(SQLException sqle) {
            sqle.printStackTrace();
        }

        return list;
    }
*/
    /** Parses a string to find any tables in it
     * @param s The string to parse
     * @return A collection containing the names of any tables found
     */
    public Set<Table> findTables(String s) {
        return findTables(s, null);
    }

    /** Parses a string to find any tables in it
     * @param s The string to parse
     * @return A collection containing the names of any tables found
     */
    public Set<Table> findTables(String s, String defaultDataSource) {
        final Set<Table> results = new TreeSet<Table>(tableComparator); //HashSet<Table>();
        findTables(results, s, defaultDataSource);
        return results;
    }

    /** Parses a string to find any tables in it
     * @param s The string to parse
     * @return A collection containing the names of any tables found
     */
    public void findTables(final Set<Table> tables, String s, String defaultDataSource) {
        parseDataColumn(s, new AbstractTableColumnHandler(defaultDataSource) {
            @Override
			public void handleColumn(Column column) {
            	tables.add(column.getTable());
            }
            public void handleFunction(String s, int wordStart, int lastDot, int wordEnd, int beginParen) {
            }
            @Override
			public boolean isCaseSensitive() {
                return caseSensitive;
            }

			@Override
			public void handleParameter(Parameter parameter) {
				// Do nothing
			}
        });
    }

    public Table parseTable(String expression) {
        if(expression == null || expression.trim().length() == 0) return null;
        Matcher matcher = tablePattern.matcher(expression);
        if(matcher.matches()) {
            return new Table(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), caseSensitive);
        }
        return null;
    }
    protected static final Pattern tablePattern = Pattern.compile("(?:(\\w+?)[.])??(?:(\\w+?)[.])?(\\w+?)(?:\\[(\\w+?)\\])?");
    protected static final Pattern columnPattern = Pattern.compile("(?:(\\w+?)[.])??(?:(\\w+?)[.])?(\\w+?)(?:\\[(\\w+?)\\])?[.](\\w+?)");
    /** Parses a string to find any tables in it
     * @param s The string to parse
     * @return A collection containing the names of any tables found
     */
    public Collection<Column> findColumns(String s) {
        return findColumns(s, null);
    }
    /** Parses a string to find any tables in it
     * @param s The string to parse
     * @return A collection containing the names of any tables found
     */
    public Collection<Column> findColumns(String expression, String defaultDataSource) {
        return findColumns(expression, defaultDataSource, isCaseSensitive());
    }
    /** Parses a string to find any tables in it
     * @param expression The string to parse
     * @return A collection containing the names of any tables found
     */
    public static Collection<Column> findColumns(String expression, String defaultDataSource, final boolean caseSensitive) {
        final Collection<Column> results = new HashSet<Column>();
        parseDataColumn(expression, new AbstractTableColumnHandler(defaultDataSource) {
            @Override
			public void handleColumn(Column column) {
                results.add(column);
            }
            public void handleFunction(String s, int wordStart, int lastDot, int wordEnd, int beginParen) {
            }
            @Override
			public boolean isCaseSensitive() {
                return caseSensitive;
            }

			@Override
			public void handleParameter(Parameter parameter) {
				// Do nothing
			}
        });
        return results;
    }
    /** Returns the array suffix of the given table name or null if no array suffix exists
     * @param s The table name
     * @return The array suffix of the table name or null if no array suffix exists
     */
    public static String getArraySuffix(String s) {
        if(s.endsWith("]")) {
            int p = s.lastIndexOf('[');
            if(p == 0) {// if whole thing is bracketted then strip off brackets and return middle
                return null;
            } else if(p > 0) {
                return s.substring(p);
            }
        }
        return null;
    }

    /**
     * Determines whether the word which ends at the specified position, pos,
     * is a function or not.
     * @param s The string to search
     * @param pos The next character after possible function or table/column word
     * @return Whether the text starting at the given position is a function's suffix or not
     */
    protected static boolean isFunctionSuffix(String s, int pos) {
        //check if first non-whitespace character is "("
        pos = nextNonWhitespace(s, pos);
        if(pos < s.length() && s.charAt(pos) == '(') {
            // check to make sure it's not the outer join indicator, "(+)"
            pos = nextNonWhitespace(s, pos+1);
            if(pos < s.length() && s.charAt(pos) == '+') {
                pos = nextNonWhitespace(s, pos+1);
                if(pos < s.length() && s.charAt(pos) == ')') { // its the outer join indicator
                    return false;
                }
            }
            // otherwise it's a function
            return true;
        }
        return false;
    }

    /** Finds and returns the index of the next non-whitespace character in the specified string.
     * Returns the length of the string if no such character is found.
     * @return The index of the next non-whitespace character
     * @param s The string to search
     * @param start The index at which to begin searching. The returned value is always >= start.
     */
    protected static int nextNonWhitespace(String s, int start) {
        for(int i = start; i < s.length(); i++) {
            char ch = s.charAt(i);
            if(!Character.isWhitespace(ch)) return i;
        }
        return s.length();
    }

	protected static final Pattern parseDataColumnPattern = Pattern.compile("\\s*(?:('[^']*')|((?:[A-Za-z]\\w*\\.)*([A-Za-z]\\w*))\\s*(\\()(?!\\s*\\+\\s*\\))|((?:[A-Za-z]\\w*(?:\\[\\w+\\])?\\.)+([A-Za-z]\\w*))(?:\\s*\\Q(+)\\E)?(?!\\()|(\\:[.\\w]+(\\:\\:\\w+(?:\\(\\s*\\d+(?:,\\s*\\d+)?\\s*\\))?(?:\\:\\w+(?:\\(\\s*\\d+(?:,\\s*\\d+)?\\s*\\)?))?)?)|(\\()|(\\)))");

	/**
	 * Parses the given string and makes call-backs to the specified handler for any
	 * tables, functions, or parameters found in the string
	 * 
	 * @param s
	 *            The string to parse
	 * @param handler
	 *            The handler that is called when a table is found
	 */
	public static void parseDataColumn(String s, TableColumnHandler handler) {
		Matcher matcher = parseDataColumnPattern.matcher(s);
		while(matcher.find()) {
			for(int i = 1; i < matcher.groupCount() + 1; i++) {
				if(matcher.start(i) >= 0) {
					switch(i) {
						case 1: // quotes - ignore
							break;
						case 2: // Function
							int lastDot;
							if(matcher.start(i + 1) > matcher.start(i))
								lastDot = matcher.start(i + 1) - 1;
							else
								lastDot = -1;
							handler.handleFunction(s, matcher.start(i), lastDot, matcher.end(i), matcher.start(i + 2));
							handler.handleBeginParen(s, matcher.start(i + 2));
							break;
						case 5: // table column
							handler.handleTableColumn(s, matcher.start(i), matcher.start(i + 1) - 1, matcher.end(i));
							break;
						case 7: // param
							int wordEnd;
							int typeStart;
							if(matcher.start(i + 1) > 0) {
								wordEnd = matcher.start(i + 1);
								typeStart = wordEnd + 2;
							} else {
								wordEnd = matcher.end(i);
								typeStart = -1;
							}
							handler.handleParam(s, matcher.start(i), wordEnd, typeStart, matcher.end(i + 1));
							break;
						case 9: // (
							handler.handleBeginParen(s, matcher.start(i));
							break;
						case 10:
							handler.handleEndParen(s, matcher.start(i));
							break;
					}
					break; // next match
				}
			}
		}
	}
    /** Parses the given string and makes call-backs to the specified handler for any
     * tables found in the string
     * @param s The string to parse
     * @param handler The handler that is called when a table is found
     */
	protected static void parseDataColumn_old(String s, TableColumnHandler handler) {
        int wordStart = -1;
        int lastDot = -1;
        int wordEnd = -1;
		boolean param = false;
        for(int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            switch(ch) {
                case '\'': //quotes
					if(param && wordStart >= 0) {
						if(lastDot >= 0)
							handler.handleParam(s, wordStart, lastDot, lastDot + 2, wordEnd);
						else
							handler.handleParam(s, wordStart, wordEnd, -1, -1);
					}
                    int p = s.indexOf(ch, i+1);
                    if(p < 0) throw new IllegalArgumentException("Invalid data source; Unmatched quotes at position " + (i+1));
                    i = p; // skip to end of quotes
                    wordStart = -1;
                    lastDot = -1;
                    wordEnd = -1;
					param = false;
                    break;
                case '.':
                    if(wordStart >= 0) {
						if(!param)
							lastDot = i;
                    	wordEnd = i+1;
                    }
                    break;
                case '(':
					if(param && lastDot >= 0) {
						wordEnd = i + 1;
						break;
					}
                	if(wordStart >= 0) {
						if(param && wordStart >= 0) {
							if(lastDot >= 0)
								handler.handleParam(s, wordStart, lastDot, lastDot + 2, wordEnd);
							else
								handler.handleParam(s, wordStart, wordEnd, -1, -1);
						} else if(isFunctionSuffix(s, i)) // if it's not a function, we've got one
                			handler.handleFunction(s, wordStart, lastDot, wordEnd, i);
						else if(lastDot >= 0)
                            handler.handleTableColumn(s, wordStart, lastDot, wordEnd);
                	}
                    wordStart = -1;
                    lastDot = -1;
                    wordEnd = -1;
					param = false;
                    handler.handleBeginParen(s, i);
                    break;
                case ')':
					if(param && lastDot >= 0) {
						wordEnd = i + 1;
						break;
					}
                	if(wordStart >= 0 && lastDot >= 0) { //we've got one!
						if(param)
							if(lastDot >= 0)
								handler.handleParam(s, wordStart, lastDot, lastDot + 2, wordEnd);
							else
								handler.handleParam(s, wordStart, wordEnd, -1, -1);
						else
							handler.handleTableColumn(s, wordStart, lastDot, wordEnd);
                        wordStart = -1;
                        lastDot = -1;
                        wordEnd = -1;
						param = false;
                    }
                	handler.handleEndParen(s, i);
                	break;
				case ',':
					if(param && lastDot >= 0) {
						wordEnd = i + 1;
						break;
					}
					// fall-though
                default:
                    if(!Character.isWhitespace(ch)) {
						if(':' == ch && param) {
							if(lastDot < 0)
								lastDot = i;
							else {
								wordEnd = i + 1;
								break;
							}
						}
                    	if(wordEnd < i) {
	                    	if(wordStart >= 0 && lastDot >= 0) { //we've got one!
								if(param)
									if(lastDot >= 0)
										handler.handleParam(s, wordStart, lastDot, lastDot + 2, wordEnd);
									else
										handler.handleParam(s, wordStart, wordEnd, -1, -1);
								else
									handler.handleTableColumn(s, wordStart, lastDot, wordEnd);
	                    	}
	                        wordStart = -1;
	                        lastDot = -1;
	                        wordEnd = -1;
							param = false;
                    	}
						if(':' == ch) {
							if(wordStart >= 0 && lastDot >= 0) { // we've got one!
								if(param)
									if(lastDot >= 0)
										handler.handleParam(s, wordStart, lastDot, lastDot + 2, wordEnd);
									else
										handler.handleParam(s, wordStart, wordEnd, -1, -1);
								else
									handler.handleTableColumn(s, wordStart, lastDot, i);
							}
							wordStart = i;
							lastDot = -1;
							wordEnd = i + 1;
							param = true;
						} else if(Character.isJavaIdentifierStart(ch)) {
	                        if(wordStart == -1) wordStart = i;
	                        wordEnd = i+1;
	                    } else if(wordStart >= 0 && (Character.isJavaIdentifierPart(ch) || ch == '[' || ch == ']')) { //brackets are for same table used more than once
	                    	wordEnd = i+1;
	                    } else if(wordStart >= 0 && lastDot >= 0) { //we've got one!
							if(param)
								if(lastDot >= 0)
									handler.handleParam(s, wordStart, lastDot, lastDot + 2, wordEnd);
								else
									handler.handleParam(s, wordStart, wordEnd, -1, -1);
							else
								handler.handleTableColumn(s, wordStart, lastDot, wordEnd);
	                        wordStart = -1;
	                        lastDot = -1;
	                        wordEnd = -1;
							param = false;
	                    }
                    }
            }
        }
		if(wordStart >= 0) { // we've got one!
			if(param)
				if(lastDot >= 0)
					handler.handleParam(s, wordStart, lastDot, lastDot + 2, wordEnd);
				else
					handler.handleParam(s, wordStart, wordEnd, -1, -1);
			else if(lastDot >= 0)
				handler.handleTableColumn(s, wordStart, lastDot, wordEnd);
        }
    }

    /** Replaces any aliased tables in the specified string
     * @return The newly create string with all the table names replaced with their aliases
     * @param arraySuffix The array suffix of the current tables. Of the form "[1]".
     * @param s The string in which to replace table names with their aliases
     * @param tables The Map of table names to aliases
     */
    public String replaceAliases(String s, final Map<Table,String> tables, final String viewName, final String defaultDateSource) {
        final StringBuilder sb = new StringBuilder(s);
        parseDataColumn(s, new TableColumnHandler() {
            protected int diff = 0;
            public void handleTableColumn(String s, int wordStart, int lastDot, int wordEnd) {
                Column column = createColumn(s, wordStart, wordEnd, caseSensitive, defaultDateSource);
                // NOTE: Order of calculating alias is very important if Views are used
                String alias;
                if(column.getTable().hasViewName()) {
                    //try first with view, then with out
                    alias = tables.get(column.getTable());
                    if(alias == null) {
                        Table sTab = new Table(column.getTable().getDataSourceName(), column.getTable().getSchemaName(), column.getTable().getTableName(), column.getTable().isCaseSensitive());
                        alias = tables.get(sTab);
                    }
                } else if(viewName != null && viewName.trim().length() > 0) {
                    // try first withOUT view, then with
                    alias = tables.get(column.getTable());
                    if(alias == null) {
                        Table vTab = new Table(column.getTable().getDataSourceName(), column.getTable().getSchemaName(), column.getTable().getTableName(), viewName, column.getTable().isCaseSensitive());
                        alias = tables.get(vTab);
                    }
                } else {
                    alias = tables.get(column.getTable());
                    //try to find one with a view?
                }

                if(alias == null) {
                    log.warn("Could not find an alias for '" + column.getTable() + "' in " + tables + " while view = '" + viewName + "'; skipping this expression");
                    return;
                }
                sb.replace(wordStart + diff, lastDot + diff, alias);
                diff += (alias.length() - lastDot + wordStart);
            }
            public void handleFunction(String s, int wordStart, int lastDot, int wordEnd, int beginParen) {
            }
			public void handleBeginParen(String s, int pos) {
			}
			public void handleEndParen(String s, int pos) {
			}

			public void handleParam(String s, int wordStart, int wordEnd, int typeStart, int typeEnd) {
			}
        });
        return sb.toString();
    }

    /** Replaces any aliased tables in the specified string
     * @return The newly create string with all the table names replaced with their aliases
     * @param arraySuffix The array suffix of the current tables. Of the form "[1]".
     * @param s The string in which to replace table names with their aliases
     * @param tables The Map of table names to aliases
     */
    public String replaceAliases(String s, final Map<Table,String> tables, String viewName) {
        return replaceAliases(s, tables, viewName, null);
    }

    public boolean containsAggregate(String s, DatabaseMetaData metaData) {
        AggregateFinder af = new AggregateFinder(metaData);
        parseDataColumn(s, af);
        return af.containsAgg;
    }

    /** The call-back object for the <CODE>parseDataColumn()</CODE> method */
    public static interface TableColumnHandler {
        /** The method that is called when the <CODE>parseDataColumn()</CODE> method detects
         * a table.column structure
         * @param s The whole string that is being parsed
         * @param wordStart The position at which the table.column name starts
         * @param lastDot The position at which the last period in the table.column name is found
         * @param wordEnd The position at which the table.column name ends (the character index to which
         * this refers is the one after the last character in the table.column name)
         */
        public void handleTableColumn(String s, int wordStart, int lastDot, int wordEnd) ;
        /** The method that is called when the <CODE>parseDataColumn()</CODE> method detects
         * a function structure
         * @param s The whole string that is being parsed
         * @param wordStart The position at which the function name starts
         * @param lastDot The position at which the last period in the function name is found
         * @param wordEnd The position at which the function name ends (the character index to which
         * this refers is the one after the last character in the function name)
         */
        public void handleFunction(String s, int wordStart, int lastDot, int wordEnd, int beginParen) ;
        /** The method that is called when the <CODE>parseDataColumn()</CODE> method detects
         * an begin parenthesis
         * @param s The whole string that is being parsed
         * @param pos The position at which the parenthesis is
         */
        public void handleBeginParen(String s, int pos) ;
        /** The method that is called when the <CODE>parseDataColumn()</CODE> method detects
         * an end parenthesis
         * @param s The whole string that is being parsed
         * @param pos The position at which the parenthesis is
         */
        public void handleEndParen(String s, int pos) ;

		public void handleParam(String s, int wordStart, int wordEnd, int typeStart, int typeEnd);
    }

    public static abstract class AbstractTableColumnHandler implements TableColumnHandler {
        protected String dataSource;
        public AbstractTableColumnHandler() {
        }
        public AbstractTableColumnHandler(String dataSource) {
            this.dataSource = dataSource;
        }
        public void handleTableColumn(String s, int wordStart, int lastDot, int wordEnd) {
            handleColumn(createColumn(s, wordStart, wordEnd, isCaseSensitive(), dataSource));
        }

		@Override
		public void handleParam(String s, int wordStart, int wordEnd, int typeStart, int typeEnd) {
			Parameter p = new Parameter();
			p.setPropertyName(s.substring(wordStart + 1, wordEnd));
			p.setRequired(false);
			if(typeStart >= 0 && typeStart < typeEnd)
				try {
					p.setSqlType(ConvertUtils.convert(SQLType.class, s.substring(typeStart, typeEnd)));
				} catch(ConvertException e) {
					log.warn("Could not convert value to SQLType", e);
				}
			handleParameter(p);
		}
        public abstract void handleColumn(Column column) ;

		public abstract void handleParameter(Parameter parameter);
        public abstract boolean isCaseSensitive() ;
		public void handleBeginParen(String s, int pos) {
		}
		public void handleEndParen(String s, int pos) {
		}
    }

    public static Column createColumn(String expression, int start, int end, boolean caseSensitive, String defaultDataSource) {
        Matcher matcher = columnPattern.matcher(expression).region(start, end);
        if(matcher.matches()) {
            String ds = matcher.group(1);
            if(ds == null && defaultDataSource != null) ds = defaultDataSource;
            return new Column(new Table(ds, matcher.group(2), matcher.group(3), matcher.group(4), caseSensitive), matcher.group(5));
        }
        return null;
    }

    public static Column createColumn(String expression, int start, int end, boolean caseSensitive) {
        return createColumn(expression, start, end, caseSensitive, null);
    }

    public static Column createColumn(String expression, boolean caseSensitive) {
        return createColumn(expression, 0, expression.length(), caseSensitive, null);
    }
    /** Verifies that the table is in the database
     * @param tableName The full name of the table (schema.table)
     * @return True if no meta data is available or if the table does exist in the database
     */
    protected boolean checkTable(Table table, DatabaseMetaData metaData) {
        //if(tableName == null || tableName.trim().length() == 0) return false;
        if(metaData == null) return true; // allow everything if we can't verify
        //tableName = stripArraySuffix(tableName);
        ResultSet rs = null;
        try {
            //String[] st = splitSchemaTable(tableName);
            if (metaData.storesLowerCaseIdentifiers()) { 
            	rs = metaData.getTables(null, table.getSchemaName().toLowerCase(), table.getTableName().toLowerCase(), null);
            } else {
            	rs = metaData.getTables(null, table.getSchemaName(), table.getTableName(), null);
            }
            
            boolean found = rs.next();
            if(!found && log.isDebugEnabled())
            	log.debug(table.toString() + " is not a table in the database");
            return found;
        } catch(SQLException sqle) {
            log.info("Error while verifying table, '" + table + "'", sqle);
            return true;
        } finally {
            if(rs != null) try { rs.close(); } catch(SQLException sqle) {}
        }
    }

    /** Creates a unique alias
     * @param lastAlias The last alias used (or null if this is the first)
     * @return The alias for the table
     */
    public String createAlias(String lastAlias) {
        String alias;
        if(lastAlias == null) {
            alias = "a";
        } else {
            int i = lastAlias.length() - 1;
            while(i >= 0 && lastAlias.charAt(i) == 'z') i--;
            if(i < 0) alias = lastAlias + "a";
            else alias = lastAlias.substring(0,i) + ((char)(1 + lastAlias.charAt(i))) + lastAlias.substring(i+1);
        }
        return alias;
    }

    /** Splits a schema.table name into the schema name and the table name
     * @param fullName The full schema.table name
     * @return A two element array containing the schema name as the first element and the
     * table name as the second element
     */
    protected String[] splitSchemaTable(String fullName) {
        int p = fullName.lastIndexOf('.');
        String schema = (p >= 0 ? fullName.substring(0, p) : null);
        String table = (p >= 0 ? fullName.substring(p+1) : fullName);
        return new String[] {schema, table};
    }

    /** Finds all the primary key columns of the specified table
     * @param table The full table name (schema.table)
     * @return An array of all the primary key column names for the specified table
     */
    /*
    protected String[] getPrimaryKeys(String table, DatabaseMetaData metaData) {
        if(metaData == null) return new String[] { "ID" }; // null;
        String[] st = splitSchemaTable(table);
        List list = new java.util.LinkedList();
        try {
            ResultSet rs = metaData.getPrimaryKeys(null, st[0], st[1]);
            try {
                while(rs.next()) {
                    list.add(rs.getString("COLUMN_NAME"));
                }
            } finally {
                try { rs.close(); } catch(SQLException e1) {}
            }
            if(list.size() == 0) {
                rs = metaData.getBestRowIdentifier(null, st[0], st[1], DatabaseMetaData.bestRowSession, false);
                try {
                    while(rs.next()) {
                        list.add(rs.getString("COLUMN_NAME"));
                    }
                } finally {
                    try { rs.close(); } catch(SQLException e1) {}
                }
            }
        } catch(SQLException sqle) {
            sqle.printStackTrace();
        }
        return (String[])list.toArray(new String[list.size()]);
    }// */

    /** Loads the specified join into the map of custom defined joins for the two given
     * tables
     * @param table1 The name of the first table
     * @param table2 The name of the second table
     * @param expression The join string
     * @param order The precedence of the join (lowest order is used first)
     */
	protected void loadOneWayJoin(Table table1, Table table2, String expression, int order, JoinType joinType, JoinCardinality joinCardinality) {
        Join join = new Join();
        join.setTable1(table1);
        join.setTable2(table2);
        join.setExpression(expression);
        join.setOrder(order);
		join.setJoinType(joinType);
		if(joinCardinality != null)
			join.setJoinCardinality(joinCardinality);
        loadOneWayJoin(join);
    }
    /** Loads the specified join into the map of custom defined joins for the two given
     * tables
     * @param join The join
     */
    protected void loadOneWayJoin(Join join) {
        log.debug("Adding join from '" + join.getTable1() + "' to '" + join.getTable2() +"' with '" + join.getExpression() + "'");
        //calculate extra tables in expression
        Expression expObj = createExpression(join.getExpression());
        join.setExpressionObject(expObj);
        Set<Table> tables = new TreeSet<Table>(tableComparator);
        for(Column col : expObj.getColumns()) {
        	tables.add(col.getTable());
        }
        if(join.getTable1() == null || join.getTable2() == null || !join.getTable1().equalsBaseTable(join.getTable2())) {
        	if(fixMissingJoinTable(expObj, join.getTable1(), tables)
        			|| fixMissingJoinTable(expObj, join.getTable2(), tables)) {
        		expObj.invalidate();
        		join.setExpression(expObj.toString());
        	}
        }
        join.setAdditionalTables(tables);
        if(join.getTable2() == null) {
        	//its an extra join
        	Table tab = join.getTable1();
			if(!join.getAdditionalTables().isEmpty() && !expressionContains(expObj, tab)) {
				Iterator<Table> iter = join.getAdditionalTables().iterator();
				Table first = iter.next();
				iter.remove();
				join.setTable1(first);
        	}
        	extraJoins.put(tab, join);
        } else {
        	//pre-index all joinPaths for this join
        	Map<Table,JoinPath> existing = joinPaths.get(join.getTable1());
    		if(existing == null) {
    			existing = new TreeMap<Table,JoinPath>(tableComparator);
    			joinPaths.put(join.getTable1(), existing);
    		}
    		boolean useShortestPath = true;
    		if(useShortestPath) {
    			JoinPath jp = new JoinPath();
    			jp.appendJoin(join);
    			existing.put(join.getTable2(), jp);
    		}
        	Map<Table,JoinPath> map = joinPaths.get(join.getTable2());
        	if(map != null) {
        		for(JoinPath jp : map.values()) {
        			if(!jp.getEndTable().equals(join.getTable1())) {
		        		JoinPath ejp = existing.get(jp.getEndTable());
		        		if(isJoinPathBetter(ejp, jp, join)) {
		        			JoinPath clone = jp.clone();
		        			clone.prependJoin(join);
		        			existing.put(jp.getEndTable(), clone);
		        		}
        			}
	        	}
        	}
        	List<JoinPath> updates = new ArrayList<JoinPath>();
        	for(Map.Entry<Table,Map<Table,JoinPath>> entry : joinPaths.entrySet()) {
        		if(!entry.getKey().equals(join.getTable2())) {
	        		JoinPath jp = entry.getValue().get(join.getTable1());
	        		if(jp != null) {
		        		Map<Table,JoinPath> existing2 = joinPaths.get(jp.getStartTable());
		        		JoinPath ejp = existing2.get(join.getTable2());
		        		if(isJoinPathBetter(ejp, jp, join)) {
			        		JoinPath clone = jp.clone();
		        			clone.appendJoin(join);
		        			updates.add(clone);
		        		}
	        		}
        		}
        	}
        	for(JoinPath jp : updates) {
        		Map<Table,JoinPath> existing2 = joinPaths.get(jp.getStartTable());
        		existing2.put(jp.getEndTable(), jp);
        	}
        	JoinPath ejp = existing.get(join.getTable2());
        	if(isJoinPathBetter(ejp, null, join)) {
        		JoinPath jp = new JoinPath();
    			jp.appendJoin(join);
    			existing.put(join.getTable2(), jp);
    		}
        }
        
    }

    protected boolean fixMissingJoinTable(Expression expObj, Table table, Set<Table> tables) {
    	if(table != null && !tables.remove(table)) {
	    	if(table.hasViewName()) {
	    		Table stab = new Table(table.getDataSourceName(), table.getSchemaName(), table.getTableName());
	    		if(tables.remove(stab)) {
	    			for(Column col : expObj.getColumns()) {
	    	        	if(col.getTable().equals(stab))
	    	        		col.setTable(table);
	    	        }
	    			return true;
	    		}
	    	}
    	}
    	return false;
    }

    public Expression createExpression(String exp) {
    	return createExpression(exp, null, caseSensitive);
    }

	public boolean expressionContains(Expression expObj, Table table) {
		for(Column c : expObj.getColumns())
			if(c.getTable() != null && c.getTable().equals(table))
				return true;
		return false;
	}

    public static Expression createExpression(String exp, String defaultDataSource, final boolean caseSensitive) {
		final List<Object> parts = new ArrayList<Object>();
    	final Holder<Integer> lastPos = new Holder<Integer>();
		lastPos.setValue(0);
		parseDataColumn(exp, new AbstractTableColumnHandler(defaultDataSource) {
			@Override
			public void handleColumn(Column column) {
	            parts.add(column);
	        }
	        public void handleFunction(String s, int wordStart, int lastDot, int wordEnd, int beginParen) {
	        }
	        @Override
			public void handleTableColumn(String s, int wordStart, int lastDot, int wordEnd) {
				int start = lastPos.getValue();
				if(start < wordStart) {
					parts.add(s.substring(start, wordStart));
				}
				lastPos.setValue(wordEnd);
				super.handleTableColumn(s, wordStart, lastDot, wordEnd);
			}

			@Override
			public void handleParam(String s, int wordStart, int wordEnd, int typeStart, int typeEnd) {
				int start = lastPos.getValue();
				if(start < wordStart) {
					parts.add(s.substring(start, wordStart));
				}
				lastPos.setValue(typeEnd < 0 ? wordEnd : typeEnd);
				super.handleParam(s, wordStart, wordEnd, typeStart, typeEnd);
			}
			@Override
			public boolean isCaseSensitive() {
	            return caseSensitive;
	        }

			@Override
			public void handleParameter(Parameter parameter) {
				parts.add(parameter);
			}
		});
		int start = lastPos.getValue();
		if(start < exp.length()) {
			parts.add(exp.substring(start));
		}
		return new BaseExpression(parts);
    }

    //protected static final Pattern distinctInCountPattern = Pattern.compile("\\s*DISTINCT\\s+", Pattern.CASE_INSENSITIVE);
    //protected static final Pattern castCollectPattern = Pattern.compile("\\s*[(]\\s*COLLECT\\s*[(]\\s*(DISTINCT)?\\s+", Pattern.CASE_INSENSITIVE);
    protected static final RegexAggregateParser defaultAggregateParser = new RegexAggregateParser();
    static {
    	defaultAggregateParser.registerAggregate(Aggregate.SET, "SET", Pattern.compile("SET\\s*[(]CAST\\s*[(]\\s*COLLECT\\s*[(]", Pattern.CASE_INSENSITIVE));
    	//defaultAggregateParser.registerAggregate(Aggregate.SET, "CAST", Pattern.compile("CAST\\s*[(]\\s*COLLECT\\s*[(]\\s*DISTINCT\\s+", Pattern.CASE_INSENSITIVE));
    	defaultAggregateParser.registerAggregate(Aggregate.ARRAY, "CAST", Pattern.compile("CAST\\s*[(]\\s*COLLECT\\s*[(]", Pattern.CASE_INSENSITIVE));
    	defaultAggregateParser.registerAggregate(Aggregate.AVG);
    	defaultAggregateParser.registerAggregate(Aggregate.DISTINCT, "COUNT", Pattern.compile("COUNT\\s*[(]\\s*DISTINCT\\s+", Pattern.CASE_INSENSITIVE));
    	defaultAggregateParser.registerAggregate(Aggregate.COUNT);
    	defaultAggregateParser.registerAggregate(Aggregate.FIRST);
    	defaultAggregateParser.registerAggregate(Aggregate.LAST);
    	defaultAggregateParser.registerAggregate(Aggregate.MAX);
    	defaultAggregateParser.registerAggregate(Aggregate.MIN);
    	defaultAggregateParser.registerAggregate(Aggregate.SUM);
    }
    public AggregateParser getAggregateParser() {
    	return defaultAggregateParser;
    }
    public ExtendedExpression createExtendedExpression(String exp, DatabaseMetaData metaData) {
    	return createExtendedExpression(exp, getAggregateParser(), null, caseSensitive);
    }
    public static ExtendedExpression createExtendedExpression(String exp, String defaultDataSource, final boolean caseSensitive) {
    	return createExtendedExpression(exp, defaultAggregateParser, defaultDataSource, caseSensitive);
    }
    public static ExtendedExpression createExtendedExpression(String exp, final AggregateParser aggregateParser, String defaultDataSource, final boolean caseSensitive) {
		final Set<Aggregate> aggregates = new HashSet<Aggregate>();
    	final List<Object> parts = new ArrayList<Object>();
    	final Set<Expression> inners = new HashSet<Expression>();
    	final Holder<Integer> lastPos = new Holder<Integer>();
    	lastPos.setValue(0);
    	final Holder<Integer> innerStartPos = new Holder<Integer>();
    	innerStartPos.setValue(-1);
		parseDataColumn(exp, new AbstractTableColumnHandler(null) {
			protected int depth = 0;
			protected int innerDepth = -1;
			protected int innerPartIndex = -1;

			@Override
			public void handleParameter(Parameter parameter) {
				parts.add(parameter);
			}

			@Override
			public void handleColumn(Column column) {
				parts.add(column);
	        }
	        public void handleFunction(String s, int wordStart, int lastDot, int wordEnd, int beginParen) {
	        	Aggregate agg;
	        	agg = aggregateParser.isAggregateFunction(s, wordStart, wordEnd, beginParen, innerStartPos);
	        	if(agg != null) {
                	aggregates.add(agg);
                	addPart(s, innerStartPos.getValue());
                	innerPartIndex = parts.size();
                }
            }
	        @Override
			public void handleBeginParen(String s, int pos) {
	        	if(innerStartPos.getValue() > pos)
	        		innerDepth = depth;
	        	depth++;
			}
			@Override
			public void handleEndParen(String s, int pos) {
				if(--depth == innerDepth) {
					addPart(s, pos);
					List<Object> subParts = parts.subList(innerPartIndex, parts.size());
					Expression exp = new BaseExpression(subParts);
					subParts.clear();
					parts.add(exp);
					inners.add(exp);
					innerDepth = -1;
				}
			}
			protected void addPart(String s, int endPos) {
				int start = lastPos.getValue();
				if(start < endPos) {
					parts.add(s.substring(start, endPos));
				}
				lastPos.setValue(endPos);
			}
			@Override
			public void handleTableColumn(String s, int wordStart, int lastDot, int wordEnd) {
				addPart(s, wordStart);
				super.handleTableColumn(s, wordStart, lastDot, wordEnd);
				lastPos.setValue(wordEnd);
			}
			@Override
			public void handleParam(String s, int wordStart, int wordEnd, int typeStart, int typeEnd) {
				addPart(s, wordStart);
				super.handleParam(s, wordStart, wordEnd, typeStart, typeEnd);
				lastPos.setValue(typeEnd);
			}

			@Override
			public boolean isCaseSensitive() {
	            return caseSensitive;
	        }
		});
		int start = lastPos.getValue();
		if(start < exp.length()) {
			parts.add(exp.substring(start));
		}
		return new BaseExtendedExpression(parts, inners, aggregates);
    }

	protected static enum JoinPathComparison {
		SHORTEST_PATH, NOT_MANY_TO_MANY, COMBO_NOT_MANY_TO_MANY, GRANULARITY, JOIN_TYPE, CARDINALITY, ORDER
	};

	protected JoinPathComparison[] joinPathComparisions = new JoinPathComparison[] { JoinPathComparison.NOT_MANY_TO_MANY, JoinPathComparison.COMBO_NOT_MANY_TO_MANY, JoinPathComparison.SHORTEST_PATH, /*JoinPathComparison.GRANULARITY, JoinPathComparison.JOIN_TYPE, JoinPathComparison.CARDINALITY,*/JoinPathComparison.ORDER };
    protected boolean useShortestPath = true;
	protected boolean useCardinality = true;
	protected boolean useJoinType = true;
    protected boolean isJoinPathBetter(JoinPath existingJoinPath, JoinPath baseJoinPath, Join join) {
    	if(existingJoinPath == null) return true;
		if(useShortestPath) {
    		int newLength = (baseJoinPath == null ? 1 : baseJoinPath.getLength() + 1);
			if(existingJoinPath.getLength() > newLength)
				return true;
			if(existingJoinPath.getLength() < newLength)
				return false;
		}
		int newMaxOrder = baseJoinPath == null ? join.getOrder() : Math.max(baseJoinPath.getOrders().get(baseJoinPath.getOrders().size() - 1), join.getOrder());
		int existingMaxOrder = existingJoinPath.getOrders().get(existingJoinPath.getOrders().size() - 1);
		if(existingMaxOrder > newMaxOrder)
			return true;
		return false;
    }

	protected boolean isJoinPathBetter(JoinPath existingJoinPath, JoinPath newJoinPath, JoinStrand existingJoinStrand, JoinStrand newJoinStrand) {
		if(existingJoinPath == null)
			return true;
		JoinCardinality existingCardinality = null;
		JoinCardinality newCardinality = null;

		for(JoinPathComparison jpc : joinPathComparisions) {
			switch(jpc) {
				case SHORTEST_PATH:
					int i = existingJoinPath.getLength() - newJoinPath.getLength();
					if(i < 0)
						return false;
					else if(i > 0)
						return true;
					break;
				case NOT_MANY_TO_MANY:
					if(existingCardinality == null)
						existingCardinality = existingJoinPath.getCardinality();
					if(newCardinality == null)
						newCardinality = newJoinPath.getCardinality();

					if(existingCardinality == JoinCardinality.MANY_TO_MANY && newCardinality != JoinCardinality.MANY_TO_MANY)
						return true;
					if(existingCardinality != JoinCardinality.MANY_TO_MANY && newCardinality == JoinCardinality.MANY_TO_MANY)
						return false;
					if(existingCardinality == JoinCardinality.UNKNOWN && newCardinality != JoinCardinality.UNKNOWN)
						return true;
					if(existingCardinality != JoinCardinality.UNKNOWN && newCardinality == JoinCardinality.UNKNOWN)
						return false;
					break;
				case COMBO_NOT_MANY_TO_MANY:
					if(existingCardinality == null)
						existingCardinality = existingJoinPath.getCardinality();
					if(newCardinality == null)
						newCardinality = newJoinPath.getCardinality();
					if(existingJoinStrand != null && existingJoinStrand.isManyToMany(existingCardinality.getReverse()) && (newJoinStrand == null || !newJoinStrand.isManyToMany(newCardinality.getReverse())))
						return true;
					if(newJoinStrand != null && newJoinStrand.isManyToMany(newCardinality.getReverse()) && (existingJoinStrand == null || !existingJoinStrand.isManyToMany(existingCardinality.getReverse())))
						return false;
					break;
				case CARDINALITY:
					int cs = getCardinalityScore(existingCardinality) - getCardinalityScore(newCardinality);
					if(cs < -900)
						return false;
					else if(cs > 900)
						return true;

					if(cs < 0)
						return false;
					else if(cs > 0)
						return true;
					break;
				case GRANULARITY:
					int existingGranularity = existingJoinPath.getEndGranularity();
					if(existingJoinStrand != null)
						existingGranularity += existingJoinStrand.getGranularity();
					int newGranularity = newJoinPath.getEndGranularity();
					if(newJoinStrand != null)
						newGranularity += newJoinStrand.getGranularity();
					if(existingGranularity > newGranularity)
						return true;
					if(existingGranularity < newGranularity)
						return false;
					break;
				case JOIN_TYPE:
					JoinType existingJoinType = existingJoinPath.getJoinType();
					if(existingJoinStrand != null && existingJoinStrand.getType() != null)
						existingJoinType = existingJoinType == null ? existingJoinStrand.getType() : existingJoinStrand.getType().getCombination(existingJoinType);
					JoinType newJoinType = newJoinPath.getJoinType();
					if(newJoinStrand != null && newJoinStrand.getType() != null)
						newJoinType = newJoinType == null ? newJoinStrand.getType() : newJoinStrand.getType().getCombination(newJoinType);
					// XXX: JoinStrand.getType() may not account for everything beyond the first join
					if(existingJoinType != null && newJoinType != null && existingJoinType != newJoinType) {
						// Note: INNER, RIGHT is best, then LEFT, and FULL, then OPTIONAL and last is CROSS
						if(existingJoinType == JoinType.INNER || existingJoinType == JoinType.RIGHT || newJoinType == JoinType.CROSS)
							return false;
						if(newJoinType == JoinType.INNER || newJoinType == JoinType.RIGHT || existingJoinType == JoinType.CROSS)
							return true;
						if(existingJoinType == JoinType.OPTIONAL)
							return true;
						if(newJoinType == JoinType.OPTIONAL)
							return false;
					}
					break;
				case ORDER:
					ListIterator<Integer> existingOrders = existingJoinPath.getOrders().listIterator(existingJoinPath.getOrders().size());
					ListIterator<Integer> newOrders = newJoinPath.getOrders().listIterator(newJoinPath.getOrders().size());
					while(existingOrders.hasPrevious() && newOrders.hasPrevious()) {
						int existingOrder = existingOrders.previous();
						int newOrder = newOrders.previous();
						if(existingOrder > newOrder)
							return true;
						if(existingOrder < newOrder)
							return false;
					}
					break;
			}
		}

    	return false;
    }

	protected int getCardinalityScore(JoinCardinality cardinality) {
		// 10 = all one-to-one joins, 1 = some one-to-many in same direction, 1000 = many-to-many overall
		// Note: We make the one-to-one join a worse score than one-to-many because most times one-to-one shows current state and one-to-many shows historically accurate state
		switch(cardinality) {
			case ONE_TO_ONE:
				return 10;
			case ONE_TO_MANY:
			case MANY_TO_ONE:
				return 1;
			case MANY_TO_MANY:
			default:
				return 1000;
		}
	}
    /** Loads the specified join into the map of custom defined joins for the two given
     * tables
     * @param table1 The name of the first table
     * @param table2 The name of the second table
     * @param join The join string
     */
	public void loadJoin(Table table1, Table table2, String expression, int order, JoinType joinType, JoinCardinality joinCardinality) {
		if(table1 != null)
			loadOneWayJoin(table1, table2, expression, order, joinType, joinCardinality);
		if(table2 != null)
			loadOneWayJoin(table2, table1, expression, order, joinType.getReverse(), joinCardinality != null ? joinCardinality.getReverse() : null);
    }
    /** Loads the specified join into the map of custom defined joins for the two given
     * tables
     * @param table1 The name of the first table
     * @param table2 The name of the second table
     * @param join The join string
     */
    public void loadJoin(Join join) {
		if(join.getTable1() != null)
			loadOneWayJoin(join);
		if(join.getTable2() != null)
			loadOneWayJoin(join.getReverse());
    }

    public void clearJoins() {
        extraJoins.clear();
        joinPaths.clear();
    }

	public Expression getUniqueExpression(Table table) {
		return uniqueExpressions.get(table);
	}

	public void loadUniqueExpression(Table table, String expression) {
		uniqueExpressions.put(table, createExpression(expression, table.getDataSourceName(), caseSensitive));
	}

	public void clearUniqueExpressions() {
		uniqueExpressions.clear();
	}
    /** Formats the sql expression, aliasing any tables found there-in (and updating the tables Map accordingly)
     * @param expression the sql expression to parse
     * @param tables the map into which newly found tables are added with their aliases
     * @param visibleTables the map from which already aliased tables are looked up
     * @param metaData
     * @return The formatted sql expression
     */
    public String formatExpression(String expression, Map<Table,String> tables, Map<Table,String> visibleTables, DatabaseMetaData metaData) {
    	String lastAlias = null;
        if(!visibleTables.isEmpty())
        	lastAlias = Collections.max(visibleTables.values());
        Collection<Table> c = findTables(expression);
        for(Table tab : c) {
            //check if tab is really a table in db
            if(!visibleTables.containsKey(tab) && !tables.containsKey(tab) && checkTable(tab, metaData)) {
            	lastAlias = createAlias(lastAlias);
                tables.put(tab, lastAlias);
            }
        }
        return replaceAliases(expression, tables, null);
    }

    /**
     * @return Returns the caseSensitive.
     */
    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    /**
     * @return Returns the comparator.
     */
    public Comparator<? super String> getComparator() {
        return comparator;
    }

    /**
     * @return Returns the tableComparator.
     */
    public Comparator<? super Table> getTableComparator() {
        return tableComparator;
    }

    public void printJoinPaths(PrintStream out) {
    	for(Map.Entry<Table,Map<Table,JoinPath>> entry1 : joinPaths.entrySet()) {
    		for(Map.Entry<Table,JoinPath> entry2 : entry1.getValue().entrySet()) {
    			out.print(entry1.getKey());
    			out.print('\t');
    			out.print(entry2.getKey());
    			out.print('\t');
    			boolean first = true;
    			for(Join join : entry2.getValue().getJoins()) {
    				if(first) first = false;
    				else out.print(" AND ");
    				out.print(join.getExpression().replaceAll("\\s", " "));
    			}
    			out.println();
    		}
    	}
    }

    public List<Join> makeJoinsFromFKs(DatabaseMetaData metaData, Set<Table> tables) throws SQLException {
    	List<Join> joins = new ArrayList<Join>();
    	Table[] tableArr = tables.toArray(new Table[tables.size()]);
    	class JoinInfo {
			public String pkColumn;
			public String fkColumn;
		}
		Map<String,List<JoinInfo>> infos = new HashMap<String, List<JoinInfo>>();
		StringBuilder expression = new StringBuilder();
    	for(int i = 0; i < tableArr.length; i++) {
    		for(int k = 0; k < tableArr.length; k++) {
    			if(i != k) {
    				ResultSet rs = metaData.getCrossReference(null, tableArr[i].getSchemaName(), tableArr[i].getTableName(),
    						null, tableArr[k].getSchemaName(), tableArr[k].getTableName());
    				try {
    					infos.clear();
    					while(rs.next()) {
    						String fkName = rs.getString("FK_NAME");
    						List<JoinInfo> jis = infos.get(fkName);
    						if(jis == null) {
    							jis = new ArrayList<JoinInfo>();
    							infos.put(fkName, jis);
    						}
    						JoinInfo ji = new JoinInfo();
    						ji.pkColumn = rs.getString("PKCOLUMN_NAME");
    						ji.fkColumn = rs.getString("FKCOLUMN_NAME");
    						jis.add(ji);
    					}
    					for(List<JoinInfo> jis : infos.values()) {
    						Join join = new Join();
	    	                join.setTable1(tableArr[i]);
	    	                join.setTable2(tableArr[k]);
	    	                boolean left = false;
	    	                expression.setLength(0);
	    	                for(JoinInfo ji : jis) {
	    	                	if(expression.length() > 0) expression.append(" AND ");
	    	                	expression.append(tableArr[i].toString()).append('.').append(ji.pkColumn)
	    	                		.append(" = ").append(tableArr[k].toString()).append('.').append(ji.fkColumn);
	    	                	if(!left) left = isNullable(metaData, tableArr[k].getSchemaName(), tableArr[k].getTableName(), ji.fkColumn);
	    	                }
	    	                join.setExpression(expression.toString());
	    	                if(left) join.setJoinType(JoinType.LEFT);
	    	                joins.add(join);
	    				}
    				} finally {
    	                rs.close();
    	            }
    			}
    		}
    	}
    	return joins;
    }

    public boolean isNullable(DatabaseMetaData metaData, String schema, String table, String column) throws SQLException {
    	ResultSet rs = metaData.getColumns(null, schema, table, column);
    	try {
    		if(!rs.next()) throw new SQLException("Column '" + schema + "." + table + "." + column + "' not Found");
    		switch(rs.getInt("NULLABLE")) {
    			case DatabaseMetaData.columnNoNulls:
    				return false;
    			case DatabaseMetaData.columnNullable:
    				return true;
    			default:
    				return false;
    		}
    	} finally {
    		rs.close();
    	}
    }
}
