package simple.sql;

import simple.lang.EnumCharValueLookup;
import simple.lang.InvalidValueException;

public enum JoinCardinality {
	ONE_TO_ONE('='),
	ONE_TO_MANY('<'),
	MANY_TO_ONE('>'),
	MANY_TO_MANY('X'),
 UNKNOWN('?')
	;
	private final char value;
	private static final EnumCharValueLookup<JoinCardinality> lookup = new EnumCharValueLookup<JoinCardinality>(JoinCardinality.class);

	private JoinCardinality(char value) {
		this.value = value;
	}

	public char getValue() {
		return value;
	}

	public static JoinCardinality getByValue(char value) throws InvalidValueException {
		return lookup.getByValue(value);
	}

	public static JoinCardinality getByValue(int value) throws InvalidValueException {
		return getByValue((char) value);
	}

	public JoinCardinality getReverse() {
		switch(this) {
			case ONE_TO_MANY:
				return JoinCardinality.MANY_TO_ONE;
			case MANY_TO_ONE:
				return JoinCardinality.ONE_TO_MANY;
			default:
				return this;

		}
	}

	public boolean isLinkable(JoinCardinality next) {
		if(next == JoinCardinality.ONE_TO_ONE || this == JoinCardinality.ONE_TO_ONE)
			return true;
		if(next == JoinCardinality.MANY_TO_MANY || this == JoinCardinality.MANY_TO_MANY)
			return false;
		return this == next;
	}

	public JoinCardinality getCombination(JoinCardinality next) {
		if(this == next)
			return this;
		if(next == JoinCardinality.MANY_TO_MANY || this == JoinCardinality.MANY_TO_MANY)
			return JoinCardinality.MANY_TO_MANY;
		if(next == JoinCardinality.UNKNOWN || this == JoinCardinality.UNKNOWN)
			return JoinCardinality.UNKNOWN;
		if(next == JoinCardinality.ONE_TO_ONE)
			return this;
		if(this == JoinCardinality.ONE_TO_ONE)
			return next;
		if(this == JoinCardinality.ONE_TO_MANY && next == JoinCardinality.MANY_TO_ONE)
			return JoinCardinality.UNKNOWN;
		return JoinCardinality.MANY_TO_MANY;

	}
}
