/*
 * Created on Jun 10, 2005
 *
 */
package simple.sql;

import simple.bean.ConvertUtils;

public class SQLType implements Cloneable {
    protected int typeCode;
    protected String typeName;
    protected Integer precision;
    protected Integer scale;

    public SQLType() {
    }
    public SQLType(int sqlType) {
        this();
        setTypeCode(sqlType);
    }
    public SQLType(int sqlType, String typeName) {
        this();
        setTypeCode(sqlType);
        setTypeName(typeName);
    }

    public SQLType(int sqlType, String typeName, Integer precision, Integer scale) {
        this();
        setTypeCode(sqlType);
        setTypeName(typeName);
        setPrecision(precision);
        setScale(scale);
    }
    /**
     * @return Returns the sqlType.
     */
    public int getTypeCode() {
        return typeCode;
    }
    /**
     * @param sqlType The sqlType to set.
     */
    public void setTypeCode(int sqlType) {
        this.typeCode = sqlType;
        checkTypeName();
    }
    /**
     * @return Returns the typeName.
     */
    public String getTypeName() {
    	return typeName;
    }
    /**
     * @param typeName The typeName to set.
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
        if(typeName == null)
        	checkTypeName();
    }
    protected void checkTypeName() {
    	if(typeName == null) {
			String tmp = SQLTypeUtils.getTypeCodeName(typeCode);
			if(tmp != null)
				this.typeName = tmp;
        }
    }
    @Override
	public String toString() {
    	StringBuilder sb = new StringBuilder();
    	if(getTypeName() == null && getTypeCode() != 0) {
			sb.append(SQLTypeUtils.getTypeCodeName(getTypeCode()));
    	} else {
    		sb.append(getTypeName());
    	}
    	if(getPrecision() != null) {
    		sb.append('(').append(getPrecision());
    		if(getScale() != null) {
    			sb.append(',').append(getScale());
    		}
    		sb.append(')');
    	}
        return sb.toString();
    }
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SQLType) {
			SQLType sqlType = (SQLType)obj;
			return getTypeCode() == sqlType.getTypeCode()
				&& ConvertUtils.areEqual(getPrecision(), sqlType.getPrecision())
				&& ConvertUtils.areEqual(getScale(), sqlType.getScale())
				&& ConvertUtils.areEqual(getTypeName(), sqlType.getTypeName());
		}
		return false;
	}
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	public Integer getScale() {
		return scale;
	}
	public void setScale(Integer scale) {
		this.scale = scale;
	}
	public Integer getPrecision() {
		return precision;
	}
	public void setPrecision(Integer precision) {
		this.precision = precision;
	}
	@Override
	public SQLType clone() {
		try {
			return (SQLType)super.clone();
		} catch(CloneNotSupportedException e) {
			throw new LinkageError("Object is cloneable");
		}
	}
}
