/**
 * 
 */
package simple.sql;

public interface PrimaryKeyInfo {
	public ColumnInfo[] getColumnInfos() ;
	public String getName() ;
}