package simple.sql;

public interface TableInfo {
	public DatabaseInfo getDatabaseInfo() ;
	public String getDataSourceName() ;
	public String getSchemaName() ;
	public String getTableName() ;
	public ColumnInfo[] getColumnInfos() ;
	public PrimaryKeyInfo getPrimaryKeyInfo() ;
	public ForeignKeyInfo[] getForeignKeyInfos() ;
	public ForeignKeyInfo[] getForeignKeyInfos(ColumnInfo columnInfo) ;
	
}
