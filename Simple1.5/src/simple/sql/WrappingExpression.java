package simple.sql;

import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.db.Parameter;

public class WrappingExpression extends AbstractExpression {
	protected String prefix;
	protected Expression inner;
	protected String suffix;
	public WrappingExpression(String prefix, Expression inner, String suffix) {
		this.prefix = prefix;
		this.inner = inner;
		this.suffix = suffix;
	}

	public void format(Map<? extends Expression, String> replacements, Aliaser aliaser, StringBuilder appendTo) {
		appendTo.append(prefix);
		inner.format(replacements, aliaser, appendTo);
		appendTo.append(suffix);
	}

	public Set<Column> getColumns() {
		return inner.getColumns();
	}
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	@Override
	public String toString() {
		return prefix + inner.toString() + suffix;
	}
	public void invalidate() {
		inner.invalidate();
	}
	public boolean replaceTable(Table original, Table replacement) {
		return inner.replaceTable(original, replacement);
	}
	
	public WrappingExpression copy() {
		WrappingExpression copy = (WrappingExpression) super.clone();
		copy.inner = copy.inner.copy();
		return copy;
	}

	@Override
	public List<Parameter> getParameters() {
		return inner.getParameters();
	}
}
