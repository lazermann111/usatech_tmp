package simple.sql;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.db.Parameter;
import simple.lang.Initializer;

public class BaseExpression extends AbstractExpression {
	protected final StringBuilder sql = new StringBuilder();
	protected final Set<Column> columns = new HashSet<Column>();
	protected final List<Object> parts = new ArrayList<Object>();
	protected final List<Parameter> parameters = new ArrayList<Parameter>();
	protected final Set<Column> columnsUnmod = Collections.unmodifiableSet(columns);
	protected final List<Parameter> parametersUnmod = Collections.unmodifiableList(parameters);

	protected final Initializer<RuntimeException> validator = new Initializer<RuntimeException>() {
		@Override
		protected void doInitialize() throws RuntimeException {
			sql.setLength(0);
			for(Object o : parts) {
				if(o instanceof Parameter) {
					Parameter p = (Parameter) o;
					sql.append(':').append(p.getPropertyName());
					if(p.getSqlType() != null)
						sql.append("::").append(p.getSqlType().toString());
				} else
					sql.append(o);
			}
		}
		@Override
		protected void doReset() {
		}
	};
	
	public BaseExpression(Object... parts) {
		super();
		appendParts(parts);
	}
	public BaseExpression(List<Object> parts) {
		this(parts.toArray(new Object[parts.size()]));
	}
	
	public void invalidate() {
		validator.reset();
	}
	
	protected void validate() {
		try {
			validator.initialize();
		} catch(InterruptedException e) {
			throw new UndeclaredThrowableException(e);
		}
	}
	
	protected void appendParts(Object... parts) {
		addParts(this.parts.size(), parameters.size(), parts);
	}

	protected void prependParts(Object... parts) {
		addParts(0, 0, parts);
	}

	protected void addParts(int partIndex, int paramIndex, Object... parts) {
		if(parts != null) {
			for(Object part : parts) {
				this.parts.add(partIndex++, part);
				if(part instanceof Column)
					columns.add((Column) part);
				else if(part instanceof Parameter)
					parameters.add(paramIndex++, (Parameter) part);
				else if(part instanceof Expression)
					columns.addAll(((Expression) part).getColumns());
			}
		}
	}
	
	public void format(Map<? extends Expression, String> replacements, Aliaser aliaser, StringBuilder appendTo) {
		for(Object part : parts) {
			if(part instanceof Column)
				appendColumn((Column) part, replacements, aliaser, appendTo);
			else if(part instanceof Parameter)
				appendTo.append('?');
			else if(part instanceof Expression)
				appendExpression((Expression) part, replacements, aliaser, appendTo);
			else
				appendTo.append(part);
		}
	}

	@Override
	public int hashCode() {
		int hash = 0;
		for(Object o : parts)
			hash += o.hashCode();
		return hash;
	}
	
	@Override
	public String toString() {
		validate();
		return sql.toString();
	}

	public boolean replaceTable(Table original, Table replacement) {
		boolean found = false;
		for(Object part : parts) {
			if(part instanceof Expression) {
				found = found || ((Expression)part).replaceTable(original, replacement);
			}
		}
		return found;
	}
	
	public BaseExpression copy() {
		Object[] partsCopy = parts.toArray(new Object[parts.size()]);
		for(int i = 0; i < partsCopy.length; i++) {
			if(partsCopy[i] instanceof Column)
				partsCopy[i] = ((Column) partsCopy[i]).copy();
			else if(partsCopy[i] instanceof Parameter)
				partsCopy[i] = ((Parameter) partsCopy[i]).clone();
			else if(partsCopy[i] instanceof Expression)
				partsCopy[i] = ((Expression) partsCopy[i]).copy();
		}
		return new BaseExpression(partsCopy);
	}

	@Override
	public List<Parameter> getParameters() {
		return parametersUnmod;
	}

	@Override
	public Set<Column> getColumns() {
		return columnsUnmod;
	}
}
