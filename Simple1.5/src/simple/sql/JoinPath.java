package simple.sql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JoinPath {
	protected final List<Join> joins = new ArrayList<Join>();
	protected final List<Join> unmodJoins = Collections.unmodifiableList(joins);
	protected final List<Integer> orders = new ArrayList<Integer>();
	protected final List<Integer> unmodOrders = Collections.unmodifiableList(orders);
	public JoinPath() {
		super();
	}

	public void appendJoin(Join join) {
		if(!joins.isEmpty()) {
			Join prev = joins.get(joins.size()-1);
			if(!prev.getTable2().equals(join.getTable1()))
				throw new IllegalArgumentException("First table in this join ("+ join.getTable1() 
						+ ") does not match previous join's second table (" + prev.getTable2() + ")");
		}
		joins.add(join);
		orders.add(join.getOrder());
		Collections.sort(orders);
	}

	public void prependJoin(Join join) {
		if(!joins.isEmpty()) {
			Join prev = joins.get(0);
			if(!prev.getTable1().equals(join.getTable2()))
				throw new IllegalArgumentException("Second table in this join ("+ join.getTable2() 
						+ ") does not match next join's first table (" + prev.getTable1() + ")");
		}
		joins.add(0, join);
		orders.add(join.getOrder());
		Collections.sort(orders);
	}

	public List<Integer> getOrders() {
		return unmodOrders;
	}	
	
	public Table getStartTable() {
		return joins.isEmpty() ? null : joins.get(0).getTable1();
	}
	
	public Table getEndTable() {
		return joins.isEmpty() ? null : joins.get(joins.size()-1).getTable2();
	}

	public JoinType getJoinType() {
		if(joins.isEmpty())
			return null;
		JoinType joinType = null;
		for(Join join : joins) {
			if(join.getJoinType() == JoinType.CROSS)
				return join.getJoinType();
			if(joinType == null)
				joinType = join.getJoinType();
			else
				joinType = joinType.getCombination(join.getJoinType());
		}
		return joinType;
	}
	public JoinPath cloneWithViewFirst(String viewName) {
		JoinPath clone = new JoinPath();
		if(!joins.isEmpty()) {
			Join join = joins.get(0).clone();
			Table tab = join.getTable1();
			join.setTable1(new Table(tab.getDataSourceName(), tab.getSchemaName(), tab.getTableName(), viewName));
			clone.joins.add(join);
			clone.joins.addAll(joins.subList(1,joins.size()));
			clone.orders.addAll(orders);
		}
		return clone;
	}
	
	public JoinPath cloneWithViewLast(String viewName) {
		JoinPath clone = new JoinPath();
		if(!joins.isEmpty()) {
			Join join = joins.get(joins.size() - 1).clone();
			Table tab = join.getTable2();
			join.setTable2(new Table(tab.getDataSourceName(), tab.getSchemaName(), tab.getTableName(), viewName));
			clone.joins.addAll(joins.subList(0, joins.size() - 1));
			clone.joins.add(join);
			clone.orders.addAll(orders);
		}
		return clone;
	}

	public JoinPath clone() {
		JoinPath clone = new JoinPath();
		clone.joins.addAll(joins);
		clone.orders.addAll(orders);
		return clone;
	}
	
	public List<Join> getJoins() {
		return unmodJoins;
	}
	
	public int getLength() {
		return joins.size();
	}

	/**
	 * @return
	 */
	public JoinCardinality getCardinality() {
		JoinCardinality cardinality = JoinCardinality.ONE_TO_ONE;
		for(Join join : joins)
			cardinality = cardinality.getCombination(join.getJoinCardinality());
		return cardinality;
	}

	public int getStartGranularity() {
		int p = 0;
		for(Join join : joins)
			switch(join.getJoinCardinality()) {
				case MANY_TO_ONE:
					p--;
					break;
				case ONE_TO_MANY:
					p++;
					break;
				case ONE_TO_ONE:
					switch(join.getJoinType()) {
						case CROSS:
						case FULL:
						case RIGHT:
							p++;
					}
					break;
			}
		return p;
	}

	public int getEndGranularity() {
		int p = 0;
		for(Join join : joins)
			switch(join.getJoinCardinality()) {
				case ONE_TO_MANY:
					p--;
					break;
				case MANY_TO_ONE:
					p++;
					break;
				case ONE_TO_ONE:
					switch(join.getJoinType()) {
						case CROSS:
						case FULL:
						case LEFT:
							p++;
					}
					break;
			}
		return p;
	}
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Table tab = getStartTable();
		if(tab != null && getEndTable() != null) {
			sb.append(tab.toString());
			for(Join join : joins)
				sb.append(' ').append(join.getJoinCardinality().getValue()).append(' ').append(join.getTable2());
		}
		return sb.toString();
	}
}
