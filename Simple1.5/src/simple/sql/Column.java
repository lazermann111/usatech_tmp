/*
 * Created on Oct 5, 2005
 *
 */
package simple.sql;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.db.Parameter;
import simple.xml.ObjectBuilder;

public class Column extends AbstractExpression implements Comparable<Column>, Expression, Serializable {
    private static final long serialVersionUID = 1765165226793804028L;
	protected String columnName;
    protected Table table;
    protected Set<Column> columns;
    public Column() {
    	this(new Table(), "");
    }
    
    public Column(String dataSourceName, String schemaName, String tableName, String columnName) {
        this(new Table(dataSourceName, schemaName, tableName), columnName);
    }
    public Column(Table table, String columnName) {
        setTable(table);
        setColumnName(columnName);
    }

	@Override
	public List<Parameter> getParameters() {
		return EMPTY_PARAMETERS;
	}
    public String getColumnName() {
        return columnName;
    }
    public String getDataSourceName() {
        return getTable().getDataSourceName();
    }
    public String getSchemaName() {
        return getTable().getSchemaName();
    }
    public String getTableName() {
        return getTable().getTableName();
    }
    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
    	if(table == null) throw new NullPointerException("Table cannot be null");
        this.table = table;
    }

    @Override
    public boolean equals(Object obj) {
    	if(obj == this) return true;
    	if(!(obj instanceof Column)) return super.equals(obj);
        Column col = (Column)obj;
        return getTable().equals(col.getTable())
            && getColumnName().equalsIgnoreCase(col.getColumnName());
    }
    @Override
    public int hashCode() {
        return getTable().hashCode() * 10 + getColumnName().toUpperCase().hashCode();
    }
    @Override
    public String toString() {
        return getTable().toString() + '.' + getColumnName();
    }
    public void setColumnName(String columnName) {
        this.columnName = (columnName == null ? "" : columnName.trim());
    }
    public void readXML(ObjectBuilder builder) {
        setColumnName(builder.get("column", String.class));        
        getTable().readXML(builder);
    }
    public void addXMLAttributes(Map<String,String> atts) {
        getTable().addXMLAttributes(atts);
        atts.put("column", getColumnName());       
    }

    public int compareTo(Column o) {
        int i = getTable().compareTo(o.getTable());
        if(i != 0) return i;
        return getTable().isCaseSensitive() ? 
                getColumnName().compareTo(o.getColumnName()) : 
                getColumnName().compareToIgnoreCase(o.getColumnName());
    }
    
    public Column copy() {
        return new Column(getTable().clone(), getColumnName());
    }

	public void format(Map<? extends Expression, String> replacements, Aliaser aliaser, StringBuilder appendTo) {
		appendColumn(this, replacements, aliaser, appendTo);
	}

	public Set<Column> getColumns() {
		if(columns == null) columns = Collections.singleton(this);
		return columns;
	}

	public void invalidate() {
		//no need to do anything
	}

	public boolean replaceTable(Table original, Table replacement) {
		if(table.equals(original)) {
			setTable(replacement);
			return true;
		}
		return false;
	}
}
