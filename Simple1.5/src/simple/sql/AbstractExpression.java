package simple.sql;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import simple.db.Parameter;

public abstract class AbstractExpression implements Expression, Cloneable {
	protected static final Map<Expression, String> EMPTY_REPLACEMENTS = Collections.emptyMap();
	protected static final List<Parameter> EMPTY_PARAMETERS = Collections.emptyList();
    
	protected AbstractExpression clone() {
		try {
			return (AbstractExpression) super.clone();
		} catch(CloneNotSupportedException e) {
			throw new InternalError("Cloneable");
		}
	}
		
	public void format(Aliaser aliaser, StringBuilder appendTo) {
		format(EMPTY_REPLACEMENTS, aliaser, appendTo);
	}

	public String format(Aliaser aliaser) {
		return format(EMPTY_REPLACEMENTS, aliaser);
	}
	
	public String format(Map<? extends Expression, String> replacements, Aliaser aliaser) {
		StringBuilder sb = new StringBuilder();
		format(replacements, aliaser, sb);
		return sb.toString();
	}
	
	protected void appendColumn(Column col, Map<? extends Expression, String> replacements, Aliaser aliaser, StringBuilder appendTo) {
		String rep = replacements.get(col);
		if(rep != null) 
			appendTo.append(rep);
		else {
			String alias = aliaser.getAlias(col.getTable());
			appendTo.append(alias).append('.').append(col.getColumnName());
		}
	}
	
	protected void appendExpression(Expression expression, Map<? extends Expression, String> replacements, Aliaser aliaser, StringBuilder appendTo) {
		String rep = replacements.get(expression);
		if(rep != null) 
			appendTo.append(rep);
		else {
			expression.format(replacements, aliaser, appendTo);
		}
	}
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		return (obj instanceof Expression && toString().equals(obj.toString()));
	}

	@Override
	public int hashCode() {
		String s = toString();
		return s == null ? 0 : s.hashCode();
	}
}
