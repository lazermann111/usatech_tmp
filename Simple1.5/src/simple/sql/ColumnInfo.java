package simple.sql;

public interface ColumnInfo {
	public TableInfo getTableInfo() ;
	public String getColumnName() ;
	public SQLType getSqlType() ;
	public boolean isNullable() ;
	public boolean isPrimaryKey() ;
	public boolean isForeignKey() ;	
}
