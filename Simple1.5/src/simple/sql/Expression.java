package simple.sql;

import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.db.Parameter;
import simple.util.Copyable;

public interface Expression extends Copyable<Expression> {
	public List<Parameter> getParameters();
	public Set<Column> getColumns() ;

	public void format(Aliaser aliaser, StringBuilder appendTo);

	public String format(Aliaser aliaser);

	public void format(Map<? extends Expression, String> replacements, Aliaser aliaser, StringBuilder appendTo);

	public String format(Map<? extends Expression, String> replacements, Aliaser aliaser);
	
	public boolean replaceTable(Table original, Table replacement) ;
	/**
	 * Tells the expression object to recalculate its toString() representation
	 */
	public void invalidate() ;
	
	public Expression copy() ;
}
