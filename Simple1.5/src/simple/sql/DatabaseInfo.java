/*
 * Created on Dec 27, 2005
 *
 */
package simple.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceFactory;

public class DatabaseInfo {
    protected DataSourceFactory dataSourceFactory;
    protected static boolean contains(ColumnInfo[] columnInfos, ColumnInfo columnInfo) {
    	for(ColumnInfo ci : columnInfos) {
    		if(ci.equals(columnInfo))
    			return true;
    	}
    	return false;
    }
    protected class InnerTableInfo implements TableInfo {
    	protected ColumnInfo[] columnInfos;
    	protected String dataSourceName;
    	protected String tableName;
    	protected String schemaName;
    	protected ForeignKeyInfo[] foreignKeyInfos;
    	protected PrimaryKeyInfo primaryKeyInfo;
    	
		public DatabaseInfo getDatabaseInfo() {
			return DatabaseInfo.this;
		}

		public ForeignKeyInfo[] getForeignKeyInfos(ColumnInfo columnInfo) {
			List<ForeignKeyInfo> fkis = new ArrayList<ForeignKeyInfo>();
			for(ForeignKeyInfo fki : getForeignKeyInfos()) {
				if(contains(fki.getReferringColumns(), columnInfo))
					fkis.add(fki);
			}
			return fkis.toArray(new ForeignKeyInfo[fkis.size()]);
		}

		public ColumnInfo[] getColumnInfos() {
			return columnInfos;
		}


		public String getDataSourceName() {
			return dataSourceName;
		}


		public String getTableName() {
			return tableName;
		}


		public String getSchemaName() {
			return schemaName;
		}


		public ForeignKeyInfo[] getForeignKeyInfos() {
			return foreignKeyInfos;
		}


		public PrimaryKeyInfo getPrimaryKeyInfo() {
			return primaryKeyInfo;
		}   
		@Override
		public int hashCode() {
			return getDataSourceName().hashCode() ^ getSchemaName().hashCode() ^ getTableName().hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof TableInfo)) return false;
			TableInfo ti = (TableInfo) obj;
			return getDataSourceName().equals(ti.getDataSourceName()) && getSchemaName().equals(ti.getSchemaName()) && getTableName().equals(ti.getTableName());
		}
	    protected class InnerColumnInfo implements ColumnInfo {
	    	protected String columnName;
	    	protected SQLType sqlType;
	    	protected boolean nullable;
	    	
			
			public String getColumnName() {
				return columnName;
			}
	
			public SQLType getSqlType() {
				return sqlType;
			}
	
			public TableInfo getTableInfo() {
				return InnerTableInfo.this;
			}
	
			public boolean isNullable() {
				return nullable;
			}
	
			public boolean isForeignKey() {
				return getTableInfo().getForeignKeyInfos(this) != null;
			}
	
			public boolean isPrimaryKey() {
				PrimaryKeyInfo pki = getTableInfo().getPrimaryKeyInfo();
				return pki != null && contains(pki.getColumnInfos(), this);
			}
			@Override
			public int hashCode() {
				return getColumnName().hashCode() ^ getTableInfo().hashCode();
			}
			@Override
			public boolean equals(Object obj) {
				if(!(obj instanceof ColumnInfo)) return false;
				ColumnInfo ci = (ColumnInfo) obj;
				return getColumnName().equals(ci.getColumnName()) && getTableInfo().equals(ci.getTableInfo());
			}
	    }
    }
    protected class InnerPrimaryKeyInfo implements PrimaryKeyInfo {
    	protected ColumnInfo[] columnInfos;
    	protected String name;
		public ColumnInfo[] getColumnInfos() {
			return columnInfos;
		}
		public String getName() {
			return name;
		}
    	
    }
    protected class InnerForeignKeyInfo implements ForeignKeyInfo {
    	protected ColumnInfo[] foreignColumnInfos;
    	protected ColumnInfo[] referringColumnInfos;
    	protected String name;
		
    	protected String foreignDataSourceName;
    	protected String foreignSchemaName;
    	protected String foreignTableName;
    	protected String[] foreignColumnNames;
    	
		public ColumnInfo[] getForeignColumns() throws SQLException, DataLayerException {
			if(foreignColumnInfos == null) {
				TableInfo ti = getTableInfo(foreignDataSourceName, foreignSchemaName, foreignTableName);
				ColumnInfo[] fcis = new ColumnInfo[foreignColumnNames.length];
				ColumnInfo[] cis = ti.getColumnInfos();
				for(int i = 0; i < foreignColumnNames.length; i++) {
					fcis[i] = find(cis, foreignColumnNames[i]);
				}
			}
			return foreignColumnInfos;
		}

		public String getName() {
			return name;
		}

		public ColumnInfo[] getReferringColumns() {
			return referringColumnInfos;
		}
    	
    }
    public DatabaseInfo() {
    	this(DataLayerMgr.getDataSourceFactory());
    }
    
    protected static ColumnInfo find(ColumnInfo[] cis, String columnName) {
		for(ColumnInfo ci : cis)
			if(ci.getColumnName().equals(columnName))
				return ci;
		return null;
	}

	public DatabaseInfo(DataSourceFactory dataSourceFactory) {
        super();
        this.dataSourceFactory = dataSourceFactory;
    }

    public TableInfo getTableInfo(String dataSourceName, String schemaName, String tableName) throws SQLException, DataLayerException {
    	Connection conn = DataLayerMgr.getConnection(dataSourceName);
        try {
        	InnerTableInfo ti = new InnerTableInfo();
        	Map<String,ColumnInfo> cis = new LinkedHashMap<String,ColumnInfo>();
            ResultSet rs = conn.getMetaData().getColumns(null, schemaName, tableName, "%");
            try {
                while(rs.next()) {
                	ColumnInfo ci = makeColumnInfo(rs, ti); 
                    cis.put(ci.getColumnName(), ci); 
                }
            } finally {
                rs.close();
            }
            ti.columnInfos = cis.values().toArray(new ColumnInfo[cis.size()]);
            ti.dataSourceName = dataSourceName;
            ti.schemaName = schemaName;
            ti.tableName = tableName;
            InnerPrimaryKeyInfo pki = null;
            List<ColumnInfo> pkCols = null;
            rs = conn.getMetaData().getPrimaryKeys(null, schemaName, tableName);
            try {
                while(rs.next()) {
                	if(pki == null) {
                		 pki = new InnerPrimaryKeyInfo();
                		 pki.name = rs.getString("PK_NAME");
                		 pkCols = new ArrayList<ColumnInfo>();
                	}
                	pkCols.add(cis.get(rs.getString("COLUMN_NAME"))); 
                }
            } finally {
                rs.close();
            }
            if(pki != null) pki.columnInfos = pkCols.toArray(new ColumnInfo[pkCols.size()]);
            ti.primaryKeyInfo = pki;
            List<ForeignKeyInfo> fkis = new ArrayList<ForeignKeyInfo>();
            String foreignSchema = null;
            String foreignTable = null;
            InnerForeignKeyInfo fki = null;
            List<String> cns = new ArrayList<String>();
            List<ColumnInfo> rcis = new ArrayList<ColumnInfo>();
            rs = conn.getMetaData().getImportedKeys(null, schemaName, tableName);
            try {
            	while(rs.next()) {
                	String fs = rs.getString("PKTABLE_SCHEM");
                	String ft = rs.getString("PKTABLE_NAME");
                	if(!ConvertUtils.areEqual(fs, foreignSchema) || !ConvertUtils.areEqual(ft, foreignTable)) {
                		if(fki != null) {
                			fki.foreignColumnNames = cns.toArray(new String[cns.size()]);
                    		cns.clear();
                		}
                		fki = new InnerForeignKeyInfo();
                		fki.foreignDataSourceName = dataSourceName;
                		fki.foreignSchemaName = fs;
                		fki.foreignTableName = ft;
                		fki.name = rs.getString("FK_NAME");
                		fkis.add(fki);
                	}
                	cns.add(rs.getString("PKCOLUMN_NAME"));
                	rcis.add(cis.get(rs.getString("FKCOLUMN_NAME")));
                }
            } finally {
                rs.close();
            }
    		if(fki != null) {
    			fki.foreignColumnNames = cns.toArray(new String[cns.size()]);
        		cns.clear();
    		}
            ti.foreignKeyInfos = fkis.toArray(new ForeignKeyInfo[fkis.size()]);
            return ti;
        } finally {
        	conn.close();
        }
    }

    protected ColumnInfo makeColumnInfo(ResultSet rs, InnerTableInfo ti) throws SQLException {
    	InnerTableInfo.InnerColumnInfo ci = ti.new InnerColumnInfo();
    	ci.columnName = rs.getString("COLUMN_NAME");
    	ci.nullable = !rs.getString("IS_NULLABLE").equalsIgnoreCase("NO");
    	ci.sqlType = new SQLType(rs.getInt("DATA_TYPE"), rs.getString("TYPE_NAME"), rs.getInt("COLUMN_SIZE"), rs.getInt("DECIMAL_DIGITS"));
    	return ci;
    }

}
