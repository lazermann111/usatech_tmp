package simple.sql;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;
import simple.text.StringUtils;

public class JoinStrand {
	protected final Table table;
	protected final JoinType type;
	protected Expression expression;
	protected final Collection<JoinStrand> strands = new AbstractCollection<JoinStrand>() {
		@Override
		public Iterator<JoinStrand> iterator() {
			return new Iterator<JoinStrand>() {
				protected JoinStrand next = first;

				@Override
				public boolean hasNext() {
					return next != null;
				}

				@Override
				public JoinStrand next() {
					JoinStrand js = next;
					next = next.next;
					return js;
				}

				@Override
				public void remove() {
					throw new UnsupportedOperationException();
				}
			};
		}

		@Override
		public int size() {
			return count;
		}
	};
	protected JoinStrand parent;
	protected JoinStrand prev;
	protected JoinStrand next;
	protected JoinStrand first;
	protected JoinStrand last;
	protected int count;
	protected final JoinCardinality cardinalityFromParent;

	public JoinStrand(Table table, JoinType type, JoinCardinality cardinalityFromParent, Expression expression) {
		super();
		this.table = table;
		this.type = type;
		this.cardinalityFromParent = cardinalityFromParent;
		this.expression = expression;
	}

	public Table getTable() {
		return table;
	}

	public JoinType getType() {
		return type;
	}

	public Expression getExpression() {
		return expression;
	}

	public Collection<JoinStrand> getStrands() {
		return strands;
	}

	@Override
	public String toString() {
		return appendString(new StringBuilder(), 0).toString();
	}

	protected StringBuilder appendString(StringBuilder sb, int level) {
		for(int i = 0; i < level; i++)
			sb.append('\t');
		if(type != null)
			sb.append(type.toString()).append("> ");
		if(table != null) {
			if(!StringUtils.isBlank(table.getSchemaName()))
				sb.append(table.getSchemaName()).append('.');
			sb.append(table.getTableName());
			if(table.hasViewName())
				sb.append('[').append(table.getViewName()).append("]");
			if(expression != null)
				sb.append(" ON ").append(expression.toString());
		} else if(expression != null)
			sb.append(expression.toString());
		
		sb.append(SystemUtils.getNewLine());
		for(JoinStrand sub : strands)
			sub.appendString(sb, level + 1);
		return sb;
	}

	public void prependChild(JoinStrand child) {
		child.removeFromParent();

		child.parent = this;
		child.next = first;
		if(first != null)
			first.prev = child;
		else if(last == null)
			last = child;
		first = child;

		count++;
	}

	public void appendChild(JoinStrand child) {
		child.removeFromParent();

		child.parent = this;
		child.prev = last;
		if(last != null)
			last.next = child;
		else if(first == null)
			first = child;
		last = child;

		count++;
	}

	public void prependSibling(JoinStrand sibling) {
		sibling.removeFromParent();

		sibling.parent = parent;
		if(prev != null) {
			sibling.prev = prev;
			prev.next = sibling;
		} else
			parent.first = sibling;
		sibling.next = this;
		prev = sibling;

		parent.count++;
	}

	public void appendSibling(JoinStrand sibling) {
		sibling.removeFromParent();

		sibling.parent = parent;
		if(next != null) {
			sibling.next = next;
			next.prev = sibling;
		} else
			parent.last = sibling;
		sibling.prev = this;
		next = sibling;

		parent.count++;
	}

	protected void removeFromParent() {
		if(parent == null)
			return;
		parent.count--;
		if(next != null)
			next.prev = prev;
		else
			parent.last = prev;

		if(prev != null)
			prev.next = next;
		else
			parent.first = next;
		next = null;
		prev = null;
		parent = null;
	}

	public JoinStrand getParent() {
		return parent;
	}

	public JoinStrand getPrev() {
		return prev;
	}

	public JoinStrand getNext() {
		return next;
	}

	public JoinStrand getFirst() {
		return first;
	}

	public JoinStrand getLast() {
		return last;
	}

	public int getCount() {
		return count;
	}

	public boolean isAncestorOf(JoinStrand other) {
		if(other.parent == this)
			return true;
		if(other.parent == null)
			return false;
		return isAncestorOf(other.parent);
	}

	public boolean isPriorSiblingOf(JoinStrand other) {
		if(other.prev == this)
			return true;
		if(other.prev == null)
			return false;
		return isPriorSiblingOf(other.prev);
	}

	public void moveAfter(JoinStrand other) {
		int d1 = depth();
		int d2 = other.depth();
		JoinStrand sibling = this;
		for(; d1 > d2; d1--)
			sibling = sibling.parent;
		for(; d2 > d1; d2--)
			other = other.parent;
		if(sibling.isPriorSiblingOf(other))
			other.appendSibling(sibling);
	}

	public int depth() {
		int d = 0;
		for(JoinStrand js = this; js.parent != null; js = js.parent)
			d++;
		return d;
	}

	public void moveChildrenTo(JoinStrand oldParent) {
		if(first == null)
			first = oldParent.first;
		if(last == null)
			last = oldParent.last;
		else {
			last.next = oldParent.first;
			if(oldParent.first != null)
				oldParent.first.prev = last;
			last = oldParent.last;
		}
		count += oldParent.count;
		for(JoinStrand js = first; js != null; js = js.next)
			js.parent = this;
		oldParent.first = null;
		oldParent.last = null;
		oldParent.count = 0;
	}

	public void appendExpression(Expression extraExpression) {
		if(expression == null)
			expression = extraExpression;
		else
			expression = new BaseExpression(expression, " AND ", extraExpression);
	}

	public JoinCardinality getCardinality() {
		return getCardinality(true, true, true);
	}

	protected JoinCardinality getCardinality(boolean checkChildren, boolean checkSiblings, boolean checkParent) {
		JoinCardinality cardinality = cardinalityFromParent;
		if(checkChildren) {
			for(JoinStrand child : getStrands()) {
				cardinality = cardinality.getCombination(child.getCardinality(true, false, false).getReverse());
			}
		}
		if(parent == null)
			return cardinality;
		if(checkParent) {
			cardinality = cardinality.getCombination(parent.getCardinality(false, true, true));
		}
		if(checkSiblings) {
			for(JoinStrand sibling = prev; sibling != null; sibling = sibling.prev) {
				cardinality = cardinality.getCombination(sibling.getCardinality(true, false, false).getReverse());
			}
			for(JoinStrand sibling = next; sibling != null; sibling = sibling.next) {
				cardinality = cardinality.getCombination(sibling.getCardinality(true, false, false).getReverse());
			}
		}
		return cardinality;
	}

	public boolean isManyToMany(JoinCardinality startCardinality) {
		return isManyToMany(startCardinality, true, true, true);
	}

	protected boolean isManyToMany(JoinCardinality startCardinality, boolean checkChildren, boolean checkSiblings, boolean checkParent) {
		startCardinality = startCardinality.getCombination(cardinalityFromParent);
		if(startCardinality == JoinCardinality.MANY_TO_MANY)
			return true;
		if(checkChildren) {
			for(JoinStrand child : getStrands()) {
				if(child.isManyToMany(startCardinality, true, false, false))
					return true;
			}
		}
		if(parent == null)
			return false;
		if(checkParent) {
			if(parent.isManyToMany(startCardinality, false, true, true))
				return true;
		}
		if(checkSiblings) {
			for(JoinStrand sibling = prev; sibling != null; sibling = sibling.prev) {
				if(sibling.isManyToMany(startCardinality, true, false, false))
					return true;
			}
			for(JoinStrand sibling = next; sibling != null; sibling = sibling.next) {
				if(sibling.isManyToMany(startCardinality, true, false, false))
					return true;
			}
		}
		return false;
	}

	protected int getGranularity(boolean checkChildren, boolean checkSiblings, boolean checkParent) {
		int g = 0;
		if(checkChildren) {
			for(JoinStrand child : getStrands()) {
				int p = 0;
				switch(child.cardinalityFromParent) {
					case MANY_TO_ONE:
					case MANY_TO_MANY:
						continue;
					case ONE_TO_MANY:
						p++;
						break;
					case ONE_TO_ONE:
						switch(child.getType()) {
							case CROSS:
							case FULL:
							case RIGHT:
								p++;
						}
						break;
				}

				g = Math.max(g, p + child.getGranularity(true, false, false));
			}
		}
		if(parent != null) {
			int p = 0;
			switch(cardinalityFromParent) {
				case ONE_TO_MANY:
				case MANY_TO_MANY:
					return g;
				case MANY_TO_ONE:
					p++;
					break;
				case ONE_TO_ONE:
					switch(getType()) {
						case CROSS:
						case FULL:
						case LEFT:
							p++;
					}
					break;
			}
			g = Math.max(g, p);
			if(checkParent) {
				g = Math.max(g, p + parent.getGranularity(false, false, true));
			}
			if(checkSiblings) {
				for(JoinStrand sibling = prev; sibling != null; sibling = sibling.prev) {
					g = Math.max(g, p + sibling.getGranularity(true, false, false));
				}
				for(JoinStrand sibling = next; sibling != null; sibling = sibling.next) {
					g = Math.max(g, p + sibling.getGranularity(true, false, false));
				}
			}
		}
		return g;
	}

	public int getGranularity() {
		/*
		int g = 1;
		OUTER: for(JoinStrand p = this; p != null; p = p.parent) {
			switch(p.cardinalityFromParent) {
				case ONE_TO_MANY:
				case MANY_TO_MANY:
					break OUTER;
				case MANY_TO_ONE:
					g++;
					break;
				case ONE_TO_ONE:
					break;
			}
		}
		int childG = getChildGranularity();
		if(g < childG)
			g = childG;
		return g;
		*/
		return getGranularity(true, true, true);
	}

	protected int getChildGranularity() {
		int maxG = 1;
		for(JoinStrand child : getStrands()) {
			int g;
			switch(child.cardinalityFromParent) {
				case MANY_TO_ONE:
				case MANY_TO_MANY:
					g = 1;
					break;
				case ONE_TO_MANY:
					g = 1 + child.getChildGranularity();
					break;
				case ONE_TO_ONE:
				default:
					g = child.getChildGranularity();
					break;
			}
			if(g > maxG)
				maxG = g;
		}
		return maxG;
	}

	public boolean isLowestGranularity(Table table) {
		JoinStrand top = this;
		while(top.parent != null)
			top = top.parent;
		// find the table
		JoinStrand js = top.findChild(table);
		if(js == null)
			return false;

		// determine cardinalities to it
		return js.isLowestGranularity(true, true, true);
	}

	protected boolean isLowestGranularity(boolean checkChildren, boolean checkSiblings, boolean checkParent) {
		if(checkChildren) {
			for(JoinStrand child : getStrands()) {
				switch(child.cardinalityFromParent) {
					case MANY_TO_MANY:
					case ONE_TO_MANY:
						return false;
				}
				if(!child.isLowestGranularity(true, false, false))
					return false;
			}
		}
		if(parent == null)
			return true;
		if(checkParent) {
			switch(cardinalityFromParent) {
				case MANY_TO_MANY:
				case MANY_TO_ONE:
					return false;
			}
			if(!parent.isLowestGranularity(false, true, true))
				return false;
		}
		if(checkSiblings) {
			for(JoinStrand sibling = prev; sibling != null; sibling = sibling.prev) {
				switch(sibling.cardinalityFromParent) {
					case MANY_TO_MANY:
					case MANY_TO_ONE:
						return false;
				}
				if(!sibling.isLowestGranularity(true, false, false))
					return false;
			}
			for(JoinStrand sibling = next; sibling != null; sibling = sibling.next) {
				switch(sibling.cardinalityFromParent) {
					case MANY_TO_MANY:
					case MANY_TO_ONE:
						return false;
				}
				if(!sibling.isLowestGranularity(true, false, false))
					return false;
			}
		}
		return true;
	}

	protected JoinStrand findChild(Table table) {
		if(ConvertUtils.areEqual(this.table, table))
			return this;
		for(JoinStrand child : getStrands()) {
			JoinStrand found = child.findChild(table);
			if(found != null)
				return found;
		}
		return null;
	}
}
