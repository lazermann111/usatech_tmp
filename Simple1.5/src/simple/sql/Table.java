/*
 * Created on Oct 5, 2005
 *
 */
package simple.sql;

import java.io.Serializable;
import java.util.Map;

import simple.xml.ObjectBuilder;

public class Table implements Comparable<Table>, Serializable {
    private static final long serialVersionUID = -2559409920558767339L;
	protected String dataSourceName = "";
    protected String schemaName = "";
    protected String tableName = "";
    protected String viewName = "";
    protected boolean caseSensitive = false;
    protected int hashCode; // store this for quick access
    public Table() {    
    }
    public Table(String dataSourceName, String schemaName, String tableName, String viewName) {
        this(dataSourceName, schemaName, tableName, viewName, false);
    }
    public Table(String dataSourceName, String schemaName, String tableName) {
        this(dataSourceName, schemaName, tableName, "");
    }
    public Table(String dataSourceName, String schemaName, String tableName, boolean caseSensitive) {
        this(dataSourceName, schemaName, tableName, "", caseSensitive);
    }
    public Table(String dataSourceName, String schemaName, String tableName, String viewName, boolean caseSensitive) {
        setDataSourceName(dataSourceName);
        setSchemaName(schemaName);
        setTableName(tableName);
        setViewName(viewName);
        setCaseSensitive(caseSensitive);
    }
    public String getDataSourceName() {
        return dataSourceName;
    }
    public String getSchemaName() {
        return schemaName;
    }
    public String getTableName() {
        return tableName;
    }
    public String getViewName() {
        return viewName;
    }
    public void setViewName(String viewName) {
        this.viewName = (viewName == null ? "" : viewName.trim());
        calcHashCode();
    }
    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = (dataSourceName == null ? "" : dataSourceName.trim());
        calcHashCode();
    }
    public void setSchemaName(String schemaName) {
        this.schemaName = (schemaName == null ? "" : schemaName.trim());
        calcHashCode();
    }
    public void setTableName(String tableName) {
        this.tableName = (tableName == null ? "" : tableName.trim());
        calcHashCode();
    }   
    /**
     * @return Returns the caseSensitive.
     */
    public boolean isCaseSensitive() {
        return caseSensitive;
    }
    /**
     * @param caseSensitive The caseSensitive to set.
     */
    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }
    @Override
    public boolean equals(Object obj) {
        Table tab = (Table)obj;
        return hashCode == tab.hashCode && equalsBaseTable(tab)
            && partEquals(getViewName(), tab.getViewName());
    }
    
    protected boolean partEquals(String part1, String part2) {
        if(isCaseSensitive()) return part1.equals(part2);
        else return part1.equalsIgnoreCase(part2);
    }
    protected void calcHashCode() {
        hashCode = getDataSourceName().toUpperCase().hashCode() * 100 
            + getSchemaName().toUpperCase().hashCode() * 10 
            + getTableName().toUpperCase().hashCode()
            + getViewName().toUpperCase().hashCode() * 1000;
    }
    @Override
    public int hashCode() {
        return hashCode;
    }
    @Override
    public String toString() {
        return (getDataSourceName().length() > 0 ? getDataSourceName() + "." : "")
            + (getSchemaName().length() > 0 ? getSchemaName() + "." : "") + getTableName() 
            + (getViewName().length() > 0 ? "[" + getViewName() + "]": "");
    }
    public void readXML(ObjectBuilder builder) {
        setDataSourceName(builder.get("dataSource", String.class));
        setSchemaName(builder.get("schema", String.class));
        setTableName(builder.get("table", String.class));
        setViewName(builder.get("view", String.class));
    }
    
    public void addXMLAttributes(Map<String,String> atts) {
        atts.put("dataSource", getDataSourceName());
        atts.put("schema", getSchemaName());
        atts.put("table", getTableName());
        if(getViewName().length() > 0) 
            atts.put("view", getViewName());
    }

    public boolean hasViewName() {
        return getViewName().length() > 0;
    }
    
    public boolean equalsBaseTable(Table table) {
         return partEquals(getDataSourceName(), table.getDataSourceName())
             && partEquals(getSchemaName(), table.getSchemaName())
             && partEquals(getTableName(), table.getTableName());
    }
    protected int partCompareTo(String part1, String part2) {
        if(isCaseSensitive()) return part1.compareTo(part2);
        else return part1.compareToIgnoreCase(part2);
    }
    public int compareTo(Table o) {
        int i = partCompareTo(getDataSourceName(), o.getDataSourceName());
        if(i != 0) return i;
        i = partCompareTo(getSchemaName(), o.getSchemaName());
        if(i != 0) return i;
        i = partCompareTo(getTableName(), o.getTableName());
        if(i != 0) return i;
        return partCompareTo(getViewName(), o.getViewName());
    }
    
    public Table clone() {
        return new Table(getDataSourceName(), getSchemaName(), getTableName(), getViewName());
    }
}
