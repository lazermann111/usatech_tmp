/*
 * Created on Feb 3, 2006
 *
 */
package simple.sql;

public class BuildSQLException extends Exception {
    private static final long serialVersionUID = -1609712326203400559L;

    public BuildSQLException() {
        super();
    }

    public BuildSQLException(String message) {
        super(message);
    }

    public BuildSQLException(String message, Throwable cause) {
        super(message, cause);
    }

    public BuildSQLException(Throwable cause) {
        super(cause);
    }
}
