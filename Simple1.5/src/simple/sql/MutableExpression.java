package simple.sql;

public interface MutableExpression extends Expression {
	public void insert(Expression expression) ;
	public void append(Expression expression) ;		
}
