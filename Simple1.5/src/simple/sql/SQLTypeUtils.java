package simple.sql;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Types;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.converters.SQLTypeConverter;
import simple.db.CharacterStream;
import simple.db.ConnectionHolder;
import simple.db.DBHelper;
import simple.db.DataLayerMgr;
import simple.db.DataSourceNotFoundException;
import simple.io.BinaryStream;
import simple.io.Log;
import simple.lang.Holder;

public class SQLTypeUtils {
	private static final Log log = Log.getLog();
	protected static Map<Integer,String> typeCodeNames;
	protected static Map<String, Integer> typeNameCodes;
	static {
		loadTypeCodeNames();
	}

	public static int getTypeCode(String typeCodeName) throws NoSuchFieldException {
		Integer typeCode = typeNameCodes.get(typeCodeName.toUpperCase());
		if(typeCode == null)
			throw new NoSuchFieldException("Type '" + typeCodeName + "' is not valid");
		return typeCode;
	}

	public static SQLType getSqlType(String typeCode, String typeName, ConnectionHolder ch) throws IllegalArgumentException, SQLException, DataSourceNotFoundException {
		SQLType type = null;
		if(typeCode != null && (typeCode=typeCode.trim()).length() > 0) {
			try {
				type = SQLTypeConverter.convertToSQLType(typeCode);
	        } catch(NoSuchFieldException e) {
	            try {
	                type = new SQLType(Integer.parseInt(typeCode));
	            } catch(NumberFormatException e1) {
	            	if(typeName != null && !typeName.equalsIgnoreCase(typeCode))
	            		throw new IllegalArgumentException("Type Code '" + typeCode + "' is not recognized");
	            	else
	            		typeName = typeCode;
	            }
	        } catch(SecurityException e) {
	        	if(typeName != null && !typeName.equalsIgnoreCase(typeCode))
	        		throw new IllegalArgumentException("Type Code '" + typeCode + "' is not recognized");
	        	else
	        		typeName = typeCode;
	        } catch (IllegalArgumentException e) {
	            throw new RuntimeException("This should never happen");
	        } catch (IllegalAccessException e) {
	        	if(typeName != null && !typeName.equalsIgnoreCase(typeCode))
	        		throw new IllegalArgumentException("Type Code '" + typeCode + "' is not recognized");
	        	else
	        		typeName = typeCode;
	        } catch(ConvertException e) {
        		throw new IllegalArgumentException("Type Code '" + typeCode + "' is not recognized", e);
			}
	    }
		DBHelper helper = DataLayerMgr.getDBHelper(ch.getConnectionClass());
		if(helper == null)
			helper = DataLayerMgr.getDBHelper((Connection)null);
		return helper.translateSqlType(type, typeName, ch);
	}

	public static String getTypeCodeName(int typeCode) {
	    return typeCodeNames.get(typeCode);
	}

	public static Map<Integer, String> getTypeCodeNames() {
	    return typeCodeNames;
	}

	private static void loadTypeCodeNames() {
	    if(typeCodeNames == null) {
	    	Map<Integer,String> tcns = new HashMap<Integer, String>();
			Map<String, Integer> tncs = new HashMap<String, Integer>();
			Class<?>[] typeClasses = new Class[] { Types.class, AdditionalTypes.class };
			for(Class<?> cls : typeClasses) {
				Field[] fields = cls.getFields();
				for(int i = 0; i < fields.length; i++) {
					if(Modifier.isPublic(fields[i].getModifiers()) && int.class.isAssignableFrom(fields[i].getType()) && Modifier.isStatic(fields[i].getModifiers())) {
						try {
							int tc = fields[i].getInt(null);
							String tn = fields[i].getName();
							tcns.put(tc, tn);
							tncs.put(tn, tc);
						} catch(IllegalArgumentException e) {
							log.warn("Could not load type code and name", e);
						} catch(IllegalAccessException e) {
							log.warn("Could not load type code and name", e);
						}
					}
				}
			}
			typeNameCodes = Collections.unmodifiableMap(tncs);
	        typeCodeNames = Collections.unmodifiableMap(tcns);
	    }
	}

	public static Class<? extends Object> getJavaType(SQLType sqlType) {
        switch(sqlType.getTypeCode()) {
            case Types.ARRAY:
            	SQLType compType;
            	if(sqlType instanceof ArraySQLType && (compType=((ArraySQLType)sqlType).getComponentType()) != null) {
            		Class<?> clazz = getJavaType(compType);
                	return Array.newInstance(clazz, 0).getClass();
            	} else {
            		return Object[].class;
            	}
            //TODO: implement other complex types
            default:
                return getJavaType(sqlType.getTypeCode());
        }
    }

	/**  Returns the Java Class that the PreparedStatement Object expects for
	 *  the given SQLType.
	 * @return
	 * @param typeCode
	 */
	public static Class<? extends Object> getJavaType(int typeCode) {
	    switch(typeCode) {
	    	// simple types
	        case Types.CHAR:
	        case Types.VARCHAR:
	        case Types.LONGVARCHAR:
	            return String.class;
	        case Types.NUMERIC:
	        case Types.DECIMAL:
	            return java.math.BigDecimal.class;
	        case Types.BIT:
	            return boolean.class;
	        case Types.TINYINT:
	            return byte.class;
	        case Types.SMALLINT:
	            return int.class;
	        case Types.INTEGER:
	            return int.class;
	        case Types.BIGINT:
	            return long.class;
	        case Types.REAL:
	            return float.class;
	        case Types.FLOAT:
	        case Types.DOUBLE:
	            return double.class;
	        case Types.BINARY:
	        case Types.VARBINARY:
	        case Types.LONGVARBINARY:
	            return byte[].class;
	        case Types.DATE:
	            return java.util.Date.class;
	        case Types.TIME:
	            return java.util.Date.class;
	        case Types.TIMESTAMP:
	            return java.util.Date.class;
	        case Types.BLOB:
	            return String.class;
	        case Types.CLOB:
	            return String.class;
	        case Types.BOOLEAN:
	        	return boolean.class;
	        case Types.NULL:
	        	//return Void.class;
	        	return Object.class; //ie - no conversion
	        // complex types
	        case Types.ARRAY:
	        	return Object[].class;
	        case Types.REF:
	        	return Holder.class;
	        case Types.DATALINK:
	        	return Object.class; //???
	        case Types.OTHER:
	        case Types.JAVA_OBJECT:
		        return Object.class;
	        case Types.STRUCT:
	        	return Object.class;
			case AdditionalTypes.TIMESTAMPTZ:
				return Calendar.class;
			case AdditionalTypes.TIMETZ:
				return String.class;
	    }

	    return null;
	}

	public static Class<? extends Object> getJdbcType(int typeCode) {
	    switch(typeCode) {
	    	// simple types
	        case Types.CHAR:
	        case Types.VARCHAR:
	            return String.class;
	        case Types.LONGVARCHAR:
	        	return CharacterStream.class;
	        case Types.NUMERIC:
	        case Types.DECIMAL:
	            return java.math.BigDecimal.class;
	        case Types.BIT:
	            return boolean.class;
	        case Types.TINYINT:
	            return byte.class;
	        case Types.SMALLINT:
	            return int.class;
	        case Types.INTEGER:
	            return int.class;
	        case Types.BIGINT:
	            return long.class;
	        case Types.REAL:
	            return float.class;
	        case Types.FLOAT:
	        case Types.DOUBLE:
	            return double.class;
	        case Types.BINARY:
	        case Types.VARBINARY:
	            return byte[].class;
	        case Types.LONGVARBINARY:
		        return BinaryStream.class;
	        case Types.DATE:
	            return java.sql.Date.class;
	        case Types.TIME:
	            return java.sql.Time.class;
	        case Types.TIMESTAMP:
	            return java.sql.Timestamp.class;
	        case Types.BLOB:
	            return java.sql.Blob.class;
	        case Types.CLOB:
	            return java.sql.Clob.class;
	        case Types.BOOLEAN:
	        	return boolean.class;
	        case Types.NULL:
	        	// return Void.class;
	        	return Object.class; //ie - no conversion
	        // complex types
	        case Types.ARRAY:
	        	return java.sql.Array.class;
	        case Types.REF:
	        	return java.sql.Ref.class;
	        case Types.DATALINK:
	        	return Object.class; //???
	        case Types.OTHER:
	        case Types.JAVA_OBJECT:
		        return Object.class;
	        case Types.STRUCT:
	        	return java.sql.Struct.class;
			case AdditionalTypes.TIMESTAMPTZ:
				return Calendar.class;
			case AdditionalTypes.TIMETZ:
				return String.class;
	    }

	    return null;
	}

	/**  Returns the SQLType that the PreparedStatement Object expects for
	 *  the given Java Class.
	 * @return
	 * @param Java Class
	 */
	public static int getTypeCode(Class<?> javaType) {
	    javaType = ConvertUtils.convertToWrapperClass(javaType);
	    if(javaType.equals(String.class)) {
	        return Types.VARCHAR;
	    } else if(BigDecimal.class.isAssignableFrom(javaType)) {
	        return Types.DECIMAL; //Types.NUMERIC;
	    } else if(BigInteger.class.isAssignableFrom(javaType)) {
	        return Types.BIGINT;
	    } else if(Long.class.isAssignableFrom(javaType)) {
	        return Types.BIGINT;
	    } else if(Integer.class.isAssignableFrom(javaType)) {
	        return Types.INTEGER;
	    } else if(Short.class.isAssignableFrom(javaType)) {
	        return Types.SMALLINT;
	    } else if(Byte.class.isAssignableFrom(javaType)) {
	        return Types.TINYINT;
	    } else if(Double.class.isAssignableFrom(javaType)) {
	        return Types.DOUBLE;
	    } else if(Float.class.isAssignableFrom(javaType)) {
	        return Types.FLOAT;
	    } else if(byte[].class.isAssignableFrom(javaType)) {
	        return Types.VARBINARY;
	    } else if(Byte[].class.isAssignableFrom(javaType)) {
	        return Types.VARBINARY;
	    } else if(Number.class.isAssignableFrom(javaType)) {
	        return Types.NUMERIC;
	    } else if(Date.class.isAssignableFrom(javaType)) {
	        return Types.DATE;
	    } else if(Time.class.isAssignableFrom(javaType)) {
	        return Types.TIME;
	    } else if(java.util.Date.class.isAssignableFrom(javaType)) {
	        return Types.TIMESTAMP;
	    } else if(Boolean.class.isAssignableFrom(javaType)) {
	        return Types.BIT;
	    } else if(Character.class.isAssignableFrom(javaType)) {
	        return Types.CHAR;
	    } else if(Character[].class.isAssignableFrom(javaType)) {
	        return Types.CHAR;
	    } else if(char[].class.isAssignableFrom(javaType)) {
	        return Types.CHAR;
	    } else if(Blob.class.isAssignableFrom(javaType)) {
	        return Types.BLOB;
	    } else if(Clob.class.isAssignableFrom(javaType)) {
	        return Types.CLOB;
	    } else if(Array.class.isAssignableFrom(javaType)) {
	        return Types.ARRAY;
	    } else if(javaType.isArray()) {
	        return Types.ARRAY;
		} else if(Calendar.class.isAssignableFrom(javaType)) {
			return AdditionalTypes.TIMESTAMPTZ;
	    }

	    return Types.VARCHAR;
	}
}
