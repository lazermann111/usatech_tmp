package simple.sql;

import java.util.Map;

public class AlphaOrderAliaser extends AbstractAliaser {
	protected String lastAlias;

	public AlphaOrderAliaser(Map<Table, String> tables) {
		super(tables);
	}

	@Override
	protected String generateAlias(Table table) {
		String alias;
		if(lastAlias == null) {
			alias = "a";
		} else {
			int i = lastAlias.length() - 1;
			while(i >= 0 && lastAlias.charAt(i) == 'z')
				i--;
			if(i < 0)
				alias = lastAlias + "a";
			else
				alias = lastAlias.substring(0, i) + ((char) (1 + lastAlias.charAt(i))) + lastAlias.substring(i + 1);
		}
		return alias;
	}
}
