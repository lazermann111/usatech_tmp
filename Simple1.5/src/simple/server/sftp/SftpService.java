package simple.server.sftp;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.sshd.SshServer;
import org.apache.sshd.common.Channel;
import org.apache.sshd.common.Cipher;
import org.apache.sshd.common.Compression;
import org.apache.sshd.common.FactoryManager;
import org.apache.sshd.common.KeyExchange;
import org.apache.sshd.common.Mac;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.common.RequestHandler;
import org.apache.sshd.common.Signature;
import org.apache.sshd.common.cipher.AES128CBC;
import org.apache.sshd.common.cipher.AES128CTR;
import org.apache.sshd.common.cipher.AES192CBC;
import org.apache.sshd.common.cipher.AES256CBC;
import org.apache.sshd.common.cipher.AES256CTR;
import org.apache.sshd.common.cipher.ARCFOUR128;
import org.apache.sshd.common.cipher.ARCFOUR256;
import org.apache.sshd.common.cipher.BlowfishCBC;
import org.apache.sshd.common.cipher.TripleDESCBC;
import org.apache.sshd.common.compression.CompressionDelayedZlib;
import org.apache.sshd.common.compression.CompressionNone;
import org.apache.sshd.common.compression.CompressionZlib;
import org.apache.sshd.common.file.nativefs.NativeFileSystemFactory;
import org.apache.sshd.common.forward.DefaultTcpipForwarderFactory;
import org.apache.sshd.common.forward.TcpipServerChannel;
import org.apache.sshd.common.future.CloseFuture;
import org.apache.sshd.common.mac.HMACMD5;
import org.apache.sshd.common.mac.HMACMD596;
import org.apache.sshd.common.mac.HMACSHA1;
import org.apache.sshd.common.mac.HMACSHA196;
import org.apache.sshd.common.mac.HMACSHA256;
import org.apache.sshd.common.mac.HMACSHA512;
import org.apache.sshd.common.random.BouncyCastleRandom;
import org.apache.sshd.common.random.JceRandom;
import org.apache.sshd.common.random.SingletonRandomFactory;
import org.apache.sshd.common.session.ConnectionService;
import org.apache.sshd.common.signature.SignatureDSA;
import org.apache.sshd.common.signature.SignatureECDSA;
import org.apache.sshd.common.signature.SignatureRSA;
import org.apache.sshd.common.util.SecurityUtils;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.channel.ChannelSession;
import org.apache.sshd.server.command.ScpCommandFactory;
import org.apache.sshd.server.global.CancelTcpipForwardHandler;
import org.apache.sshd.server.global.KeepAliveHandler;
import org.apache.sshd.server.global.NoMoreSessionsHandler;
import org.apache.sshd.server.global.TcpipForwardHandler;
import org.apache.sshd.server.kex.DHG1;
import org.apache.sshd.server.kex.DHG14;
import org.apache.sshd.server.kex.DHGEX;
import org.apache.sshd.server.kex.DHGEX256;
import org.apache.sshd.server.kex.ECDHP256;
import org.apache.sshd.server.kex.ECDHP384;
import org.apache.sshd.server.kex.ECDHP521;
import org.apache.sshd.server.sftp.SftpSubsystem;

import simple.app.Service;
import simple.app.ServiceException;
import simple.app.ServiceStatusListener;
import simple.util.concurrent.ResultFuture;

public class SftpService extends SshServer implements Service {
	protected final String serviceName;
	protected boolean compressionEnabled = false;
	protected int numThreads = 0;
	protected final ReentrantLock lock = new ReentrantLock();

	public SftpService(String serviceName) {
		super();
		this.serviceName = serviceName;
		setUpDefaults();
		/*
		 * SshServer sshd = SshServer.setUpDefaultServer();
    sshd.setPort(22);
    sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider("hostkey.ser"));

    List<NamedFactory<UserAuth>> userAuthFactories = new ArrayList<NamedFactory<UserAuth>>();
    userAuthFactories.add(new UserAuthNone.Factory());
    sshd.setUserAuthFactories(userAuthFactories);

    sshd.setCommandFactory(new ScpCommandFactory());

    List<NamedFactory<Command>> namedFactoryList = new ArrayList<NamedFactory<Command>>();
    namedFactoryList.add(new SftpSubsystem.Factory());
    sshd.setSubsystemFactories(namedFactoryList);

    try {
        sshd.start();
    } catch (Exception e) {
        e.printStackTrace();
    }
		 */
	}

	@SuppressWarnings("unchecked")
	protected void setUpDefaults() {
		if (SecurityUtils.isBouncyCastleRegistered()) {
            setKeyExchangeFactories(Arrays.<NamedFactory<KeyExchange>>asList(
                    new DHGEX256.Factory(),
                    new DHGEX.Factory(),
                    new ECDHP256.Factory(),
                    new ECDHP384.Factory(),
                    new ECDHP521.Factory(),
                    new DHG14.Factory(),
                    new DHG1.Factory()));
            setSignatureFactories(Arrays.<NamedFactory<Signature>>asList(
                    new SignatureECDSA.NISTP256Factory(),
                    new SignatureECDSA.NISTP384Factory(),
                    new SignatureECDSA.NISTP521Factory(),
                    new SignatureDSA.Factory(),
                    new SignatureRSA.Factory()));
            setRandomFactory(new SingletonRandomFactory(new BouncyCastleRandom.Factory()));
        // EC keys are not supported until OpenJDK 7
        } else if (SecurityUtils.hasEcc()) {
            setKeyExchangeFactories(Arrays.<NamedFactory<KeyExchange>>asList(
                    new DHGEX256.Factory(),
                    new DHGEX.Factory(),
                    new ECDHP256.Factory(),
                    new ECDHP384.Factory(),
                    new ECDHP521.Factory(),
                    new DHG1.Factory()));
            setSignatureFactories(Arrays.<NamedFactory<Signature>>asList(
                    new SignatureECDSA.NISTP256Factory(),
                    new SignatureECDSA.NISTP384Factory(),
                    new SignatureECDSA.NISTP521Factory(),
                    new SignatureDSA.Factory(),
                    new SignatureRSA.Factory()));
            setRandomFactory(new SingletonRandomFactory(new JceRandom.Factory()));
        } else {
            setKeyExchangeFactories(Arrays.<NamedFactory<KeyExchange>>asList(
                    new DHGEX256.Factory(),
                    new DHGEX.Factory(),
                    new DHG1.Factory()));
            setSignatureFactories(Arrays.<NamedFactory<Signature>>asList(
                    new SignatureDSA.Factory(),
                    new SignatureRSA.Factory()));
			setRandomFactory(new SingletonRandomFactory(new JceRandom.Factory()));
        }
		setUpDefaultCiphers();
		setCompressionFactories(Collections.singletonList((NamedFactory<Compression>) new CompressionNone.Factory()));
        setMacFactories(Arrays.<NamedFactory<Mac>>asList(
                new HMACSHA256.Factory(),
                new HMACSHA512.Factory(),
                new HMACSHA1.Factory(),
                new HMACMD5.Factory(),
                new HMACSHA196.Factory(),
                new HMACMD596.Factory()));
        setChannelFactories(Arrays.<NamedFactory<Channel>>asList(
                new ChannelSession.Factory(),
                new TcpipServerChannel.DirectTcpipFactory()));
        setFileSystemFactory(new NativeFileSystemFactory());
        setTcpipForwarderFactory(new DefaultTcpipForwarderFactory());
        setGlobalRequestHandlers(Arrays.<RequestHandler<ConnectionService>>asList(
                new KeepAliveHandler(),
                new NoMoreSessionsHandler(),
                new TcpipForwardHandler(),
                new CancelTcpipForwardHandler()
        ));

		setCommandFactory(new ScpCommandFactory());
		setSubsystemFactories(Collections.singletonList((NamedFactory<Command>) new SftpSubsystem.Factory()));
	}

	protected void setUpDefaultCiphers() {
		List<NamedFactory<Cipher>> avail = new LinkedList<NamedFactory<Cipher>>();
		avail.add(new AES128CTR.Factory());
		avail.add(new AES256CTR.Factory());
		avail.add(new ARCFOUR128.Factory());
		avail.add(new ARCFOUR256.Factory());
		avail.add(new AES128CBC.Factory());
		avail.add(new TripleDESCBC.Factory());
		avail.add(new BlowfishCBC.Factory());
		avail.add(new AES192CBC.Factory());
		avail.add(new AES256CBC.Factory());

		for(Iterator<NamedFactory<Cipher>> i = avail.iterator(); i.hasNext();) {
			final NamedFactory<Cipher> f = i.next();
			try {
				final Cipher c = f.create();
				final byte[] key = new byte[c.getBlockSize()];
				final byte[] iv = new byte[c.getIVSize()];
				c.init(Cipher.Mode.Encrypt, key, iv);
			} catch(InvalidKeyException e) {
				i.remove();
			} catch(Exception e) {
				i.remove();
			}
		}
		setCipherFactories(avail);
	}

	@Override
	public String getServiceName() {
		return serviceName;
	}

	@Override
	public int getNumThreads() {
		return numThreads;
	}

	@Override
	public int startThreads(int numThreads) throws ServiceException {
		if(numThreads < 1)
			return 0;
		return restartThreads(numThreads + this.numThreads, 0L);
	}

	@Override
	public int restartThreads(long timeout) throws ServiceException {
		return restartThreads(this.numThreads, timeout);
	}

	protected int restartThreads(int targetThreads, long timeout) throws ServiceException {
		if(targetThreads < 0)
			targetThreads = 0;
		lock.lock();
		try {
			if(this.numThreads > 0) {
				// I supppose we must shutdown existing and start new - ugh!
				Future<Integer> stopFuture = stopAllThreads(false);
				if(timeout > 0)
					stopFuture.get(0, TimeUnit.MILLISECONDS);
				else
					stopFuture.get();
			}
			getProperties().put(FactoryManager.NIO_WORKERS, String.valueOf(targetThreads));
			if(targetThreads > 0)
				start();
			this.numThreads = targetThreads;
		} catch(IOException e) {
			throw new ServiceException("Could not start SSH Server", e);
		} catch(InterruptedException e) {
			throw new ServiceException("Could not stop SSH Server", e);
		} catch(ExecutionException e) {
			throw new ServiceException("Could not stop SSH Server", e);
		} catch(TimeoutException e) {
			throw new ServiceException("Could not stop SSH Server", e);
		} finally {
			lock.unlock();
		}

		return targetThreads;
	}

	
	@Override
	public int pauseThreads() throws ServiceException {
		return 0; // for now don't implement
	}

	@Override
	public int unpauseThreads() throws ServiceException {
		return 0; // for now don't implement
	}

	@Override
	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException {
		if(numThreads < 1)
			return ZERO_FUTURE;
		final int targetThreads = this.numThreads - numThreads;
		return new ResultFuture<Integer>() {
			@Override
			protected Integer produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				try {
					return restartThreads(targetThreads, timeout);
				} catch(ServiceException e) {
					if(e.getCause() instanceof ExecutionException)
						throw (ExecutionException) e.getCause();
					throw new ExecutionException(e);
				}
			}
		};
	}

	@Override
	public Future<Integer> stopAllThreads(boolean force) throws ServiceException {
		lock.lock();
		try {
			if(numThreads > 0) {
				final int n = numThreads;
				final CloseFuture cf = close(force);
				numThreads = 0;
				return new Future<Integer>() {
					@Override
					public boolean cancel(boolean mayInterruptIfRunning) {
						return false;
					}

					@Override
					public boolean isCancelled() {
						return false;
					}

					@Override
					public boolean isDone() {
						return cf.isDone();
					}

					@Override
					public Integer get() throws InterruptedException, ExecutionException {
						cf.await();
						return n;
					}

					@Override
					public Integer get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
						cf.await(timeout, unit);
						return n;
					}
				};
			} else
				return ZERO_FUTURE;
		} finally {
			lock.unlock();
		}
	}

	@Override
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		try {
			return stopAllThreads(force).get(timeout, TimeUnit.MILLISECONDS);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		} catch(ExecutionException e) {
			throw new ServiceException(e);
		} catch(TimeoutException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		try {
			return stopThreads(numThreads, force).get(timeout, TimeUnit.MILLISECONDS);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		} catch(ExecutionException e) {
			throw new ServiceException(e);
		} catch(TimeoutException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean addServiceStatusListener(ServiceStatusListener listener) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeServiceStatusListener(ServiceStatusListener listener) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Future<Boolean> shutdown() throws ServiceException {
		return TRUE_FUTURE;
	}

	@Override
	public Future<Boolean> prepareShutdown() throws ServiceException {
		return TRUE_FUTURE;
	}

	public boolean isCompressionEnabled() {
		return compressionEnabled;
	}

	public void setCompressionEnabled(boolean compressionEnabled) {
		if(compressionEnabled == this.compressionEnabled)
			return;
		List<NamedFactory<Compression>> list = new ArrayList<NamedFactory<Compression>>();
		list.add(new CompressionNone.Factory());
		if(compressionEnabled) {
			list.add(new CompressionZlib.Factory());
			list.add(new CompressionDelayedZlib.Factory());
		}
		setCompressionFactories(list);
		this.compressionEnabled = compressionEnabled;
	}

}
