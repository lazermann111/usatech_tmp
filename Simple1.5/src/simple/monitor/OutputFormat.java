package simple.monitor;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import simple.results.Results;
import simple.text.StringUtils;

public enum OutputFormat {
	NAGIOS_SHORT {
		public void writeServiceCheckResult(long checkTime, String host, String checkName, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosResult(checkTime, host, checkName, state, outputDateFormat, output);
			writeNagiosComplete(output);
		}

		public void writeHostCheckResult(long checkTime, String host, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosHostResult(checkTime, host, state, outputDateFormat, output);
			writeNagiosComplete(output);
		}
	},
	NAGIOS_SHORT_PERF {
		public void writeServiceCheckResult(long checkTime, String host, String checkName, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosResult(checkTime, host, checkName, state, outputDateFormat, output);
			writeNagiosPerf(perfCheckResults, output);
			writeNagiosComplete(output);
		}

		public void writeHostCheckResult(long checkTime, String host, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosHostResult(checkTime, host, state, outputDateFormat, output);
			writeNagiosPerf(perfCheckResults, output);
			writeNagiosComplete(output);
		}
	},
	NAGIOS_LONG {
		public void writeServiceCheckResult(long checkTime, String host, String checkName, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosResult(checkTime, host, checkName, state, outputDateFormat, output);
			writeNagiosExtra(comments, textResults, output);
			writeNagiosComplete(output);
		}

		public void writeHostCheckResult(long checkTime, String host, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosHostResult(checkTime, host, state, outputDateFormat, output);
			writeNagiosExtra(comments, textResults, output);
			writeNagiosComplete(output);
		}
	},
	NAGIOS_LONG_PERF {
		public void writeServiceCheckResult(long checkTime, String host, String checkName, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosResult(checkTime, host, checkName, state, outputDateFormat, output);
			writeNagiosPerf(perfCheckResults, output);
			writeNagiosExtra(comments, textResults, output);
			writeNagiosComplete(output);
		}

		public void writeHostCheckResult(long checkTime, String host, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosHostResult(checkTime, host, state, outputDateFormat, output);
			writeNagiosPerf(perfCheckResults, output);
			writeNagiosExtra(comments, textResults, output);
			writeNagiosComplete(output);
		}
	},
	NAGIOS_LONG_PERF_ALT {
		public void writeServiceCheckResult(long checkTime, String host, String checkName, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosResult(checkTime, host, checkName, state, outputDateFormat, output);
			writeNagiosExtra(comments, textResults, output);
			writeNagiosPerf(perfCheckResults, output);
			writeNagiosComplete(output);
		}

		public void writeHostCheckResult(long checkTime, String host, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException {
			writeNagiosHostResult(checkTime, host, state, outputDateFormat, output);
			writeNagiosExtra(comments, textResults, output);
			writeNagiosPerf(perfCheckResults, output);
			writeNagiosComplete(output);
		}
	};

	public String getResultsColumnSeparator() {
		return "     ";
	}

	public String getResultsLineSeparator() {
		return "\\n";
	}

	public boolean isQuotePerfLabel() {
		return false;
	}

	public abstract void writeServiceCheckResult(long checkTime, String host, String checkName, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException;

	public abstract void writeHostCheckResult(long checkTime, String host, CheckResultState state, DateFormat outputDateFormat, List<PerfCheckResult> perfCheckResults, Results textResults, String comments, Writer output) throws IOException;

	protected void writeNagiosHostResult(long checkTime, String host, CheckResultState state, DateFormat outputDateFormat, Writer output) throws IOException {
		output.write('[');
		output.write(String.valueOf(checkTime / 1000));
		output.write("] PROCESS_HOST_CHECK_RESULT;");
		writeNagiosEscaped(host, output);// should use short name
		output.write(';');
		output.write(String.valueOf(state.getValue()));
		output.write(';');
		writeNagiosEscaped(host, output);
		output.write(" is ");
		output.write(state.toString());
		output.write(" at ");
		output.write(outputDateFormat.format(new Date(checkTime)));
	}

	protected void writeNagiosResult(long checkTime, String host, String checkName, CheckResultState state, DateFormat outputDateFormat, Writer output) throws IOException {
		output.write('[');
		output.write(String.valueOf(checkTime / 1000));
		output.write("] PROCESS_SERVICE_CHECK_RESULT;");
		writeNagiosEscaped(host, output);// should use short name
		output.write(';');
		writeNagiosEscaped(checkName, output);
		output.write(';');
		output.write(String.valueOf(state.getValue()));
		output.write(';');
		writeNagiosEscaped(checkName, output);
		output.write(" is ");
		output.write(state.toString());
		output.write(" at ");
		output.write(outputDateFormat.format(new Date(checkTime)));
	}

	protected void writeNagiosPerf(List<PerfCheckResult> results, Writer output) throws IOException {
		if(results == null)
			return;
		output.write('|');
		boolean first = true;
		for(PerfCheckResult r : results) {
			if(r.getValue() == null)
				continue;
			if(first)
				first = false;
			else
				output.write(' ');
			if(isQuotePerfLabel()) {
				output.write('\'');
				output.write(r.getName().replace("'", "''"));
				output.write('\'');
			} else {
				output.write(r.getName().replaceAll("[\\W]", "_"));
			}
			output.write('=');
			UOM uom = r.getPerfCheck().getUom();
			if(uom == null)
				uom = UOM.NONE;
			output.write(uom.format(r.getValue()));
			output.write(';');
			Number n;
			if(r.getPerfCheck().getWarnRange() != null && (n = r.getPerfCheck().getWarnRange().getMin()) != null)
				output.write(uom.formatWithoutSuffix(n));
			else
				output.write('~');
			output.write(':');
			if(r.getPerfCheck().getWarnRange() != null && (n = r.getPerfCheck().getWarnRange().getMax()) != null)
				output.write(uom.formatWithoutSuffix(r.getPerfCheck().getWarnRange().getMax()));
			output.write(';');
			if(r.getPerfCheck().getCritRange() != null && (n = r.getPerfCheck().getCritRange().getMin()) != null)
				output.write(uom.formatWithoutSuffix(r.getPerfCheck().getCritRange().getMin()));
			else
				output.write('~');
			output.write(':');
			if(r.getPerfCheck().getCritRange() != null && (n = r.getPerfCheck().getCritRange().getMax()) != null)
				output.write(uom.formatWithoutSuffix(r.getPerfCheck().getCritRange().getMax()));
			output.write(';');
			if((n = r.getPerfCheck().getMin()) != null)
				output.write(uom.formatWithoutSuffix(n));
			output.write(';');
			if((n = r.getPerfCheck().getMax()) != null)
				output.write(uom.formatWithoutSuffix(n));
		}
	}

	protected void writeNagiosExtra(String comments, Results textResults, Writer output) throws IOException {
		final String csep = getResultsColumnSeparator();
		final String lsep = getResultsLineSeparator();
		if(!StringUtils.isBlank(comments)) {
			output.write(lsep);
			writeNagiosEscaped(comments, output);
		}
		if(textResults == null)
			return;
		output.write(lsep);
		for(int i = 1; i <= textResults.getColumnCount(); i++) {
			if(i > 1)
				output.write(csep);
			String s = textResults.getColumnName(i);
			if(StringUtils.isBlank(s)) {
				output.write("Column ");
				output.write(String.valueOf(i));
			} else
				writeNagiosEscaped(s, output);
		}
		output.write(lsep);

		while(textResults.next()) {
			for(int i = 1; i <= textResults.getColumnCount(); i++) {
				if(i > 1)
					output.write(csep);
				String s = textResults.getFormattedValue(i);
				if(!StringUtils.isBlank(s))
					writeNagiosEscaped(s, output);
			}
			output.write(lsep);
		}
	}

	public static void writeNagiosEscaped(String text, Writer output) throws IOException {
		if(text != null) {
			for(int i = 0; i < text.length(); i++) {
				char ch = text.charAt(i);
				switch(ch) {
					case '\n':
						output.write('\\');
						break;
					case ';':
					case '|':
						ch = ':';
						break;
					default:
						if(ch < ' ' || ch > '~')
							ch = ' ';
				}
				output.write(ch);
			}
		}
	}

	public static void writeNagiosComplete(Writer output) throws IOException {
		output.write('\n');
		output.flush();
	}
}