package simple.monitor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

import simple.bean.ConvertUtils;
import simple.io.BlockingWriter;
import simple.io.BufferStream;
import simple.io.HeapBufferStream;

public class LocalCommandFileWriter extends BlockingWriter {
	protected File pipe;
	protected File tmpDir;
	protected FileOutputStream pipeOutputStream;
	protected long pipeBufferSize = 4096;
	protected final BufferStream buffer = new HeapBufferStream(true);
	protected final Charset charset = Charset.forName("UTF-8");
	@Override
	protected Writer openDelegate() throws IOException {
		// TODO: we could write up to the pipeBufferSize bytes into the buffer and then write to a file thereafter
		return new OutputStreamWriter(buffer.getOutputStream(), charset);
	}

	@Override
	protected void prepareDelegate() throws IOException {
		// reset buffer
		buffer.clear();
	}

	@Override
	public void flush() throws IOException {
		super.flush();
		// if bytes in buffer is less than pipeBufferSize write to pipe, else write to file
		// TODO: we could use NIO to avoid waiting on a full pipe and write to a file instead when pipe is not ready for writing
		if(pipeOutputStream == null)
			pipeOutputStream = new FileOutputStream(getPipe());
		if(buffer.size() <= getPipeBufferSize()) {
			buffer.copyInto(pipeOutputStream);
			pipeOutputStream.flush();
		} else {
			File tmpFile = File.createTempFile("tmp-monitor-commands-", "", tmpDir);
			FileOutputStream fos = new FileOutputStream(tmpFile);
			try {
				buffer.copyInto(fos);
				fos.flush();
			} finally {
				fos.close();
			}
			buffer.clear();
			delegate.write('[');
			delegate.write(String.valueOf(System.currentTimeMillis() / 1000));
			delegate.write("] PROCESS_FILE;");
			OutputFormat.writeNagiosEscaped(tmpFile.getAbsolutePath(), delegate);
			delegate.write(";1\n");
			delegate.flush();
			buffer.copyInto(pipeOutputStream);
			pipeOutputStream.flush();
		}
		buffer.clear();
	}

	@Override
	protected void closeDelegate() {
		super.closeDelegate();
		try {
			buffer.clear();
		} catch(IOException e1) {
			// ignore
		}
		if(pipeOutputStream != null) {
			try {
				pipeOutputStream.close();
			} catch(IOException e) {
				// ignore
			}
			pipeOutputStream = null;
		}
	}

	public File getPipe() {
		return pipe;
	}

	public void setPipe(File pipe) {
		File old = this.pipe;
		this.pipe = pipe;
		if(!ConvertUtils.areEqual(old, pipe))
			reset();
	}

	public long getPipeBufferSize() {
		return pipeBufferSize;
	}

	public void setPipeBufferSize(long pipeBufferSize) {
		this.pipeBufferSize = pipeBufferSize;
	}

	public File getTmpDir() {
		return tmpDir;
	}

	public void setTmpDir(File tmpDir) {
		this.tmpDir = tmpDir;
	}
}
