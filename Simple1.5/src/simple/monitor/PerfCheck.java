package simple.monitor;

import java.util.Map;
import java.util.Set;

public interface PerfCheck<V extends Number> {
	public V getValue(Map<String, Number> values);

	public Set<String> getAttributeNames();

	public Range<V> getOkayRange();

	public Range<V> getWarnRange();

	public Range<V> getCritRange();

	public V getMin();

	public V getMax();

	public UOM getUom();

	public Class<V> getValueClass();
}
