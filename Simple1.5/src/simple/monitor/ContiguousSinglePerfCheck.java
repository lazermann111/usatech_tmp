package simple.monitor;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;

public class ContiguousSinglePerfCheck<V extends Number & Comparable<V>> implements PerfCheck<V> {
	protected final boolean increasing;
	protected V startLimit;
	protected V warnLimit;
	protected V critLimit;
	protected V endLimit;

	protected abstract class ContiguousRange implements Range<V> {
		@Override
		public boolean isIn(V value) {
			if(value == null)
				return false;
			if(getMin() != null) {
				int i = getMin().compareTo(value);
				if(i < 0 || (i == 0 && !increasing))
					return false;
			}
			if(getMax() != null) {
				int i = getMax().compareTo(value);
				if(i > 0 || (i == 0 && increasing))
					return false;
			}
			return true;
		}
	}

	protected final Range<V> okayRange = new ContiguousRange() {
		@Override
		public V getMin() {
			return increasing ? startLimit : warnLimit;
		}

		@Override
		public V getMax() {
			return increasing ? warnLimit : startLimit;
		}
	};
	protected Range<V> warnRange = new ContiguousRange() {
		@Override
		public V getMin() {
			return increasing ? warnLimit : critLimit;
		}

		@Override
		public V getMax() {
			return increasing ? critLimit : warnLimit;
		}
	};
	protected Range<V> critRange = new ContiguousRange() {
		@Override
		public V getMin() {
			return increasing ? critLimit : endLimit;
		}

		@Override
		public V getMax() {
			return increasing ? endLimit : critLimit;
		}
	};
	protected String attributeName;
	protected UOM uom;
	protected final Class<V> valueClass;
	protected final Set<String> attributeNames = new AbstractSet<String>() {
		public Iterator<String> iterator() {
			return new Iterator<String>() {
				private boolean hasNext = true;

				public boolean hasNext() {
					return hasNext;
				}

				public String next() {
					if(hasNext) {
						hasNext = false;
						return attributeName;
					}
					throw new NoSuchElementException();
				}

				public void remove() {
					throw new UnsupportedOperationException();
				}
			};
		}

		public int size() {
			return 1;
		}

		public boolean contains(Object o) {
			return ConvertUtils.areEqual(o, attributeName);
		}
	};

	public static ContiguousSinglePerfCheck<Double> getInstance(String attributeName, double startLimit, double warnLimit, double critLimit, double endLimit, UOM uom) {
		return new ContiguousSinglePerfCheck<Double>(Double.class, attributeName, startLimit, warnLimit, critLimit, endLimit, uom);
	}

	public static ContiguousSinglePerfCheck<Long> getInstance(String attributeName, long startLimit, long warnLimit, long critLimit, long endLimit, UOM uom) {
		return new ContiguousSinglePerfCheck<Long>(Long.class, attributeName, startLimit, warnLimit, critLimit, endLimit, uom);
	}

	public static ContiguousSinglePerfCheck<Integer> getInstance(String attributeName, int startLimit, int warnLimit, int critLimit, int endLimit, UOM uom) {
		return new ContiguousSinglePerfCheck<Integer>(Integer.class, attributeName, startLimit, warnLimit, critLimit, endLimit, uom);
	}

	public ContiguousSinglePerfCheck(Class<V> valueClass, String attributeName, V startLimit, V warnLimit, V critLimit, V endLimit, UOM uom) {
		this.valueClass = valueClass;
		@SuppressWarnings("unchecked")
		V limit1 = SystemUtils.nvl(startLimit, warnLimit, critLimit, endLimit);
		@SuppressWarnings("unchecked")
		V limit2 = SystemUtils.nvl(endLimit, critLimit, warnLimit, startLimit);
		if(limit1 != null && limit2 != null) {
			if(limit1 == limit2)
				this.increasing = (startLimit != null);
			else
				this.increasing = limit1.compareTo(limit2) <= 0;
		} else
			this.increasing = true;

		this.startLimit = startLimit;
		this.warnLimit = warnLimit;
		this.critLimit = critLimit;
		this.endLimit = endLimit;
		this.attributeName = attributeName;
		this.uom = uom;
	}

	@Override
	public Set<String> getAttributeNames() {
		return attributeNames;
	}

	@Override
	public V getValue(Map<String, Number> values) {
		return (V) values.get(getAttributeName());
	}

	public Range<V> getOkayRange() {
		return okayRange;
	}

	public Range<V> getWarnRange() {
		return warnRange;
	}

	public Range<V> getCritRange() {
		return critRange;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public UOM getUom() {
		return uom;
	}

	public void setUom(UOM uom) {
		this.uom = uom;
	}

	@SuppressWarnings("unchecked")
	public V getMin() {
		return SystemUtils.least(getOkayRange() != null ? getOkayRange().getMin() : null, getWarnRange() != null ? getWarnRange().getMin() : null, getCritRange() != null ? getCritRange().getMin() : null);
	}

	@SuppressWarnings("unchecked")
	public V getMax() {
		return SystemUtils.greatest(getOkayRange() != null ? getOkayRange().getMax() : null, getWarnRange() != null ? getWarnRange().getMax() : null, getCritRange() != null ? getCritRange().getMax() : null);
	}

	public Class<V> getValueClass() {
		return valueClass;
	}

	public V getStartLimit() {
		return startLimit;
	}

	public void setStartLimit(V startLimit) {
		this.startLimit = startLimit;
	}

	public V getWarnLimit() {
		return warnLimit;
	}

	public void setWarnLimit(V warnLimit) {
		this.warnLimit = warnLimit;
	}

	public V getCritLimit() {
		return critLimit;
	}

	public void setCritLimit(V critLimit) {
		this.critLimit = critLimit;
	}

	public V getEndLimit() {
		return endLimit;
	}

	public void setEndLimit(V endLimit) {
		this.endLimit = endLimit;
	}
}
