package simple.monitor;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.util.Locale;

public enum UOM {
	NONE(""), SECONDS("s"), MILLISECONDS("ms"), MICROSECONDS("us"), PERCENT("%") {
		@Override
		public String format(Number value) {
			return value == null ? "" : super.format(value.doubleValue() * 100);
		}
	},
	BYTES("B"), KILOBYTES("KB"), MEGABYTES("MB"), GIGABYTES("GB"), TERABYTES("TB"), COUNTER("c");
	protected static final DecimalFormat FORMAT;
	static {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.getDefault());
		symbols.setInfinity("~");
		symbols.setNaN("?");
		FORMAT = new DecimalFormat("0.#", symbols);
		FORMAT.setDecimalSeparatorAlwaysShown(false);
		FORMAT.setMaximumFractionDigits(Integer.MAX_VALUE);
	}
	private final String suffix;

	private UOM(String suffix) {
		this.suffix = suffix;
	}

	public String format(Number value) {
		if(value == null)
			return "";
		StringBuffer sb = new StringBuffer();
		return FORMAT.format(value, sb, new FieldPosition(0)).append(suffix).toString();
	}

	public String formatWithoutSuffix(Number value) {
		if(value == null)
			return "";
		StringBuffer sb = new StringBuffer();
		return FORMAT.format(value, sb, new FieldPosition(0)).toString();
	}
}
