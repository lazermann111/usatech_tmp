package simple.monitor;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;

public class SinglePerfCheck<V extends Number & Comparable<V>> implements PerfCheck<V> {
	protected Range<V> okayRange;
	protected Range<V> warnRange;
	protected Range<V> critRange;
	protected String attributeName;
	protected UOM uom;
	protected final Class<V> valueClass;
	protected final Set<String> attributeNames = new AbstractSet<String>() {
		public Iterator<String> iterator() {
			return new Iterator<String>() {
				private boolean hasNext = true;

				public boolean hasNext() {
					return hasNext;
				}

				public String next() {
					if(hasNext) {
						hasNext = false;
						return attributeName;
					}
					throw new NoSuchElementException();
				}

				public void remove() {
					throw new UnsupportedOperationException();
				}
			};
		}

		public int size() {
			return 1;
		}

		public boolean contains(Object o) {
			return ConvertUtils.areEqual(o, attributeName);
		}
	};

	public static SinglePerfCheck<Double> getInstance(String attributeName, double startLimit, double okayLimit, double warnLimit, double critLimit, UOM uom) {
		Range<Double> okayRange, warnRange, critRange;
		if(startLimit <= okayLimit) { // forward
			okayRange = (startLimit < okayLimit ? new ComparableRange<Double>(startLimit, okayLimit) : null);
			warnRange = (okayLimit < warnLimit ? new ComparableRange<Double>(Math.nextUp(okayLimit), warnLimit) : null);
			critRange = (warnLimit < critLimit ? new ComparableRange<Double>(Math.nextUp(warnLimit), critLimit) : null);
		} else {
			okayRange = (startLimit > okayLimit ? new ComparableRange<Double>(okayLimit, startLimit) : null);
			warnRange = (okayLimit > warnLimit ? new ComparableRange<Double>(warnLimit, Math.nextAfter(okayLimit, Double.MIN_VALUE)) : null);
			critRange = (warnLimit > critLimit ? new ComparableRange<Double>(critLimit, Math.nextAfter(warnLimit, Double.MIN_VALUE)) : null);
		}
		return new SinglePerfCheck<Double>(Double.class, attributeName, okayRange, warnRange, critRange, uom);
	}

	public static SinglePerfCheck<Long> getInstance(String attributeName, long startLimit, long okayLimit, long warnLimit, long critLimit, UOM uom) {
		Range<Long> okayRange, warnRange, critRange;
		if(startLimit <= okayLimit) { // forward
			okayRange = (startLimit < okayLimit ? new ComparableRange<Long>(startLimit, okayLimit) : null);
			warnRange = (okayLimit < warnLimit ? new ComparableRange<Long>(okayLimit + 1, warnLimit) : null);
			critRange = (warnLimit < critLimit ? new ComparableRange<Long>(warnLimit + 1, critLimit) : null);
		} else {
			okayRange = (startLimit > okayLimit ? new ComparableRange<Long>(okayLimit, startLimit) : null);
			warnRange = (okayLimit > warnLimit ? new ComparableRange<Long>(warnLimit, okayLimit - 1) : null);
			critRange = (warnLimit > critLimit ? new ComparableRange<Long>(critLimit, warnLimit - 1) : null);
		}
		return new SinglePerfCheck<Long>(Long.class, attributeName, okayRange, warnRange, critRange, uom);
	}

	public static SinglePerfCheck<Integer> getInstance(String attributeName, int startLimit, int okayLimit, int warnLimit, int critLimit, UOM uom) {
		Range<Integer> okayRange, warnRange, critRange;
		if(startLimit <= okayLimit) { // forward
			okayRange = (startLimit < okayLimit ? new ComparableRange<Integer>(startLimit, okayLimit) : null);
			warnRange = (okayLimit < warnLimit ? new ComparableRange<Integer>(okayLimit + 1, warnLimit) : null);
			critRange = (warnLimit < critLimit ? new ComparableRange<Integer>(warnLimit + 1, critLimit) : null);
		} else {
			okayRange = (startLimit > okayLimit ? new ComparableRange<Integer>(okayLimit, startLimit) : null);
			warnRange = (okayLimit > warnLimit ? new ComparableRange<Integer>(warnLimit, okayLimit - 1) : null);
			critRange = (warnLimit > critLimit ? new ComparableRange<Integer>(critLimit, warnLimit - 1) : null);
		}
		return new SinglePerfCheck<Integer>(Integer.class, attributeName, okayRange, warnRange, critRange, uom);
	}

	public SinglePerfCheck(Class<V> valueClass) {
		this.valueClass = valueClass;
	}

	public SinglePerfCheck(Class<V> valueClass, String attributeName, Range<V> okayRange, Range<V> warnRange, Range<V> critRange, UOM uom) {
		this(valueClass);
		this.okayRange = okayRange;
		this.warnRange = warnRange;
		this.critRange = critRange;
		this.attributeName = attributeName;
		this.uom = uom;
	}

	@Override
	public Set<String> getAttributeNames() {
		return attributeNames;
	}

	@Override
	public V getValue(Map<String, Number> values) {
		return (V) values.get(getAttributeName());
	}

	public Range<V> getOkayRange() {
		return okayRange;
	}

	public Range<V> getWarnRange() {
		return warnRange;
	}

	public Range<V> getCritRange() {
		return critRange;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public void setOkayRange(Range<V> okayRange) {
		this.okayRange = okayRange;
	}

	public void setWarnRange(Range<V> warnRange) {
		this.warnRange = warnRange;
	}

	public void setCritRange(Range<V> critRange) {
		this.critRange = critRange;
	}

	public UOM getUom() {
		return uom;
	}

	public void setUom(UOM uom) {
		this.uom = uom;
	}

	@SuppressWarnings("unchecked")
	public V getMin() {
		return SystemUtils.least(getOkayRange() != null ? getOkayRange().getMin() : null, getWarnRange() != null ? getWarnRange().getMin() : null, getCritRange() != null ? getCritRange().getMin() : null);
	}

	@SuppressWarnings("unchecked")
	public V getMax() {
		return SystemUtils.greatest(getOkayRange() != null ? getOkayRange().getMax() : null, getWarnRange() != null ? getWarnRange().getMax() : null, getCritRange() != null ? getCritRange().getMax() : null);
	}

	public Class<V> getValueClass() {
		return valueClass;
	}
}
