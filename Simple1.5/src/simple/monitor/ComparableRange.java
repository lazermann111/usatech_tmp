package simple.monitor;

public class ComparableRange<V extends Comparable<V>> implements Range<V> {
	protected V min;
	protected V max;

	public ComparableRange() {

	}

	public ComparableRange(V min, V max) {
		this();
		this.min = min;
		this.max = max;
	}

	@Override
	public boolean isIn(V value) {
		if(value == null)
			return false;
		if(min != null && min.compareTo(value) < 0)
			return false;
		if(max != null && max.compareTo(value) > 0)
			return false;
		return true;
	}

	@Override
	public V getMin() {
		return min;
	}

	@Override
	public V getMax() {
		return max;
	}

	public void setMin(V min) {
		this.min = min;
	}

	public void setMax(V max) {
		this.max = max;
	}
}
