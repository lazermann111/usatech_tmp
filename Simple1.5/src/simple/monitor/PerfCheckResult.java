package simple.monitor;


public class PerfCheckResult {
	protected final String name;
	protected final CheckResultState state;
	protected final Number value;
	protected final PerfCheck<?> perfCheck;

	public PerfCheckResult(String name, CheckResultState state, Number value, PerfCheck<? extends Number> perfCheck) {
		this.name = name;
		this.state = state;
		this.value = value;
		this.perfCheck = perfCheck;
	}

	public CheckResultState getState() {
		return state;
	}

	public Number getValue() {
		return value;
	}

	public PerfCheck<? extends Number> getPerfCheck() {
		return perfCheck;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return getName() + "=" + getValue() + " (" + state + ")";
	}
}
