package simple.monitor;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.Configurer;
import simple.io.Log;
import simple.util.concurrent.CustomThreadFactory;

public class AppMonitorMain extends BaseWithConfig implements Configurer {
	private static Log log = Log.getLog();
	protected final CountDownLatch exitSignal = new CountDownLatch(1);
	protected final RemoteAppMonitor monitor = new RemoteAppMonitor();
	public static void main(String[] args) {
		new AppMonitorMain().run(args);
	}

	@Override
	public void configure(Configuration config) {
		monitor.configure(config);
	}

	public Configuration getConfig(String propertiesFile, Class<?> appClass, Configuration defaults) throws IOException {
		return loadConfig(propertiesFile, appClass, defaults, this, 5000, 30000);
	}

	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		log.info("Starting AppMonitor");
		monitor.setScheduledExecutorService(new ScheduledThreadPoolExecutor(25, new CustomThreadFactory("Check-", true, 5)));
		monitor.configureFromScratch(config);

		// await exit
		try {
			exitSignal.await();
		} catch(InterruptedException e) {
			log.error("Main thread was interupted", e);
			System.exit(10);
			return;
		}
		log.info("Exiting AppMonitor");
	}
}
