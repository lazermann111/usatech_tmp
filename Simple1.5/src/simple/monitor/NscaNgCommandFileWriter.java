package simple.monitor;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicLong;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import simple.bean.ConvertUtils;
import simple.io.BlockingWriter;
import simple.io.BufferStream;
import simple.io.HeapBufferStream;
import simple.text.StringUtils;

public class NscaNgCommandFileWriter extends BlockingWriter {
	protected final static String sessionIDPrefix = StringUtils.toHex(System.currentTimeMillis());
	protected final static AtomicLong sessionIDGenerator = new AtomicLong(Double.doubleToLongBits(Math.random()));
	protected String host;
	protected int port = 5668;
	protected int timeout = 5000;
	protected Socket socket;
	protected OutputStream socketOutputStream;
	protected BufferedReader socketReader;
	protected long sessionId;
	protected final BufferStream buffer = new HeapBufferStream(true);
	protected final Charset charset = Charset.forName("UTF-8");
	protected static final byte[] EOL = new byte[] { 13, 10 };
	@Override
	protected Writer openDelegate() throws IOException {
		/*
		TlsClient tlsClient = new PSKTlsClient(new TlsPSKIdentity() {
			@Override
			public void skipIdentityHint() {
				// TODO Auto-generated method stub

			}

			@Override
			public void notifyIdentityHint(byte[] psk_identity_hint) {
				// TODO Auto-generated method stub

			}

			@Override
			public byte[] getPSKIdentity() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public byte[] getPSK() {
				// TODO Auto-generated method stub
				return null;
			}
		}) {
			@Override
			public TlsAuthentication getAuthentication() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
		};
		*/
		// open connection
		socket = SSLSocketFactory.getDefault().createSocket();
		if(socket instanceof SSLSocket) {
			SSLSocket sslSocket = (SSLSocket) socket;
			sslSocket.setEnabledProtocols(new String[] { "TLSv1" });
		}
		socket.connect(new InetSocketAddress(getHost(), getPort()), getTimeout());
		socket.setSoTimeout(timeout);
		// generate session id
		sessionId = sessionIDGenerator.incrementAndGet();
		socketOutputStream = socket.getOutputStream();
		socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));
		// send MOIN, wait for MOIN
		String command = StringUtils.appendHex(new StringBuilder(23).append("MOIN 1 ").append(sessionIDPrefix), sessionId).toString();
		sendCommand(command);
		awaitResponse("MOIN 1");
		prepareDelegate();
		return new OutputStreamWriter(buffer.getOutputStream(), charset);
	}

	protected void sendCommand(String command) throws IOException {
		socketOutputStream.write(command.getBytes(charset));
		socketOutputStream.write(EOL);
		socketOutputStream.flush();
	}

	protected void awaitResponse(String match) throws IOException {
		String line = socketReader.readLine();
		if(line == null)
			throw new EOFException("Null receive from readLine()");
		if(!line.startsWith(match))
			throw new IOException("Unexpected response: " + line);
	}
	@Override
	protected void prepareDelegate() throws IOException {
		// reset buffer
		buffer.clear();
	}

	@Override
	public void flush() throws IOException {
		super.flush();
		// send PUSH, wait for OKAY and send all data, and wait for OKAY
		String command = new StringBuilder(23).append("PUSH ").append(buffer.size()).toString();
		sendCommand(command);
		awaitResponse("OKAY");
		buffer.copyInto(socketOutputStream);
		socketOutputStream.flush();
		awaitResponse("OKAY");
		buffer.clear();
	}

	@Override
	protected void closeDelegate() {
		super.closeDelegate();
		try {
			sendCommand("QUIT");
			awaitResponse("OKAY");
		} catch(IOException e) {
			// ignore
		}
		try {
			socketReader.close();
		} catch(IOException e) {
			// ignore
		}
		try {
			socket.close();
		} catch(IOException e) {
			// ignore
		}
		socketReader = null;
		socketOutputStream = null;
		socket = null;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String remoteCommandHost) {
		String old = this.host;
		this.host = remoteCommandHost;
		if(!ConvertUtils.areEqual(old, remoteCommandHost))
			reset();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int remoteCommandPort) {
		int old = this.port;
		this.port = remoteCommandPort;
		if(old != remoteCommandPort)
			reset();
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int remoteCommandTimeout) {
		int old = this.timeout;
		this.timeout = remoteCommandTimeout;
		Socket socket = this.socket;
		if(old != remoteCommandTimeout && socket != null)
			try {
				socket.setSoTimeout(remoteCommandTimeout);
			} catch(SocketException e) {
				// ignore
			}
	}
}
