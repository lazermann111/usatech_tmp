package simple.monitor;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.Charset;

import javax.net.ssl.SSLSocketFactory;

import simple.bean.ConvertUtils;
import simple.io.BlockingWriter;

public class RemoteCommandFileWriter extends BlockingWriter {
	protected String host;
	protected int port = 5668;
	protected int timeout = 5000;
	protected Socket socket;
	protected OutputStream socketOutputStream;
	// protected BufferedReader socketReader;
	protected final Charset charset = Charset.forName("UTF-8");
	@Override
	protected Writer openDelegate() throws IOException {
		// open connection
		socket = SSLSocketFactory.getDefault().createSocket();
		/*
		if(socket instanceof SSLSocket) {
			SSLSocket sslSocket = (SSLSocket) socket;
			sslSocket.setEnabledProtocols(new String[] { "TLSv1" });
		}//*/
		socket.connect(new InetSocketAddress(getHost(), getPort()), getTimeout());
		socket.setSoTimeout(timeout);
		socketOutputStream = socket.getOutputStream();
		// socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));
		return new OutputStreamWriter(socketOutputStream, charset);
	}
	@Override
	protected void prepareDelegate() throws IOException {
	}

	@Override
	protected void closeDelegate() {
		super.closeDelegate();
		try {
			socket.close();
		} catch(IOException e) {
			// ignore
		}
		// socketReader = null;
		socketOutputStream = null;
		socket = null;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String remoteCommandHost) {
		String old = this.host;
		this.host = remoteCommandHost;
		if(!ConvertUtils.areEqual(old, remoteCommandHost))
			reset();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int remoteCommandPort) {
		int old = this.port;
		this.port = remoteCommandPort;
		if(old != remoteCommandPort)
			reset();
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int remoteCommandTimeout) {
		int old = this.timeout;
		this.timeout = remoteCommandTimeout;
		Socket socket = this.socket;
		if(old != remoteCommandTimeout && socket != null)
			try {
				socket.setSoTimeout(remoteCommandTimeout);
			} catch(SocketException e) {
				// ignore
			}
	}
}
