package simple.monitor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import simple.app.AbstractService;
import simple.app.ServiceException;
import simple.io.BlockingWriter;
import simple.io.ByteChecker;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.ResultFuture;

public class AppMonitorService extends AbstractService {
	protected final ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(0, new CustomThreadFactory("AppMonitorCheck-", true, 3));
	protected final BlockingQueue<Runnable> pausedRunnables = new LinkedBlockingQueue<Runnable>();
	protected final LocalAppMonitor localAppMonitor = new LocalAppMonitor();
	protected String appInstance;

	public AppMonitorService(String serviceName) {
		super(serviceName);
		localAppMonitor.setScheduledExecutorService(scheduler);
	}

	public void initialize() {
		// schedule init
		localAppMonitor.scheduleSetup(appInstance);
	}

	@Override
	public int getNumThreads() {
		return scheduler.getCorePoolSize();
	}

	@Override
	public int pauseThreads() throws ServiceException {
		scheduler.getQueue().drainTo(pausedRunnables);
		return getNumThreads();
	}

	@Override
	public int unpauseThreads() throws ServiceException {
		pausedRunnables.drainTo(scheduler.getQueue());
		return getNumThreads();
	}

	@Override
	public int startThreads(int numThreads) throws ServiceException {
		if(numThreads <= 0)
			return 0;
		synchronized(scheduler) {
			scheduler.setCorePoolSize(scheduler.getCorePoolSize() + numThreads);
		}
		return numThreads;
	}

	@Override
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		if(numThreads <= 0)
			return 0;
		synchronized(scheduler) {
			int current = scheduler.getCorePoolSize();
			if(current < numThreads)
				numThreads = current;
			scheduler.setCorePoolSize(current - numThreads);
		}
		return numThreads;
	}

	@Override
	public int restartThreads(long timeout) throws ServiceException {
		return 0;
	}

	@Override
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		int numThreads;
		synchronized(scheduler) {
			numThreads = scheduler.getCorePoolSize();
			scheduler.setCorePoolSize(0);
		}
		return numThreads;
	}

	@Override
	public Future<Boolean> shutdown() throws ServiceException {
		return new ResultFuture<Boolean>() {
			protected Boolean produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				scheduler.shutdown();
				return Boolean.TRUE;
			}
		};
	}

	public String getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(String appInstance) {
		this.appInstance = appInstance;
	}

	public RemoteFileUpdater getRemoteFileUpdater() {
		return localAppMonitor.getRemoteFileUpdater();
	}

	public void setRemoteFileUpdater(RemoteFileUpdater remoteFileUpdater) {
		localAppMonitor.setRemoteFileUpdater(remoteFileUpdater);
	}

	public long getServiceCheckInterval() {
		return localAppMonitor.getServiceCheckInterval();
	}

	public void setServiceCheckInterval(long serviceCheckInterval) {
		localAppMonitor.setServiceCheckInterval(serviceCheckInterval);
	}

	public String getServiceDefinitionTemplate() {
		return localAppMonitor.getServiceDefinitionTemplate();
	}

	public void setServiceDefinitionTemplate(String serviceDefinitionTemplate) {
		localAppMonitor.setServiceDefinitionTemplate(serviceDefinitionTemplate);
	}

	public OutputFormat getOutputFormat() {
		return localAppMonitor.getOutputFormat();
	}

	public void setOutputFormat(OutputFormat outputFormat) {
		localAppMonitor.setOutputFormat(outputFormat);
	}

	public int getServiceRefreshFrequency() {
		return localAppMonitor.getServiceRefreshFrequency();
	}

	public void setServiceRefreshFrequency(int serviceRefreshFrequency) {
		localAppMonitor.setServiceRefreshFrequency(serviceRefreshFrequency);
	}

	public boolean isReloadOnDefinitionChange() {
		return localAppMonitor.isReloadOnDefinitionChange();
	}

	public void setReloadOnDefinitionChange(boolean reloadOnDefinitionChange) {
		localAppMonitor.setReloadOnDefinitionChange(reloadOnDefinitionChange);
	}

	public void setByteChecker(ByteChecker<?> byteChecker) {
		localAppMonitor.setByteChecker(byteChecker);
	}

	public BlockingWriter getExternalCommandWriter() {
		return localAppMonitor.getExternalCommandWriter();
	}

	public void setExternalCommandWriter(BlockingWriter externalCommandWriter) {
		localAppMonitor.setExternalCommandWriter(externalCommandWriter);
	}
}
