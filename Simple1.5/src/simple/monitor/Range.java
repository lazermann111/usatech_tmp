package simple.monitor;

public interface Range<V> {
	public boolean isIn(V value);

	public V getMin();

	public V getMax();
}
