package simple.monitor;

import java.sql.Types;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.db.Column;
import simple.results.ListResults;
import simple.util.AbstractLookupMap;
import simple.util.CollectionUtils;

public class PerfCheckResults extends ListResults<PerfCheckResult> {
	protected static final Column[] COLUMNS = new Column[] {
		new Column(), new Column(), new Column()
	};
	protected static final Set<String> COLUMN_NAMES;
	static {
		COLUMNS[0].setColumnClass(String.class);
		COLUMNS[0].setIndex(1);
		COLUMNS[0].setName("Stat");
		COLUMNS[0].setPropertyName("Stat");
		COLUMNS[0].setSqlType(Types.VARCHAR);

		COLUMNS[1].setColumnClass(Number.class);
		COLUMNS[1].setIndex(2);
		COLUMNS[1].setName("Value");
		COLUMNS[1].setPropertyName("Value");
		COLUMNS[1].setSqlType(Types.NUMERIC);

		COLUMNS[2].setColumnClass(String.class);
		COLUMNS[2].setIndex(3);
		COLUMNS[2].setName("Result");
		COLUMNS[2].setPropertyName("Result");
		COLUMNS[2].setSqlType(Types.VARCHAR);

		Set<String> names = new LinkedHashSet<String>(COLUMNS.length);
		for(Column c : COLUMNS)
			names.add(c.getPropertyName());
		COLUMN_NAMES = Collections.unmodifiableSet(names);
	}
	public PerfCheckResults(List<PerfCheckResult> perfCheckList) {
		super(COLUMNS, perfCheckList);
	}

	@Override
	protected String format(Object value, int column) {
		switch(column) {
			case 2:
				PerfCheckResult pcr = rows.get((currentRow - 1) + offset);
				UOM uom = pcr.getPerfCheck().getUom();
				if(uom == null)
					uom = UOM.NONE;
				return uom.format((Number) value);
		}
		return super.format(value, column);
	}

	protected Object[] toValueArray(PerfCheckResult rowData) {
		return new Object[] { rowData.getName(), rowData.getValue(), rowData.getState() };
	}

	protected Map<String, Object> toValueMap(final PerfCheckResult rowData) {
		return new AbstractLookupMap<String, Object>() {
			@Override
			public Object get(Object key) {
				if(!(key instanceof String))
					return null;
				switch(CollectionUtils.iterativeSearch(COLUMN_NAMES, (String) key)) {
					case 0:
						return rowData.getName();
					case 1:
						return rowData.getValue();
					case 2:
						return rowData.getState();
				}
				return null;
			}

			@Override
			public Set<String> keySet() {
				return COLUMN_NAMES;
			}
		};
	}
}
