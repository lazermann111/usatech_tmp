package simple.monitor;

import java.io.IOException;
import java.nio.channels.WritableByteChannel;

public interface RemoteFileUpdater {
	public static interface RemoteFileUpdate {
		public WritableByteChannel getWritableByteChannel() throws IOException;

		public boolean commitChanges() throws IOException;
	}

	public RemoteFileUpdate getRemoteFileUpdate(String filename) throws IOException;
}
