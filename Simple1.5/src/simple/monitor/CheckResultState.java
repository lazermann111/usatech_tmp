package simple.monitor;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.lang.InvalidIntValueException;

public enum CheckResultState {
	OK, WARNING, CRITICAL, UNKNOWN;
	public int getValue() {
		return ordinal();
	}

	public static CheckResultState getByValue(int value) throws InvalidIntValueException {
		try {
			return values()[value];
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new InvalidIntValueException(value);
		}
	}

	public CheckResultState and(CheckResultState other) {
		switch(this) {
			case CRITICAL:
				return this;
			case WARNING:
				if(other == CRITICAL)
					return other;
				return this;
			case OK:
				if(other == UNKNOWN)
					return this;
			default:
				return other;
		}
	}

	public static <V extends Number> CheckResultState determineState(PerfCheck<V> perfCheck, Object value) throws ConvertException {
		return determineState(perfCheck, ConvertUtils.convert(perfCheck.getValueClass(), value));
	}

	public static <V extends Number, S extends V> CheckResultState determineState(PerfCheck<V> perfCheck, S value) {
		if(perfCheck.getCritRange() != null && perfCheck.getCritRange().isIn(value))
			return CheckResultState.CRITICAL;
		if(perfCheck.getWarnRange() != null && perfCheck.getWarnRange().isIn(value))
			return CheckResultState.WARNING;
		if(perfCheck.getOkayRange() != null && perfCheck.getOkayRange().isIn(value))
			return CheckResultState.OK;
		return CheckResultState.UNKNOWN;
	}
}