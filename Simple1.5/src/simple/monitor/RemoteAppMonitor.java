package simple.monitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.Configurer;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.converters.MapConverter;
import simple.db.Call;
import simple.db.Cursor;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;
import simple.io.BlockingWriter;
import simple.io.IOUtils;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.results.Results;
import simple.text.StringUtils;
import simple.util.MapBackedSet;

public class RemoteAppMonitor extends AppMonitor implements Configurer {
	private static Log log = Log.getLog();
	protected static final char[] DEPEND_NAME_SEPARATORS = ":./".toCharArray();
	protected static final Pattern CFG_FILE_PATTERN = Pattern.compile("(A|H|Q|DS)_.+\\.cfg(?:\\.NEW)?");
	protected static final Charset REMOTE_LISTENER_CHARSET = Charset.forName("UTF-8");

	protected static String getHostDefinitionFileName(String host) {
		return "H_" + host + ".cfg";
	}

	protected static String getDataSourceDefinitionFileName(String dataSourceName) {
		return "DS_" + dataSourceName + ".cfg";
	}

	protected static String getQueryDefinitionFileName(QueryCheck queryCheck) {
		return "Q_" + queryCheck.getDataSourceName() + '_' + queryCheck.getName() + ".cfg";
	}

	protected DefinitionFile getHostDefinitionFile(String host) {
		return new LocalDefinitionFile(getHostDefinitionFileName(host));
	}

	protected DefinitionFile getDataSourceDefinitionFile(String dataSourceName) {
		return new LocalDefinitionFile(getDataSourceDefinitionFileName(dataSourceName));
	}

	protected DefinitionFile getQueryDefinitionFile(QueryCheck queryCheck) {
		return new LocalDefinitionFile(getQueryDefinitionFileName(queryCheck));
	}

	protected class LocalDefinitionFile extends DefinitionFile {
		protected final File targetFile;
		protected final File tmpFile;

		public LocalDefinitionFile(String definitionFileName) {
			super(definitionFileName);
			String targetPath = getDefinitionDir().getAbsolutePath() + File.separator + definitionFileName;
			this.targetFile = new File(targetPath);
			this.tmpFile = new File(targetPath + ".NEW");
		}

		@Override
		protected WritableByteChannel getWritableByteChannel() throws IOException {
			return new FileOutputStream(tmpFile).getChannel();
		}

		protected boolean update() {
			if(targetFile.exists()) {
				try {
					if(IOUtils.firstDifferenceIgnoreWhitespace(targetFile, tmpFile) < 0)
						return false;
				} catch(IOException e) {
					log.warn("Could not compare old vs new cfg file " + targetFile.getAbsolutePath(), e);
				}
			}
			boolean result = IOUtils.moveOverrideFile(tmpFile, targetFile);
			if(result) {
				if(log.isInfoEnabled())
					log.info("Copied new contents to " + targetFile.getAbsolutePath());
			} else if(log.isWarnEnabled())
				log.warn("Could not copy new contents to " + targetFile.getAbsolutePath());
			if(result && isReloadOnDefinitionChange()) {
				reloadPending.set(true);
			}
			return result;
		}
	}

	protected class RemoteAppCheck extends AppCheck {
		protected final int appPort;
		protected JMXConnector jmxc;
		protected final AppGroup appGroup;

		public RemoteAppCheck(AppGroup appGroup, String appHost, int appPort) {
			super(new FileBasedDefinition(new LocalDefinitionFile("A_" + appGroup.getAppName() + '_' + appHost + "_" + appPort + ".cfg")), appGroup.getAppName() + '_' + appPort, appHost, appGroup.getAppName() + " on " + appHost + ':' + appPort);
			this.appGroup = appGroup;
			this.appPort = appPort;
		}

		protected void checkConnection() {
			JMXConnector jmxc = this.jmxc;
			if(jmxc != null)
				try {
					jmxc.getConnectionId();
				} catch(IOException e) {
					log.info("Closing JMXConnection because " + e.getMessage());
					close();
				}
		}

		@Override
		protected MBeanServerConnection getMBeanServerConnection() throws IOException {
			if(mbsc == null) {
				JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + appHost + ':' + appPort + "/jmxrmi");
				Map<String, Object> environment;
				if(!StringUtils.isBlank(jmxUsername)) {
					environment = new HashMap<String, Object>();
					environment.put(JMXConnector.CREDENTIALS, new String[] { jmxUsername, jmxPassword });
				} else
					environment = null;
				jmxc = JMXConnectorFactory.connect(url, environment);
				mbsc = jmxc.getMBeanServerConnection();
			}
			return mbsc;
		}

		@Override
		protected void closeConnection() {
			if(jmxc != null)
				try {
					jmxc.close();
				} catch(IOException e) {
					// ignore
				}
			jmxc = null;
		}

		public int getAppPort() {
			return appPort;
		}

		public AppGroup getAppGroup() {
			return appGroup;
		}
	}

	public class AppGroup {
		protected final Map<String, AppCheck> appChecks = new LinkedHashMap<String, AppCheck>();
		protected int[] appPorts;
		protected final String appName;
		protected String[] appHosts;
		protected long serviceCheckInterval = 60000;

		public AppGroup(String appName) {
			this.appName = appName;
		}

		public int[] getAppPorts() {
			return appPorts;
		}

		public void setAppPorts(int[] appPorts) {
			this.appPorts = appPorts;
		}

		public String getAppName() {
			return appName;
		}

		public String[] getAppHosts() {
			return appHosts;
		}

		public void setAppHosts(String[] appHosts) {
			this.appHosts = appHosts;
		}

		public long getServiceCheckInterval() {
			return serviceCheckInterval;
		}

		public void setServiceCheckInterval(long serviceCheckInterval) {
			this.serviceCheckInterval = serviceCheckInterval;
		}

		public void cancelChecking() {
			for(AppCheck ac : appChecks.values())
				ac.cancelChecking();
		}

		public void scheduleChecking() throws IOException {
			long interval = getServiceCheckInterval();
			if(appHosts == null || appPorts == null || appHosts.length == 0 || appPorts.length == 0 || interval <= 0) {
				for(AppCheck ac : appChecks.values())
					ac.cancelChecking();
				appChecks.clear();
			} else {
				Set<String> remaining = new LinkedHashSet<String>(appChecks.keySet());
				for(int p : appPorts) {
					for(String h : appHosts) {
						final String key = h + ':' + p;
						remaining.remove(key);
						AppCheck ac = appChecks.get(key);
						if(ac == null) {
							ac = new RemoteAppCheck(this, h, p);
							appChecks.put(key, ac);
							if(interval > 0)
								ac.scheduleChecking(interval);
						} else {
							if(interval > 0)
								ac.scheduleChecking(interval);
							else
								ac.cancelChecking();
						}
						if(!definedHosts.contains(h)) {
							InetAddress ia;
							try {
								ia = InetAddress.getByName(h);
							} catch(UnknownHostException e) {
								log.error("Could not find host '" + h + "'");
								continue;
							}
							writeHostDefinitionFile(ia);
						}
					}
				}
				for(String key : remaining) {
					AppCheck ac = appChecks.get(key);
					if(ac != null) {
						ac.cancelChecking();
						appChecks.remove(key);
					}
				}
			}
		}
	}

	public class QueryGroup implements Runnable {
		protected final ReentrantLock lock = new ReentrantLock();
		protected ScheduledFuture<?> scheduled;
		protected final Map<String, QueryCheck> queryChecks = new LinkedHashMap<String, QueryCheck>();
		protected long checkInterval = 60000;
		protected long usedInterval;
		protected final String name;
		protected String definitionTemplate;

		public QueryGroup(String name) {
			super();
			this.name = name;
		}

		public void run() {
			for(QueryCheck queryCheck : queryChecks.values())
				try {
					queryCheck.doCheck();
				} catch(Throwable e) {
					log.error("Unexpected error while checking query " + queryCheck.getName() + " on " + queryCheck.getDataSourceName(), e);
				}
		}

		public long getCheckInterval() {
			return checkInterval;
		}

		public void setCheckInterval(long checkInterval) {
			this.checkInterval = checkInterval;
		}

		public String getName() {
			return name;
		}

		public QueryCheck getQueryCheck(String name) {
			QueryCheck qc = queryChecks.get(name);
			if(qc == null) {
				qc = new QueryCheck(this, name);
				queryChecks.put(name, qc);
			}
			return qc;
		}
		
		public void scheduleChecking() {
			long interval = getCheckInterval();
			lock.lock();
			try {
				if(scheduled != null && this.usedInterval == interval)
					return;
				if(dataSourceCheckInterval == 0 || dataSourceCheckInterval > interval)
					dataSourceCheckInterval = interval;
				if(scheduled != null) {
					scheduled.cancel(false);
					scheduled = null;
				}
				scheduled = scheduledExecutorService.scheduleAtFixedRate(this, 0, interval, TimeUnit.MILLISECONDS);
				this.usedInterval = interval;
			} finally {
				lock.unlock();
			}
			if(log.isInfoEnabled())
				log.info("Scheduled checking Query Group " + getName() + " every " + interval + " ms");
		}

		public void cancelChecking() {
			lock.lock();
			try {
				if(this.scheduled != null) {
					scheduled.cancel(false);
					scheduled = null;
				}
			} finally {
				lock.unlock();
			}
		}

		public String getDefinitionTemplate() {
			return StringUtils.isBlank(definitionTemplate) ? getQueryDefinitionTemplate() : definitionTemplate;
		}

		public void setDefinitionTemplate(String definitionTemplate) {
			this.definitionTemplate = definitionTemplate;
		}
	}

	protected static final Set<String> QUERY_CHECK_EXCLUDE_CONFIG_SETTINGS = new HashSet<String>(Arrays.asList(new String[] { "use", "host_name", "service_description", "display_name" }));

	protected void handleServiceCheckResult(long checkTime, QueryCheck queryCheck, CheckResultState state, List<PerfCheckResult> perfCheckResults, Results textResults, String comments) {
		handleServiceCheckResult(checkTime, queryCheck.getDataSourceName(), queryCheck.getName(), state, perfCheckResults, textResults, comments);
	}

	public class QueryCheck {
		protected final QueryGroup queryGroup;
		protected final ReentrantLock lock = new ReentrantLock();
		protected final Call call;
		protected final Map<String, ContiguousSinglePerfCheck<?>> valueChecks = new LinkedHashMap<String, ContiguousSinglePerfCheck<?>>();
		protected ContiguousSinglePerfCheck<? extends Number> perfCheck = ContiguousSinglePerfCheck.getInstance(null, 0, 1, 1, Integer.MAX_VALUE, UOM.NONE);
		protected boolean rowCountCheck = true;
		protected long lastCheckTime;
		protected final Set<QueryCheck> depends = new HashSet<QueryCheck>();
		protected volatile CheckResultState lastState;
		protected final Map<String, String> configSettings = new LinkedHashMap<String, String>();
		protected String definitionTemplate;

		public QueryCheck(QueryGroup queryGroup, String name) {
			this.queryGroup = queryGroup;
			this.call = new Call();
			Cursor c = new Cursor();
			c.setAllColumns(true);
			this.call.setArgument(0, c);
			this.call.setLabel(name);
		}

		public int getCriticalRowCount() {
			if(!rowCountCheck)
				return -1;
			@SuppressWarnings("unchecked")
			ContiguousSinglePerfCheck<Integer> pci = (ContiguousSinglePerfCheck<Integer>) perfCheck;
			return pci.getCritLimit();
		}
		public void setCriticalRowCount(int rowCount) {
			if(rowCount < 1)
				throw new IllegalArgumentException("Row count limit must be greater than zero");
			if(!rowCountCheck) {
				perfCheck = ContiguousSinglePerfCheck.getInstance(null, 0, rowCount, rowCount, Integer.MAX_VALUE, UOM.NONE);
				rowCountCheck = true;
			} else {
				@SuppressWarnings("unchecked")
				ContiguousSinglePerfCheck<Integer> pci = (ContiguousSinglePerfCheck<Integer>) perfCheck;
				pci.setWarnLimit(rowCount);
				pci.setCritLimit(rowCount);
			}
			
		}

		public Number getCriticalColumnValue(String column) {
			if(rowCountCheck)
				return null;
			ContiguousSinglePerfCheck<?> pc = valueChecks.get(column);
			if(pc == null)
				return null;
			return pc.getCritLimit();
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		public void setCriticalColumnValue(String column, Number value) {
			rowCountCheck = false;
			if(value == null)
				valueChecks.remove(column);
			else {
				ContiguousSinglePerfCheck pc = valueChecks.get(column);
				if(pc == null) 
					valueChecks.put(column, new ContiguousSinglePerfCheck(value.getClass(), column, null, value, value, null, UOM.NONE));
				else
					pc.setCritLimit(value);
			}
		}

		public String getSql() {
			return call.getSql();
		}

		public void setSql(String sql) {
			call.setSql(sql);
		}

		public void setMaxRows(int maxRows) {
			call.setMaxRows(maxRows);
		}

		public double getQueryTimeout() {
			return call.getQueryTimeout();
		}

		public void setQueryTimeout(double queryTimeout) {
			call.setQueryTimeout(queryTimeout);
		}

		public String getDataSourceName() {
			return call.getDataSourceName();
		}

		public void setDataSourceName(String dataSourceName) {
			call.setDataSourceName(dataSourceName);
			// TODO: handle change
		}

		public String getName() {
			return call.getLabel();
		}

		protected void writeConfigSetting(Writer output, String name, String defaultValue) throws IOException {
			String value = getConfigSetting(name);
			if(StringUtils.isBlank(value))
				value = defaultValue;
			if(!StringUtils.isBlank(value))
				writeDirective(name, value, output);
		}
		
		public void initialize() throws IOException {
			String dsn = getDataSourceName();
			AtomicLong lastDataSourceCheck = definedDataSourceNames.get(dsn);
			if(lastDataSourceCheck == null) {
				DefinitionFile sdFile = getDataSourceDefinitionFile(dsn);
				Writer output = sdFile.newFileWriter();
				output.write("define host{\n");
				if(!StringUtils.isBlank(getDataSourceDefinitionTemplate()))
					writeDirective("use", getDataSourceDefinitionTemplate(), output);
				writeDirective("host_name", dsn, output);
				writeDirective("alias", dsn, output);
				writeDirective("address", "-", output);
				output.write("}\n");
				output.flush();
				output.close();
				sdFile.updateIfChanged();
				lastDataSourceCheck = new AtomicLong();
				definedDataSourceNames.put(dsn, lastDataSourceCheck);
			}
			DefinitionFile sdFile = getQueryDefinitionFile(this);
			Writer output = sdFile.newFileWriter();
			if(log.isInfoEnabled())
				log.info("Setting up query check for " + getName() + " on " + dsn);
			output.write("define service {\n");
			writeDirective("use", getDefinitionTemplate(), output);
			writeDirective("host_name", dsn, output);
			writeDirective("service_description", getName(), output);
			writeConfigSetting(output, "display_name", getName());
			for(Map.Entry<String, String> entry : configSettings.entrySet()) {
				if(!StringUtils.isBlank(entry.getKey()) && !StringUtils.isBlank(entry.getValue()) && !QUERY_CHECK_EXCLUDE_CONFIG_SETTINGS.contains(entry.getKey()))
					writeDirective(entry.getKey(), entry.getValue(), output);
			}
			output.write("}\n");
			for(QueryCheck depend : depends) {
				output.write("\ndefine servicedependency{\n");
				writeDirective("dependent_host_name", depend.getDataSourceName(), output);
				writeDirective("dependent_service_description", depend.getName(), output);
				writeDirective("host_name", dsn, output);
				writeDirective("service_description", getName(), output);
				writeDirective("inherits_parent", "1", output);
				writeDirective("execution_failure_criteria", "c,u,p", output);
				writeDirective("notification_failure_criteria", "c,u,p", output);
				output.write("}\n");
			}

			output.flush();
			output.close();
			sdFile.updateIfChanged();
		}

		public void doCheck() {
			lock.lock();
			try {
				QueryCheck failedDepend = getFirstFailingDepend();
				if(failedDepend != null) {
					if(log.isInfoEnabled())
						log.info("Skipping check '" + getName() + "' because '" + failedDepend + "' is " + failedDepend.getLastState());
					return;
				}
				String dsn = getDataSourceName();
				AtomicLong lastDataSourceCheck = definedDataSourceNames.get(dsn);
				long time = System.currentTimeMillis();
				boolean writeDSResult;
				while(true) {
					long c = lastDataSourceCheck.get();
					if(time <= c + dataSourceCheckInterval) {
						writeDSResult = false;
						break;
					}
					if(lastDataSourceCheck.compareAndSet(c, time)) {
						writeDSResult = true;
						break;
					}
				}
				Connection conn;
				try {
					conn = DataLayerMgr.getConnection(dsn);
				} catch(SQLException e) {
					log.error("Could not execute query check '" + getName() + "'", e);
					lastCheckTime = time;
					lastState = CheckResultState.UNKNOWN;
					handleServiceCheckResult(lastCheckTime, this, CheckResultState.UNKNOWN, null, null, StringUtils.exceptionToString(e));
					if(writeDSResult)
						handleHostCheckResult(time, dsn, CheckResultState.CRITICAL, null, null, StringUtils.exceptionToString(e));
					return;
				} catch(DataLayerException e) {
					log.error("Could not execute query check '" + getName() + "'", e);
					lastCheckTime = time;
					lastState = CheckResultState.UNKNOWN;
					handleServiceCheckResult(lastCheckTime, this, CheckResultState.UNKNOWN, null, null, StringUtils.exceptionToString(e));
					if(writeDSResult)
						handleHostCheckResult(time, dsn, CheckResultState.CRITICAL, null, null, StringUtils.exceptionToString(e));
					return;
				}
				try {
					if(writeDSResult)
						handleHostCheckResult(time, dsn, CheckResultState.OK, null, null, null);
					final Results results = call.executeQuery(conn, null, null);
					lastCheckTime = System.currentTimeMillis();
					CheckResultState state = CheckResultState.UNKNOWN;
					final List<PerfCheckResult> checkResults;
					if(isRowCountCheck()) {
						int value = results.getRowCount();
						try {
							state = CheckResultState.determineState(perfCheck, value);
						} catch(ConvertException e) {
							log.warn("Could not convert " + value, e);
							state = CheckResultState.UNKNOWN;
						}
						checkResults = Collections.singletonList(new PerfCheckResult("Rows", state, value, perfCheck));
					} else {
						checkResults = new ArrayList<PerfCheckResult>(results.getRowCount() * valueChecks.size());
						while(results.next()) {
							for(ContiguousSinglePerfCheck<?> valueCheck : valueChecks.values()) {
								Map<String, Number> values = new LinkedHashMap<String, Number>(valueCheck.getAttributeNames().size());
								for(String an : valueCheck.getAttributeNames())
									values.put(an, ConvertUtils.convertSafely(Number.class, results.getValue(an), null));
								Number value = valueCheck.getValue(values);
								CheckResultState localState;
								try {
									localState = CheckResultState.determineState(valueCheck, value);
								} catch(ConvertException e) {
									log.warn("Could not convert " + value, e);
									localState = CheckResultState.UNKNOWN;
								}
								checkResults.add(new PerfCheckResult(StringUtils.join(valueCheck.getAttributeNames(), ","), localState, value, valueCheck));
								state = state.and(localState);
							}
						}
					}
					lastState = state;
					results.setRow(0);
					handleServiceCheckResult(lastCheckTime, this, state, checkResults, results, null);
				} catch(ParameterException e) {
					log.error("Could not execute query check '" + getName() + "'", e);
					lastCheckTime = System.currentTimeMillis();
					lastState = CheckResultState.UNKNOWN;
					handleServiceCheckResult(lastCheckTime, this, CheckResultState.UNKNOWN, null, null, StringUtils.exceptionToString(e));
				} catch(SQLException e) {
					log.error("Failed when executing query check '" + getName() + "'", e);
					lastCheckTime = System.currentTimeMillis();
					lastState = CheckResultState.CRITICAL;
					handleServiceCheckResult(lastCheckTime, this, CheckResultState.CRITICAL, null, null, StringUtils.exceptionToString(e));
				} finally {
					try {
						conn.close();
					} catch(SQLException e) {
						// ignore
					}
				}
			} finally {
				lock.unlock();
			}
		}

		public boolean isRowCountCheck() {
			return rowCountCheck;
		}

		public void setRowCountCheck(boolean rowCountCheck) {
			this.rowCountCheck = rowCountCheck;
		}

		public CheckResultState getLastState() {
			return lastState;
		}

		protected QueryCheck getFirstFailingDepend() {
			for(QueryCheck queryCheck : depends)
				if(queryCheck.getLastState() != CheckResultState.OK)
					return queryCheck;
			return null;
		}

		public QueryCheck[] getDepends() {
			return depends.toArray(new QueryCheck[depends.size()]);
		}

		public void setDepends(QueryCheck[] depends) {
			this.depends.clear();
			if(depends != null)
				this.depends.addAll(Arrays.asList(depends));
		}

		public String[] getDependNames() {
			String[] dependNames = new String[depends.size()];
			int i = 0;
			for(QueryCheck depend : depends)
				dependNames[i++] = depend.getQueryGroup().getName() + DEPEND_NAME_SEPARATORS[0] + depend.getName();
			return dependNames;
		}

		public void setDependNames(String[] dependNames) {
			this.depends.clear();
			if(dependNames != null)
				for(String dn : dependNames) {
					String[] parts = StringUtils.split(dn, DEPEND_NAME_SEPARATORS, true);
					switch(parts.length) {
						case 0:
							break; // do nothing
						case 1:
							this.depends.add(RemoteAppMonitor.this.getQueryGroup("").getQueryCheck(parts[0]));
							break;
						case 2:
							this.depends.add(RemoteAppMonitor.this.getQueryGroup(parts[0]).getQueryCheck(parts[1]));
							break;
						default:
							throw new IllegalArgumentException("Invalid number of separators found in dependNames in '" + dn + "'");
					}
				}
		}

		public String getConfigSetting(String name) {
			return configSettings.get(name);
		}

		public void setConfigSetting(String name, String value) {
			configSettings.put(name, value);
		}

		public QueryGroup getQueryGroup() {
			return queryGroup;
		}

		public String getDefinitionTemplate() {
			return StringUtils.isBlank(definitionTemplate) ? getQueryGroup().getDefinitionTemplate() : definitionTemplate;
		}

		public void setDefinitionTemplate(String definitionTemplate) {
			this.definitionTemplate = definitionTemplate;
		}
	}
	protected final CountDownLatch exitSignal = new CountDownLatch(1);
	protected final Map<String, AppGroup> appGroups = new LinkedHashMap<String, AppGroup>();
	protected File definitionDir;
	protected String jmxUsername;
	protected String jmxPassword;
	protected String queryDefinitionTemplate = "app-monitor-query";
	protected String hostDefinitionTemplate = "linux-server";
	protected String dataSourceDefinitionTemplate = "data-source";
	protected final Set<String> definedHosts = new MapBackedSet<String>(new ConcurrentHashMap<String, Object>());
	protected final Map<String, AtomicLong> definedDataSourceNames = new ConcurrentHashMap<String, AtomicLong>();
	protected final Map<String, QueryGroup> queryGroups = new LinkedHashMap<String, QueryGroup>();
	protected long dataSourceCheckInterval;
	protected int listenPort;
	protected boolean listenSsl = true;

	@Override
	public void configure(Configuration config) {
		reset();
		configureFromScratch(config);
	}

	protected void reset() {
		super.reset();
		for(Iterator<AppGroup> iter = appGroups.values().iterator(); iter.hasNext();) {
			AppGroup ag = iter.next();
			ag.cancelChecking();
			iter.remove();
		}
		for(Iterator<QueryGroup> iter = queryGroups.values().iterator(); iter.hasNext();) {
			QueryGroup qg = iter.next();
			qg.cancelChecking();
			iter.remove();
		}
		definedHosts.clear();
		definedDataSourceNames.clear();
		
		// Reset simple properties to their default values
		definitionDir = null;
		jmxUsername = null;
		jmxPassword = null;
		queryDefinitionTemplate = "app-monitor-query";
		hostDefinitionTemplate = "linux-server";
		dataSourceDefinitionTemplate = "data-source";
		dataSourceCheckInterval = 0;
		
		log.info("Reset RemoteAppMonitor");
	}

	protected void configureFromScratch(Configuration config) {
		// configure
		Map<String, Object> namedReferences = new HashMap<String, Object>();
		namedReferences.put(getClass().getName(), this);
		try {
			BaseWithConfig.configureDataSourceFactory(config, namedReferences);
			BaseWithConfig.configureProperties(this, config.subset(getClass().getName()), namedReferences, true);
		} catch(Throwable e) {
			log.error("Could not configure AppMonitor; exiting", e);
			System.exit(1);
			return;
		}

		// start timers
		for(AppGroup ag : appGroups.values())
			try {
				ag.scheduleChecking();
			} catch(IOException e) {
				log.error("Could not configure AppChecks for " + ag.getAppName() + "; exiting", e);
				System.exit(5);
				return;
			}

		for(QueryGroup qg : queryGroups.values())
			qg.scheduleChecking();

		// clean up
		for(File f : getDefinitionDir().listFiles()) {
			if(CFG_FILE_PATTERN.matcher(f.getName()).matches() && !cfgFiles.contains(f.getName()) && f.canWrite() && f.isFile()) {
				if(f.delete()) {
					if(isReloadOnDefinitionChange())
						reloadPending.set(true);
					if(log.isInfoEnabled())
						log.info("Deleted unused cfg file '" + f.getAbsolutePath() + "'");
				} else if(log.isWarnEnabled())
					log.warn("Could not delete unused cfg file '" + f.getAbsolutePath() + "'");
			}
		}

		reloadExternalIfNeeded();
		try {
			startRemoteCommandListener();
		} catch(IOException e) {
			log.error("Could not start remote listening server on port " + getListenPort() + "; exiting", e);
			System.exit(7);
			return;
		}
		log.info("Configured and Started AppMonitor");
	}

	protected void startRemoteCommandListener() throws IOException {
		if(getListenPort() > 0) {
			final ServerSocket serverSocket;
			if(isListenSsl())
				serverSocket = SSLServerSocketFactory.getDefault().createServerSocket(getListenPort());
			else
				serverSocket = ServerSocketFactory.getDefault().createServerSocket(getListenPort());
			//For now we use a poor man's implementation - one acceptor thread - spawn a thread for every client
			Thread acceptorThread = new Thread("RemoteListener-Acceptor") {
				public void run() {
					IOException lastException = null;
					int attempts = 0;
					while(true) {
						final Socket socket;
						try {
							socket = serverSocket.accept();
						} catch(IOException e) {
							if(lastException == null)
								log.warn("Failed to accept connection", e);
							else if(SystemUtils.sameThrowable(lastException, e)) {
								long time = Math.max(5000 * attempts, 60000);
								log.info("Got same exception trying to accept connection again; sleeping for " + time + " ms");
								try {
									Thread.sleep(time);
								} catch(InterruptedException e1) {
									// ignore
								}
								attempts++;
							} else {
								log.warn("Failed to accept connection", e);
							}
							lastException = e;
							continue;
						}
						boolean okay = false;
						try {
							attempts = 0;
							final String source = socket.getRemoteSocketAddress().toString();
							final InetAddress host = socket.getInetAddress();
							Thread processThread = new Thread("RemoteListener-Processor-" + source) {
								public void run() {
									try {
										writeHostDefinitionFile(host);
										BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), REMOTE_LISTENER_CHARSET));
										String line;
										while((line = reader.readLine()) != null) {
											handleRemoteCommand(line, host, source);
										}
									} catch(IOException e) {
										log.warn("Failed to read from connection " + source, e);
									} finally {
										try {
											socket.close();
										} catch(IOException e) {
											// ignore
										}
									}
									log.info("Processing of data from " + source + " is ending");
								}
							};
							processThread.start();
							okay = true;
						} finally {
							if(!okay)
								try {
									socket.close();
								} catch(IOException e) {
									// ignore
								}
						}
					}
				}
			};
			acceptorThread.start();
			
		}
	}

	protected static final Pattern remoteCommandPattern = Pattern.compile("^\\[\\d+\\] (PROCESS_HOST_CHECK_RESULT|PROCESS_SERVICE_CHECK_RESULT|UPDATE_HOST_CHECK_CONFIG|UPDATE_SERVICE_CHECK_CONFIG);([ -~]*)$");

	protected void handleRemoteCommand(String command, InetAddress host, String source) throws IOException {
		Matcher matcher = remoteCommandPattern.matcher(command);
		if(!matcher.matches())
			throw new IOException("Invalid command from " + source);
		String commandType = matcher.group(1);
		if("UPDATE_HOST_CHECK_CONFIG".equals(commandType))
			updateHostCheckConfig(matcher.group(2), host, source);
		else if("UPDATE_SERVICE_CHECK_CONFIG".equals(commandType))
			updateServiceCheckConfig(matcher.group(2), host, source);
		else
			writeRemoteCommand(command, source);
	}

	protected void updateServiceCheckConfig(String data, InetAddress host, String source) throws IOException {
		Map<String, String> parsed = new HashMap<String, String>();
		try {
			MapConverter.parseKeyValuePairs(data, parsed);
		} catch(ConvertException e) {
			throw new IOException(e);
		}
		String template = parsed.get("template");
		String service = parsed.get("name");
		String desc = parsed.get("desc");
		if(StringUtils.isBlank(service))
			throw new IOException("Service name was not provided in 'name' key value pair from " + source);
		if(StringUtils.isBlank(template)) {
			template = getServiceDefinitionTemplate();
			if(StringUtils.isBlank(template))
				throw new IOException("Service template was not provided in 'template' key value pair from " + source + " and no default is set");
		}
		String appHost = host.getHostName();
		String fileName = "RS_" + appHost + '_' + service + ".cfg";
		Definition sd = new FileBasedDefinition(new LocalDefinitionFile(fileName));
		sd.startChanges();
		boolean okay = false;
		try {
			sd.addService(service, template, appHost, desc);
			okay = true;
		} finally {
			if(okay)
				sd.commitChanges();
			else
				sd.abandonChanges();
		}
	}

	protected void updateHostCheckConfig(String data, InetAddress host, String source) throws IOException {
		Map<String, String> parsed = new HashMap<String, String>();
		try {
			MapConverter.parseKeyValuePairs(data, parsed);
		} catch(ConvertException e) {
			throw new IOException(e);
		}
		String name = parsed.get("name");
		String template = parsed.get("template");
		String alias = parsed.get("alias");
		String address = parsed.get("address");
		if(StringUtils.isBlank(name))
			throw new IOException("Host name was not provided in 'name' key value pair from " + source);
		if(StringUtils.isBlank(template)) {
			template = getHostDefinitionTemplate();
			if(StringUtils.isBlank(template))
				throw new IOException("Host template was not provided in 'template' key value pair from " + source + " and no default is set");
		}
		Definition def = new FileBasedDefinition(new LocalDefinitionFile("RH_" + name + ".cfg"));
		def.startChanges();
		boolean okay = false;
		try {
			def.addHost(name, template, alias, address);
			okay = true;
		} finally {
			if(okay)
				def.commitChanges();
			else
				def.abandonChanges();
		}
	}

	protected void writeRemoteCommand(String command, String source) {
		BlockingWriter output = getExternalCommandWriter();
		try {
			output.open();
		} catch(IOException e) {
			if(log.isWarnEnabled())
				log.error("Could not open ExternalCommandWriter", e);
			return;
		}
		try {
			output.write(command);
			OutputFormat.writeNagiosComplete(output);
		} catch(IOException e) {
			if(log.isWarnEnabled())
				log.warn("Failed to write command from " + source, e);
			return;
		} finally {
			output.close();
		}
		if(log.isInfoEnabled())
			log.info("Wrote result from " + source);
	}

	public AppGroup getAppGroup(String name) {
		AppGroup ag = appGroups.get(name);
		if(ag == null) {
			ag = new AppGroup(name);
			appGroups.put(name, ag);
		}
		return ag;
	}

	protected void writeHostDefinitionFile(InetAddress host) throws IOException {
		if(!definedHosts.contains(host.getHostName())) {
			Definition def = new FileBasedDefinition(getHostDefinitionFile(host.getHostName()));
			def.startChanges();
			boolean okay = false;
			try {
				def.addHost(host, getHostDefinitionTemplate());
				okay = true;
			} finally {
				if(okay)
					def.commitChanges();
				else
					def.abandonChanges();
			}
			definedHosts.add(host.getHostName());
		}
	}

	public QueryGroup getQueryGroup(String name) {
		QueryGroup qg = queryGroups.get(name);
		if(qg == null) {
			qg = new QueryGroup(name);
			queryGroups.put(name, qg);
		}
		return qg;
	}

	public File getDefinitionDir() {
		return definitionDir;
	}

	public void setDefinitionDir(File serviceDefinitionDir) {
		this.definitionDir = serviceDefinitionDir;
	}

	public String getJmxUsername() {
		return jmxUsername;
	}

	public void setJmxUsername(String jmxUsername) {
		this.jmxUsername = jmxUsername;
	}

	public String getJmxPassword() {
		return jmxPassword;
	}

	public void setJmxPassword(String jmxPassword) {
		this.jmxPassword = jmxPassword;
	}

	public String getHostDefinitionTemplate() {
		return hostDefinitionTemplate;
	}

	public void setHostDefinitionTemplate(String hostDefinitionTemplate) {
		this.hostDefinitionTemplate = hostDefinitionTemplate;
	}

	public String getQueryDefinitionTemplate() {
		return queryDefinitionTemplate;
	}

	public void setQueryDefinitionTemplate(String queryDefinitionTemplate) {
		this.queryDefinitionTemplate = queryDefinitionTemplate;
	}

	public String getDataSourceDefinitionTemplate() {
		return dataSourceDefinitionTemplate;
	}

	public void setDataSourceDefinitionTemplate(String dataSourceDefinitionTemplate) {
		this.dataSourceDefinitionTemplate = dataSourceDefinitionTemplate;
	}

	public int getListenPort() {
		return listenPort;
	}

	public void setListenPort(int listenPort) {
		this.listenPort = listenPort;
	}

	public boolean isListenSsl() {
		return listenSsl;
	}

	public void setListenSsl(boolean listenSsl) {
		this.listenSsl = listenSsl;
	}
}
