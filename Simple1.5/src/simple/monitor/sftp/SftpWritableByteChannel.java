package simple.monitor.sftp;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

import sun.security.jca.JCAUtil;
import com.trilead.ssh2.SFTPv3Client;
import com.trilead.ssh2.SFTPv3FileHandle;

public class SftpWritableByteChannel implements WritableByteChannel {
	protected final SFTPv3Client sftp;
	protected final SFTPv3FileHandle handle;
	protected long position = 0;
	protected byte[] tempArray;

	public SftpWritableByteChannel(SFTPv3Client sftp, String filepath) throws IOException {
		this(sftp, sftp.createFileTruncate(filepath));
	}

	public SftpWritableByteChannel(SFTPv3Client sftp, SFTPv3FileHandle handle) {
		this.sftp = sftp;
		this.handle = handle;
	}

	@Override
	public void close() throws IOException {
		sftp.closeFile(handle);
	}

	@Override
	public boolean isOpen() {
		try {
			sftp.fstat(handle);
		} catch(IOException e) {
			return false;
		}
		return true;
	}

	@Override
	public int write(ByteBuffer src) throws IOException {
		if(src.hasRemaining() == false)
			return 0;
		final int len;
		if(src.hasArray()) {
			byte[] b = src.array();
			int ofs = src.arrayOffset();
			int pos = src.position();
			int lim = src.limit();
			len = lim - pos;
			sftp.write(handle, position, b, ofs + pos, len);
			src.position(lim);
			position += len;
		} else {
			len = src.remaining();
			int remain = src.remaining();
			int n = JCAUtil.getTempArraySize(len);
			if((tempArray == null) || (n > tempArray.length)) {
				tempArray = new byte[n];
			}
			while(remain > 0) {
				int chunk = Math.min(remain, tempArray.length);
				src.get(tempArray, 0, chunk);
				sftp.write(handle, position, tempArray, 0, chunk);
				remain -= chunk;
				position += chunk;
			}
		}
		return len;
	}
}
