package simple.monitor.sftp;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.nio.channels.WritableByteChannel;

import simple.io.IOUtils;
import simple.io.Log;
import simple.lang.Initializer;
import simple.monitor.RemoteFileUpdater;
import simple.security.crypt.CryptUtils;
import com.trilead.ssh2.Connection;
import com.trilead.ssh2.SFTPv3Client;
import com.trilead.ssh2.ServerHostKeyVerifier;

public class SftpRemoteFileUpdater implements RemoteFileUpdater {
	private static Log log = Log.getLog();
	protected String configDir;
	protected String host;
	protected int port = 22;
	protected Connection connection;
	protected ServerHostKeyVerifier hostKeyVerification;
	protected int connectTimeout = 20000;
	protected int kexTimeout = 20000;
	protected String username;
	protected String password;
	protected String privateKey;
	protected String passphrase;
	protected String dsaPrivateKey;
	protected String rsaPrivateKey;
	protected String tmpPrefix;
	protected String tmpSuffix = ".TMP";

	protected Initializer<IOException> initializer = new Initializer<IOException>() {
		@Override
		protected void doInitialize() throws IOException {
			connection = openRemoteConnection();
		}

		@Override
		protected void doReset() {
			if(connection != null) {
				connection.close();
				connection = null;
			}
		}
	};

	protected Connection getRemoteConnection() throws IOException {
		try {
			initializer.initialize();
		} catch(InterruptedException e) {
			throw new IOException(e);
		}
		return connection;
	}

	protected Connection openRemoteConnection() throws IOException {
		Connection connection = new Connection(getHost(), getPort());
		// Connect to the host
		long connectionStart = System.currentTimeMillis();
		connection.connect(hostKeyVerification, connectTimeout, kexTimeout);
		log.info("Connection to " + connection.getHostname() + ":" + connection.getPort() + " took " + (System.currentTimeMillis() - connectionStart) + " ms");
		// authenticate
		boolean authenticated = false;
		StringBuilder methods = new StringBuilder();
		if(privateKey != null) {
			authenticated = connection.authenticateWithPublicKey(username, privateKey.toCharArray(), passphrase);
			methods.append("private key, ");
		}
		if(!authenticated && dsaPrivateKey != null) {
			CharArrayWriter appendTo = new CharArrayWriter();
			CryptUtils.writePEMKey("DSA PRIVATE KEY", dsaPrivateKey.getBytes(), appendTo);
			authenticated = connection.authenticateWithPublicKey(username, appendTo.toCharArray(), passphrase);
			methods.append("dsa private key, ");
		}
		if(!authenticated && rsaPrivateKey != null) {
			CharArrayWriter appendTo = new CharArrayWriter();
			CryptUtils.writePEMKey("RSA PRIVATE KEY", rsaPrivateKey.getBytes(), appendTo);
			authenticated = connection.authenticateWithPublicKey(username, appendTo.toCharArray(), passphrase);
			methods.append("rsa private key, ");
		}
		if(!authenticated && password != null) {
			authenticated = connection.authenticateWithPassword(username, password);
			methods.append("password, ");
		}
		if(!authenticated) {
			connection.close();
			throw new IOException("Could not log into " + connection.getHostname() + ":" + connection.getPort() + " using " + methods.substring(0, methods.length() - 2));
		}
		return connection;
	}

	protected class SftpRemoteFileUpdate implements RemoteFileUpdate {
		protected final String tmpFilePath;
		protected final String filePath;
		protected final SFTPv3Client sftp;

		public SftpRemoteFileUpdate(SFTPv3Client sftp, String tmpFilePath, String filePath) {
			this.tmpFilePath = tmpFilePath;
			this.filePath = filePath;
			this.sftp = sftp;
		}

		@Override
		public WritableByteChannel getWritableByteChannel() throws IOException {
			return new SftpWritableByteChannel(sftp, tmpFilePath);
		}

		@Override
		public boolean commitChanges() throws IOException {
			sftp.rm(filePath);
			sftp.mv(tmpFilePath, filePath);
			return true;
		}
	}

	@Override
	public RemoteFileUpdate getRemoteFileUpdate(String filename) throws IOException {
		SFTPv3Client sftp;
		try {
			sftp = new SFTPv3Client(getRemoteConnection()); // if this fails, reset connection
		} catch(IOException e) {
			initializer.reset();
			sftp = new SFTPv3Client(getRemoteConnection());
		}
		StringBuilder tmpFileName = new StringBuilder();
		String t = getTmpPrefix();
		if(t != null)
			tmpFileName.append(t);
		tmpFileName.append(filename);
		t = getTmpSuffix();
		if(t != null)
			tmpFileName.append(t);
		String tmpFilePath = IOUtils.getFullPath(getConfigDir(), tmpFileName.toString());
		String filePath = IOUtils.getFullPath(getConfigDir(), filename);

		return new SftpRemoteFileUpdate(sftp, tmpFilePath, filePath);
	}

	public String getConfigDir() {
		return configDir;
	}

	public void setConfigDir(String remoteCfgDir) {
		this.configDir = remoteCfgDir;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String remoteCfgHost) {
		this.host = remoteCfgHost;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int remoteCfgPort) {
		this.port = remoteCfgPort;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getKexTimeout() {
		return kexTimeout;
	}

	public void setKexTimeout(int kexTimeout) {
		this.kexTimeout = kexTimeout;
	}

	public String getKnownHostsFile() {
		if(hostKeyVerification instanceof KnownHostsVerifier)
			return ((KnownHostsVerifier) hostKeyVerification).getKnownHostsFile().getPath();
		return null;
	}

	public void setKnownHostsFile(String knownHostsFile) throws IOException {
		hostKeyVerification = new KnownHostsVerifier(new File(knownHostsFile));
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getPassphrase() {
		return passphrase;
	}

	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}

	public String getDsaPrivateKey() {
		return dsaPrivateKey;
	}

	public void setDsaPrivateKey(String dsaPrivateKey) {
		this.dsaPrivateKey = dsaPrivateKey;
	}

	public String getRsaPrivateKey() {
		return rsaPrivateKey;
	}

	public void setRsaPrivateKey(String rsaPrivateKey) {
		this.rsaPrivateKey = rsaPrivateKey;
	}

	public String getTmpPrefix() {
		return tmpPrefix;
	}

	public void setTmpPrefix(String tmpPrefix) {
		this.tmpPrefix = tmpPrefix;
	}

	public String getTmpSuffix() {
		return tmpSuffix;
	}

	public void setTmpSuffix(String tmpSuffix) {
		this.tmpSuffix = tmpSuffix;
	}
}
