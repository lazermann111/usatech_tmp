package simple.monitor;

import java.io.IOException;
import java.io.Writer;
import java.net.InetAddress;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.InstanceNotFoundException;
import javax.management.JMException;
import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import simple.bean.ConvertUtils;
import simple.io.BlockingWriter;
import simple.io.ByteChannelWriter;
import simple.io.ByteChecker;
import simple.io.CRCByteChecker;
import simple.io.CheckingWritableByteChannel;
import simple.io.Log;
import simple.mq.peer.PeerQueueMXBean;
import simple.mq.peer.PollType;
import simple.results.Results;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.text.ThreadSafeDateFormat;

public class AppMonitor {
	private static Log log = Log.getLog();
	protected static final String[] SERVICE_CHECK_INFO_ATTRIBUTE_NAMES = new String[] { "queueKey", "numThreads" };
	protected static final DateFormat OUTPUT_DATE_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"));
	protected static Charset serviceDefinitionCharset = Charset.forName("UTF-8");

	protected final Set<String> cfgFiles = new HashSet<String>();
	protected ByteChecker<?> byteChecker;
	protected BlockingWriter externalCommandWriter;
	protected String serviceDefinitionTemplate = "app-monitor-service";
	protected OutputFormat outputFormat = OutputFormat.NAGIOS_LONG_PERF;
	protected int serviceRefreshFrequency = 10;
	protected boolean reloadOnDefinitionChange = true;
	protected final AtomicBoolean reloadPending = new AtomicBoolean();
	protected ScheduledExecutorService scheduledExecutorService;

	protected interface Definition {
		public void startChanges() throws IOException;

		public void addService(String name, String template, String host, String desc) throws IOException;

		public void addHost(InetAddress host, String template) throws IOException;

		public void addHost(String hostName, String template, String hostAlias, String hostAddress) throws IOException;

		public void commitChanges() throws IOException;

		public void abandonChanges() throws IOException;
	}

	protected abstract class DefinitionFile {
		protected final String definitionFileName;
		protected long length;
		protected Object checkValue;
		protected CheckingWritableByteChannel cwbc;
		protected Writer writer;

		public DefinitionFile(String definitionFileName) {
			this.definitionFileName = definitionFileName;
			cfgFiles.add(definitionFileName);
		}

		public Writer newFileWriter() throws IOException {
			reset();
			cwbc = new CheckingWritableByteChannel(getWritableByteChannel(), getByteChecker());
			writer = new ByteChannelWriter(cwbc, serviceDefinitionCharset.newEncoder());
			return writer;
		}

		protected abstract WritableByteChannel getWritableByteChannel() throws IOException;

		protected abstract boolean update() throws IOException;

		public boolean updateIfChanged() throws IOException {
			if(cwbc == null)
				throw new IllegalStateException("newFileWriter() must first be called");
			long newLength = cwbc.getCount();
			Object newCheckValue = cwbc.getCheckValue();
			writer.close(); // release underlying stream
			if(newLength == length && ConvertUtils.areEqual(newCheckValue, checkValue))
				return false; // no change
			return update();
		}

		public String getFileName() {
			return definitionFileName;
		}

		public void reset() {
			if(cwbc != null) {
				try {
					cwbc.close();
				} catch(IOException e) {
					// ignore
				}
				cwbc = null;
			}
			if(writer != null) {
				try {
					writer.close();
				} catch(IOException e) {
					// ignore
				}
				writer = null;
			}
		}
	}

	protected class FileBasedDefinition implements Definition {
		protected final DefinitionFile definitionFile;
		protected Writer output;

		public FileBasedDefinition(DefinitionFile definitionFile) {
			this.definitionFile = definitionFile;
		}

		@Override
		public void startChanges() throws IOException {
			this.output = definitionFile.newFileWriter();
		}
		@Override
		public void addService(String name, String template, String hostName, String desc) throws IOException {
			output.write("define service {\n");
			if(!StringUtils.isBlank(template))
				writeDirective("use", template, output);
			writeDirective("host_name", hostName, output);
			writeDirective("service_description", name, output);
			writeDirective("display_name", desc, output); // Could update this
			output.write("}\n\n");
		}

		public void addHost(InetAddress host, String template) throws IOException {
			addHost(host.getHostName(), template, host.getCanonicalHostName(), host.getHostAddress());
		}

		public void addHost(String hostName, String template, String hostAlias, String hostAddress) throws IOException {
			output.write("define host{\n");
			if(!StringUtils.isBlank(template))
				writeDirective("use", template, output);
			writeDirective("host_name", hostName, output);
			writeDirective("alias", hostAlias, output);
			writeDirective("address", hostAddress, output);
			output.write("}\n\n");
		}
		@Override
		public void commitChanges() throws IOException {
			try {
				output.flush();
				output.close();
			} finally {
				definitionFile.updateIfChanged();
			}
		}

		public void abandonChanges() throws IOException {
			try {
				output.flush();
				output.close();
			} finally {
				definitionFile.reset();
			}
		}
	}
	protected class ServiceCheck {
		protected final ReentrantLock lock = new ReentrantLock();
		protected String serviceName;
		protected String queueName;
		protected PollType queuePollType;
		protected long queuePollFrequency;
		protected int numThreads;
		protected long lastCheckTime;
		protected final AppCheck appCheck;
		protected MBeanServerConnection mbsc;
		protected ObjectName objectName;
		protected PeerQueueMXBean queueProxy;
		protected final Map<String, PerfCheck<? extends Number>> perfChecks = new LinkedHashMap<String, PerfCheck<? extends Number>>();
		protected String[] perfAttributes;
		protected String uniqueServiceDecription;

		public ServiceCheck(AppCheck appCheck) {
			this.appCheck = appCheck;
		}

		public void doCheck() throws InstanceNotFoundException, ReflectionException, IOException {
			lock.lock();
			try {
				if(log.isDebugEnabled())
					log.debug("Checking " + uniqueServiceDecription + " on " + appCheck.toString());
				if(perfAttributes == null) {
					Set<String> attrs = new HashSet<String>();
					for(PerfCheck<?> pc : perfChecks.values()) {
						attrs.addAll(pc.getAttributeNames());
					}
					perfAttributes = attrs.toArray(new String[attrs.size()]);
				}
				Map<String, Number> values = new HashMap<String, Number>();
				AttributeList al = mbsc.getAttributes(objectName, perfAttributes);
				lastCheckTime = System.currentTimeMillis();
				for(Attribute a : al.asList())
					values.put(a.getName(), ConvertUtils.convertSafely(Number.class, a.getValue(), null));

				CheckResultState state = CheckResultState.OK;
				List<PerfCheckResult> results = new ArrayList<PerfCheckResult>(perfChecks.size());
				for(Map.Entry<String, PerfCheck<? extends Number>> entry : perfChecks.entrySet()) {
					PerfCheck<Number> pc = (PerfCheck<Number>) entry.getValue();
					Number value = pc.getValue(values);
					CheckResultState localState;
					if(pc.getCritRange() != null && pc.getCritRange().isIn(value))
						localState = CheckResultState.CRITICAL;
					else if(pc.getWarnRange() != null && pc.getWarnRange().isIn(value))
						localState = CheckResultState.WARNING;
					else if(pc.getOkayRange() != null && pc.getOkayRange().isIn(value))
						localState = CheckResultState.OK;
					else
						localState = CheckResultState.UNKNOWN;
					results.add(new PerfCheckResult(entry.getKey(), localState, value, pc));
					state = state.and(localState);
				}
				handleServiceCheckResult(lastCheckTime, appCheck, uniqueServiceDecription, state, results, null);
			} finally {
				lock.unlock();
			}
		}

		public String getQueueName() {
			return queueName;
		}

		public int getNumThreads() {
			return numThreads;
		}

		public void updateProxies(ObjectName objectName, MBeanServerConnection mbsc) throws IOException, JMException {
			AttributeList al = mbsc.getAttributes(objectName, SERVICE_CHECK_INFO_ATTRIBUTE_NAMES);
			String queueName = null;
			int numThreads = 0;
			for(Attribute a : al.asList()) {
				if("queueKey".equals(a.getName()))
					queueName = ConvertUtils.getStringSafely(a.getValue(), null);
				else if("numThreads".equals(a.getName()))
					numThreads = ConvertUtils.getIntSafely(a.getValue(), 0);
			}
			long queuePollFrequency;
			PollType queuePollType;
			PeerQueueMXBean queueProxy;
			if(!StringUtils.isBlank(queueName)) {
				queueProxy = JMX.newMBeanProxy(mbsc, new ObjectName("simple.mq.peer:Type=PeerSupervisor,Queue=" + queueName), PeerQueueMXBean.class, false);
				if(queueProxy != null) {
					queuePollFrequency = queueProxy.getDataPollFrequency();
					queuePollType = queueProxy.getPollType();
				} else {
					queuePollFrequency = 0;
					queuePollType = null;
				}
			} else {
				queuePollFrequency = 0;
				queuePollType = null;
				queueProxy = null;
			}
			lock.lock();
			try {
				this.queueName = queueName;
				this.numThreads = numThreads;
				this.objectName = objectName;
				this.serviceName = objectName.getKeyProperty("ServiceName");
				this.mbsc = mbsc;
				this.queueProxy = queueProxy;
				this.queuePollFrequency = queuePollFrequency;
				this.queuePollType = queuePollType;
				// calc perfchecks
				double limit;
				double idle;
				if(queuePollType == null) {
					limit = Double.POSITIVE_INFINITY;
					idle = 0.0;
				} else {
					switch(queuePollType) {
						case DIRECT:
						case HYBRID:
						case HYBRID_AND_DIRECT:
							limit = 1500;
							idle = 0.25;
							break;
						default:
							limit = 2 * queuePollFrequency;
							idle = 0.10;
					}
				}
				perfChecks.clear();
				perfChecks.put("Iterations", SinglePerfCheck.getInstance("TotalIterations", 0L, Long.MAX_VALUE, Long.MAX_VALUE, Long.MAX_VALUE, UOM.COUNTER));
				perfChecks.put("Average Processing Time", SinglePerfCheck.getInstance("AvgProcessingTime", 0.0, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, UOM.MILLISECONDS));
				perfChecks.put("Average Latency Time", SinglePerfCheck.getInstance("AvgLatencyTime", 0.0, limit, limit * 2, Long.MAX_VALUE, UOM.MILLISECONDS));
				perfChecks.put("Average Other Time", SinglePerfCheck.getInstance("AvgOtherTime", 0.0, 200, 400, Double.POSITIVE_INFINITY, UOM.MILLISECONDS));
				perfChecks.put("Percent Idle", SinglePerfCheck.getInstance("PercentIdle", 1.0, idle, idle / 2, 0.0, UOM.PERCENT));
				perfAttributes = null;
				uniqueServiceDecription = getUniqueServiceDecription(this);
			} finally {
				lock.unlock();
			}
		}

		public PollType getQueuePollType() {
			return queuePollType;
		}

		public String getServiceName() {
			return serviceName;
		}

		public Map<String, PerfCheck<? extends Number>> getPerfChecks() {
			return perfChecks;
		}

		public AppCheck getAppCheck() {
			return appCheck;
		}
	}

	protected abstract class AppCheck implements Runnable {
		protected final ReentrantLock lock = new ReentrantLock();
		protected final Map<ObjectName, ServiceCheck> serviceChecks = new LinkedHashMap<ObjectName, ServiceCheck>();
		protected MBeanServerConnection mbsc;
		protected final AtomicInteger updateNeed = new AtomicInteger(0);
		protected ScheduledFuture<?> scheduled;
		protected long interval;
		protected final Definition appDefinition;
		protected final String desc;
		protected final String appHost;
		protected final String appName;

		public AppCheck(Definition appDefinition, String appName, String appHost, String desc) {
			super();
			this.appDefinition = appDefinition;
			this.appName = appName;
			this.appHost = appHost;
			this.desc = desc;
		}

		public void scheduleChecking(long interval) {
			lock.lock();
			try {
				if(scheduled != null && this.interval == interval)
					return;
				if(scheduled != null) {
					scheduled.cancel(false);
					scheduled = null;
				}
				scheduled = getScheduledExecutorService().scheduleAtFixedRate(this, 0, interval, TimeUnit.MILLISECONDS);
				this.interval = interval;
			} finally {
				lock.unlock();
			}
			if(log.isInfoEnabled())
				log.info("Scheduled checking " + toString() + " every " + interval + " ms");
		}

		public void cancelChecking() {
			lock.lock();
			try {
				if(this.scheduled != null) {
					scheduled.cancel(false);
					scheduled = null;
				}
			} finally {
				lock.unlock();
			}
		}

		public void run() {
			try {
				checkServices(null);
			} catch(IOException e) {
				log.warn("Could not check stats for " + toString(), e);
				checkConnection();
			} catch(Throwable e) {
				log.error("Unexpected error while checking stats for " + toString(), e);
			}
		}

		protected abstract void checkConnection();

		protected abstract MBeanServerConnection getMBeanServerConnection() throws IOException;

		public String getAppHost() {
			return appHost;
		}

		protected abstract void closeConnection();

		public void updateServices() throws IOException {
			if(log.isInfoEnabled())
				log.info("Updating services for " + toString());
			boolean okay = false;
			lock.lock();
			try {
				MBeanServerConnection mbsc = getMBeanServerConnection();
				Set<ObjectName> ons = mbsc.queryNames(new ObjectName("simple.app.jmx:Type=ServiceManagerWithConfig,ServiceName=*"), null);
				appDefinition.startChanges();
				try {
					for(ObjectName on : ons) {
						ServiceCheck sc = serviceChecks.get(on);
						if(sc == null) {
							sc = new ServiceCheck(this);
							sc.updateProxies(on, mbsc);
							serviceChecks.put(on, sc);
						} else if(mbsc != this.mbsc) {
							sc.updateProxies(on, mbsc);
						}
						if(log.isInfoEnabled())
							log.info("Setting up check for " + toString());
						String name = getUniqueServiceDecription(sc);
						appDefinition.addService(name, getServiceDefinitionTemplate(), getAppHost(), sc.getServiceName() + " on " + desc);
					}
					this.mbsc = mbsc;
					serviceChecks.keySet().retainAll(ons);
					okay = true;
				} finally {
					if(okay) {
						okay = false;
						appDefinition.commitChanges();
						okay = true;
					} else
						appDefinition.abandonChanges();
				}
			} catch(MalformedObjectNameException e) {
				throw new IOException(e);
			} catch(JMException e) {
				throw new IOException(e);
			} finally {
				if(okay) {
					while(true) {
						int u = updateNeed.get();
						if(u > 0 || updateNeed.compareAndSet(u, getServiceRefreshFrequency()))
							break;
					}
				} else
					updateNeed.set(0);
				lock.unlock();
			}
			if(log.isInfoEnabled())
				log.info("Monitoring " + serviceChecks.size() + " services for " + toString());
		}

		public void checkServices(Set<PollType> pollTypes) throws IOException {
			if(updateNeed.decrementAndGet() <= 0)
				updateServices();
			for(ServiceCheck sc : serviceChecks.values()) {
				if(pollTypes == null || pollTypes.contains(sc.getQueuePollType()))
					try {
						sc.doCheck();
					} catch(InstanceNotFoundException e) {
						// update services
						updateNeed.set(0);
					} catch(ReflectionException e) {
						log.warn("Could not read stats for " + sc.getServiceName(), e);
					}
			}
		}

		public void close() {
			lock.lock();
			try {
				closeConnection();
				mbsc = null;
				updateNeed.set(0);
			} finally {
				lock.unlock();
			}
		}

		public Collection<ServiceCheck> getServiceChecks() {
			return serviceChecks.values();
		}

		@Override
		public String toString() {
			return desc;
		}

		public String getAppName() {
			return appName;
		}
	}

	protected void reset() {
		cfgFiles.clear();
		reloadPending.set(false);
		
		// Reset simple properties to their default values
		serviceDefinitionTemplate = "app-monitor-service";
		outputFormat = OutputFormat.NAGIOS_LONG_PERF;
		serviceRefreshFrequency = 10;
		reloadOnDefinitionChange = true;
		reloadPending.set(false);
		
		log.info("Reset AppMonitor");
	}

	// nagios external cmd format:
	// [<timestamp>] PROCESS_SERVICE_CHECK_RESULT;<host_name>;<svc_description>;<return_code>;<plugin_output>
	// <plugin_output> = <text>|'label1'=value1[UOM];[warn];[crit];[min];[max] 'label2'=value2[UOM];[warn];[crit];[min];[max]
	/*
	timestamp is the time in time_t format (seconds since the UNIX epoch) that the service check was perfomed (or submitted). Please note the single space after the right bracket.
	host_name is the short name of the host associated with the service in the service definition
	svc_description is the description of the service as specified in the service definition
	return_code is the return code of the check (0=OK, 1=WARNING, 2=CRITICAL, 3=UNKNOWN)
	plugin_output is the text output of the service check (i.e. the plugin output)


	space separated list of label/value pairs
	label can contain any characters
	the single quotes for the label are optional. Required if spaces, = or ' are in the label
	label length is arbitrary, but ideally the first 19 characters are unique (due to a limitation in RRD). Be aware of a limitation in the amount of data that NRPE returns to Nagios
	to specify a quote character, use two single quotes
	warn, crit, min/ or max/ may be null (for example, if the threshold is not defined or min and max do not apply). Trailing unfilled semicolons can be dropped
	min and max are not required if UOM=%
	value, min and max in class [-0-9.]. Must all be the same UOM
	warn and crit are in the range format (see Section 2.5). Must be the same UOM
	UOM (unit of measurement) is one of:
	no unit specified - assume a number (int or float) of things (eg, users, processes, load averages)
	s - seconds (also us, ms)
	% - percentage
	B - bytes (also KB, MB, TB, GB?)
	c - a continous counter (such as bytes transmitted on an interface)


	Range definition

	Generate an alert if x...

	10	< 0 or > 10, (outside the range of {0 .. 10})
	10:	< 10, (outside {10 .. ∞})
	~:10	> 10, (outside the range of {-∞ .. 10})
	10:20	< 10 or > 20, (outside the range of {10 .. 20})
	@10:20	≥ 10 and ≤ 20, (inside the range of {10 .. 20})
	10	< 0 or > 10, (outside the range of {0 .. 10})

	 */
	protected void handleServiceCheckResult(long checkTime, AppCheck appCheck, String checkName, CheckResultState state, List<PerfCheckResult> perfCheckResults, String comments) {
		handleServiceCheckResult(checkTime, appCheck.getAppHost(), checkName, state, perfCheckResults, new PerfCheckResults(perfCheckResults), comments);
	}

	protected void handleServiceCheckResult(long checkTime, String host, String checkName, CheckResultState state, List<PerfCheckResult> perfCheckResults, Results textResults, String comments) {
		BlockingWriter output = getExternalCommandWriter();
		try {
			output.open();
		} catch(IOException e) {
			if(log.isWarnEnabled())
				log.error("Could not open ExternalCommandWriter", e);
			return;
		}
		try {
			getOutputFormat().writeServiceCheckResult(checkTime, host, checkName, state, OUTPUT_DATE_FORMAT, perfCheckResults, textResults, comments, output);
		} catch(IOException e) {
			if(log.isWarnEnabled())
				log.warn("Failed to write result for " + checkName + " on " + host + " as " + state + " with results: " + perfCheckResults, e);
			return;
		} finally {
			output.close();
		}
		if(log.isDebugEnabled()) {
			log.debug("Wrote result for " + checkName + " on " + host + " as " + state + " with results: " + perfCheckResults);
		} else if(log.isInfoEnabled())
			log.info("Wrote result for " + checkName + " on " + host + " as " + state);
	}

	protected void handleHostCheckResult(long checkTime, String host, CheckResultState state, List<PerfCheckResult> perfCheckResults, Results textResults, String comments) {
		BlockingWriter output = getExternalCommandWriter();
		try {
			output.open();
		} catch(IOException e) {
			if(log.isWarnEnabled())
				log.error("Could not open ExternalCommandWriter", e);
			return;
		}
		try {
			getOutputFormat().writeHostCheckResult(checkTime, host, state, OUTPUT_DATE_FORMAT, perfCheckResults, textResults, comments, output);
		} catch(IOException e) {
			if(log.isWarnEnabled())
				log.warn("Failed to write result for " + host + " as " + state + " with results: " + perfCheckResults, e);
			return;
		} finally {
			output.close();
		}
		if(log.isDebugEnabled()) {
			log.debug("Wrote result for " + host + " as " + state + " with results: " + perfCheckResults);
		} else if(log.isInfoEnabled())
			log.info("Wrote result for " + host + " as " + state);
	}

	protected static void writeDirective(String key, Object value, Writer output) throws IOException {
		output.write("        ");
		StringUtils.appendPadded(output, key, ' ', 30, Justification.LEFT);
		output.write(' ');
		if(value != null)
			output.write(value.toString());
		output.write('\n');
	}

	protected void reloadExternalIfNeeded() {
		if(reloadPending.compareAndSet(true, false)) {
			try {
				BlockingWriter output = getExternalCommandWriter();
				try {
					output.open();
				} catch(IOException e) {
					if(log.isWarnEnabled())
						log.error("Could not open ExternalCommandWriter", e);
					return;
				}
				try {
					output.write('[');
					output.write(String.valueOf(System.currentTimeMillis() / 1000));
					output.write("] RESTART_PROGRAM\n");
					output.flush();
				} finally {
					output.close();
				}
				if(log.isInfoEnabled())
					log.info("Wrote restart command to external command file");
			} catch(IOException e) {
				log.warn("Could not write restart command to external command file", e);
			}
		}
	}
	protected String getUniqueServiceDecription(ServiceCheck serviceCheck) {
		return serviceCheck.getAppCheck().getAppName() + '_' + serviceCheck.getServiceName() + "_perf"; // TODO: ensure uniqueness

	}

	public String getServiceDefinitionTemplate() {
		return serviceDefinitionTemplate;
	}

	public void setServiceDefinitionTemplate(String serviceDefinitionTemplate) {
		this.serviceDefinitionTemplate = serviceDefinitionTemplate;
	}

	public OutputFormat getOutputFormat() {
		return outputFormat;
	}

	public void setOutputFormat(OutputFormat outputFormat) {
		this.outputFormat = outputFormat;
	}

	public int getServiceRefreshFrequency() {
		return serviceRefreshFrequency;
	}

	public void setServiceRefreshFrequency(int serviceRefreshFrequency) {
		this.serviceRefreshFrequency = serviceRefreshFrequency;
	}

	public boolean isReloadOnDefinitionChange() {
		return reloadOnDefinitionChange;
	}

	public void setReloadOnDefinitionChange(boolean reloadOnDefinitionChange) {
		this.reloadOnDefinitionChange = reloadOnDefinitionChange;
	}

	public ByteChecker<?> getByteChecker() {
		if(byteChecker == null)
			setByteChecker(new CRCByteChecker());
		return byteChecker;
	}

	public void setByteChecker(ByteChecker<?> byteChecker) {
		this.byteChecker = byteChecker;
	}

	public BlockingWriter getExternalCommandWriter() {
		return externalCommandWriter;
	}

	public void setExternalCommandWriter(BlockingWriter externalCommandWriter) {
		this.externalCommandWriter = externalCommandWriter;
	}

	public ScheduledExecutorService getScheduledExecutorService() {
		return scheduledExecutorService;
	}

	public void setScheduledExecutorService(ScheduledExecutorService scheduledExecutorService) {
		this.scheduledExecutorService = scheduledExecutorService;
	}
}
