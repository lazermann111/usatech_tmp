package simple.monitor;

import java.io.IOException;
import java.io.Writer;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.TimeUnit;

import javax.management.MBeanServerConnection;

import simple.io.BlockingWriter;
import simple.io.HeapBufferReaderWriter;
import simple.io.Log;
import simple.monitor.RemoteFileUpdater.RemoteFileUpdate;

public class LocalAppMonitor extends AppMonitor {
	private static Log log = Log.getLog();
	protected RemoteFileUpdater remoteFileUpdater;
	protected long serviceCheckInterval = 60000;

	protected class SocketBasedServiceDefinition implements Definition {
		protected final HeapBufferReaderWriter buffer = new HeapBufferReaderWriter(true);
		@Override
		public void startChanges() throws IOException {
			buffer.clear();
		}

		@Override
		public void addService(String name, String template, String host, String desc) throws IOException {
			Writer output = buffer.getWriter();
			output.write('[');
			output.write(String.valueOf(System.currentTimeMillis() / 1000));
			output.write("] UPDATE_SERVICE_CHECK_CONFIG;name=");
			OutputFormat.writeNagiosEscaped(name, output);
			output.write(";template=");
			OutputFormat.writeNagiosEscaped(template, output);
			output.write(";host=");
			OutputFormat.writeNagiosEscaped(host, output);
			output.write(";desc=");
			OutputFormat.writeNagiosEscaped(desc, output);
			output.write(";\n");
			output.flush();
		}

		@Override
		public void addHost(InetAddress host, String template) throws IOException {
			addHost(host.getHostName(), template, host.getCanonicalHostName(), host.getHostAddress());
		}

		@Override
		public void addHost(String hostName, String template, String hostAlias, String hostAddress) throws IOException {
			Writer output = buffer.getWriter();
			output.write('[');
			output.write(String.valueOf(System.currentTimeMillis() / 1000));
			output.write("] UPDATE_HOST_CHECK_CONFIG;name=");
			OutputFormat.writeNagiosEscaped(hostName, output);
			output.write(";template=");
			OutputFormat.writeNagiosEscaped(template, output);
			output.write(";alias=");
			OutputFormat.writeNagiosEscaped(hostAlias, output);
			output.write(";address=");
			OutputFormat.writeNagiosEscaped(hostAddress, output);
			output.write(";\n");
			output.flush();
		}

		@Override
		public void commitChanges() throws IOException {
			BlockingWriter output = getExternalCommandWriter();
			try {
				output.open();
			} catch(IOException e) {
				if(log.isWarnEnabled())
					log.error("Could not open ExternalCommandWriter", e);
				return;
			}
			try {
				buffer.copyInto(output);
			} catch(IOException e) {
				if(log.isWarnEnabled())
					log.warn("Failed to write config to " + output, e);
				return;
			} finally {
				output.close();
			}
			if(log.isInfoEnabled())
				log.info("Wrote config to" + output);
		}

		@Override
		public void abandonChanges() throws IOException {
			buffer.clear();
		}
	}
	protected class LocalAppCheck extends AppCheck {
		public LocalAppCheck(String appHost, String appName, String instance) {
			super(new SocketBasedServiceDefinition(), appName, appHost, appName + (instance == null ? "" : "_" + instance) + " on " + appHost);
		}

		protected void checkConnection() {
		}

		@Override
		protected void closeConnection() {
		}

		@Override
		protected MBeanServerConnection getMBeanServerConnection() throws IOException {
			return ManagementFactory.getPlatformMBeanServer();
		}
	}

	protected class RemoteServiceDefinitionFile extends DefinitionFile {
		protected final RemoteFileUpdate remoteFileUpdate;

		public RemoteServiceDefinitionFile(String definitionFileName) throws IOException {
			super(definitionFileName);
			remoteFileUpdate = getRemoteFileUpdater().getRemoteFileUpdate(definitionFileName);
		}

		@Override
		protected WritableByteChannel getWritableByteChannel() throws IOException {
			return remoteFileUpdate.getWritableByteChannel();
		}

		@Override
		protected boolean update() throws IOException {
			return remoteFileUpdate.commitChanges();
		}
	}

	/**
	 * @throws IOException
	 */
	public void setup(String appInstance) throws IOException {
		String hostname;
		try {
			hostname = InetAddress.getLocalHost().getCanonicalHostName();
		} catch(UnknownHostException e) {
			log.warn("Could not get host name", e);
			hostname = System.getProperty("app.hostname", "UNKNOWN");
		}
		String appname = System.getProperty("app.servicename", "UNKNOWN");

		LocalAppCheck appCheck = new LocalAppCheck(hostname, appname, appInstance);
		appCheck.scheduleChecking(getServiceCheckInterval());
	}

	public void scheduleSetup(final String appInstance) {
		Runnable setupRunnable = new Runnable() {
			@Override
			public void run() {
				try {
					setup(appInstance);
				} catch(IOException e) {
					long delay = getServiceCheckInterval() * getServiceRefreshFrequency();
					log.warn("Could not setup app check; re-scheduling for " + delay + " ms later", e);
					getScheduledExecutorService().schedule(this, delay, TimeUnit.MILLISECONDS);
				}
			}
		};
		getScheduledExecutorService().schedule(setupRunnable, getServiceCheckInterval(), TimeUnit.MILLISECONDS);
	}

	public RemoteFileUpdater getRemoteFileUpdater() {
		return remoteFileUpdater;
	}

	public void setRemoteFileUpdater(RemoteFileUpdater remoteFileUpdater) {
		this.remoteFileUpdater = remoteFileUpdater;
	}

	public long getServiceCheckInterval() {
		return serviceCheckInterval;
	}

	public void setServiceCheckInterval(long serviceCheckInterval) {
		this.serviceCheckInterval = serviceCheckInterval;
	}
}
