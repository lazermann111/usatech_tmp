/*
 * NullFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;

import java.text.FieldPosition;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParsePosition;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;

/**
 * Uses the specified default when value is null.
 * 
 * @author Brian S. Krug
 */
public class WithDefaultNumberFormat extends NumberFormat {
	private static final long serialVersionUID = -5761562732495356587L;
	protected NumberFormat delegate;
	protected Number defaultValue;
	protected String defaultValueText;
    
	public WithDefaultNumberFormat(NumberFormat delegate, Number defaultValue) {
		this.delegate = delegate;
		this.defaultValue = defaultValue;
		updateDefaultValueText();
    }
    
	@Override
	public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
		if(delegate == null)
			return toAppendTo.append(number);
		return delegate.format(number, toAppendTo, pos);
	}

	@Override
	public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
		if(delegate == null)
			return toAppendTo.append(number);
		return delegate.format(number, toAppendTo, pos);
	}

	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		String s;
		if(obj == null || (s = ConvertUtils.getStringSafely(obj, "")).isEmpty()) {
			pos.setBeginIndex(toAppendTo.length());
			pos.setEndIndex(toAppendTo.length() + defaultValueText.length());
			return toAppendTo.append(defaultValueText);
		}
		if(delegate == null) {
			pos.setBeginIndex(toAppendTo.length());
			pos.setEndIndex(toAppendTo.length() + s.length());
			return toAppendTo.append(s);
		}
		return delegate.format(obj, toAppendTo, pos);
    }
    
	public Number parse(String source, ParsePosition pos) {
		if(source.regionMatches(pos.getIndex(), defaultValueText, 0, defaultValueText.length())) {
			pos.setIndex(pos.getIndex() + defaultValueText.length());
			return null;
        }
		if(delegate == null) {
			try {
				Number val = ConvertUtils.convert(Number.class, source.substring(pos.getIndex()));
				pos.setIndex(source.length());
				return val;
			} catch(ConvertException e) {
				pos.setErrorIndex(pos.getIndex());
				return null;
			}
		}
		return delegate.parse(source, pos);
    }

	public Format getDelegate() {
		return delegate;
	}

	public void setDelegate(NumberFormat delegate) {
		this.delegate = delegate;
		updateDefaultValueText();
	}

	public Number getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Number defaultValue) {
		this.defaultValue = defaultValue;
		updateDefaultValueText();
	}    

	protected void updateDefaultValueText() {
		if(defaultValue == null)
			defaultValueText = "";
		else if(delegate == null)
			defaultValueText = defaultValue.toString();
		else
			defaultValueText = delegate.format(defaultValue);
	}

	@Override
	public int hashCode() {
		return SystemUtils.addHashCodes(0, delegate, defaultValue);
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof WithDefaultNumberFormat))
			return false;
		WithDefaultNumberFormat other = (WithDefaultNumberFormat) obj;
		return ConvertUtils.areEqual(delegate, other.delegate) && ConvertUtils.areEqual(defaultValue, other.defaultValue);
	}
}
