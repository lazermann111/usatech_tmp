/*
 * WordNumberFormat.java
 *
 * Created on July 9, 2002, 12:00 PM
 */

package simple.text;

import java.math.BigDecimal;
import java.math.BigInteger;

import simple.util.EnhancedStringTokenizer;

/**
 *
 * @author  bkrug
 * @version
 */
public class WordNumberFormat extends java.text.NumberFormat {
	private static final long serialVersionUID = 8104790426453329791L;
	protected String[] units = new String[] {"zero","one", "two", "three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen", "fifteen","sixteen","seventeen","eightteen","nineteen"};
	protected String[] tens = new String[] {"","ten","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety","hundred"};
	protected String[] pow1000 = new String[] {"","thousand","million","billion","trillion","quadrillion"};
	protected String[] decimals = new String[] {null,"tenth","hundredth","thousandth",null,null,"millionth",null,null,"billionth",null,null,"trillionth",null,null,"quadrillionth"};
	protected static final BigInteger ten = new BigInteger("10");
	protected boolean isCurrency = false;
	protected BigDecimal mult = null;
	protected String[] currencyParts = new String[] {"dollar",null,"cent"};
	protected static final int ZERO = 0;
	protected static final int UNIT = 1;//1-9
	protected static final int TENS = 10;//20-90
	protected static final int TEENS = 11;//10-19
	protected static final int HUNDRED = 100;
	protected static final int POW1000 = 1000;
	protected static final int DECIMALS = -2;
	protected static final int NOTHING = -1;
	protected static final int CURRENCY = -5;
	
	/** Holds value of property maximumFractionDigits. */
	private int maximumFractionDigits;
	
	/** Holds value of property minimumFractionDigits. */
	private int minimumFractionDigits;
	
        protected WordNumberFormat() {
	}
	public static WordNumberFormat getWordNumberInstance() {
		WordNumberFormat wnf = new WordNumberFormat();
		return wnf;
	}
	public static WordNumberFormat getWordCurrencyInstance() {
		WordNumberFormat wnf = new WordNumberFormat();
		wnf.isCurrency = true;
		wnf.setMaximumFractionDigits(2);
		return wnf;
	}
	public static WordNumberFormat getWordPercentInstance() {
		WordNumberFormat wnf = new WordNumberFormat();
		wnf.mult = new BigDecimal("100");
		return wnf;
	}
	public java.lang.StringBuffer format(double number, java.lang.StringBuffer results, java.text.FieldPosition pos) {
		return format(new BigDecimal(number),results,pos);
	}
	
	public java.lang.Number parse(java.lang.String str, java.text.ParsePosition pos) {
		EnhancedStringTokenizer token = new EnhancedStringTokenizer(str," -\t\n\r,");
		BigDecimal whole = new BigDecimal("0");
		BigDecimal dec = null;
		int n = 0;
		int last = NOTHING;
		int lastPow1000 = 0;
		int index;
		while(token.hasMoreTokens()) {
			String w = token.nextToken();
			pos.setIndex(token.getCurrentPosition());
			switch(last) {
				case DECIMALS:
					pos.setErrorIndex(token.getCurrentPosition()); //nothing allowed after decimals
					return whole;
				case HUNDRED:
				case POW1000:
				case NOTHING:
				case TENS:
					index = findWord(w,units);//units
					if(index > -1) {
						n = n + index;
						if(index == 0) {
							last = ZERO;
						} else if(index > 9) {
							last = TEENS;
						} else {
							last = UNIT;
						}
						break;
					} else if(last != TENS) {
						index = findWord(w,tens); //tens
						if(index == 10) {
							if(last != UNIT) {
								pos.setErrorIndex(token.getCurrentPosition());
								return whole;
							}	
							n = n * 100;
							last = HUNDRED;
							break;
						} else if(index > -1) {
							n = n + (10*index);
							last = TENS;
							break;
						} else if(last == NOTHING) {
							pos.setErrorIndex(token.getCurrentPosition());
							return whole;
						}
					}
				case TEENS:
				case UNIT:
					if(last == UNIT && tens[10].equalsIgnoreCase(w)) { //check for hundred
						n = n * 100;
						last = HUNDRED;
						break;
					}
					if(last != POW1000) {
						index = findWord(w,pow1000);
						if(index > -1) {//found, make sure its less
							if(lastPow1000 != 0 && index >=lastPow1000) { //Error
								pos.setErrorIndex(token.getCurrentPosition()); 
								return whole;
							}
							lastPow1000 = index;
							if(dec == null) whole = whole.add(new BigDecimal(ten.pow(index*3)).multiply(BigDecimal.valueOf(n)));
							else dec = dec.add(new BigDecimal(ten.pow(index*3)).multiply(BigDecimal.valueOf(n)));
							n = 0;
							last = POW1000;
							break;
						}
					}
				case ZERO:
				case CURRENCY:
					if(dec == null && isCurrency && w.equalsIgnoreCase(currencyParts[0]) && last != NOTHING && last != CURRENCY) {
						last = CURRENCY;
						break;
					} else if(dec == null && w.equalsIgnoreCase("AND") && last != NOTHING) {
						dec = BigDecimal.valueOf(0);
						lastPow1000 = 0;
						last = NOTHING;
						whole = whole.add(BigDecimal.valueOf(n));
						n = 0;
						break;
					}
					if(n != 1 && (w.endsWith("S") || w.endsWith("s"))) w = w.substring(0,w.length()-1);
					index = findWord(w,(isCurrency ? currencyParts : decimals));
					if(index > -1) {
						if(dec == null) dec = BigDecimal.valueOf(0);
						dec = dec.add(BigDecimal.valueOf(n)).movePointLeft(index);
						last = DECIMALS;
						break;
					}
				default:
					pos.setErrorIndex(token.getCurrentPosition()); //nothing allowed after decimals
					return whole;
			}			
		}
		pos.setIndex(str.length());
		if(dec == null) {
			return whole.add(BigDecimal.valueOf(n));
		} else {
			if(last != DECIMALS) {
				pos.setIndex(str.length()-1);
				pos.setErrorIndex(str.length()-1);
			}
			return whole.add(dec);
		}
	}
	
	protected int findWord(String word, String[] values) {
		for(int i = 0; i < values.length; i++) if(values[i] != null && word.equalsIgnoreCase(values[i])) return i;
		return -1;		
	}
	
	public java.lang.StringBuffer format(long number, java.lang.StringBuffer results, java.text.FieldPosition pos) {
		return format(BigDecimal.valueOf(number),results,pos);
	}
	
	public java.lang.StringBuffer format(BigDecimal number, java.lang.StringBuffer results, java.text.FieldPosition pos) {
		if(mult != null) number = number.multiply(mult);
		String[] ss = (isCurrency ? currencyParts : decimals);
		int d = Math.min(maximumFractionDigits, ss.length - 1);
		if(number.scale() > d) number = number.setScale(d,number.ROUND_HALF_UP);
		d = getSignificantDigits(number);
		if(d < minimumFractionDigits) d = minimumFractionDigits;
		formatInteger(number.toBigInteger(),results);
		if(ss[0] != null) {
			results.append(" ");
			results.append(ss[0]);
			if(number.longValue() != 1) results.append("s");
		}
		if(d > 0) {
			while(ss[d] == null) d++;
			number = number.subtract(number.setScale(0,number.ROUND_FLOOR));
			number = number.movePointRight(d);
			results.append(" and ");
			formatInteger(number.toBigInteger(),results);
			results.append(" ");
			results.append(ss[d]);
			if(number.longValue() != 1) results.append("s");
		}
		return results;
	}
	
	protected void formatInteger(long number, StringBuffer sb) {
		//do power of 1000 first
		int p  = ((int)Math.log(number))/3;
		boolean first = true;
		for(; p >= 0; p--) {
			int n = (int)(number %((int)Math.pow(10,3*(p+1))))/ ((int)Math.pow(10,3*p));
			if(n == 0) continue;
			if(first) first = false;
			else sb.append(" ");
			formatUnder1000(n, sb);
			if(p > 0) {
				sb.append(" ");
				sb.append(pow1000[p]);
			}
		}
	}
	protected void formatInteger(BigInteger number, StringBuffer sb) {
		//do power of 1000 first
		int p  =  thousands(number);
		boolean first = true;
		for(; p >= 0; p--) {
			int n = number.mod(ten.pow(3*(p+1))).divide(ten.pow(3*p)).intValue();
			if(n == 0) continue;
			if(first) first = false;
			else sb.append(" ");
			formatUnder1000(n, sb);
			if(p > 0) {
				sb.append(" ");
				sb.append(pow1000[p]);
			}
		}
	}
	
	protected static int thousands(BigInteger number) {
		return ((int)Math.log(number.doubleValue()))/3;
	}
	protected static int getSignificantDigits(BigDecimal number) {
		String s = number.toString();
		int p = s.indexOf(".");
		if(p == -1) return 0; //no decimal point
		s = s.substring(p);
		int i = s.length() - 1;
		while(s.charAt(i) == '0') i--;
		return i;
	}
	protected void formatUnder1000(int number, StringBuffer sb) {
		//how many hundreds
		int h = number / 100;
		number = number % 100;
		if(h > 0) {
			sb.append(units[h]);
			sb.append("-");
			sb.append(tens[10]);
			if(number > 0) sb.append(" ");
		}
		//tens
		if(number >= units.length) {
			int t = number / 10;
			sb.append(tens[t]);
			number = number % 10;
			if(number > 0) sb.append("-");
		}
		//units
		if(number > 0) {
			sb.append(units[number]);
		}
	}		
	
	/** Getter for property maximumFractionDigits.
	 * @return Value of property maximumFractionDigits.
	 */
	public int getMaximumFractionDigits() {
		return maximumFractionDigits;
	}
	
	/** Setter for property maximumFractionDigits.
	 * @param maximumFractionDigits New value of property maximumFractionDigits.
	 */
	public void setMaximumFractionDigits(int maximumFractionDigits) {
		this.maximumFractionDigits = maximumFractionDigits;
	}
	
	/** Getter for property minimumFractionDigits.
	 * @return Value of property minimumFractionDigits.
	 */
	public int getMinimumFractionDigits() {
		return minimumFractionDigits;
	}
	
	/** Setter for property minimumFractionDigits.
	 * @param minimumFractionDigits New value of property minimumFractionDigits.
	 */
	public void setMinimumFractionDigits(int minimumFractionDigits) {
		this.minimumFractionDigits = minimumFractionDigits;
	}
	
}
