/*
 * CentsFormat.java
 *
 * Created on December 27, 2001, 10:16 AM
 */

package simple.text;

/**
 *
 * Valid characters for the pattern are the same as for DecimalFormat except that 'V' is used instead of '.' because it indicates an inserted decimal point.
 * Thus a pattern of "$#0V00" given the number 345 would yield "$3.45".
 *
 *
 * @author  bkrug
 * @version
 */
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CentsFormat extends NumberFormat {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8481774999914L;
	protected DecimalFormat delegate = null;
	protected String pattern;
    protected int positiveDivider = 1;
    protected int negativeDivider = 1;
    public CentsFormat() { //this uses defaults of adjustScale = 2 and delegate is the Currency Format
		super();
		delegate = (DecimalFormat)java.text.NumberFormat.getCurrencyInstance();
		setPositiveDivider(100);
        setNegativeDivider(100);
        pattern = "$#,###V##";
	}

	public CentsFormat(DecimalFormat decimalFormat, int divider) {
		super();
		this.delegate = decimalFormat;
		setPositiveDivider(divider);
		setNegativeDivider(divider);
		String pattern = decimalFormat.toPattern();
		/*
		String[] parts = StringUtils.splitQuotesAnywhere(pattern, ';', '\'', false);
		for(int i = 0; i < parts.length; i++) {
			int p = 
		}*/
		this.pattern = pattern + "/" + divider;
	}
	/** Creates new CentsFormat */
	public CentsFormat(String pattern) {
		super();
		applyPattern(pattern);
		this.pattern = pattern;
	}
	
	protected void applyPattern(String pattern) {
        int[] divs = new int[5];
        int part = 0;
        for(int i = 0; i < pattern.length(); i++) {
			switch(pattern.charAt(i)) {
                case ';'://subpattern divider
                    part++;
                    break;
                case '\''://skip to beginning of quote
                    i = pattern.indexOf('\'', i);
                    break;
				case '#': case '0':
                    divs[part] *= 10;
					break;
				case 'V':
					pattern = pattern.substring(0,i) + '.' + pattern.substring(i+1);
					divs[part] = 1;
                    break;
			}
		}
		delegate = new DecimalFormat(pattern);
        setPositiveDivider(divs[0] == 0 ? 1 : divs[0]);
        setNegativeDivider(divs[1] == 0 ? 1 : divs[1]);       
	}
	
	public java.lang.StringBuffer format(double d, java.lang.StringBuffer toAppendTo, java.text.FieldPosition pos) {
        return delegate.format(d / getDivider(d), toAppendTo, pos);
	}
	
	public java.lang.Number parse(java.lang.String str, java.text.ParsePosition status) {
        Number n = delegate.parse(str, status);
        if(n instanceof Long) return new Long(n.longValue() * getDivider(n));
        else return new Double(n.doubleValue() * getDivider(n));
	}
	
	public java.lang.StringBuffer format(long l, java.lang.StringBuffer toAppendTo, java.text.FieldPosition pos) {
		return delegate.format(l / (double) getDivider(l), toAppendTo, pos);
	}
	
    protected int getDivider(Number n) {
        if(n.doubleValue() < 0.0) return getNegativeDivider();
        else return getPositiveDivider();       
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof CentsFormat 
            && getClass().equals(obj.getClass()) 
            && toPattern().equals(((CentsFormat)obj).toPattern());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return toPattern().hashCode();
    } 
        
    public String toPattern() {
        return pattern;
    }
    
    public String toString() {
        return getClass().getName() + ":" + toPattern();
    }

    public int getNegativeDivider() {
        return negativeDivider;
    }

    public void setNegativeDivider(int negativeDivider) {
        this.negativeDivider = negativeDivider;
    }

    public int getPositiveDivider() {
        return positiveDivider;
    }

    public void setPositiveDivider(int positiveDivider) {
        this.positiveDivider = positiveDivider;
    }
}
