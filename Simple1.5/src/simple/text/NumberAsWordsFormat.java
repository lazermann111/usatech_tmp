/*
 * Created on Jan 18, 2005
 *
 */
package simple.text;

import java.math.BigDecimal;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

/**
 * @author bkrug
 *
 */
public class NumberAsWordsFormat extends Format {
    /**
	 * 
	 */
	private static final long serialVersionUID = 876101971234L;
	private static String[] groupNames = new String[] { "thousand", "million", "billion", "trillion", "quadrillion", "quintillion" };
	private static String[] groupRanks = new String[] { "thousandth", "millionth", "billionth", "trillionth", "quadrillionth", "quintillionth" };
    private static String[] digitNames = {"one", "two", "three", "four","five", "six", "seven","eight","nine"};
    private static String[] digitRanks = {"first", "second", "third", "fourth", "fifth", "sixth", "seventh","eighth","ninth"};
    private static String[] teenNames = {"ten", "eleven", "twelve", "thirteen", "forteen", "fifteen", "sixteen","seventeen","eighteen","nineteen"};
    private static String[] teenRanks = {"tenth", "eleventh", "twelfth", "thirteenth", "forteenth", "fifteenth", "sixteenth","seventeenth","eighteenth","nineteenth"};
    private static String[] tensNames = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy","eighty","ninety"};
    private static String[] tensRanks = {"twentieth", "thirtieth", "fortieth", "fiftieth", "sixtieth", "seventieth","eightieth","nintieth"};
    private boolean rank;
    
    public NumberAsWordsFormat() {
        this(false);
    }
    
    public NumberAsWordsFormat(boolean rank) {
        this.rank = rank;  
    }
    /* (non-Javadoc)
     * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
     */
    public Object parseObject(String source, ParsePosition pos) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer, java.text.FieldPosition)
     */
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        BigDecimal num;
        try {
            num = ConvertUtils.convert(BigDecimal.class, obj);
        } catch (ConvertException e) {
            return toAppendTo.append("Not A Number");
        }
        String s = num.toString();
        int pt = s.indexOf('.');
        if(pt == -1) pt = s.length();
        boolean first = true;
        int last = s.length();
        if(rank) {
            last--;
            while(last > 0 && s.charAt(last-1) == '0') last--;
        }
        int i = 0;
        switch(pt%3) {
        	case 1: i = -2; break;   	
        	case 2: i = -1; break;
        }
        for(; i+2 < pt; i+=3) {
            char hundreds = (i < 0 ? '0' : s.charAt(i));
            char tens = (i+1 < 0 ? '0' : s.charAt(i+1));
            char ones = (i+2 < 0 ? '0' : s.charAt(i+2));
            int denom = ((pt-i)/3) - 1;
            int start = toAppendTo.length();
            if(hundreds != '0') {
                String word = digitNames[hundreds-'1'];
                if(first) first = false;
                else toAppendTo.append(' ');
                toAppendTo.append(word);
                toAppendTo.append('-');
                toAppendTo.append((i == last && denom == 0 ? "hundredth" : "hundred"));
            }
            if(tens == '1') {
                String word = ((i+1 == last || i+2 == last)  && denom == 0 ? teenRanks : teenNames)[ones-'0'];
                if(first) first = false;
                else toAppendTo.append(' ');
                toAppendTo.append(word);             
            } else {
                if(tens > '1') {
	                String word = (i+1 == last  && denom == 0 ? tensRanks : tensNames)[tens-'2'];
	                if(first) first = false;
	                else toAppendTo.append(' ');
	                toAppendTo.append(word);
	                if(ones != '0') toAppendTo.append('-');	  	        } else if(first) first = false;
                else if(ones != '0') toAppendTo.append(' ');
                if(ones != '0') {
                    String word = (i+2 == last  && denom == 0 ? digitRanks : digitNames)[ones-'1'];
                    toAppendTo.append(word);
                }
            }
            if(denom > 0 && start < toAppendTo.length()) {
                String word = (i >= last && i+2 <= last ? groupRanks : groupNames)[denom-1];
                toAppendTo.append(' ');
                toAppendTo.append(word);
            }
        }
        return toAppendTo;
    }
}
