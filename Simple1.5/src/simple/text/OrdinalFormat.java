package simple.text;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;

public class OrdinalFormat extends DecimalFormat {
	private static final long serialVersionUID = 8036171418694025942L;
	protected static final String[] suffixes = new String[] {
		"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"
	};

	public OrdinalFormat() {
		super();
	}

	public OrdinalFormat(String pattern, DecimalFormatSymbols symbols) {
		super(pattern, symbols);
	}

	public OrdinalFormat(String pattern) {
		super(pattern);
	}
	public static String getSuffix(long number) {
		return suffixes[Math.abs((int) (number % 10))];
	}
	@Override
	public StringBuffer format(long number, StringBuffer result, FieldPosition fieldPosition) {
		return super.format(number, result, fieldPosition).append(getSuffix(number));
	}
	
	@Override
	public StringBuffer format(double number, StringBuffer result, FieldPosition fieldPosition) {
		return format((long) number, result, fieldPosition);
	}
}
