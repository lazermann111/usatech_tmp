package simple.text;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.util.Currency;
import java.util.Locale;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

public class MultiCurrencyFormat extends AbstractPatternedFormat {
	private static final long serialVersionUID = 5030604865373252608L;
	protected static final Pattern ZERO_PATTERN = Pattern.compile("0(?:\\.0+)?");
	protected static final String CURRENCY_SYMBOL = "\u00A4";
	protected final ThreadLocal<DecimalFormat> decimalFormatLocal = new ThreadLocal<DecimalFormat>() {
		@Override
		protected DecimalFormat initialValue() {
			return new DecimalFormat(decimalFormatString);
		}
	};
	protected final String decimalFormatString;
	protected boolean blankOnZero;
	public MultiCurrencyFormat(String pattern) {
		super(pattern);
		if(pattern.startsWith("*")) {
			blankOnZero = true;
			pattern = pattern.substring(1);
		}
		String currencySymbol = Currency.getInstance(Locale.getDefault()).getSymbol();
		int si = pattern.indexOf(currencySymbol);
		if(si >= 0) {
			int qi = pattern.indexOf('\'');
			if(qi < 0)
				pattern = pattern.replace(currencySymbol, CURRENCY_SYMBOL);
			else {
				StringBuilder sb = new StringBuilder();
				if(si < qi)
					sb.append(pattern.substring(0, qi).replace(currencySymbol, CURRENCY_SYMBOL));
				int eqi = pattern.indexOf('\'', qi + 1);
				if(eqi < 0)
					eqi = pattern.length() - 1;
				sb.append(pattern, qi, eqi + 1);

			}
		}
		decimalFormatString = pattern;
	}

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		//expect obj to be a two element array of number and currency code
		Number value;
		String currencyCode;
		try {
			Object[] arr = ConvertUtils.convert(Object[].class, obj);
			value = ConvertUtils.convert(Number.class, arr[0]);
			currencyCode = ConvertUtils.convert(String.class, arr[1]);
		} catch(ConvertException e) {
			throw new IllegalArgumentException("Could not convert to an array of number and currency", e);
		}
		return format(value, currencyCode, toAppendTo, pos);
	}

	public StringBuffer format(Number value, String currencyCode, StringBuffer toAppendTo, FieldPosition pos) {
		if(value != null && (!blankOnZero || !ZERO_PATTERN.matcher(value.toString()).matches())) {
			Currency currency;
			if(currencyCode == null) {
				currency = null;
				currencyCode = "";
			} else
				try {
					currency = Currency.getInstance(currencyCode);
				} catch(IllegalArgumentException e) {
					currency = null;
				}
			DecimalFormat decimalFormat = decimalFormatLocal.get();
			if(currency == null) {
				DecimalFormatSymbols dfs = decimalFormat.getDecimalFormatSymbols();
				dfs.setInternationalCurrencySymbol(currencyCode);
				dfs.setCurrencySymbol(currencyCode);
				decimalFormat.setDecimalFormatSymbols(dfs);
			} else if(!currency.equals(decimalFormat.getCurrency())) {
				DecimalFormatSymbols dfs = decimalFormat.getDecimalFormatSymbols();
				dfs.setCurrency(currency);
				dfs.setCurrencySymbol(ConvertUtils.getCurrencySymbol(currency));
				decimalFormat.setDecimalFormatSymbols(dfs);
			}
			decimalFormat.format(value, toAppendTo, pos);
		}
		return toAppendTo;
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		// TODO Auto-generated method stub
		return null;
	}
}
