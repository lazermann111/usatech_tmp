/*
 * MatchFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;

import java.util.List;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

/** This class is similar to java.text.ChoiceFormat except that it compares Strings rather than
 * Numbers. It is configured with a list of key-value pairs in the form "key1=value1;key2=value2;".
 * To include an "=" or ";" in the key or value pair use single-quotes to quote it. To format an
 * object, this class searches for the first key (a Regular Expression) that matches the object to be formatted and uses it's
 * corresponding value as the output. The value can hold references to the grouped portions of the 
 * Regular Expression using the "\0"-"\9" to represent the whole String and capturing groups 1 - 9. (Remember
 * Java uses "\" as an escape character so you must use "\\0" in Java code to represent the whole match.)
 * If no match is found nothing is appended to the StringBuffer or an empty String is returned.
 *
 * @author  Brian S. Krug
 */
public class MatchFormat extends AbstractPatternedFormat {
    /**
	 * 
	 */
	private static final long serialVersionUID = 80912345710654L;

	protected interface Output {
        public String get(RegexUtils.Finder finder) ;
    }
    protected class LiteralOutput implements Output {
        protected String literal;
        public LiteralOutput(String _literal) {
            literal = _literal;
        }
        public String get(RegexUtils.Finder finder) {
            return literal;
        }
    }
    protected class GroupOutput implements Output {
        protected int group;
        public GroupOutput(int _group) {
            group = _group;
        }
        public String get(RegexUtils.Finder finder) {
            return finder.match()[group];
        }
    }
    
    protected class MatchObject {
        protected String match;
        protected Output[] outputs;
    }
    
    protected MatchObject[] matches;
	protected String defaultValue;
        
    /** Creates a new instance of MatchFormat */
    public MatchFormat(String pattern) {
        super(pattern);
        parsePattern(pattern);
    }
    
    public StringBuffer format(Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos) {
        String s;
        try {
            s = ConvertUtils.convert(String.class, obj);
        } catch(ConvertException ce) {
            s = obj == null ? "" : obj.toString();
        }
        if(s == null) s = "";
        for(int i = 0; i < matches.length; i++) {
            RegexUtils.Finder finder = RegexUtils.find(matches[i].match, s, null);
            if(finder.matches()) {
                for(int k = 0; k < matches[i].outputs.length; k++)
                    toAppendTo.append(matches[i].outputs[k].get(finder));
				return toAppendTo;
            }
        }
		if(defaultValue != null)
			toAppendTo.append(defaultValue);
        return toAppendTo;
    }
    
    public Object parseObject(String source, java.text.ParsePosition pos) {
        /*java.util.Map m = new java.util.HashMap();
        for(int i = 0; i < formatObjects.length; i++) {
            Object value = formatObjects[i].format.parseObject(source, pos);
            if(formatObjects[i].parameter != null) m.put(formatObjects[i].parameter, value);
        }        
        return m;
         **/
        return null;
    }
    
    protected Output[] parseOutputs(String pattern) {
        StringBuffer segment = new StringBuffer();
        List<Output> list = new java.util.LinkedList<Output>();
        for(int p = 0; p < pattern.length(); p++) {
            char ch = pattern.charAt(p);
            switch(ch) {
                case '\\':
                    if(p+1 < pattern.length()) {
                        char next = pattern.charAt(p+1);
                        if(Character.isDigit(next)) {
                            if(segment.length() > 0) {
                                list.add(new LiteralOutput(segment.toString()));
                                segment.setLength(0);
                            }
                            list.add(new GroupOutput(Character.digit(next, 10)));
                        } else if(next == '\\') {
                            segment.append(ch);
                        } else {
                            segment.append(ch);
                            segment.append(next);
                        }
                        p++;
                    } else {
                        segment.append(ch);
                    }
                    break;
                default:
                    segment.append(ch);
            }
        }
        if(segment.length() > 0) list.add(new LiteralOutput(segment.toString()));
        return list.toArray(new Output[list.size()]);                            
    }
    
    protected void parsePattern(String pattern) {
		String[] pairs = StringUtils.splitQuotesAnywhere(pattern, ';', '\'', true);
        List<MatchObject> list = new java.util.LinkedList<MatchObject>();
        for(int i = 0; i < pairs.length; i++) {
			if(pairs[i].length() == 0)
				continue;
            int p = pairs[i].indexOf('=');
			if(p < 0) {
				if(defaultValue != null)
					throw new IllegalArgumentException("Pattern is not formatted correctly; Only one default value can be provided.");
				defaultValue = pairs[i];
			} else {
				MatchObject mo = new MatchObject();
				mo.match = pairs[i].substring(0, p);
				mo.outputs = parseOutputs(pairs[i].substring(p + 1));
				list.add(mo);
			}
        }
        matches = list.toArray(new MatchObject[list.size()]);
    }    
}
