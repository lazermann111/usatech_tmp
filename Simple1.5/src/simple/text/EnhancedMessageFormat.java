/*
 * EnhancedMessageFormat.java
 *
 * Created on December 26, 2001, 11:41 AM
 */

package simple.text;

/**
 * This class builds on MessageFormat and allows StringFormat  as well as being extendable
 *
 * @author  bkrug
 * @version
 */
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.text.ChoiceFormat;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import simple.bean.ConvertUtils;


public class EnhancedMessageFormat extends Format {
	private static final long serialVersionUID = -5700788409875794264L;
	protected static final String[] typeList =
	{"", "", "number", "", "date", "", "time", "", "choice", "", "string", "","cents"};
	protected static final String[] modifierList =
	{"", "", "currency", "", "percent", "", "integer"};
	protected static final String[] dateModifierList =
	{"", "", "short", "", "medium", "", "long", "", "full"};
	protected Locale locale = Locale.getDefault();
	protected String pattern = "";
	protected static final int MAX_ARGUMENTS = 10;
	protected Format[] formats = new Format[MAX_ARGUMENTS];
	protected int[] offsets = new int[MAX_ARGUMENTS];
	protected int[] argumentNumbers = new int[MAX_ARGUMENTS];
	protected int maxOffset = -1;
	
	/** Creates new EnhancedMessageFormat */
	public EnhancedMessageFormat(String pattern) {
		applyPattern(pattern);
	}
	
	public void setLocale(Locale theLocale) {
		locale = theLocale;
	}
	
	public Locale getLocale() {
		return locale;
	}
	
	public void applyPattern(String newPattern) {
		StringBuffer[] segments = new StringBuffer[4];
		for (int i = 0; i < segments.length; ++i) {
			segments[i] = new StringBuffer();
		}
		int part = 0;
		int formatNumber = 0;
		boolean inQuote = false;
		int braceStack = 0;
		maxOffset = -1;
		for (int i = 0; i < newPattern.length(); ++i) {
			char ch = newPattern.charAt(i);
			if (part == 0) {
				if (ch == '\'') {
					if (i + 1 < newPattern.length()
					&& newPattern.charAt(i+1) == '\'') {
						segments[part].append(ch);  // handle doubles
						++i;
					} else {
						inQuote = !inQuote;
					}
				} else if (ch == '{' && !inQuote) {
					part = 1;
				} else {
					segments[part].append(ch);
				}
			} else  if (inQuote) {              // just copy quotes in parts
				segments[part].append(ch);
				if (ch == '\'') {
					inQuote = false;
				}
			} else {
				switch (ch) {
					case ',':
						if (part < 3)
							part += 1;
						else
							segments[part].append(ch);
						break;
					case '{':
						++braceStack;
						segments[part].append(ch);
						break;
					case '}':
						if (braceStack == 0) {
							part = 0;
							makeFormat(i, formatNumber, segments);
							formatNumber++;
						} else {
							--braceStack;
							segments[part].append(ch);
						}
						break;
					case '\'':
						inQuote = true;
						// fall through, so we keep quotes in other parts
					default:
						segments[part].append(ch);
						break;
				}
			}
		}
		if (braceStack == 0 && part != 0) {
			maxOffset = -1;
			throw new IllegalArgumentException("Unmatched braces in the pattern.");
		}
		pattern = segments[0].toString();
	}
	
	public String toPattern() {
		// later, make this more extensible
		int lastOffset = 0;
		StringBuffer result = new StringBuffer();
		for (int i = 0; i <= maxOffset; ++i) {
			copyAndFixQuotes(pattern, lastOffset, offsets[i],result);
			lastOffset = offsets[i];
			result.append('{');
			result.append(argumentNumbers[i]);
			if (formats[i] == null) {
				// do nothing, string format
			} else if (formats[i] instanceof DecimalFormat) {
				if (formats[i].equals(NumberFormat.getInstance(locale))) {
					result.append(",number");
				} else if (formats[i].equals(
				NumberFormat.getCurrencyInstance(locale))) {
					result.append(",number,currency");
				} else if (formats[i].equals(
				NumberFormat.getPercentInstance(locale))) {
					result.append(",number,percent");
				} else if (formats[i].equals(getIntegerFormat(locale))) {
					result.append(",number,integer");
				} else {
					result.append(",number," +
					((DecimalFormat)formats[i]).toPattern());
				}
			} else if (formats[i] instanceof SimpleDateFormat) {
				if (formats[i].equals(DateFormat.getDateInstance(
				DateFormat.DEFAULT,locale))) {
					result.append(",date");
				} else if (formats[i].equals(DateFormat.getDateInstance(
				DateFormat.SHORT,locale))) {
					result.append(",date,short");
				} else if (formats[i].equals(DateFormat.getDateInstance(
				DateFormat.DEFAULT,locale))) {
					result.append(",date,medium");
				} else if (formats[i].equals(DateFormat.getDateInstance(
				DateFormat.LONG,locale))) {
					result.append(",date,long");
				} else if (formats[i].equals(DateFormat.getDateInstance(
				DateFormat.FULL,locale))) {
					result.append(",date,full");
				} else if (formats[i].equals(DateFormat.getTimeInstance(
				DateFormat.DEFAULT,locale))) {
					result.append(",time");
				} else if (formats[i].equals(DateFormat.getTimeInstance(
				DateFormat.SHORT,locale))) {
					result.append(",time,short");
				} else if (formats[i].equals(DateFormat.getTimeInstance(
				DateFormat.DEFAULT,locale))) {
					result.append(",time,medium");
				} else if (formats[i].equals(DateFormat.getTimeInstance(
				DateFormat.LONG,locale))) {
					result.append(",time,long");
				} else if (formats[i].equals(DateFormat.getTimeInstance(
				DateFormat.FULL,locale))) {
					result.append(",time,full");
				} else {
					result.append(",date,"
					+ ((SimpleDateFormat)formats[i]).toPattern());
				}
			} else if (formats[i] instanceof ChoiceFormat) {
				result.append(",choice,"
				+ ((ChoiceFormat)formats[i]).toPattern());
			} else if (formats[i] instanceof StringFormat) {
				result.append(",string,"
				+ ((StringFormat)formats[i]).getPattern());
			} else if (formats[i] instanceof CentsFormat) {
				result.append(",cents");
				String pat = ((CentsFormat)formats[i]).toPattern();
				if(pat != null) result.append("," + pat);
			} else {
				//result.append(", unknown");
			}
			result.append('}');
		}
		copyAndFixQuotes(pattern, lastOffset, pattern.length(), result);
		return result.toString();
	}
	
	public void setFormats(Format[] newFormats) {
		formats = newFormats.clone();
	}
	
	public void setFormat(int variable, Format newFormat) {
		formats[variable] = newFormat;
	}
	
	public Format[] getFormats() {
		return formats.clone();
	}
	
	public final StringBuffer format(Object[] source, StringBuffer result,	FieldPosition ignore) {
		return format(source,result,ignore, 0);
	}
	
	public static String format(String pattern, Object[] arguments) {
		EnhancedMessageFormat temp = new EnhancedMessageFormat(pattern);
		return temp.format(arguments);
	}
	
	public final StringBuffer format(Object source, StringBuffer result, FieldPosition ignore) {
		return format((Object[])source, result,ignore, 0);
	}
	
	public Object[] parse(String source, ParsePosition status) {
		Object[] empty = {};
		if (source == null) return empty;
		Object[] resultArray = new Object[10];
		int patternOffset = 0;
		int sourceOffset = status.getIndex();
		ParsePosition tempStatus = new ParsePosition(0);
		for (int i = 0; i <= maxOffset; ++i) {
			// match up to format
			int len = offsets[i] - patternOffset;
			if (len == 0 || pattern.regionMatches(patternOffset,
			source, sourceOffset, len)) {
				sourceOffset += len;
				patternOffset += len;
			} else {
				status.setErrorIndex(sourceOffset);
				return null; // leave index as is to signal error
			}
			
			// now use format
			if (formats[i] == null) {   // string format
				// if at end, use longest possible match
				// otherwise uses first match to intervening string
				// does NOT recursively try all possibilities
				int tempLength = (i != maxOffset) ? offsets[i+1] : pattern.length();
				
				int next;
				if (patternOffset >= tempLength) {
					next = source.length();
				}else{
					next = source.indexOf( pattern.substring(patternOffset,tempLength), sourceOffset);
				}
				
				if (next < 0) {
					status.setErrorIndex(sourceOffset);
					return null; // leave index as is to signal error
				} else {
					String strValue= source.substring(sourceOffset,next);
					if (!strValue.equals("{"+argumentNumbers[i]+"}"))
						resultArray[argumentNumbers[i]]	= source.substring(sourceOffset,next);
					sourceOffset = next;
				}
			} else {
				tempStatus.setIndex(sourceOffset);
				resultArray[argumentNumbers[i]] = formats[i].parseObject(source,tempStatus);
				if (tempStatus.getIndex() == sourceOffset) {
					status.setErrorIndex(sourceOffset);
					return null; // leave index as is to signal error
				}
				sourceOffset = tempStatus.getIndex(); // update
			}
		}
		int len = pattern.length() - patternOffset;
		if (len == 0 || pattern.regionMatches(patternOffset, source, sourceOffset, len)) {
			status.setIndex(sourceOffset + len);
		} else {
			status.setErrorIndex(sourceOffset);
			return null; // leave index as is to signal error
		}
		return resultArray;
	}
	
	public Object[] parse(String source) throws ParseException {
		ParsePosition status  = new ParsePosition(0);
		Object[] result = parse(source, status);
		if (status.getIndex() == 0)  // unchanged, returned object is null
			throw new ParseException("EnhancedMessageFormat parse error!", status.getErrorIndex());
		
		return result;
	}
	public Object parseObject(String text, ParsePosition status) {
		return parse(text, status);
	}
	
	public Object clone() {
		EnhancedMessageFormat other = (EnhancedMessageFormat) super.clone();
		
		// clone arrays. Can't do with utility because of bug in Cloneable
		other.formats = formats.clone(); // shallow clone
		for (int i = 0; i < formats.length; ++i) {
			if (formats[i] != null)
				other.formats[i] = (Format)formats[i].clone();
		}
		// for primitives or immutables, shallow clone is enough
		other.offsets = offsets.clone();
		other.argumentNumbers = argumentNumbers.clone();
		
		return other;
	}
	
	/**
	 * Equality comparision between two message format objects
	 */
	public boolean equals(Object obj) {
		if (this == obj)                      // quick check
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		EnhancedMessageFormat other = (EnhancedMessageFormat) obj;
		return (maxOffset == other.maxOffset
		&& pattern.equals(other.pattern)
		&& ConvertUtils.areEqual(locale, other.locale)   // does null check
		&& Arrays.equals(offsets,other.offsets)
		&& Arrays.equals(argumentNumbers,other.argumentNumbers)
		&& Arrays.equals(formats,other.formats));
	}
	
	public int hashCode() {
		return pattern.hashCode(); // enough for reasonable distribution
	}
	
	protected EnhancedMessageFormat(String pattern, Locale loc) {
		locale = (Locale)loc.clone();
		applyPattern(pattern);
	}
	
	protected StringBuffer format(Object[] arguments, StringBuffer result,	FieldPosition status, int recursionProtection) {
		// note: this implementation assumes a fast substring & index.
		// if this is not true, would be better to append chars one by one.
		int lastOffset = 0;
		for (int i = 0; i <= maxOffset; ++i) {
			result.append(pattern.substring(lastOffset, offsets[i]));
			lastOffset = offsets[i];
			int argumentNumber = argumentNumbers[i];
			if (arguments == null || argumentNumber >= arguments.length) {
				result.append("{" + argumentNumber + "}");
				continue;
			}
			// int argRecursion = ((recursionProtection >> (argumentNumber*2)) & 0x3);
			/*
			if (false) { // if (argRecursion == 3){
				// prevent loop!!!
				result.append('\uFFFD');
			} else */{
				Object obj = arguments[argumentNumber];
				String arg;
				boolean tryRecursion = false;
				if (obj == null || obj.toString() == null) {
					arg = "null";
				} else if (obj.toString().length() == 0) {
					arg = "";
				} else if (formats[i] != null) {
					if(formats[i] instanceof NumberFormat) {
						if(!(obj instanceof Number)) obj = new java.math.BigDecimal(obj.toString());
					} else if(formats[i] instanceof DateFormat) {
						if(!(obj instanceof java.util.Date)) obj = ConvertUtils.convertSafely(java.util.Date.class, obj, null);
					} 
					arg = formats[i].format(obj);
					tryRecursion = formats[i] instanceof ChoiceFormat;
				} else if (obj instanceof Number) {
					// format number if can
					arg = NumberFormat.getInstance(locale).format(obj); // fix
				} else if (obj instanceof Date) {
					// format a Date if can
					arg = DateFormat.getDateTimeInstance(DateFormat.SHORT,	DateFormat.SHORT, locale).format(obj);//fix
				} else if (obj instanceof String) {
					arg = (String) obj;
				} else {
					arg = obj.toString();
				}
				
				// recurse if necessary
				if (tryRecursion && arg.indexOf('{') >= 0) {
					EnhancedMessageFormat temp = new EnhancedMessageFormat(arg, locale);
					temp.format(arguments,result,status,recursionProtection);
				} else {
					result.append(arg);
				}
			}
		}
		result.append(pattern.substring(lastOffset, pattern.length()));
		return result;
	}
	
	protected void makeFormat(int position, int offsetNumber, StringBuffer[] segments) {
		// get the number
		int argumentNumber;
		int oldMaxOffset = maxOffset;
		try {
			argumentNumber = Integer.parseInt(segments[1].toString()); // always unlocalized!
			if (argumentNumber < 0 || argumentNumber > 9) {
				throw new NumberFormatException();
			}
			maxOffset = offsetNumber;
			offsets[offsetNumber] = segments[0].length();
			argumentNumbers[offsetNumber] = argumentNumber;
		} catch (Exception e) {
			throw new IllegalArgumentException("argument number too large at ");
		}
		
		// now get the format
		Format newFormat = null;
		switch (findKeyword(segments[2].toString(), typeList)) {
			case 0:
				break;
			case 1: case 2:// number
				switch (findKeyword(segments[3].toString(), modifierList)) {
					case 0: // default;
						newFormat = NumberFormat.getInstance(locale);
						break;
					case 1: case 2:// currency
						newFormat = NumberFormat.getCurrencyInstance(locale);
						break;
					case 3: case 4:// percent
						newFormat = NumberFormat.getPercentInstance(locale);
						break;
					case 5: case 6:// integer
						newFormat = getIntegerFormat(locale);
						break;
					default: // pattern
						newFormat = NumberFormat.getInstance(locale);
						try {
							((DecimalFormat)newFormat).applyPattern(segments[3].toString());
						} catch (Exception e) {
							maxOffset = oldMaxOffset;
							throw new IllegalArgumentException("Pattern incorrect or locale does not support formats, error at ");
						}
						break;
				}
				break;
			case 3: case 4: // date
				switch (findKeyword(segments[3].toString(), dateModifierList)) {
					case 0: // default
						newFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
						break;
					case 1: case 2: // short
						newFormat = DateFormat.getDateInstance(DateFormat.SHORT, locale);
						break;
					case 3: case 4: // medium
						newFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
						break;
					case 5: case 6: // long
						newFormat = DateFormat.getDateInstance(DateFormat.LONG, locale);
						break;
					case 7: case 8: // full
						newFormat = DateFormat.getDateInstance(DateFormat.FULL, locale);
						break;
					default:
						newFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
						try {
							((SimpleDateFormat)newFormat).applyPattern(segments[3].toString());
						} catch (Exception e) {
							maxOffset = oldMaxOffset;
							throw new IllegalArgumentException(
							"Pattern incorrect or locale does not support formats, error at ");
						}
						break;
				}
				break;
			case 5: case 6:// time
				switch (findKeyword(segments[3].toString(), dateModifierList)) {
					case 0: // default
						newFormat = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale);
						break;
					case 1: case 2: // short
						newFormat = DateFormat.getTimeInstance(DateFormat.SHORT, locale);
						break;
					case 3: case 4: // medium
						newFormat = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale);
						break;
					case 5: case 6: // long
						newFormat = DateFormat.getTimeInstance(DateFormat.LONG, locale);
						break;
					case 7: case 8: // full
						newFormat = DateFormat.getTimeInstance(DateFormat.FULL, locale);
						break;
					default:
						newFormat = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale);
						try {
							((SimpleDateFormat)newFormat).applyPattern(segments[3].toString());
						} catch (Exception e) {
							maxOffset = oldMaxOffset;
							throw new IllegalArgumentException(
							"Pattern incorrect or locale does not support formats, error at ");
						}
						break;
				}
				break;
			case 7: case 8:// choice
				try {
					newFormat = new ChoiceFormat(segments[3].toString());
				} catch (Exception e) {
					maxOffset = oldMaxOffset;
					throw new IllegalArgumentException(
					"Choice Pattern incorrect, error at ");
				}
				break;
			case 9: case 10:// string
				try {
					newFormat = new StringFormat(segments[3].toString());
				} catch (Exception e) {
					maxOffset = oldMaxOffset;
					throw new IllegalArgumentException("String Pattern incorrect, error at ");
				}
				break;
			case 11: case 12: //cents
				try {
					if(segments[3] == null || segments[3].toString().trim().length() == 0) newFormat = new CentsFormat();
					else newFormat = new CentsFormat(segments[3].toString());
				} catch (Exception e) {
					maxOffset = oldMaxOffset;
					throw new IllegalArgumentException("Cents Pattern incorrect, error at ");
				}
				break;
				
			default:
				maxOffset = oldMaxOffset;
				throw new IllegalArgumentException("unknown format type at ");
		}
		formats[offsetNumber] = newFormat;
		segments[1].setLength(0);   // throw away other segments
		segments[2].setLength(0);
		segments[3].setLength(0);
	}
	
	protected static final int findKeyword(String s, String[] list) {
		s = s.trim().toLowerCase();
		for (int i = 0; i < list.length; ++i) {
			if (s.equals(list[i]))
				return i;
		}
		return -1;
	}
	
	/**
	 * Convenience method that ought to be in NumberFormat
	 */
	protected NumberFormat getIntegerFormat(Locale locale) {
		NumberFormat temp = NumberFormat.getInstance(locale);
		if (temp instanceof DecimalFormat) {
			DecimalFormat temp2 = (DecimalFormat) temp;
			temp2.setMaximumFractionDigits(0);
			temp2.setDecimalSeparatorAlwaysShown(false);
			temp2.setParseIntegerOnly(true);
		}
		return temp;
	}
	
	protected static final void copyAndFixQuotes(String source, int start, int end, StringBuffer target) {
		for (int i = start; i < end; ++i) {
			char ch = source.charAt(i);
			if (ch == '{') {
				target.append("'{'");
			} else if (ch == '}') {
				target.append("'}'");
			} else if (ch == '\'') {
				target.append("''");
			} else {
				target.append(ch);
			}
		}
	}
	
	protected void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		boolean isValid = maxOffset >= -1
		&& maxOffset < MAX_ARGUMENTS
		&& formats.length == MAX_ARGUMENTS
		&& offsets.length == MAX_ARGUMENTS
		&& argumentNumbers.length == MAX_ARGUMENTS;
		if (isValid) {
			int lastOffset = pattern.length() + 1;
			for (int i = maxOffset; i >= 0; --i) {
				if ((offsets[i] < 0) || (offsets[i] > lastOffset)) {
					isValid = false;
					break;
				} else {
					lastOffset = offsets[i];
				}
			}
		}
		if (!isValid) {
			throw new InvalidObjectException("Could not reconstruct EnhancedMessageFormat from corrupt stream.");
		}
	}
	
}