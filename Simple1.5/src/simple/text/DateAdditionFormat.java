/*
 * Created on Feb 17, 2006
 *
 */
package simple.text;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

public class DateAdditionFormat extends AbstractPatternedFormat {
    private static final long serialVersionUID = -2433805357398957361L;
    protected ThreadLocal<DateFormat> dateFormat;
    protected int dateField;
    protected int amount;
    public DateAdditionFormat(String pattern) throws IllegalArgumentException, SecurityException, IllegalAccessException, NoSuchFieldException {
        super(pattern);
        parsePattern(pattern);
    }

    protected void parsePattern(String pattern) throws IllegalArgumentException, SecurityException, IllegalAccessException, NoSuchFieldException {
        String[] parts = pattern.split(",",3);
        if(parts.length != 3) 
            throw new IllegalArgumentException("Pattern must be in the form 'DATE_FIELD,AMOUNT,DATE_FORMAT'");
        dateField = Calendar.class.getField(parts[0].trim().toUpperCase()).getInt(null);
        amount = Integer.parseInt(parts[1].trim());
        final String datePattern = parts[2].trim();
        dateFormat = new ThreadLocal<DateFormat>() {
    		@Override
    		protected DateFormat initialValue() {
    			return new SimpleDateFormat(datePattern);
    		}		
    	};
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        Date date;
        try {
            date = ConvertUtils.convert(Date.class, obj);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(dateField, amount);
            date = cal.getTime();
        } catch (ConvertException e) {
            return toAppendTo.append("Not A Date");
        }
        return dateFormat.get().format(date, toAppendTo, pos);
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
        Date date = dateFormat.get().parse(source, pos);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(dateField, -amount);
        date = cal.getTime();
        return date;
    }
}
