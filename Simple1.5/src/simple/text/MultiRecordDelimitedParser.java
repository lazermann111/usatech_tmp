/**
 *
 */
package simple.text;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Brian S. Krug
 *
 */
public class MultiRecordDelimitedParser<T> extends AbstractDelimitedParser {
	protected final Map<T,List<DelimitedColumn>> recordColumns = new HashMap<T,List<DelimitedColumn>>();
	protected DelimitedColumn selectorColumn;
	
	public MultiRecordDelimitedParser() {
	}
	
	public void setSelectorColumn(String label, Class<T> type) {
		selectorColumn = new DelimitedColumn(label, type);
	}
	
	public void setRecordColumns(T selectorValue, DelimitedColumn[] columns) {
		recordColumns.put(selectorValue, Arrays.asList(columns.clone()));
	}
	
	public void removeRecordColumns(T selectorValue) {
		recordColumns.remove(selectorValue);
	}
	
	protected String[] getColumnLabels() throws IOException {
		if(getSelectorColumn() == null)
			throw new IOException("The selectorColumn was not configured");
		Set<String> columnNameSet = new LinkedHashSet<>();
		columnNameSet.add(getSelectorColumn().getLabel());
		for(List<DelimitedColumn> cols : recordColumns.values())
			for(DelimitedColumn col : cols)
				columnNameSet.add(col.getLabel());
		return columnNameSet.toArray(new String[columnNameSet.size()]);
	}
	
	protected DelimitedColumn getFirstColumn() throws IOException {
		if(getSelectorColumn() == null)
			throw new IOException("The selectorColumn was not configured");
		return getSelectorColumn();
	}
	
	protected List<DelimitedColumn> getRemainingColumns(Object selectorValue) {
		return getRecordColumns(selectorValue);
	}
	
	public DelimitedColumn getSelectorColumn() {
		return selectorColumn;
	}
	
	public List<DelimitedColumn> getRecordColumns(Object selectorValue) {
		return recordColumns.get(selectorValue);
	}
	
	public boolean hasRecordColumns(Object selectorValue) {
		return recordColumns.containsKey(selectorValue);
	}
}
