/**
 *
 */
package simple.text;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import simple.text.StringUtils.Justification;

/**
 * @author Brian S. Krug
 *
 */
public class MultiRecordFixedWidthParser<T> extends AbstractFixedWidthParser {
	protected final Map<T,List<FixedWidthColumn>> recordColumns = new HashMap<T,List<FixedWidthColumn>>();
	protected FixedWidthColumn selectorColumn;
	
	public MultiRecordFixedWidthParser() {
	}
	public void setSelectorColumn(String label, int size, Justification justification, char padding, Class<T> type) {
		selectorColumn = new FixedWidthColumn(size, justification, padding, label, type);
	}
	public void setRecordColumns(T selectorValue, FixedWidthColumn[] columns) {
		recordColumns.put(selectorValue, Arrays.asList(columns.clone()));
	}
	protected String[] getColumnLabels() throws IOException {
		if(getSelectorColumn() == null)
			throw new IOException("The selectorColumn was not configured");
		Set<String> columnNameSet = new HashSet<String>();
		columnNameSet.add(getSelectorColumn().getLabel());
		for(List<FixedWidthColumn> cols : recordColumns.values())
			for(FixedWidthColumn col : cols)
				columnNameSet.add(col.getLabel());
		return columnNameSet.toArray(new String[columnNameSet.size()]);	
	}
	protected FixedWidthColumn getFirstColumn() throws IOException {
		if(getSelectorColumn() == null)
			throw new IOException("The selectorColumn was not configured");
		return getSelectorColumn();
	}
	protected List<FixedWidthColumn> getRemainingColumns(Object selectorValue) {
		return recordColumns.get(selectorValue);
	}
	public FixedWidthColumn getSelectorColumn() {
		return selectorColumn;
	}
}
