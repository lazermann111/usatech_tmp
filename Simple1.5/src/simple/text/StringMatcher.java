/*
 * Created on Jan 27, 2005
 *
 */
package simple.text;

import java.util.HashMap;
import java.util.Map;

/**
 * @author bkrug
 *
 */
public class StringMatcher {
    protected int where;
    protected boolean caseSensitive;
    protected Map<String,String>[] matches;
    public static final int MATCH_BEGINNING = 1;
    public static final int MATCH_ENDING = 2;
    
    public StringMatcher(int where, boolean caseSensitive) {
        this.where = where;
        this.caseSensitive = caseSensitive;        
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addMatch(String find, String replace) {
        int len = find.length();
        if(matches == null) matches = new Map[len+1];
        else if(matches.length <= len) {
            Map[] tmp = new Map[len+1];
            System.arraycopy(matches, 0, tmp, 0, matches.length);
            matches = tmp;
        }
        if(matches[len] == null) matches[len] = new HashMap();
        if(caseSensitive) {
            matches[len].put(find, replace);
        } else {
            matches[len].put(find.toUpperCase(), replace.toUpperCase());          
            matches[len].put(find.toLowerCase(), replace.toLowerCase());          
        }
    }
    
    public String matchAndReplace(String s) {
        if(matches != null)
	        for(int i = Math.min(s.length(), matches.length)-1; i >= 0; i--) {
	            if(matches[i] != null) {
	                String find = null;
	                switch(where) {
	                	case MATCH_BEGINNING:
	                	    find = s.substring(0, i);
	                	    break;
	                	case MATCH_ENDING:
	                	    find = s.substring(s.length() - i);
	                }
	                String replace = matches[i].get(find);
	                if(replace != null) {
		                switch(where) {
		                	case MATCH_BEGINNING:
		                	    return replace + s.substring(i);
		                	case MATCH_ENDING:
		                	    return s.substring(0, s.length() - find.length()) + replace;
		                }	                    
	                }
	            }
	        }
        return s;
    }
}
