/**
 *
 */
package simple.text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import simple.text.StringUtils.Justification;

/**
 * @author Brian S. Krug
 *
 */
public class FixedWidthParser extends AbstractFixedWidthParser {
	protected final List<FixedWidthColumn> columns = new ArrayList<FixedWidthColumn>();
	protected FixedWidthColumn firstColumn;
	
	public FixedWidthParser() {
	}

	public void addColumn(String label, int size, Justification justification) {
		addColumn(label, size, justification, ' ', null);
	}
	public void addColumn(String label, int size, Justification justification, char padding, Class<?> type) {
		addColumn(new FixedWidthColumn(size, justification, padding, label, type));
	}
	public void addColumn(FixedWidthColumn column) {
		if(firstColumn == null)
			firstColumn = column;
		else
			columns.add(column);
	}
	@Override
	protected String[] getColumnLabels() throws IOException {
		if(firstColumn == null)
			throw new IOException("No columns were configured");
		String[] columnLabels = new String[columns.size() + 1];
		columnLabels[0] = firstColumn.getLabel();
		int i = 1;
		for(FixedWidthColumn column : columns) {
			columnLabels[i++] = column.getLabel();
		}
		return columnLabels;
	}
	
	@Override
	protected FixedWidthColumn getFirstColumn() throws IOException {
		if(firstColumn == null)
			throw new IOException("No columns were configured");
		return firstColumn;
	}
	@Override
	protected List<FixedWidthColumn> getRemainingColumns(Object selectorValue) throws IOException {
		return columns;
	}
}
