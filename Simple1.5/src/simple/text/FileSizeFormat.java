/*
 * FileSizeFormat.java
 *
 * Created on March 7, 2003, 1:21 PM
 */

package simple.text;

import java.math.BigDecimal;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author  Brian S. Krug
 */
public class FileSizeFormat extends Format {
    /**
	 * 
	 */
	private static final long serialVersionUID = 88948194912334L;
	private static final BigDecimal ONE = BigDecimal.valueOf(1);
    private static final int K = 1024;
    private static final int M = K*K;
    private static final int G = K*K*K;
    private static final Map<String,BigDecimal> units = new HashMap<String,BigDecimal>();
    static {
        units.put("K",BigDecimal.valueOf(K));
        units.put("M",BigDecimal.valueOf(M));
        units.put("G",BigDecimal.valueOf(G));       
    }
    /** Creates a new instance of FileSizeFormat */
    public FileSizeFormat() {
    }
    
    public StringBuffer format(Object obj, java.lang.StringBuffer result, FieldPosition fieldPosition) {        
        result.append(((Number)obj).toString());
        return result;
    }
    
    public Object parseObject(String str, ParsePosition pos) {
        Object o = parse(str);
        pos.setIndex(str.length());
        return o;
    }
    
    public static BigDecimal parse(String str) {
        //strip B from end
        str = str.trim().toUpperCase();
        if(str.endsWith("B")) str = str.substring(0,str.length() - 1);
        //calc unit
        BigDecimal mult = units.get(str.substring(str.length()-1));
        if(mult != null) str = str.substring(0,str.length() - 1);
        else mult = ONE;
        
        //parse number
        BigDecimal bd = new BigDecimal(str);
        return bd.multiply(mult);
    }

}
