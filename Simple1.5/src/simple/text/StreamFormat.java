/**
 *
 */
package simple.text;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.text.FieldPosition;
import java.text.ParsePosition;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.HeapBufferStream;
import simple.io.IOUtils;

/** Decompresses the input
 * @author Brian S. Krug
 *
 */
public class StreamFormat extends AbstractPatternedFormat {
	private static final long serialVersionUID = 8263726362775982814L;
	protected final Charset charset;
	/**
	 * @param pattern
	 */
	public StreamFormat(String pattern) {
		super(pattern);
		charset = Charset.forName(pattern == null || (pattern=pattern.trim()).length() == 0 ? "UTF-8" : pattern);
	}

	/**
	 * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer, java.text.FieldPosition)
	 */
	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		InputStream in;
		try {
			in = ConvertUtils.convertRequired(InputStream.class, obj);
		} catch(ConvertException e) {
			throw new IllegalArgumentException("Could not convert object to an InputStream", e);
		}
		Reader reader = new InputStreamReader(in, charset);
    	try {
			IOUtils.copy(reader, toAppendTo);
		} catch(IOException e) {
			throw new RuntimeException("Could not read compressed data", e);
		}
    	return toAppendTo;
	}

	/**
	 * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
	 */
	@Override
	public Object parseObject(String source, ParsePosition pos) {
		HeapBufferStream stream = new HeapBufferStream(1024, source.length());
		Writer writer = new OutputStreamWriter(stream.getOutputStream(), charset);
    	try {
			writer.write(source, pos.getIndex(), source.length() - pos.getIndex());
			writer.flush();
	    } catch(IOException e) {
			pos.setErrorIndex(pos.getIndex());
			return null;
		} finally {
			try {
				writer.close();
			} catch(IOException e) {
			}
		}
    	pos.setIndex(source.length());
		return stream.getInputStream();
	}

}
