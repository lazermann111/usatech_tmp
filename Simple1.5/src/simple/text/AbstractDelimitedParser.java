/**
 *
 */
package simple.text;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.Decision;
import simple.results.DatasetHandler;
import simple.util.CollectionUtils;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractDelimitedParser {
	private static final Log log = Log.getLog();

	public static enum ExtraHandling {
		IGNORE, OUTPUT, ERROR
	};
	protected String lineSeparator = "\r\n";
	protected String columnSeparator = ",";
	protected char[] quotes = null;
	protected int initialBufferSize = 512;
	protected ExtraHandling extraColumnHandling = ExtraHandling.ERROR;
	protected ExtraHandling missingSelectorHandling = ExtraHandling.IGNORE;
	protected List<String> missingSelectorValues = new ArrayList<String>();
	
	protected abstract String[] getColumnLabels() throws IOException ;
	protected abstract DelimitedColumn getFirstColumn() throws IOException ;
	protected abstract List<DelimitedColumn> getRemainingColumns(Object selectorValue) throws IOException ;
	
	public <I extends Exception> Reader newFilter(final Reader reader, final DatasetHandler<I> handler, Decision<Object> shouldPassThru) throws IOException {
		final DelimitedColumn first = getFirstColumn();
		return new Reader() {
			protected char[] buffer = null;
			protected int pos = 0;
			protected int valid = 0;
			protected Character currentQuote = null;
			protected int start = 0;
			protected int offset = 0;
			protected long tot = 0;
			protected int ls = 0;
			protected int cs = 0;
			protected int colIndex = 0;
			protected List<DelimitedColumn> columns = null;
			protected StringBuilder stored = null;
			protected boolean filtering = false;

			@Override
			public int read() throws IOException {
				if(pos < valid)
					return buffer[pos++];
				fill();
				if(pos < valid)
					return buffer[pos++];
				return -1;
			}

			@Override
			public int read(char[] cbuf, int off, int len) throws IOException {
				if(pos < valid) {
					int r = Math.min(valid - pos, len);
					System.arraycopy(buffer, pos, cbuf, off, r);
					pos += r;
					return r;
				}
				fill();
				if(pos < valid) {
					int r = Math.min(valid - pos, len);
					System.arraycopy(buffer, pos, cbuf, off, r);
					pos += r;
					return r;
				}
				return -1;
			}

			protected void fill() throws IOException {
				try {
					if(buffer == null) {
						buffer = new char[getInitialBufferSize()];
						handler.handleDatasetStart(getColumnLabels());
					}

					if(offset >= buffer.length) {
						int s = (filtering || pos > start ? start : pos);
						if(s > 0) {
							if(s > 16) {
								System.arraycopy(buffer, s, buffer, 0, offset - s);
							} else { // expand buffer
								char[] tmp = new char[buffer.length * 2];
								System.arraycopy(buffer, s, tmp, 0, offset - s);
								buffer = tmp;

							}
							offset -= s;
							pos -= s;
							valid -= s;
							start -= s;
						}
					}
					int r;
					while((r = reader.read(buffer, offset, buffer.length - offset)) != -1) {
						if(r == 0)
							continue;
						tot += r;
						for(int i = offset; i < r + offset; i++) {
							if(currentQuote != null) {
								int q = CollectionUtils.iterativeSearch(buffer, currentQuote, i, r + offset - i);
								if(q < 0) {
									cs = 0;
									ls = 0;
									break; // need more data
								} else {
									currentQuote = null;
									if(stored == null)
										stored = new StringBuilder();
									stored.append(buffer, start, q - start);
									start = q + 1;
									i = q;
									cs = 0;
									ls = 0;
									continue;
								}
							}
							if(start == i && quotes != null && quotes.length > 0) {
								int e = CollectionUtils.iterativeSearch(quotes, buffer[i], 0, quotes.length);
								if(e >= 0) {
									currentQuote = quotes[e];
									start++;
									cs = 0;
									ls = 0;
									continue;
								}
							}
							if(buffer[i] == columnSeparator.charAt(cs)) {
								cs++;
								if(cs >= columnSeparator.length()) {
									String value;
									if(stored == null)
										value = new String(buffer, start, i - start - cs + 1);
									else {
										stored.append(buffer, start, i - start - cs + 1);
										value = stored.toString();
										stored = null;
									}
									if(colIndex > 0 && columns != null && colIndex > columns.size()) {
										switch(extraColumnHandling) {
											case IGNORE:
												break;
											case ERROR:
												throw new IOException("Extra Columns found at position " + (tot - r - offset + i + 1));
											case OUTPUT:
												DelimitedColumn column = new DelimitedColumn("");
												columns.add(column);
												handleValue(column, value, handler);
										}
									} else {
										if(colIndex == 0) {
											Object selectorValue;
											if(first.getType() == null)
												selectorValue = value;
											else
												try {
													selectorValue = ConvertUtils.convert(first.getType(), value);
												} catch(ConvertException e) {
													throw new IOException("For column " + first.getLabel() + ", value '" + value + "'", e);
												}

											columns = getRemainingColumns(selectorValue);
											if(columns == null && missingSelectorHandling == ExtraHandling.OUTPUT)
												columns = new ArrayList<>();
											if(columns == null) {
												switch(missingSelectorHandling) {
													case IGNORE:
														if(!missingSelectorValues.contains(value)) {
															log.debug("No columns are defined for selector {0}={1}", first.getLabel(), value);
															missingSelectorValues.add(value);
														}
														break;
													case ERROR:
														throw new IOException("No columns are defined for selector '" + value + '\'');
												}
											} else {
												handler.handleRowStart();
												handler.handleValue(first.getLabel(), selectorValue);
											}
											boolean nextFiltering = (shouldPassThru != null && !shouldPassThru.decide(selectorValue));
											if(filtering == false && nextFiltering == true) {
												valid = start;
											} else if(filtering == true && nextFiltering == false) {
												pos = start;
												valid = i;
											}
											filtering = nextFiltering;
										} else if(columns != null) {
											handleValue(columns.get(colIndex - 1), value, handler);
										}
									}
									start = i + 1;
									colIndex++;
								} else
									continue;
							} else if(buffer[i] == lineSeparator.charAt(ls)) {
								ls++;
								if(ls >= lineSeparator.length()) {
									if(columns == null || colIndex == 0) {
										boolean content = false;
										for(int k = start; k < i - ls; k++)
											if(!Character.isWhitespace(buffer[k])) {
												content = true;
												break;
											}
										if(!content) {
											start = i + 1;
											colIndex = 0;
											continue;// skip blank lines
										}
									}
									String value;
									if(stored == null)
										value = new String(buffer, start, i - start - ls + 1);
									else {
										stored.append(buffer, start, i - start - ls + 1);
										value = stored.toString();
										stored = null;
									}
									if(colIndex > 0 && (columns == null || colIndex > columns.size())) {
										switch(extraColumnHandling) {
											case IGNORE:
												break;
											case ERROR:
												throw new IOException("Extra Columns found at position " + (tot - r - offset + i + 1));
											case OUTPUT:
												DelimitedColumn column = new DelimitedColumn("");
												columns.add(column);
												handleValue(column, value, handler);
										}
									} else {
										DelimitedColumn column;
										if(colIndex == 0) {
											handler.handleRowStart();
											column = first;
										} else {
											column = columns.get(colIndex - 1);
										}
										handleValue(column, value, handler);
									}
									handler.handleRowEnd();
									start = i + 1;
									colIndex = 0;
								} else
									continue;
							}
							cs = 0;
							ls = 0;
						}
						offset += r;
						if(!filtering)
							valid = offset;
						if(pos < valid)
							return;
						if(offset >= buffer.length) {
							int s = (filtering || pos > start ? start : pos);
							if(s > 0) {
								if(s > 16) {
									System.arraycopy(buffer, s, buffer, 0, offset - s);
								} else { // expand buffer
									char[] tmp = new char[buffer.length * 2];
									System.arraycopy(buffer, s, tmp, 0, offset - s);
									buffer = tmp;

								}
								offset -= s;
								pos -= s;
								valid -= s;
								start -= s;
							}
						}
					}
					if(currentQuote != null) {
						throw new IOException("Unclosed quote (" + currentQuote + ") at position " + (tot - offset + start));
					}
					if(start < offset && columns != null) {
						String value;
						if(stored == null)
							value = new String(buffer, start, offset - start);
						else {
							stored.append(buffer, start, offset - start);
							value = stored.toString();
							stored = null;
						}
						if(colIndex > 0 && colIndex > columns.size()) {
							switch(extraColumnHandling) {
								case IGNORE:
									break;
								case ERROR:
									throw new IOException("Extra Columns found at position " + (tot - offset + start + 1));
								case OUTPUT:
									DelimitedColumn column = new DelimitedColumn("");
									columns.add(column);
									handleValue(column, value, handler);
							}
						} else {
							DelimitedColumn column;
							if(colIndex == 0) {
								handler.handleRowStart();
								column = first;
							} else {
								column = columns.get(colIndex - 1);
							}
							colIndex = 0;
							handleValue(column, value, handler);
						}
						handler.handleRowEnd();
					}
					handler.handleDatasetEnd();
				} catch(Exception e) {
					throw new IOException(e);
				}
			}

			@Override
			public boolean ready() throws IOException {
				return pos < valid || reader.ready();
			}

			@Override
			public void close() throws IOException {
				reader.close();
			}
		};
	}
	public <I extends Exception> void parse(Reader reader, DatasetHandler<I> handler) throws IOException, I {
		DelimitedColumn first = getFirstColumn();
		handler.handleDatasetStart(getColumnLabels());
		char[] buffer = new char[getInitialBufferSize()];
		Character currentQuote = null;
		int r;
		int start = 0;
		int offset = 0;
		long tot = 0;
		int ls = 0;
		int cs = 0;
		int colIndex = 0;
		List<DelimitedColumn> columns = null;
		StringBuilder stored = null;
		while((r = reader.read(buffer, offset, buffer.length - offset)) != -1) {
			if(r == 0)
				continue;
			tot += r;
			for(int i = offset; i < r + offset; i++) {
				if(currentQuote != null) {
					int q = CollectionUtils.iterativeSearch(buffer, currentQuote, i, r + offset - i);
					if(q < 0) {
						cs = 0;
						ls = 0;
						break; // need more data
					} else {
						currentQuote = null;
						if(stored == null)
							stored = new StringBuilder();
						stored.append(buffer, start, q - start);
						start = q + 1;
						i = q;
						cs = 0;
						ls = 0;
						continue;
					}
				}
				if(start == i && quotes != null && quotes.length > 0) {
					int e = CollectionUtils.iterativeSearch(quotes, buffer[i], 0, quotes.length);
					if(e >= 0) {
						currentQuote = quotes[e];
						start++;
						cs = 0;
						ls = 0;
						continue;
					}
				}
				if(buffer[i] == columnSeparator.charAt(cs)) {
					cs++;
					if(cs >= columnSeparator.length()) {
						String value;
						if(stored == null)
							value = new String(buffer, start, i - start - cs + 1);
						else {
							stored.append(buffer, start, i - start - cs + 1);
							value = stored.toString();
							stored = null;
						}
						if(colIndex > 0 && columns != null && colIndex > columns.size()) {
							switch(extraColumnHandling) {
								case IGNORE:
									break;
								case ERROR:
									throw new IOException("Extra Columns found at position " + (tot - r - offset + i + 1));
								case OUTPUT:
									DelimitedColumn column = new DelimitedColumn("");
									columns.add(column);
									handleValue(column, value, handler);
							}
						} else {
							if(colIndex == 0) {
								Object selectorValue;
								if(first.getType() == null)
									selectorValue = value;
								else
									try {
										selectorValue = ConvertUtils.convert(first.getType(), value);
									} catch(ConvertException e) {
										throw new IOException("For column " + first.getLabel() + ", value '" + value + "'", e);
									}

								columns = getRemainingColumns(selectorValue);
								if(columns == null && missingSelectorHandling == ExtraHandling.OUTPUT)
									columns = new ArrayList<>();
								if(columns == null) {
									switch(missingSelectorHandling) {
										case IGNORE:
											if(!missingSelectorValues.contains(value)) {
												log.debug("No columns are defined for selector {0}={1}", first.getLabel(), value);
												missingSelectorValues.add(value);
											}
											break;
										case ERROR:
											throw new IOException("No columns are defined for selector '" + value + '\'');
									}
								} else {
									handler.handleRowStart();
									handler.handleValue(first.getLabel(), selectorValue);
								}
							} else if(columns != null) {
								handleValue(columns.get(colIndex - 1), value, handler);
							}
						}
						start = i + 1;
						colIndex++;
					} else
						continue;
				} else if(buffer[i] == lineSeparator.charAt(ls)) {
					ls++;
					if(ls >= lineSeparator.length()) {
						if(columns == null || colIndex == 0) {
							boolean content = false;
							for(int k = start; k < i - ls; k++)
								if(!Character.isWhitespace(buffer[k])) {
									content = true;
									break;
								}
							if(!content) {
								start = i + 1;
								colIndex = 0;
								cs = 0;
								ls = 0;
								continue;// skip blank lines
							}
						}
						String value;
						if(stored == null)
							value = new String(buffer, start, i - start - ls + 1);
						else {
							stored.append(buffer, start, i - start - ls + 1);
							value = stored.toString();
							stored = null;
						}
						if(colIndex > 0 && (columns == null || colIndex > columns.size())) {
							switch(extraColumnHandling) {
								case IGNORE:
									break;
								case ERROR:
									throw new IOException("Extra Columns found at position " + (tot - r - offset + i + 1));
								case OUTPUT:
									DelimitedColumn column = new DelimitedColumn("");
									columns.add(column);
									handleValue(column, value, handler);
							}
						} else {
							DelimitedColumn column;
							if(colIndex == 0) {
								handler.handleRowStart();
								column = first;
							} else {
								column = columns.get(colIndex - 1);
							}
							handleValue(column, value, handler);
						}
						handler.handleRowEnd();
						start = i + 1;
						colIndex = 0;
					} else
						continue;
				}
				cs = 0;
				ls = 0;
			}
			offset += r;
			if(offset >= buffer.length) {
				if(start > 16) {
					System.arraycopy(buffer, start, buffer, 0, offset - start);
					offset -= start;
					start = 0;
				} else { // expand buffer
					char[] tmp = new char[buffer.length * 2];
					System.arraycopy(buffer, start, tmp, 0, offset - start);
					buffer = tmp;
					offset -= start;
					start = 0;
				}
			}
		}
		if(currentQuote != null) {
			throw new IOException("Unclosed quote (" + currentQuote + ") at position " + (tot - offset + start));
		}
		if(start < offset && columns != null) {
			String value;
			if(stored == null)
				value = new String(buffer, start, offset - start);
			else {
				stored.append(buffer, start, offset - start);
				value = stored.toString();
				stored = null;
			}
			if(colIndex > 0 && colIndex > columns.size()) {
				switch(extraColumnHandling) {
					case IGNORE:
						break;
					case ERROR:
						throw new IOException("Extra Columns found at position " + (tot - offset + start + 1));
					case OUTPUT:
						DelimitedColumn column = new DelimitedColumn("");
						columns.add(column);
						handleValue(column, value, handler);
				}
			} else {
				DelimitedColumn column;
				if(colIndex == 0) {
					handler.handleRowStart();
					column = first;
				} else {
					column = columns.get(colIndex - 1);
				}
				colIndex = 0;
				handleValue(column, value, handler);
			}
			handler.handleRowEnd();
		}
		handler.handleDatasetEnd();
	}

	protected <I extends Exception> Object handleValue(DelimitedColumn column, String stringValue, DatasetHandler<I> handler) throws IOException, I {
		Object value;
		if(column.getType() == null)
			value = stringValue;
		else
			try {
				value = ConvertUtils.convert(column.getType(), stringValue);
			} catch(ConvertException e) {
				throw new IOException("For column " + column.getLabel() + ", value '" + stringValue + "'", e);
			}
		handler.handleValue(column.getLabel(), value);
		return value;
	}
	public String getLineSeparator() {
		return lineSeparator;
	}
	public void setLineSeparator(String lineSeparator) {
		this.lineSeparator = lineSeparator;
	}
	public String getColumnSeparator() {
		return columnSeparator;
	}
	public void setColumnSeparator(String columnSeparator) {
		this.columnSeparator = columnSeparator;
	}
	public char[] getQuotes() {
		return quotes;
	}
	public void setQuotes(char... quotes) {
		this.quotes = quotes;
	}
	public int getInitialBufferSize() {
		return initialBufferSize;
	}
	public void setInitialBufferSize(int initialBufferSize) {
		this.initialBufferSize = initialBufferSize;
	}
	public boolean isIgnoringExtraColumns() {
		return extraColumnHandling == ExtraHandling.IGNORE;
	}
	public void setIgnoringExtraColumns(boolean ignoringExtraColumns) {
		this.extraColumnHandling = ExtraHandling.IGNORE;
	}

	public ExtraHandling getExtraColumnHandling() {
		return extraColumnHandling;
	}

	public void setExtraColumnHandling(ExtraHandling extraColumnHandling) {
		this.extraColumnHandling = extraColumnHandling;
	}

	public ExtraHandling getMissingSelectorHandling() {
		return missingSelectorHandling;
	}

	public void setMissingSelectorHandling(ExtraHandling missingSelectorHandling) {
		this.missingSelectorHandling = missingSelectorHandling;
	}	
}
