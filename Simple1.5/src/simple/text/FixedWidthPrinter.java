/**
 *
 */
package simple.text;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import simple.bean.ConvertUtils;
import simple.text.StringUtils.Justification;

/**
 * @author Brian S. Krug
 *
 */
public class FixedWidthPrinter {
	protected final PrintWriter writer;
	protected final List<FixedWidthColumn> columns = new ArrayList<FixedWidthColumn>();
	protected boolean headerWritten = false;
	protected String columnSeparator = "  ";
	protected int count;

	public FixedWidthPrinter(PrintWriter writer) {
		this.writer = writer;
	}

	public void addColumn(String label, int size, Justification justification) {
		addColumn(label, size, justification, ' ', null);
	}
	public void addColumn(String label, int size, Justification justification, char padding, Class<?> type) {
		addColumn(new FixedWidthColumn(size, justification, padding, label, type));
	}
	public void addColumn(FixedWidthColumn column) {
		if(headerWritten)
			throw new IllegalStateException("Header has already been written; cannot add columns");
		columns.add(column);
	}
	public void writeRow(Object... values) {
		if(values.length > columns.size())
			throw new IllegalArgumentException("Only " + columns.size() + " columns were defined but " + values.length + " values were provided");
		checkHeaderWritten();
		int index = 0;
		for(FixedWidthColumn info : columns) {
			String value = (values.length > index ? ConvertUtils.getStringSafely(values[index]) : null);
			try {
				StringUtils.appendPadded(writer, value, info.getPadding(), info.getSize(), info.getJustification());
			} catch(IOException e) {
				// ignore
			}
			writer.print(columnSeparator);
			index++;
		}
		writer.println();
		count++;
	}
	public void writeFooter() {
		checkHeaderWritten();
		if(count == 0) {
			writer.println("No Rows Found");
		} else {
			int total = -columnSeparator.length();
			for(FixedWidthColumn info : columns) {
				total += info.size + columnSeparator.length();
			}
			writeChar('-', total);
			writer.println();
			writer.print(count);
			writer.println(" row(s)");
		}
		writer.flush();
	}
	/**
	 *
	 */
	protected void checkHeaderWritten() {
		if(!headerWritten) {
			for(FixedWidthColumn info : columns) {
				writeValue(info.label, info.size, info.justification);
				writer.print(columnSeparator);
			}
			writer.println();
			for(FixedWidthColumn info : columns) {
				writeChar('-', info.size);
				writer.print(columnSeparator);
			}
			writer.println();

			headerWritten = true;
		}
	}
	protected void writeChar(char ch, int times) {
		for(int i = 0; i < times; i++)
			writer.print(ch);
	}
	protected void writeValue(String value, int size, Justification justification) {
		if(value == null) {
			writeChar(' ', size);
		} else if(value.length() >= size){
			writer.write(value, 0, size);
		} else {
			switch(justification) {
	            case LEFT:
	            	writer.write(value);
	            	writeChar(' ', size - value.length());
	            	break;
	            case CENTER:
	            	int n = (size - value.length()) / 2;
	            	writeChar(' ', n);
	            	writer.write(value);
	            	writeChar(' ', size - value.length() - n);
	            	break;
	            case RIGHT:
	            	writeChar(' ', size - value.length());
	            	writer.write(value);
	            	break;
	        }
		}
	}

	public String getColumnSeparator() {
		return columnSeparator;
	}

	public void setColumnSeparator(String columnSeparator) {
		this.columnSeparator = columnSeparator;
	}

	public int getCount() {
		return count;
	}
}
