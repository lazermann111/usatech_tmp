/*
 * Created on Feb 6, 2006
 *
 */
package simple.text;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Collection;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

public class NVLFormat extends Format {
    private static final long serialVersionUID = -667379411197982710L;

    public NVLFormat() {
        super();
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		if(obj != null) {
        	try {
				Collection<?> coll = ConvertUtils.asCollection(obj);
				if(coll != null) {
					for(Object o : coll) {
						if(o != null) {
							String s = ConvertUtils.getStringSafely(o);
							if(s != null && s.trim().length() > 0) {
								toAppendTo.append(s);
								break;
							}
						}
					}
				}
			} catch(ConvertException e) {
				toAppendTo.append(ConvertUtils.getStringSafely(obj));
			}
        }
        return toAppendTo;
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
        return source;
    }

}
