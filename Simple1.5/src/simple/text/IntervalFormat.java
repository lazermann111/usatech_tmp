package simple.text;

import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.concurrent.TimeUnit;

/**
 * Create a string that says the number of days, hours, minutes, seconds, or milliseconds. Show <code>depth</code> units unless 0
 * 
 * @author bkrug
 * 
 */
public class IntervalFormat extends NumberFormat {
	private static final long serialVersionUID = 7540398533138297293L;
	protected static final TimeUnit[] timeUnits = TimeUnit.values();
	protected String separator = ", ";
	protected int depth = 2;
	@Override
	public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
		double d = Math.floor(number);
		return format((long) d, (long) (1000L * 1000L * (number - d)), toAppendTo);
	}

	@Override
	public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
		return format(number, 0L, toAppendTo);
	}

	protected StringBuffer format(long millis, long nanos, StringBuffer toAppendTo) {
		int depth = getDepth();
		for(int i = timeUnits.length - 1; i >= 2 && depth > 0 && millis > 0; i--) {
			long t = timeUnits[i].convert(millis, TimeUnit.MILLISECONDS);
			if(t == 0)
				continue;
			if(depth-- < getDepth())
				toAppendTo.append(separator);
			toAppendTo.append(t).append(' ').append(timeUnits[i].toString().toLowerCase());
			if(t == 1 && toAppendTo.charAt(toAppendTo.length() - 1) == 's')
				toAppendTo.setLength(toAppendTo.length() - 1);
			millis -= timeUnits[i].toMillis(t);
		}
		for(int i = 1; i >= 0 && depth > 0 && nanos > 0; i--) {
			long t = timeUnits[i].convert(nanos, TimeUnit.NANOSECONDS);
			if(t == 0)
				continue;
			if(depth-- < getDepth())
				toAppendTo.append(separator);
			toAppendTo.append(t).append(' ').append(timeUnits[i].toString().toLowerCase());
			if(t == 1 && toAppendTo.charAt(toAppendTo.length() - 1) == 's')
				toAppendTo.setLength(toAppendTo.length() - 1);
			nanos -= timeUnits[i].toNanos(t);
		}
		return toAppendTo;
	}

	@Override
	public Number parse(String source, ParsePosition parsePosition) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}
}
