/*
 * MessageFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.ChoiceFormat;
import java.text.DateFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.SystemUtils;

/** This class is an enhancement of the java.text.MessageFormat class. It allows names and complex
 * bean properties (see ReflectionUtils.getProperty() method). It also does not limit the number of
 * paramaters allowed as does the java.text.MessageFormat class. It IS thread-safe, unlike java.text.MessageFormat
 *
 * @author  Brian S. Krug
 */
public class MessageFormat extends AbstractPatternedFormat {
    private static final long serialVersionUID = 8239414184L;
    private static final Log log = Log.getLog();
	protected static final char[] FORMAT_ESCAPEES = "{}'".toCharArray();
	protected static final char FORMAT_ESCAPER = '\'';

	public static class FormatObject {
        public final Format format;
        public final String[] parameters;
        public FormatObject(Format format) {
        	this.format = format;
        	this.parameters = null;
        }
        public FormatObject(Format format, String parameter) {
        	this.format = format;
        	this.parameters = StringUtils.split(parameter, '&', '\'');
		}
        public boolean equals(Object obj) {
            if(!(obj instanceof FormatObject)) return false;
            FormatObject fo = (FormatObject)obj;
            return ConvertUtils.areEqual(format, fo.format)
                && ConvertUtils.areEqual(parameters, fo.parameters);
        }

		@Override
		public int hashCode() {
			return SystemUtils.addHashCodes(0, format, parameters);
		}
    }
    protected FormatObject[] formatObjects;
    protected boolean allowSubFormats = true;
    
    /** Creates a new instance of MessageFormat */
	public MessageFormat(String pattern, Object defaultFormats, boolean allowSubFormats, Locale locale) {
        super(pattern);
		parsePattern(pattern, defaultFormats, locale);
        this.allowSubFormats = allowSubFormats;
    }
    
    /** Creates a new instance of MessageFormat */
    public MessageFormat(String pattern) {
		this(pattern, null, true, Locale.getDefault());
	}

	public MessageFormat(String pattern, Locale locale) {
		this(pattern, null, true, locale);
    }
    
    public StringBuffer format(Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos) {
        //NOTE: For performance reasons, we'll attempt to do this without creating a new StringBuffer tmp
        /*
        StringBuffer tmp = new StringBuffer();
        for(int i = 0; i < formatObjects.length; i++) {
            Object param = getParameter(obj, formatObjects[i].parameter);
            if(formatObjects[i].format == null) toAppendTo.append(param == null ? "" : param);
            else {
                formatObjects[i].format.format(param, tmp, pos);
                if(tmp.toString().indexOf("{") >= 0) new MessageFormat(tmp.toString()).format(obj, toAppendTo, pos);
                else toAppendTo.append(tmp.toString());
                tmp.setLength(0);
            }
        }
        */
        for(int i = 0; i < formatObjects.length; i++) {
			Object param = getParameters(obj, formatObjects[i].parameters);
            if(formatObjects[i].format == null) {
                if(param != null) try {
                    toAppendTo.append(ConvertUtils.convert(String.class, param));
                } catch(ConvertException e) {
                    log.warn("Could not convert '" + param + "' to a String. Using toString() method");
                    toAppendTo.append(param);
                }
            }
            else {
                int start = toAppendTo.length();
                if(formatObjects[i].format instanceof DateFormat) {
                    try {
                        param = ConvertUtils.convert(Date.class, param);
                    } catch(ConvertException e) {
                        log.warn("Could not convert '" + param + "' to a Date");
                    }
                } else if(formatObjects[i].format instanceof NumberFormat) {
                    try {
                        param = ConvertUtils.convert(BigDecimal.class, param);
                    } catch(ConvertException e) {
                        log.warn("Could not convert '" + param + "' to a Number");
                    }
                }
                try {
                    formatObjects[i].format.format(param, toAppendTo, pos);
                } catch(IllegalArgumentException e) {
                    if(param != null) throw e;
                } catch(NullPointerException e) {
                    if(param != null) throw e;                    
                }
                if(allowSubFormats && 
                		(  formatObjects[i].format instanceof MessageFormat 
                        || formatObjects[i].format instanceof ChoiceFormat
                        || formatObjects[i].format instanceof NullFormat
                        || formatObjects[i].format instanceof MatchFormat)) {
                    int p = toAppendTo.indexOf("{", start);
                    if(p >= 0) {
                        String sub = toAppendTo.substring(start);
                        toAppendTo.setLength(start);
                        new MessageFormat(sub).format(obj, toAppendTo, pos);
                    }
                }
            }
        }
        
        return toAppendTo;
    }
    
    public Object parseObject(String source, java.text.ParsePosition pos) {
        java.util.Map<String,Object> m = new java.util.HashMap<String,Object>();
        for(int i = 0; i < formatObjects.length; i++) {
            Object value = formatObjects[i].format.parseObject(source, pos);
			if(formatObjects[i].parameters != null) {
				if(formatObjects[i].parameters.length == 1)
					m.put(formatObjects[i].parameters[0], value);
				else {
					Object[] arr = ConvertUtils.convertSafely(Object[].class, value, new Object[] { value });
					if(arr != null)
						for(int k = 0; k < arr.length && k < formatObjects[i].parameters.length; k++)
							m.put(formatObjects[i].parameters[k], arr[k]);
				}
			}
        }        
        return m;
    }
    
	protected void parsePattern(String pattern, Object defaultFormats, Locale locale) {
        int braces = 0;
        List<FormatObject> list = new java.util.LinkedList<FormatObject>();
        String[] parts = new String[3];
        StringBuffer segment = new StringBuffer();
        for(int p = 0; p < pattern.length(); p++) {
            char ch = pattern.charAt(p);
            switch(ch) {
                case '{':
                    //format previous part
                    if(segment.length() > 0) {
						FormatObject fo = new FormatObject(new LiteralFormat(segment.toString()));
                        list.add(fo);
                        segment.setLength(0);
                    }
                    braces = 1;
                    //look for parameter stuff
                    int part = 0;
                    for(p++; braces > 0 && p < pattern.length(); p++) {
                        ch = pattern.charAt(p);
                        switch(ch) {
                            case '\'': //Keep quotes but condense pairs of quotes to one
                                segment.append(ch);
                                if(p + 1 < pattern.length() && pattern.charAt(p+1) == ch) {
                                    p++;
                                } else {
                                    int start = p + 1;
                                    p = pattern.indexOf('\'',p+1);
                                    if(p < 0) throw new IllegalArgumentException("Unmatched single-quote in param section of format pattern (character " + start + ")");
                                    segment.append(pattern.substring(start, p+1));
                                }
                                break;
                            case '{':
                                if(part == 2) braces++;
                                segment.append(ch);
                                break;
                            case '}':
                                braces--;
                                if(braces == 0) {
                                    parts[part++] = segment.toString().trim();
                                    segment.setLength(0);
                                } else segment.append(ch);
                                break;
                             case ',':
                                if(braces == 1 && part < 2) {
                                    parts[part++] = segment.toString().trim();
                                    segment.setLength(0);
                                } else segment.append(ch);
                                break;                                
                            default:
                                segment.append(ch);
                        }
                    }
                    if(braces != 0) throw new IllegalArgumentException("Unbalanced braces; expecting '}'");
                    p--;
					String parameter = parts[0].length() == 0 ? null : parts[0];
					Format format;
                    if(part > 1) {
						format = ConvertUtils.getFormat(parts[1], part > 2 ? parts[2] : null, locale);
                    } else if(defaultFormats != null) {
                        try {
							format = ConvertUtils.convert(Format.class, simple.bean.ReflectionUtils.getProperty(defaultFormats, parameter));
                        } catch (ConvertException e) {
							format = null;
                        } catch (IntrospectionException e) {
							format = null;
                        } catch (IllegalAccessException e) {
							format = null;
                        } catch (InvocationTargetException e) {
							format = null;
                        } catch(ParseException e) {
							format = null;
            			}
					} else
						format = null;
					FormatObject fo = new FormatObject(format, parameter);
                    list.add(fo);
                    break;
                case '\'': //Remove quote and condense pairs of quotes to one
                    if(p + 1 < pattern.length() && pattern.charAt(p+1) == ch) {
                        segment.append(ch);
                        p++;
                    } else {
                        int start = p + 1;
                        p = pattern.indexOf('\'',p+1);
                        if(p < 0) p = pattern.length();
                        segment.append(pattern.substring(start, p));
                    }
                    break;
                default:
                    segment.append(ch);
            }               
        }
        if(segment.length() > 0) {
			FormatObject fo = new FormatObject(new LiteralFormat(segment.toString()));
            list.add(fo);
        }
        formatObjects = list.toArray(new FormatObject[list.size()]);
    }
    
	protected Object getParameters(Object bean, String[] parameters) {
		if(bean == null || parameters == null)
			return bean;
		switch(parameters.length) {
			case 0:
				return bean;
			case 1:
				return getParameter(bean, parameters[0]);
			default:
				Object[] arr = new Object[parameters.length];
				for(int i = 0; i < parameters.length; i++)
					arr[i] = getParameter(bean, parameters[i]);
				return arr;
		}
	}

	protected Object getParameter(Object bean, String parameter) {
		if(parameter == null)
			return bean;
        if(parameter.startsWith("'") && parameter.endsWith("'"))
            return parameter.substring(1, parameter.length() - 1);
        try {
            return simple.bean.ReflectionUtils.getProperty(bean, parameter);
        } catch(IntrospectionException e) {
            log.debug("Could not get property '" + parameter + "' in " + bean, e);
            return "{" + parameter + "}";
        } catch (IllegalAccessException e) {
            log.debug("Could not get property '" + parameter + "' in " + bean, e);
            return "{" + parameter + "}";
        } catch (InvocationTargetException e) {
            log.debug("Could not get property '" + parameter + "' in " + bean, e);
            return "{" + parameter + "}";
        } catch(ParseException e) {
            log.debug("Could not get property '" + parameter + "' in " + bean, e);
            return "{" + parameter + "}";
		}
    }
        
    public FormatObject[] getFormatObjects() {
        return formatObjects.clone(); // so outsiders can change it
    }

    /* (non-Javadoc)
     * @see simple.text.AbstractPatternedFormat#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && Arrays.deepEquals(formatObjects, ((MessageFormat)obj).formatObjects);
    }
    
	public static String format(String pattern, Object... params) {
    	return new MessageFormat(pattern).format(params);
    }

	public static String escape(String s) {
		if(s == null)
			return null;
		if(s.indexOf('{') >= 0 || s.indexOf(FORMAT_ESCAPER) >= 0) {
			StringBuilder sb = new StringBuilder(s.length() + 2);
			for(int i = 0; i < s.length(); i++) {
				char ch = s.charAt(i);
				switch(ch) {
					case FORMAT_ESCAPER:
						sb.append(FORMAT_ESCAPER);
						sb.append(ch);
						break;
					case '{':
						sb.append(FORMAT_ESCAPER);
						sb.append(ch);
						sb.append(FORMAT_ESCAPER);
						break;
					default:
						sb.append(ch);
				}
			}
			return sb.toString();
		}
		return s;
	}
}
