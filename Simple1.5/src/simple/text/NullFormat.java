/*
 * NullFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;

import simple.bean.ConvertUtils;

/** Formats an object returning different Strings based on whether the object is null or not.
 *
 * @author  Brian S. Krug
 */
public class NullFormat extends AbstractPatternedFormat {    
    /**
	 * 
	 */
	private static final long serialVersionUID = 80134714700L;
	private String whenNull;
    private String whenNotNull;
    
    /** Creates a new instance of NullFormat with the specified pattern.
     * @param pattern The comma-separated list of what to return if not null, then
     * what to return if null.
     * 
     */
    public NullFormat(String pattern) {
        super(pattern);
        String[] parts = StringUtils.split(pattern, StringUtils.STANDARD_DELIMS, StringUtils.STANDARD_QUOTES);
        switch(parts.length) {
        	case 1:
				whenNotNull = parts[0];
                whenNull = "";
                break;
        	case 2:
                whenNotNull = parts[0];
                whenNull = parts[1];
        		break;
        	default:
        		whenNotNull = parts[0];
            	whenNull = StringUtils.join(parts, ",", 1, parts.length - 1);    		
        }
    }
    
    public StringBuffer format(Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos) {
		toAppendTo.append(obj == null || ConvertUtils.getStringSafely(obj, "").isEmpty() ? whenNull : whenNotNull);
        return toAppendTo;
    }
    
    public Object parseObject(String source, java.text.ParsePosition pos) {
        if(source.regionMatches(pos.getIndex(), whenNotNull, 0, whenNotNull.length())) {
            pos.setIndex(pos.getIndex() + whenNotNull.length());
        } else if(source.regionMatches(pos.getIndex(), whenNull, 0, whenNull.length())) {
            pos.setIndex(pos.getIndex() + whenNull.length());
        } else {
            pos.setErrorIndex(pos.getIndex());
        }
        return null;
    }
         
    /** Getter for property whenNull.
     * @return Value of property whenNull.
     *
     */
    public java.lang.String getWhenNull() {
        return whenNull;
    }
        
    /** Getter for property whenNotNull.
     * @return Value of property whenNotNull.
     *
     */
    public java.lang.String getWhenNotNull() {
        return whenNotNull;
    }
        
}
