package simple.text;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParsePosition;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
/**
 * Generate the division.
 * @author yhe
 *
 */
public class DivideNumberFormat extends AbstractPatternedFormat {    
    
	private static final long serialVersionUID = 876103106099L;
	protected DecimalFormat dFormat;
    public DivideNumberFormat(String pattern) {
        super(pattern);
        dFormat=new DecimalFormat(pattern);
    }
    
    public StringBuffer format(Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos) {
    	BigDecimal[] numbers;
        try {
        	numbers = ConvertUtils.convert(BigDecimal[].class, obj);
        } catch(ConvertException ce) {
        	return toAppendTo;
        }
        if(numbers!=null){
        	if(numbers.length==1){
        		if(numbers[0]!=null){
        			toAppendTo.append(dFormat.format(getInfinity(numbers[0]))); 
        		}else{
        			toAppendTo.append("");
        		}
	        }else if(numbers.length==2){
	        	if(numbers[1]!=null){
	        		if(numbers[1].compareTo(BigDecimal.ZERO)==0){
		        		if(numbers[0]!=null){
		        			toAppendTo.append(dFormat.format(getInfinity(numbers[0]))); 
		        		}else{
		        			toAppendTo.append("");
		        		}
	        		}else{
	        			if(numbers[0]!=null){
	        				toAppendTo.append(dFormat.format(numbers[0].divide(numbers[1], dFormat.getMaximumFractionDigits()+(int)Math.log10(dFormat.getMultiplier()), RoundingMode.HALF_UP))); 
	            		}else{
	            			toAppendTo.append("");
	            		}
	        		}
	        	}else{
	        		if(numbers[0]!=null){
	        			toAppendTo.append(dFormat.format(getInfinity(numbers[0]))); 
	        		}else{
	        			toAppendTo.append("");
	        		}
	        	}
	        }
        }else{
        	toAppendTo.append("");
        }
        return toAppendTo;
    }
    
    public double getInfinity(BigDecimal number){
    	if(number.compareTo(BigDecimal.ZERO)>-1){
    		return Double.POSITIVE_INFINITY;
    	}else{
    		return Double.NEGATIVE_INFINITY;
    	}
    }
    
	public Object parseObject(String source, ParsePosition pos) {
		return null;
	}  
}
