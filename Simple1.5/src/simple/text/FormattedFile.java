package simple.text;

import static simple.text.MessageFormat.format;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import simple.bean.ConvertUtils;
import simple.text.FormattedFile.RowDefinition.FieldDefinition;
import simple.text.FormattedFile.Row.Field;
import simple.text.StringUtils;

/**
 * This class can write both delimited and fixed-width format, or a combination of the 2.
 * 
 * Warning: Instances of this class are NOT thread safe and the iterators are fail-fast.
 */
public class FormattedFile implements Serializable, Iterable<FormattedFile.Row> {
	
	private static final long serialVersionUID = 1L;

	private final RowFormat rowFormat;
	private final String fieldSeparator;
	private final char fieldPadding;
	private final String rowTerminator;

	private List<String> rawHeaders = new ArrayList<>();
	private List<String> rawFooters = new ArrayList<>();
	
	private List<RowDefinition> headerDefinitions = new ArrayList<>();
	private List<Row> headerRows = new ArrayList<>();

	private List<RowDefinition> footerDefinitions = new ArrayList<>();
	private List<Row> footerRows = new ArrayList<>();

	private RowDefinition dataDefinition;
	private List<Row> dataRows = new ArrayList<>();
	
	private boolean validated = false;
	
	private RowDefinition currentHeaderDefinition;
	private RowDefinition currentFooterDefinition;

	private Row currentHeader;
	private int currentHeaderDefinitionIndex;
	
	private Row currentFooter;
	private int currentFooterDefinitionIndex;

	private Row currentData;
	
	public FormattedFile(RowFormat rowFormat, String fieldSeparator, String rowTerminator) {
		this(rowFormat, ' ', fieldSeparator, rowTerminator);
	}
	
	public FormattedFile(RowFormat rowFormat, char fieldPadding, String rowTerminator) {
		this(rowFormat, fieldPadding, "", rowTerminator);
	}
	
	public FormattedFile(RowFormat rowFormat, char fieldPadding, String fieldSeparator, String rowTerminator) {
		this.rowFormat = rowFormat;
		this.fieldPadding = fieldPadding;
		this.fieldSeparator = fieldSeparator;
		this.rowTerminator = rowTerminator;
	}
	
	public RowFormat getRowFormat() {
		return rowFormat;
	}

	public String getFieldSeparator() {
		return fieldSeparator;
	}
	
	public char getFieldPadding() {
		return fieldPadding;
	}

	public String getRowTerminator() {
		return rowTerminator;
	}
	
	public boolean isValidated() {
		return validated;
	}
	
	private void needsValidation() {
		validated = false;
	}
	
	public void validateDefinition() throws ValidationException {
		if (validated)
			return;
		
		if (!hasHeaders() && !hasData()  && !hasFooters())
			throw new ValidationException("empty file");
		
//		if (getTotalRowCount() == 0)
//			throw new ValidationException("empty file");
		
		if (hasHeaders()) {
			for(RowDefinition headerDefinition : headerDefinitions)
				headerDefinition.validate();
		}

		if (hasData()) {
			dataDefinition.validate();
		}

		if (hasFooters()) {
			for(RowDefinition footerDefinition : footerDefinitions)
				footerDefinition.validate();
		}
		
		validated = true;
	}
	
	public boolean hasRawHeader() {
		return hasRawHeaders();
	}

	public boolean hasRawHeaders() {
		return !rawHeaders.isEmpty();
	}

	public String getRawHeader() {
		return getRawHeader(0);
	}

	public String getRawHeader(int number) {
		try {
			return rawHeaders.get(number);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalStateException(format("raw header {0} is not defined", number));
		}
	}
	
	public List<String> getRawHeaders() {
		return rawHeaders;
	}
	
	public void addRawHeader(String data) {
		rawHeaders.add(data);
	}
	
	public void setRawHeader(int index, String data) {
		rawHeaders.set(index, data);
	}
	
	public boolean hasRawFooter() {
		return hasRawFooters();
	}

	public boolean hasRawFooters() {
		return !rawFooters.isEmpty();
	}

	public String getRawFooter() {
		return getRawFooter(0);
	}

	public String getRawFooter(int number) {
		try {
			return rawFooters.get(number);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalStateException(format("raw footer {0} is not defined", number));
		}
	}
	
	public List<String> getRawFooters() {
		return rawFooters;
	}
	
	public void addRawFooter(String data) {
		rawFooters.add(data);
	}
	
	public void setRawFooter(int index, String data) {
		rawFooters.set(index, data);
	}

	public boolean hasHeader() {
		return hasHeaders();
	}

	public boolean hasHeaders() {
		return !headerDefinitions.isEmpty();
	}

	public RowDefinition getHeaderDefinition() {
		return getHeaderDefinition(0);
	}

	public RowDefinition getHeaderDefinition(int number) {
		try {
			return headerDefinitions.get(number);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalStateException(format("header {0} is not defined", number));
		}
	}
	
	public List<RowDefinition> getHeaderDefinitions() {
		return headerDefinitions;
	}
	
	public RowDefinition defineNextHeader() {
		RowDefinition rd = new RowDefinition();
		headerDefinitions.add(rd);
		currentHeaderDefinition = rd;
		needsValidation();
		return rd;
	}
	
	public RowDefinition defineHeader() {
		if (currentHeaderDefinition == null) {
			defineNextHeader();
		}
		return currentHeaderDefinition;
	}
	
	public Row nextHeader() {
		if (currentHeaderDefinitionIndex >= headerDefinitions.size())
			throw new IllegalStateException(format("header {0} is not defined", currentHeaderDefinitionIndex));

		RowDefinition rd = getHeaderDefinition(currentHeaderDefinitionIndex++);
		currentHeader = new Row(rd);
		return currentHeader;
	}
	
	public Row header() {
		if (currentHeader == null) {
			nextHeader();
		}
		return currentHeader;
	}

	public void commitHeader() throws ValidationException {
		commitHeader(false);
	}

	public void commitHeader(boolean force) throws ValidationException {
		if (currentHeader == null)
			throw new IllegalStateException("header not started");
		
		try {
			currentHeader.validate();
		} catch (ValidationException e) {
			if (!force)
				throw e;
		}
		
		headerRows.add(currentHeader);
		
		if (currentHeaderDefinitionIndex < headerDefinitions.size())
			nextHeader();
	}
	
	public boolean hasFooter() {
		return hasFooters();
	}

	public boolean hasFooters() {
		return !footerDefinitions.isEmpty();
	}

	public RowDefinition getFooterDefinition() {
		return getFooterDefinition(0);
	}

	public RowDefinition getFooterDefinition(int number) {
		try {
			return footerDefinitions.get(number);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalStateException(format("footer {0} is not defined", number));
		}
	}
	
	public List<RowDefinition> getFooterDefinitions() {
		return footerDefinitions;
	}
	
	public RowDefinition defineNextFooter() {
		RowDefinition rd = new RowDefinition();
		footerDefinitions.add(rd);
		currentFooterDefinition = rd;
		needsValidation();
		return rd;
	}
	
	public RowDefinition defineFooter() {
		if (currentFooterDefinition == null) {
			defineNextFooter();
		}
		return currentFooterDefinition;
	}
	
	public Row nextFooter() {
		if (currentFooterDefinitionIndex >= footerDefinitions.size())
			throw new IllegalStateException(format("footer {0} is not defined", currentFooterDefinitionIndex));

		RowDefinition rd = getFooterDefinition(currentFooterDefinitionIndex++);
		currentFooter = new Row(rd);
		return currentFooter;
	}
	
	public Row footer() {
		if (currentFooter == null) {
			nextFooter();
		}
		return currentFooter;
	}

	public void commitFooter() throws ValidationException {
		commitFooter(false);
	}

	public void commitFooter(boolean force) throws ValidationException {
		if (currentFooter == null)
			throw new IllegalStateException("footer not started");
		
		try {
			currentFooter.validate();
		} catch (ValidationException e) {
			if (!force)
				throw e;
		}
		
		footerRows.add(currentFooter);
		
		if (currentFooterDefinitionIndex < footerDefinitions.size())
			nextFooter();
	}
	
	public boolean hasData() {
		return dataDefinition != null;
	}

	public RowDefinition getDataDefinition() {
		if (dataDefinition == null)
			throw new IllegalStateException(format("data is not defined"));
		
		return dataDefinition;
	}
	
	public RowDefinition defineData() {
		if (dataDefinition == null) {
			dataDefinition = new RowDefinition();
		}
		return dataDefinition;
	}
	
	public Row getDataRow(int number) {
		if (dataDefinition == null)
			throw new IllegalStateException("data not defined");
	
		return dataRows.get(number);
	}
	

	
	public Row nextData() {
		currentData = new Row(dataDefinition);
		return currentData;
	}
	
	public Row data() {
		if (currentData == null) {
			nextData();
		}
		return currentData;
	}

	public void commitData() throws ValidationException {
		commitData(false);
	}

	public void commitData(boolean force) throws ValidationException {
		if (currentData == null)
			throw new IllegalStateException("data not started");
		
		dataDefinition.validate();

		try {
			currentData.validate();
		} catch (ValidationException e) {
			if (!force)
				throw e;
		}
		
		dataRows.add(currentData);
		
		nextData();
	}

	public int getDataRowCount() {
		return dataRows.size();
	}

	public int getTotalRowCount() {
		return dataRows.size() + headerDefinitions.size() + footerDefinitions.size();
	}
	
	@Override
	public Iterator<FormattedFile.Row> iterator() {
		return dataRows.iterator();
	}
	
	public Appendable appendTo(Appendable appendable) throws IOException {
		if (hasHeaders() && headerRows.isEmpty())
			throw new IllegalStateException("header is defined but not populated");
		
		if (hasFooter() && footerRows.isEmpty())
			throw new IllegalStateException("footer is defined but not populated");
		
		if (hasRawHeaders()) 
			appendStrings(appendable, rawHeaders);

		if(hasHeaders())
			appendRows(appendable, headerRows);

		appendRows(appendable, dataRows);

		if(hasFooter())
			appendRows(appendable, footerRows);
		
		if (hasRawFooters()) 
			appendStrings(appendable, rawFooters);

		return appendable;
	}
	
	private void appendStrings(Appendable appendable, Collection<String> rows) throws IOException {
		for(String row : rows) {
			appendable.append(row);
			appendable.append(rowTerminator);
		}
	}
	
	private void appendRows(Appendable appendable, Collection<Row> rows) throws IOException {
		for(Row row : rows) {
			row.appendTo(appendable);
		}
	}

	@Override
	public String toString() {
		return String.format("FormattedFile [rowFormat=%s, fieldSeparator=%s, rowTerminator=%s]", rowFormat, fieldSeparator, rowTerminator);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataDefinition == null) ? 0 : dataDefinition.hashCode());
		result = prime * result + ((dataRows == null) ? 0 : dataRows.hashCode());
		result = prime * result + ((fieldSeparator == null) ? 0 : fieldSeparator.hashCode());
		result = prime * result + ((footerDefinitions == null) ? 0 : footerDefinitions.hashCode());
		result = prime * result + ((footerRows == null) ? 0 : footerRows.hashCode());
		result = prime * result + ((headerDefinitions == null) ? 0 : headerDefinitions.hashCode());
		result = prime * result + ((headerRows == null) ? 0 : headerRows.hashCode());
		result = prime * result + ((rawFooters == null) ? 0 : rawFooters.hashCode());
		result = prime * result + ((rawHeaders == null) ? 0 : rawHeaders.hashCode());
		result = prime * result + ((rowFormat == null) ? 0 : rowFormat.hashCode());
		result = prime * result + ((rowTerminator == null) ? 0 : rowTerminator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		FormattedFile other = (FormattedFile)obj;
		if(dataDefinition == null) {
			if(other.dataDefinition != null)
				return false;
		} else if(!dataDefinition.equals(other.dataDefinition))
			return false;
		if(dataRows == null) {
			if(other.dataRows != null)
				return false;
		} else if(!dataRows.equals(other.dataRows))
			return false;
		if(fieldSeparator == null) {
			if(other.fieldSeparator != null)
				return false;
		} else if(!fieldSeparator.equals(other.fieldSeparator))
			return false;
		if(footerDefinitions == null) {
			if(other.footerDefinitions != null)
				return false;
		} else if(!footerDefinitions.equals(other.footerDefinitions))
			return false;
		if(footerRows == null) {
			if(other.footerRows != null)
				return false;
		} else if(!footerRows.equals(other.footerRows))
			return false;
		if(headerDefinitions == null) {
			if(other.headerDefinitions != null)
				return false;
		} else if(!headerDefinitions.equals(other.headerDefinitions))
			return false;
		if(headerRows == null) {
			if(other.headerRows != null)
				return false;
		} else if(!headerRows.equals(other.headerRows))
			return false;
		if(rawFooters == null) {
			if(other.rawFooters != null)
				return false;
		} else if(!rawFooters.equals(other.rawFooters))
			return false;
		if(rawHeaders == null) {
			if(other.rawHeaders != null)
				return false;
		} else if(!rawHeaders.equals(other.rawHeaders))
			return false;
		if(rowFormat != other.rowFormat)
			return false;
		if(rowTerminator == null) {
			if(other.rowTerminator != null)
				return false;
		} else if(!rowTerminator.equals(other.rowTerminator))
			return false;
		return true;
	}
	
	// - END OF MAIN CLASS ------------------------------------------------------
	
	public static enum RowType {
		HEADER,
		DATA,
		FOOTER;
	}
	
	public static enum RowFormat {
		DELIMITED,
		FIXED_WIDTH;
	}

	public static enum DataType {
		ALPHANUMERIC('A', "Alphanumeric"),
		NUMERIC('H', "Numeric");

		private final char code;
		private final String description;

		private DataType(char code, String description) {
			this.code = code;
			this.description = description;
		}

		public char getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}
	}
	
	public static enum Justification {
		LEFT,
		CENTER,
		RIGHT;
		
		public simple.text.StringUtils.Justification getJustification() {
			return simple.text.StringUtils.Justification.valueOf(this.toString());
		}
	}
	
	// - MEMBER INNER CLASSES ---------------------------------------------------

	public class Row implements Serializable, Iterable<Row.Field> {
		
		private static final long serialVersionUID = 1L;

		private RowDefinition definition;
		private SortedMap<Integer, Field> fieldsByNumber = new TreeMap<>();
		private SortedMap<String, Field> fieldsByKey = new TreeMap<>();
		
		Row(RowDefinition definition) {
			this.definition = definition;
			
			for(FieldDefinition fieldDefinition: definition) {
				addField(new Field(fieldDefinition));
			}
		}

		public RowDefinition getDefinition() {
			return definition;
		}

		void addField(Field field) {
			fieldsByNumber.put(field.getNumber(), field);
			fieldsByKey.put(field.getKey(), field);
		}

		public Field getField(int number) {
			if (!fieldsByNumber.containsKey(number))
				throw new IllegalArgumentException(format("field number {0} is not defined", number));

			return fieldsByNumber.get(number);
		}

		public Field getField(String key) {
			if (!fieldsByKey.containsKey(key))
				throw new IllegalArgumentException(format("field key {0} is not defined", key));
			
			return fieldsByKey.get(key);
		}

		@Override
		public Iterator<Field> iterator() {
			return fieldsByNumber.values().iterator();
		}
		
		public void setValue(int number, String value) {
			getField(number).setValue(value);
		}
		
		public void setValue(String key, String value) {
			getField(key).setValue(value);
		}
		
		public Row set(String key, String value) {
			getField(key).setValue(value);
			return this;
		}

		
		public void validate() throws ValidationException {
			definition.validate(this);
		}
		
		@Override
		public String toString() {
			return StringUtils.join(fieldsByNumber.values(), fieldSeparator);
		}
		
		public Appendable appendTo(Appendable appendable) throws IOException {
			int i=0, l=fieldsByNumber.size();
			for(Field field : fieldsByNumber.values()) {
				field.appendTo(appendable);
				if (i++ < l-1) {
					if (getDefinition().getRowFormat() == RowFormat.DELIMITED) {
							appendable.append(fieldSeparator);
					}
				}
			}
			appendable.append(rowTerminator);
			return appendable;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((definition == null) ? 0 : definition.hashCode());
			result = prime * result + ((fieldsByNumber == null) ? 0 : fieldsByNumber.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			Row other = (Row)obj;
			if(definition == null) {
				if(other.definition != null)
					return false;
			} else if(!definition.equals(other.definition))
				return false;
			if(fieldsByNumber == null) {
				if(other.fieldsByNumber != null)
					return false;
			} else if(!fieldsByNumber.equals(other.fieldsByNumber))
				return false;
			return true;
		}
		
		public class Field implements Serializable {
			
			private static final long serialVersionUID = 1L;

			private final FieldDefinition definition;
			private String value;

			Field(FieldDefinition definition) {
				this.definition = definition;
			}

			public FieldDefinition getDefinition() {
				return definition;
			}
			
			public Row getRow() {
				return Row.this;
			}

			public int getNumber() {
				return definition.getNumber();
			}
			
			public String getKey() {
				return definition.getKey();
			}

			public String getValue() {
				return value;
			}

			public void setValue(String value) {
				this.value = value;
			}

			public boolean isPopulated() {
				if(value == null)
					return false;

				if(definition.getDataType() == DataType.ALPHANUMERIC) {
					return !StringUtils.isBlank(value.toString());
				} else {
					return !ConvertUtils.areSimilar(value, 0);
				}
			}
			
			@Override
			public String toString() {
				//return String.format("Field[definition={0}, value={0}]", definition, value);
				return format("{0}={1}", getKey(), value);
				//return value == null ? "" : value;
			}
			
			public Appendable appendTo(Appendable appendable) throws IOException {
				if (Row.this.getDefinition().getRowFormat() == RowFormat.DELIMITED) {
					if (value != null) {
						appendable.append(value);
					}
				} else {
					StringUtils.appendPadded(appendable, value != null ? value : "", definition.getFieldPadding(), definition.getSize(), definition.getJustification().getJustification());
				}
				return appendable;
			}

			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((definition == null) ? 0 : definition.hashCode());
				result = prime * result + ((value == null) ? 0 : value.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if(this == obj)
					return true;
				if(obj == null)
					return false;
				if(getClass() != obj.getClass())
					return false;
				Field other = (Field)obj;
				if(definition == null) {
					if(other.definition != null)
						return false;
				} else if(!definition.equals(other.definition))
					return false;
				if(value == null) {
					if(other.value != null)
						return false;
				} else if(!value.equals(other.value))
					return false;
				return true;
			}
		}
	}
	
	public class RowDefinition implements Serializable, Iterable<RowDefinition.FieldDefinition> {

		private static final long serialVersionUID = 1L;
		
		private RowFormat rowFormat = FormattedFile.this.rowFormat;
		private SortedMap<Integer, FieldDefinition> definitionsByNumber = new TreeMap<>();
		private SortedMap<String, FieldDefinition> definitionsByKey = new TreeMap<>();
		
		private boolean validated = false;

		public RowFormat getRowFormat() {
			return rowFormat;
		}
		
		public void setRowFormat(RowFormat rowFormat) {
			this.rowFormat = rowFormat;
			needsValidation();
		}
		
		public RowDefinition rowFormat(RowFormat rowFormat) {
			setRowFormat(rowFormat);
			return this;
		}
		
		public RowDefinition delimited() {
			return rowFormat(RowFormat.DELIMITED);
		}
		
		public RowDefinition fixedWidth() {
			return rowFormat(RowFormat.FIXED_WIDTH);
		}

		public boolean isValidated() {
			return validated;
		}
		
		private void needsValidation() {
			validated = false;
		}
		
		public void validate() throws ValidationException {
			if (validated)
				return;
			
			if (isEmpty())
				throw new ValidationException("empty row definition");
			
			int i=-1;
			for(FieldDefinition fieldDefinition : this) {
				if (i == -1) {
					if (fieldDefinition.getNumber() > 1)
						throw new ValidationException(format("missing column number {0}", fieldDefinition.getNumber()-1));
					i = fieldDefinition.getNumber();
				} else if (fieldDefinition.getNumber() != i) {
					throw new ValidationException(format("missing column number {0}", i));
				}
				fieldDefinition.validate();
				i++;
			}
			
			validated = true;
		}
		
		public void validate(Row row) throws ValidationException {
			for(FieldDefinition fieldDefinition : this) {
				fieldDefinition.validate(row.getField(fieldDefinition.getNumber()));
			}
		}
		
		public FieldDefinition field(String key) {
			FieldDefinition fd = definitionsByKey.get(key);
			if (fd == null) {
				int number = definitionsByNumber.size()+1;
				fd = new FieldDefinition(number, key);
				addFieldDefinition(fd);
			}
			return fd;
		}
		
		public void addFieldDefinition(FieldDefinition definition) {
			if (definitionsByNumber.containsKey(definition.getNumber()))
					throw new IllegalArgumentException(format("field number {0} is already defined", definition.getNumber()));
			if (definitionsByKey.containsKey(definition.getKey()))
					throw new IllegalArgumentException(format("field key {0} is already defined", definition.getKey()));
			
			definitionsByNumber.put(definition.getNumber(), definition);
			definitionsByKey.put(definition.getKey(), definition);
			
			needsValidation();
		}

		public FieldDefinition getFieldDefinition(int number) {
			return definitionsByNumber.get(number);
		}

		public FieldDefinition getFieldDefinition(String key) {
			return definitionsByKey.get(key);
		}

		public boolean isEmpty() {
			return definitionsByNumber.isEmpty();
		}
		
		@Override
		public Iterator<FieldDefinition> iterator() {
			return definitionsByNumber.values().iterator();
		}
		
		public int size() {
			return definitionsByNumber.size();
		}

		@Override
		public String toString() {
			return format("RowDefinition[{0}]", StringUtils.join(definitionsByNumber.values(), ","));
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((definitionsByNumber == null) ? 0 : definitionsByNumber.hashCode());
			result = prime * result + ((rowFormat == null) ? 0 : rowFormat.hashCode());
			result = prime * result + (validated ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			RowDefinition other = (RowDefinition)obj;
			if(!getOuterType().equals(other.getOuterType()))
				return false;
			if(definitionsByNumber == null) {
				if(other.definitionsByNumber != null)
					return false;
			} else if(!definitionsByNumber.equals(other.definitionsByNumber))
				return false;
			if(rowFormat != other.rowFormat)
				return false;
			if(validated != other.validated)
				return false;
			return true;
		}
		
		public class FieldDefinition implements Serializable {
			
			private static final long serialVersionUID = 1L;

			private final int number;
			private final String key;
			
			private int size;
			private Justification justification = Justification.LEFT;
			private char fieldPadding = FormattedFile.this.fieldPadding;
			private DataType dataType = DataType.ALPHANUMERIC;
			private List<Requirement> requirements = new ArrayList<>();

			private boolean validated = false;

			FieldDefinition(int number, String key) {
				if (key == null)
					throw new NullPointerException("key");
				if (number < 0) 
					throw new IllegalArgumentException(Integer.toString(number));
				
				this.number = number;
				this.key = key;
//				
//				this.size = size;
//				this.justification = justification;
//				this.dataType = dataType;
//				Collections.addAll(this.requirements, requirements);
			}
		
			public int getNumber() {
				return number;
			}
	
			public String getKey() {
				return key;
			}

			public int getSize() {
				return size;
			}

			public void setSize(int size) {
				this.size = size;
				needsValidation();
			}
			
			public FieldDefinition size(int size) {
				setSize(size);
				return this;
			}

			public Justification getJustification() {
				return justification;
			}

			public void setJustification(Justification justification) {
				this.justification = justification;
				needsValidation();
			}
			
			public FieldDefinition justification(Justification justification) {
				setJustification(justification);
				return this;
			}
			
			public FieldDefinition leftJustified() {
				return justification(Justification.LEFT);
			}
			
			public FieldDefinition centerJustified() {
				return justification(Justification.CENTER);
			}

			public FieldDefinition rightJustified() {
				return justification(Justification.RIGHT);
			}
			
			public char getFieldPadding() {
				return fieldPadding;
			}

			public void setFieldPadding(char fieldPadding) {
				this.fieldPadding = fieldPadding;
				needsValidation();
			}
			
			public FieldDefinition padWith(char fieldPadding) {
				setFieldPadding(fieldPadding);
				return this;
			}
			
			public FieldDefinition spacePadded() {
				return padWith(' ');
			}
			
			public FieldDefinition zeroPadded() {
				return padWith('0');
			}
			
			public FieldDefinition numericRightZero() {
				return numeric().rightJustified().zeroPadded();
			}

			public DataType getDataType() {
				return dataType;
			}

			public void setDataType(DataType dataType) {
				this.dataType = dataType;
				needsValidation();
			}
			
			public FieldDefinition dataType(DataType dataType) {
				setDataType(dataType);
				return this;
			}
			
			public FieldDefinition alphanumeric() {
				return dataType(DataType.ALPHANUMERIC);
			}

			public FieldDefinition numeric() {
				return dataType(DataType.NUMERIC);
			}

			public List<Requirement> getRequirements() {
				return requirements;
			}

			public void setRequirements(List<Requirement> requirements) {
				this.requirements = requirements;
				needsValidation();
			}
			
			public void addRequirement(Requirement requirement) {
				requirements.add(requirement);
				needsValidation();
			}
			
			public FieldDefinition requirement(Requirement requirement) {
				addRequirement(requirement);
				return this;
			}
			
			public FieldDefinition required() {
				return requirement(REQUIRED);
			}
			
			public FieldDefinition requiredIfEquals(String testField, String testValue) {
				return requirement(new RequiredIfEquals(testField, testValue));
			}
			
			public FieldDefinition requiredIfPopulated(String populatedField) {
				return requirement(new RequiredIfPopulated(populatedField));
			}
			
			public boolean isValidated() {
				return validated;
			}
			
			private void needsValidation() {
				validated = false;
			}
			
			public void validate() throws ValidationException {
				if (validated)
					return;
				
				if (size < 0)
					throw new ValidationException(format("field {0} has invalid size {1}", key, size));
				if (RowDefinition.this.getRowFormat() == RowFormat.FIXED_WIDTH && size == 0)
					throw new ValidationException(format("size of field {0} is required for fixed width format", key));
			}

			public void validate(Field field) throws ValidationException {
				for(Requirement requirement : requirements)
					requirement.check(field);
			}
			
			@Override
			public String toString() {
				return format("{0}({1})[{2},{3},{4},{5}]", number, key, dataType, size, justification, StringUtils.join(requirements, ","));
			}

			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + getOuterType().hashCode();
				result = prime * result + ((dataType == null) ? 0 : dataType.hashCode());
				result = prime * result + ((justification == null) ? 0 : justification.hashCode());
				result = prime * result + ((key == null) ? 0 : key.hashCode());
				result = prime * result + number;
				result = prime * result + ((requirements == null) ? 0 : requirements.hashCode());
				result = prime * result + size;
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if(this == obj)
					return true;
				if(obj == null)
					return false;
				if(getClass() != obj.getClass())
					return false;
				FieldDefinition other = (FieldDefinition)obj;
				if(!getOuterType().equals(other.getOuterType()))
					return false;
				if(dataType != other.dataType)
					return false;
				if(justification != other.justification)
					return false;
				if(key == null) {
					if(other.key != null)
						return false;
				} else if(!key.equals(other.key))
					return false;
				if(number != other.number)
					return false;
				if(requirements == null) {
					if(other.requirements != null)
						return false;
				} else if(!requirements.equals(other.requirements))
					return false;
				if(size != other.size)
					return false;
				return true;
			}

			private RowDefinition getOuterType() {
				return RowDefinition.this;
			}
		}

		private FormattedFile getOuterType() {
			return FormattedFile.this;
		}
	}
	
	// - STATIC INNER CLASSES ---------------------------------------------------
	
	public interface Requirement {
		void check(Field field) throws ValidationException;
	}

	public static final Requirement REQUIRED = new Requirement() {
		@Override
		public void check(Field field) throws ValidationException {
			if (!field.isPopulated())
				throw new ValidationException(format("{0} is required", field.getKey()));
		}
		
		@Override
		public String toString() {
			return "REQUIRED";
		}
	};

	public static final Requirement NOT_REQUIRED = new Requirement() {
		@Override
		public void check(Field field) throws ValidationException {
		}
		
		@Override
		public String toString() {
			return "NOT_REQUIRED";
		}
	};

	public static class RequiredIfPopulated implements Serializable, Requirement {
		
		private static final long serialVersionUID = 1L;

		private String populatedField;

		private RequiredIfPopulated(String populatedField) {
			this.populatedField = populatedField;
		}

		public String getPopulatedField() {
			return populatedField;
		}

		@Override
		public void check(Field field) throws ValidationException {
			if (field.getRow().getField(populatedField).isPopulated() && !field.isPopulated())
				throw new ValidationException(format("{0} is required if {1} is populated", field.getKey(), populatedField));
		}

		@Override
		public String toString() {
			return format("RequiredIfPopulated[{0}]", populatedField);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((populatedField == null) ? 0 : populatedField.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			RequiredIfPopulated other = (RequiredIfPopulated)obj;
			if(populatedField == null) {
				if(other.populatedField != null)
					return false;
			} else if(!populatedField.equals(other.populatedField))
				return false;
			return true;
		}
	}

	public static class RequiredIfEquals implements Serializable, Requirement {
		
		private static final long serialVersionUID = 1L;

		private String testField;
		private String testValue;

		private RequiredIfEquals(String testField, String testValue) {
			this.testField = testField;
			this.testValue = testValue;
		}

		public String getTestField() {
			return testField;
		}

		public String getTestValue() {
			return testValue;
		}

		@Override
		public void check(Field field) throws ValidationException {
			Field f = field.getRow().getField(testField);
			if (!f.isPopulated() || !f.getValue().equals(testValue))
				return;
			
			if (!field.isPopulated())
				throw new ValidationException(format("{0} is required when {1} equals {2}", field.getKey(), testField, testValue));
		}

		@Override
		public String toString() {
			return format("RequiredIfEquals[{0},{1}]", testField, testValue);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((testField == null) ? 0 : testField.hashCode());
			result = prime * result + ((testValue == null) ? 0 : testValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			RequiredIfEquals other = (RequiredIfEquals)obj;
			if(testField == null) {
				if(other.testField != null)
					return false;
			} else if(!testField.equals(other.testField))
				return false;
			if(testValue == null) {
				if(other.testValue != null)
					return false;
			} else if(!testValue.equals(other.testValue))
				return false;
			return true;
		}
	}
	
	public static class ValidationException extends Exception
	{
		private static final long serialVersionUID = 1L;

		public ValidationException()
		{
			super();
		}

		public ValidationException(String msg)
		{
			super(msg);
		}

		public ValidationException(String msg, Exception e)
		{
			super(msg, e);
		}
	}
}
