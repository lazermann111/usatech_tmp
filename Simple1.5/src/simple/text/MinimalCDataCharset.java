package simple.text;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import simple.bean.ConvertUtils;
import simple.io.Log;

public class MinimalCDataCharset extends Charset {
	private static final Log log = Log.getLog();
	protected static final Map<String, Byte> entities = new HashMap<String, Byte>();
	static {
		try {
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			if(cl == null) {
				cl = MinimalCDataCharset.class.getClassLoader();
				if(cl == null)
					cl = ClassLoader.getSystemClassLoader();
			}
			Properties props = new Properties();
			try (InputStream in = cl.getResourceAsStream("simple/xml/serializer/HTMLEntities.properties")) {
				if(in == null)
					throw new IOException("File 'HTMLEntities.properties' not found");
				props.load(in);
			}
			for(String key : props.stringPropertyNames()) {
				int b = ConvertUtils.getIntSafely(props.getProperty(key), -1);
				if(b < 256 && b >= 0)
					entities.put(key, (byte) b);
			}
			log.debug("Loaded entites from 'HTMLEntities.properties'");
		} catch(IOException e) {
			log.warn("Could not load entities", e);
			entities.put("quot", (byte) 34);
			entities.put("amp", (byte) 38);
			entities.put("lt", (byte) 60);
			entities.put("gt", (byte) 62);
			entities.put("nbsp", (byte) 160);
		}
	}
	protected final BitSet escapeSet = new BitSet(256);
	public MinimalCDataCharset() {
		super("CDATA", null);
		escapeSet.set(0, 9);
		escapeSet.set(11, 13);
		escapeSet.set(14, 32);
		escapeSet.set((byte) '&');
		escapeSet.set((byte) '<');
		escapeSet.set((byte) '>');
		escapeSet.set((byte) '\'');
		escapeSet.set((byte) '"');
		escapeSet.set(128, 256);
	}

	@Override
	public boolean contains(Charset cs) {
		return false;
	}

	@Override
	public CharsetDecoder newDecoder() {
		return new CharsetDecoder(this, 1.0f, 7.0f) {
			@Override
			protected CoderResult decodeLoop(ByteBuffer in, CharBuffer out) {
				while(in.hasRemaining() && out.hasRemaining()) {
					byte b = in.get();
					if(needsEncoding(b)) {
						if(out.remaining() < 6) {
							in.position(in.position() - 1);
							return CoderResult.OVERFLOW;
						}
						out.put('&');
						out.put('#');
						out.put(Integer.toString(b & 0xff));
						out.put(';');
					} else {
						out.put((char) (b & 0xff));
					}
				}
				return CoderResult.UNDERFLOW;
			}
		};
	}

	protected boolean needsEncoding(byte b) {
		return escapeSet.get(b & 0xFF);
	}

	@Override
	public CharsetEncoder newEncoder() {
		return new CharsetEncoder(this, 1.0f, 7.0f) {
			protected int search = 0;

			protected CoderResult handleEntity(CharBuffer in, ByteBuffer out) {
				for(; search < in.remaining(); search++) {
					if(in.charAt(search) == ';') {
						if(search == 0) {
							in.get();
							return CoderResult.malformedForLength(in.position() - 1);
						}
						if(in.charAt(0) == '#') {
							in.get();
							char[] carr = new char[search - 1];
							in.get(carr);
							int value;
							try {
								value = Integer.parseInt(new String(carr));
							} catch(NumberFormatException e) {
								return CoderResult.unmappableForLength(in.position() - search - 1);
							}
							if(value < 128)
								out.put((byte) value);
							else
								return CoderResult.malformedForLength(in.position() - search - 1);
						} else {
							// lookup entity
							char[] carr = new char[search];
							in.get(carr);
							String entity = new String(carr);
							Byte b = entities.get(entity);
							if(b == null)
								return CoderResult.unmappableForLength(in.position() - search - 1);
							out.put(b);
						}
						in.get(); // consume the ';'
						search = 0;
						return null;
					}
				}
				return CoderResult.UNDERFLOW;
			}
			@Override
			protected CoderResult encodeLoop(CharBuffer in, ByteBuffer out) {
				while(in.hasRemaining()) {
					if(search > 0) {
						CoderResult result = handleEntity(in, out);
						if(result != null)
							return result;
					}
					char c = in.get();
					if(c == '&') {
						CoderResult result = handleEntity(in, out);
						if(result != null)
							return result;
					} else if(c < 128)
						out.put((byte)c);
					else
						return CoderResult.malformedForLength(in.position() - 1);
				}
				return CoderResult.UNDERFLOW;
			}
		};
	}

	public boolean isEscaped(int ch) {
		return escapeSet.get(ch);
	}

	public void setEscaped(int ch, boolean escape) {
		escapeSet.set(ch, escape);
	}
}
