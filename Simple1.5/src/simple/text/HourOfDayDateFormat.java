package simple.text;

import java.text.ParsePosition;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
/**
 * Format to convert number of hour 0-23 to text.
 * @author yhe
 *
 */
public class HourOfDayDateFormat extends AbstractPatternedFormat {    
    
	private static final long serialVersionUID = 876103106098L;
    public HourOfDayDateFormat(String pattern) {
        super(pattern);
    }
    
    public StringBuffer format(Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos) {
        int hour;
        try {
        	hour = ConvertUtils.convert(int.class, obj);
        } catch(ConvertException ce) {
        	toAppendTo.append("");
        	return toAppendTo;
        }
        if(hour==0){
        	toAppendTo.append("12 AM");
        }else if(hour>0&&hour<12){
        	toAppendTo.append(hour+" AM");
        }else if(hour==12){
        	toAppendTo.append("12 PM");
        }else if(hour>12&&hour<24){
        	toAppendTo.append((hour-12)+" PM");
        }else{
        	toAppendTo.append("");
        }
        return toAppendTo;
    }
   
	public Object parseObject(String source, ParsePosition pos) {
		return null;
	}  
}
