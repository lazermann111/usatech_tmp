/*
 * Created on Feb 10, 2005
 *
 */
package simple.text;

import java.text.FieldPosition;
import java.text.ParsePosition;


/**
 * @author bkrug
 *
 */
public class PrepareFormat extends AbstractPatternedFormat {
    /**
	 * 
	 */
	private static final long serialVersionUID = 80905725785825L;

	public PrepareFormat(String preparations) {
        super(preparations);
    }

    public Object parseObject(String source, ParsePosition pos) {
        // TODO I can do this
        return null;
    }

    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        toAppendTo.append(StringUtils.prepareString((obj == null ? "" : obj.toString()), pattern));
        return toAppendTo;
    }
}
