package simple.text;


public class PositionedStringBuilder implements Appendable {
	protected final StringBuilder delegate;
	protected int start;
	protected int end;
	protected final PositionedStringBuilder parent;

	public PositionedStringBuilder(StringBuilder delegate, int start, int end) {
		this(null, delegate, start, end);
	}

	protected PositionedStringBuilder(PositionedStringBuilder parent, StringBuilder delegate, int start, int end) {
		this.delegate = delegate;
		this.start = start;
		this.end = end;
		this.parent = parent;
	}

	@Override
	public PositionedStringBuilder append(CharSequence csq) {
		if(csq != null) {
			delegate.insert(end, csq);
			updateEnd(csq.length());
		}
		return this;
	}

	@Override
	public PositionedStringBuilder append(CharSequence csq, int start, int end) {
		if(csq != null) {
			delegate.insert(this.end, csq, start, end);
			updateEnd(end - start);
		}
		return this;
	}

	@Override
	public PositionedStringBuilder append(char c) {
		delegate.insert(end, c);
		updateEnd(1);
		return this;
	}

	public PositionedStringBuilder prepend(CharSequence csq) {
		if(csq != null) {
			delegate.insert(start, csq);
			updateEnd(csq.length());
		}
		return this;
	}

	public PositionedStringBuilder prepend(CharSequence csq, int start, int end) {
		if(csq != null) {
			delegate.insert(this.start, csq, start, end);
			updateEnd(end - start);
		}
		return this;
	}

	public PositionedStringBuilder prepend(char c) {
		delegate.insert(start, c);
		updateEnd(1);
		return this;
	}

	public PositionedStringBuilder sub() {
		return new PositionedStringBuilder(this, delegate, end, end);
	}

	protected void updateEnd(int n) {
		this.end += n;
		if(parent != null)
			parent.updateEnd(n);
	}
}
