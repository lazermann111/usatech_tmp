/**
 *
 */
package simple.text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian S. Krug
 *
 */
public class DelimitedParser extends AbstractDelimitedParser {
	protected final List<DelimitedColumn> columns = new ArrayList<DelimitedColumn>();
	protected DelimitedColumn firstColumn;
	
	public DelimitedParser() {
	}

	public void addColumn(String label) {
		addColumn(label, null);
	}
	public void addColumn(String label, Class<?> type) {
		addColumn(new DelimitedColumn(label, type));
	}
	public void addColumn(DelimitedColumn column) {
		if(firstColumn == null)
			firstColumn = column;
		else
			columns.add(column);
	}
	@Override
	protected String[] getColumnLabels() throws IOException {
		if(firstColumn == null)
			throw new IOException("No columns were configured");
		String[] columnLabels = new String[columns.size() + 1];
		columnLabels[0] = firstColumn.getLabel();
		int i = 1;
		for(DelimitedColumn column : columns) {
			columnLabels[i++] = column.getLabel();
		}
		return columnLabels;
	}
	
	@Override
	protected DelimitedColumn getFirstColumn() throws IOException {
		if(firstColumn == null)
			throw new IOException("No columns were configured");
		return firstColumn;
	}
	@Override
	protected List<DelimitedColumn> getRemainingColumns(Object selectorValue) throws IOException {
		return columns;
	}
}
