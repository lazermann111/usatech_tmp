/*
 * Created on Feb 16, 2005
 *
 */
package simple.text;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

/**
 * @author bkrug
 *
 */
public class NullSafeFormat extends Format {
    /**
	 * 
	 */
	private static final long serialVersionUID = 84566269901223L;
	protected Format delegate;
    
    public NullSafeFormat(Format delegate) {
        this.delegate = delegate;
    }
    /* (non-Javadoc)
     * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
     */
    public Object parseObject(String source, ParsePosition pos) {
        if(source == null || source.length() == 0) return null;
        if(delegate == null) return source;
        return delegate.parseObject(source, pos);
    }

    /* (non-Javadoc)
     * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer, java.text.FieldPosition)
     */
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        if(obj != null) {
            if(delegate != null) toAppendTo = delegate.format(obj, toAppendTo, pos);
            else toAppendTo.append(obj);
        }
        return toAppendTo;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof NullSafeFormat 
            && delegate.equals(((NullSafeFormat)obj).delegate);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return delegate.hashCode() * 3;
    } 
      
}
