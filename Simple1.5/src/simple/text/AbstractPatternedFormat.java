/*
 * Created on Jul 21, 2005
 *
 */
package simple.text;

import java.text.Format;

public abstract class AbstractPatternedFormat extends Format {
    private static final long serialVersionUID = 2042499263874443510L;
	protected final String pattern;
    public AbstractPatternedFormat(String pattern) {
        super();
        this.pattern = pattern;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof AbstractPatternedFormat
            && getClass().equals(obj.getClass())
            && toPattern().equals(((AbstractPatternedFormat)obj).toPattern());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return toPattern().hashCode();
    }

    public String toPattern() {
        return pattern;
    }

    @Override
	public String toString() {
        return getClass().getName() + ":" + toPattern();
    }

}
