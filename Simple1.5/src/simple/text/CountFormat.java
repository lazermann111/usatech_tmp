/*
 * MatchFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

/**
 * Counts the number of times the specified regex is found and returns it formatted using the specified DecimalFormat
 *
 * @author Brian S. Krug
 */
public class CountFormat extends AbstractPatternedFormat {
	private static final long serialVersionUID = 8243241999001432L;
	protected final Pattern lookFor;
	protected final Format subFormat;
        
    /** Creates a new instance of MatchFormat */
    public CountFormat(String pattern) {
        super(pattern);
		String[] pairs = StringUtils.splitQuotesAnywhere(pattern, ';', '\'', true);
		switch(pairs.length) {
			case 0:
				this.lookFor = Pattern.compile(".");
				this.subFormat = null;
				break;
			case 1:
				this.lookFor = Pattern.compile(pairs[0]);
				this.subFormat = null;
				break;
			case 2:
				this.lookFor = Pattern.compile(pairs[0]);
				if(pairs[1].indexOf(':') > 0)
					this.subFormat = ConvertUtils.getFormat(pairs[1]);
				else
					this.subFormat = new DecimalFormat(pairs[1]);
				break;
			default:
				this.lookFor = Pattern.compile(pairs[0]);
				if(pairs[1].indexOf(':') > 0)
					this.subFormat = ConvertUtils.getFormat(StringUtils.join(pairs, ";", 1, pairs.length - 1));
				else
					this.subFormat = new DecimalFormat(StringUtils.join(pairs, ";", 1, pairs.length - 1));
				break;
		}
    }
    
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        String s;
        try {
            s = ConvertUtils.convert(String.class, obj);
        } catch(ConvertException ce) {
            s = obj == null ? "" : obj.toString();
        }
        if(s == null) s = "";
		Matcher m = lookFor.matcher(s);
		int c = 0;
		while(m.find())
			c++;
		if(subFormat == null)
			toAppendTo.append(c);
		else
			subFormat.format(c, toAppendTo, new FieldPosition(0));
		return toAppendTo;
    }
    
    public Object parseObject(String source, java.text.ParsePosition pos) {
        return null;
    }
    
}
