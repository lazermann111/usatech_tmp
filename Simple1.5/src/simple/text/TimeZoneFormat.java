/*
 * TimeZoneFormat.java
 *
 * Created on March 26, 2002, 1:13 PM
 */

package simple.text;

/**
 *
 * @author  bkrug
 * @version 
 */
public class TimeZoneFormat extends java.text.Format {
	private static final long serialVersionUID = 1990970901693096134L;
	private static java.text.NumberFormat hf = new java.text.DecimalFormat("+00;-00");
	private static java.text.NumberFormat mf = new java.text.DecimalFormat("00");
	/** Creates new TimeZoneFormat */
    public TimeZoneFormat() {
    }

    public java.lang.StringBuffer format(java.lang.Object obj, java.lang.StringBuffer toAppendTo, java.text.FieldPosition pos) {
	    int tzoffset;
	    if(obj instanceof Number) tzoffset = ((Number)obj).intValue();
	    else tzoffset = ((java.util.TimeZone)obj).getRawOffset();
	    toAppendTo.append("GMT");
	    if(tzoffset == 0) return toAppendTo;
	    int hours = tzoffset / (1000 * 60 * 60);
	     toAppendTo.append(hf.format(hours));
	    toAppendTo.append(':');
	    int minutes = Math.abs((tzoffset % (1000 * 60 * 60)) / (1000 * 60));
	    toAppendTo.append(mf.format(minutes));
	    return toAppendTo;
    }
    
    public java.lang.Object parseObject(java.lang.String str, java.text.ParsePosition parsePosition) {
	    return null;
	}
}
