/*
 * Created on Feb 10, 2005
 *
 */
package simple.text;

import java.lang.reflect.Array;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

/** Designed to quickly return object.toString() or "" if object is null
 * @author bkrug
 *
 */
public class ToStringFormat extends Format {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8102935402161L;

	/**
     * 
     */
    public ToStringFormat() {
        super();
    }

    /* (non-Javadoc)
     * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
     */
    public Object parseObject(String source, ParsePosition pos) {
        if(source == null || source.length() == 0) return null;
        else return source;
    }

    /* (non-Javadoc)
     * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer, java.text.FieldPosition)
     */
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        toString(obj, toAppendTo);
        return toAppendTo;
    }

    protected static void toString(Object obj, StringBuffer toAppendTo) {
        if(obj != null) {
            if(obj.getClass().isArray()) arrayToString(obj, toAppendTo);
            else toAppendTo.append(obj);
        }
    }
    
    public static void arrayToString(Object array, StringBuffer toAppendTo) {
        int len = Array.getLength(array);
        for(int i = 0; i < len; i++) {
            if(i > 0) toAppendTo.append(",");
            toString(Array.get(array, i), toAppendTo);
        }
    }
    
    public static String arrayToString(Object array) {
        StringBuffer sb = new StringBuffer();
        arrayToString(array, sb);
        return sb.toString();
    }
}
