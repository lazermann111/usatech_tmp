package simple.text;

/** A SimpleDateFormat that uses the supplied TimeZone in the date format calendar
 *
 * @author  bkrug
 * @version
 */
import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;

public class DateByTimezoneFormat extends AbstractPatternedFormat {
	private static final long serialVersionUID = 8113354288914L;
	protected final ThreadSafeDateFormat delegate;
	protected String timeZoneAttr;
	protected String dateAttr;
	public DateByTimezoneFormat(String pattern) {
        super(pattern);
        String[] parts = StringUtils.split(pattern, ',', 2);
        String df;
        switch(parts.length) {
        	case 1:
        		df = pattern;
        		this.dateAttr = null;
        		this.timeZoneAttr = "~scene.timeZone";
        		break;
        	case 2:
        		df = parts[1];
        		this.dateAttr = null;
        		this.timeZoneAttr = parts[0];
        		break;
        	case 3:
        		df = parts[2];
        		this.dateAttr = parts[0];
        		this.timeZoneAttr = parts[1];
        		break;
        	default:
        		throw new IllegalArgumentException();
        }
        this.delegate = new ThreadSafeDateFormat(new SimpleDateFormat(df));
	}
	
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition fieldPosition) {
		Date date;
		try {
			if(getDateAttr() == null || getDateAttr().length() == 0)
				date = ConvertUtils.convert(Date.class, obj);
			else
				date = ConvertUtils.convert(Date.class, ReflectionUtils.getProperty(obj, getDateAttr()));
		} catch(ConvertException e) {
			throw new IllegalArgumentException(e);
		} catch(IntrospectionException e) {
			throw new IllegalArgumentException(e);
		} catch(IllegalAccessException e) {
			throw new IllegalArgumentException(e);
		} catch(InvocationTargetException e) {
			throw new IllegalArgumentException(e);
		} catch(ParseException e) {
			throw new IllegalArgumentException(e);
		} 
		if(date == null)
			return toAppendTo;
		TimeZone timeZone;
		try {
			timeZone = ConvertUtils.convert(TimeZone.class, ReflectionUtils.getProperty(obj, getTimeZoneAttr()));
		} catch(ConvertException e) {
			throw new IllegalArgumentException(e);
		} catch(IntrospectionException e) {
			throw new IllegalArgumentException(e);
		} catch(IllegalAccessException e) {
			throw new IllegalArgumentException(e);
		} catch(InvocationTargetException e) {
			throw new IllegalArgumentException(e);
		} catch(ParseException e) {
			throw new IllegalArgumentException(e);
		}
		DateFormat dateFormat = delegate.getDelegate();
		TimeZone orig = dateFormat.getTimeZone();
		if(timeZone != null)
			dateFormat.setTimeZone(timeZone);
		try {
			return dateFormat.format(date, toAppendTo, fieldPosition);
		} finally {
			if(orig != null && timeZone != null)
				dateFormat.setTimeZone(orig);
		}
	}
	
	public Object parseObject(String source, ParsePosition parsePosition) {
		return delegate.getDelegate().parseObject(source, parsePosition);
	}

	public String getTimeZoneAttr() {
		return timeZoneAttr;
	}

	public void setTimeZoneAttr(String timeZoneAttr) {
		this.timeZoneAttr = timeZoneAttr;
	}

	public String getDateAttr() {
		return dateAttr;
	}

	public void setDateAttr(String dateAttr) {
		this.dateAttr = dateAttr;
	}    
}
