/**
 * 
 */
package simple.text;

import simple.text.StringUtils.Justification;

public class FixedWidthColumn {
	protected final int size;
	protected final Justification justification;
	protected final String label;
	protected final Class<?> type;
	protected final char padding;
	public FixedWidthColumn(int size, Justification justification, char padding, String label, Class<?> type) {
		this.size = size;
		this.justification = justification;
		this.label = label;
		this.type = type;
		this.padding = padding;
	}
	public int getSize() {
		return size;
	}
	public Justification getJustification() {
		return justification;
	}
	public String getLabel() {
		return label;
	}
	public Class<?> getType() {
		return type;
	}
	public char getPadding() {
		return padding;
	}
}