package simple.text;

public class DelimitedColumn {
	
	protected final String label;
	protected final Class<?> type;
	protected final String format;
	
	public DelimitedColumn(String label) {
		this(label, String.class);
	}
	
	public DelimitedColumn(String label, Class<?> type) {
		this(label, type, null);
	}
	
	public DelimitedColumn(String label, Class<?> type, String format) {
		this.label = label;
		this.type = type;
		this.format = format;
	}
	
	public String getLabel() {
		return label;
	}
	
	public Class<?> getType() {
		return type;
	}
	
	public String getFormat() {
		return format;
	}
	
	public String setFormat(String format) {
		return format;
	}

	@Override
	public String toString() {
		return String.format("DelimitedColumn[label=%s, type=%s, format=%s]", label, type, format);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((format == null) ? 0 : format.hashCode());
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		DelimitedColumn other = (DelimitedColumn)obj;
		if(format == null) {
			if(other.format != null)
				return false;
		} else if(!format.equals(other.format))
			return false;
		if(label == null) {
			if(other.label != null)
				return false;
		} else if(!label.equals(other.label))
			return false;
		if(type == null) {
			if(other.type != null)
				return false;
		} else if(!type.equals(other.type))
			return false;
		return true;
	}
}