/*
 * Created on Feb 6, 2006
 *
 */
package simple.text;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

public class ThrowableFormat extends Format {
    private static final long serialVersionUID = -667379411197982710L;

    public ThrowableFormat() {
        super();
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        Throwable t = (Throwable)obj;
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        pw.flush();
        toAppendTo.append(sw.getBuffer());
        return toAppendTo;
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
        return null;
    }

}
