package simple.text;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

public class DateFromDayFormat extends AbstractPatternedFormat {
	private static final long serialVersionUID = 8445225115658677072L;
	protected final Format dateFormat;

	public DateFromDayFormat(String pattern) {
		super(pattern);
		dateFormat = ConvertUtils.getFormat("DATE", pattern);
	}

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		if(obj == null)
			return toAppendTo;
		Short days;
		try {
			days = ConvertUtils.convert(Short.class, obj);
		} catch(ConvertException e) {
			throw new IllegalArgumentException("Expecting a number", e);
		}
		if(days == null)
			return toAppendTo;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(TimeUnit.DAYS.toMillis(days));
		Date d = new Date(cal.getTimeInMillis() - cal.getTimeZone().getOffset(cal.getTimeInMillis()));
		return dateFormat.format(d, toAppendTo, pos);
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		Object date = dateFormat.parseObject(source, pos);
		Calendar cal;
		try {
			cal = ConvertUtils.convert(Calendar.class, date);
		} catch(ConvertException e) {
			throw new IllegalArgumentException("Expecting a date", e);
		}
		return TimeUnit.MILLISECONDS.toDays(cal.getTimeInMillis());
	}

}
