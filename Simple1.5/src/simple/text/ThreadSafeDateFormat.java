package simple.text;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ThreadSafeDateFormat extends DateFormat {
	private static final long serialVersionUID = 452345234521L;
	protected final ThreadLocal<DateFormat> localDelegate;
	
	public ThreadSafeDateFormat(final DateFormat delegate) {
		super();
		this.localDelegate = new ThreadLocal<DateFormat>() {
			@Override
			protected DateFormat initialValue() {
				try {
					return (DateFormat)delegate.clone();
				} catch(NullPointerException e) {
					// clone() of DateFormat throws NPE if implementations fail to set the calendar and numberFormat fields
					return delegate;
				}
			}			
		};
	}
	public DateFormat getDelegate() {
		return localDelegate.get();
	}
	@Override
	public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition fieldPosition) {
		return localDelegate.get().format(date, toAppendTo, fieldPosition);
	}

	@Override
	public Date parse(String source, ParsePosition pos) {
		return localDelegate.get().parse(source, pos);
	}

	@Override
	public int hashCode() {
		return localDelegate.get().hashCode();
	}

	@Override
	public String toString() {
		return localDelegate.get().toString();
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj instanceof ThreadSafeDateFormat)
			return localDelegate.get().equals(((ThreadSafeDateFormat)obj).localDelegate.get());
		return localDelegate.get().equals(obj);
	}

	@Override
	public Object clone() {
		return new ThreadSafeDateFormat(localDelegate.get());
	}

	@Override
	public Calendar getCalendar() {
		return localDelegate.get().getCalendar();
	}

	@Override
	public NumberFormat getNumberFormat() {
		return localDelegate.get().getNumberFormat();
	}

	@Override
	public TimeZone getTimeZone() {
		return localDelegate.get().getTimeZone();
	}

	@Override
	public boolean isLenient() {
		return localDelegate.get().isLenient();
	}

	@Override
	public void setCalendar(Calendar newCalendar) {
		localDelegate.get().setCalendar(newCalendar);
	}

	@Override
	public void setLenient(boolean lenient) {
		localDelegate.get().setLenient(lenient);
	}

	@Override
	public void setNumberFormat(NumberFormat newNumberFormat) {
		localDelegate.get().setNumberFormat(newNumberFormat);
	}

	@Override
	public void setTimeZone(TimeZone zone) {
		localDelegate.get().setTimeZone(zone);
	}
}
