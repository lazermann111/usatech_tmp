/*
 * LiteralFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;


/** This class is always returns (or appends) the same pre-defined String for its format() methods.
 *
 * @author  Brian S. Krug
 */
public class LiteralFormat extends AbstractPatternedFormat {    
    /**
	 * 
	 */
	private static final long serialVersionUID = 81440934510L;

	/** Creates a new instance of LiteralFormat */
    public LiteralFormat(String literal) {
        super(literal);
    }
    
    public StringBuffer format(Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos) {
        toAppendTo.append(pattern);
        return toAppendTo;
    }
    
    public Object parseObject(String source, java.text.ParsePosition pos) {
        if(source.regionMatches(pos.getIndex(), pattern, 0, pattern.length())) {
            pos.setIndex(pos.getIndex() + pattern.length());
        } else {
            pos.setErrorIndex(pos.getIndex());
        }
        return null;
    }
        
}
