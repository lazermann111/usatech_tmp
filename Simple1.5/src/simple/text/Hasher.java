package simple.text;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.locks.ReentrantLock;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.io.Base64;

public class Hasher {
	protected final MessageDigest digest;
	protected final boolean base64;
	protected final ReentrantLock lock = new ReentrantLock();

	public Hasher(String algorithm, boolean base64) throws NoSuchAlgorithmException {
		digest = MessageDigest.getInstance(algorithm);
		this.base64 = base64;
	}

	public String hash(Object value) throws ConvertException {
		if(value == null)
    		return null;
    	byte[] bytes;
		if(value instanceof MaskedString)
			bytes = ((MaskedString)value).getValue().getBytes();
		else
			bytes = ConvertUtils.convert(byte[].class, value);

		return hash(bytes);
	}

	public String hash(String value) {
		if(value == null)
    		return null;
    	return hash(value.getBytes());
	}

	public String hash(byte[] bytes) {
		if(bytes == null)
    		return null;
		bytes = hashRaw(bytes);
		if(isBase64())
			return Base64.encodeBytes(bytes, true);
		else
			return new String(bytes);
	}

	public byte[] hashRaw(byte[] bytes) {
		lock.lock();
		try {
			return digest.digest(bytes);
		} finally {
			lock.unlock();
		}
	}
	
	public byte[] hashRaw(byte[] bytes, byte[] salt) {
		byte[] bytesAndSalt = new byte[bytes.length + salt.length];
		System.arraycopy(bytes, 0, bytesAndSalt, 0, bytes.length);
		System.arraycopy(salt, 0, bytesAndSalt, bytes.length, salt.length);
		return hashRaw(bytesAndSalt);
	}
	
	public boolean isBase64() {
		return base64;
	}
}
