package simple.text;

import simple.bean.ConvertUtils;
import simple.bean.Getter;
import simple.lang.SystemUtils;

public class FormatDeputy implements Getter<String> {
	protected String format;
	protected Object params;

	public FormatDeputy() {
	}

	public FormatDeputy(String format) {
		this.format = format;
	}

	@Override
	public String get() {
		return ConvertUtils.formatObject(params, format);
	}

	@Override
	public Class<String> getType() {
		return String.class;
	}

	@Override
	public String toString() {
		return get();
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Object getParams() {
		return params;
	}

	public void setParams(Object params) {
		this.params = params;
	}

	public void setParamsArray(Object[] params) {
		this.params = params;
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof FormatDeputy))
			return false;
		FormatDeputy fd = (FormatDeputy) obj;
		return ConvertUtils.areEqual(format, fd.format) && ConvertUtils.areEqual(params, fd.params);
	}

	@Override
	public int hashCode() {
		return SystemUtils.addHashCodes(0, format, params);
	}
}
