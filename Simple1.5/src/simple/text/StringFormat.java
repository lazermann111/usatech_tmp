/*
 * StringFormat.java
 *
 * Created on December 26, 2001, 10:42 AM
 */

package simple.text;

/**
 *
 * @author  bkrug
 * @version
 *
 *  Formats a string using the specified pattern. The following symbols are valid in the pattern string:
 *	# Any valid number, uses Character.isDigit.
 *	' Escape character, used to escape any of the special formatting characters.
 *	U Any character (Character.isLetter). All lowercase letters are mapped to upper case.
 *	L Any character (Character.isLetter). All upper case letters are mapped to lower case.
 *	A Any character or number (Character.isLetter or Character.isDigit)
 *	? Any character (Character.isLetter).
 *	* Anything.
 *	H Any hex character (0-9, a-f or A-F).
 *	_ Any amount of whitespace, replaces with single space
 *	S Any character, is not added to formatted string but is skipped
 *	[ Begins number section
 *	] Ends number section
 *
 * These symbols (except for '_', 'i','[',']') were chosen to match the symbols used in java 1.4's  javax.swing.text.MaskFormatter
 *
 * The following symbols are valid in a number section
 *	0 Any number, adds leading or trailing zeros as needed
 *	9 Any number, removes leading or trailing zeros in its place
 *	^ Insert Decimal point
 *	. Use strings decimal point at this place
 *	, Grouping indicator
 */
import java.text.Format;
import java.text.ParseException;

public class StringFormat extends Format {
	private static final long serialVersionUID = 6364870137346782730L;
	protected String pattern;
	protected boolean ignoreErrors;
	/** Creates new StringFormat */
	public StringFormat(String pattern)  {
		this(pattern, true);
	}
	/** Creates new StringFormat */
	public StringFormat(String pattern, boolean ignoreErrors)  {
		super();
		setPattern(pattern);
		this.ignoreErrors = ignoreErrors;
	}
	public java.lang.StringBuffer format(java.lang.Object obj, java.lang.StringBuffer result, java.text.FieldPosition fieldPosition) {
		if(obj == null) return result;
        String s = obj.toString();
		char c;
		int p = 0;
		boolean isDecimalSection = false;
		boolean leadingZeros = true;
		try {
			for(int i = 0; i < pattern.length() && p < s.length(); i++) {
				switch(pattern.charAt(i)) {
					/*
					case '[': // Begins number section
						int i1 = pattern.indexOf(']',i+1);
						if(i1 < 0) throw new ParseException("Pattern is missing number section terminator, ']'", p);
						int group = 0;
						boolean neg = false;
						boolean parens = false;
						boolean dec = false;
						// read string until end of number
						StringBuffer num = new StringBuffer(); // holds only digits and optionally one decimal pt
						while(Character.isWhitespace(s.charAt(p)) && p < s.length()) p++; //trim leading whitespace
						for(;p < s.length(); p++) {
							c = s.charAt(p);
							if(c == '.') {
								if(dec) break; //this is the second decimal point so end
								else num.append(c);
								dec = true;
							} else if(Character.isDigit(c)) num.append(c);
							else if(c == '-') {
								neg = true;
								if(num.length() > 0) break;
							} else if(c == '(' && num.length() == 0) {
								parens = true;
								neg = true;
							} else break;
						}
						if(parens) {
							while(p < s.length()) {
								c =  s.charAt(p);
								if(c == ')') {
									p++;
									break;
								} else if(Character.isWhitespace(c)) p++;
								else break;
							}
						}

						int idot = pattern.indexOf('^',i+1);
						if(idot >= i1) idot = -1;//ignore carrots after the end of the number section
						if(idot >= 0) { //match up the last digit in the string to the last in the pattern and format towards the front
							StringBuffer tmp = new StringBuffer();
							int q = tmp.length();
							int lastDigit = 0; //for trimming leading zeros
							for(int k = i1 - 1; k > i; k--) {
								switch(pattern.charAt(k)) {
									case '^':
										tmp.insert(0,'.');
										break;
									case ',':
										if(group == 0 && k < idot) group = (idot - k - 1);
										break;
									case '0': // Any number, adds leading or trailing zeros as needed
										if(q >= 0) {
											c = num.charAt(q);
											if(Character.isDigit(c)) {
												lastDigit = 0;
												tmp.insert(0,c);
											}
											q--;
										} else tmp.insert(0,'0'); //no more of string left
										break;
									case '9': // Any number, removes leading or trailing zeros in its place
										if(q >= 0) {
											c = num.charAt(q);
											if(Character.isDigit(c)) {
												if(c == '0') {
													if(tmp.length() > 0) {
														tmp.insert(0,c);
														 lastDigit++;
													}
												} else	tmp.insert(0,c);
											}
											q--;
										}
										break;
									case '#': // Any number
										if(q >= 0) {
											c = num.charAt(q);
											if(Character.isDigit(c)) {
												lastDigit = 0;
												tmp.insert(0,c);
											}
											q--;
										}
										break;
									default:
								}
							}

							if(lastDigit > 0) tmp.delete(0,lastDigit); //trim leading zeros
							if(tmp.length() > 0 && tmp.charAt(tmp.length() - 1) == '.') tmp.setLength(tmp.length() - 1); //remove hanging decimal pt
							if(group > 0) for(int k = tmp.length() - group; k > 0; k -= group) tmp.insert(k,','); //add groupings
							//determine if neg sign
							c = pattern.charAt(i+1);
							if(neg && (c == '-' || c == '(')) tmp.insert(0,c);
							result.append(tmp);
							c = pattern.charAt(i1-1);
							if(neg && (c == '-' || c == ')')) result.append(c);
						} else {// match the decimal up with that in the string and format from it out
							idot = pattern.indexOf('.',i+1);
							if(idot >= i1 || idot < 0) idot = i1;//ignore dots after the end of the number section
							int pdot = (dec ? num.toString().indexOf('.') : num.length());
							StringBuffer tmp = new StringBuffer();
							int q = pdot - 1;
							int lastDigit = 0; //for trimming leading zeros
							for(int k = idot - 1; k > i; k--) {
								switch(pattern.charAt(k)) {
									case ',':
										if(group == 0) group = (idot - k - 1);
										break;
									case '0': // Any number, adds leading or trailing zeros as needed
										if(q >= 0) {
											c = num.charAt(q);
											if(Character.isDigit(c)) {
												lastDigit = 0;
												tmp.insert(0,c);
											}
											q--;
										} else tmp.insert(0,'0'); //no more of string left
										break;
									case '9': // Any number, removes leading or trailing zeros in its place
										if(q >= 0) {
											c = num.charAt(q);
											if(Character.isDigit(c)) {
												if(c == '0') lastDigit++;
												tmp.insert(0,c);
											}
											q--;
										}
										break;
									case '#': // Any number
										if(q >= 0) {
											c = num.charAt(q);
											if(Character.isDigit(c)) {
												lastDigit = 0;
												tmp.insert(0,c);
											}
											q--;
										}
										break;
									default:
								}
							}

							if(lastDigit > 0) tmp.delete(0,lastDigit); //trim leading zeros
							if(group > 0) for(int k = tmp.length() - group; k > 0; k -= group) tmp.insert(k,','); //add groupings
							//determine if neg sign
							c = pattern.charAt(i+1);
							if(neg && (c == '-' || c == '(')) tmp.insert(0,c);
							result.append(tmp);

							//now do decimal part
							tmp.setLength(0);
							lastDigit = 0; //for trimming leading zeros
							q = pdot + 1;
							boolean done = false;
							for(int k = idot + 1; k < i1 && q < num.length(); k++) {
								switch(pattern.charAt(k)) {
									case '0': // Any number, adds leading or trailing zeros as needed
										if(!done) {
											c = num.charAt(q);
											if(Character.isDigit(c)) {
												tmp.append(c);
												q++;
											} else {
												tmp.append('0'); //no more of string left
												done = true;
											}
										} else tmp.append('0'); //no more of string left
										lastDigit = q;
										break;
									case '9': // Any number, removes leading or trailing zeros in its place
										if(!done) {
											c = num.charAt(q);
											if(Character.isDigit(c)) {
												if(c != '0') lastDigit = q;
												tmp.append(c);
												q++;
											} else {
												done = true;
											}
										}
										break;
									case '#': // Any number
										if(!done) {
											c = num.charAt(q);
											if(Character.isDigit(c)) {
												lastDigit = q;
												tmp.append(c);
												q++;
											} else {
												done = true;
											}
										}
										break;
									default:
								}
							}
							tmp.setLength(lastDigit);
							if(tmp.length() > 0) {
								result.append('.');
								result.append(tmp);
							}

							c = pattern.charAt(i1-1);
							if(neg && (c == '-' || c == ')')) result.append(c);
						}
						i = i1;
						break;*/
					case '.': //the decimal
						result.append(pattern.charAt(i));
						leadingZeros = false;
						isDecimalSection = true;
						break;
					case '_': // Any amount of whitespace, replaces with single space
						if(isDecimalSection) endDecimalSection(result, i-1);
						while(p < s.length() && Character.isWhitespace(s.charAt(p))) p++;
						result.append(" ");
						leadingZeros = true;
						isDecimalSection = false;
						break;
					case 'S': //Any character, is not added to formatted string but is skipped
						p++;
						break;
					case '9': //Any valid number, uses Character.isDigit. If it is a leading or trailing zero it is not added to the formatted string
						c = s.charAt(p);
						if(!Character.isDigit(c)) throw new ParseException("String does not match pattern, expecting a number", p);
						if(c != '0' || !leadingZeros) {
							result.append(c);
							leadingZeros = false;
						}
						p++;
						break;
					case '#': //Any valid number, uses Character.isDigit.
						c = s.charAt(p);
						if(!Character.isDigit(c)) throw new ParseException("String does not match pattern, expecting a number", p);
						result.append(c);
						leadingZeros = false;
						p++;
						break;
					case 'U': //Any character (Character.isLetter). All lowercase letters are mapped to upper case.
						if(isDecimalSection) endDecimalSection(result, i-1);
						c = s.charAt(p);
						if(!Character.isLetter(c)) throw new ParseException("String does not match pattern, expecting a letter", p);
						result.append(Character.toUpperCase(c));
						p++;
						leadingZeros = true;
						isDecimalSection = false;
						break;
					case 'L': //Any character (Character.isLetter). All upper case letters are mapped to lower case.
						if(isDecimalSection) endDecimalSection(result, i-1);
						c = s.charAt(p);
						if(!Character.isLetter(c)) throw new ParseException("String does not match pattern, expecting a letter", p);
						result.append(Character.toLowerCase(c));
						p++;
						leadingZeros = true;
						isDecimalSection = false;
						break;
					case 'A': // Any character or number (Character.isLetter or Character.isDigit)
						c = s.charAt(p);
						if(!Character.isLetterOrDigit(c)) throw new ParseException("String does not match pattern, expecting a letter or number", p);
						result.append(c);
						p++;
						leadingZeros = true;
						if(isDecimalSection) endDecimalSection(result, i-1);
						isDecimalSection = false;
						break;
					case '?': //Any character (Character.isLetter).
						if(isDecimalSection) endDecimalSection(result, i-1);
						c = s.charAt(p);
						if(!Character.isLetter(c)) throw new ParseException("String does not match pattern, expecting a letter", p);
						result.append(c);
						p++;
						leadingZeros = true;
						isDecimalSection = false;
						break;
					case '*': //Anything.
						if(isDecimalSection) endDecimalSection(result, i-1);
						result.append(s.charAt(p));
						p++;
						leadingZeros = true;
						isDecimalSection = false;
						break;
					case 'H': //Any hex character (0-9, a-f or A-F).
						if(isDecimalSection) endDecimalSection(result, i-1);
						c = s.charAt(p);
						if(!isHex(c)) throw new ParseException("String does not match pattern, expecting a hex character (0-9, A-F or a-f)", p);
						result.append(c);
						p++;
						leadingZeros = true;
						isDecimalSection = false;
						break;
					case '\'': //Escape character, used to escape any of the special formatting characters.
						i++;
					default:
						if(isDecimalSection) endDecimalSection(result, i-1);
						result.append(pattern.charAt(i));
						isDecimalSection = false;
				}
			}
			if(isDecimalSection) endDecimalSection(result, pattern.length()-1);
		} catch(ParseException pe) {
			if(ignoreErrors) {
				result.append(s, p, s.length());
			} else
				throw new IllegalArgumentException(pe.getMessage() + " at position " + pe.getErrorOffset());
		}

		return result;
	}

	protected void endDecimalSection(StringBuffer result, int patternPos) {
		for(int i = result.length() - 1; result.charAt(i) == '0' && pattern.charAt(patternPos) == '9'; i--, patternPos--) result.setLength(i);
		if(result.charAt(result.length() - 1) == '.' && pattern.charAt(patternPos) == '.') result.setLength(result.length() - 1);
	}

	public java.lang.Object parseObject(java.lang.String str, java.text.ParsePosition status) {
		StringBuffer result = new StringBuffer();
		char c;
		int p = 0;
		try {
			for(int i = 0; i < pattern.length(); i++) {
				switch(pattern.charAt(i)) {
					case '9': //Any valid number, uses Character.isDigit.
						//need to ensure that it wasn't left off for a leading or trailing zero
					case '#': //Any valid number, uses Character.isDigit.
						c = str.charAt(p);
						if(!Character.isDigit(c)) throw new ParseException("String does not match pattern, expecting a number", p);
						result.append(c);
						p++;
						break;
					case 'U': //Any character (Character.isLetter). All lowercase letters are mapped to upper case.
						c = str.charAt(p);
						if(!Character.isLetter(c)) throw new ParseException("String does not match pattern, expecting a letter", p);
						result.append(Character.toUpperCase(c));
						p++;
						break;
					case 'L': //Any character (Character.isLetter). All upper case letters are mapped to lower case.
						c = str.charAt(p);
						if(!Character.isLetter(c)) throw new ParseException("String does not match pattern, expecting a letter", p);
						result.append(Character.toLowerCase(c));
						p++;
						break;
					case 'A': // Any character or number (Character.isLetter or Character.isDigit)
						c = str.charAt(p);
						if(!Character.isLetterOrDigit(c)) throw new ParseException("String does not match pattern, expecting a letter or number", p);
						result.append(c);
						p++;
						break;
					case '?': //Any character (Character.isLetter).
						c = str.charAt(p);
						if(!Character.isLetter(c)) throw new ParseException("String does not match pattern, expecting a letter", p);
						result.append(c);
						p++;
						break;
					case '*': //Anything.
						result.append(str.charAt(p));
						p++;
						break;
					case 'H': //Any hex character (0-9, a-f or A-F).
						c = str.charAt(p);
						if(!isHex(c)) throw new ParseException("String does not match pattern, expecting a hex character (0-9, A-F or a-f)", p);
						result.append(c);
						p++;
						break;
					case '\'': //Escape character, used to escape any of the special formatting characters.
						i++;
					default:
						if(str.charAt(p) != pattern.charAt(i)) throw new ParseException("String does not match pattern, expecting a '" + pattern.charAt(i) + "'" , p);
				}
			}
			status.setIndex(p); // do this at end to indicate success
		} catch(ParseException pe) {
			status.setErrorIndex(pe.getErrorOffset());
		}
		return result.toString();
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String newPattern) {
		pattern = newPattern;
	}

	protected boolean isHex(char c) {
		if(Character.isDigit(c)) return true;
		if(!Character.isLetter(c)) return false;
		c = Character.toUpperCase(c);
		return (c >= 'A' &&  c <= 'F');
	}

}
