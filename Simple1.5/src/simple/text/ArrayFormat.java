/*
 * LiteralFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;


/** This class formats an array of objects using the specified sub-format and concatenating them together with the specified separator.
 *  The pattern is of the form: &lt;separator&gt;&lt;direction&gt;&lt;sub-format&gt; , where separator is any sequence of characters except '*' and ';'
 *  and the '\' is used to escape the next character (so '*' and ';' can be used by putting '\*' or '\;' in the separator). The direction is ';' to
 *  indicate normal vertical arrays and the '*' to expect a two-dimension array where the primary array is the columns.
 *
 * @author  Brian S. Krug
 */
public class ArrayFormat extends AbstractPatternedFormat {
    private static final long serialVersionUID = 7254040615165012410L;
    protected String separator = ", ";
    protected String formatString;
    protected boolean horizontal;

	/** Creates a new instance of LiteralFormat */
    public ArrayFormat(String pattern) {
        super(pattern);
        parsePattern(pattern);
    }

    /**
	 * @param pattern
	 */
	protected void parsePattern(String pattern) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < pattern.length(); i++) {
			char ch = pattern.charAt(i);
			switch(ch) {
				case '\\':
					i++;
					break;
				case ';':
					separator = sb.toString();
					formatString = pattern.substring(i+1);
					return;
				case '*':
					separator = sb.toString();
					formatString = pattern.substring(i+1);
					horizontal = true;
					return;
				default:
					sb.append(ch);
			}
		}
		separator = sb.toString();
		formatString = null;
	}

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos) {
    	try {
    		Collection<?> coll = ConvertUtils.asCollection(obj);
    		if(horizontal) { // Assumes a two-dimentional array
    			Iterator<?>[] array = new Iterator<?>[coll.size()];
    			int i = 0;
    			for(Object o : coll) {
    				Collection<?> c = ConvertUtils.asCollection(o);
    				array[i++] = (c == null ? null : c.iterator());
    			}
				Collection<String> elements = createFormattedCollection(16);
    			while(true) {
    				Object[] row = new Object[array.length];
    				boolean done = true;
    				for(int k = 0; k < array.length; k++) {
    					if(array[k].hasNext()) {
    						row[k] = array[k].next();
    						done = false;
    					}
    				}
    				if(done)
    					break;
    				elements.add(ConvertUtils.formatObject(row, formatString));
    			}
    			StringUtils.join(elements, separator, toAppendTo);
    		} else {
				Collection<String> elements = createFormattedCollection(coll.size());
    			for(Object o : coll)
    				elements.add(ConvertUtils.formatObject(o, formatString));
    			StringUtils.join(elements, separator, toAppendTo);
    		}
		} catch(ConvertException e) {
			toAppendTo.append(obj);
		}
        return toAppendTo;
    }

	protected Collection<String> createFormattedCollection(int estimatedSize) {
		return new ArrayList<String>(estimatedSize);
	}

    @Override
	public Object parseObject(String source, java.text.ParsePosition pos) {
    	if(source == null) return null;
    	String[] array = StringUtils.split(source.substring(pos.getIndex()), pattern);
    	pos.setIndex(source.length());
        return array;
    }

}
