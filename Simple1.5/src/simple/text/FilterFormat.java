/*
 * LiteralFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;

import java.text.ParsePosition;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

/** This class returns a String that contains only those characters that
 *  match via regular expression the pattern provided as the constructor argument. 
 *
 * @author  Brian S. Krug
 */
public class FilterFormat extends AbstractPatternedFormat {    
    /**
	 * 
	 */
	private static final long serialVersionUID = 812346105451L;


	/** Creates a new instance of FilterFormat */
    public FilterFormat(String pattern) {
        super(pattern);
    }
    
    public StringBuffer format(Object obj, StringBuffer toAppendTo, java.text.FieldPosition pos) {
        String s;
        try {
            s = ConvertUtils.convert(String.class, obj);
        } catch(ConvertException ce) {
            s = obj == null ? "" : obj.toString();
        }
        if(s == null) s = "";
        RegexUtils.Finder finder = RegexUtils.find(pattern, s, null);
        String[] result;
        while((result=finder.find()) != null) {
            toAppendTo.append(result[0]);
        }
        return toAppendTo;
    }
    
        
	/** Not Implemented
	 * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
	 */
	public Object parseObject(String source, ParsePosition pos) {
		return null;
	}  
}
