package simple.text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.io.Log;

public class Censor {
	private static final Log log = Log.getLog();
	protected static List<Pattern> censoredWords;
	protected static final Pattern WHITESPACE = Pattern.compile("\\s");
	
	static {
		censoredWords = Collections.synchronizedList(new ArrayList<Pattern>());
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(Censor.class.getResourceAsStream("Censor.properties")));
	        censoredWords = new ArrayList<Pattern>();
	        String line;
	        while((line = in.readLine()) != null) {
	        	if (!StringUtils.isBlank(line) && !line.startsWith("#"))
	        		censoredWords.add(Pattern.compile(new StringBuilder("(?i)").append(line.trim()).toString()));
	        }
		} catch (Exception e) {
			log.error("Error loading censored words from Censor.properties", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
					
				}
			}
		}
	}
	
	public static String sanitizeText(String text) throws IOException {
		if (StringUtils.isBlank(text))
			return text;
		for (Pattern censoredWord: censoredWords) {	
			Matcher matcher = censoredWord.matcher(text);
			while (matcher.find()) {
				String censored = matcher.group(2);
				StringBuilder sanitizedWord = new StringBuilder(matcher.group(1)).append(censored.substring(0, 1));
				int i;
				for (i=1; i < censored.length() - 1; i++)
					if (WHITESPACE.matcher(censored.substring(i, i + 1)).matches())
						sanitizedWord.append(censored.substring(i, i + 1));
					else
						sanitizedWord.append("*");
				sanitizedWord.append(censored.substring(i, i + 1)).append(matcher.group(3));
				text = text.replaceFirst(censoredWord.pattern(), sanitizedWord.toString().replaceAll("\\$", "\\\\\\$"));
			}
		}
		return text;
	}
}
