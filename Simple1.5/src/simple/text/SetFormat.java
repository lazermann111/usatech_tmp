package simple.text;

import java.util.Collection;
import java.util.LinkedHashSet;

public class SetFormat extends ArrayFormat {
	private static final long serialVersionUID = 3303517617490416457L;

	public SetFormat(String pattern) {
		super(pattern);
	}

	@Override
	protected Collection<String> createFormattedCollection(int estimatedSize) {
		return new LinkedHashSet<String>();
	}
}
