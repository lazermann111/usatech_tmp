package simple.text;

/**
 * @author pcowan
 *
 */
public class CDataFormat extends PrepareFormat
{
	private static final long serialVersionUID = 81205725785825L;

	public CDataFormat()
	{
		super("CDATA");
	}
}
