/*
 * MatchFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;

import java.text.FieldPosition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

/**
 * Replaces any occurrence of the specified regular expression with the specified replacement and returns the result.
 *
 * @author Brian S. Krug
 */
public class ReplaceFormat extends AbstractPatternedFormat {
	private static final long serialVersionUID = 8243241999001432L;
	protected final Pattern lookFor;
	protected final String replacement;
        
    /** Creates a new instance of MatchFormat */
    public ReplaceFormat(String pattern) {
        super(pattern);
		String[] pairs = StringUtils.splitQuotesAnywhere(pattern, ';', '\'', true);
		switch(pairs.length) {
			case 0:
				this.lookFor = null;
				this.replacement = null;
				break;
			case 1:
				this.lookFor = Pattern.compile(pairs[0]);
				this.replacement = null;
				break;
			case 2:
				this.lookFor = Pattern.compile(pairs[0]);
				replacement = RegexUtils.unescapeBackslash(pairs[1]);
				break;
			default:
				this.lookFor = Pattern.compile(pairs[0]);
				replacement = RegexUtils.unescapeBackslash(StringUtils.join(pairs, ";", 1, pairs.length - 1));
				break;
		}
    }
    
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        String s;
        try {
            s = ConvertUtils.convert(String.class, obj);
        } catch(ConvertException ce) {
            s = obj == null ? "" : obj.toString();
        }
        if(s == null) s = "";
		Matcher m = lookFor.matcher(s);
		while(m.find())
			m.appendReplacement(toAppendTo, replacement);
		m.appendTail(toAppendTo);

		return toAppendTo;
    }
    
    public Object parseObject(String source, java.text.ParsePosition pos) {
        return null;
    }
    
}
