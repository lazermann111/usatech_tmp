/*
 * NullFormat.java
 *
 * Created on January 20, 2004, 10:18 AM
 */

package simple.text;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;

/**
 * Uses the specified default when value is null.
 * 
 * @author Brian S. Krug
 */
public class WithDefaultFormat extends Format {
	private static final long serialVersionUID = -3761562732495356587L;
	protected Format delegate;
	protected String defaultValue;
    
	public WithDefaultFormat(Format delegate, String defaultValue) {
		this.delegate = delegate;
		this.defaultValue = defaultValue;
    }
    
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		String s;
		if(obj == null || (s = ConvertUtils.getStringSafely(obj, "")).isEmpty()) {
			pos.setBeginIndex(toAppendTo.length());
			pos.setEndIndex(toAppendTo.length() + defaultValue.length());
			return toAppendTo.append(defaultValue);
		}
		if(delegate == null) {
			pos.setBeginIndex(toAppendTo.length());
			pos.setEndIndex(toAppendTo.length() + s.length());
			return toAppendTo.append(s);
		}
		return delegate.format(obj, toAppendTo, pos);
    }
    
	public Object parseObject(String source, ParsePosition pos) {
		if(source.regionMatches(pos.getIndex(), defaultValue, 0, defaultValue.length())) {
			pos.setIndex(pos.getIndex() + defaultValue.length());
			return null;
        }
		if(delegate == null) {
			pos.setIndex(source.length());
			return source;
		}
		return delegate.parseObject(source, pos);
    }

	public Format getDelegate() {
		return delegate;
	}

	public void setDelegate(Format delegate) {
		this.delegate = delegate;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}    

	@Override
	public int hashCode() {
		return SystemUtils.addHashCodes(0, delegate, defaultValue);
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof WithDefaultFormat))
			return false;
		WithDefaultFormat other = (WithDefaultFormat) obj;
		return ConvertUtils.areEqual(delegate, other.delegate) && ConvertUtils.areEqual(defaultValue, other.defaultValue);
	}
}
