package simple.text;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OracleDateFormat extends AbstractPatternedFormat {
	private static final long serialVersionUID = 4235252367271L;
	protected static enum PatternPartType {LITERAL, DIGITS, WORD, MILLIS, REGEX, TIMEZONE}
	protected static class ConversionBuilder {
		protected StringBuilder regex = new StringBuilder();
		protected List<PatternPartType> patternPartTypes = new ArrayList<PatternPartType>();
		protected List<String> replacements = new ArrayList<String>();
		public void addConversion(String oracleRegex, String javaReplacement, PatternPartType ppt) {
			if(regex.length() > 0) 
				regex.append("|");
			regex.append(oracleRegex);
			replacements.add(javaReplacement);
			patternPartTypes.add(ppt);
		}
		public Pattern getRegex() {
			return Pattern.compile(regex.toString(), Pattern.CASE_INSENSITIVE);
		}
		public String[] getReplacements() {
			return replacements.toArray(new String[replacements.size()]);
		}
		public PatternPartType[] getPartTypes() {
			return patternPartTypes.toArray(new PatternPartType[patternPartTypes.size()]);
		}
	}
	protected final ThreadLocal<DateFormat> javaDateFormat; // DateFormat is NOT thread-safe
	protected int millisDigits = 0;
	protected Pattern preParsePattern;
	protected Pattern postParsePattern;
	protected static Pattern dateElementPattern;
	protected static String[] dateElementReplacements;
	protected static PatternPartType[] partTypes;
	static {
		ConversionBuilder cb = new ConversionBuilder();
		
		cb.addConversion("[\"](.+)[\"]", "'$1'", PatternPartType.LITERAL);
		cb.addConversion("(MONTH)", "MMMM", PatternPartType.WORD);
		cb.addConversion("(F+[1-9]?)", "SSS", PatternPartType.MILLIS);
		cb.addConversion("(HH24)", "HH", PatternPartType.DIGITS);
		cb.addConversion("((?:YEAR)|(?:YYYY))", "yyyy", PatternPartType.DIGITS);
		cb.addConversion("((?:A\\.D\\.)|(?:B\\.C\\.))", "G.G.", PatternPartType.REGEX);
		cb.addConversion("((?:A\\.M\\.)|(?:P\\.M\\.))", "a.a.", PatternPartType.REGEX);
		cb.addConversion("(MON)", "MMM", PatternPartType.WORD);
		cb.addConversion("(TZD)", "zzz", PatternPartType.TIMEZONE);
		cb.addConversion("(TZR)", "zzzz", PatternPartType.TIMEZONE);
		cb.addConversion("(DDD)", "DD", PatternPartType.DIGITS);
		cb.addConversion("(DAY)", "EEEE", PatternPartType.WORD);
		cb.addConversion("((?:HH12)|(?:HH))", "h", PatternPartType.DIGITS);
		cb.addConversion("((?:AD)|(?:BC))", "GG", PatternPartType.REGEX);
		cb.addConversion("((?:AM)|(?:PM))", "aa", PatternPartType.REGEX);
		cb.addConversion("([\"][\"])", "", PatternPartType.LITERAL);
		cb.addConversion("(DY)", "EEE", PatternPartType.WORD);
		cb.addConversion("(DD)", "dd", PatternPartType.DIGITS);
		cb.addConversion("(WW)", "w", PatternPartType.DIGITS);
		cb.addConversion("(MI)", "mm", PatternPartType.DIGITS);
		cb.addConversion("(MM)", "MM", PatternPartType.DIGITS);
		cb.addConversion("(RR)", "yy", PatternPartType.DIGITS);
		cb.addConversion("(SS)", "ss", PatternPartType.DIGITS);
		cb.addConversion("(['])", "''", PatternPartType.LITERAL);
		cb.addConversion("(D)", "E", PatternPartType.WORD);
		cb.addConversion("(W)", "W", PatternPartType.DIGITS);
		cb.addConversion("(Y)", "y", PatternPartType.DIGITS);
		
		dateElementPattern = cb.getRegex();
		dateElementReplacements = cb.getReplacements();
		partTypes = cb.getPartTypes();
	}
	public OracleDateFormat(String pattern) {
		super(pattern);
		final String convertedPattern = convertDateFormat(pattern);
		javaDateFormat = new ThreadLocal<DateFormat>() {
			@Override
			protected DateFormat initialValue() {
				return new SimpleDateFormat(convertedPattern);
			}			
		};		
	}

	protected String convertDateFormat(String pattern) {
		boolean exact = true;
		if(pattern.regionMatches(true, 0, "FM", 0, 2)) {
			exact = false;
			pattern = pattern.substring(2);
		}
		Matcher matcher = dateElementPattern.matcher(pattern);
		StringBuffer sb = new StringBuffer();
		StringBuilder preparse = new StringBuilder();
		int last = 0; 
		int millisStart = -1;
		int millisEnd = -1;
		while(matcher.find()) {
			int g = 1;
			for( ; g < matcher.groupCount(); g++) {
				if(matcher.start(g) >= 0) {
					break;
				}
			}
			matcher.appendReplacement(sb, dateElementReplacements[g-1]);
			String s = pattern.substring(last, matcher.start());
			if(s.length() > 0) {
				preparse.append("\\Q");
				preparse.append(s.replace("\\E", "\\\\E"));
				preparse.append("\\E");
			}
			switch(partTypes[g-1]) {
				case LITERAL:
					preparse.append("\\Q");
					preparse.append(matcher.group(g).replace("\\E", "\\\\E"));
					preparse.append("\\E");
					break;
				case DIGITS:
					if(exact || dateElementReplacements[g-1].equals("yyyy")) {
						preparse.append("\\d{").append(dateElementReplacements[g-1].length()).append("}");
					} else {
						preparse.append("\\d{1,").append(dateElementReplacements[g-1].length()).append("}");
					}
					break;
				case WORD:
					preparse.append("\\w+");
					break;
				case MILLIS:
					String ff = matcher.group(g).trim();
					char ch = ff.charAt(ff.length()-1);
					if(ch > '0' && ch <= '9') {
						millisDigits = (ch - '0');
					} else {
						millisDigits = 6;
					}
					millisStart = preparse.length();
					preparse.append("(\\d{").append(millisDigits).append("})");
					millisEnd = preparse.length();
					break;
				case REGEX:
					String re =  matcher.group(g).trim();
					if(re.equals("aa")) {
						preparse.append("[AaPp][Mm]");
					} else if(re.equals("a.a.")) {
						preparse.append("[AaPp][.][Mm][.]");
					} else if(re.equals("GG")) {
						preparse.append("[AaBb][DdCc]");
					} else if(re.equals("G.G.")) {
						preparse.append("[AaBb][.][DdCc][.]");
					}
				case TIMEZONE:
					preparse.append("(?:GMT[-+]\\d{2}[:]\\d{2})|(?:[-+]\\d{4})|(?:(?:[-\\w]+\\s)*[-\\w]+)");
					break;
			}
			last = matcher.end();
		}
		matcher.appendTail(sb);
		preParsePattern = Pattern.compile(preparse.toString(), Pattern.CASE_INSENSITIVE);
		if(millisStart > -1 && millisDigits != 3) {
			preparse.replace(millisStart, millisEnd, "(\\d{3})");
		}
		postParsePattern = Pattern.compile(preparse.toString(), Pattern.CASE_INSENSITIVE);
		return sb.toString();
	}
	
	private static final char[] ZEROS = "000000".toCharArray();
	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		StringBuffer sb = javaDateFormat.get().format(obj, toAppendTo, pos);
		// SimpleDateFormat is too tough to override (everything is private or package access)
		// So we are forced to post-parse the string to find and change milleseconds  
		if(millisDigits != 0 && millisDigits != 3) {
			Matcher matcher = postParsePattern.matcher(sb);
			if(matcher.matches()) {
				switch(millisDigits) {
					case 1:
						sb.delete(matcher.start(1)+1, matcher.end(1));
						break;
					case 2:
						sb.delete(matcher.start(1)+2, matcher.end(1));
						break;
					default:
						sb.insert(matcher.end(1), ZEROS, 0, millisDigits - 3);
				}
			}
		}
		return sb;
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		// SimpleDateFormat is too tough to override (everything is private or package access)
		// So we are forced to pre-parse the string to find and change milleseconds  
		if(millisDigits != 0 && millisDigits != 3) {
			Matcher matcher = preParsePattern.matcher(source);
			if(matcher.matches()) {
				StringBuilder sb = new StringBuilder();
				sb.append(source.subSequence(0, matcher.start(1)));
				switch(millisDigits) {
					case 1:
						sb.append("00").append(matcher.group(1));
						break;
					case 2:
						sb.append('0').append(matcher.group(1));
						break;
					default:
						sb.append(matcher.group(1).subSequence(0, 3));
				}
				sb.append(source.substring(matcher.end(1)));
				source = sb.toString();
			}
		}
		return javaDateFormat.get().parseObject(source, pos);
	}	
}
