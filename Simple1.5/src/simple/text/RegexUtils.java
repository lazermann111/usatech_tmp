/*
 * RegexUtils.java
 *
 * Created on June 22, 2004, 9:43 AM
 */

package simple.text;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Performs regular expression operations (uses jakarta ORO for java 1.3 and lower and uses java.util.regex for java 1.4 and higher )
 *
 * @author  Brian S Krug
 */
public class RegexUtils {
	public static interface Finder {
        public String[] find() ;
        public String[] match() ;
        public boolean matches() ;
        public int getStartIndex() ;
        public int getEndIndex() ;
        public String substitute(String replace) ;
    }

    /** Creates a new instance of RegexUtils */
    public RegexUtils() {
    }

    public static boolean matches(String regex, String search, String flags) {
        return find(regex, search, flags).match() != null;
    }

    public static String[] match(String regex, String search, String flags) {
        return find(regex, search, flags).match();
    }

    public static String[] match(Pattern regex, String search) {
        return find(regex, search).match();
    }

    public static String substitute(String regex, String search, String replace, String flags) {
        return find(regex, search, flags).substitute(replace);
    }

    public static String[] split(String regex, String search, String flags) {
        return split(regex, search, flags, false);
    }

    public static String[] split(String regex, String search, String flags, boolean keepEmpty) {
        Matcher matcher = Pattern.compile(regex, translateFlags(flags)).matcher(search);
        List<String> splits = new ArrayList<String>();
        int pos = 0;
        while(matcher.find()) {
            if(keepEmpty || pos < matcher.start()) splits.add(search.substring(pos, matcher.start()));
            pos = matcher.end();
        }
        if(keepEmpty || pos < search.length()) splits.add(search.substring(pos, search.length()));
        return splits.toArray(new String[splits.size()]);
    }

    public static Finder find(String regex, String search, String flags) {
        return new FinderImpl(Pattern.compile(regex, translateFlags(flags)).matcher(search));
    }

    public static Finder find(Pattern regex, String search) {
        return new FinderImpl(regex.matcher(search));
    }

    protected static class FinderImpl implements Finder {
        protected Matcher matcher;
        public FinderImpl(Matcher _matcher) {
            matcher = _matcher;
        }
        public String[] find() {
            if(!matcher.find()) return null;
            String[] groups = new String[matcher.groupCount()+1];
            for(int i = 0 ; i < groups.length; i++) groups[i] = matcher.group(i);
            return groups;
        }

        public int getEndIndex() {
            return matcher.end();
        }

        public int getStartIndex() {
            return matcher.start();
        }

        public String[] match() {
            if(!matcher.matches()) return null;
            String[] groups = new String[matcher.groupCount()+1];
            for(int i = 0 ; i < groups.length; i++) groups[i] = matcher.group(i);
            return groups;
        }

        public String substitute(String replace) {
            return matcher.replaceAll(replace);
        }

        public boolean matches() {
            return matcher.matches();
        }
    }

    public static int translateFlags(String flags) {
        int flag = 0;
        if(flags != null) {
            for(int i = 0; i < flags.length(); i++) {
                switch(flags.charAt(i)) {
                    case 'i': flag += Pattern.CASE_INSENSITIVE; break;
                    case 'x': flag += Pattern.COMMENTS; break;
                    case 's': flag += Pattern.DOTALL; break;
                    case 'm': flag += Pattern.MULTILINE; break;
                    case 'u': flag += Pattern.UNICODE_CASE; break;
                    case 'd': flag += Pattern.UNIX_LINES; break;
                    case 'c': flag += Pattern.CANON_EQ; break;
                }
            }
        }
        return flag;
    }

	public static String escape(String s) {
		if(s == null)
			return null;
		StringBuilder sb = null;
		int l = 0;
		for(int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if(Character.isLetterOrDigit(ch))
				continue;
			switch(ch) {
				case ' ':
				case '_':
					continue;
			}
			if(sb == null)
				sb = new StringBuilder(s.length() + 5);

			sb.append(s, l, i);
			sb.append('\\').append(ch);
			l = i + 1;
		}
		if(sb == null)
			return s;
		sb.append(s, l, s.length());
		return sb.toString();
	}

	public static StringBuilder appendEscaped(String s, StringBuilder appendTo) {
		return appendEscaped(s, 0, s.length(), appendTo);
	}

	public static StringBuilder appendEscaped(String s, int start, int end, StringBuilder appendTo) {
		if(s == null)
			return appendTo;
		if(end > s.length())
			end = s.length();
		int l = start;
		for(int i = start; i < end; i++) {
			char ch = s.charAt(i);
			if(Character.isLetterOrDigit(ch))
				continue;
			switch(ch) {
				case ' ':
				case '_':
					continue;
			}
			appendTo.append(s, l, i);
			appendTo.append('\\').append(ch);
			l = i + 1;
		}
		appendTo.append(s, l, end);
		return appendTo;
	}

	protected static final Pattern WILDCARD_REGEX = Pattern.compile("(?:^|[^\\\\])(?:[.?+*$\\^|{\\[]|\\\\[A-Zb-dg-mpqsv-z0-9])");
	/**
	 * Determines whether the given string contains regular expression wild cards. In other words,
	 * if matches(regex, anystring) always returns the same as regex.equals(anystring), then this method returns FALSE.
	 * 
	 * @param regex
	 * @return
	 */
	public static boolean containsWildcards(String regex) {
		return WILDCARD_REGEX.matcher(regex).find();
	}

	/**
	 * Takes a Pattern and removes any quoted sections, baskslash-escaping any special characters within
	 * 
	 * @param s
	 * @return
	 */
	public static String fromQuotesToBackslash(String s) {
		int pos = s.indexOf("\\Q");
		if(pos < 0)
			return s;
		StringBuilder sb = new StringBuilder(s.length());
		int start = 0;
		while(true) {
			sb.append(s, start, pos);
			start = pos + 2;
			if(start >= s.length())
				break;
			pos = s.indexOf("\\E", start);
			if(pos < 0)
				pos = s.length(); // unmatched quote but assume end
			appendEscaped(s, start, pos, sb);
			start = pos + 2;
			if(start >= s.length())
				break;
			pos = s.indexOf("\\Q", start);
			if(pos < 0)
				pos = s.length();
		}
		return sb.toString();
	}

	public static String unescapeBackslash(String s) {
		int pos = s.indexOf('\\');
		if(pos < 0)
			return s;
		StringBuilder sb = new StringBuilder(s.length());
		int start = 0;
		OUTER: while(true) {
			sb.append(s, start, pos);
			if(pos + 1 >= s.length())
				break;
			char ch = s.charAt(pos + 1);
			start = pos + 2;
			switch(ch) {
				case 't':
					sb.append('\t');
					break;
				case 'b':
					sb.append('\b');
					break;
				case 'n':
					sb.append('\n');
					break;
				case 'f':
					sb.append('\f');
					break;
				case 'r':
					sb.append('\r');
					break;
				case 'u':
					char[] unicode = new char[4];
					for(int i = 0; i < 4; i++) {
						if(start >= s.length()) {
							sb.append("\\u");
							for(int k = 0; k < i; k++)
								sb.append(unicode[k]);
							break OUTER;
						}
						unicode[i] = s.charAt(start++);
					}
					sb.append((char) Integer.parseInt(new String(unicode), 16));
					break;
				default:
					sb.append(ch);
					break;
			}
			pos = s.indexOf('\\', start);
			if(pos < 0) {
				sb.append(s, start, s.length());
				break;
			}
		}
		return sb.toString();
	}
}
