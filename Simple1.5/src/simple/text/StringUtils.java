/*
 * StringUtils.java
 *
 * Created on February 4, 2003, 1:44 PM
 */

package simple.text;

import java.beans.IntrospectionException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.EncodingException;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Convertible;
import simple.bean.NestedNameTokenizer;
import simple.bean.ReflectionUtils;
import simple.bean.converters.ByteArrayConverter;
import simple.results.Results;
import simple.util.CollectionUtils;

/** Contains various String Utility methods
 * @author Brian Krug
 */
/**
 * @author bkrug
 *
 */
public class StringUtils {
	public static final Charset XML_CHARSET = Charset.forName("UTF-8");
	public static final String[] EMPTY_ARRAY = new String[0];
	public static final DateFormat dateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy"));
	public static class SortedChars implements CharSequence {
		protected final char[] chars;
		public SortedChars(char[] chars) {
			this.chars = chars;
			Arrays.sort(chars);
		}
		public char charAt(int index) {
			return chars[index];
		}
		public int length() {
			return chars.length;
		}
		public CharSequence subSequence(int start, int end) {
			return new String(chars, start, end-start);
		}
		public char[] getChars() {
			return chars.clone();
		}
		@Override
		public String toString() {
			return new String(chars);
		}
		public int indexOf(char c) {
			return Arrays.binarySearch(chars, c);
		}
		public boolean contains(char c) {
			return indexOf(c) >= 0;
		}
	}
    /**
     * Holds the standard list delimiter characters
     */
    public static final char[] STANDARD_DELIMS = ",;".toCharArray();
    /**
     * Holds the standard quote characters
     */
    public static final char[] STANDARD_QUOTES = "\"'".toCharArray();
    /**
     * Holds the standard whitespace characters
     */
    public static final char[] STANDARD_WHITESPACE = new char[] {9,10,11,12,13,28,29,30,31,32};

    /**
     * Indicates left justification
     */
    public static final int JUSTIFY_LEFT = 1;
    /**
     * Indicates center justification
     */
    public static final int JUSTIFY_CENTER = 2;
    /**
     * Indicates right justification
     */
    public static final int JUSTIFY_RIGHT = 3;

    public static enum Justification {LEFT, CENTER, RIGHT};

    /**
     * The space character
     */
    public static final char SPACE = ' ';

    /** Indicates no "preparation" */
    public static final int PREP_NONE = 0;
    /** Indicates "preparation" for HTML use */
    public static final int PREP_HTML = 1;
    /** Indicates "preparation" for JavaScript use */
    public static final int PREP_SCRIPT = 2;
    /** Indicates "preparation" for RTF use */
    public static final int PREP_RTF = 3;
    /** Indicates "preparation" for use in a URL */
    public static final int PREP_URL_PART = 4;
    /** Indicates "preparation" for HTML use while leaving blank as no data */
    public static final int PREP_HTML_BLANK = 5;
    /** Indicates "preparation" for CDATA use */
    public static final int PREP_CDATA = 6;
    
    public static final char FS = 0x1C;

    /** Never create new StringUtils */
    private StringUtils() {
    }

	/**
	 * Calculates the number of "steps" between the start characters and the end characters using the given set of "valid" characters.
	 * This method assumes that valids is sorted in ascending natural ordering.
	 * 
	 * @param start
	 * @param end
	 * @param valids
	 * @return
	 */
	public static BigInteger getGapBetween(char[] start, char[] end, char[] valids) {
		int len = Math.max(start.length, end.length);
		BigInteger total = BigInteger.ZERO;
		for(int i = 0; i < len; i++) {
			char s = (start.length > i ? start[i] : 0);
			char e = (end.length > i ? end[i] : 0);
			if(s > e && total.compareTo(BigInteger.ZERO) == 0)
				return null; // start is not less than end
			if(s != e) {
				int sindex = Arrays.binarySearch(valids, s);
				if(sindex < 0)
					sindex = -sindex - 1;
				int eindex = Arrays.binarySearch(valids, e);
				if(eindex < 0)
					eindex = -eindex - 1;
				BigInteger rest = BigInteger.valueOf(valids.length).pow(len - i - 1);
				if(sindex != eindex)
					rest = rest.multiply(BigInteger.valueOf(eindex - sindex));
				else if(s > e)
					rest = rest.negate();
				total = total.add(rest);
			}
		}
		return total;
	}

	public static boolean increment(char[] values, BigInteger times, char[] valids) {
		BigInteger max = BigInteger.valueOf(valids.length).pow(values.length);
		if(max.compareTo(times) <= 0) { // requested times is more than max
			Arrays.fill(values, valids[valids.length - 1]);
			return false;
		}
		int[] incs = new int[values.length];
		BigInteger d = BigInteger.valueOf(valids.length);
		BigInteger c = times;
		int n;
		for(n = incs.length - 1; c.compareTo(d) > 0; n--) {
			BigInteger[] r = c.divideAndRemainder(d);
			c = r[0];
			incs[n] = r[1].intValue();
		}
		incs[n] = c.intValue();
		for(n = values.length - 1; n >= 0; n--) {
			if(incs[n] > 0) {
				int index = Arrays.binarySearch(valids, values[n]);
				if(index < 0)
					index = -index - 1;
				index += incs[n];
				if(index >= valids.length) {
					if(n == 0) { // requested times is more than max
						Arrays.fill(values, valids[valids.length - 1]);
						return false;
					}
					incs[n - 1]++;
					index -= valids.length;
				}
				values[n] = valids[index];
			}
		}
		return true;
	}

	public static String reverse(String original) {
		if(original == null)
			return null;
		char[] ch = new char[original.length()];
		original.getChars(0, original.length(), ch, 0);
		for(int i = 0; i < ch.length / 2; i++) {
			char tmp = ch[ch.length - i - 1];
			ch[ch.length - i - 1] = ch[i];
			ch[i] = tmp;
		}
		return new String(ch);
	}
    public static String asCharacters(long num, char characterSetStart, int characterSetLength) {
    	char[] cs = new char[characterSetLength];
    	for(int i = 0; i < cs.length; i++)
    		cs[i] = (char)(characterSetStart + i);
    	return asCharacters(num, cs);
    }//*
    public static String asCharacters(long num, char[] characterSet) {
    	if(num == 0)
    		return "";
    	int pos;
    	char[] chars;
    	boolean neg = num < 0;
    	if(neg)
    		 num = -num;
    	if(num > characterSet.length) {
	    	pos = 1 + (int)Math.ceil(Math.log1p(num - characterSet.length) / Math.log(characterSet.length));
	    	if(neg) pos++;
	    	chars = new char[pos];
	    	while(num >= characterSet.length) {
	    		chars[--pos] = characterSet[(int)((num-1) % characterSet.length)];
	    	    num = (num - 1) / characterSet.length;
	    	}
    	} else {
    		chars = new char[1];
    		pos = 1;
    	}
    	if(num > 0)
    		chars[--pos] = characterSet[(int)num-1];
    	if(neg) chars[--pos] = '-';
    	return new String(chars, pos, chars.length - pos);
    }

	public static String trim(String original) {
		if(original != null)
			original = original.trim();
		return original;
	}

    public static String trimChars(String original, char[] chars) {
    	chars = chars.clone();
        Arrays.sort(chars);
    	int start = 0;
    	while(start < original.length() && Arrays.binarySearch(chars, original.charAt(start)) >= 0)
    		start++;

    	int end = original.length();
    	while(end > start && Arrays.binarySearch(chars, original.charAt(end-1)) >= 0)
    		end--;
        return original.substring(start, end);
    }
    /*/
    public static String asCharacters(long num, char[] characterSet) {
    	int pos;
    	char[] chars;
    	boolean neg = num < 0;
    	if(num > characterSet.length) {
	    	pos = 1 + (int)Math.ceil(Math.log1p(num - characterSet.length) / Math.log(characterSet.length));
	    	if(neg) pos++;
	    	chars = new char[pos];
	    	pos--;
	    	while(num >= characterSet.length) {
	    		chars[pos--] = characterSet[(int)((num-1) % characterSet.length)];
	    	    num = (num - 1) / characterSet.length;
	    	}
    	} else if(num == 0) {
    		return "";
    	} else {
    		chars = new char[1];
    		pos = 0;
    	}
    	chars[pos] = characterSet[(int)num-1];
    	if(neg) chars[--pos] = '-';
    	return new String(chars, pos, chars.length - pos);
    }*/
    /** Replaces each occurence of the specified String with the given String.
     * @param orig The string to search
     * @param find The string to find
     * @param replace The string to insert at each occurance of the <CODE>find</CODE> parameter
     * @return The modified result
     */
    public static String replace(String orig, String find, String replace) {
        StringBuilder result = new StringBuilder();
        int last = 0;
        while(true) {
            int next = orig.indexOf(find,last);
            if(next < 0) break;
            result.append(orig.substring(last,next));
            result.append(replace);
            last = next + find.length();
        }
        result.append(orig.substring(last));
        return result.toString();
    }

    /** Replaces each occurence of the specified Strings with the corresponding replacement String.
     * The array of "find" Strings and the array of "replace" Strings should be the same length. If
     * the "replace" array is shorter, the empty String is used to replace those elements of the
     * "find" array whose indices are beyond the highest index of the "replace" array.
     * @param orig The string to search
     * @param find The strings to find
     * @param replace The strings to insert at each occurance of the <CODE>find</CODE> parameter.
     * @return The modified result
     */
    public static String replace(String orig, String[] find, String[] replace) {
        for(int i = 0; i < find.length; i++) {
            String rep = (replace != null && i < replace.length ? replace[i] : "");
            orig = replace(orig, find[i], rep);
            /*int last = 0;
            while(true) {
                int next = orig.indexOf(find[i],last);
                if(next < 0) break;
                result.append(orig.substring(last,next));
                result.append(rep);
                last = next + find[i].length();
            }
            result.append(orig.substring(last));*/
        }
        //return result.toString();
        return orig;
    }

    /** Replaces each occurence of the specified characters with the given character.
     *  Designed for efficency.
     * @param orig - a StringBuffer of the original text
     * @param find - the characters to find
     * @param replace - the replacement character
     */
    public static void replace(StringBuffer orig, char[] find, char replace) {
        for(int i = 0; i < orig.length(); i++) {
            char c = orig.charAt(i);
            for(int k = 0; k < find.length; k++) if(c == find[k]) orig.setCharAt(i, replace);
        }
    }

    /** Replaces each occurence of the specified characters with the given character.
     *  Designed for efficency.
     * @param orig - a StringBuilder of the original text
     * @param find - the characters to find
     * @param replace - the replacement character
     */
    public static void replace(StringBuilder orig, char[] find, char replace) {
        for(int i = 0; i < orig.length(); i++) {
            char c = orig.charAt(i);
            for(int k = 0; k < find.length; k++)
            	if(c == find[k]) {
            		orig.setCharAt(i, replace);
            		break;
            	}
        }
    }

    /** Converts a tab-separated InputStream into a list of String arrays
     * @param is The InputStream that has tab-separated data
     * @throws IOException If the InputStream can not be read
     * @return A List of String arrays
     */
    public static List<String[]> fromTabbed(InputStream is) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        List<String[]> list = new ArrayList<String[]>();
        String line;
        try {
					while((line = in.readLine()) != null) list.add(StringUtils.split(line,'\t','"'));
				} catch (IOException e) {
					throw e;
				} finally {
					in.close();
				}
        return list;
    }

    /** Converts a CSV-formatted InputStream into a list of String arrays
     * @param is The InputStream that has CSV-formatted data
     * @throws IOException If the InputStream can not be read
     * @return A List of String arrays
     */
    public static List<String[]> fromCSV(InputStream is) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        List<String[]> list = new ArrayList<String[]>();
        String line;
        try {
					while((line = in.readLine()) != null) list.add(StringUtils.split(line,',','"', true));
				} catch (IOException e) {
					throw e;
				} finally {
					in.close();
				}
        return list;
    }

    /**
     * Writes a ResultSet to an OutputStream in CSV-format
     * @param rs The ResultSet
     * @param os The OutputStream
     * @throws IOException If the OutputStream can not be written to
     * @throws SQLException If the ResultSet throws an exception
     */
	public static int writeCSV(final java.sql.ResultSet rs, OutputStream os) throws java.sql.SQLException {
		return writeCSV(rs, os, false);
    }

    /**
     * Writes a ResultSet to an OutputStream in CSV-format
     * @param header Whether to include the column names as a header or not
     * @param rs The ResultSet
     * @param os The OutputStream
     * @throws IOException If the OutputStream can not be written to
     * @throws SQLException If the ResultSet throws an exception
     */
	public static int writeCSV(final java.sql.ResultSet rs, OutputStream os, boolean header) throws java.sql.SQLException {
		return writeCSV(rs, new PrintWriter(os), header);
    }

    /**
     * Writes a ResultSet to a PrintWriter in CSV-format
     * @param header Whether to include the column names as a header or not
     * @param rs The ResultSet
     * @param pw The PrintWriter
     * @throws IOException If the OutputStream can not be written to
     * @throws SQLException If the ResultSet throws an exception
     */
	public static int writeCSV(final java.sql.ResultSet rs, PrintWriter pw, boolean header) throws java.sql.SQLException {
        if(header) {
            java.sql.ResultSetMetaData md = rs.getMetaData();
            String[] names = new String[md.getColumnCount()];
            for(int i = 0; i < names.length; i++) {
                names[i] = md.getColumnName(i+1);
            }
            writeCSVLine(names, pw);
        }
		AtomicInteger count = new AtomicInteger();
        writeCSV(new java.util.Iterator<Object[]>() {
            private boolean next = rs.next();
            private final int num = rs.getMetaData().getColumnCount();
            public boolean hasNext() { return next; }
            public Object[] next() {
                Object[] values = new Object[num];
                try {
                    for(int i = 0; i < num; i++) values[i] = rs.getObject(i+1);
					count.incrementAndGet();
                    next = rs.next();
                } catch(java.sql.SQLException sqle) {
                    throw new RuntimeException("While retrieving ResultSet data values", sqle);
                }
                return values;
            }
            public void remove() {}
        }, pw);
        pw.flush();
		return count.get();
    }

    /**
     * Writes a Results Object to an OutputStream in CSV-format
     * @param header Whether to include the column names as a header or not
     * @param results The Results
     * @param os The OutputStream
     * @throws IOException If the OutputStream can not be written to
     * @throws SQLException If the ResultSet throws an exception
     */
    public static void writeCSV(final Results results, OutputStream os, boolean header) {
        writeCSV(results, new PrintWriter(os), header);
    }

    /**
     * Writes a Results Object to an OutputStream in CSV-format
     * @param header Whether to include the column names as a header or not
     * @param results The Results
     * @param pw The PrintWriter
     * @throws IOException If the OutputStream can not be written to
     * @throws SQLException If the ResultSet throws an exception
     */
    public static void writeCSV(final Results results, PrintWriter pw, boolean header) {
		if(header) {
            String[] names = new String[results.getColumnCount()];
            for(int i = 0; i < names.length; i++) {
                names[i] = results.getColumnName(i+1);
            }
            writeCSVLine(names, pw);
        }
        writeCSV(new java.util.Iterator<Object[]>() {
            private boolean next = results.next();
            private final int num = results.getColumnCount();
            public boolean hasNext() { return next; }
            public Object[] next() {
                Object[] values = new Object[num];
                for(int i = 0; i < num; i++) values[i] = results.getValue(i+1);
                next = results.next();
                return values;
            }
            public void remove() {}
        }, pw);
        pw.flush();
    }

	public static void writeCSV(final Results results, PrintWriter pw, boolean header, final int[] columnIndexes) {
		if(header) {
			String[] names = new String[columnIndexes.length];
			for(int i = 0; i < names.length; i++) {
				names[i] = results.getColumnName(columnIndexes[i]);
			}
			writeCSVLine(names, pw);
		}
		writeCSV(new java.util.Iterator<Object[]>() {
			private boolean next = results.next();
			private final int num = columnIndexes.length;

			public boolean hasNext() {
				return next;
			}

			public Object[] next() {
				Object[] values = new Object[num];
				for(int i = 0; i < num; i++)
					values[i] = results.getValue(columnIndexes[i]);
				next = results.next();
				return values;
			}

			public void remove() {
			}
		}, pw);
		pw.flush();
	}

    /**
     * Writes an Iterator of Object arrays to an OutputStream in CSV-format
     * @param iter The iterator containing the Object arrays
     * @param os The OutputStream to which to write
     * @throws IOException If the OutputStream can not be written to
     */
    public static void writeCSV(java.util.Iterator<Object[]> iter, OutputStream os) {
        writeCSV(iter, new PrintWriter(os));
    }

    /** Writes an Iterator of Object arrays to a PrintStream in CSV-format
     * @param iter The iterator containing the Object arrays
     * @param out The PrintWriter to which to write
     * @throws IOException If the OutputStream can not be written to
     */
    public static void writeCSV(java.util.Iterator<Object[]> iter, PrintWriter out) {
        while(iter.hasNext()) writeCSVLine(iter.next(), out);
        out.flush();
    }

    /** Writes the specified Object array to the given PrintStream in CSV-format (Comma-delimited values).
     *  Numbers, Dates and Boolean objects are written directly to the PrintStream, but
     * other objects are converted to String and quoted and appropriately escaped.
     * @param data The array of Objects that is the data for this line
     * @param out The PrintStream to which to write
     * @throws IOException If the OutputStream can not be written to
     */
    public static void writeCSVLine(Object[] data, PrintStream out) {
        PrintWriter pw = new PrintWriter(out);
        writeCSVLine(data, pw);
        pw.flush();
    }

    /** Writes the specified Object array to the given PrintStream in CSV-format (Comma-delimited values).
     *  Numbers, Dates and Boolean objects are written directly to the PrintStream, but
     * other objects are converted to String and quoted and appropriately escaped.
     * @param data The array of Objects that is the data for this line
     * @param out The PrintWriter to which to write
     * @throws IOException If the OutputStream can not be written to
     */
    public static void writeCSVLine(Object[] data, PrintWriter out) {
    	writeCSVLine(data, out, false);
    }
    /** Writes the specified Object array to the given PrintStream in CSV-format (Comma-delimited values).
     *  Numbers, Dates and Boolean objects are written directly to the PrintStream, but
     * other objects are converted to String and quoted and appropriately escaped.
     * @param data The array of Objects that is the data for this line
     * @param out The PrintWriter to which to write
     * @param avoidQuotes if <code>true</code> then only use quotes if value contains a comma
     * @throws IOException If the OutputStream can not be written to
     */
    public static void writeCSVLine(Object[] data, PrintWriter out, boolean avoidQuotes) {
        for(int i = 0; i < data.length; i++) {
            if(i > 0) out.print(',');
            Object o;
            if(data[i] instanceof Convertible)
            	try {
            		o = ((Convertible)data[i]).convert(Object.class);
            	} catch(ConvertException e) {
            		o = data[i];
            	}
        	else
        		o = data[i];
        	if(o instanceof java.util.Date
                || o instanceof Number
                || o instanceof Boolean) out.print(o);
            else if(o != null) {
            	String s = ConvertUtils.getStringSafely(o);
            	if(s != null) {
	            	if(avoidQuotes && s.indexOf(',') < 0) {
	            		out.print(s);
	            	} else {
		            	out.print("\"");
		            	out.print(escapeCSVValue(s));
		            	out.print("\"");
	            	}
            	}
            }
        }
        out.println();
    }
    
    public static String escapeCSVValue(String value) {
    	if(value == null) return "";
    	/*
    	StringBuilder sb = new StringBuilder(value.length());
    	for(int i = 0; i < value.length(); i++) {
    		char ch = value.charAt(i);
    		switch(ch) {
    			case '"':
    				sb.append("\"\"");
    				break;
    			case '\r': case '\n':
    				sb.append(' ');
    				break;
    			default:
    				sb.append(ch);
    		}
    	}
        return sb.toString();
        */
    	return replace(value, '"', "\"\"");
    }

    public static String replace(String original, char find, String replace) {
    	int lastPos = 0;
    	StringBuilder sb = null;
    	while(true) {
    		int pos = original.indexOf(find, lastPos);
    		if(pos == -1) {
    			if(lastPos == 0)
    				return original;
    			else {
    				return sb.append(original.substring(lastPos)).toString();
    			}
    		} else {
    			if(lastPos == 0){
    				sb = new StringBuilder(original.length());
    			}
    			sb.append(original.substring(lastPos, pos)).append(replace);
    			lastPos = pos+1;
    		}
        }
    }
    public static String quote(String original, char quote) {
    	return new StringBuilder().append(quote).append(replace(original, quote, "" + quote + quote)).append(quote).toString();
    }
    /**
     * Finds each occurance of the escapees chars and inserts the specified escaper char
     *  before each one. Each occurance of the escaper char is replaced with a pair of that char.
     * @param original The original string
     * @param escapees The characters that must be escaped
     * @param escaper The escape character
     * @return The string with each escapee character prefixed with the specified escape character
     */
    public static String escape(String original, char[] escapees, char escaper) {
        StringBuilder sb = new StringBuilder(original);
        escapees = escapees.clone();
        Arrays.sort(escapees);
        for(int pos = 0; pos < original.length(); pos++) {
            if(Arrays.binarySearch(escapees,original.charAt(pos)) >= 0) {
                sb.insert(pos + (sb.length()-original.length()),escaper);
            }
        }
        return sb.toString();
    }

    /** Splits the given String into an array of Strings using any of the listed delimiters.
     * @param original
     * @param delimiters
     * @param keepEmptySplits
     * @return An array of Strings
     */
    public static String[] split(String original, char[] delimiters, boolean keepEmptySplits) {
		if(original == null)
			return null;
    	List<String> l = new ArrayList<String>();
		split(original, delimiters, keepEmptySplits, l);
		String[] retVal = l.toArray(new String[l.size()]);
		return retVal;
	}

	public static void split(String original, char[] delimiters, boolean keepEmptySplits, Collection<String> addTo) {
		if(isBlank(original))
			return;
    	delimiters = delimiters.clone();
        Arrays.sort(delimiters);
        int start = 0, pos = 0;
        for(; pos < original.length(); pos++) {
            if(Arrays.binarySearch(delimiters,original.charAt(pos)) >= 0) {
				if(keepEmptySplits || pos > start)
					addTo.add(original.substring(start, pos));
                start = pos+1;
            }
        }
		if(keepEmptySplits || pos > start)
			addTo.add(original.substring(start, pos));
    }

    /** Splits the given String into an array of Strings using the given delimiter.
     * @param original
     * @param delimiter
     * @return
     */
    public static String[] split(String original, char delimiter) {
    	if(original == null) return null;
    	List<String> l = new ArrayList<String>();
		int start = 0, end = 0;
		for(; start < original.length(); start = end + 1) {
            end = original.indexOf(delimiter,start);
			if(end < 0)
				end = original.length();
            l.add(original.substring(start,end));
        }
		if(end == original.length() - 1)
			l.add("");
        String[] retVal = l.toArray(new String[l.size()]);
        return retVal;
    }

    /** Splits the given String into an array of Strings using the given delimiter.
     * @param original
     * @param delimiter
     * @return
     */
    public static String[] split(String original, char delimiter, int limit) {
    	if(original == null) return null;
    	List<String> l = new ArrayList<String>();    	
		int start = 0, end = 0, i = 0;
		for(; start < original.length(); start = end + 1, i++) {
            end = original.indexOf(delimiter,start);
            if(end < 0) {
            	l.add(original.substring(start));
            	break;
            } else {
            	l.add(original.substring(start,end));
            	if(limit > 0 && i+1 >= limit) {
            		l.add(original.substring(end+1));
                	break;
				} else if(end == original.length() - 1)
					l.add("");
            }  	
        }
		String[] retVal = l.toArray(new String[l.size()]);
        return retVal;
    }
    
    /** Splits the given String into an array of Strings using the given delimiter.
     * @param original
     * @param delimiter
     * @return
     */
    public static String[] split(String original, String delimiter) {
    	if(original == null) return null;
    	List<String> l = new ArrayList<String>();
		int start = 0, end = 0;
		for(; start < original.length(); start = end + delimiter.length()) {
            end = original.indexOf(delimiter,start);
            if(end < 0) end = original.length();
            l.add(original.substring(start,end));
        }
		if(end == original.length() - 1)
			l.add("");
        String[] retVal = l.toArray(new String[l.size()]);
        return retVal;
    }

    /** Splits the given String into an array of Strings using the given delimiter but
     *  ignoring the delimiter if it is between the given quote char.
     * @param original
     * @param delimiter
     * @param quote
     * @return
     */
    public static String[] split(String original, char delimiter, char quote) {
    	if(original == null) return null;
    	List<String> l = new ArrayList<String>();
        for(int start = 0, end = 0; start < original.length(); start = end + 1) {
            String word = null;
            if(original.charAt(start) == quote) {
                start++;
                for(end = start; end < original.length(); end++) {
                    if(original.charAt(end) == quote)
                        if(end+1 == original.length()) break;
                        else if(original.charAt(end+1) == quote) end++;
                        else if(original.charAt(end+1) == delimiter) {
                            word = replace(original.substring(start, end), "" + quote + quote, "" + quote);
                            end++;
                            break;
                        } else {
                            int pos = end;
                            end = original.indexOf(delimiter,end+1);
                            if(end < 0) end = original.length();
                            word = replace(original.substring(start, pos), "" + quote + quote, "" + quote) + original.substring(pos, end);
                            break;
                        }
                }
                if(word == null) word = replace(original.substring(start, end), "" + quote + quote, "" + quote);
            } else {
                end = original.indexOf(delimiter,start);
                if(end < 0) end = original.length();
                word = original.substring(start,end);
            }
            l.add(word);
            if(end == original.length() - 1) l.add(""); // delimiter is last character so add an empty word
        }
        String[] retVal = l.toArray(new String[l.size()]);
        return retVal;
    }

	public static String[] splitQuotesAnywhere(String original, char delimiter, char quote, boolean condenseQuotes) {
		if(original == null)
			return null;
		List<String> l = new ArrayList<String>();
		splitQuotesAnywhere(original, delimiter, quote, condenseQuotes, l);
		String[] retVal = l.toArray(new String[l.size()]);
		return retVal;
	}

	public static void splitQuotesAnywhere(String original, char delimiter, char quote, boolean condenseQuotes, Collection<String> addTo) {
		if(original == null)
			return;
		StringBuilder sb = new StringBuilder();
		boolean inquote = false;
		for(int i = 0; i < original.length(); i++) {
			char ch = original.charAt(i);
			if(ch == quote) {
				if(condenseQuotes && !inquote && i + 1 < original.length() && quote == original.charAt(i + 1)) {
					sb.append(ch);
					i++;
					continue;
				}
				if(!condenseQuotes)
					sb.append(ch);
				inquote = !inquote;
			} else if(ch == delimiter && !inquote) {
				addTo.add(sb.toString());
				sb.setLength(0);
			} else
				sb.append(ch);
		}
		if(sb.length() > 0)
			addTo.add(sb.toString());
	}

	public static String[] splitQuotesAnywhere(String original, String delimiters, String quotes, boolean condenseQuotes) {
		if(original == null)
			return null;
		List<String> l = new ArrayList<String>();
		splitQuotesAnywhere(original, delimiters, quotes, condenseQuotes, l);
		String[] retVal = l.toArray(new String[l.size()]);
		return retVal;
	}

	public static void splitQuotesAnywhere(String original, String delimiters, String quotes, boolean condenseQuotes, Collection<String> addTo) {
		if(original == null)
			return;
		StringBuilder sb = new StringBuilder();
		char[] delimArray = delimiters.toCharArray();
		Arrays.sort(delimArray);
		char[] quoteArray = quotes.toCharArray();
		Arrays.sort(quoteArray);

		for(int i = 0; i < original.length(); i++) {
			char ch = original.charAt(i);
			if(Arrays.binarySearch(quoteArray, ch) >= 0) {
				int pos = original.indexOf(ch, i + 1);
				if(pos < 0) {
					sb.append(original, i, original.length());
					break;
				}
				sb.append(original, i, pos);
				if(!condenseQuotes || original.charAt(pos + 1) != ch)
					sb.append(ch);
				i = pos;
			} else if(Arrays.binarySearch(delimArray, ch) >= 0) {
				addTo.add(sb.toString());
				sb.setLength(0);
			} else
				sb.append(ch);
		}
		addTo.add(sb.toString());
	}

    protected static enum CharacterRole { DELIMITER, QUOTE, BEGIN_GROUP, END_GROUP };
    public static class ParsingCharacters {
    	protected final Map<Character, CharacterRole> roles = new HashMap<Character, CharacterRole>();
    	protected final Map<Character, Character> groupings = new HashMap<Character, Character>();

    	public void clear() {
    		roles.clear();
    	}
    	public void addDelimiter(char delimiter) {
    		roles.put(delimiter, CharacterRole.DELIMITER);
    	}
    	public void addQuote(char quote) {
    		roles.put(quote, CharacterRole.QUOTE);
    	}
    	public void addGrouping(char beginGroup, char endGroup) {
    		roles.put(beginGroup, CharacterRole.BEGIN_GROUP);
    		roles.put(endGroup, CharacterRole.END_GROUP);
    		groupings.put(beginGroup, endGroup);
    	}
    	public void addDelimiters(char[] delimiters) {
    		for(char ch : delimiters)
    			addDelimiter(ch);
    	}
    	public void addQuotes(char[] quotes) {
    		for(char ch : quotes)
    			addQuote(ch);
    	}
    }
    /** Finds the next occurance of the specified <code>delimiter</code> (accounting for the specified quote characters)
     *  in the given CharBuffer (starting at the CharBuffer's current position)
     *  and returns the String before that occurance. The CharBuffer's position is left at the index immediately after the delimiter. If the delimiter
     *  is not found, the remainder of the input is returned and the CharBuffer's position is its limit.
     * @param input
     * @param delimiter
     * @param quotes
     * @return
     */
    public static String stringBefore(CharBuffer input, char delimiter, char[] quotes, char[] startGroup, char[] endGroup) {
    	int start = input.position();
    	while(input.hasRemaining()) {
    		char ch = input.get();
    		int a;
    		if(ch == delimiter) {
    			char[] result = new char[input.position() - start - 1];
    			for(int i = 0; i < result.length; i++)
    				result[i] = input.get(start + i);
    			return new String(result);
    		} else if(quotes != null && (a=CollectionUtils.iterativeSearch(quotes, ch)) >= 0) {
    			while(input.hasRemaining())
    				if(input.get() == quotes[a])
    					break;
    		} else if(startGroup != null && (a=CollectionUtils.iterativeSearch(startGroup, ch)) >= 0) {
    			findEndOfGrouping(input, quotes, startGroup, endGroup, a);
    		}
    	}
    	return null;
    }

    protected static void findEndOfGrouping(CharBuffer input, char[] quotes, char[] startGroup, char[] endGroup, int groupIndex) {
    	while(input.hasRemaining()) {
			char ch = input.get();
    		int a;
    		if(ch == endGroup[groupIndex]) {
    			return;
    		} else if(quotes != null && (a=CollectionUtils.iterativeSearch(quotes, ch)) >= 0) {
    			while(input.hasRemaining())
    				if(input.get() == quotes[a])
    					break;
    		} else if(startGroup != null && (a=CollectionUtils.iterativeSearch(startGroup, ch)) >= 0) {
    			findEndOfGrouping(input, quotes, startGroup, endGroup, a);
    		}
    	}
    }

    /** Finds the next occurance of the specified <code>delimiter</code> (accounting for the specified quote characters)
     *  in the given CharBuffer (starting at the CharBuffer's current position)
     *  and returns the String before that occurance. The CharBuffer's position is left at the index immediately after the delimiter. If the delimiter
     *  is not found, the remainder of the input is returned and the CharBuffer's position is its limit.
     * @param input
     * @param parsingCharacters
     * @return
     */
    public static String stringBefore(CharBuffer input, ParsingCharacters parsingCharacters) throws ParseException {
    	int start = input.position();
    	while(input.hasRemaining()) {
    		char ch = input.get();
    		CharacterRole role = parsingCharacters.roles.get(ch);
    		if(role != null) {
    			switch(role) {
    				case DELIMITER:
    					char[] result = new char[input.position() - start - 1];
    	    			for(int i = 0; i < result.length; i++)
    	    				result[i] = input.get(start + i);
    	    			return new String(result);
    				case QUOTE:
    					boolean found = false;
    					int p = input.position() - 1;
    					while(input.hasRemaining())
    	    				if(input.get() == ch) {
    	    					found = true;
    	    					break;
    	    				}
    					if(!found)
    						throw new ParseException("Unmatched quote character '" + ch + "'", p);
    					break;
    				case BEGIN_GROUP:
    					findEndOfGrouping(input, parsingCharacters, parsingCharacters.groupings.get(ch));
    					break;
    				case END_GROUP:
    					throw new ParseException("Unmatched end group character '" + ch + "'", input.position() - 1);
    			}
    		}
    	}
    	char[] result = new char[input.position() - start];
		for(int i = 0; i < result.length; i++)
			result[i] = input.get(start + i);
		return new String(result);
    }

    protected static void findEndOfGrouping(CharBuffer input, ParsingCharacters parsingCharacters, char endGroup) throws ParseException {
    	while(input.hasRemaining()) {
			char ch = input.get();
    		if(ch == endGroup) {
    			return;
    		} else {
    			CharacterRole role = parsingCharacters.roles.get(ch);
        		if(role != null) {
        			switch(role) {
        				case QUOTE:
        					boolean found = false;
        					int p = input.position() - 1;
        					while(input.hasRemaining())
        	    				if(input.get() == ch) {
        	    					found = true;
        	    					break;
        	    				}
        					if(!found)
        						throw new ParseException("Unmatched quote character '" + ch + "'", p);
        					break;
        				case BEGIN_GROUP:
        					findEndOfGrouping(input, parsingCharacters, parsingCharacters.groupings.get(ch));
        					break;
        				case END_GROUP:
        					throw new ParseException("Unmatched end group character '" + ch + "'", input.position() - 1);
        			}
        		}
    		}
    	}
    }

    public static String[] split(String original, char delimiter, char quote, boolean trimUnquoted) {
    	if(original == null) return null;
    	List<String> l = new ArrayList<String>();
    	for(int start = 0, end = 0; start < original.length(); start = end + 1) {
            String word = null;
            if(trimUnquoted)
            	while(start < original.length() && Character.isWhitespace(original.charAt(start)))
            		start++;
            if(start < original.length() && original.charAt(start) == quote) {
                start++;
                boolean inquote = true;
                for(end = start; end < original.length() - 1; end++) {
                    if(original.charAt(end) == quote) {
                    	word = (word == null ? original.substring(start, end) : word + original.substring(start, end));
                    	start = end + 1;
                    	if(original.charAt(start) == quote) {
                    		end++;
                    	} else {
                    		end = original.indexOf(delimiter, start);
                            if(end < 0) end = original.length();
                            inquote = false;
                            break;
                    	}
                    }
                }
                //check last char
                int pos = end;
                if(end == original.length() - 1) {
                	char lastCh = original.charAt(end);
                	if(inquote && start < end && lastCh == quote) {
                		pos = end;
                		end++;
                	} else if(inquote || start < end || lastCh != delimiter) {
                		end++;
                		pos = end;
                	}
                	/* below is every case, we condense for efficiency
                	} else if(!inquote && start == end && lastCh == delimiter) {
                		pos = end;
                	} else if(inquote && start == end && lastCh == quote) {
                		end++;
                		pos = end;
                	} else if(inquote && start < end && lastCh == delimiter) {
                		end++;
                		pos = end;
                	} else if(!inquote && start == end && lastCh != delimiter && lastCh != quote) {
                		end++;
                		pos = end;
                	} else if(inquote && start < end && lastCh != delimiter && lastCh != quote) {
                		end++;
                		pos = end;
                	}*/
                }
                if(trimUnquoted && !inquote)
                	while(pos > start && Character.isWhitespace(original.charAt(pos-1)))
                		pos--;

                word = (word == null ? original.substring(start, pos) : word + original.substring(start, pos));
            } else {
                end = original.indexOf(delimiter,start);
                if(end < 0) end = original.length();
                int pos = end;
                if(trimUnquoted)
                	while(pos > start && Character.isWhitespace(original.charAt(pos-1)))
                		pos--;
                word = original.substring(start,pos);
            }
            l.add(word);
            if(end == original.length() - 1) l.add(""); // delimiter is last character so add an empty word
        }
        String[] retVal = l.toArray(new String[l.size()]);
        return retVal;
    }
   /** Splits the given String into an array of Strings using the given delimiters but
     *  ignoring the delimiters if they are between the given quote chars.
     * @param original
     * @param delimiters
     * @param quotes
     * @return
     */
    public static String[] split(String original, char[] delimiters, char[] quotes) {
    	if(original == null) return null;
    	List<String> l = new ArrayList<String>();
    	delimiters = delimiters.clone();
        Arrays.sort(delimiters);
        quotes = quotes.clone();
        Arrays.sort(quotes);
        StringBuilder sb = new StringBuilder();
        for(int pos = 0; pos < original.length(); pos++) {
            char ch = original.charAt(pos);
            if(Arrays.binarySearch(quotes,ch) >= 0) {//find quotes
                while(true) {
                    int end = original.indexOf(ch, pos+1);
                    if(end == -1) end = original.length();
                    sb.append(original.substring(pos+1, end));
                    pos = end;
                    //check for double quote char
                    if(pos + 1 < original.length() && original.charAt(pos+1) == ch) {
                        sb.append(ch);
                        pos++;
                    } else break;
                }
            } else if(Arrays.binarySearch(delimiters,ch) >= 0) {//find delim
                l.add(sb.toString());
                sb.setLength(0);
            } else {
                sb.append(ch);
            }
        }
        l.add(sb.toString());
        String[] retVal = l.toArray(new String[l.size()]);
        return retVal;
    }

    /** Parses the given String into key-value pairs and puts them into the map
     * @param map
     * @param propertiesString
     * @throws ParseException
     */
    public static void intoMap(Map<? super String, ? super String> map, String propertiesString) throws ParseException {
    	intoMap(map, propertiesString, '=', ';', STANDARD_QUOTES, true, false);
    }

    /** Parses the given String into key-value pairs and puts them into the map; if hasFinalValue is true then the remaining String is returned
     *  after all parsing
     * @param map
     * @param propertiesString
     * @param keyValueSeparator
     * @param entrySeparator
     * @param quotes
     * @param trimNonQuoted
     * @param hasFinalValue
     * @return The remaining unmatched portion of the String (if hasFinalValue is true)
     * @throws ParseException
     */
    public static String intoMap(Map<? super String, ? super String> map, String propertiesString, char keyValueSeparator, char entrySeparator, char[] quotes, boolean trimNonQuoted, boolean hasFinalValue) throws ParseException {
    	if(propertiesString == null || propertiesString.indexOf(keyValueSeparator) < 1) return null;
    	StringBuilder sb = new StringBuilder();
    	sb.append("(?:");
    	for(char q : quotes)
    		sb.append("(?:\\s*\\").append(q).append("([^\\").append(q).append("]*(?:\\").append(q).append("{2}[^\\")
    			.append(q).append("]*)*)\\").append(q).append("\\s*)|");
    	sb.append("([^\\").append(keyValueSeparator).append("\\").append(entrySeparator).append("]*))");
    	String partRE = sb.toString();
        sb.append('\\').append(keyValueSeparator).append(partRE).append("(?:$|\\").append(entrySeparator).append(')');
        //sb.append('\\').append(keyValueSeparator).append(partRE).append("\\").append(entrySeparator);
    	Matcher matcher = Pattern.compile(sb.toString()).matcher(propertiesString);
    	int index = 0;
    	while(matcher.lookingAt()) {
    		String key = null;
    		String value = null;
    		for(int i = 1; i <= quotes.length + 1; i++)
    			if(matcher.start(i) > -1) {
    				key = matcher.group(i);
    				if(trimNonQuoted && i == quotes.length + 1)
    					key = key.trim();
    				break;
    			}
    		for(int i = quotes.length + 2; i <= (quotes.length + 1) * 2; i++)
    			if(matcher.start(i) > -1) {
    				value = matcher.group(i);
    				if(trimNonQuoted && i == (quotes.length + 1) * 2)
    					value = value.trim();
    				break;
    			}

    		map.put(key, value);
    		index = matcher.end();
    		matcher.region(index, propertiesString.length());
    	}
    	if(hasFinalValue) {
    		matcher = Pattern.compile(partRE).matcher(propertiesString);
    		matcher.region(index, propertiesString.length());
    		if(matcher.matches()) {
	    		String value = null;
	    		for(int i = 1; i <= quotes.length + 1; i++)
	    			if(matcher.start(i) > -1) {
	    				value = matcher.group(i);
	    				if(trimNonQuoted && i == quotes.length + 1)
	    					value = value.trim();
	    				return value;
	    			}
    		}
    		throw new ParseException("Could not parse properties string at " + index, index);
    	} else if(index < propertiesString.length() && propertiesString.substring(index).trim().length() > 0) {
    		throw new ParseException("Could not parse properties string at " + index, index);
    	} else {
    		return null;
    	}
    }

	/**
	 * Parses the given String into key-value pairs and puts them into the map; if hasFinalValue is true then the remaining String is returned
	 * after all parsing
	 * 
	 * @param map
	 * @param propertiesString
	 * @param keyValueSeparator
	 * @param entrySeparator
	 * @param quotes
	 * @param trimNonQuoted
	 * @param hasFinalValue
	 * @return The remaining unmatched portion of the String (if hasFinalValue is true)
	 * @throws ParseException
	 */
	public static String intoMap(Map<? super String, ? super String> map, String propertiesString, String keyValueSeparator, String entrySeparator, char[] quotes, boolean trimNonQuoted, boolean hasFinalValue) throws ParseException {
		if(propertiesString == null || propertiesString.indexOf(keyValueSeparator) < 1)
			return null;
		StringBuilder sb = new StringBuilder();
		sb.append("(?:");
		for(char q : quotes)
			sb.append("(?:\\s*\\").append(q).append("([^\\").append(q).append("]*(?:\\").append(q).append("{2}[^\\").append(q).append("]*)*)\\").append(q).append("\\s*)|");
		String partRE = sb.toString();
		sb.append("((?:(?!").append(Pattern.quote(keyValueSeparator)).append(").)*))");
		sb.append(Pattern.quote(keyValueSeparator)).append(partRE).append("((?:(?!").append(Pattern.quote(entrySeparator)).append(").)*))(?:$|").append(Pattern.quote(entrySeparator)).append(')');
		Matcher matcher = Pattern.compile(sb.toString()).matcher(propertiesString);
		int index = 0;
		while(matcher.lookingAt()) {
			String key = null;
			String value = null;
			for(int i = 1; i <= quotes.length + 1; i++)
				if(matcher.start(i) > -1) {
					key = matcher.group(i);
					if(trimNonQuoted && i == quotes.length + 1)
						key = key.trim();
					break;
				}
			for(int i = quotes.length + 2; i <= (quotes.length + 1) * 2; i++)
				if(matcher.start(i) > -1) {
					value = matcher.group(i);
					if(trimNonQuoted && i == (quotes.length + 1) * 2)
						value = value.trim();
					break;
				}

			map.put(key, value);
			index = matcher.end();
			matcher.region(index, propertiesString.length());
		}
		if(hasFinalValue) {
			matcher = Pattern.compile(partRE).matcher(propertiesString);
			matcher.region(index, propertiesString.length());
			if(matcher.matches()) {
				String value = null;
				for(int i = 1; i <= quotes.length + 1; i++)
					if(matcher.start(i) > -1) {
						value = matcher.group(i);
						if(trimNonQuoted && i == quotes.length + 1)
							value = value.trim();
						return value;
					}
			}
			throw new ParseException("Could not parse properties string at " + index, index);
		} else if(index < propertiesString.length() && propertiesString.substring(index).trim().length() > 0) {
			throw new ParseException("Could not parse properties string at " + index, index);
		} else {
			return null;
		}
	}
	public static int indexOfSequence(char[] chars, int start, int length, char[] sequence) {
		final int end = start + length;
		int match = 0;
		for(int i = start; i < end; i++) {
			if(chars[i] == sequence[match]) {
				if(++match >= sequence.length)
					return i - sequence.length + 1;
			} else
				match = 0;
		}
		return -1;
	}

    /** Returns the last index of any of the find char's within the given String
     * @param s
     * @param find
     * @return
     */
    public static int lastIndexOf(CharSequence s, char[] find) {
        return lastIndexOf(s,find,s.length()-1);
    }

    /**
     * @param s
     * @param find
     * @param offset
     * @return
     */
    public static int lastIndexOf(CharSequence s, char[] find, int offset) {
    	find = find.clone();
        Arrays.sort(find);
        for(int i = offset; i >= 0; i--) {
            if(Arrays.binarySearch(find,s.charAt(i)) >= 0) return i;
        }
        return -1;
    }

    /**
     * @param s
     * @param find
     * @return
     */
    public static int indexOf(CharSequence s, char[] find) {
        return indexOf(s,find,0);
    }

    /**
     * @param s
     * @param find
     * @param offset
     * @return
     */
    public static int indexOf(CharSequence s, char[] find, int offset) {
    	find = find.clone();
        Arrays.sort(find);
        for(int i = offset; i < s.length(); i++) {
            if(Arrays.binarySearch(find,s.charAt(i)) >= 0) return i;
        }
        return -1;
    }

    /**
     * @param s
     * @param find
     * @return
     */
    public static int indexOf(CharSequence s, Set<Character> find) {
        return indexOf(s,find,0);
    }

    /**
     * @param s
     * @param find
     * @param offset
     * @return
     */
    public static int indexOf(CharSequence s, Set<Character> find, int offset) {
        for(int i = offset; i < s.length(); i++) {
            if(find.contains(s.charAt(i))) return i;
        }
        return -1;
    }

	public static String generateList(String prefix, String suffix, String separator, int start, int count, int first, int last) {
		StringBuilder sb = new StringBuilder();
		int digits = 1 + (int) Math.floor(Math.log10(last));
		if(start <= 0)
			start = last - (start % (last - first + 1));
		else if(start < first || start > last)
			start = ((start - 1) % (last - first + 1)) + first;
		for(int i = 0; i < count; i++) {
			if(i > 0)
				sb.append(separator);
			if(prefix != null)
				sb.append(prefix);
			StringUtils.appendPadded(sb, String.valueOf(start), '0', digits, Justification.RIGHT);
			if(suffix != null)
				sb.append(suffix);
			start = (start % (last - first + 1)) + first;
		}
		return sb.toString();
	}
	public static String generateReverseList(String prefix, String suffix, String separator, int start, int count, int first, int last) {
		StringBuilder sb = new StringBuilder();
		int digits = 1 + (int) Math.floor(Math.log10(last));
		if(start <= 0)
			start = last - (start % (last - first + 1));
		else if(start < first || start > last)
			start = ((start - 1) % (last - first + 1)) + first;
		for(int i = 0; i < count; i++) {
			if(i > 0)
				sb.append(separator);
			if(prefix != null)
				sb.append(prefix);
			StringUtils.appendPadded(sb, String.valueOf(start), '0', digits, Justification.RIGHT);
			if(suffix != null)
				sb.append(suffix);
			start = ((start + last - first - 1) % (last - first + 1)) + first;
		}
		return sb.toString();
	}
    /** Joins the elements of the specified array together in a String separated by the given delimiter.
     *
     * @param values
     * @param delimiter
     * @return
     */
    public static String join(String[] values, String delimiter) {
        return join(values,delimiter,0,values.length);
    }

    /** Joins the elements of the specified array together in a String separated by the given delimiter.
    *
    * @param values
    * @param delimiter
    * @param wrapper
    * @return
    */
   public static String join(String[] values, String delimiter, String wrapper) {
       return join(values,delimiter,0,values.length,wrapper);
   }
    
    /** Joins the elements of the specified collection together in a String separated by the given delimiter.
     * @param values
     * @param delimiter
     * @param offset
     * @param length
     * @param wrapper
     * @return
     */
    public static String join(String[] values, String delimiter, int offset, int length, String wrapper) {
		return join(values, delimiter, offset, length, wrapper, new StringBuilder()).toString();
	}

	public static StringBuilder join(String[] values, String delimiter, int offset, int length, String wrapper, StringBuilder toAppendTo) {
		try {
			return (StringBuilder) join(values, delimiter, offset, length, wrapper, (Appendable) toAppendTo);
		} catch(IOException e) {
			return toAppendTo;
		}
	}

	public static Appendable join(String[] values, String delimiter, int offset, int length, String wrapper, Appendable toAppendTo) throws IOException {
        for(int i = offset; i < offset+length; i++) {
			if(i != offset)
				toAppendTo.append(delimiter);
			if(values[i] != null) {
				if (wrapper != null)
					toAppendTo.append(wrapper);
				toAppendTo.append(values[i]);
				if (wrapper != null)
					toAppendTo.append(wrapper);
        }
        }
		return toAppendTo;
    }

	public static StringBuilder join(String[] values, String delimiter, StringBuilder toAppendTo) {
		return join(values, delimiter, 0, values.length, null, toAppendTo);
	}

	public static String join(String[] values, char delimiter, char quote) {
		return join(values, delimiter, quote, new StringBuilder()).toString();
	}

	public static StringBuilder join(String[] values, char delimiter, char quote, StringBuilder toAppendTo) {
		return join(values, delimiter, quote, 0, values.length, toAppendTo);
	}

	public static StringBuilder join(String[] values, char delimiter, char quote, int offset, int length, StringBuilder toAppendTo) {
		for(int i = offset; i < offset + length; i++) {
			if(i != offset)
				toAppendTo.append(delimiter);
			if(values[i] != null) {
				int pos = values[i].indexOf(quote);
				if(pos >= 0) {
					toAppendTo.append(quote).append(values[i], 0, pos + 1).append(quote);
					while(pos < values[i].length()) {
						int prev = pos + 1;
						pos = values[i].indexOf(quote, prev);
						if(pos < 0) {
							toAppendTo.append(values[i], prev, values[i].length());
							break;
						}
						toAppendTo.append(values[i], prev, pos + 1).append(quote);
					}
					toAppendTo.append(quote);
				} else if(values[i].indexOf(delimiter) >= 0) {
					toAppendTo.append(quote).append(values[i]).append(quote);
				} else
					toAppendTo.append(values[i]);
			}
		}
		return toAppendTo;
	}

    /** Joins the elements of the specified collection together in a String separated by the given delimiter.
     * @param values
     * @param delimiter
     * @param offset
     * @param length
     * @return
     */
    public static String join(String[] values, String delimiter, int offset, int length) {
        return join(values, delimiter, offset, length, null);
    }

    /** Joins the elements of the specified collection together in a String separated by the given delimiter.
     *
     * @param values
     * @param delimiter
     * @return
     */
    public static String join(Collection<?> values, String delimiter) {
        return join(values,delimiter,0,values.size());
    }

    /** Joins the elements of the specified collection together in a String separated by the given delimiter.
     * @param values
     * @param delimiter
     * @param offset
     * @param length
     * @return
     */
    public static String join(Collection<?> values, String delimiter, int offset, int length) {
        return join(values, delimiter, offset, length, new StringBuilder()).toString();
    }


    /** Joins the elements of the specified collection together in a String separated by the given delimiter.
    *
    * @param values
    * @param delimiter
    * @return
    */
   public static Appendable join(Collection<?> values, String delimiter, Appendable toAppendTo) {
       return join(values,delimiter,0,values.size(), toAppendTo);
   }

   /** Joins the elements of the specified collection together in a String separated by the given delimiter.
     * @param values
     * @param delimiter
     * @param offset
     * @param length
     * @return
     */
    public static Appendable join(Collection<?> values, String delimiter, int offset, int length, Appendable toAppendTo) {
        Iterator<?> iter;
        if(values instanceof List<?>) iter = ((List<?>)values).listIterator(offset);
        else {
            iter = values.iterator();
            for(int i = 0; iter.hasNext() && i < offset; i++) iter.next();
        }
        for(int i = offset; iter.hasNext() && i < offset+length; i++) {
        	try {
				if(i != offset) toAppendTo.append(delimiter);
	            toAppendTo.append(String.valueOf(iter.next()));
        	} catch(IOException e) {
				//do nothing
			}
        }
        return toAppendTo;
    }

	public static String join(Collection<?> values, String delimiter, String quote, String quoteReplacement) {
		return join(values, delimiter, quote, quoteReplacement, 0, values.size(), new StringBuilder()).toString();
	}

	/**
	 * Joins the elements of the specified collection together in a String separated by the given delimiter.
	 * 
	 * @param values
	 * @param delimiter
	 * @param offset
	 * @param length
	 * @return
	 */
	public static Appendable join(Collection<?> values, String delimiter, String quote, String quoteReplacement, int offset, int length, Appendable toAppendTo) {
		if(!isBlank(quote) && isBlank(quoteReplacement))
			quoteReplacement = quote + quote;
		Iterator<?> iter;
		if(values instanceof List<?>)
			iter = ((List<?>) values).listIterator(offset);
		else {
			iter = values.iterator();
			for(int i = 0; iter.hasNext() && i < offset; i++)
				iter.next();
		}
		for(int i = offset; iter.hasNext() && i < offset + length; i++) {
			try {
				if(i != offset)
					toAppendTo.append(delimiter);
				String value = String.valueOf(iter.next());
				if(value == null)
					continue;
				if(!isBlank(quote) && value.contains(delimiter))
					toAppendTo.append(quote).append(value.replace(quote, quoteReplacement)).append(quote);
				else
					toAppendTo.append(value);
			} catch(IOException e) {
				// do nothing
			}
		}
		return toAppendTo;
	}
    /**
     * @param s
     * @param find
     * @param occurance
     * @return
     */
    public static int nthIndexOf(String s, char find, int occurance) {
        int p;
        for(p = -1; occurance > 0; occurance--) {
            p = s.indexOf(find,p+1);
            if(p < 0) break;
        }
        return p;
    }

	public static String toStringProperties(Object bean) throws IntrospectionException {
		return toStringProperties(bean, null);
	}

	public static String toStringProperties(Object bean, Set<String> maskedProperties) throws IntrospectionException {
		if(bean == null)
			return "";
		if(maskedProperties == null)
			maskedProperties = Collections.emptySet();
		StringBuilder sb = new StringBuilder();
		sb.append(bean.getClass().getSimpleName()).append(' ');
		for(Map.Entry<String, ?> entry : ReflectionUtils.toPropertyMap(bean).entrySet()) {
			sb.append(entry.getKey()).append(": ");
			if(entry.getValue() != null && maskedProperties.contains(entry.getKey())) {
				String svalue = ConvertUtils.getStringSafely(entry.getValue());
				if(svalue != null)
					for(int i = 0; i < svalue.length(); i++)
						sb.append('*');
			} else
				sb.append(entry.getValue());
			sb.append(", ");
		}

		return sb.substring(0, sb.length() - 2);
	}
    /**
     * @param o
     * @return
     */
    public static String toStringList(Object o) {
        if(o == null) return null;
        if(!o.getClass().isArray()) return o.toString();
        StringBuilder sb = new StringBuilder("[");
        int len = java.lang.reflect.Array.getLength(o);
        for(int i = 0; i < len; i++) {
            if(i != 0) sb.append(", ");
            Object ele = java.lang.reflect.Array.get(o,i);
            if(ele != null)	sb.append(toStringList(ele));
        }
        sb.append(']');
        return sb.toString();
    }

    /**
     * @param s
     * @return
     */
    public static String capitalizeFirst(String s) {
        return s.length() == 0 ? s : s.substring(0,1).toUpperCase() + s.substring(1);
    }

	public static String capitalizeAllWords(String s) {
		StringBuilder sb = new StringBuilder(s);
		boolean cap = true;
		for(int i = 0; i < sb.length(); i++) {
			char ch = sb.charAt(i);
			if(Character.isWhitespace(ch))
				cap = true;
			else if(cap) {
				sb.setCharAt(i, Character.toTitleCase(ch));
				cap = false;
			}
		}
		return sb.toString();
	}

    protected static StringMatcher pluralMatcher = new StringMatcher(StringMatcher.MATCH_ENDING, false);
    static {
        pluralMatcher.addMatch("ies", "y");
        pluralMatcher.addMatch("ches", "ch");
        pluralMatcher.addMatch("sses", "ss");
        pluralMatcher.addMatch("xes", "x");
        pluralMatcher.addMatch("s", "");
        pluralMatcher.addMatch("um", "a");
        }
    /**
     * @param s
     * @return
     */
    public static String singular(String s) {
        return pluralMatcher.matchAndReplace(s);
    }
    /**
     * Returns the package of the class as a file path using the system's
     * separator character
     */
    public static String packageAsPath(Class<?> clazz) {
        String path = clazz.getName();
        path = path.substring(0, path.lastIndexOf('.')+1).replace('.', java.io.File.separatorChar);
        return path;
    }

    /**
     * Converts the given exception to a string that is its stack trace.
     */
    public static String exceptionToString(Throwable exception) {
        if(exception == null) return "";
        try {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw, true);
            exception.printStackTrace(pw);
            return sw.toString();
        } catch (Exception e) {
            return "";
        }
    }
    public static StringBuilder appendPadded(StringBuilder appendTo, String s, char padChar, int length, Justification justification) {
    	try {
			appendPadded((Appendable)appendTo, s, padChar, length, justification);
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e);
		}
    	return appendTo;
    }

	public static StringBuffer appendPadded(StringBuffer appendTo, String s, char padChar, int length, Justification justification) {
		try {
			appendPadded((Appendable) appendTo, s, padChar, length, justification);
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e);
		}
		return appendTo;
	}
    public static Appendable appendPadded(Appendable appendTo, String s, char padChar, int length, Justification justification) throws IOException {
    	if(s == null) {
    		for(int i = 0; i < length; i++)
    			appendTo.append(padChar);
    	} else if(s.length() >= length) {
    		int start;
    		switch(justification) {
    			case RIGHT:
    				start = s.length() - length; break;
    			case CENTER:
    				start = (s.length() - length)/2; break;
    			case LEFT: default:
    				start = 0; break;
    		}
    		appendTo.append(s, start, start + length);
    	} else {
	    	switch(justification) {
	            case LEFT:
	            	for(int i = 0; i < s.length(); i++)
	            		appendTo.append(s.charAt(i));
	            	for(int i = s.length(); i < length; i++)
	        			appendTo.append(padChar);
	            	break;
	            case CENTER:
	            	int start = (length - s.length()) / 2;
	            	for(int i = 0; i < start; i++)
	        			appendTo.append(padChar);
	            	for(int i = 0; i < s.length(); i++)
	            		appendTo.append(s.charAt(i));
	            	for(int i = start + s.length(); i < length; i++)
	        			appendTo.append(padChar);
	            	break;
	            case RIGHT:
	            	for(int i = 0; i < length-s.length(); i++)
	        			appendTo.append(padChar);
	            	for(int i = 0; i < s.length(); i++)
	            		appendTo.append(s.charAt(i));
	            	break;
	        }
	    }
    	return appendTo;
    }
    /**
     * Pads the given string with the provided characters so that the resultant string has a length equal to the specified length
     * @param s The original string
     * @param padChar The characterto use as padding if the length of the original string is less than the desired length
     * @param length The desired length of the resultant string
     * @param justification How to justify the string. (LEFT, RIGHT, or CENTER)
     * @return The resultant string
     */
    public static String pad(String s, char padChar, int length, Justification justification) {
    	if(s == null) {
    		char[] chars = new char[length];
    		Arrays.fill(chars, 0, length, padChar);
            return new String(chars);
    	}
    	if(s.length() >= length) {
    		int start;
    		switch(justification) {
    			case RIGHT:
    				start = s.length() - length; break;
    			case CENTER:
    				start = (s.length() - length)/2; break;
    			case LEFT: default:
    				start = 0; break;
    		}
    		return s.substring(start, start + length);
    	}
    	char[] chars = new char[length];
    	switch(justification) {
            case LEFT:
            	s.getChars(0, s.length(), chars, 0);
            	Arrays.fill(chars, s.length(), length, padChar);
                break;
            case CENTER:
            	int start = (length - s.length()) / 2;
            	s.getChars(0, s.length(), chars, start);
            	Arrays.fill(chars, 0, start, padChar);
            	Arrays.fill(chars, start + s.length(), length, padChar);
                break;
            case RIGHT:
            	s.getChars(0, s.length(), chars, length-s.length());
            	Arrays.fill(chars, 0, length-s.length(), padChar);
                break;
        }
    	return new String(chars);
    }

    /**
     * Pads the given string with the provided characters so that the resultant string has a length equal to the specified length
     * @param s The original string
     * @param padChars The character(s) to use as padding if the length of the original string is less than the desired length
     * @param length The desired length of the resultant string
     * @param justification How to justify the string. (JUSTIFY_LEFT, JUSTIFY_RIGHT, or JUSTIFY_CENTER)
     * @return The resultant string
     */
    public static String pad(String s, String padChars, int length, int justification) {
    	if(s.length() >= length) {
    		int start;
    		switch(justification) {
    			case JUSTIFY_RIGHT:
    				start = s.length() - length; break;
    			case JUSTIFY_CENTER:
    				start = (s.length() - length)/2; break;
    			case JUSTIFY_LEFT: default:
    				start = 0; break;
    		}
    		return s.substring(start, start + length);
    	}
        switch(justification) {
            case JUSTIFY_LEFT:
                return s + fillString(length - s.length(), padChars);
            case JUSTIFY_CENTER:
                return fillString((length - s.length()) / 2, padChars) + s + fillString((length - s.length() + 1) / 2, padChars);
            case JUSTIFY_RIGHT:
                return fillString(length - s.length(), padChars) + s;
            default:
                throw new IllegalArgumentException("Justication " + justification + " is not valid");
        }
    }

    public static String unpad(String original, Justification justification, char padding) {
		int i;
		switch(justification) {
			case RIGHT:
				i = 0;
				while(i < original.length() && original.charAt(i) == padding) 
					i++;
				return original.substring(i, original.length());
			case LEFT:
				i = original.length();
				while(i > 0 && original.charAt(i - 1) == padding)
					i--;
				return original.substring(0,i);
			case CENTER:
				i = 0;
				while(i < original.length() && original.charAt(i) == padding) 
					i++;
				int k = original.length();
				while(k > i && original.charAt(k - 1) == padding)
					k--;
				return original.substring(i,k);
			default:
				return original;
		}		
	}
    
	public static void writeEncodedNVP(Writer writer, String prefix, String name, String value) throws IOException {
		if(!StringUtils.isBlank(prefix)) {
			writer.write(URLEncoder.encode(prefix, XML_CHARSET.name()));
			writer.write(':');
		}
		if(!StringUtils.isBlank(name))
			writer.write(URLEncoder.encode(name, XML_CHARSET.name()));
		writer.write('=');
		if(!StringUtils.isBlank(value))
			writer.write(URLEncoder.encode(value, XML_CHARSET.name()));
		writer.write('&');
	}

	public static void writeEncodedMap(Writer writer, String prefix, Map<?, ?> map) throws IOException {
		for(Map.Entry<?, ?> entry : map.entrySet())
			writeEncodedNVP(writer, prefix, ConvertUtils.getStringSafely(entry.getKey()), ConvertUtils.getStringSafely(entry.getValue()));
	}

	public static interface EncodedHandler<I extends Exception> {
		public void handle(String prefix, String name, String value) throws I;
	}

	public static <I extends Exception> void readEncoded(Readable reader, EncodedHandler<I> handler) throws I, IOException {
		readEncoded(reader, handler, 512);
	}

	public static <I extends Exception> void readEncoded(Readable reader, EncodedHandler<I> handler, int bufferSize) throws I, IOException {
		CharBuffer buffer = CharBuffer.allocate(bufferSize);
		String name = null;
		String prefix = null;
		int start = 0;
		while(reader.read(buffer) >= 0) {
			buffer.flip();
			while(buffer.hasRemaining()) {
				if(name != null) {
					if(buffer.get() == '&') {
						String value = extractDecodedString(buffer, start, false);
						handler.handle(prefix, name, value);
						name = null;
						prefix = null;
						start = buffer.position(); // should be 0
					}
				} else {
					switch(buffer.get()) {
						case '=':
							name = extractDecodedString(buffer, start, false);
							start = buffer.position(); // should be 0
							break;
						case ':':
							prefix = extractDecodedString(buffer, start, false);
							start = buffer.position(); // should be 0
							break;
					}
				}
			}
			if(buffer.limit() > buffer.capacity() - 32) {
				// we need a bigger buffer
				CharBuffer newBuffer = CharBuffer.allocate(buffer.capacity() + bufferSize);
				buffer.position(start);
				newBuffer.put(buffer);
				buffer = newBuffer;
			} else {
				buffer.limit(buffer.capacity());
			}
		}
		if(name != null) {
			String value = extractDecodedString(buffer, start, true);
			handler.handle(prefix, name, value);
		} else if(prefix != null || start < buffer.position() - 1) {
			name = extractDecodedString(buffer, start, true);
			handler.handle(prefix, name, null);
		}
	}

	protected static String extractDecodedString(CharBuffer buffer, int start, boolean atEnd) throws IOException {
		int end = buffer.position();
		if(!atEnd)
			end--;
		int origLimit = buffer.limit();
		buffer.position(start);
		buffer.limit(end);
		String s;
		try {
			s = URLDecoder.decode(buffer.toString(), XML_CHARSET.name());
		} catch(IllegalArgumentException e) {
			throw new IOException(e);
		}
		buffer.limit(origLimit);
		buffer.position(end + 1);
		buffer.compact();
		buffer.flip();
		return s;
	}

    /**
     * Creates and returns a string that has a length equal to the specified number parameter and is a repetition of the specified character(s)
     * @param number The desired length of the returned string
     * @param padChars The character(s) to repeat
     * @return The string of repeated characters
     */
    public static String fillString(int number, String padChars) {
        StringBuilder sb = new StringBuilder(number);
        for(int i = 0; i < number; i += padChars.length()) sb.append(padChars);
        return sb.substring(0, number);
    }

    /**
     * Creates and returns a string that is a repetition of the specified character(s) the specified number of times
     * @param number The number of times to repeat the pattern
     * @param pattern The character(s) to repeat
     * @return The string of repeated characters
     */
    public static String repeat(String pattern, int number) {
        StringBuilder sb = new StringBuilder(number * pattern.length());
        for(int i = 0; i < number; i++) sb.append(pattern);
        return sb.toString();
    }
    /** Converts the string to a standard SQL identifier (all upper case,
     * replace non-alphanumerics with underscore)
     * @param s The string to convert
     * @return
     */
    protected static final Pattern removeNonAlphaNumPattern = Pattern.compile("\\W+");
    public static String toSqlName(String s) {
        return removeNonAlphaNumPattern.matcher(s).replaceAll("_").toUpperCase();
    }
    /** Converts the string to a java variable style name (lower case first character,
     * remove non-alphanumerics, upper case first letter in each subsequent word
     * @param s The string to convert
     * @return
     */
    public static String toJavaName(String s) {
        return toJavaName(s, EMPTY_CHAR_ARRAY);
    }

    /** Converts the string to a java nested-name (retain ".", "(", ")", "[", and "]", lower case first character,
     * remove non-alphanumerics, upper case first letter in each subsequent word
     * @param s The string to convert
     * @return
     */
    public static String toJavaNestedName(String s) {
        return toJavaName(s, NESTED_NAME_CHARS);
    }

    protected static final char[] EMPTY_CHAR_ARRAY = new char[0];
    protected static final char[] NESTED_NAME_CHARS = new char[] {
        NestedNameTokenizer.MAPPED_DELIM,
        NestedNameTokenizer.MAPPED_DELIM2,
        NestedNameTokenizer.NESTED_DELIM,
        NestedNameTokenizer.INDEXED_DELIM,
        NestedNameTokenizer.INDEXED_DELIM2
    };
    static { Arrays.sort(NESTED_NAME_CHARS); }

    protected static String toJavaName(String s, char[] retainSet) {
        StringBuilder sb = new StringBuilder(s.length());
        boolean newWord = false;
        for(int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if(Character.isJavaIdentifierStart(ch) || (i > 0 && Character.isJavaIdentifierPart(ch))) {
                if(newWord) {
                    sb.append(Character.toUpperCase(ch));
                    newWord = false;
                } else if(i == 0)
                	sb.append(Character.toLowerCase(ch));
                else
                    sb.append(ch);
            } else if(Arrays.binarySearch(retainSet, ch) >= 0) {
                sb.append(ch);
            } else {
                newWord = true;
            }
        }
        return sb.toString();
    }

    public static int count(String search, char find, char quote) {
        int count = 0;
        for(int i = 0; i < search.length(); i++) {
            char c = search.charAt(i);
            if(c == quote) {
                i = search.indexOf(c, i+1);
                if(i < 0) break; //untermined quote
            } else if(c == find)
                count++;
        }
        return count;
    }

    public static String getHostOfUrl(String url) {
        if(url != null) {
            String[] parts = RegexUtils.match("^\\w+[:][/]*([^/?]+).*", url, null);
            if(parts != null && parts.length > 0) return parts[1];
        }
        return null;
    }

    public static String gather(String original, SortedChars retain, SortedChars ignore) throws ParseException {
    	StringBuilder sb = new StringBuilder(original.length());
    	for(int i = 0; i < original.length(); i++) {
    		char c = original.charAt(i);
    		if(retain.contains(c))
    			sb.append(c);
    		else if(!ignore.contains(c))
    			throw new ParseException("Invalid character '" + c + "' at position " + (i+1),i+1);

    	}
    	return sb.toString();
    }

    public static String gather(String original, Pattern pattern) throws ParseException {
    	StringBuilder sb = new StringBuilder(original.length());
    	Matcher matcher = pattern.matcher(original);
    	while(!matcher.hitEnd()) {
    		if(!matcher.lookingAt())
    			throw new ParseException("Invalid character '" + original.charAt(matcher.regionStart()) + "' at position " + (matcher.regionStart()+1),matcher.regionStart()+1);
    		sb.append(matcher.group(1));
    		matcher.region(matcher.end(), matcher.regionEnd());
    	}
    	return sb.toString();
	}

    protected static final char[] HEX_DIGITS = {
    	'0' , '1' , '2' , '3' , '4' , '5' ,
    	'6' , '7' , '8' , '9' , 'A' , 'B' ,
    	'C' , 'D' , 'E' , 'F'};

    public static String toHex(byte[] bytes, int offset, int length) {
    	if(bytes == null) return null;
    	char[] result = new char[length*2];
    	for(int i = 0; i < length; i++) {
    		result[i*2] = HEX_DIGITS[(bytes[offset + i] >> 4) & 0x0f];
    		result[i*2+1] = HEX_DIGITS[bytes[offset + i] & 0x0f];
    	}
    	return new String(result);
    }

    public static String toHex(ByteBuffer buffer, int position, int length) {
    	if(buffer == null) return null;
    	char[] result = new char[length*2];
    	for(int i = 0; i < length; i++) {
    		result[i*2] = HEX_DIGITS[(buffer.get(position + i) >> 4) & 0x0f];
    		result[i*2+1] = HEX_DIGITS[buffer.get(position + i) & 0x0f];
    	}
    	return new String(result);
    }
    
    public static String toHex(byte[] bytes) {
    	if(bytes == null) return null;
    	return toHex(bytes, 0, bytes.length);
    }

    public static String toHex(String string) {
    	if(string == null) return null;
    	byte[] bytes = ByteArrayConverter.toBytes(string);
    	return toHex(bytes, 0, bytes.length);
    }

    public static String toHex(long l) {
    	byte[] bytes = new byte[8];
    	bytes[0] = (byte)(l >>> 56);
        bytes[1] = (byte)(l >>> 48);
        bytes[2] = (byte)(l >>> 40);
        bytes[3] = (byte)(l >>> 32);
        bytes[4] = (byte)(l >>> 24);
        bytes[5] = (byte)(l >>> 16);
        bytes[6] = (byte)(l >>>  8);
        bytes[7] = (byte)(l >>>  0);
        return toHex(bytes, 0, bytes.length);
	}

    public static String toHex(int i) {
    	byte[] bytes = new byte[4];
    	bytes[0] = (byte)(i >>> 24);
        bytes[1] = (byte)(i >>> 16);
        bytes[2] = (byte)(i >>>  8);
        bytes[3] = (byte)(i >>>  0);
        return toHex(bytes, 0, bytes.length);
	}

    public static String toHex(short s) {
    	byte[] bytes = new byte[2];
    	bytes[0] = (byte)(s >>>  8);
        bytes[1] = (byte)(s >>>  0);
        return toHex(bytes, 0, bytes.length);
	}
    public static String toHex(byte b) {
        char[] result = new char[2];
        result[0] = HEX_DIGITS[(b >> 4) & 0x0f];
        result[1] = HEX_DIGITS[b & 0x0f];
        return new String(result);
    }
    public static char hexDigit1(byte b) {
    	return HEX_DIGITS[(b >> 4) & 0x0f];
    }

    public static char hexDigit2(byte b) {
    	return HEX_DIGITS[b & 0x0f];
    }

    public static StringBuilder appendHex(StringBuilder appendTo, long l) {
    	return appendTo
	    	.append(StringUtils.hexDigit1((byte)(l >>> 56))).append(StringUtils.hexDigit2((byte)(l >>> 56)))
			.append(StringUtils.hexDigit1((byte)(l >>> 48))).append(StringUtils.hexDigit2((byte)(l >>> 48)))
			.append(StringUtils.hexDigit1((byte)(l >>> 40))).append(StringUtils.hexDigit2((byte)(l >>> 40)))
			.append(StringUtils.hexDigit1((byte)(l >>> 32))).append(StringUtils.hexDigit2((byte)(l >>> 32)))
			.append(StringUtils.hexDigit1((byte)(l >>> 24))).append(StringUtils.hexDigit2((byte)(l >>> 24)))
    		.append(StringUtils.hexDigit1((byte)(l >>> 16))).append(StringUtils.hexDigit2((byte)(l >>> 16)))
    		.append(StringUtils.hexDigit1((byte)(l >>>  8))).append(StringUtils.hexDigit2((byte)(l >>>  8)))
    		.append(StringUtils.hexDigit1((byte)(l >>>  0))).append(StringUtils.hexDigit2((byte)(l >>>  0)));
	}

    public static StringBuilder appendHex(StringBuilder appendTo, int i) {
    	return appendTo
    	    .append(StringUtils.hexDigit1((byte)(i >>> 24))).append(StringUtils.hexDigit2((byte)(i >>> 24)))
    		.append(StringUtils.hexDigit1((byte)(i >>> 16))).append(StringUtils.hexDigit2((byte)(i >>> 16)))
    		.append(StringUtils.hexDigit1((byte)(i >>>  8))).append(StringUtils.hexDigit2((byte)(i >>>  8)))
    		.append(StringUtils.hexDigit1((byte)(i >>>  0))).append(StringUtils.hexDigit2((byte)(i >>>  0)));
	}

    public static StringBuilder appendHex(StringBuilder appendTo, short s) {
    	return appendTo
    	    .append(StringUtils.hexDigit1((byte)(s >>>  8))).append(StringUtils.hexDigit2((byte)(s >>>  8)))
    		.append(StringUtils.hexDigit1((byte)(s >>>  0))).append(StringUtils.hexDigit2((byte)(s >>>  0)));
	}
    public static StringBuilder appendHex(StringBuilder appendTo, byte b) {
    	return appendTo
    	    .append(StringUtils.hexDigit1(b)).append(StringUtils.hexDigit2(b));
	}

	public static StringBuilder appendHex(StringBuilder appendTo, byte[] bytes) {
		if(bytes == null)
			return appendTo;
		return appendHex(appendTo, bytes, 0, bytes.length);
	}

    public static StringBuilder appendHex(StringBuilder appendTo, byte[] bytes, int offset, int length) {
    	if(bytes == null) return appendTo;
    	for(int i = 0; i < length; i++) {
    		appendTo.append(hexDigit1(bytes[offset + i]));
    		appendTo.append(hexDigit2(bytes[offset + i]));
    	}
    	return appendTo;
    }

    public static boolean startsWith(CharSequence search, CharSequence prefix) {
    	if(search.length() < prefix.length())
    		return false;
    	for(int i = 0; i < prefix.length(); i++)
    		if(search.charAt(i) != prefix.charAt(i))
    			return false;
    	return true;
    }

    /** Writes the specified Object array to the given PrintStream in fixed width format.
     * @param data The array of Objects that is the data for this line
     * @param widths The number of characters in each column
     * @param justifications The justification of each column
     * @param out The PrintWriter to which to write
     * @throws IOException If the OutputStream can not be written to
     */
    public static void writeFixedWidthLine(Object[] data, int[] widths, Justification[] justifications, PrintWriter out) {
        writeFixedWidthLine(data, widths, justifications, 2, out);
    }

    public static void writeFixedWidthLine(Object[] data, int[] widths, Justification[] justifications, int spacing, PrintWriter out) {
        for(int i = 0; i < data.length; i++) {
        	if(widths[i] > 0)
        		out.print(pad(ConvertUtils.getStringSafely(data[i]), SPACE, widths[i], justifications == null || justifications[i] == null ? Justification.LEFT : justifications[i]));
        	out.print(pad(null, SPACE, spacing, Justification.LEFT));
        }
        out.println();
    }
    /**
     * Writes a Results Object to an OutputStream in fixed width format
     * @param header Whether to include the column names as a header or not
     * @param results The Results
     * @param pw The PrintWriter
     * @throws IOException If the OutputStream can not be written to
     * @throws SQLException If the ResultSet throws an exception
     */
    public static void writeFixedWidth(final Results results, int[] widths, Justification[] justifications, PrintWriter pw, boolean header, boolean headerLine, boolean summary) {
    	final int num = widths.length;
    	final int r = Math.min(num, results.getColumnCount());
		if(justifications == null) {
    		justifications = new Justification[num];
    	} else if(justifications.length != num) {
    		Justification[] tmp = new Justification[num];
    		System.arraycopy(justifications, 0, tmp, 0, justifications.length);
    		justifications = tmp;
    	}
		for(int i = 0; i < num; i++) {
			if(justifications[i] == null) {
				if(r < i)
					justifications[i] = Justification.LEFT;
				else {
					Class<?> clazz = results.getColumnClass(i+1);
					if(Number.class.isAssignableFrom(clazz))
						justifications[i] = Justification.RIGHT;
					else if(Date.class.isAssignableFrom(clazz))
						justifications[i] = Justification.RIGHT;
					else
						justifications[i] = Justification.LEFT;
				}
			}
		}
    	if(header) {
            String[] names = new String[num];
            for(int i = 0; i < r; i++) {
                names[i] = results.getColumnName(i+1);
            }
            for(int i = r; i < num; i++) {
                names[i] = "<NOT PROVIDED>";
            }
            writeFixedWidthLine(names, widths, justifications, pw);
        }
    	if(headerLine) {
    		String[] lines = new String[num];
            for(int i = 0; i < num; i++) {
            	lines[i] = StringUtils.pad(null, '-', widths[i], Justification.LEFT);
            }
            writeFixedWidthLine(lines, widths, null, pw);
    	}
    	final AtomicInteger cnt = new AtomicInteger();
    	writeFixedWidth(new java.util.Iterator<Object[]>() {
            private boolean next = results.next();
            public boolean hasNext() { return next; }
            public Object[] next() {
                Object[] values = new Object[num];
                for(int i = 0; i < r; i++)
                	values[i] = results.getValue(i+1);
                next = results.next();
                cnt.incrementAndGet();
                return values;
            }
            public void remove() {}
        }, widths, justifications, pw);
    	if(summary) {
    		for(int i = 0; i < num; i++) {
            	for(int k = 0; k < widths[i]; k++)
            		pw.print('-');
    		}
    		for(int k = 0; k < (widths.length - 1) * 2; k++)
        		pw.print('-');
    		pw.println();
    		pw.print(cnt.intValue());
    		pw.println(" row(s) found.");
    	}
        pw.flush();
    }
    /** Writes an Iterator of Object arrays to a PrintStream in fixed width format
     * @param iter The iterator containing the Object arrays
     * @param out The PrintWriter to which to write
     * @throws IOException If the OutputStream can not be written to
     */
    public static void writeFixedWidth(java.util.Iterator<Object[]> iter, int[] widths, Justification[] justifications, PrintWriter out) {
        while(iter.hasNext()) writeFixedWidthLine(iter.next(), widths, justifications, out);
        out.flush();
    }
    /** Returns the given String with any offending characters escaped based on
     * the specified preparation.
     * @return The formatted String
     * @param text The String to "prepare"
     * @param preparation The type of "preparation" required.
     */
    public static String prepare(String text, int preparation) {
        switch(preparation) {
            case PREP_HTML:
                return prepareHTML(text);
            case PREP_SCRIPT:
                return prepareScript(text);
            case PREP_RTF:
                return prepareRTF(text);
            case PREP_URL_PART:
                return prepareURLPart(text);
            case PREP_HTML_BLANK:
                return prepareHTMLBlank(text);
            case PREP_CDATA:
                return prepareCDATA(text);
            default:
                return text;
        }
    }

    /** Returns the given text String with any offensive characters properly escaped.
     * @return The formatted String
     * @param text The String to "prepare"
     * @param prepare The type(s) of "preparation" required separated by commas, slashes, or ampersands.
     */
    public static String prepareString(String text, String prepare) {
        StringTokenizer token = new StringTokenizer(prepare, " ,+/\\&");
        while(token.hasMoreTokens()) {
            String p = token.nextToken();
            if(p.equalsIgnoreCase("HTML")) {
                text = prepareHTML(text);
            } else if(p.equalsIgnoreCase("SCRIPT")) {
                text = prepareScript(text);
            } else if(p.equalsIgnoreCase("RTF")) {
                text = prepareRTF(text);
            } else if(p.equalsIgnoreCase("LABEL")) {
                text = prepareSectionLabel(text);
            } else if(p.equalsIgnoreCase("URL")) {
                text = prepareURLPart(text);
            } else if(p.equalsIgnoreCase("HTML-BLANK")) {
                text = prepareHTMLBlank(text);
            } else if(p.equalsIgnoreCase("CDATA")) {
                text = prepareCDATA(text);
             } else {
                //logDebug("Couldn't find prepare of '" + p + "'");
            }
        }
        return text;
    }

    /** Returns the given String with any characters invalid in SQL escaped
     * @return The formatted String
     * @param text The String to "prepare"
     */
    public static String prepareSQL(String text) {
        //weed out all bad characters
		StringBuilder result = new StringBuilder(text.length());
        for(int i = 0; i < text.length(); i++) {
            String replace = getSQLEquiv(text.charAt(i));
            if(replace == null) result.append(text.charAt(i));
            else result.append(replace);
        }
        return result.toString();
    }

    /** Returns the given Object as a String with any characters invalid in an HTML
     * id attribute changed to an underscore
     * @return The formatted String
     * @param o The object the represents the section label
     */
    public static String prepareSectionLabel(Object o) {
        //make a 'unique' div label for an html page based  on the object
        if(o == null) return "";
        if(o instanceof java.util.Date) return "" + ((java.util.Date)o).getTime();
        else {
            String s = o.toString();
			StringBuilder sb = new StringBuilder(s.length());
            for(int i = 0; i < s.length(); i++) {
                char ch = s.charAt(i);
                if(Character.isLetterOrDigit(ch)) sb.append(ch);
                else sb.append('_');
            }
            return sb.toString();
        }
    }

    /** Returns the given String with any characters invalid in rich-text format
     * escaped
     * @return The formatted String
     * @param text The String to "prepare"
     */
    public static String prepareRTF(String text) {
        if(text == null || text.length() == 0) return "";
        //weed out all bad characters
		StringBuilder result = new StringBuilder(text.length());
        for(int i = 0; i < text.length(); i++) {
            String replace = getRTFEquiv(text.charAt(i));
            if(replace == null) result.append(text.charAt(i));
            else result.append(replace);
        }
        return result.toString();
    }

    /** Returns the given String with any characters invalid in javascript escaped
     * @return The formatted String
     * @param text The String to "prepare"
     */
    public static String prepareScript(String text) {
        if(text == null || text.length() == 0) return "";
        //weed out all bad characters
		StringBuilder result = new StringBuilder(text.length());
        for(int i = 0; i < text.length(); i++) {
            String replace = getScriptEquiv(text.charAt(i));
            if(replace == null) result.append(text.charAt(i));
            else result.append(replace);
        }
        return result.toString();
    }

	public static Appendable appendScript(Appendable appendTo, String text) throws IOException {
		if(text == null || text.length() == 0)
			return appendTo;
		// weed out all bad characters
		for(int i = 0; i < text.length(); i++) {
			String replace = getScriptEquiv(text.charAt(i));
			if(replace == null)
				appendTo.append(text.charAt(i));
			else
				appendTo.append(replace);
		}
		return appendTo;
	}

	public static boolean isDisplayable(char ch) {
		return ch >= ' ' && ch <= '~';
	}
	public static String preparePrintable(String text) {
		if(text == null || text.length() == 0)
			return "";
		// weed out all unprintable characters
		StringBuilder result = new StringBuilder(text.length());
		for(int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);
			if(isDisplayable(ch))
				result.append(ch);
			else {
				result.append("<0x");
				String s = Integer.toHexString(ch);
				if((s.length() % 2) == 1)
					result.append('0');
				result.append(s).append('>');
			}
		}
		return result.toString();
	}
	
	public static String prepareJavascriptPrintable(String text) {
		if(text == null || text.length() == 0)
			return "";
		// weed out all unprintable characters
		StringBuilder result = new StringBuilder(text.length());
		for(int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);
			if(isDisplayable(ch))
				result.append(ch);
			else {
				result.append("\\x");
				String s = Integer.toHexString(ch);
				result.append(s);
			}
		}
		return result.toString();
	}

	public static String prepareDisplayable(String text, boolean normalizeSpace) {
		if(text == null || text.length() == 0)
			return "";
		// change unprintable chars to space
		boolean trim = normalizeSpace;
		StringBuilder result = new StringBuilder(text.length());
		for(int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);
			if(ch > ' ' && ch <= '~') {
				trim = false;
				result.append(ch);
			} else if(!trim) {
				result.append(' ');
				trim = normalizeSpace;
			}
		}
		return result.toString();
	}

    /** Returns the given String with any characters invalid in a URL escaped
     * @return The formatted String
     * @param text The String to "prepare"
     */
    public static String prepareURLPart(String text) {
        if(text == null) return "";
        try {
            return URLEncoder.encode(text, "UTF-8");
        } catch(UnsupportedEncodingException e) {
            return text;
        }
    }

    /** Returns the given String with any characters invalid in HTML escaped.
     * Also if the given String is null or of zero length "&#160;" is returned
     * as an aid to displaying an HTML table element (if their is nothing in
     * the cell of an HTML table the borders of the cell are not displayed
     * resulting in strange looking tables)
     * @return The formatted String
     * @param text The String to "prepare"
     */
    public static String prepareHTML(String text) {
        //weed out all bad characters
        if(text == null || text.length() == 0) return "&#160;";
        return prepareHTMLBlank(text);
    }

    /** Returns the given String with any characters invalid in HTML escaped.
     * Also if the given String is null or of zero length "" is returned
     * @return The formatted String
     * @param text The String to "prepare"
     */
    public static String prepareHTMLBlank(String text) {
        if(text == null || text.length() == 0) return "";
		try {
			return appendHTMLBlank(new StringBuilder(), text).toString();
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e);
		}
	}

	public static Appendable appendHTML(Appendable appendTo, String text) throws IOException {
		if(text == null || text.length() == 0)
			return appendTo.append("&#160;");
		else
			return appendHTMLBlank(appendTo, text);
	}

	public static Appendable appendHTMLBlank(Appendable appendTo, String inputText) throws IOException {
		String text= inputText;
		if(text != null) {
			text = text.replaceAll("<br */>", "\n");
			for(int i = 0; i < text.length(); i++) {
				char c = text.charAt(i);
				switch(c) {
					case '&':
				/*	appendTo.append("&amp;");
						break;
					
					boolean replace = true;
					if(text.length() > i+1 && text.charAt(i+1) == '#') {
					    check: for(int p = 2; p + i < text.length(); p++) {
					        switch(text.charAt(p+ i)) {
					            case ';':
					                if(p > 2) replace = false;
					                break check;
					            case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
					                break;
					            default:
					                break check;
					        }
					    }
					} else {
					    for(int p = 1; p + i < text.length(); p++) {
					        char ch = text.charAt(p+ i);
					        if(ch == ';') {
					            if(p > 2) replace = false;
					            break;
					        } else if(!Character.isLetter(ch)){
					            break;
					        }
					    }
					}
					if(!replace) {
					    appendTo.append(c);
					    break;
					}*/
					case '<':
					case '>':
					case '\'':
					case '"':
					case '/':
						appendTo.append("&#" + String.valueOf((int) c) + ";");
						break;
					case '\t':
						appendTo.append("&#160;&#160;&#160;&#160;&#160;");
						break;
					case '\n':
						if(i + 1 < text.length() && text.charAt(i + 1) == '\r')
							i++;
						appendTo.append("<br/>");
						break;
					case '\r':
						if(i + 1 < text.length() && text.charAt(i + 1) == '\n')
							i++;
						appendTo.append("<br/>");
						break;
					default:
						appendTo.append(c);
				}
			}
		}
		return appendTo;
    }

	public static Appendable appendHTMLBlank(Appendable appendTo, Readable text) throws IOException {
		return appendHTMLBlank(appendTo, text, 1024);
	}

	public static Appendable appendHTMLBlank(Appendable appendTo, Readable text, int bufferSize) throws IOException {
		if(text != null) {
			CharBuffer buffer = CharBuffer.allocate(bufferSize > 0 ? bufferSize : 1024);
			char ignoreNL = ' ';
			while(true) {
				int n = text.read(buffer);
				if(n < 0)
					break;
				for(int i = 0; i < n; i++) {
					char c = buffer.get(i);
					switch(c) {
						case '&':
							appendTo.append("&amp;");
							break;
						case '<':
						case '>':
						case '\'':
						case '"':
							appendTo.append("&#" + String.valueOf((int) c) + ";");
							break;
						case '\t':
							appendTo.append("&#160;&#160;&#160;&#160;&#160;");
							break;
						case '\n':
							if(ignoreNL == '\n')
								ignoreNL = ' ';
							else {
								ignoreNL = '\r';
								appendTo.append("<br/>");
							}
							break;
						case '\r':
							if(ignoreNL == '\r')
								ignoreNL = ' ';
							else {
								ignoreNL = '\n';
								appendTo.append("<br/>");
							}
							break;
						default:
							appendTo.append(c);
					}
				}
				buffer.clear();
			}
		}
		return appendTo;
	}

    /** Returns the given String with any characters invalid in escaped for CDATA.
     * Also if the given String is null or of zero length "" is returned
     * @return The formatted String
     * @param text The String to "prepare"
     */
    public static String prepareCDATA(String text) {
		if(text == null || text.length() == 0)
			return "";
		try {
			return appendCDATA(new StringBuilder(), text).toString();
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e);
		}
	}

	public static Appendable appendCDATA(Appendable appendTo, String text) throws IOException {
		if(text != null)
			for(int i = 0; i < text.length(); i++) {
				char c = text.charAt(i);
				int decimal = c;
				switch(c) {
					case '&':
					case '<':
					case '>':
					case '\'':
					case '"':
					case '/':
					case '\t':
						appendTo.append("&#" + String.valueOf(decimal) + ";");
						break;
					case '\n':
						if(i + 1 < text.length() && text.charAt(i + 1) == '\r')
							i++;
						appendTo.append(" ");
						appendTo.append("&#" + String.valueOf(decimal) + ";");
						break;
					case '\r':
						if(i + 1 < text.length() && text.charAt(i + 1) == '\n')
							i++;
						appendTo.append(" ");
						appendTo.append("&#" + String.valueOf(decimal) + ";");
						break;
					default:
						if (decimal < 32 || decimal > 126)
							appendTo.append("&#" + String.valueOf(decimal) + ";");
						else
							appendTo.append(c);
				}
			}
		return appendTo;
    }

    /** Returns the given String with any characters invalid in escaped for CDATA.
     * Also if the given String is null or of zero length "" is returned
     * @return The formatted String
     * @param text The String to "prepare"
     */
    public static String prepareJMXNameValue(String text) {
        if(text == null || text.length() == 0) return "";
        StringBuilder result = new StringBuilder();
        result.append('"');
        boolean quoted = false;
        for(int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            switch(c) {//comma, equals, colon, or quote, A question mark (?) or asterisk (*). , newline
                case ',':
                case '=':
                case ':':
                	quoted = true;
                	result.append(c);
                	break;
                case '"':
                case '\\':
                case '?':
                case '*':
                	quoted = true;
                	result.append('\\').append(c);
                	break;
                case '\n':
                	quoted = true;
                	result.append('\\').append('n');
                	break;
                default:
                    result.append(c);
            }
        }
        if(quoted)
        	return result.append('"').toString();
        else
        	return text;
    }

    /** Returns a String representing the escaped character for rich-text format
     * or null if the character does not need to be escaped
     * @param c The character
     * @return The replacement
     */
    protected static String getRTFEquiv(char c) {
        switch(c) {
            case '\\': case '{': case '}':
                return "\\" + c;
            default:
                return null;
        }
    }

    /** Returns a String representing the escaped character for javascript
     * or null if the character does not need to be escaped
     * @param c The character
     * @return The replacement
     */
    protected static String getScriptEquiv(char c) {
        switch(c) {
			case '/':
            case '%':
            case '\'':
            case '"':
            case '\\':
			case '<':
			case '>':
			case '&':
			case '\n':
            case '\r': //return the unicode escaped string for these characters
                StringBuilder sb = new StringBuilder(Integer.toHexString(c));
                while(sb.length() < 4) sb.insert(0,'0');
                sb.insert(0, "\\u");
                return sb.toString();
            default:
                return null;
        }
    }

    /** Returns a String representing the escaped character for sql
     * or null if the character does not need to be escaped
     * @param c The character
     * @return The replacement
     */
    protected static String getSQLEquiv(char c) {
        switch(c) {
            case '\'':
                return "''";
            default:
                return null;
        }
    }

    /** Returns a String with an whitespace replaced with "&#160;" for HTML
     * formatting
     * @return The formatted String
     * @param text The String to "prepare"
     */
    public static String formatWithNoBreaks(String text) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if(Character.isWhitespace(c)) sb.append("&#160;");
            else sb.append(c);
        }
        return sb.toString();
    }
    /**
     * <p>Gets the substring before the first occurrence of a separator.
     * The separator is not returned.</p>
     *
     * <p>A <code>null</code> string input will return <code>null</code>.
     * An empty ("") string input will return the empty string.
     * A <code>null</code> separator will return the input string.</p>
     *
     * <pre>
     * StringUtils.substringBefore(null, *)      = null
     * StringUtils.substringBefore("", *)        = ""
     * StringUtils.substringBefore("abc", "a")   = ""
     * StringUtils.substringBefore("abcba", "b") = "a"
     * StringUtils.substringBefore("abc", "c")   = "ab"
     * StringUtils.substringBefore("abc", "d")   = "abc"
     * StringUtils.substringBefore("abc", "")    = ""
     * StringUtils.substringBefore("abc", null)  = "abc"
     * </pre>
     *
     * @param str  the String to get a substring from, may be null
     * @param separator  the String to search for, may be null
     * @return the substring before the first occurrence of the separator,
     *  <code>null</code> if null String input
     * @since 2.0
     */
    public static String substringBefore(String str, String separator) {
        if (str == null || str.length() == 0 || separator == null) {
            return str;
        }
        if (separator.length() == 0) {
            return "";
        }
        int pos = str.indexOf(separator);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }

    /**
     * <p>Gets the substring after the first occurrence of a separator.
     * The separator is not returned.</p>
     *
     * <p>A <code>null</code> string input will return <code>null</code>.
     * An empty ("") string input will return the empty string.
     * A <code>null</code> separator will return the empty string if the
     * input string is not <code>null</code>.</p>
     *
     * <pre>
     * StringUtils.substringAfter(null, *)      = null
     * StringUtils.substringAfter("", *)        = ""
     * StringUtils.substringAfter(*, null)      = ""
     * StringUtils.substringAfter("abc", "a")   = "bc"
     * StringUtils.substringAfter("abcba", "b") = "cba"
     * StringUtils.substringAfter("abc", "c")   = ""
     * StringUtils.substringAfter("abc", "d")   = ""
     * StringUtils.substringAfter("abc", "")    = "abc"
     * </pre>
     *
     * @param str  the String to get a substring from, may be null
     * @param separator  the String to search for, may be null
     * @return the substring after the first occurrence of the separator,
     *  <code>null</code> if null String input
     * @since 2.0
     */
    public static String substringAfter(String str, String separator) {
        if (str == null || str.length() == 0) {
            return str;
        }
        if (separator == null) {
            return "";
        }
        int pos = str.indexOf(separator);
        if (pos == -1) {
            return "";
        }
        return str.substring(pos + separator.length());
    }

    public static String substringAfterLast(String str, String separator) {
        if (str == null || str.length() == 0) {
            return str;
        }
        if (separator == null) {
            return "";
        }
        int pos = str.lastIndexOf(separator);
        if (pos == -1) {
            return "";
        }
        return str.substring(pos + separator.length());
    }
    public static String substringBeforeLast(String str, char[] separators) {
        if(str == null || str.length() == 0) {
            return str;
        }
        if(separators == null || separators.length == 0) {
            return str;
        }
        int pos = lastIndexOf(str, separators);
        if (pos == -1) {
            return "";
        }
        return str.substring(0, pos);
    }

	public static String substringAfterLast(String str, char[] separators) {
		if(str == null || str.length() == 0) {
			return str;
		}
		if(separators == null || separators.length == 0) {
			return str;
		}
		int pos = lastIndexOf(str, separators);
		if(pos == -1) {
			return "";
		}
		return str.substring(pos + 1);
	}
	public static boolean isBlank(String str) {
		return str == null || str.trim().length() == 0;
	}

	public static boolean regionsMatch(CharSequence cs1, int start1, CharSequence cs2, int start2, int length) {
		for(int i = 0; i < length; i++) {
			if(cs1.charAt(i + start1) != cs2.charAt(i + start2))
				return false;
		}
		return true;
	}
	
	public static String objectToString (Object object, String prefix) {
		Map<String,?> props;
		try {
			props = ReflectionUtils.toPropertyMap(object);
		} catch (IntrospectionException e) {
			return "Error occurred: " + e.getMessage();
		}
		StringBuilder sb = new StringBuilder();
		for (String key: props.keySet()) {
			if ("class".equals(key))
				continue;
			sb.append(", ").append(key).append(": ");
			Object value = props.get(key);
			if (value instanceof Object[]) {
				Object[] array = (Object[]) value;
				for (int i=0; i<array.length; i++)
					sb.append(objectToString(array[i], "[")).append("]");
			} else if (value instanceof Calendar) {
				sb.append(((Calendar) value).getTime());
			} else
				sb.append(value);
		}
		if (prefix != null)
			sb.replace(0, 2, prefix);
		return sb.toString();
	}
	
	public static String formatCardNumberForDisplay(String cardNumber) {
		if (cardNumber == null || cardNumber.length() == 0)
			return "";
		int len = cardNumber.length();
		if (len < 5)
			return cardNumber;
		int pos = 0;
		StringBuilder sb = new StringBuilder();		
		while (pos + 4 < len) {
			sb.append(cardNumber.substring(pos, pos + 4)).append(" ");
			pos += 4;
		}
		if (pos < len)
			sb.append(cardNumber.substring(pos));
		return sb.toString();
	}
	
	public String getDelimitedFieldValue(String data, String fieldKey)
	{
		if (isBlank(data))
			return null; 
		StringBuilder sb = new StringBuilder(FS).append(fieldKey).append('=');
		int fromIndex = data.indexOf(sb.toString());
		if (fromIndex > -1)
			return data.substring(data.indexOf('=', fromIndex) + 1, data.indexOf(FS, fromIndex));
		return null;
	}
	
	public static String getDate(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return dateFormat.format(calendar.getTime());
	}

	public static String toWords(CharSequence camelcase) {
		if(camelcase == null)
			return null;
		if(camelcase.length() == 0)
			return "";
		StringBuilder sb = new StringBuilder();
		char prev = camelcase.charAt(0);
		sb.append(prev);
		int prevType = Character.getType(prev);
		for(int i = 1; i < camelcase.length(); i++) {
			char ch = camelcase.charAt(i);
			/*
			if(ch == '-' || ch == '_') {
				ch = ' ';
			} else if(Character.isDigit(ch)) {
				if(!Character.isDigit(prev))
					sb.append(' ');
			} else if(!Character.isLetter(ch)) {
				if(Character.isLetterOrDigit(prev))
					sb.append(' ');
			} else if(Character.isUpperCase(ch) && !Character.isUpperCase(prev))
				sb.append(' ');
			*/
			int type = Character.getType(ch);
			switch(type) {
				case Character.DECIMAL_DIGIT_NUMBER:
					switch(prevType) {
						case Character.UPPERCASE_LETTER:
						case Character.LOWERCASE_LETTER:
							sb.append(' ');
					}
					break;
				case Character.DASH_PUNCTUATION:
				case Character.CONNECTOR_PUNCTUATION:
					ch = ' ';
					break;
				case Character.UPPERCASE_LETTER:
					switch(prevType) {
						case Character.LOWERCASE_LETTER:
						case Character.DECIMAL_DIGIT_NUMBER:
							sb.append(' ');
					}
					break;
				case Character.LOWERCASE_LETTER:
					if(prevType == Character.UPPERCASE_LETTER && sb.length() > 2 && sb.charAt(sb.length() - 2) != ' ')
						sb.insert(sb.length() - 1, ' ');
					break;
				case Character.COMBINING_SPACING_MARK:
				case Character.CONTROL:
				case Character.ENCLOSING_MARK:
				case Character.END_PUNCTUATION:
				case Character.FINAL_QUOTE_PUNCTUATION:
				case Character.FORMAT:
				case Character.INITIAL_QUOTE_PUNCTUATION:
				case Character.LETTER_NUMBER:
				case Character.LINE_SEPARATOR:
				case Character.MATH_SYMBOL:
				case Character.MODIFIER_LETTER:
				case Character.MODIFIER_SYMBOL:
				case Character.NON_SPACING_MARK:
				case Character.OTHER_LETTER:
				case Character.OTHER_NUMBER:
				case Character.OTHER_PUNCTUATION:
				case Character.OTHER_SYMBOL:
				case Character.PARAGRAPH_SEPARATOR:
				case Character.PRIVATE_USE:
				case Character.SPACE_SEPARATOR:
				case Character.CURRENCY_SYMBOL:
				case Character.START_PUNCTUATION:
				case Character.SURROGATE:
				case Character.TITLECASE_LETTER:
				case Character.UNASSIGNED:
					break;
			}
			sb.append(ch);
			prev = ch;
			prevType = type;
		}
		return sb.toString();
	}

	public static Map<String,Object> getQueryParams(String url) {
		try {
			Map<String,Object> params = new HashMap<>();
			String[] urlParts = url.split("\\?");
			if (urlParts.length > 1) {
				String query = urlParts[1];
				for (String param : query.split("&")) {
					String[] pair = param.split("=");
					String key = URLDecoder.decode(pair[0], "UTF-8");
					String value = "";
					if (pair.length > 1) {
						value = URLDecoder.decode(pair[1], "UTF-8");
					}
					params.put(key, value);
				}
			}

			return params;
		} catch (UnsupportedEncodingException ex) {
			throw new RuntimeException(ex);
		}
	}

	static Pattern NOT_NUMBER_PATTERN = Pattern.compile("\\D+");

	public static String[] splitNumbers(String numbers) {
		if (numbers == null) {
			return null;
		}
		return NOT_NUMBER_PATTERN.split(numbers);
	}
	
	public static String getCSVParamList(String paramLines) {
		if (paramLines == null)
			return paramLines;
		return paramLines.replace('\n', ',').replace("\r", "").replace(" ", "");
	}

	public static String removeCRLF(String param) {
		if (param == null)
			return null;
		return param.replace("\r", "").replace("\n", "");
	}

	public static String encodeForHTML(String param) {
		if (param == null)
			return "";
		return ESAPI.encoder().encodeForHTML(param);
	}

	public static String encodeForHTML(Object param) {
		if (param == null)
			return "";
		return ESAPI.encoder().encodeForHTML(param.toString());
	}

	public static String encodeForHTMLAttribute(String param) {
		if (param == null)
			return "";
		return ESAPI.encoder().encodeForHTMLAttribute(param);
	}

	public static String encodeForHTMLAttribute(Object param) {
		if (param == null)
			return "";
		return ESAPI.encoder().encodeForHTMLAttribute(param.toString());
	}

	public static String encodeForURL(String param) {
		if (param == null)
			return "";
		try {
			return ESAPI.encoder().encodeForURL(param);
		} catch (EncodingException e) {
			return "";
		}
	}

	public static String encodeForJavaScript(String param) {
		if (param == null)
			return "";
		return ESAPI.encoder().encodeForJavaScript(param);
	}

	public static String encodeForJavaScript(Object param) {
		if (param == null)
			return "";
		return ESAPI.encoder().encodeForJavaScript(param.toString());
	}
	
	public static String encodeForLDAP(String param) {
		if (param == null)
			return "";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < param.length(); i++) {
			char c = param.charAt(i);
			switch (c) {
			case '\\':
				sb.append("\\5c");
				break;
			case '*':
				sb.append("\\2a");
				break;
			case '(':
				sb.append("\\28");
				break;
			case ')':
				sb.append("\\29");
				break;
			case '\0':
				sb.append("\\00");
				break;
			default:
				sb.append(c);
			}
		}
		return sb.toString();
		//return ESAPI.encoder().encodeForLDAP(param);
	}

	public static boolean isValidSafeHTML(String context, String input, int maxLength, boolean allowNull) {
		return ESAPI.validator().isValidSafeHTML(context, input, maxLength, allowNull);
	}
	
	public static String getValidSafeHTML(String context, String input, int maxLength, boolean allowNull) throws IntrusionException, ValidationException {
		return ESAPI.validator().getValidSafeHTML(context, input, maxLength, allowNull);
	}
	
}
