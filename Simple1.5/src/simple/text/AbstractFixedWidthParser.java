/**
 *
 */
package simple.text;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.results.DatasetHandler;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractFixedWidthParser {
	protected String lineSeparator = "\r\n";

	protected abstract String[] getColumnLabels() throws IOException ;
	protected abstract FixedWidthColumn getFirstColumn() throws IOException ;
	protected abstract List<FixedWidthColumn> getRemainingColumns(Object selectorValue) throws IOException ;
	
	public <I extends Exception> void parse(Reader reader, DatasetHandler<I> handler) throws IOException, I {
		FixedWidthColumn first = getFirstColumn();
		handler.handleDatasetStart(getColumnLabels());
		int cnt = 0;
		while(true) {
			String s = readString(reader, first.getSize());
			if(s == null)
				break;
			handler.handleRowStart();
			cnt++;
			Object selectorValue = handleValue(first, s, handler);
			List<FixedWidthColumn> columns = getRemainingColumns(selectorValue);
			if(columns != null)
				for(FixedWidthColumn column : columns) {
					s = readString(reader, column.getSize());
					if(s == null)
						throw new EOFException("End of Stream encountered while reading column '" + column.getLabel() + "' line #" + cnt);
					handleValue(column, s, handler);
				}
			if(lineSeparator != null && lineSeparator.length() > 0) {
				s = readString(reader, lineSeparator.length());
				if(s == null) 
					break;
				if(!s.equals(lineSeparator)) {
					if(columns == null)
						throw new IOException("Selector Value '" + selectorValue + "' does not match any configured selectors at line #" + cnt);
					else
						throw new IOException("Line Separator for line #" + cnt + " (" + StringUtils.toHex(s.getBytes())+ ") does not match configured line separator '" + StringUtils.toHex(lineSeparator.getBytes()) + "'");
				}
			}
			handler.handleRowEnd();			
		}
		handler.handleDatasetEnd();
	}
	
	protected <I extends Exception> Object handleValue(FixedWidthColumn column, String stringValue, DatasetHandler<I> handler) throws IOException, I {
		stringValue = StringUtils.unpad(stringValue, column.getJustification(), column.getPadding());
		Object value;
		if(column.getType() == null)
			value = stringValue;
		else if(stringValue.length() == 0 && column.getPadding() == '0' && Number.class.isAssignableFrom(column.getType())) {
			try {
				value = ConvertUtils.convert(column.getType(), 0);
			} catch(ConvertException e) {
				throw new IOException("Could not convert zero(s) in column '" + column.getLabel() + "'", e);
			}
		} else
			try {
				value = ConvertUtils.convert(column.getType(), stringValue);
			} catch(ConvertException e) {
				throw new IOException("Could not convert data in column '" + column.getLabel() + "'", e);
			}
		handler.handleValue(column.getLabel(), value);
		return value;
	}
	
	protected String readString(Reader reader, int len) throws IOException {
		char[] cbuf = new char[len];
		int r;
		int tot = 0;
		while(tot < len) {
			r = reader.read(cbuf, tot, len - tot);
			if(r < 0) 
				return null;
			tot += r;
		}
		return new String(cbuf);
	}
	public String getLineSeparator() {
		return lineSeparator;
	}
	public void setLineSeparator(String lineSeparator) {
		this.lineSeparator = lineSeparator;
	}	
}
