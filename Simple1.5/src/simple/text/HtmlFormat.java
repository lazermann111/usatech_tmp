package simple.text;

/**
 * @author pcowan
 *
 */
public class HtmlFormat extends PrepareFormat
{
	private static final long serialVersionUID = 82205725785825L;

	public HtmlFormat()
	{
		super("HTML");
	}
}
