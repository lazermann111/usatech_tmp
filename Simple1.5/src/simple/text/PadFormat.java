/*
 * PadFormat.java
 *
 * Created on May 21, 2002, 11:52 AM
 */

package simple.text;

/**
 *
 * @author  bkrug
 * @version
 */
import java.text.Format;

import simple.bean.ConvertUtils;

public class PadFormat extends AbstractPatternedFormat {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8113354288914L;
	protected int length;
	protected boolean padRight;
	protected char[] padding = null;
	protected Format firstFormat = null;
	
	/** Creates new PadFormat */
	public PadFormat(int length, char padChar, boolean padRight) {
		this(length, padChar, padRight, null);
	}
	
	public PadFormat(int length, char padChar, boolean padRight, Format firstFormat) {
		this(makePattern(length, padChar, padRight));
        this.firstFormat = firstFormat;
	}
	
    protected static String makePattern(int length, char padChar, boolean padRight) {
        String pad = StringUtils.fillString(length, "" + padChar);
        if(padRight) return "*" + pad;
        else return pad + "*";
    }
    
	public PadFormat(String pattern) {
        super(pattern);
		if(pattern.startsWith("*")) {
			padRight = true;
			padding = pattern.substring(1).toCharArray();
			length = pattern.length() - 1;			
		} else if(pattern.endsWith("*")) {
			padRight = false;
			padding = pattern.substring(0, pattern.length() - 1).toCharArray();
			length = pattern.length() - 1;
		} else {
			throw new IllegalArgumentException("Pattern must start or end with '*' to indicate whether left or right padding should be used");
		}
	}
	
	public java.lang.StringBuffer format(java.lang.Object obj, java.lang.StringBuffer stringBuffer, java.text.FieldPosition fieldPosition) {
		String s = null;
		if(firstFormat != null) try { s = firstFormat.format(obj); } catch(Exception e) {}
		else if(obj != null) s = obj.toString();
		
		if(s == null) stringBuffer.append(padding);
		else if(s.length() > length) {
			if(padRight) stringBuffer.append(s.substring(0,length));
			else stringBuffer.append(s.substring(s.length()-length));
		} else {
			if(!padRight && s.length() < length) stringBuffer.append(padding, 0, length - s.length());
			stringBuffer.append(s);
			if(padRight  && s.length() < length) stringBuffer.append(padding, s.length(), length - s.length());
		}
		return stringBuffer;
	}
	
	public java.lang.Object parseObject(java.lang.String str, java.text.ParsePosition parsePosition) {
		int i;
		String ret;
		if(str == null) ret = null;
		else if(!padRight) {
			for(i = 0; i < length && str.charAt(i) == padding[i]; i++) ;
			ret = str.substring(i,length);
		} else {
			for(i = length; i > 0 && str.charAt(i - 1) == padding[i-1]; i--) ;
			ret = str.substring(0,i);
		}
		if(firstFormat == null) {
			parsePosition.setIndex(length);
			return ret;
		} else {
			return firstFormat.parseObject(ret, parsePosition);
		}
	}

    public Format getFirstFormat() {
        return firstFormat;
    }

    public void setFirstFormat(Format firstFormat) {
        this.firstFormat = firstFormat;
    }

    @Override
    public boolean equals(Object obj) {
       return super.equals(obj) && ConvertUtils.areEqual(getFirstFormat(), ((PadFormat)obj).getFirstFormat());
    } 
    
}
