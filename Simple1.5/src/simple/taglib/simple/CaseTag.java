/*
 * CaseTag.java
 *
 * Created on November 24, 2003, 10:18 AM
 */

package simple.taglib.simple;

import javax.servlet.jsp.JspException;

/**
 *
 * @author  Brian S. Krug
 */
public class CaseTag extends IfTag {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of CaseTag */
    public CaseTag() {
    }
    
    public int doStartTag() throws JspException {
        int ret = super.doStartTag();
        if(ret == EVAL_BODY_INCLUDE) {
            getParentSwitch().childEvaluated();
        }
        return ret;
    }
    
    public String getOperator() {
        if(super.getOperator() == null) return getParentSwitch().getOperator();
        else return super.getOperator();
    }
    
    protected Object getRequestObject() throws JspException {
        if(super.getBean() == null && super.getProperty() == null) return getParentSwitch().getRequestObject();
        else return super.getRequestObject();
    }
    
    protected SwitchTag getParentSwitch() {
        return (SwitchTag) getParent();
    }    
}
