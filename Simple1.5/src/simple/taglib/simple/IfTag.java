/*
 * IfTag.java
 *
 * Created on January 9, 2003, 5:30 PM
 */

package simple.taglib.simple;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;

import simple.bean.ConditionUtils;
import simple.servlet.RequestUtils;
import simple.taglib.SimpleTagSupport;

/** Evaluates the body of this tag only if the condition is met.
 *
 * @author  Brian S. Krug
 */
public class IfTag extends SimpleTagSupport {   
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Holds value of property bean. */
    private String bean;
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property property. */
    private String property;
    
    /** Holds value of property value. */
    private String value;
    
    /** Holds value of property operator. */
    private String operator;
    
    /** Holds value of property valueBean. */
    private String valueBean;
    
    /** Creates new IfTag */
    public IfTag() {
    }

    public int doStartTag() throws JspException {
        if(condition()) return EVAL_BODY_INCLUDE;
        else return SKIP_BODY;
    }
    
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
    
    protected boolean condition() throws JspException {
        return ConditionUtils.check(getRequestObject(), getOperator(), getValueObject());
    }       

    protected Object getValueObject() throws JspException {
        if(value != null)  return value; 
        else if(valueBean != null)
            try {
                Object rv = RequestUtils.getAttribute(pageContext,valueBean,false,scope);
                return rv;
            } catch(ServletException se) {
                throw new JspException(RequestUtils.exceptionToString(se));
            }  
        else return null;
    }
    
    protected Object getRequestObject() throws JspException {
        String tmp = (bean != null ? bean : "") + 
            (bean != null && property != null ? "." : "") + 
            (property != null ? property : "");
        if(tmp != null) {
            try {
                Object rv = RequestUtils.getAttribute(pageContext,tmp,false,scope);
                return rv;
            } catch(ServletException se) {
                throw new JspException(RequestUtils.exceptionToString(se));
            }
        }
        return null;
    }
    
    /** Getter for property bean.
     * @return Value of property bean.
     */
    public String getBean() {
        return this.bean;
    }
    
    /** Setter for property bean.
     * @param bean New value of property bean.
     */
    public void setBean(String bean) {
        this.bean = bean;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /** Getter for property property.
     * @return Value of property property.
     */
    public String getProperty() {
        return this.property;
    }
    
    /** Setter for property property.
     * @param property New value of property property.
     */
    public void setProperty(String property) {
        this.property = property;
    }
    
    /** Getter for property value.
     * @return Value of property value.
     */
    public String getValue() {
        return this.value;
    }
    
    /** Setter for property value.
     * @param value New value of property value.
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    /** Getter for property operator.
     * @return Value of property operator.
     */
    public String getOperator() {
        return this.operator;
    }
    
    /** Setter for property operator.
     * @param operator New value of property operator.
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }
    
    /** Getter for property valueBean.
     * @return Value of property valueBean.
     */
    public String getValueBean() {
        return this.valueBean;
    }
    
    /** Setter for property valueBean.
     * @param valueBean New value of property valueBean.
     */
    public void setValueBean(String valueBean) {
        this.valueBean = valueBean;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        bean = null;
        operator = null;
        property = null;
        scope = null;
        value = null;
        valueBean = null;
    }
}
