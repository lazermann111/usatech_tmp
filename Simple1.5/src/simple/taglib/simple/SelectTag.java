/*
 * SelectTag.java
 */


package simple.taglib.simple;


import javax.servlet.jsp.JspException;

import simple.bean.ConvertUtils;
import simple.servlet.ResponseUtils;

/**
 * Convenience base class for the various input tags for text fields.
 *
 * @author BSK
 */

public class SelectTag extends BaseControlTag {    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Object valueObject = null;
    private boolean multiple;
    
    /** Holds value of property required. */
    private boolean required;
    
    protected void prepareTag(StringBuilder results) throws JspException {
        results.append("<select");
        if(required) results.append(" required=\"true\"");
        if(value != null) valueObject = value;
        else valueObject = getRequestValue();
    }
    
    /** Write body content
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException {
        // Render the output from this iteration to the output stream
        if (bodyContent != null) {
            ResponseUtils.writePrevious(pageContext, bodyContent.getString());
            bodyContent.clearBody();
        }
        return SKIP_BODY;
    }
    
    public int doEndTag() throws JspException {
        ResponseUtils.write(pageContext, "</select>");
        return EVAL_PAGE;
    }

    public boolean isMatch(Object optionValue) {
        if(valueObject == null) return false;
        try {
            optionValue = ConvertUtils.convert(valueObject.getClass(), optionValue);
        } catch(simple.bean.ConvertException ce) {            
        }
        debug("Checking if " + valueObject + " equals " + optionValue);
        return (optionValue != null && valueObject.equals(optionValue));
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        value = null;
        multiple = false;
        valueObject = null;
    }

    /** Getter for property multiple.
     * @return Value of property multiple.
     */
    public boolean isMultiple() {
        return multiple;
    }
    
    /** Setter for property multiple.
     * @param multiple New value of property multiple.
     */
    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }
    
    /** Getter for property required.
     * @return Value of property required.
     */
    public boolean isRequired() {
        return required;
    }
    
    /** Setter for property required.
     * @param required New value of property required.
     */
    public void setRequired(boolean required) {
        this.required = required;
    }
    
}
