/*
 * FieldTag.java
 */


package simple.taglib.simple;


import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.servlet.RequestUtils;
import simple.servlet.ResponseUtils;
import simple.text.StringUtils;
import simple.util.LinkedHashMap;

/**
 * This tag morphs into an INPUT or SELECT or CHECKBOX or RADIO tag dependent on its properties.
 * It is useful for dynamic page generation when the fields on a page are data-driven.
 *
 * @author BSK
 */

public class FieldTag extends BaseHandlerTag {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int FIELD_TYPE_TEXT = 1;
    public static final int FIELD_TYPE_INTEGER = 2;
    public static final int FIELD_TYPE_DECIMAL = 3;
    public static final int FIELD_TYPE_PHONE = 4;
    public static final int FIELD_TYPE_ZIP = 5;
    public static final int FIELD_TYPE_SELECT = 6;
    public static final int FIELD_TYPE_YES_NO = 7;
    public static final int FIELD_TYPE_FILE = 8;
    public static final int FIELD_TYPE_ON_OFF = 9;
    public static final int FIELD_TYPE_READONLY = 10;
    public static final int FIELD_TYPE_TIME = 11;
    public static final int FIELD_TYPE_RANGE = 12;

    protected static final Map OPTION_MAP_YES_NO = new LinkedHashMap();
    protected static final Map OPTION_MAP_ON_OFF = new LinkedHashMap();
    static {
        OPTION_MAP_YES_NO.put("N","No");
        OPTION_MAP_YES_NO.put("Y","Yes");
        OPTION_MAP_ON_OFF.put("0","Off");
        OPTION_MAP_ON_OFF.put("1","On");
    }
    
    protected Object dataValue;
    protected String descriptionValue;
    protected String fieldIdValue;
    protected int fieldTypeValue;
    protected String formatValue;
    protected int maximumValue;
    protected int minimumValue;
    protected boolean requiredValue;
    protected int widthValue;
    
    /** Holds value of property fieldType. */
    private String fieldType;
    
    /** Holds value of property maximum. */
    private String maximum;
    
    /** Holds value of property minimum. */
    private String minimum;
    
    /** Holds value of property required. */
    private String required;
    
    /** Holds value of property fieldId. */
    private String fieldId;
    
    /** Holds value of property prefix. */
    private String prefix;
    
    /** Holds value of property options. */
    private String options;
    
    /** Holds value of property data. */
    private String data;
    
    /** Holds value of property bean. */
    private String bean;
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property format. */
    private String format;
    
    /** Holds value of property description. */
    private String description;
    
    /** Holds value of property width. */
    private String width;
    
    /** Holds value of property styleId. */
    private String styleId;
    
    /** Holds value of property style. */
    private String style;
    
    public FieldTag() {
        release(); // set default values
    }
    
    public int doStartTag() throws JspException {
        populateValues();
        StringBuilder results = new StringBuilder();
        switch(fieldTypeValue) {
            case FIELD_TYPE_READONLY:
                //read-only
                results.append("<span");
                results.append(" style=\"border: 2px inset white;");
                if(widthValue > 0) results.append(" width:"+widthValue+"px;");        
                if(style != null) results.append(" " + StringUtils.prepareCDATA(style));
                results.append("\"");
                prepareEventHandlers(results);
                prepareStyles(results);
                results.append(">")
                       .append(StringUtils.prepareHTML(ConvertUtils.formatObject(dataValue, formatValue)))
                       .append("</span>"); 
                break;
            case FIELD_TYPE_FILE:
                results.append("<span id=\"display_" + prefix + StringUtils.prepareCDATA(fieldIdValue) + "\"");
                results.append(" style=\"border: 2px inset white;");
                if(widthValue > 0) results.append(" width:"+widthValue+"px;");        
                if(style != null) results.append(" " + StringUtils.prepareCDATA(style));
                results.append("\"");
                prepareMouseEvents(results);
                prepareKeyEvents(results);
                prepareFocusEvents(results);
                prepareStyles(results);
                results.append(">")
                       .append(StringUtils.prepareHTML(ConvertUtils.formatObject(dataValue, formatValue)))
                       .append("</span>");
                /* IE will not allow input type=file to be programmatically clicked - 
                   it yields an Access Denied error when submitting the form
                results.append("<button type=\"button\" onclick=\"this.form.")     
                       .append(prefix + ResponseUtils.prepareCDATA(fieldIdValue))
                       .append(".click()\"");
                prepareStyles(results);
                if(style != null) results.append(" style=\"" + style + "\"");
                results.append(">...</button><div style=\"display: none;\">");
                 */
                results.append("<div style=\"position:absolute; width: 5px; height: 22px;\"");
                if(getStyleClass() != null) results.append(" class=\"" + getStyleClass() + "\"");
                results.append("></div>");
                results.append("<input type=\"file\" name=\"")
                       .append(prefix + StringUtils.prepareCDATA(fieldIdValue))
                       .append("\" style=\"width: 0px;\" hidefocus=\"true\" unselectable=\"true\" tabindex=\"-1\"");
                if(minimumValue > 0) results.append(" minlength=\""+minimumValue+"\"");        
                if(requiredValue) results.append(" required=\"true\"");
                if(descriptionValue != null) results.append(" description=\""+descriptionValue+"\"");
                String tmp = getOnchange();
                if(tmp == null) tmp = "";
                tmp = "document.all['display_" + prefix + StringUtils.prepareCDATA(fieldIdValue) + "'].innerText = this.value; " + tmp;
                setOnchange(tmp);
                prepareStyles(results);
                prepareTextEvents(results);
                prepareMouseEvents(results);
                prepareOtherEvents(results);
                if(styleId != null) results.append(" id=\"" + styleId + "\"");
                results.append(">");
                //results.append("</div>");
                break;
            case FIELD_TYPE_TIME: 
                results.append("<select id=\"hh_" + prefix 
                    + StringUtils.prepareCDATA(fieldIdValue) 
                    +  "\" onchange=\"this.form." + prefix  
                    + StringUtils.prepareCDATA(fieldIdValue) + 
                    ".value = this.options[this.selectedIndex].value + ':' + document.all.mm_" + prefix  
                    + StringUtils.prepareCDATA(fieldIdValue) + 
                    ".options[document.all.mm_" + prefix  
                    + StringUtils.prepareCDATA(fieldIdValue) + 
                    ".selectedIndex].value + ':00'; this.form."
                    + prefix + StringUtils.prepareCDATA(fieldIdValue) + ".onchange();\"");
                if(style != null || widthValue > 0) {
                    results.append(" style=\"");
                    if(widthValue > 0) results.append(" width:"+widthValue+"px;");        
                    if(style != null) results.append(" " + StringUtils.prepareCDATA(style));
                    results.append("\"");
                }
                prepareMouseEvents(results);
                prepareKeyEvents(results);
                prepareFocusEvents(results);
                prepareStyles(results);
                results.append(">" + createHourOptions() + "</select>&nbsp;:&nbsp;");
                
                results.append("<select id=\"mm_" + prefix 
                    + StringUtils.prepareCDATA(fieldIdValue) 
                    +  "\" onchange=\"this.form." + prefix  
                    + StringUtils.prepareCDATA(fieldIdValue) + 
                    ".value = document.all.hh_" + prefix  
                    + StringUtils.prepareCDATA(fieldIdValue) + 
                    ".options[document.all.hh_" + prefix  
                    + StringUtils.prepareCDATA(fieldIdValue) + 
                    ".selectedIndex].value + ':' + this.options[this.selectedIndex].value + ':00'; this.form."
                    + prefix + StringUtils.prepareCDATA(fieldIdValue) + ".onchange();\"");
                if(style != null || widthValue > 0) {
                    results.append(" style=\"");
                    if(widthValue > 0) results.append(" width:"+widthValue+"px;");        
                    if(style != null) results.append(" " + StringUtils.prepareCDATA(style));
                    results.append("\"");
                }
                prepareMouseEvents(results);
                prepareKeyEvents(results);
                prepareFocusEvents(results);
                prepareStyles(results);
                results.append(">" + createMinuteOptions() + "</select>");
                                
                results.append("<input type=\"hidden\" name=\"" + prefix + StringUtils.prepareCDATA(fieldIdValue) + "\"")
                       .append(" value=\"" + StringUtils.prepareCDATA(ConvertUtils.formatObject(dataValue, formatValue)) + "\"");
                
                if(requiredValue) results.append(" required=\"true\"");
                if(descriptionValue != null) results.append(" description=\""+descriptionValue+"\"");
                prepareStyles(results);
                prepareTextEvents(results);
                prepareMouseEvents(results);
                prepareOtherEvents(results);
                if(styleId != null) results.append(" id=\"" + styleId + "\"");
                results.append(">");
                break;
            default:             
                results.append("<")
                       .append(getStartTagValue())
                       .append(" name=\"" + prefix + StringUtils.prepareCDATA(fieldIdValue) + "\"")
                       .append(" value=\"" + StringUtils.prepareCDATA(ConvertUtils.formatObject(dataValue, formatValue)) + "\"");
                //if(accept != null) results.append(" accept=\""+accept+"\"");
                if(fieldTypeValue == FIELD_TYPE_INTEGER || fieldTypeValue == FIELD_TYPE_DECIMAL) {
                    if(maximumValue > 0) results.append(" maximum=\""+maximumValue+"\"");
                    if(minimumValue > 0) results.append(" minimum=\""+minimumValue+"\"");                            
                } else if(fieldTypeValue != FIELD_TYPE_RANGE) {
                    if(maximumValue > 0) results.append(" maxlength=\""+maximumValue+"\"");
                    if(minimumValue > 0) results.append(" minlength=\""+minimumValue+"\"");        
                }
                if(style != null || widthValue > 0) {
                    results.append(" style=\"");
                    if(widthValue > 0) results.append(" width:"+widthValue+"px;");        
                    if(style != null) results.append(" " + StringUtils.prepareCDATA(style));
                    results.append("\"");
                }
                //if(getMaskValue() != null) results.append(" mask=\""+getMaskValue()+"\"");
                if(requiredValue) results.append(" required=\"true\"");
                if(descriptionValue != null) results.append(" description=\""+descriptionValue+"\"");
                prepareEventHandlers(results);
                prepareStyles(results);
                results.append(">");
                results.append(getBodyValue());
        }
        ResponseUtils.write(pageContext, results.toString()); // Print this field to our output writer  
        return EVAL_BODY_BUFFERED;// Continue processing this page
    }
    
    protected void populateValues() throws JspException {
        Object beanValue = lookup(null, bean);
        if(bean != null && bean.length() > 0 && beanValue == null) throw new JspException("Could not find bean '" + bean + "' in the request");
        Object tmp;
        
        tmp = lookup(beanValue, getFieldId());
        if(tmp == null) throw new JspException("Could not find field id, '" + getFieldId() + "'");
        fieldIdValue = tmp.toString();
        
        tmp = lookup(beanValue, getFormat());
        formatValue  = (tmp == null || tmp.toString().length() == 0 ? null : tmp.toString());
        
        tmp = lookup(beanValue, getDescription());
        descriptionValue = (tmp == null || tmp.toString().length() == 0 ? null : StringUtils.prepareCDATA(tmp.toString()));
        
        tmp = lookup(beanValue, getData());
        dataValue = tmp;
        
        tmp = lookup(beanValue, getFieldType());
        if(tmp == null) fieldTypeValue = 0;
        else if(tmp instanceof Number) fieldTypeValue = ((Number)tmp).intValue();
        else try {
            fieldTypeValue = Integer.parseInt(tmp.toString());
        } catch(NumberFormatException nfe) {
            fieldTypeValue = 0;
        }
        
        tmp = lookup(beanValue, getMaximum());
        if(tmp == null) maximumValue = 0;
        else if(tmp instanceof Number) maximumValue = ((Number)tmp).intValue();
        else try {
            maximumValue = Integer.parseInt(tmp.toString());
        } catch(NumberFormatException nfe) {
            maximumValue = 0;
        }
        
        tmp = lookup(beanValue, getMinimum());
        if(tmp == null) minimumValue = 0;
        else if(tmp instanceof Number) minimumValue = ((Number)tmp).intValue();
        else try {
            minimumValue = Integer.parseInt(tmp.toString());
        } catch(NumberFormatException nfe) {
            minimumValue = 0;
        }
        
        tmp = lookup(beanValue, getRequired());
        try {
            requiredValue = ConvertUtils.getBoolean(beanValue, false);
        } catch(ConvertException e) {
            requiredValue = false;
        }
        tmp = lookup(beanValue, getWidth());
        if(tmp == null) widthValue = 0;
        else if(tmp instanceof Number) widthValue = ((Number)tmp).intValue();
        else try {
            widthValue = Integer.parseInt(tmp.toString());
        } catch(NumberFormatException nfe) {
            widthValue = 0;
        }
    }
    
    protected Object lookup(Object beanValue, String id) throws JspException {
        try {
            if(beanValue != null) return ReflectionUtils.getProperty(beanValue, id);
            if(id == null || id.trim().length() == 0) return null;
            Object rv = RequestUtils.getAttribute(pageContext,id,false,scope);
            return rv;
        } catch(ServletException e) {
            throw new JspException(e);
        } catch (IntrospectionException e) {
            throw new JspException(e);
        } catch (IllegalAccessException e) {
            throw new JspException(e);
        } catch (InvocationTargetException e) {
            throw new JspException(e);
        } catch(ParseException e) {
        	throw new JspException(e);
		}
    }
 
    protected String getStartTagValue() throws JspException {
        switch(fieldTypeValue) {
            case FIELD_TYPE_INTEGER:
                return "input type=\"text\" mask=\"integer\"";
            case FIELD_TYPE_FILE:
                return "input type=\"file\"";// filter=\"*.bmp;*.gif;*.png\"";
            case FIELD_TYPE_SELECT: case FIELD_TYPE_YES_NO: 
            case FIELD_TYPE_ON_OFF: case FIELD_TYPE_RANGE:
                return "select";
            case FIELD_TYPE_TEXT: default:
                return "input type=\"text\"";
        }                
    }
    
    protected String getBodyValue() throws JspException {
        Map optionsMap = null;
        switch(fieldTypeValue) {
            case FIELD_TYPE_SELECT:
                return getOptionsString(retrieveOptionsMap()) + "</select>";
            case FIELD_TYPE_YES_NO:
                return getOptionsString(OPTION_MAP_YES_NO) + "</select>";
            case FIELD_TYPE_ON_OFF:
                return getOptionsString(OPTION_MAP_ON_OFF) + "</select>";
            case FIELD_TYPE_RANGE:
                return createRangeOptions(minimumValue, maximumValue) + "</select>";
            default:
                return "";
        }                
    }

    protected boolean dataEquals(Object o) throws JspException {
        if(o == null) return dataValue == null;
        else if(dataValue == null) return false;
        else return o.equals(dataValue);
    }

    protected Map retrieveOptionsMap() throws JspException {
        OptionsFactory of = (OptionsFactory)lookup(null,options);
        if(of == null) throw new JspException("Could not find options factory, '" + options + "' in the request");
        return of.getOptions(fieldIdValue);
    }
    
    protected String getOptionsString(Map optionsMap) throws JspException {   
        if(optionsMap == null) return "";
        String s = "";
        int i = 1;
        for(Iterator iter = optionsMap.entrySet().iterator(); iter.hasNext(); i++) {
            Map.Entry e = (Map.Entry) iter.next();
            Object key = e.getKey();
            Object value = e.getValue();
            /*if(key == null && value == null) { key = "" + i; value = key; }
            else if(key == null) key = value;
            else if(value == null) value = key;
             **/
            s += "<option value=\"" + StringUtils.prepareCDATA(key == null ? "" : key.toString()) + "\"" 
                + (dataEquals(key) ? " selected" : "") +">" 
                + StringUtils.prepareHTML(value == null ? "<NULL>" : value.toString()) + "</option>\n";
        }
        return s;
    }
    
    protected String createRangeOptions(int min, int max) throws JspException {
        String s = "";
        int v = -1;
        if(dataValue != null) try {
            v = ((Number)ConvertUtils.convert(Integer.class, dataValue)).intValue();
        } catch(ConvertException e) {}
        for(int i = min; i <= max; i++) s += "<option value=\"" + i + "\"" + 
            (i == v ? " selected" : "") +">" + i + "</option>\n";
        return s;
    }
    
    protected String createHourOptions() throws JspException {
        String s = "";
        int v = -1;
        if(dataValue != null) try {
            java.util.Date date = ConvertUtils.convert(java.util.Date.class, dataValue);
            log.debug("Converted '" + dataValue + "' to date " + date);
            v = ConvertUtils.convert(java.util.Date.class, dataValue).getHours();
        } catch(ConvertException e) {
            log.warn("While converting to date", e);
        }
        else log.debug("Data Value is null");
        for(int i = 0; i < 10; i++) s += "<option value=\"0" + i + "\"" + 
            (i == v ? " selected" : "") +">0" + i + "</option>\n";
        for(int i = 10; i < 24; i++) s += "<option value=\"" + i + "\"" + 
            (i == v ? " selected" : "") +">" + i + "</option>\n";       
        return s;
    }

    protected String createMinuteOptions() throws JspException {
        String s = "";
        int v = -1;
        if(dataValue != null) try {
            v = ConvertUtils.convert(java.util.Date.class, dataValue).getMinutes();
        } catch(ConvertException e) {}
        for(int i = 0; i < 10; i++) s += "<option value=\"0" + i + "\"" + 
            (i == v ? " selected" : "") +">0" + i + "</option>\n";
        for(int i = 10; i < 60; i++) s += "<option value=\"" + i + "\"" + 
            (i == v ? " selected" : "") +">" + i + "</option>\n";       
        return s;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        bean = null;
        data = "value";
        fieldId = "fieldId";
        fieldType = "fieldType";
        format = "format";
        maximum = "maximum";
        minimum = "minimum";
        prefix = "field_";
        required = "required";
        scope = null;
        options = "selections";
        description = "label";
        width = "width";
        
        dataValue = null;
        descriptionValue = null;
        fieldIdValue = null;
        formatValue = null;    
    }

    /** Getter for property fieldType.
     * @return Value of property fieldType.
     */
    public String getFieldType() {
        return fieldType;
    }
    
    /** Setter for property fieldType.
     * @param fieldType New value of property fieldType.
     */
    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }
    
    /** Getter for property maximum.
     * @return Value of property maximum.
     */
    public String getMaximum() {
        return maximum;
    }
    
    /** Setter for property maximum.
     * @param maximum New value of property maximum.
     */
    public void setMaximum(String maximum) {
        this.maximum = maximum;
    }
    
    /** Getter for property minimum.
     * @return Value of property minimum.
     */
    public String getMinimum() {
        return minimum;
    }
    
    /** Setter for property minimum.
     * @param minimum New value of property minimum.
     */
    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }
    
    /** Getter for property required.
     * @return Value of property required.
     */
    public String getRequired() {
        return required;
    }
    
    /** Setter for property required.
     * @param required New value of property required.
     */
    public void setRequired(String required) {
        this.required = required;
    }
    
    /** Getter for property fieldId.
     * @return Value of property fieldId.
     */
    public String getFieldId() {
        return fieldId;
    }
    
    /** Setter for property fieldId.
     * @param fieldId New value of property fieldId.
     */
    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }
    
    /** Getter for property prefix.
     * @return Value of property prefix.
     */
    public String getPrefix() {
        return prefix;
    }
    
    /** Setter for property prefix.
     * @param prefix New value of property prefix.
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
    /** Getter for property options.
     * @return Value of property options.
     */
    public String getOptions() {
        return options;
    }
    
    /** Setter for property options.
     * @param options New value of property options.
     */
    public void setOptions(String options) {
        this.options = options;
    }

    /** Getter for property data.
     * @return Value of property data.
     */
    public String getData() {
        return data;
    }
    
    /** Setter for property data.
     * @param data New value of property data.
     */
    public void setData(String data) {
        this.data = data;
    }
    
    /** Getter for property bean.
     * @return Value of property bean.
     */
    public String getBean() {
        return bean;
    }
    
    /** Setter for property bean.
     * @param bean New value of property bean.
     */
    public void setBean(String bean) {
        this.bean = bean;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     */
    public String getScope() {
        return scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /** Getter for property format.
     * @return Value of property format.
     */
    public String getFormat() {
        return format;
    }
    
    /** Setter for property format.
     * @param format New value of property format.
     */
    public void setFormat(String format) {
        this.format = format;
    }
    
    /** Getter for property description.
     * @return Value of property description.
     */
    public String getDescription() {
        return description;
    }
    
    /** Setter for property description.
     * @param description New value of property description.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /** Getter for property width.
     * @return Value of property width.
     */
    public String getWidth() {
        return width;
    }
    
    /** Setter for property width.
     * @param width New value of property width.
     */
    public void setWidth(String width) {
        this.width = width;
    }
    
    /** Getter for property styleId.
     * @return Value of property styleId.
     */
    public String getStyleId() {
        return this.styleId;
    }
    
    /** Setter for property styleId.
     * @param styleId New value of property styleId.
     */
    public void setStyleId(String styleId) {
        this.styleId = styleId;
    }
    
    /** Getter for property style.
     * @return Value of property style.
     */
    public String getStyle() {
        return this.style;
    }
    
    /** Setter for property style.
     * @param style New value of property style.
     */
    public void setStyle(String style) {
        this.style = style;
    }
    
}
