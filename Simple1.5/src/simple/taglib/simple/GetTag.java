/*
 * GetTag.java
 *
 * Created on December 12, 2002, 1:19 PM
 */

package simple.taglib.simple;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;

import simple.bean.ConvertUtils;
import simple.servlet.RequestUtils;
import simple.servlet.ResponseUtils;
import simple.taglib.SimpleTagSupport;
import simple.text.StringUtils;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class GetTag extends SimpleTagSupport {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Holds value of property name. */
    private String name;
    
    /** Holds value of property required. */
    private boolean required = false;
    
    /** Holds value of property defaultValue. */
    private String defaultValue;
    
    /** Holds value of property prepare. */
    private String prepare = "HTML";
    
    /** Holds value of property format. */
    private String format;
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Creates new GetTag */
    public GetTag() {
    }
    
    /**
     * Render the specified error messages if there are any.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
        // get the value
        log.debug("Evaluating GetTag with name=" + name);
        Object value;
        try {
            value = RequestUtils.getAttribute(pageContext, name, false, scope);
        } catch(ServletException se) {
            throw new JspException(se.getMessage());
        }
        if(value == null) {
            if(required) throw new JspException("Could not find required attribute '" + name + "'");
            value = defaultValue;
        }
        log.debug("Value='" + value + "'; format='" + format + "'");
        String s = StringUtils.prepareString(ConvertUtils.formatObject(value, format), prepare);
        if (s != null && s.length() > 0) ResponseUtils.write(pageContext, s);
          
        return (EVAL_BODY_INCLUDE); // Continue processing this page
    }
        
   /** Getter for property name.
     * @return Value of property name.
     */
    public String getName() {
        return name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /** Getter for property required.
     * @return Value of property required.
     */
    public boolean isRequired() {
        return required;
    }
    
    /** Setter for property required.
     * @param required New value of property required.
     */
    public void setRequired(boolean required) {
        this.required = required;
    }
    
    /** Getter for property defaultValue.
     * @return Value of property defaultValue.
     */
    public String getDefaultValue() {
        return defaultValue;
    }
    
    /** Setter for property defaultValue.
     * @param defaultValue New value of property defaultValue.
     */
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    /** Getter for property prepare.
     * @return Value of property prepare.
     */
    public String getPrepare() {
        return prepare;
    }
    
    /** Setter for property prepare.
     * @param prepare New value of property prepare.
     */
    public void setPrepare(String prepare) {
        this.prepare = prepare;
    }
    
    /** Getter for property format.
     * @return Value of property format.
     */
    public String getFormat() {
        return format;
    }
    
    /** Setter for property format.
     * @param format New value of property format.
     */
    public void setFormat(String format) {
        this.format = format;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        name = null;
        format = null;
        prepare = "HTML";
        required = false;
        defaultValue = null;
        scope = null;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
}
