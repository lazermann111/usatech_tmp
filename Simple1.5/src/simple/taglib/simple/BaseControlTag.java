/*
 * BaseFieldTag.java
 */


package simple.taglib.simple;


import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;

import simple.bean.ConvertUtils;
import simple.servlet.RequestUtils;
import simple.servlet.ResponseUtils;
import simple.text.StringUtils;

/**
 * Convenience base class for the various input tags for text fields.
 *
 * @author BSK
 */

public abstract class BaseControlTag extends BaseHandlerTag {


    // ----------------------------------------------------- Instance Variables



    /**
     * The name of the bean containing our underlying property.
     */
    protected String name = null;

    public String getName() {
        return (this.name);
    }

    public void setName(String name) {
        this.name = name;
    }



    protected String size = null;



    /**
     * The name of the field (and associated property) being processed.
     */
    protected String property = null;



    /**
     * The value for this field, or <code>null</code> to retrieve the
     * corresponding property from our associated bean.
     */
    protected String value = null;

    /** Holds value of property format. */
    private String format;    

    /** Holds value of property bean. */
    private String bean;
    
    /** Holds value of property scope. */
    private String scope;
    
    // ------------------------------------------------------------- Properties






    /**
     * Return the property name.
     */
    public String getProperty() {

	return (this.property);

    }


    /**
     * Set the property name.
     *
     * @param property The new property name
     */
    public void setProperty(String property) {

	this.property = property;

    }




    /**
     * Return the size of this field (synonym for <code>getCols()</code>).
     */
    public String getSize() {
	return size;

    }


    /**
     * Set the size of this field (synonym for <code>setCols()</code>).
     *
     * @param size The new size
     */
    public void setSize(String size) {
        this.size = size;
    }


    /**
     * Return the field value (if any).
     */
    public String getValue() {
	return (this.value);

    }


    /**
     * Set the field value (if any).
     *
     * @param value The new field value, or <code>null</code> to retrieve the
     *  corresponding property from the bean
     */
    public void setValue(String value) {

	this.value = value;

    }

    
    public int doStartTag() throws JspException {
        StringBuilder results = new StringBuilder();
        prepareTag(results);
        prepareControl(results);
        prepareEventHandlers(results);
        prepareStyles(results);
        results.append(">");

        // Print this field to our output writer
        ResponseUtils.write(pageContext, results.toString());

        // Continue processing this page
        return EVAL_BODY_BUFFERED;

    }

    protected void prepareControl(StringBuilder results) throws JspException {
        if(name != null) {
            results.append(" name=\"");
            results.append(StringUtils.prepareCDATA(name));
            results.append("\"");
        }
        if (size != null) {
            results.append(" size=\"");
            results.append(StringUtils.prepareCDATA(size));
            results.append("\"");
        }
    }

    protected abstract void prepareTag(StringBuilder results) throws JspException ;

    protected String getValueString() throws JspException {
        String vs = null;
        Object tmp;
        if (value != null) vs = value;
        else if((tmp=getRequestValue()) != null) 
            vs = ConvertUtils.formatObject(tmp, format);
        else vs = null;
        return vs;
    }
    
    protected Object getRequestValue() throws JspException {
        String tmp = (property != null ? property : name); //use property if not null or name if property is null
        if(bean != null) tmp = bean + (tmp == null ? "" : "." + tmp);
        if(tmp != null) {
            try {
                Object rv = RequestUtils.getAttribute(pageContext,tmp,false,scope);
                return rv;
            } catch(ServletException se) {
                throw new JspException(RequestUtils.exceptionToString(se));
            }
        }
        return null;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        name = null;
        property = null;
	size = null;
	value = null;
        bean = null;
        format = null;
        scope = null;
    }

    /** Getter for property format.
     * @return Value of property format.
     */
    public String getFormat() {
        return format;
    }    

    /** Setter for property format.
     * @param format New value of property format.
     */
    public void setFormat(String format) {
        this.format = format;
    }
    
    /** Getter for property bean.
     * @return Value of property bean.
     */
    public String getBean() {
        return bean;
    }
    
    /** Setter for property bean.
     * @param bean New value of property bean.
     */
    public void setBean(String bean) {
        this.bean = bean;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     */
    public String getScope() {
        return scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
}
