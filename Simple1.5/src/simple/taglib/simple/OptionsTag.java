/*
 * OptionTag.java
 */


package simple.taglib.simple;


import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.results.Results;
import simple.results.ResultsIterator;
import simple.servlet.RequestUtils;
import simple.servlet.ResponseUtils;
import simple.taglib.SimpleTagSupport;
import simple.text.StringUtils;
import simple.util.PrimitiveArrayList;

/**
 * Select option tag.
 *
 * @author BSK
 */

public class OptionsTag extends SimpleTagSupport {    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected SelectTag selectTag;
    protected int lengthCount = 0;
    protected int lengthValue = 0;
    protected int offsetValue = 0;
    protected String id = null;
    private String label;
    protected Iterator iterator;
    
    /**
     * Construct an iterator for the specified collection, and begin
     * looping through the body once per element.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
	// Acquire the object we are going to iterate over
        Object o;
        try {
            o = RequestUtils.getAttribute(pageContext, (name == null ? id : name), true, scope);
        } catch(ServletException se) {
            throw new JspException(se.getMessage(), se);
        }

	// Calculate the starting offset
	offsetValue = getIntAttribute(offset, 0);

	// Calculate the rendering length
	lengthValue = getIntAttribute(length, 0);
	if(lengthValue < 0) lengthValue = 0;
	lengthCount = 0;
           
        iterator = getIterator(o);        
        if(id != null && id.trim().length() > 0) pageContext.setAttribute(id, iterator);
        loop();
        return EVAL_BODY_INCLUDE;
    }

    protected Iterator getIterator(Object o) {
        //turn object into iterator and skip the leading elements up to the starting offset
        //make it a collection first
        Collection coll;
        if(o instanceof Collection) {
            coll = (Collection) o;
        } else if(o instanceof Map) {
            coll = ((Map)o).entrySet();
        } else if(o instanceof Object[]) {
            coll = Arrays.asList((Object[])o);
        } else if(o != null && o.getClass().isArray()) {
            coll = new PrimitiveArrayList(o);
        } else if(o instanceof Results) {
            Results results = (Results)o;
            if(offsetValue < 0) offsetValue--; // must set row to row before the next desired row
            results.setRow(offsetValue); // allow negatives as positions from the end
            if(offsetValue < 0) offsetValue = results.getRow();         
            return new ResultsIterator(results.clone(), offsetValue);
        } else {
            coll = Collections.singletonList(o);
        }        
        
        //now make it an iterator        
        if(offsetValue < 0) offsetValue = Math.max(0, coll.size() + offsetValue);
        if(coll instanceof List) {
            return ((List)coll).listIterator(offsetValue);
        } else {
            Iterator iterator = coll.iterator();
            for(int i = 0; i < offsetValue && iterator.hasNext(); i++) iterator.next();  
            return iterator;
        }       
    }

    protected int getIntAttribute(String attr, int defaultValue) {
	if(attr == null)
	    return defaultValue;
	else {
	    try {
		return Integer.parseInt(attr);
	    } catch (NumberFormatException e) {
                try {
                    Integer i = ConvertUtils.convert(Integer.class, pageContext.findAttribute(attr));
                    return i == null ? defaultValue : i.intValue();
                } catch(simple.bean.ConvertException ce) {
                    return defaultValue;
                }
	    }
	}
    }
    
    protected void writeOption() throws JspException {
        Object o = iterator.next();
        Object keyValue = null;
        if(key == null) {
            if(o instanceof Map.Entry) keyValue = ((Map.Entry)o).getKey();
            else keyValue = o;
        } else if(o instanceof Map) {
            keyValue = ((Map)o).get(key);
        } else if(o != null) {
            try {
                keyValue = ReflectionUtils.getProperty(o,key);
            } catch(IntrospectionException e) {
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            } catch(ParseException e) {
			}
        }
        Object labelValue = null;
        if(label == null) {
            if(o instanceof Map.Entry) labelValue = ((Map.Entry)o).getValue();
            else labelValue = o;
        } else if(o instanceof Map) {
            labelValue = ((Map)o).get(label);
        } else if(o != null) {
            try {
                labelValue = ReflectionUtils.getProperty(o,label);
            } catch(IntrospectionException e) {
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            } catch(ParseException e) {
			}
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append("<option value=\"");
	sb.append(StringUtils.prepareCDATA(ConvertUtils.formatObject(keyValue, keyFormat)));
	sb.append("\"");
        SelectTag st = getSelectTag(); 
        if(st != null && st.isMatch(keyValue)) sb.append(" selected=\"selected\"");
        sb.append(">");
        sb.append(StringUtils.prepareHTML(ConvertUtils.formatObject(labelValue, labelFormat)));
        sb.append("</option>");
        ResponseUtils.write(pageContext, sb.toString());        
    }
    
    /**
     * Goto next row and loop, or
     * finish if there are no more rows.
     *
     * @exception JspException if a JSP exception has occurred
     */
    protected void loop() throws JspException {
        // Decide whether to iterate or quit
        while((lengthValue <= 0 || lengthCount < lengthValue) && iterator.hasNext()) {
            lengthCount++;
            writeOption();
        }
    }
    
    public int doEndTag() throws JspException {
        return (EVAL_PAGE);
    }

    protected SelectTag getSelectTag() {
        if(selectTag == null) selectTag = findSelectTag();
        return selectTag;
    }
    
    protected SelectTag findSelectTag() {
        for(Tag tag = getParent(); tag != null; tag = tag.getParent()) {
            if(tag instanceof SelectTag) return (SelectTag)tag;
        }
        return null;   
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        label = null;
        selectTag = null;
        
	iterator = null;
	lengthCount = 0;
	lengthValue = 0;
	offsetValue = 0;

        id = null;
        length = null;
        name = null;
        offset = null;
        scope = null;
     }

    /** Getter for property label.
     * @return Value of property label.
     */
    public String getLabel() {
        return label;
    }
    
    /** Setter for property label.
     * @param label New value of property label.
     */
    public void setLabel(String label) {
        this.label = label;
    }
    

    public String getId() {
	return (this.id);
    }

    public void setId(String id) {
	this.id = id;
    }


    /**
     * <p>Return the zero-relative index of the current iteration through the
     * loop.  If you specify an <code>offset</code>, the first iteration
     * through the loop will have that value; otherwise, the first iteration
     * will return zero.</p>
     *
     * <p>This property is read-only, and gives nested custom tags access to
     * this information.  Therefore, it is <strong>only</strong> valid in
     * between calls to <code>doStartTag()</code> and <code>doEndTag()</code>.
     * </p>
     */
    public int getIndex() {
        return (offsetValue + lengthCount - 1);
   }



    /**
     * The length value or attribute name (<=0 means no limit).
     */
    protected String length = null;

    public String getLength() {
	return (this.length);
    }

    public void setLength(String length) {
	this.length = length;
    }


    /**
     * The name of the results.
     */
    protected String name = null;

    public String getName() {
        return (this.name);
    }

    public void setName(String name) {
	this.name = name;
    }


    /**
     * The starting offset (zero relative).
     */
    protected String offset = null;

    public String getOffset() {
	return (this.offset);
    }

    public void setOffset(String offset) {
	this.offset = offset;
    }

    /** Holds value of property key. */
    private String key;
    
    /** Holds value of property keyFormat. */
    private String keyFormat;
    
    /** Holds value of property labelFormat. */
    private String labelFormat;
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Getter for property key.
     * @return Value of property key.
     */
    public String getKey() {
        return key;
    }
    
    /** Setter for property key.
     * @param key New value of property key.
     */
    public void setKey(String key) {
        this.key = key;
    }
    
    /** Getter for property keyFormat.
     * @return Value of property keyFormat.
     */
    public String getKeyFormat() {
        return keyFormat;
    }
    
    /** Setter for property keyFormat.
     * @param keyFormat New value of property keyFormat.
     */
    public void setKeyFormat(String keyFormat) {
        this.keyFormat = keyFormat;
    }
    
    /** Getter for property labelFormat.
     * @return Value of property labelFormat.
     */
    public String getLabelFormat() {
        return labelFormat;
    }
    
    /** Setter for property labelFormat.
     * @param labelFormat New value of property labelFormat.
     */
    public void setLabelFormat(String labelFormat) {
        this.labelFormat = labelFormat;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
}
