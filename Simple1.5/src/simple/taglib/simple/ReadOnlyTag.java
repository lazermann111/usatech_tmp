/*
 * ReadOnlyTag.java
 *
 * Created on February 20, 2003, 11:30 AM
 */

package simple.taglib.simple;

import javax.servlet.jsp.JspException;

import simple.servlet.ResponseUtils;
import simple.text.StringUtils;

/**
 *
 * @author  Brian S. Krug
 */
public class ReadOnlyTag extends BaseControlTag {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	int sizeValue = 0;
    
    /** Holds value of property style. */
    private String style;
    
    /** Creates a new instance of ReadOnlyTag */
    public ReadOnlyTag() {
    }
    
    protected void prepareTag(StringBuilder results) throws JspException {}
    
    public int doStartTag() throws JspException {
        StringBuilder results = new StringBuilder();
        results.append("<span style=\"border: 2px inset white;");
        if(getSizeValue() > 0) results.append(" width: " + getSizeValue() + "ex;");        
        if(style != null) results.append(" " + style);
        results.append("\"");
        prepareEventHandlers(results);
        prepareStyles(results);
        results.append(">")
               .append(StringUtils.prepareHTML(getValueString()))
               .append("</span>");
               
        ResponseUtils.write(pageContext, results.toString());
        return EVAL_BODY_BUFFERED;
    }
    
    protected int getSizeValue() {
        if(size == null || size.trim().length() == 0) return 0;
        try { return Integer.parseInt(size); } catch(NumberFormatException nfe) {}
        return 0;            
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        sizeValue = 0;
    }

    /** Getter for property style.
     * @return Value of property style.
     */
    public String getStyle() {
        return this.style;
    }
    
    /** Setter for property style.
     * @param style New value of property style.
     */
    public void setStyle(String style) {
        this.style = style;
    }
    
}
