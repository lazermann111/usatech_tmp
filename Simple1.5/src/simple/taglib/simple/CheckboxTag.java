/*
 * CheckboxTag.java
 */

package simple.taglib.simple;

import javax.servlet.jsp.JspException;

import simple.text.StringUtils;

/**
 * Custom tag for input fields of type "checkbox".
 *
 * @author BSK
 */
public class CheckboxTag extends BaseInputTag {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** Holds value of property required. */
    private boolean required;
    
    /**
     * Construct a new instance of this tag.
     */
    public CheckboxTag() {
	super();
    }
    
    /** The type of input field represented by this tag (text, password, or
     * hidden).
     */
    protected String getType() {
        return "checkbox";
    }

    protected void prepareValue(StringBuilder results) throws JspException {
        if(value != null) {
            results.append(" value=\"");
            results.append(StringUtils.prepareCDATA(value));
            results.append("\"");
            Object rv = getRequestValue();
            if(rv != null && value.equals(rv)) {
                results.append(" checked");
            }
        }
    }
    
    /** Getter for property required.
     * @return Value of property required.
     */
    public boolean isRequired() {
        return required;
    }
    
    /** Setter for property required.
     * @param required New value of property required.
     */
    public void setRequired(boolean required) {
        this.required = required;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        required = false;
    }
}