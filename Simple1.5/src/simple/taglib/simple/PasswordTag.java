/*
 * PasswordTag.java
 */

package simple.taglib.simple;


/**
 * Custom tag for input fields of type "password".
 *
 * @author BSK
 */
public class PasswordTag extends BaseInputTag {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Construct a new instance of this tag.
     */
    public PasswordTag() {
	super();
    }
    
    /** The type of input field represented by this tag (text, password, or
     * hidden).
     */
    protected String getType() {
        return "password";
    }
    
}