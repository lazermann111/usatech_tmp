/*
 * OptionsFactory.java
 *
 * Created on February 26, 2003, 9:48 AM
 */

package simple.taglib.simple;

import java.util.Map;

/**
 *
 * @author  Brian S. Krug
 */
public interface OptionsFactory {
    public Map getOptions(String fieldId) ;
}
