/*
 */
package simple.taglib.simple;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;

/**
 * Implementation of <code>TagExtraInfo</code> for the <b>each</b>
 * tag, identifying the scripting object(s) to be made visible.
 *
 */
public class EachTei extends TagExtraInfo {    
    /**
     * Return information about the scripting variables to be created.
     */
    public VariableInfo[] getVariableInfo(TagData data) {
        List variables = new ArrayList();        
        /* id : object of the current iteration */
        String id = data.getAttributeString("id");
        if (id != null) variables.add(new VariableInfo(id, "java.lang.Object", true,VariableInfo.NESTED));
        
        /* indexId : number value of the current iteration */
        String indexId = data.getAttributeString("indexId");
        if (indexId != null) variables.add(new VariableInfo(indexId, "java.lang.Integer", true, VariableInfo.NESTED));
        
        /* create returning array, and copy results */
        return (VariableInfo[])variables.toArray(new VariableInfo[variables.size()]);       
    }
}
