/*
 * PrivTag.java
 *
 * Created on January 9, 2003, 5:30 PM
 */

package simple.taglib.simple;

import javax.servlet.jsp.JspException;

import simple.servlet.RequestUtils;
import simple.servlet.ServletUser;
import simple.servlet.SimpleServlet;
import simple.taglib.SimpleTagSupport;
import simple.text.StringUtils;

/** Evaluates the body of this tag only if the user has the specified privilege(s)
 *
 * @author  Brian S. Krug
 */
public class PrivTag extends SimpleTagSupport {   
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** Holds value of property list. */
    private String list;
    
    /** Creates new IfTag */
    public PrivTag() {
    }

    public int doStartTag() throws JspException {
        if(condition()) {
            if(getParent() instanceof SwitchTag) {
                ((SwitchTag)getParent()).childEvaluated();
            }
            return EVAL_BODY_INCLUDE;
        } else return SKIP_BODY;
    }
    
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
    
    protected boolean condition() throws JspException {
        //get user
        ServletUser user = (ServletUser)RequestUtils.getPageContextAttribute(pageContext, SimpleServlet.ATTRIBUTE_USER, null);
        if(user == null) return false;
        //parse list into String array
        if(list == null || list.trim().length() == 0) return true;        
        String[] privs = StringUtils.split(list, StringUtils.STANDARD_DELIMS, false);
        //check each
        for(int i = 0; i < privs.length; i++) {
            if(!user.hasPrivilege(privs[i])) return false;
        }
        return true;
    }       

    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        list = null;
    }
    
    /** Getter for property list.
     * @return Value of property list.
     *
     */
    public String getList() {
        return this.list;
    }
    
    /** Setter for property list.
     * @param list New value of property list.
     *
     */
    public void setList(String list) {
        this.list = list;
    }
    
}
