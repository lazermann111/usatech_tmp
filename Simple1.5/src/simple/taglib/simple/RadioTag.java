/*
 * RadioTag.java
 */

package simple.taglib.simple;


/**
 * Custom tag for input fields of type "radio".
 *
 * @author BSK
 */
public class RadioTag extends CheckboxTag {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Construct a new instance of this tag.
     */
    public RadioTag() {
	super();
    }
    
    /** The type of input field represented by this tag (text, password, or
     * hidden).
     */
    protected String getType() {
        return "radio";
    }
    
}