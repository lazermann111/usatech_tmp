/*
 * BaseFieldTag.java
 */


package simple.taglib.simple;


import javax.servlet.jsp.JspException;

import simple.text.StringUtils;

/**
 * Convenience base class for the various input tags for text fields.
 *
 * @author BSK
 */

public abstract class BaseInputTag extends BaseControlTag {
    /**
     * The type of input field represented by this tag (text, password, or
     * hidden).
     */
    protected abstract String getType() ;
    
    /** Holds value of property maxlength. */
    private String maxlength;
    
    /** Holds value of property accept. */
    private String accept;
    
    /** Holds value of property minlength. */
    private String minlength;
    
    /** Holds value of property mask. */
    private String mask;
    
    /** Holds value of property required. */
    private boolean required;
    
    protected void prepareTag(StringBuilder results) throws JspException {
        results.append("<input type=\"");
        results.append(getType());
        results.append("\"");
        prepareValue(results);
    }
    
    protected void prepareValue(StringBuilder results) throws JspException {
        String val = getValueString();
        if (val != null && val.length() > 0) {
            results.append(" value=\"");
            results.append(StringUtils.prepareCDATA(val));
            results.append("\"");
        }
        if (accept != null) {
            results.append(" accept=\"");
            results.append(accept);
            results.append("\"");
        }
        if (maxlength != null) {
            results.append(" maxlength=\"");
            results.append(maxlength);
            results.append("\"");
        }
        if (minlength != null) {
            results.append(" minlength=\"");
            results.append(minlength);
            results.append("\"");
        }
        if (mask != null) {
            results.append(" mask=\"");
            results.append(mask);
            results.append("\"");
        }
        if (required) {
            results.append(" required=\"true\"");
        }
    }

    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        maxlength = null;
        accept = null;
        minlength = null;
        mask = null;
    }

    /** Getter for property maxlength.
     * @return Value of property maxlength.
     */
    public String getMaxlength() {
        return maxlength;
    }
    
    /** Setter for property maxlength.
     * @param maxlength New value of property maxlength.
     */
    public void setMaxlength(String maxlength) {
        this.maxlength = maxlength;
    }
    
    /** Getter for property accept.
     * @return Value of property accept.
     */
    public String getAccept() {
        return accept;
    }
    
    /** Setter for property accept.
     * @param accept New value of property accept.
     */
    public void setAccept(String accept) {
        this.accept = accept;
    }
    
    /** Getter for property minlength.
     * @return Value of property minlength.
     */
    public String getMinlength() {
        return minlength;
    }
    
    /** Setter for property minlength.
     * @param minlength New value of property minlength.
     */
    public void setMinlength(String minlength) {
        this.minlength = minlength;
    }
    
    /** Getter for property mask.
     * @return Value of property mask.
     */
    public String getMask() {
        return mask;
    }
    
    /** Setter for property mask.
     * @param mask New value of property mask.
     */
    public void setMask(String mask) {
        this.mask = mask;
    }
    
    /** Getter for property required.
     * @return Value of property required.
     *
     */
    public boolean isRequired() {
        return this.required;
    }
    
    /** Setter for property required.
     * @param required New value of property required.
     *
     */
    public void setRequired(boolean required) {
        this.required = required;
    }
    
}
