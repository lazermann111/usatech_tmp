/*
 * OptionTag.java
 */


package simple.taglib.simple;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;

import simple.bean.ConvertUtils;
import simple.servlet.RequestUtils;
import simple.servlet.ResponseUtils;
import simple.text.StringUtils;

/**
 * Select option tag.
 *
 * @author BSK
 */

public class OptionTag extends simple.taglib.SimpleBodyTagSupport {    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected SelectTag selectTag;
    /** Holds value of property label. */
    private String label;
    
    /** Holds value of property value. */
    private String value;
    
    /** Holds value of property valueBean. */
    private String valueBean;
    
    /** Holds value of property labelBean. */
    private String labelBean;
    
    /** Holds value of property labelFormat. */
    private String labelFormat;
    
    /** Holds value of property valueFormat. */
    private String valueFormat;
    
    /** Holds value of property labelScope. */
    private String labelScope;
    
    /** Holds value of property valueScope. */
    private String valueScope;
    
    public int doStartTag() throws JspException {
        StringBuilder results = new StringBuilder();
        results.append("<option");
        if(value != null) {
            results.append(" value=\"");
            results.append(getValueString());
            results.append("\"");
        }
        SelectTag st = getSelectTag(); 
        if(st != null && st.isMatch(value)) results.append(" selected=\"selected\"");
        results.append(">");
        
        // Print this field to our output writer
        ResponseUtils.write(pageContext, results.toString());

        // Continue processing this page
        return EVAL_BODY_BUFFERED;
    }
    
    public int doEndTag() throws JspException {
        ResponseUtils.write(pageContext, StringUtils.prepareHTML(getLabelString()));
        ResponseUtils.write(pageContext, "</option>");
        return (EVAL_PAGE);
    }

    protected SelectTag getSelectTag() {
        if(selectTag == null) selectTag = findSelectTag();
        return selectTag;
    }
    
    protected SelectTag findSelectTag() {
        for(Tag tag = getParent(); tag != null; tag = tag.getParent()) {
            if(tag instanceof SelectTag) return (SelectTag)tag;
        }
        return null;   
    }
    
    protected String getValueString() throws JspException {
        if (value != null || valueBean == null) return value;
        Object tmp = null;
        try {
            tmp = RequestUtils.getAttribute(pageContext,valueBean,false,valueScope);
        } catch(ServletException se) {
            throw new JspException(RequestUtils.exceptionToString(se));
        }
        if(tmp == null) return null;
        else return ConvertUtils.formatObject(tmp, valueFormat);
    }
    
    protected String getLabelString() throws JspException {
        if (label != null || labelBean == null) return label;
        Object tmp = null;
        try {
            tmp = RequestUtils.getAttribute(pageContext,labelBean,false,labelScope);
        } catch(ServletException se) {
            throw new JspException(RequestUtils.exceptionToString(se));
        }
        if(tmp == null) return null;
        else return ConvertUtils.formatObject(tmp, labelFormat);
    }

    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        label = null;
        selectTag = null;
        valueBean = null;
        labelBean = null;
        labelFormat = null;
        valueFormat = null;
        labelScope = null;
        valueScope = null;
    }

    /** Getter for property label.
     * @return Value of property label.
     */
    public String getLabel() {
        return label;
    }
    
    /** Setter for property label.
     * @param label New value of property label.
     */
    public void setLabel(String label) {
        this.label = label;
    }
    
    /** Getter for property valueBean.
     * @return Value of property valueBean.
     *
     */
    public String getValueBean() {
        return this.valueBean;
    }
    
    /** Setter for property valueBean.
     * @param valueBean New value of property valueBean.
     *
     */
    public void setValueBean(String valueBean) {
        this.valueBean = valueBean;
    }
    
    /** Getter for property labelBean.
     * @return Value of property labelBean.
     *
     */
    public String getLabelBean() {
        return this.labelBean;
    }
    
    /** Setter for property labelBean.
     * @param labelBean New value of property labelBean.
     *
     */
    public void setLabelBean(String labelBean) {
        this.labelBean = labelBean;
    }
    
    /** Getter for property labelFormat.
     * @return Value of property labelFormat.
     *
     */
    public String getLabelFormat() {
        return this.labelFormat;
    }
    
    /** Setter for property labelFormat.
     * @param labelFormat New value of property labelFormat.
     *
     */
    public void setLabelFormat(String labelFormat) {
        this.labelFormat = labelFormat;
    }
    
    /** Getter for property valueFormat.
     * @return Value of property valueFormat.
     *
     */
    public String getValueFormat() {
        return this.valueFormat;
    }
    
    /** Setter for property valueFormat.
     * @param valueFormat New value of property valueFormat.
     *
     */
    public void setValueFormat(String valueFormat) {
        this.valueFormat = valueFormat;
    }
    
    /** Getter for property value.
     * @return Value of property value.
     *
     */
    public String getValue() {
        return this.value;
    }
    
    /** Setter for property value.
     * @param value New value of property value.
     *
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    /** Getter for property valueScope.
     * @return Value of property valueScope.
     *
     *
     */
    public String getValueScope() {
        return this.valueScope;
    }
    
    /** Getter for property labelScope.
     * @return Value of property labelScope.
     *
     *
     */
    public String getLabelScope() {
        return this.labelScope;
    }
    
    /** Setter for property labelScope.
     * @param labelScope New value of property labelScope.
     *
     */
    public void setLabelScope(String labelScope) {}
    
    /** Setter for property valueScope.
     * @param valueScope New value of property valueScope.
     *
     */
    public void setValueScope(String valueScope) {}
    
}
