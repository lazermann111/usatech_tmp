/*
 */


package simple.taglib.simple;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;

import simple.bean.ConvertUtils;
import simple.results.Results;
import simple.results.ResultsIterator;
import simple.servlet.RequestUtils;
import simple.servlet.ResponseUtils;
import simple.taglib.SimpleBodyTagSupport;
import simple.util.PrimitiveArrayList;

/**
 */

public class EachTag extends SimpleBodyTagSupport {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Iterator iterator = null;
    protected int lengthCount = 0;
    protected int lengthValue = 0;
    protected int offsetValue = 0;
    /**
     * The name of the scripting variable to be exposed.
     */
    protected String id = null;

    public String getId() {
	return (this.id);
    }

    public void setId(String id) {
	this.id = id;
    }


    /**
     * <p>Return the zero-relative index of the current iteration through the
     * loop.  If you specify an <code>offset</code>, the first iteration
     * through the loop will have that value; otherwise, the first iteration
     * will return zero.</p>
     *
     * <p>This property is read-only, and gives nested custom tags access to
     * this information.  Therefore, it is <strong>only</strong> valid in
     * between calls to <code>doStartTag()</code> and <code>doEndTag()</code>.
     * </p>
     */
    public int getIndex() {
        return (offsetValue + lengthCount - 1);
   }


    /**
     * The name of the scripting variable to be exposed as the current index.
     */
    protected String indexId = null;

    public String getIndexId() {
	return (this.indexId);
    }

    public void setIndexId(String indexId) {
	this.indexId = indexId;
    }


    /**
     * The length value or attribute name (<=0 means no limit).
     */
    protected String length = null;

    public String getLength() {
	return (this.length);
    }

    public void setLength(String length) {
	this.length = length;
    }


    /**
     * The name attribute under which the object to iterator over is stored.
     */
    protected String name = null;

    public String getName() {
        return (this.name);
    }

    public void setName(String name) {
	this.name = name;
    }


    /**
     * The starting offset (zero relative).
     */
    protected String offset = null;

    public String getOffset() {
	return (this.offset);
    }

    public void setOffset(String offset) {
	this.offset = offset;
    }

    /**
     * The scope of the bean specified by the name, if any.
     */
    protected String scope = null;

    /** Holds value of property countId. */
    private String countId;
    
    public String getScope() {
        return (this.scope);
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * Construct an iterator for the specified collection, and begin
     * looping through the body once per element.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
	// Acquire the object we are going to iterate over
        Object o;
        try {
            o = RequestUtils.getAttribute(pageContext, name, true, scope);
        } catch(ServletException se) {
            throw new JspException(se.getMessage(), se);
        }

	// Calculate the starting offset
	offsetValue = getIntAttribute(offset, 0);

	// Calculate the rendering length
	lengthValue = getIntAttribute(length, 0);
	if(lengthValue < 0) lengthValue = 0;
	lengthCount = 0;
         
        iterator = getIterator(o);
                
        log.debug("Iterating over " + iterator + " starting at " + offsetValue + " and continuing " 
            + (lengthValue > 0 ? " for " + lengthValue + " elements" : " to the end"));
        return doNext();
    }

    protected Iterator getIterator(Object o) {
        //turn object into iterator and skip the leading elements up to the starting offset
        //make it a collection first
        Collection coll;
        if(o instanceof Collection) {
            coll = (Collection) o;
        } else if(o instanceof Map) {
            coll = ((Map)o).entrySet();
        } else if(o instanceof Object[]) {
            coll = Arrays.asList((Object[])o);
        } else if(o != null && o.getClass().isArray()) {
            coll = new PrimitiveArrayList(o);
        } else if(o instanceof Results) {
            Results results = (Results)o;
            if(offsetValue < 0) offsetValue--; // must set row to row before the next desired row
            results.setRow(offsetValue); // allow negatives as positions from the end
            if(offsetValue < 0) offsetValue = results.getRow();         
            return new ResultsIterator(results.clone(), offsetValue);
        } else {
            coll = Collections.singletonList(o);
        }        
        
        //now make it an iterator        
        if(offsetValue < 0) offsetValue = Math.max(0, coll.size() + offsetValue);
        if(coll instanceof List) {
            return ((List)coll).listIterator(offsetValue);
        } else {
            Iterator iterator = coll.iterator();
            for(int i = 0; i < offsetValue && iterator.hasNext(); i++) iterator.next();  
            return iterator;
        }       
    }
    
    protected int getIntAttribute(String attr, int defaultValue) {
	if(attr == null)
	    return defaultValue;
	else {
	    try {
		return Integer.parseInt(attr);
	    } catch (NumberFormatException e) {
                try {
                    Integer i = ConvertUtils.convert(Integer.class, pageContext.findAttribute(attr));
                    return i == null ? defaultValue : i.intValue();
                } catch(simple.bean.ConvertException ce) {
                    return defaultValue;
                }
	    }
	}
    }
    
    protected int doNext() {
    	// if not at end then eval body else skip body
	if(iterator.hasNext()) {
            pageContext.setAttribute(id, iterator.next());        
	    lengthCount++;
            if(indexId != null) pageContext.setAttribute(indexId, new Integer(getIndex()));
            return EVAL_BODY_AGAIN;
        } else {
            if(countId != null) pageContext.setAttribute(countId, new Integer(getIndex()+1));
            return SKIP_BODY;
        }
    
    }
    /**
     * Goto next row and loop, or
     * finish if there are no more rows.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException {
        // Render the output from this iteration to the output stream
        if (bodyContent != null) {
            ResponseUtils.writePrevious(pageContext, bodyContent.getString());
            bodyContent.clearBody();
        }

        // Decide whether to iterate or quit
	if(lengthValue > 0 && lengthCount >= lengthValue) return (SKIP_BODY);
        else return doNext();
    }


    /**
     * Clean up after processing this enumeration.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {
	// Continue processing this page
	return (EVAL_PAGE);
    }


    /**
     * Release all allocated resources.
     */
    public void release() {
	super.release();
	iterator = null;
	lengthCount = 0;
	lengthValue = 0;
	offsetValue = 0;

        id = null;
        length = null;
        name = null;
        offset = null;
        scope = null;
        indexId = null;
        countId = null;
    }
        
    /** Getter for property countId.
     * @return Value of property countId.
     *
     */
    public String getCountId() {
        return this.countId;
    }
    
    /** Setter for property countId.
     * @param countId New value of property countId.
     *
     */
    public void setCountId(String countId) {
        this.countId = countId;
    }
    
}