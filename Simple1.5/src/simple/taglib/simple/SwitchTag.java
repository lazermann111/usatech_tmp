/*
 * CompareTag.java
 *
 * Created on January 9, 2003, 5:30 PM
 */

package simple.taglib.simple;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;

import simple.servlet.RequestUtils;
import simple.taglib.SimpleTagSupport;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class SwitchTag extends SimpleTagSupport {    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Holds value of property bean. */
    private String bean;
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property property. */
    private String property;
    
    /** Holds value of property operator. */
    private String operator = simple.bean.ConditionUtils.OP_EQUAL;
    
    protected Object requestValue;
        
    /** Holds value of property childEvaluated. */
    private boolean childEvaluated = false;
    
    /** Creates new BaseConditionTag */
    public SwitchTag() {
    }

    public int doStartTag() throws JspException {
        String tmp = (bean != null ? bean : "") + 
            (bean != null && property != null ? "." : "") + 
            (property != null ? property : "");
        if(tmp != null) {
            try {
                requestValue = RequestUtils.getAttribute(pageContext,tmp,false,scope);
             } catch(ServletException se) {
                throw new JspException(RequestUtils.exceptionToString(se));
            }
        }
        return EVAL_BODY_INCLUDE;
    }
    
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
        
    public Object getRequestObject() throws JspException {
        return requestValue;
    }
    
    /** Getter for property bean.
     * @return Value of property bean.
     */
    public String getBean() {
        return this.bean;
    }
    
    /** Setter for property bean.
     * @param bean New value of property bean.
     */
    public void setBean(String bean) {
        this.bean = bean;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /** Getter for property property.
     * @return Value of property property.
     */
    public String getProperty() {
        return this.property;
    }
    
    /** Setter for property property.
     * @param property New value of property property.
     */
    public void setProperty(String property) {
        this.property = property;
    }
    
    /** Getter for property operator.
     * @return Value of property operator.
     */
    public String getOperator() {
        return this.operator;
    }
    
    /** Setter for property operator.
     * @param operator New value of property operator.
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        bean = null;
        operator = null;
        property = null;
        scope = null;
        requestValue = null;
        childEvaluated = false;
    }
    
    /** Getter for property childEvaluated.
     * @return Value of property childEvaluated.
     *
     */
    public boolean isChildEvaluated() {
        return this.childEvaluated;
    }
    
    /** Sets the childEvaluated property to true
     *
     */
    public void childEvaluated() {
        this.childEvaluated = true;
    }
    
}
