/*
 * CaseTag.java
 *
 * Created on November 24, 2003, 10:18 AM
 */

package simple.taglib.simple;

import javax.servlet.jsp.JspException;

import simple.taglib.SimpleBodyTagSupport;

/**
 *
 * @author  Brian S. Krug
 */
public class DefaultTag extends SimpleBodyTagSupport {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates a new instance of CaseTag */
    public DefaultTag() {
    }
    
    public int doStartTag() throws JspException {
        if(!getParentSwitch().isChildEvaluated()) return EVAL_BODY_INCLUDE;
        else return SKIP_BODY;
    }
        
    protected SwitchTag getParentSwitch() {
        return (SwitchTag) getParent();
    }    
}
