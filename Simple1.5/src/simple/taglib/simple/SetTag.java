/*
 * SetTag.java
 *
 * Created on January 3, 2005
 */

package simple.taglib.simple;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;

import simple.servlet.RequestUtils;
import simple.taglib.SimpleTagSupport;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class SetTag extends SimpleTagSupport {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * @return Returns the local.
     */
    public boolean isLocal() {
        return local;
    }
    /**
     * @param local The local to set.
     */
    public void setLocal(boolean local) {
        this.local = local;
    }
    /**
     * @return Returns the newName.
     */
    public String getNewName() {
        return newName;
    }
    /**
     * @param newName The newName to set.
     */
    public void setNewName(String newName) {
        this.newName = newName;
    }
    /**
     * @return Returns the newScope.
     */
    public String getNewScope() {
        return newScope;
    }
    /**
     * @param newScope The newScope to set.
     */
    public void setNewScope(String newScope) {
        this.newScope = newScope;
    }
    /** Holds value of property name. */
    private String name;
    
    /** Holds value of property newName. */
    private String newName;
    
    /** Holds value of property required. */
    private boolean required = false;
    
    /** Holds value of property defaultValue. */
    private String defaultValue;
        
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property scope. */
    private String newScope = "request";
    
    private boolean local = false;
    
    private Object original = null;
    
    /** Creates new SetTag */
    public SetTag() {
    }
    
    /**
     * Render the specified error messages if there are any.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
        // get the value
        log.debug("Evaluating SetTag with name=" + name);
        Object value;
        try {
            value = RequestUtils.getAttribute(pageContext, name, false, scope);
            if(local) original = RequestUtils.getAttribute(pageContext, newName, false, newScope);
	        if(value == null) {
	            if(required) throw new JspException("Could not find required attribute '" + name + "'");
	            value = defaultValue;
	        }
	        log.debug("Value='" + value + "'");
	        RequestUtils.setAttribute(pageContext, newName, value, newScope);  
	        return (EVAL_BODY_INCLUDE); // Continue processing this page
        } catch(ServletException se) {
            throw new JspException(se.getMessage(), se);
        } catch(IllegalArgumentException e) {
            throw new JspException("While retrieving attribute", e);
        }
    }
        
   /** Getter for property name.
     * @return Value of property name.
     */
    public String getName() {
        return name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /** Getter for property required.
     * @return Value of property required.
     */
    public boolean isRequired() {
        return required;
    }
    
    /** Setter for property required.
     * @param required New value of property required.
     */
    public void setRequired(boolean required) {
        this.required = required;
    }
    
    /** Getter for property defaultValue.
     * @return Value of property defaultValue.
     */
    public String getDefaultValue() {
        return defaultValue;
    }
    
    /** Setter for property defaultValue.
     * @param defaultValue New value of property defaultValue.
     */
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        name = null;
        scope = null;
        newName = null;
        newScope = "request";
        required = false;
        defaultValue = null;
        local = false;
        original = null;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.Tag#doEndTag()
     */
    public int doEndTag() throws JspException {
        if(local) {
            log.debug("Resetting value to '" + original + "'");
            RequestUtils.setAttribute(pageContext, newName, original, newScope);  
        }  
        return super.doEndTag();
    }
}
