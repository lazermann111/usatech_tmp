/*
 * BaseHandlerTag.java
 */

package simple.taglib.simple;

import simple.taglib.SimpleBodyTagSupport;
import simple.text.StringUtils;

/**
 * Base class for tags that render form elements capable of including JavaScript
 * event handlers and/or CSS Style attributes. This class does not implement
 * the doStartTag() or doEndTag() methods. Subclasses should provide
 * appropriate implementations of these.
 *
 * @author Don Clasen
 * @modified by BSK
 */

public abstract class BaseHandlerTag extends SimpleBodyTagSupport {
//  Navigation Management

    /** Access key character. */
    protected String accesskey= null;

    /** Tab index value. */
    protected String tabindex = null;

//  Mouse Events

    /** Mouse click event. */
    private String onclick = null;

    /** Mouse double click event. */
    private String ondblclick = null;

    /** Mouse over component event. */
    private String onmouseover = null;

    /** Mouse exit component event. */
    private String onmouseout = null;

    /** Mouse moved over component event. */
    private String onmousemove = null;

    /** Mouse pressed on component event. */
    private String onmousedown = null;

    /** Mouse released on component event. */
    private String onmouseup = null;

//  Keyboard Events

    /** Key down in component event. */
    private String onkeydown = null;

    /** Key released in component event. */
    private String onkeyup = null;

    /** Key down and up together in component event. */
    private String onkeypress = null;

// Text Events

    /** Text selected in component event. */
    private String onselect = null;

    /** Content changed after component lost focus event. */
    private String onchange = null;

// Focus Events and States

    /** Component lost focus event. */
    private String onblur = null;

    /** Component has received focus event. */
    private String onfocus = null;

    /** Component is disabled. */
    private boolean disabled = false;

    /** Component is readonly. */
    private boolean readonly = false;

// CSS Style Support

    /** Style attribute associated with component. */
    private String style = null;

    /** Named Style class associated with component. */
    private String styleClass = null;

    /** Identifier associated with component.  */
    private String styleId = null;

// Other Common Attributes

    /** The alternate text of this element. */
    private String alt = null;

    /** The advisory title of this element. */
    private String title = null;

    /** Holds value of property onpropertychange. */
    private String onpropertychange;    

    /** Holds value of property onvalidate. */
    private String onvalidate;
    
    /** Holds value of property description. */
    private String description;
    
    /** Holds value of property onpaste. */
    private String onpaste;
    
    // ------------------------------------------------------------- Properties

//  Navigation Management

    /** Sets the accessKey character. */
    public void setAccesskey(String accessKey) {
        this.accesskey = accessKey;
    }

    /** Returns the accessKey character. */
    public String getAccesskey() {
        return (this.accesskey);
    }


    /** Sets the tabIndex value. */
    public void setTabindex(String tabIndex) {
        this.tabindex = tabIndex;
    }

    /** Returns the tabIndex value. */
    public String getTabindex() {
        return (this.tabindex);
    }

//  Indexing ability for Iterate [since Struts 1.1]

// Mouse Events

    /** Sets the onClick event handler. */
    public void setOnclick(String onClick) {
        this.onclick = onClick;
    }

    /** Returns the onClick event handler. */
    public String getOnclick() {
        return onclick;
    }

    /** Sets the onDblClick event handler. */
    public void setOndblclick(String onDblClick) {
        this.ondblclick = onDblClick;
    }

    /** Returns the onDblClick event handler. */
    public String getOndblclick() {
        return ondblclick;
    }

    /** Sets the onMouseDown event handler. */
    public void setOnmousedown(String onMouseDown) {
        this.onmousedown = onMouseDown;
    }

    /** Returns the onMouseDown event handler. */
    public String getOnmousedown() {
        return onmousedown;
    }

    /** Sets the onMouseUp event handler. */
    public void setOnmouseup(String onMouseUp) {
        this.onmouseup = onMouseUp;
    }

    /** Returns the onMouseUp event handler. */
    public String getOnmouseup() {
        return onmouseup;
    }

    /** Sets the onMouseMove event handler. */
    public void setOnmousemove(String onMouseMove) {
        this.onmousemove = onMouseMove;
    }

    /** Returns the onMouseMove event handler. */
    public String getOnmousemove() {
        return onmousemove;
    }

    /** Sets the onMouseOver event handler. */
    public void setOnmouseover(String onMouseOver) {
        this.onmouseover = onMouseOver;
    }

    /** Returns the onMouseOver event handler. */
    public String getOnmouseover() {
        return onmouseover;
    }

    /** Sets the onMouseOut event handler. */
    public void setOnmouseout(String onMouseOut) {
        this.onmouseout = onMouseOut;
    }

    /** Returns the onMouseOut event handler. */
    public String getOnmouseout() {
        return onmouseout;
    }

// Keyboard Events

    /** Sets the onKeyDown event handler. */
    public void setOnkeydown(String onKeyDown) {
        this.onkeydown = onKeyDown;
    }

    /** Returns the onKeyDown event handler. */
    public String getOnkeydown() {
        return onkeydown;
    }

    /** Sets the onKeyUp event handler. */
    public void setOnkeyup(String onKeyUp) {
        this.onkeyup = onKeyUp;
    }

    /** Returns the onKeyUp event handler. */
    public String getOnkeyup() {
        return onkeyup;
    }

    /** Sets the onKeyPress event handler. */
    public void setOnkeypress(String onKeyPress) {
        this.onkeypress = onKeyPress;
    }

    /** Returns the onKeyPress event handler. */
    public String getOnkeypress() {
        return onkeypress;
    }

// Text Events

    /** Sets the onChange event handler. */
    public void setOnchange(String onChange) {
        this.onchange = onChange;
    }

    /** Returns the onChange event handler. */
    public String getOnchange() {
        return onchange;
    }

    /** Sets the onSelect event handler. */
    public void setOnselect(String onSelect) {
        this.onselect = onSelect;
    }

    /** Returns the onSelect event handler. */
    public String getOnselect() {
        return onselect;
    }

// Focus Events and States

    /** Sets the onBlur event handler. */
    public void setOnblur(String onBlur) {
        this.onblur = onBlur;
    }

    /** Returns the onBlur event handler. */
    public String getOnblur() {
        return onblur;
    }

    /** Sets the onFocus event handler. */
    public void setOnfocus(String onFocus) {
        this.onfocus = onFocus;
    }

    /** Returns the onFocus event handler. */
    public String getOnfocus() {
        return onfocus;
    }

    /** Sets the disabled event handler. */
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    /** Returns the disabled event handler. */
    public boolean getDisabled() {
        return disabled;
    }

    /** Sets the readonly event handler. */
    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    /** Returns the readonly event handler. */
    public boolean getReadonly() {
        return readonly;
    }

// CSS Style Support

    /** Sets the style attribute. */
    public void setStyle(String style) {
        this.style = style;
    }

    /** Returns the style attribute. */
    public String getStyle() {
        return style;
    }

    /** Sets the style class attribute. */
    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    /** Returns the style class attribute. */
    public String getStyleClass() {
        return styleClass;
    }

    /** Sets the style id attribute.  */
    public void setStyleId(String styleId) {
        this.styleId = styleId;
    }

    /** Returns the style id attribute.  */
    public String getStyleId() {
        return styleId;
    }

// Other Common Elements

    /** Returns the alternate text attribute. */
    public String getAlt() {
        return alt;
    }

    /** Sets the alternate text attribute. */
    public void setAlt(String alt) {
        this.alt = alt;
    }

    /** Returns the advisory title attribute. */
    public String getTitle() {
        return title;
    }

    /** Sets the advisory title attribute. */
    public void setTitle(String title) {
        this.title = title;
    }


    // --------------------------------------------------------- Public Methods


    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        accesskey = null;
        alt = null;
        onclick = null;
        ondblclick = null;
        onmouseover = null;
        onmouseout = null;
        onmousemove = null;
        onmousedown = null;
        onmouseup = null;
        onkeydown = null;
        onkeyup = null;
        onkeypress = null;
        onselect = null;
        onchange = null;
        onblur = null;
        onfocus = null;
        disabled = false;
        readonly = false;
        style = null;
        styleClass = null;
        styleId = null;
        tabindex = null;
        title = null;
        onvalidate = null;
        description = null;
        onpaste = null;
    }


    // ------------------------------------------------------ Protected Methods


    /**
     * Prepares the style attributes for inclusion in the component's HTML tag.
     * @return The prepared String for inclusion in the HTML tag.
     */
    protected void prepareStyles(StringBuilder sb) {
        if (style != null) {
            sb.append(" style=\"");
            sb.append(StringUtils.prepareCDATA(style));
            sb.append("\"");
        }
        if (styleClass != null) {
            sb.append(" class=\"");
            sb.append(StringUtils.prepareCDATA(styleClass));
            sb.append("\"");
        }
        if (styleId != null) {
            sb.append(" id=\"");
            sb.append(StringUtils.prepareCDATA(styleId));
            sb.append("\"");
        }
        if (title != null) {
            sb.append(" title=\"");
            sb.append(StringUtils.prepareCDATA(title));
            sb.append("\"");
        }
        if (alt != null) {
            sb.append(" alt=\"");
            sb.append(StringUtils.prepareCDATA(alt));
            sb.append("\"");
        }
        if (accesskey != null) {
            sb.append(" accesskey=\"");
            sb.append(StringUtils.prepareCDATA(accesskey));
            sb.append("\"");
        }
        if (tabindex != null) {
            sb.append(" tabindex=\"");
            sb.append(StringUtils.prepareCDATA(tabindex));
            sb.append("\"");
        }
    }

    /**
     * Prepares the event handlers for inclusion in the component's HTML tag.
     * @return The prepared String for inclusion in the HTML tag.
     */
    protected void prepareEventHandlers(StringBuilder sb) {
        prepareMouseEvents(sb);
        prepareKeyEvents(sb);
        prepareTextEvents(sb);
        prepareFocusEvents(sb);
        prepareOtherEvents(sb);
    }

    /**
     * Prepares the mouse event handlers, appending them to the the given
     * StringBuilder.
     * @param handlers The StringBuilder that output will be appended to.
     */
    protected void prepareOtherEvents(StringBuilder handlers) {
        if (onpaste != null) {
            handlers.append(" onpaste=\"");
            handlers.append(StringUtils.prepareCDATA(getOnpaste()));
            handlers.append("\"");
        }
        if (onpropertychange != null) {
            handlers.append(" onpropertychange=\"");
            handlers.append(StringUtils.prepareCDATA(getOnpropertychange()));
            handlers.append("\"");
        }
        if (onvalidate != null) {
            handlers.append(" onvalidate=\"");
            handlers.append(StringUtils.prepareCDATA(getOnvalidate()));
            handlers.append("\"");
        }        
        if (description != null) {
            handlers.append(" description=\"");
            handlers.append(StringUtils.prepareCDATA(getDescription()));
            handlers.append("\"");
        }        
    }
    /**
     * Prepares the mouse event handlers, appending them to the the given
     * StringBuilder.
     * @param handlers The StringBuilder that output will be appended to.
     */
    protected void prepareMouseEvents(StringBuilder handlers) {
        if (onclick != null) {
            handlers.append(" onclick=\"");
            handlers.append(StringUtils.prepareCDATA(getOnclick()));
            handlers.append("\"");
        }

        if (ondblclick != null) {
            handlers.append(" ondblclick=\"");
            handlers.append(StringUtils.prepareCDATA(getOndblclick()));
            handlers.append("\"");
        }

        if (onmouseover != null) {
            handlers.append(" onmouseover=\"");
            handlers.append(StringUtils.prepareCDATA(getOnmouseover()));
            handlers.append("\"");
        }

        if (onmouseout != null) {
            handlers.append(" onmouseout=\"");
            handlers.append(StringUtils.prepareCDATA(getOnmouseout()));
            handlers.append("\"");
        }

        if (onmousemove != null) {
            handlers.append(" onmousemove=\"");
            handlers.append(StringUtils.prepareCDATA(getOnmousemove()));
            handlers.append("\"");
        }

        if (onmousedown != null) {
            handlers.append(" onmousedown=\"");
            handlers.append(StringUtils.prepareCDATA(getOnmousedown()));
            handlers.append("\"");
        }

        if (onmouseup != null) {
            handlers.append(" onmouseup=\"");
            handlers.append(StringUtils.prepareCDATA(getOnmouseup()));
            handlers.append("\"");
        }
    }

    /**
     * Prepares the keyboard event handlers, appending them to the the given
     * StringBuilder.
     * @param handlers The StringBuilder that output will be appended to.
     */
    protected void prepareKeyEvents(StringBuilder handlers) {

        if (onkeydown != null) {
            handlers.append(" onkeydown=\"");
            handlers.append(StringUtils.prepareCDATA(getOnkeydown()));
            handlers.append("\"");
        }

        if (onkeyup != null) {
            handlers.append(" onkeyup=\"");
            handlers.append(StringUtils.prepareCDATA(getOnkeyup()));
            handlers.append("\"");
        }

        if (onkeypress != null) {
            handlers.append(" onkeypress=\"");
            handlers.append(StringUtils.prepareCDATA(getOnkeypress()));
            handlers.append("\"");
        }
    }

    /**
     * Prepares the text event handlers, appending them to the the given
     * StringBuilder.
     * @param handlers The StringBuilder that output will be appended to.
     */
    protected void prepareTextEvents(StringBuilder handlers) {

        if (onselect != null) {
            handlers.append(" onselect=\"");
            handlers.append(StringUtils.prepareCDATA(getOnselect()));
            handlers.append("\"");
        }

        if (onchange != null) {
            handlers.append(" onchange=\"");
            handlers.append(StringUtils.prepareCDATA(getOnchange()));
            handlers.append("\"");
        }
    }

    /**
     * Prepares the focus event handlers, appending them to the the given
     * StringBuilder.
     * @param handlers The StringBuilder that output will be appended to.
     */
    protected void prepareFocusEvents(StringBuilder handlers) {

        if (onblur != null) {
            handlers.append(" onblur=\"");
            handlers.append(StringUtils.prepareCDATA(getOnblur()));
            handlers.append("\"");
        }

        if (onfocus != null) {
            handlers.append(" onfocus=\"");
            handlers.append(StringUtils.prepareCDATA(getOnfocus()));
            handlers.append("\"");
        }

        if (disabled) {
            handlers.append(" disabled=\"disabled\"");
        }

        if (readonly) {
            handlers.append(" readonly=\"readonly\"");
        }

    }

    /** Getter for property onpropertychange.
     * @return Value of property onpropertychange.
     */
    public String getOnpropertychange() {
        return onpropertychange;
    }    

    /** Setter for property onpropertychange.
     * @param onpropertychange New value of property onpropertychange.
     */
    public void setOnpropertychange(String onpropertychange) {
        this.onpropertychange = onpropertychange;
    }
    
    /** Getter for property onvalidate.
     * @return Value of property onvalidate.
     */
    public String getOnvalidate() {
        return onvalidate;
    }
    
    /** Setter for property onvalidate.
     * @param onvalidate New value of property onvalidate.
     */
    public void setOnvalidate(String onvalidate) {
        this.onvalidate = onvalidate;
    }
    
    /** Getter for property description.
     * @return Value of property description.
     */
    public String getDescription() {
        return description;
    }
    
    /** Setter for property description.
     * @param description New value of property description.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /** Getter for property onpaste.
     * @return Value of property onpaste.
     */
    public String getOnpaste() {
        return onpaste;
    }
    
    /** Setter for property onpaste.
     * @param onpaste New value of property onpaste.
     */
    public void setOnpaste(String onpaste) {
        this.onpaste = onpaste;
    }
    
}
