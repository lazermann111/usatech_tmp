/*
 * FileTag.java
 */

package simple.taglib.simple;

import javax.servlet.jsp.JspException;

/**
 * Custom tag for input fields of type "file".
 *
 * @author BSK
 */
public class FileTag extends BaseInputTag {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Construct a new instance of this tag.
     */
    public FileTag() {
	super();
    }
    
    /** The type of input field represented by this tag (text, password, or
     * hidden).
     */
    protected String getType() {
        return "file";
    }

    protected void prepareValue(StringBuilder results) throws JspException {
    }
}