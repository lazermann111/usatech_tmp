/*
 * HiddenTag.java
 */

package simple.taglib.simple;


/**
 * Custom tag for input fields of type "hidden".
 *
 * @author BSK
 */
public class HiddenTag extends BaseInputTag {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Construct a new instance of this tag.
     */
    public HiddenTag() {
	super();
    }
    
    /** The type of input field represented by this tag (text, password, or
     * hidden).
     */
    protected String getType() {
        return "hidden";
    }
    
}