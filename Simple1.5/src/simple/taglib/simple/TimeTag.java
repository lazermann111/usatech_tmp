/*
 * TimeTag.java
 *
 * Created on December 16, 2002, 1:55 PM
 */

package simple.taglib.simple;

import java.text.NumberFormat;

import javax.servlet.jsp.JspException;

import simple.taglib.SimpleTagSupport;

/**
 * JSP Custom Tag that times the difference between when the start tag is
 * processed and when the end tag is processed and writes that time to
 * standard out.
 *
 * @author  B Krug
 * @version 
 */
public class TimeTag extends SimpleTagSupport {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected long start;
    protected static final NumberFormat nf = NumberFormat.getNumberInstance();
    
    /** Holds value of property name. */
    private String name;
    
    /** Creates new TimeTag */
    public TimeTag() {
    }

    /**
     * Records the start time.
     */
    public int doStartTag() throws JspException {
        start = System.currentTimeMillis();
        return EVAL_BODY_INCLUDE;
    }
    
    /**
     * Calculates the elapsed time and writes it to standard out
     */
    public int doEndTag() throws JspException {
        logDuration(System.currentTimeMillis() - start);
        return EVAL_PAGE;
    }

    /**
     * Writes the elapsed time to standard out
     */
    protected void logDuration(long ms) {
       log.debug("~~~ Processing of JSP section, " + name + ", took " + nf.format(ms) + " milliseconds.");
        
    }
    
    /** Getter for property name.
     * @return Value of property name.
     */
    public String getName() {
        return name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        name = null;
    }
}
