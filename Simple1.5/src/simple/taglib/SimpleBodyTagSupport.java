/*
 * SimpleTagSupport.java
 *
 * Created on March 4, 2003, 10:52 AM
 */

package simple.taglib;

import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

import simple.io.Log;

/**
 *
 * @author  Brian S. Krug
 */
public class SimpleBodyTagSupport extends BodyTagSupport {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1847349141412L;
	protected Log log = Log.getLog(getClass().getName());
    
    /** Creates a new instance of SimpleTagSupport */
    public SimpleBodyTagSupport() {
        super();
    }
    /**
     * The following is a hack for Tomcat 4.1.18 because it does not call
     * release when it reuses a tag
     */
    public void setParent(Tag parent) {
        release();
        super.setParent(parent);
    }
    
    protected void debug(String msg) {
        log.debug(msg);
    }
}
