/*
 * SimpleTagSupport.java
 *
 * Created on March 4, 2003, 10:52 AM
 */

package simple.taglib;

import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import simple.io.Log;

/**
 *
 * @author  Brian S. Krug
 */
public class SimpleTagSupport extends TagSupport {
    /**
	 * 
	 */
	private static final long serialVersionUID = -674599198914L;
	protected Log log = Log.getLog(getClass().getName());
    /** Creates a new instance of SimpleTagSupport */
    public SimpleTagSupport() {
        super();
    }
    /**
     * The following is a hack for Tomcat 4.1.18 because it does not call
     * release when it reuses a tag
     */
    public void setParent(Tag parent) {
        release();
        super.setParent(parent);
    }
}
