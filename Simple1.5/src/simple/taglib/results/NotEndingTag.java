/*
 * NotEndingTag.java
 *
 * Created on January 9, 2003, 5:44 PM
 */

package simple.taglib.results;

import javax.servlet.jsp.JspException;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class NotEndingTag extends EndingTag {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates new NotBeginsTag */
    public NotEndingTag() {
    }

    protected boolean condition() throws JspException {
        return !super.condition();
    }
}
