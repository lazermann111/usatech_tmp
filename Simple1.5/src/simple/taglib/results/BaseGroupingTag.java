/*
 * BaseConditionTag.java
 *
 * Created on January 9, 2003, 5:30 PM
 */

package simple.taglib.results;

import javax.servlet.jsp.JspException;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public abstract class BaseGroupingTag extends BaseResultsTag {

    /** Holds value of property group. */
    protected String group;
    
    /** Creates new BaseConditionTag */
    public BaseGroupingTag() {
    }

    public int doStartTag() throws JspException {
        if(condition()) return EVAL_BODY_INCLUDE;
        else return SKIP_BODY;
    }
    
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
    
    protected abstract boolean condition() throws JspException ;
    
    /** Getter for property group.
     * @return Value of property group.
     */
    public String getGroup() {
        return group;
    }
    
    /** Setter for property group.
     * @param group New value of property group.
     */
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        group = null;
    }
}
