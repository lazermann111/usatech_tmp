/*
 * GetTag.java
 *
 * Created on December 12, 2002, 1:19 PM
 */

package simple.taglib.results;

import javax.servlet.jsp.JspException;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class ValueTag extends BaseValueTag {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates new GetTag */
    public ValueTag() {
    }
    
    protected Object getValue() throws JspException {
        return getResults().getValue(name);
    }   
}
