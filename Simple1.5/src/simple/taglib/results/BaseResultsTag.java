/*
 * BaseResultsTag.java
 *
 * Created on December 12, 2002, 1:19 PM
 */

package simple.taglib.results;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;

import simple.results.Results;
import simple.servlet.RequestUtils;
import simple.taglib.SimpleTagSupport;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public abstract class BaseResultsTag extends SimpleTagSupport {
    
    /** Holds value of property bean. */
    protected String bean;
    
    private Results results = null;
    
    /** Holds value of property scope. */
    protected String scope;
    
    /** Creates new GetTag */
    public BaseResultsTag() {
    }
        
    protected Results getResults() throws JspException {
        if(results == null) {
            if(bean == null) { //get parent iterate tag's results
                for(Tag tag = getParent(); tag != null; tag = tag.getParent()) {
                    if(tag instanceof IterateTag) {
                        results = ((IterateTag)tag).getResults();
                        break;
                    }
                }
                if(results == null) throw new JspException("Could not find results object");
            } else {
                try {
                    results = (Results) RequestUtils.getAttribute(pageContext,bean,true,scope);
                } catch(ServletException se) {
                    throw new JspException(se.getMessage());
                }
            }
        }
        return results;       
    }
        
    /** Getter for property bean.
     * @return Value of property bean.
     */
    public String getBean() {
        return bean;
    }
    
    /** Setter for property bean.
     * @param bean New value of property bean.
     */
    public void setBean(String bean) {
        this.bean = bean;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     */
    public String getScope() {
        return scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        bean = null;
        results = null;
        scope = null;
    }
}
