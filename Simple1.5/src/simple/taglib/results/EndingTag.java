/*
 * EndsTag.java
 *
 * Created on January 9, 2003, 5:33 PM
 */

package simple.taglib.results;

import javax.servlet.jsp.JspException;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class EndingTag extends BaseGroupingTag {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates new begins */
    public EndingTag() {
    }

    protected boolean condition() throws JspException {
        return getResults().isGroupEnding(group);
    }
    
}
