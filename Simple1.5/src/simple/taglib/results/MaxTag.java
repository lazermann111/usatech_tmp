/*
 * CountTag.java
 *
 * Created on January 9, 2003, 3:54 PM
 */

package simple.taglib.results;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class MaxTag extends BaseAggrTag {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Creates new CountTag */
    public MaxTag() {
        aggregateType = simple.results.Results.Aggregate.MAX;
    }

}
