/*
 * GetTag.java
 *
 * Created on December 12, 2002, 1:19 PM
 */

package simple.taglib.results;

import javax.servlet.jsp.JspException;

import simple.results.Results;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public abstract class BaseAggrTag extends BaseValueTag {
    protected Results.Aggregate aggregateType;
    
    /** Holds value of property group. */
    private String group;
    
    /** Creates new GetTag */
    public BaseAggrTag() {
    }
    
    protected Object getValue() throws JspException {
        return getResults().getAggregate(name, group, aggregateType);
    }   
    
    /** Getter for property group.
     * @return Value of property group.
     */
    public String getGroup() {
        return group;
    }
    
    /** Setter for property group.
     * @param group New value of property group.
     */
    public void setGroup(String group) {
        this.group = group;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        group = null;
    }    
}
