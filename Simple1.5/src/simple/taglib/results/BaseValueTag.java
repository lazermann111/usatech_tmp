/*
 * BaseResultsTag.java
 *
 * Created on December 12, 2002, 1:19 PM
 */

package simple.taglib.results;

import javax.servlet.jsp.JspException;

import simple.bean.ConvertUtils;
import simple.servlet.ResponseUtils;
import simple.text.StringUtils;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public abstract class BaseValueTag extends BaseResultsTag {
    
    /** Holds value of property name. */
    protected String name;
    
    /** Holds value of property prepare. */
    protected String prepare = "HTML";
    
    /** Holds value of property format. */
    protected String format;
    
    /** Creates new GetTag */
    public BaseValueTag() {
    }
    
    /**
     * Render the specified error messages if there are any.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {
        String s = StringUtils.prepareString(ConvertUtils.formatObject(getValue(), format), prepare);
        if (s != null && s.length() > 0) ResponseUtils.write(pageContext, s);
        
        return (EVAL_BODY_INCLUDE); // Continue processing this page
    }
    
    protected abstract Object getValue() throws JspException ;
        
    /** Getter for property name.
     * @return Value of property name.
     */
    public String getName() {
        return name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /** Getter for property prepare.
     * @return Value of property prepare.
     */
    public String getPrepare() {
        return prepare;
    }
    
    /** Setter for property prepare.
     * @param prepare New value of property prepare.
     */
    public void setPrepare(String prepare) {
        this.prepare = prepare;
    }
    
    /** Getter for property format.
     * @return Value of property format.
     */
    public String getFormat() {
        return format;
    }
    
    /** Setter for property format.
     * @param format New value of property format.
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * Release any acquired resources.
     */
    public void release() {
        super.release();
        name = null;
        prepare = "HTML";
        format = null;
    }
}
