/*
 * Created on Jul 25, 2005
 *
 */
package simple.app;

public class RetrySpecifiedServiceException extends ServiceException {
	private static final long serialVersionUID = 12353393827511L;
	protected final WorkRetryType retryType;
	protected final boolean unguardedRetryAllowed;

	public RetrySpecifiedServiceException(WorkRetryType retryType) {
        this(retryType, false);
    }

    public RetrySpecifiedServiceException(String message, WorkRetryType retryType) {
        this(message, retryType, false);
    }

    public RetrySpecifiedServiceException(String message, Throwable cause, WorkRetryType retryType) {
        this(message, cause, retryType, false);
    }

    public RetrySpecifiedServiceException(Throwable cause, WorkRetryType retryType) {
        this(cause, retryType, false);
   }

	public RetrySpecifiedServiceException(WorkRetryType retryType, boolean unguardedRetryAllowed) {
        super();
        this.retryType = retryType;
        this.unguardedRetryAllowed = unguardedRetryAllowed;
    }

    public RetrySpecifiedServiceException(String message, WorkRetryType retryType, boolean unguardedRetryAllowed) {
        super(message);
        this.retryType = retryType;
        this.unguardedRetryAllowed = unguardedRetryAllowed;
   }

    public RetrySpecifiedServiceException(String message, Throwable cause, WorkRetryType retryType, boolean unguardedRetryAllowed) {
        super(message, cause);
        this.retryType = retryType;
        this.unguardedRetryAllowed = unguardedRetryAllowed;
   }

    public RetrySpecifiedServiceException(Throwable cause, WorkRetryType retryType, boolean unguardedRetryAllowed) {
        super(cause);
        this.retryType = retryType;
        this.unguardedRetryAllowed = unguardedRetryAllowed;
   }
	public WorkRetryType getRetryType() {
		return retryType;
	}

	public boolean isUnguardedRetryAllowed() {
		return unguardedRetryAllowed;
	}
}
