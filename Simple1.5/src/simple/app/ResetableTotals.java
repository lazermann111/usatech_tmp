package simple.app;

public interface ResetableTotals<S> extends Totals<S>{
	public void reset();
}
