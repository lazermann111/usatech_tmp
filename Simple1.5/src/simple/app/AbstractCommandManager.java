package simple.app;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import simple.util.CaseInsensitiveComparator;

/** Base class that allows registration of various commands, construction of a usage message based
 * on those registered commands and processing of commands from a {@link Commander}.
 * @author Brian S. Krug
 *
 * @param <C> The type of argument passed to the {@link Command#executeCommand(Object, PrintWriter, String[])} method
 */
public abstract class AbstractCommandManager<C> implements CommandManager<C> {
	protected final Map<String,Command<C>> commands = new TreeMap<String,Command<C>>(new CaseInsensitiveComparator<String>());
	protected final Collection<Command<C>> commandCollUnmod = Collections.unmodifiableCollection(commands.values());
	public abstract static class AbstractCommand<C> implements Command<C> {
		protected final String key;
		protected final String description;
		protected final CommandArgument[] commandArguments;
		public AbstractCommand(String key, String description, CommandArgument[] commandArguments) {
			this.key = key;
			this.description = description;
			this.commandArguments = commandArguments;
		}
		public String getKey() {
			return key;
		}
		public String getDescription() {
			return description;
		}
		public CommandArgument[] getCommandArguments() {
			return commandArguments;
		}
	}


	/** Creates a new AbstractCommandManager and calls {@link #registerDefaultCommands()}
	 *
	 */
	public AbstractCommandManager() {
		registerDefaultCommands();
	}

	/** Allows sub-classes to register commands
	 *
	 */
	protected abstract void registerDefaultCommands() ;

	/**
	 * @see simple.app.CommandManager#registerCommand(simple.app.AbstractCommandManager.Command)
	 */
	public boolean registerCommand(Command<C> command) {
		return commands.put(command.getKey(), command) != null;
	}

	/**
	 * @see simple.app.CommandManager#deregisterCommand(java.lang.String)
	 */
	public boolean deregisterCommand(String commandKey) {
		return commands.remove(commandKey) != null;
	}

	/** Constructs the command usage message based on the registered commands
	 * @param out The PrintWriter to which the usage message is written
	 */
	protected void printHelp(PrintWriter out) {
		out.println();
		out.println("Valid Commands:");
		for(Command<C> command : commands.values()) {
			out.print("    '");
			out.print(command.getKey());
			out.print("' - ");
			out.println(command.getDescription());
		}
		out.println();
	}

	/** Constructs and returns the command usage message based on the registered commands
	 * @return The usage message
	 */
	public String getHelpPrompt() {
		StringWriter out = new StringWriter();
		printHelp(new PrintWriter(out, true));
		return out.toString();
	}

	/**
	 * @see simple.app.CommandManager#getCommand(java.lang.String)
	 *//*
	public void processCommands(Commander commander) {
		String prompt = getHelpPrompt();
		if(prompt.length() == 0)
			prompt = null;
		commander.setPrompt(prompt);
		boolean done = false;
		while(!done) {
			CommandLine line;
			try {
				line = commander.nextCommandLine();
			} catch(IOException e) {
				log.warn("IO Exception reading from input. Getting next Command Line...", e);
				continue;
			}
			if(line == null) break; //end of stream reached
			if(line.getAction() == null) continue; // an empty line, wait for next line
			Command<C> cmd = commands.get(line.getAction());
			PrintWriter out = line.getPrintWriter();
			if(out == null) {
				log.debug("CommandLine PrintWriter is null - not processing command '" + line.getAction() + "'");
			} else {
				if(cmd == null) {
					out.println("Command '" + line.getAction() + "' not recognized");
				} else {
					done = !cmd.executeCommand(getCommandArgument(), out, line.getArguments());
				}
				if(prompt != null && !done)
					out.println(prompt);
				//out.flush();
				out.close();
			}
		}
		commander.shutdown();
	}
*/
	public Command<C> getCommand(String key) {
		return commands.get(key);
	}

	/**
	 * @see simple.app.CommandManager#getCommands()
	 */
	public Collection<Command<C>> getCommands() {
		return commandCollUnmod;
	}
}
