package simple.app;

import org.apache.commons.configuration.Configuration;

public interface ConfigurationTracker {
	public void setConfiguration(Configuration config);
}
