/**
 *
 */
package simple.app;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.io.Log;
import simple.util.MapBackedSet;

/**
 * @author Brian S. Krug
 *
 */
public class PrerequisiteManager implements PrerequisiteCheck, ResetAvailabilityListener {
	private static final Log log = Log.getLog();
	protected static final Prerequisite[] EMPTY_PREREQUISITES = new Prerequisite[0];
	//NOTE: The only places where we need concurrent access are in getPrerequisites() and setPrerequisites()
	protected final ConcurrentMap<Prerequisite, AtomicReference<AvailableState>> prerequisites = new ConcurrentHashMap<Prerequisite, AtomicReference<AvailableState>>();
	protected final Set<LocalAvailability> availabilities = new MapBackedSet<LocalAvailability>(new ConcurrentHashMap<LocalAvailability, Object>());
	protected final Set<AvailabilityChangeListener> listeners = new LinkedHashSet<AvailabilityChangeListener>();

	protected static enum AvailableState {
		UNKNOWN, AVAILABLE, UNAVAILABLE, CANCELLED, PREVIOUSLY_AVAILABLE, PREVIOUSLY_UNAVAILABLE
	};
	protected class LocalAvailability {
		protected final AtomicReference<AvailableState> state = new AtomicReference<AvailableState>(AvailableState.UNKNOWN);
		/**
		 *
		 */
		public LocalAvailability() {
			availabilities.add(this);
		}

		public boolean isAvailable(boolean wait) {
			if(state.get() == AvailableState.AVAILABLE)
				return true;
			if(!wait)
				return false;
			while(true) {
				AvailableState oldState = state.get();
				switch(oldState) {
					case AVAILABLE:
						log.info("Prerequisites are now available");
						return true;
					case CANCELLED:
						if(state.compareAndSet(AvailableState.CANCELLED, AvailableState.UNKNOWN)) {
							log.info("Prerequisite check is cancelled; returning unavailable");
							return false;
						}
						break;
					case UNKNOWN:
					case PREVIOUSLY_AVAILABLE: // should never be
					case PREVIOUSLY_UNAVAILABLE: // should never be
						AvailableState newState = checkState();
						if(state.compareAndSet(oldState, newState) && log.isInfoEnabled())
							log.info("State is now " + newState);
						break;
					case UNAVAILABLE:
						if(log.isDebugEnabled())
							log.debug("State is " + state + "; Waiting " + getInterval() + " milliseconds before checking prerequisites");
						lock.lock();
						try {
							signal.await(getInterval(), TimeUnit.MILLISECONDS);
						} catch(InterruptedException e) {
							// ignore
						} finally {
							lock.unlock();
						}
						// state could be CANCELLED
						newState = checkState();
						state.compareAndSet(AvailableState.UNAVAILABLE, newState);
						if(newState == AvailableState.UNAVAILABLE)
							return false;
				}
			}		
		}
		public void cancel() {
			boolean updated = false;
			while(!updated) {
				AvailableState current = state.get();
				switch(current) {
					case CANCELLED:
					case UNKNOWN:
					case PREVIOUSLY_AVAILABLE: // should never be
					case PREVIOUSLY_UNAVAILABLE: // should never be
						updated = this.state.compareAndSet(current, AvailableState.CANCELLED);
						break;
					case AVAILABLE: case UNAVAILABLE:
						updated = true;
						break;
				}
			}
			lock.lock();
			try {
				signal.signalAll();
			} finally {
				lock.unlock();
			}
			
		}
		
		public void resetAvailability() {
			this.state.set(AvailableState.UNKNOWN);
			lock.lock();
			try {
				signal.signalAll();
			} finally {
				lock.unlock();
			}			
		}
	}
	protected final ThreadLocal<LocalAvailability> availableLocal = new ThreadLocal<LocalAvailability>() {
		/**
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected LocalAvailability initialValue() {
			return new LocalAvailability();
		}
	};
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition signal = lock.newCondition();
	protected long interval = 5000;

	protected AvailableState checkState() {
		if(log.isDebugEnabled())
			log.debug("Checking prerequisites");
		if(prerequisites.isEmpty()) {
			fireAvailabilityChanged(null, true);
			return AvailableState.AVAILABLE;
		}
		for(Map.Entry<Prerequisite, AtomicReference<AvailableState>> entry : prerequisites.entrySet()) {
			if(entry.getValue().get() != AvailableState.AVAILABLE) {
				if(entry.getKey().isAvailable()) {
					AvailableState oldState = entry.getValue().getAndSet(AvailableState.AVAILABLE);
					switch(oldState) {
						case AVAILABLE:
						case PREVIOUSLY_AVAILABLE:
							break;
						default:
							fireAvailabilityChanged(entry.getKey(), true);
					}
				} else {
					AvailableState oldState = entry.getValue().getAndSet(AvailableState.UNAVAILABLE);
					switch(oldState) {
						case UNAVAILABLE:
						case PREVIOUSLY_UNAVAILABLE:
							break;
						default:
							fireAvailabilityChanged(entry.getKey(), false);
					}
					return AvailableState.UNAVAILABLE;
				}
			}
		}
		return AvailableState.AVAILABLE;
	}
	/**
	 * @see simple.app.WorkQueue.Retriever#arePrequisitesAvailable()
	 */
	public boolean arePrequisitesAvailable() {
		return availableLocal.get().isAvailable(true);
	}
	/**
	 * @see simple.app.WorkQueue.Retriever#cancelPrequisiteCheck(java.lang.Thread)
	 */
	public void cancelPrequisiteCheck(Thread thread) {
		availableLocal.get().cancel();
	}

	public void resetAvailability() {
		boolean reset = false;
		for(Map.Entry<Prerequisite, AtomicReference<AvailableState>> entry : prerequisites.entrySet()) {
			reset |= resetAvailability(entry.getKey(), entry.getValue(), false);
		}
		if(!reset)
			return;
		log.info("Setting availability to " + AvailableState.UNKNOWN);
		for(LocalAvailability la : availabilities) {
			la.resetAvailability();
		}
	}

	protected boolean resetAvailability(Prerequisite prerequisite, AtomicReference<AvailableState> state, boolean resetLocals) {
		while(true) {
			AvailableState oldState = state.get();
			AvailableState newState;
			switch(oldState) {
				case AVAILABLE:
					newState = AvailableState.PREVIOUSLY_AVAILABLE;
					break;
				case UNAVAILABLE:
					newState = AvailableState.PREVIOUSLY_UNAVAILABLE;
					break;
				case CANCELLED:
					newState = AvailableState.UNKNOWN;
					break;
				default:
					return false;
			}
			if(state.compareAndSet(oldState, newState)) {
				if(resetLocals) {
					log.info("Setting availability to " + newState + " for " + prerequisite);
					for(LocalAvailability la : availabilities) {
						la.resetAvailability();
					}
				}
				return true;
			}
		}
	}

	public void resetAvailability(Prerequisite prerequisite) {
		AtomicReference<AvailableState> state = prerequisites.get(prerequisite);
		if(state == null)
			throw new IllegalArgumentException("This prerequisite is not registered: " + prerequisite);
		resetAvailability(prerequisite, state, true);
	}

	public Prerequisite[] getPrerequisites() {
		return prerequisites.keySet().toArray(EMPTY_PREREQUISITES);
	}

	public void setPrerequisites(Prerequisite[] prereqs) {
		prerequisites.keySet().retainAll(new HashSet<Prerequisite>(Arrays.asList(prereqs)));
		for(Prerequisite pr : prereqs)
			addPrerequisite(pr);
	}

	public boolean addPrerequisite(Prerequisite prereq) {
		if(prerequisites.putIfAbsent(prereq, new AtomicReference<AvailableState>(AvailableState.UNKNOWN)) == null) {
			if(prereq instanceof ResetAvailabilityTrigger)
				((ResetAvailabilityTrigger) prereq).registerListener(this);
			return true;
		}
		return false;
	}

	public boolean removePrerequisite(Prerequisite prereq) {
		if(prerequisites.remove(prereq) != null) {
			if(prereq instanceof ResetAvailabilityTrigger)
				((ResetAvailabilityTrigger) prereq).deregisterListener(this);
			return true;
		}
		return false;
	}

	public long getInterval() {
		return interval;
	}
	public void setInterval(long interval) {
		this.interval = interval;
	}

	public void addAvailabilityChangeListener(AvailabilityChangeListener listener) {
		listeners.add(listener);
		// fire any available or unavailables
		for(Map.Entry<Prerequisite, AtomicReference<AvailableState>> entry : prerequisites.entrySet())
			switch(entry.getValue().get()) {
				case UNKNOWN:
				case PREVIOUSLY_AVAILABLE:
				case PREVIOUSLY_UNAVAILABLE:
					break;
				case AVAILABLE:
					listener.availibilityChanged(entry.getKey(), true);
					break;
				default:
					listener.availibilityChanged(entry.getKey(), false);
					break;
			}
	}

	public void removeAvailabilityChangeListener(AvailabilityChangeListener listener) {
		listeners.remove(listener);
	}

	protected void fireAvailabilityChanged(Prerequisite prerequisite, boolean available) {
		for(AvailabilityChangeListener listener : listeners)
			listener.availibilityChanged(prerequisite, available);
	}
}
