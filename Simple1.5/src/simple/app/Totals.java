package simple.app;

public interface Totals<S> {
	public void replace(S oldStats, S newStats) ;

	public S getSampleTotal();

	public S getGrandTotal();
}
