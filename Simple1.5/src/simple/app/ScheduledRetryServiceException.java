package simple.app;

public class ScheduledRetryServiceException extends RetrySpecifiedServiceException {
	private static final long serialVersionUID = 3417981859550152128L;
	protected final RetryPolicy retryPolicy;
	protected final boolean recheckPrerequisites;

	public ScheduledRetryServiceException(RetryPolicy retryPolicy) {
		this(retryPolicy, false, false);
	}

	public ScheduledRetryServiceException(String message, RetryPolicy retryPolicy) {
		this(message, retryPolicy, false, false);
	}

	public ScheduledRetryServiceException(String message, Throwable cause, RetryPolicy retryPolicy) {
		this(message, cause, retryPolicy, false, false);
	}

	public ScheduledRetryServiceException(Throwable cause, RetryPolicy retryPolicy) {
		this(cause, retryPolicy, false, false);
	}

	public ScheduledRetryServiceException(RetryPolicy retryPolicy, boolean unguardedRetryAllowed) {
		this(retryPolicy, unguardedRetryAllowed, false);
	}

	public ScheduledRetryServiceException(String message, RetryPolicy retryPolicy, boolean unguardedRetryAllowed) {
		this(message, retryPolicy, unguardedRetryAllowed, false);
	}

	public ScheduledRetryServiceException(String message, Throwable cause, RetryPolicy retryPolicy, boolean unguardedRetryAllowed) {
		this(message, cause, retryPolicy, unguardedRetryAllowed, false);
	}

	public ScheduledRetryServiceException(Throwable cause, RetryPolicy retryPolicy, boolean unguardedRetryAllowed) {
		this(cause, retryPolicy, unguardedRetryAllowed, false);
	}

	public ScheduledRetryServiceException(RetryPolicy retryPolicy, boolean unguardedRetryAllowed, boolean recheckPrerequisites) {
		super(WorkRetryType.SCHEDULED_RETRY, unguardedRetryAllowed);
		this.retryPolicy = retryPolicy;
		this.recheckPrerequisites = recheckPrerequisites;
	}

	public ScheduledRetryServiceException(String message, RetryPolicy retryPolicy, boolean unguardedRetryAllowed, boolean recheckPrerequisites) {
		super(message, WorkRetryType.SCHEDULED_RETRY, unguardedRetryAllowed);
		this.retryPolicy = retryPolicy;
		this.recheckPrerequisites = recheckPrerequisites;
	}

	public ScheduledRetryServiceException(String message, Throwable cause, RetryPolicy retryPolicy, boolean unguardedRetryAllowed, boolean recheckPrerequisites) {
		super(message, cause, WorkRetryType.SCHEDULED_RETRY, unguardedRetryAllowed);
		this.retryPolicy = retryPolicy;
		this.recheckPrerequisites = recheckPrerequisites;
	}

	public ScheduledRetryServiceException(Throwable cause, RetryPolicy retryPolicy, boolean unguardedRetryAllowed, boolean recheckPrerequisites) {
		super(cause, WorkRetryType.SCHEDULED_RETRY, unguardedRetryAllowed);
		this.retryPolicy = retryPolicy;
		this.recheckPrerequisites = recheckPrerequisites;
	}
	public RetryPolicy getRetryPolicy() {
		return retryPolicy;
	}

	public boolean isRecheckPrerequisites() {
		return recheckPrerequisites;
	}
}
