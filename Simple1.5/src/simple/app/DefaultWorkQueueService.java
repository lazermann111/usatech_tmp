package simple.app;

import simple.db.StatementMonitor;



/** Default implementation of WorkQueueService. It uses a common WorkProcessor and thus does not provide functionality to cancel
 *  processing mid-stream.
 *
 * @author Brian S. Krug
 *
 * @param <Q>
 */
public class DefaultWorkQueueService<Q> extends AbstractWorkQueueService<Q> {
	protected Processor<Q,Void> processor;
	protected WorkProcessor<Q> commonWorkProcessor;

	public DefaultWorkQueueService(String serviceName) {
		super(serviceName);
		this.commonWorkProcessor = new WorkProcessor<Q>() {
			public void processWork(Work<Q> work) throws ServiceException, WorkQueueException {
				process(work.getBody());
			}

			public boolean interruptProcessing(Thread thread) {
				StatementMonitor.cancelStatements(thread);
				thread.interrupt();
				return true;
			}
		};
	}

	@Override
	protected WorkProcessor<Q> getWorkProcessor() {
		return commonWorkProcessor;
	}

	protected void process(Q body) throws ServiceException {
		if(processor != null) processor.process(body);
		else throw new ServiceException("No processor set");
	}

	public Processor<Q,Void> getProcessor() {
		return processor;
	}

	public void setProcessor(Processor<Q,Void> processor) {
		if(processor != null && getQueue() != null && !processor.getArgumentType().isAssignableFrom(getQueue().getWorkBodyType()))
			throw new IllegalArgumentException("Processor argument type '" + processor.getArgumentType().getName()
					+ "' is not assignable from the queue work body type '" + getQueue().getWorkBodyType().getName() + "'");
		this.processor = processor;
	}

	@Override
	public void setQueue(WorkQueue<Q> queue) {
		if(getProcessor() != null && queue != null && !getProcessor().getArgumentType().isAssignableFrom(queue.getWorkBodyType()))
			throw new IllegalArgumentException("Processor argument type '" + getProcessor().getArgumentType().getName()
					+ "' is not assignable from the queue work body type '" + queue.getWorkBodyType().getName() + "'");
		super.setQueue(queue);
	}
}
