package simple.app;

public interface Processor<A,R> {
	public R process(A argument) throws ServiceException ;
	public Class<R> getReturnType() ;
	public Class<A> getArgumentType() ;
}
