package simple.app;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.mail.MessagingException;

import simple.bean.ConvertException;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.mail.Email;
import simple.mail.Emailer;
import simple.util.MapBackedSet;

public class EmailingAdvisor implements Advisor {
	private static final Log log = Log.getLog();
	protected final Set<String> downItems = new MapBackedSet<String>(new ConcurrentHashMap<String, Object>());
	protected final Emailer emailer;
	protected String actionMailTo;
	protected String errorMailTo;
	protected String warningMailTo;
	protected String upDownMailTo;
	protected String mailFrom;
	public EmailingAdvisor(Properties properties) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
		emailer = new Emailer(properties);
		ReflectionUtils.copyProperties(this, properties);
	}
	public void advise(AdviseType adviseType, String category, String item, String details) {
		String mailTo;
		String key;
		switch(adviseType) {
			case DOWN:
				key = createUpDownKey(category, item);
				if(downItems.add(key))
					mailTo = getUpDownMailTo();
				else
					return;
				break;
			case UP:
				key = createUpDownKey(category, item);
				if(downItems.remove(key))
					mailTo = getUpDownMailTo();
				else
					return;
				break;
			case ACTION:
				mailTo = getActionMailTo();
				break;
			case ERROR:
				mailTo = getErrorMailTo();
				break;
			case WARNING:
				mailTo = getWarningMailTo();
				break;
			default:
				return;
		}
		if(mailTo != null) {
			try {
				Email email = emailer.createEmail();
				email.setTo(mailTo);
				email.setFrom(getMailFrom());
				email.setSubject(adviseType.toString() + " - " + category + " - " + item);
				email.setBody(details);
				email.send();
			} catch(MessagingException e) {
				log.error("Could not send message", e);
			} catch(RuntimeException e) {
				log.error("Could not send message", e);
			}
		}
	}
	public String getActionMailTo() {
		return actionMailTo;
	}
	public void setActionMailTo(String actionMailTo) {
		this.actionMailTo = actionMailTo;
	}
	public String getErrorMailTo() {
		return errorMailTo;
	}
	public void setErrorMailTo(String errorMailTo) {
		this.errorMailTo = errorMailTo;
	}
	public String getWarningMailTo() {
		return warningMailTo;
	}
	public void setWarningMailTo(String warningMailTo) {
		this.warningMailTo = warningMailTo;
	}
	public String getUpDownMailTo() {
		return upDownMailTo;
	}
	public void setUpDownMailTo(String upDownMailTo) {
		this.upDownMailTo = upDownMailTo;
	}

	protected String createUpDownKey(String category, String item) {
		return category + '\0' + item;
	}
	public String getMailFrom() {
		return mailFrom;
	}
	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}
}
