package simple.app;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.configuration.ConversionException;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.FileSystem;
import org.apache.commons.configuration.FileSystemBased;
import org.apache.commons.configuration.SubsetConfiguration;
import org.apache.commons.configuration.reloading.ReloadingStrategy;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.EnhancedBufferedReader;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.CollectionUtils;
import simple.util.FilteredIterator;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

public class SimpleConfiguration implements FileConfiguration, FileSystemBased /*extends AbstractFileConfiguration*/{
	protected final static Log log = Log.getLog();

	protected class Parser {
		protected int state = -1; // -1 = not started; 0 = whitespace; 1 = key; 2 = separator; 3 = value; 4 = comment; 5 = after dollar; 6 = in interpolation
		protected StringBuilder raw = new StringBuilder();
		protected StringBuilder sb = new StringBuilder();
		protected char prev;
		protected String rawProperty;
		protected String key;
		protected String[][] valueReferenceArrays;
		protected final FileInfo fileInfo;

		public Parser(FileInfo fileInfo) {
			this.fileInfo = fileInfo;
		}

		/**
		 * @throws ConfigurationException
		 */
		public void raw(char ch) throws ConfigurationException {
			if(state >= 0)
				raw.append(prev);
			else
				state = 0;
			prev = ch;
		}

		public void comment(char ch) throws ConfigurationException {
			switch(state) {
				case 0:
					state = 4;
					break;
				default:
					interpretted(ch);
			}
		}

		/**
		 * @throws ConfigurationException
		 */
		public void interpretted(char ch) throws ConfigurationException {
			switch(state) {
				case 0:
					state = 1;
				case 1:
					sb.append(ch);
					break;
				case 2:
					rawProperty = raw.toString();
					raw.setLength(0);
					state = 3;
				case 3:
				case 6:
					sb.append(ch);
					break;
				case 5:
					sb.append('$');
					sb.append(ch);
					state = 3;
					break;
			}
		}

		public void newline(char ch) throws ConfigurationException {
			switch(state) {
				case 1:
					rawProperty = raw.toString();
					raw.setLength(0);
					key = sb.toString().trim();
					sb.setLength(0);
					makeProperty();
					break;
				case 2:
					rawProperty = raw.toString();
					raw.setLength(0);
					makeProperty();
					break;
				case 5:
					sb.append('$');
					state = 3;
				case 3:
				case 6:
					makeProperty();
					break;
				case 4:
					state = 0;
					break;
			}
		}

		/**
		 * @throws ConfigurationException
		 */
		public void whitespace(char ch) throws ConfigurationException {
			switch(state) {
				case 1: // Is this right to treat whitespace as separator?
					key = sb.toString().trim();
					sb.setLength(0);
					state = 2;
					break;
				case 2:
					break;
				case 5:
					sb.append('$');
					state = 3;
				case 3:
				case 6:
					sb.append(ch);
					break;
			}
		}

		public void separator(char ch) throws ConfigurationException {
			switch(state) {
				case 1:
					key = sb.toString().trim();
					sb.setLength(0);
					state = 2;
					break;
				default:
					interpretted(ch);
			}
		}

		public void list(char ch) throws ConfigurationException {
			switch(state) {
				case 5:
					sb.append('$');
					state = 3;
				case 3:
					// value is a list
					String value = sb.toString();
					sb.setLength(0);
					if(valueReferenceArrays == null)
						valueReferenceArrays = new String[][] { { value }, { null } };
					else {
						if(value.length() > 0) {
							String[] vra = valueReferenceArrays[valueReferenceArrays.length - 1];
							if(vra == null)
								vra = new String[] { value };
							else if(vra[vra.length - 1] == null)
								vra[vra.length - 1] = value;
							else
								vra = CollectionUtils.add(vra, value);
							valueReferenceArrays[valueReferenceArrays.length - 1] = vra;
						}
						valueReferenceArrays = CollectionUtils.add(valueReferenceArrays, null);
					}
					break;
				default:
					interpretted(ch);
			}
		}

		public void finish() throws ConfigurationException {
			switch(state) {
				case 1:
					rawProperty = raw.toString();
					raw.setLength(0);
					key = sb.toString().trim();
					sb.setLength(0);
					makeProperty();
					break;
				case 2:
					rawProperty = raw.toString();
					raw.setLength(0);
					makeProperty();
					break;
				case 5:
					sb.append('$');
					state = 3;
				case 3:
					makeProperty();
					break;
				case 6:
					if(!isIgnoreUnparseableLines())
						throw new ConfigurationException("Unterminated variable at end of file");
					makeProperty();
					break;
			}
			if(state >= 0)
				raw.append(prev);
			fileInfo.setRemainder(raw.toString());
		}

		@SuppressWarnings("unused")
		protected void makeProperty() throws ConfigurationException {
			String rawValue = raw.toString();
			raw.setLength(0);
			String value = sb.toString();
			sb.setLength(0);
			if(isInclude(key)) {
				new PropertyInfo(rawProperty, fileInfo, key, rawValue, (String[][]) null);
				if(valueReferenceArrays == null)
					loadIncludeFile(value);
				else {
					String[] vra = valueReferenceArrays[valueReferenceArrays.length - 1];
					if(vra == null)
						vra = new String[] { value };
					else if(vra[vra.length - 1] == null)
						vra[vra.length - 1] = value;
					else
						vra = CollectionUtils.add(vra, value);
					valueReferenceArrays[valueReferenceArrays.length - 1] = vra;
					for(String[] valueReferenceArray : valueReferenceArrays) {
						if(valueReferenceArray == null || valueReferenceArray.length == 0)
							continue;
						if(valueReferenceArray.length == 1)
							loadIncludeFile(valueReferenceArray[0]);
						else {
							StringBuilder sb = new StringBuilder();
							for(int i = 0; i < valueReferenceArray.length; i++) {
								if((i % 2) == 0)
									sb.append(valueReferenceArray[i]);
								else
									sb.append("${").append(valueReferenceArray[i]).append('}');
							}
							loadIncludeFile(sb.toString());
						}
					}
				}
			} else if(valueReferenceArrays == null)
				properties.put(key, new PropertyInfo(rawProperty, fileInfo, key, rawValue, value));
			else {
				String[] vra = valueReferenceArrays[valueReferenceArrays.length - 1];
				if(vra == null)
					vra = new String[] { value };
				else if(vra[vra.length - 1] == null)
					vra[vra.length - 1] = value;
				else
					vra = CollectionUtils.add(vra, value);
				valueReferenceArrays[valueReferenceArrays.length - 1] = vra;
				properties.put(key, new PropertyInfo(rawProperty, fileInfo, key, rawValue, valueReferenceArrays));
			}
			state = 0;
			valueReferenceArrays = null;
		}

		public void dollar(char ch) throws ConfigurationException {
			switch(state) {
				case 2:
					rawProperty = raw.toString();
					raw.setLength(0);
				case 3:
					state = 5;
					break;
				default:
					interpretted(ch);
			}
		}

		public void openBrace(char ch) throws ConfigurationException {
			switch(state) {
				case 5:
					state = 6;
					String value = sb.toString();
					sb.setLength(0);
					if(valueReferenceArrays == null)
						valueReferenceArrays = new String[][] { { value, null } };
					else if(valueReferenceArrays[valueReferenceArrays.length - 1] == null)
						valueReferenceArrays[valueReferenceArrays.length - 1] = new String[] { value, null };
					else {
						String[] vra = valueReferenceArrays[valueReferenceArrays.length - 1];
						String[] tmp = new String[vra.length + 2];
						System.arraycopy(vra, 0, tmp, 0, vra.length);
						tmp[vra.length] = value;
						valueReferenceArrays[valueReferenceArrays.length - 1] = tmp;
					}
					break;
				default:
					interpretted(ch);
			}
		}

		public void closeBrace(char ch) throws ConfigurationException {
			switch(state) {
				case 6:
					state = 3;
					String value = sb.toString();
					sb.setLength(0);
					String[] vra = valueReferenceArrays[valueReferenceArrays.length - 1];
					vra[vra.length - 1] = value;
					break;
				default:
					interpretted(ch);
			}
		}
	}

	protected class PropertyInfo {
		protected final String key;
		protected final String rawProperty;
		protected final FileInfo fileInfo;
		protected boolean saved;
		protected String rawValue;
		protected String[][] valueReferenceArrays;
		protected String literalValue;

		public PropertyInfo(String rawProperty, FileInfo fileInfo, String key) {
			this.rawProperty = rawProperty;
			this.fileInfo = fileInfo;
			this.key = key;
			this.saved = true;
			fileInfo.lock();
			try {
				fileInfo.addPropertyInfo(this);
			} finally {
				fileInfo.unlock();
			}
		}

		public PropertyInfo(String rawProperty, FileInfo fileInfo, String key, String rawValue, String[] literalValues) {
			this(rawProperty, fileInfo, key);
			setLiteralValues(rawValue, literalValues);
		}

		public PropertyInfo(String rawProperty, FileInfo fileInfo, String key, String rawValue, String[][] values) {
			this(rawProperty, fileInfo, key);
			this.rawValue = rawValue;
			this.valueReferenceArrays = values;
		}

		public PropertyInfo(String rawProperty, FileInfo fileInfo, String key, String[] literalValues) {
			this(rawProperty, fileInfo, key, escapeValues(literalValues), literalValues);
		}

		public PropertyInfo(String rawProperty, FileInfo fileInfo, String key, String[][] values) {
			this(rawProperty, fileInfo, key, escapeValueReferenceArrays(values), values);
		}

		public PropertyInfo(String rawProperty, FileInfo fileInfo, String key, String rawValue, String value) {
			this(rawProperty, fileInfo, key, rawValue, new String[][] { { value } });
			this.literalValue = value;
		}

		public String getInterpolatedValue(Set<PropertyInfo> recursion) {
			if(literalValue != null)
				return literalValue;
			if(valueReferenceArrays == null || valueReferenceArrays.length == 0)
				return null;
			if(valueReferenceArrays.length == 1)
				return interpolatedValue(valueReferenceArrays[0], recursion);
			else {
				StringBuilder sb = new StringBuilder();
				sb.append(interpolatedValue(valueReferenceArrays[0], recursion));
				for(int i = 0; i < valueReferenceArrays.length; i++)
					sb.append(',').append(interpolatedValue(valueReferenceArrays[i], recursion));
				return sb.toString();
			}
		}

		public String[] getInterpolatedValues(Set<PropertyInfo> recursion) {
			if(valueReferenceArrays == null || valueReferenceArrays.length == 0)
				return null;
			String[] interpolatedValues = new String[valueReferenceArrays.length];
			for(int i = 0; i < valueReferenceArrays.length; i++)
				interpolatedValues[i] = interpolatedValue(valueReferenceArrays[i], recursion);
			return interpolatedValues;
		}

		public String getRawValue() {
			return rawValue;
		}

		public void setValue(Object newValue) {
			fileInfo.lock();
			try {
				if(newValue == null) {
					setLiteralValues(null, null);
				} else if(ConvertUtils.isCollectionType(newValue.getClass()))
					setLiteralValues(ConvertUtils.convertSafely(String[].class, newValue, null));
				else
					setLiteralValue(ConvertUtils.getStringSafely(newValue));
				this.saved = false;
				fileInfo.markAsUnsaved();
			} finally {
				fileInfo.unlock();
			}
		}

		protected void setLiteralValue(String literalValue) {
			setLiteralValues(escapeValue(literalValue), new String[] { literalValue });
			this.literalValue = literalValue;
		}

		protected void setLiteralValues(String[] literalValues) {
			setLiteralValues(escapeValues(literalValues), literalValues);
		}

		protected void setLiteralValues(String rawValue, String[] literalValues) {
			this.rawValue = rawValue;
			if(literalValues == null || literalValues.length == 0)
				this.valueReferenceArrays = null;
			else {
				this.valueReferenceArrays = new String[literalValues.length][];
				for(int i = 0; i < literalValues.length; i++)
					this.valueReferenceArrays[i] = new String[] { literalValues[i] };
				if(literalValues.length == 1)
					literalValue = literalValues[0];
				else
					literalValue = StringUtils.join(literalValues, ",");
			}
		}

		public void markAsSaved() {
			saved = true;
		}

		public String getKey() {
			return key;
		}

		public long writeProperty(Appendable writer) throws IOException {
			writer.append(rawProperty);
			long tot = rawProperty.length();
			if(rawValue != null) {
				writer.append(rawValue);
				tot += rawValue.length();
			}
			return tot;
		}

		public void remove() {
			fileInfo.lock();
			try {
				fileInfo.removePropertyInfo(this);
			} finally {
				fileInfo.unlock();
			}
		}

		public void addValue(Object additionalValue) {
			fileInfo.lock();
			try {
				if(additionalValue == null)
					return;
				else if(ConvertUtils.isCollectionType(additionalValue.getClass())) {
					String[] additionalValues = ConvertUtils.convertSafely(String[].class, additionalValue, null);
					if(additionalValues == null || additionalValues.length == 0)
						return;
					if(this.valueReferenceArrays == null || this.valueReferenceArrays.length == 0)
						setLiteralValues(additionalValues);
					else {
						String[][] tmp = new String[valueReferenceArrays.length + additionalValues.length][];
						System.arraycopy(valueReferenceArrays, 0, tmp, 0, valueReferenceArrays.length);
						for(int i = 0; i < additionalValues.length; i++)
							tmp[i + valueReferenceArrays.length] = new String[] {additionalValues[i]};
						this.valueReferenceArrays = tmp;
					}
				} else {
					String av = ConvertUtils.getStringSafely(additionalValue);
					if(this.valueReferenceArrays == null || this.valueReferenceArrays.length == 0)
						setLiteralValue(av);
					else {
						String[][] tmp = new String[valueReferenceArrays.length + 1][];
						System.arraycopy(valueReferenceArrays, 0, tmp, 0, valueReferenceArrays.length);
						tmp[valueReferenceArrays.length] = new String[] { av };
						this.valueReferenceArrays = tmp;
					}
				}
				this.saved = false; 
				fileInfo.markAsUnsaved();				
			} finally {
				fileInfo.unlock();
			}
		}

		public Object getInterpolatedObject(Set<PropertyInfo> recursion) {
			if(valueReferenceArrays == null || valueReferenceArrays.length == 0)
				return null;
			if(valueReferenceArrays.length == 1)
				return interpolatedValue(valueReferenceArrays[0], recursion);
			String[] interpolatedValues = new String[valueReferenceArrays.length];
			for(int i = 0; i < valueReferenceArrays.length; i++)
				interpolatedValues[i] = interpolatedValue(valueReferenceArrays[i], recursion);
			return interpolatedValues;
		}
	}

	protected static class FileInfo extends ReentrantLock {
		private static final long serialVersionUID = -1981723488139377583L;
		protected final File file;
		protected final List<PropertyInfo> propertyInfos = new ArrayList<PropertyInfo>();
		protected String newline = "\n";
		protected String remainder;
		protected boolean saved = true;
		protected long lastLoaded;

		public FileInfo(File file) {
			this.file = file;
		}

		public void markAsUnsaved() {
			saved = false;
		}

		public void addPropertyInfo(PropertyInfo propertyInfo) {
			propertyInfos.add(propertyInfo);
		}

		public void removePropertyInfo(PropertyInfo propertyInfo) {
			int index = propertyInfos.lastIndexOf(propertyInfo);
			if(index >= 0) {
				propertyInfos.remove(index);
				markAsUnsaved();
			}
		}

		public long save(Appendable writer) throws IOException {
			long time = System.currentTimeMillis();
			long tot = 0L;
			for(PropertyInfo pi : propertyInfos)
				tot += pi.writeProperty(writer);
			if(remainder != null) {
				writer.append(remainder);
				tot += remainder.length();
			}
			saved = true;
			lastLoaded = time;
			return tot;
		}

		public void clearPropertyInfos(ConcurrentMap<String, PropertyInfo> properties) {
			Iterator<PropertyInfo> iter = propertyInfos.iterator();
			while(iter.hasNext()) {
				PropertyInfo pi = iter.next();
				properties.remove(pi.getKey(), pi);
				iter.remove();
			}
			markAsUnsaved();
		}

		public File getFile() {
			return file;
		}

		public String getRemainder() {
			return remainder;
		}

		public void setRemainder(String endComment) {
			this.remainder = endComment;
		}

		public void checkSave() throws IOException {
			if(file != null && !saved) {
				lock();
				try {
					if(saved)
						return;
					FileWriter writer = new FileWriter(file);
					try {
						save(writer);
					} finally {
						writer.close();
					}
				} finally {
					unlock();
				}
			}
		}

		public boolean hasChanged() {
			return !saved || file.lastModified() > lastLoaded;
		}

		public long getLastLoaded() {
			return lastLoaded;
		}

		public void setLastLoaded(long lastLoaded) {
			this.lastLoaded = lastLoaded;
		}
	}

	protected final ConcurrentMap<String, PropertyInfo> properties = new ConcurrentHashMap<String, PropertyInfo>(16, 0.75f, 1);
	protected final Cache<File, FileInfo, RuntimeException> files = new LockSegmentCache<File, FileInfo, RuntimeException>() {
		protected FileInfo createValue(File key, Object... additionalInfo) throws RuntimeException {
			return new FileInfo(key);
		}
	};
	protected boolean ignoreUnparseableLines = true;
	protected boolean includedUpdated = false;
	protected String include = "include";
	protected File file;
	protected FileSystem fileSystem = FileSystem.getDefaultFileSystem();
	protected Set<String> keys;
	protected boolean autoSave = false;
	protected String fileName;
	protected String basePath;
	protected String encoding;
	protected ReloadingStrategy reloadingStrategy;

	public void setFileSystem(FileSystem fileSystem) {
		if(fileSystem == null)
			throw new NullPointerException("A valid FileSystem must be specified");
		this.fileSystem = fileSystem;
	}

	public void resetFileSystem() {
		this.fileSystem = FileSystem.getDefaultFileSystem();
	}

	public FileSystem getFileSystem() {
		return this.fileSystem;
	}

	protected String interpolatedValue(String[] valueReferenceArray, Set<PropertyInfo> recursion) {
		switch(valueReferenceArray.length) {
			case 0:
				return "";
			case 1:
				return valueReferenceArray[0];
			default:
				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < valueReferenceArray.length; i++) {
					if(!StringUtils.isBlank(valueReferenceArray[i])) {
						if((i % 2) == 0)
							sb.append(valueReferenceArray[i]);
						else {
							PropertyInfo pi = properties.get(valueReferenceArray[i]);
							if(pi == null)
								sb.append("${").append(valueReferenceArray[i]).append('}');
							else {
								if(recursion == null)
									recursion = new HashSet<PropertyInfo>();
								if(recursion.add(pi))
									sb.append(pi.getInterpolatedValue(recursion));
								else
									// invalid because of recursion
									sb.append("${").append(valueReferenceArray[i]).append('}');
							}
						}
					}
				}
				return sb.toString();
		}
	}
	public void load(File file) throws ConfigurationException {
		if(file == null)
			throw new ConfigurationException("No file specified");
		try {
			final FileInfo fileInfo = files.getOrCreate(file);
			fileInfo.lock();
			try {
				if(this.file == null)
					setFile(file);
				read(fileInfo);
			} finally {
				fileInfo.unlock();
			}
		} catch(IOException e) {
			throw new ConfigurationException(e);
		}
	}

	protected void read(final FileInfo fileInfo) throws ConfigurationException, IOException {
		long time = System.currentTimeMillis();
		EnhancedBufferedReader reader = new EnhancedBufferedReader(new FileReader(fileInfo.getFile()));
		try {
			fileInfo.clearPropertyInfos(properties);
			int n = 1;
			Parser parser = new Parser(fileInfo);
			boolean nl = false;
			int ch;
			while((ch = reader.read()) >= 0) {
				parser.raw((char) ch);
				switch(ch) {
					case '\r':
						if(nl)
							nl = false;
						else {
							parser.newline((char) ch);
							n++;
						}
						break;
					case '\n':
						nl = true;
						parser.newline((char) ch);
						n++;
						break;
					case ' ':
					case '\t':
						nl = false;
						parser.whitespace((char) ch);
						break;
					case '\\':
						nl = false;
						if((ch = reader.read()) < 0) {
							if(!isIgnoreUnparseableLines())
								throw new ConfigurationException("Unparseable at line " + n + "; char " + reader.getReadCount());
						} else {
							switch(ch) {
								case '\n':
									nl = true;
								case '\r':
									// continuation
									n++;
									break;
								case 't':
									parser.interpretted('\t');
									break;
								case 'b':
									parser.interpretted('\b');
									break;
								case 'n':
									parser.interpretted('\n');
									break;
								case 'f':
									parser.interpretted('\f');
									break;
								case 'r':
									parser.interpretted('\r');
									break;
								case 'u':
									char[] unicode = new char[4];
									for(int i = 0; i < 4; i++) {
										ch = reader.read();
										if(ch < 0)
											throw new ConfigurationException("Unfinished unicode escape at line " + n + "; char " + reader.getReadCount());
										parser.raw((char) ch);
										unicode[i] = (char) ch;
									}
									parser.interpretted((char) Integer.parseInt(new String(unicode), 16));
									break;
								default:
									parser.interpretted((char) ch);
									break;
							}
							parser.raw((char) ch);
						}
						break;
					case '#':
					case '!':
						nl = false;
						parser.comment((char) ch);
						break;
					case ':':
					case '=':
						nl = false;
						parser.separator((char) ch);
						break;
					case ',':
						nl = false;
						parser.list((char) ch);
						break;
					case '$':
						nl = false;
						parser.dollar((char) ch);
						break;
					case '{':
						nl = false;
						parser.openBrace((char) ch);
						break;
					case '}':
						nl = false;
						parser.closeBrace((char) ch);
						break;
					default:
						nl = false;
						parser.interpretted((char) ch);
						break;
				}
			}
			parser.finish();
		} finally {
			reader.close();
		}
		fileInfo.setLastLoaded(time);
	}

	protected boolean isInclude(String key) {
		return !StringUtils.isBlank(getInclude()) && key.equalsIgnoreCase(getInclude());
	}

	protected void loadIncludeFile(String fileName) throws ConfigurationException {
		URL url = ConfigurationUtils.locate(getFileSystem(), getBasePath(), fileName);
		if(url == null)
			throw new ConfigurationException("Cannot resolve include file " + fileName);
		File file = ConfigurationUtils.fileFromURL(url);
		if(file == null)
			throw new ConfigurationException("Cannot find file for " + url);

		load(file);
	}

	@Override
	public void load(URL url) throws ConfigurationException {
		File file = ConfigurationUtils.fileFromURL(url);
		if(file != null)
			load(file);
		else
			throw new ConfigurationException("You must use a file-backed url");
	}

	@Override
	public void load(Reader in) throws ConfigurationException {
		throw new ConfigurationException("This method is not supported. Use load(File file) or load(String fileName) or load(URL url) instead");
	}

	@Override
	public void save(Writer out) throws ConfigurationException {
		File file = getFile();
		FileInfo fileInfo;
		if(file == null) {
			Iterator<FileInfo> iter = files.values().iterator();
			if(!iter.hasNext())
				return;
			fileInfo = iter.next();
		} else
			fileInfo = files.getIfPresent(file);
		if(fileInfo == null)
			return;
		fileInfo.lock();
		try {
			fileInfo.save(out);
			out.flush();
		} catch(IOException e) {
			throw new ConfigurationException(e);
		} finally {
			fileInfo.unlock();
		}
	}

	@Override
	public void save(File file) throws ConfigurationException {
		if(file == null)
			throw new ConfigurationException("No file specified");
		try {
			FileInfo fileInfo = files.getIfPresent(file);
			if(fileInfo == null) {
				if(this.file != null)
					fileInfo = files.getIfPresent(this.file);
				Iterator<FileInfo> iter = files.values().iterator();
				if(!iter.hasNext())
					return;
				fileInfo = iter.next();
				if(fileInfo == null)
					return;
			}
			fileInfo.lock();
			try {
				Writer out = new FileWriter(file);
				try {
					fileInfo.save(out);
					out.flush();
				} finally {
					out.close();
				}
			} finally {
				fileInfo.unlock();
			}
		} catch(IOException e) {
			throw new ConfigurationException(e);
		}
	}

	@Override
	public void save(URL url) throws ConfigurationException {
		File file = ConfigurationUtils.fileFromURL(url);
		if(file != null)
			save(file);
		else
			throw new ConfigurationException("You must use a file-backed url");
	}

	public boolean isIgnoreUnparseableLines() {
		return ignoreUnparseableLines;
	}

	public void setIgnoreUnparseableLines(boolean ignoreUnparseableLines) {
		this.ignoreUnparseableLines = ignoreUnparseableLines;
	}

	protected static String escapeKey(String key) {
		if(key == null)
			return "";
		return escapeKey(new StringBuilder(), key).toString();
	}

	protected static String escapeValueReferenceArrays(String[][] valueReferenceArrays) {
		if(valueReferenceArrays == null || valueReferenceArrays.length == 0)
			return "";
		StringBuilder sb = new StringBuilder();
		escapeValueReferenceArray(sb, valueReferenceArrays[0]);
		for(int i = 1; i < valueReferenceArrays.length; i++) {
			sb.append(',');
			escapeValueReferenceArray(sb, valueReferenceArrays[i]);
		}

		return sb.toString();
	}

	protected static void escapeValueReferenceArray(StringBuilder sb, String[] valueReferenceArray) {
		for(int i = 0; i < valueReferenceArray.length; i++) {
			if(!StringUtils.isBlank(valueReferenceArray[i])) {
				if((i % 2) == 0)
					escapeValue(sb, valueReferenceArray[i]);
				else {
					sb.append("${");
					escapeValue(sb, valueReferenceArray[i]);
					sb.append('}');
				}
			}
		}
	}

	protected static String escapeValue(Object value) {
		if(value == null)
			return "";
		return escapeValue(new StringBuilder(), value).toString();
	}

	protected static StringBuilder escapeValue(StringBuilder sb, Object value) {
		if(value != null) {
			if(ConvertUtils.isCollectionType(value.getClass())) {
				// NOTE: n-dimensional collections get flattened
				boolean first = true;
				try {
					for(Object o : ConvertUtils.asCollection(value)) {
						if(first)
							first = false;
						else
							sb.append(',');
						escapeValue(sb, o);
					}
					return sb;
				} catch(ConvertException e) {
					// do nothing
				}
			} else {
				return escapeValue(sb, ConvertUtils.getStringSafely(value));
			}
		}
		return sb;
	}

	protected static String escapeValues(String[] values) {
		if(values == null || values.length == 0)
			return "";
		return escapeValue(new StringBuilder(), values).toString();
	}

	protected static StringBuilder escapeValues(StringBuilder sb, String[] values) {
		if(values != null) {
			for(int i = 0; i < values.length; i++) {
				if(i > 0)
					sb.append(',');
				escapeValue(sb, values[i]);
			}
		}
		return sb;
	}

	protected static StringBuilder escapeValue(StringBuilder sb, String svalue) {
		if(svalue != null)
			for(int i = 0; i < svalue.length(); i++) {
				char ch = svalue.charAt(i);
				switch(ch) {
					case '\n':
						sb.append('\\').append('n');
						break;
					case '\r':
						sb.append('\\').append('r');
						break;
					case '\t':
						sb.append('\\').append('t');
						break;
					case '\b':
						sb.append('\\').append('b');
						break;
					case '\f':
						sb.append('\\').append('f');
						break;
					case '\\':
					case ',':
					case '$':
						sb.append('\\').append(ch);
						break;
					case ' ':
						if(i == 0 || i + 1 >= svalue.length())
							sb.append('\\');
					default:
						sb.append(ch);
				}
			}
		return sb;
	}

	protected static StringBuilder escapeKey(StringBuilder sb, String key) {
		if(key != null)
			for(int i = 0; i < key.length(); i++) {
				char ch = key.charAt(i);
				switch(ch) {
					case '\n':
						sb.append('\\').append('n');
						break;
					case '\r':
						sb.append('\\').append('r');
						break;
					case '\t':
						sb.append('\\').append('t');
						break;
					case '\b':
						sb.append('\\').append('b');
						break;
					case '\f':
						sb.append('\\').append('f');
						break;
					case '\\':
					case '=':
					case ':':
						sb.append('\\').append(ch);
						break;
					case ' ':
						if(i + 1 >= key.length())
							sb.append('\\');
					default:
						sb.append(ch);
				}
			}
		return sb;
	}

	protected void checkSave() {
		if(isAutoSave()) {
			for(FileInfo fi : files.values())
				try {
					fi.checkSave();
				} catch(IOException e) {
					log.warn("Could not save configuration file '" + fi.getFile().getAbsolutePath(), e);
				}
		}
	}

	protected String getNewline() {
		return "\n";
	}

	protected String getSeparator() {
		return "=";
	}

	public String getInclude() {
		return include;
	}

	public void setInclude(String include) {
		this.include = include;
	}

	@Override
	public Configuration subset(String prefix) {
		return new SubsetConfiguration(this, prefix, ".");
	}

	@Override
	public boolean isEmpty() {
		return properties.isEmpty();
	}

	@Override
	public boolean containsKey(String key) {
		return properties.containsKey(key);
	}

	@Override
	public void addProperty(String key, Object value) {
		PropertyInfo pi = properties.get(key);
		if(pi == null) {
			pi = new PropertyInfo(getNewline() + key + getSeparator(), files.getOrCreate(file), key);
			PropertyInfo oldPi = properties.putIfAbsent(key, pi);
			if(oldPi != null) {
				pi.remove();
				pi = oldPi;
			}
		}
		pi.addValue(value);
		checkSave();
	}

	@Override
	public void setProperty(String key, Object value) {
		key = key.trim();
		PropertyInfo pi = properties.get(key);
		if(pi == null || (!isIncludedUpdated() && !ConvertUtils.areEqual(pi.fileInfo.file, file))) {
			pi = new PropertyInfo(getNewline() + escapeKey(key) + getSeparator(), files.getOrCreate(file), key);
			PropertyInfo oldPi = properties.putIfAbsent(key, pi);
			if(oldPi != null && (isIncludedUpdated() || ConvertUtils.areEqual(oldPi.fileInfo.file, file))) {
				pi.remove();
				pi = oldPi;
			}
		}
		pi.setValue(value);
		checkSave();
	}


	@Override
	public void clearProperty(String key) {
		PropertyInfo pi = properties.remove(key);
		if(pi != null)
			pi.remove();
	}

	@Override
	public void clear() {
		Iterator<PropertyInfo> iter = properties.values().iterator();
		while(iter.hasNext()) {
			iter.next().remove();
			iter.remove();
		}
	}

	@Override
	public Object getProperty(String key) {
		PropertyInfo pi = properties.get(key);
		return pi == null ? null : pi.getInterpolatedObject(null);
	}

	@Override
	public Iterator<String> getKeys(final String prefix) {
		return new FilteredIterator<String, String>(getKeys()) {
			@Override
			protected String convert(String element) {
				return element;
			}

			@Override
			protected boolean accept(String element) {
				return element != null && element.startsWith(prefix);
			}
		};
	}

	@Override
	public Iterator<String> getKeys() {
		if(keys == null)
			keys = Collections.unmodifiableSet(properties.keySet());
		return keys.iterator();
	}

	@Override
	public Properties getProperties(String key) {
		String[] tokens = getStringArray(key);
		Properties props = new Properties();
		for(String token : tokens) {
			int equalSign = token.indexOf('=');
			if(equalSign > 0) {
				String pkey = token.substring(0, equalSign).trim();
				String pvalue = token.substring(equalSign + 1).trim();
				props.put(pkey, pvalue);
			} else if(tokens.length == 1 && "".equals(token)) {
				// Semantically equivalent to an empty Properties
				// object.
				break;
			} else {
				throw new IllegalArgumentException('\'' + token + "' does not contain an equals sign");
			}
		}
		return props;
	}

	@Override
	public boolean getBoolean(String key) {
		try {
			return ConvertUtils.getBoolean(getString(key));
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public boolean getBoolean(String key, boolean defaultValue) {
		try {
			return ConvertUtils.getBoolean(getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public Boolean getBoolean(String key, Boolean defaultValue) {
		try {
			return ConvertUtils.convertDefault(Boolean.class, getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public byte getByte(String key) {
		try {
			return ConvertUtils.getByte(getString(key));
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public byte getByte(String key, byte defaultValue) {
		try {
			return ConvertUtils.getByte(getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public Byte getByte(String key, Byte defaultValue) {
		try {
			return ConvertUtils.convertDefault(Byte.class, getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public double getDouble(String key) {
		try {
			return ConvertUtils.getDouble(getString(key));
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public double getDouble(String key, double defaultValue) {
		try {
			return ConvertUtils.getDouble(getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public Double getDouble(String key, Double defaultValue) {
		try {
			return ConvertUtils.convertDefault(Double.class, getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public float getFloat(String key) {
		try {
			return ConvertUtils.getFloat(getString(key));
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public float getFloat(String key, float defaultValue) {
		try {
			return ConvertUtils.getFloat(getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public Float getFloat(String key, Float defaultValue) {
		try {
			return ConvertUtils.convertDefault(Float.class, getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public int getInt(String key) {
		try {
			return ConvertUtils.getInt(getString(key));
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public int getInt(String key, int defaultValue) {
		try {
			return ConvertUtils.getInt(getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public Integer getInteger(String key, Integer defaultValue) {
		try {
			return ConvertUtils.convertDefault(Integer.class, getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public long getLong(String key) {
		try {
			return ConvertUtils.getLong(getString(key));
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public long getLong(String key, long defaultValue) {
		try {
			return ConvertUtils.getLong(getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public Long getLong(String key, Long defaultValue) {
		try {
			return ConvertUtils.convertDefault(Long.class, getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public short getShort(String key) {
		try {
			return ConvertUtils.getShort(getString(key));
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public short getShort(String key, short defaultValue) {
		try {
			return ConvertUtils.getShort(getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public Short getShort(String key, Short defaultValue) {
		try {
			return ConvertUtils.convertDefault(Short.class, getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public BigDecimal getBigDecimal(String key) {
		try {
			return ConvertUtils.convert(BigDecimal.class, getString(key));
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public BigDecimal getBigDecimal(String key, BigDecimal defaultValue) {
		try {
			return ConvertUtils.convertDefault(BigDecimal.class, getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public BigInteger getBigInteger(String key) {
		try {
			return ConvertUtils.convert(BigInteger.class, getString(key));
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public BigInteger getBigInteger(String key, BigInteger defaultValue) {
		try {
			return ConvertUtils.convertDefault(BigInteger.class, getString(key), defaultValue);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
	}

	@Override
	public String getString(String key) {
		PropertyInfo pi = properties.get(key);
		return pi == null ? null : pi.getInterpolatedValue(null);
	}

	@Override
	public String getString(String key, String defaultValue) {
		String value = getString(key);
		return value == null ? defaultValue : value;
	}

	@Override
	public String[] getStringArray(String key) {
		PropertyInfo pi = properties.get(key);
		return pi == null ? null : pi.getInterpolatedValues(null);
	}

	@Override
	public List<Object> getList(String key) {
		Object[] values = getStringArray(key);
		return values == null ? null : Arrays.asList(values);
	}

	@Override
	public List<Object> getList(String key, List<Object> defaultValue) {
		List<Object> value = getList(key);
		return value == null ? defaultValue : value;
	}

	public boolean isAutoSave() {
		return autoSave;
	}

	public void setAutoSave(boolean autoSave) {
		this.autoSave = autoSave;
	}

	@Override
	public void load() throws ConfigurationException {
		load(getFile());
	}

	@Override
	public void load(String fileName) throws ConfigurationException {
		load(ConfigurationUtils.getFile(getBasePath(), fileName));
	}

	@Override
	public void load(InputStream in) throws ConfigurationException {
		throw new ConfigurationException("This method is not supported. Use load(File file) or load(String fileName) or load(URL url) instead");
	}

	@Override
	public void load(InputStream in, String encoding) throws ConfigurationException {
		throw new ConfigurationException("This method is not supported. Use load(File file) or load(String fileName) or load(URL url) instead");
	}

	@Override
	public void save() throws ConfigurationException {
		save(getFile());
	}

	@Override
	public void save(String fileName) throws ConfigurationException {
		save(ConfigurationUtils.getFile(getBasePath(), fileName));
	}

	@Override
	public void save(OutputStream out) throws ConfigurationException {
		save(new OutputStreamWriter(out));
	}

	@Override
	public void save(OutputStream out, String encoding) throws ConfigurationException {
		try {
			save(new OutputStreamWriter(out, encoding));
		} catch(UnsupportedEncodingException e) {
			throw new ConfigurationException(e);
		}
	}

	@Override
	public String getFileName() {
		return fileName;
	}

	@Override
	public void setFileName(String fileName) {
		this.fileName = fileName;
		this.file = null;
	}

	@Override
	public String getBasePath() {
		return basePath == null ? "" : basePath;
	}

	@Override
	public void setBasePath(String basePath) {
		this.basePath = basePath;
		this.file = null;
	}

	@Override
	public File getFile() {
		if(file == null && !StringUtils.isBlank(getFileName()))
			file = ConfigurationUtils.getFile(getBasePath(), getFileName());
		return file;
	}

	@Override
	public void setFile(File file) {
		this.file = file;
		if(file != null) {
			this.fileName = file.getName();
			this.basePath = (file.getParentFile() != null) ? file.getParentFile().getAbsolutePath() : null;
		}
	}

	@Override
	public URL getURL() {
		File f = getFile();
		try {
			return f == null ? null : f.toURI().toURL();
		} catch(MalformedURLException e) {
			return null;
		}
	}

	@Override
	public void setURL(URL url) {
		setFile(ConfigurationUtils.fileFromURL(url));
	}

	@Override
	public ReloadingStrategy getReloadingStrategy() {
		return reloadingStrategy;
	}

	@Override
	public void setReloadingStrategy(ReloadingStrategy strategy) {
		this.reloadingStrategy = strategy;
	}

	@Override
	public void reload() {
		ReloadingStrategy rs = getReloadingStrategy();
		if(rs != null && !rs.reloadingRequired())
			return;
		for(FileInfo fileInfo : files.values()) {
			fileInfo.lock();
			try {
				if(!fileInfo.hasChanged())
					read(fileInfo);
			} catch(ConfigurationException e) {
				log.warn("Could not reload file " + fileInfo.getFile().getAbsolutePath(), e);
			} catch(IOException e) {
				log.warn("Could not reload file " + fileInfo.getFile().getAbsolutePath(), e);
			} finally {
				fileInfo.unlock();
			}
		}
		if(rs != null)
			rs.reloadingPerformed();
	}

	@Override
	public String getEncoding() {
		return encoding;
	}

	@Override
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(FileInfo fileInfo : files.values()) {
			sb.append('[').append(fileInfo.getFile().getPath()).append("]\n");
			for(PropertyInfo pi : fileInfo.propertyInfos) {
				sb.append(pi.getKey()).append('=').append(pi.getInterpolatedValue(null)).append('\n');
			}
			sb.append('\n');
		}
		return sb.toString();
	}

	public boolean isIncludedUpdated() {
		return includedUpdated;
	}

	public void setIncludedUpdated(boolean includedUpdated) {
		this.includedUpdated = includedUpdated;
	}
}

