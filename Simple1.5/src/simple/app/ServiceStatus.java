package simple.app;


public enum ServiceStatus {
	INITIALIZING("Thread is not yet running"),
	PAUSED("Thread is paused"),
	DELAYED("Thread is delaying after a queue error"),
	AWAITING_PREREQ("Thread is waiting for all prerequisites to become available"),
	AWAITING_NEXT("Thread is awaiting next message from the queue"),
	PROCESSING_MSG("Thread is processing a message from the queue"),
	ACKNOWLEDGING_MSG("Thread is acknowledging a message from the queue"),
	NACKING_MSG("Thread is rolling back a message from the queue because of a processing exception"),
	TIMEDOUT_MSG("Thread is rolling back a message from the queue because of a processing timeout"),
	DONE("Thread has finished running"),
	HANDLING_ERROR("Thread is handling a queue error"),

	STARTING("Service is starting up"),
	STARTED("Service is running"),
	STOPPING("Service is shutting down"),
	STOPPED("Service is shut down"),

	OTHER("Other");

	protected final String description;

	private ServiceStatus(String description) {
		this.description = description;
	}
	public String getDescription() {
		return description;
	}
}