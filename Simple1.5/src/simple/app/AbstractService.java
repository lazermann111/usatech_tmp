package simple.app;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import simple.util.MapBackedSet;
import simple.util.concurrent.ResultFuture;
import simple.util.concurrent.TrivialFuture;

public abstract class AbstractService implements Service {
	protected final Set<ServiceStatusListener> serviceStatusListeners = new MapBackedSet<ServiceStatusListener>(new ConcurrentHashMap<ServiceStatusListener, Object>()); // new
	protected final String serviceName;
	protected final static AtomicInteger lastThreadId = new AtomicInteger();

	public AbstractService(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceName() {
		return serviceName;
	}

	protected String getNewThreadName() {
		return getServiceName() + " Thread #" + lastThreadId.incrementAndGet();
	}

	protected ServiceStatusInfo[] getCurrentServiceStatusInfos() {
		return null;
	}

	public boolean addServiceStatusListener(ServiceStatusListener listener) {
		boolean changed = serviceStatusListeners.add(listener);
		if(changed) {
			ServiceStatusInfo[] ssis = getCurrentServiceStatusInfos();
			if(ssis != null)
				for(ServiceStatusInfo ssi : ssis)
					listener.serviceStatusChanged(ssi);
		}
		return changed;
	}

	public boolean removeServiceStatusListener(ServiceStatusListener listener) {
		return serviceStatusListeners.remove(listener);
	}

	protected void fireServiceStatusChanged(Thread thread, ServiceStatus serviceStatus, long iterationCount, Long queueLatency) {
		if(serviceStatusListeners.isEmpty())
			return;
		ServiceStatusInfo info = new ServiceStatusInfo();
		info.setIterationCount(iterationCount);
		info.setQueueLatency(queueLatency);
		info.setService(this);
		info.setServiceStatus(serviceStatus);
		info.setThread(thread);
		for(ServiceStatusListener l : serviceStatusListeners) {
			l.serviceStatusChanged(info);
		}
	}

	@Override
	public Future<Integer> stopThreads(final int numThreads, final boolean force) throws ServiceException {
		if(numThreads <= 0)
			return new TrivialFuture<Integer>(0);
		return new ResultFuture<Integer>() {
			@Override
			protected Integer produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				try {
					return stopThreads(numThreads, force, unit.toMillis(timeout));
				} catch(ServiceException e) {
					throw new ExecutionException(e);
				}
			}
		};
	}

	@Override
	public Future<Integer> stopAllThreads(final boolean force) throws ServiceException {
		return new ResultFuture<Integer>() {
			@Override
			protected Integer produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				try {
					return stopAllThreads(force, unit.toMillis(timeout));
				} catch(ServiceException e) {
					throw new ExecutionException(e);
				}
			}
		};
	}

	@Override
	public Future<Boolean> prepareShutdown() throws ServiceException {
		return WorkQueue.TRUE_FUTURE;
	}

	/**
	 * @see simple.app.Service#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() throws ServiceException {
		final Future<Integer> shutdownFuture = stopAllThreads(false);
		return new ResultFuture<Boolean>() {
			protected Boolean produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				shutdownFuture.get(timeout, unit);
				return Boolean.TRUE;
			}
		};
	}
}
