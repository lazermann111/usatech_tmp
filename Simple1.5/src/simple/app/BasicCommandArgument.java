/**
 *
 */
package simple.app;

/**
 * @author Brian S. Krug
 *
 */
public class BasicCommandArgument implements CommandArgument {
	protected final String name ;
	protected final Class<?> type;
	protected final String description;
	protected final boolean optional;

	public BasicCommandArgument(String name, Class<?> type, String description, boolean optional) {
		super();
		this.name = name;
		this.type = type;
		this.description = description;
		this.optional = optional;
	}
	public String getName() {
		return name;
	}
	public Class<?> getType() {
		return type;
	}
	public String getDescription() {
		return description;
	}
	public boolean isOptional() {
		return optional;
	}
}
