package simple.app;

public class ServiceStatusInfo {
	protected Service service;
	protected Thread thread;
	protected ServiceStatus serviceStatus;
	protected long iterationCount;
	protected Long queueLatency;

	public ServiceStatusInfo() {
	}
	
	public ServiceStatusInfo(Service service, Thread thread, ServiceStatus serviceStatus, long iterationCount, Long queueLatency) {
		this.service = service;
		this.thread = thread;
		this.serviceStatus = serviceStatus;
		this.iterationCount = iterationCount;
		this.queueLatency = queueLatency;
	}

	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
	public Thread getThread() {
		return thread;
	}
	public void setThread(Thread thread) {
		this.thread = thread;
	}
	public ServiceStatus getServiceStatus() {
		return serviceStatus;
	}
	public void setServiceStatus(ServiceStatus serviceStatus) {
		this.serviceStatus = serviceStatus;
	}
	public long getIterationCount() {
		return iterationCount;
	}
	public void setIterationCount(long iterationCount) {
		this.iterationCount = iterationCount;
	}
	public Long getQueueLatency() {
		return queueLatency;
	}
	public void setQueueLatency(Long queueLatency) {
		this.queueLatency = queueLatency;
	}
}
