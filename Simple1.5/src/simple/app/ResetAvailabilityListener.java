package simple.app;

public interface ResetAvailabilityListener {
	public void resetAvailability(Prerequisite prerequisite);
}
