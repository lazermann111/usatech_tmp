package simple.app;

import java.util.Map;

/** This interface represents a piece of work to process.
 * @author Brian S. Krug
 *
 * @param <Q> The class of the object returned from {@link #getBody()}
 */
public interface Work<Q> {
	/** The time in milliseconds past epoch that the message was enqueued. Return 0 to indicate unknown
	 * @return
	 */
	public long getEnqueueTime() ;
	/** The details of this piece of work
	 * @return the details of the Work
	 */
	public Q getBody();
	/** Completes or commits the work, generally this removes the work from the WorkQueue
	 * @throws WorkQueueException If the work cannot be committed
	 */
	public void workComplete() throws WorkQueueException;
	/** Aborts or rolls back the work, generally putting the work back into the queue for retry later
	 * or into an error bin.
	 * @param retryType What to do this with Work
	 * @param unguardedRetryAllowed Whether a retry of this Work can proceed as if it was a first time attempt
	 * @param cause Why the work was aborted
	 * @throws WorkQueueException If the work cannot be rolled back
	 */
	public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException;
	/** A publisher that will put new pieces of work into the queue-system
	 * from which this piece of work was obtained. Generally, calls to {@link #workComplete()}
	 * will also commit any work published with this publisher.
	 * @return A publisher or <code>null</code> if not supported
	 * @throws WorkQueueException
	 */
	public Publisher<Q> getPublisher() throws WorkQueueException;
	/** If {@link #workComplete()} or {@link #workAborted(Throwable)} has been called
	 * @return <code>true</code> if and only if {@link #workComplete()} or {@link #workAborted(Throwable)} has been called
	 */
	public boolean isComplete() ;

	/** If this work is re-delivered or not
	 * @return <code>true</code> if and only if there is a possibility that this piece of work was partially processed already
	 */
	public boolean isRedelivered() ;
	
	/** If this work can be retried as if it had never been attempted before
	 * @return <code>true</code> if and only if it is absolutely safe to treat this as a first time message
	 */
	public boolean isUnguardedRetryAllowed() ;
	
	/** The number of times this Work was previously re-tried or -1 if not known
	 * @return
	 */
	public int getRetryCount() ;
	
	/** The name of the queue or null if this information is not available
	 * @return
	 */
	public String getQueueName() ;
	
	/** The name of the original queue or null if this information is not available
	 * @return
	 */
	public String getOriginalQueueName() ;
	
	/** A set of properties representing "header" information
	 * @return
	 */
	public Map<String,?> getWorkProperties() ;
	
	public QoS getQoS() ;

	public String getGuid();

	public void unblock(String... blocksToClear);

	public String getSubscription();
}
