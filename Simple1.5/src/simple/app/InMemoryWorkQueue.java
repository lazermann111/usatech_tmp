/**
 *
 */
package simple.app;

import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import simple.event.ProgressListener;
import simple.io.Log;

/**
 * @author Brian S. Krug
 *
 */
public class InMemoryWorkQueue<J> extends AbstractWorkQueue<J> implements Publisher<J> {
	private static final Log log = Log.getLog();
	protected final Class<J> bodyType;
	protected BlockingQueue<Work<J>> queue;
	protected DelayQueue<InMemoryWork> retryQueue;
	protected boolean blockingRetryAllowed = false;
	protected final AtomicInteger orderGenerator = new AtomicInteger();
	protected int capacity;
	protected static QoS DEFAULT_QOS = new QoS() {
		public long getExpiration() {
			return 0;
		}
		public Map<String, ?> getMessageProperties() {
			return null;
		}
		public int getPriority() {
			return 0;
		}
		public boolean isPersistent() {
			return false;
		}
		public long getTimeToLive() {
			return 0;
		}
		public long getAvailable() {
			return 0;
		}
	};
	protected class InMemoryWork implements Work<J>, Delayed {
		protected final J body;
		protected boolean complete = false;
		protected int attempts = 0;
		protected long availableAfter;
		protected final int order;
		protected final long enqueueTime;
		protected boolean unguardedRetryAllowed = true;
		public InMemoryWork(J body) {
			this.body = body;
			this.order = orderGenerator.incrementAndGet();
			this.enqueueTime = System.currentTimeMillis();
		}
		public QoS getQoS() {
			return DEFAULT_QOS;
		}
		public Map<String, ?> getWorkProperties() {
			return null;
		}
		public long getEnqueueTime() {
			return enqueueTime;
		}

		public String getGuid() {
			return String.valueOf(order);
		}
		/**
		 * @see simple.app.Work#getBody()
		 */
		public J getBody() {
			return body;
		}
		/**
		 * @see simple.app.Work#isComplete()
		 */
		public boolean isComplete() {
			return complete;
		}
		/**
		 * @see simple.app.Work#isRedelivered()
		 */
		public boolean isRedelivered() {
			return attempts > 0;
		}
		public int getRetryCount() {
			return attempts;
		}
		public boolean isUnguardedRetryAllowed() {
			return unguardedRetryAllowed;
		}
		/**
		 * @see simple.app.Work#getPublisher()
		 */
		public Publisher<J> getPublisher() throws WorkQueueException {
			return InMemoryWorkQueue.this;
		}
		/**
		 * @see simple.app.Work#workComplete()
		 */
		public void workComplete() throws WorkQueueException {
			if(complete) throw new WorkQueueException("Work is already complete; workComplete() may not be called");
			complete = true;
			//do nothing else
		}
		/**
		 * @see simple.app.Work#workAborted(simple.app.WorkRetryType, boolean, java.lang.Throwable)
		 */
		public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
			if(complete) throw new WorkQueueException("Work is already complete; workAborted() may not be called");
			complete = true;
			RetryPolicy retryPolicy;
			if(cause instanceof ScheduledRetryServiceException)
				retryPolicy = ((ScheduledRetryServiceException) cause).getRetryPolicy();
			else
				retryPolicy = getRetryPolicy();
			if(++attempts > retryPolicy.getMaximumRedeliveries()) {
				log.info("Disgarding work " + body + " after " + attempts + " failures");
			} else {
				this.unguardedRetryAllowed = this.unguardedRetryAllowed && unguardedRetryAllowed;
				switch(retryType) {
					case IMMEDIATE_RETRY:
						try {
							enqueue(this, 10);
						} catch(InterruptedException e) {
							throw new WorkQueueException("Interrupted while adding to queue", e);
						}
						break;
					case NO_RETRY:
						break;
					case BLOCKING_RETRY:
						//Re-check prerequisites
						//XXX: In the future, we may allow the exception to hold info on which prerequisite is unavailable
						prerequisiteManager.resetAvailability();
						if(isBlockingRetryAllowed()) {
							try {
								enqueue(this, 1);
							} catch(InterruptedException e) {
								throw new WorkQueueException("Interrupted while adding to queue", e);
							}
							break;
						}
						log.debug("Blocking retry is not allowed; Re-checking prerequisites and using non-blocking retry");
					case NONBLOCKING_RETRY:
					case SCHEDULED_RETRY:
						availableAfter = System.currentTimeMillis() + retryPolicy.calculateDelay(attempts);
						retryQueue.put(this);
						break;
				}
			}
		}
		public String getOriginalQueueName() {
			return null;
		}
		public String getQueueName() {
			return getQueueKey();
		}
		/**
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(Delayed o) {
			@SuppressWarnings("unchecked")
			InMemoryWork delayed = (InMemoryWork)o;
			if(availableAfter < delayed.availableAfter)
				return -1;
			if(availableAfter > delayed.availableAfter)
				return 1;
			return order - delayed.order;
		}
		/**
		 * @see java.util.concurrent.Delayed#getDelay(java.util.concurrent.TimeUnit)
		 */
		public long getDelay(TimeUnit unit) {
			return unit.convert(availableAfter - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
		}

		public void unblock(String... blocksToClear) {
			// TODO Auto-generated method stub
		}

		public String getSubscription() {
			return null;
		}
	}
	protected final Retriever<J> retriever = new Retriever<J>() {
		/**
		 * @see simple.app.PrerequisiteCheck#arePrequisitesAvailable()
		 */
		public boolean arePrequisitesAvailable() {
			return getPrerequisiteManager().arePrequisitesAvailable();
		}
		/**
		 * @see simple.app.PrerequisiteCheck#cancelPrequisiteCheck(java.lang.Thread)
		 */
		public void cancelPrequisiteCheck(Thread thread) {
			getPrerequisiteManager().cancelPrequisiteCheck(thread);
		}

		public void resetAvailability() {
			getPrerequisiteManager().resetAvailability();
		}

		/**
		 * @see simple.app.WorkQueue.Retriever#next()
		 */
		public Work<J> next() throws WorkQueueException, InterruptedException {
			return dequeue();
		}
		/**
		 * @see simple.app.WorkQueue.Retriever#cancel(java.lang.Thread)
		 */
		public boolean cancel(Thread thread) {
			return cancelDequeue(thread);
		}
		/**
		 * @see simple.app.WorkQueue.Retriever#close()
		 */
		public void close() {
			//do nothing
		}
	};
	/**
	 *
	 */
	public InMemoryWorkQueue(Class<J> bodyType) {
		this.bodyType = bodyType;
		setCapacity(-1);
	}
	/**
	 * @see simple.app.WorkQueue#getRetriever()
	 */
	public Retriever<J> getRetriever(ProgressListener listener) throws WorkQueueException {
		return retriever;
	}

	/**
	 * @see simple.app.WorkQueue#getWorkBodyType()
	 */
	public Class<J> getWorkBodyType() {
		return bodyType;
	}

	/**
	 * @see simple.app.WorkQueue#isAutonomous()
	 */
	public boolean isAutonomous() {
		return true;
	}

	/**
	 * @see simple.app.Publisher#close()
	 */
	public void close() {
		// Do nothing
	}
	/**
	 * @see simple.app.WorkQueue#shutdown()
	 */
	public Future<Boolean> shutdown() {
		return TRUE_FUTURE;
	}

	protected void enqueue(Work<J> work, int priority) throws InterruptedException {
		BlockingQueue<Work<J>> queue = this.queue;
		if(priority <= 0)
			queue.put(work);
		else if(queue instanceof BlockingDeque<?>)
			((BlockingDeque<Work<J>>)queue).putFirst(work);
		else {
			log.debug("Prioritization not supported by the BlockingQueue currently used (" + queue.getClass().getName() + ")");
			queue.put(work);
		}
	}

	protected Work<J> dequeue() {
		try {
			return queue.take();
		} catch(InterruptedException e) {
			return null;
		}
	}
	/**
	 * @param thread
	 * @return
	 */
	protected boolean cancelDequeue(Thread thread) {
		thread.interrupt();
		return true;
	}

	public void publish(String queueKey, boolean multicast, boolean temporary, J content, Long correlation, QoS qos, String... blocks) throws ServiceException {
		//TODO: support QoS Settings
		if(qos != null && qos.getAvailable() > 0 && qos.getAvailable() > System.currentTimeMillis()) {
			throw new ServiceException("Delayed execute is not supported");
		}
		if(queueKey != null && queueKey.trim().length() > 0)
			throw new ServiceException("Publishing to a different queue is not supported for " + this);
		if(multicast)
			throw new ServiceException("Multicast is not supported for " + this);
		try {
			enqueue(new InMemoryWork(content), 0);
		} catch(InterruptedException e) {
			throw new ServiceException("Interrupted while adding to queue", e);
		}
	}
	public void publish(String queueKey, boolean multicast, boolean temporary, J content) throws ServiceException {
		publish(queueKey, multicast, temporary, content, null, null);
	}
	public void publish(String queueKey, J content) throws ServiceException {
		publish(queueKey, false, false, content);
	}
	public boolean isBlockingRetryAllowed() {
		return blockingRetryAllowed;
	}

	public void setBlockingRetryAllowed(boolean blockingRetryAllowed) {
		this.blockingRetryAllowed = blockingRetryAllowed;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		int oldCapacity = this.capacity;
		this.capacity = capacity;
		if(oldCapacity != capacity || queue == null) {
			BlockingQueue<Work<J>> oldQueue = queue;
			if(capacity == 0) {
				queue = new SynchronousQueue<Work<J>>();
			} else if(capacity < 0) {
				queue = new LinkedBlockingDeque<Work<J>>();
			} else { // capacity > 0
				queue = new LinkedBlockingDeque<Work<J>>(capacity);
			}
			oldQueue.drainTo(queue);
		}
	}

	@Override
	public int getConsumerCount(String queueName, boolean temporary) {
		return 1;
	}

	public String[] getSubscriptions(String queueKey) throws ServiceException {
		return null;
	}

	@Override
	public void block(String... blocks) throws ServiceException {
		throw new ServiceException("Blocking is not supported in this publisher implementation");
	}
}
