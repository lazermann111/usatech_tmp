package simple.app;



public interface Publisher<Q> {
	/** Sends a message to a queue system
	 * @param queueKey To whom the message should go. May not be null.
	 * @param content The content of the message. May not be null.
	 * @throws ServiceException
	 */
	public void publish(String queueKey, Q content) throws ServiceException;
	
	/**
	 * Sends a message to a queue system
	 * 
	 * @param queueKey
	 *            To whom the message should go. May not be null.
	 * @param multicast
	 *            Whether to send to all consumers or only one.
	 * @param temporary
	 *            Whether the message does not need to persist
	 * @param content
	 *            The content of the message. May not be null.
	 * @throws ServiceException
	 */
	public void publish(String queueKey, boolean multicast, boolean temporary, Q content) throws ServiceException;

	/**
	 * Sends a message to a queue system
	 * 
	 * @param queueKey
	 *            To whom the message should go. May not be null.
	 * @param multicast
	 *            Whether to send to all consumers or only one.
	 * @param temporary
	 *            Whether the message does not need to persist
	 * @param content
	 *            The content of the message. May not be null.
	 * @param correlation
	 *            The correlation id that allows throttling
	 * @param blocks
	 *            The blocks to waitFor before allowing this message to be processed
	 * @throws ServiceException
	 */
	public void publish(String queueKey, boolean multicast, boolean temporary, Q content, Long correlation, QoS qos, String... blocks) throws ServiceException;

	
	//public void close(int closeCode, String closeMessage) throws WorkQueueException;

	public void close() ;

	public int getConsumerCount(String queueKey, boolean temporary);

	public String[] getSubscriptions(String queueKey) throws ServiceException;

	public void block(String... blocks) throws ServiceException;

}