/**
 *
 */
package simple.app;

import simple.io.Log;

/**
 * @author Brian S. Krug
 *
 */
public class LoggingServiceStatusListener implements ServiceStatusListener {
	private static final Log log = Log.getLog();

	/**
	 * @see simple.app.ServiceStatusListener#serviceStatusChanged(simple.app.Service, java.lang.Thread, simple.app.ServiceStatus, java.lang.Number)
	 */
	public void serviceStatusChanged(ServiceStatusInfo serviceStatusInfo) {
		if(log.isInfoEnabled())
			log.info("Service " + serviceStatusInfo.getService().getServiceName() + " " + serviceStatusInfo.getServiceStatus());
	}
}
