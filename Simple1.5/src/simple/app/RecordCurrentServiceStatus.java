/*
 * Created on May 9, 2005
 *
 */
package simple.app;

import java.util.Map;
import java.util.TreeMap;

import simple.bean.ConvertUtils;
import simple.bean.DynaBean;
import simple.db.CachedBatchRecorder;
import simple.db.ParameterException;
import simple.io.Log;
import simple.lang.SystemUtils;

/** Records Service Status in the database on a regular interval.
 *
 * @author Brian S. Krug
 *
 */
public class RecordCurrentServiceStatus extends CachedBatchRecorder<String,ServiceThreadStatusInfo> implements ServiceStatusListener {
	private static final Log log = Log.getLog();
	public static class Info {
		protected String serviceName;
		protected final StringBuilder threadDetails = new StringBuilder();
		protected long lastUpdateTime;
		protected ServiceStatus serviceStatus;
		public ServiceStatus getServiceStatus() {
			return serviceStatus;
		}
		public String getServiceName() {
			return serviceName;
		}
		public String getThreadDetails() {
			return threadDetails.toString();
		}
		public long getLastUpdateTime() {
			return lastUpdateTime;
		}
		public DynaBean getSystemInfo() {
			return SystemUtils.getSystemInfo();
		}
	}
    public RecordCurrentServiceStatus() {
        super();
    }

	@Override
	protected ServiceThreadStatusInfo createInfo(String infoKey, Object... additionalInfo) {
		return new ServiceThreadStatusInfo();
	}

	public void serviceStatusChanged(ServiceStatusInfo serviceStatusInfo) {
		String key = getKey(serviceStatusInfo.getService(), serviceStatusInfo.getThread());
		ServiceThreadStatusInfo info = getOrCreateInfo(key);
		info.update(serviceStatusInfo);
		initialize();
	}

	protected String getKey(Service service, Thread thread) {
		return String.valueOf(thread.getId()) + '-' + service.getServiceName();
	}
	@Override
	protected void addBatches() {
		Map<String, Info> infos = new TreeMap<String, Info>();
		for(ServiceThreadStatusInfo threadInfo : currentBatches.values()) {
			Info info = infos.get(threadInfo.getServiceName());
			if(info == null) {
				info = new Info();
				infos.put(threadInfo.getServiceName(), info);
				info.serviceName = threadInfo.getServiceName();
				info.lastUpdateTime = threadInfo.getUpdateTime();
				info.serviceStatus = threadInfo.getServiceStatus();
			} else {
				if(info.lastUpdateTime < threadInfo.getUpdateTime())
					info.lastUpdateTime = threadInfo.getUpdateTime();
				info.threadDetails.append(SystemUtils.getNewLine());
				if(!info.getServiceStatus().equals(threadInfo.getServiceStatus())) {
					info.serviceStatus = ServiceStatus.OTHER;
				}
			}
			info.threadDetails.append(threadInfo.getThreadName()).append(": ").append(threadInfo.getServiceStatus())
				.append(" at ").append(ConvertUtils.formatObject(threadInfo.getUpdateTime(), "DATE:MM/dd/yyyy HH:mm:ss"))
				.append(" [").append(threadInfo.getIterationCount()).append(" iterations]");
		}
		for(Info info : infos.values())
			try {
				batch.addBatch(info);
			} catch(ParameterException e) {
				log.warn("Could not add batch for " + info.getServiceName(), e);
			}
	}
}
