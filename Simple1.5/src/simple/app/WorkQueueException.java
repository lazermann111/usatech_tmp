/*
 * Created on Jul 25, 2005
 *
 */
package simple.app;

public class WorkQueueException extends Exception {

    /**
	 *
	 */
	private static final long serialVersionUID = 53393827511L;

	public WorkQueueException() {
        super();
    }

    public WorkQueueException(String message) {
        super(message);
    }

    public WorkQueueException(String message, Throwable cause) {
        super(message, cause);
    }

    public WorkQueueException(Throwable cause) {
        super(cause);
    }

}
