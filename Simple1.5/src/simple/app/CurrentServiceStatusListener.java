package simple.app;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class CurrentServiceStatusListener implements ServiceStatusListener {
	protected final Map<String,ServiceThreadStatusInfo> currentStatuses = new ConcurrentHashMap<String,ServiceThreadStatusInfo>();

	public void serviceStatusChanged(ServiceStatusInfo serviceStatusInfo) {
		String key = getKey(serviceStatusInfo.getService(), serviceStatusInfo.getThread());
		ServiceThreadStatusInfo threadInfo = currentStatuses.get(key);
		if(threadInfo == null) {
			threadInfo = new ServiceThreadStatusInfo();
			currentStatuses.put(key, threadInfo);
			infoCreated(key, threadInfo);
		}
		updateServiceStatusInfo(threadInfo, serviceStatusInfo);
	}
	/** A hook for sub-classes to let them know that a new info was created
	 * @param key
	 * @param info
	 */
	protected void infoCreated(String key, ServiceThreadStatusInfo info) {
	}
	protected void updateServiceStatusInfo(ServiceThreadStatusInfo info, ServiceStatusInfo serviceStatusInfo) {
		info.update(serviceStatusInfo);
	}

	public Collection<ServiceThreadStatusInfo> getCurrentThreadStatuses() {
		return currentStatuses.values();
	}

	protected abstract String getKey(Service service, Thread thread) ;
}
