package simple.app;


public interface Commander {
	public <C> void start(CommandManager<C> commandManager) ;
	public <C> void processCommands(CommandManager<C> commandManager) ;
	public void shutdown() ;
	public String getDescription() ;
	public void setPrompt(String prompt) ;
}
