package simple.app;

public interface ResetAvailabilityTrigger {
	public void registerListener(ResetAvailabilityListener listener);

	public void deregisterListener(ResetAvailabilityListener listener);
}
