/**
 *
 */
package simple.app;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;

/**
 * @author Brian S. Krug
 *
 */
public class DatabasePrerequisite implements Prerequisite {
	private static final Log log = Log.getLog();
    protected String dataSourceName;
	protected String validationQuery;
	/**
	 * @see simple.app.Prerequisite#isAvailable()
	 */
	public boolean isAvailable() {
		String dsn = getDataSourceName();
		String sql = getValidationQuery();

		if(log.isDebugEnabled())
			log.debug("Checking availability of data source '" + dsn + "'" + (sql != null && sql.trim().length() > 0 ? " using sql '" + sql + "'" : ""));
		try {
			Connection conn = DataLayerMgr.getConnection(getDataSourceName());
			try {
				if(sql != null && sql.trim().length() > 0) {
					if(log.isDebugEnabled())
						log.debug("Connection for data source '" + dsn + "' successfully obtained; running validation sql");
					Statement st = conn.createStatement();
					try {
						boolean hasResults = st.execute(sql);
						if(hasResults) {
							ResultSet rs = st.getResultSet();
							try {
								boolean hasNext = rs.next();
								if(hasNext) {
									if(log.isInfoEnabled())
										log.info("Data source '" + dsn + "' is available");
								} else if(log.isWarnEnabled()) {
									log.warn("Data source '" + dsn + "' is not available because vaildation query returned zero rows");
								}
								return hasNext;
							} finally {
								rs.close();
							}
						} else {
							if(log.isInfoEnabled())
								log.info("Data source '" + dsn + "' is available");
							return true;
						}
					} finally {
						st.close();
					}
				}
			} finally {
				conn.close();
			}
			if(log.isInfoEnabled())
				log.info("Data source '" + dsn + "' is available");
			return true;
		} catch(SQLException e) {
			log.warn("Data source '" + dsn + "' is not available", e);
		} catch(DataLayerException e) {
			log.warn("Data source '" + dsn + "' is not available", e);
		}
		return false;
	}
	public String getDataSourceName() {
		return dataSourceName;
	}
	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
	public String getValidationQuery() {
		return validationQuery;
	}
	public void setValidationQuery(String validationQuery) {
		this.validationQuery = validationQuery;
	}
	public static ServiceException createServiceException(SQLException e) {
		return createServiceException(e.toString(), e);
	}
	public static ServiceException createServiceException(String message, SQLException e) {
		WorkRetryType retryType = determineRetryType(e);
		if(retryType == null)
			return new ServiceException(message, e);
		else
			return new RetrySpecifiedServiceException(message, e, retryType);
	}

	public static WorkRetryType determineRetryType(SQLException e) {
		String sqlState = e.getSQLState();
		WorkRetryType retryType;
		if(sqlState == null) {
			if(e.getMessage().contains("Io exception:")) {//Underlying IO exception (at least with Oracle)
				retryType = WorkRetryType.BLOCKING_RETRY;
			} else if(e.getMessage().contains("Timeout")) {//Underlying timeout exception (at least with DBCP)
				retryType = WorkRetryType.BLOCKING_RETRY;
			} else {
				retryType = WorkRetryType.NONBLOCKING_RETRY;
			}
		} else {
			retryType = sqlStateRetryTypes.get(sqlState.substring(0, 3));
			if(retryType == null)
				retryType = sqlStateRetryTypes.get(sqlState.substring(0, 2));
		}

		return retryType;
	}

	protected static final Map<String,WorkRetryType> sqlStateRetryTypes = new HashMap<String, WorkRetryType>();
	static {
		sqlStateRetryTypes.put("01", WorkRetryType.NONBLOCKING_RETRY); // Warnings
		sqlStateRetryTypes.put("02", WorkRetryType.NONBLOCKING_RETRY); // Not enough rows
		sqlStateRetryTypes.put("07", WorkRetryType.NONBLOCKING_RETRY); // Dynamic SQL Error
		sqlStateRetryTypes.put("08", WorkRetryType.BLOCKING_RETRY); // Connection issue
		sqlStateRetryTypes.put("09", WorkRetryType.NONBLOCKING_RETRY); // Triggered Action Exception
		sqlStateRetryTypes.put("0A", WorkRetryType.BLOCKING_RETRY); // Unsupported Feature Exception
		sqlStateRetryTypes.put("20", WorkRetryType.NONBLOCKING_RETRY); // Case not found Exception
		sqlStateRetryTypes.put("21", WorkRetryType.NONBLOCKING_RETRY); // Too Many Rows
		sqlStateRetryTypes.put("22", WorkRetryType.NONBLOCKING_RETRY); // Data issue
		sqlStateRetryTypes.put("23", WorkRetryType.NONBLOCKING_RETRY); // Constraint issue
		sqlStateRetryTypes.put("24", WorkRetryType.NONBLOCKING_RETRY); // Invalid Cursor State
		sqlStateRetryTypes.put("25", WorkRetryType.NONBLOCKING_RETRY); // Invalid Transaction State
		sqlStateRetryTypes.put("27", WorkRetryType.NONBLOCKING_RETRY); // Triggered Data Change Violation
		sqlStateRetryTypes.put("28", WorkRetryType.BLOCKING_RETRY); // Authorization issue
		sqlStateRetryTypes.put("2D", WorkRetryType.NONBLOCKING_RETRY); //  Invalid Transaction Termination
		sqlStateRetryTypes.put("2E", WorkRetryType.BLOCKING_RETRY); // Invalid Connection Name
		sqlStateRetryTypes.put("2F", WorkRetryType.NONBLOCKING_RETRY); // SQL Function Exception
		sqlStateRetryTypes.put("34", WorkRetryType.NONBLOCKING_RETRY); // Invalid Cursor Name
		sqlStateRetryTypes.put("38", WorkRetryType.NONBLOCKING_RETRY); // External Function Exception
		sqlStateRetryTypes.put("39", WorkRetryType.NONBLOCKING_RETRY); // External Function Call Exception
		sqlStateRetryTypes.put("3B", WorkRetryType.NONBLOCKING_RETRY); // Savepoint Exception
		sqlStateRetryTypes.put("40", WorkRetryType.IMMEDIATE_RETRY); // Transaction Rollback
		sqlStateRetryTypes.put("42", WorkRetryType.NO_RETRY); // Syntax Error or Access Rule Violation
		sqlStateRetryTypes.put("425", WorkRetryType.BLOCKING_RETRY); // Access Rule Violation
		sqlStateRetryTypes.put("61", WorkRetryType.NONBLOCKING_RETRY); // Resource Error
		sqlStateRetryTypes.put("65", WorkRetryType.NO_RETRY); // PL/SQL errors
		sqlStateRetryTypes.put("82", WorkRetryType.NONBLOCKING_RETRY); // Various runtime errors
	}
}
