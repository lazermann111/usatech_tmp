package simple.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.app.Advisor.AdviseType;
import simple.event.ProgressEvent;
import simple.event.ProgressListener;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.LockOnWriteList;
import simple.util.concurrent.ResultFuture;
import simple.util.concurrent.TrivialFuture;

/** Provides foundation for a Service that pulls work from a queue and executes it. All public methods are
 *  thread-safe and do not require external synchronization.
 *
 * @author Brian S. Krug
 *
 * @param <Q>
 */
public abstract class AbstractWorkQueueService<Q> extends AbstractService implements ControllingService {
	private static final Log log = Log.getLog();
	public static interface WorkProcessor<Q> {
		public void processWork(Work<Q> work) throws ServiceException, WorkQueueException;
		public boolean interruptProcessing(Thread thread) ;
	}

	public static enum ErrorAction { STOP_ALL, FINISH_THREAD, PAUSE, CONTINUE, DELAY }
	protected static enum ControlState { CONTINUE, STOP, PAUSE_PENDING, DELAY_PENDING, PREPARING, PAUSED, DELAYED }
	protected static enum AutoGrowthCheck { NONE, LATENCY, AVG_WAIT, LATENCY_AND_AVG_WAIT }
	protected final List<WorkQueueServiceThread> threads = new LockOnWriteList<WorkQueueServiceThread>(); //new ArrayList<WorkQueueServiceThread>();
	protected WorkQueue<Q> queue;
	protected ErrorAction errorAction = ErrorAction.CONTINUE;
	protected WorkRetryType defaultRetryType = WorkRetryType.NONBLOCKING_RETRY;
	protected final RetryPolicy delayOnErrorPolicy = new RetryPolicy(-1, 30000, 1);
	protected int processingTimeout;
	protected Advisor advisor;
	protected long autoGrowthLatency;
	protected float autoGrowthFactor = 1.0f;
	protected int autoGrowthMaxThreads;
	protected int autoGrowthWait = 5000;
	protected long[] autoGrowthSamples = new long[0];
	protected int autoGrowthSampleIndex;
	protected long autoGrowthTotalWait;
	protected long autoGrowthMinAvgWait;
	protected AutoGrowthCheck autoGrowthCheck = AutoGrowthCheck.NONE;
	protected long lastAuthGrowthTime = 0;
	protected int maxCancelAttempts = 10;
	protected int pausePerLoopMillis = -1;
	protected int pausePerLoopIterations = 1;
	protected ServiceManagerControl serviceManagerControl;
	protected final ReentrantLock autoGrowthLock = new ReentrantLock();
	protected final ScheduledThreadPoolExecutor processingTimeoutService = new ScheduledThreadPoolExecutor(1, new CustomThreadFactory("ProcessWatchdog-", true, 3));
	
	protected class WorkQueueServiceThread extends Thread {
		protected final ReentrantLock lock = new ReentrantLock();
		protected final Condition pauseSignal = lock.newCondition();
		protected final AtomicReference<ControlState> control = new AtomicReference<ControlState>(ControlState.CONTINUE);
		protected final WorkQueue.Retriever<Q> retriever;
		protected final WorkProcessor<Q> workProcessor;
		protected long iterationCount = 0;
		protected Long queueLatency;
		protected volatile ServiceStatus status = ServiceStatus.INITIALIZING;
		protected int errorCount;

		public WorkQueueServiceThread(String name) throws WorkQueueException {
			super(name);
			if(queue == null)
				throw new WorkQueueException("Queue property on " + getServiceName() + " is not set");
			this.retriever = queue.getRetriever(new ProgressListener() {				
				public void progressUpdate(ProgressEvent event) {
					setServiceStatus(ServiceStatus.STARTING);
				}			
				public void progressStart(ProgressEvent event) {
					setServiceStatus(ServiceStatus.INITIALIZING);
				}			
				public void progressFinish(ProgressEvent event) {
					setServiceStatus(ServiceStatus.STARTED);
				}
			});
			this.workProcessor = getWorkProcessor();
			threads.add(this);
		}
		
		@Override
		public void run() {
			try {
				threadStarting(this);
				loop();
			} catch(RuntimeException e) {
				log.error("Unexpected exception while running thread; Ending thread.", e);
			} catch(OutOfMemoryError e) {
				try {
					System.exit(2);
					log.error("Out of Memory while running thread; Shutting down application.", e);
					Log.finish();
					Thread.sleep(5000);
				} catch(InterruptedException e1) {
					// ignore
				} finally {
					Runtime.getRuntime().halt(2);
				}
			} catch(Error e) {
				log.error("Unexpected error while running thread; Ending thread.", e);
			} finally {
				setServiceStatus(ServiceStatus.DONE);
				retriever.close(); // allow cleanup
				threads.remove(this);
			}
		}
		protected void loop() {
			while(true) {
				ControlState cs = control.get();
				switch(cs) {
					case STOP:
						return;
					case PAUSE_PENDING:
						if(!control.compareAndSet(ControlState.PAUSE_PENDING, ControlState.PREPARING))
							continue;
						lock.lock();
						try {
							if(!control.compareAndSet(ControlState.PREPARING, ControlState.PAUSED))
								continue;
							setServiceStatus(ServiceStatus.PAUSED);
							while(control.get() == ControlState.PAUSED)
								try {
									pauseSignal.await();
								} catch(InterruptedException e) {
								}
						} finally {
							lock.unlock();
						}
						continue;
					case DELAY_PENDING:
						if(!control.compareAndSet(ControlState.DELAY_PENDING, ControlState.PREPARING))
							continue;
						lock.lock();
						try {
							if(!control.compareAndSet(ControlState.PREPARING, ControlState.DELAYED))
								continue;
							setServiceStatus(ServiceStatus.DELAYED);
							long delay = nextDelay();
							long until = System.currentTimeMillis() + delay;
							if(log.isInfoEnabled())
								log.info("Delaying for " + delay + "milliseconds until " + until);
							while(control.get() == ControlState.DELAYED) {
								long wait = until - System.currentTimeMillis();
								if(wait <= 0) {
									control.compareAndSet(ControlState.DELAYED, ControlState.CONTINUE);
									break;
								}
								if(log.isDebugEnabled())
									log.debug("Waiting " + wait + " milliseconds");
								try {
									pauseSignal.await(wait, TimeUnit.MILLISECONDS);
								} catch(InterruptedException e) {
								}
							}
						} finally {
							lock.unlock();
						}
						continue;
					case CONTINUE:
						break;
					default:
						log.warn("Invalid control state '" + cs + "'");
						if(!control.compareAndSet(cs, ControlState.CONTINUE))
							continue;
						break;
				}
				int pplm = getPausePerLoopMillis();
				int ppli = getPausePerLoopIterations();
				if(pplm >= 0 && ppli > 0 && (iterationCount % ppli) == 0) {
					if(pplm == 0)
						Thread.yield();
					else
						try {
							Thread.sleep(pplm);
						} catch(InterruptedException e) {
							//ignore
						}
				}
				setServiceStatus(ServiceStatus.AWAITING_PREREQ);
    			if(retriever.arePrequisitesAvailable()) {
					setServiceStatus(ServiceStatus.AWAITING_NEXT);
					AutoGrowthCheck agCheck = autoGrowthCheck;
					long startWait;
					switch(agCheck) {
						case AVG_WAIT:
						case LATENCY_AND_AVG_WAIT:
							startWait = System.currentTimeMillis();
							break;
						default:
							startWait = 0;
					}
	    			Work<Q> work;
					try {
						work = retriever.next();
					} catch(WorkQueueException e) {
						handleQueueException(e);
						continue;
					} catch(InterruptedException e) {
						log.info("Interrupted while getting message", e);
						continue;
					} catch(RuntimeException e) {
						handleQueueException(e);
						continue;
					} catch(Error e) {
						handleQueueException(e);
						continue;
					}
					if(work != null) {
						//send queue latency info
						if(work.getEnqueueTime() > 0) {
							long currentTime = System.currentTimeMillis();
		    				long latency = currentTime - work.getEnqueueTime();
							queueLatency = latency;
		    				checkAutoGrowth(agCheck, latency, currentTime, startWait > 0 ? currentTime - startWait : 0);
		    			} else if(startWait > 0 ) {
		    				long currentTime = System.currentTimeMillis();
		    				checkAutoGrowth(agCheck, 0, currentTime, currentTime - startWait);	
		    			}
						if(control.get() == ControlState.STOP) {
							if(!work.isComplete())
								try {
									work.workAborted(WorkRetryType.NONBLOCKING_RETRY, false, new InterruptedException("Cancelled by user"));
								} catch(WorkQueueException e) {
									handleQueueException(e);
								}
							break;
						}
						if(work.getBody() != null) {
							iterationCount++;
			    			setServiceStatus(ServiceStatus.PROCESSING_MSG);
			    			AtomicInteger cancelCount = new AtomicInteger(0);
							try {
								long timeout = getProcessingTimeout();
								Future<?> processingTimeoutTask;
								if(timeout > 0)
									processingTimeoutTask = scheduleProcessingTimeout(this, workProcessor, timeout, work.toString(), cancelCount);
								else
									processingTimeoutTask = null;
								boolean interrupted;
								try {
									workProcessor.processWork(work);
								} finally {
									if(processingTimeoutTask != null) {
										cancelProcessingTimeout(processingTimeoutTask);
									}
									// ensure that interrupted status is cleared
									interrupted = Thread.interrupted();
								}
								if(interrupted && log.isInfoEnabled())
									log.info("Thread " + this.getName() + " was interrupted, but processor did not throw an exception. Processing message normally");

								setServiceStatus(ServiceStatus.ACKNOWLEDGING_MSG); // this is after the fact but oh well - it avoids a messy interface in processAndCompleteWork()
								if(!work.isComplete()) {
									if(log.isDebugEnabled())
										log.debug("Acknowleging successful processing of work");
									work.workComplete();
								}
							} catch (RetrySpecifiedServiceException e) {
								if(work.isComplete()) {
									log.info("Exception occurred while processing message, but message is already completed", e);
								} else {
									setServiceStatus(cancelCount.get() > 0 ? ServiceStatus.TIMEDOUT_MSG : ServiceStatus.NACKING_MSG);
									WorkRetryType retryType = e.getRetryType();
									if(retryType!= null)
										switch(retryType) {
											case BLOCKING_RETRY:
												log.info("Could not process message; using blocking retry", e);
												break;
											case NONBLOCKING_RETRY:
												log.info("Could not process message; using non-blocking retry", e);
												break;
											case NO_RETRY:
												log.warn("Could not process message; not retrying", e);
												break;
											case IMMEDIATE_RETRY:
												log.info("Could not process message; using immediate retry", e);
												break;
											case SCHEDULED_RETRY:
												log.info("Could not process message right now because " + e.getMessage());
												break;
										}
									else {
										log.error("Could not process message; rolling back", e);
										retryType = getDefaultRetryType();
									}
									try {
										work.workAborted(retryType, e.isUnguardedRetryAllowed(), e);
									} catch(WorkQueueException e1) {
										handleQueueException(e1);
										continue;
									}
								}
							} catch (ServiceException e) {
								if(work.isComplete()) {
									log.info("Exception occurred while processing message, but message is already completed", e);
								} else {
									setServiceStatus(cancelCount.get() > 0 ? ServiceStatus.TIMEDOUT_MSG : ServiceStatus.NACKING_MSG);
									log.warn("Could not process message; rolling back", e);
									try {
										work.workAborted(getDefaultRetryType(), false, e);
									} catch(WorkQueueException e1) {
										handleQueueException(e1);
										continue;
									}
								}
							} catch(WorkQueueException e) {
								handleQueueException(e);
								continue;
							} catch(RuntimeException e) {
								if(work.isComplete()) {
									log.info("RuntimeException occurred while processing message, but message is already completed", e);
								} else {
									setServiceStatus(cancelCount.get() > 0 ? ServiceStatus.TIMEDOUT_MSG : ServiceStatus.NACKING_MSG);
									log.error("Could not process message (Runtime Exception); rolling back", e);
									try {
										work.workAborted(getDefaultRetryType(), false, e);
									} catch(WorkQueueException e1) {
										handleQueueException(e1);
										continue;
									} finally {
										Advisor a = getAdvisor();
										if(a != null) {
											if(cancelCount.get() > 0)
												a.advise(AdviseType.WARNING, "Processing Timeout", getServiceName(), work.toString());
											else
												a.advise(AdviseType.ERROR, "Runtime Exception", getServiceName(), StringUtils.exceptionToString(e));
										}
									}
								}
							} catch(Error e) {
								if(work.isComplete()) {
									log.info("Error occurred while processing message, but message is already completed", e);
								} else {
									setServiceStatus(cancelCount.get() > 0 ? ServiceStatus.TIMEDOUT_MSG : ServiceStatus.NACKING_MSG);
									log.error("Could not process message (Error); rolling back", e);
									try {
										work.workAborted(getDefaultRetryType(), false, e);
									} catch(WorkQueueException e1) {
										handleQueueException(e1);
										continue;
									} finally {
										Advisor a = getAdvisor();
										if(a != null) {
											if(cancelCount.get() > 0)
												a.advise(AdviseType.WARNING, "Processing Timeout", getServiceName(), work.toString());
											else
												a.advise(AdviseType.ERROR, "Runtime Exception", getServiceName(), StringUtils.exceptionToString(e));
										}
									}
								}
							}
						} else {
							log.debug("Skipping null message");
							setServiceStatus(ServiceStatus.ACKNOWLEDGING_MSG);
							if(!work.isComplete())
								try {
									work.workComplete();
								} catch(WorkQueueException e) {
									handleQueueException(e);
									continue;
								}
						}
						//success!
						resetDelay();
					}
    			}
			}
		}
		/**
		 *
		 */
		protected void resetDelay() {
			errorCount = 0;
		}

		/**
		 * @return
		 */
		protected long nextDelay() {
			return delayOnErrorPolicy.calculateDelay(errorCount++);
		}

		protected void handleQueueException(Throwable e) {
			setServiceStatus(ServiceStatus.HANDLING_ERROR);
			switch(getErrorAction()) {
				case STOP_ALL:
					log.fatal("Queue error; shutting down", e);
					internalFinish();
					try {
						stopAllThreads(true).get(10000, TimeUnit.MILLISECONDS);
					} catch(ExecutionException e1) {
						log.warn("Could not stop all threads", e1);
					} catch(InterruptedException e1) {
						log.warn("Could not stop all threads", e1);
					} catch(TimeoutException e1) {
						log.warn("Could not stop all threads", e1);
					}
					break;
				case CONTINUE:
					log.fatal("Queue error; continuing", e);
					break;
				case FINISH_THREAD:
					log.fatal("Queue error; finishing thread", e);
					internalFinish();
					break;
				case DELAY:
					log.fatal("Queue error; delaying one thread", e);
					delay();
					break;
				case PAUSE:
					log.fatal("Queue error; pausing all threads", e);
					pause();
					try {
						pauseThreads();
					} catch(ServiceException e1) {
						log.warn("Could not pause all threads", e1);
					}
					break;
			}
		}

		protected void setServiceStatus(ServiceStatus status) {
			this.status = status;
			fireServiceStatusChanged(this, status, iterationCount, queueLatency);
		}
		protected void internalFinish() {
			while(!control.compareAndSet(ControlState.CONTINUE, ControlState.STOP)) {
				switch(control.get()) {
					case STOP:
						return;
					case DELAY_PENDING:
						if(control.compareAndSet(ControlState.DELAY_PENDING, ControlState.STOP))
							return;
						else
							continue;
					case PAUSE_PENDING:
						if(control.compareAndSet(ControlState.PAUSE_PENDING, ControlState.STOP))
							return;
						else
							continue;
					case PAUSED:
					case DELAYED:
						unpause();
						continue;
					case PREPARING:
						Thread.yield();
						continue;
				}
			}
		}
		public boolean pause() {
			while(!control.compareAndSet(ControlState.CONTINUE, ControlState.PAUSE_PENDING)) {
				switch(control.get()) {
					case STOP:
						return false;
					case DELAY_PENDING:
						if(control.compareAndSet(ControlState.DELAY_PENDING, ControlState.PAUSE_PENDING))
							return true;
						else
							continue;
					case PAUSE_PENDING:
					case PAUSED:
						return false;
					case DELAYED:
						unpause();
						continue;
					case PREPARING:
						Thread.yield();
						continue;
				}
			}
			return true;
		}
		protected boolean delay() {
			while(!control.compareAndSet(ControlState.CONTINUE, ControlState.DELAY_PENDING)) {
				switch(control.get()) {
					case STOP:
						return false;
					case PAUSE_PENDING:
						if(control.compareAndSet(ControlState.PAUSE_PENDING, ControlState.DELAY_PENDING))
							return true;
						else
							continue;
					case DELAY_PENDING:
					case DELAYED:
						return false;
					case PAUSED:
						unpause();
						continue;
					case PREPARING:
						Thread.yield();
						continue;
				}
			}
			return true;
		}
		public boolean unpause() {
			lock.lock();
			try {
				while(true) {
					ControlState cs = control.get();
					switch(cs) {
						case STOP:
							pauseSignal.signalAll();
							return true;
						case PAUSE_PENDING: case DELAY_PENDING:
							if(!control.compareAndSet(cs, ControlState.CONTINUE))
								continue;
						case PAUSED: case DELAYED:
							if(control.compareAndSet(cs, ControlState.CONTINUE)) {
								pauseSignal.signalAll();
								return true;
							} else {
								continue;
							}
						case CONTINUE:
							return false;
						case PREPARING:
							Thread.yield();
							continue;
					}
				}
			} finally {
				lock.unlock();
			}
		}
		public void finish() {
			internalFinish();
			if(isAlive()) {
				switch(status) {
					case PAUSED: case DELAYED:
						log.warn("Thread is " + status + " after internalFinish(). This should not happen", new Throwable());
						//interrupt();
						unpause();
						break;
					case AWAITING_PREREQ:
						retriever.cancelPrequisiteCheck(this);
						break;
					case AWAITING_NEXT:
						retriever.cancel(this);
						break;
					case PROCESSING_MSG:
						workProcessor.interruptProcessing(this);
						break;
					case HANDLING_ERROR:
						return;
				}
			}
		}
		public void waitForComplete(boolean force, long timeout) throws InterruptedException {
			if(timeout > 0)
				try {
					join(timeout);
				} catch(InterruptedException e) {
					// ignore -- this is what we want!
				}
			if(isAlive() && this != Thread.currentThread()) {
				if(force) {
					interrupt();
					closeRetrieverSafely();
					// setServiceStatus(ServiceStatus.STOPPING);
				} else
					throw new InterruptedException("Not able to cancel next() of queue");
			}
		}

		protected void closeRetrieverSafely() {
			log.info("Closing retriever for thread '" + getName() + "' on service '" + getServiceName() + "'");
			try {
				retriever.close();
			} catch(Throwable t) {
				log.warn("Could not close retriever for thread '" + getName() + "' on service '" + getServiceName() + "'", t);
			}
		}
	}

	public AbstractWorkQueueService(String serviceName) {
		super(serviceName);
	}

	/**
	 * This is a hook for subclasses to do any thread-by-thread initialization
	 * 
	 * @param thread
	 */
	protected void threadStarting(WorkQueueServiceThread thread) {

	}

	protected boolean checkAutoGrowth(AutoGrowthCheck agCheck, long queueLatency, long currentTime, long waitTime) {
		switch(agCheck) {
			case LATENCY:
				return checkQueueLatency(queueLatency, currentTime);
			case LATENCY_AND_AVG_WAIT:
				if(checkQueueLatency(queueLatency, currentTime))
					return true;
				return checkQueueAvgWait(waitTime, currentTime);
			case AVG_WAIT:
				return checkQueueAvgWait(waitTime, currentTime);
			default:
				return false;
		}
	}
	protected boolean checkQueueLatency(long queueLatency, long currentTime) {
		long agLatency = getAutoGrowthLatency();
		if(agLatency > 0 && agLatency < queueLatency) {
			autoGrowthLock.lock();
			try {
				int agMaxThreads = getAutoGrowthMaxThreads();
				int numThreads = getNumThreads();
				if(agMaxThreads <= 0 || numThreads < agMaxThreads) {
					long agWait = Math.max(1000, autoGrowthWait);
					if(lastAuthGrowthTime <= 0) {
						lastAuthGrowthTime = System.currentTimeMillis(); // This helps with start-up
					} else if(lastAuthGrowthTime + agWait < currentTime) {
						int n = Math.max(1, (int)Math.ceil(numThreads * Math.max(0.0f, getAutoGrowthFactor())));
						try {
							startThreads(n);
						} catch(ServiceException e) {
							log.error("Could not start additional threads on " + getServiceName() + " for auto-growth feature", e);
						}
						String details = "To " + (n + numThreads) + " threads from " + numThreads + " because latency is " + queueLatency + " ms at " + new Date(currentTime);
						log.info("Auto Growing " + getServiceName() + ": " + details);
						Advisor a = getAdvisor();
						if(a != null)
							a.advise(AdviseType.ACTION, "Auto Growth", getServiceName(), details);
						lastAuthGrowthTime = System.currentTimeMillis();	
						return true;
					}
				}
			} finally {
				autoGrowthLock.unlock();
			}
		}
		return false;
	}
	protected boolean checkQueueAvgWait(long waitTime, long currentTime) {
		long agMinAvgWait = getAutoGrowthMinAvgWait();
		if(agMinAvgWait > 0) {
			autoGrowthLock.lock();
			try {
				int agMaxThreads = getAutoGrowthMaxThreads();
				int numThreads = getNumThreads();
				if(agMaxThreads <= 0 || numThreads < agMaxThreads) {
					boolean grew = false;
					if(autoGrowthSampleIndex >= autoGrowthSamples.length) {
						// time to check
						long agWait = autoGrowthWait;
						if(agWait <= 0 || lastAuthGrowthTime + agWait < currentTime) {					
							long avgWait = (waitTime + autoGrowthTotalWait) / (autoGrowthSamples.length + 1);
							if(agMinAvgWait > avgWait) {
								int n = Math.max(1, (int)Math.ceil(numThreads * Math.max(0.0f, getAutoGrowthFactor())));
								try {
									startThreads(n);
								} catch(ServiceException e) {
									log.error("Could not start additional threads on " + getServiceName() + " for auto-growth feature", e);
								}
								String details = "To " + (n + numThreads) + " threads from " + numThreads + " because average wait is " + avgWait + " ms at " + new Date(currentTime);
								log.info("Auto Growing " + getServiceName() + ": " + details);
								Advisor a = getAdvisor();
								if(a != null)
									a.advise(AdviseType.ACTION, "Auto Growth", getServiceName(), details);
								lastAuthGrowthTime = System.currentTimeMillis();	
								grew = true;
							}
						} 
						autoGrowthSampleIndex = 0;
					}
					autoGrowthTotalWait = autoGrowthTotalWait + waitTime - autoGrowthSamples[autoGrowthSampleIndex];
					autoGrowthSamples[autoGrowthSampleIndex++] = waitTime;
					return grew;
				}
			} finally {
				autoGrowthLock.unlock();
			}
		}
		return false;
	}
	/**
	 * @param workQueueServiceThread
	 * @param workProcessor
	 * @param timeout
	 * @return
	 */
	protected Future<?> scheduleProcessingTimeout(final WorkQueueServiceThread workQueueServiceThread, final WorkProcessor<Q> workProcessor, final long timeout, final String description, final AtomicInteger cancelCount) {
		return processingTimeoutService.scheduleWithFixedDelay(new Runnable() {
			public void run() {
				int cnt = cancelCount.getAndIncrement();
				int max = getMaxCancelAttempts();
				if(cnt == 0) {
					Throwable throwable;
					if(log.isInfoEnabled()) {
						throwable = new TimeoutException();
						throwable.setStackTrace(workQueueServiceThread.getStackTrace());
					} else
						throwable = null;					
					workProcessor.interruptProcessing(workQueueServiceThread);
					log.error("Processing of '" + description + "' on service '" + getServiceName() + "', thread '" + workQueueServiceThread.getName() + "' is being cancelled after " + timeout + " milliseconds", throwable);
				} else if(max > 0 && cnt > max) {
					Throwable throwable = new TimeoutException();
					throwable.setStackTrace(workQueueServiceThread.getStackTrace());
					workProcessor.interruptProcessing(workQueueServiceThread);
					workQueueServiceThread.closeRetrieverSafely();
					ServiceManagerControl smc = getServiceManagerControl();
					if(smc != null) {
						log.fatal("Failed " + cnt + " times to cancel processing of '" + description + "' on service '" + getServiceName() + "', thread '" + workQueueServiceThread.getName() + "' - shutting down application", throwable);
						smc.requestShutdown(-1); // use default
					} else {
						log.fatal("Failed " + cnt + " times to cancel processing of '" + description + "' on service '" + getServiceName() + "', thread '" + workQueueServiceThread.getName() + "' but cannot shutdown because ServiceManagerControl is not set", throwable);						
					}
				} else {
					workProcessor.interruptProcessing(workQueueServiceThread);
					log.warn("Attempting to cancel processing of '" + description + "' on service '" + getServiceName() + "', thread '" + workQueueServiceThread.getName() + "' again because it is still not complete");
				}
			}
		}, timeout, timeout, TimeUnit.MILLISECONDS);
	}

	protected void cancelProcessingTimeout(Future<?> processingTimeoutTask) {
		if(!(processingTimeoutTask instanceof RunnableScheduledFuture<?>) 
				|| !processingTimeoutService.remove((RunnableScheduledFuture<?>)processingTimeoutTask)) {
			if(!processingTimeoutTask.cancel(false))  //NOTE: Must use false for mayInterruptIfRunning b/c when the reaper runs it calls this method!
				log.warn("Failed to cancel processing timeout");	
		}
	}
	protected abstract WorkProcessor<Q> getWorkProcessor() ;

	public ErrorAction getErrorAction() {
		return errorAction ;
	}

	public void setErrorAction(ErrorAction errorAction) {
		this.errorAction = errorAction;
	}

	public int getNumThreads() {
		return threads.size();
	}

	@Override
	protected ServiceStatusInfo[] getCurrentServiceStatusInfos() {
		List<ServiceStatusInfo> infos = new ArrayList<ServiceStatusInfo>();
		for(WorkQueueServiceThread t : threads) {
			infos.add(new ServiceStatusInfo(this, t, t.status, t.iterationCount, t.queueLatency));
		}
		return infos.toArray(new ServiceStatusInfo[infos.size()]);
	}

	/**
	 * @throws ServiceException  
	 */
	protected Map<String,InterruptedException> waitForComplete(Set<WorkQueueServiceThread> threads, boolean force, long timeout) {
		Map<String,InterruptedException> exceptionThreadNames = null;
		long end = System.currentTimeMillis() + timeout;
		for(WorkQueueServiceThread thread : threads) {
			try {
				thread.waitForComplete(force, timeout);
			} catch(InterruptedException e) {
				if(exceptionThreadNames == null) {
					exceptionThreadNames = new LinkedHashMap<String,InterruptedException>();
				}
				exceptionThreadNames.put(thread.getName(), e);
				//threads.add(thread);
				timeout = 0;
			}
			if(timeout > 0) {
				timeout = end - System.currentTimeMillis();
				if(timeout < 0)
					timeout = 0;
			}
		}
		return exceptionThreadNames;
	}
	public int restartThreads(long timeout) throws ServiceException {
		log.info("Restarting threads on " + getServiceName());
		Map<WorkQueueServiceThread,WorkQueueServiceThread> stopped = new HashMap<WorkQueueServiceThread,WorkQueueServiceThread>();
		for(WorkQueueServiceThread old : threads) {
			if(old != null) { //it was removed during iteration
				WorkQueueServiceThread replacement;
				try {
					replacement = new WorkQueueServiceThread(old.getName());
				} catch(WorkQueueException e) {
					throw new ServiceException("Could not create Queue Service Thread", e);
				}
				stopped.put(old, replacement);
			}
		}
		for(Map.Entry<WorkQueueServiceThread,WorkQueueServiceThread> entry : stopped.entrySet()) {
			WorkQueueServiceThread old =  entry.getKey();
			WorkQueueServiceThread replacement = entry.getValue();
			old.finish();
			replacement.start();
		}
		Map<String,InterruptedException> exceptions = waitForComplete(stopped.keySet(), false, timeout);
		if(exceptions != null && !exceptions.isEmpty() && log.isWarnEnabled()) {
			for(Map.Entry<String,InterruptedException> entry : exceptions.entrySet()) {
				log.warn("Could not stop thread '" + entry.getKey() + "'", entry.getValue());
			}	
		}
		return stopped.size();
	}

	public int startThreads(int numThreads) throws ServiceException {
		if(numThreads < 1) return 0;
		log.info("Starting " + numThreads + " threads on " + getServiceName());
		for(int i = 0; i < numThreads; i++) {
			WorkQueueServiceThread thread;
			try {
				thread = new WorkQueueServiceThread(getNewThreadName());
			} catch(WorkQueueException e) {
				throw new ServiceException("Could not create Queue Service Thread", e);
			}
			thread.start();
		}
		return numThreads;
	}

	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		try {
			return stopAllThreads(force).get(timeout, TimeUnit.MILLISECONDS);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof ServiceException)
				throw (ServiceException)e.getCause();
			else if(e.getCause() != null)
				throw new ServiceException(e.getCause());
			else
				throw new ServiceException(e);
		} catch(TimeoutException e) {
			throw new ServiceException(e);
		}
	}
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		try {
			return stopThreads(numThreads, force).get(timeout, TimeUnit.MILLISECONDS);
		} catch(InterruptedException e) {
			throw new ServiceException(e);
		} catch(ExecutionException e) {
			if(e.getCause() instanceof ServiceException)
				throw (ServiceException)e.getCause();
			else if(e.getCause() != null)
				throw new ServiceException(e.getCause());
			else
				throw new ServiceException(e);
		} catch(TimeoutException e) {
			throw new ServiceException(e);
		}
	}
	
	public Future<Integer> stopAllThreads(final boolean force) {
		log.info("Stopping all threads on " + getServiceName());
		final Set<WorkQueueServiceThread> stopped = new HashSet<WorkQueueServiceThread>();
		for(WorkQueueServiceThread thread : threads) {
			if(thread != null) { // it was removed during iteration
				thread.finish();
				stopped.add(thread);
			}
		}
		return new ResultFuture<Integer>() {
			protected Integer produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				Map<String,InterruptedException> exceptions = waitForComplete(stopped, force, unit.toMillis(timeout));
				if(exceptions != null && !exceptions.isEmpty() && log.isWarnEnabled()) {
					for(Map.Entry<String,InterruptedException> entry : exceptions.entrySet()) {
						log.warn("Could not stop thread '" + entry.getKey() + "'", entry.getValue());
					}	
					return stopped.size() - exceptions.size();
				}
				return stopped.size();
			}
		};
	}

	public Future<Integer> stopThreads(int numThreads, final boolean force) {
		if(numThreads < 1) 
			return new TrivialFuture<Integer>(0);
		log.info("Stopping " + numThreads + " threads on " + getServiceName());
		int n = numThreads;
		final Set<WorkQueueServiceThread> stopped = new HashSet<WorkQueueServiceThread>();
		for(WorkQueueServiceThread thread : threads) {
			if(thread != null) { //it was removed during iteration
				thread.finish();
				stopped.add(thread);
				if(--n < 1) break;
			}
		}
		return new ResultFuture<Integer>() {
			protected Integer produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				Map<String,InterruptedException> exceptions = waitForComplete(stopped, force, unit.toMillis(timeout));
				if(exceptions != null && !exceptions.isEmpty() && log.isWarnEnabled()) {
					for(Map.Entry<String,InterruptedException> entry : exceptions.entrySet()) {
						log.warn("Could not stop thread '" + entry.getKey() + "'", entry.getValue());
					}	
					return stopped.size() - exceptions.size();
				}
				return stopped.size();
			}
		};
	}
	public int pauseThreads() throws ServiceException {
		if(queue instanceof AvailabilityChangeListener)
			((AvailabilityChangeListener) queue).availibilityChanged(null, false);
		int n = 0;
		for(WorkQueueServiceThread thread : threads ) {
			if(thread != null && thread.pause())
				n++;
		}
		return n;
	}

	public int unpauseThreads() throws ServiceException {
		if(queue instanceof AvailabilityChangeListener)
			((AvailabilityChangeListener) queue).availibilityChanged(null, true);
		int n = 0;
		for(WorkQueueServiceThread thread : threads) {
			if(thread.unpause())
				n++;
		}
		return n;
	}
	public WorkQueue<Q> getQueue() {
		return queue;
	}
	public void setQueue(WorkQueue<Q> queue) {
		this.queue = queue;
	}

	public WorkRetryType getDefaultRetryType() {
		return defaultRetryType;
	}

	public void setDefaultRetryType(WorkRetryType defaultRetryPolicy) {
		this.defaultRetryType = defaultRetryPolicy;
	}
	public int getProcessingTimeout() {
		return processingTimeout;
	}

	public void setProcessingTimeout(int processingTimeout) {
		this.processingTimeout = processingTimeout;
	}

	@Override
	public Future<Boolean> prepareShutdown() throws ServiceException {
		WorkQueue<Q> q = getQueue();
		if(q != null)
			return q.shutdown();
		else
			return WorkQueue.TRUE_FUTURE;
	}

	/**
	 * @see simple.app.Service#shutdown()
	 */
	public Future<Boolean> shutdown() {
		final Future<Integer> stopFuture = stopAllThreads(false);
		return new ResultFuture<Boolean>() {
			@Override
			protected Boolean produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				stopFuture.get(timeout, unit);
				if(threads.isEmpty()) {
					return true;
				}
				stopAllThreads(true).get(0, unit);
				return false;
			}
		};
	}
	
	public Advisor getAdvisor() {
		return advisor;
	}

	public void setAdvisor(Advisor advisor) {
		this.advisor = advisor;
	}

	public long getAutoGrowthLatency() {
		return autoGrowthLatency;
	}

	public void setAutoGrowthLatency(long autoGrowthLatency) {
		autoGrowthLock.lock();
		try {
			this.autoGrowthLatency = Math.max(0, autoGrowthLatency);
			calculateAutoGrowthCheck();
		} finally {
			autoGrowthLock.unlock();
		}
	}


	public float getAutoGrowthFactor() {
		return autoGrowthFactor;
	}

	public void setAutoGrowthFactor(float autoGrowthFactor) {
		autoGrowthLock.lock();
		try {
			this.autoGrowthFactor = Math.max(0.0f, autoGrowthFactor);
		} finally {
			autoGrowthLock.unlock();
		}
	}

	public int getAutoGrowthMaxThreads() {
		return autoGrowthMaxThreads;
	}

	public void setAutoGrowthMaxThreads(int autoGrowthMaxThreads) {
		autoGrowthLock.lock();
		try {
			this.autoGrowthMaxThreads = Math.max(0, autoGrowthMaxThreads);
		} finally {
			autoGrowthLock.unlock();
		}
	}

	public int getAutoGrowthWait() {
		return autoGrowthWait;
	}

	public void setAutoGrowthWait(int autoGrowthWait) {
		autoGrowthLock.lock();
		try {
			this.autoGrowthWait = Math.max(0, autoGrowthWait);
		} finally {
			autoGrowthLock.unlock();
		}
	}

	public int getAutoGrowthSampleSize() {
		return autoGrowthSamples.length;
	}

	public void setAutoGrowthSampleSize(int autoGrowthSampleSize) {
		autoGrowthLock.lock();
		try {
			autoGrowthSamples = new long[Math.max(autoGrowthSampleSize, 0)];
			calculateAutoGrowthCheck();
		} finally {
			autoGrowthLock.unlock();
		}
	}

	public long getAutoGrowthMinAvgWait() {
		return autoGrowthMinAvgWait;
	}

	public void setAutoGrowthMinAvgWait(long autoGrowthMinAvgWait) {
		autoGrowthLock.lock();
		try {
			this.autoGrowthMinAvgWait = Math.max(0, autoGrowthMinAvgWait);
			calculateAutoGrowthCheck();
		} finally {
			autoGrowthLock.unlock();
		}
	}

	protected void calculateAutoGrowthCheck() {
		if(autoGrowthLatency > 0) {
			if(autoGrowthMinAvgWait > 0 && autoGrowthSamples.length > 0) {
				autoGrowthCheck = AutoGrowthCheck.LATENCY_AND_AVG_WAIT;
			} else {
				autoGrowthCheck = AutoGrowthCheck.LATENCY;
			}
		} else if(autoGrowthMinAvgWait > 0 && autoGrowthSamples.length > 0) {
			autoGrowthCheck = AutoGrowthCheck.AVG_WAIT;
		} else {
			autoGrowthCheck = AutoGrowthCheck.NONE;
		}
	}

	public int getMaxCancelAttempts() {
		return maxCancelAttempts;
	}

	public void setMaxCancelAttempts(int maxCancelAttempts) {
		this.maxCancelAttempts = maxCancelAttempts;
	}

	public ServiceManagerControl getServiceManagerControl() {
		return serviceManagerControl;
	}

	public void setServiceManagerControl(ServiceManagerControl serviceManagerControl) {
		this.serviceManagerControl = serviceManagerControl;
	}

	public int getPausePerLoopMillis() {
		return pausePerLoopMillis;
	}

	public void setPausePerLoopMillis(int pausePerLoopMillis) {
		this.pausePerLoopMillis = pausePerLoopMillis;
	}

	public int getPausePerLoopIterations() {
		return pausePerLoopIterations;
	}

	public void setPausePerLoopIterations(int pausePerLoopIterations) {
		this.pausePerLoopIterations = pausePerLoopIterations;
	}
}
