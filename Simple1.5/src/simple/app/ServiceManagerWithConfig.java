package simple.app;

import java.beans.IntrospectionException;
import java.text.ParseException;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.configuration.Configuration;

import simple.bean.ConvertException;
import simple.io.Log;
import simple.text.StringUtils;

/**
 * This class configures and controls a set of services based on the properties passed to it.
 * The {@link #configureServiceManager(Properties)} method performs the following configuration steps:
 * <ol>
 * <li><u>Register a {@link simle.db.DataSourceFactory}</u>: The "simple.db.datasource.class" property in the provided Properties determines
 * the implementation of DataSourceFactory that is used. If that property is not provided then {@link simple.db.LocalDataSourceFactory}
 * is used. All other properties that start with "simple.db.datasource." are used to configure the DataSourceFactory. See {@link Main#configure}
 * for details about how this is done. If no properties that start with "simple.db.datasource." are provided, a DataSourceFactory is not registered.
 * But if properties are provided, the configured DataSourceFactory is passed to the
 * {@link simple.db.DataLayerMgr#setDataSourceFactory(simple.db.DataSourceFactory)} method</li>
 * <li><u>Read Data Layer Files</u>: If the "simple.db.DataLayer.files" property is provided, its value is read as a comma- or semi-colon- separated
 * list of data layer files which are read into the default {@link simple.db.DataLayer}. See the
 * <a href="{@docRoot}/simple/resources/data-layer.xsd">Data Layer Schema Definition</a> for more information on the format of these files.</li>
 * <li><u>Configure Services</u>: If a value is provided in the properties for "simple.app.Service.services", it is used as a  comma- or semi-colon- separated
 * list of service names to configure. For each service name, all properties that start with "simple.app.Service.services.<code>&lt;Service Name&gt;</code>." are
 * used to create and configure that service by calling {@link Main#configure}. If the "simple.app.Service.services" property is not provided,
 * this class attempts to use service names of "1", "2", etc until no properties exist for the next number. The "simple.app.Service.services.<code>&lt;Service Name&gt;</code>.threads"
 * property specifies the initial number of threads of each service to start up.</li>
 * <li><u>Configure Service Status Listeners</u>: If a value is provided in the properties for "simple.app.ServiceStatus.listeners", it is used as a comma- or semi-colon- separated
 * list of {@link ServiceStatusListener} names to configure. For each {@link ServiceStatusListener} name, all properties that start with "simple.app.ServiceStatus.<code>&lt;Service Status Listener Name&gt;</code>." are
 * used to create and configure that service by calling {@link Main#configure}. The "simple.app.ServiceStatus.<code>&lt;Service Status Listener Name&gt;</code>.services" property
 * specifies a a comma- or semi-colon- separated list of Services to listen on for each Service Status Listener.
 * </li>
 * <li><u>Register Default Service Status Listener</u>: The {@link ServiceStatusListener} returned from {@link #getDefaultServiceStatusListener}
 * is registered on each service.
 * </li>
 * </ol>
 *
 * @author Brian S. Krug
 *
 */
public class ServiceManagerWithConfig extends AbstractServiceManager<Configuration> {
	private static final Log log = Log.getLog();
	public ServiceManagerWithConfig() {
		super();
	}

	/** Configures the services using the provided properties. See the {@link ServiceManagerWithConfig} class summary for more detail.
	 * @param properties The properties to use to configure the services
	 * @param namedReferences A map of named references
	 * @throws ServiceException If the services could not be configured
	 */
	@Override
	protected void configureServices(Configuration config, Map<String,Object> namedReferences) throws ServiceException {
		Configuration subConfig = config.subset("simple.app.ServiceManager");
		try {
			BaseWithConfig.configureProperties(this, subConfig, namedReferences, false);
		} catch(SecurityException e) {
			throw new ServiceException(e);
		} catch(IllegalArgumentException e) {
			throw new ServiceException(e);
		} catch(IntrospectionException e) {
			throw new ServiceException(e);
		} catch(ParseException e) {
			throw new ServiceException(e);
		} catch(InstantiationException e) {
			throw new ServiceException(e);
		} catch(ConvertException e) {
			throw new ServiceException(e);
		}
		String[] serviceNames = StringUtils.split(config.getString("simple.app.Service.services"), StringUtils.STANDARD_DELIMS, false);
		if(serviceNames != null && serviceNames.length > 0) {
			for(String serviceName : serviceNames) {
				if(addService(config, serviceName.trim(), namedReferences) == null) throw new ServiceException("Could not find properties for service '" + serviceName + "'");
			}
		} else {
			for(int i = 1; true; i++) {
				if(addService(config, String.valueOf(i), namedReferences) == null) break;
			}
		}
		log.debug("Configured " + services.size() + " service(s)");
	}

	protected Service addService(Configuration config, String serviceName, Map<String,Object> namedReferences) throws ServiceException {
		Configuration subConfig = config.subset("simple.app.Service." + serviceName);
    	if(subConfig.isEmpty())
    		return null;
		Service service = configureService(subConfig, namedReferences, serviceName);
		services.add(service);
		int threads = subConfig.getInt("threads", 1);
		initialThreads.put(service, threads);
		return service;
	}

	@Override
	protected void configureStatics(Configuration config, Map<String, Object> namedReferences) throws ServiceException {
		BaseWithConfig.configureStatics(config, namedReferences);
	}

	/**
	 * @throws ServiceException
	 */
	@Override
	protected void configureSystem(Configuration config, Map<String,Object> namedReferences) throws ServiceException {
		configureEditorSearchPath(config.getString(EDITOR_SEARCH_PATH));
		BaseWithConfig.configureSystem(config, namedReferences);
	}

	@Override
	protected void configureDataLayers(Configuration config, Map<String,Object> namedReferences) throws ServiceException {
		BaseWithConfig.configureDataLayer(config);
	}

	@Override
	protected void configureDataSources(Configuration config, Map<String,Object> namedReferences) throws ServiceException {
		BaseWithConfig.configureDataSourceFactory(config, namedReferences);
	}

	@Override
	protected void configureServiceStatusListeners(Configuration config, Map<String,Object> namedReferences) throws ServiceException {
		String[] serviceStatusListeners = StringUtils.split(config.getString("simple.app.ServiceStatus.listeners"), StringUtils.STANDARD_DELIMS, false);
		if(serviceStatusListeners != null) {
			for(String listenerName : serviceStatusListeners) {
				Configuration subConfig = config.subset("simple.app.ServiceStatus." + listenerName);
				ServiceStatusListener ssl = BaseWithConfig.configure(ServiceStatusListener.class, subConfig, namedReferences, true);
		    	String[] serviceNames = StringUtils.split(subConfig.getString("services"), StringUtils.STANDARD_DELIMS, false);
				for(String serviceName : serviceNames) {
					Service service = findService(serviceName);
					if(service == null) throw new ServiceException("Service '" + serviceName + "' could not be found as requested by ServiceStatusListener '" + listenerName + "'");
					service.addServiceStatusListener(ssl);
				}
			}
		}
		ServiceStatusListener ssl = getDefaultServiceStatusListener();
		if(ssl != null)
			for(Service service : services) {
				service.addServiceStatusListener(ssl);
			}
		long inactivityThreshhold = config.getLong("simple.app.ServiceManager.inactivityThreshhold", 0);
		if(inactivityThreshhold > 0) {
			long shutdownTimeout = config.getLong("simple.app.ServiceManager.inactivityShutdownTimeout", 0);
			Prerequisite pre = BaseWithConfig.configureFromBase(Prerequisite.class, "simple.app.ServiceManager.shutdownAntirequisite", config, namedReferences, true);
			configureWatchdog(inactivityThreshhold, shutdownTimeout, pre);
		}
	}

	protected Service configureService(Configuration config, Map<String,Object> namedReferences, String serviceName) throws ServiceException {
		Service service = BaseWithConfig.configure(Service.class, config, namedReferences, true, serviceName);
		if(service instanceof ControllingService)
			((ControllingService)service).setServiceManagerControl(this);
		return service;
	}

	/** Configures the commanders using the provided properties. See the {@link ServiceManager} class summary for more detail.
	 * @param properties The properties to use to configure the services
	 * @param namedReferences A map of named references
	 * @throws ServiceException If the commanders could not be configured
	 */
	@Override
	protected void configureCommanders(Configuration config, Map<String,Object> namedReferences) throws ServiceException {
		String[] commanderNames = StringUtils.split(config.getString("simple.app.Commanders"), StringUtils.STANDARD_DELIMS, false);
		if(commanderNames != null && commanderNames.length > 0) {
			for(String commanderName : commanderNames) {
				addCommander(config, commanderName.trim(), namedReferences);
			}
		} else {
			for(int i = 1; true; i++) {
				if(addCommander(config, String.valueOf(i), namedReferences) == null) break;
			}
		}
		log.debug("Configured " + commanders.size() + " commander(s)");
	}

	@Override
	protected Commander addCommander(Configuration config, String commanderName, Map<String,Object> namedReferences) throws ServiceException {
		Configuration subConfig = config.subset("simple.app.Commander." + commanderName);
    	if(subConfig.isEmpty())
    		return null;
		Commander commander = configureCommander(subConfig, namedReferences);
		addCommander(commander);
		return commander;
	}

	protected Commander configureCommander(Configuration subConfig, Map<String,Object> namedReferences) throws ServiceException {
		return BaseWithConfig.configure(Commander.class, subConfig, namedReferences, true);
	}
}
