package simple.app;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

/** This class is the starting point for the Simple Service Framework. It is designed to allow easy configuration of a set of
 * "Services" to run. The <code>main()</code> method of this class performs the following operations:
 * <ol><li><u>Parses the command line arguments</u>: the methods {@link #registerDefaultActions}
 *  and {@link #registerDefaultCommandLineArguments} establish what actions and options are available. The default implementation setups up three
 *  actions: <code>start</code>, <code>restart</code>, and <code>stop</code> and registers two command line switches: <code>-p</code> (the properties file location)
 *  and <code>-i</code> the port on which to listen for management commands and one command line argument: <code>action</code> which is used to determine
 *  which action to perform. Run <code>java simple.app.Main</code> without arguments to see a usage message printed to standard output. To add or change
 *  the default command line switches or arguments, sub-class this class and override the {@link #registerDefaultActions}
 *  and {@link #registerDefaultCommandLineArguments} methods. When you do so, your custom switches and actions will be part of the usage message
 *  that is printed if arguments are not provided or are incorrect.</li>
 *  <li><u>Loads the properties file</u>: If the <code>-p</code> switch is provided with a file name after it, that file is loaded from the classpath. If no properties file
 *  name is provided the name of this class with ".properties" appended is used and if such a file exists in the classpath, it is loaded</li>
 *  <li><u>Performs the action specified</u>: The <code>start</code> action creates, configures, and starts a {@link simple.app.ServiceManager}
 *  instance using the properties loaded in the previous step and then listens on the specified port for management commands. See {@link ServiceManager} for further information. The <code>restart</code> action
 *  sends "restart" to the management port and waits for a response. The <code>stop</code> action
 *  sends "quit" to the management port and waits for a response. If the <code>-i</code> switch is not provided, the <code>manager-port</code>
 *  property in the properties loaded by the previous step is used to determine the management port used. If none is provided, the <code>start</code>
 *  action will use standard in and out, but the <code>restart</code> and <code>stop</code> actions will not work.</li></ol>
 *
 *
 * @author Brian S. Krug
 * @see ServiceManager
 * @see Service
 */
public class Main extends Base {
	public static interface Action {
		public void perform(Map<String,Object> argumentMap, Properties properties) ;
	}

	protected Map<String,Action> actions;

	public Main() {
		super();
	}

	@Override
	protected void init() {
		actions = new LinkedHashMap<String, Action>();
		registerDefaultActions();
		super.init();

	}
	/** Registers the specified {@link Action}
	 * @param actionKey The argument value that points to the action
	 * @param action The {@link Action} to execute if the actionKey is used as the action argument
	 * @return <code>true</code> if this is a new actionKey, <code>false</code> if this action is replacing
	 * an already registered one
	 */
	public boolean registerAction(String actionKey, Action action) {
		return actions.put(actionKey, action) == null;
	}

	/** Deregisters the action associated with the specified <code>actionKey</code>
	 * @param actionKey The argument value that points to the action ro deregister
	 * @return <code>true</code> if the actionKey was deregistered, <code>false</code> if the actionKey was not
	 * previously registered
	 */
	public boolean deregisterAction(String actionKey) {
		return actions.remove(actionKey) != null;
	}

	protected void registerDefaultActions() {
		registerAction("start", new Action() {
			public void perform(Map<String, Object> argumentMap, Properties properties) {
				int managerPort;
				try {
					managerPort = ConvertUtils.getInt(properties.get("manager-port"), 0);
				} catch(ConvertException e) {
					finishAndExit("Could not convert '" + properties.get("manager-port") + "' to a port number", 180, null);
					return;
				}
				ServiceManager sm;
				try {
					sm = getServiceManager(properties);
				} catch(ServiceException e) {
					finishAndExit("Could not create service manager", 200, e);
					return;
				} catch(RuntimeException e) {
					finishAndExit("Could not create service manager", 201, e);
					return;
				} catch(Error e) {
					finishAndExit("Could not create service manager", 202, e);
					return;
				}
				try {
					sm.configureServiceManager(properties);
				} catch(ServiceException e) {
					finishAndExit("Could not configure service manager", 210, e);
					return;
				} catch(RuntimeException e) {
					finishAndExit("Could not configure service manager", 211, e);
					return;
				} catch(Error e) {
					finishAndExit("Could not configure service manager", 212, e);
					return;
				}
				if(managerPort <= 0) {
					if(sm.getCommanders().isEmpty()) {
						sm.addCommander(new InOutCommander());
					}
				} else {
					String remoteAddressRegex = ConvertUtils.getStringSafely(properties.get("control-address-regex"));
					if(remoteAddressRegex == null || (remoteAddressRegex=remoteAddressRegex.trim()).length() == 0)
						remoteAddressRegex = null;
					try {
						sm.addCommander(new NIOSelectorPortCommander(managerPort, remoteAddressRegex));
					} catch(IOException e) {
						finishAndExit("Error occured while creating commander on port " + managerPort, 220, e);
						return;
					} catch(InterruptedException e) {
						finishAndExit("Error occured while creating commander on port " + managerPort, 220, e);
						return;
					}
				}
				try {
					sm.run();
				} catch(ServiceException e) {
					finishAndExit("Error occured while running service manager", 250, e);
					return;
				} catch(RuntimeException e) {
					finishAndExit("Error occured while running service manager", 251, e);
					return;
				} catch(Error e) {
					finishAndExit("Error occured while running service manager", 252, e);
					return;
				}
			}
		});
		registerAction("restart", new Action() {
			public void perform(Map<String, Object> argumentMap, Properties properties) {
				int managerPort;
				try {
					managerPort = ConvertUtils.getInt(properties.get("manager-port"), 0);
				} catch(ConvertException e) {
					finishAndExit("Could not convert '" + properties.get("manager-port") + "' to a port number", 180, e);
					return;
				}
				long timeout;
				try {
					timeout = ConvertUtils.getLong(properties.get("timeout"), 0);
				} catch(ConvertException e) {
					finishAndExit("Could not convert '" + properties.get("timeout") + "' to a number", 180, e);
					return;
				}
				String response;
				try {
					response = sendCommand("localhost", managerPort, "restart" + (timeout > 0 ? " " + timeout : ""));
				} catch(IOException e) {
					finishAndExit("Error occured while running contacting process", 260, e);
					return;
				}
				finishAndExit("Process responded with:\n" + response, 0, null);
			}
		});

		registerAction("stop", new Action() {
			public void perform(Map<String, Object> argumentMap, Properties properties) {
				int managerPort;
				try {
					managerPort = ConvertUtils.getInt(properties.get("manager-port"), 0);
				} catch(ConvertException e) {
					finishAndExit("Could not convert '" + properties.get("manager-port") + "' to a port number", 180, e);
					return;
				}
				long timeout;
				try {
					timeout = ConvertUtils.getLong(properties.get("timeout"), 0);
				} catch(ConvertException e) {
					finishAndExit("Could not convert '" + properties.get("timeout") + "' to a number", 180, e);
					return;
				}
				String response;
				try {
					response = sendCommand("localhost", managerPort, "quit" + (timeout > 0 ? " " + timeout : ""));
				} catch(IOException e) {
					finishAndExit("Error occured while running contacting process", 260, e);
					return;
				}
				finishAndExit("Process responded with:\n" + response, 0, null);
			}
		});
	}
	@Override
	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
		registerCommandLineSwitch('i', "managerPort", true, true, "manager-port", "The port on which to listen for management commands");
		registerCommandLineSwitch('a', "controlAddressRegex", true, true, "control-address-regex", "The regular express of remote IP addresses or hostnames from which to allow connections for management commands");

		StringBuilder sb = new StringBuilder();
		sb.append("The action to take. One of ");
		for(Iterator<String> iter = actions.keySet().iterator(); iter.hasNext(); ) {
			String actionKey = iter.next();
			sb.append(", ");
			if(iter.hasNext())
				sb.append('\'');
			else
				sb.append("or '");
			sb.append(actionKey);
			sb.append('\'');
		}
		registerCommandLineArgument(false, "action", sb.toString());
		registerCommandLineArgument(true, "timeout", "The number of milliseconds to wait for the action to complete");
	}

	/**
	 * @see simple.app.Base#execute(java.util.Map, java.util.Properties)
	 */
	@Override
	protected void execute(Map<String, Object> argMap, Properties properties) {
		String actionKey = (String)argMap.get("action");
		if(actionKey == null) {
			finishAndExit("No action specified on the command line", 150, true, null);
			return;
		} else {
			Action action = actions.get(actionKey);
			if(action != null) {
				action.perform(argMap, properties);
			} else {
				finishAndExit("Invalid action '" + actionKey + "' specified on the command line", 170, true, null);
				return;
			}
		}
	}

	/** Instantiates the ServiceManager. Override to provide a custom ServiceManager.
	 * @param properties Properties to instantiate the ServiceManager
	 * @return The ServiceManager
	 * @throws ServiceException
	 */
	protected ServiceManager getServiceManager(Properties properties) throws ServiceException {
		return new ServiceManager();
	}

	@Override
	protected boolean allowExtraArguments(Map<String, Object> argMap) {
		Object actionKey = argMap.get("action");
		return actionKey == null || (!"start".equals(actionKey) && !"restart".equals(actionKey) && !"stop".equals(actionKey));
	}

	/** The start point of the application
	 * @param args
	 */
	public static void main(String[] args) {
		new Main().run(args);
	}
}
