package simple.app;

public abstract class AbstractWorkQueue<J> implements WorkQueue<J>, AvailabilityChangeListener {
	protected String queueKey;
	protected RetryPolicy retryPolicy;
	protected final PrerequisiteManager prerequisiteManager;

	public AbstractWorkQueue() {
		prerequisiteManager = new PrerequisiteManager();
		prerequisiteManager.addAvailabilityChangeListener(this);
	}
	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}
	public RetryPolicy getRetryPolicy() {
		RetryPolicy rp = retryPolicy;
		if(rp == null) {
			rp = retryPolicy = new RetryPolicy(10, 3500, 3.0f);
		}
		return rp;
	}
	public void setRetryPolicy(RetryPolicy retryPolicy) {
		this.retryPolicy = retryPolicy;
	}
	public PrerequisiteManager getPrerequisiteManager() {
		return prerequisiteManager;
	}

	@Override
	public void availibilityChanged(Prerequisite prerequisite, boolean available) {
	}
}
