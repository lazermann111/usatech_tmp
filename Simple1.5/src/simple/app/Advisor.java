package simple.app;

public interface Advisor {
	public static enum AdviseType {
		UP, DOWN, ACTION, ERROR, WARNING
	}
	public void advise(AdviseType adviseType, String category, String item, String details) ;
}
