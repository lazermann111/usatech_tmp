package simple.app;

import java.util.Map;
import java.util.Properties;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

/**
 * This class configures and controls a set of services based on the properties passed to it.
 * The {@link #configureServiceManager(Properties)} method performs the following configuration steps:
 * <ol>
 * <li><u>Register a {@link simle.db.DataSourceFactory}</u>: The "simple.db.datasource.class" property in the provided Properties determines
 * the implementation of DataSourceFactory that is used. If that property is not provided then {@link simple.db.LocalDataSourceFactory}
 * is used. All other properties that start with "simple.db.datasource." are used to configure the DataSourceFactory. See {@link Main#configure}
 * for details about how this is done. If no properties that start with "simple.db.datasource." are provided, a DataSourceFactory is not registered.
 * But if properties are provided, the configured DataSourceFactory is passed to the
 * {@link simple.db.DataLayerMgr#setDataSourceFactory(simple.db.DataSourceFactory)} method</li>
 * <li><u>Read Data Layer Files</u>: If the "simple.db.DataLayer.files" property is provided, its value is read as a comma- or semi-colon- separated
 * list of data layer files which are read into the default {@link simple.db.DataLayer}. See the
 * <a href="{@docRoot}/simple/resources/data-layer.xsd">Data Layer Schema Definition</a> for more information on the format of these files.</li>
 * <li><u>Configure Services</u>: If a value is provided in the properties for "simple.app.Service.services", it is used as a  comma- or semi-colon- separated
 * list of service names to configure. For each service name, all properties that start with "simple.app.Service.services.<code>&lt;Service Name&gt;</code>." are
 * used to create and configure that service by calling {@link Main#configure}. If the "simple.app.Service.services" property is not provided,
 * this class attempts to use service names of "1", "2", etc until no properties exist for the next number. The "simple.app.Service.services.<code>&lt;Service Name&gt;</code>.threads"
 * property specifies the initial number of threads of each service to start up.</li>
 * <li><u>Configure Service Status Listeners</u>: If a value is provided in the properties for "simple.app.ServiceStatus.listeners", it is used as a comma- or semi-colon- separated
 * list of {@link ServiceStatusListener} names to configure. For each {@link ServiceStatusListener} name, all properties that start with "simple.app.ServiceStatus.<code>&lt;Service Status Listener Name&gt;</code>." are
 * used to create and configure that service by calling {@link Main#configure}. The "simple.app.ServiceStatus.<code>&lt;Service Status Listener Name&gt;</code>.services" property
 * specifies a a comma- or semi-colon- separated list of Services to listen on for each Service Status Listener.
 * </li>
 * <li><u>Register Default Service Status Listener</u>: The {@link ServiceStatusListener} returned from {@link #getDefaultServiceStatusListener}
 * is registered on each service.
 * </li>
 * </ol>
 *
 * @author Brian S. Krug
 *
 */
public class ServiceManager extends AbstractServiceManager<Properties>{
	private static final Log log = Log.getLog();

	public ServiceManager() {
		super();
	}

	@Override
	protected void configureStatics(Properties properties, Map<String, Object> namedReferences) throws ServiceException {
		Base.configureStatics(properties, namedReferences);
	}

	/**
	 * @throws ServiceException
	 */
	@Override
	protected void configureSystem(Properties properties, Map<String,Object> namedReferences) throws ServiceException {
		configureEditorSearchPath(properties.getProperty(EDITOR_SEARCH_PATH));
		Base.configureSystem(properties, namedReferences);
	}

	@Override
	protected void configureDataLayers(Properties properties, Map<String,Object> namedReferences) throws ServiceException {
		Base.configureDataLayer(properties);
	}

	@Override
	protected void configureDataSources(Properties properties, Map<String,Object> namedReferences) throws ServiceException {
		Base.configureDataSourceFactory(properties, namedReferences);
	}

	/** Configures the services using the provided properties. See the {@link ServiceManager} class summary for more detail.
	 * @param properties The properties to use to configure the services
	 * @param namedReferences A map of named references
	 * @throws ServiceException If the services could not be configured
	 */
	@Override
	protected void configureServices(Properties properties, Map<String,Object> namedReferences) throws ServiceException {
		String sn = properties.getProperty("simple.app.Service.services");
		if(sn != null) {
			String[] serviceNames = StringUtils.split(sn, StringUtils.STANDARD_DELIMS, false);
			for(String serviceName : serviceNames) {
				addService(properties, serviceName.trim(), namedReferences);
			}
		} else {
			for(int i = 1; true; i++) {
				if(addService(properties, String.valueOf(i), namedReferences) == null) break;
			}
		}
		log.debug("Configured " + services.size() + " service(s)");
	}

	protected Service addService(Properties properties, String serviceName, Map<String,Object> namedReferences) throws ServiceException {
		Properties subProperties = CollectionUtils.getSubProperties(properties, "simple.app.Service." + serviceName + '.');
    	if(subProperties.isEmpty())
    		return null;
		Service service = configureService(subProperties, properties, namedReferences, serviceName);
		services.add(service);
		int numThread = 1;
		String threads = subProperties.getProperty("threads");
		if(threads != null) {
			try {
				numThread = ConvertUtils.getInt(threads, 1);
			} catch(ConvertException e) {
				log.warn("Could not convert '" + threads + "' to a number for 'simple.app.Service." + service.getServiceName() + ".threads'; using none", e);
				numThread = 0;
			}
		}
		initialThreads.put(service, numThread);
		return service;
	}

	protected Service configureService(Properties subProperties, Properties rootProperties, Map<String,Object> namedReferences, String serviceName) throws ServiceException {
		Service service = Main.configure(Service.class, subProperties, rootProperties, namedReferences, true, serviceName);
		if(service instanceof ControllingService)
			((ControllingService)service).setServiceManagerControl(this);
		return service;
	}

	/** Configures the commanders using the provided properties. See the {@link ServiceManager} class summary for more detail.
	 * @param properties The properties to use to configure the services
	 * @param namedReferences A map of named references
	 * @throws ServiceException If the commanders could not be configured
	 */
	@Override
	protected void configureCommanders(Properties properties, Map<String,Object> namedReferences) throws ServiceException {
		String cn = properties.getProperty("simple.app.Commanders");
		if(cn != null) {
			String[] commanderNames = StringUtils.split(cn, StringUtils.STANDARD_DELIMS, false);
			for(String commanderName : commanderNames) {
				addCommander(properties, commanderName.trim(), namedReferences);
			}
		} else {
			for(int i = 1; true; i++) {
				if(addCommander(properties, String.valueOf(i), namedReferences) == null) break;
			}
		}
		log.debug("Configured " + services.size() + " commander(s)");
	}

	@Override
	protected Commander addCommander(Properties properties, String commanderName, Map<String,Object> namedReferences) throws ServiceException {
		Properties subProperties = CollectionUtils.getSubProperties(properties, "simple.app.Commander." + commanderName + '.');
    	if(subProperties.isEmpty())
    		return null;
    	Commander commander = configureCommander(subProperties, properties, namedReferences);
    	addCommander(commander);
		return commander;
	}

	@Override
	protected Commander configureCommander(Properties subProperties, Properties rootProperties, Map<String,Object> namedReferences) throws ServiceException {
		return Main.configure(Commander.class, subProperties, rootProperties, namedReferences, true);
	}

	@Override
	protected void configureServiceStatusListeners(Properties properties, Map<String,Object> namedReferences) throws ServiceException {
		String s = properties.getProperty("simple.app.ServiceStatus.listeners");
		if(s != null) {
			String[] serviceStatusListeners = StringUtils.split(s, StringUtils.STANDARD_DELIMS, false);
			for(String listenerName : serviceStatusListeners) {
				Properties subProperties = CollectionUtils.getSubProperties(properties, "simple.app.ServiceStatus." + listenerName + '.');
		    	ServiceStatusListener ssl = Main.configure(ServiceStatusListener.class, subProperties, properties, namedReferences, true);
		    	String sn = subProperties.getProperty("services");
		    	String[] serviceNames = StringUtils.split(sn, StringUtils.STANDARD_DELIMS, false);
				for(String serviceName : serviceNames) {
					Service service = findService(serviceName);
					if(service == null) throw new ServiceException("Service '" + serviceName + "' could not be found as requested by ServiceStatusListener '" + listenerName + "'");
					service.addServiceStatusListener(ssl);
				}
			}
		}
		ServiceStatusListener ssl = getDefaultServiceStatusListener();
		if(ssl != null)
			for(Service service : services) {
				service.addServiceStatusListener(ssl);
			}
		long inactivityThreshhold = ConvertUtils.getLongSafely(properties.get("simple.app.ServiceManager.inactivityThreshhold"), 0);
		if(inactivityThreshhold > 0) {
			long shutdownTimeout = ConvertUtils.getLongSafely(properties.get("simple.app.ServiceManager.inactivityShutdownTimeout"), 0);
			Prerequisite pre = Main.configureFromBase(Prerequisite.class, "simple.app.ServiceManager.shutdownAntirequisite", properties, namedReferences, true);
			configureWatchdog(inactivityThreshhold, shutdownTimeout, pre);
		}
	}
}
