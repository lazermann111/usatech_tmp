/**
 *
 */
package simple.app;

/**
 * @author Brian S. Krug
 *
 */
public class RetryPolicy {
	protected int maximumRedeliveries;
	protected long initialRedeliveryDelay;
	protected float backOffMultiplier;

	public RetryPolicy() {
	}
	public RetryPolicy(int maximumRedeliveries, long initialRedeliveryDelay, float backOffMultiplier) {
		this.maximumRedeliveries = maximumRedeliveries;
		this.initialRedeliveryDelay = initialRedeliveryDelay;
		this.backOffMultiplier = backOffMultiplier;
	}
	public int getMaximumRedeliveries() {
		return maximumRedeliveries;
	}
	public void setMaximumRedeliveries(int maximumRedeliveries) {
		this.maximumRedeliveries = maximumRedeliveries;
	}
	public long getInitialRedeliveryDelay() {
		return initialRedeliveryDelay;
	}
	public void setInitialRedeliveryDelay(long initialRedeliveryDelay) {
		this.initialRedeliveryDelay = initialRedeliveryDelay;
	}
	public float getBackOffMultiplier() {
		return backOffMultiplier;
	}
	public void setBackOffMultiplier(float backOffMultiplier) {
		this.backOffMultiplier = backOffMultiplier;
	}
	public long calculateDelay(int redeliveryAttempts) {
		if(redeliveryAttempts <= 0 || getBackOffMultiplier() <= 0.0)
			return getInitialRedeliveryDelay();
		else
			return (long)(getInitialRedeliveryDelay() * Math.pow(getBackOffMultiplier(), redeliveryAttempts));
	}
}
