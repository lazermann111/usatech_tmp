package simple.app;

import java.beans.IntrospectionException;
import java.beans.PropertyEditorManager;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.remote.rmi.RMIConnectorServer;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

import com.sun.jmx.remote.security.MBeanServerFileAccessController;
import com.sun.jmx.snmp.InetAddressAcl;
import com.sun.jmx.snmp.IPAcl.SnmpAcl;
import com.sun.jmx.snmp.agent.SnmpMib;
import com.sun.jmx.snmp.daemon.SnmpAdaptorServer;

import simple.app.jmx.ServiceManagerDynamicMBean;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.security.provider.SimpleProvider;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.concurrent.CustomThreadFactory;
import sun.management.snmp.jvminstr.JVM_MANAGEMENT_MIB_IMPL;
import sun.management.snmp.jvminstr.NotificationTarget;
import sun.management.snmp.jvminstr.NotificationTargetImpl;
import sun.management.snmp.util.JvmContextFactory;

/**
 * This class configures and controls a set of services based on the properties passed to it.
 * The {@link #configureServiceManager(Properties)} method performs the following configuration steps:
 * <ol>
 * <li><u>Register a {@link simle.db.DataSourceFactory}</u>: The "simple.db.datasource.class" property in the provided Properties determines
 * the implementation of DataSourceFactory that is used. If that property is not provided then {@link simple.db.LocalDataSourceFactory}
 * is used. All other properties that start with "simple.db.datasource." are used to configure the DataSourceFactory. See {@link Main#configure}
 * for details about how this is done. If no properties that start with "simple.db.datasource." are provided, a DataSourceFactory is not registered.
 * But if properties are provided, the configured DataSourceFactory is passed to the
 * {@link simple.db.DataLayerMgr#setDataSourceFactory(simple.db.DataSourceFactory)} method</li>
 * <li><u>Read Data Layer Files</u>: If the "simple.db.DataLayer.files" property is provided, its value is read as a comma- or semi-colon- separated
 * list of data layer files which are read into the default {@link simple.db.DataLayer}. See the
 * <a href="{@docRoot}/simple/resources/data-layer.xsd">Data Layer Schema Definition</a> for more information on the format of these files.</li>
 * <li><u>Configure Services</u>: If a value is provided in the properties for "simple.app.Service.services", it is used as a  comma- or semi-colon- separated
 * list of service names to configure. For each service name, all properties that start with "simple.app.Service.services.<code>&lt;Service Name&gt;</code>." are
 * used to create and configure that service by calling {@link Main#configure}. If the "simple.app.Service.services" property is not provided,
 * this class attempts to use service names of "1", "2", etc until no properties exist for the next number. The "simple.app.Service.services.<code>&lt;Service Name&gt;</code>.threads"
 * property specifies the initial number of threads of each service to start up.</li>
 * <li><u>Configure Service Status Listeners</u>: If a value is provided in the properties for "simple.app.ServiceStatus.listeners", it is used as a comma- or semi-colon- separated
 * list of {@link ServiceStatusListener} names to configure. For each {@link ServiceStatusListener} name, all properties that start with "simple.app.ServiceStatus.<code>&lt;Service Status Listener Name&gt;</code>." are
 * used to create and configure that service by calling {@link Main#configure}. The "simple.app.ServiceStatus.<code>&lt;Service Status Listener Name&gt;</code>.services" property
 * specifies a a comma- or semi-colon- separated list of Services to listen on for each Service Status Listener.
 * </li>
 * <li><u>Register Default Service Status Listener</u>: The {@link ServiceStatusListener} returned from {@link #getDefaultServiceStatusListener}
 * is registered on each service.
 * </li>
 * </ol>
 *
 * @author Brian S. Krug
 *
 */
public abstract class AbstractServiceManager<P> extends AbstractCommandManager<List<Service>> implements ServiceManagerControl {
	private static final Log log = Log.getLog();
	public static final String EDITOR_SEARCH_PATH = "java.beans.PropertyEditorManager.searchPath";
	protected final List<Service> services = new ArrayList<Service>();
	protected final List<Service> servicesUnmod = Collections.unmodifiableList(services);
	protected final List<Commander> commanders = new ArrayList<Commander>();
	protected final List<Commander> commandersUnmod = Collections.unmodifiableList(commanders);
	protected final Map<Service,Integer> initialThreads = new HashMap<Service,Integer>();
	protected long defaultThreadTimeout = 2 * 60 * 1000; //2 minutes;
	protected long defaultShutdownTimeout = 1 * 60 * 1000; // 1 minutes;
	protected ServiceStatusListener defaultServiceStatusListener;
	protected final CountDownLatch exitSignal = new CountDownLatch(1);
	protected ObjectName mBeanName;
	protected final Map<String, Object> namedReferences = new HashMap<String, Object>();
	protected final ScheduledThreadPoolExecutor inactivityExecutorService = new ScheduledThreadPoolExecutor(1, new CustomThreadFactory("InactivityWatchdog-", true, 2));
	protected final ThreadPoolExecutor shutdownExecutor = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 5000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new CustomThreadFactory("ShutdownExecutor-", true, 2));
	protected class Watchdog implements ServiceStatusListener, Runnable {
		protected final Prerequisite shutdownAntirequisite;
		protected final long shutdownTimeout;
		protected final long inactivityTimeout;
		protected ScheduledFuture<?> inactivityFuture;
		protected final ReentrantLock lock = new ReentrantLock();
		protected boolean executed = false;
		public Watchdog(long inactivityTimeout, long shutdownTimeout, Prerequisite shutdownAntirequisite) {
			this.shutdownTimeout = shutdownTimeout;
			this.inactivityTimeout = inactivityTimeout;
			this.shutdownAntirequisite = shutdownAntirequisite;
			resetInactivityTimer();
		}
		public void serviceStatusChanged(ServiceStatusInfo serviceStatusInfo) {
			switch(serviceStatusInfo.getServiceStatus()) {
				case ACKNOWLEDGING_MSG:
				case STARTING:
				case STARTED:
					resetInactivityTimer();
					break;
			}
		}
		protected void resetInactivityTimer() {
			lock.lock();
			try {
				if(executed) {
					log.info("Inactivity Watchdog was reset after it already ran");
					return; // only do this once
				}
				if(inactivityFuture != null) {
					if(!(inactivityFuture instanceof RunnableScheduledFuture<?>)
							|| !inactivityExecutorService.remove((RunnableScheduledFuture<?>)inactivityFuture)) {
						if(!inactivityFuture.cancel(false))
							log.warn("Failed to cancel Inactivity Watchdog");	
					}
				}
				inactivityFuture = inactivityExecutorService.schedule(this, inactivityTimeout, TimeUnit.MILLISECONDS);
			} finally {
				lock.unlock();
			}
		}
		public void run() {
			String message;
			Throwable throwable = null;
			// last ditch effort to see if app is still viable
			if(shutdownAntirequisite != null) {
				Future<Boolean> shutdownAntiResult = shutdownExecutor.submit(new Callable<Boolean>() {
					public Boolean call() throws Exception {
						return shutdownAntirequisite.isAvailable();
					}
				});
				try {
					boolean preventShutdown = shutdownAntiResult.get(shutdownTimeout, TimeUnit.MILLISECONDS);
					if(preventShutdown) {
						log.info("Detected no activity for " + inactivityTimeout + " milliseconds on any service thread but ShutdownAntirequisite still reports that the app is viable. Not shutting down.");
						resetInactivityTimer();
						return;
					} else {
						message = "Detected no activity for " + inactivityTimeout + " milliseconds on any service thread and ShutdownAntirequisite reports that the app is NOT viable. Shutting down application...";
					}
				} catch(InterruptedException e) {
					message = "Detected no activity for " + inactivityTimeout + " milliseconds on any service thread and ShutdownAntirequisite was interrupted. Shutting down application...";
					throwable = e;
				} catch(ExecutionException e) {
					message = "Detected no activity for " + inactivityTimeout + " milliseconds on any service thread and ShutdownAntirequisite threw an exception. Shutting down application...";
					throwable = e;
				} catch(TimeoutException e) {
					message = "Detected no activity for " + inactivityTimeout + " milliseconds on any service thread and ShutdownAntirequisite did not complete in " + shutdownTimeout + " milliseconds. Shutting down application...";
				} catch(RuntimeException e) {
					message = "Detected no activity for " + inactivityTimeout + " milliseconds on any service thread and ShutdownAntirequisite threw an exception. Shutting down application...";
					throwable = e;
				} catch(Error e) {
					message = "Detected no activity for " + inactivityTimeout + " milliseconds on any service thread and ShutdownAntirequisite threw an error. Shutting down application...";
					throwable = e;
				}				
			} else {
				message = "Detected no activity for " + inactivityTimeout + " milliseconds on any service thread. Shutting down application...";
			}
			lock.lock();
			try {				
				if(executed) {
					log.info("Inactivity Watchdog ran again after it already ran");
					return; // only do this once
				}
				executed = true;
			} finally {
				lock.unlock();
			}
			// release lock so things that happen in shutdown won't block on it
			log.warn(message, throwable);		
			// print stack trace
			File dir = new File("logs");
			if(!dir.isDirectory())
				dir = new File(".");
			try {
				SystemUtils.dumpThreads(new FileWriter(new File(dir, "inactivity_thread_dump_" + System.currentTimeMillis() + ".dmp")), true, true);
			} catch(IOException e) {
				log.error("Could not dump threads", e);
			}
			terminate(shutdownTimeout, 20000);
		}
	}
	
	public AbstractServiceManager() {
		super();
		namedReferences.put("simple.app.ServiceManager", this);
	}
	/**
	 * @return the services
	 */
	public List<Service> getServices() {
		return servicesUnmod;
	}

	protected Service findService(String serviceName) {
		for(Service s : services) {
			if(s.getServiceName().equals(serviceName))
				return s;
		}
		return null;
	}

	/**
	 * @return the commanders
	 */
	public List<Commander> getCommanders() {
		return commandersUnmod;
	}

	protected void setStaticProperty(String property, Object value) {
		try {
			if(!ReflectionUtils.setStaticProperty(property, value)) {
				log.info("Could not set static property '" + property + "'");
			}
		} catch(ClassCastException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(IllegalArgumentException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(IntrospectionException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(IllegalAccessException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(InvocationTargetException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(InstantiationException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(ConvertException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(ParseException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(NoSuchMethodException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		}
	}

	/** Registers the following commands:
	 * <ul><li>pause</li>
	 * <li>unpause</li>
	 * <li>quit</li>
	 * <li>status</li>
	 * <li>restart</li>
	 * <li>threads</li>
	 * </ol>
	 * The {@link Commander} will print a command usage message when it starts processing commands. Also,
	 * {@link AbstractCommandManager#printHelp(PrintWriter)} will print the command usage message that describes the registered commands.
	 * @see simple.app.AbstractCommandManager#registerDefaultCommands()
	 */
	@Override
	protected void registerDefaultCommands() {
		registerCommand(new AbstractCommand<List<Service>>("pause", "Pause processing", null)  {
			public boolean executeCommand(List<Service> services, PrintWriter out, Object[] arguments) {
				List<String> completed = new ArrayList<String>();
				for(Service service : services) {
					try {
						service.pauseThreads();
						completed.add(service.getServiceName());
					} catch(ServiceException e) {
						if(completed.isEmpty()) {
							out.println("Did not pause any services because error occured while pausing threads on service '" + service.getServiceName() + "':");
						} else {
							out.println("Paused threads on the following services: '" + StringUtils.join(completed, "', '."));
							out.println("But an error occured while pausing threads on service '" + service.getServiceName() + "':");

						}
						e.printStackTrace(out);
						return true;
					}
				}
				out.println("Services are paused...");
    			return true;
			}
		});
		registerCommand(new AbstractCommand<List<Service>>("unpause", "Unpause processing", null)  {
			public boolean executeCommand(List<Service> services, PrintWriter out, Object[] arguments) {
				List<String> completed = new ArrayList<String>();
				for(Service service : services) {
					try {
						service.unpauseThreads();
						completed.add(service.getServiceName());
					} catch(ServiceException e) {
						if(completed.isEmpty()) {
							out.println("Did not unpause any services because error occured while unpausing threads on service '" + service.getServiceName() + "':");
						} else {
							out.println("Unpaused threads on the following services: '" + StringUtils.join(completed, "', '."));
							out.println("But an error occured while unpausing threads on service '" + service.getServiceName() + "':");

						}
						e.printStackTrace(out);
						return true;
					}
				}
				out.println("Services are now processing...");
    			return true;
			}
		});
		registerCommand(new AbstractCommand<List<Service>>("quit", "Stop all services and exit", new CommandArgument[] {
				new BasicCommandArgument("timeout", Long.class, "number of milliseconds to wait until attempting a hard shutdown", true)
			})  {
			protected String usage =  "Command is: 'quit (<timeout>)'";
			public boolean executeCommand(List<Service> services, PrintWriter out, Object[] arguments) {
				long timeout;
				try {
					timeout = ConvertUtils.getLong(arguments.length > 0 ? arguments[0] : null, Long.MAX_VALUE);
				} catch(ConvertException e1) {
					out.println("Could not convert the first argument, '" + arguments[0] + "', to a number." + usage);
					return true;
				}

				out.println("Shutting down Services...");
				requestShutdown(timeout);
				return false;
			}
		});
		final CurrentServiceThreadStatus currentServiceThreadStatus = new CurrentServiceThreadStatus();
		final Comparator<ServiceThreadStatusInfo> threadStatusInfoComparator = new Comparator<ServiceThreadStatusInfo>() {
			public int compare(ServiceThreadStatusInfo o1, ServiceThreadStatusInfo o2) {
				int i = o1.getServiceName().compareTo(o2.getServiceName());
				if(i != 0) return i;
				return (int)(o1.threadId - o2.threadId);
			}
		};
		final DateFormat updatedDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");	
		defaultServiceStatusListener = currentServiceThreadStatus;
		registerCommand(new AbstractCommand<List<Service>>("status", "Print current status of all service threads", null)  {
			public boolean executeCommand(List<Service> services, PrintWriter out, Object[] arguments) {
				out.println("Service Status:");
				out.println();
				out.print(StringUtils.pad("Service", ' ', 30, Justification.LEFT));
				out.print("\t ");
				out.print(StringUtils.pad("Thread", ' ', 35, Justification.LEFT));
				out.print("\t ");
				out.print(StringUtils.pad("Status", ' ', 20, Justification.LEFT));
				out.print("\t ");
				out.print(StringUtils.pad("Iteration", ' ', 12, Justification.LEFT));
				out.print("\t ");
				out.print(StringUtils.pad("Updated", ' ', 19, Justification.LEFT));
				out.println();
				out.print(StringUtils.pad(null, '-', 30, Justification.LEFT));
				out.print("\t ");
				out.print(StringUtils.pad(null, '-', 35, Justification.LEFT));
				out.print("\t ");
				out.print(StringUtils.pad(null, '-', 20, Justification.LEFT));
				out.print("\t ");
				out.print(StringUtils.pad(null, '-', 12, Justification.LEFT));
				out.print("\t ");
				out.print(StringUtils.pad(null, '-', 19, Justification.LEFT));
				out.println();
				Collection<ServiceThreadStatusInfo> coll = currentServiceThreadStatus.getCurrentThreadStatuses();
				ServiceThreadStatusInfo[] statuses = coll.toArray(new ServiceThreadStatusInfo[coll.size()]);
				Arrays.sort(statuses, threadStatusInfoComparator);
				for(ServiceThreadStatusInfo status : statuses) {
					out.print(StringUtils.pad(status.getServiceName(), ' ', 30, Justification.LEFT));
					out.print("\t ");
					out.print(StringUtils.pad(status.getThreadName(), ' ', 35, Justification.LEFT));
					out.print("\t ");
					out.print(StringUtils.pad(status.getServiceStatus().toString(), ' ', 20, Justification.LEFT));
					out.print("\t ");
					out.print(StringUtils.pad(String.valueOf(status.getIterationCount()), ' ', 12, Justification.LEFT));
					out.print("\t ");
					out.print(updatedDateFormat.format(new Date(status.getUpdateTime())));
					out.println();
				}
				out.println(StringUtils.pad(null, '-', 132, Justification.LEFT));
				return true;
			}
		});
		registerCommand(new AbstractCommand<List<Service>>("restart", "Restart all service threads", new CommandArgument[] {
				new BasicCommandArgument("timeout", Long.class, "number of milliseconds to wait until attempting a hard shutdown", true)
			})  {
			protected String usage =  "Command is: 'restart (<timeout>)'";
			public boolean executeCommand(List<Service> services, PrintWriter out, Object[] arguments) {
				long timeout;
				try {
					timeout = ConvertUtils.getLong(arguments.length > 0 ? arguments[0] : null, defaultThreadTimeout);
				} catch(ConvertException e1) {
					out.println("Could not convert the first argument, '" + arguments[0] + "', to a number." + usage);
					return true;
				}
				List<String> completed = new ArrayList<String>();
				for(Service service : services) {
					try {
						service.restartThreads(timeout);
						completed.add(service.getServiceName());
					} catch(ServiceException e) {
						if(completed.isEmpty()) {
							out.println("Did not restart any services because error occured while restarting threads on service '" + service.getServiceName() + "':");
						} else {
							out.println("Restarted threads on the following services: '" + StringUtils.join(completed, "', '."));
							out.println("But an error occured while restarting threads on service '" + service.getServiceName() + "':");

						}
						e.printStackTrace(out);
						return true;
					}
				}
				out.println("Re-started all threads.");
    			return true;
    		}
		});
		registerCommand(new AbstractCommand<List<Service>>("threads", "Add or remove service threads", new CommandArgument[] {
				new BasicCommandArgument("service", String.class, "name of the service", false),
				new BasicCommandArgument("numberOfThreads", Integer.class, "number of threads to add (if positive number) or remove (if negative number)", false),
				new BasicCommandArgument("timeout", Long.class, "number of milliseconds to wait until attempting a hard shutdown", true)
			})  {
			protected String usage =  "Command is: 'threads <service> (+|-)<# of threads> (<timeout>)'";
			public boolean executeCommand(List<Service> services, PrintWriter out, Object[] arguments) {
				if(arguments.length < 2) {
					out.println("Not enough arguments." + usage);
					return true;
				}

				Service service = null;
				for(Service s : services) {
					if(s.getServiceName().equals(arguments[0])) {
						service = s;
						break;
					}
				}
				if(service == null) {
					out.println("Service, '" + arguments[0] + "', is not configured." + usage);
					return true;
				}
				int numThreads;
				try {
					numThreads = ConvertUtils.getInt(arguments[1]);
				} catch(ConvertException e) {
					out.println("Could not convert the second argument, '" + arguments[1] + "', to a number." + usage);
					return true;
				}
				long timeout;
				try {
					timeout = ConvertUtils.getLong(arguments.length > 2 ? arguments[2] : null, defaultThreadTimeout);
				} catch(ConvertException e1) {
					out.println("Could not convert the third argument, '" + arguments[2] + "', to a number." + usage);
					return true;
				}

				int n;
				if(numThreads > 0) {
					try {
						n = service.startThreads(numThreads);
					} catch(ServiceException e) {
						out.println("Could not add threads to service '" + service.getServiceName() + "':");
						e.printStackTrace(out);
						return true;
					}
					out.println("Added " + n + " threads to service '" + service.getServiceName() + "'");
				} else if(numThreads < 0) {
					try {
						n = service.stopThreads(Math.abs(numThreads), true, timeout);
					} catch(ServiceException e) {
						out.println("Could not remove threads from service '" + service.getServiceName() + "':");
						e.printStackTrace(out);
						return true;
					}
					out.println("Removed " + n + " threads from service '" + service.getServiceName() + "'");
				} else {
					out.println("Doing nothing because numberOfThreads is 0");
				}

				return true;
			}
		});
		registerCommand(new AbstractCommand<List<Service>>("jmx-get", "Print current value of the attribute of the specified jmx bean", new CommandArgument[] { 
				new BasicCommandArgument("beanName", String.class, "The jmx bean name", false),
				new BasicCommandArgument("attribute", String.class, "The attribute name", false)})  {
			public boolean executeCommand(List<Service> services, PrintWriter out, Object[] arguments) {
				try {
					Object value = ManagementFactory.getPlatformMBeanServer().getAttribute(new ObjectName((String)arguments[0]), (String)arguments[1]);
					out.print("Value: ");
					out.println(value);
				} catch(AttributeNotFoundException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(InstanceNotFoundException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(MalformedObjectNameException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(MBeanException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(ReflectionException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(NullPointerException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				}
				return true;
			}
		});
		registerCommand(new AbstractCommand<List<Service>>("jmx-set", "Sets the current value of the attribute of the specified jmx bean to the provided value", new CommandArgument[] { 
				new BasicCommandArgument("beanName", String.class, "The jmx bean name", false),
				new BasicCommandArgument("attribute", String.class, "The attribute name", false),
				new BasicCommandArgument("value", String.class, "The value to which to set the attribute", false)})  {
			protected MBeanAttributeInfo findAttribute(MBeanInfo info, String attributeName) {
				for(MBeanAttributeInfo att : info.getAttributes()) {
					if(att.getName().equals(attributeName))
						return att;
				}
				return null;
			}
			public boolean executeCommand(List<Service> services, PrintWriter out, Object[] arguments) {
				try {
					MBeanServer mbeans = ManagementFactory.getPlatformMBeanServer();
					MBeanInfo info = mbeans.getMBeanInfo(new ObjectName((String)arguments[0]));
					MBeanAttributeInfo att = findAttribute(info, (String)arguments[1]);
					if(att == null) {
						out.print("ERROR: No attribute '"+ (String)arguments[1] + "' found on mbean '" + (String)arguments[0] + "'");
						return true;
					}
					Class<?> type = ConvertUtils.convert(Class.class, att.getType());
					Object value = ConvertUtils.convert(type, arguments[2]); 
					ManagementFactory.getPlatformMBeanServer().setAttribute(new ObjectName((String)arguments[0]), new Attribute((String)arguments[1], value));
					out.print("Successfully set attribute '");
					out.print((String)arguments[1]);
					out.print("' on '");
					out.print((String)arguments[0]);
					out.print("' to '");
					out.print(value);
					out.println("'");
				} catch(AttributeNotFoundException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(InstanceNotFoundException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(MalformedObjectNameException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(MBeanException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(ReflectionException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(NullPointerException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(javax.management.IntrospectionException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(InvalidAttributeValueException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				} catch(ConvertException e) {
					out.print("ERROR: ");
					e.printStackTrace(out);
				}
				return true;
			}
		});
	}

	/**
	 *  Sets up a watchdog ServiceStatusListener to shutdown the app after the specified amount of inactivity.
	 *  This method must be called AFTER configureServices()
	 * @param inactivityThreshhold
	 * @param shutdownTimeout
	 */
	protected void configureWatchdog(long inactivityThreshhold, long shutdownTimeout, Prerequisite shutdownAntirequisite) {
		ServiceStatusListener l = new Watchdog(inactivityThreshhold, shutdownTimeout, shutdownAntirequisite);
		for(Service service : getServices())
			service.addServiceStatusListener(l);
	}
	
	/** Runs the manager. Any services or commanders previously configured are started.
	 *  This method does not return until the exit() method is called by another thread.
	 * @throws IOException
	 * @throws Exception
	 */
	public void run() throws ServiceException {
		try {
			startup();
			awaitExit();
	        log.debug("Exit requested; shutting down...");
	        shutdown();
		} catch(ServiceException e) {
			log.error("Service Exception; exitting...", e);
			throw e;
		} catch(RuntimeException e) {
			log.fatal("Runtime Exception; exitting...", e);
			throw new ServiceException(e);
		} catch(Error e) {
			log.fatal("Java Error; exitting...", e);
			throw new ServiceException(e);
		} finally {
			log.debug("----------------------------------------------------------------------------------");
		}
	}

	protected void startup() throws ServiceException {
		printStartupInfo();
		log.debug("Registering MBeans...");
		registerMBeans();
		log.debug("MBeans registered. Starting services...");
		startServices();
		log.debug("Services are starting; starting Commanders...");
		startCommanders();
		log.debug("Commanders started.");
	}

	public void requestShutdown(final long shutdownTimeout) {
		log.info("Requesting application shutdown...");
		shutdownExecutor.submit(new Runnable() {
			public void run() {
				terminate(shutdownTimeout, 20000);
			}
		});
	}

	protected void terminate(long shutdownTimeout, final long haltTimeout) {
		if(shutdownTimeout <= 0)
			shutdownTimeout = getDefaultShutdownTimeout();
		final long exitTimeout = shutdownTimeout * 2;
		Runnable exitTask = new Runnable() {
			public void run() {
				try {
					Thread.sleep(exitTimeout);
				} catch(InterruptedException e) {
					// Ignore
				}
				log.warn("Application would not shutdown in " + exitTimeout + " milliseconds. Exitting the Java virtual machine...");
				System.exit(-10);
				try {
					Thread.sleep(haltTimeout);
				} catch(InterruptedException e) {
					// Ignore
				}
				log.warn("Application would not exit the Java virtual machine (shutdown hooks are probably hung). Halting the Java virtual machine...");
				Runtime.getRuntime().halt(-20);
			}
		};
		shutdownExecutor.submit(exitTask);
		exit(shutdownTimeout, shutdownTimeout);
	}
	protected void shutdown() throws ServiceException {
		log.debug("Services shut down; unregistering MBeans...");
		unregisterMBeans();
		log.debug("MBeans unregistered.");
		closeDataSources();
		log.debug("Data Sources closed. Shutting down Commanders...");
		shutdownCommanders();
		log.debug("Commanders shut down.");
		printShutdownInfo();
	}

	protected void closeDataSources() {
		DataLayerMgr.getDataSourceFactory().close();		
	}
	
	protected void printStartupInfo() {
		Package p = getClass().getPackage();
        if (p != null) {
			String version = SystemUtils.getApplicationVersionSafely();
			if(StringUtils.isBlank(version))
				version = p.getImplementationVersion();
            String title = p.getImplementationTitle();
            if(version != null && title != null) {
            	log.info("Starting up application '" + title + "' (v. " + version + ")");
            }
        }     
	}
	
	protected void printShutdownInfo() {
		Package p = getClass().getPackage();
        if (p != null) {
            String version = p.getImplementationVersion();
            String title = p.getImplementationTitle();
            if(version != null && title != null) {
            	log.info("Shut down application '" + title + "' (v. " + version + ")");
            }
        }     
	}
	
	public void exit(long prepareTimeout, long shutdownTimeout) {
		log.debug("Shutting down Services...");
		if(prepareTimeout <= 0)
			prepareTimeout = getDefaultShutdownTimeout();
		if(shutdownTimeout <= 0)
			shutdownTimeout = getDefaultShutdownTimeout();
		shutdownServices(prepareTimeout, shutdownTimeout);
		exitSignal.countDown();
	}

	protected void startServices() throws ServiceException {
		for(Service service : services) {
			int numThread = initialThreads.get(service);
			service.startThreads(numThread);
		}
	}

	/**
	 * @throws ServiceException
	 */
	protected void startCommanders() throws ServiceException {
		for(Commander commander : commanders) {
			commander.setPrompt(getHelpPrompt());
			commander.start(this);
			log.debug("Listening for management commands on " + commander.getDescription() + "...");
		}
	}

	protected void registerMBeans() {
		try {
			mBeanName = ManagementFactory.getPlatformMBeanServer().registerMBean(new ServiceManagerDynamicMBean(this), null).getObjectName();
		} catch(InstanceAlreadyExistsException e) {
			log.warn("Could not register MBean for ServiceManager", e);
		} catch(MBeanRegistrationException e) {
			log.warn("Could not register MBean for ServiceManager", e);
		} catch(NotCompliantMBeanException e) {
			log.warn("Could not register MBean for ServiceManager", e);
		}
	}

	protected void awaitExit() {
		log.info("Awaiting Exit...");
        try {
			exitSignal.await();
		} catch(InterruptedException e) {
			log.warn("Thread interrupted; Exitting...", e);
		}
	}

	protected void unregisterMBeans() {
		try {
			ManagementFactory.getPlatformMBeanServer().unregisterMBean(mBeanName);
		} catch(MBeanRegistrationException e) {
			log.warn("Could not unregister MBean for ServiceManager", e);
		} catch(InstanceNotFoundException e) {
			log.warn("Could not unregister MBean for ServiceManager", e);
		}
	}

	/**
	 * @throws ServiceException
	 */
	protected void shutdownCommanders() throws ServiceException {
		for(Commander commander : commanders) {
			log.debug("Stopping commander " + commander.getDescription());
			commander.shutdown();
		}
	}

	/**
	 * 
	 */
	protected void shutdownServices(long prepareTimeout, long shutdownTimeout) {
		List<Future<Boolean>> results = new ArrayList<Future<Boolean>>(services.size());
		for(Service service : services) {
			log.debug("Preparing service " + service.getServiceName() + " for shutdown");
			Future<Boolean> result;
			try {
				result = service.prepareShutdown();
			} catch(ServiceException e) {
				log.warn("Could not prepare for shutdown service " + service.getServiceName(), e);
				result = Service.FALSE_FUTURE;
			}
			results.add(result);
		}
		int i = 0;
		long end = System.currentTimeMillis() + prepareTimeout;
		for(Future<Boolean> result : results) {
			try {
				result.get(prepareTimeout, TimeUnit.MILLISECONDS);
			} catch(InterruptedException e) {
				log.warn("Interrupted while preparing shutdown of service " + services.get(i).getServiceName(), e);
				prepareTimeout = 0;
			} catch(ExecutionException e) {
				log.warn("Could not prepare shutdown of service " + services.get(i).getServiceName(), e);
			} catch(TimeoutException e) {
				log.warn("Timed out while preparing shutdown of service " + services.get(i).getServiceName(), e);
			}
			if(prepareTimeout > 0) {
				prepareTimeout = end - System.currentTimeMillis();
				if(prepareTimeout < 0)
					prepareTimeout = 0;
			}
			i++;
		}
		results.clear();
		for(Service service : services) {
			log.debug("Shutting down service " + service.getServiceName());
			Future<Boolean> result;
			try {
				result = service.shutdown();
			} catch(ServiceException e) {
				log.warn("Could not shutdown service " + service.getServiceName(), e);
				result = Service.FALSE_FUTURE;
			}
			results.add(result);
		}
		i = 0;
		end = System.currentTimeMillis() + shutdownTimeout;
		for(Future<Boolean> result : results) {
			try {
				result.get(shutdownTimeout, TimeUnit.MILLISECONDS);
			} catch(InterruptedException e) {
				log.warn("Interrupted while shutting down service " + services.get(i).getServiceName(), e);
				shutdownTimeout = 0;
			} catch(ExecutionException e) {
				log.warn("Could not shutdown service " + services.get(i).getServiceName(), e);
			} catch(TimeoutException e) {
				log.warn("Timed out while waiting for shutdown of service " + services.get(i).getServiceName(), e);
			}
			if(shutdownTimeout > 0) {
				shutdownTimeout = end - System.currentTimeMillis();
				if(shutdownTimeout < 0)
					shutdownTimeout = 0;
			}
			i++;
		}
	}

	public Object getNamedReference(String key) {
		return namedReferences.get(key);
	}

	protected void configureEditorSearchPath(String searchPathList) {
		String[] searchPath = StringUtils.split(searchPathList, StringUtils.STANDARD_DELIMS, false);
		if(searchPath != null && searchPath.length > 0) {
			String[] oldSearchPath = PropertyEditorManager.getEditorSearchPath();
			if(oldSearchPath != null && oldSearchPath.length > 0) {
				String[] tmp = new String[searchPath.length + oldSearchPath.length];
				System.arraycopy(searchPath, 0, tmp, 0, searchPath.length);
				System.arraycopy(oldSearchPath, 0, tmp, searchPath.length, oldSearchPath.length);
				PropertyEditorManager.setEditorSearchPath(tmp);
			}
		}
	}
	/** Configures the ServiceManager by performing the following steps as detailed in the {@link AbstractServiceManager} class summary:
	 * <ol>
	 * <li>Register a {@link simle.db.DataSourceFactory}</li>
	 * <li>Read Data Layer Files</li>
	 * <li>Configure Services</li>
	 * <li>Configure Service Status Listeners</li>
	 * <li>Register Default Service Status Listener</li>
	 * </ol>
	 * @param properties The properties used to configure the ServiceManager
	 * @throws ServiceException If the ServiceManager could not be configured
	 */
	public void configureServiceManager(P properties) throws ServiceException {
		configureJMXServer();
		configureSystem(properties, namedReferences);
		configureDataSources(properties, namedReferences);
		configureDataLayers(properties, namedReferences);
		configureStatics(properties, namedReferences);
		configureServices(properties, namedReferences);
		configureServiceStatusListeners(properties, namedReferences);
		configureCommanders(properties, namedReferences);
	}

	public void addCommander(Commander commander) {
		commanders.add(commander);
	}
	public boolean removeCommander(Commander commander) {
		return commanders.remove(commander);
	}
	protected Commander configureCommander(Properties subProperties, Properties rootProperties, Map<String,Object> namedReferences) throws ServiceException {
		return Main.configure(Commander.class, subProperties, rootProperties, namedReferences, true);
	}

	protected ServiceStatusListener getDefaultServiceStatusListener() {
		return defaultServiceStatusListener;
	}

	public List<Service> getContext() {
		return servicesUnmod;
	}

	public Set<String> getJMXReadableProperties() {
		return Collections.emptySet();
	}

	public Set<String> getJMXWritableProperties() {
		return Collections.emptySet();
	}

	protected abstract void configureStatics(P properties, Map<String, Object> namedReferences) throws ServiceException;
	/**
	 * @throws ServiceException
	 */
	protected abstract void configureSystem(P properties, Map<String,Object> namedReferences) throws ServiceException ;

	protected abstract void configureDataLayers(P properties, Map<String,Object> namedReferences) throws ServiceException ;

	protected abstract void configureDataSources(P properties, Map<String,Object> namedReferences) throws ServiceException ;

	/** Configures the services using the provided properties. See the {@link AbstractServiceManager} class summary for more detail.
	 * @param properties The properties to use to configure the services
	 * @param namedReferences A map of named references
	 * @throws ServiceException If the services could not be configured
	 */
	protected abstract void configureServices(P properties, Map<String,Object> namedReferences) throws ServiceException ;

	/** Configures the commanders using the provided properties. See the {@link AbstractServiceManager} class summary for more detail.
	 * @param properties The properties to use to configure the services
	 * @param namedReferences A map of named references
	 * @throws ServiceException If the commanders could not be configured
	 */
	protected abstract void configureCommanders(P properties, Map<String,Object> namedReferences) throws ServiceException ;

	protected abstract Commander addCommander(P properties, String commanderName, Map<String,Object> namedReferences) throws ServiceException ;

	protected abstract void configureServiceStatusListeners(P properties, Map<String,Object> namedReferences) throws ServiceException ;

	public long getDefaultShutdownTimeout() {
		return defaultShutdownTimeout;
	}

	public void setDefaultShutdownTimeout(long defaultShutdownTimeout) {
		this.defaultShutdownTimeout = defaultShutdownTimeout;
	}

	protected static class TracingSslRMIServerSocketFactory extends SslRMIServerSocketFactory {
		private static SSLSocketFactory defaultSSLSocketFactory = null;

		private static synchronized SSLSocketFactory getDefaultSSLSocketFactory() {
			if(defaultSSLSocketFactory == null)
				defaultSSLSocketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
			return defaultSSLSocketFactory;
		}

		@Override
		public ServerSocket createServerSocket(int port) throws IOException {
			final SSLSocketFactory sslSocketFactory = getDefaultSSLSocketFactory();
			trace("Creating a server socket on port " + port);
			return new ServerSocket(port) {
				@Override
				public Socket accept() throws IOException {
					Socket socket = super.accept();
					trace("Accepting a connection from " + socket.getRemoteSocketAddress());
					SSLSocket sslSocket = (SSLSocket) sslSocketFactory.createSocket(socket, socket.getInetAddress().getHostName(), socket.getPort(), true);
					sslSocket.setUseClientMode(false);
					if(getEnabledCipherSuites() != null) {
						sslSocket.setEnabledCipherSuites(getEnabledCipherSuites());
					}
					if(getEnabledProtocols() != null) {
						sslSocket.setEnabledProtocols(getEnabledProtocols());
					}
					sslSocket.setNeedClientAuth(getNeedClientAuth());
					return sslSocket;
				}
			};
		}

		protected static void trace(String message) {
			System.out.println("~~~ " + message);
		}
	}

	protected void configureJMXServer() throws ServiceException {
		// then set up jmx
		System.setProperty("java.rmi.server.randomIDs", "true");
		final String defaultPrefix = "jmx.remote.x.";
		String trace = getSimpleProviderProperty(defaultPrefix, null, "trace");
		final SslRMIClientSocketFactory csf = new SslRMIClientSocketFactory();
		final SslRMIServerSocketFactory ssf;
		if(trace != null && (trace = trace.trim()).length() > 0 && "true".equalsIgnoreCase(trace))
			ssf = new TracingSslRMIServerSocketFactory();
		else
			ssf = new SslRMIServerSocketFactory();
		final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		String hostname;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch(UnknownHostException e) {
			throw new ServiceException(e);
		}

		int count = Integer.parseInt(SimpleProvider.getProperties().getProperty(defaultPrefix + "count", "0"));
		if(count < 1) {
			initializeJMXServer(defaultPrefix, null, csf, ssf, mbs, hostname, false);
		} else {
			for(int i = 1; i <= count; i++) {
				initializeJMXServer(defaultPrefix + i + ".", defaultPrefix, csf, ssf, mbs, hostname, true);
			}
		}
		initializeSNMPAgent("com.sun.management.snmp.", null, null);
	}

	protected String getSimpleProviderProperty(String propertyPrefix, String fallbackPrefix, String name) {
		Properties properties = SimpleProvider.getProperties();
		String value = properties.getProperty(propertyPrefix + name);
		if(value == null && fallbackPrefix != null)
			value = properties.getProperty(fallbackPrefix + name);
		return value;
	}

	protected void initializeJMXServer(String propertyPrefix, String fallbackPrefix, SslRMIClientSocketFactory csf, SslRMIServerSocketFactory ssf, MBeanServer mbs, String hostname, boolean required) throws ServiceException {
		String value;
		// get rmiServerPort
		value = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "rmiServerPort");
		if(value == null) {
			if(!required)
				return;
			throw new ServiceException("The '" + propertyPrefix + "rmiServerPort' property is required but was not specified");
		}
		final int rmiServerPort = Integer.parseInt(value);

		// get rmiRegistryPort
		value = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "rmiRegistryPort");
		final int rmiRegistryPort;
		final boolean createRegistry;
		if(value == null || (value = value.trim()).isEmpty()) {
			createRegistry = true;
			rmiRegistryPort = Registry.REGISTRY_PORT;
		} else if(value.startsWith("(") && value.endsWith(")")) {
			value = value.substring(1, value.length() - 1).trim();
			createRegistry = false;
			if(value.isEmpty())
				rmiRegistryPort = Registry.REGISTRY_PORT;
			else
				rmiRegistryPort = Integer.parseInt(value);
		} else {
			createRegistry = true;
			rmiRegistryPort = Integer.parseInt(value);
		}
		// get registry hostname
		String registryHostname = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "rmiRegistryHost");
		if(registryHostname == null || (registryHostname = registryHostname.trim()).length() == 0)
			registryHostname = hostname;

		// Create or get registry - just to test validity
		final String registryName;
		if(createRegistry) {
			try {
				LocateRegistry.createRegistry(rmiRegistryPort);
			} catch(RemoteException e) {
				throw new ServiceException(e);
			}
			registryName = "jmxrmi";
		} else {
			try {
				LocateRegistry.getRegistry(registryHostname, rmiRegistryPort);
			} catch(RemoteException e) {
				throw new ServiceException(e);
			}
			String tmp = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "rmiRegistryName");
			if(tmp == null || (tmp = tmp.trim()).isEmpty()) {
				tmp = SimpleProvider.getProperties().getProperty("app.servicename");
				if(tmp == null || (tmp = tmp.trim()).isEmpty())
					tmp = String.valueOf(rmiServerPort);
			}
			registryName = tmp;
		}
		HashMap<String, Object> env = new HashMap<String, Object>();
		env.put(RMIConnectorServer.RMI_CLIENT_SOCKET_FACTORY_ATTRIBUTE, csf);
		env.put(RMIConnectorServer.RMI_SERVER_SOCKET_FACTORY_ATTRIBUTE, ssf);

		// env.put("com.sun.jndi.rmi.factory.socket", csf);
		String loginConfig = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "login.config");
		if(loginConfig != null) {
			env.put("jmx.remote.x.login.config", loginConfig);
		}
		final String jmxAccess = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "access.file");
		if(jmxAccess != null) {
			env.put("jmx.remote.x.access.file", jmxAccess);
		}
		// env.put("com.sun.management.jmxremote.login.config", System.getProperty("com.sun.management.jmxremote.login.config","JMXControl"));
		// env.put("com.sun.management.jmxremote.access.file", System.getProperty("com.sun.management.jmxremote.access.file","jmx.access"));
		String urlString = new StringBuilder().append("service:jmx:rmi://").append(hostname).append(':').append(rmiServerPort).append("/jndi/rmi://").append(registryHostname).append(':').append(rmiRegistryPort).append('/').append(registryName).toString();
		JMXServiceURL url;
		try {
			url = new JMXServiceURL(urlString);
		} catch(MalformedURLException e) {
			throw new ServiceException(e);
		}
		// no need
		// env.put(JMXConnectorServer.AUTHENTICATOR, new JMXPluggableAuthenticator(env));

		System.out.println("Server URL: " + url);
		JMXConnectorServer cs;
		try {
			cs = JMXConnectorServerFactory.newJMXConnectorServer(url, env, mbs);
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		System.out.println("Start the RMI connector server rmiRegistryPort:" + rmiRegistryPort + " rmiServerPort:" + rmiServerPort);
		final AtomicLong lastRefresh = new AtomicLong(System.currentTimeMillis());
		try {
			cs.start();
		} catch(IOException e) {
			throw new ServiceException(e);
		}
		System.out.println("Server started at: " + cs.getAddress());
		String tmp = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "access.file.refresh");
		long jmxAccessRefresh;
		try {
			if(tmp != null && (tmp = tmp.trim()).length() > 0) {
				jmxAccessRefresh = Long.parseLong(tmp);
			} else {
				jmxAccessRefresh = 60 * 1000; // each minute
			}
		} catch(NumberFormatException e) {
			System.out.println("Could not parse access.file.refresh property, '" + tmp + "',  to a number");
			jmxAccessRefresh = 0;
		}

		if(jmxAccessRefresh > 0 && jmxAccess != null && cs.getMBeanServer() instanceof MBeanServerFileAccessController) {
			final File jmxAccessFile = new File(jmxAccess);
			final MBeanServerFileAccessController mbsfac = (MBeanServerFileAccessController) cs.getMBeanServer();
			Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
				/**
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					long last = jmxAccessFile.lastModified();
					if(last != 0 && last > lastRefresh.get()) {
						lastRefresh.set(System.currentTimeMillis());
						try {
							mbsfac.refresh();
							System.out.println("Reloaded jmx access file '" + jmxAccess + "'");
						} catch(IOException e) {
							System.out.println("Could not reload jmx access file '" + jmxAccess + "':");
							e.printStackTrace();
						}
					}
				}
			}, jmxAccessRefresh, jmxAccessRefresh, TimeUnit.MILLISECONDS);
		}
	}

	protected boolean initializeSNMPAgent(String propertyPrefix, String fallbackPrefix, String hostname) throws ServiceException {
		String value;
		value = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "port");
		if(value == null) {
			return false; // no agent configured
		}
		final int port = Integer.parseInt(value);

		final InetAddress address;
		try {
			address = hostname == null ? InetAddress.getByAddress(new byte[4]) : InetAddress.getByName(hostname);
		} catch(UnknownHostException e) {
			throw new ServiceException("UNKNOWN_SNMP_INTERFACE", e);
		}
		final int trapPort;
		value = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "trap");
		if(value == null)
			trapPort = 162;
		else
			trapPort = Integer.parseInt(value);
		String aclFileName = getSimpleProviderProperty(propertyPrefix, fallbackPrefix, "acl.file");

		System.out.println("Initializing SNMP Agent on port " + port + " with trapPort=" + trapPort + "; BindAddress=" + address + "; ACLFile=" + aclFileName);

		// TODO: Understand this acl better to know what we should do here.
		final InetAddressAcl acl;
		try {
			acl = aclFileName != null ? new SnmpAcl(System.getProperty("user.name"), aclFileName) : null;
		} catch(UnknownHostException e) {
			throw new ServiceException("UNKNOWN_SNMP_INTERFACE", e);
		}

		// Create adaptor
		final SnmpAdaptorServer adaptor = new SnmpAdaptorServer(acl, port, address);
		adaptor.setUserDataFactory(new JvmContextFactory());
		adaptor.setTrapPort(trapPort);
		// Configure the trap destinations.
		final List<NotificationTarget> targetList = new ArrayList<NotificationTarget>();
		if(acl != null) {
			final Enumeration<?> td = acl.getTrapDestinations();
			while(td.hasMoreElements()) {
				final InetAddress targetAddr = (InetAddress) td.nextElement();
				final Enumeration<?> tc = acl.getTrapCommunities(targetAddr);
				while(tc.hasMoreElements()) {
					final String community = (String) tc.nextElement();
					final NotificationTarget target = new NotificationTargetImpl(targetAddr, trapPort, community);
					targetList.add(target);
				}
			}
		}

		final SnmpMib[] mibs = new SnmpMib[] { new JVM_MANAGEMENT_MIB_IMPL() // Create JVM MGT MIB
		};
		for(SnmpMib mib : mibs) {
			try {
				mib.init();
			} catch(IllegalAccessException x) {
				throw new ServiceException("SNMP_MIB_INIT_FAILED", x);
			}

			if(mib instanceof JVM_MANAGEMENT_MIB_IMPL)
				((JVM_MANAGEMENT_MIB_IMPL) mib).addTargets(targetList);
		}

		// Start Adaptor
		try {
			// Will wait until the adaptor starts or fails to start.
			// If the adaptor fails to start, a CommunicationException or
			// an InterruptedException is thrown.
			adaptor.start(Long.MAX_VALUE);
		} catch(Exception x) {
			Throwable t = x;
			if(x instanceof com.sun.jmx.snmp.daemon.CommunicationException) {
				final Throwable next = t.getCause();
				if(next != null)
					t = next;
			}
			throw new ServiceException("SNMP_ADAPTOR_START_FAILED", t);
		}

		// double check that adaptor is actually started (should always
		// be active, so that exception should never be thrown from here)
		if(!adaptor.isActive()) {
			throw new ServiceException("SNMP_ADAPTOR_START_FAILED: " + address + ":" + port);
		}

		for(SnmpMib mib : mibs) {
			try {
				// Add MIB to adaptor
				adaptor.addMib(mib);

				// Add Adaptor to the MIB
				mib.setSnmpAdaptor(adaptor);
			} catch(RuntimeException e) {
				System.out.println("Could not setup SNMP Adaptor");
				e.printStackTrace();
				if(mib instanceof JVM_MANAGEMENT_MIB_IMPL)
					try {
						((JVM_MANAGEMENT_MIB_IMPL) mib).terminate();
					} catch(Exception e1) {
						System.out.println("Could not terminal jvmmib:");
						e1.printStackTrace();
					}

				// Stop the adaptor
				adaptor.stop();
				throw e;
			}
		}
		System.out.println("successfully initialized SNMP Agent on port " + port + " with trapPort=" + trapPort + "; BindAddress=" + address + "; ACLFile=" + aclFileName);
		return true;
	}
}
