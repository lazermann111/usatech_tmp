package simple.app;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import simple.io.IOUtils;
import simple.io.Log;
import simple.io.server.NIORequestResponse;
import simple.io.server.RequestResponseNIOServer;
import simple.text.StringUtils;
import simple.util.concurrent.CreationException;

public class NIOSelectorPortCommander extends AbstractCommander {
	private static final Log log = Log.getLog();
	protected final RequestResponseNIOServer handler;
	protected Pattern remoteAddressRegex = null;

	public NIOSelectorPortCommander(int port, String remoteAddressRegex) throws InterruptedException, IOException, PatternSyntaxException {
		this(port, remoteAddressRegex == null);
		setRemoteAddressRegex(remoteAddressRegex);
	}
	public NIOSelectorPortCommander(int port) throws InterruptedException, IOException {
		this(port, true);
	}

	public NIOSelectorPortCommander(int port, boolean localOnly) throws InterruptedException, IOException {
		this(new InetSocketAddress(localOnly ? InetAddress.getByAddress(new byte[] {127,0,0,1}) : null, port));
	}

	public NIOSelectorPortCommander(InetSocketAddress socketAddress) throws InterruptedException, IOException {
		this.handler = new RequestResponseNIOServer(socketAddress) {
			@Override
			protected void onSocketAccepted(SocketChannel socketChannel, NIORequestResponse attachment) {
				if(prompt != null) {
					try {
						PrintWriter out = attachment.getPrintWriter();
						out.print(prompt);
						out.flush();
					} catch(IOException e) {
						log.warn("Could not write prompt to socket", e);
					}
				}
			}
			/**
			 * @see simple.io.server.NIOServer#allowConnection(java.net.Socket)
			 */
			@Override
			protected boolean allowConnection(Socket socket) {
				if(socket == null || !(socket.getRemoteSocketAddress() instanceof InetSocketAddress))
					return false;
				InetSocketAddress addr = (InetSocketAddress) socket.getRemoteSocketAddress();
				if(addr.getAddress().isLoopbackAddress())
					return super.allowConnection(socket);
				Pattern regex = getRemoteAddressRegex();
				if(regex != null && regex.matcher(addr.getAddress().getHostAddress()).matches() || regex.matcher(addr.getAddress().getCanonicalHostName()).matches())
					return super.allowConnection(socket);

				return false;
			}
		};
		this.handler.initialize();
	}

	@Override
	public void shutdown() {
		handler.close();
		super.shutdown();
	}

	public String getDescription() {
		return "on " + handler.getSocketAddress();
	}

	/** This implementation is NOT thread-safe.
	 * @see simple.app.Commander#nextCommandLine()
	 */
	@Override
	public CommandLine nextCommandLine() throws IOException {
		final NIORequestResponse rr;
		try {
			rr = handler.borrowObject();
		} catch(TimeoutException e) {
			log.info("Connection timed out", e);
			return null;
		} catch(CreationException e) {
			if(e.getCause() instanceof IOException)
				throw (IOException)e.getCause();
			log.warn("While getting connection", e);
			return null;
		} catch(InterruptedException e) {
			log.debug(e.getMessage());
			return null;
		}
		String line;
		try {
			line = IOUtils.readFullyNIO(rr.getReader());
		} catch(IOException e) {
			handler.returnObject(rr);
			throw e;
		}
		if((line=line.trim()).length() < 1) {
			handler.returnObject(rr);
			return EMPTY_LINE; // an empty line, wait for next line
		}
		String[] parts = StringUtils.split(line, StringUtils.STANDARD_WHITESPACE, StringUtils.STANDARD_QUOTES);
		final String action = parts[0];
		final String[] args = new String[parts.length-1];
		System.arraycopy(parts, 1, args, 0, args.length);
		return new CommandLine() {
			public String getAction() {
				return action;
			}
			public String[] getArguments() {
				return args;
			}
			public PrintWriter getPrintWriter() {
				try {
					return rr.getPrintWriter();
				} catch(IOException e) {
					log.warn("Could not get the printwriter for the command line", e);
					handler.returnObject(rr);
					return null;
				}
			}
		};
	}

	public RequestResponseNIOServer getHandler() {
		return handler;
	}

	public Pattern getRemoteAddressRegex() {
		return remoteAddressRegex;
	}

	public void setRemoteAddressRegex(Pattern remoteAddressRegex) {
		this.remoteAddressRegex = remoteAddressRegex;
	}
	public void setRemoteAddressRegex(String remoteAddressRegex) throws PatternSyntaxException {
		setRemoteAddressRegex(remoteAddressRegex == null ? null : Pattern.compile(remoteAddressRegex));
	}
}
