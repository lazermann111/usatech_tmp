/**
 *
 */
package simple.app;

import simple.io.Log;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;

/**
 * @author Brian S. Krug
 *
 */
public class ResourceFolderPrerequisite implements Prerequisite {
	private static final Log log = Log.getLog();
	protected ResourceFolder resourceFolder;
	protected ResourceMode resourceMode;
	/**
	 * @see simple.app.Prerequisite#isAvailable()
	 */
	public boolean isAvailable() {
		ResourceFolder resourceFolder = this.resourceFolder;
		ResourceMode resourceMode = this.resourceMode;
		if(resourceFolder == null) {
			log.warn("ResourceFolder property is not set on " + this);
			return false;
		}
		if(resourceMode == null) {
			log.warn("ResourceMode property is not set on " + this);
			return false;
		}
		return resourceFolder.isAvailable(resourceMode);
	}
	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}
	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}
	public ResourceMode getResourceMode() {
		return resourceMode;
	}
	public void setResourceMode(ResourceMode resourceMode) {
		this.resourceMode = resourceMode;
	}
}
