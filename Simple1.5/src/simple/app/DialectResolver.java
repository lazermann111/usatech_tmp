package simple.app;

public class DialectResolver {

	public static final String ORACLE = "ora";
	public static final String POSTGRESQL = "pgs";

	private static String dialect;

	public static String resolveDialect() {
		return dialect != null ? dialect : ORACLE;
	}

	public static boolean isOracle() {
		return ORACLE.equals(dialect);
	}

	protected static void initDialect(String dbDialect) {
		if (dbDialect == null) {
			dialect = ORACLE;
			return;
		}

		if (ORACLE.equals(dbDialect)) {
			dialect = ORACLE;
			return;
		}

		if (POSTGRESQL.equals(dbDialect)) {
			dialect = POSTGRESQL;
			return;
		}

		throw new IllegalArgumentException(
				"Unsupported dialect: '" + dbDialect + "'; Expected : [" + ORACLE + ", " + POSTGRESQL + "]");
	}

}
