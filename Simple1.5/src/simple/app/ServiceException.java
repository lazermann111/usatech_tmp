/*
 * Created on Jul 25, 2005
 *
 */
package simple.app;

public class ServiceException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 53393827511L;

	public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

}
