package simple.app;

import java.util.concurrent.Future;

import simple.event.ProgressListener;
import simple.util.concurrent.TrivialFuture;


/** This interface represents a queue that handles out Work to be processed. Each thread wishing to process Work from this queue
 * should call {@link #getRetriever()} once and remember the value and use that to obtain Work via the
 * {@link Retriever#next()} method.
 *
 * @author Brian S. Krug
 *
 * @param <Q> The class of the body of the Work produced by this queue
 */
public interface WorkQueue<Q> {
	/** This interface allows allows Queues to manager concurrent requests. Each thread must use it's
	 * own Retriever
	 * @author Brian S. Krug
	 *
	 * @param <Q>
	 */
	public static interface Retriever<Q> extends PrerequisiteCheck {
		/** Blocks until Work is available and then returns it to the caller
		 * @return The next {@link Work} that is available
		 * @throws WorkQueueException
		 * @throws InterruptedException If the retriever is cancelled by another thread via the {@link #cancel(Thread)}
		 * method
		 */
		public Work<Q> next() throws WorkQueueException, InterruptedException;
		/** Cancels the retrieval of Work from the {@link #next()} call.
		 * @param thread The thread that called {@link #next()}
		 * @return <code>true</code> if and only if cancelling was successful
		 */
		public boolean cancel(Thread thread) ;
		/** Releases any resources that this object holds. After this method is closed,
		 * no other methods on this Retriever should be invoked
		 *
		 */
		public void close() ;
	}
	/** The name of the queue.
	 * @return The name or key of the queue
	 */
	public String getQueueKey() ;
	/** Creates and returns a thread-specific retriever to be used to obtain Work objects to process.
	 *  Each thread should get its own Retriever and then use that repeatedly.
	 * @return A Retriever for this WorkQueue
	 * @throws WorkQueueException if the Retriever cannot be created
	 */
	public Retriever<Q> getRetriever(ProgressListener listener) throws WorkQueueException ;
	/** The class of the objects returned from Work.getBody()
	 * @return Class<Q>
	 */
	public Class<Q> getWorkBodyType();

	/** Whether or not the {@link #getRetriever()}s from this queue can handle non-serial completion of its Work.
	 * All queues must support concurrent calls of {@link Retriever#next()} on different threads. For one
	 * Retriever the normal sequence of calls is {@link Retriever#next()} then {@link Work#workComplete()} or {@link Work#workAborted(Throwable)},
	 * then {@link Retriever#next()} again. This method indicates
	 * whether a single thread may call shuffle this order.
	 * @return <code>true</code> if and only {@link Retriever#next()} may
	 * be invoked multiple times by the same thread on the same Retriever
	 * before {@link Work#workComplete()} or {@link Work#workAborted(Throwable)} is called and if {@link Work#workComplete()}
	 * and {@link Work#workAborted(Throwable)} may be called
	 */
	public boolean isAutonomous() ;

	/** Shuts down the queue. When the get() method of the returned Future is called, it will take up to specified timeout to shutdown nicely
	 *  and then it must shut itself down forcibly immediately. The queue may decide to not return any more Work
	 *  from its retrievers after this call, but this is not required.
	 * @return a Future which returns <code>true</code> from its get() methods if and only if the queue was shut down nicely
	 */
	public Future<Boolean> shutdown() ;
	
	public static final Future<Boolean> TRUE_FUTURE = new TrivialFuture<Boolean>(true);
	
}
