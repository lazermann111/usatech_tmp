package simple.app;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

/**
 * @author bkrug
 *
 * @param <S>
 */
public class RollingSample<S> {
	protected final AtomicReferenceArray<S> samples;
	protected final AtomicLong index = new AtomicLong();
	protected final Totals<S> totals;
	
	public RollingSample(Totals<S> totals, int sampleSize) {
		this.totals = totals;
		this.samples = new AtomicReferenceArray<S>(sampleSize);
	}

	public void addStats(S stats) {
		AtomicReferenceArray<S> samples = this.samples;
		S oldStats = samples.getAndSet((int) (index.getAndIncrement() % samples.length()), stats);
		totals.replace(oldStats, stats);
	}

	public int getSampleSize() {
		return (int) Math.min(index.get(), samples.length());
	}

	public long getGrandSize() {
		return index.get();
	}

	public S getSampleTotal() {
		return totals.getSampleTotal();
	}

	public S getGrandTotal() {
		return totals.getGrandTotal();
	}
	
}
