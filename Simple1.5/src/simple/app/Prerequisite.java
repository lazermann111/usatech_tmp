/**
 *
 */
package simple.app;

/**
 * @author Brian S. Krug
 *
 */
public interface Prerequisite {
	public boolean isAvailable() ;
}
