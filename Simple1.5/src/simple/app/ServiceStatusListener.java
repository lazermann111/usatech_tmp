package simple.app;


public interface ServiceStatusListener {
	/** Notification that one thread of a Service has updated its status
	 * @param service The service whose status is updated
	 * @param thread The thread which has updated its status
	 * @param serviceStatus The new status
	 * @param iterationCount The number of times (starting with 1) that the thread has iterated. If this status occurs each time,
	 * then this number should equal the number of times this method has been called with this particular status.
	 */
	public void serviceStatusChanged(ServiceStatusInfo serviceStatusInfo) ;
}