package simple.app;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.MapConfiguration;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;

/**
 * @author Brian S. Krug
 *
 */
public class MainWithConfig extends Main {
	private static final Log log = Log.getLog();

	public static void main(String[] args) {
		new MainWithConfig().run(args);
	}

	public static interface Action {
		public void perform(Map<String,Object> argumentMap, Configuration config) ;
	}

	protected Map<String,Action> actions;

	@Override
	protected void init() {
		actions = new LinkedHashMap<String, Action>();
		super.init();
	}
	/** Registers the specified {@link Action}
	 * @param actionKey The argument value that points to the action
	 * @param action The {@link Action} to execute if the actionKey is used as the action argument
	 * @return <code>true</code> if this is a new actionKey, <code>false</code> if this action is replacing
	 * an already registered one
	 */
	public boolean registerAction(String actionKey, Action action) {
		return actions.put(actionKey, action) == null;
	}

	/** Deregisters the action associated with the specified <code>actionKey</code>
	 * @param actionKey The argument value that points to the action ro deregister
	 * @return <code>true</code> if the actionKey was deregistered, <code>false</code> if the actionKey was not
	 * previously registered
	 */
	@Override
	public boolean deregisterAction(String actionKey) {
		return actions.remove(actionKey) != null;
	}

	@Override
	protected void registerDefaultActions() {
		registerAction("start", new Action() {
			public void perform(Map<String, Object> argumentMap, Configuration config) {
				startServiceManager(argumentMap, config);
			}
		});
		registerAction("restart", new Action() {
			public void perform(Map<String, Object> argumentMap, Configuration config) {
				int managerPort = config.getInt("manager-port", 0);
				String response;
				try {
					response = sendCommand("localhost", managerPort, "restart");
				} catch(IOException e) {
					finishAndExit("Error occured while running contacting process", 240, e);
					return;
				}
				finishAndExit("Process responded with:\n" + response, 0, null);
			}
		});

		registerAction("stop", new Action() {
			public void perform(Map<String, Object> argumentMap, Configuration config) {
				int managerPort = config.getInt("manager-port", 0);
				String response;
				try {
					response = sendCommand("localhost", managerPort, "quit");
				} catch(IOException e) {
					finishAndExit("Error occured while running contacting process", 240, e);
					return;
				}
				finishAndExit("Process responded with:\n" + response, 0, null);
			}
		});
	}

	protected ServiceManagerWithConfig startServiceManager(Map<String, Object> argumentMap, Configuration config) {
		int managerPort = config.getInt("manager-port", 0);
		ServiceManagerWithConfig sm;
		try {
			sm = getServiceManager(config);
		} catch(ServiceException e) {
			finishAndExit("Could not create service manager", 200, e);
			return null;
		} catch(RuntimeException e) {
			finishAndExit("Could not create service manager", 201, e);
			return null;
		} catch(Error e) {
			finishAndExit("Could not create service manager", 202, e);
			return null;
		}
		log.debug("Configuring Service Manager '" + sm.getClass().getName() + "' with manager port " + managerPort);
		try {
			sm.configureServiceManager(config);
		} catch(ServiceException e) {
			finishAndExit("Could not configure service manager", 210, e);
			return null;
		} catch(RuntimeException e) {
			finishAndExit("Could not configure service manager", 211, e);
			return null;
		} catch(Error e) {
			finishAndExit("Could not configure service manager", 212, e);
			return null;
		}
		if(managerPort <= 0) {
			if(sm.getCommanders().isEmpty()) {
				sm.addCommander(new InOutCommander());
			}
		} else {
			String remoteAddressRegex = config.getString("control-address-regex");
			if(remoteAddressRegex == null || (remoteAddressRegex = remoteAddressRegex.trim()).length() == 0)
				remoteAddressRegex = null;
			try {
				sm.addCommander(new NIOSelectorPortCommander(managerPort, remoteAddressRegex));
			} catch(IOException e) {
				finishAndExit("Error occured while creating commander on port " + managerPort, 220, e);
				return null;
			} catch(InterruptedException e) {
				finishAndExit("Error occured while creating commander on port " + managerPort, 220, e);
				return null;
			}
		}

		// Sanity check
		try {
			ConvertUtils.getInt("123");
			ConvertUtils.getString(456, true);
		} catch(ConvertException e) {
			finishAndExit("Could not do simple conversion; shutting down", 230, e);
			return null;
		}

		log.debug("Starting Service Manager '" + sm.getClass().getName() + "' with manager port " + managerPort);
		try {
			sm.run();
		} catch(ServiceException e) {
			finishAndExit("Error occured while running service manager", 250, e);
			return null;
		} catch(RuntimeException e) {
			finishAndExit("Error occured while running service manager", 251, e);
			return null;
		} catch(Error e) {
			finishAndExit("Error occured while running service manager", 252, e);
			return null;
		}
		return sm;
	}

	/** Performs the steps outlined in the {@link Main} class summary:
	 * <ol><li>Parses the command line arguments</li>
	 *  <li>Loads the properties file</li>
	 *  <li>Performs the action specified</li></ol>
	 * @param args The command line arguments
	 * @see Main
	 */
	@Override
	public void run(String[] args) {
		printInvocation(args);
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		Configuration config;
		try {
			config = getConfig(ConvertUtils.getStringSafely(argMap.get("propertiesFile")), getClass(), null);
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			config.setProperty(entry.getKey(), entry.getValue());
		}
		String actionKey = (String)argMap.get("action");
		if(actionKey == null) {
			finishAndExit("No action specified on the command line", 150, true, null);
			return;
		} else {
			Action action = actions.get(actionKey);
			if(action != null) {
				action.perform(argMap, config);
			} else {
				finishAndExit("Invalid action '" + actionKey + "' specified on the command line", 170, true, null);
				return;
			}
		}

		Log.finish();
		System.exit(0);
	}

	/** Instantiates the ServiceManager. Override to provide a custom ServiceManager.
	 * @param properties Properties to instantiate the ServiceManager
	 * @return The ServiceManager
	 * @throws ServiceException
	 */
	protected ServiceManagerWithConfig getServiceManager(Configuration config) throws ServiceException {
		return new ServiceManagerWithConfig();
	}

	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object. Then returns that {@link java.util.Properties} object. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	public Configuration getConfig(String propertiesFile, Class<?> appClass, Configuration defaults) throws IOException {
		return loadConfig(propertiesFile, appClass, defaults);
	}
	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object. Then returns that {@link java.util.Properties} object. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	public static Configuration loadConfig(String propertiesFile, Class<?> appClass, Configuration defaults) throws IOException {
		if(propertiesFile == null) propertiesFile = appClass.getSimpleName() + ".properties";
		Configuration config;
		AppPropertiesConfiguration pc = new AppPropertiesConfiguration();
		pc.setFileName(propertiesFile);
		try {
			pc.load();
		} catch(ConfigurationException e) {
			if(e.getCause() instanceof IOException)
				throw (IOException)e.getCause();
			else {
				IOException ioe = new IOException(e.getMessage());
				ioe.initCause(e);
				throw ioe;
			}
		}
		if(defaults != null && !defaults.isEmpty()) {
			CompositeConfiguration cc = new CompositeConfiguration();
			cc.addConfiguration(pc);
			cc.addConfiguration(defaults);
			config = cc;
		} else {
			config = pc;
		}
		return config;
	}

	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object using Apache Commons Configuration which
	 *  allows variable interpolation and inclusion of other properties files. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	@Override
	public Properties getProperties(String propertiesFile, Class<?> appClass, Properties defaults)
			throws IOException {
		return loadPropertiesWithConfig(propertiesFile, appClass, defaults);
	}

	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object using Apache Commons Configuration which
	 *  allows variable interpolation and inclusion of other properties files. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	public static Properties loadPropertiesWithConfig(String propertiesFile, Class<?> appClass, Properties defaults)
			throws IOException {
		return ConfigurationConverter.getProperties(loadConfig(propertiesFile, appClass, defaults == null ? null : new MapConfiguration(defaults)));
	}
}
