package simple.app;

public interface ServiceManagerControl {
	public void requestShutdown(long shutdownTimeout) ;
}
