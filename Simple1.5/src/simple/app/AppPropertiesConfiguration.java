package simple.app;

import java.io.File;
import java.net.URL;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Extension of {@link org.apache.commons.configuration.PropertiesConfiguration} that allows the setting
 * of the includesAllowed property and sets both the includesAllowed and delimiterParsingDisabled properties
 * to true initially.
 * 
 * @author Brian S. Krug
 * 
 */
public class AppPropertiesConfiguration extends PropertiesConfiguration {
	public AppPropertiesConfiguration() {
		super();
		initSettings();
	}

	public AppPropertiesConfiguration(File file) throws ConfigurationException {
		this();
		// set the file and update the url, the base path and the file name
        setFile(file);

        // load the file
        if (file.exists())
        	load();
	}

	public AppPropertiesConfiguration(String fileName) throws ConfigurationException {
		this();
		// store the file name
        setFileName(fileName);

        // load the file
        load();
	}

	public AppPropertiesConfiguration(URL url) throws ConfigurationException {
		this();
		// set the URL and update the base path and the file name
        setURL(url);

        // load the file
        load();
	}

	protected void initSettings() {
		setIncludesAllowed(true);
		setDelimiterParsingDisabled(true);
	}

	@Override
	public void setIncludesAllowed(boolean includesAllowed) {
		super.setIncludesAllowed(includesAllowed);
	}
}