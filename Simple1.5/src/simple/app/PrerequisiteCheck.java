/**
 *
 */
package simple.app;

/**
 * @author Brian S. Krug
 *
 */
public interface PrerequisiteCheck {
	/** Cancels the waiting for prerequisites to be available
	 * @param thread
	 */
	public void cancelPrequisiteCheck(Thread thread);
	/** Waits until all prerequisites are available or {@link #cancelPrequisiteCheck(Thread)} is called
	 * and returns whether all are available or not
	 * @return
	 */
	public boolean arePrequisitesAvailable();

	/**
	 * Notifies the prerequisite that it needs to check again
	 * 
	 */
	public void resetAvailability();
}
