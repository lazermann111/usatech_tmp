/**
 *
 */
package simple.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Map;
import java.util.Properties;

/**
 * @author Brian S. Krug
 *
 */
public abstract class CommandLineInterface<C> extends Base {

	/**
	 * @see simple.app.Base#execute(java.util.Map, java.util.Properties)
	 */
	@Override
	protected void execute(Map<String, Object> argMap, final Properties properties) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintStream out = System.out;
		Commander commander = createCommander(properties, in, out);
		final C context = createContext(properties, in, out);
		CommandManager<C> commandManager = new AbstractCommandManager<C>() {
			/**
			 * @see simple.app.CommandManager#getContext()
			 */
			public C getContext() {
				return context;
			}
			/**
			 * @see simple.app.AbstractCommandManager#registerDefaultCommands()
			 */
			@Override
			protected void registerDefaultCommands() {
				Command<C>[] commands = CommandLineInterface.this.getCommands(properties);
				if(commands != null)
					for(Command<C> command : commands)
						registerCommand(command);
			}
		};
		commander.setPrompt(commandManager.getHelpPrompt());
		commander.processCommands(commandManager);
	}

	/**
	 * @return
	 */
	protected abstract Command<C>[] getCommands(Properties properties) ;

	/**
	 * @param in
	 * @param out
	 * @return
	 */
	protected abstract C createContext(Properties properties, BufferedReader in, PrintStream out) ;

	protected Commander createCommander(Properties properties, BufferedReader in, OutputStream out) {
		return new InOutCommander(in, out);
	}
}
