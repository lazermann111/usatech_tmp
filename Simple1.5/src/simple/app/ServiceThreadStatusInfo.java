/**
 *
 */
package simple.app;

import java.util.Date;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import simple.bean.DynaBean;
import simple.lang.SystemUtils;

/** Holds Service Status information
 * @author Brian S. Krug
 *
 */
public class ServiceThreadStatusInfo {
	protected Service service;
	protected long threadId;
	protected String threadName;
	protected String threadGroupName;
	protected long updateTime;
	protected ServiceStatus serviceStatus;
	protected long iterationCount;
	protected long processingTimeoutCount;
	protected long processingExceptionCount;
	protected Long queueLatency;
	protected final Map<ServiceStatus, AtomicLong> timeTotals = new EnumMap<ServiceStatus, AtomicLong>(ServiceStatus.class);
	protected final Map<ServiceStatus, AtomicLong> intervalTimeTotals = new EnumMap<ServiceStatus, AtomicLong>(ServiceStatus.class);

	public ServiceThreadStatusInfo() {
	}
	public String getServiceName() {
		return service == null ? null : service.getServiceName();
	}
	public long getThreadId() {
		return threadId;
	}
	public String getThreadName() {
		return threadName;
	}
	public String getThreadGroupName() {
		return threadGroupName;
	}
	public long getUpdateTime() {
		return updateTime;
	}
	public ServiceStatus getServiceStatus() {
		return serviceStatus;
	}
	public long getIterationCount() {
		return iterationCount;
	}
	public long getProcessingTimeoutCount() {
		return processingTimeoutCount;
	}
	public long getProcessingExceptionCount() {
		return processingExceptionCount;
	}
	public Long getQueueLatency() {
		return queueLatency;
	}
	public Long getTotalTime(ServiceStatus serviceStatus) {
		AtomicLong total = timeTotals.get(serviceStatus);
		return total == null ? null : total.get();
	}
	public Long getIntervalTime(ServiceStatus serviceStatus) {
		AtomicLong interval = intervalTimeTotals.get(serviceStatus);
		return interval == null ? null : interval.get();
	}
	public Map<ServiceStatus,Long> getIntervalTimes() {
		Map<ServiceStatus,Long> copy = new EnumMap<ServiceStatus, Long>(ServiceStatus.class);
		for(Map.Entry<ServiceStatus, AtomicLong> entry : intervalTimeTotals.entrySet()) {
			copy.put(entry.getKey(), entry.getValue().get());
		}
		return copy;
	}
	public void resetIntervalTimes() {
		for(AtomicLong interval : intervalTimeTotals.values())
			interval.set(0L);
	}
	public DynaBean getSystemInfo() {
		return SystemUtils.getSystemInfo();
	}
	public void update(ServiceStatusInfo serviceStatusInfo) {
		if(serviceStatusInfo == null)
			return;
		long oldTime = this.updateTime;
		this.updateTime = System.currentTimeMillis();
		if(this.serviceStatus != null) {
			AtomicLong total = timeTotals.get(this.serviceStatus);
			AtomicLong interval = intervalTimeTotals.get(this.serviceStatus);
			if(total == null) {
				total = new AtomicLong();
				timeTotals.put(this.serviceStatus, total);
				interval = new AtomicLong();
				intervalTimeTotals.put(this.serviceStatus, interval);	
			}
			long time = this.updateTime - oldTime;
			total.addAndGet(time);
			interval.addAndGet(time);
		}
		this.service = serviceStatusInfo.getService();
		if(serviceStatusInfo.getThread() == null) {
			this.threadId = 0;
			this.threadName = "UNKNOWN";
			this.threadGroupName = "UNKNOWN";
		} else {
			this.threadId = serviceStatusInfo.getThread().getId();
			this.threadName = serviceStatusInfo.getThread().getName();
			this.threadGroupName = serviceStatusInfo.getThread().getThreadGroup().getName();
		}
		this.serviceStatus = serviceStatusInfo.getServiceStatus();
		this.iterationCount = serviceStatusInfo.getIterationCount();
		switch(serviceStatus) {
			case NACKING_MSG:
				processingExceptionCount++; break;
			case TIMEDOUT_MSG:
				processingTimeoutCount++; break;
		}
		this.queueLatency = serviceStatusInfo.getQueueLatency();
	}
	@Override
	public String toString() {
		return "Service '" + getServiceName() + "' on Thread " + getThreadName() + " (" + getThreadId() + ") is " + getServiceStatus()
		 + " (" + getServiceStatus().getDescription() + ") since " + new Date(getUpdateTime()) + " for the " + getIterationCount() + " time";
	}
	public Service getService() {
		return service;
	}
}