package simple.app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.concurrent.atomic.AtomicBoolean;

import simple.io.Log;
import simple.text.StringUtils;

public class InOutCommander extends AbstractCommander {
	private static final Log log = Log.getLog();
	protected final BufferedReader in;
	protected final PrintWriter out;
	protected AtomicBoolean init = new AtomicBoolean();

	public InOutCommander() {
		this(new BufferedReader(new InputStreamReader(System.in)), System.out);
	}

	public InOutCommander(BufferedReader in, OutputStream out) {
		this(in, new BufferedWriter(new OutputStreamWriter(out)));
	}

	public InOutCommander(BufferedReader in, Writer out) {
		this.in = in;
		this.out = new PrintWriter(out, true) {
			@Override
			public void close() {
				flush();
			}
		};
	}

	@Override
	public CommandLine nextCommandLine() throws IOException {
		if(init.compareAndSet(false, true)) initialize();
		String line = readNextLine();
		if(line == null) return null; //end of stream reached
		if((line=line.trim()).length() < 1) return EMPTY_LINE; // an empty line, wait for next line
		String[] parts = StringUtils.split(line, StringUtils.STANDARD_WHITESPACE, StringUtils.STANDARD_QUOTES);
		final String action = parts[0];
		final String[] args = new String[parts.length-1];
		System.arraycopy(parts, 1, args, 0, args.length);
		return new CommandLine() {
			public String getAction() {
				return action;
			}
			public String[] getArguments() {
				return args;
			}
			public PrintWriter getPrintWriter() {
				return out;
			}
		};
	}

	protected String readNextLine() {
		try {
			return in.readLine();
		} catch(IOException e) {
			log.warn("IO Exception reading from standard input. Quiting...", e);
			return null;
		}
	}
	protected void initialize() {
		if(prompt != null) {
			out.print(prompt);
			out.flush();
		}
	}

	public String getDescription() {
		return "standard in";
	}
}
