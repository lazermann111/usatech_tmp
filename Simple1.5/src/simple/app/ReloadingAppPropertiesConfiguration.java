package simple.app;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.configuration.FileConfiguration;
import org.apache.commons.configuration.reloading.InvariantReloadingStrategy;
import org.apache.commons.configuration.reloading.ReloadingStrategy;

import simple.util.IntervalThread;

public class ReloadingAppPropertiesConfiguration extends AppPropertiesConfiguration {
	protected static final String JAR_PROTOCOL = "jar";

	protected class Reloader extends IntervalThread {
		protected File file = getFile();
		public Reloader() {
			super("Reloader of '" + getURL() + "'");
		}

		@Override
		protected void action() {
			reload();
		}
	}

	protected long lastLoaded;
	protected long lastChecked;
	protected long lastModified;
	protected Runnable loader;
	protected IntervalThread reloadingThread;
	protected final Set<File> files = new HashSet<File>();
	protected long minInterval = 5000;
	protected long maxInterval = 60000;
	protected final ReloadingStrategy rs = new ReloadingStrategy() {

		public void reloadingPerformed() {
			loader.run();
		}

		@Override
		public void setConfiguration(FileConfiguration configuration) {
		}

		@Override
		public void init() {
			for(File f : files) {
				long mod = f.lastModified();
				if(mod > lastModified)
					lastModified = mod;
			}
		}

		@Override
		public boolean reloadingRequired() {
			long time = System.currentTimeMillis();
			if(lastChecked + minInterval >= time)
				return false;
			for(File f : files) {
				long mod = f.lastModified();
				if(mod > lastModified)
					lastModified = mod;
			}
			lastChecked = time;
			if(lastModified > lastLoaded) {
				lastLoaded = time;
				return true;
			}
			return false;
		}
	};

	public ReloadingAppPropertiesConfiguration() {
		super();
	}

	public ReloadingAppPropertiesConfiguration(File file) throws ConfigurationException {
		super(file);
	}

	public ReloadingAppPropertiesConfiguration(String fileName) throws ConfigurationException {
		super(fileName);
	}

	public ReloadingAppPropertiesConfiguration(URL url) throws ConfigurationException {
		super(url);
	}

	public void registerLoader(final Runnable loader, long minInterval, long maxInterval) {
		this.loader = loader;
		this.minInterval = minInterval;
		this.maxInterval = maxInterval;
		if(loader != null) {
			setReloadingStrategy(rs);
			if(reloadingThread == null)
				reloadingThread = new Reloader();
			reloadingThread.setInterval(maxInterval);
			reloadingThread.start();
		} else {
			setReloadingStrategy(new InvariantReloadingStrategy());
			if(reloadingThread != null) {
				reloadingThread.finish();
				reloadingThread = null;
			}
		}
	}

	public void load(URL url) throws ConfigurationException {
		if(lastLoaded == 0)
			lastLoaded = System.currentTimeMillis();
		File f = fileFromURL(url);
		if(f != null)
			files.add(f);
		super.load(url);
	}

	protected File fileFromURL(URL url) {
		if(JAR_PROTOCOL.equals(url.getProtocol())) {
			String path = url.getPath();
			try {
				return ConfigurationUtils.fileFromURL(new URL(path.substring(0, path.indexOf('!'))));
			} catch(MalformedURLException mex) {
				return null;
			}
		} else {
			return ConfigurationUtils.fileFromURL(url);
		}
	}
}
