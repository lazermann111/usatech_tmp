package simple.app;

public interface SelfProcessor {
	public void process() throws ServiceException ;
}
