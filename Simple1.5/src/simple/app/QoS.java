/**
 * 
 */
package simple.app;

import java.util.Map;

/**
 * @author bkrug
 *
 */
public interface QoS {
	/** The priority of the message. 0 means default, 1 is lowest, 5 is normal, 10 is highest
	 * @return
	 */
	public int getPriority() ;
	/** Whether the message must survive message system restarts
	 * @return
	 */
	public boolean isPersistent() ;
	/** The time (in milliseconds past epoch) after which the message is expired. Returns 0 if not set. 
	 *  Either getExpiration() or getTimeToLive() may return a non-zero result, but
	 *  usually both should not.
	 * @return
	 */
	public long getExpiration() ;
	
	/** The number of milliseconds before the message expires. Returns 0 if not set. 
	 *  Either getExpiration() or getTimeToLive() may return a non-zero result, but
	 *  usually both should not.
	 * @return
	 */
	public long getTimeToLive() ;
	
	/**
	 * The time (in milliseconds past epoch) after which the message is
	 * available. Returns 0 if it immediately available. Not all publishers may
	 * support this.
	 * 
	 * @return
	 */
	public long getAvailable();

	/** A map of the message properties
	 * @return
	 */
	public Map<String, ?> getMessageProperties() ;
}