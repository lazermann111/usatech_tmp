package simple.app;

import simple.db.StatementMonitor;

public class SelfProcessingQueueService<Q extends SelfProcessor> extends AbstractWorkQueueService<Q> {
	protected final WorkProcessor<Q> processor = new WorkProcessor<Q>() {
		public void processWork(Work<Q> work) throws ServiceException, WorkQueueException {
			if(work == null)
				return;
			Q body = work.getBody();
			if(body == null)
				return;
			body.process();
		}
		public boolean interruptProcessing(Thread thread) {
			StatementMonitor.cancelStatements(thread);
			thread.interrupt();
			return true;
		}
	};
	
	public SelfProcessingQueueService(String serviceName) {
		super(serviceName);
	}

	@Override
	protected WorkProcessor<Q> getWorkProcessor() {
		return processor;
	}
}
