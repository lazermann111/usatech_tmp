package simple.app;

import org.apache.commons.configuration.Configuration;

public interface Configurer {
	public void configure(Configuration config);
}
