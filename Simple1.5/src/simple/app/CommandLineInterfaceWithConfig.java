/**
 *
 */
package simple.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Map;

import org.apache.commons.configuration.Configuration;

/**
 * @author Brian S. Krug
 *
 */
public abstract class CommandLineInterfaceWithConfig<C> extends BaseWithConfig {

	/**
	 * @see simple.app.Base#execute(java.util.Map, java.util.Properties)
	 */
	@Override
	protected void execute(Map<String, Object> argMap, final Configuration config) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintStream out = System.out;
		Commander commander = createCommander(config, in, out);
		final C context = createContext(config, in, out);
		CommandManager<C> commandManager = new AbstractCommandManager<C>() {
			/**
			 * @see simple.app.CommandManager#getContext()
			 */
			public C getContext() {
				return context;
			}
			/**
			 * @see simple.app.AbstractCommandManager#registerDefaultCommands()
			 */
			@Override
			protected void registerDefaultCommands() {
				Command<C>[] commands = CommandLineInterfaceWithConfig.this.getCommands(config);
				if(commands != null)
					for(Command<C> command : commands)
						registerCommand(command);
			}
		};
		commander.setPrompt(commandManager.getHelpPrompt());
		commander.processCommands(commandManager);
	}

	/**
	 * @return
	 */
	protected abstract Command<C>[] getCommands(Configuration config) ;

	/**
	 * @param in
	 * @param out
	 * @return
	 */
	protected abstract C createContext(Configuration config, BufferedReader in, PrintStream out) ;

	protected Commander createCommander(Configuration config, BufferedReader in, OutputStream out) {
		return new InOutCommander(in, out);
	}
}
