package simple.app;

public interface AvailabilityChangeListener {
	/**
	 * @param prerequisite
	 *            The prerequisite or <code>null</code> if a queue has no prerequisites and is now available
	 * @param available
	 */
	public void availibilityChanged(Prerequisite prerequisite, boolean available);
}
