package simple.app;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;

import simple.bean.ConvertException;
import simple.bean.NestedNameTokenizer;
import simple.bean.ReflectionUtils;
import simple.io.Log;

public class ConfigReloader extends FileChangedReloadingStrategy {
	private static final Log log = Log.getLog();
	protected Map<String, Object> previousProperties;
	protected final Map<String, Object> namedReferences;

	public ConfigReloader(Map<String, Object> namedReferences) {
		super();
		this.namedReferences = namedReferences;
	}

	@Override
	public void init() {
		super.init();
		Map<String, Object> currentProperties = new HashMap<String, Object>();
		BaseWithConfig.intoMap(configuration, currentProperties);
		previousProperties = Collections.unmodifiableMap(currentProperties);
	}

	@Override
	public void reloadingPerformed() {
		super.reloadingPerformed();
		Map<String, Object> currentProperties = new HashMap<String, Object>();
		BaseWithConfig.intoMap(configuration, currentProperties);
		currentProperties = Collections.unmodifiableMap(currentProperties);
		handleConfigChange(previousProperties, currentProperties);
		previousProperties = currentProperties;
	}

	protected void handleConfigChange(Map<String, Object> previousProperties, Map<String, Object> currentProperties) {
		Map<String, Object> differentProperties = new HashMap<String, Object>(currentProperties);
		differentProperties.entrySet().removeAll(previousProperties.entrySet());
		processConfigChange(differentProperties);
	}

	protected void processConfigChange(Map<String, Object> differentProperties) {
		for(Map.Entry<String, Object> entry : differentProperties.entrySet()) {
			String nestedProperty = entry.getKey();
			NestedNameTokenizer.Token[] tokens;
			try {
				tokens = new NestedNameTokenizer(nestedProperty).tokenize();
			} catch(ParseException e) {
				log.warn("Could not parse property '" + nestedProperty + "' while reloading config", e);
				continue;
			}
			for(int i = tokens.length - 2; i >= 0; i--) {
				Object o = namedReferences.get(nestedProperty.substring(0, tokens[i].getNextPos() - 1));
				if(o != null) {
					try {
						if(ReflectionUtils.setProperty(o, nestedProperty.substring(tokens[i].getNextPos()), entry.getValue(), true, true))
							break;
					} catch(IntrospectionException e) {
						log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
					} catch(IllegalAccessException e) {
						log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
					} catch(InvocationTargetException e) {
						log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
					} catch(InstantiationException e) {
						log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
					} catch(ConvertException e) {
						log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
					} catch(ParseException e) {
						log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
					}
				} else if(tokens[i].isIndexed()) {
					o = namedReferences.get(nestedProperty.substring(0, tokens[i].getEndPos()));
					if(o != null) {
						try {
							o = ReflectionUtils.getIndexedProperty(o, null, tokens[i].getIndex(), true);
						} catch(IntrospectionException e) {
							log.warn("Could not get property '" + nestedProperty + "' while reloading config", e);
						} catch(IllegalAccessException e) {
							log.warn("Could not get property '" + nestedProperty + "' while reloading config", e);
						} catch(InvocationTargetException e) {
							log.warn("Could not get property '" + nestedProperty + "' while reloading config", e);
						}
						try {
							if(ReflectionUtils.setProperty(o, nestedProperty.substring(tokens[i].getNextPos()), entry.getValue(), true, true))
								break;
						} catch(IntrospectionException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(IllegalAccessException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(InvocationTargetException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(InstantiationException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(ConvertException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(ParseException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						}
					}
				} else if(tokens[i].isMapped()) {
					o = namedReferences.get(nestedProperty.substring(0, tokens[i].getEndPos()));
					if(o != null) {
						try {
							o = ReflectionUtils.getMappedProperty(o, null, tokens[i].getKey(), true);
						} catch(IntrospectionException e) {
							log.warn("Could not get property '" + nestedProperty + "' while reloading config", e);
						} catch(IllegalAccessException e) {
							log.warn("Could not get property '" + nestedProperty + "' while reloading config", e);
						} catch(InvocationTargetException e) {
							log.warn("Could not get property '" + nestedProperty + "' while reloading config", e);
						}
						try {
							if(ReflectionUtils.setProperty(o, nestedProperty.substring(tokens[i].getNextPos()), entry.getValue(), true, true))
								break;
						} catch(IntrospectionException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(IllegalAccessException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(InvocationTargetException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(InstantiationException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(ConvertException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						} catch(ParseException e) {
							log.warn("Could not set property '" + nestedProperty + "' while reloading config", e);
						}
					}
				}
			}
		}
	}
}