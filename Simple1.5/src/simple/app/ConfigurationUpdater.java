package simple.app;

import org.apache.commons.configuration.event.ConfigurationListener;

public interface ConfigurationUpdater {
	public void addConfigurationListener(ConfigurationListener listener);

	public void clearConfigurationListeners();

	public void removeConfigurationListener(ConfigurationListener listener);
}
