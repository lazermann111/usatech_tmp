package simple.app;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;

public abstract class AbstractCommander implements Commander {
	private static final Log log = Log.getLog();
	protected static final int STATE_STOPPED = 0;
	protected static final int STATE_STARTING = 1;
	protected static final int STATE_STARTED = 2;
	protected static final int STATE_STOPPING = 3;

	protected final AtomicInteger state = new AtomicInteger(STATE_STOPPED);
	protected String prompt;
	protected Thread executor;

	public static interface CommandLine {
		public String getAction() ;
		public String[] getArguments() ;
		public PrintWriter getPrintWriter() ;
	}
	public static final CommandLine EMPTY_LINE = new CommandLine() {
		public String getAction() {
			return null;
		}
		public String[] getArguments() {
			return null;
		}
		public PrintWriter getPrintWriter() {
			return null;
		}
	};

	protected abstract CommandLine nextCommandLine() throws IOException;

	/**
	 * @see simple.app.Commander#start()
	 */
	public <C> void start(final CommandManager<C> commandManager) {
		int localState = state.get();
		switch(localState) {
			case STATE_STOPPED:
				if(state.compareAndSet(STATE_STOPPED, STATE_STARTING)) {
					log.debug("Starting Commander " + getDescription() + " ...");
					executor = new Thread("CommanderThread - " + getDescription()) {
						/**
						 * @see java.lang.Thread#run()
						 */
						@Override
						public void run() {
							if(state.compareAndSet(STATE_STARTING, STATE_STARTED)) {
								while(state.get() == STATE_STARTED) {
									try {
										if(!runNextCommand(commandManager)) {
											state.set(STATE_STOPPING);
											break;
										}
									} catch(RuntimeException e) {
										log.error("While running next command", e);
									} catch(Error e) {
										log.error("While running next command", e);
									}
								}
							}
						}
					};
					executor.setPriority(4);
					executor.setDaemon(true);
					executor.start();
					return;
				}
			case STATE_STARTED:
			case STATE_STARTING:
				log.debug("Commander already started or starting");
				return;
			case STATE_STOPPING:
				Thread.yield();
				start(commandManager);
				return;
			default:
				throw new IllegalStateException("Invalid state " + localState);
		}
	}

	public void shutdown()  {
		int localState = state.get();
		switch(localState) {
			case STATE_STOPPED:
			case STATE_STOPPING:
				log.debug("Commander already stopped or stopping");
				return;
			case STATE_STARTED:
				if(state.compareAndSet(STATE_STARTED, STATE_STOPPING)) {
					log.debug("Interrupting executor " + executor);
					executor.interrupt();
					executor = null;
					return;
				}
			case STATE_STARTING:
				Thread.yield();
				shutdown();
				return;
			default:
				throw new IllegalStateException("Invalid state " + localState);
		}
	}

	/** Processes commands from a {@link Commander} until {@link Command#executeCommand(Object, PrintWriter, String[])}
	 *  returns <code>false</code>.
	 * @param commander
	 */
	public <C> void processCommands(CommandManager<C> commandManager) {
		state.set(STATE_STARTED);
		try {
			while(runNextCommand(commandManager)) ;
		} finally {
			state.set(STATE_STOPPED);
		}
	}

	protected <C> boolean runNextCommand(CommandManager<C> commandManager) {
		CommandLine line;
		try {
			line = nextCommandLine();
		} catch(IOException e) {
			log.warn("IO Exception reading from input. Getting next Command Line...", e);
			return true;
		}
		if(line == null) {
			return false; //end of stream reached
		}
		if(line.getAction() == null) return true; // an empty line, wait for next line
		Command<C> cmd = commandManager.getCommand(line.getAction());
		PrintWriter out = line.getPrintWriter();
		if(out == null) {
			log.debug("CommandLine PrintWriter is null - not processing command '" + line.getAction() + "'");
			return true;
		} else {
			boolean done = false;
			if(cmd == null) {
				out.println("Command '" + line.getAction() + "' not recognized");
			} else {
				done = executeCommand(cmd, commandManager.getContext(), line.getArguments(), out);
			}
			if(prompt != null && !done)
				out.println(prompt);
			//out.flush();
			out.close();
			return !done;
		}
	}
	/**
	 * @param <C>
	 * @param cmd
	 * @param context
	 * @param lineArgs
	 * @param out
	 * @return <code>true</code> if and only if processing is done (should not continue processing commands)
	 */
	protected <C> boolean executeCommand(Command<C> cmd, C context, String[] lineArgs, PrintWriter out) {
		CommandArgument[] cas = cmd.getCommandArguments();
		Object[] args;
		if(cas != null && cas.length > 0) {
			args = new Object[cas.length];
			if(lineArgs != null) {
				for(int i = 0; i < args.length; i++) {
					try {
						args[i] = ConvertUtils.convert(cas[i].getType(), i < lineArgs.length ? lineArgs[i] : null);
					} catch(ConvertException e) {
						out.println("Could not convert '" + lineArgs[i] + "' to " + cas[i].getType().getName() + " for argument '" + cas[i].getName() + "'");
						printCommandArguments(cmd, out);
						return false;
					}
					if(args[i] == null && !cas[i].isOptional()) {
						out.println("Argument '" + cas[i].getName() + "' was not provided");
						printCommandArguments(cmd, out);
						return false;
					}
				}
			}
		} else {
			args = null;
		}
		return !cmd.executeCommand(context, out, args);
	}
	protected void printCommandArguments(Command<?> cmd, PrintWriter out) {
		CommandArgument[] cas = cmd.getCommandArguments();
		out.print("Usage: '");
		out.print(cmd.getKey());
		for(CommandArgument ca : cas) {
			out.print(" <");
			out.print(ca.getName());
			out.print('>');
		}
		out.println("'");
		for(CommandArgument ca : cas) {
			out.print('\t');
			out.print(ca.getName());
			out.print(" (");
			out.print(ca.getType().getName());
			out.print(") - ");
			out.print(ca.getDescription());
			out.println();
		}
	}
	public String getPrompt() {
		return prompt;
	}
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}
}
