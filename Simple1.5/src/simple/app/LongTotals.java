package simple.app;

import java.util.concurrent.atomic.AtomicLong;

public class LongTotals implements ResetableTotals<Number> {
	protected final AtomicLong grandTotal = new AtomicLong();
	protected final AtomicLong sampleTotal = new AtomicLong();

	@Override
	public Number getGrandTotal() {
		return grandTotal;
	}

	@Override
	public Number getSampleTotal() {
		return sampleTotal;
	}
	
	@Override
	public void replace(Number oldStats, Number newStats) {
		long l;
		if(newStats != null) {
			l = newStats.longValue();
			if(l > 0)
				grandTotal.addAndGet(l);
			else if(l < 0)
				l = 0;
		} else
			l = 0;
		long diff = l;
		if(oldStats != null) {
			l = oldStats.longValue();
			if(l > 0)
				diff -= l;
		}
		if(diff != 0)
			sampleTotal.addAndGet(diff);
	}

	@Override
	public void reset() {
		grandTotal.set(0);
		sampleTotal.set(0);
	}
}
