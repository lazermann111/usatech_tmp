/**
 * 
 */
package simple.app;

import java.util.Collections;
import java.util.Map;

public class BasicQoS implements QoS {
	public static final Map<String, ?> EMPTY_MESSAGE_PROPERTIES = Collections.emptyMap();
	public static final int DEFAULT_PRIORITY = 5;
	protected long timeToLive;
	protected long expiration;
	protected long available;
	protected int priority;
	protected boolean persistent = true;
	protected Map<String,?> messageProperties;
	
	public BasicQoS() {
	}
	
	public BasicQoS(int priority, boolean persistent, Map<String, ?> messageProperties) {
		super();
		this.priority = priority;
		this.persistent = persistent;
		this.messageProperties = messageProperties;
	}

	public BasicQoS(long timeToLive, int priority, boolean persistent, Map<String, ?> messageProperties) {
		super();
		this.timeToLive = timeToLive;
		this.priority = priority;
		this.persistent = persistent;
		this.messageProperties = messageProperties;
	}
	/** The priority of the message. 0 means default, 1 is lowest, 5 is normal, 10 is highest
	 * @return
	 */
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public boolean isPersistent() {
		return persistent;
	}
	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}
	public long getExpiration() {
		return expiration;
	}
	public void setExpiration(long expiration) {
		this.expiration = expiration;
	}
	public long getTimeToLive() {
		return timeToLive;
	}
	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}
	public Map<String, ?> getMessageProperties() {
		return messageProperties;
	}
	public void setMessageProperties(Map<String, ?> messageProperties) {
		this.messageProperties = messageProperties;
	}
	public long getAvailable() {
		return available;
	}
	public void setAvailable(long available) {
		this.available = available;
	}
}