/**
 *
 */
package simple.app;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Provider;

import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactorySpi;

import simple.bean.ReflectionUtils;

/**
 * @author Brian S. Krug
 *
 */
public class MultiFileTrustManagerProvider extends Provider {
	private static final long serialVersionUID = -2020166665903373739L;
	protected static String delegateNamePrefix = "com.sun.net.ssl.internal.ssl.TrustManagerFactoryImpl$";
	public static abstract class AbstractFactory extends TrustManagerFactorySpi {
		protected final TrustManagerFactorySpi delegate;
		protected TrustManager[] trustManagers;
		public AbstractFactory() throws IllegalArgumentException, InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException {
			this.delegate = ReflectionUtils.createObject(delegateNamePrefix + getClass().getSimpleName(), TrustManagerFactorySpi.class);
		}
		/**
		 * @see javax.net.ssl.TrustManagerFactorySpi#engineGetTrustManagers()
		 */
		@Override
		protected TrustManager[] engineGetTrustManagers() {
			return trustManagers;
		}

		/**
		 * @see javax.net.ssl.TrustManagerFactorySpi#engineInit(java.security.KeyStore)
		 */
		@Override
		protected void engineInit(KeyStore keystore) throws KeyStoreException {
			//Add additional files to keystore

			//invoke delegate methods
			try {
				Method method = delegate.getClass().getDeclaredMethod("engineInit", KeyStore.class);
				method.setAccessible(true);
				try {
					method.invoke(delegate, keystore);
				} finally {
					method.setAccessible(false);
				}
				method = delegate.getClass().getDeclaredMethod("engineGetTrustManagers");
				method.setAccessible(true);
				try {
					trustManagers = (TrustManager[])method.invoke(delegate);
				} finally {
					method.setAccessible(false);
				}
			} catch(Exception e) {
				throw new KeyStoreException("Could not call methods on delegate " + delegate, e);
			}
		}

		/**
		 * @see javax.net.ssl.TrustManagerFactorySpi#engineInit(javax.net.ssl.ManagerFactoryParameters)
		 */
		@Override
		protected void engineInit(ManagerFactoryParameters managerfactoryparameters) throws InvalidAlgorithmParameterException {
			// TODO Auto-generated method stub

		}
	}
	public static class SimpleFactory extends AbstractFactory {
		public SimpleFactory() throws IllegalArgumentException, InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException {
			super();
		}
	}
	public static class PKIXFactory extends AbstractFactory {
		public PKIXFactory() throws IllegalArgumentException, InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException {
			super();
		}
	}
	/**
	 * @param name
	 * @param version
	 * @param info
	 */
	public MultiFileTrustManagerProvider(String name, double version, String info) {
		super(name, version, info);
		put("TrustManagerFactory.SunX509", SimpleFactory.class.getName()); //"com.sun.net.ssl.internal.ssl.TrustManagerFactoryImpl$SimpleFactory");
		put("TrustManagerFactory.PKIX", PKIXFactory.class.getName()); //"com.sun.net.ssl.internal.ssl.TrustManagerFactoryImpl$PKIXFactory");
	}

	protected static KeyStore getCacertsKeyStore(String s) throws Exception {
		//* <-MISALIGNED-> *//* 112*/String s1 = null;
		//* <-MISALIGNED-> *//* 113*/Object obj = null;
		//* <-MISALIGNED-> *//* 114*/FileInputStream fileinputstream = null;
		//* <-MISALIGNED-> *//* 117*/HashMap hashmap = new HashMap();
		//* <-MISALIGNED-> *//* 118*/String s4 = File.separator;
		//* <-MISALIGNED-> *//* 119*/KeyStore keystore = null;
		//* <-MISALIGNED-> *//* 121*/AccessController.doPrivileged(new PrivilegedExceptionAction(hashmap) {
		//	public Object run() throws Exception {
				//* <-MISALIGNED-> *//* 123*/props.put("trustStore", System.getProperty("javax.net.ssl.trustStore"));
				//* <-MISALIGNED-> *//* 125*/props.put("javaHome", System.getProperty("java.home"));
				//* <-MISALIGNED-> *//* 127*/props.put("trustStoreType", System.getProperty("javax.net.ssl.trustStoreType", KeyStore.getDefaultType()));
				//* <-MISALIGNED-> *//* 130*/props.put("trustStoreProvider", System.getProperty("javax.net.ssl.trustStoreProvider", ""));
				//* <-MISALIGNED-> *//* 132*/props.put("trustStorePasswd", System.getProperty("javax.net.ssl.trustStorePassword", ""));
				//* <-MISALIGNED-> *//* 134*/return null;
			//}

			//final HashMap val$props;
			//{
				//* <-MISALIGNED-> *//* 121*/props = hashmap;
				//* <-MISALIGNED-> *//* 121*/super();
			//}
		//});
		//* <-MISALIGNED-> *//* 147*/s1 = (String) hashmap.get("trustStore");
		//* <-MISALIGNED-> *//* 148*/if(!"NONE".equals(s1)) {
		//	File file;
			//* <-MISALIGNED-> *//* 149*/if(s1 != null) {
				//* <-MISALIGNED-> *//* 150*/file = new File(s1);
				//* <-MISALIGNED-> *//* 151*/fileinputstream = getFileInputStream(file);
			//} else {
				//* <-MISALIGNED-> *//* 153*/String s5 = (String) hashmap.get("javaHome");
				//* <-MISALIGNED-> *//* 154*/file = new File((new StringBuilder()).append(s5).append(s4).append("lib").append(s4).append("security").append(s4).append("jssecacerts").toString());
				//* <-MISALIGNED-> *//* 157*/if((fileinputstream = getFileInputStream(file)) == null) {
					//* <-MISALIGNED-> *//* 158*/file = new File((new StringBuilder()).append(s5).append(s4).append("lib").append(s4).append("security").append(s4).append("cacerts").toString());
					//* <-MISALIGNED-> *//* 161*/fileinputstream = getFileInputStream(file);
				//}
			//}
			//* <-MISALIGNED-> *//* 165*/if(fileinputstream != null)
				//* <-MISALIGNED-> *//* 166*/s1 = file.getPath();
			//* <-MISALIGNED-> *//* 168*/else
				//* <-MISALIGNED-> *//* 168*/s1 = "No File Available, using empty keystore.";
		//}
		//* <-MISALIGNED-> *//* 172*/String s2 = (String) hashmap.get("trustStoreType");
		//* <-MISALIGNED-> *//* 173*/String s3 = (String) hashmap.get("trustStoreProvider");
		//* <-MISALIGNED-> *//* 174*/if(debug != null && Debug.isOn(s)) {
			//* <-MISALIGNED-> *//* 175*/System.out.println((new StringBuilder()).append("trustStore is: ").append(s1).toString());
			//* <-MISALIGNED-> *//* 176*/System.out.println((new StringBuilder()).append("trustStore type is : ").append(s2).toString());
			//* <-MISALIGNED-> *//* 178*/System.out.println((new StringBuilder()).append("trustStore provider is : ").append(s3).toString());
		//}
		//* <-MISALIGNED-> *//* 185*/if(s2.length() != 0) {
			//* <-MISALIGNED-> *//* 186*/if(debug != null && Debug.isOn(s))
				//* <-MISALIGNED-> *//* 187*/System.out.println("init truststore");
			//* <-MISALIGNED-> *//* 189*/if(s3.length() == 0)
				//* <-MISALIGNED-> *//* 190*/keystore = KeyStore.getInstance(s2);
			//* <-MISALIGNED-> *//* 192*/else
				//* <-MISALIGNED-> *//* 192*/keystore = KeyStore.getInstance(s2, s3);
			//* <-MISALIGNED-> *//* 195*/char ac[] = null;
			//* <-MISALIGNED-> *//* 196*/String s6 = (String) hashmap.get("trustStorePasswd");
			//* <-MISALIGNED-> *//* 198*/if(s6.length() != 0)
				//* <-MISALIGNED-> *//* 199*/ac = s6.toCharArray();
			//* <-MISALIGNED-> *//* 202*/keystore.load(fileinputstream, ac);
			//* <-MISALIGNED-> *//* 205*/if(ac != null) {
				//* <-MISALIGNED-> *//* 206*/for(int i = 0; i < ac.length; i++)
					//* <-MISALIGNED-> *//* 207*/ac[i] = '\0';
			//}
		//}
		//* <-MISALIGNED-> *//* 212*/if(fileinputstream != null)
			//* <-MISALIGNED-> *//* 213*/fileinputstream.close();
		//* <-MISALIGNED-> *//* 216*/return keystore;
		return null;
	}
}
