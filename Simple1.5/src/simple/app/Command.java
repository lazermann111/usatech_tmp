package simple.app;

import java.io.PrintWriter;

/** A command that performs some action when executed. Can be registered with {@link AbstractCommandManager}.
 * @author Brian S. Krug
 *
 * @param <C> The type of the context object passed to the {@link Command#executeCommand(Object, PrintWriter, String[])} method
 */
public interface Command<C> {
	/** Executes the command
	 * @param context A custom-defined context object to pass. From {@link AbstractCommandManager#getContext()}.
	 * @param out The PrintWriter to which any response is written.
	 * @param arguments The command arguments
	 * @return <code>true</code> to continue processing commands, <code>false</code> to stop processing commands
	 */
	public boolean executeCommand(C context, PrintWriter out, Object[] arguments) ;
	/** The identifier by which the {@link Commander} activates this command
	 * @return The identifier for this command
	 */
	public String getKey() ;
	/** The description of this command. Used in contructing the usage message.
	 * @return The description of this command
	 */
	public String getDescription() ;

	/** The list of command arguments for this command
	 * @return
	 */
	public CommandArgument[] getCommandArguments() ;
}