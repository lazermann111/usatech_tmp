/**
 *
 */
package simple.app;

/**
 * @author Brian S. Krug
 *
 */
public interface CommandArgument {
	public String getName() ;
	public Class<?> getType();
	public String getDescription() ;
	public boolean isOptional() ;
}
