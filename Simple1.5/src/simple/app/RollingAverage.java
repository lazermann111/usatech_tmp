package simple.app;

public class RollingAverage<S extends Number> extends RollingSample<S> {
	
	public RollingAverage(ResetableTotals<S> totals, int sampleSize) {
		super(totals, sampleSize);
	}
	
	public double getAverage() {
		double sampleSize = getSampleSize();
		if (sampleSize <= 0)
			return 0;
		return getSampleTotal().doubleValue() / sampleSize;
	}
	
	public void reset() {
		((ResetableTotals<S>)totals).reset();
		for(int i=0; i<samples.length(); i++)
			samples.set(i, null);
	}
	
}
