/*
 * Created on May 9, 2005
 *
 */
package simple.app;

import simple.db.BasicBatchRecorder;

/** Records Service Status in the database on a regular interval.
 *
 * @author Brian S. Krug
 *
 */
public class RecordServiceThreadStatus extends BasicBatchRecorder<ServiceThreadStatusInfo> implements ServiceStatusListener {
	public RecordServiceThreadStatus() {
        super();
    }

    public void serviceStatusChanged(ServiceStatusInfo serviceStatusInfo) {
    	ServiceThreadStatusInfo info = new ServiceThreadStatusInfo();
    	info.update(serviceStatusInfo);
    	record(info);
    }
}
