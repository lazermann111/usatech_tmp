package simple.app;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.event.ConfigurationEvent;
import org.apache.commons.configuration.event.ConfigurationListener;

import simple.bean.ConvertUtils;

public class ConfigurationUpdaterSupport implements ConfigurationUpdater {
	protected Set<ConfigurationListener> listeners = Collections.synchronizedSet(new LinkedHashSet<ConfigurationListener>());

	@Override
	public void addConfigurationListener(ConfigurationListener listener) {
		listeners.add(listener);
	}

	@Override
	public void clearConfigurationListeners() {
		listeners.clear();
	}

	@Override
	public void removeConfigurationListener(ConfigurationListener listener) {
		listeners.remove(listener);
	}

	public void handlePropertyChange(String propertyName, Object oldPropertyValue, Object newPropertyValue) {
		if(!listeners.isEmpty() && !ConvertUtils.areEqual(oldPropertyValue, newPropertyValue)) {
			ConfigurationEvent event = new ConfigurationEvent(this, AbstractConfiguration.EVENT_SET_PROPERTY, propertyName, newPropertyValue, false);
			for(ConfigurationListener listener : listeners)
				listener.configurationChanged(event);
		}

	}

}
