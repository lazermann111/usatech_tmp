/**
 *
 */
package simple.app.jmx;

/**
 * @author Brian S. Krug
 *
 */
public interface DataSourceMXBean {
	/** Gets the url of the database for this DataSource
	 * @return
	 */
	public String getUrl() ;

	/**Gets the username of this DataSource
	 * @return
	 */
	public String getUsername() ;

	/** Returns information about the internals of the Data Source
	 * @return
	 */
	public String getCurrentConnectionInfo() ;
}
