/**
 *
 */
package simple.app.jmx;

import java.lang.management.ManagementFactory;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import simple.app.CurrentServiceStatusListener;
import simple.app.LongTotals;
import simple.app.RollingSample;
import simple.app.Service;
import simple.app.ServiceStatus;
import simple.app.ServiceStatusInfo;
import simple.app.ServiceThreadStatusInfo;
import simple.app.Totals;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.AbstractLookupMap;


/**
 * @author Brian S. Krug
 *
 */
public class JMXServiceStatusListener extends CurrentServiceStatusListener {
	private static final Log log = Log.getLog();
	protected interface Stats {
		public Map<ServiceStatus, Long> getStatusTimes() ;
		public long getQueueLatency() ;
	}
	protected class BasicStats implements Stats {
		public final Map<ServiceStatus,Long> statusTimes;
		public final long queueLatency;
		public BasicStats(Map<ServiceStatus, Long> statusTimes, long queueLatency) {
			this.statusTimes = statusTimes;
			this.queueLatency = queueLatency;
		}
		public Map<ServiceStatus, Long> getStatusTimes() {
			return statusTimes;
		}
		public long getQueueLatency() {
			return queueLatency;
		}		
	}
	protected final String objectNamePrefix;
	protected final Set<ObjectName> registeredNames;
	protected final ConcurrentMap<String,RollingSample<Stats>> samples = new ConcurrentHashMap<String, RollingSample<Stats>>();
	protected int sampleSize = 100;

	protected static class ServiceStatsTotals implements Totals<Stats> {
		protected final Map<ServiceStatus, LongTotals> timeTotals = new EnumMap<ServiceStatus, LongTotals>(ServiceStatus.class);
		protected final LongTotals queueLatency = new LongTotals();
		protected final Stats grandTotalStats = new Stats() {
			protected final Map<ServiceStatus, Long> statusTimes = new AbstractLookupMap<ServiceStatus, Long>() {
				@Override
				public Set<ServiceStatus> keySet() {
					return timeTotals.keySet();
				}

				@Override
				public Long get(Object key) {
					LongTotals totals = timeTotals.get(key);
					if(totals == null)
						return null;
					Number n = totals.getGrandTotal();
					if(n == null)
						return null;
					return n.longValue();
				}
			};

			public Map<ServiceStatus, Long> getStatusTimes() {
				return statusTimes;
			}
			public long getQueueLatency() {
				Number n = queueLatency.getGrandTotal();
				if(n == null)
					return 0;
				return n.longValue();
			}
		};

		protected final Stats sampleTotalStats = new Stats() {
			protected final Map<ServiceStatus, Long> statusTimes = new AbstractLookupMap<ServiceStatus, Long>() {
				@Override
				public Set<ServiceStatus> keySet() {
					return timeTotals.keySet();
				}

				@Override
				public Long get(Object key) {
					LongTotals totals = timeTotals.get(key);
					if(totals == null)
						return null;
					Number n = totals.getSampleTotal();
					if(n == null)
						return null;
					return n.longValue();
				}
			};

			public Map<ServiceStatus, Long> getStatusTimes() {
				return statusTimes;
			}

			public long getQueueLatency() {
				Number n = queueLatency.getSampleTotal();
				if(n == null)
					return 0;
				return n.longValue();
			}
		};
		public void replace(Stats oldStats, Stats newStats) {
			final Map<ServiceStatus, Long> newStatusTimes = (newStats != null ? newStats.getStatusTimes() : null);
			final Map<ServiceStatus, Long> oldStatusTimes = (oldStats != null ? oldStats.getStatusTimes() : null);
			for(ServiceStatus status : ServiceStatus.values()) {
				Long value = (newStatusTimes != null ? newStatusTimes.get(status) : null);
				Long oldValue = (oldStatusTimes != null ? oldStatusTimes.get(status) : null);
				if(value != null || oldValue != null) {
					LongTotals totals = timeTotals.get(status);
					if(totals == null) {
						totals = new LongTotals();
						timeTotals.put(status, totals);
					}
					totals.replace(oldValue, value);
				}
			}
			queueLatency.replace(oldStats != null ? oldStats.getQueueLatency() : null, newStats != null ? newStats.getQueueLatency() : null);
		}

		@Override
		public Stats getGrandTotal() {
			return grandTotalStats;
		}

		@Override
		public Stats getSampleTotal() {
			return sampleTotalStats;
		}
	}
	protected class InternalExtendedServiceMXBean extends BasicServiceMXBean implements ExtendedServiceMXBean {
		protected final RollingSample<Stats> sample;

		public InternalExtendedServiceMXBean(Service service, RollingSample<Stats> sample) {
			super(service);
			this.sample = sample;
		}

		protected Double getSampleAverage(Long stat) {
			if(stat == null)
				return null;
			return stat / (double)sample.getSampleSize();
		}

		protected Double getGrandAverage(Long stat) {
			if(stat == null)
				return null;
			return stat / (double) sample.getGrandSize();
		}

		protected Double getSampleAverage(ServiceStatus status) {
			return getSampleAverage(sample.getSampleTotal().getStatusTimes().get(status));
		}

		protected Double getGrandAverage(ServiceStatus status) {
			return getGrandAverage(sample.getGrandTotal().getStatusTimes().get(status));
		}
		public Double getAvgLatencyTime() {
			return getSampleAverage(sample.getSampleTotal().getQueueLatency());
		}

		public Double getAvgProcessingTime() {
			return getSampleAverage(ServiceStatus.PROCESSING_MSG);
		}
		public Double getAvgIdleTime() {
			return getSampleAverage(ServiceStatus.AWAITING_NEXT);
		}
		public Double getAvgOtherTime() {
			long tot = 0;
			boolean empty = true;
			for(Map.Entry<ServiceStatus, Long> entry : sample.getSampleTotal().getStatusTimes().entrySet()) {
				switch(entry.getKey()) {
					case PROCESSING_MSG: case AWAITING_NEXT:
						break;
					default:
						if(entry.getValue() != null) {
							tot += entry.getValue();
							empty = false;
						}
				}
			}
			return empty ? null : getSampleAverage(tot);
		}
		public Double getPercentIdle() {
			long tot = 0;
			long idle = 0;
			boolean empty = true;
			for(Map.Entry<ServiceStatus, Long> entry : sample.getSampleTotal().getStatusTimes().entrySet()) {
				switch(entry.getKey()) {
					case AWAITING_NEXT:
						if(entry.getValue() != null) {
							idle += entry.getValue();
							empty = false;
						}
					default:
						if(entry.getValue() != null) {
							tot += entry.getValue();
						}
				}
			}
			return empty ? null : idle / (double) tot;
		}

		public Double getPercentOther() {
			long tot = 0;
			long other = 0;
			boolean empty = true;
			for(Map.Entry<ServiceStatus, Long> entry : sample.getSampleTotal().getStatusTimes().entrySet()) {
				if(entry.getValue() != null) {
					tot += entry.getValue();
				}
				switch(entry.getKey()) {
					case PROCESSING_MSG:
					case AWAITING_NEXT:
						break;
					default:
						if(entry.getValue() != null) {
							other += entry.getValue();
							empty = false;
						}
				}
			}
			return empty ? null : other / (double) tot;
		}
		public int getSampleSize() {
			return sample.getSampleSize();
		}

		@Override
		public long getTotalLatencyTime() {
			return sample.getGrandTotal().getQueueLatency();
		}

		@Override
		public long getTotalProcessingTime() {
			Long l = sample.getGrandTotal().getStatusTimes().get(ServiceStatus.PROCESSING_MSG);
			return l == null ? 0 : l.longValue();
		}

		@Override
		public long getTotalIdleTime() {
			Long l = sample.getGrandTotal().getStatusTimes().get(ServiceStatus.AWAITING_NEXT);
			return l == null ? 0 : l.longValue();
		}

		@Override
		public long getTotalOtherTime() {
			long tot = 0;
			for(Map.Entry<ServiceStatus, Long> entry : sample.getGrandTotal().getStatusTimes().entrySet()) {
				switch(entry.getKey()) {
					case PROCESSING_MSG:
					case AWAITING_NEXT:
						break;
					default:
						if(entry.getValue() != null)
							tot += entry.getValue();
				}
			}
			return tot;
		}

		@Override
		public long getTotalIterations() {
			return sample.getGrandSize();
		}
	}

	public JMXServiceStatusListener(String objectNamePrefix, Set<ObjectName> registeredNames) {
		super();
		if(objectNamePrefix == null || (objectNamePrefix=objectNamePrefix.trim()).length() == 0)
			this.objectNamePrefix = "";
		else if(objectNamePrefix.endsWith(","))
			this.objectNamePrefix = objectNamePrefix;
		else
			this.objectNamePrefix = objectNamePrefix + ',';
		this.registeredNames = registeredNames;
	}

	@Override
	protected String getKey(Service service, Thread thread) {
		StringBuilder sb = new StringBuilder();
		sb.append("ServiceName=");
		if(service != null)
			sb.append(StringUtils.prepareJMXNameValue(service.getServiceName()));
		else
			sb.append("UNKNOWN");
		sb.append(",ThreadName=");
		if(thread != null)
			sb.append(StringUtils.prepareJMXNameValue(thread.getName()));
		else
			sb.append("UNKNOWN");
		return sb.toString();
	}
	
	protected String getServiceKey(String serviceName) {
		StringBuilder sb = new StringBuilder();
		sb.append("ServiceName=");
		if(serviceName != null)
			sb.append(StringUtils.prepareJMXNameValue(serviceName));
		else
			sb.append("UNKNOWN");
		return sb.toString();
	}
	
	/**
	 * @throws NullPointerException
	 * @throws MalformedObjectNameException
	 * @throws NotCompliantMBeanException
	 * @throws MBeanRegistrationException
	 * @throws InstanceAlreadyExistsException
	 * @see simple.app.CurrentServiceStatusListener#infoCreated(java.lang.String, simple.app.ServiceThreadStatusInfo)
	 */
	@Override
	protected void infoCreated(String key, final ServiceThreadStatusInfo info) {
		super.infoCreated(key, info);
		try {
			ObjectName objectName = new ObjectName(objectNamePrefix + key);
			ManagementFactory.getPlatformMBeanServer().registerMBean(new ServiceThreadMXBean() {
				public Long getIterations() {
					return info.getIterationCount();
				}
				public int getStatusCode() {
					return info.getServiceStatus().ordinal();
				}
				public String getStatusDetails() {
					return info.getServiceStatus().getDescription();
				}
				public String getStatusName() {
					return info.getServiceStatus().toString();
				}
				public Long getProcessingTime() {
					return info.getTotalTime(ServiceStatus.PROCESSING_MSG);
				}
				public Long getWaitingTime() {
					return info.getTotalTime(ServiceStatus.AWAITING_NEXT);
				}
				public Long getProcessingExceptions() {
					return info.getProcessingExceptionCount();
				}
				public Long getProcessingTimeouts() {
					return info.getProcessingTimeoutCount();
				}
				public Date getLastUpdatedDate() {
					return new Date(info.getUpdateTime());
				}
				public Long getQueueLatency() {
					return info.getQueueLatency();
				}
			}, objectName);
			if(registeredNames != null)
				registeredNames.add(objectName);
		} catch(InstanceAlreadyExistsException e) {
			log.warn("Could not register ServiceThreadMXBean for '" + key + "'", e);
		} catch(MBeanRegistrationException e) {
			log.warn("Could not register ServiceThreadMXBean for '" + key + "'", e);
		} catch(NotCompliantMBeanException e) {
			log.warn("Could not register ServiceThreadMXBean for '" + key + "'", e);
		} catch(MalformedObjectNameException e) {
			log.warn("Could not register ServiceThreadMXBean for '" + key + "'", e);
		} catch(NullPointerException e) {
			log.warn("Could not register ServiceThreadMXBean for '" + key + "'", e);
		}	
	}
	
	public ServiceMXBean createServiceMXBean(Service service) {
		RollingSample<Stats> sample = new RollingSample<Stats>(new ServiceStatsTotals(), getSampleSize());
		samples.put(service.getServiceName(), sample);
		return new InternalExtendedServiceMXBean(service, sample);
	}
	protected void updateServiceStatusInfo(ServiceThreadStatusInfo info, ServiceStatusInfo serviceStatusInfo) {		
		long prevIterationCount = info.getIterationCount();
		super.updateServiceStatusInfo(info, serviceStatusInfo);
		if(prevIterationCount >= 0 && info.getIterationCount() > prevIterationCount) {
			RollingSample<Stats> sample = samples.get(info.getServiceName());
			if(sample != null) {
				sample.addStats(new BasicStats(info.getIntervalTimes(), serviceStatusInfo.getServiceStatus() == ServiceStatus.PROCESSING_MSG && serviceStatusInfo.getQueueLatency() != null ? serviceStatusInfo.getQueueLatency() : -1));
			}
			info.resetIntervalTimes();
		}
	}

	public int getSampleSize() {
		return sampleSize;
	}

	public void setSampleSize(int sampleSize) {
		this.sampleSize = sampleSize;
	}
}
