/**
 *
 */
package simple.app.jmx;

import java.util.Date;


/**
 * @author Brian S. Krug
 *
 */
public interface ServiceThreadMXBean {
	public int getStatusCode() ;
	public String getStatusName() ;
	public String getStatusDetails() ;
	public Long getIterations() ;
	public Long getProcessingTime() ;
	public Long getWaitingTime() ;
	public Long getProcessingTimeouts() ;
	public Long getProcessingExceptions() ;
	public Date getLastUpdatedDate() ;
	public Long getQueueLatency() ;
}
