/**
 *
 */
package simple.app.jmx;

import simple.app.ServiceException;

/**
 * @author Brian S. Krug
 *
 */
public interface ServiceMXBean {
	public String getServiceClassName() ;

	public String getQueueKey();
	public int getNumThreads() ;
	/** Attempts to start the specified number of threads
	 * @param numThreads The number of threads to start up
	 * @return The number of threads started
	 * @throws ServiceException
	 */
	public int startThreads(int numThreads) throws ServiceException;
	public int restartThreads(long timeout) throws ServiceException;
	public int pauseThreads() throws ServiceException;
	public int unpauseThreads() throws ServiceException;
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException;
	public int stopAllThreads(boolean force, long timeout) throws ServiceException;
}
