package simple.app.jmx;

public interface ExtendedServiceMXBean extends ServiceMXBean {
	public Double getAvgLatencyTime();
	public Double getAvgProcessingTime() ;
	public Double getAvgIdleTime() ;
	public Double getAvgOtherTime() ;
	public Double getPercentIdle() ;

	public Double getPercentOther();

	public int getSampleSize() ;

	public long getTotalLatencyTime();

	public long getTotalProcessingTime();

	public long getTotalIdleTime();

	public long getTotalOtherTime();

	public long getTotalIterations();

}
