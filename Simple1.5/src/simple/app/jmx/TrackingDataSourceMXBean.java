package simple.app.jmx;

public interface TrackingDataSourceMXBean extends DataSourceMXBean {
	/** Whether or not this data source throws an exception when getConnection() is called
	 * @return true if and only if this data source throws an exception when getConnection() is called
	 */
	public boolean isDisabled() ;
	
	/** Close all open connections and throw an exception when getConnection() is called on this data source
	 * 
	 */
	public void disableImmediate() throws InterruptedException ;
	/** Wait for all open connections to be closed and throw an exception when getConnection() is called on this data source
	 * 
	 */
	public void disable() throws InterruptedException ;
	
	/** Enable this data source so that getConnection() no longer throws an exception
	 * 
	 */
	public void enable() ;
	
	public int getNumActive() ;
}
