package simple.app.jmx;

import java.beans.IntrospectionException;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanRegistration;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import simple.app.AbstractServiceManager;
import simple.app.Command;
import simple.app.CommandArgument;
import simple.app.Service;
import simple.bean.ReflectionUtils;
import simple.bean.ReflectionUtils.BeanProperty;
import simple.io.Log;
import simple.text.StringUtils;

/**
 *
 * @author Brian S. Krug
 *
 */
public class ServiceManagerDynamicMBean implements DynamicMBean, MBeanRegistration {
	private static final Log log = Log.getLog();
	protected static final MBeanConstructorInfo[] EMPTY_MBEAN_CONSTRS = new MBeanConstructorInfo[0];
	protected static final MBeanNotificationInfo[] EMPTY_MBEAN_NOTIFS = new MBeanNotificationInfo[0];
	protected final AbstractServiceManager<?> serviceManager;
	protected final Map<String, BeanProperty> serviceManagerAttributes;
	protected MBeanInfo mbeanInfo;
	protected ObjectName currentObjectName;
	protected MBeanServer currentServer;
	protected final Set<ObjectName> registeredNames = Collections.newSetFromMap(new ConcurrentHashMap<ObjectName,Boolean>());
	protected JMXServiceStatusListener listener;

	protected static class RestrictedBeanProperty implements BeanProperty {
		protected final boolean readable;
		protected final boolean writable;
		protected final BeanProperty delegate;

		public RestrictedBeanProperty(BeanProperty delegate, boolean readable, boolean writable) {
			super();
			this.readable = readable;
			this.writable = writable;
			this.delegate = delegate;
		}

		public String getName() {
			return delegate.getName();
		}

		public Object getValue(Object bean) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
			if(!readable)
				throw new IllegalAccessException("Attribute is not readable");
			return delegate.getValue(bean);
		}

		public void setValue(Object bean, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
			if(!writable)
				throw new IllegalAccessException("Attribute is not writable");
			delegate.setValue(bean, value);
		}

		public Class<?> getType() {
			return delegate.getType();
		}

		public boolean isReadable() {
			return readable;
		}

		public boolean isWritable() {
			return writable;
		}
	}
	public ServiceManagerDynamicMBean(AbstractServiceManager<?> serviceManager) {
		super();
		this.serviceManager = serviceManager;
		this.serviceManagerAttributes = findAttributes(serviceManager);
	}

/*
	protected static class JMXCommand<C> extends AbstractCommand<C> {
		public JMXCommand(String key, String description, Method method) {
			super(key, description);
		}
		public boolean executeCommand(C commandArgument, PrintWriter out, String[] arguments) {

		}
	}
*/

	protected static Map<String, BeanProperty> findAttributes(AbstractServiceManager<?> serviceManager) {
		if((serviceManager.getJMXReadableProperties() != null && !serviceManager.getJMXReadableProperties().isEmpty()) || (serviceManager.getJMXWritableProperties() != null && !serviceManager.getJMXWritableProperties().isEmpty())) {
			Map<String, BeanProperty> attributes = new HashMap<String, BeanProperty>();
			if(serviceManager.getJMXReadableProperties() != null)
				for(String prop : serviceManager.getJMXReadableProperties()) {
					try {
						BeanProperty bp = ReflectionUtils.findBeanPropertyFor(serviceManager, prop, true);
						if(bp != null) {
							boolean writable = bp.isWritable() && (serviceManager.getJMXWritableProperties() != null && serviceManager.getJMXWritableProperties().contains(prop));
							if(!bp.isReadable() && !writable)
								continue; // nothing can be done with this - ignore it
							if(!bp.isReadable() || (bp.isWritable() != writable))
								bp = new RestrictedBeanProperty(bp, bp.isReadable(), writable);
							attributes.put(prop, bp);
						}
					} catch(IntrospectionException e) {
						log.warn("Could not get bean property '" + prop + "' on " + serviceManager.getClass().getName() + " for JMX Attribute", e);
					}
				}
			if(serviceManager.getJMXWritableProperties() != null)
				for(String prop : serviceManager.getJMXWritableProperties()) {
					try {
						if(serviceManager.getJMXReadableProperties() == null || !serviceManager.getJMXReadableProperties().contains(prop)) {
							BeanProperty bp = ReflectionUtils.findBeanPropertyFor(serviceManager, prop, true);
							if(bp != null && bp.isWritable()) {
								if(bp.isReadable())
									bp = new RestrictedBeanProperty(bp, false, true);
								attributes.put(prop, bp);
							}
						}
					} catch(IntrospectionException e) {
						log.warn("Could not get bean property '" + prop + "' on " + serviceManager.getClass().getName() + " for JMX Attribute", e);
					}
				}
			return attributes;
		}
		return Collections.emptyMap();
	}

	// JMX Methods
	/**
	 * @see javax.management.DynamicMBean#getAttribute(java.lang.String)
	 */
	public Object getAttribute(String attribute) throws AttributeNotFoundException, MBeanException,
			ReflectionException {
		BeanProperty bp = serviceManagerAttributes.get(attribute);
		if(bp == null || !bp.isReadable())
			throw new AttributeNotFoundException();
		try {
			return bp.getValue(serviceManager);
		} catch(IllegalArgumentException e) {
			throw new MBeanException(e);
		} catch(IllegalAccessException e) {
			throw new MBeanException(e);
		} catch(InvocationTargetException e) {
			throw new MBeanException(e);
		}
	}

	/**
	 * @see javax.management.DynamicMBean#getAttributes(java.lang.String[])
	 */
	public AttributeList getAttributes(String[] attributes) {
		AttributeList al = new AttributeList();
		if(attributes != null)
			for(String attribute : attributes)
				try {
					al.add(new Attribute(attribute, getAttribute(attribute)));
				} catch(AttributeNotFoundException e) {
				} catch(MBeanException e) {
				} catch(ReflectionException e) {
				}
		return al;
	}
	/**
	 * @see javax.management.DynamicMBean#getMBeanInfo()
	 */
	public MBeanInfo getMBeanInfo() {
		if(mbeanInfo == null) {
			List<MBeanOperationInfo> opList = new ArrayList<MBeanOperationInfo>();
			for(Command<List<Service>> comm : serviceManager.getCommands()) {
				MBeanParameterInfo[] signature;
				CommandArgument[] cas = comm.getCommandArguments();
				if(cas != null) {
					signature = new MBeanParameterInfo[cas.length];
					for(int i = 0; i < cas.length; i++) {
						signature[i] = new MBeanParameterInfo(cas[i].getName(), cas[i].getType().getName(), cas[i].getDescription());
					}
				} else {
					signature = new MBeanParameterInfo[0];
				}

				opList.add(new MBeanOperationInfo(comm.getKey(), comm.getDescription(), signature, String.class.getName(), MBeanOperationInfo.ACTION));
			}
			MBeanOperationInfo[] operations = opList.toArray(new MBeanOperationInfo[opList.size()]);
			MBeanAttributeInfo[] attributes;
			if(serviceManagerAttributes.isEmpty())
				attributes = null;
			else {
				attributes = new MBeanAttributeInfo[serviceManagerAttributes.size()];
				int i = 0;
				for(BeanProperty bp : serviceManagerAttributes.values()) {
					attributes[i++] = new MBeanAttributeInfo(bp.getName(), bp.getType().getName(), bp.getName(), bp.isReadable(), bp.isWritable(), bp.getType().equals(boolean.class));
				}
			}
			mbeanInfo = new MBeanInfo(getClass().getName(), "Management of Services", attributes, EMPTY_MBEAN_CONSTRS, operations, EMPTY_MBEAN_NOTIFS);
		}
		return mbeanInfo;
	}
	/**
	 * @see javax.management.DynamicMBean#invoke(java.lang.String, java.lang.Object[], java.lang.String[])
	 */
	public Object invoke(String actionName, Object[] params, String[] signature)
			throws MBeanException, ReflectionException {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		Command<List<Service>> comm = serviceManager.getCommand(actionName);
		if(comm != null) {
			PrintWriter out = new PrintWriter(result);
			comm.executeCommand(serviceManager.getContext(), out, params);
			out.close();
		} else {
			return "Invalid Operation: action name not found";
		}

		return result.toString();
	}
	/**
	 * @see javax.management.DynamicMBean#setAttribute(javax.management.Attribute)
	 */
	public void setAttribute(Attribute attribute) throws AttributeNotFoundException,
			InvalidAttributeValueException, MBeanException, ReflectionException {
		BeanProperty bp = serviceManagerAttributes.get(attribute.getName());
		if(bp == null || !bp.isWritable())
			throw new AttributeNotFoundException();
		try {
			bp.setValue(serviceManager, attribute.getValue());
		} catch(IllegalArgumentException e) {
			throw new MBeanException(e);
		} catch(IllegalAccessException e) {
			throw new MBeanException(e);
		} catch(InvocationTargetException e) {
			throw new MBeanException(e);
		}
	}
	/**
	 * @see javax.management.DynamicMBean#setAttributes(javax.management.AttributeList)
	 */
	public AttributeList setAttributes(AttributeList attributes) {
		AttributeList al = new AttributeList();
		if(attributes != null)
			for(Attribute attribute : attributes.asList())
				try {
					setAttribute(attribute);
					al.add(attribute);
				} catch(AttributeNotFoundException e) {
				} catch(MBeanException e) {
				} catch(ReflectionException e) {
				} catch(InvalidAttributeValueException e) {
				}
		return al;
	}

	// MBeanRegistration methods
	/**
	 * @see javax.management.MBeanRegistration#postDeregister()
	 */
	public void postDeregister() {
		MBeanServer server = currentServer;
		if(server == null)
			server = ManagementFactory.getPlatformMBeanServer();
		for(Iterator<ObjectName> iter = registeredNames.iterator(); iter.hasNext(); ) {
			ObjectName objectName = iter.next();
			try {
				server.unregisterMBean(objectName);
			} catch(MBeanRegistrationException e) {
				log.warn("Could not unregister MBean '" + objectName + "'", e);
			} catch(InstanceNotFoundException e) {
				log.warn("Could not unregister MBean '" + objectName + "'", e);
			}
			iter.remove();
		}
		if(serviceManager != null && serviceManager.getServices() != null)
			for(Service service : serviceManager.getServices()) {
				service.removeServiceStatusListener(listener);
			}
	}

	/**
	 * @see javax.management.MBeanRegistration#postRegister(java.lang.Boolean)
	 */
	public void postRegister(Boolean registrationDone) {
		if(registrationDone != null && registrationDone.booleanValue()) {
			if(serviceManager != null && serviceManager.getServices() != null) {
				String name = currentObjectName.getCanonicalName();
				MBeanServer server = currentServer;
				if(server == null)
					server = ManagementFactory.getPlatformMBeanServer();
				for(Service service : serviceManager.getServices()) {
					ObjectName objectName;
					try {
						objectName = new ObjectName(name + ",ServiceName=" + StringUtils.prepareJMXNameValue(service.getServiceName()));
					} catch(MalformedObjectNameException e) {
						log.warn("Could not construct ObjectName", e);
						continue;
					} catch(NullPointerException e) {
						log.warn("Could not construct ObjectName", e);
						continue;
					}
					try {
						server.registerMBean(listener.createServiceMXBean(service), objectName);
						registeredNames.add(objectName);	
						service.addServiceStatusListener(listener);
					} catch(InstanceAlreadyExistsException e) {
						log.warn("Could not register MBean '" + objectName + "'", e);
					} catch(MBeanRegistrationException e) {
						log.warn("Could not register MBean '" + objectName + "'", e);
					} catch(NotCompliantMBeanException e) {
						log.warn("Could not register MBean '" + objectName + "'", e);
					}
				}
			}
		}
	}

	/**
	 * @see javax.management.MBeanRegistration#preDeregister()
	 */
	public void preDeregister() throws Exception {
	}

	/**
	 * @see javax.management.MBeanRegistration#preRegister(javax.management.MBeanServer, javax.management.ObjectName)
	 */
	public ObjectName preRegister(MBeanServer server, ObjectName name) throws Exception {
		if(name == null)
			name = new ObjectName(getClass().getPackage().getName() + ":Type=" + serviceManager.getClass().getSimpleName());
		currentObjectName = name;
		currentServer = server;
		listener = new JMXServiceStatusListener(name.getCanonicalName(), registeredNames);
		return name;
	}
}
