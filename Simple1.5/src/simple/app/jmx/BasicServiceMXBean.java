/**
 *
 */
package simple.app.jmx;

import simple.app.AbstractWorkQueueService;
import simple.app.Service;
import simple.app.ServiceException;
import simple.app.WorkQueue;

/**
 * @author Brian S. Krug
 *
 */
public class BasicServiceMXBean implements ServiceMXBean {
	protected final Service service;
	public BasicServiceMXBean(Service service) {
		super();
		this.service = service;
	}

	public String getServiceClassName() {
		return service.getClass().getName();
	}

	public String getQueueKey() {
		WorkQueue<?> q;
		if(service instanceof AbstractWorkQueueService && (q = ((AbstractWorkQueueService<?>) service).getQueue()) != null)
			return q.getQueueKey();
		return null;
	}
	public int getNumThreads() {
		return service.getNumThreads();
	}
	public int pauseThreads() throws ServiceException {
		return service.pauseThreads();
	}
	public int restartThreads(long timeout) throws ServiceException {
		return service.restartThreads(timeout);
	}
	public int startThreads(int numThreads) throws ServiceException {
		return service.startThreads(numThreads);
	}
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		return service.stopAllThreads(force, timeout);
	}
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		return service.stopThreads(numThreads, force, timeout);
	}
	public int unpauseThreads() throws ServiceException {
		return service.unpauseThreads();
	}
}
