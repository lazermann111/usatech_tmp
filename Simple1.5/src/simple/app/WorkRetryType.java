/**
 *
 */
package simple.app;

/**
 * @author Brian S. Krug
 *
 */
public enum WorkRetryType {
	NO_RETRY, NONBLOCKING_RETRY, BLOCKING_RETRY, IMMEDIATE_RETRY, SCHEDULED_RETRY
}
