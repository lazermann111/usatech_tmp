package simple.app;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.Socket;
import java.nio.charset.Charset;
import java.sql.DriverManager;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.net.SocketFactory;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.NestedNameTokenizer;
import simple.bean.ReflectionUtils;
import simple.bean.ReflectionUtils.BeanProperty;
import simple.bean.ReflectionUtils.IndexedBeanProperty;
import simple.bean.ReflectionUtils.MappedBeanProperty;
import simple.db.DataLayerMgr;
import simple.db.DataSourceFactory;
import simple.db.LocalDataSourceFactory;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

/** This class is the base class for Main (for services) and CommandLineInterface (for interactive programs). The <code>run()</code> method of this class performs the following operations:
 * <ol><li><u>Parses the command line arguments</u>: {@link #registerDefaultCommandLineArguments} establish what options are available. The default implementation
 *  registers one command line switches: <code>-p</code> (the properties file location). Run <code>java simple.app.Main</code> without arguments to see a usage message printed to standard output. To add or change
 *  the default command line switches or arguments, sub-class this class and override the {@link #registerDefaultCommandLineArguments} method. When you do so, your custom switches will be part of the usage message
 *  that is printed if arguments are not provided or are incorrect.</li>
 *  <li><u>Loads the properties file</u>: If the <code>-p</code> switch is provided with a file name after it, that file is loaded from the classpath. If no properties file
 *  name is provided the name of this class with ".properties" appended is used and if such a file exists in the classpath, it is loaded</li>
 *  <li><u>Calls the execute() method</u>: This abstract method must be implemented by a sub-class</li></ol>
 *
 *
 * @author Brian S. Krug
 * @see Main
 * @see CommandLineInterface
 */
public abstract class Base {
	private static final Log log = Log.getLog();
	public static final String DATA_SOURCE_FACTORY_PROPERTIES_PREFIX = "simple.db.datasource.";
	public static final String DATA_LAYER_FILES_PROPERTY = "simple.db.DataLayer.files";
	public static final String DB_DIALECT_PROPERTY = "simple.db.dialect";
	
	protected static final String DATA_SOURCE_FACTORY_PROPERTIES_SUBCONFIG = DATA_SOURCE_FACTORY_PROPERTIES_PREFIX.substring(0, DATA_SOURCE_FACTORY_PROPERTIES_PREFIX.length() - 1);
	protected final Map<String,CommandLineArgument> commandLineSwitches = new LinkedHashMap<String, CommandLineArgument>();
	protected final List<CommandLineArgument> commandLineArgs = new ArrayList<CommandLineArgument>();
	public static final String STATIC_CONFIG_PREFIX = "STATIC.";
	public static final String SYSTEM_CONFIG_PREFIX = "java.lang.System.";
	protected static final String STATIC_CONFIG_SUBCONFIG = STATIC_CONFIG_PREFIX.substring(0, STATIC_CONFIG_PREFIX.length() - 1);
	protected static final String SYSTEM_CONFIG_SUBCONFIG = (STATIC_CONFIG_PREFIX + SYSTEM_CONFIG_PREFIX).substring(0, STATIC_CONFIG_PREFIX.length() + SYSTEM_CONFIG_PREFIX.length() - 1);

	protected static class CommandLineArgument {
		public String shortName;
		public String longName;
		public boolean optional;
		public boolean content;
		public String propertyName;
		public String description;
	}

	public Base() {
		init();
	}

	protected void init() {
		registerDefaultCommandLineArguments();

	}
	protected void finishAndExit(String message, int exitCode, Throwable exception) {
		finishAndExit(message, exitCode, false, exception);
	}

	protected void finishAndExit(String message, int exitCode, boolean printUsage, Throwable exception) {
		log.warn("Exitting: " + message, exception);
		Log.finish();
		PrintStream out;
		if(exitCode == 0)
			out = System.out;
		else
			out = System.err;
		if(message != null) out.println(message);
		if(printUsage) printUsage(out);
		if(exception != null) exception.printStackTrace(out);
		System.exit(exitCode);
	}

	protected void registerDefaultCommandLineArguments() {
		registerCommandLineSwitch('p', "properties-file", true, true, "propertiesFile", "The properties file to use");
	}

	/** Registers a command line switch such as <code>-p</code>.
	 * @param ch The switch character.
	 * @param alias An optional alias that can be used as <code>--alias</code>
	 * @param optional Whether this switch is optional or not
	 * @param content Whether this switch expects a command line argument to follow it that contains the value of the switch
	 * @param propertyName The property in which to store the value of the switch. (If <code>content</code> is <code>true</code>, the value
	 * of the command line argument immediately after the switch is used, otherwise <code>true</code> is used as the value for the given propertyName
	 * @param description A description to use in the usage message for this switch
	 * @return <code>true</code> if this switch has never been registered before, <code>false</code> if this switch is replacing one already registered
	 * for the specified character.
	 */
	public boolean registerCommandLineSwitch(char ch, String alias, boolean optional, boolean content, String propertyName, String description) {
		CommandLineArgument cla = new CommandLineArgument();
		cla.shortName = (ch == '-' ? "" : "-") + ch;
		alias = fixCLAName(alias);
		cla.longName = (alias == null ? null : "--" + alias);
		cla.optional = optional;
		cla.content = content;
		cla.propertyName = propertyName;
		cla.description = (description == null || (description=description.trim()).length() == 0 ? "The " + propertyName + " property" : description);
		boolean added = (commandLineSwitches.put(cla.shortName, cla) == null);
		if(cla.longName != null)
			commandLineSwitches.put(cla.longName, cla);
		return added;
	}

	/** Registers a command line argument. Command Line Arguments must be registered in the order they are expected to appear.
	 *
	 * @param optional Whether this argument is optional or not
	 * @param propertyName The property in which to store the value of this argument
	 * @param description A description to use in the usage message for this argument
	 */
	public void registerCommandLineArgument(boolean optional, String propertyName, String description) {
		CommandLineArgument cla = new CommandLineArgument();
		cla.optional = optional;
		cla.content = true;
		cla.propertyName = propertyName;
		cla.description = (description == null || (description=description.trim()).length() == 0 ? "The " + propertyName + " property" : description);
		commandLineArgs.add(cla);
	}

	/** Deregisters a command line switch.
	 * @param ch The switch character.
	 * @return <code>true</code> if this switch was deregistered, <code>false</code> if the switch was not
	 * previously registered
	*/
	public boolean deregisterCommandLineSwitch(char ch) {
		CommandLineArgument cla = commandLineSwitches.remove((ch == '-' ? "" : "-") + ch);
		if(cla == null) return false;
		if(cla.longName != null) commandLineSwitches.remove(cla.longName);
		return true;
	}
	/** Deregisters a command line argument.
	 * @param order The index order (1-based) of the argument to deregister.
	 * @return <code>true</code> if an argument was deregistered, <code>false</code> if the list of arguments
	 * contains less entries than the specified <code>order</order>
	 */
	public boolean deregisterCommandLineArgument(int order) {
		if(commandLineArgs.size() >= order) {
			commandLineArgs.remove(order-1);
			return true;
		} else
			return false;
	}

	protected String fixCLAName(String name) {
		return (name == null || (name=name.trim()).length() == 0 ? null : name);
		/*
		int pos = 0;
		if(name != null) {
			if((name=name.trim()).length() == 0)
				name = null;
			else {
				while(pos < name.length() && name.charAt(pos) == '-') pos++;
				if(pos > 0 && pos < name.length())
					name = name.substring(pos);
			}
		}
		return name;*/
	}

	/** Prints a usage message to the specified {@link java.io.OutputStream} based on the
	 *  currently registered command line arguments and command line switches.
	 * @param out The {@link java.io.OutputStream} to which the usage message is written
	 * @see #registerCommandLineArgument(boolean, String, String)
	 * @see #registerCommandLineSwitch(char, String, boolean, boolean, String, String)
	 */
	public void printUsage(PrintStream out) {
		out.print("Usage: ");
		out.print(getClass().getSimpleName());
		Set<CommandLineArgument> printed = new LinkedHashSet<CommandLineArgument>();
		for(CommandLineArgument cla : commandLineSwitches.values()) {
			if(printed.add(cla)) {
				out.print(' ');
				if(cla.optional) out.print('[');
				if(cla.shortName != null) {
					out.print(cla.shortName);
				} else if(cla.longName != null) {
					out.print(cla.longName);
				}
				if(cla.content) {
					out.print(" <");
					out.print(cla.propertyName);
					out.print('>');
				}
				if(cla.optional) out.print(']');
			}
		}
		for(CommandLineArgument cla : commandLineArgs) {
			out.print(' ');
			if(cla.optional) out.print('[');
			out.print('<');
			out.print(cla.propertyName);
			out.print('>');
			if(cla.optional) out.print(']');
		}
		out.println();
		out.println();
		for(CommandLineArgument cla : printed) {
			out.print('\t');
			if(cla.shortName != null) {
				if(cla.longName != null) {
					out.print("(");
					out.print(cla.shortName);
					out.print("|");
					out.print(cla.longName);
					out.print(") ");
				} else {
					out.print(cla.shortName);
					out.print(' ');
				}
			} else if(cla.longName != null) {
				out.print(cla.longName);
				out.print(' ');
			}
			if(cla.content) {
				out.print('<');
				out.print(cla.propertyName);
				out.print('>');
			}
			out.print(" \t\t");
			out.print(cla.description);
			if(cla.optional) out.print(" (optional)");
			out.println();
		}
		for(CommandLineArgument cla : commandLineArgs) {
			out.print('\t');
			out.print('<');
			out.print(cla.propertyName);
			out.print('>');
			out.print(" \t\t");
			out.print(cla.description);
			if(cla.optional) out.print(" (optional)");
			out.println();
		}

		out.println();
	}

	protected void printInvocation(String[] args) {
		System.out.print("Running ");
		System.out.print(getClass().getName());
		System.out.print(" with arguments \"");
		if(args != null)
			for(int i = 0; i < args.length; i++) {
				if(i > 0)
					System.out.print(' ');
				System.out.print(args[i]);
			}
		System.out.println('"');
	}
	/** Performs the steps outlined in the {@link Base} class summary:
	 * <ol><li>Parses the command line arguments</li>
	 *  <li>Loads the properties file</li>
	 *  <li>Performs the action specified</li></ol>
	 * @param args The command line arguments
	 * @see Base
	 */
	public void run(String[] args) {
		printInvocation(args);
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		Properties properties;
		if(hasPropertiesFile())
			try {
				properties = getProperties(ConvertUtils.getStringSafely(argMap.get("propertiesFile")), getClass(), null);
			} catch(IOException e) {
				finishAndExit("Could not read properties file", 120, e);
				return;
			}
		else
			properties = new Properties();
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}
		
		execute(argMap, properties);

		Log.finish();
		System.exit(0);
	}

	protected boolean hasPropertiesFile() {
		return true;
	}
	
	/**
	 * @param argMap
	 * @param properties
	 */
	protected abstract void execute(Map<String, Object> argMap, Properties properties) ;

	protected String sendCommand(String host, int port, String command) throws IOException {
		Socket socket = SocketFactory.getDefault().createSocket(host, port);
		InputStream in = socket.getInputStream();
		//read all inputInputStream in;
		try {
			while(in.available() > 0) {
				in.skip(in.available());
			}
		} catch(IOException e) {
			IOException ioe = new IOException("An error occured while disgarding existing data on input stream");
			ioe.initCause(e);
			throw ioe;
		}
		PrintStream ps =  new PrintStream(socket.getOutputStream());
		ps.println(command);
		InputStreamReader reader = new InputStreamReader(in);
		StringBuilder sb = new StringBuilder();
		try {
			int ch = reader.read();
			if(ch < 0) {
				throw new IOException("Server closed input stream after command was sent. The status of the process is indeterminate.");
			}
			sb.append((char)ch);
			char[] c = new char[1024];
			float per = Charset.forName(reader.getEncoding()).newDecoder().maxCharsPerByte();

			while(in.available() * per > 0) {
				int len = reader.read(c, 0, Math.max(1, (int)(in.available() * per)));
				sb.append(c, 0, len);
			}
			while(sb.lastIndexOf("\n") < 0) {
				ch = reader.read();
				if(ch < 0) {
					throw new IOException("Server closed input stream after sending '" + sb.toString() + "'. The status of the process is indeterminate.");
				}
				sb.append((char)ch);
				while(in.available() * per > 0) {
					int len = reader.read(c, 0, Math.max(1, (int)(in.available() * per)));
					sb.append(c, 0, len);
				}
			}
		} catch(IOException e) {
			throw new IOException("Error occurred reading input stream after receiving '" + sb.toString() + "'. The status of the process is indeterminate.");
		}
		return sb.toString();
	}

	/** Parses the command line arguments as configured by the {@link #registerDefaultActions}
	 *  and {@link #registerDefaultCommandLineArguments} methods.
	 * @param args The command line arguments
	 * @return A Map of key-values pulled from the command line arguments based on the configuration setup in the {@link #registerDefaultActions}
	 *  and {@link #registerDefaultCommandLineArguments} methods.
	 * @throws IOException If a required switch or argument is not provided
	 */
	public Map<String,Object> parseArguments(String[] args) throws IOException {
		Map<String,Object> argMap = new HashMap<String, Object>();
		Set<String> pns = new HashSet<String>();
		int k = 0;
		for(int i = 0; i < args.length; i++) {
			CommandLineArgument cla = commandLineSwitches.get(args[i]);
			if(cla == null) {
				if(k < commandLineArgs.size()) { //TODO: This does not handle optional command line arguments right now
					cla = commandLineArgs.get(k++);
					argMap.put(cla.propertyName, args[i]);
					pns.add(cla.propertyName);
				} else if(allowExtraArguments(argMap)) {
					List<String> extraArgs = CollectionUtils.uncheckedCollection((List<?>)argMap.get("*"), String.class);
					if(extraArgs == null) {
						extraArgs = new ArrayList<String>();
						argMap.put("*", extraArgs);
					}
					extraArgs.add(args[i]);
				} else
					throw new IOException("Invalid option, '" + args[i] + "',  at position " + (i+1) + " used.");
			} else {
				if(cla.content && i + 1 >= args.length)
					throw new IOException("The value for option, '" + args[i] + "', was not specified.");
				argMap.put(cla.propertyName, cla.content ? args[++i] : true);
				pns.add(cla.propertyName);
			}
		}
		for(CommandLineArgument cla : commandLineSwitches.values()) {
			if(!cla.optional && !pns.contains(cla.propertyName))
				throw new IOException("Required option '" + (cla.shortName == null ? cla.longName : cla.shortName) + "' was not used.");
		}
		for(CommandLineArgument cla : commandLineArgs) {
			if(!cla.optional && !pns.contains(cla.propertyName))
				throw new IOException("Required argument <" + cla.propertyName + "> was not used.");
		}
		return argMap;
	}

	protected boolean allowExtraArguments(Map<String, Object> argMap) {
		return false;
	}

	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object. Then returns that {@link java.util.Properties} object. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	public Properties getProperties(String propertiesFile, Class<?> appClass, Properties defaults) throws IOException {
		return loadProperties(propertiesFile, appClass, defaults);
	}
	/** Reads in Data Layer Files to the default DataLayer. If the "simple.db.DataLayer.files" property is provided, its value is read as a comma- or semi-colon- separated
	 * list of data layer files which are read into the default {@link simple.db.DataLayer}. See the
	 * <a href="{@docRoot}/simple/resources/data-layer.xsd">Data Layer Schema Definition</a> for more information on the format of these files.
	 * @param properties
	 * @throws ServiceException If an error occurs loading the data layer files
	 */
	public static void configureDataLayer(Properties properties) throws ServiceException {
		DialectResolver.initDialect(properties.getProperty(DB_DIALECT_PROPERTY));

		String[] files = StringUtils.split(properties.getProperty(DATA_LAYER_FILES_PROPERTY),
				StringUtils.STANDARD_DELIMS, false);
		if (files == null) {
			return;
		}
		for (String file : files) {
			log.debug("Loading the file '" + file + "' into the DataLayer");
			try {
				file = buildDataLayerFilenameWithDialectSpecifics(file);
				ConfigLoader.loadConfig(file);
			} catch (ConfigException e) {
				throw new ServiceException("Could not load data-layer file, '" + file + "'", e);
			} catch (IOException e) {
				throw new ServiceException("Could not load data-layer file, '" + file + "'", e);
			} catch (LoadingException e) {
				throw new ServiceException("Could not load data-layer file, '" + file + "'", e);
			}
		}
	}

	protected static String buildDataLayerFilenameWithDialectSpecifics(String filename) {
		if (DialectResolver.isOracle()) {
			return filename;
		}

		return filename.replace(".xml", "") + "-" + DialectResolver.resolveDialect() + ".xml";
	}

	/** Creates and registers a {@link simle.db.DataSourceFactory}. The "simple.db.datasource.class" property in the provided Properties determines
	 * the implementation of DataSourceFactory that is used. If that property is not provided then {@link simple.db.LocalDataSourceFactory}
	 * is used. All other properties that start with "simple.db.datasource." are used to configure the DataSourceFactory. See {@link configure}
	 * for details about how this is done. If no properties that start with "simple.db.datasource." are provided, a DataSourceFactory is not registered.
	 * But if properties are provided, the configured DataSourceFactory is passed to the
	 * {@link simple.db.DataLayerMgr#setDataSourceFactory(simple.db.DataSourceFactory)} method
	 * @param properties
	 * @param namedReferences
	 * @throws ServiceException If the DataSourceFactory could not be configured
	 */
	public static void configureDataSourceFactory(Properties properties, Map<String,Object> namedReferences) throws ServiceException {
		Properties subProperties = CollectionUtils.getSubProperties(properties, DATA_SOURCE_FACTORY_PROPERTIES_PREFIX);
		if(subProperties.isEmpty()) return;
		DataSourceFactory dsf = configure(DataSourceFactory.class, subProperties, properties, namedReferences, true, LocalDataSourceFactory.class);
		DataLayerMgr.setDataSourceFactory(dsf);
		int loginTimeout = ConvertUtils.getIntSafely(subProperties.getProperty("loginTimeout"), -1);
		if(loginTimeout >= 0) {
			DriverManager.setLoginTimeout(loginTimeout);
		}
	}

	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object. Then returns that {@link java.util.Properties} object. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	public static Properties loadProperties(String propertiesFile, Class<?> appClass, Properties defaults) throws IOException {
		String origpf = propertiesFile;
		if(propertiesFile == null) propertiesFile = appClass.getSimpleName() + ".properties";
		Properties properties = new Properties(defaults);
		InputStream propIn = ReflectionUtils.getContextClassLoader().getResourceAsStream(propertiesFile);
		if(propIn == null) appClass.getResourceAsStream(propertiesFile);
		if(propIn != null) properties.load(propIn);
		else handlePropertiesNotFound(propertiesFile, origpf, appClass);
		return properties;
	}

	public static void handlePropertiesNotFound(String propertiesFile, String origPropertiesFile, Class<?> appClass) throws IOException {
		throw new IOException("Could not find properties file '" + propertiesFile +"' for " + appClass.getName());
	}
	
	/** Creates an object and sets its properties according to the key/value pairs in the properties argument. If a property's value
	 * is enclosed in double-quotes, this signifies a named reference. The configure method will create one instance of each named reference using
	 * the name as the property prefix and setting it on each object that references it. (i.e. - if a propery is globalObject="com.example.MyGlobalObject", then this method
	 * will configure an object using all "sub" properties of "com.example.MyGlobalObject" (thus com.example.MyGlobalObject.class would specify the concrete
	 * class of the object, and that object would then be used to set the globalObject property on the current object being configured.)
	 * @param <C>
	 * @param clazz The super class of the object to be returned
	 * @param propertyBase The property key that is used for configuring the object
	 * @param rootProperties The root properties. Used for named references
	 * @param namedReferences A map that holds named references. Must be modifiable. Use null to have method create its own map.
	 * @param initialize Whether to call "initialize" on objects or not
	 * @param constructorParams List of params for the constructor of the object
	 * @return The configured object
	 * @throws ServiceException
	 */
	public static <C> C configureFromBase(Class<C> clazz, String propertyBase, Properties rootProperties, Map<String,Object> namedReferences, boolean initialize, Object... constructorParams) throws ServiceException {	
		String direct = rootProperties.getProperty(propertyBase);
		if(direct != null && direct.length() > 2 && direct.charAt(0) == '"' && direct.charAt(direct.length()-1) == '"') {
			String name = direct.substring(1, direct.length() - 1);
			Object value;
			if(namedReferences == null) {
				namedReferences = new HashMap<String, Object>();
				value = null;
			} else {
				value = namedReferences.get(name);
			}
			if(value == null) {
				Properties refProperties = CollectionUtils.getSubProperties(rootProperties, name + '.');
				value = configure(clazz, refProperties, rootProperties, namedReferences, initialize);
				namedReferences.put(name, value);
			} else if(!clazz.isInstance(value)) {
				throw new ServiceException("Property '" + propertyBase + "' references an object of class '" + value.getClass().getName() + "' that does not sub-class " + clazz.getName());
			}
			return clazz.cast(value);
		}
		Properties subProperties = CollectionUtils.getSubProperties(rootProperties, propertyBase + '.');
		return configure(clazz, subProperties, rootProperties, namedReferences, initialize, constructorParams);
	}
	
	/** Creates an object and sets its properties according to the key/value pairs in the properties argument. If a property's value
	 * is enclosed in double-quotes, this signifies a named reference. The configure method will create one instance of each named reference using
	 * the name as the property prefix and setting it on each object that references it. (i.e. - if a propery is globalObject="com.example.MyGlobalObject", then this method
	 * will configure an object using all "sub" properties of "com.example.MyGlobalObject" (thus com.example.MyGlobalObject.class would specify the concrete
	 * class of the object, and that object would then be used to set the globalObject property on the current object being configured.)
	 * @param <C>
	 * @param clazz The super class of the object to be returned
	 * @param properties The properties that apply to the object. The class property is used to determine which concrete class is used.
	 * @param rootProperties The root properties. Used for named references
	 * @param namedReferences A map that holds named references. Must be modifiable. Use null to have method create its own map.
	 * @param constructorParams List of params for the constructor of the object
	 * @return The configured object
	 * @throws ServiceException
	 */
	public static <C> C configure(Class<C> clazz, Properties properties, Properties rootProperties) throws ServiceException {
		return configure(clazz, properties, rootProperties, null, true);
	}

	/** Creates an object and sets its properties according to the key/value pairs in the properties argument. If a property's value
	 * is enclosed in double-quotes, this signifies a named reference. The configure method will create one instance of each named reference using
	 * the name as the property prefix and setting it on each object that references it. (i.e. - if a propery is globalObject="com.example.MyGlobalObject", then this method
	 * will configure an object using all "sub" properties of "com.example.MyGlobalObject" (thus com.example.MyGlobalObject.class would specify the concrete
	 * class of the object, and that object would then be used to set the globalObject property on the current object being configured.)
	 * @param <C>
	 * @param clazz The super class of the object to be returned
	 * @param properties The properties that apply to the object. The class property is used to determine which concrete class is used.
	 * @param rootProperties The root properties. Used for named references
	 * @param namedReferences A map that holds named references. Must be modifiable. Use null to have method create its own map.
	 * @param constructorParams List of params for the constructor of the object
	 * @return The configured object
	 * @throws ServiceException
	 */
	public static <C> C configure(Class<C> clazz, Properties properties, Properties rootProperties, Map<String,Object> namedReferences, boolean initialize, Object... constructorParams) throws ServiceException {
		return configure(clazz, properties, rootProperties, namedReferences, initialize, null, constructorParams);
	}

	/** Creates an object and sets its properties according to the key/value pairs in the properties argument. If a property's value
	 * is enclosed in double-quotes, this signifies a named reference. The configure method will create one instance of each named reference using
	 * the name as the property prefix and setting it on each object that references it. (i.e. - if a propery is globalObject="com.example.MyGlobalObject", then this method
	 * will configure an object using all "sub" properties of "com.example.MyGlobalObject" (thus com.example.MyGlobalObject.class would specify the concrete
	 * class of the object, and that object would then be used to set the globalObject property on the current object being configured.)
	 * @param <C>
	 * @param clazz The super class of the object to be returned
	 * @param properties The properties that apply to the object. The class property is used to determine which concrete class is used.
	 * @param rootProperties The root properties. Used for named references
	 * @param namedReferences A map that holds named references. Must be modifiable. Use null to have method create its own map.
	 * @param constructorParams List of params for the constructor of the object
	 * @return The configured object
	 * @throws ServiceException
	 */
	public static <C> C configure(Class<C> clazz, Properties properties, Properties rootProperties, Map<String,Object> namedReferences, boolean initialize, Class<? extends C> defaultClazz, Object... constructorParams) throws ServiceException {
		String className = properties.getProperty("class");
		Class<? extends C> objectClass;
		if(className != null && (className=className.trim()).length() > 0) {
			try {
				objectClass = Class.forName(className).asSubclass(clazz);
			} catch(ClassNotFoundException e) {
				throw new ServiceException("Class '" + className + "' could not be found", e);
			} catch(ClassCastException e) {
				throw new ServiceException("Class '" + className + "' is not a sub class of '" + clazz.getName() + "'", e);
			}
		} else if(defaultClazz != null) {
			if(defaultClazz.isInterface() || Modifier.isAbstract(defaultClazz.getModifiers()))
				throw new ServiceException("Default Class " + defaultClazz.getName() + " is not instantiable");
			objectClass = defaultClazz;
		} else if(clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers())) {
			return null; //could not be created
		} else {
			objectClass = clazz;
		}
		try {
			C object;
			Object[] params1;
			if(constructorParams == null || constructorParams.length == 0) {
				params1 = new Object[] {properties};
			} else {
				params1 = new Object[constructorParams.length + 1];
				params1[params1.length-1] = properties;
				System.arraycopy(constructorParams, 0, params1, 0, constructorParams.length);
			}
			try {
				object = ReflectionUtils.createObject(objectClass, params1);
			} catch(SecurityException e) {
				object = ReflectionUtils.createObject(objectClass, constructorParams);
				configureProperties(object, properties, rootProperties, namedReferences == null ? new HashMap<String, Object>() : namedReferences, initialize);
			} catch(IllegalArgumentException e) {
				object = ReflectionUtils.createObject(objectClass, constructorParams);
				configureProperties(object, properties, rootProperties, namedReferences == null ? new HashMap<String, Object>() : namedReferences, initialize);
			}

			return object;
		} catch(IllegalArgumentException e) {
			throw new ServiceException(e.getMessage(), e);
		} catch(SecurityException e) {
			StringBuilder sb = new StringBuilder();
	        for(Object pt : constructorParams)
	        	sb.append(pt == null ? "*" : constructorParams.getClass().getName()).append(',');
	        sb.setLength(sb.length()-1);
	        throw new ServiceException("Could not find a constructor matching " + objectClass.getSimpleName() + "(Properties," + sb.toString() + ") or " + objectClass.getSimpleName() + "(" + sb.toString() + ") for Class '" + className + "'", e);
		} catch(InstantiationException e) {
			throw new ServiceException("Class '" + className + "' could not be instantiated", e);
		} catch(IllegalAccessException e) {
			throw new ServiceException("Class '" + className + "' could not be instantiated", e);
		} catch(IntrospectionException e) {
			throw new ServiceException("Class '" + className + "' could not be configured", e);
		} catch(InvocationTargetException e) {
			throw new ServiceException("Class '" + className + "' could not be configured", e);
		} catch(ConvertException e) {
			throw new ServiceException("Class '" + className + "' could not be configured", e);
		} catch(ParseException e) {
			throw new ServiceException("Class '" + className + "' could not be configured", e);
		}
	}

	protected static void configureProperties(Object object, Properties properties, Properties rootProperties, Map<String,Object> namedReferences, boolean initialize) throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException, InstantiationException, ConvertException, ServiceException {
		Set<String> subKeys = new HashSet<String>();
		for(Map.Entry<Object,Object> entry : properties.entrySet()) {
			String key = (String)entry.getKey();
			if(key.equals("class"))
				continue;
			NestedNameTokenizer tok = new NestedNameTokenizer(key);
			String prop = tok.nextProperty();
			if(subKeys.add(tok.getConsumed())) { // use tok.getConsumed() instead of prop to allow indexed and mapped to do all indices/keys
				BeanProperty bp = ReflectionUtils.findBeanPropertyFor(object, prop, true);
				if(bp != null) {
					Object value = null;
					Properties subProperties = CollectionUtils.getSubProperties(properties, tok.getConsumed() + '.');
					String base;
					if(tok.hasNext()) {
						base = properties.getProperty(tok.getConsumed());
					} else {
						base = (String)entry.getValue();
					}
					if(tok.isIndexed()) {
						IndexedBeanProperty ibp = (bp instanceof IndexedBeanProperty ? (IndexedBeanProperty) bp : new ReflectionUtils.WrappedIndexedBeanProperty(bp, object.getClass()));
						if(ibp.isIndexedWritable()) {
							value = constructValue(base, ibp.getIndexedType(), subProperties, rootProperties, namedReferences, initialize);
							ibp.setIndexedValue(object, tok.getIndex(), value);
						} else if(ibp.isIndexedReadable()) {
							value = ibp.getIndexedValue(object, tok.getIndex());
							if(value != null)
								configureProperties(value, subProperties, rootProperties, namedReferences, initialize);
						}
					} else if(tok.isMapped()) {
						MappedBeanProperty mbp = (bp instanceof MappedBeanProperty ? (MappedBeanProperty) bp : new ReflectionUtils.WrappedMappedBeanProperty(bp, object.getClass()));
						if(mbp.isMappedWritable()) {
							value = constructValue(base, mbp.getMappedType(), subProperties, rootProperties, namedReferences, initialize);
							mbp.setMappedValue(object, tok.getKey(), value);
						} else if(mbp.isMappedReadable()) {
							value = mbp.getMappedValue(object, tok.getKey());
							if(value != null)
								configureProperties(value, subProperties, rootProperties, namedReferences, initialize);
						}
					} else {
						if(bp.isWritable()) {
							value = constructValue(base, bp.getType(), subProperties, rootProperties, namedReferences, initialize);
							bp.setValue(object, value);
						} else if(bp.isReadable()) {
							value = bp.getValue(object);
							if(value != null)
								configureProperties(value, subProperties, rootProperties, namedReferences, initialize);
						}
					}
				}
			}
        }
		if(initialize) {
			try {
				ReflectionUtils.invokeMethod(object, "initialize");
			} catch(NoSuchMethodException e) {
				//ignore
			}
		}
	}

	protected static Object constructValue(String base, Class<?> type, Properties subProperties, Properties rootProperties, Map<String,Object> namedReferences, boolean initialize) throws ServiceException {
		Object value = null;
		if(base != null) {
			try {
				value = ConvertUtils.convert(type, base);
			} catch(ConvertException e) {
				if(base.length() > 2 && base.charAt(0) == '"' && base.charAt(base.length()-1) == '"') {
					String name = base.substring(1, base.length() - 1);
					value = namedReferences.get(name);
					if(value == null) {
						Properties refProperties = CollectionUtils.getSubProperties(rootProperties, name + '.');
						value = configure(type, refProperties, rootProperties, namedReferences, initialize);
						namedReferences.put(name, value);
					}
				}
			}
		}
		if(value == null)
			value = configure(type, subProperties, rootProperties, namedReferences, initialize);
		/* The following could lead to confusion (since a named reference's properties could be changed by one of the referring objects) so don't do it
		else if(!subProperties.isEmpty())
			configureProperties(value, subProperties, rootProperties, namedReferences, initialize);
		*/
		return value;
	}
	
	public static void configureSystem(Properties properties, Map<String,Object> namedReferences) {
		if(namedReferences == null)
			namedReferences = new HashMap<String, Object>();
		for(Map.Entry<Object, Object> entry : properties.entrySet()) {
			if(entry.getKey() instanceof String) {
				String s = (String)entry.getKey();
				if(s.startsWith(Base.SYSTEM_CONFIG_PREFIX)) {
					setStaticProperty(s.substring(Base.SYSTEM_CONFIG_PREFIX.length()), entry.getValue());
				}
			}
		}
	}

	public static void configureStatics(Properties properties, Map<String, Object> namedReferences) {
		if(namedReferences == null)
			namedReferences = new HashMap<String, Object>();
		for(Map.Entry<Object, Object> entry : properties.entrySet()) {
			if(entry.getKey() instanceof String) {
				String s = (String) entry.getKey();
				if(s.startsWith(Base.STATIC_CONFIG_PREFIX) && !s.startsWith(Base.SYSTEM_CONFIG_PREFIX)) {
					setStaticProperty(s.substring(Base.STATIC_CONFIG_PREFIX.length()), entry.getValue());
				}
			}
		}
	}
	protected static void setStaticProperty(String property, Object value) {
		try {
			if(!ReflectionUtils.setStaticProperty(property, value)) {
				log.info("Could not set static property '" + property + "'");
			}
		} catch(ClassCastException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(IllegalArgumentException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(IntrospectionException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(IllegalAccessException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(InvocationTargetException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(InstantiationException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(ConvertException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(ParseException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		} catch(NoSuchMethodException e) {
			log.warn("Error while setting static property '" + property + "'", e);
		}
	}
}
