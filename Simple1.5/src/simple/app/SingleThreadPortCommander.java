package simple.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import simple.io.Log;
import simple.io.server.SocketStreams;

public class SingleThreadPortCommander extends InOutCommander {
	private static final Log log = Log.getLog();
	protected final SocketStreams socketStreams;
	public SingleThreadPortCommander(int port) throws IOException {
		this(new SocketStreams(port));
	}
	protected SingleThreadPortCommander(SocketStreams socketStreams) {
		super(new BufferedReader(new InputStreamReader(socketStreams.getInputStream())), socketStreams.getOutputStream());
		this.socketStreams = socketStreams;
	}

	@Override
	public void shutdown() {
		try {
			socketStreams.close();
		} catch(IOException e) {
			log.warn("Could not close socket streams", e);
		}
		super.shutdown();
	}

	@Override
	protected void initialize() {
		super.initialize();
		log.info("Listening on port " + socketStreams.getPort() + " for management commands");
	}
	@Override
	public String getDescription() {
		return "on port " + socketStreams.getPort();
	}
}
