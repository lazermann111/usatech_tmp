/**
 *
 */
package simple.app;

import java.util.Collection;

/**
 * @author Brian S. Krug
 *
 * @param <C>
 */
public interface CommandManager<C> {
	/** Returns the context object that is passed to every command
	 * @return
	 */
	public C getContext();

	/** Adds a command to the manager.
	 * @param command The command to add
	 * @return <code>true</code> if a command was already registered under that key and is replaced; <code>false</code> otherwise.
	 */
	public boolean registerCommand(Command<C> command);

	/** Removes a command from the manager.
	 * @param commandKey The key of the command to remove
	 * @return <code>true</code> if the specified command was registered and is removed; <code>false</code> otherwise.
	 */
	public boolean deregisterCommand(String commandKey);

	/** Returns the command registered under the given key or null if not found
	 *
	 * @param key
	 * @return
	 */
	public Command<C> getCommand(String key);
	/**
	 * @return the currently registered commands
	 */
	public Collection<Command<C>> getCommands();

	public String getHelpPrompt() ;

}