package simple.app;

import java.util.concurrent.Future;

import simple.util.concurrent.TrivialFuture;

public interface Service {
	public static final Future<Boolean> FALSE_FUTURE = new TrivialFuture<Boolean>(false);
	public static final Future<Boolean> TRUE_FUTURE = new TrivialFuture<Boolean>(true);
	public static final Future<Integer> ZERO_FUTURE = new TrivialFuture<Integer>(0);

	public String getServiceName() ;
	public int getNumThreads() ;
	public int startThreads(int numThreads) throws ServiceException;
	public int restartThreads(long timeout) throws ServiceException;
	public int pauseThreads() throws ServiceException;
	public int unpauseThreads() throws ServiceException;
	/** Begins stopping the indicated number of threads in the Service and then returns a Future that returns a result from get()
	 *  indicating the number of threads successfully stopped. Each thread may take up to <code>timeout</code> of the get() method to stop before giving up.
	 * @param numThreads The number of threads to stop
	 * @param force Whether to force the threads to stop
	 * @return A Future that returns from its get() methods the number of threads successfully stopped
	 */
	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException;
	/** Begins stopping the all threads in the Service and then returns a Future that returns a result from get()
	 *  indicating the number of threads successfully stopped. Each thread may take up to <code>timeout</code> of the get() method to stop before giving up.
	 * @param force Whether to force the threads to stop
	 * @return A Future that returns from its get() methods the number of threads successfully stopped
	 */
	public Future<Integer> stopAllThreads(boolean force) throws ServiceException;
	public int stopAllThreads(boolean force, long timeout) throws ServiceException ;
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException ;
	public boolean addServiceStatusListener(ServiceStatusListener listener) ;
	public boolean removeServiceStatusListener(ServiceStatusListener listener) ;
	/** Begins shutting down the Service and then returns a Future that returns a result from get()
	 *  indicating when the Service has shutdown and if it did so nicely. 
	 *  The Queue in the Service may take up to the get() <code>timeout</code> milliseconds to shutdown
	 *  and the Service may take up to <code>timeout</code> of the get() method to shutdown nicely
	 *  and then it must shut itself down forcibly immediately.
	 * @return A Future that returns from its get() methods, <code>true</code>, if and only if the service was shut down nicely
	 */
	public Future<Boolean> shutdown() throws ServiceException;
	/** Handles any thing that needs to be done prior to all the Services being shut down and then returns a Future that returns a result from get()
	 *  indicating when the Service has been prepared for shutdown and if it did so nicely. 
	 *  Preparation for shutdown may take up the get() <code>timeout</code> milliseconds
	 * @return A Future that returns from its get() methods, <code>true</code>, if and only if the service has been prepared for shutdown nicely
	 */
	public Future<Boolean> prepareShutdown() throws ServiceException;
}
