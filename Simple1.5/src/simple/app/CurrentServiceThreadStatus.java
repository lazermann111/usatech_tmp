package simple.app;


public class CurrentServiceThreadStatus extends CurrentServiceStatusListener {
	@Override
	protected String getKey(Service service, Thread thread) {
		return String.valueOf(thread == null ? 0 : thread.getId()) + '-' + service.getServiceName();
	}
}
