package simple.app;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.Socket;
import java.nio.charset.Charset;
import java.sql.DriverManager;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.net.SocketFactory;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.MapConfiguration;
import org.apache.commons.configuration.PropertyConverter;
import org.apache.commons.configuration.SubsetConfiguration;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.NestedNameTokenizer;
import simple.bean.ReflectionUtils;
import simple.bean.ReflectionUtils.BeanProperty;
import simple.bean.ReflectionUtils.IndexedBeanProperty;
import simple.bean.ReflectionUtils.MappedBeanProperty;
import simple.bean.Setter;
import simple.db.DataLayerMgr;
import simple.db.DataSourceFactory;
import simple.db.LocalDataSourceFactory;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.text.RegexUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

/** This class is the base class for Main (for services) and CommandLineInterface (for interactive programs). The <code>run()</code> method of this class performs the following operations:
 * <ol><li><u>Parses the command line arguments</u>: {@link #registerDefaultCommandLineArguments} establish what options are available. The default implementation
 *  registers one command line switches: <code>-p</code> (the properties file location). Run <code>java simple.app.Main</code> without arguments to see a usage message printed to standard output. To add or change
 *  the default command line switches or arguments, sub-class this class and override the {@link #registerDefaultCommandLineArguments} method. When you do so, your custom switches will be part of the usage message
 *  that is printed if arguments are not provided or are incorrect.</li>
 *  <li><u>Loads the properties file</u>: If the <code>-p</code> switch is provided with a file name after it, that file is loaded from the classpath. If no properties file
 *  name is provided the name of this class with ".properties" appended is used and if such a file exists in the classpath, it is loaded</li>
 *  <li><u>Calls the execute() method</u>: This abstract method must be implemented by a sub-class</li></ol>
 *
 *
 * @author Brian S. Krug
 * @see Main
 * @see CommandLineInterface
 */
public abstract class BaseWithConfig extends Base {
	private static final Log log = Log.getLog();
	public BaseWithConfig() {
		super();
	}

	/** Performs the steps outlined in the {@link BaseWithConfig} class summary:
	 * <ol><li>Parses the command line arguments</li>
	 *  <li>Loads the properties file</li>
	 *  <li>Performs the action specified</li></ol>
	 * @param args The command line arguments
	 * @see BaseWithConfig
	 */
	@Override
	public void run(String[] args) {
		printInvocation(args);
		Map<String,Object> argMap;
		try {
			argMap = parseArguments(args);
		} catch(IOException e) {
			finishAndExit(e.getMessage(), 100, true, null);
			return;
		}
		Configuration config;
		try {
			config = getConfig(ConvertUtils.getStringSafely(argMap.get("propertiesFile")), getClass(), null);
		} catch(IOException e) {
			finishAndExit("Could not read properties file", 120, e);
			return;
		}
		for(Map.Entry<String,Object> entry : argMap.entrySet()) {
			config.setProperty(entry.getKey(), entry.getValue());
		}

		execute(argMap, config);

		Log.finish();
		System.exit(0);
	}

	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object. Then returns that {@link java.util.Properties} object. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	public Configuration getConfig(String propertiesFile, Class<?> appClass, Configuration defaults) throws IOException {
		return loadConfig(propertiesFile, appClass, defaults);
	}
	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object. Then returns that {@link java.util.Properties} object. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	public static Configuration loadConfig(String propertiesFile, Class<?> appClass, Configuration defaults) throws IOException {
		return loadConfig(propertiesFile, appClass, defaults, null, 0, 0);
	}

	/**
	 * Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a {@link java.util.Properties} object. Then returns that {@link java.util.Properties}
	 * object. If the <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 * as the propertiesFile.
	 * 
	 * @param propertiesFile
	 *            The file in the classpath to load
	 * @param appClass
	 *            The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults
	 *            The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException
	 *             If the specified file could not be located in the classpath
	 */
	public static Configuration loadConfig(String propertiesFile, Class<?> appClass, Configuration defaults, final Configurer reconfigurer, long minInterval, long maxInterval) throws IOException {
		if(propertiesFile == null) propertiesFile = appClass.getSimpleName() + ".properties";
		final Configuration config;
		AppPropertiesConfiguration pc;
		ReloadingAppPropertiesConfiguration rpc;
		if(reconfigurer != null) {
			rpc = new ReloadingAppPropertiesConfiguration();
			pc = rpc;
		} else {
			rpc = null;
			pc = new AppPropertiesConfiguration();
		}
		pc.setFileName(propertiesFile);
		try {
			pc.load();
		} catch(ConfigurationException e) {
			if(e.getCause() instanceof IOException)
				throw (IOException)e.getCause();
			else {
				IOException ioe = new IOException(e.getMessage());
				ioe.initCause(e);
				throw ioe;
			}
		}
		if(defaults != null && !defaults.isEmpty()) {
			CompositeConfiguration cc = new CompositeConfiguration();
			cc.addConfiguration(pc);
			cc.addConfiguration(defaults);
			config = cc;
		} else {
			config = pc;
		}

		if(rpc != null)
			rpc.registerLoader(new Runnable() {
				@Override
				public void run() {
					reconfigurer.configure(config);
				}
			}, minInterval, maxInterval);

		return config;
	}

	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object using Apache Commons Configuration which
	 *  allows variable interpolation and inclusion of other properties files. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	@Override
	public Properties getProperties(String propertiesFile, Class<?> appClass, Properties defaults)
			throws IOException {
		return loadPropertiesWithConfig(propertiesFile, appClass, defaults);
	}

	/** Finds the file specified by the <code>propertyFile</code> parameter in the class path and loads it into a
	 *  {@link java.util.Properties} object using Apache Commons Configuration which
	 *  allows variable interpolation and inclusion of other properties files. If the
	 *  <code>propertiesFile</code> parameter is null, then the simple name of the <code>appClass</code> parameter with ".properties" appended is used
	 *  as the propertiesFile.
	 * @param propertiesFile The file in the classpath to load
	 * @param appClass The class whose simple name is used for the properties file, if the propertiesFile parameter is null.
	 * @param defaults The default properties to use. May be null.
	 * @return The {@link java.util.Properties} object loaded with the specified file
	 * @throws IOException If the specified file could not be located in the classpath
	 */
	public static Properties loadPropertiesWithConfig(String propertiesFile, Class<?> appClass, Properties defaults)
			throws IOException {
		return toProperties(loadConfig(propertiesFile, appClass, defaults == null ? null : new MapConfiguration(defaults)));
	}

	/**
	 * @param argMap
	 * @param properties
	 */
	@Override
	protected void execute(Map<String, Object> argMap, Properties properties) {
		execute(argMap, ConfigurationConverter.getConfiguration(properties));
	}

	protected abstract void execute(Map<String, Object> argMap, Configuration config) ;

	@Override
	protected String sendCommand(String host, int port, String command) throws IOException {
		Socket socket = SocketFactory.getDefault().createSocket(host, port);
		InputStream in = socket.getInputStream();
		//read all inputInputStream in;
		try {
			while(in.available() > 0) {
				in.skip(in.available());
			}
		} catch(IOException e) {
			IOException ioe = new IOException("An error occured while disgarding existing data on input stream");
			ioe.initCause(e);
			throw ioe;
		}
		PrintStream ps =  new PrintStream(socket.getOutputStream());
		ps.println(command);
		InputStreamReader reader = new InputStreamReader(in);
		StringBuilder sb = new StringBuilder();
		try {
			int ch = reader.read();
			if(ch < 0) {
				throw new IOException("Server closed input stream after command was sent. The status of the process is indeterminate.");
			}
			sb.append((char)ch);
			char[] c = new char[1024];
			float per = Charset.forName(reader.getEncoding()).newDecoder().maxCharsPerByte();

			while(in.available() * per > 0) {
				int len = reader.read(c, 0, Math.max(1, (int)(in.available() * per)));
				sb.append(c, 0, len);
			}
			while(sb.lastIndexOf("\n") < 0) {
				ch = reader.read();
				if(ch < 0) {
					throw new IOException("Server closed input stream after sending '" + sb.toString() + "'. The status of the process is indeterminate.");
				}
				sb.append((char)ch);
				while(in.available() * per > 0) {
					int len = reader.read(c, 0, Math.max(1, (int)(in.available() * per)));
					sb.append(c, 0, len);
				}
			}
		} catch(IOException e) {
			throw new IOException("Error occurred reading input stream after receiving '" + sb.toString() + "'. The status of the process is indeterminate.");
		}
		return sb.toString();
	}

	/** Parses the command line arguments as configured by the {@link #registerDefaultActions}
	 *  and {@link #registerDefaultCommandLineArguments} methods.
	 * @param args The command line arguments
	 * @return A Map of key-values pulled from the command line arguments based on the configuration setup in the {@link #registerDefaultActions}
	 *  and {@link #registerDefaultCommandLineArguments} methods.
	 * @throws IOException If a required switch or argument is not provided
	 */
	@Override
	public Map<String,Object> parseArguments(String[] args) throws IOException {
		Map<String,Object> argMap = new HashMap<String, Object>();
		Set<String> pns = new HashSet<String>();
		int k = 0;
		for(int i = 0; i < args.length; i++) {
			CommandLineArgument cla = commandLineSwitches.get(args[i]);
			if(cla == null) {
				if(k < commandLineArgs.size()) { //TODO: This does not handle optional command line arguments right now
					cla = commandLineArgs.get(k++);
					argMap.put(cla.propertyName, args[i]);
					pns.add(cla.propertyName);
				} else if(allowExtraArguments(argMap)) {
					List<String> extraArgs = CollectionUtils.uncheckedCollection((List<?>)argMap.get("*"), String.class);
					if(extraArgs == null) {
						extraArgs = new ArrayList<String>();
						argMap.put("*", extraArgs);
					}
					extraArgs.add(args[i]);
				} else
					throw new IOException("Invalid option, '" + args[i] + "',  at position " + (i+1) + " used.");
			} else {
				if(cla.content && i + 1 >= args.length)
					throw new IOException("The value for option, '" + args[i] + "', was not specified.");
				argMap.put(cla.propertyName, cla.content ? args[++i] : true);
				pns.add(cla.propertyName);
			}
		}
		for(CommandLineArgument cla : commandLineSwitches.values()) {
			if(!cla.optional && !pns.contains(cla.propertyName))
				throw new IOException("Required option '" + (cla.shortName == null ? cla.longName : cla.shortName) + "' was not used.");
		}
		for(CommandLineArgument cla : commandLineArgs) {
			if(!cla.optional && !pns.contains(cla.propertyName))
				throw new IOException("Required argument <" + cla.propertyName + "> was not used.");
		}
		return argMap;
	}

	@Override
	protected boolean allowExtraArguments(Map<String, Object> argMap) {
		return false;
	}

	/** Creates an object and sets its properties according to the key/value pairs in the properties argument. If a property's value
	 * is enclosed in double-quotes, this signifies a named reference. The configure method will create one instance of each named reference using
	 * the name as the property prefix and setting it on each object that references it. (i.e. - if a propery is globalObject="com.example.MyGlobalObject", then this method
	 * will configure an object using all "sub" properties of "com.example.MyGlobalObject" (thus com.example.MyGlobalObject.class would specify the concrete
	 * class of the object, and that object would then be used to set the globalObject property on the current object being configured.)
	 * @param <C>
	 * @param clazz The super class of the object to be returned
	 * @param propertyBase The property key that is used for configuring the object
	 * @param rootProperties The root properties. Used for named references
	 * @param namedReferences A map that holds named references. Must be modifiable. Use null to have method create its own map.
	 * @param initialize Whether to call "initialize" on objects or not
	 * @param constructorParams List of params for the constructor of the object
	 * @return The configured object
	 * @throws ServiceException
	 */
	public static <C> C configureFromBase(Class<C> clazz, String propertyBase, Configuration rootConfig, Map<String,Object> namedReferences, boolean initialize, Object... constructorParams) throws ServiceException {	
		String direct = rootConfig.getString(propertyBase);
		if(direct != null && direct.length() > 2 && direct.charAt(0) == '"' && direct.charAt(direct.length()-1) == '"') {
			String name = direct.substring(1, direct.length() - 1);
			Object value;
			if(namedReferences == null) {
				namedReferences = new HashMap<String, Object>();
				value = null;
			} else {
				value = namedReferences.get(name);
			}
			if(value == null) {
				value = configure(clazz, rootConfig.subset(name), namedReferences, initialize);
				namedReferences.put(name, value);
			} else if(!clazz.isInstance(value)) {
				throw new ServiceException("Property '" + propertyBase + "' references an object of class '" + value.getClass().getName() + "' that does not sub-class " + clazz.getName());
			}
			return clazz.cast(value);
		}
		return configure(clazz, rootConfig.subset(propertyBase), namedReferences, initialize, constructorParams);
	}

	/** Creates an object and sets its properties according to the key/value pairs in the properties argument. If a property's value
	 * is enclosed in double-quotes, this signifies a named reference. The configure method will create one instance of each named reference using
	 * the name as the property prefix and setting it on each object that references it. (i.e. - if a propery is globalObject="com.example.MyGlobalObject", then this method
	 * will configure an object using all "sub" properties of "com.example.MyGlobalObject" (thus com.example.MyGlobalObject.class would specify the concrete
	 * class of the object, and that object would then be used to set the globalObject property on the current object being configured.)
	 * @param <C>
	 * @param clazz The super class of the object to be returned
	 * @param properties The properties that apply to the object. The class property is used to determine which concrete class is used.
	 * @param rootProperties The root properties. Used for named references
	 * @param namedReferences A map that holds named references. Must be modifiable. Use null to have method create its own map.
	 * @param constructorParams List of params for the constructor of the object
	 * @return The configured object
	 * @throws ServiceException
	 */
	public static <C> C configure(Class<C> clazz, Configuration config) throws ServiceException {
		return configure(clazz, config, null, true);
	}

	/** Creates an object and sets its properties according to the key/value pairs in the properties argument. If a property's value
	 * is enclosed in double-quotes, this signifies a named reference. The configure method will create one instance of each named reference using
	 * the name as the property prefix and setting it on each object that references it. (i.e. - if a propery is globalObject="com.example.MyGlobalObject", then this method
	 * will configure an object using all "sub" properties of "com.example.MyGlobalObject" (thus com.example.MyGlobalObject.class would specify the concrete
	 * class of the object, and that object would then be used to set the globalObject property on the current object being configured.)
	 * @param <C>
	 * @param clazz The super class of the object to be returned
	 * @param properties The properties that apply to the object. The class property is used to determine which concrete class is used.
	 * @param rootProperties The root properties. Used for named references
	 * @param namedReferences A map that holds named references. Must be modifiable. Use null to have method create its own map.
	 * @param constructorParams List of params for the constructor of the object
	 * @return The configured object
	 * @throws ServiceException
	 */
	public static <C> C configure(Class<C> clazz, Configuration config, Map<String,Object> namedReferences, boolean initialize, Object... constructorParams) throws ServiceException {
		return configure(clazz, config, namedReferences, null, initialize, null, constructorParams);
	}

	public static <C> C configure(Class<C> clazz, Configuration config, Map<String,Object> namedReferences, boolean initialize, Class<? extends C> defaultClazz, Object... constructorParams) throws ServiceException {
		return configure(clazz, config, namedReferences, null, initialize, defaultClazz, constructorParams);
	}
	
	protected static class ConstructorExecutor<C> implements Comparable<ConstructorExecutor<?>>{
		protected final int extraParamRank;
		protected int baseParamLengthRank;
		protected int baseParamMatchRank;
		protected final Constructor<?> constructor;
		protected final Class<? extends C> objectClass;
		protected final Object[] constructorParams;
		protected final boolean configure;
		protected final Configuration config;
		protected final Map<String,Object> namedReferences;
		
		public ConstructorExecutor(Class<? extends C> objectClass, Constructor<?> constructor, Configuration config, Map<String,Object> namedReferences, Object[] baseParams) {
			this.objectClass = objectClass;
			this.constructor = constructor;
			this.config = config;
			this.namedReferences = namedReferences;
			Class<?>[] paramTypes = constructor.getParameterTypes();
			this.constructorParams = new Object[paramTypes.length];
			int baseParamLength;
			if(paramTypes.length > 1 && Configuration.class.isAssignableFrom(paramTypes[paramTypes.length - 2]) && Map.class.isAssignableFrom(paramTypes[paramTypes.length - 1])) {
				baseParamLength = paramTypes.length - 2;
				extraParamRank = 5;
				constructorParams[baseParamLength] = config;
				constructorParams[baseParamLength+1] = namedReferences;				
				configure = false;
			} else if(paramTypes.length > 1 && Properties.class.isAssignableFrom(paramTypes[paramTypes.length - 2]) && Map.class.isAssignableFrom(paramTypes[paramTypes.length - 1])) {
				baseParamLength = paramTypes.length - 2;
				extraParamRank = 4;
				constructorParams[baseParamLength] = toProperties(config);
				constructorParams[baseParamLength+1] = namedReferences;				
				configure = false;
			} else if(paramTypes.length > 0 && Configuration.class.isAssignableFrom(paramTypes[paramTypes.length - 1])) {
				baseParamLength = paramTypes.length - 1;
				extraParamRank = 3;
				constructorParams[baseParamLength] = config;
				configure = false;
			} else if(paramTypes.length > 0 && Properties.class.isAssignableFrom(paramTypes[paramTypes.length - 1])) {
				baseParamLength = paramTypes.length - 1;
				extraParamRank = 2;
				constructorParams[baseParamLength] = toProperties(config);
				configure = false;
			} else {
				baseParamLength = paramTypes.length;
				extraParamRank = 0;
				configure = true;
			}
			int origBaseParamLength = (baseParams == null ? 0 : baseParams.length);
			if(baseParamLength < origBaseParamLength) { // check for collection/array in constructor param types
				//find the last collection type parameter where baseParam is not a collection type
				baseParamLengthRank = 3;				
				baseParamMatchRank = 0;
				boolean collectionFound = false;
				for(int k = 1; k <= baseParamLength; k++) {
					if(ConvertUtils.isCollectionType(paramTypes[baseParamLength - k]) && (baseParams[origBaseParamLength - k] == null || !ConvertUtils.isCollectionType(baseParams[origBaseParamLength - k].getClass()))) {
						/*
						int length =  1 + origBaseParamLength - baseParamLength;
						Object collection = CollectionUtils.createArray(componentType, length);
						*/
						Object[] collection = new Object[1 + origBaseParamLength - baseParamLength];
						System.arraycopy(baseParams, baseParamLength - k, collection, 0, collection.length);
						try {
							constructorParams[baseParamLength - k] = ConvertUtils.convert(paramTypes[baseParamLength - k], collection);
							if(paramTypes[baseParamLength - k].isArray()) {
								Class<?> componentType = paramTypes[baseParamLength - k].getComponentType();
								for(int i = 0; i < collection.length; i++) {
									if(collection[i] != null) {
										if(ConvertUtils.convertToWrapperClass(componentType).isInstance(collection[i])) {
											// perfect match
											baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
										} else if(baseParams[i].getClass().isArray() && Array.getLength(baseParams[i]) == 1 && ConvertUtils.convertToWrapperClass(paramTypes[i]).isInstance(Array.get(baseParams[i], 0))) {
											// perfect match
											baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
										} else {
											baseParamMatchRank += 1;
										}	
									} else if(componentType.isPrimitive()) {
										// could not convert
										baseParamMatchRank += Integer.MIN_VALUE;
									} else {
										// null match
										baseParamMatchRank += baseParamLength + 1;
									}
								}
							}
						} catch(ConvertException e) {
							// could not convert
							constructorParams[baseParamLength - k] = collection;
							baseParamMatchRank += Integer.MIN_VALUE;
						}
						for(int i = 0; i < baseParamLength - k; i++) {
							if(baseParams[i] != null) {
								Object tmp;
								if(ConvertUtils.convertToWrapperClass(paramTypes[i]).isInstance(baseParams[i])) {
									// perfect match
									constructorParams[i] = baseParams[i];
									baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
								} else if(baseParams[i].getClass().isArray() && Array.getLength(baseParams[i]) == 1 && ConvertUtils.convertToWrapperClass(paramTypes[i]).isInstance((tmp=Array.get(baseParams[i], 0)))) {
									// perfect match
									constructorParams[i] = tmp;
									baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
								} else {
									try {
										constructorParams[i] = ConvertUtils.convert(paramTypes[i], baseParams[i]);
										baseParamMatchRank += 1;
										// convertible match
									} catch(ConvertException e) {
										// could not convert
										constructorParams[i] = baseParams[i];
										baseParamMatchRank += Integer.MIN_VALUE;
									}
								}	
							} else if(paramTypes[i].isPrimitive()) {
								// could not convert
								constructorParams[i] = baseParams[i];
								baseParamMatchRank += Integer.MIN_VALUE;
							} else {
								// null match
								constructorParams[i] = baseParams[i];
								baseParamMatchRank += baseParamLength + 1;
							}
						}
						collectionFound = true;
						break;
					} else {
						if(baseParams[origBaseParamLength - k] != null) {
							Object tmp;
							if(ConvertUtils.convertToWrapperClass(paramTypes[baseParamLength - k]).isInstance(baseParams[origBaseParamLength - k])) {
								// perfect match
								constructorParams[baseParamLength - k] = baseParams[origBaseParamLength - k];
								baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
							} else if(baseParams[origBaseParamLength - k].getClass().isArray() && Array.getLength(baseParams[origBaseParamLength - k]) == 1 && ConvertUtils.convertToWrapperClass(paramTypes[baseParamLength - k]).isInstance((tmp=Array.get(baseParams[origBaseParamLength - k], 0)))) {
								// perfect match
								constructorParams[baseParamLength - k] = tmp;
								baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
							} else {
								try {
									constructorParams[baseParamLength - k] = ConvertUtils.convert(paramTypes[baseParamLength - k], baseParams[origBaseParamLength - k]);
									baseParamMatchRank += 1;
									// convertible match
								} catch(ConvertException e) {
									// could not convert
									constructorParams[baseParamLength - k] = baseParams[origBaseParamLength - k];
									baseParamMatchRank += Integer.MIN_VALUE;
								}
							}	
						} else if(paramTypes[baseParamLength - k].isPrimitive()) {
							// could not convert
							constructorParams[baseParamLength - k] = baseParams[origBaseParamLength - k];
							baseParamMatchRank += Integer.MIN_VALUE;
						} else {
							// null match
							constructorParams[baseParamLength - k] = baseParams[origBaseParamLength - k];
							baseParamMatchRank += baseParamLength + 1;
						}
					}
				}
				if(!collectionFound)
					baseParamLengthRank = Integer.MIN_VALUE;
			} else if(baseParamLength > origBaseParamLength) { // use null for rest of params ?
				baseParamLengthRank = origBaseParamLength - baseParamLength;
				baseParamMatchRank = 0;
				for(int i = 0; i < baseParamLength; i++) {
					if(i < origBaseParamLength && baseParams[i] != null) {
						Object tmp;
						if(ConvertUtils.convertToWrapperClass(paramTypes[i]).isInstance(baseParams[i])) {
							// perfect match
							constructorParams[i] = baseParams[i];
							baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
						} else if(baseParams[i].getClass().isArray() && Array.getLength(baseParams[i]) == 1 && ConvertUtils.convertToWrapperClass(paramTypes[i]).isInstance((tmp=Array.get(baseParams[i], 0)))) {
							// perfect match
							constructorParams[i] = tmp;
							baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
						} else {
							try {
								constructorParams[i] = ConvertUtils.convert(paramTypes[i], baseParams[i]);
								baseParamMatchRank += 1;
								// convertible match
							} catch(ConvertException e) {
								// could not convert
								constructorParams[i] = baseParams[i];
								baseParamMatchRank += Integer.MIN_VALUE;
							}
						}	
					} else if(paramTypes[i].isPrimitive()) {
						// could not convert
						constructorParams[i] = null;
						baseParamMatchRank += Integer.MIN_VALUE;
					} else {
						// null match
						constructorParams[i] = null;
						baseParamMatchRank += baseParamLength + 1;
					}
				}
			} else { // baseParamLength == origBaseParamLength
				baseParamLengthRank = 5;
				baseParamMatchRank = 0;
				for(int i = 0; i < baseParamLength; i++) {
					if(baseParams[i] != null) {
						Object tmp;
						if(ConvertUtils.convertToWrapperClass(paramTypes[i]).isInstance(baseParams[i])) {
							// perfect match
							constructorParams[i] = baseParams[i];
							baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
						} else if(baseParams[i].getClass().isArray() && Array.getLength(baseParams[i]) == 1 && ConvertUtils.convertToWrapperClass(paramTypes[i]).isInstance((tmp=Array.get(baseParams[i], 0)))) {
							// perfect match
							constructorParams[i] = tmp;
							baseParamMatchRank += baseParamLength * (baseParamLength + 1) + 1;
						} else {
							try {
								constructorParams[i] = ConvertUtils.convert(paramTypes[i], baseParams[i]);
								baseParamMatchRank += 1;
								// convertible match
							} catch(ConvertException e) {
								// could not convert
								constructorParams[i] = baseParams[i];
								baseParamMatchRank += Integer.MIN_VALUE;
							}
						}	
					} else if(paramTypes[i].isPrimitive()) {
						// could not convert
						constructorParams[i] = baseParams[i];
						baseParamMatchRank += Integer.MIN_VALUE;
					} else {
						// null match
						constructorParams[i] = baseParams[i];
						baseParamMatchRank += baseParamLength + 1;
					}
				}
			}
		}
		public int compareTo(ConstructorExecutor<?> o) {
			if(extraParamRank > o.extraParamRank)
				return -1;
			if(extraParamRank < o.extraParamRank)
				return 1;
			if(baseParamLengthRank > o.baseParamLengthRank)
				return -1;
			if(baseParamLengthRank < o.baseParamLengthRank)
				return 1;
			if(baseParamMatchRank > o.baseParamMatchRank)
				return -1;
			if(baseParamMatchRank < o.baseParamMatchRank)
				return 1;
			return constructor.toString().compareTo(o.toString());
		}
		
		public C createAndConfigureObject(Set<Object> initializeObjects, String referenceName) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, SecurityException, IntrospectionException, ParseException, ConvertException, ServiceException  {
			C object = objectClass.cast(constructor.newInstance(constructorParams));
			if(log.isInfoEnabled()) {
				StringBuilder sb = new StringBuilder();
				sb.append("Created ");
				if(referenceName != null)
					sb.append('\'').append(referenceName).append('\'');
				else if(config instanceof SubsetConfiguration)
					sb.append('\'').append(((SubsetConfiguration) config).getPrefix()).append('\'');
				else
					sb.append("anonymous object");
				sb.append("using constructor ").append(constructor);
				log.info(sb.toString());
			}
			if(namedReferences != null && referenceName != null)
				namedReferences.put(referenceName, object);
			if(configure) {
				configureProperties(object, config, namedReferences, initializeObjects);
			} 
			if(object instanceof ConfigurationTracker)
				((ConfigurationTracker) object).setConfiguration(config);
			return object;
		}
		@Override
		public String toString() {
			return constructor.toString() + " [" + extraParamRank + ", " + baseParamLengthRank + ", " + baseParamMatchRank + "]";
		}
	}
	
	public static Properties toProperties(Configuration config) {
		Properties properties = new Properties();
		intoMap(config, properties);
		return properties;
	}

	public static void intoMap(Configuration config, Map<? super String, Object> map) {
		AbstractConfiguration interpolater = getInterpolater(config);
		for(Iterator<String> iter = config.getKeys(); iter.hasNext();) {
			String key = iter.next();
			Object value = config.getProperty(key);
			if(interpolater != null)
				value = interpolateConfigValue(value, interpolater);
			map.put(key, value);
		}
	}

	protected static AbstractConfiguration getInterpolater(Configuration config) {
		if(config instanceof SubsetConfiguration) {
			AbstractConfiguration tmp = getInterpolater(((SubsetConfiguration) config).getParent());
			if(tmp != null)
				return tmp;
		} else if(config instanceof CompositeConfiguration) {
			CompositeConfiguration tmp = new CompositeConfiguration();
			CompositeConfiguration cc = (CompositeConfiguration) config;
			for(int i = 0; i < cc.getNumberOfConfigurations(); i++)
				tmp.addConfiguration(getInterpolater(cc.getConfiguration(i)));
			return tmp;
		}
		if(config instanceof AbstractConfiguration)
			return (AbstractConfiguration) config;
		return null;
	}

	protected static Object interpolateConfigValue(Object value, AbstractConfiguration config) {
		if(value instanceof String)
			return PropertyConverter.interpolate(value, config);
		else if(value instanceof List) {
			List<Object> list = new ArrayList<Object>();
			for(Object o : (List<?>) value) {
				list.add(interpolateConfigValue(o, config));
			}
			return list;
		}
		return value;
	}

	/** Creates an object and sets its properties according to the key/value pairs in the properties argument. If a property's value
	 * is enclosed in double-quotes, this signifies a named reference. The configure method will create one instance of each named reference using
	 * the name as the property prefix and setting it on each object that references it. (i.e. - if a propery is globalObject="com.example.MyGlobalObject", then this method
	 * will configure an object using all "sub" properties of "com.example.MyGlobalObject" (thus com.example.MyGlobalObject.class would specify the concrete
	 * class of the object, and that object would then be used to set the globalObject property on the current object being configured.)
	 * @param <C>
	 * @param clazz The super class of the object to be returned
	 * @param properties The properties that apply to the object. The class property is used to determine which concrete class is used.
	 * @param rootProperties The root properties. Used for named references
	 * @param namedReferences A map that holds named references. Must be modifiable. Use null to have method create its own map.
	 * @param constructorParams List of params for the constructor of the object
	 * @return The configured object
	 * @throws ServiceException
	 */
	public static <C> C configure(Class<C> clazz, final Configuration config, Map<String, Object> namedReferences, String referenceName, boolean initialize, Class<? extends C> defaultClazz, Object... constructorParams) throws ServiceException {
		Set<Object> initializeObjects = (initialize ? new HashSet<Object>() : null);
		final C object = configure(clazz, config, namedReferences, referenceName, initializeObjects, defaultClazz, constructorParams);
		if(initializeObjects != null) {
			for(Object o : initializeObjects)
				try {
					ReflectionUtils.invokeMethod(o, "initialize");
				} catch(NoSuchMethodException e) {
					//ignore
				} catch(SecurityException e) {
					throw new ServiceException("Could not initialize " + o, e);
				} catch(IllegalArgumentException e) {
					throw new ServiceException("Could not initialize " + o, e);
				} catch(IllegalAccessException e) {
					throw new ServiceException("Could not initialize " + o, e);
				} catch(InvocationTargetException e) {
					throw new ServiceException("Could not initialize " + o, e);
				}
		}/*
		if(object instanceof ConfigurationUpdater) {
			((ConfigurationUpdater) object).addConfigurationListener(new ConfigurationListener() {
				@Override
				public void configurationChanged(ConfigurationEvent event) {
					if(event.getType() == AbstractConfiguration.EVENT_SET_PROPERTY) {
						Configuration sourceConfig;
						if(config instanceof CompositeConfiguration) {
							sourceConfig = ((CompositeConfiguration) config).getSource(event.getPropertyName());
							if(sourceConfig == null)
								return;
						} else
							sourceConfig = config;
						// check for no interpolation
						Object oldValue = sourceConfig.getProperty(event.getPropertyName());
						if(oldValue instanceof String && !((String) oldValue).equals(sourceConfig.getString(event.getPropertyName())))
							return;

						sourceConfig.setProperty(event.getPropertyName(), event.getPropertyValue());
					}
				}
			});
		}*/
		return object;
	}
	public static <C> C configure(Class<C> clazz, Configuration config, Map<String,Object> namedReferences, String referenceName, Set<Object> initializeObjects, Class<? extends C> defaultClazz, Object... constructorParams) throws ServiceException {
		String entry;
		if(config instanceof SubsetConfiguration)
			entry = ((SubsetConfiguration)config).getPrefix();
		else
			entry = "<UNKNOWN>";
		String className = config.getString("class");
		Class<? extends C> objectClass;
		if(className != null && (className=className.trim()).length() > 0) {
			try {
				objectClass = Class.forName(className).asSubclass(clazz);
			} catch(ClassNotFoundException e) {
				throw new ServiceException("Class '" + className + "' could not be found for entry '" + entry + "'", e);
			} catch(ClassCastException e) {
				throw new ServiceException("Class '" + className + "' is not a sub class of '" + clazz.getName() + "' for entry '" + entry + '\'', e);
			}
		} else if(defaultClazz != null) {
			if(defaultClazz.isInterface() || Modifier.isAbstract(defaultClazz.getModifiers()))
				throw new ServiceException("Default Class " + defaultClazz.getName() + " is not instantiable for entry '" + entry + '\'');
			objectClass = defaultClazz;
		} else if(clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers())) {
			return null; //could not be created
		} else {
			objectClass = clazz;
		}
		SortedSet<ConstructorExecutor<C>> constructors = new TreeSet<ConstructorExecutor<C>>();
		for(Constructor<?> constructor : objectClass.getConstructors()) {
			constructors.add(new ConstructorExecutor<C>(objectClass, constructor, config, namedReferences, constructorParams));
		}
		ServiceException firstException = null;
		for(ConstructorExecutor<C> ce : constructors) {
			try {
				return ce.createAndConfigureObject(initializeObjects, referenceName);
			} catch(ServiceException e) {
				if(firstException == null)
					firstException = e;
			} catch(IllegalArgumentException e) {
				if(firstException == null)
					firstException = new ServiceException(e.getMessage() + " for entry '" + entry + '\'', e);
			} catch(SecurityException e) {
				if(firstException == null) {
					StringBuilder sb = new StringBuilder();
					sb.append("Class '").append(objectClass.getSimpleName()).append("' could not be instantiated with parameters [");
					for(Object pt : constructorParams)
						sb.append(pt == null ? "*" : constructorParams.getClass().getName()).append(',');
					if(constructorParams.length > 0)
						sb.setLength(sb.length() - 1);
					sb.append("] for entry '").append(entry).append('\'');
					firstException = new ServiceException(sb.toString(), e);
				}
			} catch(InstantiationException e) {
				if(firstException == null) {
					StringBuilder sb = new StringBuilder();
					sb.append("Class '").append(objectClass.getSimpleName()).append("' could not be instantiated with parameters [");
					for(Object pt : constructorParams)
						sb.append(pt == null ? "*" : constructorParams.getClass().getName()).append(',');
					if(constructorParams.length > 0)
						sb.setLength(sb.length() - 1);
					sb.append("] for entry '").append(entry).append('\'');
					firstException = new ServiceException(sb.toString(), e);
				}
			} catch(IllegalAccessException e) {
				if(firstException == null) {
					StringBuilder sb = new StringBuilder();
					sb.append("Class '").append(objectClass.getSimpleName()).append("' could not be instantiated with parameters [");
					for(Object pt : constructorParams)
						sb.append(pt == null ? "*" : constructorParams.getClass().getName()).append(',');
					if(constructorParams.length > 0)
						sb.setLength(sb.length() - 1);
					sb.append("] for entry '").append(entry).append('\'');
					firstException = new ServiceException(sb.toString(), e);
				}
			} catch(IntrospectionException e) {
				if(firstException == null)
					firstException = new ServiceException("Class '" + objectClass.getSimpleName() + "' could not be configured for entry '" + entry + '\'', e);
			} catch(InvocationTargetException e) {
				if(firstException == null)
					firstException = new ServiceException("Class '" + objectClass.getSimpleName() + "' could not be configured for entry '" + entry + '\'', e);
			} catch(ConvertException e) {
				if(firstException == null)
					firstException = new ServiceException("Class '" + objectClass.getSimpleName() + "' could not be configured for entry '" + entry + '\'', e);
			} catch(ParseException e) {
				if(firstException == null)
					firstException = new ServiceException("Class '" + objectClass.getSimpleName() + "' could not be configured for entry '" + entry + '\'', e);
			}
		}
		if(firstException == null) {
			StringBuilder sb = new StringBuilder();
			sb.append("Class '").append(objectClass.getSimpleName()).append("' could not be instantiated with parameters [");
	        for(Object pt : constructorParams)
	        	sb.append(pt == null ? "*" : constructorParams.getClass().getName()).append(',');
	        if(constructorParams.length > 0)
	        	sb.setLength(sb.length()-1);
	        sb.append("] because no appropriate constructor could be found for entry '").append(entry).append('\'');
	        firstException = new ServiceException(sb.toString());
		}
		throw firstException;		
	}
	public static void configureProperties(Object object, Configuration config, Map<String,Object> namedReferences, boolean initialize) throws IntrospectionException, ParseException, InstantiationException, ConvertException, ServiceException, SecurityException, IllegalArgumentException {
		Set<Object> initializeObjects = (initialize ? new HashSet<Object>() : null);
		configureProperties(object, config, namedReferences, initializeObjects);
		if(initializeObjects != null) {
			for(Object o : initializeObjects)
				try {
					ReflectionUtils.invokeMethod(o, "initialize");
				} catch(NoSuchMethodException e) {
					//ignore
				} catch(SecurityException e) {
					throw new ServiceException("Could not initialize " + o, e);
				} catch(IllegalArgumentException e) {
					throw new ServiceException("Could not initialize " + o, e);
				} catch(IllegalAccessException e) {
					throw new ServiceException("Could not initialize " + o, e);
				} catch(InvocationTargetException e) {
					throw new ServiceException("Could not initialize " + o, e);
				}
		}
	}
	public static void configureProperties(Object object, Configuration config, Map<String,Object> namedReferences, Set<Object> initializeObjects) throws IntrospectionException, ParseException, InstantiationException, ConvertException, ServiceException, SecurityException, IllegalArgumentException {
		if(log.isInfoEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Configuring ");
			if(config instanceof SubsetConfiguration)
				sb.append('\'').append(((SubsetConfiguration) config).getPrefix()).append('\'');
			else if(object != null)
				sb.append("object of ").append(object.getClass().getName());
			sb.append(" with properties [");
			AbstractConfiguration interpolater = getInterpolater(config);
			boolean first = true;
			for(Iterator<String> iter = config.getKeys(); iter.hasNext();) {
				String key = iter.next();
				Object value = config.getProperty(key);
				if(interpolater != null)
					value = interpolateConfigValue(value, interpolater);
				if(first)
					first = false;
				else
					sb.append("; ");
				sb.append(key).append('=').append(value);
			}
			sb.append(']');
			log.info(sb.toString());
		}

		if(namedReferences == null)
			namedReferences = new HashMap<String, Object>();
		Set<String> subKeys = new HashSet<String>();
		for(Iterator<?> iter = config.getKeys(); iter.hasNext(); ) {
			String key = (String)iter.next();
			if(key.equals("class"))
				continue;
			NestedNameTokenizer tok = new NestedNameTokenizer(key);
			String prop = tok.nextProperty();
			if(subKeys.add(tok.getConsumed())) { // use tok.getConsumed() instead of prop to allow indexed and mapped to do all indices/keys
				BeanProperty bp = ReflectionUtils.findBeanPropertyFor(object, prop, true);
				if(bp != null) {
					try {
						Object value = null;
						Configuration subConfig = config.subset(tok.getConsumed());
						String base;
						if(tok.hasNext()) {
							base = config.getString(tok.getConsumed());
						} else {
							base = config.getString(key);
						}
						boolean readFirst = StringUtils.isBlank(subConfig.getString("class")) && base == null; // NOTE: do not trim base - we may want the spaces!
						if(tok.isIndexed()) {
							IndexedBeanProperty ibp = (bp instanceof IndexedBeanProperty ? (IndexedBeanProperty) bp : new ReflectionUtils.WrappedIndexedBeanProperty(bp, object.getClass()));
							if(readFirst && ibp.isIndexedReadable()) {
								value = ibp.getIndexedValue(object, tok.getIndex());
								if(value != null) {
									configureProperties(value, subConfig, namedReferences, initializeObjects);
									continue;
								}
							}
							if(ibp.isIndexedWritable()) {
								value = constructValue(base, ibp.getIndexedType(), subConfig, namedReferences, initializeObjects);
								if(value != null) {
									ibp.setIndexedValue(object, tok.getIndex(), value);
									continue;
								}
							}
							if(!readFirst && ibp.isIndexedReadable()) {
								value = ibp.getIndexedValue(object, tok.getIndex());
								if(value != null)
									configureProperties(value, subConfig, namedReferences, initializeObjects);
							}
						} else if(tok.isMapped()) {
							MappedBeanProperty mbp = (bp instanceof MappedBeanProperty ? (MappedBeanProperty) bp : new ReflectionUtils.WrappedMappedBeanProperty(bp, object.getClass()));
							if(readFirst && mbp.isMappedReadable()) {
								value = mbp.getMappedValue(object, tok.getKey());
								if(value != null) {
									configureProperties(value, subConfig, namedReferences, initializeObjects);
									continue;
								}
							}
							if(mbp.isMappedWritable()) {
								value = constructValue(base, mbp.getMappedType(), subConfig, namedReferences, initializeObjects);
								if(value != null) {
									mbp.setMappedValue(object, tok.getKey(), value);
									continue;
								}
							}
							if(!readFirst && mbp.isMappedReadable()) {
								value = mbp.getMappedValue(object, tok.getKey());
								if(value != null)
									configureProperties(value, subConfig, namedReferences, initializeObjects);
							}
						} else {
							if(readFirst && bp.isReadable()) {
								value = bp.getValue(object);
								if(value != null) {
									configureProperties(value, subConfig, namedReferences, initializeObjects);
									continue;
								}
							}
							if(bp.isWritable()) {
								Class<?> componentType;
								if(bp instanceof IndexedBeanProperty)
									componentType = ((IndexedBeanProperty) bp).getIndexedType();
								else if(bp instanceof MappedBeanProperty)
									componentType = ((MappedBeanProperty) bp).getMappedType();
								else
									componentType = null;
								value = constructValue(base, bp.getType(), componentType, subConfig, namedReferences, initializeObjects);
								if(value != null) {
									bp.setValue(object, value);
									continue;
								}
							}
							if(!readFirst && bp.isReadable()) {
								value = bp.getValue(object);
								if(value != null)
									configureProperties(value, subConfig, namedReferences, initializeObjects);
							}
						}
					} catch(IllegalArgumentException e) {
						throw new ServiceException("Exception while setting property '" + prop + "' on " + object, e);
					} catch(IllegalAccessException e) {
						throw new ServiceException("Exception while setting property '" + prop + "' on " + object, e);
					} catch(InvocationTargetException e) {
						throw new ServiceException("Exception while setting property '" + prop + "' on " + object, e);
					}
				}
			}
        }
		if(object != null && initializeObjects != null && ReflectionUtils.hasMethod(object.getClass(), "initialize") && ConvertUtils.getBoolean(config.getProperty("{initialize}"), true)) {
			initializeObjects.add(object);
		}
	}

	protected static Object constructValue(String base, Class<?> type, Configuration config, Map<String,Object> namedReferences, Set<Object> initializeObjects) throws ServiceException {
		return constructValue(base, type, null, config, namedReferences, initializeObjects);
	}

	protected static Object constructValue(String base, Class<?> type, Class<?> componentType, Configuration config, Map<String, Object> namedReferences, Set<Object> initializeObjects) throws ServiceException {
		Object value = null;
		if(base != null) {
			if(base.length() > 2 && base.charAt(0) == '"' && base.charAt(base.length()-1) == '"') {
				//prefer a reference
				try {
					value = constructReference(base, type, config, namedReferences, initializeObjects);
				} catch(ServiceException e) {
					try {
						value = ConvertUtils.convert(type, componentType, base);
					} catch(ConvertException e1) {
						throw e;
					}
				}
			} else {
				try {
					value = ConvertUtils.convert(type, componentType, base);
				} catch(ConvertException e) {
					if(base.length() > 2 && base.charAt(0) == '"' && base.charAt(base.length()-1) == '"') {
						value = constructReference(base, type, config, namedReferences, initializeObjects);
					}
				}
			}
		}
		if(value == null) {
			if(base != null) {
				Object[] constructorParams;
				try {
					constructorParams = ConvertUtils.convert(Object[].class, base);
				} catch(ConvertException e) {
					constructorParams = new Object[] {base};
				}
				if(constructorParams != null)
					for(int i = 0; i < constructorParams.length; i++) {
						if(constructorParams[i] instanceof String) {
							String cp = (String) constructorParams[i];
							if(cp != null && (cp = cp.trim()).length() > 2 && cp.charAt(0) == '"' && cp.charAt(cp.length() - 1) == '"') {
								constructorParams[i] = constructReference(cp, Object[].class, config, namedReferences, initializeObjects);
							}
						}
					}
				value = configure(type, config, namedReferences, null, initializeObjects, null, constructorParams);
			} else
				value = configure(type, config, namedReferences, null, initializeObjects, null);
		}/* The following could lead to confusion (since a named reference's properties could be changed by one of the referring objects) so don't do it
		else if(!config.isEmpty())
			configureProperties(value, config, namedReferences, initialize);
			*/
		return value;
	}
	
	protected static Object constructReference(String base, Class<?> type, Configuration config, Map<String,Object> namedReferences, Set<Object> initializeObjects) throws ServiceException {
		String[] names = RegexUtils.split("\\\"\\s*,\\s*\\\"", base.substring(1, base.length() - 1), null);
		Object[] values = new Object[names.length];
		Class<?> componentType;
		switch(names.length) {
			case 0:
				return values;
			case 1:
				if(type.isArray())
					componentType = type.getComponentType();
				else if(ConvertUtils.isCollectionType(type))
					componentType = Object.class;
				else
					componentType = type;
				break;
			default:
				if(type.isArray())
					componentType = type.getComponentType();
				else if(ConvertUtils.isCollectionType(type))
					componentType = Object.class;
				else if(type.equals(Object.class)) //hope that an array will suffice
					componentType = Object.class;
				else
					throw new ServiceException("Array of values specified when type is not a collection type");
		}
		for(int i = 0; i < names.length; i++) {
			int p = names[i].indexOf(':');
			String refName;
			String refProp;
			Class<?> refType;
			if(p > 0) {
				refName = names[i].substring(0, p);
				refProp = names[i].substring(p+1);
				refType = Object.class; //We don't know what it should be
			} else {
				refName = names[i];
				refProp = null;
				refType = componentType;
			}
			Object refObject = namedReferences.get(refName);
			if(refObject == null) {
				Configuration parentConfig;
				if(config instanceof SubsetConfiguration)
					parentConfig = ((SubsetConfiguration)config).getParent();
				else
					parentConfig = config;
				Configuration refConfig = parentConfig.subset(refName);
				Object refBase = refConfig.getProperty("");
				if(refBase != null) {
					Object[] constructorParams;
					try {
						constructorParams = ConvertUtils.convert(Object[].class, refBase);
					} catch(ConvertException e) {
						constructorParams = new Object[] {refBase};
					}
					for(int j = 0; j < constructorParams.length; j++) {
						if(constructorParams[j] instanceof String) {
							String cp = (String)constructorParams[j];
							if(cp != null && (cp=cp.trim()).length() > 2 && cp.charAt(0) == '"' && cp.charAt(cp.length()-1) == '"') {
								constructorParams[j] = constructReference(cp, Object.class, refConfig, namedReferences, initializeObjects);
							}
						}
					}
					refObject = configure(refType, refConfig, namedReferences, refName, initializeObjects, null, constructorParams);
				} else
					refObject = configure(refType, refConfig, namedReferences, refName, initializeObjects, null);
			}
			if(refProp == null || (refProp=refProp.trim()).length() == 0) {
				values[i] = refObject;
			} else {
				try {
					values[i] = ReflectionUtils.getProperty(refObject, refProp);
				} catch(IntrospectionException e) {
					throw new ServiceException("Could not get property '" + refProp + "' from " + refObject, e);
				} catch(IllegalAccessException e) {
					throw new ServiceException("Could not get property '" + refProp + "' from " + refObject, e);
				} catch(InvocationTargetException e) {
					throw new ServiceException("Could not get property '" + refProp + "' from " + refObject, e);
				} catch(ParseException e) {
					throw new ServiceException("Could not get property '" + refProp + "' from " + refObject, e);
				}
			}	
		}
		try {
			return ConvertUtils.convert(type, (names.length == 1 ? values[0] : values));
		} catch(ConvertException e1) {
			throw new ServiceException("Could not convert property to necessary type", e1);
		}
	}
	
	public static void configureSystem(Configuration config, Map<String,Object> namedReferences) throws ServiceException {
		if(namedReferences == null)
			namedReferences = new HashMap<String, Object>();
		Configuration subset = config.subset(SYSTEM_CONFIG_SUBCONFIG);
		Set<Object> initializeObjects = new HashSet<Object>();
		for(Iterator<?> iter = subset.getKeys(); iter.hasNext(); ) {
			String property = (String)iter.next();
			Setter<Object> setter;
			try {
				setter = ReflectionUtils.getStaticPropertySetter(SYSTEM_CONFIG_PREFIX + property);
			} catch(ClassCastException e) {
				throw new ServiceException("Could not set static property '" + SYSTEM_CONFIG_PREFIX + property + "'", e);
			} catch(IllegalArgumentException e) {
				throw new ServiceException("Could not set static property '" + SYSTEM_CONFIG_PREFIX + property + "'", e);
			} catch(IntrospectionException e) {
				throw new ServiceException("Could not set static property '" + SYSTEM_CONFIG_PREFIX + property + "'", e);
			} catch(IllegalAccessException e) {
				throw new ServiceException("Could not set static property '" + SYSTEM_CONFIG_PREFIX + property + "'", e);
			} catch(InvocationTargetException e) {
				throw new ServiceException("Could not set static property '" + SYSTEM_CONFIG_PREFIX + property + "'", e);
			} catch(ParseException e) {
				throw new ServiceException("Could not set static property '" + SYSTEM_CONFIG_PREFIX + property + "'", e);
			} catch(NoSuchMethodException e) {
				throw new ServiceException("Could not set static property '" + SYSTEM_CONFIG_PREFIX + property + "'", e);
			}
			if(setter != null) {
				Object value = constructValue(subset.getString(property), setter.getType(), config, namedReferences, initializeObjects);
				setter.set(value);
			}
		}
		for(Object o : initializeObjects)
			try {
				ReflectionUtils.invokeMethod(o, "initialize");
			} catch(NoSuchMethodException e) {
				// ignore
			} catch(SecurityException e) {
				throw new ServiceException("Could not initialize " + o, e);
			} catch(IllegalArgumentException e) {
				throw new ServiceException("Could not initialize " + o, e);
			} catch(IllegalAccessException e) {
				throw new ServiceException("Could not initialize " + o, e);
			} catch(InvocationTargetException e) {
				throw new ServiceException("Could not initialize " + o, e);
			}

	}
	
	public static void configureStatics(Configuration config, Map<String, Object> namedReferences) throws ServiceException {
		if(namedReferences == null)
			namedReferences = new HashMap<String, Object>();
		Configuration subset = config.subset(STATIC_CONFIG_SUBCONFIG);
		Set<Object> initializeObjects = new HashSet<Object>();
		for(Iterator<?> iter = subset.getKeys(); iter.hasNext();) {
			String property = (String) iter.next();
			if(property.startsWith(SYSTEM_CONFIG_PREFIX))
				continue;
			Setter<Object> setter;
			try {
				setter = ReflectionUtils.getStaticPropertySetter(property);
			} catch(ClassCastException e) {
				throw new ServiceException("Could not set static property '" + property + "'", e);
			} catch(IllegalArgumentException e) {
				throw new ServiceException("Could not set static property '" + property + "'", e);
			} catch(IntrospectionException e) {
				throw new ServiceException("Could not set static property '" + property + "'", e);
			} catch(IllegalAccessException e) {
				throw new ServiceException("Could not set static property '" + property + "'", e);
			} catch(InvocationTargetException e) {
				throw new ServiceException("Could not set static property '" + property + "'", e);
			} catch(ParseException e) {
				throw new ServiceException("Could not set static property '" + property + "'", e);
			} catch(NoSuchMethodException e) {
				throw new ServiceException("Could not set static property '" + property + "'", e);
			}
			if(setter != null) {
				Object value = constructValue(subset.getString(property), setter.getType(), config, namedReferences, initializeObjects);
				if(value != null || !setter.getType().isPrimitive())
					setter.set(value);
			}
		}
		for(Object o : initializeObjects)
			try {
				ReflectionUtils.invokeMethod(o, "initialize");
			} catch(NoSuchMethodException e) {
				// ignore
			} catch(SecurityException e) {
				throw new ServiceException("Could not initialize " + o, e);
			} catch(IllegalArgumentException e) {
				throw new ServiceException("Could not initialize " + o, e);
			} catch(IllegalAccessException e) {
				throw new ServiceException("Could not initialize " + o, e);
			} catch(InvocationTargetException e) {
				throw new ServiceException("Could not initialize " + o, e);
			}

	}
	/** Reads in Data Layer Files to the default DataLayer. If the "simple.db.DataLayer.files" property is provided, its value is read as a comma- or semi-colon- separated
	 * list of data layer files which are read into the default {@link simple.db.DataLayer}. See the
	 * <a href="{@docRoot}/simple/resources/data-layer.xsd">Data Layer Schema Definition</a> for more information on the format of these files.
	 * @param config
	 * @throws ServiceException If an error occurs loading the data layer files
	 */
	public static void configureDataLayer(Configuration config) throws ServiceException {
		configureDataLayer(config, ConfigLoader.DEFUALT_MIN_INTERVAL);
	}

	/** Reads in Data Layer Files to the default DataLayer. If the "simple.db.DataLayer.files" property is provided, its value is read as a comma- or semi-colon- separated
	 * list of data layer files which are read into the default {@link simple.db.DataLayer}. See the
	 * <a href="{@docRoot}/simple/resources/data-layer.xsd">Data Layer Schema Definition</a> for more information on the format of these files.
	 * @param config
	 * @param minInterval the number of seconds to wait between checks to see if the files have been modified
	 * @throws ServiceException If an error occurs loading the data layer files
	 */
	public static void configureDataLayer(Configuration config, long minInterval) throws ServiceException {
		DialectResolver.initDialect(config.getString(DB_DIALECT_PROPERTY));
		String[] files = StringUtils.split(config.getString(DATA_LAYER_FILES_PROPERTY), StringUtils.STANDARD_DELIMS,
				false);
		if (files == null) {
			return;
		}

		for (String file : files) {
			file = file.trim();
			log.debug("Loading the file '" + file + "' into the DataLayer");
			try {
				file = buildDataLayerFilenameWithDialectSpecifics(file);
				ConfigLoader.loadConfig(file, minInterval);
			} catch (ConfigException e) {
				throw new ServiceException("Could not load data-layer file, '" + file + "'", e);
			} catch (IOException e) {
				throw new ServiceException("Could not load data-layer file, '" + file + "'", e);
			} catch (LoadingException e) {
				throw new ServiceException("Could not load data-layer file, '" + file + "'", e);
			}
		}
	}
	
	/** Creates and registers a {@link simle.db.DataSourceFactory}. The "simple.db.datasource.class" property in the provided Properties determines
	 * the implementation of DataSourceFactory that is used. If that property is not provided then {@link simple.db.LocalDataSourceFactory}
	 * is used. All other properties that start with "simple.db.datasource." are used to configure the DataSourceFactory. See {@link Main#configure}
	 * for details about how this is done. If no properties that start with "simple.db.datasource." are provided, a DataSourceFactory is not registered.
	 * But if properties are provided, the configured DataSourceFactory is passed to the
	 * {@link simple.db.DataLayerMgr#setDataSourceFactory(simple.db.DataSourceFactory)} method
	 * @param properties
	 * @param namedReferences
	 * @throws ServiceException If the DataSourceFactory could not be configured
	 */
	public static void configureDataSourceFactory(Configuration config, Map<String,Object> namedReferences) throws ServiceException {
		Configuration subConfig = config.subset(DATA_SOURCE_FACTORY_PROPERTIES_SUBCONFIG);
		if(subConfig.isEmpty()) return;
		DataSourceFactory dsf = configure(DataSourceFactory.class, subConfig, namedReferences, true, LocalDataSourceFactory.class);
		DataLayerMgr.setDataSourceFactory(dsf);
		int loginTimeout = subConfig.getInt("loginTimeout", -1);
		if(loginTimeout >= 0) {
			DriverManager.setLoginTimeout(loginTimeout);
		}
	}
}
