package simple.app;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.lang.text.StrSubstitutor;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;

public class ConfigUtils {

	public static String interpolate(String text, final Object properties) {
		return new StrSubstitutor(new StrLookup() {			
			@Override
			public String lookup(String key) {
				try {
					return ConvertUtils.getStringSafely(ReflectionUtils.getProperty(properties, key));
				} catch(IntrospectionException e) {
					return null;
				} catch(IllegalAccessException e) {
					return null;
				} catch(InvocationTargetException e) {
					return null;
				} catch(ParseException e) {
					return null;
				}
			}
		}).replace(text);
	}
}
