/*
 * Created on Jan 23, 2006
 *
 */
package simple.translator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import simple.app.ServiceException;
import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.text.StringUtils;

public class ConfigSourceTranslatorFactory extends AbstractTranslatorFactory {
    private static final Log log = Log.getLog();
    protected String baseName;

	protected class ConfigSourceTranslator extends AbstractTranslator {
		protected volatile Map<String, String> translations;
		protected final ConfigSource configSrc;

		public ConfigSourceTranslator(ConfigSource configSrc) {
			this.configSrc = configSrc;
			configSrc.registerLoader(new Loader() {
				public void load(ConfigSource src) throws LoadingException {
					loadTranslations(src);
				}
			}, getCheckInterval(), 0L);
			if(log.isDebugEnabled())
				log.debug("Found resource " + configSrc.getUri() + " for translations");
        }

        @Override
		protected String translate(String key) {
        	try {
				configSrc.reload();
			} catch(LoadingException e) {
				log.warn("Could not reload translations", e);
			}
			String x = translations.get(key);
			if(x == null) {
				log.warn("Could not find translation for '" + key + "' in " + configSrc.getUri());
            } else {
                if(log.isDebugEnabled())
					log.debug("Found translation '" + x + "' for '" + key + "' in " + configSrc.getUri());
            }
            return x;
        }

		protected void loadTranslations(ConfigSource src) throws LoadingException {
			Properties p = new Properties();
			try {
				p.load(src.getInputStream());
			} catch(IOException e) {
				throw new LoadingException(e);
			}
			Map<String, String> t = new HashMap<String, String>();
			for(Map.Entry<Object, Object> entry : p.entrySet())
				t.put((String) entry.getKey(), (String) entry.getValue());
			translations = t;
		}

		@Override
		public void refresh(long since) throws ServiceException {
			// this reloads automatically, so don't do anything
		}

		@Override
		protected long getCheckInterval() {
			return ConfigSourceTranslatorFactory.this.getCheckInterval();
		}
    }

    public ConfigSourceTranslatorFactory() {
        super();
    }

    @Override
    protected Translator createTranslator(ContextKey contextKey) {
    	String bn = getBaseName();
    	if(bn == null) {
    		log.warn("BaseName is not set on " + this + "; using DefaultTranslator");
    		return DefaultTranslatorFactory.getTranslatorInstance();
    	}
    	Locale locale = contextKey.getLocale();
    	String variant = locale.getVariant();
		StringBuilder sb = new StringBuilder(bn);
		String context;
    	if((context=contextKey.getContext()) != null && (context=context.trim()).length() > 0) {
    		context = context.replaceAll("\\W", "_");
    		if(variant == null || (variant=variant.trim()).length() == 0)
    			variant = context;
    		else
    			variant += '_' + context;
    	}
		if(!StringUtils.isBlank(variant)) {
			if(!StringUtils.isBlank(locale.getLanguage()))
				sb.append('_').append(locale.getLanguage());
			if(!StringUtils.isBlank(locale.getCountry()))
				sb.append('_').append(locale.getCountry());
			sb.append('_').append(variant).append(".properties");
			try {
				return new ConfigSourceTranslator(ConfigSource.createConfigSource(sb.toString()));
			} catch(IOException e) {
				sb.setLength(bn.length());
			}
		}
		if(!StringUtils.isBlank(locale.getCountry())) {
			if(!StringUtils.isBlank(locale.getLanguage()))
				sb.append('_').append(locale.getLanguage());
			sb.append('_').append(locale.getCountry()).append(".properties");
			try {
				return new ConfigSourceTranslator(ConfigSource.createConfigSource(sb.toString()));
			} catch(IOException e) {
				sb.setLength(bn.length());
			}
		}
		if(!StringUtils.isBlank(locale.getLanguage())) {
			sb.append('_').append(locale.getLanguage()).append(".properties");
			try {
				return new ConfigSourceTranslator(ConfigSource.createConfigSource(sb.toString()));
			} catch(IOException e) {
				sb.setLength(bn.length());
			}
		}
		sb.append(".properties");
		try {
			return new ConfigSourceTranslator(ConfigSource.createConfigSource(sb.toString()));
		} catch(IOException e) {
			log.warn("Could not find file " + sb.toString() + "; using DefaultTranslator");
			return DefaultTranslatorFactory.getTranslatorInstance();
		}
    }

	public String getBaseName() {
		return baseName;
	}

	public void setBaseName(String baseName) {
		this.baseName = baseName;
	}
}
