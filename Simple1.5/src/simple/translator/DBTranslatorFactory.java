/*
 * Created on Jan 23, 2006
 *
 */
package simple.translator;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.util.LRUMap;
import simple.util.StaticMap;

public class DBTranslatorFactory extends AbstractTranslatorFactory {
    private static final Log log = Log.getLog();
    protected String callId;
    protected int maxTranslations = 5000;

    protected class DBTranslator extends AbstractTranslator {
        protected Map<String,String> translations;
        protected ContextKey contextKey;
        public DBTranslator(ContextKey contextKey, Map<String,String> translations) {
            this.contextKey = contextKey;
            this.translations = translations;
        }

        @Override
		protected String translate(String key) {
        	String x = translations.get(key);
            if(x == null && !translations.containsKey(key)) {
            	x = getFromDB(key, contextKey);
                if(getMaxTranslations() != 0)
                    translations.put(key, x);
                if(x == null) {
                    log.warn("Could not find translation for '" + key + "' for locale '" + contextKey.getLocale() + "' and context '" + contextKey.getContext() + "'");
                } else {
                    if(log.isDebugEnabled())
                        log.debug("Found translation '" + x + "' for '" + key + "' where locale = '" + contextKey.getLocale() + "' and context = '" + contextKey.getContext() + "'");
                }
            }
            return x;
        }

		@Override
		public void refresh(long since) throws ServiceException {
			if(!translations.isEmpty())
				translations.clear();
		}

		@Override
		protected long getCheckInterval() {
			return DBTranslatorFactory.this.getCheckInterval();
		}
    }

    public DBTranslatorFactory() {
        super();
    }

    protected static final String[] callParamNames = new String[] {"locale", "context", "literal"};
    protected String getFromDB(String text, ContextKey contextKey) {
        Results results;
        try {
            results = DataLayerMgr.executeQuery(getCallId(), StaticMap.create(callParamNames, new Object[] {contextKey.getLocale().toString(), contextKey.getContext(), text}));
            if(results.next()) {
                if(!results.isGroupEnding(0)) throw new SQLException("Too many rows");
                return results.getFormattedValue(1);
            }
        } catch(SQLException e) {
            log.warn("While getting translation for '" + text + "' for locale '" + contextKey.getLocale() + "' and context '" + contextKey.getContext(), e);
        } catch(DataLayerException e) {
            log.warn("While getting translation for '" + text + "' for locale '" + contextKey.getLocale() + "' and context '" + contextKey.getContext(), e);
        }
        return null;
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public int getMaxTranslations() {
        return maxTranslations;
    }

    public void setMaxTranslations(int maxTranslations) {
        this.maxTranslations = maxTranslations;
        cache.clear();
    }

    @Override
    protected Translator createTranslator(ContextKey contextKey) {
        Map<String,String> translations;
        if(maxTranslations == 0) {
            translations = Collections.emptyMap();
        } else if(maxTranslations > 0) {
            translations = new LRUMap<String,String>(maxTranslations);
        } else {
            translations = new HashMap<String,String>();
        }
        return new DBTranslator(contextKey, translations);
    }
}
