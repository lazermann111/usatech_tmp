/*
 * Created on Jan 24, 2006
 *
 */
package simple.translator;

import java.text.Format;
import java.util.Locale;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;

public abstract class TranslatorFactory {
    protected static TranslatorFactory defaultFactory;
    
    protected TranslatorFactory() {
        super();
    }

    public static TranslatorFactory getDefaultFactory() {
        if(defaultFactory == null) defaultFactory = new DefaultTranslatorFactory();
        return defaultFactory;
    }

    public static void setDefaultFactory(TranslatorFactory defaultFactory) {
        TranslatorFactory.defaultFactory = defaultFactory;
    }
  
	public abstract Translator getTranslator(Locale locale, String context) throws ServiceException;
    
	public abstract void refreshTranslations() throws ServiceException;
    
    public static Format translateFormat(Format format, Translator translator) {
    	if(format == null || translator == null) return format;
		return ConvertUtils.getFormat(translator.translate(ConvertUtils.getFormatString(format), (Object[]) null));
    }
}
