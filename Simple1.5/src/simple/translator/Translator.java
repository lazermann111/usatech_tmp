/*
 * Created on Jan 24, 2006
 *
 */
package simple.translator;

import simple.util.Refresher;

public interface Translator extends Refresher {
    public String translate(String key, Object... params) ;
    public String translate(String key, String defaultText, Object... params) ;
}
