package simple.translator;

import java.sql.SQLException;
import java.util.regex.Pattern;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.util.StaticMap;

public class RecordMissingDBFullTranslatorFactory extends DBFullTranslatorFactory {
	private static final Log log = Log.getLog();
	protected Pattern filter = Pattern.compile(".+");
	protected String onMissingCallId = "SAVE_PLACEHOLDER_TRANSLATION";
	protected static final String[] callParamNames = new String[] {"key", "default"};
	
	@Override
	protected String handleNoTranslation(String key, String defaultText, MapTranslator translator) {
		if(filter.matcher(key).matches() && onMissingCallId != null) {
			//insert into database
			try {
				DataLayerMgr.executeUpdate(onMissingCallId, StaticMap.create(callParamNames, new Object[] {key, key.equalsIgnoreCase(defaultText) ? null : defaultText}), true);
				translator.setTranslation(key, defaultText == null ? "" : defaultText);
			} catch(SQLException e) {
				log.warn("Failed to save translation",e);
			} catch(DataLayerException e) {
				log.warn("Failed to save translation",e);
			}
		}
		return super.handleNoTranslation(key, defaultText, translator);
	}

	public String getFilter() {
		return filter.pattern();
	}

	public void setFilter(String filter) {
		this.filter = Pattern.compile(filter);
	}

	public String getOnMissingCallId() {
		return onMissingCallId;
	}

	public void setOnMissingCallId(String onMissingCallId) {
		this.onMissingCallId = onMissingCallId;
	}
}
