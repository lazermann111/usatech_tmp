/*
 * Created on Jan 23, 2006
 *
 */
package simple.translator;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;

public abstract class AbstractTranslatorFactory extends TranslatorFactory {
	protected Map<ContextKey, Translator> cache = new ConcurrentHashMap<ContextKey, Translator>(16, 0.75f, 1);
	protected long checkInterval = 10 * 60 * 1000L; // 10 minutes

    public static class ContextKey {
        protected final Locale locale;
        protected final String context;
        public ContextKey(Locale locale, String context) {
            this.locale = locale;
            this.context = context;
        }
        public boolean equals(Object o) {
            if(o instanceof ContextKey) {
                ContextKey ck = (ContextKey)o;
                return ConvertUtils.areEqual(locale, ck.locale) &&
                    ConvertUtils.areEqual(context, ck.context);
            }
            return false;
        }
        public int hashCode() {
			return SystemUtils.addHashCodes(0, locale, context);
        }
        public String getContext() {
            return context;
        }
        public Locale getLocale() {
            return locale;
        }
    }
        
    public AbstractTranslatorFactory() {
        super();
    }

    @Override
    public void refreshTranslations() throws ServiceException {
		for(Translator translator : cache.values())
        	translator.refresh();
    }

    @Override
	public Translator getTranslator(Locale locale, String context) throws ServiceException {
        ContextKey contextKey = new ContextKey(locale, context);
        Translator translator = cache.get(contextKey);
        if(translator == null) {
			translator = createTranslator(contextKey);
			cache.put(contextKey, translator);
        }
        return translator;
    }

	protected abstract Translator createTranslator(ContextKey contextKey) throws ServiceException;

	public long getCheckInterval() {
		return checkInterval;
	}

	public void setCheckInterval(long checkInterval) {
		this.checkInterval = checkInterval;
	}
}
