/*
 * Created on Jan 24, 2006
 *
 */
package simple.translator;

import java.util.Locale;

import simple.app.ServiceException;

public class DefaultTranslatorFactory extends TranslatorFactory {
    protected static Translator translator = new AbstractTranslator() {
        @Override
		protected String translate(String key) {
            return null;
        }

		@Override
		public void refresh(long since) throws ServiceException {
			// do nothing!
		}

		@Override
		protected long getCheckInterval() {
			return 0;
		}
    };
    public DefaultTranslatorFactory() {
        super();
    }

    @Override
    public Translator getTranslator(Locale locale, String context) {
        return translator;
    }

    public static Translator getTranslatorInstance() {
        return translator;
    }

    @Override
    public void refreshTranslations() {
        // do nothing!
    }
}
