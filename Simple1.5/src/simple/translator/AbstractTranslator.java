/**
 *
 */
package simple.translator;

import java.util.concurrent.atomic.AtomicLong;

import simple.app.ServiceException;
import simple.io.Log;
import simple.text.MessageFormat;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractTranslator implements Translator {
	private static final Log log = Log.getLog();
	protected final AtomicLong lastRefreshed = new AtomicLong(System.currentTimeMillis());
	protected abstract String translate(String key) ;

	/**
	 * @see simple.translator.Translator#translate(java.lang.String, java.lang.Object[])
	 */
	public String translate(String key, Object... params) {
		return translate(key, key, params);
	}

	/**
	 * @see simple.translator.Translator#translate(java.lang.String, java.lang.String, java.lang.Object[])
	 */
	public String translate(String key, String defaultText, Object... params) {
		String text;
		if(key == null)
			text = defaultText;
		else {
			checkUpToDate();
			text = translate(key);
			if(text == null)
				text = defaultText;
		}
		if(params != null && text != null && text.indexOf('{') >= 0) {
			try {
				text = new MessageFormat(text).format(params);
			} catch(RuntimeException e) {
				log.warn("Could not create Message Format from '" + text + "'", e);
			}
		}
		return text;
	}

	protected void checkUpToDate() {
		if(getCheckInterval() < 0)
			return; // refresh disabled
		long time = System.currentTimeMillis();
		long last = lastRefreshed.get();
		if(time > last + getCheckInterval() && lastRefreshed.compareAndSet(last, time)) {
			try {
				refresh(last);
			} catch(ServiceException e) {
				log.warn("Failed to refresh translations", e);
			}
		}
	}

	public abstract void refresh(long since) throws ServiceException;

	@Override
	public void refresh() throws ServiceException {
		refresh(0L);
	}

	protected abstract long getCheckInterval();
}
