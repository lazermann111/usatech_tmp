/*
 * Created on Jan 23, 2006
 *
 */
package simple.translator;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import simple.app.ServiceException;
import simple.io.Log;

public class ResourceBundleTranslatorFactory extends AbstractTranslatorFactory {
    private static final Log log = Log.getLog();
    protected String baseName;

    protected class ResourceBundleTranslator extends AbstractTranslator {
		protected final Locale locale;
		protected ResourceBundle rb;

		public ResourceBundleTranslator(Locale locale) throws ServiceException {
			this.locale = locale;
			refresh();
        }

        @Override
		protected String translate(String key) {
        	String x;
        	try {
        		x = rb.getString(key);
        	} catch(MissingResourceException e) {
        		x = null;
        	}
            if(x == null) {
                log.warn("Could not find translation for '" + key + "' in " + rb);
            } else {
                if(log.isDebugEnabled())
                    log.debug("Found translation '" + x + "' for '" + key + "' in " + rb);
            }
            return x;
        }

		@Override
		public void refresh(long since) throws ServiceException {
			rb = ResourceBundle.getBundle(getBaseName(), locale);
			if(log.isDebugEnabled())
				log.debug("Found resource bundle " + rb + " for translations of " + locale);
		}

		@Override
		protected long getCheckInterval() {
			return ResourceBundleTranslator.this.getCheckInterval();
		}
    }

    public ResourceBundleTranslatorFactory() {
        super();
    }

    @Override
	protected Translator createTranslator(ContextKey contextKey) throws ServiceException {
    	String bn = getBaseName();
    	if(bn == null) {
    		log.warn("BaseName is not set on " + this + "; using DefaultTranslator");
    		return DefaultTranslatorFactory.getTranslatorInstance();
    	}
    	Locale locale = contextKey.getLocale();
    	String context;
    	if((context=contextKey.getContext()) != null && (context=context.trim()).length() > 0) {
    		context = context.replaceAll("\\W", "_");
    		String variant = locale.getVariant();
    		if(variant == null || (variant=variant.trim()).length() == 0)
    			variant = context;
    		else
    			variant += '_' + context;
    		locale = new Locale(locale.getLanguage(), locale.getCountry(), variant);
    	}
		return new ResourceBundleTranslator(locale);
    }

	public String getBaseName() {
		return baseName;
	}

	public void setBaseName(String baseName) {
		this.baseName = baseName;
	}
}
