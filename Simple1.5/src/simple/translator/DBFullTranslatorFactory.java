/*
 * Created on Jan 23, 2006
 *
 */
package simple.translator;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;

/** This implementation retrieves ALL the translations for a given context & locale up front - so that
 *  retrieval is fast.
 *
 * @author bkrug
 *
 */
public class DBFullTranslatorFactory extends AbstractTranslatorFactory {
    private static final Log log = Log.getLog();
    protected String callId;

    protected class MapTranslator extends AbstractTranslator {
		protected final Map<String, String> translations = new ConcurrentHashMap<String, String>(256, 0.75f, 1);
		protected final ContextKey contextKey;

		public MapTranslator(ContextKey contextKey) throws ServiceException {
			this.contextKey = contextKey;
			refresh();
        }

        @Override
		protected String translate(String key) {
            String x = translations.get(key);
            return (x == null ? handleNoTranslation(key, null, this) : x);
        }

        public void setTranslation(String key, String value) {
        	translations.put(key, value);
        }

		@Override
		public void refresh(long since) throws ServiceException {
			updateTranslations(translations, contextKey, since);
		}

		@Override
		protected long getCheckInterval() {
			return DBFullTranslatorFactory.this.getCheckInterval();
		}
    }

    public DBFullTranslatorFactory() {
        super();
    }

    protected String handleNoTranslation(String key, String defaultText, MapTranslator translator) {
    	return defaultText;
    }

	protected void updateTranslations(Map<String, String> translations, ContextKey contextKey, long since) throws ServiceException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("context", contextKey.getContext());
		params.put("locale", contextKey.getLocale());
		params.put("since", since);
		Set<String> retain = (since == 0L ? new HashSet<String>() : null);
        Results results;
        try {
			results = DataLayerMgr.executeQuery(getCallId(), params);
            while(results.next()) {
				String key = results.getFormattedValue(1);
				translations.put(key, results.getFormattedValue(2));
				if(retain != null)
					retain.add(key);
            }
        } catch(SQLException e) {
            log.warn("While getting translations for locale '" + contextKey.getLocale() + "' and context '" + contextKey.getContext(), e);
			throw new ServiceException(e);
        } catch(DataLayerException e) {
            log.warn("While getting translations for locale '" + contextKey.getLocale() + "' and context '" + contextKey.getContext(), e);
			throw new ServiceException(e);
        }
		if(retain != null)
			translations.keySet().retainAll(retain);
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    @Override
	protected Translator createTranslator(ContextKey contextKey) throws ServiceException {
		return new MapTranslator(contextKey);
    }
}
