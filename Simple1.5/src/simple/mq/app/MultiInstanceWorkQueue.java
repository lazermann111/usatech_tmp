package simple.mq.app;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import simple.app.Prerequisite;
import simple.app.RetryPolicy;
import simple.app.ServiceStatus;
import simple.app.Work;
import simple.app.WorkQueue;
import simple.app.WorkQueueException;
import simple.event.ProgressListener;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.Initializer;
import simple.mq.ConsumedMessage;
import simple.mq.Message;
import simple.mq.MessagingException;
import simple.mq.Worker;
import simple.util.MapBackedSet;
import simple.util.concurrent.AbstractLockingPool;
import simple.util.concurrent.CreationException;
import simple.util.concurrent.ResultFuture;

public abstract class MultiInstanceWorkQueue extends SimpleWorkQueue {
	private static final Log log = Log.getLog();
	protected static final String MULTI_SUFFIX = "#";
	protected final SynchronousQueue<Work<ByteInput>> transfer = new SynchronousQueue<Work<ByteInput>>();
	protected final Set<RetrieverThread> retrieverThreads = new MapBackedSet<RetrieverThread>(new ConcurrentHashMap<RetrieverThread, Object>());
	protected final AtomicInteger retrievingCount = new AtomicInteger();
	protected int configuredMaxActive;
	protected final AbstractLockingPool<MultiInstanceWorker> workers = new AbstractLockingPool<MultiInstanceWorker>() {
		@Override
		protected MultiInstanceWorker createObject(long timeout) throws CreationException, TimeoutException, InterruptedException {
			try {
				return new MultiInstanceWorker(supervisor.createWorker());
			} catch(MessagingException e) {
				throw new CreationException(e);
			}
		}

		@Override
		protected void closeObject(MultiInstanceWorker obj) throws Exception {
			obj.close();
		}
	};
	protected final Initializer<WorkQueueException> initializer = new Initializer<WorkQueueException>() {
		@Override
		protected void doInitialize() throws WorkQueueException {
			String[] instanceQueueNames = getInstanceQueueNames();
			for(String instanceQueueName : instanceQueueNames) {
				RetrieverThread rt = new RetrieverThread(instanceQueueName);
				rt.start();
			}
		}

		@Override
		protected void doReset() {
			for(RetrieverThread rt : retrieverThreads)
				rt.shutdown();
		}
	};
	protected final Retriever<ByteInput> aggregatingRetriever = new Retriever<ByteInput>() {
		@Override
		public Work<ByteInput> next() throws WorkQueueException, InterruptedException {
			return transfer.take();
		}

		@Override
		public void close() {
			retrievingCount.decrementAndGet();
			try {
				adjustMaxActive();
			} catch(WorkQueueException e) {
				log.warn("Could not adjust max active", e);
			}
		}

		@Override
		public boolean cancel(Thread thread) {
			thread.interrupt();
			return true;
		}

		public boolean arePrequisitesAvailable() {
			return prerequisiteManager.arePrequisitesAvailable();
		}

		public void cancelPrequisiteCheck(Thread thread) {
			prerequisiteManager.cancelPrequisiteCheck(thread);
		}

		public void resetAvailability() {
			prerequisiteManager.resetAvailability();
		}
	};

	protected class MultiInstanceWorker implements Worker {
		protected final Worker delegate;

		public MultiInstanceWorker(Worker delegate) {
			this.delegate = delegate;
		}

		public void publish(Message message, boolean broadcast, String... blocks) throws MessagingException {
			delegate.publish(message, broadcast, blocks);
		}

		public ConsumedMessage consume(String queueName, String subscription) throws MessagingException {
			return delegate.consume(queueName, subscription);
		}

		public void commit() throws MessagingException {
			try {
				delegate.commit();
			} finally {
				workers.returnObject(this);
			}
		}

		public void rollback(Throwable reason, RetryPolicy retryPolicy) throws MessagingException {
			try {
				delegate.rollback(reason, retryPolicy);
			} finally {
				workers.returnObject(this);
			}
		}

		public void close() {
			delegate.close();
		}

		public boolean cancelConsume(Thread consumingThread) {
			return delegate.cancelConsume(consumingThread);
		}

		@Override
		public void block(String... blocks) throws MessagingException {
			delegate.block(blocks);
		}
	}
	protected class RetrieverThread extends Thread implements Retriever<ByteInput> {
		protected final String instanceQueueName;
		protected final AtomicReference<ServiceStatus> status = new AtomicReference<ServiceStatus>(ServiceStatus.INITIALIZING);
		protected volatile Worker worker;

		public RetrieverThread(String instanceQueueName) {
			super("Retriever-" + instanceQueueName);
			this.instanceQueueName = instanceQueueName;
			retrieverThreads.add(this);
			status.set(ServiceStatus.STARTING);
		}

		public boolean cancel(Thread thread) {
			Worker worker = this.worker;
			return worker == null ? false : worker.cancelConsume(thread);
		}

		public void close() {
			Worker worker = this.worker;
			if(worker != null)
				worker.close();
		}

		public Work<ByteInput> next() throws WorkQueueException, InterruptedException {
			prerequisiteManager.arePrequisitesAvailable();
			MultiInstanceWorker worker;
			try {
				this.worker = worker = workers.borrowObject();
			} catch(TimeoutException e) {
				throw new WorkQueueException(e);
			} catch(CreationException e) {
				if(e.getCause() instanceof WorkQueueException)
					throw (WorkQueueException) e.getCause();
				else if(e.getCause() instanceof InterruptedException)
					throw (InterruptedException) e.getCause();
				throw new WorkQueueException(e.getCause());
			}
			boolean okay = false;
			try {
				Work<ByteInput> work = retrieveWork(worker, instanceQueueName, null, prerequisiteManager);
				okay = (work != null);
				return work;
			} finally {
				if(!okay)
					workers.returnObject(worker);
			}
		}

		public boolean arePrequisitesAvailable() {
			return prerequisiteManager.arePrequisitesAvailable();
		}

		public void cancelPrequisiteCheck(Thread thread) {
			prerequisiteManager.cancelPrequisiteCheck(thread);
		}

		public void resetAvailability() {
			prerequisiteManager.resetAvailability();
		}

		@Override
		public void run() {
			if(status.compareAndSet(ServiceStatus.STARTING, ServiceStatus.STARTED)) {
				do {
					try {
						Work<ByteInput> work = next();
						if(work != null)
							transfer.put(work);
					} catch(InterruptedException e) {
						log.info("Interrupted while retrieving from queue " + instanceQueueName);
					} catch(WorkQueueException e) {
						log.warn("Error while retrieving from queue " + instanceQueueName, e);
					}
				} while(status.get() == ServiceStatus.STARTED);
			}
			status.compareAndSet(ServiceStatus.STOPPING, ServiceStatus.STOPPED);
			retrieverThreads.remove(this);
		}

		public boolean shutdown() {
			boolean result = status.compareAndSet(ServiceStatus.STARTED, ServiceStatus.STOPPING);
			Worker worker = this.worker;
			if(worker != null) {
				worker.cancelConsume(this);
				worker.close();
			}
			return result;
		}
	}

	@Override
	public WorkQueue.Retriever<ByteInput> getRetriever(ProgressListener listener) throws WorkQueueException {
		try {
			initializer.initialize();
		} catch(InterruptedException e) {
			throw new WorkQueueException(e);
		}
		retrievingCount.incrementAndGet();
		adjustMaxActive();
		return aggregatingRetriever;
	}

	protected void adjustMaxActive() throws WorkQueueException {
		int r = retrievingCount.get();
		if(r < 1)
			r = 1;
		r += getInstanceQueueNames().length;
		int maxActive = getMaxActive();
		int target;
		if((maxActive < r || configuredMaxActive < maxActive) && (target = Math.max(r, configuredMaxActive)) != maxActive)
			workers.setMaxActive(target);
	}

	@Override
	public Class<ByteInput> getWorkBodyType() {
		return ByteInput.class;
	}

	@Override
	public boolean isAutonomous() {
		return false;
	}

	public Future<Boolean> shutdown() {
		initializer.reset();
		return new ResultFuture<Boolean>() {
			@Override
			protected Boolean produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				return waitForThreads(unit.toMillis(timeout));
			}
		};
	}

	protected boolean waitForThreads(long timeout) {
		long end = System.currentTimeMillis() + timeout;
		for(RetrieverThread rt : retrieverThreads) {
			if(rt.isAlive()) {
				try {
					rt.join(timeout);
				} catch(InterruptedException e) {
					timeout = 0;
				}
				if(timeout > 0) {
					timeout = end - System.currentTimeMillis();
					if(timeout < 0)
						timeout = 0;
				}
			}
		}
		return retrieverThreads.isEmpty();
	}

	protected abstract String[] getInstanceQueueNames() throws WorkQueueException;

	public int getMaxActive() {
		return workers.getMaxActive();
	}

	public void setMaxActive(int maxActive) {
		workers.setMaxActive(maxActive);
		configuredMaxActive = maxActive;
	}

	public int getMaxIdle() {
		return workers.getMaxIdle();
	}

	public void setMaxIdle(int maxIdle) {
		workers.setMaxIdle(maxIdle);
	}

	public long getMaxWait() {
		return workers.getMaxWait();
	}

	public void setMaxWait(long maxWait) {
		workers.setMaxWait(maxWait);
	}

	public static String getBaseQueueName(String instanceQueueName) {
		int pos = instanceQueueName.lastIndexOf(MULTI_SUFFIX);
		if(pos > 0)
			return instanceQueueName.substring(0, pos);
		return instanceQueueName;
	}

	@Override
	public void availibilityChanged(Prerequisite prerequisite, boolean available) {
		String[] instanceQueueNames;
		try {
			instanceQueueNames = getInstanceQueueNames();
		} catch(WorkQueueException e) {
			log.error("Could not get instanceQueueNames to set availability", e);
			return;
		}
		if(available)
			for(String instanceQueueName : instanceQueueNames)
				try {
					supervisor.unpause(instanceQueueName, getSubscription());
				} catch(MessagingException e) {
					log.warn("Could not unpause queue " + instanceQueueName, e);
				}

		else
			for(String instanceQueueName : instanceQueueNames)
				try {
					supervisor.pause(instanceQueueName, getSubscription());
				} catch(MessagingException e) {
					log.warn("Could not pause queue " + instanceQueueName, e);
				}
	}

	public boolean isTracing() {
		return workers.isTracing();
	}

	public void setTracing(boolean tracing) {
		workers.setTracing(tracing);
	}
}
