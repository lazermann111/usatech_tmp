package simple.mq.app;

import java.util.ArrayList;
import java.util.List;

import simple.app.WorkQueueException;

public class AllOrNotOneInstanceWorkQueue extends MultiInstanceWorkQueue {
	protected static final String ANY_SUFFIX = MULTI_SUFFIX + "any";
	protected static final String NOT_SEPARATOR = MULTI_SUFFIX + "not.";
	protected String[] otherInstances;
	protected String instance;

	@Override
	protected String[] getInstanceQueueNames() throws WorkQueueException {
		String instance = getInstance();
		if(instance == null)
			throw new WorkQueueException("instance property was not set");
		if(getOtherInstances() == null)
			throw new WorkQueueException("otherInstances property was not set");
		List<String> iqns = new ArrayList<String>();
		iqns.add(getQueueKey() + ANY_SUFFIX);
		for(String other : getOtherInstances()) {
			int c = instance.compareTo(other);
			if(c != 0)
				iqns.add(getInstanceQueueName(getQueueKey(), other));
		}
		return iqns.toArray(new String[iqns.size()]);
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String[] getOtherInstances() {
		return otherInstances;
	}

	public void setOtherInstances(String[] otherInstances) {
		this.otherInstances = otherInstances;
	}

	public static String getInstanceQueueName(String baseQueueName, String excludeInstance) {
		return baseQueueName + NOT_SEPARATOR + excludeInstance;
	}
}
