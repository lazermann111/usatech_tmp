package simple.mq.app;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import simple.app.AbstractWorkQueue;
import simple.app.BasicQoS;
import simple.app.Prerequisite;
import simple.app.PrerequisiteManager;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.RetryPolicy;
import simple.app.ScheduledRetryServiceException;
import simple.app.ServiceException;
import simple.app.Work;
import simple.app.WorkQueueException;
import simple.app.WorkRetryType;
import simple.bean.ReflectionUtils;
import simple.event.ProgressListener;
import simple.io.ByteInput;
import simple.io.InputStreamByteInput;
import simple.io.Log;
import simple.mq.ConsumedMessage;
import simple.mq.MessagingException;
import simple.mq.Supervisor;
import simple.mq.Worker;

public class SimpleWorkQueue extends AbstractWorkQueue<ByteInput> {
	private static final Log log = Log.getLog();
	protected static final Set<String> EXCLUDED_MESSAGE_PROPERTIES = new HashSet<String>(Arrays.asList(new String[] {
			"content", 
	}));
	protected static final RetryPolicy IMMEDIATE_RETRY_POLICY = new RetryPolicy(20, 0, 0.0f);
	protected static final RetryPolicy NO_RETRY_POLICY = new RetryPolicy(0, Long.MAX_VALUE, 0.0f);
	protected final RetryPolicy scheduledRetryPolicy = new RetryPolicy(16, 15 * 1000L, 2.0f);
	
	protected Supervisor supervisor;
	protected class SimpleRetriever implements Retriever<ByteInput> {
		protected final Worker worker;
		public SimpleRetriever(Worker worker) {
			this.worker = worker;
			start();
		}

		public boolean cancel(Thread thread) {
			return worker.cancelConsume(thread);
		}

		public void close() {
			worker.close();
		}

		public Work<ByteInput> next() throws WorkQueueException, InterruptedException {
			return retrieveWork(worker, getQueueKey(), getSubscription(), prerequisiteManager);
		}

		public boolean arePrequisitesAvailable() {
			return prerequisiteManager.arePrequisitesAvailable();
		}
		
		public void cancelPrequisiteCheck(Thread thread) {
			prerequisiteManager.cancelPrequisiteCheck(thread);
		}

		public void resetAvailability() {
			prerequisiteManager.resetAvailability();
		}

		public void start() {
			Prerequisite[] ps = prerequisiteManager.getPrerequisites();
			if(ps != null)
				for(Prerequisite p : ps)
					if(!p.isAvailable())
						return;
			// all prereqs are met
			try {
				supervisor.unpause(getQueueKey(), getSubscription());
			} catch(MessagingException e) {
				log.warn("Could not unpause queue " + getQueueKey(), e);
			}
		}
	}

	protected Work<ByteInput> retrieveWork(final Worker worker, final String queueKey, final String subscription, final PrerequisiteManager prerequisiteManager) throws WorkQueueException {
		final ConsumedMessage message;
		try {
			message = worker.consume(queueKey, subscription);
		} catch(MessagingException e) {
			throw new WorkQueueException(e);
		}
		if(message == null)
			return null;
		// TODO: use a better implemention of ByteInput around BinaryStream
		final ByteInput body;
		try {
			body = new InputStreamByteInput(message.getContent().getInputStream());
		} catch(IOException e) {
			throw new WorkQueueException(e);
		}
		Map<String, ?> props;
		try {
			props = ReflectionUtils.toPropertyMap(message, EXCLUDED_MESSAGE_PROPERTIES);
		} catch(IntrospectionException e) {
			log.warn("Could not get properties from message", e);
			props = Collections.emptyMap();
		}
		final BasicQoS qos = new BasicQoS(message.getPriority(), !message.isTemporary(), props);
		qos.setExpiration(message.getExpiration(message.getPosted()));
		final Publisher<ByteInput> publisher = new AbstractSimplePublisher() {
			public void close() {
				// do nothing
			}

			protected void onMessageSent(Worker worker) throws ServiceException {
				// do nothing
			}

			protected Worker getWorker() throws ServiceException {
				return worker;
			}

			public Supervisor getSupervisor() {
				return supervisor;
			}
		};
		return new Work<ByteInput>() {
			protected boolean complete;

			public String getGuid() {
				return message.getMessageId();
			}
			public ByteInput getBody() {
				return body;
			}

			public long getEnqueueTime() {
				return message.getPosted();
			}

			public String getOriginalQueueName() {
				return message.getQueueName();
			}

			public Publisher<ByteInput> getPublisher() throws WorkQueueException {
				return publisher;
			}

			public QoS getQoS() {
				return qos;
			}

			public String getQueueName() {
				return message.getQueueName();
			}

			public String getSubscription() {
				return message.getSubscription();
			}

			public int getRetryCount() {
				return message.getRetryCount();
			}

			public Map<String, ?> getWorkProperties() {
				return Collections.emptyMap();
			}

			public boolean isComplete() {
				return complete;
			}

			public boolean isRedelivered() {
				return message.isRedelivered() || message.getRetryCount() > 0;
			}

			public boolean isUnguardedRetryAllowed() {
				// TODO: we may wish to implement this better
				return !message.isRedelivered() && message.getRetryCount() == 0;
			}

			public void workAborted(WorkRetryType retryType, boolean unguardedRetryAllowed, Throwable cause) throws WorkQueueException {
				if(complete)
					throw new WorkQueueException("Work is already complete; workAborted() may not be called");
				RetryPolicy retryPolicy;
				switch(retryType) {
					case IMMEDIATE_RETRY:
						retryPolicy = IMMEDIATE_RETRY_POLICY;
						break;
					case NO_RETRY:
						retryPolicy = NO_RETRY_POLICY;
						break;
					case SCHEDULED_RETRY:
						if(cause instanceof ScheduledRetryServiceException) {
							ScheduledRetryServiceException srse = (ScheduledRetryServiceException) cause;
							retryPolicy = srse.getRetryPolicy();
							if(prerequisiteManager != null && srse.isRecheckPrerequisites())
								prerequisiteManager.resetAvailability();
						} else
							retryPolicy = scheduledRetryPolicy;
						break;
					case BLOCKING_RETRY:
						// Re-check prerequisites
						if(prerequisiteManager != null)
							prerequisiteManager.resetAvailability();
						log.debug("Blocking retry is not allowed; Re-checking prerequisites and using non-blocking retry");
					case NONBLOCKING_RETRY:
					default:
						retryPolicy = null; // Use default in Supervisor
						break;
				}

				try {
					worker.rollback(cause, retryPolicy);
				} catch(MessagingException e) {
					throw new WorkQueueException(e);
				}
				complete = true;
			}

			public void workComplete() throws WorkQueueException {
				if(complete)
					throw new WorkQueueException("Work is already complete; workComplete() may not be called");
				try {
					worker.commit();
				} catch(MessagingException e) {
					throw new WorkQueueException(e);
				}
				complete = true;
			}

			@Override
			public void unblock(String... blocksToClear) {
				if(blocksToClear != null && blocksToClear.length > 0)
					message.unblock(blocksToClear);
			}
		};
	}
	public Retriever<ByteInput> getRetriever(ProgressListener listener) throws WorkQueueException {
		Supervisor supervisor = getSupervisor();
		if(supervisor == null)
			throw new WorkQueueException("Supervisor was not set on " + this);
		try {
			return new SimpleRetriever(supervisor.createWorker());
		} catch(MessagingException e) {
			throw new WorkQueueException(e);
		}
	}

	@Override
	public void availibilityChanged(Prerequisite prerequisite, boolean available) {
		if(available)
			try {
				supervisor.unpause(getQueueKey(), getSubscription());
			} catch(MessagingException e) {
				log.warn("Could not unpause queue " + getQueueKey(), e);
			}
		else
			try {
				supervisor.pause(getQueueKey(), getSubscription());
			} catch(MessagingException e) {
				log.warn("Could not pause queue " + getQueueKey(), e);
			}
	}
	protected String getSubscription() {
		return null;
	}
	
	public Class<ByteInput> getWorkBodyType() {
		return ByteInput.class;
	}

	public boolean isAutonomous() {
		return false;
	}

	public Future<Boolean> shutdown() {
		return TRUE_FUTURE;
	}

	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}

	public RetryPolicy getScheduledRetryPolicy() {
		return scheduledRetryPolicy;
	}
}
