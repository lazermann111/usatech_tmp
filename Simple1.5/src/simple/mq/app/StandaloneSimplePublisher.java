package simple.mq.app;

import java.util.concurrent.ExecutionException;

import simple.app.ServiceException;
import simple.mq.MessagingException;
import simple.mq.Supervisor;
import simple.mq.Worker;
import simple.util.concurrent.RunOnGetFuture;

public class StandaloneSimplePublisher extends AbstractSimplePublisher {
	protected Supervisor supervisor;
	protected final RunOnGetFuture<Worker> sessionFuture = new RunOnGetFuture<Worker>() {
		@Override
		protected Worker call(Object... params) throws Exception {
			return createWorker();
		}
		@Override
		protected void resettingRunResult(Worker result) {
			result.close();	
		}
	};

	public void close() {
		sessionFuture.reset();
	}

	@Override
	protected Worker getWorker() throws ServiceException {
		try {
			return sessionFuture.get();
		} catch(InterruptedException e) {
			throw new ServiceException("Could not create session", e);
		} catch(ExecutionException e) {
			throw new ServiceException("Could not create session", e);
		}
	}
	
	protected void onMessageSent(Worker worker) throws ServiceException {
		try {
			worker.commit();
		} catch(MessagingException e) {
			throw new ServiceException("Could not commit message", e);
		}
	}

	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}
}
