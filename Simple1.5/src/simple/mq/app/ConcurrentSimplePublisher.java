package simple.mq.app;

import java.util.concurrent.locks.ReentrantLock;

import simple.app.QoS;
import simple.app.ServiceException;
import simple.io.ByteInput;


/** A <b>thread-safe</b> implementation of Publisher for JMS system.
 * @author Brian S. Krug
 *
 */
public class ConcurrentSimplePublisher extends StandaloneSimplePublisher {
	protected final ReentrantLock lock =  new ReentrantLock();

	@Override
	public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content, Long correlation, QoS qos, String... blocks) throws ServiceException {
		lock.lock();
		try {
			super.publish(queueKey, multicast, temporary, content, correlation, qos, blocks);
		} finally {
			lock.unlock();
		}
	}
}
