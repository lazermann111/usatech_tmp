package simple.mq.app;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import simple.app.ServiceException;
import simple.mq.MessagingException;
import simple.mq.Supervisor;
import simple.mq.Worker;
import simple.util.MapBackedSet;

public class ThreadLocalSimplePublisher extends AbstractSimplePublisher {
	protected Supervisor supervisor;
	protected final Set<Worker> workers = new MapBackedSet<Worker>(new ConcurrentHashMap<Worker, Object>());
	protected final ThreadLocal<Worker> locals = new ThreadLocal<Worker>() {
		@Override
		protected Worker initialValue() {
			try {
				Worker worker = createWorker();
				workers.add(worker);
				return worker;
			} catch(ServiceException e) {
				throw new UndeclaredThrowableException(e);
			}
		}
	};
	
	public void close() {
		for(Iterator<Worker> iter = workers.iterator(); iter.hasNext(); ) {
			Worker worker = iter.next();
			iter.remove();
			worker.close();
		}
	}
	
	@Override
	protected Worker getWorker() throws ServiceException {
		try {
			return locals.get();
		} catch(UndeclaredThrowableException e) {
			if(e.getCause() instanceof ServiceException)
				throw (ServiceException)e.getCause();
			throw e;
		}
	}
	protected void onMessageSent(Worker worker) throws ServiceException {
		try {
			worker.commit();
		} catch(MessagingException e) {
			throw new ServiceException("Could not commit message", e);
		}
	}

	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}
}
