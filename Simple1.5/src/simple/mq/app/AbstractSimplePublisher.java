package simple.mq.app;

import simple.app.BasicQoS;
import simple.app.Publisher;
import simple.app.QoS;
import simple.app.ServiceException;
import simple.io.BinaryStream;
import simple.io.ByteInput;
import simple.io.ByteInputBinaryStream;
import simple.mq.Message;
import simple.mq.MessagingException;
import simple.mq.Supervisor;
import simple.mq.Worker;

public abstract class AbstractSimplePublisher implements Publisher<ByteInput> {
	protected static final QoS DEFAULT_QOS = new BasicQoS(0, BasicQoS.DEFAULT_PRIORITY, true, BasicQoS.EMPTY_MESSAGE_PROPERTIES);

	protected abstract Worker getWorker() throws ServiceException ;
	
	protected Worker createWorker() throws ServiceException {
		Supervisor supervisor = getSupervisor();
		if(supervisor == null)
			throw new ServiceException("Supervisor property is not set on " + this);
		try {
			return supervisor.createWorker();
		} catch(MessagingException e) {
			throw new ServiceException(e);
		}
	}

	public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content, Long correlation, QoS qos, String... blocks) throws ServiceException {
		Worker worker = getWorker();
		sendMessage(worker, queueKey, multicast, temporary, content, correlation, qos, blocks);
		onMessageSent(worker);
	}

	protected abstract void onMessageSent(Worker worker) throws ServiceException ;

	public abstract Supervisor getSupervisor();

	public void publish(String queueKey, ByteInput content) throws ServiceException {
		publish(queueKey, false, false, content);
	}

	public void publish(String queueKey, boolean multicast, boolean temporary, ByteInput content) throws ServiceException {
		publish(queueKey, multicast, temporary, content, null, DEFAULT_QOS);
	}

	protected void sendMessage(Worker worker, final String queueKey, final boolean multicast, final boolean temporary, final ByteInput content, final Long correlation, QoS qos, String... blocks) throws ServiceException {
		final QoS useQos;
		if(qos == null)
			useQos = DEFAULT_QOS;
		else
			useQos = qos;
		final BinaryStream binaryStream = new ByteInputBinaryStream(content);
		try {
			worker.publish(new Message() {					
				public boolean isTemporary() {
					return temporary || !useQos.isPersistent();
				}					
				public boolean isRedelivered() {
					return false;
				}				
				public int getRetryCount() {
					return 0;
				}					
				public String getQueueName() {
					return queueKey;
				}					
				public byte getPriority() {
					return (byte)useQos.getPriority();
				}				
				public long getPosted() {
					return 0;
				}					
				public String getMessageId() {
					return null;
				}					
				public long getExpiration(long posted) {
					long expiration = useQos.getExpiration();
					long ttl;
					if(expiration == 0 && (ttl=useQos.getTimeToLive()) > 0) {
						expiration = posted + ttl;
						if(expiration < posted)
							expiration = Long.MAX_VALUE; // handles overflow of long
					}
					return expiration;
				}					
				public BinaryStream getContent() {
					return binaryStream;
				}
				public long getAvailable() {
					return useQos.getAvailable();
				}

				@Override
				public Long getCorrelation() {
					return correlation;
				}
			}, multicast, blocks);
		} catch(MessagingException e) {
			throw new ServiceException(e);
		}
	}

	public int getConsumerCount(String queueKey, boolean temporary) {
		return getSupervisor().getConsumerCount(queueKey, temporary);
	}

	@Override
	public String[] getSubscriptions(String queueKey) throws ServiceException {
		try {
			return getSupervisor().getSubscriptions(queueKey);
		} catch(MessagingException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void block(String... blocks) throws ServiceException {
		try {
			getWorker().block(blocks);
		} catch(MessagingException e) {
			throw new ServiceException(e);
		}
	}
}
