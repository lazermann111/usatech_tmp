package simple.mq.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import simple.app.WorkQueueException;

public class AnyTwoInstanceWorkQueue extends MultiInstanceWorkQueue {
	protected static final String ANY_SUFFIX = MULTI_SUFFIX + "any";
	protected static final String EITHER_SEPARATOR = ".or.";
	protected String[] otherInstances;
	protected String instance;
	protected boolean any;

	@Override
	protected String[] getInstanceQueueNames() throws WorkQueueException {
		String instance = getInstance();
		if(instance == null)
			throw new WorkQueueException("instance property was not set");
		if(getOtherInstances() == null)
			throw new WorkQueueException("otherInstances property was not set");
		List<String> iqns = new ArrayList<String>();
		if(any)
			iqns.add(constructQueueNameAny(getQueueKey()));
		iqns.add(constructQueueName(getQueueKey(), instance));
		for(String other : getOtherInstances()) {
			if(other.equals(instance))
				continue;
			String qn = constructQueueName(getQueueKey(), instance, other);
			if(qn != null)
				iqns.add(qn);
		}
		return iqns.toArray(new String[iqns.size()]);
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String[] getOtherInstances() {
		return otherInstances;
	}

	public void setOtherInstances(String[] otherInstances) {
		this.otherInstances = otherInstances;
	}

	public static String constructQueueNameAny(String queueName) {
		return queueName + ANY_SUFFIX;
	}

	public static String constructQueueName(String queueName, String... instances) {
		Arrays.sort(instances);
		StringBuilder sb = new StringBuilder(queueName);
		for(int i = 0; i < instances.length; i++) {
			if(i == 0)
				sb.append(MULTI_SUFFIX);
			else
				sb.append(EITHER_SEPARATOR);
			sb.append(instances[i]);
		}
		return sb.toString();
	}

	public boolean isAny() {
		return any;
	}

	public void setAny(boolean any) {
		this.any = any;
	}
}
