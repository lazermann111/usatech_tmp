package simple.mq.peer;

import java.io.IOException;

import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.results.DatasetUtils;

public class DirectPeerMethodData_INQUIREDB extends DirectPeerMethodData {
	protected String queueName;
	protected String dataSourceName;
	protected String subscription;
	
	public DirectPeerMethodData_INQUIREDB() {
		super(DirectPeerMethod.INQUIREDB);
	}

	@Override
	public void readData(ByteInput input) throws IOException {
		setDataSourceName(DatasetUtils.readSmallString(input));
		setQueueName(DatasetUtils.readSmallString(input));
		setSubscription(DatasetUtils.readSmallString(input));
	}

	@Override
	public void writeData(ByteOutput output) throws IOException {
		DirectPeerUtils.writeMethod(output, getMethod());
		DatasetUtils.writeSmallString(output, dataSourceName);
		DatasetUtils.writeSmallString(output, queueName);
		DatasetUtils.writeSmallString(output, subscription);
		output.flush();
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}
}
