package simple.mq.peer;

import java.io.IOException;

import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.results.DatasetUtils;

public class DirectPeerMethodData_ACK extends DirectPeerMethodData {
	protected String messageId;
	
	public DirectPeerMethodData_ACK() {
		super(DirectPeerMethod.ACK);
	}
	
	public void readData(ByteInput input) throws IOException {
		messageId = DatasetUtils.readSmallString(input);		
	}
	@Override
	public void writeData(ByteOutput output) throws IOException {
		DirectPeerUtils.writeMethod(output, getMethod());
		DatasetUtils.writeSmallString(output, messageId);
		output.flush();
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMethod()).append(" [MessageId=").append(messageId).append("]");
		return sb.toString();
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

}
