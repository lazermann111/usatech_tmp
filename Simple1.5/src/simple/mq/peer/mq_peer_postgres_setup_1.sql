CREATE SCHEMA MQ
  AUTHORIZATION admin;

ALTER DEFAULT PRIVILEGES
    IN SCHEMA MQ
    REVOKE EXECUTE ON FUNCTIONS
    FROM PUBLIC;
     
CREATE OR REPLACE FUNCTION PUBLIC.FROM_HEX(
	pv_hex VARCHAR)
    RETURNS BIGINT
AS $$
DECLARE
	ln_num BIGINT;
BEGIN
	IF pv_hex IS NULL OR LENGTH(TRIM(pv_hex)) = 0 THEN
		RETURN NULL;
	ELSIF TRIM(pv_hex) !~ '^[0-9A-Fa-f]+$' THEN
		RAISE EXCEPTION 'Input contains non-hex character' USING ERRCODE = 'invalid_binary_representation';
	ELSE
		EXECUTE 'SELECT x''' || TRIM(pv_hex) || '''::BIGINT' INTO ln_num;
		RETURN ln_num;
	END IF;			
END;
$$ LANGUAGE plpgsql;

CREATE TABLE MQ.QUEUE
(
   QUEUE_ID SERIAL NOT NULL,
   DATA_SOURCE_NAME VARCHAR(100),
   QUEUE_NAME VARCHAR(100) NOT NULL,
   SUBSCRIPTION_NAME VARCHAR(100),
   QUEUE_TABLE VARCHAR(305) NOT NULL,
   PRIMARY_WAITING_PID INTEGER,
   CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   CONSTRAINT PK_QUEUE PRIMARY KEY(QUEUE_ID)
) 
WITH (
  OIDS = FALSE
);

CREATE UNIQUE INDEX UX_QUEUE_DS_QUEUE_SUB_NAME ON MQ.QUEUE(DATA_SOURCE_NAME,QUEUE_NAME,SUBSCRIPTION_NAME) ;
CREATE UNIQUE INDEX UX_QUEUE_QUEUE_TABLE ON MQ.QUEUE(QUEUE_TABLE) ;

CREATE TABLE MQ.QUEUE_SHADOW_DELETION
(
	QUEUE_ID BIGINT NOT NULL,
	SHADOW_DATA_SOURCE_NAME VARCHAR(100) NOT NULL,
	PRIORITY SMALLINT NOT NULL,
	LAST_MESSAGE_ID VARCHAR(100) NOT NULL,
	LAST_POSTED_TIME BIGINT NOT NULL,
	LAST_AVAILABLE_TIME BIGINT NOT NULL,
	CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
	UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
	CONSTRAINT PK_QUEUE_SHADOW_DELETION PRIMARY KEY(QUEUE_ID, SHADOW_DATA_SOURCE_NAME, PRIORITY),
	CONSTRAINT FK_QSD_QUEUE_ID FOREIGN KEY(QUEUE_ID) REFERENCES MQ.QUEUE(QUEUE_ID)
) 
WITH (
  OIDS = FALSE
);

CREATE TABLE MQ.CARRYOVER_CLEAR_BEFORE
(
	QUEUE_ID BIGINT NOT NULL,
	SHADOW_DATA_SOURCE_NAME VARCHAR(100) NOT NULL,
	LAST_POSTED_TIME BIGINT NOT NULL,
	CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
	UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
	CONSTRAINT PK_CARRYOVER_CLEAR_BEFORE PRIMARY KEY(QUEUE_ID, SHADOW_DATA_SOURCE_NAME),
	CONSTRAINT FK_CCB_QUEUE_ID FOREIGN KEY(QUEUE_ID) REFERENCES MQ.QUEUE(QUEUE_ID)
) 
WITH (
  OIDS = FALSE
);

CREATE TABLE MQ.Q_(
	QUEUE_TABLE VARCHAR(100) NOT NULL,
	MESSAGE_ID VARCHAR(100) NOT NULL,
	CONTENT BYTEA NOT NULL,
	POSTED_TIME BIGINT NOT NULL,
	AVAILABLE_TIME BIGINT NOT NULL,
	EXPIRE_TIME BIGINT NOT NULL,
	PRIORITY SMALLINT NOT NULL,
	TEMPORARY_FLAG BOOLEAN NOT NULL,
	REDELIVERED_FLAG BOOLEAN NOT NULL DEFAULT FALSE,
	RETRY_COUNT INTEGER NOT NULL DEFAULT 0,
	SHADOW_DATA_SOURCE_NAME VARCHAR(100),
	ERROR_MESSAGE TEXT
)
WITH (
  OIDS = FALSE
);

CREATE TABLE MQ.DLQ(
	PRIMARY KEY(MESSAGE_ID)
) INHERITS(MQ.Q_);

CREATE INDEX IX_DLQ_QUEUE_TABLE ON MQ.DLQ(QUEUE_TABLE);
CREATE INDEX IX_DLQ_POSTED_TIME ON MQ.DLQ(POSTED_TIME);

CREATE TABLE MQ.IQ_(
	QUEUE_TABLE VARCHAR(100) NOT NULL,
	MESSAGE_ID VARCHAR(100) NOT NULL,
	CONNECTION_ID VARCHAR(100) NOT NULL
)
WITH (
  OIDS = FALSE
);

CREATE OR REPLACE FUNCTION MQ.FTRAD_QUEUE() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'DROP TABLE IF EXISTS MQ.' || OLD.QUEUE_TABLE;
	EXECUTE 'DROP TABLE IF EXISTS MQ.I' || OLD.QUEUE_TABLE;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.CREATE_QUEUE(
	pv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE,
	pb_primary BOOLEAN
) 
	RETURNS VOID 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'CREATE TABLE MQ.' || pv_queue_table || '('
		|| 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
		|| 'CONSTRAINT CK_' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
		|| 'PRIMARY KEY(MESSAGE_ID)'
		|| ') INHERITS(MQ.Q_)';
	EXECUTE 'GRANT SELECT, INSERT ON MQ.'|| pv_queue_table || ' TO use_mq';
	EXECUTE 'ALTER TABLE MQ.'|| pv_queue_table || ' OWNER TO use_mq';
	EXECUTE 'CREATE UNIQUE INDEX UX_' || pv_queue_table || '_ORDER ON MQ.' || pv_queue_table || '(AVAILABLE_TIME, PRIORITY DESC, POSTED_TIME, MESSAGE_ID)';
	EXECUTE 'CREATE INDEX IX_' || pv_queue_table || '_EXPIRE_TIME ON MQ.' || pv_queue_table || '(EXPIRE_TIME)';
	IF pb_primary THEN
		EXECUTE 'CREATE TABLE MQ.I' || pv_queue_table || '('
			|| 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
			|| 'CONSTRAINT CK_I' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
			|| 'PRIMARY KEY(MESSAGE_ID)'
			|| ') INHERITS(MQ.IQ_)';
		EXECUTE 'ALTER TABLE MQ.I'|| pv_queue_table || ' OWNER TO use_mq';	
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FTRAI_QUEUE() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	PERFORM MQ.CREATE_QUEUE(NEW.QUEUE_TABLE, NEW.DATA_SOURCE_NAME IS NULL);
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FTRAU_QUEUE() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'ALTER TABLE MQ.' || OLD.QUEUE_TABLE || ' RENAME TO MQ.' || NEW.QUEUE_TABLE;
	EXECUTE 'ALTER TABLE MQ.' || NEW.QUEUE_TABLE || ' ALTER QUEUE_TABLE SET DEFAULT ''' || NEW.QUEUE_TABLE || '''';		
	EXECUTE 'ALTER TABLE MQ.' || NEW.QUEUE_TABLE || ' DROP CONSTRAINT CK_' || OLD.QUEUE_TABLE;
	EXECUTE 'ALTER TABLE MQ.' || NEW.QUEUE_TABLE || ' ADD CONSTRAINT CK_' || NEW.QUEUE_TABLE
		|| ' CHECK (QUEUE_TABLE = ''' || NEW.QUEUE_TABLE || ''')';
	EXECUTE 'ALTER INDEX UX_' || OLD.QUEUE_TABLE || '_ORDER RENAME TO UX_' || NEW.QUEUE_TABLE || '_ORDER';
	EXECUTE 'ALTER INDEX IX_' || OLD.QUEUE_TABLE || '_EXPIRE_TIME RENAME TO IX_' || NEW.QUEUE_TABLE || '_EXPIRE_TIME';
	IF NEW.DATA_SOURCE_NAME IS NULL THEN
		EXECUTE 'ALTER TABLE MQ.I' || OLD.QUEUE_TABLE || ' RENAME TO MQ.I' || NEW.QUEUE_TABLE;
		EXECUTE 'ALTER TABLE MQ.I' || NEW.QUEUE_TABLE || ' ALTER QUEUE_TABLE SET DEFAULT ''' || NEW.QUEUE_TABLE || '''';		
		EXECUTE 'ALTER TABLE MQ.I' || NEW.QUEUE_TABLE || ' DROP CONSTRAINT CK_I' || OLD.QUEUE_TABLE;
		EXECUTE 'ALTER TABLE MQ.I' || NEW.QUEUE_TABLE || ' ADD CONSTRAINT CK_I' || NEW.QUEUE_TABLE
			|| ' CHECK (QUEUE_TABLE = ''' || NEW.QUEUE_TABLE || ''')';
	END IF;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TRAD_QUEUE
AFTER DELETE ON MQ.QUEUE
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRAD_QUEUE();

CREATE TRIGGER TRAI_QUEUE
AFTER INSERT ON MQ.QUEUE
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRAI_QUEUE();
    
CREATE TRIGGER TRAU_QUEUE
AFTER UPDATE ON MQ.QUEUE
    FOR EACH ROW
    WHEN (NEW.QUEUE_TABLE != OLD.QUEUE_TABLE) 
	EXECUTE PROCEDURE MQ.FTRAU_QUEUE();
    
CREATE OR REPLACE FUNCTION MQ.FTRBI_Q_() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'INSERT INTO MQ.' || NEW.QUEUE_TABLE || ' VALUES($1.*)'
		USING NEW;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FTRBU_Q_() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'UPDATE MQ.' || NEW.QUEUE_TABLE || ' SET(*) = ($1) WHERE MESSAGE_ID = $2'
		USING NEW, OLD.MESSAGE_ID;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FTRBD_Q_() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'DELETE FROM MQ.' || OLD.QUEUE_TABLE || ' WHERE MESSAGE_ID = $1'
		USING OLD.MESSAGE_ID;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TRBD_Q_
BEFORE DELETE ON MQ.Q_
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRBD_Q_();

CREATE TRIGGER TRBI_Q_
BEFORE INSERT ON MQ.Q_
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRBI_Q_();
    
CREATE TRIGGER TRBU_Q_
BEFORE UPDATE ON MQ.Q_
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRBU_Q_();

CREATE OR REPLACE FUNCTION MQ.GET_QUEUE_TABLE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
	RETURNS MQ.QUEUE.QUEUE_TABLE%TYPE
	IMMUTABLE
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := 'Q_' 
		|| CASE WHEN pv_data_source_name IS NULL THEN '' ELSE UPPER(REGEXP_REPLACE(pv_data_source_name, E'\\W', '_', 'g')) || '$' END
		|| COALESCE(UPPER(REGEXP_REPLACE(pv_queue_name, E'\\W', '_', 'g')), '_') 
		|| CASE WHEN pv_subscription_name IS NULL THEN '' ELSE '$' || UPPER(REGEXP_REPLACE(pv_subscription_name, E'\\W', '_', 'g')) END;
	IF LENGTH(lv_queue_table) > 62 THEN
		lv_queue_table := substr(lv_queue_table, 1, 53) || '_' || substr(md5(substr(lv_queue_table, 54)), 1, 8);
	END IF;
	RETURN lv_queue_table;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.REGISTER_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	BEGIN
		INSERT INTO MQ.QUEUE(DATA_SOURCE_NAME, QUEUE_NAME, SUBSCRIPTION_NAME, QUEUE_TABLE)
			  VALUES(pv_data_source_name, pv_queue_name, pv_subscription_name, lv_queue_table);
		RETURN TRUE;
	EXCEPTION
		WHEN unique_violation THEN
			RETURN FALSE;
	END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_QUEUE_ID(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS MQ.QUEUE.QUEUE_ID%TYPE
	SECURITY DEFINER
AS $$
DECLARE
	ln_queue_id MQ.QUEUE.QUEUE_ID%TYPE;
BEGIN
	SELECT QUEUE_ID
	  INTO ln_queue_id
	  FROM MQ.QUEUE 
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
	   AND DATA_SOURCE_NAME	= pv_data_source_name;
	IF NOT FOUND THEN
		PERFORM MQ.REGISTER_QUEUE(pv_data_source_name, pv_queue_name, pv_subscription_name);	
		RETURN MQ.GET_QUEUE_ID(pv_data_source_name, pv_queue_name, pv_subscription_name);
	ELSE
		RETURN ln_queue_id;
	END IF;
END;
$$ LANGUAGE plpgsql;
	   
CREATE OR REPLACE FUNCTION MQ.PREPARE_QUEUE_LOCK(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pb_primary BOOLEAN,
	pb_wait BOOLEAN)
    RETURNS MQ.QUEUE.QUEUE_ID%TYPE
	SECURITY DEFINER
AS $$
DECLARE
	ln_queue_id MQ.QUEUE.QUEUE_ID%TYPE;
BEGIN
	SELECT QUEUE_ID
	  INTO ln_queue_id
	  FROM MQ.QUEUE 
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
	   AND DATA_SOURCE_NAME IS NULL;
	IF NOT FOUND THEN
		PERFORM MQ.REGISTER_QUEUE(NULL, pv_queue_name, pv_subscription_name);
		RETURN MQ.PREPARE_QUEUE_LOCK(pv_queue_name, pv_subscription_name, pb_primary, pb_wait);
	ELSIF pb_wait AND pb_primary THEN
		UPDATE MQ.QUEUE
		   SET PRIMARY_WAITING_PID = PG_BACKEND_PID()
		 WHERE QUEUE_ID = ln_queue_id;
	END IF;
	RETURN ln_queue_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.ACQUIRE_QUEUE_LOCK(
	pn_queue_id MQ.QUEUE.QUEUE_ID%TYPE,
	pn_clear_before BIGINT,
	pb_primary BOOLEAN,
	pb_wait BOOLEAN)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lb_success BOOLEAN;
	lr_rec RECORD;
	ln_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE;
	lv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE;	
BEGIN
	SELECT QUEUE_TABLE
	  INTO STRICT lv_queue_table
	  FROM MQ.QUEUE 
	 WHERE QUEUE_ID = pn_queue_id;
	IF pb_wait THEN
		PERFORM PG_ADVISORY_LOCK(105, pn_queue_id);
		lb_success := TRUE;
		IF pb_primary THEN
			UPDATE MQ.QUEUE
			   SET PRIMARY_WAITING_PID = NULL
			 WHERE QUEUE_ID = pn_queue_id
			   AND PRIMARY_WAITING_PID = PG_BACKEND_PID();
		END IF;		
	ELSE
		lb_success := PG_TRY_ADVISORY_LOCK(105, pn_queue_id);
	END IF;
	IF lb_success THEN
		IF pn_clear_before > 0 THEN
			EXECUTE 'DELETE FROM MQ.' || lv_queue_table
				 || ' WHERE POSTED_TIME < $1'
				USING pn_clear_before;
		END IF;	
		EXECUTE 'UPDATE MQ.' || lv_queue_table || ' SET REDELIVERED_FLAG = TRUE'
			 || ' WHERE MESSAGE_ID IN('
			 || 'SELECT MESSAGE_ID'
			 || ' FROM MQ.I' || lv_queue_table|| ')';
		EXECUTE 'TRUNCATE TABLE MQ.I' || lv_queue_table;
		FOR lr_rec IN 
			SELECT PRIORITY, LAST_AVAILABLE_TIME
			  FROM MQ.QUEUE_SHADOW_DELETION
		     WHERE QUEUE_ID = pn_queue_id LOOP
			EXECUTE 'SELECT POSTED_TIME, MESSAGE_ID FROM MQ.' || lv_queue_table || ' WHERE AVAILABLE_TIME <= $2 AND PRIORITY = $1 ORDER BY POSTED_TIME ASC, MESSAGE_ID ASC LIMIT 1'
			   INTO ln_last_posted_time, lv_last_message_id
			  USING lr_rec.PRIORITY, lr_rec.LAST_AVAILABLE_TIME;
			IF ln_last_posted_time IS NOT NULL THEN
				UPDATE MQ.QUEUE_SHADOW_DELETION
				   SET LAST_POSTED_TIME = ln_last_posted_time,
				       LAST_MESSAGE_ID = lv_last_message_id
		         WHERE QUEUE_ID = pn_queue_id
		           AND PRIORITY = lr_rec.PRIORITY;
			END IF;
		END LOOP;
		RETURN TRUE;
	ELSE
		RETURN FALSE;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.REMOVE_INPROCESS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	EXECUTE 'TRUNCATE TABLE MQ.I' || lv_queue_table;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.RELEASE_QUEUE_LOCK(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_queue_id MQ.QUEUE.QUEUE_ID%TYPE;
	ln_primary_waiting_pid MQ.QUEUE.PRIMARY_WAITING_PID%TYPE;
	lb_success BOOLEAN;
BEGIN
	SELECT QUEUE_ID, QUEUE_TABLE, PRIMARY_WAITING_PID
	  INTO ln_queue_id, lv_queue_table, ln_primary_waiting_pid
	  FROM MQ.QUEUE 
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
	   AND DATA_SOURCE_NAME IS NULL;
    IF NOT FOUND THEN
		RETURN FALSE;
	ELSE
		IF ln_primary_waiting_pid IS NOT NULL AND ln_primary_waiting_pid = PG_BACKEND_PID() THEN
			UPDATE MQ.QUEUE
			   SET PRIMARY_WAITING_PID = NULL
			 WHERE QUEUE_ID = ln_queue_id
			   AND PRIMARY_WAITING_PID = PG_BACKEND_PID();
		END IF;
		lb_success := PG_ADVISORY_UNLOCK(105, ln_queue_id);
		IF lb_success THEN
			EXECUTE 'TRUNCATE TABLE MQ.I' || lv_queue_table;
		END IF;
		RETURN lb_success;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.MARK_INPROCESS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.IQ_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pb_check_existence BOOLEAN,
	pb_redelivered OUT MQ.Q_.REDELIVERED_FLAG%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lv_current_connection_id MQ.IQ_.CONNECTION_ID%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	IF pb_check_existence THEN
		EXECUTE 'SELECT REDELIVERED_FLAG FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
		   INTO pb_redelivered
		  USING pv_message_id;
		IF pb_redelivered IS NULL THEN
			RAISE EXCEPTION 'Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U01', HINT = 'Another poller processed or is processing this message, or queue was cleared';
		END IF;
	END IF;
	BEGIN
		EXECUTE 'INSERT INTO MQ.I' || lv_queue_table || '(MESSAGE_ID, CONNECTION_ID) VALUES($1,$2)'
		  USING pv_message_id, pv_connection_id;
	EXCEPTION
		WHEN unique_violation THEN
			EXECUTE 'SELECT CONNECTION_ID FROM MQ.I' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
			   INTO lv_current_connection_id  
			  USING pv_message_id;
			IF pv_connection_id != lv_current_connection_id THEN
				RAISE EXCEPTION 'Message Id ''%'' is already marked in process by connection ''%''', pv_message_id, lv_current_connection_id USING ERRCODE = 'unique_violation', HINT = 'Another poller processed or is processing this message';
			END IF;
	END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.DELETE_MESSAGE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pb_remove_inprocess BOOLEAN)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	IF pb_remove_inprocess AND pv_data_source_name IS NULL THEN
		EXECUTE 'DELETE FROM MQ.I' || lv_queue_table || ' WHERE MESSAGE_ID = $1 AND CONNECTION_ID = $2'
		  USING pv_message_id, pv_connection_id;
		GET DIAGNOSTICS ln_count = ROW_COUNT;
		IF ln_count < 1 THEN
			RAISE EXCEPTION 'Inprocess Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U02', HINT = 'Another poller processed or is processing this message, or queue was cleared';
		END IF;
	END IF;
	BEGIN
		EXECUTE 'DELETE FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
		  USING pv_message_id;
		GET DIAGNOSTICS ln_count = ROW_COUNT;
		IF ln_count < 1 THEN
			RAISE EXCEPTION 'Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U01', HINT = 'Another poller processed or is processing this message, or queue was cleared';
		END IF;
	EXCEPTION
		WHEN undefined_table THEN
			-- do nothing
	END;	
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.REMOVE_EXPIRED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[])
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	BEGIN
		EXECUTE 'DELETE FROM MQ.' || lv_queue_table 
			 || ' WHERE EXPIRE_TIME != 0 AND EXPIRE_TIME <= $1'
			 || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
		  USING pn_current_time, pa_exclude_message_ids;
		GET DIAGNOSTICS ln_count = ROW_COUNT;
	EXCEPTION
		WHEN undefined_table THEN
			ln_count := 0;
	END;
	RETURN ln_count;
END;
$$ LANGUAGE plpgsql;
		
CREATE OR REPLACE FUNCTION MQ.RETRY_MESSAGE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pn_retry_count MQ.Q_.RETRY_COUNT%TYPE,
	pv_error_message MQ.Q_.ERROR_MESSAGE%TYPE,
	pb_no_retry BOOLEAN,
	pb_remove_inprocess BOOLEAN)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	IF pb_remove_inprocess AND pv_data_source_name IS NULL THEN
		EXECUTE 'DELETE FROM MQ.I' || lv_queue_table || ' WHERE MESSAGE_ID = $1 AND CONNECTION_ID = $2'
		  USING pv_message_id, pv_connection_id;
		GET DIAGNOSTICS ln_count = ROW_COUNT;
		IF ln_count < 1 THEN
			RAISE EXCEPTION 'Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U01', HINT = 'Another poller processed or is processing this message, or queue was cleared';
		END IF;
	END IF;
	IF pb_no_retry THEN
		BEGIN
			EXECUTE 'INSERT INTO MQ.DLQ(QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME,'
				|| ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME, RETRY_COUNT, ERROR_MESSAGE)'
				|| ' SELECT QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, $2,'
				|| ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME, $3, $4'
				|| ' FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
				USING pv_message_id, pn_available_time, pn_retry_count, pv_error_message;
			EXECUTE 'DELETE FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
			  USING pv_message_id;
		EXCEPTION
			WHEN undefined_table THEN
				-- do nothing
		END;
	ELSE
		BEGIN
			EXECUTE 'UPDATE MQ.' || lv_queue_table || ' SET AVAILABLE_TIME = $2, RETRY_COUNT = $3, ERROR_MESSAGE = $4 WHERE MESSAGE_ID = $1'
			  USING pv_message_id, pn_available_time, pn_retry_count, pv_error_message;
		EXCEPTION
			WHEN undefined_table THEN
				-- do nothing
		END;
	END IF;
END;
$$ LANGUAGE plpgsql;
	
CREATE OR REPLACE FUNCTION MQ.CLEAR_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	IF pv_data_source_name IS NULL THEN
		EXECUTE 'TRUNCATE TABLE MQ.I' || lv_queue_table;
	END IF;
	BEGIN
		EXECUTE 'TRUNCATE TABLE MQ.' || lv_queue_table;
	EXCEPTION
		WHEN undefined_table THEN
			-- do nothing
	END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.DROP_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
BEGIN
	DELETE FROM MQ.QUEUE
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name) 
	   AND DATA_SOURCE_NAME IS NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.ADD_MESSAGE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pb_content MQ.Q_.CONTENT%TYPE,
	pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
	pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
	pn_priority MQ.Q_.PRIORITY%TYPE,
	pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
	pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	BEGIN
		EXECUTE 'INSERT INTO MQ.' || lv_queue_table || '(MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME,'
			|| ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME)'
			|| ' VALUES($1, $2, $3, $4, $5, $6, $7, $8)'
			USING pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, 
				  pb_temporary_flag, pv_shadow_data_source_name;
	EXCEPTION
		WHEN undefined_table THEN
			INSERT INTO MQ.QUEUE(DATA_SOURCE_NAME, QUEUE_NAME, SUBSCRIPTION_NAME, QUEUE_TABLE)
			  VALUES(pv_data_source_name, pv_queue_name, pv_subscription_name, lv_queue_table);
			EXECUTE 'INSERT INTO MQ.' || lv_queue_table || '(MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME,'
				|| ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME)'
				|| ' VALUES($1, $2, $3, $4, $5, $6, $7, $8)'
				USING pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, 
					  pb_temporary_flag, pv_shadow_data_source_name;
	
	END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.BROADCAST_MESSAGE(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pb_content MQ.Q_.CONTENT%TYPE,
	pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
	pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
	pn_priority MQ.Q_.PRIORITY%TYPE,
	pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
	pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE)
    RETURNS VARCHAR[]
	SECURITY DEFINER
AS $$
DECLARE
	la_subscriptions VARCHAR[];
BEGIN
	SELECT ARRAY_AGG(DISTINCT SUBSCRIPTION_NAME ORDER BY SUBSCRIPTION_NAME)
	  INTO la_subscriptions
	  FROM MQ.QUEUE 
	 WHERE QUEUE_NAME = pv_queue_name 
	   AND DATA_SOURCE_NAME IS NULL
	   AND SUBSCRIPTION_NAME IS NOT NULL;
	IF la_subscriptions IS NOT NULL AND ARRAY_LENGTH(la_subscriptions, 1) > 0 THEN
		FOR i IN ARRAY_LOWER(la_subscriptions, 1)..ARRAY_UPPER(la_subscriptions, 1) LOOP
			PERFORM MQ.ADD_MESSAGE(NULL, pv_queue_name, la_subscriptions[i], pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, pb_temporary_flag, pv_shadow_data_source_name);		
		END LOOP;
	END IF;
	RETURN la_subscriptions;
END;
$$ LANGUAGE plpgsql;
	
CREATE OR REPLACE FUNCTION MQ.REGISTER_SUBSCRIPTIONS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pa_subscriptions VARCHAR[])
    RETURNS VARCHAR[]
	SECURITY DEFINER
AS $$
DECLARE
	la_missing_subscriptions VARCHAR[];
	la_new_subscriptions VARCHAR[];
BEGIN
	SELECT ARRAY_AGG(DISTINCT CASE WHEN S IS NULL THEN Q.SUBSCRIPTION_NAME END), 
	       ARRAY_AGG(DISTINCT CASE WHEN Q.SUBSCRIPTION_NAME IS NULL THEN S END)
	  INTO la_missing_subscriptions, la_new_subscriptions
	  FROM (SELECT * 
	          FROM  MQ.QUEUE 
	         WHERE QUEUE_NAME = pv_queue_name 
	           AND DATA_SOURCE_NAME IS NULL
	           AND SUBSCRIPTION_NAME IS NOT NULL) Q
	  FULL OUTER JOIN UNNEST(pa_subscriptions) S ON Q.SUBSCRIPTION_NAME = S
	 WHERE S IS NULL OR Q.SUBSCRIPTION_NAME IS NULL;
	   
	IF la_new_subscriptions IS NOT NULL AND ARRAY_LENGTH(la_new_subscriptions, 1) > 0 THEN
		FOR i IN ARRAY_LOWER(la_new_subscriptions, 1)..ARRAY_UPPER(la_new_subscriptions, 1) LOOP
			PERFORM MQ.REGISTER_QUEUE(pv_data_source_name, pv_queue_name, la_new_subscriptions[i]);		
		END LOOP;
	END IF;
	RETURN la_missing_subscriptions;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_MESSAGES(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_max_rows INTEGER,
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[])
    RETURNS TABLE(
  		MESSAGE_ID MQ.Q_.MESSAGE_ID%TYPE, 
  		CONTENT_HEX TEXT,
  		POSTED_TIME MQ.Q_.POSTED_TIME%TYPE,
		AVAILABLE_TIME MQ.Q_.AVAILABLE_TIME%TYPE,
		EXPIRE_TIME MQ.Q_.EXPIRE_TIME%TYPE,
		PRIORITY MQ.Q_.PRIORITY%TYPE,
		TEMPORARY_FLAG MQ.Q_.TEMPORARY_FLAG%TYPE,
		REDELIVERED_FLAG MQ.Q_.REDELIVERED_FLAG%TYPE,
		RETRY_COUNT MQ.Q_.RETRY_COUNT%TYPE,
		SHADOW_DATA_SOURCE_NAME MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE)
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(NULL, pv_queue_name, pv_subscription_name);
	RETURN QUERY EXECUTE 'SELECT MESSAGE_ID, ENCODE(CONTENT, ''HEX''), POSTED_TIME, AVAILABLE_TIME, EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, REDELIVERED_FLAG, RETRY_COUNT, SHADOW_DATA_SOURCE_NAME'
		|| ' FROM MQ.' || lv_queue_table 
		|| ' WHERE AVAILABLE_TIME <= $1'
		|| ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
		|| ' AND (EXPIRE_TIME = 0 OR EXPIRE_TIME > $1)'
		|| ' ORDER BY PRIORITY DESC, POSTED_TIME, MESSAGE_ID LIMIT $3'
		USING pn_current_time, pa_exclude_message_ids, pn_max_rows;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_COUNT_OF_WAITERS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	ln_count INTEGER;
BEGIN
	SELECT COUNT(*)
	  INTO ln_count
	  FROM PG_LOCKS L
	  JOIN MQ.QUEUE Q ON Q.QUEUE_ID = L.OBJID AND Q.PRIMARY_WAITING_PID = L.PID
	 WHERE L.CLASSID = 105
	   AND L.OBJSUBID = 2
	   AND L.LOCKTYPE = 'advisory'
	   AND L.GRANTED = FALSE
	   AND Q.QUEUE_NAME = pv_queue_name
	   AND ((Q.SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR Q.SUBSCRIPTION_NAME = pv_subscription_name) 
	   AND DATA_SOURCE_NAME IS NULL;
	RETURN ln_count;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.QUEUE_EXISTS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
DECLARE
	ln_count INTEGER;
BEGIN
	SELECT COUNT(*)
	  INTO ln_count
	  FROM MQ.QUEUE
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name) 
	   AND DATA_SOURCE_NAME IS NULL;
	RETURN ln_count > 0;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_SHADOW_DELETIONS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS TABLE(
  		SHADOW_DATA_SOURCE_NAME MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
  		PRIORITY MQ.Q_.PRIORITY%TYPE,
  		LAST_MESSAGE_ID MQ.Q_.MESSAGE_ID%TYPE, 
  		LAST_POSTED_TIME MQ.Q_.POSTED_TIME%TYPE,
		LAST_AVAILABLE_TIME MQ.Q_.AVAILABLE_TIME%TYPE)
	SECURITY DEFINER
AS $$
BEGIN
	RETURN QUERY 
		SELECT QSD.SHADOW_DATA_SOURCE_NAME, QSD.PRIORITY, QSD.LAST_MESSAGE_ID, QSD.LAST_POSTED_TIME, QSD.LAST_AVAILABLE_TIME
		  FROM MQ.QUEUE_SHADOW_DELETION QSD
		  JOIN MQ.QUEUE Q ON QSD.QUEUE_ID = Q.QUEUE_ID
		 WHERE Q.QUEUE_NAME = pv_queue_name
		   AND ((Q.SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR Q.SUBSCRIPTION_NAME = pv_subscription_name)
		   AND Q.DATA_SOURCE_NAME IS NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.MARK_FOR_SHADOW_DELETION(
	pv_shadow_data_source_name MQ.QUEUE_SHADOW_DELETION.SHADOW_DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_priority MQ.QUEUE_SHADOW_DELETION.PRIORITY%TYPE,
	pv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE,
	pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
	pn_last_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	ln_queue_id MQ.QUEUE.QUEUE_ID%TYPE;
	lv_current_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE;
	ln_current_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE;
	ln_current_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE;
BEGIN
	SELECT Q.QUEUE_ID
	  INTO STRICT ln_queue_id
	  FROM MQ.QUEUE Q
	 WHERE Q.QUEUE_NAME = pv_queue_name
		   AND ((Q.SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR Q.SUBSCRIPTION_NAME = pv_subscription_name)
		   AND Q.DATA_SOURCE_NAME IS NULL;
	SELECT LAST_MESSAGE_ID, LAST_POSTED_TIME, LAST_AVAILABLE_TIME
	  INTO lv_current_message_id, ln_current_posted_time, ln_current_available_time
	  FROM MQ.QUEUE_SHADOW_DELETION
	 WHERE SHADOW_DATA_SOURCE_NAME = pv_shadow_data_source_name
	   AND PRIORITY = pn_priority
	   AND QUEUE_ID = ln_queue_id
	   FOR UPDATE;
	IF NOT FOUND THEN
		BEGIN	
			INSERT INTO MQ.QUEUE_SHADOW_DELETION(QUEUE_ID, SHADOW_DATA_SOURCE_NAME, PRIORITY, LAST_MESSAGE_ID, LAST_POSTED_TIME, LAST_AVAILABLE_TIME)
			  VALUES(ln_queue_id, pv_shadow_data_source_name, pn_priority, pv_last_message_id, pn_last_posted_time, pn_last_available_time);
		EXCEPTION 
			WHEN unique_violation THEN
        		PERFORM MQ.MARK_FOR_SHADOW_DELETION(pv_shadow_data_source_name, pv_queue_name, pv_subscription_name, pn_priority, pv_last_message_id, pn_last_posted_time, pn_last_available_time);
    	END;
	ELSIF pn_last_available_time > ln_current_available_time 
	  OR (pn_last_available_time = ln_current_available_time AND (pn_last_posted_time > ln_current_posted_time 
	  OR (pn_last_posted_time = ln_current_posted_time AND pv_last_message_id > lv_current_message_id))) THEN
		UPDATE MQ.QUEUE_SHADOW_DELETION
		   SET LAST_MESSAGE_ID = pv_last_message_id,
		   	   LAST_POSTED_TIME = pn_last_posted_time,
		   	   LAST_AVAILABLE_TIME = pn_last_available_time
		 WHERE SHADOW_DATA_SOURCE_NAME = pv_shadow_data_source_name
		   AND PRIORITY = pn_priority
		   AND QUEUE_ID = ln_queue_id;
		IF NOT FOUND THEN
			PERFORM MQ.MARK_FOR_SHADOW_DELETION(pv_shadow_data_source_name, pv_queue_name, pv_subscription_name, pn_priority, pv_last_message_id, pn_last_posted_time, pn_last_available_time);
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
	  	
CREATE OR REPLACE FUNCTION MQ.REMOVE_SHADOW_MESSAGES(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_priority MQ.QUEUE_SHADOW_DELETION.PRIORITY%TYPE,
	pv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE,
	pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
	pn_last_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[])
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	EXECUTE 'DELETE FROM MQ.' || lv_queue_table 
		 || ' WHERE AVAILABLE_TIME <= $1'
		 || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
		 || ' AND PRIORITY = $3'
		 || ' AND (POSTED_TIME < $4 OR (POSTED_TIME = $4 AND MESSAGE_ID < $5))'
	  USING pn_last_available_time, pa_exclude_message_ids, pn_priority, pn_last_posted_time, pv_last_message_id;
	GET DIAGNOSTICS ln_count = ROW_COUNT;
	RETURN ln_count;
END;
$$ LANGUAGE plpgsql;
  	
CREATE OR REPLACE FUNCTION MQ.START_CARRYOVER(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_copy_from_sql OUT TEXT,
	pv_copy_to_sql OUT TEXT)
    SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lv_new_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	lv_new_queue_table := MQ.GET_QUEUE_TABLE(NULL, pv_queue_name, pv_subscription_name);
	EXECUTE 'LOCK MQ.' || lv_queue_table || ' IN EXCLUSIVE MODE';
	EXECUTE 'SELECT COUNT(*) FROM MQ.' || lv_queue_table INTO ln_count;
	IF ln_count = 0 THEN
		pv_copy_from_sql := NULL;
		pv_copy_to_sql := NULL;
	ELSE
		pv_copy_from_sql := 'COPY (SELECT ''' || lv_new_queue_table || ''' QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME, EXPIRE_TIME, '
			|| 'PRIORITY, TEMPORARY_FLAG, TRUE REDELIVERED_FLAG, RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, ERROR_MESSAGE FROM MQ.'
			|| lv_queue_table || ') TO STDOUT (FORMAT ''binary'')';
		pv_copy_to_sql := 'COPY MQ.' || lv_new_queue_table || ' FROM STDIN (FORMAT ''binary'')';
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FINISH_CARRYOVER(
	pv_new_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_old_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_shadow_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pn_last_posted_time OUT MQ.Q_.POSTED_TIME%TYPE,
	pn_row_count OUT INTEGER)
    SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lv_new_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lv_new_table_name VARCHAR;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_old_data_source_name, pv_queue_name, pv_subscription_name);
	lv_new_queue_table := MQ.GET_QUEUE_TABLE(pv_new_data_source_name, pv_queue_name, pv_subscription_name);
	SELECT 'C' || lv_queue_table || '_' || TO_HEX(COALESCE(MAX(FROM_HEX(SUBSTR(TABLENAME, 55, 8))::INTEGER), 0) + 1)
	  INTO STRICT lv_new_table_name
	  FROM PG_TABLES 
     WHERE SCHEMANAME = 'mq'
       AND SUBSTR(TABLENAME, 1, 54) = 'C' || SUBSTR(lv_queue_table, 1, 52) || '_'
       AND SUBSTR(TABLENAME, 55, 8) ~ '^[0-9A-Fa-f]{8}$';
	EXECUTE 'ALTER TABLE MQ.' || lv_queue_table || ' RENAME TO ' || lv_new_table_name;
	EXECUTE 'ALTER INDEX MQ.UX_' || lv_queue_table || '_ORDER RENAME TO UX_' || lv_new_table_name || '_ORDER';
	EXECUTE 'ALTER INDEX MQ.IX_' || lv_queue_table || '_EXPIRE_TIME RENAME TO IX_' || lv_new_table_name || '_EXPIRE_TIME';
	EXECUTE 'ALTER TABLE MQ.' || lv_new_table_name || ' DROP CONSTRAINT CK_' || lv_queue_table;
	PERFORM MQ.CREATE_QUEUE(lv_queue_table, false);	
	EXECUTE 'SELECT MAX(POSTED_TIME) FROM MQ.' || lv_new_table_name
		INTO pn_last_posted_time;  
	EXECUTE 'UPDATE MQ.' || lv_new_table_name || ' SET QUEUE_TABLE = $1, REDELIVERED_FLAG = TRUE'
		USING lv_new_queue_table;	
	GET DIAGNOSTICS pn_row_count = ROW_COUNT;
	PERFORM MQ.SET_CARRYOVER_LAST_POSTED(pv_old_data_source_name, pv_queue_name, pv_subscription_name, pv_shadow_data_source_name, pn_last_posted_time);
	PERFORM MQ.GET_QUEUE_ID(pv_new_data_source_name, pv_queue_name, pv_subscription_name); -- Make sure it exists
	EXECUTE 'ALTER TABLE MQ.' || lv_new_table_name || ' ADD CONSTRAINT CK_' || lv_new_queue_table || ' CHECK (QUEUE_TABLE = ''' || lv_new_queue_table || ''')';
	EXECUTE 'ALTER TABLE MQ.' || lv_new_table_name || ' INHERIT MQ.' || lv_new_queue_table;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.CLEAN_EMPTY_CARRYOVERS()
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	lr_rec RECORD;
	ln_count INTEGER;
	ln_num INTEGER := 0;
BEGIN
	FOR lr_rec IN 
		SELECT T.SCHEMANAME || '.' || T.RELNAME TABLENAME
		  FROM PG_STAT_ALL_TABLES T
		  JOIN PG_NAMESPACE NS ON T.SCHEMANAME = NS.NSPNAME
	      JOIN PG_INHERITS I ON T.RELID = I.INHRELID
	      JOIN PG_CLASS PC ON I.INHPARENT = PC.OID AND NS.OID = PC.RELNAMESPACE
	      JOIN PG_TABLES PT ON PC.RELNAME = PT.TABLENAME AND PT.SCHEMANAME = T.SCHEMANAME
	     WHERE T.SCHEMANAME = 'mq'
	       AND PT.TABLENAME != 'q_'
	       AND SUBSTR(PT.TABLENAME, 1, 2) = 'q_'  
		   AND SUBSTR(T.RELNAME, 1, 3) = 'cq_'
		   /*AND T.N_LIVE_TUP = 0*/
		 ORDER BY T.RELNAME LOOP
		EXECUTE 'SELECT COUNT(*) FROM ' || lr_rec.TABLENAME INTO ln_count;
		IF ln_count = 0 THEN
			EXECUTE 'DROP TABLE ' || lr_rec.TABLENAME;
			ln_num := ln_num + 1;
		END IF;
	 END LOOP;
	 RETURN ln_num;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.SET_CARRYOVER_LAST_POSTED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_shadow_data_source_name MQ.CARRYOVER_CLEAR_BEFORE.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_last_posted_time MQ.CARRYOVER_CLEAR_BEFORE.LAST_POSTED_TIME%TYPE)
	RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	ln_count INTEGER;
	ln_queue_id MQ.QUEUE.QUEUE_ID%TYPE;
BEGIN
	ln_queue_id := MQ.GET_QUEUE_ID(pv_data_source_name, pv_queue_name, pv_subscription_name);
	UPDATE MQ.CARRYOVER_CLEAR_BEFORE
	   SET LAST_POSTED_TIME = pn_last_posted_time
	 WHERE QUEUE_ID = ln_queue_id
	   AND SHADOW_DATA_SOURCE_NAME = pv_shadow_data_source_name;
	GET DIAGNOSTICS ln_count = ROW_COUNT;
	IF ln_count = 0 THEN
		INSERT INTO MQ.CARRYOVER_CLEAR_BEFORE(QUEUE_ID, SHADOW_DATA_SOURCE_NAME, LAST_POSTED_TIME)
			VALUES(ln_queue_id, pv_shadow_data_source_name, pn_last_posted_time);
	END IF;
EXCEPTION 
	WHEN unique_violation THEN
    	PERFORM MQ.SET_CARRYOVER_LAST_POSTED(pv_data_source_name, pv_queue_name, pv_subscription_name, pv_shadow_data_source_name, pn_last_posted_time);        	
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_CARRYOVER_LAST_POSTED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
	RETURNS TABLE(
  		SHADOW_DATA_SOURCE_NAME MQ.CARRYOVER_CLEAR_BEFORE.SHADOW_DATA_SOURCE_NAME%TYPE,
		LAST_POSTED_TIME MQ.CARRYOVER_CLEAR_BEFORE.LAST_POSTED_TIME%TYPE)
	SECURITY DEFINER
AS $$
BEGIN
	RETURN QUERY
		SELECT CCB.SHADOW_DATA_SOURCE_NAME, CCB.LAST_POSTED_TIME
		  FROM MQ.CARRYOVER_CLEAR_BEFORE CCB
		  JOIN MQ.QUEUE Q ON CCB.QUEUE_ID = Q.QUEUE_ID 
		 WHERE Q.DATA_SOURCE_NAME = pv_data_source_name
		   AND Q.QUEUE_NAME = pv_queue_name
		   AND ((Q.SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR Q.SUBSCRIPTION_NAME = pv_subscription_name)
		   AND CCB.LAST_POSTED_TIME > 0;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_SHADOW_QUEUE_TABLES(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE)
    RETURNS TABLE(
  		QUEUE_NAME MQ.QUEUE.QUEUE_NAME%TYPE,
		SUBSCRIPTION_NAME MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
	SECURITY DEFINER
AS $$
BEGIN
	RETURN QUERY 
		SELECT Q.QUEUE_NAME, Q.SUBSCRIPTION_NAME
		  FROM MQ.QUEUE Q
		 WHERE Q.DATA_SOURCE_NAME = pv_data_source_name;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.PREPARE_QUEUE_LOCK(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pb_primary BOOLEAN,
	pb_wait BOOLEAN) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.ACQUIRE_QUEUE_LOCK(
	pn_queue_id MQ.QUEUE.QUEUE_ID%TYPE,
	pn_clear_before BIGINT,
	pb_primary BOOLEAN,
	pb_wait BOOLEAN) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REMOVE_INPROCESS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.RELEASE_QUEUE_LOCK(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.MARK_INPROCESS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.IQ_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pb_check_existence BOOLEAN,
	pb_redelivered OUT MQ.Q_.REDELIVERED_FLAG%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.DELETE_MESSAGE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pb_remove_inprocess BOOLEAN) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REMOVE_EXPIRED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.ADD_MESSAGE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pb_content MQ.Q_.CONTENT%TYPE,
	pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
	pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
	pn_priority MQ.Q_.PRIORITY%TYPE,
	pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
	pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.BROADCAST_MESSAGE(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pb_content MQ.Q_.CONTENT%TYPE,
	pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
	pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
	pn_priority MQ.Q_.PRIORITY%TYPE,
	pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
	pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REGISTER_SUBSCRIPTIONS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pa_subscriptions VARCHAR[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.GET_MESSAGES(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_max_rows INTEGER,
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.GET_COUNT_OF_WAITERS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.RETRY_MESSAGE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pn_retry_count MQ.Q_.RETRY_COUNT%TYPE,
	pv_error_message MQ.Q_.ERROR_MESSAGE%TYPE,
	pb_no_retry BOOLEAN,
	pb_remove_inprocess BOOLEAN) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.CLEAR_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) to use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REGISTER_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.QUEUE_EXISTS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.DROP_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.GET_SHADOW_DELETIONS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
  		
GRANT EXECUTE ON FUNCTION MQ.MARK_FOR_SHADOW_DELETION(
	pv_shadow_data_source_name MQ.QUEUE_SHADOW_DELETION.SHADOW_DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_priority MQ.QUEUE_SHADOW_DELETION.PRIORITY%TYPE,
	pv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE,
	pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
	pn_last_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REMOVE_SHADOW_MESSAGES(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_priority MQ.QUEUE_SHADOW_DELETION.PRIORITY%TYPE,
	pv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE,
	pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
	pn_last_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.START_CARRYOVER(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_copy_from_sql OUT TEXT,
	pv_copy_to_sql OUT TEXT) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.FINISH_CARRYOVER(
	pv_new_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_old_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_shadow_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pn_last_posted_time OUT MQ.Q_.POSTED_TIME%TYPE,
	pn_row_count OUT INTEGER) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.CLEAN_EMPTY_CARRYOVERS() TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.SET_CARRYOVER_LAST_POSTED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_shadow_data_source_name MQ.CARRYOVER_CLEAR_BEFORE.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_last_posted_time MQ.CARRYOVER_CLEAR_BEFORE.LAST_POSTED_TIME%TYPE) TO use_mq;
    
GRANT EXECUTE ON FUNCTION MQ.GET_CARRYOVER_LAST_POSTED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
    
GRANT EXECUTE ON FUNCTION MQ.GET_SHADOW_QUEUE_TABLES(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE) TO use_mq;

GRANT SELECT ON ALL TABLES IN SCHEMA MQ TO read_mq;
GRANT USAGE ON SCHEMA MQ TO read_mq;
GRANT USAGE ON SCHEMA MQ TO use_mq;
GRANT read_mq TO admin;
GRANT use_mq TO admin;