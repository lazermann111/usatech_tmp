/**
 * 
 */
package simple.mq.peer;

import java.sql.SQLException;

import simple.db.ConnectionGroup;
import simple.db.DataLayerException;
import simple.io.Log;
import simple.mq.ConsumedMessage;
import simple.util.concurrent.CorrelationControl;
import simple.util.concurrent.Readiness;

public abstract class ConsumedPeerMessage extends StandardPeerMessage implements ConsumedMessage {
	protected final String instanceId;
	protected final CorrelationControl<Long> corrControl;
	protected String subscription;
	protected String[] unblocks;

	protected ConsumedPeerMessage(String instanceId, CorrelationControl<Long> corrControl) {
		this.instanceId = instanceId;
		this.corrControl = corrControl;
	}
	public String getInstanceId() {
		return instanceId;
	}
	protected boolean isRetryable() {
		if(getAvailable() == Long.MAX_VALUE)
			return false;
		long exp = getExpiration(getPosted());
		if(exp <= 0)
			return true;
		if(exp <= getAvailable())
			return false;
		if(exp <= System.currentTimeMillis())
			return false;
		return true;
	}
	public String getSubscription() {
		return subscription;
	}
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	public Readiness getReadiness() {
		return corrControl == null || correlation == null || !corrControl.getLimited().contains(correlation) ? Readiness.READY : Readiness.LAST;
	}

	// NOTE: we assume that correlation has not changed since receive()
	public void acknowledge(boolean success) {
		if(corrControl != null && correlation != null)
			try {
				corrControl.decrement(correlation);
			} catch(IllegalStateException e) {
				Log.getLog().warn("Could not decrement correlation control; continuing anyway", e);
			}
	}

	public void receive() {
		if(corrControl != null && correlation != null)
			corrControl.increment(correlation);
	}
	public abstract boolean markInprocess() ;
	public abstract boolean isDbMessage() ;
	public abstract boolean isHybrid() ;
	public abstract void markComplete(ConnectionGroup connGroup) throws SQLException, DataLayerException ;

	public String[] getUnblocks() {
		return unblocks;
	}

	public void setUnblocks(String[] unblocks) {
		this.unblocks = unblocks;
	}

	@Override
	public void unblock(String... blockKeys) {
		if(blockKeys != null && blockKeys.length > 0) {
			if(unblocks != null && unblocks.length > 0) {
				String[] tmp = new String[blockKeys.length + unblocks.length];
				System.arraycopy(unblocks, 0, tmp, 0, unblocks.length);
				System.arraycopy(blockKeys, 0, tmp, unblocks.length, blockKeys.length);
				blockKeys = tmp;
			}
			setUnblocks(blockKeys);
		}
	}
}