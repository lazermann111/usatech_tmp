/**
 * 
 */
package simple.mq.peer;

import java.security.NoSuchAlgorithmException;

import simple.bean.ConvertUtils;
import simple.text.Hasher;
import simple.text.StringUtils;

public class QueueKey {
	protected final String queueName;
	protected final String subscription;
	protected final int hash;
	protected final String desc;
	public QueueKey(String queueName, String subscription) {
		this(queueName, subscription, 0);
	}
	protected QueueKey(String queueName, String subscription, int hashBase) {
		this.queueName = queueName;
		this.subscription = subscription;
		this.hash = queueName.hashCode() + (subscription == null ? 0 : 31 * subscription.hashCode()) + 31 * 31 * hashBase;	
		StringBuilder sb = new StringBuilder();
		sb.append(queueName);
		if(subscription != null)
			sb.append(" (").append(subscription).append(')');
		this.desc = sb.toString();
	}
		
	public String getQueueName() {
		return queueName;
	}
	public String getSubscription() {
		return subscription;
	}
	public String getDataSourceName() {
		return null;
	}
	@Override
	public int hashCode() {
		return hash;
	}
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(!(obj instanceof QueueKey))
			return false;
		QueueKey qk = (QueueKey)obj;
		if(qk.hash != hash)
			return false;
		if(!qk.queueName.equals(queueName))
			return false;
		if(!ConvertUtils.areEqual(qk.subscription, subscription))
			return false;
		return ConvertUtils.areEqual(qk.getDataSourceName(), getDataSourceName());
	}

	@Override
	public String toString() {
		return desc;
	}
	protected static final Hasher md5;
	static {
		Hasher h;
		try {
			h = new Hasher("MD5", false);
		} catch(NoSuchAlgorithmException e) {
			System.err.println("Could not instantiate MD5 hash digest");
			e.printStackTrace();
			h = null;
		}
		md5 = h;
	}
	
	public String getQueueTable(String dataSourceName) {
		StringBuilder sb = new StringBuilder();
		sb.append("Q_");
		if(dataSourceName != null) {
			appendToQueueTable(sb, dataSourceName.toUpperCase());
			sb.append('$');
		}
		appendToQueueTable(sb, queueName.toUpperCase());
		if(subscription != null) {
			sb.append('$');
			appendToQueueTable(sb, subscription.toUpperCase());
		}
		if(sb.length() > 62) {
			byte[] bytes = md5.hashRaw(sb.substring(53).getBytes());
			sb.setLength(53);
			sb.append('_');
			sb.append(StringUtils.toHex(bytes, 0, 4).toLowerCase());
		}
		return sb.toString();
	}
	protected static void appendToQueueTable(StringBuilder sb, String part) {
		for(int i  = 0; i < part.length(); i++) {
			char ch = part.charAt(i);
			if(Character.isLetterOrDigit(ch) || ch == '_')
				sb.append(ch);
			else
				sb.append('_');
		}
	}
}