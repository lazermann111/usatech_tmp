package simple.mq.peer;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.management.ManagementFactory;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.postgresql.PGConnection;
import org.postgresql.copy.CopyIn;
import org.postgresql.copy.CopyOut;

import simple.app.RetryPolicy;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.ConcurrentDataSourceAvailability;
import simple.db.ConnectionGroup;
import simple.db.DBUnwrap;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceAvailability;
import simple.db.DataSourceNotFoundException;
import simple.db.DbUtils;
import simple.db.DefaultConnectionGroup;
import simple.db.StringCharacterStream;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.IOUtils;
import simple.io.LoadingException;
import simple.io.Log;
import simple.mq.ConsumedMessage;
import simple.mq.Message;
import simple.mq.MessagingException;
import simple.mq.Supervisor;
import simple.mq.Worker;
import simple.results.Results;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.AbstractLookupMap;
import simple.util.MapBackedSet;
import simple.util.SortedStats;
import simple.util.concurrent.Cache;
import simple.util.concurrent.CachingCorrelationControl;
import simple.util.concurrent.ConcurrentQueueBuffer;
import simple.util.concurrent.CorrelationControl;
import simple.util.concurrent.CustomThreadFactory;
import simple.util.concurrent.FutureCache;
import simple.util.concurrent.LockSegmentCache;
import simple.util.concurrent.LockSegmentFutureCache;
import simple.util.concurrent.QueueBuffer;
import simple.util.concurrent.QueueBufferFactory;
import simple.util.concurrent.Readiness;

public class PeerSupervisor implements Supervisor, PeerSupervisorMXBean {
	private static final Log log = Log.getLog();
	
	protected final String instanceId = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':' + Long.toHexString(Double.doubleToLongBits(Math.random())).toUpperCase();
	protected String clusterName = "Unknown";
	protected String dataSourceNamePrefix = null;
	protected String[] secondaryDataSourceNames;
	protected long carryoverCleanupInterval = 120000L;
	protected long queueSizeRefreshInterval = 120000L;
	protected long expiredInstanceCleanupInterval = 70 * 1000L;
	protected long lockRenewalInterval = 30 * 1000L;
	protected long cleanupShadowBlocksInterval = 2 * 60 * 60 * 1000L;
	protected int lockRenewalRetryAttempts = 5;
	protected long defaultNotificationPollFrequency = 50L;
	protected long defaultDataPollFrequency = 5000L;
	protected long secondaryReconnectInterval = 15000L;
	protected long primaryReconnectInterval = 5000L;
	protected long defaultCleanupInterval = 120000L;
	protected int defaultQueueBufferSize = 100;
	protected int defaultAnalyzeAfterCount = 10000;
	protected float defaultPollOnPercentEmpty = 0.5f;
	protected PollType defaultPollType = PollType.DB;
	protected long defaultDirectPublishTimeout = 1000L;
	protected int defaultDirectPublishAttempts = 5;
	protected RedeliveryDetection defaultRedeliveryDetection = RedeliveryDetection.NONE;
	protected PublishType defaultDurablePublishType = PublishType.DB;
	protected PublishType defaultTemporaryPublishType = PublishType.DIRECT_OR_DB;
	protected int lockRenewalThreadPriority = 7;
	protected long lockRenewalTimeBuffer = 5000L;
	protected boolean legacyRequestSent = false;
	protected final Cache<String, DirectPollerConfig, RuntimeException> directPollerConfigs = new LockSegmentCache<String, DirectPollerConfig, RuntimeException>() {
		@Override
		protected DirectPollerConfig createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new DirectPollerConfig();
		}
	};
	
	protected final AtomicReference<DirectPeerProducer> directProducerRef = new AtomicReference<DirectPeerProducer>();
	protected static final DataLayer dataLayer = new DataLayer();
	protected final RetryPolicy defaultRetryPolicy = new RetryPolicy(18, 2000, 2.0f);
	protected final ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(2, new CustomThreadFactory("DBMQ-Cleanup-", true, 3));
	protected boolean jmxEnabled = true;
	protected boolean multiplexingEnabled = true;
	protected final Set<Poller> multiplexingDirectPollers = new LinkedHashSet<Poller>();	
	protected final Set<HybridDirectPoller> multiplexingHybridPollers = new LinkedHashSet<HybridDirectPoller>();	//we may be able to combine these
	protected MultiHybridPollerAssistant multiplexingHybridAssistant;
	protected int multiplexingDirectPollerInstances;
	protected int multiplexingHybridPollerInstances;
	protected QueueBufferFactory<ConsumedPeerMessage> queueBufferFactory;
	protected ObjectName jmxObjectName;
	protected static final String jmxNamePrefix = PeerSupervisor.class.getPackage().getName() + ":Type=" + PeerSupervisor.class.getSimpleName();
	protected final List<DataSourceEntry> dataSourceEntries = new ArrayList<DataSourceEntry>();
	protected final Map<String, DataSourceEntry> dataSourceNames = new LinkedHashMap<String, DataSourceEntry>();
	protected final Cache<String, SortedStats<String, Double>, RuntimeException> dbPublishPreferredStats = new LockSegmentCache<String, SortedStats<String, Double>, RuntimeException>() {
		@Override
		protected SortedStats<String, Double> createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new SortedStats<String, Double>(new ConcurrentHashMap<String, Double>(16, 0.75f, 4));
		}
	};
	
	protected final Map<String, String> preferredDataSources = new AbstractLookupMap<String, String>() {
		@Override
		public String get(Object key) {
			if(getQueueSizeRefreshInterval() > 0 && key instanceof String) {
				SortedStats<String, Double> ss = dbPublishPreferredStats.getIfPresent((String) key);
				if(ss != null)
					return ss.toString();
			}
			return null;
		}

		@Override
		public Set<String> keySet() {
			if(getQueueSizeRefreshInterval() > 0)
				return dbPublishPreferredStats.keySet();
			return Collections.emptySet();
		}
	};
	
	protected static final Comparator<DataSourceEntry> dataSourceComparator = new Comparator<DataSourceEntry>() {
		public int compare(DataSourceEntry o1, DataSourceEntry o2) {
			if(o1.getPriority() > o2.getPriority())
				return -1;
			if(o1.getPriority() < o2.getPriority())
				return 1;
			if(o1.isPrimary() && !o2.isPrimary())
				return -1;
			if(!o1.isPrimary() && o2.isPrimary())
				return 1;
			if(o1.getName() == o2.getName())
				return 0;
			if(o1.getName() == null)
				return -1;
			if(o2.getName() == null)
				return 1;
			return o1.getName().compareToIgnoreCase(o2.getName());
		}
	};
	
	protected final FutureCache<QueueKey, QueueHandler, ExecutionException> queueHandlers = new LockSegmentFutureCache<QueueKey, QueueHandler>(100, 0.75f, 16) {
		@Override
		protected QueueHandler createValue(QueueKey key, Object... additionalInfo) throws Exception {
			return createQueueHandler(key);
		}
		protected void resetValue(QueueKey key, QueueHandler value) {
			closeQueueHandler(value);
		}
		public long getExpireTime() {
			return 0;
		}
	};
	
	protected final AtomicBoolean wholeStopped = new AtomicBoolean(false);
	protected final DataSourceAvailability dataSourceAvailability = new ConcurrentDataSourceAvailability() {
		@Override
		protected void availabilityChanged(String dataSourceName, boolean available) {
			if(available) {
				DataSourceEntry dse = dataSourceNames.get(dataSourceName);
				if(dse == null) {
					log.error("Data Source Entry was not found for '" + dataSourceName + "'", new ServiceException());
					return;
				}
				dse.startRenewLock();
				cleanupExpiredLocks(dataSourceName);
				cleanupCarryover(dataSourceName);
			}
		}
	};

	protected class DataSourceEntry {
		protected String name;
		protected int priority;
		protected boolean primary;
		protected final ReentrantLock lock = new ReentrantLock();
		protected final Condition signal = lock.newCondition();
		protected Thread renewer;
		
		public DataSourceEntry(String name, int priority, boolean primary) {
			this.name = name;
			this.priority = priority;
			this.primary = primary;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getPriority() {
			return priority;
		}

		public void setPriority(int value) {
			this.priority = value;
		}
		
		public boolean isPrimary() {
			return primary;
		}

		public void setPrimary(boolean primary) {
			this.primary = primary;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof DataSourceEntry))
				return false;
			DataSourceEntry dse = (DataSourceEntry)obj;
			return name.equals(dse.name) && priority == dse.priority && primary == dse.primary;
		}
		
		@Override
		public int hashCode() {
			return name.hashCode();
		}

		public void startRenewLock() {
			lock.lock();
			try {
				if(renewer == null) {
					renewer = new Thread("DBMQ-Renewer-" + name) {
						@Override
						public void run() {
							lock.lock();
							try {
								long expiredTime = 0;
								int retryAttempts = getLockRenewalRetryAttempts();
								while(!wholeStopped.get()) {
									if(dataSourceAvailability.isAvailable(name)) {
										long duration = getLockRenewalInterval();
										long newExpiredTime;
										lock.unlock();
										try {
											newExpiredTime = renewLock(name, duration, instanceId);
											if(newExpiredTime <= 0 && retryAttempts-- > 0)
												continue; // retry immediately
											if(expiredTime > 0 && expiredTime < newExpiredTime - duration)
												log.error("Did not renew lock for " + name + " before it expired; Another application may have acquired ths applications queues for a brief time", new TimeoutException());
											retryAttempts = getLockRenewalRetryAttempts();
											expiredTime = newExpiredTime;
										} finally {
											lock.lock();
										}
										try {
											signal.awaitUntil(new Date(newExpiredTime - getLockRenewalTimeBuffer()));
										} catch(InterruptedException e) {
											// ignore
										}
									} else {
										try {
											signal.await();
										} catch(InterruptedException e) {
											// ignore
										}
									}
								}
							} finally {
								lock.unlock();
							}
						}
					};
					renewer.setDaemon(true);
					renewer.setPriority(getLockRenewalThreadPriority());
					renewer.start();
				} else {
					signal.signal();
				}
			} finally {
				lock.unlock();
			}
		}
	}
	
	protected class QueueSettingsImpl implements QueueSettings {
		protected Long dataPollFrequency;
		protected Float pollOnPercentEmpty;
		protected Integer queueBufferSize;
		protected Long notificationPollFrequency;
		protected Long cleanupInterval;
		protected PollType pollType;
		protected Integer primaryDirectPollers;
		protected Integer secondaryDirectPollers;
		protected Long directPublishTimeout;
		protected Integer directPublishAttempts;
		protected RedeliveryDetection redeliveryDetection;
		protected PublishType durablePublishType;
		protected PublishType temporaryPublishType;
		protected Integer analyzeAfterCount;
		protected int limitPerCorrelation;
		protected CachingCorrelationControl<Long> correlationControl;
		
		public long getDataPollFrequency() {
			return dataPollFrequency != null ? dataPollFrequency : getDefaultDataPollFrequency();
		}

		public void setDataPollFrequency(long dataPollFrequency) {
			this.dataPollFrequency = dataPollFrequency == -1L ? null : dataPollFrequency;
		}

		public float getPollOnPercentEmpty() {
			return pollOnPercentEmpty != null ? pollOnPercentEmpty : getDefaultPollOnPercentEmpty();
		}

		public void setPollOnPercentEmpty(float pollOnPercentEmpty) {
			this.pollOnPercentEmpty = pollOnPercentEmpty < 0.0f ? null : pollOnPercentEmpty;
		}
		
		public int getQueueBufferSize() {
			return queueBufferSize != null ? queueBufferSize : getDefaultQueueBufferSize();
		}

		public void setQueueBufferSize(int queueBufferSize) {
			this.queueBufferSize = queueBufferSize == -1 ? null : queueBufferSize;
		}

		public long getNotificationPollFrequency() {
			return notificationPollFrequency != null ? notificationPollFrequency : getDefaultNotificationPollFrequency();
		}

		public void setNotificationPollFrequency(long notificationPollFrequency) {
			this.notificationPollFrequency = notificationPollFrequency == -1L ? null : notificationPollFrequency;
		}
		
		public long getCleanupInterval() {
			return cleanupInterval != null ? cleanupInterval : getDefaultCleanupInterval();
		}

		public void setCleanupInterval(long cleanupInterval) {
			this.cleanupInterval = cleanupInterval == -1L ? null : cleanupInterval;
		}

		public PollType getPollType() {
			return pollType != null ? pollType : getDefaultPollType();
		}
		
		public void setPollType(PollType pollType) {
			this.pollType = pollType;
		}
		
		public long getDirectPublishTimeout() {
			return directPublishTimeout != null ? directPublishTimeout : getDefaultDirectPublishTimeout();
		}

		public void setDirectPublishTimeout(long directPublishTimeout) {
			this.directPublishTimeout = directPublishTimeout == -1L ? null : directPublishTimeout;
		}

		public int getDirectPublishAttempts() {
			return directPublishAttempts != null ? directPublishAttempts : getDefaultDirectPublishAttempts();
		}

		public void setDirectPublishAttempts(int directPublishAttempts) {
			this.directPublishAttempts = directPublishAttempts == -1 ? null : directPublishAttempts;
		}
		public RedeliveryDetection getRedeliveryDetection() {
			return redeliveryDetection != null ? redeliveryDetection : getDefaultRedeliveryDetection();
		}
		
		public void setRedeliveryDetection(RedeliveryDetection redeliveryDetection) {
			this.redeliveryDetection = redeliveryDetection;
		}
		
		public PublishType getDurablePublishType() {
			return durablePublishType != null ? durablePublishType : getDefaultDurablePublishType();
		}
		
		public void setDurablePublishType(PublishType durablePublishType) {
			this.durablePublishType = durablePublishType;
		}
		
		public PublishType getTemporaryPublishType() {
			return temporaryPublishType != null ? temporaryPublishType : getDefaultTemporaryPublishType();
		}
		
		public void setTemporaryPublishType(PublishType temporaryPublishType) {
			this.temporaryPublishType = temporaryPublishType;
		}
		
		public int getAnalyzeAfterCount() {
			return analyzeAfterCount != null ? analyzeAfterCount : getDefaultAnalyzeAfterCount();
		}

		public void setAnalyzeAfterCount(int analyzeAfterCount) {
			this.analyzeAfterCount = analyzeAfterCount == -1 ? null : analyzeAfterCount;
		}

		public int getLimitPerCorrelation() {
			return limitPerCorrelation;
		}

		public void setLimitPerCorrelation(int limitPerCorrelation) {
			int old = this.limitPerCorrelation;
			this.limitPerCorrelation = limitPerCorrelation;
			if(old != limitPerCorrelation) {
				if(limitPerCorrelation > 0) {
					if(correlationControl == null)
						correlationControl = new CachingCorrelationControl<Long>();
					correlationControl.setLimit(limitPerCorrelation);
				} else if(correlationControl != null)
					correlationControl = null;
			}
		}

		public CorrelationControl<Long> getCorrelationControl() {
			return correlationControl;
		}

		@Override
		public boolean isLegacyRequestSent() {
			return legacyRequestSent;
		}
	}

	protected boolean exactQueueSettings = false;
	protected final ConcurrentNavigableMap<String, QueueSettingsImpl> queueSettingsCache = new ConcurrentSkipListMap<String, QueueSettingsImpl>();

	protected static enum QueueState {
		INITIAL, PAUSED, UNPAUSED
	};
	
	protected class QueueHandler implements PeerQueueMXBean {
		protected final QueueKey queueKey;
		protected final int dbPollSize;
		protected final int maxPending;
		protected final Collection<Poller> pollers;
		protected final QueueBuffer<ConsumedPeerMessage> queueBuffer;
		protected final Set<Object> consumers = new MapBackedSet<Object>(new ConcurrentHashMap<Object, Object>());
		protected final AtomicInteger consumerCount = new AtomicInteger();
		protected final PollType pollType;
		protected ObjectName jmxObjectName;
		protected final ReentrantLock lock = new ReentrantLock();
		protected QueueState state = QueueState.PAUSED;
		
		public QueueHandler(QueueKey queueKey, int dbPollSize, int directMaxPending, QueueBuffer<ConsumedPeerMessage> queueBuffer, Collection<Poller> pollers, PollType pollType, ObjectName jmxObjectName) {
			this.queueKey = queueKey;
			this.dbPollSize = dbPollSize;
			this.maxPending = directMaxPending;
			this.queueBuffer = queueBuffer;
			this.pollers = pollers;
			this.pollType = pollType;

			if(jmxObjectName != null) {
				try {
					ManagementFactory.getPlatformMBeanServer().registerMBean(this, jmxObjectName);
					this.jmxObjectName = jmxObjectName;
				} catch(InstanceAlreadyExistsException e) {
					log.warn("Could not register queue '" + queueKey.getQueueName() + "' in JMX", e);
				} catch(MBeanRegistrationException e) {
					log.warn("Could not register queue '" + queueKey.getQueueName() + "' in JMX", e);
				} catch(NotCompliantMBeanException e) {
					log.warn("Could not register queue '" + queueKey.getQueueName() + "' in JMX", e);
				} catch(NullPointerException e) {
					log.warn("Could not register queue '" + queueKey.getQueueName() + "' in JMX", e);
				}
			}
		}

		public int registerConsumer(Object consumer) {
			if(consumers.add(consumer)) {
				int cnt = consumerCount.incrementAndGet();
				if(cnt == 1)
					checkState();
				return cnt;
			} else
				return -1;
		}
		
		public int deregisterConsumer(Object consumer) {
			if(consumers.remove(consumer)) {
				int cnt = consumerCount.decrementAndGet();
				if(cnt == 0)
					checkState();
				return cnt;
			} else
				return -1;
		}
		
		public ConsumedPeerMessage nextMessage() throws InterruptedException {
			return queueBuffer.take();
		}
		
		public void handleExpiredMessage(ConsumedPeerMessage message) {
			message.acknowledge(false); // allow deletion
			//NOTE: We don't need to bother deleting this message here, we will let the scheduled clean-up task handle that
			//deleteExpiredMessage(message, message.getPrimaryDataSourceName());
			//deleteExpiredMessage(message, message.getSecondaryDataSourceName());			
		}
		
		/*
		protected void deleteExpiredMessage(PeerMessageImpl message, String dsn) {
			try {
				BatchUpdate bu = expiredMessageDeleteBatches.getOrCreate(dsn);
				bu.addBatch(message);
				if(bu.getPendingCount() >= getQueueBufferSize(message.getQueueName())) {
					Connection conn = dataLayer.getConnection(dsn, true);
					try {
						bu.executeBatch(conn);
					} catch(SQLException e) {
						log.warn("Could not delete expired messages in batch for " + dsn, e);
					} finally {
						DbUtils.closeSafely(conn);
					}
				}
			} catch(ParameterException e) {
				log.warn("Could not add expired message to delete batch", e);
			} catch(DataLayerException e) {
				log.warn("Could not add expired message to delete batch", e);
			} catch(SQLException e) {
				log.warn("Could not get connection to delete expired messages in " + dsn, e);
			}
		}*/
		
		public String getQueueName() {
			return queueKey.getQueueName();
		}
		
		public String getSubscription() {
			return queueKey.getSubscription();
		}
		
		public long getDataPollFrequency() {
			return PeerSupervisor.this.getDataPollFrequency(getQueueName());
		}
		
		public int getQueueBufferCapacity() {
			return queueBuffer.getCapacity() + dbPollSize;
		}
		
		public int getQueueBufferItems() {
			return queueBuffer.size();
		}
		
		public int getQueueBufferThreshhold() {
			return queueBuffer.getCapacity();
		}

		protected CorrelationControl<Long> getCorrelationControl() {
			return getQueueSettings(getQueueName()).getCorrelationControl();
		}

		public String getCorrelationControlInfo() {
			CorrelationControl<Long> cc = getCorrelationControl();
			return cc == null ? null : cc.getCountInfo();
		}
		
		public void close() {
			if(jmxObjectName != null) {
				try {
					ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmxObjectName);
				} catch(InstanceNotFoundException e) {
					log.warn("Could not unregister queue '" + getQueueName() + "' in JMX", e);
				} catch(MBeanRegistrationException e) {
					log.warn("Could not unregister queue '" + getQueueName() + "' in JMX", e);
				} catch(NullPointerException e) {
					log.warn("Could not unregister queue '" + getQueueName() + "' in JMX", e);
				}
				jmxObjectName = null;
			}
			for(Poller poller : pollers) {
				if(multiplexingDirectPollers.contains(poller))
					poller.updateConcurrency(queueKey.getQueueName(), queueKey.getSubscription(), 0);
				else if(!multiplexingHybridPollers.contains(poller))
					poller.shutdown();
			}
		}
		
		public PollType getPollType() {
			return pollType;
		}

		public boolean isPaused() {
			switch(state) {
				case PAUSED:
					return true;
				default:
					return false;
			}
		}

		protected void checkState() {
			lock.lock();
			try {
				boolean hasConsumers = consumerCount.get() > 0;
				switch(state) {
					case PAUSED:
						return; // don't change
					case INITIAL:
						if(hasConsumers) {
							sendConcurrency();
							state = QueueState.UNPAUSED;
						}
						break;
					case UNPAUSED:
						if(!hasConsumers) {
							sendNoConcurrency();
							state = QueueState.INITIAL;
						}
						break;
				}
			} finally {
				lock.unlock();
			}
		}

		public void pause() {
			lock.lock();
			try {
				switch(state) {
					case INITIAL:
						state = QueueState.PAUSED;
						break;
					case UNPAUSED:
						sendNoConcurrency();
						state = QueueState.PAUSED;
						break;
				}
			} finally {
				lock.unlock();
			}
		}

		public void unpause() {
			lock.lock();
			try {
				boolean hasConsumers = consumerCount.get() > 0;
				switch(state) {
					case PAUSED:
					case INITIAL:
						if(hasConsumers) {
							sendConcurrency();
							state = QueueState.UNPAUSED;
						} else
							state = QueueState.INITIAL;
						break;
				}
			} finally {
				lock.unlock();
			}
		}

		protected void sendConcurrency() {
			for(Poller poller : pollers) {
				if(poller instanceof DbPoller)
					poller.updateConcurrency(queueKey.getQueueName(), queueKey.getSubscription(), dbPollSize);
				else if(poller instanceof DirectPoller)
					poller.updateConcurrency(queueKey.getQueueName(), queueKey.getSubscription(), Math.max(maxPending * ((DirectPoller) poller).getMaxPendingPercent(), 1));
				else
					poller.updateConcurrency(queueKey.getQueueName(), queueKey.getSubscription(), maxPending);
			}
		}

		protected void sendNoConcurrency() {
			for(Poller poller : pollers) {
				poller.updateConcurrency(queueKey.getQueueName(), queueKey.getSubscription(), 0);
			}
		}

		public String getPollerInfo() {
			StringBuilder sb = new StringBuilder();
			for(Poller poller : pollers)
				sb.append(poller.toString()).append("\r\n");
			return sb.toString();
		}
	}

	static {
		dataLayer.setDataSourceFactory(DataLayerMgr.getDataSourceFactory());
		try {
			ConfigLoader.loadConfig("resource:/simple/mq/peer/mq-peer-data-layer.xml", dataLayer);
		} catch(ConfigException e) {
			log.error("Could not load data-layer config file", e);
		} catch(IOException e) {
			log.error("Could not load data-layer config file", e);
		} catch(LoadingException e) {
			log.error("Could not load data-layer config file", e);
		}
	}
	
	/*
	protected class SingleHybridDbPoller extends SingleDbPoller {
		protected final ObjectName jmxParentObjectName;
		protected final ReentrantLock pollersLock = new ReentrantLock();
		protected boolean paused = true;
		protected Map<SocketAddress,List<Poller>> hybridDirectPollerMap = new HashMap<SocketAddress,List<Poller>>();
		protected Number consumerCount;
		public SingleHybridDbPoller(DbPollerAssistant assistant, String dataSourceName, String queueName, String subscription, boolean primary, int pollSize, ObjectName jmxParentObjectName) {
			super(assistant, dataSourceName, queueName, subscription, primary, pollSize);
			this.jmxParentObjectName = jmxParentObjectName;
		}
		
		@Override
		protected void exitingPollLoop() {
			pollersLock.lock();
			try {
				for(List<Poller> pollers :  hybridDirectPollerMap.values()) {
					Iterator<Poller> iter = pollers.iterator();
					while(iter.hasNext()) {
						Poller poller = iter.next();
						poller.shutdown();
						iter.remove();
					}
				}
			} finally {
				pollersLock.unlock();
			}
		}
		@Override
		protected void handlePollNotSatisfied(int rowsFound, Connection pollerConnection, PGConnection notificationConnection, Validity inProcessValidity, long retrieved) {
			assistant.setRetrieved(retrieved);
			pollersLock.lock();
			try {
				if(paused) {
					paused = false;
					for(List<Poller> pollers :  hybridDirectPollerMap.values()) {
						Iterator<Poller> iter = pollers.iterator();
						while(iter.hasNext()) {
							Poller poller = iter.next();
							poller.updateConcurrency(queueName, subscription, consumerCount);
						}
					}
				}
				Set<SocketAddress> configSocketAddresses = new HashSet<SocketAddress>(hybridDirectPollerMap.keySet());
				for(DirectPollerConfig config : directPollerConfigs.values()) {
					if(config.getUrl() != null && config.isPollingQueueName(queueName)) {
						int target = config.getPollerThreads(queueName);
						List<Poller> pollers =  hybridDirectPollerMap.get(config.getSocketAddress());
						if(pollers == null) {
							pollers = new ArrayList<Poller>();
							hybridDirectPollerMap.put(config.getSocketAddress(), pollers);
						}
						if(target < pollers.size()) {
							//remove some
							Iterator<Poller> iter = pollers.listIterator(target);
							while(iter.hasNext()) {
								Poller poller = iter.next();
								poller.shutdown();
								iter.remove();
							}
						} else if(target > pollers.size()) {
							// add some
							for(int i = pollers.size(); i < target; i++) {
								Poller poller = new HybridDirectPoller(assistant, queueName, config, jmxParentObjectName);
								pollers.add(poller);
								poller.updateConcurrency(queueName, subscription, consumerCount);
								poller.start();
							}
						}
						configSocketAddresses.remove(config.getSocketAddress());
					}
				}
				// Remove those for which there is not longer a config
				for(SocketAddress socketAddress : configSocketAddresses) {
					List<Poller> pollers =  hybridDirectPollerMap.remove(socketAddress);
					Iterator<Poller> iter = pollers.iterator();
					while(iter.hasNext()) {
						Poller poller = iter.next();
						poller.shutdown();
						iter.remove();
					}
				}
			} finally {
				pollersLock.unlock();
			}
			super.handlePollNotSatisfied(rowsFound, pollerConnection, notificationConnection, inProcessValidity, retrieved);
		}

		public boolean updateLastCount(String queueName, String subscription, int count) {
			return true;
		}
	}
	*/
	
	protected QueueSettingsImpl getQueueSettings(String queueName, boolean exact) {
		if(exact) {
			QueueSettingsImpl qsi = queueSettingsCache.get(queueName);
			if(qsi == null) {
				qsi = new QueueSettingsImpl();
				QueueSettingsImpl old = queueSettingsCache.putIfAbsent(queueName, qsi);
				return old == null ? qsi : old;
			}
			return qsi;
		}
		
		Map.Entry<String, QueueSettingsImpl> closest = queueSettingsCache.floorEntry(queueName);
		while(closest != null) {
			if(queueName.startsWith(closest.getKey()))
				return closest.getValue();
			if(closest.getKey().length() < queueName.length())
				break; // didn't find one
			closest = queueSettingsCache.lowerEntry(closest.getKey());
		}
		
		QueueSettingsImpl qsi = new QueueSettingsImpl();
		QueueSettingsImpl old = queueSettingsCache.putIfAbsent(queueName, qsi);
		return old == null ? qsi : old;
	}
	
	public long getDataPollFrequency(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getDataPollFrequency();
	}
	
	public void setDataPollFrequency(String queueName, long dataPollFrequency) {
		getQueueSettings(queueName, true).setDataPollFrequency(dataPollFrequency);
	}
	
	public int getQueueBufferSize(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getQueueBufferSize();
	}
	
	public void setQueueBufferSize(String queueName, int queueBufferSize) {
		getQueueSettings(queueName, true).setQueueBufferSize(queueBufferSize);
	}
	
	public float getPollOnPercentEmpty(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getPollOnPercentEmpty();
	}
	
	public void setPollOnPercentEmpty(String queueName, float pollOnPercentEmpty) {
		getQueueSettings(queueName, true).setPollOnPercentEmpty(pollOnPercentEmpty);
	}
	
	public long getNotificationPollFrequency(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getNotificationPollFrequency();
	}
	
	public void setNotificationPollFrequency(String queueName, long notificationPollFrequency) {
		getQueueSettings(queueName, true).setNotificationPollFrequency(notificationPollFrequency);
	}
	
	public long getCleanupInterval(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getCleanupInterval();
	}
	
	public void setCleanupInterval(String queueName, long cleanupInterval) {
		getQueueSettings(queueName, true).setCleanupInterval(cleanupInterval);
	}
	
	public PollType getPollType(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getPollType();
	}
	
	public void setPollType(String queueName, PollType pollType) {
		getQueueSettings(queueName, true).setPollType(pollType);
	}
	
	public long getDirectPublishTimeout(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getDirectPublishTimeout();
	}
	
	public void setDirectPublishTimeout(String queueName, long directPublishTimeout) {
		getQueueSettings(queueName, true).setDirectPublishTimeout(directPublishTimeout);
	}
	
	public int getDirectPublishAttempts(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getDirectPublishAttempts();
	}
	
	public void setDirectPublishAttempts(String queueName, int directPublishAttempts) {
		getQueueSettings(queueName, true).setDirectPublishAttempts(directPublishAttempts);
	}
	
	public RedeliveryDetection getRedeliveryDetection(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getRedeliveryDetection();
	}
	
	public void setRedeliveryDetection(String queueName, RedeliveryDetection redeliveryDetection) {
		getQueueSettings(queueName, true).setRedeliveryDetection(redeliveryDetection);
	}
	public PublishType getDurablePublishType(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getDurablePublishType();
	}
	
	public void setDurablePublishType(String queueName, PublishType publishType) {
		getQueueSettings(queueName, true).setDurablePublishType(publishType);
	}
	
	public PublishType getTemporaryPublishType(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getTemporaryPublishType();
	}
	
	public void setTemporaryPublishType(String queueName, PublishType publishType) {
		getQueueSettings(queueName, true).setTemporaryPublishType(publishType);
	}
	
	public int getAnalyzeAfterCount(String queueName) {
		return getQueueSettings(queueName, isExactQueueSettings()).getAnalyzeAfterCount();
	}
	
	public void setAnalyzeAfterCount(String queueName, int analyzeAfterCount) {
		getQueueSettings(queueName, true).setAnalyzeAfterCount(analyzeAfterCount);
	}

	protected class PeerWorker implements Worker {
		protected final Collection<ConsumedPeerMessage> consumed = new ArrayList<ConsumedPeerMessage>();
		protected final Collection<PublishedPeerMessage> published = new TreeSet<PublishedPeerMessage>(new Comparator<PublishedPeerMessage>() {
			public int compare(PublishedPeerMessage o1, PublishedPeerMessage o2) {
				if(o1.getPriority() > o2.getPriority())
					return -1;
				if(o1.getPriority() < o2.getPriority())
					return 1;
				if(o1.getMessageSequence() < o2.getMessageSequence())
					return -1;
				if(o1.getMessageSequence() > o2.getMessageSequence())
					return 1;
				return 0;
			}
		});
		
		protected final Collection<String> blocked = new HashSet<String>();
		protected final ReentrantLock lock = new ReentrantLock();

		public ConsumedMessage consume(String queueName, String subscription) throws MessagingException {
			lock.lock();
			try {
				ConsumedPeerMessage message = consumeMessage(queueName, subscription, this);
				if(message != null)
					consumed.add(message);
				return message;
			} finally {
				lock.unlock();
			}
		}

		public void publish(Message message, boolean broadcast, String... blocks) throws MessagingException {
			lock.lock();
			try {
				published.add(new PublishedPeerMessage(message, broadcast, instanceId, blocks));
			} finally {
				lock.unlock();
			}
		}

		public void commit() throws MessagingException {
			lock.lock();
			try {
				try {
					commitWorker(consumed, published, blocked);
				} finally {
					consumed.clear();
					published.clear();
					blocked.clear();
				}
			} finally {
				lock.unlock();
			}
		}
		
		public void rollback(Throwable reason, RetryPolicy retryPolicy) throws MessagingException {
			lock.lock();
			try {
				try {
					rollbackWorker(consumed, reason, retryPolicy);
				} finally {
					consumed.clear();
					published.clear();
					blocked.clear();
				}
			} finally {
				lock.unlock();
			}
		}
		
		public void close() {
			//remove session from each queueHandler it consumed from
			lock.lock();
			try {
				try {
					rollbackWorker(consumed, new IncompleteException("Worker closed before message was processed"), null);
				} catch(MessagingException e) {
					log.warn("Could not rollback session during close", e);
				} finally {
					consumed.clear();
					published.clear();
					blocked.clear();
				}
				for(Iterator<QueueHandler> iter = queueHandlers.values().iterator(); iter.hasNext(); ) {
					QueueHandler queueHandler = iter.next(); 
					if(queueHandler == null || queueHandler.deregisterConsumer(this) == 0) {
						iter.remove();
					}
				}
			} finally {
				lock.unlock();
			}
		}
		
		public boolean cancelConsume(Thread consumingThread) {
			if(consumingThread != null) {
				consumingThread.interrupt();
				return true;
			}
			return false;
		}
		
		@Override
		public void block(String... blocks) throws MessagingException {
			if(blocks != null)
				for(String b : blocks)
					blocked.add(b);
		}
	}
	
	protected class MultiDbPollerAssistant extends AbstractDbPollerAssistant {	
		protected final Cache<QueueKey, AtomicInteger, RuntimeException> consumerCounts = new LockSegmentCache<QueueKey, AtomicInteger, RuntimeException>(100, 0.75f, 16) {
			@Override
			protected AtomicInteger createValue(QueueKey key, Object... additionalInfo) throws RuntimeException {
				return new AtomicInteger();
			}
		};

		public QueueBuffer<ConsumedPeerMessage> getQueueBuffer(String queueName, String subscription) throws IOException {
			QueueHandler qh;
			try {
				qh = queueHandlers.getOrCreate(new QueueKey(queueName, subscription));
			} catch(ExecutionException e) {
				if(e.getCause() instanceof IOException)
					throw (IOException)e.getCause();
				else
					throw new IOException(e);
			}
			return qh.queueBuffer;
		}
	}
	
	protected class SingleDbPollerAssistant extends AbstractDbPollerAssistant {
		protected final String queueName;
		protected final QueueBuffer<ConsumedPeerMessage> queueBuffer;

		public SingleDbPollerAssistant(String queueName, QueueBuffer<ConsumedPeerMessage> queueBuffer) {
			this.queueName = queueName;
			this.queueBuffer = queueBuffer;
		}

		public QueueBuffer<ConsumedPeerMessage> getQueueBuffer(String queueName, String subscription) throws IOException {
			if(!this.queueName.equals(queueName))
				throw new IOException("Queue name mis-match. Expecting '" + this.queueName + "' but received '" + queueName + "'");
			return queueBuffer;
		}
	}
	
	protected abstract class AbstractDbPollerAssistant extends AbstractPollerAssistant implements DbPollerAssistant {		
		public ConnectionGroup createConnectionGroup() {
			return new DefaultConnectionGroup(getDataSourceNames(), dataSourceAvailability, dataLayer);
		}

		public DataLayer getDataLayer() {
			return dataLayer;
		}

		public DataSourceAvailability getDataSourceAvailability() {
			return dataSourceAvailability;
		}

		public Set<String> getDataSourceNames() {
			return getDataSourceSet();
		}

		public String toMessageDataSourceName(String connectionDataSourceName) {
			return determineMessageDataSourceName(connectionDataSourceName);
		}

		public String toConnectionDataSourceName(String messageDataSourceName) {
			return determineSupervisorDataSourceName(messageDataSourceName);
		}
	}
	
	protected abstract class AbstractPollerAssistant implements PollerAssistant {
		public String getInstanceId() {
			return instanceId;
		}
		
		public long getPrimaryReconnectInterval() {
			return primaryReconnectInterval;
		}
		
		public long getSecondaryReconnectInterval() {
			return secondaryReconnectInterval;
		}
		
		public QueueSettings getQueueSettings(String queueName) {
			return PeerSupervisor.this.getQueueSettings(queueName, isExactQueueSettings());
		}
		
		public ScheduledThreadPoolExecutor getScheduler() {
			return scheduler;
		}
	}
	
	protected class SingleDirectPollerAssistant extends AbstractPollerAssistant implements PollerAssistant {
		protected final String queueName;
		protected final QueueBuffer<ConsumedPeerMessage> queueBuffer;

		public SingleDirectPollerAssistant(String queueName, QueueBuffer<ConsumedPeerMessage> queueBuffer) {
			this.queueName = queueName;
			this.queueBuffer = queueBuffer;
		}

		public QueueBuffer<ConsumedPeerMessage> getQueueBuffer(String queueName, String subscription) throws IOException {
			if(!this.queueName.equals(queueName))
				throw new IOException("Queue name mis-match. Expecting '" + this.queueName + "' but received '" + queueName + "'");
			return queueBuffer;
		}
	}
	
	protected static ConcurrencyUpdate updateMultiConcurrency(Cache<QueueKey, AtomicInteger, RuntimeException> consumerCounts, String queueName, String subscription, int concurrency) {
		AtomicInteger stored;
		if(concurrency > 0) {
			stored = consumerCounts.getOrCreate(new QueueKey(queueName, subscription));
		} else {
			stored = consumerCounts.getIfPresent(new QueueKey(queueName, subscription));
			if(stored == null)
				return ConcurrencyUpdate.NO_CHANGE;
		}
		int old = stored.getAndSet(concurrency);
		if(old == concurrency)
			return ConcurrencyUpdate.NO_CHANGE;
		else if(old <= 0)
			return ConcurrencyUpdate.ACTIVATE;
		else if(concurrency <= 0)
			return ConcurrencyUpdate.DEACTIVATE;
		else
			return ConcurrencyUpdate.UPDATE;
	}
	
	protected class MultiDirectPollerAssistant extends AbstractPollerAssistant implements PollerAssistant {
		protected final Cache<QueueKey, AtomicInteger, RuntimeException> consumerCounts = new LockSegmentCache<QueueKey, AtomicInteger, RuntimeException>(100, 0.75f, 16) {
			@Override
			protected AtomicInteger createValue(QueueKey key, Object... additionalInfo) throws RuntimeException {
				return new AtomicInteger();
			}
		};
		
		public QueueBuffer<ConsumedPeerMessage> getQueueBuffer(String queueName, String subscription) throws IOException {
			QueueHandler qh;
			try {
				qh = queueHandlers.getOrCreate(new QueueKey(queueName, subscription));
			} catch(ExecutionException e) {
				if(e.getCause() instanceof IOException)
					throw (IOException)e.getCause();
				else
					throw new IOException(e);
			}
			return qh.queueBuffer;
		}
	}
	
	protected abstract class AbstractHybridPollerAssistant extends AbstractPollerAssistant implements HybridPollerAssistant {
		protected final DbPollerAssistant subAssistant;
		
		public AbstractHybridPollerAssistant(DbPollerAssistant subAssistant) {
			super();
			this.subAssistant = subAssistant;
		}

		public DbPollerAssistant getDbPollerAssistant() {
			return subAssistant;
		}

		public QueueBuffer<ConsumedPeerMessage> getQueueBuffer(String queueName, String subscription) throws IOException {
			return subAssistant.getQueueBuffer(queueName, subscription);
		}
	}
	
	protected class SingleHybridPollerAssistant extends AbstractHybridPollerAssistant {
		protected HybridDbPoller parentPoller;
		
		public SingleHybridPollerAssistant(DbPollerAssistant subAssistant) {
			super(subAssistant);
		}

		public void deregisterParentPoller(HybridQueueKey queueKey, HybridDbPoller parentPoller) {
			parentPoller = null;
		}

		public HybridDbPoller getParentPoller(HybridQueueKey queueKey) {
			return parentPoller;
		}

		public void registerParentPoller(HybridQueueKey queueKey, HybridDbPoller parentPoller) {
			this.parentPoller = parentPoller;
		}
	}
	
	protected class MultiHybridPollerAssistant extends AbstractHybridPollerAssistant {
		protected final Cache<QueueKey, AtomicInteger, RuntimeException> consumerCounts = new LockSegmentCache<QueueKey, AtomicInteger, RuntimeException>(100, 0.75f, 16) {
			@Override
			protected AtomicInteger createValue(QueueKey key, Object... additionalInfo) throws RuntimeException {
				return new AtomicInteger();
			}
		};
		
		protected final ConcurrentMap<HybridQueueKey,HybridDbPoller> parentPollers = new ConcurrentHashMap<HybridQueueKey, HybridDbPoller>();
		protected final Set<HybridDirectPoller> subPollers;
		public MultiHybridPollerAssistant(DbPollerAssistant subAssistant, Set<HybridDirectPoller> subPollers) {
			super(subAssistant);
			this.subPollers = subPollers;
		}

		public void deregisterParentPoller(HybridQueueKey queueKey, HybridDbPoller parentPoller) {
			parentPollers.remove(queueKey, parentPoller);
		}

		public HybridDbPoller getParentPoller(HybridQueueKey queueKey) {
			return parentPollers.get(queueKey);
		}

		public void registerParentPoller(HybridQueueKey queueKey, HybridDbPoller parentPoller) {
			parentPollers.put(queueKey, parentPoller);
		}

		public Set<HybridDirectPoller> getSubPollers() {
			return subPollers;
		}
	}
	
	protected ConsumedPeerMessage consumeMessage(String queueName, String subscription, PeerWorker session) throws MessagingException {
		ConsumedPeerMessage message;
		QueueHandler queueHandler;
		try {
			queueHandler = queueHandlers.getOrCreate(new QueueKey(queueName, subscription));
			queueHandler.registerConsumer(session);
		} catch(ExecutionException e) {
			throw new MessagingException(e);
		}
		if(queueHandler.isPaused())
			return null;
		long start;
		if(log.isInfoEnabled())
			start = System.currentTimeMillis();
		else
			start = 0;
		while(true) {
			try {
				message = queueHandler.nextMessage();
			} catch(InterruptedException e) {
				return null;
			}
			if(queueHandler.isPaused()) {
				message.acknowledge(false); // so that someone else might retrieve it
				return null;
			}
			long exp = message.getExpiration(message.getPosted());
			long current;
			if(exp > 0 || log.isInfoEnabled())
				current = System.currentTimeMillis();
			else
				current = 0;
			if(exp > 0 && exp < current) {
				if(log.isInfoEnabled()) {
					final StringBuilder sb = new StringBuilder();
					sb.append("Expiring ");
					if(message.isHybrid())
						sb.append("hybrid");
					else if(message.isDbMessage())
						sb.append("db");
					else
						sb.append("direct");
					sb.append(" message '").append(message.getMessageId()).append("' from '").append(determineSupervisorDataSourceName(message.getDataSourceName()))
						.append("' (retryCount=").append(message.getRetryCount()).append("; redelivered=").append(message.isRedelivered())
						.append(") because it is ").append(current-message.getPosted()).append(" ms old");
					log.info(sb.toString());
				}
				queueHandler.handleExpiredMessage(message);
			} else if(message.markInprocess()) {
				if(log.isInfoEnabled()) {
					final StringBuilder sb = new StringBuilder();
					sb.append("Processing new ");
					if(message.isHybrid())
						sb.append("hybrid");
					else if(message.isDbMessage())
						sb.append("db");
					else
						sb.append("direct");
					sb.append(" message '").append(message.getMessageId()).append("' from '").append(determineSupervisorDataSourceName(message.getDataSourceName()))
						.append("' for '").append(queueName)
						.append("' (retryCount=").append(message.getRetryCount()).append("; redelivered=").append(message.isRedelivered())
						.append(") after waiting ").append(current-start).append(" ms");
					log.info(sb.toString());
				}
				return message; // this is a good message, so exit while loop
			} else {
				// we no longer have the same queue lock as when we retrieved the message
				message.acknowledge(false); // so that we might retrieve it again
			}
		}
	}
	
	protected boolean isClosedSafely(Connection conn) {
		try {
			return conn.isClosed();
		} catch(SQLException e) {
			return true;
		}
	}
	
	protected ConnectionGroup createConnectionGroup() {
		return new DefaultConnectionGroup(getDataSourceSet(), dataSourceAvailability, dataLayer);
	}

	protected int addSingleDbPollers(QueueKey queueKey, Collection<Poller> pollerList, QueueBuffer<ConsumedPeerMessage> queueBuffer, ObjectName jmxParentObjectName) {
		DbPollerAssistant assistant = new SingleDbPollerAssistant(queueKey.getQueueName(), queueBuffer);
		int cnt = 0;
		for(DataSourceEntry entry : dataSourceEntries) {
			if(entry.getName() == null)
				continue;
			Poller poller = new DbPoller(assistant, instanceId, entry.getName(), queueKey.getQueueName(), queueKey.getSubscription(), entry.isPrimary(), entry.getPriority(), false);			
			pollerList.add(poller);
			poller.start();
			cnt++;
		}
		return cnt;
	}

	protected int addSingleDirectPollers(QueueKey queueKey, Collection<Poller> pollerList, QueueBuffer<ConsumedPeerMessage> queueBuffer, ObjectName jmxParentObjectName) throws MessagingException {
		if(directPollerConfigs.size() == 0)
			throw new MessagingException("No DirectPollers are configured");
		PollerAssistant assistant = new SingleDirectPollerAssistant(queueKey.getQueueName(), queueBuffer);
		String label = queueKey.getQueueName();
		int cnt = 0;
		for(DirectPollerConfig config : directPollerConfigs.values()) {
			if(config.getUrl() != null && config.isPollingQueueName(queueKey.getQueueName())) {
				int num = config.getPollerThreads(queueKey.getQueueName());
				for(int i = 0; i < num; i++) {
					Poller poller = new DirectPoller(assistant, instanceId, label, config, jmxParentObjectName);
					pollerList.add(poller);
					poller.start();
					cnt++;
				}
			}
		}
		if(cnt == 0)
			throw new MessagingException("No DirectPollers are configured");
		return cnt;
	}

	protected int addSingleHybridDirectPollers(QueueKey queueKey, Collection<HybridDirectPoller> pollers, QueueBuffer<ConsumedPeerMessage> queueBuffer, ObjectName jmxParentObjectName, HybridPollerAssistant assistant) throws MessagingException {
		if(directPollerConfigs.size() == 0)
			throw new MessagingException("No DirectPollers are configured");
		String label = queueKey.getQueueName();
		int cnt = 0;
		for(DirectPollerConfig config : directPollerConfigs.values()) {
			if(config.getUrl() != null && config.isPollingQueueName(queueKey.getQueueName())) {
				int num = config.getPollerThreads(queueKey.getQueueName());
				for(int i = 0; i < num; i++) {
					HybridDirectPoller poller = new HybridDirectPoller(assistant, instanceId, label, config, jmxParentObjectName);
					pollers.add(poller);
					poller.start();
					cnt++;
				}
			}
		}
		if(cnt == 0)
			throw new MessagingException("No DirectPollers are configured");	
		return cnt;
	}
	
	protected int[] addSingleHybridPollers(QueueKey queueKey, Collection<Poller> pollers, QueueBuffer<ConsumedPeerMessage> queueBuffer, ObjectName jmxParentObjectName) throws MessagingException {
		DbPollerAssistant subAssistant = new SingleDbPollerAssistant(queueKey.getQueueName(), queueBuffer);
		int[] cnts = new int[] {0,0};
		for(DataSourceEntry entry : dataSourceEntries) {
			if(entry.getName() == null)
				continue;
			HybridPollerAssistant assistant = new SingleHybridPollerAssistant(subAssistant);
			Collection<HybridDirectPoller> subPollers = new ArrayList<HybridDirectPoller>(directPollerConfigs.size());
			cnts[1] = Math.max(cnts[1], addSingleHybridDirectPollers(queueKey, subPollers, queueBuffer, jmxParentObjectName, assistant));
			Poller poller = new HybridDbPoller(assistant, instanceId, entry.getName(), queueKey.getQueueName(), queueKey.getSubscription(), entry.isPrimary(), entry.getPriority(), subPollers);			
			pollers.add(poller);
			poller.start();
			cnts[0]++;
		}
		return cnts;
	}

	protected int addMultiHybridPollers(QueueKey queueKey, Collection<Poller> pollers, QueueBuffer<ConsumedPeerMessage> queueBuffer, ObjectName jmxParentObjectName) {
		int cnt = 0;
		for(DataSourceEntry entry : dataSourceEntries) {
			if(entry.getName() == null)
				continue;
			Poller poller = new HybridDbPoller(multiplexingHybridAssistant, instanceId, entry.getName(), queueKey.getQueueName(), queueKey.getSubscription(), entry.isPrimary(), entry.getPriority(), multiplexingHybridAssistant.getSubPollers());			
			pollers.add(poller);
			poller.start();
			cnt++;
		}
		return cnt;
	}
	
	protected QueueHandler createQueueHandler(QueueKey queueKey) throws MessagingException {
		if(wholeStopped.get())
			throw new MessagingException("Connection is closed. No further operations are allowed");
		
		ObjectName jmxObjectName = null;
		if(isJmxEnabled()) {
			StringBuilder jmxName = new StringBuilder();
			jmxName.append(jmxNamePrefix).append(",Queue=").append(StringUtils.prepareJMXNameValue(queueKey.getQueueName()));
			if(queueKey.getSubscription() != null)
				jmxName.append(",Subscription=").append(queueKey.getSubscription());
			try {
				jmxObjectName = new ObjectName(jmxName.toString());
			} catch(MalformedObjectNameException e) {
				log.warn("Could not register queue '" + queueKey.getQueueName() + "' in JMX", e);
			} catch(NullPointerException e) {
				log.warn("Could not register queue '" + queueKey.getQueueName() + "' in JMX", e);
			}
		}
		
		Collection<Poller> pollers = new ArrayList<Poller>(secondaryDataSourceNames == null ? 1 : secondaryDataSourceNames.length + 1);
		int size = getQueueBufferSize(queueKey.getQueueName());
		int pollSize = Math.max(1, (int)(getPollOnPercentEmpty(queueKey.getQueueName()) * size));
		int threshhold = Math.max(0, size - pollSize);
		QueueBuffer<ConsumedPeerMessage> queueBuffer = createQueueBuffer(threshhold);
		
		PollType pollType = getPollType(queueKey.getQueueName());
		switch(pollType) {
			case DB:
				addSingleDbPollers(queueKey, pollers, queueBuffer, jmxObjectName);
				break;
			case DIRECT:
				if(!isMultiplexingEnabled())
					addSingleDirectPollers(queueKey, pollers, queueBuffer, jmxObjectName);
				else {
					pollers.addAll(multiplexingDirectPollers);
				}
				break;
			case HYBRID:
				if(!isMultiplexingEnabled()) {
					addSingleHybridPollers(queueKey, pollers, queueBuffer, jmxObjectName);
				} else {
					addMultiHybridPollers(queueKey, pollers, queueBuffer, jmxObjectName);
				}
				break;
			case DB_AND_DIRECT:
				addSingleDbPollers(queueKey, pollers, queueBuffer, jmxObjectName);
				if(!isMultiplexingEnabled())
					addSingleDirectPollers(queueKey, pollers, queueBuffer, jmxObjectName);
				else {
					pollers.addAll(multiplexingDirectPollers);
				}
				break;
			case HYBRID_AND_DIRECT:
				if(!isMultiplexingEnabled()) {
					addSingleDirectPollers(queueKey, pollers, queueBuffer, jmxObjectName);
					addSingleHybridPollers(queueKey, pollers, queueBuffer, jmxObjectName);
				} else {
					pollers.addAll(multiplexingDirectPollers);
					addMultiHybridPollers(queueKey, pollers, queueBuffer, jmxObjectName);
				}
				break;
		}
		
		QueueHandler qh = new QueueHandler(queueKey, pollSize, size, queueBuffer, Collections.unmodifiableCollection(pollers), pollType, jmxObjectName);
		return qh;
	}

	protected QueueBuffer<ConsumedPeerMessage> createQueueBuffer(int threshhold) {
		return new ConcurrentQueueBuffer<ConsumedPeerMessage>(threshhold) {
			@Override
			protected Readiness getReadiness(ConsumedPeerMessage item) {
				return item.getReadiness();
			}
		};
	}

	protected void closeQueueHandler(QueueHandler queueHandler) {
		if(queueHandler == null)
			return;
		queueHandler.close();
	}
	
	public void close() {
		if(wholeStopped.getAndSet(true)) {
			for(Iterator<QueueHandler> iter = queueHandlers.values().iterator(); iter.hasNext(); ) {
				iter.next(); 
				iter.remove();
			}
			synchronized(multiplexingDirectPollers) {
				for(Poller poller : multiplexingDirectPollers)
					poller.shutdown();
				multiplexingDirectPollers.clear();
			}
		}
	}

	public Worker createWorker() throws MessagingException {
		return new PeerWorker();
	}

	protected void rollbackWorker(Collection<ConsumedPeerMessage> consumed, Throwable reason, RetryPolicy retryPolicy) throws MessagingException {
		if(wholeStopped.get())
			throw new MessagingException("Connection is closed. No further operations are allowed");
		
		if(consumed == null || consumed.isEmpty())
			return;
		
		long start = System.currentTimeMillis();
		if(retryPolicy == null)
			retryPolicy = getDefaultRetryPolicy();
		
		final ConnectionGroup connGroup = createConnectionGroup();
		boolean okay = false;
		try {
			for(ConsumedPeerMessage message : consumed) {
				int retryCount = message.getRetryCount();
				long available;
				if(retryCount >= retryPolicy.getMaximumRedeliveries()) {
					available = Long.MAX_VALUE;
				} else {
					long delay = Math.max(0, retryPolicy.calculateDelay(retryCount));
					available = start + delay;
					if(available < start)
						available = Long.MAX_VALUE; // handles overflow of long			
				}
				message.setWritable();
				message.setAvailable(available);
				message.setRetryCount(retryCount + 1);
				if(reason != null)
					message.setErrorMessage(new StringCharacterStream(StringUtils.exceptionToString(reason)));
				if(message.isDbMessage()) {
					if(!message.isTemporary() && message.getShadowDataSourceName() != null)
						try {
							dataLayer.executeCall(connGroup.getConnection(determineSupervisorDataSourceName(message.getShadowDataSourceName()), false), "RETRY_SHADOW_MESSAGE", message);
						} catch(SQLException e) {
							log.warn("Could not update for retry shadow message '" + message.getMessageId() + "' from " + determineSupervisorDataSourceName(message.getShadowDataSourceName()));
							// Consider all carryovers to be re-delivered
						} catch(DataLayerException e) {
							log.warn("Could not update for retry shadow message '" + message.getMessageId() + "' from " + message.getShadowDataSourceName());
							// Consider all carryovers to be re-delivered
						}					
					dataLayer.executeCall(connGroup.getConnection(determineSupervisorDataSourceName(message.getDataSourceName()), false), "RETRY_MESSAGE", message);
				}
				if(log.isInfoEnabled())
					log.info("Rolled back message " + message.getMessageId() + " scheduling a retry " + (available == Long.MAX_VALUE ? "NEVER" : String.valueOf((available - start) / 1000) + " seconds from now"));
			}
			connGroup.commit();
			okay = true;
		} catch(SQLException e) {
			throw new MessagingException(e);
		} catch(DataLayerException e) {
			throw new MessagingException(e);
		} finally {
			connGroup.close(!okay);
			for(ConsumedPeerMessage message : consumed) {
				message.acknowledge(false);
			}
		}
	}

	protected Connection getConnection(String dataSourceName) throws SQLException, DataLayerException {
		try {
			Connection conn = dataLayer.getConnection(dataSourceName);
			dataSourceAvailability.setAvailable(dataSourceName, true);
			return conn;
		} catch(SQLException e) {
			dataSourceAvailability.setAvailable(dataSourceName, false);
			throw e;
		} catch(DataLayerException e) {
			dataSourceAvailability.setAvailable(dataSourceName, false);
			throw e;
		}
	}
	
	protected boolean checkDataSource(String dataSourceName) {
		if(dataSourceAvailability.isAvailable(dataSourceName))
			return true;
		
		try {
			Connection conn = dataLayer.getConnection(dataSourceName);
			DbUtils.closeSafely(conn);
			return true;
		} catch(SQLException e) {
			return false; 
		} catch(DataLayerException e) {
			return false;
		}
	}
	
	public void carryoverQueue(String failedDataSourceName, String queueName, String subscription) throws MessagingException {
		/*
		if(checkDataSource(failedDataSourceName))
			throw new MessagingException("Data Source '" + failedDataSourceName + "' is available. It may not be carried over");
		*/
		internalCarryoverQueue(failedDataSourceName, queueName, subscription);
	}
	
	public void carryoverQueues(String failedDataSourceName) throws MessagingException {
		final ConnectionGroup connGroup = createConnectionGroup();
		boolean okay = false;
		try {
			/*
			if(checkDataSource(failedDataSourceName))
				throw new MessagingException("Data Source '" + failedDataSourceName + "' is available. It may not be carried over");
			*/
			final Map<String,Object> params = new HashMap<String, Object>();
			final Set<String> blocks = new HashSet<String>();
			params.put("dataSourceName", determineMessageDataSourceName(failedDataSourceName));
			for(String dsn : getDataSourceSet()) {
				if(!failedDataSourceName.equalsIgnoreCase(dsn)) {
					final Connection conn = connGroup.getConnection(dsn, true);
					final Results results = dataLayer.executeQuery(conn, "GET_SHADOW_QUEUE_TABLES", params);
					try {
						while(results.next()) {
							String queueName = results.getValue("queueName", String.class);
							String subscription = results.getValue("subscription", String.class);
							internalCarryoverQueue(connGroup, dsn, failedDataSourceName, queueName, subscription);
						}
					} finally {
						results.close();
					}
					dataLayer.executeCall(conn, "GET_SHADOW_BLOCKS", params);
					Object newBlocks = params.get("blocks");
					if(newBlocks != null)
						blocks.addAll(ConvertUtils.asCollection(newBlocks, String.class));
				}
			}
			
			params.clear();
			params.put("blocks", blocks);
			for(String dsn : getDataSourceSet()) {
				if(!failedDataSourceName.equalsIgnoreCase(dsn))
					dataLayer.executeCall(connGroup.getConnection(dsn), "ADD_PRIMARY_BLOCKS", params);
			}
			connGroup.commit();
			okay = true;
		} catch(SQLException e) {
			throw new MessagingException("Could not carry over messages from '" + failedDataSourceName + "'", e);
		} catch(DataLayerException e) {
			throw new MessagingException("Could not carry over messages from '" + failedDataSourceName + "'", e);
		} catch(ConvertException e) {
			throw new MessagingException("Could not carry over messages from '" + failedDataSourceName + "'", e);
		} catch(MessagingException e) {
			log.warn("Could not carry over queues", e);
			throw e;
		} finally {
			connGroup.close(!okay);
		}
	}

	protected void internalCarryoverBlocks(String failedDataSourceName) throws MessagingException {
		final ConnectionGroup connGroup = createConnectionGroup();
		final Set<String> blocks = new HashSet<String>();
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("dataSourceName", failedDataSourceName);
		boolean okay = false;
		try {
			for(String dsn : getDataSourceSet()) {
				if(!failedDataSourceName.equalsIgnoreCase(dsn)) {
					dataLayer.executeCall(connGroup.getConnection(dsn), "GET_SHADOW_BLOCKS", params);
					Object newBlocks = params.get("blocks");
					if(newBlocks != null)
						blocks.addAll(ConvertUtils.asCollection(newBlocks, String.class));
				}
			}
			params.clear();
			params.put("blocks", blocks);
			for(String dsn : getDataSourceSet()) {
				if(!failedDataSourceName.equalsIgnoreCase(dsn))
					dataLayer.executeCall(connGroup.getConnection(dsn), "ADD_PRIMARY_BLOCKS", params);
			}
			connGroup.commit();
			okay = true;
		} catch(SQLException e) {
			throw new MessagingException(e);
		} catch(ConvertException e) {
			throw new MessagingException(e);
		} catch(DataLayerException e) {
			throw new MessagingException(e);
		} finally {
			connGroup.close(!okay);
		}
	}
	
	protected void internalCarryoverQueue(String failedDataSourceName, String queueName, String subscription) throws MessagingException {
		final ConnectionGroup connGroup = createConnectionGroup();
		boolean okay = false;
		try {
			for(String dsn : getDataSourceSet()) {
				if(!failedDataSourceName.equalsIgnoreCase(dsn)) {
					internalCarryoverQueue(connGroup, dsn, failedDataSourceName, queueName, subscription);
				}
			}
			okay = true;
		} finally {
			connGroup.close(!okay);
		}
	}
	
	protected void internalCarryoverQueue(ConnectionGroup connGroup, String dataSourceName, String failedDataSourceName, String queueName, String subscription) throws MessagingException {
		final Map<String,Object> params = new HashMap<String, Object>();
		try {
			Connection conn = connGroup.getConnection(dataSourceName);
			String newDsn;
			for(int i = 0; true; i++) {
				newDsn = connGroup.getDataSourceName(i);
				if(!newDsn.equals(dataSourceName) && !newDsn.equals(failedDataSourceName))
					break;
			}
			
			Connection newConn = connGroup.getConnection(newDsn);
			params.clear();
			params.put("dataSourceName", determineMessageDataSourceName(failedDataSourceName));
			params.put("queueName", queueName);
			params.put("subscription", subscription);
			dataLayer.executeCall(conn, "START_CARRYOVER", params);
			String copyFromSql = ConvertUtils.getString(params.get("copyFromSql"), false);
			String copyToSql = ConvertUtils.getString(params.get("copyToSql"), false);
			if(copyFromSql == null && copyToSql == null) {
				if(log.isInfoEnabled())
					log.info("Queue '" + queueName + "' on '" + failedDataSourceName + "' is empty in '" + dataSourceName + "' - not copying");
			} else {
				PGConnection pgConn = (PGConnection)DBUnwrap.getRealConnection(conn);
				PGConnection pgNewConn = (PGConnection)DBUnwrap.getRealConnection(newConn);
				CopyOut copyOut = pgConn.getCopyAPI().copyOut(copyFromSql);
				CopyIn copyIn = pgNewConn.getCopyAPI().copyIn(copyToSql);
				boolean okay2 = false;
				try {
					byte[] bytes;
					while((bytes=copyOut.readFromCopy()) != null)
						copyIn.writeToCopy(bytes, 0, bytes.length);
					okay2 = true;
				} finally {
					if(okay2)
						copyIn.endCopy();
					else
						copyIn.cancelCopy();
				}
				params.put("newDataSourceName", determineMessageDataSourceName(newDsn));
				params.put("shadowDataSourceName", determineMessageDataSourceName(dataSourceName));
				dataLayer.executeCall(conn, "FINISH_CARRYOVER", params);
				int cnt = ConvertUtils.getInt(params.get("rowCount"));	
				long lastPostedTime = ConvertUtils.getLong(params.get("lastPostedTime"), 0);
				if(lastPostedTime > 0) {
					dataLayer.executeCall(newConn, "SET_CARRYOVER_LAST_POSTED", params);
				}
				if(log.isInfoEnabled())
					log.info("Copied queue '" + queueName + "' (" + cnt + " messages) with shadow on '" + dataSourceName + "' from '" + failedDataSourceName + "' to '" + newDsn + "'");
			}
			connGroup.commit();
		} catch(SQLException e) {
			throw new MessagingException("Could not carry over data from '" + failedDataSourceName + "' to '" + dataSourceName + "' for queue '" + queueName + "'", e);
		} catch(DataLayerException e) {
			throw new MessagingException("Could not carry over data from '" + failedDataSourceName + "' to '" + dataSourceName + "' for queue '" + queueName + "'", e);
		} catch(ConvertException e) {
			throw new MessagingException("Could not carry over data from '" + failedDataSourceName + "' to '" + dataSourceName + "' for queue '" + queueName + "'", e);
		}
	}
	
	protected void cleanupCarryover() {
		for(String dsn : dataSourceAvailability.getAvailables()) {
			cleanupCarryover(dsn);
		}
	}
	
	protected void cleanupCarryover(String dataSourceName) {
		final Map<String,Object> params = new HashMap<String, Object>();
		try {
			Connection conn = dataLayer.getConnection(dataSourceName, true);
			try {
				dataLayer.executeCall(conn, "CLEAN_EMPTY_CARROVERS", params);
				int r = ConvertUtils.getInt(params.get("tableCount"));
				if(r > 0) {
					if(log.isInfoEnabled())
						log.info("Removed " + r + " empty carry over tables on " + dataSourceName);
				} else if(log.isDebugEnabled())
					log.debug("Removed " + r + " empty carry over tables on " + dataSourceName);
			} finally {
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException e) {
			log.warn("Error occurred while removing empty carry over tables on '" + dataSourceName + "'", e);				
		} catch(DataLayerException e) {
			log.warn("Data Layer Exception while removing empty carry over tables on '" + dataSourceName + "'", e);
		} catch(ConvertException e) {
			log.warn("Conversion error occurred while removing empty carry over tables on '" + dataSourceName + "'", e);
		}
	}

	protected void cleanupShadowBlocks() {
		for(String dsn : dataSourceAvailability.getAvailables()) {
			cleanupShadowBlocks(dsn);
		}
	}

	protected void cleanupShadowBlocks(String dataSourceName) {
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("dataSourceName", dataSourceName);
		try {
			Connection conn = dataLayer.getConnection(dataSourceName, true);
			try {
				dataLayer.executeCall(conn, "GET_PRIMARY_BLOCKS", params);
				Object blocks = params.get("blocks");
				if(blocks == null)
					return;
				if(ConvertUtils.asCollection(blocks).isEmpty())
					return;
				for(String dsn : dataSourceAvailability.getAvailables()) {
					if(!dataSourceName.equals(dsn)) {
						dataLayer.executeCall(dsn, "REMOVE_SHADOW_BLOCKS", params, params, true, null);
						int r = ConvertUtils.getInt(params.get("count"));
						if(r > 0) {
							if(log.isInfoEnabled())
								log.info("Removed " + r + " shadow blocks from " + dataSourceName + " on " + dsn);
						} else if(log.isDebugEnabled())
							log.debug("Removed " + r + " shadow blocks from " + dataSourceName + " on " + dsn);
					}
				}
			} finally {
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException e) {
			log.warn("Error occurred while removing shadow blocks from '" + dataSourceName + "'", e);
		} catch(DataLayerException e) {
			log.warn("Data Layer Exception while removing shadow blocks from '" + dataSourceName + "'", e);
		} catch(ConvertException e) {
			log.warn("Conversion error occurred while removing shadow blocks from '" + dataSourceName + "'", e);
		}
	}
	
	protected static long renewLock(String dataSourceName, long duration, String instanceId) {
		final Map<String,Object> params = new HashMap<String, Object>();
		params.put("duration", duration);
		params.put("instanceId", instanceId);
		try {
			Connection conn = dataLayer.getConnection(dataSourceName, true);
			try {
				dataLayer.executeCall(conn, "RENEW_INSTANCE_LOCK", params);
				long expiredTime = ConvertUtils.getLongSafely(params.get("expiredTime"), 0);
				if(log.isDebugEnabled())
					log.debug("Renewed lock on " + instanceId + " for " + duration + " milliseconds (until " + expiredTime + ") on " + dataSourceName);
				return expiredTime;
			} finally {
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException e) {
			log.warn("Error occurred while renewing lock on '" + dataSourceName + "'", e);

		} catch(DataLayerException e) {
			log.warn("Data Layer Exception while renewing lock on '" + dataSourceName + "'", e);
		}
		return 0;
	}

	protected void cleanupExpiredLocks() {
		for(String dsn : dataSourceAvailability.getAvailables()) {
			cleanupExpiredLocks(dsn);
		}
	}
	
	protected void cleanupExpiredLocks(String dataSourceName) {
		final Map<String,Object> params = new HashMap<String, Object>();
		long leeway = 2 * (getLockRenewalInterval() + getLockRenewalTimeBuffer());
		params.put("leeway", leeway);
		try {
			Connection conn = dataLayer.getConnection(dataSourceName, true);
			try {
				dataLayer.executeCall(conn, "CLEANUP_EXPIRED_LOCKS", params);
				int r = ConvertUtils.getInt(params.get("instanceCount"));
				if(r > 0) {
					if(log.isInfoEnabled())
						log.info("Removed " + r + " expired instances on " + dataSourceName);
				} else if(log.isDebugEnabled())
					log.debug("Removed " + r + " expired instances on " + dataSourceName);
			} finally {
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException e) {
			log.warn("Error occurred while removing expired instances on '" + dataSourceName + "'", e);				
		} catch(DataLayerException e) {
			log.warn("Data Layer Exception while removing expired instances on '" + dataSourceName + "'", e);
		} catch(ConvertException e) {
			log.warn("Conversion error occurred while removing expired instances on '" + dataSourceName + "'", e);
		}
	}
	
	protected void printLockedQueues() {
		for(String dsn : dataSourceAvailability.getAvailables()) {
			printLockedQueues(dsn);
		}
	}
	
	protected void printLockedQueues(String dataSourceName) {
		try {
			Connection conn = dataLayer.getConnection(dataSourceName, true);
			Results results;
			try {
				results = dataLayer.executeQuery(conn, "GET_LOCKED_QUEUES", null);
			} finally {
				DbUtils.closeSafely(conn);
			}
			
			StringWriter writer = new StringWriter();
			PrintWriter pw = new PrintWriter(writer);
			pw.print("Locked Queues on ");
			pw.print(dataSourceName);
			pw.println(":");
			Justification[] justs = new Justification[8];
			Arrays.fill(justs, Justification.LEFT);
			justs[4] = Justification.RIGHT;
			int[] widths = new int[8];
			Arrays.fill(widths, 30);
			widths[4] = 3;
			widths[3] = 50;
			widths[7] = 5;
			StringUtils.writeFixedWidth(results, widths, justs, pw, true, true, false);
			pw.flush();
			log.trace(writer.toString());
		} catch(SQLException e) {
			log.warn("Error occurred while removing expired instances on '" + dataSourceName + "'", e);				
		} catch(DataLayerException e) {
			log.warn("Data Layer Exception while removing expired instances on '" + dataSourceName + "'", e);
		}
	}
	
	protected void refreshQueueSizeStats() {
		for(SortedStats<String, Double> ss : dbPublishPreferredStats.values())
			ss.clearCache();
		for(DataSourceEntry dse : dataSourceEntries)
			if(dse.getName() != null)
				refreshQueueSizeStats(dse.getName(), dse.getPriority());
		for(SortedStats<String, Double> ss : dbPublishPreferredStats.values())
			ss.sortStats();
	}

	protected void refreshQueueSizeStats(String dataSourceName, double priority) {
		try {
			Connection conn = dataLayer.getConnection(dataSourceName, true);
			try {
				Results results = dataLayer.executeQuery(conn, "GET_QUEUE_SIZES", Collections.singletonMap("currentTime", System.currentTimeMillis()));
				while(results.next()) {
					String queue = results.getValue("queueName", String.class);
					long count = results.getValue("messageCount", Long.class);
					dbPublishPreferredStats.getOrCreate(queue).putStat(dataSourceName, (count / priority) - priority);
					if(log.isDebugEnabled())
						log.debug("Queue '" + queue + " ' on " + dataSourceName + " has " + count + " pending messages");
				}
			} finally {
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException e) {
			log.warn("Error occurred while refreshing queue size stats on '" + dataSourceName + "'", e);
		} catch(DataLayerException e) {
			log.warn("Data Layer Exception while refreshing queue size stats on '" + dataSourceName + "'", e);
		} catch(ConvertException e) {
			log.warn("Conversion error occurred while refreshing queue size stats on '" + dataSourceName + "'", e);
		}
	}

	protected void commitWorker(Collection<ConsumedPeerMessage> consumed, Collection<PublishedPeerMessage> published, Collection<String> blocked) throws MessagingException {
		if(wholeStopped.get())
			throw new MessagingException("Supervisor is closed. No further operations are allowed");
		
		long start;
		if(log.isInfoEnabled()) {
			start = System.currentTimeMillis();
		} else {
			start = 0;
		}
		
		final ConnectionGroup connGroup = createConnectionGroup();
		int directConsumed = 0;
		int dbConsumed = 0;
		int directPublished = 0;
		int dbPublished = 0;
		int hybridPublished = 0;
		long consumeConnectingTime;
		long time;
		boolean okay = false;
		try {
			if(consumed != null && !consumed.isEmpty()) {
				for(ConsumedPeerMessage message : consumed) {
					if(message.isDbMessage()) {
						message.markComplete(connGroup);
						dbConsumed++;
					} else {
						directConsumed++;
					}
				}
			}
			
			consumeConnectingTime = connGroup.getConnectingTime();
			time = System.currentTimeMillis();
			if(published != null && !published.isEmpty()) {
				for(PublishedPeerMessage message : published) {
					message.setPosted(time);
					PublishType publishType = message.isTemporary() ? getTemporaryPublishType(message.getQueueName()) : getDurablePublishType(message.getQueueName());
					switch(publishType) {
						case HYBRID:
							publishDbMessage(message, connGroup, true);
							hybridPublished++;
							break;
						case DIRECT:
							getOrCreateDirectProducer().publishMessage(message, getDirectPublishTimeout(message.getQueueName()), getDirectPublishAttempts(message.getQueueName()), publishType, message.isBroadcast());
							directPublished++;
							break;
						case DIRECT_OR_DB:
							DirectPeerProducer directProducer = directProducerRef.get();
							if(directProducer != null && directProducer.publishMessage(message, getDirectPublishTimeout(message.getQueueName()), getDirectPublishAttempts(message.getQueueName()), publishType, message.isBroadcast())) {
								directPublished++;
							} else {
								publishDbMessage(message, connGroup, false);
								dbPublished++;
							}
							break;
						case DB:
							publishDbMessage(message, connGroup, false);
							dbPublished++;
							break;
					}
				}
				
				if(blocked != null && !blocked.isEmpty()) {
					addBlocks(blocked, connGroup);
				}
			}
			connGroup.commit();
			okay = true;
		} catch(SQLException e) {
			log.error("Could not commit message deletion or creation to QueueLayer database", e);
			throw new MessagingException(e);
		} catch(DataLayerException e) {
			log.error("Could not commit message deletion or creation to QueueLayer database", e);
			throw new MessagingException(e);
		} finally {
			connGroup.close(!okay);
			if(consumed != null && !consumed.isEmpty()) {
				for(ConsumedPeerMessage message : consumed) {
					message.acknowledge(okay);
				}
			}
		}
		
		//NOTE: Do this after message.acknowledge() so that pending count on consumed queues is decremented before pending count on published queues is incremented
		if(hybridPublished > 0 && published != null) {
			for(PublishedPeerMessage message : published) {
				PublishType publishType = message.isTemporary() ? getTemporaryPublishType(message.getQueueName()) : getDurablePublishType(message.getQueueName());
				switch(publishType) {
					case HYBRID:
						//NOTE: This must occur after the db commit, else we have a race condition
						getOrCreateDirectProducer().publishMessage(message, getDirectPublishTimeout(message.getQueueName()), getDirectPublishAttempts(message.getQueueName()), publishType, message.isBroadcast());
						break;
				}
			}
		}
		
		if(log.isDebugEnabled()) {
			final long end = System.currentTimeMillis();
			final StringBuilder sb = new StringBuilder();
			sb.append("Committed worker with ");
			boolean first = true;
			if(directConsumed > 0) {
				first = false;
				sb.append(directConsumed).append(" direct consumed messages ");	
			}
			
			if(dbConsumed > 0) {
				if(first)
					first = false;
				else
					sb.append("and ");
				sb.append(dbConsumed).append(" database consumed messages ");				
			}
			
			if(directConsumed > 0 || dbConsumed > 0)
				sb.append("[in ").append(time-start-consumeConnectingTime).append(" ms] ");
			if(directPublished > 0) {
				if(first)
					first = false;
				else
					sb.append("and ");
				sb.append(directPublished).append(" direct published messages ");
			}
			
			if(dbPublished > 0) {
				if(first)
					first = false;
				else
					sb.append("and ");
				sb.append(dbPublished).append(" database published messages ");				
			}
			
			if(hybridPublished > 0) {
				if(first)
					first = false;
				else
					sb.append("and ");
				sb.append(hybridPublished).append(" hybrid published messages ");				
			}
			
			if(directPublished > 0 || dbPublished > 0 || hybridPublished > 0)
				sb.append("[in ").append(end-time-connGroup.getConnectingTime()+consumeConnectingTime).append(" ms] ");
			if(first)
				sb.append("no messages");
			else
				sb.append("with " + connGroup.getConnectingTime() + " ms to connect to database");
			
			log.debug(sb.toString());
		}
	}

	protected String determineMessageDataSourceName(String supervisorDataSourceName) {
		if(!StringUtils.isBlank(dataSourceNamePrefix) && supervisorDataSourceName.startsWith(dataSourceNamePrefix)) {
			return supervisorDataSourceName.substring(dataSourceNamePrefix.length());
		}
		return supervisorDataSourceName;
	}

	protected String determineSupervisorDataSourceName(String messageDataSourceName) {
		if(!StringUtils.isBlank(dataSourceNamePrefix) && !messageDataSourceName.startsWith(dataSourceNamePrefix)) {
			return dataSourceNamePrefix + messageDataSourceName;
		}
		return messageDataSourceName;
	}

	protected class ToConnectionDataSourceNameIterator implements Iterable<String> {
		protected final Iterable<String> delegate;

		public ToConnectionDataSourceNameIterator(Iterable<String> delegate) {
			this.delegate = delegate;
		}

		@Override
		public Iterator<String> iterator() {
			final Iterator<String> iter = delegate.iterator();
			return new Iterator<String>() {
				@Override
				public boolean hasNext() {
					return iter.hasNext();
				}

				@Override
				public String next() {
					return determineSupervisorDataSourceName(iter.next());
				}

				@Override
				public void remove() {
					iter.remove();
				}
			};
		}
	}

	protected void addBlocks(Collection<String> blocks, ConnectionGroup connGroup) throws SQLException, DataLayerException {
		List<String> dataSourceNames = connGroup.getPreferredDataSourceNames(2);
		Connection firstConn = connGroup.getConnection(dataSourceNames.get(0), false);
		Connection secondConn = connGroup.getConnection(dataSourceNames.get(1), false);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("blocks", blocks);
		params.put("dataSourceName", determineMessageDataSourceName(dataSourceNames.get(0)));
		dataLayer.executeCall(secondConn, "ADD_SHADOW_BLOCKS", params);
		dataLayer.executeCall(firstConn, "ADD_PRIMARY_BLOCKS", params);
	}

	protected void publishDbMessage(PublishedPeerMessage message, ConnectionGroup connGroup, boolean hybrid) throws SQLException, DataLayerException, MessagingException {
		if(hybrid) {
			Iterable<String> preferred = getOrCreateDirectProducer().getPreferredDataSourceName(message.getQueueName());
			if(preferred == null)
				connGroup.setPreferred(null);
			else if(StringUtils.isBlank(dataSourceNamePrefix))
				connGroup.setPreferred(preferred);
			else
				connGroup.setPreferred(new ToConnectionDataSourceNameIterator(preferred));
			if (log.isDebugEnabled())
				log.debug("Set preferred for hybrid queue {0} to {1}", message.getQueueName(), preferred);
			
		} else {
			Iterable<String> preferred = getPreferredDataSourceName(message.getQueueName());
			connGroup.setPreferred(preferred);
			if (log.isDebugEnabled())
				log.debug("Set preferred for db queue {0} to {1}", message.getQueueName(), preferred);
		}
		
		List<String> dataSourceNames = connGroup.getPreferredDataSourceNames(message.isTemporary() ? 1 : 2);
		
		message.setDataSourceName(determineMessageDataSourceName(dataSourceNames.get(0)));
		Connection firstConn = connGroup.getConnection(dataSourceNames.get(0), false);
		Connection secondConn;
		if(!message.isTemporary()) {
			message.setShadowDataSourceName(determineMessageDataSourceName(dataSourceNames.get(1)));
			secondConn = connGroup.getConnection(dataSourceNames.get(1), false);
		} else {
			secondConn = null;
		}
		
		if(message.isBroadcast()) {
			Map<String,Object> params = new HashMap<String, Object>();
			dataLayer.executeCall(firstConn, "BROADCAST_MESSAGE", message, params);
			if(log.isInfoEnabled())
				log.info("Published " + (message.isTemporary() ? "temporary" : "durable") + " message '" + message.getMessageId() + "' to '" + dataSourceNames.get(0) + "' for " + message.getQueueName());
			String[] subscriptions;
			try {
				subscriptions = ConvertUtils.convert(String[].class, params.get("subscriptions"));
			} catch(ConvertException e) {
				throw new MessagingException(e);
			}
			
			if(!message.isTemporary()) {
				//Check other datasource for subscriptions
				params.put("dataSourceName", message.getDataSourceName());
				params.put("queueName", message.getQueueName());
				dataLayer.executeCall(secondConn, "REGISTER_SUBSCRIPTIONS", params);
				String[] missingSubscriptions;
				try {
					missingSubscriptions = ConvertUtils.convert(String[].class, params.get("missingSubscriptions"));
				} catch(ConvertException e) {
					throw new MessagingException(e);
				}
				
				if(missingSubscriptions != null) {
					for(String sub : missingSubscriptions) {
						message.setSubscription(sub);
						dataLayer.executeCall(firstConn, "ADD_MESSAGE", message);
						dataLayer.executeCall(secondConn, "ADD_SHADOW_MESSAGE", message);
					}
				}
				
				if(subscriptions != null) {
					for(String sub : subscriptions) {
						message.setSubscription(sub);
						dataLayer.executeCall(secondConn, "ADD_SHADOW_MESSAGE", message);
					}
				} else {
					log.info("No active subscriptions were found for queue '" + message.getQueueName() + "'");
				}
			}
		} else {
			if(!message.isTemporary())
				dataLayer.executeCall(secondConn, "ADD_SHADOW_MESSAGE", message);
			dataLayer.executeCall(firstConn, "ADD_MESSAGE", message);
			if(log.isInfoEnabled())
				log.info("Published " + (message.isTemporary() ? "temporary" : "durable") + " message '" + message.getMessageId() + "' to '" + dataSourceNames.get(0) + "' for " + message.getQueueName());
		}
	}

	protected Iterable<String> getPreferredDataSourceName(String queueName) {
		return getQueueSizeRefreshInterval() > 0 ? dbPublishPreferredStats.getIfPresent(queueName) : null;
	}

	@Override
	public boolean isPaused(String queueName, String subscription) throws MessagingException {
		QueueHandler queueHandler;
		try {
			queueHandler = queueHandlers.getOrCreate(new QueueKey(queueName, subscription));
		} catch(ExecutionException e) {
			throw new MessagingException(e);
		}
		return queueHandler.isPaused();
	}

	@Override
	public void pause(String queueName, String subscription) throws MessagingException {
		QueueHandler queueHandler;
		try {
			queueHandler = queueHandlers.getOrCreate(new QueueKey(queueName, subscription));
		} catch(ExecutionException e) {
			throw new MessagingException(e);
		}
		queueHandler.pause();
	}

	@Override
	public void unpause(String queueName, String subscription) throws MessagingException {
		QueueHandler queueHandler;
		try {
			queueHandler = queueHandlers.getOrCreate(new QueueKey(queueName, subscription));
		} catch(ExecutionException e) {
			throw new MessagingException(e);
		}
		queueHandler.unpause();
	}

	public void clearSubscription(String queueName, String subscription) throws MessagingException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("queueName", queueName);
		params.put("subscription", subscription);
		params.put("instanceId", instanceId);
		params.put("clearBefore", System.currentTimeMillis());
		params.put("primary", true);
		for(String dsn : getDataSourceSet()) {
			Connection conn;
			try {
				conn = dataLayer.getConnection(dsn, true);
			} catch(DataSourceNotFoundException e) {
				log.info("Could not get connection for database '" + dsn + "'", e);	
				dataSourceAvailability.setAvailable(dsn, false);	
				continue;// XXX: do we need to mark this and then remove all messages posted before this time if we do acquire a lock in the future?
			} catch(SQLException e) {
				log.info("Could not get connection for database '" + dsn + "'", e);	
				dataSourceAvailability.setAvailable(dsn, false);	
				continue;// XXX: do we need to mark this and then remove all messages posted before this time if we do acquire a lock in the future?
			}
			
			try {
				dataSourceAvailability.setAvailable(dsn, true);							
				params.put("dataSourceName", determineMessageDataSourceName(dsn));
				try {
					dataLayer.executeCall(conn, "ACQUIRE_QUEUE_LOCK", params);
					if(!ConvertUtils.getBoolean(params.get("success"))) {
						throw new MessagingException("Could not lock queue '" + queueName + "' in database '" + dsn + "' to clear subscription '" + subscription + "'");
					}
					if(log.isInfoEnabled())
						log.info("Acquired lock on queue '" + queueName + "' in database '" + dsn + "' for subscription '" + subscription + "' and cleared pending messages");
					// release
					if(log.isInfoEnabled())
						log.info("Releasing lock on queue '" + queueName + "' in database '" + dsn + "' after attempting to clear subcription '" + subscription + "'");
					try {
						dataLayer.executeCall(conn, "RELEASE_QUEUE_LOCK", params);
					} catch(SQLException e) {
						log.warn("While releasing lock on queue '" + queueName + "' in database '" + dsn + "' after attempting to clear subcription '" + subscription + "'", e);
					} catch(DataLayerException e) {
						log.warn("While releasing lock on queue '" + queueName + "' in database '" + dsn + "' after attempting to clear subcription '" + subscription + "'", e);
					}
				} catch(SQLException e) {
					throw new MessagingException("Error occurred while trying to lock queue '" + queueName + "' in database '" + dsn + "' to clear subscription '" + subscription + "'");
				} catch(DataLayerException e) {
					throw new MessagingException("Error occurred while trying to lock queue '" + queueName + "' in database '" + dsn + "' to clear subscription '" + subscription + "'");
				} catch(ConvertException e) {
					throw new MessagingException("Conversion error occurred while trying to lock queue '" + queueName + "' in database '" + dsn + "' to clear subscription '" + subscription + "'", e);
				}
			} finally {
				DbUtils.closeSafely(conn);
			}
		}
	}
	
	public String[] getSubscriptions(String queueName) throws MessagingException {
		Map<String, Object> params = Collections.singletonMap("queueName", (Object) queueName);
		final Set<String> subscriptions = new TreeSet<String>();
		for(String dsn : getDataSourceSet()) {
			if(dataSourceAvailability.isAvailable(dsn)) {
				try {
					Connection conn = dataLayer.getConnection(dsn, true);
					try {
						Results results = dataLayer.executeQuery(conn, "GET_SUBSCRIPTIONS", params);
						try {
							while(results.next())
								subscriptions.add(results.getValue("subscription", String.class));
						} finally {
							results.close();
						}
					} catch(SQLException e) {
						log.warn("Error while getting subscriptions for queue '" + queueName + "' in database '" + dsn + "'", e);
					} catch(DataLayerException e) {
						log.warn("Error while getting subscriptions for queue '" + queueName + "' in database '" + dsn + "'", e);
					} catch(ConvertException e) {
						throw new MessagingException(e);
					} finally {
						DbUtils.closeSafely(conn);
					}
				} catch(SQLException e) {
					if(log.isInfoEnabled())
						log.info("Could not get connection for database '" + dsn + "'", e);
					dataSourceAvailability.setAvailable(dsn, false);
				} catch(DataSourceNotFoundException e) {
					if(log.isInfoEnabled())
						log.info("Could not get connection for database '" + dsn + "'", e);
					dataSourceAvailability.setAvailable(dsn, false);
				}
			}
		}
		
		DirectPeerProducer directProducer = directProducerRef.get();
		if(directProducer != null)
			subscriptions.addAll(directProducer.getDirectSubscriptions(queueName));

		return subscriptions.toArray(new String[subscriptions.size()]);
	}

	public boolean isSubscriptionActive(String queueName, String subscription) throws MessagingException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("queueName", queueName);
		params.put("subscription", subscription);
		int count = 0;
		for(String dsn : getDataSourceSet()) {
			if(dataSourceAvailability.isAvailable(dsn)) {
				try {
					Connection conn = dataLayer.getConnection(dsn, true);
					try {
						dataLayer.executeCall(conn, "QUEUE_EXISTS", params);
						if(ConvertUtils.getBoolean(params.get("exists"))) {
							if(++count >= 2)
								return true;
						}
					} catch(SQLException e) {
						log.warn("Error while checking for queue '" + queueName + "' in database '" + dsn + "'", e);
					} catch(DataLayerException e) {
						log.warn("Error while checking for queue '" + queueName + "' in database '" + dsn + "'", e);
					} catch(ConvertException e) {
						throw new MessagingException(e);
					} finally {
						DbUtils.closeSafely(conn);
					}
				} catch(SQLException e) {
					if(log.isInfoEnabled())
						log.info("Could not get connection for database '" + dsn + "'", e);
					dataSourceAvailability.setAvailable(dsn, false);												
				} catch(DataSourceNotFoundException e) {
					if(log.isInfoEnabled())
						log.info("Could not get connection for database '" + dsn + "'", e);
					dataSourceAvailability.setAvailable(dsn, false);												
				}
			}
		}
		return false;
	}
	
	public void activateSubscription(String queueName, String subscription) throws MessagingException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("queueName", queueName);
		params.put("subscription", subscription);
		int active = 0;
		Exception lastException = null;
		for(String dsn : dataSourceAvailability.getAvailables()) {
			Connection conn;
			try {
				conn = dataLayer.getConnection(dsn, true);
			} catch(DataSourceNotFoundException e) {
				if(log.isInfoEnabled())
					log.info("Could not get connection for database '" + dsn + "'", e);
				dataSourceAvailability.setAvailable(dsn, false);	
				lastException = e;
				continue;
			} catch(SQLException e) {
				if(log.isInfoEnabled())
					log.info("Could not get connection for database '" + dsn + "'", e);
				dataSourceAvailability.setAvailable(dsn, false);	
				lastException = e;
				continue;
			}
			try {
				try {
					dataLayer.executeCall(conn, "REGISTER_QUEUE", params);
					active++;
				} catch(SQLException e) {
					log.warn("Error while registering subscription '" + subscription + "' on queue '" + queueName + "' in database '" + dsn, e);
					lastException = e;
				} catch(DataLayerException e) {
					log.warn("Error while registering subscription '" + subscription + "' on queue '" + queueName + "' in database '" + dsn, e);
					lastException = e;
				}
			} finally {
				DbUtils.closeSafely(conn);
			}
		}
		// isSubscriptionActive() requires 2 so activateSubscription() can be total - 1
		int total = dataSourceNames.size();
		int required = total - 1;
		if(total == 0) {
			throw new MessagingException("No data sources are configured");
		} else if(active == 0) {
			throw new MessagingException("Could not register subscription '" + subscription + "' on queue '" + queueName + "' in any data source", lastException);
		} else if(active < required) {
			throw new MessagingException("Could not register subscription '" + subscription + "' on queue '" + queueName + "' in enough data sources [" + required + " required]", lastException);
		}
	}
	
	public String getPrimaryDataSourceName() {
		if(dataSourceEntries.isEmpty())
			return null;
		DataSourceEntry entry = dataSourceEntries.get(0);
		if(entry.getPriority() == 10)
			return entry.getName();
		return null;
	}

	public void setPrimaryDataSourceName(String primaryDataSourceName) {
		if(dataSourceEntries.isEmpty()) {
			if(primaryDataSourceName != null)
				dataSourceEntries.add(new DataSourceEntry(primaryDataSourceName, 10, true));
		} else {
			DataSourceEntry existing = dataSourceEntries.get(0);
			if(existing.getName() == null) {
				existing.setName(primaryDataSourceName);
				existing.setPrimary(true);
			} else if(existing.isPrimary()) {
				if(primaryDataSourceName == null) {
					dataSourceEntries.remove(0);
				} else if(primaryDataSourceName.equals(dataSourceEntries.get(0).getName()))
					return;
				dataSourceEntries.set(0, new DataSourceEntry(primaryDataSourceName, 10, true));
				for(Iterator<DataSourceEntry> iter = dataSourceEntries.listIterator(1); iter.hasNext();) {
					if(primaryDataSourceName.equals(iter.next().getName()))
						iter.remove();
				}
			} else if(primaryDataSourceName != null) {
				dataSourceEntries.add(new DataSourceEntry(primaryDataSourceName, 10, true));
				for(Iterator<DataSourceEntry> iter = dataSourceEntries.listIterator(1); iter.hasNext();) {
					if(primaryDataSourceName.equals(iter.next().getName()))
						iter.remove();
				}
			}
		}
		updateDataSourceSet();
	}

	public String[] getSecondaryDataSourceNames() {
		if(dataSourceEntries.isEmpty())
			return null;
		int clearIndex = 0;
		for(DataSourceEntry entry : dataSourceEntries) {
			if(!entry.isPrimary())
				break;
			else
				clearIndex++;
		}
		return toDataSourceNameArray(dataSourceEntries.subList(clearIndex, dataSourceEntries.size()));
	}

	public void setSecondaryDataSourceNames(String[] secondaryDataSourceNames) {
		if(dataSourceEntries.isEmpty()) {
			if(secondaryDataSourceNames != null) {
				for(String dsn : secondaryDataSourceNames)
					dataSourceEntries.add(new DataSourceEntry(dsn, 5, false));
			}
		} else {
			int i = 0;
			int priority = Integer.MAX_VALUE;
			ListIterator<DataSourceEntry> iter = dataSourceEntries.listIterator();
			for(; iter.hasNext(); i++) {
				DataSourceEntry entry = iter.next();
				if(entry.isPrimary() || (i == 0 && entry.getName() == null)) {
					if(entry.getPriority() <= priority)
						priority = (int) Math.floor(entry.getPriority() / 2.0);
				} else {
					iter.previous();
					break;
				}
			}
			int clearIndex = i + (secondaryDataSourceNames == null ? 0 : secondaryDataSourceNames.length);
			if(secondaryDataSourceNames != null) {
				i = 0;
				for(; i < secondaryDataSourceNames.length && iter.hasNext(); i++) {
					DataSourceEntry entry = iter.next();
					entry.setName(secondaryDataSourceNames[i]);
					if(entry.getPriority() > priority)
						entry.setPriority(priority);
				}
				if(priority == Integer.MAX_VALUE)
					priority = 5;
				for(; i < secondaryDataSourceNames.length; i++)
					dataSourceEntries.add(new DataSourceEntry(secondaryDataSourceNames[i], priority, false));
			}
			if(clearIndex < dataSourceEntries.size()) {
				dataSourceEntries.subList(clearIndex, dataSourceEntries.size()).clear();
			}
		}
		updateDataSourceSet();
	}
	
	public long getDefaultNotificationPollFrequency() {
		return defaultNotificationPollFrequency;
	}
	
	public void setDefaultNotificationPollFrequency(long notificationPollFrequency) {
		this.defaultNotificationPollFrequency = notificationPollFrequency;
	}
	
	public long getDefaultDataPollFrequency() {
		return defaultDataPollFrequency;
	}
	
	public void setDefaultDataPollFrequency(long defaultDataPollFrequency) {
		this.defaultDataPollFrequency = defaultDataPollFrequency;
	}
	
	public int getDefaultQueueBufferSize() {
		return defaultQueueBufferSize;
	}
	
	public void setDefaultQueueBufferSize(int defaultQueueBufferSize) {
		this.defaultQueueBufferSize = defaultQueueBufferSize;
	}
	
	public float getDefaultPollOnPercentEmpty() {
		return defaultPollOnPercentEmpty;
	}
	
	public void setDefaultPollOnPercentEmpty(float defaultPollOnPercentEmpty) {
		this.defaultPollOnPercentEmpty = defaultPollOnPercentEmpty;
	}
	
	public long getSecondaryReconnectInterval() {
		return secondaryReconnectInterval;
	}
	
	public void setSecondaryReconnectInterval(long secondaryReconnectInterval) {
		this.secondaryReconnectInterval = secondaryReconnectInterval;
	}
	
	public long getPrimaryReconnectInterval() {
		return primaryReconnectInterval;
	}
	
	public void setPrimaryReconnectInterval(long primaryReconnectInterval) {
		this.primaryReconnectInterval = primaryReconnectInterval;
	}
	
	public RetryPolicy getDefaultRetryPolicy() {
		return defaultRetryPolicy;
	}

	public long getDefaultCleanupInterval() {
		return defaultCleanupInterval;
	}

	public void setDefaultCleanupInterval(long cleanupInterval) {
		this.defaultCleanupInterval = cleanupInterval;
	}

	public boolean isJmxEnabled() {
		return jmxEnabled;
	}

	public void setJmxEnabled(boolean jmxEnabled) {
		this.jmxEnabled = jmxEnabled;
		DirectPeerProducer directProducer = directProducerRef.get();
		if(directProducer != null) 
			directProducer.setJmxEnabled(jmxEnabled);	
	}

	public PollType getDefaultPollType() {
		return defaultPollType;
	}

	public void setDefaultPollType(PollType defaultPollType) {
		if(defaultPollType == null)
			throw new NullPointerException("DefaultPollType may not be null");		
		this.defaultPollType = defaultPollType;
	}

	public DirectPollerConfig getDirectPollerConfig(String key) {
		return directPollerConfigs.getOrCreate(key);
	}
	
	public void initialize() throws ServiceException, InterruptedException, MessagingException {
		DirectPeerProducer directProducer = directProducerRef.get();
		if(directProducer != null && directProducer.getListenAddress() != null)
			directProducer.start();
		ObjectName jmxObjectName = null;
		if(isJmxEnabled()) {
			try {
				jmxObjectName = new ObjectName(jmxNamePrefix + ",clusterName=" + clusterName);
			} catch(MalformedObjectNameException e) {
				log.warn("Could not register '" + this + "' in JMX", e);
			} catch(NullPointerException e) {
				log.warn("Could not register '" + this + "' in JMX", e);
			}
			
			if(jmxObjectName != null) {
				try {
					ManagementFactory.getPlatformMBeanServer().registerMBean(this, jmxObjectName);
					this.jmxObjectName = jmxObjectName;
				} catch(InstanceAlreadyExistsException e) {
					log.warn("Could not register '" + this + "' in JMX", e);
				} catch(MBeanRegistrationException e) {
					log.warn("Could not register '" + this + "' in JMX", e);
				} catch(NotCompliantMBeanException e) {
					log.warn("Could not register '" + this + "' in JMX", e);
				} catch(NullPointerException e) {
					log.warn("Could not register '" + this + "' in JMX", e);
				}
			}
		}
		
		scheduler.scheduleAtFixedRate(new Runnable() {
			public void run() {
				cleanupCarryover();
			}
		}, 0, getCarryoverCleanupInterval(), TimeUnit.MILLISECONDS);
		
		scheduler.scheduleAtFixedRate(new Runnable() {
			public void run() {
				cleanupExpiredLocks();
			}
		}, 0, getExpiredInstanceCleanupInterval(), TimeUnit.MILLISECONDS);
		
		if(getQueueSizeRefreshInterval() > 0) {
			scheduler.scheduleAtFixedRate(new Runnable() {
				public void run() {
					refreshQueueSizeStats();
				}
			}, 0, getQueueSizeRefreshInterval(), TimeUnit.MILLISECONDS);
		}
		
		if(getCleanupShadowBlocksInterval() > 0) {
			scheduler.scheduleAtFixedRate(new Runnable() {
				public void run() {
					cleanupShadowBlocks();
				}
			}, 0, getCleanupShadowBlocksInterval(), TimeUnit.MILLISECONDS);

		}

		if(log.isTraceEnabled()) {
			scheduler.scheduleAtFixedRate(new Runnable() {
				public void run() {
					printLockedQueues();
				}
			}, 0, 4000, TimeUnit.MILLISECONDS);
		}
		
		if(isMultiplexingEnabled() && directPollerConfigs.size() > 0) {
			// start up direct and hybrid pollers
			boolean directPollers;
			boolean hybridPollers;
			switch(getDefaultPollType()) {
				case DB_AND_DIRECT: case DIRECT:
					directPollers = true;
					hybridPollers = false;
					break;
				case HYBRID:
					directPollers = false;
					hybridPollers = true;
					break;
				case HYBRID_AND_DIRECT:
					directPollers = true;
					hybridPollers = true;
					break;
				default:
					directPollers = false;
					hybridPollers = false;
					break;
			}
			
			if(!directPollers || !hybridPollers) {
				OUTER: for(QueueSettings qs : queueSettingsCache.values()) {
					switch(qs.getPollType()) {
						case DB_AND_DIRECT: case DIRECT:
							directPollers = true;
							if(hybridPollers)
								break OUTER;
							break;
						case HYBRID:
							hybridPollers = true;
							if(directPollers)
								break OUTER;
							break;
						case HYBRID_AND_DIRECT:
							directPollers = true;
							hybridPollers = true;
							break OUTER;
						default:
							break;
					}
				}
			}
			
			if(directPollers) {
				if(directPollerConfigs.size() == 0)
					throw new MessagingException("No DirectPollers are configured");
				for(DirectPollerConfig config : directPollerConfigs.values()) {
					if(config.getUrl() != null) {
						PollerAssistant assistant = new MultiDirectPollerAssistant();
						int num = config.getDefaultPollerThreads();
						for(int i = 0; i < num; i++) {
							Poller poller = new DirectPoller(assistant, instanceId, null, config, jmxObjectName);
							multiplexingDirectPollers.add(poller);
							poller.start();
						}
						multiplexingDirectPollerInstances++;
					}
				}
				if(multiplexingDirectPollers.isEmpty())
					throw new MessagingException("No DirectPollers are configured");
			}
			
			if(hybridPollers) {
				if(directPollerConfigs.size() == 0)
					throw new MessagingException("No HybridDirectPollers are configured");
				DbPollerAssistant subAssistant = new MultiDbPollerAssistant();
				Set<HybridDirectPoller> subPollers = new LinkedHashSet<HybridDirectPoller>();
				multiplexingHybridAssistant = new MultiHybridPollerAssistant(subAssistant, subPollers);	
				for(DirectPollerConfig config : directPollerConfigs.values()) {
					if(config.getUrl() != null) {
						int num = config.getDefaultPollerThreads();
						for(int i = 0; i < num; i++) {
							HybridDirectPoller poller = new HybridDirectPoller(multiplexingHybridAssistant, instanceId, null, config, jmxObjectName);
							subPollers.add(poller);
							poller.start();
						}
						multiplexingHybridPollers.addAll(subPollers);
						multiplexingHybridPollerInstances++;
					}
				}
				if(multiplexingHybridPollers.isEmpty())
					throw new MessagingException("No HybridDirectPollers are configured");
			}
		}
	}
	
	public void shutdown() {
		if(jmxObjectName != null) {
			try {
				ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmxObjectName);
			} catch(InstanceNotFoundException e) {
				log.warn("Could not unregister '" + this + "' in JMX", e);
			} catch(MBeanRegistrationException e) {
				log.warn("Could not unregister '" + this + "' in JMX", e);
			} catch(NullPointerException e) {
				log.warn("Could not unregister '" + this + "' in JMX", e);
			}
			jmxObjectName = null;
		}
	}
	
	public int getConsumerCount(String queueName, boolean temporary) {
		return getConsumerCount(queueName, null, temporary);
	}

	public int getConsumerCount(String queueName, String subscription, boolean temporary) {
		PublishType publishType = (temporary ? getTemporaryPublishType(queueName) : getDurablePublishType(queueName));
		switch(publishType) {
			case DB:
				Iterator<String> iter = dataSourceAvailability.getAvailables().iterator();
				int cnt = 0;
				while(iter.hasNext()) {
					iter.next();
					cnt++;
				}
				return cnt;
			case DIRECT:
				DirectPeerProducer directProducer = directProducerRef.get();
				if(directProducer == null)
					return 0;
				return directProducer.getDirectConsumerCount(queueName, subscription);
			case DIRECT_OR_DB:
				iter = dataSourceAvailability.getAvailables().iterator();
				cnt = 0;
				while(iter.hasNext()) {
					iter.next();
					cnt++;
				}
				directProducer = directProducerRef.get();
				if(directProducer != null)
					cnt += directProducer.getDirectConsumerCount(queueName, subscription);
				return cnt;
			case HYBRID:
				directProducer = directProducerRef.get();
				if(directProducer == null)
					return 0;
				return directProducer.getHybridConsumerCount(queueName, subscription);
			default:
				return -1;
		}
	}

	public void setDirectListenUrl(String directListenUrl) throws UnknownHostException, ConvertException {
		getOrCreateDirectProducer().setListenAddress(IOUtils.parseInetSocketAddress(directListenUrl));
	}
	
	public String getDirectListenUrl() {
		DirectPeerProducer dpp = directProducerRef.get();
		return dpp == null ? null : dpp.getListenAddressString();
	}

	@Deprecated
	public void setDirectListenAddress(InetSocketAddress directListenAddress) {
		getOrCreateDirectProducer().setListenAddress(directListenAddress);
	}

	@Deprecated
	public InetSocketAddress getDirectListenAddress() {
		return getOrCreateDirectProducer().getListenAddress();
	}
	
	@Deprecated
	public void setDirectMaxConnections(int maxConnections) {
		getOrCreateDirectProducer().setMaxConnections(maxConnections);
	}

	@Deprecated
	public int getDirectMaxConnections() {
		DirectPeerProducer dpp = directProducerRef.get();
		return dpp == null ? 0 : dpp.getMaxConnections();
	}

	@Deprecated
	public void setDirectListenerThreads(int numThreads) {
		getOrCreateDirectProducer().setNumThreads(numThreads);
	}

	@Deprecated
	public int getDirectListenerThreads() {
		DirectPeerProducer dpp = directProducerRef.get();
		return dpp == null ? 0 : dpp.getNumThreads();
	}

	public DirectPeerProducer getDirectProducer() {
		return getOrCreateDirectProducer();
	}

	protected DirectPeerProducer getOrCreateDirectProducer() {
		DirectPeerProducer directProducer = directProducerRef.get();
		if(directProducer == null) {
			directProducer = new DirectPeerProducer();
			if(directProducerRef.compareAndSet(null, directProducer)) {
				directProducer.setJmxEnabled(isJmxEnabled());
				return directProducer;
			} else {
				return getOrCreateDirectProducer();
			}
		}
		return directProducer;
	}

	public PublishType getDefaultDurablePublishType() {
		return defaultDurablePublishType;
	}

	public void setDefaultDurablePublishType(PublishType durablePublishType) {
		if(durablePublishType == null)
			throw new NullPointerException("DurablePublishType may not be null");
		this.defaultDurablePublishType = durablePublishType;
	}

	public PublishType getDefaultTemporaryPublishType() {
		return defaultTemporaryPublishType;
	}

	public void setDefaultTemporaryPublishType(PublishType temporaryPublishType) {
		if(temporaryPublishType == null)
			throw new NullPointerException("TemporaryPublishType may not be null");
		this.defaultTemporaryPublishType = temporaryPublishType;
	}

	public long getDefaultDirectPublishTimeout() {
		return defaultDirectPublishTimeout;
	}

	public void setDefaultDirectPublishTimeout(long defaultDirectPublishTimeout) {
		this.defaultDirectPublishTimeout = defaultDirectPublishTimeout;
	}

	public int getDefaultDirectPublishAttempts() {
		return defaultDirectPublishAttempts;
	}

	public void setDefaultDirectPublishAttempts(int defaultDirectPublishAttempts) {
		this.defaultDirectPublishAttempts = defaultDirectPublishAttempts;
	}

	public long getCarryoverCleanupInterval() {
		return carryoverCleanupInterval;
	}

	public void setCarryoverCleanupInterval(long carryoverCleanupInterval) {
		this.carryoverCleanupInterval = carryoverCleanupInterval;
	}

	public RedeliveryDetection getDefaultRedeliveryDetection() {
		return defaultRedeliveryDetection;
	}

	public void setDefaultRedeliveryDetection(RedeliveryDetection defaultRedeliveryDetection) {
		if(defaultRedeliveryDetection == null)
			throw new NullPointerException("DefaultRedeliveryDetection may not be null");
		this.defaultRedeliveryDetection = defaultRedeliveryDetection;
	}
	
	public boolean isMultiplexingEnabled() {
		return multiplexingEnabled;
	}
	
	public void setMultiplexingEnabled(boolean multiplexingEnabled) {
		this.multiplexingEnabled = multiplexingEnabled;
	}
	
	public long getExpiredInstanceCleanupInterval() {
		return expiredInstanceCleanupInterval;
	}
	
	public void setExpiredInstanceCleanupInterval(long expiredInstanceCleanupInterval) {
		this.expiredInstanceCleanupInterval = expiredInstanceCleanupInterval;
	}
	
	public long getLockRenewalInterval() {
		return lockRenewalInterval;
	}
	
	public void setLockRenewalInterval(long lockRenewalInterval) {
		this.lockRenewalInterval = lockRenewalInterval;
	}
	
	public int getDefaultAnalyzeAfterCount() {
		return defaultAnalyzeAfterCount;
	}
	
	public void setDefaultAnalyzeAfterCount(int defaultAnalyzeAfterCount) {
		this.defaultAnalyzeAfterCount = defaultAnalyzeAfterCount;
	}
	
	protected Set<String> getDataSourceSet() {
		return dataSourceNames.keySet();
	}
	
	public String[] getDataSourceNames() {
		return toDataSourceNameArray(dataSourceEntries);
	}
	
	public void setDataSourceNames(String[] dataSourceNames) {
		if(dataSourceEntries.isEmpty()) {
			if(dataSourceNames != null) {
				int priority = 10;
				for(String dsn : dataSourceNames) {
					dataSourceEntries.add(new DataSourceEntry(dsn, priority--, false));
				}
			}
		} else if(dataSourceNames == null) {
			dataSourceEntries.clear();
		} else {
			dataSourceEntries.subList(dataSourceNames.length, dataSourceEntries.size()).clear();
			for(int i = 0; i < dataSourceEntries.size(); i++) {
				dataSourceEntries.get(i).setName(dataSourceNames[i]);
			}
			int priority = dataSourceEntries.get(dataSourceEntries.size()-1).getPriority() - 1;
			for(int i = dataSourceEntries.size(); i < dataSourceNames.length; i++) {
				dataSourceEntries.add(new DataSourceEntry(dataSourceNames[i], priority--, false));
			}
		}
		updateDataSourceSet();
	}
	
	protected String[] toDataSourceNameArray(List<DataSourceEntry> entries) {
		String[] result = new String[entries.size()];
		int i = 0;
		for(DataSourceEntry entry : entries)
			result[i++] = entry.getName();
		return result;
	}
	
	public int[] getDataSourcePriorities() {
		return toDataSourcePriorityArray(dataSourceEntries);
	}
	
	public void setDataSourcePriorities(int[] dataSourcePriorities) {
		if(dataSourceEntries.isEmpty()) {
			if(dataSourcePriorities != null) {
				for(int p : dataSourcePriorities)
					dataSourceEntries.add(new DataSourceEntry(null, p, false));
			}
		} else if(dataSourcePriorities == null) {
			dataSourceEntries.clear();
		} else {
			if(dataSourcePriorities.length < dataSourceEntries.size())
				dataSourceEntries.subList(dataSourcePriorities.length, dataSourceEntries.size()).clear();
			for(int i = 0; i < dataSourceEntries.size(); i++) {
				dataSourceEntries.get(i).setPriority(dataSourcePriorities[i]);
			}
			for(int i = dataSourceEntries.size(); i < dataSourcePriorities.length; i++) {
				dataSourceEntries.add(new DataSourceEntry(null, dataSourcePriorities[i], false));
			}
		}
		updateDataSourceSet();
	}
	
	protected int[] toDataSourcePriorityArray(List<DataSourceEntry> entries) {
		int[] result = new int[entries.size()];
		int i = 0;
		for(DataSourceEntry entry : entries)
			result[i++] = entry.getPriority();
		return result;
	}
	
	public boolean[] getDataSourcePrimaries() {
		return toDataSourcePrimaryArray(dataSourceEntries);
	}
	
	public void setDataSourcePrimaries(boolean[] dataSourcePrimaries) {
		if(dataSourceEntries.isEmpty()) {
			if(dataSourcePrimaries != null) {
				for(boolean p : dataSourcePrimaries)
					dataSourceEntries.add(new DataSourceEntry(null, p ? 10 : 5, p));
			}
		} else if(dataSourcePrimaries == null) {
			dataSourceEntries.clear();
		} else {
			dataSourceEntries.subList(dataSourcePrimaries.length, dataSourceEntries.size()).clear();
			for(int i = 0; i < dataSourceEntries.size(); i++) {
				dataSourceEntries.get(i).setPrimary(dataSourcePrimaries[i]);
			}
			for(int i = dataSourceEntries.size(); i < dataSourcePrimaries.length; i++) {
				dataSourceEntries.add(new DataSourceEntry(null, dataSourcePrimaries[i] ? 10 : 5,dataSourcePrimaries[i]));
			}
		}
		updateDataSourceSet();
	}
	
	protected boolean[] toDataSourcePrimaryArray(List<DataSourceEntry> entries) {
		boolean[] result = new boolean[entries.size()];
		int i = 0;
		for(DataSourceEntry entry : entries)
			result[i++] = entry.isPrimary();
		return result;
	}
	
	protected void updateDataSourceSet() {
		dataSourceNamePrefix = (StringUtils.isBlank(clusterName) ? "" : null);
		dataSourceNames.clear();
		DataSourceEntry[] sorted = dataSourceEntries.toArray(new DataSourceEntry[dataSourceEntries.size()]);
		Arrays.sort(sorted, dataSourceComparator);
		for(DataSourceEntry dse : sorted) {
			if(dse.getName() != null) {
				dataSourceNames.put(dse.getName(), dse);
				if(dataSourceNamePrefix == null) {
					if(dse.getName().startsWith(clusterName)) {
						char ch = dse.getName().charAt(clusterName.length());
						if(Character.isLetterOrDigit(ch))
							dataSourceNamePrefix = clusterName;
						else
							dataSourceNamePrefix = dse.getName().substring(0, clusterName.length() + 1);
					} else
						dataSourceNamePrefix = "";
				} else if(dataSourceNamePrefix.length() > 0 && !dse.getName().startsWith(dataSourceNamePrefix))
					dataSourceNamePrefix = "";
			}
		}
	}

	public long getQueueSizeRefreshInterval() {
		return queueSizeRefreshInterval;
	}

	public void setQueueSizeRefreshInterval(long queueSizeRefreshInterval) {
		this.queueSizeRefreshInterval = queueSizeRefreshInterval;
	}
	
	/**
	 * Get the postgres cluster name used to name this PeerSupervisor instance in JMX
	 * @return The postgres cluster name Unknown, MST, MSR  
	 */
	public String getClusterName() {
		return clusterName;
	}
	
	/**
	 * Used to set the postgres cluster name from configuration
	 * @param clusterName MST, or MSR.
	 */
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
		updateDataSourceSet(); // may need to update prefix
	}

	public boolean isExactQueueSettings() {
		return exactQueueSettings;
	}

	public void setExactQueueSettings(boolean exactQueueSettings) {
		this.exactQueueSettings = exactQueueSettings;
	}

	@Override
	public Map<String, String> getPreferredDataSources() {
		return preferredDataSources;
	}
	
	public int getLockRenewalThreadPriority() {
		return lockRenewalThreadPriority;
	}

	public void setLockRenewalThreadPriority(int renewThreadPriority) {
		this.lockRenewalThreadPriority = renewThreadPriority;
	}

	public long getLockRenewalTimeBuffer() {
		return lockRenewalTimeBuffer;
	}

	public void setLockRenewalTimeBuffer(long renewAwaitTimeBuffer) {
		this.lockRenewalTimeBuffer = renewAwaitTimeBuffer;
	}

	public int getLockRenewalRetryAttempts() {
		return lockRenewalRetryAttempts;
	}

	public void setLockRenewalRetryAttempts(int lockRenewalRetryAttempts) {
		this.lockRenewalRetryAttempts = lockRenewalRetryAttempts;
	}

	public QueueSettings getQueueSettings(String queueName) {
		return getQueueSettings(queueName, true);
	}

	public long getCleanupShadowBlocksInterval() {
		return cleanupShadowBlocksInterval;
	}

	public void setCleanupShadowBlocksInterval(long cleanupShadowBlocksInterval) {
		this.cleanupShadowBlocksInterval = cleanupShadowBlocksInterval;
	}

	public boolean isLegacyRequestSent() {
		return legacyRequestSent;
	}

	public void setLegacyRequestSent(boolean legacyRequestSent) {
		this.legacyRequestSent = legacyRequestSent;
	}
}
