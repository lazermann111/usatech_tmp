package simple.mq.peer;

import java.io.IOException;

import simple.io.ByteInput;
import simple.io.ByteOutput;

public abstract class DirectPeerMethodData {
	protected DirectPeerMethod method;

	public DirectPeerMethodData(DirectPeerMethod method) {
		this.method = method;
	}

	public DirectPeerMethod getMethod() {
		return method;
	}	
	
	public abstract void readData(ByteInput input) throws IOException ;
    public abstract void writeData(ByteOutput output) throws IOException ;
}
