package simple.mq.peer;


public class HybridQueueKey extends QueueKey {
	protected final String dataSourceName;
	
	public HybridQueueKey(String dataSourceName, String queueName, String subscription) {
		super(queueName, subscription, (dataSourceName == null ? 0 : dataSourceName.hashCode()));
		this.dataSourceName = dataSourceName;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}
}
