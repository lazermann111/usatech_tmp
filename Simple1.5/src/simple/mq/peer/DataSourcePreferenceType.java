package simple.mq.peer;

public enum DataSourcePreferenceType {
	AFFINITY, LOAD;
}
