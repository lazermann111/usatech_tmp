/**
 * 
 */
package simple.mq.peer;

import java.util.concurrent.atomic.AtomicLong;

import simple.io.BinaryStream;
import simple.mq.Message;

public class PublishedPeerMessage implements Message {
	protected static final AtomicLong messageSequenceGenerator = new AtomicLong();
	protected String dataSourceName;
	protected String shadowDataSourceName;
	protected long posted;
	protected final Message delegate;	
	protected final boolean broadcast;
	protected final String messageId;
	protected String subscription;
	protected int retryCount;
	protected final long messageSequence;
	protected final String[] blocks;
	
	public PublishedPeerMessage(Message delegate, boolean broadcast, String instanceId, String[] blocks) {
		this.delegate = delegate;
		this.broadcast = broadcast;
		this.retryCount = delegate.getRetryCount();
		this.messageSequence = messageSequenceGenerator.incrementAndGet();
		this.messageId = instanceId + ':' + Long.toHexString(messageSequence);
		this.blocks = blocks;
	}
	public String getDataSourceName() {
		return dataSourceName;
	}
	public void setDataSourceName(String primaryDataSourceName) {
		this.dataSourceName = primaryDataSourceName;
	}
	public String getShadowDataSourceName() {
		return shadowDataSourceName;
	}
	public void setShadowDataSourceName(String secondaryDataSourceName) {
		this.shadowDataSourceName = secondaryDataSourceName;
	}
	public long getAvailable() {
		return delegate.getAvailable();
	}		
	public long getExpiration(long posted) {
		return delegate.getExpiration(posted);
	}
	public String getQueueName() {
		final String queueName = delegate.getQueueName();
		return queueName == null ? "" : queueName;
	}
	public boolean isRedelivered() {
		return delegate.isRedelivered();
	}
	public boolean isTemporary() {
		return delegate.isTemporary();
	}
	public long getPosted() {
		return posted;
	}
	public void setPosted(long posted) {
		this.posted = posted;
	}
	public BinaryStream getContent() {
		return delegate.getContent();
	}
	public int getRetryCount() {
		return retryCount;
	}
	public byte getPriority() {
		return delegate.getPriority();
	}
	public long getExpiration() {
		return getExpiration(posted);
	}
	public String getMessageId() {
		return messageId;
	}
	public boolean isBroadcast() {
		return broadcast;
	}
	public String getSubscription() {
		return subscription;
	}
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}
	public long getMessageSequence() {
		return messageSequence;
	}

	public Long getCorrelation() {
		return delegate.getCorrelation();
	}

	public String[] getBlocks() {
		return blocks;
	}
}