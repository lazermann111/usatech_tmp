package simple.mq.peer;

import java.io.IOException;

import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.results.DatasetUtils;

public class DirectPeerMethodData_REQUESTDB extends DirectPeerMethodData_REQUEST {
	protected String dataSourceName;
	
	public DirectPeerMethodData_REQUESTDB() {
		super(DirectPeerMethod.REQUESTDB3);
	}
	
	public DirectPeerMethodData_REQUESTDB(DirectPeerMethod method) {
		super(method);
	}

	@Override
	protected void readQueueQualifiers(ByteInput input) throws IOException {
		setDataSourceName(DatasetUtils.readSmallString(input));
	}

	@Override
	protected void writeQueueQualifiers(ByteOutput output) throws IOException {
		DatasetUtils.writeSmallString(output, dataSourceName);
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMethod()).append(" [DataSourceName=").append(dataSourceName).append("; QueueName=").append(queueName);
		if(subscription != null)
			sb.append("; Subscription=").append(subscription);
		sb.append("; Priority=").append(priority).append("; Concurrency=").append(concurrency).append("]");
		return sb.toString();
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
}
