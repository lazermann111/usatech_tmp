package simple.mq.peer;

import simple.util.concurrent.CorrelationControl;

public interface QueueSettings {

	public long getDataPollFrequency();

	public float getPollOnPercentEmpty();

	public int getQueueBufferSize();

	public long getNotificationPollFrequency();

	public long getCleanupInterval();

	public PollType getPollType();

	public long getDirectPublishTimeout();

	public int getDirectPublishAttempts();

	public RedeliveryDetection getRedeliveryDetection();

	public PublishType getDurablePublishType();

	public PublishType getTemporaryPublishType();
	
	public int getAnalyzeAfterCount() ; 
	
	public void setAnalyzeAfterCount(int analyzeAfterCount);

	public CorrelationControl<Long> getCorrelationControl();

	public boolean isLegacyRequestSent();

}