package simple.mq.peer;

public interface PeerQueueMXBean {
	public String getQueueName() ;
	public String getSubscription() ;
	public int getQueueBufferCapacity() ;
	public int getQueueBufferThreshhold() ;
	public int getQueueBufferItems() ;
	public long getDataPollFrequency() ;
	public PollType getPollType() ;
	public String getCorrelationControlInfo();
	public String getPollerInfo();	
}
