/**
 * 
 */
package simple.mq.peer;

public interface Poller {
	public void shutdown() ;
	public void updateConcurrency(String queueName, String subscription, Number concurrency) ;
	public PollerAssistant getAssistant() ;
	public void start() ;
	public int getPriority() ;
	public void setPriority(int newPriority) ;
}