/**
 * 
 */
package simple.mq.peer;

import java.util.Set;

import simple.db.ConnectionGroup;
import simple.db.DataLayer;
import simple.db.DataSourceAvailability;

public interface DbPollerAssistant extends PollerAssistant {	
	public ConnectionGroup createConnectionGroup() ;
	public DataLayer getDataLayer() ;
	public DataSourceAvailability getDataSourceAvailability() ;
	public Set<String> getDataSourceNames() ;

	public String toMessageDataSourceName(String connectionDataSourceName);

	public String toConnectionDataSourceName(String messageDataSourceName);
}