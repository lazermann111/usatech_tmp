package simple.mq.peer;

import java.util.Map;

import simple.mq.MessagingException;

public interface PeerSupervisorMXBean {
	public void carryoverQueue(String failedDataSourceName, String queueName, String subscription) throws MessagingException  ;
	public void carryoverQueues(String failedDataSourceName) throws MessagingException  ;

	public Map<String, String> getPreferredDataSources();
}
