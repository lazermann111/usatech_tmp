/**
 * 
 */
package simple.mq.peer;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.io.Log;

public abstract class AbstractPoller extends Thread implements Poller {
	private static final Log log = Log.getLog();
	protected static final AtomicInteger pollerIdGenerator = new AtomicInteger();
	protected final AtomicBoolean stopped = new AtomicBoolean(false);
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition signal = lock.newCondition();
	protected boolean paused = true;
	protected final String instanceId;
	public AbstractPoller(String instanceId, String label) {
		super();
		this.instanceId = instanceId;
		setName("QL-" + getClass().getSimpleName() + '-' + (label == null ? "" : label + '-') + pollerIdGenerator.incrementAndGet());
		setDaemon(true);
	}
	public void shutdown() {
		if(stopped.compareAndSet(false, true)) {
			interrupt();
		}
	}
	public void run() {
		long delay = 0;
		while(!stopped.get())
			try {
				lock.lockInterruptibly();
				try {
					if(paused) {
						do {
							signal.await();
						} while(paused) ;
					} else if(delay > 0) 
						signal.await(delay, TimeUnit.MILLISECONDS);
				} finally {
					lock.unlock();
				}
				ExitReason exitReason = poll();
				if(stopped.get())
					return;			
				delay = getDelay(exitReason);	
			} catch(InterruptedException e) {
				//Do nothing
			} catch(Throwable e) {
				log.warn("Poller exitted because of EXCEPTION; polling again", e);					
			}
	}
	
	public boolean pause() throws InterruptedException {
		lock.lockInterruptibly();
		try {
			if(!paused) {
				paused = true;
				return true;
			} 
			return false;
		} finally {
			lock.unlock();
		}
	}
	
	public boolean unpause() throws InterruptedException {
		lock.lockInterruptibly();
		try {
			if(paused) {
				paused = false;
				signal.signalAll();
				return true;
			} 
			return false;
		} finally {
			lock.unlock();
		}
	}
	
	public void awaken() throws InterruptedException {
		lock.lockInterruptibly();
		try {
			signal.signalAll();
		} finally {
			lock.unlock();
		}
	}
	
	protected abstract ExitReason poll() ;
	protected abstract long getDelay(ExitReason exitReason) ;
}