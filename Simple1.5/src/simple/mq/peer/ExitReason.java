/**
 * 
 */
package simple.mq.peer;

public enum ExitReason { COULD_NOT_LOCK, CONNECTION_FAILED, EXCEPTION, STOPPED }