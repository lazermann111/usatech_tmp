/**
 * 
 */
package simple.mq.peer;

import java.io.IOException;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import simple.util.concurrent.QueueBuffer;

public interface PollerAssistant {	
	public long getSecondaryReconnectInterval() ;
	public long getPrimaryReconnectInterval() ;
	public ScheduledThreadPoolExecutor getScheduler() ;
	public QueueSettings getQueueSettings(String queueName) ;
	public String getInstanceId() ;

	public QueueBuffer<ConsumedPeerMessage> getQueueBuffer(String queueName, String subscription) throws IOException;
}