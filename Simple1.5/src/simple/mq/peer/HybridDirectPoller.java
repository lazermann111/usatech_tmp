/**
 * 
 */
package simple.mq.peer;

import java.io.IOException;

import javax.management.ObjectName;

import simple.io.Log;

public class HybridDirectPoller extends DirectPoller {
	private static final Log log = Log.getLog();
	protected final HybridPollerAssistant assistant;
			
	public HybridDirectPoller(HybridPollerAssistant assistant, String instanceId, String label, DirectPollerConfig config, ObjectName jmxParentObjectName) {
		super(assistant, instanceId, label, config, jmxParentObjectName);
		this.assistant = assistant;
	}
	@Override
	protected void sendRequest(QueueKey queueKey, int concurrency, boolean initial) throws IOException {
		DirectPeerMethodData_REQUESTDB data;
		if(assistant.getQueueSettings(queueKey.getQueueName()).isLegacyRequestSent())
			data = new DirectPeerMethodData_REQUESTDB(DirectPeerMethod.REQUESTDB);
		else
			data = new DirectPeerMethodData_REQUESTDB();
		data.setInstanceId(instanceId);
		data.setDataSourceName(queueKey.getDataSourceName());
		data.setQueueName(queueKey.getQueueName());
		data.setSubscription(queueKey.getSubscription());
		data.setPriority(config.getPriority());
		data.setConcurrency(concurrency);
		data.writeData(output);
		String msg = "Requested " + concurrency + " hybrid messages for datasource '" + queueKey.getDataSourceName() + "' queue '" + queueKey.getQueueName() + "' " + (queueKey.getSubscription() == null ? "" : "subscription '" + queueKey.getSubscription() + "' ") + "from " + clientId;
		if(initial)
			log.info(msg);
		else
			log.debug(msg);
	}
	
	@Override
	protected void handleData(DirectPeerMethodData data) throws InterruptedException, IOException {
		switch(data.getMethod()) {
			case SENDDB:
			case SENDDB_LONG:
			case SENDDB2:
			case SENDDB2_LONG:
			case SENDDB3:
				DirectPeerMethodData_SENDDB dataSENDDB = (DirectPeerMethodData_SENDDB)data;
				HybridQueueKey queueKey = new HybridQueueKey(dataSENDDB.getDataSourceName(), dataSENDDB.getQueueName(), dataSENDDB.getSubscription());
				HybridDbPoller parentPoller = assistant.getParentPoller(queueKey);
				if(parentPoller == null)
					throw new IOException("ParentPoller not found for datasource '" + dataSENDDB.getDataSourceName() + "' queue '" + dataSENDDB.getQueueName() + "' " + (dataSENDDB.getSubscription() == null ? "" : "subscription '" + dataSENDDB.getSubscription() + "'"));
				parentPoller.processDirectMessage(this, dataSENDDB);		
				break;
			case INQUIREDB:
				DirectPeerMethodData_INQUIREDB dataINQUIREDB = (DirectPeerMethodData_INQUIREDB)data;
				queueKey = new HybridQueueKey(dataINQUIREDB.getDataSourceName(), dataINQUIREDB.getQueueName(), dataINQUIREDB.getSubscription());
				parentPoller = assistant.getParentPoller(queueKey);
				if(parentPoller == null)
					log.info("Publisher inquired about datasource '" + dataINQUIREDB.getDataSourceName() + "' queue '" + dataINQUIREDB.getQueueName() + "' " + (dataINQUIREDB.getSubscription() == null ? "" : "subscription '" + dataINQUIREDB.getSubscription()) + "' but no ParentPoller was found");
				else {
					Integer concurrency = requests.get(queueKey);
					if(concurrency == null || concurrency <= 0)
						log.info("Publisher inquired about datasource '" + dataINQUIREDB.getDataSourceName() + "' queue '" + dataINQUIREDB.getQueueName() + "' " + (dataINQUIREDB.getSubscription() == null ? "" : "subscription '" + dataINQUIREDB.getSubscription()) + "' but there are no consumers for this queue");
					else {
						outputLock.lock();
						try {
							if(output != null)
								sendRequest(queueKey, concurrency, false);
							else
								log.info("Publisher inquired about datasource '" + dataINQUIREDB.getDataSourceName() + "' queue '" + dataINQUIREDB.getQueueName() + "' " + (dataINQUIREDB.getSubscription() == null ? "" : "subscription '" + dataINQUIREDB.getSubscription()) + "' but no output stream is yet set");
						} finally {
							outputLock.unlock();
						}
						parentPoller.awaken();
					}
				}
				break;
			default:
				throw new IOException("Invalid method " + data.getMethod() + " received");
		}
	}
}