package simple.mq.peer;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.Log;
import simple.util.concurrent.ConcurrentLRUMap;
import simple.util.concurrent.QueueBuffer;

public class HybridDbPoller extends DbPoller {
	private static final Log log = Log.getLog();
	protected final HybridPollerAssistant assistant;
	protected final Collection<HybridDirectPoller> hybridDirectPollers;
	protected final ConcurrentLRUMap<String, ConsumedPeerMessage> recentProcessedMessages = new ConcurrentLRUMap<String,ConsumedPeerMessage>(200, 100, 1.0f, 16); 
	protected volatile Number concurrency;
	protected static enum MessageState { INFLIGHT, DB_ACKED, DB_NACKED, HYBRID_ACKED, HYBRID_NACKED }
	protected class HybridConsumedPeerMessage extends DbConsumedPeerMessage {
		protected volatile HybridDirectPoller subPoller;
		protected volatile MessageState state = MessageState.INFLIGHT;
		protected HybridConsumedPeerMessage() {
		}
		@Override
		public void acknowledge(boolean success) {
			super.acknowledge(success);
			if(!success)
				recentProcessedMessages.remove(getMessageId()); // allow retry
			HybridDirectPoller subPoller = this.subPoller;
			if(subPoller != null) {
				//TODO: We may wish to be more strict about retry timing
				subPoller.sendAcknowledge(success || !availableSoon(), this);
				state = (success ? MessageState.HYBRID_ACKED : MessageState.HYBRID_NACKED);		
			} else
				state = (success ? MessageState.DB_ACKED : MessageState.DB_NACKED);
		}
		protected boolean availableSoon() {
			return System.currentTimeMillis() + assistant.getQueueSettings(getQueueName()).getDataPollFrequency() > getAvailable();
		}
		@Override
		public boolean isHybrid() {
			return true;
		}
	};
	public HybridDbPoller(HybridPollerAssistant assistant, String instanceId, String dataSourceName, String queueName, String subscription, boolean primary, int priority, Collection<HybridDirectPoller> hybridDirectPollers) {
		super(assistant.getDbPollerAssistant(), instanceId, dataSourceName, queueName, subscription, primary, priority, true);
		this.hybridDirectPollers = hybridDirectPollers;
		this.assistant = assistant;
		assistant.registerParentPoller(new HybridQueueKey(dataSourceName, queueName, subscription), this);
	}
	
	protected void handleConcurrencyChange(int oldConcurrency, int newConcurrency) {
		this.concurrency = newConcurrency;	
		super.handleConcurrencyChange(oldConcurrency, newConcurrency);
		if(newConcurrency > 0) {
			recentProcessedMessages.setMaxSize(newConcurrency * 5);
		}
	}
	@Override
	protected void lockAcquired() {
		Number concurrency = this.concurrency;
		for(HybridDirectPoller poller : hybridDirectPollers) {
			poller.updateConcurrency(dataSourceName, queueKey.getQueueName(), queueKey.getSubscription(), concurrency == null ? 0 : concurrency);
		}
	}
	@Override
	protected void exitingPollLoop() {
		for(HybridDirectPoller poller : hybridDirectPollers)
			poller.updateConcurrency(dataSourceName, queueKey.getQueueName(), queueKey.getSubscription(), 0);
	}
	@Override
	public void shutdown() {
		for(HybridDirectPoller poller : hybridDirectPollers) {
			poller.updateConcurrency(dataSourceName, queueKey.getQueueName(), queueKey.getSubscription(), 0);
		}
		assistant.deregisterParentPoller(new HybridQueueKey(dataSourceName, queueKey.getQueueName(), queueKey.getSubscription()), this);
		super.shutdown();
	}
	@Override
	protected boolean checkMessage(ConsumedPeerMessage message) {
		// Check correlation
		switch(message.getReadiness()) {
			case LAST:
			case NEVER:
				if(log.isInfoEnabled())
					log.info("Message " + message.getMessageId() + " is limited by its correlation " + message.getCorrelation());
				return false;
		}
		if(recentProcessedMessages.putIfAbsent(message.getMessageId(), message) != null) {
			if(log.isDebugEnabled())
				log.debug("Message " + message.getMessageId() + " already recently received");
			return false;
		}
		if(inflightMessages.putIfAbsent(message.getMessageId(), message) != null) {
			if(log.isDebugEnabled())
				log.debug("Message " + message.getMessageId() + " already received and inflight");
			return false;
		}
		message.receive();
		if(log.isDebugEnabled())
			log.debug("New message " + message.getMessageId() + " received");
		return true;
	}

	protected boolean checkMessage(ConsumedPeerMessage message, HybridDirectPoller subPoller) {
		if(!lockAcquired) {
			log.info("Received message '" + message.getMessageId() + "', but this poller does not have the lock for its queue");
			subPoller.sendAcknowledge(false, message);
			return false;
		}
		// Check correlation
		switch(message.getReadiness()) {
			case LAST:
			case NEVER:
				handleHybridMessage((HybridConsumedPeerMessage) message, subPoller);
				if(log.isInfoEnabled())
					log.info("Message " + message.getMessageId() + " is limited by its correlation " + message.getCorrelation());
				return false;
		}
		if(message.getBlocks() != null && message.getBlocks().length > 0) {
			int cnt = getBlockCount(message.getBlocks());
			if(cnt != 0) {
				handleHybridMessage((HybridConsumedPeerMessage) message, subPoller);
				if(log.isInfoEnabled())
					log.info("Message " + message.getMessageId() + " is blocked by " + Arrays.toString(message.getBlocks()) + "; waiting for blocks to clear");
				return false;
			}
		}
		ConsumedPeerMessage old = recentProcessedMessages.putIfAbsent(message.getMessageId(), message);
		if(old != null) {
			handleHybridMessage((HybridConsumedPeerMessage) old, subPoller);
			if(log.isDebugEnabled())
				log.debug("Message " + message.getMessageId() + " already recently received");
			return false;
		}
		old = inflightMessages.putIfAbsent(message.getMessageId(), message);
		if(old != null) {
			handleHybridMessage((HybridConsumedPeerMessage)old, subPoller);
			if(log.isDebugEnabled())
				log.debug("Message " + message.getMessageId() + " already received and inflight");
			return false;
		}

		message.receive();
		if(log.isDebugEnabled())
			log.debug("New message " + message.getMessageId() + " received");
		return true;
	}
	
	protected int getBlockCount(String[] blocks) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("blocks", blocks);
		Connection conn;
		try {
			conn = getPollerConnection();
		} catch(PollerException e) {
			log.warn("Could not get poller db connection to check blocks", e);
			return -1;
		}
		try {
			DataLayerMgr.executeCall(conn, "GET_BLOCK_COUNT", params);
			return ConvertUtils.getInt(params.get("count"));
		} catch(ConvertException e) {
			log.warn("Could not convert result to check blocks", e);
			return -1;
		} catch(SQLException e) {
			log.warn("Could not execute sql to check blocks", e);
			return -1;
		} catch(DataLayerException e) {
			log.warn("Could not execute sql to check blocks", e);
			return -1;
		} finally {
			DbUtils.closeSafely(conn);
		}
	}
	protected void handleHybridMessage(HybridConsumedPeerMessage hybridMessage, HybridDirectPoller subPoller) {
		MessageState state = hybridMessage.state;
		switch(state) {
			case DB_ACKED:
				hybridMessage.subPoller = subPoller;
			case HYBRID_ACKED:
				subPoller.sendAcknowledge(true, hybridMessage);
				hybridMessage.state = MessageState.HYBRID_ACKED;
				break;
			case DB_NACKED:
				hybridMessage.subPoller = subPoller;
			case HYBRID_NACKED:
				subPoller.sendAcknowledge(false, hybridMessage);
				hybridMessage.state = MessageState.HYBRID_NACKED;
				break;
			case INFLIGHT:
				hybridMessage.subPoller = subPoller;
				if(hybridMessage.state != state)
					handleHybridMessage(hybridMessage, subPoller);
				break;
		}
	}
	protected void undoMessageId(String messageId) {
		inflightMessages.remove(messageId);
		recentProcessedMessages.remove(messageId);
	}
	@Override
	protected HybridConsumedPeerMessage createMessage() {
		return new HybridConsumedPeerMessage();
	}
	public void processDirectMessage(HybridDirectPoller subPoller, DirectPeerMethodData_SENDDB dataSENDDB) throws InterruptedException, IOException {
		HybridConsumedPeerMessage message = readDirectMessage(dataSENDDB);
		message.subPoller = subPoller;
		if(checkMessage(message, subPoller)) {
			boolean okay = false;
			try {
				QueueBuffer<ConsumedPeerMessage> queueBuffer = assistant.getQueueBuffer(message.getQueueName(), message.getSubscription());
				if(log.isDebugEnabled())
					log.debug("Adding message " + message.getMessageId() + " to QueueBuffer for " + message.getQueueName() + "; count=" + queueBuffer.size());
				queueBuffer.put(message, false);
				okay = true;
			} finally {
				if(!okay) {
					undoMessageId(message.getMessageId());
					subPoller.sendAcknowledge(false, message);
				}
			}
		}	
	}
	/**
	 * @throws IOException  
	 */
	protected HybridConsumedPeerMessage readDirectMessage(DirectPeerMethodData_SENDDB data) {
		HybridConsumedPeerMessage message = createMessage();
		data.updateMessage(message);
		message.setSubscription(data.getSubscription());
		message.setRedelivered(data.getRetryCount() > 0);
		message.setTemporary(message.getShadowDataSourceName() == null);
		if(log.isDebugEnabled())
			log.debug("Read new message hybrid '" + message.getMessageId() + "'");
		return message;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append(" (concurrency=").append(concurrency).append(")");
		if (hybridDirectPollers != null && !hybridDirectPollers.isEmpty()) {
			sb.append("\r\n");
			boolean first = true;
			for(HybridDirectPoller hdp : hybridDirectPollers) {
				if (!first)
					sb.append("\r\n");
				sb.append(" ").append(hdp.toString());
				first = false;
			}
		}
		return sb.toString();
	}
}
