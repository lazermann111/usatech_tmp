package simple.mq.peer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import simple.io.ByteArrayUtils;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.lang.InvalidByteValueException;
import simple.text.StringUtils;

public class DirectPeerUtils {
	protected static final byte[] DIRECT_MAGIC = ByteArrayUtils.fromHex("DEC0DE11");
	
	public static DirectPeerMethod readMethod(ByteInput input) throws IOException, InvalidByteValueException {
		byte[] magic = new byte[DIRECT_MAGIC.length];
		input.readBytes(magic, 0, magic.length);
		if(!Arrays.equals(magic, DIRECT_MAGIC)) {
			throw new IOException("Inconsistent state; expected magic bytes but got '" + StringUtils.toHex(magic) + "'");
		}
		byte method = input.readByte();
		return DirectPeerMethod.getByValue(method);
	}

	public static DirectPeerMethodData readMethodData(ByteInput input)  throws IOException, InvalidByteValueException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException  {
		DirectPeerMethod method = readMethod(input);
		DirectPeerMethodData data = method.createData();
		data.readData(input);
		return data;
	}

	public static void writeMethod(ByteOutput output, DirectPeerMethod method) throws IOException {
		output.write(DIRECT_MAGIC);
		output.writeByte(method.getValue());	
	}
	
	public static int getHeaderLength() {
		return DIRECT_MAGIC.length + 1;
	}
}
