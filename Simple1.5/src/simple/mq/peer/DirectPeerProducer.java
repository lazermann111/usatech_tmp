package simple.mq.peer;

import java.io.EOFException;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.channels.ByteChannel;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import javax.management.ObjectName;

import simple.app.LongTotals;
import simple.app.RollingAverage;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.ByteChannelByteInput;
import simple.io.ByteChannelByteOutput;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.io.Log;
import simple.io.server.AbstractBufferInterpretter;
import simple.io.server.NIOServer;
import simple.io.server.SelfProcessingInterpretter;
import simple.lang.Initializer;
import simple.lang.InvalidByteValueException;
import simple.mq.MessagingException;
import simple.text.MessageFormat;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.util.Cycle;
import simple.util.concurrent.*;

public class DirectPeerProducer implements DirectPeerProducerMXBean {
	private static final Log log = Log.getLog();
	private static ThreadSafeDateFormat DATE_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS"));
	
	protected static final String jmxNamePrefix = DirectPeerProducer.class.getPackage().getName() + ":Type=" + DirectPeerProducer.class.getSimpleName();
	protected static final AtomicInteger lastThreadId = new AtomicInteger();

	protected final List<ProducerThread> threads = new LockOnWriteList<ProducerThread>(); //new ArrayList<WorkQueueServiceThread>();	
	protected String listenAddressString;
	protected int numThreads = 5;
	protected long reprioritizeInterval = TimeUnit.SECONDS.toMillis(10);
	protected int latencyRollingAverageSize = 100;
	protected long minLatencyDiff = 50;
	protected long inactivityResetTime = TimeUnit.MINUTES.toMillis(5);
	protected int maxRetryCount = 5;
	protected int estimatedConnCount = 5;
	protected DataSourcePreferenceType dataSourcePreferenceType = DataSourcePreferenceType.LOAD;
	protected long outputWriteTimeout = 4000;
	protected long socketWriteTimeout = 1000;
	protected int bufferSize = 2048;
	protected ObjectName jmxObjectName;

	protected final NIOServer<DirectPeerInterpretter> server = new NIOServer<DirectPeerInterpretter>() {
		@Override
		protected DirectPeerInterpretter createInterpretter(Socket socket, ByteChannel byteChannel) throws IOException {
			return new DirectPeerInterpretter(bufferSize, socket, byteChannel);
		}

		@Override
		public String getConnectionInfo() {
			StringBuilder sb = new StringBuilder();
			sb.append("CONNECTIONS -----------------------\r\n");
			sb.append(super.getConnectionInfo());
			sb.append("DIRECT PRODUCERS ------------------\r\n");
			for (String s: getDirectProducersInfo())
				sb.append(s).append("\r\n");
			sb.append("HYBRID PRODUCERS ------------------\r\n");
			for (String s: getHybridProducersInfo())
				sb.append(s).append("\r\n");
			sb.append("HYBRID PRIORITIZERS ---------------\r\n");
			for (String s: getHybridPrioritizerInfo())
				sb.append(s).append("\r\n");
			return sb.toString();
		}

		@Override
		protected int getSocketReadTimeout() {
			return 0;
		}

		protected int getSocketWriteTimeout() {
			return (int) socketWriteTimeout;
		}
	};

	protected class ProducerThread extends Thread {
		protected final AtomicBoolean stopped = new AtomicBoolean();

		public ProducerThread() {
			super("DirectPeerProducer Thread #" + lastThreadId.incrementAndGet());
			setDaemon(true);
			setPriority(Thread.NORM_PRIORITY + 1);
			threads.add(this);
		}

		@Override
		public void run() {
			while (!stopped.get())
				try {
					final DirectPeerInterpretter dpi = server.borrowObject();
					try {
						dpi.process();
					} finally {
						server.returnObject(dpi);
					}
				} catch (InterruptedException e) {
					//ignore
				} catch (TimeoutException e) {
					log.warn("Could not get next direct peer message to process", e);
				} catch (CreationException e) {
					log.warn("Could not get next direct peer message to process", e);
				} catch (Throwable e) {
					log.fatal("Unexpected error", e);
				}
			threads.remove(this);
		}

		public void shutdown(boolean force) {
			stopped.set(true);
			if (force)
				interrupt();
		}
	}

	protected final SegmentCache<String, Instance, RuntimeException> directInstancesCache = new LockSegmentCache<String, Instance, RuntimeException>() {
		@Override
		protected Instance createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new Instance(key, false);
		}
	};

	protected final SegmentCache<String, Instance, RuntimeException> hybridInstancesCache = new LockSegmentCache<String, Instance, RuntimeException>() {
		@Override
		protected Instance createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new Instance(key, true);
		}
	};

	protected final Comparator<InstanceInfo> priorityComparator = new Comparator<InstanceInfo>() {
		@Override
		public int compare(InstanceInfo o1, InstanceInfo o2) {
			switch (getDataSourcePreferenceType()) {
				case AFFINITY:
					if (o1 == o2)
						return 0;
					int c = Double.compare(o2.priority, o1.priority);
					if (c != 0)
						return c;
					c = o1.dataSourceName.compareToIgnoreCase(o2.dataSourceName);
					if (c != 0)
						return c;
					return o1.instance.instanceId.compareTo(o2.instance.instanceId);
				case LOAD:
				default:
					return o1.compareTo(o2);
			}
		}
	};

	protected class InstanceInfoPrioritizer implements Iterable<String>, Runnable {
		protected final String queueName;
		protected InstanceInfo[] ordered = new InstanceInfo[0];
		protected final ReentrantLock lock = new ReentrantLock();
		protected final AtomicLong lastPrioritizedTs = new AtomicLong(System.currentTimeMillis());
		protected final AtomicBoolean reprioritizeFlag = new AtomicBoolean(); // to only reorder once if multiple tasks are submitted (because there is a backlog)
		
		public InstanceInfoPrioritizer(String queueName) {
			this.queueName = queueName;
		}

		public void run() {
			lock.lock();
			try {
				if (!reprioritizeFlag.compareAndSet(true, false))
					return;
				// first check for expired latencies
				hybridQueueProducersCache.entrySet().stream()
					.filter(e -> e.getKey().queueName.equals(queueName))
					.forEach(e -> e.getValue().values()
						.forEach(qp -> checkLatencies(qp.getInstances())));
				Map<String, InstanceInfo> bestInstancePerDataSourceName = new HashMap<>();
				// this collection stream collects all InstanceInfos for the queue per dataSource, sorts them, and gets the first
				hybridQueueProducersCache.entrySet().stream()
					.filter(e -> e.getKey().queueName.equals(queueName))
					.forEach(e -> bestInstancePerDataSourceName.put(
						e.getKey().dataSourceName, 
						e.getValue().values().stream()
							.flatMap(e2 -> e2.instances.values().stream())
							.sorted(priorityComparator).findFirst().orElse(null)));
				// now that we have the best instanceInfo per dataSource we just sort them using priorityComparator and transform to an array
				ordered = bestInstancePerDataSourceName.values().stream().filter(ii -> ii != null).sorted(priorityComparator).toArray(InstanceInfo[]::new);
				if (ordered == null || ordered.length == 0) {
					log.warn("No peers are registered for {0}", queueName);
				} else if (ordered.length > 1) {
					log.info("Best dataSource for {0} is {1} of {2} peers: {3}", queueName, ordered[0].dataSourceName, ordered.length, Arrays.toString(ordered));
				}
//				else {
//					log.debug("Only dataSource for {0} is {1} of {2} peers: {3}", queueName, ordered[0].dataSourceName, ordered.length, Arrays.toString(ordered));
//				}
				lastPrioritizedTs.set(System.currentTimeMillis());
			} catch (Throwable e) {
				log.error("Reprioritize {0} failed! hybridQueueProducersCache={1}", this, hybridQueueProducersCache, e);
			} finally {
				lock.unlock();
			}
		}

		protected void reprioritize() {
			reprioritizeFlag.set(true);
			executor.submit(this);
		}
		
		public String toString() {
			return MessageFormat.format("{0}(best={1}, lastPrioritizedAgo={2}, ordered={3})", 
					queueName, 
					(ordered != null && ordered.length > 0 ? ordered[0].dataSourceName : ""), 
					(System.currentTimeMillis()-lastPrioritizedTs.get()), 
					Arrays.toString(ordered));
		}

		@Override
		public Iterator<String> iterator() {
			if ((System.currentTimeMillis() - lastPrioritizedTs.get()) >= reprioritizeInterval) {
				reprioritize();
			}

			final InstanceInfo[] finalOrdered = ordered;
			return new Iterator<String>() {
				protected int index = 0;

				@Override
				public boolean hasNext() {
					return index < finalOrdered.length && finalOrdered[index] != null;
				}

				@Override
				public String next() {
					return finalOrdered[index++].dataSourceName;
				}

				@Override
				public void remove() {
					throw new UnsupportedOperationException("Not modifiable");
				}
			};
		}
	}

	protected final SegmentCache<String, InstanceInfoPrioritizer, RuntimeException> hybridPrioritizerCache = new LockSegmentCache<String, InstanceInfoPrioritizer, RuntimeException>() {
		@Override
		protected InstanceInfoPrioritizer createValue(String key, Object... additionalInfo) throws RuntimeException {
			return new InstanceInfoPrioritizer(key);
		}
	};

	protected final ThreadPoolExecutor executor = new ThreadPoolExecutor(3, 15, 1000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new CustomThreadFactory("AsyncDirectPeerProducer Thread #", true, 5));

	protected final Initializer<ServiceException> initializer = new Initializer<ServiceException>() {
		@Override
		protected void doInitialize() throws ServiceException {
			try {
				server.initialize();
			} catch (InterruptedException e) {
				throw new ServiceException(e);
			} catch (IOException e) {
				throw new ServiceException(e);
			}
			startThreads(numThreads);
			
			try {
				ObjectName jmxObjectName = new ObjectName(MessageFormat.format("{0},name={1}", jmxNamePrefix, StringUtils.prepareJMXNameValue(listenAddressString)));
				ManagementFactory.getPlatformMBeanServer().registerMBean(DirectPeerProducer.this, jmxObjectName);
				DirectPeerProducer.this.jmxObjectName = jmxObjectName;
			} catch(Exception e) {
				log.warn("Could not register DirectPeerProducer {0} in JMX", listenAddressString, e);
			}
		}

		@Override
		protected void doReset() {
			server.close();
			stopAllThreads(true, 5000);
		}
	};

	protected static enum DPIStatus {
		NEW,
		ATTACHED,
		CLOSED
	};

	protected static enum DPIReadState {
		NEW,
		METHOD,
		DATA
	};

	protected class DirectPeerInterpretter extends AbstractBufferInterpretter implements SelfProcessingInterpretter {
		protected final ReentrantLock outputLock = new ReentrantLock();
		protected final AtomicInteger outputCount = new AtomicInteger();
		protected final ReentrantLock regLock = new ReentrantLock();
		protected final ByteOutput output;
		protected final ByteInput input;
		protected DirectPeerMethod method;
		protected DirectPeerMethodData data;
		protected final String clientId;
		protected final ByteChannel byteChannel;
		protected DPIReadState state = DPIReadState.NEW;
		protected int index = -1;
		protected Instance instance;
		protected DPIStatus status = DPIStatus.NEW;
		protected long total;

		public DirectPeerInterpretter(int inputBufferSize, Socket socket, ByteChannel byteChannel) {
			super(inputBufferSize);
			clientId = socket.getRemoteSocketAddress().toString() + " on port " + socket.getLocalPort();
			// input = new ByteBufferByteInput(bufferReadOnly);
			input = new ByteChannelByteInput(byteChannel, bufferReadOnly, buffer.array(), buffer.arrayOffset());
			output = new ByteChannelByteOutput(byteChannel, inputBufferSize);
			this.byteChannel = byteChannel;
		}

		@Override
		//only one thread accesses this method at a time, but another thread may be accessing other methods
		protected int findEOR(int newBytes) throws IOException {
			if (newBytes < 1)
				return -1;
			total += newBytes;
			switch (state) {
				case NEW:
					if (bufferReadOnly.remaining() < DirectPeerUtils.getHeaderLength())
						return -1;
					if (log.isTraceEnabled())
						log.trace("Reading magic at position " + (total - newBytes) + " of " + clientId);
					try {
						method = DirectPeerUtils.readMethod(input);
					} catch (InvalidByteValueException e) {
						throw new IOException("Invalid peer method specified", e);
					}
					state = DPIReadState.METHOD;
					// fall thru
				case METHOD:
					try {
						data = method.createData();
					} catch (IllegalArgumentException e) {
						throw new IOException("Could not create method data", e);
					} catch (InstantiationException e) {
						throw new IOException("Could not create method data", e);
					} catch (IllegalAccessException e) {
						throw new IOException("Could not create method data", e);
					} catch (InvocationTargetException e) {
						throw new IOException("Could not create method data", e);
					}
					state = DPIReadState.DATA;
					// fall thru
				case DATA:
					bufferReadOnly.mark();
					try {
						data.readData(input);
					} catch (EOFException e) {
						if (log.isInfoEnabled())
							log.info("EOF in input while reading " + data.getMethod() + " at " + bufferReadOnly.position(), e);
						bufferReadOnly.reset();
						return -1;
					}
					// if we read into bufferReadOnly from ByteChannel then we need to update buffer
					buffer.position(bufferReadOnly.limit());
					state = DPIReadState.NEW;
					break;
			}
			return bufferReadOnly.position();
		}

		@Override
		public void close(boolean clientInitiated) {
			if (byteChannel.isOpen())
				try {
					byteChannel.close();
				} catch (IOException e) {
					if (log.isInfoEnabled())
						log.info("Could not close channel '" + clientId + "' because: " + e.getMessage());
				}
			super.close(clientInitiated);
			cleanup();
		}

		protected void cleanup() {
			regLock.lock();
			try {
				switch (status) {
					case NEW:
						status = DPIStatus.CLOSED;
						break;
					case ATTACHED:
						if (instance == null || index < 0)
							log.warn("Invalid status for " + clientId + "; instance or index not set");
						else {
							if (instance.pool.remove(index, this)) {
								if (instance.pool.isEmpty())
									instance.close();
								if (log.isInfoEnabled())
									log.info("Removed " + clientId + " from pool for " + (instance.hybrid ? "hybrid" : "direct") + " instance " + instance.instanceId + " at index " + index + " of " + instance.pool.size());
							} else log.warn("Channel " + clientId + " was not registered in pool for " + (instance.hybrid ? "hybrid" : "direct") + " instance " + instance.instanceId + " at index " + index + " of " + instance.pool.size());
							instance = null;
							index = -1;
							status = DPIStatus.CLOSED;
						}
						break;
					case CLOSED:
						if (log.isInfoEnabled())
							log.info("Channel " + clientId + " is already closed");
						break;
				}
			} finally {
				regLock.unlock();
			}
		}

		public boolean isOpen() {
			if (byteChannel.isOpen())
				return true;
			close(false);
			return false;
		}

		@Override
		public String toString() {
			Instance instance = this.instance;
			if (instance == null)
				return "NONE";
			else 
				return MessageFormat.format("{0}(method={1}, data={2}, state={3}, status={4})", instance.instanceId, method, data, state, status);
		}

		public void flushOutput() {
			// do nothing

		}

		public boolean prepareCurrent() throws IOException {
			return true;
		}

		@Override
		public boolean prepareNext() {
			method = null;
			data = null;
			return super.prepareNext();
		}

		//only one thread accesses this method at a time, but another thread may be accessing other methods
		public void process() {
			switch (method) {
				case ACK:
					DirectPeerMethodData_ACK dataACK = (DirectPeerMethodData_ACK)data;
					Instance instance = this.instance;
					if (instance == null) {
						if (log.isInfoEnabled())
							log.info("Received acknowledge for message " + dataACK.getMessageId() + " on closed connection " + clientId);
					} else {
						PublishInfo publishInfo = instance.pendingMessages.remove(dataACK.getMessageId());
						if (publishInfo == null) {
							if (log.isInfoEnabled())
								log.info("Received acknowledge for message not sent: " + dataACK.getMessageId() + " for " + clientId);
						} else {
							long latency = System.currentTimeMillis() - publishInfo.message.getPosted();
							publishInfo.instanceInfo.updateLatency(latency);
							if (log.isDebugEnabled())
								log.debug("Received ACK for message {0} with latency {1}. {2} messages still in-process for {3}", dataACK.getMessageId(), latency, instance.pendingMessages.size(), clientId);
						}
					}
					break;
				case NAK:
					DirectPeerMethodData_NAK dataNAK = (DirectPeerMethodData_NAK)data;
					if (log.isInfoEnabled())
						log.info("Received NAK for message " + dataNAK.getMessageId());
					instance = this.instance;
					if (instance == null) {
						if (log.isInfoEnabled())
							log.info("Received NAK for message not sent: " + dataNAK.getMessageId() + " for " + clientId);
					} else {
						PublishInfo publishInfo = instance.pendingMessages.remove(dataNAK.getMessageId());
						if (publishInfo == null) {
							if (log.isInfoEnabled())
								log.info("Received NAK for message not sent: " + dataNAK.getMessageId() + " for " + clientId);
						} else {
							publishInfo.queueProducer.receivedNak.set(true); //cause re-calc of best
							long origPosted = publishInfo.message.getPosted();
							long latency = publishInfo.message.getPosted() - origPosted;
							instance.resendMessage(publishInfo);
							// don't use NAK for latency, they are usually slow and not a good indicator of 
							//publishInfo.instanceInfo.updateLatency(latency);
							if (log.isInfoEnabled())
								log.info("Received NAK for message {0} with latency {1}. {2} messages still in-process for {3}", dataNAK.getMessageId(), latency, instance.pendingMessages.size(), clientId);
						}
					}
					break;
				case REQUEST:
				case REQUEST3:
					DirectPeerMethodData_REQUEST dataREQUEST = (DirectPeerMethodData_REQUEST)data;
					handleRequest(dataREQUEST.getInstanceId(), dataREQUEST.getConcurrency(), dataREQUEST.getPriority(), null, dataREQUEST.getQueueName(), dataREQUEST.getSubscription(), false, dataREQUEST.getVersion());
					break;
				case REQUESTDB:
				case REQUESTDB3:
					DirectPeerMethodData_REQUESTDB dataREQUESTDB = (DirectPeerMethodData_REQUESTDB)data;
					handleRequest(dataREQUESTDB.getInstanceId(), dataREQUESTDB.getConcurrency(), dataREQUESTDB.getPriority(), dataREQUESTDB.getDataSourceName(), dataREQUESTDB.getQueueName(), dataREQUESTDB.getSubscription(), true, dataREQUESTDB.getVersion());
					break;
				default:
					log.warn("Received unsupported Peer Method " + method + "; ignoring");
			}
		}

		protected Instance registerInstance(String instanceId, boolean hybrid) throws IOException {
			regLock.lock();
			try {
				switch (status) {
					case ATTACHED:
						if (instance != null && !instance.instanceId.equals(instanceId))
							throw new IOException("Instance has changed! Was " + instance.instanceId + " now is " + instanceId);
						if (log.isTraceEnabled())
							log.trace("Connection " + clientId + " for " + (hybrid ? "hybrid" : "direct") + " instance " + instanceId + " at index " + index + " of " + instance.pool.size() + " was already attached");
						return instance;
					case NEW:
						instance = (hybrid ? hybridInstancesCache : directInstancesCache).getOrCreate(instanceId);
						index = instance.pool.add(this);
						status = DPIStatus.ATTACHED;
						if (log.isInfoEnabled())
							log.info("Added " + clientId + " to pool for " + (hybrid ? "hybrid" : "direct") + " instance " + instanceId + " at index " + index + " of " + instance.pool.size());
						return instance;
					case CLOSED:
						if (log.isInfoEnabled())
							log.info("Connection " + clientId + " for " + (hybrid ? "hybrid" : "direct") + " instance " + instanceId + " at index " + index + " is closed - ignoring");
						break;

				}
			} finally {
				regLock.unlock();
			}
			return null;
		}

		protected void handleRequest(String instanceId, int concurrency, byte priority, String dataSourceName, String queueName, String subscription, boolean hybrid, int version) {
			Instance instance;
			try {
				instance = registerInstance(instanceId, hybrid);
			} catch (IOException e) {
				log.error("Could not register instance " + instanceId + "; closing connection", e);
				close(false);
				return;
			}
			if (instance == null) {
				log.info("Instance {0} is closing; not registering request for {1} on datasource {2} queue {3}", instanceId, clientId, dataSourceName, queueName);
				return;
			}
			if (instance.getVersion() < version)
				instance.setVersion(version);
			if (concurrency < 1) {
				QueueProducer queueProducer = getIfPresentProducer(hybrid, dataSourceName, queueName, subscription);
				if (queueProducer == null) {
					log.info("Received request with no concurrency for instance {0} on {1} on datasource {2} queue {3}: not registering", instanceId, clientId, dataSourceName, queueName);
				} else {
					log.info("Received request with no concurrency for instance {0} on {1} on datasource {2} queue {3}: removing from registry", instanceId, clientId, dataSourceName, queueName);
					queueProducer.deregisterInstance(instance);
				}
			} else {
				QueueProducer queueProducer = getOrCreateProducer(hybrid, dataSourceName, queueName, subscription);
				boolean updated = queueProducer.registerInstance(instance, priority, concurrency, dataSourceName);
				if (updated) {
					log.info("Registered instance {0} on {1} to receive {2} {3} messages on datasource {4} queue {5}", instanceId, clientId, concurrency, (hybrid ? "hybrid" : "direct"), dataSourceName, queueName);
				} else if (log.isTraceEnabled()) {
					log.trace("Re-registered instance {0} on {1} to receive {2} {3} messages on datasource {4} queue {5}", instanceId, clientId, concurrency, (hybrid ? "hybrid" : "direct"), dataSourceName, queueName);
				}
			}
		}

		public void writeData(DirectPeerMethodData data) throws IOException {
			while (true) {
				int origCount = outputCount.get();
				try {
					if (outputLock.tryLock(outputWriteTimeout, TimeUnit.MILLISECONDS))
						try {
							data.writeData(output);
							outputCount.incrementAndGet();
							return;
						} finally {
							outputLock.unlock();
						}
					else if (origCount == outputCount.get()) {
						close(false);
						throw new IOException("Could not obtain write lock on output in " + outputWriteTimeout + "ms");
					}
				} catch (InterruptedException e) {
					// just try again
				}
			}
		}
	}

	protected class PublishInfo {
		public final PublishedPeerMessage message;
		public final QueueProducer queueProducer;
		public final InstanceInfo instanceInfo;
		public final long resendTimeout;
		public final int attempts;
		public final boolean require;

		public PublishInfo(PublishedPeerMessage message, QueueProducer queueProducer, InstanceInfo instanceInfo, long resendTimeout, int attempts, boolean require) {
			this.message = message;
			this.queueProducer = queueProducer;
			this.instanceInfo = instanceInfo;
			this.resendTimeout = resendTimeout;
			this.attempts = attempts;
			this.require = require;
		}
	}

	protected class Instance {
		protected int version;
		protected final Cycle<DirectPeerInterpretter> pool = new SingleLockArrayCycle<DirectPeerInterpretter>(getEstimatedConnCount() + 1, DirectPeerInterpretter.class);

		protected final ConcurrentMap<String, PublishInfo> pendingMessages = new ConcurrentHashMap<String, PublishInfo>() {
			private static final long serialVersionUID = 1855697254949829228L;

			@Override
			public PublishInfo put(String key, PublishInfo value) {
				PublishInfo old = super.put(key, value);
				handleAdd(value);
				handleRemove(old);
				return old;
			}

			@Override
			public PublishInfo putIfAbsent(String key, PublishInfo value) {
				PublishInfo old = super.putIfAbsent(key, value);
				handleAdd(value);
				handleRemove(old);
				return old;
			}

			@Override
			public PublishInfo remove(Object key) {
				PublishInfo old = super.remove(key);
				handleRemove(old);
				return old;
			}

			@Override
			public boolean remove(Object key, Object value) {
				boolean success = super.remove(key, value);
				if (success && value instanceof PublishInfo)
					handleRemove((PublishInfo)value);
				return success;
			}

			@Override
			public boolean replace(String key, PublishInfo oldValue, PublishInfo newValue) {
				boolean success = super.replace(key, oldValue, newValue);
				if (success) {
					handleAdd(newValue);
					handleRemove(oldValue);
				}
				return success;
			}

			@Override
			public PublishInfo replace(String key, PublishInfo value) {
				PublishInfo old = super.replace(key, value);
				handleAdd(value);
				handleRemove(old);
				return old;
			}

			protected void handleRemove(PublishInfo removed) {
				if (removed == null)
					return;
				removed.instanceInfo.pendingRemoved();
			}

			protected void handleAdd(PublishInfo added) {
				if (added == null)
					return;
				added.instanceInfo.pendingAdded();
			}
		};

		protected final String instanceId;
		protected final boolean hybrid;

		public Instance(String instanceId, boolean hybrid) {
			this.instanceId = instanceId;
			this.hybrid = hybrid;
		}

		public void close() {
			log.info("Closing instance '" + instanceId + "'");

			if (hybrid) {
				hybridInstancesCache.remove(instanceId, this);
				for (Cache<String, QueueProducer, RuntimeException> queueProducers: hybridQueueProducersCache.values())
					for (QueueProducer queueProducer: queueProducers.values())
						queueProducer.deregisterInstance(this);// this is kind of a trick in that InstanceInfo with this instance equals this instance
			} else {
				directInstancesCache.remove(instanceId, this);
				for (Cache<String, QueueProducer, RuntimeException> queueProducers: queueProducersCache.values())
					for (QueueProducer queueProducer: queueProducers.values())
						queueProducer.deregisterInstance(this);// this is kind of a trick in that InstanceInfo with this instance equals this instance
			}

			// attempt to re-send all pending messages
			for (String messageId: pendingMessages.keySet()) {
				PublishInfo publishInfo = pendingMessages.remove(messageId);
				if (publishInfo != null)
					resendMessage(publishInfo);
			}

			// It would be unusual for any dpi's to be in the pool but just to make sure, close them
			for (DirectPeerInterpretter dpi: pool.drain()) {
				dpi.close(false);
			}
		}

		protected void resendMessage(PublishInfo publishInfo) {
			if (publishInfo.message.getRetryCount() >= getMaxRetryCount()) {
				log.warn("Message " + publishInfo.message.getMessageId() + " failed processing " + publishInfo.message.getRetryCount() + " times; Not retrying.");
				return;
			}

			if (log.isInfoEnabled())
				log.info("Resending message " + publishInfo.message.getMessageId());

			long time = System.currentTimeMillis();
			publishInfo.message.setPosted(time);
			publishInfo.message.setRetryCount(publishInfo.message.getRetryCount() + 1);

			try {
				publishInfo.queueProducer.sendMessage(publishInfo.message, publishInfo.resendTimeout, publishInfo.attempts, publishInfo.require);
			} catch (MessagingException e) {
				log.warn("Failed to re-publish message: " + publishInfo.message.getMessageId(), e);
			}
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof InstanceInfo)
				return equals(((InstanceInfo)obj).instance);
			return super.equals(obj);
		}

		@Override
		public int hashCode() {
			return instanceId.hashCode();
		}

		public int getVersion() {
			return version;
		}

		public void setVersion(int version) {
			this.version = version;
		}
		
		public String toString() {
			return MessageFormat.format("{0}(version={1}, type={2}, poolSize={3}, pending={4})", instanceId, version, (hybrid ? "hybrid" : "direct"), pool.size(), pendingMessages.size());
		}
	}

	protected class InstanceInfo implements Comparable<InstanceInfo> {
		public final Instance instance;
		public final AtomicInteger pending = new AtomicInteger();
		public final double priority;
		public final String dataSourceName;
		public volatile int concurrency;
		protected final ReentrantLock lock = new ReentrantLock();
		protected final Condition notFull = lock.newCondition();
		protected RollingAverage<Number> latencyAverage;
		protected AtomicLong lastLatency = new AtomicLong();
		protected AtomicLong lastLatencyTs = new AtomicLong(System.currentTimeMillis());

		public InstanceInfo(Instance instance, byte priority, int concurrency, String dataSourceName) {
			this.instance = instance;
			this.priority = priority;
			this.concurrency = concurrency;
			this.dataSourceName = dataSourceName;
			latencyAverage = new RollingAverage<Number>(new LongTotals(), latencyRollingAverageSize);
		}
		
		public Instance getInstance() {
			return instance;
		}
		
		public double getPriority() {
			return priority;
		}
		
		public int getConcurrency() {
			return concurrency;
		}
		
		public String getDataSourceName() {
			return dataSourceName;
		}

		public long getLatency() {
			//return Math.round(latencyAverage.getAverage());
			// calc and cache latency in updateLatency so we don't recalc on each get call
			return lastLatency.get();
		}

		public void updateLatency(long latency) {
			latencyAverage.addStats(latency);
			lastLatency.set(Math.round(latencyAverage.getAverage()));
			lastLatencyTs.set(System.currentTimeMillis());
		}

		public void resetLatency() {
			latencyAverage.reset();
			lastLatency.set(0);
			lastLatencyTs.set(System.currentTimeMillis());
		}

		public int compareTo(InstanceInfo o) {
			int remaining1 = concurrency - pending.get();
			int remaining2 = o.concurrency - o.pending.get();
			int m1, m2;
			if (remaining1 < 2) {
				if (remaining2 < 2) {
					m1 = -remaining1 + 2;
					m2 = -remaining2 + 2;
				} else {
					return 1;
				}
			} else if (remaining2 < 2) {
				return -1;
			} else {
				m1 = pending.get() + 1;
				m2 = o.pending.get() + 1;
			}

			double calc1 = (m1 * getLatency()) / priority;
			double calc2 = (m2 * o.getLatency()) / o.priority;
			if (calc1 > calc2 + getMinLatencyDiff())
				return 1;
			if (calc2 > calc1 + getMinLatencyDiff())
				return -1;
			if (priority > o.priority)
				return -1;
			if (priority < o.priority)
				return 1;
			int i = (concurrency - o.concurrency);
			if (i > 0)
				return -1; //Descending
			if (i < 0)
				return 1; //Descending
			//return instance.instanceId.compareTo(o.instance.instanceId);
			// At this point the 2 are equal but we should not always send to one of them
			// So just pick a random one to distribute the load
			return ThreadLocalRandom.current().nextBoolean() ? 1 : -1;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof InstanceInfo)
				return instance.equals(((InstanceInfo)obj).instance);
			if (obj instanceof Instance)
				return instance.equals(obj);
			return false;
		}

		@Override
		public int hashCode() {
			return instance.hashCode();
		}

		public boolean awaitNotFull(long before) {
			if (pending.get() < concurrency)
				return true; //success
			lock.lock();
			try {
				if (pending.get() >= concurrency) {
					try {
						return notFull.awaitUntil(new Date(before));
					} catch (InterruptedException e) {
						notFull.signalAll(); // propagate to non-interrupted thread
						return pending.get() < concurrency;
					}
				}
			} finally {
				lock.unlock();
			}
			return true; //success
		}

		public void signalNotFull() {
			lock.lock();
			try {
				notFull.signalAll();
			} finally {
				lock.unlock();
			}
		}

		public void pendingRemoved() {
			if (pending.getAndDecrement() == concurrency) {
				signalNotFull();
			}
		}

		public void pendingAdded() {
			pending.getAndIncrement();
		}

		public String sendMessage(QueueProducer queueProducer, PublishedPeerMessage message, long resendTimeout, int attempts, boolean require) throws IOException, NoSuchElementException {
			DirectPeerMethodData data;
			if (queueProducer.isHybrid()) {
				DirectPeerMethodData_SENDDB dataSENDDB = new DirectPeerMethodData_SENDDB();
				dataSENDDB.updateData(message);
				dataSENDDB.setDataSourceName(queueProducer.getDataSourceName());
				dataSENDDB.setQueueName(queueProducer.getQueueName());
				dataSENDDB.setSubscription(queueProducer.getSubscription());
				dataSENDDB.setLegacy(instance.getVersion() == 0);
				data = dataSENDDB;
			} else {
				DirectPeerMethodData_SEND dataSEND = new DirectPeerMethodData_SEND();
				dataSEND.updateData(message);
				dataSEND.setQueueName(queueProducer.getQueueName());
				dataSEND.setSubscription(queueProducer.getSubscription());
				dataSEND.setLegacy(instance.getVersion() == 0);
				data = dataSEND;
			}
			boolean okay = false;
			DirectPeerInterpretter dpi;
			while (true) {
				try {
					dpi = instance.pool.next();
				} catch (NoSuchElementException e) {
					instance.close();
					throw new NoSuchElementException("Instance has no available connections");
				}
				instance.pendingMessages.put(message.getMessageId(), new PublishInfo(message, queueProducer, this, resendTimeout, attempts, require));
				try {
					dpi.writeData(data);
					okay = true;
					return dpi.clientId;
				} catch (SocketTimeoutException e) {
					dpi.close(false);
					throw e;
				} catch (IOException e) {
					if (dpi.isOpen())
						throw e;
				} finally {
					if (!okay) {
						instance.pendingMessages.remove(message.getMessageId());// Avoid double sending on Exception
					}
				}
			}
		}

		public boolean isFull() {
			return pending.get() >= concurrency;
		}

		public String toString() {
			if (dataSourceName != null)
				return MessageFormat.format("{0}(dataSource={1}, pending={2}, priority={3}, concurrency={4}, latency={5}, lastLatencyAgo={6}, full={7})", instance.instanceId, dataSourceName, pending, priority, concurrency, getLatency(), (System.currentTimeMillis()-lastLatencyTs.get()), isFull());
			else
				return MessageFormat.format("{0}(pending={1}, priority={2}, concurrency={3}, latency={4}, lastLatencyAgo={5}, full={6})", instance.instanceId, pending, priority, concurrency, getLatency(), (System.currentTimeMillis()-lastLatencyTs.get()), isFull());
		}
	}

	protected class QueueProducer {
		protected final String dataSourceName;
		protected final String queueName;
		protected final String subscription;
		protected final String desc;
		protected final ConcurrentMap<String, InstanceInfo> instances = new ConcurrentHashMap<String, InstanceInfo>((int)((getEstimatedConnCount() + 1) / 0.75f), 0.75f, 1 + (getEstimatedConnCount() / 8));
		protected final AtomicReference<InstanceInfo> currentInstanceInfo = new AtomicReference<InstanceInfo>();
		protected final AtomicLong lastPrioritizedTs = new AtomicLong(System.currentTimeMillis());
		protected final AtomicBoolean receivedNak = new AtomicBoolean(false);
		protected final boolean hybrid;
		protected final ReentrantLock lock = new ReentrantLock();
		protected final Condition signal = lock.newCondition();

		public QueueProducer(String dataSourceName, String queueName, String subscription, boolean hybrid) {
			this.dataSourceName = dataSourceName;
			this.queueName = queueName;
			this.subscription = subscription;
			this.hybrid = hybrid;

			StringBuilder sb = new StringBuilder();
			sb.append(queueName);
			if (subscription != null)
				sb.append('(').append(subscription).append(')');
			if (hybrid) {
				sb.append(" ");
				sb.append(dataSourceName);
			}
			this.desc = sb.toString();
		}

		public boolean publishMessage(final PublishedPeerMessage message, final long timeout, final int attempts, final boolean require, final boolean async) throws MessagingException {
			if (async) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							sendMessage(message, timeout, attempts, require);
						} catch (MessagingException e) {
							log.warn("Could not send message '" + message.getMessageId() + "' asynchronously to " + desc, e);
						}
					}
				});
				return true;
			} else {
				return sendMessage(message, timeout, attempts, require);
			}
		}

		public boolean registerInstance(Instance instance, byte priority, int concurrency, String dataSourceName) {
			InstanceInfo prev = instances.get(instance.instanceId);
			if (prev != null && prev.priority == priority && ConvertUtils.areEqual(prev.dataSourceName, dataSourceName)) {
				if (prev.concurrency != concurrency) {
					log.debug("Updating concurrency of {0} to {1} for {2}", prev, concurrency, desc);
					prev.concurrency = concurrency;
					reprioritize(queueName, hybrid, true);
				}
				return false;
			} else {
				InstanceInfo instanceInfo = new InstanceInfo(instance, priority, concurrency, dataSourceName);
				boolean expected;
				if (prev == null)
					expected = (instances.putIfAbsent(instance.instanceId, instanceInfo) == null);
				else expected = instances.replace(instance.instanceId, prev, instanceInfo);
				if (!expected)
					return registerInstance(instance, priority, concurrency, dataSourceName);
				if (prev == null)
					log.debug("Added {0} to {1}", instanceInfo, desc);
				else
					log.debug("Replaced {0} with {1} in {2}", prev, instanceInfo, desc);
				reprioritize(queueName, hybrid, true);
				lock.lock();
				try {
					signal.signal();
				} finally {
					lock.unlock();
				}
				return (prev == null);
			}
		}

		public void deregisterInstance(InstanceInfo instanceInfo) {
			boolean changed = instances.remove(instanceInfo.instance.instanceId, instanceInfo);
			changed = changed || currentInstanceInfo.compareAndSet(instanceInfo, null);
			instanceInfo.concurrency = 0;
			if (changed) {
				reprioritize(queueName, hybrid, false);
			}
		}

		public void deregisterInstance(Instance instance) {
			InstanceInfo instanceInfo = instances.get(instance.instanceId);
			if (instanceInfo != null && instanceInfo.instance.equals(instance)) {
				deregisterInstance(instanceInfo);
				return;
			}
			// do it the slow way
			// this is awful and confusing and it is really necessary?
			boolean changed = false;
			while (true) {
				instanceInfo = currentInstanceInfo.get();
				if (instanceInfo == null || !instanceInfo.instance.equals(instance) || (changed = changed || currentInstanceInfo.compareAndSet(instanceInfo, null)))
					break;
			}
			if (changed) {
				reprioritize(queueName, hybrid, false);
			}
		}

		public Collection<InstanceInfo> getInstances() {
			return instances.values();
		}
		
		protected boolean sendMessage(PublishedPeerMessage message, long timeout, int attempts, boolean require) throws MessagingException {
			final long publishBefore;
			final boolean expired;
			long st = System.currentTimeMillis();
			long expiration = message.getExpiration(message.getPosted());
			if (!require && timeout == 0) {
				publishBefore = 0;
				expired = false;
			} else if (expiration > 0) {
				long tmp;
				if (timeout > 0 && (tmp = (System.currentTimeMillis() + timeout)) < expiration) {
					publishBefore = tmp;
					expired = false;
				} else {
					publishBefore = expiration;
					expired = true;
				}
			} else {
				expired = false;
				if (timeout > 0) {
					publishBefore = System.currentTimeMillis() + timeout;
				} else {
					publishBefore = 0;
				}
			}

			while (true) {
				// get instance
				while (instances.isEmpty()) {
					if (hybrid) {
						// ask any connected instances if they want messages from this queue
						DirectPeerMethodData_INQUIREDB data = new DirectPeerMethodData_INQUIREDB();
						data.setDataSourceName(getDataSourceName());
						data.setQueueName(getQueueName());
						data.setSubscription(getSubscription());
						for (Instance instance: hybridInstancesCache.values()) {
							DirectPeerInterpretter dpi;
							try {
								dpi = instance.pool.next();
							} catch (NoSuchElementException e) {
								// instance.close();
								continue;
							}
							try {
								dpi.writeData(data);
							} catch (IOException e) {
								log.warn("Tried to inquire about queue '" + message.getQueueName() + "' with datasource '" + message.getDataSourceName() + "' on '" + dpi.clientId + "' but failed", e);
							}
						}
					} else {
						// ask any connected instances if they want messages from this queue
						DirectPeerMethodData_INQUIRE data = new DirectPeerMethodData_INQUIRE();
						data.setQueueName(getQueueName());
						data.setSubscription(getSubscription());
						for (Instance instance: directInstancesCache.values()) {
							DirectPeerInterpretter dpi;
							try {
								dpi = instance.pool.next();
							} catch (NoSuchElementException e) {
								// instance.close();
								continue;
							}
							try {
								dpi.writeData(data);
							} catch (IOException e) {
								log.warn("Tried to inquire about queue '" + message.getQueueName() + "' on '" + dpi.clientId + "' but failed", e);
							}
						}
					}

					//wait until available
					if (publishBefore > 0) {
						lock.lock();
						try {
							if (!instances.isEmpty())
								break;
							if (signal.awaitUntil(new Date(publishBefore)))
								continue;
						} catch (InterruptedException e) {
							signal.signal();
						} finally {
							lock.unlock();
						}
					}
					if (require) {
						throw new MessagingException("Could not publish message '" + message.getMessageId() + "' to " + desc + " in " + timeout + " ms");
					} else {
						if (log.isInfoEnabled())
							log.info("Could not publish message '" + message.getMessageId() + "' to " + desc + " in " + timeout + " ms");
						return false;
					}
				}

				boolean changedInstance = false;
				long lastPrioritizedTsLong = lastPrioritizedTs.get();
				boolean currentExpired = (System.currentTimeMillis() - lastPrioritizedTsLong >= reprioritizeInterval);
				boolean currentReceivedNak = receivedNak.getAndSet(false);
				InstanceInfo instanceInfo = currentInstanceInfo.get();
				if (instanceInfo == null || instanceInfo.instance.pool.isEmpty() || instanceInfo.isFull() || currentReceivedNak || (currentExpired && lastPrioritizedTs.compareAndSet(lastPrioritizedTsLong, System.currentTimeMillis()))) {
					if (log.isDebugEnabled())
						log.debug("Prioritizing {0} peers for {1}: isEmpty={2} isFull={3} lastPrioritizedTs={4} currentExpired={5} receivedNak={6} current={7}", instances.size(), desc, instanceInfo != null ? instanceInfo.instance.pool.isEmpty() : "", instanceInfo != null ? instanceInfo.isFull() : "", DATE_FORMAT.format(new Date(lastPrioritizedTsLong)), currentExpired, currentReceivedNak, instanceInfo);
					if (currentExpired)
						checkLatencies(instances.values());
					InstanceInfo best = null;
					for (InstanceInfo ii: instances.values()) {
						if (best == null || best.compareTo(ii) > 0)
							best = ii;
					}
					if (best == null)
						continue; //check if instances is empty
					if (!currentInstanceInfo.compareAndSet(instanceInfo, best))
						continue;
					if (!best.equals(instanceInfo))
						changedInstance = true;
					instanceInfo = best;
					if (instances.size() > 1)
						log.info("Best peer for {0} is {1}: {2} of {3} peers: {4}", desc, changedInstance? "new" : "current", instanceInfo.instance.instanceId, instances.size(), instances.values());
				} else {
					if (log.isDebugEnabled()) {
						log.debug("Using current peer for {0}: isEmpty={1} isFull={2} lastPrioritizedTs={3} currentExpired={4} receivedNak={5} current={6}", desc, instanceInfo != null ? instanceInfo.instance.pool.isEmpty() : "", instanceInfo != null ? instanceInfo.isFull() : "", DATE_FORMAT.format(new Date(lastPrioritizedTsLong)), currentExpired, currentReceivedNak, instanceInfo);
					}
				}

				// check flow control -- if(pending >= max) get next instance or wait
				if (!instanceInfo.awaitNotFull(publishBefore)) {
					if (expired) {
						if (log.isInfoEnabled())
							log.info("Message '" + message.getMessageId() + "' expired before it could be published to " + desc);
						return true;
					} else if (require) {
						throw new MessagingException("Could not publish message '" + message.getMessageId() + "' to " + desc + " in " + timeout + " ms because there are already " + instanceInfo.pending + " pending messages");
					} else {
						if (log.isInfoEnabled())
							log.info("Could not publish message '" + message.getMessageId() + "' to " + desc + " in " + timeout + " ms because there are already " + instanceInfo.pending + " pending messages");
						return false;
					}
				}

				// send by instance
				try {
					String clientId = instanceInfo.sendMessage(this, message, timeout, attempts, require);
					if (log.isInfoEnabled())
						log.info("Sent {0} message ''{1}'' to ''{2}'' at ''{3}'' on ''{4}'' in {5} ms using {6} of {7}", hybrid ? "hybrid" : "direct", message.getMessageId(), desc, instanceInfo.instance.instanceId, clientId, (System.currentTimeMillis() - st), changedInstance ? "new" : "current", instances.size());
					return true;
				} catch (IOException e) {
					if (--attempts > 0) {
						if (log.isInfoEnabled())
							log.info("Could not send message to '" + instanceInfo.instance.instanceId + "' because: " + (e.getMessage() == null ? e.getClass().getName() : e.getMessage()) + "; Retrying...", e);
						continue;
					} else if (require)
						throw new MessagingException("Could not send message to '" + instanceInfo.instance.instanceId + "'", e);
					if (log.isInfoEnabled())
						log.info("Could not send message to '" + instanceInfo.instance.instanceId + "' because: " + (e.getMessage() == null ? e.getClass().getName() : e.getMessage()), e);
					return false;
				} catch (NoSuchElementException e) {
					InstanceInfo ii = instances.get(instanceInfo.instance.instanceId);
					if (ii == null) {
						if (log.isInfoEnabled())
							log.info("Instance '" + instanceInfo.instance.instanceId + "' was closed and removed");
					} else if (ii == instanceInfo) {
						log.warn("Instance '" + instanceInfo.instance.instanceId + "' was closed but NOT removed from the list of instances; removing it now");
						instances.remove(instanceInfo.instance.instanceId, instanceInfo);
						currentInstanceInfo.compareAndSet(instanceInfo, null);
					} else if (ii.instance.pool.isEmpty()) {// test if new instance has connections
						currentInstanceInfo.compareAndSet(ii, null);
						if (log.isInfoEnabled())
							log.info("Instance '" + instanceInfo.instance.instanceId + "' was closed and removed, and replaced with one that does not have any connections");
					} else {
						if (log.isInfoEnabled())
							log.info("Instance '" + instanceInfo.instance.instanceId + "' was closed and removed, but a new one replaced it. The new one has " + ii.instance.pool.size() + " connections");
					}
					if (--attempts > 0) {
						if (log.isInfoEnabled())
							log.info("Could not send message to '" + instanceInfo.instance.instanceId + "' because: " + (e.getMessage() == null ? e.getClass().getName() : e.getMessage()) + "; QueueProducer now has instances: " + instances.keySet() + "; Retrying...", e);
						return sendMessage(message, timeout, attempts, require);
					} else if (require)
						throw new MessagingException("Could not send message to '" + instanceInfo.instance.instanceId + "'", e);
					if (log.isInfoEnabled())
						log.info("Could not send message to '" + instanceInfo.instance.instanceId + "' because: " + (e.getMessage() == null ? e.getClass().getName() : e.getMessage()), e);
					return false;
				}
			}
		}

		public String getQueueName() {
			return queueName;
		}

		public String getSubscription() {
			return subscription;
		}

		public String getDataSourceName() {
			return dataSourceName;
		}

		public boolean isHybrid() {
			return hybrid;
		}
		
		public String toString() {
			StringBuilder sb = new StringBuilder();
			InstanceInfo current = currentInstanceInfo.get();
			sb.append(hybrid ? "Hybrid" : "Direct");
			sb.append("QueueProducer(queue=");
			sb.append(desc);
			sb.append(", instanceCount=");
			sb.append(instances.size());
			sb.append(", lastPrioritizedAgo=");
			sb.append(System.currentTimeMillis() - lastPrioritizedTs.get());
			sb.append(", receivedNack=");
			sb.append(receivedNak.get());
			if (current != null) {
				sb.append(", current=");
				sb.append(current.instance.instanceId);
			} else {
				sb.append(", current=None");
			}
			if (!instances.isEmpty()) {
				sb.append(", instances=");
				sb.append(instances.values().toString());
			}
			sb.append(")");
			return sb.toString();
		}

		public Map<String, String> getInstancesInfo() {
			Map<String, String> tempMap = new LinkedHashMap<>();
			instances.entrySet().stream().forEach(e -> tempMap.put(e.getKey(), e.getValue().toString()));
			return tempMap;
		}

		public String getCurrentInstance() {
			InstanceInfo ii = currentInstanceInfo.get();
			if (ii != null)
				return ii.toString();
			return null;
		}

		public Date getLastPrioritizedTs() {
			return new Date(lastPrioritizedTs.get());
		}

		public boolean isReceivedNak() {
			return receivedNak.get();
		}

		public void resetAllLatencies() {
			instances.values().forEach(ii -> ii.resetLatency());
			lastPrioritizedTs.set(System.currentTimeMillis() - reprioritizeInterval);
		}

		public void resetInstanceLatencies(String instanceId) {
			instances.values().stream().filter(ii -> ii.instance.instanceId.equals(instanceId)).forEach(ii -> ii.resetLatency());
		}
	}

	protected void checkLatencies(Collection<InstanceInfo> instances) {
		// Latencies can become "corrupt" over time when a server gets slow and then recovers
		// If we haven't sent to one of the instances in a while, reset it so it gets used again
		if (instances == null || instances.isEmpty())
			return;

		for (InstanceInfo ii: instances) {
			if (ii.getLatency() > 0) {
				long et = System.currentTimeMillis() - ii.lastLatencyTs.get();
				if (et >= inactivityResetTime) {
					if (log.isDebugEnabled())
						log.debug("Resetting instance due to aged last latency of {0} from {1} ms ago: {2}", ii.getLatency(), et, ii);
					ii.resetLatency();
				}
			}
		}
	}
	
	protected void reprioritize(String queueName, boolean hybrid, boolean create) {
		if (!hybrid)
			return;
		InstanceInfoPrioritizer p = create ? hybridPrioritizerCache.getOrCreate(queueName) : hybridPrioritizerCache.getIfPresent(queueName);
		if (p != null)
			p.reprioritize();
	}

	protected class HybridQueueKey {
		public final String dataSourceName;
		public final String queueName;

		public HybridQueueKey(String dataSourceName, String queueName) {
			this.dataSourceName = dataSourceName;
			this.queueName = queueName;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof HybridQueueKey))
				return false;
			HybridQueueKey o = (HybridQueueKey)obj;
			return ConvertUtils.areEqual(dataSourceName, o.dataSourceName) && ConvertUtils.areEqual(queueName, o.queueName);
		}

		@Override
		public int hashCode() {
			return (dataSourceName == null ? 0 : dataSourceName.hashCode()) * 31 + (queueName == null ? 0 : queueName.hashCode());
		}

		@Override
		public String toString() {
			return MessageFormat.format("{0} {1}", queueName, dataSourceName);
		}
	}

	protected final Cache<String, Cache<String, QueueProducer, RuntimeException>, RuntimeException> queueProducersCache = new LockSegmentCache<String, Cache<String, QueueProducer, RuntimeException>, RuntimeException>() {
		@Override
		protected Cache<String, QueueProducer, RuntimeException> createValue(final String queueName, Object... additionalInfo) throws RuntimeException {
			return new LockSegmentCache<String, QueueProducer, RuntimeException>(5, 0.75f, 1) {
				@Override
				protected QueueProducer createValue(String subscription, Object... additionalInfo) throws RuntimeException {
					return new QueueProducer(null, queueName, subscription, false);
				}
			};
		}
	};

	protected final Cache<HybridQueueKey, Cache<String, QueueProducer, RuntimeException>, RuntimeException> hybridQueueProducersCache = new LockSegmentCache<HybridQueueKey, Cache<String, QueueProducer, RuntimeException>, RuntimeException>() {
		@Override
		protected Cache<String, QueueProducer, RuntimeException> createValue(final HybridQueueKey key, Object... additionalInfo) throws RuntimeException {
			return new LockSegmentCache<String, QueueProducer, RuntimeException>(5, 0.75f, 1) {
				@Override
				protected QueueProducer createValue(String subscription, Object... additionalInfo) throws RuntimeException {
					return new QueueProducer(key.dataSourceName, key.queueName, subscription, true);
				}
			};
		}
	};

	/*
	protected class Sorting implements Comparable<Sorting> {
		protected final byte priority;
		protected final long latency;
		protected final int pending;
		protected final int concurrency;
		protected final long sequence;
		protected String desc;
		public Sorting(byte priority, long latency, int pending, int concurrency, long sequence) {
			this.priority = priority;
			this.latency = latency;
			this.pending = pending;
			this.concurrency = concurrency;
			this.sequence = sequence;
		}
		public byte getPriority() {
			return priority;
		}
		public long getLatency() {
			return latency;
		}
		public int getPending() {
			return pending;
		}
		public int getConcurrency() {
			return concurrency;
		}
		public int compareTo(Sorting o) {
			if(sequence > o.sequence + getLatencyResetCount())
				return 1;
			if(o.sequence > sequence + getLatencyResetCount())
				return -1;
			double calc1 = ((pending + 1) * latency) / (double)priority;
			double calc2 = ((o.pending + 1) * o.latency) / (double)o.priority;
			if(calc1 > calc2 + getMinLatencyDiff())
				return 1;
			if(calc2 > calc1 + getMinLatencyDiff())
				return -1;
			if(concurrency > o.concurrency)
				return -1; //Descending
			if(o.concurrency > concurrency)
				return 1; //Descending
			return 0;
		}
		@Override
		public int hashCode() {
			return (31 * (31 * (31 * priority + (int)latency) + pending) + concurrency);
		}
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof Sorting))
				return false;
			Sorting s = (Sorting)obj;
			return sequence == s.sequence && priority == s.priority && latency == s.latency && pending == s.pending && concurrency == s.concurrency;
		}
		@Override
		public String toString() {
			if(desc == null) {
				StringBuilder sb = new StringBuilder();
				sb.append("[Sequence=").append(sequence).append("; Priority=").append(priority).append("; Latency=").append(latency).append("; Pending=").append(pending).append("; Concurrency=").append(concurrency).append(']');
				desc = sb.toString();
			}
			return desc;
		}
		public long getSequence() {
			return sequence;
		}
	}
	*/
	protected QueueProducer getOrCreateProducer(boolean hybrid, String dataSourceName, String queueName, String subscription) {
		Cache<String, QueueProducer, RuntimeException> producers;
		if (hybrid)
			producers = hybridQueueProducersCache.getOrCreate(new HybridQueueKey(dataSourceName, queueName));
		else 
			producers = queueProducersCache.getOrCreate(queueName);
		return producers.getOrCreate(subscription);
	}

	protected QueueProducer getIfPresentProducer(boolean hybrid, String dataSourceName, String queueName, String subscription) {
		Cache<String, QueueProducer, RuntimeException> producers;
		if (hybrid)
			producers = hybridQueueProducersCache.getIfPresent(new HybridQueueKey(dataSourceName, queueName));
		else 
			producers = queueProducersCache.getIfPresent(queueName);
		return producers.getOrCreate(subscription);
	}

	public int getDirectConsumerCount(String queueName, String subscription) {
		Cache<String, QueueProducer, RuntimeException> producers = queueProducersCache.getIfPresent(queueName);
		if (producers == null)
			return 0;
		QueueProducer producer = producers.getIfPresent(subscription);
		if (producer == null)
			return 0;
		int cnt = 0;
		for (InstanceInfo instanceInfo: producer.getInstances()) {
			if (instanceInfo.concurrency > 0)
				cnt += instanceInfo.instance.pool.size();
		}
		return cnt;
	}

	public int getHybridConsumerCount(String queueName, String subscription) {
		Iterable<String> pdsn = getPreferredDataSourceName(queueName);
		if (pdsn == null)
			return 0;
		Iterator<String> iter = pdsn.iterator();
		if (!iter.hasNext())
			return 0;
		String dataSourceName = iter.next();
		Cache<String, QueueProducer, RuntimeException> producers = hybridQueueProducersCache.getIfPresent(new HybridQueueKey(dataSourceName, queueName));
		if (producers == null)
			return 0;
		QueueProducer producer = producers.getIfPresent(subscription);
		if (producer == null)
			return 0;
		int cnt = 0;
		for (InstanceInfo instanceInfo: producer.getInstances()) {
			if (instanceInfo.concurrency > 0)
				cnt += instanceInfo.instance.pool.size();
		}
		return cnt;
	}

	public Set<String> getDirectSubscriptions(String queueName) {
		Cache<String, QueueProducer, RuntimeException> producers = queueProducersCache.getIfPresent(queueName);
		if (producers == null)
			return Collections.emptySet();
		return Collections.unmodifiableSet(producers.keySet());
	}

	public Set<String> getHybridSubscriptions(String queueName) {
		Iterable<String> pdsn = getPreferredDataSourceName(queueName);
		if (pdsn == null)
			return Collections.emptySet();
		Iterator<String> iter = pdsn.iterator();
		if (!iter.hasNext())
			return Collections.emptySet();
		String dataSourceName = iter.next();
		Cache<String, QueueProducer, RuntimeException> producers = hybridQueueProducersCache.getIfPresent(new HybridQueueKey(dataSourceName, queueName));
		if (producers == null)
			return Collections.emptySet();
		return Collections.unmodifiableSet(producers.keySet());
	}

	public boolean publishMessage(PublishedPeerMessage message, long timeout, int attempts, PublishType publishType, boolean async) throws MessagingException {
		Cache<String, QueueProducer, RuntimeException> queueProducers;
		boolean required;
		switch (publishType) {
			case DIRECT:
				queueProducers = queueProducersCache.getOrCreate(message.getQueueName());
				required = (timeout != 0);
				break;
			case DIRECT_OR_DB:
				queueProducers = queueProducersCache.getIfPresent(message.getQueueName());
				if (queueProducers == null || queueProducers.size() == 0)
					return false;
				required = false;
				break;
			case HYBRID:
				queueProducers = hybridQueueProducersCache.getOrCreate(new HybridQueueKey(message.getDataSourceName(), message.getQueueName()));
				required = false;
				break;
			default:
				throw new MessagingException("Publish Type " + publishType + " is not supported by " + this);
		}
		boolean success = false;
		if (message.isBroadcast()) {
			for (QueueProducer queueProducer: queueProducers.values()) {
				if (queueProducer.getSubscription() != null)
					success |= queueProducer.publishMessage(message, timeout, attempts, required, async);
			}
		} else {
			QueueProducer queueProducer;
			switch (publishType) {
				case DIRECT:
				case HYBRID:
					queueProducer = queueProducers.getOrCreate(null);
					break;
				case DIRECT_OR_DB:
					queueProducer = queueProducers.getIfPresent(null);
					if (queueProducer == null)
						return false;
					break;
				default:
					throw new MessagingException("Publish Type " + publishType + " is not supported by " + this);
			}
			success = queueProducer.publishMessage(message, timeout, attempts, required, async);
		}
		return success;
	}

	public void start() throws ServiceException, InterruptedException {
		initializer.initialize();
	}

	public void shutdown() {
		initializer.reset();
	}

	public InetSocketAddress getListenAddress() {
		return server.getSocketAddress();
	}

	public void setListenAddress(InetSocketAddress listenAddress) {
		server.setSocketAddress(listenAddress);
		listenAddressString = (listenAddress == null ? null : listenAddress.toString());
	}
	
	public String getListenAddressString() {
		return listenAddressString;
	}

	public int getNumThreads() {
		return numThreads;
	}

	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
		if (initializer.isDone()) {
			int diff = numThreads - getNumThreads();
			if (diff > 0)
				startThreads(diff);
			else if (diff < 0)
				stopThreads(diff, true, 5000);
		}
	}

	protected int stopThreads(int numThreads, boolean force, long timeout) {
		Set<ProducerThread> stopped = new HashSet<ProducerThread>();
		int n = numThreads;
		for (ProducerThread thread: threads) {
			if (thread != null) { //it was removed during iteration
				thread.shutdown(force);
				stopped.add(thread);
				if (--n < 1)
					break;
			}
		}
		int cnt = 0;
		for (ProducerThread thread: stopped) {
			try {
				thread.join(timeout);
				cnt++;
			} catch (InterruptedException e) {
				log.warn("Interrupted while waiting for thread " + thread, e);
			}
		}
		return cnt;
	}

	protected int stopAllThreads(boolean force, long timeout) {
		Set<ProducerThread> stopped = new HashSet<ProducerThread>();
		for (ProducerThread thread: threads) {
			if (thread != null) { //it was removed during iteration
				thread.shutdown(force);
				stopped.add(thread);
			}
		}
		int cnt = 0;
		for (ProducerThread thread: stopped) {
			try {
				thread.join(timeout);
				cnt++;
			} catch (InterruptedException e) {
				log.warn("Interrupted while waiting for thread " + thread, e);
			}
		}
		return cnt;
	}

	protected int startThreads(int numThreads) {
		if (numThreads < 1)
			return 0;
		if (log.isInfoEnabled())
			log.info("Starting " + numThreads + " threads for DirectPeerProducer listening on " + listenAddressString);
		for (int i = 0; i < numThreads; i++) {
			Thread thread = new ProducerThread();
			thread.start();
		}
		return numThreads;
	}

	public boolean isJmxEnabled() {
		return server.isJmxDetailEnabled();
	}

	public void setJmxEnabled(boolean jmxEnabled) {
		server.setJmxDetailEnabled(jmxEnabled);
	}

	public int getMaxConnections() {
		return server.getMaxConnections();
	}

	public void setMaxConnections(int maxConnections) {
		server.setMaxConnections(maxConnections);
	}

	public long getReprioritizeInterval() {
		return reprioritizeInterval;
	}

	public void setReprioritizeInterval(long reprioritizeInterval) {
		this.reprioritizeInterval = reprioritizeInterval;
	}

	public long getMinLatencyDiff() {
		return minLatencyDiff;
	}

	public void setMinLatencyDiff(long minLatencyDiff) {
		this.minLatencyDiff = minLatencyDiff;
	}

	public long getInactivityResetTime() {
		return inactivityResetTime;
	}

	public void setInactivityResetTime(long inactivityResetTime) {
		this.inactivityResetTime = inactivityResetTime;
	}

	public int getMaxRetryCount() {
		return maxRetryCount;
	}

	public void setMaxRetryCount(int maxRetryCount) {
		this.maxRetryCount = maxRetryCount;
	}

	public int getEstimatedConnCount() {
		return estimatedConnCount;
	}

	public void setEstimatedConnCount(int estimatedAppCount) {
		this.estimatedConnCount = estimatedAppCount;
	}

	public Iterable<String> getPreferredDataSourceName(String queueName) {
		return hybridPrioritizerCache.getIfPresent(queueName);
	}

	public DataSourcePreferenceType getDataSourcePreferenceType() {
		return dataSourcePreferenceType;
	}

	public void setDataSourcePreferenceType(DataSourcePreferenceType dataSourcePreferenceType) {
		this.dataSourcePreferenceType = dataSourcePreferenceType;
	}

	public long getOutputWriteTimeout() {
		return outputWriteTimeout;
	}

	public void setOutputWriteTimeout(long outputWriteTimeout) {
		this.outputWriteTimeout = outputWriteTimeout;
	}

	public long getSocketWriteTimeout() {
		return socketWriteTimeout;
	}

	public void setSocketWriteTimeout(long socketWriteTimeout) {
		this.socketWriteTimeout = socketWriteTimeout;
	}

	public int getLatencyRollingAverageSize() {
		return latencyRollingAverageSize;
	}

	public void setLatencyRollingAverageSize(int latencyRollingAverageSize) {
		this.latencyRollingAverageSize = latencyRollingAverageSize;
	}

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	public int getThreadCount() {
		return threads.size();
	}

	public int getLastThreadId() {
		return lastThreadId.get();
	}

	public String getConnectionInfo() {
		return server.getConnectionInfo();
	}
	
	public String getExecutorInfo() {
		return MessageFormat.format("poolSize={0}, active={1}, queueSize={2}, largestPoolSize={3}, completedCount={4}, coreSize={5}, maxSize={6}, keepAliveTime={7}", 
				executor.getPoolSize(), 
				executor.getActiveCount(), 
				executor.getQueue().size(), 
				executor.getLargestPoolSize(),
				executor.getCompletedTaskCount(),
				executor.getCorePoolSize(),
				executor.getMaximumPoolSize(),
				executor.getKeepAliveTime(TimeUnit.MILLISECONDS));
	}

	public List<String> getDirectInstancesInfo() {
		return directInstancesCache.values().stream().map(ii -> ii.toString()).sorted().collect(Collectors.toList());
	}

	public List<String> getHybridInstancesInfo() {
		return hybridInstancesCache.values().stream().map(ii -> ii.toString()).sorted().collect(Collectors.toList());
	}

	public List<String> getDirectProducersInfo() {
		return queueProducersCache.entrySet().stream().map(e -> MessageFormat.format("{0} = {1}", e.getKey(), e.getValue().values())).sorted().collect(Collectors.toList());
	}
	
	public List<String> getHybridProducersInfo() {
		return hybridQueueProducersCache.entrySet().stream().map(e -> MessageFormat.format("{0} = {1}", e.getKey(), e.getValue().values())).sorted().collect(Collectors.toList());
	}
	
	public List<String> getHybridPrioritizerInfo() {
		return hybridPrioritizerCache.entrySet().stream().map(e -> MessageFormat.format("{0} = {1}", e.getKey(), e.getValue())).sorted().collect(Collectors.toList());
	}

	public void resetAllLatencies() {
		log.info("Resetting all latencies");
		hybridQueueProducersCache.values().forEach(c -> c.values().forEach(qp -> qp.resetAllLatencies()));
		queueProducersCache.values().forEach(c -> c.values().forEach(qp -> qp.resetAllLatencies()));
		hybridPrioritizerCache.values().forEach(iip -> iip.reprioritize());
	}

	public void resetInstanceLatencies(String instanceId) {
		log.info("Resetting all latencies for instance {0}", instanceId);
		hybridQueueProducersCache.values().forEach(c -> c.values().forEach(qp -> qp.resetInstanceLatencies(instanceId)));
		queueProducersCache.values().forEach(c -> c.values().forEach(qp -> qp.resetInstanceLatencies(instanceId)));
		hybridPrioritizerCache.values().forEach(iip -> iip.reprioritize()); // TODO: this should only do prioritizers that contain this instance
	}
	
	public void resetQueueLatencies(String queueName) {
		log.info("Resetting all latencies for queue {0}", queueName);
		hybridQueueProducersCache.entrySet().stream().filter(e -> e.getKey().queueName.equals(queueName)).forEach(e -> e.getValue().values().forEach(qp -> qp.resetAllLatencies()));
		queueProducersCache.entrySet().stream().filter(e -> e.getKey().equals(queueName)).forEach(e -> e.getValue().values().forEach(qp -> qp.resetAllLatencies()));
		hybridPrioritizerCache.entrySet().stream().filter(e -> e.getKey().equals(queueName)).forEach(e -> e.getValue().reprioritize());
	}
}
