package simple.mq.peer;

import simple.db.CharacterStream;
import simple.mq.StandardMessage;

public abstract class StandardPeerMessage extends StandardMessage implements PeerMessage {
	protected String dataSourceName;
	protected String shadowDataSourceName;
	protected CharacterStream errorMessage;
	public String getDataSourceName() {
		return dataSourceName;
	}
	public void setDataSourceName(String primaryDataSourceName) {
		this.dataSourceName = primaryDataSourceName;
	}
	public String getShadowDataSourceName() {
		return shadowDataSourceName;
	}
	public void setShadowDataSourceName(String secondaryDataSourceName) {
		this.shadowDataSourceName = secondaryDataSourceName;
	}
	protected void setReadOnly() {
		readOnly.set(true);
	}
	protected void setWritable() {
		readOnly.set(false);
	}
	public CharacterStream getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(CharacterStream errorMessage) {
		this.errorMessage = errorMessage;
	}		
}
