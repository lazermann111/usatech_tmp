package simple.mq.peer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;

import simple.lang.EnumByteValueLookup;
import simple.lang.InvalidByteValueException;


public enum DirectPeerMethod {
	REQUEST((byte)0xA1),
	SEND((byte)0xB1),
	SEND_LONG((byte) 0xB3),
    INQUIRE((byte)0xC1),
    ACK((byte)0xA2),
    NAK((byte)0xA3),
    REQUESTDB((byte)0xA4),
    SENDDB((byte)0xB2),
    SENDDB_LONG((byte) 0xB4),
    INQUIREDB((byte)0xC2),
    SEND2((byte)0xB5),
    SEND2_LONG((byte) 0xB6), 
    SENDDB2((byte) 0xB7), 
    SENDDB2_LONG((byte) 0xB8),
    SEND3((byte)0xB0),
    SENDDB3((byte)0xB9),
    REQUEST3((byte)0xA5),
    REQUESTDB3((byte)0xA6),
    
    ;

	private static interface DataCreator {
		public DirectPeerMethodData create() throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException;
	}

	private static DataCreator makeCreator(final DirectPeerMethod method) {
		String s = method.toString();
		if(s.startsWith("SENDDB"))
			return new DataCreator() {
				@Override
				public DirectPeerMethodData create() {
					return new DirectPeerMethodData_SENDDB(method);
				}
			};
		if(s.startsWith("SEND"))
			return new DataCreator() {
				@Override
				public DirectPeerMethodData create() {
					return new DirectPeerMethodData_SEND(method);
				}
			};
		if(s.startsWith("REQUESTDB"))
			return new DataCreator() {
				@Override
				public DirectPeerMethodData create() {
					return new DirectPeerMethodData_REQUESTDB(method);
				}
			};
		if(s.startsWith("REQUEST"))
			return new DataCreator() {
				@Override
				public DirectPeerMethodData create() {
					return new DirectPeerMethodData_REQUEST(method);
				}
			};
		final Constructor<? extends DirectPeerMethodData> dataConstructor;
		try {
			dataConstructor = Class.forName(DirectPeerMethodData.class.getName() + "_" + s).asSubclass(DirectPeerMethodData.class).getConstructor();
		} catch(SecurityException e) {
			throw new UndeclaredThrowableException(e);
		} catch(NoSuchMethodException e) {
			throw new UndeclaredThrowableException(e);
		} catch(ClassNotFoundException e) {
			throw new UndeclaredThrowableException(e);
		}
		return new DataCreator() {
			@Override
			public DirectPeerMethodData create() throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
				return dataConstructor.newInstance();
			}
		};
	}
    private final byte value;
	private final DataCreator dataCreator;

	private DirectPeerMethod(byte value) {
    	this.value = value;
		this.dataCreator = makeCreator(this);
    }

    public byte getValue() {
        return value;
    }
    protected final static EnumByteValueLookup<DirectPeerMethod> lookup = new EnumByteValueLookup<DirectPeerMethod>(DirectPeerMethod.class);
    public static DirectPeerMethod getByValue(byte value) throws InvalidByteValueException {
    	return lookup.getByValue(value);
    }
	public DirectPeerMethodData createData() throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		return dataCreator.create();
	}
}
