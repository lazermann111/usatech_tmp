CREATE SCHEMA MQ
  AUTHORIZATION admin;
     
CREATE OR REPLACE FUNCTION PUBLIC.FROM_HEX(
	pv_hex VARCHAR)
    RETURNS BIGINT
AS $$
DECLARE
	ln_num BIGINT;
BEGIN
	IF pv_hex IS NULL OR LENGTH(TRIM(pv_hex)) = 0 THEN
		RETURN NULL;
	ELSIF TRIM(pv_hex) !~ '^[0-9A-Fa-f]+$' THEN
		RAISE EXCEPTION 'Input contains non-hex character' USING ERRCODE = 'invalid_binary_representation';
	ELSE
		EXECUTE 'SELECT x''' || TRIM(pv_hex) || '''::BIGINT' INTO ln_num;
		RETURN ln_num;
	END IF;			
END;
$$ LANGUAGE plpgsql;

CREATE TABLE MQ.QUEUE
(
   QUEUE_ID SERIAL NOT NULL,
   DATA_SOURCE_NAME VARCHAR(100),
   QUEUE_NAME VARCHAR(100) NOT NULL,
   SUBSCRIPTION_NAME VARCHAR(100),
   QUEUE_TABLE VARCHAR(305) NOT NULL,
   PRIMARY_WAITING_PID INTEGER,
   LOCKED_INSTANCE_ID VARCHAR(100),
   LOCKED_PRIORITY SMALLINT,
   LOCKED_TS TIMESTAMP,
   CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   CONSTRAINT PK_QUEUE PRIMARY KEY(QUEUE_ID)
) 
WITH (
  OIDS = FALSE
);

CREATE UNIQUE INDEX UX_QUEUE_DS_QUEUE_SUB_NAME ON MQ.QUEUE(DATA_SOURCE_NAME,QUEUE_NAME,SUBSCRIPTION_NAME) ;
CREATE UNIQUE INDEX UX_QUEUE_QUEUE_TABLE ON MQ.QUEUE(QUEUE_TABLE) ;

CREATE TABLE MQ.QUEUE_SHADOW_DELETION
(
	QUEUE_ID BIGINT NOT NULL,
	SHADOW_DATA_SOURCE_NAME VARCHAR(100) NOT NULL,
	PRIORITY SMALLINT NOT NULL,
	LAST_MESSAGE_ID VARCHAR(100) NOT NULL,
	LAST_POSTED_TIME BIGINT NOT NULL,
	LAST_AVAILABLE_TIME BIGINT NOT NULL,
	CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
	UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
	CONSTRAINT PK_QUEUE_SHADOW_DELETION PRIMARY KEY(QUEUE_ID, SHADOW_DATA_SOURCE_NAME, PRIORITY),
	CONSTRAINT FK_QSD_QUEUE_ID FOREIGN KEY(QUEUE_ID) REFERENCES MQ.QUEUE(QUEUE_ID)
) 
WITH (
  OIDS = FALSE
);

CREATE TABLE MQ.CARRYOVER_CLEAR_BEFORE
(
	QUEUE_ID BIGINT NOT NULL,
	SHADOW_DATA_SOURCE_NAME VARCHAR(100) NOT NULL,
	LAST_POSTED_TIME BIGINT NOT NULL,
	CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
	UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
	CONSTRAINT PK_CARRYOVER_CLEAR_BEFORE PRIMARY KEY(QUEUE_ID, SHADOW_DATA_SOURCE_NAME),
	CONSTRAINT FK_CCB_QUEUE_ID FOREIGN KEY(QUEUE_ID) REFERENCES MQ.QUEUE(QUEUE_ID)
) 
WITH (
  OIDS = FALSE
);

CREATE TABLE MQ.Q_(
	QUEUE_TABLE VARCHAR(100) NOT NULL,
	MESSAGE_ID VARCHAR(100) NOT NULL,
	CONTENT BYTEA NOT NULL,
	POSTED_TIME BIGINT NOT NULL,
	AVAILABLE_TIME BIGINT NOT NULL,
	EXPIRE_TIME BIGINT NOT NULL,
	PRIORITY SMALLINT NOT NULL,
	TEMPORARY_FLAG BOOLEAN NOT NULL,
	REDELIVERED_FLAG BOOLEAN NOT NULL DEFAULT FALSE,
	RETRY_COUNT INTEGER NOT NULL DEFAULT 0,
	SHADOW_DATA_SOURCE_NAME VARCHAR(100),
	ERROR_MESSAGE TEXT,
	CORRELATION BIGINT,
	BLOCKS VARCHAR(100)[]
)
WITH (
  OIDS = FALSE
);

CREATE TABLE MQ.DLQ(
	PRIMARY KEY(QUEUE_TABLE, MESSAGE_ID)
) INHERITS(MQ.Q_);

CREATE INDEX IX_DLQ_QUEUE_TABLE ON MQ.DLQ(QUEUE_TABLE);
CREATE INDEX IX_DLQ_MESSAGE_ID ON MQ.DLQ(MESSAGE_ID);
CREATE INDEX IX_DLQ_POSTED_TIME ON MQ.DLQ(POSTED_TIME);

CREATE TABLE MQ.IQ_(
	QUEUE_TABLE VARCHAR(100) NOT NULL,
	MESSAGE_ID VARCHAR(100) NOT NULL,
	CONNECTION_ID VARCHAR(100) NOT NULL
)
WITH (
  OIDS = FALSE
);

CREATE TABLE MQ.INSTANCE
(
   INSTANCE_ID VARCHAR(100) NOT NULL,
   LOCK_EXPIRATION_TS TIMESTAMP NOT NULL,
   CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT STATEMENT_TIMESTAMP(),
   CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT STATEMENT_TIMESTAMP(),
   UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   CONSTRAINT PK_INSTANCE PRIMARY KEY(INSTANCE_ID)
) 
WITH (
  OIDS = FALSE
);

CREATE TABLE MQ.BLOCK(
   BLOCK_KEY VARCHAR(200) NOT NULL,
   DATA_SOURCE_NAME VARCHAR(100),
   EXPIRE_TIME BIGINT NOT NULL,
   CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   CONSTRAINT PK_BLOCK PRIMARY KEY(BLOCK_KEY)
) 
WITH (
  OIDS = FALSE
);

CREATE OR REPLACE FUNCTION MQ.FTRBU_AUDIT() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	NEW.UPDATED_UTC_TS := STATEMENT_TIMESTAMP();
	NEW.UPDATED_BY := SESSION_USER;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TRBU_INSTANCE
BEFORE UPDATE ON MQ.INSTANCE
    FOR EACH ROW
    EXECUTE PROCEDURE MQ.FTRBU_AUDIT();
	
CREATE TRIGGER TRBU_QUEUE
BEFORE UPDATE ON MQ.QUEUE
    FOR EACH ROW
    EXECUTE PROCEDURE MQ.FTRBU_AUDIT();

CREATE TRIGGER TRBU_QUEUE_SHADOW_DELETION
BEFORE UPDATE ON MQ.QUEUE_SHADOW_DELETION
    FOR EACH ROW
    EXECUTE PROCEDURE MQ.FTRBU_AUDIT();

CREATE TRIGGER TRBU_CARRYOVER_CLEAR_BEFORE
BEFORE UPDATE ON MQ.CARRYOVER_CLEAR_BEFORE
    FOR EACH ROW
    EXECUTE PROCEDURE MQ.FTRBU_AUDIT();

CREATE OR REPLACE FUNCTION MQ.FTRAD_QUEUE() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'DROP TABLE IF EXISTS MQ.' || OLD.QUEUE_TABLE;
	EXECUTE 'DROP TABLE IF EXISTS MQ.I' || OLD.QUEUE_TABLE;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.CONSTRUCT_NAME(pv_base_name VARCHAR, pv_suffix VARCHAR)
    RETURNS VARCHAR
    SECURITY DEFINER
AS $$
BEGIN
    IF LENGTH(pv_base_name) + LENGTH(pv_suffix) <= 60 THEN
        RETURN pv_base_name || '_' || pv_suffix;
    ELSIF LENGTH(pv_base_name) > 51 AND LENGTH(pv_suffix) > 51 THEN
        RETURN SUBSTR(pv_base_name, 1, 21) || '$' || SUBSTR(MD5(pv_base_name), 1, 8) || '_' || SUBSTR(pv_suffix, 1, 21) || '$' || SUBSTR(MD5(pv_suffix), 1, 8);
    ELSIF LENGTH(pv_base_name) > 21 THEN
        RETURN SUBSTR(pv_base_name, 1, 51 - LENGTH(pv_suffix)) || '$' || SUBSTR(MD5(pv_base_name), 1, 8) || '_' || pv_suffix;
    ELSE
        RETURN pv_base_name || '_' || SUBSTR(pv_suffix, 1,  51 - LENGTH(pv_base_name)) || '_$' || SUBSTR(MD5(pv_suffix), 1, 8);
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.CREATE_QUEUE(
	pv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE,
	pb_primary BOOLEAN
) 
	RETURNS VOID 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'CREATE TABLE MQ.' || pv_queue_table || '('
		|| 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
		|| 'CONSTRAINT CK_' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
		|| 'PRIMARY KEY(MESSAGE_ID)'
		|| ') INHERITS(MQ.Q_)';
	EXECUTE 'GRANT SELECT, INSERT ON MQ.'|| pv_queue_table || ' TO use_mq';
	EXECUTE 'ALTER TABLE MQ.'|| pv_queue_table || ' OWNER TO use_mq';
	EXECUTE 'CREATE UNIQUE INDEX ' || MQ.CONSTRUCT_NAME('UX_' || pv_queue_table, 'ORDER') || ' ON MQ.' || pv_queue_table || '(PRIORITY DESC, POSTED_TIME, MESSAGE_ID)';
	EXECUTE 'CREATE INDEX ' || MQ.CONSTRUCT_NAME('IX_' || pv_queue_table, 'AVAILABLE_TIME') || ' ON MQ.' || pv_queue_table || '(AVAILABLE_TIME)';
	EXECUTE 'CREATE INDEX ' || MQ.CONSTRUCT_NAME('IX_' || pv_queue_table, 'EXPIRE_TIME') || ' ON MQ.' || pv_queue_table || '(EXPIRE_TIME)';
	IF pb_primary THEN
		EXECUTE 'CREATE TABLE MQ.I' || pv_queue_table || '('
			|| 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
			|| 'CONSTRAINT CK_I' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
			|| 'PRIMARY KEY(MESSAGE_ID)'
			|| ') INHERITS(MQ.IQ_)';
		EXECUTE 'ALTER TABLE MQ.I'|| pv_queue_table || ' OWNER TO use_mq';	
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FTRAI_QUEUE() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	PERFORM MQ.CREATE_QUEUE(NEW.QUEUE_TABLE, NEW.DATA_SOURCE_NAME IS NULL);
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FTRAU_QUEUE() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'ALTER TABLE MQ.' || OLD.QUEUE_TABLE || ' RENAME TO MQ.' || NEW.QUEUE_TABLE;
	EXECUTE 'ALTER TABLE MQ.' || NEW.QUEUE_TABLE || ' ALTER QUEUE_TABLE SET DEFAULT ''' || NEW.QUEUE_TABLE || '''';		
	EXECUTE 'ALTER TABLE MQ.' || NEW.QUEUE_TABLE || ' DROP CONSTRAINT CK_' || OLD.QUEUE_TABLE;
	EXECUTE 'ALTER TABLE MQ.' || NEW.QUEUE_TABLE || ' ADD CONSTRAINT CK_' || NEW.QUEUE_TABLE
		|| ' CHECK (QUEUE_TABLE = ''' || NEW.QUEUE_TABLE || ''')';
	EXECUTE 'ALTER INDEX ' || MQ.CONSTRUCT_NAME('UX_' || OLD.QUEUE_TABLE, 'ORDER') || ' RENAME TO ' || MQ.CONSTRUCT_NAME('UX_' || NEW.QUEUE_TABLE, 'ORDER');
	EXECUTE 'ALTER INDEX ' || MQ.CONSTRUCT_NAME('IX_' || OLD.QUEUE_TABLE, 'AVAILABLE_TIME') || ' RENAME TO ' || MQ.CONSTRUCT_NAME('IX_' || NEW.QUEUE_TABLE, 'AVAILABLE_TIME');
    EXECUTE 'ALTER INDEX ' || MQ.CONSTRUCT_NAME('IX_' || OLD.QUEUE_TABLE, 'EXPIRE_TIME') || ' RENAME TO ' || MQ.CONSTRUCT_NAME('IX_' || NEW.QUEUE_TABLE, 'EXPIRE_TIME');
	IF NEW.DATA_SOURCE_NAME IS NULL THEN
		EXECUTE 'ALTER TABLE MQ.I' || OLD.QUEUE_TABLE || ' RENAME TO MQ.I' || NEW.QUEUE_TABLE;
		EXECUTE 'ALTER TABLE MQ.I' || NEW.QUEUE_TABLE || ' ALTER QUEUE_TABLE SET DEFAULT ''' || NEW.QUEUE_TABLE || '''';		
		EXECUTE 'ALTER TABLE MQ.I' || NEW.QUEUE_TABLE || ' DROP CONSTRAINT CK_I' || OLD.QUEUE_TABLE;
		EXECUTE 'ALTER TABLE MQ.I' || NEW.QUEUE_TABLE || ' ADD CONSTRAINT CK_I' || NEW.QUEUE_TABLE
			|| ' CHECK (QUEUE_TABLE = ''' || NEW.QUEUE_TABLE || ''')';
	END IF;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TRAD_QUEUE
AFTER DELETE ON MQ.QUEUE
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRAD_QUEUE();

CREATE TRIGGER TRAI_QUEUE
AFTER INSERT ON MQ.QUEUE
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRAI_QUEUE();
    
CREATE TRIGGER TRAU_QUEUE
AFTER UPDATE ON MQ.QUEUE
    FOR EACH ROW
    WHEN (NEW.QUEUE_TABLE != OLD.QUEUE_TABLE) 
	EXECUTE PROCEDURE MQ.FTRAU_QUEUE();
    
CREATE OR REPLACE FUNCTION MQ.FTRBI_Q_() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'INSERT INTO MQ.' || NEW.QUEUE_TABLE || ' VALUES($1.*)'
		USING NEW;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FTRBU_Q_() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'UPDATE MQ.' || NEW.QUEUE_TABLE || ' SET(*) = ($1) WHERE MESSAGE_ID = $2'
		USING NEW, OLD.MESSAGE_ID;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FTRBD_Q_() 
	RETURNS TRIGGER 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'DELETE FROM MQ.' || OLD.QUEUE_TABLE || ' WHERE MESSAGE_ID = $1'
		USING OLD.MESSAGE_ID;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TRBD_Q_
BEFORE DELETE ON MQ.Q_
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRBD_Q_();

CREATE TRIGGER TRBI_Q_
BEFORE INSERT ON MQ.Q_
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRBI_Q_();
    
CREATE TRIGGER TRBU_Q_
BEFORE UPDATE ON MQ.Q_
    FOR EACH ROW
	EXECUTE PROCEDURE MQ.FTRBU_Q_();

CREATE OR REPLACE FUNCTION MQ.GET_QUEUE_TABLE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
	RETURNS MQ.QUEUE.QUEUE_TABLE%TYPE
	IMMUTABLE
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := 'Q_' 
		|| CASE WHEN pv_data_source_name IS NULL THEN '' ELSE UPPER(REGEXP_REPLACE(pv_data_source_name, E'\\W', '_', 'g')) || '$' END
		|| COALESCE(UPPER(REGEXP_REPLACE(pv_queue_name, E'\\W', '_', 'g')), '_') 
		|| CASE WHEN pv_subscription_name IS NULL THEN '' ELSE '$' || UPPER(REGEXP_REPLACE(pv_subscription_name, E'\\W', '_', 'g')) END;
	IF LENGTH(lv_queue_table) > 62 THEN
		lv_queue_table := substr(lv_queue_table, 1, 53) || '_' || substr(md5(substr(lv_queue_table, 54)), 1, 8);
	END IF;
	RETURN lv_queue_table;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.REGISTER_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	BEGIN
		INSERT INTO MQ.QUEUE(DATA_SOURCE_NAME, QUEUE_NAME, SUBSCRIPTION_NAME, QUEUE_TABLE)
			  VALUES(pv_data_source_name, pv_queue_name, pv_subscription_name, lv_queue_table);
		RETURN TRUE;
	EXCEPTION
		WHEN unique_violation THEN
			RETURN FALSE;
	END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_QUEUE_ID(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS MQ.QUEUE.QUEUE_ID%TYPE
	SECURITY DEFINER
AS $$
DECLARE
	ln_queue_id MQ.QUEUE.QUEUE_ID%TYPE;
BEGIN
	SELECT QUEUE_ID
	  INTO ln_queue_id
	  FROM MQ.QUEUE 
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
	   AND DATA_SOURCE_NAME	= pv_data_source_name;
	IF NOT FOUND THEN
		PERFORM MQ.REGISTER_QUEUE(pv_data_source_name, pv_queue_name, pv_subscription_name);	
		RETURN MQ.GET_QUEUE_ID(pv_data_source_name, pv_queue_name, pv_subscription_name);
	ELSE
		RETURN ln_queue_id;
	END IF;
END;
$$ LANGUAGE plpgsql;
	   
CREATE OR REPLACE FUNCTION MQ.PREPARE_QUEUE_LOCK(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS MQ.QUEUE.QUEUE_ID%TYPE
	SECURITY DEFINER
AS $$
DECLARE
	ln_queue_id MQ.QUEUE.QUEUE_ID%TYPE;
BEGIN
	SELECT QUEUE_ID
	  INTO ln_queue_id
	  FROM MQ.QUEUE 
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
	   AND DATA_SOURCE_NAME IS NULL;
	IF NOT FOUND THEN
		PERFORM MQ.REGISTER_QUEUE(NULL, pv_queue_name, pv_subscription_name);
		RETURN MQ.PREPARE_QUEUE_LOCK(pv_queue_name, pv_subscription_name);
	END IF;
	RETURN ln_queue_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.ACQUIRE_QUEUE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	pn_queue_id MQ.QUEUE.QUEUE_ID%TYPE,
	pn_clear_before BIGINT,
	pn_priority SMALLINT,
	pb_keep_inprocess BOOLEAN DEFAULT FALSE)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lr_rec RECORD;
	ln_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE;
	lv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE;	
BEGIN
	UPDATE MQ.QUEUE Q
	   SET LOCKED_INSTANCE_ID = pv_instance_id,
	       LOCKED_TS = STATEMENT_TIMESTAMP(),
	       LOCKED_PRIORITY = pn_priority
	 WHERE Q.QUEUE_ID = pn_queue_id
	   AND (Q.LOCKED_INSTANCE_ID IS NULL
	   		OR Q.LOCKED_INSTANCE_ID = pv_instance_id
	   		OR Q.LOCKED_PRIORITY < pn_priority
	   		OR NOT EXISTS(SELECT 1
	   			FROM MQ.INSTANCE I
	   		   WHERE I.INSTANCE_ID = Q.LOCKED_INSTANCE_ID
	   		     AND I.LOCK_EXPIRATION_TS BETWEEN STATEMENT_TIMESTAMP() AND STATEMENT_TIMESTAMP() + INTERVAL'5 minutes'))
	 RETURNING Q.QUEUE_TABLE INTO lv_queue_table;
	   		   
	IF FOUND THEN
		IF pn_clear_before > 0 THEN
			EXECUTE 'DELETE FROM MQ.' || lv_queue_table
				 || ' WHERE POSTED_TIME < $1'
				USING pn_clear_before;
		END IF;	
		EXECUTE 'UPDATE MQ.' || lv_queue_table || ' SET REDELIVERED_FLAG = TRUE'
			 || ' WHERE MESSAGE_ID IN('
			 || 'SELECT MESSAGE_ID'
			 || ' FROM MQ.I' || lv_queue_table|| ')';
		IF pb_keep_inprocess THEN
		    EXECUTE 'DELETE FROM MQ.I' || lv_queue_table || ' WHERE CONNECTION_ID NOT IN(SELECT INSTANCE_ID FROM MQ.INSTANCE)';
		ELSE
		    EXECUTE 'TRUNCATE TABLE MQ.I' || lv_queue_table;
		END IF;
		
		FOR lr_rec IN 
			SELECT PRIORITY, LAST_AVAILABLE_TIME
			  FROM MQ.QUEUE_SHADOW_DELETION
		     WHERE QUEUE_ID = pn_queue_id LOOP
			EXECUTE 'SELECT POSTED_TIME, MESSAGE_ID FROM MQ.' || lv_queue_table || ' WHERE AVAILABLE_TIME <= $2 AND PRIORITY = $1 ORDER BY POSTED_TIME ASC, MESSAGE_ID ASC LIMIT 1'
			   INTO ln_last_posted_time, lv_last_message_id
			  USING lr_rec.PRIORITY, lr_rec.LAST_AVAILABLE_TIME;
			IF ln_last_posted_time IS NOT NULL THEN
				UPDATE MQ.QUEUE_SHADOW_DELETION
				   SET LAST_POSTED_TIME = ln_last_posted_time,
				       LAST_MESSAGE_ID = lv_last_message_id
		         WHERE QUEUE_ID = pn_queue_id
		           AND PRIORITY = lr_rec.PRIORITY;
			END IF;
		END LOOP;
		RETURN TRUE;
	ELSE
		RETURN FALSE;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.REMOVE_INPROCESS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	EXECUTE 'TRUNCATE TABLE MQ.I' || lv_queue_table;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.RELEASE_QUEUE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
BEGIN
	UPDATE MQ.QUEUE Q
	   SET LOCKED_INSTANCE_ID = NULL
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
	   AND DATA_SOURCE_NAME IS NULL
	   AND Q.LOCKED_INSTANCE_ID = pv_instance_id;
	RETURN FOUND;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.MARK_INPROCESS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.IQ_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pb_check_existence BOOLEAN,
	pb_redelivered OUT MQ.Q_.REDELIVERED_FLAG%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lv_current_connection_id MQ.IQ_.CONNECTION_ID%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	IF pb_check_existence THEN
		EXECUTE 'SELECT REDELIVERED_FLAG FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
		   INTO pb_redelivered
		  USING pv_message_id;
		IF pb_redelivered IS NULL THEN
			RAISE EXCEPTION 'Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U01', HINT = 'Another poller processed or is processing this message, or queue was cleared';
		END IF;
	END IF;
	BEGIN
		EXECUTE 'INSERT INTO MQ.I' || lv_queue_table || '(MESSAGE_ID, CONNECTION_ID) VALUES($1,$2)'
		  USING pv_message_id, pv_connection_id;
	EXCEPTION
		WHEN unique_violation THEN
			EXECUTE 'SELECT CONNECTION_ID FROM MQ.I' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
			   INTO lv_current_connection_id  
			  USING pv_message_id;
			IF pv_connection_id != lv_current_connection_id THEN
				RAISE EXCEPTION 'Message Id ''%'' is already marked in process by connection ''%''', pv_message_id, lv_current_connection_id USING ERRCODE = 'unique_violation', HINT = 'Another poller processed or is processing this message';
			END IF;
	END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.DELETE_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
    pb_remove_inprocess BOOLEAN,
    pa_unblocks VARCHAR[])
    RETURNS VOID
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    ln_count INTEGER;
BEGIN
    IF pa_unblocks IS NOT NULL AND array_length(pa_unblocks, 1) > 0 THEN
        DELETE FROM MQ.BLOCK WHERE BLOCK_KEY = ANY(pa_unblocks);
    END IF;
    lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
    IF pb_remove_inprocess AND pv_data_source_name IS NULL THEN
        EXECUTE 'DELETE FROM MQ.I' || lv_queue_table || ' WHERE MESSAGE_ID = $1 AND CONNECTION_ID = $2'
          USING pv_message_id, pv_connection_id;
        GET DIAGNOSTICS ln_count = ROW_COUNT;
        IF ln_count < 1 THEN
            RAISE EXCEPTION 'Inprocess Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U02', HINT = 'Another poller processed or is processing this message, or queue was cleared';
        END IF;
    END IF;
    BEGIN
        EXECUTE 'DELETE FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
          USING pv_message_id;
        GET DIAGNOSTICS ln_count = ROW_COUNT;
        IF ln_count < 1 THEN
            RAISE EXCEPTION 'Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U01', HINT = 'Another poller processed or is processing this message, or queue was cleared';
        END IF;
    EXCEPTION
        WHEN undefined_table THEN
            -- do nothing
    END;    
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.REMOVE_EXPIRED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[])
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	BEGIN
		EXECUTE 'DELETE FROM MQ.' || lv_queue_table 
			 || ' WHERE EXPIRE_TIME != 0 AND EXPIRE_TIME <= $1'
			 || ' AND MESSAGE_ID NOT IN(SELECT MESSAGE_ID FROM MQ.I' || lv_queue_table || ')'
			 || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
		  USING pn_current_time, pa_exclude_message_ids;
		GET DIAGNOSTICS ln_count = ROW_COUNT;
	EXCEPTION
		WHEN undefined_table THEN
			ln_count := 0;
	END;
	RETURN ln_count;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.RETRY_MESSAGE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pn_retry_count MQ.Q_.RETRY_COUNT%TYPE,
	pv_error_message MQ.Q_.ERROR_MESSAGE%TYPE,
	pb_no_retry BOOLEAN,
	pb_remove_inprocess BOOLEAN)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	IF pb_remove_inprocess AND pv_data_source_name IS NULL THEN
		EXECUTE 'DELETE FROM MQ.I' || lv_queue_table || ' WHERE MESSAGE_ID = $1 AND CONNECTION_ID = $2'
		  USING pv_message_id, pv_connection_id;
		GET DIAGNOSTICS ln_count = ROW_COUNT;
		IF ln_count < 1 THEN
			RAISE EXCEPTION 'Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U01', HINT = 'Another poller processed or is processing this message, or queue was cleared';
		END IF;
	END IF;
	IF pb_no_retry THEN
		BEGIN
			EXECUTE 'INSERT INTO MQ.DLQ(QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME,'
				|| ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME, RETRY_COUNT, ERROR_MESSAGE, CORRELATION)'
				|| ' SELECT QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, $2,'
				|| ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME, $3, $4, CORRELATION'
				|| ' FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
				USING pv_message_id, pn_available_time, pn_retry_count, pv_error_message;
			EXECUTE 'DELETE FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
			  USING pv_message_id;
		EXCEPTION
			WHEN undefined_table THEN
				-- do nothing
		END;
	ELSE
		BEGIN
			EXECUTE 'UPDATE MQ.' || lv_queue_table || ' SET AVAILABLE_TIME = $2, RETRY_COUNT = $3, ERROR_MESSAGE = $4 WHERE MESSAGE_ID = $1'
			  USING pv_message_id, pn_available_time, pn_retry_count, pv_error_message;
		EXCEPTION
			WHEN undefined_table THEN
				-- do nothing
		END;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.CLEAR_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	IF pv_data_source_name IS NULL THEN
		EXECUTE 'TRUNCATE TABLE MQ.I' || lv_queue_table;
	END IF;
	BEGIN
		EXECUTE 'TRUNCATE TABLE MQ.' || lv_queue_table;
	EXCEPTION
		WHEN undefined_table THEN
			-- do nothing
	END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.DROP_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
BEGIN
	DELETE FROM MQ.QUEUE
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name) 
	   AND DATA_SOURCE_NAME IS NULL;
END;
$$ LANGUAGE plpgsql;
   
CREATE OR REPLACE FUNCTION MQ.ADD_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE,
    pa_blocks VARCHAR[])
    RETURNS VOID
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
    lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
    BEGIN
        EXECUTE 'INSERT INTO MQ.' || lv_queue_table || '(MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME,'
            || ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME, CORRELATION, BLOCKS)'
            || ' VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)'
            USING pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, 
                  pb_temporary_flag, pv_shadow_data_source_name, pn_correlation, pa_blocks;
    EXCEPTION
        WHEN undefined_table THEN
            PERFORM MQ.REGISTER_QUEUE(pv_data_source_name, pv_queue_name, pv_subscription_name);
            PERFORM MQ.ADD_MESSAGE(pv_data_source_name, pv_queue_name, pv_subscription_name, pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, pb_temporary_flag, pv_shadow_data_source_name, pn_correlation, pa_blocks); 
    END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.BROADCAST_MESSAGE(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE,
    pa_blocks VARCHAR[])
    RETURNS VARCHAR[]
    SECURITY DEFINER
AS $$
DECLARE
    la_subscriptions VARCHAR[];
BEGIN
    SELECT ARRAY_AGG(DISTINCT SUBSCRIPTION_NAME ORDER BY SUBSCRIPTION_NAME)
      INTO la_subscriptions
      FROM MQ.QUEUE 
     WHERE QUEUE_NAME = pv_queue_name 
       AND DATA_SOURCE_NAME IS NULL
       AND SUBSCRIPTION_NAME IS NOT NULL;
    IF la_subscriptions IS NOT NULL AND ARRAY_LENGTH(la_subscriptions, 1) > 0 THEN
        FOR i IN ARRAY_LOWER(la_subscriptions, 1)..ARRAY_UPPER(la_subscriptions, 1) LOOP
            PERFORM MQ.ADD_MESSAGE(NULL, pv_queue_name, la_subscriptions[i], pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, pb_temporary_flag, pv_shadow_data_source_name, pn_correlation, pa_blocks);     
        END LOOP;
    END IF;
    RETURN la_subscriptions;
END;
$$ LANGUAGE plpgsql;

	
CREATE OR REPLACE FUNCTION MQ.REGISTER_SUBSCRIPTIONS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pa_subscriptions VARCHAR[])
    RETURNS VARCHAR[]
	SECURITY DEFINER
AS $$
DECLARE
	la_missing_subscriptions VARCHAR[];
	la_new_subscriptions VARCHAR[];
BEGIN
	SELECT ARRAY_AGG(DISTINCT CASE WHEN S IS NULL THEN Q.SUBSCRIPTION_NAME END), 
	       ARRAY_AGG(DISTINCT CASE WHEN Q.SUBSCRIPTION_NAME IS NULL THEN S END)
	  INTO la_missing_subscriptions, la_new_subscriptions
	  FROM (SELECT * 
	          FROM  MQ.QUEUE 
	         WHERE QUEUE_NAME = pv_queue_name 
	           AND DATA_SOURCE_NAME IS NULL
	           AND SUBSCRIPTION_NAME IS NOT NULL) Q
	  FULL OUTER JOIN UNNEST(pa_subscriptions) S ON Q.SUBSCRIPTION_NAME = S
	 WHERE S IS NULL OR Q.SUBSCRIPTION_NAME IS NULL;
	   
	IF la_new_subscriptions IS NOT NULL AND ARRAY_LENGTH(la_new_subscriptions, 1) > 0 THEN
		FOR i IN ARRAY_LOWER(la_new_subscriptions, 1)..ARRAY_UPPER(la_new_subscriptions, 1) LOOP
			PERFORM MQ.REGISTER_QUEUE(pv_data_source_name, pv_queue_name, la_new_subscriptions[i]);		
		END LOOP;
	END IF;
	RETURN la_missing_subscriptions;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_MESSAGES(
    pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pn_max_rows INTEGER,
    pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pa_exclude_message_ids VARCHAR(100)[],
    pa_exclude_correlations BIGINT[])
    RETURNS TABLE(
        MESSAGE_ID MQ.Q_.MESSAGE_ID%TYPE, 
        CONTENT_HEX TEXT,
        POSTED_TIME MQ.Q_.POSTED_TIME%TYPE,
        AVAILABLE_TIME MQ.Q_.AVAILABLE_TIME%TYPE,
        EXPIRE_TIME MQ.Q_.EXPIRE_TIME%TYPE,
        PRIORITY MQ.Q_.PRIORITY%TYPE,
        TEMPORARY_FLAG MQ.Q_.TEMPORARY_FLAG%TYPE,
        REDELIVERED_FLAG MQ.Q_.REDELIVERED_FLAG%TYPE,
        RETRY_COUNT MQ.Q_.RETRY_COUNT%TYPE,
        SHADOW_DATA_SOURCE_NAME MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
        CORRELATION MQ.Q_.CORRELATION%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    lv_locked_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE;
BEGIN
    SELECT LOCKED_INSTANCE_ID, QUEUE_TABLE
      INTO lv_locked_instance_id, lv_queue_table
      FROM MQ.QUEUE
     WHERE QUEUE_NAME = pv_queue_name
       AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
           AND DATA_SOURCE_NAME IS NULL;
    IF lv_locked_instance_id IS NULL OR lv_locked_instance_id != pv_instance_id THEN
        RAISE EXCEPTION 'Instance ''%'' does not currently hold the lock for ''%''', pv_instance_id, pv_queue_name USING ERRCODE = '55U06', HINT = 'Instance ''' || lv_locked_instance_id || ''' holds the lock';
    END IF;
    RETURN QUERY EXECUTE 'SELECT MESSAGE_ID, ENCODE(CONTENT, ''HEX''), POSTED_TIME, AVAILABLE_TIME, EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, REDELIVERED_FLAG, RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, CORRELATION'
        || ' FROM MQ.' || lv_queue_table || ' Q'
        || ' WHERE AVAILABLE_TIME <= $1'
        || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
        || ' AND ($4 IS NULL OR CORRELATION IS NULL OR CORRELATION != ALL($4))'
        || ' AND (EXPIRE_TIME = 0 OR EXPIRE_TIME > $1)'
        || ' AND MESSAGE_ID NOT IN(SELECT MESSAGE_ID FROM MQ.I' || lv_queue_table || ')'
        || ' AND (BLOCKS IS NULL OR NOT EXISTS(SELECT 1 FROM MQ.BLOCK B WHERE B.DATA_SOURCE_NAME IS NULL AND B.BLOCK_KEY = ANY(Q.BLOCKS) AND (B.EXPIRE_TIME = 0 OR B.EXPIRE_TIME > $1)))'
        || ' ORDER BY PRIORITY DESC, POSTED_TIME, MESSAGE_ID LIMIT $3'
        USING pn_current_time, pa_exclude_message_ids, pn_max_rows, pa_exclude_correlations;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_COUNT_OF_WAITERS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	ln_count INTEGER;
BEGIN
	SELECT COUNT(*)
	  INTO ln_count
	  FROM PG_LOCKS L
	  JOIN MQ.QUEUE Q ON Q.QUEUE_ID = L.OBJID AND Q.PRIMARY_WAITING_PID = L.PID
	 WHERE L.CLASSID = 105
	   AND L.OBJSUBID = 2
	   AND L.LOCKTYPE = 'advisory'
	   AND L.GRANTED = FALSE
	   AND Q.QUEUE_NAME = pv_queue_name
	   AND ((Q.SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR Q.SUBSCRIPTION_NAME = pv_subscription_name) 
	   AND DATA_SOURCE_NAME IS NULL;
	RETURN ln_count;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.QUEUE_EXISTS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
DECLARE
	ln_count INTEGER;
BEGIN
	SELECT COUNT(*)
	  INTO ln_count
	  FROM MQ.QUEUE
	 WHERE QUEUE_NAME = pv_queue_name
	   AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name) 
	   AND DATA_SOURCE_NAME IS NULL;
	RETURN ln_count > 0;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_SHADOW_DELETIONS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE)
    RETURNS TABLE(
  		SHADOW_DATA_SOURCE_NAME MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
  		PRIORITY MQ.Q_.PRIORITY%TYPE,
  		LAST_MESSAGE_ID MQ.Q_.MESSAGE_ID%TYPE, 
  		LAST_POSTED_TIME MQ.Q_.POSTED_TIME%TYPE)
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(NULL, pv_queue_name, pv_subscription_name);
	RETURN QUERY EXECUTE 'SELECT SHADOW_DATA_SOURCE_NAME, PRIORITY, MIN(MESSAGE_ID)::VARCHAR, MIN(POSTED_TIME)'
		|| ' FROM MQ.' || lv_queue_table 
		|| ' WHERE AVAILABLE_TIME <= $1'
		|| ' GROUP BY SHADOW_DATA_SOURCE_NAME, PRIORITY'
		USING pn_current_time;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.MARK_FOR_SHADOW_DELETION(
	pv_shadow_data_source_name MQ.QUEUE_SHADOW_DELETION.SHADOW_DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_priority MQ.QUEUE_SHADOW_DELETION.PRIORITY%TYPE,
	pv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE,
	pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
	pn_last_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	ln_queue_id MQ.QUEUE.QUEUE_ID%TYPE;
	lv_current_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE;
	ln_current_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE;
	ln_current_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE;
BEGIN
	SELECT Q.QUEUE_ID
	  INTO STRICT ln_queue_id
	  FROM MQ.QUEUE Q
	 WHERE Q.QUEUE_NAME = pv_queue_name
		   AND ((Q.SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR Q.SUBSCRIPTION_NAME = pv_subscription_name)
		   AND Q.DATA_SOURCE_NAME IS NULL;
	SELECT LAST_MESSAGE_ID, LAST_POSTED_TIME, LAST_AVAILABLE_TIME
	  INTO lv_current_message_id, ln_current_posted_time, ln_current_available_time
	  FROM MQ.QUEUE_SHADOW_DELETION
	 WHERE SHADOW_DATA_SOURCE_NAME = pv_shadow_data_source_name
	   AND PRIORITY = pn_priority
	   AND QUEUE_ID = ln_queue_id
	   FOR UPDATE;
	IF NOT FOUND THEN
		BEGIN	
			INSERT INTO MQ.QUEUE_SHADOW_DELETION(QUEUE_ID, SHADOW_DATA_SOURCE_NAME, PRIORITY, LAST_MESSAGE_ID, LAST_POSTED_TIME, LAST_AVAILABLE_TIME)
			  VALUES(ln_queue_id, pv_shadow_data_source_name, pn_priority, pv_last_message_id, pn_last_posted_time, pn_last_available_time);
		EXCEPTION 
			WHEN unique_violation THEN
        		PERFORM MQ.MARK_FOR_SHADOW_DELETION(pv_shadow_data_source_name, pv_queue_name, pv_subscription_name, pn_priority, pv_last_message_id, pn_last_posted_time, pn_last_available_time);
    	END;
	ELSIF pn_last_available_time > ln_current_available_time 
	  OR (pn_last_available_time = ln_current_available_time AND (pn_last_posted_time > ln_current_posted_time 
	  OR (pn_last_posted_time = ln_current_posted_time AND pv_last_message_id > lv_current_message_id))) THEN
		UPDATE MQ.QUEUE_SHADOW_DELETION
		   SET LAST_MESSAGE_ID = pv_last_message_id,
		   	   LAST_POSTED_TIME = pn_last_posted_time,
		   	   LAST_AVAILABLE_TIME = pn_last_available_time
		 WHERE SHADOW_DATA_SOURCE_NAME = pv_shadow_data_source_name
		   AND PRIORITY = pn_priority
		   AND QUEUE_ID = ln_queue_id;
		IF NOT FOUND THEN
			PERFORM MQ.MARK_FOR_SHADOW_DELETION(pv_shadow_data_source_name, pv_queue_name, pv_subscription_name, pn_priority, pv_last_message_id, pn_last_posted_time, pn_last_available_time);
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
	  	
CREATE OR REPLACE FUNCTION MQ.REMOVE_SHADOW_MESSAGES(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_priority MQ.QUEUE_SHADOW_DELETION.PRIORITY%TYPE,
	pv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE,
	pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
	pn_last_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[])
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	EXECUTE 'DELETE FROM MQ.' || lv_queue_table 
		 || ' WHERE AVAILABLE_TIME <= $1'
		 || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
		 || ' AND PRIORITY = $3'
		 || ' AND (POSTED_TIME < $4 OR (POSTED_TIME = $4 AND MESSAGE_ID < $5))'
	  USING pn_last_available_time, pa_exclude_message_ids, pn_priority, pn_last_posted_time, pv_last_message_id;
	GET DIAGNOSTICS ln_count = ROW_COUNT;
	RETURN ln_count;
END;
$$ LANGUAGE plpgsql;
  	
CREATE OR REPLACE FUNCTION MQ.REMOVE_SHADOW_MESSAGES_MISSING(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pa_exclude_priorities SMALLINT[],
    pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
    pa_exclude_message_ids VARCHAR(100)[])
    RETURNS INTEGER
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    ln_count INTEGER;
BEGIN
    lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
    BEGIN
        EXECUTE 'DELETE FROM MQ.' || lv_queue_table 
             || ' WHERE AVAILABLE_TIME <= $1'
             || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
             || ' AND ($3 IS NULL OR PRIORITY != ALL($3))'
             || ' AND POSTED_TIME < $4'
          USING pn_last_posted_time, pa_exclude_message_ids, pa_exclude_priorities, pn_last_posted_time;
        GET DIAGNOSTICS ln_count = ROW_COUNT;
    EXCEPTION
        WHEN undefined_table THEN
            ln_count := 0;
    END; 
    RETURN ln_count;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.START_CARRYOVER(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_copy_from_sql OUT TEXT,
	pv_copy_to_sql OUT TEXT)
    SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lv_new_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	lv_new_queue_table := MQ.GET_QUEUE_TABLE(NULL, pv_queue_name, pv_subscription_name);
	EXECUTE 'LOCK MQ.' || lv_queue_table || ' IN EXCLUSIVE MODE';
	EXECUTE 'SELECT COUNT(*) FROM MQ.' || lv_queue_table INTO ln_count;
	IF ln_count = 0 THEN
		pv_copy_from_sql := NULL;
		pv_copy_to_sql := NULL;
	ELSE
		pv_copy_from_sql := 'COPY (SELECT ''' || lv_new_queue_table || ''' QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME, EXPIRE_TIME, '
			|| 'PRIORITY, TEMPORARY_FLAG, TRUE REDELIVERED_FLAG, RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, ERROR_MESSAGE, CORRELATION FROM MQ.'
			|| lv_queue_table || ') TO STDOUT (FORMAT ''binary'')';
		pv_copy_to_sql := 'COPY MQ.' || lv_new_queue_table || ' FROM STDIN (FORMAT ''binary'')';
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FINISH_CARRYOVER(
	pv_new_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_old_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_shadow_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pn_last_posted_time OUT MQ.Q_.POSTED_TIME%TYPE,
	pn_row_count OUT INTEGER)
    SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lv_new_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lv_new_table_name VARCHAR;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_old_data_source_name, pv_queue_name, pv_subscription_name);
	lv_new_queue_table := MQ.GET_QUEUE_TABLE(pv_new_data_source_name, pv_queue_name, pv_subscription_name);
	SELECT 'C' || lv_queue_table || '_' || TO_HEX(COALESCE(MAX(FROM_HEX(SUBSTR(TABLENAME, 55, 8))::INTEGER), 0) + 1)
	  INTO STRICT lv_new_table_name
	  FROM PG_TABLES 
     WHERE SCHEMANAME = 'mq'
       AND SUBSTR(TABLENAME, 1, 54) = 'C' || SUBSTR(lv_queue_table, 1, 52) || '_'
       AND SUBSTR(TABLENAME, 55, 8) ~ '^[0-9A-Fa-f]{8}$';
	EXECUTE 'ALTER TABLE MQ.' || lv_queue_table || ' RENAME TO ' || lv_new_table_name;
	EXECUTE 'ALTER INDEX MQ.UX_' || lv_queue_table || '_ORDER RENAME TO UX_' || lv_new_table_name || '_ORDER';
	EXECUTE 'ALTER INDEX MQ.IX_' || lv_queue_table || '_EXPIRE_TIME RENAME TO IX_' || lv_new_table_name || '_EXPIRE_TIME';
	EXECUTE 'ALTER TABLE MQ.' || lv_new_table_name || ' DROP CONSTRAINT CK_' || lv_queue_table;
	PERFORM MQ.CREATE_QUEUE(lv_queue_table, false);	
	EXECUTE 'SELECT MAX(POSTED_TIME) FROM MQ.' || lv_new_table_name
		INTO pn_last_posted_time;  
	EXECUTE 'UPDATE MQ.' || lv_new_table_name || ' SET QUEUE_TABLE = $1, REDELIVERED_FLAG = TRUE'
		USING lv_new_queue_table;	
	GET DIAGNOSTICS pn_row_count = ROW_COUNT;
	PERFORM MQ.SET_CARRYOVER_LAST_POSTED(pv_old_data_source_name, pv_queue_name, pv_subscription_name, pv_shadow_data_source_name, pn_last_posted_time);
	PERFORM MQ.GET_QUEUE_ID(pv_new_data_source_name, pv_queue_name, pv_subscription_name); -- Make sure it exists
	EXECUTE 'ALTER TABLE MQ.' || lv_new_table_name || ' ADD CONSTRAINT CK_' || lv_new_queue_table || ' CHECK (QUEUE_TABLE = ''' || lv_new_queue_table || ''')';
	EXECUTE 'ALTER TABLE MQ.' || lv_new_table_name || ' INHERIT MQ.' || lv_new_queue_table;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.CLEAN_EMPTY_CARRYOVERS()
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	lr_rec RECORD;
	ln_count INTEGER;
	ln_num INTEGER := 0;
BEGIN
	FOR lr_rec IN 
		SELECT T.SCHEMANAME || '.' || T.RELNAME TABLENAME
		  FROM PG_STAT_ALL_TABLES T
		  JOIN PG_NAMESPACE NS ON T.SCHEMANAME = NS.NSPNAME
	      JOIN PG_INHERITS I ON T.RELID = I.INHRELID
	      JOIN PG_CLASS PC ON I.INHPARENT = PC.OID AND NS.OID = PC.RELNAMESPACE
	      JOIN PG_TABLES PT ON PC.RELNAME = PT.TABLENAME AND PT.SCHEMANAME = T.SCHEMANAME
	     WHERE T.SCHEMANAME = 'mq'
	       AND PT.TABLENAME != 'q_'
	       AND SUBSTR(PT.TABLENAME, 1, 2) = 'q_'  
		   AND SUBSTR(T.RELNAME, 1, 3) = 'cq_'
		   /*AND T.N_LIVE_TUP = 0*/
		 ORDER BY T.RELNAME LOOP
		EXECUTE 'SELECT COUNT(*) FROM ' || lr_rec.TABLENAME INTO ln_count;
		IF ln_count = 0 THEN
			EXECUTE 'DROP TABLE ' || lr_rec.TABLENAME;
			ln_num := ln_num + 1;
		END IF;
	 END LOOP;
	 RETURN ln_num;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.SET_CARRYOVER_LAST_POSTED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_shadow_data_source_name MQ.CARRYOVER_CLEAR_BEFORE.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_last_posted_time MQ.CARRYOVER_CLEAR_BEFORE.LAST_POSTED_TIME%TYPE)
	RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	ln_count INTEGER;
	ln_queue_id MQ.QUEUE.QUEUE_ID%TYPE;
BEGIN
	ln_queue_id := MQ.GET_QUEUE_ID(pv_data_source_name, pv_queue_name, pv_subscription_name);
	UPDATE MQ.CARRYOVER_CLEAR_BEFORE
	   SET LAST_POSTED_TIME = pn_last_posted_time
	 WHERE QUEUE_ID = ln_queue_id
	   AND SHADOW_DATA_SOURCE_NAME = pv_shadow_data_source_name;
	GET DIAGNOSTICS ln_count = ROW_COUNT;
	IF ln_count = 0 THEN
		INSERT INTO MQ.CARRYOVER_CLEAR_BEFORE(QUEUE_ID, SHADOW_DATA_SOURCE_NAME, LAST_POSTED_TIME)
			VALUES(ln_queue_id, pv_shadow_data_source_name, pn_last_posted_time);
	END IF;
EXCEPTION 
	WHEN unique_violation THEN
    	PERFORM MQ.SET_CARRYOVER_LAST_POSTED(pv_data_source_name, pv_queue_name, pv_subscription_name, pv_shadow_data_source_name, pn_last_posted_time);        	
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_CARRYOVER_LAST_POSTED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
	RETURNS TABLE(
  		SHADOW_DATA_SOURCE_NAME MQ.CARRYOVER_CLEAR_BEFORE.SHADOW_DATA_SOURCE_NAME%TYPE,
		LAST_POSTED_TIME MQ.CARRYOVER_CLEAR_BEFORE.LAST_POSTED_TIME%TYPE)
	SECURITY DEFINER
AS $$
BEGIN
	RETURN QUERY
		SELECT CCB.SHADOW_DATA_SOURCE_NAME, CCB.LAST_POSTED_TIME
		  FROM MQ.CARRYOVER_CLEAR_BEFORE CCB
		  JOIN MQ.QUEUE Q ON CCB.QUEUE_ID = Q.QUEUE_ID 
		 WHERE Q.DATA_SOURCE_NAME = pv_data_source_name
		   AND Q.QUEUE_NAME = pv_queue_name
		   AND ((Q.SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR Q.SUBSCRIPTION_NAME = pv_subscription_name)
		   AND CCB.LAST_POSTED_TIME > 0;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_SHADOW_QUEUE_TABLES(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE)
    RETURNS TABLE(
  		QUEUE_NAME MQ.QUEUE.QUEUE_NAME%TYPE,
		SUBSCRIPTION_NAME MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
	SECURITY DEFINER
AS $$
BEGIN
	RETURN QUERY 
		SELECT Q.QUEUE_NAME, Q.SUBSCRIPTION_NAME
		  FROM MQ.QUEUE Q
		 WHERE Q.DATA_SOURCE_NAME = pv_data_source_name;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.HOLDS_QUEUE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
DECLARE
	lv_locked_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE;
BEGIN
	SELECT LOCKED_INSTANCE_ID
	  INTO lv_locked_instance_id
	  FROM MQ.QUEUE Q
	 WHERE Q.QUEUE_NAME = pv_queue_name
		   AND ((Q.SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR Q.SUBSCRIPTION_NAME = pv_subscription_name)
		   AND Q.DATA_SOURCE_NAME IS NULL;
	RETURN lv_locked_instance_id IS NOT NULL AND lv_locked_instance_id = pv_instance_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.RENEW_INSTANCE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	ln_time_ms BIGINT)
    RETURNS TIMESTAMP WITH TIME ZONE
	SECURITY DEFINER
AS $$
DECLARE
	ld_expiration_ts MQ.INSTANCE.LOCK_EXPIRATION_TS%TYPE;
BEGIN
	UPDATE MQ.INSTANCE
	   SET LOCK_EXPIRATION_TS = CASE 
	           WHEN LOCK_EXPIRATION_TS > STATEMENT_TIMESTAMP() + (5 * ln_time_ms || ' millisecond')::interval THEN STATEMENT_TIMESTAMP() + (ln_time_ms || ' millisecond')::interval /* The unreasonable */
	           WHEN LOCK_EXPIRATION_TS < STATEMENT_TIMESTAMP() + (ln_time_ms || ' millisecond')::interval THEN STATEMENT_TIMESTAMP() + (ln_time_ms || ' millisecond')::interval /* The old */
	           ELSE LOCK_EXPIRATION_TS END
	 WHERE INSTANCE_ID = pv_instance_id
	RETURNING LOCK_EXPIRATION_TS INTO ld_expiration_ts;
	IF NOT FOUND THEN
		BEGIN
			INSERT INTO MQ.INSTANCE(INSTANCE_ID, LOCK_EXPIRATION_TS)
			   VALUES(pv_instance_id, STATEMENT_TIMESTAMP() + (ln_time_ms || ' millisecond')::interval)
			RETURNING LOCK_EXPIRATION_TS INTO ld_expiration_ts;
		EXCEPTION
			WHEN unique_violation THEN
				RETURN MQ.RENEW_INSTANCE_LOCK(pv_instance_id, ln_time_ms);
		END;
	END IF;
	RETURN ld_expiration_ts;
END;
$$ LANGUAGE plpgsql;
	
CREATE OR REPLACE FUNCTION MQ.CLEANUP_EXPIRED_LOCKS(
	ln_time_ms BIGINT)
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	ln_count INTEGER := 0;
	lv_instance_id MQ.INSTANCE.INSTANCE_ID%TYPE;
BEGIN
	FOR lv_instance_id IN
		DELETE 
		  FROM MQ.INSTANCE
		 WHERE LOCK_EXPIRATION_TS < STATEMENT_TIMESTAMP() - (ln_time_ms || ' millisecond')::interval
		 RETURNING INSTANCE_ID
	LOOP
	    DELETE FROM MQ.IQ_
	     WHERE CONNECTION_ID = lv_instance_id;
		ln_count := ln_count + 1;
	END LOOP;
	RETURN ln_count;	 
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_LOCKED_QUEUES()
    RETURNS TABLE(
  		DATA_SOURCE_NAME MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
  		QUEUE_NAME MQ.QUEUE.QUEUE_NAME%TYPE, 
  		SUBSCRIPTION_NAME MQ.QUEUE.SUBSCRIPTION_NAME%TYPE, 
		LOCKED_INSTANCE_ID MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE, 
		LOCKED_PRIORITY MQ.QUEUE.LOCKED_PRIORITY%TYPE,
		LOCKED_TS MQ.QUEUE.LOCKED_TS%TYPE,
		LOCK_EXPIRATION_TS MQ.INSTANCE.LOCK_EXPIRATION_TS%TYPE, 
		IS_EXPIRED BOOLEAN)
	SECURITY DEFINER
AS $$
BEGIN
	RETURN QUERY 
	SELECT Q.DATA_SOURCE_NAME, Q.QUEUE_NAME, Q.SUBSCRIPTION_NAME, Q.LOCKED_INSTANCE_ID, 
		   Q.LOCKED_PRIORITY, Q.LOCKED_TS, I.LOCK_EXPIRATION_TS, STATEMENT_TIMESTAMP() > I.LOCK_EXPIRATION_TS
      FROM MQ.QUEUE Q
	  LEFT OUTER JOIN MQ.INSTANCE I ON Q.LOCKED_INSTANCE_ID = I.INSTANCE_ID
     WHERE Q.LOCKED_INSTANCE_ID IS NOT NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.CHECK_EXISTENCE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.IQ_.MESSAGE_ID%TYPE)
    RETURNS VOID
	SECURITY DEFINER
    STABLE
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	pb_redelivered BOOLEAN;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	EXECUTE 'SELECT REDELIVERED_FLAG FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
	   INTO pb_redelivered
	  USING pv_message_id;
	IF pb_redelivered IS NULL THEN
		RAISE EXCEPTION 'Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U01', HINT = 'Another poller processed or is processing this message, or queue was cleared';
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.RESURRECT_MESSAGE(
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lr_message MQ.Q_%ROWTYPE;
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	FOR lr_message IN 
		SELECT QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, 0 AVAILABLE_TIME, EXPIRE_TIME, 
			   PRIORITY, TEMPORARY_FLAG, true REDELIVERED_FLAG, 0 RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, ERROR_MESSAGE, CORRELATION
		  FROM MQ.DLQ 
		 WHERE MESSAGE_ID = pv_message_id LOOP

		EXECUTE 'INSERT INTO MQ.' || lr_message.queue_table || ' SELECT (X).* FROM (SELECT $1 AS X) SS'
		  USING lr_message;		  
		DELETE FROM MQ.DLQ WHERE MESSAGE_ID = lr_message.MESSAGE_ID;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE TYPE MQ.QUEUE_SIZE_TYPE AS (
    QUEUE_NAME VARCHAR(100),
    MESSAGE_COUNT BIGINT
);

CREATE OR REPLACE FUNCTION MQ.GET_QUEUE_SIZES(
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE)
    RETURNS SETOF MQ.QUEUE_SIZE_TYPE
	SECURITY DEFINER
	STABLE
AS $$
DECLARE
	ln_count MQ.QUEUE_SIZE_TYPE.MESSAGE_COUNT%TYPE;
	la_array MQ.QUEUE_SIZE_TYPE[] := ARRAY[]::MQ.QUEUE_SIZE_TYPE[];
	lr_rec RECORD;
	i INTEGER := 1;
BEGIN
	FOR lr_rec IN 
		SELECT Q.QUEUE_NAME, Q.SUBSCRIPTION_NAME, Q.QUEUE_TABLE
		  FROM MQ.QUEUE Q
		 WHERE Q.DATA_SOURCE_NAME IS NULL LOOP
		EXECUTE 'SELECT COUNT(*) FROM MQ.' || lr_rec.QUEUE_TABLE 
			 || ' WHERE AVAILABLE_TIME <= $1'
		   INTO ln_count
		  USING pn_current_time;
		la_array[i] := ROW(lr_rec.QUEUE_NAME, ln_count);
		i := i+ 1;
	END LOOP;
	RETURN QUERY SELECT QUEUE_NAME, SUM(MESSAGE_COUNT)::BIGINT FROM UNNEST(la_array) GROUP BY QUEUE_NAME;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.GET_QUEUE_SIZES(
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE) TO use_mq;
	
CREATE OR REPLACE FUNCTION MQ.DRAIN_DLQ(
	pn_min_posted_time MQ.DLQ.POSTED_TIME%TYPE,
	pn_max_posted_time MQ.DLQ.POSTED_TIME%TYPE,
	pv_file_path VARCHAR(4000),
	pn_message_count OUT BIGINT)
    SECURITY DEFINER
AS $$
DECLARE
	lv_sql VARCHAR(4000);
BEGIN
	lv_sql :='COPY (SELECT * FROM MQ.DLQ WHERE POSTED_TIME BETWEEN ' || pn_min_posted_time || ' AND ' || pn_max_posted_time || ') TO '
		|| QUOTE_LITERAL(pv_file_path) || ' (FORMAT ''csv'')';
	RAISE NOTICE 'Executing ''%''', lv_sql;
	EXECUTE lv_sql;
        DELETE 
          FROM MQ.DLQ 
         WHERE POSTED_TIME BETWEEN pn_min_posted_time AND pn_max_posted_time;
        GET DIAGNOSTICS pn_message_count = ROW_COUNT;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.DRAIN_DLQ(
	pn_before_time MQ.DLQ.POSTED_TIME%TYPE,
	pv_file_path OUT VARCHAR(4000),
	pn_message_count OUT BIGINT)
    SECURITY DEFINER 
AS $$
DECLARE
	ln_min_posted_time MQ.DLQ.POSTED_TIME%TYPE;
	ln_max_posted_time MQ.DLQ.POSTED_TIME%TYPE;
BEGIN
	SELECT MIN(POSTED_TIME), MAX(POSTED_TIME), COUNT(*)
	  INTO ln_min_posted_time, ln_max_posted_time, pn_message_count
	  FROM MQ.DLQ
	 WHERE pn_before_time IS NULL OR pn_before_time > POSTED_TIME;

	IF pn_message_count > 0 THEN
		SELECT '/home/postgres/dlq_' || current_database() || '_' || to_char('epoch'::timestamp + (ln_min_posted_time/1000 || ' seconds')::interval, 'YYYYMMDD-HHMISS')
			|| '_to_' || to_char('epoch'::timestamp + (ln_max_posted_time/1000 || ' seconds')::interval, 'YYYYMMDD-HHMISS') || '.csv'
		  INTO pv_file_path;
		SELECT a.pn_message_count
		  INTO pn_message_count
		  FROM MQ.DRAIN_DLQ(ln_min_posted_time, ln_max_posted_time, pv_file_path) a;		
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_SUBSCRIPTIONS(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE)
    RETURNS TABLE(
        SUBSCRIPTION_NAME MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    SECURITY DEFINER
AS $$
BEGIN
    RETURN QUERY SELECT DISTINCT Q.SUBSCRIPTION_NAME FROM MQ.QUEUE Q WHERE Q.QUEUE_NAME = pv_queue_name AND Q.SUBSCRIPTION_NAME IS NOT NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_BLOCK_COUNT(
    pa_blocks VARCHAR[])
    RETURNS BIGINT
    SECURITY DEFINER
AS $$
DECLARE
    ln_count BIGINT;
BEGIN
    SELECT COUNT(*)
      INTO ln_count
      FROM MQ.BLOCK
     WHERE BLOCK_KEY = ANY(pa_blocks)
       AND DATA_SOURCE_NAME IS NULL;
    RETURN ln_count;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE)
    RETURNS VARCHAR[]
    SECURITY DEFINER
AS $$
DECLARE
    la_blocks VARCHAR[];
BEGIN
    SELECT ARRAY_AGG(BLOCK_KEY)
      INTO la_blocks
      FROM MQ.BLOCK
     WHERE DATA_SOURCE_NAME IS NOT DISTINCT FROM pv_data_source_name;
    RETURN la_blocks;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.ADD_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pa_blocks VARCHAR[])
    RETURNS VOID
    SECURITY DEFINER
AS $$
BEGIN
    IF pv_data_source_name IS NULL THEN
        UPDATE MQ.BLOCK
           SET DATA_SOURCE_NAME = NULL
         WHERE DATA_SOURCE_NAME IS NOT NULL
           AND BLOCK_KEY = ANY(pa_blocks);     
    END IF;
    INSERT INTO MQ.BLOCK(BLOCK_KEY, EXPIRE_TIME)
        SELECT b, 0 
          FROM unnest(pa_blocks) b
         WHERE NOT EXISTS(SELECT 1 FROM MQ.BLOCK WHERE BLOCK_KEY = b);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.REMOVE_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pa_blocks VARCHAR[])
    RETURNS BIGINT
    SECURITY DEFINER
AS $$
DECLARE
    ln_count BIGINT;
BEGIN
    DELETE FROM MQ.BLOCK WHERE BLOCK_KEY = ANY(pa_blocks) AND DATA_SOURCE_NAME IS NOT DISTINCT FROM pv_data_source_name;
    GET DIAGNOSTICS ln_count = ROW_COUNT;
    RETURN ln_count;   
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.DRAIN_DLQ(
	pn_before_time MQ.DLQ.POSTED_TIME%TYPE,
	pv_file_path OUT VARCHAR(4000),
	pn_message_count OUT BIGINT) TO admin_1;
	
GRANT EXECUTE ON FUNCTION MQ.PREPARE_QUEUE_LOCK(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.ACQUIRE_QUEUE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	pn_queue_id MQ.QUEUE.QUEUE_ID%TYPE,
	pn_clear_before BIGINT,
	pn_priority SMALLINT,
    pb_keep_inprocess BOOLEAN) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.RELEASE_QUEUE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REMOVE_INPROCESS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.MARK_INPROCESS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.IQ_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pb_check_existence BOOLEAN,
	pb_redelivered OUT MQ.Q_.REDELIVERED_FLAG%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.DELETE_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
    pb_remove_inprocess BOOLEAN,
    pa_unblocks VARCHAR[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REMOVE_EXPIRED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.ADD_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE,
    pa_blocks VARCHAR[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.BROADCAST_MESSAGE(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE,
    pa_blocks VARCHAR[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REGISTER_SUBSCRIPTIONS(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pa_subscriptions VARCHAR[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.GET_MESSAGES(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_max_rows INTEGER,
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[],
    pa_exclude_correlations BIGINT[]) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.GET_COUNT_OF_WAITERS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.RETRY_MESSAGE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
	pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
	pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pn_retry_count MQ.Q_.RETRY_COUNT%TYPE,
	pv_error_message MQ.Q_.ERROR_MESSAGE%TYPE,
	pb_no_retry BOOLEAN,
	pb_remove_inprocess BOOLEAN) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.CLEAR_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) to use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REGISTER_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.QUEUE_EXISTS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.DROP_QUEUE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.GET_SHADOW_DELETIONS(
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_current_time MQ.Q_.AVAILABLE_TIME%TYPE) TO use_mq;
		
GRANT EXECUTE ON FUNCTION MQ.MARK_FOR_SHADOW_DELETION(
	pv_shadow_data_source_name MQ.QUEUE_SHADOW_DELETION.SHADOW_DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_priority MQ.QUEUE_SHADOW_DELETION.PRIORITY%TYPE,
	pv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE,
	pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
	pn_last_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.REMOVE_SHADOW_MESSAGES(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_priority MQ.QUEUE_SHADOW_DELETION.PRIORITY%TYPE,
	pv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE,
	pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
	pn_last_available_time MQ.QUEUE_SHADOW_DELETION.LAST_AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[]) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.REMOVE_SHADOW_MESSAGES_MISSING(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pa_exclude_priorities SMALLINT[],
    pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
    pa_exclude_message_ids VARCHAR(100)[]) TO use_mq;
    
GRANT EXECUTE ON FUNCTION MQ.START_CARRYOVER(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_copy_from_sql OUT TEXT,
	pv_copy_to_sql OUT TEXT) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.FINISH_CARRYOVER(
	pv_new_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_old_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_shadow_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pn_last_posted_time OUT MQ.Q_.POSTED_TIME%TYPE,
	pn_row_count OUT INTEGER) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.CLEAN_EMPTY_CARRYOVERS() TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.SET_CARRYOVER_LAST_POSTED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_shadow_data_source_name MQ.CARRYOVER_CLEAR_BEFORE.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_last_posted_time MQ.CARRYOVER_CLEAR_BEFORE.LAST_POSTED_TIME%TYPE) TO use_mq;
    
GRANT EXECUTE ON FUNCTION MQ.GET_CARRYOVER_LAST_POSTED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
    
GRANT EXECUTE ON FUNCTION MQ.GET_SHADOW_QUEUE_TABLES(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.HOLDS_QUEUE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.RENEW_INSTANCE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	ln_time_ms BIGINT) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.CLEANUP_EXPIRED_LOCKS(
	ln_time_ms BIGINT) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.GET_LOCKED_QUEUES() TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.CHECK_EXISTENCE(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pv_message_id MQ.IQ_.MESSAGE_ID%TYPE) TO use_mq;
	
GRANT EXECUTE ON FUNCTION MQ.RESURRECT_MESSAGE(
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.GET_SUBSCRIPTIONS(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.GET_BLOCK_COUNT(
    pa_blocks VARCHAR[]) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.GET_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.ADD_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pa_blocks VARCHAR[]) TO use_mq;
    
GRANT EXECUTE ON FUNCTION MQ.REMOVE_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pa_blocks VARCHAR[]) TO use_mq;
    
GRANT SELECT ON ALL TABLES IN SCHEMA MQ TO read_mq;
REVOKE EXECUTE ON ALL FUNCTIONS IN SCHEMA MQ FROM PUBLIC;
GRANT USAGE ON SCHEMA MQ TO read_mq;
GRANT USAGE ON SCHEMA MQ TO use_mq;
GRANT read_mq TO admin;
GRANT use_mq TO admin;
