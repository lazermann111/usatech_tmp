CREATE OR REPLACE FUNCTION MQ.CONSTRUCT_NAME(pv_base_name VARCHAR, pv_suffix VARCHAR)
    RETURNS VARCHAR
    SECURITY DEFINER
AS $$
BEGIN
    IF LENGTH(pv_base_name) + LENGTH(pv_suffix) <= 60 THEN
        RETURN pv_base_name || '_' || pv_suffix;
    ELSIF LENGTH(pv_base_name) > 51 AND LENGTH(pv_suffix) > 51 THEN
        RETURN SUBSTR(pv_base_name, 1, 21) || '$' || SUBSTR(MD5(pv_base_name), 1, 8) || '_' || SUBSTR(pv_suffix, 1, 21) || '$' || SUBSTR(MD5(pv_suffix), 1, 8);
    ELSIF LENGTH(pv_base_name) > 21 THEN
        RETURN SUBSTR(pv_base_name, 1, 51 - LENGTH(pv_suffix)) || '$' || SUBSTR(MD5(pv_base_name), 1, 8) || '_' || pv_suffix;
    ELSE
        RETURN pv_base_name || '_' || SUBSTR(pv_suffix, 1,  51 - LENGTH(pv_base_name)) || '_$' || SUBSTR(MD5(pv_suffix), 1, 8);
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.CREATE_QUEUE(
    pv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE,
    pb_primary BOOLEAN
) 
    RETURNS VOID 
    SECURITY DEFINER
AS $$
BEGIN
    EXECUTE 'CREATE TABLE MQ.' || pv_queue_table || '('
        || 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
        || 'CONSTRAINT CK_' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
        || 'PRIMARY KEY(MESSAGE_ID)'
        || ') INHERITS(MQ.Q_)';
    EXECUTE 'GRANT SELECT, INSERT ON MQ.'|| pv_queue_table || ' TO use_mq';
    EXECUTE 'ALTER TABLE MQ.'|| pv_queue_table || ' OWNER TO use_mq';
    EXECUTE 'CREATE UNIQUE INDEX ' || MQ.CONSTRUCT_NAME('UX_' || pv_queue_table, 'ORDER') || ' ON MQ.' || pv_queue_table || '(PRIORITY DESC, POSTED_TIME, MESSAGE_ID)';
    EXECUTE 'CREATE INDEX ' || MQ.CONSTRUCT_NAME('IX_' || pv_queue_table, 'AVAILABLE_TIME') || ' ON MQ.' || pv_queue_table || '(AVAILABLE_TIME)';
    EXECUTE 'CREATE INDEX ' || MQ.CONSTRUCT_NAME('IX_' || pv_queue_table, 'EXPIRE_TIME') || ' ON MQ.' || pv_queue_table || '(EXPIRE_TIME)';
    IF pb_primary THEN
        EXECUTE 'CREATE TABLE MQ.I' || pv_queue_table || '('
            || 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
            || 'CONSTRAINT CK_I' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
            || 'PRIMARY KEY(MESSAGE_ID)'
            || ') INHERITS(MQ.IQ_)';
        EXECUTE 'ALTER TABLE MQ.I'|| pv_queue_table || ' OWNER TO use_mq';  
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.FTRAU_QUEUE() 
    RETURNS TRIGGER 
    SECURITY DEFINER
AS $$
BEGIN
    EXECUTE 'ALTER TABLE MQ.' || OLD.QUEUE_TABLE || ' RENAME TO MQ.' || NEW.QUEUE_TABLE;
    EXECUTE 'ALTER TABLE MQ.' || NEW.QUEUE_TABLE || ' ALTER QUEUE_TABLE SET DEFAULT ''' || NEW.QUEUE_TABLE || '''';     
    EXECUTE 'ALTER TABLE MQ.' || NEW.QUEUE_TABLE || ' DROP CONSTRAINT CK_' || OLD.QUEUE_TABLE;
    EXECUTE 'ALTER TABLE MQ.' || NEW.QUEUE_TABLE || ' ADD CONSTRAINT CK_' || NEW.QUEUE_TABLE
        || ' CHECK (QUEUE_TABLE = ''' || NEW.QUEUE_TABLE || ''')';
    EXECUTE 'ALTER INDEX ' || MQ.CONSTRUCT_NAME('UX_' || OLD.QUEUE_TABLE, 'ORDER') || ' RENAME TO ' || MQ.CONSTRUCT_NAME('UX_' || NEW.QUEUE_TABLE, 'ORDER');
    EXECUTE 'ALTER INDEX ' || MQ.CONSTRUCT_NAME('IX_' || OLD.QUEUE_TABLE, 'AVAILABLE_TIME') || ' RENAME TO ' || MQ.CONSTRUCT_NAME('IX_' || NEW.QUEUE_TABLE, 'AVAILABLE_TIME');
    EXECUTE 'ALTER INDEX ' || MQ.CONSTRUCT_NAME('IX_' || OLD.QUEUE_TABLE, 'EXPIRE_TIME') || ' RENAME TO ' || MQ.CONSTRUCT_NAME('IX_' || NEW.QUEUE_TABLE, 'EXPIRE_TIME');
    IF NEW.DATA_SOURCE_NAME IS NULL THEN
        EXECUTE 'ALTER TABLE MQ.I' || OLD.QUEUE_TABLE || ' RENAME TO MQ.I' || NEW.QUEUE_TABLE;
        EXECUTE 'ALTER TABLE MQ.I' || NEW.QUEUE_TABLE || ' ALTER QUEUE_TABLE SET DEFAULT ''' || NEW.QUEUE_TABLE || '''';        
        EXECUTE 'ALTER TABLE MQ.I' || NEW.QUEUE_TABLE || ' DROP CONSTRAINT CK_I' || OLD.QUEUE_TABLE;
        EXECUTE 'ALTER TABLE MQ.I' || NEW.QUEUE_TABLE || ' ADD CONSTRAINT CK_I' || NEW.QUEUE_TABLE
            || ' CHECK (QUEUE_TABLE = ''' || NEW.QUEUE_TABLE || ''')';
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;
