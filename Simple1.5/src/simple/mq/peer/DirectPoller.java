/**
 * 
 */
package simple.mq.peer;

import java.io.EOFException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.channels.ByteChannel;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import javax.management.ObjectName;

import simple.db.ConnectionGroup;
import simple.db.DataLayerException;
import simple.io.ByteChannelByteInput;
import simple.io.ByteChannelByteOutput;
import simple.io.ByteOutput;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.TimedByteChannel;
import simple.io.server.TrackingJMXByteChannel;
import simple.lang.InvalidByteValueException;
import simple.text.MessageFormat;
import simple.util.RecentExceptionSet;
import simple.util.concurrent.QueueBuffer;

public class DirectPoller extends AbstractPoller {
	private static final Log log = Log.getLog();
	protected final DirectPollerConfig config;
	protected final ObjectName jmxParentObjectName;
	protected final RecentExceptionSet recentExceptions = new RecentExceptionSet(3);
	protected final ReentrantLock outputLock = new ReentrantLock();
	protected SocketChannel pingerSocketChannel;
	protected final PollerAssistant assistant;
	protected final Map<QueueKey, Integer> requests = new LinkedHashMap<QueueKey, Integer>();
	protected volatile long lastReceivedTime = 0;
	protected final Runnable pinger = new Runnable() {
		public void run() {
			final long t = lastReceivedTime; 
			if(t + config.getPingInterval() < System.currentTimeMillis()) {
				outputLock.lock();
				try {
					if(output != null) {
						if (log.isDebugEnabled())
							log.debug("Sending requests to " + clientId + " because no data has been received since " + new Date(t));
						for(int attempt = 0; attempt < 5; attempt++) {
							try {
								for(Map.Entry<QueueKey, Integer> entry : requests.entrySet()) {
									sendRequest(entry.getKey(), entry.getValue(), false);
								}
								return;
							} catch(SocketTimeoutException e) {
								log.warn("Timed out while sending request during ping to " + clientId + "; closing socket", e);
								SocketChannel sc = pingerSocketChannel;
								if(sc != null) {
									try {
										sc.close();
									} catch(IOException e1) {
										log.warn("Could not close socket channel", e1);
									}
									pingerSocketChannel = null;
								}
								return;
							} catch(IOException e) {
								SocketChannel sc = pingerSocketChannel;
								if(sc != null && !sc.isConnected()) {
									log.warn("Error while sending request during ping to " + clientId + "; closing socket", e);
									try {
										sc.close();
									} catch(IOException e1) {
										log.warn("Could not close socket channel", e1);
									}
									pingerSocketChannel = null;
									return;
								}
								log.warn("Error while sending request during ping to " + clientId, e);
							}
						}

						log.warn("Could not send ping request to " + clientId + "; closing socket");
						SocketChannel sc = pingerSocketChannel;
						if(sc != null) {
							try {
								sc.close();
							} catch(IOException e1) {
								log.warn("Could not close socket channel", e1);
							}
							pingerSocketChannel = null;
						}
					}
				} finally {
					outputLock.unlock();
				}
			}
		}
	};
	protected ByteOutput output;
	protected String clientId;
			
	public DirectPoller(PollerAssistant assistant, String instanceId, String label, DirectPollerConfig config, ObjectName jmxParentObjectName) {
		super(instanceId, (label == null ? "" : label + '-') + config.getUrl());
		this.assistant = assistant;
		this.config = config;
		this.jmxParentObjectName = jmxParentObjectName;
	}
		
	public void updateConcurrency(String queueName, String subscription, Number concurrency) {
		updateConcurrency(null, queueName, subscription, concurrency);
	}
	public void updateConcurrency(String dataSourceName, String queueName, String subscription, Number concurrency) {
		QueueKey queueKey = (dataSourceName == null ? new QueueKey(queueName, subscription) : new HybridQueueKey(dataSourceName, queueName, subscription));
		int c = (concurrency == null ? 0 : concurrency.intValue());
		outputLock.lock();
		try {
			Integer old = requests.get(queueKey);
			if(c > 0) {
				if(old == null || old != c) {
					requests.put(queueKey, c);
					if(output != null)				
						sendRequest(queueKey, c, true);
					try {
						unpause();
					} catch(InterruptedException e) {
						//
					}
				}
			} else if(old != null && old > 0) {
				requests.remove(queueKey);
				if(output != null)				
					sendRequest(queueKey, c, true);
				if(requests.isEmpty())
					try {
						pause();
					} catch(InterruptedException e) {
						//
					}
			}
		} catch(IOException e) {
			log.warn("Could not update concurrency with producer " + config.getUrl() + " for queue '" + queueName + "'", e);
		} finally {
			outputLock.unlock();
		}
	}
	
	@Override
	protected long getDelay(ExitReason exitReason) {
		switch(exitReason) {
			case CONNECTION_FAILED:
				return config.getReconnectInterval();
			case COULD_NOT_LOCK:
				return config.getReconnectInterval();
			case EXCEPTION:
				return 0;
			case STOPPED:
				return 0;
			default:
				return 0;
		}
	}

	protected void connectChannel(SocketChannel socketChannel) throws IOException {
		socketChannel.configureBlocking(false);
		long to = config.getConnectTimeout();
		final long expire;
		if(to > 0)
			expire = System.currentTimeMillis() + to;
		else
			expire = 0;
		if(socketChannel.connect(config.getSocketAddress()))
			return;
		Selector connectSelector = socketChannel.provider().openSelector();
		try {
			SelectionKey connectSelectionKey = socketChannel.register(connectSelector, SelectionKey.OP_CONNECT);
			for(;;) {
				if(!socketChannel.isOpen())
					throw new ClosedChannelException();
				int ns = connectSelector.select(to);
				if(ns > 0 && connectSelectionKey.isConnectable() && socketChannel.finishConnect())
					return;
				connectSelector.selectedKeys().remove(connectSelectionKey);
				if(expire > 0) {
					long now = System.currentTimeMillis();
					if(now >= expire)
						throw new SocketTimeoutException();
					to = expire - now;
				}
			}
		} finally {
			try {
				connectSelector.close();
			} catch(IOException e) {
			}
		}
	}
	protected ExitReason poll() {		
		ByteChannel channel;
		final SocketChannel socketChannel;
		try {
			channel = socketChannel = SocketChannel.open();
		} catch(IOException e) {		
			if(recentExceptions.add(e)) {
				log.warn("Could not get socket for '" + config.getUrl() + "'", e);
			}
			return ExitReason.CONNECTION_FAILED;
		}
		try {
			try {
				connectChannel(socketChannel);
			} catch(IOException e) {
				if(recentExceptions.add(e)) {
					log.info("Could not connect to '" + config.getUrl() + "' for direct polling: " + e.getMessage());
				}
				return ExitReason.CONNECTION_FAILED;
			} catch(ClosedSelectorException e) {
				if(recentExceptions.add(e)) {
					log.info("Could not connect to '" + config.getUrl() + "' for direct polling: " + e.getMessage());
				}
				return ExitReason.CONNECTION_FAILED;
			} catch(RuntimeException e) {
				if(recentExceptions.add(e)) {
					log.info("Could not connect to '" + config.getUrl() + "' for direct polling: " + e.getMessage());
				}
				return ExitReason.CONNECTION_FAILED;
			}
			final Socket socket = socketChannel.socket();
			clientId = socket.getRemoteSocketAddress().toString() + " from port " + socket.getLocalPort();
			try {
				socket.setKeepAlive(true);
			} catch(SocketException e) {
				log.warn("Failed to set keep alive on socket " + clientId, e);
			}
			if(config.getSocketReadTimeout() > 0 || config.getSocketWriteTimeout() > 0)
				try {
					channel = new TimedByteChannel(socketChannel, config.getSocketReadTimeout(), config.getSocketWriteTimeout());
				} catch(IOException e) {
					log.warn("Failed to create Timed Read Byte Channel for " + clientId, e);
					return ExitReason.EXCEPTION;
				}
			if(jmxParentObjectName != null) {
				/* if(log.isTraceEnabled())
				channel = new CopyingJMXByteChannel(channel, jmxParentObjectName.toString(), socketChannel.socket().getLocalSocketAddress().toString());
				else */
				channel = new TrackingJMXByteChannel(channel, jmxParentObjectName.toString(), socketChannel.socket().getLocalSocketAddress().toString());
			}

			final long pingInterval = config.getPingInterval();
			final ByteChannelByteInput input = new ByteChannelByteInput(channel, 1024);
			try {
				outputLock.lockInterruptibly();
				try {
					output = new ByteChannelByteOutput(channel, 1024);
					for(Map.Entry<QueueKey, Integer> entry : requests.entrySet()) {
						sendRequest(entry.getKey(), entry.getValue(), true);
					}
					if(pingInterval > 0)
						this.pingerSocketChannel = socketChannel;
				} finally {
					outputLock.unlock();
				}
				final ScheduledFuture<?> pingerTask;
				if(pingInterval > 0)
					pingerTask = assistant.getScheduler().scheduleAtFixedRate(pinger, pingInterval, pingInterval, TimeUnit.MILLISECONDS);
				else
					pingerTask = null;
				try {
					// while not shutdown, read from input and transform to message
					while(!stopped.get()) {
						DirectPeerMethodData data;
						try {
							data = DirectPeerUtils.readMethodData(input);
						} catch(java.net.SocketTimeoutException e) {
							if(socket.isConnected()) {
								if(log.isDebugEnabled())
									log.trace("Read timed out on connection " + clientId);
								continue;
							} else {
								log.info("Detected closed connection after read timed out on connection " + clientId + ":" + e.getMessage());
								return ExitReason.CONNECTION_FAILED;
							}
						}
						lastReceivedTime = System.currentTimeMillis();
						handleData(data);
					}
				} finally {
					if(pingerTask != null) {
						pingerTask.cancel(false);
						if(pingerTask instanceof RunnableScheduledFuture<?>)
							assistant.getScheduler().remove((RunnableScheduledFuture<?>) pingerTask);
					}
				}
			} catch(InvalidByteValueException e) {
				log.warn("While reading from " + clientId, e);
				return ExitReason.EXCEPTION;
			} catch(EOFException e) {
				log.debug("Connection closed while reading from " + clientId);
				return ExitReason.CONNECTION_FAILED;
			} catch(IOException e) {
				log.info("While reading from " + clientId + ":" + e.getMessage());
				return ExitReason.CONNECTION_FAILED;
			} catch(IllegalArgumentException e) {
				log.warn("While reading from " + clientId, e);
				return ExitReason.EXCEPTION;
			} catch(InstantiationException e) {
				log.warn("While reading from " + clientId, e);
				return ExitReason.EXCEPTION;
			} catch(IllegalAccessException e) {
				log.warn("While reading from " + clientId, e);
				return ExitReason.EXCEPTION;
			} catch(InvocationTargetException e) {
				log.warn("While reading from " + clientId, e);
				return ExitReason.EXCEPTION;
			} catch(InterruptedException e) {
				return ExitReason.STOPPED;
			} catch(RuntimeException e) {
				log.warn("While reading from " + clientId, e);
				return ExitReason.EXCEPTION;
			} finally {
				outputLock.lock();
				try {
					output = null;
				} finally {
					outputLock.unlock();
				}
			}
		} finally {
			try {
				channel.close();
			} catch(IOException e) {
				// ignore
			}
		}
		return ExitReason.STOPPED;
	}
	
	public void sendAcknowledge(boolean success, ConsumedPeerMessage message) {
		outputLock.lock();
		try {
			if(output != null) {
				if(success || !message.isRetryable()) {
					DirectPeerMethodData_ACK dataACK = new DirectPeerMethodData_ACK();
					dataACK.setMessageId(message.getMessageId());
					dataACK.writeData(output);
				} else {
					DirectPeerMethodData_NAK dataNAK = new DirectPeerMethodData_NAK();
					dataNAK.setMessageId(message.getMessageId());
					if(message.getErrorMessage() != null) {
						dataNAK.setErrorMessage(IOUtils.readFully(message.getErrorMessage().getReader()));
					}
					dataNAK.writeData(output);
				}
			}
		} catch(IOException e) {
			log.warn("Could not acknowledge message '" + message.getMessageId() + "' from producer " + config.getUrl() + " for queue '" + message.getQueueName() + "'", e);
		} finally {
			outputLock.unlock();
		}
	}
	
	protected void sendRequest(QueueKey queueKey, int concurrency, boolean initial) throws IOException {
		DirectPeerMethodData_REQUEST data;
		if(assistant.getQueueSettings(queueKey.getQueueName()).isLegacyRequestSent())
			data = new DirectPeerMethodData_REQUEST(DirectPeerMethod.REQUEST);
		else
			data = new DirectPeerMethodData_REQUEST();
		data.setInstanceId(instanceId);
		data.setQueueName(queueKey.getQueueName());
		data.setSubscription(queueKey.getSubscription());
		data.setPriority(config.getPriority());
		data.setConcurrency(concurrency);
		data.writeData(output);
		String msg = "Requested " + concurrency + " direct messages for queue '" + queueKey.getQueueName() + "' " + (queueKey.getSubscription() == null ? "" : "subscription '" + queueKey.getSubscription() + "' ") + "from " + clientId;
		if(initial)
			log.info(msg);
		else
			log.debug(msg);
	}
	
	/**
	 * @throws IOException  
	 */
	protected ConsumedPeerMessage readDirectMessage(DirectPeerMethodData_SEND data) {
		ConsumedPeerMessage message = new ConsumedPeerMessage(assistant.getInstanceId(), assistant.getQueueSettings(data.getQueueName()).getCorrelationControl()) {
			@Override
			public boolean isDbMessage() {
				return false;
			}
			@Override
			public boolean markInprocess() {
				setReadOnly();
				return true;
			}		
			@Override
			public void markComplete(ConnectionGroup connGroup) throws SQLException, DataLayerException {
				// do nothing
			}
			@Override
			public void acknowledge(boolean success) {
				super.acknowledge(success);
				sendAcknowledge(success, this);
			}
			public boolean isRedeliveryTracked() {
				return false;
			}
			@Override
			public boolean isHybrid() {
				return false;
			}
		}; 
		data.updateMessage(message);
		message.setSubscription(data.getSubscription());
		message.setDataSourceName(config.getUrl().toString());
		message.setRedelivered(data.getRetryCount() > 0);
		message.setTemporary(true);
		if(log.isDebugEnabled())
			log.debug("Read new direct message ''{0}'' for ''{1}'' from ''{2}''", message.getMessageId(), message.getQueueName(), clientId);
		return message;
	}
	
	protected void handleData(DirectPeerMethodData data) throws InterruptedException, IOException {
		switch(data.getMethod()) {
			case SEND:
			case SEND_LONG:
			case SEND2:
			case SEND2_LONG:
			case SEND3:
				addMessage(readDirectMessage((DirectPeerMethodData_SEND)data));
				break;
			case INQUIRE:
				DirectPeerMethodData_INQUIRE dataINQUIRE = (DirectPeerMethodData_INQUIRE)data;
				QueueKey queueKey = new QueueKey(dataINQUIRE.getQueueName(), dataINQUIRE.getSubscription());
				Integer concurrency = requests.get(queueKey);
				if(concurrency == null || concurrency <= 0)
					log.info("Publisher {0} with concurrency {1} inquired about queue {2}{3} but there are no consumers for this queue", clientId, concurrency, dataINQUIRE.getQueueName(), dataINQUIRE.getSubscription() == null ? "" : " subscription" + dataINQUIRE.getSubscription());
				else {
					outputLock.lock();
					try {
						if(output != null)
							sendRequest(queueKey, concurrency, false);
						else
							log.info("Publisher inquired about queue '" + dataINQUIRE.getQueueName() + "' " + (dataINQUIRE.getSubscription() == null ? "" : "subscription '" + dataINQUIRE.getSubscription() + "' but no output stream is yet set"));
					} finally {
						outputLock.unlock();
					}
				}
				break;
			default:
				throw new IOException("Invalid method " + data.getMethod() + " received");
		}
	}
	
	protected void addMessage(ConsumedPeerMessage message) throws IOException, InterruptedException {
		if(message.getBlocks() != null && message.getBlocks().length > 0) {
			// TODO: check that blocks are cleared
			log.warn("Message " + message.getMessageId() + " is blocked by " + Arrays.toString(message.getBlocks()) + " but blocking is not implemented for Direct Distros; not adding message");
			message.receive();
			return;
		}
		QueueBuffer<ConsumedPeerMessage> queueBuffer = assistant.getQueueBuffer(message.getQueueName(), message.getSubscription());
		if(log.isDebugEnabled())
			log.debug("Adding message " + message.getMessageId() + " to QueueBuffer for " + message.getQueueName() + "; count=" + queueBuffer.size());
		message.receive();
		queueBuffer.put(message, false);
	}

	public PollerAssistant getAssistant() {
		return assistant;
	}

	public float getMaxPendingPercent() {
		return config.getMaxPendingPercent();
	}

	public String toString() {
		return MessageFormat.format("{0}(instanceId={1}, clientId={2}, lastReceivedTime={3}, requests={4})",
				getClass().getSimpleName(),
				instanceId,
				clientId,
				new Date(lastReceivedTime),
				requests);
	}
}