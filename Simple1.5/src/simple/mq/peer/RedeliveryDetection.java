package simple.mq.peer;

public enum RedeliveryDetection {
	NONE, MILD, STRICT, GATED
}
