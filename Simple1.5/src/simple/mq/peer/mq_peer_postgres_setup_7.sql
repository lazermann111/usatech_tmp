CREATE OR REPLACE FUNCTION MQ.ACQUIRE_QUEUE_LOCK(
    pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
    pn_queue_id MQ.QUEUE.QUEUE_ID%TYPE,
    pn_clear_before BIGINT,
    pn_priority SMALLINT)
    RETURNS BOOLEAN
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    lr_rec RECORD;
    ln_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE;
    lv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE;   
BEGIN
    UPDATE MQ.QUEUE Q
       SET LOCKED_INSTANCE_ID = pv_instance_id,
           LOCKED_TS = STATEMENT_TIMESTAMP(),
           LOCKED_PRIORITY = pn_priority
     WHERE Q.QUEUE_ID = pn_queue_id
       AND (Q.LOCKED_INSTANCE_ID IS NULL
            OR Q.LOCKED_INSTANCE_ID = pv_instance_id
            OR Q.LOCKED_PRIORITY < pn_priority
            OR NOT EXISTS(SELECT 1
                FROM MQ.INSTANCE I
               WHERE I.INSTANCE_ID = Q.LOCKED_INSTANCE_ID
                 AND I.LOCK_EXPIRATION_TS BETWEEN STATEMENT_TIMESTAMP() AND STATEMENT_TIMESTAMP() + INTERVAL'5 minutes'))
     RETURNING Q.QUEUE_TABLE INTO lv_queue_table;
               
    IF FOUND THEN
        IF pn_clear_before > 0 THEN
            EXECUTE 'DELETE FROM MQ.' || lv_queue_table
                 || ' WHERE POSTED_TIME < $1'
                USING pn_clear_before;
        END IF; 
        EXECUTE 'UPDATE MQ.' || lv_queue_table || ' SET REDELIVERED_FLAG = TRUE'
             || ' WHERE MESSAGE_ID IN('
             || 'SELECT MESSAGE_ID'
             || ' FROM MQ.I' || lv_queue_table|| ')';
        EXECUTE 'TRUNCATE TABLE MQ.I' || lv_queue_table;
        FOR lr_rec IN 
            SELECT PRIORITY, LAST_AVAILABLE_TIME
              FROM MQ.QUEUE_SHADOW_DELETION
             WHERE QUEUE_ID = pn_queue_id LOOP
            EXECUTE 'SELECT POSTED_TIME, MESSAGE_ID FROM MQ.' || lv_queue_table || ' WHERE AVAILABLE_TIME <= $2 AND PRIORITY = $1 ORDER BY POSTED_TIME ASC, MESSAGE_ID ASC LIMIT 1'
               INTO ln_last_posted_time, lv_last_message_id
              USING lr_rec.PRIORITY, lr_rec.LAST_AVAILABLE_TIME;
            IF ln_last_posted_time IS NOT NULL THEN
                UPDATE MQ.QUEUE_SHADOW_DELETION
                   SET LAST_POSTED_TIME = ln_last_posted_time,
                       LAST_MESSAGE_ID = lv_last_message_id
                 WHERE QUEUE_ID = pn_queue_id
                   AND PRIORITY = lr_rec.PRIORITY;
            END IF;
        END LOOP;
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.RENEW_INSTANCE_LOCK(
    pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
    ln_time_ms BIGINT)
    RETURNS TIMESTAMP WITH TIME ZONE
    SECURITY DEFINER
AS $$
DECLARE
    ld_expiration_ts MQ.INSTANCE.LOCK_EXPIRATION_TS%TYPE;
BEGIN
    UPDATE MQ.INSTANCE
       SET LOCK_EXPIRATION_TS = CASE 
               WHEN LOCK_EXPIRATION_TS > STATEMENT_TIMESTAMP() + (5 * ln_time_ms || ' millisecond')::interval THEN STATEMENT_TIMESTAMP() + (ln_time_ms || ' millisecond')::interval /* The unreasonable */
               WHEN LOCK_EXPIRATION_TS < STATEMENT_TIMESTAMP() + (ln_time_ms || ' millisecond')::interval THEN STATEMENT_TIMESTAMP() + (ln_time_ms || ' millisecond')::interval /* The old */
               ELSE LOCK_EXPIRATION_TS END
     WHERE INSTANCE_ID = pv_instance_id
    RETURNING LOCK_EXPIRATION_TS INTO ld_expiration_ts;
    IF NOT FOUND THEN
        BEGIN
            INSERT INTO MQ.INSTANCE(INSTANCE_ID, LOCK_EXPIRATION_TS)
               VALUES(pv_instance_id, STATEMENT_TIMESTAMP() + (ln_time_ms || ' millisecond')::interval)
            RETURNING LOCK_EXPIRATION_TS INTO ld_expiration_ts;
        EXCEPTION
            WHEN unique_violation THEN
                RETURN MQ.RENEW_INSTANCE_LOCK(pv_instance_id, ln_time_ms);
        END;
    END IF;
    RETURN ld_expiration_ts;
END;
$$ LANGUAGE plpgsql;
