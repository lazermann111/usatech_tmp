CREATE OR REPLACE FUNCTION MQ.ACQUIRE_QUEUE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	pn_queue_id MQ.QUEUE.QUEUE_ID%TYPE,
	pn_clear_before BIGINT,
	pn_priority SMALLINT,
	pb_keep_inprocess BOOLEAN DEFAULT FALSE)
    RETURNS BOOLEAN
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	lr_rec RECORD;
	ln_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE;
	lv_last_message_id MQ.QUEUE_SHADOW_DELETION.LAST_MESSAGE_ID%TYPE;	
BEGIN
	UPDATE MQ.QUEUE Q
	   SET LOCKED_INSTANCE_ID = pv_instance_id,
	       LOCKED_TS = STATEMENT_TIMESTAMP(),
	       LOCKED_PRIORITY = pn_priority
	 WHERE Q.QUEUE_ID = pn_queue_id
	   AND (Q.LOCKED_INSTANCE_ID IS NULL
	   		OR Q.LOCKED_INSTANCE_ID = pv_instance_id
	   		OR Q.LOCKED_PRIORITY < pn_priority
	   		OR NOT EXISTS(SELECT 1
	   			FROM MQ.INSTANCE I
	   		   WHERE I.INSTANCE_ID = Q.LOCKED_INSTANCE_ID
	   		     AND I.LOCK_EXPIRATION_TS BETWEEN STATEMENT_TIMESTAMP() AND STATEMENT_TIMESTAMP() + INTERVAL'5 minutes'))
	 RETURNING Q.QUEUE_TABLE INTO lv_queue_table;
	   		   
	IF FOUND THEN
		IF pn_clear_before > 0 THEN
			EXECUTE 'DELETE FROM MQ.' || lv_queue_table
				 || ' WHERE POSTED_TIME < $1'
				USING pn_clear_before;
		END IF;	
		EXECUTE 'UPDATE MQ.' || lv_queue_table || ' SET REDELIVERED_FLAG = TRUE'
			 || ' WHERE MESSAGE_ID IN('
			 || 'SELECT MESSAGE_ID'
			 || ' FROM MQ.I' || lv_queue_table|| ')';
		IF pb_keep_inprocess THEN
		    EXECUTE 'DELETE FROM MQ.I' || lv_queue_table || ' WHERE CONNECTION_ID NOT IN(SELECT INSTANCE_ID FROM MQ.INSTANCE)';
		ELSE
		    EXECUTE 'TRUNCATE TABLE MQ.I' || lv_queue_table;
		END IF;
		
		FOR lr_rec IN 
			SELECT PRIORITY, LAST_AVAILABLE_TIME
			  FROM MQ.QUEUE_SHADOW_DELETION
		     WHERE QUEUE_ID = pn_queue_id LOOP
			EXECUTE 'SELECT POSTED_TIME, MESSAGE_ID FROM MQ.' || lv_queue_table || ' WHERE AVAILABLE_TIME <= $2 AND PRIORITY = $1 ORDER BY POSTED_TIME ASC, MESSAGE_ID ASC LIMIT 1'
			   INTO ln_last_posted_time, lv_last_message_id
			  USING lr_rec.PRIORITY, lr_rec.LAST_AVAILABLE_TIME;
			IF ln_last_posted_time IS NOT NULL THEN
				UPDATE MQ.QUEUE_SHADOW_DELETION
				   SET LAST_POSTED_TIME = ln_last_posted_time,
				       LAST_MESSAGE_ID = lv_last_message_id
		         WHERE QUEUE_ID = pn_queue_id
		           AND PRIORITY = lr_rec.PRIORITY;
			END IF;
		END LOOP;
		RETURN TRUE;
	ELSE
		RETURN FALSE;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.REMOVE_EXPIRED(
	pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
	pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
	pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
	pa_exclude_message_ids VARCHAR(100)[])
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
	ln_count INTEGER;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
	BEGIN
		EXECUTE 'DELETE FROM MQ.' || lv_queue_table 
			 || ' WHERE EXPIRE_TIME != 0 AND EXPIRE_TIME <= $1'
			 || ' AND MESSAGE_ID NOT IN(SELECT MESSAGE_ID FROM MQ.I' || lv_queue_table || ')'
			 || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
		  USING pn_current_time, pa_exclude_message_ids;
		GET DIAGNOSTICS ln_count = ROW_COUNT;
	EXCEPTION
		WHEN undefined_table THEN
			ln_count := 0;
	END;
	RETURN ln_count;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_MESSAGES(
    pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pn_max_rows INTEGER,
    pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pa_exclude_message_ids VARCHAR(100)[],
    pa_exclude_correlations BIGINT[])
    RETURNS TABLE(
        MESSAGE_ID MQ.Q_.MESSAGE_ID%TYPE, 
        CONTENT_HEX TEXT,
        POSTED_TIME MQ.Q_.POSTED_TIME%TYPE,
        AVAILABLE_TIME MQ.Q_.AVAILABLE_TIME%TYPE,
        EXPIRE_TIME MQ.Q_.EXPIRE_TIME%TYPE,
        PRIORITY MQ.Q_.PRIORITY%TYPE,
        TEMPORARY_FLAG MQ.Q_.TEMPORARY_FLAG%TYPE,
        REDELIVERED_FLAG MQ.Q_.REDELIVERED_FLAG%TYPE,
        RETRY_COUNT MQ.Q_.RETRY_COUNT%TYPE,
        SHADOW_DATA_SOURCE_NAME MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
        CORRELATION MQ.Q_.CORRELATION%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    lv_locked_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE;
BEGIN
    SELECT LOCKED_INSTANCE_ID, QUEUE_TABLE
      INTO lv_locked_instance_id, lv_queue_table
      FROM MQ.QUEUE
     WHERE QUEUE_NAME = pv_queue_name
       AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
           AND DATA_SOURCE_NAME IS NULL;
    IF lv_locked_instance_id IS NULL OR lv_locked_instance_id != pv_instance_id THEN
        RAISE EXCEPTION 'Instance ''%'' does not currently hold the lock for ''%''', pv_instance_id, pv_queue_name USING ERRCODE = '55U06', HINT = 'Instance ''' || lv_locked_instance_id || ''' holds the lock';
    END IF;
    RETURN QUERY EXECUTE 'SELECT MESSAGE_ID, ENCODE(CONTENT, ''HEX''), POSTED_TIME, AVAILABLE_TIME, EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, REDELIVERED_FLAG, RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, CORRELATION'
        || ' FROM MQ.' || lv_queue_table || ' Q'
        || ' WHERE AVAILABLE_TIME <= $1'
        || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
        || ' AND ($4 IS NULL OR CORRELATION IS NULL OR CORRELATION != ALL($4))'
        || ' AND (EXPIRE_TIME = 0 OR EXPIRE_TIME > $1)'
        || ' AND MESSAGE_ID NOT IN(SELECT MESSAGE_ID FROM MQ.I' || lv_queue_table || ')'
        || ' AND (BLOCKS IS NULL OR NOT EXISTS(SELECT 1 FROM MQ.BLOCK B WHERE B.DATA_SOURCE_NAME IS NULL AND B.BLOCK_KEY = ANY(Q.BLOCKS) AND (B.EXPIRE_TIME = 0 OR B.EXPIRE_TIME > $1)))'
        || ' ORDER BY PRIORITY DESC, POSTED_TIME, MESSAGE_ID LIMIT $3'
        USING pn_current_time, pa_exclude_message_ids, pn_max_rows, pa_exclude_correlations;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.CLEANUP_EXPIRED_LOCKS(
	ln_time_ms BIGINT)
    RETURNS INTEGER
	SECURITY DEFINER
AS $$
DECLARE
	ln_count INTEGER := 0;
	lv_instance_id MQ.INSTANCE.INSTANCE_ID%TYPE;
BEGIN
	FOR lv_instance_id IN
		DELETE 
		  FROM MQ.INSTANCE
		 WHERE LOCK_EXPIRATION_TS < STATEMENT_TIMESTAMP() - (ln_time_ms || ' millisecond')::interval
		 RETURNING INSTANCE_ID
	LOOP
	    DELETE FROM MQ.IQ_
	     WHERE CONNECTION_ID = lv_instance_id;
		ln_count := ln_count + 1;
	END LOOP;
	RETURN ln_count;	 
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION MQ.ACQUIRE_QUEUE_LOCK(
    pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
    pn_queue_id MQ.QUEUE.QUEUE_ID%TYPE,
    pn_clear_before BIGINT,
    pn_priority SMALLINT);
    
GRANT EXECUTE ON FUNCTION MQ.ACQUIRE_QUEUE_LOCK(
	pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
	pn_queue_id MQ.QUEUE.QUEUE_ID%TYPE,
	pn_clear_before BIGINT,
	pn_priority SMALLINT,
    pb_keep_inprocess BOOLEAN) TO use_mq;

	