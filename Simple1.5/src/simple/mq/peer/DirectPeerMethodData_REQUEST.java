package simple.mq.peer;

import java.io.IOException;

import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.results.DatasetUtils;

public class DirectPeerMethodData_REQUEST extends DirectPeerMethodData {
	public static final byte MIN_PRIORITY = 1;
	public static final byte MAX_PRIORITY = 100;	
	protected String instanceId;
	protected String queueName;
	protected String subscription;
	protected byte priority = 5;
	protected int concurrency;
	protected int version = 1;
	
	public static void checkPriority(byte priority) throws IllegalArgumentException {
		if(priority < MIN_PRIORITY || priority > MAX_PRIORITY)
			throw new IllegalArgumentException("Priority must be between " + MIN_PRIORITY + " and " + MAX_PRIORITY + ", not " + priority);
	}
	
	public DirectPeerMethodData_REQUEST() {
		super(DirectPeerMethod.REQUEST3);
	}

	public DirectPeerMethodData_REQUEST(DirectPeerMethod method) {
		super(method);
	}
	
	/**
	 * @param input
	 * @throws IOException
	 */
	protected void readQueueQualifiers(ByteInput input) throws IOException {
	}

	public void readData(ByteInput input) throws IOException {
		int version;
		switch(getMethod()) {
			case REQUEST3:
			case REQUESTDB3:
				version = input.readInt();
				break;
			default:
				version = 0;
		}
		setVersion(version);
		setInstanceId(DatasetUtils.readSmallString(input));
		readQueueQualifiers(input);
		setQueueName(DatasetUtils.readSmallString(input));
		setSubscription(DatasetUtils.readSmallString(input));
		setPriority(input.readByte());
		setConcurrency(input.readInt());		
	}
	@Override
	public void writeData(ByteOutput output) throws IOException {
		DirectPeerUtils.writeMethod(output, getMethod());
		switch(getMethod()) {
			case REQUEST3:
			case REQUESTDB3:
				output.writeInt(getVersion());
				break;
		}
		DatasetUtils.writeSmallString(output, instanceId);
		writeQueueQualifiers(output);
		DatasetUtils.writeSmallString(output, queueName);
		DatasetUtils.writeSmallString(output, subscription);
		output.writeByte(priority);
		output.writeInt(concurrency);
		output.flush();
	}

	/**
	 * @param output
	 * @throws IOException
	 */
	protected void writeQueueQualifiers(ByteOutput output) throws IOException {
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMethod()).append(" [ConnectionId=").append(instanceId).append("; QueueName=").append(queueName);
		if(subscription != null)
			sb.append("; Subscription=").append(subscription);
		sb.append("; Priority=").append(priority).append("; Concurrency=").append(concurrency).append("]");
		return sb.toString();
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	public byte getPriority() {
		return priority;
	}

	public void setPriority(byte priority) {
		checkPriority(priority);
		this.priority = priority;
	}

	public int getConcurrency() {
		return concurrency;
	}

	public void setConcurrency(int concurrency) {
		this.concurrency = concurrency;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String connectionId) {
		this.instanceId = connectionId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
