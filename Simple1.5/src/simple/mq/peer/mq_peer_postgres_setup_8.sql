ALTER TABLE MQ.DLQ DROP CONSTRAINT dlq_pkey;

ALTER TABLE MQ.DLQ ADD PRIMARY KEY(QUEUE_TABLE, MESSAGE_ID);

CREATE INDEX IX_DLQ_MESSAGE_ID ON MQ.DLQ(MESSAGE_ID);

CREATE TABLE MQ.BLOCK(
   BLOCK_KEY VARCHAR(200) NOT NULL,
   DATA_SOURCE_NAME VARCHAR(100),
   EXPIRE_TIME BIGINT NOT NULL,
   CREATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CREATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   UPDATED_UTC_TS TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UPDATED_BY VARCHAR(30) NOT NULL DEFAULT SESSION_USER,
   CONSTRAINT PK_BLOCK PRIMARY KEY(BLOCK_KEY)
) 
WITH (
  OIDS = FALSE
);

ALTER TABLE MQ.Q_ ADD BLOCKS VARCHAR(200)[];

CREATE OR REPLACE FUNCTION MQ.ADD_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE,
    pa_blocks VARCHAR[])
    RETURNS VOID
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
    BEGIN
        EXECUTE 'INSERT INTO MQ.' || lv_queue_table || '(MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME,'
            || ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME, CORRELATION, BLOCKS)'
            || ' VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)'
            USING pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, 
                  pb_temporary_flag, pv_shadow_data_source_name, pn_correlation, pa_blocks;
    EXCEPTION
        WHEN undefined_table THEN
            PERFORM MQ.REGISTER_QUEUE(pv_data_source_name, pv_queue_name, pv_subscription_name);
            PERFORM MQ.ADD_MESSAGE(pv_data_source_name, pv_queue_name, pv_subscription_name, pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, pb_temporary_flag, pv_shadow_data_source_name, pn_correlation, pa_blocks); 
    END;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.ADD_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE,
    pa_blocks VARCHAR[]) TO use_mq;
    
CREATE OR REPLACE FUNCTION MQ.BROADCAST_MESSAGE(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE,
    pa_blocks VARCHAR[])
    RETURNS VARCHAR[]
    SECURITY DEFINER
AS $$
DECLARE
    la_subscriptions VARCHAR[];
BEGIN
    SELECT ARRAY_AGG(DISTINCT SUBSCRIPTION_NAME ORDER BY SUBSCRIPTION_NAME)
      INTO la_subscriptions
      FROM MQ.QUEUE 
     WHERE QUEUE_NAME = pv_queue_name 
       AND DATA_SOURCE_NAME IS NULL
       AND SUBSCRIPTION_NAME IS NOT NULL;
    IF la_subscriptions IS NOT NULL AND ARRAY_LENGTH(la_subscriptions, 1) > 0 THEN
        FOR i IN ARRAY_LOWER(la_subscriptions, 1)..ARRAY_UPPER(la_subscriptions, 1) LOOP
            PERFORM MQ.ADD_MESSAGE(NULL, pv_queue_name, la_subscriptions[i], pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, pb_temporary_flag, pv_shadow_data_source_name, pn_correlation, pa_blocks);     
        END LOOP;
    END IF;
    RETURN la_subscriptions;
END;
$$ LANGUAGE plpgsql;
    
GRANT EXECUTE ON FUNCTION MQ.BROADCAST_MESSAGE(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE,
    pa_blocks VARCHAR[]) TO use_mq;
    
CREATE OR REPLACE FUNCTION MQ.ADD_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE)
    RETURNS VOID
    SECURITY DEFINER
AS $$
BEGIN
	PERFORM MQ.ADD_MESSAGE(
	    pv_data_source_name,
	    pv_queue_name,
	    pv_subscription_name,
	    pv_message_id,
	    pb_content,
	    pn_posted_time,
	    pn_available_time,
	    pn_expire_time,
	    pn_priority,
	    pb_temporary_flag,
	    pv_shadow_data_source_name,
	    pn_correlation,
	    NULL::VARCHAR[]);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.BROADCAST_MESSAGE(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE)
    RETURNS VARCHAR[]
    SECURITY DEFINER
AS $$
BEGIN
	RETURN MQ.BROADCAST_MESSAGE(
	    pv_queue_name,
	    pv_message_id,
	    pb_content,
	    pn_posted_time,
	    pn_available_time,
	    pn_expire_time,
	    pn_priority,
	    pb_temporary_flag,
	    pv_shadow_data_source_name,
	    pn_correlation,
	    NULL::VARCHAR[]);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_MESSAGES(
    pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pn_max_rows INTEGER,
    pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pa_exclude_message_ids VARCHAR(100)[],
    pa_exclude_correlations BIGINT[])
    RETURNS TABLE(
        MESSAGE_ID MQ.Q_.MESSAGE_ID%TYPE, 
        CONTENT_HEX TEXT,
        POSTED_TIME MQ.Q_.POSTED_TIME%TYPE,
        AVAILABLE_TIME MQ.Q_.AVAILABLE_TIME%TYPE,
        EXPIRE_TIME MQ.Q_.EXPIRE_TIME%TYPE,
        PRIORITY MQ.Q_.PRIORITY%TYPE,
        TEMPORARY_FLAG MQ.Q_.TEMPORARY_FLAG%TYPE,
        REDELIVERED_FLAG MQ.Q_.REDELIVERED_FLAG%TYPE,
        RETRY_COUNT MQ.Q_.RETRY_COUNT%TYPE,
        SHADOW_DATA_SOURCE_NAME MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
        CORRELATION MQ.Q_.CORRELATION%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    lv_locked_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE;
BEGIN
    SELECT LOCKED_INSTANCE_ID, QUEUE_TABLE
      INTO lv_locked_instance_id, lv_queue_table
      FROM MQ.QUEUE
     WHERE QUEUE_NAME = pv_queue_name
       AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
           AND DATA_SOURCE_NAME IS NULL;
    IF lv_locked_instance_id IS NULL OR lv_locked_instance_id != pv_instance_id THEN
        RAISE EXCEPTION 'Instance ''%'' does not currently hold the lock for ''%''', pv_instance_id, pv_queue_name USING ERRCODE = '55U06', HINT = 'Instance ''' || lv_locked_instance_id || ''' holds the lock';
    END IF;
    RETURN QUERY EXECUTE 'SELECT MESSAGE_ID, ENCODE(CONTENT, ''HEX''), POSTED_TIME, AVAILABLE_TIME, EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, REDELIVERED_FLAG, RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, CORRELATION'
        || ' FROM MQ.' || lv_queue_table || ' Q'
        || ' WHERE AVAILABLE_TIME <= $1'
        || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
        || ' AND ($4 IS NULL OR CORRELATION IS NULL OR CORRELATION != ALL($4))'
        || ' AND (EXPIRE_TIME = 0 OR EXPIRE_TIME > $1)'
        || ' AND (BLOCKS IS NULL OR NOT EXISTS(SELECT 1 FROM MQ.BLOCK B WHERE B.DATA_SOURCE_NAME IS NULL AND B.BLOCK_KEY = ANY(Q.BLOCKS) AND (B.EXPIRE_TIME = 0 OR B.EXPIRE_TIME > $1)))'
        || ' ORDER BY PRIORITY DESC, POSTED_TIME, MESSAGE_ID LIMIT $3'
        USING pn_current_time, pa_exclude_message_ids, pn_max_rows, pa_exclude_correlations;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.DELETE_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
    pb_remove_inprocess BOOLEAN,
    pa_unblocks VARCHAR[])
    RETURNS VOID
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    ln_count INTEGER;
BEGIN
	IF pa_unblocks IS NOT NULL AND array_length(pa_unblocks, 1) > 0 THEN
	    DELETE FROM MQ.BLOCK WHERE BLOCK_KEY = ANY(pa_unblocks);
	END IF;
    lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
    IF pb_remove_inprocess AND pv_data_source_name IS NULL THEN
        EXECUTE 'DELETE FROM MQ.I' || lv_queue_table || ' WHERE MESSAGE_ID = $1 AND CONNECTION_ID = $2'
          USING pv_message_id, pv_connection_id;
        GET DIAGNOSTICS ln_count = ROW_COUNT;
        IF ln_count < 1 THEN
            RAISE EXCEPTION 'Inprocess Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U02', HINT = 'Another poller processed or is processing this message, or queue was cleared';
        END IF;
    END IF;
    BEGIN
        EXECUTE 'DELETE FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
          USING pv_message_id;
        GET DIAGNOSTICS ln_count = ROW_COUNT;
        IF ln_count < 1 THEN
            RAISE EXCEPTION 'Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U01', HINT = 'Another poller processed or is processing this message, or queue was cleared';
        END IF;
    EXCEPTION
        WHEN undefined_table THEN
            -- do nothing
    END;    
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.DELETE_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
    pb_remove_inprocess BOOLEAN,
    pa_unblocks VARCHAR[]) TO use_mq;
    
CREATE OR REPLACE FUNCTION MQ.DELETE_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
    pb_remove_inprocess BOOLEAN)
    RETURNS VOID
    SECURITY DEFINER
AS $$
BEGIN
	PERFORM MQ.DELETE_MESSAGE(
	    pv_data_source_name,
	    pv_queue_name,
	    pv_subscription_name,
	    pv_message_id,
	    pv_connection_id,
	    pb_remove_inprocess,
	    NULL::VARCHAR[]);   
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.GET_SUBSCRIPTIONS(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE)
    RETURNS TABLE(
        SUBSCRIPTION_NAME MQ.QUEUE.SUBSCRIPTION_NAME%TYPE)
    SECURITY DEFINER
AS $$
BEGIN
    RETURN QUERY SELECT DISTINCT Q.SUBSCRIPTION_NAME FROM MQ.QUEUE Q WHERE Q.QUEUE_NAME = pv_queue_name AND Q.SUBSCRIPTION_NAME IS NOT NULL;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.GET_SUBSCRIPTIONS(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE) TO use_mq;
    
CREATE OR REPLACE FUNCTION MQ.GET_BLOCK_COUNT(
    pa_blocks VARCHAR[])
    RETURNS BIGINT
    SECURITY DEFINER
AS $$
DECLARE
    ln_count BIGINT;
BEGIN
	SELECT COUNT(*)
	  INTO ln_count
	  FROM MQ.BLOCK
	 WHERE BLOCK_KEY = ANY(pa_blocks)
	   AND DATA_SOURCE_NAME IS NULL;
    RETURN ln_count;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.GET_BLOCK_COUNT(
    pa_blocks VARCHAR[]) TO use_mq;
    
CREATE OR REPLACE FUNCTION MQ.GET_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE)
    RETURNS VARCHAR[]
    SECURITY DEFINER
AS $$
DECLARE
    la_blocks VARCHAR[];
BEGIN
    SELECT ARRAY_AGG(BLOCK_KEY)
      INTO la_blocks
      FROM MQ.BLOCK
     WHERE DATA_SOURCE_NAME IS NOT DISTINCT FROM pv_data_source_name;
    RETURN la_blocks;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.GET_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE) TO use_mq;

CREATE OR REPLACE FUNCTION MQ.ADD_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pa_blocks VARCHAR[])
    RETURNS VOID
    SECURITY DEFINER
AS $$
BEGIN
    IF pv_data_source_name IS NULL THEN
        UPDATE MQ.BLOCK
           SET DATA_SOURCE_NAME = NULL
         WHERE DATA_SOURCE_NAME IS NOT NULL
           AND BLOCK_KEY = ANY(pa_blocks);     
    END IF;
	INSERT INTO MQ.BLOCK(BLOCK_KEY, EXPIRE_TIME)
        SELECT b, 0 
          FROM unnest(pa_blocks) b
         WHERE NOT EXISTS(SELECT 1 FROM MQ.BLOCK WHERE BLOCK_KEY = b);
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.ADD_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pa_blocks VARCHAR[]) TO use_mq;
    
CREATE OR REPLACE FUNCTION MQ.REMOVE_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pa_blocks VARCHAR[])
    RETURNS BIGINT
    SECURITY DEFINER
AS $$
DECLARE
    ln_count BIGINT;
BEGIN
    DELETE FROM MQ.BLOCK WHERE BLOCK_KEY = ANY(pa_blocks) AND DATA_SOURCE_NAME IS NOT DISTINCT FROM pv_data_source_name;
    GET DIAGNOSTICS ln_count = ROW_COUNT;
    RETURN ln_count;   
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.REMOVE_BLOCKS(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pa_blocks VARCHAR[]) TO use_mq;
    
CREATE OR REPLACE FUNCTION MQ.RESURRECT_MESSAGE(
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE)
    RETURNS VOID
    SECURITY DEFINER
AS $$
DECLARE
    lr_message MQ.Q_%ROWTYPE;
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
    FOR lr_message IN 
        SELECT QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, 0 AVAILABLE_TIME, EXPIRE_TIME, 
               PRIORITY, TEMPORARY_FLAG, true REDELIVERED_FLAG, 0 RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, ERROR_MESSAGE, CORRELATION
          FROM MQ.DLQ 
         WHERE MESSAGE_ID = pv_message_id LOOP

        EXECUTE 'INSERT INTO MQ.' || lr_message.queue_table || ' SELECT (X).* FROM (SELECT $1 AS X) SS'
          USING lr_message;       
    END LOOP;
    DELETE FROM MQ.DLQ WHERE MESSAGE_ID = lr_message.MESSAGE_ID;
END;
$$ LANGUAGE plpgsql;
