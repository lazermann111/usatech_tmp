package simple.mq.peer;

import java.io.IOException;

import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.results.DatasetUtils;

public class DirectPeerMethodData_SENDDB extends DirectPeerMethodData_SEND {
	protected String dataSourceName;
	protected String shadowDataSourceName;
	
	public DirectPeerMethodData_SENDDB(DirectPeerMethod method) {
		super(method);
	}
	public DirectPeerMethodData_SENDDB() {
		super(DirectPeerMethod.SENDDB3);
	}
	
	@Override
	protected void readQueueQualifiers(ByteInput input) throws IOException {
		dataSourceName = DatasetUtils.readSmallString(input);
		shadowDataSourceName = DatasetUtils.readSmallString(input);
		super.readQueueQualifiers(input);
	}
	@Override
	protected void writeQueueQualifiers(ByteOutput output) throws IOException {
		DatasetUtils.writeSmallString(output, getDataSourceName());
		DatasetUtils.writeSmallString(output, getShadowDataSourceName());
		super.writeQueueQualifiers(output);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMethod()).append(" [DataSourceName=").append(getDataSourceName()).append("; ShadowDataSourceName=").append(getShadowDataSourceName())
			.append("; QueueName=").append(getQueueName());
		if(getSubscription() != null)
			sb.append("; Subscription=").append(getSubscription());
		sb.append("; MessageId=").append(getMessageId())
			.append("; Posted=").append(getPosted()).append("; Expiration=").append(getExpiration()).append("; Priority=")
			.append(getPriority());
		if(getCorrelation() != null)
			sb.append("; Correlation=").append(getCorrelation());
		sb.append("; Content=<").append(getContent().getLength()).append(" bytes>; RetryCount=")
			.append(getRetryCount()).append("]");
		return sb.toString();
	}
	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public String getShadowDataSourceName() {
		return shadowDataSourceName;
	}

	public void setShadowDataSourceName(String shadowDataSourceName) {
		this.shadowDataSourceName = shadowDataSourceName;
	}
	
	public void updateData(PublishedPeerMessage message) {
		super.updateData(message);
		setDataSourceName(message.getDataSourceName());
		setShadowDataSourceName(message.getShadowDataSourceName());
	}
	
	public void updateMessage(StandardPeerMessage message) {
		super.updateMessage(message);
		message.setDataSourceName(getDataSourceName());
		message.setShadowDataSourceName(getShadowDataSourceName());
	}

	protected DirectPeerMethod updateMethod() {
		DirectPeerMethod method;
		if(isLegacy() && (blocks == null || blocks.length == 0)) {
			if(content != null && content.getLength() > DatasetUtils.MAX_MEDIUM_LENGTH) {
				if(correlation == null)
					method = DirectPeerMethod.SENDDB_LONG;
				else
					method = DirectPeerMethod.SENDDB2_LONG;
			} else {
				if(correlation == null)
					method = DirectPeerMethod.SENDDB;
				else
					method = DirectPeerMethod.SENDDB2;
			}
		} else
			method = DirectPeerMethod.SENDDB3;
		this.method = method;
		return method;
	}
}
