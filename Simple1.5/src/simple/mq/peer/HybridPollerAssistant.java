/**
 * 
 */
package simple.mq.peer;



public interface HybridPollerAssistant extends PollerAssistant {	
	public DbPollerAssistant getDbPollerAssistant() ;
	public HybridDbPoller getParentPoller(HybridQueueKey queueKey) ;
	public void registerParentPoller(HybridQueueKey queueKey, HybridDbPoller parentPoller) ;
	public void deregisterParentPoller(HybridQueueKey queueKey, HybridDbPoller parentPoller) ;
}