/**
 * 
 */
package simple.mq.peer;

import simple.mq.Message;

public interface PeerMessage extends Message {
	public String getSubscription() ;
	public String getDataSourceName() ;
	public String getShadowDataSourceName() ;
	public boolean isRedeliveryTracked() ;
}