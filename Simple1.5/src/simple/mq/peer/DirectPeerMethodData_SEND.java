package simple.mq.peer;

import java.io.IOException;

import simple.io.BinaryStream;
import simple.io.ByteArrayBinaryStream;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.mq.StandardMessage;
import simple.results.DatasetUtils;

public class DirectPeerMethodData_SEND extends DirectPeerMethodData {
	private static final byte LONG_CONTENT_BIT = (byte) 0x80;
	private static final byte CORRELATION_BIT = (byte) 0x40;
	private static final byte BLOCKS_BIT = (byte) 0x20;

	protected String queueName;
	protected String subscription;
	protected String messageId;
	protected long posted;	
	protected long expiration;			
	protected byte priority;
	protected BinaryStream content;
	protected int retryCount;			
	protected Long correlation;
	protected String[] blocks;
	protected boolean legacy;

	public DirectPeerMethodData_SEND(DirectPeerMethod method) {
		super(method);
	}
	
	public DirectPeerMethodData_SEND() {
		super(DirectPeerMethod.SEND3);
	}
	
	public void readData(ByteInput input) throws IOException {
		byte bitmap; // 0 = use long content; 1 = has correlation; 2 = has blocks
		switch(getMethod()) {
			case SEND_LONG:
			case SENDDB_LONG:
				bitmap = LONG_CONTENT_BIT;
				break;
			case SEND2_LONG:
			case SENDDB2_LONG:
				if(correlation != null)
					bitmap = LONG_CONTENT_BIT | CORRELATION_BIT;
				else
					bitmap = LONG_CONTENT_BIT;
				break;
			case SEND:
			case SENDDB:
				bitmap = 0;
				break;
			case SEND2:
			case SENDDB2:
				if(correlation != null)
					bitmap = CORRELATION_BIT;
				else
					bitmap = 0;
				break;
			case SEND3:
			case SENDDB3:
				bitmap = input.readByte();
				break;
			default:
				bitmap = 0;
		}
		readQueueQualifiers(input);
		queueName = DatasetUtils.readSmallString(input);
		subscription = DatasetUtils.readSmallString(input);
		messageId = DatasetUtils.readSmallString(input);
		posted = input.readLong();	
		expiration = input.readLong();			
		priority = input.readByte();
		if((LONG_CONTENT_BIT & bitmap) == LONG_CONTENT_BIT)
			content = new ByteArrayBinaryStream(DatasetUtils.readLargeByteArray(input));
		else
			content = new ByteArrayBinaryStream(DatasetUtils.readMediumByteArray(input));
		retryCount = input.readUnsignedShort();
		if((CORRELATION_BIT & bitmap) == CORRELATION_BIT)
			correlation = input.readLong();
		else
			correlation = null;
		if((BLOCKS_BIT & bitmap) == BLOCKS_BIT) {
			int len = input.readUnsignedByte();
			if(len > 0) {
				blocks = new String[len];
				for(int i = 0; i < blocks.length; i++)
					blocks[i] = DatasetUtils.readSmallString(input);
			} else
				blocks = null;
		} else
			blocks = null;			
	}

	/**
	 * @param input
	 * @throws IOException
	 */
	protected void readQueueQualifiers(ByteInput input) throws IOException {
	}

	@Override
	public void writeData(ByteOutput output) throws IOException {
		DirectPeerMethod method = updateMethod();
		DirectPeerUtils.writeMethod(output, method);
		byte bitmap; // 0 = use long content; 1 = has correlation; 2 = has blocks
		switch(method) {
			case SEND_LONG:
			case SENDDB_LONG:
				bitmap = LONG_CONTENT_BIT;
				break;
			case SEND2_LONG:
			case SENDDB2_LONG:
				if(correlation != null)
					bitmap = LONG_CONTENT_BIT | CORRELATION_BIT;
				else
					bitmap = LONG_CONTENT_BIT;
				break;
			case SEND:
			case SENDDB:
				bitmap = 0;
				break;
			case SEND2:
			case SENDDB2:
				if(correlation != null)
					bitmap = CORRELATION_BIT;
				else
					bitmap = 0;
				break;
			case SEND3:
			case SENDDB3:
				bitmap = 0;
				if(content != null && content.getLength() > DatasetUtils.MAX_MEDIUM_LENGTH)
					bitmap |= LONG_CONTENT_BIT;
				if(correlation != null)
					bitmap |= CORRELATION_BIT;
				if(blocks != null && blocks.length > 0)
					bitmap |= BLOCKS_BIT;
				output.writeByte(bitmap);
				break;
			default:
				bitmap = 0;
		}
		writeQueueQualifiers(output);
		DatasetUtils.writeSmallString(output, getQueueName());
		DatasetUtils.writeSmallString(output, getSubscription());
		DatasetUtils.writeSmallString(output, getMessageId());
		output.writeLong(getPosted());	
		output.writeLong(getExpiration());			
		output.writeByte(getPriority());
		if((LONG_CONTENT_BIT & bitmap) == LONG_CONTENT_BIT)
			DatasetUtils.writeLargeByteArray(output, getContent());
		else
			DatasetUtils.writeMediumByteArray(output, getContent());
		output.writeShort(getRetryCount());	
		if((CORRELATION_BIT & bitmap) == CORRELATION_BIT)
			output.writeLong(getCorrelation());
		if((BLOCKS_BIT & bitmap) == BLOCKS_BIT) {
			output.writeByte(blocks.length);
			for(String b : blocks)
				DatasetUtils.writeSmallString(output, b);
		}
		output.flush();
	}

	/**
	 * @param output
	 * @throws IOException
	 */
	protected void writeQueueQualifiers(ByteOutput output) throws IOException {
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMethod()).append(" [QueueName=").append(getQueueName());
		if(getSubscription() != null)
			sb.append("; Subscription=").append(getSubscription());
		sb.append("; MessageId=").append(getMessageId())
			.append("; Posted=").append(getPosted()).append("; Expiration=").append(getExpiration()).append("; Priority=")
			.append(getPriority());
		if(getCorrelation() != null)
			sb.append("; Correlation=").append(getCorrelation());
		sb.append("; Content=<").append(getContent().getLength()).append(" bytes>; RetryCount=")
			.append(getRetryCount()).append("]");
		return sb.toString();
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public long getPosted() {
		return posted;
	}

	public void setPosted(long posted) {
		this.posted = posted;
	}

	public long getExpiration() {
		return expiration;
	}

	public void setExpiration(long expiration) {
		this.expiration = expiration;
	}

	public byte getPriority() {
		return priority;
	}

	public void setPriority(byte priority) {
		this.priority = priority;
	}

	public BinaryStream getContent() {
		return content;
	}

	public void setContent(BinaryStream content) {
		this.content = content;
	}

	protected DirectPeerMethod updateMethod() {
		DirectPeerMethod method;
		if(isLegacy() && (blocks == null || blocks.length == 0)) {
			if(content != null && content.getLength() > DatasetUtils.MAX_MEDIUM_LENGTH) {
				if(correlation == null)
					method = DirectPeerMethod.SEND_LONG;
				else
					method = DirectPeerMethod.SEND2_LONG;
			} else {
				if(correlation == null)
					method = DirectPeerMethod.SEND;
				else
					method = DirectPeerMethod.SEND2;
			}
		} else
			method = DirectPeerMethod.SEND3;
		this.method = method;
		return method;
	}
	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}
	
	public void updateData(PublishedPeerMessage message) {
		setQueueName(message.getQueueName());
		setMessageId(message.getMessageId());
		setPosted(message.getPosted());
		setExpiration(message.getExpiration(message.getPosted()));
		setPriority(message.getPriority());
		setContent(message.getContent());
		setRetryCount(message.getRetryCount());
		setCorrelation(message.getCorrelation());
		setBlocks(message.getBlocks());
	}
	
	public void updateMessage(StandardMessage message) {
		message.setQueueName(getQueueName());
		message.setMessageId(getMessageId());
		message.setPosted(getPosted());
		message.setExpiration(getExpiration());
		message.setPriority(getPriority());
		message.setContent(getContent());
		message.setRetryCount(getRetryCount());
		message.setCorrelation(getCorrelation());
		message.setBlocks(getBlocks());
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	public Long getCorrelation() {
		return correlation;
	}

	public void setCorrelation(Long correlation) {
		this.correlation = correlation;
	}

	public String[] getBlocks() {
		return blocks;
	}

	public void setBlocks(String[] blocks) {
		this.blocks = blocks;
	}

	public boolean isLegacy() {
		return legacy;
	}

	public void setLegacy(boolean legacy) {
		this.legacy = legacy;
	}
}
