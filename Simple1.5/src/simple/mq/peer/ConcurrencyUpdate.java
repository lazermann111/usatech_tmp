/**
 * 
 */
package simple.mq.peer;

public enum ConcurrencyUpdate { NO_CHANGE, ACTIVATE, DEACTIVATE, UPDATE }