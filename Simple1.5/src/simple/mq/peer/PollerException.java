package simple.mq.peer;

public class PollerException extends Exception {
	private static final long serialVersionUID = -151106281783L;
	protected final ExitReason reason;
	public PollerException(String message, ExitReason reason) {
		super(message);
		this.reason = reason;
	}

	public PollerException(Throwable cause, ExitReason reason) {
		super(cause);
		this.reason = reason;
	}

	public PollerException(String message, Throwable cause, ExitReason reason) {
		super(message, cause);
		this.reason = reason;
	}

	public ExitReason getReason() {
		return reason;
	}
}
