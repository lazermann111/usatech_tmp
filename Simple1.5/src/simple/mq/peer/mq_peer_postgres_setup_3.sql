CREATE OR REPLACE FUNCTION MQ.RESURRECT_MESSAGE(
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	lr_message MQ.Q_%ROWTYPE;
	lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
	SELECT QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, 0 AVAILABLE_TIME, EXPIRE_TIME, 
		   PRIORITY, TEMPORARY_FLAG, true REDELIVERED_FLAG, 0 RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, ERROR_MESSAGE 
	  INTO STRICT lr_message
	  FROM MQ.DLQ 
	 WHERE MESSAGE_ID = pv_message_id;

	EXECUTE 'INSERT INTO MQ.' || lr_message.queue_table || ' SELECT (X).* FROM (SELECT $1 AS X) SS'
	  USING lr_message;
	  
	DELETE FROM MQ.DLQ WHERE MESSAGE_ID = lr_message.MESSAGE_ID;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.RESURRECT_MESSAGE(
	pv_message_id MQ.Q_.MESSAGE_ID%TYPE) TO use_mq;
