package simple.mq.peer;

import java.io.IOException;

import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.results.DatasetUtils;

public class DirectPeerMethodData_INQUIRE extends DirectPeerMethodData {
	protected String queueName;
	protected String subscription;
	
	public DirectPeerMethodData_INQUIRE() {
		super(DirectPeerMethod.INQUIRE);
	}

	@Override
	public void readData(ByteInput input) throws IOException {
		setQueueName(DatasetUtils.readSmallString(input));
		setSubscription(DatasetUtils.readSmallString(input));
	}

	@Override
	public void writeData(ByteOutput output) throws IOException {
		DirectPeerUtils.writeMethod(output, getMethod());
		DatasetUtils.writeSmallString(output, queueName);
		DatasetUtils.writeSmallString(output, subscription);
		output.flush();
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}
}
