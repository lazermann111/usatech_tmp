ALTER TABLE MQ.Q_ ADD CORRELATION BIGINT;


CREATE OR REPLACE FUNCTION MQ.RETRY_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pv_connection_id MQ.IQ_.CONNECTION_ID%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_retry_count MQ.Q_.RETRY_COUNT%TYPE,
    pv_error_message MQ.Q_.ERROR_MESSAGE%TYPE,
    pb_no_retry BOOLEAN,
    pb_remove_inprocess BOOLEAN)
    RETURNS VOID
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    ln_count INTEGER;
BEGIN
    lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
    IF pb_remove_inprocess AND pv_data_source_name IS NULL THEN
        EXECUTE 'DELETE FROM MQ.I' || lv_queue_table || ' WHERE MESSAGE_ID = $1 AND CONNECTION_ID = $2'
          USING pv_message_id, pv_connection_id;
        GET DIAGNOSTICS ln_count = ROW_COUNT;
        IF ln_count < 1 THEN
            RAISE EXCEPTION 'Message Id ''%'' not found', pv_message_id USING ERRCODE = '02U01', HINT = 'Another poller processed or is processing this message, or queue was cleared';
        END IF;
    END IF;
    IF pb_no_retry THEN
        BEGIN
            EXECUTE 'INSERT INTO MQ.DLQ(QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME,'
                || ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME, RETRY_COUNT, ERROR_MESSAGE, CORRELATION)'
                || ' SELECT QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, $2,'
                || ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME, $3, $4, CORRELATION'
                || ' FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
                USING pv_message_id, pn_available_time, pn_retry_count, pv_error_message;
            EXECUTE 'DELETE FROM MQ.' || lv_queue_table || ' WHERE MESSAGE_ID = $1'
              USING pv_message_id;
        EXCEPTION
            WHEN undefined_table THEN
                -- do nothing
        END;
    ELSE
        BEGIN
            EXECUTE 'UPDATE MQ.' || lv_queue_table || ' SET AVAILABLE_TIME = $2, RETRY_COUNT = $3, ERROR_MESSAGE = $4 WHERE MESSAGE_ID = $1'
              USING pv_message_id, pn_available_time, pn_retry_count, pv_error_message;
        EXCEPTION
            WHEN undefined_table THEN
                -- do nothing
        END;
    END IF;
END;
$$ LANGUAGE plpgsql;

   
CREATE OR REPLACE FUNCTION MQ.ADD_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE)
    RETURNS VOID
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
    lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
    BEGIN
        EXECUTE 'INSERT INTO MQ.' || lv_queue_table || '(MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME,'
            || ' EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, SHADOW_DATA_SOURCE_NAME, CORRELATION)'
            || ' VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)'
            USING pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, 
                  pb_temporary_flag, pv_shadow_data_source_name, pn_correlation;
    EXCEPTION
        WHEN undefined_table THEN
            PERFORM MQ.REGISTER_QUEUE(pv_data_source_name, pv_queue_name, pv_subscription_name);
            PERFORM MQ.ADD_MESSAGE(pv_data_source_name, pv_queue_name, pv_subscription_name, pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, pb_temporary_flag, pv_shadow_data_source_name, pn_correlation); 
    END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.BROADCAST_MESSAGE(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE)
    RETURNS VARCHAR[]
    SECURITY DEFINER
AS $$
DECLARE
    la_subscriptions VARCHAR[];
BEGIN
    SELECT ARRAY_AGG(DISTINCT SUBSCRIPTION_NAME ORDER BY SUBSCRIPTION_NAME)
      INTO la_subscriptions
      FROM MQ.QUEUE 
     WHERE QUEUE_NAME = pv_queue_name 
       AND DATA_SOURCE_NAME IS NULL
       AND SUBSCRIPTION_NAME IS NOT NULL;
    IF la_subscriptions IS NOT NULL AND ARRAY_LENGTH(la_subscriptions, 1) > 0 THEN
        FOR i IN ARRAY_LOWER(la_subscriptions, 1)..ARRAY_UPPER(la_subscriptions, 1) LOOP
            PERFORM MQ.ADD_MESSAGE(NULL, pv_queue_name, la_subscriptions[i], pv_message_id, pb_content, pn_posted_time, pn_available_time, pn_expire_time, pn_priority, pb_temporary_flag, pv_shadow_data_source_name, pn_correlation);     
        END LOOP;
    END IF;
    RETURN la_subscriptions;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION MQ.GET_MESSAGES(
    pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pn_max_rows INTEGER,
    pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pa_exclude_message_ids VARCHAR(100)[],
    pa_exclude_correlations BIGINT[])
    RETURNS TABLE(
        MESSAGE_ID MQ.Q_.MESSAGE_ID%TYPE, 
        CONTENT_HEX TEXT,
        POSTED_TIME MQ.Q_.POSTED_TIME%TYPE,
        AVAILABLE_TIME MQ.Q_.AVAILABLE_TIME%TYPE,
        EXPIRE_TIME MQ.Q_.EXPIRE_TIME%TYPE,
        PRIORITY MQ.Q_.PRIORITY%TYPE,
        TEMPORARY_FLAG MQ.Q_.TEMPORARY_FLAG%TYPE,
        REDELIVERED_FLAG MQ.Q_.REDELIVERED_FLAG%TYPE,
        RETRY_COUNT MQ.Q_.RETRY_COUNT%TYPE,
        SHADOW_DATA_SOURCE_NAME MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
        CORRELATION MQ.Q_.CORRELATION%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    lv_locked_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE;
BEGIN
    SELECT LOCKED_INSTANCE_ID, QUEUE_TABLE
      INTO lv_locked_instance_id, lv_queue_table
      FROM MQ.QUEUE
     WHERE QUEUE_NAME = pv_queue_name
       AND ((SUBSCRIPTION_NAME IS NULL AND pv_subscription_name IS NULL) OR SUBSCRIPTION_NAME = pv_subscription_name)
           AND DATA_SOURCE_NAME IS NULL;
    IF lv_locked_instance_id IS NULL OR lv_locked_instance_id != pv_instance_id THEN
        RAISE EXCEPTION 'Instance ''%'' does not currently hold the lock for ''%''', pv_instance_id, pv_queue_name USING ERRCODE = '55U06', HINT = 'Instance ''' || lv_locked_instance_id || ''' holds the lock';
    END IF;
    RETURN QUERY EXECUTE 'SELECT MESSAGE_ID, ENCODE(CONTENT, ''HEX''), POSTED_TIME, AVAILABLE_TIME, EXPIRE_TIME, PRIORITY, TEMPORARY_FLAG, REDELIVERED_FLAG, RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, CORRELATION'
        || ' FROM MQ.' || lv_queue_table 
        || ' WHERE AVAILABLE_TIME <= $1'
        || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
        || ' AND ($4 IS NULL OR CORRELATION IS NULL OR CORRELATION != ALL($4))'
        || ' AND (EXPIRE_TIME = 0 OR EXPIRE_TIME > $1)'
        || ' ORDER BY PRIORITY DESC, POSTED_TIME, MESSAGE_ID LIMIT $3'
        USING pn_current_time, pa_exclude_message_ids, pn_max_rows, pa_exclude_correlations;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.START_CARRYOVER(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_copy_from_sql OUT TEXT,
    pv_copy_to_sql OUT TEXT)
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    lv_new_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    ln_count INTEGER;
BEGIN
    lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
    lv_new_queue_table := MQ.GET_QUEUE_TABLE(NULL, pv_queue_name, pv_subscription_name);
    EXECUTE 'LOCK MQ.' || lv_queue_table || ' IN EXCLUSIVE MODE';
    EXECUTE 'SELECT COUNT(*) FROM MQ.' || lv_queue_table INTO ln_count;
    IF ln_count = 0 THEN
        pv_copy_from_sql := NULL;
        pv_copy_to_sql := NULL;
    ELSE
        pv_copy_from_sql := 'COPY (SELECT ''' || lv_new_queue_table || ''' QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, AVAILABLE_TIME, EXPIRE_TIME, '
            || 'PRIORITY, TEMPORARY_FLAG, TRUE REDELIVERED_FLAG, RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, ERROR_MESSAGE, CORRELATION FROM MQ.'
            || lv_queue_table || ') TO STDOUT (FORMAT ''binary'')';
        pv_copy_to_sql := 'COPY MQ.' || lv_new_queue_table || ' FROM STDIN (FORMAT ''binary'')';
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.RESURRECT_MESSAGE(
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE)
    RETURNS VOID
    SECURITY DEFINER
AS $$
DECLARE
    lr_message MQ.Q_%ROWTYPE;
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
BEGIN
    SELECT QUEUE_TABLE, MESSAGE_ID, CONTENT, POSTED_TIME, 0 AVAILABLE_TIME, EXPIRE_TIME, 
           PRIORITY, TEMPORARY_FLAG, true REDELIVERED_FLAG, 0 RETRY_COUNT, SHADOW_DATA_SOURCE_NAME, ERROR_MESSAGE, CORRELATION
      INTO STRICT lr_message
      FROM MQ.DLQ 
     WHERE MESSAGE_ID = pv_message_id;

    EXECUTE 'INSERT INTO MQ.' || lr_message.queue_table || ' SELECT (X).* FROM (SELECT $1 AS X) SS'
      USING lr_message;
      
    DELETE FROM MQ.DLQ WHERE MESSAGE_ID = lr_message.MESSAGE_ID;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.ADD_MESSAGE(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE) TO use_mq;
    
GRANT EXECUTE ON FUNCTION MQ.BROADCAST_MESSAGE(
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_message_id MQ.Q_.MESSAGE_ID%TYPE,
    pb_content MQ.Q_.CONTENT%TYPE,
    pn_posted_time MQ.Q_.POSTED_TIME%TYPE,
    pn_available_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pn_expire_time MQ.Q_.EXPIRE_TIME%TYPE,
    pn_priority MQ.Q_.PRIORITY%TYPE,
    pb_temporary_flag MQ.Q_.TEMPORARY_FLAG%TYPE,
    pv_shadow_data_source_name MQ.Q_.SHADOW_DATA_SOURCE_NAME%TYPE,
    pn_correlation MQ.Q_.CORRELATION%TYPE) TO use_mq;

GRANT EXECUTE ON FUNCTION MQ.GET_MESSAGES(
    pv_instance_id MQ.QUEUE.LOCKED_INSTANCE_ID%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pn_max_rows INTEGER,
    pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE,
    pa_exclude_message_ids VARCHAR(100)[],
    pa_exclude_correlations BIGINT[]) TO use_mq;

