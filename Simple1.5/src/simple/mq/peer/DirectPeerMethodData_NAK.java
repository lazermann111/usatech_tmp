package simple.mq.peer;

import java.io.IOException;

import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.results.DatasetUtils;

public class DirectPeerMethodData_NAK extends DirectPeerMethodData {
	protected String messageId;
	protected String errorMessage;
	
	public DirectPeerMethodData_NAK() {
		super(DirectPeerMethod.NAK);
	}
	
	public void readData(ByteInput input) throws IOException {
		messageId = DatasetUtils.readSmallString(input);	
		errorMessage = DatasetUtils.readMediumString(input);
	}
	@Override
	public void writeData(ByteOutput output) throws IOException {
		DirectPeerUtils.writeMethod(output, getMethod());
		DatasetUtils.writeSmallString(output, messageId);
		DatasetUtils.writeMediumString(output, errorMessage);
		output.flush();
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getMethod()).append(" [MessageId=").append(messageId).append("; ErrorMessage=").append(errorMessage).append("]");
		return sb.toString();
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
