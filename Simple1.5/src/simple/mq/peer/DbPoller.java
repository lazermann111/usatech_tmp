/**
 * 
 */
package simple.mq.peer;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call;
import simple.db.ConnectionGroup;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.DataSourceAvailability;
import simple.db.DataSourceNotFoundException;
import simple.db.DbUtils;
import simple.db.ParameterException;
import simple.io.ByteArrayBinaryStream;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.lang.Decision;
import simple.results.BeanException;
import simple.results.Results;
import simple.text.MessageFormat;
import simple.util.FilteredIterator;
import simple.util.RecentExceptionSet;
import simple.util.concurrent.CorrelationControl;
import simple.util.concurrent.QueueBuffer;

public class DbPoller extends AbstractPoller {
	private static final Log log = Log.getLog();
	protected final String dataSourceName;
	protected final String messageDataSourceName;
	protected final QueueKey queueKey;
	protected final boolean primary;
	protected final int priority;
	protected final ConcurrentMap<String, ConsumedPeerMessage> inflightMessages = new ConcurrentHashMap<String, ConsumedPeerMessage>();
	protected final RecentExceptionSet recentExceptions = new RecentExceptionSet(5);
	protected final DataSourceAvailability dataSourceAvailability;
	protected final DataLayer dataLayer;
	protected final DbPollerAssistant assistant;
	protected final AtomicInteger pollSize = new AtomicInteger();
	protected volatile boolean lockAcquired = false;
	protected final Call analyzeCall = new Call();
	protected final Call analyzeShadowCall = new Call();
	
	protected class DbConsumedPeerMessage extends ConsumedPeerMessage {
		protected DbConsumedPeerMessage() {
			super(assistant.getInstanceId(), assistant.getQueueSettings(queueKey.getQueueName()).getCorrelationControl());
		}
		@Override
		public boolean isDbMessage() {
			return true;
		}
		@Override
		public void acknowledge(boolean success) {
			super.acknowledge(success);
			inflightMessages.remove(getMessageId());
		}
		@Override
		public boolean markInprocess() {
			return DbPoller.this.markInprocess(this);
		}
		@Override
		public void markComplete(ConnectionGroup connGroup) throws SQLException, DataLayerException {
			DbPoller.this.markComplete(connGroup, this);
		}
		//XXX: Should we make isRedelivered() true, if isRedeliveryTracked() is false?
		public boolean isRedeliveryTracked() {
			switch(assistant.getQueueSettings(queueName).getRedeliveryDetection()) {
				case NONE: case MILD: return false;
				case STRICT:
				case GATED:
				default:
					return true;
			}
		}
		public boolean isHybrid() {
			return false;
		}
	}
	
	public DbPoller(DbPollerAssistant assistant, String instanceId, String dataSourceName, String queueName, String subscription, boolean primary, int priority, boolean reprocessProtection) {
		super(instanceId, queueName + '-' + dataSourceName);
		this.dataSourceName = dataSourceName;
		this.messageDataSourceName = assistant.toMessageDataSourceName(dataSourceName);
		this.assistant = assistant;
		this.dataSourceAvailability = assistant.getDataSourceAvailability();
		this.dataLayer = assistant.getDataLayer();
		this.primary = primary;
		this.priority = priority;
		this.queueKey = new QueueKey(queueName, subscription);
		StringBuilder sb = new StringBuilder();
		String queueTable = queueKey.getQueueTable(null);
		sb.append("VACUUM ANALYZE VERBOSE MQ.").append(queueTable).append("; VACUUM ANALYZE VERBOSE MQ.I").append(queueTable).append(';');
		this.analyzeCall.setSql(sb.toString());
		this.analyzeCall.setQueryTimeout(120.0);
		sb.setLength(0);
		String shadowQueueTable = queueKey.getQueueTable(messageDataSourceName);
		sb.append("VACUUM ANALYZE VERBOSE MQ.").append(shadowQueueTable).append(';');
		this.analyzeShadowCall.setSql(sb.toString());
		this.analyzeShadowCall.setQueryTimeout(120.0);
	}
	
	protected void markComplete(ConnectionGroup connGroup, ConsumedPeerMessage message) throws SQLException, DataLayerException {
		String dsn = assistant.toConnectionDataSourceName(message.getDataSourceName());

		dataLayer.executeCall(connGroup.getConnection(dsn, false), "DELETE_MESSAGE", message);
		if(!message.isTemporary()) {
			String sdsn = assistant.toConnectionDataSourceName(message.getShadowDataSourceName());
			Connection conn = null;
			try {
				conn = connGroup.getConnection(sdsn, false);
			} catch(SQLException e) {
				log.warn("Could not delete shadow message '" + message.getMessageId() + "' from " + sdsn, e);
				//NOTE: the clean up task will delete this when it can
			} catch(DataLayerException e) {
				log.warn("Could not delete shadow message '" + message.getMessageId() + "' from " + sdsn, e);
				//NOTE: the clean up task will delete this when it can
			}
			if(conn != null)
				try {
					dataLayer.executeCall(conn, "DELETE_SHADOW_MESSAGE", message);
				} catch(SQLException e) {
					DbUtils.rollbackSafely(conn);
					log.warn("Could not delete shadow message '" + message.getMessageId() + "' from " + sdsn, e);
					//NOTE: the clean up task will delete this when it can
				} catch(DataLayerException e) {
					log.warn("Could not delete shadow message '" + message.getMessageId() + "' from " + sdsn, e);
					//NOTE: the clean up task will delete this when it can
				}
		}
		if(log.isInfoEnabled())
			log.info("Marked message '" + message.getMessageId() + "' as completed in '" + dsn + "'");
	}

	public void updateConcurrency(String queueName, String subscription, Number concurrency) {
		int c = (concurrency == null ? 0 : concurrency.intValue());
		int old  = pollSize.getAndSet(c);
		if(old != c)
			handleConcurrencyChange(old, c);
	}
		
	protected void handleConcurrencyChange(int oldConcurrency, int newConcurrency) {
		if(newConcurrency > 0) {
			if(oldConcurrency <= 0) {
				try {
					unpause();
				} catch(InterruptedException e) {
					//
				}
			}			
		} else if(oldConcurrency > 0) {
			try {
				pause();
			} catch(InterruptedException e) {
				//
			}
		}
	}

	protected ExitReason poll() {
		int pollSize = this.pollSize.get();
		if(log.isDebugEnabled())
			log.debug("Polling for " + pollSize + " messages on " + queueKey);
		if(pollSize <= 0)
			return ExitReason.STOPPED;
		QueueBuffer<ConsumedPeerMessage> queueBuffer;
		try {
			queueBuffer = assistant.getQueueBuffer(queueKey.getQueueName(), queueKey.getSubscription());
		} catch(IOException e) {
			log.warn("Could not get QueueBuffer for " + queueKey, e);
			return ExitReason.EXCEPTION;
		}
		final QueueSettings queueSettings = assistant.getQueueSettings(queueKey.getQueueName());
		try {
			Decision<ConsumedPeerMessage> redeliveredDecision = acquireQueue(queueKey);
			lockAcquired();
			lockAcquired = true;
			ScheduledFuture<?> cleanup = assistant.getScheduler().scheduleAtFixedRate(new Runnable() {
				public void run() {
					try {
						cleanup(queueKey, inflightMessages.keySet());
					} catch(PollerException e) {
						// ignore
					}
				}
			}, 0, queueSettings.getCleanupInterval(), TimeUnit.MILLISECONDS);			
			final AtomicInteger analyzeCount = new AtomicInteger(Integer.MAX_VALUE);		
			final int analyzeThreshhold = queueSettings.getAnalyzeAfterCount();
			//TODO: do we need inProcessValidity?
			//LockingValidity validity = new LockingValidity();
			try {
				while(!stopped.get()) {
					int rowsFound = retrieveMessages(queueKey, pollSize, queueBuffer, inflightMessages.keySet(), redeliveredDecision, analyzeCount, analyzeThreshhold);
					if(stopped.get() || (!primary && shouldReleaseLock(queueKey)))
						break;
					if(rowsFound < pollSize) {
						handlePollNotSatisfied(rowsFound, queueSettings.getDataPollFrequency());
					}
					pollSize = this.pollSize.get();
					if(pollSize <= 0)
						return ExitReason.STOPPED;
				}
			} finally {
				lockAcquired = false;
				exitingPollLoop();
				//inProcessValidity.invalidate();
				cleanup.cancel(false);
				if(cleanup instanceof RunnableScheduledFuture<?>)
					assistant.getScheduler().remove((RunnableScheduledFuture<?>)cleanup);
				if(log.isDebugEnabled())
					log.debug("Exiting poll loop on " + queueKey);
			}

			releaseQueue(queueKey);
			return ExitReason.STOPPED;
		} catch(PollerException e) {
			return e.getReason();
		} catch(InterruptedException e) {
			try {
				releaseQueue(queueKey);
			} catch(PollerException e1) {
				//ignore
			} 
			return ExitReason.STOPPED;
		}
	}

	@Override
	protected long getDelay(ExitReason exitReason) {
		switch(exitReason) {
			case CONNECTION_FAILED:
				if(primary)
					return assistant.getPrimaryReconnectInterval();
				else
					return assistant.getSecondaryReconnectInterval();
			case COULD_NOT_LOCK:
				return assistant.getSecondaryReconnectInterval();
			case EXCEPTION:
				return 0;
			case STOPPED:
				return 0;
			default:
				return 0;
		}
	}

	protected Decision<ConsumedPeerMessage> acquireQueue(QueueKey queueKey) throws PollerException {
		final Map<String,Long> lastPostedTimes = new HashMap<String, Long>();
		// lock on this queue and add to list of notifications
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("queueName", queueKey.getQueueName());
		params.put("subscription", queueKey.getSubscription());
		params.put("dataSourceName", messageDataSourceName);
		params.put("instanceId", assistant.getInstanceId());
		switch(assistant.getQueueSettings(queueKey.getQueueName()).getRedeliveryDetection()) {
			case GATED:
				params.put("keepInprocess", true);
		}
		long clearBefore = 0;
		for(Iterator<String> iter = otherDataSourceNameIterator(); iter.hasNext(); ) {
			String dsn = iter.next();
			Connection otherConn;
			try {
				otherConn = dataLayer.getConnection(dsn);
			} catch(SQLException e) {
				if(dataSourceAvailability.isAvailable(dsn)) {
					log.info("Could not get connection for database '" + dsn + "'", e);	
					dataSourceAvailability.setAvailable(dsn, false);
				}
				continue;
			} catch(DataLayerException e) {
				if(dataSourceAvailability.isAvailable(dsn)) {
					log.info("Could not get connection for database '" + dsn + "'", e);	
					dataSourceAvailability.setAvailable(dsn, false);
				}
				continue;
			}
			try {
				Results results;
				try {
					results = dataLayer.executeQuery(otherConn, "GET_CARRYOVER_LAST_POSTED", params);
				} finally {
					DbUtils.closeSafely(otherConn);
				}
				while(results.next()) {
					String sdsn = results.getValue("shadowDataSourceName", String.class);
					long lastPostedTime = results.getValue("lastPostedTime", Long.class);
					Long prev = lastPostedTimes.get(sdsn);
					if(prev == null || prev < lastPostedTime) {
						lastPostedTimes.put(sdsn, lastPostedTime);
					} 
				}
				if(!lastPostedTimes.containsKey(dsn))
					lastPostedTimes.put(dsn, -1L);
			} catch(SQLException e) {
				log.warn("Could not get carryover last posted from '" + dsn + "'", e);
			} catch(DataLayerException e) {
				log.warn("Could not get carryover last posted from '" + dsn + "'", e);		
			} catch(ConvertException e) {
				log.warn("Could not get carryover last posted from '" + dsn + "'", e);
			}				
		}
		final long carryoverSafety;
		if(assistant.getDataSourceNames().size() - 2 > lastPostedTimes.size()) {
			// NOTE: we can't be sure that some of the messages weren't carried over to the two that are now down,
			// so we consider messages with that shadow dsn to be re-delivered.
			carryoverSafety = System.currentTimeMillis();
		} else {
			carryoverSafety = 0L;
		}
		for(Long lpt : lastPostedTimes.values())
			if(lpt > 0 && lpt >= clearBefore)
				clearBefore = lpt + 1;
		params.put("clearBefore", clearBefore);
		params.put("priority", priority);
		Connection pollerConnection = getPollerConnection();
		try {
			try {
				dataLayer.executeCall(pollerConnection, "PREPARE_QUEUE_LOCK", params);
			} catch(SQLException e) {
				if(DbUtils.isClosedSafely(pollerConnection)) {
					log.warn("Connection was closed while preparing to lock queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
					throw new PollerException(e, ExitReason.EXCEPTION);
				} else {
					log.warn("Error occurred while preparing to lock queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
					throw new PollerException(e, ExitReason.EXCEPTION);
				}
			} catch(DataLayerException e) {
				log.warn("Data Layer Exception while preparing to lock queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
				throw new PollerException(e, ExitReason.EXCEPTION);
			}
			try {
				dataLayer.executeCall(pollerConnection, "ACQUIRE_QUEUE_LOCK", params);
				if(!ConvertUtils.getBoolean(params.get("success"))) {
					String message = "Could not lock queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "' with priority " + priority;
					PollerException pe = new PollerException(message, ExitReason.COULD_NOT_LOCK);
					if(primary && queueKey.getQueueName().indexOf('#') < 0) {
						if(log.isWarnEnabled())
							log.warn(message, pe);
					} else if(log.isDebugEnabled())
						log.debug(message);
					throw pe;
				}
			} catch(SQLException e) {
				if(DbUtils.isClosedSafely(pollerConnection)) {
					log.warn("Connection was closed while locking queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
					throw new PollerException(e, ExitReason.EXCEPTION);
				} else {
					log.warn("Error occurred while locking queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
					throw new PollerException(e, ExitReason.EXCEPTION);
				}
			} catch(DataLayerException e) {
				log.warn("Data Layer Exception while locking queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
				throw new PollerException(e, ExitReason.EXCEPTION);
			} catch(ConvertException e) {
				log.warn("Conversion error occurred while locking queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
				throw new PollerException(e, ExitReason.EXCEPTION);
			}
			if(log.isInfoEnabled())
				log.info("Acquired lock on queue '" + queueKey.getQueueName() + "' " + (queueKey.getSubscription() == null ? "" : "subscription '" + queueKey.getSubscription() + "' ") + "in database '" + dataSourceName + "'  with priority " + priority + " and connection " + pollerConnection);
		} finally {
			DbUtils.closeSafely(pollerConnection);
		}
		
		return new Decision<ConsumedPeerMessage>() {
			public boolean decide(ConsumedPeerMessage message) {
				return (message.getPosted() < carryoverSafety && !lastPostedTimes.containsKey(message.getShadowDataSourceName()));
			}							
		};
	}

	protected boolean shouldReleaseLock(QueueKey queueKey) throws PollerException {
		// check if we should release the lock
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("queueName", queueKey.getQueueName());
		params.put("subscription", queueKey.getSubscription());
		params.put("dataSourceName", messageDataSourceName);
		params.put("instanceId", assistant.getInstanceId());
		Connection pollerConnection = getPollerConnection();
		try {
			//dataLayer.executeCall(pollerConnection, "GET_COUNT_OF_WAITERS", params);
			//return ConvertUtils.getInt(params.get("waiters")) > 0;
			dataLayer.executeCall(pollerConnection, "HOLDS_QUEUE_LOCK", params);
			return !ConvertUtils.getBoolean(params.get("result"));
		} catch(SQLException e) {
			log.warn("While getting count of waiters on queue '" + queueKey.getQueueName() + "'", e);
			throw new PollerException(e, ExitReason.EXCEPTION);
		} catch(DataLayerException e) {
			log.warn("While getting count of waiters on queue '" + queueKey.getQueueName() + "'", e);
			throw new PollerException(e, ExitReason.EXCEPTION);
		} catch(ConvertException e) {
			log.warn("While getting count of waiters on queue '" + queueKey.getQueueName() + "'", e);
			throw new PollerException(e, ExitReason.EXCEPTION);
		} finally {
			DbUtils.closeSafely(pollerConnection);
		}
	}
	protected void releaseQueue(QueueKey queueKey) throws PollerException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("queueName", queueKey.getQueueName());
		params.put("subscription", queueKey.getSubscription());
		params.put("dataSourceName", messageDataSourceName);
		params.put("instanceId", assistant.getInstanceId());
		Connection pollerConnection = getPollerConnection();
		try {
			log.info("Releasing lock on queue '" + queueKey.getQueueName() + "' " + (queueKey.getSubscription() == null ? "" : "queueKey.getSubscription() '" + queueKey.getSubscription() + "' ") + "in database '" + dataSourceName + "' with connection " + pollerConnection);						
			dataLayer.executeCall(pollerConnection, "RELEASE_QUEUE_LOCK", params);
		} catch(SQLException e) {
			log.warn("While releasing lock on queue '" + queueKey.getQueueName() + "'", e);
			throw new PollerException(e, ExitReason.EXCEPTION);
		} catch(DataLayerException e) {
			log.warn("While releasing lock on queue '" + queueKey.getQueueName() + "'", e);
			throw new PollerException(e, ExitReason.EXCEPTION);
		} finally {
			DbUtils.closeSafely(pollerConnection);
		}
	}
	
	protected Connection getPollerConnection() throws PollerException {
		Connection pollerConnection;
		try {
			pollerConnection = dataLayer.getConnection(dataSourceName, true);
		} catch(SQLException e) {
			if(recentExceptions.add(e)) {
				if(dataSourceAvailability.isAvailable(dataSourceName))
					log.warn("Could not get connection for '" + dataSourceName + "'", e);
				else
					log.info("Could not get initial connection for '" + dataSourceName + "': " + e.getMessage() + "; will try again later");
			}
			dataSourceAvailability.setAvailable(dataSourceName, false);
			throw new PollerException(e, ExitReason.CONNECTION_FAILED);
		} catch(DataLayerException e) {
			if(recentExceptions.add(e)) {
				if(dataSourceAvailability.isAvailable(dataSourceName))
					log.warn("Could not get connection for '" + dataSourceName + "'", e);
				else
					log.info("Could not get initial connection for '" + dataSourceName + "': " + e.getMessage() + "; will try again later");
			}
			dataSourceAvailability.setAvailable(dataSourceName, false);
			throw new PollerException(e, ExitReason.CONNECTION_FAILED);
		}
		recentExceptions.clear();
		dataSourceAvailability.setAvailable(dataSourceName, true);
		
		return pollerConnection;
	}

	protected ConsumedPeerMessage createMessage() {
		return new DbConsumedPeerMessage();
	}
	
	protected boolean analyzeQueue() throws PollerException {
		boolean success = false;
		Connection conn = getPollerConnection();
		boolean origAutoCommit;
		try {
			origAutoCommit = conn.getAutoCommit();
			conn.setAutoCommit(true);
			try {
				analyzeCall.executeCall(conn, null, null);
				log.info("Analyzed queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'");
				success = true;
			} catch(SQLException e) {
				if(DbUtils.isClosedSafely(conn)) {
					log.warn("Connection was closed while analyzing queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
					throw new PollerException(e, ExitReason.EXCEPTION);
				} else {
					log.warn("Error occurred while analyzing queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
				}
			} catch(DataLayerException e) {
				log.warn("Data Layer Exception while analyzing queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
			} finally {
				conn.setAutoCommit(origAutoCommit);
			}
		} catch(SQLException e) {
			log.warn("SQL Exception while preparing to analyze queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
		} finally {
			DbUtils.closeSafely(conn);
		}
		for(String dsn : dataSourceAvailability.getAvailables()) {
			if(dsn != null && !dsn.equals(dataSourceName)) {
				try {
					conn = dataLayer.getConnection(dsn);
				} catch(SQLException e) {
					log.info("Could not get connection for database '" + dsn + "'", e);	
					dataSourceAvailability.setAvailable(dsn, false);
					continue;
				} catch(DataSourceNotFoundException e) {
					log.info("Could not get connection for database '" + dsn + "'", e);	
					dataSourceAvailability.setAvailable(dsn, false);
					continue;
				}
				try {
					origAutoCommit = conn.getAutoCommit();
					conn.setAutoCommit(true);
					try {
						analyzeShadowCall.executeCall(conn, null, null);
						log.info("Analyzed shadow in " + dsn + " for queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'");
					} catch(SQLException e) {
						log.warn("SQL Exception while analyzing shadow in " + dsn + " for queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
						success = false;
					} catch(ParameterException e) {
						log.warn("Data Layer Exception while analyzing shadow in " + dsn + " for queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
						success = false;
					} finally {
						try {
							conn.setAutoCommit(origAutoCommit);
						} catch(SQLException e1) {
							//ignore
						}
					}
				} catch(SQLException e) {
					log.warn("SQL Exception while analyzing shadow in " + dsn + " for queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
				} finally {
					DbUtils.closeSafely(conn);	
				}				
			}
		}
		return success;
	}

	protected int retrieveMessages(final QueueKey queueKey, int pollSize, final QueueBuffer<ConsumedPeerMessage> queueBuffer, Set<String> inflightMessageIds, final Decision<ConsumedPeerMessage> redeliveredDecision, final AtomicInteger analyzeCount, int analyzeThreshhold) throws PollerException, InterruptedException {
		Map<String,Object> params = new HashMap<String, Object>();	
		params.put("queueName", queueKey.getQueueName());
		params.put("subscription", queueKey.getSubscription());
		params.put("dataSourceName", messageDataSourceName);
		params.put("instanceId", assistant.getInstanceId());
		params.put("availableDataSources", dataSourceAvailability);
		params.put("maxRows", pollSize);
		params.put("excludeMessageIds", inflightMessageIds == null ? null : new CopyOnWriteArraySet<String>(inflightMessageIds));
		params.put("excludeCorrelations", getExcludeCorrelations());
		if(analyzeThreshhold > 0 && analyzeCount.get() >= analyzeThreshhold) {
			analyzeQueue();
			analyzeCount.set(0); //XXX: Should this only be reset on success?
		}
		//register subscription in each db
		if(queueKey.getSubscription() != null) {
			for(String dsn : dataSourceAvailability.getAvailables()) {
				if(dsn != null && !dsn.equals(dataSourceName)) {
					try {
						Connection conn = dataLayer.getConnection(dsn, true);
						try {
							//register subscription in each db
							//TODO: we may need to be more aggressive in doing this for datasources that are down during first attempt
							try {
								dataLayer.executeCall(conn, "REGISTER_QUEUE", params);
							} catch(SQLException e) {
								log.warn("Error while registering subscription '" + queueKey.getSubscription() + "' on queue '" + queueKey.getQueueName() + "' in database '" + dsn, e);
							} catch(DataLayerException e) {
								log.warn("Error while registering subscription '" + queueKey.getSubscription() + "' on queue '" + queueKey.getQueueName() + "' in database '" + dsn, e);
							}
						} finally {
							DbUtils.closeSafely(conn);
						}
					} catch(SQLException e) {
						log.info("Could not get connection for database '" + dsn + "'", e);	
						dataSourceAvailability.setAvailable(dsn, false);												
					} catch(DataSourceNotFoundException e) {
						log.info("Could not get connection for database '" + dsn + "'", e);	
						dataSourceAvailability.setAvailable(dsn, false);												
					}
				}
			}
		}
		final Results results;
		Connection conn = getPollerConnection();
		final long start;
		try {			
			start = System.currentTimeMillis();
			params.put("currentTime", start);
			results = dataLayer.executeQuery(conn, "GET_MESSAGES", params);
		} catch(SQLException e) {
			if("55U06".equals(e.getSQLState())) {
				log.warn("Queue was not locked by this instance in " + dataSourceName + " (priority " + priority + ")", e);
				throw new PollerException(e, ExitReason.COULD_NOT_LOCK);
			} else {
				log.warn("While retrieving messages from queues in " + dataSourceName, e);
				throw new PollerException(e, ExitReason.EXCEPTION);
			}
		} catch(DataLayerException e) {
			log.warn("While retrieving messages from queues in " + dataSourceName, e);
			throw new PollerException(e, ExitReason.EXCEPTION);
		} finally {
			DbUtils.closeSafely(conn);
		}
		int rowsFound;
		if(!results.isGroupEnding(null)) {
			queueBuffer.put(new Iterator<ConsumedPeerMessage>() {
				protected ConsumedPeerMessage next = getNext();

				@Override
				public boolean hasNext() {
					return next != null;
				}

				@Override
				public ConsumedPeerMessage next() {
					if(!hasNext())
						throw new NoSuchElementException("At end");
					ConsumedPeerMessage curr = next;
					next = getNext();
					return curr;
				}

				protected ConsumedPeerMessage getNext() {
					while(results.next()) {
						final ConsumedPeerMessage message;
						try {
							message = createMessage();
							message.setDataSourceName(messageDataSourceName);
							message.setQueueName(queueKey.getQueueName());
							message.setSubscription(queueKey.getSubscription());
							// XXX: for now just store a byte[] copy of content; in the future consider streaming from db
							message.setContent(new ByteArrayBinaryStream(ByteArrayUtils.fromHex(results.getValue("contentHex", String.class))));
							results.fillBean(message);
							if(!message.isTemporary() && message.getShadowDataSourceName() != null && !message.isRedelivered()) {
								if(redeliveredDecision != null && redeliveredDecision.decide(message))
									message.setRedelivered(true);
							}
						} catch(ConvertException e) {
							log.warn("While creating message '" + results.getValue("messageId") + "' for queue '" + queueKey.getQueueName() + "'", e);
							continue;
						} catch(BeanException e) {
							log.warn("While creating message '" + results.getValue("messageId") + "' for queue '" + queueKey.getQueueName() + "'", e);
							continue;
						}
						if(checkMessage(message)) {
							if(log.isDebugEnabled())
								log.debug("Adding message " + message.getMessageId() + " to QueueBuffer for " + message.getQueueName() + "; count=" + queueBuffer.size());
							analyzeCount.incrementAndGet();
							return message;
						}
					}
					return null;
				}

				@Override
				public void remove() {
					throw new UnsupportedOperationException("Not modifiable");
				}
			}, true);
			rowsFound = results.getRow() - 1;
		} else
			rowsFound = 0;
		if(log.isDebugEnabled()) {
			long time = System.currentTimeMillis() - start;
			log.debug("Retrieved " + rowsFound + " messages in " + time + " ms");
		}
		return rowsFound;
	}
	
	protected Set<Long> getExcludeCorrelations() {
		QueueSettings qs = getQueueSettings();
		if(qs == null)
			return null;
		CorrelationControl<Long> cc = qs.getCorrelationControl();
		if(cc == null)
			return null;
		return cc.getLimited();
	}

	protected void lockAcquired() {
		// Do Nothing -- Hook for sub-classes
		
	}

	protected void exitingPollLoop() {
		// Do Nothing -- Hook for sub-classes
	}

	/**
	 * @param rowsFound
	 * @param pollerConnection
	 * @param notificationConnection
	 * @param retrieved 
	 * @return Whether to exit the poll loop
	 */
	protected void handlePollNotSatisfied(int rowsFound, long dataPollFrequency) {
		if(!stopped.get())
			try {
				Thread.sleep(dataPollFrequency);
			} catch(InterruptedException e) {
				//ignore
			}
	}
	
	public void cleanup(QueueKey queueKey, Set<String> inflightMessageIds) throws PollerException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("queueName", queueKey.getQueueName());
		params.put("subscription", queueKey.getSubscription());
		params.put("dataSourceName", messageDataSourceName);
		params.put("currentTime", System.currentTimeMillis());
		params.put("excludeMessageIds", inflightMessageIds == null ? null : new CopyOnWriteArraySet<String>(inflightMessageIds));
		Map<String,List<Map<String,Object>>> shadowDeletions = new HashMap<String, List<Map<String,Object>>>();
		Connection conn = getPollerConnection();
		try {
			try {
				dataLayer.executeCall(conn, "REMOVE_EXPIRED", params);
				int r = ConvertUtils.getInt(params.get("removedCount"));
				if(r > 0) {
					if(log.isInfoEnabled())
						log.info("Removed " + r + " expired messages from queue '" + queueKey.getQueueName() + "' on " + dataSourceName);
				} else if(log.isDebugEnabled())
					log.debug("Removed " + r + " expired messages from queue '" + queueKey.getQueueName() + "' on " + dataSourceName);
			} catch(SQLException e) {
				log.warn("Error occurred while removing expired messages in queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
			} catch(DataLayerException e) {
				log.warn("Data Layer Exception while removing expired messages in queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
			} catch(ConvertException e) {
				log.warn("Conversion error occurred while removing expired messages in queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
			}
			//Grab the last shadow message marked for deletion in the queue
			try {
				Results results = dataLayer.executeQuery(conn, "GET_SHADOW_DELETIONS", params);
				while(results.next()) {
					String dsn = results.getValue("shadowDataSourceName", String.class);
					Map<String,Object> shadowDeletionParams = new HashMap<String, Object>(params);
					results.fillBean(shadowDeletionParams);
					List<Map<String,Object>> sdpList = shadowDeletions.get(dsn);
					if(sdpList == null) {
						sdpList = new ArrayList<Map<String,Object>>();
						shadowDeletions.put(dsn, sdpList);
					}
					sdpList.add(shadowDeletionParams);
				}
			} catch(SQLException e) {
				log.warn("Error occurred while finding shadow deletions needed for queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
			} catch(DataLayerException e) {
				log.warn("Data Layer Exception while finding shadow deletions needed for queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
			} catch(ConvertException e) {
				log.warn("Conversion error occurred while finding shadow deletions needed for queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
			} catch(BeanException e) {
				log.warn("Bean error occurred while finding shadow deletions needed for queue '" + queueKey.getQueueName() + "' in database '" + dataSourceName + "'", e);
			}				
		} finally {
			DbUtils.closeSafely(conn);
		}
		for(String dsn : dataSourceAvailability.getAvailables()) {
			if(dsn != null && !dsn.equals(dataSourceName)) {
				try {
					conn = dataLayer.getConnection(dsn, true);
				} catch(DataSourceNotFoundException e) {
					dataSourceAvailability.setAvailable(dsn, false);
					log.warn("Could not get connection to remove expired shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
					return;
				} catch(SQLException e) {
					dataSourceAvailability.setAvailable(dsn, false);
					log.warn("Could not get connection to remove expired shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
					return;
				}
				try {
					try {
						dataLayer.executeCall(conn, "REMOVE_SHADOW_EXPIRED", params);
						int r = ConvertUtils.getInt(params.get("removedCount"));
						if(r > 0) {
							if(log.isInfoEnabled())
								log.info("Removed " + r + " expired shadow messages from queue '" + queueKey.getQueueName() + "' on " + dsn);
						} else if(log.isDebugEnabled())
							log.debug("Removed " + r + " expired shadow messages from queue '" + queueKey.getQueueName() + "' on " + dsn);
					} catch(SQLException e) {
						log.warn("Error occurred while trying to remove expired shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
					} catch(DataLayerException e) {
						log.warn("Data Layer Exception while trying to remove expired shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
					} catch(ConvertException e) {
						log.warn("Conversion error occurred while trying to remove expired shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
					}
					// Remove all messages after the last one in the primary queue
					List<Map<String,Object>> sdpList = shadowDeletions.get(dsn);
					short[] excludePriorities;
					if(sdpList != null) {
						int i = 0;
						excludePriorities = new short[sdpList.size()];
						for(Map<String,Object> shadowDeletionParams : sdpList)
							try {
								excludePriorities[i++] = ConvertUtils.getShort(shadowDeletionParams.get("priority"));
								dataLayer.executeCall(conn, "REMOVE_SHADOW_MESSAGES", shadowDeletionParams);
								int r = ConvertUtils.getInt(shadowDeletionParams.get("removedCount"));
								if(r > 0) {
									if(log.isInfoEnabled())
										log.info("Removed " + r + " un-matched shadow messages from queue '" + queueKey.getQueueName() + "' on " + dsn);
								} else if(log.isDebugEnabled())
									log.debug("Removed " + r + " un-matched shadow messages from queue '" + queueKey.getQueueName() + "' on " + dsn);
							} catch(SQLException e) {
								log.warn("Error occurred while removing un-matched shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
							} catch(DataLayerException e) {
								log.warn("Data Layer Exception while removing un-matched shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
							} catch(ConvertException e) {
								log.warn("Conversion error occurred while removing un-matched shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
							}
					} else
						excludePriorities = new short[0];

					// Remove any shadow messages without corresponding primary queue records
					params.put("excludePriorities", excludePriorities);
					try {
						dataLayer.executeCall(conn, "REMOVE_SHADOW_MESSAGES_MISSING", params);
						int r = ConvertUtils.getInt(params.get("removedCount"));
						if(r > 0) {
							if(log.isInfoEnabled())
								log.info("Removed " + r + " un-matched shadow messages from queue '" + queueKey.getQueueName() + "' on " + dsn);
						} else if(log.isDebugEnabled())
							log.debug("Removed " + r + " un-matched shadow messages from queue '" + queueKey.getQueueName() + "' on " + dsn);
					} catch(SQLException e) {
						log.warn("Error occurred while removing un-matched shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
					} catch(DataLayerException e) {
						log.warn("Data Layer Exception while removing un-matched shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
					} catch(ConvertException e) {
						log.warn("Conversion error occurred while removing un-matched shadow messages in queue '" + queueKey.getQueueName() + "' in database '" + dsn + "'", e);
					}
					//TODO: Check if we need to perform a carry over
				} finally {
					DbUtils.closeSafely(conn);
				}
			}
		}
	}
	
	protected Iterator<String> otherDataSourceNameIterator() {
		return new FilteredIterator<String,String>(assistant.getDataSourceNames().iterator()) {
			@Override
			protected boolean accept(String element) {
				return !ConvertUtils.areEqual(element, dataSourceName);
			}
			@Override
			protected String convert(String element) {
				return element;
			}			
		};
	}

	protected boolean markInprocess(ConsumedPeerMessage message) {
		try {
			String callId;
			switch(assistant.getQueueSettings(message.getQueueName()).getRedeliveryDetection()) {
				case NONE: 
					return true;
				case MILD: 
					callId = "CHECK_EXISTENCE";
					break;
				case STRICT:
				case GATED:
				default:
					callId = "MARK_INPROCESS";
					break;
			}
			boolean okay = false;
			try {
				final Connection conn = dataLayer.getConnection(dataSourceName, true);
				try {
					// We don't need to mark in-process in the shadow b/c we will consider all carryovers to be re-delivered
					dataLayer.executeCall(conn, callId, message);
					okay = true;
					return true;
				} catch(SQLException e) {
					if("02U01".equals(e.getSQLState()))
						log.info("Message '" + message.getMessageId() + "' on queue '" + message.getQueueName() + "' is already processed");
					else
						log.warn("Could not mark message '" + message.getMessageId() + "' on queue '" + message.getQueueName() + "' as in-process", e);
				} catch(DataLayerException e) {
					log.warn("Could not mark message '" + message.getMessageId() + "' on queue '" + message.getQueueName() + "' as in-process", e);
				} finally {
					DbUtils.closeSafely(conn);
				}
			} catch(DataSourceNotFoundException e) {
				log.warn("Could not get connetion to " + dataSourceName + " to mark message '" + message.getMessageId() + "' on queue '" + message.getQueueName() + "' as in-process", e);
			} catch(SQLException e) {
				log.warn("Could not get connetion to " + dataSourceName + " to mark message '" + message.getMessageId() + "' on queue '" + message.getQueueName() + "' as in-process", e);
			} finally {
				if(!okay) {
					message.acknowledge(false); // so we re-select it from db				
				}
			}
			return false;
		} finally {
			message.setReadOnly();
		}
	}

	protected boolean checkMessage(ConsumedPeerMessage message) {
		// Check correlation
		switch(message.getReadiness()) {
			case LAST:
			case NEVER:
				if(log.isInfoEnabled())
					log.info("Message " + message.getMessageId() + " is limited by its correlation " + message.getCorrelation());
				return false;
		}
		if(inflightMessages.putIfAbsent(message.getMessageId(), message) != null) {
			if(log.isDebugEnabled())
				log.debug("Message " + message.getMessageId() + " already received and inflight");
			return false;
		}
		message.receive();
		if(log.isDebugEnabled())
			log.debug("New message " + message.getMessageId() + " received");
		return true;
	}

	public DbPollerAssistant getAssistant() {
		return assistant;
	}

	protected QueueSettings getQueueSettings() {
		return assistant.getQueueSettings(queueKey.getQueueName());
	}

	public String toString() {
		return MessageFormat.format("{0}(instanceId={1}, queueKey={2}, dataSource={3}, messageDataSource={4}, primary={5}, priority={6}, inflight={7})",
				getClass().getSimpleName(),
				instanceId,
				queueKey,
				dataSourceName,
				messageDataSourceName,
				primary,
				priority,
				inflightMessages.keySet());
	}
}