/**
 * 
 */
package simple.mq.peer;

public enum PublishType { DB, DIRECT, DIRECT_OR_DB, HYBRID }