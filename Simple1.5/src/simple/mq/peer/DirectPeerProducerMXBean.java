package simple.mq.peer;

import java.util.List;

public interface DirectPeerProducerMXBean {
	public String getListenAddressString();
	public int getNumThreads();
	public long getReprioritizeInterval();
	public int getLatencyRollingAverageSize();
	public long getMinLatencyDiff();
	public long getInactivityResetTime();
	public int getMaxRetryCount();
	public int getEstimatedConnCount();
	public DataSourcePreferenceType getDataSourcePreferenceType();
	public long getOutputWriteTimeout();
	public long getSocketWriteTimeout();
	public int getBufferSize();
	public int getThreadCount();
	public int getLastThreadId();
	
	public String getConnectionInfo();
	public String getExecutorInfo();
	
	public List<String> getDirectInstancesInfo();
	public List<String> getHybridInstancesInfo();
	
	public List<String> getDirectProducersInfo();
	public List<String> getHybridProducersInfo();
	
	public List<String> getHybridPrioritizerInfo();

	public void resetAllLatencies();
	public void resetInstanceLatencies(String instanceId);
	public void resetQueueLatencies(String queueName);
}