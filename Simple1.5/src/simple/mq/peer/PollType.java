/**
 * 
 */
package simple.mq.peer;

public enum PollType { DB, DIRECT, HYBRID, DB_AND_DIRECT, HYBRID_AND_DIRECT }