CREATE TYPE MQ.QUEUE_SIZE_TYPE AS (
    QUEUE_NAME VARCHAR(100),
    MESSAGE_COUNT BIGINT
);

CREATE OR REPLACE FUNCTION MQ.GET_QUEUE_SIZES(
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE)
    RETURNS SETOF MQ.QUEUE_SIZE_TYPE
	SECURITY DEFINER
	STABLE
AS $$
DECLARE
	ln_count MQ.QUEUE_SIZE_TYPE.MESSAGE_COUNT%TYPE;
	la_array MQ.QUEUE_SIZE_TYPE[] := ARRAY[]::MQ.QUEUE_SIZE_TYPE[];
	lr_rec RECORD;
	i INTEGER := 1;
BEGIN
	FOR lr_rec IN 
		SELECT Q.QUEUE_NAME, Q.SUBSCRIPTION_NAME, Q.QUEUE_TABLE
		  FROM MQ.QUEUE Q
		 WHERE Q.DATA_SOURCE_NAME IS NULL LOOP
		EXECUTE 'SELECT COUNT(*) FROM MQ.' || lr_rec.QUEUE_TABLE 
			 || ' WHERE AVAILABLE_TIME <= $1'
		   INTO ln_count
		  USING pn_current_time;
		la_array[i] := ROW(lr_rec.QUEUE_NAME, ln_count);
		i := i+ 1;
	END LOOP;
	RETURN QUERY SELECT QUEUE_NAME, SUM(MESSAGE_COUNT)::BIGINT FROM UNNEST(la_array) GROUP BY QUEUE_NAME;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.GET_QUEUE_SIZES(
	pn_current_time MQ.Q_.AVAILABLE_TIME%TYPE) TO use_mq;

	
CREATE OR REPLACE FUNCTION MQ.DRAIN_DLQ(
	pn_min_posted_time MQ.DLQ.POSTED_TIME%TYPE,
	pn_max_posted_time MQ.DLQ.POSTED_TIME%TYPE,
	pv_file_path VARCHAR(4000),
	pn_message_count OUT BIGINT)
    SECURITY DEFINER
AS $$
DECLARE
	lv_sql VARCHAR(4000);
BEGIN
	lv_sql :='COPY (SELECT * FROM MQ.DLQ WHERE POSTED_TIME BETWEEN ' || pn_min_posted_time || ' AND ' || pn_max_posted_time || ') TO '
		|| QUOTE_LITERAL(pv_file_path) || ' (FORMAT ''csv'')';
	RAISE NOTICE 'Executing ''%''', lv_sql;
	EXECUTE lv_sql;
        DELETE 
          FROM MQ.DLQ 
         WHERE POSTED_TIME BETWEEN pn_min_posted_time AND pn_max_posted_time;
        GET DIAGNOSTICS pn_message_count = ROW_COUNT;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MQ.DRAIN_DLQ(
	pn_before_time MQ.DLQ.POSTED_TIME%TYPE,
	pv_file_path OUT VARCHAR(4000),
	pn_message_count OUT BIGINT)
    SECURITY DEFINER 
AS $$
DECLARE
	ln_min_posted_time MQ.DLQ.POSTED_TIME%TYPE;
	ln_max_posted_time MQ.DLQ.POSTED_TIME%TYPE;
BEGIN
	SELECT MIN(POSTED_TIME), MAX(POSTED_TIME), COUNT(*)
	  INTO ln_min_posted_time, ln_max_posted_time, pn_message_count
	  FROM MQ.DLQ
	 WHERE pn_before_time IS NULL OR pn_before_time > POSTED_TIME;

	IF pn_message_count > 0 THEN
		SELECT '/home/postgres/dlq_' || current_database() || '_' || to_char('epoch'::timestamp + (ln_min_posted_time/1000 || ' seconds')::interval, 'YYYYMMDD-HHMISS')
			|| '_to_' || to_char('epoch'::timestamp + (ln_max_posted_time/1000 || ' seconds')::interval, 'YYYYMMDD-HHMISS') || '.csv'
		  INTO pv_file_path;
		SELECT a.pn_message_count
		  INTO pn_message_count
		  FROM MQ.DRAIN_DLQ(ln_min_posted_time, ln_max_posted_time, pv_file_path) a;		
	END IF;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.DRAIN_DLQ(
	pn_before_time MQ.DLQ.POSTED_TIME%TYPE,
	pv_file_path OUT VARCHAR(4000),
	pn_message_count OUT BIGINT) TO admin_1;

CREATE OR REPLACE FUNCTION MQ.CREATE_QUEUE(
	pv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE,
	pb_primary BOOLEAN
) 
	RETURNS VOID 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'CREATE TABLE MQ.' || pv_queue_table || '('
		|| 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
		|| 'CONSTRAINT CK_' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
		|| 'PRIMARY KEY(MESSAGE_ID)'
		|| ') INHERITS(MQ.Q_)';
	EXECUTE 'GRANT SELECT, INSERT ON MQ.'|| pv_queue_table || ' TO use_mq';
	EXECUTE 'ALTER TABLE MQ.'|| pv_queue_table || ' OWNER TO use_mq';
	EXECUTE 'CREATE UNIQUE INDEX UX_' || pv_queue_table || '_ORDER ON MQ.' || pv_queue_table || '(PRIORITY DESC, POSTED_TIME, MESSAGE_ID)';
	EXECUTE 'CREATE INDEX IX_' || pv_queue_table || '_AVAILABLE_TIME ON MQ.' || pv_queue_table || '(AVAILABLE_TIME)';
	EXECUTE 'CREATE INDEX IX_' || pv_queue_table || '_EXPIRE_TIME ON MQ.' || pv_queue_table || '(EXPIRE_TIME)';
	IF pb_primary THEN
		EXECUTE 'CREATE TABLE MQ.I' || pv_queue_table || '('
			|| 'QUEUE_TABLE VARCHAR(100) NOT NULL DEFAULT ''' || pv_queue_table || ''','
			|| 'CONSTRAINT CK_I' || pv_queue_table || ' CHECK (QUEUE_TABLE = ''' || pv_queue_table || '''),'
			|| 'PRIMARY KEY(MESSAGE_ID)'
			|| ') INHERITS(MQ.IQ_)';
		EXECUTE 'ALTER TABLE MQ.I'|| pv_queue_table || ' OWNER TO use_mq';	
	END IF;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.DRAIN_DLQ(
	pn_before_time MQ.DLQ.POSTED_TIME%TYPE,
	pv_file_path OUT VARCHAR(4000),
	pn_message_count OUT BIGINT) TO admin_1;

CREATE OR REPLACE FUNCTION MQ.TMP_CONVERT_INDEXES(
	pv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE
) 
	RETURNS VOID 
	SECURITY DEFINER
AS $$
BEGIN
	EXECUTE 'DROP INDEX MQ.UX_' || pv_queue_table || '_ORDER';
	EXECUTE 'CREATE UNIQUE INDEX UX_' || pv_queue_table || '_ORDER ON MQ.' || pv_queue_table || '(PRIORITY DESC, POSTED_TIME, MESSAGE_ID)';
	EXECUTE 'CREATE INDEX IX_' || pv_queue_table || '_AVAILABLE_TIME ON MQ.' || pv_queue_table || '(AVAILABLE_TIME)';
END;
$$ LANGUAGE plpgsql;

SELECT c.relname, MQ.TMP_CONVERT_INDEXES(UPPER(c.relname))
  FROM pg_index x
  JOIN pg_class c ON c.oid = x.indrelid
  JOIN pg_class i ON i.oid = x.indexrelid
  JOIN pg_attribute a ON x.indkey[0] = a.attnum AND c.oid = a.attrelid
  LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
 WHERE n.nspname = 'mq'
   AND i.relname = 'ux_' || c.relname || '_order'
   AND a.attname = 'available_time' 
   AND c.relkind = 'r'::char AND i.relkind = 'i'::char
 ORDER BY c.relname;
 
COMMIT;

DROP FUNCTION MQ.TMP_CONVERT_INDEXES(
	pv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE
);
