package simple.mq.peer;

public class IncompleteException extends Exception {
	private static final long serialVersionUID = -1511062817825189323L;
	public IncompleteException(String message) {
		super(message);
	}

	public IncompleteException(Throwable cause) {
		super(cause);
	}

	public IncompleteException(String message, Throwable cause) {
		super(message, cause);
	}
}
