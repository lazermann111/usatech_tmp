/**
 * 
 */
package simple.mq.peer;

import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.io.IOUtils;

public class DirectPollerConfig {
	protected SocketAddress socketAddress;

	protected byte priority = 5;
	protected float maxPendingPercent = 0.25f;
	protected long reconnectInterval = 15000;
	protected long pingInterval = 1 * 60 * 1000;
	protected int defaultPollerThreads = 1;
	protected final Map<String, Integer> pollerThreads = new HashMap<String, Integer>();
	protected Pattern queueNamePattern;
	protected int connectTimeout = 5000;
	protected int socketReadTimeout = 30000;
	protected int socketWriteTimeout = 5000;

	public String getUrl() {
		return (socketAddress == null ? null : socketAddress.toString());
	}

	public void setUrl(String url) throws UnknownHostException, ConvertException {
		if (url == null || url.trim().length() == 0)
			this.socketAddress = null;
		else this.socketAddress = IOUtils.parseInetSocketAddress(url);
	}

	public byte getPriority() {
		return priority;
	}

	public void setPriority(byte priority) {
		DirectPeerMethodData_REQUEST.checkPriority(priority);
		this.priority = priority;
	}

	public long getReconnectInterval() {
		return reconnectInterval;
	}

	public void setReconnectInterval(long reconnectInterval) {
		this.reconnectInterval = reconnectInterval;
	}

	public int getDefaultPollerThreads() {
		return defaultPollerThreads;
	}

	public void setDefaultPollerThreads(int defaultPollerThreads) {
		this.defaultPollerThreads = defaultPollerThreads;
	}

	public int getPollerThreads(String queueName) {
		Integer n = pollerThreads.get(queueName);
		return (n == null || n < 0 ? getDefaultPollerThreads() : n);
	}

	public void setPollerThreads(String queueName, int numThreads) {
		pollerThreads.put(queueName, numThreads);
	}

	public SocketAddress getSocketAddress() {
		return socketAddress;
	}

	public String getQueueNamePattern() {
		return queueNamePattern == null ? null : queueNamePattern.pattern();
	}

	public void setQueueNamePattern(String queueNamePattern) {
		this.queueNamePattern = (queueNamePattern == null ? null : Pattern.compile(queueNamePattern));
	}

	public boolean isPollingQueueName(String queueName) {
		if (queueNamePattern == null)
			return true;
		return queueNamePattern.matcher(queueName).matches();
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public long getPingInterval() {
		return pingInterval;
	}

	public void setPingInterval(long pingInterval) {
		this.pingInterval = pingInterval;
	}

	public float getMaxPendingPercent() {
		return maxPendingPercent;
	}

	public void setMaxPendingPercent(float maxPendingPercent) {
		this.maxPendingPercent = maxPendingPercent;
	}

	public int getSocketReadTimeout() {
		return socketReadTimeout;
	}

	public void setSocketReadTimeout(int socketTimeout) {
		this.socketReadTimeout = socketTimeout;
	}

	public int getSocketWriteTimeout() {
		return socketWriteTimeout;
	}

	public void setSocketWriteTimeout(int socketWriteTimeout) {
		this.socketWriteTimeout = socketWriteTimeout;
	}
}
