package simple.mq;

import simple.app.RetryPolicy;

public interface Worker {
	public void publish(Message message, boolean broadcast, String... blocks) throws MessagingException;
	public ConsumedMessage consume(String queueName, String subscription) throws MessagingException;
	public void commit() throws MessagingException;
	public void rollback(Throwable reason, RetryPolicy retryPolicy) throws MessagingException;
	public void close() ;
	public boolean cancelConsume(Thread consumingThread) ;
	public void block(String... blocks) throws MessagingException;
}
