package simple.mq;

public interface Supervisor {
	public Worker createWorker() throws MessagingException;
	public void close() ;
	public boolean isSubscriptionActive(String queueName, String subscription) throws MessagingException ;
	public void clearSubscription(String queueName, String subscription) throws MessagingException ;
	public void activateSubscription(String queueName, String subscription) throws MessagingException ;

	public void pause(String queueName, String subscription) throws MessagingException;

	public void unpause(String queueName, String subscription) throws MessagingException;

	public boolean isPaused(String queueName, String subscription) throws MessagingException;

	public int getConsumerCount(String queueName, boolean temporary);

	public String[] getSubscriptions(String queueName) throws MessagingException;
}
