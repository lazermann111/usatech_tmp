package simple.mq;

public interface ConsumedMessage extends Message {
	public void unblock(String... blockKeys);

	public String getSubscription();
}
