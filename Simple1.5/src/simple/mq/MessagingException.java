package simple.mq;

public class MessagingException extends Exception {
	private static final long serialVersionUID = -1511062817825189323L;
	public MessagingException(String message) {
		super(message);
	}

	public MessagingException(Throwable cause) {
		super(cause);
	}

	public MessagingException(String message, Throwable cause) {
		super(message, cause);
	}
}
