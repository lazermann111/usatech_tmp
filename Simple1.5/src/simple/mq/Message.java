package simple.mq;

import simple.io.BinaryStream;

public interface Message {
	public long getExpiration(long posted) ;
	public long getAvailable() ;
	public long getPosted() ;
	public byte getPriority() ;
	public boolean isTemporary() ;
	public boolean isRedelivered() ;
	public int getRetryCount() ;
	public String getMessageId() ;
	public String getQueueName() ;
	public BinaryStream getContent() ;
	public Long getCorrelation() ;
}
