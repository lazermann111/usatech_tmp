package simple.mq.server;

import simple.mq.Message;
import simple.mq.storage.StorageException;

public interface MessageQueue {
	public long getSize() ;
	public String getQueueName() ;
	public String postMessage(Message message) throws StorageException ;
	public Message nextMessageNoWait() throws StorageException ;
	public Message nextMessage(long timeout) throws InterruptedException, StorageException ;
	public void deleteMessage(String messageId) throws StorageException ;
	public boolean removeSubscription(String subscriptionId) throws StorageException ;
}
