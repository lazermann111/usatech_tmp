package simple.mq.server;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.io.Log;
import simple.mq.Message;
import simple.mq.storage.Storage;
import simple.mq.storage.StorageException;

public class StandardMessageQueue implements MessageQueue {
	private static final Log log = Log.getLog();
	protected final String queueName;
	protected final Storage storage;
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition notEmpty = lock.newCondition();
	protected Message[] messages;
	protected Iterator<Message> storageMessages;
	protected int takeIndex;
	protected int putIndex;
	protected int count;
	protected final AtomicLong size = new AtomicLong(0);

	public StandardMessageQueue(String queueName, Storage storage) throws StorageException {
		this.queueName = queueName;
		this.storage = storage;
		size.set(storage.getQueueSize(queueName));
	}

	public long getSize() {
		return size.get();
	}
	
	public void deleteMessage(String messageId) throws StorageException {
		if(!storage.removeMessage(messageId)) {
			log.warn("Message '" + messageId + "' was acknowledged but does not exist in storage");
		}
	}
	/*NOTE: This must be called only under lock */
	protected Message extract() {
		Message x = messages[takeIndex];
		messages[takeIndex] = null;
        takeIndex = (++takeIndex == messages.length)? 0 : takeIndex;
        --count;
        return x;
    }
	public Message nextMessage(long timeout) throws InterruptedException, StorageException {
		long nanos = TimeUnit.MILLISECONDS.toNanos(timeout);
	    final ReentrantLock lock = this.lock;
        lock.lockInterruptibly();
        try {
            for (;;) {
                if (count != 0) {
                	Message x = extract();    	
                    if(!removeIfExpired(x))
                    	return x;
                    else
                    	continue;
                } else {
                	if(storageMessages == null) {
                		storageMessages = storage.getMessages(queueName);
                	}
                	while(storageMessages.hasNext()) {
                		Message x = storageMessages.next();
                		if(!removeIfExpired(x)) {
                			long wait = (x.getAvailable() == 0 ? 0 : x.getAvailable() - System.currentTimeMillis());
                			if(wait <= 0)
                				return x;
                			long waitNanos = TimeUnit.MILLISECONDS.toNanos(wait);
                			if(waitNanos < nanos) {
                				while(waitNanos > 0)
		                			try {
		                				waitNanos = notEmpty.awaitNanos(waitNanos);
		                            } catch (InterruptedException ie) {
		                                notEmpty.signal(); // propagate to non-interrupted thread
		                                throw ie;
		                            }
		                        return x;
                			} else {
                				break;
                			}
                		}
                	}
                }
                if (nanos <= 0)
                    return null;
                try {
                	nanos = notEmpty.awaitNanos(nanos);
                } catch (InterruptedException ie) {
                    notEmpty.signal(); // propagate to non-interrupted thread
                    throw ie;
                }
            }
        } finally {
            lock.unlock();
        }
        /*
		StandardMessage message = messages.pollFirst();
		if(message == null) {
			// check storage
			// wait for available message
		}
		//TODO: check if we need to load from storage
		return messages.poll(timeout, TimeUnit.MILLISECONDS);
		*/
    }
	
	protected boolean removeIfExpired(Message x) {
		// TODO Auto-generated method stub
		return false;
	}

	public Message nextMessageNoWait() throws StorageException {
		//TODO: check if we need to load from storage
		return null; //messages.pollFirst();
	}

	public String postMessage(Message message) throws StorageException {
		String messageId = storage.addMessage(message);
		//messages.
		return messageId;
	}

	public boolean removeSubscription(String subscriptionId) throws StorageException {
		// TODO Auto-generated method stub
		return false;
	}

	public String getQueueName() {
		return queueName;
	}

}
