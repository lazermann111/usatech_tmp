package simple.mq;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import simple.io.BinaryStream;

public class StandardMessage implements Message, Delayed {
	protected long posted;
	protected long available;
	protected long expiration;
	protected String messageId;
	protected byte priority;
	protected boolean temporary;
	protected String queueName;
	protected boolean redelivered;
	protected int retryCount;
	protected BinaryStream content;
	protected final AtomicBoolean readOnly = new AtomicBoolean(false);
	protected Long correlation;
	protected String[] blocks;
	
	protected void checkWritable() {
		if(readOnly.get())
			throw new IllegalStateException("Message is read-only");
	}
	public long getDelay(TimeUnit unit) {
		if(available == 0)
			return 0;
		return unit.toMillis(available - System.currentTimeMillis());
	}

	public int compareTo(Delayed o) {
		// order by available asc, priority desc, posted asc
		StandardMessage sm = (StandardMessage) o;
		if(available < sm.available)
			return -1;
		if(available > sm.available)
			return 1;
		if(priority < sm.priority)
			return 1;
		if(priority > sm.priority)
			return -1;
		if(posted < sm.posted)
			return -1;
		if(posted > sm.posted)
			return 1;
			
		return 0;
	}

	public long getAvailable() {
		return available;
	}

	public void setAvailable(long available) {
		checkWritable();
		this.available = available;
	}

	public long getExpiration() {
		return expiration;
	}

	public void setExpiration(long expiration) {
		checkWritable();
		this.expiration = expiration;
	}

	public long getExpiration(long posted) {
		return expiration;
	}
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		checkWritable();
		this.messageId = messageId;
	}

	public byte getPriority() {
		return priority;
	}

	public void setPriority(byte priority) {
		checkWritable();
		this.priority = priority;
	}

	public boolean isTemporary() {
		return temporary;
	}

	public void setTemporary(boolean temporary) {
		checkWritable();
		this.temporary = temporary;
	}

	public String getQueueName() {
		return queueName;
	}
	
	public void setQueueName(String queueName) {
		checkWritable();
		this.queueName = queueName;
	}

	public long getPosted() {
		return posted;
	}

	public void setPosted(long posted) {
		checkWritable();
		this.posted = posted;
		this.expiration = getExpiration(posted);
	}

	public boolean isRedelivered() {
		return redelivered;
	}

	public void setRedelivered(boolean redelivered) {
		checkWritable();
		this.redelivered = redelivered;
	}
	public int getRetryCount() {
		return retryCount;
	}
	public void setRetryCount(int retryCount) {
		checkWritable();
		this.retryCount = retryCount;
	}
	public BinaryStream getContent() {
		return content;
	}
	public void setContent(BinaryStream content) {
		checkWritable();
		this.content = content;
	}

	public Long getCorrelation() {
		return correlation;
	}

	public void setCorrelation(Long correlation) {
		checkWritable();
		this.correlation = correlation;
	}

	public String[] getBlocks() {
		return blocks;
	}

	public void setBlocks(String[] blocks) {
		this.blocks = blocks;
	}
}
