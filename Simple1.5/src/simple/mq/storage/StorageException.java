package simple.mq.storage;

public class StorageException extends Exception {
	private static final long serialVersionUID = 9087986466817156800L;

	public StorageException(String message) {
		super(message);
	}

	public StorageException(Throwable cause) {
		super(cause);
	}

	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
