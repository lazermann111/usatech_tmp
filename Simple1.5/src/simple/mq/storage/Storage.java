package simple.mq.storage;

import java.util.Iterator;

import simple.mq.Message;

public interface Storage {
	public long getQueueSize(String queueName) throws StorageException ;
	public Iterator<Message> getMessages(String queueName) throws StorageException ;
	public String addMessage(Message message) throws StorageException ;
	public boolean removeMessage(String messageId) throws StorageException ;
	public boolean removeSubscription(String queueName, String subscriptionId) throws StorageException ;
	public long drainMessageTo(String newNodeId) throws StorageException ;
	public long clearMessages(String queuePattern) throws StorageException ;	
}
