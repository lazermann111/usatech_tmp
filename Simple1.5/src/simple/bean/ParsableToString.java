/**
 *
 */
package simple.bean;

/** This interface is a marker that a class implements a toString() from which this object can be re-parsed exactly. This is used to avoid
 * employing other techniques when converting this class to a String.
 * @author Brian S. Krug
 *
 */
public interface ParsableToString {

}
