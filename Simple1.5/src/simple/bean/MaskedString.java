package simple.bean;

import java.util.Arrays;

import simple.text.StringUtils;

public class MaskedString implements Convertible {
	protected String value;
	protected String mask;

	public MaskedString() {
	}

	public MaskedString(String value) {
		this.value = value;
		char[] ch = new char[value.length()];
		Arrays.fill(ch, '*');
		this.mask = new String(ch);
	}

	public MaskedString(String value, String mask) {
		this.value = value;
		this.mask = mask;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
		if(StringUtils.isBlank(value))
			return;
		if(StringUtils.isBlank(mask)) {
			char[] ch = new char[value.length()];
			Arrays.fill(ch, '*');
			mask = new String(ch);
		} else if(mask.length() < value.length())
			mask = mask.substring(0, value.length());
		else if(mask.length() > value.length()) {
			char[] ch = new char[value.length()];
			for(int offset = 0; offset < ch.length; offset += mask.length())
				mask.getChars(0, mask.length(), ch, offset);
			mask = new String(ch);
		}
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof MaskedString && value.equals(((MaskedString)obj).getValue());
	}
	@Override
	public int hashCode() {
		return value.hashCode();
	}
	@Override
	public String toString() {
		return mask;
	}
	public <Target> Target convert(Class<Target> toClass) throws ConvertException {
		return ConvertUtils.convert(toClass, getValue());
	}

}
