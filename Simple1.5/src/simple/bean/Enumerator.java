package simple.bean;

/**
 * Turns an object into a String representation. Generally, a object implementing
 * this interface will also implement Converter and its format method will be
 * the functional opposite of its convert method.
 * @author Brian S. Krug
 */
public interface Enumerator<Source> {
    /** Returns a Number representation of the given object
     * @param object The object to enumerate
     * @return A Number representation of the object
     */
    public Number enumerate(Source object);

    public Class<Source> getSourceClass() ;

	public Class<? extends Number> getTargetClass();
}

