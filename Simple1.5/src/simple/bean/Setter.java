package simple.bean;

public interface Setter<T> {
	public void set(T t) ;
	public Class<T> getType() ;
}
