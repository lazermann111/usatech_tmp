package simple.bean.conditions;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import simple.bean.Condition;
import simple.bean.ConvertUtils;

/**
 * Evaluates whether the given string is an email address
 */
public class EmailCondition implements Condition {
    /**
     * The array of operators handled by this Condition. Includes "EMAIL" and "NOT EMAIL".
     */    
    protected final String[] operators = new String[] {
        "EMAIL",
        "NOT EMAIL"
    };
    
     /**
      * Determines if the first object matches the regular expression specified by the second object.
      * @param o1 The first object
      * @param operator The index of the operator to use to for this check
      * @param o2 The second object
      * @return Whether the specified objects pass the condition
      */
    public boolean check(Object o1, int operator, Object o2) {
    	switch(operator) {
			case 0:
				return checkInternal(o1, operator, o2);
    		case 1:
    			return !checkInternal(o1, operator, o2);
    		default:
    			return false;
    	}
    }
    protected boolean checkInternal(Object o1, int operator, Object o2) {
        // turn both to strings
        String s1 = ConvertUtils.getStringSafely(o1);
        if(s1 == null || s1.trim().isEmpty())
        	return false;
        InternetAddress ia = new InternetAddress();
        ia.setAddress(s1);
        try {
			ia.validate();
		} catch(AddressException e) {
			return false;
		}
        return true;
    }
        
    /** Returns an array of all the operators handled by this Condition. The index of the operator is
     * passed back to this object when the <CODE>check()</CODE> method is called. Includes "EMAIL" and "NOT EMAIL".
     * @return An array of valid operator names for this Condition object
     */
    public String[] getOperators() {
        return operators;
    }  
}

