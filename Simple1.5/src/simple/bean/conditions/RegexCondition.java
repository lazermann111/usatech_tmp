package simple.bean.conditions;

import simple.bean.Condition;
import simple.bean.ConditionUtils;

/**
 * Evaluates whether the given string matches the configured regular expression
 */
public class RegexCondition implements Condition {
    private static simple.io.Log log = simple.io.Log.getLog();
    
    /**
     * The array of operators handled by this Condition. Includes "MATCH" and "REGEX".
     */    
    protected static final String[] operators = new String[] {
        ConditionUtils.OP_MATCH,
        "REGEX",
        "NOT MATCH"
    };
    
     /**
      * Determines if the first object matches the regular expression specified by the second object.
      * @param o1 The first object
      * @param operator The index of the operator to use to for this check
      * @param o2 The second object
      * @return Whether the specified objects pass the condition
      */
    public boolean check(Object o1, int operator, Object o2) {
    	switch(operator) {
			case 0:
			case 1:
    			return checkInternal(o1, operator, o2);
			case 2:
    			return !checkInternal(o1, operator, o2);
    		default:
    			return false;
    	}
    }
    protected boolean checkInternal(Object o1, int operator, Object o2) {
        // turn both to strings
        if(o2 == null) return false; // this shouldn't be
        String s1 = (o1 == null ? "" : (String)ConditionUtils.prepare(o1, String.class));
        String p = getPattern(o2);
        log.debug("Determining if '" + s1 + "' matches the pattern, '" + p + "'");
        return p != null && simple.text.RegexUtils.matches(p, s1, null);
    }
    
    /**
     * Converts the given object to a String
     * @param o2 The object that represents the regular expression
     * @return The String to use as a regular expression
     */    
    protected String getPattern(Object o2) {
        String regex = (String)ConditionUtils.prepare(o2, String.class);
        if(regex == null || regex.trim().length() == 0) return null;
        return regex;
    }
    
    /** Returns an array of all the operators handled by this Condition. The index of the operator is
     * passed back to this object when the <CODE>check()</CODE> method is called. Includes "MATCH" and "REGEX".
     * @return An array of valid operator names for this Condition object
     */
    public String[] getOperators() {
        return operators;
    }
    
}

