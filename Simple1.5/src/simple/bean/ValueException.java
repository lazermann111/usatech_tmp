package simple.bean;

import java.math.BigDecimal;

public abstract class ValueException extends Exception {
	private static final long serialVersionUID = -4346066563974600191L;
	protected ValueException() {
	}

	protected ValueException(String message) {
		super(message);
	}

	protected ValueException(String message, Throwable cause) {
		super(message, cause);
	}

	protected ValueException(Throwable cause) {
		super(cause);
	}

	public static class ValueMissingException extends ValueException {
		private static final long serialVersionUID = 5L;
		protected String name;
		public ValueMissingException(String message, String name) {
			super(message);
			this.name = name;
		}
		public ValueMissingException(String name) {
			super();
			this.name = name;
		}
		public ValueMissingException() {
			super();
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	
	public static class ValueInvalidException extends ValueException {
		private static final long serialVersionUID = 4L;
		protected String name;
		protected Object value;
		public ValueInvalidException(String message, String name, Object value) {
			super(message);
			this.name = name;
			this.value = value;
		}
		public ValueInvalidException(String name, Object value) {
			super();
			this.name = name;
			this.value = value;
		}
		public ValueInvalidException(Object value) {
			super();
			this.value = value;
		}
		public ValueInvalidException(String message, String name, Object value, Throwable cause) {
			super(message, cause);
			this.name = name;
			this.value = value;
		}
		public ValueInvalidException(String name, Object value, Throwable cause) {
			super(cause);
			this.name = name;
			this.value = value;
		}
		public ValueInvalidException(Object value, Throwable cause) {
			super(cause);
			this.value = value;
		}
		public String getName() {
			return name;
		}
		public Object getValue() {
			return value;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	public static class StringTooLongException extends ValueInvalidException {
		private static final long serialVersionUID = 10L;
		protected int maxlength;
		public StringTooLongException( Object value, int maxlength) {
			super(value);
			this.maxlength = maxlength;
		}
		public StringTooLongException(String name, Object value, int maxlength) {
			super(name, value);
			this.maxlength = maxlength;
		}
		public StringTooLongException(String message, String name, Object value, int maxlength) {
			super(message, name, value);
			this.maxlength = maxlength;
		}
		
		public int getMaxlength() {
			return maxlength;
		}
	}
	public static class StringTooShortException extends ValueInvalidException {
		private static final long serialVersionUID = 11L;
		protected int minlength;
		public StringTooShortException( Object value, int minlength) {
			super(value);
			this.minlength = minlength;
		}
		public StringTooShortException(String name, Object value, int minlength) {
			super(name, value);
			this.minlength = minlength;
		}
		public StringTooShortException(String message, String name, Object value, int minlength) {
			super(message, name, value);
			this.minlength = minlength;
		}
		
		public int getMinlength() {
			return minlength;
		}
	}
	public static class ConversionException extends ValueInvalidException {
		private static final long serialVersionUID = 6L;
		protected Class<?> type;
		public ConversionException(ConvertException cause) {
			super(cause.getSourceObject(),cause);
			this.type = cause.getTargetClass();
		}
		public ConversionException(String name, ConvertException cause) {
			super(name, cause.getSourceObject(),cause);
			this.type = cause.getTargetClass();
		}
		public ConversionException(String message, String name, ConvertException cause) {
			super(message, name, cause.getSourceObject(), cause);
			this.type = cause.getTargetClass();
		}
		public Class<?> getType() {
			return type;
		}
	}
	public static class NumberTooLowException extends ValueInvalidException {
		private static final long serialVersionUID = 15L;
		protected BigDecimal minValue;
		public NumberTooLowException(Object value, BigDecimal minValue) {
			super(value);
			this.minValue = minValue;
		}
		public NumberTooLowException(String name, Object value, BigDecimal minValue) {
			super(name, value);
			this.minValue = minValue;
		}
		public NumberTooLowException(String message, String name, Object value, BigDecimal minValue) {
			super(message, name, value);
			this.minValue = minValue;
		}
		
		public BigDecimal getMinlength() {
			return minValue;
		}
	}
	public static class NumberTooHighException extends ValueInvalidException {
		private static final long serialVersionUID = 19L;
		protected BigDecimal maxValue;
		public NumberTooHighException(Object value, BigDecimal maxValue) {
			super(value);
			this.maxValue = maxValue;
		}
		public NumberTooHighException(String name, Object value, BigDecimal maxValue) {
			super(name, value);
			this.maxValue = maxValue;
		}
		public NumberTooHighException(String message, String name, Object value, BigDecimal maxValue) {
			super(message, name, value);
			this.maxValue = maxValue;
		}
		
		public BigDecimal getMinlength() {
			return maxValue;
		}
	}
}
