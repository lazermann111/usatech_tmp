/*
 * ConvertingDynaBean.java
 *
 * Created on November 6, 2003, 10:51 AM
 */

package simple.bean;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Set;

import org.apache.commons.beanutils.DynaProperty;

/** Provides DynaBean implemention around a specified bean, using simple.bean.ReflectionUtils to
 *  get and set properties on the bean.
 *
 * @author  Brian S. Krug
 */
public class BridgeDynaBean extends AbstractDynaBean {
    private static final simple.io.Log log = simple.io.Log.getLog();
    protected Set<String> exclude;
    protected Object bean;
    protected static final char[] DELIMS = new char[] { 
        NestedNameTokenizer.INDEXED_DELIM, 
        NestedNameTokenizer.MAPPED_DELIM, 
        NestedNameTokenizer.NESTED_DELIM
    };
    protected DynaProperty[] dynaProperties;
    
    /** Creates a new instance of BridgeDynaBean using the specified bean as the underlying object
     * @param excludeProperties The names of the properties on the bean which should be ignored and not included
     * in the list of DynaProperties
     * @param bean The bean
     */
    public BridgeDynaBean(Set<String> excludeProperties, Object bean) {
        this.exclude = excludeProperties;
        this.bean = bean;
    }
    
    /** Creates a new instance of BridgeDynaBean using this object as the underlying bean
     * @param excludeProperties The names of the properties on the bean which should be ignored and not included
     * in the list of DynaProperties
     */
    public BridgeDynaBean(Set<String> excludeProperties) {
        this.exclude = excludeProperties;
        this.bean = this;
    }
    
    protected boolean hasDynaProperty(String name) {
        if(checkExclude(name)) return false;
        try {
            return ReflectionUtils.hasProperty(this, name, false);
        } catch(IntrospectionException e) {
            return false;
        } catch (IllegalAccessException e) {
            return false;
        } catch (InvocationTargetException e) {
            return false;
        } catch(ParseException e) {
        	return false;
		}
    }
    
    protected boolean checkExclude(String name) {
        log.debug("Checking exclude for '" + name + "'");
        if(exclude.contains(name)) return true;
        int p = simple.text.StringUtils.lastIndexOf(name, DELIMS);
        if(p > 0) return checkExclude(name.substring(0, p));
        return false;       
    }
        
    protected String getDynaClassName() {
        return bean.getClass().getName();
    }
    
    protected Object getDynaProperty(String name) {
        try {
            return ReflectionUtils.getProperty(this, name, false);
        } catch(IntrospectionException e) {
            log.warn("While getting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be read");
        } catch (IllegalAccessException e) {
            log.warn("While getting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be read");
        } catch (InvocationTargetException e) {
            log.warn("While getting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be read");
        } catch(ParseException e) {
            log.warn("While getting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be read");
		}
    }
    
    protected void setDynaProperty(String name, Object value) {
        try {
            ReflectionUtils.setProperty(this, name, value, true, false);
        } catch(IntrospectionException e) {
            log.warn("While setting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be set");
        } catch (IllegalAccessException e) {
            log.warn("While setting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be set");
        } catch (InvocationTargetException e) {
            log.warn("While setting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be set");
        } catch (InstantiationException e) {
            log.warn("While setting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be set");
        } catch (ConvertException e) {
            log.warn("While setting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be set");
        } catch(ParseException e) {
            log.warn("While setting property '" + name + "'", e);
            throw new IllegalArgumentException("Property '" + name + "' can't be set");
		}
    }
    
    protected DynaProperty[] getDynaClassProperties() { 
        if(dynaProperties == null) {
            try {
                PropertyDescriptor[] pds = ReflectionUtils.getPropertyDescriptors(bean.getClass());
                dynaProperties = new DynaProperty[pds.length];
                for(int i = 0; i < pds.length; i++) {
                    dynaProperties[i] = new DynaProperty(pds[i].getName(), pds[i].getPropertyType());
                }
            } catch (IntrospectionException e) {
                log.warn("Could not create properties", e);
            }          
        }
        return dynaProperties;
    }

}
