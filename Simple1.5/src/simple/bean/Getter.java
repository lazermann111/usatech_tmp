package simple.bean;

public interface Getter<T> {
	public T get() ;
	public Class<T> getType() ;
}
