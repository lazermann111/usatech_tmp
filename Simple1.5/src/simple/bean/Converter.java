/*
 * Converter.java
 *
 * Created on January 30, 2003, 9:34 AM
 */

package simple.bean;

/**
 * Converters take objects and return an equivalent object of a different class.
 * Each Converter class should handle receiving an object of any type, but return
 * an object that is equal to or a sub-class of the Class returned from the getTargetClass() method.
 * Once a converter is created, register it with ConvertUtils using the register() method.
 *
 * @author  Brian S. Krug
 */
public interface Converter<Target> {

    /** Converts the object to the target class for this Converter
     * @param object The object to convert
     * @throws Exception If the object could not be converted
     * @return An object that is an instance of the Target value for this Converter or
     * null
     */
    public Target convert(Object object) throws Exception;
    /** The Class that the convert method attempts to return
     * @return The target Class for this Converter
     */
    public Class<Target> getTargetClass() ;
    
}

