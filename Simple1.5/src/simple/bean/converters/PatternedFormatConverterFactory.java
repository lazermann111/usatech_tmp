/*
 * Created on Oct 3, 2005
 *
 */
package simple.bean.converters;

import java.lang.reflect.Constructor;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.ConverterFactory;
import simple.io.Log;
import simple.text.AbstractPatternedFormat;

public class PatternedFormatConverterFactory implements ConverterFactory<AbstractPatternedFormat> {
    private static final Log log = Log.getLog();
	protected static final Class<?>[] STRING_ARG = new Class[] { String.class };

	public PatternedFormatConverterFactory() {
        super();
    }

    public <S> Converter<S> createConverter(final Class<S> specificType) throws ConvertException {
        if(log.isDebugEnabled())
            log.debug("Creating Converter for Format " + specificType.getName());
        final Constructor<S> constructor;
		try {
			constructor = specificType.getConstructor(STRING_ARG);
		} catch(SecurityException e) {
			throw new ConvertException("Could not find accessible 1-String-argument constructor", specificType, null, e);
		} catch(NoSuchMethodException e) {
			throw new ConvertException("Could not find accessible 1-String-argument constructor", specificType, null, e);
		}

        return new Converter<S>() {
            public S convert(Object object) throws Exception {
            	String pattern = ConvertUtils.getString(object, true);
                return constructor.newInstance(new Object[] {pattern});
            }
            public Class<S> getTargetClass() {
                return specificType;
            }
        };
    }

    public Class<AbstractPatternedFormat> getBaseClass() {
        return AbstractPatternedFormat.class;
    }

}
