/*
 * ByteConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

/**
 *
 * @author  Brian S. Krug
 */
public class ByteConverter extends AbstractNumberConverter<Byte> {

    /** Creates new ByteConverter */
    public ByteConverter() {
    }

    public Class<Byte> getTargetClass() {
        return Byte.class;
    }
    @Override
    protected Byte convertLong(long time) {
        return (byte)time;
    }

    @Override
    protected Byte convertNumber(Number number) {
        return number.byteValue();
    }

    @Override
    protected Byte convertString(String s) {
        return new Byte(removeTrailingZeros(s.trim()));
    }
}
