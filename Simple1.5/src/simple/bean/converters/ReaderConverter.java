package simple.bean.converters;

import java.io.CharArrayReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Clob;

import simple.bean.ConvertUtils;
import simple.bean.Converter;

public class ReaderConverter implements Converter<java.io.Reader> {
	public Reader convert(Object object) throws Exception {
		if(object instanceof Clob) return ((Clob)object).getCharacterStream();
        else if(object instanceof char[]) return new CharArrayReader((char[])object);
        else if(object instanceof Character[]) return new CharArrayReader(ConvertUtils.convert(char[].class, object));
        else if(object instanceof File) return new FileReader((File)object);
        else if(object instanceof String) return new StringReader((String)object);
        else return new InputStreamReader(ConvertUtils.convert(InputStream.class, object));
	}

	public Class<Reader> getTargetClass() {
		return Reader.class;
	}
}
