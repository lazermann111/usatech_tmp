/*
 * FormatConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.nio.charset.Charset;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;

/**
 *
 * @author  Brian S. Krug
 */
public class CharsetConverter implements Converter<Charset>, Formatter<Charset> {
	public Charset convert(Object object) throws Exception {
       if(object == null)
    	   return null;
       String s = ConvertUtils.getString(object, false);
       if(s == null || (s=s.trim()).length() == 0)
    	   return null;
       return Charset.forName(s);
    }

    public Class<Charset> getTargetClass() {
        return Charset.class;
    }

    public String format(Charset object) {
        return object == null ? null : object.name();
    }
}
