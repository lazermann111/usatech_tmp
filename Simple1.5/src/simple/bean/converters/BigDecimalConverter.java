/*
 * BigDecimalConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.math.BigDecimal;
import java.math.BigInteger;

import simple.bean.ConvertUtils;

/**
 * Converts objects to java.math.BigDecimal
 * @author Brian S. Krug
 */
public class BigDecimalConverter extends AbstractNumberConverter<BigDecimal> {

    /** Creates new BigDecimalConverter */
    public BigDecimalConverter() {
    }

    /**
     * Converts objects to BigDecimal
     * @param object The converted object
     * @throws Exception If cannot convert from the given object into a BigDecimal
     * @return A BigDecimal represenation of the object
     */    
    public BigDecimal convert(Object object) throws Exception {
        if(object instanceof byte[] || object instanceof Byte[]) object = ConvertUtils.convert(BigInteger.class, object);
        return super.convert(object);
    }    
    
    /**
     * Returns BigDecimal as the target class
     * @return java.math.BigDecimal
     */    
    public Class<BigDecimal> getTargetClass() {
        return BigDecimal.class;
    }

    @Override
    protected BigDecimal convertLong(long time) {
        return BigDecimal.valueOf(time);
    }

    @Override
    protected BigDecimal convertString(String s) {
        return new BigDecimal(s.trim());
    }   
}
