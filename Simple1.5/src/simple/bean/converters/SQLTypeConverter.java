/*
 * Created on Feb 6, 2006
 *
 */
package simple.bean.converters;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;
import simple.sql.ArraySQLType;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;

public class SQLTypeConverter implements Converter<SQLType>, Formatter<SQLType> {
	protected static final Pattern pattern = Pattern.compile("\\s*(\\w+)(?:\\s*[(]\\s*(\\d+)\\s*(?:[,]\\s*(\\d+)\\s*)?[)])?(?:(?:\\s*[:]\\s*|\\s+OF\\s+)(\\w+(?:\\s*[(]\\s*\\d+\\s*(?:[,]\\s*\\d+\\s*)?[)])?(?:(?:\\s*[:]\\s*|\\s+OF\\s+)\\w+(?:\\s*[(]\\s*\\d+\\s*(?:[,]\\s*\\d+\\s*)?[)])?)*))?\\s*", Pattern.CASE_INSENSITIVE);
	protected static final Map<String, Integer> additionalNamesToCode = new HashMap<String, Integer>();
	protected static final String[] arraySuffixes = new String[] {
		"_LIST",
		"S",
		"_ARRAY"
	};
	static {
		additionalNamesToCode.put("NUMBER", Types.NUMERIC);
	}
	public SQLTypeConverter() {
    }
	
	
	

    public SQLType convert(Object object) throws Exception {
    	String s = ConvertUtils.convert(String.class, object);
    	return convertToSQLType(s, false);
    }

    public static SQLType convertToSQLType(String sqlTypeString) throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException, ConvertException {
    	return convertToSQLType(sqlTypeString, false);
    }

    protected static SQLType convertToSQLType(String sqlTypeString, boolean isComponent) throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException, ConvertException {
    	Matcher matcher = pattern.matcher(sqlTypeString);
    	if(matcher.matches()) {
    		String typeName = matcher.group(1);
			String dbName = null;
    		int typeCode;
    		try {
				typeCode = SQLTypeUtils.getTypeCode(typeName);
    		} catch (NoSuchFieldException e) {
				Integer alt = additionalNamesToCode.get(typeName);
				if(alt != null) {
					typeCode = alt;
					dbName = typeName;
				} else if(isComponent)
    				typeCode = getArrayComponentTypeCode(typeName, e);
    			else
    				throw e;
			}
    		Integer precision = ConvertUtils.convert(Integer.class, matcher.group(2));
    		Integer scale = ConvertUtils.convert(Integer.class, matcher.group(3));
    		String componentName = matcher.group(4);
    		if(typeCode == Types.ARRAY && componentName != null && componentName.length() > 0) {
    			SQLType sqlType = convertToSQLType(componentName, true);
    			return new ArraySQLType(null, precision, scale, sqlType);
    		} else {
				return new SQLType(typeCode, dbName, precision, scale);
    		}
    	} else {
    		throw new ConvertException("Could not convert '" + sqlTypeString + "' to a SQLType. Improper format.", SQLType.class, sqlTypeString);
    	}
    }

	protected static int getArrayComponentTypeCode(String typeName, NoSuchFieldException e) throws NoSuchFieldException {
		typeName = typeName.toUpperCase();
		for(String s : arraySuffixes)
			if(typeName.endsWith(s)) {
				try {
	                return SQLTypeUtils.getTypeCode(typeName.substring(0, typeName.length() - s.length()));
	            } catch(NoSuchFieldException e1) {
	            }
			}
		throw e;
	}

    public Class<SQLType> getTargetClass() {
        return SQLType.class;
    }

	public String format(SQLType object) {
		StringBuilder sb = new StringBuilder();
		format(object, sb);
		return sb.toString();
	}

	protected void format(SQLType object, StringBuilder appendTo) {
		String typeName = SQLTypeUtils.getTypeCodeName(object.getTypeCode());
		appendTo.append(typeName);
		if(hasPrecision(object.getTypeCode()) && object.getPrecision() != null) {
			appendTo.append('(').append(object.getPrecision());
			if(object.getScale() != null)
				appendTo.append(',').append(object.getScale());
			appendTo.append(')');
		}
		if(object instanceof ArraySQLType) {
			SQLType compType = ((ArraySQLType)object).getComponentType();
			if(compType != null) {
				appendTo.append(':');
				format(compType, appendTo);
			}
		}
	}

	protected boolean hasPrecision(int typeCode) {
		switch(typeCode) {
		    case Types.CHAR:
            case Types.VARCHAR:
            case Types.NUMERIC:
            case Types.DECIMAL:
            case Types.VARBINARY:
            	return true;
            default:
            	return false;
		}
	}
}