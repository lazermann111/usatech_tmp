/*
 * XDateConverter.java
 *
 * Created on April 2, 2003, 12:47 PM
 */

package simple.bean.converters;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.text.RegexUtils;
import simple.text.StringUtils;
import simple.util.concurrent.Factory;

/**
 * Uses a series of regular expressions to parse dates of various forms into a calendar object
 * @author  Brian S. Krug
 */
public class CalendarParser {
    public static final int IGNORED_DATE_PART = -1;
    public static final int MILLISECONDS_SINCE_EPOCH = -2;
    protected class PatternHolder {
        protected Pattern pattern;
        protected int[] dateParts;
        protected boolean relative;
    }
    protected Set<PatternHolder> patterns = new LinkedHashSet<PatternHolder>();
    protected Map<String, Integer> months = new HashMap<String, Integer>();
    protected Map<String, Integer> weekdays = new HashMap<String, Integer>();
    protected Map<String, Integer> ampms = new HashMap<String, Integer>();
    protected Map<String, Integer> eras = new HashMap<String, Integer>();
	protected Map<String, int[]> zoneOffsets = new HashMap<String, int[]>();
	protected Map<String, TimeZone> zones = new HashMap<String, TimeZone>();

    /** Creates a new instance of XDateConverter
     *
     */
    public CalendarParser() {
    	Locale[] allLocales=Locale.getAvailableLocales();
    	Locale defaultLocale=Locale.getDefault();
    	for(Locale locale:allLocales){
    		if(!locale.equals(defaultLocale)){
	    		DateFormatSymbols dfs = new DateFormatSymbols(locale);
	            addArrayToMap(months, dfs.getMonths());
	            addArrayToMap(months, dfs.getShortMonths());
	            addArrayToMap(weekdays, dfs.getWeekdays());
	            addArrayToMap(weekdays, dfs.getShortWeekdays());
	            addArrayToMap(ampms, dfs.getAmPmStrings());
	            addArrayToMap(eras, dfs.getEras());
				addZonesToMap(zones, zoneOffsets, dfs.getZoneStrings());
    		}
    	}
    	//add default at the end
    	DateFormatSymbols dfs = new DateFormatSymbols();
        addArrayToMap(months, dfs.getMonths());
        addArrayToMap(months, dfs.getShortMonths());
        addArrayToMap(weekdays, dfs.getWeekdays());
        addArrayToMap(weekdays, dfs.getShortWeekdays());
        addArrayToMap(ampms, dfs.getAmPmStrings());
        addArrayToMap(eras, dfs.getEras());
		addZonesToMap(zones, zoneOffsets, dfs.getZoneStrings());
        init();
    }

    protected void init() {
    	String monthNumRE = "(?:0?[1-9])|(?:1[0-2])";
    	StringBuilder sb = new StringBuilder();
    	for(String mm : months.keySet())
    		if(mm != null && mm.trim().length() > 0)
				RegexUtils.appendEscaped(mm, sb.append("(?:")).append(")|");
    	sb.setLength(sb.length()-1);

    	String monthNameRE = sb.toString();

    	sb.setLength(0);
    	for(String wd : weekdays.keySet())
    		if(wd != null && wd.trim().length() > 0)
				RegexUtils.appendEscaped(wd, sb.append("(?:")).append(")|");
    	sb.setLength(sb.length()-1);

    	String weekdayNameRE = sb.toString();
    	String dayNumRE = "(?:0?[1-9])|(?:[1-2][0-9])|(?:3[0-1])";
		// TODO: Use AMPM strings to create RE
    	Factory<Integer, String, RuntimeException> timeRE = new Factory<Integer, String, RuntimeException>() {
    		public String create(Integer sepIndex, Object... additionalInfo) {
				return "(\\d{1,2})(?:([:\\-\\.]?)(\\d{1,2})(?:\\" + sepIndex + "(\\d{1,2})(?:\\.(\\d{1,3}))?)?)?\\s*((?:[AP]M?)|(?:[AP]\\.M\\.))?(?:\\s+(.*))??";
			}
    	};
		// TODO: Use ERA strings to create RE
    	String eraRE = "(?:AD)|(?:BC)|(?:A\\.D\\.)|(?:B\\.C\\.)";
    	String commaOrSpaceRE = "(?:(?:\\s*\\,\\s*)|(?:\\s+))";

    	// XXX: this should be configured in a resource file but hard-code it for now
        // this first pattern hopefully accomodates the most common date formats
        //The toString() of Date uses EEE MMM dd HH:mm:ss zzz yyyy
    	addDatePattern("(?:(" + weekdayNameRE + ")"+commaOrSpaceRE+")?(" + monthNumRE + "|" + monthNameRE + ")\\s+(" + dayNumRE + ")\\s+"
    			+ "(\\d{1,2})(?:\\s*([:\\-\\. ]\\s*)(\\d{1,2})(?:\\s*\\5\\s*(\\d{1,2})(?:\\.(\\d{1,3}))?)?)?\\s*((?:[AP]M?)|(?:[AP]\\.M\\.))?(?:\\s+(\\S+))??"
    			+ "\\s+(?:(\\d{2,4})\\s*(" + eraRE + ")?)", "i",
                new int[] {Calendar.DAY_OF_WEEK, Calendar.MONTH, Calendar.DAY_OF_MONTH,
                        Calendar.HOUR_OF_DAY, IGNORED_DATE_PART, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND, Calendar.AM_PM, Calendar.ZONE_OFFSET,
                        Calendar.YEAR, Calendar.ERA},
                        false);

    	// mm-dd-yyyy or mm/dd/yyyy or mmm/dd/yyyy or mmmm-dd-yyyy or any of these with two digit years and with optional time
		addDatePattern("(?:(" + weekdayNameRE + ")" + commaOrSpaceRE + ")?(" + monthNumRE + "|" + monthNameRE + ")\\s*([-/\\\\\\s\\.])\\s*(" + dayNumRE + ")(?:\\s*\\3\\s*(\\d{2,4})\\s*(" + eraRE + ")?)"
                + "(?:\\s+" + timeRE.create(8) + ")?", "i",
                new int[] {Calendar.DAY_OF_WEEK, Calendar.MONTH, IGNORED_DATE_PART, Calendar.DAY_OF_MONTH, Calendar.YEAR, Calendar.ERA,
                        Calendar.HOUR_OF_DAY, IGNORED_DATE_PART, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND, Calendar.AM_PM, Calendar.ZONE_OFFSET},
                        false);

        // missing day
		addDatePattern("(" + monthNumRE + "|" + monthNameRE + ")\\s*[-/\\\\\\.]\\s*(\\d{2,4})\\s*(" + eraRE + ")?"
        		+ "(?:\\s+" + timeRE.create(5) + ")?", "i",
                new int[] {Calendar.MONTH, Calendar.YEAR, Calendar.ERA,
                        Calendar.HOUR_OF_DAY, IGNORED_DATE_PART, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND, Calendar.AM_PM, Calendar.ZONE_OFFSET},
                        false);

        // yyyy-mm-dd or yyyy/mm/dd and with optional time
		addDatePattern("(?:(" + weekdayNameRE + ")" + commaOrSpaceRE + ")?(\\d{4})\\s*([-/\\\\\\.])\\s*(" + monthNumRE + "|" + monthNameRE + ")(?:\\s*\\3\\s*(" + dayNumRE + "))?\\s*(" + eraRE + ")?"
        		+ "(?:\\s+" + timeRE.create(8) + ")?", "i",
                new int[] {Calendar.DAY_OF_WEEK, Calendar.YEAR, IGNORED_DATE_PART, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.ERA,
                        Calendar.HOUR_OF_DAY, IGNORED_DATE_PART, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND, Calendar.AM_PM, Calendar.ZONE_OFFSET},
                        false);

        //month as word or abbrev (mmm dd, yyyy)
        addDatePattern("(?:(" + weekdayNameRE + ")"+commaOrSpaceRE+")?(" + monthNameRE + ")\\s*("+dayNumRE+")(?:"+commaOrSpaceRE+"(\\d{2,4})\\s*("+eraRE+")?)?"
        		+ "(?:\\s+" + timeRE.create(7) + ")?", "i",
                new int[] {Calendar.DAY_OF_WEEK, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.YEAR, Calendar.ERA,
                        Calendar.HOUR_OF_DAY, IGNORED_DATE_PART, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND, Calendar.AM_PM, Calendar.ZONE_OFFSET},
                        false);

        //Month as word or abbrev, missing day
        addDatePattern("(" + monthNameRE + ")"+commaOrSpaceRE+"(\\d{2,4})\\s*("+eraRE+")?", "i",
                new int[] {Calendar.MONTH, Calendar.YEAR, Calendar.ERA},
                        false);

        //day first, month as word style
		addDatePattern("(?:(" + weekdayNameRE + ")" + commaOrSpaceRE + ")?(" + dayNumRE + ")\\s*[-/\\\\\\.]?\\s*(" + monthNameRE + ")(?:\\s*[-/\\\\,\\.]?\\s*(\\d{2,4})\\s*(" + eraRE + ")?)?"
        		+ "(?:\\s+" + timeRE.create(7) + ")?", "i",
                new int[] {Calendar.DAY_OF_WEEK, Calendar.DAY_OF_MONTH, Calendar.MONTH, Calendar.YEAR, Calendar.ERA,
                        Calendar.HOUR_OF_DAY, IGNORED_DATE_PART, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND, Calendar.AM_PM, Calendar.ZONE_OFFSET},
                        false);

        //mmddyyyy with optional time
        addDatePattern("(" + monthNumRE + ")(" + dayNumRE + ")(?:(\\d{2,4})\\s*("+eraRE+")?)?"
                + "(?:\\s+" + timeRE.create(6) + ")?", "i",
                new int[] {Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.YEAR, Calendar.ERA,
                        Calendar.HOUR_OF_DAY, IGNORED_DATE_PART, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND, Calendar.AM_PM, Calendar.ZONE_OFFSET},
                        false);

        // missing date part
        addDatePattern(timeRE.create(2), "i",
                new int[] {Calendar.HOUR_OF_DAY, IGNORED_DATE_PART, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND, Calendar.AM_PM, Calendar.ZONE_OFFSET},
                false);

        //this pattern allows an absolute time in milleseconds past epoch
        addDatePattern("\\{(-?\\d+)\\}",
                new int[] {MILLISECONDS_SINCE_EPOCH}, false);

        //this pattern is the relative time in milleseconds past the current time
        addDatePattern("\\{\\*(-?\\d+)\\}",
                new int[] {MILLISECONDS_SINCE_EPOCH}, true);

        //-- The relative formats
        //Trunc to Year
        addDatePattern("\\{\\*YEAR((?:-|[+])\\d+)?\\}", "i",
                new int[] {Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND}, true);

        //Trunc to Month
        addDatePattern("\\{\\*MONTH((?:-|[+])\\d+)?\\}", "i",
                new int[] {Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND}, true);

        //Trunc to Day
        addDatePattern("\\{\\*DAY((?:-|[+])\\d+)?\\}",  "i",
                new int[] {Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND}, true);

        //Trunc to Hour
        addDatePattern("\\{\\*HOUR((?:-|[+])\\d+)?\\}",  "i",
                new int[] {Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND}, true);
    }

	protected static void addZonesToMap(Map<String, TimeZone> zones, Map<String, int[]> zoneOffsets, String[][] zoneInfo) {
		for(int i = 0; i < zoneInfo.length; i++) {
			TimeZone tz = TimeZone.getTimeZone(zoneInfo[i][0]);
			if(tz != null) {
				zones.put(zoneInfo[i][0].toUpperCase(), tz);
				int[] z0 = new int[2];
	            z0[0] = tz.getRawOffset();
				for(int k = 1; k < 3; k++)
					zoneOffsets.put(zoneInfo[i][k].toUpperCase(), z0);
	            int[] z1 = new int[2];
	            z1[0] = tz.getRawOffset();
	            z1[1] = tz.getDSTSavings();
				for(int k = 3; k < 5; k++)
					zoneOffsets.put(zoneInfo[i][k].toUpperCase(), z1);
			}
		}
	}
    protected static void addArrayToMap(Map<String, Integer> map, String[] arr) {
        for(int i = 0; i < arr.length; i++) map.put(arr[i].toUpperCase(), i);
    }

    protected static int getValueFromMap(Map<String,? extends Number> map, String key) throws ParseException {
        Number n = map.get(key.toUpperCase());
        if(n == null) throw new ParseException("Invalid string '" + key + "' for date part", 1);
        return n.intValue();
    }

    protected static final Pattern calendarStringPattern = Pattern.compile("((?:[A-Za-z$_][\\w$]*\\.)*[A-Za-z$_][\\w$]*Calendar)\\[time=(\\?|\\d+),areFieldsSet=(false|true),areAllFieldsSet=(false|true),lenient=(false|true)," +
			"zone=((?:[A-Za-z$_][\\w$]*\\.)*[A-Za-z$_][\\w$]*)\\[(.*)\\],firstDayOfWeek=(\\d+),minimalDaysInFirstWeek=(\\d+)(,.*)\\]");
    protected static final Pattern calendarStringFieldPattern = Pattern.compile(",(\\w+)=(\\?|\\d+)(?=,|$)");
    protected static final Pattern timeZoneStringPattern = Pattern.compile("(?:^|,)id=\\\"([^\"]+)\\\"");
    /** Reverse Calendar.toString()
     * @param s
     * @return
     * @throws ParseException
     */
    public Calendar parseCalendarString(String s) throws ParseException {
    	String[] parts = RegexUtils.match(calendarStringPattern, s);
    	if(parts == null)
    		return null;
    	Class<?> clazz;
		try {
			clazz = Class.forName(parts[1]);
		} catch(ClassNotFoundException e) {
			throw new ParseException("Class '" + parts[1] + "' could not be found", 1);
		}
    	if(!Calendar.class.isAssignableFrom(clazz))
    		throw new ParseException("Class '" + parts[1] + "' is not a subtype of java.util.Calendar", 1);
    	if(Modifier.isAbstract(clazz.getModifiers()))
    		throw new ParseException("Class '" + parts[1] + "' is abstract and cannot be instantiated", 1);
    	if(Modifier.isInterface(clazz.getModifiers()))
    		throw new ParseException("Class '" + parts[1] + "' is an interface and cannot be instantiated", 1);
    	Calendar cal;
		try {
			cal = clazz.asSubclass(Calendar.class).newInstance();
		} catch(InstantiationException e) {
			throw new ParseException("Could not instantiate '" + parts[1] + "': " + e.getMessage(), 1);
		} catch(IllegalAccessException e) {
			throw new ParseException("Class '" + parts[1] + "' is an interface and cannot be instantiated", 1);
		}
    	if(!parts[2].equals("?"))
			try {
				cal.setTimeInMillis(ConvertUtils.getLong(parts[2]));
			} catch(ConvertException e) {
				throw new ParseException("Could not convert '" + parts[2] + "' to a long for the time", 2);
			}
		try {
			cal.setLenient(ConvertUtils.getBoolean(parts[5]));
		} catch(ConvertException e) {
			throw new ParseException("Could not convert '" + parts[5] + "' to a boolean for lenient", 5);
		}

		//parse timezone
		RegexUtils.Finder find = RegexUtils.find(timeZoneStringPattern, parts[7]);
		String[] tzParts = find.find();
		if(tzParts == null)
			throw new ParseException("Timezone details, '" + parts[7] + "' do not match expected format", 7);
		cal.setTimeZone(TimeZone.getTimeZone(tzParts[1]));

		try {
			cal.setFirstDayOfWeek(ConvertUtils.getInt(parts[8]));
		} catch(ConvertException e) {
			throw new ParseException("Could not convert '" + parts[8] + "' to an int for the first day of the week", 8);
		}

		try {
			cal.setMinimalDaysInFirstWeek(ConvertUtils.getInt(parts[9]));
		} catch(ConvertException e) {
			throw new ParseException("Could not convert '" + parts[9] + "' to an int for the minimum days in first week", 9);
		}

		// now all the fields
		find = RegexUtils.find(calendarStringFieldPattern, parts[10]);
		String[] fieldParts;
		while((fieldParts=find.find()) != null) {
			Field field;
			try {
				field = Calendar.class.getField(fieldParts[1]);
			} catch(SecurityException e) {
				throw new ParseException("Field '" + fieldParts[1] + "' could not be accessed in java.util.Calendar", 10);
			} catch(NoSuchFieldException e) {
				throw new ParseException("Field '" + fieldParts[1] + "' could not be found in java.util.Calendar", 10);
			}
			try {
				cal.set(field.getInt(null), ConvertUtils.getInt(fieldParts[2]));
			} catch(IllegalArgumentException e) {
				throw new ParseException("The int value of field '" + fieldParts[1] + "' could not obtained from java.util.Calendar", 10);
			} catch(IllegalAccessException e) {
				throw new ParseException("The int value of field '" + fieldParts[1] + "' could not obtained from java.util.Calendar", 10);
			} catch(ConvertException e) {
				throw new ParseException("Could not convert '" + fieldParts[2] + "' to an int for " + fieldParts[1], 10);
			}
		}
		return cal;

    }

    public Calendar parse(String s) throws ParseException {
    	Calendar cal = parseCalendarString(s);
    	if(cal != null)
    		return cal;
        ParseException firstExc = null;
        for(PatternHolder ph : patterns) {
            try {
                RegexUtils.Finder find = RegexUtils.find(ph.pattern, s);
                if(find.matches()) {
                    String[] parts = find.match();
                    cal = Calendar.getInstance();
                    final int year = cal.get(Calendar.YEAR);
                    int hourOfDay = -1;
                    int amPm = -1;
                    int addMS = 0;
                    if(!ph.relative) {
                    	cal.clear();
                    	//While technically correct to blank out dst and zone offset, it breaks most uses since a SimpleDateFormat will convert to local time before printing
                    	//cal.set(Calendar.DST_OFFSET, 0);
                        //cal.set(Calendar.ZONE_OFFSET, 0);
                    }
                    for(int i = 0; i < ph.dateParts.length; i++) {
                        if(ph.dateParts[i] == IGNORED_DATE_PART) continue;
                        int v;
                        if(ph.relative) {
                            if(i+1 >= parts.length) {
                                switch(ph.dateParts[i]) {
                                    case Calendar.ERA:
                                    case Calendar.DAY_OF_WEEK:
                                    case Calendar.DAY_OF_MONTH:
                                    case Calendar.DAY_OF_YEAR:
                                    case Calendar.WEEK_OF_YEAR:
                                    case Calendar.WEEK_OF_MONTH:
                                        v = 1;
                                        break;
                                    default:
                                        v = 0;
                                }
                                cal.set(ph.dateParts[i], v);
                            } else if(parts[i+1] != null && parts[i+1].trim().length() > 0) {
                                String part = parts[i+1].trim();
                                if(part.startsWith("+")) part = part.substring(1);
                                if(ph.dateParts[i] == MILLISECONDS_SINCE_EPOCH) {
                                    long ms = Long.parseLong(part) + System.currentTimeMillis();
                                    cal.setTimeInMillis(ms);
                                } else {
                                    v = Integer.parseInt(part);
                                    cal.add(ph.dateParts[i], v);
                                }
                            }
                        } else if(i+1 < parts.length && parts[i+1] != null && parts[i+1].trim().length() > 0){
                            String part = parts[i+1].trim();
                            switch(ph.dateParts[i]) {
                                case Calendar.HOUR_OF_DAY: case Calendar.HOUR:
                                    hourOfDay = Integer.parseInt(part);
                                    continue;
                                case Calendar.AM_PM:
    	                            //convert as necessary
    	                            String tmp = StringUtils.replace(part, new String[] {" ", "."}, null);
    	                            if(!tmp.toUpperCase().endsWith("M")) tmp += "M";
                                    amPm = getValueFromMap(ampms, tmp);
                                    continue;
    	                        case Calendar.ERA:
    	                            String era = StringUtils.replace(part, new String[] {" ", "."}, null);
                                	v = getValueFromMap(eras, era);
    	                        	break;
                                case MILLISECONDS_SINCE_EPOCH:
                                    long ms = Long.parseLong(part);
                                    cal.setTimeInMillis(ms);
                                    continue;
                                case Calendar.ZONE_OFFSET:
									int[] z;
									TimeZone tz = zones.get(part.toUpperCase());
									if(tz != null) {
										cal.setTimeZone(tz);
										continue;
									} else if((z = zoneOffsets.get(part.toUpperCase())) != null) {
										v = z[0];
										cal.set(Calendar.DST_OFFSET, z[1]);
										break;
                                    } else {
                                    	// look for [+-]d'd'hh:mm:ss.SSSS format or [+-]hhmm (RFC 822)
                                    	char ch = part.charAt(0);
                                    	switch(ch) {
                                    		case '+': case '-':
                                    			final int[] factors = new int[] {24*60*60*1000, 60*60*1000, 60*1000, 1000, 1};
                                    			if(RegexUtils.matches("\\d{4}", part.substring(1).trim(), null)) {
                                    				int h = Integer.parseInt(part.substring(0,3));
                                      				int m = Integer.parseInt(part.substring(3,5));
                                      				if(h > 23 || h < -23) throw new ParseException("Timezone hours must be between 0 and 23", 1);
                                      				if(m > 59 || m < 0) throw new ParseException("Timezone minutes must be between 0 and 59", 1);
                                      				cal.set(Calendar.ZONE_OFFSET, (h * factors[1]) + (m * factors[2]));
                                        			cal.set(Calendar.DST_OFFSET, 0);
                                        			break;
                                    			} else {
                                    				String[] addTimeParts = RegexUtils.match("(?:(\\d+)d\\s*)?(?:(\\d+)(?:[:](\\d+)(?:[:](\\d+)(?:[.](\\d+))?)?)?)?",
                                        					part.substring(1).trim(), null);
	                                        		if(addTimeParts != null) {
	                                    				int offset = 0;
	                                    				for(int k = 1; k < addTimeParts.length; k++) {
	                                    					if(addTimeParts[k] != null && addTimeParts[k].length() > 0) {
	                                    						int n = Integer.parseInt(addTimeParts[k]);
	                                    						offset += n * factors[k-1];
	                                    					}
	                                    				}
	                                    				if(ch == '-') offset = -offset;
	                                    				addMS = offset;
	                                    				break;
	                                    			}
	                                        	}
                                    		default:
                                    			throw new ParseException("Invalid string '" + part + "' for Timezone date part", 1);
                                    	}
                                    	continue;
                                    }
                                default:
    		                        try {
    		                            v =  Integer.parseInt(part);
    		                            if(ph.dateParts[i] == Calendar.MONTH) {
    		                                v--;
    		                            } else if(ph.dateParts[i] == Calendar.YEAR && part.length() < 4) {
    		                                int unit = (int)Math.pow(10, part.length());
    		                                v = v + ((year / unit) * unit);
    		                                if(v > year + (unit / 2)) v -= unit;
    		                                /*
    		                                switch(part.length()) {
    		                                    case 2:
    		                                        v = v + ((year / 100) * 100);
    		                                        if(v > year + 50) v -= 100;
    		                                        break;
    		                                    case 3:
    		                                        v = v + ((year / 1000) * 1000);
    		                                        if(v > year + 500) v -= 1000;
    		                                        break;
    		                                }*/
    		                            }
    		                        } catch(NumberFormatException nfe) {
    		                            switch(ph.dateParts[i]) {
    		                                case Calendar.DAY_OF_WEEK: v = getValueFromMap(weekdays, part); break;
    		                                case Calendar.MONTH: v = getValueFromMap(months, part); break;
    		                                default: throw nfe;
    		                            }
    		                        }
                            }
                            cal.set(ph.dateParts[i], v);
                        } else if(ph.dateParts[i] == Calendar.YEAR) {
                        	cal.set(ph.dateParts[i], year);
                        }
                    }
                    //calc hour of day
                    if(hourOfDay >= 0) {
                        switch(amPm) {
                            case Calendar.AM:
                                cal.set(Calendar.HOUR_OF_DAY, hourOfDay % 12);
                                break;
                            case Calendar.PM:
                                cal.set(Calendar.HOUR_OF_DAY, hourOfDay % 12 + 12);
                                break;
                            default:
                                cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                break;
                        }
                    }
                    if(addMS != 0) {
                    	cal.add(Calendar.MILLISECOND, addMS);
                    }
                    return cal;
                }
            } catch(ParseException e) {
                if(firstExc == null) firstExc = e;
            } catch(IllegalArgumentException e) {
                if(firstExc == null) {
                    firstExc = new ParseException(e.getClass().getName() + ": " + e.getMessage(), 1);
                    firstExc.initCause(e);
                }
            }
        }
        if(firstExc != null) throw firstExc;
        return null;
    }
    public void addDatePattern(String regexPattern, int[] dateParts, boolean relative) {
    	addDatePattern(Pattern.compile(regexPattern), dateParts, relative);
    }
    public void addDatePattern(String regexPattern, String flags, int[] dateParts, boolean relative) {
    	addDatePattern(Pattern.compile(regexPattern, RegexUtils.translateFlags(flags)), dateParts, relative);
    }
    public void addDatePattern(Pattern regexPattern, int[] dateParts, boolean relative) {
        PatternHolder ph = new PatternHolder();
        ph.pattern = regexPattern;
        ph.dateParts = dateParts;
        ph.relative = relative;
        patterns.add(ph);
    }
}
