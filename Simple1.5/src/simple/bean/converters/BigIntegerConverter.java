/*
 * BigIntegerConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author  Brian S. Krug
 */
public class BigIntegerConverter extends AbstractNumberConverter<BigInteger> {

    /** Creates new BigIntegerConverter */
    public BigIntegerConverter() {
    }

    @Override
	public BigInteger convert(Object object) throws Exception {
        if(object instanceof Byte[]) {
            Byte[] arr = (Byte[]) object;
            byte[] b = new byte[arr.length];
            for (int i = 0; i < b.length; i++){
                b[i] = (arr[i] == null ? 0 : arr[i].byteValue());
            }
            return convertByteArray(b);
        } else if(object instanceof byte[]) {
            return convertByteArray((byte[])object);
        } else if(object instanceof BigDecimal) {
        	return ((BigDecimal)object).toBigIntegerExact();
        } else if(object instanceof Long) {
        	return BigInteger.valueOf((Long)object);
        } else if(object instanceof Integer) {
        	return BigInteger.valueOf((Integer)object);
        } else if(object instanceof Short) {
        	return BigInteger.valueOf((Short)object);
        } else if(object instanceof Byte) {
        	return BigInteger.valueOf((Byte)object);
        }
        return super.convert(object);
    }

    protected BigInteger convertByteArray(byte[] b) {
        //must chop off all zeros on end of byte array
        int i;
        for(i = b.length - 1; i > 0; i--) if(b[i] != 0) break;
        byte[] nb = new byte[i+1];
        System.arraycopy(b,0,nb,0,nb.length);
        return new java.math.BigInteger(nb);
    }

    public Class<BigInteger> getTargetClass() {
        return BigInteger.class;
    }

    @Override
    protected BigInteger convertLong(long time) {
        return BigInteger.valueOf(time);
    }

    @Override
    protected BigInteger convertString(String s) {
        return new BigInteger(removeTrailingZeros(s.trim()));
    }

}
