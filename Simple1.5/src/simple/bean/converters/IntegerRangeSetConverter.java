/*
 * PropertyConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.util.Collection;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;
import simple.util.IntegerRangeSet;
import simple.util.OptimizedIntegerRangeSet;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class IntegerRangeSetConverter implements Converter<IntegerRangeSet>, Formatter<IntegerRangeSet> {

    /** Creates new PropertyConverter */
    public IntegerRangeSetConverter() {
    }

    public IntegerRangeSet convert(Object object) throws Exception {
        if(object == null) return null;
        IntegerRangeSet irs = new OptimizedIntegerRangeSet();
        if(ConvertUtils.isCollectionType(object.getClass())) {
        	Collection<?> coll = ConvertUtils.asCollection(object);
        	for(Object e : coll) {
        		if(e instanceof Number)
        			irs.add(((Number)e).intValue());
        		else
        			irs.addRanges(ConvertUtils.getString(e, false));
        	}
        } else {
        	irs.addRanges(ConvertUtils.getString(object, false));
        }
        return irs;
    }

    public String format(IntegerRangeSet object) {
        return object.toString();
    }

    public Class<IntegerRangeSet> getTargetClass() {
        return IntegerRangeSet.class;
    }

}
