/*
 * ClassConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.net.InetSocketAddress;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.io.IOUtils;

/**
 *
 * @author  Brian S. Krug
 */
public class InetSocketAddressConverter implements Converter<InetSocketAddress> {
	public InetSocketAddress convert(Object object) throws Exception {
        String s = ConvertUtils.getString(object, false);
        if(s == null) return null;
		return IOUtils.parseInetSocketAddress(s);
    }    
    
	public Class<InetSocketAddress> getTargetClass() {
		return InetSocketAddress.class;
    }

}
