/*
 * Created on Oct 3, 2005
 *
 */
package simple.bean.converters;

import java.util.Map;
import java.util.TreeMap;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.ConverterFactory;
import simple.io.Log;
import simple.util.CaseInsensitiveComparator;

@SuppressWarnings("rawtypes")
public class EnumConverterFactory implements ConverterFactory<Enum> {
    private static final Log log = Log.getLog();
    public EnumConverterFactory() {
        super();
    }

    public <S /*extends Enum*/> Converter<S> createConverter(final Class<S> specificType) throws ConvertException {
        final S[] constants = specificType.getEnumConstants();
        final Map<String, S> nameMap = new TreeMap<String, S>(new CaseInsensitiveComparator<String>());
        for(S s : constants)
            nameMap.put(s.toString(), s);
        if(log.isDebugEnabled()) 
            log.debug("Creating Converter for Enum " + specificType.getName());
        
        return new Converter<S>() {
            public S convert(Object object) throws Exception {
                try {
                    //NOTE: we are making ordinals 1-based (unlike Enum.ordinal())
                    Integer ordinal = ConvertUtils.convert(Integer.class, object);
                    if(ordinal != null && ordinal > 0 && ordinal <= constants.length)
                        return constants[ordinal-1];
                    else
                        log.debug("Invalid ordinal " + ordinal + "; must be in range 1-" + constants.length);
                } catch(ConvertException e) {                    
                    //log.debug("Could not convert " + object + " to an integer; using name");
                }
				String name = ConvertUtils.convert(String.class, object).trim().replaceAll("-|\\s", "_");
                S s = nameMap.get(name);
                if(s == null) log.warn("Could not find constant named '" + name + "' for type " + specificType.getName());
                return s;
            }
            public Class<S> getTargetClass() {
                return specificType;
            }           
        };
    }

    public Class<Enum> getBaseClass() {
        return Enum.class;
    }

}
