package simple.bean.converters;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.sql.Blob;
import java.sql.Clob;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.ReflectionUtils;

public class InputStreamConverter implements Converter<InputStream> {
	public InputStream convert(Object object) throws Exception {
		Method method;
		if(object instanceof Blob) return ((Blob)object).getBinaryStream();
        else if(object instanceof Clob) return ((Clob)object).getAsciiStream();
        else if(object instanceof byte[]) return new ByteArrayInputStream((byte[])object);
        else if(object instanceof String) return new ByteArrayInputStream(ByteArrayConverter.toBytes((String)object));
        else if(object instanceof File) return new FileInputStream((File)object);
		//else if(object instanceof Reader) return new ReaderInputStream((Reader)object); // needs ant
		/*else if(object instanceof ConfigSource) return ((ConfigSource)object).getInputStream();
		else if(object instanceof DataSource) return ((DataSource)object).getInputStream();
		else if(object instanceof InputFile) return ((InputFile)object).getInputStream();
		else if(object instanceof Socket) return ((Socket)object).getInputStream();
		//else if(object instanceof URLConnection) return ((URLConnection)object).getInputStream();
		 */
        else if((method = ReflectionUtils.findMethod(object.getClass(), "getInputStream", ReflectionUtils.ZERO_PARAMS)) != null && getTargetClass().isAssignableFrom(method.getReturnType()))
        	return getTargetClass().cast(method.invoke(object));
        else return new ByteArrayInputStream(ConvertUtils.convert(byte[].class, object));
	}

	public Class<InputStream> getTargetClass() {
		return InputStream.class;
	}
}
