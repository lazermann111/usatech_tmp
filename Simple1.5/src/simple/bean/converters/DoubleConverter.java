/*
 * DoubleConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;


/**
 *
 * @author  Brian S. Krug
 */
public class DoubleConverter extends AbstractNumberConverter<Double> {

    /** Creates new DoubleConverter */
    public DoubleConverter() {
    }   
    
    public Class<Double> getTargetClass() {
        return Double.class;
    }
    @Override
    protected Double convertLong(long time) {
        return (double)time;
    }

    @Override
    protected Double convertNumber(Number number) {
        return number.doubleValue();
    }

    @Override
    protected Double convertString(String s) {
        return new Double(s.trim());
    }   
}
