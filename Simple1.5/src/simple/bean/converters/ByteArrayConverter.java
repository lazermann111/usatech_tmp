/*
 * ByteArrayConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.io.InputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;

import simple.bean.ConvertUtils;
import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 */
public class ByteArrayConverter implements Converter<byte[]> {
	public static byte[] toBytes(String s) {
		if(s == null)
        	return null;
        byte[] bytes = new byte[s.length()];
    	for(int i = 0; i < bytes.length; i++)
    		bytes[i] = (byte) s.charAt(i);
    	return bytes;
	}

    /** Creates new ByteArrayConverter */
    public ByteArrayConverter() {
    }

    public byte[] convert(Object object) throws Exception {
        if(object == null) return null;
        if(object instanceof ByteBuffer) {
        	ByteBuffer bb = (ByteBuffer)object;
        	byte[] b = new byte[bb.remaining()];
        	bb.get(b);
        	return b;
        }
        if(object instanceof BigInteger)
        	return ((BigInteger)object).toByteArray();
        if(object instanceof char[])  {
        	char[] ch = (char[]) object;
        	byte[] b = new byte[ch.length];
        	for (int i = 0; i < b.length; i++){
                b[i] = (byte)ch[i];
            }
        	return b;
        }
        if(object instanceof Character[])  {
        	Character[] ch = (Character[]) object;
        	byte[] b = new byte[ch.length];
        	for (int i = 0; i < b.length; i++) {
                b[i] = ch[i] == null ? 0 : (byte)ch[i].charValue();
            }
        	return b;
        }
		if(object instanceof InputStream) {
			InputStream in = (InputStream) object;
			int size = in.available();
			if(size <= 0)
				size = 512; // guess
			byte[] result = new byte[size];
			int offset = 0;
			int r;
			while((r = in.read(result, offset, result.length - offset)) >= 0) {
				offset += r;
				if(offset >= result.length) {
					int next = in.read();
					if(next < 0)
						return result;
					byte[] tmp = new byte[result.length + 1024];
					System.arraycopy(result, 0, tmp, 0, result.length);
					tmp[offset++] = (byte) next;
					result = tmp;
				}
			}
			if(result.length > offset) {
				byte[] tmp = new byte[offset];
				System.arraycopy(result, 0, tmp, 0, offset);
				result = tmp;
			}
			return result;
		}
        if(object.getClass().isArray()) {
            byte[] b = new byte[java.lang.reflect.Array.getLength(object)];
            for (int i = 0; i < b.length; i++){
                Byte tmp = ConvertUtils.convert(Byte.class, java.lang.reflect.Array.get(object,i));
                b[i] = (tmp == null ? 0 : tmp.byteValue());
            }
            return b;
        }

        return toBytes(object.toString());
    }

    public Class<byte[]> getTargetClass() {
        return byte[].class;
    }

    protected byte[] convertChars(char[] chars) {
    	byte[] b = new byte[chars.length];
    	for (int i = 0; i < b.length; i++){
            b[i] = (byte)chars[i];
        }
    	return b;
    }
}
