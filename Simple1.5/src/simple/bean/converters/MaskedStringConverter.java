/*
 * FormatConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.MaskedString;

/**
 *
 * @author  Brian S. Krug
 */
public class MaskedStringConverter implements Converter<MaskedString> {

    /** Creates new FormatConverter */
    public MaskedStringConverter() {
    }

    public MaskedString convert(Object object) throws Exception {
       if(object == null) return null;
       String s = ConvertUtils.convert(String.class, object);
       return new MaskedString(s);
    }

    public Class<MaskedString> getTargetClass() {
        return MaskedString.class;
    }
}
