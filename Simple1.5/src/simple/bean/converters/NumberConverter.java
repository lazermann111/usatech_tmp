/*
 * NumberConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.math.BigDecimal;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class NumberConverter extends AbstractNumberConverter<Number> {

    /** Creates new NumberConverter */
    public NumberConverter() {
    }

    public Class<Number> getTargetClass() {
        return Number.class;
    }
    @Override
    protected Long convertLong(long time) {
        return time;
    }

    @Override
    protected Number convertNumber(Number number) {
        return number;
    }

    @Override
    protected BigDecimal convertString(String s) {
        return new BigDecimal(s.trim());
    }
}
