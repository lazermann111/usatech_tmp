/*
 * BooleanConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import simple.bean.Converter;
import simple.util.StaticCollection;

/**
 *
 * @author  Brian S. Krug
 */
public class BooleanConverter implements Converter<Boolean> {   
    protected static StaticCollection<String> trueStrings = StaticCollection.create(
            new simple.util.CaseInsensitiveComparator<String>(),
            "true", //this is strict
            "yes", "y", "t", "on" // these are loose
            );
    
    /** Creates new BooleanConverter */
    public BooleanConverter() {
    }
    
    public Boolean convert(Object object) throws Exception {
        if(object == null) return null;
        if(object instanceof Number) return new Boolean(((Number)object).byteValue() != 0);
        String s = object.toString().trim();
        if(s.length() == 0) return null;
        return trueStrings.contains(s);
    }
    
    public Class<Boolean> getTargetClass() {
        return Boolean.class;
    }   
}
