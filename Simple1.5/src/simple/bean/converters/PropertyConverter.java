/*
 * PropertyConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.beans.PropertyDescriptor;
import java.text.ParseException;

import simple.bean.Converter;
import simple.bean.Formatter;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class PropertyConverter implements Converter<PropertyDescriptor>, Formatter<PropertyDescriptor> {

    /** Creates new PropertyConverter */
    public PropertyConverter() {
    }

    public PropertyDescriptor convert(Object object) throws Exception {
        if(object == null) return null;
        String s = object.toString();
        if(s.length() == 0) return null;
        int pos = s.indexOf("@");
        if(pos <= 0) throw new ParseException("Invalid property string: does not contain '@'",s.length());
        return new PropertyDescriptor(s.substring(0,pos),Class.forName(s.substring(pos+1)));
    }    
    
    public String format(PropertyDescriptor object) {
        return object.getName() + "@" + object.getPropertyType().getName();                
    }   
    
    public Class<PropertyDescriptor> getTargetClass() {
        return PropertyDescriptor.class;
    }
    
}
