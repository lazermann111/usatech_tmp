/*
 * LongConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

/**
 *
 * @author  Brian S. Krug
 */
public class LongConverter extends AbstractNumberConverter<Long> {

    /** Creates new LongConverter */
    public LongConverter() {
    }

    public Class<Long> getTargetClass() {
        return Long.class;
    }

    @Override
    protected Long convertLong(long time) {
        return time;
    }

    @Override
    protected Long convertNumber(Number number) {
        return number.longValue();
    }

    @Override
    protected Long convertString(String s) {
        return new Long(removeTrailingZeros(s.trim()));
    }
}
