/*
 * ByteArrayConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.nio.ByteBuffer;

import simple.bean.ConvertUtils;
import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 */
public class ByteBufferConverter implements Converter<ByteBuffer> {

    /** Creates new ByteByteBufferConverter */
    public ByteBufferConverter() {
    }

    public ByteBuffer convert(Object object) throws Exception {
        if(object == null) return null;
        byte[] bytes = ConvertUtils.convert(byte[].class, object);
        return ByteBuffer.wrap(bytes);
    }

    public Class<ByteBuffer> getTargetClass() {
        return ByteBuffer.class;
    }
}
