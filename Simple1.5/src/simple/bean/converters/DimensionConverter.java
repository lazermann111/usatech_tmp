/*
 * PropertyConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.awt.Dimension;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class DimensionConverter implements Converter<Dimension>, Formatter<Dimension> {

    /** Creates new PropertyConverter */
    public DimensionConverter() {
    }

    public Dimension convert(Object object) throws ConvertException {
        int[] o = ConvertUtils.convert(int[].class, object);
        if(o == null) return null;
        if(o.length != 2) throw new ConvertException("Value cannot be converted to an array of length 2", Dimension.class, o);
        return new Dimension(o[0], o[1]);
    }    
    
    public String format(Dimension object) {
        java.awt.Dimension d = object;
        return "" + d.width + ", " + d.height;                
    }   
    
    public Class<Dimension> getTargetClass() {
        return Dimension.class;
    }
    
}
