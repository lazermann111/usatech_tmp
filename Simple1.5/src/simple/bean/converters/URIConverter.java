/*
 * ClassConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.net.URI;

import simple.bean.ConvertUtils;
import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 */
public class URIConverter implements Converter<URI> {
	public URI convert(Object object) throws Exception {
        String s = ConvertUtils.getString(object, false);
        if(s == null) return null;
		return new URI(s);
    }    
    
	public Class<URI> getTargetClass() {
		return URI.class;
    }

}
