/*
 * BooleanConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class CharacterConverter implements Converter<Character> {

    /** Creates new NumberConverter */
    public CharacterConverter() {
    }

    public Character convert(Object object) throws Exception {
        if(object == null) return null;
        if(object instanceof Number) return new Character((char)((Number)object).intValue());
        String s = object.toString().trim();
        if(s.length() == 0) return null;
        return new Character(s.charAt(0));
    }    
    
    public Class<Character> getTargetClass() {
        return Character.class;
    }
    
}
