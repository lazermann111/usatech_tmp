/*
 * ClassConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.net.InetAddress;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.io.IOUtils;

/**
 *
 * @author  Brian S. Krug
 */
public class InetAddressConverter implements Converter<InetAddress> {
	public InetAddress convert(Object object) throws Exception {
        String s = ConvertUtils.getString(object, false);
        if(s == null) return null;
		return IOUtils.parseInetAddress(s);
    }    
    
	public Class<InetAddress> getTargetClass() {
		return InetAddress.class;
    }

}
