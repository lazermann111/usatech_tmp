/*
 * DateConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.util.Calendar;

import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class DateConverter extends CalendarParser implements Converter<java.util.Date> {
    public static java.text.DateFormat dateFormat = java.text.DateFormat.getDateTimeInstance();

    /** Creates new DateConverter */
    public DateConverter() {
    }

    public java.util.Date convert(Object object) throws Exception {
        if(object == null) return null;
        if(object instanceof Number) return new java.util.Date(((Number)object).longValue());
        if(object instanceof java.util.Calendar) return ((java.util.Calendar)object).getTime();
        String s = object.toString().trim();
        if(s.length() == 0) return null;
        Calendar cal = parse(s);
        if(cal != null)
        	return cal.getTime();
        return dateFormat.parse(s);
    }

    public Class<java.util.Date> getTargetClass() {
        return java.util.Date.class;
    }
}
