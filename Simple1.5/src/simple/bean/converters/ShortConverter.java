/*
 * ShortConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

/**
 *
 * @author  Brian S. Krug
 */
public class ShortConverter extends AbstractNumberConverter<Short> {

    /** Creates new ShortConverter */
    public ShortConverter() {
    }

    public Class<Short> getTargetClass() {
        return Short.class;
    }
    @Override
    protected Short convertLong(long time) {
        return (short)time;
    }

    @Override
    protected Short convertNumber(Number number) {
        return number.shortValue();
    }

    @Override
    protected Short convertString(String s) {
        return new Short(removeTrailingZeros(s.trim()));
    }
}
