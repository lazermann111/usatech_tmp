/*
 * ClassConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.util.HashMap;
import java.util.Map;

import simple.bean.Converter;
import simple.bean.Formatter;

/**
 *
 * @author  Brian S. Krug
 */
@SuppressWarnings("rawtypes")
public class ClassConverter implements Converter<Class>, Formatter<Class<?>> {
	protected static final Map<String,Class<?>> primitiveClasses = new HashMap<String, Class<?>>();
	static {
		Class<?>[] primitives = {long.class, int.class, short.class, byte.class, char.class, boolean.class, double.class, float.class, void.class};
		for(Class<?> p : primitives)
			primitiveClasses.put(p.getName(), p);
	}
    /** Creates new ClassConverter */
    public ClassConverter() {
    }

    public Class<?> convert(Object object) throws Exception {
        if(object == null) 
        	return null;
        String s = object.toString().trim();
        if(s.length() == 0) 
        	return null;
        Class<?> cls = primitiveClasses.get(s);
        if(cls != null)
        	return cls;
        return Class.forName(s);
    }    
    
    public String format(Class<?> object) {
        return object.getName();
    }
    
	public Class<Class> getTargetClass() {
		return Class.class;
    }
    
}
