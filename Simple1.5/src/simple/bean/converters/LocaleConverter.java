/*
 * ClassConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.util.Locale;
import java.util.regex.Pattern;

import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 */
public class LocaleConverter implements Converter<Locale> {
    protected static final Pattern splitter = Pattern.compile("[^A-Za-z0-9]+");
    /** Creates new ClassConverter */
    public LocaleConverter() {
    }

    public Locale convert(Object object) throws Exception {
        if(object == null) return null;
        String s = object.toString().trim();
        if(s.length() == 0) return null;
        String[] parts = splitter.split(s, 3);
        switch(parts.length) {
            case 1: return new Locale(parts[0]);
            case 2: return new Locale(parts[0], parts[1]);
            case 3: return new Locale(parts[0], parts[1], parts[2]);
            default: throw new IllegalStateException("Locale string has been split into too many parts!");
        }
    }    
    
    public Class<Locale> getTargetClass() {
        return Locale.class;
    }   
}
