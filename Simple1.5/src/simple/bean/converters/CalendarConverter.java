package simple.bean.converters;

import java.util.Calendar;
import java.util.Date;

import simple.bean.Converter;

public class CalendarConverter extends CalendarParser implements Converter<Calendar> {

	public CalendarConverter() {
	}

	public Calendar convert(Object object) throws Exception {
		if(object instanceof Number) {
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(((Number)object).longValue());
			return cal;
		} else if(object instanceof Date) {
			Calendar cal = Calendar.getInstance();
			cal.setTime((Date)object);
			return cal;
		} else {
			String s = object.toString().trim();
	        if(s.length() == 0) return null;
	        return parse(s);
		}
	}

	public Class<Calendar> getTargetClass() {
		return Calendar.class;
	}
}
