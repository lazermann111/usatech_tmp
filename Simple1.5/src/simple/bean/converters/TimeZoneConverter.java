/*
 * ClassConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.util.TimeZone;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;

/**
 *
 * @author  Brian S. Krug
 */
public class TimeZoneConverter implements Converter<TimeZone>, Formatter<TimeZone> {
    public TimeZone convert(Object object) throws Exception {
        String s = ConvertUtils.getString(object, false);
        if(s == null) return null;
        return TimeZone.getTimeZone(s);
    }    
    
    public Class<TimeZone> getTargetClass() {
        return TimeZone.class;
    }

	public String format(TimeZone object) {
		return object == null ? null : object.getID();
	}   
}
