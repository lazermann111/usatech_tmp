/*
 * IntegerConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

/**
 *
 * @author  Brian S. Krug
 */
public class IntegerConverter extends AbstractNumberConverter<Integer> {

    /** Creates new IntegerConverter */
    public IntegerConverter() {
    }

    public Class<Integer> getTargetClass() {
        return Integer.class;
    }
    @Override
    protected Integer convertLong(long time) {
        return (int)time;
    }

    @Override
    protected Integer convertNumber(Number number) {
        return number.intValue();
    }

    @Override
    protected Integer convertString(String s) {
    	String cleanS = removeTrailingZeros(s.trim());
    	return new Integer(cleanS.startsWith("+") ? cleanS.substring(1) : cleanS);
    }
}
