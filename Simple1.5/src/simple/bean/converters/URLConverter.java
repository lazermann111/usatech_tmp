/*
 * ClassConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.net.URL;

import simple.bean.ConvertUtils;
import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 */
public class URLConverter implements Converter<URL> {
	public URL convert(Object object) throws Exception {
        String s = ConvertUtils.getString(object, false);
        if(s == null) return null;
		return new URL(s);
    }    
    
	public Class<URL> getTargetClass() {
		return URL.class;
    }

}
