/*
 * LongConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 */
public abstract class AbstractNumberConverter<E extends Number> implements Converter<E> {

    /** Creates new LongConverter */
    public AbstractNumberConverter() {
    }

    public E convert(Object object) throws Exception {
        if(object == null) return null;
        if(object instanceof Number) return convertNumber((Number)object);
        if(object instanceof java.util.Date) return convertLong(((java.util.Date)object).getTime());
        if(object instanceof java.util.Calendar) return convertLong(((java.util.Calendar)object).getTimeInMillis());
        Number n = ConvertUtils.enumerate(object);
        if(n != null) return convertNumber(n);
        if(object instanceof Enum<?>) return convertLong(((Enum<?>)object).ordinal() + 1);
        if(object.getClass().isArray()) {
            int len = Array.getLength(object);
            switch(len) {
                case 0: return null;
                case 1: return convert(Array.get(object, 0));
            }
            // otherwise fall through to last resort
        } else if(object instanceof Collection<?>) {
            Collection<?> coll = (Collection<?>)object;
            switch(coll.size()) {
                case 0: return null;
                case 1: return convert(coll.iterator().next());
            }
            // otherwise fall through to last resort
        } else if(object instanceof java.sql.Array) {
            Object arr = ((java.sql.Array)object).getArray();
            int len = Array.getLength(arr);
            switch(len) {
                case 0: return null;
                case 1: return convert(Array.get(arr, 0));
            }
            // otherwise fall through to last resort
        }

        String s = object.toString().trim();
        if(s.length() == 0) return null;
        try {
        	return convertString(s);
        } catch(NumberFormatException e) {
        	throw new ConvertException(e.getMessage(), getTargetClass(), object, e);
        }
    }

    protected E convertLong(long time) {
        return convertNumber(time);
    }

    protected E convertNumber(Number number) {
        return convertString(number.toString());
    }

    protected abstract E convertString(String s) throws NumberFormatException;

    protected static final Pattern trailingZerosPattern = Pattern.compile("(.*)\\.0+");
    protected String removeTrailingZeros(String s) {
    	Matcher m = trailingZerosPattern.matcher(s);
    	if(m.matches()) {
    		return m.group(1);
    	}
    	return s;
    }
}
