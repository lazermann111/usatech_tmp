/*
 * FormatConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.text.Format;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;

/**
 *
 * @author  Brian S. Krug
 */
public class FormatConverter implements Converter<Format>, Formatter<Format> {

    /** Creates new FormatConverter */
    public FormatConverter() {
    }

    public Format convert(Object object) throws Exception {
       if(object == null) return null;
       String s = object.toString().trim();
       if(s.length() == 0) return null;
       return ConvertUtils.getFormat(s);
    }    
    
    public Class<Format> getTargetClass() {
        return Format.class;
    }

    public String format(Format object) {
        return ConvertUtils.getFormatString(object);
    }
    
}
