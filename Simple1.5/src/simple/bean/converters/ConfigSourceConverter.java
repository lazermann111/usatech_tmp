package simple.bean.converters;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.io.ConfigSource;

public class ConfigSourceConverter implements Converter<ConfigSource> {
	public ConfigSource convert(Object object) throws Exception {
		if(object instanceof File) return ConfigSource.createConfigSource((File)object);
        else if(object instanceof InputStream) return ConfigSource.createConfigSource((InputStream)object);
        else if(object instanceof byte[]) return ConfigSource.createConfigSource((byte[])object);
        else if(object instanceof URL) return ConfigSource.createConfigSource((URL)object);
        else {
        	String path = ConvertUtils.convert(String.class, object);
        	if(path == null)
        		return null;
        	return ConfigSource.createConfigSource(path);
        }
	}

	public Class<ConfigSource> getTargetClass() {
		return ConfigSource.class;
	}
}
