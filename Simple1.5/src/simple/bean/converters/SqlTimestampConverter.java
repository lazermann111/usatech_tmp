/*
 * SqlTimestampConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import simple.bean.ConvertUtils;
import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 */
public class SqlTimestampConverter implements Converter<java.sql.Timestamp> {

    /** Creates new SqlTimestampConverter */
    public SqlTimestampConverter() {
    }

    public java.sql.Timestamp convert(Object object) throws Exception {
        if(object == null) return null;
        String s = object.toString().trim();
        if(s.length() == 0) return null;
        if(object instanceof java.util.Date) return new java.sql.Timestamp(((java.util.Date)object).getTime());
        else if(object instanceof java.util.Calendar) return new java.sql.Timestamp(((java.util.Calendar)object).getTimeInMillis());
        //try straight conversion first?
        try {
            return java.sql.Timestamp.valueOf(s);
        } catch(IllegalArgumentException e) {
            java.util.Date dt = ConvertUtils.convert(java.util.Date.class, object);
            return new java.sql.Timestamp(dt.getTime());
        }
    }

    public Class<java.sql.Timestamp> getTargetClass() {
        return java.sql.Timestamp.class;
    }
}
