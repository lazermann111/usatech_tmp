package simple.bean.converters;

import java.util.Calendar;
import java.util.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import simple.bean.Converter;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class LocalDateConverter extends CalendarParser implements Converter<LocalDate> {
	
	public static DateTimeFormatter dateFormat = DateTimeFormatter.ISO_LOCAL_DATE;

	public java.time.LocalDate convert(Object object) throws Exception {
		if (object == null)
			return null;
		if (object instanceof Number)
			return LocalDate.from(new Date(((Number)object).longValue()).toInstant().atZone(ZoneId.systemDefault()));
		if (object instanceof Date)
			return LocalDate.from(((Date)object).toInstant().atZone(ZoneId.systemDefault()));
		if (object instanceof java.util.Calendar)
			return LocalDate.from(((Calendar)object).toInstant().atZone(ZoneId.systemDefault()));
		String s = object.toString().trim();
		if (s.length() == 0)
			return null;
		Calendar cal = parse(s);
		if (cal != null)
			return LocalDate.from(cal.getTime().toInstant().atZone(ZoneId.systemDefault()));
		return LocalDate.parse(s, dateFormat);
	}

	public Class<LocalDate> getTargetClass() {
		return LocalDate.class;
	}
}
