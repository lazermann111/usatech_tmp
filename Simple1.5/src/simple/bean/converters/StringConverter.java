/*
 * StringConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.util.Iterator;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;
import simple.bean.ParsableToString;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class StringConverter implements Converter<String> {

    /** Creates new StringConverter */
    public StringConverter() {
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public String convert(Object object) throws Exception {
		if(object == null)
			return null;
		if(object instanceof char[])
			return new String((char[]) object);
        Formatter f = ConvertUtils.lookupFormatter(object.getClass());
        if(f == null) {
            if(!(object instanceof ParsableToString) && ConvertUtils.isCollectionType(object.getClass())) {
                StringBuilder sb = new StringBuilder();
                for(Iterator<?> iter = ConvertUtils.asCollection(object).iterator(); iter.hasNext(); ) {
                    sb.append(convert(iter.next()));
                    if(iter.hasNext()) sb.append(",");
                }
                return sb.toString();
            } else
                return object.toString();
        }
        else return f.format(object);
    }

    public Class<String> getTargetClass() {
        return String.class;
    }

}
