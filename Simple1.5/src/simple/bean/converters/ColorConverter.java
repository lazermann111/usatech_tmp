/*
 * ColorConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.awt.Color;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;
import simple.text.StringUtils;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class ColorConverter implements Converter<Color>, Formatter<Color> {
    protected static final java.util.Map<String, Color> NAMED_COLORS = new java.util.HashMap<String, Color>();
    static { // load the colors
        java.lang.reflect.Field[] fields = Color.class.getFields();
        for(int i = 0; i < fields.length; i++)
            if(fields[i].getType().equals(Color.class) && java.lang.reflect.Modifier.isStatic(fields[i].getModifiers())) {
                try {
                    Color c = (Color)fields[i].get(null);
                    String name = fields[i].getName();
                    NAMED_COLORS.put(name.toUpperCase(), c);
                } catch(IllegalArgumentException e) {
                } catch (IllegalAccessException e) {
                }
            }
    }

    /** Creates new PropertyConverter */
    public ColorConverter() {
    }

    public Color convert(Object object) throws ConvertException {
        if(object == null) return null;
        String s = ConvertUtils.convert(String.class, object);
        if(s == null || (s=s.trim()).length() == 0) return null;
        if(s.startsWith("#")) { // hex
        	switch(s.length()) {
        		case 4: //handle 3 digit form
        			char[] full = new char[6];
        			full[0] = s.charAt(1);
        			full[2] = s.charAt(2);
        			full[4] = s.charAt(3);
        			full[1] = full[0];
        			full[3] = full[2];
        			full[5] = full[4];
        			return new Color(Integer.parseInt(new String(full),16));
        		case 7: //handle 6 digit form
        			return new Color(Integer.parseInt(s.substring(1),16));
        	}
        } else if(Character.isDigit(s.charAt(0))){
            int[] rgb = ConvertUtils.convert(int[].class, s);
            if(rgb == null) return null;
            if(rgb.length == 1) return new Color(rgb[0]);
            else if(rgb.length == 3) return new Color(rgb[0], rgb[1], rgb[2]);
            else if(rgb.length == 4) return new Color(rgb[0], rgb[1], rgb[2], rgb[3]);
        } else {
            Color color = NAMED_COLORS.get(s.toUpperCase());
            if(color != null) return color;
        }
        throw new ConvertException("Could not convert '" + s + "' to a color", Color.class, s);
    }

    public String format(Color object) {
       return toHex(object);
    }

    public Class<Color> getTargetClass() {
        return Color.class;
    }

    public static String toHex(Color color) {
        String s = Integer.toHexString(0x00FFFFFF & color.getRGB());
        return "#" + StringUtils.pad(s, "0", 6, StringUtils.JUSTIFY_LEFT);
    }

    public static String hsbToRgbHex(Number hue, Number saturation, Number brightness) {
    	return toHex(Color.getHSBColor(hue.floatValue(), saturation.floatValue(), brightness.floatValue()));
    }
}
