/**
 *
 */
package simple.bean.converters;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;


/**<p>
 * A simple Map Converter/Formatter. The key and value of the target Map will be <code>String</code> type.
 * </p>
 * @author Evan, bkrug
 *
 */
@SuppressWarnings("rawtypes")
public class MapConverter implements Converter<Map>, Formatter<Map<?, ?>> {
	protected static final Pattern escaperPattern = Pattern.compile("([,;\\\\])");

	/**
	 * Convert a String to a Map.
	 * The String input must conform to the output of <code>Map.toString()</code>,
	 * which is like "{key1=value1, key2=value2}". The preferable source is the output of
	 * {@link MapConverter#format(Map)}.
	 *
	 * @param object a String representation of Map.
	 * @see simple.bean.Converter#convert(java.lang.Object)
	 */
	public Map<?, ?> convert(Object object) throws ConvertException {
		String s = ConvertUtils.getString(object, false);
		if(s == null)
			return null;
		if((s=s.trim()).length() == 0)
			return Collections.emptyMap();
		if(s.startsWith("{") && s.endsWith("}")) {
			s = s.substring(1, s.length()-1);
		}
		Map<String, String> map = new LinkedHashMap<String, String>();
		parseKeyValuePairs(s, map);
		return map;
	}

	/**
	 * @see simple.bean.Converter#getTargetClass()
	 */
	public Class<Map> getTargetClass() {
		return Map.class;
	}

	/**<p>
	 * This method returns the result like <code>Map.toString()</code>, but
	 * escapes the ',' and '=' in the <strong>key</strong> and <strong>value</strong>,
	 * which used as delimiter for parsing. When parsing, the escaped
	 * characters will be reverted back.
	 * </p><p>
	 * Proper available Formatter will be used to format the keys and values.
	 * </p>
	 * @see simple.bean.Formatter#format(java.lang.Object)
	 */
	public String format(Map<?,?> object) {
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		for (Map.Entry<?, ?> entry : object.entrySet()) {
			String k = ConvertUtils.getStringSafely(entry.getKey());
			if(k != null)
				sb.append(escaperPattern.matcher(k).replaceAll("\\\\$1"));
			sb.append('=');
			String v = ConvertUtils.getStringSafely(entry.getValue());
			if(v != null)
				sb.append(escaperPattern.matcher(v).replaceAll("\\\\$1"));
			sb.append(", ");
		}
		if(sb.length() > 1)
			sb.replace(sb.length() - 2, sb.length(), "}");
		else
			sb.append('}');
		return sb.toString();
	}

	public static void parseKeyValuePairs(String data, Map<String, String> putInto) throws ConvertException {
		int start = 0;
		String key = null;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < data.length(); i++) {
			char ch = data.charAt(i);
			switch(ch) {
				case '\\': // escape next char
					sb.append(data, start, i);
					sb.append(data, i + 1, i + 2);
					start = i + 2;
					i++;
					break;
				case '=':
					if(key != null)
						throw new ConvertException("Found extra '=' at character " + (i + 1), Map.class, data);
					sb.append(data, start, i);
					key = sb.toString();
					sb.setLength(0);
					start = i + 1;
					break;
				case ',':
				case ';':
					sb.append(data, start, i);
					String value;
					if(key == null) {
						key = sb.toString();
						value = null;
					} else {
						value = sb.toString();
					}
					putInto.put(key, value);
					sb.setLength(0);
					start = i + 1;
					key = null;
					break;
				case ' ':
					if(key == null && sb.length() == 0)
						start = i + 1;
					break;
			}
		}
		sb.append(data, start, data.length());
		if(key != null) {
			putInto.put(key, sb.toString());
		} else if(sb.length() > 0) {
			putInto.put(sb.toString(), null);
		}
	}
}
