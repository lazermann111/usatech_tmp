/*
 * IteratorConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import simple.bean.Converter;
import simple.results.Results;
import simple.results.ResultsIterator;
import simple.util.PrimitiveArrayList;

/** Converts an Collection, Map, Array or Results object to an Iterator. That iterator maybe a 
 *  ListIterator depending on the original object.
 *
 * @author  Brian S. Krug
 */
@SuppressWarnings("rawtypes")
public class IteratorConverter implements Converter<Iterator> {
    
    /** Creates new IteratorConverter */
    public IteratorConverter() {
    }
    
    public Iterator<?> convert(Object object) throws Exception {
        if(object == null) {
            return null;
        } else if(object instanceof List) {
            return ((List<?>)object).listIterator();
        } else if(object instanceof Map) {
            return ((Map<?,?>)object).entrySet().iterator();
        } else if(object instanceof Collection) {
            return ((Collection<?>)object).iterator();
        } else if(object instanceof Object[]) {
            return Arrays.asList((Object[])object).listIterator();
        } else if(object instanceof Results) {
            return new ResultsIterator(((Results)object).clone(), 0);
        } else if(object.getClass().isArray()) {
            return new PrimitiveArrayList(object).listIterator();
        } else {
            return Collections.singletonList(object).listIterator();
            //throw new ConvertException("Object cannot be converted to an Iterator");
        }
    }
    
    public Class<Iterator> getTargetClass() {
        return Iterator.class;
    }
    
}
