/*
 * FloatConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.bean.converters;

/**
 *
 * @author  Brian S. Krug
 */
public class FloatConverter extends AbstractNumberConverter<Float> {

    /** Creates new FloatConverter */
    public FloatConverter() {
    }

    public Class<Float> getTargetClass() {
        return Float.class;
    }
    @Override
    protected Float convertLong(long time) {
        return (float)time;
    }

    @Override
    protected Float convertNumber(Number number) {
        return number.floatValue();
    }

    @Override
    protected Float convertString(String s) {
        return new Float(s.trim());
    }         
}
