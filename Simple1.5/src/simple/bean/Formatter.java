/*
 * Formatter.java
 *
 * Created on January 28, 2003, 3:26 PM
 */

package simple.bean;

/**
 * Turns an object into a String representation. Generally, a object implementing
 * this interface will also implement Converter and its format method will be
 * the functional opposite of its convert method.
 * @author Brian S. Krug
 */
public interface Formatter<Source> {
    /** Returns a String representation of the given object
     * @param object The object to format
     * @return A String representation of the object
     */
    public String format(Source object);
}

