/*
 * NestedNameTokenizer.java
 *
 * Created on December 13, 2002, 2:32 PM
 */

package simple.bean;

import java.text.ParseException;
import java.util.List;

/** Tokenizes a String into bean properties using a period to represent a normal
 * property on a bean, brackets to represent an indexed property and parentheses
 * to represent a mapped property. Thus, a String "mybean.myproperty.myindexed[7]"
 * would break into three tokens: a normal property "mybean", another normal
 * property "myproperty", and an indexed property "myindexed" with an index of 7.
 * This class is used by RequestUtils.getAttributeObject to retrieve values of
 * beans from the request.
 * @author Brian S. Krug
 */
public class NestedNameTokenizer {
    public static final char MAPPED_DELIM = '(';
    public static final char MAPPED_DELIM2 = ')';
    public static final char INDEXED_DELIM = '[';
    public static final char INDEXED_DELIM2 = ']';
    public static final char NESTED_DELIM = '.';

    protected String name;
    protected int lastPos = 0;
    protected int pos = 0;
    protected int index = -1;
    protected String key = null;

    public static class Token {
        protected String property;
        protected String key;
        protected int index = -1;
        protected int endPos = 0;
        protected int nextPos = 0;
        /**
         * Returns the index for this token
         */
        public int getIndex() {
            return index;
        }

        /**
         * Returns the key for this token
         */
        public String getKey() {
            return key;
        }

        /**
         * Returns whether this token is an indexed name
         */
        public boolean isIndexed() {
            return index != -1;
        }

        /**
         * Returns whether this token is an mapped name
         */
        public boolean isMapped() {
            return key != null;
        }
        /**
         * Returns the property value of this token
         */
        public String getProperty() {
            return property;
        }

        /** Getter for property endPos.
         * @return Value of property endPos.
         *
         */
        public int getEndPos() {
            return endPos;
        }

        /** Getter for property nextPos.
         * @return Value of property nextPos.
         *
         */
        public int getNextPos() {
            return nextPos;
        }

    }

    /**
     * Creates a new NestedNameTokenizer using the specified String
     */
    public NestedNameTokenizer(String nestedName) {
        name = nestedName;
    }

    /**
     * Returns whether there is another token or not
     */
    public boolean hasNext() {
        return pos < name.length();
    }

	protected int findEndSymbol(String search, int pos, char startSymbol, char endSymbol) {
		for(int depth = 1; pos < search.length(); pos++) {
			char ch = search.charAt(pos);
			if(ch == startSymbol)
				depth++;
			else if(ch == endSymbol) {
				depth--;
				if(depth == 0)
					return pos;
			}
		}
		return -1;
	}
    /**
     * Returns the next token
     */
    public String nextProperty() throws ParseException {
        key = null;
        index = -1;
        int endPos = -1;
        lastPos = pos;
        for(; pos < name.length(); pos++) {
            switch(name.charAt(pos)) {
                case MAPPED_DELIM:
					endPos = findEndSymbol(name, pos + 1, MAPPED_DELIM, MAPPED_DELIM2);
                    if(endPos < 0)
                    	throw new ParseException("Could not find '" + MAPPED_DELIM2 + "' after position " + (pos+1), pos);//endPos = name.length();
                    else key = name.substring(pos+1,endPos);
                    if(name.length() > endPos + 1 && name.charAt(endPos+1) != NESTED_DELIM)
                    	throw new ParseException("Character at position " + (endPos+1) + " must be '" + NESTED_DELIM + "'", (endPos+1));
                    else
                    	endPos++;
                    break;
                case INDEXED_DELIM:
					endPos = findEndSymbol(name, pos + 1, INDEXED_DELIM, INDEXED_DELIM2);
                    if(endPos < 0) throw new ParseException("Could not find '" + INDEXED_DELIM2 + "' after position " + (pos+1), pos);//endPos = name.length();
                    else {
                    	String s = name.substring(pos+1,endPos);
                        try {
                            index = Integer.parseInt(s);
                        } catch(NumberFormatException nfe) {
                            key = s;
                        }
                    }
                    if(name.length() > endPos + 1 && name.charAt(endPos+1) != NESTED_DELIM)
                    	throw new ParseException("Character at position " + (endPos+1) + " must be '" + NESTED_DELIM + "'", (endPos+1));
                    else
                    	endPos++;
                    break;
                case NESTED_DELIM:
                    break;
                default:
                    continue;
            }
            break;
        }
        if(endPos == -1) endPos = pos;
        String s = name.substring(lastPos, pos);
        pos = endPos+1;
        return s;
    }

    /**
     * Returns the part of the String not yet "read" by this tokenizer
     */
    public String getRemaining() {
        return hasNext() ? name.substring(pos) : null;
    }

    /**
     * Returns the part of the String already "read" by this tokenizer,
     * excluding the last read period if any
     */
    public String getConsumed() {
        return pos >= name.length() ? name : name.substring(0, pos-1);
    }

    /**
     * Returns the index for the last retrieved indexed property
     */
    public int getIndex() {
        return index;
    }

    /**
     * Returns the key for the last retrieved mapped property
     */
    public String getKey() {
        return key;
    }

    /**
     * Returns whether the last retrieved property is an indexed name
     */
    public boolean isIndexed() {
        return index != -1;
    }

    /**
     * Returns whether the last retrieved property is an mapped name
     */
    public boolean isMapped() {
        return key != null;
    }

    /**
     * Returns an array of tokens representing the parsed tokens of this object.
     * Parsing begins at the next token.
     * @throws ParseException
     */
    public Token[] tokenize() throws ParseException {
        List<Token> l = new java.util.ArrayList<Token>();
        while(hasNext()) {
            Token tok = new Token();
            tok.property = nextProperty();
            tok.index = index;
            tok.key = key;
            tok.nextPos = pos;
            tok.endPos = lastPos + tok.property.length();
            l.add(tok);
        }
        return l.toArray(new Token[l.size()]);
    }
}