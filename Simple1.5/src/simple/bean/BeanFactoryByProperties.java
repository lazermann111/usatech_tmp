package simple.bean;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import simple.util.concurrent.LockSegmentCache;

/** Caches beans by their properties. This does not handle expiration / cleanup or release of resources
 * @author Brian S. Krug
 *
 */
public class BeanFactoryByProperties {
	//TODO: add expiration ?
	protected static final LockSegmentCache<Map<String,Object>, Object, Exception> beanCache = new LockSegmentCache<Map<String,Object>, Object, Exception>() {
		@Override
		protected Object createValue(Map<String, Object> key, Object... additionalInfo)
				throws Exception {
			Class<?> beanClass = (Class<?>)key.get("class");
			Object bean = beanClass.newInstance();
			ReflectionUtils.populateProperties(bean, key);
			return bean;
		}

		@Override
		protected boolean keyEquals(Map<String, Object> key1, Map<String, Object> key2) {
			return key1.equals(key2);
		}

		@Override
		protected boolean valueEquals(Object value1, Object value2) {
			return value1.equals(value2);
		}
	};

	public static <B> B getInstance(Class<B> beanClass, Map<String,?> properties) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
		Map<String,Object> key = new HashMap<String, Object>();
		key.put("class", beanClass);
		for(Map.Entry<String, ?> entry : properties.entrySet()) {
			try {
				if(ReflectionUtils.findBeanPropertyChain(beanClass, entry.getKey(), true) != null)
					key.put(entry.getKey(), entry.getValue());
			} catch(IntrospectionException e) {
				//ignore
			} catch(ParseException e) {
				//ignore
			}
		}
		try {
			B bean = beanClass.cast(beanCache.getOrCreate(key));
			if(bean == null)
				throw new IllegalStateException("Bean is null and should not be. Cache = " + beanCache);
			return bean;
		} catch(IntrospectionException e) {
			throw e;
		} catch(IllegalAccessException e) {
			throw e;
		} catch(InvocationTargetException e) {
			throw e;
		} catch(InstantiationException e) {
			throw e;
		} catch(ConvertException e) {
			throw e;
		} catch(ParseException e) {
			throw e;
		} catch(Exception e) {
			throw new UndeclaredThrowableException(e);
		}
	}

	public static int size() {
		return beanCache.size();
	}
}
