package simple.bean;

public interface Convertible {
	public <Target> Target convert(Class<Target> toClass) throws ConvertException ;
}
