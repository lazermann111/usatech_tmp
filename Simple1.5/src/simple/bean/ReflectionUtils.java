/*
 * ReflectionUtils.java
 *
 * Created on December 12, 2002, 5:23 PM
 */

package simple.bean;

import java.beans.BeanInfo;
import java.beans.IndexedPropertyDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.beanutils.DynaProperty;

import simple.io.ExtensionFileFilter;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.CollectionUtils;
import simple.util.ConversionList;
import simple.util.DynaBeanMap;
import simple.util.ExcludingMap;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

/** Provides utility methods for retrieving property descriptors and property
 * values from bean classes and objects. Caches the property descriptors for
 * quick access. Nested, indexed, or mapped properties (or combinations of them)
 * can be set or retrieved by using the following notation:<BR>
 * <UL><LI>For nested properties, use a period(.) to separate the property
 * names.</LI>
 * <LI>For indexed properties, use a begin and end brackets([]) to enclose
 * the index.</LI>
 * <LI>For mapped properties, use a begin and end parentheses(()) to enclose
 * the index.</LI>
 * <LI>For complex properties, use a combinations of the three conventions listed
 * above.</LI></UL>
 * <BR>For example, the property <CODE>car.passenger[2].name("first")</CODE>
 * would find the "car" property on the original object, then the passenger
 * property of that car object. The third element (at index 2) of that
 * passenger array or list would be searched for a name property. That name
 * property would be assumed to implement java.util.Map and that Map would be
 * asked for its value at the key, "first". For the getProperty() method that
 * value at the key, "first" would be returned. For the setProperty() method
 * that key would get a new value, the one specified in the method's parameters.
 * @author Brian S. Krug
 */
public class ReflectionUtils {
    private static final Log log = Log.getLog();
    /** An empty array */
    public static final Object[] ZERO_ARGS = new Object[0];
    public static final Class<?>[] ZERO_PARAMS = new Class[0];
    public static final Class<?>[] ANY_ONE_PARAM = new Class<?>[1];
	public static final Class<?>[] INDEXED_AND_ANY_PARAMS = new Class<?>[] { int.class, null };
	public static final Class<?>[] INDEXED_PARAM = new Class<?>[] { int.class };
	public static final Class<?>[] MAPPED_AND_ANY_PARAMS = new Class<?>[] { String.class, null };
	public static final Class<?>[] MAPPED_PARAM = new Class<?>[] { String.class };
	public static final Class<?>[] ARRAY_PARAMS = new Class[] { Object[].class };

    public static interface BeanProperty {
    	public String getName() ;
    	public Object getValue(Object bean) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException ;
    	public void setValue(Object bean, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException;
    	public Class<?> getType() ;
    	public boolean isReadable() ;
    	public boolean isWritable() ;
    }
    public static interface IndexedBeanProperty extends BeanProperty {
    	public Object getIndexedValue(Object bean, int index) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException ;
    	public void setIndexedValue(Object bean, int index, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException;
    	public Class<?> getIndexedType() ;
    	public boolean isIndexedReadable() ;
    	public boolean isIndexedWritable() ;
    }
    public static interface MappedBeanProperty extends BeanProperty {
    	public Object getMappedValue(Object bean, String key) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException ;
    	public void setMappedValue(Object bean, String key, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException;
    	public Class<?> getMappedType() ;
    	public boolean isMappedReadable() ;
    	public boolean isMappedWritable() ;
    }

    protected static class SimpleBeanProperty implements BeanProperty {
    	protected final PropertyDescriptor pd;
    	public SimpleBeanProperty(PropertyDescriptor pd) {
    		this.pd = pd;
    	}
    	public String getName() {
    		return pd.getName();
    	}
    	public Object getValue(Object bean) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		return pd.getReadMethod().invoke(bean, ZERO_ARGS);
    	}
    	public void setValue(Object bean, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		pd.getWriteMethod().invoke(bean, value);
    	}
    	public Class<?> getType() {
    		return pd.getPropertyType();
    	}
		public boolean isReadable() {
			return pd.getReadMethod() != null;
		}
		public boolean isWritable() {
			return pd.getWriteMethod() != null;
		}
    }
    protected static class SimpleIndexedBeanProperty extends SimpleBeanProperty implements IndexedBeanProperty {
    	protected final IndexedPropertyDescriptor ipd;
    	public SimpleIndexedBeanProperty(IndexedPropertyDescriptor ipd) {
    		super(ipd);
    		this.ipd = ipd;
    	}
    	public Object getIndexedValue(Object bean, int index) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		return ipd.getIndexedReadMethod().invoke(bean, index);
    	}
    	public void setIndexedValue(Object bean, int index, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		ipd.getIndexedWriteMethod().invoke(bean, index, value);
    	}
    	public Class<?> getIndexedType() {
    		return ipd.getIndexedPropertyType();
    	}
		public boolean isIndexedReadable() {
			return ipd.getIndexedReadMethod() != null;
		}
		public boolean isIndexedWritable() {
			return ipd.getIndexedWriteMethod() != null;
		}
    }
    protected static class SimpleMappedBeanProperty extends SimpleBeanProperty implements MappedBeanProperty {
    	protected final MappedPropertyDescriptor mpd;
    	public SimpleMappedBeanProperty(MappedPropertyDescriptor mpd) {
    		super(mpd);
    		this.mpd = mpd;
    	}
    	public Object getMappedValue(Object bean, String key) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		return mpd.getMappedReadMethod().invoke(bean, key);
    	}
    	public void setMappedValue(Object bean, String key, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		mpd.getMappedWriteMethod().invoke(bean, key, value);
    	}
    	public Class<?> getMappedType() {
    		return mpd.getMappedPropertyType();
    	}
		public boolean isMappedReadable() {
			return mpd.getMappedReadMethod() != null;
		}
		public boolean isMappedWritable() {
			return mpd.getMappedWriteMethod() != null;
		}
    }
    protected static class FieldBeanProperty implements BeanProperty {
    	protected final Field field;
    	public FieldBeanProperty(Field field) {
    		this.field = field;
    	}
    	public String getName() {
    		return field.getName();
    	}
    	public Object getValue(Object bean) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		return field.get(bean);
    	}
    	public void setValue(Object bean, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		field.set(bean, value);
    	}
    	public Class<?> getType() {
    		return field.getType();
    	}
		public boolean isReadable() {
			return true;
		}
		public boolean isWritable() {
			return !Modifier.isFinal(field.getModifiers());
		}
    }
    public static class WrappedIndexedBeanProperty implements IndexedBeanProperty {
    	protected final BeanProperty bp;
		protected final Class<?> indexedType;

		public WrappedIndexedBeanProperty(BeanProperty bp, Class<?> beanClass) {
    		this.bp = bp;
    		if(!isIndexedType(bp.getType()))
    			throw new IllegalArgumentException("BeanProperty is not of an indexed type");
    		if(bp.getType().isArray())
    			indexedType = bp.getType().getComponentType();
    		else if(bp instanceof SimpleBeanProperty) {
    			PropertyDescriptor pd = ((SimpleBeanProperty)bp).pd;
				Class<?> tmpType = null;
				if(pd.getWriteMethod() != null) {
					List<Method> altMethods = findMethods(beanClass, pd.getWriteMethod().getName(), ANY_ONE_PARAM);
					if(altMethods != null) {
						for(Method altMethod : altMethods) {
							if(altMethod.getParameterTypes().length == 1 && altMethod.getParameterTypes()[0].isArray()) {
								Class<?> compType = altMethod.getParameterTypes()[0].getComponentType();
								if(tmpType == null || compType.isAssignableFrom(tmpType))
									tmpType = compType;
							}
						}
					}
				}
				indexedType = tmpType == null ? Object.class : tmpType;
    		} else
    			indexedType = Object.class;
    	}
    	public Object getIndexedValue(Object bean, int index) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		return getElement(getValue(bean), index);
    	}
    	public void setIndexedValue(Object bean, int index, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		try {
				setElement(getValue(bean), index, value);
			} catch(ConvertException e) {
				throw new IllegalArgumentException(e);
			}
    	}
    	public Class<?> getIndexedType() {
			return indexedType;
    	}
		public boolean isIndexedReadable() {
			return isReadable();
		}
		public boolean isIndexedWritable() {
			return isReadable();
		}
		public String getName() {
			return bp.getName();
		}
		public Class<?> getType() {
			return bp.getType();
		}
		public Object getValue(Object bean) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			return bp.getValue(bean);
		}
		public boolean isReadable() {
			return bp.isReadable();
		}
		public boolean isWritable() {
			return bp.isWritable();
		}
		public void setValue(Object bean, Object value) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			bp.setValue(bean, value);
		}
    }
    public static class WrappedMappedBeanProperty implements MappedBeanProperty {
    	protected final BeanProperty bp;
    	protected final Class<? extends Map<?,?>> mapType;
    	@SuppressWarnings("unchecked")
		public WrappedMappedBeanProperty(BeanProperty bp, Class<?> beanClass) {
    		this.bp = bp;
    		if(!Map.class.isAssignableFrom(bp.getType()))
    			throw new IllegalArgumentException("Property is not of a mapped type");
    		mapType = (Class<? extends Map<?,?>>)bp.getType();
    	}
    	public Object getMappedValue(Object bean, String key) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		Map<?,?> map = mapType.cast(getValue(bean));
    		return (map == null ? null : map.get(key));
    	}
    	@SuppressWarnings({ "unchecked", "rawtypes" })
		public void setMappedValue(Object bean, String key, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		Map map = mapType.cast(getValue(bean));
    		map.put(key, value);
    	}
    	public Class<?> getMappedType() {
    		return Object.class;
    	}
		public boolean isMappedReadable() {
			return isReadable();
		}
		public boolean isMappedWritable() {
			return isReadable();
		}
		public String getName() {
			return bp.getName();
		}
		public Class<?> getType() {
			return bp.getType();
		}
		public Object getValue(Object bean) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			return bp.getValue(bean);
		}
		public boolean isReadable() {
			return bp.isReadable();
		}
		public boolean isWritable() {
			return bp.isWritable();
		}
		public void setValue(Object bean, Object value) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			bp.setValue(bean, value);
		}
    }
    protected static class DynaBeanProperty implements BeanProperty {
    	protected final DynaProperty dp;
    	public DynaBeanProperty(DynaProperty dp) {
			this.dp = dp;
		}
		public String getName() {
			return dp.getName();
		}
		public Class<?> getType() {
			return dp.getType();
		}
		public Object getValue(Object bean) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
			return ((DynaBean)bean).get(getName());
		}
		public void setValue(Object bean, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
			((DynaBean)bean).set(getName(), value);
		}
		public boolean isReadable() {
			return true;
		}
		public boolean isWritable() {
			return true;
		}

		public Class<?> getIndexedType() {
			if(dp.getType().isArray())
    			return dp.getType().getComponentType();
			else if(isIndexedType(dp.getType()))
				return Object.class;
			return null;
		}
		public Object getIndexedValue(Object bean, int index) throws IllegalArgumentException {
			return ((DynaBean)bean).get(getName(), index);
		}
		public void setIndexedValue(Object bean, int index, Object value) throws IllegalArgumentException {
			((DynaBean)bean).set(getName(), index, value);
		}
		public boolean isIndexedReadable() {
			return dp.isIndexed();
		}
		public boolean isIndexedWritable() {
			return dp.isIndexed();
		}

		public Object getMappedValue(Object bean, String key) throws IllegalArgumentException {
			return ((DynaBean)bean).get(getName(), key);
		}
		public void setMappedValue(Object bean, String key, Object value) throws IllegalArgumentException {
			((DynaBean)bean).set(getName(), key, value);
		}
		public Class<?> getMappedType() {
			return dp.isMapped() ? Object.class : null;
		}
		public boolean isMappedReadable() {
			return dp.isMapped();
		}
		public boolean isMappedWritable() {
			return dp.isMapped();
		}
    }

    protected static class IndexedDynaBeanProperty extends DynaBeanProperty implements IndexedBeanProperty {
		public IndexedDynaBeanProperty(DynaProperty dp) {
			super(dp);
		}
    }

    protected static class MappedDynaBeanProperty extends DynaBeanProperty implements MappedBeanProperty {
		public MappedDynaBeanProperty(DynaProperty dp) {
			super(dp);
		}
    }

    protected static class IndexedAndMappedDynaBeanProperty extends DynaBeanProperty implements IndexedBeanProperty, MappedBeanProperty {
		public IndexedAndMappedDynaBeanProperty(DynaProperty dp) {
			super(dp);
		}
    }

    protected static class IndexedTypeBeanProperty implements BeanProperty {
    	protected final int index;
    	protected final Class<?> type;
    	protected final boolean writeable;
    	public IndexedTypeBeanProperty(int index, Class<?> beanClass) {
    		this.index = index;
    		this.type = (beanClass.isArray() ?  beanClass.getComponentType() : Object.class);
    		this.writeable = isIndexedType(beanClass);
    	}
    	public String getName() {
			return String.valueOf(index);
		}
		public Class<?> getType() {
			return type;
		}
		public Object getValue(Object bean) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			return getElement(bean, index);
		}
		public boolean isReadable() {
			return true;
		}
		public boolean isWritable() {
			return writeable;
		}
		public void setValue(Object bean, Object value) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			try {
				setElement(bean, index, value);
			} catch(ConvertException e) {
				throw new IllegalArgumentException(e);
			}
		}
    }

    protected static class MapBeanProperty implements BeanProperty {
    	protected final String key;
    	public MapBeanProperty(String key) {
    		this.key = key;
    	}
    	public String getName() {
			return key;
		}
		public Class<?> getType() {
			return Object.class;
		}
		public Object getValue(Object bean) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			return ((Map<?,?>)bean).get(key);
		}
		public boolean isReadable() {
			return true;
		}
		public boolean isWritable() {
			return true;
		}
		public void setValue(Object bean, Object value) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			CollectionUtils.uncheckedMap((Map<?,?>)bean, String.class, Object.class).put(key, value);
		}
    }

    protected static class UnwrappingIndexedBeanProperty implements BeanProperty {
    	protected final IndexedBeanProperty ibp;
    	protected final int index;
    	protected final String name;
    	public UnwrappingIndexedBeanProperty(IndexedBeanProperty ibp, int index) {
    		this.ibp = ibp;
    		this.index = index;
    		this.name = ibp.getName() + "[" + index + "]";
    	}
    	public String getName() {
    		return name;
    	}
    	public Object getValue(Object bean) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		return ibp.getIndexedValue(bean, index);
    	}
    	public void setValue(Object bean, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		ibp.setIndexedValue(bean, index, value);
    	}
    	public Class<?> getType() {
    		return ibp.getIndexedType();
    	}
		public boolean isReadable() {
			return ibp.isIndexedReadable();
		}
		public boolean isWritable() {
			return ibp.isIndexedWritable();
		}
    }

    protected static class UnwrappingMappedBeanProperty implements BeanProperty {
    	protected final MappedBeanProperty mbp;
    	protected final String key;
    	protected final String name;
    	public UnwrappingMappedBeanProperty(MappedBeanProperty ibp, String key) {
    		this.mbp = ibp;
    		this.key = key;
    		this.name = mbp.getName() + "(" + key + ")";
    	}
    	public String getName() {
    		return name;
    	}
    	public Object getValue(Object bean) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		return mbp.getMappedValue(bean, key);
    	}
    	public void setValue(Object bean, Object value) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    		mbp.setMappedValue(bean, key, value);
    	}
    	public Class<?> getType() {
    		return mbp.getMappedType();
    	}
		public boolean isReadable() {
			return mbp.isMappedReadable();
		}
		public boolean isWritable() {
			return mbp.isMappedWritable();
		}
    }
    protected static final Comparator<Object> propertyComparator = new Comparator<Object>() {
        public int compare(Object o1, Object o2) {
            String s1 = (o1 instanceof PropertyDescriptor ? ((PropertyDescriptor)o1).getName() : String.valueOf(o1));
            String s2 = (o2 instanceof PropertyDescriptor ? ((PropertyDescriptor)o2).getName() : String.valueOf(o2));
            return s1.compareTo(s2);
        }
    };

    public static abstract class MethodComparable implements Comparable<MethodComparable> {
        public abstract boolean isStrict() ;
        public abstract String getName() ;
        public abstract Class<?>[] getParameterTypes() ;
        public abstract boolean isVarArgs() ;
        public int compareTo(MethodComparable mc) {
            int i = getName().compareTo(mc.getName());
            if(i != 0) return i;
            return compareParameters(getParameterTypes(), mc.getParameterTypes(), isVarArgs(), mc.isVarArgs(), isStrict(), mc.isStrict());
            /*
            i = mc.getParameterCount() - getParameterCount();
            if(i > 0) {
            	if(isVarArgs() && getParameterCount() > 0 && getParameterTypes()[getParameterCount()-1].isArray()) {
            		//there's a chance that this could work if last argument is vararg
            		//first check any non-vararg parameters
            		for(int k = 0; k < getParameterCount() - 1; k++) {
                        Class<?> c1 = getParameterTypes()[k];
                        Class<?> c2 = mc.getParameterTypes()[k];
                        if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                        if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                        i = compareTypes(c1, c2, isStrict(), mc.isStrict());
                        if(i != 0) return i;
                    }
            		Class<?> compType = ConvertUtils.convertToWrapperClass(getParameterTypes()[getParameterCount()-1].getComponentType());
            		for(int k = getParameterCount() - 1; k < mc.getParameterCount(); k++) {
            			Class<?> c2 = mc.getParameterTypes()[k];
                        if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                        i = compareTypes(compType, c2, isStrict(), mc.isStrict());
                        if(i != 0) return i;
            		}
            	} else if(mc.isVarArgs() && i == 1) {
            		for(int k = 0; k < getParameterCount() - 1; k++) {
                        Class<?> c1 = getParameterTypes()[k];
                        Class<?> c2 = mc.getParameterTypes()[k];
                        if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                        if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                        i = compareTypes(c1, c2, isStrict(), mc.isStrict());
                        if(i != 0) return i;
                    }
            	} else {
            		return i;
            	}
            } else if(i < 0) {
            	if(mc.isVarArgs() && mc.getParameterCount() > 0 && mc.getParameterTypes()[mc.getParameterCount()-1].isArray()) {
            		//there's a chance that this could work if last argument is vararg
            		// first check any non-vararg parameters
            		for(int k = 0; k < mc.getParameterCount() - 1; k++) {
                        Class<?> c1 = getParameterTypes()[k];
                        Class<?> c2 = mc.getParameterTypes()[k];
                        if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                        if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                        i = compareTypes(c1, c2, isStrict(), mc.isStrict());
                        if(i != 0) return i;
                    }
            		Class<?> compType = ConvertUtils.convertToWrapperClass(mc.getParameterTypes()[mc.getParameterCount()-1].getComponentType());
            		for(int k = mc.getParameterCount() - 1; k < getParameterCount(); k++) {
            			Class<?> c1 = getParameterTypes()[k];
                        if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                        i = compareTypes(c1, compType, isStrict(), mc.isStrict());
                        if(i != 0) return i;
            		}
            	} else if(isVarArgs() && i == -1) {
            		for(int k = 0; k < mc.getParameterCount() - 1; k++) {
                        Class<?> c1 = getParameterTypes()[k];
                        Class<?> c2 = mc.getParameterTypes()[k];
                        if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                        if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                        i = compareTypes(c1, c2, isStrict(), mc.isStrict());
                        if(i != 0) return i;
                    }
            	} else {
            		return i;
            	}
            } else {
	            for(int k = 0; k < getParameterCount(); k++) {
	                Class<?> c1 = getParameterTypes()[k];
	                Class<?> c2 = mc.getParameterTypes()[k];
	                if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
	                if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
	                i = compareTypes(c1, c2, isStrict(), mc.isStrict());
	                if(i != 0) return i;
	            }
            }
            return 0;*/
        }
        protected int getParameterCount() {
            return (getParameterTypes() == null ? 0 : getParameterTypes().length);
        }
    }
    public static class RealMethodComparable extends MethodComparable {
    	//TODO: it's a lot of work but we should consider using soft references here
        protected Method method;
        public RealMethodComparable(Method _method) {
            method = _method;
        }
        @Override
		public String getName() { return method.getName(); }
        @Override
		public Class<?>[] getParameterTypes() { return method.getParameterTypes(); }
        @Override
		public boolean isStrict() { return true; }
        public Method getMethod() { return method; }
		@Override
		public boolean isVarArgs() { return method.isVarArgs(); }
    }
    public static class FakeMethodComparable extends MethodComparable {
        protected String name;
        protected Class<?>[] paramTypes;
        public FakeMethodComparable(String _name, Class<?>[] _paramTypes) {
            name = _name;
            paramTypes = _paramTypes;
        }
        @Override
		public String getName() { return name; }
        @Override
		public Class<?>[] getParameterTypes() { return paramTypes; }
        @Override
		public boolean isStrict() { return false; }
		@Override
		public boolean isVarArgs() { return false; }
    }

    protected static final Cache<Class<?>, Map<String,PropertyDescriptor>, IntrospectionException> propertyCache = new LockSegmentCache<Class<?>, Map<String,PropertyDescriptor>, IntrospectionException>() {
		@Override
		protected Map<String,PropertyDescriptor> createValue(Class<?> key, Object... additionalInfo) throws IntrospectionException {
			return createPropertyDescriptors(key);
		}
		@Override
		protected boolean keyEquals(Class<?> key1, Class<?> key2) {
			return ConvertUtils.areEqual(key1, key2);
		}
		@Override
		protected boolean valueEquals(Map<String,PropertyDescriptor> value1, Map<String,PropertyDescriptor> value2) {
			return ConvertUtils.areEqual(value1, value2);
		}
    };
    protected static final Cache<Class<?>, MethodComparable[], SecurityException> methodCache = new LockSegmentCache<Class<?>, MethodComparable[], SecurityException>(){
		@Override
		protected MethodComparable[] createValue(Class<?> key, Object... additionalInfo) throws SecurityException {
			return createMethodComparables(key);
		}
		@Override
		protected boolean keyEquals(Class<?> key1, Class<?> key2) {
			return ConvertUtils.areEqual(key1, key2);
		}
		@Override
		protected boolean valueEquals(MethodComparable[] value1, MethodComparable[] value2) {
			return ConvertUtils.areEqual(value1, value2);
		}
	};
    protected static final Cache<Class<?>, Map<String,BeanProperty>, IntrospectionException> beanPropertyCache = new LockSegmentCache<Class<?>, Map<String,BeanProperty>, IntrospectionException>(){
		@Override
		protected Map<String, BeanProperty> createValue(Class<?> key, Object... additionalInfo) throws IntrospectionException {
			return createBeanPropertyMap(key);
		}
		@Override
		protected boolean keyEquals(Class<?> key1, Class<?> key2) {
			return ConvertUtils.areEqual(key1, key2);
		}
		@Override
		protected boolean valueEquals(Map<String, BeanProperty> value1,	Map<String, BeanProperty> value2) {
			return ConvertUtils.areEqual(value1, value2);
		}
    };
    protected static final Cache<DynaProperty, DynaBeanProperty, RuntimeException> dynaBeanPropertyCache = new LockSegmentCache<DynaProperty, DynaBeanProperty, RuntimeException>(){
		@Override
		protected DynaBeanProperty createValue(DynaProperty key, Object... additionalInfo)  {
			return createDynaBeanProperty(key);
		}
		@Override
		protected boolean keyEquals(DynaProperty key1, DynaProperty key2) {
			return ConvertUtils.areEqual(key1, key2);
		}
		@Override
		protected boolean valueEquals(DynaBeanProperty value1, DynaBeanProperty value2) {
			return ConvertUtils.areEqual(value1, value2);
		}
    };
    protected static class ClassPair {
        protected final Class<?> startClass;
        protected final Class<?> stopClass;

        public ClassPair(Class<?> startClass, Class<?> stopClass) {
            this.startClass = startClass;
            this.stopClass = stopClass;
        }
        @Override
		public int hashCode() {
            return (startClass == null ? 0 : startClass.hashCode()) + (stopClass == null ? 0 : stopClass.hashCode());
        }
        @Override
		public boolean equals(Object o) {
            ClassPair other = (ClassPair) o;
            return match(startClass, other.startClass) && match(stopClass, other.stopClass);
        }
        private static boolean match(Class<?> c1, Class<?> c2) {
            if(c1 == null) return c2 == null;
            else if(c2 == null) return false;
            return c1.equals(c2);
        }
		public Class<?> getStartClass() {
			return startClass;
		}
		public Class<?> getStopClass() {
			return stopClass;
		}
    }

    public static class BeanPropertyChain implements BeanProperty {
    	protected final List<BeanProperty> beanProperties = new ArrayList<BeanProperty>();
		public String getName() {
			StringBuilder sb = new StringBuilder();
			for(BeanProperty bp : beanProperties) {
				if(sb.length() > 0)
					sb.append('.');
				sb.append(bp.getName());
			}
			return sb.toString();
		}
		protected BeanProperty getLast() {
			return beanProperties.get(beanProperties.size()-1);
		}
		public Class<?> getType() {
			return getLast().getType();
		}
		public Object getValue(Object bean) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			for(BeanProperty bp : beanProperties) {
				bean = bp.getValue(bean);
				if(bean == null) break;
			}
			return bean;
		}
		public boolean isReadable() {
			return getLast().isReadable();
		}
		public boolean isWritable() {
			return getLast().isWritable();
		}
		public void setValue(Object bean, Object value) throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			Iterator<BeanProperty> iter = beanProperties.iterator();
			BeanProperty bp = iter.next();
			while(iter.hasNext()) {
				bean = bp.getValue(bean);
				bp = iter.next();
			}
			bp.setValue(bean, value);
		}
    }
    /** Never create new ReflectionUtils */
    private ReflectionUtils() {
    }

    protected static boolean isIndexedType(Class<?> type) {
    	return type.isArray() || List.class.isAssignableFrom(type) || java.sql.Array.class.isAssignableFrom(type);
    }

    protected static int compareParameters(Class<?>[] paramTypes1, Class<?>[] paramTypes2, boolean varArgs1, boolean varArgs2, boolean strict1, boolean strict2) {
    	int i = paramTypes2.length - paramTypes1.length;
        if(i > 0) {
        	if(varArgs1 && paramTypes1.length > 0 && paramTypes1[paramTypes1.length-1].isArray()) {
        		//there's a chance that this could work if last argument is vararg
        		//first check any non-vararg parameters
        		for(int k = 0; k < paramTypes1.length - 1; k++) {
                    Class<?> c1 = paramTypes1[k];
                    Class<?> c2 = paramTypes2[k];
                    if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                    if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                    i = compareTypes(c1, c2, strict1, strict2);
                    if(i != 0) return i;
                }
        		Class<?> compType = ConvertUtils.convertToWrapperClass(paramTypes1[paramTypes1.length-1].getComponentType());
        		for(int k = paramTypes1.length - 1; k < paramTypes2.length; k++) {
        			Class<?> c2 = paramTypes2[k];
                    if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                    i = compareTypes(compType, c2, strict1, strict2);
                    if(i != 0) return i;
        		}
        	} else if(varArgs2 && i == 1) {
        		for(int k = 0; k < paramTypes1.length - 1; k++) {
                    Class<?> c1 = paramTypes1[k];
                    Class<?> c2 = paramTypes2[k];
                    if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                    if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                    i = compareTypes(c1, c2, strict1, strict2);
                    if(i != 0) return i;
                }
        	} else {
        		return i;
        	}
        } else if(i < 0) {
        	if(varArgs2 && paramTypes2.length > 0 && paramTypes2[paramTypes2.length-1].isArray()) {
        		//there's a chance that this could work if last argument is vararg
        		// first check any non-vararg parameters
        		for(int k = 0; k < paramTypes2.length - 1; k++) {
                    Class<?> c1 = paramTypes1[k];
                    Class<?> c2 = paramTypes2[k];
                    if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                    if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                    i = compareTypes(c1, c2, strict1, strict2);
                    if(i != 0) return i;
                }
        		Class<?> compType = ConvertUtils.convertToWrapperClass(paramTypes2[paramTypes2.length-1].getComponentType());
        		for(int k = paramTypes2.length - 1; k < paramTypes1.length; k++) {
        			Class<?> c1 = paramTypes1[k];
                    if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                    i = compareTypes(c1, compType, strict1, strict2);
                    if(i != 0) return i;
        		}
        	} else if(varArgs1 && i == -1) {
        		for(int k = 0; k < paramTypes2.length - 1; k++) {
                    Class<?> c1 = paramTypes1[k];
                    Class<?> c2 = paramTypes2[k];
                    if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                    if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                    i = compareTypes(c1, c2, strict1, strict2);
                    if(i != 0) return i;
                }
        	} else {
        		return i;
        	}
        } else {
            for(int k = 0; k < paramTypes1.length; k++) {
                Class<?> c1 = paramTypes1[k];
                Class<?> c2 = paramTypes2[k];
                if(c1 != null) c1 = ConvertUtils.convertToWrapperClass(c1);
                if(c2 != null) c2 = ConvertUtils.convertToWrapperClass(c2);
                i = compareTypes(c1, c2, strict1, strict2);
                if(i != 0) {
                	//check for case when one of the methods is var args
                	if(k == paramTypes1.length-1) {
                		if(varArgs1) {
                			Class<?> compType1 = ConvertUtils.convertToWrapperClass(c1.getComponentType());
                    		i = compareTypes(compType1, c2, strict1, strict2);
                        } else if(varArgs2) {
                			Class<?> compType2 = ConvertUtils.convertToWrapperClass(c2.getComponentType());
                    		i = compareTypes(c1, compType2, strict1, strict2);
                		}
                	}
                	return i;
                }
            }
        }
        return 0;
    }
    protected static <C1,C2> int compareTypes(Class<C1> c1, Class<C2> c2, boolean strict1, boolean strict2) {
        if(c1 != null && c2 != null && c1.equals(c2)) return 0;
        if(c1 == c2) return 0;
        if(strict1) {
            if(c1 == null) return -1;
            if(strict2) {
                if(c2 == null) return 1;
                if(c1.isAssignableFrom(c2) && c2.isAssignableFrom(c1)) return 0;
                return c1.getName().compareTo(c2.getName());
            }
            if(c2 == null) return 0;
            if(c1.isAssignableFrom(c2)) return 0;
            if(Number.class.isAssignableFrom(c1)  && Number.class.isAssignableFrom(c2)) return 0;
            return c1.getName().compareTo(c2.getName());

        } else if(strict2) {
            if(c2 == null) return 1;
            if(c1 == null) return 0;
            if(c2.isAssignableFrom(c1)) return 0;
            if(Number.class.isAssignableFrom(c1)  && Number.class.isAssignableFrom(c2)) return 0;
            return c1.getName().compareTo(c2.getName());
        }
        return 0;
    }
    /**
     * Searches for the bean property with the specified property name in
     * the specified class.
     *
     * @param clazz The class to search
     * @param property The property name to look for
     * @return The matching BeanProperty Object or null if not found
     */
    public static BeanProperty findBeanPropertyFor(Object bean, String property, boolean useDynaBean) throws IntrospectionException {
    	if(useDynaBean && bean instanceof DynaBean) {
			DynaProperty dp = ((DynaBean)bean).getDynaClass().getDynaProperty(property);
			if(dp != null)
				return dynaBeanPropertyCache.getOrCreate(dp);
    	} else if(bean instanceof Map<?,?>)
    		return new MapBeanProperty(property);
    	return findBeanProperty(bean.getClass(), property, true);
    }

    protected static DynaBeanProperty createDynaBeanProperty(DynaProperty dp) {
    	if(dp.isIndexed() && dp.isMapped())
    		return new IndexedAndMappedDynaBeanProperty(dp);
    	if(dp.isIndexed())
    		return new IndexedDynaBeanProperty(dp);
    	if(dp.isMapped())
    		return new MappedDynaBeanProperty(dp);
    	return new DynaBeanProperty(dp);
    }

    public static BeanPropertyChain findBeanPropertyChain(Class<?> type, String nestedProperty, boolean useIndexedType) throws IntrospectionException, ParseException {
    	BeanPropertyChain bpc = new BeanPropertyChain();
    	NestedNameTokenizer token = new NestedNameTokenizer(nestedProperty);
        while(token.hasNext() && type != null) {
            String prop = token.nextProperty();
            BeanProperty bp = findBeanProperty(type, prop, useIndexedType);
            if(bp == null) return null;
            if(token.isIndexed()) {
            	IndexedBeanProperty ibp;
            	if(bp instanceof IndexedBeanProperty)
            		ibp = (IndexedBeanProperty) bp;
            	else if(isIndexedType(bp.getType()))
					ibp = new WrappedIndexedBeanProperty(bp, type);
            	else
            		return null;
        		bp = new UnwrappingIndexedBeanProperty(ibp,token.getIndex());
            } else if(token.isMapped()) {
            	MappedBeanProperty mbp;
            	if(bp instanceof MappedBeanProperty)
            		mbp = (MappedBeanProperty) bp;
            	else if(Map.class.isAssignableFrom(bp.getType()))
					mbp = new WrappedMappedBeanProperty(bp, type);
            	else
            		return null;
        		bp = new UnwrappingMappedBeanProperty(mbp,token.getKey());
            }
            bpc.beanProperties.add(bp);
        }
        return bpc;
    }

    /**
     * Searches for the bean property with the specified property name in
     * the specified class.
     *
     * @param clazz The class to search
     * @param property The property name to look for
     * @return The matching BeanProperty Object or null if not found
     */
    public static BeanProperty findBeanProperty(Class<?> clazz, String property) throws IntrospectionException {
    	return findBeanProperty(clazz, property, true);
    }

    /**
     * Searches for the bean property with the specified property name in
     * the specified class.
     *
     * @param clazz The class to search
     * @param property The property name to look for
     * @return The matching BeanProperty Object or null if not found
     */
    public static BeanProperty findBeanProperty(Class<?> clazz, String property, boolean useIndexedType) throws IntrospectionException {
    	Map<String,BeanProperty> bps = beanPropertyCache.getOrCreate(clazz);
    	BeanProperty bp = bps.get(property);
    	if(bp == null && isIndexedType(clazz))
        	try {
        		int index = Integer.parseInt(property);
                bp = new IndexedTypeBeanProperty(index, clazz);
            } catch(NumberFormatException e) {
            } catch(IndexOutOfBoundsException e) {
            }
    	return bp;
    }

    protected static Map<String,BeanProperty> createBeanPropertyMap(Class<?> clazz) throws IntrospectionException {
		Map<String, BeanProperty> map = new TreeMap<String, BeanProperty>();
    	for(PropertyDescriptor pd : getPropertyDescriptorMap(clazz).values()) {
    		BeanProperty bp;
    		if(pd instanceof IndexedPropertyDescriptor)
    			bp = new SimpleIndexedBeanProperty((IndexedPropertyDescriptor)pd);
    		else if(pd instanceof MappedPropertyDescriptor)
    			bp = new SimpleMappedBeanProperty((MappedPropertyDescriptor)pd);
    		else {
    			bp = new SimpleBeanProperty(pd);
	    		if(isIndexedType(bp.getType()))
					bp = new WrappedIndexedBeanProperty(bp, clazz);
	    		if(Map.class.isAssignableFrom(bp.getType()))
					bp = new WrappedMappedBeanProperty(bp, clazz);
    		}
    		map.put(bp.getName(), bp);
    	}
    	for(Field field : clazz.getFields()) {
    		if(!Modifier.isStatic(field.getModifiers()) && !map.containsKey(field.getName())) {
    			BeanProperty bp = new FieldBeanProperty(field);
    			if(isIndexedType(bp.getType()))
					bp = new WrappedIndexedBeanProperty(bp, clazz);
        		if(Map.class.isAssignableFrom(bp.getType()))
					bp = new WrappedMappedBeanProperty(bp, clazz);
        		map.put(bp.getName(), bp);
    		}
    	}
    	return map;
    }

    public static Collection<BeanProperty> getBeanProperties(Class<?> clazz) throws IntrospectionException {
    	return Collections.unmodifiableCollection(beanPropertyCache.getOrCreate(clazz).values());
    }

    /**
     * Searches for the property descriptor with the specified property name in
     * the specified class.
     *
     * @param clazz The class to search
     * @param property The property name to look for
     * @return The matching PropertyDescriptor or null if not found
     */
    public static PropertyDescriptor findPropertyDescriptor(Class<?> clazz, String property) throws IntrospectionException {
    	return propertyCache.getOrCreate(clazz).get(property);
    }

    protected static Map<String, PropertyDescriptor> getPropertyDescriptorMap(Class<?> clazz) throws IntrospectionException {
    	return propertyCache.getOrCreate(clazz);
    }
    /**
     * Retrieves all the property descriptors for the given class and sorts
     * them by property name.
     *
     * @param clazz The class from which to retrieve property descriptors
     * @return An array of PropertyDescriptors for the given class
     */
    public static PropertyDescriptor[] getPropertyDescriptors(Class<?> clazz) throws IntrospectionException {
    	Collection<PropertyDescriptor> pdset = propertyCache.getOrCreate(clazz).values();
    	PropertyDescriptor[] array = pdset.toArray(new PropertyDescriptor[pdset.size()]);
    	Arrays.sort(array, propertyComparator);
        return array;
    }

    protected static PropertyDescriptor[] createPropertyDescriptors(ClassPair key) throws IntrospectionException {
    	BeanInfo bi = Introspector.getBeanInfo(key.getStartClass(), key.getStopClass());
    	PropertyDescriptor[] pds = bi.getPropertyDescriptors();
        // make read & write methods accessible if possible
        for(PropertyDescriptor pd : pds) {
            Method rm = pd.getReadMethod();
            if(rm != null) {
                Method rm1 = findAccessibleMethod(rm);
                if(rm != rm1) pd.setReadMethod(rm1);
            }
            Method wm = pd.getWriteMethod();
            if(wm != null) {
                Method wm1 = findAccessibleMethod(wm);
                if(wm != wm1) pd.setWriteMethod(wm1);
            }
            if(pd instanceof IndexedPropertyDescriptor) {
                IndexedPropertyDescriptor ipd = (IndexedPropertyDescriptor)pd;
                Method irm = ipd.getIndexedReadMethod();
                if(irm != null) {
                    Method irm1 = findAccessibleMethod(irm);
                    if(irm != irm1) ipd.setIndexedReadMethod(irm1);
                }
                Method iwm = ipd.getIndexedWriteMethod();
                if(iwm != null) {
                    Method iwm1 = findAccessibleMethod(iwm);
                    if(iwm != iwm1) ipd.setIndexedWriteMethod(iwm1);
                }
            }
        }

        Arrays.sort(pds,propertyComparator);
        return pds;
    }
    protected static Map<String,PropertyDescriptor> createPropertyDescriptors(Class<?> type) {
		Map<String, PropertyDescriptor> pds = new TreeMap<String, PropertyDescriptor>();
    	Method[] methods = type.getMethods();

		Map<String, Set<Method>> writeMethodArbiter = new HashMap<String, Set<Method>>();
    	// Now analyze each method.
	    for (int i = 0; i < methods.length; i++) {
	        // skip static methods.
			if(Modifier.isStatic(methods[i].getModifiers()))
        		continue;
			String name = methods[i].getName();
	        Class<?>[] argTypes = methods[i].getParameterTypes();
	        Class<?> resultType = methods[i].getReturnType();
	        try {
		        switch(argTypes.length) {
		        	case 0:
		        		if(resultType == boolean.class && name.startsWith("is") && name.length() > 2)
		        			addPropertyGetter(pds, Introspector.decapitalize(name.substring(2)), methods[i]);
		        		else if(name.startsWith("get") && name.length() > 3 && !methods[i].getReturnType().equals(Void.TYPE))
		        			addPropertyGetter(pds, Introspector.decapitalize(name.substring(3)), methods[i]);
		        		break;
		        	case 1:
		        		if(name.startsWith("set") && name.length() > 3)
							addPropertySetter(pds, Introspector.decapitalize(name.substring(3)), methods[i], writeMethodArbiter);
		        		else if(resultType == boolean.class && name.startsWith("is") && name.length() > 2) {
		        			if(argTypes[0] == String.class)
		        				addMappedPropertyGetter(pds, Introspector.decapitalize(name.substring(2)), methods[i]);
		        			else if(argTypes[0] == int.class)
								addIndexedPropertyGetter(pds, Introspector.decapitalize(name.substring(2)), methods[i], writeMethodArbiter);
		        		} else if(name.startsWith("get") && name.length() > 3) {
		        			if(argTypes[0] == String.class)
		        				addMappedPropertyGetter(pds, Introspector.decapitalize(name.substring(3)), methods[i]);
		        			else if(argTypes[0] == int.class)
								addIndexedPropertyGetter(pds, Introspector.decapitalize(name.substring(3)), methods[i], writeMethodArbiter);
		        		}
		        		break;
		        	case 2:
		        		if(name.startsWith("set") && name.length() > 3) {
		        			if(argTypes[0] == String.class)
								addMappedPropertySetter(pds, Introspector.decapitalize(name.substring(3)), methods[i], writeMethodArbiter);
		        			else if(argTypes[0] == int.class)
								addIndexedPropertySetter(pds, Introspector.decapitalize(name.substring(3)), methods[i], writeMethodArbiter);
		        		}
			        	break;
		        }
	        } catch (IntrospectionException ex) {
			    // This happens if a PropertyDescriptor or IndexedPropertyDescriptor
				// constructor finds that the method violates details of the design
			    // pattern, e.g. by having an empty name, or a getter returning
			    // void , or whatever.
				log.debug("Error while processing method: '" + methods[i] + "', type: '" + type + "'", ex);
			 }
	    }
		for(Map.Entry<String, Set<Method>> entry : writeMethodArbiter.entrySet()) {
			PropertyDescriptor pd = pds.get(entry.getKey());
			if(pd instanceof IndexedPropertyDescriptor) {
				IndexedPropertyDescriptor ipd = (IndexedPropertyDescriptor) pd;
				if(ipd.getIndexedReadMethod() != null && (ipd.getIndexedWriteMethod() == null || !ipd.getIndexedReadMethod().getReturnType().equals(ipd.getIndexedWriteMethod().getParameterTypes()[1]))) {
					for(Method m : entry.getValue())
						if(ipd.getIndexedReadMethod().getReturnType().equals(m.getParameterTypes()[1])) {
							try {
								ipd.setIndexedWriteMethod(m);
								break;
							} catch(IntrospectionException e) {
								// This happens if a PropertyDescriptor or IndexedPropertyDescriptor
								// constructor finds that the method violates details of the design
								// pattern, e.g. by having an empty name, or a getter returning
								// void , or whatever.
								log.debug("Error while processing method: '" + m + "', type: '" + type + "'", e);
							}
						}
				}
			} else if(pd instanceof MappedPropertyDescriptor) {
				MappedPropertyDescriptor mpd = (MappedPropertyDescriptor) pd;
				if(mpd.getMappedReadMethod() != null && (mpd.getMappedWriteMethod() == null || !mpd.getMappedReadMethod().getReturnType().equals(mpd.getMappedWriteMethod().getParameterTypes()[1]))) {
					for(Method m : entry.getValue())
						if(mpd.getMappedReadMethod().getReturnType().equals(m.getParameterTypes()[1])) {
							try {
								mpd.setMappedWriteMethod(m);
								break;
							} catch(IntrospectionException e) {
								// This happens if a PropertyDescriptor or IndexedPropertyDescriptor
								// constructor finds that the method violates details of the design
								// pattern, e.g. by having an empty name, or a getter returning
								// void , or whatever.
								log.debug("Error while processing method: '" + m + "', type: '" + type + "'", e);
							}
						}
				}
			} else if(pd.getReadMethod() != null) {
				if(pd.getWriteMethod() == null || !pd.getReadMethod().getReturnType().equals(pd.getWriteMethod().getParameterTypes()[0])) {
					for(Method m : entry.getValue())
						if(pd.getReadMethod().getReturnType().equals(m.getParameterTypes()[0])) {
							try {
								pd.setWriteMethod(m);
								break;
							} catch(IntrospectionException e) {
								// This happens if a PropertyDescriptor or IndexedPropertyDescriptor
								// constructor finds that the method violates details of the design
								// pattern, e.g. by having an empty name, or a getter returning
								// void , or whatever.
								log.debug("Error while processing method: '" + m + "', type: '" + type + "'", e);
							}
						}
				}
			}
		}
	    return pds;
    }
    private static void addPropertyGetter(Map<String,PropertyDescriptor> pds, String propertyName, Method method) throws IntrospectionException {
    	method = findAccessibleMethod(method);
    	PropertyDescriptor pd = pds.get(propertyName);
		if(pd == null || !method.getReturnType().equals(pd.getPropertyType())) {
    		pds.put(propertyName, new PropertyDescriptor(propertyName, method, null));
    	} else {
    		pd.setReadMethod(method);
    	}
    }

	private static void addPropertySetter(Map<String, PropertyDescriptor> pds, String propertyName, Method method, Map<String, Set<Method>> writeMethodArbiter) throws IntrospectionException {
    	method = findAccessibleMethod(method);
    	PropertyDescriptor pd = pds.get(propertyName);
    	if(pd == null) {
    		pds.put(propertyName, new PropertyDescriptor(propertyName, null, method));
		} else if(method.getParameterTypes()[0].equals(pd.getPropertyType())) {
			pd.setWriteMethod(method);
		} else {
			Set<Method> writeMethods = writeMethodArbiter.get(propertyName);
			if(writeMethods == null) {
				writeMethods = new HashSet<Method>();
				writeMethodArbiter.put(propertyName, writeMethods);
			}
			writeMethods.add(method);
    	}
    }

	private static void addIndexedPropertyGetter(Map<String, PropertyDescriptor> pds, String propertyName, Method method, Map<String, Set<Method>> writeMethodArbiter) throws IntrospectionException {
    	method = findAccessibleMethod(method);
    	PropertyDescriptor pd = pds.get(propertyName);
    	if(pd == null) {
    		pds.put(propertyName, new IndexedPropertyDescriptor(propertyName, null, null, method, null));
    	} else if(pd instanceof IndexedPropertyDescriptor) {
			IndexedPropertyDescriptor ipd = (IndexedPropertyDescriptor) pd;
			if(ipd.getIndexedPropertyType().equals(method.getReturnType()))
				ipd.setIndexedReadMethod(method);
			else {
				if(ipd.getIndexedWriteMethod() != null) {
					Set<Method> writeMethods = writeMethodArbiter.get(propertyName);
					if(writeMethods == null) {
						writeMethods = new HashSet<Method>();
						writeMethodArbiter.put(propertyName, writeMethods);
					}
					writeMethods.add(ipd.getIndexedWriteMethod());
				}
				pds.put(propertyName, new IndexedPropertyDescriptor(propertyName, null, null, method, null));
			}
    	} else if(pd instanceof MappedPropertyDescriptor) {
    		// do nothing ???
    	} else {
			Method readMethod = pd.getReadMethod();
			Method writeMethod = pd.getWriteMethod();
			if(readMethod != null && (!readMethod.getReturnType().isArray() || !readMethod.getReturnType().getComponentType().equals(method.getReturnType())))
				readMethod = null;
			if(writeMethod != null && (!writeMethod.getParameterTypes()[0].isArray() || !writeMethod.getParameterTypes()[0].getComponentType().equals(method.getReturnType()))) {
				Set<Method> writeMethods = writeMethodArbiter.get(propertyName);
				if(writeMethods == null) {
					writeMethods = new HashSet<Method>();
					writeMethodArbiter.put(propertyName, writeMethods);
				}
				writeMethods.add(writeMethod);
				writeMethod = null;
			}
			pds.put(propertyName, new IndexedPropertyDescriptor(propertyName, readMethod, writeMethod, method, null));
    	}
    }

	private static void addIndexedPropertySetter(Map<String, PropertyDescriptor> pds, String propertyName, Method method, Map<String, Set<Method>> writeMethodArbiter) throws IntrospectionException {
    	method = findAccessibleMethod(method);
    	PropertyDescriptor pd = pds.get(propertyName);
    	if(pd == null) {
    		pds.put(propertyName, new IndexedPropertyDescriptor(propertyName, null, null, null, method));
    	} else if(pd instanceof IndexedPropertyDescriptor) {
			IndexedPropertyDescriptor ipd = (IndexedPropertyDescriptor) pd;
			if(ipd.getIndexedWriteMethod() == null || ipd.getIndexedPropertyType().equals(method.getParameterTypes()[1]))
				ipd.setIndexedWriteMethod(method);
			else {
				Set<Method> writeMethods = writeMethodArbiter.get(propertyName);
				if(writeMethods == null) {
					writeMethods = new HashSet<Method>();
					writeMethodArbiter.put(propertyName, writeMethods);
				}
				writeMethods.add(method);
			}
    	} else if(pd instanceof MappedPropertyDescriptor) {
    		// do nothing ???
    	} else {
			Method readMethod = pd.getReadMethod();
			Method writeMethod = pd.getWriteMethod();
			if(readMethod != null && (!readMethod.getReturnType().isArray() || !readMethod.getReturnType().getComponentType().equals(method.getParameterTypes()[1])))
				readMethod = null;
			if(writeMethod != null && (!writeMethod.getParameterTypes()[0].isArray() || !writeMethod.getParameterTypes()[0].getComponentType().equals(method.getParameterTypes()[1]))) {
				Set<Method> writeMethods = writeMethodArbiter.get(propertyName);
				if(writeMethods == null) {
					writeMethods = new HashSet<Method>();
					writeMethodArbiter.put(propertyName, writeMethods);
				}
				writeMethods.add(writeMethod);
				writeMethod = null;
			}
			pds.put(propertyName, new IndexedPropertyDescriptor(propertyName, readMethod, writeMethod, null, method));
    	}
    }
    private static void addMappedPropertyGetter(Map<String,PropertyDescriptor> pds, String propertyName, Method method) throws IntrospectionException {
    	method = findAccessibleMethod(method);
    	PropertyDescriptor pd = pds.get(propertyName);
    	if(pd == null) {
    		pds.put(propertyName, new MappedPropertyDescriptor(propertyName, null, null, method, null));
    	} else if(pd instanceof MappedPropertyDescriptor) {
    		((MappedPropertyDescriptor)pd).setMappedReadMethod(method);
    	} else if(pd instanceof IndexedPropertyDescriptor) {
    		pds.put(propertyName, new MappedPropertyDescriptor(propertyName, pd.getReadMethod(), pd.getWriteMethod(), method, null));
    	} else {
    		pds.put(propertyName, new MappedPropertyDescriptor(propertyName, pd.getReadMethod(), pd.getWriteMethod(), method, null));
    	}
    }

	private static void addMappedPropertySetter(Map<String, PropertyDescriptor> pds, String propertyName, Method method, Map<String, Set<Method>> writeMethodArbiter) throws IntrospectionException {
    	method = findAccessibleMethod(method);
    	PropertyDescriptor pd = pds.get(propertyName);
    	if(pd == null) {
    		pds.put(propertyName, new MappedPropertyDescriptor(propertyName, null, null, null, method));
    	} else if(pd instanceof MappedPropertyDescriptor) {
			MappedPropertyDescriptor mpd = (MappedPropertyDescriptor) pd;
			mpd.setMappedWriteMethod(method);
			if(mpd.getMappedWriteMethod() == null)
				mpd.setMappedWriteMethod(method);
			else if(!mpd.getMappedWriteMethod().equals(method)) {
				Set<Method> writeMethods = writeMethodArbiter.get(propertyName);
				if(writeMethods == null) {
					writeMethods = new HashSet<Method>();
					writeMethodArbiter.put(propertyName, writeMethods);
				}
				writeMethods.add(method);
			}
    	} else if(pd instanceof IndexedPropertyDescriptor) {
    		pds.put(propertyName, new MappedPropertyDescriptor(propertyName, pd.getReadMethod(), pd.getWriteMethod(), null, method));
    	} else {
    		pds.put(propertyName, new MappedPropertyDescriptor(propertyName, pd.getReadMethod(), pd.getWriteMethod(), null, method));
    	}
    }

    public static boolean hasSimpleProperty(Object bean, String property, boolean useDynaBean) throws IntrospectionException {
        if(property == null || property.length() == 0) return true;
        if(useDynaBean && bean instanceof DynaBean) {
            return ((DynaBean)bean).getDynaClass().getDynaProperty(property) != null;
        } else if(bean instanceof Map<?,?>) {
            return true; //((Map)bean).containsKey(property);
        } else if(bean != null) {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
        	return (bp != null && bp.isReadable());
        }
        return false;
    }

    public static boolean hasIndexedProperty(Object bean, String property, boolean useDynaBean) throws IntrospectionException {
        if(bean == null) return false;
        if(useDynaBean && bean instanceof DynaBean) {
        	DynaProperty dp = ((DynaBean)bean).getDynaClass().getDynaProperty(property);
            return dp != null && dp.isIndexed();
        } else if(bean instanceof Map<?,?>) {
        	// NOTE: Because calling findBeanPropertyFor() with a bean that is a Map results in a uncached creation of
            // a new MapDynaBean we use early exit to avoid unnecessary object construction
            return false; //((Map)bean).containsKey(property);
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
        	return (bp instanceof IndexedBeanProperty && ((IndexedBeanProperty)bp).isIndexedReadable());
        }
    }

    public static boolean hasMappedProperty(Object bean, String property, String key, boolean useDynaBean) throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        if(bean == null) return false;
        if(useDynaBean && bean instanceof DynaBean) {
        	DynaProperty dp = ((DynaBean)bean).getDynaClass().getDynaProperty(property);
            return dp != null && dp.isMapped();
        } else if(bean instanceof Map<?,?>) {
            return true; //((Map)bean).containsKey(property);
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
        	if(bp instanceof MappedBeanProperty)
        		return ((MappedBeanProperty)bp).isMappedReadable();
        	else if(bp != null && bp.isReadable()) {
        		if(Map.class.isAssignableFrom(bp.getType())) {
        			return true;
        		} else  {
        			bean = bp.getValue(bean);
	        		return hasSimpleProperty(bean, key, useDynaBean);
        		}
        	}
        }
        return false;
    }

    /**
     * Returns whether the specified bean has a certain property.
     *
     * @param bean The object to search for the property
     * @param property The property name of the property for which to search
     * @return Whether the property exists or not
     * @throws ParseException
     */
    public static boolean hasProperty(Object bean, String nestedProperty)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
        return hasProperty(bean, nestedProperty, true);
    }

    /**
     * Returns whether the specified bean has a certain property.
     *
     * @param bean The object to search for the property
     * @param property The property name of the property for which to search
     * @return Whether the property exists or not
     * @throws ParseException
     */
    public static boolean hasProperty(Object bean, String nestedProperty, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
        NestedNameTokenizer token = new NestedNameTokenizer(nestedProperty);
        while(token.hasNext() && bean != null) {
            String prop = token.nextProperty();
            if(!token.hasNext()) {
                if(token.isIndexed()) return hasIndexedProperty(bean, prop, useDynaBean);
                else if(token.isMapped()) return hasMappedProperty(bean, prop, token.getKey(), useDynaBean);
                else return hasSimpleProperty(bean, prop, useDynaBean);
            } else if(token.isIndexed()) bean = getIndexedProperty(bean, prop, token.getIndex(), useDynaBean);
            else if(token.isMapped()) bean = getMappedProperty(bean, prop, token.getKey(), useDynaBean);
            else bean = getSimpleProperty(bean, prop, useDynaBean);
        }
        return true;
    }
    //hasWritableProperty methods
    public static boolean hasWritableSimpleProperty(Object bean, String property, boolean useDynaBean) throws IntrospectionException {
        if(property == null || property.length() == 0) return true;
        if(bean == null)
        	return false;
        if(useDynaBean && bean instanceof DynaBean) {
            return ((DynaBean)bean).getDynaClass().getDynaProperty(property) != null;
        } else if(bean instanceof Map<?,?>) {
            return true; //((Map)bean).containsKey(property);
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
        	return (bp != null && bp.isWritable());
        }
    }

    public static boolean hasWritableIndexedProperty(Object bean, String property, boolean useDynaBean) throws IntrospectionException {
        if(bean == null) return false;
        if(useDynaBean && bean instanceof DynaBean) {
        	DynaProperty dp = ((DynaBean)bean).getDynaClass().getDynaProperty(property);
            return dp != null && dp.isIndexed();
        } else if(bean instanceof Map<?,?>) {
        	// NOTE: Because calling findBeanPropertyFor() with a bean that is a Map results in a uncached creation of
            // a new MapDynaBean we use early exit to avoid unnecessary object construction
            return false; //((Map)bean).containsKey(property);
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
        	return (bp instanceof IndexedBeanProperty && ((IndexedBeanProperty)bp).isIndexedWritable());
        }
    }

    public static boolean hasWritableMappedProperty(Object bean, String property, String key, boolean useDynaBean) throws IntrospectionException, IllegalAccessException, InvocationTargetException {
    	if(bean == null) return false;
        if(useDynaBean && bean instanceof DynaBean) {
        	DynaProperty dp = ((DynaBean)bean).getDynaClass().getDynaProperty(property);
            return dp != null && dp.isMapped();
        } else if(bean instanceof Map<?,?>) {
            return true; //((Map)bean).containsKey(property);
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
        	if(bp instanceof MappedBeanProperty)
        		return ((MappedBeanProperty)bp).isMappedWritable();
        	else if(bp != null && bp.isReadable()) {
        		if(Map.class.isAssignableFrom(bp.getType())) {
        			return true;
        		} else  {
        			bean = bp.getValue(bean);
	        		return hasWritableSimpleProperty(bean, key, useDynaBean);
        		}
        	}
        }
        return false;
    }

    /**
     * Returns whether the specified bean has a certain property.
     *
     * @param bean The object to search for the property
     * @param property The property name of the property for which to search
     * @return Whether the property exists or not
     * @throws ParseException
     */
    public static boolean hasWritableProperty(Object bean, String nestedProperty)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
        return hasWritableProperty(bean, nestedProperty, true);
    }

    /**
     * Returns whether the specified bean has a certain property.
     *
     * @param bean The object to search for the property
     * @param property The property name of the property for which to search
     * @return Whether the property exists or not
     * @throws ParseException
     */
    public static boolean hasWritableProperty(Object bean, String nestedProperty, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
        NestedNameTokenizer token = new NestedNameTokenizer(nestedProperty);
        while(token.hasNext() && bean != null) {
            String prop = token.nextProperty();
            if(!token.hasNext()) {
                if(token.isIndexed()) return hasWritableIndexedProperty(bean, prop, useDynaBean);
                else if(token.isMapped()) return hasWritableMappedProperty(bean, prop, token.getKey(), useDynaBean);
                else return hasWritableSimpleProperty(bean, prop, useDynaBean);
            } else if(token.isIndexed()) bean = getIndexedProperty(bean, prop, token.getIndex(), useDynaBean);
            else if(token.isMapped()) bean = getMappedProperty(bean, prop, token.getKey(), useDynaBean);
            else bean = getSimpleProperty(bean, prop, useDynaBean);
        }
        return true;
    }

    /**
     * Returns the value of the given property on the specified object
     *
     * @param clazz The class to search
     * @param property The property name to look for
     * @return The value of the property or null if property is not found
     */
    public static Object getSimpleProperty(Object bean, String property, boolean useDynaBean) throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        if(property == null || property.length() == 0) return bean;
        if(useDynaBean && bean instanceof DynaBean) {
            return ((DynaBean)bean).get(property);
        } else if(bean instanceof Map<?,?>) {
            return ((Map<?,?>)bean).get(property);
        } else if(bean != null) {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
            if(bp != null && bp.isReadable())
            	try {
            		return bp.getValue(bean);
            	} catch(IndexOutOfBoundsException e) {
            		if(!(bp instanceof IndexedTypeBeanProperty))
            			throw e;
            	}
        }
        return null;
    }

    /**
     * Sets the specified simple property on the given object to the given value.
     *
     * @param bean The object whose property will be set
     * @param property The property name of the property to set
     * @param value The value to which to set the property
     * @return Whether the property was set or not
     */

	public static boolean setSimpleProperty(Object bean, String property, Object value, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ConvertException {
        if(bean == null) {
            return false;
        } else if(useDynaBean && bean instanceof DynaBean) {
            ((DynaBean)bean).set(property, value);
            return true;
        } else if(bean instanceof Map<?,?>) {
        	CollectionUtils.uncheckedMap((Map<?,?>)bean, String.class, Object.class).put(property, value);
            return true;
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
            if(bp == null || !bp.isWritable())
            	return false;
            value = ConvertUtils.convert(bp.getType(), value);
            if(value == null && bp.getType().isPrimitive())
            	return false;
            try {
            	bp.setValue(bean, value);
            } catch(IllegalArgumentException e) {
                if(log.isInfoEnabled())
                    log.info("Value '" + value + "' of class " + (value == null ? "NULL" : value.getClass().getName())
                            + " is wrong type for property '" + property + "' on " + bean, e);
                throw e;
            }
            return true;
        }
    }

    /**
     * Returns the value of the given indexed property on the specified object using
     * the given index
     *
     * @param clazz The class to search
     * @param property The property name to look for
     * @param index The index of the indexed property or of the property array
     * or list
     * @return The value of the property or null if property is not found
     */
    public static Object getIndexedProperty(Object bean, String property, int index, boolean useDynaBean) throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        if(bean == null) return null;
        if(useDynaBean && bean instanceof DynaBean) {
            return ((DynaBean)bean).get(property, index);
        } else if(bean instanceof Map<?,?>) {
            return getElement(((Map<?,?>)bean).get(property), index);
        } else if(property == null) {
        	Collection<?> coll;
			try {
				coll = ConvertUtils.asCollection(bean);
			} catch(ConvertException e) {
				throw new InvocationTargetException(e);
			}
			if(coll.size() <= index)
				return null;
        	if(coll instanceof List<?>) {
        		return ((List<?>)coll).get(index);
        	} else {
        		Iterator<?> iter = coll.iterator();
        		for(int i = 0; i < index; i++)
        			iter.next();
        		return iter.next();
        	}
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
            if(bp instanceof IndexedBeanProperty) {
            	IndexedBeanProperty ibp = (IndexedBeanProperty)bp;
            	if(ibp.isIndexedReadable())
            		try {
            			return ibp.getIndexedValue(bean, index);
    	            } catch(IndexOutOfBoundsException e) {
    	            	return null;
    	            } catch(ClassCastException e) {
    	            	return null;
    	            }
            }
            if(bp != null && bp.isReadable()) {
            	try {
	            	return getElement(bp.getValue(bean), index);
	            } catch(IndexOutOfBoundsException e) {
	            	return null;
	            } catch(ClassCastException e) {
	            	return null;
	            }
            }
        }
        return null;
    }

    /**
     * Sets the specified indexed property on the given object to the given value
     * at the given index.
     *
     * @param bean The object whose property will be set
     * @param property The property name of the property to set
     * @param index The index of the indexed property to set or the index of the
     *              array object at the given property
     * @param value The value to which to set the property
     * @return Whether the property was set or not
     * @throws ConvertException
     */
    public static boolean setIndexedProperty(Object bean, String property, int index, Object value, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ConvertException {
        if(bean == null) return false;
        if(useDynaBean && bean instanceof DynaBean) {
            ((DynaBean)bean).set(property, index, value);
            return true;
        } else if(bean instanceof Map<?,?>) {
        	setElement(((Map<?,?>)bean).get(property), index, value);
            return true;
        } else if(property == null) {
        	Collection<?> coll;
			try {
				coll = ConvertUtils.asCollection(bean);
			} catch(ConvertException e) {
				throw new InvocationTargetException(e);
			}
			if(coll instanceof List<?>) {
        		try {
        			CollectionUtils.uncheckedCollection((List<?>)coll, Object.class).set(index, value);
        		} catch(IndexOutOfBoundsException e) {
	            	return false;
	            } catch(ClassCastException e) {
	            	return false;
	            }
        		return true;
        	} else {
        		return false;
        	}
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
            if(bp instanceof IndexedBeanProperty) {
            	IndexedBeanProperty ibp = (IndexedBeanProperty)bp;
            	if(ibp.isIndexedWritable()) {
            		value = ConvertUtils.convert(ibp.getIndexedType(), value);
                    ibp.setIndexedValue(bean, index, value);
            		return true;
            	}
            }
            if(bp != null && bp.isReadable()) {
            	try {
            		return setElement(bp.getValue(bean), index, value);
	            } catch(IndexOutOfBoundsException e) {
	            	return false;
	            } catch(ClassCastException e) {
	            	return false;
	            }
            }
        }
        return false;
    }

    /**
     * Returns the value of the given mapped property on the specified object using
     * the given key
     *
     * @param clazz The class to search
     * @param property The property name to look for
     * @param key The key of the mapped property or of the property map
     * @return The value of the property or null if property is not found
     */
    public static Object getMappedProperty(Object bean, String property, String key, boolean useDynaBean) throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        if(bean == null) return null;
        if(useDynaBean && bean instanceof DynaBean) {
        	return ((DynaBean)bean).get(property, key);
        } else if(bean instanceof Map<?,?>) {
            return getSimpleProperty(((Map<?,?>)bean).get(property), key, useDynaBean);
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
            if(bp instanceof MappedBeanProperty) {
            	MappedBeanProperty mbp = (MappedBeanProperty)bp;
            	if(mbp.isMappedReadable())
            		return mbp.getMappedValue(bean, key);
            }
            if(bp != null && bp.isReadable()) {
            	return getSimpleProperty(bp.getValue(bean), key, useDynaBean);
            }
        }
        return null;
    }

    /**
     * Sets the specified mapped property on the given object to the given value
     * at the given index.
     *
     * @param bean The object whose property will be set
     * @param property The property name of the property to set
     * @param key The key of the mapped property to set or the key of the
     *              Map object at the given property
     * @param value The value to which to set the property
     * @return Whether the property was set or not
     */
	public static boolean setMappedProperty(Object bean, String property, String key, Object value, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ConvertException {
        if(bean == null) return false;
        if(useDynaBean && bean instanceof DynaBean) {
            ((DynaBean)bean).set(property, key, value);
            return true;
        } else if(bean instanceof Map<?,?>) {
        	return setSimpleProperty(((Map<?,?>)bean).get(property), key, value, useDynaBean);
        } else {
        	BeanProperty bp = findBeanProperty(bean.getClass(), property);
            if(bp instanceof MappedBeanProperty) {
            	MappedBeanProperty mbp = (MappedBeanProperty)bp;
            	if(mbp.isMappedWritable()) {
            		value = ConvertUtils.convert(mbp.getMappedType(), value);
                    mbp.setMappedValue(bean, key, value);
            		return true;
            	}
            }
            if(bp != null && bp.isReadable()) {
            	return setSimpleProperty(bp.getValue(bean), key, value, useDynaBean);
            }
        }
        return false;
    }

	protected static Object getArray(java.sql.Array array) throws IllegalAccessException {
    	try {
			return array.getArray();
		} catch (SQLException e) {
			IllegalAccessException iae = new IllegalAccessException("SQLException while retrieving Array");
			iae.initCause(e);
			throw iae;
		}
    }

    /** Retreives the object at the indicated index in the given bean. The bean must be an array or list type object.
     * @param bean
     * @param index
     * @return
     */
    public static Object getElement(Object bean, int index) {
    	if(bean == null) return null;
        else if(bean.getClass().isArray()) return Array.get(bean,index);
        else if(bean instanceof List<?>) return ((List<?>)bean).get(index);
        else if(bean instanceof java.sql.Array)
			try {
				return Array.get(((java.sql.Array)bean).getArray(),index);
			} catch(SQLException e) {
				throw new ClassCastException("Could not convert object to an array");
			}
		else if(bean instanceof Collection<?>) {
			Collection<?> coll = (Collection<?>)bean;
			if(coll.size() <= index)
				throw new IndexOutOfBoundsException("Index is greater than or equal to the size of the collection");
			Iterator<?> iter = coll.iterator();
			for(int i = 0; i < index; i++) {
				iter.next();
			}
			return iter.next();
		} else if(bean instanceof String) {
        	String s = (String) bean;
        	if(s.trim().length() == 0)
        		return null;
            //assume it's a list
            List<String> list = Arrays.asList(StringUtils.split(s, ConvertUtils.LIST_DELIMS.toCharArray(), true));
            return list.get(index);
        } else if(index == 0) {
        	return bean;
		} else throw new ClassCastException("Could not convert object to a list");
    }

    /** Retreives the object at the indicated index in the given bean. The bean must be an array or list type object.
     * @param bean
     * @param index
     * @return
     * @throws ConvertException
     */
	public static boolean setElement(Object bean, int index, Object value) throws ConvertException {
    	if(bean == null) {
    		return false; //throw new NullPointerException("Cannot set element; Bean is null");
    	} else if(bean.getClass().isArray()) {
    		value = ConvertUtils.convert(bean.getClass().getComponentType(), value);
            Array.set(bean,index, value);
    		return true;
    	} else if(bean instanceof List<?>) {
			List<Object> unchecked = CollectionUtils.uncheckedCollection((List<?>) bean, Object.class);
			if(unchecked.size() == index)
				unchecked.add(value);
			else
				unchecked.set(index, value);
    		return true;
    	} else if(bean instanceof java.sql.Array) {
			try {
				Array.set(((java.sql.Array)bean).getArray(),index, value);
			} catch(SQLException e) {
				throw new ClassCastException("Could not convert object to an array");
			}
			return false;
    	} else {
    		return false; //throw new ClassCastException("Could not convert object to a list");
    	}
    }


    /*
    public static PropertyDescriptor[] findNestedPropertyDescriptors(Class clazz, String nestedProperty) throws IntrospectionException {
        NestedNameTokenizer token = new NestedNameTokenizer(nestedProperty);
        List props = new ArrayList();
        while(token.hasNext()) {
            String prop = token.nextProperty();
            PropertyDescriptor pd = findPropertyDescriptor(clazz, prop);
            if(pd == null)
            props.add(pd);
            clazz = pd.getPropertyType();
        }
        return (PropertyDescriptor[])props.toArray(new PropertyDescriptor[props.size()]);
    }
*/
    /**
     * Returns the value of the given property on the specified object
     *
     * @param clazz The class to search
     * @param property The property name to look for
     * @return The value of the property or null if property is not found
     * @throws ParseException
     */
    public static Object getProperty(Object bean, String nestedProperty)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
        return getProperty(bean, nestedProperty, true);
    }

    /**
     * Returns the value of the given normally non-accessible property on the specified object
     *
     * @param clazz The class to search
     * @param nestedProperty The property name to look for
     * @return The value of the property or null if property is not found
     * @throws ParseException
     */
    public static Object getPrivateProperty(Object bean, String nestedProperty)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
    	NestedNameTokenizer token = new NestedNameTokenizer(nestedProperty);
        while(token.hasNext() && bean != null) {
            String prop = token.nextProperty();
            Object subBean = getSimplePrivateProperty(bean, prop, true);
            if(token.isIndexed()) {
            	if(subBean != null && ConvertUtils.isCollectionType(subBean.getClass()))
            		bean = getElement(subBean, token.getIndex());
            	else
            		bean = null;
            } else if(token.isMapped()) {
            	if(subBean instanceof Map<?,?>)
            		bean = ((Map<?,?>)subBean).get(token.getKey());
            	else
            		bean = getSimplePrivateProperty(subBean, token.getKey(), true);
            } else bean = subBean;
        }
        return bean;
    }

    /**
     * Returns the value of the given private property on the specified object
     *
     * @param clazz The class to search
     * @param property The property name to look for
     * @return The value of the property or null if property is not found
     */
    public static Object getSimplePrivateProperty(final Object bean, String property, boolean useDynaBean) throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        if(property == null || property.length() == 0 || bean == null) return bean;
    	try {
    		Method method = bean.getClass().getDeclaredMethod("get" + StringUtils.capitalizeFirst(property), ZERO_PARAMS);
    		if(method != null) {
    			method.setAccessible(true);
    			return method.invoke(bean, ZERO_ARGS);
    		}
		} catch(NoSuchMethodException e) {
			//Ignore
		}
    	try {
    		final Method method = bean.getClass().getDeclaredMethod("is" + StringUtils.capitalizeFirst(property), ZERO_PARAMS);
    		if(method != null) {
    			method.setAccessible(true);
    			return method.invoke(bean, ZERO_ARGS);
    		}
		} catch(NoSuchMethodException e) {
			//Ignore
		}
    	try {
    		final Field field = bean.getClass().getDeclaredField(property);
    		if(field != null) {
    			field.setAccessible(true);
    			return field.get(bean);
    		}
		} catch(NoSuchFieldException e) {
			//Ignore
		}

		return getSimpleProperty(bean, property, useDynaBean);
    }
    public static Object getSimplePrivateField(final Object bean, String fieldName) throws IllegalAccessException, NoSuchFieldException {
    	if(fieldName == null || fieldName.length() == 0 || bean == null) 
    		return bean;
    	NoSuchFieldException nsfe = null;   	
    	for(Class<?> type = bean.getClass(); type != null; type = type.getSuperclass()) {
    		try {
    			Field field = type.getDeclaredField(fieldName);
    			field.setAccessible(true);
    			return field.get(bean);
    		} catch(NoSuchFieldException e) {
    			if(nsfe == null)
    				nsfe = e;
    		}
    	}
		throw nsfe;
    }
    /**
     * Returns the value of the given property on the specified DynaBean object.
     * Looks for a property that matches the entire nestedProperty value. If this
     * yields null, then it goes back a "token" (of a NestedNameTokenizer) and
     * looks for a property that matches that value. Thus, for a nestedProperty
     * value of "foo.bar.boo.far". First a property of "foo.bar.boo.far" would be sought, then
     * "foo.bar.boo", then "foo.bar" then "foo". If a property cannot be found null is
     * returned
     *
     * @param bean The DynaBean to search
     * @param nestedProperty The property name to look for
     * @return The value of the property or null if property is not found
     * @throws ParseException
     */
    public static Object getDynaProperty(DynaBean bean, String nestedProperty)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
        Object o = null;
        NestedNameTokenizer.Token[] tokens = new NestedNameTokenizer(nestedProperty).tokenize();
        if(tokens.length == 0) return bean.get("");
        for(int i = tokens.length - 1; i >= 0; i--) {
            if(tokens[i].isIndexed()) {
                o = bean.get(nestedProperty.substring(0,tokens[i].getEndPos()), tokens[i].getIndex());
            } else if(tokens[i].isMapped()) {
                o = bean.get(nestedProperty.substring(0,tokens[i].getEndPos()), tokens[i].getKey());
            } else {
               o = bean.get(nestedProperty.substring(0,tokens[i].getEndPos()));
            }
            if(o != null) return i == tokens.length - 1 ? o : getProperty(o, nestedProperty.substring(tokens[i].getNextPos()), true);
        }
        return null;
    }

    /**
     * Returns the value of the given property on the specified Map.
     * Looks for a key-value pair that matches the entire nestedProperty String. If this
     * yields null, then it goes back a "token" (of a NestedNameTokenizer) and
     * looks for a key-value pair that matches that value. Thus, for a nestedProperty
     * value of "foo.bar.boo.far". First a key-value pair of "foo.bar.boo.far" would be sought, then
     * "foo.bar.boo", then "foo.bar" then "foo". If a key-value pair cannot be found, null is
     * returned
     *
     * @param map The Map to search
     * @param nestedProperty The property name to look for
     * @return The value of the key-value pair or null if key-value pair is not found
     * @throws ParseException
     */
    public static Object getMapProperty(Map<?,?> map, String nestedProperty)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
        Object o = null;
        NestedNameTokenizer.Token[] tokens = new NestedNameTokenizer(nestedProperty).tokenize();
        for(int i = tokens.length - 1; i >= 0; i--) {
            o = map.get(nestedProperty.substring(0,tokens[i].getNextPos()-1));
            if(o != null) return i == tokens.length - 1 ? o : getProperty(o, nestedProperty.substring(tokens[i].getNextPos()), true);
            if(tokens[i].isIndexed()) {
                o = map.get(nestedProperty.substring(0,tokens[i].getEndPos()));
                if(o != null) {
                    o = getIndexedProperty(o, null, tokens[i].getIndex(), true);
                    return i == tokens.length - 1 ? o : getProperty(o, nestedProperty.substring(tokens[i].getNextPos()), true);
                }
            } else if(tokens[i].isMapped()) {
                o = map.get(nestedProperty.substring(0,tokens[i].getEndPos()));
                if(o != null) {
					o = getProperty(o, tokens[i].getKey(), true);
                    return i == tokens.length - 1 ? o : getProperty(o, nestedProperty.substring(tokens[i].getNextPos()), true);
                }
            }
        }
        return null;
    }

    /**
     * Returns the value of the given property on the specified object
     *
     * @param bean The object to search
     * @param property The property name to look for
     * @return The value of the property or null if property is not found
     * @throws ParseException
     */
    public static Object getBeanProperty(Object bean, String nestedProperty, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
        NestedNameTokenizer token = new NestedNameTokenizer(nestedProperty);
        while(token.hasNext() && bean != null) {
            String prop = token.nextProperty();
            if(token.isIndexed()) bean = getIndexedProperty(bean, prop, token.getIndex(), useDynaBean);
            else if(token.isMapped()) bean = getMappedProperty(bean, prop, token.getKey(), useDynaBean);
            else bean = getSimpleProperty(bean, prop, useDynaBean);
        }
        return bean;
    }

    /**
     * Returns the value of the given property on the specified object
     *
     * @param bean The object to search
     * @param property The property name to look for
     * @return The value of the property or null if property is not found
     * @throws ParseException
     */
    public static Object getProperty(Object bean, String nestedProperty, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
        if(useDynaBean) {
            if(bean instanceof DynaBean) return getDynaProperty((DynaBean)bean, nestedProperty);
            if(bean instanceof Map<?,?>) return getMapProperty((Map<?,?>)bean, nestedProperty);
        }
        return getBeanProperty(bean, nestedProperty, useDynaBean);
    }

    /**
     * Sets the specified complex property on the given object to the given value
     * at the given index.
     *
     * @param bean The object whose property will be set
     * @param nestedProperty The complex property name of the property to set
     * @param value The value to which to set the property
     * @param create Whether to create new objects for the given property tree
     *               if they are null or not
     * @return Whether the property was set or not
     * @throws ParseException
     */
    public static boolean setProperty(Object bean, String nestedProperty, Object value, boolean create)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        return setProperty(bean, nestedProperty, value, create, true);
    }

    /**
     * Sets the specified complex property on the given DynaBean object to the given value
     *
     * @param bean The DynaBean whose property will be set
     * @param nestedProperty The complex property name of the property to set
     * @param value The value to which to set the property
     * @param create Whether to create new objects for the given property tree
     *               if they are null or not
     * @return Whether the property was set or not
     * @throws ParseException
     */
    public static boolean setDynaProperty(DynaBean bean, String nestedProperty, Object value, boolean create)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        Object o = null;
        NestedNameTokenizer.Token[] tokens = new NestedNameTokenizer(nestedProperty).tokenize();
        for(int i = tokens.length - 2; i >= 0; i--) {
            if(tokens[i].isIndexed()) {
                o = bean.get(nestedProperty.substring(0,tokens[i].getEndPos()), tokens[i].getIndex());
            } else if(tokens[i].isMapped()) {
                o = bean.get(nestedProperty.substring(0,tokens[i].getEndPos()), tokens[i].getKey());
            } else {
               o = bean.get(nestedProperty.substring(0,tokens[i].getEndPos()));
            }
            if(o != null) return setProperty(o, nestedProperty.substring(tokens[i].getNextPos()), value, create, true);
        }
        bean.set(nestedProperty, value);
        return true;
    }

    /**
     * Sets the specified complex property on the given Map to the given value
     *
     * @param map The Map whose value will be set
     * @param nestedProperty The complex property name of the property or key to set
     * @param value The value to which to set the property or key
     * @param create Whether to create new objects for the given property tree
     *               if they are null or not
     * @return Whether the property was set or not
     * @throws ParseException
     */
    public static boolean setMapProperty(Map<String,Object> map, String nestedProperty, Object value, boolean create)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        Object o = null;
        NestedNameTokenizer.Token[] tokens = new NestedNameTokenizer(nestedProperty).tokenize();
        for(int i = tokens.length - 2; i >= 0; i--) {
            o = map.get(nestedProperty.substring(0,tokens[i].getNextPos()-1));
            if(o != null) {
            	if(setProperty(o, nestedProperty.substring(tokens[i].getNextPos()), value, create, true))
            		return true;
            } else if(tokens[i].isIndexed()) {
                o = map.get(nestedProperty.substring(0,tokens[i].getEndPos()));
                if(o != null) {
                    o = getIndexedProperty(o, null, tokens[i].getIndex(), true);
                    if(setProperty(o, nestedProperty.substring(tokens[i].getNextPos()), value, create, true))
                    	return true;
                }
            } else if(tokens[i].isMapped()) {
                o = map.get(nestedProperty.substring(0,tokens[i].getEndPos()));
                if(o != null) {
                    o = getMappedProperty(o, null, tokens[i].getKey(), true);
                    if(setProperty(o, nestedProperty.substring(tokens[i].getNextPos()), value, create, true))
                    	return true;
                }
            }
        }
        map.put(nestedProperty, value);
        return true;
    }

    /**
     * Sets the specified complex property on the given object to the given value
     *
     * @param bean The object whose property will be set
     * @param nestedProperty The complex property name of the property to set
     * @param value The value to which to set the property
     * @param create Whether to create new objects for the given property tree
     *               if they are null or not
     * @return Whether the property was set or not
     * @throws ParseException
     */
    public static boolean setBeanProperty(Object bean, String nestedProperty, Object value, boolean create, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        NestedNameTokenizer token = new NestedNameTokenizer(nestedProperty);
        while(token.hasNext() && bean != null) {
            Object oldBean = bean;
            String prop = token.nextProperty();
            if(!token.hasNext()) {
                if(token.isIndexed()) return setIndexedProperty(bean, prop, token.getIndex(), value, useDynaBean);
                else if(token.isMapped()) return setMappedProperty(bean, prop, token.getKey(), value, useDynaBean);
                else return setSimpleProperty(bean, prop, value, useDynaBean);
            } else if(token.isIndexed()) bean = getIndexedProperty(bean, prop, token.getIndex(), useDynaBean);
            else if(token.isMapped()) bean = getMappedProperty(bean, prop, token.getKey(), useDynaBean);
            else bean = getSimpleProperty(bean, prop, useDynaBean);
            if(bean == null && create && hasWritableProperty(oldBean, prop, useDynaBean)) bean = createProperty(oldBean, prop, useDynaBean);
        }
        return false;
    }

    /**
     * Sets the specified complex property on the given object to the given value
     *
     * @param bean The object whose property will be set
     * @param nestedProperty The complex property name of the property to set
     * @param value The value to which to set the property
     * @param create Whether to create new objects for the given property tree
     *               if they are null or not
     * @return Whether the property was set or not
     * @throws ParseException
     */
	public static boolean setProperty(Object bean, String nestedProperty, Object value, boolean create, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        if(useDynaBean) {
            if(bean instanceof DynaBean) return setDynaProperty((DynaBean)bean, nestedProperty, value, create);
            if(bean instanceof Map<?,?>) return setMapProperty(CollectionUtils.uncheckedMap((Map<?,?>)bean, String.class, Object.class), nestedProperty, value, create);
        }
        return setBeanProperty(bean, nestedProperty, value, create, useDynaBean);
    }

    /** Creates a new instance of the specified class using the given arguments.
     * @param clazz The class from which to create a new instance
     * @param arguments The constructor arguments
     * @return A new instance of the object
     * @throws InstantiationException If the object cannot be created
     * @throws IllegalAccessException If the constructor for the specified class is inaccessible
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     */
    public static <T> T createObject(Class<T> clazz, Object... arguments) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    	if(arguments == null || arguments.length == 0)
    		return clazz.newInstance();
    	Class<?>[] paramTypes = new Class<?>[arguments.length];
    	for(int i = 0; i < paramTypes.length; i++)
    		paramTypes[i] = (arguments[i] == null ? null : arguments[i].getClass());
        for(Constructor<?> cons : clazz.getConstructors()) {
        	if(compareParameters(paramTypes, cons.getParameterTypes(), false, cons.isVarArgs(), false, true) == 0)
    			return clazz.cast(cons.newInstance(arguments));
    	}
        StringBuilder sb = new StringBuilder();
        sb.append("Constructor with specified parameters types (");
        for(Class<?> pt : paramTypes)
        	sb.append(pt == null ? "*" : pt.getName()).append(',');
        sb.setLength(sb.length()-1);
        sb.append(") could not be found for ").append(clazz.getName());
        throw new IllegalArgumentException(sb.toString());
    }

    /** Creates a new instance of the specified class and check's its type against the
     *  provided class.
     * @param clazz The class from which to create a new instance
     * @param checkClazz The class against which to check the new object's type. If null, then no check is performed
     * @return A new instance of the object
     * @throws InstantiationException If the object cannot be created
     * @throws IllegalAccessException If the constructor for the specified class is inaccessible
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     */
    public static <T> T createObject(Class<?> clazz, Class<T> checkClazz, Object... arguments) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Class<? extends T> castedClass;
        try {
    		castedClass = clazz.asSubclass(checkClazz);
        } catch(ClassCastException e) {
        	throw new ClassCastException("Class '" + clazz.getName() + "' cannot be converted to '" + checkClazz.getName() + "'");
        }
        return createObject(castedClass, arguments);
    }

    /** Creates a new instance of the specified class and check's its type against the
     *  provided class.
     * @param clazzName The name of the class from which to create a new instance
     * @param checkClazz The class against which to check the new object's type. If null, then no check is performed
     * @return A new instance of the object
     * @throws InstantiationException If the object cannot be created
     * @throws IllegalAccessException If the constructor for the specified class is inaccessible
     * @throws ClassNotFoundException If the class name is invalid
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     */
    public static <T> T createObject(String clazzName, Class<T> checkClazz, Object... arguments) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException, InvocationTargetException {
        return createObject(Class.forName(clazzName), checkClazz, arguments);
    }

    /** Creates a new Object for the specified property on the given bean. The new Object is
     *  an instance of the property's class. The specified property is then set to the new Object.
     * @param bean
     * @param property
     * @param useDynaBean
     * @throws IntrospectionException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws ConvertException
     * @return The new Object
     */
    public static Object createProperty(Object bean, String property, boolean useDynaBean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException {
        PropertyDescriptor pd = findPropertyDescriptor(bean.getClass(), property);
        if(pd == null) return null;
        Object newBean = pd.getPropertyType().newInstance();
        if(newBean != null) setSimpleProperty(bean, property, newBean, useDynaBean);
        return newBean;
    }

    /** Puts all the values of the properties of the specified bean into a Map and returns that Map object
     * @param bean The bean whose properties are accessed
     * @throws IntrospectionException
     * @return A Map of each property on the bean - the keys are the properties' names, the values are the properties' values
     */
    public static Map<String,?> toPropertyMap(Object bean) throws IntrospectionException {
    	Set<String> exclude = java.util.Collections.emptySet();
        return toPropertyMap(bean, exclude);
    }

    /** Puts all the values of the properties of the specified bean into a Map and returns that Map object
     * @param bean The bean whose properties are accessed
     * @param exclude A Set of property names to exclude from the returned Map object
     * @throws IntrospectionException
     * @return A Map of each property on the bean - the keys are the properties' names, the values are the properties' values
     */
    public static Map<String,?> toPropertyMap(Object bean, Collection<String> exclude) throws IntrospectionException {
        if(bean instanceof DynaBean) {
            return new ExcludingMap<String,Object>(new DynaBeanMap((DynaBean)bean), exclude);
        } else if(bean instanceof Map<?,?>) {
            return new ExcludingMap<String,Object>(CollectionUtils.uncheckedMap((Map<?,?>)bean, String.class, Object.class), exclude);
        } else if(bean != null) {
			Map<String, Object> m = new TreeMap<String, Object>();
            for(PropertyDescriptor pd : getPropertyDescriptorMap(bean.getClass()).values()) {
                if(pd.getReadMethod() != null && !exclude.contains(pd.getName())) try {
                    m.put(pd.getName(), findAccessibleMethod(pd.getReadMethod()).invoke(bean,ZERO_ARGS));
                } catch(IllegalArgumentException e) {
                    m.put(pd.getName(), e);
                } catch (IllegalAccessException e) {
                    m.put(pd.getName(), e);
                } catch (InvocationTargetException e) {
                    m.put(pd.getName(), e);
                }
            }
            return m;
        } else {
            return Collections.emptyMap();
        }
    }


	public static Set<String> getPropertyNames(Object bean) throws IntrospectionException {
        if(bean instanceof DynaBean) {
        	Set<String> set = new java.util.HashSet<String>();
            DynaProperty[] props = ((DynaBean)bean).getDynaClass().getDynaProperties();
            if(props == null) return null;
            for(int i = 0; i < props.length; i++) set.add(props[i].getName());
            return set;
        } else if(bean instanceof Map<?,?>) {
        	Set<String> set = new java.util.HashSet<String>();
            for(Object key : ((Map<?,?>)bean).keySet())
        		if(key instanceof String)
        			set.add((String)key);
            return set;
        } else if(bean != null) {
            return getPropertyDescriptorMap(bean.getClass()).keySet();
        } else {
        	return Collections.emptySet();
        }
    }

	public static Set<String> getPropertyNames(Class<?> beanClass) throws IntrospectionException {
        return getPropertyDescriptorMap(beanClass).keySet();
    }
    /**
     *  Copies the value of properties of the second bean to the first bean
     * @throws ParseException
     */
    public static void copyProperties(Object toBean, Object fromBean) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        if(fromBean != null && toBean != null) {
            Set<String> propNames = getPropertyNames(fromBean);
            if(propNames == null) propNames = getPropertyNames(toBean);
            if(propNames != null) {
                for(String name : propNames) {
                    if(hasProperty(toBean, name))
                        setProperty(toBean, name, getProperty(fromBean, name), true);
                }
            }
        }
    }

    /**
     *  Sets the properties of a bean to the corresponding values in a map
     * @throws ParseException
     */
    public static void populateProperties(Object bean, Map<String,?> values) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        if(bean != null) {
            for(Map.Entry<String,?> e : values.entrySet()) {
                //NOTE: as setProperty does not throw exception if property doesn't exist we don't need to check: if(hasWritableProperty(bean, e.getKey()))
                    setProperty(bean, e.getKey(), e.getValue(), true);
            }
        }
    }

    /**
     *  Sets the properties of a bean to the corresponding values in a map list (as created by simple.xml.MapListXMLLoader)
     *//*
    public static void populatePropertiesFromMapList(Object bean, Map mapList, String prefix, String suffix, String tagKey, String textKey) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException {
        if(bean != null) {
            for(java.util.Iterator iter = mapList.entrySet().iterator(); iter.hasNext(); ) {
                Map.Entry e = (Map.Entry) iter.next();
                String key = (String) e.getKey();
                if(e.getValue() instanceof String || e.getValue() == null) {
                    if(tagKey == key || (tagKey != null && key != null && tagKey.equals(key))) {
                        //nothing
                    } else if(textKey == key|| (textKey != null && key != null && textKey.equals(key))) {
                        //nothing
                    } else if(key != null) { // its an attribute so set the prop on the bean
                        if(hasProperty(bean, key))
                            setProperty(bean, key, e.getValue(), true);
                    }
                } else if(e.getValue() instanceof List) {
                    if(!"*".equals(key)) { //ignore all list
                        //check for property as is
                        if(hasProperty(bean, e.getKey().toString()))
                            setProperty(bean, e.getKey().toString(), e.getValue(), true);
                        // otherwise if list.size = 1 check for property with prefix & suffix stripped from it

                    }
                }
            }
        }
    }//*/

    /**
     *  Sets the public non-static fields of an object to the corresponding values in a map
     */
    public static void populateFields(Object bean, Map<String,?> values) throws IllegalAccessException {
        if(bean != null) {
            java.lang.reflect.Field[] fields = bean.getClass().getFields();
            for(int i = 0; i < fields.length; i++) {
                Object val = values.get(fields[i].getName());
                if(val != null) fields[i].set(bean, val);
            }
        }
    }

    /** Returns the StackTraceElement for the number of frames above the calling method.
     *  Specifying 0 returns the StackTraceElement for the direct calling method of this method.
     *  Returns null if no frame for the given index exists.
     *//*jdk1.4+ only
    public static StackTraceElement getStackFrame(int frameIndex) {
        StackTraceElement[] stack = new Throwable().getStackTrace();
        if(frameIndex+1 < stack.length) return stack[frameIndex+1];
        else return null;
    }//*/

    public static boolean hasConstructor(Class<?> clazz, Class<?>... parameterTypes) {
    	//for now we will just try the constructor
    	try {
			return clazz.getConstructor(parameterTypes) != null;
		} catch(SecurityException e) {
			return false;
		} catch(NoSuchMethodException e) {
			return false;
		}
    }

	public static <C> Constructor<C> findConstructor(Class<C> clazz, Class<?>... paramTypes) {
		for(Constructor<?> constructor : clazz.getConstructors()) {
			if(compareParameters(paramTypes, constructor.getParameterTypes(), false, constructor.isVarArgs(), false, true) == 0)
				try {
					return clazz.getConstructor(constructor.getParameterTypes());
				} catch(SecurityException e) {
					// ignore
				} catch(NoSuchMethodException e) {
					// ignore
				}
		}
		return null;
	}

    public static boolean hasMethod(Class<?> clazz, String methodName, Class<?>... parameterTypes) {
    	return findMethodComparable(clazz, methodName, parameterTypes) instanceof RealMethodComparable;
    }

    /**
     *  Invokes the specified method on the specified class using the given parameters
     */
    public static Object invokeStaticMethod(Class<?> clazz, String methodName, Object... params)
            throws SecurityException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return invokeMethod(null, clazz, methodName, params);
    }

    /**
     *  Invokes the specified method using the instance and parameters given
     */
    public static Object invokeMethod(Object instance, String methodName, Object... params)
            throws SecurityException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return invokeMethod(instance, instance.getClass(), methodName, params);
    }

    /**
     *  Invokes the specified method on the specified class using the instance and parameters given
     */
    protected static Object invokeMethod(Object instance, Class<?> clazz, String methodName, Object[] params)
            throws SecurityException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if(params == null) params = new Object[0];
    	Class<?>[] paramTypes = new Class[params.length];
        for(int i = 0; i < paramTypes.length; i++) paramTypes[i] = (params[i] == null ? null : params[i].getClass());
        MethodComparable mc = findMethodComparable(clazz, methodName, paramTypes);
        if(!(mc instanceof RealMethodComparable)) {
            String msg = "Could not find method '" + methodName + "' in class '" + clazz.getName() + "' with parameters matching [";
            for(int i = 0; i < paramTypes.length; i++) {
                if(i > 0) msg += ", ";
                msg += (paramTypes[i] == null ? "*" : paramTypes[i].getName());
            }
            msg += "]";
            throw new NoSuchMethodException(msg);
        }
        Method method = ((RealMethodComparable)mc).getMethod();
        Class<?>[] realParamTypes = method.getParameterTypes();

        //convert params for varargs if necessary
        if(method.isVarArgs()) {
        	if(paramTypes.length != realParamTypes.length || paramTypes[paramTypes.length-1] == null || !paramTypes[paramTypes.length-1].isArray()) {
        		Object[] newParams = new Object[realParamTypes.length];
        		System.arraycopy(params, 0, newParams, 0, realParamTypes.length-1);
        		Object vararg = Array.newInstance(
        				realParamTypes[realParamTypes.length-1].getComponentType(),
        				paramTypes.length - realParamTypes.length + 1);
        		for(int i = realParamTypes.length - 1; i < params.length; i++)
        			Array.set(vararg, i, params[i]);
        		newParams[realParamTypes.length-1] = vararg;
        		params = newParams;
        	}
        }

        for(int i = 0; i < params.length; i++) {
        	if(params[i] instanceof Number && !realParamTypes[i].isAssignableFrom(params[i].getClass()))
        		try {
        			params[i] = ConvertUtils.convert(realParamTypes[i], params[i]);
        		} catch(ConvertException e) {
        			//will most likely fail on invoke()
        		}
        }
        return findAccessibleMethod(method).invoke(instance, params);
    }

    /**
     * Checks that the given method is from a public class. If not, this method
     * searches the super classes and interfaces of the declared class of the given
     * method for the same method. If it finds that method it returns it. Otherwise,
     * it returns the original method
     * @param method The method for which to find a public method
     * @return A public method on a public class or the original method if such can not be found
     */
    protected static Method findAccessibleMethod(Method method) {
        //log.debug("Checking that Method " + method.getName() + " on class " + method.getDeclaringClass().getName() + " is accessible");
        if(!Modifier.isPublic(method.getDeclaringClass().getModifiers())) {
            //TODO:implement caching
            log.debug("Method " + method.getName() + " on class " + method.getDeclaringClass().getName() + " is not accessible because its declaring class is not public; Looking for public ancestor method...");
            for(Class<?> sup: getSuperTypes(method.getDeclaringClass())) {
                Method m;
                try {
                    m = sup.getMethod(method.getName(), method.getParameterTypes());
                } catch(NoSuchMethodException e) {
                    continue;
                } catch(SecurityException e) {
                    continue;
                }
                if(Modifier.isPublic(m.getDeclaringClass().getModifiers())) {
                    log.debug("Found an accessible method " + m.getName() + " on class " + m.getDeclaringClass());
                    return m;
                }
            }
        }
        return method;
    }

    public static Class<?>[] getSuperTypes(Class<?> base) {
        Collection<Class<?>> types = new LinkedHashSet<Class<?>>();
        addSuperTypes(base, types);
        return types.toArray(new Class[types.size()]);
    }

    protected static void addSuperTypes(Class<?> base, Collection<Class<?>> types) {
        // put in interfaces first
        Class<?>[] interfaces = base.getInterfaces();
        for(Class<?> cls : interfaces) {
            types.add(cls);
            //XXX:Should this be after all primary interfaces are added?
            addSuperTypes(cls, types);
        }
        //now put in superclass
        Class<?> sup = base.getSuperclass();
        if(sup != null) {
            types.add(sup);
            addSuperTypes(sup, types);
        }
    }

	public static Class<?>[] getSubTypes(Class<?> base) {
		return getSubTypes(base, Thread.currentThread().getContextClassLoader());
	}

	public static Class<?>[] getSubTypes(Class<?> base, ClassLoader classLoader) {
		Collection<Class<?>> types = new LinkedHashSet<Class<?>>();
		addSubTypes(base, classLoader, types);
		return types.toArray(new Class[types.size()]);
	}

	public static Class<?>[] getSubTypes(Class<?> base, URL[] urls, ClassLoader classLoader) {
		Collection<Class<?>> types = new LinkedHashSet<Class<?>>();
		addSubTypes(base, urls, classLoader, types);
		return types.toArray(new Class[types.size()]);
	}

	protected static FileFilter CLASS_FILE_FILTER = new ExtensionFileFilter("java classes", "class");

	protected static void addSubTypes(Class<?> base, ClassLoader classLoader, Collection<Class<?>> types) {
		for(; classLoader != null; classLoader = classLoader.getParent())
			if(classLoader instanceof URLClassLoader)
				addSubTypes(base, ((URLClassLoader) classLoader).getURLs(), classLoader, types);
	}

	protected static void addSubTypes(Class<?> base, URL[] urls, ClassLoader classLoader, Collection<Class<?>> types) {
		for(URL url : urls) {
			if(!StringUtils.isBlank(url.getPath())) {
				if("jar".equals(url.getProtocol())) {
					File file;
					try {
						file = new File(URLDecoder.decode(url.getPath(), "UTF-8"));
					} catch(UnsupportedEncodingException e) {
						log.info("Could not decode file name '" + url.getPath() + "'", e);
						continue;
					}
					addSubTypesFromJar(base, file, classLoader, types);
					// find classes in jar entry list
				} else if("file".equals(url.getProtocol())) {
					File file;
					try {
						file = new File(URLDecoder.decode(url.getPath(), "UTF-8"));
					} catch(UnsupportedEncodingException e) {
						log.info("Could not decode file name '" + url.getPath() + "'", e);
						continue;
					}
					if(file.isDirectory() || file.getName().endsWith(".class"))
						addSubTypes(base, file, null, classLoader, types);
					else if(file.getName().endsWith(".jar") || file.getName().endsWith(".zip"))
						addSubTypesFromJar(base, file, classLoader, types);
				}
			}
		}
	}

	protected static void addSubTypesFromJar(Class<?> base, File jarFile, ClassLoader classLoader, Collection<Class<?>> types) {
		JarFile jar;
		try {
			jar = new JarFile(jarFile);
		} catch(IOException e) {
			log.info("Could not load jar '" + jarFile.getPath() + "'", e);
			return;
		}
		try {
			addSubTypes(base, jar, classLoader, types);
		} finally {
			try {
				jar.close();
			} catch(IOException e) {
				// ignore
			}
		}
	}
	protected static void addSubTypes(Class<?> base, JarFile jar, ClassLoader classLoader, Collection<Class<?>> types) {
		Enumeration<JarEntry> entries = jar.entries();
		while(entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();
			if(!entry.isDirectory() && entry.getName().endsWith(".class")) {
				String classname = entry.getName().substring(0, entry.getName().length() - 6).replaceAll("[/\\\\]", ".");
				Class<?> cls;
				try {
					cls = Class.forName(classname, false, classLoader);
				} catch(ClassNotFoundException e) {
					// Ignore
					log.info("Could not load class '" + classname + "' with " + classLoader, e);
					continue;
				} catch(LinkageError e) {
					// Ignore
					log.info("Could not load class '" + classname + "' with " + classLoader, e);
					continue;
				} catch(SecurityException e) {
					// Ignore
					log.info("Could not load class '" + classname + "' with " + classLoader, e);
					continue;
				}
				if(base.isAssignableFrom(cls))
					types.add(cls);
			}
		}
	}

	protected static void addSubTypes(Class<?> base, File dir, String packagePrefix, ClassLoader classLoader, Collection<Class<?>> types) {
		File[] files = dir.listFiles(CLASS_FILE_FILTER);
		if(files != null)
			for(File file : files) {
				if(file.isDirectory()) {
					StringBuilder sb = new StringBuilder();
					if(!StringUtils.isBlank(packagePrefix))
						sb.append(packagePrefix);
					sb.append(file.getName()).append('.');
					addSubTypes(base, file, sb.toString(), classLoader, types);
				} else {
					String suffix = file.getName().substring(0, file.getName().length() - 6);
					String classname = (!StringUtils.isBlank(packagePrefix) ? new StringBuilder().append(packagePrefix).append(suffix).toString() : suffix);
					Class<?> cls;
					try {
						cls = Class.forName(classname, false, classLoader);
					} catch(ClassNotFoundException e) {
						// Ignore
						log.info("Could not load class '" + classname + "' with " + classLoader, e);
						continue;
					} catch(LinkageError e) {
						// Ignore
						log.info("Could not load class '" + classname + "' with " + classLoader, e);
						continue;
					} catch(SecurityException e) {
						// Ignore
						log.info("Could not load class '" + classname + "' with " + classLoader, e);
						continue;
					}
					if(base.isAssignableFrom(cls))
						types.add(cls);
				}
			}
	}

    /**
     * Searches for the MethodComparable with the specified method name and parameter types in
     * the specified class.
     *
     * @param clazz The class to search
     * @param methodName The method name to look for
     * @param paramTypes The array of parameter types of the method. If an element in the array
     * is null it will match <strong>any</strong parameter type at that index
     * @return The matching MethodComparable or null if not found
     */
    public static MethodComparable findMethodComparable(Class<?> clazz, String methodName, Class<?>[] paramTypes) throws SecurityException {
        MethodComparable[] mcs = getMethodComparables(clazz);
        int p = Arrays.binarySearch(mcs, new FakeMethodComparable(methodName, paramTypes));
        if(p < 0) return null;
        return mcs[p];
    }

    /**
	 * Searches for the MethodComparable with the specified method name and parameter types in
	 * the specified class.
	 * 
	 * @param clazz
	 *            The class to search
	 * @param methodName
	 *            The method name to look for
	 * @param paramTypes
	 *            The array of parameter types of the method. If an element in the array
	 *            is null it will match <strong>any</strong parameter type at that index
	 * @return The matching MethodComparable or null if not found
	 */
	public static List<MethodComparable> findMethodComparables(Class<?> clazz, String methodName, Class<?>[] paramTypes) throws SecurityException {
		MethodComparable[] mcs = getMethodComparables(clazz);
		FakeMethodComparable find = new FakeMethodComparable(methodName, paramTypes);
		int p = Arrays.binarySearch(mcs, find);
		if(p < 0)
			return null;
		int end = p + 1;
		for(; end < mcs.length; end++) {
			if(mcs[end].compareTo(find) != 0)
				break;
		}
		for(; p > 0; p--) {
			if(mcs[p - 1].compareTo(find) != 0)
				break;
		}
		return Arrays.asList(mcs).subList(p, end);
	}

	/**
	 * Searches for the MethodComparable with the specified method name and parameter types in
	 * the specified class.
	 * 
	 * @param clazz
	 *            The class to search
	 * @param methodName
	 *            The method name to look for
	 * @param paramTypes
	 *            The array of parameter types of the method. If an element in the array
	 *            is null it will match <strong>any</strong> parameter type at that index
	 * @return The matching MethodComparable or null if not found
	 */
    public static Method findMethod(Class<?> clazz, String methodName, Class<?>[] paramTypes) throws SecurityException {
        MethodComparable mc = findMethodComparable(clazz, methodName, paramTypes);
        if(mc instanceof RealMethodComparable)
        	return ((RealMethodComparable)mc).getMethod();
        return null;
    }
    
	/**
	 * Searches for the MethodComparable with the specified method name and parameter types in
	 * the specified class.
	 * 
	 * @param clazz
	 *            The class to search
	 * @param methodName
	 *            The method name to look for
	 * @param paramTypes
	 *            The array of parameter types of the method. If an element in the array
	 *            is null it will match <strong>any</strong> parameter type at that index
	 * @return The matching MethodComparable or null if not found
	 */
	public static List<Method> findMethods(Class<?> clazz, String methodName, Class<?>[] paramTypes) throws SecurityException {
		List<MethodComparable> mcs = findMethodComparables(clazz, methodName, paramTypes);
		if(mcs == null)
			return null;

		return new ConversionList<MethodComparable, Method>(mcs) {
			@Override
			protected MethodComparable convertTo(Method object1) {
				return object1 == null ? null : new RealMethodComparable(object1);
			}

			@Override
			protected Method convertFrom(MethodComparable object0) {
				if(object0 instanceof RealMethodComparable)
					return ((RealMethodComparable) object0).getMethod();
				return null;
			}

			@Override
			protected boolean isReversible() {
				return true;
			}
		};
	}

	/**
	 * Retrieves all the MethodComparables for the given class and sorts
	 * them appropriately.
	 * 
	 * @param clazz
	 *            The class from which to retrieve property descriptors
	 * @return An array of MethodComparable for the given class
	 */
    public static MethodComparable[] getMethodComparables(Class<?> clazz) throws SecurityException {
        return methodCache.getOrCreate(clazz);
    }

    protected static MethodComparable[] createMethodComparables(Class<?> clazz) throws SecurityException {
        Method[] methods = clazz.getMethods(); // XXX: Using this instead of getDeclaredMethods() to avoid IllegalAccessExceptions later
        MethodComparable[] mcs = new MethodComparable[methods.length];
        for(int i = 0; i < methods.length; i++) mcs[i] = new RealMethodComparable(findAccessibleMethod(methods[i]));
        Arrays.sort(mcs);
        return mcs;
    }

    public static StackTraceElement getPrevCallerStackElement(Object o) {
        return getPrevCallerStackElement(o.getClass(),
                /*jdk14 logging uses new throwable() not "Thread.currentThread()"*/
                new Throwable().getStackTrace());
    }

    public static StackTraceElement getPrevCallerStackElement(Class<?> clazz) {
        return getPrevCallerStackElement(clazz,
                /*jdk14 logging uses new throwable() not "Thread.currentThread()"*/
                new Throwable().getStackTrace());
    }

    public static StackTraceElement getPrevCallerStackElement(String ofClassName) {
        return getPrevCallerStackElement(ofClassName,
                /*jdk14 logging uses new throwable() not "Thread.currentThread()"*/
                new Throwable().getStackTrace());
    }

    public static StackTraceElement getOutOfPackageStackElement(String ofPackageName) {
        return getOutOfPackageStackElement(ofPackageName,
                /*jdk14 logging uses new throwable() not "Thread.currentThread()"*/
                new Throwable().getStackTrace());
    }

    public static StackTraceElement getOutOfPackageStackElement(String ofPackageName, StackTraceElement[] stack) {
        boolean in = false;
        for(StackTraceElement ste : stack) {
            if(ste.getClassName().startsWith(ofPackageName + ".")) {
                in = true;
            } else if(in) return ste;
        }
        return null;
    }

    public static StackTraceElement getPrevCallerStackElement(Object o, StackTraceElement[] stack) {
        return getPrevCallerStackElement(o.getClass(), stack);
    }

    public static StackTraceElement getPrevCallerStackElement(Class<?> clazz, StackTraceElement[] stack) {
        return getPrevCallerStackElement(clazz.getName(), stack);
    }

    public static StackTraceElement getPrevCallerStackElement(String ofClassName, StackTraceElement[] stack) {
        boolean in = false;
        for(StackTraceElement ste : stack) {
            if(ste.getClassName().equals(ofClassName) || ste.getClassName().startsWith(ofClassName + "$")) {
                in = true;
            } else if(in) return ste;
        }
        return null;
    }

    public static StackTraceElement getCallerStackElement(Object o) {
        return getCallerStackElement(o.getClass(),
                /*jdk14 logging uses new throwable() not "Thread.currentThread()"*/
                new Throwable().getStackTrace());
    }

    public static StackTraceElement getCallerStackElement(Class<?> clazz) {
        return getCallerStackElement(clazz,
                /*jdk14 logging uses new throwable() not "Thread.currentThread()"*/
                new Throwable().getStackTrace());
    }

    public static StackTraceElement getCallerStackElement(Object o, StackTraceElement[] stack) {
        return getCallerStackElement(o.getClass(), stack);
    }

    public static StackTraceElement getCallerStackElement(Class<?> clazz, StackTraceElement[] stack) {
        return getCallerStackElement(clazz.getName(), stack);
    }

    public static StackTraceElement getCallerStackElement(String className) {
        return getCallerStackElement(className,
                /*jdk14 logging uses new throwable() not "Thread.currentThread()"*/
                new Throwable().getStackTrace());
    }

    public static StackTraceElement getCallerStackElement(String className, StackTraceElement[] stack) {
        for(StackTraceElement ste : stack) {
            if(ste.getClassName().equals(className) || ste.getClassName().startsWith(className + "$")) {
                return ste;
            }
        }
        return null;
    }
    public static ClassLoader getContextClassLoader() {
    	ClassLoader cl = Thread.currentThread().getContextClassLoader();
        if(cl == null) {
            cl = ReflectionUtils.class.getClassLoader();
            if(cl == null) cl = ClassLoader.getSystemClassLoader();
        }
        return cl;
    }
    public static Class<?> loadContextClass(String className) throws ClassNotFoundException {
    	return getContextClassLoader().loadClass(className);
    }

    public static Setter<Object> getStaticPropertySetter(String property) throws ClassCastException, IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException, IllegalArgumentException, NoSuchMethodException {
		NestedNameTokenizer token = new NestedNameTokenizer(property);
		token.nextProperty();
        while(token.hasNext()) {
        	String classname = token.getConsumed();
            String name = token.nextProperty();
            Class<?> clazz;
			try {
				clazz = Class.forName(classname);
			} catch(ClassNotFoundException e) {
				continue;
			}
            String baseName = StringUtils.capitalizeFirst(name);
            if(!token.hasNext()) {
            	if(token.isIndexed()) {
            		final Method method = findMethod(clazz, "set" + baseName, INDEXED_AND_ANY_PARAMS);
            		final int index = token.getIndex();
            		if(method != null) {
            			final Class<?> type = method.getParameterTypes()[1];
                    	return new Setter<Object>() {
                    		public void set(Object t) {
                    			try {
                    				method.invoke(null, index, ConvertUtils.convert(type, t));
                    			} catch(IllegalArgumentException e) {
    								throw new UndeclaredThrowableException(e);
    							} catch(IllegalAccessException e) {
    								throw new UndeclaredThrowableException(e);
    							} catch(InvocationTargetException e) {
    								throw new UndeclaredThrowableException(e);
    							} catch(ConvertException e) {
    								throw new UndeclaredThrowableException(e);
								}
                    		}
                    		@SuppressWarnings("unchecked")
							public Class<Object> getType() {
                    			return (Class<Object>)type;
                    		}
						};
                    }
                    final Object base = getSimpleStaticProperty(clazz, name);
                    if(base != null) {
                    	final Class<?> type;
                    	if(base.getClass().isArray())
                    		type = base.getClass().getComponentType();
                    	else
                    		type = Object.class;
                    	return new Setter<Object>() {
                    		public void set(Object t) {
                    			try {
									setElement(base, index, t);
								} catch(ConvertException e) {
									throw new UndeclaredThrowableException(e);
								}
                    		}
                    		@SuppressWarnings("unchecked")
							public Class<Object> getType() {
                    			return (Class<Object>)type;
                    		}
						};
        	        }
            		return null;
            	} else if(token.isMapped()) {
            		final Method method = findMethod(clazz, "set" + baseName, MAPPED_AND_ANY_PARAMS);
            		final String key = token.getKey();
                	if(method != null) {
                		final Class<?> type = method.getParameterTypes()[1];
                    	return new Setter<Object>() {
                    		public void set(Object t) {
                    			try {
                    				method.invoke(null, key, ConvertUtils.convert(type, t));
	                    		} catch(IllegalArgumentException e) {
									throw new UndeclaredThrowableException(e);
								} catch(IllegalAccessException e) {
									throw new UndeclaredThrowableException(e);
								} catch(InvocationTargetException e) {
									throw new UndeclaredThrowableException(e);
								} catch(ConvertException e) {
									throw new UndeclaredThrowableException(e);
								}
                    		}
                    		@SuppressWarnings("unchecked")
							public Class<Object> getType() {
                    			return (Class<Object>)type;
                    		}
						};
                    }
                    final Object base = getSimpleStaticProperty(clazz, name);
                    if(base instanceof Map<?,?>) {
                    	return new Setter<Object>() {
                    		public void set(Object t) {
                    			CollectionUtils.uncheckedPut((Map<?,?>)base, key, t);
                    		}
                    		public Class<Object> getType() {
                    			return Object.class;
                    		}
						};
                    } else if(base instanceof DynaBean) {
                    	return new Setter<Object>() {
                    		public void set(Object t) {
                    			((DynaBean)base).set(key, t);
                    		}
                    		public Class<Object> getType() {
                    			return Object.class;
                    		}
						};                   	
                    }
            		return null;
            	} else {
            		final Method method = findMethod(clazz, "set" + baseName, ANY_ONE_PARAM);
            		if(method != null) {
	            		final Class<?> type = method.getParameterTypes()[0];
	                	return new Setter<Object>() {
	                		public void set(Object t) {
	                			try {
									method.invoke(null, ConvertUtils.convert(type, t));
								} catch(IllegalArgumentException e) {
									throw new UndeclaredThrowableException(e);
								} catch(IllegalAccessException e) {
									throw new UndeclaredThrowableException(e);
								} catch(InvocationTargetException e) {
									throw new UndeclaredThrowableException(e);
								} catch(ConvertException e) {
									throw new UndeclaredThrowableException(e);
								}
	                		}
	                		@SuppressWarnings("unchecked")
							public Class<Object> getType() {
	                			return (Class<Object>)type;
	                		}
						};
            		} else {
            			return null;
            		}
            	}
            } else {
            	Object base;
            	if(token.isIndexed()) {
            		Method method = findMethod(clazz, "get" + baseName, INDEXED_PARAM);
                    if(method != null) {
                    	base = method.invoke(null, token.getIndex());
                    } else {
                    	base = getSimpleStaticProperty(clazz, name);
	                    if(base != null) {
	                    	base = getElement(base, token.getIndex());
	        	        }
                    }
                } else if(token.isMapped()) {
            		Method method = findMethod(clazz, "get" + baseName, MAPPED_PARAM);
                    if(method != null) {
                    	base = method.invoke(null, token.getKey());
                    } else {
                    	base = getSimpleStaticProperty(clazz, name);
	                    if(base instanceof Map<?,?>) {
	                    	base = ((Map<?,?>)base).get(token.getKey());
	                    } else if(base instanceof DynaBean) {
	                    	base = ((DynaBean)base).get(token.getKey());
	                    } else {
	                    	throw new ClassCastException("Could not convert " + base.getClass() + " to a mapped object");
	                    }
                    }
            	} else {
            		base = getSimpleStaticProperty(clazz, name);
            	}
            	if(base != null) {
            		final Object baseFinal = base;
            		final BeanPropertyChain bpc = findBeanPropertyChain(base.getClass(), token.getRemaining(), true);
	            	if(bpc != null) {
	            		return new Setter<Object>() {
	                		public void set(Object t) {
	                			try {
									bpc.setValue(baseFinal, ConvertUtils.convert(bpc.getType(), t));
								} catch(IllegalArgumentException e) {
									throw new UndeclaredThrowableException(e);
								} catch(IllegalAccessException e) {
									throw new UndeclaredThrowableException(e);
								} catch(InvocationTargetException e) {
									throw new UndeclaredThrowableException(e);
								} catch(ConvertException e) {
									throw new UndeclaredThrowableException(e);
								}
	                		}
	                		@SuppressWarnings("unchecked")
							public Class<Object> getType() {
	                			return (Class<Object>)bpc.getType();
	                		}
						};
	            	}
            	} else {
            		return null;
            	}
            }
        }
        return null;
	}
	public static boolean setStaticProperty(String property, Object value) throws ClassCastException, IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException, IllegalArgumentException, NoSuchMethodException {
		NestedNameTokenizer token = new NestedNameTokenizer(property);
		token.nextProperty();
        while(token.hasNext()) {
        	String classname = token.getConsumed();
            String name = token.nextProperty();
            Class<?> clazz;
			try {
				clazz = Class.forName(classname);
			} catch(ClassNotFoundException e) {
				continue;
			}
            String baseName = StringUtils.capitalizeFirst(name);
            if(!token.hasNext()) {
            	if(token.isIndexed()) {
            		Method method = findMethod(clazz, "set" + baseName, INDEXED_AND_ANY_PARAMS);
                    if(method != null) {
                    	method.invoke(null, token.getIndex(), ConvertUtils.convert(method.getParameterTypes()[1], value));
                    	return true;
                    }
                    Object base = getSimpleStaticProperty(clazz, name);
                    if(base != null) {
                    	return setElement(base, token.getIndex(), value);
        	        }
            		return false;
            	} else if(token.isMapped()) {
            		Method method = findMethod(clazz, "set" + baseName, MAPPED_AND_ANY_PARAMS);
                    if(method != null) {
                    	method.invoke(null, token.getKey(), ConvertUtils.convert(method.getParameterTypes()[1], value));
                    	return true;
                    }
                    Object base = getSimpleStaticProperty(clazz, name);
                    if(base instanceof Map<?,?>) {
                    	CollectionUtils.uncheckedPut((Map<?,?>)base, token.getKey(), value);
                        return true;
                    } else if(base instanceof DynaBean) {
                    	((DynaBean)base).set(token.getKey(), value);
                    	return true;
                    }
            		return false;
            	} else {
            		return setSimpleStaticProperty(clazz, name, value);
            	}
            } else {
            	Object base;
            	if(token.isIndexed()) {
            		Method method = findMethod(clazz, "get" + baseName, INDEXED_PARAM);
                    if(method != null) {
                    	base = method.invoke(null, token.getIndex());
                    } else {
                    	base = getSimpleStaticProperty(clazz, name);
	                    if(base != null) {
	                    	base = getElement(base, token.getIndex());
	        	        }
                    }
                } else if(token.isMapped()) {
            		Method method = findMethod(clazz, "get" + baseName, MAPPED_PARAM);
                    if(method != null) {
                    	base = method.invoke(null, token.getKey());
                    } else {
                    	base = getSimpleStaticProperty(clazz, name);
	                    if(base instanceof Map<?,?>) {
	                    	base = ((Map<?,?>)base).get(token.getKey());
	                    } else if(base instanceof DynaBean) {
	                    	base = ((DynaBean)base).get(token.getKey());
	                    } else {
	                    	throw new ClassCastException("Could not convert " + base.getClass() + " to a mapped object");
	                    }
                    }
            	} else {
            		base = getSimpleStaticProperty(clazz, name);
            	}
            	return setProperty(base, token.getRemaining(), value, true);
            }
        }
        return false;
	}

	public static Object getSimpleStaticProperty(Class<?> clazz, String name) throws IllegalArgumentException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
		Field field;
		try {
			field = clazz.getField(name);
			if(Modifier.isStatic(field.getModifiers())) {
	        	return field.get(null);
	        }
        } catch(SecurityException e) {
		} catch(NoSuchFieldException e) {
		}
        Method method = ReflectionUtils.findMethod(clazz, "get" + StringUtils.capitalizeFirst(name), ReflectionUtils.ZERO_PARAMS);
        if(method != null && Modifier.isStatic(method.getModifiers())) {
        	return method.invoke(null);
        }
        throw new NoSuchMethodException("No static readable property '" + clazz.getName() + '.' + name + "' exists");
	}
	public static boolean setSimpleStaticProperty(Class<?> clazz, String name, Object value) throws IllegalArgumentException, IllegalAccessException, ConvertException, InvocationTargetException, NoSuchMethodException {
		Field field;
		try {
			field = clazz.getField(name);
			if(Modifier.isStatic(field.getModifiers()) && !Modifier.isFinal(field.getModifiers())) {
	        	field.set(null, ConvertUtils.convert(field.getType(), value));
	        	return true;
	        }
        } catch(SecurityException e) {
		} catch(NoSuchFieldException e) {
		}
        Method method = ReflectionUtils.findMethod(clazz, "set" + StringUtils.capitalizeFirst(name), ANY_ONE_PARAM);
        if(method != null) {
        	method.invoke(null, ConvertUtils.convert(method.getParameterTypes()[0], value));
        	return true;
        }
        throw new NoSuchMethodException("No static writable property '" + clazz.getName() + '.' + name + "' exists");
	}
    
    public static Set<Class<?>> getClassHierarchy(Class<?> clazz) {
    	Set<Class<?>> hierarchy = new LinkedHashSet<Class<?>>();
    	addClassHierarchy(clazz, hierarchy);
    	return hierarchy;
    }
    
    protected static void addClassHierarchy(Class<?> clazz, Collection<Class<?>> hierarchy) {
    	if(hierarchy.add(clazz)) {
    		for(Class<?> interfaze : clazz.getInterfaces()) {
    			addClassHierarchy(interfaze, hierarchy);
    		}
    		Class<?> superclazz = clazz.getSuperclass();
    		if(superclazz != null)
    			addClassHierarchy(superclazz, hierarchy);
    	}
    }
}
