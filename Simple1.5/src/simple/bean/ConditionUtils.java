/*
 * ConditionUtils.java
 *
 * Created on December 22, 2003, 4:40 PM
 */

package simple.bean;

import java.math.BigDecimal;
import java.util.Map;
import java.util.StringTokenizer;

import simple.bean.conditions.EmailCondition;
import simple.bean.conditions.RegexCondition;

/**
 * Utility class for determining if a given object meets a certain condition.
 *
 * @author  Brian S. Krug
 */
public class ConditionUtils {
    private static final simple.io.Log log = simple.io.Log.getLog();
    public static final String OP_EQUAL = "=";
    public static final String OP_NOT_EQUAL = "!=";
    public static final String OP_GREATER = ">";
    public static final String OP_GREATER_EQUAL = ">=";
    public static final String OP_LESSER = "<";
    public static final String OP_LESSER_EQUAL = "<=";
    public static final String OP_STARTS_WITH = "->";
    public static final String OP_ENDS_WITH = "<-";
    public static final String OP_IN = "IN";
    public static final String OP_NOT_IN = "NOT IN";
    public static final String OP_CONTAINS = "CONTAINS";
    public static final String OP_NOT_CONTAINS = "NOT CONTAINS";
    public static final String OP_PRESENT = "PRESENT";
    public static final String OP_NOT_PRESENT = "NOT PRESENT";
    public static final String OP_MATCH = "MATCH";
    public static final String OP_TRUE = "TRUE";
    public static final String OP_FALSE = "FALSE";
    public static final String OP_HAS = "HAS";
    public static final String OP_NOT_HAS = "NOT HAS";
    protected static final Map<String, Op> opMap = new java.util.HashMap<String, Op>();

    static {
        register(new BaseCondition());
        register(new RegexCondition());
        try {
        	register(new EmailCondition());
        } catch(Exception e) {
        	//Ignore
        }
    }

    /** Creates a new instance of ConditionUtils */
    public ConditionUtils() {
    }

    /** Checks the primary object for validity using the specified operator and the second object (if necessary) for
     * determining whether it meets this condition.
     * @param o1 The primary object
     * @param operator The operator to use to for this check
     * @param o2 The second object
     * @return Whether the specified objects pass the condition
     */
    public static boolean check(Object o1, String operator, Object o2) {
        Op op = opMap.get(operator.toUpperCase());
        if(op == null) throw new IllegalArgumentException("Invalid operator '" + operator + "'");
        return op.Condition.check(o1, op.index, o2);
    }

    /** Registers the specified Condition with this Utility object so that the
     * Condition's operators can be referenced in the check() method of this Utility
     * @param condition The Condition object to add to this utility
     */
    public static void register(Condition condition) {
        String[] ops = condition.getOperators();
        for(int i = 0; i < ops.length; i++) {
            opMap.put(ops[i].toUpperCase(), new Op(i, condition));
        }
    }

    protected static boolean in(Number n, String list) {
        BigDecimal bd;
        if(n instanceof BigDecimal) bd = (BigDecimal)n;
        else bd = new BigDecimal(n.toString());
        StringTokenizer token = new StringTokenizer(list, " \t,;");
        log.debug("Checking through " + token.countTokens() + " items");
        while(token.hasMoreTokens()) {
            String s = token.nextToken();
            int div = s.indexOf('-');
            try {
                if(div > 0) {
                    BigDecimal low = new BigDecimal(s.substring(0,div));
                    BigDecimal high = new BigDecimal(s.substring(div+1));
                    if(low.compareTo(bd) <= 0 && high.compareTo(bd) >= 0) return true;
                } else {
                    BigDecimal listBD = new BigDecimal(s);
                    log.debug("Check if " + bd + " = " + listBD);
                    if(listBD.equals(bd)) return true;
                }
            } catch(NumberFormatException nfe) {
                log.debug("Could not convert '" + s + "' to a BigDecimal");
            }
        }
        return false;
    }

    protected static boolean equal(Object o1, Object o2) {
        if(o1 == null) return o2 == null;
        else return o1.equals(prepare(o2, o1.getClass()));
    }

    public static Object prepare(Object o2, Class<?> c1) {
        try{
            Object ret = ConvertUtils.convert(c1, o2);
            if(ret != null) return ret;
        } catch(simple.bean.ConvertException ce) {
        }
        return o2; // if conversion resulted in a null or an exception return the original value
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected static Integer compare(Object o1, Object o2) {
        if(o1 instanceof Comparable && o2 instanceof Comparable)
            try {
            	return new Integer(((Comparable)o1).compareTo(o2));
            } catch(ClassCastException e) {
            	//Ignore
            }
        return null;
    }

    protected static class Op {
        public int index;
        public Condition Condition;
        public Op(int i, Condition v) {
            index = i;
            Condition = v;
        }
    }

    public static class BaseCondition implements Condition {
        protected String[] operators = new String[] {
            OP_EQUAL,
            OP_NOT_EQUAL,
            OP_GREATER,
            OP_GREATER_EQUAL,
            OP_LESSER,
            OP_LESSER_EQUAL,
            OP_STARTS_WITH,
            OP_ENDS_WITH,
            OP_CONTAINS,
            OP_NOT_CONTAINS,
            OP_IN,
            OP_NOT_IN,
            OP_PRESENT,
            OP_NOT_PRESENT,
            OP_TRUE,
            OP_FALSE,
            "ABSENT", // same as NOT PRESENT
            OP_HAS,
            OP_NOT_HAS
        };

        public boolean check(Object o1, int operatorIndex, Object o2) {
        	if(log.isDebugEnabled()) log.debug("Checking if " + o1 + (o1 != null ? " (" + o1.getClass().getName() + ")" : "") + " " + operators[operatorIndex] + " " + o2);
            String s1, s2;
            switch(operatorIndex) {
            	case 6: case 7: case 8: case 9:
                	s1 = ConvertUtils.getStringSafely(o1);
            		s2 = ConvertUtils.getStringSafely(o2);
            		switch(operatorIndex) {
                        case 6: //starts with
		                	return s1 != null && s2 != null && s1.startsWith(s2);
		                case 7: //ends with
		                	return s1 != null && s2 != null && s1.endsWith(s2);
		                case 8: //contains
		                	return s1 != null && s2 != null && s1.indexOf(s2) >= 0;
		                case 9: //doesn't contain
		                	return s1 != null && s2 != null && s1.indexOf(s2) < 0;
            		}
            		break;
				case 17:
				case 18:
					Object otmp = o2;
					o2 = o1;
					o1 = otmp;
            	case 10: case 11:
            		if(o2 != null && o2.getClass().isArray()) o2 = new simple.util.PrimitiveArrayList(o2); // really ought to sort and binary search
            		switch(operatorIndex) {
						case 10:
						case 17: // in, has
            				if(o2 == null) return false;
            				if(o1 instanceof Number && !ConvertUtils.isCollectionType(o2.getClass())) {
            					return in((Number)o1, ConvertUtils.getStringSafely(o2));
            				} else {
                            	try {
        							if(o1 == null) return ConvertUtils.asCollection(o2).contains(o1);
        							return ConvertUtils.asCollection(o2, o1.getClass()).contains(o1);
        						} catch(ConvertException e) {
        							return false;
        						}
            				}
						case 11:
						case 18: // not in, not has
                        	if(o2 == null) return false;
            				if(o1 instanceof Number && !ConvertUtils.isCollectionType(o2.getClass())) {
            					return !in((Number)o1, ConvertUtils.getStringSafely(o2));
            				} else {
                            	try {
        							if(o1 == null) return !ConvertUtils.asCollection(o2).contains(o1);
        							return !ConvertUtils.asCollection(o2, o1.getClass()).contains(o1);
        						} catch(ConvertException e) {
        							return false;
        						}
            				}
                	}
            		break;
        		default: // try to make them the same class
        			if(o1 != null) o2 = prepare(o2, o1.getClass());
	        		Integer tmp;
	                switch(operatorIndex) {
	                    case 0: //equals
	                        return equal(o1, o2);
	                    case 1: // not equals
	                        return !equal(o1, o2);
	                    case 2: // greater than
	                        return ((tmp=compare(o1, o2)) != null && tmp.intValue() > 0);
	                    case 3: //greater or equal
	                        return ((tmp=compare(o1, o2)) != null && tmp.intValue() >= 0);
	                    case 4: // less than
	                        return ((tmp=compare(o1, o2)) != null && tmp.intValue() < 0);
	                    case 5: // less or equal
	                        return ((tmp=compare(o1, o2)) != null && tmp.intValue() <= 0);
	                    case 12: //present
	                        return o1 != null && (s1=ConvertUtils.getStringSafely(o1)) != null && s1.trim().length() > 0;
	                    case 13: case 16: //not present
	                        return o1 == null || (s1=ConvertUtils.getStringSafely(o1)) == null || s1.trim().length() == 0;
	                    case 14: //true
	                        try {
	                            Boolean b = ConvertUtils.convert(Boolean.class, o1);
	                            return b != null && b.booleanValue();
	                        } catch(ConvertException ce) {
	                            return false;
	                        }
	                    case 15: //false
	                        try {
	                            Boolean b = ConvertUtils.convert(Boolean.class, o1);
	                            return b == null || !b.booleanValue();
	                        } catch(ConvertException ce) {
	                            return false;
	                        }
	                }
            }
            return false;
        }

        public String[] getOperators() {
            return operators;
        }
    }
}
