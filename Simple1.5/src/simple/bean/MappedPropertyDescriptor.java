package simple.bean;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.Map;

public class MappedPropertyDescriptor extends PropertyDescriptor {
	private Reference<Class<?>> classRef;
	private Reference<Class<?>> mappedPropertyTypeRef;
	private Reference<Method> mappedReadMethodRef;
	private Reference<Method> mappedWriteMethodRef;
	private String mappedReadMethodName;
	private String mappedWriteMethodName;
	private String baseName;

	/**
	 * This constructor constructs an MappedPropertyDescriptor for a property
	 * that follows the standard Java conventions by having getFoo and setFoo
	 * accessor methods, for both mapped access and array access.
	 * <p>
	 * Thus if the argument name is "fred", it will assume that there is an mapped reader method "getFred", a nmappedxed (array) reader method also called "getFred", an mapped writer method "setFred",
	 * and finally a non-mapped writer method "setFred".
	 * 
	 * @param propertyName
	 *            The programmatic name of the property.
	 * @param beanClass
	 *            <?>The Class<?>object for the target bean.
	 * @exception IntrospectionException
	 *                if an exception occurs during
	 *                introspection.
	 */
	public MappedPropertyDescriptor(String propertyName, Class<?> beanClass)
			throws IntrospectionException {
		this(propertyName, beanClass,
				"get" + capitalize(propertyName),
				"set" + capitalize(propertyName),
				"get" + capitalize(propertyName),
				"set" + capitalize(propertyName));
	}

	protected static String capitalize(String name) {
		if(name == null || name.length() == 0) {
			return name;
		}
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	protected void setClass0(Class<?> cls) {
		classRef = cls == null ? null : new WeakReference<Class<?>>(cls);
	}

	protected Class<?> getClass0() {
		return classRef == null ? null : classRef.get();
	}

	protected String getBaseName() {
		if(baseName == null) {
			baseName = capitalize(getName());
		}
		return baseName;
	}

	/**
	 * This constructor takes the name of a simple property, and method
	 * names for reading and writing the property, both mapped
	 * and non-mapped.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property.
	 * @param beanClass
	 *            <?> The Class<?>object for the target bean.
	 * @param readMethodName
	 *            The name of the method used for reading the property
	 *            values as an array. May be null if the property is write-only
	 *            or must be mapped.
	 * @param writeMethodName
	 *            The name of the method used for writing the property
	 *            values as an array. May be null if the property is read-only
	 *            or must be mapped.
	 * @param mappedReadMethodName
	 *            The name of the method used for reading
	 *            an mapped property value.
	 *            May be null if the property is write-only.
	 * @param mappedWriteMethodName
	 *            The name of the method used for writing
	 *            an mapped property value.
	 *            May be null if the property is read-only.
	 * @exception IntrospectionException
	 *                if an exception occurs during
	 *                introspection.
	 */
	public MappedPropertyDescriptor(String propertyName, Class<?> beanClass,
			String readMethodName, String writeMethodName,
			String mappedReadMethodName, String mappedWriteMethodName)
			throws IntrospectionException {
		super(propertyName, beanClass, readMethodName, writeMethodName);

		this.mappedReadMethodName = mappedReadMethodName;
		if(mappedReadMethodName != null && getMappedReadMethod() == null) {
			throw new IntrospectionException("Method not found: " + mappedReadMethodName);
		}

		this.mappedWriteMethodName = mappedWriteMethodName;
		if(mappedWriteMethodName != null && getMappedWriteMethod() == null) {
			throw new IntrospectionException("Method not found: " + mappedWriteMethodName);
		}
		// Implemented only for type checking.
		findMappedPropertyType(getMappedReadMethod(), getMappedWriteMethod());
	}

	/**
	 * This constructor takes the name of a simple property, and Method
	 * objects for reading and writing the property.
	 * 
	 * @param propertyName
	 *            The programmatic name of the property.
	 * @param readMethod
	 *            The method used for reading the property values as an array.
	 *            May be null if the property is write-only or must be mapped.
	 * @param writeMethod
	 *            The method used for writing the property values as an array.
	 *            May be null if the property is read-only or must be mapped.
	 * @param mappedReadMethod
	 *            The method used for reading an mapped property value.
	 *            May be null if the property is write-only.
	 * @param mappedWriteMethod
	 *            The method used for writing an mapped property value.
	 *            May be null if the property is read-only.
	 * @exception IntrospectionException
	 *                if an exception occurs during
	 *                introspection.
	 */
	public MappedPropertyDescriptor(String propertyName, Method readMethod, Method writeMethod,
			Method mappedReadMethod, Method mappedWriteMethod)
			throws IntrospectionException {
		super(propertyName, readMethod, writeMethod);

		setMappedReadMethod0(mappedReadMethod);
		setMappedWriteMethod0(mappedWriteMethod);

		// Type checking
		setMappedPropertyType(findMappedPropertyType(mappedReadMethod, mappedWriteMethod));
	}

	/**
	 * Gets the method that should be used to read an mapped
	 * property value.
	 * 
	 * @return The method that should be used to read an mapped
	 *         property value.
	 *         May return null if the property isn't mapped or is write-only.
	 */
	public synchronized Method getMappedReadMethod() {
		Method mappedReadMethod = getMappedReadMethod0();
		if(mappedReadMethod == null) {
			Class<?> cls = getClass0();
			if(cls == null ||
					(mappedReadMethodName == null && mappedReadMethodRef == null)) {
				// the Mapped readMethod was explicitly set to null.
				return null;
			}
			if(mappedReadMethodName == null) {
				Class<?> type = getMappedPropertyType0();
				if(type == boolean.class || type == null) {
					mappedReadMethodName = "is" + getBaseName();
				} else {
					mappedReadMethodName = "get" + getBaseName();
				}
			}

			Class<?>[] args = { String.class };

			mappedReadMethod = ReflectionUtils.findMethod(cls, mappedReadMethodName, args);
			if(mappedReadMethod == null) {
				// no "is" method, so look for a "get" method.
				mappedReadMethodName = "get" + getBaseName();
				mappedReadMethod = ReflectionUtils.findMethod(cls, mappedReadMethodName, args);
			}
			setMappedReadMethod0(mappedReadMethod);
		}
		return mappedReadMethod;
	}

	/**
	 * Sets the method that should be used to read an mapped property value.
	 * 
	 * @param readMethod
	 *            The new mapped read method.
	 */
	public synchronized void setMappedReadMethod(Method readMethod)
			throws IntrospectionException {

		// the mapped property type is set by the reader.
		setMappedPropertyType(findMappedPropertyType(readMethod,
				getMappedWriteMethod0()));
		setMappedReadMethod0(readMethod);
	}

	private void setMappedReadMethod0(Method readMethod) {
		if(readMethod == null) {
			mappedReadMethodName = null;
			mappedReadMethodRef = null;
			return;
		}
		setClass0(readMethod.getDeclaringClass());

		mappedReadMethodName = readMethod.getName();
		mappedReadMethodRef = new WeakReference<Method>(readMethod);
	}

	/**
	 * Gets the method that should be used to write an mapped property value.
	 * 
	 * @return The method that should be used to write an mapped
	 *         property value.
	 *         May return null if the property isn't mapped or is read-only.
	 */
	public synchronized Method getMappedWriteMethod() {
		Method mappedWriteMethod = getMappedWriteMethod0();
		if(mappedWriteMethod == null) {
			Class<?> cls = getClass0();
			if(cls == null ||
					(mappedWriteMethodName == null && mappedWriteMethodRef == null)) {
				// the Mapped writeMethod was explicitly set to null.
				return null;
			}

			// We need the mapped type to ensure that we get the correct method.
			// Cannot use the getMappedPropertyType method since that could
			// result in an infinite loop.
			Class<?> type = getMappedPropertyType0();
			if(type == null) {
				try {
					type = findMappedPropertyType(getMappedReadMethod(), null);
					setMappedPropertyType(type);
				} catch(IntrospectionException ex) {
					// Set iprop type to be the classic type
					Class<?> propType = getPropertyType();
					if(Map.class.isAssignableFrom(propType)) {
						type = Object.class;
					}
				}
			}

			if(mappedWriteMethodName == null) {
				mappedWriteMethodName = "set" + getBaseName();
			}
			mappedWriteMethod = ReflectionUtils.findMethod(cls, mappedWriteMethodName, new Class[] { String.class, type });
			setMappedWriteMethod0(mappedWriteMethod);
		}
		return mappedWriteMethod;
	}

	/**
	 * Sets the method that should be used to write an mapped property value.
	 * 
	 * @param writeMethod
	 *            The new mapped write method.
	 */
	public synchronized void setMappedWriteMethod(Method writeMethod)
			throws IntrospectionException {

		// If the mapped property type has not been set, then set it.
		Class<?> type = findMappedPropertyType(getMappedReadMethod(),
				writeMethod);
		setMappedPropertyType(type);
		setMappedWriteMethod0(writeMethod);
	}

	private void setMappedWriteMethod0(Method writeMethod) {
		if(writeMethod == null) {
			mappedWriteMethodName = null;
			mappedWriteMethodRef = null;
			return;
		}
		setClass0(writeMethod.getDeclaringClass());

		mappedWriteMethodName = writeMethod.getName();
		mappedWriteMethodRef = new WeakReference<Method>(writeMethod);
	}

	/**
	 * Gets the <code>Class</code> object of the mapped properties' type.
	 * The returned <code>Class</code> may describe a primitive type such as <code>int</code>.
	 * 
	 * @return The <code>Class</code> for the mapped properties' type; may return <code>null</code> if the type cannot be determined.
	 */
	public synchronized Class<?> getMappedPropertyType() {
		Class<?> type = getMappedPropertyType0();
		if(type == null) {
			try {
				type = findMappedPropertyType(getMappedReadMethod(),
						getMappedWriteMethod());
				setMappedPropertyType(type);
			} catch(IntrospectionException ex) {
				// fall
			}
		}
		return type;
	}

	// Private methods which set get/set the Reference objects

	private void setMappedPropertyType(Class<?> type) {
		mappedPropertyTypeRef = new WeakReference<Class<?>>(type);
	}

	private Class<?> getMappedPropertyType0() {
		return mappedPropertyTypeRef == null ? null : mappedPropertyTypeRef.get();
	}

	private Method getMappedReadMethod0() {
		return mappedReadMethodRef == null ? null : mappedReadMethodRef.get();
	}

	private Method getMappedWriteMethod0() {
		return mappedWriteMethodRef == null ? null : mappedWriteMethodRef.get();
	}

	private Class<?> findMappedPropertyType(Method mappedReadMethod,
			Method mappedWriteMethod)
			throws IntrospectionException {
		Class<?> mappedPropertyType = null;

		if(mappedReadMethod != null) {
			Class<?> params[] = mappedReadMethod.getParameterTypes();
			if(params.length != 1) {
				throw new IntrospectionException("bad mapped read method arg count");
			}
			if(params[0] != String.class) {
				throw new IntrospectionException("non String key mapped read method");
			}
			mappedPropertyType = mappedReadMethod.getReturnType();
			if(mappedPropertyType == Void.TYPE) {
				throw new IntrospectionException("mapped read method returns void");
			}
		}
		if(mappedWriteMethod != null) {
			Class<?> params[] = mappedWriteMethod.getParameterTypes();
			if(params.length != 2) {
				throw new IntrospectionException("bad mapped write method arg count");
			}
			if(params[0] != String.class) {
				throw new IntrospectionException("non String key mapped write method");
			}
			if(mappedPropertyType != null && mappedPropertyType != params[1]) {
				throw new IntrospectionException(
						"type mismatch between mapped read and mapped write methods: "
								+ getName());
			}
			mappedPropertyType = params[1];
		}
		Class<?> propertyType = getPropertyType();
		if(propertyType != null && (!Map.class.isAssignableFrom(propertyType))) {
			throw new IntrospectionException("type mismatch between mapped and non-mapped methods: "
					+ getName());
		}
		return mappedPropertyType;
	}

	/**
	 * Compares this <code>PropertyDescriptor</code> against the specified object.
	 * Returns true if the objects are the same. Two <code>PropertyDescriptor</code>s
	 * are the same if the read, write, property types, property editor and
	 * flags are equivalent.
	 * 
	 * @since 1.4
	 */
	@Override
	public boolean equals(Object obj) {
		// Note: This would be identical to PropertyDescriptor but they don't
		// share the same fields.
		if(this == obj) {
			return true;
		}

		if(obj != null && obj instanceof MappedPropertyDescriptor) {
			MappedPropertyDescriptor other = (MappedPropertyDescriptor) obj;
			Method otherMappedReadMethod = other.getMappedReadMethod();
			Method otherMappedWriteMethod = other.getMappedWriteMethod();

			if(!compareMethods(getMappedReadMethod(), otherMappedReadMethod)) {
				return false;
			}

			if(!compareMethods(getMappedWriteMethod(), otherMappedWriteMethod)) {
				return false;
			}

			if(getMappedPropertyType() != other.getMappedPropertyType()) {
				return false;
			}
			return super.equals(obj);
		}
		return false;
	}

	protected boolean compareMethods(Method a, Method b) {
		// Note: perhaps this should be a protected method in FeatureDescriptor
		if((a == null) != (b == null)) {
			return false;
		}

		if(a != null && b != null) {
			if(!a.equals(b)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns a hash code value for the object.
	 * See {@link java.lang.Object#hashCode} for a complete description.
	 * 
	 * @return a hash code value for this object.
	 * @since 1.5
	 */
	@Override
	public int hashCode() {
		int result = super.hashCode();

		result = 37 * result + ((mappedWriteMethodName == null) ? 0 :
				mappedWriteMethodName.hashCode());
		result = 37 * result + ((mappedReadMethodName == null) ? 0 :
				mappedReadMethodName.hashCode());
		result = 37 * result + ((getMappedPropertyType() == null) ? 0 :
				getMappedPropertyType().hashCode());

		return result;
	}

}
