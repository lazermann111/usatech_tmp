/*
 * ConvertException.java
 *
 * Created on March 11, 2003, 11:14 AM
 */

package simple.bean;

/** Thrown whenever an error occurs trying to convert an object from one class
 *  to another.
 *
 * @author  Brian S. Krug
 */
public class ConvertException extends java.lang.Exception {
    private static final long serialVersionUID = 785441234L;
    protected Class<?> targetClass;
    protected Object sourceObject;
    
    public ConvertException(String message, Class<?> targetClass, Object sourceObject, Throwable cause) {
		super(message, cause);
		this.targetClass = targetClass;
		this.sourceObject = sourceObject;
	}

	public ConvertException(String message, Class<?> targetClass, Object sourceObject) {
		super(message);
		this.targetClass = targetClass;
		this.sourceObject = sourceObject;
	}

	public ConvertException(Class<?> targetClass, Object sourceObject) {
		super();
		this.targetClass = targetClass;
		this.sourceObject = sourceObject;
	}

	public Object getSourceObject() {
		return sourceObject;
	}

	public void setSourceObject(Object sourceObject) {
		this.sourceObject = sourceObject;
	}

	public Class<?> getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(Class<?> targetClass) {
		this.targetClass = targetClass;
	} 
}
