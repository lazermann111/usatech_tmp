/*
 * AbstractDynaBean.java
 *
 * Created on November 6, 2003, 10:51 AM
 */

package simple.bean;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;

/** Provides implementation of DynaBean's methods to allow you to add properties dynamically.
 * @author Brian S. Krug
 */
public class GetterSetterDynaBean implements DynaBean {
	protected static class GetterSetterDynaProperty<T> extends DynaProperty {
		private static final long serialVersionUID = 5081985950086767160L;
		protected final Getter<T> getter;
		protected final Setter<T> setter;

		public GetterSetterDynaProperty(String name, Class<T> type, Getter<T> getter, Setter<T> setter) {
			super(name, type);
			this.getter = getter;
			this.setter = setter;
		}
	}
    protected DynaProperty[] dynaProperties;
    protected final String dynaClassName;
    protected final Map<String, GetterSetterDynaProperty<?>> dynaPropertyMap = new HashMap<String,GetterSetterDynaProperty<?>>();
    protected final DynaClass dynaClass = new DynaClass() {
		public DynaProperty[] getDynaProperties() {
			DynaProperty[] dp = dynaProperties;
			if(dp == null)
				dynaProperties = dp = dynaPropertyMap.values().toArray(new DynaProperty[dynaPropertyMap.size()]);
			return dp;
		}

		public DynaProperty getDynaProperty(String name) {
			return dynaPropertyMap.get(name);
		}

		public String getName() {
			return dynaClassName;
		}
		public org.apache.commons.beanutils.DynaBean newInstance() throws IllegalAccessException, InstantiationException {
			return new GetterSetterDynaBean(dynaClassName);
		}
    };

    /** Creates a new instance of GetterSetterDynaBean */
    public GetterSetterDynaBean(String dynaClassName) {
    	this.dynaClassName = dynaClassName;
    }

    public <T> void addDynaProperty(String name, Class<T> type, Getter<T> getter, Setter<T> setter) {
    	dynaPropertyMap.put(name, new GetterSetterDynaProperty<T>(name, type, getter, setter));
    	dynaProperties = null;
    }
    public <T> void removeDynaProperty(String name) {
    	dynaPropertyMap.remove(name);
    	dynaProperties = null;
    }
    public void set(String name, int index, Object value) {
    	GetterSetterDynaProperty<?> gsdp = dynaPropertyMap.get(name);
    	if(gsdp != null && gsdp.isIndexed() && gsdp.getter != null) {
    		Object bean = gsdp.getter.get();
    		try {
				ReflectionUtils.setElement(bean, index, value);
			} catch(ConvertException e) {
				throw new ConversionException(e);
			}
    	}
    }

    public void set(String name, String key, Object value) {
    	GetterSetterDynaProperty<?> gsdp = dynaPropertyMap.get(name);
    	if(gsdp != null && gsdp.isMapped() && gsdp.getter != null) {
    		Object bean = gsdp.getter.get();
    		if(bean instanceof Map<?,?>)
    			castMap(bean).put(key, value);
    	}
    }

	public void set(String name, Object value) {
    	GetterSetterDynaProperty<Object> gsdp = (GetterSetterDynaProperty<Object>)dynaPropertyMap.get(name);
    	if(gsdp != null && gsdp.setter != null)
    		gsdp.setter.set(value);
    }

    public void remove(String name, String key) {
    	GetterSetterDynaProperty<?> gsdp = dynaPropertyMap.get(name);
    	if(gsdp != null && gsdp.isMapped() && gsdp.getter != null) {
    		Object bean = gsdp.getter.get();
    		if(bean instanceof Map<?,?>)
    			castMap(bean).remove(key);
    	}
    }

    public DynaClass getDynaClass() {
        return dynaClass;
    }

    public Object get(String name, int index) {
    	GetterSetterDynaProperty<?> gsdp = dynaPropertyMap.get(name);
    	if(gsdp != null && gsdp.isIndexed() && gsdp.getter != null) {
    		Object bean = gsdp.getter.get();
    		try {
            	return ReflectionUtils.getElement(bean, index);
            } catch(IndexOutOfBoundsException e) {
            } catch(ClassCastException e) {
            }
    	}
    	return null;
    }

    public Object get(String name, String key) {
    	GetterSetterDynaProperty<?> gsdp = dynaPropertyMap.get(name);
    	if(gsdp != null && gsdp.isMapped() && gsdp.getter != null) {
    		Object bean = gsdp.getter.get();
    		if(bean instanceof Map<?,?>)
    			return castMap(bean).get(key);
    	}
    	return null;
    }

    public Object get(String name) {
    	GetterSetterDynaProperty<?> gsdp = dynaPropertyMap.get(name);
    	if(gsdp != null && gsdp.getter != null)
    		return gsdp.getter.get();
    	return null;
    }

    public boolean contains(String name, String key) {
    	GetterSetterDynaProperty<?> gsdp = dynaPropertyMap.get(name);
    	if(gsdp != null && gsdp.isMapped() && gsdp.getter != null) {
    		Object bean = gsdp.getter.get();
    		if(bean instanceof Map<?,?>)
    			return castMap(bean).containsKey(key);
    	}
    	return false;
    }

    @SuppressWarnings("unchecked")
	protected Map<String,Object> castMap(Object map) {
    	return (Map<String,Object>) map;
    }
}
