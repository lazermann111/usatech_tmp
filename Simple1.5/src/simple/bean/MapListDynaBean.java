/*
 * MapListDynaBean.java
 *
 * Created on May 27, 2004, 11:09 AM
 */

package simple.bean;

import java.util.List;
import java.util.Map;

/**
 *
 * @author  Brian S. Krug
 */
public class MapListDynaBean extends AbstractDynaBean {
     /** Holds the underlying Map object */
     private Map<String,Object> mapList;
     
     /** Holds value of property prefix. */
     private String prefix;
     
     /** Holds value of property suffix. */
     private String suffix;
     
     /** Holds value of property tagKey. */
     private String tagKey;
     
     /** Holds value of property textKey. */
     private String textKey;

    /** Creates a new instance of MapListDynaBean 
      * @param prefix The String prepended to a sub-element's tag that is used as the key entry for
      * the list of same tag name sub-elements.
      * @param suffix The String appended to a sub-element's tag that is used as the key entry for
      * the list of same tag name sub-elements.
      * @param tagKey The string used as the key for entering the name of the tag into the
      * Map object.
      * @param textKey The string used as the key for entering the character body content of an XML element into the
      * Map object. Content broken by sub-elements is concatenated together.
     */
    public MapListDynaBean(Map<String,Object> mapList, String prefix, String suffix, String tagKey, String textKey) {
        this.mapList = mapList;
        this.prefix = (prefix == null ? "" : prefix);
        this.suffix = (suffix == null ? "" : suffix);
        this.tagKey = tagKey;
        this.textKey = textKey;
    }
    
    protected String getDynaClassName() {
        return getClass().getName();
    }
    
    @SuppressWarnings("unchecked")
	protected Object getDynaProperty(String name) {
        Object o = null;
        if(mapList.containsKey(name)) {
            o = mapList.get(name);
        } else if(mapList.containsKey(prefix + name + suffix)) {
            o = mapList.get(prefix + name + suffix);
        } else { //try the nested prop
            
        }
        if(o instanceof List<?>) {
            List<Object> list = (List<Object>)o;
			if(list.size() == 1)
				o = new MapListDynaBean((Map<String, Object>) list.get(0), prefix, suffix, tagKey, textKey);
            else for(java.util.ListIterator<Object> iter = list.listIterator(); iter.hasNext(); ) {
					Map<String, Object> m = (Map<String, Object>) iter.next();
                iter.set(new MapListDynaBean(m, prefix, suffix, tagKey, textKey));
            }
        }
        return o;
    }
    
    protected boolean hasDynaProperty(String name) {
        // NOTE: for now don't worry about nested name properties
        return mapList.containsKey(name) || mapList.containsKey(prefix + name + suffix);
    }
    
    protected void setDynaProperty(String name, Object value) {
    }
    
}
