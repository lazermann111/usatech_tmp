/*
 * Condition.java
 *
 * Created on December 23, 2003, 10:18 AM
 */

package simple.bean;

/**
 * Determines whether an object meets a certain condition.
 *
 * @author  Brian S. Krug
 */
public interface Condition {
    /** Checks the primary object for validity using the specified operator and the second object (if necessary) for
     * determining whether it meets this condition.
     * @param o1 The primary object
     * @param operatorIndex The index of the operator to use to for this check
     * @param o2 The second object
     * @return Whether the specified objects pass the condition
     */
    public boolean check(Object o1, int operatorIndex, Object o2) ;
    /** Returns an array of all the operators handled by this Condition. The index of the operator is
     * passed back to this object when the <CODE>check()</CODE> method is called.
     * @return An array of valid operator names for this Condition object
     */
    public String[] getOperators() ;
}
