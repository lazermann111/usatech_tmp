/*
 * AbstractDynaBean.java
 *
 * Created on November 6, 2003, 10:51 AM
 */

package simple.bean;

import java.util.Map;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;

/** Provides limited implementation of DynaBean's methods to simply sub-classing. A
 * sub-class needs only implement four methods: hasDynaProperty, setDynaProperty,
 * getDynaProperty, and getDynaClassName.
 * @author Brian S. Krug
 */
public abstract class AbstractDynaBean implements DynaBean {
    protected DynaClass dynaClass;

    /** Creates a new instance of AbstractDynaBean */
    public AbstractDynaBean() {
    }

    protected abstract boolean hasDynaProperty(String name) ;
    protected abstract void setDynaProperty(String name, Object value) ;
    protected abstract Object getDynaProperty(String name) ;
    protected abstract String getDynaClassName() ;
    protected DynaProperty[] getDynaClassProperties() {
        return null;
    }

    public void set(String name, int index, Object value) {
        //set(name + NestedNameTokenizer.INDEXED_DELIM + index + NestedNameTokenizer.INDEXED_DELIM2, value);
    	Object bean = get(name);
    	try {
			ReflectionUtils.setElement(bean, index, value);
		} catch(ConvertException e) {
			throw new ConversionException(e);
		}
    }

    public void set(String name, String key, Object value) {
        set(name + NestedNameTokenizer.MAPPED_DELIM + key + NestedNameTokenizer.MAPPED_DELIM2, value);
    }

    public void set(String name, Object value) {
        if(hasDynaProperty(name)) {
            setDynaProperty(name, value);
        }
    }

    public void remove(String name, String key) {
        Object m = get(name);
        if(m instanceof Map<?,?>) ((Map<?,?>)m).remove(key);
        else throw new IllegalArgumentException("Property '" + name + "' did not return a java.util.Map object");
    }

    public DynaClass getDynaClass() {
        if(dynaClass == null) dynaClass = new InnerDynaClass();
        return dynaClass;
    }

    public Object get(String name, int index) {
        //return get(name + NestedNameTokenizer.INDEXED_DELIM + index + NestedNameTokenizer.INDEXED_DELIM2);
    	Object bean = get(name);
    	try {
        	return ReflectionUtils.getElement(bean, index);
        } catch(IndexOutOfBoundsException e) {
        	return null;
        } catch(ClassCastException e) {
        	return null;
        }
    }

    public Object get(String name, String key) {
        return get(name + NestedNameTokenizer.MAPPED_DELIM + key + NestedNameTokenizer.MAPPED_DELIM2);
    }

    public Object get(String name) {
        if(hasDynaProperty(name)) {
            return getDynaProperty(name);
        }
        return null;
    }

    public boolean contains(String name, String key) {
        Object m = get(name);
        if(m instanceof Map<?,?>) return ((Map<?,?>)m).containsKey(key);
        else throw new IllegalArgumentException("Property '" + name + "' did not return a java.util.Map object");
    }

    protected class InnerDynaClass implements DynaClass {
        /** Creates a new instance of InnerDynaClass */
        public InnerDynaClass() {
        }

        public DynaProperty[] getDynaProperties() {
            return getDynaClassProperties();
        }

        public DynaProperty getDynaProperty(String name) {
            if(!hasDynaProperty(name)) return null;
            DynaProperty[] dps = getDynaProperties();
            if(dps != null) for(int i = 0; i < dps.length; i++) {
                if(dps[i].getName().equals(name)) return dps[i];
            }
            return new DynaProperty(name, Object.class);
        }

        public String getName() {
            return getDynaClassName();
        }

        public org.apache.commons.beanutils.DynaBean newInstance() throws IllegalAccessException, InstantiationException {
            return null;
        }
    }
}
