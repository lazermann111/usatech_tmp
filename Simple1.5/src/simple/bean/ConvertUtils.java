package simple.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import simple.lang.Initializer;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.text.WithDefaultFormat;
import simple.text.WithDefaultNumberFormat;
import simple.util.CollectionUtils;
import simple.util.PrimitiveArrayList;
import simple.util.StaticCollection;

/** Provides utility method for formatting Objects into Strings using a specified Format.
 * For performance, this class caches each Format it creates.
 * @author Brian S. Krug
 */
public class ConvertUtils {
	
	public static enum ConversionFormat
	{
		CHOICE,
		NULL,
		MESSAGE,
		MATCH,
		TEXTFILTER,
		PREPARE,
		LITERAL,
		ARRAY,
		HTML,
		CDATA
	}
	
    private static simple.io.Log log;
	private static final Map<Class<?>, ConverterFactory<?>> converterFactories = new ConcurrentHashMap<Class<?>, ConverterFactory<?>>(16, 0.75f, 1);
	private static final Map<String, Format> formatKeys = new ConcurrentHashMap<String, Format>(16, 0.75f, 1);
	private static final Map<Format, String> formatValues = new ConcurrentHashMap<Format, String>(16, 0.75f, 1);
	private static final Map<String, Class<? extends Format>> formatClasses = new ConcurrentHashMap<String, Class<? extends Format>>(16, 0.75f, 1);
	protected static final Map<Currency, String> currencySigns = new HashMap<Currency, String>();
	protected static final Class<?>[] STRING_ARG = new Class[] { String.class };
	protected static final Class<?>[] LOCALE_ARG = new Class[] { Locale.class };
	protected static final Class<?>[] STRING_AND_LOCALE_ARG = new Class[] { String.class, Locale.class };
	private static final Map<Class<?>, Converter<?>> converters = new ConcurrentHashMap<Class<?>, Converter<?>>(64, 0.9f, 1);
	private static final Map<Class<?>, Enumerator<?>> enumerators = new ConcurrentHashMap<Class<?>, Enumerator<?>>(16, 0.75f, 1);
    protected static final String LIST_DELIMS = new String(StringUtils.STANDARD_DELIMS);
	protected static final Set<Character> FORMAT_TYPE_SEPARATORS = Collections.unmodifiableSet(new HashSet<Character>(StaticCollection.create(new Character[] { ':', ',', ';' })));
    protected static final AtomicInteger nonStandardFormatId = new AtomicInteger(0);
	protected static final Class<?>[] CHECK_CLASSES = new Class[] { String.class, Integer.class, Long.class, Byte.class, Double.class, Float.class, Short.class, Boolean.class, Date.class };
	protected static final AtomicBoolean SANITY_CHECK = new AtomicBoolean(false);
	protected static boolean formatWhitelisting;
	protected static final Initializer<Exception> INITIALIZER = new Initializer<Exception>() {
		@Override
		protected void doInitialize() throws Exception {
			registerFormat(ConversionFormat.CHOICE.name(), java.text.ChoiceFormat.class);
			registerFormat(ConversionFormat.NULL.name(), simple.text.NullFormat.class);
			registerFormat(ConversionFormat.MESSAGE.name(), simple.text.MessageFormat.class);
			registerFormat(ConversionFormat.MATCH.name(), simple.text.MatchFormat.class);
			registerFormat(ConversionFormat.TEXTFILTER.name(), simple.text.FilterFormat.class);
			registerFormat(ConversionFormat.PREPARE.name(), simple.text.PrepareFormat.class);
			registerFormat(ConversionFormat.LITERAL.name(), simple.text.LiteralFormat.class);
			registerFormat(ConversionFormat.ARRAY.name(), simple.text.ArrayFormat.class);
			registerFormat(ConversionFormat.HTML.name(), simple.text.HtmlFormat.class);
			registerFormat(ConversionFormat.CDATA.name(), simple.text.CDataFormat.class);

			// get the log after registering formats so that if the log uses those formats they are available
			log = simple.io.Log.getLog();

			// load converters from list
			loadConverters();

			// load currencies
			loadCurrencies();

			loadFormats();
		}

		@Override
		protected void doReset() {
			// do nothing
		}
	};

    static {
		try {
			INITIALIZER.initialize();
		} catch(InterruptedException e) {
			System.err.println(new Date() + " - could not load converters");
			e.printStackTrace(System.err);
		} catch(Exception e) {
			System.err.println(new Date() + " - could not load converters");
			e.printStackTrace(System.err);
		}
	}

	protected static void loadConverters() throws IOException {
		int converterCnt = 0;
		int factoryCnt = 0;
		InputStream in = null;
		InputStreamReader isr = null;
		BufferedReader reader = null;
		try {
			in = ConvertUtils.class.getClassLoader().getResourceAsStream("simple/bean/converters/converters.lst");
			if(in == null) {
				log.error("Could not find 'simple/bean/converters/converters.lst' in class path. No converters were loaded");
				return;
			}
			isr = new InputStreamReader(in);
			reader = new BufferedReader(isr);
			String line;
			while((line = reader.readLine()) != null) {
				line = line.trim();
				if(line.length() == 0)
					continue;
				try {
					Object o = Class.forName(line).newInstance();
					if(o instanceof Converter<?>) {
						register((Converter<?>) o);
						converterCnt++;
					} else if(o instanceof ConverterFactory<?>) {
						registerFactory((ConverterFactory<?>) o);
						factoryCnt++;
					} else
						log.warn("Class '" + line + "' is neither a Converter nor ConverterFactory");
				} catch(IllegalAccessException e) {
					log.info("Could not load Converter of class '" + line + "' because " + e.getClass().getName() + ": " + e.getMessage());
				} catch(ClassNotFoundException e) {
					log.info("Could not load Converter of class '" + line + "' because " + e.getClass().getName() + ": " + e.getMessage());
				} catch(InstantiationException e) {
					log.info("Could not load Converter of class '" + line + "' because " + e.getClass().getName() + ": " + e.getMessage());
				} catch(NoClassDefFoundError e) {
					log.info("Could not load Converter of class '" + line + "' because " + e.getClass().getName() + ": " + e.getMessage());
				}
			}
		} finally {
			if (reader != null)
				reader.close();
			else if (isr != null)
				isr.close();
			else if (in != null)
				in.close();
		}
		if(log.isInfoEnabled())
			log.info("Loaded " + converterCnt + " converters and " + factoryCnt + " factories");
    }

	protected static void loadFormats() throws IOException {
		InputStream in = ConvertUtils.class.getClassLoader().getResourceAsStream("simple/bean/formats.lst");
		if(in == null)
			return;
		int formatCnt = 0;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(in));
			String line;
			while((line = reader.readLine()) != null) {
				line = line.trim();
				if(line.length() == 0)
					continue;
				try {
					Class<? extends Format> cls = Class.forName(line).asSubclass(Format.class);
					registerFormat(line, cls);
					formatCnt++;
				} catch(ClassCastException e) {
					log.info("Could not register Format of class '" + line + "' because " + e.getClass().getName() + ": " + e.getMessage());
				} catch(ClassNotFoundException e) {
					log.info("Could not register Format of class '" + line + "' because " + e.getClass().getName() + ": " + e.getMessage());
				} catch(NoClassDefFoundError e) {
					log.info("Could not register Format of class '" + line + "' because " + e.getClass().getName() + ": " + e.getMessage());
				}
			}
			formatWhitelisting = true;
		} finally {
			if (reader != null)
				reader.close();
			in.close();
		}
		if(log.isInfoEnabled())
			log.info("Registered " + formatCnt + " Formats");
	}

	protected static void loadCurrencies() {
		for(Locale locale : Locale.getAvailableLocales()) {
			try {
				Currency cur = Currency.getInstance(locale);
				if(cur == null)
					continue;
				String sign = cur.getSymbol(locale);
				String oldSign = currencySigns.put(cur, sign);
				if(oldSign != null && sign.length() > oldSign.length())
					currencySigns.put(cur, oldSign);
			} catch(IllegalArgumentException e) {
				if(log.isDebugEnabled())
					log.debug("Locale '" + locale.toString() + "' does not have a default currency");
			}
		}
	}

	protected static void checkConvertersForReload() {
		if(INITIALIZER.isRunning())
			return;
		if(!INITIALIZER.isSuccess())
			try {
				INITIALIZER.initialize();
			} catch(InterruptedException e) {
				System.err.println(new Date() + " - could not load converters");
				e.printStackTrace(System.err);
			} catch(Exception e) {
				System.err.println(new Date() + " - could not load converters");
				e.printStackTrace(System.err);
			}
		else if(!converterSanityCheck()) {
			INITIALIZER.reset();
			checkConvertersForReload();
		}
	}

	protected static boolean converterSanityCheck() {
		if(SANITY_CHECK.get())
			return true;
		for(Class<?> cls : CHECK_CLASSES) {
			if(!converters.containsKey(cls))
				return false;
		}
		SANITY_CHECK.set(true);
		return true;
	}

    /** Never create new ConvertUtils */
    private ConvertUtils() {
    }

    /**
     * Formats the specified object using the Format indicated by the formatString.
     * The formatString can be one of the following: NUMBER, CURRENCY, PERCENT,
     * or DATE or it can be a fully qualified class name of a class extending
     * java.text.Format. Including a colon after the type or class name will
     * cause everything beyond the colon to be passed as the constructor argument
     * of the specified format. Thus, to format an object using a date format
     * of "MM/yyyy", pass "DATE:MM/yyyy" as the formatString.
     *
     * @param object The object to format
     * @param formatString The String specifying which Format to use. It is of the
     * form <i>formatTypeOrClass</i>:<i>pattern</i>
     * @return the object formatted as specified or an empty String if the
     * object is null or the format is invalid or not found
     */
    public static String formatObject(Object object, String formatString) {
		return formatObject(object, formatString, null, false);
	}

	public static String formatObject(Object object, String formatString, Locale locale) {
		return formatObject(object, formatString, locale, false);
    }
	
	public static String formatObject(Object object, String formatString, boolean ignoreErrors) {
		return formatObject(object, formatString, null, ignoreErrors);
    }

	public static String formatObject(Object object, String formatString, Locale locale, boolean ignoreErrors) {
        Format f;
        if(formatString == null || formatString.length() == 0)
        	f = null;
        else
			f = getFormat(formatString, locale);
        return formatObject(object, f, ignoreErrors);
    }

    public static String formatObject(Object object, Format format) {
    	return formatObject(object, format, false);
    }

    public static String formatObject(Object object, Format format, boolean ignoreErrors) {
    	if(format == null) {
            if(object == null) return "";
            else try {
                return ConvertUtils.convert(String.class, object);
            } catch(ConvertException e) {
                return object.toString();
            }
        } else {
        	if(format instanceof DateFormat) {
        		if(object instanceof Calendar) {
        			Calendar cal = (Calendar) object;
        			((DateFormat)format).setCalendar(cal);
        			object = cal.getTime();
        		} else {
		            try {
		            	object = ConvertUtils.convert(Date.class, object);
		            } catch(ConvertException e) {
		                log.warn("Could not convert '" + object + "' to a Date; attempting to format it anyway");
		            }
        		}
	        } else if(format instanceof NumberFormat) {
	            try {
					object = ConvertUtils.convert(Number.class, object);
	            } catch(ConvertException e) {
	                log.warn("Could not convert '" + object + "' to a Number; attempting to format it anyway");
	            }
	        }
	        try {
	        		if (format instanceof DecimalFormat) {
		        		String formattedValue = format.format(object);
		        		if (formattedValue != null && formattedValue.indexOf("?") > -1) {
		        			DecimalFormat df = (DecimalFormat) format;
		        			//additional logging for invalid currency values such as "8 822,64 ???." in "MORE Money" field on the MORE website
		        			StringBuilder sb = new StringBuilder("Found ? symbol in formatted number '").append(formattedValue).append("'"); 
		        			if (df.getCurrency() != null)
		        				sb.append(", currency code: '").append(df.getCurrency().getCurrencyCode()).append("'");
		        			if (df.getDecimalFormatSymbols() != null) {
		        				sb.append(", currency symbol: '").append(df.getDecimalFormatSymbols().getCurrencySymbol()).append("'"); 
		        				sb.append(", decimal separator: '").append(df.getDecimalFormatSymbols().getDecimalSeparator() + "'");
		        			}
		        			log.warn(sb.toString());
		        		}
		            return formattedValue;
	        		} else
	        			return format.format(object);
	        } catch(IllegalArgumentException iae) {
	            //not all format implementations handle nulls
	            if(object == null) return "";
	            else if(ignoreErrors) {
	            	log.warn("Could not format '" + object + "' (class " + object.getClass().getName() + ") using format string of " + getFormatString(format) + ";ignoring exception and returning null", iae);
	                return "";
	            } else {
	                log.warn("Could not format '" + object + "' (class " + object.getClass().getName() + ") using format string of " + getFormatString(format), iae);
	                throw iae;
	            }
	        }
        }
    }

    /**
     * Returns the Format indicated by the formatString. First this method
     * checks its cache to see if it already has a format matching the
     * specified formatString. If it does not then it creates one.
     * The formatString can be one of the following: NUMBER, CURRENCY, PERCENT,
     * or DATE or it can be a fully qualified class name of a class extending
     * java.text.Format. Including a colon after the type or class name will
     * cause everything beyond the colon to be passed as the constructor argument
     * of the specified format. Thus, to format an object using a date format
     * of "MM/yyyy", pass "DATE:MM/yyyy" as the formatString.
     *
     * @param formatString The String specifying which Format to return. It is of the
     * form <i>formatTypeOrClass</i>:<i>pattern</i>
     * @return the Format object or null if formatString is invalid
     *
     */
    public static Format getFormat(String formatString) {
		return getFormat(formatString, (Locale) null);
	}

	public static Format getFormat(String formatString, Locale locale) {
        int i = simple.text.StringUtils.indexOf(formatString, FORMAT_TYPE_SEPARATORS);
        String type;
        String pattern = null;
        if(i < 0) {
            type = formatString;
        } else {
            type = formatString.substring(0,i);
            pattern = formatString.substring(i+1);
        }
		return getFormat(type, pattern, locale);
    }

    /**
     * Returns the Format indicated by the type and pattern paramaters. First this method
     * checks its cache to see if it already has a format matching the
     * specified type and pattern. If it does not then it creates one.
     * The type can be one of the following: NUMBER, CURRENCY, PERCENT,
     * or DATE or it can be a fully qualified class name of a class extending
     * java.text.Format. If the pattern is not null it is passed as the constructor argument
     * when creating the new Format object of the specified type.
     * Thus, to format an object using a date format
     * of "MM/yyyy", pass "DATE" as type and "MM/yyyy" as the pattern.
     *
     * @param type The Format type (a class name or NUMBER, CURRENCY, PERCENT, or DATE)
     * @param pattern The value passed to the Format constructor
     * @return the Format object or null if type is invalid
     *
     */
    public static Format getFormat(String type, String pattern) {
		return getFormat(type, pattern, null);
	}

	public static Format getFormat(String type, String pattern, Locale locale) {
		if(type == null)
			return null;
        type = type.trim();
        String key;
        if(pattern != null) {
			// pattern = pattern.trim();
            key = type + ":" + pattern;
        } else key = type;
        Format f = formatKeys.get(key);
        if(f == null) {
			if(locale == null)
				locale = Locale.getDefault();
            if("NUMBER".equalsIgnoreCase(type)) {
				f = getNumberFormat(pattern, locale);
            } else if("CURRENCY".equalsIgnoreCase(type)) {
				f = getCurrencyFormat(pattern, locale);
            } else if("PERCENT".equalsIgnoreCase(type)) {
				f = getPercentFormat(pattern, locale);
            } else if("DATE".equalsIgnoreCase(type)) {
				if(pattern == null)
					f = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, locale);
				else
					f = new SimpleDateFormat(pattern, locale);
			} else if("DATETZ".equalsIgnoreCase(type)) {
				if(pattern == null)
					f = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, locale);
				else {
					int i = simple.text.StringUtils.indexOf(pattern, ":;".toCharArray());
					String tz;
					if(i < 0) {
						tz = pattern;
					} else {
						tz = pattern.substring(0, i);
						pattern = pattern.substring(i + 1);
					}

					DateFormat dateFormat;
					if(pattern == null)
						dateFormat = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, locale);
					else
						dateFormat = new SimpleDateFormat(pattern, locale);
					TimeZone timeZone = TimeZone.getTimeZone(tz);
					if(!"GMT".equalsIgnoreCase(tz) && "GMT".equalsIgnoreCase(timeZone.getID()))
						log.warn("Could not find timezone='" + tz + "'");
					dateFormat.setTimeZone(timeZone);
					f = dateFormat;
				}
            } else {
				int pos = type.indexOf('-');
				if(pos >= 0) {
					String def = type.substring(pos + 1);
					type = type.substring(0, pos);
					f = getFormat(type, pattern, locale);
					if(f instanceof NumberFormat)
						try {
							f = new WithDefaultNumberFormat((NumberFormat) f, ConvertUtils.convert(BigDecimal.class, def));
						} catch(ConvertException e) {
							log.warn("Could not convert '" + def + "' to number for Format type='" + type + "'", e);
							return null;
						}
					else
						f = new WithDefaultFormat(f, def);
				} else {
					try {
						Class<? extends Format> fc = formatClasses.get(type.toUpperCase());
						if(fc == null) {
							if(formatWhitelisting)
								throw new SecurityException("Class '" + type + "' is not on the whitelist for formats and cannot be used");						
							Class<?> c = Class.forName(type);
							if(c != null)
								fc = c.asSubclass(Format.class);
						}
						if(fc != null) {
							Constructor<? extends Format> constructor;
							if(pattern == null) {
								constructor = ReflectionUtils.findConstructor(fc, LOCALE_ARG);
								if(constructor != null)
									f = constructor.newInstance(locale);
								else {
									constructor = ReflectionUtils.findConstructor(fc);
									if(constructor != null)
										f = constructor.newInstance();
									else {
										log.warn("Could not find or create Format for type='" + type + "'");
										return null;
									}
								}
							} else {
								constructor = ReflectionUtils.findConstructor(fc, STRING_AND_LOCALE_ARG);
								if(constructor != null)
									f = constructor.newInstance(pattern, locale);
								else {
									constructor = ReflectionUtils.findConstructor(fc, STRING_ARG);
									if(constructor != null)
										f = constructor.newInstance(pattern);
									else {
										log.warn("Could not find or create Format for type='" + type + "' with  a pattern");
										return null;
									}
								}
							}
						}
					} catch(ClassNotFoundException e) {
						log.warn("Could not find or create Format for type='" + type + "'", e);
						return null;
					} catch(InstantiationException e) {
						log.warn("Could not find or create Format for type='" + type + "'", e);
						return null;
					} catch(IllegalAccessException e) {
						log.warn("Could not find or create Format for type='" + type + "'", e);
						return null;
					} catch(IllegalArgumentException e) {
						log.warn("Could not find or create Format for type='" + type + "'", e);
						return null;
					} catch(SecurityException e) {
						log.warn("Could not find or create Format for type='" + type + "'", e);
						return null;
					} catch(InvocationTargetException e) {
						log.warn("Could not find or create Format for type='" + type + "'", e);
						return null;
					} catch(ClassCastException e) {
						log.warn("Could not find or create Format for type='" + type + "'", e);
						return null;
					}
				}
            }
            if(f instanceof DateFormat && !(f instanceof ThreadSafeDateFormat)) { // DateFormats are NOT thread-safe
            	f = new ThreadSafeDateFormat((DateFormat)f);
            }

            formatKeys.put(key,f);
			formatValues.put(f, key);
        }
        return f;
    }

	protected static Format getNumberFormat(String pattern, Locale locale) {
		if(pattern == null)
			return NumberFormat.getNumberInstance(locale);
		else {
			String[] parts = StringUtils.splitQuotesAnywhere(pattern, '|', '\'', false);
			DecimalFormat df = new DecimalFormat(parts[0], new DecimalFormatSymbols(locale));
			if(parts.length > 1 && !StringUtils.isBlank(parts[1])) {
				RoundingMode rm = convertSafely(RoundingMode.class, parts[1], null);
				if(rm != null)
					df.setRoundingMode(rm);
			}
			return df;
		}
	}

	protected static Format getCurrencyFormat(String pattern, Locale locale) {
		//try and avoid issues with invalid currency values such as "8 822,64 ???." in "MORE Money" field on the MORE website
		Locale currencyLocale;
		if (locale == null || locale.toString() == null || !locale.toString().startsWith("en_"))
			currencyLocale = Locale.US;
		else
			currencyLocale = locale;
		NumberFormat f = NumberFormat.getCurrencyInstance(currencyLocale);
		if(f instanceof DecimalFormat && !StringUtils.isBlank(pattern))
			try {
				DecimalFormat df = (DecimalFormat) f;
				Currency currency = Currency.getInstance(pattern);
				df.setCurrency(currency);
				String sign = currencySigns.get(currency);
				if(sign != null && !sign.equals(currency.getCurrencyCode()) && !df.getPositivePrefix().contains("\u00A4\u00A4") && !df.getPositiveSuffix().contains("\u00A4\u00A4")) {
					df.setPositivePrefix(df.getPositivePrefix().replace(df.getDecimalFormatSymbols().getCurrencySymbol(), sign));
					df.setNegativePrefix(df.getNegativePrefix().replace(df.getDecimalFormatSymbols().getCurrencySymbol(), sign));
					String ps = df.getPositiveSuffix().replace(df.getDecimalFormatSymbols().getCurrencySymbol(), sign);
					df.setPositiveSuffix(ps + " " + currency.getCurrencyCode());
					String ns = df.getNegativeSuffix().replace(df.getDecimalFormatSymbols().getCurrencySymbol(), sign);
					if(ns.startsWith(ps))
						ns = ns.substring(0, ps.length()) + " " + currency.getCurrencyCode() + ns.substring(ps.length());
					else
						ns = ns + " " + currency.getCurrencyCode();
					df.setNegativeSuffix(ns);
				}
			} catch(IllegalArgumentException e) {
				log.warn("Could not find currency='" + pattern + "'", e);
			}
		return f;
	}

	public static String getCurrencySymbol(Currency currency) {
		String sign = currencySigns.get(currency);
		if(sign != null)
			return sign;
		return currency.getSymbol();
	}

	protected static Format getPercentFormat(String pattern, Locale locale) {
		return NumberFormat.getPercentInstance(locale);
	}
	
    /**
     * Returns the string that represents the given Format in the form 'type:pattern'.
     * The type can be one of the following: NUMBER, CURRENCY, PERCENT,
     * or DATE or it can be a fully qualified class name of a class extending
     * java.text.Format. This method is the inverse of getFormat(String).
     *
     * @param format The Format object
     * @return the String representation of the Format object
     *
     */
    public static String getFormatString(Format format) {
        if(format == null) return null;
        String key = formatValues.get(format);
        if(key != null) return key;
        key = createFormatString(format);
        if(format instanceof DateFormat && !(format instanceof ThreadSafeDateFormat)) { // DateFormats are NOT thread-safe
        	format = new ThreadSafeDateFormat((DateFormat)format);
        }
        formatKeys.put(key, format);
        formatValues.put(format, key);
        return key;
    }

    protected static String createFormatString(Format format) {
        if(format instanceof NumberFormat) {
            if(format.equals(NumberFormat.getNumberInstance())) {
                return "NUMBER";
            } else if(format.equals(NumberFormat.getCurrencyInstance())) {
                return "CURRENCY";
            } else if(format.equals(NumberFormat.getPercentInstance())) {
                return "PERCENT";
            } else if(format instanceof DecimalFormat){
                return "NUMBER:" + ((DecimalFormat)format).toPattern();
            } else {
                return getNonStandardFormatString(format);
            }
        } else if(format instanceof DateFormat) {
        	if(format instanceof ThreadSafeDateFormat)
        		format = ((ThreadSafeDateFormat)format).getDelegate();
            if(format.equals(DateFormat.getDateTimeInstance())) {
                return "DATE";
            } else if(format instanceof SimpleDateFormat) {
                return "DATE:" + ((SimpleDateFormat)format).toPattern();
            } else {
                return getNonStandardFormatString(format);
            }
        } else {
            return getNonStandardFormatString(format);
        }
    }

    protected static String getTypeForFormat(Format format) {
        Class<? extends Format> clazz = format.getClass();
		for(Map.Entry<String, Class<? extends Format>> e : formatClasses.entrySet()) {
            if(clazz.equals(e.getValue())) return e.getKey();
        }
        return null;
    }

    /**
     * Attempts to returns the string that represents the given Format in the form 'type:pattern'.
     * The type is the fully qualified class name of a class extending
     * java.text.Format. This method is the inverse of getFormat(String).
     *
     * @param format The Format object
     * @return the String representation of the Format object
     *
     */
    protected static String getNonStandardFormatString(Format format) {
        String type = getTypeForFormat(format);
        if(type == null) type = format.getClass().getName();
        //see if format has a toPattern method & a constructor with a single string
        try {
            format.getClass().getConstructor(STRING_ARG);
            String pattern = (String)format.getClass().getMethod("toPattern", (Class[])null).invoke(format, (Object[])null);
            return type + ":" + pattern;
        } catch(NoSuchMethodException e) {
        } catch(IllegalArgumentException e) {
        } catch(SecurityException e) {
        } catch(IllegalAccessException e) {
        } catch(InvocationTargetException e) {
        } catch(ClassCastException e) {
        }
        // last resort - make up a pattern and then put it in the formatters cache
        int id = nonStandardFormatId.incrementAndGet();
        return type + ":ID=" + id;
    }

    /**
     *  Registers the given class as the Format class for the specified case-insensitive type name
     *
     *  @throws IllegalArgumentException If the provided class is not a subclass of <code>java.text.Format</code>
     */
    public static void registerFormat(String typeName, Class<? extends Format> formatClass) {
        //if(!Format.class.isAssignableFrom(formatClass)) throw new IllegalArgumentException("Specified class MUST extend java.text.Format");
        formatClasses.put(typeName.toUpperCase(), formatClass);
    }

    /**
     *  De-registers the given case-insensitive type name as a format
     *
     */
    public static void deregisterFormat(String typeName) {
        formatClasses.remove(typeName.toUpperCase());
    }

    /** Locates a converter for the given class. Use the register() method to
     * assign converters to certain classes.
     * @param cls The class for which to find a Converter
     * @return The appropriate Converter or null
     */
    @SuppressWarnings("unchecked")
	public static <Target> Converter<Target> lookupConverter(Class<Target> cls) {
        cls = (Class<Target>)convertToWrapperClass(cls);
        Converter<Target> converter = (Converter<Target>)converters.get(cls);
        if(converter != null)
        	return converter;
     	//enums are not statically initialized until they are used
    	if(cls.isEnum()) {
    		cls.getEnumConstants();
    		converter = (Converter<Target>)converters.get(cls);
    		if(converter != null)
            	return converter;
     	}
        ConverterFactory<? super Target> factory = lookupConverterFactory(cls);
        if(factory != null) {
            try {
                converter = factory.createConverter(cls);
                register(converter);
            } catch (ConvertException e) {
                log.warn("ConverterFactory could not create converter for type '" + cls.getName() + "'");
            }
        }
        //if(log.isDebugEnabled() && converter == null) log.debug("Could not find " + cls + " in " + converters);
        return converter;
    }

    /** Locates an enumerator for the given class. Use the registerEnumerator() method to
     * assign enumerators to certain classes.
     * @param cls The class for which to find an enumerator
     * @return The appropriate Enumerator or null
     */
    @SuppressWarnings("unchecked")
	public static <Source> Enumerator<Source> lookupEnumerator(Class<Source> cls) {
        cls = (Class<Source>)convertToWrapperClass(cls);
        Enumerator<Source> enumerator = (Enumerator<Source>)enumerators.get(cls);
        if(enumerator != null)
        	return enumerator;
        if(cls != null && cls.getSuperclass() != null)
            return (Enumerator<Source>)lookupEnumerator(cls.getSuperclass());
        return null;
    }
    @SuppressWarnings("unchecked")
	public static Number enumerate(Object object) {
    	if(object == null)
    		return null;
    	Enumerator<Object> enumerator = (Enumerator<Object>)ConvertUtils.lookupEnumerator(object.getClass());
        if(enumerator == null)
        	return null;
    	return enumerator.enumerate(object);
    }
    /** Locates a ConverterFactory for the given class. Use the registerFactory() method to
     * assign ConverterFactories to certain classes.
     * @param cls The class for which to find a ConverterFactory
     * @return The appropriate ConverterFactory or null
     */
	public static <Target> ConverterFactory<? super Target> lookupConverterFactory(Class<Target> cls) {
        ConverterFactory<Target> factory = (ConverterFactory<Target>)converterFactories.get(cls);
        if(factory == null && cls != null && cls.getSuperclass() != null)
            return lookupConverterFactory(cls.getSuperclass());
        return factory;
    }

    /** Locates a formatter for the given class. Use the register() method to
     * assign converters to certain classes.
     * @param cls The class for which to find a Formatter
     * @return The appropriate Formatter or null
     */
    @SuppressWarnings("unchecked")
	public static <S> Formatter<? super S> lookupFormatter(Class<S> cls) {
        if(cls == null)
        	return null;
    	for(Class<?> subcls : ReflectionUtils.getClassHierarchy(cls)) {
    		Converter<?> converter = converters.get(subcls);
            if(converter instanceof Formatter) 
            	return (Formatter<? super S>)converter;
    	}
        return null;
    }

    /** Converts the object to the specified class using a converter registered
     * with the register() method or one registered by default if available. If
     * a convert is not found to convert this object a ConvertException is thrown.
     * If the original object is an array and a special converter is not registered
     * for the array class, then this method attempts to convert each element of
     * the array.
     * @param toClass The target Class
     * @param object The object to convert
     * @throws ConvertException If the object could not be converted
     * @return An object that is an instance of the specified target Class or null
     */
	public static <Target> Target convert(Class<Target> toClass, Object object) throws ConvertException {
		return convert(toClass, null, object);
	}

	@SuppressWarnings("unchecked")
	protected static <Target> Class<Target> convertToWrapperClassTarget(Class<Target> toClass) {
		return (Class<Target>) convertToWrapperClass(toClass);
	}

	@SuppressWarnings("unchecked")
	public static <Target> Target convert(Class<Target> toClass, Class<?> componentType, Object object) throws ConvertException {
		if(object == null)
			return null;
        Class<?> fromClass = object.getClass();
		toClass = convertToWrapperClassTarget(toClass); // use the wrapper class
        //if fromClass is a subclass of or equal to toClass return the object as is
        if(toClass.isAssignableFrom(fromClass)) return toClass.cast(object);
        try {
        	if(Convertible.class.isAssignableFrom(fromClass))
        		return ((Convertible)object).convert(toClass);
            Converter<Target> converter = lookupConverter(toClass);
            if(converter != null) {
                //log.debug("Using converter " + converter + " for " + toClass.getName());
                return converter.convert(object);
            } else if(isCollectionType(toClass)) {
				Collection<?> objectColl = asCollection(object, convertToWrapperClass(toClass.isArray() ? toClass.getComponentType() : componentType == null ? Object.class : componentType));
                if(objectColl == null) {
                    log.warn("asCollection returned null for " + object);
                    return null;
				} else if(toClass.isAssignableFrom(objectColl.getClass()))
					return toClass.cast(objectColl);
                else if(Object[].class.isAssignableFrom(toClass)) {
                    int len = objectColl.size();
                    Object[] retArray = (Object[])Array.newInstance(toClass.getComponentType(), len);
                    Iterator<?> iter = objectColl.iterator();
                    for(int i = 0; iter.hasNext(); i++) retArray[i] = convert(toClass.getComponentType(), iter.next());
                    return toClass.cast(retArray);
                } else if(toClass.isArray()) { //its primitive array
                    int len = objectColl.size();
                    Object retArray = Array.newInstance(toClass.getComponentType(),len);
                    Iterator<?> iter = objectColl.iterator();
                    /*
                    Class compClass = convertToWrapperClass(toClass.getComponentType());
                    int i = 0;
                    while(iter.hasNext()) {
                        Object next = convert(compClass, iter.next());
                        //don't include null entries in primitive array
                        if(next != null) Array.set(retArray, i++, next);
                    }
                    if(i < len)
                    */
                    for(int i = 0; iter.hasNext(); i++) {
                        Object next = convert(toClass.getComponentType(), iter.next());
                        if(next != null) Array.set(retArray, i, next);
                    }
                    return toClass.cast(retArray);
                } else if(Collection.class.isAssignableFrom(toClass)) {
                    //figure out implementation
                    Collection<Object> coll;
                    if(toClass.isInterface() || java.lang.reflect.Modifier.isAbstract(toClass.getModifiers())) {
                        if(toClass.isAssignableFrom(java.util.ArrayList.class)) coll = new java.util.ArrayList<Object>();
                        else if(toClass.isAssignableFrom(java.util.LinkedList.class)) coll = new java.util.LinkedList<Object>();
                        else if(toClass.isAssignableFrom(java.util.HashSet.class)) coll = new java.util.HashSet<Object>();
                        else if(toClass.isAssignableFrom(java.util.TreeSet.class)) coll = new java.util.TreeSet<Object>();
                        else throw new ConvertException("Could not find concrete class that is an instance of '" + toClass + "'", toClass, object);
                    } else {
                        coll = (Collection<Object>)toClass.newInstance();
                    }
                    coll.addAll(objectColl);
                    return toClass.cast(coll);
                } else {
					checkConvertersForReload();
                    throw new ConvertException("Could not find a converter to convert from '" + fromClass.getName() + "' to '" + toClass.getName() + "'", toClass, object);
                }
			} else if(Map.class.isAssignableFrom(toClass)) {
				String s = getString(object, null);
				Map<String, String> map;
				if(toClass.isInterface() || java.lang.reflect.Modifier.isAbstract(toClass.getModifiers())) {
					if(toClass.isAssignableFrom(ConcurrentNavigableMap.class))
						map = new ConcurrentSkipListMap<String, String>();
					else if(toClass.isAssignableFrom(ConcurrentMap.class))
						map = new ConcurrentHashMap<String, String>();
					else if(toClass.isAssignableFrom(NavigableMap.class))
						map = new TreeMap<String, String>();
					else if(toClass.isAssignableFrom(SortedMap.class))
						map = new TreeMap<String, String>();
					else
						map = new LinkedHashMap<String, String>();
				} else
					map = toClass.asSubclass(Map.class).newInstance();
				if(s != null && s.trim().length() > 0) {
					// assume it's a list of key/value pairs
					for(String entry : StringUtils.split(s, LIST_DELIMS.toCharArray(), true)) {
						String[] pair = StringUtils.split(entry, '=', 1);
						map.put(pair[0], pair.length > 1 ? pair[1] : null);
					}
				}
				return toClass.cast(map);
			} else {
				Class<?> redefinedToClass = Class.forName(toClass.getName());
				if(!redefinedToClass.equals(toClass)) {
					log.warn("Multiple versions of the same class exist for '" + toClass.getName() + "'; This classloader=" + ConvertUtils.class.getClassLoader() + "; Other classloader=" + toClass.getClassLoader());
					return (Target) convert(redefinedToClass, componentType, object);
				} else {
					checkConvertersForReload();
					throw new ConvertException("Could not find a converter to convert '" + fromClass.getName() + "' to '" + toClass.getName() + "'; Existing converters=" + converters, toClass, object);
				}
			}
        } catch(ConvertException ce) {
            throw ce;
        } catch (Exception e) {
            throw new ConvertException("Exception occurred while converting '" + object + "' from '" + fromClass.getName() + "' to '" + toClass.getName() + "'", toClass, object, e);
        }
    }

	public static <Target> Target[] convertToArrayNoParse(Class<Target> toClass, Object object) throws ConvertException {
		if(object == null)
			return null;
		if(isIterableType(object.getClass()))
			return convert((Class<Target[]>) Array.newInstance(toClass, 0).getClass(), object);
		Target t = convert(toClass, object);
		Target[] ts = (Target[]) Array.newInstance(toClass, 1);
		ts[0] = t;
		return ts;
	}

	public static String[] convertToStringArrayNoParse(Object object) throws ConvertException {
		if(object == null)
			return null;
		if(isIterableType(object.getClass()))
			return convert(String[].class, object);
		return new String[] { convert(String.class, object) };
	}

	public static boolean isIterableType(Class<?> clazz) {
        if(clazz.isArray()) return true;
        if(Collection.class.isAssignableFrom(clazz)) return true;
        if(java.sql.Array.class.isAssignableFrom(clazz)) return true;
        if(Iterable.class.isAssignableFrom(clazz)) return true;
        if(Iterator.class.isAssignableFrom(clazz)) return true;
        return false;
    }

    public static Iterable<?> asIterable(final Object object) throws ConvertException {
        Class<?> clazz = object.getClass();
        if(object instanceof Convertible) return ((Convertible)object).convert(Iterable.class);
        if(object instanceof Object[]) return Arrays.asList((Object[])object);
        if(clazz.isArray()) return new PrimitiveArrayList(object);
        if(object instanceof Iterator<?>) return new Iterable<Object>() {
        	@SuppressWarnings("unchecked")
			public Iterator<Object> iterator() {
        		return ((Iterator<Object>)object);
        	}
        };
        if(object instanceof Iterable<?>) return ((Iterable<?>)object);
        if(object instanceof java.sql.Array) try {
            return new PrimitiveArrayList(((java.sql.Array)object).getArray());
        } catch(SQLException sqle) {
            throw new ConvertException("Couldn't convert sql array to java array", Object[].class, object, sqle);
        }
        if(object instanceof String) {
        	String s = (String) object;
        	if(s.trim().length() == 0)
        		return null;
            //assume it's a list
            List<String> list = Arrays.asList(StringUtils.split(s, LIST_DELIMS.toCharArray(), true));
            return list;
        }
        return Collections.singletonList(object);
    }
	public static <T> Iterable<T> asIterable(Object object, final Class<T> componentType) throws ConvertException {
		final Iterable<?> delegate = asIterable(object);
		return new Iterable<T>() {
        	public Iterator<T> iterator() {
        		final Iterator<?> iter = delegate.iterator();
        		return new Iterator<T>() {
        			public boolean hasNext() {
        				return iter.hasNext();
        			}
        			public T next() {
        				try {
        					return convert(componentType, iter.next());
        				} catch(ConvertException e) {
        					throw new UndeclaredThrowableException(e);
        				}
        			}
        			public void remove() {
        				iter.remove();
        			}
        		};
        	}
        };
	}
    public static boolean isCollectionType(Class<?> clazz) {
        if(clazz.isArray()) return true;
        if(Collection.class.isAssignableFrom(clazz)) return true;
        if(java.sql.Array.class.isAssignableFrom(clazz)) return true;
        return false;
    }

    public static Collection<?> asCollection(Object object) throws ConvertException {
		if(object == null)
			return Collections.emptyList();
        Class<?> clazz = object.getClass();
        if(object instanceof Convertible) return ((Convertible)object).convert(Collection.class);
        if(object instanceof Object[]) return Arrays.asList((Object[])object);
        if(clazz.isArray()) return new PrimitiveArrayList(object);
        if(object instanceof Collection<?>) return ((Collection<?>)object);
        if(object instanceof java.sql.Array) try {
            return new PrimitiveArrayList(((java.sql.Array)object).getArray());
        } catch(SQLException sqle) {
            throw new ConvertException("Couldn't convert sql array to java array", Object[].class, object, sqle);
        }
        if(object instanceof String) {
        	String s = (String) object;
        	if(s.trim().length() == 0)
				return Collections.emptyList();
            //assume it's a list
            List<String> list = Arrays.asList(StringUtils.split(s, LIST_DELIMS.toCharArray(), true));
            return list;
        }
        return Collections.singletonList(object);
    }

    @SuppressWarnings("unchecked")
	public static <T> Collection<T> asCollection(Object object, Class<T> componentType) throws ConvertException {
        Class<?> clazz = object.getClass();
        if(clazz.isArray()) {
        	if(componentType.isAssignableFrom(clazz.getComponentType()))
        		return Arrays.asList((T[])object);
        	T[] array = CollectionUtils.createArray(componentType, Array.getLength(object));
        	for(int i = 0; i < array.length; i++)
        		array[i] = convert(componentType, Array.get(object, i));
        	return Arrays.asList(array);
        }
        if(object instanceof Set) {
			Set<T> set = new LinkedHashSet<T>();
        	for(Object elem : (Set<T>)object)
        		set.add(convert(componentType, elem));
			return set;
        }
        if(object instanceof Collection) {
        	List<T> list = new ArrayList<T>();
        	for(Object elem : (Collection<T>)object)
        		list.add(convert(componentType, elem));
			return list;
        }
        if(object instanceof java.sql.Array) try {
            return asCollection(((java.sql.Array)object).getArray(), componentType);
        } catch(SQLException sqle) {
            throw new ConvertException("Couldn't convert sql array to java array", Object[].class, object, sqle);
        }
        if(object instanceof String) {
        	String s = (String) object;
        	if(s.trim().length() == 0)
				return Collections.emptyList();
            //assume it's a list
        	String[] sArray = StringUtils.split(s, LIST_DELIMS.toCharArray(), true);
        	T[] array = CollectionUtils.createArray(componentType, sArray.length);
        	for(int i = 0; i < array.length; i++)
        		array[i] = convert(componentType, sArray[i]);
        	return Arrays.asList(array);
        }
        return Collections.singletonList(convert(componentType, object));
    }

    /** Registers a converter to be used to convert from any class to the
     * specified class.
     * @param converter The Converter to register
     */
    public static <Target> void register(Converter<Target> converter){
        log.debug("Registering converter '" + converter.getClass().getName() + "'");
        converters.put(converter.getTargetClass(), converter);
    }

    /** Unregisters the converter assigned to the
     * specified class.
     * @param clazz The Converter to register
     */
    public static void deregister(Class<?> clazz) {
        Converter<?> c = converters.remove(clazz);
        log.debug("Deregistering converter '" + c.getClass().getName() + "'");
    }

    public static void registerFactory(ConverterFactory<?> factory) {
        log.debug("Registering converter factory '" + factory.getClass().getName() + "'");
        converterFactories.put(factory.getBaseClass(), factory);
    }

    @SuppressWarnings("unchecked")
	public static <T> T cast(Object object) {
    	return (T)object;
    }
    /** Registers a converter to be used to convert from any class to the
     * specified class.
     * @param converter The Converter to register
     */
    public static <Source> void registerEnumerator(Enumerator<Source> enumerator){
        log.debug("Registering enumerator '" + enumerator.getClass().getName() + "'");
        enumerators.put(enumerator.getSourceClass(), enumerator);
    }

    /** Unregisters the converter assigned to the
     * specified class.
     * @param clazz The Converter to register
     */
    public static void deregisterEnumerator(Class<?> clazz) {
    	Enumerator<?> c = enumerators.remove(clazz);
        log.debug("Deregistering enumerator '" + c.getClass().getName() + "'");
    }
    /** Returns the "wrapper" class of a primitive class or the original class if the
     * class is not primitive.
     * @param primitiveClass The original Class
     * @return The original class or the "wrapper" class of the original class if the original
     * class is a primitive class
     */
    public static Class<?> convertToWrapperClass(Class<?> primitiveClass) {
        if(!primitiveClass.isPrimitive()) return primitiveClass;
        if(primitiveClass == long.class) return Long.class;
        if(primitiveClass == int.class) return Integer.class;
        if(primitiveClass == short.class) return Short.class;
        if(primitiveClass == byte.class) return Byte.class;
        if(primitiveClass == char.class) return Character.class;
        if(primitiveClass == boolean.class) return Boolean.class;
        if(primitiveClass == double.class) return Double.class;
        if(primitiveClass == float.class) return Float.class;
        if(primitiveClass == void.class) return Void.class;
        throw new RuntimeException("Primitive Class " + primitiveClass.getName() + " is not accounted for");
        //return null;
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static boolean areSimilar(Object o1, Object o2) {
		if(o1 == o2)
			return true;
		else if(o1 == null || o2 == null)
			return false;
		Object o2C;
		try {
			o2C = convert(o1.getClass(), o2);
			if(o2C == null)
				o2C = o2;
			else if(o1 instanceof Comparable)
				return ((Comparable) o1).compareTo(o2C) == 0;
		} catch(ConvertException e) {
			o2C = o2;
		}
		return areEqual(o1, o2C);
	}
    /** Tests for equality accounting for nulls
     * @param o1 The first object
     * @param o2 The second object
     * @return Whether the two objects are equal
     */
    public static boolean areEqual(Object o1, Object o2) {
    	if(o1 == o2)
			return true;
		else if(o1 == null || o2 == null)
			return false;
		/* Using the following line actually takes significantly longer!
		else if(!o1.getClass().isArray() || !o2.getClass().isArray())
			return o1.equals(o2);*/
		// handle arrays
		else if(o1 instanceof Object[] && o2 instanceof Object[])
			return Arrays.deepEquals((Object[]) o1, (Object[]) o2);
		else if(o1 instanceof byte[] && o2 instanceof byte[])
			return Arrays.equals((byte[]) o1, (byte[]) o2);
		else if(o1 instanceof short[] && o2 instanceof short[])
			return Arrays.equals((short[]) o1, (short[]) o2);
		else if(o1 instanceof int[] && o2 instanceof int[])
			return Arrays.equals((int[]) o1, (int[]) o2);
		else if(o1 instanceof long[] && o2 instanceof long[])
			return Arrays.equals((long[]) o1, (long[]) o2);
		else if(o1 instanceof char[] && o2 instanceof char[])
			return Arrays.equals((char[]) o1, (char[]) o2);
		else if(o1 instanceof float[] && o2 instanceof float[])
			return Arrays.equals((float[]) o1, (float[]) o2);
		else if(o1 instanceof double[] && o2 instanceof double[])
			return Arrays.equals((double[]) o1, (double[]) o2);
		else if(o1 instanceof boolean[] && o2 instanceof boolean[])
			return Arrays.equals((boolean[]) o1, (boolean[]) o2);
		//handle collections
		else if(o1 instanceof List<?> && o2 instanceof List<?>) {
			List<?> l1 = (List<?>)o1;
			List<?> l2 = (List<?>)o2;
			if(l1.size() != l2.size())
				return false;
			Iterator<?> i1 = l1.iterator();
			Iterator<?> i2 = l2.iterator();
			while(i1.hasNext()) {
				if(!areEqual(i1.next(), i2.next()))
					return false;
			}
			return true;
		}
		/*		NOTE: sets cannot be handled any different
		else if(o1 instanceof Set && o2 instanceof Set) {
			Set s1 = (Set)o1;
			Set s2 = (Set)o2;
			if(s1.size() != s2.size())
				return false;
			for(Object e1 : s1)
		}
		*/

		else
			return o1.equals(o2);
    }

    /** Converts the given object to the specified type and returns it.
     *  If the object is null or an error occurs, the defaultValue is returned
     * @param object The object to convert
     * @param defaultValue The default value is the object is null
     * @return An object that is an instance of the specified target Class
     */
    public static <Target> Target convertSafely(Class<Target> toClass, Object object, Target defaultValue) {
        try {
			Target t = convert(toClass, object);
			if(t == null)
				return defaultValue;
			else
				return t;
		} catch(ConvertException e) {
			return defaultValue;
		}
    }
    
	/**
	 * Converts the given object to the specified type and returns it.
	 * If the object is null or an error occurs, the defaultValue is returned
	 * 
	 * @param object
	 *            The object to convert
	 * @param defaultValue
	 *            The default value is the object is null
	 * @return An object that is an instance of the specified target Class
	 * @throws ConvertException
	 */
	public static <Target> Target convertDefault(Class<Target> toClass, Object object, Target defaultValue) throws ConvertException {
		Target t = convert(toClass, object);
		if(t == null)
			return defaultValue;
		else
			return t;
	}

	/**
	 * Converts the given object to the specified type and returns it.
	 * If the object is null a ConvertException is thrown.
	 * 
	 * @param object
	 *            The object to convert
	 * @throws ConvertException
	 *             If the object could not be converted or is null
	 * @return An object that is an instance of the specified target Class (never null)
	 */
    public static  <Target> Target convertRequired(Class<Target> toClass, Object object) throws ConvertException {
    	Target t = convert(toClass, object);
		if(t == null)
			throw new ConvertException("Value is required", toClass, null);
        return t;
    }

    protected static final NumberFormat twoDigitFormat = new DecimalFormat("00");

    public static TimeZone createTimeZone(int offsetMillis) {
    	int offsetHours = offsetMillis / (60 * 60 * 1000);
		int offsetMinutes = offsetMillis / (60 * 1000) - (offsetHours * 60);
		StringBuilder id = new StringBuilder();
		id.append("GMT");
		if(offsetMillis < 0)
			id.append('-');
		else
			id.append('+');
		id.append(twoDigitFormat.format(Math.abs(offsetHours)));
		id.append(':');
		id.append(twoDigitFormat.format(Math.abs(offsetMinutes)));
		return new SimpleTimeZone(offsetMillis, id.toString());
    }
	/**
	 * @param time
	 * @param timeZone
	 * @return
	 */
	public static long getLocalTime(long time, String timeZoneGuid) {
		if(timeZoneGuid != null  && (timeZoneGuid=timeZoneGuid.trim()).length() > 0) {
			time += TimeZone.getTimeZone(timeZoneGuid).getOffset(time);
		}
		return time;
	}

	/**
	 * @param time
	 * @param timeZone
	 * @return
	 */
	public static long getLocalTime(long time, TimeZone timeZone) {
		if(timeZone != null) {
			time += timeZone.getOffset(time);
		}
		return time;
	}

    protected static int[] CALENDAR_COPY_FIELDS = new int[] {Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND };

    public static Date getDateUTC(Object object) throws ConvertException {
    	return getDateUTC(convert(Calendar.class, object));
    }

    public static Date getDateUTC(Calendar cal) {
    	if(cal == null)
    		return null;
		return new Date(getMillisUTC(cal));
    }

    public static Date getDateUTC(long timeInMillis) {
    	Calendar cal = Calendar.getInstance();
    	cal.clear();
    	cal.setTimeInMillis(timeInMillis);
    	return getDateUTC(cal);
    }

    public static Date getDateUTC(Date date) {
    	Calendar cal = Calendar.getInstance();
    	cal.clear();
    	cal.setTime(date);
    	return getDateUTC(cal);
    }

    public static long getMillisUTC(Object object) throws ConvertException {
    	return getMillisUTC(convert(Calendar.class, object));
    }

    public static long getMillisUTC(long timeInMillis) {
    	Calendar cal = Calendar.getInstance();
    	cal.clear();
    	cal.setTimeInMillis(timeInMillis);
    	return getMillisUTC(cal);
    }

    public static long getMillisUTC(Calendar cal) {
    	return getMillisUTC(cal.getTimeInMillis(), cal.getTimeZone());
    }

    public static long getMillisUTC(long localTime, String timeZoneGuid) {
    	return getMillisUTC(localTime, TimeZone.getTimeZone(timeZoneGuid));
    }
    public static long getMillisUTC(long localTime, TimeZone timeZone) {
    	/* WARNING: Java doesn't provide a getMillisUTC(long localTime, TimeZone timeZone) function
    	 * most likely because there are ambiguities in determining UTC time from local time around DST switchover.
    	 * Our function will be accurate in most cases except for time around DST switchover where the returned
    	 * UTC time may be off by one hour for an hour.
    	 */
    	return localTime - timeZone.getOffset(localTime - timeZone.getRawOffset());
    }

    /** Converts the given object to a Byte and returns its primitive value
     * @param value The object to convert
     * @throws ConvertException If the object could not be converted or is null
     * @return An byte representation of the value object
     */
    public static byte getByte(Object value) throws ConvertException {
        Byte b = ConvertUtils.convert(Byte.class, value);
        if(b == null) throw new ConvertException("Value is required", byte.class, null);
        return b.byteValue();
    }

    /** Converts the given object to a Byte and returns its primitive value or the
     * default value if the object is null
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @throws ConvertException If the object could not be converted
     * @return An byte representation of the value object
     */
    public static byte getByte(Object value, byte defaultValue) throws ConvertException {
    	Byte b = ConvertUtils.convert(Byte.class, value);
        if(b == null) return defaultValue;
        return b.byteValue();
    }

    /** Converts the given object to a Byte and returns its primitive value.
     *  If the object is null or an error occurs, the defaultValue is returned
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @return A byte representation of the value object
     */
    public static byte getByteSafely(Object value, byte defaultValue) {
        try {
			return getByte(value, defaultValue);
		} catch(ConvertException e) {
			return defaultValue;
		}
    }
    /** Converts the given object to a Character and returns its primitive value
     * @param value The object to convert
     * @throws ConvertException If the object could not be converted or is null
     * @return An char representation of the value object
     */
    public static char getChar(Object value) throws ConvertException {
    	Character c = ConvertUtils.convert(Character.class, value);
        if(c == null) throw new ConvertException("Value is required", char.class, null);
        return c.charValue();
    }

    /** Converts the given object to a Character and returns its primitive value or the
     * default value if the object is null
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @throws ConvertException If the object could not be converted
     * @return An char representation of the value object
     */
    public static char getChar(Object value, char defaultValue) throws ConvertException {
    	Character c = ConvertUtils.convert(Character.class, value);
        if(c == null) return defaultValue;
        return c.charValue();
    }

    /** Converts the given object to a Character and returns its primitive value.
     *  If the object is null or an error occurs, the defaultValue is returned
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @return A char representation of the value object
     */
    public static char getCharSafely(Object value, char defaultValue) {
        try {
			return getChar(value, defaultValue);
		} catch(ConvertException e) {
			return defaultValue;
		}
    }
    /** Converts the given object to a Short and returns its primitive value
     * @param value The object to convert
     * @throws ConvertException If the object could not be converted or is null
     * @return An short representation of the value object
     */
    public static short getShort(Object value) throws ConvertException {
        Short s = ConvertUtils.convert(Short.class, value);
        if(s == null) throw new ConvertException("Value is required", short.class, null);
        return s.shortValue();
    }

	/**
	 * Converts the given object to a Short and returns its primitive value
	 * 
	 * @param value
	 *            The object to convert
	 * @throws ConvertException
	 *             If the object could not be converted or is null
	 * @return An short representation of the value object
	 */
	public static short getShort(Object value, short defaultValue) throws ConvertException {
		Short s = ConvertUtils.convert(Short.class, value);
		if(s == null)
			return defaultValue;
		return s.shortValue();
	}

    /** Converts the given object to an Integer and returns its primitive value
     * @param value The object to convert
     * @throws ConvertException If the object could not be converted or is null
     * @return An int representation of the value object
     */
    public static int getInt(Object value) throws ConvertException {
        Integer i = ConvertUtils.convert(Integer.class, value);
        if(i == null) throw new ConvertException("Value is required", int.class, null);
        return i.intValue();
    }

    /** Converts the given object to an Integer and returns its primitive value or the
     * default value if the object is null
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @throws ConvertException If the object could not be converted
     * @return An int representation of the value object
     */
    public static int getInt(Object value, int defaultValue) throws ConvertException {
        Integer i = ConvertUtils.convert(Integer.class, value);
        if(i == null) return defaultValue;
        return i.intValue();
    }

    /** Converts the given object to an Integer and returns its primitive value.
     *  If the object is null or an error occurs, the defaultValue is returned
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @return A int representation of the value object
     */
    public static int getIntSafely(Object value, int defaultValue) {
        try {
			return getInt(value, defaultValue);
		} catch(ConvertException e) {
			return defaultValue;
		}
    }

    /** Converts the given object to a Long and returns its primitive value.
     *  If the object is null or an error occurs, the defaultValue is returned
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @return A long representation of the value object
     */
    public static long getLongSafely(Object value, long defaultValue) {
        try {
			return getLong(value, defaultValue);
		} catch(ConvertException e) {
			return defaultValue;
		}
    }
    /** Converts the given object to an Long and returns its primitive value
     * @param value The object to convert
     * @throws ConvertException If the object could not be converted or is null
     * @return An long representation of the value object
     */
    public static long getLong(Object value) throws ConvertException {
        Long l = ConvertUtils.convert(Long.class, value);
        if(l == null) throw new ConvertException("Value is required", long.class, null);
        return l.longValue();
    }

    /** Converts the given object to an Long and returns its primitive value or the
     * default value if the object is null
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @throws ConvertException If the object could not be converted
     * @return An long representation of the value object
     */
    public static long getLong(Object value, long defaultValue) throws ConvertException {
        Long l = ConvertUtils.convert(Long.class, value);
        if(l == null) return defaultValue;
        return l.longValue();
    }

    /** Converts the given object to an Float and returns its primitive value
     * @param value The object to convert
     * @throws ConvertException If the object could not be converted or is null
     * @return An float representation of the value object
     */
    public static float getFloat(Object value) throws ConvertException {
        Float f = ConvertUtils.convert(Float.class, value);
        if(f == null) throw new ConvertException("Value is required", float.class, null);
        return f.floatValue();
    }
    
    /** Converts the given object to an Float and returns its primitive value or the
     * default value if the object is null
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @throws ConvertException If the object could not be converted
     * @return An float representation of the value object
     */
    public static float getFloat(Object value, float defaultValue) throws ConvertException {
        Float d = ConvertUtils.convert(Float.class, value);
        if(d == null) return defaultValue;
        return d.floatValue();
    }    
    
    /** Converts the given object to an Double and returns its primitive value
     * @param value The object to convert
     * @throws ConvertException If the object could not be converted or is null
     * @return An double representation of the value object
     */
    public static double getDouble(Object value) throws ConvertException {
        Double d = ConvertUtils.convert(Double.class, value);
        if(d == null) throw new ConvertException("Value is required", double.class, null);
        return d.doubleValue();
    }

    /** Converts the given object to an Double and returns its primitive value or the
     * default value if the object is null
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @throws ConvertException If the object could not be converted
     * @return An double representation of the value object
     */
    public static double getDouble(Object value, double defaultValue) throws ConvertException {
        Double d = ConvertUtils.convert(Double.class, value);
        if(d == null) return defaultValue;
        return d.doubleValue();
    }

    /** Converts the given object to an Boolean and returns its primitive value
     * @param value The object to convert
     * @throws ConvertException If the object could not be converted or is null
     * @return An boolean representation of the value object
     */
    public static boolean getBoolean(Object value) throws ConvertException {
        Boolean b = ConvertUtils.convert(Boolean.class, value);
        if(b == null) throw new ConvertException("Value is required", boolean.class, null);
        return b.booleanValue();
    }

    /** Converts the given object to an Boolean and returns its primitive value or the
     * default value if the object is null
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @throws ConvertException If the object could not be converted
     * @return An boolean representation of the value object
     */
    public static boolean getBoolean(Object value, boolean defaultValue) throws ConvertException {
        Boolean b = ConvertUtils.convert(Boolean.class, value);
        if(b == null) return defaultValue;
        return b.booleanValue();
    }

    /** Converts the given object to a boolean and returns it. If the object is null or an error occurs, the defaultValue is returned
     * @param value The object to convert
     * @return A boolean representation of the value object
     */
    public static boolean getBooleanSafely(Object value, boolean defaultValue) {
        try {
			return getBoolean(value, defaultValue);
		} catch(ConvertException e) {
			return defaultValue;
		}
    }

    /** Converts the given object to an String and returns it
     * @param required Whether to throw an exception if the object is null or not
     * @param value The object to convert
     * @throws ConvertException If the object could not be converted or is null and the required parameter is true
     * @return A String representation of the value object
     */
    public static String getString(Object value, boolean required) throws ConvertException {
        String s = ConvertUtils.convert(String.class, value);
        if(s == null && required) throw new ConvertException("Value is required", String.class, null);
        return s;
    }

    /** Converts the given object to a String and returns it. If an error occurs, the toString() method
     * on the object is used to return a String.
     * @param value The object to convert
     * @return A String representation of the value object
     */
    public static String getStringSafely(Object value) {
        try {
			return ConvertUtils.convert(String.class, value);
		} catch(ConvertException e) {
			return (value == null ? null : value.toString());
		}
    }

    /** Converts the given object to an String and returns it or the
     * default value if the object is null
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @throws ConvertException If the object could not be converted
     * @return An String representation of the value object
     */
    public static String getString(Object value, String defaultValue) throws ConvertException {
        String s = ConvertUtils.convert(String.class, value);
        if(s == null) return defaultValue;
        return s;
    }

    /** Converts the given object to a String and returns it. If an error occurs, the toString() method
     * on the object is used to return a String. The default value is returned if the object or its String representation is null
     * @param value The object to convert
     * @param defaultValue The default value is the object is null
     * @throws ConvertException If the object could not be converted
     * @return An String representation of the value object
     */
    public static String getStringSafely(Object value, String defaultValue) {
    	if(value == null) return defaultValue;
        String s = getStringSafely(value);
        if(s == null) return defaultValue;
        return s;
    }
    /** Convenience method to convert a collection of numbers to an int array
     * @param collection
     * @return
     */
    public static int[] toIntArray(Collection<? extends Number> collection) {
        int[] array = new int[collection.size()];
        int i = 0;
        for(Number n : collection)
            array[i++] = n.intValue();
        return array;
    }
    /** Convenience method to convert a collection of numbers to an long array
     * @param collection
     * @return
     */
    public static long[] toLongArray(Collection<? extends Number> collection) {
        long[] array = new long[collection.size()];
        int i = 0;
        for(Number n : collection)
            array[i++] = n.longValue();
        return array;
    }
    /** Convenience method to convert a collection of numbers to an double array
     * @param collection
     * @return
     */
    public static double[] toDoubleArray(Collection<? extends Number> collection) {
        double[] array = new double[collection.size()];
        int i = 0;
        for(Number n : collection)
            array[i++] = n.doubleValue();
        return array;
    }

	@SafeVarargs
	public static <T> T[] toUniqueArray(Object original, Class<T> componentType, T... defaults) throws ConvertException {
		Collection<T> values = ConvertUtils.asCollection(original, componentType);
		if(values == null)
			return defaults;
		Set<T> set;
		if(values instanceof Set<?>)
			set = (Set<T>) values;
		else
			set = new LinkedHashSet<T>(values);
		if(defaults != null)
			for(T d : defaults)
				set.add(d);
		T[] array = CollectionUtils.createArray(componentType, set.size());
		return set.toArray(array);
	}
}
