/**
 *
 */
package simple.bean;

import java.util.Map;

/**
 * @author Brian S. Krug
 *
 */
public class MapDynaBean extends AbstractDynaBean {
	protected final Map<String,Object> delegate;

	public MapDynaBean(Map<String, Object> delegate) {
		super();
		this.delegate = delegate;
	}

	@Override
	protected String getDynaClassName() {
        return getClass().getName();
    }

    @Override
	protected Object getDynaProperty(String name) {
        return delegate.get(name);
    }

    @Override
	protected boolean hasDynaProperty(String name) {
        //return delegate.containsKey(name);
        return true;
    }

    @Override
	protected void setDynaProperty(String name, Object value) {
    	delegate.put(name, value);
    }
}
