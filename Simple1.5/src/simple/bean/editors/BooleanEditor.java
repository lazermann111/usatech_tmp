/**
 *
 */
package simple.bean.editors;

import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class BooleanEditor extends AbstractEditor<Boolean> {
	public BooleanEditor() {
		super(Boolean.class, new Boolean[] {
				null,
				Boolean.TRUE,
				Boolean.FALSE
		});
	}
	@Override
	public String getAsText() {
		return StringUtils.capitalizeFirst(valueAsString());
	}
}
