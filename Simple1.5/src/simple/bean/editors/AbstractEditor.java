/**
 *
 */
package simple.bean.editors;

import java.beans.PropertyEditorSupport;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractEditor<T> extends PropertyEditorSupport {
	protected final Class<T> valueClass;
	protected final T[] values;

	protected AbstractEditor(Class<T> valueClass, T[] values) {
		this.valueClass = valueClass;
		this.values = values;
	}
	protected String valueAsString(T value) {
		return value == null ? "" : value.toString();
	}
	protected String valueAsString() {
		return valueAsString(valueClass.cast(getValue()));
	}
	@Override
	public String getJavaInitializationString() {
		return valueAsString();
	}

	@Override
	public String getAsText() {
		return valueAsString();
	}

	@Override
	public void setAsText(String s) throws IllegalArgumentException {
		try {
			setValue(ConvertUtils.convert(valueClass, s));
		} catch(ConvertException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public String[] getTags() {
		if(values == null)
			return null;
		String[] tags = new String[values.length];
		for(int i = 0; i < values.length; i++)
			tags[i] = valueAsString(values[i]);
		return tags;
	}
}
