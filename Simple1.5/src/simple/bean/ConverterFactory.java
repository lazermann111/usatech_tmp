/*
 * Converter.java
 *
 * Created on January 30, 2003, 9:34 AM
 */

package simple.bean;

/**
 * Creates Converters that produce conversions to objects that are sub-classes of the 
 * BaseClass of this factory.
 *
 * @author  Brian S. Krug
 */
public interface ConverterFactory<B> {
    /** Creates a converter of the specified specificType
     * @param specificType The exact type requested
     * @throws ConvertException If the converter could not be created
     * @return A converter that converts objects to the specificType specified
     */
    public <S /*extends B*/> Converter<S> createConverter(Class<S> specificType) throws ConvertException;
    /** The base class of all converters created by this factory
     * @return The base Class for this factory
     */
    public Class<B> getBaseClass() ;
    
}

