package simple.bean;

public abstract class CachedGetter<T> implements Getter<T> {
	protected T value;
	public T get() {
		if(value == null)
			value = initial();
		return value;
	}
	protected abstract T initial() ;
}
