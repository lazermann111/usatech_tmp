/*
 * ServletUser.java
 *
 * Created on January 27, 2003, 8:53 AM
 */

package simple.servlet;

import java.io.Serializable;

/** A user of the web application. An object implementing this interface can be
 * stored in the HttpSession to indicate a logged-in application user.
 * @author Brian S. Krug
 */
public interface ServletUser extends Serializable {
    /** The name of the user
     * @return The userName property's value
     */    
    public String getUserName() ;
    /** Whether this user has the specified privilege or not
     * @param privilege The privilege for which to check
     * @return <CODE>true</CODE> if the user has the specified privilege; <CODE>false</CODE> otherwise
     */    
    public boolean hasPrivilege(String privilege);
}

