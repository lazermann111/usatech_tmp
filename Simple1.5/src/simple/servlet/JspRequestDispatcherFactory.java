package simple.servlet;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.FilterRegistration.Dynamic;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.SessionCookieConfig;
import javax.servlet.SessionTrackingMode;
import javax.servlet.descriptor.JspConfigDescriptor;

import org.apache.jasper.Constants;
import org.apache.jasper.servlet.JspServlet;
import org.apache.tomcat.InstanceManager;
import org.apache.tomcat.SimpleInstanceManager;

import simple.io.ConfigSource;
import simple.io.Log;
import simple.lang.Initializer;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public class JspRequestDispatcherFactory implements RequestDispatcherFactory, ServletContext {
	private static final Log log = Log.getLog();
	protected final JspServlet jspServlet = new JspServlet();
	protected final Map<String, String> initParameters = new LinkedHashMap<String, String>();
	protected final Initializer<ServletException> initializer = new Initializer<ServletException>() {
		@Override
		protected void doInitialize() throws ServletException {
			ServletConfig config = new ServletConfig() {
				@Override
				public String getServletName() {
					return "JspInterceptor";
				}

				@Override
				public ServletContext getServletContext() {
					return JspRequestDispatcherFactory.this;
				}

				@Override
				public String getInitParameter(String name) {
					return getServletContext().getInitParameter(name);
				}

				@Override
				public Enumeration<String> getInitParameterNames() {
					return getServletContext().getInitParameterNames();
				}
			};
			String cp = RequestUtils.getClassPath(JspRequestDispatcherFactory.this);
			if(!StringUtils.isBlank(cp))
				setAttribute(Constants.SERVLET_CLASSPATH, cp);
			if(StringUtils.isBlank(getInitParameter("scratchdir")))
				setInitParameter("scratchdir", RequestUtils.determineScratchDir());
			jspServlet.init(config);
			jspServlet.getServletContext().setAttribute(InstanceManager.class.getName(), new SimpleInstanceManager());
		}

		@Override
		protected void doReset() {
			jspServlet.destroy();
		}
	};

	protected final Map<String, Object> attributes = new LinkedHashMap<String, Object>();

	@Override
	public void setSessionTrackingModes(Set<SessionTrackingMode> sessionTrackingModes) {
	}

	@Override
	public boolean setInitParameter(String name, String value) {
		if(initParameters.containsKey(name))
			return false;
		initParameters.put(name, value);
		return true;
	}

	@Override
	public void setAttribute(String name, Object object) {
		if(object == null)
			attributes.remove(name);
		else
			attributes.put(name, object);
	}

	@Override
	public void removeAttribute(String name) {
		attributes.remove(name);
	}

	@Override
	public void log(String message, Throwable throwable) {
		log.info(message, throwable);
	}

	@Override
	public void log(Exception exception, String msg) {
		log.info(msg, exception);
	}

	@Override
	public void log(String msg) {
		log.info(msg);
	}

	@Override
	public SessionCookieConfig getSessionCookieConfig() {
		return null;
	}

	@Override
	public Enumeration<Servlet> getServlets() {
		return CollectionUtils.emptyEnumeration();
	}

	@Override
	public Map<String, ? extends ServletRegistration> getServletRegistrations() {
		return Collections.emptyMap();
	}

	@Override
	public ServletRegistration getServletRegistration(String servletName) {
		return null;
	}

	@Override
	public Enumeration<String> getServletNames() {
		return CollectionUtils.emptyEnumeration();
	}

	@Override
	public String getServletContextName() {
		return null;
	}

	@Override
	public Servlet getServlet(String name) throws ServletException {
		return null;
	}

	@Override
	public String getServerInfo() {
		return null;
	}

	@Override
	public Set<String> getResourcePaths(String path) {
		try {
		URL url = getResource(path);
		if("jar".equals(url.getProtocol())) {
			int pos = url.getPath().indexOf('!');
			if(pos == -1)
				throw new MalformedURLException("no ! found in url spec:" + url.getPath());
			if(url.getPath().length() <= pos + 1)
				throw new IOException("no entry name specified");
			URL jarURL = new URL(url.getPath().substring(0, pos));
			String host;
			if(jarURL.getProtocol().equalsIgnoreCase("file") && ((host = jarURL.getHost()) == null || host.equals("") || host.equals("~") || host.equalsIgnoreCase("localhost"))) {
				JarFile jarFile = new JarFile(URLDecoder.decode(jarURL.getPath(), "UTF-8"));
				String entryName = ConfigSource.decodeJarPath(url.getPath().substring(pos + 1));
				Set<String> paths = new LinkedHashSet<String>();
				for(Enumeration<JarEntry> en = jarFile.entries(); en.hasMoreElements();) {
					JarEntry jarEntry = en.nextElement();
						if(jarEntry.getName().startsWith(entryName))
							paths.add(jarEntry.getName());
				}
					return paths;
			}
		} else if("file".equals(url.getProtocol()) || StringUtils.isBlank(url.getProtocol())) {
			final File file = new File(URLDecoder.decode(url.getPath(), "UTF-8"));
			if(path.endsWith("/"))
				return new LinkedHashSet<String>(Arrays.asList(file.list()));
			else
				return new LinkedHashSet<String>(Arrays.asList(file.getParentFile().list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.startsWith(file.getName());
					}
				})));
		}
		} catch(IOException e) {
			// ignore
		}
		return null;
	}

	@Override
	public InputStream getResourceAsStream(String path) {
		return getClassLoader().getResourceAsStream(sanitizePath(path));
	}

	@Override
	public URL getResource(String path) throws MalformedURLException {
		return getClassLoader().getResource(sanitizePath(path));
	}

	protected String sanitizePath(String path) {
		int pos = 0;
		while(path.length() > pos && path.charAt(pos) == '/')
			pos++;
		if(pos > 0)
			path = path.substring(pos);
		return path;
	}
	@Override
	public RequestDispatcher getRequestDispatcher(final String path) {
		return new RequestDispatcher() {
			@Override
			public void forward(ServletRequest request, ServletResponse response) throws ServletException, IOException {
				try {
					initializer.initialize();
				} catch(InterruptedException e) {
					throw new ServletException(e);
				}
				request.setAttribute(RequestDispatcher.INCLUDE_SERVLET_PATH, path != null && !path.endsWith(".jsp") ? path + ".jsp" : path);
				jspServlet.service(request, response);
			}

			@Override
			public void include(ServletRequest request, ServletResponse response) throws ServletException, IOException {
				try {
					initializer.initialize();
				} catch(InterruptedException e) {
					throw new ServletException(e);
				}
				request.setAttribute(RequestDispatcher.INCLUDE_SERVLET_PATH, path != null && !path.endsWith(".jsp") ? path + ".jsp" : path);
				jspServlet.service(request, response);
			}
		};
	}

	@Override
	public String getRealPath(String path) {
		try {
			URL url = getResource(path);
			return url == null ? path : url.toString();
		} catch(MalformedURLException e) {
			return path;
		}
	}

	@Override
	public RequestDispatcher getNamedDispatcher(String name) {
		return null;
	}

	@Override
	public int getMinorVersion() {
		return 0;
	}

	@Override
	public String getMimeType(String file) {
		return null;
	}

	@Override
	public int getMajorVersion() {
		return 0;
	}

	@Override
	public JspConfigDescriptor getJspConfigDescriptor() {
		return null;
	}

	@Override
	public Enumeration<String> getInitParameterNames() {
		return Collections.enumeration(initParameters.keySet());
	}

	@Override
	public String getInitParameter(String name) {
		return initParameters.get(name);
	}

	@Override
	public Map<String, ? extends FilterRegistration> getFilterRegistrations() {
		return Collections.emptyMap();
	}

	@Override
	public FilterRegistration getFilterRegistration(String filterName) {
		return null;
	}

	@Override
	public Set<SessionTrackingMode> getEffectiveSessionTrackingModes() {
		return null;
	}

	@Override
	public int getEffectiveMinorVersion() {
		return 0;
	}

	@Override
	public int getEffectiveMajorVersion() {
		return 0;
	}

	@Override
	public Set<SessionTrackingMode> getDefaultSessionTrackingModes() {
		return null;
	}

	@Override
	public String getContextPath() {
		return "";
	}

	@Override
	public ServletContext getContext(String uripath) {
		return null;
	}

	@Override
	public ClassLoader getClassLoader() {
		return Thread.currentThread().getContextClassLoader();
	}

	@Override
	public Enumeration<String> getAttributeNames() {
		return Collections.enumeration(attributes.keySet());
	}

	@Override
	public Object getAttribute(String name) {
		return attributes.get(name);
	}

	@Override
	public void declareRoles(String... roleNames) {
	}

	@Override
	public <T extends Servlet> T createServlet(Class<T> clazz) throws ServletException {
		try {
			return clazz.newInstance();
		} catch(InstantiationException e) {
			throw new ServletException(e);
		} catch(IllegalAccessException e) {
			throw new ServletException(e);
		}
	}

	@Override
	public <T extends EventListener> T createListener(Class<T> clazz) throws ServletException {
		try {
			return clazz.newInstance();
		} catch(InstantiationException e) {
			throw new ServletException(e);
		} catch(IllegalAccessException e) {
			throw new ServletException(e);
		}
	}

	@Override
	public <T extends Filter> T createFilter(Class<T> clazz) throws ServletException {
		try {
			return clazz.newInstance();
		} catch(InstantiationException e) {
			throw new ServletException(e);
		} catch(IllegalAccessException e) {
			throw new ServletException(e);
		}
	}

	@Override
	public javax.servlet.ServletRegistration.Dynamic addServlet(String servletName, Class<? extends Servlet> servletClass) {
		return null;
	}

	@Override
	public javax.servlet.ServletRegistration.Dynamic addServlet(String servletName, Servlet servlet) {
		return null;
	}

	@Override
	public javax.servlet.ServletRegistration.Dynamic addServlet(String servletName, String className) {
		return null;
	}

	@Override
	public void addListener(Class<? extends EventListener> listenerClass) {
	}

	@Override
	public <T extends EventListener> void addListener(T t) {
	}

	@Override
	public void addListener(String className) {
	}

	@Override
	public Dynamic addFilter(String filterName, Class<? extends Filter> filterClass) {
		return null;
	}

	@Override
	public Dynamic addFilter(String filterName, Filter filter) {
		return null;
	}

	@Override
	public Dynamic addFilter(String filterName, String className) {
		return null;
	}

	@Override
	public String getVirtualServerName() {
		// TODO Auto-generated method stub
		return null;
	}
}
