/*
 * Action.java
 *
 * Created on January 14, 2003, 3:09 PM
 */

package simple.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** The Action that is called to respond to a servlet request
 *
 * @author  Brian S. Krug
 */
public interface Action {
    /** Processes a request
     * @param dispatcher The dispatcher for this request. Use this to call another action or to forward
     * processing to a file resource like a JSP or HTML file.
     * @param form The InputForm for this request. It holds all the parameters and attributes for
     * this request. Acts as a DynaBean with the attributes of the request (and session
     * and application) as its properties
     * @param request The ServletRequest that triggered this Step
     * @param response The ServletResponse
     * @throws ServletException If an exception occurs
     */    
    public void invoke(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException ;
    /** Whether the Action is allowed to be called or not
     * @return if this Action can be called
     */    
    public boolean isAccessible() ;
    /** Whether a user must login to access this Action.
     * @return if a user is required to access this Action
     */    
    public boolean isUserRequired() ;

	/**
	 * Checks the privileges to ensure that user is allowed to perform this action
	 */    
	public boolean isAuthorized(ServletUser user);

	/**
	 * A description of requirements to be allowed to perform this action
	 * 
	 * @return
	 */
	public String getAuthorizationRequirements();
    
	public boolean isXsrfProtected();

    public boolean isPrivate();
}
