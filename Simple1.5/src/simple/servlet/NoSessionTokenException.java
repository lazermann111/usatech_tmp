/*
 * NotLoggedOnException.java
 *
 * Created on January 27, 2003, 9:31 AM
 */

package simple.servlet;

import javax.servlet.ServletException;

/** Signifies that a request did not provide a session token
 * @author Brian S. Krug
 */
public class NoSessionTokenException extends ServletException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 987213454146767L;


	/**
     * Creates new <code>NotLoggedOnException</code> without detail message.
     */
    public NoSessionTokenException() {
    }


    /**
     * Constructs an <code>NotLoggedOnException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public NoSessionTokenException(String msg) {
        super(msg);
    }
}


