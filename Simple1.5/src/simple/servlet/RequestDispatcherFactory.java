package simple.servlet;

import javax.servlet.RequestDispatcher;

public interface RequestDispatcherFactory {
	public RequestDispatcher getRequestDispatcher(String path);
}