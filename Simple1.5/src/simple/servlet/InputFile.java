/*
 * InputFile.java
 *
 * Created on March 7, 2003, 12:35 PM
 */

package simple.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

import org.apache.commons.fileupload.FileItem;

import simple.io.SliceInputStream;

/** Holds information about a file uploaded in a request
 * @author Brian S. Krug
 */
public class InputFile {
    /** The FileItem objec that this InputFile represents */
    protected FileItem item;
    /** Implementation of java.sql.Blob that delegates its operation to this InputFile */
    protected class InputBlob implements Blob {
        private long cnt = 0;
        private InputStream is = null;
        public InputStream getBinaryStream() throws SQLException {
            try {
                return getInputStream();
            } catch(IOException ioe) {
                throw new SQLException("IOException occured: " + ioe.getMessage());
            }
        }
        public byte[] getBytes(long pos, int length) throws SQLException {
            byte[] b = new byte[length];
            try{
                if(pos <= cnt) {
                    is = getInputStream();
                    cnt = 0;
                }
                int read = is.read(b, (int)(pos - cnt - 1),length);
                if(read < 0) return null;
                cnt += read;
                if(read < length) {
                    byte[] b1 = new byte[read];
                    System.arraycopy(b,0,b1,0,read);
                    b = b1;
                }
                return b;
            } catch(IOException ioe) {
                throw new SQLException("IOException occured: " + ioe.getMessage());
            }
        }
        public InputStream getBinaryStream(long pos, long length) throws SQLException {
        	try {
				return new SliceInputStream(item.getInputStream(), pos, length);
			} catch(IOException e) {
				throw new SQLException("IOException occured: " + e.getMessage());
			}
        }
        public void free() throws SQLException {
        	item.delete();
        }
        public long length() throws SQLException {
            return getLength();
        }
        public long position(byte[] pattern, long start) throws SQLException {
            return -1;
        }
        public long position(Blob pattern, long start) throws SQLException {
            return -1;
        }
        public java.io.OutputStream setBinaryStream(long pos) throws SQLException {
            throw new UnsupportedOperationException("This Java 1.4 method is not supported");
        }
        public int setBytes(long pos, byte[] bytes) throws SQLException {
            throw new UnsupportedOperationException("This Java 1.4 method is not supported");
        }
        public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
            throw new UnsupportedOperationException("This Java 1.4 method is not supported");
        }
        public void truncate(long len) throws SQLException {
            throw new UnsupportedOperationException("This Java 1.4 method is not supported");
        }
    }

    /** Holds value of property content. */
    private final Blob content = new InputBlob();

    /** Creates a new instance of InputFile
     * @param item The FileItem that this object will represent
     */
    public InputFile(FileItem item) {
        this.item = item;
    }

    /** Getter for property path.
     * @return Value of property path.
     */
    public String getPath() {
        return item.getName();
    }

    /** Getter for property content.
     * @return Value of property content.
     */
    public Blob getContent() {
        return this.content;
    }

    public String getFieldName() {
    	return item.getFieldName();
    }
    /** Getter for property inputStream.
     * @return Value of property inputStream.
     * @throws IOException If the file can not be read
     */
    public InputStream getInputStream() throws IOException {
        return item.getInputStream();
    }

    /** Getter for property length.
     * @return Value of property length.
     */
    public long getLength() {
        return item.getSize();
    }

    /** Releases objects to which this InputFile had been holding */
    @Override
	protected void finalize() {
        item.delete();
    }

	@Override
	public String toString() {
		return getPath();
	}
	
	public String getContentType(){
		if (item!=null){
			return item.getContentType();
		}else{
			return null;
		}
	}
}
