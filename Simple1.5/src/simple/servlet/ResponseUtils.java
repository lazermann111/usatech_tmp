/*
 * ResponseUtils.java
 *
 * Created on January 6, 2003, 9:21 AM
 */

package simple.servlet;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.Format;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.CoteCache;
import simple.io.CoteCache.ItemClosedException;
import simple.io.Log;
import simple.io.WriterOutputStream;
import simple.results.Results;
import simple.text.StringUtils;

/** Contains a variety of utility methods for formating for a servlet response or
 * writing data to it
 * @author Brian S. Krug
 */
public class ResponseUtils {
	/** Never create new ResponseUtils */
    private ResponseUtils() {
    }

	public static StringBuilder appendQueryParam(String name, long value, StringBuilder appendTo) {
		if(appendTo.length() > 0)
			appendTo.append('&');
		appendTo.append(StringUtils.prepareURLPart(name)).append('=').append(value);
		return appendTo;
	}

	public static StringBuilder appendQueryParam(String name, int value, StringBuilder appendTo) {
		if(appendTo.length() > 0)
			appendTo.append('&');
		appendTo.append(StringUtils.prepareURLPart(name)).append('=').append(value);
		return appendTo;
	}

	public static StringBuilder appendQueryParam(String name, short value, StringBuilder appendTo) {
		if(appendTo.length() > 0)
			appendTo.append('&');
		appendTo.append(StringUtils.prepareURLPart(name)).append('=').append(value);
		return appendTo;
	}

	public static StringBuilder appendQueryParam(String name, byte value, StringBuilder appendTo) {
		if(appendTo.length() > 0)
			appendTo.append('&');
		appendTo.append(StringUtils.prepareURLPart(name)).append('=').append(value);
		return appendTo;
	}

	public static StringBuilder appendQueryParam(String name, boolean value, StringBuilder appendTo) {
		if(appendTo.length() > 0)
			appendTo.append('&');
		appendTo.append(StringUtils.prepareURLPart(name)).append('=').append(value);
		return appendTo;
	}

	public static StringBuilder appendQueryParam(String name, double value, StringBuilder appendTo) {
		if(appendTo.length() > 0)
			appendTo.append('&');
		appendTo.append(StringUtils.prepareURLPart(name)).append('=').append(value);
		return appendTo;
	}

	public static StringBuilder appendQueryParam(String name, float value, StringBuilder appendTo) {
		if(appendTo.length() > 0)
			appendTo.append('&');
		appendTo.append(StringUtils.prepareURLPart(name)).append('=').append(value);
		return appendTo;
	}

	public static StringBuilder appendQueryParam(String name, Object value, StringBuilder appendTo) {
		if(value == null)
			return appendTo;
		String escapedValue;
		if(value instanceof Number || value instanceof Boolean)
			escapedValue = value.toString();
		else if(value instanceof Date)
			escapedValue = ConvertUtils.formatObject(value, "DATE:yyyy-MM-dd HH:mm:ss.SSS zzz", true);
		else if(value instanceof Calendar)
			escapedValue = ConvertUtils.formatObject(((Calendar) value).getTime(), "DATE:yyyy-MM-dd HH:mm:ss.SSS zzz", true);
		else if(value instanceof String)
			escapedValue = StringUtils.prepareURLPart((String) value);
		else
			escapedValue = StringUtils.prepareURLPart(ConvertUtils.getStringSafely(value));
		if(appendTo.length() > 0)
			appendTo.append('&');
		appendTo.append(StringUtils.prepareURLPart(name)).append('=').append(escapedValue);
		return appendTo;
	}

    /** Write the specified text to the response's writer
     * @param pageContext The PageContext object for this page
     * @param text The text to be written
     * @throws JspException if an input/output error occurs (already saved)
     */
    public static void write(PageContext pageContext, String text) throws JspException {
        JspWriter writer = pageContext.getOut();
        try {
            writer.print(text);
        } catch (IOException e) {
            throw new JspException(e.toString());
        }
    }

    /** Write the specified text as the response to the writer associated with
     * the body content for the tag within which we are currently nested.
     * @param pageContext The PageContext object for this page
     * @param text The text to be written
     * @exception JspException if an input/output error occurs (already saved)
     */
    public static void writePrevious(PageContext pageContext, String text) throws JspException {
        JspWriter writer = pageContext.getOut();
        if (writer instanceof BodyContent)
            writer = ((BodyContent) writer).getEnclosingWriter();
        try {
            writer.print(text);
        } catch (IOException e) {
            throw new JspException(e.toString());
        }
    }

    public static void writeSelectElement(PageContext pageContext, String name, Results optionResults, Format valueFormat, Format labelFormat, Map<String,String> selectAttributes) throws JspException {
    	writeSelectElement(pageContext, name, optionResults, valueFormat, labelFormat, selectAttributes, null);
    }
    public static void writeSelectElement(PageContext pageContext, String name, Results optionResults, Format valueFormat, Format labelFormat, Map<String,String> selectAttributes, Map<String,Format> optionAttributes) throws JspException {
		writeSelectElement(pageContext, name, null, optionResults, valueFormat, labelFormat, selectAttributes, optionAttributes);
	}

	public static void writeSelectElement(PageContext pageContext, String name, Object initialValue, Results optionResults, Format valueFormat, Format labelFormat,
			Map<String, String> selectAttributes, Map<String, Format> optionAttributes) throws JspException {
        JspWriter writer = pageContext.getOut();
        try {
        	writer.print("<select name=\"");
        	writer.print(StringUtils.prepareCDATA(name));
        	writer.print('"');
    		if(selectAttributes != null && !selectAttributes.isEmpty()) {
    			for(Map.Entry<String, String> entry : selectAttributes.entrySet()) {
    				writer.print(" ");
        			writer.print(StringUtils.prepareCDATA(entry.getKey()));
    				writer.print("=\"");
    				writer.print(StringUtils.prepareCDATA(entry.getValue()));
    				writer.print("\"");
    			}
    		}
    		writer.println('>');
			String selectedValue = ConvertUtils.getStringSafely(initialValue);
			if(StringUtils.isBlank(selectedValue))
				selectedValue = ConvertUtils.getStringSafely(RequestUtils.getAttributeSafely(pageContext, name), "");
    		while(optionResults.next()) {
    			String label = labelFormat.format(optionResults);
    			String value = valueFormat.format(optionResults);
    			writer.print("\t<option value=\"");
    			writer.print(StringUtils.prepareCDATA(value));
    			writer.print('"');
    			if(value != null && value.equals(selectedValue)) {
    				writer.print(" selected=\"selected\"");
    			}
    			if(optionAttributes != null && !optionAttributes.isEmpty()) {
        			for(Map.Entry<String, Format> entry : optionAttributes.entrySet()) {
        				writer.print(" ");
            			writer.print(StringUtils.prepareCDATA(entry.getKey()));
        				writer.print("=\"");
        				if(entry.getValue() != null)
        					writer.print(StringUtils.prepareCDATA(entry.getValue().format(optionResults)));
        				writer.print("\"");
        			}
        		}
        		
    			writer.print('>');
    			writer.print(StringUtils.prepareCDATA(label));
    			writer.println("</option>");
    		}
    		writer.println("</select>");
        } catch (IOException e) {
            throw new JspException(e.toString());
        }
    }

	public static void writeCoteResource(PageContext pageContext, String fileName) throws ServletException, ConvertException, IOException {
		CoteCache.Retriever coteCacheRetriever = RequestUtils.getAttribute(pageContext.getRequest(), CoteCache.class.getName() + ".Retriever", CoteCache.Retriever.class, true);
		for(int attempts = 0; attempts < 2; attempts++) {
			CoteCache.Item item = coteCacheRetriever.getItem(fileName);
			if(item != null && item.getLength() != 0) {
				JspWriter writer = pageContext.getOut();
				String charset = pageContext.getResponse().getCharacterEncoding();
				if(charset == null)
					charset = "ISO-8859-1";
				try {
					item.writeTo(new WriterOutputStream(writer, Charset.forName(charset), 1024));
				} catch(ItemClosedException e) {
					// try again
					Log.getLog().warn("Item is closed for " + fileName + (attempts == 0 ? "; will try again" : ""), e);
					continue;
				}
			}
			return;
		}
		throw new IOException("Could not read item");
	}

	public static void writeXsfrFormProtection(PageContext pageContext) throws IOException {
		if(!(pageContext.getRequest() instanceof HttpServletRequest))
			return;
		String sessionToken = RequestUtils.getSessionToken((HttpServletRequest) pageContext.getRequest());
		if(StringUtils.isBlank(sessionToken))
			return;
		JspWriter writer = pageContext.getOut();
		writer.append("<input type=\"hidden\" name=\"session-token\" value=\"").append(StringUtils.prepareCDATA(sessionToken)).append("\"/>");
	}

	public static void writeXsfrJSONProtection(PageContext pageContext, boolean first, boolean last) throws IOException {
		if(!(pageContext.getRequest() instanceof HttpServletRequest))
			return;
		String sessionToken = RequestUtils.getSessionToken((HttpServletRequest) pageContext.getRequest());
		if(StringUtils.isBlank(sessionToken))
			return;
		JspWriter writer = pageContext.getOut();
		if(!first)
			writer.append(", ");
		writer.append("\"session-token\": \"").append(StringUtils.prepareScript(sessionToken)).append("\"");
		if(!last)
			writer.append(", ");
	}
}
