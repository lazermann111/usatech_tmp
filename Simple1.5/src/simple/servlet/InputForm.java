/*
 * InputForm.java
 *
 * Created on January 14, 2003, 3:11 PM
 */

package simple.servlet;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;

import simple.bean.DynaBean;

/** Wraps a ServletRequest to provide DynaBean methods and other convenience methods
 * @author Brian S. Krug
 */
public interface InputForm extends DynaBean, RequestInfo {
    /**
     * Retrieves the specified object from the request's, session's or application's attributes
     *
     * @param key The attribute name
     * @param scope The scope of the attribute - "request", "session", "application", or null 
     * (which looks in all scopes; this is the default)
     * @return The value of the attribute
     */
    public Object getAttribute(String key, String scope) ;

    /**
     * Sets the specified attribute in the request, session or application to the specified object
     *
     * @param key The attribute name
     * @param value The new value of the attribute
     * @param scope The scope of the attribute - "request", "session", or "application"
     * (if not provided "request" is used)
     */
    public void setAttribute(String key, Object value, String scope) ;

    /**
     * Retrieves the specified object from the request's, session's or application's attributes
     *
     * @param key The attribute name
     * @return The value of the attribute
     */
    public Object getAttribute(String key) ;

    /**
     * Sets the specified attribute in the request to the specified object
     *
     * @param key The attribute name
     * @param value The new value of the attribute
     */
    public void setAttribute(String key, Object value) ;
    
    /** Retrieves a BigDecimal from the request's attributes
     *
     * @return The value as a BigDecimal
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public BigDecimal getBigDecimal(String name, boolean required) throws ServletException ;
    /** Retrieves a Boolean from the request's attributes
     *
     * @return The value as a Boolean or null if not existent
     * @param name The attribute or parameter name
     * @throws ServletException If an exception occurs
     */
    public Boolean getBooleanObject(String name) throws ServletException;
 
    /** Retrieves a boolean from the request's attributes
     *
     * @return The value as a boolean
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @param defaultValue The value to return if the value is not found and
     * the required parameter is false
     * @throws ServletException If an exception occurs
     */
    public boolean getBoolean(String name, boolean required, boolean defaultValue) throws ServletException ;
 
    /** Retrieves a boolean array from the request's attributes
     *
     * @return The value as a boolean array
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public boolean[] getBooleanArray(String name, boolean required) throws ServletException;
    
    /** Retrieves a Date from the request's attributes
     *
     * @return The value as a Date
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @param df The DateFormat to use to parse the value into a Date
     * @throws ServletException If an exception occurs
     */
    public java.util.Date getDate(String name, boolean required, DateFormat df) throws ServletException ;

    /** Retrieves an int from the request's attributes
     * @return The value as an int
     * @param defaultValue The default value to use if none is found
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public int getInt(String name, boolean required, int defaultValue) throws ServletException ;
    
    /** Retrieves an int array from the request's attributes
     *
     * @return The value as an int array
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public int[] getIntArray(String name, boolean required) throws ServletException;
    
    /** Retrieves a long from the request's attributes
     * @return The value as a long
     * @param defaultValue The default value to use if none is found
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public long getLong(String name, boolean required, long defaultValue) throws ServletException;
    
    /** Retrieves a long array from the request's attributes
     *
     * @return The value as a long array
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public long[] getLongArray(String name, boolean required) throws ServletException ;
    
    /** Retrieves a String of digits from the request's attributes
     *
     * @return The value as a String with all non-digits stripped out
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public String getNumberString(String name, boolean required) throws ServletException ;
      
    /** Retrieves a String from the request's attributes
     *
     * @return The value as a String
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public String getString(String name, boolean required) throws ServletException ;
    
    /** Retrieves a String from the request's attributes
    *
    * @return The value as a String
    * @param name The attribute or parameter name
    * @param defaultValue The default value
    */
   public String getStringSafely(String name, String defaultValue);
    
    /** Retrieves a String array from the request's attributes
     *
     * @return The value as a String array
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public String[] getStringArray(String name, boolean required) throws ServletException ;
    
    /** Getter for property request.
     * @return Value of property request.
     *
     *
     */
    public ServletRequest getRequest();
 
    /** Getter for property parameters.
     * @return Value of property parameters.
     *
     *
     */
    public Map<String,Object> getParameters();
    
    /** Returns an object cached in the session of the form
     * @param cacheId The id of the cached object
     * @return The object or null if not found
     */
    //public Object getSessionCachedObject(long cacheId) ;
    
    /** Returns an object cached in the session of the form 
     * and then removes it
     * @param cacheId The id of the cached object
     * @return The object or null if not found
     */
    //public Object removeSessionCachedObject(long cacheId) ;

    /** Adds the specified object to the session cache and returns a unique id 
     * that can later be used to retrieve the object.
     * @param o The object to cache
     * @return the cacheId that can later be used to retrieve
     * the object using getSessionCacheObject() or removeSessionCachedObject()
     */
    //public long addSessionCachedObject(Object o) ;
    
    public String getHeader(String name) ;
    public Cookie getCookie(String name) ;
    
    /**
     * This method sets a redirect's URI to redirect a client on step performer invocation.
     * @param redirectUri
     */
    public void setRedirectUri(String redirectUri);
    /**
     * If this is not {@code null} if redirect is required.
     * @return Redirect URI or {@code null} if was not set.
     */
    public String getRedirectUri();

}
