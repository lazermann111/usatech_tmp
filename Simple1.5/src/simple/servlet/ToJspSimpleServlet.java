package simple.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.servlet.JspServlet;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.steps.AbstractLogonStep;
import simple.text.StringUtils;
import simple.util.CollectionUtils;
import simple.util.Refresher;

public class ToJspSimpleServlet extends SimpleServlet {
	private static final long serialVersionUID = -2317289587486655920L;
	public static final String PATH_TRANSLATIONS_REFRESHER_ATTRIBUTE = ToJspSimpleServlet.class.getName() + ".pathTranslationRefresher";
	public static final String REPLACED_PATH_ATTRIBUTE = ToJspSimpleServlet.class.getName() + ".replacedPath";
	protected AbstractLogonStep logonStep;
	protected String loginFailurePage = "/login.html";
	protected String generalFailurePage = "/error.html";
	protected String defaultPage = "/index.html";
	protected String pathTranslationCallId = null;
	protected NavigableMap<String, String> pathTranslationMap;
	protected final JspServlet jspServlet = new JspServlet();

	public class JspServletWrapper implements Servlet, ServletConfig {
		public int hashCode() {
			return jspServlet.hashCode();
		}

		public boolean equals(Object obj) {
			return jspServlet.equals(obj);
		}

		public String getInitParameter(String name) {
			return jspServlet.getInitParameter(name);
		}

		public void init(ServletConfig config) throws ServletException {
			// jspServlet.init(config);
		}

		public Enumeration<String> getInitParameterNames() {
			return jspServlet.getInitParameterNames();
		}

		public ServletConfig getServletConfig() {
			return jspServlet.getServletConfig();
		}

		public ServletContext getServletContext() {
			return jspServlet.getServletContext();
		}

		public int getJspCount() {
			return jspServlet.getJspCount();
		}

		public String getServletInfo() {
			return jspServlet.getServletInfo();
		}

		public void setJspReloadCount(int count) {
			jspServlet.setJspReloadCount(count);
		}

		public int getJspReloadCount() {
			return jspServlet.getJspReloadCount();
		}

		/*public int getJspErrorCount() {
			return jspServlet.getJspErrorCount();
		}*/

		public void init() {
			// jspServlet.init();
		}

		public void log(String msg) {
			jspServlet.log(msg);
		}

		public String toString() {
			return jspServlet.toString();
		}

		public void log(String message, Throwable t) {
			jspServlet.log(message, t);
		}

		public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			jspServlet.service(request, response);
		}

		public String getServletName() {
			return jspServlet.getServletName();
		}

		public void destroy() {
			// jspServlet.destroy();
		}

		public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
			jspServlet.service(req, res);
		}
	}

	protected JspServletWrapper jspServletWrapper;
	protected final NavigableSet<String> userNotRequiredPages = new TreeSet<String>(Arrays.asList(loginFailurePage, "/logout.html", "/error.html"));
	protected final Set<String> xsrfProtectedPages = new HashSet<String>();
	protected final Set<String> xsrfProtectedMethods = new HashSet<String>();

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = config.getServletContext();
		String cp = RequestUtils.getClassPath(context);
		if(!StringUtils.isBlank(cp))
			config = new JspServletConfigWrapper(config, cp);
		jspServlet.init(config);
		if(!StringUtils.isBlank(getPathTranslationCallId()))
			config.getServletContext().setAttribute(PATH_TRANSLATIONS_REFRESHER_ATTRIBUTE, new Refresher() {
				public void refresh() {
					clearPathTranslations();
				}
			});
	}

	@Override
	public void destroy() {
		super.destroy();
		jspServlet.destroy();
	}

	protected boolean isUserRequired(String pathInfo) {
		String match = CollectionUtils.bestPrefixMatch(userNotRequiredPages, pathInfo);
		return match == null;
	}

	protected void forwardToJsp(InputForm form, HttpServletRequest request, HttpServletResponse response, String pathInfo, boolean handleException) throws ServletException {
		try {
			// getJspDispatcher(pathInfo + ".jsp").forward(request, response);
			request.setAttribute(RequestDispatcher.INCLUDE_SERVLET_PATH, pathInfo + ".jsp");
			jspServlet.service(request, response);
		} catch(IOException e) {
			if(handleException)
				handleException(e, getGeneralFailurePage(), form, request, response);
			else
				throw new ServletException(e);
		} catch(ServletException e) {
			if(handleException)
				handleException(e, getGeneralFailurePage(), form, request, response);
			else
				throw e;
		}
	}

	@Override
	public void processRequest(InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		if(form.getAttribute(ATTRIBUTE_REQUEST_URI_QUERY) == null)
			form.setAttribute(ATTRIBUTE_REQUEST_URI_QUERY, RequestUtils.getRequestURIQuery(request));
		String path = request.getPathInfo();
		if(path == null)
			path = request.getServletPath(); // its a suffix match
		path = translatePath(path, form);
		if(StringUtils.isBlank(path) || "/".equals(path)) {
			try {
				response.sendRedirect(getDefaultPage());
				return;
			} catch(IOException e) {
				throw new ServletException(e);
			}
		}
		boolean xsrfProtected = isXsrfProtected(path, request.getMethod());
		if(!checkLogin(form, request, response, isUserRequired(path), xsrfProtected))
			return;
		if(xsrfProtected) {
			String storedSessionToken = RequestUtils.getSessionToken(request);
			String requestedSessionToken = form.getString(REQUEST_SESSION_TOKEN, false);
			if(requestedSessionToken == null) {
				handleException(new NoSessionTokenException("Session Token was not provided"), getLoginFailurePage(), form, request, response);
				return;
			}
			if(!requestedSessionToken.equals(storedSessionToken)) {
				handleException(new InvalidSessionTokenException("Session Token is invalid"), getLoginFailurePage(), form, request, response);
				return;
			}
		}
		forwardToJsp(form, request, response, path, true);
	}

	protected String translatePath(String path, InputForm form) {
		if(StringUtils.isBlank(path))
			return path;
		NavigableMap<String, String> m;
		String callId;
		if((m = pathTranslationMap) == null) {
			if((callId = getPathTranslationCallId()) == null)
				return path;
			m = new TreeMap<String, String>();
			Results results;
			try {
				results = DataLayerMgr.executeQuery(callId, null);
			} catch(SQLException e) {
				log.warn("Could not get path translations using call " + callId, e);
				return path;
			} catch(DataLayerException e) {
				log.warn("Could not get path translations using call " + callId, e);
				return path;
			}
			while(results.next()) {
				m.put(results.getFormattedValue(1), results.getFormattedValue(2));
			}
			pathTranslationMap = m;
		}
		Map.Entry<String, String> entry = CollectionUtils.getBestMatch(m, path);
		if(entry == null)
			return path;
		String replaced = path.substring(0, entry.getKey().length());
		form.setAttribute(REPLACED_PATH_ATTRIBUTE, replaced);
		String newPath = entry.getValue() + path.substring(entry.getKey().length());
		return newPath;
	}

	protected boolean isXsrfProtected(String pathInfo, String method) {
		if(!xsrfProtectedMethods.isEmpty() && method != null && !xsrfProtectedMethods.contains(method.toUpperCase()))
			return false;
		return getXsrfProtection() == XsrfProtectionLevel.ENABLED_FOR_ALL || (getXsrfProtection() == XsrfProtectionLevel.ENABLED_FOR_SOME && xsrfProtectedPages.contains(pathInfo));
	}

	protected boolean checkLogin(InputForm form, HttpServletRequest request, HttpServletResponse response, boolean required, boolean xsrfProtected) throws ServletException {
		ServletUser user = (ServletUser) request.getSession().getAttribute(ATTRIBUTE_USER);
		if(user != null)
			return true;
		if(form.getParameters().get("username") == null || form.getParameters().get("password") == null) {
			if(!required)
				return true;
			handleException(new NotLoggedOnException("User is required"), getLoginFailurePage(), form, request, response);
			return false;
		}
		if(logonStep == null) {
			if(!required)
				return true;
			handleException(new ServletException("Login not configured"), getLoginFailurePage(), form, request, response);
			return false;
		}
		try {
			user = logonStep.loginUser(form, request, response);
		} catch(ServletException e) {
			handleException(e, getLoginFailurePage(), form, request, response);
			return false;
		}
		try {
			RequestUtils.redirectWithCarryOver(request, response, RequestUtils.getRequestURIQuery(request), xsrfProtected, true);
		} catch(IOException e) {
			throw new ServletException(e);
		}
		return false;
	}

	protected void handleException(Throwable exception, String handlerPage, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		if(exception instanceof NotLoggedOnException)
			log.info("User not logged on. Forwarding to " + handlerPage);
		else
			log.error("Could not service page. Forwarding to " + handlerPage, exception);
		request.setAttribute(SimpleAction.ATTRIBUTE_EXCEPTION, exception);
		String message = exception.getMessage() != null ? exception.getMessage() : exception.getClass().getName();
		if(message != null) {
			int index = message.indexOf('\n');
			if(index > -1)
				message = message.substring(0, index).trim();
		}
		request.setAttribute(SimpleAction.ATTRIBUTE_EXCEPTION_MESSAGE, message);
		forwardToJsp(form, request, response, handlerPage, false);
	}
	public AbstractLogonStep getLogonStep() {
		return logonStep;
	}

	public void setLogonStep(AbstractLogonStep logonStep) {
		this.logonStep = logonStep;
	}

	public String getLoginFailurePage() {
		return loginFailurePage;
	}

	public void setLoginFailurePage(String loginFailurePage) {
		this.loginFailurePage = loginFailurePage;
	}

	public String getGeneralFailurePage() {
		return generalFailurePage;
	}

	public void setGeneralFailurePage(String generalFailurePage) {
		this.generalFailurePage = generalFailurePage;
	}

	public String[] getXsrfProtectedPages() {
		return xsrfProtectedPages.toArray(new String[xsrfProtectedPages.size()]);
	}

	public void setXsrfProtectedPages(String[] xsrfProtectedPages) {
		this.xsrfProtectedPages.clear();
		this.xsrfProtectedPages.addAll(Arrays.asList(xsrfProtectedPages));
	}

	public String[] getXsrfProtectedMethods() {
		return xsrfProtectedMethods.toArray(new String[xsrfProtectedMethods.size()]);
	}

	public void setXsrfProtectedMethods(String[] xsrfProtectedMethods) {
		this.xsrfProtectedMethods.clear();
		if(xsrfProtectedMethods != null)
			for(String xpm : xsrfProtectedMethods)
				if(xpm != null)
					this.xsrfProtectedMethods.add(xpm.toUpperCase());
	}

	public String[] getUserNotRequiredPages() {
		return userNotRequiredPages.toArray(new String[userNotRequiredPages.size()]);
	}

	public void setUserNotRequiredPages(String[] userNotRequiredPages) {
		this.userNotRequiredPages.clear();
		this.userNotRequiredPages.addAll(Arrays.asList(userNotRequiredPages));
	}

	public JspServletWrapper getJspServlet() {
		if(jspServletWrapper == null)
			jspServletWrapper = new JspServletWrapper();
		return jspServletWrapper;
	}

	public String getDefaultPage() {
		return defaultPage;
	}

	public void setDefaultPage(String defaultPage) {
		this.defaultPage = defaultPage;
	}

	public String getPathTranslationCallId() {
		return pathTranslationCallId;
	}

	public void setPathTranslationCallId(String pathTranslationCallId) {
		this.pathTranslationCallId = pathTranslationCallId;
	}

	public String getPathTranslation(String key) {
		NavigableMap<String, String> m = pathTranslationMap;
		return m == null ? null : m.get(key);
	}

	public void setPathTranslation(String key, String value) {
		NavigableMap<String, String> m = pathTranslationMap;
		if(m == null) {
			m = new TreeMap<String, String>();
			pathTranslationMap = m;
		}
		m.put(key, value);
	}

	public void clearPathTranslations() {
		pathTranslationMap = null;
	}

	public static String untranslatePath(ServletRequest request, String path) {
		String replaced = (String) request.getAttribute(REPLACED_PATH_ATTRIBUTE);
		if(!StringUtils.isBlank(replaced)) {
			char lastCh = replaced.charAt(replaced.length() - 1);
			switch(lastCh) {
				case '/':
				case '\\':
					if(StringUtils.isBlank(path))
						return replaced;
					switch(path.charAt(0)) {
						case '/':
						case '\\':
							replaced = replaced.substring(0, replaced.length() - 1);
					}
				default:
					if(StringUtils.isBlank(path))
						return replaced + '/';

			}
			return replaced + path;
		}
		return path;
	}
}
