/*
 * SimpleServlet.java
 *
 * Created on October 11, 2001, 2:48 PM
 */

package simple.servlet;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.configuration.ConversionException;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.net.protocol.AddProtocols;
import simple.text.FileSizeFormat;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveComparator;
import simple.util.CollectionUtils;
import simple.util.CompositeSet;
import simple.xml.XMLLoader;

/** This class is the core of the Simple Web Application Structure. To process a
 * ServletRequest, the SimpleServlet attempts to remove from the request path that
 * which mapped it to the SimpleServlet. Thus, if a suffix of ".i" maps to this
 * servlet, then if the request path ends with ".i" it is removed. Then, the
 * servlet uses this path to determine the "Action" to perform. Each "Action" can be
 * configured in an XML file. The sequence of "steps" involved in each action as well
 * as any required user privileges are all specified in an XML file following
 * the {@link /resources/actions.xsd} schema. The XML file's location is specified
 * in the servlet's configuration file using an &lt;init-param&gt; of "action-config-files".
 * Each action can contain any number of built-in or custom <CODE>simple.servlet.Step</CODE>s.
 * The built-in steps are found in the <CODE>simple.servlet.steps</CODE> package and are referenced
 * in the XML file using the lower-case form of their class name minus the "Step" suffix. Thus,
 * the <CODE>simple.servlet.steps.DbStep</CODE> class would be referenced using &lt;db/&gt; in the XML file.
 * Other built-in steps include "jsp", "intercept", and "pdf".
 * See the Simple Application Framework Overview for more discussion
 * on how the SimpleServlet makes a developer's life easier.
 * @author Brian S. Krug
 */
public class SimpleServlet extends HttpServlet {
    private static final long serialVersionUID = 111222333L;

	private static Log sLog = Log.getLog();

    /** Value under which the <CODE>simple.servlet.FutureCanceller</CODE> object is stored in
     * the session
     */
    public static final String ATTRIBUTE_CANCELLER = "simple.servlet.SimpleServlet.RequestTaskCanceller";
    /** Value under which the <CODE>simple.servlet.InputForm</CODE> object is stored in
     * the request
     */
    public static final String ATTRIBUTE_FORM = "simple.servlet.InputForm";
    /** Value under which the request URI and query string are stored in
     * the InputForm object
     */
    public static final String ATTRIBUTE_REQUEST_URI_QUERY = "simple.servlet.InputForm.RequestURIQuery";
    /** Value under which the redirect URI and query string are stored in
     * the InputForm object
     */
    public static final String ATTRIBUTE_REDIRECT_URI_QUERY = "simple.servlet.InputForm.RedirectURIQuery";
    /** Value under which the forward URI and query string are stored in
     * the InputForm object
     */
    public static final String ATTRIBUTE_FORWARD_URI_QUERY = "simple.servlet.InputForm.ForwardURIQuery";
    /** Value under which the <CODE>simple.servlet.ServletUser</CODE> object is stored in
     * the session
     */
    public static final String ATTRIBUTE_USER = "simple.servlet.ServletUser";
    /** Value under which the <CODE>simple.servlet.SelectedMenuItem</CODE> object is stored in
     * the session
     */
    public static final String ATTRIBUTE_SELECTED_MENU_ITEM = "simple.servlet.SelectedMenuItem";
    /** Value under which the initial path is stored in
     * the request
     */
    public static final String ATTRIBUTE_SEARCH_PATH = "simple.servlet.SimpleServlet.SearchPath";
	/**
	 * Value under which the current path is stored in the request
	 */
	public static final String ATTRIBUTE_CURRENT_PATH = "simple.servlet.SimpleServlet.CurrentPath";
    /** Value under which the session token is stored in for XSRF protection
     */
	public static final String ATTRIBUTE_XSRF_PROTECTION = "simple.servlet.SimpleServlet.XsrfProtection";
    public static final String ATTRIBUTE_SESSION_TOKEN = "simple.servlet.SessionToken";
    public static final String REQUEST_SESSION_TOKEN = "session-token";
	public static final String ATTRIBUTE_TIME_ZONE = "simple.servlet.SimpleServlet.TimeZone";

    protected static final char[] SLASHES = new char[] {'\\'};
    protected static final String[] DEFAULT_MASKED_PARAMETERS = new String[] {
    	"app_user_password",
    	"confirm", 
    	"credential",
    	"original_pos_pta_encrypt_key",
    	"original_pos_pta_encrypt_key2",
    	"password",
    	"pos_pta_encrypt_key",
    	"pos_pta_encrypt_key2"
    };
    
    protected static String DEFAULT_MASKED_REGEX="[0-9]{15,19}";

    protected Log log;
    protected final Map<String,RequestDispatcher> jspDispatchers = new ConcurrentHashMap<String,RequestDispatcher>();
    protected final Map<String,Action> actions = new ConcurrentHashMap<String,Action>();
    protected boolean securityOff = false;
	protected XsrfProtectionLevel xsrfProtection = XsrfProtectionLevel.ENABLED_FOR_SOME;
    protected boolean copyParamsToAttributes = false;
    protected String securityType;
    protected FileUpload upload;
    protected String actionConfigFiles;
    protected String maxRequestSize;
    protected String tempDir;
    protected String[] maskedParameters = DEFAULT_MASKED_PARAMETERS;
    protected final Map<String,Object> applicationAttributes = new HashMap<String, Object>();
    
    protected final XMLLoader<Map<String,Object>> actionXMLLoader = new simple.xml.MapListXMLLoader();
    protected static Map<String,Class<? extends Step>> stepClasses = new ConcurrentHashMap<String,Class<? extends Step>>();
    protected boolean caseSensitive = false;
    protected boolean slashSensitive = false;
    protected String[] servletMappings;
    protected final Set<ConfigSource> actionsFiles = new LinkedHashSet<ConfigSource>();
    protected final List<Catch> globalCatches = new CopyOnWriteArrayList<Catch>();
    protected final Loader actionsLoader = new Loader() {
        public void load(ConfigSource src) throws LoadingException {
            loadActions(src);
        }
    };

    protected final Set<String> maskedParameterNames = new TreeSet<String>(new CaseInsensitiveComparator<String>());
    protected final Set<String> restrictedParameterNames = new HashSet<String>();
    protected final Set<String> truncatedParameterNames = new HashSet<String>();

    protected long refreshInterval = -1;
	protected boolean allowClientCaching = false;

    /** Creates new SimpleServlet */
    public SimpleServlet() {
        restrictedParameterNames.add(ATTRIBUTE_SEARCH_PATH);
        restrictedParameterNames.add(ATTRIBUTE_FORM);
        restrictedParameterNames.add(ATTRIBUTE_USER);
        restrictedParameterNames.add(ATTRIBUTE_CANCELLER);
    }

    /** Process incoming HTTP GET requests
     * @param request Object that encapsulates the request to the servlet
     * @param response Object that encapsulates the response from the servlet
     * @throws ServletException If an exception occurs
     * @throws IOException If an IO exception occurs
     */
    @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }
    /** Process incoming HTTP POST requests
     * @param request Object that encapsulates the request to the servlet
     * @param response Object that encapsulates the response from the servlet
     * @throws ServletException If an exception occurs
     * @throws IOException If an IO exception occurs
     */
    @Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    /**
     * Initializes the servlet.
     */
    @Override
	public void init() throws ServletException {
        AddProtocols.run();
        /*
        App.setMasterAppName(getAppName());
        final App.PropertiesLoader parentPL = App.getPropertiesLoader();
        App.setPropertiesLoader(new App.PropertiesLoader() {
    		public Properties loadProperties(String appName) throws IOException {
    			Properties parentProps;
    	        try {
    	        	parentProps = parentPL.loadProperties(getAppName());
    	        } catch(IOException ioe) {
    	            log.warn("While reading properties file:",ioe);
    	            parentProps = null;
    	        }
    	        Properties props = new Properties(parentProps);
    	        for(Enumeration<?> en = getServletConfig().getInitParameterNames(); en.hasMoreElements(); ) {
    	        	String key = (String)en.nextElement();
    	        	props.setProperty(key, getServletConfig().getInitParameter(key));
    	        }
    			return props;
    		}
    	});
		*/
        log = Log.getLog(getClass().getName());

        // read web.xml for servlet mapping
		Collection<String> mappings = getServletContext().getServletRegistration(getServletName()).getMappings();
		servletMappings = mappings.toArray(new String[mappings.size()]);
		log.debug("Found these servlet mappings: " + StringUtils.join(servletMappings, ", "));
		/*
		try {
		    servletMappings = getServletMappings(getServletName(), getServletContext().getResourceAsStream("/WEB-INF/web.xml"));
		    log.debug("Found these servlet mappings: " + StringUtils.join(servletMappings, ", "));
		} catch(IOException e) {
		    log.warn("Could not parse web.xml for servlet mapping", e);
		} catch (SAXException e) {
		    log.warn("Could not parse web.xml for servlet mapping", e);
		} catch (ParserConfigurationException e) {
		    log.warn("Could not parse web.xml for servlet mapping", e);
		}
		//if(servletMappings == null || servletMappings.length == 0) servletMappings = new String[] {"*.i"}; // the default
		*/
        //load config
        configChanged();
        log.info("Servlet '" + getServletConfig().getServletName() + "' Initialized");
    }

    /**
     * Returns the application name. The properties file for this servlet is
     * expected to be named the same as the application name, with a ".properties"
     * extention. For example, if the application name is "myapp" the servlet
     * attempts to find a file "myapp.properties" as its properties file.
     */
    protected String getAppName() {
        String s = getClass().getName();
        int p = s.lastIndexOf('.');
        if(s.endsWith("Servlet")) return s.substring(p+1,s.length()-7);
        else return s.substring(p+1);
    }

    protected void addActionsFile(String filePath, long minInterval) throws IOException, LoadingException {
    	log.debug("Loading the Actions in the file '" + filePath + "' into simple.servlet.SimpleServlet");
    	try {
    		ConfigSource actionsFile = ConfigSource.createConfigSource(filePath);
            if(actionsFiles.add(actionsFile)) {
	            actionsFile.registerLoader(actionsLoader, minInterval, 0);
	            actionsFile.reload(); // ensure that it is loaded initially!
            } else {
            	log.info("Actions File '" + filePath + "' is already loaded");
            }
        } catch(IOException e) {
            log.warn("Could not load file '" + filePath + "'", e);
            throw e;
        } catch (LoadingException e) {
            log.warn("Could not load file '" + filePath + "'", e);
            throw e;
        }
    }
    /**
     * Handles the initialization of the servlet. This method is called when
     * the servlet is initialized. It parses the action-config-files, security-off,
     * data-layer-files, method-start, package-delimit, max-request-size, and temp-dir
     * init params.
     * @throws ServletException 
     */
    protected synchronized void configChanged() throws ServletException {
    	//refresh interval
    	getServletContext().setAttribute("refresh-interval", getRefreshInterval());
    	//action config file
		String acf = getActionConfigFiles();
		if(!StringUtils.isBlank(acf)) {
			StringTokenizer token = new StringTokenizer(acf, "\n\r\t:;,");
			while(token.hasMoreTokens()) {
				String filePath = token.nextToken().trim();
				try {
					addActionsFile(filePath, refreshInterval);
				} catch(IOException e) {
					log.warn("Could not load file '" + filePath + "'", e);
				} catch(LoadingException e) {
					log.warn("Could not load file '" + filePath + "'", e);
				}
			}
		}
        //FileUpload stuff
        int maxRequestBytes;
        String mrs = getMaxRequestSize();
        if(mrs != null && mrs.length() > 0) {
	        try {
	        	maxRequestBytes = FileSizeFormat.parse(mrs).intValue();
	        } catch(NumberFormatException e) {
	        	log.warn("Could not convert '" + mrs + "' to a file size", e);
	        	maxRequestBytes = 5 * 1024 * 1024;
	        } catch(ConversionException e) {
		    	log.warn("Could not convert '" + mrs + "' to a string", e);
		    	maxRequestBytes = 5 * 1024 * 1024;
	        }
        } else
        	maxRequestBytes = 5 * 1024 * 1024;
        String td = getTempDir();
        File tempDirFile = null;
        if (td == null || td.length() == 0) {
            tempDirFile = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
            if(tempDirFile == null) 
            	td = System.getProperty("java.io.tmpdir");
        }
        if(tempDirFile == null) 
        	tempDirFile = new java.io.File(td);
        if(!tempDirFile.exists()) try {
            tempDirFile.createNewFile();
        } catch(IOException ioe) {
            log.warn("Could not create Temp Folder, '" + tempDirFile.getAbsolutePath() + "'", ioe);
        }

        upload = RequestUtils.createFileUpload(tempDirFile, maxRequestBytes);

        String[] mp = getMaskedParameters();
        if(mp != null)
	    	for(String e : mp)
	        	maskedParameterNames.add(e);
        
        for(Map.Entry<String,Object> entry : applicationAttributes.entrySet()) {
        	getServletContext().setAttribute(entry.getKey(), entry.getValue());
        }
		getServletContext().setAttribute(ATTRIBUTE_XSRF_PROTECTION, xsrfProtection);
    }
    
    public Object getApplicationAttribute(String attributeName) {
    	return applicationAttributes.get(attributeName);
    }
    
    public void setApplicationAttribute(String attributeName, Object attributeValue) {
    	applicationAttributes.put(attributeName, attributeValue);
    }
    
    /**
     * This method is called when the properties file for this application is
     * changed. The property "refresh-interval" determines how often (in millesecons)
     * the servlet checks for changes in its properties file.
     */
    protected void propertiesChanged(Properties props) {
    }

    /**
     *  Ensures that the properties refresher stops.
     */
    @Override
	public void destroy() {
    	super.destroy();
        log.info("Servlet Finalized");
        Log.finish();
    }

    /**
     * Process requests.
     * Creation date: (5/25/2001 11:30:16 AM)
     * @param request javax.servlet.http.HttpServletRequest
     * @param response javax.servlet.http.HttpServletResponse
     */
    protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        long start = System.currentTimeMillis();
        //try { //this is for a bug in WAS 4.01 which ignores error pages, so we do it manually, here
            //Create a request handler object and call its handleRequest method
            //for debugging
		Object oldForm = request.getAttribute(ATTRIBUTE_FORM);
		try {
			processInit(request, response);
			InputForm form = processInput(request, response);
			processRequest(form, request, response);
			processComplete(request, response);
		} finally {
			request.setAttribute(ATTRIBUTE_FORM, oldForm);
		}
      /*
      } catch(Throwable e) {
            log.info("Exception occured: ", e);
            if(e instanceof ServletException) log.info("Root Cause: ", ((ServletException)e).getRootCause());
            try { response.reset(); } catch(Throwable t) {}
            request.setAttribute("javax.servlet.jsp.jspException", e);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/error.jsp");
            try {rd.forward(request, response); } catch(Throwable t) {rd.include(request, response);}
            response.setHeader("Success", "false");
            response.setHeader("ErrorMsg", e.getMessage());
        }//*/
        log.debug("**Request took " + (System.currentTimeMillis() - start) + " milleseconds to process.");
    }

    /** Perform any processing necessary before the request is processed. This implementation
     * only logs info about the request.
     * @param request Object that encapsulates the request to the servlet
     * @param response Object that encapsulates the response to the servlet
     * @throws ServletException If an exception occurs
     */
    public void processInit(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        if(log.isDebugEnabled()) {
            StringBuilder sb = new StringBuilder();
            // header info
            for(Enumeration<?> en = request.getHeaderNames(); en.hasMoreElements(); ) {
                String name = (String)en.nextElement();
                String value = request.getHeader(name);
                if(sb.length() > 0) sb.append("; ");
                sb.append(name);
                sb.append(" = ");
                sb.append(value);
            }
            log.debug("Request received with headers = [" + sb.toString() + "]");
            // request info
            sb.setLength(0);
            for(Enumeration<?> en = request.getAttributeNames(); en.hasMoreElements(); ) {
                String name = en.nextElement().toString();
                Object value =  request.getAttribute(name);
                if(sb.length() > 0) sb.append("; ");
                sb.append(name);
                sb.append(" = ");
                sb.append(value);
           }
           log.debug("Request received with attributes = [" + sb.toString() + "]");
        }
		if(!isAllowClientCaching()) {
			response.setHeader("Cache-Control", "no-cache"); // HTTP 1.1.
			response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			response.setHeader("Expires", "0"); // Proxies.
		}
    }

    /** Perform any processing necessary after the request is processed. This implementation
     * flushes the buffer.
     * @param request Object that encapsulates the request to the servlet
     * @param response Object that encapsulates the response to the servlet
     * @throws ServletException If an exception occurs
     */
    public void processComplete(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
            response.flushBuffer();
        } catch (IOException e) {
            log.info("Could not flush buffer", e);
        }
    }

    /** Handles requests for which an actions class could not be found.
     * @param dispatcher The dispatcher for this request. Use this to call another action or to forward
     * processing to a file resource like a JSP or HTML file
     * @param form The InputForm for this request. It holds all the parameters and attributes for
     * this request. Acts as a DynaBean with the attributes of the request (and session
     * and application) as its properties
     * @param request Object that encapsulates the request to the servlet
     * @param response Object that encapsulates the response to the servlet
     * @throws ServletException If an exception occurs
     */
    public void notFound(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		if(globalCatches != null && SimpleAction.forwardToCatch(globalCatches.iterator(), new ActionNotFoundException("Cannot handle request, '" + request.getServletPath() + "'."), dispatcher, form, request, response))
			return;
		log.info("Cannot handle request, '" + request.getServletPath() + "'.");
        try {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } catch(IOException ioe) {
            throw new ServletException(ioe);
        }
        //dispatcher.include("/RequestNotHandled.html");
    }

    /** Puts each parameter into the request's attributes. Also, handles multipart
     * content by parsing the request and creating FileItems to hold uploaded
     * files' data. Finally, creates an InputForm and puts it into the request's
     * attributes as well.
     * @return The initialized InputForm object
     * @param request Object that encapsulates the request to the servlet
     * @param response Object that encapsulates the response to the servlet
     * @throws ServletException If an exception occurs
     */
    public InputForm processInput(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        // we must restrict the parameters to not overwrite anything in the session or application
        // e.g. - the user
    	CompositeSet<String> restricted = new CompositeSet<String>();
        restricted.merge(getRestrictedParameterNames());
        Set<String> sessionAttributes = new HashSet<String>();
        for(int i = 0; i < 5; i++) // retry 5 times
	        try {
		        for(Enumeration<?> en = request.getSession().getAttributeNames(); en.hasMoreElements(); )
		        	sessionAttributes.add((String)en.nextElement());
		        break;
	        } catch(java.util.ConcurrentModificationException e) {
	        	log.debug("While getting session attribute names", e);
	        }
        for(int i = 0; i < 5; i++) // retry 5 times
	        try {
	        	for(Enumeration<?> en = request.getSession().getServletContext().getAttributeNames(); en.hasMoreElements(); )
	            	sessionAttributes.add((String)en.nextElement());
	            break;
	        } catch(java.util.ConcurrentModificationException e) {
	        	log.debug("While getting application attribute names", e);
	        }
        restricted.merge(sessionAttributes);

        Map<String,Object> parameters;
        try {
        	parameters = RequestUtils.getRequestParameters(request, restricted, maskedParameterNames, upload);
        } catch(FileUploadException fue) {
            throw new ServletException(fue);
        }
		RequestUtils.carryOverAttributes(request, restricted);
        if(log.isInfoEnabled()) {
	        StringBuilder sb = new StringBuilder();
	        sb.append("Request received for '").append(request.getServletPath()).append("' with params = [");
	        boolean first = true;
	        for(Map.Entry<String, Object> entry : parameters.entrySet()) {
	        	if(first)
	        		first = false;
	        	else
	        		sb.append("; ");
	        	sb.append(entry.getKey()).append('=');
	        	String valueString;
	        	try {
	        		valueString = ConvertUtils.convert(String.class, entry.getValue());
				} catch(ConvertException e) {
					if(entry.getValue() == null)
						valueString = "";
					else 
						valueString = entry.getValue().toString();
				}
	        	if (valueString == null)
	        		valueString = "";
	            if(maskedParameterNames.contains(entry.getKey()) || truncatedParameterNames.contains(entry.getKey()))
	            	sb.append("<").append(valueString.length()).append(" bytes>");
	            else
	            	sb.append(valueString.replaceAll("\\r", "\\\\r").replaceAll("\\n", "\\\\n").replaceAll(DEFAULT_MASKED_REGEX,"*"));
	        }
	    	sb.append(']');
	        log.info(sb.toString());
        }
        if(isCopyParamsToAttributes()) {
        	for(Map.Entry<String, Object> entry : parameters.entrySet()) {
        		if(request.getAttribute(entry.getKey()) == null)
        			request.setAttribute(entry.getKey(), entry.getValue());
        	}
        	//parameters = Collections.emptyMap();
        }
        InputForm form = new SimpleInputForm(request, parameters);

        // If this request was forwarded or included, the parameters may have changed so create a new form
        request.setAttribute(ATTRIBUTE_FORM, form);
        return form;
    }

    protected Object convertParameter(Object value) {
    	if(value instanceof String) {
    		return ((String)value).replace((char)160,' ').trim();
    	} else if(value instanceof String[]) {
    		String[] arr = (String[]) value;
    		for(int i = 0; i < arr.length; i++) {
    			arr[i] = arr[i].replace((char)160,' ').trim();
    		}
    		return arr;
    	} else {
    		return value;
    	}
    }

    protected Set<String> getRestrictedParameterNames() {
        return restrictedParameterNames;
    }

	public String[] getRetrictedParameters() {
		return restrictedParameterNames.toArray(new String[restrictedParameterNames.size()]);
	}

	public void setRetrictedParameters(String... retrictedParameters) {
		restrictedParameterNames.clear();
		for(String rp : retrictedParameters)
			restrictedParameterNames.add(rp);
	}
    /** Processes the request by creating a dispatcher and calling dispatch() on it.
     * @param form The InputForm for this request. It holds all the parameters and attributes for
     * this request. Acts as a DynaBean with the attributes of the request (and session
     * and application) as its properties
     * @param request Object that encapsulates the request to the servlet
     * @param response Object that encapsulates the response to the servlet
     * @throws ServletException If an exception occurs
     */
    public void processRequest(InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	//set the original request URI and query string    	
    	if (form.getAttribute(ATTRIBUTE_REQUEST_URI_QUERY) == null)
    		form.setAttribute(ATTRIBUTE_REQUEST_URI_QUERY, RequestUtils.getRequestURIQuery(request));
    	Dispatcher d = new SimpleDispatcher(request, response, form);
        log.debug("Dispatch request for '" + request.getServletPath() + "' + '" + request.getPathInfo() + "'");
        String path = (String)request.getAttribute("javax.servlet.include.servlet_path");
        if(path == null) path = request.getServletPath();
        path = formatPath(stripMapping(path));
        String pathInfo = formatPath(request.getPathInfo());
        if(pathInfo == null) pathInfo = "";
        String dispatch = formatPath(form.getString(getDispatchName(),false));
        d.dispatch(path + pathInfo + dispatch, false);
    }

    /**
     * Default implementation of the Dispatcher interface. Uses a j2ee RequestDispatcher
     * for forward() and include() methods and uses the Simple Actions Structure
     * for the dispatch() method.
     */
    protected class SimpleDispatcher implements Dispatcher {
        protected HttpServletRequest request;
        protected HttpServletResponse response;
        protected InputForm form;
        /** Creates new SimpleDispatcher */
        public SimpleDispatcher(HttpServletRequest request, HttpServletResponse response, InputForm form) {
            this.request = request;
            this.response = response;
            this.form = form;
        }

        /** Returns the InputForm for the request that generated this Dispatcher
         * @return The InputForm object
         */
        public InputForm getForm() { return form; }

        /** Forwards processing of this request using SimpleServlet's built-in request routing
         * @param path The path is used to route this request
         * @throws ServletException If an exception occurs
         */
        public void dispatch(String path, boolean privateReferer) throws ServletException {
        	String searchPath = formatPath(path);
            //set the original search path
			if(form.get(ATTRIBUTE_SEARCH_PATH) == null)
				form.set(ATTRIBUTE_SEARCH_PATH, searchPath);
			form.set(ATTRIBUTE_CURRENT_PATH, searchPath);

            Action am = getAction(searchPath);
			if(am == null)
				notFound(this, form, request, response);
			else
				try {
					checkAction(am, request, form, privateReferer);
					am.invoke(this, form, request, response);
				} catch(ServletException e) {
					// let's see if an global catches will handle this
					SimpleAction.handleException(globalCatches == null ? null : globalCatches.iterator(), e, this, form, request, response);
				}
        }

        /** Includes the request processing of the specified web resource using the ServletContainer's RequestDispatcher
         * @param path The web resource to which processing is passed
         * @throws ServletException If an exception occurs
         */
        public void include(String path) throws ServletException {
            RequestDispatcher rd = getJspDispatcher(path);
            try {
                rd.include(request, response);
            } catch(IOException ioe) {
                throw new ServletException(ioe);
            }
        }

        /** Forwards the request processing to the specified web resource using the ServletContainer's RequestDispatcher
         * @param path The web resource to which processing is passed
         * @throws ServletException If an exception occurs
         */
        public void forward(String path) throws ServletException {
            RequestDispatcher rd = getJspDispatcher(path);
            try {
                rd.forward(request, response);
            } catch(IOException ioe) {
                throw new ServletException(ioe);
            }
        }

        /** Intercepts the request processing response of the specified web resource using the WrappedServletRequest
         * and WrappedServletResponse objects. This only works on Servlets 2.3 and higher
         * @param path The web resource to which processing is passed and whose response is intercepted
         * @param out The OutputStream into which the response is written
         * @throws ServletException If an exception occurs
         */
        public void intercept(String path, OutputStream out) throws ServletException {
            RequestDispatcher rd = getJspDispatcher(path);
            Interceptor.intercept(rd, request, response, out);
        }
        
        /** Intercepts the request processing response of the specified web resource using the WrappedServletRequest
         * and WrappedServletResponse objects. This only works on Servlets 2.3 and higher
         * @param path The web resource to which processing is passed and whose response is intercepted
         * @param writer The Writer into which the response is written
         * @throws ServletException If an exception occurs
         */
        public void intercept(String path, Writer writer) throws ServletException {
            RequestDispatcher rd = getJspDispatcher(path);
            Interceptor.intercept(rd, request, response, writer);
        }
    }

    /** Finds or creates a RequestDispatcher for the specified path.
     * @return A RequestDispatcher for the specified path
     * @param path The path to the JSP or HTML or other file
     * @throws ServletException If an exception occurs
     */
    public RequestDispatcher getJspDispatcher(String path) throws ServletException {
        RequestDispatcher rd = jspDispatchers.get(path);
        if(rd == null) {
            if(!path.startsWith("/")) path = "/" + path;
            rd = getServletContext().getRequestDispatcher((!path.startsWith("/") ? "/" + path : path));
            jspDispatchers.put(path, rd);
        }
        return rd;
    }

    /**
     * Returns the name of a parameter on a request that can be used to specify
     * the method name of an actions call. For example, if the URL "myapp/.i"
     * was requested with a parameter named by the dispatch name which had
     * a value of "mymethod", the servlet would translate that to be processed the
     * same as if the URL "myapp/mymethod.i" was requested.
     */
    protected String getDispatchName() {
        return "dispatch";
    }

    /**
     * Returns an Action object that matches the given path.
     */
    protected Action getAction(String searchPath) {
        log.debug("Get Action for path '" + searchPath + "'");
        /*log.debug("+++ Real Path='" + getServletContext().getRealPath("") +
            //"'; Resource='" + getServletContext().getResource("") +
            "'; ServerInfo='" + getServletContext().getServerInfo() +
            "'; ContextName='" + getServletContext().getServletContextName() +
            "'; NamedDispatcher='" + getServletContext().getNamedDispatcher(getServletName()) +
            "'; ServletInfo='" + getServletInfo() +
            "'; ServletName='" + getServletName() + " +++");*/
        Action act = actions.get(searchPath);
        if(act instanceof SimpleSourcedAction) { // reload the source file if necessary
            try {
                if(((SimpleSourcedAction)act).getSourceFile().reload())
                    act = actions.get(searchPath);
            } catch(LoadingException e) {
                log.warn("Could not reload actions file, '" + ((SimpleSourcedAction)act).getSourceFile() + "'", e);
            }
        } else if(act == null) {
        	for(ConfigSource src : actionsFiles) {
        		try {
					if(src.reload()) {
						act = actions.get(searchPath);
						if(act != null)
							break;
					}
				} catch(LoadingException e) {
					log.warn("Could not reload actions file, '" + src + "'", e);
				}
        	}
        }
        return act;
    }

    /**
     * Implements the security of the SimpleServlet. If the security-off init param
     * is false then it checks that the Action is accessible and that the
     * current ServletUser has the required privileges.
     */
    protected void checkAction(Action action, HttpServletRequest request, InputForm form, boolean privateReferer) throws ServletException {
        if(securityOff) return;
        if(!action.isAccessible()) {
            log.debug("Action for " + action + " is not accessible");
            throw new NotAuthorizedException("You are not authorized to use this function.");
        }
        if(action.isUserRequired()) {
            ServletUser user = getUser(request);
            if(user == null) {
                log.debug("Action " + action + " requires a user and no user object is present in the session");
                throw new NotLoggedOnException();
            }
			if(getXsrfProtection() == XsrfProtectionLevel.ENABLED_FOR_ALL || (getXsrfProtection() == XsrfProtectionLevel.ENABLED_FOR_SOME && action.isXsrfProtected())) {
            	String storedSessionToken = RequestUtils.getSessionToken(request);
            	String requestedSessionToken = form.getString(REQUEST_SESSION_TOKEN, false);
            	if(requestedSessionToken == null)
            		throw new NoSessionTokenException("Session Token was not provided");
            	if(!requestedSessionToken.equals(storedSessionToken))
            		throw new InvalidSessionTokenException("Session Token is invalid");
            }
			if(!action.isAuthorized(user)) {
				log.info("User '" + user.getUserName() + "' does not have " + action.getAuthorizationRequirements());
				throw new NotAuthorizedException("You are not authorized to perform this function.");
            }
        }
		if(action.isPrivate() && !privateReferer) {
        	throw new NotAuthorizedException("You are not authorized to use this function.");
        }
    }

    /** Returns the user as stored in the HttpSession. If no user is found, login() is called
     * to retrieve it. NOTE: This method does not save the user retrieved from the login()
     * method into the session. Not every implementation may want the user stored in the session
     * from request to request.
     * @return The ServletUser for this session or null if none exists
     * @param request Object that encapsulates the request to the servlet
     * @throws ServletException If an exception occurs
     */
    public ServletUser getUser(HttpServletRequest request) throws ServletException {
        ServletUser user = (ServletUser) request.getSession().getAttribute(ATTRIBUTE_USER);
        if(user == null) user = login(request);
        if(log.isDebugEnabled() && user == null) log.debug("Could not find user in attribute '" + ATTRIBUTE_USER + "'");
        return user;
    }

    /**
     * Creates a new user. Called whenever getUser() can't find a user in the
     * current session. The default implementation is to return nothing which
     * causes the checkAction method to throw a NotLoggedOnException.
     * This allows an Application to specify a "login" page as
     * the error page for a NotLoggedOnException.
     * Sub-classes may wish to provide their own implementation of this method to handle the user
     * management.
     */
    @SuppressWarnings("unused")
	protected ServletUser login(HttpServletRequest request) throws ServletException {
        /*if("headers".equalsIgnoreCase(securityType)) {
            //get user from header values
            StringBuilder sb = new StringBuilder();
            for(Enumeration en = request.getHeaderNames(); en.hasMoreElements(); ) {
                String name = en.nextElement().toString();
                Enumeration values =  request.getHeaders(name);
                sb.append(name);
                sb.append(" = [");
                while(values.hasMoreElements()) {
                    sb.append(values.nextElement());
                    if(values.hasMoreElements()) sb.append(", ");
                }
                sb.append("]");
                if(en.hasMoreElements()) sb.append("; ");
            }
            log.debug("HEADERS-->" + sb.toString());
        } else*/ if("container".equalsIgnoreCase(securityType) && request.getAuthType() != null) {
            //get user from container
            ContainerServletUser user = (ContainerServletUser) request.getSession().getAttribute(ContainerServletUser.ATTRIBUTE_CONTAINER_USER);
            if(user == null) {
                user = new ContainerServletUser();
                request.getSession().setAttribute(ContainerServletUser.ATTRIBUTE_CONTAINER_USER, user);
            }
            request.setAttribute(ATTRIBUTE_USER, user);
            user.setRequest(request);
            return user;
        }
        return null;
    }

    /**  Loads all the actions from the action-config-file(s)
     *
     */
    protected void loadActions(ConfigSource src) {
        try {
            actions.keySet().removeAll(src.getLoadKeys());
            InputStream in = src.getInputStream();
            Map<String,Object> root;
            try {
                root = actionXMLLoader.load(in);
            } finally {
                in.close();
            }
            if(!"actions".equalsIgnoreCase(root.get("tag").toString())) throw new SAXException("Invalid XML: root entity must be 'actions'");
            String basePath = formatPath((String)root.get("basePath"));
            // process global catches
			int catchIndex = globalCatches.size();
			for(Object o : src.getLoadKeys()) {
				int i = globalCatches.indexOf(o);
				if(i >= 0 && i < catchIndex) {
					catchIndex = i;
				}
			}
            globalCatches.removeAll(src.getLoadKeys());

            // NOTE: (These will be matched in order that they are loaded, so catches loaded first override those after)
            List<?> catchList = (List<?>)root.get("catchs");
            if(catchList != null) {
            	List<Catch> catches = new ArrayList<Catch>(catchList.size());
                for(Iterator<?> iter = catchList.iterator(); iter.hasNext(); ) {
                    try {
                        Map<?,?> cc = (Map<?,?>)iter.next();
                        log.debug("Creating catch with config " + cc);
                        Catch c = loadCatch(cc);
                        if(!catches.contains(c)) {
	                        src.addLoadKey(c);
	                        catches.add(c);
	                        log.debug("Added a global catch for '" + c + "'");
                        } else {
                        	log.debug("A global catch for '" + c + "' is already defined; Ignoring this entry");
                        }
                    } catch(ConvertException e) {
                        log.warn("While loading catch from '" + src + "'", e);
                    } catch (ClassNotFoundException e) {
                        log.warn("While loading catch from '" + src + "'", e);
                    } catch (InstantiationException e) {
                        log.warn("While loading catch from '" + src + "'", e);
                    } catch (IllegalAccessException e) {
                        log.warn("While loading catch from '" + src + "'", e);
                    } catch (StepConfigException e) {
                        log.warn("While loading catch from '" + src + "'", e);
                    }
                }
				globalCatches.addAll(catchIndex, catches);
            }

            List<?> actionList = (List<?>)root.get("actions");
            if(actionList != null) {
                log.debug("Found " + actionList.size() + " actions...");
                for(Iterator<?> actionIter = actionList.iterator(); actionIter.hasNext(); ) {
                    try {
                    	Map<String,Object> actionMap = CollectionUtils.uncheckedMap((Map<?,?>)actionIter.next(), String.class, Object.class);
                        String path = basePath + formatPath(actionMap.get("path").toString());
                        // if an action for this path already exists, skip this
                        if(!actions.containsKey(path)) {
                            SimpleAction action = new SimpleSourcedAction(src);
                            log.debug("Created a SimpleSourcedAction; setting properties...");
							ReflectionUtils.populateProperties(action, actionMap);

                            //steps
                            log.debug("Loading Steps...");
                            List<?> steps = (List<?>)actionMap.get("*"); // '*' = all sub-entities
                            if(steps != null) {
                                for(Object cfg : steps) {
                                	Step step = loadStep(CollectionUtils.uncheckedMap((Map<?,?>)cfg, String.class, Object.class));
                                    if(step != null) {
                                        action.addStep(step);
                                        log.debug("Added a Step of class '" + step.getClass().getName() + "'");
                                    }
                                }
                            }

                            //privs
                            Object privs = actionMap.get("privs");
							String privString;
							if(privs instanceof String)
								privString = (String) privs; // this is the new way
							else if(privs instanceof List<?>)
								privString = StringUtils.join((List<?>) privs, ",");
							else if(privs != null)
								privString = privs.toString();
							else
								privString = null;

							action.setRequiredPrivileges(privString);

                            //catches
                            List<?> catches = (List<?>)actionMap.get("catchs");
                            if(catches != null) {
                                for(Iterator<?> iter = catches.iterator(); iter.hasNext(); ) {
                                    Map<?,?> props = (Map<?,?>)iter.next();
                                    //add catch at action level
                                    try {
                                        action.addCatch(loadCatch(props));
                                    } catch(ConvertException e) {
                                        log.warn("While loading catch from '" + src + "'", e);
                                    } catch (ClassNotFoundException e) {
                                        log.warn("While loading catch from '" + src + "'", e);
                                    } catch (InstantiationException e) {
                                        log.warn("While loading catch from '" + src + "'", e);
                                    } catch (IllegalAccessException e) {
                                        log.warn("While loading catch from '" + src + "'", e);
                                    } catch (StepConfigException e) {
                                        log.warn("While loading catch from '" + src + "'", e);
                                    }
                                }
                            }

                            /* NOTE: This is not necessary as the display method on SimpleDispatcher forwards to global catches
                            // add global catches
                            for(Iterator catchIter = globalCatches.iterator(); catchIter.hasNext(); )
                                action.addCatch((Catch)catchIter.next());
							*/

                            // add to actions cache
                            actions.put(path, action);
                            src.addLoadKey(path);
                            log.debug("Added an Action for the path '" + path + "'");
                        } else {
                            log.debug("An Action for the path '" + path + "' is already defined; Ignoring this entry");
                        }
                    } catch(ConvertException e) {
                        log.warn("While loading actions from '" + src + "'", e);
                    } catch (ClassNotFoundException e) {
                        log.warn("While loading actions from '" + src + "'", e);
                    } catch (InstantiationException e) {
                        log.warn("While loading actions from '" + src + "'", e);
                    } catch (IllegalAccessException e) {
                        log.warn("While loading actions from '" + src + "'", e);
                    } catch (StepConfigException e) {
                        log.warn("While loading actions from '" + src + "'", e);
                    } catch (IntrospectionException e) {
                        log.warn("While loading actions from '" + src + "'", e);
                    } catch (InvocationTargetException e) {
                        log.warn("While loading actions from '" + src + "'", e);
                    } catch(ParseException e) {
                        log.warn("While loading actions from '" + src + "'", e);
					}
                }
            } else {
                log.info("No Actions Found");
            }
        } catch(IOException e) {
            log.warn("While loading actions from '" + src + "'", e);
        } catch (SAXException e) {
            log.warn("While loading actions from '" + src + "'", e);
        } catch (ParserConfigurationException e) {
            log.warn("While loading actions from '" + src + "'", e);
        }
    }

    /** Creates a Step object from the specified property map and configures it
     * @param m The property map that holds the Step configuration information
     * @throws Exception If an exception occurs
     * @return The new Step object
     * @throws ClassNotFoundException
     * @throws SAXException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws StepConfigException
     */
    public static Step loadStep(Map<String,?> m) throws ClassNotFoundException, SAXException, InstantiationException, IllegalAccessException, StepConfigException {
        String tag = m.get("tag").toString();
        if("priv".equalsIgnoreCase(tag) || "catch".equalsIgnoreCase(tag)) {
            //ignore these
            return null;
        } else if("return".equalsIgnoreCase(tag)) {
            return SimpleAction.RETURN_STEP;
        } else{
            Class<? extends Step> stepClass;
            if("step".equalsIgnoreCase(tag)) {
                String className = (String)m.get("class");
                sLog.debug("Find Step class, '" + className + "'");
                stepClass = Class.forName(className).asSubclass(Step.class);
            } else {
                stepClass = stepClasses.get(tag);
                if(stepClass == null) {
                    try {
                        String className = "simple.servlet.steps." + tag.substring(0,1).toUpperCase() + tag.substring(1) + "Step";
                        sLog.debug("Find Step class, '" + className + "'");
                        stepClass = Class.forName(className).asSubclass(Step.class);
                        stepClasses.put(tag, stepClass);
                    } catch(ClassNotFoundException cnfe) {
                        throw new SAXException("Invalid XML: expecting 'step' or a step name tag, not '" + tag + "'", cnfe);
                    }
                }
            }
            Step step = stepClass.newInstance();
            step.configure(m);
            sLog.debug("Step created and configured");
            return step;
        }
    }

    /** Creates a Catch object from the specified property map
     * @param m The property map that holds the Catch configuration information
     * @throws Exception If an exception occurs
     * @return The new Catch object
     * @throws ConvertException
     * @throws StepConfigException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws SAXException
     * @throws ClassNotFoundException
     */
    public static Catch loadCatch(Map<?,?> m) throws ConvertException, ClassNotFoundException, SAXException, InstantiationException, IllegalAccessException, StepConfigException {
        Catch c = new Catch();
        c.setPage((String)m.get("page"));
        c.setAction((String)m.get("action"));
        c.setExceptionClass(((Class<?>)simple.bean.ConvertUtils.convert(Class.class, m.get("class"))).asSubclass(Throwable.class));
        List<?> list = simple.bean.ConvertUtils.convert(List.class, m.get("*"));
        if(list != null) {
        	for(Object cfg : list) {
            	Map<String,Object> sub = CollectionUtils.uncheckedMap((Map<?,?>)cfg, String.class, Object.class);
                String tag = sub.get("tag").toString();
                if("criteria".equalsIgnoreCase(tag)) c.addCriteria((String)sub.get("property"),(String)sub.get("operator"), (String)sub.get("value"));
                else c.addStep(loadStep(sub));
            }
        }
        return c;
    }

    /** Strips the servlet mapping from the path
     * @param path The path to manipulate
     * @return The path without the servlet mapping piece
     */
    protected String stripMapping(String path) {
        if(path == null || (path=path.trim()).length() == 0) return "";
        //strip servlet match part
        if(servletMappings == null) { // guess the mapping as *.something
            log.debug("Servlet Mappings are unknown; try to deduce the action path as '/path.something'");
            int p = path.lastIndexOf('.');
            if(p > 0) path = path.substring(0, p);
        } else {
            for(int i = 0; i < servletMappings.length; i++) {
                int p = servletMappings[i].indexOf("*");
                String prefix;
                String suffix;
                if(p == -1) {
                    prefix = servletMappings[i];
                    suffix = "";
                } else {
                    prefix = servletMappings[i].substring(0,p);
                    suffix = servletMappings[i].substring(p+1);
                }
                if(path.startsWith(prefix) && path.endsWith(suffix)) {
                    path = path.substring(prefix.length(), path.length() - suffix.length());
                    break;
                }
            }
        }
        return path;
    }

    /** Formats the path in a standard way
     * @param path The path to format
     * @return The formatted path
     */
    protected String formatPath(String path) {
        if(path == null || (path=path.trim()).length() == 0) return "";
        StringBuilder sb = new StringBuilder(path);

        //modify slashes
        if(!slashSensitive) StringUtils.replace(sb, SLASHES, '/');
        if(sb.charAt(sb.length()-1) == '/') sb.setLength(sb.length() - 1);

        //ensure leading slash
        if(sb.length() > 0 && sb.charAt(0) != '/') sb.insert(0, '/');

        //modify to lowercase if necessary
        if(!caseSensitive) return sb.toString().toLowerCase();
        else return sb.toString();
    }

    /** Parses the web.xml file to find the servlet mappings for the specified servlet name.
     * @return An array of servlet mappings for this servlet
     * @param servletName The name of the servlet for which to find mappings
     * @param webXml An InputStream with the xml of the web configuration (from the web.xml file)
     * @throws IOException If an IO exception occurs
     * @throws SAXException If a parsing exception occurs
     * @throws ParserConfigurationException If a parser configuration error occurs
     */
	public static String[] getServletMappings(String servletName, java.io.InputStream webXml) throws IOException, SAXException, ParserConfigurationException {
        XMLLoader<Map<String,Object>> loader = new simple.xml.MapListXMLLoader();
        loader.setValidating(false);
        Map<String,Object> m = loader.load(webXml);
        List<?> l = (List<?>) m.get("servlet-mappings");
        List<String> mappings = new ArrayList<String>();
		if(l != null)
			for(Iterator<?> iter = l.iterator(); iter.hasNext();) {
				Map<?, ?> mapping = (Map<?, ?>) iter.next();
				String nm = ConvertUtils.getStringSafely(((Map<?, ?>) ((List<?>) mapping.get("servlet-names")).get(0)).get(""));
				if(servletName.equals(nm.trim())) {
					String uc = ConvertUtils.getStringSafely(((Map<?, ?>) ((List<?>) mapping.get("url-patterns")).get(0)).get(""));
					mappings.add(uc.trim());
				}
			}
        return mappings.toArray(new String[mappings.size()]);
    }

    /** Provides a means of reloading Action's when the actions configuration file changes */
    protected static class SimpleSourcedAction extends SimpleAction {
        /** Holds the ConfigSource value */
        protected ConfigSource src;
        /** Creates a new SimpleSourcedAction
         * @param _src The ConfigSource of this Action
         */
        public SimpleSourcedAction(ConfigSource _src) {
            src = _src;
        }
        /** Returns the ConfigSource for this Action
         * @return The ConfigSource for this Action as specified in the constructor of this object
         */
        public ConfigSource getSourceFile() {
            return src;
        }
    }

	public long getRefreshInterval() {
		return refreshInterval;
	}

	public void setRefreshInterval(long refreshInterval) {
		this.refreshInterval = refreshInterval;
	}

	public boolean isSecurityOff() {
		return securityOff;
	}

	public void setSecurityOff(boolean securityOff) {
		this.securityOff = securityOff;
	}

	public String getSecurityType() {
		return securityType;
	}

	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}

	public boolean isCaseSensitive() {
		return caseSensitive;
	}

	public void setCaseSensitive(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

	public boolean isSlashSensitive() {
		return slashSensitive;
	}

	public void setSlashSensitive(boolean slashSensitive) {
		this.slashSensitive = slashSensitive;
	}

	public String getActionConfigFiles() {
		return actionConfigFiles;
	}

	public void setActionConfigFiles(String actionConfigFiles) {
		this.actionConfigFiles = actionConfigFiles;
	}

	public String getMaxRequestSize() {
		return maxRequestSize;
	}

	public void setMaxRequestSize(String maxRequestSize) {
		this.maxRequestSize = maxRequestSize;
	}

	public String getTempDir() {
		return tempDir;
	}

	public void setTempDir(String tempDir) {
		this.tempDir = tempDir;
	}

	public String[] getMaskedParameters() {
		return maskedParameters;
	}

	public void setMaskedParameters(String[] maskedParameters) {
		this.maskedParameters = maskedParameters;
	}

	public boolean isCopyParamsToAttributes() {
		return copyParamsToAttributes;
	}

	public void setCopyParamsToAttributes(boolean copyParamsToAttributes) {
		this.copyParamsToAttributes = copyParamsToAttributes;
	}

	public XsrfProtectionLevel getXsrfProtection() {
		return xsrfProtection;
	}

	public void setXsrfProtection(XsrfProtectionLevel xsrfProtection) {
		this.xsrfProtection = xsrfProtection;
	}

	public boolean isAllowClientCaching() {
		return allowClientCaching;
	}

	public void setAllowClientCaching(boolean allowClientCaching) {
		this.allowClientCaching = allowClientCaching;
	}

	public static String getDEFAULT_MASKED_REGEX() {
		return DEFAULT_MASKED_REGEX;
	}

	public static void setDEFAULT_MASKED_REGEX(String dEFAULT_MASKED_REGEX) {
		DEFAULT_MASKED_REGEX = dEFAULT_MASKED_REGEX;
	}
	
	
}
