package simple.servlet;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** A <CODE>Step</CODE> represents one piece of a response to a servlet request.
 * Steps are generally designed to be highly configurable and thus highly reusable.
 * Many of the most common pieces of processing are already implemented in the
 * built-in steps found in the {@link simple.servlet.steps} package. If you need to
 * perform some sort of processing to fulfill a servlet request that is not
 * represented by these built-in steps, create a class that implements this
 * interface. Steps are configured and called in an XML file that follows the
 * Actions Schema {@link /resources/Action.xsd}. See the XML file or the
 * simple.servlet.SimpleServlet {@link simple.servlet.SimpleServlet} for more imformation.
 */
public interface Step {
    /** Performs some piece of processing
     * @param dispatcher The dispatcher for this request. Use this to call another action or to forward
     * processing to a file resource like a JSP or HTML file.
     * @param form The InputForm for this request. It holds all the parameters and attributes for
     * this request. Acts as a DynaBean with the attributes of the request (and session
     * and application) as its properties
     * @param request The ServletRequest that triggered this Step
     * @param response The ServletResponse
     * @throws ServletException If an exception occurs
     */    
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
        HttpServletResponse response) throws ServletException;
    /** Configures the Step with the specified properties. These properties are passed
     * from <CODE>simple.servlet.SimpleServlet</CODE> with heirarchical configuration information
     * represented as <CODE>java.util.List</CODE>s of <CODE>java.util.Map</CODE>s. For
     * each Map each sub-element is found in a List under the Map key that is the name
     * of the element's tag plus "s" and each element's character data is under the key
     * "" (empty String). Thus, for the following XML:<CODE>
     * &lt;parent class="my.test" activate="true"&gt;
     *    &lt;sub id="1"&gt;One&lt;/sub&gt;
     *    &lt;sub id="2"&gt;Two&lt;/sub&gt;
     *    &lt;sub id="3"&gt;Three&lt;/sub&gt;
     *    &lt;other name="Foo" level="Bar"/&gt;
     *    &lt;other name="Bar" level="Foo"/&gt;
     * &lt;/parent&gt;
     * </CODE>
     * A Map would be created with five keys:
     * <ol><li>"class" would contain a String "my.test"</li>
     * <li>"activate" would contain a String "true"</li>
     * <li>"subs" would contain a List object of three Maps</li>
     * <li>"others" would contain a List object of two Maps</li>
     * <li>"*" would contain a List Object of 5 Maps (reference the same Maps created
     * for the "subs" and "others" values</li></OL>
     * @param props The Map of Lists and Strings for configuring this Step.
     * @throws StepConfigException If any error occurs during configuring this Step
     */    
    public void configure(Map<String,?> props) throws StepConfigException;
    /** Adds a Catch to this Step
     * @param c The catch to add
     */    
    public void addCatch(Catch c) ;
    /** An iterator of all the Catch objects registered with this Step
     * @return An Iterator of all Catch objects for this Step
     */    
    public Iterator<Catch> getCatches() ;
}

