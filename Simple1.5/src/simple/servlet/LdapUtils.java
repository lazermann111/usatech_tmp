/**
 *
 * Copyright 2009 USATechnologies. All rights reserved.
 *
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package simple.servlet;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimeZone;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InvalidAttributeValueException;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.servlet.steps.LoginFailureException;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;

/**
 * Provides the methods to authenticate via LDAP server.
 */
public class LdapUtils {
	private static final simple.io.Log log = simple.io.Log.getLog();

	private static final String ATTRIBUTE_USER = "sAMAccountName";
	private static final ThreadSafeDateFormat DATE_PARSER = new ThreadSafeDateFormat(new SimpleDateFormat("yyyyMMddHHmmss'Z'"));
	private static LdapType ldapType = LdapType.ACTIVE_DIRECTORY;
	private static String ldapUrl;
	private static String ldapDomain;
	private static String ldapDNBase;

	public static interface LdapAcctInfo {
		public String getFullName() ;

		public Set<String> getGroups() ;

		public Date getPasswordExpiration();

		public String getLdapName();
	}

	/* First thing, load the LDAP configurations. */
	static {
		initialize();
	}

	private static void initialize() {
		DATE_PARSER.getDelegate().setTimeZone(TimeZone.getTimeZone("GMT"));
		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		ResourceBundle bundle = ResourceBundle.getBundle("ldap_conf");
		try {
			ldapType = ConvertUtils.convert(LdapType.class, bundle.getObject("ldap.type"));
		} catch(ConvertException e) {
			log.warn("Could not convert ldap.type property to LdapType; using ACTIVE_DIRECTORY", e);
		} catch(MissingResourceException e) {
			log.warn("Could not find ldap.type property in resource bundle; using ACTIVE_DIRECTORY");
		}
		if(ldapType == LdapType.ACTIVE_DIRECTORY) {
			ldapDomain = bundle.getString("ldap.domain");
		}
		ldapUrl = bundle.getString("ldap.url");
		ldapDNBase = bundle.getString("ldap.dn.base");
	}

	public static LdapAcctInfo authenticate(String username, String password) throws ServletException {
		if(ldapType == null)
			throw new ServletException("Unsupported ldap.type configuration");

		// if username/password is not found, set error message and return.
		if(StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
			throw new LoginFailureException("Please enter your username and password and try again.", LoginFailureException.INVALID_USERNAME);
		}

		LdapContext ldapContext = bind(username, password);
		try {
			return getLdapAcctInfo(ldapContext, username);
		} finally {
			try {
				ldapContext.close();
			} catch(NamingException e) {
				log.warn("Failed to close LDAP connection: ", e);
			}
		}
	}

	public static LdapAcctInfo authenticateAndChangePassword(String username, String password, String newPassword) throws ServletException {
		if(ldapType == null)
			throw new ServletException("Unsupported ldap.type configuration");

		// if username/password is not found, set error message and return.
		if(StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(newPassword)) {
			throw new LoginFailureException("Please enter your username, old password, and new password and try again.", LoginFailureException.INVALID_USERNAME);
		}
		LdapContext ldapContext = bind(username, password);
		try {
			LdapAcctInfo lai = getLdapAcctInfo(ldapContext, username);
			changePassword(ldapContext, lai.getLdapName(), newPassword);
			return lai;
		} finally {
			try {
				ldapContext.close();
			} catch(NamingException e) {
				log.warn("Failed to close LDAP connection: ", e);
			}
		}
	}

	private static LdapContext bind(String username, String password) throws ServletException {
		Hashtable<String, Object> env = new Hashtable<String, Object>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, ldapUrl);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PROTOCOL, "ssl");

		switch(ldapType) {
			case ACTIVE_DIRECTORY:
				env.put(Context.SECURITY_PRINCIPAL, ldapDomain + "\\" + username);
				break;
			case DIRECTORY_SERVER_389:
				env.put(Context.SECURITY_PRINCIPAL, "uid=" + username + "," + ldapDNBase);
				break;
		}
		env.put(Context.SECURITY_CREDENTIALS, password);
		// env.put("com.sun.jndi.ldap.trace.ber", System.out);

		try {
			return new InitialLdapContext(env, null);
		} catch(InvalidAttributeValueException e) {
			// This can be: [LDAP: error code 19 - Exceed password retry limit. Please try later.]
			if(e.getExplanation().contains("Exceed password retry limit"))
				throw new LoginFailureException(e.getExplanation(), LoginFailureException.PASSWORD_EXPIRED);
			throw new LoginFailureException(e.getExplanation(), LoginFailureException.ACCOUNT_LOCKED); // NOTE: Not sure what this should be
		} catch(AuthenticationException e) {
			throw new LoginFailureException("Invalid username or password, authentication failed.", LoginFailureException.INVALID_CREDENTIALS);
		} catch(NamingException e) {
			log.error("****>> LDAP server unreachable.", e);
			throw new ServletException("LDAP server unreachable", e);
		}

	}

	//NOTE: this is not tested
	private static void changePassword(LdapContext context, String ldapName, String newPassword) throws ServletException {
		String name;
		Object value;
		switch(ldapType) {
			case ACTIVE_DIRECTORY:
				name = "unicodePwd";
				try {
					value = new StringBuilder(newPassword.length() + 2).append('"').append(newPassword).append('"').toString().getBytes("UTF-16LE");
				} catch(UnsupportedEncodingException e) {
					throw new ServletException(e);
				}
				break;
			default:
			case DIRECTORY_SERVER_389:
				name = "userPassword";
				value = newPassword;
				break;
		}
		try {
			context.modifyAttributes(ldapName, new ModificationItem[] { new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(name, value)) });
		} catch(NamingException e) {
			throw new ServletException(e);
		}
	}

	private static LdapAcctInfo getLdapAcctInfo(LdapContext context, String username) throws ServletException {
		try {
			/* 1. Get the Common Name of the user. */
			String returnedAtts[] = { "CN", "passwordexpirationtime" }; // , "pwdPolicySubentry" };
			username = StringUtils.encodeForLDAP(username);
			String searchFilter;
			switch(ldapType) {
				case DIRECTORY_SERVER_389:
					searchFilter = "(&(objectClass=inetorgperson)(uid=" + username + "))";
					break;
				case ACTIVE_DIRECTORY:
				default:
					searchFilter = "(&(objectClass=user)(" + ATTRIBUTE_USER + "=" + username + "))";
					break;
			}
			// Create the search controls
			SearchControls searchCtls = new SearchControls();
			searchCtls.setReturningAttributes(returnedAtts);
			// Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			NamingEnumeration<SearchResult> answer = context.search(ldapDNBase, searchFilter, searchCtls);

			// we only get the first one found.
			if(answer.hasMoreElements()) {
				SearchResult searchResult = answer.next();
				Attributes attrs = searchResult.getAttributes();

				Attribute attr = attrs.get("CN");
				final String fullName = (String) attr.get(0);
				attr = attrs.get("passwordexpirationtime");
				Date d;
				try {
					d = DATE_PARSER.parse((String) attr.get(0));
				} catch(ParseException e) {
					log.warn("Could not parse password expiration time: " + attr.get(0), e);
					d = null;
				}
				final Date expireDate = d;
				
				final String fullLdapName = searchResult.getNameInNamespace();

				/* 2. Get the groups of which the user is a direct member. */

				searchFilter = "(&(objectClass=Group)(objectCategory=Group)(member=" + fullLdapName + "))";
				switch(ldapType) {
					case DIRECTORY_SERVER_389:
						searchFilter = "(&(objectClass=groupOfUniqueNames)(uniqueMember=" + fullLdapName + "))";
						break;
					case ACTIVE_DIRECTORY:
					default:
						searchFilter = "(&(objectClass=Group)(objectCategory=Group)(member=" + fullLdapName + "))";
						break;
				}
				answer = context.search(ldapDNBase, searchFilter, searchCtls);
				Set<String> groups = new HashSet<String>();
				while(answer.hasMoreElements()) {
					searchResult = answer.next();
					attrs = searchResult.getAttributes();
					attr = attrs.get("CN");
					groups.add((String) attr.get(0));
				}
				final Set<String> groupsUnmod = Collections.unmodifiableSet(groups);
				return new LdapAcctInfo() {					
					public Set<String> getGroups() {
						return groupsUnmod;
					}				
					public String getFullName() {
						return fullName;
					}

					public Date getPasswordExpiration() {
						return expireDate;
					}

					public String getLdapName() {
						return fullLdapName;
					}
				};
			} else {
				return new LdapAcctInfo() {					
					public Set<String> getGroups() {
						return Collections.emptySet();
					}				
					public String getFullName() {
						return null;
					}

					public Date getPasswordExpiration() {
						return null;
					}

					public String getLdapName() {
						return null;
					}
				};
			}
		} catch(NamingException e) {
			log.error("****>> Error communicating with LDAP server", e);
			throw new ServletException("Error communicating with LDAP server", e);
		}
	}
}
