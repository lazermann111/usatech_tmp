/*
 * Created on Jul 25, 2005
 *
 */
package simple.servlet;

public class StepConfigException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 53393827511L;

	public StepConfigException() {
        super();
    }

    public StepConfigException(String message) {
        super(message);
    }

    public StepConfigException(String message, Throwable cause) {
        super(message, cause);
    }

    public StepConfigException(Throwable cause) {
        super(cause);
    }

}
