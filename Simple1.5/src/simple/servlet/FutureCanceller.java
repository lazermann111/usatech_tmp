/*
 * Created on Feb 24, 2006
 *
 */
package simple.servlet;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import simple.io.Log;

/**
 * @author bkrug
 *
 */
public class FutureCanceller implements HttpSessionBindingListener {
    private static final Log log = Log.getLog();
    protected Map<Integer,Future<?>> futures = new HashMap<Integer,Future<?>>();
    protected AtomicInteger idGen = new AtomicInteger(0);
    /**
     * 
     */
    public FutureCanceller() {
        super();
    }

    /**
     * @see javax.servlet.http.HttpSessionBindingListener#valueBound(javax.servlet.http.HttpSessionBindingEvent)
     */
    public void valueBound(HttpSessionBindingEvent event) {
        // do nothing
    }

    /**
     * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(javax.servlet.http.HttpSessionBindingEvent)
     */
    public void valueUnbound(HttpSessionBindingEvent event) {
        log.debug("Canceller being removed from session");
        cancelAll();
    }
    /**
     * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(javax.servlet.http.HttpSessionBindingEvent)
     */
    public int cancelAll() {
        log.debug("Cancelling " + futures.size() + " futures of " + toString());
        // cancel all futures
        int cnt = 0;
        for(Iterator<Future<?>> iter = futures.values().iterator(); iter.hasNext(); ) {
            if(iter.next().cancel(true)) cnt++;
            iter.remove();
        }
        return cnt;
    }
    
    public boolean cancel(int id) {
        log.debug("Cancelling future " + id + " of " + toString());
        // cancel specified future
        Future<?> f = futures.remove(id);
        return (f != null && f.cancel(true));
    }
    public int addFuture(Future<?> f) {
        int id = idGen.incrementAndGet();
        futures.put(id, f);
        log.debug("Future " + f + " added to Canceller with id = " + id);
        return id;
    }
    public void removeFuture(Future<?> f) {
        futures.values().remove(f);
        log.debug("Future " + f + " removed from Canceller");
    }
    public void removeFuture(int id) {
        Future<?> f = futures.remove(id);
        log.debug("Future " + f + " removed from Canceller");
    }
}
