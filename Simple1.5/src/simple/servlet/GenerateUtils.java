package simple.servlet;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.Log;
import simple.lang.Holder;
import simple.lang.Initializer;
import simple.lang.SystemUtils;
import simple.results.Results;
import simple.text.RegexUtils;
import simple.text.StringUtils;
import simple.translator.Translator;

public class GenerateUtils {
	public static class Country {
		public final String code;
		public final String name;
		public final String postalRegex;
		public final String postalMask;
		public final int postalPrimaryParts;

		public Country(String code, String name, String postalRegex, String postalMask, int postalPrimaryParts) {
			super();
			this.code = code;
			this.name = name;
			this.postalRegex = postalRegex;
			this.postalMask = postalMask;
			this.postalPrimaryParts = postalPrimaryParts;
		}
	}
	
	public static class CountryStore {
		protected final Map<String, Country> countries = new LinkedHashMap<String, Country>();
		protected final Collection<Country> countriesUnmod = Collections.unmodifiableCollection(countries.values());
		protected final Initializer<SQLException> countryInitializer = new Initializer<SQLException>() {
			@Override
			protected void doInitialize() throws SQLException {
				Results results;
				try {
					results = DataLayerMgr.executeQuery("GET_COUNTRIES", null);
				} catch(DataLayerException e) {
					throw new SQLException(e);
				}
				while(results.next()) {
					Country country = createCountry(results);
					countries.put(country.code, country);
				}
			}

			@Override
			protected void doReset() {
				resetCountries();
			}
		};

		protected Country createCountry(Results results) throws SQLException {
			try {
				return new Country(
					results.getValue("code", String.class),
					results.getValue("name", String.class),
					results.getValue("postalRegex", String.class),
					results.getValue("postalMask", String.class), 
					results.getValue("postalPrimaryParts", int.class));
			} catch(ConvertException e) {
				throw new SQLException(e);
			}
		}

		protected void resetCountries() {
			countries.clear();

		}

		public Country getCountry(String countryCd) throws SQLException {
			try {
				countryInitializer.initialize();
			} catch(InterruptedException e) {
				throw new SQLException(e);
			}
			return countries.get(countryCd);
		}

		public Collection<Country> getCountries() throws SQLException {
			try {
				countryInitializer.initialize();
			} catch(InterruptedException e) {
				throw new SQLException(e);
			}
			return countriesUnmod;
		}
	}

	protected static String postalLookupUrl;
	protected static CountryStore countryStore;

	public static Country getCountry(String countryCd) throws SQLException {
		return getCountryStore().getCountry(countryCd);
	}

	public static Collection<Country> getCountries() throws SQLException {
		return getCountryStore().getCountries();
	}

	public static String getCountriesConfigScript(Translator translator) throws SQLException {
		StringBuilder sb = new StringBuilder();
		try {
			writeCountriesConfigScript(translator, sb, null, SystemUtils.getNewLine());
		} catch(IOException e) {
			// StringBuilder does not throw IOExceptions
		}
		return sb.toString();
	}

	public static void writeCountriesConfigScript(Translator translator, Appendable out, String linePrefix, String lineSuffix) throws SQLException, IOException {
		for(Country country : getCountries()) {
			if(linePrefix != null && linePrefix.length() > 0)
				out.append(linePrefix);
			out.append("Form.Validator.addPostalValidator('").append(country.code).append("', '");
			String instructs = translator.translate("prompt-postal-" + country.code.toLowerCase(), "Please enter a valid postal code");
			if(instructs != null)
				out.append(StringUtils.prepareScript(instructs));
			out.append("', '").append(StringUtils.prepareScript(country.postalRegex)).append("', '").append(StringUtils.prepareScript(country.postalMask)).append("');");
			if(lineSuffix != null && lineSuffix.length() > 0)
				out.append(lineSuffix);
		}
	}

	protected static String applicationVersion;
	public static String getApplicationVersion() {
		if(applicationVersion == null) {
			String av = SystemUtils.getApplicationVersionSafely();
			if(!StringUtils.isBlank(av))
				applicationVersion = av;
			else {
				av = GenerateUtils.class.getPackage().getSpecificationVersion();
				if(!StringUtils.isBlank(av))
					applicationVersion = av;
				try {
					applicationVersion = SystemUtils.nvl(IOUtils.readFully(ConfigSource.createConfigSource("app-version").getInputStream()), "UNKNOWN").trim();
				} catch(FileNotFoundException e) {
					Log.getLog().info("Could not find version.txt in '" + new File(".").getAbsolutePath() + "'");
					applicationVersion = "UNKNOWN";
				} catch(IOException e) {
					applicationVersion = "UNKNOWN";
				}
			}
		}
		return applicationVersion;
	}

	public static int getCurrentYear() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}

	public static String getPrimaryPostal(String countryCd, String postal) throws SQLException {
		if(postal == null || (postal = postal.trim()).length() == 0)
			return null;
		// XXX: should we do: postal = postal.toUpperCase(); ?
		GenerateUtils.Country country = GenerateUtils.getCountry(countryCd);
		if(country != null) {
			String[] matches = RegexUtils.match(country.postalRegex, postal, null);
			if(matches != null && matches.length > 1)
				postal = StringUtils.join(matches, "", 1, country.postalPrimaryParts);
		}
		return postal;
	}
	public static String formatPostal(String countryCd, String postal) throws SQLException {
		if(postal == null || (postal = postal.trim()).length() == 0)
			return null;
		// XXX: should we do: postal = postal.toUpperCase(); ?
		GenerateUtils.Country country = GenerateUtils.getCountry(countryCd);
		if(country != null) {
			String[] matches = RegexUtils.match(country.postalRegex, postal, null);
			if(matches != null && matches.length > 1)
				postal = StringUtils.join(matches, "", 1, matches.length - 1);
		}
		return postal;
	}

	public static boolean checkPostal(String countryCd, String postal, boolean required, Holder<String> formattedPostal) throws SQLException {
		if(postal == null || (postal = postal.trim()).length() == 0)
			return !required;
		// XXX: should we do: postal = postal.toUpperCase(); ?
		GenerateUtils.Country country = GenerateUtils.getCountry(countryCd);
		if(country == null)
			return false;
		String[] matches = RegexUtils.match(country.postalRegex, postal, null);
		if(matches == null)
			return false;

		switch(matches.length) {
			case 0: // shouldn't happen
				return false;
			case 1: // matches but no groups
				break;
			default:
				postal = StringUtils.join(matches, "", 1, matches.length - 1);
		}

		if(formattedPostal != null)
			formattedPostal.setValue(postal);

		return true;
	}

	public static String formatPostal(String prefix, InputForm form) throws SQLException, ServletException, ConvertException {
		if(prefix == null)
			prefix = "";
		String postal = RequestUtils.getAttribute(form.getRequest(), prefix + "postal", String.class, true);
		String countryCd = RequestUtils.getAttribute(form.getRequest(), prefix + "country", String.class, true);
		postal = formatPostal(countryCd, postal);
		form.setAttribute(prefix + "postal", postal);
		return postal;
	}

	public static void writePostalTableElement(PageContext pageContext, String prefix, String postal, String city, String state, String country, boolean verified, Collection<Country> countries) throws JspException {
		writePostalTableElement(pageContext, prefix, postal, city, state, country, verified, countries, null, true);
	}
	
	public static void writePostalTableElement(PageContext pageContext, String prefix, String postal, String city, String state, String country, boolean verified, Collection<Country> countries, String onselectFunction, boolean showHelp) throws JspException {
		writePostalTableElement(pageContext, prefix, postal, city, state, country, verified, countries, onselectFunction, true, true);
	}
	public static void writePostalTableElement(PageContext pageContext, String prefix, String postal, String city, String state, String country, boolean verified, Collection<Country> countries, String onselectFunction, boolean showHelp, boolean showCountry) throws JspException {
		if(prefix != null && prefix.trim().length() == 0)
			prefix = null;
		JspWriter writer = pageContext.getOut();
		try {
			writer.print("<table id=\"");
			if(prefix != null)
				writer.print(prefix);
			writer.print("postalLookup\" class=\"addressTable");
			if(!StringUtils.isBlank(postal)) {
				if(verified)
					writer.print(" postal-found");
				else
					writer.print(" postal-not-found");
			}
			writer.print("\"><tr><td><input class=\"addressCity\" type=\"text\" name=\"");
			if(prefix != null)
				writer.print(prefix);
			writer.print("city\" data-validators=\"required\" value=\"");
			writer.print(StringUtils.prepareCDATA(city));
			writer.print("\"");
			if(verified || StringUtils.isBlank(postal))
				writer.print(" readonly=\"readonly\"");
			writer.print("/></td>");
			writer.println();
			writer.print("<td>,&#160;<input class=\"addressState\" type=\"text\" name=\"");
			if(prefix != null)
				writer.print(prefix);
			writer.print("state\" data-validators=\"required\" value=\"");
			writer.print(StringUtils.prepareCDATA(state));
			writer.print("\"");
			if(verified || StringUtils.isBlank(postal))
				writer.print(" readonly=\"readonly\"");
			writer.print("/></td>");
			writer.println();
			
			writer.print("<td><input class=\"addressPostal\" type=\"text\" name=\"");
			if(prefix != null)
				writer.print(prefix);
			if(country == null || (country = country.trim()).length() == 0 || GenerateUtils.getCountry(country) == null)
				country = countries.iterator().next().code;
			writer.print("postal\" edit-mask=\"Fixed.Postal");
			writer.print(StringUtils.encodeForHTML(country.toUpperCase()));
			writer.print("\" data-validators=\"required validate-postalcode-lookup-");
			writer.print(StringUtils.encodeForHTML(country.toLowerCase()));
			writer.print("\" value=\"");
			writer.print(StringUtils.prepareCDATA(postal));
			writer.print('"');
			if(!StringUtils.isBlank(prefix) || !StringUtils.isBlank(getPostalLookupUrl())) {
				writer.print(" data-validator-properties=\"{");
				if(!StringUtils.isBlank(getPostalLookupUrl())) {
					writer.print(" url: '");
					writer.print(StringUtils.prepareScript(getPostalLookupUrl()));
					writer.print('\'');
					if(!StringUtils.isBlank(prefix))
						writer.print(',');
				}				
				if(!StringUtils.isBlank(prefix)) {
					writer.print(" fieldPrefix: '");
					writer.print(StringUtils.prepareScript(prefix));
					writer.print('\'');
				}
				writer.print("}\"");			
			}
			writer.print("/>");
			writer.print("</td>");
			writer.println();
			if(showCountry){
			writer.print("<td><select name=\"");
			if(prefix != null)
				writer.print(prefix);
			writer.print("country\" data-validators=\"required\">");
			writer.println();
			for(Country c : countries) {
				writer.print("<option value=\"");
				writer.print(c.code);
				writer.print("\" onselect=\"App.updateValidation($(this).getParent().form.");
				if(prefix != null)
					writer.print(prefix);
				writer.print("postal, 'required validate-postalcode-lookup-");
				writer.print(c.code.toLowerCase());
				writer.print("', 'Fixed.Postal");
				writer.print(c.code.toUpperCase());
				writer.print("');");
				if(onselectFunction != null && (onselectFunction = onselectFunction.trim()).length() > 0) {
					writer.print(' ');
					writer.print(StringUtils.prepareScript(onselectFunction));
					writer.print("($(this));");
				}
				writer.print('"');
				if(c.code.equalsIgnoreCase(country))
					writer.print(" selected=\"selected\"");
				writer.print(">");
				writer.print(c.name);
				writer.print("</option>");
				writer.println();
			}
			writer.print("</select>");
			if(showHelp) {
				writer.print("<span class=\"help-button\"><a onclick=\"postalInstructsDialog.show();\">&#160;</a></span>");
				writer.print("<script type=\"text/javascript\">var postalInstructsDialog = new Dialog.Help({ helpKey: 'postal-lookup-instructions', destroyOnClose: false });</script>");
			} 
			writer.print("</td>");
			}
			writer.print("</tr></table>");
		} catch(IOException e) {
			throw new JspException(e);
		} catch(SQLException e) {
			throw new JspException(e);
		}
	}
	
	public static void writeCountryTdElement(PageContext pageContext, String prefix,String country, Collection<Country> countries, String onselectFunction) throws JspException {
		JspWriter writer = pageContext.getOut();
		try {
			
			writer.print("<td><select name=\"");
			if(prefix != null)
				writer.print(prefix);
			writer.print("country\" data-validators=\"required\">");
			writer.println();
			for(Country c : countries) {
				writer.print("<option value=\"");
				writer.print(c.code);
				writer.print("\" onselect=\"App.updateValidation($(this).getParent().form.");
				if(prefix != null)
					writer.print(prefix);
				writer.print("postal, 'required validate-postalcode-lookup-");
				writer.print(c.code.toLowerCase());
				writer.print("', 'Fixed.Postal");
				writer.print(c.code.toUpperCase());
				writer.print("');");
				if(onselectFunction != null && (onselectFunction = onselectFunction.trim()).length() > 0) {
					writer.print(' ');
					writer.print(StringUtils.prepareScript(onselectFunction));
					writer.print("($(this));");
				}
				writer.print('"');
				if(c.code.equalsIgnoreCase(country))
					writer.print(" selected=\"selected\"");
				writer.print(">");
				writer.print(c.name);
				writer.print("</option>");
				writer.println();
			}
			writer.print("</select>");
			writer.print("</td>");
		} catch(IOException e) {
			throw new JspException(e);
		} 
	}

	public static String getPostalLookupUrl() {
		return postalLookupUrl;
	}

	public static void setPostalLookupUrl(String postalLookupUrl) {
		GenerateUtils.postalLookupUrl = postalLookupUrl;
	}

	public static CountryStore getCountryStore() {
		if(countryStore == null)
			countryStore = new CountryStore();
		return countryStore;
	}

	public static void setCountryStore(CountryStore countryStore) {
		GenerateUtils.countryStore = countryStore;
	}

	public static void writeSortedTableHeader(PageContext pageContext, String fieldName, String label, String[] sortBy) throws JspException {
		writeSortedTableHeader(pageContext, fieldName, label, sortBy, false);
	}

	public static void writeSortedTableHeader(PageContext pageContext, String fieldName, String label, String[] sortBy, boolean replaceSpaces) throws JspException {
		int sort;
		if(sortBy == null || sortBy.length == 0 || StringUtils.isBlank(sortBy[0]))
			sort = 0;
		else if(sortBy[0].charAt(0) == '-' && fieldName.length() == sortBy[0].length() - 1 && fieldName.regionMatches(0, sortBy[0], 1, fieldName.length()))
			sort = -1;
		else if(fieldName.equals(sortBy[0]))
			sort = 1;
		else
			sort = 0;
		JspWriter writer = pageContext.getOut();
		try {
			writer.print("<th");
			switch(sort) {
				case 1:
					writer.print(" class=\"sortASC\"");
					break;
				case -1:
					writer.print(" class=\"sortDESC\"");
					break;
			}
			writer.print("><button class=\"sortable\" name=\"sortBy\" value=\"");
			if(sort == 1)
				writer.print('-');
			writer.print(fieldName);
			writer.print('"');
			// writer.print(" onclick=\"$(this.form).getChildren('input[name=sortBy][type=hidden]').set('disabled',true);\">");
			writer.print('>');
			writer.print(replaceSpaces && label != null ? label.replace(" ", "&nbsp;") : label);
			writer.print("</button></th>");
		} catch(IOException e) {
			throw new JspException(e);
		}
	}
}
