package simple.servlet;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.text.StringUtils;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

public class RequestInfoUtils {
	protected static class LocalFileCache extends LockSegmentCache<String, ConfigSource, RuntimeException> {
		@Override
		protected ConfigSource createValue(String key, Object... additionalInfo) {
			return ConfigSource.createConfigSource(new File(key));
		}

		@Override
		protected boolean keyEquals(String key1, String key2) {
			return key1.equals(key2);
		}

		@Override
		protected boolean valueEquals(ConfigSource value1, ConfigSource value2) {
			return value1.equals(value2);
		}
	}

	protected static final LocalFileCache defaultLocalFileCache = new LocalFileCache();
	protected static final Pattern uriMatchPattern = Pattern.compile("(?:(?:(?:\\w+\\:)?//([^/:]*)(?:\\:\\d+)?)?(/)?)?(.*)"); // groups: 1=server name; 2=/ or empty; 3=path
	protected static final char[] DIRECTORY_SEPARATORS = new char[] { '/', '\\' };

	protected static void appendDirectory(StringBuilder appendTo, String path) {
		int pos = StringUtils.lastIndexOf(path, DIRECTORY_SEPARATORS);
		if(pos >= 0)
			appendTo.append(path, 0, pos + 1);
		else
			appendTo.append('/');
	}

	public static String addLastModifiedToUri(RequestInfo requestInfo, String path) {
		return addLastModifiedToUri(requestInfo, path, defaultLocalFileCache);
	}

	public static String addLastModifiedToUri(RequestInfo requestInfo, String path, Cache<String, ConfigSource, RuntimeException> fileCache) {
		Matcher matcher = uriMatchPattern.matcher(path);
		StringBuilder localPath = new StringBuilder();
		if(matcher.matches()) {
			String pathServerName = matcher.group(1);
			boolean pathRelative = (matcher.group(2) == null);
			String pathPath = matcher.group(3);
			if(pathServerName == null || pathServerName.length() == 0) {
				// check base
				String base = requestInfo.getBaseUrl();
				if(base != null) {
					matcher = uriMatchPattern.matcher(base);
					if(!matcher.matches())
						return path;
					String baseServerName = matcher.group(1);
					boolean baseRelative = (matcher.group(2) == null);
					// String basePath = matcher.group(3);
					if(baseServerName != null && baseServerName.length() != 0 && !baseServerName.equalsIgnoreCase(requestInfo.getServerName())) {
						return path;
					}
					if(pathRelative) {
						if(baseRelative) {
							localPath.append(requestInfo.getContextPath());
							if(requestInfo.getPathInfo() != null) {
								appendDirectory(localPath, requestInfo.getPathInfo());
							} else {
								localPath.append('/');
							}
						} else {
							localPath.append('/');
						}
						// appendDirectory(localPath, basePath);
					}
				} else if(pathRelative) {
					localPath.append(requestInfo.getContextPath());
					if(requestInfo.getPathInfo() != null) {
						appendDirectory(localPath, requestInfo.getPathInfo());
					} else {
						localPath.append('/');
					}
				} else {
					localPath.append('/');
				}
			} else if(pathServerName.equalsIgnoreCase(requestInfo.getServerName())) {
				localPath.append('/');
			} else {
				return path;
			}
			localPath.append(pathPath);
			String realPath = requestInfo.getRealPath(localPath.toString());
			if(realPath == null)
				return path;
			return IOUtils.addLastModifiedToFile(path, realPath, fileCache == null ? defaultLocalFileCache : fileCache);
		} else {
			return path;
		}
	}

	protected final static char[] contentTypeSeparators = "/; ".toCharArray();
	protected final static Pattern contentTypePattern = Pattern.compile("([^;/]*)/([^;/]*)(?:[;/](.*))?");
	protected static final Set<String> htmlContentTypes = new HashSet<String>();
	protected final static String LEGACY_HTML_CONTENT_TYPE = "text/html";

	static {
		htmlContentTypes.add(LEGACY_HTML_CONTENT_TYPE);
		htmlContentTypes.add("text/xhtml");
		htmlContentTypes.add("application/xhtml+xml");
	}

	public static String getContentTypeForLegacyBrowsers(String contentType) {
		if(htmlContentTypes.contains(contentType))
			return LEGACY_HTML_CONTENT_TYPE;
		return contentType;
	}

	public static boolean isDirectlyRendered(String contentType) {
		if(contentType == null)
			return true;
		Matcher matcher = contentTypePattern.matcher(contentType);
		if(!matcher.matches())
			return false;
		if("text".equals(matcher.group(1)) && !"csv".equalsIgnoreCase(matcher.group(2)))
				return true;
		return htmlContentTypes.contains(matcher.group(1) + "/" + matcher.group(2));
	}
}
