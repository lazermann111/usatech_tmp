/*
 * Interceptor.java
 *
 * Created on January 27, 2003, 3:46 PM
 */

package simple.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.servlet.http.HttpSessionContext;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

/** Uses ServletRequestWrapper and ServletResponseWrapper to intercept a response
 * and write it instead to the specified OutputStream
 * @author Brian S. Krug
 */
@SuppressWarnings("deprecation")
public class Interceptor {
	protected static class InterceptingSession implements HttpSession {
		protected final Map<String, Object> attributes = new LinkedHashMap<String, Object>();
		protected final ServletContext context;
		protected final long creationTime = System.currentTimeMillis();

		public InterceptingSession(ServletContext context) {
			this.context = context;
		}
		@Override
		public long getCreationTime() {
			return creationTime;
		}

		@Override
		public String getId() {
			return null;
		}

		@Override
		public long getLastAccessedTime() {
			return 0;
		}

		@Override
		public ServletContext getServletContext() {
			return context;
		}

		@Override
		public void setMaxInactiveInterval(int interval) {
		}

		@Override
		public int getMaxInactiveInterval() {
			return 0;
		}

		@Override
		@Deprecated
		public HttpSessionContext getSessionContext() {
			return null;
		}

		@Override
		public Object getAttribute(String name) {
			return attributes.get(name);
		}

		@Override
		public Object getValue(String name) {
			return getAttribute(name);
		}

		@Override
		public Enumeration<String> getAttributeNames() {
			return Collections.enumeration(attributes.keySet());
		}

		@Override
		public String[] getValueNames() {
			return attributes.keySet().toArray(new String[attributes.size()]);
		}

		@Override
		public void setAttribute(String name, Object value) {
			Object old;
			if(value == null)
				old = attributes.remove(name);
			else
				old = attributes.put(name, value);
			if(old instanceof HttpSessionBindingListener)
				((HttpSessionBindingListener) old).valueUnbound(new HttpSessionBindingEvent(this, name, old));
			if(value instanceof HttpSessionBindingListener)
				((HttpSessionBindingListener) value).valueBound(new HttpSessionBindingEvent(this, name, value));
		}

		@Override
		public void putValue(String name, Object value) {
			setAttribute(name, value);
		}

		@Override
		public void removeAttribute(String name) {
			setAttribute(name, null);
		}

		@Override
		public void removeValue(String name) {
			removeAttribute(name);
		}

		@Override
		public void invalidate() {
			attributes.clear();
		}

		@Override
		public boolean isNew() {
			return true;
		}
	}
	protected static class InterceptingRequest implements HttpServletRequest {
		protected final Map<String, Object> attributes = new LinkedHashMap<String, Object>();
		protected final Map<String, String[]> parameters;
		protected final Map<String, Object> headers = new LinkedHashMap<String, Object>();
		protected String encoding;
		protected final Locale locale;
		protected final String path; // needs to start with /
		protected final RequestDispatcherFactory requestDispatcherFactory;
		protected final ServletContext context;
		protected HttpSession session;

		public InterceptingRequest(ServletContext context, String path, Locale locale, Map<String, String[]> parameters, RequestDispatcherFactory requestDispatcherFactory) {
			this.path = relativize("/", path); // should this be "relativize(context.getContextPath(), path);" ???
			this.locale = locale;
			if(parameters == null)
				this.parameters = Collections.emptyMap();
			else
				this.parameters = Collections.unmodifiableMap(parameters);
			this.requestDispatcherFactory = requestDispatcherFactory;
			this.context = context;
		}

		@Override
		public Object getAttribute(String name) {
			return attributes.get(name);
		}

		@Override
		public Enumeration<String> getAttributeNames() {
			return Collections.enumeration(attributes.keySet());
		}

		@Override
		public String getCharacterEncoding() {
			return encoding;
		}

		@Override
		public void setCharacterEncoding(String env) throws UnsupportedEncodingException {
			this.encoding = env;
		}

		@Override
		public int getContentLength() {
			return -1;
		}

		@Override
		public String getContentType() {
			return null;
		}

		@Override
		public ServletInputStream getInputStream() throws IOException {
			return null;
		}

		@Override
		public String getParameter(String name) {
			String[] values = getParameterValues(name);
			return values == null || values.length == 0 ? null : values[0];
		}

		@Override
		public Enumeration<String> getParameterNames() {
			return Collections.enumeration(parameters.keySet());
		}

		@Override
		public String[] getParameterValues(String name) {
			return parameters.get(name);
		}

		@Override
		public Map<String, String[]> getParameterMap() {
			return parameters;
		}

		@Override
		public String getProtocol() {
			return null;
		}

		@Override
		public String getScheme() {
			return null;
		}

		@Override
		public String getServerName() {
			return null;
		}

		@Override
		public int getServerPort() {
			return 0;
		}

		@Override
		public BufferedReader getReader() throws IOException {
			return null;
		}

		@Override
		public String getRemoteAddr() {
			return null;
		}

		@Override
		public String getRemoteHost() {
			return null;
		}

		@Override
		public void setAttribute(String name, Object value) {
			if(value == null)
				attributes.remove(name);
			else
				attributes.put(name, value);
		}

		public void setAttributes(Map<? extends String, ? extends Object> attributes) {
			this.attributes.putAll(attributes);
		}

		@Override
		public void removeAttribute(String name) {
			attributes.remove(name);
		}

		@Override
		public Locale getLocale() {
			return locale;
		}

		@Override
		public Enumeration<Locale> getLocales() {
			return Collections.enumeration(Collections.singleton(locale));
		}

		@Override
		public boolean isSecure() {
			return true;
		}

		@Override
		public RequestDispatcher getRequestDispatcher(String path) {
			String adjustedPath = relativize(this.path, path);
			return requestDispatcherFactory == null ? null : requestDispatcherFactory.getRequestDispatcher(adjustedPath);
		}

		protected String relativize(String base, String child) {
			if(StringUtils.isBlank(child) || child.trim().equals("."))
				return base;
			if(child.startsWith("/"))
				return child; // it's absolute
			int pos = base.lastIndexOf('/');
			if(pos < 0)
				return child;
			return base.substring(0, pos + 1) + child;
		}
		@Override
		public String getRealPath(String path) {
			return getServletContext().getRealPath(path);
		}

		@Override
		public int getRemotePort() {
			return 0;
		}

		@Override
		public String getLocalName() {
			return null;
		}

		@Override
		public String getLocalAddr() {
			return null;
		}

		@Override
		public int getLocalPort() {
			return 0;
		}

		@Override
		public ServletContext getServletContext() {
			return null;
		}

		@Override
		public AsyncContext startAsync() throws IllegalStateException {
			return null;
		}

		@Override
		public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException {
			return null;
		}

		@Override
		public boolean isAsyncStarted() {
			return false;
		}

		@Override
		public boolean isAsyncSupported() {
			return false;
		}

		@Override
		public AsyncContext getAsyncContext() {
			return null;
		}

		@Override
		public DispatcherType getDispatcherType() {
			return DispatcherType.REQUEST;
		}

		@Override
		public String getAuthType() {
			return null;
		}

		@Override
		public Cookie[] getCookies() {
			return null;
		}

		@Override
		public long getDateHeader(String name) {
			try {
				return ConvertUtils.getLong(headers.get(name), -1);
			} catch(ConvertException e) {
				throw new IllegalArgumentException(e);
			}
		}

		@Override
		public String getHeader(String name) {
			return ConvertUtils.getStringSafely(headers.get(name));
		}

		@Override
		public Enumeration<String> getHeaders(String name) {
			String value = getHeader(name);
			Enumeration<String> en;
			if(value == null)
				en = CollectionUtils.emptyEnumeration();
			else
				en = Collections.enumeration(Collections.singleton(value));
			return en;
		}

		@Override
		public Enumeration<String> getHeaderNames() {
			return Collections.enumeration(headers.keySet());
		}

		@Override
		public int getIntHeader(String name) {
			try {
				return ConvertUtils.getInt(headers.get(name), -1);
			} catch(ConvertException e) {
				throw new IllegalArgumentException(e);
			}
		}

		@Override
		public String getMethod() {
			return "POST";
		}

		@Override
		public String getPathInfo() {
			return path;
		}

		@Override
		public String getPathTranslated() {
			return path;
		}

		@Override
		public String getContextPath() {
			return "";
		}

		@Override
		public String getQueryString() {
			return null;
		}

		@Override
		public String getRemoteUser() {
			return null;
		}

		@Override
		public boolean isUserInRole(String role) {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getRequestedSessionId() {
			return null;
		}

		@Override
		public String getRequestURI() {
			return path;
		}

		@Override
		public StringBuffer getRequestURL() {
			return new StringBuffer(path);
		}

		@Override
		public String getServletPath() {
			return "";
		}

		@Override
		public HttpSession getSession(boolean create) {
			if(create && session == null)
				session = new InterceptingSession(context);
			return session;
		}

		@Override
		public HttpSession getSession() {
			return getSession(true);
		}

		@Override
		public boolean isRequestedSessionIdValid() {
			return false;
		}

		@Override
		public boolean isRequestedSessionIdFromCookie() {
			return false;
		}

		@Override
		public boolean isRequestedSessionIdFromURL() {
			return false;
		}

		@Override
		public boolean isRequestedSessionIdFromUrl() {
			return false;
		}

		@Override
		public boolean authenticate(HttpServletResponse response) throws IOException, ServletException {
			return false;
		}

		@Override
		public void login(String username, String password) throws ServletException {
		}

		@Override
		public void logout() throws ServletException {
		}

		@Override
		public Collection<Part> getParts() throws IOException, ServletException {
			return Collections.emptySet();
		}

		@Override
		public Part getPart(String name) throws IOException, ServletException {
			return null;
		}

		@Override
		public long getContentLengthLong() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public String changeSessionId() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <T extends HttpUpgradeHandler> T upgrade(Class<T> arg0) throws IOException, ServletException {
			// TODO Auto-generated method stub
			return null;
		}
	}
	protected static class InterceptingResponse implements HttpServletResponse {
		protected final HttpServletRequest request;
		protected String contentType;
		protected int status = 0;
		protected final PrintWriter pw;
		protected final ServletOutputStream sos;
		protected Locale locale;
		protected final Map<String, Object> headers = new LinkedHashMap<String, Object>();

		public InterceptingResponse(HttpServletRequest request, final OutputStream out) {
			this.request = request;
			this.locale = request.getLocale();
			pw = new PrintWriter(out);
			sos = new ServletOutputStream() {
				public void write(int b) throws IOException {
					out.write(b);
				}

				@Override
				public void flush() throws IOException {
					out.flush();
				}

				@Override
				public boolean isReady() {
					// TODO Auto-generated method stub
					return false;
				}

				@Override
				public void setWriteListener(WriteListener arg0) {
					// TODO Auto-generated method stub
					
				}
			};
		}

		public InterceptingResponse(HttpServletRequest request, final Writer out) {
			this.request = request;
			this.locale = request.getLocale();
			pw = (out instanceof PrintWriter ? (PrintWriter) out : new PrintWriter(out));
			sos = new ServletOutputStream() {
				public void write(int b) throws IOException {
					out.write(b);
				}

				@Override
				public void flush() throws IOException {
					out.flush();
				}

				@Override
				public boolean isReady() {
					// TODO Auto-generated method stub
					return false;
				}

				@Override
				public void setWriteListener(WriteListener arg0) {
					// TODO Auto-generated method stub
					
				}
			};
		}

		@Override
		public String getCharacterEncoding() {
			return request.getCharacterEncoding();
		}

		@Override
		public String getContentType() {
			return contentType;
		}

		@Override
		public ServletOutputStream getOutputStream() throws IOException {
			return sos;
		}

		@Override
		public PrintWriter getWriter() throws IOException {
			return pw;
		}

		@Override
		public void setCharacterEncoding(String charset) {
			try {
				request.setCharacterEncoding(charset);
			} catch(UnsupportedEncodingException e) {
				throw new UndeclaredThrowableException(e);
			}
		}

		@Override
		public void setContentLength(int len) {
			throw new UnsupportedOperationException("Use writer or outputStream to add content");
		}

		@Override
		public void setContentType(String type) {
			this.contentType = type;
		}

		@Override
		public void setBufferSize(int size) {
			// ignore
		}

		@Override
		public int getBufferSize() {
			return 0;
		}

		@Override
		public void flushBuffer() throws IOException {
			commit();
		}

		@Override
		public void resetBuffer() {
			// does nothing
		}

		@Override
		public boolean isCommitted() {
			return true;
		}

		@Override
		public void reset() {
			// do nothing
		}

		@Override
		public void setLocale(Locale loc) {
			this.locale = loc;
		}

		@Override
		public Locale getLocale() {
			return locale;
		}

		@Override
		public void addCookie(Cookie cookie) {
		}

		@Override
		public boolean containsHeader(String name) {
			return headers.containsKey(name);
		}

		@Override
		public String encodeURL(String url) {
			try {
				return URLEncoder.encode(url, getCharacterEncoding());
			} catch(UnsupportedEncodingException e) {
				return URLEncoder.encode(url);
			}
		}

		@Override
		public String encodeRedirectURL(String url) {
			return encodeURL(url);
		}

		@Override
		public String encodeUrl(String url) {
			return encodeURL(url);
		}

		@Override
		public String encodeRedirectUrl(String url) {
			return encodeURL(url);
		}

		@Override
		public void addDateHeader(String name, long value) {
			headers.put(name, value);
		}

		@Override
		public void addHeader(String name, String value) {
			headers.put(name, value);
		}

		@Override
		public void setIntHeader(String name, int value) {
			headers.put(name, value);
		}

		@Override
		public void addIntHeader(String name, int value) {
			headers.put(name, value);
		}

		@Override
		public String getHeader(String name) {
			return ConvertUtils.getStringSafely(headers.get(name));
		}

		@Override
		public Collection<String> getHeaderNames() {
			return headers.keySet();
		}

		@Override
		public Collection<String> getHeaders(String name) {
			return headers.keySet();
		}

		@Override
		public int getStatus() {
			return status;
		}

		@Override
		public void sendError(int sc, String msg) throws IOException {
			setStatus(sc, msg);
			commit();
		}

		@Override
		public void sendError(int sc) throws IOException {
			setStatus(sc);
		}

		@Override
		public void sendRedirect(String location) throws IOException {
			try {
				request.getRequestDispatcher(location).forward(request, this);
			} catch(ServletException e) {
				throw new IOException(e);
			}
		}

		@Override
		public void setDateHeader(String name, long value) {
			headers.put(name, value);
		}

		@Override
		public void setHeader(String name, String value) {
			headers.put(name, value);
		}

		@Override
		public void setStatus(int sc) {
			this.status = sc;
		}

		@Override
		public void setStatus(int sc, String sm) {
			setStatus(sc);
		}

		protected void commit() {
			pw.flush();
		}

		@Override
		public void setContentLengthLong(long arg0) {
			// TODO Auto-generated method stub
			
		}
	}

    /** Never Create new Interceptor */
    private Interceptor() {
    }

    /** Intercepts the output from the specified RequestDispatcher and writes it instead to the specified OutputStream
     * @param rd The RequestDispatch that will produce the output
     * @param request The request object
     * @param response The response object
     * @param out The OutputStream into which the output will be written
     * @throws ServletException If an exception occurs
     */    
    public static void intercept(RequestDispatcher rd, HttpServletRequest request, HttpServletResponse response, final OutputStream out) throws ServletException {
        final PrintWriter pw = new PrintWriter(out);
        final ServletOutputStream sos = new ServletOutputStream() {
            public void write(int b)  throws IOException {
                out.write(b);
            }

			@Override
			public boolean isReady() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setWriteListener(WriteListener arg0) {
				// TODO Auto-generated method stub
				
			}
        };         
        HttpServletRequest iRequest = new javax.servlet.http.HttpServletRequestWrapper(request);
        HttpServletResponse iResponse = new javax.servlet.http.HttpServletResponseWrapper(response) {
            public ServletOutputStream getOutputStream() throws IOException {
                return sos;
            }
            public PrintWriter getWriter() throws IOException {
                return pw;
            }            
        };
        try {
            //rd.forward(iRequest, iResponse);
            rd.include(iRequest, iResponse);
            pw.flush();
            sos.flush();
        } catch(IOException ioe) {
            throw new ServletException(ioe);
        }  
    }
    public static void intercept(RequestDispatcher rd, HttpServletRequest request, HttpServletResponse response, Writer writer) throws ServletException {
        final PrintWriter pw = (writer instanceof PrintWriter ? (PrintWriter)writer : new PrintWriter(writer));
        final ServletOutputStream sos = new ServletOutputStream() {
            public void write(int b)  throws IOException {
                pw.write(b);
            }

			@Override
			public boolean isReady() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void setWriteListener(WriteListener arg0) {
				// TODO Auto-generated method stub
				
			}
        };         
        HttpServletRequest iRequest = new javax.servlet.http.HttpServletRequestWrapper(request);
        HttpServletResponse iResponse = new javax.servlet.http.HttpServletResponseWrapper(response) {
            public ServletOutputStream getOutputStream() throws IOException {
                return sos;
            }
            public PrintWriter getWriter() throws IOException {
                return pw;
            }            
        };
        try {
            //rd.forward(iRequest, iResponse);
            rd.include(iRequest, iResponse);
            pw.flush();
            sos.flush();
        } catch(IOException ioe) {
            throw new ServletException(ioe);
        }  
    }

	protected static void internalIntercept(HttpServletRequest iRequest, InterceptingResponse iResponse, String path) throws ServletException {
		RequestDispatcher rd = iRequest.getRequestDispatcher(path);
		try {
			rd.include(iRequest, iResponse);
			iResponse.flushBuffer();
		} catch(IOException ioe) {
			throw new ServletException(ioe);
		}
	}

	public static void intercept(HttpServletRequest request, String path, Writer writer) throws ServletException {
		internalIntercept(new javax.servlet.http.HttpServletRequestWrapper(request), new InterceptingResponse(request, writer), path);
	}

	public static void intercept(HttpServletRequest request, String path, OutputStream out) throws ServletException {
		internalIntercept(new javax.servlet.http.HttpServletRequestWrapper(request), new InterceptingResponse(request, out), path);
	}

	public static String intercept(HttpServletRequest request, String path) throws ServletException {
		StringWriter writer = new StringWriter();
		intercept(request, path, writer);
		return writer.toString();
	}

	public static void intercept(RequestDispatcherFactory requestDispatcherFactory, ServletContext context, String path, Locale locale, Map<String, String[]> parameters, Map<String, Object> attributes, Writer writer) throws ServletException {
		InterceptingRequest iRequest = new InterceptingRequest(context, path, locale, parameters, requestDispatcherFactory);
		if(attributes != null)
			iRequest.setAttributes(attributes);
		internalIntercept(iRequest, new InterceptingResponse(iRequest, writer), "");
	}

	public static void intercept(RequestDispatcherFactory requestDispatcherFactory, ServletContext context, String path, Locale locale, Map<String, String[]> parameters, Map<String, Object> attributes, OutputStream out) throws ServletException {
		InterceptingRequest iRequest = new InterceptingRequest(context, path, locale, parameters, requestDispatcherFactory);
		if(attributes != null)
			iRequest.setAttributes(attributes);
		internalIntercept(iRequest, new InterceptingResponse(iRequest, out), "");
	}

	public static String intercept(RequestDispatcherFactory requestDispatcherFactory, ServletContext context, String path, Locale locale, Map<String, String[]> parameters, Map<String, Object> attributes) throws ServletException {
		StringWriter writer = new StringWriter();
		intercept(requestDispatcherFactory, context, path, locale, parameters, attributes, writer);
		return writer.toString();
	}
}
