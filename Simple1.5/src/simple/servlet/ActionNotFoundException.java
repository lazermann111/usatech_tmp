package simple.servlet;

public class ActionNotFoundException extends Exception {
	private static final long serialVersionUID = -5361598847441954732L;

	public ActionNotFoundException() {
	}

	public ActionNotFoundException(String message) {
		super(message);
	}

	public ActionNotFoundException(Throwable cause) {
		super(cause);
	}

	public ActionNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
