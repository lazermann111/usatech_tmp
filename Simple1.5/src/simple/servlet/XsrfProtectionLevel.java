package simple.servlet;

public enum XsrfProtectionLevel {
	ENABLED_FOR_ALL, ENABLED_FOR_SOME, NOT_ENABLED
}
