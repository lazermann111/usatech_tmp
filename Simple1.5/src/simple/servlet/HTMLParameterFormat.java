/*
 * FileSizeFormat.java
 *
 * Created on March 7, 2003, 1:21 PM
 */

package simple.servlet;

import java.lang.reflect.Array;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import simple.text.StringUtils;

/**
 *
 * @author  Brian S. Krug
 */
public class HTMLParameterFormat extends Format {
    /**
	 * 
	 */
	private static final long serialVersionUID = 91671717911L;
	protected String paramName;
    /** Creates a new instance of FileSizeFormat
     * @param paramName
     */
    public HTMLParameterFormat(String paramName) {
        this.paramName = StringUtils.prepareURLPart(paramName);
    }
    
    /**
     * @param obj
     * @param result
     * @param fieldPosition
     * @return
     */    
    public StringBuffer format(Object obj, StringBuffer result, FieldPosition fieldPosition) {        
        if(obj == null) return result;
        if(obj.getClass().isArray()) {
            for(int i = 0; i < Array.getLength(obj); i++) {
                Object elem = Array.get(obj, i);
                if(elem == null) continue;
                if(i > 0) result.append("&");
                result.append(paramName + "=" + StringUtils.prepareURLPart(elem.toString()));
            }
        } else {
            result.append(paramName + "=" + StringUtils.prepareURLPart(obj.toString()));
        }
        return result;
    }
    
    /**
     * @param str
     * @param pos
     * @return
     */    
    public Object parseObject(String str, ParsePosition pos) {
        Object o = null;//parse(str);
        pos.setIndex(str.length());
        return o;
    }
}
