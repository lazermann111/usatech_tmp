/*
 * Created on Feb 15, 2005
 *
 */
package simple.servlet;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLStreamHandler;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import simple.io.Log;
import simple.io.ResourceResolver;

/**
 * @author bkrug
 *
 */
public class ServletContextResourceResolver implements ResourceResolver {
	private static final Log log = Log.getLog();
    protected ServletContext servletContext;
    protected String defaultPrefix = "web:/";
    protected static final Pattern pathPattern = Pattern.compile("\\s*([a-zA-Z]{2,}):([^?;#]+)(?:[?;#](.*))?");

    /**
     *
     */
    public ServletContextResourceResolver(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
    /* (non-Javadoc)
     * @see simple.falcon.engine.ResourceResolver#getResourceURL(java.lang.String)
     */
    public URL getResourceURL(String path) throws MalformedURLException, FileNotFoundException {
    	Matcher matcher = pathPattern.matcher(path);
    	if(matcher.matches()) {
    		String protocol = matcher.group(1);
    		String file = matcher.group(2);
    		if(protocol.equalsIgnoreCase("web")) {
    			URL url = servletContext.getResource(file);
                if(url == null)
                    throw new FileNotFoundException("Could not find '" + file + "' in the servlet context (web:)");
                return url;
    		} else if(protocol.equals("file")) {
    			 return new URL("file:" + file);
    		} else {
    			try {
    				return new URL(path);
    			} catch(MalformedURLException e) {
    				log.warn("Unknown protocol '" + protocol + "'", e);
					if(e.getMessage().contains("unknown protocol")) {
        				try {
        					URLStreamHandler handler = Class.forName("simple.net.protocol." + protocol + ".Handler").asSubclass(URLStreamHandler.class).newInstance();
        					return new URL((URL)null, path, handler);
						} catch(ClassNotFoundException e1) {
							log.warn("Could not find handler for protocol '" + protocol + "'", e1);
						} catch(InstantiationException e1) {
							log.warn("Could not create handler for protocol '" + protocol + "'", e1);
						} catch(IllegalAccessException e1) {
							log.warn("Could not create handler for protocol '" + protocol + "'", e1);
						} catch(ClassCastException e1) {
							log.warn("Handler is wrong class for protocol '" + protocol + "'", e1);
						}
        		    }
        			throw e;
        		}
    		}
    	} else if(path.charAt(0) == '/') { //absolute - XXX: what should we do here?
    		URL url = getClass().getClassLoader().getResource(path.substring(1));
    		if(url == null)
                throw new FileNotFoundException("Could not find '" + path + "' in the class path");
            return url;
        } else if(defaultPrefix != null && defaultPrefix.trim().length() > 0 && !path.startsWith(defaultPrefix)) {
            return getResourceURL(defaultPrefix + path);
        } else {
            return new URL(path);
        }
    }
    /**
     * @return Returns the defaultPrefix.
     */
    public String getDefaultPrefix() {
        return defaultPrefix;
    }
    /**
     * @param defaultPrefix The defaultPrefix to set.
     */
    public void setDefaultPrefix(String defaultPrefix) {
        this.defaultPrefix = defaultPrefix;
    }

}
