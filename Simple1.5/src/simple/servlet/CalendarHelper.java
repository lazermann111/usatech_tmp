/**
 *
 */
package simple.servlet;

import java.util.Calendar;
import java.util.Locale;

/**
 * @author Brian S. Krug
 *
 */
public class CalendarHelper {
	protected final Locale locale;
	protected final Calendar beginCal;
	protected final Calendar endCal;
	protected final int maxYear;
	protected final int minYear;

	public CalendarHelper(Locale locale, Calendar beginCal, Calendar endCal) {
		this(locale, beginCal, endCal, null);
	}

	public CalendarHelper(Locale locale, Calendar beginCal, Calendar endCal, Calendar minCal) {
		this.locale = locale;
		if(beginCal == null && endCal == null) {
			this.beginCal = this.endCal = Calendar.getInstance(locale);
		} else if(beginCal == null) {
			this.beginCal = this.endCal = endCal;
		} else if(endCal == null) {
			this.beginCal = this.endCal = beginCal;
		} else {
			this.beginCal = beginCal;
			this.endCal = endCal;
		}
		Calendar maxCal = Calendar.getInstance(locale);
		maxCal.roll(Calendar.DATE, 1);
		this.maxYear = maxCal.get(Calendar.YEAR);	
		if(minCal != null)
			this.minYear = minCal.get(Calendar.YEAR);
		else
			this.minYear = this.maxYear - 5;
	}

	public String[] getMonthNames() {
		java.text.DateFormatSymbols dfs = new java.text.DateFormatSymbols(locale);
	    return dfs.getMonths();
	}

	public int getBeginYear() {
		return beginCal.get(Calendar.YEAR);
	}

	public int getBeginMonth() {
		return beginCal.get(Calendar.MONTH) + 1;
	}

	public int getBeginDay() {
		return beginCal.get(Calendar.DAY_OF_MONTH);
	}

	public int getBeginHour() {
		return beginCal.get(Calendar.HOUR_OF_DAY);
	}

	public int getBeginMinute() {
		return beginCal.get(Calendar.MINUTE);
	}

	public int getBeginSecond() {
		return beginCal.get(Calendar.SECOND);
	}
	
	public int getEndYear() {
		return endCal.get(Calendar.YEAR);
	}

	public int getEndMonth() {
		return endCal.get(Calendar.MONTH) + 1;
	}

	public int getEndDay() {
		return endCal.get(Calendar.DAY_OF_MONTH);
	}

	public int getEndHour() {
		return endCal.get(Calendar.HOUR_OF_DAY);
	}

	public int getEndMinute() {
		return endCal.get(Calendar.MINUTE);
	}

	public int getEndSecond() {
		return endCal.get(Calendar.SECOND);
	}

	public int getMaxYear() {
		return maxYear;
	}

	public int getMinYear() {
		return minYear;
	}
}
