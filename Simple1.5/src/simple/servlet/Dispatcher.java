/*
 * Dispatcher.java
 *
 * Created on January 15, 2003, 8:59 AM
 */

package simple.servlet;

import java.io.OutputStream;
import java.io.Writer;

import javax.servlet.ServletException;

/** The "callback" object of the SimpleServlet that allows other objects to
 * dispatch (use SimpleServlet's built-in request routing), include or forward
 * (using the ServletContainer's RequestDispatcher), or intercept (using
 * WrappedServletRequest and WrappedServletResponse objects) requests.
 * @author Brian S. Krug
 */
public interface Dispatcher {
    /** Forwards processing of this request using SimpleServlet's built-in request routing
     * @param path The path is used to route this request
     * @param privateReferer boolean to indicate whether the dispatch is from our internal program or from www
     * @throws ServletException If an exception occurs
     */    
    public void dispatch(String path, boolean privateReferer) throws ServletException;
    /** Forwards the request processing to the specified web resource using the ServletContainer's RequestDispatcher
     * @param path The web resource to which processing is passed
     * @throws ServletException If an exception occurs
     */    
    public void forward(String path) throws ServletException;
    /** Includes the request processing of the specified web resource using the ServletContainer's RequestDispatcher
     * @param path The web resource to which processing is passed
     * @throws ServletException If an exception occurs
     */    
    public void include(String path) throws ServletException; 
    /** Intercepts the request processing response of the specified web resource using the WrappedServletRequest
     * and WrappedServletResponse objects
     * @param path The web resource to which processing is passed and whose response is intercepted
     * @param out The OutputStream into which the response is written
     * @throws ServletException If an exception occurs
     */    
    public void intercept(String path, OutputStream out) throws ServletException;
    
    /** Intercepts the request processing response of the specified web resource using the WrappedServletRequest
     * and WrappedServletResponse objects
     * @param path The web resource to which processing is passed and whose response is intercepted
     * @param writer The Writer into which the response is written
     * @throws ServletException If an exception occurs
     */    
    public void intercept(String path, Writer writer) throws ServletException ;
    /** Returns the InputForm for the request that generated this Dispatcher
     * @return The InputForm object
     */    
    public InputForm getForm() ;
}

