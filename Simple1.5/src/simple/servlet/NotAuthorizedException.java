/*
 * NotAuthorizedException.java
 *
 * Created on January 27, 2003, 9:31 AM
 */

package simple.servlet;

import javax.servlet.ServletException;

/** Signifies that the current user of the ServletSession is not authorized to
 * perform the request
 * @author Brian S. Krug
 */
public class NotAuthorizedException extends ServletException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 810431865L;


	/**
     * Creates new <code>NotAuthorizedException</code> without detail message.
     */
    public NotAuthorizedException() {
    }


    /**
     * Constructs an <code>NotAuthorizedException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public NotAuthorizedException(String msg) {
        super(msg);
    }
}


