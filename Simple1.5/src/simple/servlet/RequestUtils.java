/*
 * RequestUtils.java
 *
 * Created on March 7, 2002, 11:36 AM
 */

package simple.servlet;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.ServerSocket;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.AccessControlException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;
import org.eclipse.jetty.util.URIUtil;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.bean.NestedNameTokenizer;
import simple.bean.ReflectionUtils;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.security.SecurityUtils;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.AbstractLookupMap;
import simple.util.CaseInsensitiveComparator;
import simple.util.TimeUtils;
import simple.util.concurrent.LockSegmentCache;
import simple.xml.sax.MessagesInputSource;

/** Utility class for various functions on ServletRequests
 * @author Brian S. Krug
 */
public class RequestUtils {
	private static final Log log = Log.getLog();
	public static final String PAGE_SCOPE = "page";
    public static final String REQUEST_SCOPE = "request";
    public static final String SESSION_SCOPE = "session";
    public static final String APPLICATION_SCOPE = "application";
	public static final String ATTRIBUTE_MESSAGES = MessagesInputSource.class.getName();
	
	public static final String CSRNG_ALGORITHM = "SHA1PRNG";
	public static final String CSRNG_PROVIDER = "SUN";
	protected static SecureRandom sessionTokenGenerator = null;

    protected static Map<String,AttributeMgr> requestAttrMgrs = new LinkedHashMap<String,AttributeMgr>();
    protected static Map<String,AttributeMgr> pageContextAttrMgrs = new LinkedHashMap<String,AttributeMgr>();    
    static {
        requestAttrMgrs.put("request", new RequestAttributeMgr());
        requestAttrMgrs.put("session", new SessionAttributeMgr());
        requestAttrMgrs.put("application", new ApplicationAttributeMgr());

        pageContextAttrMgrs.put("page", new PageContextAttributeMgr(PageContext.PAGE_SCOPE));
		pageContextAttrMgrs.put("request", new RequestAttributeMgr());
		pageContextAttrMgrs.put("session", new SessionAttributeMgr());
		pageContextAttrMgrs.put("application", new ApplicationAttributeMgr());

        try {
        	sessionTokenGenerator = SecureRandom.getInstance(CSRNG_ALGORITHM, CSRNG_PROVIDER);
        } catch (Exception e) {
        	log.error("Error getting instance of SecureRandom", e);
        }
    }

    protected static interface AttributeMgr {
        public Object getAttribute(Object base, String key) ;
        public void setAttribute(Object base, String key, Object value) ;
        public Object setAttributeIfNull(Object base, String key, Object value) ;
    }

    protected static class RequestAttributeMgr implements AttributeMgr {
        public Object setAttributeIfNull(Object base, String key, Object value) {
			ServletRequest request = getRequest(base);
			if(base == null)
				return null;
        	synchronized(request) {
				Object attr = getAttribute(request, key);
                if(attr != null) return attr;
				setAttribute(request, key, value);
                return value;
            }
        }
        public Object getAttribute(Object base, String key) {
			if(base instanceof InputForm)
				return getAttribute((InputForm) base, key);
			ServletRequest request = getRequest(base);
			if(request == null)
				return null;
			return getAttribute(request, key);
		}

		protected Object getAttribute(InputForm form, String key) {
			Object attr = form.getRequest().getAttribute(key);
			if(attr == null)
				attr = form.getParameters().get(key);
            return attr;
        }

		protected Object getAttribute(ServletRequest request, String key) {
			Object attr = request.getAttribute(key);
			if(attr == null) {
				InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
				if(form != null)
					attr = form.getParameters().get(key);
				else
					attr = request.getParameter(key);
			}
			return attr;
		}

        public void setAttribute(Object base, String key, Object value) {
			if(base instanceof InputForm) {
				((InputForm) base).setAttribute(key, value);
			} else {
				ServletRequest request = getRequest(base);
				if(request == null)
					return;
				setAttribute(request, key, value);
			}
		}

		protected void setAttribute(ServletRequest request, String key, Object value) {
            if (key == null)
            	return;
			request.setAttribute(key, value);
        }
    }

    protected static class SessionAttributeMgr implements AttributeMgr {
        public Object setAttributeIfNull(Object base, String key, Object value) {
        	HttpSession session = getSession(base);
        	if(session != null) {
	        	synchronized(session) {
	                Object attr = session.getAttribute(key);
	                if(attr != null) return attr;
	                session.setAttribute(key, value);
	                return value;
	            }
        	}
        	return null;
        }
        public Object getAttribute(Object base, String key) {
        	HttpSession session = getSession(base);
        	return session == null ? null : session.getAttribute(key);
        }
        public void setAttribute(Object base, String key, Object value) {
        	HttpSession session = getSession(base);
        	if(session != null)
        		session.setAttribute(key, value);
        }
    }

    protected static class ApplicationAttributeMgr implements AttributeMgr {
        public Object setAttributeIfNull(Object base, String key, Object value) {
        	HttpSession session = getSession(base);
        	if(session != null) {
        		ServletContext s = session.getServletContext();
	            synchronized(s) {
	                Object attr = s.getAttribute(key);
	                if(attr != null) return attr;
	                s.setAttribute(key, value);
	                return value;
	            }
        	}
        	return null;
        }
        public Object getAttribute(Object base, String key) {
            HttpSession session = getSession(base);
        	return session == null ? null : session.getServletContext().getAttribute(key);
        }
        public void setAttribute(Object base, String key, Object value) {
        	HttpSession session = getSession(base);
        	if(session != null)
        		session.getServletContext().setAttribute(key, value);
        }
    }

    protected static class PageContextAttributeMgr implements AttributeMgr {
        private final int scope;
        public PageContextAttributeMgr(int _scope) {
            scope = _scope;
        }
        public Object setAttributeIfNull(Object base, String key, Object value) {
            synchronized(base) {
                Object attr = getAttribute(base, key);
                if(attr != null) return attr;
                setAttribute(base, key, value);
                return value;
            }
        }
        public Object getAttribute(Object base, String key) {
            PageContext pc = (PageContext)base;
            Object o = pc.getAttribute(key, scope);
            if(o == null && scope == PageContext.REQUEST_SCOPE)
                o = pc.getRequest().getParameter(key);
            return o;
        }
        public void setAttribute(Object base, String key, Object value) {
            PageContext pc = (PageContext)base;
            pc.setAttribute(key, value, scope);
        }
    }

	protected static ServletRequest getRequest(Object base) {
		if(base instanceof InputForm)
			return ((InputForm) base).getRequest();
		if(base instanceof PageContext)
			return ((PageContext) base).getRequest();
		if(base instanceof ServletRequest)
			return (ServletRequest) base;
		return null;
	}

    protected static HttpSession getSession(Object base) {
		ServletRequest request = getRequest(base);
    	if(request instanceof HttpServletRequest) 
    		return ((HttpServletRequest)request).getSession();
        return null;
    }
    /** All methods are static, no need to create */
    private RequestUtils() {
    }

    public static Map<String,Object> getAttributeMap(final ServletRequest request) {
    	return new AbstractLookupMap<String,Object>() {
    		protected Set<String> keySet;
			@Override
			public Object get(Object key) {
				return request.getAttribute((String)key);
			}
			@Override
			public Set<String> keySet() {
				if(keySet == null) {
					keySet = new HashSet<String>();
					for(Enumeration<?> en = request.getAttributeNames(); en.hasMoreElements(); ) {
			            String name = (String)en.nextElement();
			            keySet.add(name);
					}
				}
				return keySet;
			}
    	};
    }

    public static Map<String,Object> getAttributeMap(final HttpSession session) {
    	return new AbstractLookupMap<String,Object>() {
    		protected Set<String> keySet;
			@Override
			public Object get(Object key) {
				return session.getAttribute((String)key);
			}
			@Override
			public Set<String> keySet() {
				if(keySet == null) {
					keySet = new HashSet<String>();
					for(Enumeration<?> en = session.getAttributeNames(); en.hasMoreElements(); ) {
			            String name = (String)en.nextElement();
			            keySet.add(name);
					}
				}
				return keySet;
			}
    	};
    }

    /** Retrieves a String from the request's attributes or paramters
     *
     * @return The value as a String
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
/*    public static String getRequestString(HttpServletRequest request, String name, boolean required) throws ServletException {
        Object o = request.getAttribute(name);
        String s = null;
        if(o != null) s = o.toString().replace((char)160,' ').trim();
        if(o == null || s.length() == 0) {
            s = request.getParameter(name);
            if(s != null) s = s.replace((char)160,' ').trim();
            if(required && (s == null || s.length() == 0)) throw new ServletException("Parameter, '" + name + "', is missing");
        }
        return s;
    }*/

    /** Retrieves an int from the request's attributes or paramters
     *
     * @return The value as an int
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
/*    public static int getRequestInt(HttpServletRequest request, String name, boolean required) throws ServletException {
        Object o =  request.getAttribute(name);
        if(o instanceof Number) return ((Number)o).intValue();
        if(o == null) o = getRequestString(request, name, required);
        int i = 0;
        try {
            i = Integer.parseInt(o.toString());
        } catch(NumberFormatException nfe) {
            if(required) throw new ServletException("Parameter, '" + name + "', is not a number");
        } catch(NullPointerException npe) {
            if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        }
        return i;
    }
*/
    /** Retrieves a long from the request's attributes or paramters
     *
     * @return The value as a long
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
/*    public static long getRequestLong(HttpServletRequest request, String name, boolean required) throws ServletException {
        Object o =  request.getAttribute(name);
        if(o instanceof Number) return ((Number)o).longValue();
        if(o == null) o = getRequestString(request, name, required);
        long i = 0;
        try {
            i = Long.parseLong(o.toString());
        } catch(NumberFormatException nfe) {
            if(required) throw new ServletException("Parameter, '" + name + "', is not a number");
        } catch(NullPointerException npe) {
            if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        }
        return i;
    }
*/
    /** Retrieves a String of digits from the request's attributes or paramters
     *
     * @return The value as a String with all non-digits stripped out
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
/*    public static String getRequestNumberString(HttpServletRequest request, String name, boolean required) throws ServletException {
        String s = getRequestString(request, name, required);
        if(s == null) return s;
        char c;
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            if(Character.isDigit(c)) sb.append(c);
        }
        if(required && sb.length() == 0) throw new ServletException("Parameter, '" + name + "', is missing");
        return sb.toString();
    }
*/
    /** Retrieves an int array from the request's attributes or paramters
     *
     * @return The value as an int array
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
/*    public static int[] getRequestIntArray(HttpServletRequest request, String name, boolean required) throws ServletException {
        Object o =  request.getAttribute(name);
        try {
            if(o != null) {
                Class c = o.getClass();
                if(c.isArray()) {
                    if(c.getComponentType() == int.class) return (int[]) o;
                    else {
                        int len = Array.getLength(o);
                        int k = 0;
                        int[] r = new int[len];
                        for(int i = 0; i < len; i++) {
                            Object t =  Array.get(o,i);
                            if(t instanceof Number) r[i+k] = ((Number)t).intValue();
                            else if(t != null) {
                                try {
                                    r[i+k] = Integer.parseInt(t.toString());
                                } catch(NumberFormatException nfe0) {
                                    java.util.StringTokenizer token = new java.util.StringTokenizer(t.toString(),",");
                                    int[] tmp = new int[r.length+token.countTokens()];
                                    if(i+k > 0) System.arraycopy(r,0,tmp,0,i+k);
                                    r = tmp;
                                    for(; token.hasMoreTokens(); k++) r[i+k] = Integer.parseInt(token.nextToken().trim());
                                }
                            }
                        }
                        return r;
                    }
                } else if(o instanceof Number) {
                    return new int[] {((Number)o).intValue()};
                } else {
                    java.util.StringTokenizer token = new java.util.StringTokenizer(o.toString(),",");
                    int[] r = new int[token.countTokens()];
                    for(int i = 0; i < r.length; i++) r[i] = Integer.parseInt(token.nextToken().trim());
                    return r;
                }
            }
            String[] s = request.getParameterValues(name);
            if(s == null || s.length == 0) {
                if(required) throw new ServletException( "Parameter, '" + name + "', is missing");
                else return new int[0];
            }
            int[] r;
            if(s.length == 1) {
                java.util.StringTokenizer token = new java.util.StringTokenizer(s[0].trim(),",");
                r = new int[token.countTokens()];
                for(int i = 0; i < r.length; i++) r[i] = Integer.parseInt(token.nextToken().trim());
            } else {
                r = new int[s.length];
                int k = 0;
                for(int i = 0; i < s.length; i++) {
                    try {
                        r[i+k] = Integer.parseInt(s[i].trim());
                    } catch(NumberFormatException nfe0) {
                        java.util.StringTokenizer token = new java.util.StringTokenizer(s[i].trim(),",");
                        int[] tmp = new int[r.length+token.countTokens()];
                        if(i+k > 0) System.arraycopy(r,0,tmp,0,i+k);
                        r = tmp;
                        for(; token.hasMoreTokens(); k++) r[i+k] = Integer.parseInt(token.nextToken().trim());
                    }
                }
            }
            return r;
        } catch(NumberFormatException nfe) {
            throw new ServletException(  "Parameter, '" + name + "', contains non-numeric values");
        } catch(IllegalArgumentException iae) {
            throw new ServletException(  "An exception occurred reading the array");
        }
    }
*/
    /** Retrieves a long array from the request's attributes or paramters
     *
     * @return The value as a long array
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
/*    public static long[] getRequestLongArray(HttpServletRequest request, String name, boolean required) throws ServletException {
        Object o =  request.getAttribute(name);
        try {
            if(o != null) {
                Class c = o.getClass();
                if(c.isArray()) {
                    if(c.getComponentType() == long.class) return (long[]) o;
                    else {
                        int len = Array.getLength(o);
                        int k = 0;
                        long[] r = new long[len];
                        for(int i = 0; i < len; i++) {
                            Object t =  Array.get(o,i);
                            if(t instanceof Number) r[i+k] = ((Number)t).longValue();
                            else if(t != null) {
                                try {
                                    r[i+k] = Long.parseLong(t.toString());
                                } catch(NumberFormatException nfe0) {
                                    java.util.StringTokenizer token = new java.util.StringTokenizer(t.toString(),",");
                                    long[] tmp = new long[r.length+token.countTokens()];
                                    if(i+k > 0) System.arraycopy(r,0,tmp,0,i+k);
                                    r = tmp;
                                    for(; token.hasMoreTokens(); k++) r[i+k] = Long.parseLong(token.nextToken().trim());
                                }
                            }
                        }
                        return r;
                    }
                } else if(o instanceof Number) {
                    return new long[] {((Number)o).longValue()};
                } else {
                    java.util.StringTokenizer token = new java.util.StringTokenizer(o.toString(),",");
                    long[] r = new long[token.countTokens()];
                    for(int i = 0; i < r.length; i++) r[i] = Long.parseLong(token.nextToken().trim());
                    return r;
                }
            }
            String[] s = request.getParameterValues(name);
            if(s == null || s.length == 0) {
                if(required) throw new ServletException( "Parameter, '" + name + "', is missing");
                else return new long[0];
            }
            long[] r;
            if(s.length == 1) {
                java.util.StringTokenizer token = new java.util.StringTokenizer(s[0].trim(),",");
                r = new long[token.countTokens()];
                for(int i = 0; i < r.length; i++) r[i] = Long.parseLong(token.nextToken().trim());
            } else {
                r = new long[s.length];
                int k = 0;
                for(int i = 0; i < s.length; i++) {
                    try {
                        r[i+k] = Long.parseLong(s[i].trim());
                    } catch(NumberFormatException nfe0) {
                        java.util.StringTokenizer token = new java.util.StringTokenizer(s[i].trim(),",");
                        long[] tmp = new long[r.length+token.countTokens()];
                        if(i+k > 0) System.arraycopy(r,0,tmp,0,i+k);
                        r = tmp;
                        for(; token.hasMoreTokens(); k++) r[i+k] = Long.parseLong(token.nextToken().trim());
                    }
                }
            }
            return r;
        } catch(NumberFormatException nfe) {
            throw new ServletException("Parameter, '" + name + "', contains non-numeric values");
        } catch(IllegalArgumentException iae) {
            throw new ServletException("An exception occurred reading the array");
        }
    }
*/
    /** Retrieves a String array from the request's attributes or paramters
     *
     * @return The value as a String array
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
/*    public static String[] getRequestStringArray(HttpServletRequest request, String name, boolean required) throws ServletException {
        Object o =  request.getAttribute(name);
        try {
            if(o != null) {
                Class c = o.getClass();
                if(c.isArray()) {
                    if(c.getComponentType() == String.class) return (String[]) o;
                    else {
                        String[] r = new String[Array.getLength(o)];
                        for(int i = 0; i < r.length; i++) {
                            Object t =  Array.get(o,i);
                            if(t != null) r[i] = t.toString();
                        }
                        return r;
                    }
                }
            }
            String[] s = request.getParameterValues(name);
            if(required && (s == null || s.length == 0) ) throw new ServletException("Parameter, '" + name + "', is missing");
            if(s != null && s.length == 1) {
                java.util.StringTokenizer token = new java.util.StringTokenizer(s[0].trim(),",");
                s = new String[token.countTokens()];
                for(int i = 0; i < s.length; i++) s[i] = token.nextToken().trim();
            }
            return s;
        } catch(IllegalArgumentException iae) {
            throw new ServletException("An exception occurred reading the array", iae);
        }
    }
*/
    /** Retrieves a boolean array from the request's attributes or paramters
     *
     * @return The value as a boolean array
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
/*    public static boolean[] getRequestBooleanArray(HttpServletRequest request, String name, boolean required) throws ServletException {
        Object o =  request.getAttribute(name);
        try {
            if(o != null) {
                Class c = o.getClass();
                if(c.isArray()) {
                    if(c.getComponentType() == boolean.class) return (boolean[]) o;
                    else {
                        boolean[] r = new boolean[Array.getLength(o)];
                        for(int i = 0; i < r.length; i++) {
                            Object t =  Array.get(o,i);
                            if(t instanceof Boolean) r[i] = ((Boolean)t).booleanValue();
                            else if(t != null) r[i] = Boolean.valueOf(t.toString()).booleanValue();
                        }
                        return r;
                    }
                } else if(o instanceof Boolean) {
                    return new boolean[] {((Boolean)o).booleanValue()};
                } else {
                    java.util.StringTokenizer token = new java.util.StringTokenizer(o.toString(),",");
                    boolean[] r = new boolean[token.countTokens()];
                    for(int i = 0; i < r.length; i++) r[i] = Boolean.valueOf(token.nextToken().trim()).booleanValue();
                    return r;
                }
            }
            String[] s = request.getParameterValues(name);
            if(s == null || s.length == 0) {
                if(required) throw new ServletException("Parameter, '" + name + "', is missing");
                else return new boolean[0];
            }
            boolean[] r;
            if(s.length == 1) {
                java.util.StringTokenizer token = new java.util.StringTokenizer(s[0].trim(),",");
                r = new boolean[token.countTokens()];
                for(int i = 0; i < r.length; i++) r[i] = Boolean.valueOf(token.nextToken().trim()).booleanValue();
            } else {
                r = new boolean[s.length];
                for(int i = 0; i < r.length; i++) r[i] = Boolean.valueOf(s[i].trim()).booleanValue();
            }
            return r;
        } catch(NumberFormatException nfe) {
            throw new ServletException("Parameter, '" + name + "', contains non-boolean values");
        } catch(IllegalArgumentException iae) {
            throw new ServletException("An exception occurred reading the array", iae);
        }
    }
*/
    /** Retrieves a BigDecimal from the request's attributes or paramters
     *
     * @return The value as a BigDecimal
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
/*    public static BigDecimal getRequestBigDecimal(HttpServletRequest request, String name, boolean required) throws ServletException {
        Object o =  request.getAttribute(name);
        if(o instanceof BigDecimal) return (BigDecimal)o;
        if(o == null) o = getRequestString(request, name, required);
        BigDecimal bd = null;
        try {
            bd = new BigDecimal(o.toString());
        } catch(NumberFormatException nfe) {
            if(required) throw new ServletException("Parameter, '" + name + "', is not a number");
        } catch(NullPointerException npe) {
            if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        }
        return bd;
    }
*/
    /** Retrieves a boolean from the request's attributes or paramters
     *
     * @return The value as a boolean
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @param defaultValue The value to return if the value is not found and
     * the required parameter is false
     * @throws ServletException If an exception occurs
     */
/*    public static boolean getRequestBoolean(HttpServletRequest request, String name, boolean required, boolean defaultValue) throws ServletException {
        Object o =  request.getAttribute(name);
        if(o instanceof Boolean) return ((Boolean)o).booleanValue();
        if(o == null) o = getRequestString(request, name, required);
        String s;
        if(o == null || (s = o.toString().trim()).length() == 0) {
            if(required) throw new ServletException("Parameter, '" + name + "', is missing");
            else return defaultValue;
        } else {
            try {
                return ConvertUtils.getBoolean(s);
            } catch (ConvertException e) {
                return defaultValue;
            }
        }
    }
*/
    /** Retrieves a Date from the request's attributes or paramters
     *
     * @return The value as a Date
     * @param request The request in which to look
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @param df The DateFormat to use to parse the value into a Date
     * @throws ServletException If an exception occurs
     */
/*    public static java.util.Date getRequestDate(HttpServletRequest request, String name, boolean required, java.text.DateFormat df) throws ServletException {
        Object o =  request.getAttribute(name);
        if(o instanceof java.util.Date) return (java.util.Date)o;
        if(o == null) o = getRequestString(request, name, required);
        if(o == null || o.toString().trim().length() == 0) {
            if(required) throw new ServletException("Parameter, '" + name + "', is missing");
            else return null;
        }
        java.util.Date d = null;
        d = parseDate(o.toString(), df);
        return d;
    }
*/
    /**
     * Returns the specified text as a Date using the DateFormat provided. If
     * the text is enclosed in braces, {}, then alternate parsing is used:
     * If the next character after the begin brace is an asteric, *, then this
     * method returns the start of the current YEAR, MONTH, or WEEK. Otherwise,
     * it attempts to turn the text between the braces into a long and adds that
     * many milliseconds to the current date returns the new offset date.
     *
     * @param text The String to parse
     * @param df The DateFormat to use to parse the text
     * @return The parsed Date
     */
/*    protected static java.util.Date parseDate(String text, java.text.DateFormat df) throws ServletException {
        java.util.Date date = null;
        if(text.startsWith("{") && text.endsWith("}")) {
            if(text.charAt(1) == '*') {
                text = text.substring(2,text.length()-1);
                if("YEAR".equalsIgnoreCase(text)) {
                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.DAY_OF_YEAR,1);
                    cal.set(Calendar.HOUR_OF_DAY,0);
                    cal.clear(Calendar.MINUTE);
                    cal.clear(Calendar.SECOND);
                    cal.clear(Calendar.MILLISECOND);
                    date = cal.getTime();
                } else if("MONTH".equalsIgnoreCase(text)) {
                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.DAY_OF_MONTH,1);
                    cal.set(Calendar.HOUR_OF_DAY,0);
                    cal.clear(Calendar.MINUTE);
                    cal.clear(Calendar.SECOND);
                    cal.clear(Calendar.MILLISECOND);
                    date = cal.getTime();
                } else if("WEEK".equalsIgnoreCase(text)) {
                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.DAY_OF_WEEK,1);
                    cal.clear(Calendar.MINUTE);
                    cal.clear(Calendar.SECOND);
                    cal.clear(Calendar.MILLISECOND);
                    date = cal.getTime();
                } else {
                    throw new ServletException("Date parameter symbol not recognized: '" + text + "'");
                }
            } else {
                try {
                    long adj = Long.parseLong(text.substring(1,text.length()-1));
                    date = new java.util.Date(System.currentTimeMillis() + adj);
                } catch(NumberFormatException e) {
                    throw new ServletException("Exception parsing date: '" + text + "'",e);
                }
            }
        } else {
            try {
                date = df.parse(text.trim());
            } catch(ParseException e) {
                throw new ServletException("Exception parsing date: '" + text + "'",e);
            }
        }
        return date;
    }
*/
    /** Returns the String primaryString or the String defaultString if primaryString is null
     * @return The primary String unless it is NULL, then the default String
     * @param primaryString The primary String
     * @param defaultString The default String
     */
/*    public static String nvl(String primaryString, String defaultString) {
        return (primaryString == null ? defaultString : primaryString);
    }
*/
    /** Returns the given object as comma-separated String
     * @return
     * @param o
     */
/*    public static String toStringList(Object o) {
        if(o == null) return null;
        if(!o.getClass().isArray()) return o.toString();
        StringBuilder sb = new StringBuilder();
        int len = Array.getLength(o);
        for(int i = 0; i < len; i++) {
            if(i != 0) sb.append(", ");
            Object ele = Array.get(o,i);
            if(ele != null)	sb.append(ele.toString());
        }
        return sb.toString();
    }
*/
    /** Splits a query string into name, value pairs in a Map and returns
     * that Map
     * @return
     * @param paramString
     */
	public static Map<String, Object> parseParameters(String paramString) {
        Map<String,Object> map = new HashMap<String,Object>();
        String[] params = StringUtils.split(paramString, '&');
        for(int i = 0; i < params.length; i++) {
            String[] nv = StringUtils.split(params[i],'=');
            if(nv.length < 2) continue;
			try {
				nv[0] = URLDecoder.decode(nv[0], "UTF-8");
				nv[1] = URLDecoder.decode(nv[1], "UTF-8");
			} catch(UnsupportedEncodingException e) {
				continue;
			}
            Object old = map.get(nv[0]);
            Object val;
            if(old == null) {
                val = nv[1];
            } else if(old instanceof String[]) {
                int len = ((String[])old).length;
                val = new String[len+1];
                System.arraycopy(old,0,val,0,len);
                ((String[])val)[len] = nv[1];
            } else if(old instanceof String) {
                val = new String[] {(String)old, nv[1]};
            } else {
                val = nv[1];
            }
            map.put(nv[0],val);
        }
        return map;
    }

    /** Splits a String into an array of Strings, using the given
     * delimiter
     * @return
     * @param original
     * @param delimiter
     */
/*    public static String[] split(String original, char delimiter) {
        List<String> l = new ArrayList<String>();
        for(int start = 0, end = 0; start < original.length(); start = end + 1) {
            end = original.indexOf(delimiter,start);
            if(end < 0) end = original.length();
            l.add(original.substring(start,end));
        }
        return l.toArray(new String[l.size()]);
    }
*/
	protected static boolean containsNestedName(String name, Set<String> set) {
    	NestedNameTokenizer token = new NestedNameTokenizer(name);
        while(token.hasNext()) {
            try {
				token.nextProperty();
			} catch(ParseException e) {
				return false;
			}
            if(set.contains(token.getConsumed())) {
                return true;
            }
        }
        return false;
    }

	public static String getBaseUrl(ServletRequest request) {
		if(request instanceof HttpServletRequest)
			return getBaseUrl((HttpServletRequest) request);
		return null;
	}

	public static String getBaseUrl(HttpServletRequest request, boolean includeUri, boolean includePage) {
		StringBuilder url = new StringBuilder();
		String scheme = request.getScheme();
		int port = request.getServerPort();
		url.append(scheme);
		url.append("://");
		url.append(request.getServerName());
		if(port > 0 && ((scheme.equalsIgnoreCase(URIUtil.HTTP) && port != 80) || (scheme.equalsIgnoreCase(URIUtil.HTTPS) && port != 443))) {
			url.append(':');
			url.append(port);
		}
		if (includeUri) {
			String uri = (String) request.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI);
			if(uri == null)
				uri = request.getRequestURI();
			if (includePage)
				url.append(uri);
			else {
				int lastSlashPos = uri.lastIndexOf("/");
				if (lastSlashPos > -1)
					url.append(uri.subSequence(0, lastSlashPos + 1));
			}
		}
		return url.toString();
	}

	public static String getBaseUrl(HttpServletRequest request, boolean includeUri) {
		return getBaseUrl(request, includeUri, true);
	}
	public static String getBaseUrl(HttpServletRequest request) {
		return getBaseUrl(request, true, true);
	}

    public static FileUpload createFileUpload(File tmpDir, long maxSize) {
    	FileUpload fileUpload = new FileUpload();
		fileUpload.setSizeMax(maxSize);
		fileUpload.setFileItemFactory(new DiskFileItemFactory(DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD, tmpDir));
		//fileUpload.setFileItemFactory(new DefaultFileItemFactory(DefaultFileItemFactory.DEFAULT_SIZE_THRESHOLD, tmpDir));
		return fileUpload;
    }

    @SuppressWarnings("deprecation")
	public static boolean isMultipartContent(HttpServletRequest request) {
    	return ServletFileUpload.isMultipartContent(request);
    	//return FileUpload.isMultipartContent(request);
    }

    public static final String ATTRIBUTE_CARRYOVER = "simple.servlet.SimpleServlet.carryover";
	public static final String ATTRIBUTE_PARAMETERS = "simple.servlet.SimpleServlet.parameters";

	protected static class CarryOver {
		protected final Map<String, Object> parameters = new HashMap<String, Object>();
		protected final Map<String, Object> attributes = new HashMap<String, Object>();
    }

	protected static CarryOver getCarryOver(HttpSession session) {
		Object co = session.getAttribute(ATTRIBUTE_CARRYOVER);
		CarryOver carryOver;
		if(co instanceof CarryOver) {
			carryOver = (CarryOver) co;
		} else {
			carryOver = new CarryOver();
			session.setAttribute(ATTRIBUTE_CARRYOVER, carryOver);
		}
		return carryOver;
	}

	public static void storeAllParamsForNextRequest(HttpServletRequest request) {
		InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
		Map<String, ? extends Object> values;
		if(form != null)
			values = form.getParameters();
		else
			values = request.getParameterMap();
		getCarryOver(request.getSession()).parameters.putAll(values);
	};

    public static void storeForNextRequest(HttpSession session, String name, Object value) {
		getCarryOver(session).attributes.put(name, value);
    }

	public static void storeForNextRequest(HttpSession session, Map<String, ? extends Object> values) {
		getCarryOver(session).attributes.putAll(values);
	}

	public static void redirectWithCarryOver(HttpServletRequest request, HttpServletResponse response, String location) throws IOException {
		redirectWithCarryOver(request, response, location, false);
	}

	public static void redirectWithCarryOver(HttpServletRequest request, HttpServletResponse response, String location, boolean overrideSessionToken) throws IOException {
		redirectWithCarryOver(request, response, location, overrideSessionToken, false);
	}

	public static void redirectWithCarryOver(HttpServletRequest request, HttpServletResponse response, String location, boolean overrideSessionToken, boolean storeAllParams) throws IOException {
		if(storeAllParams)
			storeAllParamsForNextRequest(request);
		Object obj = getMessagesSource(request);
		if(obj != null)
			storeForNextRequest(request.getSession(), ATTRIBUTE_MESSAGES, obj);
		if(overrideSessionToken) {
			String sessionToken = getSessionTokenIfPresent(request);
			if(sessionToken != null)
				storeForNextRequest(request.getSession(), SimpleServlet.ATTRIBUTE_SESSION_TOKEN, sessionToken);
		}
    	response.sendRedirect(location);
    }

    /** Returns a Map of all the parameters in a request
     * @return
     * @param request
     * @throws FileUploadException
     */
	public static Map<String, Object> getRequestParameters(ServletRequest request, Set<String> restricted, Collection<String> masked, FileUpload upload) throws FileUploadException {
		Map<String, Object> parameters = (Map<String, Object>) request.getAttribute(ATTRIBUTE_PARAMETERS);
		if(parameters == null) {
			parameters = new HashMap<String, Object>();
			HttpServletRequest httpRequest;
			if(request instanceof HttpServletRequest)
				httpRequest = (HttpServletRequest) request;
			else
				httpRequest = null;
			if(httpRequest != null) {
				HttpSession session = httpRequest.getSession(false);
				if(session != null) {
					Object co = session.getAttribute(ATTRIBUTE_CARRYOVER);
					if(co instanceof CarryOver) {
						CarryOver carryOver = (CarryOver) co;
						parameters.putAll(carryOver.parameters);
						carryOver.parameters.clear();
					}
				}
			}

			if(httpRequest != null && httpRequest.getMethod().equalsIgnoreCase("POST") && isMultipartContent(httpRequest)) {
				if(httpRequest.getAttribute(SimpleServlet.ATTRIBUTE_FORM) != null) { // It's a forward or include so merge parameters with the request having preference over the multi-part post
					addParametersFromMultipartPost(httpRequest, restricted, masked, upload, parameters);
					addParametersFromRequest(request, restricted, masked, parameters);
				} else {
					addParametersFromMultipartPost(httpRequest, restricted, masked, upload, parameters);
				}
			} else
				addParametersFromRequest(request, restricted, masked, parameters);
			parameters = Collections.unmodifiableMap(parameters);
			request.setAttribute(ATTRIBUTE_PARAMETERS, parameters);
		}
		return parameters;
    }

	public static void carryOverAttributes(HttpServletRequest request, Set<String> restricted) {
		HttpSession session = request.getSession(false);
		if(session != null) {
			Object co = session.getAttribute(ATTRIBUTE_CARRYOVER);
			if(co instanceof CarryOver) {
				CarryOver carryOver = (CarryOver) co;
				for(Iterator<Map.Entry<String, Object>> iter = carryOver.attributes.entrySet().iterator(); iter.hasNext();) {
					Map.Entry<String, Object> entry = iter.next();
					if(!containsNestedName(entry.getKey(), restricted))
						request.setAttribute(entry.getKey(), entry.getValue());
					iter.remove();
				}
			}
		}
	}

	protected static void addParametersFromRequest(ServletRequest request, Set<String> restricted, Collection<String> masked, Map<String, Object> parameters) {
    	for(Enumeration<?> en = request.getParameterNames(); en.hasMoreElements(); ) {
            String name = en.nextElement().toString();
            if(!containsNestedName(name, restricted)) {
            	String[] values =  request.getParameterValues(name);
                if(values != null) {
                    switch(values.length) {
                        case 0: break;
                        case 1:
                        	if(masked.contains(name))
                        		parameters.put(name, new MaskedString(values[0]));
                        	else
                        		parameters.put(name, values[0]);
                        	break;
                        default:
                        	if(masked.contains(name)) {
                        		MaskedString[] arr = new MaskedString[values.length];
                        		for(int i = 0; i < arr.length; i++)
                        			arr[i] = new MaskedString(values[i]);
                        		parameters.put(name, arr);
                        	} else
                        		parameters.put(name, values);
                    }
                }
            }
        }
    }

	protected static void addParametersFromMultipartPost(HttpServletRequest httpRequest, Set<String> restricted, Collection<String> masked, FileUpload upload, Map<String, Object> parameters) throws FileUploadException {
    	for(Iterator<?> iter = upload.parseRequest(new ServletRequestContext(httpRequest)).iterator(); iter.hasNext(); ) {
    		//for(Iterator iter = upload.parseRequest(httpRequest).iterator(); iter.hasNext(); ) {
            FileItem item = (FileItem)iter.next();
            String name = item.getFieldName();
            if(!containsNestedName(name, restricted)) {
            	if(!item.isFormField()) {
                    InputFile file = new InputFile(item);
					// value = value.replace((char)160,' ').trim();
					Object oldValue = parameters.get(name);
					if(oldValue == null) {
						parameters.put(name, file);
					} else if(oldValue instanceof InputFile) {
						InputFile[] arr = new InputFile[2];
						arr[0] = (InputFile) oldValue;
						arr[1] = file;
						parameters.put(name, arr);
					} else if(oldValue instanceof InputFile[]) {
						int len = ((InputFile[]) oldValue).length;
						InputFile[] arr = new InputFile[len + 1];
						System.arraycopy(oldValue, 0, arr, 0, len);
						arr[len] = file;
						parameters.put(name, arr);
					} else { // what should we do here?
						parameters.put(name, file);
					}
                } else {
                    String value = null;
                    if(httpRequest.getCharacterEncoding() == null)
                    	value = item.getString();
                    else
                        try {
                            value = item.getString(httpRequest.getCharacterEncoding());
                        } catch (UnsupportedEncodingException e) {
                            value = item.getString();
                        }
                    if(value != null) {
                    	if(masked.contains(name)) {
                    		Object oldValue = parameters.get(name);
                            if(oldValue == null) {
                            	parameters.put(name, new MaskedString(value));
                            } else if(oldValue instanceof MaskedString) {
                            	MaskedString[] arr = new MaskedString[2];
                                arr[0] = (MaskedString) oldValue;
                                arr[1] = new MaskedString(value);
                                parameters.put(name, arr);
                            } else if(oldValue instanceof MaskedString[]) {
                                int len = ((MaskedString[])oldValue).length;
                                MaskedString[] arr = new MaskedString[len + 1];
                                System.arraycopy(oldValue, 0, arr, 0, len);
                                arr[len] = new MaskedString(value);
                                parameters.put(name, arr);
                            } else { //what should we do here?
                            	parameters.put(name, new MaskedString(value));
                            }
                    	} else {
                        	//value = value.replace((char)160,' ').trim();
                        	Object oldValue = parameters.get(name);
                            if(oldValue == null) {
                            	parameters.put(name, value);
                            } else if(oldValue instanceof String) {
                                String[] arr = new String[2];
                                arr[0] = (String) oldValue;
                                arr[1] = value;
                                parameters.put(name, arr);
                            } else if(oldValue instanceof String[]) {
                                int len = ((String[])oldValue).length;
                                String[] arr = new String[len + 1];
                                System.arraycopy(oldValue, 0, arr, 0, len);
                                arr[len] = value;
                                parameters.put(name, arr);
                            } else { //what should we do here?
                            	parameters.put(name, value);
                            }
                    	}
                    }
                }
            }
    	}
    }
    /** Returns the specified attribute from the specified scope from the page context object
     * @pageContext The page context object from which to retrieve the attribute
     * @param key The attribute name
     * @param scope The scope in which to look ("request", "session", "application", "page" or null for ANY
     * scope)
     * @return The attribute value
     */
    public static Object getPageContextAttribute(PageContext pageContext, String key, String scope) throws IllegalArgumentException {
        if(scope == null) {
            Object o = null;
            for(Iterator<AttributeMgr> iter = pageContextAttrMgrs.values().iterator(); o == null && iter.hasNext(); )
                o = getAttribute(iter.next(), pageContext, key);
            return o;
        } else {
            AttributeMgr am = pageContextAttrMgrs.get(scope.toLowerCase());
            if(am == null) throw new IllegalArgumentException("Invalid scope '" + scope + "'");
            return getAttribute(am, pageContext, key);
        }
    }

    /** Returns the specified attribute from the specified scope from the request object
     * @request The request object from which to retrieve the attribute
     * @param key The attribute name
     * @param scope The scope in which to look ("request", "session", "application" or null for ANY
     * scope)
     * @return The attribute value
     */
    public static Object getRequestAttribute(ServletRequest request, String key, String scope) throws IllegalArgumentException {
        if(scope == null) {
            Object o = null;
            for(Iterator<AttributeMgr> iter = requestAttrMgrs.values().iterator(); o == null && iter.hasNext(); )
                o = getAttribute(iter.next(), request, key);
            return o;
        } else {
            AttributeMgr am = requestAttrMgrs.get(scope.toLowerCase());
            if(am == null) throw new IllegalArgumentException("Invalid scope '" + scope + "'");
            return getAttribute(am, request, key);
        }
    }

    protected static Object getAttribute(AttributeMgr am, Object base, String key) {
        if(key == null || key.trim().length() == 0) return base;
        Object bean = am.getAttribute(base, key);
        if(bean != null) return bean;
        try {
	        NestedNameTokenizer.Token[] tokens = new NestedNameTokenizer(key).tokenize();
	        for(int i = tokens.length - 1; i >= 0; i--) {
	            bean = am.getAttribute(base, key.substring(0,tokens[i].getNextPos()-1));
	            if(bean != null) return i == tokens.length - 1 ? bean : ReflectionUtils.getProperty(bean, key.substring(tokens[i].getNextPos()), true);
	            if(tokens[i].isIndexed()) {
	                bean = am.getAttribute(base, key.substring(0,tokens[i].getEndPos()));
	                if(bean != null) {
	                    bean = ReflectionUtils.getIndexedProperty(bean, null, tokens[i].getIndex(), true);
	                    return i == tokens.length - 1 ? bean : ReflectionUtils.getProperty(bean, key.substring(tokens[i].getNextPos()), true);
	                }
	            } else if(tokens[i].isMapped()) {
	                bean = am.getAttribute(base, key.substring(0,tokens[i].getEndPos()));
	                if(bean != null) {
	                    bean = ReflectionUtils.getMappedProperty(bean, null, tokens[i].getKey(), true);
	                    return i == tokens.length - 1 ? bean : ReflectionUtils.getProperty(bean, key.substring(tokens[i].getNextPos()), true);
	                }
	            }
	        }
        } catch(ParseException e) {
        } catch(IntrospectionException e) {
		} catch(IllegalAccessException e) {
		} catch(InvocationTargetException e) {
		}
        return null;
    }

    /** Returns the Object represented by the given name. Uses the NestedNameTokenizer
     * to split the given name into property parts and gets the value of the
     * property represented by each part. For example, "subType.code" would return
     * the value of the code property on the subType bean in the request's
     * attributes.
     *
     * @return The object in the attribute represented by the given name
     * @param pageContext The PageContext whose attributes are searched for the object
     * @param name The name of the attribute and/or its sub-properties to retrieve
     * @param required Whether to throw an exception if the attribute is not found
     * @throws ServletException If an exception occurs
     */
    public static Object getAttribute(PageContext pageContext, String name, boolean required) throws ServletException {
        return getAttribute(pageContext, name, required, null);
    }

    public static Object getAttributeSafely(PageContext pageContext, String name) {
        return getAttributeSafely(pageContext, name, null);
    }

    /** Returns the Object represented by the given name. Uses the NestedNameTokenizer
     * to split the given name into property parts and gets the value of the
     * property represented by each part. For example, "subType.code" would return
     * the value of the code property on the subType bean in the request's
     * attributes.
     *
     * @return The object in the attribute represented by the given name
     * @param pageContext The PageContext whose attributes are searched for the object
     * @param name The name of the attribute and/or its sub-properties to retrieve
     * @param required Whether to throw an exception if the attribute is not found
     * @param scope Whether to search the application, session, request or all three
     * for the attribute
     * @throws ServletException If required is true and the object returned is null
     */
    public static Object getAttribute(PageContext pageContext, String name, boolean required, String scope) throws ServletException {
        Object o = getPageContextAttribute(pageContext, name, scope);
        if(o == null && required) throw new ServletException("Required attribute '" + name + "' not found");
        return o;
    }

    public static Object getAttributeSafely(PageContext pageContext, String name, String scope) {
        try {
			return getAttribute(pageContext, name, false, scope);
		} catch(ServletException e) {
			return null;
		}
    }
    
    /** Returns the Object represented by the given name. Uses the NestedNameTokenizer
     * to split the given name into property parts and gets the value of the
     * property represented by each part. For example, "subType.code" would return
     * the value of the code property on the subType bean in the request's
     * attributes.
     *
     * @return The object in the attribute represented by the given name
     * @param request The ServletRequest whose attributes are searched for the object
     * @param name The name of the attribute and/or its sub-properties to retrieve
     * @param required Whether to throw an exception if the attribute is not found
     * @throws ServletException If required is true and the object returned is null
     */
    public static Object getAttribute(ServletRequest request, String name, boolean required) throws ServletException {
        return getAttribute(request, name, required, null);
    }

    /** Returns the Object represented by the given name. Uses the NestedNameTokenizer
     * to split the given name into property parts and gets the value of the
     * property represented by each part. For example, "subType.code" would return
     * the value of the code property on the subType bean in the request's
     * attributes.
     *
     * @return The object in the attribute represented by the given name
     * @param request The ServletRequest whose attributes are searched for the object
     * @param name The name of the attribute and/or its sub-properties to retrieve
     * @param required Whether to throw an exception if the attribute is not found
     * @throws ServletException If required is true and the object returned is null
     * @throws ConvertException
     */
    public static <T> T getAttribute(ServletRequest request, String name, Class<T> type, boolean required) throws ServletException, ConvertException {
        return ConvertUtils.convert(type, getAttribute(request, name, required, null));
    }
    /** Returns the Object represented by the given name. Uses the NestedNameTokenizer
     * to split the given name into property parts and gets the value of the
     * property represented by each part. For example, "subType.code" would return
     * the value of the code property on the subType bean in the request's
     * attributes.
     *
     * @return The object in the attribute represented by the given name
     * @param request The ServletRequest whose attributes are searched for the object
     * @param name The name of the attribute and/or its sub-properties to retrieve
     * @param required Whether to throw an exception if the attribute is not found
     * @param scope Whether to search the application, session, request or all three
     * for the attribute
     * @throws ServletException If required is true and the object returned is null
     */
    public static Object getAttribute(ServletRequest request, String name, boolean required, String scope) throws ServletException {
        Object o = getRequestAttribute(request, name, scope);
        if(o == null && required) throw new ServletException("Required attribute '" + name + "' not found");
        return o;
    }

    /** Returns the Object represented by the given name. Uses the NestedNameTokenizer
     * to split the given name into property parts and gets the value of the
     * property represented by each part. For example, "subType.code" would return
     * the value of the code property on the subType bean in the request's
     * attributes.
     *
     * @return The object in the attribute represented by the given name
     * @param request The ServletRequest whose attributes are searched for the object
     * @param name The name of the attribute and/or its sub-properties to retrieve
     * @param required Whether to throw an exception if the attribute is not found
     * @param scope Whether to search the application, session, request or all three
     * for the attribute
     * @throws ServletException If required is true and the object returned is null
     * @throws ConvertException
     */
    public static <T> T getAttribute(ServletRequest request, String name, Class<T> type, boolean required, String scope) throws ServletException, ConvertException {
        return ConvertUtils.convert(type, getAttribute(request, name, required, scope));
    }

    /** Sets the attribute with the specified name in the given object.
     *
     * @param request The ServletRequest whose attribute is set
     * @param name The name of the attribute to set
     * @param value The new value of the attribute
     * @param scope Whether to set the attribute in the application, session, or request scope
     * @throws ServletException If required is true and the object returned is null
     */
    public static void setAttribute(ServletRequest request, String name, Object value, String scope) throws IllegalArgumentException {
        AttributeMgr am = requestAttrMgrs.get(scope.toLowerCase());
        if(am == null) throw new IllegalArgumentException("Invalid scope '" + scope + "'");
        am.setAttribute(request, name, value);
    }

    /** Sets the attribute with the specified name in the given object.
     *
     * @param pageContext The PageContext whose attribute is set
     * @param name The name of the attribute to set
     * @param value The new value of the attribute
     * @param scope Whether to set the attribute in the application, session, or request scope
     * @throws ServletException If an exception occurs
     */
    public static void setAttribute(PageContext pageContext, String name, Object value, String scope) throws IllegalArgumentException {
        AttributeMgr am = pageContextAttrMgrs.get(scope.toLowerCase());
        if(am == null) throw new IllegalArgumentException("Invalid scope '" + scope + "'");
        am.setAttribute(pageContext, name, value);
    }

    /** Sets the attribute with the specified name if it is null in the given object.
    *
     * @param request The ServletRequest whose attribute is set
     * @param name The name of the attribute to set
     * @param value The new value of the attribute
     * @param scope Whether to set the attribute in the application, session, or request scope
     * @return The new value of the attribute
     * @throws ServletException If required is true and the object returned is null
     */
    public static Object setAttributeIfNull(ServletRequest request, String name, Object value, String scope) throws IllegalArgumentException {
        AttributeMgr am = requestAttrMgrs.get(scope.toLowerCase());
        if(am == null) throw new IllegalArgumentException("Invalid scope '" + scope + "'");
        return am.setAttributeIfNull(request, name, value);
    }
    /** Removes any occurrence of the specified attribute from any scope in the request
     * @request The request object from which to remove the attribute
     * @param key The attribute name
     */
    public static void removeAttribute(ServletRequest request, String key) {
        for(Iterator<AttributeMgr> iter = requestAttrMgrs.values().iterator(); iter.hasNext(); ) {
            iter.next().setAttribute(request, key, null);
        }
    }

	public static <T> T getAttributeDefault(ServletRequest request, String name, Class<T> type, T defaultValue) throws ServletException, ConvertException {
		return ConvertUtils.convertDefault(type, getAttribute(request, name, false, null), defaultValue);
	}

    /** Prints the rootCause's (or the actually exception's) stack trace of a
     * ServletException into a String and returns that String
     * @return
     * @param e
     */
    public static String exceptionToString(ServletException e) {
        return exceptionToString(e.getRootCause() != null ? e.getRootCause() : e);
    }

    /** Prints the stack trace of an exception into a String and returns
     * that String
     * @return
     * @param e
     */
    public static String exceptionToString(Throwable e) {
        java.io.StringWriter sw = new java.io.StringWriter();
	sw.write(e.getMessage());
        sw.write(":\n");
        java.io.PrintWriter pw = new java.io.PrintWriter(sw, true);
	e.printStackTrace(pw);
	return sw.toString();
    }

    public static <T> T getOrCreateAttribute(ServletRequest request, String name, Class<T> type, String scope) throws ServletException, ConvertException, InstantiationException, IllegalAccessException {
        Object o = getAttribute(request, name, false, scope);
        T attr = ConvertUtils.convert(type, o);
        if(attr == null) {
            //create object
            attr = type.newInstance();
            o = setAttributeIfNull(request, name, attr, scope);
            attr = ConvertUtils.convert(type, o);
        }
        return attr;
    }

	@Deprecated
    public static Locale getLocale(InputForm form) throws ConvertException {
        String lang = form.getHeader("accept-language");
        if(lang != null) lang = lang.split("[,]", 2)[0];
        return ConvertUtils.convert(Locale.class, lang);
    }
    public static Locale getLocale(HttpServletRequest request) throws ConvertException {
        String lang = request.getHeader("accept-language");
        if(lang != null) lang = lang.split("[,]", 2)[0];
        return ConvertUtils.convert(Locale.class, lang);
    }

	protected static float FLOAT_ONE = 1.0f;
	protected static char[] ACCEPT_SPECIAL_CHARS = "=,;\"".toCharArray();
	protected static class AcceptEntry implements Comparable<AcceptEntry> {
		protected final float weight;
		protected final int order;
		protected final String full;
		protected final String major;
		protected final String minor;
		protected final SortedMap<String, String> params = new TreeMap<>(new CaseInsensitiveComparator<String>());

		public AcceptEntry(String contentTypeString, int order) throws ParseException {
			int pos = contentTypeString.indexOf('/');
			if(pos < 0)
				throw new ParseException("No '/' found in content type string", contentTypeString.length());
			String tmp = contentTypeString.substring(0, pos);
			if(tmp.equals("*"))
				major = null;
			else
				major = tmp;
			int start = pos + 1;
			pos = contentTypeString.indexOf(';', start);
			if(pos < 0)
				pos = contentTypeString.length();
			tmp = contentTypeString.substring(start, pos);
			if(tmp.equals("*"))
				minor = null;
			else
				minor = tmp;
			if(pos < contentTypeString.length()) {
				StringUtils.intoMap(params, contentTypeString.substring(pos + 1), '=', ';', new char[] { '"' }, true, false);
			}
			this.order = order;
			String q = this.params.remove("q");
			if(q == null) {
				this.full = contentTypeString;
				this.weight = FLOAT_ONE;
			} else if(StringUtils.isBlank(q)) {
				this.full = constructFull();
				this.weight = FLOAT_ONE;
			} else {
				this.full = constructFull();
				Float f = ConvertUtils.convertSafely(Float.class, q, FLOAT_ONE);
				if(f > FLOAT_ONE || f < 0.0f)
					f = FLOAT_ONE;
				this.weight = f;
			}
		}

		protected String constructFull() {
			StringBuilder sb = new StringBuilder();
			if(major == null)
				sb.append('*');
			else
				sb.append(major);
			sb.append('/');
			if(minor == null)
				sb.append('*');
			else
				sb.append(minor);
			if(!params.isEmpty()) {
				for(Map.Entry<String, String> entry : params.entrySet()) {
					sb.append(';');
					if(StringUtils.indexOf(entry.getKey(), ACCEPT_SPECIAL_CHARS) >= 0)
						sb.append('"').append(entry.getKey().replace("\"", "\"\"")).append('"');
					else
						sb.append(entry.getKey());
					sb.append('=');
					if(StringUtils.indexOf(entry.getValue(), ACCEPT_SPECIAL_CHARS) >= 0)
						sb.append('"').append(entry.getValue().replace("\"", "\"\"")).append('"');
					else
						sb.append(entry.getValue());
				}
			}
			return sb.toString();
		}
		public boolean matches(AcceptEntry contentType) {
			if(full.equalsIgnoreCase(contentType.full))
				return true;
			if(major != null && !major.equalsIgnoreCase(contentType.major))
				return false;
			if(minor != null && !minor.equalsIgnoreCase(contentType.minor))
				return false;
			if(params.isEmpty())
				return true;
			if(contentType.params.entrySet().containsAll(params.entrySet()))
				return true;
			return false;
		}

		public int compareSpecificity(AcceptEntry o) {
			if(major != null && o.major == null)
				return -1;
			if(major == null && o.major != null)
				return 1;
			if(minor != null && o.minor == null)
				return -1;
			if(minor == null && o.minor != null)
				return 1;
			int i = o.params.size() - params.size();
			if(i < 0)
				return -1;
			if(i > 0)
				return 1;
			return 0;
		}

		public String getFull() {
			return full;
		}

		@Override
		public int compareTo(AcceptEntry o) {
			if(weight > o.weight)
				return -1;
			if(weight < o.weight)
				return 1;
			int i = compareSpecificity(o);
			if(i != 0)
				return i;
			if(order < o.order)
				return -1;
			if(order > o.order)
				return 1;

			return 0;
		}

		@Override
		public String toString() {
			return full;
		}
	}

	public static String[] getPreferredContentType(HttpServletRequest request, String... potentials) {
		return getPreferredContentType(request.getHeader("accept"));
	}

	public static String[] getPreferredContentType(String accept, String... potentials) {
		if(StringUtils.isBlank(accept))
        	return null;
		String[] entries = StringUtils.split(accept, ',', '"', true);
		List<AcceptEntry> accepts = new ArrayList<>(entries.length);
		for(int i = 0; i < entries.length; i++)
			try {
				accepts.add(new AcceptEntry(entries[i], i));
			} catch(ParseException e) {
				log.warn("Invalid accept entry '" + entries[i] + "'; skipping...");
				continue;
			}
		Collections.sort(accepts);
		if(potentials == null || potentials.length == 0) {
			String[] result = new String[accepts.size()];
			for(int i = 0; i < result.length; i++)
				result[i] = accepts.get(i).getFull();
			return result;
		}
		List<AcceptEntry> pots = new ArrayList<>(potentials.length);
		for(int i = 0; i < potentials.length; i++) {
			try {
				pots.add(new AcceptEntry(potentials[i], i));
			} catch(ParseException e) {
				log.warn("Invalid potential '" + potentials[i] + "'; skipping...");
				continue;
			}
		}
		Collections.sort(pots);
		List<String> result = new ArrayList<>(pots.size());
		OUTER: for(AcceptEntry ae : accepts) {
			Iterator<AcceptEntry> iter = pots.iterator();
			while(iter.hasNext()) {
				AcceptEntry ct = iter.next();
				if(ae.matches(ct)) {
					result.add(ct.getFull());
					iter.remove();
					if(pots.isEmpty())
						break OUTER;
				}
			}
		}
		return result.toArray(new String[result.size()]);
    }
	protected static final Pattern clientTimeZonePattern = Pattern.compile("^.* \\(([^)]+)\\)$");

	public static TimeZone getTimeZone(InputForm form) throws ServletException {
		String clientTimestamp = form.getString("clientTimestamp", false);
		if(clientTimestamp != null) {
			int clientTimezoneOffsetMinutes = form.getInt("clientTimezoneOffset", true, 0);
			Matcher matcher = clientTimeZonePattern.matcher(clientTimestamp);
			if(matcher.find()) {
				String tzText = matcher.group(1);
				Locale locale = form.getLocale();
				return TimeUtils.findTimeZone(tzText, -60 * 1000 * clientTimezoneOffsetMinutes, locale);
			}
		}
		return null;
	}

	public static CalendarHelper getCalendarHelper(HttpServletRequest request, String beginDateAttributeName, String endDateAttributeName) throws ConvertException, ServletException {
		return getCalendarHelper(request, beginDateAttributeName, endDateAttributeName, null);
	}

	public static CalendarHelper getCalendarHelper(HttpServletRequest request, String beginDateAttributeName, String endDateAttributeName, String minDateAttributeName) throws ConvertException, ServletException {
		return new CalendarHelper(getLocale(request), ConvertUtils.convert(Calendar.class, getAttribute(request, beginDateAttributeName, false)), ConvertUtils.convert(Calendar.class, getAttribute(request, endDateAttributeName, false)), StringUtils.isBlank(minDateAttributeName) ? null : ConvertUtils.convert(Calendar.class, getAttribute(request, minDateAttributeName, false)));
    }

	@Deprecated
	public static Translator getTranslator(InputForm form) {
		return getTranslator((HttpServletRequest) form.getRequest());
	}

	public static Translator getTranslator(HttpServletRequest request) {
		Translator translator = (Translator) getRequestAttribute(request, "simple.util.Translator", null);
		if(translator == null) {
			final String host = request.getServerName();
			final String[] dirs = StringUtils.split(request.getRequestURI(), DIRECTORY_SEPARATORS, true);
			final String context;
			if(dirs != null && dirs.length > 1) {
				int len = dirs.length - 1;
				for(int i = 0; i < len; i++) {
					if(StringUtils.isBlank(dirs[i]) || dirs[i].equals(".")) {
						System.arraycopy(dirs, i + 1, dirs, i, len - i - 1);
						len--;
						i--;
					} else if(dirs[i].equals("..")) {
						if(i == 0) {
							System.arraycopy(dirs, i + 2, dirs, i, len - i - 1);
						} else {
							System.arraycopy(dirs, i + 1, dirs, i - 1, len - i - 1);
							i -= 2;
						}
						len -= 2;
					}
				}
				if(len > 0) {
					StringBuilder sb = new StringBuilder().append(host).append('/');
					StringUtils.join(dirs, "/", 0, len, null, sb);
					context = sb.toString();
				} else
					context = host;
			} else
				context = host;
			Locale locale;
			try {
				locale = getLocale(request);
			} catch(ConvertException e) {
				locale = null;
			}
			StringBuilder sb = new StringBuilder();
			sb.append("simple.util.Translator(").append(context);
			if(locale != null)
				sb.append(',').append(locale);
			sb.append(')');
			String key = sb.toString();
			translator = (Translator) getRequestAttribute(request, key, null);
			if(translator == null) {
				try {
					translator = TranslatorFactory.getDefaultFactory().getTranslator(locale, context);
				} catch(ServiceException e) {
					log.warn("Could not get translator", e);
					translator = DefaultTranslatorFactory.getTranslatorInstance();
				}
				setAttribute(request, key, translator, "session");
			}
			setAttribute(request, "simple.util.Translator", translator, "request");
		}
        return translator;
    }
    
    public static ServletUser getUser(HttpServletRequest request) {
    	return(ServletUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
    }

	public static ServletUser getUser(InputForm form) {
		return (ServletUser) form.getAttribute(SimpleServlet.ATTRIBUTE_USER, SESSION_SCOPE);
	}

	public static MessagesInputSource getOrCreateMessagesSource(InputForm form) {
		Object obj = form.getAttribute(ATTRIBUTE_MESSAGES);
		if(obj instanceof MessagesInputSource) {
			return (MessagesInputSource) obj;
		} else {
			MessagesInputSource mis = new MessagesInputSource(form.getTranslator());
			form.setAttribute(ATTRIBUTE_MESSAGES, mis);
			return mis;
		}
	}

	public static MessagesInputSource getMessagesSource(InputForm form) {
		Object obj = form.getAttribute(ATTRIBUTE_MESSAGES);
		if(obj instanceof MessagesInputSource) {
			return (MessagesInputSource) obj;
		}
		return null;
	}

	public static MessagesInputSource getMessagesSource(HttpServletRequest request) {
		Object obj = getRequestAttribute(request, ATTRIBUTE_MESSAGES, REQUEST_SCOPE);
		if(obj instanceof MessagesInputSource) {
			return (MessagesInputSource) obj;
		}
		return null;
	}

	public static MessagesInputSource getOrCreateMessagesSource(HttpServletRequest request) {
		Object obj = getRequestAttribute(request, ATTRIBUTE_MESSAGES, REQUEST_SCOPE);
		if(obj instanceof MessagesInputSource) {
			return (MessagesInputSource) obj;
		} else {
			MessagesInputSource mis = new MessagesInputSource(getTranslator(request));
			request.setAttribute(ATTRIBUTE_MESSAGES, mis);
			return mis;
		}
	}
	public static List<MessagesInputSource.Message> getMessages(HttpServletRequest request) {
		Object obj = getRequestAttribute(request, ATTRIBUTE_MESSAGES, REQUEST_SCOPE);
		if(obj instanceof MessagesInputSource) {
			return ((MessagesInputSource) obj).getMessages();
		}
		return Collections.emptyList();
	}

	protected static class HtmlEntityLoader {
		protected static Map<String, Character> entities = loadEntities();

		protected static Map<String, Character> loadEntities() {
			Properties props = new Properties();
			try {
				ClassLoader cl = Thread.currentThread().getContextClassLoader();
				if(cl == null) {
					cl = RequestUtils.class.getClassLoader();
					if(cl == null)
						cl = ClassLoader.getSystemClassLoader();
				}
				InputStream in = cl.getResourceAsStream("simple/xml/serializer/HTMLEntities.properties");
				if(in == null)
					throw new IOException("File 'simple/xml/serializer/HTMLEntities.properties' not found");
				try {
					props.load(in);
				} finally {
					in.close();
				}
			} catch(IOException e) {
				log.warn("Could not load entities", e);
				props.setProperty("quot", "34");
				props.setProperty("amp", "38");
				props.setProperty("lt", "60");
				props.setProperty("gt", "62");
				props.setProperty("nbsp", "160");
			}
			Map<String, Character> entities = new LinkedHashMap<>();
			for(Map.Entry<Object, Object> entry : props.entrySet())
				entities.put(entry.getKey().toString(), (char) Integer.parseInt(entry.getValue().toString()));
			return entities;
		}
	}

	public static void writeTextResultFromMessages(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/plain");
		response.getWriter().append(getTextResultFromMessages(request));
	}

	public static String getTextResultFromMessages(HttpServletRequest request) {
		List<MessagesInputSource.Message> messages = getMessages(request);
		StringBuilder sb = new StringBuilder();
		String result = null;
		if(messages != null && !messages.isEmpty()) {
			for(MessagesInputSource.Message message : messages) {
				String type = message.getType();
				if(type != null) {
					switch(type.toLowerCase()) {
						case "error":
						case "failure":
						case "fail":
							if(result != null && !result.equals("ERROR\n"))
								result = "PARTIAL\n";
							else
								result = "ERROR\n";
							break;
						case "success":
							if(result != null && !result.equals("SUCCESS\n"))
								result = "PARTIAL\n";
							else
								result = "SUCCESS\n";
							break;
					}
					sb.append(type.toUpperCase()).append(": ");
				}

				appendTextResponse(sb, message.toHtml());
				sb.append('\n');
			}
		}
		if(result == null)
			result = "OK\n";
		sb.insert(0, result);
		return sb.toString();
	}

	public static void writeTextResponse(HttpServletResponse response, String type, String htmlMessage) throws IOException {
		PrintWriter out = response.getWriter();
		switch(type.toLowerCase()) {
			case "error":
			case "failure":
			case "fail":
				out.append("ERROR\n");
				break;
			case "success":
				out.append("SUCCESS\n");
				break;
			default:
				out.append("OK\n");
		}
		appendTextResponse(out, htmlMessage);
	}

	public static StringBuilder appendTextResponse(StringBuilder appendTo, String html) {
		try {
			appendTextResponse((Appendable) appendTo, html);
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e);
		}
		return appendTo;
	}

	public static Appendable appendTextResponse(Appendable appendTo, String html) throws IOException {
		if(html != null)
			OUTER: for(int i = 0; i < html.length(); i++) {
				// find < or '&'
				char ch = html.charAt(i);
				switch(ch) {
					case '<':
						if(html.regionMatches(i + 1, "![CDATA[", 0, 8)) {
							// CDATA section - find end and append all in between
							int start = i + 9;
							int end = html.indexOf("]]>", start);
							if(end < 0)
								end = html.length();
							for(i = start; i < end; i++) {
								ch = html.charAt(i);
								switch(Character.getType(ch)) {
									case Character.CONTROL:
									case Character.UNASSIGNED:
										appendTo.append(' ');
										break;
									default:
										appendTo.append(ch);
								}
							}
							i = end + 2;
						} else {
							int end = html.indexOf('>', i + 1);
							if(end < 0)
								i = html.length();
							else
								i = end;
						}
						continue OUTER;
					case '&':
						i++;
						ch = html.charAt(i);
						int start = i;
						if(ch == '#') {
							i++;
							int num = 0;
							for(; i < html.length(); i++) {
								ch = html.charAt(i);
								if(ch == ';') {
									if(num <= 0) {
										// invalid but ignore
										appendTo.append(html, start, i + 1);
										continue OUTER;
									} else {
										ch = (char) num;
									}
									break;
								} else if(Character.isDigit(ch)) {
									num = num * 10 + (ch - '0');
								} else {
									// invalid but ignore
									appendTo.append(html, start, i);
									break;
								}
							}
						} else if(Character.isLetterOrDigit(ch)) {
							i++;
							for(; i < html.length(); i++) {
								ch = html.charAt(i);
								if(ch == ';') {
									String entity = html.substring(start + 1, i);
									Character ech = HtmlEntityLoader.entities.get(entity);
									if(ech == null) {
										// invalid but ignore
										appendTo.append(html, start, i + 1);
										continue OUTER;
									} else
										ch = ech;
									break;
								} else if(!Character.isLetterOrDigit(ch)) {
									// invalid but ignore
									appendTo.append(html, start, i);
									break;
								}
							}
						} else {
							// invalid but ignore
							appendTo.append('&');
						}
						break;
				}
				switch(Character.getType(ch)) {
					case Character.CONTROL:
					case Character.UNASSIGNED:
						appendTo.append(' ');
						break;
					default:
						appendTo.append(ch);
				}
			}
		return appendTo;
	}

	public static boolean isXsrfProtectionEnabled(HttpServletRequest request) {
		XsrfProtectionLevel xpl = ConvertUtils.convertSafely(XsrfProtectionLevel.class, request.getSession().getServletContext().getAttribute(SimpleServlet.ATTRIBUTE_XSRF_PROTECTION), XsrfProtectionLevel.NOT_ENABLED);
		switch(xpl) {
			case ENABLED_FOR_ALL:
			case ENABLED_FOR_SOME:
				return true;
			default:
				return false;
		}
	}
    public static String getSessionToken(InputForm form) {
    	return getSessionToken((HttpServletRequest)form.getRequest());
    }
    public static String getSessionToken(HttpServletRequest request) {
    	String token = (String)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_SESSION_TOKEN);
    	if(token == null) {
    		if(getUser(request) != null) {
				token = newSessionToken(request);
    		}
    	}
    	return token;
    }

	public static String newSessionToken(HttpServletRequest request) {
		String token = Long.toString(sessionTokenGenerator.nextLong(), 36) + Long.toString(sessionTokenGenerator.nextLong(), 36);
		request.getSession().setAttribute(SimpleServlet.ATTRIBUTE_SESSION_TOKEN, token);
		return token;
	}
    public static String getSessionTokenIfPresent(InputForm form) {
    	return getSessionTokenIfPresent((HttpServletRequest)form.getRequest());
    }
    public static String getSessionTokenIfPresent(HttpServletRequest request) {
    	return (String)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_SESSION_TOKEN);
    }    
    protected static final TimeZone GMT = TimeZone.getTimeZone("GMT");
    protected static final String JAN011970 = "Thu, 01-Jan-1970 00:00:00 GMT";
    protected static final SimpleDateFormat simpleCookieDateFormat = new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss zzz");
    protected static final ThreadSafeDateFormat cookieDateFormat = new ThreadSafeDateFormat(simpleCookieDateFormat);
	static {
		simpleCookieDateFormat.setTimeZone(GMT);
	}
    
	public static void setHttpOnlyCookieHeader(HttpServletRequest request, HttpServletResponse response, Cookie cookie) {
		// suppress caching of the Set-Cookie header
		response.setHeader("Cache-Control", "no-cache=\"set-cookie\"");
		// create a cookie header instead of creating a Java cookie to force HttpOnly
        StringBuilder sb = new StringBuilder();
        if (cookie.getName() != null && cookie.getName().length() > 0)
        	sb.append(cookie.getName());
        if (cookie.getValue() != null)
        	sb.append('=').append(cookie.getValue());
        if (cookie.getVersion() > 0) {
        	sb.append(";Version=").append(cookie.getVersion());
        	if (cookie.getComment() != null)
        		sb.append(";Comment=\"").append(cookie.getComment()).append("\"");
        }
        if (cookie.getMaxAge() >= 0) {
            if (cookie.getVersion() == 0) {
            	sb.append(";Expires=");
            	if (cookie.getMaxAge() == 0)
            		sb.append(JAN011970);
            	else {
            		Calendar calendar = Calendar.getInstance(GMT);
            		calendar.setTimeInMillis(System.currentTimeMillis() + 1000 * cookie.getMaxAge());
            		sb.append(cookieDateFormat.format(calendar.getTime()));
            	}
            }
            else
            	sb.append(";Max-Age=").append(cookie.getMaxAge());
        }
        else if (cookie.getVersion() > 0) {
            sb.append(";Discard");
        }
        if (cookie.getDomain() != null)
            sb.append(";Domain=").append(cookie.getDomain());
        if (cookie.getPath() != null)
            sb.append(";Path=").append(cookie.getPath());
        if (cookie.getSecure() || "https".equalsIgnoreCase(request.getScheme()))
            sb.append(";Secure");
        sb.append(";HttpOnly");
        // prevent potential CRLF injection
        String secureCookie = sb.toString().replace("\r", "").replace("\n", "");
		response.addHeader("Set-Cookie", secureCookie);
    }    
    
    public static void removeCookies(HttpServletRequest request, HttpServletResponse response, String name) {
    	Cookie[] cookieArray = request.getCookies();
		if(cookieArray != null)
			for(Cookie c : cookieArray) {
				if(name.equals(c.getName())) {
					c.setMaxAge(0);
					setHttpOnlyCookieHeader(request, response, c);
				}
			}
    }

    protected static final Pattern uriMatchPattern = Pattern.compile("(?:(?:(?:\\w+\\:)?//([^/:]*)(?:\\:\\d+)?)?(/)?)?(.*)"); // groups: 1=server name; 2=/ or empty; 3=path
    protected static final char[] DIRECTORY_SEPARATORS = new char[] {'/', '\\'};

    protected static void appendDirectory(StringBuilder appendTo, String path) {
    	int pos = StringUtils.lastIndexOf(path, DIRECTORY_SEPARATORS);
		if(pos >= 0) appendTo.append(path,0,pos+1);
		else appendTo.append('/');
    }

    public static String addLastModifiedToUri(InputForm form, String base, String path) {
    	return addLastModifiedToUri((HttpServletRequest)form.getRequest(), base, path);
    }

    public static String addLastModifiedToUri(HttpServletRequest request, String base, String path) {
    	Matcher matcher = uriMatchPattern.matcher(path);
    	StringBuilder localPath = new StringBuilder();
    	if(matcher.matches()) {
    		String pathServerName = matcher.group(1);
    		boolean pathRelative = (matcher.group(2) == null);
    		String pathPath = matcher.group(3);
    		if(pathServerName == null ||pathServerName.length() == 0) {
    			//check base
    			if(base != null) {
    				matcher = uriMatchPattern.matcher(base);
    				if(!matcher.matches())
    					return path;
    				String baseServerName = matcher.group(1);
    	    		boolean baseRelative = (matcher.group(2) == null);
    	    		String basePath = matcher.group(3);
    	    		if(baseServerName != null && baseServerName.length() != 0 && !baseServerName.equalsIgnoreCase(request.getServerName())) {
    	    			return path;
    	    		}
    	    		if(pathRelative) {
    	    			if(baseRelative) {
    	    				localPath.append(request.getContextPath());
	    	    			if(request.getPathInfo() != null) {
	    	    				appendDirectory(localPath, request.getPathInfo());
	    	    			} else {
	    	    				localPath.append('/');
	    	    			}
	    	    		} else {
	    	    			localPath.append('/');
	    	    		}
    	    			appendDirectory(localPath, basePath);
    	    		}
    			} else if(pathRelative) {
    				localPath.append(request.getContextPath());
	    			if(request.getPathInfo() != null) {
	    				appendDirectory(localPath, request.getPathInfo());
	    			} else {
	    				localPath.append('/');
	    			}
    			} else {
    				localPath.append('/');
    			}
    		} else if(pathServerName.equalsIgnoreCase(request.getServerName())) {
    			localPath.append('/');
    		} else {
    			return path;
    		}
    		localPath.append(pathPath);
    		ServletContext servletContext = request.getSession().getServletContext();
    		String realPath;
    		if(StringUtils.startsWith(localPath, request.getContextPath())) {
    			realPath = servletContext.getRealPath(localPath.substring(request.getContextPath().length()));
    		} else {
    			ServletContext rootServletContext = servletContext.getContext("/");
    			if(rootServletContext == null)
    				return path;
    			realPath = rootServletContext.getRealPath(localPath.toString());
    		}
    		if(realPath == null)
    			return path;
    		LocalFileCache lfc = (LocalFileCache) servletContext.getAttribute("simple.servlet.RequestUtils.LocalFileCache");
    		if(lfc == null) {
    			synchronized(servletContext) {
    				lfc = (LocalFileCache) servletContext.getAttribute("simple.servlet.RequestUtils.LocalFileCache");
    				if(lfc == null) {
    					lfc = new LocalFileCache();
    					servletContext.setAttribute("simple.servlet.RequestUtils.LocalFileCache", lfc);
    				}
    			}
    		}
    		return IOUtils.addLastModifiedToFile(path, realPath, lfc);
    	} else {
    		return path;
    	}
    }

    public static String addLastModifiedToUri(RequestInfo requestInfo, String base, String path) {
    	Matcher matcher = uriMatchPattern.matcher(path);
    	StringBuilder localPath = new StringBuilder();
    	if(matcher.matches()) {
    		String pathServerName = matcher.group(1);
    		boolean pathRelative = (matcher.group(2) == null);
    		String pathPath = matcher.group(3);
    		if(pathServerName == null ||pathServerName.length() == 0) {
    			//check base
    			if(base != null) {
    				matcher = uriMatchPattern.matcher(base);
    				if(!matcher.matches())
    					return path;
    				String baseServerName = matcher.group(1);
    	    		boolean baseRelative = (matcher.group(2) == null);
    	    		String basePath = matcher.group(3);
    	    		if(baseServerName != null && baseServerName.length() != 0 && !baseServerName.equalsIgnoreCase(requestInfo.getServerName())) {
    	    			return path;
    	    		}
    	    		if(pathRelative) {
    	    			if(baseRelative) {
    	    				localPath.append(requestInfo.getContextPath());
	    	    			if(requestInfo.getPathInfo() != null) {
	    	    				appendDirectory(localPath, requestInfo.getPathInfo());
	    	    			} else {
	    	    				localPath.append('/');
	    	    			}
	    	    		} else {
	    	    			localPath.append('/');
	    	    		}
    	    			appendDirectory(localPath, basePath);
    	    		}
    			} else if(pathRelative) {
    				localPath.append(requestInfo.getContextPath());
	    			if(requestInfo.getPathInfo() != null) {
	    				appendDirectory(localPath, requestInfo.getPathInfo());
	    			} else {
	    				localPath.append('/');
	    			}
    			} else {
    				localPath.append('/');
    			}
    		} else if(pathServerName.equalsIgnoreCase(requestInfo.getServerName())) {
    			localPath.append('/');
    		} else {
    			return path;
    		}
    		localPath.append(pathPath);
    		String realPath = requestInfo.getRealPath(localPath.toString());
    		if(realPath == null)
    			return path;
    		return IOUtils.addLastModifiedToFile(path, realPath, defaultLocalFileCache);
    	} else {
    		return path;
    	}
    }
    protected static class LocalFileCache extends LockSegmentCache<String, ConfigSource, RuntimeException> {
		@Override
		protected ConfigSource createValue(String key, Object... additionalInfo) {
			return ConfigSource.createConfigSource(new File(key));
		}
		@Override
		protected boolean keyEquals(String key1, String key2) {
			return key1.equals(key2);
		}
		@Override
		protected boolean valueEquals(ConfigSource value1, ConfigSource value2) {
			return value1.equals(value2);
		}
    }
    protected static final LocalFileCache defaultLocalFileCache = new LocalFileCache();
    
    public static String getRequestURIQuery(final HttpServletRequest request) {
			StringBuilder requestURIQuery = new StringBuilder(request.getRequestURI());
			if (request.getQueryString() != null)
				requestURIQuery.append("?").append(request.getQueryString());
			return requestURIQuery.toString();
	}
    
	public static InputForm getInputForm(ServletRequest request) {
		return (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	}

    public static String getActionURIQuery(final HttpServletRequest request, String defaultAction) {
    	String action;
		InputForm form = getInputForm(request);
    	if (form == null)
    		return defaultAction;
		action = (String) form.getAttribute(SimpleServlet.ATTRIBUTE_REQUEST_URI_QUERY);
		if (action == null || action.length() == 0)
			return defaultAction;
		int pos = action.indexOf("&msg=");
		if (pos > -1)
			return action.substring(0, pos);
		else
			return action;
    }
    
    public static int handleActionRedirect(InputForm form, final HttpServletRequest request, final HttpServletResponse response, String redirectQuery) {
    	if (request.getMethod().equalsIgnoreCase("POST")) {
    		String action = getActionURIQuery(request, null);
    		if (action == null)
    			return SimpleAction.INVOKE_RESULT_NORMAL_FLOW;
    		StringBuilder sb = new StringBuilder(action);
    		if (redirectQuery != null) {
    			sb.append(action.indexOf('?') > -1 ? '&' : '?');
    			sb.append(redirectQuery);
    		}
    		if (sb.indexOf("?") > -1) {
				if (sb.indexOf("&msg=") == -1)
					sb.append("&msg=Changes+saved");
				form.setRedirectUri(new StringBuilder(sb).toString());
				return SimpleAction.INVOKE_RESULT_REDIRECT;
	    	}
    	}
    	return SimpleAction.INVOKE_RESULT_NORMAL_FLOW;
    }
    
    public static String getMessage(final HttpServletRequest request) {
    	String msg = request.getParameter("msg");
    	if (msg == null)
    		return "";
    	return msg;
    }

	public static class UserAgentOS {
		public final String name;
		public final boolean mobile;
		public final String family;

		protected UserAgentOS(String name, boolean mobile, String family) {
			this.name = name;
			this.mobile = mobile;
			this.family = family;
		}
	}

	protected static Map<Pattern, UserAgentOS> userAgentMatches = new LinkedHashMap<Pattern, UserAgentOS>();
	static {
		// Windows
		registerUserAgentMatch("(Win16)", "Windows 3.11", "Windows", false);
		registerUserAgentMatch("(Windows 95)|(Win95)|(Windows_95)", "Windows 95", "Windows", false);
		registerUserAgentMatch("(Windows 98)|(Win98)", "Windows 98", "Windows", false);
		registerUserAgentMatch("(Windows NT 5.0)|(Windows 2000)", "Windows 2000", "Windows", false);
		registerUserAgentMatch("(Windows NT 5.01)", "Windows 2000 Service Pack 1", "Windows", false);
		registerUserAgentMatch("(Windows NT 5.1)|(Windows XP)", "Windows XP", "Windows", false);
		registerUserAgentMatch("(Windows NT 5.2)", "Windows Server 2003", "Windows", false);
		registerUserAgentMatch("(Windows NT 6.0)|(Windows Vista)", "Windows Vista", "Windows", false);
		registerUserAgentMatch("(Windows NT 6.1)|(Windows 7)", "Windows 7", "Windows", false);
		registerUserAgentMatch("(Windows NT 6.2)|(Windows 8)", "Windows 8", "Windows", false);
		registerUserAgentMatch("(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)", "Windows NT 4.0", "Windows", false);
		registerUserAgentMatch("(Windows ME)|(Windows 98; Win 9x 4.90 )", "Windows ME", "Windows", false);
		registerUserAgentMatch("(Windows CE)", "Windows CE", "Windows", true);
		// UNIX Like Operating Systems
		registerUserAgentMatch("(Mac OS X beta)", "Mac OS X Kodiak (beta)", "Macintosh", false);
		registerUserAgentMatch("(Mac OS X 10.0)", "Mac OS X Cheetah", "Macintosh", false);
		registerUserAgentMatch("(Mac OS X 10.1)", "Mac OS X Puma", "Macintosh", false);
		registerUserAgentMatch("(Mac OS X 10.2)", "Mac OS X Jaguar", "Macintosh", false);
		registerUserAgentMatch("(Mac OS X 10.3)", "Mac OS X Panther", "Macintosh", false);
		registerUserAgentMatch("(Mac OS X 10.4)", "Mac OS X Tiger", "Macintosh", false);
		registerUserAgentMatch("(Mac OS X 10.5)", "Mac OS X Leopard", "Macintosh", false);
		registerUserAgentMatch("(Mac OS X 10.6)", "Mac OS X Snow Leopard", "Macintosh", false);
		registerUserAgentMatch("(Mac OS X 10.7)", "Mac OS X Lion", "Macintosh", false);
		registerUserAgentMatch("(Mac OS X)", "Mac OS X", "Macintosh", false);
		registerUserAgentMatch("(Mac_PowerPC)|(PowerPC)|(Macintosh)", "Mac OS", "Macintosh", false);
		registerUserAgentMatch("(OpenBSD)", "Open BSD", "Open BSD", false);
		registerUserAgentMatch("(SunOS)", "SunOS", "SunOS", false);
		registerUserAgentMatch("(Solaris/11)|(Solaris11)", "Solaris 11", "Solaris", false);
		registerUserAgentMatch("((Solaris/10)|(Solaris10))", "Solaris 10", "Solaris", false);
		registerUserAgentMatch("((Solaris/9)|(Solaris9))", "Solaris 9", "Solaris", false);
		registerUserAgentMatch("(Solaris)", "Solaris", "Solaris", false);
		registerUserAgentMatch("(CentOS)", "CentOS", "CentOS", false);
		registerUserAgentMatch("(QNX)", "QNX", "QNX", false);
		// Kernels
		registerUserAgentMatch("(UNIX)", "UNIX", "UNIX", false);
		// Linux Operating Systems
		registerUserAgentMatch("(Ubuntu/12.10)|(Ubuntu 12.10)", "Ubuntu 12.10", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/12.04)|(Ubuntu 12.04)", "Ubuntu 12.04 LTS", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/11.10)|(Ubuntu 11.10)", "Ubuntu 11.10", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/11.04)|(Ubuntu 11.04)", "Ubuntu 11.04", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/10.10)|(Ubuntu 10.10)", "Ubuntu 10.10", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/10.04)|(Ubuntu 10.04)", "Ubuntu 10.04 LTS", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/9.10)|(Ubuntu 9.10)", "Ubuntu 9.10", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/9.04)|(Ubuntu 9.04)", "Ubuntu 9.04", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/8.10)|(Ubuntu 8.10)", "Ubuntu 8.10", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/8.04)|(Ubuntu 8.04)", "Ubuntu 8.04 LTS", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu/6.06)|(Ubuntu 6.06)", "Ubuntu 6.06 LTS", "Ubuntu", false);
		registerUserAgentMatch("(Ubuntu)", "Ubuntu", "Ubuntu", false);
		registerUserAgentMatch("(Red Hat Enterprise)", "Red Hat Enterprise Linux", "Red Hat Enterprise Linux", false);
		registerUserAgentMatch("(Red Hat)", "Red Hat Linux", "Red Hat Linux", false);
		registerUserAgentMatch("(Fedora/17)|(Fedora 17)", "Fedora 17", "Fedora", false);
		registerUserAgentMatch("(Fedora/16)|(Fedora 16)", "Fedora 16", "Fedora", false);
		registerUserAgentMatch("(Fedora/15)|(Fedora 15)", "Fedora 15", "Fedora", false);
		registerUserAgentMatch("(Fedora/14)|(Fedora 14)", "Fedora 14", "Fedora", false);
		registerUserAgentMatch("(Fedora)", "Fedora", "Fedora", false);
		registerUserAgentMatch("(ChromiumOS)", "Chromium OS", "Chromium OS", false);
		registerUserAgentMatch("(ChromeOS)", "Google Chrome OS", "Google Chrome OS", false);
		// Kernel
		registerUserAgentMatch("(Linux)|(X11)", "Linux", "Linux", false);
		// BSD Operating Systems
		registerUserAgentMatch("(OpenBSD)", "OpenBSD", "OpenBSD", false);
		registerUserAgentMatch("(FreeBSD)", "FreeBSD", "FreeBSD", false);
		registerUserAgentMatch("(NetBSD)", "NetBSD", "NetBSD", false);
		// Mobile Devices
		registerUserAgentMatch("(Android)", "Andriod", "Andriod", true);
		registerUserAgentMatch("(iPod)", "iPod", "iPod", true);
		registerUserAgentMatch("(iPhone)", "iPhone", "iPhone", true);
		registerUserAgentMatch("(iPad)", "iPad", "iPad", true);
		//DEC Operating Systems
		registerUserAgentMatch("(OS/8)|(OS8)", "OS/8", "DEC", false);
		registerUserAgentMatch("(DEC)|(RSTS)|(RSTS/E)", "Older DEC OS", "DEC", false);
		registerUserAgentMatch("(WPS-8)|(WPS8)", "WPS-8", "DEC", false);
		// BeOS Like Operating Systems
		registerUserAgentMatch("(BeOS)|(BeOS r5)", "BeOS", "BeOS", false);
		registerUserAgentMatch("(BeIA)", "BeIA", "BeIA", false);
		// OS/2 Operating Systems
		registerUserAgentMatch("(OS/220)|(OS/2 2.0)", "OS/2 2.0", "OS/2", false);
		registerUserAgentMatch("(OS/2)|(OS2)", "OS/2", "OS/2", false);
		// Search engines
		registerUserAgentMatch("(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(msnbot)|(Ask Jeeves/Teoma)|(ia_archiver)", "Search engine or robot", "Search engine or robot", false);
	}

	protected static void registerUserAgentMatch(String pattern, String name, String family, boolean mobile) {
		userAgentMatches.put(Pattern.compile(pattern, Pattern.CASE_INSENSITIVE), new UserAgentOS(name, mobile, family));
	}

	public static UserAgentOS getUserAgentOS(InputForm form) {
		return getUserAgentOS(form.getUserAgent());
	}

	protected static Pattern userAgentPattern = Pattern.compile("[^\\(]+ \\(([^\\(]+)\\) .*");
	protected static Pattern userAgentMobilePattern = Pattern.compile("\\bmobile(?:\\b|\\W)", Pattern.CASE_INSENSITIVE);

	public static UserAgentOS getUserAgentOS(String userAgent) {
		if(userAgent == null)
			return null;
		Matcher m = userAgentPattern.matcher(userAgent);
		if(m.matches()) {
			String systemInfo = m.group(1);
			for(Map.Entry<Pattern, UserAgentOS> entry : userAgentMatches.entrySet()) {
				m = entry.getKey().matcher(systemInfo);
				if(m.find())
					return entry.getValue();
			}
		}
		return null;
	}

	public static boolean isMobileUserAgentOS(InputForm form) {
		return isMobileUserAgentOS(form.getUserAgent());
	}

	public static boolean isMobileUserAgentOS(String userAgent) {
		return userAgent != null && userAgentMobilePattern.matcher(userAgent).find();
	}

	public static String getClassPath(ServletContext context) {
		ClassLoader cl;
		try {
			cl = context.getClassLoader();
		} catch(AccessControlException e) {
			cl = Thread.currentThread().getContextClassLoader();
		}
		StringBuilder classpath = new StringBuilder();
		for(; cl != null; cl = cl.getParent()) {
			if(cl instanceof URLClassLoader) {
				URL[] urls = ((URLClassLoader) cl).getURLs();
				for(URL url : urls) {
					if(!StringUtils.isBlank(url.getPath())) {
						if(classpath.length() > 0)
							classpath.append(File.pathSeparatorChar);
						classpath.append(url.getPath());
					}
				}
			}
		}
		if(classpath.length() == 0)
			return null;
		return classpath.toString();
	}

	public static String determineScratchDir() {
		Path tempDir = FileSystems.getDefault().getPath(System.getProperty("java.io.tmpdir"));
		StringBuilder sb = new StringBuilder();
		sb.append("__jsp_app_");
		int port = ConvertUtils.getIntSafely(SecurityUtils.getProperties().getProperty("jmx.remote.x.rmiRegistryPort"), -1);
		if(port <= 0) {
			port = ConvertUtils.getIntSafely(SystemUtils.getSystemInfo().get("processId"), -1);
			if(port <= 0) {
				try {
					@SuppressWarnings("resource")
					// intentionally don't close
					ServerSocket serverSocket = new ServerSocket(0);
					port = serverSocket.getLocalPort();
					sb.append("ss_");
				} catch(IOException e) {
					port = (int) (System.currentTimeMillis() & 0x7FFFFFFF);
					sb.append("r_");
				}
			} else
				sb.append("pid_");
		}
		sb.append(port);
		Path scratchDir;
		try {
			scratchDir = Files.createTempDirectory(tempDir, sb.toString()); // , PosixFilePermissions.asFileAttribute(EnumSet.of(OWNER_READ, OWNER_WRITE, OWNER_EXECUTE)));
		} catch(IOException e) {
			throw new UndeclaredThrowableException(e);
		}
		IOUtils.deleteDirectoryOnExit(scratchDir);
		return scratchDir.toAbsolutePath().toString();
	}

	/**
	 * Adds performer attribute to form, that represents user who actually done last operation in session.
	 * I.e. user or admin user who impersonated as current user
	 *
	 * @param form                  Form
	 * @param userId                Current userId
	 * @param masterUserId          MasterUserId
	 * @param userInternal          Is current user internal?
	 * @param masterUserInternal	Is master user internal?
     */
	public static void setPerformerUserAttribute(InputForm form, long userId, long masterUserId, boolean userInternal, boolean masterUserInternal) {
		if (masterUserId != 0 && userId != masterUserId) {
			// Use master user
			form.setAttribute("performer.userId", masterUserId);
			form.setAttribute("performer.internal", masterUserInternal);
		} else {
			// Use default user
			form.setAttribute("performer.userId", userId);
			form.setAttribute("performer.internal", userInternal);
		}
	}
}
