package simple.servlet.axis2;

import org.apache.axis2.AxisFault;
import org.apache.axis2.builder.Builder;
import org.apache.axis2.description.Parameter;
import org.apache.axis2.description.TransportInDescription;
import org.apache.axis2.description.TransportOutDescription;
import org.apache.axis2.engine.AxisConfiguration;
import org.apache.axis2.engine.MessageReceiver;
import org.apache.axis2.transport.MessageFormatter;


public class SimpleAxisConfiguration extends AxisConfiguration {
	/*
	public static class ParameterWrapper extends AbstractDynaBean {
		protected final ParameterInclude parameters;

		public ParameterWrapper(ParameterInclude parameters) {
			super();
			this.parameters = parameters;
		}

		@Override
		protected String getDynaClassName() {
			return parameters.getClass().getName() + "Wrapper";
		}

		@Override
		protected boolean hasDynaProperty(String name) {
			return true;
		}

		@Override
		protected void setDynaProperty(String name, Object value) {
			parameters.addParameter(new Parameter(name, value));
		}

		@Override
		protected Object getDynaProperty(String name) {
			return parameters.getParameter(name);
		}
	}*/
	public static class SimpleTransportInDescription extends TransportInDescription {
		public SimpleTransportInDescription(String name) {
			super(name);
		}

		public void setParameter(String name, Object value) throws AxisFault {
			addParameter(new Parameter(name, value));
		}
	}

	public static class SimpleTransportOutDescription extends TransportOutDescription {
		public SimpleTransportOutDescription(String name) {
			super(name);
		}

		public void setParameter(String name, Object value) throws AxisFault {
			addParameter(new Parameter(name, value));
		}
	}

	protected SimpleTransportInDescription httpTransportIn;
	protected SimpleTransportInDescription httpsTransportIn;

	protected SimpleTransportOutDescription httpTransportOut;
	protected SimpleTransportOutDescription httpsTransportOut;

	public void setParameter(String name, Object value) throws AxisFault {
		addParameter(name, value);
	}

	public void setMessageReceiver(String mepURL, MessageReceiver messageReceiver) {
		addMessageReceiver(mepURL, messageReceiver);
	}

	public void addMessageFormatter(String contentType, MessageFormatter messageFormatter) {
		addMessageFormatter(contentType, messageFormatter);
	}

	public void setMessageBuilder(String contentType, Builder messageBuilder) {
		addMessageBuilder(contentType, messageBuilder);
	}

	public SimpleTransportInDescription getHttpTransportIn() {
		if(httpTransportIn == null)
			httpTransportIn = new SimpleTransportInDescription("http");
		return httpTransportIn;
	}

	public SimpleTransportInDescription getHttpsTransportIn() {
		if(httpsTransportIn == null)
			httpsTransportIn = new SimpleTransportInDescription("https");
		return httpsTransportIn;
	}

	public SimpleTransportOutDescription getHttpTransportOut() {
		if(httpTransportOut == null)
			httpTransportOut = new SimpleTransportOutDescription("http");
		return httpTransportOut;
	}

	public SimpleTransportOutDescription getHttpsTransportOut() {
		if(httpsTransportOut == null)
			httpsTransportOut = new SimpleTransportOutDescription("https");
		return httpsTransportOut;
	}

	public void initialize() throws AxisFault {
		if(httpTransportIn != null)
			addTransportIn(httpTransportIn);
		if(httpsTransportIn != null)
			addTransportIn(httpsTransportIn);
		if(httpTransportOut != null)
			addTransportOut(httpTransportOut);
		if(httpsTransportOut != null)
			addTransportOut(httpsTransportOut);
	}
}
