/*
 * Catch.java
 *
 * Created on December 24, 2003, 10:55 AM
 */

package simple.servlet;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import simple.bean.ConditionUtils;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.util.CollectionUtils;

/** Represents an exception handler for an action or step that determines if a given exception
 * matches its criteria and holds the page to forward to if the exception matches.
 *
 * @author  Brian S. Krug
 */
public class Catch {
    private static final Log log = Log.getLog();
    /** Holds the list of criterium for this Catch */
    protected List<String[]> criterium = new ArrayList<String[]>();

    /** Holds the list of sub-steps for this Catch */
    protected List<Step> steps = new ArrayList<Step>();

    /** Holds value of property exceptionClass. */
    private Class<? extends Throwable> exceptionClass;

    /** Holds value of property page. */
    private String page;

    /** Holds value of property action. */
    private String action;

    protected int hash;

    /** Creates a new instance of Catch */
    public Catch() {
    }

    /** Getter for property exceptionClass.
     * @return Value of property exceptionClass.
     *
     */
    public Class<? extends Throwable> getExceptionClass() {
        return this.exceptionClass;
    }

    /** Setter for property exceptionClass.
     * @param exceptionClass New value of property exceptionClass.
     *
     */
    public void setExceptionClass(Class<? extends Throwable> exceptionClass) {
        this.exceptionClass = exceptionClass;
    }

    /** Determines whether the specified Throwable matches the criterium for this Catch
     * @param exc The Throwable object
     * @return True - if the Throwable matches all criterium, False - otherwise
     */
    public boolean matches(Throwable exc) {
        if(exceptionClass != null && !exceptionClass.isInstance(exc)) return false;
        for(String[] crit : criterium) {
            try {
                Object o1 = ReflectionUtils.getProperty(exc, crit[0]);
            	if(log.isDebugEnabled()) {
            		log.debug("Checking exception  property '" + crit[0] + "'='" +  o1 + "' against '" + crit[2] + "' using " + crit[1]);
            	}
                if(!ConditionUtils.check(o1, crit[1], crit[2])) {
                	return false;
                }
            } catch(IntrospectionException e) {
                log.debug("Exception '" + exc + "' threw an error while checking if it matched " + this);
                return false;
            } catch (IllegalAccessException e) {
                log.debug("Exception '" + exc + "' threw an error while checking if it matched " + this);
                return false;
            } catch (InvocationTargetException e) {
                log.debug("Exception '" + exc + "' threw an error while checking if it matched " + this);
                return false;
            } catch(ParseException e) {
                log.debug("Exception '" + exc + "' threw an error while checking if it matched " + this);
                return false;
			}
        }
        return true; //passed all match criteria
    }

    /** Adds a new criteria to the list for this Catch
     * @param property The name of the Throwable's property that is begin tested
     * @param operator The type of comparison to perform on the specified property value of any Throwable
     * attempting to match this Catch object
     * @param value The value against which the Throwable's property value is compared
     */
    public void addCriteria(String property, String operator, String value) {
        criterium.add(new String[] {property, operator, value});
    }

    /** Getter for property page.
     * @return Value of property page.
     *
     */
    public String getPage() {
        return this.page;
    }

    /** Setter for property page.
     * @param page New value of property page.
     *
     */
    public void setPage(String page) {
        this.page = page;
    }

	/**
	 * @return Returns the action.
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action The action to set.
	 */
	public void setAction(String action) {
		this.action = action;
	}

    public void addStep(Step step) {
        if(step != null) steps.add(step);
    }

    public Iterator<Step> getSteps() {
    	return steps.iterator();
    }
    @Override
    public boolean equals(Object obj) {
    	if(!(obj instanceof Catch)) return false;
    	Catch c = (Catch)obj;
    	if(!ConvertUtils.areEqual(exceptionClass, c.exceptionClass)) return false;
    	return CollectionUtils.deepEquals(criterium, c.criterium);
    }
    @Override
    public int hashCode() {
    	if(hash == 0) {
    		hash = (exceptionClass == null ? 0 : exceptionClass.hashCode()) + CollectionUtils.deepHashCode(criterium);
    	}
    	return hash;
    }
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	sb.append(getClass().getName()).append(" (");
    	boolean first = true;
    	if(exceptionClass != null) {
    		sb.append("Class = ").append(exceptionClass.getName());
    		first = false;
    	}
    	for(String[] crit : criterium) {
    		if(first) first = false;
    		else sb.append(" and ");
    		sb.append(crit[0]).append(' ').append(crit[1]).append(' ').append(crit[2]);
    	}
    	sb.append(')');
    	return sb.toString();
    }
}
