/*
 * SecureCustomActionGroup.java
 *
 * Created on January 27, 2003, 8:51 AM
 */

package simple.servlet;

/** Tells the CustomActionServlet that this ActionGroup needs a user with the listed privileges.  
 *
 * @author  Brian S. Krug
 */
public interface SecureCustomActionGroup extends CustomActionGroup {
    /** Returns an array of privileges that are required to access the specified method of this
     * object. If a method on this object requires special privileges to access it, it should
     * return an array of the necessary privileges
     * @return An array of required privileges
     * @param methodName The method name for which required priviliges are returned
     */    
    public String[] getRequiredPrivileges(String methodName) ;
}

