/*
 * NotLoggedOnException.java
 *
 * Created on January 27, 2003, 9:31 AM
 */

package simple.servlet;

import javax.servlet.ServletException;

/** Signifies that a user has not yet been created for the ServletSession
 * @author Brian S. Krug
 */
public class NotLoggedOnException extends ServletException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 987014146767L;


	/**
     * Creates new <code>NotLoggedOnException</code> without detail message.
     */
    public NotLoggedOnException() {
    }


    /**
     * Constructs an <code>NotLoggedOnException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public NotLoggedOnException(String msg) {
        super(msg);
    }
}


