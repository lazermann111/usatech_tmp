/*
 * Created on May 9, 2005
 *
 */
package simple.servlet;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.BasicBatchRecorder;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.AbstractLookupMap;
import simple.util.CaseInsensitiveComparator;

/** Records the request before and updates after the request is handled
 * @author Brian S. Krug
 *
 */
public class RecordRequestFilter2 implements Filter {
	private static final Log log = Log.getLog();
	protected final BasicBatchRecorder<CallInputs> recorder = new BasicBatchRecorder<CallInputs>();
	protected int maxValues = 20;
    protected Collection<String> maskedParameterNames = new TreeSet<String>(new CaseInsensitiveComparator<String>());
	protected Set<String> restrictedParameterNames = Collections.emptySet();
    protected String[] errorSearchAttributes;
    protected FileUpload fileUpload;
    protected static final String[] DEFAULT_ERROR_SEARCH_ATTRIBUTES = new String[] {"javax.servlet.error.exception", "javax.servlet.jsp.jspException","simple.servlet.SimpleAction.exception"};
    protected static final String vmId = ManagementFactory.getRuntimeMXBean().getName();
    protected static final Random random = new Random();

    protected class ParameterStringMap extends AbstractLookupMap<String, String> {
    	protected final StringBuilder sb;
		protected final Map<String,Object> parameters;
		protected ParameterStringMap(Map<String,Object> parameters) {
			this.parameters = parameters;
			this.sb = new StringBuilder();
		}
		@Override
		public String get(Object key) {
			Object value = parameters.get(key);
			if(value == null) return null;
			if(value instanceof String) return (String)value;
			if(value instanceof String[]) {
				String[] arr = (String[]) value;
				sb.setLength(0);
				for(int i = 0; i < arr.length && (maxValues < 1 || i < maxValues); i++) {
					if(i == 0) sb.append("[");
					sb.append(",").append(arr[i]);
				}
				if(maxValues > 0 && arr.length > maxValues)
					sb.append("...");
				sb.append("]");
				return sb.toString();
			}
			if(value instanceof InputFile) {
				return ((InputFile)value).getPath();
			}
			if(value instanceof InputFile[]) {
				InputFile[] arr = (InputFile[]) value;
				sb.setLength(0);
				for(int i = 0; i < arr.length && (maxValues < 1 || i < maxValues); i++) {
					if(i == 0) sb.append("[");
					sb.append(",").append(arr[i].getPath());
				}
				if(maxValues > 0 && arr.length > maxValues)
					sb.append("...");
				sb.append("]");
				return sb.toString();
			}
			return value.toString();
		}
		@Override
		public Set<String> keySet() {
			return parameters.keySet();
		}
    }
    public class CallInputs {
    	protected final String requestId;
    	protected final HttpServletRequest request;
    	protected Map<String, String> headers;
    	protected Map<String, String> parameters;
    	protected long endTime;
    	protected String exceptionText;
    	protected final long startTime;
    	protected Map<String,Object> requestAttributes;
    	protected Map<String,Object> sessionAttributes;

    	public CallInputs(HttpServletRequest request, long startTime) {
    		this.request = request;
    		this.startTime = startTime;
    		this.requestId = generateRequestId(startTime);
    	}
		public Map<String, String> getHeaders() {
			if(headers == null) {
				headers = new AbstractLookupMap<String,String>() {
		    		protected Set<String> keySet;
					@Override
					public String get(Object key) {
						return request.getHeader((String)key);
					}
					@Override
					public Set<String> keySet() {
						if(keySet == null) {
							keySet = new HashSet<String>();
							for(Enumeration<?> en = request.getHeaderNames(); en.hasMoreElements(); ) {
					            String name = (String)en.nextElement();
					            keySet.add(name);
							}
						}
						return keySet;
					}
		    	};
			}
			return headers;
		}
		public Map<String, String> getParameters() throws FileUploadException {
			if(parameters == null) {
				InputForm form = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
				if(form != null) {
					parameters = new ParameterStringMap(form.getParameters());
				} else {
					if(fileUpload == null && request.getMethod().equalsIgnoreCase("POST")
							&& RequestUtils.isMultipartContent(request)) {
						File tempDirFile = (File) request.getSession().getServletContext().getAttribute("javax.servlet.context.tempdir");
						if(tempDirFile == null)
							tempDirFile = new java.io.File(System.getProperty("java.io.tmpdir"));
				        if(!tempDirFile.exists()) try {
				            tempDirFile.createNewFile();
				        } catch(IOException ioe) {
				            log.warn("Could not create Temp Folder, '" + tempDirFile.getAbsolutePath() + "'", ioe);
				        }
				        fileUpload = RequestUtils.createFileUpload(tempDirFile, 5 * 1024 * 1024);
					}
					parameters = new ParameterStringMap(RequestUtils.getRequestParameters(request, restrictedParameterNames, maskedParameterNames, fileUpload));
				}
			}
			return parameters;
		}
		public HttpServletRequest getRequest() {
			return request;
		}
		public String getExceptionText() {
			return exceptionText;
		}
		public void setExceptionText(String exceptionText) {
			this.exceptionText = exceptionText;
		}
		public ServletUser getUser() {
			return (ServletUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
		}
		public Map<String, Object> getRequestAttributes() {
			if(requestAttributes == null) {
				requestAttributes = RequestUtils.getAttributeMap(request);
			}
			return requestAttributes;
		}
		public Map<String, Object> getSessionAttributes() {
			if(sessionAttributes == null) {
				HttpSession session = request.getSession(false);
				if(session != null)
					sessionAttributes = RequestUtils.getAttributeMap(session);
				else
					sessionAttributes = Collections.emptyMap();
			}
			return sessionAttributes;
		}
		public String getSessionId() {
			HttpSession session = request.getSession(false);
			if(session != null)
				try {
					return session.getId();
				} catch(IllegalStateException e) {
				}
			return null;
		}
		public String getRequestId() {
			return requestId;
		}
		public long getEndTime() {
			return endTime;
		}
		public void setEndTime(long endTime) {
			this.endTime = endTime;
		}
		public long getStartTime() {
			return startTime;
		}
		public long getDuration() {
			return endTime - startTime;
		}
    }
    public RecordRequestFilter2() {
        super();
    }

    protected static String generateRequestId(long startTime) {
    	return vmId + ":" + Long.toHexString(startTime).toUpperCase() + ":" + StringUtils.toHex(random.nextLong());
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    	String callId = filterConfig.getInitParameter("call-id");
    	recorder.setCallId(callId);
    	try {
			Integer tmp = ConvertUtils.convert(Integer.class, filterConfig.getInitParameter("max-values"));
	    	if(tmp != null)
	    		maxValues = tmp.intValue();
		} catch (ConvertException e) {
			log.warn("Could not convert max-values init param to a number", e);
		}
		try {
			Integer tmp = ConvertUtils.convert(Integer.class, filterConfig.getInitParameter("batch-size"));
	    	if(tmp != null)
	    		recorder.setBatchSize(tmp.intValue());
		} catch (ConvertException e) {
			log.warn("Could not convert batch-size init param to a number", e);
		}
		try {
			String[] tmp = ConvertUtils.convert(String[].class, filterConfig.getInitParameter("masked-parameters"));
	        if(tmp != null)
	        	for(String s : tmp)
	        		maskedParameterNames.add(s);
	        else {
	        	maskedParameterNames.add("password");
	        	maskedParameterNames.add("confirm");
	        }
		} catch(ConvertException e) {
			log.warn("Could not convert to array of Strings", e);
		}
		try {
			recorder.setAsynchronous(ConvertUtils.getBoolean(filterConfig.getInitParameter("run-out-of-line"), false));
		} catch(ConvertException e) {
			log.warn("Could not convert 'run-out-of-line' to boolean", e);
		}
		try {
			recorder.setPollFrequency(ConvertUtils.getInt(filterConfig.getInitParameter("poll-frequency"), 5000));
		} catch(ConvertException e) {
			log.warn("Could not convert 'poll-frequency' to a number", e);
		}

		errorSearchAttributes = StringUtils.split(filterConfig.getInitParameter("error-search-attributes"), StringUtils.STANDARD_DELIMS, false);
        if(errorSearchAttributes == null)
        	errorSearchAttributes = DEFAULT_ERROR_SEARCH_ATTRIBUTES;
        else
        	for(int i = 0; i < errorSearchAttributes.length; i++)
        		errorSearchAttributes[i] = errorSearchAttributes[i].trim();
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        if(request instanceof HttpServletRequest && recorder.getCallId() != null) {
        	CallInputs info = new CallInputs((HttpServletRequest)request, System.currentTimeMillis());
        	recorder.record(info);
        	String exception = null;
        	try {
        		chain.doFilter(request, response);
        	} catch(ServletException e) {
        		exception = StringUtils.exceptionToString(e);
        		throw e;
        	} catch(IOException e) {
        		exception = StringUtils.exceptionToString(e);
        		throw e;
        	} finally {
        		info.setEndTime(System.currentTimeMillis());
        		if(exception == null)
        			exception = getErrorText(request, response);
        		if(exception != null)
        			info.setExceptionText(exception);
        		recorder.record(info);
        	}
        } else {
        	chain.doFilter(request, response);
        }
    }

    protected String getErrorText(ServletRequest request, ServletResponse response) {
    	if(errorSearchAttributes != null) {
			for(String attr : errorSearchAttributes) {
    			Throwable e = (Throwable)request.getAttribute(attr);
    			if(e != null) {
    				return StringUtils.exceptionToString(e);
    			}
			}
    	}
    	return null;
    }

    public void destroy() {
    	recorder.finish();
    }
}
