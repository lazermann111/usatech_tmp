/*
 * Created on May 9, 2005
 *
 */
package simple.servlet;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call;
import simple.db.CallNotFoundException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.AbstractLookupMap;
import simple.util.CaseInsensitiveComparator;

public class RecordRequestFilter implements Filter {
	private static final Log log = Log.getLog();
    protected String callId;
    protected int maxValues = 20;
    protected int batchSize = 20;
    protected Call call;
    protected Call.BatchUpdate batch;
    protected final ReentrantLock lock = new ReentrantLock();
    protected Collection<String> maskedParameterNames = new TreeSet<String>(new CaseInsensitiveComparator<String>());
	protected Set<String> restrictedParameterNames = Collections.emptySet();
    protected String[] errorSearchAttributes;
    protected boolean asynchronous = false;
    protected FileUpload fileUpload;
    protected Thread batchThread;
    protected int pollFrequency;
	protected Pattern excludeUrlPattern;
	protected Pattern excludeIPPattern;
    protected volatile boolean done = false;
    protected static final String[] DEFAULT_ERROR_SEARCH_ATTRIBUTES = new String[] {"javax.servlet.error.exception", "javax.servlet.jsp.jspException","simple.servlet.SimpleAction.exception"};
	public static class CallInputs implements Serializable {
		private static final long serialVersionUID = 6949704137766113514L;
		protected Map<String, String> headers;
		protected Map<String, Object> realParameters;
		protected int maxValues;
		protected final Map<String, String> parameters = new AbstractLookupMap<String, String>() {
			protected final StringBuilder sb = new StringBuilder();

			@Override
			public String get(Object key) {
				Object value = realParameters.get(key);
				if(value == null)
					return null;
				if(value instanceof String)
					return (String) value;
				if(value instanceof String[]) {
					String[] arr = (String[]) value;
					sb.setLength(0);
					for(int i = 0; i < arr.length && (maxValues < 1 || i < maxValues); i++) {
						if(i == 0)
							sb.append("[");
						sb.append(",").append(arr[i]);
					}
					if(maxValues > 0 && arr.length > maxValues)
						sb.append("...");
					sb.append("]");
					return sb.toString();
				}
				if(value instanceof InputFile) {
					return ((InputFile) value).getPath();
				}
				if(value instanceof InputFile[]) {
					InputFile[] arr = (InputFile[]) value;
					sb.setLength(0);
					for(int i = 0; i < arr.length && (maxValues < 1 || i < maxValues); i++) {
						if(i == 0)
							sb.append("[");
						sb.append(",").append(arr[i].getPath());
					}
					if(maxValues > 0 && arr.length > maxValues)
						sb.append("...");
					sb.append("]");
					return sb.toString();
				}
				return value.toString();
			}

			@Override
			public Set<String> keySet() {
				return realParameters.keySet();
			}
		};
    	protected Map<String, Object> explicitParameters;
    	protected long duration;
    	protected String exceptionText;
    	protected String appCd;
    	protected final Date currentTime = new Date();
    	protected Map<String,Object> requestAttributes;
    	protected Map<String,Object> sessionAttributes;
    	protected String actionName;
    	protected String objectTypeCd;
    	protected String objectCd;
		protected String requestURL;
		protected String remoteAddr;
		protected int remotePort;
		protected String sessionId;
		protected ServletUser user;

		public CallInputs() {
    	}

		public Map<String, String> getHeaders() {
			return headers;
		}

		public Map<String, String> getParameters() {
			return parameters;
		}
		public long getDuration() {
			return duration;
		}
		public String getExceptionText() {
			return exceptionText;
		}		

		public void setDuration(long duration) {
			this.duration = duration;
		}
		public void setExceptionText(String exceptionText) {
			this.exceptionText = exceptionText;
		}

		public ServletUser getUser() {
			return user;
		}
		public Date getCurrentTime() {
			return currentTime;
		}
		public Map<String, Object> getRequestAttributes() {
			return requestAttributes;
		}
		public Map<String, Object> getSessionAttributes() {
			return sessionAttributes;
		}
		public String getSessionId() {
			return sessionId;
		}
		public String getAppCd() {
			return appCd;
		}
		public void setAppCd(String appCd) {
			this.appCd = appCd;
		}
		public String getActionName() {
			return actionName;
		}
		public void setActionName(String actionName) {
			this.actionName = actionName;
		}
		public String getObjectTypeCd() {
			return objectTypeCd;
		}
		public void setObjectTypeCd(String objectTypeCd) {
			this.objectTypeCd = objectTypeCd;
		}
		public String getObjectCd() {
			return objectCd;
		}
		public void setObjectCd(String objectCd) {
			this.objectCd = objectCd;
		}
		public Map<String, Object> getExplicitParameters() {
			return explicitParameters;
		}
		public void setExplicitParameters(Map<String, Object> explicitParameters) {
			this.explicitParameters = explicitParameters;
		}

		public String getRequestURL() {
			return requestURL;
		}

		public String getRemoteAddr() {
			return remoteAddr;
		}

		public int getRemotePort() {
			return remotePort;
		}

		public void setHeaders(Map<String, String> headers) {
			this.headers = headers;
		}

		public void setRealParameters(Map<String, Object> realParameters, int maxValuesToShow) {
			this.realParameters = realParameters;
			this.maxValues = maxValuesToShow;
		}

		public void setRequestAttributes(Map<String, Object> requestAttributes) {
			this.requestAttributes = requestAttributes;
		}

		public void setSessionAttributes(Map<String, Object> sessionAttributes) {
			this.sessionAttributes = sessionAttributes;
		}

		public void setRequestURL(String requestURL) {
			this.requestURL = requestURL;
		}

		public void setRemoteAddr(String remoteAddr) {
			this.remoteAddr = remoteAddr;
		}

		public void setRemotePort(int remotePort) {
			this.remotePort = remotePort;
		}

		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}

		public void setUser(ServletUser user) {
			this.user = user;
		}

		public Map<String, Object> getRealParameters() {
			return realParameters;
		}
    }
    public RecordRequestFilter() {
        super();
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    	callId = filterConfig.getInitParameter("call-id");
    	try {
			Integer tmp = ConvertUtils.convert(Integer.class, filterConfig.getInitParameter("max-values"));
	    	if(tmp != null)
	    		maxValues = tmp.intValue();
		} catch (ConvertException e) {
			log.warn("Could not convert max-values init param to a number", e);
		}
		try {
			Integer tmp = ConvertUtils.convert(Integer.class, filterConfig.getInitParameter("batch-size"));
	    	if(tmp != null)
	    		batchSize = tmp.intValue();
		} catch (ConvertException e) {
			log.warn("Could not convert batch-size init param to a number", e);
		}
		try {
			String[] tmp = ConvertUtils.convert(String[].class, filterConfig.getInitParameter("masked-parameters"));
	        if(tmp != null)
	        	for(String s : tmp)
	        		maskedParameterNames.add(s);
	        else {
	        	maskedParameterNames.add("password");
	        	maskedParameterNames.add("confirm");
	        }
		} catch(ConvertException e) {
			log.warn("Could not convert to array of Strings", e);
		}
		try {
			asynchronous = ConvertUtils.getBoolean(filterConfig.getInitParameter("run-out-of-line"), false);
		} catch(ConvertException e) {
			log.warn("Could not convert 'run-out-of-line' to boolean", e);
		}
		try {
			pollFrequency = ConvertUtils.getInt(filterConfig.getInitParameter("poll-frequency"), 5000);
		} catch(ConvertException e) {
			log.warn("Could not convert 'run-out-of-line' to boolean", e);
		}

		errorSearchAttributes = StringUtils.split(filterConfig.getInitParameter("error-search-attributes"), StringUtils.STANDARD_DELIMS, false);
        if(errorSearchAttributes == null)
        	errorSearchAttributes = DEFAULT_ERROR_SEARCH_ATTRIBUTES;
        else
        	for(int i = 0; i < errorSearchAttributes.length; i++)
        		errorSearchAttributes[i] = errorSearchAttributes[i].trim();
		String tmp = filterConfig.getInitParameter("exclude-ip-pattern");
		if(!StringUtils.isBlank(tmp))
			setExcludeIPPattern(tmp);
		tmp = filterConfig.getInitParameter("exclude-url-pattern");
		if(!StringUtils.isBlank(tmp))
			setExcludeUrlPattern(tmp);
        if(asynchronous) {
			initRecordRequestThread();
		}
    }

    protected void initRecordRequestThread() {
    	if(batchThread == null) {
			batchThread = new Thread("RecordRequestThread") {
				@Override
				public void run() {
					while(!done) {
						try {
							Thread.sleep(pollFrequency);
						} catch(InterruptedException e) {
							log.warn("Thread interrupted; ignoring and continuing", e);
						}
						if(batch != null) {
							while(batch.getPendingCount() >= batchSize) {
								try {
									commitBatch();
								} catch(SQLException e) {
									log.error("Could not record requests", e);
								} catch(DataLayerException e) {
									log.error("Could not record requests", e);
								}
							}
		    			}
					}
				}
			};
			batchThread.setDaemon(true);
			batchThread.setPriority(3);
			batchThread.start();
		}
    }

	protected static boolean matchesPattern(Pattern pattern, String input) {
		if(pattern == null)
			return false;
		return pattern.matcher(input).matches();
	}

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
		if(request instanceof HttpServletRequest && callId != null && !matchesPattern(excludeIPPattern, request.getRemoteAddr()) && !matchesPattern(excludeUrlPattern, ((HttpServletRequest) request).getRequestURI())) {
        	long start = System.currentTimeMillis();
        	String exception = null;
        	try {
        		chain.doFilter(request, response);
        	} catch(ServletException e) {
        		exception = StringUtils.exceptionToString(e);
        		throw e;
        	} catch(IOException e) {
        		exception = StringUtils.exceptionToString(e);
        		throw e;
        	} finally {
        		if(exception == null)
        			exception = getErrorText(request, response);
        		recordRequest((HttpServletRequest)request, System.currentTimeMillis() - start, exception);
        	}
        } else {
        	chain.doFilter(request, response);
        }
    }

    protected String getErrorText(ServletRequest request, ServletResponse response) {
    	if(errorSearchAttributes != null) {
			for(String attr : errorSearchAttributes) {
    			Throwable e = (Throwable)request.getAttribute(attr);
    			if(e != null) {
    				return StringUtils.exceptionToString(e);
    			}
			}
    	}
    	return null;
    }

    public void destroy() {
    	if(batch != null && batch.getPendingCount() > 0) {
			try {
				commitBatch();
			} catch (SQLException e) {
				log.error("Could not record request", e);
			} catch (DataLayerException e) {
				log.error("Could not record request", e);
			}
    	}
    	done = true;
    	batchThread = null;
    }

	public void updateWithRequest(CallInputs ci, HttpServletRequest request) {
		StringBuffer url = request.getRequestURL();
		ci.setRequestURL(url == null ? null : url.toString());
		ci.setRemoteAddr(request.getRemoteAddr());
		ci.setRemotePort(request.getRemotePort());

		InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
		if(form != null) {
			ci.setRealParameters(form.getParameters(), maxValues);
		} else {
			if(fileUpload == null && request.getMethod().equalsIgnoreCase("POST") && RequestUtils.isMultipartContent(request)) {
				File tempDirFile = (File) request.getSession().getServletContext().getAttribute("javax.servlet.context.tempdir");
				if(tempDirFile == null)
					tempDirFile = new java.io.File(System.getProperty("java.io.tmpdir"));
				if(!tempDirFile.exists())
					try {
						tempDirFile.createNewFile();
					} catch(IOException ioe) {
						log.warn("Could not create Temp Folder, '" + tempDirFile.getAbsolutePath() + "'", ioe);
					}
				fileUpload = RequestUtils.createFileUpload(tempDirFile, 5 * 1024 * 1024);
			}
			try {
				ci.setRealParameters(RequestUtils.getRequestParameters(request, restrictedParameterNames, maskedParameterNames, fileUpload), maxValues);
			} catch(FileUploadException e) {
				log.error("Couldn't get parameters", e);
				ci.setRealParameters(Collections.emptyMap(), maxValues);
			}
		}

		Map<String, Object> requestAttributes = new LinkedHashMap<>();
		for(Enumeration<String> en = request.getAttributeNames(); en.hasMoreElements();) {
			String name = en.nextElement();
			requestAttributes.put(name, request.getAttribute(name));
		}
		ci.setRequestAttributes(requestAttributes);
		Map<String, String> headers = new LinkedHashMap<>();
		for(Enumeration<String> en = request.getHeaderNames(); en.hasMoreElements();) {
			String name = en.nextElement();
			headers.put(name, request.getHeader(name));
		}
		ci.setHeaders(headers);

		Map<String, Object> sessionAttributes = new LinkedHashMap<>();
		HttpSession session = request.getSession(false);
		if(session != null) {
			ci.setUser((ServletUser) session.getAttribute(SimpleServlet.ATTRIBUTE_USER));
			for(Enumeration<String> en = session.getAttributeNames(); en.hasMoreElements();) {
				String name = en.nextElement();
				sessionAttributes.put(name, session.getAttribute(name));
			}
			try {
				ci.setSessionId(session.getId());
			} catch(IllegalStateException e) {
				ci.setSessionId(null);
			}
		} else {
			ci.setUser(null);
			ci.setSessionId(null);
		}
		ci.setSessionAttributes(sessionAttributes);
	}
    protected void recordRequest(HttpServletRequest request, long duration, String exceptionText) {
		CallInputs ci = new CallInputs();
		ci.setDuration(duration);
		ci.setExceptionText(exceptionText);		
		updateWithRequest(ci, request);
    	if(asynchronous) {
    		try {
				initBatch();
				batch.addBatch(ci);
			} catch(CallNotFoundException e) {
				log.error("Could not record request", e);
			} catch(ParameterException e) {
				log.error("Could not record request", e);
			}
    	} else
	    	try {
					lock.lock();
		    		try {
		    			initBatch();
		    			batch.addBatch(ci);
		    			if(batch.getPendingCount() >= batchSize) {
		    				commitBatch();
		    			}
		    		} finally {
		    			lock.unlock();
		    		}
			} catch (SQLException e) {
				log.error("Could not record request", e);
			} catch (DataLayerException e) {
				log.error("Could not record request", e);
			}
    }

	protected void initBatch() throws CallNotFoundException, ParameterException {
    	if(batch == null && callId != null) {
    		call = DataLayerMgr.getGlobalDataLayer().findCall(callId);
    		batch = call.createBatchUpdate(asynchronous, 1.0f);
    	}
    }

    protected void commitBatch() throws SQLException, DataLayerException {
    	if(batch != null) {
	    	Connection conn = DataLayerMgr.getConnection(call.getDataSourceName());
			try {
				batch.executeBatch(conn, batchSize);
				conn.commit();
			} catch(SQLException e) {
				log.error("Could not record request", e);
			} finally {
				conn.close();
			}
		}
    }

	public String getExcludeUrlPattern() {
		return excludeUrlPattern == null ? null : excludeUrlPattern.pattern();
	}

	public void setExcludeUrlPattern(String excludeUrlPattern) {
		this.excludeUrlPattern = StringUtils.isBlank(excludeUrlPattern) ? null : Pattern.compile(excludeUrlPattern);
	}

	public String getExcludeIPPattern() {
		return excludeIPPattern == null ? null : excludeIPPattern.pattern();
	}

	public void setExcludeIPPattern(String excludeIPPattern) {
		this.excludeIPPattern = StringUtils.isBlank(excludeIPPattern) ? null : Pattern.compile(excludeIPPattern);
	}

	public String[] getMaskedParameters() {
		return maskedParameterNames.toArray(new String[maskedParameterNames.size()]);
	}

	public void setMaskedParameters(String[] maskedParameters) {
		maskedParameterNames.clear();
		if(maskedParameters == null || maskedParameters.length == 0) {
			maskedParameterNames.add("password");
			maskedParameterNames.add("confirm");
		} else
			for(String mp : maskedParameters)
				maskedParameterNames.add(mp);
	}
}
