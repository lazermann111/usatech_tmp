package simple.servlet;

import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import simple.text.StringUtils;

public class JspServletConfigWrapper implements ServletConfig {
	protected final ServletConfig delegate;
	protected final ServletContext context;
	protected String scratchDir;
	public JspServletConfigWrapper(ServletConfig delegate, String classpath) {
		super();
		this.delegate = delegate;
		this.context = new JspServletContextWrapper(delegate.getServletContext(), classpath);
	}

	public String getServletName() {
		return delegate.getServletName();
	}

	public ServletContext getServletContext() {
		return context;
	}

	public String getInitParameter(String name) {
		String value = delegate.getInitParameter(name);
		if("scratchdir".equals(name) && StringUtils.isBlank(value)) {
			if(scratchDir == null)
				scratchDir = RequestUtils.determineScratchDir();
			value = scratchDir;
		}
		return value;
	}

	public Enumeration<String> getInitParameterNames() {
		return delegate.getInitParameterNames();
	}


}