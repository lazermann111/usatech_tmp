package simple.servlet;

import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;

public enum LdapType {
	ACTIVE_DIRECTORY("ACTIVE_DIRECTORY"), 
	DIRECTORY_SERVER_389("389_DIRECTORY_SERVER");

	private final String value;
	protected final static EnumStringValueLookup<LdapType> lookup = new EnumStringValueLookup<LdapType>(LdapType.class);

	private LdapType(String value) {
		this.value = value;
	}

	public static LdapType getByValue(String value) throws InvalidValueException {
		return lookup.getByValue(value);
	}

	public String getValue() {
		return value;
	}
}