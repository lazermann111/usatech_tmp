/*
 * CustomActionServlet.java
 *
 * Created on October 11, 2001, 2:48 PM
 */

package simple.servlet;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.text.StringUtils;

/**
 * This extension to the SimpleServlet allows a web request to be processed by
 * any instance method on any object implementing <CODE>simple.servlet.CustomActionGroup</CODE>.
 * The method must have four arguments of the following classes:
 * <CODE>simple.servlet.Dispatcher</CODE>, <CODE>simple.servlet.InputForm</CODE>,
 * <CODE>javax.servlet.HttpServletRequest</CODE>, and
 * <CODE>javax.servlet.HttpServletResponse</CODE>. If an Action is not found for a
 * given web request's path, then the path is separated into two pieces. This servlet
 * finds the LAST occurence of one of the <CODE>method-start</CODE> characters
 * ("/.\!@" by default). Everything after that character is the method name. Everything
 * before that character is further split into sub-pieces at each occurence of any of the
 * <CODE>package-delimit</CODE> characters ("/.\-" by default). The last of these
 * sub-pieces is the bare classname. This bare classname is capitalized and the
 * String "Actions" is appended to it to form the polished classname. The other
 * sub-pieces are concatenated together using a period (.) to form the extended
 * package location. To each element in the <CODE>action-search-paths</CODE> list,
 * the CustomActionServlet appends the package location (if any) and attempts to find
 * a class whose name matches either the bare classname or the polished classname.
 * If it finds such a class, the CustomActionServlet creates an instance of that class
 * and looks for a method on that class whose name matches the method name parsed
 * from the request path. If such a method is found (and it has the four arguments mentioned previously), it
 * is invoked.
 *
 * @author  Brian S. Krug
 */
public class CustomActionServlet extends SimpleServlet {
    /**
	 *
	 */
	private static final long serialVersionUID = -349011424571L;
	protected char[] methodStartChars = "/.\\!@".toCharArray();
    protected char[] packageDelimiters = "/.\\".toCharArray();
    protected static final Class<?>[] ACTIONS_PARAMETER_CLASSES = new Class[] {
        Dispatcher.class, InputForm.class, HttpServletRequest.class, HttpServletResponse.class};
    protected Map<List<String>,Object> actionGroupCache = new HashMap<List<String>,Object>();
    protected String[] actionSearchPaths;


    /** Creates new CustomActionServlet */
    public CustomActionServlet() {
    }
    /**
     * Handles the initialization of the servlet. This method is called when
     * the servlet is initialized. It parses the "action-search-paths"
     * init param.
     * @throws ServletException
     */
    @Override
	protected void configChanged() throws ServletException {
        super.configChanged();
        //make sure there is at least one element in the array
        if(actionSearchPaths == null || actionSearchPaths.length == 0) 
        	actionSearchPaths = new String[] {""};
        else for(int i = 0; i < actionSearchPaths.length; i++) {
            if(actionSearchPaths[i].equals(".") || actionSearchPaths[i].equals("")) actionSearchPaths[i] = "";
            else if(!actionSearchPaths[i].endsWith(".")) actionSearchPaths[i] += ".";
        }
        log.debug("Action Search Path = " + StringUtils.toStringList(actionSearchPaths) + " (" + actionSearchPaths.length + " elements)");
    }

    /**
     * Searches for the class that matches the specified list of sub-pieces of
     * the URL using the actionSearchPaths as the base package. See the class
     * overview for more information.
     */
    protected Class<?> getActionsClass(List<String> pkgs) {
        //get from xml
        //get as classname in specified pkgs
        String pack;
        if(pkgs.size() > 1) pack = StringUtils.join((String[])pkgs.toArray(),".",0,pkgs.size()-1) + ".";
        else pack = "";
        String className = pkgs.get(pkgs.size()-1);
        String[] classNames = new String[]{pack + className.substring(0,1).toUpperCase() + className.substring(1) + "Actions",
            pack + className
        };
        for(int i = 0; i < actionSearchPaths.length; i++) {
            for(int k = 0; k < classNames.length; k++) {
                try {
                    return Class.forName(actionSearchPaths[i] + classNames[k]);
                } catch(ClassNotFoundException cnfe) {
                    log.debug("Class '" + actionSearchPaths[i] + classNames[k] + "' does not exist");
                }
            }
        }
        log.debug("No class found matching any of the following: " + StringUtils.toStringList(classNames));
        return null;
    }

    /**
     * Returns an Action object that matches the given path.
     */
    @Override
	protected Action getAction(String path) {
        Action act = super.getAction(path);
        if(act != null) return act;
        StringBuilder sb = new StringBuilder(path);
        int p0 = StringUtils.lastIndexOf(sb.toString(), methodStartChars);
        String methodName;
        methodName = sb.substring(p0+1);
        sb.setLength(p0);
        StringUtils.replace(sb, packageDelimiters, packageDelimiters[0]);
        path = sb.toString() + packageDelimiters[0] + methodName;
        log.debug("Create new Action for path '" + path + "'");
        act = createAction(sb.toString(), methodName);
        if(act != null) actions.put(path, act);
        return act;
    }

    /**
     * Creates a new Action object for the given path and returns it.
     */
    protected Action createAction(String className, String methodName) {
        Object actionGroup = null;
        log.debug("Method Name = '" + methodName + "'");
        if(className.length() > 0) {
            List<String> pkgs = Arrays.asList(StringUtils.split(className,packageDelimiters,false));
            log.debug("Get Actions Object for '" + pkgs + "'");
            actionGroup = actionGroupCache.get(pkgs);
            if(actionGroup == null) {
               log.debug("Create new Actions Object for '" + pkgs + "'");
               Class<?> clazz = getActionsClass(pkgs);
               if(clazz != null) try {
                   actionGroup = clazz.newInstance();
                   actionGroupCache.put(pkgs, actionGroup);
               } catch(InstantiationException ise) {
                   if(log.isDebugEnabled()) log.warn("While creating new ActionGroup Object '" + clazz.getName() + "'", ise);
               } catch(IllegalAccessException iae) {
                   if(log.isDebugEnabled()) log.warn("While creating new ActionGroup Object '" + clazz.getName() + "'", iae);
               }
            }
        }
        if(actionGroup == null) actionGroup = this;
        try {
            Method m = actionGroup.getClass().getDeclaredMethod(methodName, ACTIONS_PARAMETER_CLASSES);
            if(m == null) throw new NoSuchMethodException();
            log.debug("Found method '" + m.getName() + "'");
            return new CustomAction(actionGroup, m);
        } catch(NoSuchMethodException nsme) {
            if(log.isDebugEnabled()) log.warn("While getting method '" + methodName + "' on ActionGroup Object '" + actionGroup.getClass().getName() + "'", nsme);
        } catch(SecurityException se) {
            if(log.isDebugEnabled()) log.warn("While getting method '" + methodName + "' on ActionGroup Object '" + actionGroup.getClass().getName() + "'", se);
        }
        return null;
    }
	public char[] getMethodStartChars() {
		return methodStartChars;
	}
	public void setMethodStartChars(char[] methodStartChars) {
		this.methodStartChars = methodStartChars;
	}
	public char[] getPackageDelimiters() {
		return packageDelimiters;
	}
	public void setPackageDelimiters(char[] packageDelimiters) {
		this.packageDelimiters = packageDelimiters;
	}
	public String[] getActionSearchPaths() {
		return actionSearchPaths;
	}
	public void setActionSearchPaths(String[] actionSearchPaths) {
		this.actionSearchPaths = actionSearchPaths;
	}

}
