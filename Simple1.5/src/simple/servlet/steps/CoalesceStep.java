/*
 * SetStep.java
 *
 * Created on February 6, 2004, 5:49 PM
 */

package simple.servlet.steps;

import java.util.ArrayList;
import java.util.List;

/**
 * Takes a set of parameters and turns them into an array
 * @author  Brian S. Krug
 */
public class CoalesceStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property result. */
    private String result;
    
    /** Holds value of property prefix. */
    private String prefix;
    
    /** Holds value of property suffix. */
    private String suffix;
    
    private String maxIndex;
    
    private int min = 1;
    
    /** Creates a new instance of SetStep */
    public CoalesceStep() {
    }
    
    public void perform(simple.servlet.Dispatcher dispatcher, simple.servlet.InputForm form, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException {
        List<Object> list = new ArrayList<Object>();
        int max = -1;
        if(getMaxIndex() != null && getMaxIndex().trim().length() > 0) {
            max = form.getInt(getMaxIndex(), false, -1);
        }
        if(prefix == null) prefix = "";
        if(suffix == null) suffix = "";   
        for(int i = min; max == -1 || i <= max; i++) {
            log.debug("Looking for '" + prefix + i + suffix + "'");
            Object o = form.getAttribute(prefix + i + suffix);
            if(o != null) {
                list.add(o);
            } else if(max == -1) {
                break;
            }
        }
        log.debug("Coalesced " + list.size() +  " entries into " + list);
    	form.setAttribute(getResult(), list.toArray(), (scope == null ? simple.servlet.RequestUtils.REQUEST_SCOPE : scope));
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * @return Returns the result.
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result The result to set.
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * @return Returns the prefix.
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * @param prefix The prefix to set.
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * @return Returns the suffix.
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * @param suffix The suffix to set.
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    /**
     * @return Returns the maxIndex.
     */
    public String getMaxIndex() {
        return maxIndex;
    }

    /**
     * @param maxIndex The maxIndex to set.
     */
    public void setMaxIndex(String maxIndex) {
        this.maxIndex = maxIndex;
    }

    /**
     * @return Returns the min.
     */
    public int getMin() {
        return min;
    }

    /**
     * @param min The min to set.
     */
    public void setMin(int min) {
        this.min = min;
    }
    
}
