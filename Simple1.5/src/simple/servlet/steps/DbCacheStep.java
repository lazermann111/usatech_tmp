package simple.servlet.steps;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.Call;
import simple.db.CallCacher;
import simple.db.CallNotFoundException;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.ParameterException;
import simple.io.Log;
import simple.results.ResultsCreationException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.StepConfigException;
import simple.text.StringUtils;
import simple.util.concurrent.CacheCountTracker;
import simple.util.concurrent.DefaultLockSegmentFutureCache;
import simple.util.concurrent.LogCacheCountTracker;

public class DbCacheStep extends AbstractStep 
{
    public static final String CLEAR_FLAG = "DbCacheStep.clearCache";
   
    private static final Log log = Log.getLog();

    protected final CallCacher cacher;
    protected DefaultLockSegmentFutureCache<Object[],Object[]> cache;
    protected String callId;
    protected String[] results;
    protected DataLayer dataLayer = DataLayerMgr.getGlobalDataLayer();

    /** Holds value of property dataSource. */
    protected String dataSource;

    /** Holds value of property commit. */
    protected boolean commit;

    protected int cacheCapacity = 100;
    protected int cacheSize = 100;
    protected float cacheLoadFactor = 0.75f;
    protected int cacheConcurrency = 16;
    protected long cacheTime = 10 * 60 * 1000; //10 minutes
    protected boolean trackCounts = false;

    protected static CacheCountTracker countTracker = new LogCacheCountTracker();

    public DbCacheStep() {
    	cacher = new CallCacher() {
			@Override
			protected String getCacheName(Call call) {
				return getCallId();
			}

			@Override
			protected Call getCall() throws CallNotFoundException {
				return dataLayer.findCall(getCallId());
			}

			@Override
			protected Connection getConnection(Call call) throws SQLException, DataLayerException {
				String ds = findDataSource(call);
		    	return dataLayer.getConnection(ds);
			}

			@Override
			protected CacheCountTracker getCountTracker() {
				return countTracker;
			}

			@Override
			protected boolean doCommit() {
				return commit;
			}
    	};
    }

    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	checkClearCache(request);
    	Object[] res;
    	try {
			res = cacher.getResults(form);
		} catch(CallNotFoundException e) {
			throw new ServletException("Could not find call '" + getCallId() + "'", e);
		} catch(ParameterException e) {
			throw new ServletException("Could not extract or save call parameters from form", e);
		} catch(ExecutionException e) {
			throw new ServletException("While executing call '" + getCallId() + "'", (e.getCause() != null ? e.getCause() : e));
		} catch(ResultsCreationException e) {
			throw new ServletException("Could not retrieve results", e);
		}

		String[] resultsNames = getResults();
    	if(resultsNames != null)
			for(int i = 0; i < res.length && i < resultsNames.length; i++) {
				if(!StringUtils.isBlank(resultsNames[i])) {
					form.setAttribute(resultsNames[i], res[i]);
					log.debug("Set attribute '" + resultsNames[i] + "' to " + res[i]);
				}
	    	}
    }

    @Override
	public void configure(java.util.Map<String,?> props) throws StepConfigException {
        super.configure(props);
        //create cache
        cache = new DefaultLockSegmentFutureCache<Object[], Object[]>(getCacheCapacity(), getCacheLoadFactor(), getCacheConcurrency());
        cache.setExpireTime(getCacheTime());
        cache.setMaxSize(getCacheSize());
        cache.setTrackingCounts(isTrackCounts());
		cache.setValueFactory(cacher);
		cacher.setCache(cache);
    }

    /** Getter for property dataSource.
     * @return Value of property dataSource.
     *
     */
    public String getDataSource() {
        return this.dataSource;
    }

    protected String findDataSource(Call call) {
        if(dataSource == null) {
            log.debug("Getting DataSource from call...");
            dataSource = call.getDataSourceName();
        }
        return this.dataSource;
    }

    /** Setter for property dataSource.
     * @param dataSource New value of property dataSource.
     *
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    /** Getter for property commit.
     * @return Value of property commit.
     *
     */
    public boolean isCommit() {
        return this.commit;
    }

    /** Setter for property commit.
     * @param commit New value of property commit.
     *
     */
    public void setCommit(boolean commit) {
        this.commit = commit;
    }

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public String[] getResults() {
		return results;
	}

	public void setResults(String[] results) {
		this.results = results;
	}

	public int getCacheCapacity() {
		return cacheCapacity;
	}

	public void setCacheCapacity(int cacheCapacity) {
		this.cacheCapacity = cacheCapacity;
	}

	public int getCacheConcurrency() {
		return cacheConcurrency;
	}

	public void setCacheConcurrency(int cacheConcurrency) {
		this.cacheConcurrency = cacheConcurrency;
	}

	public float getCacheLoadFactor() {
		return cacheLoadFactor;
	}

	public void setCacheLoadFactor(float cacheLoadFactor) {
		this.cacheLoadFactor = cacheLoadFactor;
	}

	public int getCacheSize() {
		return cacheSize;
	}

	public void setCacheSize(int cacheSize) {
		this.cacheSize = cacheSize;
	}

	public long getCacheTime() {
		return cacheTime;
	}

	public void setCacheTime(long cacheTime) {
		this.cacheTime = cacheTime;
	}

	public boolean isTrackCounts() {
		return trackCounts;
	}

	public void setTrackCounts(boolean trackCounts) {
		this.trackCounts = trackCounts;
	}
	
	public static void clearCache(ServletRequest request, String callId) throws ServletException {
	   List<String> callsToClear = (List<String>) RequestUtils.getAttribute(request, CLEAR_FLAG, false);
	   if (callsToClear == null) {
	      callsToClear = new ArrayList<String>();
	      RequestUtils.setAttribute(request, CLEAR_FLAG, callsToClear, RequestUtils.REQUEST_SCOPE);
	   }
	   callsToClear.add(callId);
	}
	
	protected void checkClearCache(ServletRequest request) throws ServletException {
	   List<String> callsToClear = (List<String>) RequestUtils.getAttribute(request, CLEAR_FLAG, false);
	   if (callsToClear == null)
	      return;
	   if (callsToClear.contains(getCallId())) {
	      cache.clear();
	      callsToClear.remove(getCallId());
	   }
	}
}

