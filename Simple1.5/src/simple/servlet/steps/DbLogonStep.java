/*
 * DbLogonStep.java
 *
 * Created on December 26, 2003, 2:30 PM
 */

package simple.servlet.steps;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Arrays;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.db.DataLayerException;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.security.SecureHash;
import simple.servlet.BasicServletUser;
import simple.servlet.InputForm;
import simple.servlet.ServletUser;
import simple.text.StringUtils;

/** Uses a database to authenticate the user information (username & password)
 *
 * @author  Brian S. Krug
 */
public class DbLogonStep extends AbstractLogonStep {
    private static Log log = Log.getLog();
    /** Holds value of property userCall. */
	protected String userCall = "GET_USER";
    
    /** Holds value of property userClass. */
	protected Class<? extends ServletUser> userClass = BasicServletUser.class;
    
    /** Holds value of property passwordProperty. */
	protected String passwordProperty = "password";
	protected String passwordHashProperty = "passwordHash";
	protected String passwordSaltProperty = "passwordSalt";
	protected String passwordAlgProperty = "passwordAlg";
	protected boolean usingHash = false;
    
    /** Holds value of property privilegesCall. */
	protected String privilegesCall = "GET_PRIVILEGES";

	protected boolean rememberUserName = false;
	protected String rememberUserNameProperty = null;
    
    /** Creates a new instance of DbLogonStep */
    public DbLogonStep() {
    }
    
    public ServletUser authenticateUser(simple.servlet.InputForm form) throws ServletException {
        try {
            Results results = simple.db.DataLayerMgr.executeQuery(getUserCall(), form, false);
            if(results.next()) {
                //check password
				boolean passwordResult;
				if(isUsingHash())
					passwordResult = checkPasswordHash(form.get(getPasswordProperty()), results.getValue(getPasswordHashProperty(), byte[].class), results.getValue(getPasswordSaltProperty(), byte[].class), results.getValue(getPasswordAlgProperty(), String.class));
				else
					passwordResult = checkPassword(form.get(getPasswordProperty()), results.getValue(getPasswordProperty()));
				if(!passwordResult)
                    throw new LoginFailureException("Invalid password", LoginFailureException.INVALID_PASSWORD); 
                //ok, now create the user
                try {
                    log.debug("Creating user of class " + getUserClass().getName());
                    ServletUser user = getUserClass().newInstance();
                    results.fillBean(user);
                    if(user instanceof BasicServletUser && getPrivilegesCall() != null && getPrivilegesCall().trim().length() > 0) {
                        results = simple.db.DataLayerMgr.executeQuery(getPrivilegesCall(), user, false);
                        BasicServletUser bsu = (BasicServletUser) user;
                        while(results.next()) {
                            bsu.addPrivilege(ConvertUtils.convert(String.class, results.getValue(1)));
                        }
                    }
                    log.debug("User " + user.getUserName() + " successfully logged on");
                    return user;
                } catch(InstantiationException e) {
                    //log.warn("Could not create user of class '" + getUserClass().getName() + "'", e);
                    throw new ServletException("Could not create user of class '" + getUserClass().getName() + "'", e);
                } catch (IllegalAccessException e) {
                    //log.warn("Could not create user of class '" + getUserClass().getName() + "'", e);
                    throw new ServletException("Could not create user of class '" + getUserClass().getName() + "'", e);
                } catch (BeanException e) {
                    //log.warn("Could not create user of class '" + getUserClass().getName() + "'", e);
                    throw new ServletException("Could not create user of class '" + getUserClass().getName() + "'", e);
                }
            } else { // user doesn't exist!
                throw new LoginFailureException("User does not exist", LoginFailureException.INVALID_USERNAME);
            }  
		} catch(SQLException sqle) {
            throw new ServletException("Could not run the query '" + getUserCall() + "'", sqle);
		} catch(DataLayerException dle) {
            throw new ServletException("Could not process the query '" + getUserCall() + "'", dle);
		} catch(ConvertException ce) {
            throw new ServletException("Could not convert values for the query '" + getUserCall() + "'", ce);            
		} catch(NoSuchAlgorithmException e) {
			throw new ServletException("Could not hash password", e);
		}
    }
    
	protected boolean checkPasswordHash(Object enteredPassword, byte[] storedPasswordHash, byte[] storedPasswordSalt, String storedPasswordAlg) throws NoSuchAlgorithmException {
		if(enteredPassword == null)
			enteredPassword = "";
		byte[] enteredPasswordBytes = (enteredPassword instanceof MaskedString ? ((MaskedString) enteredPassword).getValue() : enteredPassword.toString()).getBytes();
		byte[] enteredPasswordHash = SecureHash.getHash(enteredPasswordBytes, storedPasswordSalt, storedPasswordAlg);
		return Arrays.equals(enteredPasswordHash, storedPasswordHash);
	}

	protected boolean checkPassword(Object enteredPassword, Object storedPassword) {
        if(enteredPassword == null) enteredPassword = "";
        if(storedPassword == null) storedPassword = "";
        String s1 = (enteredPassword instanceof MaskedString ? ((MaskedString)enteredPassword).getValue() : enteredPassword.toString());
        return s1.equals(storedPassword.toString());
    }
    
    /** Getter for property userCall.
     * @return Value of property userCall.
     *
     */
    public String getUserCall() {
        return this.userCall;
    }
    
    /** Setter for property userCall.
     * @param userCall New value of property userCall.
     *
     */
    public void setUserCall(String userCall) {
        this.userCall = userCall;
    }
    
    /** Getter for property userClass.
     * @return Value of property userClass.
     *
     */
    public Class<? extends ServletUser> getUserClass() {
        return this.userClass;
    }
    
    /** Setter for property userClass.
     * @param userClass New value of property userClass.
     *
     */
    public void setUserClass(Class<? extends ServletUser> userClass) {
        if(!ServletUser.class.isAssignableFrom(userClass)) throw new IllegalArgumentException("Class must be sub-class of simple.servlet.ServletUser");
        this.userClass = userClass;
    }
    
    /** Getter for property passwordProperty.
     * @return Value of property passwordProperty.
     *
     */
    public String getPasswordProperty() {
        return this.passwordProperty;
    }
    
    /** Setter for property passwordProperty.
     * @param passwordProperty New value of property passwordProperty.
     *
     */
    public void setPasswordProperty(String passwordProperty) {
        this.passwordProperty = passwordProperty;
    }
    
    /** Getter for property privilegesCall.
     * @return Value of property privilegesCall.
     *
     */
    public String getPrivilegesCall() {
        return this.privilegesCall;
    }
    
    /** Setter for property privilegesCall.
     * @param privilegesCall New value of property privilegesCall.
     *
     */
    public void setPrivilegesCall(String privilegesCall) {
        this.privilegesCall = privilegesCall;
    }

	public boolean isRememberUserName() {
		return rememberUserName;
	}

	public void setRememberUserName(boolean rememberUserName) {
		this.rememberUserName = rememberUserName;
	}
    
	@Override
	protected boolean rememberUserName(InputForm form) {
		if(!StringUtils.isBlank(getRememberUserNameProperty()))
			return ConvertUtils.getBooleanSafely(form.getAttribute(getRememberUserNameProperty()), false);
		return isRememberUserName();
	}

	public String getPasswordHashProperty() {
		return passwordHashProperty;
	}

	public void setPasswordHashProperty(String passwordHashProperty) {
		this.passwordHashProperty = passwordHashProperty;
	}

	public String getPasswordSaltProperty() {
		return passwordSaltProperty;
	}

	public void setPasswordSaltProperty(String passwordSaltProperty) {
		this.passwordSaltProperty = passwordSaltProperty;
	}

	public String getPasswordAlgProperty() {
		return passwordAlgProperty;
	}

	public void setPasswordAlgProperty(String passwordAlgProperty) {
		this.passwordAlgProperty = passwordAlgProperty;
	}

	public boolean isUsingHash() {
		return usingHash;
	}

	public void setUsingHash(boolean usingHash) {
		this.usingHash = usingHash;
	}

	public String getRememberUserNameProperty() {
		return rememberUserNameProperty;
	}

	public void setRememberUserNameProperty(String rememberUserNameProperty) {
		this.rememberUserNameProperty = rememberUserNameProperty;
	}
}
