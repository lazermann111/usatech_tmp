/*
 * Created on Sep 30, 2005
 *
 */
package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.text.Format;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.sql.Expression;
import simple.text.StringUtils;
import simple.util.MapBackedSet;

public class JsonBuilder {
	protected static final Set<String> EMPTY_EXCLUDED = Collections.emptySet();

	protected static interface ValueWriter {
		public void writeValue(Object value, Writer writer) throws IOException, ConvertException;
	}

	protected static final ValueWriter stringValueWriter = new ValueWriter() {
		public void writeValue(Object value, Writer writer) throws IOException, ConvertException {
			String text = ConvertUtils.getString(value, "");
			writer.write('"');
			int last = 0;
			for(int i = 0; i < text.length(); i++) {
				char ch = text.charAt(i);
				switch(ch) {
					case '"':
					case '\\':
					case '/':
					case '\b':
					case '\f':
					case '\n':
					case '\r':
					case '\t':
						if(i > last)
							writer.write(text, last, i - last);
						writer.write('\\');
						last = i;
						break;
					default:
						if(ch >= 0x7F || ch < 0x20) {
							if(i > last)
								writer.write(text, last, i - last);
							writer.write("\\u");
							writer.write(StringUtils.toHex((short) ch));
							last = i + 1;
						}
				}
			}
			if(text.length() > last)
				writer.write(text, last, text.length() - last);
			writer.write('"');
		}
	};
	protected static final ValueWriter numberValueWriter = new ValueWriter() {
		public void writeValue(Object value, Writer writer) throws IOException, ConvertException {
			Number num = ConvertUtils.convertRequired(Number.class, value);
			writer.write(num.toString());
		}
	};
	protected static final ValueWriter booleanValueWriter = new ValueWriter() {
		public void writeValue(Object value, Writer writer) throws IOException, ConvertException {
			boolean bool = ConvertUtils.getBoolean(value);
			writer.write(String.valueOf(bool));
		}
	};

	protected final Map<Class<?>, ValueWriter> simpleInterfaces = new HashMap<Class<?>, ValueWriter>();
	protected final Map<Class<?>, ValueWriter> simpleClasses = new HashMap<Class<?>, ValueWriter>();

	public JsonBuilder() {
        registerDefaultSimpleTypes();
    }

    protected void registerDefaultSimpleTypes() {
		registerSimpleType(Number.class, numberValueWriter);
		registerSimpleType(Character.class, stringValueWriter);
		registerSimpleType(java.util.Date.class, numberValueWriter);
		registerSimpleType(Class.class, stringValueWriter);
		registerSimpleType(Boolean.class, booleanValueWriter);
		registerSimpleType(CharSequence.class, stringValueWriter);
		registerSimpleType(Format.class, stringValueWriter);
		registerSimpleType(Expression.class, stringValueWriter);
		registerSimpleType(Reader.class, stringValueWriter);
	}

	public void write(Object obj, Writer writer, Set<String> included, Set<String> excluded) throws IOException, ConvertException {
        if(obj != null) {
			writeJSON(obj, writer, included, excluded == null ? EMPTY_EXCLUDED : excluded, "", new MapBackedSet<Object>(new IdentityHashMap<Object, Object>()));
        }
    }

	protected void writeJSON(Object obj, Writer writer, Set<String> included, Set<String> excluded, String prefix, Set<Object> processed) throws IOException, ConvertException {
		if(obj == null)
			writer.write("null");
		else {
			ValueWriter valueWriter = getValueWriter(obj.getClass());
			if(valueWriter != null)
				valueWriter.writeValue(obj, writer);
			else if(isArrayType(obj.getClass())) {
				if(!processed.add(obj))
					throw new ConvertException("Recursion Detected on '" + prefix + "'", Object.class, obj);
				writer.write('[');
				Iterator<?> iter = ConvertUtils.asIterable(obj).iterator();
				for(int i = 0; iter.hasNext(); i++) {
					if(i > 0)
						writer.write(',');
					writeJSON(iter.next(), writer, null, excluded, prefix + '[' + i + ']', processed);
				}
				writer.write(']');
				processed.remove(obj);
			} else if(Map.class.isAssignableFrom(obj.getClass())) {
				if(!processed.add(obj))
					throw new ConvertException("Recursion Detected on '" + prefix + "'", Object.class, obj);
				writer.write('{');
				if(prefix.length() > 0)
					prefix += '.';
				boolean first = true;
				for(Map.Entry<?, ?> e : ((Map<?, ?>) obj).entrySet()) {
					String name = ConvertUtils.getString(e.getKey(), "");
					if(included != null && !included.contains(prefix + name))
						continue;
					if(excluded.contains(prefix + name))
						continue;
					if(first)
						first = false;
					else
						writer.write(',');
					stringValueWriter.writeValue(name, writer);
					writer.write(':');
					writeJSON(e.getValue(), writer, null, excluded, prefix + name, processed);
				}
				writer.write('}');
				processed.remove(obj);
			} else {
				if(!processed.add(obj))
					throw new ConvertException("Recursion Detected on '" + prefix + "'", Object.class, obj);
				writer.write('{');
				try {
					Set<String> propNames = ReflectionUtils.getPropertyNames(obj);
					if(propNames != null) {
						if(prefix.length() > 0)
							prefix += '.';
						boolean first = true;
						for(String name : propNames) {
							if(name.equals("class"))
								continue;
							if(included != null && !included.contains(prefix + name))
								continue;
							if(excluded.contains(prefix + name))
								continue;
							if(first)
								first = false;
							else
								writer.write(',');
							stringValueWriter.writeValue(name, writer);
							writer.write(':');
							Object value;
							if(obj instanceof Throwable && name.equals("stackTrace")) {
								value = StringUtils.exceptionToString((Throwable) obj);
							} else {
								value = ReflectionUtils.getSimpleProperty(obj, name, true);
							}
							writeJSON(value, writer, null, excluded, prefix + name, processed);
						}
					}
				} catch(IntrospectionException e) {
					throw new ConvertException("Could not read properties of object", String.class, obj, e);
				} catch(IllegalAccessException e) {
					throw new ConvertException("Could not read properties of object", String.class, obj, e);
				} catch(InvocationTargetException e) {
					throw new ConvertException("Could not read properties of object", String.class, obj, e);
				}
				writer.write('}');
				processed.remove(obj);
			}
		}
	}

	protected boolean isArrayType(Class<?> cls) {
		return ConvertUtils.isIterableType(cls);
	}

	protected ValueWriter getValueWriter(Class<?> cls) {
        // this is strange but we don't need to print the string representation of Object (since it is meaningless)
        // also this makes the recursion on superclasses work
		if(cls == null || Object.class.equals(cls))
			return null;
		ValueWriter valueWriter = simpleClasses.get(cls);
		if(valueWriter != null)
			return valueWriter;
        Class<?>[] ifs = cls.getInterfaces();
        if(ifs != null)
			for(Class<?> inter : ifs) {
				valueWriter = simpleInterfaces.get(inter);
				if(valueWriter != null)
					return valueWriter;
			}
		return getValueWriter(cls.getSuperclass());
    }

	protected void registerSimpleType(Class<?> cls, ValueWriter valueWriter) {
    	if(cls.isInterface())
			simpleInterfaces.put(cls, valueWriter);
    	else
			simpleClasses.put(cls, valueWriter);
    }
}
