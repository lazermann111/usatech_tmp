/*
 * SetStep.java
 *
 * Created on February 6, 2004, 5:49 PM
 */

package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.servlet.AbstractInputForm;
import simple.servlet.RequestUtils;
import simple.servlet.StepConfigException;

/**
 *
 * @author  Brian S. Krug
 */
public class SetCookieStep extends AbstractStep {
    private static final Log log = Log.getLog();
    
    private Cookie cookie;
    
    /** Holds value of property bean. */
    private String bean;
    
    private String type;
    
    private String format;
    
    private boolean clear;
    
    /** Creates a new instance of SetStep */
    public SetCookieStep() {
    }
    
    public void perform(simple.servlet.Dispatcher dispatcher, simple.servlet.InputForm form, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException {
    	if(log.isDebugEnabled()) {
    		StringBuilder sb = new StringBuilder("Cookies before this step: ");
    		if(request.getCookies() != null)
    			for(Cookie c : request.getCookies()) {
    			try {
					sb.append(ReflectionUtils.toPropertyMap(c)).append(",");
				} catch (IntrospectionException e) {
					log.warn(e);
				}
    		}
    		log.debug(sb.substring(0, sb.length() - 1));
    	}
    	Cookie newCookie;
    	if(cookie.getName() != null && cookie.getName().trim().length() > 0) {
    		if(isClear()) {
    			RequestUtils.removeCookies(request, response, cookie.getName());
    			newCookie = null;
    		} else if(form instanceof AbstractInputForm)
    			newCookie = ((AbstractInputForm)form).getCookie(cookie.getName());
    		else
    			newCookie = (Cookie)form.get("cookie:" + cookie.getName());
    		if(newCookie == null)
    			newCookie = (Cookie)cookie.clone();
    		else {
				newCookie.setComment(cookie.getComment());	
				if(cookie.getDomain() != null)
					newCookie.setDomain(cookie.getDomain());	
				newCookie.setMaxAge(cookie.getMaxAge());	
				newCookie.setPath(cookie.getPath());	
				newCookie.setSecure(cookie.getSecure());	
				newCookie.setValue(cookie.getValue());	
				newCookie.setVersion(cookie.getVersion());	
			} 
    	} else
    		newCookie = (Cookie)cookie.clone();
        if(getBean() != null) {
            Object value = form.get(getBean());
            if(value != null) {
            	if(type != null) try {
                    log.debug("Converting value to " + type);
                    value = ConvertUtils.convert(Class.forName(type), value);           
                } catch(ConvertException e) {
                    throw new ServletException(e);
                } catch (ClassNotFoundException e) {
                    log.warn("Class Not Found: " + type, e);
                    throw new ServletException(e);
                }
                newCookie.setValue(format == null ? value.toString() : ConvertUtils.formatObject(value, format));
            }
        } 
        /*if(newCookie.getDomain() == null)
        	newCookie.setDomain(request.getServerName());*/
    	RequestUtils.setHttpOnlyCookieHeader(request, response, newCookie);
    	if(log.isDebugEnabled()) {
    		try {
				log.debug("Adding cookie: " + ReflectionUtils.toPropertyMap(newCookie));
			} catch (IntrospectionException e) {
				log.warn(e);
			}   		
    	}
    }
       
    @Override
	public void configure(Map<String, ?> props) throws StepConfigException {
    	String name = (String)props.get("name");
    	if(name == null) throw new StepConfigException("Name must be provided");
    	cookie = new Cookie(name, null);
        try {
			simple.bean.ReflectionUtils.populateProperties(cookie, props);
		} catch (IntrospectionException e) {
			throw new StepConfigException(e);
		} catch (IllegalAccessException e) {
			throw new StepConfigException(e);
		} catch (InvocationTargetException e) {
			throw new StepConfigException(e);
		} catch (InstantiationException e) {
			throw new StepConfigException(e);
		} catch (ConvertException e) {
			throw new StepConfigException(e);
		} catch(ParseException e) {
			throw new StepConfigException(e);
		}
		super.configure(props);
	}

	/**
	 * @return Returns the bean.
	 */
	public String getBean() {
		return bean;
	}
	/**
	 * @param bean The bean to set.
	 */
	public void setBean(String bean) {
		this.bean = bean;
	}

    /**
     * @return Returns the format.
     */
    public String getFormat() {
        return format;
    }

    /**
     * @param format The format to set.
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }

	public boolean isClear() {
		return clear;
	}

	public void setClear(boolean clear) {
		this.clear = clear;
	}
}
