package simple.servlet.steps;

import javax.servlet.ServletException;

import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

public class JspStep extends AbstractStep {
	private static final Log log = Log.getLog();
    /** Holds value of property page. */
    private String page;
    private String contentType = "text/html";
    
    public JspStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        if(contentType != null && contentType.trim().length() > 0) response.setContentType(getContentType());
		if(log.isInfoEnabled())
			log.info("Including jsp page '" + page + "'");
		dispatcher.include(page);
    }    

    /** Getter for property page.
     * @return Value of property page.
     *
     */
    public String getPage() {
        return this.page;
    }
    
    /** Setter for property page.
     * @param page New value of property page.
     *
     */
    public void setPage(String page) {
        this.page = page;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }    
}

