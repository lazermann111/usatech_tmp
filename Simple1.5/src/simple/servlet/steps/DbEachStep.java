/*
 * DbScrollStep.java
 *
 * Created on February 4, 2004, 2:52 PM
 */

package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Iterator;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.Call;
import simple.db.CallNotFoundException;
import simple.db.ParameterException;
import simple.servlet.InputForm;

/**
 * Executes a set of database calls for each of the objects in a specified array or collection
 * @author  Brian S. Krug
 */
public class DbEachStep extends DbStep {
    private static final simple.io.Log log = simple.io.Log.getLog();

    /** Holds value of property key. */
    private String key;

    /** Holds value of property affix. */
    private String affix;
    private boolean prefix;

    /** Holds value of property list. */
    private String list;

    /** Holds value of property name. */
    private String name;

    /** Creates a new instance of DbScrollStep */
    public DbEachStep() {
    }

    @Override
	protected void executeCalls(Connection conn, InputForm form) throws ConvertException, CallNotFoundException, ParameterException, SQLException {
        Iterator<?> listIter = getIterator(form);
        if(listIter == null) return;
        while(listIter.hasNext()) {
            Object element = listIter.next();
            if(name != null && name.trim().length() > 0) element = java.util.Collections.singletonMap(name, element);
            // from superclass
            String scope = getResultsScope();
            for(DbCall cc : calls) {
                Call call = dataLayer.findCall(cc.id);
                Object[] res = call.executeCall(conn, element, null);
                for(int i = 0; i < res.length && i < cc.results.length; i++) {
                    String name = getResultName(element, cc.results[i]);
                    form.setAttribute(name, res[i], scope);
                    log.debug("Set attribute '" + name + "' to " + res[i] + " in " + scope);
                }
            }
        }
    }

    protected Iterator<?> getIterator(InputForm form) throws ConvertException {
        return ConvertUtils.convert(Iterator.class, form.getAttribute(getList()));
    }

    protected String getResultName(Object element, String baseName) throws ConvertException {
        String keyValue;
        try {
            keyValue = ConvertUtils.convert(String.class, ReflectionUtils.getProperty(element, getKey()));
        } catch (IntrospectionException e) {
            throw new ConvertException("Could not get property on object", Object.class, element, e);
        } catch (IllegalAccessException e) {
            throw new ConvertException("Could not get property on object", Object.class, element, e);
        } catch (InvocationTargetException e) {
            throw new ConvertException("Could not get property on object", Object.class, element, e);
        } catch(ParseException e) {
            throw new ConvertException("Could not get property on object", Object.class, element, e);
		}
        if(prefix) return keyValue + baseName;
        else return baseName + keyValue;
    }

    /** Getter for property key.
     * @return Value of property key.
     *
     */
    public String getKey() {
        return this.key;
    }

    /** Setter for property key.
     * @param key New value of property key.
     *
     */
    public void setKey(String key) {
        this.key = key;
    }

    /** Getter for property affix.
     * @return Value of property affix.
     *
     */
    public String getAffix() {
        return this.affix;
    }

    /** Setter for property affix.
     * @param affix New value of property affix.
     *
     */
    public void setAffix(String affix) {
        this.affix = affix;
        prefix = "PREFIX".equalsIgnoreCase(affix);
    }

    /** Getter for property list.
     * @return Value of property list.
     *
     */
    public String getList() {
        return this.list;
    }

    /** Setter for property list.
     * @param list New value of property list.
     *
     */
    public void setList(String list) {
        this.list = list;
    }

    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public String getName() {
        return this.name;
    }

    /** Setter for property name.
     * @param name New value of property name.
     *
     */
    public void setName(String name) {
        this.name = name;
    }

}
