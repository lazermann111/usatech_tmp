/*
 * ExecuteStep.java
 *
 * Created on February 10, 2004, 2:06 PM
 */

package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.servlet.RequestUtils;
import simple.servlet.StepConfigException;

/** This step executes a method on an object using the configured parameter values
 *
 * @author  Brian S. Krug
 */
public class ExecuteStep extends AbstractStep {
    private static Log log = Log.getLog();
    protected final List<Arg> args = new java.util.LinkedList<Arg>();
    
    /** Holds value of property method. */
    private String method;
    
    /** Holds value of property bean. */
    private String bean;
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property result. */
    private String result;
    
    /** Holds value of property type. */
    private String type;
    
    /** Creates a new instance of ExecuteStep */
    public ExecuteStep() {
    }
    
    public void perform(simple.servlet.Dispatcher dispatcher, simple.servlet.InputForm form, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws ServletException {
        try {
            Object[] params = new Object[args.size()];
            int i = 0;
            for(Arg arg : args) {
				params[i++] = arg.retrieveValue(form);
            }
            Object res;
            if(getType() != null) {
				if(log.isInfoEnabled())
					log.info("Invoking static method '" + getMethod() + "' on " + getType());
                Class<?> clazz = ReflectionUtils.loadContextClass(getType());
                res = ReflectionUtils.invokeStaticMethod(clazz, getMethod(), params);
            } else {
                Object object = form.getAttribute(getBean(), getScope());
				if(log.isInfoEnabled())
					log.info("Invoking instance method '" + getMethod() + "' on " + object);
                res = ReflectionUtils.invokeMethod(object, getMethod(), params);                
            }
            if(getResult() != null) {
                form.setAttribute(getResult(), res, (getScope() == null ? RequestUtils.REQUEST_SCOPE : getScope()));
                log.debug("Stored result in '" + getResult() + "'");
            }
        } catch(ConvertException e) {
            throw new ServletException("Could not execute method '" + getMethod() + "'", e);
        } catch (ClassNotFoundException e) {
            throw new ServletException("Could not execute method '" + getMethod() + "'", e);
        } catch (SecurityException e) {
            throw new ServletException("Could not execute method '" + getMethod() + "'", e);
        } catch (IllegalArgumentException e) {
            throw new ServletException("Could not execute method '" + getMethod() + "'", e);
        } catch (NoSuchMethodException e) {
            throw new ServletException("Could not execute method '" + getMethod() + "'", e);
        } catch (IllegalAccessException e) {
            throw new ServletException("Could not execute method '" + getMethod() + "'", e);
        } catch (InvocationTargetException e) {
            throw new ServletException("Could not execute method '" + getMethod() + "'", e.getTargetException());
        }
    }
    
	public void configure(Map<String, ?> props) throws StepConfigException {
        super.configure(props);
        try {
            Object tmp = props.get("args");
            if(tmp != null) {
                List<?> argList = ConvertUtils.convert(List.class,  tmp);
                for(Iterator<?> iter = argList.iterator(); iter.hasNext(); ) {
                    addArg((Map<String,?>)iter.next());
                }
            }
        } catch(ConvertException e) {
            throw new StepConfigException(e);
        } catch (IllegalAccessException e) {
            throw new StepConfigException(e);
        } catch (IntrospectionException e) {
            throw new StepConfigException(e);
        } catch (InvocationTargetException e) {
            throw new StepConfigException(e);
        } catch (InstantiationException e) {
            throw new StepConfigException(e);
        } catch(ParseException e) {
        	throw new StepConfigException(e);
		}
    }
    
    protected void addArg(Map<String,?> m) throws ConvertException, IllegalAccessException, 
            java.beans.IntrospectionException, java.lang.reflect.InvocationTargetException, InstantiationException, ParseException {
        Arg v = new Arg();
        ReflectionUtils.populateProperties(v, m);        
        args.add(v);
    }
    
    /** Getter for property method.
     * @return Value of property method.
     *
     */
    public String getMethod() {
        return this.method;
    }
    
    /** Setter for property method.
     * @param method New value of property method.
     *
     */
    public void setMethod(String method) {
        this.method = method;
    }
    
    /** Getter for property bean.
     * @return Value of property bean.
     *
     */
    public String getBean() {
        return this.bean;
    }
    
    /** Setter for property bean.
     * @param bean New value of property bean.
     *
     */
    public void setBean(String bean) {
        this.bean = bean;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /** Getter for property result.
     * @return Value of property result.
     *
     */
    public String getResult() {
        return this.result;
    }
    
    /** Setter for property result.
     * @param result New value of property result.
     *
     */
    public void setResult(String result) {
        this.result = result;
    }
    
    /** Getter for property type.
     * @return Value of property type.
     *
     */
    public String getType() {
        return this.type;
    }
    
    /** Setter for property type.
     * @param type New value of property type.
     *
     */
    public void setType(String type) {
        this.type = type;
    }
}
