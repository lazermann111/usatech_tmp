package simple.servlet.steps;

import simple.bean.ConvertUtils;

/** Holds information for determining if a certain condition is met by calling
 *  <code>simple.bean.ConditionUtils</code>.
 *
 */
public class Check {
    
    /** Holds value of property bean.  */
    private String bean;
    
    /** Holds value of property format.  */
    private String format;
    
    /** Holds value of property operator.  */
    private String operator;
    
    /** Holds value of property value.  */
    private String value;
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Creates a new Check Object **/
    public Check() {
    }
    
    /** Getter for property bean.
     * @return Value of property bean.
     *
     *
     */
    public String getBean() {
        return this.bean;
    }
    
    /** Getter for property operator.
     * @return Value of property operator.
     *
     *
     */
    public String getOperator() {
        return this.operator;
    }
    
    /** Getter for property value.
     * @return Value of property value.
     *
     *
     */
    public String getValue() {
        return this.value;
    }
    
    /** Setter for property bean.
     * @param bean New value of property bean.
     *
     */
    public void setBean(String bean) {
        this.bean = bean;
    }
        
    /** Setter for property value.
     * @param value New value of property value.
     *
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    /** Setter for property operator.
     * @param operator New value of property operator.
     *
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /** Determines whether the specified beanValue object mets the conditions
     * of this Check object
     * @param beanValue The value being tested against this Check
     * @return <code>true</code> if the beanValue passed, otherwise <code>false</code>
     */
    public boolean check(Object beanValue) {
        return simple.bean.ConditionUtils.check(beanValue, operator, value);
    }

    /** Determines whether the specified beanValue object mets the conditions
     * of this Check object
     * @param beanValue The value being tested against this Check
     * @return <code>true</code> if the beanValue passed, otherwise <code>false</code>
     */
    public Object format(Object beanValue) {
        return ConvertUtils.formatObject(beanValue, format);
    }
 
	/**
	 * @return Returns the format.
	 */
	public String getFormat() {
		return format;
	}
	/**
	 * @param format The format to set.
	 */
	public void setFormat(String format) {
		this.format = format;
	}
}

