/*
 * AbstractLogonStep.java
 *
 * Created on December 26, 2003, 10:49 AM
 */

package simple.servlet.steps;

import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.ServletUser;
import simple.servlet.SimpleServlet;
import simple.text.StringUtils;

/**
 * A base class for providing log-on functionality
 *
 * @author  Brian S. Krug
 */
public abstract class AbstractLogonStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    public static final String ATTRIBUTE_FORWARD = "simple.servlet.steps.LogonStep.forward";
    
	protected boolean requireSecureConnection;
	protected String defaultForwardPath = "home.i";
	protected boolean createSessionToken = true;
	protected boolean calcTimeZone = false;
    
    /** Creates a new instance of AbstractLogonStep */
    public AbstractLogonStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {        
        try {
			loginUser(form, request, response);
        } catch(ServletException e) {
        	handleAuthenticationFailure(dispatcher, form, e);
        	return;
        }
		String forward = (String) form.getAttribute(ATTRIBUTE_FORWARD);
		if(StringUtils.isBlank(forward))
			forward = getDefaultForwardPath();
		log.info("Forwarding to '" + forward + "'");
		dispatcher.forward(forward);
    }
       
	public ServletUser loginUser(InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		if(isRequireSecureConnection() && !request.isSecure())
			throw new ServletException("A secure connection is required to log in to this system");
		ServletUser user = authenticateUser(form);
		request.getSession().setAttribute("remoteUser", user.getUserName());
		request.getSession().setAttribute(SimpleServlet.ATTRIBUTE_USER, user);
		String sessionToken;
		if(isCreateSessionToken()) {
			sessionToken = RequestUtils.getSessionToken(request);
			if(RequestUtils.isXsrfProtectionEnabled(request))
				form.setAttribute(SimpleServlet.REQUEST_SESSION_TOKEN, sessionToken);
		} else {
			sessionToken = null;
		}
		if(isCalcTimeZone()) {
			TimeZone timeZone = RequestUtils.getTimeZone(form);
			if(timeZone != null)
				request.getSession().setAttribute(SimpleServlet.ATTRIBUTE_TIME_ZONE, timeZone);
		}
		log.info("Logon successful for '" + user.getUserName() + "'" + (sessionToken == null ? "" : " using session-token '" + sessionToken + "'"));
		if(rememberUserName(form)) {
			Cookie usernameCookie = new Cookie("username", user.getUserName());
			// usernameCookie.setSecure(true);
			RequestUtils.setHttpOnlyCookieHeader(request, response, usernameCookie);
		} else {
			RequestUtils.removeCookies(request, response, "username");
		}
		return user;
	}
	protected boolean rememberUserName(InputForm form) {
		return false;
	}

	protected void handleAuthenticationFailure(Dispatcher dispatcher, InputForm form, ServletException exception) throws ServletException {
    	throw exception;
    }

	/** Getter for property requireSecureConnection.
     * @return Value of property requireSecureConnection.
     *
     */
    public boolean isRequireSecureConnection() {
        return this.requireSecureConnection;
    }
    
    /** Setter for property requireSecureConnection.
     * @param requireSecureConnection New value of property requireSecureConnection.
     *
     */
    public void setRequireSecureConnection(boolean requireSecureConnection) {
        this.requireSecureConnection = requireSecureConnection;
    }
    
    public abstract ServletUser authenticateUser(InputForm form) throws ServletException ;

	public String getDefaultForwardPath() {
		return defaultForwardPath;
	}

	public void setDefaultForwardPath(String defaultForwardPath) {
		this.defaultForwardPath = defaultForwardPath;
	}

	public boolean isCreateSessionToken() {
		return createSessionToken;
	}

	public void setCreateSessionToken(boolean createSessionToken) {
		this.createSessionToken = createSessionToken;
	}

	public boolean isCalcTimeZone() {
		return calcTimeZone;
	}

	public void setCalcTimeZone(boolean calcTimeZone) {
		this.calcTimeZone = calcTimeZone;
	}
}
