package simple.servlet.steps;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call;
import simple.db.CallNotFoundException;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.ParameterException;
import simple.servlet.CallStatementCanceller;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.StepConfigException;
import simple.text.StringUtils;

public class DbStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    public static final String ATTRIBUTE_CANCELLER = "simple.servlet.steps.DBStep.Canceller";
    protected List<DbCall> calls = new java.util.LinkedList<DbCall>();
    protected DataLayer dataLayer = simple.db.DataLayerMgr.getGlobalDataLayer();

    /** Holds value of property dataSource. */
    protected String dataSource;

    /** Holds value of property commit. */
    protected boolean commit;

    /** Holds value of property connCache. */
    protected String connCache;

    protected static final Map<String, ReentrantLock> cacheLocks = new java.util.HashMap<String, ReentrantLock>();

    /** Holds value of property cacheResults. */
    protected boolean cacheResults;

    protected boolean cancellable;

    public DbStep() {
    }

    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        perform(form, true);
    }

    protected void perform(InputForm form, boolean retry) throws ServletException {
        //check for early exit
        if(useCachedResults(form)) {
            log.debug("Returning cached results");
            return;
        }
        Connection conn = null;
        try {
            conn = getConnection(form);
            executeCalls(conn, form);
			if(commit) {
				if(log.isInfoEnabled())
					log.info("Committing connection for " + findDataSource());
				conn.commit();
			}
        } catch(SQLException sqle) {
            if(commit && conn != null) try { conn.rollback(); } catch(SQLException e0) {}
            if(retry && isRetryable(conn, sqle)) {
                if(conn != null) try { conn.close(); } catch(SQLException e) {}
                conn = null;
                log.debug("A database call failed with the following exception and will be re-tried once", sqle);
                perform(form, false);
            } else {
                throw new ServletException(sqle);
            }
        } catch(CallNotFoundException e) {
            if(commit && conn != null) try { conn.rollback(); } catch(SQLException e0) {}
            throw new ServletException(e);
        } catch (DataLayerException e) {
            if(commit && conn != null) try { conn.rollback(); } catch(SQLException e0) {}
            throw new ServletException(e);
        } catch (ConvertException e) {
            if(commit && conn != null) try { conn.rollback(); } catch(SQLException e0) {}
            throw new ServletException(e);
        } catch(InterruptedException e) {
        	throw new ServletException(e);
		} finally {
            if(conn != null && connCache == null) try { conn.close(); } catch(SQLException e) {}
        }
    }

    @SuppressWarnings("unused")
	protected void executeCalls(Connection conn, InputForm form) throws ConvertException, CallNotFoundException, ParameterException, SQLException {
        String scope = getResultsScope();
        for(DbCall cc : calls) {
            Call call = dataLayer.findCall(cc.id);
			if(log.isInfoEnabled())
				log.info("Executing call '" + cc.id + "'");
            Object[] res;
            if(cancellable) {
                Call.CallStatement cs = call.createCallStatement(conn); //TODO: could store in a ThreadLocal and re-use
                // add to session canceller
                CallStatementCanceller canceller = null;
                try {
                    canceller = RequestUtils.getOrCreateAttribute(form.getRequest(), ATTRIBUTE_CANCELLER, CallStatementCanceller.class, "session");
                } catch(ServletException e) {
                    log.warn("Could not create DBStep Canceller in session", e);
                } catch(ConvertException e) {
                    log.warn("Could not create DBStep Canceller in session", e);
                } catch(InstantiationException e) {
                    log.warn("Could not create DBStep Canceller in session", e);
                } catch(IllegalAccessException e) {
                    log.warn("Could not create DBStep Canceller in session", e);
                }
                int csId = 0;
                if(canceller != null) csId = canceller.addCallStatement(cs);
                try {
                    res = cs.executeCall(form, null);
                } catch(SQLException e) {
                	throw new SQLException("For Call '" + cc.id + "': " + e.getMessage(), e.getSQLState(), e.getErrorCode(), e);
                } finally {
                    if(canceller != null) canceller.removeCallStatement(csId);
                }
            } else {
                try {
                	res = call.executeCall(conn, form, null);
	            } catch(SQLException e) {
	            	throw new SQLException("For Call '" + cc.id + "': " + e.getMessage(), e.getSQLState(), e.getErrorCode(), e);
	            }
            }
			for(int i = 0; i < res.length && i < cc.results.length; i++) {
				if(!StringUtils.isBlank(cc.results[i])) {
					form.setAttribute(cc.results[i], res[i], scope);
					log.debug("Set attribute '" + cc.results[i] + "' to " + res[i] + " in " + scope);
				}
            }
        }
    }

    protected boolean useCachedResults(InputForm form) throws ServletException {
        //check for early exit
        return isCacheResults() && !form.getBoolean("refreshCache", false, false) && !needsRefresh(form);
    }

    protected Connection getConnection(InputForm form) throws SQLException, DataLayerException, InterruptedException {
        if(getConnCache() != null) {
            return getCachedConnection(form);
        } else {
            String ds = findDataSource();
            log.debug("Obtaining db connection for '" + ds + "' for use with " + calls.size() + " db call(s)");
            return dataLayer.getConnection(ds);
        }
    }

    protected boolean needsRefresh(InputForm form) {
        for(DbCall cc : calls) {
            if(!hasResults(form, cc)) return true;
        }
        return false;
    }

    protected boolean hasResults(InputForm form, DbCall cc) {
        for(int i = 0; i < cc.results.length; i++) {
            if(form.getAttribute(cc.results[i], getResultsScope()) == null) return false;
        }
        return true;
    }

    protected String getResultsScope() {
        return isCacheResults() ? RequestUtils.SESSION_SCOPE : RequestUtils.REQUEST_SCOPE;
    }

    /** Getter for property dataSource.
     * @return Value of property dataSource.
     *
     */
    public String getDataSource() {
        return this.dataSource;
    }

    protected String findDataSource() throws CallNotFoundException {
        if(dataSource == null && !calls.isEmpty()) {
            log.debug("Getting DataSource from call...");
            DbCall cc = calls.get(0);
            Call call = dataLayer.findCall(cc.id);
            dataSource = call.getDataSourceName();
        }
        return this.dataSource;
    }

    /** Setter for property dataSource.
     * @param dataSource New value of property dataSource.
     *
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    /** Getter for property commit.
     * @return Value of property commit.
     *
     */
    public boolean isCommit() {
        return this.commit;
    }

    /** Setter for property commit.
     * @param commit New value of property commit.
     *
     */
    public void setCommit(boolean commit) {
        this.commit = commit;
    }

    /**
     * @see simple.servlet.steps.AbstractStep#configure(java.util.Map)
     */
    @Override
	public void configure(java.util.Map<String,?> props) throws StepConfigException {
        super.configure(props);
        Object tmp = props.get("calls");
        if(tmp != null) {
            try {
                String[] defaultResults = ConvertUtils.convert(String[].class, props.get("results"));
                if(defaultResults == null) defaultResults = new String[0];
                List<?> callList = ConvertUtils.convert(List.class,  tmp);
                for(Object c : callList) {
                    addCall(c, defaultResults);
                }
            } catch(ConvertException e) {
                throw new StepConfigException(e);
            }
        }
    }

    protected void addCall(Object o, String[] defaultResults) throws ConvertException {
        if(o == null) return;
        DbCall call = new DbCall();
        if(o instanceof Map<?,?>) {
            Map<?,?> m = (Map<?,?>)o;
            call.id = (String)m.get("id");
            if(call.id == null) call.id = (String)m.get("");
            call.results = ConvertUtils.convert(String[].class, m.get("results"));
            if(call.results == null) call.results = defaultResults;
        } else {
            call.id = o.toString();
            call.results = defaultResults;
        }
        log.debug("Added DB Call (id="+call.id + "; results=" +simple.text.StringUtils.toStringList(call.results));
        calls.add(call);
    }

    /** Getter for property connCache.
     * @return Value of property connCache.
     *
     */
    public String getConnCache() {
        return this.connCache;
    }

    /** Setter for property connCache.
     * @param connCache New value of property connCache.
     *
     */
    public void setConnCache(String connCache) {
        if(connCache != null) {
            connCache = connCache.trim();
            if(connCache.length() == 0) connCache = null;
            else if(!cacheLocks.containsKey(connCache)) { // this is not thread-safe but it would be rare to load Actions in different threads
                cacheLocks.put(connCache, new ReentrantLock());
            }
        }
        this.connCache = connCache;
    }

    protected Connection getCachedConnection(InputForm form) throws DataLayerException, SQLException, InterruptedException {
        ReentrantLock lock = cacheLocks.get(connCache);
        lock.lockInterruptibly();
        try {
            Connection conn = (Connection)form.getAttribute(connCache, simple.servlet.RequestUtils.APPLICATION_SCOPE);
            try {
                if(conn != null && conn.isClosed()) {
                    log.debug("Cached Connection is closed! Removing it and getting another...");
                    conn.close();
                    conn = null;
                    form.setAttribute(connCache, null, simple.servlet.RequestUtils.APPLICATION_SCOPE);
                }
            } catch(SQLException e) {
                conn = null; // get a new one!
                form.setAttribute(connCache, null, simple.servlet.RequestUtils.APPLICATION_SCOPE);
            }
            if(conn == null) {
                String ds = findDataSource();
                log.debug("Obtaining db connection for '" + ds + "' for use as the cached connection");
                conn = dataLayer.getConnection(ds);
                form.setAttribute(connCache, conn, simple.servlet.RequestUtils.APPLICATION_SCOPE);
            }
            return conn;
        } finally {
            lock.unlock();
        }
    }

    /** Getter for property cacheResults.
     * @return Value of property cacheResults.
     *
     */
    public boolean isCacheResults() {
        return this.cacheResults;
    }

    /** Setter for property cacheResults.
     * @param cacheResults New value of property cacheResults.
     *
     */
    public void setCacheResults(boolean cacheResults) {
        this.cacheResults = cacheResults;
    }

    /**
     * Determines if a new connection should be obtained and the statements retried based on the
     * given SQLException that was thrown for the previous attempt
     */
    protected boolean isRetryable(Connection conn, SQLException sqle) {
        return DataLayer.DEFAULT_RETRY.decide(conn, sqle);
    }

    protected class DbCall {
        public String id;
        public String[] results;
    }

    public boolean isCancellable() {
        return cancellable;
    }

    public void setCancellable(boolean cancellable) {
        this.cancellable = cancellable;
    }
}

