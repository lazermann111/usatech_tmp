package simple.servlet.steps;

import java.io.ByteArrayOutputStream;

import javax.servlet.ServletException;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

public class InterceptStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    /** Holds value of property page. */
    private String page;
    
    /** Holds value of property toAttr. */
    private String toAttr = "content";
    
    /** Holds value of property toScope. */
    private String toScope;
    
    public InterceptStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        log.debug("Intercepting toAttr of '" + page + "'");        
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        dispatcher.intercept(page, out);
        form.setAttribute(toAttr, out, toScope);        
        log.debug("Intercepted " + out.size() + " bytes and stored them in attribute '" + toAttr + "'");        
    }    

    /** Getter for property page.
     * @return Value of property page.
     *
     */
    public String getPage() {
        return this.page;
    }
    
    /** Setter for property page.
     * @param page New value of property page.
     *
     */
    public void setPage(String page) {
        this.page = page;
    }
    
    /** Getter for property toAttr.
     * @return Value of property toAttr.
     *
     */
    public String getToAttr() {
        return this.toAttr;
    }
    
    /** Setter for property toAttr.
     * @param toAttr New value of property toAttr.
     *
     */
    public void setToAttr(String toAttr) {
        this.toAttr = toAttr;
    }
    
    /** Getter for property toScope.
     * @return Value of property toScope.
     *
     */
    public String getToScope() {
        return this.toScope;
    }
    
    /** Setter for property toScope.
     * @param toScope New value of property toScope.
     *
     */
    public void setToScope(String toScope) {
        this.toScope = toScope;
    }
    
}

