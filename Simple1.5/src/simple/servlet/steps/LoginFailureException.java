/*
 * LoginFailureException.java
 *
 * Created on January 5, 2004, 3:56 PM
 */

package simple.servlet.steps;

/**
 *
 * @author  Brian S. Krug
 */
public class LoginFailureException extends javax.servlet.ServletException {
    /**
	 *
	 */
	private static final long serialVersionUID = -1324948001412L;
	public static final int INVALID_USERNAME = 1;
    public static final int INVALID_PASSWORD = 2;
    public static final int INVALID_CREDENTIALS = 3;
    public static final int ACCOUNT_LOCKED = 4;
	public static final int PASSWORD_EXPIRED = 5;

    /** Holds value of property type. */
    private int type;

    /**
     * Creates a new instance of <code>LoginFailureException</code> without detail message.
     */
    public LoginFailureException(int type) {
        this.type = type;
    }


    /**
     * Constructs an instance of <code>LoginFailureException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public LoginFailureException(String msg, int type) {
        super(msg);
        this.type = type;
    }

    public LoginFailureException(int type, Throwable cause) {
		super(cause);
		this.type = type;
	}

    public LoginFailureException(String msg, int type, Throwable cause) {
		super(msg, cause);
		this.type = type;
	}

	/** Getter for property type.
     * @return Value of property type.
     *
     */
    public int getType() {
        return this.type;
    }

}
