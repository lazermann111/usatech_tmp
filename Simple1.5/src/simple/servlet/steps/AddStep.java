/*
 * SetStep.java
 *
 * Created on February 6, 2004, 5:49 PM
 */

package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.ParseException;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;

/**
 * Adds two values together
 * @author  Brian S. Krug
 */
public class AddStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();

    private String scope;
    private String result;
    private String format;
    private String bean1;
    private String value1;
    private String bean2;
    private String value2;

    /** Creates a new instance of SetStep */
    public AddStep() {
    }

    public void perform(simple.servlet.Dispatcher dispatcher, simple.servlet.InputForm form, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException {
    	Object v1;
        if(bean1 != null) {
            try {
				v1 = ReflectionUtils.getProperty(form, bean1);
			} catch (IntrospectionException e) {
				throw new ServletException(e);
			} catch (IllegalAccessException e) {
				throw new ServletException(e);
			} catch (InvocationTargetException e) {
				throw new ServletException(e);
			} catch(ParseException e) {
				throw new ServletException(e);
			}
            if(v1 == null) v1 = value1; // let value be the default if bean is not in form
        } else v1 = value1;
        Object v2;
        if(bean2 != null) {
            try {
				v2 = ReflectionUtils.getProperty(form, bean2);
			} catch (IntrospectionException e) {
				throw new ServletException(e);
			} catch (IllegalAccessException e) {
				throw new ServletException(e);
			} catch (InvocationTargetException e) {
				throw new ServletException(e);
			} catch(ParseException e) {
				throw new ServletException(e);
			}
            if(v2 == null) v2 = value2; // let value be the default if bean is not in form
        } else v2 = value2;
        BigDecimal bd1;
		try {
			bd1 = ConvertUtils.convert(BigDecimal.class, v1);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
        BigDecimal bd2;
		try {
			bd2 = ConvertUtils.convert(BigDecimal.class, v2);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
        BigDecimal resultBd;
        if(bd1 == null)
        	resultBd = bd2;
        else if(bd2 == null)
        	resultBd = bd1;
        else
        	resultBd = bd1.add(bd2);
        Object resultValue;
        if(format != null)
        	resultValue = ConvertUtils.formatObject(resultBd, format);
        else
        	resultValue = resultBd;
        if(log.isDebugEnabled()) 
        	log.debug("Setting attribute " + result + " to '" + resultValue + "'");
    	form.setAttribute(result, resultValue, (scope == null ? simple.servlet.RequestUtils.REQUEST_SCOPE : scope));
    }

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getBean1() {
		return bean1;
	}

	public void setBean1(String bean1) {
		this.bean1 = bean1;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getBean2() {
		return bean2;
	}

	public void setBean2(String bean2) {
		this.bean2 = bean2;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}
}
