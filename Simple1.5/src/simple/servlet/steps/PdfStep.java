package simple.servlet.steps;

import javax.servlet.ServletException;

import org.apache.fop.apps.Driver;
import org.xml.sax.InputSource;

import simple.io.HeapBufferStream;
import simple.io.PassThroughOutputStream;
import simple.io.logging.avalon.SimpleAvalonLogger;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

public class PdfStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    /** Holds value of property fromPage. */
    private String fromPage;
    
    /** Holds value of property toFile. */
    private String toFile;
    
    /** Holds value of property toAttr. */
    private String toAttr;
    
    /** Holds value of property writePage. */
    private boolean writePage = true;
    
    /** Holds value of property fromFile. */
    private String fromFile;
    
    /** Holds value of property fromAttr. */
    private String fromAttr;
    
    /** Holds value of property fromScope. */
    private String fromScope;
    
    /** Holds value of property toScope. */
    private String toScope;
    
    /**
     * Holds value of property downloadName.
     */
    private String downloadName;
    
    public PdfStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        try { 
            // get the input source
            InputSource src;
            String fileName;
            if(fromPage != null) {
                HeapBufferStream qs = new HeapBufferStream(0, 512);
                dispatcher.intercept(fromPage, qs.getOutputStream());
                src = new InputSource(qs.getInputStream());
                fileName = fromPage;
            } else if(fromFile != null) {
                src = new InputSource(new java.io.FileInputStream(fromFile)); // should we get this as relative to the web page or in the classpath?
                fileName = fromFile;
            } else if(fromAttr != null) {
                Object in = form.getAttribute(getFromAttr(), getFromScope());
                if(in == null) throw new ServletException("No data found in attribute '" + getFromAttr() + "' of the form");
                if(in instanceof InputSource) src = (InputSource)in;
                else if(in instanceof java.io.InputStream) src = new InputSource((java.io.InputStream)in);
                else if(in instanceof java.io.Reader) src = new InputSource((java.io.Reader)in);
                else if(in instanceof java.io.ByteArrayOutputStream) src = new InputSource(new java.io.ByteArrayInputStream(((java.io.ByteArrayOutputStream)in).toByteArray()));
                else src = new InputSource(new java.io.StringReader(in.toString()));
                fileName = fromAttr;
            } else throw new ServletException("No input source was specified for the PdfStep; please set either 'fromPage', 'fromFile', or 'fromAttr'");
            
            //prepare the outputstream
            PassThroughOutputStream out = new PassThroughOutputStream();
            
            // add Response OutputStream
            if(isWritePage()) {
                response.setContentType("application/pdf"); 
                //String fn = form.getString(ATTRIBUTE_SEARCH_PATH, false);
                if(downloadName != null && downloadName.trim().length() > 0) fileName = downloadName;              
                if(!(fileName=fileName.trim()).toLowerCase().endsWith(".pdf")) fileName = fileName + ".pdf";
                response.setHeader("Content-Disposition","attachment;filename=" + fileName);
                out.addWriter(response.getWriter());
                // NOTE: May need to write to a ByteArrayOutputStream as a buffer to avoid problems with IE rendering PDF file
            }
            
            //add FileOutputStream to save to file
            if(getToFile() != null) {
                out.addOutputStream(new java.io.BufferedOutputStream(new java.io.FileOutputStream(getToFile())));
            }
            
            // add ByteArrayOutputStream to save to attribute
            if(getToAttr() != null) {
                java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
                form.setAttribute(getToAttr(), baos, getToScope());
            }
            
            // render the PDF
            Driver driver = new Driver(src, out);
            driver.setLogger(new SimpleAvalonLogger(log));
            driver.setRenderer(Driver.RENDER_PDF);
            driver.run();
            
            out.flush();
        } catch (Exception ex) {
            throw new ServletException(ex);
        } finally {
            //out.close();
        }
    }    
           
    /** Getter for property fromPage.
     * @return Value of property fromPage.
     *
     */
    public String getFromPage() {
        return this.fromPage;
    }
    
    /** Setter for property fromPage.
     * @param page New value of property fromPage.
     *
     */
    public void setFromPage(String fromPage) {
        this.fromPage = fromPage;
    }
 
    /** Getter for property toFile.
     * @return Value of property toFile.
     *
     */
    public String getToFile() {
        return this.toFile;
    }
    
    /** Setter for property toFile.
     * @param toFile New value of property toFile.
     *
     */
    public void setToFile(String toFile) {
        this.toFile = toFile;
    }
    
    /** Getter for property toAttr.
     * @return Value of property toAttr.
     *
     */
    public String getToAttr() {
        return this.toAttr;
    }
    
    /** Setter for property toAttr.
     * @param saveToAttr New value of property toAttr.
     *
     */
    public void setToAttr(String toAttr) {
        this.toAttr = toAttr;
    }
    
    /** Getter for property writePage.
     * @return Value of property writePage.
     *
     */
    public boolean isWritePage() {
        return this.writePage;
    }
    
    /** Setter for property writePage.
     * @param writePage New value of property writePage.
     *
     */
    public void setWritePage(boolean writePage) {
        this.writePage = writePage;
    }
    
    /** Getter for property fromFile.
     * @return Value of property fromFile.
     *
     */
    public String getFromFile() {
        return this.fromFile;
    }
    
    /** Setter for property fromFile.
     * @param fromFile New value of property fromFile.
     *
     */
    public void setFromFile(String fromFile) {
        this.fromFile = fromFile;
    }
    
    /** Getter for property fromAttr.
     * @return Value of property fromAttr.
     *
     */
    public String getFromAttr() {
        return this.fromAttr;
    }
    
    /** Setter for property fromAttr.
     * @param fromAttr New value of property fromAttr.
     *
     */
    public void setFromAttr(String fromAttr) {
        this.fromAttr = fromAttr;
    }
    
    /** Getter for property fromScope.
     * @return Value of property fromScope.
     *
     */
    public String getFromScope() {
        return this.fromScope;
    }
    
    /** Setter for property fromScope.
     * @param fromScope New value of property fromScope.
     *
     */
    public void setFromScope(String fromScope) {
        this.fromScope = fromScope;
    }
    
    /** Getter for property toScope.
     * @return Value of property toScope.
     *
     */
    public String getToScope() {
        return this.toScope;
    }
    
    /** Setter for property toScope.
     * @param toScope New value of property toScope.
     *
     */
    public void setToScope(String toScope) {
        this.toScope = toScope;
    }
    
    /**
     * Getter for property downloadName.
     * @return Value of property downloadName.
     */
    public String getDownloadName() {
        return this.downloadName;
    }
    
    /**
     * Setter for property downloadName.
     * @param downloadName New value of property downloadName.
     */
    public void setDownloadName(String downloadName) {
        this.downloadName = downloadName;
    }
}

