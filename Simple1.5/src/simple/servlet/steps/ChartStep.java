package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.Format;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.jfree.chart.plot.PlotOrientation;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.chart.ChartCreator;
import simple.chart.ResultsCategoryDataset;
import simple.chart.ResultsDataset;
import simple.chart.ResultsXYZDataset;
import simple.io.PassThroughOutputStream;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.StepConfigException;

public class ChartStep extends AbstractStep {
    /** Holds value of property toFile. */
    private String toFile;

    /** Holds value of property toAttr. */
    private String toAttr;

    /** Holds value of property writePage. */
    private boolean writePage = true;

    /** Holds value of property resultsAttr. */
    private String resultsAttr = "results";

    /** Holds value of property resultsScope. */
    private String resultsScope;

    /** Holds value of property toScope. */
    private String toScope;

    protected ChartCreator creator = new ChartCreator();
    protected ClusterConfig[] clusters;
    protected ValueConfig[] values;

    /**
     * Holds value of property downloadName.
     */
    private String downloadName;

    public ChartStep() {
    }

    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        try {
            // get the results object
            Results results = (Results)form.getAttribute(resultsAttr, resultsScope);
            if(results == null) throw new ServletException("Did not find a simple.results.Results object under the '" + resultsAttr + "' attribute in "
                + (resultsScope == null ? "any" : "the " + resultsScope) + " scope");
            //prepare the outputstream
            PassThroughOutputStream out = new PassThroughOutputStream();

            // add Response OutputStream
            if(isWritePage()) {
                String fileName = "chart.png";
                if(downloadName != null && downloadName.trim().length() > 0) fileName = downloadName;
                response.setContentType("image/png");
                if(!(fileName=fileName.trim()).toLowerCase().endsWith(".png")) fileName = fileName + ".png";
                response.setHeader("Content-Disposition","attachment;filename=" + fileName);
                out.addWriter(response.getWriter());
            }

            //add FileOutputStream to save to file
            if(getToFile() != null) {
                out.addOutputStream(new java.io.BufferedOutputStream(new java.io.FileOutputStream(getToFile())));
            }

            // add ByteArrayOutputStream to save to attribute
            if(getToAttr() != null) {
                java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
                form.setAttribute(getToAttr(), baos, getToScope());
            }

            // create the chart
            ResultsDataset dataset = createDataset();
            dataset.populate(results);
            creator.createPNG(dataset, out);
            out.flush();
        } catch (ServletException se) {
            throw se;
        } catch (Exception ex) {
            throw new ServletException(ex);
        } finally {
            //out.close();
        }
    }

    /** Getter for property toFile.
     * @return Value of property toFile.
     *
     */
    public String getToFile() {
        return this.toFile;
    }

    /** Setter for property toFile.
     * @param toFile New value of property toFile.
     *
     */
    public void setToFile(String toFile) {
        this.toFile = toFile;
    }

    /** Getter for property toAttr.
     * @return Value of property toAttr.
     *
     */
    public String getToAttr() {
        return this.toAttr;
    }

    /** Setter for property toAttr.
     * @param saveToAttr New value of property toAttr.
     *
     */
    public void setToAttr(String toAttr) {
        this.toAttr = toAttr;
    }

    /** Getter for property writePage.
     * @return Value of property writePage.
     *
     */
    public boolean isWritePage() {
        return this.writePage;
    }

    /** Setter for property writePage.
     * @param writePage New value of property writePage.
     *
     */
    public void setWritePage(boolean writePage) {
        this.writePage = writePage;
    }

    /** Getter for property resultsAttr.
     * @return Value of property resultsAttr.
     *
     */
    public String getResultsAttr() {
        return this.resultsAttr;
    }

    /** Setter for property resultsAttr.
     * @param resultsAttr New value of property resultsAttr.
     *
     */
    public void setResultsAttr(String resultsAttr) {
        this.resultsAttr = resultsAttr;
    }

    /** Getter for property resultsScope.
     * @return Value of property resultsScope.
     *
     */
    public String getResultsScope() {
        return this.resultsScope;
    }

    /** Setter for property resultsScope.
     * @param resultsScope New value of property resultsScope.
     *
     */
    public void setResultsScope(String resultsScope) {
        this.resultsScope = resultsScope;
    }

    /** Getter for property toScope.
     * @return Value of property toScope.
     *
     */
    public String getToScope() {
        return this.toScope;
    }

    /** Setter for property toScope.
     * @param toScope New value of property toScope.
     *
     */
    public void setToScope(String toScope) {
        this.toScope = toScope;
    }

    /** Creates a new dataset.
     * @return The new dataset.
     *
     */
    public ResultsDataset createDataset() throws ServletException {
        Class<?> c = creator.getDatasetInterface();
        ResultsDataset rd;
        if(c.isAssignableFrom(ResultsCategoryDataset.class)) rd = new ResultsCategoryDataset();
        else if(c.isAssignableFrom(ResultsXYZDataset.class)) rd = new ResultsXYZDataset();
        else throw new ServletException("Could not find a ResultsDataset to implement the required '"
        + c.getName() + "' interface for chart type '" + creator.getChartType() + "'");
        //configure
        for(int i = 0; i < clusters.length; i++) {
           rd.addCluster(clusters[i].clusterName, clusters[i].displayColumn, clusters[i].orderColumn, clusters[i].format);
        }
        for(int i = 0; i < values.length; i++) {
            rd.addValueColumn(values[i].valueType, values[i].columnName, values[i].columnName, null);
        }
        return rd;
    }

	@SuppressWarnings("rawtypes")
	@Override
	public void configure(Map<String, ?> props) throws StepConfigException {
        super.configure(props);
        try {
			String tmp = (String) props.remove("orientation");
            simple.bean.ReflectionUtils.populateProperties(creator, props);
			if("HORIZONTAL".equalsIgnoreCase(tmp) || "H".equalsIgnoreCase(tmp))
				creator.setOrientation(PlotOrientation.HORIZONTAL);
			else
				creator.setOrientation(PlotOrientation.VERTICAL);
            List l = (List)props.get("clusters");
            clusters = new ClusterConfig[l.size()];
            Iterator iter = l.iterator();
            for(int i = 0; iter.hasNext(); i++) {
                Map m = (Map)iter.next();
                clusters[i] = new ClusterConfig();
                clusters[i].clusterName = (String)m.get("name");
                clusters[i].displayColumn = (String)m.get("displayColumn");
                clusters[i].orderColumn = (String)m.get("orderColumn");
                clusters[i].format = ConvertUtils.convert(Format.class, m.get("format"));
            }
            l = (List)props.get("values");
            values = new ValueConfig[l.size()];
            iter = l.iterator();
            for(int i = 0; iter.hasNext(); i++) {
                Map m = (Map)iter.next();
                values[i] = new ValueConfig();
                values[i].valueType = (String)m.get("name");
                values[i].columnName = (String)m.get("column");
            }
        } catch(IntrospectionException e) {
            throw new StepConfigException(e);
        } catch (IllegalAccessException e) {
            throw new StepConfigException(e);
        } catch (InvocationTargetException e) {
            throw new StepConfigException(e);
        } catch (InstantiationException e) {
            throw new StepConfigException(e);
        } catch (ConvertException e) {
            throw new StepConfigException(e);
        } catch(ParseException e) {
            throw new StepConfigException(e);
		}
    }

    /**
     * Getter for property downloadName.
     * @return Value of property downloadName.
     */
    public String getDownloadName() {
        return this.downloadName;
    }

    /**
     * Setter for property downloadName.
     * @param downloadName New value of property downloadName.
     */
    public void setDownloadName(String downloadName) {
        this.downloadName = downloadName;
    }

    protected static class ClusterConfig {
        public String clusterName;
        public String displayColumn;
        public String orderColumn;
        public Format format;
    }
    protected static class ValueConfig {
        public String valueType;
        public String columnName;
    }

}

