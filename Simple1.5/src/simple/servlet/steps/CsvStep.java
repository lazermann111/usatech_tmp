package simple.servlet.steps;

import javax.servlet.ServletException;

import simple.io.PassThroughOutputStream;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.text.StringUtils;

public class CsvStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    protected static final char[] RANGE_DELIMS = "-:".toCharArray();
    protected static final char[] STYLE_DELIMS = ";\n\r".toCharArray();
    /** Holds value of property toFile. */
    private String toFile;
    
    /** Holds value of property toAttr. */
    private String toAttr;
    
    /** Holds value of property writePage. */
    private boolean writePage = true;

    /**
     * Holds value of property downloadName.
     */
    private String downloadName;
    
    /**
     * Holds value of property fromResults.
     */
    private String fromResults;
    
    
    public CsvStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        try { 
            //prepare the outputstream
            PassThroughOutputStream out = new PassThroughOutputStream();
            
            // add Response OutputStream
            if(isWritePage()) {
				String extension = "csv";
				response.setContentType("text/" + extension);
                String fileName;
                if(downloadName != null && downloadName.trim().length() > 0) fileName = downloadName;              
                else {
                    fileName = form.getString(SimpleServlet.ATTRIBUTE_SEARCH_PATH, false);
                    if(fileName != null && fileName.startsWith("/")) fileName = fileName.substring(1);
                    fileName = fileName.replace('/', '_');
                }
                log.trace("File Name=" + fileName);
				if(!(fileName = fileName.trim()).toLowerCase().endsWith("." + extension))
					fileName = fileName + "." + extension;
                log.trace("Now File Name=" + fileName);
                response.setHeader("Content-Disposition","attachment;filename=" + fileName);
				// out.addWriter(response.getWriter());
				out.addOutputStream(response.getOutputStream());
                // NOTE: May need to write to a ByteArrayOutputStream as a buffer to avoid problems with IE rendering PDF file
            }
            
            //add FileOutputStream to save to file
            if(getToFile() != null) {
                out.addOutputStream(new java.io.BufferedOutputStream(new java.io.FileOutputStream(getToFile())));
            }
            
            // add ByteArrayOutputStream to save to attribute
            if(getToAttr() != null) {
                java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
				form.setAttribute(getToAttr(), baos);
            }
            
            //write it out
			Results results = (Results) form.get(getFromResults());
			if(results == null)
				throw new ServletException("The value of the request attribute, '" + getFromResults() + "' is null.");

			StringUtils.writeCSV(results, out, true);
            out.flush();
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }    
           
    
    /** Getter for property toFile.
     * @return Value of property toFile.
     *
     */
    public String getToFile() {
        return this.toFile;
    }
    
    /** Setter for property toFile.
     * @param toFile New value of property toFile.
     *
     */
    public void setToFile(String toFile) {
        this.toFile = toFile;
    }
    
    /** Getter for property toAttr.
     * @return Value of property toAttr.
     *
     */
    public String getToAttr() {
        return this.toAttr;
    }
    
    /** Setter for property toAttr.
     * @param saveToAttr New value of property toAttr.
     *
     */
    public void setToAttr(String toAttr) {
        this.toAttr = toAttr;
    }
    
    /** Getter for property writePage.
     * @return Value of property writePage.
     *
     */
    public boolean isWritePage() {
        return this.writePage;
    }
    
    /** Setter for property writePage.
     * @param writePage New value of property writePage.
     *
     */
    public void setWritePage(boolean writePage) {
        this.writePage = writePage;
    }
    
    /**
     * Getter for property downloadName.
     * @return Value of property downloadName.
     */
    public String getDownloadName() {
        return this.downloadName;
    }
    
    /**
     * Setter for property downloadName.
     * @param downloadName New value of property downloadName.
     */
    public void setDownloadName(String downloadName) {
        this.downloadName = downloadName;
    }
    
    /**
     * Getter for property fromResults.
     * @return Value of property fromResults.
     */
    public String getFromResults() {
        return this.fromResults;
    }
    
    /**
     * Setter for property fromResults.
     * @param fromResults New value of property fromResults.
     */
    public void setFromResults(String fromResults) {
        this.fromResults = fromResults;
    }
}

