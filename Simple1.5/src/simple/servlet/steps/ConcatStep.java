/*
 * ExecuteStep.java
 *
 * Created on February 10, 2004, 2:06 PM
 */

package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.servlet.RequestUtils;
import simple.servlet.StepConfigException;

/** This step executes a method on an object using the configured parameter values
 *
 * @author  Brian S. Krug
 */
public class ConcatStep extends AbstractStep {
    private static Log log = Log.getLog();
    protected final List<Arg> args = new java.util.LinkedList<Arg>();
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property result. */
    private String result;
    
    /** Creates a new instance of ExecuteStep */
    public ConcatStep() {
    }
    
    public void perform(simple.servlet.Dispatcher dispatcher, simple.servlet.InputForm form, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws ServletException {
        StringBuilder sb = new StringBuilder();
        for(Arg arg : args) {
            if(arg.getBean() != null) sb.append(ConvertUtils.getStringSafely(form.getAttribute(arg.getBean(), arg.getScope())));
            else sb.append(arg.getValue());
        }
        form.setAttribute(getResult(), sb.toString(), (getScope() == null ? RequestUtils.REQUEST_SCOPE : getScope()));
        log.debug("Stored result in '" + getResult() + "'");
     }
    
    @SuppressWarnings("unchecked")
	public void configure(java.util.Map<String, ?> props) throws StepConfigException {
        super.configure(props);
        try {
            Object tmp = props.get("*");
            if(tmp != null) {
                List<?> argList = ConvertUtils.convert(List.class,  tmp);
                for(Object o : argList) {
                    addArg((Map<String,?>)o);
                }
            }
        } catch(ConvertException e) {
            throw new StepConfigException(e);
        } catch (IllegalAccessException e) {
            throw new StepConfigException(e);
        } catch (IntrospectionException e) {
            throw new StepConfigException(e);
        } catch (InvocationTargetException e) {
            throw new StepConfigException(e);
        } catch (InstantiationException e) {
            throw new StepConfigException(e);
        } catch(ParseException e) {
        	throw new StepConfigException(e);
		}
    }
    
    protected void addArg(Map<String,?> m) throws ConvertException, IllegalAccessException, 
            java.beans.IntrospectionException, java.lang.reflect.InvocationTargetException, InstantiationException, ParseException {
        Arg v = new Arg();
        ReflectionUtils.populateProperties(v, m);        
        args.add(v);
    }

    protected Object convertArg(String type, Object value) throws ClassNotFoundException, ConvertException {
        if(value == null || type == null) return value;
        return ConvertUtils.convert(Class.forName(type), value);
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /** Getter for property result.
     * @return Value of property result.
     *
     */
    public String getResult() {
        return this.result;
    }
    
    /** Setter for property result.
     * @param result New value of property result.
     *
     */
    public void setResult(String result) {
        this.result = result;
    }
    
    
    protected static class Arg {
        
        /** Holds value of property bean. */
        private String bean;
        
        /** Holds value of property scope. */
        private String scope;
        
        /** Holds value of property value. */
        private String value;
                
        /** Getter for property bean.
         * @return Value of property bean.
         *
         */
        public String getBean() {
            return this.bean;
        }
        
        /** Setter for property bean.
         * @param bean New value of property bean.
         *
         */
        public void setBean(String bean) {
            this.bean = bean;
        }
        
        /** Getter for property scope.
         * @return Value of property scope.
         *
         */
        public String getScope() {
            return this.scope;
        }
        
        /** Setter for property scope.
         * @param scope New value of property scope.
         *
         */
        public void setScope(String scope) {
            this.scope = scope;
        }
        
        /** Getter for property value.
         * @return Value of property value.
         *
         */
        public String getValue() {
            return this.value;
        }
        
        /** Setter for property value.
         * @param value New value of property value.
         *
         */
        public void setValue(String value) {
            this.value = value;
        }        
    }
}
