package simple.servlet.steps;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

public class TextStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    /** Holds value of property file. */
    private String file;
    /** Holds value of property scope. */
    private String scope;
    /** Holds value of property value. */
    private String value;
    /** Holds value of property bean. */
    private String bean;
    
    private String type;
    
	private String format;

	private String contentType = "text/plain";
    /**
     * Holds value of property downloadName.
     */
    private String downloadName;
    
    public TextStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        try { 
			response.setContentType(getContentType());
            String fileName;
            if(downloadName != null && downloadName.trim().length() > 0) {
                fileName = downloadName;              
                log.trace("File Name=" + fileName);
                response.setHeader("Content-Disposition","attachment;filename=" + fileName);
            }
            //write it out
            PrintWriter out = response.getWriter();
            String s = getText(form);
            if(log.isDebugEnabled()) {
                if(s.length() > 100)
                    log.debug("Writing text: " + s.substring(0, 100) + "... (" + (s.length()-100) + " more characters)");
                else 
                    log.debug("Writing text: " + s);
            }
            out.print(s);          
            out.flush();
        } catch (IOException e) {
            throw new ServletException(e);
        } 
    }    
           
    protected String getText(InputForm form) throws ServletException {
        Object v;
        if(value != null) v = value;
        else if(getBean() != null) {
            //log.debug("Looking for '" + getBean() + "' in form");
            v = form.get(getBean());
        }
        else v = null;
        if(type != null) try {
            log.debug("Converting value to " + type);
            v = ConvertUtils.convert(Class.forName(type), v);           
        } catch(ConvertException e) {
            throw new ServletException(e);
        } catch (ClassNotFoundException e) {
            log.warn("Class Not Found: " + type, e);
            throw new ServletException(e);
        }
        return ConvertUtils.formatObject(v, format);
    }

    /** Getter for property fromFile.
     * @return Value of property fromFile.
     *
     */
    public String getFile() {
        return this.file;
    }
    
    /** Setter for property fromFile.
     * @param fromFile New value of property fromFile.
     *
     */
    public void setFile(String fromFile) {
        this.file = fromFile;
    }
    
    /**
     * Getter for property downloadName.
     * @return Value of property downloadName.
     */
    public String getDownloadName() {
        return this.downloadName;
    }
    
    /**
     * Setter for property downloadName.
     * @param downloadName New value of property downloadName.
     */
    public void setDownloadName(String downloadName) {
        this.downloadName = downloadName;
    }

    public String getBean() {
        return bean;
    }

    public void setBean(String bean) {
        this.bean = bean;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
    
}

