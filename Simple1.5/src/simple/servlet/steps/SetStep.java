/*
 * SetStep.java
 *
 * Created on February 6, 2004, 5:49 PM
 */

package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;

/**
 *
 * @author  Brian S. Krug
 */
public class SetStep extends AbstractStep {
    private static final Log log = Log.getLog();
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property name. */
    private String name;
    
    /** Holds value of property value. */
    private String value;
    
    /** Holds value of property bean. */
    private String bean;
    
    private String type;
    
    private String format;
    
    /** Creates a new instance of SetStep */
    public SetStep() {
    }
    
    public void perform(simple.servlet.Dispatcher dispatcher, simple.servlet.InputForm form, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException {
        Object v;
        if(getBean() != null) {
            try {
				v = ReflectionUtils.getProperty(form, getBean());
			} catch (IntrospectionException e) {
				throw new ServletException(e);
			} catch (IllegalAccessException e) {
				throw new ServletException(e);
			} catch (InvocationTargetException e) {
				throw new ServletException(e);
			} catch(ParseException e) {
				throw new ServletException(e);
			}
            if(v == null) v = value; // let value be the default if bean is not in form
        } else v = value;
        if(type != null) try {
            log.debug("Converting value to " + type);
            v = ConvertUtils.convert(Class.forName(type), v);           
        } catch(ConvertException e) {
            throw new ServletException(e);
        } catch (ClassNotFoundException e) {
            log.warn("Class Not Found: " + type, e);
            throw new ServletException(e);
        }
        if(format != null) v = ConvertUtils.formatObject(v, format);
		if(log.isInfoEnabled())
			log.info("Setting attribute " + name + " to '" + v + "'");
    	form.setAttribute(name, v, (scope == null ? simple.servlet.RequestUtils.REQUEST_SCOPE : scope));
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public String getName() {
        return this.name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     *
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /** Getter for property value.
     * @return Value of property value.
     *
     */
    public String getValue() {
        return this.value;
    }
    
    /** Setter for property value.
     * @param value New value of property value.
     *
     */
    public void setValue(String value) {
        this.value = value;
    }
    
	/**
	 * @return Returns the bean.
	 */
	public String getBean() {
		return bean;
	}
	/**
	 * @param bean The bean to set.
	 */
	public void setBean(String bean) {
		this.bean = bean;
	}

    /**
     * @return Returns the format.
     */
    public String getFormat() {
        return format;
    }

    /**
     * @param format The format to set.
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }
}
