package simple.servlet.steps;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Call;
import simple.db.CallNotFoundException;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.results.BeanException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.StepConfigException;

public class DbIntoStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    protected List<String> calls = new java.util.LinkedList<String>();
    protected DataLayer dataLayer = simple.db.DataLayerMgr.getGlobalDataLayer();

    /** Holds value of property dataSource. */
    protected String dataSource;

    
    public DbIntoStep() {
    }

    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        perform(form, true);
    }

    protected void perform(InputForm form, boolean retry) throws ServletException {
        //check for early exit
        Connection conn = null;
        try {
            conn = getConnection(form);
            executeCalls(conn, form);
        } catch(SQLException sqle) {
            throw new ServletException(sqle);
        } catch (DataLayerException e) {
            throw new ServletException(e);
        } catch (BeanException e) {
            throw new ServletException(e);
        } finally {
            if(conn != null) try { conn.close(); } catch(SQLException e) {}
        }
    }

    protected void executeCalls(Connection conn, InputForm form) throws BeanException, SQLException, DataLayerException {
        for(String callId : calls) {
            try {
            	dataLayer.selectInto(conn, callId, form);
            } catch(SQLException e) {
            	throw new SQLException("For Query '" + callId + "': " + e.getMessage(), e.getSQLState(), e.getErrorCode(), e);
            }
        }
    }

    protected Connection getConnection(InputForm form) throws SQLException, DataLayerException {
        String ds = findDataSource();
        log.debug("Obtaining db connection for '" + ds + "' for use with " + calls.size() + " db queries");
        return dataLayer.getConnection(ds);
    }

    /** Getter for property dataSource.
     * @return Value of property dataSource.
     *
     */
    public String getDataSource() {
        return this.dataSource;
    }

    protected String findDataSource() throws CallNotFoundException {
        if(dataSource == null && !calls.isEmpty()) {
            log.debug("Getting DataSource from call...");
            String callId = calls.get(0);
            Call call = dataLayer.findCall(callId);
            dataSource = call.getDataSourceName();
        }
        return this.dataSource;
    }

    /** Setter for property dataSource.
     * @param dataSource New value of property dataSource.
     *
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @see simple.servlet.steps.AbstractStep#configure(java.util.Map)
     */
    @Override
	public void configure(java.util.Map<String,?> props) throws StepConfigException {
        super.configure(props);
        Object tmp = props.get("calls");
        if(tmp != null) {
            try {
            	List<?> callList = ConvertUtils.convert(List.class,  tmp);
                for(Object c : callList) {
                    addCall(c);
                }
            } catch(ConvertException e) {
                throw new StepConfigException(e);
            }
        }
    }

    protected void addCall(Object o) {
        if(o == null) return;
        String callId;
        if(o instanceof Map<?,?>) {
            Map<?,?> m = (Map<?,?>)o;
            callId = (String)m.get("id");
        } else {
        	callId = o.toString();
        }
        log.debug("Added Call (id="+callId + ")");
        calls.add(callId);
    }
}

