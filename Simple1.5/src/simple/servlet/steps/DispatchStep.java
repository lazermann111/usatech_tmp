package simple.servlet.steps;

import javax.servlet.ServletException;

import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

public class DispatchStep extends AbstractStep {
	private static final Log log = Log.getLog();
    /** Holds value of property action. */
    private String action;
    
    public DispatchStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
		if(log.isInfoEnabled())
			log.info("Performing action '" + action + "'");
		dispatcher.dispatch(action, true);
    }    

    /** Getter for property action.
     * @return Value of property action.
     *
     */
    public String getAction() {
        return this.action;
    }
    
    /** Setter for property action.
     * @param page New value of property action.
     *
     */
    public void setAction(String action) {
        this.action = action;
    }    
}

