/*
 * Created on Jan 14, 2005
 *
 */
package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.fop.apps.Driver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.BufferReaderWriter;
import simple.io.HeapBufferReaderWriter;
import simple.io.Log;
import simple.io.ResourceResolver;
import simple.io.logging.avalon.SimpleAvalonLogger;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.ServletContextResourceResolver;
import simple.servlet.StepConfigException;
import simple.translator.Translator;
import simple.util.CaseInsensitiveComparator;
import simple.util.CollectionUtils;
import simple.util.ExcludingMap;
import simple.util.StaticCollection;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;
import simple.xml.TemplatesLoader;
import simple.xml.dom.BaseDocument;
import simple.xml.dom.BaseDocumentFragment;
import simple.xml.dom.BaseNodeList;
import simple.xml.dom.PropertyElementNode;
import simple.xml.sax.AggregatingAdapter;
import simple.xml.sax.AggregatingAdapter2;
import simple.xml.sax.AggregatingInputSource;
import simple.xml.sax.AggregatingInputSource2;
import simple.xml.sax.BeanAdapter0;
import simple.xml.sax.BeanAdapter1;
import simple.xml.sax.BeanAdapter2;
import simple.xml.sax.BeanInputSource;
import simple.xml.sax.ContextLoggingErrorHandler;
import simple.xml.sax.LoggingErrorHandler;
import simple.xml.sax.MessagesInputSource;
import simple.xml.sax.ParametersAdapter;
import simple.xml.sax.ParametersInputSource;
import simple.xml.sax.PlaybackAdapter;
import simple.xml.sax.ResultsAdapter;
import simple.xml.sax.ResultsInputSource;
import simple.xml.serializer.XHTMLHandler;

/**
 * @author bkrug
 *
 */
public class TransformStep extends AbstractStep {
    private static final Log log = Log.getLog();
    public static final String ATTRIBUTE_SOURCE_ONLY = "simple.servlet.steps.TransformStep.sourceOnly";
    protected static final Comparator<String> comparator = new CaseInsensitiveComparator<String>();
    protected static final ErrorHandler saxErrorHandler = new LoggingErrorHandler();
    protected boolean translating = true;
    protected List<SourceConfig> sourceConfigs = new ArrayList<SourceConfig>();
    protected String result;
    protected String resultScope;
    protected String xsl;
    protected String contentType;
    protected String downloadName;
    protected String parameterName = "context";
    protected URL xslUrl;
    protected final Cache<ServletContext,ResourceResolver,RuntimeException> resourceResolvers = new LockSegmentCache<ServletContext,ResourceResolver,RuntimeException>(16, 0.75f, 8) {		
		@Override
		protected ResourceResolver createValue(ServletContext key, Object... additionalInfo) throws RuntimeException {
			return new ServletContextResourceResolver(key);
		}
	};
    protected String dispatcherName = null;
    protected int version = 1;
    protected boolean injectSessionToken = true;
    protected Collection<String> skipParameters = Collections.emptySet();
    protected class SourceConfig {
        protected String xpath;
        protected String attr;
        protected String href;
        protected String include;
        protected String action;
        protected String scope;
        protected boolean required = true;
        protected String namespace;
        protected final Map<String,String> extra = new HashMap<String, String>();

        public SourceConfig() {
        }
        public String getAttr() {
            return attr;
        }
        public void setAttr(String attr) {
            this.attr = attr;
        }
        public String getScope() {
            return scope;
        }
        public void setScope(String scope) {
            this.scope = scope;
        }
        public String getXpath() {
            return xpath;
        }
        public void setXpath(String xpath) {
            this.xpath = xpath;
        }
        public String getHref() {
            return href;
        }
        public void setHref(String href) {
            this.href = href;
        }
        public boolean isRequired() {
            return required;
        }
        public void setRequired(boolean required) {
            this.required = required;
        }
		public Map<String, String> getExtra() {
			return extra;
		}
		public String getInclude() {
			return include;
		}
		public void setInclude(String include) {
			this.include = include;
		}
		public String getAction() {
			return action;
		}
		public void setAction(String action) {
			this.action = action;
		}
		public String getNamespace() {
			return namespace;
		}
		public void setNamespace(String namespace) {
			this.namespace = namespace;
		}
    }
    /**
     *
     */
    public TransformStep() {
        super();
    }

    /* (non-Javadoc)
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
            HttpServletResponse response) throws ServletException {
        try {
			// get the input source
            Source src = getSource(form, dispatcher);
            // create the result object
            Result res;
            boolean sourceOnly = form.getBoolean(ATTRIBUTE_SOURCE_ONLY, false, false);
            // add Response OutputStream
            if(isWritePage()) {
            	if(sourceOnly) {
            		response.setContentType("text/xml");
            		res = new StreamResult(response.getWriter());
					if(log.isInfoEnabled())
						log.info("Transforming input to text/xml");
            	} else {
	                if(getContentType() != null) {
	                    response.setContentType(getContentType());
						if(log.isInfoEnabled())
							log.info("Transforming '" + getXsl() + "' to " + getContentType());
					} else if(log.isInfoEnabled())
						log.info("Transforming '" + getXsl() + "'");
	                if("application/pdf".equals(getContentType())) {
	                    // render the PDF
	                    Driver driver = new Driver();
	                    driver.setLogger(new SimpleAvalonLogger(log));
	                    driver.setRenderer(Driver.RENDER_PDF);
	                    driver.setOutputStream(response.getOutputStream());
	                    res = new SAXResult(driver.getContentHandler());
	                } else {
	                    res = new StreamResult(response.getWriter());
	                }
            	}
                //String fn = form.getString(ATTRIBUTE_SEARCH_PATH, false);
                if(getDownloadName() != null) response.setHeader("Content-Disposition","attachment;filename=" + getDownloadName());
            } else {
                Object resultObject = form.getAttribute(getResult(), getResultScope());
                if(resultObject instanceof Result) res = (Result)resultObject;
                else if(resultObject instanceof Node) res = new DOMResult((Node)resultObject);
                else {
                    res = new DOMResult();
                }
                form.setAttribute(getResult(), res, getResultScope());
				if(log.isInfoEnabled())
					log.info("Transforming '" + getXsl() + "' to DOM Node and storing in attribute " + getResult());
            }

            Transformer transformer;
            if(sourceOnly) {
            	transformer = TemplatesLoader.getInstance(null).newTransformer();
            } else {
            	ServletContext sc = request.getSession().getServletContext();
                URL url = getXslUrl(sc);
                Long interval = (Long)sc.getAttribute("refresh-interval");
                Translator translator = null;
                if(isTranslating()) {
					translator = form.getTranslator();
                    log.debug("Translating using " + translator);
                }
                transformer = TemplatesLoader.getInstance(translator).newTransformer(url, interval == null ? -1 : interval);
                if(isInjectSessionToken()) {
                	String token = RequestUtils.getSessionTokenIfPresent(request);
                	if(token != null)
                		transformer.setOutputProperty(XHTMLHandler.SESSION_TOKEN, token);
                }
            }
            //transformer.setParameter("form", getBeanAsNode(form, "form"));
            transformer.setParameter(getParameterName(), form);
            if(getDispatcherName() != null && getDispatcherName().trim().length() > 0) {
                transformer.setParameter(getDispatcherName(), dispatcher);
            }
            //PerformanceTracker.getDefaultInstance().startTask("transform", "XSL=" + url);
	        transformer.transform(src, res);
	        //PerformanceTracker.getDefaultInstance().endTask("transform");
	        //out.flush();
        } catch (ServletException se) {
            //log.warn("Could not transform xml", se);
            throw se;
        } catch (TransformerException e) {
        	String location = TemplatesLoader.getXSLLocation(e);
            SAXException saxe = getUnderlyingSAXException(e);
            if(saxe != null) {
                if(saxe.getException() != null)
                    throw new ServletException("At " + location, saxe.getException());
                else
                    throw new ServletException("At " + location, saxe);
            }
            throw new ServletException("At " + location, e);
        } catch (Exception ex) {
            //log.warn("Could not transform xml", ex);
            throw new ServletException(ex);
        } finally {
            //out.close();
        }

    }

    protected SAXException getUnderlyingSAXException(TransformerException e) {
        SAXException last = null;
        int i = 0;
        for(Throwable cause = e.getCause(); cause != null && i < 200; i++) {
            cause.printStackTrace();
            if(cause instanceof SAXException) {
                last = (SAXException)cause;
                cause = last.getException();
            } else {
                cause = cause.getCause();
            }
        }
        return last;
    }

    protected Node getBeanAsNode(Object bean, String name) {
        List<Node> l = new ArrayList<Node>(1);
        Node frag = new BaseDocumentFragment(new BaseNodeList(l));
        Node node = new PropertyElementNode(Collections.singletonMap(name, bean), name, frag, 0);
        l.add(node);
        return node;
    }

    protected Document getBeanAsDocument(Object bean, String name) {
        BaseDocument doc = new BaseDocument();
        doc.setDocumentElement(new PropertyElementNode(Collections.singletonMap(name, bean), name, doc, 0));
        return doc;
    }

    protected boolean isParametersAsSource() {
    	switch(getVersion()) {
    		case 0: case 1: return true;
    		case 2: return false;
    		default:
    			log.warn("Version " + getVersion() + " is not valid for TransformStep - Using version 2");
				return false;
    	}
    }
    protected Source getSource(InputForm form, Dispatcher dispatcher) throws ServletException {
        SAXSource ret;
        int offset;
        Source[] sources;
        String[] xpaths;
        String[] namespaces;
		MessagesInputSource mis = RequestUtils.getMessagesSource(form);
        switch(getVersion()) {
    		case 0:
    		case 1: {
				offset = (mis == null || mis.getMessageCount() == 0 ? 1 : 2);
				sources = new Source[sourceConfigs.size() + offset];
				xpaths = new String[sourceConfigs.size() + offset];
				namespaces = new String[sourceConfigs.size() + offset];
    	        /*NOTE: new instance of ParametersAdapter is needed because handlers of XMLReader's maybe different for each run */
    	        sources[0] = new SAXSource(new ParametersAdapter(skipParameters), new ParametersInputSource(form.getParameters()));
    	        xpaths[0] = "parameters";
				if(offset > 1) {
					sources[1] = new SAXSource(new PlaybackAdapter(), mis);
					xpaths[1] = "messages";
				}
    	        ret = new SAXSource(new AggregatingAdapter(), new AggregatingInputSource(sources, xpaths, namespaces));
    			}
    			break;
    		case 2: {
				offset = (mis == null || mis.getMessageCount() == 0 ? 0 : 1);
				sources = new Source[sourceConfigs.size() + offset];
				xpaths = new String[sourceConfigs.size() + offset];
				namespaces = new String[sourceConfigs.size() + offset];
				if(offset > 0) {
					sources[0] = new SAXSource(new PlaybackAdapter(), mis);
					xpaths[0] = "messages";
				}
    	        ret = new SAXSource(new AggregatingAdapter2(), new AggregatingInputSource2(sources, xpaths, namespaces,
    					(skipParameters.isEmpty() ? form.getParameters() : new ExcludingMap<String,Object>(form.getParameters(), skipParameters))));
    			}
    			break;
    		default: throw new IllegalStateException("Invalid version " + getVersion());
    	}
        ContextLoggingErrorHandler contextErrorHandler;
        if(log.isDebugEnabled()) {
        	contextErrorHandler = new ContextLoggingErrorHandler();
        	ret.getXMLReader().setErrorHandler(contextErrorHandler);
        } else {
        	contextErrorHandler = null;
        	ret.getXMLReader().setErrorHandler(saxErrorHandler);
        }
    	for(int i = offset; i < sources.length; i++) {
			SourceConfig srcCfg = sourceConfigs.get(i - offset);
            xpaths[i] = srcCfg.getXpath();
            namespaces[i] = srcCfg.getNamespace();
            Source src;
            if(srcCfg.getHref() != null && srcCfg.getHref().trim().length() > 0) {
                try {
                    src = new SAXSource(new InputSource(new URL(srcCfg.getHref()).openStream()));
                } catch (MalformedURLException e) {
                    throw new ServletException(e);
                } catch (IOException e) {
                    throw new ServletException(e);
                }
            } /*else if(srcCfg.getAction() != null && srcCfg.getAction().trim().length() > 0) {
            	
            } */else if(srcCfg.getInclude() != null && srcCfg.getInclude().trim().length() > 0) {
            	/*
            	BufferStream buffer = new BufferStream();
                dispatcher.intercept(srcCfg.getInclude(), buffer.getOutputStream());
                src = new SAXSource(new InputSource(buffer.getInputStream()));
                */
				BufferReaderWriter buffer = new HeapBufferReaderWriter();
                dispatcher.intercept(srcCfg.getInclude(), buffer.getWriter());
                Reader reader = buffer.getReader();
                URI systemId;
				try {
					systemId = new URI("sax", "/TransformSource/" + srcCfg.getXpath(), null);
				} catch(URISyntaxException e) {
					log.warn("Could not create URI", e);
					systemId = null;
				}
                if(contextErrorHandler != null && systemId != null)
                	reader = contextErrorHandler.registerContext(systemId, reader, 9000);
                InputSource is = new InputSource(reader);
                is.setSystemId(systemId.toString());
                src = new SAXSource(is);
            } else {
                Object sourceObject = form.getAttribute(srcCfg.getAttr(), srcCfg.getScope());
                if(sourceObject == null) {
                    if(srcCfg.isRequired())
                        throw new ServletException("No data found in attribute '" + srcCfg.getAttr() + "' of the form");
                    else
                        src = null;
                } else if(sourceObject instanceof Source) src = (Source)sourceObject;
                else if(sourceObject instanceof InputSource) src = new SAXSource((InputSource)sourceObject);
                else if(sourceObject instanceof java.io.InputStream) src = new SAXSource(new InputSource((java.io.InputStream)sourceObject));
                else if(sourceObject instanceof java.io.Reader) src = new SAXSource(new InputSource((java.io.Reader)sourceObject));
                else if(sourceObject instanceof Node) src = new DOMSource((Node)sourceObject);
                else if(sourceObject instanceof ByteArrayOutputStream) src = new SAXSource(new InputSource(new ByteArrayInputStream(((ByteArrayOutputStream)sourceObject).toByteArray())));
                else if(sourceObject instanceof Results) src = new SAXSource(new ResultsAdapter(), new ResultsInputSource((Results)sourceObject, ConvertUtils.getBooleanSafely(srcCfg.getExtra().get("showMetadata"), false)));
                //else if(sourceObject instanceof String) src = new SAXSource(new InputSource(new StringReader((String)sourceObject)));
                else {
                	String name;
                	if(getVersion() == 2) {
    	                name = null;
                	} else {
	                    int p = xpaths[i].lastIndexOf('/');
	                    name = (p < 0 ? xpaths[i] : xpaths[i].substring(p+1));
	                    String newXpath = (p < 0 ? "" : xpaths[i].substring(0, p));
	                    xpaths[i]= newXpath;
                	}
                    //src = new DOMSource(getBeanAsDocument(sourceObject, name));
                    src = new SAXSource(createBeanAdapter(), new BeanInputSource(sourceObject, name));
                }
            }
            sources[i] = src;
        }

        return ret;
    }

    protected XMLReader createBeanAdapter() {
    	switch(getVersion()) {
    		case 0: return new BeanAdapter0();
    		case 1: return new BeanAdapter1();
    		case 2: return new BeanAdapter2();
    		default: throw new IllegalStateException("Invalid version " + getVersion());
    	}
    }
    protected ResourceResolver getResourceResolver(ServletContext servletContext) {
        return resourceResolvers.getOrCreate(servletContext);
    }

    /**
     * @return
     * @throws ServletException
     * @throws MalformedURLException
     */
    protected URL getXslUrl(ServletContext servletContext) throws ServletException, IOException {
        if(xslUrl == null) {
            if(getXsl() == null) throw new ServletException("Attribute 'xsl' is required for a 'transform' step");
            xslUrl = getResourceResolver(servletContext).getResourceURL(getXsl());
            if(xslUrl == null) throw new ServletException("Could not get the resource at '" + getXsl() + "'");
        }
        return xslUrl;
    }

    @Override
	public void configure(Map<String,?> props) throws StepConfigException {
        super.configure(props);
        try {
        Object tmp = props.get("sources");
        if(tmp instanceof List<?>) {
            for(Object o : (List<?>)tmp) {
                addSourceConfig(CollectionUtils.uncheckedMap((Map<?,?>)o, String.class, Object.class));
            }
        }/* else if(tmp instanceof String) {
                String[] srcs = StringUtils.split((String)tmp, StringUtils.STANDARD_DELIMS, false);

        }//*/
        } catch(IntrospectionException e) {
            throw new StepConfigException(e);
        } catch (IllegalAccessException e) {
            throw new StepConfigException(e);
        } catch (InvocationTargetException e) {
            throw new StepConfigException(e);
        } catch (InstantiationException e) {
            throw new StepConfigException(e);
        } catch (ConvertException e) {
            throw new StepConfigException(e);
        } catch(ParseException e) {
        	throw new StepConfigException(e);
		}
    }

	public void addSourceConfig(Map<String,?> props) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
	    SourceConfig srcCfg = new SourceConfig();
	    simple.bean.ReflectionUtils.populateProperties(srcCfg, props);
	    sourceConfigs.add(srcCfg);
	}

    /**
     * @return
     */
    public boolean isWritePage() {
        return getResult() == null;
    }

    /**
     * Getter for property xsl.
     * @return Value of property xsl.
     */
    public String getXsl() {

        return this.xsl;
    }

    /**
     * Setter for property xsl.
     * @param xsl New value of property xsl.
     */
    public void setXsl(String xsl) {

        this.xsl = xsl;
    }

    /**
     * @return Returns the result.
     */
    public String getResult() {
        return result;
    }
    /**
     * @param result The result to set.
     */
    public void setResult(String result) {
        this.result = result;
    }
    /**
     * @return Returns the resultScope.
     */
    public String getResultScope() {
        return resultScope;
    }
    /**
     * @param resultScope The resultScope to set.
     */
    public void setResultScope(String resultScope) {
        this.resultScope = resultScope;
    }
    /**
     * @return Returns the contentType.
     */
    public String getContentType() {
        return contentType;
    }
    /**
     * @param contentType The contentType to set.
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    /**
     * @return Returns the downloadName.
     */
    public String getDownloadName() {
        return downloadName;
    }
    /**
     * @param downloadName The downloadName to set.
     */
    public void setDownloadName(String downloadName) {
        this.downloadName = downloadName;
    }

    /**
     * @return Returns the parameterName.
     */
    public String getParameterName() {
        return parameterName;
    }

    /**
     * @param parameterName The parameterName to set.
     */
    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    /**
     * @return Returns the dispatcherName.
     */
    public String getDispatcherName() {
        return dispatcherName;
    }

    /**
     * @param dispatcherName The dispatcherName to set.
     */
    public void setDispatcherName(String dispatcherName) {
        this.dispatcherName = dispatcherName;
    }

    public boolean isTranslating() {
        return translating;
    }

    public void setTranslating(boolean translating) {
        this.translating = translating;
    }

	public Collection<String> getSkipParameters() {
		return skipParameters;
	}

	public void setSkipParameters(String skipParameters) throws ConvertException {
		this.skipParameters = StaticCollection.create(comparator, ConvertUtils.convert(String[].class, skipParameters));
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		if(version < 0 || version > 2) {
			log.warn("Version " + getVersion() + " is not valid for TransformStep - Using version " + this.version);
		} else {
			this.version = version;
		}
	}

	public boolean isInjectSessionToken() {
		return injectSessionToken;
	}

	public void setInjectSessionToken(boolean injectSessionToken) {
		this.injectSessionToken = injectSessionToken;
	}


}
