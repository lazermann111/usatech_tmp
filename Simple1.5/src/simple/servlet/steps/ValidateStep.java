package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.StepConfigException;
import simple.util.CollectionUtils;

public class ValidateStep extends AbstractStep {
    protected List<ValidateCheck> checks = new ArrayList<ValidateCheck>();    
    protected String messagePrefix = "Could not process this request because ";
    protected String messageSeparator = " and ";
    protected String messageSuffix = ". Please correct these issues and re-submit.";
    
    /** Holds value of property earlyExit. */
    private boolean earlyExit;
    
    public ValidateStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        ValidateException ve = null;
        for(ValidateCheck v : checks) {
            //perform check
            Object o = form.getAttribute(v.getBean(), v.getScope());
            if(!v.check(o)) {
				if(ve == null)
					ve = new ValidateException(form.getTranslator(), getMessagePrefix(), getMessageSeparator(), getMessageSuffix());
                ve.addCheck(v, o);
                if(earlyExit) return;
            } else if(v.getFormat() != null){
            	form.setAttribute(v.getBean(), v.format(o), v.getScope());
            }
        }
        if(ve != null) throw ve;
    }  
    
    public void configure(java.util.Map<String,?> props) throws StepConfigException {
        super.configure(props);
        try {
        List<?> list = ConvertUtils.convert(List.class,  props.get("checks"));
        if(list != null)
            for(Object chk : list)
                addCheck(chk);
        } catch(ConvertException e) {
            throw new StepConfigException(e);
        } catch (IllegalAccessException e) {
            throw new StepConfigException(e);
        } catch (IntrospectionException e) {
            throw new StepConfigException(e);
        } catch (InvocationTargetException e) {
            throw new StepConfigException(e);
        } catch (InstantiationException e) {
            throw new StepConfigException(e);
        } catch(ParseException e) {
        	throw new StepConfigException(e);
		}
    }
        
    protected void addCheck(Object o) throws ConvertException, IllegalAccessException, 
    java.beans.IntrospectionException, java.lang.reflect.InvocationTargetException, InstantiationException, ParseException {
        if(o == null) return;
        if(!(o instanceof Map<?,?>)) throw new ConvertException("Validation checks are configured using sub-elements", ValidateCheck.class, o);
        Map<String,Object> m = CollectionUtils.uncheckedMap((Map<?,?>)o, String.class, Object.class);
        ValidateCheck v = new ValidateCheck();
        ReflectionUtils.populateProperties(v, m);        
        //log.debug("Added Check (bean="+check.bean);
        checks.add(v);
    }
        
    /** Getter for property earlyExit.
     * @return Value of property earlyExit.
     *
     */
    public boolean isEarlyExit() {
        return this.earlyExit;
    }
    
    /** Setter for property earlyExit.
     * @param earlyExit New value of property earlyExit.
     *
     */
    public void setEarlyExit(boolean earlyExit) {
        this.earlyExit = earlyExit;
    }

	public String getMessagePrefix() {
		return messagePrefix;
	}

	public void setMessagePrefix(String messagePrefix) {
		this.messagePrefix = messagePrefix;
	}

	public String getMessageSeparator() {
		return messageSeparator;
	}

	public void setMessageSeparator(String messageSeparator) {
		this.messageSeparator = messageSeparator;
	}

	public String getMessageSuffix() {
		return messageSuffix;
	}

	public void setMessageSuffix(String messageSuffix) {
		this.messageSuffix = messageSuffix;
	}
    
}

