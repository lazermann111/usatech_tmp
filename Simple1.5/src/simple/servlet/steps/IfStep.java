package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleAction;
import simple.servlet.SimpleServlet;
import simple.servlet.Step;
import simple.servlet.StepConfigException;
/** This steps performs the steps it contains only if its conditions are met
 *
 */
public class IfStep extends AbstractStep {
	private static final Log log = Log.getLog();
    protected List<Check> checks = new LinkedList<Check>();    
    protected List<Step> steps = new LinkedList<Step>();    
    protected List<Step> elseSteps = new LinkedList<Step>();    
    
    public IfStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
    	boolean yes = true;
        for(Check check : checks) {
            //perform check
            Object o = form.getAttribute(check.getBean(), check.getScope());
            if(!check.check(o)) {
            	yes = false;
            	break;
            }
        }
		if(log.isInfoEnabled())
			log.info(yes ? "Passed check; executing " + steps.size() + " true steps" : "Failed check; executing " + elseSteps.size() + " false steps");
        //now perform all the sub-steps
        for(Step s : (yes ? steps : elseSteps)) {
            SimpleAction.invokeStep(s, dispatcher, form, request, response);
        }        
    }  
    
	public void configure(java.util.Map<String, ?> props) throws StepConfigException {
        super.configure(props);
        try {
            if(props.containsKey("operator")) {// inline check
                addCheck(props);
            }
			List<?> list = ConvertUtils.convert(List.class, props.get("*"));
            if(list != null) {
				for(Iterator<?> iter = list.iterator(); iter.hasNext();) {
					Map<String, ?> m = (Map<String, ?>) iter.next();
                    String tag = m.get("tag").toString();
                    if("check".equalsIgnoreCase(tag)) addCheck(m);
                    else if("else".equalsIgnoreCase(tag)) addElse(m);
                    else addStep(m);
                }
            }
        } catch(ConvertException e) {
            throw new StepConfigException(e);
        } catch (IllegalAccessException e) {
            throw new StepConfigException(e);
        } catch (IntrospectionException e) {
            throw new StepConfigException(e);
        } catch (InvocationTargetException e) {
            throw new StepConfigException(e);
        } catch (InstantiationException e) {
            throw new StepConfigException(e);
        } catch (ClassNotFoundException e) {
            throw new StepConfigException(e);
        } catch (SAXException e) {
            throw new StepConfigException(e);
        } catch(ParseException e) {
        	throw new StepConfigException(e);
		}     
    }
        
	private void addElse(Map<?, ?> props) throws ConvertException, ClassNotFoundException, SAXException, InstantiationException, IllegalAccessException, StepConfigException {
		List<Map<String, ?>> list = ConvertUtils.convert(List.class, props.get("*"));
        if(list != null) {
			for(Iterator<Map<String, ?>> iter = list.iterator(); iter.hasNext();) {
				Map<String, ?> m = iter.next();
                /*String tag = m.get("tag").toString();
                if("check".equalsIgnoreCase(tag)) addCheck(m);
                else if("else".equalsIgnoreCase(tag)) addElse(m);
                else */addElseStep(m);
            }
        }
	}

	protected void addCheck(Map<String, ?> m) throws ConvertException, IllegalAccessException,
    java.beans.IntrospectionException, java.lang.reflect.InvocationTargetException, InstantiationException, ParseException {
        Check v = new Check();
        ReflectionUtils.populateProperties(v, m);        
        //log.debug("Added Check (bean="+check.bean);
        checks.add(v);
    }
    
	protected void addStep(Map<String, ?> m) throws ClassNotFoundException, SAXException, InstantiationException, IllegalAccessException, StepConfigException {
        Step step = SimpleServlet.loadStep(m);
        if(step != null) steps.add(step);
    } 

	protected void addElseStep(Map<String, ?> m) throws ClassNotFoundException, SAXException, InstantiationException, IllegalAccessException, StepConfigException {
        Step step = SimpleServlet.loadStep(m);
        if(step != null) elseSteps.add(step);
    } 
}

