package simple.servlet.steps;

import simple.translator.Translator;

public class ValidateCheck extends Check {
	protected String messageKey;
	protected String messageDefaultText;
	
	public ValidateCheck() {
		super();
	}

	public ValidateCheck(String messageKey, String messageDefaultText) {
		super();
		this.messageKey = messageKey;
		this.messageDefaultText = messageDefaultText;
	}

	/**
	 * Creates a String from a MessageFormat using the pattern
	 * specified in the <code>messageKey</code> (for Translator lookup) or <code>messageDefaultText</code> property
	 * 
	 * @param translator
	 *            The translator to use to get the message
	 * @param beanValue
	 *            The value being tested against this Check
	 */
    public String produceMessage(Translator translator, Object beanValue) {
        return translator.translate(getMessageKey(), getMessageDefaultText(), beanValue);
    }

	public String getMessageKey() {
		return messageKey;
	}

	public void setMessageKey(String messageKey) {
		this.messageKey = messageKey;
	}

	public String getMessageDefaultText() {
		return messageDefaultText;
	}

	public void setMessageDefaultText(String messageDefaultText) {
		this.messageDefaultText = messageDefaultText;
	}
	
	public String getMessage() {
		return getMessageDefaultText();
	}

	public void setMessage(String messageDefaultText) {
		setMessageDefaultText(messageDefaultText);
	}
}
