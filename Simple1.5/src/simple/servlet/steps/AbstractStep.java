/*
 * AbstractStep.java
 *
 * Created on December 24, 2003, 12:07 PM
 */

package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ReflectionUtils;
import simple.servlet.Catch;
import simple.servlet.SimpleServlet;
import simple.servlet.Step;
import simple.servlet.StepConfigException;

/**
 * Abstract implementation of Step that takes care of adding and returning Catch objects.
 * Also, provides a default implementation of configure() which populates any properties on 
 * this object using the values of the Map parameter passed to the method.
 *
 * @author  Brian S. Krug
 */
public abstract class AbstractStep implements Step {
    protected List<Catch> catchList = new LinkedList<Catch>();
    
    /** Creates a new instance of AbstractStep */
    public AbstractStep() {
    }
    
    public void addCatch(Catch c) {
        catchList.add(c);
    }
    
    public void configure(Map<String,?> props) throws StepConfigException {
        try {
			List<?> l;
			if(!ReflectionUtils.hasProperty(this, "propertys")) {
				l = (List<?>) props.get("propertys");
				if(l != null) {
					for(Iterator<?> iter = l.iterator(); iter.hasNext();) {
						Map<String, ?> m = (Map<String, ?>) iter.next();
						Object tmp = m.get("name");
						if(!(tmp instanceof String))
							continue;
						String key = (String) tmp;
						tmp = m.get("value");
						if(tmp != null)
							ReflectionUtils.setBeanProperty(this, key, tmp, true, true);
					}
				}
			}
            ReflectionUtils.populateProperties(this, props);
			l = (List<?>) props.get("catchs");
            if(l != null) {
                for(Iterator<?> iter = l.iterator(); iter.hasNext(); ) {
                    addCatch(SimpleServlet.loadCatch((Map<?,?>)iter.next()));
                }
            }
        } catch (IntrospectionException e) {
            throw new StepConfigException(e);
        } catch (IllegalAccessException e) {
            throw new StepConfigException(e);
        } catch (InvocationTargetException e) {
            throw new StepConfigException(e);
        } catch (InstantiationException e) {
            throw new StepConfigException(e);
        } catch (ConvertException e) {
            throw new StepConfigException(e);
        } catch (ClassNotFoundException e) {
            throw new StepConfigException(e);
        } catch (SAXException e) {
            throw new StepConfigException(e);
        } catch (StepConfigException e) {
            throw new StepConfigException(e);
        } catch(ParseException e) {
            throw new StepConfigException(e);
      	}
    }
    
    public Iterator<Catch> getCatches() {
        return catchList.iterator();
    }
    
}
