/*
 * SetStep.java
 *
 * Created on February 6, 2004, 5:49 PM
 */

package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import javax.servlet.ServletException;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.translator.Translator;

/**
 *
 * @author  Brian S. Krug
 */
public class TranslateStep extends AbstractStep {
    private static final Log log = Log.getLog();
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property name. */
    private String name;
    
    /** Holds value of property value. */
    private String key;
    
    /** Holds value of property bean. */
    private String bean;
    
    private String defaultText;
    
    /** Creates a new instance of SetStep */
    public TranslateStep() {
    }
    
    public void perform(simple.servlet.Dispatcher dispatcher, simple.servlet.InputForm form, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException {
        String k;
        if(getBean() != null) {
            try {
				k = ConvertUtils.getStringSafely(ReflectionUtils.getProperty(form, getBean()));
			} catch (IntrospectionException e) {
				throw new ServletException(e);
			} catch (IllegalAccessException e) {
				throw new ServletException(e);
			} catch (InvocationTargetException e) {
				throw new ServletException(e);
			} catch(ParseException e) {
				throw new ServletException(e);
			}
            if(k == null) k = key; // let value be the default if bean is not in form
        } else k = key;
		Translator translator = form.getTranslator();
		String text = translator.translate(k, defaultText, form);
        if(log.isDebugEnabled()) log.debug("Setting attribute " + name + " to '" + text + "' (translated)");
    	form.setAttribute(name, text, (scope == null ? simple.servlet.RequestUtils.REQUEST_SCOPE : scope));
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public String getName() {
        return this.name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     *
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /** Getter for property value.
     * @return Value of property value.
     *
     */
    public String getKey() {
        return this.key;
    }
    
    /** Setter for property value.
     * @param value New value of property value.
     *
     */
    public void setKey(String value) {
        this.key = value;
    }
    
	/**
	 * @return Returns the bean.
	 */
	public String getBean() {
		return bean;
	}
	/**
	 * @param bean The bean to set.
	 */
	public void setBean(String bean) {
		this.bean = bean;
	}

	public String getDefaultText() {
		return defaultText;
	}

	public void setDefaultText(String defaultText) {
		this.defaultText = defaultText;
	}


}
