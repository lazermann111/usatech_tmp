package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.StepConfigException;

public class AddMessageStep extends AbstractStep {
	protected final List<Arg> args = new java.util.LinkedList<Arg>();
	protected String messageType;
	protected String messageKey;

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		Object[] parameters = new Object[args.size()];
		for(int i = 0; i < parameters.length; i++)
			try {
				parameters[i] = args.get(i).retrieveValue(form);
			} catch(ClassNotFoundException e) {
				throw new ServletException(e);
			} catch(ConvertException e) {
				throw new ServletException(e);
			}
		RequestUtils.getOrCreateMessagesSource(form).addMessage(getMessageType(), messageKey, parameters);
	}

	public void configure(Map<String, ?> props) throws StepConfigException {
		super.configure(props);
		try {
			Object tmp = props.get("params");
			if(tmp != null) {
				List<?> argList = ConvertUtils.convert(List.class, tmp);
				for(Iterator<?> iter = argList.iterator(); iter.hasNext();) {
					addArg((Map<String, ?>) iter.next());
				}
			}
		} catch(ConvertException e) {
			throw new StepConfigException(e);
		} catch(IllegalAccessException e) {
			throw new StepConfigException(e);
		} catch(IntrospectionException e) {
			throw new StepConfigException(e);
		} catch(InvocationTargetException e) {
			throw new StepConfigException(e);
		} catch(InstantiationException e) {
			throw new StepConfigException(e);
		} catch(ParseException e) {
			throw new StepConfigException(e);
		}
	}

	protected void addArg(Map<String, ?> m) throws ConvertException, IllegalAccessException, java.beans.IntrospectionException, java.lang.reflect.InvocationTargetException, InstantiationException, ParseException {
		Arg v = new Arg();
		ReflectionUtils.populateProperties(v, m);
		args.add(v);
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessageKey() {
		return messageKey;
	}

	public void setMessageKey(String messageKey) {
		this.messageKey = messageKey;
	}
}
