/*
 * SetStep.java
 *
 * Created on February 6, 2004, 5:49 PM
 */

package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import javax.servlet.ServletException;

import simple.bean.ReflectionUtils;
import simple.results.Results;
import simple.results.Results.Aggregate;

/**
 * Takes a results object and sets an attribute in the form with the aggregate value of one column of the Results object
 * @author  Brian S. Krug
 */
public class TotalStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();

    /** Holds value of property scope. */
    private String scope;

    /** Holds value of property result. */
    private String result;

    /** Holds value of property prefix. */
    private String bean;

    private String column;

    private Aggregate aggregate;

    /** Creates a new instance of SetStep */
    public TotalStep() {
    }

    public void perform(simple.servlet.Dispatcher dispatcher, simple.servlet.InputForm form, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException {
    	Object v;
        try {
			v = ReflectionUtils.getProperty(form, getBean());
		} catch (IntrospectionException e) {
			throw new ServletException(e);
		} catch (IllegalAccessException e) {
			throw new ServletException(e);
		} catch (InvocationTargetException e) {
			throw new ServletException(e);
		} catch(ParseException e) {
			throw new ServletException(e);
		}
        if(v instanceof Results) {
        	Results results = (Results)v;
        	int colIndex;
        	try {
        		colIndex = results.getColumnIndex(column);
        	} catch(IllegalArgumentException e) {
        		try {
        			colIndex = Integer.parseInt(column);
        		} catch(NumberFormatException e1) {
        			throw new ServletException("Column '" + column + "' does not exist is the specified results object nor could it be parsed to a number");
        		}
        	}
        	Aggregate[] aggs = results.getAggregatesTracked(colIndex);
        	if(aggs == null)
        		results.trackAggregate(colIndex, getAggregate());
        	else {
        		Aggregate[] newAggs = new Aggregate[aggs.length + 1];
        		System.arraycopy(aggs, 0, newAggs, 0, aggs.length);
        		newAggs[aggs.length] = getAggregate();
        		results.trackAggregate(colIndex, newAggs);
        	}
        	Object total = results.getAggregate(colIndex, 0, getAggregate());
        	if(log.isDebugEnabled()) log.debug("Setting attribute " + getResult() + " to " + getAggregate().toString() + ", '" + total + "'");
        	form.setAttribute(getResult(), total, (getScope() == null ? simple.servlet.RequestUtils.REQUEST_SCOPE : getScope()));
        } else
        	throw new ServletException("Attribute '" + getBean() + "' is " + (v == null ? " null" : " a " + v.getClass().getName() + " object") + " which is not supported. Must be a simple.results.Results object");
    }

    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }

    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * @return Returns the result.
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result The result to set.
     */
    public void setResult(String result) {
        this.result = result;
    }

	public String getBean() {
		return bean;
	}
	public void setBean(String bean) {
		this.bean = bean;
	}

	public Aggregate getAggregate() {
		return aggregate;
	}

	public void setAggregate(Aggregate aggregate) {
		this.aggregate = aggregate;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}
}
