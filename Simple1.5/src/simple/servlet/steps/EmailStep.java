package simple.servlet.steps;

import java.text.Format;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.ConfigUtils;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.mail.Email;
import simple.mail.Emailer;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.StepConfigException;
import simple.util.FilterMap;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

public class EmailStep extends AbstractStep {
	private static final Log log = Log.getLog();
	protected String from;
	protected Format subject;
	protected Format body;
	protected Map<String, Object> emailerProperties;
	protected Cache<Properties, Emailer, RuntimeException> emailerCache = new LockSegmentCache<Properties, Emailer, RuntimeException>(8, 0.75f, 8) {
		@Override
		protected Emailer createValue(Properties key, Object... additionalInfo) throws RuntimeException {
			return new Emailer(key);
		}
	};
	@Override
	public void configure(Map<String, ?> props) throws StepConfigException {
		emailerProperties = new FilterMap<Object>(props, "emailer.", null);
		super.configure(props);
		if(getBody() == null)
			setBody(ConvertUtils.getFormat(ConvertUtils.getStringSafely(props.get(""))));
	}
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String to = form.getString("emailTo", true);
		String cc = form.getString("emailCc", false);
		String subjectText, bodyText;
		try {
			subjectText = ConvertUtils.formatObject(form, getSubject(), false);
			bodyText = ConvertUtils.formatObject(form, getBody(), false);
		} catch(IllegalArgumentException e) {
			throw new ServletException(e);
		}
		String fromText = ConfigUtils.interpolate(getFrom(), form);
		Properties properties = new Properties();
		for(Map.Entry<String, Object> entry : emailerProperties.entrySet()) {
			String value = ConvertUtils.getStringSafely(entry.getValue());
			if(value != null && value.trim().length() > 0)
				properties.put(entry.getKey(), ConvertUtils.formatObject(form, value, null, false));
		}
		Emailer emailer = emailerCache.getOrCreate(properties);
		try {
			Email email = emailer.createEmail();
			email.setTo(to);
			email.setCc(cc);
			email.setFrom(fromText);
			email.setSubject(subjectText);
			email.setBody(bodyText);
			email.send();
		} catch(MessagingException e) {
			throw new ServletException(e);
		} 
		log.info("Sent '" + subjectText + "' email to " + to);
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public Format getSubject() {
		return subject;
	}
	public void setSubject(Format subject) {
		this.subject = subject;
	}
	public Format getBody() {
		return body;
	}
	public void setBody(Format body) {
		this.body = body;
	}
}
