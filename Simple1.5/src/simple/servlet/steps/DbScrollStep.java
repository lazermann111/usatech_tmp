/*
 * DbScrollStep.java
 *
 * Created on February 4, 2004, 2:52 PM
 */

package simple.servlet.steps;

import javax.servlet.ServletException;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

/**
 *
 * @author  Brian S. Krug
 */
public class DbScrollStep extends DbStep {
    
    /** Holds value of property length. */
    private int length = 15;
    
    /** Creates a new instance of DbScrollStep */
    public DbScrollStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        super.perform(dispatcher, form, request, response);
        //calc - startIndex, prevIndex, nextIndex, lastIndex
        int startIndex = form.getInt("startIndex", false, 0);
        int prevIndex = Math.max(startIndex - length, 0);
        int nextIndex = startIndex + length; // problem: if this is more than lastIndex
        int lastIndex = -length;
        form.setAttribute("prevIndex", new Integer(prevIndex));
        form.setAttribute("nextIndex", new Integer(nextIndex));
        form.setAttribute("lastIndex", new Integer(lastIndex));
        form.setAttribute("length", new Integer(length));
    }
    
    /** Getter for property length.
     * @return Value of property length.
     *
     */
    public int getLength() {
        return this.length;
    }
    
    /** Setter for property length.
     * @param length New value of property length.
     *
     */
    public void setLength(int length) {
        if(length < 1) length = 15;
        this.length = length;
    }
    
}
