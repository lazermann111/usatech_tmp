package simple.servlet.steps;

import java.io.IOException;
import java.text.Format;

import javax.servlet.ServletException;

import simple.bean.ConvertUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

public class RedirectStep extends AbstractStep {
    private String value;
    private String attr;
    private Format format;
    private boolean sessionTokenAdded = true;
    
    public RedirectStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
    	String a = getAttr();
    	Format f = getFormat();
    	String redirect;
    	if(f != null) {
    		if(a != null && a.trim().length() > 0)
    			redirect = ConvertUtils.formatObject(form.getAttribute(a), getFormat());
    		else
    			redirect = ConvertUtils.formatObject(form, getFormat());
    	} else if(a != null && a.trim().length() > 0)
	    	redirect = ConvertUtils.getStringSafely(form.getAttribute(a), null);
    	else
    		redirect = getValue();
        if (redirect == null || redirect.trim().length() == 0) 
        	redirect = ".";
        if(isSessionTokenAdded()) {
        	String sessionToken = RequestUtils.getSessionTokenIfPresent(request);
        	if(sessionToken != null) {
        		StringBuilder sb = new StringBuilder(redirect);
        		if(redirect.indexOf('?') >= 0)
        			sb.append('&');
        		else
        			sb.append('?');
        		sb.append("session-token=");
        		sb.append(StringUtils.prepareURLPart(sessionToken));
        		redirect = sb.toString();
        	}
        }
        try {
			response.sendRedirect(redirect);
		} catch(IOException e) {
			throw new ServletException(e);
		}	
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAttr() {
		return attr;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}

	public Format getFormat() {
		return format;
	}

	public void setFormat(Format format) {
		this.format = format;
	}

	public boolean isSessionTokenAdded() {
		return sessionTokenAdded;
	}

	public void setSessionTokenAdded(boolean sessionTokenAdded) {
		this.sessionTokenAdded = sessionTokenAdded;
	}
}
