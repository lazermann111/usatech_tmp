/*
 * ValidateException.java
 *
 * Created on December 23, 2003, 11:06 AM
 */

package simple.servlet.steps;

import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import simple.translator.Translator;

/**
 * Indicates that a request did not pass a validation step
 * @author  Brian S. Krug
 */
public class ValidateException extends ServletException {
    private static final long serialVersionUID = -975147601254L;
	protected List<ValidateCheck> checks = new java.util.ArrayList<ValidateCheck>();
    protected List<Object> beanValues = new java.util.ArrayList<Object>();
    protected final Translator translator;
    protected final String messagePrefix;
    protected final String messageSeparator;
    protected final String messageSuffix;
    
    public ValidateException(Translator translator) {
    	this(translator, "The following checks failed:\n\t", "\n", "\n");
    }
    /** Creates a new instance of ValidateException */
    public ValidateException(Translator translator, String messagePrefix, String messageSeparator, String messageSuffix) {
        super();
        this.translator = translator;
        this.messagePrefix = messagePrefix;
        this.messageSeparator = messageSeparator;
        this.messageSuffix = messageSuffix;
    }
    
    public void addCheck(ValidateCheck validation, Object beanValue) {
        checks.add(validation);
        beanValues.add(beanValue);
    }
    
    public void removeCheck(int index) {
        checks.remove(index);
        beanValues.remove(index);        
    }
    
    public Iterator<ValidateCheck> getChecks() {
        return checks.iterator();
    }
    
    public ValidateCheck getCheck(int index) {
        return checks.get(index);
    }
    
    public Object getBeanValue(int index) {
        return beanValues.get(index);
    }

    public String getMessage(int index) {
        return getCheck(index).produceMessage(translator, getBeanValue(index));
    }

    public int getCheckCount() {
        return checks.size();
    }
    
    public String getMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append(messagePrefix);
        for(int i = 0; i < getCheckCount(); i++) {
            if(i > 0)
            	sb.append(messageSeparator);
            sb.append(getMessage(i));
        }
        sb.append(messageSuffix);
        return sb.toString();
    }
}
