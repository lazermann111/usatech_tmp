package simple.servlet.steps;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.servlet.InputForm;

public class Arg {
    
    /** Holds value of property bean. */
    private String bean;
    
    /** Holds value of property scope. */
    private String scope;
    
    /** Holds value of property value. */
    private String value;
    
    /** Holds value of property type. */
    private String type;
    
    /** Getter for property bean.
     * @return Value of property bean.
     *
     */
    public String getBean() {
        return this.bean;
    }
    
    /** Setter for property bean.
     * @param bean New value of property bean.
     *
     */
    public void setBean(String bean) {
        this.bean = bean;
    }
    
    /** Getter for property scope.
     * @return Value of property scope.
     *
     */
    public String getScope() {
        return this.scope;
    }
    
    /** Setter for property scope.
     * @param scope New value of property scope.
     *
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
    
    /** Getter for property value.
     * @return Value of property value.
     *
     */
    public String getValue() {
        return this.value;
    }
    
    /** Setter for property value.
     * @param value New value of property value.
     *
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    /** Getter for property type.
     * @return Value of property type.
     *
     */
    public String getType() {
        return this.type;
    }
    
    /** Setter for property type.
     * @param type New value of property type.
     *
     */
    public void setType(String type) {
        this.type = type;
    }
    
	public Object retrieveValue(InputForm form) throws ClassNotFoundException, ConvertException {
		return convertArg(getType(), (getBean() != null ? form.get(getBean()) : getValue()));
	}

	protected Object convertArg(String type, Object value) throws ClassNotFoundException, ConvertException {
		if(value == null || type == null)
			return value;
		return ConvertUtils.convert(Class.forName(type), value);
	}
}