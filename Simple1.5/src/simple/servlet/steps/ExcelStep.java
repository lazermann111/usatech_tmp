package simple.servlet.steps;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.bean.ReflectionUtils.BeanProperty;
import simple.io.HeapBufferStream;
import simple.io.PassThroughOutputStream;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.StepConfigException;
import simple.text.StringUtils;

public class ExcelStep extends AbstractStep {
    private static final simple.io.Log log = simple.io.Log.getLog();
    protected static final char[] RANGE_DELIMS = "-:".toCharArray();
    protected static final char[] STYLE_DELIMS = ";\n\r".toCharArray();
    /** Holds value of property fromPage. */
    private String fromPage;
    
    /** Holds value of property toFile. */
    private String toFile;
    
    /** Holds value of property toAttr. */
    private String toAttr;
    
    /** Holds value of property writePage. */
    private boolean writePage = true;
    
    /** Holds value of property fromFile. */
    private String fromFile;
    
    /** Holds value of property fromAttr. */
    private String fromAttr;
    
    /** Holds value of property fromScope. */
    private String fromScope;
    
    /** Holds value of property toScope. */
    private String toScope;
    
    /**
     * Holds value of property downloadName.
     */
    private String downloadName;
    
    /**
     * Holds value of property fromResults.
     */
    private String fromResults;
    
    private Map<String,Style> styleMap = new HashMap<String,Style>();
	private Map<Style, CellStyle> cellStyleCache = new HashMap<Style, CellStyle>();
	private boolean oldExcelFormat = false;

    /**
     * Represents one sheet on an Excel workbook. Holds the information to dynamically build the sheet.
     */
	protected class SheetConfig {
        private Value[] headerValues;
        private Value[] contentValues;
        private Value[] footerValues;
        private Range[] headerRanges;
        private Range[] contentRanges;
        private Range[] footerRanges;
        
        /**
         * Holds value of property name.
         */
        private String name;
        
        /**
         * Holds value of property headerPage.
         */
        private String headerPage;
        
        /**
         * Holds value of property headerAttr.
         */
        private String headerAttr;
        
        /**
         * Holds value of property contentPage.
         */
        private String contentPage;
        
        /**
         * Holds value of property contentAttr.
         */
        private String contentAttr;
        
        /**
         * Holds value of property contentResults.
         */
        private String contentResults;
        
        /**
         * Holds value of property footerPage.
         */
        private String footerPage;
        
        /**
         * Holds value of property footerAttr.
         */
        private String footerAttr;
        
        /**
         * Getter for property name.
         * @return Value of property name.
         */
        public String getName() {
            return this.name;
        }
        
        /**
         * Setter for property name.
         * @param name New value of property name.
         */
        public void setName(String name) {
            this.name = name;
        }
        
        /**
         * Getter for property headerPage.
         * @return Value of property headerPage.
         */
        public String getHeaderPage() {
            return this.headerPage;
        }
        
        /**
         * Setter for property headerPage.
         * @param headerPage New value of property headerPage.
         */
        public void setHeaderPage(String headerPage) {
            this.headerPage = headerPage;
        }
        
        /**
         * Getter for property headerAttr.
         * @return Value of property headerAttr.
         */
        public String getHeaderAttr() {
            return this.headerAttr;
        }
        
        /**
         * Setter for property headerAttr.
         * @param headerAttr New value of property headerAttr.
         */
        public void setHeaderAttr(String headerAttr) {
            this.headerAttr = headerAttr;
        }
        
        /**
         * Getter for property contentPage.
         * @return Value of property contentPage.
         */
        public String getContentPage() {
            return this.contentPage;
        }
        
        /**
         * Setter for property contentPage.
         * @param contentPage New value of property contentPage.
         */
        public void setContentPage(String contentPage) {
            this.contentPage = contentPage;
        }
        
        /**
         * Getter for property contentAttr.
         * @return Value of property contentAttr.
         */
        public String getContentAttr() {
            return this.contentAttr;
        }
        
        /**
         * Setter for property contentAttr.
         * @param contentAttr New value of property contentAttr.
         */
        public void setContentAttr(String contentAttr) {
            this.contentAttr = contentAttr;
        }
        
        /**
         * Getter for property results.
         * @return Value of property results.
         */
        public String getContentResults() {
            return this.contentResults;
        }
        
        /**
         * Setter for property results.
         * @param results New value of property results.
         */
        public void setContentResults(String contentResults) {
            this.contentResults = contentResults;
        }
        
        /**
         * Getter for property footerPage.
         * @return Value of property footerPage.
         */
        public String getFooterPage() {
            return this.footerPage;
        }
        
        /**
         * Setter for property footerPage.
         * @param footerPage New value of property footerPage.
         */
        public void setFooterPage(String footerPage) {
            this.footerPage = footerPage;
        }
        
        /**
         * Getter for property footerAttr.
         * @return Value of property footerAttr.
         */
        public String getFooterAttr() {
            return this.footerAttr;
        }
        
        /**
         * Setter for property footerAttr.
         * @param footerAttr New value of property footerAttr.
         */
        public void setFooterAttr(String footerAttr) {
            this.footerAttr = footerAttr;
        }
        
    }
    protected class Range {
        protected int startColumn = -1;
        protected int startRow = -1;
        protected int endColumn = -1;
        protected int endRow = -1;  
        protected Style style;
    }
    protected class Style {
		protected Map<String, String> properties;
    }
    protected class Value {
        protected int column = -1;
        protected int row = -1;
        protected Object value;
    }
    
	protected SheetConfig[] sheets;
    
    public ExcelStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        try { 
            // create the Excel Doc
			Workbook book = isOldExcelFormat() ? new HSSFWorkbook() : new XSSFWorkbook();
            for(int i = 0; i < sheets.length; i++) {
				Sheet sheet = book.createSheet();
                log.trace("Creating sheet #" + (i+1) + " (" + sheets[i].getName() + ")");
                if(sheets[i].getName() != null) book.setSheetName(book.getNumberOfSheets()-1, sheets[i].getName());
                processSheetPart(book, sheet, form, dispatcher, sheets[i].headerValues, sheets[i].headerRanges, 
                    null /*sheets[i].headerResults*/, sheets[i].headerPage, sheets[i].headerAttr);
                processSheetPart(book, sheet, form, dispatcher, sheets[i].contentValues, sheets[i].contentRanges, 
                    sheets[i].contentResults, sheets[i].contentPage, sheets[i].contentAttr);
                processSheetPart(book, sheet, form, dispatcher, sheets[i].footerValues, sheets[i].footerRanges, 
                    null /*sheets[i].footerResults*/, sheets[i].footerPage, sheets[i].footerAttr);
            }
        
            //prepare the outputstream
            PassThroughOutputStream out = new PassThroughOutputStream();
            
            // add Response OutputStream
            if(isWritePage()) {
				String extension = (book instanceof XSSFWorkbook ? "xlsx" : "xls");
				response.setContentType("application/" + extension);
                String fileName;
                if(downloadName != null && downloadName.trim().length() > 0) fileName = downloadName;              
                else {
                    fileName = form.getString(SimpleServlet.ATTRIBUTE_SEARCH_PATH, false);
                    if(fileName != null && fileName.startsWith("/")) fileName = fileName.substring(1);
                    fileName = fileName.replace('/', '_');
                }
                log.trace("File Name=" + fileName);
				if(!(fileName = fileName.trim()).toLowerCase().endsWith("." + extension))
					fileName = fileName + "." + extension;
                log.trace("Now File Name=" + fileName);
                response.setHeader("Content-Disposition","attachment;filename=" + fileName);
				// out.addWriter(response.getWriter());
				out.addOutputStream(response.getOutputStream());
                // NOTE: May need to write to a ByteArrayOutputStream as a buffer to avoid problems with IE rendering PDF file
            }
            
            //add FileOutputStream to save to file
            if(getToFile() != null) {
                out.addOutputStream(new java.io.BufferedOutputStream(new java.io.FileOutputStream(getToFile())));
            }
            
            // add ByteArrayOutputStream to save to attribute
            if(getToAttr() != null) {
                java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
                form.setAttribute(getToAttr(), baos, getToScope());
            }
            
            //write it out
            book.write(out);          
            out.flush();
        } catch (Exception ex) {
            throw new ServletException(ex);
        } finally {
            cellStyleCache.clear();
            //out.close();
        }
    }    
           
	public void configure(Map<String, ?> props) throws StepConfigException {
        super.configure(props);
        try {
            configureStyles(props);
            Object tmp = props.get("sheets");
			List<SheetConfig> sheetList = new LinkedList<SheetConfig>();
            if(tmp != null) {
				List<?> list = ConvertUtils.convert(List.class, tmp);
				for(Iterator<?> iter = list.iterator(); iter.hasNext();) {
					sheetList.add(createSheet((Map<String, ?>) iter.next()));
                }
            } else {
                sheetList.add(createSheet(props));
            }
			sheets = sheetList.toArray(new SheetConfig[sheetList.size()]);
        } catch(ConvertException e) {
            throw new StepConfigException(e);
        } catch (IntrospectionException e) {
            throw new StepConfigException(e);
        } catch (IllegalAccessException e) {
            throw new StepConfigException(e);
        } catch (InvocationTargetException e) {
            throw new StepConfigException(e);
        } catch (InstantiationException e) {
            throw new StepConfigException(e);
        } catch(ParseException e) {
        	throw new StepConfigException(e);
		}
    }
        
	protected void configureStyles(Map<String, ?> props) throws ConvertException {
        Object tmp = props.get("styles");
        if(tmp != null) {
			List<?> list = ConvertUtils.convert(List.class, tmp);
			for(Iterator<?> iter = list.iterator(); iter.hasNext();) {
				addStyle((Map<String, ?>) iter.next());
            }
        }
    }
    
	protected void configurePart(Map<String, ?> props, List<Value> valueList, List<Range> rangeList) throws ConvertException {
        configureStyles(props);
        addRows(props, valueList, rangeList);
        Object tmp = props.get("ranges");
        if(tmp != null) {
			List<?> list = ConvertUtils.convert(List.class, tmp);
			for(Iterator<?> iter = list.iterator(); iter.hasNext();) {
				addRange((Map<String, ?>) iter.next(), rangeList);
            }
        }
    }
  
	protected SheetConfig createSheet(Map<String, ?> props) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
		SheetConfig sheet = new SheetConfig();
        ReflectionUtils.populateProperties(sheet, props);       
        Object tmp = props.get("headers");
        if(tmp != null) {
			List<?> list = ConvertUtils.convert(List.class, tmp);
            List<Range> rangeList = new LinkedList<Range>();
            List<Value> valueList = new LinkedList<Value>();
			for(Iterator<?> iter = list.iterator(); iter.hasNext();) {
				configurePart((Map<String, ?>) iter.next(), valueList, rangeList);
            }
            sheet.headerValues = valueList.toArray(new Value[valueList.size()]);
            sheet.headerRanges = rangeList.toArray(new Range[rangeList.size()]);
        }
        tmp = props.get("contents");
        if(tmp != null) {
			List<?> list = ConvertUtils.convert(List.class, tmp);
            List<Range> rangeList = new LinkedList<Range>();
            List<Value> valueList = new LinkedList<Value>();
			for(Iterator<?> iter = list.iterator(); iter.hasNext();) {
				configurePart((Map<String, ?>) iter.next(), valueList, rangeList);
            }
            sheet.contentValues = valueList.toArray(new Value[valueList.size()]);
            sheet.contentRanges = rangeList.toArray(new Range[rangeList.size()]);
        }
        tmp = props.get("footers");
        if(tmp != null) {
			List<?> list = ConvertUtils.convert(List.class, tmp);
            List<Range> rangeList = new LinkedList<Range>();
            List<Value> valueList = new LinkedList<Value>();
			for(Iterator<?> iter = list.iterator(); iter.hasNext();) {
				configurePart((Map<String, ?>) iter.next(), valueList, rangeList);
            }
            sheet.footerValues = valueList.toArray(new Value[valueList.size()]);
            sheet.footerRanges = rangeList.toArray(new Range[rangeList.size()]);
        }
        return sheet;
   }

	protected void processSheetPart(Workbook book, Sheet sheet, InputForm form, Dispatcher dispatcher, Value[] values, Range[] ranges, String results, String page, String attr) throws ConvertException, IntrospectionException, ServletException, IOException, SAXException, ParserConfigurationException {
        // get the input source
        int offset = sheet.getLastRowNum();
        InputSource src = null;
        Results res = null;
        if(values != null) {
            processValues(sheet, values, offset);
		}
		if(results != null) { // use results
            res = (Results) form.get(results);
            if(res == null) throw new ServletException("The value of the request attribute, '" + results + "' is null.");
            processResults(book, sheet, res);
		}
		if(page != null) { // use <row> <cell> tags
            HeapBufferStream qs = new HeapBufferStream(0, 512);
            dispatcher.intercept(page, qs.getOutputStream());
            src = new InputSource(qs.getInputStream());
            processStream(book, sheet, src);
        	/*} else if(file != null) {
			    src = new InputSource(new java.io.FileInputStream(file)); // should we get this as relative to the web page or in the classpath?
			*/}
		if(attr != null) {
            Object in = form.getAttribute(attr, getFromScope());
            if(in == null) throw new ServletException("No data found in attribute '" + attr + "' of the form");
            if(in instanceof InputSource) src = (InputSource)in;
            else if(in instanceof java.io.InputStream) src = new InputSource((java.io.InputStream)in);
            else if(in instanceof java.io.Reader) src = new InputSource((java.io.Reader)in);
            else if(in instanceof java.io.ByteArrayOutputStream) src = new InputSource(new java.io.ByteArrayInputStream(((java.io.ByteArrayOutputStream)in).toByteArray()));
            else src = new InputSource(new java.io.StringReader(in.toString()));
            processStream(book, sheet, src);
        } else log.info("No input source was specified for part of the ExcelStep; please set either 'results', page', 'file', or 'attr'");
        if(ranges != null) processRanges(book, sheet, ranges, offset);
   }
    
	protected void addRows(Map<String, ?> props, List<Value> valueList, List<Range> rangeList) throws ConvertException {
        Style style;
        Range range;
        int row = 0;
        int maxcol = 0;
        log.debug("Add Rows for " + props);
        Object tmp = props.get("rows");
		List<?> rowList = ConvertUtils.convert(List.class, tmp);
		if(rowList != null)
			for(Iterator<?> iter = rowList.iterator(); iter.hasNext();) {
				Map<String, ?> rowProps = (Map<String, ?>) iter.next();
            int offset = parseRangeInt(rowProps.get("offset"));
            if(offset < 1) row = row + 1;
            else row = row + 1 + offset;
            style = createStyle(rowProps);
            if(style != null) {
                range = new Range();
                range.startRow = row;
                range.endRow = range.startRow;
                range.style = style;
                rangeList.add(range);
            }
            int col = 0;
            log.debug("Add Cells for " + rowProps);
            tmp = rowProps.get("cells");
				List<?> colList = ConvertUtils.convert(List.class, tmp);
				if(colList != null)
					for(Iterator<?> colIter = colList.iterator(); colIter.hasNext();) {
						Map<String, ?> colProps = (Map<String, ?>) colIter.next();
                int colOffset = parseRangeInt(colProps.get("offset"));
                if(colOffset < 1) col = col + 1;
                else col = col + 1 + offset;
                style = createStyle(colProps);
                if(style != null) {
                    range = new Range();
                    range.startColumn = col;
                    range.startRow = row;
                    range.endColumn = range.startColumn;
                    range.endRow = range.startRow;
                    range.style = style;
                    rangeList.add(range);
                }
                Value value = new Value();
                value.column = col;
                value.row = row;
                value.value = colProps.get("");
                log.debug("Add Value '" + value.value + "' from " + colProps);
                valueList.add(value);
            }
            if(col > maxcol) maxcol = col;
        }
        style = createStyle(props);
        if(style != null) {
            range = new Range();
            range.startColumn = 1;
            range.startRow = 1;
            range.endRow = row;
            range.endColumn = maxcol;
            range.style = style;
            rangeList.add(range);            
        }
    }
    
	protected void addStyle(Map<String, ?> props) {
        Style style = new Style();
        String id = (String)props.get("id");
        String text = (String)props.get("");
		style.properties = new HashMap<String, String>();
        parseStyle(text,style.properties);
        styleMap.put(id, style);
    }
    
	protected void parseStyle(String text, Map<String, String> styleProps) {
        String[] attrs = StringUtils.split(text, STYLE_DELIMS, StringUtils.STANDARD_QUOTES);
        for(int i = 0; i < attrs.length; i++) {
            if(attrs[i].trim().length() == 0) continue;
            int p = attrs[i].indexOf(':');
            if(p < 0) continue;
            styleProps.put(attrs[i].substring(0, p).trim(), attrs[i].substring(p+1).trim());
        }
    }
    
	protected Style createStyle(Map<String, ?> props) {
        String styleAttrs = (String)props.get("style");
        String styleClass = (String)props.get("styleClass");
        Style style = null;
        if(styleClass != null) style = styleMap.get(styleClass);
        if(styleAttrs != null) {
            if(style == null) {
                style = new Style();
				style.properties = new java.util.HashMap<String, String>();
            } else {
				style.properties = new java.util.HashMap<String, String>(style.properties);
            }
            parseStyle(styleAttrs, style.properties);
        }  
        return style;
    }
    
	protected void addRange(Map<String, ?> props, List<Range> list) throws NumberFormatException {
        Style style = createStyle(props);
        String s = (String)props.get("section");
        Range r;
        if(s == null) {
            r = new Range();
			r.startColumn = parseRangeInt(props.get("startColumn"));
			r.endColumn = parseRangeInt(props.get("endColumn"));
			r.startRow = parseRangeInt(props.get("startRow"));
			r.endRow = parseRangeInt(props.get("endRow"));
            r.style = style;
            list.add(r);
        } else {
            String[] ss = StringUtils.split(s, StringUtils.STANDARD_DELIMS, false);
            for(int i = 0; i < ss.length; i++) {
                //check for '-'
                String[] beginEnd = StringUtils.split(ss[i], RANGE_DELIMS, false);
                switch(beginEnd.length) {
                    case 0: // do nothing
                        break;
                    case 1:
                        r = new Range();
                        int[] cr = parseColumnRow(beginEnd[0]);
                        r.startColumn = cr[0];
                        r.startRow = cr[1];
                        r.endColumn = cr[0];
                        r.endRow = cr[1];
                        r.style = style;
                        list.add(r);
                        break;
                    case 2:
                        r = new Range();
                        int[] crBegin = parseColumnRow(beginEnd[0]);
                        int[] crEnd = parseColumnRow(beginEnd[1]);
                        r.startColumn = crBegin[0];
                        r.startRow = crBegin[1];
                        r.endColumn = crEnd[0];
                        r.endRow = crEnd[1];
                        r.style = style;
                        list.add(r);
                       break;                     
                    default:
                        throw new NumberFormatException("Too many Range Delimiters in '" + ss[i] + "'");
                }                
            }
        }
    }
    
    protected int parseRangeInt(Object val) {
        if(val == null) return -1;
        try {
            return ConvertUtils.convert(Integer.class, val);
        } catch(ConvertException ce) {
            //parse for lettered value (i.e. - "A" = 1, "B" = 2, etc)
            String s = val.toString().toUpperCase();
            int ret = 0;
            for(int i = 0; i < s.length(); i++) {
                int v = 1 + s.charAt(i) - 'A';
                if(v < 1 || v > 26) return -1;
                ret = (ret * 26) + v;
            }
            return ret;
        }
    }
    
    protected int[] parseColumnRow(String s) throws NumberFormatException {
        s = s.trim().toUpperCase();
        int[] cr = new int[] {-1, -1};
        for(int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if(Character.isLetter(ch)) {
                int v = 1 + ch - 'A';
                if(cr[0] == -1) cr[0] = v;
                else cr[0] = (cr[0] * 26) + v;
            } else {
                cr[1] = Integer.parseInt(s.substring(i));
                break;
            }
        }
        return cr;
    }
    
	protected void processResults(Workbook book, Sheet sheet, Results results) throws ConvertException {
		int n = sheet.getLastRowNum();
		if(sheet.getRow(n) != null)
			n++;
		Row row = sheet.createRow(n++);
        for(int i = 1; i <= results.getColumnCount(); i++) { //results are 1-based
			Cell cell = row.createCell((short) (i - 1));
            cell.setCellValue(results.getColumnName(i));
        }
        while(results.next()) {
			row = sheet.createRow(n++);
            for(int i = 1; i <= results.getColumnCount(); i++) { //results are 1-based
				Cell cell = row.createCell((short) (i - 1));
                Object value = results.getValue(i);
                if(value instanceof Boolean) cell.setCellValue(((Boolean)value).booleanValue());
                else if(value instanceof java.util.Calendar) cell.setCellValue((java.util.Calendar)value);
                else if(value instanceof java.util.Date) cell.setCellValue((java.util.Date)value);
                else if(value instanceof Number) cell.setCellValue(((Number)value).doubleValue());
                else if(value != null) cell.setCellValue(ConvertUtils.convert(String.class, value));
            }        
        }
    }
    
	protected void processRanges(Workbook book, Sheet sheet, Range[] ranges, int offset) throws IntrospectionException {
        for(int i = 0; i < ranges.length; i++) {
            log.trace("Processing Range (Rows= " + ranges[i].startRow + " to " + ranges[i].endRow + ", Cols= " + ranges[i].startColumn + " to " + ranges[i].endColumn + ")");
            for(int r =  (ranges[i].startRow < 1 ? 0 : ranges[i].startRow - 1) + offset; r < (ranges[i].endRow  < 1 ? sheet.getLastRowNum() + 1 : ranges[i].endRow) + offset; r++) {
				Row row = sheet.getRow(r);
                if(row == null) row = sheet.createRow(r);
                for(int c =  (ranges[i].startColumn  < 1 ? 0 : ranges[i].startColumn - 1); c < (ranges[i].endColumn  < 1 ? row.getLastCellNum() + 1 : ranges[i].endColumn); c++) {
					Cell cell = row.getCell((short) c);
                    if(cell == null) cell = row.createCell((short)c);
					CellStyle cellStyle = cellStyleCache.get(ranges[i].style);
                    if(cellStyle == null) {
                        cellStyle = book.createCellStyle();
						applyStyle(cellStyle, book, ranges[i].style.properties);
                        cellStyleCache.put(ranges[i].style, cellStyle);
                    }
                    if(log.isTraceEnabled()) log.trace("Setting cell (" + r + ", " + c + ") style to " + cellStyle);
                    cell.setCellStyle(cellStyle);
                 }
            }
       }
    }
    
	protected void applyStyle(CellStyle cellStyle, Workbook book, Map<String, String> properties) throws IntrospectionException {
		Font font = null;
		for(Map.Entry<String, String> entry : properties.entrySet()) {
			if(entry.getValue() == null)
				continue;
			BeanProperty bp;
			Object bean;
			if(entry.getKey().startsWith("font-")) {
				if(font == null) {
					font = book.createFont();
					cellStyle.setFont(font);
				}
				bean = font;
				bp = ReflectionUtils.findBeanProperty(font.getClass(), entry.getKey());
			} else {
				bean = cellStyle;
				bp = ReflectionUtils.findBeanProperty(cellStyle.getClass(), entry.getKey());
			}
			if(bp == null)
				log.info("Could not find property '" + entry.getKey() + "' on " + bean.getClass().getName());
			else {
				Object value = entry.getValue();
				if((short.class.isAssignableFrom(bp.getType()) || Short.class.isAssignableFrom(bp.getType())) && entry.getValue() != null) {
					if(entry.getKey().equals("dataFormat"))
						value = book.createDataFormat().getFormat(entry.getValue());
					else if(entry.getKey().endsWith("Color")) {
						Short val = ConvertUtils.convertSafely(Short.class, entry.getValue(), null);
						if(val == null) {
							IndexedColors icolor = IndexedColors.valueOf(entry.getValue().trim().toUpperCase().replace(' ', '_'));
							if(icolor != null)
								value = icolor.getIndex();
						} else
							value = val;
					}
				}
				try {
					value = ConvertUtils.convert(bp.getType(), value);
					bp.setValue(bean, value);
				} catch(ConvertException e) {
					log.info("Could not find convert value '" + entry.getValue() + "' to " + bp.getType().getName() + " for '" + entry.getKey() + "'", e);
				} catch(IllegalArgumentException e) {
					log.info("Could not set value '" + entry.getValue() + "' on " + bean.getClass().getName() + " for '" + entry.getKey() + "'", e);
				} catch(IllegalAccessException e) {
					log.info("Could not set value '" + entry.getValue() + "' on " + bean.getClass().getName() + " for '" + entry.getKey() + "'", e);
				} catch(InvocationTargetException e) {
					log.info("Could not set value '" + entry.getValue() + "' on " + bean.getClass().getName() + " for '" + entry.getKey() + "'", e);
				}
			}
		}
	}

	protected void processStream(Workbook book, Sheet sheet, InputSource src) throws ConvertException, IOException, SAXException, ParserConfigurationException, IntrospectionException {
        simple.xml.MapListXMLLoader loader = new simple.xml.MapListXMLLoader();
		Map<String, ?> map = loader.load(src.getByteStream());
        log.trace("Stream = " + map);
        List<Value> valueList = new LinkedList<Value>();
        List<Range> rangeList = new LinkedList<Range>();
        configurePart(map, valueList, rangeList);
        log.debug("Found " + valueList.size() + " cells and " + rangeList.size() + " styles to apply");
        int offset = sheet.getLastRowNum();
        processValues(sheet, valueList.toArray(new Value[valueList.size()]), offset);
        processRanges(book, sheet, rangeList.toArray(new Range[rangeList.size()]), offset);
    }
        
	protected void processValues(Sheet sheet, Value[] values, int offset) throws ConvertException {
        offset--;
        for(int i = 0; i < values.length; i++) {
            log.trace("Adding value '" + values[i].value + "' to cell (" + (values[i].row + offset) + ", " + (values[i].column-1) + ")");
			Row row = sheet.getRow(values[i].row + offset);
            if(row == null) row = sheet.createRow(values[i].row + offset);
			Cell cell = row.getCell((short) (values[i].column - 1));
            if(cell == null) cell = row.createCell((short)(values[i].column - 1));
            if(values[i].value instanceof Boolean) cell.setCellValue(((Boolean)values[i].value).booleanValue());
            else if(values[i].value instanceof java.util.Calendar) cell.setCellValue((java.util.Calendar)values[i].value);
            else if(values[i].value instanceof java.util.Date) cell.setCellValue((java.util.Date)values[i].value);
            //else if(values[i].value instanceof Double) cell.setCellValue(((Double)values[i].value).doubleValue());
            else if(values[i].value != null) cell.setCellValue(ConvertUtils.convert(String.class, values[i].value));
        }
    }
    
    /** Getter for property toFile.
     * @return Value of property toFile.
     *
     */
    public String getToFile() {
        return this.toFile;
    }
    
    /** Setter for property toFile.
     * @param toFile New value of property toFile.
     *
     */
    public void setToFile(String toFile) {
        this.toFile = toFile;
    }
    
    /** Getter for property toAttr.
     * @return Value of property toAttr.
     *
     */
    public String getToAttr() {
        return this.toAttr;
    }
    
    /** Setter for property toAttr.
     * @param saveToAttr New value of property toAttr.
     *
     */
    public void setToAttr(String toAttr) {
        this.toAttr = toAttr;
    }
    
    /** Getter for property writePage.
     * @return Value of property writePage.
     *
     */
    public boolean isWritePage() {
        return this.writePage;
    }
    
    /** Setter for property writePage.
     * @param writePage New value of property writePage.
     *
     */
    public void setWritePage(boolean writePage) {
        this.writePage = writePage;
    }
    
    /** Getter for property fromPage.
     * @return Value of property fromPage.
     *
     */
    public String getFromPage() {
        return this.fromPage;
    }
    
    /** Setter for property fromPage.
     * @param page New value of property fromPage.
     *
     */
    public void setFromPage(String fromPage) {
        this.fromPage = fromPage;
    }
 
    /** Getter for property fromFile.
     * @return Value of property fromFile.
     *
     */
    public String getFromFile() {
        return this.fromFile;
    }
    
    /** Setter for property fromFile.
     * @param fromFile New value of property fromFile.
     *
     */
    public void setFromFile(String fromFile) {
        this.fromFile = fromFile;
    }
    
    /** Getter for property fromAttr.
     * @return Value of property fromAttr.
     *
     */
    public String getFromAttr() {
        return this.fromAttr;
    }
    
    /** Setter for property fromAttr.
     * @param fromAttr New value of property fromAttr.
     *
     */
    public void setFromAttr(String fromAttr) {
        this.fromAttr = fromAttr;
    }
    
    /** Getter for property fromScope.
     * @return Value of property fromScope.
     *
     */
    public String getFromScope() {
        return this.fromScope;
    }
    
    /** Setter for property fromScope.
     * @param fromScope New value of property fromScope.
     *
     */
    public void setFromScope(String fromScope) {
        this.fromScope = fromScope;
    }
    
    /** Getter for property toScope.
     * @return Value of property toScope.
     *
     */
    public String getToScope() {
        return this.toScope;
    }
    
    /** Setter for property toScope.
     * @param toScope New value of property toScope.
     *
     */
    public void setToScope(String toScope) {
        this.toScope = toScope;
    }
    
    /**
     * Getter for property downloadName.
     * @return Value of property downloadName.
     */
    public String getDownloadName() {
        return this.downloadName;
    }
    
    /**
     * Setter for property downloadName.
     * @param downloadName New value of property downloadName.
     */
    public void setDownloadName(String downloadName) {
        this.downloadName = downloadName;
    }
    
    /**
     * Getter for property fromResults.
     * @return Value of property fromResults.
     */
    public String getFromResults() {
        return this.fromResults;
    }
    
    /**
     * Setter for property fromResults.
     * @param fromResults New value of property fromResults.
     */
    public void setFromResults(String fromResults) {
        this.fromResults = fromResults;
    }

	public boolean isOldExcelFormat() {
		return oldExcelFormat;
	}

	public void setOldExcelFormat(boolean oldExcelFormat) {
		this.oldExcelFormat = oldExcelFormat;
	}
}

