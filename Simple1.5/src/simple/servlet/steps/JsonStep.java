package simple.servlet.steps;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;

public class JsonStep extends AbstractStep {
	// private static final simple.io.Log log = simple.io.Log.getLog();
    /** Holds value of property scope. */
    private String scope;
    /** Holds value of property bean. */
    private String bean;
    
	private String contentType = "application/json";
    
	private Set<String> excluded;
	private Set<String> included;

	protected final JsonBuilder jsonBuilder = new JsonBuilder();

    public JsonStep() {
    }
    
    public void perform(Dispatcher dispatcher, InputForm form, javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) throws ServletException {
        try { 
			response.setContentType(getContentType());
            //write it out
            PrintWriter out = response.getWriter();
			Object value = form.getAttribute(getBean(), getScope());
			jsonBuilder.write(value, out, included, excluded);
            out.flush();
        } catch (IOException e) {
            throw new ServletException(e);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
    }    

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getBean() {
		return bean;
	}

	public void setBean(String bean) {
		this.bean = bean;
	}

	public Set<String> getExcluded() {
		return excluded;
	}

	public void setExcluded(Set<String> excluded) {
		this.excluded = excluded;
	}

	public Set<String> getIncluded() {
		return included;
	}

	public void setIncluded(Set<String> included) {
		this.included = included;
	}
    
}

