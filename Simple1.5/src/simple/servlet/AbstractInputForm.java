/*
 * InputForm.java
 *
 * Created on January 14, 2003, 3:11 PM
 */

package simple.servlet;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Date;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

/** An abstract implementation of the InputForm that handles most of the conversion / convenience methods. 
 * @author Brian S. Krug
 */
public abstract class AbstractInputForm extends simple.bean.AbstractDynaBean implements InputForm {
		private String redirectUri;
		
    protected AbstractInputForm() {
    	
    }
    /** Returns the specified attribute from ANY scope
     * @param key The attribute name
     * @return The attribute value
     */    
    public Object getAttribute(String key) {
        return getAttribute(key, null);
    }

    /** Sets the specified attribute in the request scope
     * @param key The attribute name
     * @param value The new attribute value
     */    
    public void setAttribute(String key, Object value) {
        setAttribute(key, value, "request");
    }
    
    /** Retrieves a BigDecimal from the request's attributes
     *
     * @return The value as a BigDecimal
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public BigDecimal getBigDecimal(String name, boolean required) throws ServletException {
        Object o = getAttribute(name);
        if(o != null) {
            try {
                o = ConvertUtils.convert(BigDecimal.class,o);
                if(o instanceof BigDecimal) return (BigDecimal)o;
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return null;
    }
    
    /** Retrieves a Boolean from the request's attributes
     *
     * @return The value as a Boolean or null if not existent
     * @param name The attribute or parameter name
     * @throws ServletException If an exception occurs
     */
    public Boolean getBooleanObject(String name) throws ServletException {
        Object o = getAttribute(name);
        if(o instanceof String) return new Boolean(asBoolean((String)o));
        else if(o != null) {
            try {
                o = ConvertUtils.convert(Boolean.class,o);
                if(o instanceof Boolean) return (Boolean)o;
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        return null;
    }
 
    /** Retrieves a boolean from the request's attributes
     *
     * @return The value as a boolean
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @param defaultValue The value to return if the value is not found and
     * the required parameter is false
     * @throws ServletException If an exception occurs
     */
    public boolean getBoolean(String name, boolean required, boolean defaultValue) throws ServletException {
        Object o = getAttribute(name);
        if(o instanceof String) return asBoolean((String)o);
        else if(o != null) {
            try {
                o = ConvertUtils.convert(Boolean.class,o);
                if(o instanceof Boolean) return ((Boolean)o).booleanValue();
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return defaultValue;
    }
        
    /** Converts the given String to a boolean
     * @param s The String
     * @return The boolean value of the String
     */        
    protected static boolean asBoolean(String s) {
        try {
            return ConvertUtils.getBoolean(s, false);
        } catch (ConvertException e) {
            return false;
        }    
    }
    
    /** Retrieves a boolean array from the request's attributes
     *
     * @return The value as a boolean array
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public boolean[] getBooleanArray(String name, boolean required) throws ServletException {
        Object o = getAttribute(name);
        if(o instanceof String[]) {
            String[] s = (String[]) o;
            boolean[] r = new boolean[s.length];
            for(int i = 0; i < r.length; i++) r[i] = asBoolean(s[i].trim());
            return r;
        } else if(o != null) {
            try {
                o = ConvertUtils.convert(boolean[].class,o);
                if(o instanceof boolean[]) return (boolean[])o;
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return new boolean[0];
    }
    
    /** Retrieves a Date from the request's attributes
     *
     *
     * @return The value as a Date
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @param df The DateFormat to use to parse the value into a Date
     * @throws ServletException If an exception occurs
     */
    public java.util.Date getDate(String name, boolean required, DateFormat df) throws ServletException {
        Object o = getAttribute(name);
        if(o instanceof String && ((String)o).length() > 0) {
            try {
				return ConvertUtils.convert(Date.class, o);
			} catch (ConvertException e) {
                throw new ServletException(e.getMessage(),e.getCause());
			}   
        } else if(o != null) {
            try {
                o = ConvertUtils.convert(java.util.Date.class,o);
                if(o instanceof java.util.Date || o == null) return (java.util.Date)o;
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return null;
    }
    
    /** Retrieves an int from the request's attributes
     * @return The value as an int
     * @param defaultValue The default value
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public int getInt(String name, boolean required, int defaultValue) throws ServletException {
        Object o = getAttribute(name);
        if(o != null) {
            try {
                o = ConvertUtils.convert(Integer.class,o);
                if(o instanceof Integer) return ((Integer)o).intValue();
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return defaultValue;
    }
    
    /** Retrieves an int array from the request's attributes
     *
     * @return The value as an int array
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public int[] getIntArray(String name, boolean required) throws ServletException {
        Object o = getAttribute(name);
        if(o != null) {
            try {
                o = ConvertUtils.convert(int[].class,o);
                if(o instanceof int[] || o == null) return (int[])o;
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return  new int[0];
    }
    
    /** Retrieves a long from the request's attributes
     * @return The value as a long
     * @param defaultValue The default value
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public long getLong(String name, boolean required, long defaultValue) throws ServletException {
        Object o = getAttribute(name);
        if(o != null) {
            try {
                o = ConvertUtils.convert(Long.class,o);
                if(o instanceof Long) return ((Long)o).longValue();
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return defaultValue;
    }
    
    /** Retrieves a long array from the request's attributes
     *
     * @return The value as a long array
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public long[] getLongArray(String name, boolean required) throws ServletException {
        Object o = getAttribute(name);
        if(o != null) {
            try {
                o = ConvertUtils.convert(long[].class,o);
                if(o instanceof long[]) return (long[])o;
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return  new long[0];
    }
    
    /** Retrieves a String of digits from the request's attributes
     *
     * @return The value as a String with all non-digits stripped out
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public String getNumberString(String name, boolean required) throws ServletException {
        String s = getString(name, required);
        if(s == null || s.length() == 0) return null;
        char c;
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            if(Character.isDigit(c)) sb.append(c);
        }
        if(required && sb.length() == 0) throw new ServletException("Parameter, '" + name + "', is missing");
        return sb.toString();
    }
      
    /** Retrieves a String from the request's attributes
     *
     * @return The value as a String
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public String getString(String name, boolean required) throws ServletException {
        Object o = getAttribute(name);
        if(o != null) {
            try {
                o = ConvertUtils.convert(String.class,o);
                if(o instanceof String) return (String)o;
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return null;
    }
    
    /** Retrieves a String from the request's attributes
    *
    * @return The value as a String
    * @param name The attribute or parameter name
    * @param defaultValue The default value
    */
    public String getStringSafely(String name, String defaultValue) {
    	try {
    		String s = getString(name, false);
    		return s == null ? defaultValue : s;
    	} catch (Exception e) {
    		return defaultValue;
    	}
    }
    
    /** Retrieves a String array from the request's attributes
     *
     * @return The value as a String array
     * @param name The attribute or parameter name
     * @param required Whether to throw an exception if the value is not
     * found in either the attributes or parameters
     * @throws ServletException If an exception occurs
     */
    public String[] getStringArray(String name, boolean required) throws ServletException {
        Object o = getAttribute(name);
        if(o != null) {
            try {
                o = ConvertUtils.convert(String[].class,o);
                if(o instanceof String[]) return (String[])o;
            } catch(ConvertException ce) {
                throw new ServletException(ce.getMessage(),ce.getCause());
            }
        }
        if(required) throw new ServletException("Parameter, '" + name + "', is missing");
        return  new String[0];
    } 
        
    protected String getDynaClassName() {
        return getClass().getName();
    }
    
    protected Object getDynaProperty(String name) {
        if(name.equals("request")) return getRequest();
        if(name.startsWith("header:")) return getHeader(name.substring(7));
        if(name.startsWith("cookie:")) return getCookie(name.substring(7));
        if(name.startsWith("session:")) return getAttribute(name.substring(8), RequestUtils.SESSION_SCOPE);
        if(name.startsWith("application:")) return getAttribute(name.substring(12), RequestUtils.APPLICATION_SCOPE);
        return getAttribute(name);
    }
    
    protected boolean hasDynaProperty(String name) {
        //return attrs.containsKey(name);
        return true;
    }
    
    protected void setDynaProperty(String name, Object value) {
        setAttribute(name, value);
    }
		public String getRedirectUri() {
			return redirectUri;
		}
		public void setRedirectUri(String redirectUri) {
			this.redirectUri = redirectUri;
		}

}
