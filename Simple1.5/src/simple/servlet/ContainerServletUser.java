/*
 * ContainerServletUser.java
 *
 * Created on December 26, 2003, 12:43 PM
 */

package simple.servlet;

import java.lang.ref.WeakReference;

import javax.servlet.http.HttpServletRequest;

/** A ServletUser used for Servlet container-based authenication.
 *
 * @author  Brian S. Krug
 */
public class ContainerServletUser implements ServletUser {
    private static final long serialVersionUID = -1858497811992914530L;
    /** The value under which the ContainerServletUser is stored in the session */    
    public static final String ATTRIBUTE_CONTAINER_USER = "simple.servlet.ContainerServletUser.User";
    /** Holds the value of the userName property */    
    protected String userName;
    /** Holds a weak reference to the HttpServletRequest */    
    protected transient WeakReference<HttpServletRequest> requestRef;
    /** Creates a new instance of ContainerServletUser */
    public ContainerServletUser() {
    }
    
    /** The name of the user
     * @return The userName property's value
     */    
    public String getUserName() {
        return userName;
    }
    
    /** Whether this user has the specified privilege or not. Calls the <CODE>isUserInRole()</CODE>
     * method of the HttpServletRequest object with the specified privilege as the "role"
     * to determine if the user has the privilege.
     * @param privilege The privilege for which to check
     * @return <CODE>true</CODE> if the user has the specified privilege; <CODE>false</CODE> otherwise
     */    
    public boolean hasPrivilege(String privilege) {
        if(requestRef == null) return false;
        HttpServletRequest req = requestRef.get();
        if(req == null) return false;
        return req.isUserInRole(privilege);
    }
    
    /** Establishes a WeakReference to the provided request so as to avoid
     * holding on to requests after they have been processed. This method must be called
     * for each new request so that the hasPrivilege method works from the
     * correct request.
     * @param request The new request object
     */
    public final void setRequest(HttpServletRequest request) {
        requestRef = new WeakReference<HttpServletRequest>(request);
        String tmp = request.getRemoteUser();
        if(tmp != null) userName = tmp;        
    }
}
