/*
 * NotLoggedOnException.java
 *
 * Created on January 27, 2003, 9:31 AM
 */

package simple.servlet;

import javax.servlet.ServletException;

/** Signifies that a request did not provide the correct session token
 * @author Brian S. Krug
 */
public class InvalidSessionTokenException extends ServletException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 987211254146767L;


	/**
     * Creates new <code>NotLoggedOnException</code> without detail message.
     */
    public InvalidSessionTokenException() {
    }


    /**
     * Constructs an <code>NotLoggedOnException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public InvalidSessionTokenException(String msg) {
        super(msg);
    }
}


