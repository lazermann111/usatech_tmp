package simple.servlet;

import java.util.Locale;

import simple.translator.Translator;

public interface RequestInfo {
	public String getServerName() ;

	public String getContextPath() ;

	public String getPathInfo() ;

	public String getRealPath(String webUri) ;
	
	public Locale getLocale() ;
	
	public String getBaseUrl();

	public Translator getTranslator();

	public String getUserAgent();

	public String addLastModifiedToUri(String path);

}
