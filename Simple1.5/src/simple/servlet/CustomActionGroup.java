/*
 * CustomActionGroup.java
 *
 * Created on January 27, 2003, 8:49 AM
 */

package simple.servlet;

/** Tells the CustomActionServlet it is okay to use the methods on this class. There are no methods
 *  defined in this interface - it is simply a signal.
 *
 * @author  Brian S. Krug
 */
public interface CustomActionGroup {

}
