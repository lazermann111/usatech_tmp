/*
 * BasicServletUser.java
 *
 * Created on December 26, 2003, 12:19 PM
 */

package simple.servlet;

import java.util.Collections;
import java.util.Date;
import java.util.Set;

/** A basic implementation of ServletUser that allows other objects to set it's userName and
 * privileges.
 * @author Brian S. Krug
 */
public class BasicServletUser implements ServletUser {
    private static final long serialVersionUID = -2449637644799192697L;

    protected Set<String> privs = new java.util.HashSet<String>();
    protected Set<String> unmodPrivs = Collections.unmodifiableSet(privs);
    
    /** Holds value of property userName. */
    private String userName;
    
    /** Holds value of property fullName. */
    private String fullName;
    
    /** Holds value of property emailAddress. */
    private String emailAddress;
    
	private Date passwordExpiration;

    /** Creates a new instance of BasicServletUser */
    public BasicServletUser() {
    }
    
    /** Whether this user has the specified privilege or not. NOTE: this implementation is case insensitive.
     * @param privilege The privilege for which to check
     * @return <CODE>true</CODE> if the user has the specified privilege; <CODE>false</CODE> otherwise
     */    
    public boolean hasPrivilege(String privilege) {
        return privs.contains(privilege.toUpperCase());
    }
    
    /** Getter for property userName.
     * @return Value of property userName.
     *
     */
    public String getUserName() {
        return this.userName;
    }
    
    /** Setter for property userName.
     * @param userName New value of property userName.
     *
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    /** Getter for property fullName.
     * @return Value of property fullName.
     *
     */
    public String getFullName() {
        return this.fullName;
    }
    
    /** Setter for property fullName.
     * @param userName New value of property fullName.
     *
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    /** Getter for property emailAddress.
     * @return Value of property emailAddress.
     *
     */
    public String getEmailAddress() {
        return this.emailAddress;
    }
    
    /** Setter for property emailAddress.
     * @param emailAddress New value of property emailAddress.
     *
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    /** Adds the specifed privilege to the list of required privileges.
     * NOTE: Privileges in this implementation are not case-sensitive.
     * @param privilege The privilege to add
     */    
    public void addPrivilege(String privilege) {
        privs.add(privilege.toUpperCase());
    }
    
    /** Removes the specifed privilege from the list of required privileges.
     * NOTE: Privileges in this implementation are not case-sensitive.
     * @param privilege The privilege to remove
     */    
    public void removePrivilege(String privilege) {
        privs.remove(privilege.toUpperCase());
    }
    
    /** Adds the specifed privileges to the list of required privileges.
     * NOTE: Privileges in this implementation are not case-sensitive.
     * @param privileges The privileges to add
     */    
    public void addPrivileges(String[] privileges) {
        for(int i = 0; i < privileges.length; i++) addPrivilege(privileges[i]);
    }
    
    /** Removes the specifed privilege from the list of required privileges.
     * NOTE: Privileges in this implementation are not case-sensitive.
     * @param privileges The privileges to remove
     */    
    public void removePrivileges(String[] privileges) {
        for(int i = 0; i < privileges.length; i++) removePrivilege(privileges[i]);
    }
    
    public Set<String> getPrivileges() {
    	return unmodPrivs;
    }
    
    /** Replaces the list of required privileges with the specifed privileges.
     * NOTE: Privileges in this implementation are not case-sensitive.
     * @param privileges The privileges to add
     */    
    public void setPrivilegeArray(String[] privileges) {
    	privs.clear();
    	if(privileges != null)
	        for(int i = 0; i < privileges.length; i++) 
	        	addPrivilege(privileges[i]);
    }

	public void clearPrivileges() {
		privs.clear();
	}

	public Date getPasswordExpiration() {
		return passwordExpiration;
	}

	public void setPasswordExpiration(Date passwordExpiration) {
		this.passwordExpiration = passwordExpiration;
	}
}
