package simple.servlet.jetty;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.servlet.ErrorPageErrorHandler;

public class SimpleErrorHandler extends ErrorPageErrorHandler {
    protected void writeErrorPageBody(HttpServletRequest request, Writer writer, int code, String message, boolean showStacks) throws IOException {
	    String uri= request.getRequestURI();
	    
	    writeErrorPageMessage(request,writer,code,message,uri);
	    if (showStacks)
	        writeErrorPageStacks(request,writer);
	    for (int i= 0; i < 20; i++)
	        writer.write("<br/>                                                \n");
	}
}
