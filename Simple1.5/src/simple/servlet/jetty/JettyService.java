package simple.servlet.jetty;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.security.Principal;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.server.HttpChannel;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.server.handler.ErrorHandler;
import org.eclipse.jetty.server.session.DefaultSessionIdManager;
import org.eclipse.jetty.util.ssl.SslContextFactory;

import simple.app.Service;
import simple.app.ServiceException;
import simple.app.ServiceStatus;
import simple.app.ServiceStatusInfo;
import simple.app.ServiceStatusListener;
import simple.app.WorkQueue;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.servlet.ServletUser;
import simple.text.StringUtils;
import simple.util.MapBackedSet;
import simple.util.concurrent.ResultFuture;
import simple.util.concurrent.TrivialFuture;

public class JettyService extends Server implements Service {
	private final static Log log = Log.getLog();
	protected final Set<ServiceStatusListener> serviceStatusListeners = new MapBackedSet<ServiceStatusListener>(new ConcurrentHashMap<ServiceStatusListener, Object>()); //new LinkedHashSet<ServiceStatusListener>();
	protected final String serviceName;
	protected final static int STATUS_STOPPED = 0;
	protected final static int STATUS_STARTING = 1;
	protected final static int STATUS_STARTED = 2;
	protected final static int STATUS_STOPPING = 3;
	protected final AtomicInteger status = new AtomicInteger(STATUS_STOPPED);
	protected int iteration;	
	protected ErrorHandler errorHandler;
	protected ServerConnector connector;
	protected int port;
	protected long idleTimeout = 86400000;
	protected SslContextFactory sslContextFactory;
	// our httpd conf uses: "%t %a %A (%V) %>s %T %{SSL_PROTOCOL}x %{SSL_CIPHER}x %b %{outstream}n/%{instream}n (%{ratio}n%%) \"%r\" \"%{Referer}i\" \"%{User-Agent}i\"";
	protected String requestLogFormat = "%a %A (%V) %>s %T %{SSL_PROTOCOL}x %{SSL_CIPHER}x %b \"%r\" \"%{Referer}i\" \"%{User-Agent}i\"";
	
	public JettyService(String serviceName) {
		super();	
		DefaultSessionIdManager sessionIdManager = new DefaultSessionIdManager(this);
        this.setSessionIdManager(sessionIdManager);
		this.serviceName = serviceName;
		registerJMX();
	}
	
	@Override
    protected void doStart() throws Exception {		
		HttpConfiguration httpConfig = new HttpConfiguration();
		httpConfig.setSecureScheme("https");
		if (sslContextFactory == null) {
			httpConfig.addCustomizer(new SSLForwardedRequestCustomizer());
			connector = new ServerConnector(this, new HttpConnectionFactory(httpConfig));
		} else {			
			httpConfig.addCustomizer(new SecureRequestCustomizer(true, 31536000, true));
	        connector = new ServerConnector(this, new SslConnectionFactory(sslContextFactory, HttpVersion.HTTP_1_1.asString()), new HttpConnectionFactory(httpConfig));
		}
		connector.setPort(port);
		connector.setIdleTimeout(idleTimeout);
		addConnector(connector);
		super.doStart();
	}

	protected void registerJMX() {
		MBeanContainer mbContainer = new MBeanContainer(ManagementFactory.getPlatformMBeanServer());
		addEventListener(mbContainer);
		addBean(mbContainer);
	}

	public boolean addServiceStatusListener(ServiceStatusListener listener) {
		boolean changed = serviceStatusListeners.add(listener);
		if(changed) {
			fireServiceStatusChanged(null, status.intValue() == STATUS_STARTED ? ServiceStatus.STARTED : ServiceStatus.STOPPED, iteration);
		}
		return changed;
	}

	public boolean removeServiceStatusListener(ServiceStatusListener listener) {
		return serviceStatusListeners.remove(listener);
	}

	protected void fireServiceStatusChanged(Thread thread, ServiceStatus serviceStatus, long iterationCount) {
		if(serviceStatusListeners.isEmpty())
			return;
		ServiceStatusInfo info = new ServiceStatusInfo();
		info.setIterationCount(iterationCount);
		info.setService(this);
		info.setServiceStatus(serviceStatus);
		info.setThread(thread);
		for(ServiceStatusListener l : serviceStatusListeners) {
			l.serviceStatusChanged(info);
		}
	}

	/**
	 * @see simple.app.Service#getNumThreads()
	 */
	public int getNumThreads() {
		return status.get() == STATUS_STARTED ? 1 : 0;
	}

	/**
	 * @see simple.app.Service#getServiceName()
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @see simple.app.Service#pauseThreads()
	 */
	public int pauseThreads() throws ServiceException {
		return stopAllThreads(false, 0);
	}

	/**
	 * @see simple.app.Service#restartThreads(long)
	 */
	public int restartThreads(long timeout) throws ServiceException {
		stopAllThreads(false, timeout);
		return startThreads(1);
	}

	/**
	 * @see simple.app.Service#startThreads(int)
	 */
	public int startThreads(int numThreads) throws ServiceException {
		if(numThreads > 0) {
			if(status.compareAndSet(STATUS_STOPPED, STATUS_STARTING)) {
				fireServiceStatusChanged(null, ServiceStatus.STARTING, iteration);
				try {
					start();
				} catch(Exception e) {
					status.compareAndSet(STATUS_STARTING, STATUS_STOPPED);
					fireServiceStatusChanged(null, ServiceStatus.STOPPED, iteration);
					throw new ServiceException(e);
				}
				status.compareAndSet(STATUS_STARTING, STATUS_STARTED);
				fireServiceStatusChanged(null, ServiceStatus.STARTED, ++iteration);
				return 1;
			}
		}
		return 0;
	}

	/**
	 * @see simple.app.Service#stopAllThreads(boolean, long)
	 */
	public int stopAllThreads(boolean force, long timeout) throws ServiceException {
		if(status.compareAndSet(STATUS_STARTED, STATUS_STOPPING)) {
			fireServiceStatusChanged(null, ServiceStatus.STOPPING, iteration);
			try {
				stop();
			} catch(Exception e) {
				status.compareAndSet(STATUS_STOPPING, STATUS_STARTED);
				fireServiceStatusChanged(null, ServiceStatus.STARTED, iteration);
				throw new ServiceException(e);
			}
			status.compareAndSet(STATUS_STOPPING, STATUS_STOPPED);
			fireServiceStatusChanged(null, ServiceStatus.STOPPED, iteration);
			return 1;
		}
		return 0;
	}

	public Future<Integer> stopAllThreads(boolean force) throws ServiceException {
		return new TrivialFuture<Integer>(stopAllThreads(force, 0));
	}
	
	/**
	 * @see simple.app.Service#stopThreads(int, boolean, long)
	 */
	public int stopThreads(int numThreads, boolean force, long timeout) throws ServiceException {
		if(numThreads > 0) {
			return stopAllThreads(force, timeout);
		}
		return 0;
	}
	public Future<Integer> stopThreads(int numThreads, boolean force) throws ServiceException {
		return new TrivialFuture<Integer>(stopThreads(numThreads, force, 0));
	}
	
	/**
	 * @see simple.app.Service#unpauseThreads()
	 */
	public int unpauseThreads() throws ServiceException {
		return startThreads(1);
	}

	@Override
	public Future<Boolean> prepareShutdown() throws ServiceException {
		return WorkQueue.TRUE_FUTURE;
	}

	/**
	 * @see simple.app.Service#shutdown()
	 */
	@Override
	public Future<Boolean> shutdown() throws ServiceException {
		final Future<Integer> shutdownFuture = stopAllThreads(false);
		return new ResultFuture<Boolean>() {
			protected Boolean produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
				shutdownFuture.get(timeout, unit);
				return Boolean.TRUE;
			}
		};
	}

	public ErrorHandler getErrorHandler() {
		return errorHandler;
	}

	public void setErrorHandler(ErrorHandler errorHandler) {
		if(this.errorHandler != null)
			removeBean(this.errorHandler);
		this.errorHandler = errorHandler;
		if(errorHandler != null)
			addBean(errorHandler);
	}

	@Override
	public void handle(HttpChannel channel) throws IOException, ServletException {		
		String f = getRequestLogFormat();
		long start;
		if(log.isInfoEnabled() && !StringUtils.isBlank(f))
			start = System.nanoTime();
		else
			start = 0L;
		super.handle(channel);
		if(log.isInfoEnabled() && !StringUtils.isBlank(f)) {
			long end = System.nanoTime();
			StringBuilder sb = new StringBuilder();
			int pos = 0;
			while(pos < f.length()) {
				int next = f.indexOf('%', pos);
				if(next < 0) {
					sb.append(f, pos, f.length());
					break;
				}
				sb.append(f, pos, next);
				pos = next + 1;
				if(pos >= f.length())
					break;
				char ch = f.charAt(pos++);
				String sub;
				switch(ch) {
					case '<':
					case '>': // we don't service multiple requests so ignore these
						sub = String.valueOf(ch);
						ch = f.charAt(pos++);
						break;
					case '{':
						next = f.indexOf('}', pos);
						if(next >= 0 && next + 1 < f.length()) {
							sub = f.substring(pos, next);
							pos = next + 1;
							ch = f.charAt(pos++);
						} else
							sub = null; // invalid just ignore, I guess
						break;
					default:
						sub = null;
				}
				String s;
				switch(ch) {
					case '%':
						sb.append('%');
						break;
					case 'a':
						// get remote IP from the header set by Sophos WAF
						if (StringUtils.isBlank(channel.getRequest().getHeader("X-Forwarded-For")))
							sb.append(channel.getEndPoint().getRemoteAddress().getAddress().getHostAddress());
						else
							sb.append(channel.getRequest().getHeader("X-Forwarded-For"));
						break;
					case 'A':
						sb.append(channel.getEndPoint().getLocalAddress().getAddress().getHostAddress());
						break;
					case 'B':
						sb.append(channel.getResponse().getContentCount());
						break;
					case 'b':
						long cnt = channel.getResponse().getContentCount();
						if(cnt > 0)
							sb.append(cnt);
						else
							sb.append('-');
						break;
					case 'C':
						if(sub != null) {
							Cookie[] cookies = channel.getRequest().getCookies();
							if(cookies != null)
								for(Cookie cookie : cookies) {
									if(sub.equalsIgnoreCase(cookie.getName())) {
										if(cookie.getValue() != null)
											sb.append(cookie.getValue());
										else
											sb.append('-');
										break;
									}
								}

						}
						break;
					case 'D':
						sb.append(TimeUnit.NANOSECONDS.toMicros(end - start));
						break;
					case 'e':
						if(sub != null) {
							s = System.getProperty(sub);
							if(s != null)
								sb.append(s);
							else
								sb.append('-');
						}
						break;
					case 'f':
						sb.append(channel.getRequest().getRequestURI());
						break;
					case 'h':
						sb.append(channel.getEndPoint().getRemoteAddress().getHostName());
						break;
					case 'H':
						sb.append(channel.getRequest().getProtocol());
						break;
					case 'i':
						if(sub != null) {
							s = channel.getRequest().getHeader(sub);
							if(s != null)
								sb.append(s);
							else
								sb.append('-');
						}
						break;
					case 'k':
						sb.append('-');
						break;
					case 'l':
						sb.append('-');
						break;
					case 'm':
						sb.append(channel.getRequest().getMethod());
						break;
					case 'n':
						sb.append('-');
						break;
					case 'o':
						if(sub != null) {
							s = channel.getResponse().getHeader(sub);
							if(s != null)
								sb.append(s);
							else
								sb.append('-');
						}
						break;
					case 'p':
						if(sub != null && "remote".equalsIgnoreCase(sub))
							sb.append(channel.getEndPoint().getRemoteAddress().getPort());
						else
							sb.append(channel.getEndPoint().getLocalAddress().getPort());
						break;
					case 'P':
						sb.append(Thread.currentThread().getName());
						break;
					case 'q':
						s = channel.getRequest().getQueryString();
						if(!StringUtils.isBlank(s))
							sb.append('?').append(s);
						break;
					case 'r':
						sb.append(channel.getRequest().getMethod()).append(' ').append(channel.getRequest().getRequestURI());
						s = channel.getRequest().getQueryString();
						if(!StringUtils.isBlank(s))
							sb.append('?').append(s);
						sb.append(' ').append(channel.getRequest().getProtocol());
						break;
					case 'R':
						sb.append(channel.getRequest().getServletName());
						break;
					case 's':
						sb.append(channel.getResponse().getStatus());
						break;
					case 't':
						if(sub == null)
							sub = "[dd/MMM/yyyy:HH:mm:ss Z]";
						sb.append(ConvertUtils.getFormat("DATE", sub).format(new Date()));
						break;
					case 'T':
						sb.append(TimeUnit.NANOSECONDS.toMillis(end - start));
						break;
					case 'u':
						s = channel.getRequest().getRemoteUser();
						if(s != null)
							sb.append(s);
						else {
							Principal p = channel.getRequest().getUserPrincipal();
							if(p != null)
								sb.append(p.getName());
							else {
								ServletUser user = (ServletUser) channel.getRequest().getSession().getAttribute("simple.servlet.ServletUser");
								if(user != null)
									sb.append(user.getUserName());
								else
									sb.append('-');
							}
						}
						break;
					case 'U':
						sb.append(channel.getRequest().getRequestURL());
						break;
					case 'v':
						sb.append(channel.getRequest().getServerName());
						break;
					case 'V':
						sb.append(channel.getRequest().getServerName());
						break;
					case 'x':
						if(sub != null) {
							switch(sub.toUpperCase()) {
								case "SSL_PROTOCOL":
									if (!StringUtils.isBlank(channel.getConnector().getDefaultConnectionFactory().getProtocol()))
										sb.append(channel.getConnector().getDefaultConnectionFactory().getProtocol());
									else
										sb.append('-');
									break;
								case "SSL_CIPHER":
									if (!StringUtils.isBlank(ConvertUtils.getStringSafely(channel.getRequest().getAttribute("javax.servlet.request.cipher_suite"), "")))
										sb.append(ConvertUtils.getStringSafely(channel.getRequest().getAttribute("javax.servlet.request.cipher_suite"), ""));
									else
										sb.append('-');
									break;
							}
						}
						break;
					case 'X':
						if(channel.getEndPoint().isOpen())
							sb.append('+');
						else
							sb.append('X');
						break;
					case 'I':
						sb.append('-'); // Can't find way of getting number of bytes read
						break;
					case 'O':
						sb.append('-'); // Can't find way of getting number of bytes written
						break;
					default:
						sb.append('%');
						if(sub != null)
							sb.append(sub);
						sb.append(ch);
				}
			}
			log.info(sb.toString());
		}
	}

	public String getRequestLogFormat() {
		return requestLogFormat;
	}

	public void setRequestLogFormat(String requestLogFormat) {
		this.requestLogFormat = requestLogFormat;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public long getIdleTimeout() {
		return idleTimeout;
	}

	public void setIdleTimeout(long idleTimeout) {
		this.idleTimeout = idleTimeout;
	}

	public SslContextFactory getSslContextFactory() {
		return sslContextFactory;
	}

	public void setSslContextFactory(SslContextFactory sslContextFactory) {
		this.sslContextFactory = sslContextFactory;
	}	
}
