package simple.servlet.jetty;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.ForwardedRequestCustomizer;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.Request;

public class SSLForwardedRequestCustomizer extends ForwardedRequestCustomizer {
	@Override
    public void customize(Connector connector, HttpConfiguration config, Request request) {
		if (request.getHeader("X-Forwarded-Ssl") != null)
			request.setScheme("https");
	    super.customize(connector, config, request);
	}
}
