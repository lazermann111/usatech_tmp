package simple.servlet.jetty;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.jetty.util.ssl.SslContextFactory;

public class SimpleSslContextFactory extends SslContextFactory {

	public SimpleSslContextFactory() {
		super();
	}

	public SimpleSslContextFactory(boolean trustAll) {
		super(trustAll);
	}

	public SimpleSslContextFactory(String keyStorePath) {
		super(keyStorePath);
	}

	public void selectCipherSuites(String[] enabledCipherSuites, String[] supportedCipherSuites) {
		Set<String> selected_ciphers = new LinkedHashSet<String>();

		String[] includeCipherSuites = getIncludeCipherSuites();
		Set<String> supportedSet = new HashSet<String>(Arrays.asList(supportedCipherSuites));
		// Set the starting ciphers - either from the included or enabled list
		if(includeCipherSuites != null) {
			// Use only the supported included ciphers
			for(String cipherSuite : includeCipherSuites)
				if(supportedSet.contains(cipherSuite))
					selected_ciphers.add(cipherSuite);
		} else
			selected_ciphers.addAll(Arrays.asList(enabledCipherSuites));

		String[] excludeCipherSuites = getExcludeCipherSuites();
		// Remove any excluded ciphers
		if(excludeCipherSuites != null)
			selected_ciphers.removeAll(Arrays.asList(excludeCipherSuites));
		//_selectedCipherSuites = selected_ciphers.toArray(new String[selected_ciphers.size()]);
	}
}
