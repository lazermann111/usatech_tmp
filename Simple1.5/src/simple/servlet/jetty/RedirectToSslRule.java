package simple.servlet.jetty;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.rewrite.handler.Rule;

public class RedirectToSslRule extends Rule {
	protected int sslport = 443;

	public RedirectToSslRule() {
		setHandling(true);
		setTerminating(true);
	}

	@Override
	public String matchAndApply(String target, HttpServletRequest request, HttpServletResponse response) throws IOException {
		if(request.isSecure())
			return null;
		StringBuilder sb = new StringBuilder();
		sb.append(request.getScheme()).append("s://").append(request.getServerName()).append(':').append(getSslport()).append(request.getRequestURI());
		String tmp = request.getQueryString();
		if(tmp != null)
			sb.append('?').append(tmp);
		target = sb.toString();
		response.sendRedirect(response.encodeRedirectURL(target));
		return target;
	}

	public int getSslport() {
		return sslport;
	}

	public void setSslport(int sslport) {
		this.sslport = sslport;
	}
}
