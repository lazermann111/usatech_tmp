package simple.servlet.jetty;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;

public class SSLSelectChannelConnector extends ServerConnector {
	public SSLSelectChannelConnector(Server server) {
		super(server);		
	}	

	// this works differently in Jetty 9, keeping this for now just for reference
	/*public void customize(EndPoint endpoint, Request request) throws IOException
	{
		if (request.getHeader("X-Forwarded-Ssl") != null)
			request.setScheme("https");
	    super.customize(endpoint, request);
	}
	
	public boolean isConfidential(Request request) {
        if (request.getHeader("X-Forwarded-Ssl") != null)
            return true;
        else
            return super.isConfidential(request);
    }*/
}
