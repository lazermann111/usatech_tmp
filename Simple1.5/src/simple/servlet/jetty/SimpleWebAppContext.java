package simple.servlet.jetty;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;

import org.apache.commons.configuration.Configuration;
import org.eclipse.jetty.annotations.ServletContainerInitializersStarter;
import org.eclipse.jetty.apache.jsp.JettyJasperInitializer;
import org.eclipse.jetty.plus.annotation.ContainerInitializer;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.FilterMapping;
import org.eclipse.jetty.servlet.Holder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlet.ServletMapping;
import org.eclipse.jetty.servlet.Source;
import org.eclipse.jetty.util.Loader;
import org.eclipse.jetty.webapp.WebAppContext;

import simple.app.BaseWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.io.Log;
import simple.text.StringUtils;

public class SimpleWebAppContext extends WebAppContext {
	private static final Log log = Log.getLog();
	protected static final String SERVLET_CONFIG_PREFIX = "servlet";
	protected static final String FILTER_CONFIG_PREFIX = "filter";
	protected static final Pattern SERVLET_OR_FILTER_CONFIG_PATTERN = Pattern.compile("((servlet|filter)Holder\\(([^)]+)\\))\\..+");
	protected final Configuration configuration;
	protected final Map<String, Object> namedReferences;
	protected static final String JAVA_SPEC_VERSION = System.getProperty("java.specification.version");
	protected static final Class<?> JSP_SERVLET_CLASS;
	static {
		Class<?> tmp;
		try {
			tmp = Class.forName("org.apache.jasper.servlet.JspServlet");
		} catch(Exception e) {
			tmp = null;
		}
		JSP_SERVLET_CLASS = tmp;
	}
	
	protected class SimpleServletHolder extends ServletHolder {
		public SimpleServletHolder(Source creator) {
			super(creator);
		}

		@Override
		public synchronized void setServlet(Servlet servlet) {
			super.setServlet(servlet);
			try {
				configureServlet(getServlet(), getName(), this);
			} catch(Exception e) {
				log.error("Error configuring servlet " + getName(), e);
			}
		}

		@Override
		public Servlet newInstance() throws ServletException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
			Servlet s = super.newInstance();
			configureServlet(s, getName(), this);
			return s;
		}
	}

	protected class SimpleFilterHolder extends FilterHolder {
		public SimpleFilterHolder(Source source) {
			super(source);
		}

		@Override
		public synchronized void setFilter(Filter filter) {
			super.setFilter(filter);
			try {
				configureFilter(getFilter(), getName(), this);
			} catch(Exception e) {
				log.error("Error configuring filter " + getName(), e);
			}
		}

		@Override
		public void doStart() throws Exception {
			if(getFilter() == null) {
				// try to load class
				if(getHeldClass() == null) {
					try {
						setHeldClass(Loader.loadClass(Holder.class, getClassName()));
						if(log.isDebugEnabled())
							log.debug("Holding " + _class);
					} catch(Exception e) {
						log.warn(e);
						throw new UnavailableException(e.getMessage(), -1);
					}
				}
				try {
					setFilter(((ServletContextHandler.Context) _servletHandler.getServletContext()).createFilter(getHeldClass()));
				} catch(ServletException se) {
					Throwable cause = se.getRootCause();
					if(cause instanceof InstantiationException)
						throw (InstantiationException) cause;
					if(cause instanceof IllegalAccessException)
						throw (IllegalAccessException) cause;
					throw se;
				}
			}
			super.doStart();
		}
	}

	protected class SimpleServletHandler extends ServletHandler {
		// protected final Set<ServletHolder> configuredServletHolders = new HashSet<ServletHolder>();

		public SimpleServletHandler() {
			super();
		}
		public ServletHolder newServletHolder(Source source) {
			return new SimpleServletHolder(source);
		}
		public FilterHolder newFilterHolder(Source source) {
			return new SimpleFilterHolder(source);
		}
		/*
		@Override
		public void initialize() throws Exception {
			applyConfiguration();
			super.initialize();
		}*/
	}
	
	private static List<ContainerInitializer> jspInitializers() {
		JettyJasperInitializer sci = new JettyJasperInitializer();
		ContainerInitializer initializer = new ContainerInitializer(sci, null);
		List<ContainerInitializer> initializers = new ArrayList<ContainerInitializer>();
		initializers.add(initializer);
		return initializers;
	}

	public SimpleWebAppContext(Configuration configuration, Map<String,Object> namedReferences) throws SecurityException, IllegalArgumentException, IntrospectionException, ParseException, InstantiationException, ConvertException, ServiceException {
		super();
		this.configuration = configuration;
		this.namedReferences = namedReferences;	
		BaseWithConfig.configureProperties(this, configuration, namedReferences, true);
		setServletHandler(new SimpleServletHandler());
		
		//Allow loading JSTL 
		setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",".*/[^/]*servlet-api-[^/]*\\.jar$|.*/javax.servlet.jsp.jstl-.*\\.jar$|.*/org.apache.taglibs.taglibs-standard-impl-.*\\.jar$");		
		
		//Configure the application to support the compilation of JSP files
		setAttribute("org.eclipse.jetty.containerInitializers", jspInitializers());
		addBean(new ServletContainerInitializersStarter(this), true);
	}

	public void configure() throws Exception {
		super.configure();
	}
	
	public void postConfigure() throws Exception {
		super.postConfigure();
		applyConfiguration();
	}

	protected void applyConfiguration() throws Exception {
		Set<String> configured = new HashSet<String>();
		for(Iterator<String> iter = configuration.getKeys(); iter.hasNext(); ) {
			Matcher m = SERVLET_OR_FILTER_CONFIG_PATTERN.matcher(iter.next());
			if(m.matches()) {
				String prefix = m.group(1);
				String type = m.group(2);
				String name = m.group(3);
				if(configured.add(type + "(" + name + ")")) {
					if("servlet".equals(type)) {
						ServletHolder holder = getServletHandler().getServlet(name);
						if(holder == null) {
							holder = getServletHandler().newServletHolder(Source.EMBEDDED);
							holder.setName(name);
							getServletHandler().addServlet(holder);
							String path = configuration.getString("servletHolder(" + name + ").path");
							if(!StringUtils.isBlank(path)) {
								String[] pathSpecs = StringUtils.split(path, StringUtils.STANDARD_DELIMS, false);
								ServletMapping mapping = new ServletMapping();
								mapping.setServletName(name);
								mapping.setPathSpecs(pathSpecs);
								getServletHandler().addServletMapping(mapping);
							}
						}
						configureObject(holder, prefix);
						if(JSP_SERVLET_CLASS != null /*&& ((holder.getHeldClass() != null && JSP_SERVLET_CLASS.isAssignableFrom(holder.getHeldClass())) || JSP_SERVLET_CLASS.getName().equals(holder.getClassName()))*/) {
							if(StringUtils.isBlank(holder.getInitParameter("compilerTargetVM")))
								holder.setInitParameter("compilerTargetVM", JAVA_SPEC_VERSION);
							if(StringUtils.isBlank(holder.getInitParameter("compilerSourceVM")))
								holder.setInitParameter("compilerSourceVM", JAVA_SPEC_VERSION);
						}
					} else if("filter".equals(type)) {
						FilterHolder holder = getServletHandler().getFilter(name);
						if(holder == null) {
							holder = getServletHandler().newFilterHolder(Source.EMBEDDED);
							holder.setName(name);
							String path = configuration.getString("filterHolder(" + name + ").path");
							getServletHandler().addFilter(holder);
							if(!StringUtils.isBlank(path)) {
								String[] pathSpecs = StringUtils.split(path, StringUtils.STANDARD_DELIMS, false);
								FilterMapping mapping = new FilterMapping();
								mapping.setFilterName(name);
								mapping.setPathSpecs(pathSpecs);
								getServletHandler().addFilterMapping(mapping);
							}
						}
						configureObject(holder, prefix);
					} else
						log.warn("Invalid type '" + type + "' - programming error");
				}
			}
		}
	}

	protected void configureServlet(Servlet servlet, String name, ServletHolder servletHolder) throws SecurityException, IllegalArgumentException, InstantiationException {
		configureObject(servlet, new StringBuilder(SERVLET_CONFIG_PREFIX).append('(').append(name).append(')').toString());
	}

	protected void configureFilter(Filter filter, String name, FilterHolder filterHolder) throws SecurityException, IllegalArgumentException, InstantiationException {
		configureObject(filter, new StringBuilder(FILTER_CONFIG_PREFIX).append('(').append(name).append(')').toString());
	}

	protected void configureObject(Object object, String configPrefix) throws SecurityException, IllegalArgumentException, InstantiationException {
		Configuration servletConfiguration = configuration.subset(configPrefix);
		try {
			BaseWithConfig.configureProperties(object, servletConfiguration, namedReferences, true);
		} catch(IntrospectionException e) {
			log.warn("Could not configure " + configPrefix + " " + object, e);
		} catch(ParseException e) {
			log.warn("Could not configure " + configPrefix + " " + object, e);
		} catch(ConvertException e) {
			log.warn("Could not configure " + configPrefix + " " + object, e);
		} catch(ServiceException e) {
			log.warn("Could not configure " + configPrefix + " " + object, e);
		}
	}

	public void setLocaleEncoding(String locale, String encoding) {
		super.addLocaleEncoding(locale, encoding);
	}

}
