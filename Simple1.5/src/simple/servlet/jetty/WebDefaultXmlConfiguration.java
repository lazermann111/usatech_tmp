package simple.servlet.jetty;

import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebXmlConfiguration;

public class WebDefaultXmlConfiguration extends WebXmlConfiguration {

	public WebDefaultXmlConfiguration() {
	}

	@Override
	public void preConfigure(WebAppContext context) throws Exception {
		// parse webdefault.xml
		String defaultsDescriptor = context.getDefaultsDescriptor();
		if(defaultsDescriptor != null && defaultsDescriptor.length() > 0) {
			Resource dftResource = Resource.newSystemResource(defaultsDescriptor);
			if(dftResource == null)
				dftResource = context.newResource(defaultsDescriptor);
			context.getMetaData().setDefaults(dftResource);
		}
	}
}
