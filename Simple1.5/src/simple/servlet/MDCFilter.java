package simple.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.MDC;

import simple.bean.ConvertUtils;

public class MDCFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(request instanceof HttpServletRequest) {
        	HttpServletRequest httpRequest = (HttpServletRequest) request;
        	String remoteAddress = httpRequest.getRemoteAddr();
        	if (remoteAddress != null && remoteAddress.length() > 0) {
		    	try {
		    		StringBuilder userInfo = new StringBuilder();
		    		String remoteUser = ConvertUtils.getStringSafely(httpRequest.getSession().getAttribute("remoteUser"), null);
		    		if (remoteUser != null)
		    			userInfo.append(remoteUser).append('@');
		    		userInfo.append(remoteAddress).append(':').append(httpRequest.getRemotePort());
		    		MDC.put("userInfo", userInfo);
		    		chain.doFilter(request, response);
		        }
		        finally {
		          MDC.remove("userInfo");
		        }
        	}
        } else
        	chain.doFilter(request, response);
    }
    
    public void destroy() {
    }
}
