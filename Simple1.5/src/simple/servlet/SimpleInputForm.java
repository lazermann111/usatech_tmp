/*
 * InputForm.java
 *
 * Created on January 14, 2003, 3:11 PM
 */

package simple.servlet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.fileupload.FileUploadException;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;

/** The default implementation of the InputForm. This object acts like a DynaBean
 * with the attributes of the request representing the properties of the bean
 * for easy use in many utility functions
 * @author Brian S. Krug
 */
public class SimpleInputForm extends AbstractInputForm implements RequestInfo {
    protected ServletRequest request;
    protected Map<String,Cookie> cookies;
    protected Map<String,Object> parameters;
    
    /** Creates new InputForm
     * @param request The backing ServletRequest
     * @throws FileUploadException 
     */
    public SimpleInputForm(ServletRequest request, Map<String,Object> parameters) {
    	super();
        this.request = request;
        this.parameters = parameters;
    }
        
    public String getHeader(String name) {
        if(request instanceof HttpServletRequest) {
            return ((HttpServletRequest)request).getHeader(name);
        }        
        return null;
    }
    
    /** Getter for property request.
     * @return Value of property request.
     *
     */
    public ServletRequest getRequest() {
        return this.request;
    }
                
    /** Returns the specified attribute from the specified scope
     * @param key The attribute name
     * @param scope The scope in which to look ("request", "session", "application" or null for ANY
     * scope)
     * @return The attribute value
     */    
    public Object getAttribute(String key, String scope) {
        return RequestUtils.getRequestAttribute(request, key, scope);
    }

    /** Sets the specified attribute in the specified scope
     * @param key The attribute name
     * @param value The new attribute value
     * @param scope The scope in which to set the attribute ("request", "session", or "application")
     */    
    public void setAttribute(String key, Object value, String scope) {
        RequestUtils.setAttribute(request, key, value, scope);
    }

    protected DynaProperty[] getDynaClassProperties() { 
        List<DynaProperty> props = new ArrayList<DynaProperty>();
        for(Enumeration<?> en = request.getAttributeNames(); en.hasMoreElements(); ) {
            String name = (String)en.nextElement();
            DynaProperty dp = new DynaProperty(name);
            props.add(dp);
        }
        /*for(Enumeration enum = request.getParameterNames(); enum.hasMoreElements(); ) {
            String name = (String)enum.nextElement();
            DynaProperty dp = new DynaProperty(name);
            props.add(dp);
        }*/
        if(request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest)request;
            HttpSession session = httpRequest.getSession();
            if(session != null) {
                for(Enumeration<?> en = session.getAttributeNames(); en.hasMoreElements(); ) {
                    String name = (String)en.nextElement();
                    DynaProperty dp = new DynaProperty("session:"+name);
                    props.add(dp);
                }
                for(Enumeration<?> en = session.getServletContext().getAttributeNames(); en.hasMoreElements(); ) {
                    String name = (String)en.nextElement();
                    DynaProperty dp = new DynaProperty("application:"+name);
                    props.add(dp);
                }
            } 
            for(Enumeration<?> en = httpRequest.getHeaderNames(); en.hasMoreElements(); ) {
                String name = (String)en.nextElement();
                DynaProperty dp = new DynaProperty("header:"+name, String.class);
                props.add(dp);
            }
            for(String name : getCookies().keySet()) {
                DynaProperty dp = new DynaProperty("cookie:"+name, Cookie.class);
                props.add(dp);
            }
        }
        props.add(new DynaProperty("request", ServletRequest.class));
        return props.toArray(new DynaProperty[props.size()]);       
    }

	public Cookie getCookie(String name) {
		simple.io.Log.getLog().debug("Getting Cookie '" + name + "'");
		return getCookies().get(name);
	}

	protected Map<String,Cookie> getCookies() {
		if(cookies == null) {
			if(request instanceof HttpServletRequest) {
				cookies = new HashMap<String, Cookie>();
				Cookie[] cookieArray = ((HttpServletRequest)request).getCookies();
				if(cookieArray != null)
					for(Cookie c : cookieArray) {
						cookies.put(c.getName(), c);
					}
	        } else {
	        	cookies = Collections.emptyMap();
	        }       	
		}
        return cookies;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	@Override
	public String getServerName() {
		return request.getServerName();
	}

	@Override
	public String getContextPath() {
		if(request instanceof HttpServletRequest)
			return ((HttpServletRequest) request).getContextPath();
		return null;
	}

	@Override
	public String getPathInfo() {
		if(request instanceof HttpServletRequest)
			return ((HttpServletRequest) request).getPathInfo();
		return null;
	}

	@Override
	public String getRealPath(String webUri) {
		if(!(request instanceof HttpServletRequest))
			return webUri;
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		ServletContext servletContext = httpRequest.getSession().getServletContext();
		return servletContext.getRealPath(webUri);
		/*
		String realPath;
		if(StringUtils.startsWith(webUri, httpRequest.getContextPath())) {
			realPath = servletContext.getRealPath(webUri.substring(httpRequest.getContextPath().length()));
		} else {
			ServletContext rootServletContext = servletContext.getContext("/");
			if(rootServletContext == null)
				return webUri;
			realPath = rootServletContext.getRealPath(webUri);
		}
		return realPath;
		*/
	}

	@Override
	public Locale getLocale() {
		String lang = getHeader("accept-language");
		if(lang != null)
			lang = lang.split("[,]", 2)[0];
		return ConvertUtils.convertSafely(Locale.class, lang, null);
	}

	@Override
	public String getBaseUrl() {
		if(request instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			StringBuffer url = httpRequest.getRequestURL();
			if(url != null) {
				if(!StringUtils.isBlank(httpRequest.getQueryString()))
					url.append('?').append(httpRequest.getQueryString());
				return url.toString();
			}
		}
		return null;
	}

	@Override
	public Translator getTranslator() {
		Translator translator = (Translator) getAttribute("simple.util.Translator", null);
		if(translator == null) {
			String host = request.getServerName();
			Locale locale = getLocale();
			try {
				translator = TranslatorFactory.getDefaultFactory().getTranslator(locale, host);
			} catch(ServiceException e) {
				Log.getLog().warn("Could not get translator", e);
				translator = DefaultTranslatorFactory.getTranslatorInstance();
			}
			setAttribute("simple.util.Translator", translator, "session");
		}
		return translator;
	}

	@Override
	public String addLastModifiedToUri(String path) {
		return RequestInfoUtils.addLastModifiedToUri(this, path);
	}

	@Override
	public String getUserAgent() {
		return getHeader("user-agent");
	}
}
