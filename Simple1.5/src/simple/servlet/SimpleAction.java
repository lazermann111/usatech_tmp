/*
 * SimpleAction.java
 *
 * Created on October 16, 2003, 10:56 AM
 */

package simple.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.text.StringUtils;

/** An action that performs a sequence of steps configured before-hand by the configure() method.
 *
 * @author  Brian S. Krug
 */
public class SimpleAction implements Action {
    private static final simple.io.Log log = simple.io.Log.getLog();
	protected boolean xsrfProtected = false;
    protected boolean userRequired = true;
    protected boolean isPrivate = false;
	protected String requiredPrivileges;
	protected List<String[]> privList = new ArrayList<String[]>();
    protected List<Step> stepList = new LinkedList<Step>();
    protected List<Catch> catchList = new LinkedList<Catch>();
    /** Value under which any previous pages are stored in the request so as to avoid endless recursion */    
    public static final String ATTRIBUTE_PREVIOUS_PAGES = "simple.servlet.SimpleAction.previous_pages";
    /** Value under which any previous actions are stored in the request so as to avoid endless recursion */    
    public static final String ATTRIBUTE_PREVIOUS_ACTIONS = "simple.servlet.SimpleAction.previous_actions";
    /** Value under which an exception is stored in the request */    
    public static final String ATTRIBUTE_EXCEPTION = "simple.servlet.SimpleAction.exception";
    public static final String ATTRIBUTE_EXCEPTION_MESSAGE = "simple.servlet.SimpleAction.exceptionMessage";
    
    public static final int INVOKE_RESULT_NORMAL_FLOW = 0;
    public static final int INVOKE_RESULT_REDIRECT = 1;
	protected static final char[] PRIV_SET_DELIMITER = "|".toCharArray();
    
    protected static class LastStepException extends RuntimeException {
    	private static final long serialVersionUID = 1L;
		public LastStepException() {
			super();
		}   	
    }
    
    protected static final LastStepException lastStepException = new LastStepException();
    static final Step RETURN_STEP = new Step() {
		public void addCatch(Catch c) {
		}
		public void configure(Map<String,?> props) throws StepConfigException {
		}
		public Iterator<Catch> getCatches() {
			return null;
		}
		public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
			throw lastStepException;
		}
	};
	
    /** Creates a new instance of SimpleAction */
    public SimpleAction() {
    }
    
	@Override
	public boolean isAuthorized(ServletUser user) {
		if(!isUserRequired())
			return true;
		if(user == null)
			return false;
		OUTER: for(String[] privOps : privList) {
			for(String priv : privOps)
				if(user.hasPrivilege(priv))
					continue OUTER;
			return false;
		}
		return true;
    }

	public String getAuthorizationRequirements() {
		return requiredPrivileges == null ? "none" : "the required privilege(s) [" + requiredPrivileges + "]";
	}

	public String getRequiredPrivileges() {
		return requiredPrivileges;
	}

	public void setRequiredPrivileges(String requiredPrivileges) {
		String[] sets = StringUtils.split(requiredPrivileges, StringUtils.STANDARD_DELIMS, StringUtils.STANDARD_QUOTES);
		privList.clear();
		if(sets != null) {
			for(String set : sets) {
				String[] privs = StringUtils.split(set, PRIV_SET_DELIMITER, StringUtils.STANDARD_QUOTES);
				if(privs != null && privs.length > 0)
					privList.add(privs);
			}
		}
		this.requiredPrivileges = requiredPrivileges;

	}

    /** Processes a request by peforming each of the Steps in this Action
     * @param dispatcher The dispatcher for this request. Use this to call another action or to forward
     * processing to a file resource like a JSP or HTML file.
     * @param form The InputForm for this request. It holds all the parameters and attributes for
     * this request. Acts as a DynaBean with the attributes of the request (and session
     * and application) as its properties
     * @param request The ServletRequest that triggered this Step
     * @param response The ServletResponse
     * @throws ServletException If an exception occurs
     */    
    public void invoke(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        if (log.isDebugEnabled())
        	log.debug("Invoking SimpleAction with " + stepList.size() + " step(s)");
        String redirect = form.getRedirectUri();
        if (redirect != null && redirect.length() > 0) {
        	try {
        		response.sendRedirect(redirect.replaceAll("\r", "").replaceAll("\n", ""));
        	} catch (IOException e){
        		throw new ServletException(e);
        	}
			return;
    	}
        String forward = (String) form.getAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY);
        if (forward != null && forward.length() > 0) {
        	form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, null);
        	dispatcher.forward(forward);
			return;
    	}
        for(Step step : stepList) {
            try {
                invokeStep(step, dispatcher, form, request, response);
                redirect = form.getRedirectUri();
                if (redirect != null && redirect.length() > 0) {
                	response.sendRedirect(redirect.replaceAll("\r", "").replaceAll("\n", ""));
    				return;
		    	}
                forward = (String) form.getAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY);
                if (forward != null && forward.length() > 0) {
                	form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, null);
                	dispatcher.forward(forward);
    				return;
		    	}
            } catch(ServletException se) {
				handleException(catchList.iterator(), se, dispatcher, form, request, response);
            } catch(LastStepException e) {
            	//done
            	return;
            } catch(Throwable e) {
                log.warn("Unexpected Exception Occurred", e);
                throw new ServletException(e);
            }
        }
    }

	public static void handleException(Iterator<Catch> catchIter, ServletException servletException, Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		Throwable exception;
		if(servletException.getRootCause() != null)
			exception = servletException.getRootCause();
		else
			exception = servletException;
		while(exception.getCause() != null)
			exception = exception.getCause();
		if (!(exception instanceof NotLoggedOnException)) {
			StringBuilder sb = new StringBuilder("Servlet Exception Occurred: ");
			if (servletException.getMessage() != null)
				sb.append(servletException.getMessage());
			log.warn(sb.toString(), exception);
		}
		request.setAttribute(ATTRIBUTE_EXCEPTION, exception);
		String message = exception.getMessage() != null ? exception.getMessage() : exception.getClass().getName();
		if(message != null) {
			int index = message.indexOf('\n');
			if(index > -1)
				message = message.substring(0, index).trim();
		}
		request.setAttribute(ATTRIBUTE_EXCEPTION_MESSAGE, message);
		// find the first match
		if(catchIter != null)
			do {
				if(forwardToCatch(catchIter, exception, dispatcher, form, request, response))
					return;
				exception = exception.getCause();
			} while(exception != null);
		// if the exception was not handled, re-throw it
		throw servletException;
	}

    /** Processes a request by peforming each of the Steps in this Action
     * @param dispatcher The dispatcher for this request. Use this to call another action or to forward
     * processing to a file resource like a JSP or HTML file.
     * @param form The InputForm for this request. It holds all the parameters and attributes for
     * this request. Acts as a DynaBean with the attributes of the request (and session
     * and application) as its properties
     * @param request The ServletRequest that triggered this Step
     * @param response The ServletResponse
     * @throws ServletException If an exception occurs
     */    
    public static void invokeStep(Step step, Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	try {
            step.perform(dispatcher, form, request, response);
        } catch(ServletException se) {
			handleException(step.getCatches(), se, dispatcher, form, request, response);
        } 
    }
    /** Whether the Action is allowed to be called or not. This implementation
     * always returns true
     * @return if this Action can be called
     */    
    public boolean isAccessible() {
        return true;
    }
    
    /** Whether a user must login to access this Action.
     * @return if a user is required to access this Action
     */    
    public boolean isUserRequired() {
        return userRequired;
    }
       
    /** Setter for property userRequired.
     * @param userRequired New value of property userRequired.
     *
     */
    public void setUserRequired(boolean userRequired) {
        this.userRequired = userRequired;
    }
    
    
    public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	/**
	 * Requires the user to have one of the set of privileges specified
	 * 
	 * @param priv
	 *            The set of privileges
	 */    
	public void addRequiredPrivilegeSet(String... priv) {
        privList.add(priv);
		if(StringUtils.isBlank(requiredPrivileges))
			requiredPrivileges = StringUtils.join(priv, "|");
		else {
			StringBuilder sb = new StringBuilder(requiredPrivileges + 20);
			sb.append(requiredPrivileges).append(',');
			StringUtils.join(priv, "|", sb);
			requiredPrivileges = sb.toString();
		}
    }

    /** Adds the specified step to the list of steps processed upon invoking this Action
     * @param step The step to add to the processing list
     */    
    public void addStep(Step step) {
        stepList.add(step);
    }
    
    /** Adds the specified Catch to the list of Catch objects for this Action. If one
     * of the steps throws a ServletException during processing, the root cause of the 
     * ServletException is examined. If that root cause matches one of the Catch objects
     * for this Action, then the processing is forwarded to the "page" specified by
     * that Catch object.  
     * @param c The catch object to add
     */    
    public void addCatch(Catch c) {
        catchList.add(c);
    }

    /** Attempts to find a Catch that matches and forwards request processing
     * to it. Returns whether a match was found
     * @param catchIter The iterator of Catch objects
     * @param exception The Throwable to match
     * @param dispatcher The dispatcher that is used to forward processing
     * @param request The request
     * @throws ServletException If an exception occurs
     * @return Whether a match was found
     */
    public static boolean forwardToCatch(Iterator<Catch> catchIter, Throwable exception, Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        while(catchIter.hasNext()) {
        	Catch c = catchIter.next();
        	if(log.isDebugEnabled())
        		log.debug("Checking if catch " + c + " matches exception");
            if(c.matches(exception)) {
				if(c.getPage() != null) {
					log.info(exception.getClass().getName() + ": (" + exception.getMessage() + ") matched catch " + c + " - forwarding to page=" + c.getPage());
					if(checkPage(request, c.getPage())) {
            			dispatcher.include(c.getPage()); 
                        return true;
            		}
            	} else if(c.getAction() != null) {
					log.info(exception.getClass().getName() + ": (" + exception.getMessage() + ") matched catch " + c + " - forwarding to action=" + c.getAction());
					if(checkAction(request, c.getAction())) {
            			dispatcher.dispatch(c.getAction(), true); 
                        return true;
            		}
            	} else { // perform all the sub-steps
					log.info(exception.getClass().getName() + ": (" + exception.getMessage() + ") matched catch " + c + " - executing sub-steps");
					for(Iterator<Step> iter = c.getSteps(); iter.hasNext();) {
                        Step s = iter.next();
                        invokeStep(s, dispatcher, form, request, response);
                    }  
                    return true;
            	}
            }
        }
        return false;
    }
    
    /** Ensures that the same page has not been accessed previously in the request
     * processing chain.
     * If we don't check this it could cause a infinite processing loop
     * if the 'catch' is redirecting to the same page that called this action
     * @param request The request
     * @param page The page to check
     * @return Whether this page has already been accessed during this request or not
     */
    protected static boolean checkPage(HttpServletRequest request, String page) {
        return checkSet(request, page, ATTRIBUTE_PREVIOUS_PAGES);
    }

    /** Ensures that the same action has not been accessed previously in the request
     * processing chain.
     * If we don't check this it could cause a infinite processing loop
     * if the 'catch' is redirecting to the same action that called this action
     * @param request The request
     * @param action The action to check
     * @return Whether this action has already been accessed during this request or not
     */
    protected static boolean checkAction(HttpServletRequest request, String action) {
        return checkSet(request, action, ATTRIBUTE_PREVIOUS_ACTIONS);
    }

    /** Ensures that the same item has not been accessed previously in the request
     * processing chain.
     * If we don't check this it could cause a infinite processing loop
     * if the 'catch' is redirecting to the same page that called this action
     * @param request The request
     * @param item The item to check
     * @param attr The attribute in which the set of previously accessed items is stored
     * @return Whether this item has already been accessed during this request or not
     */
	protected static boolean checkSet(HttpServletRequest request, String item, String attr) {
        Set<Object> set = (Set<Object>) request.getAttribute(attr);
        if(set == null) {
        	set = new java.util.HashSet<Object>();
            request.setAttribute(attr, set);
        } else if(set.contains(item)){
            return false;
        }
        set.add(item);
        return true;
    }

	public boolean isXsrfProtected() {
		return xsrfProtected;
	}

	public void setXsrfProtected(boolean xsrfProtected) {
		this.xsrfProtected = xsrfProtected;
	}
}
