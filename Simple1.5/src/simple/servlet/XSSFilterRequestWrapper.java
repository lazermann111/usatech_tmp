/*
 * Original source code: http://greatwebguy.com/programming/java/simple-cross-site-scripting-xss-servlet-filter/ 
 */
package simple.servlet;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public final class XSSFilterRequestWrapper extends HttpServletRequestWrapper {
	protected final Map<Pattern, String> findReplacePatterns = new LinkedHashMap<Pattern, String>();

	public XSSFilterRequestWrapper(HttpServletRequest servletRequest) {
		super(servletRequest);
		addFindReplacePattern(Pattern.compile("^javascript:", Pattern.CASE_INSENSITIVE), "javascript :");
		addFindReplacePattern(Pattern.compile("<script", Pattern.CASE_INSENSITIVE), "< script");
		addFindReplacePattern(Pattern.compile("<iframe", Pattern.CASE_INSENSITIVE), "< iframe");
		// value = value.replaceAll("<", "& lt;").replaceAll(">", "& gt;");
		// value = value.replaceAll("\\(", "& #40;").replaceAll("\\)", "& #41;");
		// value = value.replaceAll("'", "& #39;");
		// value = value.replaceAll("eval\\((.*)\\)", "");
	}

	public void addFindReplacePattern(Pattern find, String replace) {
		findReplacePatterns.put(find, replace);
	}

	public void removeFindReplacePattern(Pattern find) {
		findReplacePatterns.remove(find);
	}

	public String[] getParameterValues(String parameter) {
		String[] values = super.getParameterValues(parameter);
		if(values == null)
			return null;

		int count = values.length;
		String[] encodedValues = new String[count];
		for(int i = 0; i < count; i++) {
			encodedValues[i] = cleanXSS(values[i]);
		}
		return encodedValues;
	}

	public String getParameter(String parameter) {
		String value = super.getParameter(parameter);
		if(value == null)
			return null;
		return cleanXSS(value);
	}

	public String getHeader(String name) {
		String value = super.getHeader(name);
		if(value == null)
			return null;
		return cleanXSS(value);
	}

	protected String cleanXSS(String value) {
		for(Map.Entry<Pattern, String> entry : findReplacePatterns.entrySet()) {
			value = entry.getKey().matcher(value).replaceAll(entry.getValue());
		}
		return value;
	}
}