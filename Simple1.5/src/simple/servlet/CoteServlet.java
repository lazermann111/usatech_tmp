package simple.servlet;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.io.CoteCache;
import simple.io.CoteCache.Item;
import simple.io.CoteCache.ItemClosedException;
import simple.io.Log;
import simple.util.Refresher;

public class CoteServlet extends HttpServlet {
	private static final Log log = Log.getLog();
	private static final long serialVersionUID = -2717519236748882347L;
	protected static final String COTE_ITEM_ATTRIBUTE = CoteServlet.class.getName() + ".Item";
	public static final String CACHE_REFRESHER_ATTRIBUTE = CoteServlet.class.getName() + ".cacheRefresher";

	protected final CoteCache coteCache = new CoteCache();
	protected Pattern pathPattern = Pattern.compile("(?:.*/)([^/]+)\\.\\w+");
	protected String keyParameterName = "k";
	
	public CoteServlet() {
		super();
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		config.getServletContext().setAttribute(CACHE_REFRESHER_ATTRIBUTE, new Refresher() {
			public void refresh() {
				coteCache.clearCache();
			}
		});
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
		for(int attempts = 0; attempts < 2; attempts++) {
			Item item;
			try {
				item = getOrCreateItem(request);
			} catch(ServletException e) {
				log.warn("Could not get item for " + request.getRequestURI(), e);
				item = null;
			}
			if(item == null)
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			else {
				response.setContentType(item.getContentType());
				response.setContentLength((int) item.getLength());
				OutputStream out = response.getOutputStream();
				try {
					item.writeTo(out);
					out.flush();
					out.close();
				} catch(ItemClosedException e) {
					// try again
					log.warn("Item is closed for " + request.getRequestURI() + (attempts == 0 ? "; will try again" : ""), e);
					request.removeAttribute(COTE_ITEM_ATTRIBUTE);
					continue;
				}
			}
			return;
		}
		throw new IOException("Could not read item");
	}
	@Override
	protected long getLastModified(HttpServletRequest request) {
		Item item;
		try {
			item = getOrCreateItem(request);
		} catch(ServletException e) {
			return -1L;
		}
		if(item == null)
			return -1L;
		return item.getLastModified();
	}

	public Item getOrCreateItem(HttpServletRequest request) throws ServletException {
		Object o = request.getAttribute(COTE_ITEM_ATTRIBUTE);
		if(o instanceof Item)
			return (Item) o;
		if(o != null)
			return null;
		Item item = findItem(request);
		if(item == null)
			request.setAttribute(COTE_ITEM_ATTRIBUTE, COTE_ITEM_ATTRIBUTE);
		else
			request.setAttribute(COTE_ITEM_ATTRIBUTE, item);
		return item;
	}

	protected Item findItem(HttpServletRequest request) throws ServletException {
		String path = request.getPathInfo();
		if(path == null)
			path = request.getServletPath(); // its a suffix match
		Matcher matcher = pathPattern.matcher(path);
		if(!matcher.matches())
			return null;
		else {
			final String[] pathMatches;
			if(matcher.groupCount() == 0)
				pathMatches = new String[] { matcher.group(0) };
			else {
				pathMatches = new String[matcher.groupCount()];
				for(int i = 0; i < matcher.groupCount(); i++)
					pathMatches[i] = matcher.group(i + 1);
			}
			Object keyCd = request.getAttribute(getKeyParameterName());
			try {
				return coteCache.getItem(request.getServerName(), pathMatches[0], pathMatches[1], keyCd);
			} catch(IOException e) {
				throw new ServletException(e);
			}
		}
	}

	public String getPathPattern() {
		return pathPattern.pattern();
	}

	public void setPathPattern(String pathPattern) {
		Pattern compiled = Pattern.compile(pathPattern);
		synchronized (this) {
			this.pathPattern = compiled;
		}
	}

	public String getKeyParameterName() {
		return keyParameterName;
	}

	public void setKeyParameterName(String keyParameterName) {
		synchronized (this) {
			this.keyParameterName = keyParameterName;
		}
	}

	public String getRetrieveCallId() {
		return coteCache.getRetrieveCallId();
	}

	public void setRetrieveCallId(String retrieveCallId) {
		coteCache.setRetrieveCallId(retrieveCallId);
	}

	public File getWorkingDirectory() {
		return coteCache.getWorkingDirectory();
	}

	public void setWorkingDirectory(File workingDirectory) {
		coteCache.setWorkingDirectory(workingDirectory);
	}

	public int getMaxInMemoryLength() {
		return coteCache.getMaxInMemoryLength();
	}

	public void setMaxInMemoryLength(int maxInMemoryLength) {
		coteCache.setMaxInMemoryLength(maxInMemoryLength);
	}

	public int getMaxCacheSize() {
		return coteCache.getMaxCacheSize();
	}

	public void setMaxCacheSize(int maxSize) {
		coteCache.setMaxCacheSize(maxSize);
	}

}
