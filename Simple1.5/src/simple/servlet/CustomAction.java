/*
 * CustomAction.java
 *
 * Created on January 14, 2003, 3:09 PM
 */

package simple.servlet;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** An action that invokes a method on an CustomActionGroup object.
 *
 * @author  Brian S. Krug
 */
public class CustomAction implements Action {
    private static final simple.io.Log log = simple.io.Log.getLog();
    protected Object actions;
    protected Method method;
    protected Object[] args = new Object[4];
	protected boolean xsrfProtected;
    
    /** Creates a new CustomAction
     * @param actions The object that contains the specified method
     * @param method The name of the method to invoke
     */    
    public CustomAction(Object actions, Method method) {
       this.actions = actions;
       this.method = method;
    }
    /**
     * @param dispatcher
     * @param form
     * @param request
     * @param response
     * @throws ServletException
     */    
    public void invoke(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        args[0] = dispatcher;
        args[1] = form;
        args[2] = request;
        args[3] = response;
        try {
            method.invoke(actions,args);
        } catch(IllegalAccessException iae) {
            throw new ServletException("Could not access the method, '" + method.getName() + "', on the object " + actions, iae);
        } catch(IllegalArgumentException ire) {
            throw new ServletException("Internal inconsistency for method, '" + method.getName() + "', on the object " + actions, ire);
        } catch(InvocationTargetException ite) {
            if(ite.getTargetException() instanceof ServletException) throw (ServletException)ite.getTargetException();
            else throw new ServletException(ite.getTargetException() != null ? ite.getTargetException() : ite);
        }        
    }
    
    protected Method getMethod() {
        return method;
    }
    
    protected Object getActions() {
        return actions;
    }
    
	@Override
	public String getAuthorizationRequirements() {
		if(getActions() instanceof SecureCustomActionGroup) {
			String[] requiredPrivileges = ((SecureCustomActionGroup) getActions()).getRequiredPrivileges(getMethod().getName());
			return "the required privilege(s) [" + requiredPrivileges + "]";
		}
		return "none";
	}

	@Override
	public boolean isAuthorized(ServletUser user) {
		if(!isUserRequired())
			return true;
		if(user == null)
			return false;
		if(getActions() instanceof SecureCustomActionGroup) {
			String[] requiredPrivs = ((SecureCustomActionGroup) getActions()).getRequiredPrivileges(getMethod().getName());
			for(String req : requiredPrivs)
				if(!user.hasPrivilege(req))
					return false;
		}
		return true;
    }
    /**
     * @return
     */    
    public boolean isAccessible() {
        if(!(getActions() instanceof CustomActionGroup)) {
            log.info("Request attempted to access class, '" + getActions().getClass().getName() 
                + "', which does not implement simple.servlet.CustomActionGroup and the security is turned on for this servlet");
            return false;
        }
        return true;
    }
    
    /**
     * @return
     */    
    public boolean isUserRequired() {
        return (getActions() instanceof SecureCustomActionGroup);
    }
    
    public boolean isPrivate(){
    	return false;
    }

	public boolean isXsrfProtected() {
		return xsrfProtected;
	}

	public void setXsrfProtected(boolean xsrfProtected) {
		this.xsrfProtected = xsrfProtected;
	}
    
}
