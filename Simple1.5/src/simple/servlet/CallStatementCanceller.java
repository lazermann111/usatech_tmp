/*
 * Created on Feb 24, 2006
 *
 */
package simple.servlet;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import simple.db.Call.CallStatement;
import simple.io.Log;

/**
 * @author bkrug
 *
 */
public class CallStatementCanceller implements HttpSessionBindingListener {
    private static final Log log = Log.getLog();
    protected final Map<Integer,CallStatement> cancellables = new HashMap<Integer,CallStatement>();
    protected final AtomicInteger idGen = new AtomicInteger(0);
    /**
     * 
     */
    public CallStatementCanceller() {
        super();
    }

    /**
     * @see javax.servlet.http.HttpSessionBindingListener#valueBound(javax.servlet.http.HttpSessionBindingEvent)
     */
    public void valueBound(HttpSessionBindingEvent event) {
        // do nothing
    }

    /**
     * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(javax.servlet.http.HttpSessionBindingEvent)
     */
    public void valueUnbound(HttpSessionBindingEvent event) {
        log.debug("Canceller being removed from session");
        cancelAll();
    }
    /**
     * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(javax.servlet.http.HttpSessionBindingEvent)
     */
    public int cancelAll() {
        log.debug("Cancelling " + cancellables.size() + " calls of " + toString());
        // cancel all futures
        int cnt = 0;
        for(Iterator<CallStatement> iter = cancellables.values().iterator(); iter.hasNext(); ) {
            try {
				if(iter.next().cancel()) cnt++;
			} catch(SQLException e) {
				log.warn("Could not cancel", e);
			}
            iter.remove();
        }
        return cnt;
    }
    
    public boolean cancel(int id) throws SQLException {
        log.debug("Cancelling future " + id + " of " + toString());
        // cancel specified future
        CallStatement cs = cancellables.remove(id);
        return (cs != null && cs.cancel());
    }
    public int addCallStatement(CallStatement cs) {
        int id = idGen.incrementAndGet();
        cancellables.put(id, cs);
        log.debug("Future " + cs + " added to Canceller with id = " + id);
        return id;
    }
    public void removeCallStatement(CallStatement cs) {
    	cancellables.values().remove(cs);
        log.debug("Future " + cs + " removed from Canceller");
    }
    public void removeCallStatement(int id) {
    	CallStatement cs = cancellables.remove(id);
        log.debug("Future " + cs + " removed from Canceller");
    }
}
