package simple.servlet;

import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;

import simple.app.ServiceException;
import simple.io.Log;
import simple.translator.DefaultTranslatorFactory;
import simple.translator.Translator;
import simple.translator.TranslatorFactory;
import simple.util.FilterMap;

public class MapInputForm extends AbstractInputForm {
	protected Map<String,Object> map;
	public MapInputForm(Map<String,Object> map) {
		super();
		this.map = map;
	}

	public String getHeader(String name) {
		return (String)map.get("header:" + name);
	}

	public Object getAttribute(String key, String scope) {
		return map.get(key);
	}

	public void setAttribute(String key, Object value, String scope) {
		map.put(key, value);
	}

	public ServletRequest getRequest() {
		return null;
	}

	public Cookie getCookie(String name) {
		return (Cookie)map.get("cookie:" + name);
	}

	public Map<String, Object> getParameters() {
		return new FilterMap<Object>(map,"parameter:", null);
	}

	@Override
	public String addLastModifiedToUri(String path) {
		return RequestUtils.addLastModifiedToUri((RequestInfo) this, getBaseUrl(), path);
	}

	@Override
	public String getServerName() {
		return null;
	}

	@Override
	public String getContextPath() {
		return null;
	}

	@Override
	public String getPathInfo() {
		return null;
	}

	@Override
	public String getRealPath(String webUri) {
		return webUri;
	}

	@Override
	public Locale getLocale() {
		return Locale.getDefault();
	}

	@Override
	public String getBaseUrl() {
		return null;
	}

	@Override
	public Translator getTranslator() {
		Translator translator = (Translator) getAttribute("simple.util.Translator", null);
		if(translator == null) {
			try {
				translator = TranslatorFactory.getDefaultFactory().getTranslator(getLocale(), getServerName());
			} catch(ServiceException e) {
				Log.getLog().warn("Could not get translator", e);
				translator = DefaultTranslatorFactory.getTranslatorInstance();
			}

			setAttribute("simple.util.Translator", translator);
		}
		return translator;
	}

	@Override
	public String getUserAgent() {
		return null;
	}

}
