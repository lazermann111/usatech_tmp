package simple.param;

public class ResultsRadioParamEditor extends ResultsParamEditor {
	public ResultsRadioParamEditor(String pattern) {
		super(pattern);
	}

	public boolean isMultiple() {
		return false;
	}

	public EditorType getEditorType() {
		return EditorType.RADIO;
	}

	public boolean isRequired() {
		return true;
	}
}
