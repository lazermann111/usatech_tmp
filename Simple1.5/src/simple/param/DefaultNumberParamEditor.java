/*
 * Created on Feb 7, 2006
 * 
 */
package simple.param;

import java.math.BigDecimal;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ValueException;
import simple.text.RegexUtils;

/**
 * Modifications:
 * Date			Auth	Bug			Desc
 * ------------------------------------------------------------------------------------
 * 12/28/2009	EPC		DMS	1.0		Wrapped a grouping around "[\\x2D]|to" in default regex. 
 * 
 */

public class DefaultNumberParamEditor extends ParamEditor.NumberParamEditor {
    protected BigDecimal minValue;
    protected BigDecimal maxValue;
    protected int decimalPlaces;
    protected boolean required;
    public DefaultNumberParamEditor(){}
    public DefaultNumberParamEditor(String pattern) {
        super(pattern);
    }
    protected void parsePattern(String pattern) {
        if(pattern != null) {
            pattern = pattern.trim();
            if(pattern.startsWith("?")) {
                required = false;
                pattern = pattern.substring(1);
            } else {
                required = true;
            }
            String[] parts = RegexUtils.match("(?:([\\x2D]?\\d+(?:[.]\\d+)?)\\s*(?:[\\x2D]|to)\\s*([\\x2D]?\\d+(?:[.]\\d+)?))?\\s*(?:[(](\\d+)[)])?", pattern, "i");
            if(parts == null) throw new IllegalArgumentException("Pattern '" + pattern + "' does not conform to required format");
            if(parts.length > 1 && parts[1] != null && parts[1].trim().length() > 0) minValue = new BigDecimal(parts[1]);
            if(parts.length > 2 && parts[2] != null && parts[2].trim().length() > 0) maxValue = new BigDecimal(parts[2]);           
            if(parts.length > 3 && parts[3] != null && parts[3].trim().length() > 0) decimalPlaces = Integer.parseInt(parts[3]);  
            else {
                if(minValue != null) decimalPlaces = minValue.scale();
                if(maxValue != null) decimalPlaces = Math.max(decimalPlaces, maxValue.scale());
            }
        } else {
            required = true;
        }
    }
    public boolean isRequired() {
        return required;
    }
    public void setRequired(boolean required) {
        this.required = required;
    }
    public BigDecimal getMaxValue() {
        return maxValue;
    }
    public void setMaxValue(BigDecimal maxValue) {
        this.maxValue = maxValue;
    }
    public BigDecimal getMinValue() {
        return minValue;
    }
    public void setMinValue(BigDecimal minValue) {
        this.minValue = minValue;
    }
    public int getDecimalPlaces() {
        return decimalPlaces;
    }
    public void setDecimalPlaces(int decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }
    public String getValidRegex() {
        return "^\\d+" + (getDecimalPlaces() > 0 ? "(?:[.]\\d{1," + getDecimalPlaces() + "})?$" : "");//we may need to accomdate group-separators
    }
    @Override
    public void validate(Object value) throws ValueException {
    	BigDecimal bd;
		try {
			bd = ConvertUtils.convert(BigDecimal.class, value);
		} catch(ConvertException e) {
			throw new ValueException.ConversionException(e);
		}
		if(bd == null) {
    		if(isRequired()) throw new ValueException.ValueMissingException();
    	} else if(bd.scale() > getDecimalPlaces()){
    		throw new ValueException.ValueInvalidException("Too precise", value);
    	} else {
    		if(getMinValue() != null && bd.compareTo(getMinValue()) < 0) 
    			throw new ValueException.NumberTooLowException(value, getMinValue());
    		if(getMaxValue() != null && bd.compareTo(getMaxValue()) > 0)
    			throw new ValueException.NumberTooHighException(value, getMaxValue());
    	}
    }
}
