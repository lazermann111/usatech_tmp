/*
 * Created on Feb 7, 2006
 *
 */
package simple.param;

import java.util.LinkedHashMap;
import java.util.Map;

public class DefaultSelectParamEditor extends ParamEditor.SelectParamEditor {
    protected boolean multiple;
    protected boolean required;
	protected Map<String, Option> options;
    public DefaultSelectParamEditor(){}

	public void setOptions(Map<String, Option> options) {
		this.options = options;
	}
	public DefaultSelectParamEditor(String pattern) {
        super(pattern);
    }
    public boolean isMultiple() {
        return multiple;
    }
    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

	public Map<String, Option> getOptions() {
        return options;
    }

    protected void parsePattern(String pattern) {
		options = new LinkedHashMap<String, Option>();
        if(pattern != null) {
            pattern = pattern.trim();
            switch(pattern.charAt(0)) {
                case '?':
                    required = false;
                    multiple = false;
                    pattern = pattern.substring(1);
                    break;
                case '+':
                    required = true;
                    multiple = true;
                    pattern = pattern.substring(1);
                    break;
                case '*':
                    required = false;
                    multiple = true;
                    pattern = pattern.substring(1);
                    break;
                default:
                    required = true;
                    multiple = false;              
            }
			parseOptions(pattern, options);
        } else {
            required = true;
            multiple = false;              
        }
        if(!required && !multiple && !options.containsKey("")) {
			options.put("", makeOption("", "", null));
        }
    }
    
    public boolean isRequired() {
        return required;
    }
    public void setRequired(boolean required) {
        this.required = required;
    }
    public String getValidRegex() {
        return null;//No validation necessary
    }
}
