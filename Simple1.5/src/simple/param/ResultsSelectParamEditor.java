package simple.param;

public class ResultsSelectParamEditor extends ResultsParamEditor {
	protected boolean multiple;
	protected boolean required;
	public ResultsSelectParamEditor(String pattern) {
		super(pattern);
	}

	public EditorType getEditorType() {
		return EditorType.SELECT;
	}

	protected void parsePattern(String pattern) {
		if(pattern != null) {
			pattern = pattern.trim();
			switch(pattern.charAt(0)) {
				case '?':
					required = false;
					multiple = false;
					pattern = pattern.substring(1);
					break;
				case '+':
					required = true;
					multiple = true;
					pattern = pattern.substring(1);
					break;
				case '*':
					required = false;
					multiple = true;
					pattern = pattern.substring(1);
					break;
				default:
					required = true;
					multiple = false;
			}
		} else {
			required = true;
			multiple = false;
		}
		super.parsePattern(pattern);
	}

	public boolean isMultiple() {
		return multiple;
	}

	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
}
