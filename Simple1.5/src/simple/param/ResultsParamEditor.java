package simple.param;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.param.ParamEditor.OptionsParamEditor;
import simple.results.Results;
import simple.text.StringUtils;

public abstract class ResultsParamEditor extends OptionsParamEditor {
	protected String callId;
	protected Map<String, Option> options;
	protected String groupColumn = "group";
	protected String labelColumn = "label";
	protected String valueColumn = "value";

	public ResultsParamEditor(String pattern) {
		super(pattern);
	}

	@Override
	protected void parsePattern(String pattern) {
		this.callId = pattern == null ? null : pattern.trim();
	}

	public Map<String, Option> getOptions() throws SQLException, DataLayerException {
		if(options == null) {
			try (Results results = DataLayerMgr.executeQuery(callId, null)) {
				Map<String, Option> options = new LinkedHashMap<String, Option>();
				while(results.next()) {
					String value = results.getFormattedValue(getValueColumn());
					String group;
					if(!StringUtils.isBlank(getGroupColumn()))
						group = results.getFormattedValue(getGroupColumn());
					else
						group = null;
					String label;
					if(!StringUtils.isBlank(getLabelColumn()))
						label = results.getFormattedValue(getLabelColumn());
					else
						label = null;
					options.put(value, makeOption(value, label, group));
				}
				if(!isRequired() && !isMultiple() && !options.containsKey(""))
					options.put("", makeOption("", "", null));
				this.options = options;
			}
		}
		return options;
	}

	public String getValidRegex() {
		return null;// No validation necessary
	}

	public String getGroupColumn() {
		return groupColumn;
	}

	public void setGroupColumn(String groupColumn) {
		this.groupColumn = groupColumn;
	}

	public String getLabelColumn() {
		return labelColumn;
	}

	public void setLabelColumn(String labelColumn) {
		this.labelColumn = labelColumn;
	}

	public String getValueColumn() {
		return valueColumn;
	}

	public void setValueColumn(String valueColumn) {
		this.valueColumn = valueColumn;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		String old = this.callId;
		this.callId = callId;
		if(!ConvertUtils.areEqual(old, callId))
			this.options = null;
	}

}
