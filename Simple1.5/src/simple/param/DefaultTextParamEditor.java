/*
 * Created on Feb 7, 2006
 *
 */
package simple.param;

import simple.bean.ConvertUtils;
import simple.bean.ValueException;
import simple.text.RegexUtils;

public class DefaultTextParamEditor extends ParamEditor.TextParamEditor {
    protected String validRegex;
    protected int minLength;
    protected int maxLength;
    public DefaultTextParamEditor(){}
    public DefaultTextParamEditor(String pattern) {
        super(pattern);
    }
    @Override
	public boolean isRequired() {
        return getMinLength() > 0;
    }
    @Override
	public int getMaxLength() {
        return maxLength;
    }
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }
    @Override
	public int getMinLength() {
        return minLength;
    }
    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }
    @Override
	public String getValidRegex() {
        return validRegex;
    }
    @Override
    protected void parsePattern(String pattern) {
        if(pattern != null) {
            String[] parts = RegexUtils.match("(?:(\\d+)?\\s*(?:[\\x2D]|to)\\s*(\\d+)?)?(?:[;](.*))?", pattern, "i");
            if(parts == null) throw new IllegalArgumentException("Pattern '" + pattern + "' does not conform to required format");
            if(parts[1] != null && parts[1].trim().length() > 0) minLength = Integer.parseInt(parts[1]);
            if(parts[2] != null && parts[2].trim().length() > 0) maxLength = Integer.parseInt(parts[2]);
            if(parts[3] != null && parts[3].trim().length() > 0) {
                validRegex = parts[3].trim();
            } else if(maxLength > 0) {
                validRegex = "^.{" + minLength + "," + maxLength + "}$";
            } else if(minLength > 0) {
                validRegex = "^.{" + minLength + ",}$";
            }
        }
    }
    @Override
    public void validate(Object value) throws ValueException {
    	String s = ConvertUtils.getStringSafely(value);
    	if(s == null || s.trim().length() == 0) {
    		if(isRequired()) throw new ValueException.ValueMissingException();
    	} else if(getValidRegex() != null && getValidRegex().trim().length() > 0 && !RegexUtils.matches(getValidRegex(), s, null)){
    		throw new ValueException.ValueInvalidException(value);
    	} else if(getMinLength() > 0 && s.length() < getMinLength()) {
    		throw new ValueException.StringTooShortException(value, getMinLength());
    	} else if(getMaxLength() > 0 && s.length() > getMaxLength()) {
    		throw new ValueException.StringTooLongException(value, getMaxLength());
    	}
    }
	public void setValidRegex(String validRegex) {
		this.validRegex = validRegex;
	}
}
