/*
 * Created on Feb 7, 2006
 *
 */
package simple.param;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.bean.ValueException;
import simple.lang.SystemUtils;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;
import simple.xml.XMLizable;

public abstract class ParamEditor implements XMLizable {
	protected static final String[] XML_ATTS = new String[] {"pattern"};

    protected String pattern;
    protected ParamEditor(){}
    protected ParamEditor(String pattern) {
        this.pattern = pattern;
        parsePattern(pattern);
    }
    protected abstract void parsePattern(String pattern) ;

    public abstract boolean isRequired() ;
    public abstract EditorType getEditorType() ;
    public abstract String getValidRegex() ;
    public abstract void validate(Object value) throws ValueException ;

    @Override
	public final String toString() {
		if(pattern == null || pattern.trim().length() == 0)
			return getEditorClassName();
		return getEditorClassName() + ":" + pattern;
    }

	protected String getEditorClassName() {
		return ParamEditorFactory.getEditorClassName(getClass());
    }

	public static interface Option {
		public String getGroup();

		public String getLabel();

		public String getValue();
	}

	public static abstract class OptionsParamEditor extends ParamEditor {
		protected OptionsParamEditor() {
			super();
		}

		protected OptionsParamEditor(String pattern) {
			super(pattern);
		}

		public abstract Map<String, Option> getOptions() throws Exception;

		public abstract boolean isMultiple();

		@Override
		public void validate(Object value) throws ValueException {
			Collection<String> coll;
			try {
				coll = ConvertUtils.asCollection(value, String.class);
			} catch(ConvertException e) {
				throw new ValueException.ConversionException(e);
			}
			if(coll == null || coll.size() == 0) {
				if(isRequired())
					throw new ValueException.ValueMissingException();
			} else if(!isMultiple() && coll.size() > 1) {
				throw new ValueException.ValueInvalidException("Too many values", value);
			} else {
				Map<String, Option> map;
				try {
					map = getOptions();
				} catch(Exception e) {
					throw new ValueException.ValueInvalidException(value, e);
				}
				for(String elem : coll) {
					if(!map.containsKey(elem))
						throw new ValueException.ValueInvalidException(elem);
				}
			}
		}

		protected Option makeOption(final String value, final String label, final String group) {
			return new Option() {
				public String getGroup() {
					return group;
				}

				public String getLabel() {
					return label;
				}

				public String getValue() {
					return value;
				}
			};
		}

		protected void parseOptions(String pattern, Map<String, Option> options) {
			String[] parts = StringUtils.split(pattern, ';', '\'');
			if(parts == null)
				throw new IllegalArgumentException("Pattern '" + pattern + "' does not conform to required format");
			for(String p : parts) {
				if(p.trim().length() > 0) {
					int pos = p.indexOf('=');
					final String value;
					final String label;
					final String group;
					if(pos < 0) {
						value = p;
						label = "";
						group = null;
					} else {
						value = p.substring(0, pos);
						int pos2 = p.indexOf(pos + 1, ':');
						if(pos2 < 0) {
							label = p.substring(pos + 1);
							group = null;
						} else {
							label = p.substring(pos2 + 1);
							group = p.substring(pos + 1, pos2);
						}
					}
					options.put(value, makeOption(value, label, group));
				}
			}
		}
	}
    public static abstract class FreeFormParamEditor extends ParamEditor {
		protected FreeFormParamEditor() {
			super();
    	}
        protected FreeFormParamEditor(String pattern) {
            super(pattern);
        }
        public abstract int getMaxLength() ;
        public abstract int getMinLength() ;
	}

	public static abstract class TextParamEditor extends FreeFormParamEditor {
		protected TextParamEditor() {
			super();
		}

		protected TextParamEditor(String pattern) {
			super(pattern);
		}
        @Override
		public final EditorType getEditorType() {
			return EditorType.TEXT;
        }
    }

	public static abstract class NumberParamEditor extends FreeFormParamEditor {
		protected NumberParamEditor() {
			super();
		}
        protected NumberParamEditor(String pattern) {
            super(pattern);
        }
        public abstract BigDecimal getMinValue() ;
        public abstract BigDecimal getMaxValue();
        public abstract int getDecimalPlaces();
        @Override
		public final EditorType getEditorType() {
			return EditorType.NUMBER;
        }

		@Override
		public int getMaxLength() {
			BigDecimal ubd = getMaxValue();
			BigDecimal lbd = getMaxValue();
			int len = 1;
			int tmp;
			if(ubd != null && (tmp = ubd.precision() - ubd.scale()) > 1)
				len = tmp;
			if(lbd != null && (tmp = lbd.precision() - lbd.scale()) > 1)
				len = tmp;
			if(getDecimalPlaces() > 0)
				len += 1 + getDecimalPlaces();
			if(lbd != null && lbd.signum() < 0)
				len++;
			return len;
		}

		@Override
		public int getMinLength() {
			return isRequired() ? 1 : 0;
		}
    }

	public static abstract class SelectParamEditor extends OptionsParamEditor {
		protected SelectParamEditor() {
			super();
		}
        protected SelectParamEditor(String pattern) {
            super(pattern);
        }

		@Override
		public final EditorType getEditorType() {
			return EditorType.SELECT;
        }
    }

	public static abstract class RadioParamEditor extends OptionsParamEditor {
		protected RadioParamEditor() {
			super();
		}
        protected RadioParamEditor(String pattern) {
            super(pattern);
        }

		@Override
		public final EditorType getEditorType() {
			return EditorType.RADIO;
        }
    }

	public static abstract class CheckboxParamEditor extends OptionsParamEditor {
		protected CheckboxParamEditor() {
			super();
		}
        protected CheckboxParamEditor(String pattern) {
            super(pattern);
        }

		@Override
		public final EditorType getEditorType() {
			return EditorType.CHECKBOX;
        }
    }

	public static abstract class DateParamEditor extends FreeFormParamEditor {
    	protected DateParamEditor(){
			super();
    	}
        protected DateParamEditor(String pattern) {
            super(pattern);
        }
        @Override
		public final EditorType getEditorType() {
			return EditorType.DATE;
        }
        public abstract String getDefaultFormat() ;

		@Override
		public int getMaxLength() {
			return 0;
		}

		@Override
		public int getMinLength() {
			return isRequired() ? 1 : 0;
		}
    }
	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
		parsePattern(pattern);
	}

	protected Set<String> getXMLPropertyNames() throws IntrospectionException {
		Class<?> clazz = getClass();
		while(isCustomParamEditorClass(clazz))
			clazz = clazz.getSuperclass();
		return ReflectionUtils.getPropertyNames(clazz);
	}
	
	protected boolean isCustomParamEditorClass(Class<?> clazz) {
		if(ParamEditor.class.equals(clazz) || ParamEditor.class.equals(clazz.getEnclosingClass()))
				return false;
		return true;
	}
	
	public void writeXML(String tagName, XMLBuilder builder) throws SAXException, ConvertException {
		if(tagName == null)
			tagName = "param-editor";
		builder.elementStart(tagName, XML_ATTS, ParamEditorFactory.toEditorString(this));
		try {
            Set<String> propNames = getXMLPropertyNames();
            if(propNames != null) {
                for(String n : propNames) {
                	if(!n.equals("class")) {
                		builder.put(n, ReflectionUtils.getProperty(this, n));
                    }
                }
            }
        } catch(IntrospectionException e) {
            throw new ConvertException("Could not read properties of object", String.class, this, e);
        } catch (IllegalAccessException e) {
            throw new ConvertException("Could not read properties of object", String.class, this, e);
        } catch (InvocationTargetException e) {
            throw new ConvertException("Could not read properties of object", String.class, this, e);
        } catch(ParseException e) {
            throw new ConvertException("Could not read properties of object", String.class, this, e);
		}
		builder.elementEnd(tagName);
	}

	public static ParamEditor readXML(ObjectBuilder builder) throws SAXException {
		String pattern = builder.getAttrValue(XML_ATTS[0]);
		if(pattern == null || (pattern=pattern.trim()).length() == 0)
			return null;
		try {
			return ParamEditorFactory.createEditor(pattern);
		} catch(ClassCastException e) {
			throw new SAXException(e);
		} catch(IllegalArgumentException e) {
			throw new SAXException(e);
		} catch(ClassNotFoundException e) {
			throw new SAXException(e);
		} catch(InstantiationException e) {
			throw new SAXException(e);
		} catch(IllegalAccessException e) {
			throw new SAXException(e);
		} catch(InvocationTargetException e) {
			throw new SAXException(e);
		}
	}
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hc = getClass().getName().hashCode();
		return SystemUtils.addHashCode(hc, pattern);
	}
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == this) return true;
		if(!(obj instanceof ParamEditor)) return false;
		ParamEditor e = (ParamEditor)obj;
		return getClass().equals(obj.getClass())
			&& ConvertUtils.areEqual(pattern, e.pattern);
	}
}
