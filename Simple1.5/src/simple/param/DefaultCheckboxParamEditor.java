/*
 * Created on Feb 7, 2006
 *
 */
package simple.param;

import java.util.LinkedHashMap;
import java.util.Map;

public class DefaultCheckboxParamEditor extends ParamEditor.CheckboxParamEditor {
    protected boolean required;
	protected Map<String, Option> options;
    public DefaultCheckboxParamEditor(){}

	public void setOptions(Map<String, Option> options) {
		this.options = options;
	}
	public DefaultCheckboxParamEditor(String pattern) {
        super(pattern);
    }
    public boolean isMultiple() {
        return true;
    }
    @Override
	public Map<String, Option> getOptions() {
        return options;
    }

    @Override
	protected void parsePattern(String pattern) {
		options = new LinkedHashMap<String, Option>();
        if(pattern != null) {
            pattern = pattern.trim();
            switch(pattern.charAt(0)) {
                case '?':
                    required = false;
                    pattern = pattern.substring(1);
                    break;
                case '+':
                    required = true;
                    pattern = pattern.substring(1);
                    break;
                case '*':
                    required = false;
                    pattern = pattern.substring(1);
                    break;
                default:
                    required = true;
            }
			parseOptions(pattern, options);
        } else {
            required = true;
        }
    }

    @Override
	public boolean isRequired() {
        return required;
    }
    public void setRequired(boolean required) {
        this.required = required;
    }
    @Override
	public String getValidRegex() {
        return null;//No validation necessary
    }
}
