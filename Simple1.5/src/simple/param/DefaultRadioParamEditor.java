/*
 * Created on Feb 7, 2006
 *
 */
package simple.param;

import java.util.LinkedHashMap;
import java.util.Map;

public class DefaultRadioParamEditor extends ParamEditor.RadioParamEditor {
	protected Map<String, Option> options;
    public DefaultRadioParamEditor(){}

	public void setOptions(Map<String, Option> options) {
		this.options = options;
	}
	public DefaultRadioParamEditor(String pattern) {
        super(pattern);
    }
    @Override
	public Map<String, Option> getOptions() {
        return options;
    }

	@Override
	public boolean isMultiple() {
		return false;
	}
    @Override
	protected void parsePattern(String pattern) {
		options = new LinkedHashMap<String, Option>();
        if(pattern != null) {
            pattern = pattern.trim();
			parseOptions(pattern, options);
        }
    }
	/**
	 * @see simple.param.ParamEditor#isRequired()
	 */
	@Override
	public boolean isRequired() {
		return true;
	}
    @Override
	public String getValidRegex() {
        return null;//No validation necessary
    }
}
