/*
 * Created on Feb 8, 2006
 *
 */
package simple.param;

import java.util.Date;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ValueException;

public class DefaultDateParamEditor extends ParamEditor.DateParamEditor {
    protected boolean required;
    public DefaultDateParamEditor(){
    }
    public DefaultDateParamEditor(String pattern) {
        super(pattern);
    }
    protected void parsePattern(String pattern) {
        if(pattern != null) {
            pattern = pattern.trim();
            if(pattern.startsWith("?")) {
                required = false;
                pattern = pattern.substring(1);
            } else {
                required = true;
            }
        } else {
            required = true;
        }
    }
    public boolean isRequired() {
        return required;
    }
    public void setRequired(boolean required) {
        this.required = required;
    }
    public String getValidRegex() {
        return null; //TODO: figure out all valid combos
    }
	@Override
	public String getDefaultFormat() {
		return pattern;
	}
	@Override
    public void validate(Object value) throws ValueException {
		Date d;
		try {
    		d = ConvertUtils.convert(Date.class, value);
    	} catch(ConvertException e) {
    		throw new ValueException.ConversionException(e);
    	}
		if(d == null) {
    		if(isRequired()) throw new ValueException.ValueMissingException();
    	}
	}
}
