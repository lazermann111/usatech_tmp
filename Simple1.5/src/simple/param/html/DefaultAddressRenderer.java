package simple.param.html;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.GenerateUtils;
import simple.servlet.GenerateUtils.Country;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

public class DefaultAddressRenderer implements AddressRenderer {
	private static final Log log = Log.getLog();
	protected Collection<Country> countries;
	protected String onselectFunction;
	protected boolean showHelp;
	protected String namePrefix;
	protected String labelPrefix;

	public DefaultAddressRenderer() {
		try {
			setCountries(GenerateUtils.getCountries());
		} catch(SQLException e) {
			log.error("Could not get countries", e);
		}
	}

	public DefaultAddressRenderer(Collection<Country> countries) {
		setCountries(countries);
	}

	protected void renderCityControl(Appendable writer, String prefix, String city, boolean readonly) throws IOException {
		writer.append("<input class=\"addressCity\" type=\"text\" name=\"city\" data-validators=\"required\" value=\"");
		writer.append(StringUtils.prepareCDATA(city));
		writer.append("\"");
		if(readonly)
			writer.append(" readonly=\"readonly\"");
		writer.append("/>");
	}

	protected void renderStateControl(Appendable writer, String prefix, String state, boolean readonly) throws IOException {
		writer.append("<input class=\"addressState\" type=\"text\" name=\"");
		if(prefix != null)
			writer.append(prefix);
		writer.append("state\" maxLength=\"2\" data-validators=\"required\" value=\"").append(StringUtils.prepareCDATA(state)).append("\"");
		if(readonly)
			writer.append(" readonly=\"readonly\"");
		writer.append("/>");
	}

	protected void renderPostalControl(Appendable writer, String prefix, String postal, String country, Collection<Country> countries) throws IOException {
		writer.append("<input class=\"addressPostal\" type=\"text\" name=\"");
		if(prefix != null)
			writer.append(prefix);
		try {
			if(country == null || (country = country.trim()).length() == 0 || GenerateUtils.getCountry(country) == null)
				country = countries.iterator().next().code;
		} catch(SQLException e) {
			throw new IOException(e);
		}
		writer.append("postal\" edit-mask=\"Fixed.Postal");
		writer.append(country.toUpperCase());
		writer.append("\" data-validators=\"required validate-postalcode-lookup-");
		writer.append(country.toLowerCase());
		writer.append("\" value=\"");
		writer.append(StringUtils.prepareCDATA(postal));
		writer.append('"');
		if(!StringUtils.isBlank(prefix) || !StringUtils.isBlank(GenerateUtils.getPostalLookupUrl())) {
			writer.append(" data-validator-properties=\"{");
			if(!StringUtils.isBlank(GenerateUtils.getPostalLookupUrl())) {
				writer.append(" url: '");
				writer.append(StringUtils.prepareScript(GenerateUtils.getPostalLookupUrl()));
				writer.append('\'');
				if(!StringUtils.isBlank(prefix))
					writer.append(',');
			}
			if(!StringUtils.isBlank(prefix)) {
				writer.append(" fieldPrefix: '");
				writer.append(StringUtils.prepareScript(prefix));
				writer.append('\'');
			}
			writer.append("}\"");
		}
		writer.append("/>");
	}

	protected void renderCountryControl(Appendable writer, String prefix, String country, Collection<Country> countries, String onselectFunction) throws IOException {
		if(countries == null || countries.isEmpty()) {
			writer.append("<input type=\"text\" name=\"");
			if(prefix != null)
				writer.append(prefix);
			writer.append("country\" data-validators=\"required\" value=\"");
			writer.append(StringUtils.prepareCDATA(country));
			writer.append("\"/>");
		} else {
			writer.append("<select name=\"");
			if(prefix != null)
				writer.append(prefix);
			writer.append("country\" data-validators=\"required\">\n");
			for(Country c : countries) {
				writer.append("<option value=\"");
				writer.append(c.code);
				writer.append("\" onselect=\"App.updateValidation($(this).getParent().form.");
				if(prefix != null)
					writer.append(prefix);
				writer.append("postal, 'required validate-postalcode-lookup-");
				writer.append(c.code.toLowerCase());
				writer.append("', 'Fixed.Postal");
				writer.append(c.code.toUpperCase());
				writer.append("');");
				if(onselectFunction != null && (onselectFunction = onselectFunction.trim()).length() > 0) {
					writer.append(' ');
					writer.append(StringUtils.prepareScript(onselectFunction));
					writer.append("($(this));");
				}
				writer.append('"');
				if(c.code.equalsIgnoreCase(country))
					writer.append(" selected=\"selected\"");
				writer.append(">");
				writer.append(c.name);
				writer.append("</option>\n");
			}
			writer.append("</select>");
		}
	}

	protected void renderHelpButton(Appendable writer) throws IOException {
		writer.append("<span class=\"help-button\"><a onclick=\"postalInstructsDialog.show();\">&#160;</a></span>");
	}

	protected void renderHelpScript(Appendable writer) throws IOException {
		writer.append("<script type=\"text/javascript\">var postalInstructsDialog = new Dialog.Help({ helpKey: 'postal-lookup-instructions', destroyOnClose: false });</script>");
	}

	@Override
	public void render(Appendable writer, String country, String postal, String state, String city, boolean verified) throws IOException {
		renderInMagnitudeFormat(writer, country, postal, state, city, verified);
	}

	protected void renderInAddressFormat(Appendable writer, String prefix, String country, String postal, String state, String city, boolean verified, Collection<Country> countries, String onselectFunction, boolean showHelp) throws IOException {
		writer.append("<table id=\"");
		if(prefix != null)
			writer.append(prefix);
		writer.append("postalLookup\" class=\"addressTable");
		if(!StringUtils.isBlank(postal)) {
			if(verified)
				writer.append(" postal-found");
			else
				writer.append(" postal-not-found");
		}
		writer.append("\"><tr><td>");
		renderCityControl(writer, prefix, city, verified || StringUtils.isBlank(postal));
		writer.append("</td>\n");
		writer.append("<td>,&#160;");
		renderStateControl(writer, prefix, state, verified || StringUtils.isBlank(postal));
		writer.append("</td>\n");
		writer.append("<td>");
		renderPostalControl(writer, prefix, postal, country, countries);
		writer.append("</td>\n");
		writer.append("<td>");
		renderCountryControl(writer, prefix, country, countries, onselectFunction);
		if(showHelp) {
			renderHelpButton(writer);
			writer.append("</td></tr></table>");
			renderHelpScript(writer);
		} else
			writer.append("</td></tr></table>");
	}

	protected void renderInMagnitudeFormat(Appendable writer, String country, String postal, String state, String city, boolean verified) throws IOException {
		Collection<Country> countries = getCountries();
		String onselectFunction = getOnselectFunction();
		boolean showHelp = isShowHelp();
		String namePrefix = getNamePrefix();
		String labelPrefix = getLabelPrefix();
		writer.append("<tbody id=\"");
		if(!StringUtils.isBlank(namePrefix))
			writer.append(namePrefix);
		writer.append("postalLookup\" class=\"addressTable");
		if(!StringUtils.isBlank(postal)) {
			if(verified)
				writer.append(" postal-found");
			else
				writer.append(" postal-not-found");
		}
		writer.append("\"><tr><td>");
		if(!StringUtils.isBlank(labelPrefix))
			writer.append(labelPrefix);
		writer.append("Country:<span class=\"required-note\">*</span></td><td>");
		writer.append("<table><tr><td>");
		renderCountryControl(writer, namePrefix, country, countries, onselectFunction);
		writer.append("</td><td>");
		if(!StringUtils.isBlank(labelPrefix))
			writer.append(labelPrefix);
		writer.append("Postal:<span class=\"required-note\">*</span>");
		writer.append("</td><td>");
		renderPostalControl(writer, namePrefix, postal, country, countries);
		if(showHelp) {
			renderHelpButton(writer);
			renderHelpScript(writer);
		}
		writer.append("</td></tr></table>");
		writer.append("</td></tr>\n<tr><td>");
		if(!StringUtils.isBlank(labelPrefix))
			writer.append(labelPrefix);
		writer.append("City:<span class=\"required-note\">*</span>");
		writer.append("</td><td>");
		writer.append("<table><tr><td>");
		renderCityControl(writer, namePrefix, city, verified || StringUtils.isBlank(postal));
		writer.append("</td><td>");
		if(!StringUtils.isBlank(labelPrefix))
			writer.append(labelPrefix);
		writer.append("State:<span class=\"required-note\">*</span></td><td>");
		renderStateControl(writer, namePrefix, state, verified || StringUtils.isBlank(postal));
		writer.append("</td></tr></table>");
		writer.append("</td></tr></tbody>");
	}

	@Override
	public void render(PageContext pageContext, Map<String, String> defaultValues) throws IOException, ServletException {
		try {
			String namePrefix = getNamePrefix();
			String country = getValue(pageContext, namePrefix, "country", defaultValues);
			String postal = getValue(pageContext, namePrefix, "postal", defaultValues);
			String state = getValue(pageContext, namePrefix, "state", defaultValues);
			String city = getValue(pageContext, namePrefix, "city", defaultValues);
			boolean postalVerified = false;
			if(!StringUtils.isBlank(postal) && !StringUtils.isBlank(country)) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("postalCd", GenerateUtils.getPrimaryPostal(country, postal));
				params.put("countryCd", country);
				Results postalResults = DataLayerMgr.executeQuery("LOOKUP_POSTAL", params);
				if(postalResults.next()) {
					postalVerified = true;
					city = postalResults.getValue("city", String.class);
					state = postalResults.getValue("state", String.class);
				}
			}
			render(pageContext.getOut(), country, postal, state, city, postalVerified);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}

	protected String getValue(PageContext pageContext, String prefix, String name, Map<String, String> defaultValues) throws ServletException {
		String key = (prefix == null ? name : prefix + name);
		String value = ConvertUtils.getStringSafely(RequestUtils.getAttribute(pageContext, key, false));
		if(StringUtils.isBlank(value) && defaultValues != null)
			value = defaultValues.get(key);
		return value;
	}

	public Collection<Country> getCountries() {
		return countries;
	}

	public void setCountries(Collection<Country> countries) {
		this.countries = countries;
	}

	public String getOnselectFunction() {
		return onselectFunction;
	}

	public void setOnselectFunction(String onselectFunction) {
		this.onselectFunction = onselectFunction;
	}

	public boolean isShowHelp() {
		return showHelp;
	}

	public void setShowHelp(boolean showHelp) {
		this.showHelp = showHelp;
	}

	public String getNamePrefix() {
		return namePrefix;
	}

	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}

	public String getLabelPrefix() {
		return labelPrefix;
	}

	public void setLabelPrefix(String labelPrefix) {
		this.labelPrefix = labelPrefix;
	}

}
