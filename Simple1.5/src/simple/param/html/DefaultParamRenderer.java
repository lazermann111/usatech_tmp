package simple.param.html;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.lang.SystemUtils;
import simple.param.ParamEditor;
import simple.param.ParamEditor.DateParamEditor;
import simple.param.ParamEditor.FreeFormParamEditor;
import simple.param.ParamEditor.NumberParamEditor;
import simple.param.ParamEditor.Option;
import simple.param.ParamEditor.OptionsParamEditor;
import simple.text.StringUtils;

public class DefaultParamRenderer implements ParamRenderer {
	protected String beforeLabel = "<tr><th>";
	protected String afterLabel = "</th>";
	protected String beforeControl = "<td>";
	protected String afterControl = "</td></tr>";
	protected String beforeCheckLabel = null;
	protected String afterCheckLabel = null;
	protected String beforeCheckControl = null;
	protected String afterCheckControl = null;
	protected String beforeOption = "";
	protected String afterOption = " ";
	protected String beforeOptionLabel = "";
	protected String afterOptionLabel = " ";
	protected Integer tabIndex = null;
	protected boolean labelAfterCheckControl = false;
	protected int selectAllMinimum = -1;
	protected String onChange = null;

	@Override
	public void render(Appendable out, String name, ParamEditor editor, String label, Object value) throws IOException {
		renderLabel(out, label);
		renderEditor(out, name, editor, label, value);
	}

	protected void renderLabel(Appendable out, String label) throws IOException {
		out.append(getBeforeLabel());
		renderLabelContent(out, label);
		out.append(getAfterLabel());
	}

	protected void renderLabelContent(Appendable out, String label) throws IOException {
		StringUtils.appendHTML(out, label);
	}

	protected void renderEditor(Appendable out, String name, ParamEditor editor, String label, Object value) throws IOException {
		out.append(getBeforeControl());
		renderEditorControl(out, name, editor, label, value);
		out.append(getAfterControl());
	}

	protected void renderEditorControl(Appendable out, String name, ParamEditor editor, String label, Object value) throws IOException {
		String validator;
		switch(editor.getEditorType()) {
			case CHECKBOX:
			case RADIO:
				Collection<String> values;
				if(value == null)
					values = Collections.emptySet();
				else
					try {
						values = ConvertUtils.asCollection(value, String.class);
					} catch(ConvertException e) {
						values = Collections.emptySet();
					}
				renderNonSelectOptions(out, name, (OptionsParamEditor) editor, label, values, editor.getEditorType().toString().toLowerCase());
				break;
			case SELECT:
				try {
					values = ConvertUtils.asCollection(value, String.class);
				} catch(ConvertException e) {
					values = Collections.emptySet();
				}
				renderSelect(out, name, (OptionsParamEditor) editor, label, values);
				break;
			case NUMBER:
				Collection<String> validators = new ArrayList<String>(2);
				NumberParamEditor npe = (NumberParamEditor)editor;
				if(npe.getDecimalPlaces() > 0)
					validators.add("validate-numeric");
				else if(npe.getMinValue() != null && npe.getMinValue().signum() >= 0)
					validators.add("validate-positive-integer");
				else
					validators.add("validate-integer");
				if(npe.getMinValue() != null)
					validators.add("minimum:" + npe.getMinValue());
				if(npe.getMaxValue() != null)
					validators.add("maximum:" + npe.getMaxValue());

				renderText(out, name, npe, label, ConvertUtils.getStringSafely(value), validators.toArray(new String[validators.size()]));
				break;
			case DATE:
				if(!StringUtils.isBlank(editor.getValidRegex()))
					validator = "validate-regex:'" + editor.getValidRegex() + "'"; // TODO: we need to worry about embedded quotes somehow
				else
					validator = null;
				DateParamEditor dpe = (DateParamEditor) editor;
				renderText(out, name, dpe, label, ConvertUtils.formatObject(value, "DATE:" + SystemUtils.nvl(dpe.getDefaultFormat(), "MM/dd/yyyy"), true), validator);
				out.append("<img src=\"./images/icons/calendar.gif\" onclick=\"openCalendar('");
				StringUtils.appendCDATA(out, name);
				out.append("', '");
				StringUtils.appendCDATA(out, label);
				out.append("')\" alt=\"[c]\" />");
				break;
			case TEXT:
			default:
				if(!StringUtils.isBlank(editor.getValidRegex()))
					validator = "validate-regex:'" + editor.getValidRegex() + "'"; // TODO: we need to worry about embedded quotes somehow
				else
					validator = null;
				renderText(out, name, (FreeFormParamEditor) editor, label, ConvertUtils.getStringSafely(value), validator);
				break;

		}
	}

	protected void renderText(Appendable out, String name, FreeFormParamEditor editor, String label, String value, String... validators) throws IOException {
		// TODO: should we support edit-mask also?
		out.append("<input type=\"text\" name=\"");
		StringUtils.appendCDATA(out, name);
		out.append("\"");
		if(tabIndex != null)
			out.append(" tabIndex=\"").append(String.valueOf(tabIndex++)).append('"');
		boolean first = true;
		if(editor.isRequired()) {
			out.append(" data-validators=\"required");
			first = false;
		}
		if(validators != null && validators.length > 0) {
			for(String validator : validators) {
				if(StringUtils.isBlank(validator))
					continue;
				if(first) {
					out.append(" data-validators=\"");
					first = false;
				} else
					out.append(' ');
				StringUtils.appendCDATA(out, validator);
			}
		}
		if(editor.getMinLength() > 1) {
			if(first) {
				out.append(" data-validators=\"");
				first = false;
			} else
				out.append(' ');
			out.append("minLength:").append(String.valueOf(editor.getMinLength()));
		}
		if(editor.getMaxLength() > 0) {
			if(first) {
				out.append(" data-validators=\"");
				first = false;
			} else
				out.append(' ');
			out.append("maxLength:").append(String.valueOf(editor.getMinLength()));
		}
		if(!first)
			out.append('"');
		if(!StringUtils.isBlank(getOnChange())) {
			out.append(" onchange=\"");
			StringUtils.appendScript(out, getOnChange());
			out.append('"');
		}
		out.append(" value=\"");
		StringUtils.appendCDATA(out, value);
		out.append("\"/>");
	}

	protected void renderNonSelectOptions(Appendable out, String name, OptionsParamEditor editor, String label, Collection<String> values, String inputType) throws IOException {
		Map<String, Option> options;
		try {
			options = editor.getOptions();
		} catch(Exception e) {
			if(e instanceof IOException)
				throw (IOException) e;
			throw new IOException(e);
		}
		int count = 0;
		int groupCount = 0;
		String prevGroup = null;
		for(Map.Entry<String, Option> entry : options.entrySet()) {
			String group;
			if(entry.getValue() == null || entry.getValue().getGroup() == null)
				group = null;
			else
				group = entry.getValue().getGroup().trim();
			if(!ConvertUtils.areEqual(prevGroup, group)) {
				if(editor.isMultiple() && getSelectAllMinimum() > 0 && count >= getSelectAllMinimum())
					renderSelectAllButton(out, inputType);
				if(prevGroup != null)
					out.append("</fieldset>");
				count = 0;
				groupCount++;
				if(group != null) {
					out.append("<fieldset class=\"checkbox-grouping\">");
					if(!group.isEmpty()) {
						out.append("<legend>");
						StringUtils.appendHTMLBlank(out, group);
						out.append("</legend>");
					}
				}
				prevGroup = group;
			}
			count++;
			String optionLabel;
			if(!isLabelAfterCheckControl() && entry.getValue() != null && (optionLabel = entry.getValue().getLabel()) != null && !(optionLabel = optionLabel.trim()).isEmpty()) {
				out.append(getBeforeOptionLabel());
				StringUtils.appendHTMLBlank(out, optionLabel);
				out.append(getAfterOptionLabel());
			}
			out.append(getBeforeOption());
			out.append("<input type=\"");
			StringUtils.appendCDATA(out, inputType);
			out.append("\" name=\"");
			StringUtils.appendCDATA(out, name);
			out.append("\" value=\"");
			StringUtils.appendCDATA(out, entry.getKey());
			out.append("\"");
			if(values.contains(entry.getKey()))
				out.append(" checked=\"checked\"");
			if(tabIndex != null)
				out.append(" tabIndex=\"").append(String.valueOf(tabIndex++)).append('"');
			if(editor.isRequired()) {
				out.append(" data-validators=\"validate-reqchk-byname label: '");
				StringUtils.appendCDATA(out, label);
				out.append("'\"");
			}
			if(!StringUtils.isBlank(getOnChange())) {
				out.append(" onchange=\"");
				StringUtils.appendScript(out, getOnChange());
				out.append('"');
			}
			out.append("/>");
			if(isLabelAfterCheckControl() && entry.getValue() != null && (optionLabel = entry.getValue().getLabel()) != null && !(optionLabel = optionLabel.trim()).isEmpty()) {
				out.append(getBeforeOptionLabel());
				StringUtils.appendHTMLBlank(out, optionLabel);
				out.append(getAfterOptionLabel());
			}
			out.append(getAfterOption());
		}
		if(editor.isMultiple() && getSelectAllMinimum() > 0 && count >= getSelectAllMinimum())
			renderSelectAllButton(out, inputType);
		if(prevGroup != null) {
			out.append("</fieldset>");
			if(editor.isMultiple() && getSelectAllMinimum() > 0 && groupCount >= getSelectAllMinimum())
				renderSelectAllButton(out, inputType);
		}
	}

	protected void renderSelectAllButton(Appendable out, String inputType) throws IOException {
		out.append("<input class=\"checkbox-button\" onclick=\"var ops=['Select All','Deselect All'];var i=ops.indexOf(this.value);this.value=ops[(i+1)%2];Array.from($(this).getParent().getElements('input[type=");
		out.append(inputType);
		out.append("]')).each(function(cb) {cb.checked=(i==0);});");
		if(!StringUtils.isBlank(getOnChange())) {
			StringUtils.appendScript(out, getOnChange());
			out.append(';');
		}
		out.append("\" value=\"Select All\" type=\"button\"/>");
	}

	protected void renderSelect(Appendable out, String name, OptionsParamEditor editor, String label, Collection<String> values) throws IOException {
		out.append("<select name=\"");
		StringUtils.appendCDATA(out, name);
		out.append('"');
		if(tabIndex != null)
			out.append(" tabIndex=\"").append(String.valueOf(tabIndex++)).append('"');
		if(editor.isRequired()) {
			out.append(" data-validators=\"required\"");
		}
		if(!StringUtils.isBlank(getOnChange())) {
			out.append(" onchange=\"");
			StringUtils.appendScript(out, getOnChange());
			out.append('"');
		}
		out.append(">");
		Map<String, Option> options;
		try {
			options = editor.getOptions();
		} catch(Exception e) {
			if(e instanceof IOException)
				throw (IOException) e;
			throw new IOException(e);
		}
		String prevGroup = null;
		for(Map.Entry<String, Option> entry : options.entrySet()) {
			String group;
			if(entry.getValue() == null || entry.getValue().getGroup() == null)
				group = null;
			else
				group = entry.getValue().getGroup().trim();
			if(!ConvertUtils.areEqual(prevGroup, group)) {
				if(prevGroup != null)
					out.append("</optgroup>");
				if(group != null) {
					out.append("<optgroup");
					if(!group.isEmpty()) {
						out.append(" label=\"");
						StringUtils.appendCDATA(out, group);
						out.append("\"");
					}
					out.append('>');
				}
				prevGroup = group;
			}
			
			out.append("<option value=\"");
			StringUtils.appendCDATA(out, entry.getKey());
			out.append('"');
			if(values.contains(entry.getKey()))
				out.append(" selected=\"selected\"");
			out.append('>');
			String optionLabel;
			if(entry.getValue() != null && (optionLabel = entry.getValue().getLabel()) != null && !(optionLabel = optionLabel.trim()).isEmpty())
				StringUtils.appendHTMLBlank(out, optionLabel);
			out.append("</option>");
		}
		if(prevGroup != null)
			out.append("</optgroup>");
		out.append("</select>");
	}

	public String getBeforeLabel() {
		return beforeLabel;
	}

	public void setBeforeLabel(String beforeLabel) {
		this.beforeLabel = beforeLabel;
	}

	public String getAfterControl() {
		return afterControl;
	}

	public void setAfterControl(String afterControl) {
		this.afterControl = afterControl;
	}

	public String getAfterLabel() {
		return afterLabel;
	}

	public void setAfterLabel(String afterLabel) {
		this.afterLabel = afterLabel;
	}

	public String getBeforeControl() {
		return beforeControl;
	}

	public void setBeforeControl(String beforeControl) {
		this.beforeControl = beforeControl;
	}

	public String getAfterOption() {
		return afterOption;
	}

	public void setAfterOption(String afterOption) {
		this.afterOption = afterOption;
	}

	public String getBeforeOption() {
		return beforeOption;
	}

	public void setBeforeOption(String beforeOption) {
		this.beforeOption = beforeOption;
	}

	public String getBeforeOptionLabel() {
		return beforeOptionLabel;
	}

	public void setBeforeOptionLabel(String beforeOptionLabel) {
		this.beforeOptionLabel = beforeOptionLabel;
	}

	public String getAfterOptionLabel() {
		return afterOptionLabel;
	}

	public void setAfterOptionLabel(String afterOptionLabel) {
		this.afterOptionLabel = afterOptionLabel;
	}

	public Integer getTabIndex() {
		return tabIndex;
	}

	public void setTabIndex(Integer tabIndex) {
		this.tabIndex = tabIndex;
	}

	public String getBeforeCheckLabel() {
		return beforeCheckLabel == null ? beforeControl : beforeCheckLabel;
	}

	public void setBeforeCheckLabel(String beforeCheckLabel) {
		this.beforeCheckLabel = beforeCheckLabel;
	}

	public String getAfterCheckLabel() {
		return afterCheckLabel == null ? afterControl : afterCheckLabel;
	}

	public void setAfterCheckLabel(String afterCheckLabel) {
		this.afterCheckLabel = afterCheckLabel;
	}

	public String getBeforeCheckControl() {
		return beforeCheckControl == null ? beforeLabel : beforeCheckControl;
	}

	public void setBeforeCheckControl(String beforeCheckControl) {
		this.beforeCheckControl = beforeCheckControl;
	}

	public String getAfterCheckControl() {
		return afterCheckControl == null ? afterLabel : afterCheckControl;
	}

	public void setAfterCheckControl(String afterCheckControl) {
		this.afterCheckControl = afterCheckControl;
	}

	public boolean isLabelAfterCheckControl() {
		return labelAfterCheckControl;
	}

	public void setLabelAfterCheckControl(boolean labelAfterCheckControl) {
		this.labelAfterCheckControl = labelAfterCheckControl;
	}

	public int getSelectAllMinimum() {
		return selectAllMinimum;
	}

	public void setSelectAllMinimum(int selectAllMinimum) {
		this.selectAllMinimum = selectAllMinimum;
	}

	public String getOnChange() {
		return onChange;
	}

	public void setOnChange(String onChange) {
		this.onChange = onChange;
	}
}
