package simple.param.html;

import java.io.IOException;

import simple.param.ParamEditor;

public interface ParamRenderer {
	public void render(Appendable out, String name, ParamEditor editor, String label, Object value) throws IOException;
}
