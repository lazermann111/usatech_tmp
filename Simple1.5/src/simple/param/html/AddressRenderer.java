package simple.param.html;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;


public interface AddressRenderer {
	public void render(Appendable out, String country, String postal, String state, String city, boolean verified) throws IOException;

	public void render(PageContext pageContext, Map<String, String> defaultValues) throws IOException, ServletException;

}
