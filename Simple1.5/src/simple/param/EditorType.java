package simple.param;

public enum EditorType {
	TEXT, DATE, NUMBER, SELECT, CHECKBOX, RADIO
}
