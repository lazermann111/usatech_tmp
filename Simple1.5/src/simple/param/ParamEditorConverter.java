package simple.param;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Formatter;

public class ParamEditorConverter implements Converter<ParamEditor>, Formatter<ParamEditor> {
	@Override
	public String format(ParamEditor object) {
		return object == null ? null : ParamEditorFactory.toEditorString(object);
	}

	@Override
	public ParamEditor convert(Object object) throws Exception {
		return ParamEditorFactory.createEditor(ConvertUtils.getString(object, false));
	}

	@Override
	public Class<ParamEditor> getTargetClass() {
		return ParamEditor.class;
	}
}