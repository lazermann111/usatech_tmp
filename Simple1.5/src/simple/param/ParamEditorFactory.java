/**
 *
 */
package simple.param;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Brian S. Krug
 *
 */
public class ParamEditorFactory {
	protected static final Map<Class<? extends ParamEditor>,String> standarizedTypes = new HashMap<Class<? extends ParamEditor>, String>();
	protected static final Map<String,Class<? extends ParamEditor>> standarizedTypeNames = new HashMap<String,Class<? extends ParamEditor>>();
	protected static final Class<?>[] CONSTRUCTOR_PARAM_TYPES = new Class[] { String.class };

	static {
		registerEditor("DATE", DefaultDateParamEditor.class);
		registerEditor("NUMBER", DefaultNumberParamEditor.class);
		registerEditor("TEXT", DefaultTextParamEditor.class);
		registerEditor("SELECT", DefaultSelectParamEditor.class);
		registerEditor("RADIO", DefaultRadioParamEditor.class);
		registerEditor("CHECKBOX", DefaultCheckboxParamEditor.class);
	}

	public static void registerEditor(String editorClassName, Class<? extends ParamEditor> editorClass) throws IllegalArgumentException {
		try {
			editorClass.getConstructor(CONSTRUCTOR_PARAM_TYPES);
		} catch(SecurityException e) {
			throw new IllegalArgumentException("Class '" + editorClass.getName() + "' does not have a public String argument constructor");
		} catch(NoSuchMethodException e) {
			throw new IllegalArgumentException("Class '" + editorClass.getName() + "' does not have a String argument constructor");
		}
		standarizedTypes.put(editorClass, editorClassName);
		standarizedTypeNames.put(editorClassName, editorClass);
	}
	public static String getEditorClassName(Class<? extends ParamEditor> type) {
		if(type == null)
			return null;
		else if(standarizedTypes.containsKey(type))
			return standarizedTypes.get(type);
		else
			return type.getName();
	}
	public static Class<? extends ParamEditor> getEditorClass(String typeName) throws ClassNotFoundException, ClassCastException {
		if(typeName == null)
			return null;
		else if(standarizedTypeNames.containsKey(typeName))
			return standarizedTypeNames.get(typeName);
		else
			return Class.forName(typeName).asSubclass(ParamEditor.class);
	}
	public static ParamEditor createEditor(String editorString) throws ClassCastException, ClassNotFoundException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		String[] parts = editorString.split("[:]", 2);
        String typeName = parts[0];
        String pattern = parts.length < 2 ? null : parts[1];
        Class<? extends ParamEditor> type = getEditorClass(typeName);
        Constructor<? extends ParamEditor> constructor;
		try {
			constructor = type.getConstructor(CONSTRUCTOR_PARAM_TYPES);
		} catch(SecurityException e) {
			throw new IllegalArgumentException("Class '" + type.getName() + "' does not have a public String argument constructor");
		} catch(NoSuchMethodException e) {
			throw new IllegalArgumentException("Class '" + type.getName() + "' does not have a String argument constructor");
		}
        return constructor.newInstance(pattern);
	}
	public static String toEditorString(ParamEditor editor) {
		if(editor == null)
			return null;
		StringBuilder sb = new StringBuilder();
		sb.append(getEditorClassName(editor.getClass()));
		if(editor.getPattern() != null) {
			sb.append(':').append(editor.getPattern());
		}
		return sb.toString();
	}
}
