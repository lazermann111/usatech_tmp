package simple.param;

import java.util.regex.Pattern;

public class ResultsCheckboxParamEditor extends ResultsParamEditor {
	protected static final Pattern parseRegex = Pattern.compile("(?:~(\\d+))?([?+*]?)(.*)");
	protected boolean required;
	public ResultsCheckboxParamEditor(String pattern) {
		super(pattern);
	}

	protected void parsePattern(String pattern) {
		if(pattern != null) {
			pattern = pattern.trim();
			switch(pattern.charAt(0)) {
				case '?':
					required = false;
					pattern = pattern.substring(1);
					break;
				case '+':
					required = true;
					pattern = pattern.substring(1);
					break;
				case '*':
					required = false;
					pattern = pattern.substring(1);
					break;
				case '~':

				default:
					required = true;
			}
		} else {
			required = true;
		}
		super.parsePattern(pattern);
	}

	public boolean isMultiple() {
		return true;
	}

	public EditorType getEditorType() {
		return EditorType.CHECKBOX;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
}
