/**
 * 
 */
package simple.db.pool;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.WeakHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

import simple.app.ConfigurationUpdaterSupport;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.AbstractDataSource;
import simple.db.DelegatingCallableStatement;
import simple.db.DelegatingConnection;
import simple.db.DelegatingPreparedStatement;
import simple.db.DelegatingStatement;
import simple.db.ExtendedDataSource;
import simple.io.Log;
import simple.lang.Decision;
import simple.lang.SystemUtils;
import simple.text.StringUtils;
import simple.util.concurrent.AbstractLockingPool;
import simple.util.concurrent.CreationException;

public class PoolDataSource extends AbstractDataSource implements ExtendedDataSource, PoolDataSourceMXBean {
	private static final Log log = Log.getLog();
	protected static final AtomicInteger checkIdGenerator = new AtomicInteger();
	protected final String name;
	protected String driverClassName;
    protected boolean driverLoaded = false;
    protected boolean changed = false;
    protected String url;
    protected String username;
    protected String password;
    protected final ReentrantLock lock = new ReentrantLock();
	protected final AtomicInteger lastPreferenceScore = new AtomicInteger();
    protected final ReentrantLock checkLock = new ReentrantLock();
    protected final Condition checkSignal = checkLock.newCondition();
    protected boolean defaultAutoCommit = false;
    protected boolean trackStatements = false;
    protected boolean testOnBorrow = false;
    protected String validationQuery;
    protected long maxAge = 0;
	protected long createTimeout = -1; // if < 1, then use maxWait
    protected int minIdle = 0;
    protected float minForAvailableFactor = 0.5f; // only start giving out connections, when this percentage of minIdle is pre-created
    protected float maxEvictionFactor = 0.25f; // max percentage of idle objects to evict at once
    protected long checkConnectionsInterval = 60000;
    protected final AtomicInteger poolSize = new AtomicInteger();
    protected String propertiesString;
    protected boolean inJNDI = false;
    protected volatile Failure lastCreationFailure = null;
	protected ReborrowProtection reborrowProtection = ReborrowProtection.NONE;
	protected ConfigurationUpdaterSupport configUpdaterSupport;
	protected String[] failoverUrls;

	protected static class ReborrowInfo {
		public final PoolConnection conn;
		public final AtomicInteger reborrowCount = new AtomicInteger(0);

		public ReborrowInfo(PoolConnection conn) {
			this.conn = conn;
		}
	}

	protected final ThreadLocal<ReborrowInfo> borrowedLocal = new ThreadLocal<ReborrowInfo>();
    
    protected static class Failure {
    	public final long time;
    	public final SQLException exception;
		public Failure(long time, SQLException exception) {
			this.time = time;
			this.exception = exception;
		}  	
    }
    protected final Decision<PoolConnection> evictDecision = new Decision<PoolConnection>() {
    	public boolean decide(PoolConnection conn) {
    		return !testConnection(conn, true, true);
    	}
	};
    protected final Properties connectionProperties = new Properties();
    protected final AbstractLockingPool<PoolConnection> pool = new AbstractLockingPool<PoolConnection>() {
		@Override
		protected void closeObject(PoolConnection obj) throws Exception {
			poolSize.decrementAndGet();
			signalCheckConnections(1000L);
			obj.reallyClose();
		}
		@Override
		protected PoolConnection createObject(long timeout) throws CreationException, TimeoutException, InterruptedException {
			try {
				PoolConnection conn = createConnection(timeout);
				poolSize.incrementAndGet();
				return conn;
			} catch(SQLException e) {
				throw new CreationException(e);
			}
		}
    };
    protected final CheckConnectionsThread checkConnectionsThread;
    
    protected class CheckConnectionsThread extends Thread {   
    	protected final AtomicBoolean stopped = new AtomicBoolean(false);
		protected final CountDownLatch runOnceLatch = new CountDownLatch(1);
    	protected Exception lastAddException = null;

    	public CheckConnectionsThread(String name) {
	    	super("PoolDataSource-" + name + "-" + checkIdGenerator.incrementAndGet());
			setDaemon(true);
    	}
    	public void shutdown() {
			if(stopped.compareAndSet(false, true)) {
				interrupt();
			}
		}
    	protected int addMinIdle() {
    		int cnt = 0;
    		// add connections if necessary
			try {
				while(!disabled.get() && getMinIdle() > pool.getNumIdle() && !stopped.get()) {
					if(!pool.addObject())
						break;
					else
						cnt++;
				}
			} catch(CreationException e) {
				if(lastAddException == null  || !SystemUtils.sameThrowable(e, lastAddException)) {
					log.warn("Could not create connections for minIdle of '" + PoolDataSource.this.getName() + "'", e);
					lastAddException = e;
				}
			} catch(TimeoutException e) {
				if(lastAddException == null  || !SystemUtils.sameThrowable(e, lastAddException)) {
					log.warn("Iterrupted while creating connections for minIdle of '" + PoolDataSource.this.getName() + "'", e);
					lastAddException = e;
				}
			} catch(InterruptedException e) {
				if(lastAddException == null  || !SystemUtils.sameThrowable(e, lastAddException)) {
					log.warn("Iterrupted while creating connections for minIdle of '" + PoolDataSource.this.getName() + "'", e);
					lastAddException = e;
				}
			}
			return cnt;
    	}
		public void run() {
			while(!stopped.get()) {
				try {
	    			addMinIdle();
	    			if(stopped.get())
	    				break;
					runOnceLatch.countDown();
	    			// evict connections if necessary
					int n = pool.evict(evictDecision, pool.getNumIdle(), disabled.get() ? 0 : getMinIdle());
	    			if(stopped.get())
	    				break;
	    			// if we evicted any, start again to add more objects
	    			if(n > 0) 
	    				continue;
	    			
	    			if(stopped.get())
	    				break;
	    				
	    			// pause
	    			long interval = getCheckConnectionsInterval();
	    			checkLock.lock();
	        		try {
		    			if(interval > 0)
		    				checkSignal.await(interval, TimeUnit.MILLISECONDS);
		    			else
		    				checkSignal.await();
	    			} catch(InterruptedException e) {
	    				// do nothing
	    			} finally {
	        			checkLock.unlock();
	        		}
    			} catch(RuntimeException e) {
    				log.warn("While checking connections for '" + PoolDataSource.this.getName() + "'", e);
    			} catch(Error e) {
    				log.warn("While checking connections for '" + PoolDataSource.this.getName() + "'", e);
    			} 
    		}	
    	}

		public void awaitRunOnce(long timeout) throws InterruptedException {
			if(timeout == 0L)
				return;
			if(timeout > 0)
				runOnceLatch.await(timeout, TimeUnit.MILLISECONDS);
			else
				runOnceLatch.await();
		}
    }
    protected class PoolConnection extends DelegatingConnection {
    	protected final long createTime;
		protected final int preferenceScore;

		public PoolConnection(Connection delegate, int preferenceScore) {
			super(delegate);
			this.createTime = System.currentTimeMillis();
			this.preferenceScore = preferenceScore;
		}
		@Override
		protected void finalize() throws Throwable {
			// clean up the connection on finalize - this should occur only when a thread is gc'd
			try {
				reallyClose();
			} catch(SQLException e) {
				// Ignore
			}
		}
		@Override
    	public void close() throws SQLException {
    		// Return to pool so connection will be used later
			returnConnection(this);
    	}
		protected void reallyClose() throws SQLException {
    		super.close();
    	}

		public int getPreferenceScore() {
			return preferenceScore;
		}

		public long getCreateTime() {
			return createTime;
		}
		
		public String getDataSourceName() {
			return getName();
		}
	}
    protected class StatementTrackingConnection extends PoolConnection {
    	protected final Map<Statement,Long> statements = new WeakHashMap<Statement,Long>();
    	protected class PoolStatement extends DelegatingStatement {
			public PoolStatement(Statement delegate, Connection connection) {
				super(delegate, connection);
			}
    		@Override
    		public void close() throws SQLException {
    			statements.remove(this);
    			super.close();
    		}
    	}
    	protected class PoolPreparedStatement extends DelegatingPreparedStatement {
			public PoolPreparedStatement(PreparedStatement delegate, Connection connection) {
				super(delegate, connection);
			}
    		@Override
    		public void close() throws SQLException {
    			statements.remove(this);
    			super.close();
    		}
    	}
    	protected class PoolCallableStatement extends DelegatingCallableStatement {
			public PoolCallableStatement(CallableStatement delegate, Connection connection) {
				super(delegate, connection);
			}
    		@Override
    		public void close() throws SQLException {
    			statements.remove(this);
    			super.close();
    		}
    	}

		public StatementTrackingConnection(Connection delegate, int preferenceScore) {
			super(delegate, preferenceScore);
		}
		@Override
    	public void close() throws SQLException {
    		//clean up statements, but don't close connection
    		for(Statement s : statements.keySet())
    			while(s.getMoreResults(Statement.CLOSE_ALL_RESULTS)) ; //s.close();
    		statements.clear();
    	}
		@Override
		public Statement createStatement() throws SQLException {
			Statement s = super.createStatement();
			statements.put(s, System.currentTimeMillis());
			return new PoolStatement(s, this);
		}
		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
			Statement s = delegate.createStatement(resultSetType, resultSetConcurrency);
			statements.put(s, System.currentTimeMillis());
			return new PoolStatement(s, this);
		}
		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			Statement s = delegate.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
			statements.put(s, System.currentTimeMillis());
			return new PoolStatement(s, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql);
			statements.put(ps, System.currentTimeMillis());
			return new PoolPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, autoGeneratedKeys);
			statements.put(ps, System.currentTimeMillis());
			return new PoolPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, resultSetType, resultSetConcurrency);
			statements.put(ps, System.currentTimeMillis());
			return new PoolPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
			statements.put(ps, System.currentTimeMillis());
			return new PoolPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, columnIndexes);
			statements.put(ps, System.currentTimeMillis());
			return new PoolPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, columnNames);
			statements.put(ps, System.currentTimeMillis());
			return new PoolPreparedStatement(ps, this);
		}
		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			CallableStatement cs = delegate.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
			statements.put(cs, System.currentTimeMillis());
			return new PoolCallableStatement(cs, this);
		}
		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			CallableStatement cs = delegate.prepareCall(sql, resultSetType, resultSetConcurrency);
			statements.put(cs, System.currentTimeMillis());
			return new PoolCallableStatement(cs, this);
		}
		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			CallableStatement cs = delegate.prepareCall(sql);
			statements.put(cs, System.currentTimeMillis());
			return new PoolCallableStatement(cs, this);
		}
    }
    
    public PoolDataSource(String name, Map<String,String> props) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
    	this.name = name;
    	this.checkConnectionsThread = new CheckConnectionsThread(name);
    	configure(props);
    }

	protected void signalCheckConnections(long timeout) {
		try {
			if(!checkLock.tryLock(timeout, TimeUnit.MILLISECONDS))
				return;
		} catch(InterruptedException e) {
			return;
		}
		try {
			checkSignal.signal();
		} finally {
			checkLock.unlock();
		}
	}

	protected void configure(Map<String,String> props) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
    	ReflectionUtils.populateProperties(this, props);
    	/*
    	driverClassName = PoolDataSourceFactory.getProperty(props, PoolDataSourceFactory.PROP_DRIVER, "driverClassName");
		url = PoolDataSourceFactory.getProperty(props, PoolDataSourceFactory.PROP_DATABASE, "url");
		username = PoolDataSourceFactory.getProperty(props, PoolDataSourceFactory.PROP_USERNAME, "username");
		password = PoolDataSourceFactory.getProperty(props, PoolDataSourceFactory.PROP_PASSWORD, "password");
		defaultAutoCommit = ConvertUtils.getBooleanSafely(PoolDataSourceFactory.getProperty(props, PoolDataSourceFactory.PROP_AUTOCOMMIT, "defaultAutoCommit"), false);
		trackStatements = ConvertUtils.getBooleanSafely(PoolDataSourceFactory.getProperty(props, "trackStatements"), false);
		validationQuery = PoolDataSourceFactory.getProperty(props, PoolDataSourceFactory.PROP_VALIDATIONQUERY);
		if(validationQuery != null && (validationQuery=validationQuery.trim()).length() > 0) {
			testOnBorrow = ConvertUtils.getBooleanSafely(PoolDataSourceFactory.getProperty(props, PoolDataSourceFactory.PROP_TESTONBORROW), false);
		}
		maxAge = ConvertUtils.getLongSafely(PoolDataSourceFactory.getProperty(props, PoolDataSourceFactory.PROP_MAXAGE), 0);
		String propText = PoolDataSourceFactory.getProperty(props, PoolDataSourceFactory.PROP_CONNECTIONPROPERTIES);
		if(propText != null && propText.trim().length() > 0) {
			try {
				connectionProperties.load(new ByteArrayInputStream(propText.replace(';', '\n').getBytes()));
			} catch(IOException e) {
				PoolDataSourceFactory.log.warn("Could not read connection properties '" + propText + "'; ignoring them.", e);
			}
		}
		if(username != null && username.trim().length() > 0) {
    		connectionProperties.put("user", username);
    		if(password != null && password.trim().length() > 0)
	    		connectionProperties.put("password", password);
    	}
    	*/
    	initialize();
    }
    public void initialize() {
    	checkConnectionsThread.start();
    	if(isInJNDI()) {
    		try {
    			bindJNDIObject(new InitialContext(), "java:/simple/jdbc/" + getName(), this);
    			log.info("Bound PoolDataSource '" + getName() + "' in JNDI to 'java:/simple/jdbc/" + getName() + "'");
    		} catch(NamingException e) {
				log.warn("Could not bind datasource '" + getName() + "' in JNDI", e);
			}
    	}
    }
    protected static void bindJNDIObject(Context ctx, String name, Object object) throws NamingException {
    	String[] parts = StringUtils.split(name, '/');
    	for(int i = 0; i < parts.length - 1; i++) {
    		Object sub;
    		try {
    			sub = ctx.lookup(parts[i]);
    			if(sub instanceof Context)
    				ctx = (Context)sub;
        		else
    				throw new NamingException("Object at '" + StringUtils.join(parts, "/", 0, i+1) + "' is not a Context. It is " + sub);
    		} catch(NameNotFoundException e) {
    			ctx = ctx.createSubcontext(parts[i]);
    		}
    	}
    	ctx.rebind(parts[parts.length-1], object);
    }
	protected void updateProperties() {
		if(username != null && username.trim().length() > 0) {
    		connectionProperties.put("user", username);
    		if(password != null && password.trim().length() > 0)
	    		connectionProperties.put("password", password);
    		else
    			connectionProperties.remove("password");
		} else {
			connectionProperties.remove("user");
			connectionProperties.remove("password");		
		}
	}

    public void reload(Map<String,String> props) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        lock.lock();
        try {
        	String oldDriver = this.driverClassName;
        	String oldDatabase = this.url;
        	String oldUsername = this.username;
        	String oldPassword = this.password;
        	configure(props);
        	if(!ConvertUtils.areEqual(oldDriver,driverClassName)) {
        		driverLoaded = false;
        		changed = true;
        	} else if(!ConvertUtils.areEqual(oldDatabase,url)
                || !ConvertUtils.areEqual(oldUsername,username)
                || !ConvertUtils.areEqual(oldPassword,password)) {
            	changed = true;
            }
        } finally {
            lock.unlock();
        }
    }
    public String getDriverClassName() { return driverClassName; }
    public String getUrl() { return url; }
    public String getUsername() { return username; }
    public String getPassword() { return password; }
	public boolean isDefaultAutoCommit() {
		return defaultAutoCommit;
	}

	public void checkDriver() throws SQLException {
        if(!driverLoaded && driverClassName != null && driverClassName.trim().length() > 0) {
            try {
				Class.forName(driverClassName.trim());
			} catch(ClassNotFoundException e) {
				SQLException sqle = new SQLException("Could not load DriverManager '" + driverClassName.trim() + "'");
				sqle.initCause(e);
				throw sqle;
			}
            driverLoaded = true;
        }
    }

    @Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName()).append(" [DriverClassName=").append(driverClassName).append("; Url=").append(url);
		if(failoverUrls != null && failoverUrls.length > 0)
			sb.append("; failoverUrls=").append(Arrays.toString(failoverUrls));
		sb.append("; Username=").append(username).append("; numActive=").append(getNumActive()).append("; numIdle=").append(getNumIdle()).append(']');
		return sb.toString();
    }
    
    public void clearFailure() {
    	lastCreationFailure = null;
    }
    
	/** Creates the actual PoolConnection
	 * @param timeout
	 * @return
	 * @throws SQLException
	 * @throws InterruptedException
	 * @throws TimeoutException
	 */
	protected PoolConnection createConnection(long timeout) throws SQLException, InterruptedException, TimeoutException {
		if(getCreateTimeout() > 0)
			timeout = getCreateTimeout();
		// If we failed last time to get a connection don't bother trying for timeout number of milliseconds, just re-throw the exception
		Failure lastFailure = lastCreationFailure;
		if(lastFailure != null && lastFailure.time > 0) {
			if(lastFailure.time + timeout > System.currentTimeMillis())
				throw lastFailure.exception;
		    if(!lock.tryLock(timeout, TimeUnit.MILLISECONDS))
		    	throw lastFailure.exception; //throw new TimeoutException("Could not acquire a lock to create the connection for '" + getName() + "' in " + timeout + " milliseconds");
		} else {
			if(!lock.tryLock(timeout, TimeUnit.MILLISECONDS))
				throw new TimeoutException("Could not acquire a lock to create the connection for '" + getName() + "' in " + timeout + " milliseconds");
			if(lastFailure != lastCreationFailure) {
				lastFailure = lastCreationFailure;
				if(lastFailure != null && lastFailure.time + timeout > System.currentTimeMillis()) {
					lock.unlock();
					throw lastFailure.exception;
				}
			}
		}
		int preferenceScore;
	    Connection conn;
        try {
	    	checkDriver();
			long start = log.isInfoEnabled() ? System.currentTimeMillis() : 0L;
			try {
				conn = java.sql.DriverManager.getConnection(getUrl(), connectionProperties);
				preferenceScore = 0;
			} catch(SQLException e) {
				if(failoverUrls == null || failoverUrls.length == 0 || !shouldFailover(e))
					throw e;
				if(log.isInfoEnabled())
					log.info("Could not connect to primary database '" + getUrl() + "' because " + e.getMessage() + " after " + (System.currentTimeMillis() - start) + " ms");
				int i = 0;
				while(true) {
					try {
						if(log.isInfoEnabled())
							start = System.currentTimeMillis();
						conn = java.sql.DriverManager.getConnection(failoverUrls[i], connectionProperties);
						preferenceScore = -1 - i;
						if(log.isInfoEnabled())
							log.info("Successfully connected to secondary database '" + failoverUrls[i] + "' in " + (System.currentTimeMillis() - start) + " ms");
						break;
					} catch(SQLException e1) {
						if(log.isInfoEnabled())
							log.info("Could not connect to secondary database '" + failoverUrls[i] + "' because " + e1.getMessage());
						if(++i >= failoverUrls.length) {
							if(log.isInfoEnabled())
								log.info("Could not connect to any configured databases");
							throw e;
						}
					}
				}
			}
			if(log.isInfoEnabled())
				log.info("Successfully connected to primary database '" + getUrl() + "' in " + (System.currentTimeMillis() - start) + " ms");
			lastPreferenceScore.set(preferenceScore);
	        lastCreationFailure = null;
	    } catch(SQLException e) {
        	lastCreationFailure = new Failure(System.currentTimeMillis(), e);
        	throw e;
	    } finally {
	        lock.unlock();
	    }
		return trackStatements ? new StatementTrackingConnection(conn, preferenceScore) : new PoolConnection(conn, preferenceScore);
	}

	protected boolean shouldFailover(SQLException e) {
		return true;
	}

	protected void returnConnection(PoolConnection poolConnection) {
		switch(getReborrowProtection()) {
			case CURRENT:
				ReborrowInfo rbi = borrowedLocal.get();
				if(rbi != null && rbi.conn == poolConnection && rbi.reborrowCount.getAndDecrement() > 0)
					return;
				// fall-through
			case WARN:
			case ERROR:
				borrowedLocal.set(null);
				break;
		}
		if(isExpired(poolConnection)) {
			if(getNumIdle() >= getMinIdle()) {
				if(log.isInfoEnabled())
					log.info("Evicting expired connection " + poolConnection + " for '" + getName() + "' created at " + new Date(poolConnection.getCreateTime()));
				pool.invalidateObject(poolConnection);
				if(getNumIdle() < getMinIdle())
					signalCheckConnections(0L);
			} else {
				pool.returnObject(poolConnection);
				signalCheckConnections(0L);
			}
		} else {
			pool.returnObject(poolConnection);
		}
	}
	
    protected Connection getConnectionInternal() throws SQLException {
		PoolConnection conn;
		if(getReborrowProtection() != ReborrowProtection.NONE) {
			ReborrowInfo rbi = borrowedLocal.get();
			if(rbi != null) {
				switch(getReborrowProtection()) {
					case WARN:
						if(log.isDebugEnabled())
							log.debug("Thread borrowed a connection from '" + getName() + "' data source more than once; returning different connection", new Throwable());
						break;
					case CURRENT:
						log.warn("Thread borrowed a connection from '" + getName() + "' data source more than once; returning same connection", new Throwable());
						rbi.reborrowCount.incrementAndGet();
						return rbi.conn;
					case ERROR:
					default:
						throw new SQLException("Thread borrowed a connection from '" + getName() + "' data source more than once");
				}
			}
		}
    	while(true) {
    		checkAvailable();
			try {
				conn = pool.borrowObject();
			} catch(TimeoutException e) {
				throw new SQLException("Timeout while waiting for free connection for '" + getName() + "'; " + pool.getNumActive() + " are loaned out", e);
			} catch(CreationException e) {
				if(e.getCause() instanceof SQLException)
					throw (SQLException)e.getCause();
				else
					throw new SQLException("Could not create new connection for '" + getName() + "'; " + pool.getNumActive() + " are loaned out", e);
			} catch(InterruptedException e) {
				throw new SQLException("Interupted while waiting for free connection for '" + getName() + "'; " + pool.getNumActive() + " are loaned out", e);
			}
    		if(testConnection(conn, isTestOnBorrow(), false)) {
    			conn.setAutoCommit(isDefaultAutoCommit());
				if(getReborrowProtection() != ReborrowProtection.NONE)
					borrowedLocal.set(new ReborrowInfo(conn));
    			return conn;
    		} else
    			pool.invalidateObject(conn);
    	}
    }
    
	@Override
	public boolean isAvailable() {
		int minNum = (int) (getMinForAvailableFactor() * getMinIdle());
		if(minNum < 1)
			return true;
		if(minNum > getMinIdle())
			minNum = getMinIdle();
		int ps = poolSize.get();
		if(ps < minNum)
			return false;
		return true;
	}

    protected void checkAvailable() throws SQLException {
		int minNum = (int)(getMinForAvailableFactor() * getMinIdle());
		if(minNum < 1)
			return;
		if(minNum > getMinIdle())
			minNum = getMinIdle();
		int ps = poolSize.get(); 
		if(ps >= minNum)
			return;
		try {
			checkConnectionsThread.awaitRunOnce(-1);
			ps = poolSize.get();
			if(ps >= minNum)
				return;
		} catch(InterruptedException e) {
			// Ignore but continue on to throw exception
		}
		final Failure f = lastCreationFailure;
		throw new SQLException("Not enough connections created for " + name + " [Need " + minNum + "; have " + ps + "]", "08S00", f == null ? null : f.exception);
	}

	protected boolean testConnection(PoolConnection conn, boolean validate, boolean expiration) {
    	try {
			if(conn.isClosed())
				return false;
		} catch(SQLException e) {
			return false;
		}
		if(expiration && isExpired(conn)) {
			if(log.isInfoEnabled())
				log.info("Evicting expired connection " + conn + " for '" + getName() + "' created at " + new Date(conn.getCreateTime()));
			return false;
		}
    	return (!validate || getValidationQuery() == null || isValid(conn));
    }
    
    public void close() {
    	checkConnectionsThread.shutdown();
		pool.close();
	}
    
    @Override
    protected void closeConnections() {
    	pool.clear();
    }
    /**
	 * @param conn
	 * @return
	 */
	protected boolean isExpired(PoolConnection conn) {
		int lps;
		if(maxAge > 0 && conn.getCreateTime() < System.currentTimeMillis() - maxAge) {
			if(log.isInfoEnabled())
				log.info("Connection " + conn + " for '" + getName() + "' created at " + new Date(conn.getCreateTime()) + " has expired");
			return true;
		} else if((lps = lastPreferenceScore.get()) > conn.getPreferenceScore()) {
			if(log.isInfoEnabled())
				log.info("Connection " + conn + " for '" + getName() + "' because it is less preferred (" + conn.getPreferenceScore() + " vs " + lps + ')');
			return true;
		}
		return false;
	}

	/**
	 * @param conn
	 * @return
	 */
	protected boolean isValid(PoolConnection conn) {
		try {
			conn.setAutoCommit(true);
			Statement st = conn.createStatement();
			try {
				boolean hasResults = st.execute(getValidationQuery());
				if(hasResults) {
					ResultSet rs = st.getResultSet();
					try {
						return rs.next();
					} finally {
						rs.close();
					}
				} else {
					return true;
				}
			} finally {
				st.close();
			}
		} catch(SQLException e) {
			if(log.isDebugEnabled())
				log.debug("Connection for '" + getName() + "' is not valid because " + e.getMessage());
			return false;
		}
	}

	protected Connection getConnectionInternal(String user, String pwd) throws SQLException {
		Connection conn = java.sql.DriverManager.getConnection(getUrl(),user,pwd);
    	conn.setAutoCommit(isDefaultAutoCommit());
        return conn;
    }
    public PrintWriter getLogWriter() throws SQLException { return null; }
    public void setLogWriter(PrintWriter out) throws SQLException {}
    public void setLoginTimeout(int seconds) throws SQLException {}
    public int getLoginTimeout() throws SQLException { return 0; }
    @Override
	public void finalize() {
        //don't do this or gc may close our connection while we are using it!
        //try { if(conn != null) conn.close(); } catch(SQLException e) {}
    }
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return false;
	}
	public <T> T unwrap(Class<T> iface) throws SQLException {
		throw new SQLException("Not supported");
	}

	public boolean isTestOnBorrow() {
		return testOnBorrow;
	}

	public void setTestOnBorrow(boolean testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
	}

	public String getValidationQuery() {
		return validationQuery;
	}

	public void setValidationQuery(String validationQuery) {
		if(validationQuery == null || (validationQuery=validationQuery.trim()).length() == 0) {
			setTestOnBorrow(false);
		}
		this.validationQuery = validationQuery;
	}

	public int getMaxActive() {
		return pool.getMaxActive();
	}

	public int getMaxIdle() {
		return pool.getMaxIdle();
	}

	public long getMaxWait() {
		return pool.getMaxWait();
	}

	public void setMaxActive(int maxActive) {
		Object oldValue = pool.getMaxActive();
		pool.setMaxActive(maxActive);
		handlePropertyChange("maxActive", oldValue, maxActive);
	}

	public void setMaxIdle(int maxIdle) {
		Object oldValue = pool.getMaxIdle();
		pool.setMaxIdle(maxIdle);
		handlePropertyChange("maxIdle", oldValue, maxIdle);
	}

	public void setMaxWait(long maxWait) {
		Object oldValue = pool.getMaxWait();
		pool.setMaxWait(maxWait);
		handlePropertyChange("maxWait", oldValue, maxWait);
	}

	public boolean isTrackStatements() {
		return trackStatements;
	}

	public void setTrackStatements(boolean trackStatements) {
		Object oldValue = this.trackStatements;
		this.trackStatements = trackStatements;
		handlePropertyChange("trackStatements", oldValue, trackStatements);
	}

	public long getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(long maxAge) {
		Object oldValue = this.maxAge;
		this.maxAge = maxAge;
		handlePropertyChange("maxAge", oldValue, maxAge);
	}

	public void setDriverClassName(String driverClassName) {
		lock.lock();
        try {
        	String old = this.driverClassName;
	        this.driverClassName = driverClassName;
	        if(!ConvertUtils.areEqual(old, driverClassName)) {
        		driverLoaded = false;
        		changed = true;
        	} 
		} finally {
	    	lock.unlock();
	    }
	}

	public void setUrl(String url) {
		lock.lock();
        try {
        	this.url = url;
        } finally {
        	lock.unlock();
        }
	}

	public void setUsername(String username) {
		lock.lock();
        try {
	        this.username = username;
			updateProperties();
        } finally {
        	lock.unlock();
        }
	}

	public void setPassword(String password) {
		lock.lock();
        try {
	        this.password = password;
			updateProperties();
        } finally {
        	lock.unlock();
        }
	}

	public void setDefaultAutoCommit(boolean autoCommit) {
		this.defaultAutoCommit = autoCommit;
	}
	
	public String getConnectionProperties() {
		return propertiesString;
	}
	
	public void setConnectionProperties(String propertiesString) {
		lock.lock();
        try {
	        this.propertiesString = propertiesString;
			connectionProperties.clear();
			if(propertiesString != null && propertiesString.trim().length() > 0) {
				try {
					connectionProperties.load(new StringReader(propertiesString.replace(';', '\n')));
				} catch(IOException e) {
					log.warn("Could not read connection properties '" + propertiesString + "' for '" + getName() + "'; ignoring them.", e);
				}
			}
			updateProperties();
        } finally {
        	lock.unlock();
        }
	}

	public int getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(int minIdle) {
		Object oldValue = this.minIdle;
		this.minIdle = minIdle;
		handlePropertyChange("minIdle", oldValue, minIdle);
	}

	public long getCheckConnectionsInterval() {
		return checkConnectionsInterval;
	}

	public void setCheckConnectionsInterval(long checkConnectionsInterval) {
		Object oldValue = this.checkConnectionsInterval;
		this.checkConnectionsInterval = checkConnectionsInterval;
		handlePropertyChange("checkConnectionsInterval", oldValue, checkConnectionsInterval);
	}
	
	public long getTimeBetweenEvictionRunsMillis() {
		return getCheckConnectionsInterval();
	}
	
	public void setTimeBetweenEvictionRunsMillis(long timeBetweenEvictionRunsMillis) {
		setCheckConnectionsInterval(timeBetweenEvictionRunsMillis);
	}

	public int getNumActive() {
		return pool.getNumActive();
	}

	public int getNumIdle() {
		return pool.getNumIdle();
	}
	
	public int getNumConnections() {
		return poolSize.get();
	}
	
	public String getCurrentConnectionInfo() {
		return pool.getTracingInfo();
	}
	public boolean isTracingConnections() {
		return pool.isTracing();
	}
	public void setTracingConnections(boolean tracing) {
		Object oldValue = pool.isTracing();
		pool.setTracing(tracing);
		handlePropertyChange("tracing", oldValue, tracing);
	}

	public String getName() {
		return name;
	}

	public float getMinForAvailableFactor() {
		return minForAvailableFactor;
	}

	public void setMinForAvailableFactor(float minForAvailableFactor) {
		if(minForAvailableFactor < 0.0f || minForAvailableFactor > 1.0f)
			throw new IllegalArgumentException("minForAvailableFactor must be between 0 and 1");
		Object oldValue = this.minForAvailableFactor;
		this.minForAvailableFactor = minForAvailableFactor;
		handlePropertyChange("minForAvailableFactor", oldValue, minForAvailableFactor);
	}

	public float getMaxEvictionFactor() {
		return maxEvictionFactor;
	}

	public void setMaxEvictionFactor(float maxEvictionFactor) {
		if(maxEvictionFactor < 0.0f || maxEvictionFactor > 1.0f)
			throw new IllegalArgumentException("maxEvictionFactor must be between 0 and 1");
		float oldValue = this.maxEvictionFactor;
		this.maxEvictionFactor = maxEvictionFactor;
		handlePropertyChange("maxEvictionFactor", oldValue, maxEvictionFactor);
	}

	public boolean isInJNDI() {
		return inJNDI;
	}

	public void setInJNDI(boolean bindingInJNDI) {
		this.inJNDI = bindingInJNDI;
	}

	public ReborrowProtection getReborrowProtection() {
		return reborrowProtection;
	}

	public void setReborrowProtection(ReborrowProtection reborrowProtection) {
		if(reborrowProtection == null)
			reborrowProtection = ReborrowProtection.NONE;
		this.reborrowProtection = reborrowProtection;
	}

	public long getCreateTimeout() {
		return createTimeout;
	}

	public void setCreateTimeout(long createTimeout) {
		Object oldValue = this.createTimeout;
		this.createTimeout = createTimeout;
		handlePropertyChange("createTimeout", oldValue, createTimeout);
	}

	public ConfigurationUpdaterSupport getConfigUpdaterSupport() {
		return configUpdaterSupport;
	}

	public void setConfigUpdaterSupport(ConfigurationUpdaterSupport configUpdaterSupport) {
		this.configUpdaterSupport = configUpdaterSupport;
	}

	protected void handlePropertyChange(String propertyName, Object oldPropertyValue, Object newPropertyValue) {
		ConfigurationUpdaterSupport cus = getConfigUpdaterSupport();
		if(cus != null)
			cus.handlePropertyChange(getName() + '.' + propertyName, oldPropertyValue, newPropertyValue);
	}

	public String[] getFailoverUrls() {
		return failoverUrls;
	}

	public void setFailoverUrls(String[] failoverUrls) {
		this.failoverUrls = failoverUrls;
	}
}