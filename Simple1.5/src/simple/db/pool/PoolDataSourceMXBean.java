package simple.db.pool;

import java.sql.SQLException;

import simple.app.jmx.TrackingDataSourceMXBean;

public interface PoolDataSourceMXBean extends TrackingDataSourceMXBean {
	public int getMaxActive() ;

	public int getMaxIdle() ;

	public long getMaxWait() ;
	
	public int getNumActive() ;

	public int getNumIdle() ;
	
	public int getNumConnections() ;
	
	public int getMinIdle() ;

	public void setMaxActive(int maxActive);

	public void setMaxIdle(int maxIdle);

	public void setMaxWait(long maxWait);

	public void setMinIdle(int minIdle);

	public long getCheckConnectionsInterval();

	public long getCreateTimeout();

	public int getLoginTimeout() throws SQLException;

	public long getMaxAge();

	public float getMaxEvictionFactor();

	public float getMinForAvailableFactor();

	public long getTimeBetweenEvictionRunsMillis();

	public boolean isTracingConnections();

	public boolean isTrackStatements();

	public void setCheckConnectionsInterval(long checkConnectionsInterval);

	public void setCreateTimeout(long createTimeout);

	public void setLoginTimeout(int loginTimeout) throws SQLException;

	public void setMaxAge(long maxAge);

	public void setMaxEvictionFactor(float maxEvictionFactor);

	public void setMinForAvailableFactor(float minForAvailableFactor);

	public void setTimeBetweenEvictionRunsMillis(long timeBetweenEvictionRunsMillis);

	public void setTracingConnections(boolean tracingConnections);

	public void setTrackStatements(boolean trackStatements);

}
