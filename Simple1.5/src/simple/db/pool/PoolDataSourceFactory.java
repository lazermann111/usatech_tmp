/*
 * BasicDataSourceFactory.java
 *
 * Created on March 12, 2003, 9:45 AM
 */

package simple.db.pool;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.apache.commons.configuration.event.ConfigurationListener;

import simple.app.ConfigurationUpdater;
import simple.app.ConfigurationUpdaterSupport;
import simple.bean.ConvertException;
import simple.db.AbstractDataSourceFactory;
import simple.db.DataSourceNotFoundException;
import simple.db.ExtendedDataSource;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveComparator;
import simple.util.FilterMap;

/** Creates and retrieves DataSources from the configurations specified by the
 * properties passed to its constructor. Each DataSource should have the following
 * properties "driverClassName", "url", "username", and "password". Each of these
 * property names are prefixed with the name of the DataSource and a period ("."). The
 * default DataSource (retrieved by passing NULL or "") has no prefix on its properties.
 * Example: the following properties are passed to the constructor of this factory:
 * <P><PRE>DS1.driverClassName=oracle.jdbc.driver.OracleDriver
 * DS1.url=jdbc:oracle:thin:@10.0.0.127:1521:myDataBase
 * DS1.username=myuser
 * DS1.password=mypassword
 * DS2.driverClassName=sun.jdbc.odbc.JdbcOdbcDriver
 * DS2.url=jdbc:odbc:myODBCDataSource
 * DS2.username=myuser2
 * DS2.password=mypassword2<PRE></P>
 * Then two DataSources would be configured on named "DS1" and the other "DS2"
 *
 * @author  Brian S. Krug
 */
public class PoolDataSourceFactory extends AbstractDataSourceFactory implements ConfigurationUpdater {
	private static final Log log = Log.getLog();
    protected final Map<String,String> props = new TreeMap<String,String>(new CaseInsensitiveComparator<String>());
    protected String[] names;
    protected boolean lazyLoad = false;
	protected final ConfigurationUpdaterSupport configUpdaterSupport = new ConfigurationUpdaterSupport();
	protected boolean updateConfig;
    
    public PoolDataSourceFactory() {
    }

    /** Creates a new instance of BasicDataSourceFactory
     * @param paramString A string of properties of the form "property1=value1;property2=value2;..."
     */
    public PoolDataSourceFactory(String paramString) {
        StringTokenizer token =  new StringTokenizer(paramString, ";");
        while(token.hasMoreTokens()) {
            String line = token.nextToken().trim();
            String[] entry = StringUtils.split(line, '=');
            if(entry.length == 2) setProperty(entry[0].trim().toUpperCase(), entry[1]);
        }
        if(!lazyLoad) 
        	loadDataSources();
    }

    /** Creates a new instance of BasicDataSourceFactory with the specified properties
     * defining the set of DataSource's this factory contains.
     * @param properties The properties for configuring this factory
     */
    public PoolDataSourceFactory(Properties properties) {
        for(Object p : properties.keySet()) {
        	String name = (String)p;
        	setProperty(name, properties.getProperty(name));
        }
        if(!lazyLoad) 
        	loadDataSources();
    }

    protected void setProperty(String name, String value) {
    	if(!possiblySetBeanProperty(name,value))
    		props.put(name, value);
    }
    public void addDataSource(String name, String driver, String url, String username, String password) {
    	addDataSource(name, driver, url, username, password, false);
    }

    public void addDataSource(String name, String driver, String url, String username, String password, boolean autoCommit) {
        props.put(name + ".url", url);
        props.put(name + ".driverClassName", driver);
        props.put(name + ".username", username);
        props.put(name + ".password", password);
        props.put(name + ".defaultAutoCommit", String.valueOf(autoCommit));
        if(!lazyLoad && url != null && !url.isEmpty())
			loadDataSource(name);
    }
    protected void loadDataSources() {
		for(String name : getDataSourceNames()) {
			String url = props.get(name == null || name.isEmpty() ? "url" : name + ".url");
			if(url != null && !url.isEmpty())
				loadDataSource(name);
		}
    }
    protected void loadDataSource(String name) {
    	try {
			getDataSource(name);
		} catch(DataSourceNotFoundException e) {
			throw new IllegalStateException("Newly added datasource '" + name + "' not found", e);
		}
    }
	@Override
	protected boolean isCaseSensitive() {
		return false;
	}
    @Override
	protected ExtendedDataSource createDataSource(String name) throws DataSourceNotFoundException {
    	String prefix = (name == null || name.trim().length() == 0 ? "" : name + ".");
    	Map<String, String> map = new FilterMap<String>(props, prefix, null);
		if(StringUtils.isBlank(map.get("url")))
    		throw new DataSourceNotFoundException("Url property is not specified for '" + name + "'");
		PoolDataSource ds;
		try {
			ds = new PoolDataSource(name, map);
		} catch(IntrospectionException e) {
			throw new DataSourceNotFoundException(name, "Could not configure", e);
		} catch(IllegalAccessException e) {
			throw new DataSourceNotFoundException(name, "Could not configure", e);
		} catch(InvocationTargetException e) {
			throw new DataSourceNotFoundException(name, "Could not configure", e);
		} catch(InstantiationException e) {
			throw new DataSourceNotFoundException(name, "Could not configure", e);
		} catch(ConvertException e) {
			throw new DataSourceNotFoundException(name, "Could not configure", e);
		} catch(ParseException e) {
			throw new DataSourceNotFoundException(name, "Could not configure", e);
		}
    	try {
            ds.checkDriver();
        } catch(SQLException e) {
        	ds.close();
    		throw new DataSourceNotFoundException("Could not load Driver Class '" + ds.getDriverClassName() + "'", e);
        }
		if(isUpdateConfig())
			ds.setConfigUpdaterSupport(configUpdaterSupport);

		if(log.isInfoEnabled())
        	log.info("Creating new PoolDataSource with " + ds);
        return ds;
    }

    /**
     * @see simple.db.AbstractDataSourceFactory#getDriverClassName(java.lang.String)
     */
    @Override
    protected String getDriverClassName(String dataSourceName) {
    	String prefix = (dataSourceName == null || dataSourceName.trim().length() == 0 ? "" : dataSourceName + ".");
    	return getProperty(props, prefix + "driverClassName");
    }

    public void refreshProps(Properties properties) {
    	for(Object p : properties.keySet()) {
        	String name = (String)p;
        	setProperty(name, properties.getProperty(name));
        }
        names = null;
        props.keySet().retainAll(properties.keySet());

        for(String name : sources.keySet()) {
        	DataSource ds;
			try {
				ds = sources.getIfPresent(name);
			} catch(DataSourceNotFoundException e) {
				log.warn("This should not occur", e);
				continue;
			}
        	if(ds instanceof PoolDataSource) {
				PoolDataSource pds = (PoolDataSource) ds;
				pds.setConfigUpdaterSupport(isUpdateConfig() ? configUpdaterSupport : null);
        		String prefix = (name == null || name.trim().length() == 0 ? "" : name + ".");
                try {
					pds.reload(new FilterMap<String>(props, prefix, null));
				} catch(IntrospectionException e) {
					log.warn("Could not configure properties for data source '" + name + "'", e);
				} catch(IllegalAccessException e) {
					log.warn("Could not configure properties for data source '" + name + "'", e);
				} catch(InvocationTargetException e) {
					log.warn("Could not configure properties for data source '" + name + "'", e);
				} catch(InstantiationException e) {
					log.warn("Could not configure properties for data source '" + name + "'", e);
				} catch(ConvertException e) {
					log.warn("Could not configure properties for data source '" + name + "'", e);
				} catch(ParseException e) {
					log.warn("Could not configure properties for data source '" + name + "'", e);
				}
        	}
        }
        if(!lazyLoad) 
        	loadDataSources();
    }

    protected static <V> V getProperty(Map<String,V> map, String... names) {
    	for(String s : names) {
    		V p = map.get(s);
    		if(p != null)
    			return p;
    	}
    	return null;
    }
    /** Retrieves an array of the unique names of each <CODE>javax.sql.DataSource</CODE>
     * that this object contains. This implementation looks through the properties
     * passed to its constructor to find any that end with ".DRIVER" or equal "DRIVER".
     * @return An array of Strings that should yield a non-null return from the
     * <CODE>getDataSource()</CODE> method (although an exception might be thrown).
     */
    public String[] getDataSourceNames() {
        if(names == null) {
            final int len = ".driverClassName".length();
            java.util.List<String> l = new java.util.LinkedList<String>();
            for(String n : props.keySet()) {
                if(n.equals("driverClassName")) l.add("");
                else if(n.endsWith(".driverClassName")) 
                	l.add(n.substring(0, n.length() - len));
            }
            names = l.toArray(new String[l.size()]);
        }
        return names;
    }

	public void addConfigurationListener(ConfigurationListener listener) {
		configUpdaterSupport.addConfigurationListener(listener);
	}

	public void clearConfigurationListeners() {
		configUpdaterSupport.clearConfigurationListeners();
	}

	public void removeConfigurationListener(ConfigurationListener listener) {
		configUpdaterSupport.removeConfigurationListener(listener);
	}

	public boolean isUpdateConfig() {
		return updateConfig;
	}

	public void setUpdateConfig(boolean updateConfig) {
		this.updateConfig = updateConfig;
	}
}
