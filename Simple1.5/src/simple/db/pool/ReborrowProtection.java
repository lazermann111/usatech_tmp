package simple.db.pool;

public enum ReborrowProtection {
	NONE, CURRENT, WARN, ERROR
}
