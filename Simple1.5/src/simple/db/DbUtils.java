package simple.db;

import java.sql.Connection;
import java.sql.SQLException;

public class DbUtils {
	public static boolean isClosedSafely(Connection conn) {
		try {
			return conn.isClosed();
		} catch(SQLException e) {
			return true;
		}
	}
	
	public static boolean closeSafely(Connection conn) {
		try {
			conn.close();
			return true;
		} catch(SQLException e) {
			return false;
		}
	}
	
	public static boolean rollbackSafely(Connection conn) {
		try {
			conn.rollback();
			return true;
		} catch(SQLException e) {
			return false;
		}
	}

	public static void commitOrRollback(Connection conn, boolean commit, boolean close) throws SQLException {
		try {
			if(!conn.getAutoCommit()) {
				if(commit)
					conn.commit();
				else
					conn.rollback();
			}
		} finally {
			if(close)
				closeSafely(conn);
		}
	}
}
