/*
 * Cursor.java
 *
 * Created on December 30, 2002, 11:33 AM
 */

package simple.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.app.DialectResolver;
import simple.bean.ConvertUtils;
import simple.results.Results;
import simple.sql.SQLType;

/**
 * Represents a ResultSet object returned from a stored procedure or function
 * @author  Brian S. Krug
 *
 */
public class Cursor implements Argument, Cloneable {
	protected static SQLType CURSOR_SQLTYPE = new SQLType(DialectResolver.isOracle() ? -10 /*oracle.jdbc.driver.OracleTypes.CURSOR*/ : Types.OTHER, "CURSOR");
    /** Holds value of property columns. */
    private Column[] columns;

    /** Holds value of property beanClass. */
    private Class<?> beanClass;

    /** Holds value of property allColumns. */
    private boolean allColumns;

    /** Holds value of property propertyName.  */
    private String propertyName;

    /** Holds value of property groups. */
    private Object[] groups;

    /** Holds value of property totals. */
    private Object[] totals;

    /** Holds value of property fetchSize. */
    private int fetchSize;

    /** Holds value of property lazyAccess. */
    private boolean lazyAccess;

    /** Holds value of property updateable. */
    private boolean updateable;

    private boolean changeable = false;
    private boolean configured = false;

    private boolean caching;

    private final SQLType sqlType = CURSOR_SQLTYPE;

    /** Creates new Cursor */
    public Cursor() {
    }

    public Column[] configureColumns(ResultSet rs) throws SQLException {
        if(changeable) {
            Column[] retColumns = new Column[columns.length];
            for(int i = 0; i < columns.length; i++) {
                retColumns[i] = columns[i].clone();
            }
            return configureColumns(rs, retColumns);
        } else if(!configured) {
            columns = configureColumns(rs, columns);
            configured = true;
        }
        return columns;
    }

    protected Column[] configureColumns(ResultSet rs, Column[] origColumns) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int count = rsmd.getColumnCount();
        if(isAllColumns()) {
            List<Column> colList = new ArrayList<Column>();
            Map<String, Integer> colMap = new HashMap<String, Integer>();
            for(int i = 1; i <= count; i++) {
                String name = rsmd.getColumnName(i);
                Column c = new Column();
                c.setIndex(i);
                c.setName(name);
                c.setPropertyName(name);
                c.setSqlType(rsmd.getColumnType(i));
                try {
                    c.setColumnClass(Class.forName(rsmd.getColumnClassName(i)));
                } catch(ClassCastException e) {
                    // leave it as Object then
                } catch(ClassNotFoundException e) {
                    // leave it as Object then
                } catch(IllegalArgumentException e) {
                    // leave it as Object then
                } catch(NoClassDefFoundError e) {
                    // leave it as Object then
                }
                colMap.put(name, new Integer(i));
                colList.add(c);
            }
            for(int i = 0; origColumns != null && i < origColumns.length; i++) {
                if(origColumns[i] instanceof DerivedColumn) colList.add(origColumns[i]);
                else {
                    String name = origColumns[i].getName();
                    int index;
                    if(name != null) {
                        Integer idx = colMap.get(name);
                        if(idx == null) continue;
                        index = idx.intValue();
                    } else {
                        index = origColumns[i].getIndex();
                    }
                    if(index >= 0 && index < colList.size()) {
                        Column c = colList.get(index);
                        c.setPropertyName(origColumns[i].getPropertyName());
                        c.setFormat(origColumns[i].getFormat());
                    }
                }
            }
            return colList.toArray(new Column[colList.size()]);
        } else {
            if(origColumns != null) {
				for(int i = 0; i < origColumns.length; i++) {
                    if(origColumns[i].getIndex() == -1) {
						try {
							origColumns[i].setIndex(rs.findColumn(origColumns[i].getName())); // throws SQLException if column with the given name is not found
						} catch(SQLException e) {
							StringBuilder sb = new StringBuilder();
							sb.append("Could not find column '").append(origColumns[i].getName()).append("' amoungst ");
							for(int k = 1; k <= count; k++) {
								if(k > 1)
									sb.append(", ");
								sb.append(rsmd.getColumnName(k));
							}
							throw new SQLException(sb.toString(), e);
						}
                    } else if(origColumns[i].getIndex() > count) {
                        throw new SQLException("ResultSet only has " + count + " columns but you configured a column at index " + origColumns[i].getIndex());
                    }
                    origColumns[i].setSqlType(rsmd.getColumnType(origColumns[i].getIndex()));
					String ccn = rsmd.getColumnClassName(origColumns[i].getIndex());
					if(ccn != null)
						try {
							origColumns[i].setColumnClass(Class.forName(ccn));
						} catch(ClassCastException e) {
							// leave it as Object then
						} catch(ClassNotFoundException e) {
							// leave it as Object then
						} catch(IllegalArgumentException e) {
							// leave it as Object then
						} catch(NoClassDefFoundError e) {
							// leave it as Object then
						}
                }
            }
            return origColumns;
        }
    }

    /** If this is an OUT argument.
     * @return Value of property out.
     */
    public boolean isOut() {
        return true;
    }

    /** If this is an IN argument.
     * @return Value of property in.
     */
    public boolean isIn() {
        return false;
    }

    /** Getter for property columns.
     * @return Value of property columns.
     */
    public Column[] getColumns() {
        return columns;
    }

    /** Setter for property columns.
     * @param columns New value of property columns.
     */
    public void setColumns(Column[] columns) {
        this.columns = columns;
    }

    /** Getter for property beanClass.
     * @return Value of property beanClass.
     */
    public Class<?> getBeanClass() {
        return beanClass;
    }

    /** Setter for property beanClass.
     * @param beanClass New value of property beanClass.
     */
    public void setBeanClass(Class<?> beanClass) {
        this.beanClass = beanClass;
    }

    /** Getter for property allColumns.
     * @return Value of property allColumns.
     */
    public boolean isAllColumns() {
        return allColumns;
    }

    /** Setter for property allColumns.
     * @param allColumns New value of property allColumns.
     */
    public void setAllColumns(boolean allColumns) {
        this.allColumns = allColumns;
    }

    /** Getter for property propertyName.
     * @return Value of property propertyName.
     *
     */
    public String getPropertyName() {
        return propertyName;
    }

    /** Setter for property propertyName.
     * @param propertyName New value of property propertyName.
     *
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /** Getter for property groups.
     * @return Value of property groups.
     *
     */
    public Object[] getGroups() {
        return this.groups;
    }

    /** Setter for property groups.
     * @param groups New value of property groups.
     *
     */
    //TODO: change this to an array of columns ???
    public void setGroups(Object[] groups) {
        this.groups = groups;
    }

    @Override
	public String toString() {
        return "Cursor: " + (isAllColumns() ? "ALL" : getColumns().length + " columns");
    }

    /** Getter for property fetchSize.
     * @return Value of property fetchSize.
     *
     */
    public int getFetchSize() {
        return this.fetchSize;
    }

    /** Setter for property fetchSize.
     * @param fetchSize New value of property fetchSize.
     *
     */
    public void setFetchSize(int fetchSize) {
        this.fetchSize = fetchSize;
    }

    /** Getter for property lazyAccess.
     * @return Value of property lazyAccess.
     *
     */
    public boolean isLazyAccess() {
        return this.lazyAccess;
    }

    /** Setter for property lazyLoad.
     * @param lazyLoad New value of property lazyLoad.
     *
     */
    public void setLazyAccess(boolean lazyAccess) {
        this.lazyAccess = lazyAccess;
    }

    /**
     * @return Returns the changeable.
     */
    public boolean isChangeable() {
        return changeable;
    }

    /**
     * @param changeable The changeable to set.
     */
    public void setChangeable(boolean changeable) {
        this.changeable = changeable;
    }

    public Object[] getTotals() {
        return totals;
    }

    public void setTotals(Object[] totals) {
        this.totals = totals;
    }

    @Override
	public Cursor clone() {
		// TODO: we should make copies of all mutable fields
        try {
            return (Cursor)super.clone();
        } catch(CloneNotSupportedException e) {
            return null;
        }
    }
    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof Cursor) {
    		Cursor c = (Cursor)obj;
    		return allColumns == c.allColumns
    			&& ConvertUtils.areEqual(beanClass, c.beanClass)
    			&& ConvertUtils.areEqual(propertyName, c.propertyName)
    			&& Arrays.deepEquals(totals, c.totals)
    			&& Arrays.deepEquals(groups, c.groups)
    			&& Arrays.deepEquals(columns, c.columns);
    	}
    	return false;
    }

	@Override
	public int hashCode() {
		return Arrays.deepHashCode(columns) + (allColumns ? 231 : 0) + (propertyName == null ? 0 : propertyName.hashCode());
	}

	public SQLType getSqlType() {
		return sqlType;
	}

	public Class<? extends Object> getJavaType() {
		return Results.class;
	}

	public boolean isUpdateable() {
		return updateable;
	}

	public void setUpdateable(boolean updateable) {
		this.updateable = updateable;
	}

	public boolean isCaching() {
		return caching;
	}

	public void setCaching(boolean caching) {
		this.caching = caching;
	}

}
