/**
 *
 */
package simple.db;

import java.io.Reader;

/**
 * @author Brian S. Krug
 *
 */
public class BasicCharacterStream implements CharacterStream {
	protected final Reader reader;
	protected final long length;

	public BasicCharacterStream(Reader reader, long length) {
		super();
		this.reader = reader;
		this.length = length;
	}

	/**
	 * @see simple.db.CharacterStream#getLength()
	 */
	public long getLength() {
		return length;
	}

	/**
	 * @see simple.db.CharacterStream#getReader()
	 */
	public Reader getReader() {
		return reader;
	}

}
