/*
 * DataSourceFactory.java
 *
 * Created on March 12, 2003, 9:26 AM
 */

package simple.db;

import java.sql.Connection;


/**
 * Returns DataSource Objects based on the data source name given it.
 * @author  Brian S. Krug
 */
public interface DataSourceFactory {
    /** Returns a DataSource matching the specified name.
     * @return The appropriate <CODE>javax.sql.DataSource</CODE>
     * @param name The unique name of the DataSource
     * @throws DataSourceNotFoundException
     */
    public ExtendedDataSource getDataSource(String name) throws DataSourceNotFoundException;
    /** Returns the class of the unwrapped connection of the data source matching the specified name.
     * @return The class of the "innermost" connection
     * @param name The unique name of the DataSource
     * @throws DataSourceNotFoundException
     */
    public Class<? extends Connection> getConnectionClass(String name) throws DataSourceNotFoundException;
    /** Retrieves an array of the unique names of each <CODE>javax.sql.DataSource</CODE>
     * that this object contains.
     * @return An array of Strings that should yield a non-null return from the
     * <CODE>getDataSource()</CODE> method
     */
    public String[] getDataSourceNames() ;
	/** Whether the specified <CODE>javax.sql.DataSource</CODE> allows dynamic loading of calls versus requiring
	 *  a connection be available on start up of the app
	 * @param dataSourceName
	 * @return
	 */
	public boolean isDynamicLoadingAllowed(String dataSourceName);
	
	public void close() ;
}
