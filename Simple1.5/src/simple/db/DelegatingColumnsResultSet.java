/**
 *
 */
package simple.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;

public class DelegatingColumnsResultSet extends ColumnsResultSet {
	protected final ResultSet delegate;

	public DelegatingColumnsResultSet(ResultSet delegate, ResultSetColumn... resultSetColumns) throws SQLException  {
		super(resultSetColumns);
		this.delegate = delegate;
	}
	public boolean absolute(int row) throws SQLException {
		return delegate.absolute(row);
	}
	public void afterLast() throws SQLException {
		delegate.afterLast();
	}
	public void beforeFirst() throws SQLException {
		delegate.beforeFirst();
	}
	public void cancelRowUpdates() throws SQLException {
		delegate.cancelRowUpdates();
	}
	public void clearWarnings() throws SQLException {
		delegate.clearWarnings();
	}
	public void close() throws SQLException {
		delegate.close();
	}
	public void deleteRow() throws SQLException {
		delegate.deleteRow();
	}
	public boolean first() throws SQLException {
		return delegate.first();
	}
	public int getConcurrency() throws SQLException {
		return delegate.getConcurrency();
	}
	public String getCursorName() throws SQLException {
		return delegate.getCursorName();
	}
	public int getFetchDirection() throws SQLException {
		return delegate.getFetchDirection();
	}
	public int getFetchSize() throws SQLException {
		return delegate.getFetchSize();
	}
	public int getHoldability() throws SQLException {
		return delegate.getHoldability();
	}
	public int getRow() throws SQLException {
		return delegate.getRow();
	}
	public Statement getStatement() throws SQLException {
		return delegate.getStatement();
	}
	public int getType() throws SQLException {
		return delegate.getType();
	}
	public SQLWarning getWarnings() throws SQLException {
		return delegate.getWarnings();
	}
	public void insertRow() throws SQLException {
		delegate.insertRow();
	}
	public boolean isAfterLast() throws SQLException {
		return delegate.isAfterLast();
	}
	public boolean isBeforeFirst() throws SQLException {
		return delegate.isBeforeFirst();
	}
	public boolean isClosed() throws SQLException {
		return delegate.isClosed();
	}
	public boolean isFirst() throws SQLException {
		return delegate.isFirst();
	}
	public boolean isLast() throws SQLException {
		return delegate.isLast();
	}
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return delegate.isWrapperFor(iface);
	}
	public boolean last() throws SQLException {
		return delegate.last();
	}
	public void moveToCurrentRow() throws SQLException {
		delegate.moveToCurrentRow();
	}
	public void moveToInsertRow() throws SQLException {
		delegate.moveToInsertRow();
	}
	public boolean next() throws SQLException {
		return delegate.next();
	}
	public boolean previous() throws SQLException {
		return delegate.previous();
	}
	public void refreshRow() throws SQLException {
		delegate.refreshRow();
	}
	public boolean relative(int rows) throws SQLException {
		return delegate.relative(rows);
	}
	public boolean rowDeleted() throws SQLException {
		return delegate.rowDeleted();
	}
	public boolean rowInserted() throws SQLException {
		return delegate.rowInserted();
	}
	public boolean rowUpdated() throws SQLException {
		return delegate.rowUpdated();
	}
	public void setFetchDirection(int direction) throws SQLException {
		delegate.setFetchDirection(direction);
	}
	public void setFetchSize(int rows) throws SQLException {
		delegate.setFetchSize(rows);
	}
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return delegate.unwrap(iface);
	}
	public void updateRow() throws SQLException {
		delegate.updateRow();
	}

	public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
		return delegate.getObject(columnIndex, type);
	}

	public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
		return delegate.getObject(columnLabel, type);
	}
}