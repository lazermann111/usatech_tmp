/*
 * Created on Dec 6, 2005
 *
 */
package simple.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import simple.results.Results;

public interface CursorTransform {
    public static class Hints {
        public boolean lazyAccess;
        public boolean cacheable;
        public long elapsedTime = -1; //Unknown
        public boolean updateable;
    }
    public Results toResults(Column[] columns, ResultSet rs, DBHelper dbHelper, Hints hints) throws SQLException;
}
