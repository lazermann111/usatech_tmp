/*
 * Created on Dec 6, 2005
 *
 */
package simple.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;

import simple.event.TaskListener;
import simple.io.Log;
import simple.lang.Decision;
import simple.results.CacheableResults;
import simple.results.CachingException;
import simple.results.Results;
import simple.results.ResultsCreationException;
import simple.results.ResultsCreator;

public class CacheableCall extends Call {
    private static final Log log = Log.getLog();
    public static class ExecuteKey {
        protected String url;
        protected Object[] params;
        protected Call call;
        protected int hash;
        public ExecuteKey(Call call, Object[] params, String url) {
            this.call = call;
            this.params = params;
            this.url = url;
            calcHash();
        }
        protected void calcHash() {
            hash = call.hashCode() + url.hashCode() * 1000;
            if(params != null) {
                hash += Arrays.deepHashCode(params) * 10000;
            }

        }
        @Override
        public boolean equals(Object obj) {
            if(!(obj instanceof ExecuteKey)) return false;
            ExecuteKey ek = (ExecuteKey)obj;
            return hash == ek.hash && call.equals(ek.call)
                && Arrays.deepEquals(params, ek.params) && url.equals(ek.url);
        }
        @Override
        public int hashCode() {
            return hash;
        }
    }
    protected Map<ExecuteKey,Object[]> cache;
    protected long minElapsedTime = 2000; // any query that takes less than 2 seconds is not worth caching

    protected class CacheDecision implements Decision<Long> {
    	protected boolean result;
    	public boolean decide(Long elapsedTime) {
    		result = (elapsedTime >= getMinElapsedTime());
    		return result;
    	}
    }
    public CacheableCall(Map<ExecuteKey,Object[]> cache) {
        super();
        this.cache = cache;
    }

    public Object[] executeCall(Connection connection, Object bean, boolean useCache, TaskListener taskListener) throws SQLException, ParameterException {
        if(bean instanceof Object[] || bean == null) return executeCall(connection, (Object[]) bean, useCache, taskListener);
        else {
            Object[] outParams = executeCall(connection, extractParams(bean), useCache, taskListener);
            saveParams(outParams, bean);
            return outParams;
        }
    }

    public Object[] executeCall(Connection connection, Object[] params, boolean useCache, TaskListener taskListener) throws SQLException, ParameterException {
        // create execute key
    	params = normalizeParams(params); //so that params are somewhat "normalized"
        ExecuteKey key = createKey(connection, params);
        // if useCache, then find call
        if(useCache && log.isDebugEnabled())
            log.debug("Looking for " + key + " in results cache");

        Object[] ret = cache.get(key);
        if(ret == null) {
        	// resort to executing the call
            ret = executeMaybeCache(connection, params, key, taskListener);
        } else if(!useCache) {
        	// we are told to overwrite what is in the cache
        	ret = executeAlwaysCache(connection, params, key, taskListener);
        } else {
			log.info("Found call results for " + key + " in results cache");
            Object[] copy = new Object[ret.length];
            try {
                for(int i = 0; i < ret.length; i++) {
                    if(ret[i] instanceof ResultsCreator) {
                        copy[i] = ((ResultsCreator)ret[i]).createResults();
                        if(log.isDebugEnabled()) log.debug("Created results " + copy[i]);
                    } else
                        copy[i] = ret[i];
                }
                ret = copy;
            } catch(ResultsCreationException e) {
                log.warn("Could not create cached results; Executing call instead", e);
                ret = executeAlwaysCache(connection, params, key, taskListener);
            }
        }
        return ret;
    }

    protected Object[] executeAlwaysCache(Connection connection, Object[] params, ExecuteKey key, TaskListener taskListener) throws ParameterException, SQLException {
    	Object[] ret = super.executeCall(connection, params, Call.ALWAYS_CACHE, false, taskListener);
        cacheResults(key, ret);
        return ret;
    }

    protected Object[] executeMaybeCache(Connection connection, Object[] params, ExecuteKey key, TaskListener taskListener) throws ParameterException, SQLException {
        CacheDecision cd = new CacheDecision();
    	Object[] ret = super.executeCall(connection, params, cd, false, taskListener);
        if(cd.result)
        	cacheResults(key, ret);
        return ret;
    }

    protected void cacheResults(ExecuteKey key, Object[] ret) {
    	try {
            Object[] copy = new Object[ret.length];
            for(int i = 0; i < ret.length; i++) {
                if(ret[i] instanceof CacheableResults)
                    copy[i] = ((CacheableResults)ret[i]).getCachedResultsCreator();
                else
                    copy[i] = ret[i];
            }
            log.debug("Adding call execution to results cache at " + key);
            cache.put(key, copy);
        } catch(CachingException e) {
            log.warn("Could not create cached results creator; Not caching this execution", e);
        }
    }
    public Results executeQuery(Connection connection, Object bean, boolean useCache, TaskListener taskListener) throws SQLException, ParameterException {
        Object[] output = executeCall(connection, bean, useCache, taskListener);
        //find the first in the output
        for(int i = 0; i < output.length; i++) if(output[i] instanceof Results) return (Results) output[i];
        return null;
    }

    protected ExecuteKey createKey(Connection connection, Object[] params) throws SQLException {
        return new ExecuteKey(this, params, connection.getMetaData().getURL());
    }

    public long getMinElapsedTime() {
        return minElapsedTime;
    }

    public void setMinElapsedTime(long minElapsedTime) {
        this.minElapsedTime = minElapsedTime;
    }
}
