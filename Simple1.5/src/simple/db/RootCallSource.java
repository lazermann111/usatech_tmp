/*
 * BasicCallSource.java
 *
 * Created on January 20, 2004, 3:31 PM
 */

package simple.db;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import simple.io.Log;

/** The implementation of the CallSource interface that allows sub-CallSource objects to
 * register themselves with this object and delegates the retrieving of Call objects to them.
 *
 * @author  Brian S. Krug
 */
public class RootCallSource implements CallSource {
	private static final Log log = Log.getLog();
    protected Map<String,CallSource> callSources = new HashMap<String,CallSource>();
    protected Map<ReloadableCallSource, Set<String>> reloadableCallSources = new HashMap<ReloadableCallSource, Set<String>>();

    /** Extends CallSource interface to allow reloading of Call Configurations **/
    public static interface ReloadableCallSource extends CallSource {
        /**
         * Reloads the calls in this CallSource if necessary
         * @returns True if calls were changed otherwise false
         */
        public boolean reload() ;

        public void lock() ;

        public void unlock() ;

    }

    /** Creates a new instance of BasicCallSource */
    public RootCallSource() {
    }

    /** Returns a Call object for the specified id value. Coded for thread-safety.
     *  @param id The unique id for the Call
     *  @returns The requested Call object
     *  @throws CallNotFoundException If a Call can not be found for the specified id
     **/
    public Call getCall(String id) throws CallNotFoundException {
        CallSource cs = callSources.get(id);
        if(cs instanceof ReloadableCallSource) {
            ReloadableCallSource rcs = (ReloadableCallSource) cs;
            if(rcs.reload()) {
                rcs.lock();
                try {
                    Set<String> oldCallIds = reloadableCallSources.get(rcs);
                    if(oldCallIds != null)
                    	removeCallSource(oldCallIds, rcs);
                    addCallSource(rcs);
                    reloadableCallSources.put(rcs, new HashSet<String>(rcs.getCallIds()));
                } finally {
                    rcs.unlock();
                }
            }
            rcs.lock();
            try {
                return rcs.getCall(id);
            } finally {
                rcs.unlock();
            }
        } else if(cs == null) {
            //reload all to see if we get this new call
            for(Iterator<Map.Entry<ReloadableCallSource, Set<String>>> iter = reloadableCallSources.entrySet().iterator(); iter.hasNext(); ) {
            	Map.Entry<ReloadableCallSource, Set<String>> entry = iter.next();
                ReloadableCallSource rcs = entry.getKey();
                if(rcs.reload()) {
                    rcs.lock();
                    try {
                        Set<String> oldCallIds = entry.getValue();
                        if(oldCallIds != null)
                            removeCallSource(oldCallIds, rcs);
                        addCallSource(rcs);
                        entry.setValue(new HashSet<String>(rcs.getCallIds()));
                    } finally {
                        rcs.unlock();
                    }
                }
                rcs.lock();
                try {
                    if(rcs.getCallIds().contains(id)) return rcs.getCall(id);
                } finally {
                    rcs.unlock();
                }
            }
            throw new CallNotFoundException(id);
        } else {
            return cs.getCall(id);
        }
    }

    /** Adds the specified CallSource object to the list of underlying CallSources of this root.
     * Not thread-safe.
     * @param source The CallSource object to add
     */
    public void addCallSource(CallSource source) {
        for(String callId : source.getCallIds())
			if(callSources.put(callId, source) != null) {
				log.error("Call '" + callId + "' was already configured. Old definition has been overwritten");
			}
        if(source instanceof ReloadableCallSource)
        	reloadableCallSources.put((ReloadableCallSource)source, source.getCallIds());
    }

    /** Removes the specified CallSource object from the list of underlying CallSources of this root.
     * Not thread-safe.
     * @param source The CallSource object to remove
     */
    public void removeCallSource(CallSource source) {
        //this implementation assumes callIds have not changed since they were added
    	if(source.getCallIds() != null)
            removeCallSource(source.getCallIds(), source);
        if(source instanceof ReloadableCallSource)
        	reloadableCallSources.remove(source);
    }

    /** Removes the specified CallSource object from the list of underlying CallSources of this root.
     * Not thread-safe.
     * @param source The CallSource object to remove
     */
    protected void removeCallSource(Set<String> callIds, CallSource source) {
        //this implementation assumes callIds have not changed since they were added
        for(Iterator<String> iter = callIds.iterator(); iter.hasNext(); ) {
            Object id = iter.next();
            if(callSources.get(id) == source) callSources.remove(id);
        }
    }

    public Set<String> getCallIds() {
        return callSources.keySet();
    }

}
