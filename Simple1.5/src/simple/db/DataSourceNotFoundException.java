/*
 * DataSourceNotFoundException.java
 *
 * Created on March 12, 2003, 9:36 AM
 */

package simple.db;

/** Indicates that a DataSource could not be found for the specified name
 * @author Brian S. Krug
 */
public class DataSourceNotFoundException extends DataLayerException {   
	private static final long serialVersionUID = 5245341345258L;
    /** Constructs an instance of <code>DataSourceNotFoundException</code> with the specified detail message.
     * @param name The name of the DataSource that was request but not found
     */
    public DataSourceNotFoundException(String name) {
        super("Could not find data source, '" + name + "'");
    }
    
    /** Constructs an instance of <code>DataSourceNotFoundException</code> with the specified detail message.
     * @param name The name of the DataSource that was request but not found
     * @param msg the detail message.
     */
    public DataSourceNotFoundException(String name, String msg) {
        super("Could not find data source, '" + name + "': " + msg);
    }
    /** Constructs an instance of <code>DataSourceNotFoundException</code> with the specified detail message.
     * @param name The name of the DataSource that was request but not found
     */
    public DataSourceNotFoundException(String name, Throwable cause) {
        super("Could not find data source, '" + name + "'", cause);
    }
    
    /** Constructs an instance of <code>DataSourceNotFoundException</code> with the specified detail message.
     * @param name The name of the DataSource that was request but not found
     * @param msg the detail message.
     */
    public DataSourceNotFoundException(String name, String msg, Throwable cause) {
        super("Could not find data source, '" + name + "': " + msg, cause);
    }
}
