/**
 *
 */
package simple.db;

import java.io.Reader;
import java.io.StringReader;

/**
 * @author Brian S. Krug
 *
 */
public class StringCharacterStream implements CharacterStream {
	protected final String string;
	protected final long length;

	public StringCharacterStream(String string) {
		super();
		this.string = string;
		this.length = string.length();
	}

	/**
	 * @see simple.db.CharacterStream#getLength()
	 */
	public long getLength() {
		return length;
	}

	/**
	 * @see simple.db.CharacterStream#getReader()
	 */
	public Reader getReader() {
		return new StringReader(string);
	}

}
