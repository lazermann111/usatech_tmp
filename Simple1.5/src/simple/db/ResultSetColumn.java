/**
 * 
 */
package simple.db;

import java.sql.SQLException;

public interface ResultSetColumn {
	public String getColumnName() throws SQLException ;
	public int getSqlType() throws SQLException ;
	public Object getCurrentValue() throws SQLException ;
	public void setCurrentValue(Object object) throws SQLException ;
	public void setCurrentValue(Object object, long scaleOrLength) throws SQLException ;
	public String getCatalogName() throws SQLException ;
	public String getColumnClassName() throws SQLException ;
	public int getColumnDisplaySize() throws SQLException ;
	/**
	 * @return
	 */
	public boolean isWritable() throws SQLException ;
	/**
	 * @return
	 */
	public boolean isSigned() throws SQLException ;
	/**
	 * @return
	 */
	public boolean isSearchable() throws SQLException ;
	/**
	 * @return
	 */
	public boolean isReadOnly() throws SQLException ;
	/**
	 * @return
	 */
	public int isNullable() throws SQLException ;
	/**
	 * @return
	 */
	public String getColumnTypeName() throws SQLException ;
	/**
	 * @return
	 */
	public int getPrecision() throws SQLException ;
	/**
	 * @return
	 */
	public int getScale() throws SQLException ;
	/**
	 * @return
	 */
	public String getSchemaName() throws SQLException ;
	/**
	 * @return
	 */
	public String getTableName() throws SQLException ;
	/**
	 * @return
	 */
	public boolean isAutoIncrement() throws SQLException ;
	/**
	 * @return
	 */
	public boolean isCaseSensitive() throws SQLException ;
	/**
	 * @return
	 */
	public boolean isCurrency() throws SQLException ;
	/**
	 * @return
	 */
	public boolean isDefinitelyWritable() throws SQLException ;
	/**
	 * @return
	 */
	public String getColumnLabel() throws SQLException ;
}