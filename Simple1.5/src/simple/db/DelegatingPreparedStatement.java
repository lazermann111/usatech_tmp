package simple.db;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

public class DelegatingPreparedStatement extends DelegatingStatement implements PreparedStatement {
	protected final PreparedStatement preparedDelegate;
	
	public DelegatingPreparedStatement(PreparedStatement delegate, Connection connection) {
		super(delegate, connection);
		this.preparedDelegate = delegate;
	}

	public void setAsciiStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		preparedDelegate.setAsciiStream(parameterIndex, x, length);
	}

	public void setAsciiStream(int parameterIndex, InputStream x)
			throws SQLException {
		preparedDelegate.setAsciiStream(parameterIndex, x);
	}

	public void setBinaryStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		preparedDelegate.setBinaryStream(parameterIndex, x, length);
	}

	public void setBinaryStream(int parameterIndex, InputStream x)
			throws SQLException {
		preparedDelegate.setBinaryStream(parameterIndex, x);
	}

	public void setBlob(int parameterIndex, InputStream inputStream, long length)
			throws SQLException {
		preparedDelegate.setBlob(parameterIndex, inputStream, length);
	}

	public void setBlob(int parameterIndex, InputStream inputStream)
			throws SQLException {
		preparedDelegate.setBlob(parameterIndex, inputStream);
	}

	public void setCharacterStream(int parameterIndex, Reader reader,
			long length) throws SQLException {
		preparedDelegate.setCharacterStream(parameterIndex, reader, length);
	}

	public void setCharacterStream(int parameterIndex, Reader reader)
			throws SQLException {
		preparedDelegate.setCharacterStream(parameterIndex, reader);
	}

	public void setClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		preparedDelegate.setClob(parameterIndex, reader, length);
	}

	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		preparedDelegate.setClob(parameterIndex, reader);
	}

	public void setNCharacterStream(int parameterIndex, Reader value,
			long length) throws SQLException {
		preparedDelegate.setNCharacterStream(parameterIndex, value, length);
	}

	public void setNCharacterStream(int parameterIndex, Reader value)
			throws SQLException {
		preparedDelegate.setNCharacterStream(parameterIndex, value);
	}

	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		preparedDelegate.setNClob(parameterIndex, value);
	}

	public void setNClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		preparedDelegate.setNClob(parameterIndex, reader, length);
	}

	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		preparedDelegate.setNClob(parameterIndex, reader);
	}

	public void setNString(int parameterIndex, String value)
			throws SQLException {
		preparedDelegate.setNString(parameterIndex, value);
	}

	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		preparedDelegate.setRowId(parameterIndex, x);
	}

	public void setSQLXML(int parameterIndex, SQLXML xmlObject)
			throws SQLException {
		preparedDelegate.setSQLXML(parameterIndex, xmlObject);
	}

	public void addBatch() throws SQLException {
		preparedDelegate.addBatch();
	}

	public void clearParameters() throws SQLException {
		preparedDelegate.clearParameters();
	}

	public boolean execute() throws SQLException {
		return preparedDelegate.execute();
	}

	public ResultSet executeQuery() throws SQLException {
		return new DelegatingResultSet(preparedDelegate.executeQuery(), this);
	}

	public int executeUpdate() throws SQLException {
		return preparedDelegate.executeUpdate();
	}
	public ResultSetMetaData getMetaData() throws SQLException {
		return preparedDelegate.getMetaData();
	}

	public ParameterMetaData getParameterMetaData() throws SQLException {
		return preparedDelegate.getParameterMetaData();
	}

	public void setArray(int i, Array x) throws SQLException {
		preparedDelegate.setArray(i, x);
	}

	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
		preparedDelegate.setAsciiStream(parameterIndex, x, length);
	}

	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		preparedDelegate.setBigDecimal(parameterIndex, x);
	}

	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
		preparedDelegate.setBinaryStream(parameterIndex, x, length);
	}

	public void setBlob(int i, Blob x) throws SQLException {
		preparedDelegate.setBlob(i, x);
	}

	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		preparedDelegate.setBoolean(parameterIndex, x);
	}

	public void setByte(int parameterIndex, byte x) throws SQLException {
		preparedDelegate.setByte(parameterIndex, x);
	}

	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		preparedDelegate.setBytes(parameterIndex, x);
	}

	public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
		preparedDelegate.setCharacterStream(parameterIndex, reader, length);
	}

	public void setClob(int i, Clob x) throws SQLException {
		preparedDelegate.setClob(i, x);
	}

	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
		preparedDelegate.setDate(parameterIndex, x, cal);
	}

	public void setDate(int parameterIndex, Date x) throws SQLException {
		preparedDelegate.setDate(parameterIndex, x);
	}

	public void setDouble(int parameterIndex, double x) throws SQLException {
		preparedDelegate.setDouble(parameterIndex, x);
	}

	public void setFloat(int parameterIndex, float x) throws SQLException {
		preparedDelegate.setFloat(parameterIndex, x);
	}

	public void setInt(int parameterIndex, int x) throws SQLException {
		preparedDelegate.setInt(parameterIndex, x);
	}

	public void setLong(int parameterIndex, long x) throws SQLException {
		preparedDelegate.setLong(parameterIndex, x);
	}

	public void setNull(int paramIndex, int sqlType, String typeName) throws SQLException {
		preparedDelegate.setNull(paramIndex, sqlType, typeName);
	}

	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		preparedDelegate.setNull(parameterIndex, sqlType);
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType, int scale) throws SQLException {
		preparedDelegate.setObject(parameterIndex, x, targetSqlType, scale);
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
		preparedDelegate.setObject(parameterIndex, x, targetSqlType);
	}

	public void setObject(int parameterIndex, Object x) throws SQLException {
		preparedDelegate.setObject(parameterIndex, x);
	}

	public void setRef(int i, Ref x) throws SQLException {
		preparedDelegate.setRef(i, x);
	}

	public void setShort(int parameterIndex, short x) throws SQLException {
		preparedDelegate.setShort(parameterIndex, x);
	}

	public void setString(int parameterIndex, String x) throws SQLException {
		preparedDelegate.setString(parameterIndex, x);
	}

	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
		preparedDelegate.setTime(parameterIndex, x, cal);
	}

	public void setTime(int parameterIndex, Time x) throws SQLException {
		preparedDelegate.setTime(parameterIndex, x);
	}

	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
		preparedDelegate.setTimestamp(parameterIndex, x, cal);
	}

	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		preparedDelegate.setTimestamp(parameterIndex, x);
	}

	@Deprecated
	public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
		preparedDelegate.setUnicodeStream(parameterIndex, x, length);
	}

	public void setURL(int parameterIndex, URL x) throws SQLException {
		preparedDelegate.setURL(parameterIndex, x);
	}
	
}
