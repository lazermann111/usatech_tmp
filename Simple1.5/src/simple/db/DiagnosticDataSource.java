/*
 * Created on Nov 10, 2005
 *
 */
package simple.db;

import java.beans.IntrospectionException;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import simple.app.jmx.DataSourceMXBean;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.lang.SystemUtils;

public class DiagnosticDataSource implements ExtendedDataSource, DataSourceMXBean {
    private static final Log log = Log.getLog();
    protected static final String NEWLINE = SystemUtils.getNewLine();
    protected Map<DiagnosticConnection, String> connections = Collections.synchronizedMap(new WeakHashMap<DiagnosticConnection, String>());
    protected ExtendedDataSource delegate;
    protected boolean retryOnce = true;
    protected final AtomicInteger activeCount = new AtomicInteger();

    public DiagnosticDataSource(ExtendedDataSource delegate) {
        this.delegate = delegate;
    }

    public String getPassword() {
		return delegate.getPassword();
	}

	public String getUrl() {
		return delegate.getUrl();
	}

	public String getUsername() {
		return delegate.getUsername();
	}

	public void setUsername(String username) {
		delegate.setUsername(username);
	}

	public void setPassword(String password) {
		delegate.setPassword(password);
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return delegate.isWrapperFor(iface);
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return delegate.unwrap(iface);
	}
	public void close() throws SQLException {
		delegate.close();
	}

	public boolean isAvailable() {
		return delegate.isAvailable();
	}
	protected void diagnoseDataSourceException(SQLException e) {
        //let's start by just dumping all the threads
        if(log.isDebugEnabled()) {
            log.debug("Diagnosis of DataSource Connection Error ---", e);
            try {
                log.debug("Data Source's Properties: " + ReflectionUtils.toPropertyMap(delegate, Collections.singleton("connection")));
            } catch(IntrospectionException e1) {
            }
            Map<Thread, StackTraceElement[]> dumps = Thread.getAllStackTraces();
            StringBuilder sb = new StringBuilder();
            for(Map.Entry<Thread, StackTraceElement[]> entry : dumps.entrySet()) {
                sb.append("Dump of Thread '").append(entry.getKey().getName()).append("':");
                for(StackTraceElement ste : entry.getValue())
                    sb.append(NEWLINE).append("\tat ").append(ste);
                sb.append(NEWLINE);
            }
            log.debug(sb.toString());
            sb.setLength(0);
            appendConnectionInfo(sb);
            log.debug(sb.toString());
        }
    }

    public void appendConnectionInfo(StringBuilder sb) {
    	DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
    	sb.append("Active Connections: ").append(activeCount).append(NEWLINE);
        sb.append("Connections: ").append(NEWLINE).append("N \tConnected              \tClosed                 \tThread                 \tStatus     ");
        sb.append(NEWLINE).append("--\t-----------------------\t-----------------------\t-----------------------\t---------------");
        int n = 1;
        Map.Entry<?,?>[] entries = connections.entrySet().toArray(new Map.Entry<?,?>[0]);
        for(Map.Entry<?,?> entry : entries) {
        	DiagnosticConnection key = (DiagnosticConnection)entry.getKey();
        	sb.append(NEWLINE).append(n++).append("\t").append(df.format(new Date(key.connectTime))).append("\t");
            if(key.closeTime != 0)
                sb.append(df.format(new Date(key.closeTime)));
            else
                sb.append("NOT CLOSED");
            sb.append("\t").append(key.threadName).append("\t");
            Connection conn = key.realConnection.get();
            if(conn == null) {
            	sb.append("Finalized");
            } else {
	            try {
					sb.append(conn.isClosed() ? "Really Closed" : "Still Open");
				} catch(SQLException e) {
					sb.append("ERROR on isClosed()");
				}
				sb.append(" (").append(conn.getClass()).append(')');
            }
        }
    }

    protected boolean needReconnect(SQLException e) {
        switch(e.getErrorCode()) {
            case -2048:
            case -2063:
            case -2068:
            case -3113:
            case -1000:
            case 17002:
                return true;
            default:
                return false;
        }
    }

    protected void closeConnection(Connection conn) {
        try {
            DataLayerMgr.getDBHelper(conn).closeConnection(conn);
        } catch(SQLException e) {
            log.info("Could not close connection", e);
        }
    }

    /**
     * @see javax.sql.DataSource#getConnection()
     */
    public Connection getConnection() throws SQLException {
    	try {
            return new DiagnosticConnection(delegate.getConnection());
        } catch(SQLException e) {
        	try {
	        	if(retryOnce)
	        		return new DiagnosticConnection(delegate.getConnection());
	        	else
	        		throw e;
        	} catch(SQLException e1) {
            	diagnoseDataSourceException(e);
            	throw e;
        	}
        }
    }

    /**
     * @see javax.sql.DataSource#getConnection(java.lang.String, java.lang.String)
     */
    public Connection getConnection(String username, String password) throws SQLException {
    	try {
            return new DiagnosticConnection(delegate.getConnection(username, password));
        } catch(SQLException e) {
        	try {
	        	if(retryOnce)
	        		return new DiagnosticConnection(delegate.getConnection(username, password));
	        	else
	        		throw e;
        	} catch(SQLException e1) {
            	diagnoseDataSourceException(e);
            	throw e;
        	}
        }
    }

    /**
     * @see javax.sql.DataSource#getLoginTimeout()
     */
    public int getLoginTimeout() throws SQLException {
        return delegate.getLoginTimeout();
    }

    /**
     * @see javax.sql.DataSource#getLogWriter()
     */
    public PrintWriter getLogWriter() throws SQLException {
        return delegate.getLogWriter();
    }

    /**
     * @see javax.sql.DataSource#setLoginTimeout(int)
     */
    public void setLoginTimeout(int seconds) throws SQLException {
        delegate.setLoginTimeout(seconds);
    }

    /**
     * @see javax.sql.DataSource#setLogWriter(java.io.PrintWriter)
     */
    public void setLogWriter(PrintWriter out) throws SQLException {
        delegate.setLogWriter(out);
    }
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DiagnosticDataSource on " + delegate;
    }

    public class DiagnosticConnection extends DelegatingConnection {
        protected long connectTime;
        protected long closeTime;
        protected String threadName;
        protected WeakReference<Connection> realConnection;

        public DiagnosticConnection(Connection delegate) {
            super(delegate);
            this.connectTime = System.currentTimeMillis();
            this.threadName = Thread.currentThread().getName();
            this.realConnection = new WeakReference<Connection>(DBUnwrap.getRealConnection(delegate));
            connections.put(this, threadName);
            int n = activeCount.incrementAndGet();
            if(log.isDebugEnabled())
            	log.debug("Created connection " + delegate + "; " + n + " connections now active");
        }
        protected void diagnoseConnectionException(SQLException e) {
            if(needReconnect(e)) {
                log.debug("Closing connection " + delegate + " because of following SQLException", e);
                closeConnection(delegate);
            }
        }
        @Override
		public void clearWarnings() throws SQLException {
            try {
                super.clearWarnings();
            } catch(SQLException e) {
                diagnoseConnectionException(e);
                throw e;
            }
        }
        @Override
		public void close() throws SQLException {
            closeTime = System.currentTimeMillis();
            int n = activeCount.decrementAndGet();
            if(log.isDebugEnabled())
            	log.debug("Closing connection " + delegate + "; " + n + " connections now active");
            super.close();
            if(log.isDebugEnabled())
            	log.debug("Closed connection " + delegate);
            
        }
        @Override
		public void commit() throws SQLException {
        	if(log.isDebugEnabled())
            	log.debug("Commiting connection " + delegate);
            try {
                super.commit();
            } catch(SQLException e) {
                diagnoseConnectionException(e);
                throw e;
            }
            if(log.isDebugEnabled())
            	log.debug("Commit Complete on connection " + delegate);    
        }
        @Override
		public Statement createStatement() throws SQLException {
            return new DiagnosticStatement(delegate.createStatement(), this);
        }
        @Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return new DiagnosticStatement(delegate.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability), this);
        }
        @Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
            return new DiagnosticStatement(delegate.createStatement(resultSetType, resultSetConcurrency), this);
        }
        @Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return new DiagnosticCallableStatement(delegate.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability), this);
        }
        @Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return new DiagnosticCallableStatement(delegate.prepareCall(sql, resultSetType, resultSetConcurrency), this);
        }
        @Override
		public CallableStatement prepareCall(String sql) throws SQLException {
            return new DiagnosticCallableStatement(delegate.prepareCall(sql), this);
        }
        @Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return new DiagnosticPreparedStatement(delegate.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability), this);
        }
        @Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return new DiagnosticPreparedStatement(delegate.prepareStatement(sql, resultSetType, resultSetConcurrency), this);
        }
        @Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
            return new DiagnosticPreparedStatement(delegate.prepareStatement(sql, autoGeneratedKeys), this);
        }
        @Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
            return new DiagnosticPreparedStatement(delegate.prepareStatement(sql, columnIndexes), this);
        }
        @Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
            return new DiagnosticPreparedStatement(delegate.prepareStatement(sql, columnNames), this);
        }
        @Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
            return new DiagnosticPreparedStatement(delegate.prepareStatement(sql), this);
        }
        @Override
		public void rollback() throws SQLException {
        	if(log.isDebugEnabled())
            	log.debug("Rolling back connection " + delegate);
            try {
                super.rollback();
            } catch(SQLException e) {
                diagnoseConnectionException(e);
                throw e;
            }
            if(log.isDebugEnabled())
            	log.debug("Rollback Complete on connection " + delegate); 
        }
        @Override
		public void rollback(Savepoint savepoint) throws SQLException {
        	if(log.isDebugEnabled())
            	log.debug("Rolling back to savepoint connection " + delegate);try {
                super.rollback(savepoint);
            } catch(SQLException e) {
                diagnoseConnectionException(e);
                throw e;
            }
            if(log.isDebugEnabled())
            	log.debug("Rollback to savepoint Complete on connection " + delegate); 
        }
    }

    public class DiagnosticStatement extends DelegatingStatement {
        public DiagnosticStatement(Statement delegate, Connection connection) {
            super(delegate, connection);
        }
        protected void diagnoseStatementException(SQLException e) {
            if(needReconnect(e)) {
                log.debug("Closing connection " + connection + " because of following SQLException", e);
                closeConnection(connection);
            }
        }

        @Override
		public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
            try {
                return super.execute(sql, autoGeneratedKeys);
            } catch(SQLException e) {
            	// to do this right, we must re-connect and then copy the statement attributes to a new statement - ugh!
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql, autoGeneratedKeys);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public boolean execute(String sql, int[] columnIndexes) throws SQLException {
            try {
            	return super.execute(sql, columnIndexes);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql, columnIndexes);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public boolean execute(String sql, String[] columnNames) throws SQLException {
            try {
            	return super.execute(sql, columnNames);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql, columnNames);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public boolean execute(String sql) throws SQLException {
            try {
            	return super.execute(sql);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int[] executeBatch() throws SQLException {
            try {
            	return super.executeBatch();
            } catch(SQLException e) {
            	/* We CANNOT retry because executeBatch() clears out the current batch parameters even upon failure
            	try {
    	        	if(retryOnce)
    	        		return super.executeBatch();
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}*/
            	throw e;
            }
        }

        @Override
		public ResultSet executeQuery(String sql) throws SQLException {
            try {
            	return super.executeQuery(sql);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeQuery(sql);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
            try {
            	return super.executeUpdate(sql, autoGeneratedKeys);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql, autoGeneratedKeys);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
            try {
            	return super.executeUpdate(sql, columnIndexes);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql, columnIndexes);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql, String[] columnNames) throws SQLException {
            try {
            	return super.executeUpdate(sql, columnNames);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql, columnNames);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql) throws SQLException {
            try {
            	return super.executeUpdate(sql);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }
    }

    public class DiagnosticPreparedStatement extends DelegatingPreparedStatement {
        public DiagnosticPreparedStatement(PreparedStatement delegate, Connection connection) {
            super(delegate, connection);
        }
        protected void diagnoseStatementException(SQLException e) {
            if(needReconnect(e)) {
                log.debug("Closing connection " + connection + " because of following SQLException", e);
                closeConnection(connection);
           }
        }

        @Override
		public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
            try {
                return super.execute(sql, autoGeneratedKeys);
            } catch(SQLException e) {
            	// to do this right, we must re-connect and then copy the statement attributes to a new statement - ugh!
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql, autoGeneratedKeys);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public boolean execute(String sql, int[] columnIndexes) throws SQLException {
            try {
            	return super.execute(sql, columnIndexes);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql, columnIndexes);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public boolean execute(String sql, String[] columnNames) throws SQLException {
            try {
            	return super.execute(sql, columnNames);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql, columnNames);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public boolean execute(String sql) throws SQLException {
            try {
            	return super.execute(sql);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int[] executeBatch() throws SQLException {
            try {
            	return super.executeBatch();
            } catch(SQLException e) {
            	/* We CANNOT retry because executeBatch() clears out the current batch parameters even upon failure
            	try {
    	        	if(retryOnce)
    	        		return super.executeBatch();
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}*/
            	throw e;
            }
        }

        @Override
		public ResultSet executeQuery(String sql) throws SQLException {
            try {
            	return super.executeQuery(sql);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeQuery(sql);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
            try {
            	return super.executeUpdate(sql, autoGeneratedKeys);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql, autoGeneratedKeys);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
            try {
            	return super.executeUpdate(sql, columnIndexes);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql, columnIndexes);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql, String[] columnNames) throws SQLException {
            try {
            	return super.executeUpdate(sql, columnNames);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql, columnNames);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql) throws SQLException {
            try {
            	return super.executeUpdate(sql);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }
        public boolean execute() throws SQLException {
            try {
	            return super.execute();
	        } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute();
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
	        }
        }
        public ResultSet executeQuery() throws SQLException {
            try {
	            return super.executeQuery();
	        } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeQuery();
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
	        }
        }
        public int executeUpdate() throws SQLException {
            try {
	            return super.executeUpdate();
	        } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate();
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
	        }
        }
    }
    public class DiagnosticCallableStatement extends DelegatingCallableStatement {
        public DiagnosticCallableStatement(CallableStatement delegate, Connection connection) {
            super(delegate, connection);
        }
        protected void diagnoseStatementException(SQLException e) {
            if(needReconnect(e)) {
                log.debug("Closing connection " + connection + " because of following SQLException", e);
                closeConnection(connection);
            }
        }

        @Override
		public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
            try {
                return super.execute(sql, autoGeneratedKeys);
            } catch(SQLException e) {
            	// to do this right, we must re-connect and then copy the statement attributes to a new statement - ugh!
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql, autoGeneratedKeys);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public boolean execute(String sql, int[] columnIndexes) throws SQLException {
            try {
            	return super.execute(sql, columnIndexes);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql, columnIndexes);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public boolean execute(String sql, String[] columnNames) throws SQLException {
            try {
            	return super.execute(sql, columnNames);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql, columnNames);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public boolean execute(String sql) throws SQLException {
            try {
            	return super.execute(sql);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.execute(sql);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int[] executeBatch() throws SQLException {
            try {
            	return super.executeBatch();
            } catch(SQLException e) {
            	/* We CANNOT retry because executeBatch() clears out the current batch parameters even upon failure
            	try {
    	        	if(retryOnce)
    	        		return super.executeBatch();
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}*/
            	throw e;
            }
        }

        @Override
		public ResultSet executeQuery(String sql) throws SQLException {
            try {
            	return super.executeQuery(sql);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeQuery(sql);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
            try {
            	return super.executeUpdate(sql, autoGeneratedKeys);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql, autoGeneratedKeys);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
            try {
            	return super.executeUpdate(sql, columnIndexes);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql, columnIndexes);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql, String[] columnNames) throws SQLException {
            try {
            	return super.executeUpdate(sql, columnNames);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql, columnNames);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }

        @Override
		public int executeUpdate(String sql) throws SQLException {
            try {
            	return super.executeUpdate(sql);
            } catch(SQLException e) {
            	try {
    	        	if(retryOnce)
    	        		return super.executeUpdate(sql);
    	        	else
    	        		throw e;
            	} catch(SQLException e1) {
            		diagnoseStatementException(e);
                	throw e;
            	}
            }
        }
    }
	/**
	 * @see simple.app.jmx.DataSourceMXBean#getCurrentConnectionInfo()
	 */
	public String getCurrentConnectionInfo() {
		StringBuilder sb = new StringBuilder();
		appendConnectionInfo(sb);
		return sb.toString();
	}

	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return Logger.getLogger(getClass().getPackage().getName());
	}
}
