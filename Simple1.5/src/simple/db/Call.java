/*
 * Call.java
 *
 * Created on December 24, 2002, 11:34 AM
 */

package simple.db;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.sql.BatchUpdateException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import javax.management.InstanceNotFoundException;
import javax.management.JMException;
import javax.management.MBeanRegistrationException;
import javax.management.ObjectName;

import org.xml.sax.SAXException;

import simple.app.LongTotals;
import simple.app.RollingSample;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.DynaBean;
import simple.bean.ReflectionUtils;
import simple.db.ParameterException.ParameterExceptionType;
import simple.db.config.ConfigLoader;
import simple.event.TaskListener;
import simple.io.Log;
import simple.lang.Decision;
import simple.results.BeanException;
import simple.results.CacheableListArrayResults;
import simple.results.CacheableScrollableResultSetResults;
import simple.results.CachingForwardOnlyResults;
import simple.results.ForwardOnlyResults;
import simple.results.ListArrayResults;
import simple.results.Results;
import simple.results.ScrollableResultSetResults;
import simple.results.ScrollableResults;
import simple.results.UpdateableResultSetResults;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

/**
 *  This class performs the SQL specified as its sql Property using the Object
 *  array passed to it as the inputs and returns an Object array of its outputs.
 *  It handles creating the CallableStatement, converting the inputs to the
 *  appropriate type, setting the Statement object's parameters, executing the
 *  Statement, gather the outputs of the Statement and transforming any ResultSets
 *  into Results objects.
 *
 * @author  Brian S. Krug
 * @version
 */
public class Call implements CursorTransform, Cloneable, CallMXBean {
    private static final Log log = Log.getLog();
    protected static final Object[] EMPTY_ARRAY = new Object[0];
    protected static long defaultLogExecuteTimeThreshhold = 5000;
	protected static boolean monitorStatements = true;
    protected long logExecuteTimeThreshhold = -1;
    protected String sql;
    protected int resultSetType = ResultSet.TYPE_FORWARD_ONLY;
    protected int resultSetConcurrency = ResultSet.CONCUR_READ_ONLY;
    protected int maxRows = 0;
	protected int fetchSize = 0;
    protected Argument[] arguments;
    protected int outs = 0;
    protected int ins = 0;
    protected double queryTimeout = 0;
	protected String label;
	protected boolean jmxEnabled = true;
	protected ObjectName jmxObjectName = null;
	protected final RollingSample<Number> sampleExecutionTimes = new RollingSample<Number>(new LongTotals(), 100);
	protected boolean trackingExecuteTime = true;
	protected Sorter sorter;

    public static final Decision<Long> NEVER_CACHE = new Decision<Long>() {
    	public boolean decide(Long argument) {
    		return false;
    	}
    };
    public static final Decision<Long> ALWAYS_CACHE = new Decision<Long>() {
    	public boolean decide(Long argument) {
    		return true;
    	}
    };
    /**
     * The following inner class is NOT thread-safe for multiple executions of the execute methods. Obviously,
     * The cancel method may be called from a separate thread.
     * @author Brian S. Krug
     *
     */
    public class CallStatement {
    	protected final static int CREATED = 0;
        protected final static int CANCELLED = -1;
        protected final static int PREPARING = 2;
        protected final static int RUNNING = 3;
        protected final static int DONE = 4;
        protected CallableStatement cs;
    	protected final AtomicInteger state = new AtomicInteger(CREATED);

        protected CallStatement(CallableStatement cs) {
        	this.cs = cs;
        }
    	public Object[] executeCall(Object bean, TaskListener taskListener) throws SQLException, ParameterException {
    		if(!state.compareAndSet(CREATED, PREPARING))
    			throw new SQLException(state.get() == CANCELLED ? "Statement was cancelled" : "Statement was already executed since last reset");
    		try {
	    		if(bean instanceof Object[] || bean == null) return executeCall((Object[]) bean, taskListener);
	            else {
	                Object[] outParams = executeCall(extractParams(bean), taskListener);
	                saveParams(outParams, bean);
	                return outParams;
	            }
    		} finally {
    			state.set(DONE);
    		}
    	}

    	protected Object[] executeCall(Object[] params, TaskListener taskListener) throws SQLException, ParameterException {
			Object[] normals = setParams(cs, params, true);
    		if(!state.compareAndSet(PREPARING, RUNNING))
    			throw new SQLException("Statement was cancelled");
			return runCall(cs, NEVER_CACHE, true, taskListener, normals);
    	}

    	public boolean reset(Connection conn) throws SQLException, IllegalStateException {
    		switch(state.intValue()) {
    			case CREATED:
    				return false;
    			case CANCELLED:
    				if(!state.compareAndSet(CANCELLED, CREATED)) reset(conn);
    				resetStatement(conn);
    				return true;
    			case DONE:
    				if(!state.compareAndSet(DONE, CREATED)) reset(conn);
    				resetStatement(conn);
    				return true;
    			default:
    				throw new IllegalStateException("Statement is running and cannot be reset until complete or cancelled");
    		}
    	}

    	protected void resetStatement(Connection conn) throws SQLException {
    		if(cs == null) {
    			cs = getStatement(conn);
    		} else {
	    		Connection oldConn;
	    		try {
	    			oldConn = cs.getConnection();
	    		} catch(SQLException e) {
	    			oldConn = null;
	    		}
	    		if(oldConn != conn || oldConn.isClosed())
	    			cs = getStatement(conn);
	    		else
	    			cs.clearParameters();
    		}
    	}

    	public boolean cancel() throws SQLException {
    		switch(state.getAndSet(CANCELLED)) {
    			case CREATED:
    				return false;
    			case CANCELLED:
    				return false;
    			case DONE:
    				return false;
    			case PREPARING:
    				return true;
    			default:
    				// there is still a chance that cancel gets called after state is set to RUNNING but before the underlying jdbc driver
    				// actually submits the statement to execute. I do not see any way around this.
    				try {
    					cs.cancel();
    				} catch(NullPointerException npe) { // if close() is called
    					return false;
    				}
    				return true;
    		}
    	}
    	public void close() throws SQLException {
    		cs.close();
    		cs = null;
    	}
    }
    protected abstract class AbstractCallFuture<V> implements Future<V> {
        protected final ReentrantLock lock = new ReentrantLock();
        protected CallableStatement cs;
        protected final static int CREATED = 0;
        protected final static int CANCELLED = 1;
        protected final static int RUNNING = 2;
        protected final static int DONE = 3;
        protected final static int EXCEPTION = 4;
        protected final AtomicInteger state = new AtomicInteger(CREATED);
        protected Object[] result;
        protected ExecutionException exception;
        protected Condition signal;
        protected final Object bean;
        protected final Decision<Long> shouldCache;
        protected final TaskListener taskListener;
		protected final Object[] normals;

		protected AbstractCallFuture(CallableStatement cs, Decision<Long> shouldCache, Object bean, TaskListener taskListener, Object[] normals) {
            this.cs = cs;
            this.shouldCache = shouldCache;
            this.bean = bean;
            this.taskListener = taskListener;
			this.normals = normals;
        }
        /**
         * @see java.util.concurrent.Future#cancel(boolean)
         */
        public boolean cancel(boolean mayInterruptIfRunning) {
            switch(state.intValue()) {
                case CREATED:
                    if(state.compareAndSet(CREATED, CANCELLED)) {
                        log.debug("Call '" + Call.this.toString() + "' cancelled before it ran");
                        cs = null;
                        return true;
                    }
                    else return cancel(mayInterruptIfRunning);
                case RUNNING:
                    if(!mayInterruptIfRunning) return false;
                    try {
                        cs.cancel();
                        if(state.compareAndSet(RUNNING, CANCELLED)) {
                            log.debug("Call '" + Call.this.toString() + "' cancelled while running");
                            cs = null;
                            return true;
                        }
                    } catch(SQLException e) {
                        log.info("Could not cancel call '" + Call.this.toString() + "'", e);
                        return false;
                    } catch(NullPointerException e) {
                        if(cs != null) throw e;
                    }
                case CANCELLED: case EXCEPTION: case DONE: default:
                    log.debug("Call '" + Call.this.toString() + "' could not be cancelled (state=" + state.intValue() + ")");
                    return false;
            }
        }

        /**
         * @see java.util.concurrent.Future#isCancelled()
         */
        public boolean isCancelled() {
            return state.intValue() == CANCELLED;
        }

        /**
         * @see java.util.concurrent.Future#isDone()
         */
        public boolean isDone() {
            return state.intValue() == CANCELLED || state.intValue() == DONE;
        }

        protected abstract V convertResult(Object[] result) ;
        /**
         * @see java.util.concurrent.Future#get()
         */
        public V get() throws InterruptedException, ExecutionException {
            try {
                return get(-1, null);
            } catch(TimeoutException e) {
                throw new IllegalStateException("This should never happen");
            }
        }

        /**
         * @see java.util.concurrent.Future#get(long, java.util.concurrent.TimeUnit)
         */
        public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            switch(state.intValue()) {
                case CREATED:
                    if(state.compareAndSet(CREATED, RUNNING)) {
                        try {
							result = runCall(cs, shouldCache, false, taskListener, normals);
                            if(bean != null) saveParams(result, bean);
                            state.compareAndSet(RUNNING, DONE);
                            return convertResult(result);
                        } catch(SQLException e) {
                            exception = new ExecutionException(e);
                            state.compareAndSet(RUNNING, EXCEPTION);
                            throw exception;
                        } catch(ParameterException e) {
                            exception = new ExecutionException(e);
                            state.compareAndSet(RUNNING, EXCEPTION);
                            throw exception;
                        } finally {
                            cs = null; // for gc
                            if(signal != null) {
                                lock.lock();
                                try {
                                    signal.signalAll();
                                } finally {
                                    lock.unlock();
                                }
                            }
                        }
                    }
                case RUNNING:
                    // wait until done
                    lock.lock();
                    try {
                        if(signal == null)
                            signal = lock.newCondition();
                        if(state.intValue() == RUNNING) {
                            if(timeout >= 0 && unit != null)
                                if(!signal.await(timeout, unit)) {
                                    //timeout
                                    throw new TimeoutException("Call not complete after " + timeout + " " + unit.toString());
                                }
                            else
                                signal.await();
                        }
                    } finally {
                        lock.unlock();
                    }
                    return get();
                case CANCELLED:
                    throw new CancellationException();
                case EXCEPTION:
                    throw exception;
                case DONE: default:
                    return convertResult(result);
            }
        }
    }
    public class CallFuture extends AbstractCallFuture<Object[]> {
        /**
         * @param cs
         * @param timeHolder
         * @param bean
         */
		protected CallFuture(CallableStatement cs, Decision<Long> shouldCache, Object bean, TaskListener taskListener, Object[] normals) {
			super(cs, shouldCache, bean, taskListener, normals);
        }

        /**
         * @see simple.db.Call.AbstractCallFuture#convertResult(java.lang.Object[])
         */
        @Override
        protected Object[] convertResult(Object[] result) {
            return result;
        }
    }

    public class QueryFuture extends AbstractCallFuture<Results> {
        /**
         * @param cs
         * @param timeHolder
         * @param bean
         */
		protected QueryFuture(CallableStatement cs, Decision<Long> shouldCache, Object bean, TaskListener taskListener, Object[] normals) {
			super(cs, shouldCache, bean, taskListener, normals);
        }

        /**
         * @see simple.db.Call.AbstractCallFuture#convertResult(java.lang.Object[])
         */
        @Override
        protected Results convertResult(Object[] result) {
            for(int i = 0; i < result.length; i++) if(result[i] instanceof Results) return (Results) result[i];
            return null;
        }
    }
    public class UpdateFuture extends AbstractCallFuture<Integer> {
        /**
         * @param cs
         * @param timeHolder
         * @param bean
         */
		protected UpdateFuture(CallableStatement cs, Decision<Long> shouldCache, Object bean, TaskListener taskListener, Object[] normals) {
			super(cs, shouldCache, bean, taskListener, normals);
        }

        /**
         * @see simple.db.Call.AbstractCallFuture#convertResult(java.lang.Object[])
         */
        @Override
        protected Integer convertResult(Object[] result) {
            return (result[0] instanceof Number ? ((Number)result[0]).intValue() : -1);
        }
    }
    public class BatchUpdateResult {
    	protected int updateCount = Statement.EXECUTE_FAILED;
    	protected SQLException sqlException;
    	protected ParameterException parameterException;
    	protected final Object[] params;
    	protected boolean inBatch = false;
		public BatchUpdateResult(Object[] params) {
			super();
			this.params = params;
		}
		public void checkExceptions() throws ParameterException, SQLException {
			if(parameterException != null)
				throw parameterException;
			if(sqlException != null)
				throw sqlException;
		}
		protected void addToBatch(CallableStatement cs) throws SQLException {
			cs.clearParameters(); //This is necessary for Oracle ojdbc1.4 or it will throw a NPE on subsequent calls
    		try {
				setParams(cs, params, false);
			} catch(ParameterException e) {
				parameterException = e;
				log.warn("Could not add to batch", e);
				return;
			} catch (SQLException e) {
				sqlException = e;
				log.warn("Could not add to batch", e);
				return;
			}
    		cs.addBatch();
    		inBatch = true;
		}
		protected void executeAlone(CallableStatement cs) throws SQLException {
			cs.clearParameters(); //This is necessary for Oracle ojdbc1.4 or it will throw a NPE on subsequent calls
    		try {
				setParams(cs, params, false);
				updateCount = cs.executeUpdate();
			} catch(ParameterException e) {
				parameterException = e;
				log.warn("Could not set parameters (THIS SHOULD NOT HAPPEN)", e);
				return;
			} catch (SQLException e) {
				sqlException = e;
				log.info("Could not execute", e);
				return;
			}
		}
		public Object[] getParams() {
			return params;
		}
		public int getUpdateCount() {
			return updateCount;
		}
    }
    public class BatchUpdate {
    	protected Queue<Object[]> batchParamList;
		protected float overageFactor;
    	protected BatchUpdate(boolean concurrent, float overageAllowed) {
    		batchParamList = (concurrent ? new ConcurrentLinkedQueue<Object[]>() : new LinkedList<Object[]>());
    		if(overageAllowed == Float.MAX_VALUE)
				overageFactor = 0.0f;
    		else if(overageAllowed > 0.0f)
				overageFactor = overageAllowed + 1;
    		else
				overageFactor = 1.0f;
    	}
    	public void addBatch(Object bean) throws ParameterException {
    		if(bean instanceof Object[] || bean == null)
    			addBatch((Object[]) bean);
            else
            	addBatch(extractParams(bean));
    	}
    	public void addBatch(Object[] params) throws ParameterException {
    		//prepParams
    		batchParamList.offer(normalizeParams(params));
    	}
    	public BatchUpdateResult[] executeBatchFaultTolerant(Connection conn) throws SQLException {
			return executeBatchFaultTolerant(conn, 0);
		}

		public BatchUpdateResult[] executeBatchFaultTolerant(Connection conn, int batchSize) throws SQLException {
			final long start = System.currentTimeMillis();
			final int cnt = getPendingCount();
			if(batchSize < 1)
				batchSize = cnt;
			else if(batchSize > cnt)
				batchSize = cnt;
			BatchUpdateResult[] results = new BatchUpdateResult[batchSize];
    		int[] updateCounts;
    		CallableStatement cs = getStatement(conn);
    		try {
	    		try {
	    			for(int i = 0; i < results.length; i++) {
	    				Object[] params = batchParamList.poll();
	    				if(params == null) {
		    				BatchUpdateResult[] tmp = new BatchUpdateResult[i];
		    				System.arraycopy(results, 0, tmp, 0, tmp.length);
		    				results = tmp;
		    				break;
		    			}
	    				results[i] = new BatchUpdateResult(params);
	    				results[i].addToBatch(cs);
		    		}
		            updateCounts = cs.executeBatch();
		            SQLWarning warning = cs.getWarnings();
		            if(warning != null) {
		            	log.info("Received warning on batch execution", warning);
		            }
				} catch(BatchUpdateException e) {
					updateCounts = e.getUpdateCounts();
				}
	    		if(log.isDebugEnabled()) {
	    			long time = System.currentTimeMillis() - start;
	        		log.debug("SQL Batch Execution took " + time + " milliseconds for " + updateCounts.length + " batches");
					if(isTrackingExecuteTime())
						sampleExecutionTimes.addStats(time);
	    		}
	            int k = 0;
	            for(int i = 0; i < results.length; i++) {
	            	if(results[i].inBatch) {
	            		results[i].updateCount = updateCounts[k++];
	            		if(results[i].updateCount == Statement.EXECUTE_FAILED) {
	            			results[i].executeAlone(cs);
	            		}
	            	}
	            }
    		} finally {
    			cs.close();
    		}
    		return results;
    	}
    	public int[] executeBatch(Connection conn) throws SQLException, ParameterException {
			return executeBatch(conn, 0);
		}

		public int[] executeBatchOneByOne(Connection conn, int maxBatchSize) throws SQLException, ParameterException {
			long start = System.currentTimeMillis();
			CallableStatement cs = getStatement(conn);
			int[] updateCount = new int[maxBatchSize];
			try {
				if(maxBatchSize < 1) {
					if(overageFactor == 0.0f)
						maxBatchSize = Integer.MAX_VALUE;
					else
						maxBatchSize = Math.max((int) (batchParamList.size() * overageFactor), 1);
				}
				log.debug("SQL Batch Max Size is " + maxBatchSize);
				for(int i = 0; i < maxBatchSize; i++) {
					Object[] params = batchParamList.poll();
					if(params == null) {
						int[] tmp = new int[i];
						System.arraycopy(updateCount, 0, tmp, 0, tmp.length);
						updateCount = tmp;
						break;
					}
					cs.clearParameters(); // This is necessary for Oracle ojdbc1.4 or it will throw a NPE on subsequent calls
					setParams(cs, params, false);
					updateCount[i] = cs.executeUpdate();
					SQLWarning warning = cs.getWarnings();
					if(warning != null) {
						log.info("Received warning on batch execution", warning);
					}
				}
			} finally {
				cs.close();
			}
			if(log.isDebugEnabled()) {
				long time = System.currentTimeMillis() - start;
				log.debug("SQL Batch Execution took " + time + " milliseconds for " + updateCount.length + " batches");
				if(isTrackingExecuteTime())
					sampleExecutionTimes.addStats(time);
			}
			return updateCount;
		}
		public int[] executeBatch(Connection conn, int maxBatchSize) throws SQLException, ParameterException {
			long start = System.currentTimeMillis();
    		CallableStatement cs = getStatement(conn);
    		int[] updateCount;
    		try {
				if(maxBatchSize < 1) {
					if(overageFactor == 0.0f)
						maxBatchSize = Integer.MAX_VALUE;
					else
						maxBatchSize = Math.max((int) (batchParamList.size() * overageFactor), 1);
				}
				log.debug("SQL Batch Max Size is " + maxBatchSize);
				for(int i = 0; i < maxBatchSize; i++) {
	    			Object[] params = batchParamList.poll();
	    			if(params == null) break;
	    			cs.clearParameters(); //This is necessary for Oracle ojdbc1.4 or it will throw a NPE on subsequent calls
	        		setParams(cs, params, false);
	        		cs.addBatch();
	    		}
	            updateCount = cs.executeBatch();
	            SQLWarning warning = cs.getWarnings();
	            if(warning != null) {
	            	log.info("Received warning on batch execution", warning);
	            }
    		} finally {
    			cs.close();
    		}
			if(log.isDebugEnabled()) {
    			long time = System.currentTimeMillis() - start;
				log.debug("SQL Batch Execution took " + time + " milliseconds for " + updateCount.length + " batches");
				if(isTrackingExecuteTime())
					sampleExecutionTimes.addStats(time);
    		}
    		return updateCount;
    	}
    	public void cancelBatch() {
    		batchParamList.clear();
    	}
    	public int getPendingCount() {
    		return batchParamList.size();
    	}
    }

	public class Sorter {
		protected final Cursor cursor;
		protected final SQLType[] sqlTypes;
		protected final SQLType[] pagedSqlTypes;
		protected Map<String, Integer> columnIndexMap;
		protected Comparator<String> comp = new java.util.Comparator<String>() {
			public int compare(String o1, String o2) {
				if(o1 == null) {
					if(o2 == null)
						return 0;
					return -1;
				} else if(o2 == null) {
					return 1;
				} else {
					return o1.compareToIgnoreCase(o2);
				}
			}
		};

		public Sorter() throws DataLayerException {
			Cursor cursor = null;
			List<SQLType> sqlTypeList = new ArrayList<SQLType>();
			for(Argument a : getArguments()) {
				if(a instanceof Cursor) {
					cursor = (Cursor) a;
				} else if(a.isIn())
					sqlTypeList.add(a.getSqlType());
			}
			if(cursor == null)
				throw new DataLayerException("The given call does not have a cursor defined");
			this.cursor = cursor;
			this.sqlTypes = sqlTypeList.toArray(new SQLType[sqlTypeList.size()]);
			this.pagedSqlTypes = new SQLType[sqlTypes.length + 2];
			System.arraycopy(sqlTypes, 0, pagedSqlTypes, 0, sqlTypes.length);
			SQLType pagedSqlType = new SQLType(Types.INTEGER);
			pagedSqlTypes[sqlTypes.length] = pagedSqlType;
			pagedSqlTypes[sqlTypes.length + 1] = pagedSqlType;
		}

		protected Map<String, Integer> getColumnIndexMap() {
			if(columnIndexMap == null) {
				Map<String, Integer> m = new java.util.TreeMap<String, Integer>(comp);
				Column[] columns = cursor.getColumns();
				for(int i = 0; i < columns.length; i++)
					m.put(columns[i].getPropertyName(), i + 1);
				columnIndexMap = m;
			}
			return columnIndexMap;
		}

		protected int[] toIndexes(String[] sortBy) {
			getColumnIndexMap();
			int[] array = new int[sortBy.length];
			for(int i = 0; i < sortBy.length; i++) {
				int sign;
				Integer index;
				if(sortBy[i].startsWith("-")) {
					sign = -1;
					index = columnIndexMap.get(sortBy[i].substring(1));
				} else {
					sign = 1;
					index = columnIndexMap.get(sortBy[i]);
				}
				array[i] = index == null ? 0 : index * sign;
			}
			return array;
		}

		public Results executeSorted(Object input, int pageSize, int pageIndex, String... sortBy) throws SQLException, DataLayerException {
			return executeSorted(input, pageSize, pageIndex, toIndexes(sortBy));
		}

		public Results executeSorted(Object input, int pageSize, int pageIndex, int... sortBy) throws SQLException, DataLayerException {
			Connection conn = DataLayerMgr.getConnection(getDataSourceName());
			try {
				return executeSorted(conn, input, pageSize, pageIndex, sortBy);
			} finally {
				try {
					conn.close();
				} catch(SQLException e) {
					// ignore
				}
			}
		}

		public Results executeSorted(Connection connection, Object input, int pageSize, int pageIndex, String... sortBy) throws SQLException, DataLayerException {
			return executeSorted(connection, input, pageSize, pageIndex, toIndexes(sortBy));
		}

		public Results executeSorted(Connection connection, Object input, int pageSize, int pageIndex, int... sortBy) throws SQLException, DataLayerException {
			Object[] params;
			if(input instanceof Object[] || input == null) {
				params = (Object[]) input;
			} else {
				params = extractParams(input);
			}
			DBHelper dbHelper = DataLayerMgr.getDBHelper(connection);
			// get slice
			String sortedSql = dbHelper.getSortedSql(connection, getSql(), sortBy);
			CallableStatement cs = connection.prepareCall(sortedSql, resultSetType, resultSetConcurrency);
			log.trace("Prepared call");
			cs.setMaxRows(pageSize);
			final int fetchSize = getFetchSize();
			if(pageSize < fetchSize)
				cs.setFetchSize(pageSize);
			else if(fetchSize > 0)
				cs.setFetchSize(fetchSize);
			final double qt = getQueryTimeout();
			if(qt > 0)
				cs = dbHelper.setQueryTimeout(cs, qt);
			Object[] normals = setParamsCloseOnError(cs, params, true);
			dbHelper.setSortedRowLimits(cs, (pageIndex - 1) * pageSize + 1, pageSize);
			Object[] outputParams = runCall(cs, NEVER_CACHE, false, null, normals);
			// find the first in the output
			for(int i = 0; i < outputParams.length; i++)
				if(outputParams[i] instanceof Results)
					return (Results) outputParams[i];
			return null;
		}

		public int executeCount(Object input) throws SQLException, DataLayerException {
			Connection conn = DataLayerMgr.getConnection(getDataSourceName());
			try {
				return executeCount(conn, input);
			} finally {
				try {
					conn.close();
				} catch(SQLException e) {
					// ignore
				}
			}
		}

		public int executeCount(Connection connection, Object input) throws ParameterException, SQLException {
			Object[] params;
			if(input instanceof Object[] || input == null) {
				params = (Object[]) input;
			} else {
				params = extractParams(input);
			}
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT COUNT(*) FROM (").append(getSql()).append(") X");
			CallableStatement cs = connection.prepareCall(sql.toString(), resultSetType, resultSetConcurrency);
			log.trace("Prepared call");
			cs.setMaxRows(1);
			setParamsCloseOnError(cs, params, true);
			ResultSet rs = cs.executeQuery();
			if(!rs.next())
				throw new NotEnoughRowsException("Count query returned zero rows");
			return rs.getInt(1);
		}
	}

   /** Holds value of property dataSourceName. */
    protected String dataSourceName;

    /** Creates new Call */
    public Call() {
    }

    /** Retrieves a CallableStatement object for the sql statement of this Call
     * using the specified connection
     * @return
     * @param connection
     * @throws SQLException
     */
    protected CallableStatement getStatement(Connection connection) throws SQLException {
		log.trace("Preparing call '" + sql + "'");
		CallableStatement cs = connection.prepareCall(sql, resultSetType, resultSetConcurrency);
        log.trace("Prepared call");
		final int maxRows = getMaxRows();
		cs.setMaxRows(maxRows);
		final int fetchSize = getFetchSize();
		if(maxRows > 0 && maxRows < fetchSize)
			cs.setFetchSize(maxRows);
		else if(fetchSize > 0)
			cs.setFetchSize(fetchSize);
		final double qt = getQueryTimeout();
		if(qt > 0)
			cs = DataLayerMgr.getDBHelper(connection).setQueryTimeout(cs, qt);
        return cs;
    }

    /**  Executes the SQL Call and returns the update count
     * @return
     * @param connection
     * @param bean
     * @throws SQLException
     * @throws ParameterException
     */
    public int executeUpdate(Connection connection, Object bean, TaskListener taskListener) throws SQLException, ParameterException {
        return executeUpdate(connection, bean, bean, taskListener);
    }


    /**  Executes the SQL Call and returns the update count
     * @return
     * @param connection
     * @param bean
     * @throws SQLException
     * @throws ParameterException
     */
    public int executeUpdate(Connection connection, Object input, Object output, TaskListener taskListener) throws SQLException, ParameterException {
        Object[] results = executeCall(connection, input, output, taskListener);
        return (results[0] instanceof Number ? ((Number)results[0]).intValue() : -1);
    }
    /**  Executes the SQL Call and returns the first Results Object in the
     *  output array.
     * @return
     * @param connection
     * @param bean
     * @throws SQLException
     * @throws ParameterException
     */
    public Results executeQuery(Connection connection, Object bean, TaskListener taskListener) throws SQLException, ParameterException {
       return executeQuery(connection, bean, bean, taskListener);
    }

    /**  Executes the SQL Call and returns the first Results Object in the
     *  output array.
     * @return
     * @param connection
     * @param bean
     * @throws SQLException
     * @throws ParameterException
     */
    public Results executeQuery(Connection connection, Object input, Object output, TaskListener taskListener) throws SQLException, ParameterException {
        Object[] outputParams = executeCall(connection, input, output, taskListener);
        //find the first in the output
        for(int i = 0; i < outputParams.length; i++) if(outputParams[i] instanceof Results) return (Results) outputParams[i];
        return null;
    }

	public void selectInto(Connection connection, Object bean, TaskListener taskListener) throws SQLException, ParameterException, BeanException {
		selectInto(connection, bean, bean, taskListener);
	}

	public void selectInto(Connection connection, Object input, Object output, TaskListener taskListener) throws SQLException, ParameterException, BeanException {
		Results results = executeQuery(connection, input, output, taskListener);
		if(!results.next())
			throw new NotEnoughRowsException();
		if(!results.isGroupEnding(0))
			throw new TooManyRowsException();
		results.fillBean(output);
	}

    /**  Executes the SQL Call using the properties of the specified bean as the in parameters
     *  and returns the outputs as an array.
     * @return
     * @param connection
     * @param bean
     * @throws SQLException
     * @throws ParameterException
     */
    public Object[] executeCall(Connection connection, Object bean, TaskListener taskListener) throws SQLException, ParameterException {
        return executeCall(connection, bean, bean, taskListener);
    }

    /**  Executes the SQL Call using the properties of the specified bean as the in parameters
     *  and returns the outputs as an array.
     * @return
     * @param connection
     * @param bean
     * @throws SQLException
     * @throws ParameterException
     */
    public Object[] executeCall(Connection connection, Object input, Object output, TaskListener taskListener) throws SQLException, ParameterException {
        Object[] params;
    	if(input instanceof Object[] || input == null) {
    		params = (Object[])input;
    	} else {
        	params = extractParams(input);
    	}
    	Object[] outParams = executeCall(connection, params, NEVER_CACHE, taskListener);
        if(output != null && !(output instanceof Object[])) {
        	saveParams(outParams, output);
        }
        return outParams;
    }

    /** This method creates a CallableStatement and sets all the in parameters. The calling program is responsible for
     *  closing the CallableStatement. This method is not normally used.
     * @param connection
     * @param input
     * @return
     * @throws SQLException
     * @throws ParameterException
     */
    public CallableStatement prepareStatement(Connection connection, Object input) throws SQLException, ParameterException {
        Object[] params;
    	if(input instanceof Object[] || input == null) {
    		params = (Object[])input;
    	} else {
        	params = extractParams(input);
    	}
		CallableStatement cs = getStatement(connection);
		setParamsCloseOnError(cs, params, true);
		return cs;
    }

    /** Saves the array of the values for each OUT argument in this call into the bean.
     * The name of each argument is used to set the value of
     * the same named property from the bean.
     * @param outParams
     * @param bean
     * @throws ParameterException
     */
    public void saveParams(Object[] outParams, Object bean) throws ParameterException {
        if(bean == null || outParams == null) return;
        int k = 0;
        for(int i = 0; i < arguments.length; i++) {
            if(arguments[i].isOut()) {
                String name = arguments[i].getPropertyName();
                if(name != null) {
                    log.debug("Saving parameter '" + outParams[k] + "' into the bean");
                    /*if(bean instanceof Map) {
                        ((Map)bean).put(name, outParams[k]);
                    } else*/ {
                        try {
                            ReflectionUtils.setProperty(bean, name, outParams[k], true);
                        } catch(InvocationTargetException e) {
                            throw new ParameterException("Exception occurred while setting parameter, '"
                                + name + "' in bean " + bean, e);
                        } catch (IntrospectionException e) {
                            throw new ParameterException("Exception occurred while setting parameter, '"
                                    + name + "' in bean " + bean, e);
                        } catch (IllegalAccessException e) {
                            throw new ParameterException("Exception occurred while setting parameter, '"
                                    + name + "' in bean " + bean, e);
                        } catch (InstantiationException e) {
                            throw new ParameterException("Exception occurred while setting parameter, '"
                                    + name + "' in bean " + bean, e);
                        } catch (ConvertException e) {
                        	ParameterException pe = new ParameterException("Could not convert the parameter '" + name + "' to the proper type when setting it", e);
                        	pe.setParameterName(name);
                        	pe.setType(ParameterExceptionType.CONVERSION);
                        	throw pe;
                        } catch(ParseException e) {
                            throw new ParameterException("Unparsable parameter name, '"
                                    + name + "' in bean " + bean, e);
 						}
                    }
                }
                k++;
            }
        }
    }


    /** Creates an array of the values for each IN argument in this call. If the
     * bean object is a collection or iterator, it is turned into an array.
     * Otherwise, the name of each argument is used to retrieve the value of
     * the same named property from the bean.
     * @return
     * @param bean
     * @throws ParameterException
     */
    public Object[] extractParams(Object bean) throws ParameterException {
        if(bean == null || arguments.length <= 1) {
            return new Object[0];
        } else if(!(bean instanceof DynaBean) && bean instanceof Collection<?>) {
            return ((Collection<?>)bean).toArray();
        } else {
            List<Object> params = new ArrayList<Object>();
            for(int i = 1; i < arguments.length; i++) {
	            if(arguments[i].isIn()) {
	                Object value = null;
	                String name = arguments[i].getPropertyName();
	                if(name != null) {
	                    /*if(bean instanceof Map) {
	                        if(name != null) value = ((Map)bean).get(name);
	                        else value = ((Map)bean).get(new Integer(params.size()+1));
	                    } else if(name != null)*/ {
	                        try {
                                value = ReflectionUtils.getProperty(bean,name);
                            } catch(InvocationTargetException e) {
                                throw new ParameterException("Exception occurred while extracting parameter, '" + name + "' from bean " + bean, e);
                            } catch (IntrospectionException e) {
                                throw new ParameterException("Exception occurred while extracting parameter, '" + name + "' from bean " + bean, e);
                            } catch (IllegalAccessException e) {
                                throw new ParameterException("Exception occurred while extracting parameter, '" + name + "' from bean " + bean, e);
                            } catch(ParseException e) {
                                throw new ParameterException("Unparsable parameter name, '" + name + "' in bean " + bean, e);
                            }
	                    }
	                }
	                params.add(value);
	            }
	        }
	        return params.toArray();
        }
    }

    public BatchUpdate createBatchUpdate() throws ParameterException {
    	return createBatchUpdate(true, 1.0f);
    }
    /** Creates a BatchUpdate object that can be used to update the database in efficient batches
     * @param concurrent If the BatchUpdate object should support add to it and executing it at the same time. Even if
     * this is set to true, only one thread may invoke the executeBatch() method at a time. But it does allow multiple concurrent
     * invocations of addBatch().
     * @param overageAllowed A factor that determines how many additional batches the executeBatch() method will pick up that were added AFTER
     * the method was called.
     * @return The BatchUpdate object
     * @throws ParameterException
     */
    public BatchUpdate createBatchUpdate(boolean concurrent, float overageAllowed) throws ParameterException {
    	if(arguments != null) {
            for(int i = 1; i < arguments.length; i++) {
                if(arguments[i].isOut()) {
                    throw new ParameterException("A batch update call may NOT contain out or inout parameters");
                }
            }
    	}
    	return new BatchUpdate(concurrent, overageAllowed);
    }
    /**  Executes the SQL Call and returns the outputs as an array.
     * @return
     * @param connection
     * @param params
     * @throws SQLException
     * @throws ConvertException
     * @throws ParameterException
     *//*
    public Object[] executeCall(Connection connection, Object[] params) throws SQLException, ConvertException, ParameterException {
        return executeCall(connection, params, null);
    }*/

    /**  Creates a CallStatement that can be used to execute the call and to cancel that execution. The CallStatement
     *  can be reused (but not concurrently) by calling reset.
     * @return The CallStatement object
     * @param connection
     * @throws SQLException
     */
    public CallStatement createCallStatement(Connection connection) throws SQLException {
        return new CallStatement(getStatement(connection));
    }

    /**  Executes the SQL Call using the properties of the specified bean as the in parameters
     *  and returns the outputs as an array.
     * @return
     * @param connection
     * @param bean
     * @throws SQLException
     * @throws ParameterException
     */
    public CallFuture createCallFuture(Connection connection, Object bean, TaskListener taskListener) throws SQLException, ParameterException {
        if(bean instanceof Object[] || bean == null) return createCallFuture(connection, (Object[]) bean, NEVER_CACHE, null, taskListener);
        else {
            return createCallFuture(connection, extractParams(bean), NEVER_CACHE, bean, taskListener);
        }
    }

    /**  Executes the SQL Call using the properties of the specified bean as the in parameters
     *  and returns the outputs as an array.
     * @return
     * @param connection
     * @param bean
     * @throws SQLException
     * @throws ParameterException
     */
    public QueryFuture createQueryFuture(Connection connection, Object bean, TaskListener taskListener) throws SQLException, ParameterException {
        if(bean instanceof Object[] || bean == null) return createQueryFuture(connection, (Object[]) bean, NEVER_CACHE, null, taskListener);
        else {
            return createQueryFuture(connection, extractParams(bean), NEVER_CACHE, bean, taskListener);
        }
    }

    /**  Executes the SQL Call using the properties of the specified bean as the in parameters
     *  and returns the outputs as an array.
     * @return
     * @param connection
     * @param bean
     * @throws SQLException
     * @throws ParameterException
     */
    public UpdateFuture createUpdateFuture(Connection connection, Object bean, TaskListener taskListener) throws SQLException, ParameterException {
        if(bean instanceof Object[] || bean == null) return createUpdateFuture(connection, (Object[]) bean, NEVER_CACHE, null, taskListener);
        else {
            return createUpdateFuture(connection, extractParams(bean), NEVER_CACHE, bean, taskListener);
        }
    }

    protected Object[] executeCall(Connection connection, Object[] params, Decision<Long> shouldCache, TaskListener taskListener) throws SQLException, ParameterException {
        return executeCall(connection, params, shouldCache, true, taskListener);
    }

    protected Object[] executeCall(Connection connection, Object[] params, Decision<Long> shouldCache, boolean checkRequired, TaskListener taskListener) throws SQLException, ParameterException {
		CallableStatement cs = getStatement(connection);
		Object[] normals = setParamsCloseOnError(cs, params, checkRequired);
		return runCall(cs, shouldCache, false, taskListener, normals);
    }

    protected CallFuture createCallFuture(Connection connection, Object[] params, Decision<Long> shouldCache, Object bean, TaskListener taskListener) throws SQLException, ParameterException {
		CallableStatement cs = getStatement(connection);
		Object[] normals = setParamsCloseOnError(cs, params, true);
        log.debug("Created cancellable call for '" + sql + "'");
		return new CallFuture(cs, shouldCache, bean, taskListener, normals);
    }

    protected QueryFuture createQueryFuture(Connection connection, Object[] params, Decision<Long> shouldCache, Object bean, TaskListener taskListener) throws SQLException, ParameterException {
		CallableStatement cs = getStatement(connection);
		Object[] normals = setParamsCloseOnError(cs, params, true);
        log.debug("Created cancellable query for '" + sql + "'");
		return new QueryFuture(cs, shouldCache, bean, taskListener, normals);
    }

    protected UpdateFuture createUpdateFuture(Connection connection, Object[] params, Decision<Long> shouldCache, Object bean, TaskListener taskListener) throws SQLException, ParameterException {
		CallableStatement cs = getStatement(connection);
		Object[] normals = setParamsCloseOnError(cs, params, true);
        log.debug("Created cancellable update for '" + sql + "'");
		return new UpdateFuture(cs, shouldCache, bean, taskListener, normals);
    }

	protected Object[] setParamsCloseOnError(CallableStatement cs, Object[] params, boolean checkRequired) throws SQLException, ParameterException {
		boolean close = true;
		try {
			Object[] normals = setParams(cs, params, checkRequired);
			close = false;
			return normals;
		} finally {
			if(close)
				cs.close();
		}
	}
    /** Converts each parameter into the appropriate (non-jdbc) class
     * @param params The parameters
     * @return A new array with each parameter converted and if null, replace with default value
     * @throws ParameterException If a parameter can not be converted into the appropriate class
     */
    public Object[] normalizeParams(Object[] params) throws ParameterException {
    	if(ins > 0) {
    		int k = 0;
    		Object[] normals = new Object[ins];
            for(int i = 1; i < arguments.length; i++) {
                if(arguments[i].isIn()) {
                	Object value = checkParam(params != null && params.length > k ? params[k] : null, arguments[i]);
                	try {
                		normals[k] = ConvertUtils.convert(arguments[i].getJavaType(), value);
                		if(normals[k] != null && ConvertUtils.isCollectionType(arguments[i].getJavaType())) {
                			if(ConvertUtils.asCollection(normals[k]).isEmpty())
                				normals[k] = null;
                		}
                		k++;
                	} catch (ConvertException e) {
						ParameterException pe = new ParameterException("Could not convert the parameter '" + arguments[i].getPropertyName() + "'", e);
                    	pe.setParameterName(arguments[i].getPropertyName());
                    	pe.setType(ParameterExceptionType.CONVERSION);
                    	throw pe;
					}
                }
            }
            return normals;
        } else
        	return EMPTY_ARRAY;
    }

	protected Object[] setParams(CallableStatement cs, Object[] params, boolean checkRequired) throws SQLException, ParameterException {
    	if(arguments != null) {
    		DBHelper dbHelper = DataLayerMgr.getDBHelper(cs.getConnection());
			int k = 0;
			Object[] normals = new Object[ins];
            for(int i = 1; i < arguments.length; i++) {
                if(arguments[i].isOut()) {
                	dbHelper.registerOutParameter(cs, i, arguments[i].getPropertyName(), arguments[i].getSqlType());
                }
                if(arguments[i].isIn()) {
					Object value = (params != null && params.length > k ? params[k] : null);
					if(checkRequired)
                        value = checkParam(value, arguments[i]);
					normals[k] = value;
                    dbHelper.setInParameter(cs, i, arguments[i].getPropertyName(), value, arguments[i].getSqlType());
					k++;
                }
            }
			return normals;
		} else {
			return EMPTY_ARRAY;
        }
    }

	protected void logSqlExecution(long start, long time, CallableStatement cs, Object[] normals) {
		if(isTrackingExecuteTime()) {
			sampleExecutionTimes.addStats(time);
		}

		SQLWarning warning;
		try {
			warning = cs.getWarnings();
		} catch(SQLException e) {
			warning = null;
		}
		if(warning != null && log.isWarnEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Execution of ");
			if(getLabel() != null)
				sb.append("Call '").append(getLabel());
			else
				sb.append("SQL '").append(sql);
			sb.append("' with params [");
			if(normals == null)
				sb.append("<UNAVAILABLE>");
			else
				for(int i = 0; i < normals.length; i++) {
					if(i > 0)
						sb.append(',');
					sb.append(normals[i]);
				}
			sb.append("] took ").append(time).append(" milliseconds and produced the following warnings:");
			for(; warning != null; warning = warning.getNextWarning()) {
				sb.append("\n\tMessage: ").append(warning.getMessage());
				sb.append("\n\tErrorCode: ").append(warning.getErrorCode());
				sb.append("\n\tSQLState: ").append(warning.getSQLState());
			}
			log.warn(sb.toString());
		} else if(log.isDebugEnabled() && time > getLogExecuteTimeThreshhold()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Execution of ");
			if(getLabel() != null)
				sb.append("Call '").append(getLabel());
			else
				sb.append("SQL '").append(sql);
			sb.append("' with params [");
			if(normals == null)
				sb.append("<UNAVAILABLE>");
			else
				for(int i = 0; i < normals.length; i++) {
					if(i > 0)
						sb.append(',');
					sb.append("'");
					sb.append(ConvertUtils.getStringSafely(normals[i], ""));
					sb.append("'");
				}
			sb.append("] took ").append(time).append(" milliseconds");
			log.debug(sb.toString());
		} else if(log.isInfoEnabled() && time > getLogExecuteTimeThreshhold() || log.isDebugEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Execution of ");
			if(getLabel() != null)
				sb.append("Call '").append(getLabel());
			else
				sb.append("SQL '").append(sql);
			sb.append("' took ").append(time).append(" milliseconds");
			log.debug(sb.toString());
		}
	}

	protected Object[] runCall(CallableStatement cs, Decision<Long> shouldCache, boolean keepOpen, TaskListener taskListener, Object[] normals) throws SQLException {
        try {
            // use execute because jdbc-odbc fails if executeQuery is run against
            // an update or executeUpdate is run on a query
            log.debug("Executing SQL '" + sql + "'");
			if(isMonitorStatements())
				StatementMonitor.registerStatement(cs);
			long start;
			ResultSet rs;
			try {
				start = System.currentTimeMillis();
				rs = cs.execute() ? cs.getResultSet() : null;
			} finally {
				if(isMonitorStatements())
					StatementMonitor.deregisterStatement(cs);
			}
            long time = System.currentTimeMillis() - start;
			logSqlExecution(start, time, cs, normals);
            if(taskListener != null) {
				taskListener.taskRan("EXECUTE SQL", sql, start, start+time);
			}
            Object[] output = new Object[outs+1];
            if(rs == null)
                output[0] = new Integer(cs.getUpdateCount());
            else if(arguments!=null&&arguments.length > 0 && arguments[0] instanceof Cursor) {
                Cursor cur = (Cursor)arguments[0];
                output[0] = transformCursor(rs, cur, time, shouldCache.decide(time), this);
                keepOpen = keepOpen || cur.isLazyAccess();
            } else
                output[0] = rs;
            int last = 0;
            if(outs > 0) {
            	DBHelper dbHelper = DataLayerMgr.getDBHelper(cs.getConnection());
                for(int i = 1; i < arguments.length; i++) {
                    if(arguments[i].isOut()) {
                        output[++last] = dbHelper.getValue(cs, i, arguments[i].getSqlType());
                        if(output[last] instanceof ResultSet && arguments[i] instanceof Cursor) {
                            Cursor cur = (Cursor)arguments[i];
                            output[last] = transformCursor((ResultSet)output[last], cur, time, shouldCache.decide(time), this);
                            keepOpen = keepOpen || cur.isLazyAccess();
                        }
                    }
                }
            }
            return output;
        } catch(SQLException e) {
            throw e;
        } finally {
            if(!keepOpen) cs.close();
        }
    }

    public static Results transformCursor(ResultSet rs, Cursor cur) throws SQLException {
        return transformCursor(rs, cur, -1, false, getDefaultCursorTransform());
    }

    /** Converts a ResultSet object to a Results object using the names of each
     * column of this Call as the Results object's column names.
     * @return
     * @param rs
     * @param cur
     * @param elapsedTime
     * @throws SQLException
     */
    public static Results transformCursor(ResultSet rs, Cursor cur, long elapsedTime, boolean cacheResults, CursorTransform cursorTransform) throws SQLException {
        //prepare column names
    	log.trace("Configuring query columns");
        Column[] columns = cur.configureColumns(rs);
        log.trace("Configured query columns");
        //set fetch size
        try {
			if(cur.getFetchSize() > 0 && rs.getStatement().getFetchSize() != cur.getFetchSize())
				rs.setFetchSize(cur.getFetchSize());
        } catch(SQLException sqle) {
            log.warn("Could not set ResultSet fetchSize to " + cur.getFetchSize());
            //continue processing - this doesn't sink us
        }
        Hints hints = new Hints();
        hints.lazyAccess = cur.isLazyAccess();
        hints.cacheable = cur.isCaching() || cacheResults;
        hints.elapsedTime = elapsedTime;
        hints.updateable = cur.isUpdateable();
        Results results = transformCursor(rs, columns, cursorTransform, hints);
        //add groupings
        Object[] groups = cur.getGroups();
        if(groups != null)
            for(int i = 0; i < groups.length; i++) {
                if(groups[i] instanceof Number) results.addGroup(((Number)groups[i]).intValue());
                else if(groups[i] != null) results.addGroup(groups[i].toString());
            }
        // add totals
        Object[] totals = cur.getTotals();
        if(totals != null)
            for(int i = 0; i < totals.length; i++) {
                if(totals[i] instanceof Number) results.trackAggregate(((Number)totals[i]).intValue());
                else if(totals[i] != null) results.trackAggregate(totals[i].toString());
            }

        return results;
    }
    /** Converts a ResultSet object to a Results object using the names of each
     * column of this Call as the Results object's column names.
     * @return
     * @param rs
     * @param cur
     * @throws SQLException
     */
    public static Results transformCursor(ResultSet rs, Column[] columns, CursorTransform cursorTransform, Hints hints) throws SQLException {
        // create Results Object
    	log.trace("Getting connection from resultset");
        Connection conn = rs.getStatement().getConnection();
        return transformCursorWithConnection(conn, rs, columns,cursorTransform, hints );
    }

    public static Results transformCursorWithConnection(Connection conn, ResultSet rs, Column[] columns, CursorTransform cursorTransform, Hints hints) throws SQLException {
        // create Results Object
        DBHelper dbHelper = DataLayerMgr.getDBHelper(DBUnwrap.getRealConnection(conn));
        return cursorTransform.toResults(columns, rs, dbHelper, hints);
    }

    /**  Sets the PreparedStatement's parameter at the specified index to the value
     *  indicated. If the value object is null, the parameter is set as null.
     *  If the sqlType is ARRAY, BLOB, CLOB, REF, or STRUCT the DBHelper
     *  is asked to translate the value object appropriately.
     * @param ps
     * @param index
     * @param value
     * @param sqlType
     * @param typeName
     * @throws SQLException
     * @throws ConvertException
     */
    public static void setParam(PreparedStatement ps, int index, Object value, int sqlType, String typeName) throws SQLException, ParameterException {
    	setParam(ps, index, value, new SQLType(sqlType, typeName));
    }

    /**  Sets the PreparedStatement's parameter at the specified index to the value
     *  indicated. If the value object is null, the parameter is set as null.
     *  If the sqlType is ARRAY, BLOB, CLOB, REF, or STRUCT the DBHelper
     *  is asked to translate the value object appropriately.
     * @param ps
     * @param index
     * @param value
     * @param sqlType
     * @param typeName
     * @param prepped if the simple param values have already been converted to the proper type using the prepParams() method
     * @throws SQLException
     * @throws ConvertException
     * @throws ParameterException
     */
    protected static void setParam(PreparedStatement ps, int index, Object value, SQLType sqlType) throws SQLException, ParameterException {
    	DBHelper dbHelper = DataLayerMgr.getDBHelper(ps.getConnection());
        dbHelper.setInParameter(ps, index, "parameter #" + index, value, sqlType);
    }

    protected static Object checkParam(Object value, Argument argument) throws ParameterException {
    	if(value == null && argument instanceof Parameter) {
            Parameter p = (Parameter)argument;
            value = p.getDefaultValue();
            if(value == null && p.isRequired()) {
            	ParameterException pe = new ParameterException("The parameter '" + argument.getPropertyName() + "' is required but not provided");
            	pe.setParameterName(argument.getPropertyName());
            	pe.setType(ParameterExceptionType.MISSING);
            	throw pe;
            }
        }
    	return value;
    }

    /* (non-Javadoc)
	 * @see simple.db.CallMXBean#getSql()
	 */
    @Override
	public String getSql() {
        return sql;
    }

    /** Setter for property sql.
     * @param sql New value of property sql.
     */
    public void setSql(String sql) {
        this.sql = sql;
    }

    /* (non-Javadoc)
	 * @see simple.db.CallMXBean#getResultSetType()
	 */
    @Override
	public int getResultSetType() {
        return resultSetType;
    }

    /** Setter for property resultSetType.
     * @param resultSetType New value of property resultSetType.
     */
    public void setResultSetType(int resultSetType) {
        this.resultSetType = resultSetType;
    }

    /* (non-Javadoc)
	 * @see simple.db.CallMXBean#getResultSetConcurrency()
	 */
    @Override
	public int getResultSetConcurrency() {
        return resultSetConcurrency;
    }

    /** Setter for property resultSetConcurrency.
     * @param resultSetConcurrency New value of property resultSetConcurrency.
     */
    public void setResultSetConcurrency(int resultSetConcurrency) {
        this.resultSetConcurrency = resultSetConcurrency;
    }

    /** Getter for property arguments.
     * @return Value of property arguments.
     */
    public Argument[] getArguments() {
		return (arguments == null ? null : arguments.clone());
    }

    public Argument getArgument(int index) {
    	if(arguments == null || index >= arguments.length)
    		return null;
    	return arguments[index];
    }

	public int getArgumentCount() {
		return (arguments == null ? 0 : arguments.length);
	}

    public void setArgument(int index, Argument argument) {
    	if(arguments == null || arguments.length == 0) {
    		Argument[] args = new Argument[index+1];
    		args[index] = argument;
    		setArguments(args);
    	} else if(index >= arguments.length) {
    		Argument[] args = new Argument[index+1];
    		System.arraycopy(arguments, 0, args, 0, arguments.length);
    		args[index] = argument;
    		setArguments(args);
    	} else {
    		//back out old
    		Argument old = arguments[index];
    		int outCnt = outs;
            int inCnt = ins;
            if(old != null) {
    			if(old.isOut()) outCnt--;
    			if(old.isIn()) inCnt--;
    		}
            if(argument != null) {
            	if(argument.isOut()) outCnt++;
                if(argument.isIn()) inCnt++;
            }
            arguments[index] = argument;
            ins = inCnt;
            outs = outCnt;
    	}
    }

    /** Setter for property arguments.
     * @param arguments New value of property arguments.
     */
    public void setArguments(Argument[] arguments) {
        if(this.arguments != arguments) {
            int outCnt = 0;
            int inCnt = 0;
            if(arguments != null) {
                for(int i = 1; i < arguments.length; i++) {
                    if(arguments[i].isOut()) outCnt++;
                    if(arguments[i].isIn()) inCnt++;
                }
            }
            outs = outCnt;
            ins = inCnt;
        }
        this.arguments = arguments;
    }

    /* (non-Javadoc)
	 * @see simple.db.CallMXBean#getDataSourceName()
	 */
    @Override
	public String getDataSourceName() {
        return this.dataSourceName;
    }

    /** Setter for property dataSourceName.
     * @param dataSourceName New value of property dataSourceName.
     */
    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }

    /* (non-Javadoc)
	 * @see simple.db.CallMXBean#getMaxRows()
	 */
    @Override
	public int getMaxRows() {
        return maxRows;
    }

    /**
     * @param maxRows The maxRows to set.
     */
    public void setMaxRows(int maxRows) {
        if(maxRows < 0) this.maxRows = 0;
        else this.maxRows = maxRows;
    }

    public int getInCount() {
    	return ins;
    }

    public int getOutCount() {
    	return outs;
    }

    /* (non-Javadoc)
	 * @see simple.db.CallMXBean#getQueryTimeout()
	 */
    @Override
	public double getQueryTimeout() {
		return queryTimeout;
	}

	/* (non-Javadoc)
	 * @see simple.db.CallMXBean#setQueryTimeout(double)
	 */
	@Override
	public void setQueryTimeout(double queryTimeout) {
		this.queryTimeout = queryTimeout;
	}

	public Results toResults(Column[] columns, ResultSet rs, DBHelper dbHelper, Hints hints) throws SQLException {
        return getDefaultCursorTransform().toResults(columns, rs, dbHelper, hints);
    }

    protected static CursorTransform defaultCursorTransform = new CursorTransform() {
        public Results toResults(Column[] columns, ResultSet rs, DBHelper dbHelper, Hints hints) throws SQLException {
            Results results;
            if(hints.lazyAccess) {
                //use ForwardOnlyResults or ScrollableResultSetResults - WARNING: connection must not be closed until all rows are accessed
            	if(hints.updateable && rs.getStatement().getResultSetConcurrency() == ResultSet.CONCUR_UPDATABLE) {
                	results = new UpdateableResultSetResults(columns, rs, dbHelper);
                } else  {
	                switch(rs.getStatement().getResultSetType()) {
	                    case ResultSet.TYPE_FORWARD_ONLY:
	                        if(hints.cacheable) try {
	                            results = new CachingForwardOnlyResults(columns, rs, dbHelper);
	                        } catch(IOException e) {
	                            log.warn("Could not cache results", e);
	                            results = new ForwardOnlyResults(columns, rs, dbHelper);
	                        }
	                        else
	                            results = new ForwardOnlyResults(columns, rs, dbHelper);
	                        break;
	                    case ResultSet.TYPE_SCROLL_INSENSITIVE:
	                    case ResultSet.TYPE_SCROLL_SENSITIVE:
	                    default:
	                        if(hints.cacheable)
	                            results = new CacheableScrollableResultSetResults(columns, rs, dbHelper);
	                        else
	                            results = new ScrollableResultSetResults(columns, rs, dbHelper);
	                        break;

	                }
                }
            } else { // use ListArrayResults
                /* ArrayList takes longer to load but is faster to access */
            	try {
					if(hints.cacheable)
	                    results = new CacheableListArrayResults(columns, rs, dbHelper);
					else
						results =  new ListArrayResults(columns, rs, dbHelper);
				} catch(ConvertException e) {
					SQLException sqle = new SQLException("Could not convert column");
					sqle.initCause(e);
					throw sqle;
				}
            }
            if(log.isDebugEnabled()) log.debug("Created " + results
                    + (hints.lazyAccess || !(results instanceof ScrollableResults) ? ""
                    : " with " + ((ScrollableResults)results).size() + " rows and")
                    + " with columns: " + simple.text.StringUtils.toStringList(columns));
            return results;
        }
    };

    public static CursorTransform getDefaultCursorTransform() {
        return defaultCursorTransform;
    }

    @Override
	public Call clone() {
        try {
			Call call = (Call) super.clone();
			if(this.arguments != null) {
				call.arguments = this.arguments.clone();
				for(int i = 0; i < call.arguments.length; i++)
					call.arguments[i] = call.arguments[i].clone();
			}
			return call;
        } catch(CloneNotSupportedException e) {
            return null;
        }
    }
    /**
     * Creates and configures a call from xml. Use the static form so that we can control which Class is used
     * based on the xml.
     * @param builder
     * @return the new Call
     * @throws ConvertException
     * @throws SAXException
     */
    public static Call readXML(ObjectBuilder builder) throws ConvertException, SAXException {
        Class<?> callClass = builder.getAttr("callClass", Class.class);
        Call call;
        if(callClass != null) {
            try {
                call = ReflectionUtils.createObject(callClass, Call.class, (Object[])null);
            } catch(ClassCastException e) {
                log.warn("Could not instantiate custom call class '" + callClass + "'", e);
                call = new Call();
            } catch(InstantiationException e) {
                log.warn("Could not instantiate custom call class '" + callClass + "'", e);
                call = new Call();
            } catch (IllegalAccessException e) {
                log.warn("Could not instantiate custom call class '" + callClass + "'", e);
                call = new Call();
            } catch(IllegalArgumentException e) {
                log.warn("Could not instantiate custom call class '" + callClass + "'", e);
                call = new Call();
			} catch(InvocationTargetException e) {
                log.warn("Could not instantiate custom call class '" + callClass + "'", e);
                call = new Call();
			}
        } else {
            call = new Call();
        }
        call.readCall(builder);
        return call;
    }

    protected ConnectionHolder getConnectionHolder(String dataSourceName) {
    	if(dataSourceName == null) {
        	try {
        		DataLayerMgr.getGlobalDataLayer().getDataSourceFactory().getDataSource(dataSourceName);
        	} catch(DataSourceNotFoundException e) {
        		return new NullConnectionHolder();
        	}
        }
        return new DataSourceConnectionHolder(DataLayerMgr.getGlobalDataLayer(), dataSourceName);
    }
    public void readCall(ObjectBuilder builder) throws ConvertException, SAXException {
        setSql(builder.getAttrValue("sql"));
        String dataSourceName = builder.getAttrValue("dataSource");
        if(dataSourceName == null) {
        	ObjectBuilder parentBuilder = builder.getParentNode();
        	if(parentBuilder != null)
        		dataSourceName = parentBuilder.getAttrValue("dataSource");
        }
        setDataSourceName(dataSourceName);
        Integer mr = builder.getAttr("maxRows", Integer.class);
		if(mr != null)
			setMaxRows(mr);
		Integer fs = builder.getAttr("fetchSize", Integer.class);
		if(fs != null)
			setFetchSize(fs);
        Double queryTimeout = builder.getAttr("queryTimeout", Double.class);
        if(queryTimeout != null)
        	setQueryTimeout(queryTimeout);
        else {
        	ObjectBuilder parentBuilder = builder.getParentNode();
        	while(parentBuilder != null) {
        		queryTimeout = parentBuilder.getAttr("queryTimeout", Double.class);
        		if(queryTimeout != null) {
                	setQueryTimeout(queryTimeout);
                	break;
        		}
        		parentBuilder = parentBuilder.getParentNode();
            }
        }
        Long logThreshhold = builder.getAttr("logThreshhold", Long.class);
        if(logThreshhold != null)
        	setLogExecuteTimeThreshhold(logThreshhold);
        else {
        	ObjectBuilder parentBuilder = builder.getParentNode();
        	while(parentBuilder != null) {
        		logThreshhold = parentBuilder.getAttr("logThreshhold", Long.class);
        		if(logThreshhold != null) {
        			setLogExecuteTimeThreshhold(logThreshhold);
                	break;
        		}
        		parentBuilder = parentBuilder.getParentNode();
            }
        }
        
        String con = builder.getAttrValue("concurrency");
        if(con != null && ConfigLoader.WRITABLE_SET.contains(con.toUpperCase())) setResultSetConcurrency(ResultSet.CONCUR_UPDATABLE);
        String scroll = builder.getAttrValue("scrollable");
        if(scroll != null && ConfigLoader.SCROLLABLE_SET.contains(scroll.toUpperCase())) setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE);
        else if(scroll != null && ConfigLoader.SCROLLABLE_SENSITIVE_SET.contains(scroll.toUpperCase())) setResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);

        List<Argument> arguments = new ArrayList<Argument>();
        ConnectionHolder ch = getConnectionHolder(dataSourceName);
        try {
        	if(builder.getTag().equalsIgnoreCase("query")) {
	            arguments.add(readCursor(builder));
	            ObjectBuilder[] pnodes = builder.getSubNodes("parameter");
	            if(pnodes != null) {
	                for(ObjectBuilder node : pnodes) {
	                    arguments.add(readParameter(node, ch));
	                }
	            }
	        } else {
	            Parameter p = new Parameter();
	            p.setIn(false);
	            p.setOut(true);
	            p.setPropertyName(builder.getAttrValue("property"));
	            arguments.add(p);
		        ObjectBuilder[] pnodes = builder.getSubNodes();
		        if(pnodes != null) {
		            for(ObjectBuilder node : pnodes) {
		                if(node.getTag().equalsIgnoreCase("parameter"))
		                    arguments.add(readParameter(node, ch));
		                else if(node.getTag().equalsIgnoreCase("cursor"))
		                    arguments.add(readCursor(node));
		                else
		                    throw new ConvertException("Unrecognized element '" + node.getTag() + "'", Argument.class, node);
		            }
		        }
	        }
        } finally {
        	ch.closeConnection();
        }

        setArguments(arguments.toArray(new Argument[arguments.size()]));
    }

	public static Cursor readCursor(ObjectBuilder builder) throws SAXException {
        Cursor c = new Cursor();
        Boolean all = builder.getAttr("allColumns", Boolean.class);
        if(all != null) c.setAllColumns(all);
        c.setPropertyName(builder.getAttrValue("property"));
        Integer fetchSize = builder.getAttr("fetchSize", Integer.class);
        if(fetchSize != null) c.setFetchSize(fetchSize);
        Boolean lazy = builder.getAttr("lazyAccess", Boolean.class);
        if(lazy != null) c.setLazyAccess(lazy);
        Boolean updateable = builder.getAttr("updateable", Boolean.class);
        if(updateable != null) c.setUpdateable(updateable);
        ObjectBuilder[] subNodes = builder.getSubNodes();
        Map<String,Column> colMap =  new HashMap<String,Column>();
        List<Column> columns = new ArrayList<Column>();
        List<Column> derived = new ArrayList<Column>();
        for(ObjectBuilder node : subNodes) {
        	if(node.getTag().equals("column")) {
	            Column col = new Column();
	            String id = node.getAttrValue("column");
	            if(id != null && id.trim().length() == 0) {
	                try {
	                    col.setIndex(Integer.parseInt(id));
	                } catch(NumberFormatException nfe) {
	                    col.setName(id);
	                }
	            }
	            col.setPropertyName(node.getAttrValue("property"));
	            col.setFormat(node.getAttrValue("format"));
	            colMap.put(col.getPropertyName(), col);
	            columns.add(col);
        	} else if(node.getTag().equals("convert")) {
	            ConvertedColumn col = new ConvertedColumn();
	            String to = node.getAttrValue("to");
	            if(to != null)
					try {
						col.setToClass(Class.forName(to));
					} catch(ClassNotFoundException e) {
						throw new SAXException("Invalid class name for convert column, '" + to + "' does not exist");
					}
	            String id = node.getAttrValue("column");
	            if(id != null && id.trim().length() == 0) {
	                try {
	                    col.setIndex(Integer.parseInt(id));
	                } catch(NumberFormatException nfe) {
	                    col.setName(id);
	                }
	            }
	            col.setPropertyName(node.getAttrValue("property"));
	            col.setFormat(node.getAttrValue("format"));
	            colMap.put(col.getPropertyName(), col);
	            columns.add(col);
        	} else if(node.getTag().equals("derived")) {
        		DerivedColumn dc = new DerivedColumn();
                ObjectBuilder[] pnodes = node.getSubNodes();
                if(pnodes != null) {
                    Object[] parts = new Object[pnodes.length];
                    String[] formats = new String[pnodes.length];
                    for(int k = 0; k < pnodes.length; k++) {
                        if(pnodes[k].getTag().equalsIgnoreCase("text")) {
                            parts[k] = pnodes[k].getAttrValue("value");
                        } else if(pnodes[k].getTag().equalsIgnoreCase("property")) {
                            parts[k] = colMap.get(pnodes[k].getAttrValue("name"));
                            formats[k] = pnodes[k].getAttrValue("format");
                        }
                    }
                    dc.setParts(parts);
                    dc.setFormats(formats);
                }
                dc.setPropertyName(node.getAttrValue("property"));
                derived.add(dc);
        	}
        }
        Column[] cols = new Column[columns.size() + derived.size()];
        //now sort the columns
        int i = 0, p = 0;
        for(Column col : columns) {
        	if(col.getIndex() > 0) {
        		int k = col.getIndex() - 1;
        		while(cols[k] != null && cols[k].getIndex() <= col.getIndex()) {
        			if(++k >= columns.size())
        				throw new SAXException("Invalid column index " + col.getIndex() + " at position " + (p+1));
        		}
        		int l = k;
        		while(cols[l] != null) {
        			if(++l >= columns.size())
        				throw new SAXException("Invalid column index " + col.getIndex() + " at position " + (p+1));
        		}
        		for(; l > k; l--)
        			cols[l] = cols[l-1];
        		cols[k] = col;
        	} else {
        		while(cols[i] != null) {
        			if(++i >= columns.size())
        				throw new SAXException("Could not sort columns");
        		}
        		cols[i] = col;
        	}

        	p++;
        }
        for(i = 0; i < columns.size(); i++)
        	cols[i].setIndex(i+1);
        for(Column dc : derived) {
        	dc.setIndex(i+1);
        	cols[i++] = dc;
        }
        // read groups
        String[] tmp = builder.getAttr("groups", String[].class);
        if(tmp != null) {
            Object[] groups = new Object[tmp.length];
            for(int k = 0; k < tmp.length; k++) {
                try {
                    groups[k] = new Integer(tmp[k]);
                } catch(NumberFormatException nfe) {
                    groups[k] = tmp[k];
                }
            }
            c.setGroups(groups);
        }
        // read totals
        tmp = builder.getAttr("totals", String[].class);
        if(tmp != null) {
            Object[] totals = new Object[tmp.length];
            for(int k = 0; k < tmp.length; k++) {
                try {
                    totals[k] = new Integer(tmp[k]);
                } catch(NumberFormatException nfe) {
                    totals[k] = tmp[k];
                }
            }
            c.setTotals(totals);
        }
        c.setColumns(cols);
        return c;
    }

	public static Parameter readParameter(ObjectBuilder builder, ConnectionHolder ch) throws ConvertException {
        Parameter p = new Parameter();
        String direction = builder.getAttrValue("direction");
        if("IN".equalsIgnoreCase(direction)) {
            p.setIn(true);
            p.setOut(false);
        } else if("INOUT".equalsIgnoreCase(direction)) {
            p.setIn(true);
            p.setOut(true);
        } else if("OUT".equalsIgnoreCase(direction)) {
            p.setIn(false);
            p.setOut(true);
        }
        String type = builder.getAttrValue("type");
        String typeName = builder.getAttrValue("typeName");
        try {
			p.setSqlType(SQLTypeUtils.getSqlType(type, typeName, ch));
		} catch(IllegalArgumentException e) {
			throw new ConvertException("Could not determine SQLType", SQLType.class, type, e);
		} catch(SQLException e) {
			throw new ConvertException("Could not determine SQLType", SQLType.class, type, e);
		} catch(DataLayerException e) {
			throw new ConvertException("Could not determine SQLType", SQLType.class, type, e);
		}
        p.setPropertyName(builder.getAttrValue("property"));
        p.setDefaultValue(builder.getAttrValue("defaultValue"));
        Boolean req = builder.getAttr("required", Boolean.class);
        p.setRequired(req != null && req);
        return p;
    }

    public void writeXML(String tagName, XMLBuilder builder) throws SAXException, ConvertException {
    	writeXML(tagName, builder, null);
    }

    public void writeXML(String tagName, XMLBuilder builder, Map<String,String> extraAtts) throws SAXException, ConvertException {
        Cursor cursor = null;
        Parameter affected = null;
        if(getArguments() != null && getArguments().length > 0) {
            if(getArguments()[0] instanceof Cursor)
                cursor = (Cursor)getArguments()[0];
            else if(getArguments()[0] instanceof Parameter)
                affected = (Parameter)getArguments()[0];
        }
        if(tagName == null) {
            if(cursor != null) {
                tagName = "query";
            } else {
                tagName = "call";
            }

        }
        Map<String,String> atts = new HashMap<String, String>();
        if(extraAtts != null) atts.putAll(extraAtts);
        //atts.put("id", ???);
        atts.put("dataSource", getDataSourceName());
        atts.put("sql", getSql());
        if(getMaxRows() > 0)
            atts.put("maxRows", String.valueOf(getMaxRows()));
		if(getFetchSize() > 0)
			atts.put("fetchSize", String.valueOf(getFetchSize()));
        if(getQueryTimeout() > 0)
            atts.put("queryTimeout", String.valueOf(getQueryTimeout()));
        if(getLogExecuteTimeThreshhold() >= 0)
        	atts.put("logThreshhold", String.valueOf(getLogExecuteTimeThreshhold()));
        
        switch(getResultSetType()) {
            case ResultSet.TYPE_SCROLL_INSENSITIVE:
                atts.put("scrollable", "yes");
                break;
            case ResultSet.TYPE_SCROLL_SENSITIVE:
                atts.put("scrollable", "server");
                break;
        }
        switch(getResultSetConcurrency()) {
            case ResultSet.CONCUR_UPDATABLE:
                atts.put("concurrency", "updatable");
                break;
        }
        if(cursor != null) {
            writeQueryStartTag(tagName, atts, cursor, builder);
        } else {
            if(affected != null && affected.getPropertyName() != null && affected.getPropertyName().trim().length() > 0)
                atts.put("property", affected.getPropertyName());
            writeCallStartTag(tagName, atts, builder);
        }
		writeArgumentsXml(builder);
		builder.elementEnd(tagName);
	}

	public void writeArgumentsXml(XMLBuilder builder) throws ConvertException, SAXException {
		Argument[] arguments = getArguments();
		if(arguments != null) {
			for(int i = 1; i < arguments.length; i++) {
				if(arguments[i] instanceof Cursor) {
					writeCursorStartTag("cursor", (Cursor) arguments[i], builder);
                    builder.elementEnd("cursor");
				} else if(arguments[i] instanceof Parameter) {
					Parameter p = (Parameter) arguments[i];
                    if(p.isIn() && p.isOut())
						builder.attributeForNext("direction", "inout");
                    else if(!p.isIn() && p.isOut())
						builder.attributeForNext("direction", "out");
                    if(p.getSqlType() != null) {
						String s = ConvertUtils.getStringSafely(p.getSqlType());
						builder.attributeForNext("type", s);
						String tn = p.getSqlType().getTypeName();
						if(!StringUtils.isBlank(tn) && !s.regionMatches(true, 0, tn, 0, tn.length()))
							builder.attributeForNext("typeName", tn);
                    }
					builder.attributeForNext("property", p.getPropertyName());
                    if(p.getDefaultValue() != null)
						builder.attributeForNext("defaultValue", ConvertUtils.convert(String.class, p.getDefaultValue()));
                    if(p.isRequired())
						builder.attributeForNext("required", "true");
					builder.element("parameter");
                }
            }
        }
    }

    protected void writeQueryStartTag(String tagName, Map<String, String> atts, Cursor cursor, XMLBuilder builder) throws ConvertException, SAXException {
		writeCursorStartTag(tagName, cursor, builder);
	}

	protected void writeCallStartTag(String tagName, Map<String, String> atts, XMLBuilder builder) throws SAXException {
        builder.elementStart(tagName, atts);
	}

	protected void writeCursorStartTag(String tagName, Cursor cursor, XMLBuilder builder) throws ConvertException, SAXException {
        if(cursor.isAllColumns())
			builder.attributeForNext("allColumns", "true");
        if(cursor.isLazyAccess())
			builder.attributeForNext("lazyAccess", "true");
        if(cursor.isUpdateable())
			builder.attributeForNext("updateable", "true");
        if(cursor.getFetchSize() > 0)
			builder.attributeForNext("fetchSize", "" + cursor.getFetchSize());
        if(cursor.getGroups() != null && cursor.getGroups().length > 0)
			builder.attributeForNext("groups", ConvertUtils.convert(String.class, cursor.getGroups()));
        if(cursor.getTotals() != null && cursor.getTotals().length > 0)
			builder.attributeForNext("totals", ConvertUtils.convert(String.class, cursor.getTotals()));
		builder.elementStart(tagName);
        Column[] columns = cursor.getColumns();
        if(columns != null)
	        for(int i = 0; i < columns.length; i++) {
				builder.attributeForNext("property", columns[i].getPropertyName());
	            if(columns[i] instanceof DerivedColumn) {
					builder.elementStart("derived");
	                DerivedColumn dc = (DerivedColumn)columns[i];
	                Object[] parts = dc.getParts();
	                String[] formats = dc.getFormats();
	                final String[] propAtts = new String[] {"name","format"};
	                final String[] textAtts = new String[] {"value"};
	                if(parts != null)
		                for(int k = 0; k < parts.length; k++) {
		                    if(parts[k] instanceof Column) {
		                        builder.elementStart("property", propAtts,
		                                ((Column)parts[k]).getPropertyName(),
		                                (formats != null && formats.length > k ? formats[k] : ""));
		                        builder.elementEnd("property");
		                    } else if(parts[k] != null) {
		                        builder.elementStart("text", textAtts, parts[k].toString());
		                        builder.elementEnd("text");
		                    }
		                }
	                builder.elementEnd("derived");
	            } else {
	                if(columns[i].getFormat() != null && columns[i].getFormat().trim().length() > 0)
						builder.attributeForNext("format", columns[i].getFormat());
	                if(columns[i].getName() != null && columns[i].getName().trim().length() > 0) {
						builder.attributeForNext("column", columns[i].getName());
	                } else if(columns[i].getIndex() != i + 1) {
						builder.attributeForNext("column", "" + columns[i].getIndex());
	                }
	                String colTagName;
	                if(columns[i] instanceof ConvertedColumn) {
	                	colTagName = "convert";
	                	Class<?> toClass = ((ConvertedColumn)columns[i]).getToClass();
	                	if(toClass != null)
							builder.attributeForNext("to", toClass.getName());
	                } else
	                	colTagName = "column";
					builder.element(colTagName);
	            }
	        }
    }
    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof Call) {
    		Call c = (Call)obj;
    		return ConvertUtils.areEqual(sql, c.sql)
    			&& ConvertUtils.areEqual(dataSourceName, c.dataSourceName)
    			&& maxRows == c.maxRows
    			&& outs == c.outs
    			&& Arrays.deepEquals(arguments, c.arguments);
    	}
    	return false;
    }

	@Override
	public int hashCode() {
		return (sql == null ? 0 : sql.hashCode());
	}

	public static long getDefaultLogExecuteTimeThreshhold() {
		return defaultLogExecuteTimeThreshhold;
	}

	public static void setDefaultLogExecuteTimeThreshhold(long logExecuteTimeThreshhold) {
		Call.defaultLogExecuteTimeThreshhold = logExecuteTimeThreshhold;
	}

	/* (non-Javadoc)
	 * @see simple.db.CallMXBean#getLogExecuteTimeThreshhold()
	 */
	@Override
	public long getLogExecuteTimeThreshhold() {
		return logExecuteTimeThreshhold < 0 ? defaultLogExecuteTimeThreshhold : logExecuteTimeThreshhold;
	}

	/* (non-Javadoc)
	 * @see simple.db.CallMXBean#setLogExecuteTimeThreshhold(long)
	 */
	@Override
	public void setLogExecuteTimeThreshhold(long logExecuteTimeThreshhold) {
		this.logExecuteTimeThreshhold = logExecuteTimeThreshhold;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isJmxEnabled() {
		return jmxEnabled;
	}

	public void setJmxEnabled(boolean jmxEnabled) {
		this.jmxEnabled = jmxEnabled;
		if(!jmxEnabled && jmxObjectName != null) {
			// deregister
			try {
				if(ManagementFactory.getPlatformMBeanServer().isRegistered(jmxObjectName))
					ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmxObjectName);
			} catch(MBeanRegistrationException e) {
				log.warn("Could not unregister MBean for call '" + getLabel() + "'", e);
			} catch(InstanceNotFoundException e) {
				log.warn("Could not unregister MBean for call '" + getLabel() + "'", e);
			}
			jmxObjectName = null;
		}
	}

	public void initialize() {
		if(isJmxEnabled() && !StringUtils.isBlank(getLabel()) && jmxObjectName == null) {
			// register
			try {
				jmxObjectName = new ObjectName("simple.db:Type=Call,Id=" + getLabel());
				if(ManagementFactory.getPlatformMBeanServer().isRegistered(jmxObjectName))
					ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmxObjectName);
				ManagementFactory.getPlatformMBeanServer().registerMBean(this, jmxObjectName);
			} catch(JMException e) {
				log.warn("Could not register MBean for call '" + getLabel() + "'", e);
				jmxObjectName = null;
			}
		}
	}

	public void destroy() {
		if(jmxObjectName != null) {
			// de-register
			try {
				if(ManagementFactory.getPlatformMBeanServer().isRegistered(jmxObjectName))
					ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmxObjectName);
			} catch(JMException e) {
				log.warn("Could not unregister MBean for call '" + getLabel() + "'", e);
				jmxObjectName = null;
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see simple.db.CallMXBean#getTotalExecutionTime()
	 */
	@Override
	public long getTotalExecutionTime() {
		Number n = sampleExecutionTimes.getGrandTotal();
		return n != null ? n.longValue() : 0;
	}

	/* (non-Javadoc)
	 * @see simple.db.CallMXBean#getExecutionCount()
	 */
	@Override
	public long getExecutionCount() {
		return sampleExecutionTimes.getGrandSize();
	}

	/* (non-Javadoc)
	 * @see simple.db.CallMXBean#getRecentExecutionTime()
	 */
	@Override
	public long getRecentExecutionTime() {
		Number n = sampleExecutionTimes.getSampleTotal();
		return n != null ? n.longValue() : 0;
	}

	/* (non-Javadoc)
	 * @see simple.db.CallMXBean#getRecentExecutionCount()
	 */
	@Override
	public int getRecentExecutionCount() {
		return sampleExecutionTimes.getSampleSize();
	}

	/* (non-Javadoc)
	 * @see simple.db.CallMXBean#isTrackingExecuteTime()
	 */
	@Override
	public boolean isTrackingExecuteTime() {
		return trackingExecuteTime;
	}

	/* (non-Javadoc)
	 * @see simple.db.CallMXBean#setTrackingExecuteTime(boolean)
	 */
	@Override
	public void setTrackingExecuteTime(boolean trackingExecuteTime) {
		this.trackingExecuteTime = trackingExecuteTime;
	}

	public double getAvgExecutionTime() {
		Number n = sampleExecutionTimes.getGrandTotal();
		return n != null ? n.doubleValue() / sampleExecutionTimes.getGrandSize() : Double.NaN;
	}

	public double getRecentAvgExecutionTime() {
		Number n = sampleExecutionTimes.getSampleTotal();
		return n != null ? n.doubleValue() / sampleExecutionTimes.getSampleSize() : Double.NaN;
	}

	public int getFetchSize() {
		return fetchSize;
	}

	public void setFetchSize(int fetchSize) {
		this.fetchSize = fetchSize;
	}

	public Sorter getSorter() throws DataLayerException {
		if(sorter == null)
			sorter = new Sorter();
		return sorter;
	}

	public static boolean isMonitorStatements() {
		return monitorStatements;
	}

	public static void setMonitorStatements(boolean monitorStatements) {
		Call.monitorStatements = monitorStatements;
	}
}
