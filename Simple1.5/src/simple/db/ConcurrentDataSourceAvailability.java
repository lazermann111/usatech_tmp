package simple.db;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentDataSourceAvailability implements DataSourceAvailability {
	protected final Map<String,Boolean> dataSources = new ConcurrentHashMap<String, Boolean>();
	protected class AvailabilityIterator implements Iterator<String> {
		protected final Iterator<Map.Entry<String, Boolean>> delegate;
		protected final boolean match;
		protected String next;
		public AvailabilityIterator(boolean match) {
			this.delegate = dataSources.entrySet().iterator();
			this.match = match;
			this.next = findNext();
		}
		protected String findNext() {
			while(delegate.hasNext()) {
				Map.Entry<String, Boolean> entry = delegate.next();
				if(entry.getValue().booleanValue() == match)
					return entry.getKey();
			}
			return null;
		}
		public boolean hasNext() {
			return next != null;
		}
		public String next() {
			String ret = next;
			next = findNext();
			return ret;
		}
		public void remove() {
			throw new UnsupportedOperationException();			
		}
	}

	public Iterable<String> getAvailables() {
		return new Iterable<String>() {
			public Iterator<String> iterator() {
				return new AvailabilityIterator(true);
			}
		};
	}

	public Iterable<String> getUnavailables() {
		return new Iterable<String>() {
			public Iterator<String> iterator() {
				return new AvailabilityIterator(false);
			}
		};
	}

	public Iterable<String> getAll() {
		return dataSources.keySet();
	}
	
	public void remove(String dataSourceName) {
		dataSources.remove(dataSourceName);
	}
	
	public boolean isAvailable(String dataSourceName) {
		Boolean value = dataSources.get(dataSourceName);
		return value != null && value.booleanValue();
	}

	public void setAvailable(String dataSourceName, boolean available) {
		if(dataSourceName == null)
			throw new NullPointerException();
		Boolean old = dataSources.put(dataSourceName, available);
		if(old == null || old != available)
			availabilityChanged(dataSourceName, available);
	}

	protected void availabilityChanged(String dataSourceName, boolean available) {
	}
}
