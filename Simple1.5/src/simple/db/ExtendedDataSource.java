/**
 *
 */
package simple.db;

import java.sql.SQLException;

import javax.sql.DataSource;

/**
 * @author Brian S. Krug
 *
 */
public interface ExtendedDataSource extends DataSource {

	/** Gets the url of the database for this DataSource
	 * @return
	 */
	public String getUrl() ;

	/**Gets the username of this DataSource
	 * @return
	 */
	public String getUsername() ;

	/** Gets the password of this DataSource
	 * @return
	 */
	public String getPassword() ;

	public void setUsername(String username);

	public void setPassword(String password);
	  /**
	   * <p>Attempts to establish a connection with the data source that
	   * this <code>DataSource</code> object represents.
	   *
	   * @return  a connection to the data source
	   * @exception SQLException if a database access error occurs
	   */
	//public Connection getConnection(Properties info) throws SQLException;
	
	public void close() throws SQLException ;

	public boolean isAvailable();
}
