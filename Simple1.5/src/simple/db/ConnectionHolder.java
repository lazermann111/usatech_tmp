package simple.db;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionHolder {
	public Class<? extends Connection> getConnectionClass() throws DataSourceNotFoundException ;
	public Connection getConnection() throws SQLException, DataSourceNotFoundException ;
	public boolean closeConnection() ;
	public Connection reconnect() throws SQLException, DataSourceNotFoundException ;

	public void commitOrRollback(boolean commit) throws SQLException;
}
