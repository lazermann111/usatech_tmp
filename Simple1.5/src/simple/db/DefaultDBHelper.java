/*
 * DefaultDBHelper.java
 *
 * Created on May 13, 2003, 9:21 AM
 */

package simple.db;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Types;
import java.util.Collection;
import java.util.Iterator;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.ParameterException.ParameterExceptionType;
import simple.io.BinaryStream;
import simple.io.IOUtils;
import simple.io.Log;
import simple.sql.ArraySQLType;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;

/**
 * An empty implementation of the DBHelper Interface. Its translate methods
 * simply return the object passed to it and the create methods return null.
 *
 * @author  Brian S. Krug
 */
public class DefaultDBHelper implements DBHelper {
	private static final Log log = Log.getLog();
    
    /** Creates a new instance of DefaultDBHelper */
    public DefaultDBHelper() {
    }

    /**
	 * @throws SQLException  
	 * @throws ConvertException  
	 */
	protected Object createArray(Connection conn, SQLType sqlType) throws SQLException, ConvertException {
        return null; /*new Array() {

        };*/
    }

    /**
	 * @throws SQLException  
	 * @throws ConvertException  
	 */
	protected Object createRef(Connection conn, SQLType sqlType) throws SQLException, ConvertException {
        return null; /*new Ref() {

        };*/
    }

    /**
	 * @throws SQLException  
	 * @throws ConvertException  
	 */
	protected Object createBlob(Connection conn, SQLType sqlType) throws SQLException, ConvertException {
        return null; /*new Blob() {

        };*/
    }

    /**
	 * @throws SQLException  
	 * @throws ConvertException  
	 */
	protected Object createClob(Connection conn, SQLType sqlType) throws SQLException, ConvertException {
        return null; /*new Clob() {

        };*/
    }

    /**
	 * @throws SQLException  
	 * @throws ConvertException  
	 */
	protected Object createStruct(Connection conn, SQLType sqlType) throws SQLException, ConvertException {
        return null; /*new Struct() {

        };*/
    }

    /**
	 * @throws SQLException  
	 * @throws ConvertException  
	 */
	protected Object translateRef(Connection conn, Object ref, SQLType sqlType) throws SQLException, ConvertException {
        return ref;
    }

    protected Object translateArray(Connection conn, Object array, SQLType sqlType) throws SQLException, ConvertException {
    	if(array == null)
    		return null;
    	SQLType componentSqlType;
    	if(sqlType instanceof ArraySQLType) {
    		componentSqlType = ((ArraySQLType)sqlType).getComponentType();
    	} else {
    		componentSqlType = new SQLType(Types.OTHER);
    	}
    	return new DefaultArray(toElements(conn, array, componentSqlType.getTypeCode(), componentSqlType.getTypeName()), componentSqlType);
    }

    /* A utility method for converting to a java array
    *
    */
    protected Object[] toElements(Connection conn, Object array, int componentSqlType, String componentTypeName) throws SQLException, ConvertException {
        Collection<?> coll = ConvertUtils.convert(Collection.class, array);
        if(coll == null) return new Object[0];
        Object[] elements = new Object[coll.size()];
        int i = 0;
        for(Object e : coll) {
            elements[i++] = translateArrayElement(conn, e, componentSqlType, componentTypeName);
        }
        return elements;
    }

    protected Object translateArrayElement(Connection conn, Object e, int componentSqlType, String componentTypeName) throws SQLException, ConvertException {
        switch(componentSqlType) {
            case Types.ARRAY: 
            	SQLType componentType;
            	try {
            		componentType = new SQLType(SQLTypeUtils.getTypeCode(componentTypeName), componentTypeName);
            	} catch(NoSuchFieldException ex) {
            		componentType = new SQLType(Types.OTHER, componentTypeName);
            	}
            	return translateArray(conn, e, new ArraySQLType(componentType));
            case Types.BLOB: return translateBlob(conn, e, new SQLType(componentSqlType, componentTypeName));
            case Types.CLOB: return translateClob(conn, e, new SQLType(componentSqlType, componentTypeName));
            case Types.REF: return translateRef(conn, e, new SQLType(componentSqlType, componentTypeName));
            case Types.STRUCT: return translateStruct(conn, e, new SQLType(componentSqlType, componentTypeName));
            default:
                return translateValue(conn, e, componentSqlType);
        }
    }

    protected Object translateBlob(Connection conn, Object blob, SQLType sqlType) throws SQLException, ConvertException {
    	if(blob instanceof Blob) return blob;
        Blob specificBlob = (Blob)createBlob(conn, sqlType);
        if(specificBlob == null) return blob;
		OutputStream out = specificBlob.setBinaryStream(0L);
		try {
			InputStream in = ConvertUtils.convert(InputStream.class, blob);
			try {
				IOUtils.readInto(in, out, 1024);
				out.flush();
			} catch(java.io.IOException ioe) {
				ConvertException ce = new ConvertException("Could not read into blob", specificBlob.getClass(), blob);
				ce.initCause(ioe);
				throw ce;
			} finally {
				try {
					in.close();
				} catch(IOException e) {
					// ignore
				}
			}
		} finally {
			try {
				out.close();
			} catch(IOException e) {
				// ignore
			}
        }
        return specificBlob;
    }

    protected Object translateClob(Connection conn, Object clob, SQLType sqlType) throws SQLException, ConvertException {
    	if(clob instanceof Clob) return clob;
        Clob specificClob = (Clob)createClob(conn, sqlType);
        if(specificClob == null) return clob;
        Writer writer = specificClob.setCharacterStream(0L);
        try {
			Reader reader = ConvertUtils.convert(Reader.class, clob);
			try {
				IOUtils.readInto(reader, writer, 1024);
				writer.flush();
			} catch(java.io.IOException ioe) {
				ConvertException ce = new ConvertException("Could not read into clob", specificClob.getClass(), clob);
				ce.initCause(ioe);
				throw ce;
			} finally {
				try {
					reader.close();
				} catch(IOException e) {
					// ignore
				}
			}
		} finally {
			try {
				writer.close();
			} catch(IOException e) {
				// ignore
			}
		}
        return specificClob;
    }

    /**
	 * @throws SQLException  
	 * @throws ConvertException  
	 */
	protected Object translateStruct(Connection conn, Object struct, SQLType sqlType) throws SQLException, ConvertException {
        return struct;
    }

    public Object[] getValues(ResultSet rs, Column[] columns, boolean connected) throws SQLException, ConvertException {
        Object[] values = new Object[columns.length];
        for(int i = 0; i < values.length; i++) values[i] = getValue(rs, columns[i], connected);
        return values;
    }

    public Object getValue(ResultSet rs, Column column, boolean connected) throws SQLException, ConvertException {
        if(column instanceof DerivedColumn) {
            DerivedColumn dc = (DerivedColumn)column;
            StringBuilder sb = new StringBuilder();
            Object[] parts = dc.getParts();
            for(int i = 0; i < parts.length; i++) {
                if(parts[i] instanceof Column) sb.append(ConvertUtils.formatObject(getValue(rs, (Column)parts[i], connected), dc.getFormats()[i]));
                else if(parts[i] != null) sb.append(parts[i].toString());
            }
            return sb.toString();
        } else if(column instanceof ConvertedColumn) {
        	Object value = rs.getObject(column.getIndex());
        	return ConvertUtils.convert(((ConvertedColumn)column).getToClass(), value);
        } else {
        	Object value = rs.getObject(column.getIndex());
        	if(!connected)
        		value = disconnectValue(value);
            return value;
        }
    }

    protected Object disconnectValue(Object value) throws SQLException {
    	if(value instanceof java.sql.Blob) {
    		java.sql.Blob blob = (java.sql.Blob)value;
    		value = blob.getBytes(1L, (int)blob.length());
    	} else if(value instanceof java.sql.Clob) {
    		java.sql.Clob clob = (java.sql.Clob)value;
    		value = clob.getSubString(1L, (int)clob.length());
    	} else if(value instanceof java.sql.Ref) {
    		java.sql.Ref ref = (java.sql.Ref)value;
    		Object o = ref.getObject();
    		if(o != value)
    			value = disconnectValue(o);
    		else
    			value = o;
    	} else if(value instanceof java.sql.Array) {
			value = ((java.sql.Array) value).getArray();
    	} else if(value instanceof java.sql.Struct) {

    	}
    	return value;
    }

    public void closeConnection(Connection conn) throws SQLException {
        conn.close();
    }

    protected Object[] translateStructAttributes(Connection conn, Object struct, SQLType sqlType) throws SQLException, ConvertException {
		if(struct instanceof Struct) return ((Struct)struct).getAttributes();
        else if(struct instanceof Object[]) return (Object[]) struct;
        else if(struct instanceof java.util.Collection<?>) return ((java.util.Collection<?>)struct).toArray();
        else if(struct.getClass().isArray()) {//primitive array
            return new simple.util.PrimitiveArrayList(struct).toArray();
        } else { //bean or map
        	try {
				return ReflectionUtils.toPropertyMap(struct).values().toArray();
			} catch (IntrospectionException e) {
				throw new ConvertException("Couldn't convert to attributes", Object[].class, struct, e);
			}
        }
	}

    protected Object[] translateArrayElements(Connection conn, Object array, SQLType sqlType) throws SQLException, ConvertException {
		return toElements(conn, array, Types.OTHER, null);
	}

    protected String findArrayTypeName(Connection conn, Object array) throws SQLException, ConvertException {
		if(array instanceof Array) {
            return findArrayTypeName(conn, ((Array)array).getBaseType());
        }
        Class<?> compClass = null;
        if(array.getClass().isArray()) {
            compClass = array.getClass().getComponentType();
        } else {
            Iterator<?> iter = ConvertUtils.asCollection(array).iterator();
            while(iter.hasNext()) {
                Object o = iter.next();
                if(o != null) {
                    compClass = o.getClass();
                    break;
                }
            }
            if(compClass == null) return null;
        }

        return findArrayTypeName(conn, simple.sql.SQLTypeUtils.getTypeCode(compClass));
	}

	@SuppressWarnings("unused")
	protected String findArrayTypeName(Connection conn, int componentSqlType) throws SQLException {
		String typeName = SQLTypeUtils.getTypeCodeName(componentSqlType);
		return typeName == null ? null : typeName + arraySuffixes[0];
	}

	protected static final String[] arraySuffixes = new String[] {
		"_LIST",
		"S",
		"_ARRAY"
	};

	protected int findSubtypeOfArray(Connection conn, String typeName) throws SQLException {
		for(String s : arraySuffixes)
			if(typeName.endsWith(s)) {
				try {
	                return SQLTypeUtils.getTypeCode(typeName.substring(0, typeName.length() - s.length()));
	            } catch(NoSuchFieldException e) {
	            }
			}
		throw new SQLException("Could not calculate array component type for '" + typeName +"'");
	}

	/**
	 * @throws SQLException  
	 * @throws ConvertException  
	 */
	protected Object translateValue(Connection conn, Object value, int typeCode) throws SQLException, ConvertException {
		Class<? extends Object> clz = SQLTypeUtils.getJdbcType(typeCode);
        if(clz != null) return ConvertUtils.convert(clz, value);
        else return value;
    }

	public SQLType translateSqlType(SQLType sqlType, String databaseTypeName, ConnectionHolder connHolder) throws SQLException, DataSourceNotFoundException {
		if(sqlType == null)
			return getCustomSqlType(new SQLType(Types.OTHER), databaseTypeName, connHolder);
		if(sqlType instanceof ArraySQLType) {
			((ArraySQLType)sqlType).setTypeName(databaseTypeName);
			return sqlType;
		}
		switch(sqlType.getTypeCode()) {
			case 0:
			case Types.ARRAY:
			case Types.STRUCT:
			case Types.REF:
				if(databaseTypeName == null || (databaseTypeName=databaseTypeName.trim()).length() == 0) {
					databaseTypeName = sqlType.getTypeName();
					if(databaseTypeName == null || (databaseTypeName=databaseTypeName.trim()).length() == 0)
						return sqlType; // No translation possible
				}
				return getCustomSqlType(sqlType, databaseTypeName, connHolder);
			default:
				setTypeName(sqlType, databaseTypeName);
				return sqlType;
		}
	}

	/**
	 * @param sqlType
	 * @param databaseTypeName
	 */
	protected void setTypeName(SQLType sqlType, String databaseTypeName) {
		if(databaseTypeName != null && (databaseTypeName=databaseTypeName.trim()).length() > 0)
			sqlType.setTypeName(databaseTypeName);
	}

	/**
	 * @param typeCode
	 * @param string
	 * @param conn
	 * @return
	 * @throws SQLException
	 * @throws DataSourceNotFoundException
	 */
	protected SQLType getCustomSqlType(SQLType sqlType, String databaseTypeName, ConnectionHolder connHolder) throws SQLException, DataSourceNotFoundException {
		if(databaseTypeName == null)
			return sqlType; //throw new SQLException("Neither Type Code nor Type Name was specified");
		Connection conn;
		try {
			conn = connHolder.getConnection();
		} catch(DataSourceNotFoundException e) {
			throw new SQLException("DataSourceNotFound", e);
		}
		if(conn == null)
			return sqlType; //throw new SQLException("Cannot determine the custom SQL Type with a null connection");
		ResultSet rs = conn.getMetaData().getUDTs(null, null, databaseTypeName, null);
		try {
			if(rs.next()) {
				// String typeName = rs.getString(3);
				switch(rs.getInt(5)) {
					case Types.STRUCT:
						// TODO: return StructSQLType(typeName,???);
						throw new SQLException("STRUCTs are not supported");
					case Types.JAVA_OBJECT:
						// TODO: return JavaObjectSQLType(typeName,rs.getString(4));
						throw new SQLException("JAVA_OBJECTs are not supported");
					case Types.ARRAY:
						throw new SQLException("ARRAYs are not supported");
					case Types.REF:
						throw new SQLException("JAVA_OBJECTs are not supported");

				}
			}
		} finally {
			rs.close();
		}
		throw new SQLException("Type '" + databaseTypeName + "' not found in list of UDTs");
	}

	public void setValue(ResultSet rs, Column column, Object value) throws SQLException, ConvertException {
		rs.updateObject(column.getIndex(), translateValue(rs.getStatement().getConnection(), value, column.getSqlType()));
	}

	public boolean reconnectNeeded(Connection conn, SQLException e) {
		try {
			return conn == null || conn.isClosed();
		} catch(SQLException e1) {
			return true;
		}
	}
	/**
	 * @see simple.db.DBHelper#getValue(java.sql.CallableStatement, int, simple.sql.SQLType)
	 */
	public Object getValue(CallableStatement callableStatement, int parameterIndex, SQLType sqlType) throws SQLException {
		return callableStatement.getObject(parameterIndex);
	}
	/**
	 * @see simple.db.DBHelper#registerOutParameter(java.sql.CallableStatement, int, simple.sql.SQLType)
	 */
	public void registerOutParameter(CallableStatement callableStatement, int parameterIndex, String parameterName, SQLType sqlType) throws SQLException {
	    if(log.isDebugEnabled()) 
	    	log.debug("Registering out param " + parameterIndex + " as type=" + sqlType.getTypeCode());
        if(sqlType.getTypeName() != null)
			callableStatement.registerOutParameter(parameterIndex, sqlType.getTypeCode(), sqlType.getTypeName());
        else
        	callableStatement.registerOutParameter(parameterIndex, sqlType.getTypeCode());
    }
	
	public void setInParameter(PreparedStatement preparedStatement, int parameterIndex, String parameterName, Object value, SQLType sqlType) throws SQLException, ParameterException {
		value = prepareValue(DBUnwrap.getRealConnection(preparedStatement.getConnection()), value, parameterName, sqlType);
		log.debug("Setting parameter " + parameterIndex + " to " + value + " (type=" + sqlType.getTypeCode() + ")");
        if(value != null) {
        	if(value instanceof CharacterStream) {
            	setCharacterStream(preparedStatement, parameterIndex, parameterName, (CharacterStream)value);
            } else if(value instanceof BinaryStream) {
            	setBinaryStream(preparedStatement, parameterIndex, parameterName, (BinaryStream) value);
            } else {
            	setObject(preparedStatement, parameterIndex, parameterName, value, sqlType);
            }
        } else {
            setNull(preparedStatement, parameterIndex, parameterName, sqlType);
        }
	}	
	
	protected Object prepareValue(Connection conn, Object value, String parameterName, SQLType sqlType) throws ParameterException {
		switch(sqlType.getTypeCode()) {
            case Types.ARRAY:
                try {
                	return translateArray(conn, value, sqlType);
				} catch (SQLException e) {
					ParameterException pe = new ParameterException("Could not retrieve the database ARRAY provided for the parameter '" + parameterName + "'", e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.OTHER);
                	throw pe;
				} catch (ConvertException e) {
					ParameterException pe = new ParameterException("Could not convert the parameter '" + parameterName + "' to an array", e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.CONVERSION);
                	throw pe;
				}
            case Types.BLOB:
                try {
                	return translateBlob(conn, value, sqlType);
				} catch (SQLException e) {
					ParameterException pe = new ParameterException("Could not retrieve the database BLOB provided for the parameter '" + parameterName + "'", e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.OTHER);
                	throw pe;
				} catch (ConvertException e) {
					ParameterException pe = new ParameterException("Could not convert the parameter '" + parameterName + "' to " + String.class.getName(), e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.CONVERSION);
                	throw pe;
				}
            case Types.CLOB:
                try {
                	return translateClob(conn, value, sqlType);
				} catch (SQLException e) {
					ParameterException pe = new ParameterException("Could not retrieve the database CLOB provided for the parameter '" + parameterName + "'", e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.OTHER);
                	throw pe;
				} catch (ConvertException e) {
					ParameterException pe = new ParameterException("Could not convert the parameter '" + parameterName + "' to " + String.class.getName(), e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.CONVERSION);
                	throw pe;
				}
            case Types.REF:
            	try {
            		return translateRef(conn, value, sqlType);
				} catch (SQLException e) {
					ParameterException pe = new ParameterException("Could not retrieve the database REF provided for the parameter '" + parameterName + "'", e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.OTHER);
                	throw pe;
				} catch (ConvertException e) {
					ParameterException pe = new ParameterException("Could not convert the parameter '" + parameterName + "' to " + String.class.getName(), e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.CONVERSION);
                	throw pe;
				}
            case Types.STRUCT:
                try {
                	return translateStruct(conn, value, sqlType);
				} catch (SQLException e) {
					ParameterException pe = new ParameterException("Could not retrieve the database STRUCT provided for the parameter '" + parameterName + "'", e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.OTHER);
                	throw pe;
				} catch (ConvertException e) {
					ParameterException pe = new ParameterException("Could not convert the parameter '" + parameterName + "' to an object", e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.CONVERSION);
                	throw pe;
				}
        	default:
        		try {
        			value = translateValue(conn, value, sqlType.getTypeCode());
        			//TODO: apply sizing for all relevant types - for now we just do Strings
        			if(value instanceof String && sqlType.getPrecision() != null) {
        				String s = (String)value;
        				if(s.length() > sqlType.getPrecision())
        					value = s.substring(0,sqlType.getPrecision());
        			}
        			return value;
				} catch (SQLException e) {
					ParameterException pe = new ParameterException("Could not convert the database value provided for the parameter '" + parameterName + "'", e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.OTHER);
                	throw pe;
				} catch (ConvertException e) {
					ParameterException pe = new ParameterException("Could not convert the parameter '" + parameterName + "'", e);
                	pe.setParameterName(parameterName);
                	pe.setType(ParameterExceptionType.CONVERSION);
                	throw pe;
				}

        }
		
	}

	/**
	 * @throws SQLException  
	 * @throws ParameterException  
	 */
	protected void setNull(PreparedStatement preparedStatement, int parameterIndex, String parameterName, SQLType sqlType) throws SQLException, ParameterException {
		if(sqlType.getTypeName() != null) 
			preparedStatement.setNull(parameterIndex, sqlType.getTypeCode(), sqlType.getTypeName());
        else 
        	preparedStatement.setNull(parameterIndex, sqlType.getTypeCode());
	}
	
	/**
	 * @throws SQLException  
	 * @throws ParameterException  
	 */
	protected void setObject(PreparedStatement preparedStatement, int parameterIndex, String parameterName, Object value, SQLType sqlType) throws SQLException, ParameterException {
		preparedStatement.setObject(parameterIndex, value, sqlType.getTypeCode());
	}
	
	/**
	 * @throws SQLException  
	 * @throws ParameterException  
	 */
	protected void setBinaryStream(PreparedStatement preparedStatement, int parameterIndex, String parameterName, BinaryStream stream) throws SQLException, ParameterException {
		long length = stream.getLength();
		try {
	    	try {
	    		preparedStatement.setBinaryStream(parameterIndex, stream.getInputStream(), length);
	    	} catch(AbstractMethodError e) {// assume jdbc driver is previous to java 1.6
	    		if(length <= Integer.MAX_VALUE)
	    			preparedStatement.setBinaryStream(parameterIndex, stream.getInputStream(), (int)length);
	    		else
	    			throw new ParameterException("Length of stream (" + length + ") for parameter " + parameterName + " is greater than the max integer value and is NOT supported by " + preparedStatement.getClass().getName());
	        } 
		} catch(IOException e) {
			throw new ParameterException("Could not get InputStream", e);
		}
	}
	
	/**
	 * @throws SQLException  
	 * @throws ParameterException  
	 */
	protected void setCharacterStream(PreparedStatement preparedStatement, int parameterIndex, String parameterName, CharacterStream stream) throws SQLException, ParameterException {
		long length = stream.getLength();
    	try {
    		preparedStatement.setCharacterStream(parameterIndex, stream.getReader(), length);
    	} catch(AbstractMethodError e) {// assume jdbc driver is previous to java 1.6
    		if(length <= Integer.MAX_VALUE)
    			preparedStatement.setCharacterStream(parameterIndex, stream.getReader(), (int)length);
    		else
    			throw new ParameterException("Length of stream (" + length + ") for parameter " + parameterName + " is greater than the max integer value and is NOT supported by " + preparedStatement.getClass().getName());
        }
	}
	
	public CallableStatement setQueryTimeout(CallableStatement callableStatement, double timeoutSeconds) throws SQLException {
		int timeout = (int) timeoutSeconds;
		if(timeout > 0)
			callableStatement.setQueryTimeout(timeout);
		return callableStatement;
	}
	
	public String getConnectionIdentifier(Connection conn) {
		return conn.toString(); //XXX: We could report stuff from DatabaseMetaData
	}

	@Override
	public String getSortedSql(Connection conn, String originalSql, int[] sortBy) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM (").append(originalSql).append(") sql ");
		if(sortBy != null && sortBy.length > 0) {
			boolean first = true;
			for(int index : sortBy) {
				if(index == 0)
					continue;
				if(first) {
					sql.append(" ORDER BY ");
					first = false;
				} else
					sql.append(", ");
				sql.append(Math.abs(index));
				if(index < 0)
					sql.append(" DESC");
			}
		}
		sql.append(" LIMIT ? OFFSET ?");
		return sql.toString();
	}

	@Override
	public void setSortedRowLimits(CallableStatement callableStatement, int rowOffset, int rowLimit) throws SQLException {
		int count = callableStatement.getParameterMetaData().getParameterCount();
		callableStatement.setInt(count - 1, rowOffset);
		callableStatement.setInt(count, rowLimit);
	}
}
