package simple.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

import simple.io.Log;
import simple.results.CacheableResults;
import simple.results.CachingException;
import simple.results.Results;
import simple.results.ResultsCreationException;
import simple.results.ResultsCreator;
import simple.util.concurrent.CacheCountTracker;
import simple.util.concurrent.Factory;
import simple.util.concurrent.FutureCacheWithFactory;

public abstract class CallCacher implements Factory<Object[],Object[],Exception>{
	private static final Log log = Log.getLog();
    protected FutureCacheWithFactory<Object[],Object[],ExecutionException> cache;

	protected abstract Call getCall() throws CallNotFoundException;
	protected abstract Connection getConnection(Call call) throws SQLException, DataLayerException;
	protected abstract CacheCountTracker getCountTracker() ;
	protected abstract String getCacheName(Call call) ;
	protected abstract boolean doCommit() ;

	public CallCacher() {
		super();
	}
	public CallCacher(FutureCacheWithFactory<Object[],Object[],ExecutionException> cache) {
		super();
		setCache(cache);
	}

	protected void closeConnection(Connection conn) {
        try { conn.close(); } catch(SQLException e) {}
	}
	protected Connection reconnect(Call call, Connection conn) throws SQLException, DataLayerException {
		try { conn.close(); } catch(SQLException e) {}
        return getConnection(call);
	}

	public Object[] getResults(Object bean) throws CallNotFoundException, ParameterException, ResultsCreationException, ExecutionException {
		Call call = getCall();
		if(bean instanceof Object[] || bean == null) return getResults((Object[]) bean, call);
        else {
            Object[] outParams = getResults(call.extractParams(bean), call);
            call.saveParams(outParams, bean);
            return outParams;
        }
	}

	protected Object[] getResults(Object[] params, Call call) throws ParameterException, ExecutionException, ResultsCreationException {
		params = call.normalizeParams(params);
		Object[] res;
		try {
			res = cache.getOrCreate(params);
		} finally {
			CacheCountTracker cct = getCountTracker();
			if(cct != null && cache.isTrackingCounts()) {
				cct.cacheAccessed(getCacheName(call), cache);
			}
		}
		try {
			return normalizeResults(res);
		} catch(ResultsCreationException e) {
			log.warn("Error while retrieving the Results object from file; re-running call...", e);
			try {
				res = cache.expireAndGet(params);
			} finally {
				CacheCountTracker cct = getCountTracker();
				if(cct != null && cache.isTrackingCounts()) {
					cct.cacheAccessed(getCacheName(call), cache);
				}
			}
			return normalizeResults(res);
		}
	}

	public Object[] getFreshResults(Object bean) throws CallNotFoundException, ParameterException, ResultsCreationException, ExecutionException {
		Call call = getCall();
		if(bean instanceof Object[] || bean == null) return getFreshResults((Object[]) bean, call);
        else {
            Object[] outParams = getFreshResults(call.extractParams(bean), call);
            call.saveParams(outParams, bean);
            return outParams;
        }
	}

	protected Object[] getFreshResults(Object[] params, Call call) throws ParameterException, ExecutionException, ResultsCreationException {
		params = call.normalizeParams(params);
		Object[] res;
		try {
			res = cache.expireAndGet(params);
		} finally {
			CacheCountTracker cct = getCountTracker();
			if(cct != null && cache.isTrackingCounts()) {
				cct.cacheAccessed(getCacheName(call), cache);
			}
		}
		return normalizeResults(res);
	}


    /** Both prepares the results to be used and performs any necessary updates to the stored array of results
     * @param res The call results
     * @return A copy of the results prepared for use
     * @throws CachingException
     * @throws ResultsCreationException
     */
    public static Object[] normalizeResults(Object[] res) throws ResultsCreationException {
    	Object[] copy = new Object[res.length];
    	for(int i = 0; i < res.length; i++) {
            if(res[i] instanceof CacheableResults) {
            	copy[i] = res[i];
                try {
					res[i] = ((CacheableResults)res[i]).getCachedResultsCreator();
				} catch(CachingException e) {
					log.warn("Error while attempting to store the Results object to file", e);
					copy[i] = ((Results)res[i]).clone();
				}
            } else if(res[i] instanceof ResultsCreator)
                copy[i] = ((ResultsCreator)res[i]).createResults();
            else if(res[i] instanceof Results)
                copy[i] = ((Results)res[i]).clone();
            else
            	copy[i] = res[i];
        }
    	return copy;
    }

	public Object[] executeCall(Object[] params, boolean retry) throws SQLException, DataLayerException {
    	Call call = getCall();
    	Connection conn = getConnection(call);
        try {
            Object[] res = call.executeCall(conn, params, null);
        	if(doCommit()) conn.commit();
        	return res;
        } catch(SQLException sqle) {
            if(doCommit()) try { conn.rollback(); } catch(SQLException e0) {}
            if(retry && doRetry(sqle)) {
                log.debug("A database call failed with the following exception and will be re-tried once", sqle);
                conn = reconnect(call, conn);
                return executeCall(params, false);
            } else {
                throw sqle;
            }
        } finally {
        	closeConnection(conn);
        }
    }

    /**
     * Determines if a new connection should be obtained and the statements retried based on the
     * given SQLException that was thrown for the previous attempt
     */
    protected boolean doRetry(SQLException sqle) {
        if(sqle.getErrorCode() == 0 || sqle.getSQLState() == null) return true;
        return false;
    }

	public Object[] create(Object[] params, Object... additionalInfo) throws SQLException, DataLayerException {
		return executeCall(params, true);
	}

	public FutureCacheWithFactory<Object[],Object[],ExecutionException> getCache() {
		return cache;
	}

	public void setCache(FutureCacheWithFactory<Object[],Object[],ExecutionException> cache) {
		this.cache = cache;
		if(cache != null)
			cache.setValueFactory(this);
	}
}
