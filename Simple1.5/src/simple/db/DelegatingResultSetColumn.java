/**
 *
 */
package simple.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * @author Brian S. Krug
 *
 */
public class DelegatingResultSetColumn implements ResultSetColumn {
	protected final ResultSet resultSet;
	protected final ResultSetMetaData metaData;
	protected final int columnIndex;
	public DelegatingResultSetColumn(ResultSet resultSet, int columnIndex) throws SQLException {
		super();
		this.resultSet = resultSet;
		this.metaData = resultSet.getMetaData();
		this.columnIndex = columnIndex;
	}
	/**
	 * @see simple.db.ResultSetColumn#getCatalogName()
	 */
	public String getCatalogName() throws SQLException {
		return metaData.getCatalogName(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getColumnClassName()
	 */
	public String getColumnClassName() throws SQLException {
		return metaData.getColumnClassName(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getColumnDisplaySize()
	 */
	public int getColumnDisplaySize() throws SQLException {
		return metaData.getColumnDisplaySize(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getColumnLabel()
	 */
	public String getColumnLabel() throws SQLException {
		return metaData.getColumnLabel(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getColumnName()
	 */
	public String getColumnName() throws SQLException {
		return metaData.getColumnName(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getColumnTypeName()
	 */
	public String getColumnTypeName() throws SQLException {
		return metaData.getColumnTypeName(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getCurrentValue()
	 */
	public Object getCurrentValue() throws SQLException {
		return resultSet.getObject(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getPrecision()
	 */
	public int getPrecision() throws SQLException {
		return metaData.getPrecision(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getScale()
	 */
	public int getScale() throws SQLException {
		return metaData.getScale(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getSchemaName()
	 */
	public String getSchemaName() throws SQLException {
		return metaData.getSchemaName(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getSqlType()
	 */
	public int getSqlType() throws SQLException {
		return metaData.getColumnType(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#getTableName()
	 */
	public String getTableName() throws SQLException {
		return metaData.getTableName(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#isAutoIncrement()
	 */
	public boolean isAutoIncrement() throws SQLException {
		return metaData.isAutoIncrement(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#isCaseSensitive()
	 */
	public boolean isCaseSensitive() throws SQLException {
		return metaData.isCaseSensitive(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#isCurrency()
	 */
	public boolean isCurrency() throws SQLException {
		return metaData.isCurrency(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#isDefinitelyWritable()
	 */
	public boolean isDefinitelyWritable() throws SQLException {
		return metaData.isDefinitelyWritable(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#isNullable()
	 */
	public int isNullable() throws SQLException {
		return metaData.isNullable(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#isReadOnly()
	 */
	public boolean isReadOnly() throws SQLException {
		return metaData.isReadOnly(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#isSearchable()
	 */
	public boolean isSearchable() throws SQLException {
		return metaData.isSearchable(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#isSigned()
	 */
	public boolean isSigned() throws SQLException {
		return metaData.isSigned(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#isWritable()
	 */
	public boolean isWritable() throws SQLException {
		return metaData.isWritable(columnIndex);
	}
	/**
	 * @see simple.db.ResultSetColumn#setCurrentValue(java.lang.Object)
	 */
	public void setCurrentValue(Object object) throws SQLException {
		resultSet.updateObject(columnIndex, object);
	}
	/**
	 * @see simple.db.ResultSetColumn#setCurrentValue(java.lang.Object, long)
	 */
	public void setCurrentValue(Object object, long scaleOrLength) throws SQLException {
		//TODO: handle when scaleOrLength is outside the range of int
		resultSet.updateObject(columnIndex, object, (int)scaleOrLength);
	}
}
