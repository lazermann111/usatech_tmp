/*
 * JNDIDataSourceFactory.java
 *
 * Created on March 12, 2003, 9:45 AM
 */

package simple.db.jndi;

import java.beans.IntrospectionException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.Reference;
import javax.sql.DataSource;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.AbstractDataSourceFactory;
import simple.db.DataSourceNotFoundException;
import simple.db.ExtendedDataSource;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.BranchSet;

/**
 * Retrieves its data sources from the JNDI location specified in its
 * constructor.
 *
 * @author Brian S. Krug
 */
public class JNDIDataSourceFactory extends AbstractDataSourceFactory {
    private static final Log log = Log.getLog();

    protected final Context baseContext;

    protected volatile simple.util.BranchSet<String> namesCache;

    protected boolean strict = true;

    protected final String[] schemes;

    /**
     * Creates a new instance of JNDIDataSourceFactory
     *
     * @param jndiLocation
     *            The initial context of this factory
     * @throws NamingException
     */
    public JNDIDataSourceFactory(String jndiLocation) throws NamingException {
        this(jndiLocation, true, null);
    }

    /**
     * Creates a new instance of JNDIDataSourceFactory that will search the
     * entire JNDI tree for a match
     *
     * @throws NamingException
     */
    public JNDIDataSourceFactory() throws NamingException {
        this(null, false, new String[] { null, "java" });
    }

    /**
     * Creates a new instance of JNDIDataSourceFactory that will search the
     * entire JNDI tree for a match
     *
     * @throws NamingException
     */
    protected JNDIDataSourceFactory(String jndiLocation, boolean strict, String[] schemes)
            throws NamingException {
        Context ctx = new InitialContext();
        if(jndiLocation != null && jndiLocation.length() > 0)
        	ctx = (Context) ctx.lookup(jndiLocation);
        else {
            // we need to test that JNDI is available
            namesCache = getDataSourceTree(ctx, schemes);
        }
        baseContext = ctx;
        this.schemes = schemes;
        this.strict = strict;
        setDiagnosing(ConvertUtils.getBooleanSafely(getEnviromentProperty("diagnosticDataSource", schemes), false));
        setTracking(ConvertUtils.getBooleanSafely(getEnviromentProperty("trackingDataSource", schemes), false));
    }

    /**
     * @see simple.db.AbstractDataSourceFactory#getDriverClassName(java.lang.String)
     */
    @Override
    protected String getDriverClassName(String dataSourceName) throws DataSourceNotFoundException {
    	DataSource ds = getDataSource(dataSourceName);
    	try {
			return ConvertUtils.getStringSafely(ReflectionUtils.getSimpleProperty(ds, "driverClassName", true));
		} catch(IntrospectionException e) {
			log.warn("Could not get driverClassName of " + ds, e);
		} catch(IllegalAccessException e) {
			log.warn("Could not get driverClassName of " + ds, e);
		} catch(InvocationTargetException e) {
			log.warn("Could not get driverClassName of " + ds, e);
		}
		return null;
    }
    public static Object getEnviromentProperty(String name, String[] schemes) throws NamingException {
    	Context ictx = new InitialContext();
    	Context ctx = ictx;
        for(int i = 0; i < schemes.length; i++) {
        	try {
	            if(schemes[i] != null && schemes[i].length() > 0) {
	                ctx = (Context) ctx.lookup(schemes[i] + ":");
	            }
	            ctx = (Context) ctx.lookup("/comp/env/"); //NOTE: this may be different for app servers other than tomcat
	            Object value = ctx.lookup(name);
	            ictx.close();
	            return value;
        	} catch(NamingException e) {
        		//do nothing
        	}
        }
        return null;
    }

    @Override
    protected boolean isCaseSensitive() {
    	return true;
    }
    /**
     * Returns a DataSource from the JNDI resource structure using the location
     * specified in the constructor as the base context and the specified name
     * as the exact location of the DataSource. If you configure your data
     * source as a Web Resource with a name of "jdbc/MyDataSource", then you
     * would create a <CODE>JNDIDataSourceFactory</CODE> with a location of
     * "java:/comp/env/jdbc" and you would retrieve your DataSource with the
     * name, "MyDataSource".
     *
     * @return The appropriate <CODE>javax.sql.DataSource</CODE>
     * @param name
     *            The unique name of the DataSource
     * @throws DataSourceNotFoundException
     */
    @Override
    protected ExtendedDataSource createDataSource(String name) throws DataSourceNotFoundException {
    	DataSource ds;
    	try {
	    	if(strict) {
	            ds = (DataSource) baseContext.lookup(name);
	        } else {
	            ds = locateDataSource(name);
	        }
	        try {
	            if(ReflectionUtils.hasSimpleProperty(ds, "accessToUnderlyingConnectionAllowed", false)) {
	                ReflectionUtils.setSimpleProperty(ds, "accessToUnderlyingConnectionAllowed", true, false);
	            }
	        } catch(IntrospectionException e) {
	        } catch(IllegalAccessException e) {
	        } catch(InvocationTargetException e) {
	        } catch(ConvertException e) {
	        }
	    } catch(ClassCastException cce) {
	        throw new DataSourceNotFoundException(name, "Resource at '" + name + "' is NOT a DataSource", cce);
	    } catch(NamingException ne) {
	        throw new DataSourceNotFoundException(name, "Existing names:\n" + java.util.Arrays.asList(getDataSourceNames()), ne);
	    }
        return asExtendedDataSource(ds);
    }

	/**
     * Retrieves an array of the unique names of each <CODE>javax.sql.DataSource</CODE>
     * that this object contains. This implementation looks through the JNDI
     * location specified in the constructor for any object that implements
     * <CODE>javax.sql.DataSource</CODE>.
     *
     * @return An array of Strings that should yield a non-null return from the
     *         <CODE>getDataSource()</CODE> method
     */
    public String[] getDataSourceNames() {
        // I believe we must do this each time since JNDI listings could change
        try {
            return findDataSourceNames(baseContext, schemes);
        } catch(NamingException ne) {
            log.warn("While retrieving data source names", ne);
            return new String[0];
        }
    }

    /**
     * Retrieves an array of the unique names of each <CODE>javax.sql.DataSource</CODE>
     * or reference to a <CODE>javax.sql.DataSource</CODE> that this object
     * contains. The jndi "tree" under the specified location is searched for
     * these objects.
     *
     * @return An array of Strings that should yield a non-null return from the
     *         <CODE>getDataSource()</CODE> method
     */
    public static String[] findDataSourceNames(Context ctx, String[] schemes) throws NamingException {
        java.util.List<String[]> list = new java.util.ArrayList<String[]>();
        for(int i = 0; i < schemes.length; i++) {
            if(schemes[i] != null && schemes[i].length() > 0) {
                String[] base = new String[] { schemes[i] + ":" };
                collectDataSourceNames((Context) ctx.lookup(base[0]), list, base);
            } else {
                collectDataSourceNames(ctx, list, null);
            }
        }
        String[] names = new String[list.size()];
        java.util.Iterator<String[]> iter = list.iterator();
        for(int i = 0; i < names.length; i++) {
        	String[] tree = iter.next();
            names[i] = tree[tree.length-1]; //StringUtils.join(tree, "/");
        }
        return names;
    }

    protected static BranchSet<String> getDataSourceTree(Context ctx, String[] schemes) throws NamingException {
        BranchSet<String> names = new simple.util.BranchSet<String>(true, String.class);
        for(int i = 0; i < schemes.length; i++) {
            if(schemes[i] != null && schemes[i].length() > 0) {
                String[] base = new String[] { schemes[i] + ":" };
                collectDataSourceNames((Context) ctx.lookup(base[0]), names, base);
            } else {
                collectDataSourceNames(ctx, names, null);
            }
        }
        return names;
    }

    /**
     * Attempts to find a data source whose name matches the least significant
     * part of the JNDI tree structure. Thus, a name of "jdbc/myds" would match
     * "java:/comp/env/jdbc/myds" or "jdbc/myds" or "foo/bar/far/boo/jdbc/myds".
     */
    protected DataSource locateDataSource(String name) throws NamingException, ClassCastException {
        // try default scheme; then try java:
        if(namesCache == null) {
                // TODO: if baseContext implements EventContext register a
                // listener to clear cache on change
            namesCache = getDataSourceTree(baseContext, schemes);
        }
        String[] key = StringUtils.split(name, '/');
        java.util.Collection<?> matches = namesCache.findMatches(key);
        switch(matches.size()) {
            case 0: // not found
                throw new NamingException("No matches for '" + name + "' in JNDI tree");
            case 1: // found!
                String realName = StringUtils.join((String[]) matches.iterator().next(), "/");
                log.debug("Found a match in the JNDI tree for '" + name + "' at '" + realName + "'");
                return (DataSource) baseContext.lookup(realName);
            default: // too many
                throw new NamingException("Too many matches for '" + name + "' in JNDI tree");
        }
    }

	@SuppressWarnings("rawtypes")
	protected static void collectDataSourceNames(Context ctx, java.util.Collection<String[]> dataSourceNames, String[] base) throws NamingException {
        if(base == null)
            base = new String[0];
        NamingEnumeration names = ctx.list("");
        while(names.hasMore()) {
            NameClassPair ncp = null;
            try {
                ncp = (NameClassPair) names.next();
                Class clazz = Class.forName(ncp.getClassName());
                if(Reference.class.isAssignableFrom(clazz)) {
                    Object o = ctx.lookup(ncp.getName());
                    clazz = o.getClass();
                    // Reference ref = (Reference)ctx.lookup(ncp.getName());
                    // clazz = Class.forName(ref.getClassName());
                }
                if(DataSource.class.isAssignableFrom(clazz)) {
                    String[] subBase = new String[base.length + 1];
                    System.arraycopy(base, 0, subBase, 0, base.length);
                    subBase[base.length] = ncp.getName();
                    dataSourceNames.add(subBase);
                } else if(Context.class.isAssignableFrom(clazz)) {
                    String[] subBase = new String[base.length + 1];
                    System.arraycopy(base, 0, subBase, 0, base.length);
                    subBase[base.length] = ncp.getName();
                    collectDataSourceNames((Context) ctx.lookup(ncp.getName()), dataSourceNames, subBase);
                }
            } catch(NamingException ne) {
                log.warn("Not including JNDI listing '" + StringUtils.join(base, "/")
                        + (ncp == null ? "UNKNOWN" : ncp.getName())
                        + "' in list of Data Source Names because of exception", ne);
            } catch(ClassNotFoundException cnfe) {
                log.warn("Not including JNDI listing '" + StringUtils.join(base, "/")
						+ ncp.getName()
                        + "' in list of Data Source Names because of exception", cnfe);
            }
        }
    }

    public static void printJNDITree(Context ctx, PrintStream ps, int level) throws NamingException,
            ClassNotFoundException {
        final char[] fill = new char[level * 4];
        java.util.Arrays.fill(fill, ' ');
        final String indent = new String(fill);
        NamingEnumeration<NameClassPair> names = ctx.list("");
        while(names.hasMore()) {
            NameClassPair ncp = names.next();
            ps.println(indent + "+ " + ncp.getName() + " (" + ncp.getClassName() + ")");
            if(Context.class.isAssignableFrom(Class.forName(ncp.getClassName()))) {
                printJNDITree((Context) ctx.lookup(ncp.getName()), ps, level + 1);
            }
        }
    }

}
