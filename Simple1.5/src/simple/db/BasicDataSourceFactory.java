/*
 * BasicDataSourceFactory.java
 *
 * Created on March 12, 2003, 9:45 AM
 */

package simple.db;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import javax.sql.DataSource;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveComparator;

/** Creates and retrieves DataSources from the configurations specified by the
 * properties passed to its constructor. Each DataSource should have the following
 * properties "DRIVER", "DATABASE", "USERNAME", and "PASSWORD". Each of these
 * property names are prefixed with the name of the DataSource and a period ("."). The
 * default DataSource (retrieved by passing NULL or "") has no prefix on its properties.
 * Example: the following properties are passed to the constructor of this factory:
 * <P><PRE>DS1.DRIVER=oracle.jdbc.driver.OracleDriver
 * DS1.DATABASE=jdbc:oracle:thin:@10.0.0.127:1521:myDataBase
 * DS1.USERNAME=myuser
 * DS1.PASSWORD=mypassword
 * DS2.DRIVER=sun.jdbc.odbc.JdbcOdbcDriver
 * DS2.DATABASE=jdbc:odbc:myODBCDataSource
 * DS2.USERNAME=myuser2
 * DS2.PASSWORD=mypassword2<PRE></P>
 * Then two DataSources would be configured on named "DS1" and the other "DS2"
 *
 * @author  Brian S. Krug
 */
public class BasicDataSourceFactory extends AbstractDataSourceFactory {
    private static final Log log = Log.getLog();
    protected Map<String,String> props;
    protected final Comparator<String> comparator = new CaseInsensitiveComparator<String>();
    protected final Map<String,DataSource> shared = new HashMap<String,DataSource>();
    protected String[] names;
    public static final String PROP_DRIVER = "DRIVER";
    public static final String PROP_DATABASE = "DATABASE";
    public static final String PROP_USERNAME = "USERNAME";
    public static final String PROP_PASSWORD = "PASSWORD";
    public static final String PROP_SHARED = "SHARED";
    public static final String PROP_AUTOCOMMIT = "AUTOCOMMIT";

    public BasicDataSourceFactory() {
    	props = new TreeMap<String,String>(comparator);
    }

    /** Creates a new instance of BasicDataSourceFactory
     * @param paramString A string of properties of the form "property1=value1;property2=value2;..."
     */
    public BasicDataSourceFactory(String paramString) {
        props = new TreeMap<String,String>(comparator);
        StringTokenizer token =  new StringTokenizer(paramString, ";");
        while(token.hasMoreTokens()) {
            String line = token.nextToken().trim();
            String[] entry = StringUtils.split(line, '=');
            if(entry.length == 2) setProperty(entry[0].trim().toUpperCase(), entry[1]);
        }
    }

    /** Creates a new instance of BasicDataSourceFactory with the specified properties
     * defining the set of DataSource's this factory contains.
     * @param properties The properties for configuring this factory
     */
    public BasicDataSourceFactory(Properties properties) {
        props = new TreeMap<String,String>(comparator);
        for(Object p : properties.keySet()) {
        	String name = (String)p;
        	setProperty(name, properties.getProperty(name));
        }
    }
    public void addDataSource(String name, String driver, String url, String username, String password) {
    	addDataSource(name, driver, url, username, password, false);
    }
    public void addDataSource(String name, String driver, String url, String username, String password, boolean autoCommit) {
		setProperty(name + "." + BasicDataSourceFactory.PROP_DATABASE, url);
		setProperty(name + "." + BasicDataSourceFactory.PROP_DRIVER, driver);
		setProperty(name + "." + BasicDataSourceFactory.PROP_USERNAME, username);
		setProperty(name + "." + BasicDataSourceFactory.PROP_PASSWORD, password);
		setProperty(name + "." + BasicDataSourceFactory.PROP_AUTOCOMMIT, String.valueOf(autoCommit));
		names = null;
		clearCache();
    }
    /**
     * @throws DataSourceNotFoundException
     * @see simple.db.AbstractDataSourceFactory#getDriverClassName(java.lang.String)
     */
    @Override
    protected String getDriverClassName(String dataSourceName) throws DataSourceNotFoundException {
    	return getDataSourceProps(dataSourceName).getDriver();
    }

	@Override
    protected ExtendedDataSource createDataSource(String name) throws DataSourceNotFoundException {
    	DataSourceProps dsp = getAndCheckDataSourceProps(name);
        log.debug("Creating new BasicDataSource with " + dsp);
        return new BasicDataSource(dsp);
    }
    protected DataSourceProps getAndCheckDataSourceProps(String name) throws DataSourceNotFoundException {
    	DataSourceProps dsp = getDataSourceProps(name);
        if(dsp.getDatabase() == null || dsp.getDatabase().trim().length() == 0) {
            log.debug("Props = " + props.toString());
            throw new DataSourceNotFoundException(name, "Database property is not specified for '" + name + "'");
        }
        try {
            dsp.checkDriver();
        } catch(ClassNotFoundException cnfe) {
            throw new DataSourceNotFoundException(name, "Could not load Driver Class '" + dsp.getDriver() + "'");
        }
        return dsp;
    }
    @Override
    protected boolean isCaseSensitive() {
    	return false;
    }
    protected void setProperty(String name, String value) {
    	if(!possiblySetBeanProperty(name,value))
    		props.put(name, value);
    }
    public void refreshProps(Properties properties) {
    	props = new TreeMap<String,String>(comparator);
        for(Object p : properties.keySet()) {
        	String name = (String)p;
        	setProperty(name, properties.getProperty(name));
        }
        names = null;
        clearCache();
        /*
        for(java.util.Iterator iter = sources.entrySet().iterator(); iter.hasNext(); ) {
            java.util.Map.Entry e = (java.util.Map.Entry)iter.next();
            String name = (String)e.getKey();
            DataSourceProps dsp = (DataSourceProps) e.getValue();
            String prefix = (name == null || name.trim().length() == 0 ? "" : name + ".");
            dsp.reload(
        		getProperty(prefix, PROP_DRIVER, "driverClassName"),
        		getProperty(prefix, PROP_DATABASE, "url"),
        		getProperty(prefix, PROP_USERNAME, "username"),
        		getProperty(prefix, PROP_PASSWORD, "password"),
        		getProperty(prefix, PROP_SHARED),
        		ConvertUtils.getBooleanSafely(getProperty(prefix, PROP_AUTOCOMMIT, "defaultAutoCommit"), false)
            );
        }*/
    }

    public DataSourceProps getDataSourceProps(String name) {
        String prefix = (name == null || name.trim().length() == 0 ? "" : name + ".");
        return new DataSourceProps(
    		getProperty(prefix, PROP_DRIVER, "driverClassName"),
    		getProperty(prefix, PROP_DATABASE, "url"),
    		getProperty(prefix, PROP_USERNAME, "username"),
    		getProperty(prefix, PROP_PASSWORD, "password"),
    		getProperty(prefix, PROP_SHARED),
    		ConvertUtils.getBooleanSafely(getProperty(prefix, PROP_AUTOCOMMIT, "defaultAutoCommit"), false)
        );
    }

    protected String getProperty(String prefix, String... names) {
    	for(String s : names) {
    		String p = props.get(prefix + s);
    		if(p != null)
    			return p;
    	}
    	return null;
    }
    /** Retrieves an array of the unique names of each <CODE>javax.sql.DataSource</CODE>
     * that this object contains. This implementation looks through the properties
     * passed to its constructor to find any that end with ".DRIVER" or equal "DRIVER".
     * @return An array of Strings that should yield a non-null return from the
     * <CODE>getDataSource()</CODE> method (although an exception might be thrown).
     */
    public String[] getDataSourceNames() {
        if(names == null) {
            final int len = PROP_DRIVER.length() + 1; // add one for the period
            java.util.List<String> l = new java.util.ArrayList<String>();
            for(String n : props.keySet()) {
                if(n.equals(PROP_DRIVER)) l.add("");
                else if(n.endsWith("." + PROP_DRIVER)) l.add(n.substring(0, n.length() - len));
            }
            names = l.toArray(new String[l.size()]);
        }
        return names;
    }

    public static class DataSourceProps {
        public DataSourceProps(String driv, String db, String user, String pwd, String shrd, boolean autoCommit) {
        	this.driver = driv;
            this.database = db;
            this.username = user;
            this.password = pwd;
            setShared(shrd);
            this.autoCommit = autoCommit;
        }
        protected String driver;
        protected Class<? extends Driver> driverClass;
        protected boolean changed = false;
        protected String database;
        protected String username;
        protected String password;
        protected ReentrantLock lock = new ReentrantLock();
        protected boolean autoCommit;
        /** Holds value of property shared. */
        private boolean shared;

        protected void reload(String driv, String db, String user, String pwd, String shrd, boolean autoCommit) {
            lock.lock();
            try {
            	this.autoCommit = autoCommit;
                if(!ConvertUtils.areEqual(driv,driver)) driverClass = null;
                else if(ConvertUtils.areEqual(db,database)
                    && ConvertUtils.areEqual(user,username)
                    && ConvertUtils.areEqual(pwd,password)) return;
                this.driver = driv;
                this.database = db;
                this.username = user;
                this.password = pwd;
                setShared(shrd);
                changed = true;
            } finally {
                lock.unlock();
            }
        }
        public String getDriver() { return driver; }
        public String getDatabase() { return database; }
        public String getUsername() { return username; }
        public String getPassword() { return password; }
        public Class<? extends Driver> checkDriver() throws ClassNotFoundException {
            if(driverClass == null && driver != null && driver.trim().length() > 0) {
            	driverClass = Class.forName(driver.trim()).asSubclass(Driver.class);
            }
            return driverClass;
        }
        protected void setShared(String shrd) {
            try {
                shared = ConvertUtils.getBoolean(shrd, false);
            } catch(ConvertException e) {}
        }
        /** Getter for property shared.
         * @return Value of property shared.
         *
         */
        public boolean isShared() {
            return this.shared;
        }

        @Override
		public String toString() {
            return PROP_DRIVER + "=" + driver + "; "
                + PROP_DATABASE + "=" + database + "; "
                + PROP_USERNAME + "=" + username + "; "
                + PROP_SHARED + "=" + shared + ";";
        }
		public boolean isAutoCommit() {
			return autoCommit;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public void setPassword(String password) {
			this.password = password;
		}
    }

    protected static class BasicDataSource implements ExtendedDataSource {
        protected DataSourceProps dsp;
        protected Connection conn = null;
        public BasicDataSource(DataSourceProps dataSourceProps) {
            dsp = dataSourceProps;
        }
        public void close() throws SQLException {
        	if(conn != null)
        		conn.close();
        }
        /**
         * @see simple.db.ExtendedDataSource#getUrl()
         */
        public String getUrl() {
        	return dsp.getDatabase();
        }
        /**
         * @see simple.db.ExtendedDataSource#getUsername()
         */
        public String getUsername() {
        	return dsp.getUsername();
        }
        /**
         * @see simple.db.ExtendedDataSource#getPassword()
         */
        public String getPassword() {
        	return dsp.getPassword();
        }

		public void setUsername(String username) {
			dsp.setUsername(username);
		}

		public void setPassword(String password) {
			dsp.setPassword(password);
		}

        public Connection getConnection() throws SQLException {
        	Connection c;
            dsp.lock.lock();
            try {
	            if(dsp.shared && conn != null && !conn.isClosed()) {
	                if(!dsp.changed) {
	                	if(conn.getAutoCommit() != dsp.isAutoCommit())
	                		conn.setAutoCommit(dsp.isAutoCommit());
	                	return conn;
	                } else try { conn.close(); } catch(SQLException sqle) {}
	            }
	            log.debug("Connection is null or old - getting new one (conn=" + conn + "; changed=" + dsp.changed + "; closed=" + (conn != null ? conn.isClosed() : false) + ")");
                if(dsp.getUsername() != null && dsp.getUsername().trim().length() > 0)
                    c = java.sql.DriverManager.getConnection(dsp.getDatabase(),dsp.getUsername(),dsp.getPassword());
                else
                    c = java.sql.DriverManager.getConnection(dsp.getDatabase());
                c.setAutoCommit(dsp.isAutoCommit());
                dsp.changed = false;
                if(dsp.shared)
                	conn = c;
            } finally {
                dsp.lock.unlock();
            }
            return c;
        }
        public Connection getConnection(String user, String pwd) throws SQLException {
        	Connection conn = java.sql.DriverManager.getConnection(dsp.getDatabase(),user,pwd);
        	conn.setAutoCommit(dsp.isAutoCommit());
            return conn;
        }
        public Connection getConnection(Properties info) throws SQLException {
        	String user = dsp.getUsername();
        	if(!info.containsKey("user") && user != null && user.trim().length() > 0) {
        	    info.put("user", user);
        	}
        	String password = dsp.getPassword();
        	if(!info.containsKey("password") && password != null && password.trim().length() > 0) {
        	    info.put("password", password);
        	}
        	Connection conn = java.sql.DriverManager.getConnection(dsp.getDatabase(), info);
        	conn.setAutoCommit(dsp.isAutoCommit());
            return conn;
        }

        public PrintWriter getLogWriter() throws SQLException { return null; }
        public void setLogWriter(PrintWriter out) throws SQLException {}
        public void setLoginTimeout(int seconds) throws SQLException {}
        public int getLoginTimeout() throws SQLException { return 0; }
        @Override
		public void finalize() {
            //don't do this or gc may close our connection while we are using it!
            //try { if(conn != null) conn.close(); } catch(SQLException e) {}
        }
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return false;
		}
		public <T> T unwrap(Class<T> iface) throws SQLException {
			throw new SQLException("Not supported");
		}

		@Override
		public boolean isAvailable() {
			return true;
		}

		public Logger getParentLogger() throws SQLFeatureNotSupportedException {
			return Logger.getLogger(getClass().getPackage().getName());
		}
    }
}
