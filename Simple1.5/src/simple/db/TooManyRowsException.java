package simple.db;

import java.sql.SQLException;

public class TooManyRowsException extends SQLException {
	private static final long serialVersionUID = -2099410123414L;
	public static final String TOO_MANY_ROWS_SQL_STATE = "21000";

	public TooManyRowsException() {
		this("Too many rows");
	}

	public TooManyRowsException(String reason) {
		super(reason, TOO_MANY_ROWS_SQL_STATE);
	}
}
