package simple.db;

import java.sql.Connection;
import java.sql.SQLException;

import simple.io.Log;

public class ExistingConnectionHolder implements ConnectionHolder {
	private static final Log log = Log.getLog();
	protected final Connection conn;

	public ExistingConnectionHolder(Connection conn) {
		this.conn = conn;
	}

	@Override
	public Class<? extends Connection> getConnectionClass() throws DataSourceNotFoundException {
		return conn.getClass();
	}

	@Override
	public Connection getConnection() throws SQLException, DataSourceNotFoundException {
		return conn;
	}

	@Override
	public boolean closeConnection() {
		return false;
	}

	@Override
	public Connection reconnect() throws SQLException, DataSourceNotFoundException {
		return conn;
	}

	@Override
	public void commitOrRollback(boolean commit) throws SQLException {
		if(conn != null && !conn.getAutoCommit()) {
			if(commit)
				conn.commit();
			else
				try {
					conn.rollback();
				} catch(SQLException e) {
					log.info("Could not rollback transaction: " + e.getMessage());
				}

		}
	}
}
