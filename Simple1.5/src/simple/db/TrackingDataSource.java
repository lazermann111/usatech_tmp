/*
 * Created on Nov 10, 2005
 *
 */
package simple.db;

import java.beans.IntrospectionException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import simple.app.jmx.TrackingDataSourceMXBean;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.util.MapBackedSet;

/** This data source keeps track of the last operation of each connection and provides
 * a current status report upon request.
 * @author Brian S. Krug
 *
 */
public class TrackingDataSource extends AbstractDataSource implements ExtendedDataSource, TrackingDataSourceMXBean {
	private static final Log log = Log.getLog();
	protected Set<TrackingConnection> connections = new MapBackedSet<TrackingConnection>(new ConcurrentHashMap<TrackingConnection, Object>());
	protected AtomicInteger idGenerator = new AtomicInteger(0);
	protected ExtendedDataSource delegate;
	protected static final String[] headers = new String[]{"N", "Connect Time", "Last Operation"};
	protected final AtomicInteger activeCount = new AtomicInteger();

	public TrackingDataSource(ExtendedDataSource delegate) {
		this.delegate = delegate;
	}

    public String getPassword() {
		return delegate.getPassword();
	}

	public String getUrl() {
		return delegate.getUrl();
	}

	public String getUsername() {
		return delegate.getUsername();
	}

	public void setUsername(String username) {
		delegate.setUsername(username);
	}

	public void setPassword(String password) {
		delegate.setPassword(password);
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return delegate.isWrapperFor(iface);
	}

	public int getNumActive() {
		return activeCount.get();
	}
	
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return delegate.unwrap(iface);
	}
	
	protected void closeConnections() {
		// For now we just close active connections and don't worry about inactive connections that may still be open
		for(TrackingConnection conn : connections) {
			try {
				conn.close();
			} catch(SQLException e) {
				log.warn("Could not close connection " + conn, e);
			}
		}
	}

	public boolean isAvailable() {
		return delegate.isAvailable();
	}

	public void close() throws SQLException {
		delegate.close();
	}
	public void appendConnectionInnerDetails(Writer writer) {
		//Attempt to get size of connection pool and sessionIds of real connections
		PrintWriter pw = new PrintWriter(writer);
		try {
			Object pool = ReflectionUtils.getPrivateProperty(delegate, "connectionPool._pool");
			if(pool instanceof Collection<?>) {
				Collection<?> coll = (Collection<?>) pool;
				pw.print("The Connection Pool has ");
				pw.print(coll.size());
				pw.println(" entries:");
				for(Object o : coll) {
					Object sessionId;
					try {
						sessionId = ReflectionUtils.getPrivateProperty(o, "value.delegate.sessionId");
					} catch(IllegalAccessException e) {
						sessionId = null;
					} catch(IntrospectionException e) {
						sessionId = null;
					} catch(InvocationTargetException e) {
						sessionId = null;
					} catch(ParseException e) {
						sessionId = null;
					}
					Object serialNumber;
					try {
						serialNumber = ReflectionUtils.getPrivateProperty(o, "value.delegate.serialNumber");
					} catch(IllegalAccessException e) {
						serialNumber = null;
					} catch(IntrospectionException e) {
						serialNumber = null;
					} catch(InvocationTargetException e) {
						serialNumber = null;
					} catch(ParseException e) {
						serialNumber = null;
					}
					pw.print("\t");
					pw.print(o);
					pw.print(" (SessionId=");
					pw.print(sessionId);
					pw.print("; SerialNumber=");
					pw.print(serialNumber);
					pw.println(")");
				}
			} else {
				pw.print("Pool is not an instance of Collection; it is ");
				pw.println(pool == null ? "<NULL" : pool.getClass().getName());
			}
		} catch(IntrospectionException e) {
			pw.println("Could not get Connection Details");
			e.printStackTrace(pw);
		} catch(IllegalAccessException e) {
			pw.println("Could not get Connection Details");
			e.printStackTrace(pw);
		} catch(InvocationTargetException e) {
			pw.println("Could not get Connection Details");
			e.printStackTrace(pw);
		} catch(ParseException e) {
			pw.println("Could not get Connection Details");
			e.printStackTrace(pw);
		}
	}
	public void appendConnectionInfoFixed(Writer writer, int maxLineLength, boolean header, boolean underline, String spacing, boolean ordered) {
		PrintWriter out = (writer instanceof PrintWriter ? (PrintWriter)writer : new PrintWriter(writer));
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
		int spacingLength = (spacing == null ? 0 : spacing.length());
		int idLength = Math.max(headers[0].length(), 1 + (int)Math.log10(idGenerator.doubleValue()));
		int connectLength = Math.max(headers[1].length(), 23);
		int lastOpLength = Math.max(headers[2].length(), maxLineLength - (idLength + 23 + (2 * spacingLength)));
		int[] widths = new int[] {idLength,spacingLength,connectLength,spacingLength,lastOpLength};
		Justification[] justifications = new Justification[] {Justification.RIGHT, Justification.LEFT, Justification.LEFT, Justification.LEFT, Justification.LEFT};
		Object[] data = new Object[] {null, spacing, null, spacing, null};
		if(header) {
			data[0] = headers[0];
			data[2] = headers[1];
			data[4] = headers[2];
			StringUtils.writeFixedWidthLine(data, widths, justifications, out);
		}
		if(underline) {
			data[0] = StringUtils.fillString(widths[0], "-");
			data[2] = StringUtils.fillString(widths[2], "-");
			data[4] = StringUtils.fillString(widths[4], "-");
			StringUtils.writeFixedWidthLine(data, widths, justifications, out);
		}
		Iterator<TrackingConnection> iter;
		if(ordered) {
			Set<TrackingConnection> sorted = new TreeSet<TrackingConnection>(new Comparator<TrackingConnection>() {
				public int compare(TrackingConnection o1, TrackingConnection o2) {
					return o1.id - o2.id;
				}
			});
			sorted.addAll(connections);
			iter = sorted.iterator();
		} else {
			iter = connections.iterator();
		}
		while(iter.hasNext()) {
			TrackingConnection tc = iter.next();
			data[0] = tc.id;
			data[2] = df.format(new Date(tc.connectTime));
			data[4] = tc.lastOperation;
			StringUtils.writeFixedWidthLine(data, widths, justifications, out);
		}
		out.flush();
	}
	public void appendConnectionInfoCVS(Writer writer, boolean header, boolean ordered) {
		PrintWriter out = (writer instanceof PrintWriter ? (PrintWriter)writer : new PrintWriter(writer));
		Object[] data = new Object[headers.length];
		if(header) {
			System.arraycopy(headers, 0, data, 0, headers.length);
			StringUtils.writeCSVLine(data, out);
		}
		Iterator<TrackingConnection> iter;
		if(ordered) {
			Set<TrackingConnection> sorted = new TreeSet<TrackingConnection>(new Comparator<TrackingConnection>() {
				public int compare(TrackingConnection o1, TrackingConnection o2) {
					return o1.id - o2.id;
				}
			});
			sorted.addAll(connections);
			iter = sorted.iterator();
		} else {
			iter = connections.iterator();
		}
		while(iter.hasNext()) {
			TrackingConnection tc = iter.next();
			data[0] = tc.id;
			data[1] = new Date(tc.connectTime);
			data[2] = tc.lastOperation;
			StringUtils.writeCSVLine(data, out);
		}
		out.flush();
	}

	/**
	 * @see javax.sql.DataSource#getConnection()
	 */
	protected Connection getConnectionInternal() throws SQLException {
		return new TrackingConnection(delegate.getConnection());
	}

	/**
	 * @see javax.sql.DataSource#getConnection(java.lang.String, java.lang.String)
	 */
	protected Connection getConnectionInternal(String username, String password) throws SQLException {
		return new TrackingConnection(delegate.getConnection(username, password));
	}

	/**
	 * @see javax.sql.DataSource#getLoginTimeout()
	 */
	public int getLoginTimeout() throws SQLException {
		return delegate.getLoginTimeout();
	}

	/**
	 * @see javax.sql.DataSource#getLogWriter()
	 */
	public PrintWriter getLogWriter() throws SQLException {
		return delegate.getLogWriter();
	}

	/**
	 * @see javax.sql.DataSource#setLoginTimeout(int)
	 */
	public void setLoginTimeout(int seconds) throws SQLException {
		delegate.setLoginTimeout(seconds);
	}

	/**
	 * @see javax.sql.DataSource#setLogWriter(java.io.PrintWriter)
	 */
	public void setLogWriter(PrintWriter out) throws SQLException {
		delegate.setLogWriter(out);
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TrackingDataSource on " + delegate;
	}

	public class TrackingConnection extends DelegatingConnection {
		protected final int id;
		protected final long connectTime;
		protected String lastOperation;

		public TrackingConnection(Connection delegate) {
			super(delegate);
			this.connectTime = System.currentTimeMillis();
			this.id = idGenerator.incrementAndGet();
			logOperation("Create");
			activeCount.incrementAndGet();
			connections.add(this);
		}
		@Override
		public String toString() {
			return "TrackingConnection [id=" + id + "; connected=" + new Date(connectTime) + "; lastOp=" +lastOperation + "] on " + delegate.toString();
		}
		protected void logOperation(String operation) {
			lastOperation = operation;
		}
		@Override
		public void close() throws SQLException {
			logOperation("Close");
			if(connections.remove(this)) { // this gives us concurrency protection
				if(isDisabled()) {
					Connection realConn = DBUnwrap.getRealConnection(getDelegate());
					if(realConn != null)
						try {
							realConn.close();
						} catch(SQLException e) {
							log.warn("Could not close underlying connection " + realConn, e);
						}
				}
				try {
					delegate.close();
				} finally {
					activeCount.decrementAndGet();
				}
			}
		}
		@Override
		public void commit() throws SQLException {
			logOperation("Commit");
			super.commit();
		}
		@Override
		public Statement createStatement() throws SQLException {
			logOperation("Create Statement");
			return new TrackingStatement(delegate.createStatement(), this);
		}
		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			logOperation("Create Statement");
			return new TrackingStatement(delegate.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability), this);
		}
		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
			logOperation("Create Statement");
			return new TrackingStatement(delegate.createStatement(resultSetType, resultSetConcurrency), this);
		}
		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			logOperation("Prepare Call '" + sql + "'");
			return new TrackingCallableStatement(delegate.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability), this, sql);
		}
		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			logOperation("Prepare Call '" + sql + "'");
			return new TrackingCallableStatement(delegate.prepareCall(sql, resultSetType, resultSetConcurrency), this, sql);
		}
		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			logOperation("Prepare Call '" + sql + "'");
			return new TrackingCallableStatement(delegate.prepareCall(sql), this, sql);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			logOperation("Prepare Statement '" + sql + "'");
			return new TrackingPreparedStatement(delegate.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability), this, sql);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			logOperation("Prepare Statement '" + sql + "'");
			return new TrackingPreparedStatement(delegate.prepareStatement(sql, resultSetType, resultSetConcurrency), this, sql);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
			logOperation("Prepare Statement '" + sql + "'");
			return new TrackingPreparedStatement(delegate.prepareStatement(sql, autoGeneratedKeys), this, sql);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
			logOperation("Prepare Statement '" + sql + "'");
			return new TrackingPreparedStatement(delegate.prepareStatement(sql, columnIndexes), this, sql);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
			logOperation("Prepare Statement '" + sql + "'");
			return new TrackingPreparedStatement(delegate.prepareStatement(sql, columnNames), this, sql);
		}
		@Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			logOperation("Prepare Statement '" + sql + "'");
			return new TrackingPreparedStatement(delegate.prepareStatement(sql), this, sql);
		}
		@Override
		public void rollback() throws SQLException {
			logOperation("Rollback");
			super.rollback();
		}
		@Override
		public void rollback(Savepoint savepoint) throws SQLException {
			logOperation("Rollback");
			super.rollback(savepoint);
		}

		public class TrackingStatement extends DelegatingStatement {
			public TrackingStatement(Statement delegate, Connection connection) {
				super(delegate, connection);
			}
			@Override
			public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql, autoGeneratedKeys);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public boolean execute(String sql, int[] columnIndexes) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql, columnIndexes);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public boolean execute(String sql, String[] columnNames) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql, columnNames);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public boolean execute(String sql) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int[] executeBatch() throws SQLException {
				logOperation("Execute Batch");
				try {
					return super.executeBatch();
				} finally {
					logOperation("Completed Batch");
				}
			}

			@Override
			public ResultSet executeQuery(String sql) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeQuery(sql);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql, autoGeneratedKeys);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql, columnIndexes);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql, String[] columnNames) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql, columnNames);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}
		}

		public class TrackingPreparedStatement extends DelegatingPreparedStatement {
			protected final String preparedSql;
			public TrackingPreparedStatement(PreparedStatement delegate, Connection connection, String preparedSql) {
				super(delegate, connection);
				this.preparedSql = preparedSql;
			}
			// Statement tracking
			@Override
			public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql, autoGeneratedKeys);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public boolean execute(String sql, int[] columnIndexes) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql, columnIndexes);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public boolean execute(String sql, String[] columnNames) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql, columnNames);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public boolean execute(String sql) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int[] executeBatch() throws SQLException {
				logOperation("Execute Batch");
				try {
					return super.executeBatch();
				} finally {
					logOperation("Completed Batch");
				}
			}

			@Override
			public ResultSet executeQuery(String sql) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeQuery(sql);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql, autoGeneratedKeys);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql, columnIndexes);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql, String[] columnNames) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql, columnNames);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}
			// Prepared Statement tracking
			public void addBatch() throws SQLException {
				logOperation("Add Batch");
				super.addBatch();
			}
			public void clearParameters() throws SQLException {
				logOperation("Clear Parameters");
				super.clearParameters();
			}
			public boolean execute() throws SQLException {
				logOperation("Execute '" + preparedSql + "'");
				try {
					return super.execute();
				} finally {
					logOperation("Completed '" + preparedSql + "'");
				}
			}
			public ResultSet executeQuery() throws SQLException {
				logOperation("Execute '" + preparedSql + "'");
				try {
					return super.executeQuery();
				} finally {
					logOperation("Completed '" + preparedSql + "'");
				}
			}
			public int executeUpdate() throws SQLException {
				logOperation("Execute '" + preparedSql + "'");
				try {
					return super.executeUpdate();
				} finally {
					logOperation("Completed '" + preparedSql + "'");
				}
			}
		}
		public class TrackingCallableStatement extends DelegatingCallableStatement {
			protected final String preparedSql;
			public TrackingCallableStatement(CallableStatement delegate, Connection connection, String preparedSql) {
				super(delegate, connection);
				this.preparedSql = preparedSql;
			}
			// Statement tracking
			@Override
			public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql, autoGeneratedKeys);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public boolean execute(String sql, int[] columnIndexes) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql, columnIndexes);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public boolean execute(String sql, String[] columnNames) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql, columnNames);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public boolean execute(String sql) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.execute(sql);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int[] executeBatch() throws SQLException {
				logOperation("Execute Batch");
				try {
					return super.executeBatch();
				} finally {
					logOperation("Completed Batch");
				}
			}

			@Override
			public ResultSet executeQuery(String sql) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeQuery(sql);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql, autoGeneratedKeys);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql, columnIndexes);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql, String[] columnNames) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql, columnNames);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}

			@Override
			public int executeUpdate(String sql) throws SQLException {
				logOperation("Execute '" + sql + "'");
				try {
					return super.executeUpdate(sql);
				} finally {
					logOperation("Completed '" + sql + "'");
				}
			}
			// Prepared Statement tracking
			public void addBatch() throws SQLException {
				logOperation("Add Batch");
				super.addBatch();
			}
			public void clearParameters() throws SQLException {
				logOperation("Clear Parameters");
				super.clearParameters();
			}
			public boolean execute() throws SQLException {
				logOperation("Execute '" + preparedSql + "'");
				try {
					return super.execute();
				} finally {
					logOperation("Completed '" + preparedSql + "'");
				}
			}
			public ResultSet executeQuery() throws SQLException {
				logOperation("Execute '" + preparedSql + "'");
				try {
					return super.executeQuery();
				} finally {
					logOperation("Completed '" + preparedSql + "'");
				}
			}
			public int executeUpdate() throws SQLException {
				logOperation("Execute '" + preparedSql + "'");
				try {
					return super.executeUpdate();
				} finally {
					logOperation("Completed '" + preparedSql + "'");
				}
			}
		}
	}

	/**
	 * @see simple.app.jmx.DataSourceMXBean#getCurrentConnectionInfo()
	 */
	public String getCurrentConnectionInfo() {
		StringWriter writer = new StringWriter();
		appendConnectionInfoFixed(writer, 500, true, true, " ", true);
		return writer.toString();
	}
}
