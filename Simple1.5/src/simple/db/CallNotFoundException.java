/*
 * CallNotFoundException.java
 *
 * Created on December 30, 2002, 11:55 AM
 */

package simple.db;

/**
 *  Thrown when the DataLayer cannot find the specified Call at the given call id.
 *
 * @author  Brian S. Krug
 * 
 */
public class CallNotFoundException extends DataLayerException {
	private static final long serialVersionUID = 123413488L;

	/**
     * Constructs an <code>CallNotFoundException</code> with the specified detail message.
     * @param id the call id.
     */
    public CallNotFoundException(String id) {
        super("Could not find database call, '" + id + "'.");
    }
}


