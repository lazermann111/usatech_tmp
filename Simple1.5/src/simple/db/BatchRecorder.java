/*
 * Created on May 9, 2005
 *
 */
package simple.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.io.Log;
import simple.lang.Initializer;

/** Executes batches on the database on a regular interval or at certain threshholds.
 *
 * @author Brian S. Krug
 *
 */
public abstract class BatchRecorder<R> {
	private static final Log log = Log.getLog();
    protected String callId;
    protected int batchSize = 20;
    protected Call call;
    protected Call.BatchUpdate batch;
    protected final ReentrantLock lock = new ReentrantLock();
    protected final Condition signal = lock.newCondition();
    protected boolean currentAsync = false;
    protected Thread batchThread;
    protected int pollFrequency = 300000;
    protected volatile boolean done = false;
    public static class BatchRecorderException extends Exception {
		private static final long serialVersionUID = -3006375791534302868L;
		public BatchRecorderException() {
			super();
		}
		public BatchRecorderException(String message, Throwable cause) {
			super(message, cause);
		}
		public BatchRecorderException(String message) {
			super(message);
		}
		public BatchRecorderException(Throwable cause) {
			super(cause);
		}
    }
    protected final Initializer<BatchRecorderException> initializer = new Initializer<BatchRecorderException>() {
    	@Override
		protected void doInitialize() throws BatchRecorderException {
    		currentAsync = isAsynchronous();
    		if(callId == null) throw new BatchRecorderException("CallId was not set");
    		flush();
    		try {
				call = DataLayerMgr.getGlobalDataLayer().findCall(callId);
				batch = call.createBatchUpdate(currentAsync, 1.0f);
        	} catch(CallNotFoundException e) {
				throw new BatchRecorderException("Could not find call '" + callId + "'", e);
			} catch(ParameterException e) {
				throw new BatchRecorderException("Could not create batch update", e);
			}
			if(batchThread != null)
				log.warn("Batch Thread is NOT null and should be", new Throwable());
        	if(currentAsync && batchThread == null) {
        		done = false;
    			batchThread = new Thread(getThreadName()) {
    				@Override
    				public void run() {
    					while(!done) {
    						lock.lock();
    						try {
    							signal.await(pollFrequency, TimeUnit.MILLISECONDS);
    							if(batch != null) {
        							flushBatch(batchSize);
        						}
    						} catch(InterruptedException e) {
    							log.info("Thread interrupted; ignoring and continuing", e);
    						} finally {
    							lock.unlock();
    						}
    					}
    				}
    			};
    			batchThread.setDaemon(true);
    			batchThread.setPriority(3);
    			batchThread.start();
    		} /*else if(!currentAsync && batchThread != null) { //this should not happen
    			done = true;
    			lock.lock();
    			try {
    				signal.signalAll();
    			} finally {
    				lock.unlock();
    			}
    			try {
					batchThread.join(pollFrequency * 2);
				} catch(InterruptedException e) {
					log.info("BatchThread interrupted while joining", e);
				}
    			batchThread = null;
    		}*/
        	//Add shutdown hook to flush()
			Runtime.getRuntime().addShutdownHook(new Thread("BatchRecorderCleanupThread") {
				@Override
				public void run() {
					flush();
				}
			});
    	}
    	@Override
    	protected void doReset() {
    		flush();
    		call = null;
    		batch = null;
    		done = true;
    		if(batchThread  != null) {
				lock.lock();
				try {
					signal.signalAll();
				} finally {
					lock.unlock();
				}
				try {
					batchThread.join(pollFrequency * 2);
				} catch(InterruptedException e) {
					log.info("BatchThread interrupted while joining", e);
				}
				batchThread = null;
    		}
    	}
    };

    public BatchRecorder() {
        super();
    }

    protected String getThreadName() {
    	return getClass().getSimpleName() + "Thread";
    }

    protected boolean initialize() {
    	try {
			initializer.initialize();
		} catch(InterruptedException e) {
			log.error("Interrupted while initializing", e);
			initializer.reset();
			return false;
		} catch(BatchRecorderException e) {
			log.error("Could not initialize", e);
			initializer.reset();
			return false;
		}
    	return true;
    }

    protected abstract void addBatches() ;

    @Override
    protected void finalize() throws Throwable {
    	finish();
    	super.finalize();
    }

    public void finish() {
    	initializer.reset();
    }

    public void flush() {
    	lock.lock();
    	try {
    		flushBatch(1);
    	} finally {
    		lock.unlock();
    	}
    }

    /** Flushes the batch if pending count is greater than or equal to the specified minPendingCount. This method
     *  is NOT thread-safe
     * @param minPendingCount
     */
    protected void flushBatch(int minPendingCount) {
    	if(batch != null) {
    		addBatches();
    		while(batch.getPendingCount() >= minPendingCount) {
	    	    try {
					commitBatch();
				} catch (SQLException e) {
					log.error("Could not commit batch", e);
				} catch (DataLayerException e) {
					log.error("Could not commit batch", e);
				}
    		}
    	}
    }

    protected void signalBatch() {
	    lock.lock();
		try {
			signal.signalAll();
		} finally {
			lock.unlock();
		}
    }

    /** Actually runs the batch against the database. This method
     *  is NOT thread-safe
     */
    protected void commitBatch() throws SQLException, DataLayerException {
		Connection conn = DataLayerMgr.getConnection(call.getDataSourceName());
		try {
			batch.executeBatch(conn);
			conn.commit();
		} finally {
			conn.close();
		}
    }

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
		initializer.reset();
	}

	public int getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(int batchSize) {
		if(batchSize <= 0) throw new IllegalArgumentException("Batch Size must be greater than zero");
		this.batchSize = batchSize;
	}

	public abstract boolean isAsynchronous() ;

	public int getPollFrequency() {
		return pollFrequency;
	}

	public void setPollFrequency(int pollFrequency) {
		if(pollFrequency <= 0) throw new IllegalArgumentException("Poll Frequency must be greater than zero");
		this.pollFrequency = pollFrequency;
	}
}
