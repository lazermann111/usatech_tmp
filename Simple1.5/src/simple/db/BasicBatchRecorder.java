/*
 * Created on May 9, 2005
 *
 */
package simple.db;

import java.sql.SQLException;

import simple.io.Log;

/** Executes batches on the database on a regular interval or at certain threshholds.
 *
 * @author Brian S. Krug
 *
 */
public class BasicBatchRecorder<R> extends BatchRecorder<R> {
	private static final Log log = Log.getLog();
    protected boolean asynchronous = false;

    public BasicBatchRecorder() {
        super();
    }

    public void record(R info) {
    	if(!initialize()) return;
    	if(currentAsync) {
    		try {
				batch.addBatch(info);
				if(batch.getPendingCount() >= batchSize) {
    				signalBatch();
    			}
			} catch(ParameterException e) {
				log.error("Could not record info", e);
			}
    	} else
	    	try {
				lock.lock();
	    		try {
	    			batch.addBatch(info);
	    			if(batch.getPendingCount() >= batchSize) {
	    				commitBatch();
	    			}
	    		} finally {
	    			lock.unlock();
	    		}
			} catch (SQLException e) {
				log.error("Could not commit batches", e);
			} catch (DataLayerException e) {
				log.error("Could not commit batches", e);
			}
    }

    @Override
	protected void addBatches() {
    }

	@Override
	public boolean isAsynchronous() {
		return asynchronous;
	}

	public void setAsynchronous(boolean asynchronous) {
		this.asynchronous = asynchronous;
		initializer.reset();
	}
}
