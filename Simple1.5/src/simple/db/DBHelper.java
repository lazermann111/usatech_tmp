/*
 * DBHelper.java
 *
 * Created on May 13, 2003, 9:05 AM
 */

package simple.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import simple.bean.ConvertException;
import simple.sql.SQLType;

/**
 * Performs creation and translation services for complex SQL data types.
 *
 * @author  Brian S. Krug
 */
public interface DBHelper {
    public void setInParameter(PreparedStatement callableStatement, int parameterIndex, String parameterName, Object value, SQLType sqlType) throws SQLException, ParameterException ;
    public void registerOutParameter(CallableStatement callableStatement, int parameterIndex, String parameterName, SQLType sqlType) throws SQLException, ParameterException ;
    public Object getValue(CallableStatement callableStatement, int parameterIndex, SQLType sqlType) throws SQLException ;
    public CallableStatement setQueryTimeout(CallableStatement callableStatement, double timeoutSeconds) throws SQLException ;
    
    public Object[] getValues(ResultSet rs, Column[] columns, boolean connected) throws SQLException, ConvertException;
    public Object getValue(ResultSet rs, Column column, boolean connected) throws SQLException, ConvertException;
    public void setValue(ResultSet rs, Column column, Object value) throws SQLException, ConvertException;
    public void closeConnection(Connection conn) throws SQLException;
	public SQLType translateSqlType(SQLType sqlType, String databaseTypeName, ConnectionHolder connHolder) throws SQLException, DataSourceNotFoundException;
    public boolean reconnectNeeded(Connection conn, SQLException e) ;
    public String getConnectionIdentifier(Connection conn) ;

	public String getSortedSql(Connection conn, String originalSql, int[] sortBy);

	public void setSortedRowLimits(CallableStatement callableStatement, int rowOffset, int rowLimit) throws SQLException;
}
