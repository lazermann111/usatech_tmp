package simple.db;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

public class DelegatingConnection implements Connection {
	protected final Connection delegate;

	public DelegatingConnection(Connection delegate) {
		this.delegate = delegate;
	}

	public Array createArrayOf(String typeName, Object[] elements)
			throws SQLException {
		return delegate.createArrayOf(typeName, elements);
	}

	public Blob createBlob() throws SQLException {
		return delegate.createBlob();
	}

	public Clob createClob() throws SQLException {
		return delegate.createClob();
	}

	public NClob createNClob() throws SQLException {
		return delegate.createNClob();
	}

	public SQLXML createSQLXML() throws SQLException {
		return delegate.createSQLXML();
	}

	public Struct createStruct(String typeName, Object[] attributes)
			throws SQLException {
		return delegate.createStruct(typeName, attributes);
	}

	public Properties getClientInfo() throws SQLException {
		return delegate.getClientInfo();
	}

	public String getClientInfo(String name) throws SQLException {
		return delegate.getClientInfo(name);
	}

	public boolean isValid(int timeout) throws SQLException {
		return delegate.isValid(timeout);
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return delegate.isWrapperFor(iface);
	}

	public void setClientInfo(Properties properties)
			throws SQLClientInfoException {
		delegate.setClientInfo(properties);
	}

	public void setClientInfo(String name, String value)
			throws SQLClientInfoException {
		delegate.setClientInfo(name, value);
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return delegate.unwrap(iface);
	}

	public void clearWarnings() throws SQLException {
		delegate.clearWarnings();
	}

	public void close() throws SQLException {
		delegate.close();
	}

	public void commit() throws SQLException {
		delegate.commit();
	}

	public Statement createStatement() throws SQLException {
		return new DelegatingStatement(delegate.createStatement(), this);
	}

	public Statement createStatement(int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		return new DelegatingStatement(delegate.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability), this);
	}

	public Statement createStatement(int resultSetType, int resultSetConcurrency)
			throws SQLException {
		return new DelegatingStatement(delegate.createStatement(resultSetType, resultSetConcurrency), this);
	}

	public boolean getAutoCommit() throws SQLException {
		return delegate.getAutoCommit();
	}

	public String getCatalog() throws SQLException {
		return delegate.getCatalog();
	}

	public int getHoldability() throws SQLException {
		return delegate.getHoldability();
	}

	public DatabaseMetaData getMetaData() throws SQLException {
		return new DelegatingDatabaseMetaData(delegate.getMetaData(), this);
	}

	public int getTransactionIsolation() throws SQLException {
		return delegate.getTransactionIsolation();
	}

	public Map<String, Class<?>> getTypeMap() throws SQLException {
		return delegate.getTypeMap();
	}

	public SQLWarning getWarnings() throws SQLException {
		return delegate.getWarnings();
	}

	public boolean isClosed() throws SQLException {
		return delegate.isClosed();
	}

	public boolean isReadOnly() throws SQLException {
		return delegate.isReadOnly();
	}

	public String nativeSQL(String sql) throws SQLException {
		return delegate.nativeSQL(sql);
	}

	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		return new DelegatingCallableStatement(delegate.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability), this);
	}

	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
			throws SQLException {
		return new DelegatingCallableStatement(delegate.prepareCall(sql, resultSetType, resultSetConcurrency), this);
	}

	public CallableStatement prepareCall(String sql) throws SQLException {
		return new DelegatingCallableStatement(delegate.prepareCall(sql), this);
	}

	public PreparedStatement prepareStatement(String sql, int resultSetType,
			int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		return new DelegatingPreparedStatement(delegate.prepareStatement(sql, resultSetType, resultSetConcurrency,
				resultSetHoldability), this);
	}

	public PreparedStatement prepareStatement(String sql, int resultSetType,
			int resultSetConcurrency) throws SQLException {
		return new DelegatingPreparedStatement(delegate.prepareStatement(sql, resultSetType, resultSetConcurrency), this);
	}

	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys)
			throws SQLException {
		return new DelegatingPreparedStatement(delegate.prepareStatement(sql, autoGeneratedKeys), this);
	}

	public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
		return new DelegatingPreparedStatement(delegate.prepareStatement(sql, columnIndexes), this);
	}

	public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
		return new DelegatingPreparedStatement(delegate.prepareStatement(sql, columnNames), this);
	}

	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return new DelegatingPreparedStatement(delegate.prepareStatement(sql), this);
	}

	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		delegate.releaseSavepoint(savepoint);
	}

	public void rollback() throws SQLException {
		delegate.rollback();
	}

	public void rollback(Savepoint savepoint) throws SQLException {
		delegate.rollback(savepoint);
	}

	public void setAutoCommit(boolean autoCommit) throws SQLException {
		delegate.setAutoCommit(autoCommit);
	}

	public void setCatalog(String catalog) throws SQLException {
		delegate.setCatalog(catalog);
	}

	public void setHoldability(int holdability) throws SQLException {
		delegate.setHoldability(holdability);
	}

	public void setReadOnly(boolean readOnly) throws SQLException {
		delegate.setReadOnly(readOnly);
	}

	public Savepoint setSavepoint() throws SQLException {
		return delegate.setSavepoint();
	}

	public Savepoint setSavepoint(String name) throws SQLException {
		return delegate.setSavepoint(name);
	}

	public void setTransactionIsolation(int level) throws SQLException {
		delegate.setTransactionIsolation(level);
	}

	public void setTypeMap(Map<String, Class<?>> typeMap) throws SQLException {
		delegate.setTypeMap(typeMap);
	}

	public Connection getDelegate() {
		return delegate;
	}

	public void setSchema(String schema) throws SQLException {
		delegate.setSchema(schema);
	}

	public String getSchema() throws SQLException {
		return delegate.getSchema();
	}

	public void abort(Executor executor) throws SQLException {
		delegate.abort(executor);
	}

	public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
		delegate.setNetworkTimeout(executor, milliseconds);
	}

	public int getNetworkTimeout() throws SQLException {
		return delegate.getNetworkTimeout();
	}
}
