/*
 * DataLayer.java
 *
 * Created on December 24, 2002, 11:34 AM
 */

package simple.db;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import simple.bean.ReflectionUtils;
import simple.db.Call.BatchUpdate;
import simple.event.TaskListener;
import simple.io.App;
import simple.results.BeanException;
import simple.results.Results;

/** This class manages data requests. It allows Call Objects to be added or
 *  removed and passes through the data request to the appropriate Call using
 *  the DataLayer instance's Connection Object.
 * @author Brian S. Krug
 */
public class DataLayer {
    private static final simple.io.Log log = simple.io.Log.getLog();
    protected static String[] DEFAULT_DATA_SOURCE_FACTORY_CLASSES = {
    	"simple.db.pool.PoolDataSourceFactory",
    	"simple.db.jndi.JNDIDataSourceFactory",
    	"simple.db.dbcp.DBCPDataSourceFactory",
    };
    public static interface RetryDecision {
    	public boolean decide(Connection conn, SQLException exception) ;
    }
    protected DataSourceFactory dataSourceFactory = null;
    public static RetryDecision DEFAULT_RETRY = new RetryDecision() {
    	public boolean decide(Connection conn, SQLException exception) {
    		return DataLayerMgr.getDBHelper(conn).reconnectNeeded(conn, exception);
    	}
    };
	protected static final Set<String> EXCLUDE_DISPLAY_PROPERTIES = new HashSet<String>();
	static {
		EXCLUDE_DISPLAY_PROPERTIES.add("connection");
		EXCLUDE_DISPLAY_PROPERTIES.add("password");
	}
    /** Holds value of property callSource. */
    private CallSource callSource;
    protected TaskListener taskListener;

    /** Creates a new DataLayer */
    public DataLayer() {
    }

    /** Sets the DataSourceFactory for all DataLayer calls.
     * @param dsf The DataSourceFactory to use
     */
    public void setDataSourceFactory(DataSourceFactory dsf) {
        dataSourceFactory = dsf;
    }

    /** Retrieves the DataSourceFactory for this DataLayer. Creates a new
     * <CODE>JNDIDataSourceFactory</CODE> based at "java:/comp/env/jdbc" if no DataSourceFactory
     * exists. If that fails, it creates a new <CODE>BasicDataSourceFactory</CODE>.
     * @return The current DataSourceFactory instance
     */
    public DataSourceFactory getDataSourceFactory() {
        if(dataSourceFactory == null) {
            dataSourceFactory = createDefaultDataSourceFactory();
            if(log.isDebugEnabled()) log.debug("Using DataSourceFactory " + dataSourceFactory);
        }
        return dataSourceFactory;
    }

    protected DataSourceFactory createDefaultDataSourceFactory() {
    	for(String className : DEFAULT_DATA_SOURCE_FACTORY_CLASSES) {
    		try {
    			Class<?> clazz = Class.forName(className);
    			if(!DataSourceFactory.class.isAssignableFrom(clazz)) {
    				log.info("Could not use '" + className + "' because it does not implement " + DataSourceFactory.class.getName());
    			} else {
    				Class<? extends DataSourceFactory> dsfClass = clazz.asSubclass(DataSourceFactory.class);
    				Constructor<? extends DataSourceFactory> cons;
					try {
						cons = dsfClass.getConstructor(Properties.class);
					} catch(NoSuchMethodException e) {
						try {
							cons = dsfClass.getConstructor();
						} catch(NoSuchMethodException e1) {
							log.info("Could not use '" + className + "' because no appropriate constructor was found: " + e1.getMessage());
							continue;
						}
					}
    				return cons.newInstance(App.getProperties(App.getMasterAppName()));
    			}
            } catch (NoClassDefFoundError e) {
            	log.info("Could not use '" + className + "' because " + e.getMessage());
			} catch (RuntimeException e) {
            	log.info("Could not use '" + className + "' because " + e.getMessage());
    		} catch(ClassNotFoundException e) {
            	log.info("Could not use '" + className + "' because " + e.getMessage());
			} catch(InstantiationException e) {
				log.info("Could not use '" + className + "' because " + e.getMessage());
			} catch(IllegalAccessException e) {
				log.info("Could not use '" + className + "' because " + e.getMessage());
			} catch(InvocationTargetException e) {
				log.info("Could not use '" + className + "' because " + e.getMessage());
			} catch(IOException e) {
				log.info("Could not use '" + className + "' because " + e.getMessage());
			}
    	}
    	try {
            return new BasicDataSourceFactory(App.getProperties(App.getMasterAppName()));
        } catch (IOException ioe) {
            log.info("Could not use BasicDataSourceFactory because " + ioe.getMessage());
        }
        throw new RuntimeException("Could not create any DataSourcFactory");
    }

    /** Returns the connection from the data source factory that matches the given name.
     * @return A connection from the DataSource
     * @param dataSourceName The name of the DataSource
     * @throws SQLException
     * @throws DataLayerException
     */
    public Connection getConnection(String dataSourceName) throws SQLException, DataSourceNotFoundException {
		return getConnection(dataSourceName, null);
	}

	/**
	 * Returns the connection from the data source factory that matches the given name.
	 * 
	 * @return A connection from the DataSource
	 * @param dataSourceName
	 *            The name of the DataSource
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	public Connection getConnection(String dataSourceName, Boolean autoCommit) throws SQLException, DataSourceNotFoundException {
    	DataSource ds = getDataSourceFactory().getDataSource(dataSourceName);
    	try {
			Connection conn = ds.getConnection();
			if(autoCommit != null) {
				boolean okay = false;
				try {
					conn.setAutoCommit(autoCommit);
					okay = true;
				} finally {
					if(!okay)
						try {
							conn.close();
						} catch(SQLException e1) {
							// ignore
						}
				}
			}
			return conn;
    	} catch(SQLException e) {
    		if(log.isDebugEnabled())
	    		try {
					log.debug("Data Source's Properties: " + ReflectionUtils.toPropertyMap(ds, EXCLUDE_DISPLAY_PROPERTIES));
	            } catch(IntrospectionException e1) {
	            }
            throw e;
    	}
    }

    /** Returns the connection from the data source factory for the specified call.
     * @return The connection from the appropriate DataSource
     * @param id The call id
     * @throws SQLException
     * @throws DataLayerException
     */
    public Connection getConnectionForCall(String id) throws SQLException, DataLayerException {
        return getConnection(findCall(id).getDataSourceName());
    }

	public Connection getConnectionForCall(String id, Boolean autoCommit) throws SQLException, DataLayerException {
		return getConnection(findCall(id).getDataSourceName(), autoCommit);
	}

    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param id The call id
     * @param bean The bean from which parameters should be extracted and into which parameters
     * should be saved (if the bean is not an Object array)
     * @param commit Whether to issue a commit after the update executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public int executeUpdate(String id, Object bean, boolean commit) throws SQLException, DataLayerException {
        return executeUpdate(id, bean, commit, DEFAULT_RETRY);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the update executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public int executeUpdate(String id, Object input, Object output, boolean commit) throws SQLException, DataLayerException {
        return executeUpdate(id, input, output, commit, DEFAULT_RETRY);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @param retry A Decision of whether to retry the call once
     * @throws SQLException
     * @throws DataLayerException
     */
    public int executeUpdate(String id, Object bean, boolean commit, RetryDecision retry) throws SQLException, DataLayerException {
        return executeUpdate(id, bean, bean, commit, retry);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @param retry A Decision of whether to retry the call once
     * @throws SQLException
     * @throws DataLayerException
     */
    public int executeUpdate(String id, Object input, Object output, boolean commit, RetryDecision retry) throws SQLException, DataLayerException {
        Call call = findCall(id);
        int ret;
        int retryAttempts = 1;// retry once
        while(true) {
        	Connection conn = getConnection(call.getDataSourceName());
            try {
		        try {
		            ret = executeUpdate(conn, call, input, output);
		        } catch (SQLException sqle) {
		            if(commit && !conn.getAutoCommit()) try { conn.rollback(); } catch(SQLException e1) {}
		            if(retryAttempts > 0 && retry != null && retry.decide(conn, sqle)) {
		            	log.info("Exception occured executing '" + id + "'; Retrying with a new connection", sqle);
		            	retryAttempts--;
		            	continue;
		            } else
		            	throw sqle;
				} catch(DataLayerException e) {
					if(commit && !conn.getAutoCommit())
						try {
							conn.rollback();
						} catch(SQLException e1) {
						}
					throw e;
		        }
		        if(commit && !conn.getAutoCommit()) conn.commit();
		        return ret;
	        } finally {
	            closeConnection(conn);
	        }
        }
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param conn The connection to use
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public int executeUpdate(Connection conn, String id, Object bean) throws SQLException, DataLayerException {
        Call call = findCall(id);
        return executeUpdate(conn, call, bean, bean);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param conn The connection to use
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public int executeUpdate(Connection conn, String id, Object input, Object output) throws SQLException, DataLayerException {
        Call call = findCall(id);
        return executeUpdate(conn, call, input, output);
    }
    /**
     *  Forwards this data request to the appropriate Call Object and returns
     *  the update count.
     *
     */
    protected int executeUpdate(Connection conn, Call call, Object input, Object output) throws SQLException, DataLayerException {
        return call.executeUpdate(conn, input, output, getTaskListener());
    }
    /**
     * Forwards this data request to the appropriate Call Object and returns
     * the update count. WARNING: the connection in the connHolder is not closed
     * @param connHolder The ConnectionHolder
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param retry A Decision of whether to retry the call once
     * @throws SQLException
     * @throws DataLayerException
     */
    public int executeUpdate(ConnectionHolder connHolder, String id, Object bean, RetryDecision retry) throws SQLException, DataLayerException {
        Call call = findCall(id);
        Connection conn = connHolder.getConnection();
        int ret;
        int retryAttempts = 1;// retry once
        while(true) {
            try {
	            ret = executeUpdate(conn, call, bean, bean);
	        } catch (SQLException sqle) {
	            if(retryAttempts > 0 && retry != null && retry.decide(conn, sqle)) {
	            	log.info("Exception occured executing '" + id + "'; Retrying with a new connection", sqle);
	            	conn = connHolder.reconnect();
	            	retryAttempts--;
	            	continue;
	            } else
	            	throw sqle;
	        }
	        return ret;
        }
    }
    /**
     * Forwards this data request to the appropriate Call Object and returns
     * a Results Object. WARNING: the connection in the connHolder is not closed
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param connHolder The ConnectionHolder
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param retry A Decision of whether to retry the call once
     * @throws SQLException
     * @throws DataLayerException
     */
    public Results executeQuery(ConnectionHolder connHolder, String id, Object bean, RetryDecision retry) throws SQLException, DataLayerException {
        Call call = findCall(id);
        Connection conn = connHolder.getConnection();
        Results ret;
        int retryAttempts = 1;// retry once
        while(true) {
            try {
	            ret = executeQuery(conn, call, bean, bean);
	        } catch (SQLException sqle) {
	            if(retryAttempts > 0 && retry != null && retry.decide(conn, sqle)) {
	            	log.info("Exception occured executing '" + id + "'; Retrying with a new connection", sqle);
	            	conn = connHolder.reconnect();
	            	retryAttempts--;
	            	continue;
	            } else
	            	throw sqle;
	        }
	        return ret;
        }
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters. WARNING: the connection in the connHolder is not closed
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param connHolder The ConnectionHolder
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param retry A Decision of whether to retry the call once
     * @throws SQLException
     * @throws DataLayerException
     */
    public Object[] executeCall(ConnectionHolder connHolder, String id, Object bean, RetryDecision retry) throws SQLException, DataLayerException {
        Call call = findCall(id);
        Connection conn = connHolder.getConnection();
        Object[] ret;
        int retryAttempts = 1;// retry once
        while(true) {
            try {
	            ret = executeCall(conn, call, bean, bean);
	        } catch (SQLException sqle) {
	            if(retryAttempts > 0 && retry != null && retry.decide(conn, sqle)) {
	            	log.info("Exception occured executing '" + id + "'; Retrying with a new connection", sqle);
	            	conn = connHolder.reconnect();
	            	retryAttempts--;
	            	continue;
	            } else
	            	throw sqle;
	        }
	        return ret;
        }
    }

    /** Creates a BatchUpdate object that can be used to update the database in efficient batches
     * @param id The call id
     * @param concurrent If the BatchUpdate object should support add to it and executing it at the same time. Even if
     * this is set to true, only one thread may invoke the executeBatch() method at a time. But it does allow multiple concurrent
     * invocations of addBatch().
     * @param overageAllowed A factor that determines how many additional batches the executeBatch() method will pick up that were added AFTER
     * the method was called.
     * @return The BatchUpdate object
     * @throws ParameterException
     * @throws CallNotFoundException
     */
    public BatchUpdate createBatchUpdate(String id, boolean concurrent, float overageAllowed) throws ParameterException, CallNotFoundException {
    	Call call = findCall(id);
        return call.createBatchUpdate(concurrent, overageAllowed);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public Object[] executeCall(String id, Object bean, boolean commit) throws SQLException, DataLayerException {
        return executeCall(id, bean, commit, DEFAULT_RETRY);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public Object[] executeCall(String id, Object input, Object output, boolean commit) throws SQLException, DataLayerException {
        return executeCall(id, input, output, commit, DEFAULT_RETRY);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @param retry A Decision of whether to retry the call once
     * @throws SQLException
     * @throws DataLayerException
     */
    public Object[] executeCall(String id, Object bean, boolean commit, RetryDecision retry) throws SQLException, DataLayerException {
        return executeCall(id, bean, bean, commit, retry);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @param retry A Decision of whether to retry the call once
     * @throws SQLException
     * @throws DataLayerException
     */
    public Object[] executeCall(String id, Object input, Object output, boolean commit, RetryDecision retry) throws SQLException, DataLayerException {
    	return executeCall(null, id, input, output, commit, retry);
    }
    public Object[] executeCall(String dataSourceName, String id, Object input, Object output, boolean commit, RetryDecision retry) throws SQLException, DataLayerException {
        Call call = findCall(id);
        Object[] ret;
        int retryAttempts = 1;// retry once
        while(true) {
        	Connection conn = getConnection(dataSourceName == null ? call.getDataSourceName() : dataSourceName);
            try {
		        try {
		            ret = executeCall(conn, call, input, output);
		        } catch (SQLException sqle) {
		            if(commit && !conn.getAutoCommit()) try { conn.rollback(); } catch(SQLException e1) {}
		            if(retryAttempts > 0 && retry != null && retry.decide(conn, sqle)) {
		            	log.info("Exception occured executing '" + id + "'; Retrying with a new connection", sqle);
		            	retryAttempts--;
		            	continue;
		            } else
		            	throw sqle;
				} catch(DataLayerException e) {
					if(commit && !conn.getAutoCommit())
						try {
							conn.rollback();
						} catch(SQLException e1) {
						}
					throw e;
		        }
		        if(commit && !conn.getAutoCommit()) conn.commit();
		        return ret;
	        } finally {
	            closeConnection(conn);
	        }
        }
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param conn The connection to use
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public Object[] executeCall(Connection conn, String id, Object bean) throws SQLException, DataLayerException {
        Call call = findCall(id);
        return executeCall(conn, call, bean, bean);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param conn The connection to use
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public Object[] executeCall(Connection conn, String id, Object input, Object output) throws SQLException, DataLayerException {
        Call call = findCall(id);
        return executeCall(conn, call, input, output);
    }

    /**
     *  Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters.
     *
     */
    protected Object[] executeCall(Connection conn, Call call, Object input, Object output) throws SQLException, DataLayerException {
        return call.executeCall(conn, input, output, getTaskListener());
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public Results executeQuery(String id, Object bean) throws SQLException, DataLayerException {
        return executeQuery(id, bean, false);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public Results executeQuery(String id, Object input, Object output) throws SQLException, DataLayerException {
        return executeQuery(id, input, output, false);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public Results executeQuery(String id, Object bean, boolean commit) throws SQLException, DataLayerException {
        return executeQuery(id, bean, commit, DEFAULT_RETRY);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public Results executeQuery(String id, Object input, Object output, boolean commit) throws SQLException, DataLayerException {
        return executeQuery(id, input, output, commit, DEFAULT_RETRY);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @param retry A Decision of whether to retry the call once
     * @throws SQLException
     * @throws DataLayerException
     */
    public Results executeQuery(String id, Object bean, boolean commit, RetryDecision retry) throws SQLException, DataLayerException {
        return executeQuery(id, bean, bean, commit, retry);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @param retry A Decision of whether to retry the call once
     * @throws SQLException
     * @throws DataLayerException
     */
    public Results executeQuery(String id, Object input, Object output, boolean commit, RetryDecision retry) throws SQLException, DataLayerException {
        Call call = findCall(id);
        Results ret;
        int retryAttempts = 1;// retry once
        while(true) {
        	Connection conn = getConnection(call.getDataSourceName());
            try {
		        try {
		            ret = executeQuery(conn, call, input, output);
		        } catch (SQLException sqle) {
		            if(commit && !conn.getAutoCommit()) try { conn.rollback(); } catch(SQLException e1) {}
		            if(retryAttempts > 0 && retry != null && retry.decide(conn, sqle)) {
		            	log.info("Exception occured executing '" + id + "'; Retrying with a new connection", sqle);
		            	retryAttempts--;
		            	continue;
		            } else
		            	throw sqle;
				} catch(DataLayerException e) {
					if(commit && !conn.getAutoCommit())
						try {
							conn.rollback();
						} catch(SQLException e1) {
						}
					throw e;
		        }
		        if(commit && !conn.getAutoCommit()) conn.commit();
		        return ret;
	        } finally {
	            closeConnection(conn);
	        }
        }
    }

    public void selectInto(String id, Object bean) throws SQLException, DataLayerException, BeanException {
		selectInto(id, bean, true);
    }

    public void selectInto(String id, Object input, Object output) throws SQLException, DataLayerException, BeanException {
		selectInto(id, input, output, true);
    }

    public void selectInto(String id, Object bean, boolean commit) throws SQLException, DataLayerException, BeanException {
    	selectInto(id, bean, bean, commit);
    }

    public void selectInto(String id, Object input, Object output, boolean commit) throws SQLException, DataLayerException, BeanException {
		selectInto(id, input, output, commit, DEFAULT_RETRY);
	}

	public void selectInto(String id, Object input, Object output, boolean commit, RetryDecision retry) throws SQLException, DataLayerException, BeanException {
		Call call = findCall(id);
		Results results;
		int retryAttempts = 1;// retry once
		while(true) {
			Connection conn = getConnection(call.getDataSourceName());
			try {
				try {
					results = executeQuery(conn, call, input, output);
					if(!results.next())
						throw new NotEnoughRowsException();
					if(!results.isGroupEnding(0))
						throw new TooManyRowsException();
					results.fillBean(output);
				} catch(SQLException sqle) {
					if(commit && !conn.getAutoCommit())
						try {
							conn.rollback();
						} catch(SQLException e1) {
						}
					if(retryAttempts > 0 && retry != null && retry.decide(conn, sqle)) {
						log.info("Exception occured executing '" + id + "'; Retrying with a new connection", sqle);
						retryAttempts--;
						continue;
					} else
						throw sqle;
				} catch(DataLayerException e) {
					if(commit && !conn.getAutoCommit())
						try {
							conn.rollback();
						} catch(SQLException e1) {
						}
					throw e;
				}
				if(commit && !conn.getAutoCommit())
					conn.commit();
				return;
			} finally {
				closeConnection(conn);
			}
		}
    }

    public void selectInto(Connection conn, String id, Object bean) throws SQLException, DataLayerException, BeanException {
    	Results results = executeQuery(conn, id, bean);
    	if(!results.next())
    		throw new NotEnoughRowsException();
    	if(!results.isGroupEnding(0))
    		throw new TooManyRowsException();
    	results.fillBean(bean);
    }
    public void selectInto(Connection conn, String id, Object input, Object output) throws SQLException, DataLayerException, BeanException {
    	Results results = executeQuery(conn, id, input, output);
    	if(!results.next())
    		throw new NotEnoughRowsException();
    	if(!results.isGroupEnding(0))
    		throw new TooManyRowsException();
    	results.fillBean(output);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param conn The connection to use
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public Results executeQuery(Connection conn, String id, Object bean) throws SQLException, DataLayerException {
        Call call = findCall(id);
        return executeQuery(conn, call, bean, bean);
    }
    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param conn The connection to use
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public Results executeQuery(Connection conn, String id, Object input, Object output) throws SQLException, DataLayerException {
        Call call = findCall(id);
        return executeQuery(conn, call, input, output);
    }
    /**
     *  Forwards this data request to the appropriate Call Object and returns
     *  a Results Object.
     *
     */
    protected Results executeQuery(Connection conn, Call call, Object input, Object output) throws SQLException, DataLayerException {
       return call.executeQuery(conn, input, output, getTaskListener());
    }

    /**
     * Finds a Call based on the call id.
     */
    public Call findCall(String id) throws CallNotFoundException {
        return getCallSource().getCall(id);
    }

    /**
     * Closes a connection without throwing an exception.
     */
    protected static void closeConnection(Connection conn) {
        log.debug("Closing connection " + conn);
        if(conn != null) try { conn.close(); log.debug("Closed=" + conn.isClosed());} catch(SQLException e) { log.warn("Couldn't close connection", e);}
    }

    /** Getter for property callSource.
     * @return Value of property callSource.
     *
     */
    public CallSource getCallSource() {
        if(callSource == null) callSource = new RootCallSource();
        return this.callSource;
    }

    /** Setter for property callSource.
     * @param callSource New value of property callSource.
     *
     */
    public void setCallSource(CallSource callSource) {
        this.callSource = callSource;
    }

    /** Returns a String of details about the calls contained in the DataLayer.
     *
     * @return Formatted Info about this DataLayer
     */
    public String info() {
        StringBuilder sb = new StringBuilder(getClass().getName() + ": ");
        for(String id : getCallSource().getCallIds()) {
            sb.append("\n\tCall '");
            sb.append(id);
            try {
                Call c = findCall(id);
                sb.append("': SQL='");
                sb.append(c.getSql());
                sb.append("'; ");
                sb.append(c.getArguments().length);
                sb.append(" arguments");
            } catch(CallNotFoundException cnfe) {
                sb.append("': NOT FOUND!");
            }
        }
        return sb.toString();
    }

	public TaskListener getTaskListener() {
		return taskListener;
	}

	public void setTaskListener(TaskListener taskListener) {
		this.taskListener = taskListener;
	}
}