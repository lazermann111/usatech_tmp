/*
 * DataLayerException.java
 *
 * Created on March 12, 2003, 9:35 AM
 */

package simple.db;

/**
 * Thrown if an exception occurs in the DataLayer class
 *
 * @author  Brian S. Krug
 */
public class DataLayerException extends java.lang.Exception {
	private static final long serialVersionUID = 10090908L;
    
    /**
     * Creates a new instance of <code>DataLayerException</code> without detail message.
     */
    public DataLayerException() {
    }
    
    
    /**
     * Constructs an instance of <code>DataLayerException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public DataLayerException(String msg) {
        super(msg);
    }
    
    /** Creates a new instance of <code>DataLayerException</code> without detail message but with a cause.
     * @param cause
     */
    public DataLayerException(Throwable cause) {
        super(cause);
    }
    
    
    /** Constructs an instance of <code>DataLayerException</code> with the specified detail message and cause.
     * @param cause
     * @param msg the detail message.
     */
    public DataLayerException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
