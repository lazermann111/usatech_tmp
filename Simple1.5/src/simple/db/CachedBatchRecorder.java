/*
 * Created on May 9, 2005
 *
 */
package simple.db;

import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

/** Executes batches on the database on a regular interval from a set of current info.
 *
 * @author Brian S. Krug
 *
 */
public abstract class CachedBatchRecorder<K,R> extends BatchRecorder<R> {
	private static final Log log = Log.getLog();
    protected final Cache<K,R,RuntimeException> currentBatches = new LockSegmentCache<K,R,RuntimeException>() {
		@Override
		protected R createValue(K key, Object... additionalInfo) {
			return createInfo(key, additionalInfo);
		}
		@Override
		protected boolean keyEquals(K key1, K key2) {
			return infoKeyEquals(key1, key2);
		}
		@Override
		protected boolean valueEquals(R value1, R value2) {
			return infoEquals(value1, value2);
		}
	};

    public CachedBatchRecorder() {
        super();
    }

    protected abstract R createInfo(K infoKey, Object... additionalInfo) ;

	protected boolean infoKeyEquals(K infoKey1, K infoKey2) {
		return ConvertUtils.areEqual(infoKey1, infoKey2);
	}
	protected boolean infoEquals(R info1, R info2) {
		return ConvertUtils.areEqual(info1, info2);
	}

	protected R getOrCreateInfo(K infoKey) {
		return currentBatches.getOrCreate(infoKey);
	}
    @Override
    protected void addBatches() {
    	if(batch != null)
	    	for(R info : currentBatches.values())
				try {
					batch.addBatch(info);
				} catch(ParameterException e) {
					log.error("Could not add batches", e);
				}
    }

	@Override
	public boolean isAsynchronous() {
		return true;
	}
}
