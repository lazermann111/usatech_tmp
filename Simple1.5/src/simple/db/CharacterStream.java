/**
 *
 */
package simple.db;

import java.io.Reader;

/**
 * @author Brian S. Krug
 *
 */
public interface CharacterStream {
	public Reader getReader() ;
	public long getLength() ;
}
