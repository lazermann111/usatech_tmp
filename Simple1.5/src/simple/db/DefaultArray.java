package simple.db;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.sql.Types;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;

public class DefaultArray implements Array {
	protected Object[] elements;
	protected final SQLType componentSqlType;
	
	public DefaultArray(Object[] elements, SQLType componentSqlType) {
		this.elements = elements;
		this.componentSqlType = componentSqlType;
	}

	public void free() throws SQLException {
		elements = null;
	}

	public Object getArray() throws SQLException {
		return getArray(null);
	}

	public Object getArray(Map<String, Class<?>> map) throws SQLException {
		return elements.clone();
	}

	public Object getArray(long index, int count) throws SQLException {
		return getArray(index, count, null);
	}

	public Object getArray(long index, int count, Map<String, Class<?>> map) throws SQLException {
		if(count > elements.length - (int)index + 1)
			count = elements.length - (int)index + 1;
		Object[] ret = new Object[count];
		System.arraycopy(elements, (int)index - 1, ret, 0, count);
		return ret;
	}

	public int getBaseType() throws SQLException {
		return componentSqlType.getTypeCode();
	}

	public String getBaseTypeName() throws SQLException {
		return componentSqlType.getTypeName();
	}

	public ResultSet getResultSet() throws SQLException {
		return getResultSet(null);
	}

	public ResultSet getResultSet(Map<String, Class<?>> map) throws SQLException {
		return getResultSet(1, elements.length, map);
	}

	public ResultSet getResultSet(long index, int count) throws SQLException {
		return getResultSet(index, count, null);
	}

	public ResultSet getResultSet(long index, int count, Map<String, Class<?>> map) throws SQLException {
		final int offset = (int)index - 1;
		final int length = Math.min(count, elements.length);
		final AtomicInteger pos = new AtomicInteger(-1);
		AbstractBasicResultSetColumn indexColumn = new AbstractBasicResultSetColumn() {
			public Object getCurrentValue() throws SQLException {
				return pos.get() + offset + 1;
			}
			public void setCurrentValue(Object object) throws SQLException {
				throw new SQLException("Not supported");
			}
			public void setCurrentValue(Object object, long scaleOrLength) throws SQLException {
				throw new SQLException("Not supported");
			}			
		};
		indexColumn.setColumnClassName(SQLTypeUtils.getJavaType(componentSqlType).getName());
		indexColumn.setColumnTypeName(SQLTypeUtils.getTypeCodeName(componentSqlType.getTypeCode()));
		indexColumn.setSqlType(componentSqlType.getTypeCode());
		AbstractBasicResultSetColumn valueColumn = new AbstractBasicResultSetColumn() {
			public Object getCurrentValue() throws SQLException {
				return elements[pos.get() + offset];
			}
			public void setCurrentValue(Object object) throws SQLException {
				throw new SQLException("Not supported");
			}
			public void setCurrentValue(Object object, long scaleOrLength) throws SQLException {
				throw new SQLException("Not supported");
			}			
		};
		valueColumn.setColumnClassName(Integer.class.getName());
		valueColumn.setColumnTypeName("INTEGER");
		valueColumn.setSqlType(Types.INTEGER);
		return new ColumnsResultSet(indexColumn, valueColumn) {
			public boolean absolute(int row) throws SQLException {
				if(row < 1)
					pos.set(-1);
				else if(row > length)
					pos.set(length);
				else
					pos.set(row - 1);
				return pos.get() >= 0 && pos.get() < length;
			}
			public void afterLast() throws SQLException {
				pos.set(length);
			}
			public void beforeFirst() throws SQLException {
				pos.set(-1);
			}
			public void cancelRowUpdates() throws SQLException {				
			}
			public void clearWarnings() throws SQLException {
			}
			public void close() throws SQLException {
			}
			public void deleteRow() throws SQLException {
				throw new SQLException("Not supported");
			}
			public boolean first() throws SQLException {
				pos.set(0);
				return pos.get() >= 0 && pos.get() < length;
			}
			public int getConcurrency() throws SQLException {
				return ResultSet.CONCUR_READ_ONLY;
			}
			public String getCursorName() throws SQLException {
				return null;
			}
			public int getFetchDirection() throws SQLException {
				return ResultSet.FETCH_FORWARD;
			}
			public int getFetchSize() throws SQLException {
				return length;
			}
			public int getHoldability() throws SQLException {
				return ResultSet.HOLD_CURSORS_OVER_COMMIT;
			}
			public int getRow() throws SQLException {
				return pos.get() + 1;
			}
			public Statement getStatement() throws SQLException {
				return null;
			}
			public int getType() throws SQLException {
				return ResultSet.TYPE_SCROLL_INSENSITIVE;
			}
			public SQLWarning getWarnings() throws SQLException {
				return null;
			}
			public void insertRow() throws SQLException {
				throw new SQLException("Not supported");
			}
			public boolean isAfterLast() throws SQLException {
				return pos.get() >= length;
			}
			public boolean isBeforeFirst() throws SQLException {
				return pos.get() < 0;
			}
			public boolean isClosed() throws SQLException {
				return elements == null;
			}
			public boolean isFirst() throws SQLException {
				return pos.get() == 0;
			}
			public boolean isLast() throws SQLException {
				return pos.get() == length - 1;
			}
			public boolean last() throws SQLException {
				pos.set(length - 1);
				return pos.get() >= 0 && pos.get() < length;
			}
			public void moveToCurrentRow() throws SQLException {
			}
			public void moveToInsertRow() throws SQLException {
				throw new SQLException("Not supported");
			}
			public boolean next() throws SQLException {
				if(pos.get() >= length)
					return false;
				pos.incrementAndGet();
				return pos.get() >= 0 && pos.get() < length;
			}
			public boolean previous() throws SQLException {
				if(pos.get() < 0)
					return false;
				pos.decrementAndGet();
				return pos.get() >= 0 && pos.get() < length;
			}
			public void refreshRow() throws SQLException {				
			}
			public boolean relative(int rows) throws SQLException {
				pos.set(Math.min(pos.get() + rows, length));
				return pos.get() >= 0 && pos.get() < length;
			}
			public boolean rowDeleted() throws SQLException {
				return false;
			}
			public boolean rowInserted() throws SQLException {
				return false;
			}
			public boolean rowUpdated() throws SQLException {
				return false;
			}
			public void setFetchDirection(int direction) throws SQLException {
				throw new SQLException("Not supported");
			}
			public void setFetchSize(int rows) throws SQLException {
				throw new SQLException("Not supported");
			}
			public void updateRow() throws SQLException {
				throw new SQLException("Not supported");
			}
			public boolean isWrapperFor(Class<?> iface) throws SQLException {
				return false;
			}
			public <T> T unwrap(Class<T> iface) throws SQLException {
				return null;
			}	
		};
	}

	@Override
	public String toString() {
		return Arrays.deepToString(elements);
	}
}
