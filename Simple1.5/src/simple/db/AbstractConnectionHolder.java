package simple.db;

import java.sql.Connection;
import java.sql.SQLException;

import simple.io.Log;

public abstract class AbstractConnectionHolder implements ConnectionHolder {
	private static final Log log = Log.getLog();
	protected Connection conn;
	protected abstract Connection createConnection() throws SQLException, DataSourceNotFoundException;
	public Connection getConnection() throws SQLException, DataSourceNotFoundException {
		if(conn == null) {
			conn = createConnection();
		}
		return conn;
	}
	public boolean closeConnection() {
		if(conn != null) {
			try {
				conn.close();
			} catch(SQLException e) {
				log.info("Could not close connection: " + e.getMessage());
			}
			conn = null;
			return true;
		}
		return false;
	}
	public Connection reconnect() throws SQLException, DataSourceNotFoundException {
		closeConnection();
		return getConnection();
	}

	public void commitOrRollback(boolean commit) throws SQLException {
		if(conn != null && !conn.getAutoCommit()) {
			if(commit)
				conn.commit();
			else
				try {
					conn.rollback();
				} catch(SQLException e) {
					log.info("Could not rollback transaction: " + e.getMessage());
				}
		}
	}
}
