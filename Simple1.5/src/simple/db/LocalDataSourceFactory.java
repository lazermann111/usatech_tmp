/*
 * BasicDataSourceFactory.java
 *
 * Created on March 12, 2003, 9:45 AM
 */

package simple.db;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Statement;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import javax.sql.DataSource;

import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.CaseInsensitiveComparator;
import simple.util.FilterMap;

/** Creates and retrieves DataSources from the configurations specified by the
 * properties passed to its constructor. Each DataSource should have the following
 * properties "DRIVER", "DATABASE", "USERNAME", and "PASSWORD". Each of these
 * property names are prefixed with the name of the DataSource and a period ("."). The
 * default DataSource (retrieved by passing NULL or "") has no prefix on its properties.
 * Example: the following properties are passed to the constructor of this factory:
 * <P><PRE>DS1.DRIVER=oracle.jdbc.driver.OracleDriver
 * DS1.DATABASE=jdbc:oracle:thin:@10.0.0.127:1521:myDataBase
 * DS1.USERNAME=myuser
 * DS1.PASSWORD=mypassword
 * DS2.DRIVER=sun.jdbc.odbc.JdbcOdbcDriver
 * DS2.DATABASE=jdbc:odbc:myODBCDataSource
 * DS2.USERNAME=myuser2
 * DS2.PASSWORD=mypassword2<PRE></P>
 * Then two DataSources would be configured on named "DS1" and the other "DS2"
 *
 * @author  Brian S. Krug
 */
public class LocalDataSourceFactory extends AbstractDataSourceFactory {
    private static final Log log = Log.getLog();
	protected final Map<String, Object> props = new TreeMap<String, Object>(new CaseInsensitiveComparator<String>());
    protected String[] names;
    public static final String PROP_DRIVER = "DRIVER";
    public static final String PROP_DATABASE = "DATABASE";
    public static final String PROP_USERNAME = "USERNAME";
    public static final String PROP_PASSWORD = "PASSWORD";
    public static final String PROP_AUTOCOMMIT = "AUTOCOMMIT";
    public final static String PROP_TESTONBORROW = "testOnBorrow";
    public final static String PROP_VALIDATIONQUERY = "validationQuery";
    public final static String PROP_MAXAGE = "maxAge";
    public final static String PROP_CONNECTIONPROPERTIES = "connectionProperties";

    public LocalDataSourceFactory() {
    }

    /** Creates a new instance of BasicDataSourceFactory
     * @param paramString A string of properties of the form "property1=value1;property2=value2;..."
     */
    public LocalDataSourceFactory(String paramString) {
        StringTokenizer token =  new StringTokenizer(paramString, ";");
        while(token.hasMoreTokens()) {
            String line = token.nextToken().trim();
            String[] entry = StringUtils.split(line, '=');
            if(entry.length == 2) setProperty(entry[0].trim().toUpperCase(), entry[1]);
        }
    }

    /** Creates a new instance of BasicDataSourceFactory with the specified properties
     * defining the set of DataSource's this factory contains.
     * @param properties The properties for configuring this factory
     */
    public LocalDataSourceFactory(Properties properties) {
        for(Object p : properties.keySet()) {
        	String name = (String)p;
			setProperty(name, properties.get(name));
        }
    }

	protected void setProperty(String name, Object value) {
    	if(!possiblySetBeanProperty(name,value))
    		props.put(name, value);
    }
    public void addDataSource(String name, String driver, String url, String username, String password) {
    	addDataSource(name, driver, url, username, password, false);
    }

    public void addDataSource(String name, String driver, String url, String username, String password, boolean autoCommit) {
        props.put(name + "." + LocalDataSourceFactory.PROP_DATABASE, url);
        props.put(name + "." + LocalDataSourceFactory.PROP_DRIVER, driver);
        props.put(name + "." + LocalDataSourceFactory.PROP_USERNAME, username);
        props.put(name + "." + LocalDataSourceFactory.PROP_PASSWORD, password);
        props.put(name + "." + LocalDataSourceFactory.PROP_AUTOCOMMIT, String.valueOf(autoCommit));
    }
	@Override
	protected boolean isCaseSensitive() {
		return false;
	}
    @Override
	protected ExtendedDataSource createDataSource(String name) throws DataSourceNotFoundException {
    	String prefix = (name == null || name.trim().length() == 0 ? "" : name + ".");
		LocalDataSource lds = new LocalDataSource(new FilterMap<Object>(props, prefix, null));
    	if(lds.getDatabase() == null || lds.getDatabase().trim().length() == 0) {
    		log.debug("Props = " + props.toString());
            throw new DataSourceNotFoundException("Database property is not specified for '" + name + "'");
        }
    	try {
            lds.checkDriver();
        } catch(SQLException e) {
            throw new DataSourceNotFoundException("Could not load Driver Class '" + lds.getDriver() + "'", e);
        }
        log.debug("Creating new LocalDataSource with " + lds);
        return lds;
    }

    /**
     * @see simple.db.AbstractDataSourceFactory#getDriverClassName(java.lang.String)
     */
    @Override
    protected String getDriverClassName(String dataSourceName) {
    	String prefix = (dataSourceName == null || dataSourceName.trim().length() == 0 ? "" : dataSourceName + ".");
    	return getProperty(props, prefix + "driverClassName", prefix + LocalDataSourceFactory.PROP_DRIVER);
    }

    public void refreshProps(Properties properties) {
    	for(Object p : properties.keySet()) {
        	String name = (String)p;
        	setProperty(name, properties.getProperty(name));
        }
        names = null;
        props.keySet().removeAll(properties.keySet());

        for(String name : sources.keySet()) {
        	DataSource ds;
			try {
				ds = sources.getIfPresent(name);
			} catch(DataSourceNotFoundException e) {
				log.warn("This should not occur", e);
				continue;
			}
        	if(ds instanceof LocalDataSource) {
        		String prefix = (name == null || name.trim().length() == 0 ? "" : name + ".");
				((LocalDataSource) ds).reload(new FilterMap<Object>(props, prefix, null));
        	}
        }
    }

	protected static String getProperty(Map<String, Object> map, String... names) {
    	for(String s : names) {
			String p = ConvertUtils.getStringSafely(map.get(s));
    		if(p != null)
    			return p;
    	}
    	return null;
    }
    /** Retrieves an array of the unique names of each <CODE>javax.sql.DataSource</CODE>
     * that this object contains. This implementation looks through the properties
     * passed to its constructor to find any that end with ".DRIVER" or equal "DRIVER".
     * @return An array of Strings that should yield a non-null return from the
     * <CODE>getDataSource()</CODE> method (although an exception might be thrown).
     */
    public String[] getDataSourceNames() {
        if(names == null) {
            final int len = PROP_DRIVER.length() + 1; // add one for the period
            java.util.List<String> l = new java.util.LinkedList<String>();
            for(String n : props.keySet()) {
                if(n.equalsIgnoreCase(PROP_DRIVER) || n.equals("driverClassName")) l.add("");
                else if(n.toUpperCase().endsWith("." + PROP_DRIVER) || n.endsWith(".driverClassName")) l.add(n.substring(0, n.length() - len));
            }
            names = l.toArray(new String[l.size()]);
        }
        return names;
    }
    protected static class LocalConnection extends DelegatingConnection {
    	protected final long createTime;
    	protected final Queue<LocalConnection> pool;
    	public LocalConnection(Connection delegate, Queue<LocalConnection> pool) {
			super(delegate);
			this.pool = pool;
			this.createTime = System.currentTimeMillis();
		}
		@Override
		protected void finalize() throws Throwable {
			// clean up the connection on finalize - this should occur only when a thread is gc'd
			try {
				reallyClose();
			} catch(SQLException e) {
				// Ignore
			}
		}
		@Override
    	public void close() throws SQLException {
    		// Return to pool so connection will be used by same thread later
			if(pool != null)
				pool.add(this);
    	}
		protected void reallyClose() throws SQLException {
    		super.close();
    	}
	}
    protected static class LocalStatementTrackingConnection extends LocalConnection {
    	protected final Map<Statement,Long> statements = new WeakHashMap<Statement,Long>();
    	protected class LocalStatement extends DelegatingStatement {
			public LocalStatement(Statement delegate, Connection connection) {
				super(delegate, connection);
			}
    		@Override
    		public void close() throws SQLException {
    			statements.remove(this);
    			super.close();
    		}
    	}
    	protected class LocalPreparedStatement extends DelegatingPreparedStatement {
			public LocalPreparedStatement(PreparedStatement delegate, Connection connection) {
				super(delegate, connection);
			}
    		@Override
    		public void close() throws SQLException {
    			statements.remove(this);
    			super.close();
    		}
    	}
    	protected class LocalCallableStatement extends DelegatingCallableStatement {
			public LocalCallableStatement(CallableStatement delegate, Connection connection) {
				super(delegate, connection);
			}
    		@Override
    		public void close() throws SQLException {
    			statements.remove(this);
    			super.close();
    		}
    	}
		public LocalStatementTrackingConnection(Connection delegate, Queue<LocalConnection> pool) {
			super(delegate, pool);
		}
		@Override
    	public void close() throws SQLException {
    		//clean up statements, but don't close connection
    		for(Statement s : statements.keySet())
    			while(s.getMoreResults(Statement.CLOSE_ALL_RESULTS)) ; //s.close();
    		statements.clear();
    	}
		@Override
		public Statement createStatement() throws SQLException {
			Statement s = super.createStatement();
			statements.put(s, System.currentTimeMillis());
			return new LocalStatement(s, this);
		}
		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
			Statement s = delegate.createStatement(resultSetType, resultSetConcurrency);
			statements.put(s, System.currentTimeMillis());
			return new LocalStatement(s, this);
		}
		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			Statement s = delegate.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
			statements.put(s, System.currentTimeMillis());
			return new LocalStatement(s, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql);
			statements.put(ps, System.currentTimeMillis());
			return new LocalPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, autoGeneratedKeys);
			statements.put(ps, System.currentTimeMillis());
			return new LocalPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, resultSetType, resultSetConcurrency);
			statements.put(ps, System.currentTimeMillis());
			return new LocalPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
			statements.put(ps, System.currentTimeMillis());
			return new LocalPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, columnIndexes);
			statements.put(ps, System.currentTimeMillis());
			return new LocalPreparedStatement(ps, this);
		}
		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
			PreparedStatement ps = delegate.prepareStatement(sql, columnNames);
			statements.put(ps, System.currentTimeMillis());
			return new LocalPreparedStatement(ps, this);
		}
		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			CallableStatement cs = delegate.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
			statements.put(cs, System.currentTimeMillis());
			return new LocalCallableStatement(cs, this);
		}
		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			CallableStatement cs = delegate.prepareCall(sql, resultSetType, resultSetConcurrency);
			statements.put(cs, System.currentTimeMillis());
			return new LocalCallableStatement(cs, this);
		}
		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			CallableStatement cs = delegate.prepareCall(sql);
			statements.put(cs, System.currentTimeMillis());
			return new LocalCallableStatement(cs, this);
		}
    }
    public static class LocalDataSource implements ExtendedDataSource {
    	protected String driver;
        protected boolean driverLoaded = false;
        protected boolean changed = false;
        protected String database;
        protected String username;
        protected String password;
        protected ReentrantLock lock = new ReentrantLock();
        protected boolean autoCommit;
        protected boolean trackStatements = false;
        protected boolean testOnBorrow = false;
        protected String validationQuery;
        protected long maxAge;
		protected final Properties connectionProperties = new Properties();

        protected ThreadLocal<Queue<LocalConnection>> localConnList = new ThreadLocal<Queue<LocalConnection>>() {
        	@Override
			protected Queue<LocalConnection> initialValue() {
        		return new LinkedList<LocalConnection>();
			}
        };

		public LocalDataSource(Map<String, Object> props) {
        	configure(props);
        }

		protected void configure(Map<String, Object> props) {
			connectionProperties.clear();
			String propText = getProperty(props, PROP_CONNECTIONPROPERTIES);
			if(propText != null && propText.trim().length() > 0) {
				try {
					connectionProperties.load(new ByteArrayInputStream(propText.replace(';', '\n').getBytes()));
				} catch(IOException e) {
					log.warn("Could not read connection properties '" + propText + "'; ignoring them.", e);
				}
			}
			driver = getProperty(props, PROP_DRIVER, "driverClassName");
    		database = getProperty(props, PROP_DATABASE, "url");
			setUsername(getProperty(props, PROP_USERNAME, "username"));
			setPassword(getProperty(props, PROP_PASSWORD, "password"));
    		autoCommit = ConvertUtils.getBooleanSafely(getProperty(props, PROP_AUTOCOMMIT, "defaultAutoCommit"), false);
    		trackStatements = ConvertUtils.getBooleanSafely(getProperty(props, "trackStatements"), false);
    		validationQuery = getProperty(props, PROP_VALIDATIONQUERY);
    		if(validationQuery != null && (validationQuery=validationQuery.trim()).length() > 0) {
    			testOnBorrow = ConvertUtils.getBooleanSafely(getProperty(props, PROP_TESTONBORROW), false);
    		}
			maxAge = ConvertUtils.getLongSafely(getProperty(props, PROP_MAXAGE), 0);
        }

		public void reload(Map<String, Object> props) {
            lock.lock();
            try {
            	String oldDriver = this.driver;
            	String oldDatabase = this.database;
            	String oldUsername = this.username;
            	String oldPassword = this.password;
            	configure(props);
            	if(!ConvertUtils.areEqual(oldDriver,driver)) {
            		driverLoaded = false;
            		changed = true;
            	} else if(!ConvertUtils.areEqual(oldDatabase,database)
                    || !ConvertUtils.areEqual(oldUsername,username)
                    || !ConvertUtils.areEqual(oldPassword,password)) {
                	changed = true;
                }
            } finally {
                lock.unlock();
            }
        }
        public String getDriver() { return driver; }
        public String getDatabase() { return database; }
        public String getUrl() { return database; }
        public String getUsername() { return username; }
        public String getPassword() { return password; }
		public boolean isAutoCommit() {
			return autoCommit;
		}

		public void checkDriver() throws SQLException {
            if(!driverLoaded && driver != null && driver.trim().length() > 0) {
                try {
					Class.forName(driver.trim());
				} catch(ClassNotFoundException e) {
					SQLException sqle = new SQLException("Could not load DriverManager '" + driver.trim() + "'");
					sqle.initCause(e);
					throw sqle;
				}
                driverLoaded = true;
            }
        }

        @Override
		public String toString() {
            return PROP_DRIVER + "=" + driver + "; "
                + PROP_DATABASE + "=" + database + "; "
                + PROP_USERNAME + "=" + username;
        }
		protected Connection createConnection(Queue<LocalConnection> pool) throws SQLException {
			log.info("Creating new Connection");
		    try {
				lock.lockInterruptibly();
			} catch(InterruptedException e) {
				throw new SQLException(e);
			}
		    Connection conn;
	        try {
		    	checkDriver();
		    	conn = java.sql.DriverManager.getConnection(getDatabase(), connectionProperties);
		        conn.setAutoCommit(isAutoCommit());
		    } finally {
		        lock.unlock();
		    }
		    return trackStatements ? new LocalStatementTrackingConnection(conn, pool) : new LocalConnection(conn, pool);
		}
        public Connection getConnection() throws SQLException {
        	Queue<LocalConnection> connList = localConnList.get();
        	while(!connList.isEmpty()) {
        		LocalConnection conn = connList.poll();
        		if(!conn.isClosed() && !isExpired(conn) && (!isTestOnBorrow() || isValid(conn)))
        			return conn;
        		else
        			try {
        				conn.reallyClose();
        			} catch(SQLException e) {
        			}
        	}
        	return createConnection(connList);
        }
        //XXX: This doesn't really work b/c the thread that calls close will probably be different than the threads using the connections
		public void close() {
			for(Iterator<LocalConnection> iter = localConnList.get().iterator(); iter.hasNext(); ) {
				LocalConnection localConnection = iter.next();
				try {
					localConnection.delegate.close();
				} catch(SQLException e) {
					//ignore
				}
				iter.remove();
			}
		}
        /**
		 * @param conn
		 * @return
		 */
		protected boolean isExpired(LocalConnection conn) {
			if(maxAge > 0 && conn.createTime < System.currentTimeMillis() - maxAge) {
				if(log.isInfoEnabled())
					log.info("Expiring connection " + conn + " created at " + new Date(conn.createTime));
				return true;
			}
			return false;
		}

		@Override
		public boolean isAvailable() {
			return true;
		}
		/**
		 * @param conn
		 * @return
		 */
		protected boolean isValid(LocalConnection conn) {
			try {
				Statement st = conn.createStatement();
				try {
					boolean hasResults = st.execute(getValidationQuery());
					if(hasResults) {
						ResultSet rs = st.getResultSet();
						try {
							return rs.next();
						} finally {
							rs.close();
						}
					} else {
						return true;
					}
				} finally {
					st.close();
				}
			} catch(SQLException e) {
				log.debug("Connection is not valid because " + e.getMessage());
				return false;
			}
		}

		public Connection getConnection(String user, String pwd) throws SQLException {
			Connection conn = java.sql.DriverManager.getConnection(getDatabase(),user,pwd);
        	conn.setAutoCommit(isAutoCommit());
            return conn;
        }
        public PrintWriter getLogWriter() throws SQLException { return null; }
        public void setLogWriter(PrintWriter out) throws SQLException {}
        public void setLoginTimeout(int seconds) throws SQLException {}
        public int getLoginTimeout() throws SQLException { return 0; }
        @Override
		public void finalize() {
            //don't do this or gc may close our connection while we are using it!
            //try { if(conn != null) conn.close(); } catch(SQLException e) {}
        }
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return false;
		}
		public <T> T unwrap(Class<T> iface) throws SQLException {
			throw new SQLException("Not supported");
		}

		public boolean isTestOnBorrow() {
			return testOnBorrow;
		}

		public void setTestOnBorrow(boolean testOnBorrow) {
			this.testOnBorrow = testOnBorrow;
		}

		public String getValidationQuery() {
			return validationQuery;
		}

		public void setValidationQuery(String validationQuery) {
			if(validationQuery == null || (validationQuery=validationQuery.trim()).length() == 0) {
				setTestOnBorrow(false);
			}
			this.validationQuery = validationQuery;
		}

		public void setUsername(String username) {
			this.username = username;
			if(username != null && username.trim().length() > 0)
	    		connectionProperties.put("user", username);
			else
				connectionProperties.remove("user");
		}

		public void setPassword(String password) {
			this.password = password;
			if(password != null && password.trim().length() > 0)
	    		connectionProperties.put("password", password);
			else
				connectionProperties.remove("password");
		}

		public Logger getParentLogger() throws SQLFeatureNotSupportedException {
			return Logger.getLogger(getClass().getPackage().getName());
		}
    }
}
