package simple.db;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import simple.io.Log;

public class StatementMonitor {
	private static final Log log = Log.getLog();
	protected static final Object PRESENT = new Object();
	protected static final ConcurrentMap<WeakReference<Thread>, Set<Statement>> runningStatements = new ConcurrentHashMap<>();
	protected static final ReferenceQueue<Thread> queue = new ReferenceQueue<>();
	protected static final ThreadLocal<Set<Statement>> localStatements = new ThreadLocal<Set<Statement>>() {
		protected Set<Statement> initialValue() {
			Set<Statement> ss = new ConcurrentHashMap<Statement, Object>(8, 0.75f, 1).keySet(PRESENT);
			runningStatements.put(new WeakReference<Thread>(Thread.currentThread(), queue), ss);
			Reference<? extends Thread> old;
			while((old = queue.poll()) != null)
				runningStatements.remove(old);
			return ss;
		}
	};
	public static void registerStatement(Statement statement) {
		localStatements.get().add(statement);
	}

	public static void deregisterStatement(Statement statement) {
		localStatements.get().remove(statement);
	}

	public static int cancelStatements(Thread thread) {
		WeakReference<Thread> ref = new WeakReference<Thread>(thread);
		Set<Statement> ss = runningStatements.get(ref);
		if(ss == null)
			return 0;
		// ss is thread-safe
		int cnt = 0;
		for(Statement statement : ss) {
			try {
				statement.cancel();
				if(log.isInfoEnabled())
					log.info("Cancelled sql statement " + statement);
				cnt++;
			} catch(SQLException e) {
				if(log.isDebugEnabled())
					log.debug("Could not cancel sql statement " + statement, e);
			}
		}
		return cnt;
	}
}
