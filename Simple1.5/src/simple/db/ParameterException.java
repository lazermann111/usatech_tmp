/*
 * CallException.java
 *
 * Created on March 4, 2003, 9:41 AM
 */

package simple.db;

/** An exception indicating that a paramater failed to be extracted from or saved into
 * the bean passed to one of the execute methods of <CODE>simple.db.Call</CODE>
 * @author Brian S. Krug
 */
public class ParameterException extends DataLayerException {
    private static final long serialVersionUID = -8459852855L;
    public static enum ParameterExceptionType { MISSING, CONVERSION, OTHER};
    protected int parameterIndex;
    protected String parameterName;
    protected ParameterExceptionType type;

	/**
     * Creates a new instance of <code>ParameterException</code> without detail message.
     */
    public ParameterException() {
    	this.type = ParameterExceptionType.OTHER;
    }
    
    
    /**
     * Constructs an instance of <code>ParameterException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public ParameterException(String msg) {
        super(msg);
    	this.type = ParameterExceptionType.OTHER;
    }
    
        
    /** Creates a new instance of <code>ParameterException</code> without detail message but with a cause.
     * @param cause The cause of the exception
     */
    public ParameterException(Throwable cause) {
        super(cause);
    	this.type = ParameterExceptionType.OTHER;
    }
    
    
    /** Constructs an instance of <code>ParameterException</code> with the specified detail message and cause.
     * @param cause The cause of the exception
     * @param msg the detail message.
     */
    public ParameterException(String msg, Throwable cause) {
        super(msg, cause);
    	this.type = ParameterExceptionType.OTHER;
    }


	public int getParameterIndex() {
		return parameterIndex;
	}


	public void setParameterIndex(int parameterIndex) {
		this.parameterIndex = parameterIndex;
	}


	public String getParameterName() {
		return parameterName;
	}


	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}


	public ParameterExceptionType getType() {
		return type;
	}


	public void setType(ParameterExceptionType type) {
		this.type = type;
	}
}
