/*
 * DerivedColumn.java
 *
 * Created on February 26, 2003, 12:44 PM
 */

package simple.db;


/**
 * An extension of the Column class that concatenates the values of other Columns
 * and literal Strings together to generate this column's value.
 *
 * @author  Brian S. Krug
 */
public class DerivedColumn extends Column {   
    /** Holds value of property parts. */
    private Object[] parts;
    
    /** Holds value of property formats. */
    private String[] formats;
    
    /** Creates a new instance of DerivedColumn */
    public DerivedColumn() {
    }
        
    /** Getter for property parts.
     * @return Value of property parts.
     */
    public Object[] getParts() {
        return this.parts;
    }
    
    /** Setter for property parts.
     * @param parts New value of property parts.
     */
    public void setParts(Object[] parts) {
        this.parts = parts;
    }
    
    /** Getter for property formats.
     * @return Value of property formats.
     */
    public String[] getFormats() {
        return this.formats;
    }
    
    /** Setter for property formats.
     * @param formats New value of property formats.
     */
    public void setFormats(String[] formats) {
        this.formats = formats;
    }
    
}
