/*
 * Argument.java
 *
 * Created on December 30, 2002, 11:40 AM
 */

package simple.db;

import simple.sql.SQLType;


/**
 * Indicates a database argument in a Call
 * @author  Brian S. Krug
 * @version 
 */
public interface Argument extends Cloneable {
    /** If this is an IN argument.
     * @return Value of property in.
     */
    public boolean isIn() ;
    
    /** If this is an OUT argument.
     * @return Value of property out.
     */
    public boolean isOut() ;
    
    public SQLType getSqlType() ;
    
    /** Getter for property propertyName.
     * @return Value of property propertyName.
     *
     */
    public String getPropertyName();

	public Class<? extends Object> getJavaType();
    
	public Argument clone();
}

