/*
 * CallSource.java
 *
 * Created on January 20, 2004, 3:28 PM
 */

package simple.db;

/** Interface that DataLayer references to retrieve Call objects
 *
 * @author  Brian S. Krug
 */
public interface CallSource {
    /** Returns a Call object for the specified id value
     *  @param id The unique id for the Call
     *  @returns The requested Call object
     *  @throws CallNotFoundException If a Call can not be found for the specified id
     **/
    public Call getCall(String id) throws CallNotFoundException ; 
    /** Returns a Set of all the id values for which a Call object is defined
     *  @returns The Set of all id values
     **/
     public java.util.Set<String> getCallIds() ;
     
}
