package simple.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import simple.io.Log;
import simple.util.SimpleLinkedHashMap;

public class PlainConnectionGroup implements ConnectionGroup {
	private static final Log log = Log.getLog();
	protected final SimpleLinkedHashMap<String, Connection> connections = new SimpleLinkedHashMap<String, Connection>();
	protected long connectingTime;
	protected final DataSourceFactory dataSourceFactory;
	
	public PlainConnectionGroup(DataSourceFactory dataSourceFactory) {
		this.dataSourceFactory = dataSourceFactory;
	}
	
	public Connection getConnection(String dataSourceName) throws SQLException, DataLayerException {
		return getConnection(dataSourceName, null);
	}
	
	public Connection getConnection(String dataSourceName, Boolean autoCommit) throws SQLException, DataLayerException {
		if(dataSourceName != null)
			dataSourceName = dataSourceName.toUpperCase();
		Connection conn = connections.get(dataSourceName);
		if(conn == null) {
			long start;
			if(log.isInfoEnabled())
				start = System.currentTimeMillis();
			else
				start = 0;
			conn = dataSourceFactory.getDataSource(dataSourceName).getConnection();
			if(log.isInfoEnabled()) {
				connectingTime += (System.currentTimeMillis() - start);
			}
			connections.put(dataSourceName, conn);
		}
		if(autoCommit != null)
			conn.setAutoCommit(autoCommit);
		return conn;
	}

	public Connection refreshConnection(Connection conn) throws SQLException, DataLayerException {
		return refreshConnection(conn, null);
	}

	public Connection refreshConnection(Connection conn, Boolean autoCommit) throws SQLException, DataLayerException {
		for(Map.Entry<String, Connection> e : connections.entrySet()) {
			if(conn == e.getValue()) {
				long start;
				if(log.isInfoEnabled())
					start = System.currentTimeMillis();
				else
					start = 0;
				Connection newConn = dataSourceFactory.getDataSource(e.getKey()).getConnection();
				if(log.isInfoEnabled())
					connectingTime += (System.currentTimeMillis() - start);
				e.setValue(newConn);
				try {
					conn.close();
				} catch(SQLException ex) {
					// ignore
				}
				if(autoCommit != null)
					newConn.setAutoCommit(autoCommit);

				return newConn;
			}
		}
		throw new DataLayerException("The specified connection is not in this ConnectionGroup");
	}

	public void commit() throws SQLException {
		ListIterator<Connection> iter = connections.getOrderedValues().listIterator(connections.size());
		while(iter.hasPrevious()) {
			Connection conn = iter.previous();
			if(!conn.getAutoCommit())
				conn.commit();
		}
	}
	
	public void close(boolean rollback) {
		if(rollback) {
			ListIterator<Connection> iter = connections.getOrderedValues().listIterator(connections.size());
			while(iter.hasPrevious())
				try {
					Connection conn = iter.previous();
					if(!conn.getAutoCommit())
						conn.rollback();
				} catch(SQLException e) {
					// ignore
				} catch(RuntimeException e) {
					//ignore
				} catch(Error e) {
					//ignore
				}
		}
		ListIterator<Connection> iter = connections.getOrderedValues().listIterator(connections.size());
		while(iter.hasPrevious())
			try {
				iter.previous().close();
			} catch(SQLException e) {
				// ignore
			} catch(RuntimeException e) {
				//ignore
			} catch(Error e) {
				//ignore
			}
		connectingTime = 0;
		connections.clear();
	}
	
	public long getConnectingTime() {
		return connectingTime;
	}

	public String getDataSourceName(int index) throws SQLException, DataLayerException {
		int i = 0;
		for(String dsn : connections.keySet()) {
			if(i++ >= index)
				return dsn;
		}
		throw new NoSuchElementException("Could not get dataSourceName at index " + index);
	}

	public int getOpenedCount() {
		return connections.size();
	}

	public void setPreferred(Iterable<String> preferred) {
		// Do nothing
	}

	public Iterable<String> getPreferred() {
		return Collections.emptySet();
	}
	
	@Override
	public List<String> getPreferredDataSourceNames(int num) throws SQLException, DataLayerException, NoSuchElementException {
		List<String> list = new ArrayList<>(num);
		Iterator<String> iter = connections.keySet().iterator();
		while(iter.hasNext() && list.size() < num)
			list.add(iter.next());
		return list;
	}

}
