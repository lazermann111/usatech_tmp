/*
 * Created on Nov 10, 2005
 *
 */
package simple.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;


/** Allows disabling and enabling.
 * @author Brian S. Krug
 *
 */
public abstract class AbstractDataSource implements ExtendedDataSource {
	protected final AtomicBoolean disabled = new AtomicBoolean();
	
	public boolean isDisabled() {
		return disabled.get();
	}

	public void disable() throws InterruptedException {
		disabled.set(true);
		waitForConnectionsToClose();
	}
	
	public void disableImmediate() throws InterruptedException {
		if(disabled.compareAndSet(false, true)) {
			closeConnections();
		} else {
			waitForConnectionsToClose();
		}
	}
	  
	public void enable() {
		disabled.set(false);
	}
	
	protected void checkDisabled() throws SQLException {
		if(isDisabled())
			throw new SQLException("Data Source is disabled", "08000");
	}
	
	protected void waitForConnectionsToClose() throws InterruptedException {
		// For now we just close active connections and don't worry about inactive connections that may still be open
		while(getNumActive() > 0) {
			Thread.sleep(100);
		}
	}
	protected abstract void closeConnections() ;
	protected abstract Connection getConnectionInternal() throws SQLException ;
	protected abstract Connection getConnectionInternal(String username, String password) throws SQLException ;
	protected abstract int getNumActive() ;
	/**
	 * @see javax.sql.DataSource#getConnection()
	 */
	public Connection getConnection() throws SQLException {
		checkDisabled();
		return getConnectionInternal();
	}

	/**
	 * @see javax.sql.DataSource#getConnection(java.lang.String, java.lang.String)
	 */
	public Connection getConnection(String username, String password) throws SQLException {
		checkDisabled();
		return getConnectionInternal(username, password);
	}

	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return Logger.getLogger(getClass().getPackage().getName());
	}
}
