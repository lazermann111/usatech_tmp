package simple.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import simple.io.Log;
import simple.util.SimpleLinkedHashMap;

public class DefaultConnectionGroup implements ConnectionGroup {
	private static final Log log = Log.getLog();
	protected final SimpleLinkedHashMap<String, Connection> connections = new SimpleLinkedHashMap<String, Connection>();
	protected long connectingTime;
	protected final Set<String> dataSourceNames;
	protected final DataSourceAvailability dataSourceAvailability;
	protected final DataLayer dataLayer;
	//protected Iterable<String> preferred;
	protected List<String> preferred = new ArrayList<>();
	protected AtomicBoolean initFlag = new AtomicBoolean(true);
	protected ReentrantLock preferredLock = new ReentrantLock();

	public DefaultConnectionGroup(Set<String> dataSourceNames, DataSourceAvailability dataSourceAvailability, DataLayer dataLayer) {
		this.dataSourceNames = dataSourceNames;
		this.dataSourceAvailability = dataSourceAvailability;
		this.dataLayer = dataLayer;
	}

	protected void initialize(int size) throws DataLayerException, NoSuchElementException {
		boolean init = initFlag.get();
		if(connections.size() < size || (init && initFlag.compareAndSet(init, false))) {
			if(size > dataSourceNames.size())
				throw new NoSuchElementException("DataSourceNames only has " + dataSourceNames.size() + " entries and " + size + " were requested");
			final Set<String> untried = new LinkedHashSet<String>();
			if(preferred != null) {
				for(String dsn : preferred) {
					if(dsn != null)
						dsn = dsn.toUpperCase();
					if(!connections.containsKey(dsn)) {
						if(dataSourceAvailability.isAvailable(dsn)) {
							try {
								getConnection(dsn);
								if(connections.size() >= size) {
									if(log.isDebugEnabled())
										log.debug("Obtained " + size + " datasources " + connections.keySet().toString());
									return;
								}
							} catch(SQLException e) {
								log.warn("Could not get connection for '" + dsn + "'; marking as unavailable", e);
							} catch(DataLayerException e) {
								log.warn("Could not get connection for '" + dsn + "'; marking as unavailable", e);
							}
						} else {
							untried.add(dsn);// try after all that are in availability set
						}
					}
				}
			}
			for(String dsn : dataSourceNames) {
				if(dsn != null)
					dsn = dsn.toUpperCase();
				if(!connections.containsKey(dsn)) {
					if(dataSourceAvailability.isAvailable(dsn)) {
						try {
							getConnection(dsn);
							if(connections.size() >= size){
								if(log.isDebugEnabled())
									log.debug("Obtained " + size + " datasources " + connections.keySet().toString());
								return;
							}
						} catch(SQLException e) {
							log.warn("Could not get connection for '" + dsn + "'; marking as unavailable", e);
						} catch(DataLayerException e) {
							log.warn("Could not get connection for '" + dsn + "'; marking as unavailable", e);
						}
					} else {
						untried.add(dsn);//try after all that are in availability set
					}
				}
			}
			for(String dsn : untried) {
				if(!connections.containsKey(dsn)) {
					try {
						getConnection(dsn);
						if(connections.size() >= size) {
							if(log.isDebugEnabled())
								log.debug("Obtained " + size + " datasources " + connections.keySet().toString());
							return;
						}
					} catch(SQLException e) {
						log.warn("Could not get connection for '" + dsn + "'; marking as unavailable", e);
					} catch(DataLayerException e) {
						log.warn("Could not get connection for '" + dsn + "'; marking as unavailable", e);
					}
				}
			}
			
			if (connections.size() < size)
				throw new DataLayerException("Not enough data sources are available. Need " + size + "; Obtained " + connections.size() + " " + connections.keySet().toString());
		}
	}
	
	public Connection getConnection(String dataSourceName) throws SQLException, DataLayerException {
		return getConnection(dataSourceName, null);
	}

	public Connection getConnection(String dataSourceName, Boolean autoCommit) throws SQLException, DataLayerException {
		if(dataSourceName != null)
			dataSourceName = dataSourceName.toUpperCase();
		Connection conn = connections.get(dataSourceName);
		if(conn == null) {
			try {
				long start;
				if(log.isInfoEnabled())
					start = System.currentTimeMillis();
				else
					start = 0;
				conn = dataLayer.getConnection(dataSourceName);
				if(log.isInfoEnabled()) {
					connectingTime += (System.currentTimeMillis() - start);
				}
				connections.put(dataSourceName, conn);
				dataSourceAvailability.setAvailable(dataSourceName, true);
			} catch(SQLException e) {
				dataSourceAvailability.setAvailable(dataSourceName, false);
				throw e;
			} catch (DataLayerException e) {
				dataSourceAvailability.setAvailable(dataSourceName, false);
				throw e;
			}
		}
		if(autoCommit != null)
			conn.setAutoCommit(autoCommit);
		return conn;
	}

	public Connection refreshConnection(Connection conn) throws SQLException, DataLayerException {
		return refreshConnection(conn, null);
	}

	public Connection refreshConnection(Connection conn, Boolean autoCommit) throws SQLException, DataLayerException {
		for(Map.Entry<String, Connection> e : connections.entrySet()) {
			if(conn == e.getValue()) {
				Connection newConn;
				try {
					long start;
					if(log.isInfoEnabled())
						start = System.currentTimeMillis();
					else
						start = 0;
					newConn = dataLayer.getConnection(e.getKey());
					if(log.isInfoEnabled())
						connectingTime += (System.currentTimeMillis() - start);
					e.setValue(newConn);
					dataSourceAvailability.setAvailable(e.getKey(), true);
				} catch(SQLException | DataLayerException ex) {
					dataSourceAvailability.setAvailable(e.getKey(), false);
					throw ex;
				}
				try {
					conn.close();
				} catch(SQLException ex) {
					// ignore
				}
				if(autoCommit != null)
					newConn.setAutoCommit(autoCommit);

				return newConn;
			}
		}
		throw new DataLayerException("The specified connection is not in this ConnectionGroup");
	}

	public void commit() throws SQLException {
		ListIterator<Connection> iter = connections.getOrderedValues().listIterator(connections.size());
		while(iter.hasPrevious()) {
			Connection conn = iter.previous();
			if(!conn.getAutoCommit())
				conn.commit();
		}
	}
	
	public void close(boolean rollback) {
		if(rollback) {
			ListIterator<Connection> iter = connections.getOrderedValues().listIterator(connections.size());
			while(iter.hasPrevious())
				try {
					Connection conn = iter.previous();
					if(!conn.getAutoCommit())
						conn.rollback();
				} catch(SQLException e) {
					// ignore
				} catch(RuntimeException e) {
					//ignore
				} catch(Error e) {
					//ignore
				}
		}
		ListIterator<Connection> iter = connections.getOrderedValues().listIterator(connections.size());
		while(iter.hasPrevious())
			try {
				iter.previous().close();
			} catch(SQLException e) {
				// ignore
			} catch(RuntimeException e) {
				//ignore
			} catch(Error e) {
				//ignore
			}
		connectingTime = 0;
		connections.clear();
	}
	
	public long getConnectingTime() {
		return connectingTime;
	}

	public String getDataSourceName(int index) throws SQLException, DataLayerException {
		initialize(index+1);
		int i = 0;
		for(String dsn : connections.keySet()) {
			if(i++ >= index)
				return dsn;
		}
		throw new NoSuchElementException("Could not get dataSourceName at index " + index);
	}

	public int getOpenedCount() {
		return connections.size();
	}

	public void setPreferred(Iterable<String> preferredIter) {
		List<String> tempPreferred = new ArrayList<>();
		if (preferredIter != null)
			preferredIter.forEach(s -> tempPreferred.add(s.toUpperCase()));
		
		preferredLock.lock();
		try {
			if (!tempPreferred.equals(preferred)) {
				if(log.isDebugEnabled())
					log.debug("Preferred dataSources changed from {0} to {1}", this.preferred, tempPreferred);
				this.preferred = tempPreferred;
				initFlag.set(true);
			}
		} finally {
			preferredLock.unlock();
		}
	}

	public Iterable<String> getPreferred() {
		return preferred;
	}
	
	@Override
	public List<String> getPreferredDataSourceNames(int num) throws SQLException, DataLayerException, NoSuchElementException {
		initialize(num);
		List<String> list = new ArrayList<>(num);
		if (preferred != null) {
			Iterator<String> prefIter = preferred.iterator();
			while(prefIter.hasNext() && list.size() < num)
				list.add(prefIter.next());
		}
		if (list.size() < num && !connections.isEmpty()) {
			Iterator<String> connIter = connections.keySet().iterator();
			while(connIter.hasNext() && list.size() < num) {
				String dsn = connIter.next();
				if (!list.contains(dsn))
					list.add(dsn);
			}
		}
		return list;
	}
}
