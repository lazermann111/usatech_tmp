/*
 * CallTag.java
 *
 * Created on December 30, 2002, 5:20 PM
 */

package simple.db.config;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class CursorTag implements ResultSetTag {
    private int index;
    private List<ColumnTag> columns = new LinkedList<ColumnTag>();
    private List<DerivedTag> deriveds = new LinkedList<DerivedTag>();
    private boolean allColumns;
    
    /** Holds value of property property.  */
    private String property;
    
    /** Holds value of property groups. */
    private String groups;
    
    /** Holds value of property totals. */
    private String totals;
    
    /** Holds value of property fetchSize.  */
    private int fetchSize;
    
    /** Holds value of property lazyAccess.  */
    private boolean lazyAccess;
    
    /** Creates new CallTag */
    public CursorTag() {
    }

    public void addColumn(ColumnTag ct) {
        columns.add(ct);
    }
    
    public void add(ColumnTag ct) {
        columns.add(ct);
    }
        
    public List<ColumnTag> getColumns() {
        return columns;
    }
    
    public List<DerivedTag> getDeriveds() {
        return deriveds;
    }
    
    public void addDerived(DerivedTag dt) {
        deriveds.add(dt);
    }
    
    public void add(DerivedTag dt) {
        deriveds.add(dt);
    }
    
    /** Getter for property index.
     * @return Value of property index.
     */
    public int getIndex() {
        return index;
    }
    
    /** Setter for property index.
     * @param index New value of property index.
     */
    public void setIndex(int index) {
        this.index = index;
    }
    
    /** Getter for property allColumns.
     * @return Value of property allColumns.
     */
    public boolean isAllColumns() {
        return allColumns;
    }
    
    /** Setter for property allColumns.
     * @param allColumns New value of property allColumns.
     */
    public void setAllColumns(boolean allColumns) {
        this.allColumns = allColumns;
    }
    
    /** Getter for property property.
     * @return Value of property property.
     *
     */
    public String getProperty() {
        return property;
    }
    
    /** Setter for property property.
     * @param property New value of property property.
     *
     */
    public void setProperty(String property) {
        this.property = property;
    }
    
    /** Getter for property groups.
     * @return Value of property groups.
     *
     */
    public String getGroups() {
        return this.groups;
    }
    
    /** Setter for property groups.
     * @param groups New value of property groups.
     *
     */
    public void setGroups(String groups) {
        this.groups = groups;
    }
    
    /** Getter for property fetchSize.
     * @return Value of property fetchSize.
     *
     *
     */
    public int getFetchSize() {
        return this.fetchSize;
    }
    
    /** Getter for property lazyAccess.
     * @return Value of property lazyAccess.
     *
     *
     */
    public boolean isLazyAccess() {
        return this.lazyAccess;
    }
    
    /** Setter for property lazyAccess.
     * @param lazyAccess New value of property lazyAccess.
     *
     *
     */
    public void setLazyAccess(boolean lazyAccess) {
        this.lazyAccess = lazyAccess;
    }
    
    /** Setter for property fetchSize.
     * @param fetchSize New value of property fetchSize.
     *
     *
     */
    public void setFetchSize(int fetchSize) {
        this.fetchSize = fetchSize;
    }

    public String getTotals() {
        return totals;
    }

    public void setTotals(String totals) {
        this.totals = totals;
    }
    
}
