/*
 * CallTag.java
 *
 * Created on December 30, 2002, 5:20 PM
 */

package simple.db.config;

import java.util.List;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public interface ResultSetTag {
    public void addColumn(ColumnTag ct) ;
    public List<ColumnTag> getColumns() ;
    public void addDerived(DerivedTag dt) ;
    public List<DerivedTag> getDeriveds() ;
    public boolean isAllColumns() ;
    
    /** Getter for property property.
     * @return Value of property property.
     *
     */
    public String getProperty();
    
    /** Setter for property property.
     * @param property New value of property property.
     *
     */
    public void setProperty(String property);
    
    /** Getter for property groups.
     * @return Value of property groups.
     *
     */
    public String getGroups();
    
    /** Setter for property groups.
     * @param groups New value of property groups.
     *
     */
    public void setGroups(String groups);
    
    /** Getter for property totals.
     * @return Value of property totals.
     *
     */
    public String getTotals();
    
    /** Setter for property totals.
     * @param groups New value of property totals.
     *
     */
    public void setTotals(String totals);
    
    /** Getter for property fetchSize.
     * @return Value of property fetchSize.
     *
     */
    public int getFetchSize();
    
    /** Setter for property fetchSize.
     * @param fetchSize New value of property fetchSize.
     *
     */
    public void setFetchSize(int fetchSize);
    
    /** Getter for property laxyAccess.
     * @return Value of property laxyAccess.
     *
     */
    public boolean isLazyAccess();
    
    /** Setter for property laxyAccess.
     * @param laxyAccess New value of property laxyAccess.
     *
     */
    public void setLazyAccess(boolean laxyAccess);
    
}
