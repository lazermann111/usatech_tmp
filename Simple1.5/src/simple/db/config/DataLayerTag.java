/*
 * Created on Jul 19, 2005
 *
 */
package simple.db.config;

import java.util.ArrayList;
import java.util.List;

public class DataLayerTag {
    private String callClass;
    private String dataSource;
    protected List<CallTag> callTags = new ArrayList<CallTag>();
    public DataLayerTag() {
        super();
    }

    public void add(CallTag callTag) {
        callTags.add(callTag);
    }
    
    public List<CallTag> getCallTags() {
        return callTags;
    }

    /**
     * @return Returns the callClass.
     */
    public String getCallClass() {
        return callClass;
    }

    /**
     * @param callClass The callClass to set.
     */
    public void setCallClass(String callClass) {
        this.callClass = callClass;
    }

    /**
     * @return Returns the dataSource.
     */
    public String getDataSource() {
        return dataSource;
    }

    /**
     * @param dataSource The dataSource to set.
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }
}
