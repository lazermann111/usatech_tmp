/*
 * TextTag.java
 *
 * Created on February 26, 2003, 1:43 PM
 */

package simple.db.config;

/**
 *
 * @author  Brian S. Krug
 */
public class TextTag {
    
    /** Holds value of property value. */
    private String value;
    
    /** Creates a new instance of TextTag */
    public TextTag() {
    }
    
    /** Getter for property value.
     * @return Value of property value.
     */
    public String getValue() {
        return this.value;
    }
    
    /** Setter for property value.
     * @param value New value of property value.
     */
    public void setValue(String value) {
        this.value = value;
    }
    
}
