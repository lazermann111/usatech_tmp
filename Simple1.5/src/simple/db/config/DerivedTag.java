/*
 * DerivedTag.java
 *
 * Created on February 26, 2003, 1:46 PM
 */

package simple.db.config;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author  Brian S. Krug
 */
public class DerivedTag {
    
    /** Holds value of property parts. */
    private List<Object> parts = new LinkedList<Object>();
    
    /** Holds value of property property. */
    private String property;
    
    /** Creates a new instance of DerivedTag */
    public DerivedTag() {
    }
    
    public void addPart(TextTag tt) {
        parts.add(tt);
    }
    public void add(TextTag tt) {
        parts.add(tt);
    }
    public void addPart(PropertyTag pt) {
        parts.add(pt);
    }
    public void add(PropertyTag pt) {
        parts.add(pt);
    }    
    /** Getter for property parts.
     * @return Value of property parts.
     */
    public List<Object> getParts() {
        return this.parts;
    }
    
    /** Getter for property property.
     * @return Value of property property.
     */
    public String getProperty() {
        return this.property;
    }
    
    /** Setter for property property.
     * @param property New value of property property.
     */
    public void setProperty(String property) {
        this.property = property;
    }
    
}
