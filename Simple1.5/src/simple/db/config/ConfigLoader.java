/*
 * ConfigLoader.java
 *
 * Created on December 30, 2002, 5:18 PM
 */

package simple.db.config;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ReflectionUtils;
import simple.db.Argument;
import simple.db.BasicCallSource;
import simple.db.Call;
import simple.db.CallSource;
import simple.db.Column;
import simple.db.ConnectionHolder;
import simple.db.ConvertedColumn;
import simple.db.Cursor;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceConnectionHolder;
import simple.db.DataSourceNotFoundException;
import simple.db.DerivedColumn;
import simple.db.Parameter;
import simple.db.RootCallSource;
import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.sql.SQLTypeUtils;
import simple.text.StringUtils;
import simple.util.LazyConversionMap;
import simple.util.StaticCollection;
import simple.util.concurrent.RunOnGetFuture;
import simple.xml.BeanXMLLoader;
import simple.xml.ObjectBuilder;

/**
 * This class parses a file following the DTD specified in 'data-layer.dtd' and
 * adds any calls or queries contained therein to the DataLayer's cache. This
 * is the prefered way of configuring a DataLayer. See 'data-layer.dtd' for
 * details on the xml entities and attributes and their meanings.
 * @author Brian S. Krug
 */
public class ConfigLoader extends BasicCallSource implements RootCallSource.ReloadableCallSource, Loader {
    public static final long DEFUALT_MIN_INTERVAL = 15 * 1000; /* 15 seconds */
	/**
     * Holds the set of strings that indicate that a ResultSet has an updatable concurrency
     */
    public static final Collection<String> WRITABLE_SET = StaticCollection.create(
        "UPDATABLE", "WRITE", "WRITABLE","READWRITE","W");
        /**
         * Holds the set of strings that indicate that a ResultSet has an insentive scrollable resultset type
         */
    public static final Collection<String> SCROLLABLE_SET = StaticCollection.create(
        "YES", "TRUE", "SCROLLABLE");
        /**
         * Holds the set of strings that indicate that a ResultSet has a sentive scrollable resultset type
         */
    public static final Collection<String> SCROLLABLE_SENSITIVE_SET = StaticCollection.create(
        "SERVER", "SERVERSCROLL", "SENSITIVE","SERVERSCROLLABLE");
    private static final simple.io.Log log = simple.io.Log.getLog();

    /**
     * Holds the datalayer that is being configured
     */
    protected DataLayer dataLayer;
    /**
     * Holds the source of the configuation information
     */
    protected ConfigSource configSource;

    protected ReentrantLock lock = new ReentrantLock();

    public static enum ErrorResponse { IGNORE_ERRORS, SKIP_AND_REPORT_ERRORS, ABORT_ON_ERROR };
    protected ErrorResponse errorResponse = ErrorResponse.SKIP_AND_REPORT_ERRORS;
    protected ConfigException configException;
    protected final Map<String,Future<Call>> futureCallMap = new HashMap<String, Future<Call>>();
    /**
     * Creates new ConfigLoader
     * @param dataLayer The datalayer to configure
     * @param configSource The source of the configuration information
     */
    protected ConfigLoader(DataLayer dataLayer, ConfigSource configSource) {
		super(null);
    	this.dataLayer = dataLayer;
        this.configSource = configSource;
        this.callMap = new LazyConversionMap<String, Call>(futureCallMap, true);
    }


    /**
     * Adds a new call to the DataLayer's cache using the attributes of the
     *  given CallTag object. This method is called by the XML Digester.
     * @param ct The CallTag that holds the configuration information for the new Call
     * @throws ConfigException If configuration information is invalid
     */
    public void addCall(CallTag ct) throws ConfigException {
        Call c;
        if(ct.getCallClass() != null) {
            try {
                c = ReflectionUtils.createObject(ct.getCallClass(), Call.class);
            } catch(ClassCastException e) {
                log.warn("Could not instantiate custom call class '" + ct.getCallClass() + "'", e);
                c = new Call();
            } catch(InstantiationException e) {
                log.warn("Could not instantiate custom call class '" + ct.getCallClass() + "'", e);
                c = new Call();
            } catch (IllegalAccessException e) {
                log.warn("Could not instantiate custom call class '" + ct.getCallClass() + "'", e);
                c = new Call();
            } catch (ClassNotFoundException e) {
                log.warn("Could not instantiate custom call class '" + ct.getCallClass() + "'", e);
                c = new Call();
            } catch(IllegalArgumentException e) {
                log.warn("Could not instantiate custom call class '" + ct.getCallClass() + "'", e);
                c = new Call();
			} catch(InvocationTargetException e) {
                log.warn("Could not instantiate custom call class '" + ct.getCallClass() + "'", e);
                c = new Call();
			}
        } else {
            c = new Call();
        }
        c.setSql(ct.getSql());
        c.setDataSourceName(ct.getDataSource());
        if(ct.getConcurrency() != null && WRITABLE_SET.contains(ct.getConcurrency().toUpperCase())) c.setResultSetConcurrency(ResultSet.CONCUR_UPDATABLE);
        if(ct.getScrollable() != null && SCROLLABLE_SET.contains(ct.getScrollable().toUpperCase())) c.setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE);
        else if(ct.getScrollable() != null && SCROLLABLE_SENSITIVE_SET.contains(ct.getScrollable().toUpperCase())) c.setResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);

        //Get Arguments, sort them by index, and process them
        List<?> argList = ct.getArguments();
        argList = sortArguments(argList);
        Argument[] args = new Argument[argList.size() + 1];
        Iterator<?> iter = argList.iterator();
        ConnectionHolder ch = new DataSourceConnectionHolder(dataLayer, c.getDataSourceName());
        try {
        	for(int i = 1; iter.hasNext(); i++) {
	            Object o = iter.next();
	            if(o instanceof ResultSetTag) args[i] = processResultSet((ResultSetTag)o);
	            else if(o instanceof ParameterTag) args[i] = processParameter((ParameterTag)o, ch);
	        }
	        if(ct instanceof ResultSetTag) args[0] = processResultSet((ResultSetTag)ct);
	        else {
	            Parameter p = new Parameter();
	            p.setIn(false);
	            p.setOut(true);
	            p.setPropertyName(ct.getProperty());
	            args[0] = p;
	        }
	    } finally {
	    	ch.closeConnection();
	    }

        c.setArguments(args);
        log.info("Adding Call '" + ct.getId() + "' with " + args.length + " arguments: " + StringUtils.toStringList(args));
        addCall(ct.getId(), c);
    }

    /**
     * Sorts a call's arguments based on their indices and defined order.
     * @return The sorted List
     * @param argList The original (unsorted) list of call arguments
     */
    protected List<Object> sortArguments(List<?> argList) {
        List<Object> sorted = new ArrayList<Object>(argList);
        Iterator<?> iter = argList.iterator();
		while(iter.hasNext()) {
            Object o = iter.next();
            int index = 0;
            if(o instanceof ParameterTag) index = ((ParameterTag)o).getIndex();
            else if(o instanceof CursorTag) index = ((CursorTag)o).getIndex();
            if(index > 0) {
                sorted.remove(o);
                sorted.add(index-1,o);
            }
        }
        return sorted;
    }

    /**
     * Creates a Cursor object with all its columns and other properties set
     *  according to the attributes of the given ResultSetTag.
     * @return The Cursor for the given ResultSetTag
     * @param rst The ResultSetTag containing the configuration information
     * @throws ConfigException If the configuration information is invalid
     */
    protected Cursor processResultSet(ResultSetTag rst) throws ConfigException {
        try {
            Cursor c = new Cursor();
            c.setAllColumns(rst.isAllColumns());
            c.setPropertyName(rst.getProperty());
            c.setFetchSize(rst.getFetchSize());
            c.setLazyAccess(rst.isLazyAccess());
            //boolean useBean = (rst.getBeanClass() != null && rst.getBeanClass().trim().length() > 0);
            //if(useBean) c.setBeanClass(Class.forName(rst.getBeanClass()));
            List<?> colList = rst.getColumns();
            List<?> derList = rst.getDeriveds();
            Map<String,Column> colMap =  new HashMap<String,Column>();
            Column[] cols = new Column[colList.size() + derList.size()];
            int i = 0;
            for(Iterator<?> iter = colList.iterator(); iter.hasNext(); i++) {
                ColumnTag ct = (ColumnTag)iter.next();
                String toClass;
                if(ct instanceof ConvertColumnTag)
                    toClass = ((ConvertColumnTag)ct).getTo();
                else
                	toClass = null;
                if(toClass != null && (toClass=toClass.trim()).length() > 0) {
                	ConvertedColumn cc = new ConvertedColumn();
                	try {
                		cc.setToClass(Class.forName(toClass));
                	} catch(ClassNotFoundException e) {
                    	throw new ConfigException(e);
            		}
                	cols[i] = cc;
                } else
                	cols[i] = new Column();
                if(ct.getColumn() == null || ct.getColumn().trim().length() == 0) {
                    cols[i].setIndex(i+1);
                } else {
                    try {
                        cols[i].setIndex(Integer.parseInt(ct.getColumn()));
                    } catch(NumberFormatException nfe) {
                        cols[i].setName(ct.getColumn());
                    }
                }
                cols[i].setPropertyName(ct.getProperty());
                cols[i].setFormat(ct.getFormat());
                colMap.put(cols[i].getPropertyName(), cols[i]);
                /*if(useBean) {
                    NestedNameTokenizer token = new NestedNameTokenizer(ct.getProperty());
                    Class clazz = c.getBeanClass();
                    List props = new ArrayList();
                    while(token.hasNext()) {
                        String prop = token.nextProperty();
                        PropertyDescriptor pd = ReflectionUtils.findPropertyDescriptor(clazz, prop);
                        props.add(pd);
                        clazz = pd.getPropertyType();
                    }
                    if(props.size() > 0) cols[i].setProperty((PropertyDescriptor[])props.toArray(new PropertyDescriptor[props.size()]));
                }
                // */
            }
            //now we can do the deriveds
            for(Iterator<?> iter = derList.iterator(); iter.hasNext(); i++) {
                DerivedTag dt = (DerivedTag)iter.next();
                DerivedColumn dc = new DerivedColumn();
                cols[i] = dc;
                List<?> partList = dt.getParts();
                Object[] parts = new Object[partList.size()];
                String[] formats = new String[partList.size()];
                Iterator<?> pIter = partList.iterator();
                for(int k = 0; k < parts.length; k++) {
                    Object p = pIter.next();
                    if(p instanceof TextTag) parts[k] = ((TextTag)p).getValue();
                    else if(p instanceof PropertyTag) {
                        parts[k] = colMap.get(((PropertyTag)p).getName());
                        formats[k] = ((PropertyTag)p).getFormat();
                    }
                }
                cols[i].setPropertyName(dt.getProperty());
                dc.setParts(parts);
                dc.setFormats(formats);
            }
            // read groups
            String[] tmp = simple.bean.ConvertUtils.convert(String[].class, rst.getGroups());
            if(tmp != null) {
                Object[] groups = new Object[tmp.length];
                for(int k = 0; k < tmp.length; k++) {
                    try {
                        groups[k] = new Integer(tmp[k].trim());
                    } catch(NumberFormatException nfe) {
                        groups[k] = tmp[k].trim();
                    }
                }
                c.setGroups(groups);
            }
            // read totals
            tmp = simple.bean.ConvertUtils.convert(String[].class, rst.getTotals());
            if(tmp != null) {
                Object[] totals = new Object[tmp.length];
                for(int k = 0; k < tmp.length; k++) {
                    try {
                        totals[k] = new Integer(tmp[k].trim());
                    } catch(NumberFormatException nfe) {
                        totals[k] = tmp[k].trim();
                    }
                }
                c.setTotals(totals);
            }
            c.setColumns(cols);
            return c;
        } catch(ConvertException e) {
            throw new ConfigException(e);
        }
    }

    /**
     * Creates a Parameter object with all its properties set
     *  according to the attributes of the given ParameterTag.
     * @return The Parameter object
     * @param pt The ParamaterTag object containing the configuration information
     * @throws ConfigException
     */
    protected Parameter processParameter(ParameterTag pt, ConnectionHolder ch) throws ConfigException {
        Parameter p = new Parameter();
        if("IN".equalsIgnoreCase(pt.getDirection())) {
            p.setIn(true);
            p.setOut(false);
        } else if("INOUT".equalsIgnoreCase(pt.getDirection())) {
            p.setIn(true);
            p.setOut(true);
        } else if("OUT".equalsIgnoreCase(pt.getDirection())) {
            p.setIn(false);
            p.setOut(true);
        }
        try {
			p.setSqlType(SQLTypeUtils.getSqlType(pt.getType(), pt.getTypeName(), ch));
		} catch(IllegalArgumentException e) {
			throw new ConfigException("Could not determine SQLType", e);
		} catch(SQLException e) {
			throw new ConfigException("Could not determine SQLType", e);
		} catch(DataLayerException e) {
			throw new ConfigException("Could not determine SQLType", e);
		}
        p.setPropertyName(pt.getProperty());
        p.setDefaultValue(pt.getDefaultValue());
        p.setRequired(pt.isRequired());
        return p;
    }

    /**
     * Parses the XML of the specified ConfigSource removing any calls previously
     * created by that ConfigSource and adding any calls or queries specified in
     * the ConfigSource's InputStream to the DataLayer's cache.
     * @param configSource The source of the configuration information
     * @throws ConfigException If the configuration information is invalid
     * @throws LoadingException If the config source cannot be read
     */
    public static void loadConfig(ConfigSource configSource) throws ConfigException, LoadingException {
        loadConfig(configSource, DataLayerMgr.getGlobalDataLayer());
    }

    /**
     * Parses the XML of the specified ConfigSource removing any calls previously
     * created by that ConfigSource and adding any calls or queries specified in
     * the ConfigSource's InputStream to the DataLayer's cache.
     * @param dataLayer The datalayer to configure
     * @param configSource The source of the configuration information
     * @throws ConfigException If the configuration information is invalid
     * @throws LoadingException If the config source cannot be read
     */
    public static void loadConfig(ConfigSource configSource, DataLayer dataLayer) throws ConfigException, LoadingException {
    	loadConfig(configSource, dataLayer, DEFUALT_MIN_INTERVAL);
    }
    /**
     * Parses the XML of the specified ConfigSource removing any calls previously
     * created by that ConfigSource and adding any calls or queries specified in
     * the ConfigSource's InputStream to the DataLayer's cache.
     * @param dataLayer The datalayer to configure
     * @param configSource The source of the configuration information
     * @throws ConfigException If the configuration information is invalid
     * @throws LoadingException If the config source cannot be read
     */
    public static void loadConfig(ConfigSource configSource, DataLayer dataLayer, long minInterval) throws ConfigException, LoadingException {
    	ConfigLoader cl = new ConfigLoader(dataLayer, configSource);
        configSource.registerLoader(cl, minInterval, 0);
        CallSource cs = dataLayer.getCallSource();
        if(cs instanceof RootCallSource) {
            configSource.reload(); //cl.load(configSource);
            ((RootCallSource)cs).addCallSource(cl);
        } else if(cs instanceof BasicCallSource) {
            configSource.reload(); //cl.load(configSource);
            ((BasicCallSource)cs).addAllCalls(cl);
        } else { // trouble
            throw new ConfigException("The CallSource for the DataLayer is an instance of " + cs.getClass().getName() + " which ConfigLoader does not known how to handle");
        }
        if(cl.configException != null && cl.getErrorResponse() == ErrorResponse.SKIP_AND_REPORT_ERRORS)
        	throw cl.configException;
    }

    /**
     * Parses the XML of the specified file adding any calls or queries to
     * the DataLayer's cache. The file must be in the classpath of this application.
     * @param filePath The file path of the source of the configuration information
     * @throws ConfigException If the configuration information is invalid
     * @throws IOException If the config source cannot be read
     */
    public static void loadConfig(String filePath) throws ConfigException, IOException, LoadingException {
        loadConfig(ConfigSource.createConfigSource(filePath), DataLayerMgr.getGlobalDataLayer());
    }

    /**
     * Parses the XML of the specified file adding any calls or queries to
     * the DataLayer's cache. The file must be in the classpath of this application.
     * @param filePath The file path of the source of the configuration information
     * @throws ConfigException If the configuration information is invalid
     * @throws IOException If the config source cannot be read
     */
    public static void loadConfig(String filePath, long minInterval) throws ConfigException, IOException, LoadingException {
        loadConfig(ConfigSource.createConfigSource(filePath), DataLayerMgr.getGlobalDataLayer(), minInterval);
    }

    /**
     * Parses the XML of the specified file adding any calls or queries to
     * the DataLayer's cache. The file must be in the classpath of this application.
     * @param dataLayer The datalayer to configure
     * @param filePath The file path of the source of the configuration information
     * @throws ConfigException If the configuration information is invalid
     * @throws IOException If the config source cannot be read
     */
    public static void loadConfig(String filePath, DataLayer dataLayer) throws ConfigException, IOException, LoadingException {
        loadConfig(ConfigSource.createConfigSource(filePath), dataLayer);
    }

    /**
     * Parses the XML of the default file 'data-layer.xml' adding any calls or queries to
     * the DataLayer's cache. The file must be in the classpath of this application.
     * @throws ConfigException If the configuration information is invalid
     * @throws IOException If the config source cannot be read
     */
    public static void loadConfig() throws ConfigException, IOException, LoadingException {
        loadConfig(DataLayerMgr.getGlobalDataLayer());
    }

    /**
     * Parses the XML of the default file 'data-layer.xml' adding any calls or queries to
     * the DataLayer's cache. The file must be in the classpath of this application.
     * @param dataLayer The datalayer to configure
     * @throws ConfigException If the configuration information is invalid
     * @throws IOException If the config source cannot be read
     */
    public static void loadConfig(DataLayer dataLayer) throws ConfigException, IOException, LoadingException {
        loadConfig("data-layer.xml", dataLayer);
    }

    /**
     * Parses the specified XML adding any calls or queries to
     * the DataLayer's cache.
     * @param xml The xml configuration information
     * @throws ConfigException If the configuration information is invalid
     * @throws IOException If the config source cannot be read
     */
    public static void loadConfigFromString(String xml) throws ConfigException, LoadingException {
        loadConfigFromString(xml, DataLayerMgr.getGlobalDataLayer());
    }

    /**
     * Parses the specified XML adding any calls or queries to
     * the DataLayer's cache.
     * @param xml The xml configuration information
     * @param dataLayer The datalayer to configure
     * @throws ConfigException If the configuration information is invalid
     * @throws IOException If the config source cannot be read
     */
    public static void loadConfigFromString(String xml, DataLayer dataLayer) throws ConfigException, LoadingException {
        loadConfig(ConfigSource.createConfigSource(xml.getBytes()), dataLayer);
    }

    /**
     * Parses the XML of the specified file adding any calls or queries to
     * the DataLayer's cache.
     * @param in The stream of xml configuration information
     * @throws ConfigException If the configuration information is invalid
     * @throws IOException If the config source cannot be read
     */
    public static void loadConfig(InputStream in) throws ConfigException, LoadingException {
        loadConfig(ConfigSource.createConfigSource(in), DataLayerMgr.getGlobalDataLayer());
    }

    /*
    public void load3(ConfigSource source) throws LoadingException {
        log.info("Loading Data Layer Config XML");
        clearCalls();
        Digester digester = new Digester();
        //digester.setLogger(new simple.io.logging.ApacheLog()); // so that we log all messages
        digester.setClassLoader(ConfigLoader.class.getClassLoader());
        digester.push(this);*/
        //String prefix = "data-layer";
        //String calltag = "*/call";
        //String querytag = "*/query";
        //String paramtag = "*/parameter";
        //String cursortag = "*/cursor";
        //String columntag = "*/column";
        //String derivedtag = "*/derive";
        //String propertytag = "*/derive/property";
        //String texttag = "*/derive/text";
        /*
        digester.addObjectCreate(querytag, "simple.db.config.QueryTag");
        digester.addSetProperties(querytag);
        digester.addSetNext(querytag,"addCall");

        digester.addObjectCreate(calltag, "simple.db.config.CallTag");
        digester.addSetProperties(calltag);
        digester.addSetNext(calltag,"addCall");

        digester.addObjectCreate(paramtag, "simple.db.config.ParameterTag");
        digester.addSetProperties(paramtag);
        digester.addSetNext(paramtag,"addArgument");

        digester.addObjectCreate(cursortag, "simple.db.config.CursorTag");
        digester.addSetProperties(cursortag);
        digester.addSetNext(cursortag,"addArgument");

        digester.addObjectCreate(columntag, "simple.db.config.ColumnTag");
        digester.addSetProperties(columntag);
        digester.addSetNext(columntag,"addColumn");

        digester.addObjectCreate(derivedtag, "simple.db.config.DerivedTag");
        digester.addSetProperties(derivedtag);
        digester.addSetNext(derivedtag,"addDerived");

        digester.addObjectCreate(propertytag, "simple.db.config.PropertyTag");
        digester.addSetProperties(propertytag);
        digester.addSetNext(propertytag,"addPart");

        digester.addObjectCreate(texttag, "simple.db.config.TextTag");
        digester.addSetProperties(texttag);
        digester.addSetNext(texttag,"addPart");

        try {
            digester.parse(source.getInputStream());
        } catch(SAXException saxe) {
            throw new LoadingException("Could not load Data Layer Config XML"
            + (saxe.getMessage() != null && saxe.getMessage().trim().length() > 0 ? ": " + saxe.getMessage() : ""),
            saxe.getException() != null ? saxe.getException() : saxe);
        } catch (IOException ioe) {
            throw new LoadingException("Could not read Data Layer Config XML input stream", ioe);
        }
        log.info("Successfully processed Data Layer Config XML");
        //if basiccallsource then addall
        if(addToCallSource != null) addToCallSource.addAllCalls(this);
    }

    public void load1(ConfigSource source) throws LoadingException {
        log.info("Loading Data Layer Config XML");
        clearCalls();

        MapListXMLLoader l = new MapListXMLLoader("", "s","tag","_");
        Map root;
        try {
            root = (Map)l.load(source.getInputStream());
            String defaultCallClass = (String)root.get("callClass");

        } catch(SAXException saxe) {
            throw new LoadingException("Could not load Data Layer Config XML"
            + (saxe.getMessage() != null && saxe.getMessage().trim().length() > 0 ? ": " + saxe.getMessage() : ""),
            saxe.getException() != null ? saxe.getException() : saxe);
        } catch (IOException ioe) {
            throw new LoadingException("Could not read Data Layer Config XML input stream", ioe);
        } catch (ParserConfigurationException e) {
            throw new LoadingException("ParserConfiguration Problems", e);
        }
        log.info("Successfully processed Data Layer Config XML");
        //if basiccallsource then addall
        if(addToCallSource != null) addToCallSource.addAllCalls(this);
    }
    */
    /**
     * Parses the XML of the specified ConfigSource's InputStream and adds any calls or queries to
     * the CallSource's cache.
     * @param source The source of the configuration information
     * @throws ConfigException If the configuration information is invalid
     * @throws IOException If the config source cannot be read
     */
    public void load(ConfigSource source) throws LoadingException {
        log.info("Loading Data Layer Config XML");
        clearCalls();
        try {
			loadInternal(source);
		} catch(IOException e) {
			throw new LoadingException("Could not read Data Layer Config XML input stream", e);
		} catch(SAXException e) {
            throw new LoadingException("Could not load Data Layer Config XML"
                    + (e.getMessage() != null && e.getMessage().trim().length() > 0 ? ": " + e.getMessage() : ""),
                    e.getException() != null ? e.getException() : e);
		} catch(ParserConfigurationException e) {
		    throw new LoadingException("ParserConfiguration Problems", e);
        } catch(ConvertException e) {
        	throw new LoadingException("Could not convert value in Data Layer Config XML input stream", e);
		}
    }

    protected void loadInternal(ConfigSource source) throws IOException, ParserConfigurationException, SAXException, ConvertException {
    	ObjectBuilder builder = new ObjectBuilder(source.getInputStream());
    	Map<String,Boolean> dynamicDataSources = new HashMap<String, Boolean>();
    	for(ObjectBuilder callBuilder : builder.getSubNodes()) {
			final String id = callBuilder.getAttrValue("id");
    		String dataSourceName = callBuilder.getAttrValue("dataSource");
            Boolean dynamic = dynamicDataSources.get(dataSourceName);
            if(dynamic == null) {
            	dynamic = false;
            	if(dataLayer.getDataSourceFactory().isDynamicLoadingAllowed(dataSourceName)) {
            		try {
						dataLayer.getConnection(dataSourceName).close();
					} catch(DataSourceNotFoundException e) {
					} catch(SQLException e) {
						dynamic = true;
					}
            	}
            	dynamicDataSources.put(dataSourceName, dynamic);
            }
			if(removeCall(id) != null)
				log.error("Call '" + id + "' was already configured. Old definition has been overwritten");

    		if(dynamic) {
    			final ObjectBuilder ob = callBuilder;
    			futureCallMap.put(id, new RunOnGetFuture<Call>() {
    				/**
    				 * @see simple.util.concurrent.RunOnGetFuture#call()
    				 */
    				@Override
					protected Call call(Object... params) throws Exception {
						Call call = Call.readXML(ob);
						call.setLabel(id);
						call.initialize();
						return call;
    				}
    			});
    		} else {
	    		Call call;
				try {
					call = Call.readXML(callBuilder);
					call.setLabel(id);
					call.initialize();
					addCall(id, call);
				} catch(ConvertException e) {
					switch(getErrorResponse()) {
	            		case SKIP_AND_REPORT_ERRORS:
	            			if(configException == null)
	            				configException = new ConfigException("While parsing call '" + id + "'", e);
	            		case IGNORE_ERRORS:
	            			log.info("While reading data layer call. Continuing to process file", e);
	            			break;
	            		case ABORT_ON_ERROR:
	            			throw e;
	            	}
				} catch(SAXException e) {
					switch(getErrorResponse()) {
	            		case SKIP_AND_REPORT_ERRORS:
	            			if(configException == null)
	            				configException = new ConfigException("While parsing call '" + id + "'", e);
	            		case IGNORE_ERRORS:
	            			log.info("While reading data layer call. Continuing to process file", e);
	            			break;
	            		case ABORT_ON_ERROR:
	            			throw e;
	            	}
				}
    		}
 		}
    }
    protected void loadInternalOld(ConfigSource source) throws IOException, SAXException, ParserConfigurationException, ConfigException {
        BeanXMLLoader loader = new BeanXMLLoader();
        loader.setIgnoreAddTextErrors(true);
        loader.registerTagClass("data-layer", DataLayerTag.class);
        loader.registerTagClass("query", QueryTag.class);
        loader.registerTagClass("call", CallTag.class);
        loader.registerTagClass("parameter", ParameterTag.class);
        loader.registerTagClass("cursor", CursorTag.class);
        loader.registerTagClass("column", ColumnTag.class);
        loader.registerTagClass("derive", DerivedTag.class);
        loader.registerTagClass("property", PropertyTag.class);
        loader.registerTagClass("text", TextTag.class);
        loader.registerTagClass("convert", ConvertColumnTag.class);

        configException = null;
        DataLayerTag dlt = (DataLayerTag) loader.load(source.getInputStream());
        for(CallTag ct : dlt.getCallTags()) {
            if(ct.getCallClass() == null) ct.setCallClass(dlt.getCallClass());
            if(ct.getDataSource() == null) ct.setDataSource(dlt.getDataSource());
            try {
            	addCall(ct);
            } catch (ConfigException e) {
            	switch(getErrorResponse()) {
            		case SKIP_AND_REPORT_ERRORS:
            			if(configException == null)
            				configException = e;
            		case IGNORE_ERRORS:
            			log.info("While reading data layer call. Continuing to process file", e);
            			break;
            		case ABORT_ON_ERROR:
            			throw e;
            	}
            }
        }
        log.info("Successfully processed Data Layer Config XML");
     }

    /**
     * Reloads the datalayer with the configuration information if that info had changed
     * @return Whether the datalayer was reloaded
     */
    public boolean reload() {
        try {
            return configSource.reload();
        } catch(LoadingException e) {
            log.warn("Could not load Data Layer Config", e);
            return true; // calls may have changed
        } catch(RuntimeException e) {
            log.warn("Could not load Data Layer Config", e);
            return true; // calls may have changed
        }
    }

    public void lock() {
        lock.lock();
    }

    public void unlock() {
        lock.unlock();
    }

	public ErrorResponse getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(ErrorResponse errorResponse) {
		this.errorResponse = errorResponse;
	}

}
