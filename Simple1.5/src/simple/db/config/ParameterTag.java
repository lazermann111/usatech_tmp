/*
 * CallTag.java
 *
 * Created on December 30, 2002, 5:20 PM
 */

package simple.db.config;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class ParameterTag {

    /** Holds value of property direction. */
    private String direction;
    
    /** Holds value of property index. */
    private int index;
    
    /** Holds value of property type. */
    private String type;
    
    /** Holds value of property property. */
    private String property;
    
    /** Holds value of property typeName. */
    private String typeName;
    
    private boolean required;
    
    private String defaultValue;
    
    /** Creates new CallTag */
    public ParameterTag() {
    }

    /** Getter for property direction.
     * @return Value of property direction.
     */
    public String getDirection() {
        return direction;
    }
    
    /** Setter for property direction.
     * @param direction New value of property direction.
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }
    
    /** Getter for property index.
     * @return Value of property index.
     */
    public int getIndex() {
        return index;
    }
    
    /** Setter for property index.
     * @param index New value of property index.
     */
    public void setIndex(int index) {
        this.index = index;
    }
    
    /** Getter for property type.
     * @return Value of property type.
     */
    public String getType() {
        return type;
    }
    
    /** Setter for property type.
     * @param type New value of property type.
     */
    public void setType(String type) {
        this.type = type;
    }
    
    /** Getter for property property.
     * @return Value of property property.
     */
    public String getProperty() {
        return property;
    }
    
    /** Setter for property property.
     * @param property New value of property property.
     */
    public void setProperty(String property) {
        this.property = property;
    }
    
    /** Getter for property typeName.
     * @return Value of property typeName.
     */
    public String getTypeName() {
        return this.typeName;
    }
    
    /** Setter for property typeName.
     * @param typeName New value of property typeName.
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    
    public String getDefaultValue() {
        return defaultValue;
    }
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
    public boolean isRequired() {
        return required;
    }
    public void setRequired(boolean required) {
        this.required = required;
    }
}
