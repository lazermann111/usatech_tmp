/*
 * CallTag.java
 *
 * Created on December 30, 2002, 5:20 PM
 */

package simple.db.config;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class ColumnTag {

    /** Holds value of property property. */
    private String property;
    
    /** Holds value of property column. */
    private String column;
    
    /** Holds value of property format. */
    private String format;
    
    /** Creates new CallTag */
    public ColumnTag() {
    }

    /** Getter for property property.
     * @return Value of property property.
     */
    public String getProperty() {
        return property;
    }
    
    /** Setter for property property.
     * @param property New value of property property.
     */
    public void setProperty(String property) {
        this.property = property;
    }
    
    /** Getter for property column.
     * @return Value of property column.
     */
    public String getColumn() {
        return column;
    }
    
    /** Setter for property column.
     * @param column New value of property column.
     */
    public void setColumn(String column) {
        this.column = column;
    }
    
    /**
     * @return Returns the format.
     */
    public String getFormat() {
        return format;
    }
    
    /**
     * @param format The format to set.
     */
    public void setFormat(String format) {
        this.format = format;
    }
}
