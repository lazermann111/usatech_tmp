/*
 * PropertyTag.java
 *
 * Created on February 26, 2003, 1:44 PM
 */

package simple.db.config;

/**
 *
 * @author  Brian S. Krug
 */
public class PropertyTag {
    
    /** Holds value of property name. */
    private String name;
    
    /** Holds value of property format. */
    private String format;
    
    /** Creates a new instance of PropertyTag */
    public PropertyTag() {
    }
    
    /** Getter for property name.
     * @return Value of property name.
     */
    public String getName() {
        return this.name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /** Getter for property format.
     * @return Value of property format.
     */
    public String getFormat() {
        return this.format;
    }
    
    /** Setter for property format.
     * @param format New value of property format.
     */
    public void setFormat(String format) {
        this.format = format;
    }
    
}
