/*
 * CallTag.java
 *
 * Created on December 30, 2002, 5:20 PM
 */

package simple.db.config;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class CallTag {
    private String id;
    private String sql;
    private List<Object> arguments = new LinkedList<Object>();
    
    /** Holds value of property scrollable. */
    private String scrollable;
    
    /** Holds value of property concurrency. */
    private String concurrency;
    
    /** Holds value of property dataSource. */
    private String dataSource;
    
    /** Holds value of property property.   */
    private String property;
    
    private String callClass;
    
    /** Creates new CallTag */
    public CallTag() {
    }

    public void addArgument(ParameterTag pt) {
        arguments.add(pt);
    }
    
    public void addArgument(CursorTag ct) {
        arguments.add(ct);
    }
    
    public void add(ParameterTag pt) {
        arguments.add(pt);
    }
    
    public void add(CursorTag ct) {
        arguments.add(ct);
    }
    
    public List<Object> getArguments() {
        return arguments;
    }
    
    /** Getter for property id.
     * @return Value of property id.
     */
    public String getId() {
        return id;
    }
    
    /** Setter for property id.
     * @param id New value of property id.
     */
    public void setId(String id) {
        this.id = id;
    }
    
    /** Getter for property sql.
     * @return Value of property sql.
     */
    public String getSql() {
        return sql;
    }
    
    /** Setter for property sql.
     * @param sql New value of property sql.
     */
    public void setSql(String sql) {
        this.sql = sql;
    }
    
    /** Getter for property scrollable.
     * @return Value of property scrollable.
     */
    public String getScrollable() {
        return scrollable;
    }
    
    /** Setter for property scrollable.
     * @param scrollable New value of property scrollable.
     */
    public void setScrollable(String scrollable) {
        this.scrollable = scrollable;
    }
    
    /** Getter for property concurrency.
     * @return Value of property concurrency.
     */
    public String getConcurrency() {
        return concurrency;
    }
    
    /** Setter for property concurrency.
     * @param concurrency New value of property concurrency.
     */
    public void setConcurrency(String concurrency) {
        this.concurrency = concurrency;
    }
    
    /** Getter for property dataSource.
     * @return Value of property dataSource.
     */
    public String getDataSource() {
        return this.dataSource;
    }
    
    /** Setter for property dataSource.
     * @param dataSource New value of property dataSource.
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }
    
    /** Setter for property property.
     * @param property New value of property property.
     *
     *
     */
    public void setProperty(String property) {
        this.property = property;
    }
    
    /** Getter for property property.
     * @return Value of property property.
     *
     *
     */
    public String getProperty() {
        return property;
    }

    /**
     * @return Returns the callClass.
     */
    public String getCallClass() {
        return callClass;
    }

    /**
     * @param callClass The callClass to set.
     */
    public void setCallClass(String callClass) {
        this.callClass = callClass;
    }
    
}
