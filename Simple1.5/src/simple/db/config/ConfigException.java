/*
 * ConfigException.java
 *
 * Created on December 30, 2002, 2:16 PM
 */

package simple.db.config;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * 
 * @author Brian S. Krug
 * @version
 */
public class ConfigException extends java.lang.Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -13481300041L;
	/** Holds value of property rootCause. */
    private Exception rootCause;

    /**
     * Creates new <code>ConfigException</code> without detail message.
     * 
     * @param rootCause
     */
    public ConfigException(Exception rootCause) {
        super(rootCause);
        // this.rootCause = rootCause;
    }

    /**
     * Creates new <code>ConfigException</code> without detail message.
     * 
     * @param msg
     * @param rootCause
     */
    public ConfigException(String msg, Exception rootCause) {
        super(msg, rootCause);
        // this.rootCause = rootCause;
    }

    /**
     * Constructs an <code>ConfigException</code> with the specified detail
     * message.
     * 
     * @param msg
     *            the detail message.
     */
    public ConfigException(String msg) {
        super(msg);
    }

    /**
     * Getter for property rootCause.
     * 
     * @return Value of property rootCause.
     */
    public Exception getRootCause() {
        return rootCause;
    }

    /**
     * Prints the stack trace of the thrown rootCause exception.
     * 
     * @see java.lang.System#err
     */
    public void printStackTrace() {
        printStackTrace(System.err);
    }

    /**
     * Prints the stack trace of the thrown rootCause exception to the specified
     * print stream.
     * 
     * @param ps
     */
    public void printStackTrace(PrintStream ps) {
        if (rootCause != null) {
            ps.print(getClass().getName() + ": ");
            rootCause.printStackTrace(ps);
        } else {
            super.printStackTrace(ps);
        }
    }

    /**
     * Prints the stack trace of the thrown rootCause exception to the specified
     * print writer.
     * 
     * @param pw
     */
    public void printStackTrace(PrintWriter pw) {
        if (rootCause != null) {
            pw.print(getClass().getName() + ": ");
            rootCause.printStackTrace(pw);
        } else {
            super.printStackTrace(pw);
        }
    }
}