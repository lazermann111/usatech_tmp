package simple.db;

public interface CallMXBean {

	/** Getter for property sql.
	 * @return Value of property sql.
	 */
	public String getSql();

	/** Getter for property resultSetType.
	 * @return Value of property resultSetType.
	 */
	public int getResultSetType();

	/** Getter for property resultSetConcurrency.
	 * @return Value of property resultSetConcurrency.
	 */
	public int getResultSetConcurrency();

	/** Getter for property dataSourceName.
	 * @return Value of property dataSourceName.
	 */
	public String getDataSourceName();

	/**
	 * @return Returns the maxRows.
	 */
	public int getMaxRows();

	public double getQueryTimeout();

	public void setQueryTimeout(double queryTimeout);

	public long getLogExecuteTimeThreshhold();

	public void setLogExecuteTimeThreshhold(long logExecuteTimeThreshhold);

	public long getTotalExecutionTime();

	public long getExecutionCount();

	public long getRecentExecutionTime();

	public int getRecentExecutionCount();

	public boolean isTrackingExecuteTime();

	public void setTrackingExecuteTime(boolean trackingExecuteTime);

	public double getAvgExecutionTime();

	public double getRecentAvgExecutionTime();

}