package simple.db;

import java.sql.SQLException;

public class NotEnoughRowsException extends SQLException {
	private static final long serialVersionUID = -1099410123414L;
	public static final String NOT_ENOUGH_ROWS_SQL_STATE = "02000";
	public NotEnoughRowsException() {
		this("Not enough rows");
	}

	public NotEnoughRowsException(String reason) {
		super(reason, NOT_ENOUGH_ROWS_SQL_STATE);
	}
}
