/**
 * 
 */
package simple.db;

import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.lang.Exception;

import org.postgresql.PGConnection;

import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.util.concurrent.CustomThreadFactory;


public class WatchedCallableStatement extends DelegatingCallableStatement {
	private static final Log log = Log.getLog();
	protected static final ScheduledThreadPoolExecutor timeoutService = new ScheduledThreadPoolExecutor(1, new CustomThreadFactory("DbCallWatchdog-", true, 3));
	protected static boolean closeOnSecondTimeout = false;
	protected long timeoutMillis;
	protected static enum CallState { IDLE, RUNNING, CANCELING, CLOSING }
	protected final AtomicReference<CallState> state = new AtomicReference<CallState>(CallState.IDLE);
	protected final Callable<Boolean> canceller = new Callable<Boolean>() {
		public Boolean call() throws Exception {
			switch(state.get()) {
				case RUNNING:
					if(state.compareAndSet(CallState.RUNNING, CallState.CANCELING)) {
						cancel();
						if(log.isInfoEnabled()) {
							Connection realConn = DBUnwrap.getRealConnection(delegate.getConnection());
							log.info("Canceled call '" + delegate + "' from thread '" + threadName + "' on connection '" + DataLayerMgr.getDBHelper(realConn).getConnectionIdentifier(realConn) + "' because of call timeout");
						}
						return true;
					}
					break;
				case CANCELING:
					if(closeOnSecondTimeout) {
						if(state.compareAndSet(CallState.CANCELING, CallState.CLOSING)) {
							Connection realConn = DBUnwrap.getRealConnection(delegate.getConnection());
							pgCancelBackend(delegate.getConnection());
							realConn.close();
							if(log.isInfoEnabled())
								log.info("Closed connection '" + DataLayerMgr.getDBHelper(realConn).getConnectionIdentifier(realConn) + "' used by thread '" + threadName + "' because of call timeout");
							return true;
						}
					} else {
						cancel();
						if(log.isInfoEnabled()) {
							Connection realConn = DBUnwrap.getRealConnection(delegate.getConnection());
							log.info("Attempted again to cancel call '" + delegate + "' from thread '" + threadName + "' on connection '" + DataLayerMgr.getDBHelper(realConn).getConnectionIdentifier(realConn) + "' because of call timeout");
						}
						return true;
					}
					break;
				default:
					return false;
			}
			return call();
		}

		private void pgCancelBackend(Connection conn){
			String product;
			try {
				product = conn.getMetaData().getDatabaseProductName();
				if (!product.toUpperCase().contains("POSTGRESQL")) {
					log.info("pgCancelBackend method only for PostgreSQL");
					return;
				}
			    
			    int pid = ((PGConnection)DBUnwrap.getRealConnection(conn)).getBackendPID();
				Connection newConn = DataLayerMgr.getConnection(getDataSourceName(conn));
				Statement statement = newConn.createStatement();
				try {
					if (statement.execute("SELECT pg_cancel_backend(" + pid + ");"))
						log.info("Connection '"	+ DataLayerMgr.getDBHelper(conn).getConnectionIdentifier(conn)	+ "' was canceled ");
				} finally{
					statement.close();
					newConn.close();
				}
			} catch (Exception e) {
				log.warn("Failed to cancel backend call'"	+ DataLayerMgr.getDBHelper(conn).getConnectionIdentifier(conn), e);
			}
		}

		private String getDataSourceName(Connection conn) throws Exception {
			Method method = ReflectionUtils.findMethod(conn.getClass(),"getDataSourceName", new Class[0]);
    		String name = method.invoke(conn).toString();
			return name;
		}
	};
	protected ScheduledFuture<Boolean> cancelFuture;
	protected ScheduledFuture<Boolean> closeFuture;
	protected String threadName;
	
	public WatchedCallableStatement(CallableStatement delegate) throws SQLException {
		super(delegate, delegate.getConnection());
	}

	public WatchedCallableStatement(CallableStatement delegate, long timeoutMillis) throws SQLException {
		super(delegate, delegate.getConnection());
		setTimeoutMillis(timeoutMillis);
	}
	
	public WatchedCallableStatement(CallableStatement delegate, long timeoutMillis, boolean closeSession) throws SQLException {
		super(delegate, delegate.getConnection());
		setTimeoutMillis(timeoutMillis);
		setCloseOnSecondTimeout(closeSession);
	}

	public long getTimeoutMillis() {
		return timeoutMillis;
	}

	public void setTimeoutMillis(long timeoutMillis) {
		this.timeoutMillis = timeoutMillis;
	}

	protected void beforeExecute() {
		switch(state.get()) {
			case IDLE:
				if(timeoutMillis > 0) {
					if(state.compareAndSet(CallState.IDLE, CallState.RUNNING)) {
						cancelFuture = timeoutService.schedule(canceller, timeoutMillis, TimeUnit.MILLISECONDS);
						closeFuture = timeoutService.schedule(canceller, timeoutMillis * 2, TimeUnit.MILLISECONDS);
						if(log.isInfoEnabled())
							threadName = Thread.currentThread().getName();
						else
							threadName = "UNKNOWN";
					} else
						beforeExecute();
				}
				break;
			default:
				log.warn("Call was executed more than once before completing");
		}
	}

	protected void afterExecute() throws SQLException {
		CallState current = state.get();
		switch(current) {
			case RUNNING: case CANCELING:
				if(state.compareAndSet(current, CallState.IDLE)) {
					if(cancelFuture != null) {
						if(!(cancelFuture instanceof RunnableScheduledFuture<?>) 
								|| !timeoutService.remove((RunnableScheduledFuture<?>)cancelFuture)) {
							if(!cancelFuture.cancel(true)){
								Connection realConn = DBUnwrap.getRealConnection(delegate.getConnection());
								log.warn("Failed to cancel cancelCall watchdog " + DataLayerMgr.getDBHelper(realConn).getConnectionIdentifier(realConn));
							}
						}
						cancelFuture = null;
					}
					if(closeFuture != null) {
						if(!(closeFuture instanceof RunnableScheduledFuture<?>) 
								|| !timeoutService.remove((RunnableScheduledFuture<?>)closeFuture)) {
							if(!closeFuture.cancel(true)){
								Connection realConn = DBUnwrap.getRealConnection(delegate.getConnection());
								log.warn("Failed to cancel closeCall watchdog " + DataLayerMgr.getDBHelper(realConn).getConnectionIdentifier(realConn));
							}
						}
						closeFuture = null;
					}
				} else
					afterExecute();
				break;
			case CLOSING:
				if(state.compareAndSet(CallState.CLOSING, CallState.IDLE)) {
					log.info("Call was closed before it completed");
				} else
					afterExecute();
				break;
			default:
				log.warn("Call is in an invalid state " + current);
		}
	}

	@Override
	public boolean execute() throws SQLException {
		beforeExecute();
		try {
			return super.execute();
		} finally {
			afterExecute();
		}
	}

	@Override
	public boolean execute(String sql) throws SQLException {
		beforeExecute();
		try {
			return super.execute(sql);
		} finally {
			afterExecute();
		}
	}

	@Override
	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		beforeExecute();
		try {
			return super.execute(sql, autoGeneratedKeys);
		} finally {
			afterExecute();
		}
	}

	@Override
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		beforeExecute();
		try {
			return super.execute(sql, columnIndexes);
		} finally {
			afterExecute();
		}
	}

	@Override
	public boolean execute(String sql, String[] columnNames) throws SQLException {
		beforeExecute();
		try {
			return super.execute(sql, columnNames);
		} finally {
			afterExecute();
		}
	}

	@Override
	public int[] executeBatch() throws SQLException {
		beforeExecute();
		try {
			return super.executeBatch();
		} finally {
			afterExecute();
		}
	}

	@Override
	public ResultSet executeQuery() throws SQLException {
		beforeExecute();
		try {
			return super.executeQuery();
		} finally {
			afterExecute();
		}
	}

	@Override
	public ResultSet executeQuery(String sql) throws SQLException {
		beforeExecute();
		try {
			return super.executeQuery(sql);
		} finally {
			afterExecute();
		}
	}

	@Override
	public int executeUpdate() throws SQLException {
		beforeExecute();
		try {
			return super.executeUpdate();
		} finally {
			afterExecute();
		}
	}

	@Override
	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		beforeExecute();
		try {
			return super.executeUpdate(sql, autoGeneratedKeys);
		} finally {
			afterExecute();
		}
	}

	@Override
	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		beforeExecute();
		try {
			return super.executeUpdate(sql, columnIndexes);
		} finally {
			afterExecute();
		}
	}

	@Override
	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		beforeExecute();
		try {
			return super.executeUpdate(sql, columnNames);
		} finally {
			afterExecute();
		}
	}

	@Override
	public int executeUpdate(String sql) throws SQLException {
		beforeExecute();
		try {
			return super.executeUpdate(sql);
		} finally {
			afterExecute();
		}
	}

	public static boolean isCloseOnSecondTimeout() {
		return closeOnSecondTimeout;
	}

	public static void setCloseOnSecondTimeout(boolean closeOnSecondTimeout) {
		WatchedCallableStatement.closeOnSecondTimeout = closeOnSecondTimeout;
	}
}