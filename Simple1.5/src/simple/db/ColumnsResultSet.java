/**
 *
 */
package simple.db;

import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;

public abstract class ColumnsResultSet implements ResultSet {
	protected final ResultSetColumn[] columns;
	protected final Map<String,Integer> columnMapping = new HashMap<String, Integer>();
	protected final ResultSetMetaData metaData = new ResultSetMetaData() {

		public String getCatalogName(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getCatalogName();
		}

		public String getColumnClassName(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getColumnClassName();
		}

		public int getColumnCount() throws SQLException {
			return columns.length;
		}

		public int getColumnDisplaySize(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getColumnDisplaySize();
		}

		public String getColumnLabel(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getColumnLabel();
		}

		public String getColumnName(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getColumnName();
		}

		public int getColumnType(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getSqlType();
		}

		public String getColumnTypeName(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getColumnTypeName();
		}

		public int getPrecision(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getPrecision();
		}

		public int getScale(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getScale();
		}

		public String getSchemaName(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getSchemaName();
		}

		public String getTableName(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).getTableName();
		}

		public boolean isAutoIncrement(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).isAutoIncrement();
		}

		public boolean isCaseSensitive(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).isCaseSensitive();
		}

		public boolean isCurrency(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).isCurrency();
		}

		public boolean isDefinitelyWritable(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).isDefinitelyWritable();
		}

		public int isNullable(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).isNullable();
		}

		public boolean isReadOnly(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).isReadOnly();
		}

		public boolean isSearchable(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).isSearchable();
		}

		public boolean isSigned(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).isSigned();
		}

		public boolean isWritable(int columnIndex) throws SQLException {
			return getResultSetColumn(columnIndex).isWritable();
		}

		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return false;
		}

		public <T> T unwrap(Class<T> iface) throws SQLException {
			throw new SQLException("Unwrap not supported");
		}
	};
	protected boolean wasNull;
	protected ColumnsResultSet(ResultSetColumn... columns) throws SQLException  {
		this.columns = columns;
		for(int i = 0; i < columns.length; i++) {
			columnMapping.put(columns[i].getColumnName(), i);
		}
	}
	protected ResultSetColumn getResultSetColumn(int columnIndex) throws SQLException {
		try {
			return columns[columnIndex-1];
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new SQLException("Invalid column index " + columnIndex + " for this resultset", "42001");
		}
	}
	protected <T> T getCurrentValue(int columnIndex, Class<T> convertTo) throws SQLException {
		try {
			Object value = getResultSetColumn(columnIndex).getCurrentValue();
			wasNull = (value == null);
			if(value != null) {
				Method method = ReflectionUtils.findMethod(value.getClass(), "toJdbc", ReflectionUtils.ZERO_PARAMS);
				if(method != null)
					try {
						value = method.invoke(value);
					} catch(IllegalArgumentException e) {
						//Ignore
					} catch(IllegalAccessException e) {
						//Ignore
					} catch(InvocationTargetException e) {
						//Ignore
					}
			}
			return ConvertUtils.convert(convertTo, value);
		} catch(ConvertException e) {
			throw new SQLException("Invalid data type " + convertTo.getName() + " for column index " + columnIndex, "42002", e);
		}
	}
	protected void setCurrentValue(int columnIndex, Object object) throws SQLException {
		getResultSetColumn(columnIndex).setCurrentValue(object);
	}
	protected void setCurrentValue(int columnIndex, Object object, long scaleOrLength) throws SQLException {
		getResultSetColumn(columnIndex).setCurrentValue(object, scaleOrLength);
	}
	public int findColumn(String columnLabel) throws SQLException {
		Integer index = columnMapping.get(columnLabel);
		if(index == null)
			throw new SQLException("Column '" + columnLabel + "' does not exist in the resultset", "42000");
		return 1 + index;
	}
	public Array getArray(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, Array.class);
	}
	public Array getArray(String columnLabel) throws SQLException {
		return getArray(findColumn(columnLabel));
	}
	public InputStream getAsciiStream(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, InputStream.class);
	}
	public InputStream getAsciiStream(String columnLabel) throws SQLException {
		return getAsciiStream(findColumn(columnLabel));
	}
	public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException {
		BigDecimal bd = getBigDecimal(columnIndex);
		if(bd == null)
			return null;
		return bd.setScale(scale);
	}
	public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, BigDecimal.class);
	}
	public BigDecimal getBigDecimal(String columnLabel, int scale) throws SQLException {
		return getBigDecimal(findColumn(columnLabel), scale);
	}
	public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
		return getBigDecimal(findColumn(columnLabel));
	}
	public InputStream getBinaryStream(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, InputStream.class);
	}
	public InputStream getBinaryStream(String columnLabel) throws SQLException {
		return getBinaryStream(findColumn(columnLabel));
	}
	public Blob getBlob(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, Blob.class);
	}
	public Blob getBlob(String columnLabel) throws SQLException {
		return getBlob(findColumn(columnLabel));
	}
	public boolean getBoolean(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, boolean.class);
	}
	public boolean getBoolean(String columnLabel) throws SQLException {
		return getBoolean(findColumn(columnLabel));
	}
	public byte getByte(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, byte.class);
	}
	public byte getByte(String columnLabel) throws SQLException {
		return getByte(findColumn(columnLabel));
	}
	public byte[] getBytes(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, byte[].class);
	}
	public byte[] getBytes(String columnLabel) throws SQLException {
		return getBytes(findColumn(columnLabel));
	}
	public Reader getCharacterStream(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, Reader.class);
	}
	public Reader getCharacterStream(String columnLabel) throws SQLException {
		return getCharacterStream(findColumn(columnLabel));
	}
	public Clob getClob(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, Clob.class);
	}
	public Clob getClob(String columnLabel) throws SQLException {
		return getClob(findColumn(columnLabel));
	}
	public java.sql.Date getDate(int columnIndex, Calendar cal) throws SQLException {
		return getDate(columnIndex); // just ignore cal for now
	}
	public java.sql.Date getDate(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, java.sql.Date.class);
	}
	public java.sql.Date getDate(String columnLabel, Calendar cal) throws SQLException {
		return getDate(findColumn(columnLabel), cal);
	}
	public java.sql.Date getDate(String columnLabel) throws SQLException {
		return getDate(findColumn(columnLabel));
	}
	public double getDouble(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, double.class);
	}
	public double getDouble(String columnLabel) throws SQLException {
		return getDouble(findColumn(columnLabel));
	}
	public float getFloat(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, float.class);
	}
	public float getFloat(String columnLabel) throws SQLException {
		return getFloat(findColumn(columnLabel));
	}
	public int getInt(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, int.class);
	}
	public int getInt(String columnLabel) throws SQLException {
		return getInt(findColumn(columnLabel));
	}
	public long getLong(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, long.class);
	}
	public long getLong(String columnLabel) throws SQLException {
		return getLong(findColumn(columnLabel));
	}
	public ResultSetMetaData getMetaData() throws SQLException {
		return metaData;
	}
	public Reader getNCharacterStream(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, Reader.class);
	}
	public Reader getNCharacterStream(String columnLabel) throws SQLException {
		return getNCharacterStream(findColumn(columnLabel));
	}
	public NClob getNClob(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, NClob.class);
	}
	public NClob getNClob(String columnLabel) throws SQLException {
		return getNClob(findColumn(columnLabel));
	}
	public String getNString(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, String.class);
	}
	public String getNString(String columnLabel) throws SQLException {
		return getNString(findColumn(columnLabel));
	}
	public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException {
		return getObject(columnIndex); // Just ignore map for now
	}
	public Object getObject(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, Object.class);
	}
	public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException {
		return getObject(findColumn(columnLabel), map);
	}
	public Object getObject(String columnLabel) throws SQLException {
		return getObject(findColumn(columnLabel));
	}
	public Ref getRef(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, Ref.class);
	}
	public Ref getRef(String columnLabel) throws SQLException {
		return getRef(findColumn(columnLabel));
	}
	public RowId getRowId(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, RowId.class);
	}
	public RowId getRowId(String columnLabel) throws SQLException {
		return getRowId(findColumn(columnLabel));
	}
	public short getShort(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, short.class);
	}
	public short getShort(String columnLabel) throws SQLException {
		return getShort(findColumn(columnLabel));
	}
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, SQLXML.class);
	}
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
		return getSQLXML(findColumn(columnLabel));
	}
	public String getString(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, String.class);
	}
	public String getString(String columnLabel) throws SQLException {
		return getString(findColumn(columnLabel));
	}
	public Time getTime(int columnIndex, Calendar cal) throws SQLException {
		return getTime(columnIndex); // Just ignore cal for now
	}
	public Time getTime(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, Time.class);
	}
	public Time getTime(String columnLabel, Calendar cal) throws SQLException {
		return getTime(findColumn(columnLabel), cal);
	}
	public Time getTime(String columnLabel) throws SQLException {
		return getTime(findColumn(columnLabel));
	}
	public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException {
		return getTimestamp(columnIndex); // Just ignore cal for now
	}
	public Timestamp getTimestamp(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, Timestamp.class);
	}
	public Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException {
		return getTimestamp(findColumn(columnLabel), cal);
	}
	public Timestamp getTimestamp(String columnLabel) throws SQLException {
		return getTimestamp(findColumn(columnLabel));
	}
	public InputStream getUnicodeStream(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, InputStream.class);
	}
	public InputStream getUnicodeStream(String columnLabel) throws SQLException {
		return getUnicodeStream(findColumn(columnLabel));
	}
	public URL getURL(int columnIndex) throws SQLException {
		return getCurrentValue(columnIndex, URL.class);
	}
	public URL getURL(String columnLabel) throws SQLException {
		return getURL(findColumn(columnLabel));
	}
	public void updateArray(int columnIndex, Array x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateArray(String columnLabel, Array x) throws SQLException {
		updateArray(findColumn(columnLabel), x);
	}
	public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException {
		setCurrentValue(columnIndex, x, length);
	}
	public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
		setCurrentValue(columnIndex, x, length);
	}
	public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateAsciiStream(String columnLabel, InputStream x, int length) throws SQLException {
		updateAsciiStream(findColumn(columnLabel), x, length);
	}
	public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
		updateAsciiStream(findColumn(columnLabel), x, length);
	}
	public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
		updateAsciiStream(findColumn(columnLabel), x);
	}
	public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateBigDecimal(String columnLabel, BigDecimal x) throws SQLException {
		updateBigDecimal(findColumn(columnLabel), x);
	}
	public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException {
		setCurrentValue(columnIndex, x, length);
	}
	public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
		setCurrentValue(columnIndex, x, length);
	}
	public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateBinaryStream(String columnLabel, InputStream x, int length) throws SQLException {
		updateBinaryStream(findColumn(columnLabel), x, length);
	}
	public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
		updateBinaryStream(findColumn(columnLabel), x, length);
	}
	public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
		updateBinaryStream(findColumn(columnLabel), x);
	}
	public void updateBlob(int columnIndex, Blob x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
		setCurrentValue(columnIndex, inputStream, length);
	}
	public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
		setCurrentValue(columnIndex, inputStream);
	}
	public void updateBlob(String columnLabel, Blob x) throws SQLException {
		updateBlob(findColumn(columnLabel), x);
	}
	public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
		updateBlob(findColumn(columnLabel), inputStream, length);
	}
	public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
		updateBlob(findColumn(columnLabel), inputStream);
	}
	public void updateBoolean(int columnIndex, boolean x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateBoolean(String columnLabel, boolean x) throws SQLException {
		updateBoolean(findColumn(columnLabel), x);
	}
	public void updateByte(int columnIndex, byte x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateByte(String columnLabel, byte x) throws SQLException {
		updateByte(findColumn(columnLabel), x);
	}
	public void updateBytes(int columnIndex, byte[] x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateBytes(String columnLabel, byte[] x) throws SQLException {
		updateBytes(findColumn(columnLabel), x);
	}
	public void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException {
		setCurrentValue(columnIndex, x, length);
	}
	public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
		setCurrentValue(columnIndex, x, length);
	}
	public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateCharacterStream(String columnLabel, Reader reader, int length) throws SQLException {
		updateCharacterStream(findColumn(columnLabel), reader, length);
	}
	public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
		updateCharacterStream(findColumn(columnLabel), reader, length);
	}
	public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
		updateCharacterStream(findColumn(columnLabel), reader);
	}
	public void updateClob(int columnIndex, Clob x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
		setCurrentValue(columnIndex, reader, length);
	}
	public void updateClob(int columnIndex, Reader reader) throws SQLException {
		setCurrentValue(columnIndex, reader);
	}
	public void updateClob(String columnLabel, Clob x) throws SQLException {
		updateClob(findColumn(columnLabel), x);
	}
	public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
		updateClob(findColumn(columnLabel), reader, length);
	}
	public void updateClob(String columnLabel, Reader reader) throws SQLException {
		updateClob(findColumn(columnLabel), reader);
	}
	public void updateDate(int columnIndex, java.sql.Date x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateDate(String columnLabel, java.sql.Date x) throws SQLException {
		updateDate(findColumn(columnLabel), x);
	}
	public void updateDouble(int columnIndex, double x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateDouble(String columnLabel, double x) throws SQLException {
		updateDouble(findColumn(columnLabel), x);
	}
	public void updateFloat(int columnIndex, float x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateFloat(String columnLabel, float x) throws SQLException {
		updateFloat(findColumn(columnLabel), x);
	}
	public void updateInt(int columnIndex, int x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateInt(String columnLabel, int x) throws SQLException {
		updateInt(findColumn(columnLabel), x);
	}
	public void updateLong(int columnIndex, long x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateLong(String columnLabel, long x) throws SQLException {
		updateLong(findColumn(columnLabel), x);
	}
	public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
		setCurrentValue(columnIndex, x, length);
	}
	public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
		updateNCharacterStream(findColumn(columnLabel), reader, length);
	}
	public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
		updateNCharacterStream(findColumn(columnLabel), reader);
	}
	public void updateNClob(int columnIndex, NClob clob) throws SQLException {
		setCurrentValue(columnIndex, clob);
	}
	public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
		setCurrentValue(columnIndex, reader, length);
	}
	public void updateNClob(int columnIndex, Reader reader) throws SQLException {
		setCurrentValue(columnIndex, reader);
	}
	public void updateNClob(String columnLabel, NClob clob) throws SQLException {
		updateNClob(findColumn(columnLabel), clob);
	}
	public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
		updateNClob(findColumn(columnLabel), reader, length);
	}
	public void updateNClob(String columnLabel, Reader reader) throws SQLException {
		updateNClob(findColumn(columnLabel), reader);
	}
	public void updateNString(int columnIndex, String string) throws SQLException {
		setCurrentValue(columnIndex, string);
	}
	public void updateNString(String columnLabel, String string) throws SQLException {
		updateNString(findColumn(columnLabel), string);
	}
	public void updateNull(int columnIndex) throws SQLException {
		setCurrentValue(columnIndex, null);
	}
	public void updateNull(String columnLabel) throws SQLException {
		updateNull(findColumn(columnLabel));
	}
	public void updateObject(int columnIndex, Object x, int scaleOrLength) throws SQLException {
		setCurrentValue(columnIndex, x, scaleOrLength);
	}
	public void updateObject(int columnIndex, Object x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateObject(String columnLabel, Object x, int scaleOrLength) throws SQLException {
		updateObject(findColumn(columnLabel), x, scaleOrLength);
	}
	public void updateObject(String columnLabel, Object x) throws SQLException {
		updateObject(findColumn(columnLabel), x);
	}
	public void updateRef(int columnIndex, Ref x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateRef(String columnLabel, Ref x) throws SQLException {
		updateRef(findColumn(columnLabel), x);
	}
	public void updateRowId(int columnIndex, RowId x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateRowId(String columnLabel, RowId x) throws SQLException {
		updateRowId(findColumn(columnLabel), x);
	}
	public void updateShort(int columnIndex, short x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateShort(String columnLabel, short x) throws SQLException {
		updateShort(findColumn(columnLabel), x);
	}
	public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
		setCurrentValue(columnIndex, xmlObject);
	}
	public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
		updateSQLXML(findColumn(columnLabel), xmlObject);
	}
	public void updateString(int columnIndex, String x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateString(String columnLabel, String x) throws SQLException {
		updateString(findColumn(columnLabel), x);
	}
	public void updateTime(int columnIndex, Time x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateTime(String columnLabel, Time x) throws SQLException {
		updateTime(findColumn(columnLabel), x);
	}
	public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException {
		setCurrentValue(columnIndex, x);
	}
	public void updateTimestamp(String columnLabel, Timestamp x) throws SQLException {
		updateTimestamp(findColumn(columnLabel), x);
	}
	public boolean wasNull() throws SQLException {
		return wasNull;
	}

	public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
		return getCurrentValue(columnIndex, type);
	}

	public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
		return getCurrentValue(findColumn(columnLabel), type);
	}

	public void updateObject(int columnIndex, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
		setCurrentValue(columnIndex, x, scaleOrLength);
	}

	public void updateObject(String columnLabel, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
		setCurrentValue(findColumn(columnLabel), x, scaleOrLength);
	}

	public void updateObject(int columnIndex, Object x, SQLType targetSqlType) throws SQLException {
		setCurrentValue(columnIndex, x);
	}

	public void updateObject(String columnLabel, Object x, SQLType targetSqlType) throws SQLException {
		setCurrentValue(findColumn(columnLabel), x);
	}
}