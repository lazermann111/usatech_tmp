/**
 *
 */
package simple.db;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Brian S. Krug
 *
 */
public class NullConnectionHolder implements ConnectionHolder {
	/**
	 * @see simple.db.ConnectionHolder#closeConnection()
	 */
	public boolean closeConnection() {
		return false;
	}

	/**
	 * @see simple.db.ConnectionHolder#getConnection()
	 */
	public Connection getConnection() throws SQLException, DataSourceNotFoundException {
		return null;
	}

	/**
	 * @see simple.db.ConnectionHolder#reconnect()
	 */
	public Connection reconnect() throws SQLException, DataSourceNotFoundException {
		return null;
	}
	/**
	 * @see simple.db.ConnectionHolder#getConnectionClass()
	 */
	public Class<? extends Connection> getConnectionClass() {
		return null;
	}

	@Override
	public void commitOrRollback(boolean commit) throws SQLException {
	}
}
