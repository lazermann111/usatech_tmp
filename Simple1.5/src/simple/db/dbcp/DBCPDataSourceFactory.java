/*
 * Created on Aug 17, 2005
 *
 */
package simple.db.dbcp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import simple.db.AbstractDataSourceFactory;
import simple.db.DataSourceNotFoundException;
import simple.db.ExtendedDataSource;
import simple.io.Log;
import simple.util.StaticCollection;

public class DBCPDataSourceFactory extends AbstractDataSourceFactory {
	static final Log log = Log.getLog();
    protected Properties properties;
    protected Map<String,Properties> subProperties;
    protected String[] dataSourceNames;
    protected static final String[][] PROPERTY_TRANSLATION = new String[][] {
        {"driverClassName", "driver"},
        {"url", "database"},
        {"username", "username"}, // allows case-insensitivity
        {"password", "password"}, // allows case-insensitivity
    };
    protected static final Collection<String> EXISTS_TEST = StaticCollection.create("driverClassName","url");

    public DBCPDataSourceFactory(Properties properties) {
        super();
        this.properties = new Properties();
        for(Object p : properties.keySet()) {
        	String name = (String)p;
        	setProperty(name, properties.getProperty(name));
        }
    }
    protected void setProperty(String name, String value) {
    	if(!possiblySetBeanProperty(name,value))
    		properties.put(name, value);
    }
    protected Properties getDataSourceProperties(String key) throws DataSourceNotFoundException {
    	if(key != null) key = key.toUpperCase();
        Properties p = getSubProperties().get(key);
        if(p == null) throw new DataSourceNotFoundException(key);
        if(!p.containsKey("defaultAutoCommit")) p.setProperty("defaultAutoCommit", "false");
        return p;
    }

    protected static String[] parseName(String s) {
        int p = s.lastIndexOf('.');
        if(p < 0) return new String[]{"", s};
        else return new String[]{s.substring(0, p).trim().toUpperCase(), s.substring(p+1)};
    }
    @Override
    protected boolean isCaseSensitive() {
    	return false;
    }
    @Override
    protected ExtendedDataSource createDataSource(String name) throws DataSourceNotFoundException {
    	ExtendedDataSource ds;
    	try {
            ds = createDataSource(getDataSourceProperties(name));
        } catch (DataSourceNotFoundException e) {
        	throw e;
        } catch (Exception e) {
            throw new DataSourceNotFoundException(name, e);
        }
        return ds;
    }

    private final static String PROP_DEFAULTAUTOCOMMIT = "defaultAutoCommit";
    private final static String PROP_DEFAULTREADONLY = "defaultReadOnly";
    private final static String PROP_DEFAULTTRANSACTIONISOLATION = "defaultTransactionIsolation";
    private final static String PROP_DEFAULTCATALOG = "defaultCatalog";
    private final static String PROP_DRIVERCLASSNAME = "driverClassName";
    private final static String PROP_MAXACTIVE = "maxActive";
    private final static String PROP_MAXIDLE = "maxIdle";
    private final static String PROP_MINIDLE = "minIdle";
    private final static String PROP_INITIALSIZE = "initialSize";
    private final static String PROP_MAXWAIT = "maxWait";
    private final static String PROP_TESTONBORROW = "testOnBorrow";
    private final static String PROP_TESTONRETURN = "testOnReturn";
    private final static String PROP_TIMEBETWEENEVICTIONRUNSMILLIS = "timeBetweenEvictionRunsMillis";
    private final static String PROP_NUMTESTSPEREVICTIONRUN = "numTestsPerEvictionRun";
    private final static String PROP_MINEVICTABLEIDLETIMEMILLIS = "minEvictableIdleTimeMillis";
    private final static String PROP_TESTWHILEIDLE = "testWhileIdle";
    private final static String PROP_PASSWORD = "password";
    private final static String PROP_URL = "url";
    private final static String PROP_USERNAME = "username";
    private final static String PROP_VALIDATIONQUERY = "validationQuery";
    private final static String PROP_POOLPREPAREDSTATEMENTS = "poolPreparedStatements";
    private final static String PROP_MAXOPENPREPAREDSTATEMENTS = "maxOpenPreparedStatements";
    private final static String PROP_CONNECTIONPROPERTIES = "connectionProperties";
    private final static String PROP_MAXAGE = "maxAge";
    
    protected ExtendedBasicDataSource createBasicDataSource(Properties properties) {
    	String value = properties.getProperty(PROP_MAXAGE);
    	int maxAge = (value == null || (value=value.trim()).length() == 0 ? 0 : Integer.parseInt(value));
    	if(maxAge > 0) {
    		ExpiringBasicDataSource ds = new ExpiringBasicDataSource();
    		ds.setMaxAge(maxAge);
    		return ds;
    	} else
    		return new ExtendedBasicDataSource();

    }
    protected ExtendedDataSource createDataSource(Properties properties) throws IOException {
    	ExtendedBasicDataSource dataSource = createBasicDataSource(properties);
        String value = null;

        value = properties.getProperty(PROP_DEFAULTAUTOCOMMIT);
        if (value != null) {
            dataSource.setDefaultAutoCommit(Boolean.valueOf(value).booleanValue());
        }

        value = properties.getProperty(PROP_DEFAULTREADONLY);
        if (value != null) {
            dataSource.setDefaultReadOnly(Boolean.valueOf(value).booleanValue());
        }

        value = properties.getProperty(PROP_DEFAULTTRANSACTIONISOLATION);
        if (value != null) {
            int level = -1; //PoolableConnectionFactory.UNKNOWN_TRANSACTIONISOLATION;
            if ("NONE".equalsIgnoreCase(value)) {
                level = Connection.TRANSACTION_NONE;
            }
            else if ("READ_COMMITTED".equalsIgnoreCase(value)) {
                level = Connection.TRANSACTION_READ_COMMITTED;
            }
            else if ("READ_UNCOMMITTED".equalsIgnoreCase(value)) {
                level = Connection.TRANSACTION_READ_UNCOMMITTED;
            }
            else if ("REPEATABLE_READ".equalsIgnoreCase(value)) {
                level = Connection.TRANSACTION_REPEATABLE_READ;
            }
            else if ("SERIALIZABLE".equalsIgnoreCase(value)) {
                level = Connection.TRANSACTION_SERIALIZABLE;
            }
            else {
                try {
                    level = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    System.err.println("Could not parse defaultTransactionIsolation: " + value);
                    System.err.println("WARNING: defaultTransactionIsolation not set");
                    System.err.println("using default value of database driver");
                    level = -1; //PoolableConnectionFactory.UNKNOWN_TRANSACTIONISOLATION;
                }
            }
            dataSource.setDefaultTransactionIsolation(level);
        }

        value = properties.getProperty(PROP_DEFAULTCATALOG);
        if (value != null) {
            dataSource.setDefaultCatalog(value);
        }

        value = properties.getProperty(PROP_DRIVERCLASSNAME);
        if (value != null) {
            dataSource.setDriverClassName(value);
        }

        value = properties.getProperty(PROP_MAXACTIVE);
        if (value != null) {
            dataSource.setMaxActive(Integer.parseInt(value));
        }

        value = properties.getProperty(PROP_MAXIDLE);
        if (value != null) {
            dataSource.setMaxIdle(Integer.parseInt(value));
        }

        value = properties.getProperty(PROP_MINIDLE);
        if (value != null) {
            dataSource.setMinIdle(Integer.parseInt(value));
        }

        value = properties.getProperty(PROP_INITIALSIZE);
        if (value != null) {
            dataSource.setInitialSize(Integer.parseInt(value));
        }

        value = properties.getProperty(PROP_MAXWAIT);
        if (value != null) {
            dataSource.setMaxWait(Long.parseLong(value));
        }

        value = properties.getProperty(PROP_TESTONBORROW);
        if (value != null) {
            dataSource.setTestOnBorrow(Boolean.valueOf(value).booleanValue());
        }

        value = properties.getProperty(PROP_TESTONRETURN);
        if (value != null) {
            dataSource.setTestOnReturn(Boolean.valueOf(value).booleanValue());
        }

        value = properties.getProperty(PROP_TIMEBETWEENEVICTIONRUNSMILLIS);
        if (value != null) {
            dataSource.setTimeBetweenEvictionRunsMillis(Long.parseLong(value));
        }

        value = properties.getProperty(PROP_NUMTESTSPEREVICTIONRUN);
        if (value != null) {
            dataSource.setNumTestsPerEvictionRun(Integer.parseInt(value));
        }

        value = properties.getProperty(PROP_MINEVICTABLEIDLETIMEMILLIS);
        if (value != null) {
            dataSource.setMinEvictableIdleTimeMillis(Long.parseLong(value));
        }

        value = properties.getProperty(PROP_TESTWHILEIDLE);
        if (value != null) {
            dataSource.setTestWhileIdle(Boolean.valueOf(value).booleanValue());
        }

        value = properties.getProperty(PROP_PASSWORD);
        if (value != null) {
            dataSource.setPassword(value);
        }

        value = properties.getProperty(PROP_URL);
        if (value != null) {
            dataSource.setUrl(value);
        }

        value = properties.getProperty(PROP_USERNAME);
        if (value != null) {
            dataSource.setUsername(value);
        }

        value = properties.getProperty(PROP_VALIDATIONQUERY);
        if (value != null) {
            dataSource.setValidationQuery(value);
        }
        /*
        value = properties.getProperty(PROP_ACCESSTOUNDERLYINGCONNECTIONALLOWED);
        if (value != null) {
            dataSource.setAccessToUnderlyingConnectionAllowed(Boolean.valueOf(value).booleanValue());
        }
        */
        dataSource.setAccessToUnderlyingConnectionAllowed(true);

        value = properties.getProperty(PROP_POOLPREPAREDSTATEMENTS);
        if (value != null) {
            dataSource.setPoolPreparedStatements(Boolean.valueOf(value).booleanValue());
        }

        value = properties.getProperty(PROP_MAXOPENPREPAREDSTATEMENTS);
        if (value != null) {
            dataSource.setMaxOpenPreparedStatements(Integer.parseInt(value));
        }

        value = properties.getProperty(PROP_CONNECTIONPROPERTIES);
        if (value != null) {
          Properties p = getProperties(value);
          Enumeration<?> e = p.propertyNames();
          while (e.hasMoreElements()) {
            String propertyName = (String) e.nextElement();
            dataSource.addConnectionProperty(propertyName, p.getProperty(propertyName));
          }
        }

        // Return the configured DataSource instance
        return dataSource;
    }
    /**
     * <p>Parse properties from the string. Format of the string must be [propertyName=property;]*<p>
     * @param propText
     * @return Properties
     * @throws IOException
     * @throws Exception
     */
    protected static Properties getProperties(String propText) throws IOException {
      Properties p = new Properties();
      if (propText != null) {
        p.load(new ByteArrayInputStream(propText.replace(';', '\n').getBytes()));
      }
      return p;
    }
    /**
     * @throws DataSourceNotFoundException
     * @see simple.db.AbstractDataSourceFactory#getDriverClassName(java.lang.String)
     */
    @Override
    protected String getDriverClassName(String dataSourceName) throws DataSourceNotFoundException {
    	return getDataSourceProperties(dataSourceName).getProperty("driverClassName");
    }

    protected Map<String,Properties> getSubProperties() {
        if(subProperties == null) {
            Map<String,Properties> dsn = new HashMap<String,Properties>();
            //put into buckets by property prefix
            for(Object key : properties.keySet()) {
                if(key instanceof String) {
                    String[] parsed = parseName((String)key);
                    String name = parsed[0];
                    Properties props = dsn.get(name);
                    if(props == null) {
                        props = new Properties();
                        dsn.put(name, props);
                    }
                    props.put(translateToNew(parsed[1]), properties.get(key));
                }
            }
            subProperties = dsn;
        }
        return subProperties;
    }

    public String[] getDataSourceNames() {
        if(dataSourceNames == null) {
            Set<String> names = new HashSet<String>();
            Map<String,Properties> dsn = getSubProperties();
            for(Map.Entry<String,Properties> entry : dsn.entrySet()) {
                if(entry.getValue().keySet().containsAll(EXISTS_TEST)) {
                    names.add(entry.getKey());
                }
            }
            dataSourceNames = names.toArray(new String[names.size()]);
        }
        return dataSourceNames;
    }

    protected static String translateToOld(String propertyName) {
        for(String[] outer : PROPERTY_TRANSLATION) {
            if(outer[0].equals(propertyName)) return outer[1];
        }
        return propertyName;
    }

    protected static String translateToNew(String propertyName) {
        for(String[] outer : PROPERTY_TRANSLATION) {
            if(outer[1].equalsIgnoreCase(propertyName)) return outer[0];
        }
        return propertyName;
    }
}
