/*
 * DBCPUnwrap.java
 *
 * Created on May 13, 2003, 9:33 AM
 */

package simple.db.dbcp;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.tomcat.dbcp.dbcp.DelegatingConnection;
import org.apache.tomcat.dbcp.dbcp.PoolableConnection;

/**
 *
 * @author  Brian S. Krug
 */
public class TomcatDBCPUnwrap extends simple.db.DBUnwrap {
    //private static final simple.io.Log log = simple.io.Log.getLog();
    /** Creates a new instance of TomcatDBCPUnwrap */
    public TomcatDBCPUnwrap() {
    }

    @Override
	protected Connection getDelegateConnection(Connection conn) {
       while(conn instanceof DelegatingConnection) {
           Connection tmp = ((DelegatingConnection)conn).getDelegate();
           if(tmp == null) return conn; //in case DelegatingConnection does not allow us to see underlying connection
           else conn = tmp;
       }
       //if(log.isDebugEnabled()) log.debug("Connection = " + conn + " (" + conn.getClass().getName() + ")");
       return conn;
    }

    @Override
    public void closeConnection(Connection conn) throws SQLException {
        if(conn instanceof PoolableConnection) {
            ((PoolableConnection)conn).reallyClose();
        } else {
           super.closeConnection(conn);
        }
    }
}
