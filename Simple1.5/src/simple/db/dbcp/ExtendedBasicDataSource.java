/**
 * 
 */
package simple.db.dbcp;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import simple.db.ExtendedDataSource;


public class ExtendedBasicDataSource extends BasicDataSource implements ExtendedDataSource {
	/**
	 * @see java.sql.Wrapper#isWrapperFor(java.lang.Class)
	 */
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		if(iface.isAssignableFrom(this.getClass()))
			return true;
		return false;
	}
	/**
	 * @see java.sql.Wrapper#unwrap(java.lang.Class)
	 */
	public <T> T unwrap(Class<T> iface) throws SQLException {
		if(iface.isAssignableFrom(this.getClass()))
			return iface.cast(this);
		throw new SQLException("Could not unwrap to " + iface.getName());
	}
	
	public Connection getConnection() throws SQLException {
		boolean okay = false;
		final DataSource ds;
		try {
			ds = createDataSource();
			okay = true;
		} finally {
			if(!okay)
				close(); // this protects us from the multiple evictor issue
		}
        return ds.getConnection();
    }

	public boolean isAvailable() {
		try {
			createDataSource();
		} catch(SQLException e) {
			return false;
		}
		return true;
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return null;
	}
}