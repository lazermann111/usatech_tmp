/**
 * 
 */
package simple.db.dbcp;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.pool.KeyedObjectPoolFactory;
import org.apache.commons.pool.ObjectPool;

import simple.io.Log;

public class ExpiringPoolableConnectionFactory extends ExtendedPoolableConnectionFactory {
	private static final Log log = Log.getLog();
	protected final Map<Object, Long> creationTimes = new HashMap<Object, Long>();
	protected long maxAge;
	/**
	 * @param connFactory
	 * @param pool
	 * @param stmtPoolFactory
	 * @param validationQuery
	 * @param defaultReadOnly
	 * @param defaultAutoCommit
	 * @param defaultTransactionIsolation
	 */
	public ExpiringPoolableConnectionFactory(ConnectionFactory connFactory, ObjectPool pool, KeyedObjectPoolFactory stmtPoolFactory, String validationQuery, boolean defaultReadOnly,
			boolean defaultAutoCommit, int defaultTransactionIsolation, long maxAge) {
		super(connFactory, pool, stmtPoolFactory, validationQuery, defaultReadOnly, defaultAutoCommit, defaultTransactionIsolation);
		setMaxAge(maxAge);
	}
	/**
	 * @see org.apache.commons.dbcp.PoolableConnectionFactory#makeObject()
	 */
	@Override
	public synchronized Object makeObject() throws Exception {
		Object obj = super.makeObject();
		creationTimes.put(obj, System.currentTimeMillis());
		return obj;
	}
	/**
	 * @see org.apache.commons.dbcp.PoolableConnectionFactory#validateObject(java.lang.Object)
	 */
	@Override
	public boolean validateObject(Object obj) {
		Long createTime = creationTimes.get(obj);
		if(createTime != null && getMaxAge() > 0 && createTime < System.currentTimeMillis() - getMaxAge()) {
			if(log.isInfoEnabled())
				log.info("Expiring object '" + obj + "' created at " + new Date(createTime));
			return false;
		}
		return super.validateObject(obj);
	}
	/**
	 * @see org.apache.commons.dbcp.PoolableConnectionFactory#destroyObject(java.lang.Object)
	 */
	@Override
	public void destroyObject(Object obj) throws Exception {
		creationTimes.remove(obj);
		super.destroyObject(obj);
	}
	public long getMaxAge() {
		return maxAge;
	}
	public void setMaxAge(long maxAge) {
		this.maxAge = maxAge;
	}
}