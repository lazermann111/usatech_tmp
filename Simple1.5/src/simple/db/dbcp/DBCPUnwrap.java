/*
 * DBCPUnwrap.java
 *
 * Created on May 13, 2003, 9:33 AM
 */

package simple.db.dbcp;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp.DelegatingConnection;
import org.apache.commons.dbcp.PoolableConnection;

/**
 *
 * @author  Brian S. Krug
 */
public class DBCPUnwrap extends simple.db.DBUnwrap {
    private static final simple.io.Log log = simple.io.Log.getLog();
    /** Creates a new instance of DBCPUnwrap */
    public DBCPUnwrap() {
    }

    @Override
	protected Connection getDelegateConnection(Connection conn) {
        while(conn instanceof DelegatingConnection) conn = ((DelegatingConnection)conn).getDelegate();
        if(log.isDebugEnabled()) log.debug("Connection = " + conn + (conn != null ? " (" + conn.getClass().getName() + ")" : ""));
        return conn;
    }

    @Override
    public void closeConnection(Connection conn) throws SQLException {
        if(conn instanceof PoolableConnection) {
            ((PoolableConnection)conn).reallyClose();
        } else {
           super.closeConnection(conn);
        }
    }
}
