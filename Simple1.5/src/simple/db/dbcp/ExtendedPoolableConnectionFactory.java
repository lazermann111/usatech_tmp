/**
 * 
 */
package simple.db.dbcp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.pool.KeyedObjectPoolFactory;
import org.apache.commons.pool.ObjectPool;

import simple.io.Log;

public class ExtendedPoolableConnectionFactory extends PoolableConnectionFactory {
	private static final Log log = Log.getLog();
	/**
	 * @param connFactory
	 * @param pool
	 * @param stmtPoolFactory
	 * @param validationQuery
	 * @param defaultReadOnly
	 * @param defaultAutoCommit
	 * @param defaultTransactionIsolation
	 */
	public ExtendedPoolableConnectionFactory(ConnectionFactory connFactory, ObjectPool pool, KeyedObjectPoolFactory stmtPoolFactory, String validationQuery, boolean defaultReadOnly,
			boolean defaultAutoCommit, int defaultTransactionIsolation) {
		super(connFactory, pool, stmtPoolFactory, validationQuery, defaultReadOnly, defaultAutoCommit, defaultTransactionIsolation);
	}
	@Override
	public Object makeObject() throws Exception {
		Object o = super.makeObject();
		return o;
	}
	@Override
	public boolean validateObject(Object obj) {
        if(obj instanceof Connection) {
            try {
                validateConnection((Connection) obj);
                return true;
            } catch(Exception e) {
            	log.warn("Validate Connection failed on '" + obj + "': " + e.getMessage());
                return false;
            }
        } else {
            return false;
        }
    }
	/**
	 * @see org.apache.commons.dbcp.PoolableConnectionFactory#validateConnection(java.sql.Connection)
	 */
	@Override
	public void validateConnection(Connection conn) throws SQLException {
		String query = _validationQuery;
        if(conn.isClosed()) {
            throw new SQLException("validateConnection: connection closed");
        }
        if(query != null) {
        	Statement st = conn.createStatement();
			try {
				boolean hasResults = st.execute(query);
				if(hasResults) {
					ResultSet rs = st.getResultSet();
					try {
						if(!rs.next())
							 throw new SQLException("validationQuery didn't return a row");
					} finally {
						rs.close();
					}
				}
			} finally {
				st.close();
			}			
		}
	}
}