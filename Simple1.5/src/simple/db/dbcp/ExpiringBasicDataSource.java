/**
 * 
 */
package simple.db.dbcp;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.dbcp.SQLNestedException;
import org.apache.commons.pool.KeyedObjectPoolFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericKeyedObjectPool;
import org.apache.commons.pool.impl.GenericKeyedObjectPoolFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

public class ExpiringBasicDataSource extends ExtendedBasicDataSource {
    protected long maxAge;
	protected void log(String message) {
        if (logWriter != null) {
            logWriter.println(message);
        }
    }
    /**
	 * @see org.apache.commons.dbcp.BasicDataSource#createDataSource()
	 */
	@Override
	protected synchronized DataSource createDataSource() throws SQLException {
		// Return the pool if we have already created it
        if (dataSource != null) {
            return (dataSource);
        }

        // Load the JDBC driver class
        if (driverClassName != null) {
            try {
                Class.forName(driverClassName);
            } catch (Throwable t) {
                String message = "Cannot load JDBC driver class '" +
                    driverClassName + "'";
                logWriter.println(message);
                t.printStackTrace(logWriter);
                throw new SQLNestedException(message, t);
            }
        }

        // Create a JDBC driver instance
        Driver driver = null;
        try {
            driver = DriverManager.getDriver(url);
        } catch (Throwable t) {
            String message = "Cannot create JDBC driver of class '" +
                (driverClassName != null ? driverClassName : "") +
                "' for connect URL '" + url + "'";
            logWriter.println(message);
            t.printStackTrace(logWriter);
            throw new SQLNestedException(message, t);
        }

        // Can't test without a validationQuery
        if (validationQuery == null) {
            setTestOnBorrow(false);
            setTestOnReturn(false);
            setTestWhileIdle(false);
        }

        // Create an object pool to contain our active connections
        connectionPool = new GenericObjectPool();
        connectionPool.setMaxActive(maxActive);
        connectionPool.setMaxIdle(maxIdle);
        connectionPool.setMinIdle(minIdle);
        connectionPool.setMaxWait(maxWait);
        connectionPool.setTestOnBorrow(testOnBorrow);
        connectionPool.setTestOnReturn(testOnReturn);
        connectionPool.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        connectionPool.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
        connectionPool.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        connectionPool.setTestWhileIdle(testWhileIdle);

        // Set up statement pool, if desired
        GenericKeyedObjectPoolFactory statementPoolFactory = null;
        if (isPoolPreparedStatements()) {
            statementPoolFactory = new GenericKeyedObjectPoolFactory(null,
                        -1, // unlimited maxActive (per key)
                        GenericKeyedObjectPool.WHEN_EXHAUSTED_FAIL,
                        0, // maxWait
                        1, // maxIdle (per key)
                        maxOpenPreparedStatements);
        }

        // Set up the driver connection factory we will use
        if (username != null) {
            connectionProperties.put("user", username);
        } else {
            log("DBCP DataSource configured without a 'username'");
        }

        if (password != null) {
            connectionProperties.put("password", password);
        } else {
            log("DBCP DataSource configured without a 'password'");
        }

        DriverConnectionFactory driverConnectionFactory =
            new DriverConnectionFactory(driver, url, connectionProperties);

        // Set up the poolable connection factory we will use
        createPoolableConnectionFactory(driverConnectionFactory,
                                              connectionPool,
                                              statementPoolFactory,
                                              validationQuery,
                                              getDefaultReadOnly(),
                                              defaultAutoCommit,
                                              defaultTransactionIsolation,
                                              defaultCatalog,
                                              maxAge);

        // Create and return the pooling data source to manage the connections
        dataSource = new PoolingDataSource(connectionPool);
        ((PoolingDataSource) dataSource).setAccessToUnderlyingConnectionAllowed(isAccessToUnderlyingConnectionAllowed());
        dataSource.setLogWriter(logWriter);

        try {
            for (int i = 0 ; i < initialSize ; i++) {
                connectionPool.addObject();
            }
        } catch (Exception e) {
            throw new SQLNestedException("Error preloading the connection pool", e);
        }

        return dataSource;
	}
	protected PoolableConnectionFactory createPoolableConnectionFactory(ConnectionFactory driverConnectionFactory,
            ObjectPool connectionPool,
            KeyedObjectPoolFactory statementPoolFactory,
            String validationQuery,
            boolean defaultReadOnly,
            boolean defaultAutoCommit,
            int defaultTransactionIsolation,
            String defaultCatalog,
            long maxAge) throws SQLException {
    	PoolableConnectionFactory connectionFactory = null;
	    try {
	    	if(maxAge > 0) {
	    		connectionFactory = new ExpiringPoolableConnectionFactory(driverConnectionFactory,
                        connectionPool,
                        statementPoolFactory,
                        validationQuery,
                        defaultReadOnly,
                        defaultAutoCommit,
                        defaultTransactionIsolation,
                        maxAge);
	    	} else {
	    		connectionFactory = new ExtendedPoolableConnectionFactory(driverConnectionFactory,
	                                          connectionPool,
	                                          statementPoolFactory,
	                                          validationQuery,
	                                          defaultReadOnly,
	                                          defaultAutoCommit,
	                                          defaultTransactionIsolation);
	    	}
	        connectionFactory.setDefaultCatalog(defaultCatalog);
	        validateConnectionFactory(connectionFactory);
	    } catch (RuntimeException e) {
	        throw e;
	    } catch (Exception e) {
	        throw new SQLNestedException("Cannot create PoolableConnectionFactory (" + e.getMessage() + ")", e);
	    }
	    return connectionFactory;
	}
	protected void validateConnectionFactory(PoolableConnectionFactory connectionFactory) throws Exception {
        Connection conn = null;
        try {
            conn = (Connection) connectionFactory.makeObject();
            connectionFactory.activateObject(conn);
            connectionFactory.validateConnection(conn);
            connectionFactory.passivateObject(conn);
        }
        finally {
            connectionFactory.destroyObject(conn);
        }
    }
	public long getMaxAge() {
		return maxAge;
	}
	public void setMaxAge(long maxAge) {
		this.maxAge = maxAge;
	}
}