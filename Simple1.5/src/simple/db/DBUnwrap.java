/*
 * DBUnwrap.java
 *
 * Created on August 19, 2003, 12:28 PM
 */

package simple.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import simple.bean.ConvertException;
import simple.sql.SQLType;

/** Abstract implementation of DBHelper that unwraps a Connection object and then
 *  uses another DBHelper appropriate to the underlying connection to perform
 * each of the methods. Subclasses only need to implement the <CODE>getRealConnection(java.sql.Connection)</CODE>
 * method to make this work.
 *
 * @author  Brian S. Krug
 */
public abstract class DBUnwrap implements DBHelper {
    protected static DBHelper defaultHelper = new DefaultDBHelper();
    public static Connection getRealConnection(Connection original)  {
    	DBHelper helper = DataLayerMgr.getDBHelper(original);
		if(helper instanceof DBUnwrap)
			return getRealConnection(((DBUnwrap)helper).getDelegateConnection(original));
		else
			return original;
    }
    protected abstract Connection getDelegateConnection(Connection original) ;
    public DBHelper getRealDBHelper(ConnectionHolder connectionHolder) throws DataSourceNotFoundException {
    	DBHelper helper = DataLayerMgr.getDBHelper(connectionHolder.getConnectionClass());
    	if(helper == null)
    		return defaultHelper;
    	else
    		return helper;
    }

    public DBHelper getDelegate(Connection connection) {
        return DataLayerMgr.getDBHelper(connection);
    }

    public Object[] getValues(ResultSet rs, Column[] columns, boolean connected) throws SQLException, ConvertException {
        Connection connection = rs.getStatement().getConnection();
        Connection real = getRealConnection(connection);
        return (real == connection ? defaultHelper : getDelegate(real)).getValues(rs, columns, connected);
    }

    public void closeConnection(Connection conn) throws SQLException {
        Connection real = getRealConnection(conn);
        (real == conn ? defaultHelper : getDelegate(real)).closeConnection(conn);
    }

	public Object getValue(ResultSet rs, Column column, boolean connected) throws SQLException, ConvertException {
		Connection connection = rs.getStatement().getConnection();
        Connection real = getRealConnection(connection);
        return (real == connection ? defaultHelper : getDelegate(real)).getValue(rs, column, connected);
	}

	public SQLType translateSqlType(SQLType sqlType, String databaseTypeName, ConnectionHolder connectionHolder) throws SQLException, DataSourceNotFoundException {
		return getRealDBHelper(connectionHolder).translateSqlType(sqlType, databaseTypeName, connectionHolder);
	}

	public void setValue(ResultSet rs, Column column, Object value) throws SQLException, ConvertException {
		Connection connection = rs.getStatement().getConnection();
        Connection real = getRealConnection(connection);
        (real == connection ? defaultHelper : getDelegate(real)).setValue(rs, column, value);
	}

	public boolean reconnectNeeded(Connection conn, SQLException e) {
		Connection real = getRealConnection(conn);
        return (real == conn ? defaultHelper : getDelegate(real)).reconnectNeeded(real, e);
	}
	/**
	 * @see simple.db.DBHelper#getValue(java.sql.CallableStatement, int, simple.sql.SQLType)
	 */
	public Object getValue(CallableStatement callableStatement, int parameterIndex, SQLType sqlType) throws SQLException {
		Connection connection = callableStatement.getConnection();
        Connection real = getRealConnection(connection);
        return (real == connection ? defaultHelper : getDelegate(real)).getValue(callableStatement, parameterIndex, sqlType);
	}
	/**
	 * @throws ParameterException 
	 * @see simple.db.DBHelper#registerOutParameter(java.sql.CallableStatement, int, simple.sql.SQLType)
	 */
	public void registerOutParameter(CallableStatement callableStatement, int parameterIndex, String parameterName, SQLType sqlType) throws SQLException, ParameterException {
		Connection connection = callableStatement.getConnection();
        Connection real = getRealConnection(connection);
        (real == connection ? defaultHelper : getDelegate(real)).registerOutParameter(callableStatement, parameterIndex, parameterName, sqlType);
	}
	public void setInParameter(PreparedStatement preparedStatement, int parameterIndex, String parameterName, Object value, SQLType sqlType) throws SQLException, ParameterException {
		Connection connection = preparedStatement.getConnection();
        Connection real = getRealConnection(connection);
        (real == connection ? defaultHelper : getDelegate(real)).setInParameter(preparedStatement, parameterIndex, parameterName, value, sqlType);
	}
	public CallableStatement setQueryTimeout(CallableStatement callableStatement, double timeoutSeconds) throws SQLException {
		Connection connection = callableStatement.getConnection();
        Connection real = getRealConnection(connection);
        return (real == connection ? defaultHelper : getDelegate(real)).setQueryTimeout(callableStatement, timeoutSeconds);
	}
	public String getConnectionIdentifier(Connection conn) {
		Connection real = getRealConnection(conn);
        return (real == conn ? defaultHelper : getDelegate(real)).getConnectionIdentifier(real);
	}

	@Override
	public String getSortedSql(Connection conn, String originalSql, int[] sortBy) {
		Connection real = getRealConnection(conn);
		return (real == conn ? defaultHelper : getDelegate(real)).getSortedSql(real, originalSql, sortBy);
	}

	@Override
	public void setSortedRowLimits(CallableStatement callableStatement, int rowOffset, int rowLimit) throws SQLException {
		Connection conn = callableStatement.getConnection();
		Connection real = getRealConnection(conn);
		(real == conn ? defaultHelper : getDelegate(real)).setSortedRowLimits(callableStatement, rowOffset, rowLimit);
	}
}
