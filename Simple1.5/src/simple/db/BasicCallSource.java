/*
 * BasicCallSource.java
 *
 * Created on January 20, 2004, 3:31 PM
 */

package simple.db;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import simple.io.Log;

/** A basic implementation of the CallSource interface that holds Call objects in a Map and
 * returns them from there.
 *
 * @author  Brian S. Krug
 */
public class BasicCallSource implements CallSource {
	private static final Log log = Log.getLog();
    protected Map<String,Call> callMap;

    /** Creates a new instance of BasicCallSource */
    public BasicCallSource() {
    	this(new HashMap<String,Call>());
    }

    public BasicCallSource(Map<String,Call> callMap) {
    	this.callMap = callMap;
    }
    /** Returns a Call object for the specified id value
     *  @param id The unique id for the Call
     *  @returns The requested Call object
     *  @throws CallNotFoundException If a Call can not be found for the specified id
     **/
    public Call getCall(String id) throws CallNotFoundException {
        Call c = callMap.get(id);
        if(c == null) throw new CallNotFoundException(id);
        return c;
    }

    /** Adds a call to the CallSource registry.
     * @param id The id of the call
     * @param c The call
     */
    public void addCall(String id, Call c) {
		Call old = callMap.put(id, c);
		if(old != null) {
			log.error("Call '" + id + "' was already configured. Old definition has been overwritten");
			old.destroy();
		}
    }

    /** Removes a call from the CallSource registry.
     * @return The call that was removed
     * @param id The id of the call to remove
     */
    public Call removeCall(String id) {
		Call old = callMap.remove(id);
		if(old != null)
			old.destroy();
		return old;
    }

    /** Adds all the calls in another BasicCallSource to this one.
     * @param other The BasicCallSource containing the calls to add
     */
    public void addAllCalls(BasicCallSource other) {
		for(Map.Entry<String, Call> entry : other.callMap.entrySet())
			addCall(entry.getKey(), entry.getValue());
    }

    /** Removes all calls from the CallSource registry.
     */
    public void clearCalls() {
		for(Iterator<Call> iter = callMap.values().iterator(); iter.hasNext();) {
			iter.next().destroy();
			iter.remove();
		}
    }

    /** Returns a Set of all the id values for which a Call object is defined
     *  @returns The Set of all id values
     **/
    public java.util.Set<String> getCallIds() {
        return callMap.keySet();
    }
}
