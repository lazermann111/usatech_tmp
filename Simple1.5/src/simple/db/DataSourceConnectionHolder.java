package simple.db;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceConnectionHolder extends AbstractConnectionHolder {
	protected final DataLayer dataLayer;
	protected final String dataSourceName;
	public DataSourceConnectionHolder(final DataLayer dataLayer, final String dataSourceName) {
		super();
		this.dataLayer = dataLayer;
		this.dataSourceName = dataSourceName;
	}
	@Override
	protected Connection createConnection() throws SQLException, DataSourceNotFoundException {
		return dataLayer.getConnection(dataSourceName);
	}
	/**
	 * @throws DataSourceNotFoundException
	 * @see simple.db.ConnectionHolder#getConnectionClass()
	 */
	public Class<? extends Connection> getConnectionClass() throws DataSourceNotFoundException {
		return dataLayer.getDataSourceFactory().getConnectionClass(dataSourceName);
	}
}
