/*
 * Parameter.java
 *
 * Created on December 24, 2002, 1:15 PM
 */

package simple.db;

import simple.bean.ConvertUtils;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;

/**
 * Represents a non-cursor argument for a Call
 *
 * @author  Brian S. Krug
 */
public class Parameter implements Argument {

    /** Holds value of property in. */
    private boolean in = true;
    
    /** Holds value of property out. */
    private boolean out = false;
    
    private SQLType sqlType;
    
    /** Holds value of property propertyName. */
    private String propertyName;
    
    private Object defaultValue;
    
    private boolean required = true;
    
    /** Creates new Parameter */
    public Parameter() {
    }

	public Parameter(String propertyName, SQLType sqlType, boolean required, boolean in, boolean out) {
		this();
		setPropertyName(propertyName);
		setSqlType(sqlType);
		setRequired(required);
		setIn(in);
		setOut(out);
	}
	@Override
	public Parameter clone() {
		Parameter p;
		try {
			p = (Parameter) super.clone();
		} catch(CloneNotSupportedException e) {
			return null;
		}
		if(p.sqlType != null)
			p.sqlType = p.sqlType.clone();
		return p;
	}
    /** Getter for property in.
     * @return Value of property in.
     */
    public boolean isIn() {
        return in;
    }
    
    /** Setter for property in.
     * @param in New value of property in.
     */
    public void setIn(boolean in) {
        this.in = in;
    }
    
    /** Getter for property out.
     * @return Value of property out.
     */
    public boolean isOut() {
        return out;
    }
    
    /** Setter for property out.
     * @param out New value of property out.
     */
    public void setOut(boolean out) {
        this.out = out;
    }
        
    /** Getter for property propertyName.
     * @return Value of property propertyName.
     */
    public String getPropertyName() {
        return propertyName;
    }
    
    /** Setter for property propertyName.
     * @param propertyName New value of property propertyName.
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
        
    public String toString() {
        return "Parameter: " + (getPropertyName() == null ? "<UNNAMED>" : getPropertyName()) + " "
            + getSqlType() + " " + (isIn() ? "IN " : "") + (isOut() ? "OUT " : "");
    }
    public Object getDefaultValue() {
        return defaultValue;
    }
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }
    public boolean isRequired() {
        return required;
    }
    public void setRequired(boolean required) {
        this.required = required;
    }
    
    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof Parameter) {
    		Parameter p = (Parameter)obj;
    		return in == p.in 
    			&& out == p.out
    			&& ConvertUtils.areEqual(sqlType, p.sqlType)
    			&& ConvertUtils.areEqual(propertyName, p.propertyName);
    	}
    	return false;
    }

	@Override
	public int hashCode() {
		return (sqlType == null ? 0 : sqlType.hashCode()) + (in ? 331 : 0) + (out ? 45678 : 0) + (propertyName == null ? 0 : propertyName.hashCode());
	}

	public SQLType getSqlType() {
		return sqlType;
	}

	public void setSqlType(SQLType sqlType) {
		this.sqlType = sqlType;
	}

	public Class<? extends Object> getJavaType() {
		return getSqlType() == null ? null : SQLTypeUtils.getJavaType(getSqlType());
	} 
}
