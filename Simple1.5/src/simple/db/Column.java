/*
 * Column.java
 *
 * Created on December 30, 2002, 1:30 PM
 */

package simple.db;

import simple.bean.ConvertUtils;


/**
 * Represents a single column of a ResultSet returned from a SQL statement
 *
 * @author  Brian S. Krug
 *
 */
public class Column implements Cloneable {
    protected int index = -1;
    protected String name = null;
	protected Column uniqueColumn = null;
    protected String propertyName;
    protected String format;
    protected int sqlType;
    protected Class<?> columnClass = Object.class;

    /** Creates new Column */
    public Column() {
    }

    @Override
	public Column clone() {
        try {
            return (Column)super.clone();
        } catch(CloneNotSupportedException e) {
            throw new IllegalStateException("This should never occur", e);
        }
    }

    /** Getter for property index.  Index is 1-based (like ResultSet)
     * @return Value of property index.
     */
    public int getIndex() {
        return index;
    }

    /** Setter for property index. Index is 1-based (like ResultSet)
     * @param index New value of property index.
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /** Getter for property name.
     * @return Value of property name.
     */
    public String getName() {
        return name;
    }

    /** Setter for property name.
     * @param name New value of property name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /** Getter for property propertyName.
     * @return Value of property propertyName.
     */
    public String getPropertyName() {
        return propertyName;
    }

    /** Setter for property propertyName.
     * @param propertyName New value of property propertyName.
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /** Getter for property format.
     * @return Value of property format.
     */
    public String getFormat() {
        return format;
    }

    /** Setter for property format.
     * @param format New value of property format.
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * @return Returns the sqlType.
     */
    public int getSqlType() {
        return sqlType;
    }

    /**
     * @param sqlType The sqlType to set.
     */
    public void setSqlType(int sqlType) {
        this.sqlType = sqlType;
    }

    /**
     * @return Returns the columnClass.
     */
    public Class<?> getColumnClass() {
        return columnClass;
    }

    /**
     * @param columnClass The columnClass to set.
     */
    public void setColumnClass(Class<?> columnClass) {
        this.columnClass = columnClass;
    }

    @Override
	public String toString() {
        return getPropertyName() + " (" + (getIndex() > 0 ? "ID=" + getIndex() : "Name=" + getName())
            + "; Class=" + getColumnClass() + "; SQLType=" + getSqlType() + ")";
    }
    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof Column) {
    		Column c = (Column)obj;
    		return index == c.index
    			&& sqlType == c.sqlType
    			&& ConvertUtils.areEqual(columnClass, c.columnClass)
    			&& ConvertUtils.areEqual(propertyName, c.propertyName)
    			&& ConvertUtils.areEqual(format, c.format)
    			&& ConvertUtils.areEqual(name, c.name);
    	}
    	return false;
    }

	@Override
	public int hashCode() {
		return index + sqlType + (propertyName == null ? 0 : propertyName.hashCode())
			+ (name == null ? 0 : name.hashCode()) + + (format == null ? 0 : format.hashCode())
			+ (columnClass == null ? 0 : columnClass.hashCode());
	}

	public Column getUniqueColumn() {
		return uniqueColumn;
	}

	public void setUniqueColumn(Column uniqueColumn) {
		this.uniqueColumn = uniqueColumn;
	}
}
