/*
 * DerivedColumn.java
 *
 * Created on February 26, 2003, 12:44 PM
 */

package simple.db;


/**
 * An extension of the Column class that converts the value of the column immediately.
 *
 * @author  Brian S. Krug
 */
public class ConvertedColumn extends Column {
	protected Class<?> toClass;

    /** Creates a new instance of DerivedColumn */
    public ConvertedColumn() {
    }

	public Class<?> getToClass() {
		return toClass;
	}

	public void setToClass(Class<?> toClass) {
		this.toClass = toClass;
	}

}
