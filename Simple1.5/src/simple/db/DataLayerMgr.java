/*
 * DataLayerMgr.java
 *
 * Created on December 24, 2002, 11:34 AM
 */

package simple.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import simple.db.Call.BatchUpdate;
import simple.db.CursorTransform.Hints;
import simple.db.DataLayer.RetryDecision;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.sql.SQLType;

/** This class is simply a pass-through for a single global instance of DataLayer. It
 *  provides static methods for most methods in DataLayer and passes the calls to
 *  the DataLayer instance. It also manages the DBHelpers.
 *
 * @author  Brian S. Krug
 */
public class DataLayerMgr {
    private static Log log = Log.getLog();
    protected static DBHelper defaultHelper = new DefaultDBHelper();
    protected static Map<Object, DBHelper> helpers = new ConcurrentHashMap<Object, DBHelper>();
    protected static DataLayer globalDataLayer = null;
    protected static final Map<Class<? extends Driver>,Class<? extends Connection>> driverConnectionClasses = new HashMap<Class<? extends Driver>,Class<? extends Connection>>();

    static { //let's load those dbhelper we know about
        attemptRegisterDBHelper("org.apache.commons.dbcp.DelegatingConnection","simple.db.dbcp.DBCPUnwrap");
        attemptRegisterDBHelper("org.apache.tomcat.dbcp.dbcp.DelegatingConnection","simple.db.dbcp.TomcatDBCPUnwrap");
        attemptRegisterDBHelper("simple.db.DelegatingConnection","simple.db.helpers.DelegatingUnwrap");
        attemptRegisterDBHelper("oracle.jdbc.OracleConnection","simple.db.helpers.OracleHelper");
        attemptRegisterDBHelper("org.apache.derby.client.am.Connection","simple.db.helpers.DerbyHelper");
        attemptRegisterDBHelper("org.apache.derby.impl.jdbc.EmbedConnection","simple.db.helpers.DerbyHelper");
        //attemptRegisterDBHelper("simple.db.DiagnosticDataSource$DiagnosticConnection","simple.db.helpers.DiagnosticUnwrap");
        attemptRegisterDBHelper("org.postgresql.core.BaseConnection","simple.db.helpers.PostgresHelper");
        attemptRegisterDriver("org.apache.derby.jdbc.ClientDriver", "org.apache.derby.client.net.NetConnection");
        attemptRegisterDriver("org.apache.derby.jdbc.EmbeddedDriver", "org.apache.derby.impl.jdbc.EmbedConnection");
        attemptRegisterDriver("oracle.jdbc.driver.OracleDriver", "oracle.jdbc.OracleConnection");
        //attemptRegisterDriver("org.postgresql.Driver", "org.postgresql.jdbc3.Jdbc3Connection");
        attemptRegisterDriver("org.postgresql.Driver", "org.postgresql.core.BaseConnection");
        attemptRegisterDriver("org.continuent.sequoia.driver.Driver", "org.continuent.sequoia.driver.Connection");
    }
    /** Creates a new DataLayerMgr */
    public DataLayerMgr() {
    }

    protected static void attemptRegisterDBHelper(String connClassName, String dbHelperClassName) {
        try {
            registerDBHelper(Class.forName(connClassName), (DBHelper)Class.forName(dbHelperClassName).newInstance());
        } catch(ClassCastException e) {
            log.debug("Could not register DBHelper '" + dbHelperClassName + "' for '" + connClassName + "' because " + e.getClass().getName() + ": " + e.getMessage(),e);
        } catch (ClassNotFoundException e) {
            log.debug("Could not register DBHelper '" + dbHelperClassName + "' for '" + connClassName + "' because " + e.getClass().getName() + ": " + e.getMessage());
        } catch (InstantiationException e) {
            log.debug("Could not register DBHelper '" + dbHelperClassName + "' for '" + connClassName + "' because " + e.getClass().getName() + ": " + e.getMessage(), e);
        } catch (IllegalAccessException e) {
            log.debug("Could not register DBHelper '" + dbHelperClassName + "' for '" + connClassName + "' because " + e.getClass().getName() + ": " + e.getMessage(), e);
        }
    }

    public static boolean attemptRegisterDriver(String driverClassName, String connectionClassName) {
    	try {
			driverConnectionClasses.put(Class.forName(driverClassName).asSubclass(Driver.class), Class.forName(connectionClassName).asSubclass(Connection.class));
		} catch(ClassNotFoundException e) {
			return false;
		} catch(ClassCastException e) {
			log.debug("Invalid driver or connection class", e);
			return false;
		}
		return true;
    }

    /**
	 * @param checkDriver
	 * @return
	 */
	public static Class<? extends Connection> getConnectionClassFromDriver(Class<? extends Driver> driver) {
		return driverConnectionClasses.get(driver);
	}

    /** Returns the singleton global DataLayer object
     * @return The default instance of DataLayer
     */
    public static DataLayer getGlobalDataLayer() {
        if(globalDataLayer == null) globalDataLayer = new DataLayer();
        return globalDataLayer;
    }

    /** Sets the DataSourceFactory for all calls on the global DataLayer.
     * @param dsf The dataSourceFactory to use
     */
    public static void setDataSourceFactory(DataSourceFactory dsf) {
        getGlobalDataLayer().setDataSourceFactory(dsf);
    }

    /** Retrieves the DataSourceFactory for all global DataLayer calls. Creates a new
     * <CODE>JNDIDataSourceFactory</CODE> based at "java:/comp/env/jdbc" if no DataSourceFactory
     * exists. If that fails, it creates a new <CODE>BasicDataSourceFactory</CODE>.
     * @return The current DataSourceFactory instance
     */
    public static DataSourceFactory getDataSourceFactory() {
        return getGlobalDataLayer().getDataSourceFactory();
    }

    /** Returns the connection from the data source factory that matches the given name.
     * @return A connection from the DataSource
     * @param dataSourceName The name of the DataSource
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Connection getConnection(String dataSourceName) throws SQLException, DataLayerException {
        return getGlobalDataLayer().getConnection(dataSourceName);
    }

	public static Connection getConnection(String dataSourceName, Boolean autoCommit) throws SQLException, DataLayerException {
		return getGlobalDataLayer().getConnection(dataSourceName, autoCommit);
	}

    /** Returns the connection from the data source factory for the specified call.
     * @return The connection from the appropriate DataSource
     * @param id The call id
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Connection getConnectionForCall(String id) throws SQLException, DataLayerException {
        return getGlobalDataLayer().getConnectionForCall(id);
    }

	public static Connection getConnectionForCall(String id, Boolean autoCommit) throws SQLException, DataLayerException {
		return getGlobalDataLayer().getConnectionForCall(id, autoCommit);
	}

    /** Returns the DBHelper of the innermost Connection's class (unwraps any connection whose
     *  DBHelper extends DBUnwrap.
     * @return The DBHelper for this class of Connection
     * @param conn The connnection for which you want the a DBHelper of the innermost connection
     */
    public static DBHelper getInnermostDBHelper(Connection conn) {
        conn = DBUnwrap.getRealConnection(conn);
        return getDBHelper(conn);
    }

    /** Returns the DBHelper for the given Connection class. If no DBHelper is
     *  registered for the specified Connection class then the default DBHelper
     *  is returned.
     * @return The DBHelper for this class of Connection
     * @param conn The connnection for which you want a DBHelper
     */
    public static DBHelper getDBHelper(Connection conn) {
    	if(conn == null) return defaultHelper;
    	DBHelper helper = getDBHelper(conn.getClass());
		if(helper == null) helper = defaultHelper;
	    log.debug("Using DBHelper " + helper + " for connection class '" + conn.getClass().getName() + "'");
        return helper;
    }

    /** Returns the DBHelper for the given Connection class. If no DBHelper is
     *  registered for the specified Connection class then null
     *  is returned.
     * @return The DBHelper for this class of Connection or null if none is found
     * @param connClass The class of the connnection for which you want a DBHelper
     */
    public static DBHelper getDBHelper(Class<?> connClass) {
    	if(connClass == null) return defaultHelper;
    	DBHelper helper = helpers.get(connClass);
		if(helper == null) {
            //check super classes
            for(Class<?> superClass = connClass.getSuperclass(); superClass != null && !superClass.equals(Object.class); superClass = superClass.getSuperclass()) {
                helper = helpers.get(superClass);
                if(helper != null) {
                    helpers.put(connClass, helper);
                    return helper;
                }
            }
            //check interfaces
            for(Class<?> checkClass = connClass; checkClass != null && !checkClass.equals(Object.class); checkClass = checkClass.getSuperclass()) {
                Class<?>[] interfaces = checkClass.getInterfaces();
                for(int i = 0; i < interfaces.length; i++) {
                    helper = getDBHelper(interfaces[i]);
                    if(helper != null) {
                        helpers.put(connClass, helper);
                        return helper;
                    }
                }
            }
            return null;
        } else {
            return helper;
        }
    }

    /** Returns the DBHelper for the given data source name. If no DBHelper is
     *  registered for the specified data source name then the default DBHelper
     *  is returned.
     * @return The DBHelper for this DataSource name
     * @param dataSourceName The DataSource name for which you want a DBHelper
     */
    public static DBHelper getDBHelper(String dataSourceName) {
        DBHelper helper = helpers.get(dataSourceName);
        if(helper == null) {
            log.debug("Could not find a helper for data source '" + dataSourceName + "'; returning default helper");
            return defaultHelper;
        } else {
            return helper;
        }
    }

    /** Registers a DBHelper object for a given data source name.
     * @param dataSourceName The DataSource name
     * @param helper The DBHelper to use with the specified DataSource name
     */
    public static void registerDBHelper(String dataSourceName, DBHelper helper) {
        helpers.put(dataSourceName, helper);
        log.debug("Registered DB Helper for data source '" + dataSourceName + "'");
    }

    /** Registers a DBHelper object for a given Connection class.
     * @param connClass The class of the Connection
     * @param helper The DBHelper to use with the specified DataSource name
     */
    public static void registerDBHelper(Class<?> connClass, DBHelper helper) {
        if(helper == null) helpers.remove(connClass);
        else helpers.put(connClass, helper);
        log.debug("Registered DB Helper for connection class '" + connClass.getName() + "'");
    }

    /** Unregisters a DBHelper object for a given data source name.
     * @param dataSourceName The DataSource name
     */
    public static void deregisterDBHelper(String dataSourceName) {
        helpers.remove(dataSourceName);
    }

    /** Creates a BatchUpdate object that can be used to update the database in efficient batches
     * @param id The call id
     * @param concurrent If the BatchUpdate object should support add to it and executing it at the same time. Even if
     * this is set to true, only one thread may invoke the executeBatch() method at a time. But it does allow multiple concurrent
     * invocations of addBatch().
     * @param overageAllowed A factor that determines how many additional batches the executeBatch() method will pick up that were added AFTER
     * the method was called.
     * @return The BatchUpdate object
     * @throws ParameterException
     * @throws CallNotFoundException
     */
    public static BatchUpdate createBatchUpdate(String id, boolean concurrent, float overageAllowed) throws ParameterException, CallNotFoundException {
    	return getGlobalDataLayer().createBatchUpdate(id, concurrent, overageAllowed);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param id The call id
     * @param bean The bean from which parameters should be extracted and into which parameters
     * should be saved (if the bean is not an Object array)
     * @param commit Whether to issue a commit after the update executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public static int executeUpdate(String id, Object bean, boolean commit) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeUpdate(id, bean, commit);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param conn The connection to use
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public static int executeUpdate(Connection conn, String id, Object bean) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeUpdate(conn, id, bean);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Object[] executeCall(String id, Object bean, boolean commit) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeCall(id, bean, commit);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param conn The connection to use
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Object[] executeCall(Connection conn, String id, Object bean) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeCall(conn, id, bean);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Results executeQuery(String id, Object bean) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeQuery(id, bean);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Results executeQuery(String id, Object bean, boolean commit) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeQuery(id, bean, commit);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param conn The connection to use
     * @param id The call id
     * @param bean The object from which parameters should be extracted and into which parameters
     * should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Results executeQuery(Connection conn, String id, Object bean) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeQuery(conn, id, bean);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the update executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public static int executeUpdate(String id, Object input, Object output, boolean commit) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeUpdate(id, input, output, commit);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  the update count
     * @return The update count
     * @param conn The connection to use
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public static int executeUpdate(Connection conn, String id, Object input, Object output) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeUpdate(conn, id, input, output);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Object[] executeCall(String id, Object input, Object output, boolean commit) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeCall(id, input, output, commit);
    }

	public static Object[] executeCall(String id, Object input, Object output, boolean commit, RetryDecision retry) throws SQLException, DataLayerException {
		return getGlobalDataLayer().executeCall(id, input, output, commit, retry);
	}

    /** Forwards this data request to the appropriate Call Object and returns
     *  an array of output parameters
     * @return An array of Objects representing the OUT parameters of the call. The first element in
     * the array will be either a <CODE>simple.results.Results</CODE> object
     * (for a SELECT statement) or the update count (otherwise)
     * @param conn The connection to use
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Object[] executeCall(Connection conn, String id, Object input, Object output) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeCall(conn, id, input, output);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Results executeQuery(String id, Object input, Object output) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeQuery(id, input, output);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @param commit Whether to issue a commit after the call executes
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Results executeQuery(String id, Object input, Object output, boolean commit) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeQuery(id, input, output, commit);
    }

    /** Forwards this data request to the appropriate Call Object and returns
     *  a Results Object
     * @return A <CODE>simple.results.Results</CODE> object representing the data
     * retrieved by the call
     * @param conn The connection to use
     * @param id The call id
     * @param input The object from which parameters should be extracted
     * @param output The object into which parameters should be saved (if the object is not an Object array)
     * @throws SQLException
     * @throws DataLayerException
     */
    public static Results executeQuery(Connection conn, String id, Object input, Object output) throws SQLException, DataLayerException {
        return getGlobalDataLayer().executeQuery(conn, id, input, output);
    }

    /** Adds a call to the global DataLayer registry.
     * @param id The id of the call
     * @param c The call
     *//*
    public static void addCall(String id, Call c) {
        getGlobalDataLayer().addCall(id, c);
    }
    */
    /** Removes a call from the global DataLayer registry.
     * @return The call that was removed
     * @param id The id of the call to remove
     *//*
    public static Call removeCall(String id) {
        return getGlobalDataLayer().removeCall(id);
    }
    */
    /** A utility test method to output Database Type Information
     * @param connection The connection for which to print type info
     */
    public static void printTypeInfo(Connection connection) {
        try {
            java.sql.ResultSet rs = connection.getMetaData().getTypeInfo();
            java.sql.ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            StringBuilder sb = new StringBuilder();
            for(int i = 1; i <= columns; i++) {
                if(i > 1) sb.append(",");
                sb.append(md.getColumnName(i));
            }
            sb.append("\n");
            while(rs.next()) {
                for(int i = 1; i <= columns; i++) {
                    if(i > 1) sb.append(",");
                    sb.append(rs.getObject(i));
                }
                sb.append("\n");
            }
            System.out.println("--- TYPE INFO ---");
            System.out.println(sb.toString());
            System.out.println("--- END TYPE INFO ---");
        } catch(Throwable e) {
            e.printStackTrace();
        }
    }

	public static Results executeSQL(Connection conn, String sql, Object[] params, int[] sqlTypes, String... columnNames) throws SQLException, ParameterException {
		SQLType[] sqlTypeObjs = new SQLType[sqlTypes == null ? 0 : sqlTypes.length];
		for(int i = 0; i < sqlTypeObjs.length; i++)
			sqlTypeObjs[i] = new SQLType(sqlTypes[i]);
		return executeSQLx(conn, sql, params, sqlTypeObjs, 0, columnNames);
	}

	public static Results executeSQLx(Connection conn, String sql, Object[] params, SQLType[] sqlTypes) throws SQLException, ParameterException {
		return executeSQLx(conn, sql, params, sqlTypes, 0);
	}

	public static Results executeSQLx(Connection conn, String sql, Object[] params, SQLType[] sqlTypes, int maxRows, String... columnNames) throws SQLException, ParameterException {
		ResultSet rs = null;
        PreparedStatement ps = null;
        if(log.isDebugEnabled()) log.debug("Executing SQL '" + sql + "'");
        long start = System.currentTimeMillis();
        try {
            ps = conn.prepareStatement(sql);
            if(params != null) {
                for(int i = 0; i < params.length; i++) {
					SQLType sqlType = (sqlTypes != null && sqlTypes.length > i ? sqlTypes[i] : new SQLType(Types.VARCHAR));
                    Object paramValue = params[i];
					Call.setParam(ps, i + 1, paramValue, sqlType);
                }
            }
			if(maxRows > 0)
				ps.setMaxRows(maxRows);
            rs = ps.executeQuery();
            if(log.isDebugEnabled()) log.debug("SQL Executing took " + (System.currentTimeMillis() - start) + " milliseconds");
			return createResults(null, rs, false, false, columnNames);
        } finally {
            //if(rs != null) try { rs.close(); } catch(SQLException e) {} this is done in transformCursor
            if(ps != null) try { ps.close(); } catch(SQLException e) {}
        }
    }

	public static Column[] createColumns(ResultSet rs, String... columnNames) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int count = rsmd.getColumnCount();
        Column[] columns = new Column[count];
        for(int i = 1; i <= count; i++) {
			String name = columnNames == null || columnNames.length < i ? rsmd.getColumnName(i) : columnNames[i - 1];
            Column c = new Column();
            c.setIndex(i);
            c.setName(name);
            c.setPropertyName(name);
            c.setSqlType(rsmd.getColumnType(i));
            try {
                c.setColumnClass(Class.forName(rsmd.getColumnClassName(i)));
            } catch(ClassCastException e) {
                // leave it as Object then
            } catch(ClassNotFoundException e) {
                // leave it as Object then
            } catch(IllegalArgumentException e) {
                // leave it as Object then
            } catch(NoClassDefFoundError e) {
                // leave it as Object then
            }
            columns[i-1] = c;
        }
        return columns;
    }

    public static Results createResults(ResultSet rs, boolean lazyAccess) throws SQLException {
        return createResults(null, rs, lazyAccess, false);
    }

    public static Results createResults(Connection conn, ResultSet rs, boolean lazyAccess) throws SQLException {
        return createResults(conn, rs, lazyAccess, false);
    }

	public static Results createResults(Connection conn, ResultSet rs, boolean lazyAccess, boolean cacheable, String... columnNames) throws SQLException {
		Column[] columns = createColumns(rs, columnNames);
        Hints hints = new Hints();
        hints.lazyAccess = lazyAccess;
        hints.cacheable = cacheable;
        hints.elapsedTime = -1;
        if(conn==null){
        	return Call.transformCursor(rs, columns, Call.getDefaultCursorTransform(), hints);
        }else{
        	return Call.transformCursorWithConnection(conn, rs, columns, Call.getDefaultCursorTransform(), hints);
        }
    }

	public static Results executeSQL(String dataSourceName, String sql, Object[] params, int[] sqlTypes, String... columnNames) throws SQLException, DataLayerException {
        Connection conn = getConnection(dataSourceName);
		Results results = null;
        try {
			results = executeSQL(conn, sql, params, sqlTypes, columnNames);
			return results;
        } finally {
			DbUtils.commitOrRollback(conn, results != null, true);
        }
    }

	public static Results executeSQLx(String dataSourceName, String sql, Object[] params, SQLType[] sqlTypes, String... columnNames) throws SQLException, DataLayerException {
		Connection conn = getConnection(dataSourceName);
		Results results = null;
		try {
			results = executeSQLx(conn, sql, params, sqlTypes, 0, columnNames);
			return results;
		} finally {
			DbUtils.commitOrRollback(conn, results != null, true);
		}
	}

    public static void selectInto(String id, Object bean) throws SQLException, DataLayerException, BeanException {
    	getGlobalDataLayer().selectInto(id, bean);
    }

    public static void selectInto(String id, Object bean, boolean commit) throws SQLException, DataLayerException, BeanException {
    	getGlobalDataLayer().selectInto(id, bean, commit);
    }

    public static void selectInto(Connection conn, String id, Object bean) throws SQLException, DataLayerException, BeanException {
    	getGlobalDataLayer().selectInto(conn, id, bean);
    }
    public static void selectInto(String id, Object input, Object output) throws SQLException, DataLayerException, BeanException {
    	getGlobalDataLayer().selectInto(id, input, output);
    }

    public static void selectInto(String id, Object input, Object output, boolean commit) throws SQLException, DataLayerException, BeanException {
    	getGlobalDataLayer().selectInto(id, input, output, commit);
    }

    public static void selectInto(Connection conn, String id, Object input, Object output) throws SQLException, DataLayerException, BeanException {
    	getGlobalDataLayer().selectInto(conn, id, input, output);
    }
}