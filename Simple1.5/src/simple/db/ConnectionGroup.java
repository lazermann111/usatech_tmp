package simple.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;

public interface ConnectionGroup {
	public Connection getConnection(String dataSourceName, Boolean autoCommit) throws SQLException, DataLayerException;
	public Connection getConnection(String dataSourceName) throws SQLException, DataLayerException ;

	/**
	 * Closes the specified connection and creates and returns a new one to replace it
	 * 
	 * @param conn
	 * @param autoCommit
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	public Connection refreshConnection(Connection conn, Boolean autoCommit) throws SQLException, DataLayerException;

	/**
	 * Closes the specified connection and creates and returns a new one to replace it
	 * 
	 * @param conn
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	public Connection refreshConnection(Connection conn) throws SQLException, DataLayerException;

	public void commit() throws SQLException ;
	public void close(boolean rollback) ;
	public long getConnectingTime() ;
	public String getDataSourceName(int index) throws SQLException, DataLayerException, NoSuchElementException ;

	/**
	 * Returns the number of opened connections
	 * 
	 * @return
	 */
	public int getOpenedCount();

	/**
	 * Sets a list of preferred data source names. This ConnectionGroup will attempt to create a connection from
	 * the first item in the preferred list and so on. If connections have already been created this will only affect any
	 * new creation attempts
	 * 
	 * @param preferred
	 */
	public void setPreferred(Iterable<String> preferred);

	public Iterable<String> getPreferred();
	
	public List<String> getPreferredDataSourceNames(int num) throws SQLException, DataLayerException, NoSuchElementException;

}
