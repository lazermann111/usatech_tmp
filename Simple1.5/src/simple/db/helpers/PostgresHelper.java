package simple.db.helpers;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.postgresql.PGConnection;
import org.postgresql.copy.CopyIn;
import org.postgresql.core.BaseConnection;
import org.postgresql.jdbc.PgBlob;
import org.postgresql.jdbc.PgClob;
import org.postgresql.util.PGobject;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.CharacterStream;
import simple.db.ConnectionHolder;
import simple.db.DBUnwrap;
import simple.db.DataSourceNotFoundException;
import simple.db.DefaultArray;
import simple.db.DefaultDBHelper;
import simple.db.ParameterException;
import simple.db.WatchedCallableStatement;
import simple.io.BinaryStream;
import simple.io.PGTextOutputter;
import simple.io.ResultsPGTextOutputter;
import simple.results.Results;
import simple.sql.AdditionalTypes;
import simple.sql.ArraySQLType;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class PostgresHelper extends DefaultDBHelper {
	protected static final String JAVA_NULL = "\0";
	protected static final String POSTGRES_NULL = "";
	protected static final String LITERAL_NULL = "NULL";
	protected static final char[] ESCAPED_ARRAY_CHARS = {'"', '}', ','};
	protected static final SQLType OTHER_VARCHAR_TYPE = new SQLType(Types.OTHER, "varchar");
	protected static final SQLType OTHER_DATE_TYPE = new SQLType(Types.OTHER, "date");
	protected static final Map<Integer, String> typeNameMapping = new HashMap<Integer, String>();
	static {
		typeNameMapping.put(Types.TINYINT, "int2");
		typeNameMapping.put(Types.SMALLINT, "int2");
		typeNameMapping.put(Types.INTEGER, "int4");
		typeNameMapping.put(Types.ROWID, "oid");
		typeNameMapping.put(Types.BIGINT, "int8");
		// typeNameMapping.put(Types.DOUBLE, "money");
		typeNameMapping.put(Types.NUMERIC, "numeric");
		typeNameMapping.put(Types.DECIMAL, "numeric");
		typeNameMapping.put(Types.FLOAT, "float4");
		typeNameMapping.put(Types.REAL, "float4");
		typeNameMapping.put(Types.DOUBLE, "float8");
		typeNameMapping.put(Types.CHAR, "char");
		// typeNameMapping.put(Types.CHAR, "bpchar");
		typeNameMapping.put(Types.VARCHAR, "varchar");
		// typeNameMapping.put(Types.VARCHAR, "text");
		// typeNameMapping.put(Types.VARCHAR, "name");
		typeNameMapping.put(Types.BINARY, "bytea");
		typeNameMapping.put(Types.VARBINARY, "bytea");
		// typeNameMapping.put(Types.BIT, "bool");
		typeNameMapping.put(Types.BIT, "bit");
		typeNameMapping.put(Types.BOOLEAN, "bit");
		typeNameMapping.put(Types.DATE, "date");
		typeNameMapping.put(Types.TIME, "time");
		// typeNameMapping.put(Types.TIME, "timetz");
		typeNameMapping.put(Types.TIMESTAMP, "timestamp");
		// typeNameMapping.put(Types.TIMESTAMP, "timestamptz");
	}

	protected static class PostresArray implements java.sql.Array {
		protected final java.sql.Array delegate;
		protected final List<Object> elements = new ArrayList<Object>();

		public PostresArray(Array delegate) throws SQLException {
			this.delegate = delegate;
			try (ResultSet rs = delegate.getResultSet()) {
				while(rs.next())
					elements.add(rs.getObject(2));
			}
		}

		public String getBaseTypeName() throws SQLException {
			return delegate.getBaseTypeName();
		}

		public int getBaseType() throws SQLException {
			return delegate.getBaseType();
		}

		public Object getArray() throws SQLException {
			return elements.toArray(new Object[elements.size()]);
		}

		public Object getArray(Map<String, Class<?>> map) throws SQLException {
			return toArray(delegate.getResultSet(map)); // postgres driver does not support this currently
		}

		public Object getArray(long index, int count) throws SQLException {
			if(index > Integer.MAX_VALUE)
				throw new SQLException("Index out of bounds");
			int ii = (int) index;
			List<Object> sub = elements.subList(ii, ii + count);
			return sub.toArray(new Object[sub.size()]);
		}

		public Object getArray(long index, int count, Map<String, Class<?>> map) throws SQLException {
			return toArray(delegate.getResultSet(index, count, map)); // postgres driver does not support this currently
		}

		public ResultSet getResultSet() throws SQLException {
			return delegate.getResultSet();
		}

		public ResultSet getResultSet(Map<String, Class<?>> map) throws SQLException {
			return delegate.getResultSet(map);
		}

		public ResultSet getResultSet(long index, int count) throws SQLException {
			return delegate.getResultSet(index, count);
		}

		public ResultSet getResultSet(long index, int count, Map<String, Class<?>> map) throws SQLException {
			return delegate.getResultSet(index, count, map);
		}

		public void free() throws SQLException {
			delegate.free();
		}

		protected Object[] toArray(ResultSet rs) throws SQLException {
			List<Object> elements = new ArrayList<Object>();
			while(rs.next()) {
				elements.add(rs.getObject(2));
			}
			return elements.toArray(new Object[elements.size()]);
		}
	}
	public PostgresHelper() {
	}

	@Override
	protected Object translateValue(Connection conn, Object value, int typeCode) throws SQLException, ConvertException {
		if(value != null)
			switch(typeCode) {
				case Types.DATE:
				case Types.TIME:
				case Types.TIMESTAMP:
				case AdditionalTypes.TIMESTAMPTZ:
				case AdditionalTypes.TIMETZ:
					String typeName = SQLTypeUtils.getTypeCodeName(typeCode);
					if(typeName != null)
						typeName = typeName.toLowerCase();
					PGobject pgo = new PGobject();
					// NOTE: this is so that postgres knows it's a date, otherwise the jdbc driver uses Oid.UNSPECIFIED
					pgo.setType(typeName);
					String formatted;
					switch(typeCode) {
						case Types.DATE:
							formatted = formatDatetime(value, true, false, false);
							break;
						case Types.TIME:
							formatted = formatDatetime(value, false, true, false);
							break;
						case Types.TIMESTAMP:
							formatted = formatDatetime(value, true, true, false);
							break;
						case AdditionalTypes.TIMETZ:
							formatted = formatDatetime(value, false, true, true);
							break;
						case AdditionalTypes.TIMESTAMPTZ:
						default:
							formatted = formatDatetime(value, true, true, true);
							break;
					}

					pgo.setValue(formatted);
					return pgo;
		}
		Object o = super.translateValue(conn, value, typeCode);
		if(o != null && (typeCode == Types.VARCHAR || typeCode == Types.CHAR)) {
			o = ((String)o).replace(JAVA_NULL, POSTGRES_NULL);
		}
		return o;
	}

	/** Customized for Large Objects
	 * @see simple.db.DefaultDBHelper#registerOutParameter(java.sql.CallableStatement, int, simple.sql.SQLType)
	 */
	@Override
	public void registerOutParameter(CallableStatement callableStatement, int parameterIndex, String parameterName, SQLType sqlType) throws SQLException {
		switch(sqlType.getTypeCode()) {
			case Types.BLOB: case Types.CLOB:
				callableStatement.registerOutParameter(parameterIndex, Types.BIGINT);
				break;
			default:
				callableStatement.registerOutParameter(parameterIndex, sqlType.getTypeCode());
		}
	}
	/** Customized for Large Objects
	 * @see simple.db.DefaultDBHelper#getValue(java.sql.CallableStatement, int, simple.sql.SQLType)
	 */
	@Override
	public Object getValue(CallableStatement callableStatement, int parameterIndex, SQLType sqlType) throws SQLException {
		switch(sqlType.getTypeCode()) {
			case Types.BLOB: {
				long oid = callableStatement.getLong(parameterIndex);
				return new PgBlob((BaseConnection)DBUnwrap.getRealConnection(callableStatement.getConnection()), oid);
			}
			case Types.CLOB: {
				long oid = callableStatement.getLong(parameterIndex);
				return new PgClob((BaseConnection)DBUnwrap.getRealConnection(callableStatement.getConnection()), oid);
			}
			case Types.ARRAY: {
				java.sql.Array array = callableStatement.getArray(parameterIndex);
				if(array == null)
					return null;
				switch(array.getBaseType()) {
					case Types.BIT:
					case Types.SMALLINT:
					case Types.INTEGER:
					case Types.BIGINT:
					case Types.NUMERIC:
					case Types.REAL:
					case Types.DOUBLE:
					case Types.CHAR:
					case Types.VARCHAR:
					case Types.DATE:
					case Types.TIME:
					case Types.TIMESTAMP:
						return array;
					default: // Other types are not supported by org.postgresql.jdbc2.AbstractJdbc2Array.getArray() so use getResultSet()
						return new PostresArray(array);
				}
			}
			default:
				return super.getValue(callableStatement, parameterIndex, sqlType);
		}
	}
	
	public SQLType translateSqlType(SQLType sqlType, String databaseTypeName, ConnectionHolder connHolder) throws SQLException, DataSourceNotFoundException {
		sqlType = super.translateSqlType(sqlType, databaseTypeName, connHolder);
		if(sqlType instanceof ArraySQLType) {
			SQLType componentSqlType = ((ArraySQLType) sqlType).getComponentType();
			setTypeName(componentSqlType, componentSqlType.getTypeName());
			if(StringUtils.isBlank(sqlType.getTypeName()) || "ARRAY".equalsIgnoreCase(sqlType.getTypeName().trim()))
				sqlType.setTypeName(getPGArrayTypeName(componentSqlType));
		}
		return sqlType;
	}

	public static String getPGArrayTypeName(SQLType componentSqlType) {
		int typeCode = componentSqlType.getTypeCode();
		switch(typeCode) {
			case Types.CHAR:
				typeCode = Types.VARCHAR;
				break;
		}
		String pgTypeName = typeNameMapping.get(typeCode);
		if(pgTypeName == null)
			pgTypeName = componentSqlType.getTypeName();
		return "_" + pgTypeName;
	}

	public static String getPGTypeName(SQLType sqlType, String databaseTypeName) {
		String defaultTypeName = SQLTypeUtils.getTypeCodeName(sqlType.getTypeCode());
		String pgTypeName;
		if(sqlType instanceof ArraySQLType) {
			SQLType componentSqlType = ((ArraySQLType) sqlType).getComponentType();
			defaultTypeName = "ARRAY:" + getPGTypeName(componentSqlType, componentSqlType.getTypeName());
			pgTypeName = getPGArrayTypeName(componentSqlType);
		} else {
			pgTypeName = typeNameMapping.get(sqlType.getTypeCode());
		}
		if(databaseTypeName != null && (databaseTypeName = databaseTypeName.trim()).length() > 0 && !databaseTypeName.equalsIgnoreCase(defaultTypeName))
			return databaseTypeName.toLowerCase();
		else if(pgTypeName != null)
			return pgTypeName;
		return defaultTypeName;
	}

	@Override
	protected void setTypeName(SQLType sqlType, String databaseTypeName) {
		sqlType.setTypeName(getPGTypeName(sqlType, databaseTypeName));
	}

	protected String normalizeTypeName(SQLType componentSqlType) {
		String pgTypeName = typeNameMapping.get(componentSqlType.getTypeCode());
		if(pgTypeName != null)
			return pgTypeName;
		else if(componentSqlType instanceof ArraySQLType)
			return "_" + normalizeTypeName(((ArraySQLType) componentSqlType).getComponentType());
		else {
			String typeName = SQLTypeUtils.getTypeCodeName(componentSqlType.getTypeCode());
			return typeName != null ? typeName : componentSqlType.getTypeName();
		}
	}

	@Override
	public CallableStatement setQueryTimeout(CallableStatement callableStatement, double timeoutSeconds) throws SQLException {
		long timeoutMillis = (long) (timeoutSeconds * 1000);
		if(timeoutMillis > 0)
			return new WatchedCallableStatement(callableStatement, timeoutMillis, true);
		else
			return callableStatement;
	}
	@Override
	protected Object translateArray(Connection conn, Object array, SQLType sqlType) throws SQLException, ConvertException {
		// return an array whose toString() returns a string of the form "{element1,element2,"elem,ent3"}
		if(array == null)
    		return null;
    	SQLType componentSqlType;
    	if(sqlType instanceof ArraySQLType) {
    		componentSqlType = ((ArraySQLType)sqlType).getComponentType();
    	} else {
			componentSqlType = OTHER_VARCHAR_TYPE;
    	}
    	String typeName = componentSqlType.getTypeName();
    	if(typeName != null)
    		componentSqlType.setTypeName(typeName.toLowerCase());
    	return new DefaultArray(toElements(conn, array, componentSqlType.getTypeCode(), componentSqlType.getTypeName()), componentSqlType) {
    		@Override
    		public String toString() {
				return arrayToString(elements);
    		}

			@Override
			public String getBaseTypeName() throws SQLException {
				return normalizeTypeName(componentSqlType);
			}
    	};
	}
	@Override
	protected void setNull(PreparedStatement preparedStatement, int parameterIndex, String parameterName, SQLType sqlType) throws SQLException, ParameterException {
		if(sqlType != null)
			switch(sqlType.getTypeCode()) {
				case AdditionalTypes.TIMESTAMPTZ:
				case AdditionalTypes.TIMETZ:
					String typeName = SQLTypeUtils.getTypeCodeName(sqlType.getTypeCode());
					if(typeName != null)
						typeName = typeName.toLowerCase();
					sqlType = new SQLType(Types.OTHER, typeName);
					break;
			}
		super.setNull(preparedStatement, parameterIndex, parameterName, sqlType);
	}

	@Override
	protected void setObject(PreparedStatement preparedStatement, int parameterIndex, String parameterName, Object value, SQLType sqlType) throws SQLException, ParameterException {
		if(value instanceof PGobject) {
			sqlType = new SQLType(Types.OTHER, ((PGobject) value).getType());
		} else if(value == null && sqlType != null) {
			switch(sqlType.getTypeCode()) {
				case AdditionalTypes.TIMESTAMPTZ:
				case AdditionalTypes.TIMETZ:
					String typeName = SQLTypeUtils.getTypeCodeName(sqlType.getTypeCode());
					if(typeName != null)
						typeName = typeName.toLowerCase();
					sqlType = new SQLType(Types.OTHER, typeName);
					break;
			}
		}
		super.setObject(preparedStatement, parameterIndex, parameterName, value, sqlType);
	}

	protected String formatDatetime(Object datetime, boolean includeDate, boolean includeTime, boolean includeZone) throws ConvertException {
		Calendar cal = ConvertUtils.convert(Calendar.class, datetime);
		if(cal == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder(includeZone ? 50 : 30);
		if(includeDate) {
			StringUtils.appendPadded(sb, String.valueOf(cal.get(Calendar.YEAR)), '0', 4, Justification.RIGHT).append('-').append(1 + cal.get(Calendar.MONTH)).append('-').append(cal.get(Calendar.DAY_OF_MONTH));
		}
		if(includeTime) {
			if(includeDate)
				sb.append(' ');
			sb.append(cal.get(Calendar.HOUR_OF_DAY)).append(':').append(cal.get(Calendar.MINUTE)).append(':').append(cal.get(Calendar.SECOND));
			int ms = cal.get(Calendar.MILLISECOND);
			if(ms != 0)
				StringUtils.appendPadded(sb.append('.'), String.valueOf(ms), '0', 3, Justification.RIGHT);
		}
		if(includeZone) {
			sb.append(' ').append(cal.getTimeZone().getID());
		}
		if(includeDate)
			switch(cal.get(Calendar.ERA)) {
				case 0:
					sb.append(" BC");
					break;
			}
		return sb.toString();
	}

	@Override
	protected void setBinaryStream(PreparedStatement preparedStatement, int parameterIndex, String parameterName, BinaryStream stream) throws SQLException, ParameterException {
		long length = stream.getLength();
		try {
	    	if(length <= Integer.MAX_VALUE)
				preparedStatement.setBinaryStream(parameterIndex, stream.getInputStream(), (int)length);
			else
				throw new ParameterException("Length of stream (" + length + ") for parameter " + parameterName + " is greater than the max integer value and is NOT supported by " + preparedStatement.getClass().getName());
		} catch(IOException e) {
			throw new ParameterException("Could not get InputStream", e);
		}
	}
	@Override
	public void setInParameter(PreparedStatement preparedStatement, int parameterIndex, String parameterName, Object value, SQLType sqlType) throws SQLException, ParameterException {
		if (value instanceof String && ((String) value) != null && ((String) value).length() == 0) {
			value = null;
		}
		super.setInParameter(preparedStatement, parameterIndex, parameterName, value, sqlType);
	}	
	
	@Override
	protected void setCharacterStream(PreparedStatement preparedStatement, int parameterIndex, String parameterName, CharacterStream stream) throws SQLException, ParameterException {
		long length = stream.getLength();
		if(length <= Integer.MAX_VALUE)
			preparedStatement.setCharacterStream(parameterIndex, stream.getReader(), (int)length);
		else
			throw new ParameterException("Length of stream (" + length + ") for parameter " + parameterName + " is greater than the max integer value and is NOT supported by " + preparedStatement.getClass().getName());   
	}
	
	@Override
	public String getConnectionIdentifier(Connection conn) {
		if(!(conn instanceof BaseConnection)) {
    		conn = DBUnwrap.getRealConnection(conn);
    	}
		return super.getConnectionIdentifier(conn) + " PID=" + ((PGConnection) conn).getBackendPID(); // super.getConnectionIdentifier(conn);
	}

	// for array to string
	protected static StringBuilder appendElement(StringBuilder sb, Object element) {
		if(element == null) {
			sb.append(LITERAL_NULL);
		} else if(element instanceof Number) {
			sb.append(element);
		} else if(element instanceof byte[]) {
			sb.append("\\\\x");
			byte[] bytes = (byte[]) element;
			StringUtils.appendHex(sb, bytes, 0, bytes.length);
		} else if(ConvertUtils.isCollectionType(element.getClass())) {
			Collection<?> coll;
			try {
				coll = ConvertUtils.asCollection(element);
			} catch(ConvertException e) {
				return appendElementAsString(sb, element);
			}
			sb.append('{');
			boolean first = true;
			for(Object o : coll) {
				// if(o != null) { // exclude nulls
				if(first)
					first = false;
				else
					sb.append(',');
				appendElement(sb, o);
				// }
			}
			sb.append('}');
		} else {
			appendElementAsString(sb, element);
		}
		return sb;
	}

	protected static StringBuilder appendElementAsString(StringBuilder sb, Object element) {
		String s = ConvertUtils.getStringSafely(element);
		String safe = StringUtils.escape(s, ESCAPED_ARRAY_CHARS, '\\');
		if(safe.length() == 0 || safe.length() > s.length() || LITERAL_NULL.equalsIgnoreCase(s))
			sb.append('"').append(safe).append('"');
		else
			sb.append(s);
		return sb;
	}

	public static String arrayToString(Object elements) {
		return appendElement(new StringBuilder(), elements).toString();
	}

	public static long copyTo(Results src, Connection targetConn, String targetTable, String[] targetColumns) throws SQLException, IOException {
		if(targetColumns == null) {
			targetColumns = new String[src.getColumnCount()];
			for(int i = 0; i < targetColumns.length; i++)
				targetColumns[i] = src.getColumnName(i + 1);
		}
		final BaseConnection pgConn = (BaseConnection) DBUnwrap.getRealConnection(targetConn);
		final PGTextOutputter outputter = new ResultsPGTextOutputter(src);
		outputter.setTableName(targetTable);
		outputter.setColumnNames(targetColumns);
		final Charset encoding = Charset.forName(pgConn.getEncoding().name());
		final CopyIn copyIn = pgConn.getCopyAPI().copyIn(outputter.getCopySql(encoding));
		boolean okay = false;
		boolean beforeClose = true;
		try {
			final OutputStream out = new OutputStream() {
				@Override
				public void write(int b) throws IOException {
					try {
						copyIn.writeToCopy(new byte[] { (byte) b }, 0, 1);
					} catch(SQLException e) {
						throw new IOException(e);
					}
				}

				@Override
				public void write(byte[] b, int off, int len) throws IOException {
					try {
						copyIn.writeToCopy(b, off, len);
					} catch(SQLException e) {
						throw new IOException(e);
					}
				}
			};
			outputter.setWriter(new BufferedWriter(new OutputStreamWriter(out, encoding)));
			outputter.writeData();
			beforeClose = false;
			outputter.getWriter().close();
			okay = true;
		} finally {
			if(okay)
				return copyIn.endCopy();
			else
				copyIn.cancelCopy();
			if (beforeClose && !okay && outputter.getWriter() != null)
				outputter.getWriter().close();
		}
		return 0;
	}
	
	@Override
	public String getSortedSql(Connection conn, String originalSql, int[] sortBy) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM (").append("SELECT P.*, ROW_NUMBER() OVER() ROW_NUMBER FROM (").append(originalSql);
		if(sortBy != null && sortBy.length > 0) {
			boolean first = true;
			for(int index : sortBy) {
				if(index == 0)
					continue;
				if(first) {
					sql.append(" ORDER BY ");
					first = false;
				} else
					sql.append(", ");
				sql.append(Math.abs(index));
				if(index < 0)
					sql.append(" DESC");
			}
		}
		sql.append(") P limit ?").append(") X WHERE ROW_NUMBER >= ?");
		return sql.toString();
	}
	
	
	@Override
	public void setSortedRowLimits(CallableStatement callableStatement, int rowOffset, int rowLimit) throws SQLException {
		int count = callableStatement.getParameterMetaData().getParameterCount();
		callableStatement.setInt(count - 1, rowOffset + rowLimit);
		callableStatement.setInt(count, rowOffset);
	}
}
