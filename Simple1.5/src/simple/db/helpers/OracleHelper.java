/*
 * DBCPOracleDBHelper.java
 *
 * Created on May 13, 2003, 9:33 AM
 */

package simple.db.helpers;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.sql.Types;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.oracore.OracleNamedType;
import oracle.jdbc.oracore.OracleType;
import oracle.jdbc.oracore.OracleTypeCOLLECTION;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.BFILE;
import oracle.sql.BLOB;
import oracle.sql.CHAR;
import oracle.sql.CLOB;
import oracle.sql.CharacterSet;
import oracle.sql.DATE;
import oracle.sql.Datum;
import oracle.sql.ROWID;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import oracle.sql.TIMESTAMPTZ;
import oracle.sql.TypeDescriptor;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.Column;
import simple.db.ConnectionHolder;
import simple.db.DBUnwrap;
import simple.db.DataSourceNotFoundException;
import simple.db.WatchedCallableStatement;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.SliceInputStream;
import simple.sql.AdditionalTypes;
import simple.sql.ArraySQLType;
import simple.sql.SQLType;

/**
 *
 * @author  Brian S. Krug
 */
public class OracleHelper extends simple.db.DefaultDBHelper {
    private static Log log = Log.getLog();
    protected static final Map<String,Integer> oracleNamesToSqlTypes = new HashMap<String, Integer>();
    protected static final Charset ASCII_CHARSET = Charset.forName("US-ASCII");
    protected static final String JAVA_NULL = "\0";
    protected static final String ORACLE_NULL = "";
    
	private static Pattern ORA_ERROR_PATTERN = Pattern.compile("ORA-\\d+: (.*?)$.*$", Pattern.DOTALL|Pattern.MULTILINE);

    /** Creates a new instance of DBCPOracleDBHelper */
    public OracleHelper() {
    }
    /* can't pre-create array - need elements first
    public Object createArray(Connection conn, String typeName) throws SQLException, ConvertException {
    }
    */
	@Override
	public boolean reconnectNeeded(Connection conn, SQLException e) {
		try {
			if(conn.isClosed()) return true;
		} catch(SQLException e1) {
			return true;
		}
		switch(e.getErrorCode()) {
            case -2048:
            case -2063:
            case -2068:
            case -3113:
            case -1000:
            case 17002:
                return true;
            default:
                return false;
        }
	}
    @Override
	protected Object[] translateStructAttributes(Connection conn, Object struct, SQLType sqlType) throws SQLException, ConvertException {
    	checkOracleConnection(conn);
    	String typeName = sqlType.getTypeName();
        StructDescriptor desc = StructDescriptor.createDescriptor(typeName, conn);
        return translateStructAttributes(struct, desc);
    }

    protected Object[] translateStructAttributes(Object struct, StructDescriptor desc) throws SQLException, ConvertException {
        Object[] attrs;
        ResultSetMetaData rsmd = desc.getMetaData();
        if(struct instanceof Struct) attrs = ((Struct)struct).getAttributes();
        else if(struct instanceof Object[]) attrs = (Object[]) struct;
        else if(struct instanceof java.util.Collection<?>) attrs = ((java.util.Collection<?>)struct).toArray();
        else if(struct.getClass().isArray()) {//primitive array
            attrs = new simple.util.PrimitiveArrayList(struct).toArray();
        } else { //bean or map
            attrs = new Object[rsmd.getColumnCount()];
            for(int i = 0; i < attrs.length; i++) {
                try {
                	attrs[i] = ReflectionUtils.getProperty(struct, rsmd.getColumnName(i+1));
                } catch(IntrospectionException e) {
                    throw new ConvertException("Unable to retreive property '" + rsmd.getColumnName(i+1) + "' from " + struct, Object[].class, struct, e);
                } catch (IllegalAccessException e) {
                    throw new ConvertException("Unable to retreive property '" + rsmd.getColumnName(i+1) + "' from " + struct, Object[].class, struct, e);
                } catch (InvocationTargetException e) {
                    throw new ConvertException("Unable to retreive property '" + rsmd.getColumnName(i+1) + "' from " + struct, Object[].class, struct, e);
                } catch(ParseException e) {
                    throw new ConvertException("Unable to retreive property '" + rsmd.getColumnName(i+1) + "' from " + struct, Object[].class, struct, e);
				}
            }
        }
        if(attrs.length < rsmd.getColumnCount()) {
        	Object[] tmp = new Object[rsmd.getColumnCount()];
            System.arraycopy(attrs, 0, tmp, 0, attrs.length);
            attrs = tmp;
        } else if(attrs.length > rsmd.getColumnCount()) {
        	Object[] tmp = new Object[rsmd.getColumnCount()];
            System.arraycopy(attrs, 0, tmp, 0, tmp.length);
            attrs = tmp;
        }
        for(int i = 0; i < attrs.length; i++) {
            try {
            	attrs[i] = ConvertUtils.convert(Class.forName(rsmd.getColumnClassName(i+1)), attrs[i]);
            } catch(ConvertException ce) {
            	attrs[i] = ConvertUtils.convert(simple.sql.SQLTypeUtils.getJdbcType(rsmd.getColumnType(i+1)), attrs[i]);
            } catch(ClassNotFoundException cnfe) {
            	attrs[i] = ConvertUtils.convert(simple.sql.SQLTypeUtils.getJdbcType(rsmd.getColumnType(i+1)), attrs[i]);
            }
        }
        return attrs;
    }

    @Override
    protected Object translateStruct(Connection conn, Object struct, SQLType sqlType) throws SQLException, ConvertException {
        if(struct instanceof STRUCT || struct == null) return struct;
        checkOracleConnection(conn);
        String typeName = sqlType.getTypeName();
        StructDescriptor desc = StructDescriptor.createDescriptor(typeName, conn);
        return new STRUCT(desc, conn, translateStructAttributes(struct, desc));
    }

    @Override
    protected Object translateArray(Connection conn, Object array, SQLType sqlType) throws SQLException, ConvertException {
        checkOracleConnection(conn);
        String typeName = sqlType.getTypeName();
		SQLType componentSqlType;
		if(sqlType instanceof ArraySQLType)
			componentSqlType = ((ArraySQLType) sqlType).getComponentType();
		else
			componentSqlType = null;
		if(typeName == null || typeName.trim().length() == 0 || "ARRAY".equalsIgnoreCase(typeName)) {
			if(componentSqlType != null)
				typeName = findArrayTypeName(conn, componentSqlType.getTypeCode());
			else if(array != null)
				typeName = findArrayTypeName(conn, array);
			else
				typeName = findArrayTypeName(conn, Types.VARCHAR);// A reasonable default
			sqlType.setTypeName(typeName);
        }
		if(array instanceof ARRAY || array == null)
			return array;

        ArrayDescriptor desc = ArrayDescriptor.createDescriptor(typeName, conn);
        int componentTypeCode;
        String componentTypeName;
		if(componentSqlType != null) {
			componentTypeCode = componentSqlType.getTypeCode();
			componentTypeName = componentSqlType.getTypeName();
        } else {
        	componentTypeCode = desc.getBaseType();
        	componentTypeName = desc.getBaseName();
        }
        Object[] elements = toElements(conn, array, componentTypeCode, componentTypeName);

        // For some reason Oracle uses the server's charset (which for me is id=178)
        // for a TABLE OF VARCHAR. So we must implement this hack to use UTF (which is
        // the charaset that the thin driver can handle
        if(String.class.equals(simple.sql.SQLTypeUtils.getJdbcType(desc.getBaseType()))) {
            CharacterSet cs = CharacterSet.make(870); // UTF
            //CHAR[] oca = new CHAR[list.size()];
        	for(int i = 0; i < elements.length; i++)
        	    if(elements[i] != null) elements[i] = new CHAR((String)elements[i], cs);
        	//elements = oca;
        }
        log.debug("Creating an Oracle ARRAY of type '" + typeName + "' from " + (elements == null ? "<NULL>" : Arrays.asList(elements)));
        return new ARRAY(desc, conn, elements);
    }

    @Override
    protected Object[] translateArrayElements(Connection conn, Object array, SQLType sqlType) throws SQLException, ConvertException {
    	checkOracleConnection(conn);
    	String typeName = sqlType.getTypeName();
		SQLType componentSqlType;
		if(sqlType instanceof ArraySQLType)
			componentSqlType = ((ArraySQLType) sqlType).getComponentType();
		else
			componentSqlType = null;
        if(typeName == null || typeName.trim().length() == 0) {
			if(componentSqlType != null)
				typeName = findArrayTypeName(conn, componentSqlType.getTypeCode());
			else if(array != null)
				typeName = findArrayTypeName(conn, array);
			else
				typeName = findArrayTypeName(conn, Types.VARCHAR);// A reasonable default
        }
        ArrayDescriptor desc = ArrayDescriptor.createDescriptor(typeName, conn);
        int componentTypeCode;
        String componentTypeName;
		if(componentSqlType != null) {
			componentTypeCode = componentSqlType.getTypeCode();
			componentTypeName = componentSqlType.getTypeName();
        } else {
        	componentTypeCode = desc.getBaseType();
        	componentTypeName = desc.getBaseName();
        }
        return toElements(conn, array, componentTypeCode, componentTypeName);
    }

    protected static String[] getCompatibleTypeNames(int sqlType) {
        switch(sqlType) {
            case Types.CHAR: return new String[] {"CHAR","VARCHAR", "VARCHAR2", "CLOB", "CFILE"};
            case Types.VARCHAR: return new String[] {"VARCHAR", "VARCHAR2", "CLOB", "CFILE", "CHAR"};
            case Types.LONGVARCHAR: return new String[] {"CLOB", "CFILE", "VARCHAR", "VARCHAR2", "CHAR"};
            case Types.NUMERIC:
                return new String[] {"NUMBER", "DECIMAL", "REAL", "DOUBLE PRECISION", "FLOAT",
                    "BINARY_DOUBLE", "BINARY_FLOAT", "INTEGER",
                    "SIGNED BINARY INTEGER(32)", "SIGNED BINARY INTEGER(16)",
                    "SIGNED BINARY INTEGER(8)", "SMALLINT", "UNSIGNED BINARY INTEGER(32)",
                    "UNSIGNED BINARY INTEGER(16)", "UNSIGNED BINARY INTEGER(8)"};
            case Types.DECIMAL:
                return new String[] {"DECIMAL", "NUMBER", "REAL", "DOUBLE PRECISION", "FLOAT",
                    "BINARY_DOUBLE", "BINARY_FLOAT"};
            case Types.BIT:
                return new String[] {"SMALLINT", "NUMBER", "INTEGER",
                    "UNSIGNED BINARY INTEGER(8)","UNSIGNED BINARY INTEGER(16)",
                    "UNSIGNED BINARY INTEGER(32)","SIGNED BINARY INTEGER(8)",
                    "SIGNED BINARY INTEGER(16)", "SIGNED BINARY INTEGER(32)"};
            case Types.TINYINT:
                return new String[] {"NUMBER", "SMALLINT", "INTEGER",
                    "SIGNED BINARY INTEGER(8)", "SIGNED BINARY INTEGER(16)",
                    "SIGNED BINARY INTEGER(32)", "DECIMAL", "UNSIGNED BINARY INTEGER(8)",
                    "UNSIGNED BINARY INTEGER(16)", "UNSIGNED BINARY INTEGER(32)"};
            case Types.SMALLINT:
                return new String[] {"SMALLINT", "NUMBER", "INTEGER",
                    "SIGNED BINARY INTEGER(8)", "SIGNED BINARY INTEGER(16)",
                    "SIGNED BINARY INTEGER(32)", "DECIMAL", "UNSIGNED BINARY INTEGER(8)",
                    "UNSIGNED BINARY INTEGER(16)", "UNSIGNED BINARY INTEGER(32)"};
            case Types.INTEGER:
                return new String[] {"INTEGER", "NUMBER",
                    "SIGNED BINARY INTEGER(16)", "SIGNED BINARY INTEGER(32)", "DECIMAL",
                    "UNSIGNED BINARY INTEGER(16)", "UNSIGNED BINARY INTEGER(32)"};
            case Types.BIGINT:
                return new String[] {"NUMBER", "SIGNED BINARY INTEGER(32)",
                    "DECIMAL", "UNSIGNED BINARY INTEGER(32)"};
            case Types.REAL:
                return new String[] {"REAL", "DOUBLE PRECISION", "DECIMAL", "NUMBER", "FLOAT",
                    "BINARY_DOUBLE", "BINARY_FLOAT"};
            case Types.FLOAT:
                return new String[] {"FLOAT", "DECIMAL", "REAL", "DOUBLE PRECISION",
                    "BINARY_FLOAT", "BINARY_DOUBLE", "NUMBER"};
            case Types.DOUBLE:
                return new String[] {"DOUBLE PRECISION", "REAL", "DECIMAL", "NUMBER", "FLOAT",
                    "BINARY_DOUBLE", "BINARY_FLOAT"};
            case Types.BINARY: return new String[] {"BLOB" , "BFILE"};
            case Types.VARBINARY: return new String[] {"BLOB", "BFILE" };
            case Types.LONGVARBINARY: return new String[] {"BLOB", "BFILE" };
            case Types.DATE: return new String[] {"DATE", "TIMESTAMP","TIMESTAMP WITH LOCAL TZ","TIMESTAMP WITH TZ"};
            case Types.TIME: return new String[] {"TIME", "TIME WITH TZ", "DATE", "TIMESTAMP","TIMESTAMP WITH LOCAL TZ","TIMESTAMP WITH TZ"};
            case Types.TIMESTAMP: return new String[] {"TIMESTAMP","TIMESTAMP WITH LOCAL TZ","TIMESTAMP WITH TZ", "DATE"};
            case Types.BLOB: return new String[] {"BLOB" , "BFILE"};
            case Types.CLOB: return new String[] {"CLOB", "CFILE"};
            default: return null;
        }
    }

    @Override
    protected String findArrayTypeName(Connection conn, int componentSqlType) throws SQLException {
        String[] types = getCompatibleTypeNames(componentSqlType);
        if(types == null) return null;
        StringBuilder sql = new StringBuilder();
		sql.append("SELECT NVL(C.OWNER || '.', '') || C.TYPE_NAME FROM ALL_COLL_TYPES C JOIN ALL_USERS U ON C.OWNER = U.USERNAME WHERE C.ELEM_TYPE_NAME IN(");
        for(int i = 0; i < types.length; i++) {
            if(i > 0) sql.append(',');
            sql.append('\'').append(types[i]).append('\'');
        }
		sql.append(") AND (C.OWNER LIKE '%SYS%' OR C.TYPE_NAME NOT LIKE 'SYS%') ORDER BY DECODE(C.ELEM_TYPE_NAME");
        for(int i = 0; i < types.length; i++) {
            sql.append(", \'").append(types[i]).append("\', ").append(i);
        }
        sql.append(") ASC, ");
		sql.append("DECODE(C.OWNER, USER, 0, 'PUBLIC', 1, 2) ASC, NVL(C.UPPER_BOUND, 99999999999999999999) DESC, C.LENGTH DESC, C.PRECISION DESC, C.SCALE DESC, U.USER_ID DESC, C.TYPE_NAME ASC");
        Statement st = conn.createStatement();
        try {
            st.setMaxRows(1);
            log.debug("Executing SQL to find array type name: '" + sql.toString());
            ResultSet rs = st.executeQuery(sql.toString());
            try {
                if(rs.next()) {
                    String s = rs.getString(1);
                    log.debug("Found array type name of '" + s + "'");
                    return s;
                }
            } finally {
                rs.close();
            }
        } finally {
            st.close();
        }
        return null;
    }

    @Override
    protected int findSubtypeOfArray(Connection conn, String typeName) throws SQLException {
        String sql = "SELECT ELEM_TYPE_NAME FROM ALL_COLL_TYPES WHERE NVL(OWNER || '.', '') || TYPE_NAME = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        try {
            ps.setMaxRows(1);
            ps.setString(1, typeName);
            log.debug("Executing SQL to find sub type of array: " + sql);
            ResultSet rs = ps.executeQuery();
            try {
                if(rs.next()) {
                    String s = rs.getString(1);
                    log.debug("Found sub type name of '" + s + "'");
                    Integer subtype = oracleNamesToSqlTypes.get(s);
                    if(subtype == null) {
                        populateTypeMapping(conn);
                        subtype = oracleNamesToSqlTypes.get(s);
                    }
                    if(subtype != null) return subtype;
                }
            } finally {
                rs.close();
            }
        } finally {
            ps.close();
        }
        return Types.OTHER;
    }

    //This method could be used for all databases
    protected void populateTypeMapping(Connection conn) throws SQLException {
        ResultSet rs = conn.getMetaData().getTypeInfo();
        try {
            while(rs.next()) {
                oracleNamesToSqlTypes.put(rs.getString(1), rs.getInt(2));
            }
        } finally {
            rs.close();
        }
    }

    @Override
    protected Object createBlob(Connection conn, SQLType sqlType) throws SQLException, ConvertException {
    	checkOracleConnection(conn);
        return BLOB.createTemporary(conn, false, BLOB.DURATION_SESSION);
    }

    @Override
    protected Object translateBlob(Connection conn, Object blob, SQLType sqlType) throws SQLException, ConvertException {
        if(blob instanceof BLOB) return blob;
        if(blob == null)
        	return null;
        BLOB oracleBlob = (BLOB)createBlob(conn, sqlType);
        java.io.OutputStream os = oracleBlob.setBinaryStream(1L);
        try {
			InputStream is = ConvertUtils.convert(InputStream.class, blob);
			try {
				IOUtils.readInto(is, os, 1024);
				os.flush();
			} catch(java.io.IOException ioe) {
				log.warn("While translating blob to oracle blob", ioe);
				throw new SQLException(ioe);
			} finally {
				try {
					is.close();
				} catch(IOException e) {
					// ignore
				}
			}
		} finally {
			try {
				os.close();
			} catch(IOException e) {
				// ignore
			}
		}
        return oracleBlob;
    }

    @Override
    protected Object createClob(Connection conn, SQLType sqlType) throws SQLException, ConvertException {
    	checkOracleConnection(conn);
        return CLOB.createTemporary(conn, false, CLOB.DURATION_SESSION);
    }

    @Override
    protected Object translateClob(Connection conn, Object clob, SQLType sqlType) throws SQLException, ConvertException {
        if(clob instanceof CLOB) return clob;
        CLOB oracleClob = (CLOB)createClob(conn, sqlType);
        Writer out = oracleClob.setCharacterStream(1L);
        try {
			Reader in = ConvertUtils.convert(Reader.class, clob);
			try {
				IOUtils.readInto(in, out, 1024);
				out.flush();
			} catch(java.io.IOException ioe) {
				log.warn("While translating clob to oracle clob", ioe);
			} finally {
				try {
					in.close();
				} catch(IOException e) {
					// ignore
				}
			}
		} finally {
			try {
				out.close();
			} catch(IOException e) {
				// ignore
			}
		}
        return oracleClob;
    }

    /**
     * @throws SQLException 
     * @see simple.db.DefaultDBHelper#translateValue(java.sql.Connection, java.lang.Object, int)
     */
    @Override
    protected Object translateValue(Connection conn, Object value, int typeCode) throws SQLException, ConvertException {
    	switch(typeCode) {
    		case Types.TIMESTAMP:
    			Calendar cal = ConvertUtils.convert(Calendar.class, value);
    			if(cal == null) {
    				return null;
    			} else if(cal.get(Calendar.MILLISECOND) == 0) {
    				//we can truncate to oracle.sql.DATE and thus avoid conversions at the server that frequently cause oracle's query optimizer to not use indexes
    				return new DATE(new java.sql.Timestamp(cal.getTimeInMillis()), cal);
				} else {
					return new java.sql.Timestamp(cal.getTimeInMillis());
				}
    		case AdditionalTypes.TIMESTAMPTZ:
    			cal = ConvertUtils.convert(Calendar.class, value);
				if(cal == null)
    				return null;
    			return new TIMESTAMPTZ(conn, new java.sql.Timestamp(cal.getTimeInMillis()), cal);
    	}
    	Object o = super.translateValue(conn, value, typeCode);
		if(o != null && (typeCode == Types.VARCHAR || typeCode == Types.CHAR)) {
			o = ((String)o).replace(JAVA_NULL, ORACLE_NULL);
		}
    	return o;
    }

    protected void checkOracleConnection(Connection conn) {
       if(!(conn instanceof OracleConnection))
    	   throw new ClassCastException("Connection, '" + conn + "' of class " + conn.getClass().getName() + " is not of class oracle.jdbc.OracleConnection");
    }

    /**
     * @see simple.db.DefaultDBHelper#getValue(java.sql.ResultSet, simple.db.Column)
     * @Override
     */
    @Override
    public Object getValue(ResultSet rs, Column column, boolean connected) throws SQLException, ConvertException {
        switch(column.getSqlType()) {
			case Types.DATE:
			case -101:
				return rs.getTimestamp(column.getIndex());
			case Types.CLOB:
				if(connected)
					return rs.getClob(column.getIndex());
				break;
			case Types.BLOB:
				if(connected)
					return rs.getBlob(column.getIndex());
				break;
        }
		Object value = super.getValue(rs, column, connected);
		if(value instanceof Datum)
			value = toJdbc((Datum) value);
		return value;
    }

    protected Object toJdbc(Datum datum) throws SQLException {
    	if(datum instanceof BFILE) {
    		return new BFILEBlob((BFILE)datum);
    	} else if(datum instanceof ROWID) {
    		return ((ROWID)datum).getBytes();
    	} else if(datum instanceof ARRAY) {
    		Datum[] oracleArray = ((ARRAY)datum).getOracleArray();
    		Object[] array = new Object[oracleArray.length];
    		for(int i = 0; i < oracleArray.length; i++)
    			if(oracleArray[i] != null)
    				array[i] = toJdbc(oracleArray[i]);
    		return array;
    	} else
    		return datum.toJdbc();
    }

    @Override
	protected SQLType getCustomSqlType(SQLType sqlType, String databaseTypeName, ConnectionHolder connHolder) throws SQLException, DataSourceNotFoundException {
    	Connection conn = connHolder.getConnection();
    	if(!(conn instanceof OracleConnection)) {
    		conn = DBUnwrap.getRealConnection(conn);
    	}

    	checkOracleConnection(conn);
    	OracleConnection oconn = (OracleConnection) conn;
    	TypeDescriptor td = TypeDescriptor.getTypeDescriptor(databaseTypeName,oconn);
    	return getSqlType(td.getPickler());
    }

    /**
     * @see simple.db.DefaultDBHelper#setTypeName(simple.sql.SQLType, java.lang.String)
     */
    @Override
    protected void setTypeName(SQLType sqlType, String databaseTypeName) {
    	switch(sqlType.getTypeCode()) {
    		case Types.VARBINARY:
    			//Oracle Thin Driver has a problem when out parameters of type VARBINARY have a typeName
    			sqlType.setTypeName(null);
    			return;
    	}
    	super.setTypeName(sqlType, databaseTypeName);
    }

    protected SQLType getSqlType(OracleType oracleType) throws SQLException {
		int typeCode = oracleType.getTypeCode();
		switch(typeCode) {
	        case Types.ARRAY:
	        	OracleTypeCOLLECTION otc = (OracleTypeCOLLECTION) oracleType;
	        	return new ArraySQLType(otc.getFullName(), getSqlType(otc.getElementType()));
	        case Types.REF:
	        	//TODO: return RefSQLType(otc.getFullName(), getSqlType(otc.getElementType()));
	        case Types.DATALINK:
	        	//TODO: return ???
	        case Types.OTHER:
	        	//TODO: return ???
	        case Types.JAVA_OBJECT:
		        //TODO: return JavaObjectSQLType(otc.getFullName(),((StructDescriptor)((OracleTypeADT)oracleType).getTypeDescriptor()).getJavaClassName());
	        case Types.STRUCT:
		        //TODO: return StructSQLType(otc.getFullName(),((StructDescriptor)((OracleTypeADT)oracleType).getTypeDescriptor()).getAttributeNames());
	        default:
	        	if(oracleType instanceof OracleNamedType)
	        		return new SQLType(typeCode, ((OracleNamedType)oracleType).getFullName());
	        	else
	        		return new SQLType(typeCode);
		}
	}

	protected static class BFILEBlob implements Blob {
    	protected final BFILE bfile;
    	protected BFILEBlob(BFILE bfile) {
    		this.bfile = bfile;
    	}
    	public InputStream getBinaryStream() throws SQLException {
			return bfile.getBinaryStream();
		}
		public byte[] getBytes(long pos, int length) throws SQLException {
			return bfile.getBytes(pos, length);
		}
		public InputStream getBinaryStream(long pos, long length) throws SQLException {
			InputStream in = bfile.getBinaryStream(pos);
			//wrap this in a length limiting InputStream
			if(pos + length < length())
				try {
					in = new SliceInputStream(in, 0, length);
				} catch(IOException e) {
					throw new SQLException(e);
				}

			return in;
		}
		public void free() throws SQLException {
			bfile.close();
		}
		public long length() throws SQLException {
			return bfile.length();
		}
		public long position(byte[] pattern, long start) throws SQLException {
			return bfile.position(pattern, start);
		}
		public long position(Blob pattern, long start) throws SQLException {
			return bfile.position(pattern.getBytes(0, (int)pattern.length()), start);
		}
		public OutputStream setBinaryStream(long pos) throws SQLException {
			throw new SQLException("Modification of BFILE is not allowed");
		}
		public int setBytes(long pos, byte[] bytes) throws SQLException {
			throw new SQLException("Modification of BFILE is not allowed");
		}
		public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
			throw new SQLException("Modification of BFILE is not allowed");
		}
		public void truncate(long len) throws SQLException {
			throw new SQLException("Modification of BFILE is not allowed");
		}
    }
	
	/** Oracle's query timeout does not often work. This may succeed more consistently.
	 * @see simple.db.DefaultDBHelper#setQueryTimeout(java.sql.CallableStatement, double)
	 */
	@Override
	public CallableStatement setQueryTimeout(CallableStatement callableStatement, double timeoutSeconds) throws SQLException {
		long timeoutMillis = (long) (timeoutSeconds * 1000);
		if(timeoutMillis > 0)
			return new WatchedCallableStatement(callableStatement, timeoutMillis);
		else
			return callableStatement;
	}
	
	protected boolean useTypeName(int typeCode) {
		switch(typeCode) {
			case Types.ARRAY: case Types.JAVA_OBJECT: case Types.OTHER: case Types.STRUCT: 
				return true;
			default:
				return false;
		}
	}
	
	@Override
	public void registerOutParameter(CallableStatement callableStatement, int parameterIndex, String parameterName, SQLType sqlType) throws SQLException {
		if(sqlType.getTypeName() != null && useTypeName(sqlType.getTypeCode()))
			callableStatement.registerOutParameter(parameterIndex, sqlType.getTypeCode(), sqlType.getTypeName());
        else
        	callableStatement.registerOutParameter(parameterIndex, sqlType.getTypeCode());
    }
	
	public String getConnectionIdentifier(Connection conn) {
		if(!(conn instanceof OracleConnection)) {
    		conn = DBUnwrap.getRealConnection(conn);
    	}

    	checkOracleConnection(conn);
    	if(conn instanceof oracle.jdbc.internal.OracleConnection) {
    		try {
    			//AUTH_SESSION_ID, AUTH_SERIAL_NUM, AUTH_INSTANCE_NO
    			Properties sessionInfo = ((oracle.jdbc.internal.OracleConnection)conn).getServerSessionInfo();
				return "Oracle [SID=" + sessionInfo.get("AUTH_SESSION_ID") + "; Serial#=" + sessionInfo.get("AUTH_SERIAL_NUM") + "; Instance=" +sessionInfo.get("AUTH_INSTANCE_NO") + "]";
			} catch(SQLException e) {
				//ignore
			}
    	}
    	return super.getConnectionIdentifier(conn);
	}

	@Override
	public String getSortedSql(Connection conn, String originalSql, int[] sortBy) {
		// Oracle 12c can use "OFFSET ? ROWS FETCH NEXT ? ROWS ONLY"
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT /*+ ALL_ROWS */ * FROM (").append("SELECT P.*, ROWNUM ROW_NUMBER FROM (").append(originalSql);
		if(sortBy != null && sortBy.length > 0) {
			boolean first = true;
			for(int index : sortBy) {
				if(index == 0)
					continue;
				if(first) {
					sql.append(" ORDER BY ");
					first = false;
				} else
					sql.append(", ");
				sql.append(Math.abs(index));
				if(index < 0)
					sql.append(" DESC");
			}
		}
		sql.append(") P WHERE ROWNUM <= ?").append(") X WHERE ROW_NUMBER >= ?");
		return sql.toString();
	}

	@Override
	public void setSortedRowLimits(CallableStatement callableStatement, int rowOffset, int rowLimit) throws SQLException {
		int count = callableStatement.getParameterMetaData().getParameterCount();
		callableStatement.setInt(count - 1, rowOffset + rowLimit);
		callableStatement.setInt(count, rowOffset);
	}
	
	public static String getORAErrorMessage(SQLException e) {
		if (e.getMessage() != null) {
			Matcher matcher = ORA_ERROR_PATTERN.matcher(e.getMessage());
			if (matcher.matches()) {
				return matcher.group(1);
			}
		} 
		
		return null;
	}
}
