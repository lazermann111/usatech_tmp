package simple.db.helpers;

import java.sql.CallableStatement;
import java.sql.SQLException;

import simple.db.DefaultDBHelper;
import simple.sql.SQLType;

public class DerbyHelper extends DefaultDBHelper {
	public DerbyHelper() {
	}

	/** Derby does not support typeNames
	 * @see simple.db.DefaultDBHelper#registerOutParameter(java.sql.CallableStatement, int, simple.sql.SQLType)
	 */
	@Override
	public void registerOutParameter(CallableStatement callableStatement, int parameterIndex, String parameterName, SQLType sqlType) throws SQLException {
		callableStatement.registerOutParameter(parameterIndex, sqlType.getTypeCode());
	}
}
