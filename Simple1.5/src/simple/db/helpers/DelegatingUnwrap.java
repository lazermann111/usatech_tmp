/*
 * DBCPUnwrap.java
 *
 * Created on May 13, 2003, 9:33 AM
 */

package simple.db.helpers;

import java.sql.Connection;

import simple.db.DelegatingConnection;

/**
 *
 * @author  Brian S. Krug
 */
public class DelegatingUnwrap extends simple.db.DBUnwrap {
    /** Creates a new instance of TomcatDBCPUnwrap */
    public DelegatingUnwrap() {
    }

    @Override
	protected Connection getDelegateConnection(Connection conn) {
       while(conn instanceof DelegatingConnection) conn = ((DelegatingConnection)conn).getDelegate();
       return conn;
    }
}
