/*
 * DelegatingDataSource.java
 *
 * Created on January 24, 2008, 9:20 AM
 */

package simple.db;

/**
 *
 * @author Brian S. Krug
 */

import java.io.InputStream;
import java.io.Reader;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import javax.sql.DataSource;

import simple.db.DelegatingDataSource.InnerConnection.InnerCallableStatement;
import simple.db.DelegatingDataSource.InnerConnection.InnerPreparedStatement;
import simple.db.DelegatingDataSource.InnerConnection.InnerStatement;
import simple.db.DelegatingDataSource.InnerConnection.InnerStatement.InnerResultSet;

public abstract class DelegatingDataSource implements DataSource {
	protected DataSource delegate;

	public DelegatingDataSource(DataSource delegate) {

		this.delegate = delegate;
	}

	protected DataSource getDelegate() {

		return delegate;
	}

	public Connection getConnection() throws SQLException {
		return new InnerConnection(getDelegate().getConnection());
	}

	public Connection getConnection(java.lang.String stringVar0, java.lang.String stringVar1)
			throws SQLException {
		return new InnerConnection(getDelegate().getConnection(stringVar0, stringVar1));
	}

	public java.io.PrintWriter getLogWriter() throws SQLException {
		return getDelegate().getLogWriter();
	}

	public int getLoginTimeout() throws SQLException {
		return getDelegate().getLoginTimeout();
	}

	public void setLogWriter(java.io.PrintWriter printWriterVar0) throws SQLException {
		getDelegate().setLogWriter(printWriterVar0);
	}

	public void setLoginTimeout(int intVar0) throws SQLException {
		getDelegate().setLoginTimeout(intVar0);
	}

	// The inner classes --------------------------------------
	protected final class InnerConnection implements Connection {
		protected class InnerStatement implements Statement {
			protected final class InnerResultSet implements ResultSet {
				protected ResultSet delegate;

				public InnerResultSet(ResultSet delegate) {
					this.delegate = delegate;
					resultSetInit(this);
				}

				protected ResultSet getDelegate() {
					return delegate;
				}

				public boolean absolute(int intVar0) throws SQLException {
					return resultSetAbsolute(this, intVar0);
				}

				public void afterLast() throws SQLException {
					resultSetAfterLast(this);
				}

				public void beforeFirst() throws SQLException {
					resultSetBeforeFirst(this);
				}

				public void cancelRowUpdates() throws SQLException {
					resultSetCancelRowUpdates(this);
				}

				public void clearWarnings() throws SQLException {
					resultSetClearWarnings(this);
				}

				public void close() throws SQLException {
					resultSetClose(this);
				}

				public void deleteRow() throws SQLException {
					resultSetDeleteRow(this);
				}

				public int findColumn(java.lang.String stringVar0) throws SQLException {
					return resultSetFindColumn(this, stringVar0);
				}

				public boolean first() throws SQLException {
					return resultSetFirst(this);
				}

				public java.sql.Array getArray(int intVar0) throws SQLException {
					return resultSetGetArray(this, intVar0);
				}

				public java.sql.Array getArray(java.lang.String stringVar0) throws SQLException {
					return resultSetGetArray(this, stringVar0);
				}

				public java.io.InputStream getAsciiStream(int intVar0) throws SQLException {
					return resultSetGetAsciiStream(this, intVar0);
				}

				public java.io.InputStream getAsciiStream(java.lang.String stringVar0)
						throws SQLException {
					return resultSetGetAsciiStream(this, stringVar0);
				}

				public java.math.BigDecimal getBigDecimal(int intVar0) throws SQLException {
					return resultSetGetBigDecimal(this, intVar0);
				}

				public java.math.BigDecimal getBigDecimal(java.lang.String stringVar0)
						throws SQLException {
					return resultSetGetBigDecimal(this, stringVar0);
				}

				public java.math.BigDecimal getBigDecimal(int intVar0, int intVar1)
						throws SQLException {
					return resultSetGetBigDecimal(this, intVar0, intVar1);
				}

				public java.math.BigDecimal getBigDecimal(java.lang.String stringVar0, int intVar1)
						throws SQLException {
					return resultSetGetBigDecimal(this, stringVar0, intVar1);
				}

				public java.io.InputStream getBinaryStream(int intVar0) throws SQLException {
					return resultSetGetBinaryStream(this, intVar0);
				}

				public java.io.InputStream getBinaryStream(java.lang.String stringVar0)
						throws SQLException {
					return resultSetGetBinaryStream(this, stringVar0);
				}

				public java.sql.Blob getBlob(int intVar0) throws SQLException {
					return resultSetGetBlob(this, intVar0);
				}

				public java.sql.Blob getBlob(java.lang.String stringVar0) throws SQLException {
					return resultSetGetBlob(this, stringVar0);
				}

				public boolean getBoolean(int intVar0) throws SQLException {
					return resultSetGetBoolean(this, intVar0);
				}

				public boolean getBoolean(java.lang.String stringVar0) throws SQLException {
					return resultSetGetBoolean(this, stringVar0);
				}

				public byte getByte(int intVar0) throws SQLException {
					return resultSetGetByte(this, intVar0);
				}

				public byte getByte(java.lang.String stringVar0) throws SQLException {
					return resultSetGetByte(this, stringVar0);
				}

				public byte[] getBytes(int intVar0) throws SQLException {
					return resultSetGetBytes(this, intVar0);
				}

				public byte[] getBytes(java.lang.String stringVar0) throws SQLException {
					return resultSetGetBytes(this, stringVar0);
				}

				public java.io.Reader getCharacterStream(int intVar0) throws SQLException {
					return resultSetGetCharacterStream(this, intVar0);
				}

				public java.io.Reader getCharacterStream(java.lang.String stringVar0)
						throws SQLException {
					return resultSetGetCharacterStream(this, stringVar0);
				}

				public java.sql.Clob getClob(int intVar0) throws SQLException {
					return resultSetGetClob(this, intVar0);
				}

				public java.sql.Clob getClob(java.lang.String stringVar0) throws SQLException {
					return resultSetGetClob(this, stringVar0);
				}

				public int getConcurrency() throws SQLException {
					return resultSetGetConcurrency(this);
				}

				public java.lang.String getCursorName() throws SQLException {
					return resultSetGetCursorName(this);
				}

				public java.sql.Date getDate(int intVar0) throws SQLException {
					return resultSetGetDate(this, intVar0);
				}

				public java.sql.Date getDate(java.lang.String stringVar0) throws SQLException {
					return resultSetGetDate(this, stringVar0);
				}

				public java.sql.Date getDate(int intVar0, java.util.Calendar calendarVar1)
						throws SQLException {
					return resultSetGetDate(this, intVar0, calendarVar1);
				}

				public java.sql.Date getDate(java.lang.String stringVar0,
						java.util.Calendar calendarVar1) throws SQLException {
					return resultSetGetDate(this, stringVar0, calendarVar1);
				}

				public double getDouble(int intVar0) throws SQLException {
					return resultSetGetDouble(this, intVar0);
				}

				public double getDouble(java.lang.String stringVar0) throws SQLException {
					return resultSetGetDouble(this, stringVar0);
				}

				public int getFetchDirection() throws SQLException {
					return resultSetGetFetchDirection(this);
				}

				public int getFetchSize() throws SQLException {
					return resultSetGetFetchSize(this);
				}

				public float getFloat(int intVar0) throws SQLException {
					return resultSetGetFloat(this, intVar0);
				}

				public float getFloat(java.lang.String stringVar0) throws SQLException {
					return resultSetGetFloat(this, stringVar0);
				}

				public int getInt(int intVar0) throws SQLException {
					return resultSetGetInt(this, intVar0);
				}

				public int getInt(java.lang.String stringVar0) throws SQLException {
					return resultSetGetInt(this, stringVar0);
				}

				public long getLong(int intVar0) throws SQLException {
					return resultSetGetLong(this, intVar0);
				}

				public long getLong(java.lang.String stringVar0) throws SQLException {
					return resultSetGetLong(this, stringVar0);
				}

				public java.sql.ResultSetMetaData getMetaData() throws SQLException {
					return resultSetGetMetaData(this);
				}

				public java.lang.Object getObject(int intVar0) throws SQLException {
					return resultSetGetObject(this, intVar0);
				}

				public java.lang.Object getObject(java.lang.String stringVar0) throws SQLException {
					return resultSetGetObject(this, stringVar0);
				}

				public java.sql.Ref getRef(int intVar0) throws SQLException {
					return resultSetGetRef(this, intVar0);
				}

				public java.sql.Ref getRef(java.lang.String stringVar0) throws SQLException {
					return resultSetGetRef(this, stringVar0);
				}

				public int getRow() throws SQLException {
					return resultSetGetRow(this);
				}

				public short getShort(int intVar0) throws SQLException {
					return resultSetGetShort(this, intVar0);
				}

				public short getShort(java.lang.String stringVar0) throws SQLException {
					return resultSetGetShort(this, stringVar0);
				}

				public Statement getStatement() throws SQLException {
					return InnerStatement.this;
				}

				public java.lang.String getString(int intVar0) throws SQLException {
					return resultSetGetString(this, intVar0);
				}

				public java.lang.String getString(java.lang.String stringVar0) throws SQLException {
					return resultSetGetString(this, stringVar0);
				}

				public java.sql.Time getTime(int intVar0) throws SQLException {
					return resultSetGetTime(this, intVar0);
				}

				public java.sql.Time getTime(java.lang.String stringVar0) throws SQLException {
					return resultSetGetTime(this, stringVar0);
				}

				public java.sql.Time getTime(int intVar0, java.util.Calendar calendarVar1)
						throws SQLException {
					return resultSetGetTime(this, intVar0, calendarVar1);
				}

				public java.sql.Time getTime(java.lang.String stringVar0,
						java.util.Calendar calendarVar1) throws SQLException {
					return resultSetGetTime(this, stringVar0, calendarVar1);
				}

				public java.sql.Timestamp getTimestamp(int intVar0) throws SQLException {
					return resultSetGetTimestamp(this, intVar0);
				}

				public java.sql.Timestamp getTimestamp(java.lang.String stringVar0)
						throws SQLException {
					return resultSetGetTimestamp(this, stringVar0);
				}

				public java.sql.Timestamp getTimestamp(int intVar0, java.util.Calendar calendarVar1)
						throws SQLException {
					return resultSetGetTimestamp(this, intVar0, calendarVar1);
				}

				public java.sql.Timestamp getTimestamp(java.lang.String stringVar0,
						java.util.Calendar calendarVar1) throws SQLException {
					return resultSetGetTimestamp(this, stringVar0, calendarVar1);
				}

				public int getType() throws SQLException {
					return resultSetGetType(this);
				}

				public java.net.URL getURL(int intVar0) throws SQLException {
					return resultSetGetURL(this, intVar0);
				}

				public java.net.URL getURL(java.lang.String stringVar0) throws SQLException {
					return resultSetGetURL(this, stringVar0);
				}

				public java.io.InputStream getUnicodeStream(int intVar0) throws SQLException {
					return resultSetGetUnicodeStream(this, intVar0);
				}

				public java.io.InputStream getUnicodeStream(java.lang.String stringVar0)
						throws SQLException {
					return resultSetGetUnicodeStream(this, stringVar0);
				}

				public java.sql.SQLWarning getWarnings() throws SQLException {
					return resultSetGetWarnings(this);
				}

				public void insertRow() throws SQLException {
					resultSetInsertRow(this);
				}

				public boolean isAfterLast() throws SQLException {
					return resultSetIsAfterLast(this);
				}

				public boolean isBeforeFirst() throws SQLException {
					return resultSetIsBeforeFirst(this);
				}

				public boolean isFirst() throws SQLException {
					return resultSetIsFirst(this);
				}

				public boolean isLast() throws SQLException {
					return resultSetIsLast(this);
				}

				public boolean last() throws SQLException {
					return resultSetLast(this);
				}

				public void moveToCurrentRow() throws SQLException {
					resultSetMoveToCurrentRow(this);
				}

				public void moveToInsertRow() throws SQLException {
					resultSetMoveToInsertRow(this);
				}

				public boolean next() throws SQLException {
					return resultSetNext(this);
				}

				public boolean previous() throws SQLException {
					return resultSetPrevious(this);
				}

				public void refreshRow() throws SQLException {
					resultSetRefreshRow(this);
				}

				public boolean relative(int intVar0) throws SQLException {
					return resultSetRelative(this, intVar0);
				}

				public boolean rowDeleted() throws SQLException {
					return resultSetRowDeleted(this);
				}

				public boolean rowInserted() throws SQLException {
					return resultSetRowInserted(this);
				}

				public boolean rowUpdated() throws SQLException {
					return resultSetRowUpdated(this);
				}

				public void setFetchDirection(int intVar0) throws SQLException {
					resultSetSetFetchDirection(this, intVar0);
				}

				public void setFetchSize(int intVar0) throws SQLException {
					resultSetSetFetchSize(this, intVar0);
				}

				public void updateArray(int intVar0, java.sql.Array arrayVar1) throws SQLException {
					resultSetUpdateArray(this, intVar0, arrayVar1);
				}

				public void updateArray(java.lang.String stringVar0, java.sql.Array arrayVar1)
						throws SQLException {
					resultSetUpdateArray(this, stringVar0, arrayVar1);
				}

				public void updateAsciiStream(int intVar0, java.io.InputStream inputStreamVar1,
						int intVar2) throws SQLException {
					resultSetUpdateAsciiStream(this, intVar0, inputStreamVar1, intVar2);
				}

				public void updateAsciiStream(java.lang.String stringVar0,
						java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
					resultSetUpdateAsciiStream(this, stringVar0, inputStreamVar1, intVar2);
				}

				public void updateBigDecimal(int intVar0, java.math.BigDecimal bigDecimalVar1)
						throws SQLException {
					resultSetUpdateBigDecimal(this, intVar0, bigDecimalVar1);
				}

				public void updateBigDecimal(java.lang.String stringVar0,
						java.math.BigDecimal bigDecimalVar1) throws SQLException {
					resultSetUpdateBigDecimal(this, stringVar0, bigDecimalVar1);
				}

				public void updateBinaryStream(int intVar0, java.io.InputStream inputStreamVar1,
						int intVar2) throws SQLException {
					resultSetUpdateBinaryStream(this, intVar0, inputStreamVar1, intVar2);
				}

				public void updateBinaryStream(java.lang.String stringVar0,
						java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
					resultSetUpdateBinaryStream(this, stringVar0, inputStreamVar1, intVar2);
				}

				public void updateBlob(int intVar0, java.sql.Blob blobVar1) throws SQLException {
					resultSetUpdateBlob(this, intVar0, blobVar1);
				}

				public void updateBlob(java.lang.String stringVar0, java.sql.Blob blobVar1)
						throws SQLException {
					resultSetUpdateBlob(this, stringVar0, blobVar1);
				}

				public void updateBoolean(int intVar0, boolean booleanVar1) throws SQLException {
					resultSetUpdateBoolean(this, intVar0, booleanVar1);
				}

				public void updateBoolean(java.lang.String stringVar0, boolean booleanVar1)
						throws SQLException {
					resultSetUpdateBoolean(this, stringVar0, booleanVar1);
				}

				public void updateByte(int intVar0, byte byteVar1) throws SQLException {
					resultSetUpdateByte(this, intVar0, byteVar1);
				}

				public void updateByte(java.lang.String stringVar0, byte byteVar1)
						throws SQLException {
					resultSetUpdateByte(this, stringVar0, byteVar1);
				}

				public void updateBytes(int intVar0, byte[] byteVar1) throws SQLException {
					resultSetUpdateBytes(this, intVar0, byteVar1);
				}

				public void updateBytes(java.lang.String stringVar0, byte[] byteVar1)
						throws SQLException {
					resultSetUpdateBytes(this, stringVar0, byteVar1);
				}

				public void updateCharacterStream(int intVar0, java.io.Reader readerVar1,
						int intVar2) throws SQLException {
					resultSetUpdateCharacterStream(this, intVar0, readerVar1, intVar2);
				}

				public void updateCharacterStream(java.lang.String stringVar0,
						java.io.Reader readerVar1, int intVar2) throws SQLException {
					resultSetUpdateCharacterStream(this, stringVar0, readerVar1, intVar2);
				}

				public void updateClob(int intVar0, java.sql.Clob clobVar1) throws SQLException {
					resultSetUpdateClob(this, intVar0, clobVar1);
				}

				public void updateClob(java.lang.String stringVar0, java.sql.Clob clobVar1)
						throws SQLException {
					resultSetUpdateClob(this, stringVar0, clobVar1);
				}

				public void updateDate(int intVar0, java.sql.Date dateVar1) throws SQLException {
					resultSetUpdateDate(this, intVar0, dateVar1);
				}

				public void updateDate(java.lang.String stringVar0, java.sql.Date dateVar1)
						throws SQLException {
					resultSetUpdateDate(this, stringVar0, dateVar1);
				}

				public void updateDouble(int intVar0, double doubleVar1) throws SQLException {
					resultSetUpdateDouble(this, intVar0, doubleVar1);
				}

				public void updateDouble(java.lang.String stringVar0, double doubleVar1)
						throws SQLException {
					resultSetUpdateDouble(this, stringVar0, doubleVar1);
				}

				public void updateFloat(int intVar0, float floatVar1) throws SQLException {
					resultSetUpdateFloat(this, intVar0, floatVar1);
				}

				public void updateFloat(java.lang.String stringVar0, float floatVar1)
						throws SQLException {
					resultSetUpdateFloat(this, stringVar0, floatVar1);
				}

				public void updateInt(int intVar0, int intVar1) throws SQLException {
					resultSetUpdateInt(this, intVar0, intVar1);
				}

				public void updateInt(java.lang.String stringVar0, int intVar1) throws SQLException {
					resultSetUpdateInt(this, stringVar0, intVar1);
				}

				public void updateLong(int intVar0, long longVar1) throws SQLException {
					resultSetUpdateLong(this, intVar0, longVar1);
				}

				public void updateLong(java.lang.String stringVar0, long longVar1)
						throws SQLException {
					resultSetUpdateLong(this, stringVar0, longVar1);
				}

				public void updateNull(int intVar0) throws SQLException {
					resultSetUpdateNull(this, intVar0);
				}

				public void updateNull(java.lang.String stringVar0) throws SQLException {
					resultSetUpdateNull(this, stringVar0);
				}

				public void updateObject(int intVar0, java.lang.Object objectVar1)
						throws SQLException {
					resultSetUpdateObject(this, intVar0, objectVar1);
				}

				public void updateObject(java.lang.String stringVar0, java.lang.Object objectVar1)
						throws SQLException {
					resultSetUpdateObject(this, stringVar0, objectVar1);
				}

				public void updateObject(int intVar0, java.lang.Object objectVar1, int intVar2)
						throws SQLException {
					resultSetUpdateObject(this, intVar0, objectVar1, intVar2);
				}

				public void updateObject(java.lang.String stringVar0, java.lang.Object objectVar1,
						int intVar2) throws SQLException {
					resultSetUpdateObject(this, stringVar0, objectVar1, intVar2);
				}

				public void updateRef(int intVar0, java.sql.Ref refVar1) throws SQLException {
					resultSetUpdateRef(this, intVar0, refVar1);
				}

				public void updateRef(java.lang.String stringVar0, java.sql.Ref refVar1)
						throws SQLException {
					resultSetUpdateRef(this, stringVar0, refVar1);
				}

				public void updateRow() throws SQLException {
					resultSetUpdateRow(this);
				}

				public void updateShort(int intVar0, short shortVar1) throws SQLException {
					resultSetUpdateShort(this, intVar0, shortVar1);
				}

				public void updateShort(java.lang.String stringVar0, short shortVar1)
						throws SQLException {
					resultSetUpdateShort(this, stringVar0, shortVar1);
				}

				public void updateString(int intVar0, java.lang.String stringVar1)
						throws SQLException {
					resultSetUpdateString(this, intVar0, stringVar1);
				}

				public void updateString(java.lang.String stringVar0, java.lang.String stringVar1)
						throws SQLException {
					resultSetUpdateString(this, stringVar0, stringVar1);
				}

				public void updateTime(int intVar0, java.sql.Time timeVar1) throws SQLException {
					resultSetUpdateTime(this, intVar0, timeVar1);
				}

				public void updateTime(java.lang.String stringVar0, java.sql.Time timeVar1)
						throws SQLException {
					resultSetUpdateTime(this, stringVar0, timeVar1);
				}

				public void updateTimestamp(int intVar0, java.sql.Timestamp timestampVar1)
						throws SQLException {
					resultSetUpdateTimestamp(this, intVar0, timestampVar1);
				}

				public void updateTimestamp(java.lang.String stringVar0,
						java.sql.Timestamp timestampVar1) throws SQLException {
					resultSetUpdateTimestamp(this, stringVar0, timestampVar1);
				}

				public boolean wasNull() throws SQLException {
					return resultSetWasNull(this);
				}

				public int getHoldability() throws SQLException {
					return resultSetGetHoldability(this);
				}

				public Reader getNCharacterStream(int columnIndex) throws SQLException {
					return resultSetGetNCharacterStream(this, columnIndex);
				}

				public Reader getNCharacterStream(String columnLabel) throws SQLException {
					return resultSetGetNCharacterStream(this, columnLabel);
				}

				public NClob getNClob(int columnIndex) throws SQLException {
					return resultSetGetNClob(this, columnIndex);
				}

				public NClob getNClob(String columnLabel) throws SQLException {
					return resultSetGetNClob(this, columnLabel);
				}

				public String getNString(int columnIndex) throws SQLException {
					return resultSetGetNString(this, columnIndex);
				}

				public String getNString(String columnLabel) throws SQLException {
					return resultSetGetNString(this, columnLabel);
				}

				public Object getObject(int columnIndex, Map<String, Class<?>> map)
						throws SQLException {
					return resultSetGetObject(this, columnIndex, map);
				}

				public Object getObject(String columnLabel, Map<String, Class<?>> map)
						throws SQLException {
					return resultSetGetObject(this, columnLabel, map);
				}

				public RowId getRowId(int columnIndex) throws SQLException {
					return resultSetGetRowId(this, columnIndex);
				}

				public RowId getRowId(String columnLabel) throws SQLException {
					return resultSetGetRowId(this, columnLabel);
				}

				public SQLXML getSQLXML(int columnIndex) throws SQLException {
					return resultSetGetSQLXML(this, columnIndex);
				}

				public SQLXML getSQLXML(String columnLabel) throws SQLException {
					return resultSetGetSQLXML(this, columnLabel);
				}

				public boolean isClosed() throws SQLException {
					return resultSetIsClosed(this);
				}

				public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
					resultSetUpdateAsciiStream(this, columnIndex, x);

				}

				public void updateAsciiStream(String columnLabel, InputStream x)
						throws SQLException {
					resultSetUpdateAsciiStream(this, columnLabel, x);

				}

				public void updateAsciiStream(int columnIndex, InputStream x, long length)
						throws SQLException {
					resultSetUpdateAsciiStream(this, columnIndex, x, length);

				}

				public void updateAsciiStream(String columnLabel, InputStream x, long length)
						throws SQLException {
					resultSetUpdateAsciiStream(this, columnLabel, x, length);

				}

				public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
					resultSetUpdateBinaryStream(this, columnIndex, x);

				}

				public void updateBinaryStream(String columnLabel, InputStream x)
						throws SQLException {
					resultSetUpdateBinaryStream(this, columnLabel, x);

				}

				public void updateBinaryStream(int columnIndex, InputStream x, long length)
						throws SQLException {
					resultSetUpdateBinaryStream(this, columnIndex, x, length);

				}

				public void updateBinaryStream(String columnLabel, InputStream x, long length)
						throws SQLException {
					resultSetUpdateBinaryStream(this, columnLabel, x, length);

				}

				public void updateBlob(int columnIndex, InputStream inputStream)
						throws SQLException {
					resultSetUpdateBlob(this, columnIndex, inputStream);

				}

				public void updateBlob(String columnLabel, InputStream inputStream)
						throws SQLException {
					resultSetUpdateBlob(this, columnLabel, inputStream);

				}

				public void updateBlob(int columnIndex, InputStream inputStream, long length)
						throws SQLException {
					resultSetUpdateBlob(this, columnIndex, inputStream, length);

				}

				public void updateBlob(String columnLabel, InputStream inputStream, long length)
						throws SQLException {
					resultSetUpdateBlob(this, columnLabel, inputStream, length);

				}

				public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
					resultSetUpdateCharacterStream(this, columnIndex, x);

				}

				public void updateCharacterStream(String columnLabel, Reader reader)
						throws SQLException {
					resultSetUpdateCharacterStream(this, columnLabel, reader);

				}

				public void updateCharacterStream(int columnIndex, Reader x, long length)
						throws SQLException {
					resultSetUpdateCharacterStream(this, columnIndex, x, length);

				}

				public void updateCharacterStream(String columnLabel, Reader reader, long length)
						throws SQLException {
					resultSetUpdateCharacterStream(this, columnLabel, reader, length);

				}

				public void updateClob(int columnIndex, Reader reader) throws SQLException {
					resultSetUpdateClob(this, columnIndex, reader);

				}

				public void updateClob(String columnLabel, Reader reader) throws SQLException {
					resultSetUpdateClob(this, columnLabel, reader);

				}

				public void updateClob(int columnIndex, Reader reader, long length)
						throws SQLException {
					resultSetUpdateClob(this, columnIndex, reader, length);

				}

				public void updateClob(String columnLabel, Reader reader, long length)
						throws SQLException {
					resultSetUpdateClob(this, columnLabel, reader, length);

				}

				public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
					resultSetUpdateNCharacterStream(this, columnIndex, x);

				}

				public void updateNCharacterStream(String columnLabel, Reader reader)
						throws SQLException {
					resultSetUpdateNCharacterStream(this, columnLabel, reader);

				}

				public void updateNCharacterStream(int columnIndex, Reader x, long length)
						throws SQLException {
					resultSetUpdateNCharacterStream(this, columnIndex, x, length);

				}

				public void updateNCharacterStream(String columnLabel, Reader reader, long length)
						throws SQLException {
					resultSetUpdateNCharacterStream(this, columnLabel, reader, length);

				}

				public void updateNClob(int columnIndex, NClob clob) throws SQLException {
					resultSetUpdateNClob(this, columnIndex, clob);

				}

				public void updateNClob(String columnLabel, NClob clob) throws SQLException {
					resultSetUpdateNClob(this, columnLabel, clob);

				}

				public void updateNClob(int columnIndex, Reader reader) throws SQLException {
					resultSetUpdateNClob(this, columnIndex, reader);

				}

				public void updateNClob(String columnLabel, Reader reader) throws SQLException {
					resultSetUpdateNClob(this, columnLabel, reader);

				}

				public void updateNClob(int columnIndex, Reader reader, long length)
						throws SQLException {
					resultSetUpdateNClob(this, columnIndex, reader, length);

				}

				public void updateNClob(String columnLabel, Reader reader, long length)
						throws SQLException {
					resultSetUpdateNClob(this, columnLabel, reader, length);

				}

				public void updateNString(int columnIndex, String string) throws SQLException {
					resultSetUpdateNString(this, columnIndex, string);

				}

				public void updateNString(String columnLabel, String string) throws SQLException {
					resultSetUpdateNString(this, columnLabel, string);

				}

				public void updateRowId(int columnIndex, RowId x) throws SQLException {
					resultSetUpdateRowId(this, columnIndex, x);

				}

				public void updateRowId(String columnLabel, RowId x) throws SQLException {
					resultSetUpdateRowId(this, columnLabel, x);

				}

				public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
					resultSetUpdateSQLXML(this, columnIndex, xmlObject);

				}

				public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
					resultSetUpdateSQLXML(this, columnLabel, xmlObject);

				}

				public boolean isWrapperFor(Class<?> iface) throws SQLException {
					return resultSetIsWrapperFor(this, iface);
				}

				public <T> T unwrap(Class<T> iface) throws SQLException {
					return resultSetUnwrap(this, iface);
				}

				public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
					return resultSetGetObject(this, columnIndex, type);
				}

				public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
					return resultSetGetObject(this, columnLabel, type);
				}

				public void updateObject(int columnIndex, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
					resultSetUpdateObject(this, columnIndex, x, targetSqlType, scaleOrLength);
				}

				public void updateObject(String columnLabel, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
					resultSetUpdateObject(this, columnLabel, x, targetSqlType, scaleOrLength);
				}

				public void updateObject(int columnIndex, Object x, SQLType targetSqlType) throws SQLException {
					resultSetUpdateObject(this, columnIndex, x, targetSqlType);
				}

				public void updateObject(String columnLabel, Object x, SQLType targetSqlType) throws SQLException {
					resultSetUpdateObject(this, columnLabel, x, targetSqlType);
				}
			}

			protected Statement delegate;

			public InnerStatement(Statement delegate) {
				this.delegate = delegate;
				statementInit(this);
			}

			protected Statement getDelegate() {

				return delegate;
			}

			public void addBatch(java.lang.String stringVar0) throws SQLException {
				statementAddBatch(this, stringVar0);
			}

			public void cancel() throws SQLException {
				statementCancel(this);
			}

			public void clearBatch() throws SQLException {
				statementClearBatch(this);
			}

			public void clearWarnings() throws SQLException {
				statementClearWarnings(this);
			}

			public void close() throws SQLException {
				statementClose(this);
			}

			public boolean execute(java.lang.String stringVar0) throws SQLException {
				return statementExecute(this, stringVar0);
			}

			public boolean execute(java.lang.String stringVar0, int[] intVar1) throws SQLException {
				return statementExecute(this, stringVar0, intVar1);
			}

			public boolean execute(java.lang.String stringVar0, java.lang.String[] stringVar1)
					throws SQLException {
				return statementExecute(this, stringVar0, stringVar1);
			}

			public boolean execute(java.lang.String stringVar0, int intVar1) throws SQLException {
				return statementExecute(this, stringVar0, intVar1);
			}

			public int[] executeBatch() throws SQLException {
				return statementExecuteBatch(this);
			}

			public ResultSet executeQuery(java.lang.String stringVar0) throws SQLException {
				return statementExecuteQuery(this, stringVar0);
			}

			public int executeUpdate(java.lang.String stringVar0) throws SQLException {
				return statementExecuteUpdate(this, stringVar0);
			}

			public int executeUpdate(java.lang.String stringVar0, int[] intVar1)
					throws SQLException {
				return statementExecuteUpdate(this, stringVar0, intVar1);
			}

			public int executeUpdate(java.lang.String stringVar0, java.lang.String[] stringVar1)
					throws SQLException {
				return statementExecuteUpdate(this, stringVar0, stringVar1);
			}

			public int executeUpdate(java.lang.String stringVar0, int intVar1) throws SQLException {
				return statementExecuteUpdate(this, stringVar0, intVar1);
			}

			public Connection getConnection() throws SQLException {
				return InnerConnection.this;
			}

			public int getFetchDirection() throws SQLException {
				return statementGetFetchDirection(this);
			}

			public int getFetchSize() throws SQLException {
				return statementGetFetchSize(this);
			}

			public ResultSet getGeneratedKeys() throws SQLException {
				return statementGetGeneratedKeys(this);
			}

			public int getMaxFieldSize() throws SQLException {
				return statementGetMaxFieldSize(this);
			}

			public int getMaxRows() throws SQLException {
				return statementGetMaxRows(this);
			}

			public boolean getMoreResults() throws SQLException {
				return statementGetMoreResults(this);
			}

			public boolean getMoreResults(int intVar0) throws SQLException {
				return statementGetMoreResults(this, intVar0);
			}

			public int getQueryTimeout() throws SQLException {
				return statementGetQueryTimeout(this);
			}

			public ResultSet getResultSet() throws SQLException {
				return statementGetResultSet(this);
			}

			public int getResultSetConcurrency() throws SQLException {
				return statementGetResultSetConcurrency(this);
			}

			public int getResultSetHoldability() throws SQLException {
				return statementGetResultSetHoldability(this);
			}

			public int getResultSetType() throws SQLException {
				return statementGetResultSetType(this);
			}

			public int getUpdateCount() throws SQLException {
				return statementGetUpdateCount(this);
			}

			public java.sql.SQLWarning getWarnings() throws SQLException {
				return statementGetWarnings(this);
			}

			public void setCursorName(java.lang.String stringVar0) throws SQLException {
				statementSetCursorName(this, stringVar0);
			}

			public void setEscapeProcessing(boolean booleanVar0) throws SQLException {
				statementSetEscapeProcessing(this, booleanVar0);
			}

			public void setFetchDirection(int intVar0) throws SQLException {
				statementSetFetchDirection(this, intVar0);
			}

			public void setFetchSize(int intVar0) throws SQLException {
				statementSetFetchSize(this, intVar0);
			}

			public void setMaxFieldSize(int intVar0) throws SQLException {
				statementSetMaxFieldSize(this, intVar0);
			}

			public void setMaxRows(int intVar0) throws SQLException {
				statementSetMaxRows(this, intVar0);
			}

			public void setQueryTimeout(int intVar0) throws SQLException {
				statementSetQueryTimeout(this, intVar0);
			}

			public boolean isClosed() throws SQLException {
				return statementIsClosed(this);
			}

			public boolean isPoolable() throws SQLException {
				return statementIsPoolable(this);
			}

			public void setPoolable(boolean poolable) throws SQLException {
				statementSetPoolable(this, poolable);

			}

			public boolean isWrapperFor(Class<?> iface) throws SQLException {
				return statementIsWrapperFor(this, iface);
			}

			public <T> T unwrap(Class<T> iface) throws SQLException {
				return statementUnwrap(this, iface);
			}

			public void closeOnCompletion() throws SQLException {
				statementCloseOnCompletion(this);
			}

			public boolean isCloseOnCompletion() throws SQLException {
				return statementIsCloseOnCompletion(this);
			}
		}

		protected class InnerPreparedStatement extends InnerStatement implements PreparedStatement {
			protected PreparedStatement delegate;

			public InnerPreparedStatement(PreparedStatement delegate) {
				super(delegate);
				this.delegate = delegate;
				preparedStatementInit(this);
			}

			@Override
			protected PreparedStatement getDelegate() {
				return delegate;
			}

			public void addBatch() throws SQLException {
				preparedStatementAddBatch(this);
			}

			public void clearParameters() throws SQLException {
				preparedStatementClearParameters(this);
			}

			public boolean execute() throws SQLException {
				return preparedStatementExecute(this);
			}

			public ResultSet executeQuery() throws SQLException {
				return preparedStatementExecuteQuery(this);
			}

			public int executeUpdate() throws SQLException {
				return preparedStatementExecuteUpdate(this);
			}

			public java.sql.ResultSetMetaData getMetaData() throws SQLException {
				return preparedStatementGetMetaData(this);
			}

			public java.sql.ParameterMetaData getParameterMetaData() throws SQLException {
				return preparedStatementGetParameterMetaData(this);
			}

			public void setArray(int intVar0, java.sql.Array arrayVar1) throws SQLException {
				preparedStatementSetArray(this, intVar0, arrayVar1);
			}

			public void setAsciiStream(int intVar0, java.io.InputStream inputStreamVar1, int intVar2)
					throws SQLException {
				preparedStatementSetAsciiStream(this, intVar0, inputStreamVar1, intVar2);
			}

			public void setBigDecimal(int intVar0, java.math.BigDecimal bigDecimalVar1)
					throws SQLException {
				preparedStatementSetBigDecimal(this, intVar0, bigDecimalVar1);
			}

			public void setBinaryStream(int intVar0, java.io.InputStream inputStreamVar1,
					int intVar2) throws SQLException {
				preparedStatementSetBinaryStream(this, intVar0, inputStreamVar1, intVar2);
			}

			public void setBlob(int intVar0, java.sql.Blob blobVar1) throws SQLException {
				preparedStatementSetBlob(this, intVar0, blobVar1);
			}

			public void setBoolean(int intVar0, boolean booleanVar1) throws SQLException {
				preparedStatementSetBoolean(this, intVar0, booleanVar1);
			}

			public void setByte(int intVar0, byte byteVar1) throws SQLException {
				preparedStatementSetByte(this, intVar0, byteVar1);
			}

			public void setBytes(int intVar0, byte[] byteVar1) throws SQLException {
				preparedStatementSetBytes(this, intVar0, byteVar1);
			}

			public void setCharacterStream(int intVar0, java.io.Reader readerVar1, int intVar2)
					throws SQLException {
				preparedStatementSetCharacterStream(this, intVar0, readerVar1, intVar2);
			}

			public void setClob(int intVar0, java.sql.Clob clobVar1) throws SQLException {
				preparedStatementSetClob(this, intVar0, clobVar1);
			}

			public void setDate(int intVar0, java.sql.Date dateVar1) throws SQLException {
				preparedStatementSetDate(this, intVar0, dateVar1);
			}

			public void setDate(int intVar0, java.sql.Date dateVar1, java.util.Calendar calendarVar2)
					throws SQLException {
				preparedStatementSetDate(this, intVar0, dateVar1, calendarVar2);
			}

			public void setDouble(int intVar0, double doubleVar1) throws SQLException {
				preparedStatementSetDouble(this, intVar0, doubleVar1);
			}

			public void setFloat(int intVar0, float floatVar1) throws SQLException {
				preparedStatementSetFloat(this, intVar0, floatVar1);
			}

			public void setInt(int intVar0, int intVar1) throws SQLException {
				preparedStatementSetInt(this, intVar0, intVar1);
			}

			public void setLong(int intVar0, long longVar1) throws SQLException {
				preparedStatementSetLong(this, intVar0, longVar1);
			}

			public void setNull(int intVar0, int intVar1) throws SQLException {
				preparedStatementSetNull(this, intVar0, intVar1);
			}

			public void setNull(int intVar0, int intVar1, java.lang.String stringVar2)
					throws SQLException {
				preparedStatementSetNull(this, intVar0, intVar1, stringVar2);
			}

			public void setObject(int intVar0, java.lang.Object objectVar1) throws SQLException {
				preparedStatementSetObject(this, intVar0, objectVar1);
			}

			public void setObject(int intVar0, java.lang.Object objectVar1, int intVar2)
					throws SQLException {
				preparedStatementSetObject(this, intVar0, objectVar1, intVar2);
			}

			public void setObject(int intVar0, java.lang.Object objectVar1, int intVar2, int intVar3)
					throws SQLException {
				preparedStatementSetObject(this, intVar0, objectVar1, intVar2, intVar3);
			}

			public void setRef(int intVar0, java.sql.Ref refVar1) throws SQLException {
				preparedStatementSetRef(this, intVar0, refVar1);
			}

			public void setShort(int intVar0, short shortVar1) throws SQLException {
				preparedStatementSetShort(this, intVar0, shortVar1);
			}

			public void setString(int intVar0, java.lang.String stringVar1) throws SQLException {
				preparedStatementSetString(this, intVar0, stringVar1);
			}

			public void setTime(int intVar0, java.sql.Time timeVar1) throws SQLException {
				preparedStatementSetTime(this, intVar0, timeVar1);
			}

			public void setTime(int intVar0, java.sql.Time timeVar1, java.util.Calendar calendarVar2)
					throws SQLException {
				preparedStatementSetTime(this, intVar0, timeVar1, calendarVar2);
			}

			public void setTimestamp(int intVar0, java.sql.Timestamp timestampVar1)
					throws SQLException {
				preparedStatementSetTimestamp(this, intVar0, timestampVar1);
			}

			public void setTimestamp(int intVar0, java.sql.Timestamp timestampVar1,
					java.util.Calendar calendarVar2) throws SQLException {
				preparedStatementSetTimestamp(this, intVar0, timestampVar1, calendarVar2);
			}

			public void setURL(int intVar0, java.net.URL urlvar1) throws SQLException {
				preparedStatementSetURL(this, intVar0, urlvar1);
			}

			public void setUnicodeStream(int intVar0, java.io.InputStream inputStreamVar1,
					int intVar2) throws SQLException {
				preparedStatementSetUnicodeStream(this, intVar0, inputStreamVar1, intVar2);
			}

			public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
				preparedStatementSetAsciiStream(this, parameterIndex, x);

			}

			public void setAsciiStream(int parameterIndex, InputStream x, long length)
					throws SQLException {
				preparedStatementSetAsciiStream(this, parameterIndex, x, length);

			}

			public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
				preparedStatementSetBinaryStream(this, parameterIndex, x);

			}

			public void setBinaryStream(int parameterIndex, InputStream x, long length)
					throws SQLException {
				preparedStatementSetBinaryStream(this, parameterIndex, x, length);

			}

			public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
				preparedStatementSetBlob(this, parameterIndex, inputStream);

			}

			public void setBlob(int parameterIndex, InputStream inputStream, long length)
					throws SQLException {
				preparedStatementSetBlob(this, parameterIndex, inputStream, length);

			}

			public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
				preparedStatementSetCharacterStream(this, parameterIndex, reader);

			}

			public void setCharacterStream(int parameterIndex, Reader reader, long length)
					throws SQLException {
				preparedStatementSetCharacterStream(this, parameterIndex, reader, length);

			}

			public void setClob(int parameterIndex, Reader reader) throws SQLException {
				preparedStatementSetClob(this, parameterIndex, reader);

			}

			public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
				preparedStatementSetClob(this, parameterIndex, reader, length);

			}

			public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
				preparedStatementSetNCharacterStream(this, parameterIndex, value);

			}

			public void setNCharacterStream(int parameterIndex, Reader value, long length)
					throws SQLException {
				preparedStatementSetNCharacterStream(this, parameterIndex, value, length);

			}

			public void setNClob(int parameterIndex, NClob value) throws SQLException {
				preparedStatementSetNClob(this, parameterIndex, value);

			}

			public void setNClob(int parameterIndex, Reader reader) throws SQLException {
				preparedStatementSetNClob(this, parameterIndex, reader);

			}

			public void setNClob(int parameterIndex, Reader reader, long length)
					throws SQLException {
				preparedStatementSetNClob(this, parameterIndex, reader, length);

			}

			public void setNString(int parameterIndex, String value) throws SQLException {
				preparedStatementSetNString(this, parameterIndex, value);

			}

			public void setRowId(int parameterIndex, RowId x) throws SQLException {
				preparedStatementSetRowId(this, parameterIndex, x);

			}

			public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
				preparedStatementSetSQLXML(this, parameterIndex, xmlObject);

			}

		}

		protected class InnerCallableStatement extends InnerPreparedStatement implements
				CallableStatement {
			protected CallableStatement delegate;

			public InnerCallableStatement(CallableStatement delegate) {
				super(delegate);
				this.delegate = delegate;
				callableStatementInit(this);
			}

			@Override
			protected CallableStatement getDelegate() {
				return delegate;
			}

			public java.sql.Array getArray(int intVar0) throws SQLException {
				return callableStatementGetArray(this, intVar0);
			}

			public java.sql.Array getArray(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetArray(this, stringVar0);
			}

			public java.math.BigDecimal getBigDecimal(int intVar0) throws SQLException {
				return callableStatementGetBigDecimal(this, intVar0);
			}

			public java.math.BigDecimal getBigDecimal(java.lang.String stringVar0)
					throws SQLException {
				return callableStatementGetBigDecimal(this, stringVar0);
			}

			public java.math.BigDecimal getBigDecimal(int intVar0, int intVar1) throws SQLException {
				return callableStatementGetBigDecimal(this, intVar0, intVar1);
			}

			public java.sql.Blob getBlob(int intVar0) throws SQLException {
				return callableStatementGetBlob(this, intVar0);
			}

			public java.sql.Blob getBlob(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetBlob(this, stringVar0);
			}

			public boolean getBoolean(int intVar0) throws SQLException {
				return callableStatementGetBoolean(this, intVar0);
			}

			public boolean getBoolean(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetBoolean(this, stringVar0);
			}

			public byte getByte(int intVar0) throws SQLException {
				return callableStatementGetByte(this, intVar0);
			}

			public byte getByte(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetByte(this, stringVar0);
			}

			public byte[] getBytes(int intVar0) throws SQLException {
				return callableStatementGetBytes(this, intVar0);
			}

			public byte[] getBytes(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetBytes(this, stringVar0);
			}

			public java.sql.Clob getClob(int intVar0) throws SQLException {
				return callableStatementGetClob(this, intVar0);
			}

			public java.sql.Clob getClob(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetClob(this, stringVar0);
			}

			public java.sql.Date getDate(int intVar0) throws SQLException {
				return callableStatementGetDate(this, intVar0);
			}

			public java.sql.Date getDate(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetDate(this, stringVar0);
			}

			public java.sql.Date getDate(int intVar0, java.util.Calendar calendarVar1)
					throws SQLException {
				return callableStatementGetDate(this, intVar0, calendarVar1);
			}

			public java.sql.Date getDate(java.lang.String stringVar0,
					java.util.Calendar calendarVar1) throws SQLException {
				return callableStatementGetDate(this, stringVar0, calendarVar1);
			}

			public double getDouble(int intVar0) throws SQLException {
				return callableStatementGetDouble(this, intVar0);
			}

			public double getDouble(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetDouble(this, stringVar0);
			}

			public float getFloat(int intVar0) throws SQLException {
				return callableStatementGetFloat(this, intVar0);
			}

			public float getFloat(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetFloat(this, stringVar0);
			}

			public int getInt(int intVar0) throws SQLException {
				return callableStatementGetInt(this, intVar0);
			}

			public int getInt(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetInt(this, stringVar0);
			}

			public long getLong(int intVar0) throws SQLException {
				return callableStatementGetLong(this, intVar0);
			}

			public long getLong(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetLong(this, stringVar0);
			}

			public java.lang.Object getObject(int intVar0) throws SQLException {
				return callableStatementGetObject(this, intVar0);
			}

			public java.lang.Object getObject(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetObject(this, stringVar0);
			}

			public java.lang.Object getObject(int intVar0, java.util.Map<String, Class<?>> mapVar1)
					throws SQLException {
				return callableStatementGetObject(this, intVar0, mapVar1);
			}

			public java.lang.Object getObject(java.lang.String stringVar0, java.util.Map<String, Class<?>> mapVar1)
					throws SQLException {
				return callableStatementGetObject(this, stringVar0, mapVar1);
			}

			public java.sql.Ref getRef(int intVar0) throws SQLException {
				return callableStatementGetRef(this, intVar0);
			}

			public java.sql.Ref getRef(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetRef(this, stringVar0);
			}

			public short getShort(int intVar0) throws SQLException {
				return callableStatementGetShort(this, intVar0);
			}

			public short getShort(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetShort(this, stringVar0);
			}

			public java.lang.String getString(int intVar0) throws SQLException {
				return callableStatementGetString(this, intVar0);
			}

			public java.lang.String getString(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetString(this, stringVar0);
			}

			public java.sql.Time getTime(int intVar0) throws SQLException {
				return callableStatementGetTime(this, intVar0);
			}

			public java.sql.Time getTime(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetTime(this, stringVar0);
			}

			public java.sql.Time getTime(int intVar0, java.util.Calendar calendarVar1)
					throws SQLException {
				return callableStatementGetTime(this, intVar0, calendarVar1);
			}

			public java.sql.Time getTime(java.lang.String stringVar0,
					java.util.Calendar calendarVar1) throws SQLException {
				return callableStatementGetTime(this, stringVar0, calendarVar1);
			}

			public java.sql.Timestamp getTimestamp(int intVar0) throws SQLException {
				return callableStatementGetTimestamp(this, intVar0);
			}

			public java.sql.Timestamp getTimestamp(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetTimestamp(this, stringVar0);
			}

			public java.sql.Timestamp getTimestamp(int intVar0, java.util.Calendar calendarVar1)
					throws SQLException {
				return callableStatementGetTimestamp(this, intVar0, calendarVar1);
			}

			public java.sql.Timestamp getTimestamp(java.lang.String stringVar0,
					java.util.Calendar calendarVar1) throws SQLException {
				return callableStatementGetTimestamp(this, stringVar0, calendarVar1);
			}

			public java.net.URL getURL(int intVar0) throws SQLException {
				return callableStatementGetURL(this, intVar0);
			}

			public java.net.URL getURL(java.lang.String stringVar0) throws SQLException {
				return callableStatementGetURL(this, stringVar0);
			}

			public void registerOutParameter(int intVar0, int intVar1) throws SQLException {
				callableStatementRegisterOutParameter(this, intVar0, intVar1);
			}

			public void registerOutParameter(java.lang.String stringVar0, int intVar1)
					throws SQLException {
				callableStatementRegisterOutParameter(this, stringVar0, intVar1);
			}

			public void registerOutParameter(int intVar0, int intVar1, int intVar2)
					throws SQLException {
				callableStatementRegisterOutParameter(this, intVar0, intVar1, intVar2);
			}

			public void registerOutParameter(int intVar0, int intVar1, java.lang.String stringVar2)
					throws SQLException {
				callableStatementRegisterOutParameter(this, intVar0, intVar1, stringVar2);
			}

			public void registerOutParameter(java.lang.String stringVar0, int intVar1, int intVar2)
					throws SQLException {
				callableStatementRegisterOutParameter(this, stringVar0, intVar1, intVar2);
			}

			public void registerOutParameter(java.lang.String stringVar0, int intVar1,
					java.lang.String stringVar2) throws SQLException {
				callableStatementRegisterOutParameter(this, stringVar0, intVar1, stringVar2);
			}

			public void setAsciiStream(java.lang.String stringVar0,
					java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
				callableStatementSetAsciiStream(this, stringVar0, inputStreamVar1, intVar2);
			}

			public void setBigDecimal(java.lang.String stringVar0,
					java.math.BigDecimal bigDecimalVar1) throws SQLException {
				callableStatementSetBigDecimal(this, stringVar0, bigDecimalVar1);
			}

			public void setBinaryStream(java.lang.String stringVar0,
					java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
				callableStatementSetBinaryStream(this, stringVar0, inputStreamVar1, intVar2);
			}

			public void setBoolean(java.lang.String stringVar0, boolean booleanVar1)
					throws SQLException {
				callableStatementSetBoolean(this, stringVar0, booleanVar1);
			}

			public void setByte(java.lang.String stringVar0, byte byteVar1) throws SQLException {
				callableStatementSetByte(this, stringVar0, byteVar1);
			}

			public void setBytes(java.lang.String stringVar0, byte[] byteVar1) throws SQLException {
				callableStatementSetBytes(this, stringVar0, byteVar1);
			}

			public void setCharacterStream(java.lang.String stringVar0, java.io.Reader readerVar1,
					int intVar2) throws SQLException {
				callableStatementSetCharacterStream(this, stringVar0, readerVar1, intVar2);
			}

			public void setDate(java.lang.String stringVar0, java.sql.Date dateVar1)
					throws SQLException {
				callableStatementSetDate(this, stringVar0, dateVar1);
			}

			public void setDate(java.lang.String stringVar0, java.sql.Date dateVar1,
					java.util.Calendar calendarVar2) throws SQLException {
				callableStatementSetDate(this, stringVar0, dateVar1, calendarVar2);
			}

			public void setDouble(java.lang.String stringVar0, double doubleVar1)
					throws SQLException {
				callableStatementSetDouble(this, stringVar0, doubleVar1);
			}

			public void setFloat(java.lang.String stringVar0, float floatVar1) throws SQLException {
				callableStatementSetFloat(this, stringVar0, floatVar1);
			}

			public void setInt(java.lang.String stringVar0, int intVar1) throws SQLException {
				callableStatementSetInt(this, stringVar0, intVar1);
			}

			public void setLong(java.lang.String stringVar0, long longVar1) throws SQLException {
				callableStatementSetLong(this, stringVar0, longVar1);
			}

			public void setNull(java.lang.String stringVar0, int intVar1) throws SQLException {
				callableStatementSetNull(this, stringVar0, intVar1);
			}

			public void setNull(java.lang.String stringVar0, int intVar1,
					java.lang.String stringVar2) throws SQLException {
				callableStatementSetNull(this, stringVar0, intVar1, stringVar2);
			}

			public void setObject(java.lang.String stringVar0, java.lang.Object objectVar1)
					throws SQLException {
				callableStatementSetObject(this, stringVar0, objectVar1);
			}

			public void setObject(java.lang.String stringVar0, java.lang.Object objectVar1,
					int intVar2) throws SQLException {
				callableStatementSetObject(this, stringVar0, objectVar1, intVar2);
			}

			public void setObject(java.lang.String stringVar0, java.lang.Object objectVar1,
					int intVar2, int intVar3) throws SQLException {
				callableStatementSetObject(this, stringVar0, objectVar1, intVar2, intVar3);
			}

			public void setShort(java.lang.String stringVar0, short shortVar1) throws SQLException {
				callableStatementSetShort(this, stringVar0, shortVar1);
			}

			public void setString(java.lang.String stringVar0, java.lang.String stringVar1)
					throws SQLException {
				callableStatementSetString(this, stringVar0, stringVar1);
			}

			public void setTime(java.lang.String stringVar0, java.sql.Time timeVar1)
					throws SQLException {
				callableStatementSetTime(this, stringVar0, timeVar1);
			}

			public void setTime(java.lang.String stringVar0, java.sql.Time timeVar1,
					java.util.Calendar calendarVar2) throws SQLException {
				callableStatementSetTime(this, stringVar0, timeVar1, calendarVar2);
			}

			public void setTimestamp(java.lang.String stringVar0, java.sql.Timestamp timestampVar1)
					throws SQLException {
				callableStatementSetTimestamp(this, stringVar0, timestampVar1);
			}

			public void setTimestamp(java.lang.String stringVar0, java.sql.Timestamp timestampVar1,
					java.util.Calendar calendarVar2) throws SQLException {
				callableStatementSetTimestamp(this, stringVar0, timestampVar1, calendarVar2);
			}

			public void setURL(java.lang.String stringVar0, java.net.URL urlvar1)
					throws SQLException {
				callableStatementSetURL(this, stringVar0, urlvar1);
			}

			public boolean wasNull() throws SQLException {
				return callableStatementWasNull(this);
			}

			public Reader getCharacterStream(int parameterIndex) throws SQLException {
				return callableStatementGetCharacterStream(this, parameterIndex);
			}

			public Reader getCharacterStream(String parameterName) throws SQLException {
				return callableStatementGetCharacterStream(this, parameterName);
			}

			public Reader getNCharacterStream(int parameterIndex) throws SQLException {
				return callableStatementGetNCharacterStream(this, parameterIndex);
			}

			public Reader getNCharacterStream(String parameterName) throws SQLException {
				return callableStatementGetNCharacterStream(this, parameterName);
			}

			public NClob getNClob(int parameterIndex) throws SQLException {
				return callableStatementGetNClob(this, parameterIndex);
			}

			public NClob getNClob(String parameterName) throws SQLException {
				return callableStatementGetNClob(this, parameterName);
			}

			public String getNString(int parameterIndex) throws SQLException {
				return callableStatementGetNString(this, parameterIndex);
			}

			public String getNString(String parameterName) throws SQLException {
				return callableStatementGetNString(this, parameterName);
			}

			public RowId getRowId(int parameterIndex) throws SQLException {
				return callableStatementGetRowId(this, parameterIndex);
			}

			public RowId getRowId(String parameterName) throws SQLException {
				return callableStatementGetRowId(this, parameterName);
			}

			public SQLXML getSQLXML(int parameterIndex) throws SQLException {
				return callableStatementGetSQLXML(this, parameterIndex);
			}

			public SQLXML getSQLXML(String parameterName) throws SQLException {
				return callableStatementGetSQLXML(this, parameterName);
			}

			public void setAsciiStream(String parameterName, InputStream x) throws SQLException {
				callableStatementSetAsciiStream(this, parameterName, x);
			}

			public void setAsciiStream(String parameterName, InputStream x, long length)
					throws SQLException {
				callableStatementSetAsciiStream(this, parameterName, x, length);
			}

			public void setBinaryStream(String parameterName, InputStream x) throws SQLException {
				callableStatementSetBinaryStream(this, parameterName, x);
			}

			public void setBinaryStream(String parameterName, InputStream x, long length)
					throws SQLException {
				callableStatementSetBinaryStream(this, parameterName, x, length);
			}

			public void setBlob(String parameterName, Blob x) throws SQLException {
				callableStatementSetBlob(this, parameterName, x);
			}

			public void setBlob(String parameterName, InputStream inputStream) throws SQLException {
				callableStatementSetBlob(this, parameterName, inputStream);
			}

			public void setBlob(String parameterName, InputStream inputStream, long length)
					throws SQLException {
				callableStatementSetBlob(this, parameterName, inputStream, length);
			}

			public void setCharacterStream(String parameterName, Reader reader) throws SQLException {
				callableStatementSetCharacterStream(this, parameterName, reader);
			}

			public void setCharacterStream(String parameterName, Reader reader, long length)
					throws SQLException {
				callableStatementSetCharacterStream(this, parameterName, reader, length);
			}

			public void setClob(String parameterName, Clob x) throws SQLException {
				callableStatementSetClob(this, parameterName, x);
			}

			public void setClob(String parameterName, Reader reader) throws SQLException {
				callableStatementSetClob(this, parameterName, reader);
			}

			public void setClob(String parameterName, Reader reader, long length)
					throws SQLException {
				callableStatementSetClob(this, parameterName, reader, length);
			}

			public void setNCharacterStream(String parameterName, Reader value) throws SQLException {
				callableStatementSetNCharacterStream(this, parameterName, value);
			}

			public void setNCharacterStream(String parameterName, Reader value, long length)
					throws SQLException {
				callableStatementSetNCharacterStream(this, parameterName, value, length);
			}

			public void setNClob(String parameterName, NClob value) throws SQLException {
				callableStatementSetNClob(this, parameterName, value);
			}

			public void setNClob(String parameterName, Reader reader) throws SQLException {
				callableStatementSetNClob(this, parameterName, reader);
			}

			public void setNClob(String parameterName, Reader reader, long length)
					throws SQLException {
				callableStatementSetNClob(this, parameterName, reader, length);
			}

			public void setNString(String parameterName, String value) throws SQLException {
				callableStatementSetNString(this, parameterName, value);
			}

			public void setRowId(String parameterName, RowId x) throws SQLException {
				callableStatementSetRowId(this, parameterName, x);
			}

			public void setSQLXML(String parameterName, SQLXML xmlObject) throws SQLException {
				callableStatementSetSQLXML(this, parameterName, xmlObject);
			}

			public void closeOnCompletion() throws SQLException {
				callableStatementCloseOnCompletion(this);
			}

			public boolean isCloseOnCompletion() throws SQLException {
				return callableStatementIsCloseOnCompletion(this);
			}

			public <T> T getObject(int parameterIndex, Class<T> type) throws SQLException {
				return callableStatementGetObject(this, parameterIndex, type);
			}

			public <T> T getObject(String parameterName, Class<T> type) throws SQLException {
				return callableStatementGetObject(this, parameterName, type);
			}
		}

		protected Connection delegate;

		public InnerConnection(Connection delegate) {
			this.delegate = delegate;
			connectionInit(this);
		}

		protected Connection getDelegate() {

			return delegate;
		}

		public void clearWarnings() throws SQLException {
			connectionClearWarnings(this);
		}

		public void close() throws SQLException {
			connectionClose(this);
		}

		public void commit() throws SQLException {
			connectionCommit(this);
		}

		public Statement createStatement() throws SQLException {
			return connectionCreateStatement(this);
		}

		public Statement createStatement(int intVar0, int intVar1) throws SQLException {
			return connectionCreateStatement(this, intVar0, intVar1);
		}

		public Statement createStatement(int intVar0, int intVar1, int intVar2) throws SQLException {
			return connectionCreateStatement(this, intVar0, intVar1, intVar2);
		}

		public boolean getAutoCommit() throws SQLException {
			return connectionGetAutoCommit(this);
		}

		public java.lang.String getCatalog() throws SQLException {
			return connectionGetCatalog(this);
		}

		public int getHoldability() throws SQLException {
			return connectionGetHoldability(this);
		}

		public java.sql.DatabaseMetaData getMetaData() throws SQLException {
			return connectionGetMetaData(this);
		}

		public int getTransactionIsolation() throws SQLException {
			return connectionGetTransactionIsolation(this);
		}

		public java.util.Map<String, Class<?>> getTypeMap() throws SQLException {
			return connectionGetTypeMap(this);
		}

		public java.sql.SQLWarning getWarnings() throws SQLException {
			return connectionGetWarnings(this);
		}

		public boolean isClosed() throws SQLException {
			return connectionIsClosed(this);
		}

		public boolean isReadOnly() throws SQLException {
			return connectionIsReadOnly(this);
		}

		public java.lang.String nativeSQL(java.lang.String stringVar0) throws SQLException {
			return connectionNativeSQL(this, stringVar0);
		}

		public CallableStatement prepareCall(java.lang.String stringVar0) throws SQLException {
			return connectionPrepareCall(this, stringVar0);
		}

		public CallableStatement prepareCall(java.lang.String stringVar0, int intVar1, int intVar2)
				throws SQLException {
			return connectionPrepareCall(this, stringVar0, intVar1, intVar2);
		}

		public CallableStatement prepareCall(java.lang.String stringVar0, int intVar1, int intVar2,
				int intVar3) throws SQLException {
			return connectionPrepareCall(this, stringVar0, intVar1, intVar2, intVar3);
		}

		public PreparedStatement prepareStatement(java.lang.String stringVar0) throws SQLException {
			return connectionPrepareStatement(this, stringVar0);
		}

		public PreparedStatement prepareStatement(java.lang.String stringVar0, int[] intVar1)
				throws SQLException {
			return connectionPrepareStatement(this, stringVar0, intVar1);
		}

		public PreparedStatement prepareStatement(java.lang.String stringVar0,
				java.lang.String[] stringVar1) throws SQLException {
			return connectionPrepareStatement(this, stringVar0, stringVar1);
		}

		public PreparedStatement prepareStatement(java.lang.String stringVar0, int intVar1)
				throws SQLException {
			return connectionPrepareStatement(this, stringVar0, intVar1);
		}

		public PreparedStatement prepareStatement(java.lang.String stringVar0, int intVar1,
				int intVar2) throws SQLException {
			return connectionPrepareStatement(this, stringVar0, intVar1, intVar2);
		}

		public PreparedStatement prepareStatement(java.lang.String stringVar0, int intVar1,
				int intVar2, int intVar3) throws SQLException {
			return connectionPrepareStatement(this, stringVar0, intVar1, intVar2, intVar3);
		}

		public void releaseSavepoint(java.sql.Savepoint savepointVar0) throws SQLException {
			connectionReleaseSavepoint(this, savepointVar0);
		}

		public void rollback() throws SQLException {
			connectionRollback(this);
		}

		public void rollback(java.sql.Savepoint savepointVar0) throws SQLException {
			connectionRollback(this, savepointVar0);
		}

		public void setAutoCommit(boolean booleanVar0) throws SQLException {
			connectionSetAutoCommit(this, booleanVar0);
		}

		public void setCatalog(java.lang.String stringVar0) throws SQLException {
			connectionSetCatalog(this, stringVar0);
		}

		public void setHoldability(int intVar0) throws SQLException {
			connectionSetHoldability(this, intVar0);
		}

		public void setReadOnly(boolean booleanVar0) throws SQLException {
			connectionSetReadOnly(this, booleanVar0);
		}

		public java.sql.Savepoint setSavepoint() throws SQLException {
			return connectionSetSavepoint(this);
		}

		public java.sql.Savepoint setSavepoint(java.lang.String stringVar0) throws SQLException {
			return connectionSetSavepoint(this, stringVar0);
		}

		public void setTransactionIsolation(int intVar0) throws SQLException {
			connectionSetTransactionIsolation(this, intVar0);
		}

		public void setTypeMap(java.util.Map<String, Class<?>> mapVar0) throws SQLException {
			connectionSetTypeMap(this, mapVar0);
		}

		public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
			return connectionCreateArrayOf(this, typeName, elements);
		}

		public Blob createBlob() throws SQLException {
			return connectionCreateBlob(this);
		}

		public Clob createClob() throws SQLException {
			return connectionCreateClob(this);
		}

		public NClob createNClob() throws SQLException {
			return connectionCreateNClob(this);
		}

		public SQLXML createSQLXML() throws SQLException {
			return connectionCreateSQLXML(this);
		}

		public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
			return connectionCreateStruct(this, typeName, attributes);
		}

		public Properties getClientInfo() throws SQLException {
			return connectionGetClientInfo(this);
		}

		public String getClientInfo(String name) throws SQLException  {
			return connectionGetClientInfo(this, name);
		}

		public boolean isValid(int timeout) throws SQLException {
			return connectionIsValid(this, timeout);
		}

		public void setClientInfo(Properties properties) throws SQLClientInfoException {
			connectionSetClientInfo(this, properties);

		}

		public void setClientInfo(String name, String value) throws SQLClientInfoException {
			connectionSetClientInfo(this, name, value);

		}

		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return connectionIsWrapperFor(this, iface);
		}

		public <T> T unwrap(Class<T> iface) throws SQLException {
			return connectionUnwrap(this, iface);
		}

		public void setSchema(String schema) throws SQLException {
			connectionSetSchema(this, schema);
		}

		public String getSchema() throws SQLException {
			return connectionGetSchema(this);
		}

		public void abort(Executor executor) throws SQLException {
			connectionAbort(this, executor);
		}

		public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
			connectionSetNetworkTimeout(this, executor, milliseconds);
		}

		public int getNetworkTimeout() throws SQLException {
			return connectionGetNetworkTimeout(this);
		}
	}

	// The inner class methods --------------------------------------
	protected void connectionInit(InnerConnection connection) {
	}

	protected void connectionClearWarnings(InnerConnection connection) throws SQLException {
		connection.getDelegate().clearWarnings();
	}

	protected void connectionClose(InnerConnection connection) throws SQLException {
		connection.getDelegate().close();
	}

	protected void connectionCommit(InnerConnection connection) throws SQLException {
		connection.getDelegate().commit();
	}

	protected Statement connectionCreateStatement(InnerConnection connection) throws SQLException {
		return connection.new InnerStatement(connection.getDelegate().createStatement());
	}

	protected Statement connectionCreateStatement(InnerConnection connection, int intVar0,
			int intVar1) throws SQLException {
		return connection.new InnerStatement(connection.getDelegate().createStatement(intVar0,
				intVar1));
	}

	protected Statement connectionCreateStatement(InnerConnection connection, int intVar0,
			int intVar1, int intVar2) throws SQLException {
		return connection.new InnerStatement(connection.getDelegate().createStatement(intVar0,
				intVar1, intVar2));
	}

	protected boolean connectionGetAutoCommit(InnerConnection connection) throws SQLException {
		return connection.getDelegate().getAutoCommit();
	}

	protected java.lang.String connectionGetCatalog(InnerConnection connection) throws SQLException {
		return connection.getDelegate().getCatalog();
	}

	protected int connectionGetHoldability(InnerConnection connection) throws SQLException {
		return connection.getDelegate().getHoldability();
	}

	protected java.sql.DatabaseMetaData connectionGetMetaData(InnerConnection connection)
			throws SQLException {
		return connection.getDelegate().getMetaData();
	}

	protected int connectionGetTransactionIsolation(InnerConnection connection) throws SQLException {
		return connection.getDelegate().getTransactionIsolation();
	}

	protected java.util.Map<String, Class<?>> connectionGetTypeMap(InnerConnection connection) throws SQLException {
		return connection.getDelegate().getTypeMap();
	}

	protected java.sql.SQLWarning connectionGetWarnings(InnerConnection connection)
			throws SQLException {
		return connection.getDelegate().getWarnings();
	}

	protected boolean connectionIsClosed(InnerConnection connection) throws SQLException {
		return connection.getDelegate().isClosed();
	}

	protected boolean connectionIsReadOnly(InnerConnection connection) throws SQLException {
		return connection.getDelegate().isReadOnly();
	}

	protected java.lang.String connectionNativeSQL(InnerConnection connection,
			java.lang.String stringVar0) throws SQLException {
		return connection.getDelegate().nativeSQL(stringVar0);
	}

	protected CallableStatement connectionPrepareCall(InnerConnection connection,
			java.lang.String stringVar0) throws SQLException {
		return connection.new InnerCallableStatement(connection.getDelegate().prepareCall(
				stringVar0));
	}

	protected CallableStatement connectionPrepareCall(InnerConnection connection,
			java.lang.String stringVar0, int intVar1, int intVar2) throws SQLException {
		return connection.new InnerCallableStatement(connection.getDelegate().prepareCall(
				stringVar0, intVar1, intVar2));
	}

	protected CallableStatement connectionPrepareCall(InnerConnection connection,
			java.lang.String stringVar0, int intVar1, int intVar2, int intVar3) throws SQLException {
		return connection.new InnerCallableStatement(connection.getDelegate().prepareCall(
				stringVar0, intVar1, intVar2, intVar3));
	}

	protected PreparedStatement connectionPrepareStatement(InnerConnection connection,
			java.lang.String stringVar0) throws SQLException {
		return connection.new InnerPreparedStatement(connection.getDelegate().prepareStatement(
				stringVar0));
	}

	protected PreparedStatement connectionPrepareStatement(InnerConnection connection,
			java.lang.String stringVar0, int[] intVar1) throws SQLException {
		return connection.new InnerPreparedStatement(connection.getDelegate().prepareStatement(
				stringVar0, intVar1));
	}

	protected PreparedStatement connectionPrepareStatement(InnerConnection connection,
			java.lang.String stringVar0, java.lang.String[] stringVar1) throws SQLException {
		return connection.new InnerPreparedStatement(connection.getDelegate().prepareStatement(
				stringVar0, stringVar1));
	}

	protected PreparedStatement connectionPrepareStatement(InnerConnection connection,
			java.lang.String stringVar0, int intVar1) throws SQLException {
		return connection.new InnerPreparedStatement(connection.getDelegate().prepareStatement(
				stringVar0, intVar1));
	}

	protected PreparedStatement connectionPrepareStatement(InnerConnection connection,
			java.lang.String stringVar0, int intVar1, int intVar2) throws SQLException {
		return connection.new InnerPreparedStatement(connection.getDelegate().prepareStatement(
				stringVar0, intVar1, intVar2));
	}

	protected PreparedStatement connectionPrepareStatement(InnerConnection connection,
			java.lang.String stringVar0, int intVar1, int intVar2, int intVar3) throws SQLException {
		return connection.new InnerPreparedStatement(connection.getDelegate().prepareStatement(
				stringVar0, intVar1, intVar2, intVar3));
	}

	protected void connectionReleaseSavepoint(InnerConnection connection,
			java.sql.Savepoint savepointVar0) throws SQLException {
		connection.getDelegate().releaseSavepoint(savepointVar0);
	}

	protected void connectionRollback(InnerConnection connection) throws SQLException {
		connection.getDelegate().rollback();
	}

	protected void connectionRollback(InnerConnection connection, java.sql.Savepoint savepointVar0)
			throws SQLException {
		connection.getDelegate().rollback(savepointVar0);
	}

	protected void connectionSetAutoCommit(InnerConnection connection, boolean booleanVar0)
			throws SQLException {
		connection.getDelegate().setAutoCommit(booleanVar0);
	}

	protected void connectionSetCatalog(InnerConnection connection, java.lang.String stringVar0)
			throws SQLException {
		connection.getDelegate().setCatalog(stringVar0);
	}

	protected void connectionSetHoldability(InnerConnection connection, int intVar0)
			throws SQLException {
		connection.getDelegate().setHoldability(intVar0);
	}

	protected void connectionSetReadOnly(InnerConnection connection, boolean booleanVar0)
			throws SQLException {
		connection.getDelegate().setReadOnly(booleanVar0);
	}

	protected java.sql.Savepoint connectionSetSavepoint(InnerConnection connection)
			throws SQLException {
		return connection.getDelegate().setSavepoint();
	}

	protected java.sql.Savepoint connectionSetSavepoint(InnerConnection connection,
			java.lang.String stringVar0) throws SQLException {
		return connection.getDelegate().setSavepoint(stringVar0);
	}

	protected void connectionSetTransactionIsolation(InnerConnection connection, int intVar0)
			throws SQLException {
		connection.getDelegate().setTransactionIsolation(intVar0);
	}

	protected void connectionSetTypeMap(InnerConnection connection, java.util.Map<String, Class<?>> mapVar0)
			throws SQLException {
		connection.getDelegate().setTypeMap(mapVar0);
	}


	protected Array connectionCreateArrayOf(InnerConnection connection, String typeName, Object[] elements) throws SQLException {
		return connection.getDelegate().createArrayOf(typeName, elements);
	}

	protected Blob connectionCreateBlob(InnerConnection connection) throws SQLException {
		return connection.getDelegate().createBlob();
	}

	protected Clob connectionCreateClob(InnerConnection connection) throws SQLException {
		return connection.getDelegate().createClob();
	}

	protected NClob connectionCreateNClob(InnerConnection connection) throws SQLException {
		return connection.getDelegate().createNClob();
	}

	protected SQLXML connectionCreateSQLXML(InnerConnection connection) throws SQLException {
		return connection.getDelegate().createSQLXML();
	}

	protected Struct connectionCreateStruct(InnerConnection connection, String typeName, Object[] attributes) throws SQLException {
		return connection.getDelegate().createStruct(typeName, attributes);
	}

	protected Properties connectionGetClientInfo(InnerConnection connection) throws SQLException {
		return connection.getDelegate().getClientInfo();
	}

	protected String connectionGetClientInfo(InnerConnection connection, String name) throws SQLException  {
		return connection.getDelegate().getClientInfo(name);
	}

	protected boolean connectionIsValid(InnerConnection connection, int timeout) throws SQLException {
		return connection.getDelegate().isValid(timeout);
	}

	protected void connectionSetClientInfo(InnerConnection connection, Properties properties) throws SQLClientInfoException {
		connection.getDelegate().setClientInfo(properties);

	}

	protected void connectionSetClientInfo(InnerConnection connection, String name, String value) throws SQLClientInfoException {
		connection.getDelegate().setClientInfo(name, value);

	}

	protected boolean connectionIsWrapperFor(InnerConnection connection, Class<?> iface) throws SQLException {
		return connection.getDelegate().isWrapperFor(iface);
	}

	protected <T> T connectionUnwrap(InnerConnection connection, Class<T> iface) throws SQLException {
		return connection.getDelegate().unwrap(iface);
	}

	protected void connectionSetSchema(InnerConnection connection, String schema) throws SQLException {
		connection.getDelegate().setSchema(schema);
	}

	protected String connectionGetSchema(InnerConnection connection) throws SQLException {
		return connection.getDelegate().getSchema();
	}

	protected void connectionAbort(InnerConnection connection, Executor executor) throws SQLException {
		connection.getDelegate().abort(executor);
	}

	protected void connectionSetNetworkTimeout(InnerConnection connection, Executor executor, int milliseconds) throws SQLException {
		connection.getDelegate().setNetworkTimeout(executor, milliseconds);
	}

	protected int connectionGetNetworkTimeout(InnerConnection connection) throws SQLException {
		return connection.getDelegate().getNetworkTimeout();
	}

	protected void statementInit(InnerConnection.InnerStatement statement) {
	}

	protected void statementAddBatch(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0) throws SQLException {
		statement.getDelegate().addBatch(stringVar0);
	}

	protected void statementCancel(InnerConnection.InnerStatement statement) throws SQLException {
		statement.getDelegate().cancel();
	}

	protected void statementClearBatch(InnerConnection.InnerStatement statement)
			throws SQLException {
		statement.getDelegate().clearBatch();
	}

	protected void statementClearWarnings(InnerConnection.InnerStatement statement)
			throws SQLException {
		statement.getDelegate().clearWarnings();
	}

	protected void statementClose(InnerConnection.InnerStatement statement) throws SQLException {
		statement.getDelegate().close();
	}

	protected boolean statementExecute(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0) throws SQLException {
		return statement.getDelegate().execute(stringVar0);
	}

	protected boolean statementExecute(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0, int[] intVar1) throws SQLException {
		return statement.getDelegate().execute(stringVar0, intVar1);
	}

	protected boolean statementExecute(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0, java.lang.String[] stringVar1) throws SQLException {
		return statement.getDelegate().execute(stringVar0, stringVar1);
	}

	protected boolean statementExecute(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0, int intVar1) throws SQLException {
		return statement.getDelegate().execute(stringVar0, intVar1);
	}

	protected int[] statementExecuteBatch(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().executeBatch();
	}

	protected ResultSet statementExecuteQuery(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0) throws SQLException {
		return statement.new InnerResultSet(statement.getDelegate().executeQuery(stringVar0));
	}

	protected int statementExecuteUpdate(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0) throws SQLException {
		return statement.getDelegate().executeUpdate(stringVar0);
	}

	protected int statementExecuteUpdate(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0, int[] intVar1) throws SQLException {
		return statement.getDelegate().executeUpdate(stringVar0, intVar1);
	}

	protected int statementExecuteUpdate(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0, java.lang.String[] stringVar1) throws SQLException {
		return statement.getDelegate().executeUpdate(stringVar0, stringVar1);
	}

	protected int statementExecuteUpdate(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0, int intVar1) throws SQLException {
		return statement.getDelegate().executeUpdate(stringVar0, intVar1);
	}

	protected int statementGetFetchDirection(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getFetchDirection();
	}

	protected int statementGetFetchSize(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getFetchSize();
	}

	protected ResultSet statementGetGeneratedKeys(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.new InnerResultSet(statement.getDelegate().getGeneratedKeys());
	}

	protected int statementGetMaxFieldSize(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getMaxFieldSize();
	}

	protected int statementGetMaxRows(InnerConnection.InnerStatement statement) throws SQLException {
		return statement.getDelegate().getMaxRows();
	}

	protected boolean statementGetMoreResults(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getMoreResults();
	}

	protected boolean statementGetMoreResults(InnerConnection.InnerStatement statement, int intVar0)
			throws SQLException {
		return statement.getDelegate().getMoreResults(intVar0);
	}

	protected int statementGetQueryTimeout(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getQueryTimeout();
	}

	protected ResultSet statementGetResultSet(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.new InnerResultSet(statement.getDelegate().getResultSet());
	}

	protected int statementGetResultSetConcurrency(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getResultSetConcurrency();
	}

	protected int statementGetResultSetHoldability(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getResultSetHoldability();
	}

	protected int statementGetResultSetType(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getResultSetType();
	}

	protected int statementGetUpdateCount(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getUpdateCount();
	}

	protected java.sql.SQLWarning statementGetWarnings(InnerConnection.InnerStatement statement)
			throws SQLException {
		return statement.getDelegate().getWarnings();
	}

	protected void statementSetCursorName(InnerConnection.InnerStatement statement,
			java.lang.String stringVar0) throws SQLException {
		statement.getDelegate().setCursorName(stringVar0);
	}

	protected void statementSetEscapeProcessing(InnerConnection.InnerStatement statement,
			boolean booleanVar0) throws SQLException {
		statement.getDelegate().setEscapeProcessing(booleanVar0);
	}

	protected void statementSetFetchDirection(InnerConnection.InnerStatement statement, int intVar0)
			throws SQLException {
		statement.getDelegate().setFetchDirection(intVar0);
	}

	protected void statementSetFetchSize(InnerConnection.InnerStatement statement, int intVar0)
			throws SQLException {
		statement.getDelegate().setFetchSize(intVar0);
	}

	protected void statementSetMaxFieldSize(InnerConnection.InnerStatement statement, int intVar0)
			throws SQLException {
		statement.getDelegate().setMaxFieldSize(intVar0);
	}

	protected void statementSetMaxRows(InnerConnection.InnerStatement statement, int intVar0)
			throws SQLException {
		statement.getDelegate().setMaxRows(intVar0);
	}

	protected void statementSetQueryTimeout(InnerConnection.InnerStatement statement, int intVar0)
			throws SQLException {
		statement.getDelegate().setQueryTimeout(intVar0);
	}

	protected boolean statementIsClosed(InnerStatement statement) throws SQLException {
		return statement.getDelegate().isClosed();
	}

	protected boolean statementIsPoolable(InnerStatement statement) throws SQLException {
		return statement.getDelegate().isPoolable();
	}

	protected void statementSetPoolable(InnerStatement statement, boolean poolable) throws SQLException {
		statement.getDelegate().setPoolable(poolable);

	}

	protected boolean statementIsWrapperFor(InnerStatement statement, Class<?> iface) throws SQLException {
		return statement.getDelegate().isWrapperFor(iface);
	}

	protected <T> T statementUnwrap(InnerStatement statement, Class<T> iface) throws SQLException {
		return statement.getDelegate().unwrap(iface);
	}

	protected void statementCloseOnCompletion(InnerStatement statement) throws SQLException {
		statement.getDelegate().closeOnCompletion();
	}

	protected boolean statementIsCloseOnCompletion(InnerStatement statement) throws SQLException {
		return statement.getDelegate().isCloseOnCompletion();
	}

	protected void preparedStatementInit(InnerPreparedStatement preparedStatement) {
	}

	protected void preparedStatementAddBatch(
			InnerPreparedStatement preparedStatement) throws SQLException {
		preparedStatement.getDelegate().addBatch();
	}

	protected void preparedStatementClearParameters(
			InnerPreparedStatement preparedStatement) throws SQLException {
		preparedStatement.getDelegate().clearParameters();
	}

	protected boolean preparedStatementExecute(
			InnerPreparedStatement preparedStatement) throws SQLException {
		return preparedStatement.getDelegate().execute();
	}

	protected ResultSet preparedStatementExecuteQuery(
			InnerPreparedStatement preparedStatement) throws SQLException {
		return preparedStatement.new InnerResultSet(preparedStatement.getDelegate().executeQuery());
	}

	protected int preparedStatementExecuteUpdate(
			InnerPreparedStatement preparedStatement) throws SQLException {
		return preparedStatement.getDelegate().executeUpdate();
	}

	protected java.sql.ResultSetMetaData preparedStatementGetMetaData(
			InnerPreparedStatement preparedStatement) throws SQLException {
		return preparedStatement.getDelegate().getMetaData();
	}

	protected java.sql.ParameterMetaData preparedStatementGetParameterMetaData(
			InnerPreparedStatement preparedStatement) throws SQLException {
		return preparedStatement.getDelegate().getParameterMetaData();
	}

	protected void preparedStatementSetArray(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Array arrayVar1) throws SQLException {
		preparedStatement.getDelegate().setArray(intVar0, arrayVar1);
	}

	protected void preparedStatementSetAsciiStream(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
		preparedStatement.getDelegate().setAsciiStream(intVar0, inputStreamVar1, intVar2);
	}

	protected void preparedStatementSetBigDecimal(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.math.BigDecimal bigDecimalVar1) throws SQLException {
		preparedStatement.getDelegate().setBigDecimal(intVar0, bigDecimalVar1);
	}

	protected void preparedStatementSetBinaryStream(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
		preparedStatement.getDelegate().setBinaryStream(intVar0, inputStreamVar1, intVar2);
	}

	protected void preparedStatementSetBlob(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Blob blobVar1) throws SQLException {
		preparedStatement.getDelegate().setBlob(intVar0, blobVar1);
	}

	protected void preparedStatementSetBoolean(
			InnerPreparedStatement preparedStatement, int intVar0,
			boolean booleanVar1) throws SQLException {
		preparedStatement.getDelegate().setBoolean(intVar0, booleanVar1);
	}

	protected void preparedStatementSetByte(
			InnerPreparedStatement preparedStatement, int intVar0, byte byteVar1)
			throws SQLException {
		preparedStatement.getDelegate().setByte(intVar0, byteVar1);
	}

	protected void preparedStatementSetBytes(
			InnerPreparedStatement preparedStatement, int intVar0, byte[] byteVar1)
			throws SQLException {
		preparedStatement.getDelegate().setBytes(intVar0, byteVar1);
	}

	protected void preparedStatementSetCharacterStream(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.io.Reader readerVar1, int intVar2) throws SQLException {
		preparedStatement.getDelegate().setCharacterStream(intVar0, readerVar1, intVar2);
	}

	protected void preparedStatementSetClob(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Clob clobVar1) throws SQLException {
		preparedStatement.getDelegate().setClob(intVar0, clobVar1);
	}

	protected void preparedStatementSetDate(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Date dateVar1) throws SQLException {
		preparedStatement.getDelegate().setDate(intVar0, dateVar1);
	}

	protected void preparedStatementSetDate(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Date dateVar1, java.util.Calendar calendarVar2) throws SQLException {
		preparedStatement.getDelegate().setDate(intVar0, dateVar1, calendarVar2);
	}

	protected void preparedStatementSetDouble(
			InnerPreparedStatement preparedStatement, int intVar0, double doubleVar1)
			throws SQLException {
		preparedStatement.getDelegate().setDouble(intVar0, doubleVar1);
	}

	protected void preparedStatementSetFloat(
			InnerPreparedStatement preparedStatement, int intVar0, float floatVar1)
			throws SQLException {
		preparedStatement.getDelegate().setFloat(intVar0, floatVar1);
	}

	protected void preparedStatementSetInt(
			InnerPreparedStatement preparedStatement, int intVar0, int intVar1)
			throws SQLException {
		preparedStatement.getDelegate().setInt(intVar0, intVar1);
	}

	protected void preparedStatementSetLong(
			InnerPreparedStatement preparedStatement, int intVar0, long longVar1)
			throws SQLException {
		preparedStatement.getDelegate().setLong(intVar0, longVar1);
	}

	protected void preparedStatementSetNull(
			InnerPreparedStatement preparedStatement, int intVar0, int intVar1)
			throws SQLException {
		preparedStatement.getDelegate().setNull(intVar0, intVar1);
	}

	protected void preparedStatementSetNull(
			InnerPreparedStatement preparedStatement, int intVar0, int intVar1,
			java.lang.String stringVar2) throws SQLException {
		preparedStatement.getDelegate().setNull(intVar0, intVar1, stringVar2);
	}

	protected void preparedStatementSetObject(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.lang.Object objectVar1) throws SQLException {
		preparedStatement.getDelegate().setObject(intVar0, objectVar1);
	}

	protected void preparedStatementSetObject(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.lang.Object objectVar1, int intVar2) throws SQLException {
		preparedStatement.getDelegate().setObject(intVar0, objectVar1, intVar2);
	}

	protected void preparedStatementSetObject(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.lang.Object objectVar1, int intVar2, int intVar3) throws SQLException {
		preparedStatement.getDelegate().setObject(intVar0, objectVar1, intVar2, intVar3);
	}

	protected void preparedStatementSetRef(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Ref refVar1) throws SQLException {
		preparedStatement.getDelegate().setRef(intVar0, refVar1);
	}

	protected void preparedStatementSetShort(
			InnerPreparedStatement preparedStatement, int intVar0, short shortVar1)
			throws SQLException {
		preparedStatement.getDelegate().setShort(intVar0, shortVar1);
	}

	protected void preparedStatementSetString(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.lang.String stringVar1) throws SQLException {
		preparedStatement.getDelegate().setString(intVar0, stringVar1);
	}

	protected void preparedStatementSetTime(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Time timeVar1) throws SQLException {
		preparedStatement.getDelegate().setTime(intVar0, timeVar1);
	}

	protected void preparedStatementSetTime(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Time timeVar1, java.util.Calendar calendarVar2) throws SQLException {
		preparedStatement.getDelegate().setTime(intVar0, timeVar1, calendarVar2);
	}

	protected void preparedStatementSetTimestamp(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Timestamp timestampVar1) throws SQLException {
		preparedStatement.getDelegate().setTimestamp(intVar0, timestampVar1);
	}

	protected void preparedStatementSetTimestamp(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.sql.Timestamp timestampVar1, java.util.Calendar calendarVar2) throws SQLException {
		preparedStatement.getDelegate().setTimestamp(intVar0, timestampVar1, calendarVar2);
	}

	protected void preparedStatementSetURL(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.net.URL urlvar1) throws SQLException {
		preparedStatement.getDelegate().setURL(intVar0, urlvar1);
	}

	@Deprecated
	protected void preparedStatementSetUnicodeStream(
			InnerPreparedStatement preparedStatement, int intVar0,
			java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
		preparedStatement.getDelegate().setUnicodeStream(intVar0, inputStreamVar1, intVar2);
	}

	protected void preparedStatementSetAsciiStream(InnerPreparedStatement preparedStatement, int parameterIndex, InputStream x) throws SQLException {
		preparedStatement.getDelegate().setAsciiStream(parameterIndex, x);

	}

	protected void preparedStatementSetAsciiStream(InnerPreparedStatement preparedStatement, int parameterIndex, InputStream x, long length)
			throws SQLException {
		preparedStatement.getDelegate().setAsciiStream(parameterIndex, x, length);

	}

	protected void preparedStatementSetBinaryStream(InnerPreparedStatement preparedStatement, int parameterIndex, InputStream x) throws SQLException {
		preparedStatement.getDelegate().setBinaryStream(parameterIndex, x);

	}

	protected void preparedStatementSetBinaryStream(InnerPreparedStatement preparedStatement, int parameterIndex, InputStream x, long length)
			throws SQLException {
		preparedStatement.getDelegate().setBinaryStream(parameterIndex, x, length);

	}

	protected void preparedStatementSetBlob(InnerPreparedStatement preparedStatement, int parameterIndex, InputStream inputStream) throws SQLException {
		preparedStatement.getDelegate().setBlob(parameterIndex, inputStream);

	}

	protected void preparedStatementSetBlob(InnerPreparedStatement preparedStatement, int parameterIndex, InputStream inputStream, long length)
			throws SQLException {
		preparedStatement.getDelegate().setBlob(parameterIndex, inputStream, length);

	}

	protected void preparedStatementSetCharacterStream(InnerPreparedStatement preparedStatement, int parameterIndex, Reader reader) throws SQLException {
		preparedStatement.getDelegate().setCharacterStream(parameterIndex, reader);

	}

	protected void preparedStatementSetCharacterStream(InnerPreparedStatement preparedStatement, int parameterIndex, Reader reader, long length)
			throws SQLException {
		preparedStatement.getDelegate().setCharacterStream(parameterIndex, reader, length);

	}

	protected void preparedStatementSetClob(InnerPreparedStatement preparedStatement, int parameterIndex, Reader reader) throws SQLException {
		preparedStatement.getDelegate().setClob(parameterIndex, reader);

	}

	protected void preparedStatementSetClob(InnerPreparedStatement preparedStatement, int parameterIndex, Reader reader, long length) throws SQLException {
		preparedStatement.getDelegate().setClob(parameterIndex, reader, length);

	}

	protected void preparedStatementSetNCharacterStream(InnerPreparedStatement preparedStatement, int parameterIndex, Reader value) throws SQLException {
		preparedStatement.getDelegate().setNCharacterStream(parameterIndex, value);

	}

	protected void preparedStatementSetNCharacterStream(InnerPreparedStatement preparedStatement, int parameterIndex, Reader value, long length)
			throws SQLException {
		preparedStatement.getDelegate().setNCharacterStream(parameterIndex, value, length);

	}

	protected void preparedStatementSetNClob(InnerPreparedStatement preparedStatement, int parameterIndex, NClob value) throws SQLException {
		preparedStatement.getDelegate().setNClob(parameterIndex, value);

	}

	protected void preparedStatementSetNClob(InnerPreparedStatement preparedStatement, int parameterIndex, Reader reader) throws SQLException {
		preparedStatement.getDelegate().setNClob(parameterIndex, reader);

	}

	protected void preparedStatementSetNClob(InnerPreparedStatement preparedStatement, int parameterIndex, Reader reader, long length)
			throws SQLException {
		preparedStatement.getDelegate().setNClob(parameterIndex, reader, length);

	}

	protected void preparedStatementSetNString(InnerPreparedStatement preparedStatement, int parameterIndex, String value) throws SQLException {
		preparedStatement.getDelegate().setNString(parameterIndex, value);

	}

	protected void preparedStatementSetRowId(InnerPreparedStatement preparedStatement, int parameterIndex, RowId x) throws SQLException {
		preparedStatement.getDelegate().setRowId(parameterIndex, x);

	}

	protected void preparedStatementSetSQLXML(InnerPreparedStatement preparedStatement, int parameterIndex, SQLXML xmlObject) throws SQLException {
		preparedStatement.getDelegate().setSQLXML(parameterIndex, xmlObject);

	}

	protected void callableStatementInit(InnerConnection.InnerCallableStatement callableStatement) {
	}

	protected java.sql.Array callableStatementGetArray(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getArray(intVar0);
	}

	protected java.sql.Array callableStatementGetArray(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getArray(stringVar0);
	}

	protected java.math.BigDecimal callableStatementGetBigDecimal(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getBigDecimal(intVar0);
	}

	protected java.math.BigDecimal callableStatementGetBigDecimal(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getBigDecimal(stringVar0);
	}

	@Deprecated
	protected java.math.BigDecimal callableStatementGetBigDecimal(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0, int intVar1)
			throws SQLException {
		return callableStatement.getDelegate().getBigDecimal(intVar0, intVar1);
	}

	protected java.sql.Blob callableStatementGetBlob(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getBlob(intVar0);
	}

	protected java.sql.Blob callableStatementGetBlob(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getBlob(stringVar0);
	}

	protected boolean callableStatementGetBoolean(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getBoolean(intVar0);
	}

	protected boolean callableStatementGetBoolean(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getBoolean(stringVar0);
	}

	protected byte callableStatementGetByte(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getByte(intVar0);
	}

	protected byte callableStatementGetByte(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getByte(stringVar0);
	}

	protected byte[] callableStatementGetBytes(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getBytes(intVar0);
	}

	protected byte[] callableStatementGetBytes(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getBytes(stringVar0);
	}

	protected java.sql.Clob callableStatementGetClob(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getClob(intVar0);
	}

	protected java.sql.Clob callableStatementGetClob(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getClob(stringVar0);
	}

	protected java.sql.Date callableStatementGetDate(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getDate(intVar0);
	}

	protected java.sql.Date callableStatementGetDate(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getDate(stringVar0);
	}

	protected java.sql.Date callableStatementGetDate(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return callableStatement.getDelegate().getDate(intVar0, calendarVar1);
	}

	protected java.sql.Date callableStatementGetDate(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return callableStatement.getDelegate().getDate(stringVar0, calendarVar1);
	}

	protected double callableStatementGetDouble(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getDouble(intVar0);
	}

	protected double callableStatementGetDouble(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getDouble(stringVar0);
	}

	protected float callableStatementGetFloat(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getFloat(intVar0);
	}

	protected float callableStatementGetFloat(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getFloat(stringVar0);
	}

	protected int callableStatementGetInt(InnerConnection.InnerCallableStatement callableStatement,
			int intVar0) throws SQLException {
		return callableStatement.getDelegate().getInt(intVar0);
	}

	protected int callableStatementGetInt(InnerConnection.InnerCallableStatement callableStatement,
			java.lang.String stringVar0) throws SQLException {
		return callableStatement.getDelegate().getInt(stringVar0);
	}

	protected long callableStatementGetLong(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getLong(intVar0);
	}

	protected long callableStatementGetLong(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getLong(stringVar0);
	}

	protected java.lang.Object callableStatementGetObject(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getObject(intVar0);
	}

	protected java.lang.Object callableStatementGetObject(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getObject(stringVar0);
	}

	protected java.lang.Object callableStatementGetObject(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0,
			java.util.Map<String, Class<?>> mapVar1) throws SQLException {
		return callableStatement.getDelegate().getObject(intVar0, mapVar1);
	}

	protected java.lang.Object callableStatementGetObject(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.util.Map<String, Class<?>> mapVar1) throws SQLException {
		return callableStatement.getDelegate().getObject(stringVar0, mapVar1);
	}

	protected java.sql.Ref callableStatementGetRef(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getRef(intVar0);
	}

	protected java.sql.Ref callableStatementGetRef(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getRef(stringVar0);
	}

	protected short callableStatementGetShort(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getShort(intVar0);
	}

	protected short callableStatementGetShort(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getShort(stringVar0);
	}

	protected java.lang.String callableStatementGetString(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getString(intVar0);
	}

	protected java.lang.String callableStatementGetString(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getString(stringVar0);
	}

	protected java.sql.Time callableStatementGetTime(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getTime(intVar0);
	}

	protected java.sql.Time callableStatementGetTime(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getTime(stringVar0);
	}

	protected java.sql.Time callableStatementGetTime(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return callableStatement.getDelegate().getTime(intVar0, calendarVar1);
	}

	protected java.sql.Time callableStatementGetTime(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return callableStatement.getDelegate().getTime(stringVar0, calendarVar1);
	}

	protected java.sql.Timestamp callableStatementGetTimestamp(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getTimestamp(intVar0);
	}

	protected java.sql.Timestamp callableStatementGetTimestamp(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getTimestamp(stringVar0);
	}

	protected java.sql.Timestamp callableStatementGetTimestamp(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return callableStatement.getDelegate().getTimestamp(intVar0, calendarVar1);
	}

	protected java.sql.Timestamp callableStatementGetTimestamp(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return callableStatement.getDelegate().getTimestamp(stringVar0, calendarVar1);
	}

	protected java.net.URL callableStatementGetURL(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0)
			throws SQLException {
		return callableStatement.getDelegate().getURL(intVar0);
	}

	protected java.net.URL callableStatementGetURL(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0)
			throws SQLException {
		return callableStatement.getDelegate().getURL(stringVar0);
	}

	protected void callableStatementRegisterOutParameter(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0, int intVar1)
			throws SQLException {
		callableStatement.getDelegate().registerOutParameter(intVar0, intVar1);
	}

	protected void callableStatementRegisterOutParameter(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			int intVar1) throws SQLException {
		callableStatement.getDelegate().registerOutParameter(stringVar0, intVar1);
	}

	protected void callableStatementRegisterOutParameter(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0, int intVar1,
			int intVar2) throws SQLException {
		callableStatement.getDelegate().registerOutParameter(intVar0, intVar1, intVar2);
	}

	protected void callableStatementRegisterOutParameter(
			InnerConnection.InnerCallableStatement callableStatement, int intVar0, int intVar1,
			java.lang.String stringVar2) throws SQLException {
		callableStatement.getDelegate().registerOutParameter(intVar0, intVar1, stringVar2);
	}

	protected void callableStatementRegisterOutParameter(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			int intVar1, int intVar2) throws SQLException {
		callableStatement.getDelegate().registerOutParameter(stringVar0, intVar1, intVar2);
	}

	protected void callableStatementRegisterOutParameter(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			int intVar1, java.lang.String stringVar2) throws SQLException {
		callableStatement.getDelegate().registerOutParameter(stringVar0, intVar1, stringVar2);
	}

	protected void callableStatementSetAsciiStream(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
		callableStatement.getDelegate().setAsciiStream(stringVar0, inputStreamVar1, intVar2);
	}

	protected void callableStatementSetBigDecimal(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.math.BigDecimal bigDecimalVar1) throws SQLException {
		callableStatement.getDelegate().setBigDecimal(stringVar0, bigDecimalVar1);
	}

	protected void callableStatementSetBinaryStream(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
		callableStatement.getDelegate().setBinaryStream(stringVar0, inputStreamVar1, intVar2);
	}

	protected void callableStatementSetBoolean(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			boolean booleanVar1) throws SQLException {
		callableStatement.getDelegate().setBoolean(stringVar0, booleanVar1);
	}

	protected void callableStatementSetByte(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			byte byteVar1) throws SQLException {
		callableStatement.getDelegate().setByte(stringVar0, byteVar1);
	}

	protected void callableStatementSetBytes(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			byte[] byteVar1) throws SQLException {
		callableStatement.getDelegate().setBytes(stringVar0, byteVar1);
	}

	protected void callableStatementSetCharacterStream(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.io.Reader readerVar1, int intVar2) throws SQLException {
		callableStatement.getDelegate().setCharacterStream(stringVar0, readerVar1, intVar2);
	}

	protected void callableStatementSetDate(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.sql.Date dateVar1) throws SQLException {
		callableStatement.getDelegate().setDate(stringVar0, dateVar1);
	}

	protected void callableStatementSetDate(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.sql.Date dateVar1, java.util.Calendar calendarVar2) throws SQLException {
		callableStatement.getDelegate().setDate(stringVar0, dateVar1, calendarVar2);
	}

	protected void callableStatementSetDouble(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			double doubleVar1) throws SQLException {
		callableStatement.getDelegate().setDouble(stringVar0, doubleVar1);
	}

	protected void callableStatementSetFloat(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			float floatVar1) throws SQLException {
		callableStatement.getDelegate().setFloat(stringVar0, floatVar1);
	}

	protected void callableStatementSetInt(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			int intVar1) throws SQLException {
		callableStatement.getDelegate().setInt(stringVar0, intVar1);
	}

	protected void callableStatementSetLong(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			long longVar1) throws SQLException {
		callableStatement.getDelegate().setLong(stringVar0, longVar1);
	}

	protected void callableStatementSetNull(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			int intVar1) throws SQLException {
		callableStatement.getDelegate().setNull(stringVar0, intVar1);
	}

	protected void callableStatementSetNull(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			int intVar1, java.lang.String stringVar2) throws SQLException {
		callableStatement.getDelegate().setNull(stringVar0, intVar1, stringVar2);
	}

	protected void callableStatementSetObject(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.lang.Object objectVar1) throws SQLException {
		callableStatement.getDelegate().setObject(stringVar0, objectVar1);
	}

	protected void callableStatementSetObject(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.lang.Object objectVar1, int intVar2) throws SQLException {
		callableStatement.getDelegate().setObject(stringVar0, objectVar1, intVar2);
	}

	protected void callableStatementSetObject(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.lang.Object objectVar1, int intVar2, int intVar3) throws SQLException {
		callableStatement.getDelegate().setObject(stringVar0, objectVar1, intVar2, intVar3);
	}

	protected void callableStatementSetShort(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			short shortVar1) throws SQLException {
		callableStatement.getDelegate().setShort(stringVar0, shortVar1);
	}

	protected void callableStatementSetString(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.lang.String stringVar1) throws SQLException {
		callableStatement.getDelegate().setString(stringVar0, stringVar1);
	}

	protected void callableStatementSetTime(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.sql.Time timeVar1) throws SQLException {
		callableStatement.getDelegate().setTime(stringVar0, timeVar1);
	}

	protected void callableStatementSetTime(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.sql.Time timeVar1, java.util.Calendar calendarVar2) throws SQLException {
		callableStatement.getDelegate().setTime(stringVar0, timeVar1, calendarVar2);
	}

	protected void callableStatementSetTimestamp(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.sql.Timestamp timestampVar1) throws SQLException {
		callableStatement.getDelegate().setTimestamp(stringVar0, timestampVar1);
	}

	protected void callableStatementSetTimestamp(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.sql.Timestamp timestampVar1, java.util.Calendar calendarVar2) throws SQLException {
		callableStatement.getDelegate().setTimestamp(stringVar0, timestampVar1, calendarVar2);
	}

	protected void callableStatementSetURL(
			InnerConnection.InnerCallableStatement callableStatement, java.lang.String stringVar0,
			java.net.URL urlvar1) throws SQLException {
		callableStatement.getDelegate().setURL(stringVar0, urlvar1);
	}

	protected boolean callableStatementWasNull(
			InnerConnection.InnerCallableStatement callableStatement) throws SQLException {
		return callableStatement.getDelegate().wasNull();
	}

	protected Reader callableStatementGetCharacterStream(InnerCallableStatement callableStatement, int parameterIndex) throws SQLException {
		return callableStatement.getDelegate().getCharacterStream(parameterIndex);
	}

	protected Reader callableStatementGetCharacterStream(InnerCallableStatement callableStatement, String parameterName) throws SQLException {
		return callableStatement.getDelegate().getCharacterStream(parameterName);
	}

	protected Reader callableStatementGetNCharacterStream(InnerCallableStatement callableStatement, int parameterIndex) throws SQLException {
		return callableStatement.getDelegate().getNCharacterStream(parameterIndex);
	}

	protected Reader callableStatementGetNCharacterStream(InnerCallableStatement callableStatement, String parameterName) throws SQLException {
		return callableStatement.getDelegate().getNCharacterStream(parameterName);
	}

	protected NClob callableStatementGetNClob(InnerCallableStatement callableStatement, int parameterIndex) throws SQLException {
		return callableStatement.getDelegate().getNClob(parameterIndex);
	}

	protected NClob callableStatementGetNClob(InnerCallableStatement callableStatement, String parameterName) throws SQLException {
		return callableStatement.getDelegate().getNClob(parameterName);
	}

	protected String callableStatementGetNString(InnerCallableStatement callableStatement, int parameterIndex) throws SQLException {
		return callableStatement.getDelegate().getNString(parameterIndex);
	}

	protected String callableStatementGetNString(InnerCallableStatement callableStatement, String parameterName) throws SQLException {
		return callableStatement.getDelegate().getNString(parameterName);
	}

	protected RowId callableStatementGetRowId(InnerCallableStatement callableStatement, int parameterIndex) throws SQLException {
		return callableStatement.getDelegate().getRowId(parameterIndex);
	}

	protected RowId callableStatementGetRowId(InnerCallableStatement callableStatement, String parameterName) throws SQLException {
		return callableStatement.getDelegate().getRowId(parameterName);
	}

	protected SQLXML callableStatementGetSQLXML(InnerCallableStatement callableStatement, int parameterIndex) throws SQLException {
		return callableStatement.getDelegate().getSQLXML(parameterIndex);
	}

	protected SQLXML callableStatementGetSQLXML(InnerCallableStatement callableStatement, String parameterName) throws SQLException {
		return callableStatement.getDelegate().getSQLXML(parameterName);
	}

	protected void callableStatementSetAsciiStream(InnerCallableStatement callableStatement, String parameterName, InputStream x) throws SQLException {
		callableStatement.getDelegate().setAsciiStream(parameterName, x);

	}

	protected void callableStatementSetAsciiStream(InnerCallableStatement callableStatement, String parameterName, InputStream x, long length)
			throws SQLException {
		callableStatement.getDelegate().setAsciiStream(parameterName, x, length);

	}

	protected void callableStatementSetBinaryStream(InnerCallableStatement callableStatement, String parameterName, InputStream x) throws SQLException {
		callableStatement.getDelegate().setBinaryStream(parameterName, x);

	}

	protected void callableStatementSetBinaryStream(InnerCallableStatement callableStatement, String parameterName, InputStream x, long length)
			throws SQLException {
		callableStatement.getDelegate().setBinaryStream(parameterName, x, length);

	}

	protected void callableStatementSetBlob(InnerCallableStatement callableStatement, String parameterName, Blob x) throws SQLException {
		callableStatement.getDelegate().setBlob(parameterName, x);

	}

	protected void callableStatementSetBlob(InnerCallableStatement callableStatement, String parameterName, InputStream inputStream) throws SQLException {
		callableStatement.getDelegate().setBlob(parameterName, inputStream);

	}

	protected void callableStatementSetBlob(InnerCallableStatement callableStatement, String parameterName, InputStream inputStream, long length)
			throws SQLException {
		callableStatement.getDelegate().setBlob(parameterName, inputStream, length);

	}

	protected void callableStatementSetCharacterStream(InnerCallableStatement callableStatement, String parameterName, Reader reader) throws SQLException {
		callableStatement.getDelegate().setCharacterStream(parameterName, reader);

	}

	protected void callableStatementSetCharacterStream(InnerCallableStatement callableStatement, String parameterName, Reader reader, long length)
			throws SQLException {
		callableStatement.getDelegate().setCharacterStream(parameterName, reader, length);

	}

	protected void callableStatementSetClob(InnerCallableStatement callableStatement, String parameterName, Clob x) throws SQLException {
		callableStatement.getDelegate().setClob(parameterName, x);

	}

	protected void callableStatementSetClob(InnerCallableStatement callableStatement, String parameterName, Reader reader) throws SQLException {
		callableStatement.getDelegate().setClob(parameterName, reader);

	}

	protected void callableStatementSetClob(InnerCallableStatement callableStatement, String parameterName, Reader reader, long length)
			throws SQLException {
		callableStatement.getDelegate().setClob(parameterName, reader, length);

	}

	protected void callableStatementSetNCharacterStream(InnerCallableStatement callableStatement, String parameterName, Reader value) throws SQLException {
		callableStatement.getDelegate().setNCharacterStream(parameterName, value);

	}

	protected void callableStatementSetNCharacterStream(InnerCallableStatement callableStatement, String parameterName, Reader value, long length)
			throws SQLException {
		callableStatement.getDelegate().setNCharacterStream(parameterName, value, length);

	}

	protected void callableStatementSetNClob(InnerCallableStatement callableStatement, String parameterName, NClob value) throws SQLException {
		callableStatement.getDelegate().setNClob(parameterName, value);

	}

	protected void callableStatementSetNClob(InnerCallableStatement callableStatement, String parameterName, Reader reader) throws SQLException {
		callableStatement.getDelegate().setNClob(parameterName, reader);

	}

	protected void callableStatementSetNClob(InnerCallableStatement callableStatement, String parameterName, Reader reader, long length)
			throws SQLException {
		callableStatement.getDelegate().setNClob(parameterName, reader, length);

	}

	protected void callableStatementSetNString(InnerCallableStatement callableStatement, String parameterName, String value) throws SQLException {
		callableStatement.getDelegate().setNString(parameterName, value);

	}

	protected void callableStatementSetRowId(InnerCallableStatement callableStatement, String parameterName, RowId x) throws SQLException {
		callableStatement.getDelegate().setRowId(parameterName, x);
	}

	protected void callableStatementSetSQLXML(InnerCallableStatement callableStatement, String parameterName, SQLXML xmlObject) throws SQLException {
		callableStatement.getDelegate().setSQLXML(parameterName, xmlObject);
	}

	protected void callableStatementCloseOnCompletion(InnerCallableStatement callableStatement) throws SQLException {
		callableStatement.getDelegate().closeOnCompletion();
	}

	protected boolean callableStatementIsCloseOnCompletion(InnerCallableStatement callableStatement) throws SQLException {
		return callableStatement.getDelegate().isCloseOnCompletion();
	}

	protected <T> T callableStatementGetObject(InnerCallableStatement callableStatement, int parameterIndex, Class<T> type) throws SQLException {
		return callableStatement.getDelegate().getObject(parameterIndex, type);
	}

	protected <T> T callableStatementGetObject(InnerCallableStatement callableStatement, String parameterName, Class<T> type) throws SQLException {
		return callableStatement.getDelegate().getObject(parameterName, type);
	}

	protected void resultSetInit(InnerConnection.InnerStatement.InnerResultSet resultSet) {
	}

	protected boolean resultSetAbsolute(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().absolute(intVar0);
	}

	protected void resultSetAfterLast(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().afterLast();
	}

	protected void resultSetBeforeFirst(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().beforeFirst();
	}

	protected void resultSetCancelRowUpdates(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().cancelRowUpdates();
	}

	protected void resultSetClearWarnings(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().clearWarnings();
	}

	protected void resultSetClose(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().close();
	}

	protected void resultSetDeleteRow(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().deleteRow();
	}

	protected int resultSetFindColumn(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().findColumn(stringVar0);
	}

	protected boolean resultSetFirst(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().first();
	}

	protected java.sql.Array resultSetGetArray(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getArray(intVar0);
	}

	protected java.sql.Array resultSetGetArray(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getArray(stringVar0);
	}

	protected java.io.InputStream resultSetGetAsciiStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getAsciiStream(intVar0);
	}

	protected java.io.InputStream resultSetGetAsciiStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getAsciiStream(stringVar0);
	}

	protected java.math.BigDecimal resultSetGetBigDecimal(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getBigDecimal(intVar0);
	}

	protected java.math.BigDecimal resultSetGetBigDecimal(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getBigDecimal(stringVar0);
	}

	@Deprecated
	protected java.math.BigDecimal resultSetGetBigDecimal(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0, int intVar1)
			throws SQLException {
		return resultSet.getDelegate().getBigDecimal(intVar0, intVar1);
	}

	@Deprecated
	protected java.math.BigDecimal resultSetGetBigDecimal(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0,
			int intVar1) throws SQLException {
		return resultSet.getDelegate().getBigDecimal(stringVar0, intVar1);
	}

	protected java.io.InputStream resultSetGetBinaryStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getBinaryStream(intVar0);
	}

	protected java.io.InputStream resultSetGetBinaryStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getBinaryStream(stringVar0);
	}

	protected java.sql.Blob resultSetGetBlob(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getBlob(intVar0);
	}

	protected java.sql.Blob resultSetGetBlob(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getBlob(stringVar0);
	}

	protected boolean resultSetGetBoolean(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getBoolean(intVar0);
	}

	protected boolean resultSetGetBoolean(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getBoolean(stringVar0);
	}

	protected byte resultSetGetByte(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getByte(intVar0);
	}

	protected byte resultSetGetByte(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getByte(stringVar0);
	}

	protected byte[] resultSetGetBytes(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getBytes(intVar0);
	}

	protected byte[] resultSetGetBytes(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getBytes(stringVar0);
	}

	protected java.io.Reader resultSetGetCharacterStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getCharacterStream(intVar0);
	}

	protected java.io.Reader resultSetGetCharacterStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getCharacterStream(stringVar0);
	}

	protected java.sql.Clob resultSetGetClob(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getClob(intVar0);
	}

	protected java.sql.Clob resultSetGetClob(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getClob(stringVar0);
	}

	protected int resultSetGetConcurrency(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().getConcurrency();
	}

	protected java.lang.String resultSetGetCursorName(
			InnerConnection.InnerStatement.InnerResultSet resultSet) throws SQLException {
		return resultSet.getDelegate().getCursorName();
	}

	protected java.sql.Date resultSetGetDate(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getDate(intVar0);
	}

	protected java.sql.Date resultSetGetDate(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getDate(stringVar0);
	}

	protected java.sql.Date resultSetGetDate(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return resultSet.getDelegate().getDate(intVar0, calendarVar1);
	}

	protected java.sql.Date resultSetGetDate(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return resultSet.getDelegate().getDate(stringVar0, calendarVar1);
	}

	protected double resultSetGetDouble(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getDouble(intVar0);
	}

	protected double resultSetGetDouble(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getDouble(stringVar0);
	}

	protected int resultSetGetFetchDirection(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().getFetchDirection();
	}

	protected int resultSetGetFetchSize(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().getFetchSize();
	}

	protected float resultSetGetFloat(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getFloat(intVar0);
	}

	protected float resultSetGetFloat(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getFloat(stringVar0);
	}

	protected int resultSetGetInt(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getInt(intVar0);
	}

	protected int resultSetGetInt(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getInt(stringVar0);
	}

	protected long resultSetGetLong(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getLong(intVar0);
	}

	protected long resultSetGetLong(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getLong(stringVar0);
	}

	protected java.sql.ResultSetMetaData resultSetGetMetaData(
			InnerConnection.InnerStatement.InnerResultSet resultSet) throws SQLException {
		return resultSet.getDelegate().getMetaData();
	}

	protected java.lang.Object resultSetGetObject(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getObject(intVar0);
	}

	protected java.lang.Object resultSetGetObject(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getObject(stringVar0);
	}

	protected java.sql.Ref resultSetGetRef(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getRef(intVar0);
	}

	protected java.sql.Ref resultSetGetRef(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getRef(stringVar0);
	}

	protected int resultSetGetRow(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().getRow();
	}

	protected short resultSetGetShort(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getShort(intVar0);
	}

	protected short resultSetGetShort(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getShort(stringVar0);
	}

	protected java.lang.String resultSetGetString(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getString(intVar0);
	}

	protected java.lang.String resultSetGetString(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getString(stringVar0);
	}

	protected java.sql.Time resultSetGetTime(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getTime(intVar0);
	}

	protected java.sql.Time resultSetGetTime(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getTime(stringVar0);
	}

	protected java.sql.Time resultSetGetTime(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return resultSet.getDelegate().getTime(intVar0, calendarVar1);
	}

	protected java.sql.Time resultSetGetTime(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return resultSet.getDelegate().getTime(stringVar0, calendarVar1);
	}

	protected java.sql.Timestamp resultSetGetTimestamp(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getTimestamp(intVar0);
	}

	protected java.sql.Timestamp resultSetGetTimestamp(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getTimestamp(stringVar0);
	}

	protected java.sql.Timestamp resultSetGetTimestamp(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return resultSet.getDelegate().getTimestamp(intVar0, calendarVar1);
	}

	protected java.sql.Timestamp resultSetGetTimestamp(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0,
			java.util.Calendar calendarVar1) throws SQLException {
		return resultSet.getDelegate().getTimestamp(stringVar0, calendarVar1);
	}

	protected int resultSetGetType(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().getType();
	}

	protected java.net.URL resultSetGetURL(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().getURL(intVar0);
	}

	protected java.net.URL resultSetGetURL(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		return resultSet.getDelegate().getURL(stringVar0);
	}

	@Deprecated
	protected java.io.InputStream resultSetGetUnicodeStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		return resultSet.getDelegate().getUnicodeStream(intVar0);
	}

	@Deprecated
	protected java.io.InputStream resultSetGetUnicodeStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0)
			throws SQLException {
		return resultSet.getDelegate().getUnicodeStream(stringVar0);
	}

	protected java.sql.SQLWarning resultSetGetWarnings(
			InnerConnection.InnerStatement.InnerResultSet resultSet) throws SQLException {
		return resultSet.getDelegate().getWarnings();
	}

	protected void resultSetInsertRow(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().insertRow();
	}

	protected boolean resultSetIsAfterLast(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().isAfterLast();
	}

	protected boolean resultSetIsBeforeFirst(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().isBeforeFirst();
	}

	protected boolean resultSetIsFirst(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().isFirst();
	}

	protected boolean resultSetIsLast(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().isLast();
	}

	protected boolean resultSetLast(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().last();
	}

	protected void resultSetMoveToCurrentRow(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().moveToCurrentRow();
	}

	protected void resultSetMoveToInsertRow(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().moveToInsertRow();
	}

	protected boolean resultSetNext(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().next();
	}

	protected boolean resultSetPrevious(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().previous();
	}

	protected void resultSetRefreshRow(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().refreshRow();
	}

	protected boolean resultSetRelative(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		return resultSet.getDelegate().relative(intVar0);
	}

	protected boolean resultSetRowDeleted(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().rowDeleted();
	}

	protected boolean resultSetRowInserted(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().rowInserted();
	}

	protected boolean resultSetRowUpdated(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().rowUpdated();
	}

	protected void resultSetSetFetchDirection(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0)
			throws SQLException {
		resultSet.getDelegate().setFetchDirection(intVar0);
	}

	protected void resultSetSetFetchSize(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		resultSet.getDelegate().setFetchSize(intVar0);
	}

	protected void resultSetUpdateArray(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, java.sql.Array arrayVar1) throws SQLException {
		resultSet.getDelegate().updateArray(intVar0, arrayVar1);
	}

	protected void resultSetUpdateArray(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, java.sql.Array arrayVar1) throws SQLException {
		resultSet.getDelegate().updateArray(stringVar0, arrayVar1);
	}

	protected void resultSetUpdateAsciiStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0,
			java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
		resultSet.getDelegate().updateAsciiStream(intVar0, inputStreamVar1, intVar2);
	}

	protected void resultSetUpdateAsciiStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0,
			java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
		resultSet.getDelegate().updateAsciiStream(stringVar0, inputStreamVar1, intVar2);
	}

	protected void resultSetUpdateBigDecimal(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0,
			java.math.BigDecimal bigDecimalVar1) throws SQLException {
		resultSet.getDelegate().updateBigDecimal(intVar0, bigDecimalVar1);
	}

	protected void resultSetUpdateBigDecimal(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0,
			java.math.BigDecimal bigDecimalVar1) throws SQLException {
		resultSet.getDelegate().updateBigDecimal(stringVar0, bigDecimalVar1);
	}

	protected void resultSetUpdateBinaryStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0,
			java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
		resultSet.getDelegate().updateBinaryStream(intVar0, inputStreamVar1, intVar2);
	}

	protected void resultSetUpdateBinaryStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0,
			java.io.InputStream inputStreamVar1, int intVar2) throws SQLException {
		resultSet.getDelegate().updateBinaryStream(stringVar0, inputStreamVar1, intVar2);
	}

	protected void resultSetUpdateBlob(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, java.sql.Blob blobVar1) throws SQLException {
		resultSet.getDelegate().updateBlob(intVar0, blobVar1);
	}

	protected void resultSetUpdateBlob(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, java.sql.Blob blobVar1) throws SQLException {
		resultSet.getDelegate().updateBlob(stringVar0, blobVar1);
	}

	protected void resultSetUpdateBoolean(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, boolean booleanVar1) throws SQLException {
		resultSet.getDelegate().updateBoolean(intVar0, booleanVar1);
	}

	protected void resultSetUpdateBoolean(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, boolean booleanVar1) throws SQLException {
		resultSet.getDelegate().updateBoolean(stringVar0, booleanVar1);
	}

	protected void resultSetUpdateByte(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, byte byteVar1) throws SQLException {
		resultSet.getDelegate().updateByte(intVar0, byteVar1);
	}

	protected void resultSetUpdateByte(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, byte byteVar1) throws SQLException {
		resultSet.getDelegate().updateByte(stringVar0, byteVar1);
	}

	protected void resultSetUpdateBytes(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, byte[] byteVar1) throws SQLException {
		resultSet.getDelegate().updateBytes(intVar0, byteVar1);
	}

	protected void resultSetUpdateBytes(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, byte[] byteVar1) throws SQLException {
		resultSet.getDelegate().updateBytes(stringVar0, byteVar1);
	}

	protected void resultSetUpdateCharacterStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0,
			java.io.Reader readerVar1, int intVar2) throws SQLException {
		resultSet.getDelegate().updateCharacterStream(intVar0, readerVar1, intVar2);
	}

	protected void resultSetUpdateCharacterStream(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0,
			java.io.Reader readerVar1, int intVar2) throws SQLException {
		resultSet.getDelegate().updateCharacterStream(stringVar0, readerVar1, intVar2);
	}

	protected void resultSetUpdateClob(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, java.sql.Clob clobVar1) throws SQLException {
		resultSet.getDelegate().updateClob(intVar0, clobVar1);
	}

	protected void resultSetUpdateClob(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, java.sql.Clob clobVar1) throws SQLException {
		resultSet.getDelegate().updateClob(stringVar0, clobVar1);
	}

	protected void resultSetUpdateDate(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, java.sql.Date dateVar1) throws SQLException {
		resultSet.getDelegate().updateDate(intVar0, dateVar1);
	}

	protected void resultSetUpdateDate(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, java.sql.Date dateVar1) throws SQLException {
		resultSet.getDelegate().updateDate(stringVar0, dateVar1);
	}

	protected void resultSetUpdateDouble(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, double doubleVar1) throws SQLException {
		resultSet.getDelegate().updateDouble(intVar0, doubleVar1);
	}

	protected void resultSetUpdateDouble(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, double doubleVar1) throws SQLException {
		resultSet.getDelegate().updateDouble(stringVar0, doubleVar1);
	}

	protected void resultSetUpdateFloat(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, float floatVar1) throws SQLException {
		resultSet.getDelegate().updateFloat(intVar0, floatVar1);
	}

	protected void resultSetUpdateFloat(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, float floatVar1) throws SQLException {
		resultSet.getDelegate().updateFloat(stringVar0, floatVar1);
	}

	protected void resultSetUpdateInt(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, int intVar1) throws SQLException {
		resultSet.getDelegate().updateInt(intVar0, intVar1);
	}

	protected void resultSetUpdateInt(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, int intVar1) throws SQLException {
		resultSet.getDelegate().updateInt(stringVar0, intVar1);
	}

	protected void resultSetUpdateLong(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, long longVar1) throws SQLException {
		resultSet.getDelegate().updateLong(intVar0, longVar1);
	}

	protected void resultSetUpdateLong(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, long longVar1) throws SQLException {
		resultSet.getDelegate().updateLong(stringVar0, longVar1);
	}

	protected void resultSetUpdateNull(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0) throws SQLException {
		resultSet.getDelegate().updateNull(intVar0);
	}

	protected void resultSetUpdateNull(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0) throws SQLException {
		resultSet.getDelegate().updateNull(stringVar0);
	}

	protected void resultSetUpdateObject(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, java.lang.Object objectVar1) throws SQLException {
		resultSet.getDelegate().updateObject(intVar0, objectVar1);
	}

	protected void resultSetUpdateObject(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, java.lang.Object objectVar1) throws SQLException {
		resultSet.getDelegate().updateObject(stringVar0, objectVar1);
	}

	protected void resultSetUpdateObject(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, java.lang.Object objectVar1, int intVar2) throws SQLException {
		resultSet.getDelegate().updateObject(intVar0, objectVar1, intVar2);
	}

	protected void resultSetUpdateObject(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, java.lang.Object objectVar1, int intVar2)
			throws SQLException {
		resultSet.getDelegate().updateObject(stringVar0, objectVar1, intVar2);
	}

	protected void resultSetUpdateRef(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, java.sql.Ref refVar1) throws SQLException {
		resultSet.getDelegate().updateRef(intVar0, refVar1);
	}

	protected void resultSetUpdateRef(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, java.sql.Ref refVar1) throws SQLException {
		resultSet.getDelegate().updateRef(stringVar0, refVar1);
	}

	protected void resultSetUpdateRow(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		resultSet.getDelegate().updateRow();
	}

	protected void resultSetUpdateShort(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, short shortVar1) throws SQLException {
		resultSet.getDelegate().updateShort(intVar0, shortVar1);
	}

	protected void resultSetUpdateShort(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, short shortVar1) throws SQLException {
		resultSet.getDelegate().updateShort(stringVar0, shortVar1);
	}

	protected void resultSetUpdateString(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, java.lang.String stringVar1) throws SQLException {
		resultSet.getDelegate().updateString(intVar0, stringVar1);
	}

	protected void resultSetUpdateString(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, java.lang.String stringVar1) throws SQLException {
		resultSet.getDelegate().updateString(stringVar0, stringVar1);
	}

	protected void resultSetUpdateTime(InnerConnection.InnerStatement.InnerResultSet resultSet,
			int intVar0, java.sql.Time timeVar1) throws SQLException {
		resultSet.getDelegate().updateTime(intVar0, timeVar1);
	}

	protected void resultSetUpdateTime(InnerConnection.InnerStatement.InnerResultSet resultSet,
			java.lang.String stringVar0, java.sql.Time timeVar1) throws SQLException {
		resultSet.getDelegate().updateTime(stringVar0, timeVar1);
	}

	protected void resultSetUpdateTimestamp(
			InnerConnection.InnerStatement.InnerResultSet resultSet, int intVar0,
			java.sql.Timestamp timestampVar1) throws SQLException {
		resultSet.getDelegate().updateTimestamp(intVar0, timestampVar1);
	}

	protected void resultSetUpdateTimestamp(
			InnerConnection.InnerStatement.InnerResultSet resultSet, java.lang.String stringVar0,
			java.sql.Timestamp timestampVar1) throws SQLException {
		resultSet.getDelegate().updateTimestamp(stringVar0, timestampVar1);
	}

	protected boolean resultSetWasNull(InnerConnection.InnerStatement.InnerResultSet resultSet)
			throws SQLException {
		return resultSet.getDelegate().wasNull();
	}

	protected int resultSetGetHoldability(InnerResultSet resultSet) throws SQLException {
		return resultSet.getDelegate().getHoldability();
	}

	protected Reader resultSetGetNCharacterStream(InnerResultSet resultSet, int columnIndex) throws SQLException {
		return resultSet.getDelegate().getNCharacterStream(columnIndex);
	}

	protected Reader resultSetGetNCharacterStream(InnerResultSet resultSet, String columnLabel) throws SQLException {
		return resultSet.getDelegate().getNCharacterStream(columnLabel);
	}

	protected NClob resultSetGetNClob(InnerResultSet resultSet, int columnIndex) throws SQLException {
		return resultSet.getDelegate().getNClob(columnIndex);
	}

	protected NClob resultSetGetNClob(InnerResultSet resultSet, String columnLabel) throws SQLException {
		return resultSet.getDelegate().getNClob(columnLabel);
	}

	protected String resultSetGetNString(InnerResultSet resultSet, int columnIndex) throws SQLException {
		return resultSet.getDelegate().getNString(columnIndex);
	}

	protected String resultSetGetNString(InnerResultSet resultSet, String columnLabel) throws SQLException {
		return resultSet.getDelegate().getNString(columnLabel);
	}

	protected Object resultSetGetObject(InnerResultSet resultSet, int columnIndex, Map<String, Class<?>> map)
			throws SQLException {
		return resultSet.getDelegate().getObject(columnIndex, map);
	}

	protected Object resultSetGetObject(InnerResultSet resultSet, String columnLabel, Map<String, Class<?>> map)
			throws SQLException {
		return resultSet.getDelegate().getObject(columnLabel, map);
	}

	protected RowId resultSetGetRowId(InnerResultSet resultSet, int columnIndex) throws SQLException {
		return resultSet.getDelegate().getRowId(columnIndex);
	}

	protected RowId resultSetGetRowId(InnerResultSet resultSet, String columnLabel) throws SQLException {
		return resultSet.getDelegate().getRowId(columnLabel);
	}

	protected SQLXML resultSetGetSQLXML(InnerResultSet resultSet, int columnIndex) throws SQLException {
		return resultSet.getDelegate().getSQLXML(columnIndex);
	}

	protected SQLXML resultSetGetSQLXML(InnerResultSet resultSet, String columnLabel) throws SQLException {
		return resultSet.getDelegate().getSQLXML(columnLabel);
	}

	public boolean resultSetIsClosed(InnerResultSet resultSet) throws SQLException {
		return resultSet.getDelegate().isClosed();
	}

	protected void resultSetUpdateAsciiStream(InnerResultSet resultSet,int columnIndex, InputStream x) throws SQLException {
		resultSet.getDelegate().updateAsciiStream(columnIndex, x);

	}

	protected void resultSetUpdateAsciiStream(InnerResultSet resultSet, String columnLabel, InputStream x)
			throws SQLException {
		resultSet.getDelegate().updateAsciiStream(columnLabel, x);

	}

	protected void resultSetUpdateAsciiStream(InnerResultSet resultSet, int columnIndex, InputStream x, long length)
			throws SQLException {
		resultSet.getDelegate().updateAsciiStream(columnIndex, x, length);

	}

	protected void resultSetUpdateAsciiStream(InnerResultSet resultSet, String columnLabel, InputStream x, long length)
			throws SQLException {
		resultSet.getDelegate().updateAsciiStream(columnLabel, x, length);

	}

	protected void resultSetUpdateBinaryStream(InnerResultSet resultSet, int columnIndex, InputStream x) throws SQLException {
		resultSet.getDelegate().updateBinaryStream(columnIndex, x);

	}

	protected void resultSetUpdateBinaryStream(InnerResultSet resultSet, String columnLabel, InputStream x)
			throws SQLException {
		resultSet.getDelegate().updateBinaryStream(columnLabel, x);

	}

	protected void resultSetUpdateBinaryStream(InnerResultSet resultSet, int columnIndex, InputStream x, long length)
			throws SQLException {
		resultSet.getDelegate().updateBinaryStream(columnIndex, x, length);

	}

	protected void resultSetUpdateBinaryStream(InnerResultSet resultSet, String columnLabel, InputStream x, long length)
			throws SQLException {
		resultSet.getDelegate().updateBinaryStream(columnLabel, x, length);

	}

	protected void resultSetUpdateBlob(InnerResultSet resultSet, int columnIndex, InputStream inputStream)
			throws SQLException {
		resultSet.getDelegate().updateBlob(columnIndex, inputStream);

	}

	protected void resultSetUpdateBlob(InnerResultSet resultSet, String columnLabel, InputStream inputStream)
			throws SQLException {
		resultSet.getDelegate().updateBlob(columnLabel, inputStream);

	}

	protected void resultSetUpdateBlob(InnerResultSet resultSet, int columnIndex, InputStream inputStream, long length)
			throws SQLException {
		resultSet.getDelegate().updateBlob(columnIndex, inputStream, length);

	}

	protected void resultSetUpdateBlob(InnerResultSet resultSet, String columnLabel, InputStream inputStream, long length)
			throws SQLException {
		resultSet.getDelegate().updateBlob(columnLabel, inputStream, length);

	}

	protected void resultSetUpdateCharacterStream(InnerResultSet resultSet, int columnIndex, Reader x) throws SQLException {
		resultSet.getDelegate().updateCharacterStream(columnIndex, x);

	}

	protected void resultSetUpdateCharacterStream(InnerResultSet resultSet, String columnLabel, Reader reader)
			throws SQLException {
		resultSet.getDelegate().updateCharacterStream(columnLabel, reader);

	}

	protected void resultSetUpdateCharacterStream(InnerResultSet resultSet, int columnIndex, Reader x, long length)
			throws SQLException {
		resultSet.getDelegate().updateCharacterStream(columnIndex, x, length);

	}

	protected void resultSetUpdateCharacterStream(InnerResultSet resultSet, String columnLabel, Reader reader, long length)
			throws SQLException {
		resultSet.getDelegate().updateCharacterStream(columnLabel, reader, length);

	}

	protected void resultSetUpdateClob(InnerResultSet resultSet, int columnIndex, Reader reader) throws SQLException {
		resultSet.getDelegate().updateClob(columnIndex, reader);

	}

	protected void resultSetUpdateClob(InnerResultSet resultSet, String columnLabel, Reader reader) throws SQLException {
		resultSet.getDelegate().updateClob(columnLabel, reader);

	}

	protected void resultSetUpdateClob(InnerResultSet resultSet, int columnIndex, Reader reader, long length)
			throws SQLException {
		resultSet.getDelegate().updateClob(columnIndex, reader, length);

	}

	protected void resultSetUpdateClob(InnerResultSet resultSet, String columnLabel, Reader reader, long length)
			throws SQLException {
		resultSet.getDelegate().updateClob(columnLabel, reader, length);

	}

	protected void resultSetUpdateNCharacterStream(InnerResultSet resultSet, int columnIndex, Reader x) throws SQLException {
		resultSet.getDelegate().updateNCharacterStream(columnIndex, x);

	}

	protected void resultSetUpdateNCharacterStream(InnerResultSet resultSet, String columnLabel, Reader reader)
			throws SQLException {
		resultSet.getDelegate().updateNCharacterStream(columnLabel, reader);

	}

	protected void resultSetUpdateNCharacterStream(InnerResultSet resultSet, int columnIndex, Reader x, long length)
			throws SQLException {
		resultSet.getDelegate().updateNCharacterStream(columnIndex, x, length);

	}

	protected void resultSetUpdateNCharacterStream(InnerResultSet resultSet, String columnLabel, Reader reader, long length)
			throws SQLException {
		resultSet.getDelegate().updateNCharacterStream(columnLabel, reader, length);

	}

	protected void resultSetUpdateNClob(InnerResultSet resultSet, int columnIndex, NClob clob) throws SQLException {
		resultSet.getDelegate().updateNClob(columnIndex, clob);

	}

	protected void resultSetUpdateNClob(InnerResultSet resultSet, String columnLabel, NClob clob) throws SQLException {
		resultSet.getDelegate().updateNClob(columnLabel, clob);

	}

	protected void resultSetUpdateNClob(InnerResultSet resultSet, int columnIndex, Reader reader) throws SQLException {
		resultSet.getDelegate().updateNClob(columnIndex, reader);

	}

	protected void resultSetUpdateNClob(InnerResultSet resultSet, String columnLabel, Reader reader) throws SQLException {
		resultSet.getDelegate().updateNClob(columnLabel, reader);

	}

	protected void resultSetUpdateNClob(InnerResultSet resultSet, int columnIndex, Reader reader, long length)
			throws SQLException {
		resultSet.getDelegate().updateNClob(columnIndex, reader, length);

	}

	protected void resultSetUpdateNClob(InnerResultSet resultSet, String columnLabel, Reader reader, long length)
			throws SQLException {
		resultSet.getDelegate().updateNClob(columnLabel, reader, length);

	}

	protected void resultSetUpdateNString(InnerResultSet resultSet, int columnIndex, String string) throws SQLException {
		resultSet.getDelegate().updateNString(columnIndex, string);

	}

	protected void resultSetUpdateNString(InnerResultSet resultSet, String columnLabel, String string) throws SQLException {
		resultSet.getDelegate().updateNString(columnLabel, string);

	}

	protected void resultSetUpdateRowId(InnerResultSet resultSet, int columnIndex, RowId x) throws SQLException {
		resultSet.getDelegate().updateRowId(columnIndex, x);

	}

	protected void resultSetUpdateRowId(InnerResultSet resultSet, String columnLabel, RowId x) throws SQLException {
		resultSet.getDelegate().updateRowId(columnLabel, x);

	}

	protected void resultSetUpdateSQLXML(InnerResultSet resultSet, int columnIndex, SQLXML xmlObject) throws SQLException {
		resultSet.getDelegate().updateSQLXML(columnIndex, xmlObject);

	}

	protected void resultSetUpdateSQLXML(InnerResultSet resultSet, String columnLabel, SQLXML xmlObject) throws SQLException {
		resultSet.getDelegate().updateSQLXML(columnLabel, xmlObject);

	}

	protected boolean resultSetIsWrapperFor(InnerResultSet resultSet, Class<?> iface) throws SQLException {
		return resultSet.getDelegate().isWrapperFor(iface);
	}

	protected <T> T resultSetUnwrap(InnerResultSet resultSet, Class<T> iface) throws SQLException {
		return resultSet.getDelegate().unwrap(iface);
	}

	protected <T> T resultSetGetObject(InnerResultSet resultSet, int columnIndex, Class<T> type) throws SQLException {
		return resultSet.getDelegate().getObject(columnIndex, type);
	}

	protected <T> T resultSetGetObject(InnerResultSet resultSet, String columnLabel, Class<T> type) throws SQLException {
		return resultSet.getDelegate().getObject(columnLabel, type);
	}

	protected void resultSetUpdateObject(InnerResultSet resultSet, int columnIndex, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
		resultSet.getDelegate().updateObject(columnIndex, x, targetSqlType, scaleOrLength);
	}

	protected void resultSetUpdateObject(InnerResultSet resultSet, String columnLabel, Object x, SQLType targetSqlType, int scaleOrLength) throws SQLException {
		resultSet.getDelegate().updateObject(columnLabel, x, targetSqlType, scaleOrLength);
	}

	protected void resultSetUpdateObject(InnerResultSet resultSet, int columnIndex, Object x, SQLType targetSqlType) throws SQLException {
		resultSet.getDelegate().updateObject(columnIndex, x, targetSqlType);
	}

	protected void resultSetUpdateObject(InnerResultSet resultSet, String columnLabel, Object x, SQLType targetSqlType) throws SQLException {
		resultSet.getDelegate().updateObject(columnLabel, x, targetSqlType);
	}
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return iface.isInstance(delegate);
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		if(!isWrapperFor(iface))
			throw new SQLException("Delegate is not an instance of " + iface.getName());
		return iface.cast(delegate);
	}
}
