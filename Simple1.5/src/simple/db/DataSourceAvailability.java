package simple.db;


public interface DataSourceAvailability {
	public boolean isAvailable(String dataSourceName) ;
	public void setAvailable(String dataSourceName, boolean available) ;
	public Iterable<String> getAvailables() ;
	public Iterable<String> getUnavailables() ;
	public void remove(String dataSourceName) ;
	public Iterable<String> getAll() ;
}
