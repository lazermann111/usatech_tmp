/*
 * BasicDataSourceFactory.java
 *
 * Created on March 12, 2003, 9:45 AM
 */

package simple.db;

import java.beans.IntrospectionException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.sql.DataSource;

import simple.app.jmx.DataSourceMXBean;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;

/** Some support for caching data sources
 * @author  Brian S. Krug
 */
public abstract class AbstractDataSourceFactory implements DataSourceFactory {
	private static final Log log = Log.getLog();
    protected class DataSourceCache extends LockSegmentCache<String,ExtendedDataSource,DataSourceNotFoundException> {
		@Override
		protected ExtendedDataSource createValue(String key, Object... additionalInfo) throws DataSourceNotFoundException {
			ExtendedDataSource ds = createDataSource(key);
			final ExtendedDataSource origDs = ds;
			DataSourceMXBean mbean = null;
			if(isDiagnosing()) {
				DiagnosticDataSource dds = new DiagnosticDataSource(ds);
	        	ds = dds;
	        	mbean = dds;
			}
			if(isTracking()) {
				TrackingDataSource tds = new TrackingDataSource(ds);
	        	ds = tds;
	        	mbean = tds;
			}
			if(origDs instanceof DataSourceMXBean)
				mbean = (DataSourceMXBean)origDs;
			if(mbean != null)
		        try {
					ManagementFactory.getPlatformMBeanServer().registerMBean(mbean, getMBeanName(key));
				} catch(InstanceAlreadyExistsException e) {
					log.warn("Could not register MBean for DataSource '" + key + "'", e);
				} catch(MBeanRegistrationException e) {
					log.warn("Could not register MBean for DataSource '" + key + "'", e);
				} catch(NotCompliantMBeanException e) {
					log.warn("Could not register MBean for DataSource '" + key + "'", e);
				} catch(MalformedObjectNameException e) {
					log.warn("Could not create Object Name for DataSource '" + key + "'", e);
				}
			return ds;
		}
		
		@Override
		protected boolean keyEquals(String key1, String key2) {
			if(isCaseSensitive())
				return key1.equals(key2);
			else
				return key1.equalsIgnoreCase(key2);
		}
		@Override
		protected boolean valueEquals(ExtendedDataSource value1, ExtendedDataSource value2) {
			return value1 == value2;
		}
		@Override
		public ExtendedDataSource remove(String key) {
			ExtendedDataSource ds = super.remove(key);
			if(ds != null && (isTracking() || isDiagnosing() || ds instanceof DataSourceMXBean))
				try {
					ManagementFactory.getPlatformMBeanServer().unregisterMBean(getMBeanName(key));
				} catch(MBeanRegistrationException e) {
					log.warn("Could not unregister MBean for DataSource '" + key + "'", e);
				} catch(InstanceNotFoundException e) {
					log.warn("Could not unregister MBean for DataSource '" + key + "'", e);
				} catch(MalformedObjectNameException e) {
					log.warn("Could not unregister MBean for DataSource '" + key + "'", e);
				}
			return ds;
		}
		@Override
		public boolean remove(Object key, Object value) {
			boolean removed = super.remove(key, value);
			if(removed && (isTracking() || isDiagnosing() || value instanceof DataSourceMXBean))
				try {
					ManagementFactory.getPlatformMBeanServer().unregisterMBean(getMBeanName(castKey(key)));
				} catch(MBeanRegistrationException e) {
					log.warn("Could not unregister MBean for DataSource '" + key + "'", e);
				} catch(InstanceNotFoundException e) {
					log.warn("Could not unregister MBean for DataSource '" + key + "'", e);
				} catch(MalformedObjectNameException e) {
					log.warn("Could not unregister MBean for DataSource '" + key + "'", e);
				}
			return removed;
		}
		@Override
		public void clear() {
			Iterator<Map.Entry<String, ExtendedDataSource>> iter = entrySet().iterator();
			while(iter.hasNext()) {
				Map.Entry<String, ExtendedDataSource> entry = iter.next();
				if(entry.getValue() != null && (isTracking() || isDiagnosing() || entry.getValue() instanceof DataSourceMXBean))
					try {
						ManagementFactory.getPlatformMBeanServer().unregisterMBean(getMBeanName(entry.getKey()));
					} catch(MBeanRegistrationException e) {
						log.warn("Could not unregister MBean for DataSource '" + entry.getKey() + "'", e);
					} catch(InstanceNotFoundException e) {
						log.warn("Could not unregister MBean for DataSource '" + entry.getKey() + "'", e);
					} catch(MalformedObjectNameException e) {
						log.warn("Could not unregister MBean for DataSource '" + entry.getKey() + "'", e);
					}
				iter.remove();
			}
		}
	}
    protected final Cache<String,ExtendedDataSource,DataSourceNotFoundException> sources = new DataSourceCache();
    protected final Set<String> allowDynamicSet = new HashSet<String>();
    protected boolean diagnosing;
    protected boolean tracking;

    public AbstractDataSourceFactory() {
    }

    protected abstract ExtendedDataSource createDataSource(String name) throws DataSourceNotFoundException ;
    protected abstract boolean isCaseSensitive() ;
    protected abstract String getDriverClassName(String dataSourceName) throws DataSourceNotFoundException ;
    protected ObjectName getMBeanName(String dataSourceName) throws MalformedObjectNameException {
    	return new ObjectName(DataSourceFactory.class.getPackage().getName() + ":Type=DataSourceFactory,Name=" + dataSourceName);
    }
    /**
     * @see simple.db.DataSourceFactory#getConnectionClass(java.lang.String)
     */
    public Class<? extends Connection> getConnectionClass(String name) throws DataSourceNotFoundException {
    	if(name != null)
    		name = name.toUpperCase();
    	String driverClassName = getDriverClassName(name);
    	if(driverClassName == null)
    		return Connection.class; //throw new DataSourceNotFoundException(name);
    	Class<? extends Driver> driverClass;
		try {
			driverClass = Class.forName(driverClassName).asSubclass(Driver.class);
		} catch(ClassNotFoundException e) {
			throw new DataSourceNotFoundException(name, "Class '" + driverClassName + "' not found in classpath", e);
		} catch(ClassCastException e) {
			throw new DataSourceNotFoundException(name, "Class '" + driverClassName + "' does not implement java.sql.DriverManager", e);
		}
    	return DataLayerMgr.getConnectionClassFromDriver(driverClass);
    }
    /** Returns a DataSource from its set of configured DataSources.
     *
     * @return The appropriate <CODE>javax.sql.DataSource</CODE>
     * @param name The unique name of the DataSource
     * @throws DataSourceNotFoundException
     */
    public ExtendedDataSource getDataSource(String name) throws DataSourceNotFoundException {
    	if(name != null)
    		name = name.toUpperCase();
    	if(log.isDebugEnabled())
    		log.debug("Getting DataSource '" + name +"'");
        ExtendedDataSource ds = sources.getOrCreate(name);
        if(log.isDebugEnabled())
        	log.debug("Got DataSource '" + name +"'");
        return ds;
    }

    public void clearCache() {
    	sources.clear();
    }

	public boolean isDiagnosing() {
		return diagnosing;
	}

	public void setDiagnosing(boolean diagnosing) {
		this.diagnosing = diagnosing;
	}

	public boolean isTracking() {
		return tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}

	public void close() {
		for(Iterator<String> iter = sources.keySet().iterator(); iter.hasNext(); ) {
			String dsn = iter.next();
			ExtendedDataSource ds = sources.remove(dsn);
			try {
				ds.close();
			} catch(SQLException e) {
				//ignore
			}
		}
	}
	/**
	 * @see simple.db.DataSourceFactory#isDynamicLoadingAllowed(java.lang.String)
	 */
	public boolean isDynamicLoadingAllowed(String dataSourceName) {
		if(dataSourceName != null)
			dataSourceName = dataSourceName.toUpperCase();
    	return allowDynamicSet.contains(dataSourceName);
	}

	public void setDynamicLoadingAllowed(String dataSourceName, boolean allowDynamicLoading) {
		if(dataSourceName != null)
			dataSourceName = dataSourceName.toUpperCase();
    	if(allowDynamicLoading)
			allowDynamicSet.add(dataSourceName);
		else
			allowDynamicSet.remove(dataSourceName);
	}

	protected boolean possiblySetBeanProperty(String name, Object value) {
		try {
			return ReflectionUtils.setBeanProperty(this, name, value, true, true);
		} catch(IntrospectionException e) {
			log.warn("Could not set property '" + name + "'", e);
		} catch(IllegalAccessException e) {
			log.warn("Could not set property '" + name + "'", e);
		} catch(InvocationTargetException e) {
			log.warn("Could not set property '" + name + "'", e);
		} catch(InstantiationException e) {
			log.warn("Could not set property '" + name + "'", e);
		} catch(ConvertException e) {
			log.warn("Could not set property '" + name + "'", e);
		} catch(ParseException e) {
			log.warn("Could not set property '" + name + "'", e);
		}
		return false;
	}
	 /** Wraps the given DataSource as an ExtendedDataSource
	 * @param ds
	 * @return
	 */
	protected ExtendedDataSource asExtendedDataSource(final DataSource ds) {
		if(ds instanceof ExtendedDataSource)
			return (ExtendedDataSource)ds;

		//Find methods on original to use in Extended
		final Method urlGetterMethod = getMethodSafely(ds, "getUrl");
		final Method usernameGetterMethod = getMethodSafely(ds, "getUsername");
		final Method passwordGetterMethod = getMethodSafely(ds, "getPassword");
		final Method usernameSetterMethod = getMethodSafely(ds, "setUsername");
		final Method passwordSetterMethod = getMethodSafely(ds, "setPassword");
		final Method closeMethod = getMethodSafely(ds, "close");

		return new ExtendedDataSource() {
			public String getUrl() {
				return ConvertUtils.getStringSafely(invokeMethodSafely(ds, urlGetterMethod));
			}
			public String getUsername() {
				return ConvertUtils.getStringSafely(invokeMethodSafely(ds, usernameGetterMethod));
			}

			public void setUsername(String username) {
				invokeMethodSafely(ds, usernameSetterMethod, username);
			}
			public String getPassword() {
				return ConvertUtils.getStringSafely(invokeMethodSafely(ds, passwordGetterMethod));
			}

			public void setPassword(String password) {
				invokeMethodSafely(ds, passwordSetterMethod, password);
			}
			public void close() throws SQLException {
				invokeMethodSafely(ds, closeMethod);
			}
			public Connection getConnection() throws SQLException {
				return ds.getConnection();
			}

			public Connection getConnection(String username, String password) throws SQLException {
				return ds.getConnection(username, password);
			}

			public int getLoginTimeout() throws SQLException {
				return ds.getLoginTimeout();
			}

			public PrintWriter getLogWriter() throws SQLException {
				return ds.getLogWriter();
			}

			public boolean isWrapperFor(Class<?> iface) throws SQLException {
				return ds.isWrapperFor(iface);
			}

			public void setLoginTimeout(int seconds) throws SQLException {
				ds.setLoginTimeout(seconds);
			}

			public void setLogWriter(PrintWriter out) throws SQLException {
				ds.setLogWriter(out);
			}

			public <T> T unwrap(Class<T> iface) throws SQLException {
				return ds.unwrap(iface);
			}

			public boolean isAvailable() {
				return true;
			}

			public Logger getParentLogger() throws SQLFeatureNotSupportedException {
				return Logger.getLogger(getClass().getPackage().getName());
			}
		};
	}

	protected static Method getMethodSafely(Object instance, String methodName) {
		try {
			return instance.getClass().getMethod(methodName);
		} catch(SecurityException e) {
			log.warn("Could not get method '" + methodName + "' on class " + instance.getClass().getName(), e);
		} catch(NoSuchMethodException e) {
			log.warn("Could not find method '" + methodName + "' on class " + instance.getClass().getName(), e);
		}
		return null;
	}
	protected static Object invokeMethodSafely(Object instance, Method method, Object... args) {
		if(method == null || instance == null)
			return null;
		try {
			return method.invoke(instance, args);
		} catch(SecurityException e) {
			log.warn("Could not invoke method '" + method.getName() + "' on class " + instance.getClass().getName(), e);
		} catch(IllegalArgumentException e) {
			log.warn("Could not invoke method '" + method.getName() + "' on class " + instance.getClass().getName(), e);
		} catch(IllegalAccessException e) {
			log.warn("Could not invoke method '" + method.getName() + "' on class " + instance.getClass().getName(), e);
		} catch(InvocationTargetException e) {
			log.warn("Could not invoke method '" + method.getName() + "' on class " + instance.getClass().getName(), e);
		}
		return null;
	}
}
