/**
 *
 */
package simple.db;

import java.sql.Connection;


/**
 * @author Brian S. Krug
 *
 */
public class NullDataSourceFactory implements DataSourceFactory {
	/**
	 * @see simple.db.DataSourceFactory#getConnectionClass(java.lang.String)
	 */
	public Class<? extends Connection> getConnectionClass(String name) throws DataSourceNotFoundException {
		return null;
	}
	/**
	 * @see simple.db.DataSourceFactory#getDataSource(java.lang.String)
	 */
	public ExtendedDataSource getDataSource(String name) throws DataSourceNotFoundException {
		return null;
	}
	/**
	 * @see simple.db.DataSourceFactory#getDataSourceNames()
	 */
	public String[] getDataSourceNames() {
		return null;
	}
	/**
	 * @see simple.db.DataSourceFactory#isDynamicLoadingAllowed(java.lang.String)
	 */
	public boolean isDynamicLoadingAllowed(String dataSourceName) {
		return false;
	}
	
	public void close() {
	}
}
