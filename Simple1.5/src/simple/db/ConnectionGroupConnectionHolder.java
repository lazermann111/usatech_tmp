package simple.db;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionGroupConnectionHolder extends AbstractConnectionHolder {
	protected final ConnectionGroup connGroup;
	protected final String dataSourceName;

	public ConnectionGroupConnectionHolder(final ConnectionGroup connGroup, final String dataSourceName) {
		super();
		this.connGroup = connGroup;
		this.dataSourceName = dataSourceName;
	}
	@Override
	protected Connection createConnection() throws SQLException, DataSourceNotFoundException {
		try {
			return connGroup.getConnection(dataSourceName, null);
		} catch(DataSourceNotFoundException e) {
			throw e;
		} catch(DataLayerException e) {
			throw new DataSourceNotFoundException("Could not create connection", e);
		}
	}
	/**
	 * @throws DataSourceNotFoundException
	 * @see simple.db.ConnectionHolder#getConnectionClass()
	 */
	public Class<? extends Connection> getConnectionClass() throws DataSourceNotFoundException {
		try {
			return getConnection().getClass();
		} catch(SQLException e) {
			throw new DataSourceNotFoundException("Could not create connection", e);
		}
	}
}
