package simple.db;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

public class DelegatingCallableStatement extends DelegatingPreparedStatement implements CallableStatement {
	protected final CallableStatement callableDelegate;
	
	public DelegatingCallableStatement(CallableStatement delegate, Connection connection) {
		super(delegate, connection);
		this.callableDelegate = delegate;
	}

	public Array getArray(int parameterIndex) throws SQLException {
		return callableDelegate.getArray(parameterIndex);
	}

	public Array getArray(String parameterName) throws SQLException {
		return callableDelegate.getArray(parameterName);
	}

	@Deprecated
	public BigDecimal getBigDecimal(int parameterIndex, int scale) throws SQLException {
		return callableDelegate.getBigDecimal(parameterIndex, scale);
	}

	public BigDecimal getBigDecimal(int parameterIndex) throws SQLException {
		return callableDelegate.getBigDecimal(parameterIndex);
	}

	public BigDecimal getBigDecimal(String parameterName) throws SQLException {
		return callableDelegate.getBigDecimal(parameterName);
	}

	public Blob getBlob(int parameterIndex) throws SQLException {
		return callableDelegate.getBlob(parameterIndex);
	}

	public Blob getBlob(String parameterName) throws SQLException {
		return callableDelegate.getBlob(parameterName);
	}

	public boolean getBoolean(int parameterIndex) throws SQLException {
		return callableDelegate.getBoolean(parameterIndex);
	}

	public boolean getBoolean(String parameterName) throws SQLException {
		return callableDelegate.getBoolean(parameterName);
	}

	public byte getByte(int parameterIndex) throws SQLException {
		return callableDelegate.getByte(parameterIndex);
	}

	public byte getByte(String parameterName) throws SQLException {
		return callableDelegate.getByte(parameterName);
	}

	public byte[] getBytes(int parameterIndex) throws SQLException {
		return callableDelegate.getBytes(parameterIndex);
	}

	public byte[] getBytes(String parameterName) throws SQLException {
		return callableDelegate.getBytes(parameterName);
	}

	public Reader getCharacterStream(int parameterIndex) throws SQLException {
		return callableDelegate.getCharacterStream(parameterIndex);
	}

	public Reader getCharacterStream(String parameterName) throws SQLException {
		return callableDelegate.getCharacterStream(parameterName);
	}

	public Clob getClob(int parameterIndex) throws SQLException {
		return callableDelegate.getClob(parameterIndex);
	}

	public Clob getClob(String parameterName) throws SQLException {
		return callableDelegate.getClob(parameterName);
	}

	public Date getDate(int parameterIndex, Calendar cal) throws SQLException {
		return callableDelegate.getDate(parameterIndex, cal);
	}

	public Date getDate(int parameterIndex) throws SQLException {
		return callableDelegate.getDate(parameterIndex);
	}

	public Date getDate(String parameterName, Calendar cal) throws SQLException {
		return callableDelegate.getDate(parameterName, cal);
	}

	public Date getDate(String parameterName) throws SQLException {
		return callableDelegate.getDate(parameterName);
	}

	public double getDouble(int parameterIndex) throws SQLException {
		return callableDelegate.getDouble(parameterIndex);
	}

	public double getDouble(String parameterName) throws SQLException {
		return callableDelegate.getDouble(parameterName);
	}

	public float getFloat(int parameterIndex) throws SQLException {
		return callableDelegate.getFloat(parameterIndex);
	}

	public float getFloat(String parameterName) throws SQLException {
		return callableDelegate.getFloat(parameterName);
	}

	public int getInt(int parameterIndex) throws SQLException {
		return callableDelegate.getInt(parameterIndex);
	}

	public int getInt(String parameterName) throws SQLException {
		return callableDelegate.getInt(parameterName);
	}

	public long getLong(int parameterIndex) throws SQLException {
		return callableDelegate.getLong(parameterIndex);
	}

	public long getLong(String parameterName) throws SQLException {
		return callableDelegate.getLong(parameterName);
	}

	public Reader getNCharacterStream(int parameterIndex) throws SQLException {
		return callableDelegate.getNCharacterStream(parameterIndex);
	}

	public Reader getNCharacterStream(String parameterName) throws SQLException {
		return callableDelegate.getNCharacterStream(parameterName);
	}

	public NClob getNClob(int parameterIndex) throws SQLException {
		return callableDelegate.getNClob(parameterIndex);
	}

	public NClob getNClob(String parameterName) throws SQLException {
		return callableDelegate.getNClob(parameterName);
	}

	public String getNString(int parameterIndex) throws SQLException {
		return callableDelegate.getNString(parameterIndex);
	}

	public String getNString(String parameterName) throws SQLException {
		return callableDelegate.getNString(parameterName);
	}

	public Object getObject(int parameterIndex, Map<String, Class<?>> map) throws SQLException {
		return callableDelegate.getObject(parameterIndex, map);
	}

	public Object getObject(int parameterIndex) throws SQLException {
		return callableDelegate.getObject(parameterIndex);
	}

	public Object getObject(String parameterName, Map<String, Class<?>> map) throws SQLException {
		return callableDelegate.getObject(parameterName, map);
	}

	public Object getObject(String parameterName) throws SQLException {
		return callableDelegate.getObject(parameterName);
	}

	public Ref getRef(int parameterIndex) throws SQLException {
		return callableDelegate.getRef(parameterIndex);
	}

	public Ref getRef(String parameterName) throws SQLException {
		return callableDelegate.getRef(parameterName);
	}

	public RowId getRowId(int parameterIndex) throws SQLException {
		return callableDelegate.getRowId(parameterIndex);
	}

	public RowId getRowId(String parameterName) throws SQLException {
		return callableDelegate.getRowId(parameterName);
	}

	public short getShort(int parameterIndex) throws SQLException {
		return callableDelegate.getShort(parameterIndex);
	}

	public short getShort(String parameterName) throws SQLException {
		return callableDelegate.getShort(parameterName);
	}

	public SQLXML getSQLXML(int parameterIndex) throws SQLException {
		return callableDelegate.getSQLXML(parameterIndex);
	}

	public SQLXML getSQLXML(String parameterName) throws SQLException {
		return callableDelegate.getSQLXML(parameterName);
	}

	public String getString(int parameterIndex) throws SQLException {
		return callableDelegate.getString(parameterIndex);
	}

	public String getString(String parameterName) throws SQLException {
		return callableDelegate.getString(parameterName);
	}

	public Time getTime(int parameterIndex, Calendar cal) throws SQLException {
		return callableDelegate.getTime(parameterIndex, cal);
	}

	public Time getTime(int parameterIndex) throws SQLException {
		return callableDelegate.getTime(parameterIndex);
	}

	public Time getTime(String parameterName, Calendar cal) throws SQLException {
		return callableDelegate.getTime(parameterName, cal);
	}

	public Time getTime(String parameterName) throws SQLException {
		return callableDelegate.getTime(parameterName);
	}

	public Timestamp getTimestamp(int parameterIndex, Calendar cal) throws SQLException {
		return callableDelegate.getTimestamp(parameterIndex, cal);
	}

	public Timestamp getTimestamp(int parameterIndex) throws SQLException {
		return callableDelegate.getTimestamp(parameterIndex);
	}

	public Timestamp getTimestamp(String parameterName, Calendar cal) throws SQLException {
		return callableDelegate.getTimestamp(parameterName, cal);
	}

	public Timestamp getTimestamp(String parameterName) throws SQLException {
		return callableDelegate.getTimestamp(parameterName);
	}

	public URL getURL(int parameterIndex) throws SQLException {
		return callableDelegate.getURL(parameterIndex);
	}

	public URL getURL(String parameterName) throws SQLException {
		return callableDelegate.getURL(parameterName);
	}

	public void registerOutParameter(int parameterIndex, int sqlType, int scale) throws SQLException {
		callableDelegate.registerOutParameter(parameterIndex, sqlType, scale);
	}

	public void registerOutParameter(int parameterIndex, int sqlType, String typeName) throws SQLException {
		callableDelegate.registerOutParameter(parameterIndex, sqlType, typeName);
	}

	public void registerOutParameter(int parameterIndex, int sqlType) throws SQLException {
		callableDelegate.registerOutParameter(parameterIndex, sqlType);
	}

	public void registerOutParameter(String parameterName, int sqlType, int scale) throws SQLException {
		callableDelegate.registerOutParameter(parameterName, sqlType, scale);
	}

	public void registerOutParameter(String parameterName, int sqlType, String typeName) throws SQLException {
		callableDelegate.registerOutParameter(parameterName, sqlType, typeName);
	}

	public void registerOutParameter(String parameterName, int sqlType) throws SQLException {
		callableDelegate.registerOutParameter(parameterName, sqlType);
	}

	public void setAsciiStream(String parameterName, InputStream x, int length) throws SQLException {
		callableDelegate.setAsciiStream(parameterName, x, length);
	}

	public void setAsciiStream(String parameterName, InputStream x, long length) throws SQLException {
		callableDelegate.setAsciiStream(parameterName, x, length);
	}

	public void setAsciiStream(String parameterName, InputStream x) throws SQLException {
		callableDelegate.setAsciiStream(parameterName, x);
	}

	public void setBigDecimal(String parameterName, BigDecimal x) throws SQLException {
		callableDelegate.setBigDecimal(parameterName, x);
	}

	public void setBinaryStream(String parameterName, InputStream x, int length) throws SQLException {
		callableDelegate.setBinaryStream(parameterName, x, length);
	}

	public void setBinaryStream(String parameterName, InputStream x, long length) throws SQLException {
		callableDelegate.setBinaryStream(parameterName, x, length);
	}

	public void setBinaryStream(String parameterName, InputStream x) throws SQLException {
		callableDelegate.setBinaryStream(parameterName, x);
	}

	public void setBlob(String parameterName, Blob x) throws SQLException {
		callableDelegate.setBlob(parameterName, x);
	}

	public void setBlob(String parameterName, InputStream inputStream, long length) throws SQLException {
		callableDelegate.setBlob(parameterName, inputStream, length);
	}

	public void setBlob(String parameterName, InputStream inputStream) throws SQLException {
		callableDelegate.setBlob(parameterName, inputStream);
	}

	public void setBoolean(String parameterName, boolean x) throws SQLException {
		callableDelegate.setBoolean(parameterName, x);
	}

	public void setByte(String parameterName, byte x) throws SQLException {
		callableDelegate.setByte(parameterName, x);
	}

	public void setBytes(String parameterName, byte[] x) throws SQLException {
		callableDelegate.setBytes(parameterName, x);
	}

	public void setCharacterStream(String parameterName, Reader reader, int length) throws SQLException {
		callableDelegate.setCharacterStream(parameterName, reader, length);
	}

	public void setCharacterStream(String parameterName, Reader reader, long length) throws SQLException {
		callableDelegate.setCharacterStream(parameterName, reader, length);
	}

	public void setCharacterStream(String parameterName, Reader reader) throws SQLException {
		callableDelegate.setCharacterStream(parameterName, reader);
	}

	public void setClob(String parameterName, Clob x) throws SQLException {
		callableDelegate.setClob(parameterName, x);
	}

	public void setClob(String parameterName, Reader reader, long length) throws SQLException {
		callableDelegate.setClob(parameterName, reader, length);
	}

	public void setClob(String parameterName, Reader reader) throws SQLException {
		callableDelegate.setClob(parameterName, reader);
	}

	public void setDate(String parameterName, Date x, Calendar cal) throws SQLException {
		callableDelegate.setDate(parameterName, x, cal);
	}

	public void setDate(String parameterName, Date x) throws SQLException {
		callableDelegate.setDate(parameterName, x);
	}

	public void setDouble(String parameterName, double x) throws SQLException {
		callableDelegate.setDouble(parameterName, x);
	}

	public void setFloat(String parameterName, float x) throws SQLException {
		callableDelegate.setFloat(parameterName, x);
	}

	public void setInt(String parameterName, int x) throws SQLException {
		callableDelegate.setInt(parameterName, x);
	}

	public void setLong(String parameterName, long x) throws SQLException {
		callableDelegate.setLong(parameterName, x);
	}

	public void setNCharacterStream(String parameterName, Reader value, long length) throws SQLException {
		callableDelegate.setNCharacterStream(parameterName, value, length);
	}

	public void setNCharacterStream(String parameterName, Reader value) throws SQLException {
		callableDelegate.setNCharacterStream(parameterName, value);
	}

	public void setNClob(String parameterName, NClob value) throws SQLException {
		callableDelegate.setNClob(parameterName, value);
	}

	public void setNClob(String parameterName, Reader reader, long length) throws SQLException {
		callableDelegate.setNClob(parameterName, reader, length);
	}

	public void setNClob(String parameterName, Reader reader) throws SQLException {
		callableDelegate.setNClob(parameterName, reader);
	}

	public void setNString(String parameterName, String value) throws SQLException {
		callableDelegate.setNString(parameterName, value);
	}

	public void setNull(String parameterName, int sqlType, String typeName) throws SQLException {
		callableDelegate.setNull(parameterName, sqlType, typeName);
	}

	public void setNull(String parameterName, int sqlType) throws SQLException {
		callableDelegate.setNull(parameterName, sqlType);
	}

	public void setObject(String parameterName, Object x, int targetSqlType, int scale) throws SQLException {
		callableDelegate.setObject(parameterName, x, targetSqlType, scale);
	}

	public void setObject(String parameterName, Object x, int targetSqlType) throws SQLException {
		callableDelegate.setObject(parameterName, x, targetSqlType);
	}

	public void setObject(String parameterName, Object x) throws SQLException {
		callableDelegate.setObject(parameterName, x);
	}

	public void setRowId(String parameterName, RowId x) throws SQLException {
		callableDelegate.setRowId(parameterName, x);
	}

	public void setShort(String parameterName, short x) throws SQLException {
		callableDelegate.setShort(parameterName, x);
	}

	public void setSQLXML(String parameterName, SQLXML xmlObject) throws SQLException {
		callableDelegate.setSQLXML(parameterName, xmlObject);
	}

	public void setString(String parameterName, String x) throws SQLException {
		callableDelegate.setString(parameterName, x);
	}

	public void setTime(String parameterName, Time x, Calendar cal) throws SQLException {
		callableDelegate.setTime(parameterName, x, cal);
	}

	public void setTime(String parameterName, Time x) throws SQLException {
		callableDelegate.setTime(parameterName, x);
	}

	public void setTimestamp(String parameterName, Timestamp x, Calendar cal) throws SQLException {
		callableDelegate.setTimestamp(parameterName, x, cal);
	}

	public void setTimestamp(String parameterName, Timestamp x) throws SQLException {
		callableDelegate.setTimestamp(parameterName, x);
	}

	public void setURL(String parameterName, URL val) throws SQLException {
		callableDelegate.setURL(parameterName, val);
	}

	public boolean wasNull() throws SQLException {
		return callableDelegate.wasNull();
	}

	public <T> T getObject(int parameterIndex, Class<T> type) throws SQLException {
		return callableDelegate.getObject(parameterIndex, type);
	}

	public <T> T getObject(String parameterName, Class<T> type) throws SQLException {
		return callableDelegate.getObject(parameterName, type);
	}
}
