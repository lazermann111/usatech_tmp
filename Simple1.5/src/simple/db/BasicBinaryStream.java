/**
 *
 */
package simple.db;

import java.io.IOException;
import java.io.InputStream;

import simple.io.BinaryStream;
import simple.io.ByteOutput;
import simple.io.IOUtils;

/**
 * @author Brian S. Krug
 *
 */
public class BasicBinaryStream implements BinaryStream {
	protected final InputStream inputStream;
	protected final long length;

	public BasicBinaryStream(InputStream inputStream, long length) {
		super();
		this.inputStream = inputStream;
		this.length = length;
	}

	/**
	 * @see simple.io.BinaryStream#getLength()
	 */
	public long getLength() {
		return length;
	}

	/**
	 * @see simple.io.BinaryStream#getReader()
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	public long copyInto(ByteOutput output) throws IOException {
		return IOUtils.copy(getInputStream(), output, 0L, getLength());
	}
}
