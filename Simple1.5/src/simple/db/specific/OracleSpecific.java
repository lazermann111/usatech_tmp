package simple.db.specific;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Argument;
import simple.db.Call;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.NullDataSourceFactory;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.LoadingException;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.sql.ArraySQLType;
import simple.sql.Expression;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.sql.WrappingExpression;
import simple.text.StringUtils;

public class OracleSpecific extends AbstractSpecific {
	protected static DataLayer dataLayer;
	protected static Map<String,Integer> typeCodes = new HashMap<String, Integer>();
	protected static Map<Integer,String> typeNames = new HashMap<Integer, String>();
	protected int majorVersion;
	protected int minorVersion;
	protected Map<String,SortedMap<int[],ArraySQLType>> collTypesByElem = new HashMap<String, SortedMap<int[],ArraySQLType>>();
	protected Map<String,SQLType> compTypesByArrayName = new HashMap<String, SQLType>();
    protected static final Comparator<int[]> intArrayComparator = new Comparator<int[]>(){
        public int compare(int[] o1, int[] o2) {
            int diff = 0;
            for(int i = 0; diff == 0 && i < o1.length; i++) {
                diff = o1[i] - o2[i];
            }
            return diff;
        }
    };
    static {
    	loadTypeCodes(null); // for now, let's do this in the class
    }
	public OracleSpecific() {
	}

	@Override
	public Results getIndexes(Connection conn, String schemaName, String tableName, boolean unique) throws SQLException {
		// oracle calls ANALYZE TABLE if you use "conn.getMetaData().getIndexInfo(null, schemaName, tableName, unique, false)"
		try {
			initialize(conn);
			return dataLayer.executeQuery(conn, "GET_INDEX_COLUMNS", new Object[] {schemaName, tableName, (unique ? "Y" : "N")});
		} catch(DataLayerException e) {
			SQLException sqle = new SQLException();
			sqle.initCause(e);
			throw sqle;
		}
	}

	@Override
	public Expression createAggregateExpression(Expression expression, Aggregate aggregate, SQLType sqlType) {
		if(aggregate == null)
			return expression;
		else {
			StringBuilder prefix = new StringBuilder();
			StringBuilder suffix = new StringBuilder();
			switch(aggregate) {
				case ARRAY:
					wrapWithCollect(sqlType, prefix, suffix, false);
					break;
				case DISTINCT:
	    			prefix.append("COUNT(DISTINCT ");
	    			suffix.append(')');
	    			break;
	    		case SET:
	    			wrapWithCollect(sqlType, prefix, suffix, true);
					break;
	    		case FIRST: case LAST:
	    			throw new IllegalArgumentException("An expression for aggregate " + aggregate + " is not supported");
	    		default:
	    			prefix.append(aggregate.toString()).append('(');
	    			suffix.append(')');
	    	}
			return new WrappingExpression(prefix.toString(), expression, suffix.toString());
		}
	}

	@Override
	public Expression createParameterExpression(Expression expression, Aggregate aggregate, SQLType sqlType) {
		switch(aggregate) {
			case ARRAY:case SET:
				ArraySQLType arrayType = getArrayType(sqlType);
				if(arrayType == null) {
					return super.createParameterExpression(expression, aggregate, sqlType);
				} else {
					return new WrappingExpression("CAST(", expression, " AS " + arrayType.getTypeName() + ')');
				}
    		default:
    			return super.createParameterExpression(expression, aggregate, sqlType);
    	}	
	}

	protected void wrapWithCollect(SQLType sqlType, StringBuilder prefix, StringBuilder suffix, boolean distinct) {
		if(distinct && majorVersion <= 10)
			prefix.append("SET(");
		prefix.append("CAST(COLLECT(");
		if(distinct && majorVersion > 10)
			prefix.append("DISTINCT ");
		ArraySQLType arrayType = getArrayType(sqlType);
		if(arrayType == null) {
			suffix.append("))");
		} else {
			if(arrayType.getComponentType().getTypeName() != null && !arrayType.getComponentType().equals(sqlType)) {
				prefix.append("CAST(");
				suffix.append(" AS ").append(arrayType.getComponentType().getTypeName());
				if(arrayType.getComponentType().getPrecision() != null) {
					suffix.append('(').append(arrayType.getComponentType().getPrecision());
					if(arrayType.getComponentType().getScale() != null) {
						suffix.append(',').append(arrayType.getComponentType().getScale());
					}
					suffix.append(')');
				}
				suffix.append(')');
			}
			suffix.append(") AS ").append(arrayType.getTypeName()).append(')');
		}
		if(distinct && majorVersion <= 10)
			suffix.append(')');
	}
	@Override
	public ArraySQLType getArrayType(SQLType componentType) {
		if(componentType.getTypeCode() == Types.ARRAY) {
			SQLType innerType = getComponentType(componentType);
			componentType = getArrayType(innerType);
			SortedMap<int[], ArraySQLType> smap = collTypesByElem.get(componentType.getTypeName());
			if(smap != null && !smap.isEmpty()) return smap.values().iterator().next();
			return null;
		} else {
			String[] compats = getCompatibleTypeNames(componentType.getTypeCode());
			for(int i = 0; i < compats.length; i++) {
				SortedMap<int[], ArraySQLType> smap = collTypesByElem.get(compats[i]);
				if(smap != null) {
					ArraySQLType maxPrecisionType = null;
					for(ArraySQLType arrayType : smap.values()) {
						if(isCompatible(arrayType.getComponentType().getScale(), componentType.getScale())) {
							if(isCompatible(arrayType.getComponentType().getPrecision(), componentType.getPrecision())) {
								return arrayType;
							} else if(maxPrecisionType == null || maxPrecisionType.getComponentType().getPrecision() < arrayType.getComponentType().getPrecision()) {
								maxPrecisionType = arrayType;
							}
						}
					}
					if(maxPrecisionType != null)
						return maxPrecisionType;
				}
			}
		}
		return null;
	}

	protected boolean isCompatible(Integer i1, Integer i2) {
		if(i1 == null) return true;
		if(i2 == null) return false;
		return i1 >= i2;
	}

	@Override
	public SQLType getComponentType(SQLType arrayType) {
		//try to get from map first, as it's more reliable
		if(arrayType.getTypeName() != null && !"ARRAY".equalsIgnoreCase(arrayType.getTypeName())) {
			SQLType compType = compTypesByArrayName.get(arrayType.getTypeName());
			if(compType != null) return compType;
		}
		if(arrayType instanceof ArraySQLType)
			return ((ArraySQLType)arrayType).getComponentType();
		return null;
	}

	public void initialize(Connection conn) throws SQLException {
		DatabaseMetaData metadata = conn.getMetaData();
		this.majorVersion = metadata.getDatabaseMajorVersion();
		this.minorVersion = metadata.getDatabaseMinorVersion();
		if(dataLayer == null) {
			dataLayer = new DataLayer();
			dataLayer.setDataSourceFactory(new NullDataSourceFactory());
			try {
				ConfigLoader.loadConfig("resource:/simple/db/specific/oracle-specific-data-layer.xml", dataLayer);
			} catch(ConfigException e) {
				throw new SQLException("Could not load data-layer config file: " + e.getMessage());
			} catch(IOException e) {
				throw new SQLException("Could not load data-layer config file: " + e.getMessage());
			} catch(LoadingException e) {
				throw new SQLException("Could not load data-layer config file: " + e.getMessage());
			}
		}
		loadCollTypes(conn);
	}
	protected static final char[] IGNORE_TYPE_NAME_CHARS = new char[] {'=','+',' '};
	protected void loadCollTypes(Connection conn) throws SQLException {
		Results results;
		try {
			results = dataLayer.executeQuery(conn, "GET_COLL_TYPES", null);
		} catch(DataLayerException e) {
			throw new SQLException("Data Layer Exception: " + e.getMessage(), e);
		}
		try {
			int i = 0;
			while(results.next()) {
				String typeName = results.getValue("typeName", String.class);
				if(StringUtils.indexOf(typeName, IGNORE_TYPE_NAME_CHARS) < 0) { //ignore weird names
					String componentTypeName = results.getValue("componentTypeName", String.class);
					Integer size = results.getValue("size", Integer.class);
					Integer precision = results.getValue("precision", Integer.class);
					Integer scale = results.getValue("scale", Integer.class);
					Integer preference = results.getValue("preference", Integer.class);
					int componentTypeCode = getTypeCode(componentTypeName);
					if(precision == null || !exceedsLimit(componentTypeCode, precision)) { //ignore system special extra-long types
						SQLType componentType = new SQLType(componentTypeCode, componentTypeName, precision, scale);
						compTypesByArrayName.put(typeName, componentType);
						if(size == null) {
							SortedMap<int[], ArraySQLType> smap = collTypesByElem.get(componentTypeName);
							if(smap == null) {
								smap = new TreeMap<int[], ArraySQLType>(intArrayComparator);
								collTypesByElem.put(componentTypeName, smap);
							}
							ArraySQLType arrayType = new ArraySQLType(typeName, size, null, componentType);
							smap.put(new int[]{ preference, precision == null ? 0 : precision, i++}, arrayType);
						}
						if(componentTypeCode == Types.TIMESTAMP && size == null && "DATE".equals(componentTypeName)) {
							//oracle has a bug that causes CAST(COLLECT(value AS TIMESTAMP) AS TIMESTAMP_TABLE) to blow up
							componentType = new SQLType(Types.TIMESTAMP, "DATE", null, scale);
							componentTypeName = "TIMESTAMP";
							SortedMap<int[], ArraySQLType> smap = collTypesByElem.get(componentTypeName);
							if(smap == null) {
								smap = new TreeMap<int[], ArraySQLType>(intArrayComparator);
								collTypesByElem.put(componentTypeName, smap);
							}
							ArraySQLType arrayType = new ArraySQLType(typeName, size, null, componentType);
							smap.put(new int[]{ preference, precision == null ? 0 : precision, i++}, arrayType);
						}						
					}
				}
			}
		} catch(ConvertException e) {
			throw new SQLException("Could not convert results to sql type: " + e);
		}
	}

	protected static void loadTypeCodes(Connection conn) {
		//for now just manually populate
		typeCodes.put("ARRAY",2003);
		typeCodes.put("BFILE",2004);
		typeCodes.put("BINARY_DOUBLE", 8);
		typeCodes.put("BINARY_FLOAT", 6);
		typeCodes.put("BINARY ROWID", -3);
		typeCodes.put("BLOB",2004);
		typeCodes.put("CFILE",2005);
		typeCodes.put("CHAR",1);
		typeCodes.put("CLOB",2005);
		typeCodes.put("DATE",93);
		typeCodes.put("DECIMAL", 3);
		typeCodes.put("DOUBLE PRECISION", 8);
		typeCodes.put("FLOAT",6);
		typeCodes.put("INTEGER", 4);
		typeCodes.put("INTERVAL DAY TO SECOND", -104);
		typeCodes.put("INTERVAL YEAR TO MONTH", -103);
		typeCodes.put("INTERVALDS", -104);
		typeCodes.put("INTERVALYM",-103);
		typeCodes.put("LONG",-1);
		typeCodes.put("LONG RAW",-4);
		typeCodes.put("NUMBER",3);
		typeCodes.put("PL/SQL REF CURSOR",-10);
		typeCodes.put("POINTER",2006);
		typeCodes.put("RAW",-3);
		typeCodes.put("REAL",7);
		typeCodes.put("REF",2006);
		typeCodes.put("SIGNED BINARY INTEGER", 4);
		typeCodes.put("SIGNED BINARY INTEGER(8)", 4);
		typeCodes.put("SIGNED BINARY INTEGER(16)", 4);
		typeCodes.put("SIGNED BINARY INTEGER(32)", 4);
		typeCodes.put("SMALLINT", 5);
		typeCodes.put("STRUCT",2002);
		typeCodes.put("TABLE",2003);
		typeCodes.put("TIME", 92);
		typeCodes.put("TIME WITH TZ", 92);
		typeCodes.put("TIMESTAMP",93);
		typeCodes.put("TIMESTAMP WITH LOCAL TZ", 93);
		typeCodes.put("TIMESTAMP WITH LOCAL TIME ZONE",93); // -102
		typeCodes.put("TIMESTAMP WITH TZ", 93);
		typeCodes.put("TIMESTAMP WITH TIME ZONE",93);//-101
		typeCodes.put("UNSIGNED BINARY INTEGER", 4);
		typeCodes.put("UNSIGNED BINARY INTEGER(8)", 4);
		typeCodes.put("UNSIGNED BINARY INTEGER(16)", 4);
		typeCodes.put("UNSIGNED BINARY INTEGER(32)", 4);
		typeCodes.put("UROWID", -3);
		typeCodes.put("VARCHAR",12);
		typeCodes.put("VARCHAR2",12);
		typeCodes.put("VARRAY",2003);
		typeCodes.put("VARYING ARRAY",2003);

		typeNames.put(1, "VARCHAR");
		typeNames.put(2, "NUMBER");
		typeNames.put(3, "INTEGER");
		typeNames.put(4, "FLOAT");
		typeNames.put(7, "DECIMAL");
		typeNames.put(9, "VARCHAR2");
		typeNames.put(12, "DATE");
		typeNames.put(21, "REAL");
		typeNames.put(22, "DOUBLE PRECISION");
		typeNames.put(23, "UNSIGNED BINARY INTEGER(8)");
		typeNames.put(25, "UNSIGNED BINARY INTEGER(16)");
		typeNames.put(26, "UNSIGNED BINARY INTEGER(32)");
		typeNames.put(27, "SIGNED BINARY INTEGER(8)");
		typeNames.put(28, "SIGNED BINARY INTEGER(16)");
		typeNames.put(29, "SIGNED BINARY INTEGER(32)");
		typeNames.put(32, "POINTER");
		typeNames.put(69, "BINARY ROWID");
		typeNames.put(95, "RAW");
		typeNames.put(96, "CHAR");
		typeNames.put(100, "BINARY_FLOAT");
		typeNames.put(101, "BINARY_DOUBLE");
		typeNames.put(102, "PL/SQL REF CURSOR");
		typeNames.put(104, "UROWID");
		typeNames.put(110, "REF");
		typeNames.put(112, "CLOB");
		typeNames.put(113, "BLOB");
		typeNames.put(114, "BFILE");
		typeNames.put(115, "CFILE");
		typeNames.put(185, "TIME");
		typeNames.put(186, "TIME WITH TZ");
		typeNames.put(187, "TIMESTAMP");
		typeNames.put(188, "TIMESTAMP WITH TZ");
		typeNames.put(189, "INTERVAL YEAR TO MONTH");
		typeNames.put(190, "INTERVAL DAY TO SECOND");
		typeNames.put(228, "NAMED COLLECTION");
		typeNames.put(228, "NAMED OBJECT");
		typeNames.put(232, "TIMESTAMP WITH LOCAL TZ");
		typeNames.put(245, "OCTET");
		typeNames.put(246, "SMALLINT");
		typeNames.put(247, "VARYING ARRAY");
		typeNames.put(248, "TABLE");
		typeNames.put(250, "PL/SQL RECORD");
		typeNames.put(251, "PL/SQL COLLECTION");
		typeNames.put(252, "PL/SQL BOOLEAN");
		typeNames.put(256, "OID");
		typeNames.put(257, "CONTIGUOUS ARRAY");
		typeNames.put(258, "CANONICAL");
		typeNames.put(259, "LOB POINTER");
		typeNames.put(260, "PL/SQL POSITIVE");
		typeNames.put(261, "PL/SQL POSITIVEN");
		typeNames.put(262, "PL/SQL ROWID");
		typeNames.put(263, "PL/SQL LONG");
		typeNames.put(264, "PL/SQL LONG RAW");
		typeNames.put(265, "PL/SQL BINARY INTEGER");
		typeNames.put(266, "PL/SQL PLS INTEGER");
		typeNames.put(267, "PL/SQL NATURAL");
		typeNames.put(268, "PL/SQL NATURALN");
		typeNames.put(269, "PL/SQL STRING");
	}

    protected boolean exceedsLimit(int typeCode, int precision) {
    	switch(typeCode) {
            case Types.CHAR: return precision > 2000;
            case Types.VARCHAR: return precision > 4000;
            case Types.NUMERIC: case Types.DECIMAL: return precision > 38;
            case Types.TIMESTAMP: return precision > 9;
            case Types.VARBINARY: return precision > 2000;
            default: return false;
    	}
	}

	protected static String[] getCompatibleTypeNames(int sqlType) {
        switch(sqlType) {
        	case Types.BIGINT:
                return new String[] {"NUMBER", "SIGNED BINARY INTEGER(32)",
                    "DECIMAL", "UNSIGNED BINARY INTEGER(32)"};
        	case Types.BINARY: return new String[] {"BLOB" , "BFILE"};
        	case Types.BIT:
                return new String[] {"SMALLINT", "NUMBER", "INTEGER",
                    "UNSIGNED BINARY INTEGER(8)","UNSIGNED BINARY INTEGER(16)",
                    "UNSIGNED BINARY INTEGER(32)","SIGNED BINARY INTEGER(8)",
                    "SIGNED BINARY INTEGER(16)", "SIGNED BINARY INTEGER(32)"};
        	case Types.BLOB: return new String[] {"BLOB" , "BFILE"};
            case Types.CHAR: return new String[] {"CHAR", "VARCHAR", "VARCHAR2", "CLOB", "CFILE"};
            case Types.CLOB: return new String[] {"CLOB", "CFILE"};
            case Types.DATE: return new String[] {"DATE", "TIMESTAMP","TIMESTAMP WITH LOCAL TZ","TIMESTAMP WITH TZ"};
            case Types.DECIMAL:
                return new String[] {"DECIMAL", "NUMBER", "REAL", "DOUBLE PRECISION", "FLOAT",
                    "BINARY_DOUBLE", "BINARY_FLOAT"};
            case Types.DOUBLE:
                return new String[] {"DOUBLE PRECISION", "REAL", "DECIMAL", "NUMBER", "FLOAT",
                    "BINARY_DOUBLE", "BINARY_FLOAT"};
            case Types.FLOAT:
                return new String[] {"FLOAT", "DECIMAL", "REAL", "DOUBLE PRECISION",
                    "BINARY_FLOAT", "BINARY_DOUBLE", "NUMBER"};
            case Types.INTEGER:
                return new String[] {"INTEGER", "NUMBER",
                    "SIGNED BINARY INTEGER(16)", "SIGNED BINARY INTEGER(32)", "DECIMAL",
                    "UNSIGNED BINARY INTEGER(16)", "UNSIGNED BINARY INTEGER(32)"};
            case Types.LONGVARBINARY: return new String[] {"BLOB", "BFILE" };
            case Types.LONGVARCHAR: return new String[] {"CLOB", "CFILE", "VARCHAR", "VARCHAR2", "CHAR"};
            case Types.NUMERIC:
                return new String[] {"NUMBER", "DECIMAL", "REAL", "DOUBLE PRECISION", "FLOAT",
                    "BINARY_DOUBLE", "BINARY_FLOAT", "INTEGER",
                    "SIGNED BINARY INTEGER(32)", "SIGNED BINARY INTEGER(16)",
                    "SIGNED BINARY INTEGER(8)", "SMALLINT", "UNSIGNED BINARY INTEGER(32)",
                    "UNSIGNED BINARY INTEGER(16)", "UNSIGNED BINARY INTEGER(8)"};
            case Types.REAL:
                return new String[] {"REAL", "DOUBLE PRECISION", "DECIMAL", "NUMBER", "FLOAT",
                    "BINARY_DOUBLE", "BINARY_FLOAT"};
            case Types.SMALLINT:
                return new String[] {"SMALLINT", "NUMBER", "INTEGER",
                    "SIGNED BINARY INTEGER(8)", "SIGNED BINARY INTEGER(16)",
                    "SIGNED BINARY INTEGER(32)", "DECIMAL", "UNSIGNED BINARY INTEGER(8)",
                    "UNSIGNED BINARY INTEGER(16)", "UNSIGNED BINARY INTEGER(32)"};
            case Types.TINYINT:
                return new String[] {"NUMBER", "SMALLINT", "INTEGER",
                    "SIGNED BINARY INTEGER(8)", "SIGNED BINARY INTEGER(16)",
                    "SIGNED BINARY INTEGER(32)", "DECIMAL", "UNSIGNED BINARY INTEGER(8)",
                    "UNSIGNED BINARY INTEGER(16)", "UNSIGNED BINARY INTEGER(32)"};
            case Types.TIME: return new String[] {"TIME", "TIME WITH TZ", "DATE", "TIMESTAMP","TIMESTAMP WITH LOCAL TZ","TIMESTAMP WITH TZ"};
            case Types.TIMESTAMP: return new String[] {"TIMESTAMP","TIMESTAMP WITH LOCAL TZ","TIMESTAMP WITH TZ", "DATE"};
            case Types.VARBINARY: return new String[] {"BLOB", "BFILE" };
            case Types.VARCHAR: return new String[] {"VARCHAR2", "VARCHAR", "CLOB", "CFILE", "CHAR"};
            default: return null;
        }
    }

    protected static int getDefaultLength(int sqlType) {
        switch(sqlType) {
            case Types.CHAR:
            	return 0;//return 100; - documentation claims that size is optional for char
            case Types.VARCHAR:
            	return 4000;
            case Types.LONGVARCHAR:
            case Types.NUMERIC:
            case Types.DECIMAL:
            case Types.BIT:
            case Types.TINYINT:
            case Types.SMALLINT:
            case Types.INTEGER:
            case Types.BIGINT:
            case Types.REAL:
            case Types.FLOAT:
            case Types.DOUBLE:
            case Types.BINARY:
            case Types.VARBINARY:
            case Types.LONGVARBINARY:
            case Types.DATE:
            case Types.TIME:
            case Types.TIMESTAMP:
            case Types.BLOB:
            case Types.CLOB:
            default:
            	return 0;
        }
    }

	@Override
	public int getTypeCode(String typeName) {
		Integer typeCode = typeCodes.get(typeName);
		if(typeCode != null) return typeCode;
		return 0;
	}

	@Override
	public String getNormalizedTypeName(SQLType sqlType) {
		if(sqlType.getTypeCode() == Types.ARRAY) {
			SQLType compType = getComponentType(sqlType);
			if(compType == null) compType = new SQLType(Types.VARCHAR); //use varchar as default as it is most flexible
			SQLType arrayType = getArrayType(compType);
			if(arrayType == null) return null;
			return arrayType.getTypeName();
		}
		String[] compats = getCompatibleTypeNames(sqlType.getTypeCode());
		return compats == null ? null : compats[0];
	}

	@Override
	public void verifyCall(Call call, Connection conn) throws SQLException {
		SQLType[] columnTypes = describeQuery(conn, call.getSql());
		Argument[] args = call.getArguments();
		verifyColumns(columnTypes, args);
		PreparedStatement ps = conn.prepareStatement(call.getSql());
		try {
			verifyParameters(ps.getParameterMetaData(), args);
		} finally {
			ps.close();
		}
	}

	@Override
	public ResultSetMetaData getQueryMetaData(Connection conn, String sql) throws SQLException {
		try {
			String nativeSQL = conn.nativeSQL(sql);
			Object[] ret = dataLayer.executeCall(conn, "DESCRIBE_SQL", new Object[] { nativeSQL });
			int colCount = ConvertUtils.getInt(ret[2]);
			if(colCount == -1)
				return null;
			Results results = (Results) ret[1];
			final List<SQLType> sqlTypes = new ArrayList<SQLType>();
			final List<String> names = new ArrayList<String>();
			while(results.next()) {
				SQLType sqlType = new SQLType();
				String typeName = typeNames.get(results.getValue("typeCode", int.class));
				sqlType.setTypeName(typeName);
				sqlType.setTypeCode(getTypeCode(typeName));
				sqlType.setPrecision(results.getValue("precision", Integer.class));
				sqlType.setScale(results.getValue("scale", Integer.class));
				sqlTypes.add(sqlType);
				names.add(results.getValue("name", String.class));
			}
			return new ResultSetMetaData() {
				protected SQLType getSQLType(int column) {
					return sqlTypes.get(column - 1);
				}
				@Override
				public <T> T unwrap(Class<T> iface) throws SQLException {
					return null;
				}

				@Override
				public boolean isWrapperFor(Class<?> iface) throws SQLException {
					return false;
				}

				@Override
				public boolean isWritable(int column) throws SQLException {
					return false;
				}

				@Override
				public boolean isSigned(int column) throws SQLException {
					return Number.class.isAssignableFrom(SQLTypeUtils.getJavaType(getSQLType(column)));
				}

				@Override
				public boolean isSearchable(int column) throws SQLException {
					return true;
				}

				@Override
				public boolean isReadOnly(int column) throws SQLException {
					return true;
				}

				@Override
				public int isNullable(int column) throws SQLException {
					return columnNullableUnknown;
				}

				@Override
				public boolean isDefinitelyWritable(int column) throws SQLException {
					return false;
				}

				@Override
				public boolean isCurrency(int column) throws SQLException {
					return false;
				}

				@Override
				public boolean isCaseSensitive(int column) throws SQLException {
					return String.class.isAssignableFrom(SQLTypeUtils.getJavaType(getSQLType(column)));
				}

				@Override
				public boolean isAutoIncrement(int column) throws SQLException {
					return false;
				}

				@Override
				public String getTableName(int column) throws SQLException {
					return null;
				}

				@Override
				public String getSchemaName(int column) throws SQLException {
					return null;
				}

				@Override
				public int getScale(int column) throws SQLException {
					Integer scale = getSQLType(column).getScale();
					return scale == null ? 0 : scale;
				}

				@Override
				public int getPrecision(int column) throws SQLException {
					Integer precision = getSQLType(column).getPrecision();
					return precision == null ? 0 : precision;
				}

				@Override
				public String getColumnTypeName(int column) throws SQLException {
					return getSQLType(column).getTypeName();
				}

				@Override
				public int getColumnType(int column) throws SQLException {
					return getSQLType(column).getTypeCode();
				}

				@Override
				public String getColumnName(int column) throws SQLException {
					return names.get(column - 1);
				}

				@Override
				public String getColumnLabel(int column) throws SQLException {
					return getColumnName(column);
				}

				@Override
				public int getColumnDisplaySize(int column) throws SQLException {
					return 0;
				}

				@Override
				public int getColumnCount() throws SQLException {
					return sqlTypes.size();
				}

				@Override
				public String getColumnClassName(int column) throws SQLException {
					return SQLTypeUtils.getJdbcType(getSQLType(column).getTypeCode()).getName();
				}

				@Override
				public String getCatalogName(int column) throws SQLException {
					return null;
				}
			};
		} catch(DataLayerException e) {
			SQLException sqle = new SQLException("Datalayer Exception");
			sqle.initCause(e);
			throw sqle;
		} catch(ConvertException e) {
			SQLException sqle = new SQLException("Convert Exception");
			sqle.initCause(e);
			throw sqle;
		}
	}

	@Override
	public SQLType[] describeQuery(Connection conn, String sql) throws SQLException {
		try {
			String nativeSQL = conn.nativeSQL(sql);
			/*dataLayer.executeCall(conn, "POPULATE_DESC_TAB", new Object[] {nativeSQL});
			Results results = dataLayer.executeQuery(conn, "GET_DESC_TAB", null);
			*/
			Object[] ret = dataLayer.executeCall(conn, "DESCRIBE_SQL", new Object[] {nativeSQL});
			List<SQLType> sqlTypes = new ArrayList<SQLType>();
			int colCount = ConvertUtils.getInt(ret[2]);
			if(colCount == -1)
				return null;
			Results results = (Results)ret[1];
			while(results.next()) {
				SQLType sqlType = new SQLType();
				String typeName = typeNames.get(results.getValue("typeCode",int.class));
				sqlType.setTypeName(typeName);
				sqlType.setTypeCode(getTypeCode(typeName));
				sqlType.setPrecision(results.getValue("precision", Integer.class));
				sqlType.setScale(results.getValue("scale", Integer.class));
				sqlTypes.add(sqlType);
			}
			return sqlTypes.toArray(new SQLType[sqlTypes.size()]);
		} catch(DataLayerException e) {
			SQLException sqle = new SQLException("Datalayer Exception");
			sqle.initCause(e);
			throw sqle;
		} catch(ConvertException e) {
			SQLException sqle = new SQLException("Convert Exception");
			sqle.initCause(e);
			throw sqle;
		}
	}

	@Override
	protected ParameterMetaDataSupport getParameterMetaDataSupport() {
		return ParameterMetaDataSupport.COUNT_ONLY_SUPPORT;
	}
	/** Handle case when sqlType.getTypeCode() is Types.OTHER
	 * @see simple.db.specific.AbstractSpecific#getNormalizedType(simple.sql.SQLType)
	 */
	@Override
	public SQLType getNormalizedType(SQLType sqlType) {
		if(sqlType.getTypeCode() == Types.OTHER)
			sqlType.setTypeCode(Types.VARCHAR);
		return super.getNormalizedType(sqlType);
	}
}
