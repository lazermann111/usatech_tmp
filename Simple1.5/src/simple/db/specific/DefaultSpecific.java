package simple.db.specific;

import java.sql.Connection;
import java.sql.SQLException;

public class DefaultSpecific extends AbstractSpecific {
	//protected static DataLayer dataLayer;

	public DefaultSpecific() {
	}

	public void initialize(Connection conn) throws SQLException {
		/*if(dataLayer == null) {
			dataLayer = new DataLayer();
			try {
				ConfigLoader.loadConfig("resource:/simple/db/specific/default-specific-data-layer.xml", dataLayer);
			} catch(ConfigException e) {
				throw new SQLException("Could not load data-layer config file: " + e.getMessage());
			} catch(IOException e) {
				throw new SQLException("Could not load data-layer config file: " + e.getMessage());
			} catch(LoadingException e) {
				throw new SQLException("Could not load data-layer config file: " + e.getMessage());
			}
		}*/
	}

	@Override
	protected ParameterMetaDataSupport getParameterMetaDataSupport() {
		return ParameterMetaDataSupport.FULL_SUPPORT;
	}
}
