package simple.db.specific;

import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import simple.db.Call;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.sql.ArraySQLType;
import simple.sql.Expression;
import simple.sql.SQLType;

public interface DbSpecific {
	public void initialize(Connection conn) throws SQLException;
	public int getTypeCode(String typeName) ;
	public ArraySQLType getArrayType(SQLType componentType) ;
	public SQLType getComponentType(SQLType arrayType) ;
	public String getNormalizedTypeName(SQLType sqlType) ;
	public SQLType getNormalizedType(SQLType sqlType) ;
	public Expression createAggregateExpression(Expression expression, Aggregate aggregate, SQLType sqlType) ;
	public Expression createParameterExpression(Expression expression, Aggregate aggregate, SQLType sqlType) ;
	public SQLType[] describeQuery(Connection conn, String sql) throws SQLException;
	public ResultSetMetaData getQueryMetaData(Connection conn, String sql) throws SQLException;
	public void verifyCall(Call call, Connection conn) throws SQLException ;
	public Results getIndexes(Connection conn, String schemaName, String tableName, boolean unique) throws SQLException;
}
