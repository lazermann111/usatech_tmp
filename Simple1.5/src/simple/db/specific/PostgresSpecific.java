package simple.db.specific;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import simple.db.helpers.PostgresHelper;
import simple.results.Results.Aggregate;
import simple.sql.ArraySQLType;
import simple.sql.Expression;
import simple.sql.SQLType;
import simple.sql.WrappingExpression;

public class PostgresSpecific extends AbstractSpecific {
	public PostgresSpecific() {
	}

	public void initialize(Connection conn) throws SQLException {
	}

	@Override
	protected ParameterMetaDataSupport getParameterMetaDataSupport() {
		return ParameterMetaDataSupport.FULL_SUPPORT;
	}

	@Override
	public Expression createAggregateExpression(Expression expression, Aggregate aggregate, SQLType sqlType) {
		if(aggregate == null)
			return expression;
		switch(aggregate) {
			case ARRAY:
				return new WrappingExpression("ARRAY_AGG(", expression, ")");
			case SET:
			case DISTINCT:
				return new WrappingExpression("ARRAY_AGG(DISTINCT ", expression, ")");
			case FIRST:
				return new WrappingExpression("FIRST_VALUE(", expression, ")");
			case LAST:
				return new WrappingExpression("LAST_VALUE(", expression, ")");
			default:
				return new WrappingExpression(aggregate.toString() + '(', expression, ")");
		}
	}

	@Override
	public String getNormalizedTypeName(SQLType sqlType) {
		return PostgresHelper.getPGTypeName(sqlType, null);
	}

	@Override
	public SQLType getNormalizedType(SQLType sqlType) {
		sqlType = super.getNormalizedType(sqlType);
		SQLType componentSQLType;
		if(sqlType instanceof ArraySQLType && (componentSQLType = ((ArraySQLType) sqlType).getComponentType()).getTypeCode() == Types.CHAR)
			componentSQLType.setTypeCode(Types.VARCHAR);
		return sqlType;
	}
}
