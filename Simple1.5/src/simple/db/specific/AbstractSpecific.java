package simple.db.specific;

import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Types;

import simple.db.Argument;
import simple.db.Call;
import simple.db.Cursor;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.sql.ArraySQLType;
import simple.sql.Expression;
import simple.sql.SQLType;
import simple.sql.SQLTypeUtils;
import simple.sql.WrappingExpression;

public abstract class AbstractSpecific implements DbSpecific {
	protected static enum ParameterMetaDataSupport { NO_SUPPORT, COUNT_ONLY_SUPPORT, COUNT_MODE_SUPPORT, FULL_SUPPORT };

	public AbstractSpecific() {
	}

	public SQLType[] describeQuery(Connection conn, String sql) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(sql);
		try {
			return describeQuery(ps);
		} finally {
			ps.close();
		}
	}

	@Override
	public ResultSetMetaData getQueryMetaData(Connection conn, String sql) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(sql);
		try {
			try {
				return ps.getMetaData();
			} catch(SQLFeatureNotSupportedException e) {
				return null;
			}
		} finally {
			ps.close();
		}
	}
	protected SQLType[] describeQuery(PreparedStatement ps) throws SQLException {
		ResultSetMetaData rsmd;
		try {
			rsmd = ps.getMetaData();
		} catch(SQLFeatureNotSupportedException e1) {
			return null;
		}
		SQLType[] sqlTypes = new SQLType[rsmd.getColumnCount()];
		for(int i = 0; i < sqlTypes.length; i++) {
			sqlTypes[i] = new SQLType();
			int typeCode = rsmd.getColumnType(i+1);
			sqlTypes[i].setTypeCode(typeCode);
			sqlTypes[i].setPrecision(rsmd.getPrecision(i+1));
			sqlTypes[i].setScale(rsmd.getScale(i+1));
			sqlTypes[i].setTypeName(getNormalizedTypeName(sqlTypes[i]));
		}
		return sqlTypes;
	}

	public Results getIndexes(Connection conn, String schemaName, String tableName, boolean unique)
			throws SQLException {
		return DataLayerMgr.createResults(conn, conn.getMetaData().getIndexInfo(null, schemaName, tableName, unique, false), false);
	}
	public Expression createAggregateExpression(Expression expression, Aggregate aggregate, SQLType sqlType) {
		if(aggregate == null)
			return expression;
		else {
			return new WrappingExpression(aggregate.toString() + '(', expression, ")");
		}
	}
	public Expression createParameterExpression(Expression expression, Aggregate aggregate, SQLType sqlType) {
		return expression;
	}
	public ArraySQLType getArrayType(SQLType componentType) {
		if(componentType.getTypeCode() == Types.ARRAY) {
			SQLType innerType = getComponentType(componentType);
			componentType = getArrayType(innerType);
		}
		fillInTypeName(componentType);
		return new ArraySQLType(componentType.getTypeName() + "[]", componentType);
	}

	public SQLType getComponentType(SQLType arrayType) {
		if(arrayType instanceof ArraySQLType)
			return ((ArraySQLType)arrayType).getComponentType();
		fillInTypeName(arrayType);
		int pos = arrayType.getTypeName().indexOf("[]");
		if(pos < 0)
			return null;
		String typeName = arrayType.getTypeName().substring(0, pos);
		SQLType componentType = new SQLType(getTypeCode(typeName), typeName);
		for(pos = pos + 2; pos < arrayType.getTypeName().length(); pos += 2) {
			if(!arrayType.getTypeName().regionMatches(pos, "[]", 0, 2))
				return null;
			componentType = getArrayType(componentType);
		}
		return componentType;
	}

	protected void fillInTypeName(SQLType type) {
		if(type.getTypeName() == null || type.getTypeName().trim().length() == 0) {
			String typeName = SQLTypeUtils.getTypeCodeName(type.getTypeCode());
			type.setTypeName(typeName == null ? "UNKNOWN" : typeName);
		}
	}

	public int getTypeCode(String typeName) {
		if(typeName.endsWith("[]"))
			return Types.ARRAY;
		try {
			return SQLTypeUtils.getTypeCode(typeName);
		} catch(NoSuchFieldException e) {
			return 0;
		}
	}

	public String getNormalizedTypeName(SQLType sqlType) {
		if(sqlType.getTypeCode() == Types.ARRAY) {
			SQLType compType = getComponentType(sqlType);
			if(compType == null) compType = new SQLType(Types.VARCHAR); //use varchar as default as it is most flexible
			SQLType arrayType = getArrayType(compType);
			if(arrayType == null) return sqlType.getTypeName();
			return arrayType.getTypeName();
		}
		String typeName = SQLTypeUtils.getTypeCodeName(sqlType.getTypeCode());
		return typeName != null ? typeName : sqlType.getTypeName();
	}

	public SQLType getNormalizedType(SQLType sqlType) {
		SQLType normal = sqlType.clone();
		normal.setTypeName(getNormalizedTypeName(sqlType));
        return normal;
	}

	public void verifyCall(Call call, Connection conn) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(call.getSql());
		try {
			SQLType[] columnTypes = describeQuery(ps);
			Argument[] args = call.getArguments();
			verifyColumns(columnTypes, args);
			verifyParameters(ps.getParameterMetaData(), args);
		} finally {
			ps.close();
		}
	}

	protected void verifyColumns(SQLType[] columnTypes, Argument[] args) throws SQLException {
		if(columnTypes != null) {
			int columnCount;
			if(args[0] instanceof Cursor) {
				columnCount = ((Cursor)args[0]).getColumns().length;
			} else {
				columnCount = 0;
			}
			if(columnTypes.length != columnCount) {
				throw new SQLException("Wrong number of columns, expected " + columnTypes.length + " but " + columnCount + " were configured");
			}
		}
	}
	protected void verifyParameters(ParameterMetaData pmd, Argument[] args) throws SQLException {
		ParameterMetaDataSupport support = getParameterMetaDataSupport();
		if(support != null && support != ParameterMetaDataSupport.NO_SUPPORT) {
			if(pmd != null) {
				if(pmd.getParameterCount() != args.length - 1)
					throw new SQLException("Wrong number of parameters, expected " + pmd.getParameterCount() + " but " + (args.length - 1) + " were configured");
				if(support != ParameterMetaDataSupport.COUNT_ONLY_SUPPORT) {
					for(int i = 1; i <= pmd.getParameterCount(); i++) {
						if(!parameterModeMatches(pmd.getParameterMode(i), args[i].isIn(), args[i].isOut()))
							throw new SQLException("Parameter " + i + " should be " + getParameterModeString(pmd.getParameterMode(i)) + " but is configured as " + getParameterModeString(args[i].isIn(), args[i].isOut()));
						if(support == ParameterMetaDataSupport.FULL_SUPPORT && !isParameterTypeCompatible(pmd.getParameterType(i),args[i].getSqlType().getTypeCode()))
							throw new SQLException("Parameter " + i + " should be " + pmd.getParameterTypeName(i) + " but is configured as " + args[i].getSqlType().getTypeName());
					}
				}
			}
		}
	}
	protected boolean isParameterTypeCompatible(int parameterType, int typeCode) {
		Class<?> c1 = SQLTypeUtils.getJdbcType(parameterType);
		Class<?> c2 = SQLTypeUtils.getJdbcType(typeCode);
		return (c1 == null || c2 == null || c1.equals(c2));
	}

	protected String getParameterModeString(boolean in, boolean out) {
		return (in ? "IN" : "") + (out ? "OUT" : "");
	}

	protected String getParameterModeString(int parameterMode) {
		switch(parameterMode) {
			case ParameterMetaData.parameterModeIn:
				return "IN";
			case ParameterMetaData.parameterModeInOut:
				return "INOUT";
			case ParameterMetaData.parameterModeOut:
				return "OUT";
			case ParameterMetaData.parameterModeUnknown:
				return "UNKNOWN";
			default:
				return "UNDEFINED";
		}
	}

	protected boolean parameterModeMatches(int parameterMode, boolean in, boolean out) {
		switch(parameterMode) {
			case ParameterMetaData.parameterModeIn:
				return in && !out;
			case ParameterMetaData.parameterModeInOut:
				return in && out;
			case ParameterMetaData.parameterModeOut:
				return !in && out;
			case ParameterMetaData.parameterModeUnknown:
				return true;
			default:
				return false;
		}
	}

	protected abstract ParameterMetaDataSupport getParameterMetaDataSupport() ;
}
