package simple.ant;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.tools.ant.BuildException;

import difflib.DiffUtils;
import difflib.Patch;

public class DiffTask extends org.apache.tools.ant.Task {
	protected File patchFile;
	protected File originalDirectory;
	protected File revisedDirectory;
	protected int contextSize = 3;
    protected static enum ChangeType { ADD, REMOVE, UPDATE };
	public File getPatchFile() {
		return patchFile;
	}

	public void setPatchFile(File patchFile) {
		this.patchFile = patchFile;
	}

	public File getOriginalDirectory() {
		return originalDirectory;
	}

	public void setOriginalDirectory(File originalDirectory) {
		this.originalDirectory = originalDirectory;
	}

	public File getRevisedDirectory() {
		return revisedDirectory;
	}

	public void setRevisedDirectory(File revisedDirectory) {
		this.revisedDirectory = revisedDirectory;
	}

	public int getContextSize() {
		return contextSize;
	}

	public void setContextSize(int contextSize) {
		this.contextSize = contextSize;
	}

	public static List<String> fileToLines(Reader input) throws IOException {
		List<String> lines = new LinkedList<String>();
		String line;
		BufferedReader in = new BufferedReader(input);
		while((line = in.readLine()) != null) {
			lines.add(line);
		}
		return lines;
	}
	public static List<String> fileToLines(String filename) throws IOException {
		return fileToLines(new FileReader(filename));
	}
	public static List<String> fileToLines(File file) throws IOException {
		return fileToLines(new FileReader(file));
	}
	public static void linesToFile(List<?> lines, Writer output) throws IOException {
		BufferedWriter out = new BufferedWriter(output);		
		for(Object line : lines) {
			if(line != null)
				out.write(line.toString());
			out.newLine();
		}
		out.flush();
	}
	public static void linesToFile(List<?> lines, File file) throws IOException {
		linesToFile(lines, new FileWriter(file));
	}
	public static void linesToFile(List<?> lines, String filename) throws IOException {
		linesToFile(lines, new FileWriter(filename));
	}
	
	protected void writeDiff(final String path, final int prefixLength, final File original, final File revised, final int contextSize, final Writer output) throws IOException {
		if(revised.isDirectory()) {
			log("Searching for differences in directories '" + original.getCanonicalPath() + "' and '" + revised.getCanonicalPath() + "'");						
			String newPath;
			if(prefixLength > 0) {
				newPath = null;
			} else if(path == null || path.isEmpty())
				newPath = revised.getName() + '/' ;
			else
				newPath = path + revised.getName() + '/' ;
			SortedMap<File, ChangeType> subs = new TreeMap<File, ChangeType>();
			for(String filename : revised.list()) {
				subs.put(new File(filename), ChangeType.ADD);
			}
			for(String filename : original.list()) {
				File file = new File(filename);
				ChangeType ct = subs.get(file);
				subs.put(file, ct == ChangeType.ADD ? ChangeType.UPDATE : ChangeType.REMOVE);
			}
			for(Map.Entry<File, ChangeType> entry : subs.entrySet()) {
				switch(entry.getValue()) {
					case UPDATE:
						writeDiff(newPath, prefixLength - 1, new File(original, entry.getKey().getName()), new File(revised, entry.getKey().getName()), contextSize, output);
						break;
					case ADD:
						//Ignore for now
						break;
					case REMOVE:
						//Ignore for now
						break;
				}
			}
		} else {
			log("Diffing files '" + original.getCanonicalPath() + "' and '" + revised.getCanonicalPath() + "'");			
			List<String> originalLines = fileToLines(original);
			Patch patch = DiffUtils.diff(originalLines, fileToLines(revised));
			if(!patch.getDeltas().isEmpty()) {
				List<String> diff = DiffUtils.generateUnifiedDiff(path + original.getName(), path + revised.getName(), originalLines, patch, contextSize);
				linesToFile(diff, output);
			}
			log("Found " + patch.getDeltas().size() + " differences between files '" + original.getCanonicalPath() + "' and '" + revised.getCanonicalPath() + "'");			
		}
	}
	
	public static void main(String[] args) throws BuildException {
		DiffTask task = new DiffTask();
		task.setPatchFile(new File(args[0]));
		task.setOriginalDirectory(new File(args[1]));	
		task.setRevisedDirectory(new File(args[2]));	
		task.execute();
	}

	public void execute() throws BuildException {
		try {
			FileWriter writer = new FileWriter(patchFile);
			writeDiff(null, 1, getOriginalDirectory(), getRevisedDirectory(), getContextSize(), writer);
			writer.close();
		} catch(IOException e) {
			throw new BuildException(e);
		}
	}
}
