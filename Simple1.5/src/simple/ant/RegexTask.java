/*
 * RegexTask.java
 *
 * Created on December 16, 2003, 10:59 AM
 */

package simple.ant;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.RegularExpression;
import org.apache.tools.ant.types.Substitution;
import org.apache.tools.ant.util.FileUtils;
import org.apache.tools.ant.util.regexp.Regexp;

/**
 *
 * @author  Brian S. Krug
 */
public class RegexTask extends org.apache.tools.ant.Task {
    // unfortunately org.apache.tools.ant.taskdefs.optional.ReplaceRegExp uses private fields so we must copy its behavior
    protected File file;
    protected String flags;
    protected boolean byline;
    protected Vector<FileSet> filesets;// Keep jdk 1.1 compliant so others can use this
    protected RegularExpression regex;
    protected Substitution subs;

    protected FileUtils fileUtils = FileUtils.newFileUtils();

    /** Holds value of property input. */
    protected String input;    

    /** Holds value of property addproperty. */
    protected String addproperty;
    
    /** Default Constructor  */
    public RegexTask() {
        super();
        this.file = null;
        this.filesets = new Vector<FileSet>();
        this.flags = "";
        this.byline = false;

        this.regex = null;
        this.subs = null;
    }


    /**
     * file for which the regular expression should be replaced;
     * required unless a nested fileset is supplied.
     */
    public void setFile(File file) {
        this.file = file;
    }


    /**
     * the regular expression pattern to match in the file(s);
     * required if no nested &lt;regexp&gt; is used
     */
    public void setMatch(String match) {
        if (regex != null) {
            throw new BuildException("Only one regular expression is allowed");
        }

        regex = new RegularExpression();
        regex.setPattern(match);
    }


    /**
     * The substitution pattern to place in the file(s) in place
     * of the regular expression.
     * Required if no nested &lt;substitution&gt; is used
     */
                     
    public void setReplace(String replace) {
        if (subs != null) {
            throw new BuildException("Only one substitution expression is "
                                     + "allowed");
        }

        subs = new Substitution();
        subs.setExpression(replace);
    }

    /**
     * The flags to use when matching the regular expression.  For more
     * information, consult the Perl5 syntax.
     * <ul>
     *  <li>g : Global replacement.  Replace all occurences found
     *  <li>i : Case Insensitive.  Do not consider case in the match
     *  <li>m : Multiline.  Treat the string as multiple lines of input, 
     *         using "^" and "$" as the start or end of any line, respectively, rather than start or end of string.
     *  <li> s : Singleline.  Treat the string as a single line of input, using
     *        "." to match any character, including a newline, which normally, it would not match.
     *</ul>
     */                     
    public void setFlags(String flags) {
        this.flags = flags;
    }


    /**
     * Process the file(s) one line at a time, executing the replacement
     * on one line at a time.  This is useful if you
     * want to only replace the first occurence of a regular expression on
     * each line, which is not easy to do when processing the file as a whole.
     * Defaults to <i>false</i>.</td>
     */
    public void setByLine(String byline) {
        Boolean res = Boolean.valueOf(byline);

        if (res == null) {
            res = Boolean.FALSE;
        }
        this.byline = res.booleanValue();
    }


    /**
     * list files to apply the replacement to
     */
    public void addFileset(FileSet set) {
        filesets.addElement(set);
    }


    /**
     * A regular expression.
     * You can use this element to refer to a previously
     * defined regular expression datatype instance
     */
    public RegularExpression createRegexp() {
        if (regex != null) {
            throw new BuildException("Only one regular expression is allowed.");
        }

        regex = new RegularExpression();
        return regex;
    }


    /**
     * A substitution pattern.  You can use this element to refer to a previously
     * defined substitution pattern datatype instance.
     */
    public Substitution createSubstitution() {
        if (subs != null) {
            throw new BuildException("Only one substitution expression is "
                                     + "allowed");
        }

        subs = new Substitution();
        return subs;
    }


    protected String doReplace(RegularExpression r,
                               Substitution s,
                               String input,
                               int options) {
        String res = input;
        Regexp regexp = r.getRegexp(getProject());

        if (regexp.matches(input, options)) {
            res = regexp.substitute(input, s.getExpression(getProject()), 
                                    options);
        }

        return res;
    }


    /** Perform the replace on the entire file  */
    protected void doReplace(File f, int options)
         throws IOException {
        File parentDir = fileUtils.getParentFile(f);
        File temp = fileUtils.createTempFile("replace", ".txt", parentDir);

        FileReader r = null;
        FileWriter w = null;

        try {
            r = new FileReader(f);
            w = new FileWriter(temp);

            BufferedReader br = new BufferedReader(r);
            BufferedWriter bw = new BufferedWriter(w);
            PrintWriter pw = new PrintWriter(bw);

            boolean changes = false;

            log("Replacing pattern '" + regex.getPattern(getProject()) +
                "' with '" + subs.getExpression(getProject()) +
                "' in '" + f.getPath() + "'" +
                (byline ? " by line" : "") +
                (flags.length() > 0 ? " with flags: '" + flags + "'" : "") +
                ".",
                Project.MSG_VERBOSE);

            if (byline) {
                StringBuilder linebuf = new StringBuilder();
                String line = null;
                String res = null;
                int c;
                boolean hasCR = false;

                do {
                    c = br.read();

                    if (c == '\r') {
                        if (hasCR) {
                            // second CR -> EOL + possibly empty line
                            line = linebuf.toString();
                            res  = doReplace(regex, subs, line, options);

                            if (!res.equals(line)) {
                                changes = true;
                            }

                            pw.print(res);
                            pw.print('\r');

                            linebuf.setLength(0);
                            // hasCR is still true (for the second one)
                        } else {
                            // first CR in this line
                            hasCR = true;
                        }
                    }
                    else if (c == '\n') {
                        // LF -> EOL
                        line = linebuf.toString();
                        res  = doReplace(regex, subs, line, options);

                        if (!res.equals(line)) {
                            changes = true;
                        }

                        pw.print(res);
                        if (hasCR) {
                            pw.print('\r');
                            hasCR = false;
                        }
                        pw.print('\n');

                        linebuf.setLength(0);
                    } else { // any other char
                        if ((hasCR) || (c < 0)) {
                            // Mac-style linebreak or EOF (or both)
                            line = linebuf.toString();
                            res  = doReplace(regex, subs, line, options);

                            if (!res.equals(line)) {
                                changes = true;
                            }

                            pw.print(res);
                            if (hasCR) {
                                pw.print('\r');
                                hasCR = false;
                            }

                            linebuf.setLength(0);
                        }

                        if (c >= 0) {
                            linebuf.append((char) c);
                        }
                    }
                } while (c >= 0);

                pw.flush();
            } else {
                int flen = (int) f.length();
                char tmpBuf[] = new char[flen];
                int numread = 0;
                int totread = 0;

                while (numread != -1 && totread < flen) {
                    numread = br.read(tmpBuf, totread, flen);
                    totread += numread;
                }

                String buf = new String(tmpBuf);

                String res = doReplace(regex, subs, buf, options);

                if (!res.equals(buf)) {
                    changes = true;
                }

                pw.print(res);
                pw.flush();
            }

            r.close();
            r = null;
            w.close();
            w = null;

            if (changes) {
                if (!f.delete()) {
                    throw new BuildException("Couldn't delete " + f,
                                             getLocation());
                }
                if (!temp.renameTo(f)) {
                    throw new BuildException("Couldn't rename temporary file " 
                                             + temp, getLocation());
                }
                temp = null;
            }
        } finally {
            try {
                if (r != null) {
                    r.close();
                }
            } catch (Exception e) {
            }

            try {
                if (w != null) {
                    w.close();
                }
            } catch (Exception e) {
            }
            if (temp != null) {
                temp.delete();
            }
        }
    }


    public void execute()
         throws BuildException {
        if (regex == null) {
            throw new BuildException("No expression to match.");
        }
        if (subs == null) {
            throw new BuildException("Nothing to replace expression with.");
        }

        if (file != null && filesets.size() > 0) {
            throw new BuildException("You cannot supply the 'file' attribute "
                                     + "and filesets at the same time.");
        }

        int options = 0;

        if (flags.indexOf('g') != -1) {
            options |= Regexp.REPLACE_ALL;
        }

        if (flags.indexOf('i') != -1) {
            options |= Regexp.MATCH_CASE_INSENSITIVE;
        }

        if (flags.indexOf('m') != -1) {
            options |= Regexp.MATCH_MULTILINE;
        }

        if (flags.indexOf('s') != -1) {
            options |= Regexp.MATCH_SINGLELINE;
        }

        if (file != null && file.exists()) {
            try {
                doReplace(file, options);
            } catch (IOException e) {
                log("An error occurred processing file: '" 
                    + file.getAbsolutePath() + "': " + e.toString(),
                    Project.MSG_ERR);
            }
        } else if (file != null) {
            log("The following file is missing: '" 
                + file.getAbsolutePath() + "'", Project.MSG_ERR);
        }

        int sz = filesets.size();

        for (int i = 0; i < sz; i++) {
            FileSet fs = filesets.elementAt(i);
            DirectoryScanner ds = fs.getDirectoryScanner(getProject());

            String files[] = ds.getIncludedFiles();

            for (int j = 0; j < files.length; j++) {
                File f = new File(fs.getDir(getProject()), files[j]);

                if (f.exists()) {
                    try {
                        doReplace(f, options);
                    } catch (Exception e) {
                        log("An error occurred processing file: '" 
                            + f.getAbsolutePath() + "': " + e.toString(),
                            Project.MSG_ERR);
                    }
                } else {
                    log("The following file is missing: '" 
                        + f.getAbsolutePath() + "'", Project.MSG_ERR);
                }
            }
        }
        if(input != null) {
            String result = doReplace(regex, subs, input, options);
            if (addproperty != null && result != null) {
                getProject().setNewProperty(addproperty, result);
            }
        }
    }
    
    /** Setter for property input.
     * @param original New value of property input.
     *
     */
    public void setInput(String input) {
        this.input = input;
    }
    
    /** Setter for property addproperty.
     * @param addproperty New value of property addproperty.
     *
     */
    public void setAddproperty(String addproperty) {
        this.addproperty = addproperty;
    }
    
}
