/*
 * IterateTask.java
 *
 * Created on December 16, 2003, 11:39 AM
 */

package simple.ant;

import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.BuildListener;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;

/**
 *
 * @author  Brian S. Krug
 */
public class IterateTask extends org.apache.tools.ant.Task {
    
    /** Holds value of property list. */
    protected String list;
    
    protected Vector<FileSet> filesets;
    
    /** Holds value of property files. */
    protected String files;
    
    /** Holds value of property separators. */
    protected String separators;
    
    /** Holds value of property property. */
    protected String property;
    
    /** Holds value of property index. */
    protected String index;
    
    /** Holds value of property target. */
    protected String target;
    
    protected BuildListener taskReconfigurer = new BuildListener() {
        public void buildFinished(org.apache.tools.ant.BuildEvent event) {}        
        public void buildStarted(org.apache.tools.ant.BuildEvent event) {}        
        public void messageLogged(org.apache.tools.ant.BuildEvent event) {}        
        public void targetFinished(org.apache.tools.ant.BuildEvent event) {}        
        public void targetStarted(org.apache.tools.ant.BuildEvent event) {}        
        public void taskFinished(org.apache.tools.ant.BuildEvent event) {}        
        public void taskStarted(org.apache.tools.ant.BuildEvent event) {
        	getProject().log("Maybe Reconfigure Task " + event.getTask() + " using " + event.getTask().getRuntimeConfigurableWrapper() + " with attributes = " + event.getTask().getRuntimeConfigurableWrapper().getAttributes());
            event.getTask().maybeConfigure(); //this doesn't seem to work - try something else
            org.apache.tools.ant.ProjectHelper.configure(event.getTask(), event.getTask().getRuntimeConfigurableWrapper().getAttributes(), project);
        }        
    };
    
    /** Creates a new instance of IterateTask */
    public IterateTask() {
        super();
    }
    
    /** Setter for property list.
     * @param list New value of property list.
     *
     */
    public void setList(String list) {
        this.list = list;
    }
    
    public void execute() throws BuildException {
        // process props
        if(separators == null || separators.length() == 0) separators = ",;";
        if(list != null && list.length() > 0) {
            //must reconfigure each task in target
        	Project project = getProject();
            project.addBuildListener(taskReconfigurer);
            try {
                StringTokenizer tok = new StringTokenizer(list, separators);
                int i = 0;
                while(tok.hasMoreTokens()) {
                    String value = tok.nextToken();
                    if(property != null) project.setProperty(property, value);
                    if(index != null) project.setProperty(index, "" + (i++));
                    project.log("Running '" + target + "' with property '" + property + "' = '" + value + "'"); 
                    project.log("Properties= '" + project.getProperties());               
                    processSubTasks();
                }
            } finally { //ensure listener is removed
                project.removeBuildListener(taskReconfigurer);  
            }
        }        
    }

    protected void processSubTasks() throws BuildException {
        // because we want to use filsets nested elements we can't be a TaskContainer so we call the specified target
    	getProject().executeTarget(target);
    }
    
    /** list files to apply the replacement to
     *
     */
    public void addFileset(FileSet set) {
        filesets.addElement(set);
    }
    
    /** Setter for property files.
     * @param files New value of property files.
     *
     */
    public void setFiles(String files) {
        this.files = files;
    }
    
    /** Setter for property separators.
     * @param separators New value of property separators.
     *
     */
    public void setSeparators(String separators) {
        this.separators = separators;
    }
    
    /** Setter for property property.
     * @param property New value of property property.
     *
     */
    public void setProperty(String property) {
        this.property = property;
    }
    
    /** Setter for property index.
     * @param index New value of property index.
     *
     */
    public void setIndex(String index) {
        this.index = index;
    }
    
    /** Setter for property target.
     * @param target New value of property target.
     *
     */
    public void setTarget(String target) {
        this.target = target;
    }
    
}
