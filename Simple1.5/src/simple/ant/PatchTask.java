package simple.ant;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tools.ant.BuildException;

import difflib.DiffUtils;
import difflib.Patch;
import difflib.PatchFailedException;

public class PatchTask extends org.apache.tools.ant.Task {
	protected static class PatchEntry {
		protected final String filepath;
		protected final Patch patch;
		
		public PatchEntry(String filepath, Patch patch) {
			this.filepath = filepath;
			this.patch = patch;
		}
		public String getFilepath() {
			return filepath;
		}
		public Patch getPatch() {
			return patch;
		}		
	}
	protected File patchFile;
	protected File baseDirectory;
    
	public File getPatchFile() {
		return patchFile;
	}

	public void setPatchFile(File patchFile) {
		this.patchFile = patchFile;
	}

	public File getBaseDirectory() {
		return baseDirectory;
	}

	public void setBaseDirectory(File baseDirectory) {
		this.baseDirectory = baseDirectory;
	}

	public static List<String> fileToLines(Reader input) throws IOException {
		List<String> lines = new LinkedList<String>();
		String line;
		BufferedReader in = new BufferedReader(input);
		while((line = in.readLine()) != null) {
			lines.add(line);
		}
		return lines;
	}
	public static List<String> fileToLines(String filename) throws IOException {
		return fileToLines(new FileReader(filename));
	}
	public static List<String> fileToLines(File file) throws IOException {
		return fileToLines(new FileReader(file));
	}
	public static void linesToFile(List<?> lines, Writer output) throws IOException {
		BufferedWriter out = new BufferedWriter(output);		
		for(Object line : lines) {
			if(line != null)
				out.write(line.toString());
			out.newLine();
		}
		out.flush();
	}
	public static void linesToFile(List<?> lines, File file) throws IOException {
		linesToFile(lines, new FileWriter(file));
	}
	public static void linesToFile(List<?> lines, String filename) throws IOException {
		linesToFile(lines, new FileWriter(filename));
	}
	protected static final Pattern originalFilePattern = Pattern.compile("--- ([^\t]+)(?:\t.*)?");
	protected static final Pattern revisedFilePattern = Pattern.compile("\\+{3} ([^\t]+)(?:\t.*)?");
	
	public static List<PatchEntry> parseDiffFile(Reader input) throws IOException {
		final List<PatchEntry> patchEntries = new ArrayList<PatchEntry>();
		final List<String> lines = new ArrayList<String>();
		String line;
		String revisedFile = null;
		String origFile = null;
		int i = 0;
		BufferedReader in = new BufferedReader(input);
		while((line = in.readLine()) != null) {
			i++;
			if(revisedFile == null) {
				Matcher matcher = revisedFilePattern.matcher(line);
				if(matcher.matches()) {
					revisedFile = matcher.group(1);
					if(!revisedFile.equals(origFile))
						throw new IOException("Invalid patch file at line " + i + ": original file '" + origFile + "' does not match revised file '" + revisedFile + "'");				
				} else {
					matcher = originalFilePattern.matcher(line);
					if(matcher.matches()) {
						origFile = matcher.group(1);
					}
				}
			} else {
				Matcher matcher = originalFilePattern.matcher(line);
				if(matcher.matches()) {
					patchEntries.add(new PatchEntry(revisedFile, DiffUtils.parseUnifiedDiff(lines)));
					lines.clear();
					origFile = matcher.group(1);
					revisedFile = null;
				}
			}
			lines.add(line);
		}
		if(revisedFile != null)
			patchEntries.add(new PatchEntry(revisedFile, DiffUtils.parseUnifiedDiff(lines)));
		return patchEntries;
	}
	public static List<PatchEntry> parseDiffFile(String filename) throws IOException {
		return parseDiffFile(new FileReader(filename));
	}
	public static List<PatchEntry> parseDiffFile(File file) throws IOException {
		return parseDiffFile(new FileReader(file));
	}
	
	public static void main(String[] args) throws BuildException {
		PatchTask task = new PatchTask();
		task.setPatchFile(new File(args[0]));
		task.setBaseDirectory(new File(args[1]));	
		task.execute();
	}

	public void execute() throws BuildException {
		try {
			List<PatchEntry> patchEntries = parseDiffFile(patchFile);
			for(PatchEntry entry : patchEntries) {
				File original = new File(baseDirectory, entry.getFilepath());
				if(!original.exists()) {
					throw new IOException("File '" + original.getCanonicalPath() + "' does not exist");
				} else if(!original.canRead()) {
					throw new IOException("File '" + original.getCanonicalPath() + "' cannot be read from");
				} else if(!original.canWrite()) {
					throw new IOException("File '" + original.getCanonicalPath() + "' cannot be written to");
				}
				log("Patching file '" + original.getCanonicalPath() + "' with " + entry.getPatch().getDeltas().size() + " differences");
				List<?> result = entry.getPatch().applyTo(fileToLines(original));
				linesToFile(result, original);
				log("Patched file '" + original.getCanonicalPath() + "'"); 
			}
		} catch(IOException e) {
			throw new BuildException(e);
		} catch(PatchFailedException e) {
			throw new BuildException(e);
		}
	}
}
