package simple.util;

import java.util.Arrays;

import simple.io.IOUtils;

public class Bitmap {
	protected byte[] bytes;
	
	public void setBit(int index, boolean on) {
		int byteIndex = index / 8;
		int bitMask = (1 << (7-(index % 8)));
		if(bytes == null) {
			if(!on)
				return;
			bytes = new byte[byteIndex+1];
		} else if(byteIndex >= bytes.length) {
			if(!on)
				return;
			byte[] tmp = new byte[byteIndex+1];
			System.arraycopy(bytes, 0, tmp, 0, bytes.length);
			bytes = tmp;
		}
		if(on)
			bytes[byteIndex] = (byte)(bytes[byteIndex] | bitMask);
		else
			bytes[byteIndex] = (byte)(bytes[byteIndex] & ~bitMask);
	}
	
	public boolean getBit(int index) {
		if(bytes == null)
			return false;
		int byteIndex = index / 8;
		if(byteIndex >= bytes.length)
			return false;
		int bitMask = (1 << (7-(index % 8)));
		return (bytes[byteIndex] & bitMask) == bitMask;
	}
	
	public byte getByte(int byteIndex) {
		if(bytes == null || byteIndex >= bytes.length)
			return 0;
		else
			return bytes[byteIndex];
	}
	
	public void setByte(int byteIndex, byte value) {
		if(bytes == null) {
			bytes = new byte[byteIndex+1];
		} else if(byteIndex >= bytes.length) {
			byte[] tmp = new byte[byteIndex+1];
			System.arraycopy(bytes, 0, tmp, 0, bytes.length);
		}
		bytes[byteIndex] = value;
	}
	
	public byte[] getBytes() {
		if(bytes == null)
			return IOUtils.EMPTY_BYTES;
		else
			return bytes.clone();
	}
	
	public void setBytes(byte[] bytes) {
		this.bytes = bytes.clone();
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Bitmap && Arrays.equals(bytes, ((Bitmap)obj).bytes);
	}
	
	@Override
	public int hashCode() {
		return bytes == null ? 0 : Arrays.hashCode(bytes);
	}
	
	@Override
	public String toString() {
		if(bytes == null)
			return "";
		else {
			char[] ch = new char[bytes.length * 8];
			for(int i = 0; i < bytes.length; i++) {
				for(int b = 0; b < 8; b++) {
					int bitMask = (1 << (7-b));
					ch[i * 8 + b] = ((bytes[i] & bitMask) == bitMask ? '1' : '0');
				}
			}
			return new String(ch);
		}
	}
}
