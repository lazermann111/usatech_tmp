/*
 * FilterMap.java
 *
 * Created on June 11, 2004, 12:22 PM
 */

package simple.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/** Filters out specified entries of the underlying map.
 *
 * @author  Brian S. Krug
 */
public class ExcludingMap<K,V> extends java.util.AbstractMap<K,V> {
    protected Map<K,V> map;
    protected Collection<K> exclude;
    protected Set<Map.Entry<K,V>> entrySet;

    /** Creates a new instance of ExcludingMap */
    public ExcludingMap(Map<K,V> delegate, Collection<K> exclude) {
        this.map = delegate;
        this.exclude = exclude;
    }

    @Override
	public V put(K key, V value) {
        return map.put(key, value);
    }

    @Override
	public V get(Object key) {
        if(exclude.contains(key)) return null;
        return map.get(key);
    }

    @Override
	public int size() {
        int size = map.size();
        for(Iterator<K> iter = exclude.iterator(); iter.hasNext();)
            if(map.containsKey(iter.next())) size--;
        return size;
    }

    @Override
	public void clear() {
        map.clear();
    }

    @Override
	public V remove(Object key) {
        return map.remove(key);
    }

    @Override
	public java.util.Set<Map.Entry<K,V>> entrySet() {
        if (entrySet == null) {
	    entrySet = new java.util.AbstractSet<Map.Entry<K,V>>() {
			@Override
			public Iterator<Map.Entry<K,V>> iterator() {
			    return new Iterator<Map.Entry<K,V>>() {
			        private final Iterator<Map.Entry<K,V>> i = map.entrySet().iterator();
	                private Map.Entry<K,V> next = null;
					public boolean hasNext() {
                        if(next != null) return true;
                        while(i.hasNext()) {
                            Map.Entry<K,V> e = i.next();
                            if(!exclude.contains(e.getKey())) {
                                next = e;
                                return true;
                            }
                        }
					    return false;
					}

					public Map.Entry<K,V> next() {
                        if(!hasNext()) throw new java.util.NoSuchElementException();
                        Map.Entry<K,V> o = new Map.Entry<K,V>() {
                            private final Map.Entry<K,V> e = next;
                            public K getKey() {
                                return e.getKey();
                            }
                            public V getValue() { return e.getValue(); }
                            public V setValue(V val) { return e.setValue(val); }
                            @Override
							public boolean equals(Object o) {
                                if (!(o instanceof Map.Entry)) return false;
                                Map.Entry<?,?> e1 = (Map.Entry<?,?>)o;
                                return eq(getKey(), e1.getKey()) &&  eq(getValue(), e1.getValue());
                            }
                            private boolean eq(Object o1, Object o2) {
                                return (o1 == null ? o2 == null : o1.equals(o2));
                            }
                            @Override
							public int hashCode() {
                                Object k = getKey();
                                Object v = getValue();
                                return (k == null ? 0 : k.hashCode()) ^ (v == null ? 0 : v.hashCode());
                            }
                        };
                        next = null;
                        return o;
					}

					public void remove() {
					    i.remove();
					}
			    };
			}

			@Override
			public int size() {
			    return ExcludingMap.this.size();
			}

			@Override
			public boolean contains(Object o) {
	                    if (!(o instanceof Map.Entry)) return false;
	                    Map.Entry<?,?> e = (Map.Entry<?,?>) o;
	                    Object val = ExcludingMap.this.get(e.getKey());
	                    if(val == null) return e.getValue() == null && ExcludingMap.this.containsKey(e.getKey());
	                    else return e.getValue() != null && val.equals(e.getValue());
			}
	    };
	}
	return entrySet;
    }

    @Override
	public boolean containsKey(java.lang.Object key) {
        return map.containsKey(key) && !exclude.contains(key);
    }
}
