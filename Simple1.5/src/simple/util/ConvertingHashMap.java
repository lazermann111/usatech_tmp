package simple.util;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import simple.bean.ConvertUtils;

public abstract class ConvertingHashMap<K,V> extends java.util.LinkedHashMap<K,V> {
	private static final long serialVersionUID = 3241141409L;
	protected Set<Entry<K, V>> entrySet;
	protected class EntrySet extends AbstractSet<Map.Entry<K,V>> {
        public Iterator<Map.Entry<K,V>> iterator() {
        	final Iterator<Map.Entry<K,V>> delegate = ConvertingHashMap.super.entrySet().iterator();
            return new Iterator<Map.Entry<K,V>>() {
				public boolean hasNext() {
					return delegate.hasNext();
				}
				public Map.Entry<K, V> next() {
					final Map.Entry<K, V> entry = delegate.next();
					return new Map.Entry<K, V>() {
						public K getKey() {
							return entry.getKey();
						}

						public V getValue() {
							return entry.getValue();
						}

						public V setValue(V value) {
							return entry.setValue(convertValue(value));
						}

						@Override
						public boolean equals(Object obj) {
							if (!(obj instanceof Map.Entry))
				                return false;
				            Map.Entry<K,V> e = asEntry(obj);
				            K oKey = convertKey(e.getKey());
				            V oVal = convertValue(oKey);
				            return ConvertUtils.areEqual(getKey(), oKey) && ConvertUtils.areEqual(getValue(), oVal);				            
						}

						@Override
						public int hashCode() {
							return entry.hashCode();
						}
					};
				}
				public void remove() {
					delegate.remove();
				}
            };
        }
        public boolean contains(Object o) {
            if (!(o instanceof Map.Entry))
                return false;
            Map.Entry<K,V> e = asEntry(o);
            K oKey = convertKey(e.getKey());
            V value = get(oKey);
            V oVal = convertValue(oKey);
            if(value == null) 
            	return oVal == null && containsKey(oKey);
            
            return oVal != null && value.equals(oVal);
        }
        public boolean remove(Object o) {
        	if(contains(o)) {
        		return remove(asEntry(o).getKey());
        	}
        	return false;
        }
        public int size() {
            return ConvertingHashMap.this.size();
        }
        public void clear() {
        	ConvertingHashMap.this.clear();
        }
        @SuppressWarnings("unchecked")
		protected Map.Entry<K, V> asEntry(Object o) {
        	return (Map.Entry<K,V>) o;
        }
    }
	public ConvertingHashMap() {
		super();
	}

	public ConvertingHashMap(int initialCapacity) {
		super(initialCapacity);
	}

	public ConvertingHashMap(Map<? extends K, ? extends V> m) {
		super(m);
	}

	public ConvertingHashMap(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	protected abstract K convertKey(Object key) ;
	protected abstract V convertValue(Object value) ;

	@Override
	public boolean containsKey(Object key) {
		return super.containsKey(convertKey(key));
	}

	@Override
	public boolean containsValue(Object value) {
		return super.containsValue(convertValue(value));
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		if(entrySet == null) {
			entrySet = new EntrySet();
		}
        return entrySet;
	}

	@Override
	public V get(Object key) {
		return super.get(convertKey(key));
	}

	@Override
	public V put(K key, V value) {
		return super.put(convertKey(key), convertValue(value));
	}

	@Override
	public V remove(Object key) {
		return super.remove(convertKey(key));
	}
	
	
}
