package simple.util;

/**
 * Indicates that a class provides property change notification
 * @author  Brian S. Krug
 */
public interface PropertyChanger {
	public void addPropertyChangeListener(java.beans.PropertyChangeListener listener);
	public void removePropertyChangeListener(java.beans.PropertyChangeListener listener);
}
