package simple.util;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import simple.bean.ReflectionUtils;

/** Presents a beans properties as a Map
 * @author bkrug
 *
 * @param <K>
 * @param <V>
 */
public class BeanPropertiesMap extends AbstractMap<String,Object> {
	protected final Object bean;
	protected final Map<String,PropertyDescriptor> propertiesMap;
	protected Set<Entry<String,Object>> entrySet;
	protected Collection<Object> values;
	
	public BeanPropertiesMap(Object bean) throws IntrospectionException {
		super();
		this.bean = bean;
		if(bean == null)
			this.propertiesMap = Collections.emptyMap();
		else {
			PropertyDescriptor[] pds = ReflectionUtils.getPropertyDescriptors(bean.getClass());
			Map<String, PropertyDescriptor> map = new TreeMap<String, PropertyDescriptor>(new Comparator<String>() {
				public int compare(String o1, String o2) {
					int i = o1.compareToIgnoreCase(o2);
					if(i == 0)
						return o1.compareTo(o2);
					return i;
				}
			});
			for(PropertyDescriptor pd : pds) {
				if(pd.getReadMethod() != null)
					map.put(pd.getName(), pd);
			}	
			this.propertiesMap = Collections.unmodifiableMap(map);
		}
	}
	public void clear() {
		throw new UnsupportedOperationException("Entries may not be removed from BeanPropertiesMap");
	}
	public boolean containsKey(Object key) {
		return key != null && propertiesMap.containsKey(key);
	}
	public boolean containsValue(Object value) {
		return values().contains(value);
	}
	public Set<Map.Entry<String,Object>> entrySet() {
		if(entrySet == null) 
			entrySet = new AbstractSet<Map.Entry<String,Object>>() {
				@Override
				public Iterator<Map.Entry<String, Object>> iterator() {
					return new Iterator<Map.Entry<String, Object>>() {
						protected final Iterator<Map.Entry<String, PropertyDescriptor>> delegate = propertiesMap.entrySet().iterator();
						public boolean hasNext() {
							return delegate.hasNext();
						}
						public Map.Entry<String, Object> next() {
							final Map.Entry<String, PropertyDescriptor> entry = delegate.next();
							return new Map.Entry<String, Object>() {
								public String getKey() {
									return entry.getKey();
								}
								public Object getValue() {
									return getPropertyValue(entry.getValue());
								}
								public Object setValue(Object value) {
									return setPropertyValue(entry.getValue(), value);
								}
							};
						}
						public void remove() {
							throw new UnsupportedOperationException("Entries may not be removed from BeanPropertiesMap");
						}
					};
				}
				@Override
				public int size() {
					return BeanPropertiesMap.this.size();
				}
				@Override
				public boolean contains(Object o) {
					if (!(o instanceof Map.Entry<?,?>)) return false;
					Map.Entry<?,?> e = (Map.Entry<?,?>) o;
					Object val = BeanPropertiesMap.this.get(e.getKey());
					if(val == null) return e.getValue() == null && BeanPropertiesMap.this.containsKey(e.getKey());
					else return e.getValue() != null && val.equals(e.getValue());
				}
			};
		return entrySet;
	}
	public boolean isEmpty() {
		return propertiesMap.isEmpty();
	}
	@Override
	public Object get(Object key) {
		PropertyDescriptor pd = propertiesMap.get(key);
		if(pd == null)
			return null;
		else
			return getPropertyValue(pd);
	}
	public Object put(String key, Object value) {
		PropertyDescriptor pd = propertiesMap.get(key);
		if(pd == null)
			return null;
		else
			return setPropertyValue(pd, value);
	}
	public Object remove(Object key) {
		throw new UnsupportedOperationException("Entries may not be removed from BeanPropertiesMap");
	}
	public int size() {
		return propertiesMap.size();
	}
	public Collection<Object> values() {
		if(values == null) 
			values = new AbstractCollection<Object>() {
				@Override
				public Iterator<Object> iterator() {
					return new Iterator<Object>() {
						protected final Iterator<PropertyDescriptor> delegate = propertiesMap.values().iterator();
						public boolean hasNext() {
							return delegate.hasNext();
						}
						public Object next() {
							return getPropertyValue(delegate.next());
						}
						public void remove() {
							throw new UnsupportedOperationException("Entries may not be removed from BeanPropertiesMap");
						}
					};
				}
				@Override
				public int size() {
					return BeanPropertiesMap.this.size();
				}
			};
		return values;
	}
	@Override
	public Set<String> keySet() {
		return propertiesMap.keySet();
	}
	protected Object getPropertyValue(PropertyDescriptor pd) {
		try {
			return pd.getReadMethod().invoke(bean);
		} catch(IllegalArgumentException e) {
			throw new UndeclaredThrowableException(e);
		} catch(IllegalAccessException e) {
			throw new UndeclaredThrowableException(e);
		} catch(InvocationTargetException e) {
			throw new UndeclaredThrowableException(e);
		}
	}
	protected Object setPropertyValue(PropertyDescriptor pd, Object value) {
		if(pd.getWriteMethod() == null)
			throw new UnsupportedOperationException("Property '" + pd.getName() + "' is read only");
		try {
			return pd.getWriteMethod().invoke(bean, value);
		} catch(IllegalArgumentException e) {
			throw new UndeclaredThrowableException(e);
		} catch(IllegalAccessException e) {
			throw new UndeclaredThrowableException(e);
		} catch(InvocationTargetException e) {
			throw new UndeclaredThrowableException(e);
		}
	}
}
