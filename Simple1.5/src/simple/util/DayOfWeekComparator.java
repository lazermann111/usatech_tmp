/*
 * DayOfWeekComparator.java
 *
 * Created on July 19, 2002, 4:21 PM
 */

package simple.util;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class DayOfWeekComparator implements java.util.Comparator {
	protected static final java.util.Map indices = new java.util.HashMap();
	static {
		java.text.DateFormatSymbols dfs = new java.text.DateFormatSymbols();
		String[] weekDays = dfs.getWeekdays();
		String[] shortWeekDays = dfs.getShortWeekdays();
		for(int i = 0; i < weekDays.length; i++) indices.put(weekDays[i],new Integer(i));
		for(int i = 0; i < shortWeekDays.length; i++) indices.put(shortWeekDays[i],new Integer(i));
	}
	
	/** Creates new DayOfWeekComparator */
	public DayOfWeekComparator() {
	}
	
	public int compare(java.lang.Object o1, java.lang.Object o2) {
		return ((Number)indices.get(o1)).intValue() - ((Number)indices.get(o2)).intValue();
	}
}