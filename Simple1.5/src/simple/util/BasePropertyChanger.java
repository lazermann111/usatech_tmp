package simple.util;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class BasePropertyChanger implements PropertyChanger {
	protected final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	public BasePropertyChanger() {
		super();
	}
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}
	public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}
	public void fireIndexedPropertyChange(String propertyName, int index, boolean oldValue, boolean newValue) {
		propertyChangeSupport.fireIndexedPropertyChange(propertyName, index, oldValue, newValue);
	}
	public void fireIndexedPropertyChange(String propertyName, int index, int oldValue, int newValue) {
		propertyChangeSupport.fireIndexedPropertyChange(propertyName, index, oldValue, newValue);
	}
	public void fireIndexedPropertyChange(String propertyName, int index, Object oldValue, Object newValue) {
		if(oldValue == newValue) return;
		propertyChangeSupport.fireIndexedPropertyChange(propertyName, index, oldValue, newValue);
	}
	public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {
		propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}
	public void firePropertyChange(String propertyName, int oldValue, int newValue) {
		propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}
	public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
		if(oldValue == newValue) return;
		propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}
	public PropertyChangeListener[] getPropertyChangeListeners() {
		return propertyChangeSupport.getPropertyChangeListeners();
	}
	public PropertyChangeListener[] getPropertyChangeListeners(String propertyName) {
		return propertyChangeSupport.getPropertyChangeListeners(propertyName);
	}
	public boolean hasListeners(String propertyName) {
		return propertyChangeSupport.hasListeners(propertyName);
	}
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}
	public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
	}
}
