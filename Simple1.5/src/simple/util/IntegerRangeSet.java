/**
 *
 */
package simple.util;

import java.util.Set;

/**
 * @author Brian S. Krug
 *
 */
public interface IntegerRangeSet extends Set<Integer>, Comparable<IntegerRangeSet> {
	public int addRanges(String rangeString) throws NumberFormatException, UnsupportedOperationException ;
	public int addRange(int min, int max) throws UnsupportedOperationException ;
	public char getConcatChar() ;
	public char getRangeChar() ;
}
