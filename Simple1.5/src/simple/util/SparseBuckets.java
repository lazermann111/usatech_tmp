/*
 * Created on Sep 20, 2005
 *
 */
package simple.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class SparseBuckets<E> implements Iterable<E> {
    protected List<E>[] data;
    public SparseBuckets() {
        this(10);
    }
    public SparseBuckets(int capacity) {
        data = createList(capacity);
    }
    public List<E> get(int index) {
        return get(index, false);
    }
    protected List<E> get(int index, boolean autoExpand) {
        try {
            return data[index];
        } catch(ArrayIndexOutOfBoundsException aiooe) {
            if(autoExpand) {
                List<E>[] tmp = createList(index + 5);
                System.arraycopy(data, 0, tmp, 0, data.length);
                data = tmp;
            }
            return null;
        }
    }
    public void add(int index, E element) {
        List<E> list = get(index, true);
        if(list == null) {
            list = new ArrayList<E>();
            data[index] = list;
        }
        list.add(element);
    }
    protected class SparseBucketIterator implements Iterator<E> {
        protected int index = -1;
        protected Iterator<E> listIter;
        public SparseBucketIterator() {
            listIter = findNextList();
        }
        public boolean hasNext() {
            return listIter != null && listIter.hasNext();
        }
        public E next() {
            E elem;
            try {
                elem = listIter.next();
            } catch(NullPointerException e) {
                throw new NoSuchElementException();
            }
            if(!listIter.hasNext()) {
                listIter = findNextList();
            }
            return elem;
        }
        public void remove() {
            try {
                listIter.remove();
            } catch(NullPointerException e) {
                throw new IllegalStateException();
            }
        }
        protected Iterator<E> findNextList() {
            for(index++; index < data.length; index++) {
                if(data[index] != null) {
                    return listIter = data[index].listIterator();
                }
            }
            return null;
        }
    }
    public Iterator<E> iterator() {
        return new SparseBucketIterator();
    }
    public void clear() {
        data = createList(data.length);
    }
    @SuppressWarnings("unchecked")
	protected List<E>[] createList(int length) {
    	return new List[length];
    }
}