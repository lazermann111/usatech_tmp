/*
 * Created on Jul 21, 2005
 *
 */
package simple.util;

import java.util.Map;

public interface BidiMap<K,V> extends Map<K,V> {
    /** Gets the key that is currently mapped to the specified value.
     * 
     * @param value
     * @return The key
     */
    public K getKey(V value) ;
    /** Removes the key-value pair that is currently mapped to the specified value (optional operation).
     * 
     * @param value
     * @return the old key
     */
    public K removeValue(V value) ;
    
}
