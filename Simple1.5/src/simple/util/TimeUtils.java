package simple.util;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.annotation.Nonnull;

import simple.bean.ConvertUtils;
import simple.text.StringUtils;

public class TimeUtils {
	protected static int getLikeness(String compareName, String tzId) {
		int l = 0;
		for(String p : StringUtils.split(tzId, '/')) {
			if(compareName.contains(p.toLowerCase()) && l < p.length()) {
				l = p.length();
			}
		}
		return l;
	}
	protected static class TimeZoneEntry {
		public final Set<TimeZone> timeZones;
		public final boolean daylight;
		public TimeZoneEntry(String timeZoneName, boolean daylight) {
			super();
			this.daylight = daylight;
			final String compareName = timeZoneName.toLowerCase();
			this.timeZones = new TreeSet<TimeZone>(new Comparator<TimeZone>(){
				public int compare(TimeZone o1, TimeZone o2) {
					int i = getLikeness(compareName, o2.getID()) - getLikeness(compareName, o1.getID());
					if(i != 0)
						return i;
					return o1.getID().compareToIgnoreCase(o2.getID());
				}				
			});
		}
	}
	protected static final Map<String,TimeZoneEntry> timeZoneNameMapping;
	static {
		Map<String,TimeZoneEntry> map = new HashMap<String, TimeZoneEntry>();
		for(String tzId : TimeZone.getAvailableIDs()) {
			TimeZone timeZone = TimeZone.getTimeZone(tzId);
			String timeZoneName = timeZone.getDisplayName(false, TimeZone.LONG);
			TimeZoneEntry entry = map.get(timeZoneName);
			if(entry == null) {
				entry  = new TimeZoneEntry(timeZoneName, false);
				map.put(timeZoneName, entry);
			}
			entry.timeZones.add(timeZone);
			if(timeZone.useDaylightTime()) {
				String timeZoneDaylightName = timeZone.getDisplayName(true, TimeZone.LONG);
				if(!timeZoneDaylightName.equals(timeZoneName)) {
					entry = map.get(timeZoneDaylightName);
					if(entry == null) {
						entry  = new TimeZoneEntry(timeZoneDaylightName, true);
						map.put(timeZoneDaylightName, entry);
					}
					entry.timeZones.add(timeZone);
				}
			}
		}
		timeZoneNameMapping = Collections.unmodifiableMap(map);
	}
	
	public static TimeZone findTimeZone(String timeZoneName, int timezoneOffset, Locale locale) {
		TimeZoneEntry entry = timeZoneNameMapping.get(timeZoneName);
		if(entry == null)
			return null;
		TimeZone alternate = null;
		for(TimeZone timeZone : entry.timeZones) {
			if(timezoneOffset == timeZone.getRawOffset() + (entry.daylight ? timeZone.getDSTSavings() : 0)) {
				if(locale == null)
					return timeZone;
				String shortCountry = locale.getCountry();
				if(shortCountry == null || shortCountry.length() == 0)
					return null;
				String longCountry = locale.getDisplayCountry();
				String[] parts = StringUtils.split(timeZone.getID(), '/');
				for(String part : parts) {
					if(part.equalsIgnoreCase(shortCountry) || part.equalsIgnoreCase(longCountry))
						return timeZone;
				}
				if(alternate == null)
					alternate = timeZone;
			}
		}
		return alternate;
	}
	
	// client/database format to human readable format

	public static String utcTimeOffsetSecondsToHHMM(String utcTimeOffsetSecondsStr, int deviceUtcOffsetMinutes) {
		int utcTimeOffsetSeconds = Integer.parseInt(utcTimeOffsetSecondsStr);
		int utcLocalOffsetMin = ((utcTimeOffsetSeconds - (deviceUtcOffsetMinutes * 60)) / 60) % 1440;
		if (utcLocalOffsetMin < 0)
			utcLocalOffsetMin += 1440;
		int hours = (int) Math.floor(utcLocalOffsetMin / 60);
		int minutes = (int) (utcLocalOffsetMin % 60);
		return new StringBuilder().append(hours < 10 ? "0" : "").append(hours).append(minutes < 10 ? "0" : "").append(minutes).toString();
	}
	
	public static int intervalSecondsToMinutes(String intervalSecondsStr) {
		int intervalSeconds = Integer.parseInt(intervalSecondsStr);
		int intervalMin = ((intervalSeconds / 60) % 1440);
		return intervalMin;
	}
	
	// human readable format to client/database format
	
	public static int hhmmToUtcOffsetSeconds(String hhmm, int deviceUtcOffsetMinutes) {
		int deviceTimeSec = Integer.parseInt(hhmm.substring(0, 2)) * 3600 + Integer.parseInt(hhmm.substring(2, 4)) * 60;
		int utcOffsetSec = (deviceTimeSec + (deviceUtcOffsetMinutes * 60)) % 86400;
		if (utcOffsetSec < 0)
			utcOffsetSec += 86400;
		return utcOffsetSec;
	}
	
	public static int hhmmToUtcOffsetSeconds(String hhmm, String timeZoneGuid) {
		// hhmmToUtcOffsetSeconds assumes positive offset, conversion from US to GMT
		// IE, Eastern timezone is 240, not -240
		// So must multiply by -1
		return hhmmToUtcOffsetSeconds(hhmm, getTimeZoneOffsetMinutes(timeZoneGuid)*-1);
	}
	
	public static int getTimeZoneOffsetMinutes(String timeZoneGuid) {
		TimeZone timeZone = TimeZone.getTimeZone(timeZoneGuid);
		return (timeZone.getOffset(System.currentTimeMillis()) / 1000 / 60);
	}
	
	public static int intervalMinutesToSeconds(String intervalMinutesStr) {
		int intervalMinutes = ConvertUtils.getIntSafely(intervalMinutesStr, 720);
		return intervalMinutes * 60;
	}

	/**
	 * Calculates number of days between two dates
	 *
	 * @param from  Date 1
	 * @param to    Date 2
	 * @return      Number of days between two dates
	 */
	public static int daysBetween(@Nonnull Calendar from, @Nonnull Calendar to) {
		from = (Calendar) from.clone();
		to = (Calendar) to.clone();

		from.set(Calendar.HOUR_OF_DAY, 12);
		to.set(Calendar.HOUR_OF_DAY, 12);

		int add = from.before(to) ? 1 : -1;

		int fromY = from.get(Calendar.YEAR);
		int fromM = from.get(Calendar.MONTH);
		int fromD = from.get(Calendar.DAY_OF_MONTH);

		int toY = to.get(Calendar.YEAR);
		int toM = to.get(Calendar.MONTH);
		int toD = to.get(Calendar.DAY_OF_MONTH);

		int daysDiff = 0;

		while (fromY != toY || fromM != toM || fromD != toD) {
			from.add(Calendar.DAY_OF_MONTH, add);
			fromY = from.get(Calendar.YEAR);
			fromM = from.get(Calendar.MONTH);
			fromD = from.get(Calendar.DAY_OF_MONTH);
			daysDiff += add;
		}

		return daysDiff;
	}

	/**
	 * Calculates number of days between two dates
	 *
	 * @param from  Date 1
	 * @param to    Date 2
	 * @return      Number of days between two dates
	 */
	public static int daysBetween(@Nonnull Date from, @Nonnull Date to) {
		Calendar f = Calendar.getInstance();
		f.setTime(from);
		Calendar t = Calendar.getInstance();
		t.setTime(to);
		return daysBetween(f, t);
	}

	/**
	 * Calculates number of minutes between two dates
	 *
	 * @param from  Date 1
	 * @param to    Date 2
	 * @return      Number of minutes between two dates
	 */
	public static int minutesBetween(@Nonnull Calendar from, @Nonnull Calendar to) {
		from = (Calendar) from.clone();
		to = (Calendar) to.clone();

		int add = from.before(to) ? 1 : -1;

		int fromY = from.get(Calendar.YEAR);
		int fromM = from.get(Calendar.MONTH);
		int fromD = from.get(Calendar.DAY_OF_MONTH);
		int fromH = from.get(Calendar.HOUR_OF_DAY);
		int fromMin = from.get(Calendar.MINUTE);

		int toY = to.get(Calendar.YEAR);
		int toM = to.get(Calendar.MONTH);
		int toD = to.get(Calendar.DAY_OF_MONTH);
		int toH = to.get(Calendar.HOUR_OF_DAY);
		int toMin = to.get(Calendar.MINUTE);

		int minutesDiff = 0;

		while (fromY != toY || fromM != toM || fromD != toD || fromH != toH || fromMin != toMin) {
			from.add(Calendar.MINUTE, add);
			fromY = from.get(Calendar.YEAR);
			fromM = from.get(Calendar.MONTH);
			fromD = from.get(Calendar.DAY_OF_MONTH);
			fromH = from.get(Calendar.HOUR_OF_DAY);
			fromMin = from.get(Calendar.MINUTE);
			minutesDiff += add;
		}

		return minutesDiff;
	}

	/**
	 * Calculates number of minutes between two dates
	 *
	 * @param from  Date 1
	 * @param to    Date 2
	 * @return      Number of minutes between two dates
	 */
	public static int minutesBetween(@Nonnull Date from, @Nonnull Date to) {
		Calendar f = Calendar.getInstance();
		f.setTime(from);
		Calendar t = Calendar.getInstance();
		t.setTime(to);
		return minutesBetween(f, t);
	}

	/**
	 * Returns true if 2 dates is in same calendar day
	 *
	 * @param c1    Date 1
	 * @param c2    Date 2
	 * @return      true = 2 dates is in same calendar day, false = 2 dates are not in same day
	 */
	public static boolean sameDate(@Nonnull Calendar c1, @Nonnull Calendar c2) {
		return	c1.get(Calendar.YEAR) ==         c2.get(Calendar.YEAR) &&
				c1.get(Calendar.MONTH) ==        c2.get(Calendar.MONTH) &&
				c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Returns true if 2 dates is in same calendar day
	 *
	 * @param d1    Date 1
	 * @param d2    Date 2
	 * @return      true = 2 dates is in same calendar day, false = 2 dates are not in same day
	 */
	public static boolean sameDate(@Nonnull Date d1, @Nonnull Date d2) {
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		c1.setTime(d1);
		c2.setTime(d2);
		return sameDate(c1, c2);
	}

	public static boolean areBothDatesInOneMinuteTimeFrame(@Nonnull Date d1, @Nonnull Date d2) {
		return sameDate(d1, d2) && Math.abs(minutesBetween(d1, d2)) <= 1;
	}
}
