/**
 *
 */
package simple.util;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import simple.io.Log;
import simple.util.concurrent.ResettableFuture;
import simple.util.concurrent.TrivialFuture;

/**
 * @author Brian S. Krug
 *
 */
public class LazyConversionMap<K,V> extends ConversionMap<K,K,Future<V>,V> {
	private static final Log log = Log.getLog();
	protected boolean resetOnException;
	/**
	 * @param delegate
	 */
	public LazyConversionMap(Map<K, Future<V>> delegate, boolean resetOnException) {
		super(delegate);
		this.resetOnException = resetOnException;
	}

	/**
	 * @see simple.util.ConversionMap#convertKeyFrom(java.lang.Object)
	 */
	@Override
	protected K convertKeyFrom(K key0) {
		return key0;
	}

	/**
	 * @see simple.util.ConversionMap#convertKeyTo(java.lang.Object)
	 */
	@Override
	protected K convertKeyTo(K key1) {
		return key1;
	}

	/**
	 * @see simple.util.ConversionMap#convertValueFrom(java.lang.Object)
	 */
	@Override
	protected V convertValueFrom(Future<V> value0) {
		if(value0 == null)
			return null;
		try {
			return value0.get();
		} catch(InterruptedException e) {
			log.warn("Could not get value", e);
			if(resetOnException && value0 instanceof ResettableFuture)
				((ResettableFuture<V>)value0).reset();
		} catch(ExecutionException e) {
			log.warn("Could not get value", e);
			if(resetOnException && value0 instanceof ResettableFuture)
				((ResettableFuture<V>)value0).reset();
		}
		return null;
	}

	/**
	 * @see simple.util.ConversionMap#convertValueTo(java.lang.Object)
	 */
	@Override
	protected Future<V> convertValueTo(V value1) {
		return new TrivialFuture<V>(value1);
	}

	/**
	 * @see simple.util.ConversionMap#isValueReversible()
	 */
	@Override
	protected boolean isValueReversible() {
		return true;
	}
}
