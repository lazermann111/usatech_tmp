package simple.util;

public interface Cycle<E> {
	public int add(E obj) ;
	public boolean remove(int index, E obj) ;
	public E[] drain() ;
	public E next();
	public boolean isEmpty() ;
	public int size() ;
}