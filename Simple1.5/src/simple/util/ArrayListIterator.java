/**
 *
 */
package simple.util;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class ArrayListIterator<E> implements ListIterator<E> {
	protected final E[] array;
	protected final int start;
	protected final int end;
	protected int current;
	protected boolean forward = true;
	public ArrayListIterator(E[] array, int start, int end) {
		super();
		if(start + array.length < end)
			throw new IndexOutOfBoundsException("Value of end is too high");
		this.array = array;
		this.start = start;
		this.end = end;
		this.current = start;
	}
	public ArrayListIterator(E[] array, int start) {
		this(array, start, start + array.length);
	}
	public ArrayListIterator(E[] array) {
		this(array, 0);
	}
	public void add(E o) {
		throw new UnsupportedOperationException("Not modifiable");
	}
	public boolean hasNext() {
		return current < end;
	}
	public boolean hasPrevious() {
		return current > start;
	}
	public E next() {
		if(!hasNext())
			throw new NoSuchElementException("At end");
		forward = true;
		return array[current++ % array.length];
	}
	public int nextIndex() {
		return current - start;
	}
	public E previous() {
		if(!hasPrevious())
			throw new NoSuchElementException("At beginning");
		forward = false;
		return array[--current % array.length];
	}
	public int previousIndex() {
		return current - start - 1;
	}
	public void remove() {
		throw new UnsupportedOperationException("Not modifiable");
	}
	public void set(E o) {
		if(forward) {
			if(!hasPrevious())
				throw new NoSuchElementException("At beginning");
			array[(current-1) % array.length] = o;
		} else {
			if(!hasNext())
				throw new NoSuchElementException("At end");
			array[current % array.length] = o;
		}
	}
}