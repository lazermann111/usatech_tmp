package simple.util;

import java.util.ConcurrentModificationException;

/**
 * @author Brian S. Krug
 */
public class WeakHashSet extends java.util.AbstractSet implements java.util.Set {
	private transient java.util.WeakHashMap map;
	private static final Object PRESENT = new Object();// Dummy value to associate with an Object in the backing Map
	/**
	 * Constructs a new, empty set; the backing <tt>HashMap</tt> instance has
	 * default capacity and load factor, which is <tt>0.75</tt>.
	 */
	public WeakHashSet() {
	map = new java.util.WeakHashMap();
	}
	/**
	 * Constructs a new, empty set; the backing <tt>HashMap</tt> instance has
	 * the specified initial capacity and default load factor, which is
	 * <tt>0.75</tt>.
	 *
	 * @param      initialCapacity   the initial capacity of the hash table.
	 * @throws     IllegalArgumentException if the initial capacity is less
	 *             than zero.
	 */
	public WeakHashSet(int initialCapacity) {
	map = new java.util.WeakHashMap(initialCapacity);
	}
	/**
	 * Constructs a new, empty set; the backing <tt>HashMap</tt> instance has
	 * the specified initial capacity and the specified load factor.
	 *
	 * @param      initialCapacity   the initial capacity of the hash map.
	 * @param      loadFactor        the load factor of the hash map.
	 * @throws     IllegalArgumentException if the initial capacity is less
	 *             than zero, or if the load factor is nonpositive.
	 */
	public WeakHashSet(int initialCapacity, float loadFactor) {
	map = new java.util.WeakHashMap(initialCapacity, loadFactor);
	}
	/**
	 * Constructs a new set containing the elements in the specified
	 * collection.  The capacity of the backing <tt>HashMap</tt> instance is
	 * twice the size of the specified collection or eleven (whichever is
	 * greater), and the default load factor (which is <tt>0.75</tt>) is used.
	 */
	public WeakHashSet(java.util.Collection c) {
	map = new java.util.WeakHashMap(java.lang.Math.max(2*c.size(), 11));
	addAll(c);
	}
	/**
	 * Adds the specified element to this set if it is not already
	 * present.
	 *
	 * @param o element to be added to this set.
	 * @return <tt>true</tt> if the set did not already contain the specified
	 * element.
	 */
	public boolean add(Object o) {
	return map.put(o, PRESENT)==null;
	}
	/**
	 * Removes all of the elements from this set.
	 */
	public void clear() {
	map.clear();
	}
	/**
	 * Returns <tt>true</tt> if this set contains the specified element.
	 *
	 * @return <tt>true</tt> if this set contains the specified element.
	 */
	public boolean contains(Object o) {
	return map.containsKey(o);
	}
	/**
	 * Returns <tt>true</tt> if this set contains no elements.
	 *
	 * @return <tt>true</tt> if this set contains no elements.
	 */
	public boolean isEmpty() {
	return map.isEmpty();
	}
	/**
	 * Returns an iterator over the elements in this set.  The elements
	 * are returned in no particular order.
	 *
	 * @return an Iterator over the elements in this set.
	 * @see ConcurrentModificationException
	 */
	public java.util.Iterator iterator() {
	return map.keySet().iterator();
	}
	/**
	 * Removes the given element from this set if it is present.
	 *
	 * @param o object to be removed from this set, if present.
	 * @return <tt>true</tt> if the set contained the specified element.
	 */
	public boolean remove(Object o) {
	return map.remove(o)==PRESENT;
	}
	/**
	 * Returns the number of elements in this set (its cardinality).
	 *
	 * @return the number of elements in this set (its cardinality).
	 */
	public int size() {
	return map.size();
	}
}
