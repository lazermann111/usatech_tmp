package simple.util;

public interface Copyable<E> {
	public E copy() ;
}
