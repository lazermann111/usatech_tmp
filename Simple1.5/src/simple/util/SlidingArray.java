/*
 * Created on Oct 21, 2005
 *
 */
package simple.util;

import java.lang.reflect.Array;

public class SlidingArray<E> {
    protected E[] array;
    protected int max;
    protected int last = 0;
    public SlidingArray(int size, Class<E> elementType) {
        array = (E[])Array.newInstance(elementType, size);
        max = 0;//size-1;
        last = max;
    }
    public E get(int index) {
        if(index < 0) throw new IndexOutOfBoundsException();
        if(index <= max - array.length || index > max) return null; //index is out of range of stored values
        return array[position(index)];
    }
    protected int wrap(int orig, int len) {
        if(orig < 0) return (orig % len) + len;
        if(orig >= len) return (orig  % len);
        return orig;
    }
    protected int position(int index) {
        return wrap(index + last - max, array.length);
    }
    public int size() {
        return max;
    }
    public E set(int index, E element) {
        if(index < 0) throw new IndexOutOfBoundsException();
        E old;
        //ensure bounds
        if(index > max) {
            if(index > max + array.length) {
                //clear all and start over
                last = array.length - 1;
                max = index + last;
                // clear array - except for start index (0)
                for(int i = 1; i < array.length; i++) {
                    array[i] = null;
                }
            } else { // roll
                // clear any in between
                int newLast = position(index);
                clearRange(last, newLast);
                last = newLast;
                max = index;
            }
            old = null;                
        } else if(index <= max - array.length) {
            if(index <= 1 + max - (2 * array.length)) {
                //clear all
                for(int i = 1; i < array.length; i++) {
                    array[i] = null;
                }
                last = array.length - 1;
            } else {
                int newLast = position(index + array.length - 1);
                // clear any in between
                clearRange(newLast, last);
                // clear current last
                array[last] = null;
                last = newLast;
            }
            max = index + array.length - 1;
            old = null;
        } else {
            old = array[position(index)];
        }            
        array[position(index)] = element;
        return old;
    }
    
    /** Clears exclusive range of the array
     * @param from
     * @param to
     */
    protected void clearRange(int from, int to) {
        if(from == to) {
            //do nothing
        } else if(from < to) {
            for(from++; from < to; from++) {
                array[from] = null;
            }
        } else {
            for(from++; from < array.length; from++) {
                array[from] = null;
            }
            for(to--; to >= 0; to--) {
                array[to] = null;
            }
        }
    }
}