package simple.util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/** An implementation of the <CODE>java.util.List</CODE> interface that tracks any
 * additions or removals from itself and also tracks any property changes
 * in its elements (if they have an addPropertyChangeListener() method and a
 * removePropertyChangeListener() method).
 */
public class TrackableList<E> extends AbstractList<E> implements PropertyChanger {
    protected List<E> delegate = null;
    protected transient Set<ListDataListener> aListDataListener = null;
    protected transient Set<PropertyChangeListener> aPropertyChangeListener = null;
    protected transient PropertyChangeListener fieldElementListener = null;
    protected static Map<Class<?>, Method[]> propertyChangeMethods = null;
    protected static final Method[] NOT_A_PROPERTYCHANGER = new Method[0];
    protected static final String ADD_PCL_METHOD_NAME = "addPropertyChangeListener";
    protected static final Class<?>[] ADD_PCL_ARGS = new Class<?>[]{PropertyChangeListener.class};
    protected static final String REMOVE_PCL_METHOD_NAME = "removePropertyChangeListener";
    protected static final Class<?>[] REMOVE_PCL_ARGS = ADD_PCL_ARGS;
    /**
     * TrackableList constructor comment.
     */
    public TrackableList() {
        this(new ArrayList<E>());
    }
    /** TrackableList constructor comment.
     * @param initialCapacity
     */
    public TrackableList(int initialCapacity) {
        this(new ArrayList<E>(initialCapacity));
    }
    /** TrackableList constructor comment.
     * @param list
     */
    public TrackableList(List<E> list) {
        super();
        if(list == null) list = new ArrayList<E>();
        delegate = list;
    }
    /** add method comment.
     * @param index
     * @param element
     */
    @Override
	public void add(int index, E element) {
        delegate.add(index, element);
        installListeners(element);
        fireIntervalAdded(this,index,index);
    }
    /** Add a javax.swing.event.ListDataListener.
     * @param newListener
     */
    public void addListDataListener(ListDataListener newListener) {
        if (aListDataListener == null) aListDataListener = new LinkedHashSet<ListDataListener>();
        aListDataListener.add(newListener);
    }
    /** Add a java.beans.PropertyChangeListener.
     * @param newListener
     */
    public void addPropertyChangeListener(PropertyChangeListener newListener) {
        if (aPropertyChangeListener == null) aPropertyChangeListener = new LinkedHashSet<PropertyChangeListener>();
        aPropertyChangeListener.add(newListener);
    }
    /** contains method comment.
     * @param o
     * @return
     */
    @Override
	public boolean contains(Object o) {
        return delegate.contains(o);
    }

    public void fireElementChanged(int index) {
        fireContentsChanged(this, index, index);
    }

    /**
     * AbstractListModel subclasses must call this method <b>after</b>
     * one or more elements of the list change.  The changed elements
     * are specified by a closed interval index0, index1, i.e. the
     * range that includes both index0 and index1.  Note that
     * index0 need not be less than or equal to index1.
     *
     * @param source The ListModel that changed, typically "this".
     * @param index0 One end of the new interval.
     * @param index1 The other end of the new interval.
     * @see EventListenerList
     * @see DefaultListModel
     */
    protected void fireContentsChanged(Object source, int index0, int index1) {
        fireListDataEvent(this, javax.swing.event.ListDataEvent.CONTENTS_CHANGED, index0, index1);
    }
    /**
     * AbstractListModel subclasses must call this method <b>after</b>
     * one or more elements of the list change.  The changed elements
     * are specified by a closed interval index0, index1, i.e. the
     * range that includes both index0 and index1.  Note that
     * index0 need not be less than or equal to index1.
     *
     * @param source The ListModel that changed, typically "this".
     * @param index0 One end of the new interval.
     * @param index1 The other end of the new interval.
     * @see EventListenerList
     * @see DefaultListModel
     */
    protected void fireIntervalAdded(Object source, int index0, int index1) {
        fireListDataEvent(this, javax.swing.event.ListDataEvent.INTERVAL_ADDED, index0, index1);
    }
    /**
     * AbstractListModel subclasses must call this method <b>after</b>
     * one or more elements of the list change.  The changed elements
     * are specified by a closed interval index0, index1, i.e. the
     * range that includes both index0 and index1.  Note that
     * index0 need not be less than or equal to index1.
     *
     * @param source The ListModel that changed, typically "this".
     * @param index0 One end of the new interval.
     * @param index1 The other end of the new interval.
     * @see EventListenerList
     * @see DefaultListModel
     */
    protected void fireIntervalRemoved(Object source, int index0, int index1) {
        fireListDataEvent(this, javax.swing.event.ListDataEvent.INTERVAL_REMOVED, index0, index1);
    }
    protected void fireListDataEvent(Object source, int eventType, int index0, int index1) {
        if (aListDataListener == null) return;
        ListDataEvent e = new ListDataEvent(this, eventType, index0, index1);
        for(ListDataListener ldl : aListDataListener) {
            ldl.intervalAdded(e);
        }
    }
    /**
     * Method to support listener events.
     */
    protected void firePropertyChanged(PropertyChangeEvent e) {
        if (aPropertyChangeListener == null) return;
        for(PropertyChangeListener pcl : aPropertyChangeListener) {
            pcl.propertyChange(e);
        }
    }
    /** get method comment.
     * @param index
     * @return
     */
    @Override
	public E get(int index) {
        return delegate.get(index);
    }
    /** getElementAt method comment.
     * @param index
     * @return
     */
    public E getElementAt(int index) {
        return get(index);
    }
    /**
     * add method comment.
     */
    protected PropertyChangeListener getElementListener() {
        if(fieldElementListener == null) {
            fieldElementListener = new PropertyChangeListener() {
                public void propertyChange(PropertyChangeEvent e) {
                    firePropertyChanged(e);
                    int index = indexOf(e.getSource());
                    fireContentsChanged(this,index,index);
                }
            };
        }
        return fieldElementListener;
    }
    /**
     * getPropertyChangeMethods method comment.
     */
    protected static Method[] getPropertyChangeMethods(Class<?> elementClass) {
        if(propertyChangeMethods == null) propertyChangeMethods = new HashMap<Class<?>, Method[]>();
        Method[] methods = propertyChangeMethods.get(elementClass);
        if(methods == null) {
            try {
                Method m1 = elementClass.getMethod(ADD_PCL_METHOD_NAME, ADD_PCL_ARGS);
                Method m2 = elementClass.getMethod(REMOVE_PCL_METHOD_NAME, REMOVE_PCL_ARGS);
                if(m1 != null && m2 != null) {
                    methods = new Method[]{m1,m2};
                } else {
                    methods = NOT_A_PROPERTYCHANGER;
                }
            } catch(NoSuchMethodException nsme) {
                methods = NOT_A_PROPERTYCHANGER;
            } catch(SecurityException se) {
                methods = NOT_A_PROPERTYCHANGER;
            }
            propertyChangeMethods.put(elementClass, methods);
        }
        return methods;
    }
    /** getSize method comment.
     * @return
     */
    public int getSize() {
        return size();
    }
    /** indexOf method comment.
     * @param o
     * @return
     */
    @Override
	public int indexOf(Object o) {
        return delegate.indexOf(o);
    }
    /**
     * install method comment.
     */
    protected void installListeners(Object element) {
        if(element instanceof PropertyChanger) {
            ((PropertyChanger)element).addPropertyChangeListener(getElementListener());
        } else if(element != null) {
            java.lang.reflect.Method[] methods = getPropertyChangeMethods(element.getClass());
            if(methods != null && methods.length > 0) {
                try {
                    methods[0].invoke(element,new Object[] {getElementListener()});
                } catch(IllegalAccessException e) {
                    //Log.logException(getClass().getName(),"installListeners",e,"While invoking addPropertyChangeListener on" + element.toString());
                } catch (IllegalArgumentException e) {
                    //Log.logException(getClass().getName(),"installListeners",e,"While invoking addPropertyChangeListener on" + element.toString());
                } catch (InvocationTargetException e) {
                    //Log.logException(getClass().getName(),"installListeners",e,"While invoking addPropertyChangeListener on" + element.toString());
                }
            }
        }
    }
    /** lastIndexOf method comment.
     * @param o
     * @return
     */
    @Override
	public int lastIndexOf(Object o) {
        return delegate.lastIndexOf(o);
    }
    /** remove method comment.
     * @param index
     * @return
     */
    @Override
	public E remove(int index) {
        E element = delegate.remove(index);
        uninstallListeners(element);
        fireIntervalRemoved(this,index,index);
        return element;
    }
    /** Remove a javax.swing.event.ListDataListener.
     * @param newListener
     */
    public void removeListDataListener(javax.swing.event.ListDataListener newListener) {
        if (aListDataListener != null) aListDataListener.remove(newListener);
    }
    /** Remove a java.beans.PropertyChangeListener.
     * @param newListener
     */
    public void removePropertyChangeListener(java.beans.PropertyChangeListener newListener) {
        if (aPropertyChangeListener != null) aPropertyChangeListener.remove(newListener);
    }
    /** set method comment.
     * @param index
     * @param element
     * @return
     */
    @Override
	public E set(int index, E element) {
        E oldElement = delegate.set(index,element);
        installListeners(element);
        uninstallListeners(oldElement);
        fireContentsChanged(this,index,index);
        return oldElement;
    }
    /** size method comment.
     * @return
     */
    @Override
	public int size() {
        return delegate.size();
    }
    /** toArray method comment.
     * @return
     */
    @Override
	public java.lang.Object[] toArray() {
        return delegate.toArray();
    }
    /** toArray method comment.
     * @param a
     * @return
     */
    @Override
	public <T> T[] toArray(T[] a) {
        return delegate.toArray(a);
    }
    /**
     * uninstallListeners method comment.
     */
    protected void uninstallListeners(Object element) {
        if(element instanceof PropertyChanger) {
            ((PropertyChanger)element).removePropertyChangeListener(getElementListener());
        } else if(element != null) {
            Method[] methods = getPropertyChangeMethods(element.getClass());
            if(methods != null && methods.length > 0) {
                try {
                    methods[1].invoke(element,new Object[] {getElementListener()});
                } catch(IllegalAccessException e) {
                    //Log.logException(getClass().getName(),"uninstallListeners",e,"While invoking removePropertyChangeListener on" + element.toString());
                } catch (IllegalArgumentException e) {
                    //Log.logException(getClass().getName(),"uninstallListeners",e,"While invoking removePropertyChangeListener on" + element.toString());
                } catch (InvocationTargetException e) {
                    //Log.logException(getClass().getName(),"uninstallListeners",e,"While invoking removePropertyChangeListener on" + element.toString());
                }
            }
        }
    }

	public void setElementAt(int index, E element) {
		if(index >= size()) {
			if(element != null) {
				//add to list
				int orig = size();
				while(index > size()) delegate.add(null);
		        delegate.add(index, element);
		        installListeners(element);
		        fireIntervalAdded(this,orig,index);
			}
		} else {
			if(element == null) {
				// shrink list
				remove(index);
			} else {
				// change in place
				set(index,element);
			}
		}
	}
}
