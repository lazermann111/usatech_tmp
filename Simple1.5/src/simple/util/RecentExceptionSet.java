package simple.util;

import java.util.AbstractSet;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Set;

import simple.lang.SystemUtils;

/**
 */
public class RecentExceptionSet extends AbstractSet<Throwable> implements Set<Throwable> {
	protected static int hashThrowable(Throwable throwable) {
		String msg = throwable.getMessage();
		Throwable cause = throwable.getCause();
		return throwable.getClass().hashCode() + 31 * ((msg == null ? 0 : msg.hashCode()) + 31 * (cause == null || cause == throwable ? 0 : hashThrowable(cause)));
	}
	protected static class ThrowableEntry {
		public final Throwable throwable;
		protected final int hash;
		public ThrowableEntry(Throwable throwable) {
			super();
			this.throwable = throwable;
			this.hash = hashThrowable(throwable);
		}
		@Override
		public int hashCode() {
			return hash;
		}
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof ThrowableEntry))
				return false;
			ThrowableEntry te = (ThrowableEntry)obj;
			return hash == te.hash && SystemUtils.sameThrowable(throwable, te.throwable);
		}
		@Override
		public String toString() {
			return throwable.toString();
		}
	}
    protected final LRUMap<ThrowableEntry,Object> map = new LRUMap<ThrowableEntry, Object>(5);
    private static final Object PRESENT = new Object();// Dummy value to associate with an Object in the backing Map
    
    public RecentExceptionSet() {
    	
    }
    public RecentExceptionSet(int maxSize) {
        this.map.setMaxSize(maxSize);
    }
    /**
     * Adds the specified element to this set if it is not already
     * present.
     *
     * @param o element to be added to this set.
     * @return <tt>true</tt> if the set did not already contain the specified
     * element.
     */
    public boolean add(Throwable o) {
        return map.put(new ThrowableEntry(o), PRESENT)==null;
    }
    /**
     * Removes all of the elements from this set.
     */
    public void clear() {
        map.clear();
    }
    /**
     * Returns <tt>true</tt> if this set contains the specified element.
     *
     * @return <tt>true</tt> if this set contains the specified element.
     */
    public boolean contains(Object o) {
        return map.containsKey(new ThrowableEntry((Throwable)o));
    }
    /**
     * Returns <tt>true</tt> if this set contains no elements.
     *
     * @return <tt>true</tt> if this set contains no elements.
     */
    public boolean isEmpty() {
        return map.isEmpty();
    }
    /**
     * Returns an iterator over the elements in this set.  The elements
     * are returned in no particular order.
     *
     * @return an Iterator over the elements in this set.
     * @see ConcurrentModificationException
     */
    public java.util.Iterator<Throwable> iterator() {
    	final Iterator<ThrowableEntry> delegate = map.keySet().iterator();
    	return new Iterator<Throwable>() {			
			public void remove() {
				delegate.remove();
			}		
			public Throwable next() {
				return delegate.next().throwable;
			}		
			public boolean hasNext() {
				return delegate.hasNext();
			}
		};
    }
    /**
     * Removes the given element from this set if it is present.
     *
     * @param o object to be removed from this set, if present.
     * @return <tt>true</tt> if the set contained the specified element.
     */
    public boolean remove(Object o) {
        return map.remove(new ThrowableEntry((Throwable)o))==PRESENT;
    }
    /**
     * Returns the number of elements in this set (its cardinality).
     *
     * @return the number of elements in this set (its cardinality).
     */
    public int size() {
        return map.size();
    }
	public int getMaxSize() {
		return map.getMaxSize();
	}
	public void setMaxSize(int maxSize) {
		map.setMaxSize(maxSize);
	}   
}
