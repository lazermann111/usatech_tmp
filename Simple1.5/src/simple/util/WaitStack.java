package simple.util;

/**
 * This class is a last-in, first-out queue (a one-direction linked list)
 * @deprecated - Use java.util.concurrency classes
 * @author Brian S. Krug
 */
public class WaitStack {
	protected class Entry {
		public Object element = null;
		public Entry next = null;
	}
	protected Entry head = null;
	protected int size = 0;
	
        /** Creates a new WaitStack */        
	public WaitStack() {
		super();
	}
	/** Adds an object to the top of the stack
         * @param o The object to add
         */
	public synchronized void push(Object o) {
		Entry e = new Entry();
		e.element = o;
		e.next = head;
		head = e;
		size++;
		notify();
	}
	/** Gets the object at the top of the stack and removes it. This method blocks until an object is available.
         * @return The object that was at the top of the stack
         */
	public synchronized Object pop() {
		while(head == null) {
			try {
				wait();
			} catch(InterruptedException e) {
				return null;
			}
		}
		Object element = head.element;
		head = head.next;
		size--;
		return element;
	}
	/** Gets the object at the top of the stack and removes it. This method does not block, but returns null if the stack is empty.
         * @return The object that was at the top of the stack
         */
	public synchronized Object popNoWait() {
		if(head == null) return null;
		Object element = head.element;
		head = head.next;
		size--;
		return element;
	}
	/**
	 * Gets the object at the top of the stack without removing it.
	 * Creation date: (11/7/00 10:38:27 AM)
	 * @return java.lang.Object
	 */
	public synchronized Object peek() {
		return (head == null ? null : head.element);
	}
	/**
	 * Clears all entries in the stack
	 */
	public synchronized void clear() {
		head = null;
		size = 0;
	}
        /** Returns the number of objects in the stack
         * @return The stack's size
         */        
	public int size() {
		return size;
	}
}
