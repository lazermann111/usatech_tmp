package simple.util;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class IncludingSet<E> extends AbstractSet<E> {
	protected final Set<E> delegate;
	protected final Collection<E> included;

	public IncludingSet(Set<E> delegate, Collection<E> included) {
		super();
		this.delegate = delegate;
		this.included = included;
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private final Iterator<E> i = delegate.iterator();
			private E next = null;

			public boolean hasNext() {
				if(next != null)
					return true;
				while(i.hasNext()) {
					E e = i.next();
					if(included.contains(e)) {
						next = e;
						return true;
					}
				}
				return false;
			}

			public E next() {
				if(!hasNext())
					throw new java.util.NoSuchElementException();
				E e = next;
				next = null;
				return e;
			}

			public void remove() {
				if(next != null)
					throw new IllegalStateException("remove called twice or after hasNext");
				i.remove();
				next = null;
			}
		};
	}

	@Override
	public int size() {
		int size = included.size();
		for(E e : included)
			if(!delegate.contains(e))
				size--;
		return size;
	}

	public boolean add(E e) {
		if(!included.contains(e))
			return false;
		return delegate.add(e);
	}

	@Override
	public boolean contains(Object o) {
		return delegate.contains(o) && included.contains(o);
	}

	@Override
	public boolean isEmpty() {
		return delegate.isEmpty() || size() == 0;
	}

	@Override
	public boolean remove(Object o) {
		if(!included.contains(o))
			return false;
		return delegate.remove(o);
	}
}
