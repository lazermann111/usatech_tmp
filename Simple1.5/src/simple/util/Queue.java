package simple.util;

/**
 * This class is a first-in, first-out queue (a one-direction linked list) 
 *
 * @author Brian S. Krug
 * @deprecated - Use java.util.concurrency classes
 */
public class Queue {
	protected class Entry {
		public Object element = null;
		public Entry next = null;
	}
	protected Entry head = null;
	protected Entry tail = null;
	protected int size = 0;
/**
 * Queue constructor comment.
 */
public Queue() {
	super();
}
/** Adds an object to the end of the queue
 * @param o The object to add to the queue
 */
public synchronized void append(Object o) {
	Entry e = new Entry();
	e.element = o;
	if(tail != null) {
		tail.next = e;
	} else {
		head = e;
	}
	tail = e;
	size++;
	notify();
}
/**
 * Gets the next object in the queue and removes it. This method blocks until an object is available).
 * Creation date: (11/7/00 10:38:27 AM)
 * @return java.lang.Object
 */
public synchronized Object next() {
	while(head == null) {
		try {
			wait();
		} catch(InterruptedException e) {
			return null;
		}
	}
	Object element = head.element;
	head = head.next;
	if(head == null) tail = null;
	size--;
	return element;
}
/**
 * Gets the next object in the queue and removes it. This method blocks until an object is available).
 * Creation date: (11/7/00 10:38:27 AM)
 * @return java.lang.Object
 */
public synchronized Object nextNoWait() {
	if(head == null) return null;
	Object element = head.element;
	head = head.next;
	if(head == null) tail = null;
	size--;
	return element;
}
/**
 * Gets the first object in the queue without removing it.
 * Creation date: (11/7/00 10:38:27 AM)
 * @return java.lang.Object
 */
public synchronized Object peek() {
	return (head == null ? null : head.element);
}
/**
 * Clears all entries in the queue
 */
public synchronized void clear() {
        head = null;
        tail = null;
	size = 0;
}
/** Returns the number of objects in the queue
 * @return The size of the queue
 */
public int size() {
	return size;
}
}
