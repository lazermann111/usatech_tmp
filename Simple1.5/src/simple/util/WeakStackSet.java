package simple.util;

/**
 * This class is a last-in, first-out queue (a one-direction linked list)
 *
 * @author Brian S. Krug
 */
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

/** Provides a StackSet with weakly referenced elements. See {@link
 * simple.util.StackSet} for information on StackSets.
 */
public class WeakStackSet extends StackSet {
	protected class WeakEntry extends Entry {
		protected WeakReference wr = null;
		public void setElement(Object element) {
			wr = new WeakReference(element);
		}
		public Object getElement() {
			return wr.get();
		}
	}
	
        /** Creates a new WeakStackSet. */        
	public WeakStackSet() {
		super();
	}
	
	protected void init() {
		map = new WeakHashMap();
	}
	
	protected Entry newEntry() {
		return new WeakEntry();
	}
}