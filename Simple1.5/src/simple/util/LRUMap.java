/*
 * Created on Dec 6, 2005
 *
 */
package simple.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
/**
 * This map implementation limits the number of entries it holds to the specified maxSize and removes the 
 * least-recently used entry if it needs to. See documentation on java.util.LinkedHashMap.
 * @author bkrug
 *
 */
public class LRUMap<K,V> extends LinkedHashMap<K,V> {
    private static final long serialVersionUID = -5123567028796053310L;
    protected int maxSize;
    public LRUMap(int maxSize, int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor, true);
        if(maxSize < 1) throw new IllegalArgumentException("Max Size must be greater than zero");
        this.maxSize = maxSize;
    }

    public LRUMap(int maxSize, int initialCapacity) {
        this(maxSize, initialCapacity, 0.75f);
    }

    public LRUMap(int maxSize) {
        this(maxSize, Math.max((int) (maxSize / 0.75f) + 1, 16));
    }

    public LRUMap(int maxSize, Map<? extends K, ? extends V> m) {
        this(maxSize, Math.max((int) (m.size() / 0.75f) + 1, 16), 0.75f);
        putAll(m);
    }

    protected boolean removeEldestEntry(Map.Entry<K,V> eldest) {
       return size() > getMaxSize();
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        if(maxSize < 1) throw new IllegalArgumentException("Max Size must be greater than zero");
        if(size() > maxSize) {
            Iterator<K> iter = keySet().iterator();
            for(int n = (size() - maxSize); n > 0; n--) {
                iter.next();
                iter.remove();
            }
        }
        this.maxSize = maxSize;
    }   
}
