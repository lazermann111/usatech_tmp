/**
 *
 */
package simple.util;

import java.util.Map;
import java.util.Set;

/**
 * @author Brian S. Krug
 *
 */
public class IncludingMap<K,V> extends AbstractLookupMap<K,V> {
	protected Map<K,V> map;
    protected Set<K> include;

    /** Creates a new instance of ExcludingMap */
    public IncludingMap(Map<K,V> delegate, Set<K> include) {
        this.map = delegate;
        this.include = include;
    }

	/**
	 * @see simple.util.AbstractLookupMap#get(java.lang.Object)
	 */
	@Override
	public V get(Object key) {
		if(include.contains(key))
			return map.get(key);
		else
			return null;
	}

	/**
	 * @see simple.util.AbstractLookupMap#keySet()
	 */
	@Override
	public Set<K> keySet() {
		return include;
	}

}
