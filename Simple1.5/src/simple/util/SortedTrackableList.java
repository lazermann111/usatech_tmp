/*
 * Created on Dec 22, 2005
 *
 */
package simple.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.event.ListDataEvent;

/**This list keeps its contents sorted. It complete ignores the index parameter specifed in the add() method.
 * @author bkrug
 *
 */
public class SortedTrackableList<E> extends TrackableList<E> {
    protected boolean allowDups;
    protected Comparator<E> comparator;

    public SortedTrackableList(boolean allowDups) {
        this(allowDups, null);
    }

    public SortedTrackableList(int initialCapacity, boolean allowDups) {
        this(initialCapacity, allowDups, null);
    }

    public SortedTrackableList(List<E> list, boolean allowDups) {
        this(list, allowDups, null);
    }

    public SortedTrackableList(boolean allowDups, Comparator<E> comparator) {
        super(null);
        this.allowDups = allowDups;
        this.comparator = comparator;
    }

    public SortedTrackableList(int initialCapacity, boolean allowDups, Comparator<E> comparator) {
        super(initialCapacity);
        this.allowDups = allowDups;
        this.comparator = comparator;
    }

    public SortedTrackableList(List<E> list, boolean allowDups, Comparator<E> comparator) {
        super(list == null ? 16 : list.size());
        this.allowDups = allowDups;
        this.comparator = comparator;
        if(list != null) this.addAll(list);
    }

    @Override
    public void add(int index, E element) {
        int sortIndex = Collections.binarySearch(delegate, element, comparator);
        if(allowDups || sortIndex < 0)
            super.add(-1 - sortIndex, element);
    }

    @Override
    public E set(int index, E element) {
        E old = remove(index);
        add(element);
        return old;
    }

    @SuppressWarnings("unchecked")
	@Override
	protected void fireListDataEvent(Object source, int eventType, int index0, int index1) {
        if(eventType == ListDataEvent.CONTENTS_CHANGED) {
            Object[] changed = new Object[index1 - index0 + 1];
            for(int i = index1; i >= index0; i--) {
                changed[i-index0] = remove(i);
            }
            for(Object elem : changed)
                add((E)elem);
        }
        super.fireListDataEvent(source, eventType, index0, index1);
    }
}
