package simple.util;

/**
 * This class is a last-in, first-out queue (similiar to a two-direction linked list) that also enforces
 * uniqueness amongst its elements like a Set. It does not implement <CODE>java.util.List</CODE> or 
 * <CODE>java.util.Set</CODE> because these interfaces add methods not fundamental to this class's functionality.
 * @deprecated - Use java.util.concurrency classes
 *
 * @author Brian S. Krug
 */
public class StackSet {
	protected class Entry {
		protected Object element = null;
		public void setElement(Object element) {
			this.element = element;
		}
		public Object getElement() {
			return element;
		}
		public Entry next = null;
		public Entry previous = null;
	}
	protected Entry head = null;
	protected int size = 0;
	protected java.util.Map map;
	
	public StackSet() {
		super();
		init();
	}
	
	protected void init() {
		map = new java.util.HashMap();
	}
	
	protected Entry newEntry() {
		return new Entry();
	}
	
	/**
	 * Adds an object to the top of the stack
	 */
	public synchronized void push(Object o) {
		Entry e = newEntry();
		e.setElement(o);
		e.next = head;
		if(head != null) e.next.previous = e;
		head = e;
		map.put(o,e);
		size++;
		notify();
	}
	/**
	 * Gets the object at the top of the stack and removes it. This method blocks until an object is available.
	 */
	public synchronized Object pop() {
		while(head == null) {
			try {
				wait();
			} catch(InterruptedException e) {
				return null;
			}
		}
		return popNoWait();
	}
	/**
	 * Gets the object at the top of the stack and removes it. This method does not block, but returns null if the stack is empty.
	 */
	public synchronized Object popNoWait() {
		if(head == null) return null;
		Object element = head.getElement();
		if(element != null) map.remove(element);
		head = head.next;
		if(head != null) head.previous = null;
		size--;
		return element;
	}
	/**
	 * Gets the object at the top of the stack without removing it.
	 * 
	 * @return java.lang.Object
	 */
	public synchronized Object peek() {
		return (head == null ? null : head.getElement());
	}
	/**
	 * Clears all entries in the stack
	 */
	public synchronized void clear() {
		head = null;
		map.clear();
		size = 0;
	}
	public int size() {
		return size;
	}
	/*
	 * Remove the specified element
	 */
	public synchronized boolean remove(Object o) {
		Entry e = (Entry) map.remove(o);
		if(e != null) {
			if(e.previous != null) e.previous.next = e.next;
			if(e.next != null) e.next.previous = e.previous;
			size--;
			return true;
		}
		return false;
	}
	public boolean contains(Object o) {
		return map.containsKey(o);
	}
}
