/*
 * WrappedSetMap.java
 *
 * Created on November 13, 2003, 2:50 PM
 */

package simple.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/** Turns a <CODE>java.util.Set</CODE> into a <CODE>java.util.Map</CODE> with null
 * values.
 * @author Brian S. Krug
 */
public class WrappedSetMap implements Map {
    protected Set set;
    protected Set entrySet = null;
    
    /** Creates a new instance of WrappedSetMap */
    public WrappedSetMap(Set wrappedSet) {
        this.set = wrappedSet;
    }
    
    public void clear() {
        set.clear();
    }
    
    /**
     * @param key
     * @return
     */    
    public boolean containsKey(Object key) {
        return set.contains(key);
    }
    
    /**
     * @param value
     * @return
     */    
    public boolean containsValue(Object value) {
        return value == null;
    }
    
    /**
     * @return
     */    
    public java.util.Set entrySet() {
        if(entrySet == null) entrySet = new java.util.AbstractSet() {
            public Iterator iterator() {
                return new Iterator() {
                    protected Iterator iter = set.iterator();
                    public void remove() {
                        iter.remove();
                    }
                    public boolean hasNext() {
                        return iter.hasNext();
                    }
                    public Object next() {
                        return new Map.Entry() {
                            protected Object key = iter.next();
                            public Object getKey() { return key; }
                            public Object getValue() { return null; }
                            public Object setValue(Object newValue) { return null; }
                        };
                    }
                };
            }
            public int size() {
                return WrappedSetMap.this.size();
            }
        };
        return entrySet;
    }
    
    /**
     * @param key
     * @return
     */    
    public Object get(Object key) {
        return null;
    }
    
    /**
     * @return
     */    
    public boolean isEmpty() {
        return set.isEmpty();
    }
    
    /**
     * @return
     */    
    public java.util.Set keySet() {
        return set;
    }
    
    /**
     * @param key
     * @param value
     * @return
     */    
    public Object put(Object key, Object value) {
        set.add(key);
        return null;
    }
    
    /**
     * @param t
     */    
    public void putAll(Map t) {
        set.addAll(t.keySet());
    }
    
    /**
     * @param key
     * @return
     */    
    public Object remove(Object key) {
        set.remove(key);
        return null;
    }
    
    /**
     * @return
     */    
    public int size() {
        return set.size();
    }
    
    /**
     * @return
     */    
    public java.util.Collection values() {
        return null;
    }
    
}
