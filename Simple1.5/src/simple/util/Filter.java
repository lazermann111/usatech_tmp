package simple.util;

public interface Filter<E> {
	public boolean accept(E object) ;
}
