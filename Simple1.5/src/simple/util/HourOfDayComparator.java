/*
 * DayOfWeekComparator.java
 *
 * Created on July 19, 2002, 4:21 PM
 */

package simple.util;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class HourOfDayComparator implements java.util.Comparator {
	
	/** Creates new DayOfWeekComparator */
	public HourOfDayComparator() {
	}
	
	public int compare(java.lang.Object o1, java.lang.Object o2) {	
		return getHour((String)o1) - getHour((String)o2);
	}
	
	protected int getHour(String s) {
		s = s.trim();
		int i = 0;
		char c;
		for(; i < s.length(); i++) if((c = s.charAt(i)) < '0' || c > '9')  break;
		int val = 0;
		try {
			val = Integer.parseInt(s.substring(0,i));
			if(s.substring(i).trim().toUpperCase().startsWith("P")) val = val + 12;
		} catch(NumberFormatException nfe) {}
		return val;
	}
}