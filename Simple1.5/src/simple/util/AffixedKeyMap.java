/**
 *
 */
package simple.util;

import java.util.Map;

/** Presents the the entries of the underlying map but with the key prepended and appended by the prefix and suffix specified
 * @author Brian S. Krug
 *
 */
public class AffixedKeyMap<V> extends ConversionMap<String, String, V, V> {
	protected String prefix;
    protected String suffix;

	/**
	 * @param delegate
	 */
	public AffixedKeyMap(Map<String, V> delegate, String prefix, String suffix) {
		super(delegate);
		this.prefix = (prefix == null ? "" : prefix);
        this.suffix = (suffix == null ? "" : suffix);
	}

	/**
	 * @see simple.util.ConversionMap#convertKeyFrom(java.lang.Object)
	 */
	@Override
	protected String convertKeyFrom(String key0) {
		return prefix + key0 + suffix;
	}

	/**
	 * @see simple.util.ConversionMap#convertKeyTo(java.lang.Object)
	 */
	@Override
	protected String convertKeyTo(String key1) {
		if(key1 != null && key1.startsWith(prefix) && key1.endsWith(suffix))
			return key1.substring(prefix.length(), key1.length() - suffix.length());
		return null;
	}

	/**
	 * @see simple.util.ConversionMap#convertValueFrom(java.lang.Object)
	 */
	@Override
	protected V convertValueFrom(V value0) {
		return value0;
	}

	/**
	 * @see simple.util.ConversionMap#convertValueTo(java.lang.Object)
	 */
	@Override
	protected V convertValueTo(V value1) {
		return value1;
	}

	/**
	 * @see simple.util.ConversionMap#isValueReversible()
	 */
	@Override
	protected boolean isValueReversible() {
		return true;
	}
}
