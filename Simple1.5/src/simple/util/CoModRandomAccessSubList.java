package simple.util;

import java.util.AbstractList;
import java.util.RandomAccess;

public class CoModRandomAccessSubList<E> extends CoModSubList<E> implements RandomAccess {

	public CoModRandomAccessSubList(AbstractList<E> list, int fromIndex, int toIndex) {
		super(list, fromIndex, toIndex);
	}

}
