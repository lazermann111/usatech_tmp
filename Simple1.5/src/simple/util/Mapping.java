package simple.util;

public interface Mapping<K,V> {
	public V get(K key) ;
}
