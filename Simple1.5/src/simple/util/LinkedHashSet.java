/*
 * @(#)LinkedHashSet.java	1.8 03/01/20
 *
 */

package simple.util;

/**
 * Uses LinkedHashMap as the backing for this set. Not Java 1.4 dependent!
 */

public class LinkedHashSet extends MapBackedSet {
    /**
     * Constructs a new, empty linked hash set with the specified initial
     * capacity and load factor. 
     *
     * @param      initialCapacity the initial capacity of the linked hash set
     * @param      loadFactor      the load factor of the linked hash set.
     * @throws     IllegalArgumentException  if the initial capacity is less
     *               than zero, or if the load factor is nonpositive.
     */
    public LinkedHashSet(int initialCapacity, float loadFactor) {
        super(new LinkedHashMap(initialCapacity, loadFactor));
    }

    /**
     * Constructs a new, empty linked hash set with the specified initial
     * capacity and the default load factor (0.75).
     *
     * @param   initialCapacity   the initial capacity of the LinkedHashSet.
     * @throws  IllegalArgumentException if the initial capacity is less
     *              than zero.
     */
    public LinkedHashSet(int initialCapacity) {
        super(new LinkedHashMap(initialCapacity));
    }

    /**
     * Constructs a new, empty linked hash set with the default initial
     * capacity (16) and load factor (0.75).
     */
    public LinkedHashSet() {
        super(new LinkedHashMap());
    }

    /**
     * Constructs a new linked hash set with the same elements as the
     * specified collection.  The linked hash set is created with an initial
     * capacity sufficient to hold the elements in the specified collection
     * and the default load factor (0.75).
     *
     * @param c  the collection whose elements are to be placed into 
     *           this set.
     * @throws NullPointerException if the specified collection is null.
     */
    public LinkedHashSet(java.util.Collection c) {
        super(new LinkedHashMap(Math.max(2*c.size(), 11), .75f));
        addAll(c);
    }
}
