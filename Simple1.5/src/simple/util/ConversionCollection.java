package simple.util;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

public abstract class ConversionCollection<E0,E1> extends AbstractCollection<E1> {
	protected final Collection<E0> delegate;
	protected class ConversionIterator implements Iterator<E1> {
		protected final Iterator<E0> delegateIter = delegate.iterator();
		public boolean hasNext() {
			return delegateIter.hasNext();
		}
		public E1 next() {
			return convertFrom(delegateIter.next());
		}
		public void remove() {
			delegateIter.remove();
		}
	}
	public ConversionCollection(Collection<E0> delegate) {
		super();
		this.delegate = delegate;
	}
	@Override
	public Iterator<E1> iterator() {
		return new ConversionIterator();
	}

	// copied from AbstractSet
	@Override
	public int hashCode() {
		int h = 0;
		Iterator<E1> i = iterator();
		while (i.hasNext()) {
		    E1 obj = i.next();
	            if (obj != null)
	                h += obj.hashCode();
	        }
		return h;
    }

	@Override
	public int size() {
		return delegate.size();
	}

	@Override
	public void clear() {
		if(!isReversible()) throw new UnsupportedOperationException("Not reversible");
		delegate.clear();
	}

	@Override
	public boolean add(E1 o) {
		if(!isReversible()) throw new UnsupportedOperationException("Not reversible");
		return delegate.add(convertTo(o));
	}

	@Override
	public boolean contains(Object o) {
		if(!isReversible()) return super.contains(o);
		return delegate.contains(convertTo(castE1(o)));
	}

	@Override
	public boolean remove(Object o) {
		if(!isReversible()) throw new UnsupportedOperationException("Not reversible");
		return delegate.remove(convertTo(castE1(o)));
	}

	@SuppressWarnings("unchecked")
	protected E1 castE1(Object object1) {
		return (E1)object1;
	}

	/** Converts object from external type to internal (delegate) type. This must be implemented
	 *  only if {@link #isReversible()} returns true
	 * @param object1
	 * @return
	 */
	protected abstract E0 convertTo(E1 object1) ;
	/** Converts object from internal (delegate) type to external type. This must be implemented.
	 * @param object0
	 * @return
	 */
	protected abstract E1 convertFrom(E0 object0) ;
	/** Whether the conversion from E0 to E1 is supported. If it is supported then modification
	 * operations will be passed to the delegate otherwise, UnsupportedOperationException's will be
	 * thrown in {@link #add(Object)} and {@link #remove(Object)}
	 * @return <code>true</code> if and only if {@link #convertFrom(Object)} is fully implemented
	 */
	protected abstract boolean isReversible() ;
}
