/*
 * Created on Mar 18, 2005
 *
 */
package simple.util;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author bkrug
 *
 */
public class ObjectCache<E> {
    protected AtomicLong refId = new AtomicLong();
    protected Map<Long, E> cache = new HashMap<Long, E>();
    public ObjectCache() {
        
    }
    public E getObject(long refId) {
        return cache.get(refId);
    }
    public E getAndClearObject(long refId) {
        return cache.remove(refId);
    }
    public long putObject(E o) {
        long id = refId.incrementAndGet();
        cache.put(id, o);
        return id;
    }
}
