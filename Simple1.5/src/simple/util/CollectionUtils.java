package simple.util;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.lang.Decision;

public class CollectionUtils {
	public static Enumeration<Object> EMPTY_ENUM = new Enumeration<Object>() {
		@Override
		public boolean hasMoreElements() {
			return false;
		}

		@Override
		public Object nextElement() {
			throw new NoSuchElementException();
		}
	};

	@SuppressWarnings("unchecked")
	public static <E> Enumeration<E> emptyEnumeration() {
		return (Enumeration<E>) EMPTY_ENUM;
	}

	public static boolean containsAny(Set<?> search, Collection<?> find) {
		if(find != null && !search.isEmpty())
			for(Object o : find) {
				if(search.contains(o))
					return true;
			}
		return false;
	}

	public static boolean containsOnly(Set<?> search, Set<?> find) {
		if(find == null || search.isEmpty())
			return true;
		return find.containsAll(search);
	}

	public static boolean containsOnly(Set<?> search, Object... find) {
		if(find == null || search.isEmpty() || find.length == 0)
			return true;
		if(find.length == 1)
			return search.size() == 1 && search.contains(find[0]);
		Set<Object> findSet = new HashSet<>();
		for(Object f : find)
			findSet.add(f);
		return findSet.containsAll(search);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object uncheckedPut(Map<?,?> map, Object key, Object value) {
		return ((Map)map).put(key, value);
	}
	@SuppressWarnings("unchecked")
	public static <K,V> Map<K,V> uncheckedMap(Map<?,?> map, Class<K> keyClass, Class<V> valueClass) {
		return (Map<K,V>)map;
	}
	@SuppressWarnings("unchecked")
	public static <E> Collection<E> uncheckedCollection(Collection<?> collection, Class<E> elementClass) {
		return (Collection<E>)collection;
	}
	@SuppressWarnings("unchecked")
	public static <E> List<E> uncheckedCollection(List<?> collection, Class<E> elementClass) {
		return (List<E>)collection;
	}
	@SuppressWarnings("unchecked")
	public static <E> Set<E> uncheckedCollection(Set<?> collection, Class<E> elementClass) {
		return (Set<E>)collection;
	}
	@SuppressWarnings("unchecked")
	public static <E> Iterator<E> uncheckedIterator(Iterator<?> iterator, Class<E> elementClass) {
		return (Iterator<E>) iterator;
	}

	@SuppressWarnings("unchecked")
	public static <E> ListIterator<E> uncheckedIterator(ListIterator<?> iterator, Class<E> elementClass) {
		return (ListIterator<E>) iterator;
	}

	@SuppressWarnings("unchecked")
	public static <T> T genericize(Object object) {
    	return (T)object;
    }
	public static <T> T[] add(T[] array, T element) {
		return add(array, array.length, element, false);
	}
	public static <T> T[] add(T[] array, int index, T element) {
		return add(array, index, element, false);
	}
	public static <T> T[] add(T[] array, int index, T element, boolean allowExtraExpansion) {
		if(!allowExtraExpansion && index > array.length)
			throw new ArrayIndexOutOfBoundsException(index);
		int newLen;
		int copyLen;
		int copyLen2;
		if(index >= array.length) {
			newLen = index+1;
			copyLen = array.length;
			copyLen2 = 0;
		} else {
			newLen = array.length + 1;
			copyLen = index;
			copyLen2 = array.length - index;
		}
		T[] tmp = (T[])Array.newInstance(array.getClass().getComponentType(), newLen);
		System.arraycopy(array, 0, tmp, 0, copyLen);
		tmp[index] = element;
		if(copyLen2 > 0) {
			System.arraycopy(array, copyLen, tmp, index+1, copyLen2);
		}
		return tmp;
	}

	public static <T> T[] remove(T[] array, int index) {
		T[] tmp = (T[])Array.newInstance(array.getClass().getComponentType(), array.length - 1);
		System.arraycopy(array, 0, tmp, 0, index);
		System.arraycopy(array, index+1, tmp, index, array.length - index - 1);
		return tmp;
	}

	public static <E,C extends Collection<E>> C emptyCopy(C original, Class<? extends C> fallback) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
		C copy = copyInternal(original, fallback, false);
		copy.clear();
		return copy;
	}
	public static <E,C extends Collection<E>> C copy(C original, Class<? extends C> fallback) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
		return copyInternal(original, fallback, true);
	}
	protected static <E,C extends Collection<E>> C copyInternal(C original, Class<? extends C> fallback, boolean addToNew) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
		try {
			if(original != null && ReflectionUtils.hasMethod(original.getClass(), "copy"))
				return (C) ReflectionUtils.invokeMethod(original, "copy");
			else if(original != null && ReflectionUtils.hasMethod(original.getClass(), "clone"))
				return (C) ReflectionUtils.invokeMethod(original, "clone");
			if(fallback == null) throw new NoSuchMethodException("clone");
		} catch(SecurityException e) {
			if(fallback == null) throw e;
		} catch(IllegalArgumentException e) {
			if(fallback == null) throw e;
		} catch(NoSuchMethodException e) {
			if(fallback == null) throw e;
		} catch(IllegalAccessException e) {
			if(fallback == null) throw e;
		} catch(InvocationTargetException e) {
			if(fallback == null) throw e;
		}
		C copy = fallback.newInstance();
		if(addToNew) copy.addAll(original);
		return copy;
	}

	@SuppressWarnings("unchecked")
	public static <E,C extends Collection<E>> C deepCopy(C original, Class<? extends C> fallback) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
		C copy = emptyCopy(original, fallback);
		for(E elem : original) {
			E elemCopy;
			if(elem instanceof Collection)
				elemCopy = (E) deepCopy((Collection<?>) elem, getDefaultFallback(((Collection<?>) elem).getClass()));
			else if(elem != null && ReflectionUtils.hasMethod(elem.getClass(), "copy"))
				elemCopy = (E) ReflectionUtils.invokeMethod(elem, "copy");
			else if(elem != null && ReflectionUtils.hasMethod(elem.getClass(), "clone"))
				elemCopy = (E) ReflectionUtils.invokeMethod(elem, "clone");
			else
				elemCopy = elem;
			copy.add(elemCopy);
		}
		return copy;
	}

	@SuppressWarnings("rawtypes")
	protected static Class<? extends Collection> getDefaultFallback(Class<? extends Collection> clazz) {
		if(Cloneable.class.isAssignableFrom(clazz) || ReflectionUtils.hasConstructor(clazz))
			return clazz;
		if(SortedSet.class.isAssignableFrom(clazz))
			return TreeSet.class;
		if(Set.class.isAssignableFrom(clazz))
			return HashSet.class;
		if(BlockingQueue.class.isAssignableFrom(clazz))
			return LinkedBlockingQueue.class;
		if(java.util.Queue.class.isAssignableFrom(clazz))
			return LinkedList.class;
		if(List.class.isAssignableFrom(clazz)) {
			//if(true || RandomAccess.class.isAssignableFrom(clazz))
				return ArrayList.class;
			/*else
				return LinkedList.class;*/
		}
		return HashSet.class;
	}
	public static boolean deepEquals(Object o1, Object o2) {
		return ConvertUtils.areEqual(o1, o2);
	}

	public static int deepHashCode(Object object) {
		if(object == null)
			return 0;
		/* Using the following line actually takes significantly longer!
		else if(!object.getClass().isArray())
    		return object.hashCode();*/
		else if(object instanceof Object[])
			return Arrays.deepHashCode((Object[]) object);
		else if(object instanceof byte[])
			return Arrays.hashCode((byte[]) object);
		else if(object instanceof short[])
			return Arrays.hashCode((short[]) object);
		else if(object instanceof int[])
			return Arrays.hashCode((int[]) object);
		else if(object instanceof long[])
			return Arrays.hashCode((long[]) object);
		else if(object instanceof char[])
			return Arrays.hashCode((char[]) object);
		else if(object instanceof float[])
			return Arrays.hashCode((float[]) object);
		else if(object instanceof double[])
			return Arrays.hashCode((double[]) object);
		else if(object instanceof boolean[])
			return Arrays.hashCode((boolean[]) object);
		else if(object instanceof List<?>) {
			int hash = 1;
			for(Object elem : (List<?>)object) {
				hash = 31 * hash + deepHashCode(elem);
			}
			return hash;
		} else
			return object.hashCode();
	}

	public static String deepToString(Object object) {
		StringBuilder buf = new StringBuilder();
		deepToString(object, buf, new HashSet<Object>());
		return buf.toString();
	}

	protected static void deepToString(Object object, StringBuilder buf,  Set<Object> dejaVu) {
		if(object == null)
			buf.append("null");
		/* Using the following line actually takes significantly longer!
		else if(!object.getClass().isArray())
    		return object.hashCode();*/
		else if(object instanceof Object[]) {
			Object[] array = (Object[])object;
			buf.append('[');
	    	if(dejaVu.add(array)) {
	    		for(Object o : array) {
	    			deepToString(o, buf, dejaVu);
		    		buf.append(", ");
		    	}
	    		if(array.length > 0)
	    			buf.setLength(buf.length()-2);
	    		dejaVu.remove(array);
	    	} else {
	    		buf.append("...");
	    	}
	    	buf.append("]");
		} else if(object instanceof byte[])
			buf.append(Arrays.toString((byte[]) object));
		else if(object instanceof short[])
			buf.append(Arrays.toString((short[]) object));
		else if(object instanceof int[])
			buf.append(Arrays.toString((int[]) object));
		else if(object instanceof long[])
			buf.append(Arrays.toString((long[]) object));
		else if(object instanceof char[])
			buf.append(Arrays.toString((char[]) object));
		else if(object instanceof float[])
			buf.append(Arrays.toString((float[]) object));
		else if(object instanceof double[])
			buf.append(Arrays.toString((double[]) object));
		else if(object instanceof boolean[])
			buf.append(Arrays.toString((boolean[]) object));
		else if(object instanceof Iterable<?>) {
			Iterable<?> iterable = (Iterable<?>)object;
			buf.append('[');
	    	if(dejaVu.add(iterable)) {
	    		int count = 0;
	    		for(Object o : iterable) {
	    			deepToString(o, buf, dejaVu);
		    		buf.append(", ");
		    		count++;
		    	}
	    		if(count > 0)
	    			buf.setLength(buf.length()-2);
	    		dejaVu.remove(iterable);
	    	} else {
	    		buf.append("...");
	    	}
	    	buf.append("]");
		} else if(object instanceof Map<?, ?>) {
			Map<?, ?> map = (Map<?, ?>) object;
			if(map.isEmpty()) {
				buf.append("{}");
				return;
			}
			buf.append('{');
			for(Map.Entry<?, ?> entry : map.entrySet()) {
				deepToString(entry.getKey(), buf, dejaVu);
				buf.append('=');
				deepToString(entry.getValue(), buf, dejaVu);
				buf.append(',').append(' ');
			}
			buf.setLength(buf.length() - 2);
			buf.append('}');
		} else
			buf.append(object.toString());
	}

	public static <E> int iterativeSearch(Iterable<E> iterable, E key) {
		Iterator<E> iter = iterable.iterator();
		for(int i = 0; iter.hasNext(); i++)
			if(ConvertUtils.areEqual(iter.next(), key))
				return i;
		return -1;
	}

    public static <E> int iterativeSearch(E[] array, E key) {
    	return iterativeSearch(array, key, 0, array.length);
    }
    public static <E> int iterativeSearch(E[] array, E key, int offset, int length) {
    	int limit = Math.min(array.length, offset + length);
        for(int i = offset; i < limit; i++)
    		if(ConvertUtils.areEqual(array[i], key))
    			return i;
    	return -1;
    }

    public static int iterativeSearch(char[] array, char key) {
    	return iterativeSearch(array, key, 0, array.length);
    }
    public static int iterativeSearch(char[] array, char key, int offset, int length) {
    	int limit = Math.min(array.length, offset + length);
        for(int i = offset; i < limit; i++)
    		if(array[i] == key)
    			return i;
    	return -1;
    }

	public static int iterativeSearch(short[] array, short key) {
		return iterativeSearch(array, key, 0, array.length);
	}

	public static int iterativeSearch(short[] array, short key, int offset, int length) {
		int limit = Math.min(array.length, offset + length);
		for(int i = offset; i < limit; i++)
			if(array[i] == key)
				return i;
		return -1;
	}
	public static <E> boolean isIn(E value, E... options) {
		if(options == null || options.length == 0)
			return false;
		for(E option : options)
			if(ConvertUtils.areEqual(value, option))
				return true;
		return false;
	}
    public static int iterativeSearch(int[] array, int key) {
    	return iterativeSearch(array, key, 0, array.length);
    }
    public static int iterativeSearch(int[] array, int key, int offset, int length) {
    	int limit = Math.min(array.length, offset + length);
        for(int i = offset; i < limit; i++)
    		if(array[i] == key)
    			return i;
    	return -1;
    }
    public static boolean iterativeContainsAll(Object[] array, Object[] keys) {
    	for(int i = 0; i < keys.length; i++)
    		if(iterativeSearch(array, keys[i]) == -1) return false;
    	return true;
    }

    public static boolean binaryContainsAll(Object[] array, Object[] keys) {
    	for(int i = 0; i < keys.length; i++)
    		if(Arrays.binarySearch(array, keys[i]) == -1) return false;
    	return true;
    }

    public static <T> T[] iterativeUnion(T[] array1, T[] array2) {
    	if(array1.length < array2.length) {
    		T[] tmp = array1;
    		array1 = array2;
    		array2 = tmp;
    	}
    	T[] adds = array2.clone();
    	int cnt = 0;
    	for(T elem : array2)
    		if(iterativeSearch(array1, elem) == -1) {
    			adds[cnt++] = elem;
    		}
    	if(cnt == 0) return array1;
    	T[] ret = createArray(array1, array1.length + cnt);
    	System.arraycopy(array1, 0, ret, 0, array1.length);
    	System.arraycopy(adds, 0, ret, array1.length, cnt);
    	return ret;
    }

	public static <T> T[] createArray(Class<T> componentType, int length) {
    	return (T[])Array.newInstance(componentType, length);
    }
    /*Apparently this causes compilation issues
    @SuppressWarnings("unchecked")
	public static <T> T[] createArray(int length) {
    	return (T[])new Object[length];
    }
    */
    public static <T> T[] createArray(T[] template, int length) {
    	return createArray(getComponentType(template), length);
    }

	public static <T> Class<T> getComponentType(T[] array) {
    	return (Class<T>)array.getClass().getComponentType();
    }

	public static Properties getSubProperties(Map<?, ?> props, String prefix) {
    	Properties subProps = new Properties();
		for(Map.Entry<?, ?> entry : props.entrySet()) {
			if(entry.getKey() != null && entry.getKey().toString().startsWith(prefix)) {
				subProps.put(entry.getKey().toString().substring(prefix.length()), entry.getValue());
			}
		}
		return subProps;
    }
	/**
	 * Spreads out the hash codes.
	 * Uses the same hash code spreader as most other java.util hash tables.
	 * @param x the hash of the object
	 * @return the spread hash code
	 */
	public static int spreadHash(int hash){
		hash += ~(hash << 9);
		hash ^=  (hash >>> 14);
		hash +=  (hash << 4);
		hash ^=  (hash >>> 10);
		return hash;
	}

	public static <K,V> Map<K,V> singleValueMap(final Set<K> keys, final V value) {
		return new AbstractLookupMap<K, V>() {
			/**
			 * @see simple.util.AbstractLookupMap#get(java.lang.Object)
			 */
			@Override
			public V get(Object key) {
				return keys.contains(key) ? value : null;
			}
			/**
			 * @see simple.util.AbstractLookupMap#keySet()
			 */
			@Override
			public Set<K> keySet() {
				return keys;
			}
		};
	}

	public static <K> Map<K, K> sameValueMap(final Set<K> keys) {
		return new Map<K, K>() {
			@SuppressWarnings("unchecked")
			@Override
			public K get(Object key) {
				return keys.contains(key) ? (K) key : null;
			}
			@Override
			public Set<K> keySet() {
				return keys;
			}
			@Override
			public int size() {
				return keys.size();
			}
			@Override
			public boolean isEmpty() {
				return keys.isEmpty();
			}
			@Override
			public boolean containsKey(Object key) {
				return keys.contains(key);
			}
			@Override
			public boolean containsValue(Object value) {
				return keys.contains(value);
			}
			@Override
			public K put(K key, K value) {
				if(key != value)
					throw new IllegalArgumentException("Key must equal value in this map");
				return keys.add(key) ? null : key;
			}

			@SuppressWarnings("unchecked")
			@Override
			public K remove(Object key) {
				return keys.remove(key) ? (K) key : null;
			}
			@Override
			public void putAll(Map<? extends K, ? extends K> m) {
				for(Map.Entry<? extends K, ? extends K> entry : m.entrySet())
					put(entry.getKey(), entry.getValue());
			}
			@Override
			public void clear() {
				keys.clear();
			}
			@Override
			public Collection<K> values() {
				return keys;
			}
			@Override
			public Set<Map.Entry<K, K>> entrySet() {
				if(entrySet == null) {
					entrySet = new AbstractSet<Entry<K,K>>() {
							@Override
							public Iterator<Entry<K, K>> iterator() {
								final Iterator<K> keyIter = keys.iterator();
								return new Iterator<Entry<K, K>>() {
									public boolean hasNext() {
										return keyIter.hasNext();
									}
									public Entry<K, K> next() {
										final K key = keyIter.next();
										return new Entry<K, K>() {
											public K getKey() {
												return key;
											}
											public K getValue() {
												return key;
											}
											public K setValue(K value) {
												throw new UnsupportedOperationException("Cannot change the values of a Same Value Map");					
											}						
										};
									}
									public void remove() {
										keyIter.remove();					
									}
								};
							}

							@Override
							public int size() {
								return keys.size();
							}		
						};
				}
				return entrySet;
			}
			protected Set<Entry<K,K>> entrySet;
		};
	}
	public static <E> Enumeration<E> singletonEnumeration(final E value) {
		return new Enumeration<E>() {
			protected boolean more = true;

			public boolean hasMoreElements() {
				return more;
			}

			public E nextElement() {
				if(!more)
					throw new NoSuchElementException("Only one element exists");
				more = false;
				return value;
			}
		};
	}
	/**
	 * @param prefix
	 * @return
	 */
	public static String lastWithPrefix(SortedSet<String> set, String prefix) {
		if(set.isEmpty())
			return null;
		if(prefix == null || prefix.length() == 0) {
			return set.first();
		}
		char[] ch = prefix.toCharArray();
		int i = ch.length - 1;
		for(; i >= 0; i--) {
			if(ch[i] != Character.MAX_VALUE) {
				ch[i] = (char)(ch[i] + 1);
				break;
			} else {
				ch[i] = Character.MIN_VALUE;
			}
		}
		SortedSet<String> prefixedSet;
		if(i < 0) {
			prefixedSet = set.tailSet(prefix);
		} else {
			prefixedSet = set.subSet(prefix, new String(ch));
		}
		return prefixedSet.isEmpty() ? null : prefixedSet.last();
	}
	/**
	 * @param set A set containing the prefixes
	 * @param value
	 * @return
	 */
	public static String bestPrefixMatch(Set<String> set, String value) {
		if(set.isEmpty())
			return null;
		if(set.contains(value))
			return value;
		if(value == null || value.length() == 0)
			return null;
		if(set instanceof NavigableSet<?>) {
			NavigableSet<String> navSet = (NavigableSet<String>)set;
			String closest = navSet.floor(value);
			while(closest != null) {
				if(value.startsWith(closest))
					return closest;
				if(closest.length() < value.length())
					break; // didn't find one
				closest = navSet.lower(closest);
			}
		} else {
			char[] ch = value.toCharArray();
			int i = ch.length - 1;
			for(; i >= 0; i--) {
				String v = new String(ch,0 , i);
				if(set.contains(v))
					return v;
			}
		}
		return null;
	}

	public static <V> Map.Entry<String, V> getBestMatch(NavigableMap<String, V> lookup, String key) {
		Map.Entry<String, V> closest = lookup.floorEntry(key);
		while(closest != null) {
			if(key.startsWith(closest.getKey()))
				return closest;
			if(closest.getKey().length() < key.length())
				break; // didn't find one
			closest = lookup.lowerEntry(closest.getKey());
		}
		return null;
	}

	public static <E> Set<E> asSet(E... values) {
		return new HashSet<E>(Arrays.asList(values));
	}

	public static <T> T[] filter(T[] orig, Decision<T> include) {
		EnhancedBitSet bs = new EnhancedBitSet(orig.length);
		int cnt = 0;
		for(int i = 0; i < orig.length; i++)
			if(include.decide(orig[i])) {
				bs.set(i);
				cnt++;
			}
		T[] filtered = createArray(orig, cnt);
		cnt = 0;
		for(int i = bs.nextSetBit(0); i >= 0; i = bs.nextSetBit(i + 1))
			filtered[cnt++] = orig[i];
		return filtered;
	}

	public static <K, V> int getDiffCount(Map<K, V> map1, Map<K, V> map2) {
		if(map1 == null)
			return (map2 == null ? 0 : map2.size());
		if(map2 == null)
			return map1.size();
		int cnt = 0;
		for(Map.Entry<K, V> entry : map1.entrySet()) {
			V v2 = map2.get(entry.getKey());
			if(v2 != null) {
				if(!ConvertUtils.areEqual(entry.getValue(), v2))
					cnt++;
			} else if(!map2.containsKey(entry.getKey()) || entry.getValue() != null)
				cnt++;
		}
		for(Map.Entry<K, V> entry : map2.entrySet()) {
			if(!map1.containsKey(entry.getKey()))
				cnt++;
		}
		return cnt;
	}

	public static <K, V> ConcurrentMap<K, V> synchronizedConcurrentMap(Map<K, V> m) {
		return new SynchronizedMap<K, V>(m);
	}

	// the following synchronized collections were copied from java.util.Collections and enhanced
	protected static class SynchronizedCollection<E> implements Collection<E>, Serializable {
		// use serialVersionUID from JDK 1.2.2 for interoperability
		private static final long serialVersionUID = 3053995032091335093L;

		final Collection<E> c; // Backing Collection
		final Object mutex; // Object on which to synchronize

		SynchronizedCollection(Collection<E> c) {
			if(c == null)
				throw new NullPointerException();
			this.c = c;
			mutex = this;
		}

		SynchronizedCollection(Collection<E> c, Object mutex) {
			this.c = c;
			this.mutex = mutex;
		}

		public int size() {
			synchronized(mutex) {
				return c.size();
			}
		}

		public boolean isEmpty() {
			synchronized(mutex) {
				return c.isEmpty();
			}
		}

		public boolean contains(Object o) {
			synchronized(mutex) {
				return c.contains(o);
			}
		}

		public Object[] toArray() {
			synchronized(mutex) {
				return c.toArray();
			}
		}

		public <T> T[] toArray(T[] a) {
			synchronized(mutex) {
				return c.toArray(a);
			}
		}

		public Iterator<E> iterator() {
			return c.iterator(); // Must be manually synched by user!
		}

		public boolean add(E e) {
			synchronized(mutex) {
				return c.add(e);
			}
		}

		public boolean remove(Object o) {
			synchronized(mutex) {
				return c.remove(o);
			}
		}

		public boolean containsAll(Collection<?> coll) {
			synchronized(mutex) {
				return c.containsAll(coll);
			}
		}

		public boolean addAll(Collection<? extends E> coll) {
			synchronized(mutex) {
				return c.addAll(coll);
			}
		}

		public boolean removeAll(Collection<?> coll) {
			synchronized(mutex) {
				return c.removeAll(coll);
			}
		}

		public boolean retainAll(Collection<?> coll) {
			synchronized(mutex) {
				return c.retainAll(coll);
			}
		}

		public void clear() {
			synchronized(mutex) {
				c.clear();
			}
		}

		public String toString() {
			synchronized(mutex) {
				return c.toString();
			}
		}

		private void writeObject(ObjectOutputStream s) throws IOException {
			synchronized(mutex) {
				s.defaultWriteObject();
			}
		}
	}


	protected static class SynchronizedSet<E> extends SynchronizedCollection<E> implements Set<E> {
		private static final long serialVersionUID = 487447009682186044L;

		SynchronizedSet(Set<E> s) {
			super(s);
		}

		SynchronizedSet(Set<E> s, Object mutex) {
			super(s, mutex);
		}

		public boolean equals(Object o) {
			if(this == o)
				return true;
			synchronized(mutex) {
				return c.equals(o);
			}
		}

		public int hashCode() {
			synchronized(mutex) {
				return c.hashCode();
			}
		}
	}

	protected static class SynchronizedMap<K, V> implements ConcurrentMap<K, V>, Serializable {
		private static final long serialVersionUID = 332428479659022715L;

		private final Map<K, V> m; // Backing Map
		final Object mutex; // Object on which to synchronize

		SynchronizedMap(Map<K, V> m) {
			if(m == null)
				throw new NullPointerException();
			this.m = m;
			mutex = this;
		}

		SynchronizedMap(Map<K, V> m, Object mutex) {
			this.m = m;
			this.mutex = mutex;
		}

		public int size() {
			synchronized(mutex) {
				return m.size();
			}
		}

		public boolean isEmpty() {
			synchronized(mutex) {
				return m.isEmpty();
			}
		}

		public boolean containsKey(Object key) {
			synchronized(mutex) {
				return m.containsKey(key);
			}
		}

		public boolean containsValue(Object value) {
			synchronized(mutex) {
				return m.containsValue(value);
			}
		}

		public V get(Object key) {
			synchronized(mutex) {
				return m.get(key);
			}
		}

		public V put(K key, V value) {
			synchronized(mutex) {
				return m.put(key, value);
			}
		}

		public V remove(Object key) {
			synchronized(mutex) {
				return m.remove(key);
			}
		}

		public void putAll(Map<? extends K, ? extends V> map) {
			synchronized(mutex) {
				m.putAll(map);
			}
		}

		public void clear() {
			synchronized(mutex) {
				m.clear();
			}
		}

		private transient Set<K> keySet = null;
		private transient Set<Map.Entry<K, V>> entrySet = null;
		private transient Collection<V> values = null;

		public Set<K> keySet() {
			synchronized(mutex) {
				if(keySet == null)
					keySet = new SynchronizedSet<K>(m.keySet(), mutex);
				return keySet;
			}
		}

		public Set<Map.Entry<K, V>> entrySet() {
			synchronized(mutex) {
				if(entrySet == null)
					entrySet = new SynchronizedSet<Map.Entry<K, V>>(m.entrySet(), mutex);
				return entrySet;
			}
		}

		public Collection<V> values() {
			synchronized(mutex) {
				if(values == null)
					values = new SynchronizedCollection<V>(m.values(), mutex);
				return values;
			}
		}

		public boolean equals(Object o) {
			if(this == o)
				return true;
			synchronized(mutex) {
				return m.equals(o);
			}
		}

		public int hashCode() {
			synchronized(mutex) {
				return m.hashCode();
			}
		}

		public String toString() {
			synchronized(mutex) {
				return m.toString();
			}
		}

		private void writeObject(ObjectOutputStream s) throws IOException {
			synchronized(mutex) {
				s.defaultWriteObject();
			}
		}

		@Override
		public V putIfAbsent(K key, V value) {
			synchronized(mutex) {
				if(!m.containsKey(key))
					return m.put(key, value);
				else
					return m.get(key);
			}
		}

		@Override
		public boolean remove(Object key, Object value) {
			synchronized(mutex) {
				if(m.containsKey(key) && m.get(key).equals(value)) {
					m.remove(key);
					return true;
				} else
					return false;
			}
		}

		@Override
		public boolean replace(K key, V oldValue, V newValue) {
			synchronized(mutex) {
				if(m.containsKey(key) && m.get(key).equals(oldValue)) {
					m.put(key, newValue);
					return true;
				} else
					return false;
			}
		}

		@Override
		public V replace(K key, V value) {
			synchronized(mutex) {
				if(m.containsKey(key)) {
					return m.put(key, value);
				} else
					return null;
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> int compare(T o1, T o2) {
		if(o1 == o2)
			return 0;
		if(o1 == null)
			return -1;
		if(o2 == null)
			return 1;
		if(o1 instanceof Comparable && o2 instanceof Comparable) {
			if(o1.getClass().isInstance(o2))
				return ((Comparable<T>) o1).compareTo(o2);
			if(o2.getClass().isInstance(o1))
				return -((Comparable<T>) o2).compareTo(o1);
		} else if(o1 instanceof Iterable && o2 instanceof Iterable)
			return compare((Iterable<?>) o1, (Iterable<?>) o2);
		if(!o1.getClass().isArray())
			return GENERIC_COMPARATOR.compare(o1, o2);
		if(o1 instanceof Object[] && o2 instanceof Object[])
			return compare((Object[]) o1, (Object[]) o2);
		if(o1 instanceof byte[] && o2 instanceof byte[])
			return compare((byte[]) o1, (byte[]) o2);
		if(o1 instanceof short[] && o2 instanceof short[])
			return compare((short[]) o1, (short[]) o2);
		if(o1 instanceof int[] && o2 instanceof int[])
			return compare((int[]) o1, (int[]) o2);
		if(o1 instanceof long[] && o2 instanceof long[])
			return compare((long[]) o1, (long[]) o2);
		if(o1 instanceof float[] && o2 instanceof float[])
			return compare((float[]) o1, (float[]) o2);
		if(o1 instanceof double[] && o2 instanceof double[])
			return compare((double[]) o1, (double[]) o2);
		if(o1 instanceof boolean[] && o2 instanceof boolean[])
			return compare((boolean[]) o1, (boolean[]) o2);

		return GENERIC_COMPARATOR.compare(o1, o2);
	}

	public static <T> int compare(Iterable<?> i1, Iterable<?> i2) {
		if(i1 == i2)
			return 0;
		if(i1 == null)
			return -1;
		if(i2 == null)
			return 1;
		return compare(i1.iterator(), i2.iterator());
	}

	public static <T> int compare(Iterator<?> i1, Iterator<?> i2) {
		if(i1 == i2)
			return 0;
		if(i1 == null)
			return -1;
		if(i2 == null)
			return 1;
		while(true) {
			if(!i1.hasNext()) {
				if(!i2.hasNext())
					return 0; // end of iterators
				return -1;
			}
			if(!i2.hasNext())
				return 1;
			Object v1 = i1.next();
			Object v2 = i2.next();
			if(v1 == v2)
				continue;
			if(v1 == null)
				return -1;
			if(v2 == null)
				return 1;
			int i = CollectionUtils.compare(v1, v2);
			if(i != 0)
				return i;
		}
	}

	public static <T> int compare(Object[] a1, Object[] a2) {
		if(a1 == a2)
			return 0;
		if(a1 == null)
			return -1;
		if(a2 == null)
			return 1;
		for(int k = 0; true; k++) {
			if(k >= a1.length) {
				if(k >= a2.length)
					return 0; // end of arrays
				if(a2[k] == null)
					continue;
				return -1;
			}
			if(k >= a2.length) {
				if(a1[k] == null)
					continue;
				return 1;
			}
			if(a1[k] == a2[k])
				continue;
			if(a1[k] == null)
				return -1;
			if(a2[k] == null)
				return 1;
			int i = compare(a1[k], a2[k]);
			if(i != 0)
				return i;
		}
	}

	public static <T> int compare(byte[] a1, byte[] a2) {
		if(a1 == a2)
			return 0;
		if(a1 == null)
			return -1;
		if(a2 == null)
			return 1;
		for(int k = 0; true; k++) {
			if(k >= a1.length) {
				if(k >= a2.length)
					return 0; // end of arrays
				return -1;
			}
			if(k >= a2.length)
				return 1;
			if(a1[k] < a2[k])
				return -1;
			if(a1[k] > a2[k])
				return 1;
		}
	}

	public static <T> int compare(short[] a1, short[] a2) {
		if(a1 == a2)
			return 0;
		if(a1 == null)
			return -1;
		if(a2 == null)
			return 1;
		for(int k = 0; true; k++) {
			if(k >= a1.length) {
				if(k >= a2.length)
					return 0; // end of arrays
				return -1;
			}
			if(k >= a2.length)
				return 1;
			if(a1[k] < a2[k])
				return -1;
			if(a1[k] > a2[k])
				return 1;
		}
	}

	public static <T> int compare(int[] a1, int[] a2) {
		if(a1 == a2)
			return 0;
		if(a1 == null)
			return -1;
		if(a2 == null)
			return 1;
		for(int k = 0; true; k++) {
			if(k >= a1.length) {
				if(k >= a2.length)
					return 0; // end of arrays
				return -1;
			}
			if(k >= a2.length)
				return 1;
			if(a1[k] < a2[k])
				return -1;
			if(a1[k] > a2[k])
				return 1;
		}
	}

	public static <T> int compare(long[] a1, long[] a2) {
		if(a1 == a2)
			return 0;
		if(a1 == null)
			return -1;
		if(a2 == null)
			return 1;
		for(int k = 0; true; k++) {
			if(k >= a1.length) {
				if(k >= a2.length)
					return 0; // end of arrays
				return -1;
			}
			if(k >= a2.length)
				return 1;
			if(a1[k] < a2[k])
				return -1;
			if(a1[k] > a2[k])
				return 1;
		}
	}

	public static <T> int compare(float[] a1, float[] a2) {
		if(a1 == a2)
			return 0;
		if(a1 == null)
			return -1;
		if(a2 == null)
			return 1;
		for(int k = 0; true; k++) {
			if(k >= a1.length) {
				if(k >= a2.length)
					return 0; // end of arrays
				return -1;
			}
			if(k >= a2.length)
				return 1;
			if(a1[k] < a2[k])
				return -1;
			if(a1[k] > a2[k])
				return 1;
		}
	}

	public static <T> int compare(double[] a1, double[] a2) {
		if(a1 == a2)
			return 0;
		if(a1 == null)
			return -1;
		if(a2 == null)
			return 1;
		for(int k = 0; true; k++) {
			if(k >= a1.length) {
				if(k >= a2.length)
					return 0; // end of arrays
				return -1;
			}
			if(k >= a2.length)
				return 1;
			if(a1[k] < a2[k])
				return -1;
			if(a1[k] > a2[k])
				return 1;
		}
	}

	public static <T> int compare(boolean[] a1, boolean[] a2) {
		if(a1 == a2)
			return 0;
		if(a1 == null)
			return -1;
		if(a2 == null)
			return 1;
		for(int k = 0; true; k++) {
			if(k >= a1.length) {
				if(k >= a2.length)
					return 0; // end of arrays
				return -1;
			}
			if(k >= a2.length)
				return 1;
			if(!a1[k] && a2[k])
				return -1;
			if(a1[k] && !a2[k])
				return 1;
		}
	}
	protected static final Comparator<Object> GENERIC_COMPARATOR = new ToStringComparator<>();

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> Comparator<? super T> getComparator(Class<T> clazz) {
		if(Comparable.class.isAssignableFrom(clazz))
			return null;
		if(Iterable.class.isAssignableFrom(clazz))
			return (Comparator<? super T>) new Comparator<Iterable>() {
				@Override
				public int compare(Iterable o1, Iterable o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};
		if(!clazz.isArray())
			return GENERIC_COMPARATOR;
		if(clazz.getComponentType().equals(Comparable.class))
			return (Comparator<? super T>) new Comparator<Comparable<?>[]>() {
				@Override
				public int compare(Comparable[] o1, Comparable[] o2) {
					if(o1 == o2)
						return 0;
					if(o1 == null)
						return -1;
					if(o2 == null)
						return 1;
					for(int k = 0; true; k++) {
						if(k >= o1.length) {
							if(k >= o2.length)
								return 0; // end of arrays
							if(o2[k] == null)
								continue;
							return -1;
						}
						if(k >= o2.length) {
							if(o1[k] == null)
								continue;
							return 1;
						}
						if(o1[k] == o2[k])
							continue;
						if(o1[k] == null)
							return -1;
						if(o2[k] == null)
							return 1;
						int i = o1[k].compareTo(o2[k]);
						if(i != 0)
							return i;
					}
				}
			};
		if(clazz.getComponentType().equals(Object.class)) {
			final Comparator<Object> componentComparator = (Comparator<Object>) getComparator(clazz.getComponentType());
			return (Comparator<? super T>) new Comparator<Object[]>() {
				@Override
				public int compare(Object[] o1, Object[] o2) {
					if(o1 == o2)
						return 0;
					if(o1 == null)
						return -1;
					if(o2 == null)
						return 1;
					for(int k = 0; true; k++) {
						if(k >= o1.length) {
							if(k >= o2.length)
								return 0; // end of arrays
							if(o2[k] == null)
								continue;
							return -1;
						}
						if(k >= o2.length) {
							if(o1[k] == null)
								continue;
							return 1;
						}
						if(o1[k] == o2[k])
							continue;
						if(o1[k] == null)
							return -1;
						if(o2[k] == null)
							return 1;
						int i = componentComparator.compare(o1[k], o2[k]);
						if(i != 0)
							return i;
					}
				}
			};
		}
		if(clazz.getComponentType().equals(byte.class))
			return (Comparator<? super T>) new Comparator<byte[]>() {
				@Override
				public int compare(byte[] o1, byte[] o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};
		if(clazz.getComponentType().equals(short.class))
			return (Comparator<? super T>) new Comparator<short[]>() {
				@Override
				public int compare(short[] o1, short[] o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};
		if(clazz.getComponentType().equals(int.class))
			return (Comparator<? super T>) new Comparator<int[]>() {
				@Override
				public int compare(int[] o1, int[] o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};
		if(clazz.getComponentType().equals(long.class))
			return (Comparator<? super T>) new Comparator<long[]>() {
				@Override
				public int compare(long[] o1, long[] o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};

		if(clazz.getComponentType().equals(char.class))
			return (Comparator<? super T>) new Comparator<char[]>() {
				@Override
				public int compare(char[] o1, char[] o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};
		if(clazz.getComponentType().equals(float.class))
			return (Comparator<? super T>) new Comparator<float[]>() {
				@Override
				public int compare(float[] o1, float[] o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};

		if(clazz.getComponentType().equals(double.class))
			return (Comparator<? super T>) new Comparator<double[]>() {
				@Override
				public int compare(double[] o1, double[] o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};
		if(clazz.getComponentType().equals(boolean.class))
			return (Comparator<? super T>) new Comparator<boolean[]>() {
				@Override
				public int compare(boolean[] o1, boolean[] o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};
		if(clazz.getComponentType().equals(Iterator.class))
			return (Comparator<? super T>) new Comparator<Iterator[]>() {
				@Override
				public int compare(Iterator[] o1, Iterator[] o2) {
					return CollectionUtils.compare(o1, o2);
				}
			};
		return GENERIC_COMPARATOR;
	}
}
