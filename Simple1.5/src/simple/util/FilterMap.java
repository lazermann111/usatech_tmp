/*
 * FilterMap.java
 *
 * Created on June 11, 2004, 12:22 PM
 */

package simple.util;

import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

/**
 * Map that only exposes the entries of its delegate that match the specified prefix and suffix
 * @author  Brian S. Krug
 */
public class FilterMap<V> extends AbstractMap<String,V> {
    protected final Map<String,V> map;
    protected final String prefix;
    protected final String suffix;
    protected transient Set<String> keySet;
    protected transient Set<Map.Entry<String,V>> entrySet;
    protected transient Collection<V> values;
    protected final boolean caseInsensitive;
    protected abstract class FilterIterator<E> implements Iterator<E> {
		private final Iterator<Map.Entry<String,V>> i = map.entrySet().iterator();
		private Map.Entry<String,V> next = null;
		public boolean hasNext() {
			if(next != null) return true;
			while(i.hasNext()) {
				Map.Entry<String,V> e = i.next();
				if(e.getKey() != null && keyMatches(e.getKey())) {							
					next = e;
					return true;
				}
			}
			return false;
		}

		public E next() {
			if(!hasNext()) 
				throw new java.util.NoSuchElementException();
			E entry = convertEntry(next);
			next = null;
			return entry;
		}

		protected abstract E convertEntry(Entry<String, V> entry) ;

		public void remove() {
			i.remove();
		}
	};

    /** Creates a new instance of FilterMap */
    @SuppressWarnings("unchecked")
	public FilterMap(Map<String,? extends V> delegate, String prefix, String suffix) {
        this.map =(Map<String,V>)delegate;
        this.prefix = (prefix == null ? "" : prefix);
        this.suffix = (suffix == null ? "" : suffix);       
        if(!(delegate instanceof SortedMap<?,?>))
        	this.caseInsensitive = false;
        else {
        	Comparator<? super String> comparator = ((SortedMap<String,? extends V>)delegate).comparator();
        	if(comparator == null)
        		this.caseInsensitive = false;
        	else if(comparator.compare("A", "a") == 0)
        		this.caseInsensitive = true;
        	else
        		this.caseInsensitive = false;
        }
    }

    @Override
	public V put(String key, V value) {
        return map.put(prefix + key + suffix, value);
    }

    @Override
    public V get(Object key) {
    	return map.get(prefix + key + suffix);
    }

    @Override
	public Collection<V> values() {
    	if(values == null)
    		values = new AbstractCollection<V>() {
	    		@Override
	    		public Iterator<V> iterator() {
	    			return new FilterIterator<V>() {
	    				@Override
	    				protected V convertEntry(Map.Entry<String, V> entry) {
	    					return entry.getValue();
	    				}
					};
	    		}
	    		@Override
    			public int size() {
    				return FilterMap.this.size();
    			}
			};
        return values;
    }

    @Override
	public int size() {
        int i = 0;
    	for(String key : map.keySet())
    		if(keyMatches(key))
    			i++;
    	return i;
    }

    @Override
	public void clear() {
    	entrySet().clear();
    }

    public V remove(String key) {
        return map.remove(prefix + key + suffix);
    }

    protected boolean keyMatches(String key) {
    	if(caseInsensitive)
			return key.regionMatches(true, 0, prefix, 0, prefix.length()) && key.regionMatches(true, key.length() - suffix.length(), suffix, 0, suffix.length());
		else
			return key.startsWith(prefix) && key.endsWith(suffix);
    }
    
    @Override
    public Set<String> keySet() {
    	if(keySet == null)
    		keySet = new AbstractSet<String>() {
	    		@Override
	    		public Iterator<String> iterator() {
	    			return new FilterIterator<String>() {
	    				@Override
	    				protected String convertEntry(Map.Entry<String, V> entry) {
	    					String key = entry.getKey();
							return key.substring(prefix.length(), key.length() - suffix.length());
	    				}
					};
	    		}
	    		
	    		@Override
				public int size() {
    				return FilterMap.this.size();
    			}

    			@Override
				public boolean contains(Object o) {
    				return FilterMap.this.containsKey(o);
    			}
    			@Override
    			public boolean remove(Object o) {
    				if(!(o instanceof String))
    					return false;
    				if(contains(o)) {
    					FilterMap.this.remove((String)o);
    					return true;
    				} else {
    					return false;
    				}
    			}
			};
    	return keySet;
    }
    @Override
	public Set<Map.Entry<String,V>> entrySet() {
    	if(entrySet == null) {
    		entrySet = new AbstractSet<Map.Entry<String,V>>() {
    			@Override
				public Iterator<Map.Entry<String,V>> iterator() {
    				return new FilterIterator<Map.Entry<String,V>>() {
    					@Override
						protected Map.Entry<String, V> convertEntry(final Map.Entry<String, V> entry) {
							return new Map.Entry<String,V>() {
    							public String getKey() {
    								String key = entry.getKey();
    								return key.substring(prefix.length(), key.length() - suffix.length());
    							}
    							public V getValue() { return entry.getValue(); }
    							public V setValue(V val) { return entry.setValue(val); }
    							@Override
								public boolean equals(Object o) {
    								if (!(o instanceof Map.Entry<?,?>)) return false;
    								Map.Entry<?,?> e1 = (Map.Entry<?,?>)o;
    								return eq(getKey(), e1.getKey()) &&  eq(getValue(), e1.getValue());
    							}
    							private boolean eq(Object o1, Object o2) {
    								return (o1 == null ? o2 == null : o1.equals(o2));
    							}
    							@Override
								public int hashCode() {
    								Object k = getKey();
    								Object v = getValue();
    								return (k == null ? 0 : k.hashCode()) ^ (v == null ? 0 : v.hashCode());
    							}
    						};
						}
    				};
    			}

    			@Override
				public int size() {
    				return FilterMap.this.size();
    			}

    			@Override
				public boolean contains(Object o) {
    				if (!(o instanceof Map.Entry<?,?>)) return false;
    				Map.Entry<?,?> e = (Map.Entry<?,?>) o;
    				Object val = FilterMap.this.get(e.getKey());
    				if(val == null) return e.getValue() == null && FilterMap.this.containsKey(e.getKey());
    				else return e.getValue() != null && val.equals(e.getValue());
    			}
    		};
    	}
    	return entrySet;
    }

    @Override
	public boolean isEmpty() {
        return map.isEmpty() || size() == 0;
    }

    @Override
	public boolean containsValue(Object value) {
        return values().contains(value);
    }

    @Override
	public boolean containsKey(Object key) {
    	if(!(key instanceof String)) 
    		return false;
		return map.containsKey(prefix + key + suffix);
    }
}
