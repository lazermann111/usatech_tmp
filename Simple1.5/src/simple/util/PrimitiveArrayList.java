package simple.util;

/**
 * Provides a List interface around an array of primitive types such as int[].
 * The get() and set() methods expect objects of the wrapper class for the primitive
 * array supplied in the constructor.
 *
 * @author Brian S. Krug
 */
public class PrimitiveArrayList extends java.util.AbstractList<Object> {
	protected Object arr = null;
/**
 * PrimitiveArrayList constructor comment.
 */
public PrimitiveArrayList(Object array) {
	super();
	if(!array.getClass().isArray()) throw new IllegalArgumentException("Argument must be an array");
	arr = array;
}
/**
 * get method comment.
 */
public Object get(int index) {
	return java.lang.reflect.Array.get(arr,index);
}
/**
 * set method comment.
 */
public Object set(int index, Object element) {
	Object prev = get(index);
	java.lang.reflect.Array.set(arr,index, element);
	return prev;
}
/**
 * size method comment.
 */
public int size() {
	return java.lang.reflect.Array.getLength(arr);
}
}
