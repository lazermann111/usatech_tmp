/*
 * Created on Sep 8, 2005
 *
 */
package simple.util;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

public class ArrayComparable implements Comparable {
    protected Comparable[] array;
    private boolean nullsFirst;
    public ArrayComparable(Comparable[] array) {
        super();
        this.array = array;
    }

    public int compareTo(Object o) {
        Comparable[] other;
        if(o instanceof ArrayComparable) {
            other = ((ArrayComparable)o).array; 
        } else try {
            other = ConvertUtils.convert(array.getClass(), o);
        } catch (ConvertException e) {
            throw new ClassCastException("Cannot convert " + o + " to " + array.getClass().getName());
        }
        if(other == array) return 0;
        int d = array.length - other.length;
        if(d != 0) return getNullCompareValue() * d;
        for(int k = 0; k < array.length; k++) {
            if(array[k] == null) {
                if(other[k] != null) return -getNullCompareValue();
            } else if(other[k] == null) return getNullCompareValue();
            d = array[k].compareTo(other[k]);
            if(d != 0) return d;
        }
        return 0;
    }

    protected int getNullCompareValue() {
        return nullsFirst ? 1 : -1;
    }
}
