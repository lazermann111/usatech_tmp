/*
 * BranchSet.java
 *
 * Created on February 25, 2004, 9:45 AM
 */
package simple.util;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Stores arrays in a tree-like fashion and allows retrieval of matches on the most significant branches. Thus,
 * if the Set has the entries:
 * <ul><li>{"abc", "def", "ghi", "jkl"}</li><li>{"abc", "def", "123", "456"}</li><li>{"abc", "456"}</li>
 * <li>{"abc"}</li>
 * </ul>
 * If the findMatch() method is called with the parameter {"abc", "def"}, the first two entries will be returned.
 *
 * @author  Brian S. Krug
 */
public class BranchSet<E> extends AbstractSet<E[]> implements Set<E[]> {
	protected static interface BranchMap<B> extends Map<B, BranchMap<B>> {
    }
	protected BranchMap<E> trunk = createBranchMap();
    protected boolean reverse = false;
    protected int size = 0;
    protected final Class<E> componentType;

    public BranchSet(Class<E> componentType) {
        this(false, componentType);
    }

    public BranchSet(boolean reverse, Class<E> componentType) {
        this.reverse = reverse;
        this.componentType = componentType;
    }

    protected BranchMap<E> createBranchMap() {
    	class BranchHashMap extends HashMap<E,BranchMap<E>> implements BranchMap<E> {
    		private static final long serialVersionUID = 6256641058695090870L;
    	}
        return new BranchHashMap();
    }

    protected Collection<E[]> createMatchCollection() {
        return new HashSet<E[]>();
    }

    @Override
	public boolean add(E[] arr) {
    	BranchMap<E> map = trunk;
        boolean changed = false;
        for(int i = 0; i < arr.length; i++) {
            E current = arr[reverse ? arr.length-i-1 : i];
            if(map.containsKey(current)) {
                map = map.get(current);
            } else {
            	BranchMap<E> newMap = createBranchMap();
                map.put(current, newMap);
                changed = true;
                map = newMap;
            }
        }
        map.put(null, null); // mark this as terminal
        if(changed) size++;
        return changed;
    }

    @Override
	public void clear() {
        trunk.clear();
        size = 0;
    }

    @Override
	public boolean contains(Object o) {
        Object[] arr;
        if(o instanceof Object[]) arr = (Object[])o;
        else arr = new Object[] {o};
        BranchMap<E> map = trunk;
        for(int i = 0; i < arr.length; i++) {
            Object current = arr[reverse ? arr.length-i-1 : i];
            if(map.containsKey(current)) {
                map = map.get(current);
            } else {
                return false;
            }
        }
        return map.containsKey(null);
    }

    @Override
	public Iterator<E[]> iterator() {
        return new LeafIterator();
    }

    @Override
	public boolean remove(Object o) {
        Object[] arr;
        if(o instanceof Object[]) arr = (Object[])o;
        else arr = new Object[] {o};
        BranchMap<E> map = trunk;
        for(int i = 0; i < arr.length; i++) {
            Object current = arr[reverse ? arr.length-i-1 : i];
            if(map.containsKey(current)) {
                map = map.get(current);
            } else {
                return false;
            }
        }
        if(map.containsKey(null)) {
            map.remove(null);
            size--;
            return true;
        } else {
            return false;
        }
    }

    @Override
	public int size() {
        return size;
    }

//XXX: would be better to return an iterator that found next match & next leaf lazily

    public Collection<E[]> findMatches(E[] arr) {
        BranchMap<E> map = trunk;
        Collection<E[]> matches = createMatchCollection();
        for(int i = 0; i < arr.length; i++) {
            E current = arr[reverse ? arr.length-i-1 : i];
            if(map.containsKey(current)) {
                map = map.get(current);
            } else {
                return matches;
            }
        }
        collectLeaves(matches, map, arr);
        return matches;
    }

    protected void collectLeaves(Collection<E[]> leaves, BranchMap<E> branch, E[] base) {
        for( Map.Entry<E, BranchMap<E>> entry : branch.entrySet()) {
            if(entry.getKey() == null) {
                leaves.add(base);
            } else {
            	BranchMap<E> map = entry.getValue();
                E[] tmp;
                if(base == null) {
                	tmp = CollectionUtils.createArray(componentType, 1);
                	tmp[0] = entry.getKey();
                } else {
                	tmp = CollectionUtils.createArray(componentType, base.length+1);
                    System.arraycopy(base, 0, tmp, (reverse ? 1 : 0), base.length);
                    tmp[reverse ? 0 : base.length] = entry.getKey();
                }
                collectLeaves(leaves, map, tmp);
            }
        }
    }

    protected class LeafIterator implements Iterator<E[]> {
        protected Iterator<E[]> leaves;
        protected E[] last;

        public LeafIterator() {
            Collection<E[]> collection = createMatchCollection();
            collectLeaves(collection, trunk, null);
            leaves = collection.iterator();
        }
        public boolean hasNext() {
            return leaves.hasNext();
        }

        public  E[] next() {
            last = leaves.next();
            return last;
        }

        public void remove() {
            if(last == null) throw new IllegalStateException("Must call next() before calling remove()");
            BranchSet.this.remove(last);
            last = null;
        }
    }
}

