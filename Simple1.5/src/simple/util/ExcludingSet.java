package simple.util;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class ExcludingSet<E> extends AbstractSet<E> {
	protected final Set<E> delegate;
	protected final Collection<E> excluded;

	public ExcludingSet(Set<E> delegate, Collection<E> excluded) {
		super();
		this.delegate = delegate;
		this.excluded = excluded;
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private final Iterator<E> i = delegate.iterator();
			private E next = null;

			public boolean hasNext() {
				if(next != null)
					return true;
				while(i.hasNext()) {
					E e = i.next();
					if(!excluded.contains(e)) {
						next = e;
						return true;
					}
				}
				return false;
			}

			public E next() {
				if(!hasNext())
					throw new java.util.NoSuchElementException();
				E e = next;
				next = null;
				return e;
			}

			public void remove() {
				if(next != null)
					throw new IllegalStateException("remove called twice or after hasNext");
				i.remove();
				next = null;
			}
		};
	}

	@Override
	public int size() {
		int size = delegate.size();
		for(E e : excluded)
			if(delegate.contains(e))
				size--;
		return size;
	}

	public boolean add(E e) {
		if(excluded.contains(e))
			return false;
		return delegate.add(e);
	}

	@Override
	public boolean contains(Object o) {
		return delegate.contains(o) && !excluded.contains(o);
	}

	@Override
	public boolean isEmpty() {
		return delegate.isEmpty() || size() == 0;
	}

	@Override
	public boolean remove(Object o) {
		if(excluded.contains(o))
			return false;
		return delegate.remove(o);
	}

}
