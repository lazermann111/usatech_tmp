/*
 * CaseInsensitiveComparator.java
 *
 * Created on July 19, 2002, 4:21 PM
 */

package simple.util;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class CaseInsensitiveComparator<E> implements java.util.Comparator<E> {
	
	/** Creates new CaseInsensitiveComparator */
	public CaseInsensitiveComparator() {
	}
	
	public int compare(E o1, E o2) {
        String s1 = (o1 == null ? null : o1.toString());
        String s2 = (o2 == null ? null : o2.toString());
	    if(s1 == null) {
	        if(s2 == null) return 0;
	        else return -1;
	    } else if(s2 == null) {
	        return 1;
	    } else {
	        return s1.compareToIgnoreCase(s2);
	    }
	}
}