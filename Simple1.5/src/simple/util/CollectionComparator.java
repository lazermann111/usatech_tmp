/*
 * CollectionComparator.java
 *
 * Created on January 6, 2004, 12:44 PM
 */

package simple.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Compares arrays, iterators, or collections using an iterator and comparing each element in the
 * order that it was returned from the iterator. Thus, unless the iterators of the objects to be compared
 * always return their elements in the same order, this Comparator should NOT be used 
 * as the results will be unpredictable.
 *
 * @author  Brian S. Krug
 */
public class CollectionComparator<T> implements Comparator<T> {   
    /** Holds value of property nullsFirst. */
    private boolean nullsFirst;
    
    /** Holds value of property itemComparator. */
    private Comparator itemComparator;
    
    protected static Comparator<Comparable> defaultComparator = new Comparator<Comparable>() {
        @SuppressWarnings("unchecked")
		public int compare(Comparable o1, Comparable o2) {
            return o1.compareTo(o2);
        }
    }; 
    
    /** Creates a new instance of CollectionComparator */
    public CollectionComparator() {
        this(false, null);
    }
        
    /** Creates a new instance of CollectionComparator */
    public CollectionComparator(boolean nullsFirst) {
        this(nullsFirst, null);
    }

    /** Creates a new instance of CollectionComparator */
    public CollectionComparator(boolean nullsFirst, Comparator itemComparator) {
        this.nullsFirst = nullsFirst;
        this.itemComparator = itemComparator;
    }


    /** Getter for property nullsFirst.
     * @return Value of property nullsFirst.
     *
     */
    public boolean isNullsFirst() {
        return this.nullsFirst;
    }
    
    /** Setter for property nullsFirst.
     * @param nullsFirst New value of property nullsFirst.
     *
     */
    public void setNullsFirst(boolean nullsFirst) {
        this.nullsFirst = nullsFirst;
    }
    
    /** Getter for property itemComparator.
     * @return Value of property itemComparator.
     *
     */
    public Comparator getItemComparator() {
        return this.itemComparator;
    }
    
    protected Comparator getNonNullItemComparator() {
        Comparator c = getItemComparator();
        if(c != null) return c;
        return defaultComparator;
    }
    
    /** Setter for property itemComparator.
     * @param itemComparator New value of property itemComparator.
     *
     */
    public void setItemComparator(Comparator itemComparator) {
        this.itemComparator = itemComparator;
    }
    
    public int compare(Object o1, Object o2) {
        Iterator i1 = toIterator(o1);
        Iterator i2 = toIterator(o2);        
        return compare(i1, i2);
    }
    
    @SuppressWarnings("unchecked")
	public int compare(Iterator i1, Iterator i2) {
        Comparator comp = getNonNullItemComparator();
        //check each in order
        while(i1.hasNext() || i2.hasNext()) {
            Object o1 = (i1.hasNext() ? i1.next() : null);
            Object o2 = (i2.hasNext() ? i2.next() : null);           
            if(o1 == null) {
                if(o2 == null) return 0;
                else return -getNullCompareValue();
            } else if(o2 == null) return getNullCompareValue();
            int k = comp.compare(o1, o2);
            if(k != 0) return k;
        }
        return 0;
    }

    protected Iterator toIterator(Object o) {
        if(o == null) return java.util.Collections.EMPTY_LIST.iterator();
        else if(o instanceof Iterator) return ((Iterator)o);
        else if(o instanceof Object[]) o = java.util.Arrays.asList((Object[])o);
        else if(o.getClass().isArray()) o = new PrimitiveArrayList(o);
        return ((Collection)o).iterator(); // this may cause a class cast exception        
    }
    
    public int getNullCompareValue() {
        return nullsFirst ? 1 : -1;
    }

}
