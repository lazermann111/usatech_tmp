package simple.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

/** This set is similiar to java.util.Collections.emptySet(), except that it does not throw an error
 * on modification operations, but silently does nothing. It returns <code>true</code> from the add() operations
 * as if the set was modified, so that it is consistent with contains().
 * @author bkrug
 *
 * @param <E>
 */
public class BlackholeSet<E> implements Set<E> {
	protected final Object[] EMPTY_ARRAY = new Object[0];
	protected Iterator<E> iterator = new Iterator<E>() {
        public boolean hasNext() { return false; }
        public E next() { throw new NoSuchElementException(); }
        public void remove() { throw new IllegalStateException(); }
    };
	public boolean add(E e) {
		return true;
	}
	public boolean addAll(Collection<? extends E> c) {
		return !c.isEmpty();
	}
	public void clear() {
	}
	public boolean contains(Object o) {
		return false;
	}
	public boolean containsAll(Collection<?> c) {
		return c.isEmpty();
	}
	public boolean isEmpty() {
		return true;
	}
	public Iterator<E> iterator() {
		return iterator;
	}
	public boolean remove(Object o) {
		return false;
	}
	public boolean removeAll(Collection<?> c) {
		return false;
	}
	public boolean retainAll(Collection<?> c) {
		return false;
	}
	public int size() {
		return 0;
	}
	public Object[] toArray() {
		return EMPTY_ARRAY;
	}
	public <T> T[] toArray(T[] a) {
		if (a.length > 0)
            a[0] = null;
        return a;
	}
}
