/**
 *
 */
package simple.util;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class SingletonListIterator<E> implements ListIterator<E> {
	protected E singleton;
	protected boolean atEnd = false;
	protected boolean forward = true;
	public SingletonListIterator(E singleton) {
		super();
		this.singleton = singleton;
	}
	public void add(E o) {
		throw new UnsupportedOperationException("Not modifiable");
	}
	public boolean hasNext() {
		return !atEnd;
	}
	public boolean hasPrevious() {
		return atEnd;
	}
	public E next() {
		if(!hasNext())
			throw new NoSuchElementException("At end");
		atEnd = true;
		forward = true;
		return singleton;
	}
	public int nextIndex() {
		return atEnd ? 1 : 0;
	}
	public E previous() {
		if(!hasPrevious())
			throw new NoSuchElementException("At beginning");
		atEnd = false;
		forward = false;
		return singleton;
	}
	public int previousIndex() {
		return atEnd ? 0 : -1;
	}
	public void remove() {
		throw new UnsupportedOperationException("Not modifiable");
	}
	public void set(E o) {
		if(atEnd == forward) singleton = o;
	}
}