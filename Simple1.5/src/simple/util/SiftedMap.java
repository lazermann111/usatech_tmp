/*
 * FilterMap.java
 *
 * Created on June 11, 2004, 12:22 PM
 */

package simple.util;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Filters out specified entries of the underlying map.
 * 
 * @author Brian S. Krug
 */
public abstract class SiftedMap<K, V> extends AbstractMap<K, V> {
	protected Map<K, V> map;
	protected Set<Map.Entry<K, V>> entrySet;

	public SiftedMap(Map<K, V> delegate) {
		this.map = delegate;
	}

	protected abstract boolean siftKey(Object key, boolean update);

	@Override
	public V put(K key, V value) {
		if(siftKey(key, true))
			return null;
		return map.put(key, value);
	}

	@Override
	public V get(Object key) {
		if(siftKey(key, false))
			return null;
		return map.get(key);
	}

	@Override
	public V remove(Object key) {
		if(siftKey(key, true))
			return null;
		return map.remove(key);
	}

	@Override
	public java.util.Set<Map.Entry<K, V>> entrySet() {
		if(entrySet == null) {
			entrySet = new AbstractSet<Map.Entry<K, V>>() {
				@Override
				public Iterator<Map.Entry<K, V>> iterator() {
					return new Iterator<Map.Entry<K, V>>() {
						private final Iterator<Map.Entry<K, V>> i = map.entrySet().iterator();
						private Map.Entry<K, V> next = null;
						private K prevKey = null;

						public boolean hasNext() {
							if(next != null)
								return true;
							while(i.hasNext()) {
								Map.Entry<K, V> e = i.next();
								if(!siftKey(e.getKey(), false)) {
									next = e;
									return true;
								}
							}
							return false;
						}

						public Map.Entry<K, V> next() {
							if(!hasNext())
								throw new java.util.NoSuchElementException();
							Map.Entry<K, V> o = new Map.Entry<K, V>() {
								private final Map.Entry<K, V> e = next;

								public K getKey() {
									return e.getKey();
								}

								public V getValue() {
									return e.getValue();
								}

								public V setValue(V val) {
									if(siftKey(e.getKey(), true))
										return e.getValue();
									return e.setValue(val);
								}

								@Override
								public boolean equals(Object o) {
									if(!(o instanceof Map.Entry))
										return false;
									Map.Entry<?, ?> e1 = (Map.Entry<?, ?>) o;
									return eq(getKey(), e1.getKey()) && eq(getValue(), e1.getValue());
								}

								private boolean eq(Object o1, Object o2) {
									return (o1 == null ? o2 == null : o1.equals(o2));
								}

								@Override
								public int hashCode() {
									Object k = getKey();
									Object v = getValue();
									return (k == null ? 0 : k.hashCode()) ^ (v == null ? 0 : v.hashCode());
								}
							};
							prevKey = next.getKey();
							next = null;
							return o;
						}

						public void remove() {
							if(siftKey(prevKey, true))
								return;
							if(next != null) {
								// this will cause ConcurrentModificationException on most Map implements of the delegate, but we've no choice
								map.remove(prevKey);
							} else
								i.remove();
						}
					};
				}

				@Override
				public boolean contains(Object o) {
					if(!(o instanceof Map.Entry))
						return false;
					Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
					if(siftKey(e.getKey(), false))
						return false;
					Object val = SiftedMap.this.get(e.getKey());
					if(val == null)
						return e.getValue() == null && SiftedMap.this.containsKey(e.getKey());
					else
						return e.getValue() != null && val.equals(e.getValue());
				}

				@Override
				public int size() {
					return SiftedMap.this.size();
				}
			};
		}
		return entrySet;
	}

	@Override
	public boolean containsKey(Object key) {
		return !siftKey(key, false) && map.containsKey(key);
	}

	@Override
	public int size() {
		int i = 0;
		for(K key : map.keySet())
			if(!siftKey(key, false))
				i++;
		return i;
	}
}
