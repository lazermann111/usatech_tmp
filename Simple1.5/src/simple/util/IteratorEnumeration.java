/*
 * IteratorEnumeration.java
 *
 * Created on November 1, 2001, 10:59 AM
 */

package simple.util;

import java.util.Iterator;
/** Turns an iterator into an enumerator.
 * @author Brian S. Krug
 */
public class IteratorEnumeration<E> implements java.util.Enumeration<E> {
	protected Iterator<E> iter = null;
        /** Creates new IteratorEnumeration */
	public IteratorEnumeration(Iterator<E> iter) {
                this.iter = iter;
        }
        
        /**
         * @return
         */        
        public boolean hasMoreElements() {
                return iter.hasNext();
        }
        
        /**
         * @return
         */        
	public E nextElement() {
                return iter.next();
        }
        
}
