/*
 * Created on Jun 15, 2005
 *
 */
package simple.util;

import java.util.AbstractCollection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

public class StaticCollection<E> extends AbstractCollection<E> implements Collection<E> {
    protected E[] elements;
    protected Comparator<? super E> comparator;

    protected StaticCollection(E[] elements) {
        this(elements, null);
    }

    protected StaticCollection(E[] elements, Comparator<? super E> comparator) {
        this.elements = elements.clone();
        if(comparator == null && !Comparable.class.isAssignableFrom(elements.getClass().getComponentType()))
            throw new IllegalArgumentException("Elements must implement java.util.Comparable, unless a comparator is provided");
        this.comparator = comparator;
        Arrays.sort(this.elements, comparator);
    }

    /** Creates an unmodifable Collected backed by the given array. This static method is
     * used to ensure that elements are comparable.
     *
     * @param <T>
     * @param elements
     * @return
     */
    public static <T extends Comparable<?>> StaticCollection<T> create(T... elements) {
        return new StaticCollection<T>(elements);
    }


    /** Creates an unmodifable Collected backed by the given array.
     *
     * @param <T>
     * @param elements
     * @param comparator
     * @return
     */
    public static <T> StaticCollection<T> create(Comparator<? super T> comparator, T... elements) {
        return new StaticCollection<T>(elements, comparator);
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            protected int index = 0;
            public boolean hasNext() {
                return index < elements.length;
            }
            public E next() {
                return elements[index++];
            }
            public void remove() {
                throw new UnsupportedOperationException("StaticCollection is immutable");
            }
        };
    }

    @Override
    public int size() {
        return elements.length;
    }

    @SuppressWarnings("unchecked")
	@Override
    public boolean contains(Object o) {
        return Arrays.binarySearch(elements, (E)o, comparator) >= 0;
    }

    @Override
    public Object[] toArray() {
        return elements.clone();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("StaticCollection is immutable");
    }

}
