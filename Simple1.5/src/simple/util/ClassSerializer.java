package simple.util;

import java.io.*;

/**
 * Util class to serialize and deserialize Serializable objects to/from bytes
 *
 * @author dlozenko
 */
public final class ClassSerializer<T extends Serializable> {
    public ClassSerializer() {
    }

    public byte[] toBytes(T object) {
        if (object == null) {
            return null;
        }
        ByteArrayOutputStream bos = null;
        ObjectOutput out = null;
        try {
            bos = new ByteArrayOutputStream();
            out = new ObjectOutputStream(bos);
            out.writeObject(object);
            return bos.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public T fromBytes(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(new ByteArrayInputStream(bytes));
            return (T) in.readObject();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
