package simple.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class CompositeList<E> extends java.util.AbstractList<E> {
	protected List<List<? extends E>> lists = new ArrayList<List<? extends E>>();

	/** Creates a new instance of CompositeList */
	public CompositeList() {
		super();
	}

	/** Creates a new instance of CompositeSet */
	public CompositeList(List<? extends E>... containedLists) {
		super();
		for(int i = 0; i < containedLists.length; i++)
			merge(containedLists[i]);
	}

	public void merge(List<? extends E> list) {
		lists.add(list);
	}

	public void unmerge(List<?> list) {
		for(Iterator<List<? extends E>> iter = lists.iterator(); iter.hasNext();) {
			Collection<?> containedList = iter.next();
			if(containedList == list)
				iter.remove(); // only way to do this is to remove the EXACT same object as passed b/c if sets contain identical items they are "equal" via the equals() method
		}
	}

	public boolean add(E o) {
		throw new UnsupportedOperationException("Add is unsupported; use merge() to merge another set into this composite");
	}

	public boolean addAll(Collection<? extends E> c) {
		throw new UnsupportedOperationException("Add is unsupported; use merge() to merge another set into this composite");
	}

	public void clear() {
		throw new UnsupportedOperationException("Clear is unsupported; use unmerge() to remove a set from this composite");
	}

	public boolean contains(Object o) {
		for(List<? extends E> list : lists) {
			if(list.contains(o))
				return true;
		}
		return false;
	}

	public boolean isEmpty() {
		for(List<? extends E> list : lists) {
			if(!list.isEmpty())
				return false;
		}
		return true;
	}

	protected class CompositeIterator implements ListIterator<E> {
		protected final ListIterator<List<? extends E>> listsIter = lists.listIterator();
		protected ListIterator<? extends E> entryIter;
		protected E nextEntry;
		protected E prevEntry;
		protected boolean hasNext;
		protected boolean hasPrev;
		protected int index = 0;

		protected CompositeIterator() {
			entryIter = listsIter.next().listIterator();
			hasNext = true;
			hasPrev = false;
			moveNext();
		}

		public boolean hasNext() {
			return hasNext;
		}

		public E next() {
			if(!hasNext())
				throw new java.util.NoSuchElementException();
			E next = nextEntry;
			moveNext();
			return next;
		}

		public void remove() {
			entryIter.remove();
		}

		public void add(E e) {
			CollectionUtils.uncheckedIterator(entryIter, Object.class).add(e);
		}

		@Override
		public boolean hasPrevious() {
			return hasPrev;
		}

		@Override
		public int nextIndex() {
			return index;
		}

		@Override
		public E previous() {
			if(!hasPrevious())
				throw new java.util.NoSuchElementException();
			E prev = prevEntry;
			movePrev();
			return prev;
		}

		@Override
		public int previousIndex() {
			return index - 1;
		}

		public void set(E e) {
			CollectionUtils.uncheckedIterator(entryIter, Object.class).set(e);
		}

		protected void moveNext() {
			while(hasNext) {
				if(entryIter.hasNext()) {
					prevEntry = nextEntry;
					hasPrev = true;
					nextEntry = entryIter.next();
					index++;
					break;
				} else if(listsIter.hasNext()) {
					entryIter = listsIter.next().listIterator();
				} else {
					// done!
					if(hasPrev)
						prevEntry = nextEntry;
					hasNext = false;
					nextEntry = null;
				}
			}
		}

		protected void movePrev() {
			while(hasPrev) {
				if(entryIter.hasPrevious()) {
					nextEntry = prevEntry;
					hasNext = true;
					prevEntry = entryIter.previous();
					index--;
					break;
				} else if(listsIter.hasPrevious()) {
					entryIter = listsIter.previous().listIterator();
				} else {
					// done!
					if(hasPrev)
						nextEntry = prevEntry;
					hasPrev = false;
					prevEntry = null;
				}
			}
		}
	}

	public Iterator<E> iterator() {
		return new CompositeIterator();
	}

	@Override
	public ListIterator<E> listIterator() {
		return new CompositeIterator();
	}

	public boolean remove(Object o) {
		throw new UnsupportedOperationException("Remove is unsupported; use unmerge() to remove a set from this composite");
	}

	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("RemoveAll is unsupported; use unmerge() to remove a set from this composite");
	}

	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("RetainAll is unsupported; use unmerge() to remove a set from this composite");
	}

	public int size() {
		int size = 0;
		for(List<? extends E> list : lists) {
			size += list.size();
		}
		return size;
	}

	@Override
	public E get(int index) {
		int total = 0;
		for(List<? extends E> list : lists) {
			int size = list.size();
			if(index < total + size)
				return list.get(index - total);
			total += size;
		}
		throw new IndexOutOfBoundsException("Index " + index + " is greater than the size " + total);
	}
}
