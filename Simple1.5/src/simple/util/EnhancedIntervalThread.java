/*
 * IntervalThread.java
 *
 * Created on January 14, 2004, 12:14 PM
 */

package simple.util;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author  Brian S. Krug
 */
public class EnhancedIntervalThread extends Thread {
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition signal = lock.newCondition();
    protected boolean done = false;
    protected boolean paused = false;
    protected long interval = 0;
    protected final Runnable runnable;
    
    public EnhancedIntervalThread(ThreadGroup group, String name, int priority, boolean daemon, Runnable runnable, long interval) {
        super(group, name);
        setPriority(priority);
        setDaemon(daemon);
        setInterval(interval);
        this.runnable = runnable;
    }
    
    public EnhancedIntervalThread(String name, Runnable runnable) {
        this(null, name, 3, true, runnable, 0);
    }
    
    public void run() {
	    while(!done) {
	    	lock.lock();
	    	try {
	    		do {
		    		try {
		    	    	if(paused || interval == 0)
			    			signal.await();
			    		else
			    			signal.await(interval, TimeUnit.MILLISECONDS);
		            } catch(InterruptedException e) {
		            }
		            if(done) {
		                done = false; // for next time
		                return;
		            }
	    		} while(paused) ;
            } finally {
        		lock.unlock();
        	}
            if(runnable != null)
            	runnable.run();
        }
    }
    
    /** Stops the interval from occurring again. Once this is called start() must be called to
     *  resume execution. To disable this IntervalThread temporarily use setInterval with 0 as
     *  the interval.
     */
    public void finish() {
    	lock.lock();
    	try {
	        done = true;
	        signal.signal();
    	} finally {
    		lock.unlock();
    	}
    }
    /** Sets the number of milleseconds between each call to the runnable. Use 0 to
     *  disable this IntervalThread (i.e. - to wait indefinitely).
     *
     */
    public void setInterval(long interval) {
	 	lock.lock();
    	try {
    		long old = this.interval;
            this.interval = interval;
            if((old > interval || (old == 0 && interval > 0)) && isAlive())
            	signal.signal();
    	} finally {
    		lock.unlock();
    	}
    }
    
    public void trigger() {
    	lock.lock();
    	try {
	        signal.signal();
    	} finally {
    		lock.unlock();
    	}
    }
    
    public void pause() {
    	lock.lock();
    	try {
    		paused = true;
	    } finally {
    		lock.unlock();
    	}
    }
    
    public void unpause() {
    	lock.lock();
    	try {
    		paused = false;
	        signal.signal();
    	} finally {
    		lock.unlock();
    	}
    }
    /** Getter for property interval.
     * @return Value of property interval.
     *
     */
    public long getInterval() {
        return interval;
    } 
}
