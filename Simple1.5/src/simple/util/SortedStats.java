package simple.util;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.RandomAccess;

/**
 * Holds stats on various entities and then provides List interface to a sorted copy of those. Only the List methods are thread-safe.
 * All other methods must be accessed only by one thread at a time. Warning: get(size()-1) may throw ArrayIndexOutOfBoundsException if sort has been called
 * between the call to size() and the call to get(). Using listIterator() or iterator() is a thread-safe way to look through the entities because it uses a
 * copy of the sorted data at the time of creation.
 * 
 * @author bkrug
 * 
 * @param <K>
 *            The entity class
 * @param <V>
 *            The statistic value class
 */
public class SortedStats<K, V> extends AbstractList<K> implements List<K>, RandomAccess {
	protected final Map<K, V> entries;
	protected volatile Map.Entry<K, V>[] sorted = CollectionUtils.genericize(new Map.Entry[0]);
	protected transient Comparator<Map.Entry<K, V>> naturalComparator;

	protected class SortedListIterator implements ListIterator<K> {
		protected final Map.Entry<K, V>[] array;
		protected int current;
		protected boolean forward = true;

		public SortedListIterator(Map.Entry<K, V>[] array, int current) {
			this.array = array;
			this.current = current;
		}

		public void add(K o) {
			throw new UnsupportedOperationException("Not modifiable");
		}

		public boolean hasNext() {
			return current < array.length;
		}

		public boolean hasPrevious() {
			return current > 0;
		}

		public K next() {
			if(!hasNext())
				throw new NoSuchElementException("At end");
			forward = true;
			return array[current++ % array.length].getKey();
		}

		public int nextIndex() {
			return current;
		}

		public K previous() {
			if(!hasPrevious())
				throw new NoSuchElementException("At beginning");
			forward = false;
			return array[--current % array.length].getKey();
		}

		public int previousIndex() {
			return current - 1;
		}

		public void remove() {
			throw new UnsupportedOperationException("Not modifiable");
		}

		public void set(K o) {
			throw new UnsupportedOperationException("Not modifiable");
		}
	}

	public SortedStats() {
		this(new HashMap<K, V>());
	}

	public SortedStats(Map<K, V> entries) {
		this.entries = entries;
	}

	public V putStat(K key, V value) {
		return entries.put(key, value);
	}

	public void clearCache() {
		entries.clear();
	}

	public void sortStats() {
		sortStats(null);
	}

	public void sortStats(final Comparator<? super V> comparator) {
		final Comparator<Map.Entry<K, V>> entryComparator;
		if(comparator == null) {
			if(naturalComparator == null)
				naturalComparator = new Comparator<Map.Entry<K, V>>() {
					@SuppressWarnings({ "rawtypes", "unchecked" })
					public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
						return ((Comparable) o1.getValue()).compareTo(o2.getValue());
					}
				};
			entryComparator = naturalComparator;
		} else {
			entryComparator = new Comparator<Map.Entry<K, V>>() {
				public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
					return comparator.compare(o1.getValue(), o2.getValue());
				}
			};
		}
		Map.Entry<K, V>[] unsorted = CollectionUtils.createArray(sorted, entries.size());
		int index = 0;
		for(Map.Entry<K, V> entry : entries.entrySet()) {
			if(unsorted.length <= index) {
				int newsize = Math.max(entries.size(), index + 1);
				Map.Entry<K, V>[] tmp = unsorted;
				unsorted = CollectionUtils.createArray(sorted, newsize);
				System.arraycopy(tmp, 0, unsorted, 0, tmp.length);
			}
			unsorted[index++] = entry;
		}
		if(index != unsorted.length) {
			Map.Entry<K, V>[] tmp = unsorted;
			unsorted = CollectionUtils.createArray(sorted, index);
			System.arraycopy(tmp, 0, unsorted, 0, index);
		}
		Arrays.sort(unsorted, entryComparator);
		sorted = unsorted;
	}

	public Iterator<K> iterator() {
		return listIterator();
	}

	public ListIterator<K> listIterator(int index) {
		return new SortedListIterator(sorted, index);
	}

	public int size() {
		return sorted.length;
	}

	@Override
	public K get(int index) {
		return sorted[index].getKey();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		Map.Entry<K, V>[] sorted = this.sorted; // so it doesn't change on us
		for(Map.Entry<K, V> entry : sorted) {
			if(sb.length() > 0)
				sb.append(", ");
			sb.append(entry.getKey()).append(" (").append(entry.getValue()).append(')');
		}
		return sb.toString();
	}
}
