/*
 * FilterMap.java
 *
 * Created on June 11, 2004, 12:22 PM
 */

package simple.util;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Filters out specified entries of the underlying set.
 * 
 * @author Brian S. Krug
 */
public abstract class SiftedSet<E> extends AbstractSet<E> {
	protected Set<E> delegate;
	protected final boolean siftUpdates;

	public SiftedSet(Set<E> delegate, boolean siftUpdates) {
		this.delegate = delegate;
		this.siftUpdates = siftUpdates;
	}

	protected abstract boolean sift(Object element);
	
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private final Iterator<E> i = delegate.iterator();
			private E next = null;
			private E prev = null;
			
			public boolean hasNext() {
				if(next != null)
					return true;
				while(i.hasNext()) {
					E e = i.next();
					if(!sift(e)) {
						next = e;
						return true;
					}
				}
				return false;
			}

			public E next() {
				if(!hasNext())
					throw new java.util.NoSuchElementException();
				E curr = next;
				prev = next;
				next = null;
				return curr;
			}

			public void remove() {
				if(prev == null)
					throw new IllegalStateException();
				// this will cause ConcurrentModificationException on most implements of the delegate, but we've no choice
				delegate.remove(prev);
				prev = null;
			}
		};
	}

	@Override
	public boolean add(E e) {
		if(siftUpdates && sift(e))
			return false;
		return delegate.add(e);
	}


	@Override
	public boolean remove(Object o) {
		if(siftUpdates && sift(o))
			return false;
		return delegate.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean modified = false;
		for(Iterator<?> i = c.iterator(); i.hasNext();)
			modified |= remove(i.next());
		return modified;
	}

	@Override
	public boolean contains(Object o) {
		return !sift(o) && delegate.contains(o);
	}

	@Override
	public int size() {
		int i = 0;
		for(E e : delegate)
			if(!sift(e))
				i++;
		return i;
	}

	@Override
	public boolean isEmpty() {
		for(E e : delegate)
			if(!sift(e))
				return false;
		return true;
	}
}
