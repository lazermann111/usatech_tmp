package simple.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public abstract class ConversionList<E0, E1> extends ConversionCollection<E0, E1> implements List<E1> {
	protected final List<E0> listDelegate;

	protected class ConversionListIterator implements ListIterator<E1> {
		protected final ListIterator<E0> delegateIter;

		public ConversionListIterator(int index) {
			delegateIter = listDelegate.listIterator(index);
		}

		public boolean hasNext() {
			return delegateIter.hasNext();
		}

		public E1 next() {
			return convertFrom(delegateIter.next());
		}

		public void remove() {
			delegateIter.remove();
		}

		@Override
		public boolean hasPrevious() {
			return delegateIter.hasPrevious();
		}

		@Override
		public E1 previous() {
			return convertFrom(delegateIter.previous());
		}

		@Override
		public int nextIndex() {
			return delegateIter.nextIndex();
		}

		@Override
		public int previousIndex() {
			return delegateIter.previousIndex();
		}

		@Override
		public void set(E1 e) {
			if(!isReversible())
				throw new UnsupportedOperationException("Not reversible");
			delegateIter.set(convertTo(e));
		}

		@Override
		public void add(E1 e) {
			if(!isReversible())
				throw new UnsupportedOperationException("Not reversible");
			delegateIter.add(convertTo(e));
		}
	}

	protected class ConversionSubList extends ConversionList<E0, E1> {
		public ConversionSubList(int fromIndex, int toIndex) {
			super(ConversionList.this.listDelegate.subList(fromIndex, toIndex));
		}

		@Override
		protected E0 convertTo(E1 object1) {
			return ConversionList.this.convertTo(object1);
		}

		@Override
		protected E1 convertFrom(E0 object0) {
			return ConversionList.this.convertFrom(object0);
		}

		@Override
		protected boolean isReversible() {
			return ConversionList.this.isReversible();
		}
	}

	public ConversionList(List<E0> delegate) {
		super(delegate);
		this.listDelegate = delegate;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
		    return true;

		if(!(o instanceof List))
		    return false;
		Collection<?> c = (Collection<?>) o;
		if (c.size() != size())
		    return false;
        try {
			return c.equals(this);
        } catch(ClassCastException unused)   {
            return false;
        } catch(NullPointerException unused) {
            return false;
        }
    }

	@Override
	public boolean addAll(int index, Collection<? extends E1> c) {
		if(!isReversible())
			throw new UnsupportedOperationException("Not reversible");
		boolean modified = false;
		Iterator<? extends E1> e = c.iterator();
		while(e.hasNext()) {
			add(index++, e.next());
			modified = true;
		}
		return modified;
	}

	@Override
	public E1 get(int index) {
		return convertFrom(listDelegate.get(index));
	}

	@Override
	public E1 set(int index, E1 element) {
		if(!isReversible())
			throw new UnsupportedOperationException("Not reversible");
		return convertFrom(listDelegate.set(index, convertTo(element)));
	}

	@Override
	public void add(int index, E1 element) {
		if(!isReversible())
			throw new UnsupportedOperationException("Not reversible");
		listDelegate.add(index, convertTo(element));
	}

	@Override
	public E1 remove(int index) {
		if(!isReversible())
			throw new UnsupportedOperationException("Not reversible");
		return convertFrom(listDelegate.remove(index));
	}

	@SuppressWarnings("unchecked")
	@Override
	public int indexOf(Object o) {
		if(!isReversible())
			throw new UnsupportedOperationException("Not reversible");
		try {
			return listDelegate.indexOf(convertTo((E1) o));
		} catch(ClassCastException unused) {
			return -1;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public int lastIndexOf(Object o) {
		if(!isReversible())
			throw new UnsupportedOperationException("Not reversible");
		try {
			return listDelegate.lastIndexOf(convertTo((E1) o));
		} catch(ClassCastException unused) {
			return -1;
		}
	}

	@Override
	public ListIterator<E1> listIterator() {
		return listIterator(0);
	}

	@Override
	public ListIterator<E1> listIterator(int index) {
		return new ConversionListIterator(index);
	}

	@Override
	public List<E1> subList(int fromIndex, int toIndex) {
		return new ConversionSubList(fromIndex, toIndex);
	}
}
