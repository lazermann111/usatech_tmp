package simple.util;

import java.util.Comparator;

public class ToStringComparator<T> implements Comparator<T> {

	@Override
	public int compare(T o1, T o2) {
		if(o1 == null) {
			if(o2 == null)
				return 0;
			return -1;
		}
		if(o2 == null)
			return 1;
		String s1 = o1.toString();
		String s2 = o2.toString();
		if(s1 == null) {
			if(s2 == null)
				return 0;
			return -1;
		}
		if(s2 == null)
			return 1;
		return s1.compareTo(s2);
	}

}
