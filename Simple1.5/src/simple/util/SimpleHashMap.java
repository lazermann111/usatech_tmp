package simple.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * This is an adaptation of java.util.HashMap that allows for easy sub-classing.
 *
 * @author Brian Krug
 */

public class SimpleHashMap<K, V> extends AbstractMap<K, V> implements Map<K, V>, Cloneable, Serializable {

	/**
	 * The default initial capacity - MUST be a power of two.
	 */
	protected static final int DEFAULT_INITIAL_CAPACITY = 16;

	/**
	 * The maximum capacity, used if a higher value is implicitly specified by
	 * either of the constructors with arguments. MUST be a power of two <= 1<<30.
	 */
	protected static final int MAXIMUM_CAPACITY = 1 << 30;

	/**
	 * The load factor used when none specified in constructor.
	 */
	protected static final float DEFAULT_LOAD_FACTOR = 0.75f;

	/**
	 * The table, resized as necessary. Length MUST Always be a power of two.
	 */
	protected transient Entry<K, V>[] table;

	/**
	 * The number of key-value mappings contained in this identity hash map.
	 */
	protected transient int size;

	/**
	 * The next size value at which to resize (capacity * load factor).
	 *
	 * @serial
	 */
	protected int threshold;

	/**
	 * The load factor for the hash table.
	 *
	 * @serial
	 */
	protected final float loadFactor;

	/**
	 * The number of times this HashMap has been structurally modified
	 * Structural modifications are those that change the number of mappings in
	 * the HashMap or otherwise modify its internal structure (e.g., rehash).
	 * This field is used to make iterators on Collection-views of the HashMap
	 * fail-fast. (See ConcurrentModificationException).
	 */
	protected transient volatile int modCount;

	protected Collection<V> values;
	protected Set<K> keySet;

	/**
	 * Constructs an empty <tt>HashMap</tt> with the specified initial
	 * capacity and load factor.
	 *
	 * @param initialCapacity
	 *            The initial capacity.
	 * @param loadFactor
	 *            The load factor.
	 * @throws IllegalArgumentException
	 *             if the initial capacity is negative or the load factor is
	 *             nonpositive.
	 */
	public SimpleHashMap(int initialCapacity, float loadFactor) {
		if(initialCapacity < 0)
			throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
		if(initialCapacity > MAXIMUM_CAPACITY)
			initialCapacity = MAXIMUM_CAPACITY;
		if(loadFactor <= 0 || Float.isNaN(loadFactor))
			throw new IllegalArgumentException("Illegal load factor: " + loadFactor);

		// Find a power of 2 >= initialCapacity
		int capacity = 1;
		while(capacity < initialCapacity)
			capacity <<= 1;

		this.loadFactor = loadFactor;
		threshold = (int) (capacity * loadFactor);
		table = CollectionUtils.genericize(new Entry[capacity]);
		init();
	}

	/**
	 * Constructs an empty <tt>HashMap</tt> with the specified initial
	 * capacity and the default load factor (0.75).
	 *
	 * @param initialCapacity
	 *            the initial capacity.
	 * @throws IllegalArgumentException
	 *             if the initial capacity is negative.
	 */
	public SimpleHashMap(int initialCapacity) {
		this(initialCapacity, DEFAULT_LOAD_FACTOR);
	}

	/**
	 * Constructs an empty <tt>HashMap</tt> with the default initial capacity
	 * (16) and the default load factor (0.75).
	 */
	public SimpleHashMap() {
		this.loadFactor = DEFAULT_LOAD_FACTOR;
		threshold = (int) (DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);
		table = CollectionUtils.genericize(new Entry[DEFAULT_INITIAL_CAPACITY]);
		init();
	}

	/**
	 * Constructs a new <tt>HashMap</tt> with the same mappings as the
	 * specified <tt>Map</tt>. The <tt>HashMap</tt> is created with default
	 * load factor (0.75) and an initial capacity sufficient to hold the
	 * mappings in the specified <tt>Map</tt>.
	 *
	 * @param m
	 *            the map whose mappings are to be placed in this map.
	 * @throws NullPointerException
	 *             if the specified map is null.
	 */
	public SimpleHashMap(Map<? extends K, ? extends V> m) {
		this(Math.max((int) (m.size() / DEFAULT_LOAD_FACTOR) + 1, DEFAULT_INITIAL_CAPACITY), DEFAULT_LOAD_FACTOR);
		putAllForCreate(m);
	}

	// internal utilities

	/**
	 * Initialization hook for subclasses. This method is called in all
	 * constructors and pseudo-constructors (clone, readObject) after HashMap
	 * has been initialized but before any entries have been inserted. (In the
	 * absence of this method, readObject would require explicit knowledge of
	 * subclasses.)
	 */
	protected void init() {
	}

	/**
	 * Returns a hash value for the specified object. In addition to the
	 * object's own hashCode, this method applies a "supplemental hash
	 * function," which defends against poor quality hash functions. This is
	 * critical because HashMap uses power-of two length hash tables.
	 * <p>
	 *
	 * The shift distances in this function were chosen as the result of an
	 * automated search over the entire four-dimensional search space.
	 */
	protected int hash(K x) {
		return secondaryHash(CollectionUtils.deepHashCode(x));
	}
	
	protected static int secondaryHash(int hash) {
		// This function ensures that hashCodes that differ only by
		// constant multiples at each bit position have a bounded
		// number of collisions (approximately 8 at default load factor).
		hash ^= (hash >>> 20) ^ (hash >>> 12);
		return hash ^ (hash >>> 7) ^ (hash >>> 4);
	}

	/**
	 * Check for equality of non-null reference x and possibly-null y.
	 */
	protected boolean keyEquals(K key1, K key2) {
		return CollectionUtils.deepEquals(key1, key2);
	}

	/**
	 * Returns index for hash code h.
	 */
	protected static int indexFor(int h, int length) {
		return h & (length - 1);
	}

	/**
	 * Returns the number of key-value mappings in this map.
	 *
	 * @return the number of key-value mappings in this map.
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Returns <tt>true</tt> if this map contains no key-value mappings.
	 *
	 * @return <tt>true</tt> if this map contains no key-value mappings.
	 */
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@SuppressWarnings("unchecked")
	protected K castKey(Object key) {
		return (K) key;
	}

	@SuppressWarnings("unchecked")
	protected V castValue(Object value) {
		return (V) value;
	}

	/**
	 * Returns the value to which the specified key is mapped in this identity
	 * hash map, or <tt>null</tt> if the map contains no mapping for this key.
	 * A return value of <tt>null</tt> does not <i>necessarily</i> indicate
	 * that the map contains no mapping for the key; it is also possible that
	 * the map explicitly maps the key to <tt>null</tt>. The
	 * <tt>containsKey</tt> method may be used to distinguish these two cases.
	 *
	 * @param key
	 *            the key whose associated value is to be returned.
	 * @return the value to which this map maps the specified key, or
	 *         <tt>null</tt> if the map contains no mapping for this key.
	 * @see #put(Object, Object)
	 */
	@Override
	public V get(Object key) {
		Entry<K, V> e = getEntry(key);
		return e == null ? null : e.value;
	}

	/**
	 * Returns <tt>true</tt> if this map contains a mapping for the specified
	 * key.
	 *
	 * @param key
	 *            The key whose presence in this map is to be tested
	 * @return <tt>true</tt> if this map contains a mapping for the specified
	 *         key.
	 */
	@Override
	public boolean containsKey(Object key) {
		return getEntry(key) != null;
	}

	/**
	 * Returns the entry associated with the specified key in the HashMap.
	 * Returns null if the HashMap contains no mapping for this key.
	 */
	protected Entry<K, V> getEntry(Object key) {
		K k = castKey(key);
		int hash = hash(k);
		int i = indexFor(hash, table.length);
		Entry<K, V> e = table[i];
		while(e != null && !(e.hash == hash && keyEquals(k, e.key)))
			e = e.next;
		return e;
	}

	/**
	 * Associates the specified value with the specified key in this map. If the
	 * map previously contained a mapping for this key, the old value is
	 * replaced.
	 *
	 * @param key
	 *            key with which the specified value is to be associated.
	 * @param value
	 *            value to be associated with the specified key.
	 * @return previous value associated with specified key, or <tt>null</tt>
	 *         if there was no mapping for key. A <tt>null</tt> return can
	 *         also indicate that the HashMap previously associated
	 *         <tt>null</tt> with the specified key.
	 */
	@Override
	public V put(K key, V value) {
		int hash = hash(key);
		int i = indexFor(hash, table.length);
		Entry<K, V> e = table[i];
		while(e != null && !(e.hash == hash && keyEquals(key, e.key)))
			e = e.next;
		if(e != null) {
			V oldValue = e.value;
			e.value = value;
			e.recordAccess(this);
			return oldValue;
		} else {
			modCount++;
			addEntry(hash, key, value, i);
			return null;
		}
	}

	/**
	 * This method is used instead of put by constructors and pseudoconstructors
	 * (clone, readObject). It does not resize the table, check for
	 * comodification, etc. It calls createEntry rather than addEntry.
	 */
	protected void putForCreate(K key, V value) {
		int hash = hash(key);
		int i = indexFor(hash, table.length);
		Entry<K, V> e = table[i];

		/**
		 * Look for preexisting entry for key. This will never happen for clone
		 * or deserialize. It will only happen for construction if the input Map
		 * is a sorted map whose ordering is inconsistent w/ equals.
		 */
		for(; e != null; e = e.next)
			if(e.hash == hash && keyEquals(key, e.key))
				return;
		createEntry(hash, key, value, i);
	}

	protected void putAllForCreate(Map<? extends K, ? extends V> m) {
		for(Iterator<? extends Map.Entry<? extends K, ? extends V>> i = m.entrySet().iterator(); i
				.hasNext();) {
			Map.Entry<? extends K, ? extends V> e = i.next();
			putForCreate(e.getKey(), e.getValue());
		}
	}

	/**
	 * Rehashes the contents of this map into a new array with a larger
	 * capacity. This method is called automatically when the number of keys in
	 * this map reaches its threshold.
	 *
	 * If current capacity is MAXIMUM_CAPACITY, this method does not resize the
	 * map, but sets threshold to Integer.MAX_VALUE. This has the effect of
	 * preventing future calls.
	 *
	 * @param newCapacity
	 *            the new capacity, MUST be a power of two; must be greater than
	 *            current capacity unless current capacity is MAXIMUM_CAPACITY
	 *            (in which case value is irrelevant).
	 */
	protected void resize(int newCapacity) {
		Entry<?,?>[] oldTable = table;
		int oldCapacity = oldTable.length;
		if(oldCapacity == MAXIMUM_CAPACITY) {
			threshold = Integer.MAX_VALUE;
			return;
		}

		Entry<K, V>[] newTable = CollectionUtils.genericize(new Entry[newCapacity]);
		transfer(newTable);
		table = newTable;
		threshold = (int) (newCapacity * loadFactor);
	}

	/**
	 * Transfer all entries from current table to newTable.
	 */
	protected void transfer(Entry<K, V>[] newTable) {
		Entry<K, V>[] src = table;
		int newCapacity = newTable.length;
		for(int j = 0; j < src.length; j++) {
			Entry<K, V> e = src[j];
			if(e != null) {
				src[j] = null;
				do {
					Entry<K, V> next = e.next;
					int i = indexFor(e.hash, newCapacity);
					e.next = newTable[i];
					newTable[i] = e;
					e = next;
				} while(e != null);
			}
		}
	}

	/**
	 * Copies all of the mappings from the specified map to this map These
	 * mappings will replace any mappings that this map had for any of the keys
	 * currently in the specified map.
	 *
	 * @param m
	 *            mappings to be stored in this map.
	 * @throws NullPointerException
	 *             if the specified map is null.
	 */
	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		int numKeysToBeAdded = m.size();
		if(numKeysToBeAdded == 0)
			return;

		/*
		 * Expand the map if the map if the number of mappings to be added is
		 * greater than or equal to threshold. This is conservative; the obvious
		 * condition is (m.size() + size) >= threshold, but this condition could
		 * result in a map with twice the appropriate capacity, if the keys to
		 * be added overlap with the keys already in this map. By using the
		 * conservative calculation, we subject ourself to at most one extra
		 * resize.
		 */
		if(numKeysToBeAdded > threshold) {
			int targetCapacity = (int) (numKeysToBeAdded / loadFactor + 1);
			if(targetCapacity > MAXIMUM_CAPACITY)
				targetCapacity = MAXIMUM_CAPACITY;
			int newCapacity = table.length;
			while(newCapacity < targetCapacity)
				newCapacity <<= 1;
			if(newCapacity > table.length)
				resize(newCapacity);
		}

		for(Iterator<? extends Map.Entry<? extends K, ? extends V>> i = m.entrySet().iterator(); i
				.hasNext();) {
			Map.Entry<? extends K, ? extends V> e = i.next();
			put(e.getKey(), e.getValue());
		}
	}

	/**
	 * Removes the mapping for this key from this map if present.
	 *
	 * @param key
	 *            key whose mapping is to be removed from the map.
	 * @return previous value associated with specified key, or <tt>null</tt>
	 *         if there was no mapping for key. A <tt>null</tt> return can
	 *         also indicate that the map previously associated <tt>null</tt>
	 *         with the specified key.
	 */
	@Override
	public V remove(Object key) {
		Entry<K, V> e = removeEntryForKey(key);
		return (e == null ? null : e.value);
	}

	/**
	 * Removes and returns the entry associated with the specified key in the
	 * HashMap. Returns null if the HashMap contains no mapping for this key.
	 */
	protected Entry<K, V> removeEntryForKey(Object key) {
		K k = castKey(key);
		int hash = hash(k);
		int i = indexFor(hash, table.length);
		Entry<K, V> prev = table[i];
		Entry<K, V> e = prev;

		while(e != null) {
			Entry<K, V> next = e.next;
			if(e.hash == hash && keyEquals(k, e.key)) {
				modCount++;
				size--;
				if(prev == e)
					table[i] = next;
				else
					prev.next = next;
				e.recordRemoval(this);
				return e;
			}
			prev = e;
			e = next;
		}

		return e;
	}

	/**
	 * Special version of remove for EntrySet.
	 */
	protected Entry<K, V> removeMapping(Object o) {
		if(!(o instanceof Map.Entry<?,?>))
			return null;
		return removeEntryForKey(((Map.Entry<?,?>) o).getKey());
	}

	/**
	 * Removes all mappings from this map.
	 */
	@Override
	public void clear() {
		modCount++;
		Entry<?,?>[] tab = table;
		for(int i = 0; i < tab.length; i++)
			tab[i] = null;
		size = 0;
	}

	/**
	 * Returns <tt>true</tt> if this map maps one or more keys to the
	 * specified value.
	 *
	 * @param value
	 *            value whose presence in this map is to be tested.
	 * @return <tt>true</tt> if this map maps one or more keys to the
	 *         specified value.
	 */
	@Override
	public boolean containsValue(Object value) {
		if(value == null)
			return containsNullValue();

		Entry<?,?>[] tab = table;
		for(int i = 0; i < tab.length; i++)
			for(Entry<?,?> e = tab[i]; e != null; e = e.next)
				if(value.equals(e.value))
					return true;
		return false;
	}

	/**
	 * Special-case code for containsValue with null argument
	 */
	private boolean containsNullValue() {
		Entry<?,?>[] tab = table;
		for(int i = 0; i < tab.length; i++)
			for(Entry<?,?> e = tab[i]; e != null; e = e.next)
				if(e.value == null)
					return true;
		return false;
	}

	/**
	 * Returns a shallow copy of this <tt>HashMap</tt> instance: the keys and
	 * values themselves are not cloned.
	 *
	 * @return a shallow copy of this map.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object clone() {
		SimpleHashMap<K, V> result = null;
		try {
			result = (SimpleHashMap<K, V>) super.clone();
		} catch(CloneNotSupportedException e) {
			// assert false;
		}
		result.table = new Entry[table.length];
		result.entrySet = null;
		result.modCount = 0;
		result.size = 0;
		result.init();
		result.putAllForCreate(this);

		return result;
	}

	protected static class Entry<K, V> implements Map.Entry<K, V> {
		protected final K key;
		protected V value;
		protected final int hash;
		protected Entry<K, V> next;

		/**
		 * Create new entry.
		 */
		protected Entry(int h, K k, V v, Entry<K, V> n) {
			value = v;
			next = n;
			key = k;
			hash = h;
		}

		public K getKey() {
			return key;
		}

		public V getValue() {
			return value;
		}

		public V setValue(V newValue) {
			V oldValue = value;
			value = newValue;
			return oldValue;
		}

		@Override
		public boolean equals(Object o) {
			if(!(o instanceof Map.Entry<?,?>))
				return false;
			Map.Entry<?,?> e = (Map.Entry<?,?>) o;
			Object k1 = getKey();
			Object k2 = e.getKey();
			if(k1 == k2 || (k1 != null && k1.equals(k2))) {
				Object v1 = getValue();
				Object v2 = e.getValue();
				if(v1 == v2 || (v1 != null && v1.equals(v2)))
					return true;
			}
			return false;
		}

		@Override
		public int hashCode() {
			return (key == null ? 0 : key.hashCode()) ^ (value == null ? 0 : value.hashCode());
		}

		@Override
		public String toString() {
			return getKey() + "=" + getValue();
		}

		/**
		 * This method is invoked whenever the value in an entry is overwritten
		 * by an invocation of put(k,v) for a key k that's already in the
		 * HashMap.
		 */
		protected void recordAccess(SimpleHashMap<K, V> m) {
		}

		/**
		 * This method is invoked whenever the entry is removed from the table.
		 */
		protected void recordRemoval(SimpleHashMap<K, V> m) {
		}
	}

	/**
	 * Add a new entry with the specified key, value and hash code to the
	 * specified bucket. It is the responsibility of this method to resize the
	 * table if appropriate.
	 *
	 * Subclass overrides this to alter the behavior of put method.
	 */
	protected void addEntry(int hash, K key, V value, int bucketIndex) {
		Entry<K, V> e = table[bucketIndex];
		table[bucketIndex] = new Entry<K, V>(hash, key, value, e);
		if(size++ >= threshold)
			resize(2 * table.length);
	}

	/**
	 * Like addEntry except that this version is used when creating entries as
	 * part of Map construction or "pseudo-construction" (cloning,
	 * deserialization). This version needn't worry about resizing the table.
	 *
	 * Subclass overrides this to alter the behavior of HashMap(Map), clone, and
	 * readObject.
	 */
	protected void createEntry(int hash, K key, V value, int bucketIndex) {
		Entry<K, V> e = table[bucketIndex];
		table[bucketIndex] = new Entry<K, V>(hash, key, value, e);
		size++;
	}

	protected abstract class HashIterator<E> implements Iterator<E> {
		protected Entry<K, V> next; // next entry to return
		protected int expectedModCount; // For fast-fail
		protected int index; // current slot
		protected Entry<K, V> current; // current entry

		protected HashIterator() {
			expectedModCount = modCount;
			Entry<K, V>[] t = table;
			int i = t.length;
			Entry<K, V> n = null;
			if(size != 0) { // advance to first entry
				while(i > 0 && (n = t[--i]) == null)
					;
			}
			next = n;
			index = i;
		}

		public boolean hasNext() {
			return next != null;
		}

		protected Entry<K, V> nextEntry() {
			if(modCount != expectedModCount)
				throw new ConcurrentModificationException();
			Entry<K, V> e = next;
			if(e == null)
				throw new NoSuchElementException();

			Entry<K, V> n = e.next;
			Entry<K, V>[] t = table;
			int i = index;
			while(n == null && i > 0)
				n = t[--i];
			index = i;
			next = n;
			return current = e;
		}

		public void remove() {
			if(current == null)
				throw new IllegalStateException();
			if(modCount != expectedModCount)
				throw new ConcurrentModificationException();
			K k = current.key;
			current = null;
			SimpleHashMap.this.removeEntryForKey(k);
			expectedModCount = modCount;
		}

	}

	protected class ValueIterator extends HashIterator<V> {
		public V next() {
			return nextEntry().value;
		}
	}

	protected class KeyIterator extends HashIterator<K> {
		public K next() {
			return nextEntry().getKey();
		}
	}

	protected class EntryIterator extends HashIterator<Map.Entry<K, V>> {
		public Map.Entry<K, V> next() {
			return nextEntry();
		}
	}

	// Subclass overrides these to alter behavior of views' iterator() method
	protected Iterator<K> newKeyIterator() {
		return new KeyIterator();
	}

	protected Iterator<V> newValueIterator() {
		return new ValueIterator();
	}

	protected Iterator<Map.Entry<K, V>> newEntryIterator() {
		return new EntryIterator();
	}

	// Views

	protected transient Set<Map.Entry<K, V>> entrySet = null;

	/**
	 * Returns a set view of the keys contained in this map. The set is backed
	 * by the map, so changes to the map are reflected in the set, and
	 * vice-versa. The set supports element removal, which removes the
	 * corresponding mapping from this map, via the <tt>Iterator.remove</tt>,
	 * <tt>Set.remove</tt>, <tt>removeAll</tt>, <tt>retainAll</tt>, and
	 * <tt>clear</tt> operations. It does not support the <tt>add</tt> or
	 * <tt>addAll</tt> operations.
	 *
	 * @return a set view of the keys contained in this map.
	 */
	@Override
	public Set<K> keySet() {
		Set<K> ks = keySet;
		return (ks != null ? ks : (keySet = new KeySet()));
	}

	protected class KeySet extends AbstractSet<K> {
		@Override
		public Iterator<K> iterator() {
			return newKeyIterator();
		}

		@Override
		public int size() {
			return size;
		}

		@Override
		public boolean contains(Object o) {
			return containsKey(o);
		}

		@Override
		public boolean remove(Object o) {
			return SimpleHashMap.this.removeEntryForKey(o) != null;
		}

		@Override
		public void clear() {
			SimpleHashMap.this.clear();
		}
	}

	/**
	 * Returns a collection view of the values contained in this map. The
	 * collection is backed by the map, so changes to the map are reflected in
	 * the collection, and vice-versa. The collection supports element removal,
	 * which removes the corresponding mapping from this map, via the
	 * <tt>Iterator.remove</tt>, <tt>Collection.remove</tt>,
	 * <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt>
	 * operations. It does not support the <tt>add</tt> or <tt>addAll</tt>
	 * operations.
	 *
	 * @return a collection view of the values contained in this map.
	 */
	@Override
	public Collection<V> values() {
		Collection<V> vs = values;
		return (vs != null ? vs : (values = new Values()));
	}

	protected class Values extends AbstractCollection<V> {
		@Override
		public Iterator<V> iterator() {
			return newValueIterator();
		}

		@Override
		public int size() {
			return size;
		}

		@Override
		public boolean contains(Object o) {
			return containsValue(o);
		}

		@Override
		public void clear() {
			SimpleHashMap.this.clear();
		}
	}

	/**
	 * Returns a collection view of the mappings contained in this map. Each
	 * element in the returned collection is a <tt>Map.Entry</tt>. The
	 * collection is backed by the map, so changes to the map are reflected in
	 * the collection, and vice-versa. The collection supports element removal,
	 * which removes the corresponding mapping from the map, via the
	 * <tt>Iterator.remove</tt>, <tt>Collection.remove</tt>,
	 * <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt>
	 * operations. It does not support the <tt>add</tt> or <tt>addAll</tt>
	 * operations.
	 *
	 * @return a collection view of the mappings contained in this map.
	 * @see Map.Entry
	 */
	@Override
	public Set<Map.Entry<K, V>> entrySet() {
		Set<Map.Entry<K, V>> es = entrySet;
		return (es != null ? es : (entrySet = new EntrySet()));
	}

	protected class EntrySet extends AbstractSet<Map.Entry<K, V>> {
		@Override
		public Iterator<Map.Entry<K, V>> iterator() {
			return newEntryIterator();
		}

		@Override
		@SuppressWarnings("unchecked")
		public boolean contains(Object o) {
			if(!(o instanceof Map.Entry))
				return false;
			Map.Entry<K, V> e = (Map.Entry<K, V>) o;
			Entry<K, V> candidate = getEntry(e.getKey());
			return candidate != null && candidate.equals(e);
		}

		@Override
		public boolean remove(Object o) {
			return removeMapping(o) != null;
		}

		@Override
		public int size() {
			return size;
		}

		@Override
		public void clear() {
			SimpleHashMap.this.clear();
		}
	}

	/**
	 * Save the state of the <tt>HashMap</tt> instance to a stream (i.e.,
	 * serialize it).
	 *
	 * @serialData The <i>capacity</i> of the HashMap (the length of the bucket
	 *             array) is emitted (int), followed by the <i>size</i> of the
	 *             HashMap (the number of key-value mappings), followed by the
	 *             key (Object) and value (Object) for each key-value mapping
	 *             represented by the HashMap The key-value mappings are emitted
	 *             in the order that they are returned by
	 *             <tt>entrySet().iterator()</tt>.
	 *
	 */
	protected void writeObject(java.io.ObjectOutputStream s) throws IOException {
		Iterator<Map.Entry<K, V>> i = entrySet().iterator();

		// Write out the threshold, loadfactor, and any hidden stuff
		s.defaultWriteObject();

		// Write out number of buckets
		s.writeInt(table.length);

		// Write out size (number of Mappings)
		s.writeInt(size);

		// Write out keys and values (alternating)
		while(i.hasNext()) {
			Map.Entry<K, V> e = i.next();
			s.writeObject(e.getKey());
			s.writeObject(e.getValue());
		}
	}

	private static final long serialVersionUID = 362498820763181265L;

	/**
	 * Reconstitute the <tt>HashMap</tt> instance from a stream (i.e.,
	 * deserialize it).
	 */
	protected void readObject(java.io.ObjectInputStream s) throws IOException, ClassNotFoundException {
		// Read in the threshold, loadfactor, and any hidden stuff
		s.defaultReadObject();

		// Read in number of buckets and allocate the bucket array;
		int numBuckets = s.readInt();
		table = CollectionUtils.genericize(new Entry[numBuckets]);

		init(); // Give subclass a chance to do its thing.

		// Read in size (number of Mappings)
		int size = s.readInt();

		// Read the keys and values, and put the mappings in the HashMap
		for(int i = 0; i < size; i++) {
			K key = castKey(s.readObject());
			V value = castValue(s.readObject());
			putForCreate(key, value);
		}
	}

	// These methods are used when serializing HashSets
	protected int capacity() {
		return table.length;
	}

	protected float loadFactor() {
		return loadFactor;
	}
}
