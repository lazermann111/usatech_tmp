package simple.util;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

import simple.util.concurrent.LockSegmentCache;

public class EnhancedThreadLocal<T, I extends Exception> {
	protected final ReferenceQueue<Thread> cleanupQueue = new ReferenceQueue<Thread>();
	protected final LockSegmentCache<Thread, T, I> cache;

	protected static class ThreadReference extends WeakReference<Thread> {
		protected final int hash;
		protected final long id;

		public ThreadReference(Thread referent, ReferenceQueue<Thread> cleanupQueue) {
			super(referent, cleanupQueue);
			id = referent.getId();
			hash = referent.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(!(obj instanceof ThreadReference))
				return false;
			ThreadReference tr = (ThreadReference) obj;
			return tr.id == id && tr.hash == hash;
		}

		@Override
		public int hashCode() {
			return hash;
		}
	}

	public EnhancedThreadLocal(int initialCapacity, float loadFactor, int concurrencyLevel) {
		cache = new LockSegmentCache<Thread, T, I>(initialCapacity, loadFactor, concurrencyLevel) {
			protected T createValue(Thread key, Object... additionalInfo) throws I {
				registerThread(key);
				return initialValue();
			}
		};
	}

	@SuppressWarnings("unused")
	protected void registerThread(Thread thread) {
		new ThreadReference(thread, cleanupQueue);
	}

	/**
	 * @throws I
	 */
	protected T initialValue() throws I {
		return null;
	}

	public T get() throws I {
		return cache.getOrCreate(Thread.currentThread());
	}

	public void remove() {
		cache.remove(Thread.currentThread());
	}

	public void removeAll() {
		cache.clear();
	}

	public Collection<T> getAll() {
		return cache.values();
	}

	protected void clean() {
		Reference<? extends Thread> threadRef;
		while((threadRef = cleanupQueue.poll()) != null) {
			Thread thread = threadRef.get();
			if(thread != null)
				cache.remove(thread);
		}
	}

	protected void cleanBlocking(AtomicBoolean done) {
		while(!done.get()) {
			Reference<? extends Thread> threadRef;
			try {
				threadRef = cleanupQueue.remove();
			} catch(InterruptedException e) {
				continue;
			}
			Thread thread = threadRef.get();
			if(thread != null)
				cache.remove(thread);
		}
	}
}
