package simple.util;

import java.util.Map;

public class CaseInsensitiveHashMap<V> extends ConvertingHashMap<String,V> {
	private static final long serialVersionUID = -239410014123471461L;

	public CaseInsensitiveHashMap() {
		super();
	}

	public CaseInsensitiveHashMap(int initialCapacity) {
		super(initialCapacity);
	}

	public CaseInsensitiveHashMap(Map<? extends String, ? extends V> m) {
		super(m);
	}

	public CaseInsensitiveHashMap(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	@Override
	protected String convertKey(Object key) {
		if(key == null) return null;
		return key.toString().toUpperCase();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected V convertValue(Object value) {
		return (V)value;
	}

}
