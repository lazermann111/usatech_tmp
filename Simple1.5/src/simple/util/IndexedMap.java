/*
 * MapWrapper.java
 *
 * Created on October 03, 2002, 5:23 PM
 */

package simple.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Adds List-like functionality to a Map
 * @author  Brian S. Krug
 */
public class IndexedMap implements Map {
	protected Map map;
	protected List list;
	protected boolean oneIndex = true;
        
	public IndexedMap() {
		this(new HashMap(), new ArrayList(), true);
	}
	
	public IndexedMap(Map delegate, List indexer, boolean oneIndex) {
		this.map = delegate;
		this.list = indexer;
                this.oneIndex = oneIndex;
	}
	
	public int hashCode() {
		return  map.hashCode();
	}
	
	public java.lang.Object put(java.lang.Object key, java.lang.Object value) {
		if(!oneIndex || !map.containsKey(key)) list.add(key);
		return map.put(key, value);
	}
	
	public boolean equals(java.lang.Object objectVar0) {
		return  map.equals(objectVar0);
	}
	
	public java.lang.Object get(java.lang.Object key) {
		return  map.get(key);
	}
	
	public java.util.Collection values() {
		return  map.values();
	}
	
	public int size() {
		return  map.size();
	}
	
	public void clear() {
		list.clear();
		map.clear();
	}
	
	public java.lang.Object remove(java.lang.Object key) {
		if(map.containsKey(key)) while(list.remove(key)) ;
		return  map.remove(key);
	}
	
	public java.util.Set keySet() {
		return  map.keySet();
	}
	
	public java.util.Set entrySet() {
		return  map.entrySet();
	}
	
	public boolean isEmpty() {
		return  map.isEmpty();
	}
	
	public boolean containsValue(java.lang.Object objectVar0) {
		return  map.containsValue(objectVar0);
	}
	
	public boolean containsKey(java.lang.Object objectVar0) {
		return  map.containsKey(objectVar0);
	}
	
	public void putAll(java.util.Map amap) {
		list.addAll(amap.keySet());
		map.putAll(amap);
	}
	
	public java.lang.Object remove(int index) {
		Object key = list.remove(index);
		map.remove(key);
		return key;
	}
	
	public java.util.Iterator keyIterator() {
		return list.iterator();
	}
	
	public java.util.ListIterator keyListIterator(int index) {
		return list.listIterator(index);
	}
		
	public int indexOf(java.lang.Object key) {
		return list.indexOf(key);
	}
	
	public int lastIndexOf(java.lang.Object key) {
		return list.lastIndexOf(key);
	}
	
	public java.lang.Object getKey(int index) {
		return list.get(index);
	}
	
	public java.lang.Object getValue(int index) {
		return map.get(list.get(index));
	}
	
	public java.lang.Object setKey(int index, java.lang.Object newKey) {
		Object oldKey = list.set(index, newKey);
		Object value = map.remove(oldKey);
		map.put(newKey, value);
		return oldKey;
	}
	
	public java.lang.Object setValue(int index, java.lang.Object newValue) {
		Object key = list.get(index);
		return map.put(key, newValue);
	}

	public java.util.ListIterator keyListIterator() {
		return list.listIterator();
	}
	
	public java.lang.Object[] toKeyArray(java.lang.Object[] a) {
		return list.toArray(a);
	}
	
	public java.lang.Object[] toKeyArray() {
		return list.toArray();
	}
	
        /**
         * Getter for property oneIndex.
         * @return Value of property oneIndex.
         */
        public boolean isOneIndex() {
            return oneIndex;
        }        
}

