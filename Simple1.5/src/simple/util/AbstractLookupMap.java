package simple.util;

import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public abstract class AbstractLookupMap<K,V> extends AbstractMap<K,V> {
	protected final Set<Entry<K,V>> entrySet = new AbstractSet<Entry<K,V>>() {
		@Override
		public Iterator<Entry<K, V>> iterator() {
			final Iterator<K> keyIter = keySet().iterator();
			return new Iterator<Entry<K, V>>() {
				public boolean hasNext() {
					return keyIter.hasNext();
				}
				public Entry<K, V> next() {
					final K key = keyIter.next();
					return new Entry<K, V>() {
						public K getKey() {
							return key;
						}
						public V getValue() {
							return get(key);
						}
						public V setValue(V value) {
							throw new UnsupportedOperationException("LookupMap is not modifiable");					
						}						
					};
				}
				public void remove() {
					throw new UnsupportedOperationException("LookupMap is not modifiable");					
				}
			};
		}

		@Override
		public int size() {
			return keySet().size();
		}		
	};
	protected final Collection<V> values = new AbstractCollection<V>() {
		@Override
		public Iterator<V> iterator() {
			final Iterator<K> keyIter = keySet().iterator();
			return new Iterator<V>() {
				public boolean hasNext() {
					return keyIter.hasNext();
				}
				public V next() {
					final K key = keyIter.next();
					return get(key);
				}
				public void remove() {
					throw new UnsupportedOperationException("LookupMap is not modifiable");					
				}
			};
		}

		@Override
		public int size() {
			return keySet().size();
		}		
	};
	public AbstractLookupMap() {
		super();
	}
	public void clear() {
		throw new UnsupportedOperationException("LookupMap is not modifiable");
	}
	public boolean containsKey(Object key) {
		return keySet().contains(key);
	}
	public boolean containsValue(Object value) {
		return values().contains(value);
	}
	public Set<Entry<K, V>> entrySet() {
		return entrySet;
	}
	public boolean isEmpty() {
		return size() == 0;
	}
	public V put(K key, V value) {
		throw new UnsupportedOperationException("LookupMap is not modifiable");
	}
	public void putAll(Map<? extends K, ? extends V> t) {
		throw new UnsupportedOperationException("LookupMap is not modifiable");
	}
	public V remove(Object key) {
		throw new UnsupportedOperationException("LookupMap is not modifiable");
	}
	public int size() {
		return keySet().size();
	}
	public Collection<V> values() {
		return values;
	}
	
	public abstract V get(Object key) ;
	public abstract Set<K> keySet() ;	
}
