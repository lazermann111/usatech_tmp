/*
 * @(#)LinkedHashMap.java	1.11 03/01/23
 *
 */

package simple.util;

/**
 * Uses my adaptation of Apache's ConcurrentSequencedHashMap so that this class is not Java 1.4 dependent
  */

public class LinkedHashMap<K,V> extends ConcurrentSequencedHashMap<K,V> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 72095872405L;

	/**
     * Constructs an empty insertion-ordered <tt>LinkedHashMap</tt> instance
     * with the specified initial capacity and load factor.
     *
     * @param  initialCapacity the initial capacity.
     * @param  loadFactor      the load factor.
     * @throws IllegalArgumentException if the initial capacity is negative
     *         or the load factor is nonpositive.
     */
    public LinkedHashMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    /**
     * Constructs an empty insertion-ordered <tt>LinkedHashMap</tt> instance
     * with the specified initial capacity and a default load factor (0.75). 
     *
     * @param  initialCapacity the initial capacity.
     * @throws IllegalArgumentException if the initial capacity is negative.
     */
    public LinkedHashMap(int initialCapacity) {
	super(initialCapacity);
    }

    /**
     * Constructs an empty insertion-ordered <tt>LinkedHashMap</tt> instance
     * with a default capacity (16) and load factor (0.75).
     */
    public LinkedHashMap() {
	super();
    }

    /**
     * Constructs an insertion-ordered <tt>LinkedHashMap</tt> instance with
     * the same mappings as the specified map.  The <tt>LinkedHashMap</tt>
     * instance is created with a a default load factor (0.75) and an initial
     * capacity sufficient to hold the mappings in the specified map.
     *
     * @param  m the map whose mappings are to be placed in this map.
     * @throws NullPointerException if the specified map is null.
     */
    public LinkedHashMap(java.util.Map<? extends K,? extends V> m) {
        super(m);
    }

    /**
     * Constructs an empty <tt>LinkedHashMap</tt> instance with the
     * specified initial capacity, load factor and ordering mode.
     *
     * @param  initialCapacity the initial capacity.
     * @param  loadFactor      the load factor.
     * @param  accessOrder     the ordering mode - <tt>true</tt> for
     *         access-order, <tt>false</tt> for insertion-order.
     * @throws IllegalArgumentException if the initial capacity is negative
     *         or the load factor is nonpositive.
     */
    public LinkedHashMap(int initialCapacity, float loadFactor,
                         boolean accessOrder) {
        throw new UnsupportedOperationException("accessOrder = true is not supported");
    }

}
