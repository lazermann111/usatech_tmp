/**
 *
 */
package simple.util;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author Brian S. Krug
 *
 */
public class UnmodifiableIntegerRangeSet implements IntegerRangeSet {
	protected final IntegerRangeSet delegate;

	public UnmodifiableIntegerRangeSet(IntegerRangeSet delegate) {
		this.delegate = delegate;
	}

	public boolean add(Integer e) {
		throw new UnsupportedOperationException("Not modifiable");
	}

	public boolean addAll(Collection<? extends Integer> c) {
		throw new UnsupportedOperationException("Not modifiable");
	}

	public int addRange(int min, int max) throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Not modifiable");
	}

	public int addRanges(String rangeString) throws NumberFormatException, UnsupportedOperationException {
		throw new UnsupportedOperationException("Not modifiable");
	}

	public void clear() {
		throw new UnsupportedOperationException("Not modifiable");
	}

	public boolean contains(Object o) {
		return delegate.contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return delegate.containsAll(c);
	}

	@Override
	public boolean equals(Object o) {
		return delegate.equals(o);
	}

	public char getConcatChar() {
		return delegate.getConcatChar();
	}

	public char getRangeChar() {
		return delegate.getRangeChar();
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

	public boolean isEmpty() {
		return delegate.isEmpty();
	}

	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {
			protected final Iterator<Integer> iterDelegate = delegate.iterator();
			public boolean hasNext() {
				return iterDelegate.hasNext();
			}
			public Integer next() {
				return iterDelegate.next();
			}
			public void remove() {
				throw new UnsupportedOperationException("Not modifiable");
			}
		};
	}

	public boolean remove(Object o) {
		throw new UnsupportedOperationException("Not modifiable");
	}

	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("Not modifiable");
	}

	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("Not modifiable");
	}

	public int size() {
		return delegate.size();
	}

	public Object[] toArray() {
		return delegate.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return delegate.toArray(a);
	}

	@Override
	public String toString() {
		return delegate.toString();
	}
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(IntegerRangeSet o) {
		return delegate.compareTo(o);
	}
}
