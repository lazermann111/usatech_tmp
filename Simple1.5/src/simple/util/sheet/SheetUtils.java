package simple.util.sheet;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.math.BigDecimal;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.FontDetails;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import simple.results.Results;
import simple.text.StringUtils;

/**
 * @author bkrug
 *
 */
public class SheetUtils {
	public static class CellError {
		protected final byte errorCode;
		public CellError(byte errorCode) {
			this.errorCode = errorCode;
		}
		public byte getErrorCode() {
			return errorCode;
		}
		@Override
		public int hashCode() {
			return errorCode;
		}
		@Override
		public String toString() {
			return "Cell Error #" + errorCode;
		}
	}
	private static class ExcelRowList extends AbstractList<List<Object>> {
		private final Sheet sheet;
		public ExcelRowList(Sheet sheet) {
            this.sheet = sheet;
        }
		@Override
		public List<Object> get(int index) {
			return new ExcelCellList(sheet.getRow(index));
		}

		@Override
		public int size() {
			return sheet.getLastRowNum() - sheet.getFirstRowNum() + 1;
		}
	}
	private static class ExcelCellList extends AbstractList<Object> {
		private final Row row;
		public ExcelCellList(Row row) {
			this.row = row;
		}
		@Override
		public Object get(int index) {
			Cell cell = row.getCell(index);
			if(cell == null)
				return null;
			return getCellValue(cell, cell.getCellType());
		}

		@Override
		public int size() {
			return row == null ? 0 : row.getLastCellNum(); // - row.getFirstCellNum() + 1; if we subtract first cell num then blank cells mess this up
		}
	}
	private static class DelegatingRowIterator implements RowIterator {
		private final Iterator<List<Object>> delegate;
		private int row = -1;
        public DelegatingRowIterator(Iterator<List<Object>> delegate) {
			this.delegate = delegate;
		}
		public int getCurrentRowNum() {
			return row;
		}
		public boolean hasNext() {
			return delegate.hasNext();
		}
		public List<Object> next() {
			List<Object> list = delegate.next();
			row++;
			return list;
		}
		public void remove() {
			delegate.remove();
		}
	}

    private static class POIHSSFFontMetrics {
    	private static Properties fontMetricsProps;

    	private static Map <String, FontDetails>fontDetailsMap = new HashMap<String, FontDetails>();

    	 /** Calculates the width of a given string in units of a standard character
	     *
	     * @param text
	     * @param fontName
	     * @param fontPoint
	     * @param bold
	     *
	     * @returns float
	     */
    	protected static float getStringWidth(String text, String fontName, int fontPoint, boolean bold) {
    		FontDetails fd = getFontDetails(fontName);
    		float fudge = (bold ? 5.4f : 5.8f);
    		return (fd.getStringWidth(text) * (fontPoint / 10.0f)) / fudge;
    	}
    	/**
	     * Retrieves the fake font details for a given font.
	     *
	     * @param font
	     *            the font to lookup.
	     * @return the fake font.
	     */
    	protected static FontDetails getFontDetails(String fontName) {
    		if (fontMetricsProps == null) {
    			InputStream metricsIn = null;
    			try {
    				fontMetricsProps = new Properties();
    				metricsIn = FontDetails.class.getResourceAsStream("/font_metrics.properties");
    				if (metricsIn == null) {
    					throw new FileNotFoundException("font_metrics.properties not found in class path");
    				}
    				fontMetricsProps.load(metricsIn);
    			} catch (IOException e) {
    				throw new RuntimeException("Could not load font metrics: " + e.getMessage());
    			} finally {
    				if (metricsIn != null) {
    					try {
    						metricsIn.close();
    					} catch (IOException e) { }
    				}
    			}
    		}

    		if (fontDetailsMap.get(fontName) == null) {
    			FontDetails fontDetails = FontDetails.create(fontName, fontMetricsProps);
    			fontDetailsMap.put(fontName, fontDetails);
    			return fontDetails;
    		} else {
    			return fontDetailsMap.get(fontName);
    		}
    	}
    }

	private static final LineParser cvsLineParser = new LineParser() {
			public Object[] parseLine(String line) {
				return StringUtils.split(line, ',', '"', true);
			}
		};

    private static interface LineParser {
    	public Object[] parseLine(String line) ;
    }

    private static class LineParserRowIterator implements RowIterator {
        private final BufferedReader br;
        private String next;
        private int row = -1;
        private final LineParser lineParser;
        public LineParserRowIterator(InputStream in, LineParser lineParser) throws IOException {
            br = new BufferedReader(new InputStreamReader(in));
            this.lineParser = lineParser;
            next = br.readLine();
        }

        public boolean hasNext() {
            return next != null;
        }

        public List<Object> next() {
        	List<Object> list = Arrays.asList(lineParser.parseLine(next));
        	try {
                next = br.readLine();
            } catch (IOException ioe) {
                throw new RuntimeException(ioe.getMessage());
            }
            row++;
            return list;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

		public int getCurrentRowNum() {
			return row;
		}
    }

    public static interface RowIterator extends Iterator<List<Object>> {
    	public int getCurrentRowNum() ;
    }

    public static class RowValuesIterator implements Iterator<Map<String,Object>> {
    	protected final RowIterator rowIter;
    	protected final String[] columnNames;

    	public RowValuesIterator(RowIterator rowIter, boolean retainCaseOfColumnNames) {
    		this(rowIter, readColumnNames(rowIter, retainCaseOfColumnNames));
    	}
    	public RowValuesIterator(RowIterator rowIter, String[] columnNames) {
    		this.rowIter = rowIter;
    		this.columnNames = columnNames;
    	}
		public boolean hasNext() {
			return rowIter.hasNext();
		}
		public Map<String, Object> next() {
			return next(false);
		}

		public Map<String, Object> next(boolean includeEmptyValues) {
			Map<String, Object> columnValues = new HashMap<String, Object>();
			List<Object> cells = rowIter.next();
			for(int k = 0; k < columnNames.length; k++) {
				Object value;
				if(cells.size() > k)
					value = cells.get(k);
				else
					value = null;
				if(includeEmptyValues || (value != null && value.toString().trim().length() > 0)) {
                    columnValues.put(columnNames[k], value);
                }
            }
			return columnValues;
		}
		public void remove() {
			throw new UnsupportedOperationException();
		}
		public int getCurrentRowNum() {
			return rowIter.getCurrentRowNum();
		}
		public String[] getColumnNames() {
			return columnNames;
		}
    }

    protected static String[] readColumnNames(Iterator<List<Object>> rowIter, boolean retainCase) {
    	List<String> columnNames = new ArrayList<String>();
		// find the header row
    	while(columnNames.size() == 0 && rowIter.hasNext()) {
	    	List<Object> cells = rowIter.next();
	        for(int k = 0; k < cells.size(); k++) {
	            Object value = cells.get(k);
	            if(value != null && value.toString().trim().length() > 0) {
	                while (columnNames.size() < k)
                        columnNames.add(null);
	                String s = value.toString().trim();
	                if(!retainCase)
	                	s = s.toUpperCase();
	                columnNames.add(k, s);
	            }
	        }
    	}
    	return columnNames.toArray(new String[columnNames.size()]);
    }
/*
    public static RowIterator getExcelRowIterator(InputStream dataStream) throws IOException {
	    //      must buffer dataStream so we can reset on error
	    BufferedInputStream bif = new BufferedInputStream(dataStream);
	    bif.mark(2050);
	    try {
	        HSSFWorkbook book = new HSSFWorkbook(bif);
	        HSSFSheet sheet = book.getSheetAt(0);
	        return new DelegatingRowIterator(new ExcelRowList(sheet).iterator());
	    } catch (IOException ioe) {
	        bif.reset();
	        return new LineParserRowIterator(bif, cvsLineParser);
	    }
    }
*/
    public static RowIterator getExcelRowIterator(InputStream dataStream) throws IOException {
	    // must buffer dataStream so we can reset on error
    	if(!dataStream.markSupported()) {
    		dataStream = new PushbackInputStream(dataStream, 8);
		}
		
		if(POIFSFileSystem.hasPOIFSHeader(dataStream)) {
			return new DelegatingRowIterator(new ExcelRowList(new HSSFWorkbook(dataStream).getSheetAt(0)).iterator());
		} else if(POIXMLDocument.hasOOXMLHeader(dataStream)) {
			try {
				return new DelegatingRowIterator(new ExcelRowList(new XSSFWorkbook(OPCPackage.open(dataStream)).getSheetAt(0)).iterator());
			} catch(InvalidFormatException e) {
				throw new IOException(e);
			}
		} else {
			return new LineParserRowIterator(dataStream, cvsLineParser);
		}
    }
    public static RowIterator getFixedWidthRowIterator(InputStream dataStream, final int[] widths) throws IOException {
    	LineParser fixedWidthParser = new LineParser() {
			public Object[] parseLine(String line) {
				String[] values = new String[widths.length];
		        int pos = 0;
		        for(int i = 0; i < widths.length; i++) {
		        	int endPos = pos + widths[i];
		        	if(endPos >= line.length()) {
		        		values[i] = line.substring(pos);
		        		break;
		        	}
		        	values[i] = line.substring(pos, endPos);
		        	pos = endPos;
		        }
		        return values;
			}
    	};
    	return new LineParserRowIterator(dataStream, fixedWidthParser);

    }

    /**
     * createCSVSheet
     *
     * @param stream
     * @param cells
     * @throws Exception
     * @throws IOException
     *
     * use simple.text.StringUtil.writeCSV() instead
    public static void createCSVSheet(OutputStream stream, List<Map<Object, Object>>cells) throws Exception, IOException {

    }
    */

    public static void writeExcel(final java.sql.ResultSet rs, OutputStream os) throws IOException, java.sql.SQLException {
    	writeExcel(rs, os, false);
    }

    public static void writeExcel(final java.sql.ResultSet rs, OutputStream os, boolean header) throws IOException, java.sql.SQLException {
    	HSSFWorkbook book = new HSSFWorkbook();
    	HSSFSheet sheet = book.createSheet();

    	java.sql.ResultSetMetaData md = rs.getMetaData();
    	int cols = md.getColumnCount();

    	float []widths = new float[cols];
    	if (header) {
    		String []colNames = new String[cols];
    		for (int i = 0 ; i < cols ; i++) {
    			colNames[i] = md.getColumnClassName(i);
    		}
    		writeExcelHeader(colNames, book, widths);
    	}
    	writeExcel(new Iterator<Object[]>() {
    		private boolean next = rs.next();
    		private final int num = rs.getMetaData().getColumnCount();
    		public boolean hasNext() { return next; }
    		public Object[] next() {
    			Object[] values = new Object[num];
    			try {
    				for (int i = 0 ; i < num ; i++) values[i] = rs.getObject(i + 1);
    				next = rs.next();
    			} catch (java.sql.SQLException sqle) {
    				throw new RuntimeException("While retrieving ResultSet data values", sqle);
    			}
    			return values;
    		}
    		public void remove() {}
    	}, book, widths);

        for (int i = 0 ; i < widths.length ; i++) {
        	sheet.setColumnWidth(i, (int)(256 * widths[i]));
        }

        book.write(os);
    }

    public static void writeExcel(final Results results, OutputStream os) throws IOException {
    	writeExcel(results, os, false);
    }

    public static void writeExcel(final Results results, OutputStream os, boolean header) throws IOException {
    	HSSFWorkbook book = new HSSFWorkbook();
    	HSSFSheet sheet = book.createSheet();


    	int cols = results.getColumnCount();
    	float []widths = new float[cols];

    	if (header) {
        	String []colNames = new String[cols];
    		for (int i = 0 ; i < cols ; i++) {
    			colNames[i] = results.getColumnName(i + 1);
    		}
    		writeExcelHeader(colNames, book, widths);
    	}

    	writeExcel(new Iterator<Object[]>() {
    		private boolean next = results.next();
    		private final int num = results.getColumnCount();
    		public boolean hasNext() { return next; }
    		public Object[] next() {
    			Object[] values = new Object[num];
    			for (int i = 0 ; i < num ; i++) values[i] = results.getValue(i + 1);
    			next = results.next();
    			return values;
    		}
    		public void remove() {}
    	}, book, widths);

        for (int i = 0 ; i < widths.length ; i++) {
        	sheet.setColumnWidth(i, (int)(256 * widths[i]));
        }

        book.write(os);
    }

	public static void writeExcel(final Results results, OutputStream os, boolean header, final int[] columnIndexes) throws IOException {
		HSSFWorkbook book = new HSSFWorkbook();
		HSSFSheet sheet = book.createSheet();

		float[] widths = new float[columnIndexes.length];

		if(header) {
			String[] colNames = new String[columnIndexes.length];
			for(int i = 0; i < columnIndexes.length; i++) {
				colNames[i] = results.getColumnName(columnIndexes[i]);
			}
			writeExcelHeader(colNames, book, widths);
		}

		writeExcel(new Iterator<Object[]>() {
			private boolean next = results.next();
			private final int num = columnIndexes.length;

			public boolean hasNext() {
				return next;
			}

			public Object[] next() {
				Object[] values = new Object[num];
				for(int i = 0; i < num; i++)
					values[i] = results.getValue(columnIndexes[i]);
				next = results.next();
				return values;
			}

			public void remove() {
			}
		}, book, widths);

		for(int i = 0; i < widths.length; i++) {
			sheet.setColumnWidth(i, (int) (256 * widths[i]));
		}

		book.write(os);
	}

    public static void writeExcel(Iterator <Object[]>iter, HSSFWorkbook book) {
    	writeExcel(iter, book, null);
    }

    public static void writeExcel(Iterator <Object[]>iter, HSSFWorkbook book, float widths[]) {
    	HSSFSheet sheet = book.getSheetAt(0);
    	int rowNum = 0;
    	if (sheet == null) {
    		sheet = book.createSheet();
    	} else {
    		rowNum = sheet.getLastRowNum() + 1;
    	}

    	for ( ; iter.hasNext() ; rowNum++) writeExcelRow(iter.next(), book, rowNum, widths);
    }

    public static void writeExcelHeader(String []colNames, HSSFWorkbook book, float []widths) {
    	HSSFCellStyle style = book.createCellStyle();
    	HSSFFont font = book.createFont();
    	font.setBoldweight((short)700);
    	style.setFont(font);

    	writeExcelRow(colNames, book, 0, widths, style);
    }

    public static void writeExcelRow(Object []data, HSSFWorkbook book) {
    	writeExcelRow(data, book, 0, null, null);
    }

    public static void writeExcelRow(Object []data, HSSFWorkbook book, int rowNum) {
    	writeExcelRow(data, book, rowNum, null, null);
    }

    public static void writeExcelRow(Object []data, HSSFWorkbook book, int rowNum, float []widths) {
    	writeExcelRow(data, book, rowNum, widths, null);
    }

    public static void writeExcelRow(Object []data, HSSFWorkbook book, int rowNum, float []widths, HSSFCellStyle style) {
    	HSSFSheet sheet = book.getSheetAt(0);
    	if (sheet == null) {
    		book.createSheet();
    	}
    	HSSFRow row = sheet.createRow(rowNum);
    	for (int i = 0 ; i < data.length ; i++) {
    		HSSFCell cell = row.createCell(i);
    		if (style != null) cell.setCellStyle(style);
    		if (data[i] instanceof Boolean) cell.setCellValue((((Boolean)data[i]).booleanValue()));
    		else if (data[i] instanceof java.util.Calendar) cell.setCellValue((java.util.Calendar)data[i]);
    		else if (data[i] instanceof java.util.Date) cell.setCellValue((java.util.Date)data[i]);
    		else if (data[i] instanceof Number) cell.setCellValue(((Number)data[i]).doubleValue());
    		else if (data[i] != null) cell.setCellValue(data[i].toString());
    		if (widths != null && i >= 0 && i < widths.length) 
    			widths[i] = Math.max(widths[i], calcCellWidth(book, sheet, cell));
    	}
    }

    /**
     * createExcelSheet
     *
     * @param stream
     * @param cells
     * @throws Exception
     * @throws IOException
     */
    public static void createExcelSheet(OutputStream stream, List<Map<Object, Object>>cells) throws Exception, IOException {
    	HSSFWorkbook book = new HSSFWorkbook();
    	HSSFSheet sheet = book.createSheet();
    	HSSFRow row = sheet.createRow(0);

    	HSSFCellStyle style = book.createCellStyle();
    	HSSFFont font = book.createFont();
    	font.setBoldweight((short)700);
    	style.setFont(font);

    	Set <Object>keys = cells.get(0).keySet();
    	float []widths = new float[keys.size()];

    	// create the column headers
    	int column = 0;
    	for (Object key : keys) {
    		HSSFCell cell = row.createCell(column);
    		cell.setCellValue(key.toString());
    		cell.setCellStyle(style);

    		widths[column] = Math.max(widths[column], calcCellWidth(book, sheet, cell));
    		column++;
    	}

    	for (Map <Object, Object>rowMap : cells) {
    		row = sheet.createRow(sheet.getLastRowNum() + 1);
    		column = 0;
    		for (Object key : keys) {
    			HSSFCell cell = row.createCell(column);

    			Object value = rowMap.get(key);

    	    	if (value instanceof Boolean) cell.setCellValue(((Boolean)value).booleanValue());
    	    	else if (value instanceof java.util.Calendar) cell.setCellValue((java.util.Calendar)value);
    	    	else if (value instanceof java.util.Date) cell.setCellValue((java.util.Date)value);
    	    	else if (value instanceof Number) cell.setCellValue(((Number)value).doubleValue());
    	    	else if (value != null) cell.setCellValue(value.toString());

    	    	widths[column] = Math.max(widths[column], calcCellWidth(book, sheet, cell));
    			column++;
    		}
    	}

    	// adjust the column widths
    	for (int i = 0 ; i < widths.length ; i++) {
    		sheet.setColumnWidth(i, (int)(256 * widths[i]));
    	}

    	book.write(stream);
    }

	private static Object getCellValue(Cell cell, int cellType) {
		switch(cellType) {
			case HSSFCell.CELL_TYPE_FORMULA:
				return getCellValue(cell, cell.getCachedFormulaResultType());
			case HSSFCell.CELL_TYPE_BOOLEAN:
				return new Boolean(cell.getBooleanCellValue());
			case HSSFCell.CELL_TYPE_NUMERIC:
				return new BigDecimal(cell.getNumericCellValue());
			case HSSFCell.CELL_TYPE_STRING:
				return cell.getStringCellValue();
			case HSSFCell.CELL_TYPE_ERROR:
				return new CellError(cell.getErrorCellValue());
			default:
				return null;
		}
	}

    private static float calcCellWidth(HSSFWorkbook book, HSSFSheet sheet, HSSFCell cell) {
    	String text;
    	switch (cell.getCellType()) {
    	case HSSFCell.CELL_TYPE_BLANK:
    		return 0;
    	case HSSFCell.CELL_TYPE_BOOLEAN:
    		text = "" + cell.getBooleanCellValue();
    		break;
    	case HSSFCell.CELL_TYPE_ERROR:
            return 10; //XXX: Not sure what to use here
        case HSSFCell.CELL_TYPE_FORMULA:
            return 30; //XXX: Not sure what to use here
        case HSSFCell.CELL_TYPE_NUMERIC:
            text = "" + cell.getNumericCellValue();
            break;
        case HSSFCell.CELL_TYPE_STRING:
            text = cell.getStringCellValue();
            break;
        default:
            return 10;
    	}

    	// XXX: real limited: just report the length of the text value and hope it's a good estimate
        HSSFFont font = book.getFontAt(cell.getCellStyle().getFontIndex());
        float width = POIHSSFFontMetrics.getStringWidth(text, font.getFontName(), font.getFontHeightInPoints(), font.getBoldweight() == HSSFFont.BOLDWEIGHT_BOLD);
        //log.debug("Calculated width of '" + text + "' for " + font.getFontName() + "- " + font.getFontHeightInPoints() + " pt = " + width);
        //The max width of a column in excel is 255. It can hold more data then that 
        //but that is the max width it will display in a cell
        return (width>255f) ? 255f : width;
    }
    /*
    public static RowIterator getFixedWidthRowIterator(InputStream dataStream) throws IOException {
    	LineParser fixedWidthParser = new LineParser() {
    		protected int[] widths;
			public Object[] parseLine(String line) {
				if(widths == null) {
					List<Integer> widthList = new ArrayList<Integer>();
					if(line.trim().length() == 0) return new String[0];
					int pos = 0;
					for(int i = 0; i < line.length(); i++) {
						while(!Character.isWhitespace(line.charAt(i))) i++;
						while(Character.isWhitespace(line.charAt(i))) i++;
						widthList.add(i-pos);
						pos = i;
					}
					widths = widthList.toArray(new int[widthList.size()]);
				}
				String[] values = new String[widths.length];
		        int pos = 0;
		        for(int i = 0; i < widths.length; i++) {
		        	int endPos = pos + widths[i];
		        	if(endPos >= line.length()) {
		        		values[i] = line.substring(pos);
		        		break;
		        	}
		        	values[i] = line.substring(pos, endPos);
		        	pos = endPos;
		        }
		        return values;

			}
    	};
    	return new LineParserRowIterator(dataStream, fixedWidthParser);

    }
    */
    public static RowValuesIterator getExcelRowValuesIterator(InputStream dataStream, String[] columnNames) throws IOException {
    	return new RowValuesIterator(getExcelRowIterator(dataStream), columnNames);
    }
    public static RowValuesIterator getExcelRowValuesIterator(InputStream dataStream) throws IOException {
    	return new RowValuesIterator(getExcelRowIterator(dataStream), false);
    }
    public static RowValuesIterator getExcelRowValuesIterator(InputStream dataStream, boolean retainCaseOfColumnNames) throws IOException {
    	return new RowValuesIterator(getExcelRowIterator(dataStream), retainCaseOfColumnNames);
    }
    public static RowValuesIterator getFixedWidthRowValuesIterator(InputStream dataStream, int[] widths, String[] columnNames) throws IOException {
    	return new RowValuesIterator(getFixedWidthRowIterator(dataStream, widths), columnNames);
    }
    public static RowValuesIterator getCVSRowValuesIterator(InputStream dataStream, String[] columnNames) throws IOException {
    	return new RowValuesIterator(new LineParserRowIterator(dataStream, cvsLineParser), columnNames);
    }
    public static RowValuesIterator getCVSRowValuesIterator(InputStream dataStream) throws IOException {
    	return new RowValuesIterator(new LineParserRowIterator(dataStream, cvsLineParser), false);
    }
    public static RowValuesIterator getCVSRowValuesIterator(InputStream dataStream, boolean retainCaseOfColumnNames) throws IOException {
    	return new RowValuesIterator(new LineParserRowIterator(dataStream, cvsLineParser), retainCaseOfColumnNames);
    }
}
