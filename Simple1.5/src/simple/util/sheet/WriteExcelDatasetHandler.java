package simple.util.sheet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.WorkbookUtil;

import simple.bean.ConvertUtils;
import simple.results.DatasetHandler;

public class WriteExcelDatasetHandler implements DatasetHandler<IOException> {
	protected final HSSFWorkbook book;
	protected final HSSFCellStyle headerStyle;
	protected final OutputStream out;
	protected Map<Object, HSSFSheet> sheets;
	protected Set<String> sheetNames = new LinkedHashSet<>();
	protected String currentSelector;
	protected HSSFSheet currentSheet;
	protected String selectorColumn;
	protected boolean firstColumn = true;
	protected boolean firstRow = true;
	protected HSSFRow currentRow;
	protected HSSFRow headerRow;
	protected int rows = 0;

	public WriteExcelDatasetHandler(OutputStream out) {
		super();
		this.out = out;
		book = new HSSFWorkbook();
		headerStyle = book.createCellStyle();
		HSSFFont font = book.createFont();
		font.setBoldweight((short) 700);
		headerStyle.setFont(font);
		headerStyle.setBorderBottom((short) 1);
		headerStyle.setBottomBorderColor(new HSSFColor.BLACK().getIndex());
		headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		headerStyle.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
	}

	@Override
	public void handleDatasetStart(String[] columnNames) throws IOException {
	}

	@Override
	public void handleRowStart() throws IOException {
	}

	@Override
	public void handleValue(String columnName, Object value) throws IOException {
		if(selectorColumn != null && selectorColumn.equals(columnName)) {
			if(sheets == null)
				sheets = new LinkedHashMap<Object, HSSFSheet>();
			currentSheet = sheets.get(value);
			if(currentSheet == null) {
				startSheet(ConvertUtils.getStringSafely(value, "Data"));
				sheets.put(value, currentSheet);
			}
		} else {
			if(firstRow) {
				if(currentSheet == null)
					startSheet("Data");
				writeHeaderCell(columnName);
			}
			writeDataCell(value);
		}
	}

	protected void startSheet(String sheetName) {
		String baseName = sheetName = WorkbookUtil.createSafeSheetName(sheetName);
		int index = 2;
		while(!sheetNames.add(sheetName)) {
			String indexStr = Integer.toString(index++);
			if(baseName.length() + indexStr.length() + 2 < 31) {
				sheetName = baseName + " (" + indexStr + ")";
			} else {
				sheetName = baseName.substring(0, 31 - indexStr.length() - 2) + "(" + indexStr + ")";
			}
		}
		currentSheet = book.createSheet(sheetName);
		headerRow = currentSheet.createRow(0);
		firstRow = true;
	}

	protected void writeHeaderCell(String columnName) {
		HSSFCell cell = headerRow.createCell(Math.max(0, headerRow.getLastCellNum()));
    	cell.setCellStyle(headerStyle);
    	cell.setCellValue(columnName);
	}

	protected void writeDataCell(Object data) {
		if(firstColumn) {
			currentRow = currentSheet.createRow(currentSheet.getLastRowNum() + 1);
			firstColumn = false;
		}
		HSSFCell cell = currentRow.createCell(Math.max(0, currentRow.getLastCellNum()));
		if(data instanceof Boolean)
			cell.setCellValue((((Boolean) data).booleanValue()));
		else if(data instanceof java.util.Calendar)
			cell.setCellValue((java.util.Calendar) data);
		else if(data instanceof java.util.Date)
			cell.setCellValue((java.util.Date) data);
		else if(data instanceof Number)
			cell.setCellValue(((Number) data).doubleValue());
		else if(data != null)
			cell.setCellValue(data.toString());
	}

	@Override
	public void handleRowEnd() throws IOException {
		firstRow = false;
		firstColumn = true;
		rows++;
	}

	@Override
	public void handleDatasetEnd() throws IOException {
		book.write(out);
		out.flush();
	}

	public String getSelectorColumn() {
		return selectorColumn;
	}

	public void setSelectorColumn(String selectorColumn) {
		this.selectorColumn = selectorColumn;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
}
