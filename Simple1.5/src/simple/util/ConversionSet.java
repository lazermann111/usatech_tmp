package simple.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public abstract class ConversionSet<E0, E1> extends ConversionCollection<E0, E1>  implements Set<E1> {

	public ConversionSet(Set<E0> delegate) {
		super(delegate);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
		    return true;

		if (!(o instanceof Set))
		    return false;
		Collection<?> c = (Collection<?>) o;
		if (c.size() != size())
		    return false;
        try {
            return containsAll(c);
        } catch(ClassCastException unused)   {
            return false;
        } catch(NullPointerException unused) {
            return false;
        }
    }

    @Override
	public boolean removeAll(Collection<?> c) {
        boolean modified = false;

        if (size() > c.size()) {
            for (Iterator<?> i = c.iterator(); i.hasNext(); )
                modified |= remove(i.next());
        } else {
            for (Iterator<?> i = iterator(); i.hasNext(); ) {
                if (c.contains(i.next())) {
                    i.remove();
                    modified = true;
                }
            }
        }
        return modified;
    }
}
