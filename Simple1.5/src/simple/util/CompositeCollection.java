/*
 * CompositeSet.java
 *
 * Created on March 22, 2004, 8:55 AM
 */

package simple.util;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author  Brian S. Krug
 */
public class CompositeCollection<E> extends AbstractCollection<E> implements Collection<E> {
    protected List<Collection<? extends E>> colls = new ArrayList<Collection<? extends E>>();
    /** Creates a new instance of CompositeSet */
    public CompositeCollection() {
        super();
    }
    
    /** Creates a new instance of CompositeSet */
    public CompositeCollection(Collection<? extends E>... containedColls) {
        super();
        for(int i = 0; i < containedColls.length; i++) merge(containedColls[i]);
    }
    
    public void merge(Collection<? extends E> coll) {
    	colls.add(coll);
    }

	public void unmerge(Collection<?> coll) {
        for(Iterator<Collection<? extends E>> iter = colls.iterator(); iter.hasNext(); ) {
			Collection<?> containedColl = iter.next();
            if(containedColl == coll) iter.remove(); // only way to do this is to remove the EXACT same object as passed b/c if sets contain identical items they are "equal" via the equals() method
        }
    }

    public boolean add(E o) {
        throw new UnsupportedOperationException("Add is unsupported; use merge() to merge another set into this composite");
    }
    
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("Add is unsupported; use merge() to merge another set into this composite");
    }
    
    public void clear() {
        throw new UnsupportedOperationException("Clear is unsupported; use unmerge() to remove a set from this composite");
    }
    
    public boolean contains(Object o) {
        for(Iterator<Collection<? extends E>> iter = colls.iterator(); iter.hasNext(); ) {
			Collection<?> coll = iter.next();
            if(coll.contains(o)) return true;
        }
        return false;
    }
        
    public boolean isEmpty() {
		for(Iterator<Collection<? extends E>> iter = colls.iterator(); iter.hasNext();) {
			if(!iter.next().isEmpty())
				return false;
		}
		return true;
    }
    
    protected class CompositeIterator implements Iterator<E> { 
        protected final ListIterator<Collection<? extends E>> setsIter =  colls.listIterator();
        protected Iterator<? extends E> entryIter;
        protected E nextEntry;
        protected boolean hasNext;
        
        protected CompositeIterator() {
        	entryIter = setsIter.next().iterator();
        	hasNext = true;
        	moveNext();
        }
        public boolean hasNext() {
            return hasNext;
        }
        
        public E next() {
        	if(!hasNext()) throw new java.util.NoSuchElementException();
            E next = nextEntry;
            moveNext();
            return next;
        }
        
        public void remove() {
            throw new UnsupportedOperationException("Remove is unsupported for the composite; modify the underlying set directly");
        }
        
        protected void moveNext() {
        	while(hasNext) {
        		if(entryIter.hasNext()) {
        			nextEntry = entryIter.next();
        			break;
        		} else if(setsIter.hasNext()) {
        			entryIter = setsIter.next().iterator();
        		} else {
        			//done!
        			hasNext = false;
        			nextEntry = null;
        		}
        	}
        }
    }
    
    public Iterator<E> iterator() {
        return new CompositeIterator();
    }
    
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Remove is unsupported; use unmerge() to remove a set from this composite");
    }
    
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("RemoveAll is unsupported; use unmerge() to remove a set from this composite");
    }
    
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("RetainAll is unsupported; use unmerge() to remove a set from this composite");
    }
    
    public int size() {
        int size = 0;
        for(Iterator<Collection<? extends E>> iter = colls.iterator(); iter.hasNext(); ) {
        	size += iter.next().size();
        }
        return size;
    }   
}
