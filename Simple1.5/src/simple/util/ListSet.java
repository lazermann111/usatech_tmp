/*
 * ListSet.java
 *
 * Created on October 3, 2002, 5:04 PM
 */

package simple.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.Spliterator;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class ListSet<E> implements List<E>, Set<E> {

	protected List<E> list;
	protected Set<E> set;


	public ListSet() {
		this(new ArrayList<E>(), new HashSet<E>());
	}

	public ListSet(List<E> list, Set<E> set) {
		this.list = list;
		this.set = set;
	}

	public Spliterator<E> spliterator() {
		return list.spliterator();
	}

	public boolean contains(Object element) {
		return set.contains(element);
	}

	public E remove(int index) {
		E element = list.remove(index);
		set.remove(element);
		return element;
	}

	public Iterator<E> iterator() {
		return list.iterator();
	}

	public ListIterator<E> listIterator(int index) {
		return list.listIterator(index);
	}

	public void clear() {
		list.clear();
		set.clear();
	}

	public void add(int index, E element) {
		list.add(index, element);
		set.add(element);
	}

	public int size() {
		return list.size();
	}

	public int indexOf(Object element) {
		return list.indexOf(element);
	}

	public boolean isEmpty() {
		return set.isEmpty();
	}

	public boolean addAll(Collection<? extends E> collection) {
		list.addAll(collection);
		return set.addAll(collection);
	}

	public int lastIndexOf(Object element) {
		return list.lastIndexOf(element);
	}

	public E get(int index) {
		return list.get(index);
	}

	public boolean containsAll(Collection<?> collection) {
		return set.containsAll(collection);
	}

	public E set(int index, E element) {
		E old = list.set(index, element);
		set.add(element);
		if(!list.contains(old)) set.remove(element);
		return old;
	}

	public boolean remove(Object element) {
		if(!set.remove(element)) return false;
		while(list.remove(element)) ;
		return true;
	}

	public boolean addAll(int index, Collection<? extends E> collection) {
		set.addAll(collection);
		return list.addAll(index, collection);
	}

	public boolean add(E element) {
		list.add(element);
		return set.add(element);
	}

	public ListIterator<E> listIterator() {
		return list.listIterator();
	}

	public boolean retainAll(Collection<?> collection) {
		list.retainAll(collection);
		return set.retainAll(collection);
	}

	public List<E> subList(int fromIndex, int toIndex) {
		return list.subList(fromIndex, toIndex);
	}

	public boolean removeAll(Collection<?> collection) {
		list.removeAll(collection);
		return set.removeAll(collection);
	}

	public <T> T[] toArray(T[] a) {
		return list.toArray(a);
	}

	public Object[] toArray() {
		return list.toArray();
	}
}
