/*
 * IntervalThread.java
 *
 * Created on January 14, 2004, 12:14 PM
 */

package simple.util;

/**
 *
 * @author  Brian S. Krug
 */
public abstract class IntervalThread extends Thread {
    private boolean done = false;
    private boolean skip = false;
    private long interval = 0;
    
    /** Creates a new instance of IntervalThread */
    public IntervalThread() {
        super();
        init();
    }
    
    /** Creates a new instance of IntervalThread */
    public IntervalThread(String name) {
        super(name);
        init();
    }

    /** Creates a new instance of IntervalThread */
    public IntervalThread(ThreadGroup group, String name) {
        super(group, name);
        init();
    }

    protected void init() {
       // default some properties as appropriate
        setPriority(3);
        setDaemon(true);
    }
    
    public synchronized void run() {
        while(!done) {
            try {
                wait(interval);
            } catch(InterruptedException ie) {}
            if(done) {
                done = false; // for next time
                return;
            }
            if(!skip) action();
            skip = false;
        }
    }
    
    /** Stops the interval from occuring again. Once this is called start() must be called to
     *  resume execution. To disable this IntervalThread temporarily use setInterval with 0 as
     *  the interval.
     */
    public synchronized void finish() {
        done = true;
        notify();
    }
    /** Sets the number of milleseconds between each call to the aciton() method. Use 0 to
     *  disable this IntervalThread (i.e. - to wait indefinitely).
     *
     */
    public synchronized void setInterval(long interval) {
        this.interval = interval;
        if(isAlive()) {
            skip = true;
            notify();
        }
    }
    
    /**
     * This method is called each time the interval occurs
     */
    protected abstract void action() ;

    /** Getter for property interval.
     * @return Value of property interval.
     *
     */
    public long getInterval() {
        return interval;
    }
    
}
