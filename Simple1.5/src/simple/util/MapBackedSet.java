package simple.util;

import java.util.AbstractSet;
import java.util.ConcurrentModificationException;
import java.util.Map;

/**
 */
public class MapBackedSet<E> extends AbstractSet<E> implements java.util.Set<E> {
    private transient Map<E,Object> map;
    private static final Object PRESENT = new Object();// Dummy value to associate with an Object in the backing Map
    /**
     * Constructs a set; using the backing <ttMap</tt> instance
     */
    public MapBackedSet(Map<E,Object> map) {
        this.map = map;
    }
    /**
     * Adds the specified element to this set if it is not already
     * present.
     *
     * @param o element to be added to this set.
     * @return <tt>true</tt> if the set did not already contain the specified
     * element.
     */
    public boolean add(E o) {
        return map.put(o, PRESENT)==null;
    }
    /**
     * Removes all of the elements from this set.
     */
    public void clear() {
        map.clear();
    }
    /**
     * Returns <tt>true</tt> if this set contains the specified element.
     *
     * @return <tt>true</tt> if this set contains the specified element.
     */
    public boolean contains(Object o) {
        return map.containsKey(o);
    }
    /**
     * Returns <tt>true</tt> if this set contains no elements.
     *
     * @return <tt>true</tt> if this set contains no elements.
     */
    public boolean isEmpty() {
        return map.isEmpty();
    }
    /**
     * Returns an iterator over the elements in this set.  The elements
     * are returned in no particular order.
     *
     * @return an Iterator over the elements in this set.
     * @see ConcurrentModificationException
     */
    public java.util.Iterator<E> iterator() {
        return map.keySet().iterator();
    }
    /**
     * Removes the given element from this set if it is present.
     *
     * @param o object to be removed from this set, if present.
     * @return <tt>true</tt> if the set contained the specified element.
     */
    public boolean remove(Object o) {
        return map.remove(o)==PRESENT;
    }
    /**
     * Returns the number of elements in this set (its cardinality).
     *
     * @return the number of elements in this set (its cardinality).
     */
    public int size() {
        return map.size();
    }
}
