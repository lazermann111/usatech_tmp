package simple.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class FilteredIterator<E,D> implements Iterator<E> {
	protected Iterator<D> delegate;
	protected D next;
	
	public FilteredIterator(Iterator<D> delegate) {
		this.delegate = delegate;
		moveNext();
	}

	public boolean hasNext() {
		return next != null;
	}
	public E next() {
		if(!hasNext())
			throw new NoSuchElementException("End of iterator");
		E element = convert(next);
		moveNext();
		return element;
	}

	public void remove() {
		throw new UnsupportedOperationException("Filtered Iterator is unmodifiable");
	}
	
	protected boolean moveNext() {	
		while(delegate.hasNext()) {
			D element = delegate.next();
			if(accept(element)) {
				next = element;
				return true;
			}
		} 
		next = null;
		return false;
	}
	
	protected abstract boolean accept(D element) ;
	protected abstract E convert(D element) ;
	
}
