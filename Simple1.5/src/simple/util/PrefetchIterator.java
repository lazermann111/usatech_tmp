package simple.util;

import java.util.Iterator;

public abstract class PrefetchIterator<E> implements Iterator<E> {
	protected E nextEntry;
	protected boolean hasNext;

	protected PrefetchIterator() {
		hasNext = fetchNext();
	}

	public boolean hasNext() {
		return hasNext;
	}

	public E next() {
		if(!hasNext)
			throw new java.util.NoSuchElementException();
		E next = nextEntry;
		hasNext = fetchNext();
		return next;
	}

	public void remove() {
		throw new UnsupportedOperationException("Remove is not supported");
	}

	/**
	 * Implementations must get the next entry and if it exists set the <code>nextEntry</code> instance variable and return true.
	 * 
	 * @return
	 */
	protected abstract boolean fetchNext();
}
