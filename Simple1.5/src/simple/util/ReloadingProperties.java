/*
 * PropertiesLoader.java
 *
 * Created on January 19, 2004, 9:06 AM
 */

package simple.util;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.locks.ReentrantLock;

import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;

/**  This class poses as a Properties object but allows thread-safe reloading of its key-value pairs.
 *   It actually ignores the underlying Hashtable because hashtable is unnecessarily synchronized
 *
 * @author  Brian S. Krug
 */
public class ReloadingProperties extends Properties implements Loader {
    /**
	 * 
	 */
	private static final long serialVersionUID = -31241223480L;
	protected ConfigSource src;
    protected final ReentrantLock lock = new ReentrantLock();
    /** Creates a new instance of PropertiesLoader */
    public ReloadingProperties(ConfigSource src) {
        this.src = src;
    }
    
    public void load(ConfigSource src) throws LoadingException {
        // create a new properties set in case an error occurs
        Properties newProps = new Properties();
        try {
            newProps.load(src.getInputStream());
        } catch(IOException ioe) {
            throw new LoadingException("Could not read Properties input stream", ioe);
        }
        // now load them into the real properties object
        lock.lock();
        try {
            super.clear();
            super.putAll(newProps);
        } finally {
            lock.unlock();
        }
    }

    /* (non-Javadoc)
     * @see java.util.Hashtable#get(java.lang.Object)
     */
    @Override
    public synchronized Object get(Object key) {
        // TODO Auto-generated method stub
        return super.get(key);
    }

    /* (non-Javadoc)
     * @see java.util.Hashtable#put(K, V)
     */
    @Override
    public synchronized Object put(Object key, Object value) {
        // TODO Auto-generated method stub
        return super.put(key, value);
    }
    
    
    
}
