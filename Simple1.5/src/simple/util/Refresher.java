package simple.util;

import simple.app.ServiceException;

public interface Refresher {
	public void refresh() throws ServiceException;
}
