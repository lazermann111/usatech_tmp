package simple.util;

import java.lang.reflect.InvocationTargetException;

import simple.event.MapDataEvent;
import simple.event.MapDataListener;

/** An implementation of the <CODE>java.util.Map</CODE> interface that tracks any
 * additions, updates, or removals from itself and also tracks any property changes
 * in the key or value objects (if they have an addPropertyChangeListener() method and a
 * removePropertyChangeListener() method).
 */
public class TrackableMap extends java.util.AbstractMap {
    /** Used to represent an null value */    
	public static final Object EMPTY_VALUE = new Object();
	protected java.util.Map delegate = null;
	protected transient java.util.Set aMapDataListener = null;
	protected transient java.beans.PropertyChangeListener fieldKeyListener = null;
	protected transient java.beans.PropertyChangeListener fieldValueListener = null;
	protected static java.util.Map propertyChangeMethods = null;
	protected static final Object NOT_A_PROPERTYCHANGER = new Object();
	protected static final String ADD_PCL_METHOD_NAME = "addPropertyChangeListener";
	protected static final Class[] ADD_PCL_ARGS = new Class[]{java.beans.PropertyChangeListener.class};
	protected static final String REMOVE_PCL_METHOD_NAME = "removePropertyChangeListener";
	protected static final Class[] REMOVE_PCL_ARGS = ADD_PCL_ARGS;
	protected transient java.util.Set entrySet = null;

	protected class TrackableEntrySetIterator implements java.util.Iterator {
		protected java.util.Iterator delegateIterator = null;
		protected Object lastReturned = null;
		protected TrackableEntrySetIterator(java.util.Iterator delegate) {
			delegateIterator = delegate;
		}
		public boolean hasNext() {
			return delegateIterator.hasNext();
		}
		public Object next() {
			lastReturned = delegateIterator.next();
			return lastReturned;
		}
		public void remove() {
			delegateIterator.remove();
			if(lastReturned instanceof java.util.Map.Entry) {
				java.util.Map.Entry entry = (java.util.Map.Entry) lastReturned;
				fireKeyRemoved(entry.getKey(), entry.getValue());
			}
		}
	}
	protected class TrackableEntrySet extends java.util.AbstractSet {
		protected java.util.Set delegateEntrySet = null;
		protected TrackableEntrySet(java.util.Set delegate) {
			delegateEntrySet = delegate;
		}
		public void clear() {
			TrackableMap.this.clear();
		}
		public boolean contains(Object o) {
			return delegateEntrySet.contains(o);
		}
		public boolean containsAll(java.util.Collection c) {
			return delegateEntrySet.containsAll(c);
		}
		public boolean isEmpty() {
			return TrackableMap.this.isEmpty();
		}
		public java.util.Iterator iterator() {
			return new TrackableEntrySetIterator(delegateEntrySet.iterator());
		}
		public boolean remove(Object o) {
			boolean b = delegateEntrySet.remove(o);
			if(b && o instanceof java.util.Map.Entry) {
				java.util.Map.Entry entry = (java.util.Map.Entry) o;
				fireKeyRemoved(entry.getKey(), entry.getValue());
			}
			return b;
		}
		public boolean removeAll(java.util.Collection c) {
			boolean modified = false;
			java.util.Iterator e = iterator();
			while (e.hasNext()) {
			    if(c.contains(e.next())) {
					e.remove();
					modified = true;
	  		  	}
			}
			return modified;
		}
		public boolean retainAll(java.util.Collection c) {
			boolean modified = false;
			java.util.Iterator e = iterator();
			while (e.hasNext()) {
	 			if(!c.contains(e.next())) {
					e.remove();
					modified = true;
	 			}
			}
			return modified;
		}
		public int size() {
			return TrackableMap.this.size();
		}
		public Object[] toArray() {
			return delegateEntrySet.toArray();
		}
		public Object[] toArray(Object a[]) {
			return delegateEntrySet.toArray(a);
		}
		public String toString() {
			return delegateEntrySet.toString();
		}	
	}
/**
 * TrackableMap constructor comment.
 */
public TrackableMap() {
	this(new java.util.HashMap());
}
/** TrackableMap constructor comment.
 * @param initialCapacity
 */
public TrackableMap(int initialCapacity) {
	this(new java.util.HashMap(initialCapacity));
}
/** TrackableMap constructor comment.
 * @param map
 */
public TrackableMap(java.util.Map map) {
	super();
	delegate = map;
}
/** Add a MapDataListener.
 * @param newListener
 */
public void addMapDataListener(MapDataListener newListener) {
	if (aMapDataListener == null) aMapDataListener = new java.util.HashSet();
	aMapDataListener.add(newListener);
}
/**
 * clear method comment.
 */
public void clear() {
	java.util.Iterator iter = entrySet().iterator();
	while(iter.hasNext()) {
		java.util.Map.Entry entry = (java.util.Map.Entry) iter.next();
		if(entry != null) fireKeyRemoved(entry.getKey(),entry.getValue());
	}
	delegate.clear();
}
/** contains method comment.
 * @param key
 * @return
 */
public boolean containsKey(Object key) {
	return delegate.containsKey(key);
}
/** contains method comment.
 * @param value
 * @return
 */
public boolean containsValue(Object value) {
	return delegate.containsValue(value);
}
/** entrySet method comment.
 * @return
 */
public java.util.Set entrySet() {
	if (entrySet == null) entrySet = new TrackableEntrySet(delegate.entrySet());
	return entrySet;
}
	
/** equals method comment.
 * @param o
 * @return
 */
public boolean equals(Object o) {
	return delegate.equals(o);
}
	
/**
 * Notifies all listeners of a key being added
 */
protected void fireKeyAdded(Object key, Object value) {
	//install listeners
	if(key != null) installListeners(key, true);
	if(value != null) installListeners(value, false);
	
	if (aMapDataListener == null) return;
	MapDataEvent e = new MapDataEvent(this, MapDataEvent.KEY_ADDED, key, value, null);
	java.util.Iterator iter = aMapDataListener.iterator();
	MapDataListener tempListener = null;
	while(iter.hasNext()){
		tempListener = (MapDataListener)iter.next();
		if (tempListener != null) tempListener.keyAdded(e);
	}
}
/**
 * Method to support listener events.
 */
protected void fireKeyModified(java.beans.PropertyChangeEvent event) {
	if (aMapDataListener == null) return;
	MapDataEvent e = new MapDataEvent(this, MapDataEvent.KEY_MODIFIED, event.getSource(), null, null);
	java.util.Iterator iter = aMapDataListener.iterator();
	MapDataListener tempListener = null;
	while(iter.hasNext()){
		tempListener = (MapDataListener)iter.next();
		if (tempListener != null) tempListener.keyModified(e);
	}
}
/**
 * Notifies all listeners of a key being removed
 */
protected void fireKeyRemoved(Object key, Object value) {
	//remove listeners
	if(key != null) uninstallListeners(key, true);
	if(value != null) uninstallListeners(value, false);
	
	if (aMapDataListener == null) return;
	MapDataEvent e = new MapDataEvent(this, MapDataEvent.KEY_REMOVED, key, value, null);
	java.util.Iterator iter = aMapDataListener.iterator();
	MapDataListener tempListener = null;
	while(iter.hasNext()){
		tempListener = (MapDataListener)iter.next();
		if (tempListener != null) tempListener.keyRemoved(e);
	}
}
/**
 * Method to support listener events.
 */
protected void fireValueModified(java.beans.PropertyChangeEvent event) {
	if (aMapDataListener == null) return;
	MapDataEvent e = new MapDataEvent(this, MapDataEvent.VALUE_MODIFIED, null, event.getSource(), null);
	java.util.Iterator iter = aMapDataListener.iterator();
	MapDataListener tempListener = null;
	while(iter.hasNext()){
		tempListener = (MapDataListener)iter.next();
		if (tempListener != null) tempListener.valueModified(e);
	}
}
/**
 * Notifies all listeners of a key being added
 */
protected void fireValueReplaced(Object key, Object value, Object oldValue) {
	//install listeners
	if(oldValue != null ) uninstallListeners(oldValue, false);
	if(value != null) installListeners(value, false);
	
	if (aMapDataListener == null) return;
	MapDataEvent e = new MapDataEvent(this, MapDataEvent.VALUE_REPLACED, key, value, oldValue);
	java.util.Iterator iter = aMapDataListener.iterator();
	MapDataListener tempListener = null;
	while(iter.hasNext()){
		tempListener = (MapDataListener)iter.next();
		if (tempListener != null) tempListener.valueReplaced(e);
	}
}
/** get method comment.
 * @param key
 * @return
 */
public Object get(Object key) {
	return delegate.get(key);
}
/**
 * getKeyListener method comment.
 */
protected java.beans.PropertyChangeListener getKeyListener() {
	if(fieldKeyListener == null) {
		fieldKeyListener = new java.beans.PropertyChangeListener() {
			public void propertyChange(java.beans.PropertyChangeEvent e) {
				fireKeyModified(e);
			}
		};
	}
	return fieldKeyListener;
}
/**
 * getPropertyChangeMethods method comment.
 */
protected static java.lang.reflect.Method[] getPropertyChangeMethods(Class elementClass) {
	if(propertyChangeMethods == null) propertyChangeMethods = new java.util.HashMap();
	Object o = propertyChangeMethods.get(elementClass);
	if(o == NOT_A_PROPERTYCHANGER) {
		return null;
	} else if(o == null) {
		try {
			java.lang.reflect.Method m1 = elementClass.getMethod(ADD_PCL_METHOD_NAME, ADD_PCL_ARGS);
			java.lang.reflect.Method m2 = elementClass.getMethod(REMOVE_PCL_METHOD_NAME, REMOVE_PCL_ARGS);
			if(m1 != null && m2 != null) {
				java.lang.reflect.Method[] methods = new java.lang.reflect.Method[]{m1,m2};
				propertyChangeMethods.put(elementClass, methods);
				return methods;
			}
		} catch(NoSuchMethodException nsme) {
		} catch(SecurityException se) {
		}
		propertyChangeMethods.put(elementClass, NOT_A_PROPERTYCHANGER);
		return null;
	} else {
		return (java.lang.reflect.Method[])o;
	}
}
/** getSize method comment.
 * @return
 */
public int getSize() {
	return size();
}
/**
 * getValueListener method comment.
 */
protected java.beans.PropertyChangeListener getValueListener() {
	if(fieldValueListener == null) {
		fieldValueListener = new java.beans.PropertyChangeListener() {
			public void propertyChange(java.beans.PropertyChangeEvent e) {
				fireValueModified(e);
			}
		};
	}
	return fieldValueListener;
}
/** hashCode method comment.
 * @return
 */
public int hashCode() {
	return delegate.hashCode();
}
/**
 * install method comment.
 */
protected void installListeners(Object element, boolean isKey) {
	java.beans.PropertyChangeListener pcl = isKey ? getKeyListener() : getValueListener();
	if(element instanceof PropertyChanger) {
		((PropertyChanger)element).addPropertyChangeListener(pcl);
	} else if(element != null) {
		java.lang.reflect.Method[] methods = getPropertyChangeMethods(element.getClass());
		if(methods != null) {
			try {
				methods[0].invoke(element,new Object[] {pcl});
			} catch(IllegalAccessException e) {
				//Log.logException(getClass().getName(),"installListeners",e,"While invoking addPropertyChangeListener on" + element.toString());
			} catch (IllegalArgumentException e) {
                //Log.logException(getClass().getName(),"installListeners",e,"While invoking addPropertyChangeListener on" + element.toString());
            } catch (InvocationTargetException e) {
                //Log.logException(getClass().getName(),"installListeners",e,"While invoking addPropertyChangeListener on" + element.toString());
            }
		}
	}
}
/** isEmpty method comment.
 * @return
 */
public boolean isEmpty() {
	return delegate.isEmpty();
}
/** put method comment.
 * @param key
 * @param value
 * @return
 */
public Object put(Object key, Object value) {
	boolean contains = delegate.containsKey(key);
	Object oldValue = delegate.put(key, value);

	if(!contains || oldValue == EMPTY_VALUE) {
		fireKeyAdded(key,value);
	} else if(value != oldValue) {
		fireValueReplaced(key,value, oldValue);
	}

	return oldValue;
}
/** Puts the key-value pair into the map WITHOUT notifying listeners.
 * @param key
 * @param value
 * @return
 */
public Object putSilently(Object key, Object value) {
	boolean contains = delegate.containsKey(key);
	Object oldValue = delegate.put(key, value);

	//install listeners
	if(contains) {
		if(key != null) installListeners(key, true);
		if(value != null) installListeners(value, false);
	} else if(value != oldValue) {
		if(oldValue != null ) uninstallListeners(oldValue, false);
		if(value != null) installListeners(value, false);
	}

	return oldValue;
}
/** remove method comment.
 * @param key
 * @return
 */
public Object remove(Object key) {
	Object value = delegate.remove(key);
	if(value == EMPTY_VALUE) value = null;
	fireKeyRemoved(key,value);
	return value;
}
/** Remove a MapDataListener.
 * @param newListener
 */
public void removeMapDataListener(MapDataListener newListener) {
	if (aMapDataListener != null) aMapDataListener.remove(newListener);
}
/** Removes the key-value pair from the map WITHOUT motifying listeners.
 * @param key
 * @return
 */
public Object removeSilently(Object key) {
	Object value = delegate.remove(key);
	//remove listeners
	if(key != null) uninstallListeners(key, true);
	if(value != null) uninstallListeners(value, false);
	return value;
}
/** size method comment.
 * @return
 */
public int size() {
	return delegate.size();
}
/**
 * uninstallListeners method comment.
 */
protected void uninstallListeners(Object element, boolean isKey) {
	java.beans.PropertyChangeListener pcl = isKey ? getKeyListener() : getValueListener();
	if(element instanceof PropertyChanger) {
		((PropertyChanger)element).removePropertyChangeListener(pcl);
	} else if(element != null) {
		java.lang.reflect.Method[] methods = getPropertyChangeMethods(element.getClass());
		if(methods != null) {
			try {
				methods[1].invoke(element,new Object[] {pcl});
			} catch(IllegalAccessException e) {
				//Log.logException(getClass().getName(),"uninstallListeners",e,"While invoking removePropertyChangeListener on" + element.toString());
			} catch (IllegalArgumentException e) {
                //Log.logException(getClass().getName(),"uninstallListeners",e,"While invoking removePropertyChangeListener on" + element.toString());
            } catch (InvocationTargetException e) {
                //Log.logException(getClass().getName(),"uninstallListeners",e,"While invoking removePropertyChangeListener on" + element.toString());
            }
		}
	}
}
}
