package simple.util;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import simple.bean.ConvertUtils;

public abstract class ConversionMap<K0,K1,V0,V1> extends AbstractMap<K1,V1> {
	protected final Map<K0,V0> delegate;
	protected transient Set<Map.Entry<K1, V1>> entrySet;
	protected transient Set<K1> keySet;
	protected transient Collection<V1> values;

	protected class ConversionEntrySet extends ConversionSet<Map.Entry<K0, V0>, Map.Entry<K1, V1>> {
		public ConversionEntrySet(Set<Entry<K0, V0>> delegate) {
			super(delegate);
		}
		@Override
		protected Entry<K1, V1> convertFrom(final Entry<K0, V0> object0) {
			return new Entry<K1, V1>() {
				public K1 getKey() {
					return convertKeyFrom(object0.getKey());
				}
				public V1 getValue() {
					return convertValueFrom(object0.getValue());
				}
				public V1 setValue(V1 value) {
					return convertValueFrom(object0.setValue(convertValueTo(value)));
				}
				@Override
				public boolean equals(Object o) {
				    if (!(o instanceof Map.Entry<?,?>))
					return false;
				    Map.Entry<?,?> e = (Map.Entry<?,?>)o;
				    return ConvertUtils.areEqual(getKey(), e.getKey()) && ConvertUtils.areEqual(getValue(), e.getValue());
				}

				@Override
				public int hashCode() {
				    return ((getKey()   == null)   ? 0 :   getKey().hashCode()) ^
					   ((getValue() == null)   ? 0 : getValue().hashCode());
				}

				@Override
				public String toString() {
				    return getKey() + "=" + getValue();
				}
			};
		}
		@Override
		protected Entry<K0, V0> convertTo(final Entry<K1, V1> object1) {
			return new Entry<K0, V0>() {
				public K0 getKey() {
					return convertKeyTo(object1.getKey());
				}
				public V0 getValue() {
					return convertValueTo(object1.getValue());
				}
				public V0 setValue(V0 value) {
					return convertValueTo(object1.setValue(convertValueFrom(value)));
				}
				@Override
				public boolean equals(Object o) {
				    if (!(o instanceof Map.Entry<?,?>))
					return false;
				    Map.Entry<?,?> e = (Map.Entry<?,?>)o;
				    return ConvertUtils.areEqual(getKey(), e.getKey()) && ConvertUtils.areEqual(getValue(), e.getValue());
				}

				@Override
				public int hashCode() {
				    return ((getKey()   == null)   ? 0 :   getKey().hashCode()) ^
					   ((getValue() == null)   ? 0 : getValue().hashCode());
				}

				@Override
				public String toString() {
				    return getKey() + "=" + getValue();
				}
			};
		}
		@Override
		protected boolean isReversible() {
			return isValueReversible();
		}
	}

	protected class ConversionKeySet extends ConversionSet<K0, K1> {
		public ConversionKeySet(Set<K0> delegate) {
			super(delegate);
		}

		@Override
		protected K1 convertFrom(K0 object0) {
			return convertKeyFrom(object0);
		}

		@Override
		protected K0 convertTo(K1 object1) {
			return convertKeyTo(object1);
		}

		@Override
		protected boolean isReversible() {
			return true;
		}
	}

	protected class ConversionValues extends ConversionCollection<V0, V1> {
		public ConversionValues(Collection<V0> delegate) {
			super(delegate);
		}

		@Override
		protected V1 convertFrom(V0 object0) {
			return convertValueFrom(object0);
		}

		@Override
		protected V0 convertTo(V1 object1) {
			return convertValueTo(object1);
		}

		@Override
		protected boolean isReversible() {
			return isValueReversible();
		}
	}

	public ConversionMap(Map<K0, V0> delegate) {
		super();
		this.delegate = delegate;
	}

	@Override
	public void clear() {
		delegate.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return delegate.containsKey(convertKeyTo(castKey1(key)));
	}

	@Override
	public boolean containsValue(Object value) {
		return delegate.containsValue(convertValueTo(castValue1(value)));
	}

	@Override
	public Set<Map.Entry<K1, V1>> entrySet() {
		if(entrySet == null)
			entrySet = new ConversionEntrySet(delegate.entrySet());
		return entrySet;
	}

	@Override
	public V1 get(Object key) {
		return convertValueFrom(delegate.get(convertKeyTo(castKey1(key))));
	}

	@Override
	public boolean isEmpty() {
		return delegate.isEmpty();
	}

	@Override
	public Set<K1> keySet() {
		if(keySet == null)
			keySet = new ConversionKeySet(delegate.keySet());
		return keySet;
	}

	@Override
	public V1 put(K1 key, V1 value) {
		return convertValueFrom(delegate.put(convertKeyTo(key), convertValueTo(value)));
	}

	@Override
	public void putAll(Map<? extends K1, ? extends V1> t) {
		for(Map.Entry<? extends K1, ? extends V1> entry : t.entrySet()) {
			put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public V1 remove(Object key) {
		return convertValueFrom(delegate.remove(convertKeyTo(castKey1(key))));
	}

	@Override
	public int size() {
		return delegate.size();
	}

	@Override
	public Collection<V1> values() {
		if(values == null)
			values = new ConversionValues(delegate.values());
		return values;
	}

	@SuppressWarnings("unchecked")
	protected K1 castKey1(Object key1) {
		return (K1)key1;
	}

	@SuppressWarnings("unchecked")
	protected V1 castValue1(Object value1) {
		return (V1)value1;
	}

	/** Converts the value presented by this Map into the original value of the delegate
	 * @param value1
	 * @return
	 */
	protected abstract V0 convertValueTo(V1 value1) ;
	/** Converts the original value of the delegate into the value presented by this Map
	 * @param value0
	 * @return
	 */
	protected abstract V1 convertValueFrom(V0 value0) ;
	/** Converts the key presented by this Map into the original key of the delegate
	 * @param key1
	 * @return
	 */
	protected abstract K0 convertKeyTo(K1 key1) ;
	/** Converts the original key of the delegate into the key presented by this Map
	 * @param key0
	 * @return
	 */
	protected abstract K1 convertKeyFrom(K0 key0) ;

	/** Whether the conversion from V0 to V1 is supported. If it is supported then modification
	 * operations will be passed to the delegate otherwise, UnsupportedOperationException's will be
	 * thrown in {@link #put(K1,V1)} and {@link #remove(Object)}
	 * @return <code>true</code> if and only if {@link #convertValueFrom(V0)} is fully implemented
	 */
	protected abstract boolean isValueReversible() ;

}
