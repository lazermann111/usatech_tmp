/*
 * Created on Apr 11, 2005
 *
 */
package simple.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;

import simple.util.concurrent.Cache;
import simple.util.concurrent.CacheException;
import simple.util.concurrent.LockSegmentCache;

public class ObjectFactory<K, V> {
    protected Cache<K, V, CacheException> cache = new LockSegmentCache<K, V, CacheException>(100, 0.75f, 16) {
		@Override
		protected V createValue(K key, Object... additionalInfo) throws CacheException {
			try {
				return createObject();
			} catch(IllegalArgumentException e) {
				throw new CacheException(e);
			} catch(InstantiationException e) {
				throw new CacheException(e);
			} catch(IllegalAccessException e) {
				throw new CacheException(e);
			} catch(InvocationTargetException e) {
				throw new CacheException(e);
			}
		}
		@Override
		protected boolean keyEquals(K key1, K key2) {
			return key1.equals(key2);//Should never be null
		}
		@Override
		protected boolean valueEquals(V value1, V value2) {
			return value1.equals(value2);
		}
    };
    protected Constructor<? extends V> constructor = null;
    protected final static Class<?>[] ZERO_PARAM_CLASSES = new Class[0];
    protected final static Object[] ZERO_PARAM_OBJECTS = new Object[0];

    public ObjectFactory(Constructor<? extends V> constructor) {
        this.constructor = constructor;
    }

    public ObjectFactory(Class<? extends V> objectClass) throws SecurityException, NoSuchMethodException {
        this.constructor = objectClass.getConstructor(ZERO_PARAM_CLASSES);
    }

    public V getObject(K id) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
        try {
			return cache.getOrCreate(id);
		} catch(CacheException e) {
			if(e.getCause() instanceof IllegalArgumentException)
				throw (IllegalArgumentException) e.getCause();
			if(e.getCause() instanceof InstantiationException)
				throw (InstantiationException) e.getCause();
			if(e.getCause() instanceof IllegalAccessException)
				throw (IllegalAccessException) e.getCause();
			if(e.getCause() instanceof InvocationTargetException)
				throw (InvocationTargetException) e.getCause();
			throw new UndeclaredThrowableException(e);
		}
    }

    protected V createObject() throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return getConstructor().newInstance(ZERO_PARAM_OBJECTS);
    }

    public Constructor<? extends V> getConstructor() {
        return constructor;
    }


    public void setConstructor(Constructor<? extends V> constructor) {
        this.constructor = constructor;
    }

}
