package simple.util.concurrent;

import java.util.concurrent.Future;

public interface ResettableFuture<V> extends Future<V> {
	/** Attempts to reset the future so that it re-runs its
	 *  calculations to a result. If the Future is done, then it
	 *  may be reset. If it is still initializing or running, then it will
	 *  not be reset and the method will return <code>false</code>.
	 * @return Whether the future was successfully reset
	 */
	public boolean reset() ;

	/** Returns whether this future will reset itself when an exception occurs
	 *  or it is cancelled. If <code>true</code> then the Future will try to
	 *  run each time get() is called until successful.
	 * @return Whether the future will reset itself on an exception
	 */
	public boolean isResetOnException() ;

	/** Sets whether this future will reset itself when an exception occurs
	 *  or it is cancelled. If <code>true</code> then the Future will try to
	 *  run each time get() is called until successful.
	 * @param resetOnException Whether the future will reset itself on an exception
	 */
	public void setResetOnException(boolean resetOnException) ;


}
