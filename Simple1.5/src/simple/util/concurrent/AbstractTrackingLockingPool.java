package simple.util.concurrent;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;

import simple.io.Log;
import simple.lang.Decision;

public abstract class AbstractTrackingLockingPool<E> implements BoundedPool<E> {
	private static final Log log = Log.getLog();
	protected static class Tracker<E> {
		protected final E object;
		protected final long createdTime;
		protected final StackTraceElement[] stackTrace;
		public Tracker(E object) {
			this.object = object;
			this.createdTime = System.currentTimeMillis();
			this.stackTrace = new Throwable().getStackTrace();
		}
	}
	protected class InnerLockingPool extends AbstractLockingPool<Tracker<E>> {
		@Override
		protected void closeObject(Tracker<E> obj) throws Exception {
			AbstractTrackingLockingPool.this.closeObject(obj.object);
		}
		@Override
		protected Tracker<E> createObject(long timeout) throws CreationException, InterruptedException {
			return new Tracker<E>(AbstractTrackingLockingPool.this.createObject(timeout));
		}
		public void appendInfo(StringBuilder sb) {
			long time = System.currentTimeMillis();
			lock.lock();
			try {
				for(Object o : objects) {
					Tracker<E> tracker = (Tracker<E>) o;
					sb.append("\tObject ").append(tracker.object).append(" [Created ")
						.append((time - tracker.createdTime) / 1000).append(" seconds ago from ")
						.append(Arrays.toString(tracker.stackTrace)).append("]\n");
				}
			} finally {
				lock.unlock();
			}
		}
	};
	protected InnerLockingPool delegate = new InnerLockingPool();
	protected final Map<E,Tracker<E>> lookup = new ConcurrentHashMap<E, Tracker<E>>();
	protected abstract E createObject(long timeout) throws CreationException, InterruptedException ;
	protected abstract void closeObject(E obj) throws Exception ;
	public E borrowObject() throws TimeoutException, CreationException, InterruptedException {
		StringBuilder sb = new StringBuilder();
		sb.append("Object requested [NumActive=").append(delegate.getNumActive()).append("; MaxActive=").append(delegate.getMaxActive()).append("]\n");
		delegate.appendInfo(sb);
		log.debug(sb);
		Tracker<E> tracker = delegate.borrowObject();
		lookup.put(tracker.object, tracker);
		log.debug("Object borrowed [" + tracker.object + " | " + tracker + "]");
		return tracker.object;
	}

	public void invalidateObject(E obj) {
		Tracker<E> tracker = lookup.remove(obj);
		log.debug("Object invalidated [" + obj + " | " + tracker + "]");
		delegate.invalidateObject(tracker);
	}

	public void returnObject(E obj) {
		Tracker<E> tracker = lookup.remove(obj);
		log.debug("Object returned [" + obj + " | " + tracker + "]");
		delegate.returnObject(tracker);
	}
	
	public boolean addObject() throws CreationException, TimeoutException, InterruptedException {
		return delegate.addObject();
	}
	
	public int evict(final Decision<E> evictDecision, int maxEvictable, int minIdle) {
		return delegate.evict(new Decision<Tracker<E>>() {
			public boolean decide(Tracker<E> argument) {
				return evictDecision.decide(argument.object);
			}
		}, maxEvictable, minIdle);
	}
	/**
	 * @see simple.util.concurrent.Pool#clear()
	 */
	public void clear() {
		delegate.clear();
	}
	/**
	 * @see simple.util.concurrent.Pool#close()
	 */
	public void close() {
		delegate.close();
	}
	public int getMaxActive() {
		return delegate.getMaxActive();
	}
	public int getMaxIdle() {
		return delegate.getMaxIdle();
	}
	public long getMaxWait() {
		return delegate.getMaxWait();
	}
	public int getNumActive() {
		return delegate.getNumActive();
	}
	public int getNumIdle() {
		return delegate.getNumIdle();
	}
	public void setMaxActive(int maxActive) {
		delegate.setMaxActive(maxActive);
	}
	public void setMaxIdle(int maxIdle) {
		delegate.setMaxIdle(maxIdle);
	}
	public void setMaxWait(long maxWait) {
		delegate.setMaxWait(maxWait);
	}

}
