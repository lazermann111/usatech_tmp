/**
 *
 */
package simple.util.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author Brian S. Krug
 *
 */
public class TrivialFuture<V> implements Future<V> {
	protected final V value;
	public TrivialFuture(V value) {
		this.value = value;
	}
	/**
	 * @see java.util.concurrent.Future#cancel(boolean)
	 */
	public boolean cancel(boolean mayInterruptIfRunning) {
		return false;
	}

	/**
	 * @see java.util.concurrent.Future#get()
	 */
	public V get() throws InterruptedException, ExecutionException {
		return value;
	}

	/**
	 * @see java.util.concurrent.Future#get(long, java.util.concurrent.TimeUnit)
	 */
	public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		return value;
	}

	/**
	 * @see java.util.concurrent.Future#isCancelled()
	 */
	public boolean isCancelled() {
		return false;
	}

	/**
	 * @see java.util.concurrent.Future#isDone()
	 */
	public boolean isDone() {
		return true;
	}
}
