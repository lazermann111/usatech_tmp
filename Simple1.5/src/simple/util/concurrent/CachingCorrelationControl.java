package simple.util.concurrent;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import simple.util.FilteredIterator;

public class CachingCorrelationControl<E> implements CorrelationControl<E> {
	protected final LockSegmentCache<E, AtomicInteger, RuntimeException> cache;
	protected int limit;
	protected final Set<E> limited = new AbstractSet<E>() {
		@Override
		public Iterator<E> iterator() {
			return new FilteredIterator<E, Map.Entry<E, AtomicInteger>>(cache.entrySet().iterator()) {
				@Override
				protected boolean accept(Entry<E, AtomicInteger> element) {
					return element.getValue().get() >= getLimit(element.getKey());
				}

				@Override
				protected E convert(Entry<E, AtomicInteger> element) {
					return element.getKey();
				}
			};
		}

		@Override
		public int size() {
			int count = 0;
			Iterator<E> iter = iterator();
			while(iter.hasNext()) {
				count++;
				iter.next();
			}
			return count;
		}

		@Override
		public boolean contains(Object o) {
			@SuppressWarnings("unchecked")
			E e = (E) o;
			return isLimited(e);
		}
	};

	public CachingCorrelationControl() {
		this(100, 0.75f, 8);
	}

	public CachingCorrelationControl(int initialCapacity, float loadFactor, int concurrencyLevel) {
		cache = new LockSegmentCache<E, AtomicInteger, RuntimeException>(initialCapacity, loadFactor, concurrencyLevel) {
			@Override
			protected AtomicInteger createValue(E key, Object... additionalInfo) {
				return new AtomicInteger();
			}

			@Override
			protected Segment<E, AtomicInteger, RuntimeException> createSegment(int capacity, float loadFactor) {
				return new LockSegment<E, AtomicInteger, RuntimeException>(capacity, loadFactor, MAXIMUM_CAPACITY) {
					private static final long serialVersionUID = 1267599141134L;

					@Override
					protected AtomicInteger createValue(E key, Object... additionalInfo) {
						return cache.createValue(key, additionalInfo);
					}

					@Override
					protected boolean keyEquals(E key1, E key2) {
						return cache.keyEquals(key1, key2);
					}

					@Override
					protected boolean valueEquals(AtomicInteger value1, AtomicInteger value2) {
						return cache.valueEquals(value1, value2);
					}

					@Override
					protected void rehash() {
						// remove values with 0
						HashEntry<E, AtomicInteger>[] tab = table;
						int c = count;
						for(int index = 0; index < tab.length; index++) {
							HashEntry<E, AtomicInteger> first = tab[index];
							HashEntry<E, AtomicInteger> e = first;
							while(e != null) {
								if(e.value.compareAndSet(0, -1)) {
									++modCount;
									HashEntry<E, AtomicInteger> newFirst = e.next;
									for(HashEntry<E, AtomicInteger> p = first; p != e; p = p.next)
										newFirst = new HashEntry<E, AtomicInteger>(p.key, p.hash, newFirst, p.value);
									tab[index] = newFirst;
									c--;
								}
								e = e.next;
							}
						}
						count = c;
						if(c > threshold) // ensure capacity
							super.rehash();
					}
				};
			}
		};
	}


	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	@Override
	public Set<E> getLimited() {
		return limited;
	}

	public boolean isLimited(E corr) {
		AtomicInteger use = cache.getIfPresent(corr);
		return use != null && use.get() >= getLimit(corr);
	}
	@Override
	public void increment(E corr) {
		AtomicInteger use = cache.getOrCreate(corr);
		use.incrementAndGet();
	}

	@Override
	public void decrement(E corr) {
		AtomicInteger use = cache.getIfPresent(corr);
		if(use == null)
			throw new IllegalStateException("Correlation '" + corr + "' has zero uses - it cannot be decremented");
		int n;
		do {
			n = use.get();
			if(n <= 0)
				throw new IllegalStateException("Correlation '" + corr + "' has zero uses - it cannot be decremented");
		} while(!use.compareAndSet(n, n - 1));
	}

	@Override
	public int getLimit(E corr) {
		return getLimit();
	}

	@Override
	public int getCount(E corr) {
		AtomicInteger use = cache.getIfPresent(corr);
		if(use == null)
			return 0;
		return use.get();
	}

	@Override
	public String getCountInfo() {
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<E, AtomicInteger> entry : cache.entrySet()) {
			int n = entry.getValue().get();
			if(n > 0) {
				if(sb.length() > 0)
					sb.append("; ");
				sb.append(entry.getKey()).append('=').append(n);
			}
		}
		return sb.toString();
	}
}
