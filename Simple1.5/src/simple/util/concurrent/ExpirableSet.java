/**
 * 
 */
package simple.util.concurrent;

import java.util.Set;

public interface ExpirableSet<E> extends Set<E> {
	 public ExpirableIterator<E> iterator() ;
}