package simple.util.concurrent;

import simple.bean.ConvertUtils;

public abstract class LockSegmentCache<K,V,I extends Exception> extends SegmentCache<K,V,I> {
	public LockSegmentCache() {
		super(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR, DEFAULT_SEGMENTS);
	}
	public LockSegmentCache(int initialCapacity, float loadFactor, int concurrencyLevel) {
		super(initialCapacity, loadFactor, concurrencyLevel);
	}

	@Override
	protected Segment<K,V,I> createSegment(int capacity, float loadFactor) {
		return new LockSegment<K,V,I>(capacity, loadFactor, MAXIMUM_CAPACITY) {
			private static final long serialVersionUID = 123499099141134L;
			@Override
			protected V createValue(K key, Object... additionalInfo) throws I {
				return LockSegmentCache.this.createValue(key, additionalInfo);
			}

			@Override
			protected boolean keyEquals(K key1, K key2) {
				return LockSegmentCache.this.keyEquals(key1, key2);
			}

			@Override
			protected boolean valueEquals(V value1, V value2) {
				return LockSegmentCache.this.valueEquals(value1, value2);
			}
		};
	}

	protected abstract V createValue(K key, Object... additionalInfo) throws I ;
	protected boolean keyEquals(K key1, K key2) {
		return ConvertUtils.areEqual(key1, key2);
	}
	@Override
	protected boolean valueEquals(V value1, V value2) {
		return ConvertUtils.areEqual(value1, value2);
	}
}
