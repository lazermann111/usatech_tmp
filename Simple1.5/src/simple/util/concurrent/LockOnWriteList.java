package simple.util.concurrent;

import java.util.AbstractSequentialList;
import java.util.Collection;
import java.util.Collections;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.concurrent.locks.ReentrantLock;

import simple.util.CollectionUtils;

/** This is like a linked list only it allows thread-safe concurrent access and synchronized modification
 * @author Brian S. Krug
 *
 * @param <E>
 */
public class LockOnWriteList<E> extends AbstractSequentialList<E> {
	protected final ReentrantLock lock = new ReentrantLock();
	protected class Entry {
		protected E element;
		protected Entry next;
		protected Entry previous;

		protected Entry(E element, Entry next, Entry previous) {
		    this.element = element;
		    this.next = next;
		    this.previous = previous;
		}
    }
	protected class ListItr implements ListIterator<E> {
		private Entry lastReturned = header;
		private Entry next;
		private int nextIndex;

		public ListItr(int index) {
		    if (index < 0 /*|| index > size*/)
		    	throw new IndexOutOfBoundsException("Index: "+index+", Size: "+size);
		    //Because size is not constant during this method we can only use the foward-style means of starting at index
		    //if (index < (size >> 1)) {
				next = header.next;
				for (nextIndex=0; next != header && nextIndex<index; nextIndex++)
				    next = next.next;
		    /*} else {
				next = header;
				for (nextIndex=size; nextIndex>index; nextIndex--)
				    next = next.previous;
		    }*/
		}

		public boolean hasNext() {
		    //return nextIndex != size;
			return next != header;
		}

		public E next() {
		    if (next == header)
		    	throw new NoSuchElementException();

		    lastReturned = next;
		    next = next.next;
		    nextIndex++;
		    return lastReturned.element;
		}

		public boolean hasPrevious() {
		    return next.previous != header;
		}

		public E previous() {
		    if (next.previous == header)
		    	throw new NoSuchElementException();

		    lastReturned = next = next.previous;
		    nextIndex--;
		    return lastReturned.element;
		}

		public int nextIndex() {
		    return nextIndex;
		}

		public int previousIndex() {
		    return nextIndex-1;
		}

		public void remove() {
            Entry lastNext = lastReturned.next;
            try {
                removeEntry(lastReturned);
            } catch (NoSuchElementException e) {
                throw new IllegalStateException();
            }
		    if (next==lastReturned)
                next = lastNext;
            else
            	nextIndex--;
		    lastReturned = header;
		}

		public void set(E o) {
		    if(lastReturned == header)
				throw new IllegalStateException();
		    lastReturned.element = o;
		}

		public void add(E o) {
		    lastReturned = header;
		    addBeforeEntry(o, next);
		    nextIndex++;
		}
    }

	protected transient final Entry header = new Entry(null, null, null);
	protected transient volatile int size = 0;
	protected final ListIterator<E> EMPTY_ITERATOR = Collections.<E>emptyList().listIterator();
    /**
     * Constructs an empty list.
     */
    public LockOnWriteList() {
        header.next = header.previous = header;
    }

    protected Entry addBeforeEntry(E element, Entry entry) {
    	lock.lock();
		try {
			Entry newEntry = new Entry(element, entry, entry.previous);
			newEntry.previous.next = newEntry;
			newEntry.next.previous = newEntry;
			size++;
			return newEntry;
		} finally {
			lock.unlock();
		}
    }

    protected E removeEntry(Entry entry) {
    	lock.lock();
		try {
			if (entry == header)
			    throw new NoSuchElementException();
			E result = entry.element;
			entry.previous.next = entry.next;
			entry.next.previous = entry.previous;
			//entry.next = entry.previous = null;
			entry.element = null;
			size--;
			return result;
		} finally {
			lock.unlock();
		}
    }

	@Override
	public ListIterator<E> listIterator(int index) {
		return new ListItr(index);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean add(E o) {
		addBeforeEntry(o, header);
        return true;
    }

	@Override
	public boolean remove(Object o) {
		lock.lock();
		try {
			if (o==null) {
	            for (Entry e = header.next; e != header; e = e.next) {
	                if (e.element==null) {
	                    removeEntry(e);
	                    return true;
	                }
	            }
	        } else {
	            for (Entry e = header.next; e != header; e = e.next) {
	                if (o.equals(e.element)) {
	                	removeEntry(e);
	                    return true;
	                }
	            }
	        }
	        return false;
		} finally {
			lock.unlock();
		}

    }
	@Override
	public boolean removeAll(Collection<?> c) {
		boolean updated = false;
		lock.lock();
		try {
			for (Entry e = header.next; e != header; e = e.next) {
                if(c.contains(e.element)) {
                    removeEntry(e);
                    updated = true;
                }
            }
		} finally {
			lock.unlock();
		}
        return updated;
    }

	@Override
	public boolean retainAll(Collection<?> c) {
		boolean updated = false;
		lock.lock();
		try {
			for (Entry e = header.next; e != header; e = e.next) {
                if(!c.contains(e.element)) {
                    removeEntry(e);
                    updated = true;
                }
            }
		} finally {
			lock.unlock();
		}
        return updated;
    }
	@Override
	public boolean addAll(Collection<? extends E> c) {
		Entry first = new Entry(null, null, null);
		Entry next = first;
		int n = 0;
		for(E e : c) {
			next.element = e;
			next.next = new Entry(null, null, next);
			next = next.next;
			n++;
		}
		if(n == 0)
			return false;
		lock.lock();
		try {
			first.previous = header.previous;
			next.previous.next = header;
			header.previous.next = first;
			header.previous = next.previous;
			size += n;
		} finally {
			lock.unlock();
		}
		return true;
	}

    @Override
	public boolean addAll(int index, Collection<? extends E> c) {
    	Entry first = new Entry(null, null, null);
		Entry next = first;
		int n = 0;
		for(E e : c) {
			next.element = e;
			next.next = new Entry(null, null, next);
			next = next.next;
			n++;
		}
		if(n == 0)
			return false;
		lock.lock();
		try {
			Entry entry = header.next;
			for(int i = 0; i < index; i++) {
				if(entry == header)
					throw new NoSuchElementException("Index: "+index+", Size: "+size);
				entry = entry.next;
			}
			first.previous = entry.previous;
			next.previous.next = entry;
			entry.previous.next = first;
			entry.previous = next.previous;
			size += n;
		} finally {
			lock.unlock();
		}
		return true;
    }

    /**
     * Removes all of the elements from this list.
     */
    @Override
	public void clear() {
    	lock.lock();
		try {
			header.next = header.previous = header;
			size = 0;
		} finally {
			lock.unlock();
		}
    }

    /**
     * Returns the first element in this list.
     *
     * @return the first element in this list.
     * @throws    NoSuchElementException if this list is empty.
     */
    public E getFirst() {
    	Entry entry = header.next;
		if (entry == header)
		    throw new NoSuchElementException();
		return entry.element;
    }

    /**
     * Returns the last element in this list.
     *
     * @return the last element in this list.
     * @throws    NoSuchElementException if this list is empty.
     */
    public E getLast()  {
		Entry entry = header.previous;
		if (entry == header)
		    throw new NoSuchElementException();
		return entry.element;
    }

    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element from this list.
     * @throws    NoSuchElementException if this list is empty.
     */
    public E removeFirst() {
    	return removeEntry(header.next);
    }

    /**
     * Removes and returns the last element from this list.
     *
     * @return the last element from this list.
     * @throws    NoSuchElementException if this list is empty.
     */
    public E removeLast() {
    	return removeEntry(header.previous);
    }

    /**
     * Inserts the given element at the beginning of this list.
     *
     * @param o the element to be inserted at the beginning of this list.
     */
    public void addFirst(E o) {
    	addBeforeEntry(o, header.next);
    }

    /**
     * Appends the given element to the end of this list.  (Identical in
     * function to the <tt>add</tt> method; included only for consistency.)
     *
     * @param o the element to be inserted at the end of this list.
     */
    public void addLast(E o) {
    	addBeforeEntry(o, header);
    }

	@Override
	public boolean isEmpty() {
		return header.next == header;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a) {
		Entry entry = header.next;
		Object[] result = a;
		int i = 0;
		for(; i < a.length; i++) {
			if(entry == header) {
				result[i] = null;
				return (T[])result;
			}
			result[i] = entry.element;
			entry = entry.next;
		}
		int sz = size();
		if(entry == header || a.length >= sz)
			return (T[])result;
		Object[] tmp = CollectionUtils.createArray(a, sz);
		System.arraycopy(result, 0, tmp, 0, a.length);
		for(; i < tmp.length; i++) {
			if(entry == header) {
				Object[] tmp2 = CollectionUtils.createArray(a, i);
				System.arraycopy(tmp, 0, tmp2, 0, tmp2.length);
				return (T[])tmp2;
			}
			tmp[i] = entry.element;
			entry = entry.next;
		}
		return (T[])tmp;
	}
	@Override
	public Object[] toArray() {
		Entry entry = header.next;
		int sz = size();
		Object[] tmp = new Object[sz];
		for(int i = 0; i < tmp.length; i++) {
			if(entry == header) {
				Object[] tmp2 = new Object[i];
				System.arraycopy(tmp, 0, tmp2, 0, tmp2.length);
				return tmp2;
			}
			tmp[i] = entry.element;
			entry = entry.next;
		}
		return tmp;
	}
}
