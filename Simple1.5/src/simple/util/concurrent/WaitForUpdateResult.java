package simple.util.concurrent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class WaitForUpdateResult<E> {
	protected final CountDownLatch latch = new CountDownLatch(1);
	protected E result;

	public E awaitResult() throws InterruptedException {
		latch.await();
		return result;
	}

	public E awaitResult(long timeout) throws InterruptedException, TimeoutException {
		if(latch.await(timeout, TimeUnit.MILLISECONDS))
			return result;
		throw new TimeoutException("Result not set in " + timeout + " milliseconds");
	}

	public void setResult(E result) {
		this.result = result;
		latch.countDown();
	}
}
