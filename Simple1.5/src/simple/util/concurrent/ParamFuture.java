/**
 *
 */
package simple.util.concurrent;

import java.util.concurrent.Future;

/**
 * @author Brian S. Krug
 *
 */
public interface ParamFuture<V> extends Future<V> {
	Object[] getParams() ;
	void setParams(Object[] params) ;
}
