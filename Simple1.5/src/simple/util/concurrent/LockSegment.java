package simple.util.concurrent;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Segments are specialized versions of hash tables. This subclasses from
 * ReentrantLock opportunistically, just to simplify some locking and avoid
 * separate construction.
 */
public abstract class LockSegment<K,V,I extends Exception> extends ReentrantLock implements Segment<K,V,I> {
	/**
	 * ConcurrentHashMap list entry. This was changed to be exported out as a
	 * user-visible Map.Entry. The only issue is that setValue may return a
	 * previous old value instead of the correct current old value.
	 *
	 * Because the value field is volatile, not final, it is legal wrt the Java
	 * Memory Model for an unsynchronized reader to see null instead of initial
	 * value when read via a data race. Although a reordering leading to this is
	 * not likely to ever actually occur, the Segment.readValueUnderLock method
	 * is used as a backup in case a null (pre-initialized) value is ever seen
	 * in an unsynchronized access method.
	 */
	protected static class HashEntry<K, V> implements Map.Entry<K, V> {
		protected final K key;
		protected final int hash;
		protected volatile V value;
		protected final HashEntry<K, V> next;

		protected HashEntry(K key, int hash, HashEntry<K, V> next, V value) {
			this.key = key;
			this.hash = hash;
			this.next = next;
			this.value = value;
		}

		public K getKey() {
			return key;
		}

		public V getValue() {
			return value;
		}

		public V setValue(V value) {
			V oldValue = this.value;
			this.value = value;
			return oldValue;
		}
	}

	protected final Iterator<Map.Entry<K, V>> emptyIterator = new Iterator<Map.Entry<K, V>>() {
		public boolean hasNext() {
			return false;
		}

		public Map.Entry<K, V> next() {
			throw new NoSuchElementException();
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	};

	protected class EntryIterator implements Iterator<Map.Entry<K, V>> {
		protected int index = 0;
		protected HashEntry<K, V> next;
		protected HashEntry<K, V> prev;
		protected final HashEntry<K,V>[] tab;
		protected final int len;

		protected EntryIterator(HashEntry<K,V>[] tab) {
			this.tab = tab;
			this.len = this.tab.length;
			moveFirst();
		}

		protected void moveFirst() {
			next = null;
			index = -1;
			while(next == null && index < len -1)
				next = tab[++index];

		}

		protected void moveNext() {
			next = next.next;
			while(next == null && index < len-1)
				next = tab[++index];
		}

		public boolean hasNext() {
			return next != null;
		}

		public Map.Entry<K, V> next() {
			prev = next;
			moveNext();
			if(prev.value == null)
				readValueUnderLock(prev); // recheck
			return prev;
		}

		public void remove() {
			if(prev == null)
				throw new IllegalStateException();
			LockSegment.this.remove(prev.key, prev.hash, null);
		}
	};

	/*
	 * Segments maintain a table of entry lists that are ALWAYS kept in a
	 * consistent state, so can be read without locking. Next fields of nodes
	 * are immutable (final). All list additions are performed at the front of
	 * each bin. This makes it easy to check changes, and also fast to traverse.
	 * When nodes would otherwise be changed, new nodes are created to replace
	 * them. This works well for hash tables since the bin lists tend to be
	 * short. (The average length is less than two for the default load factor
	 * threshold.)
	 *
	 * Read operations can thus proceed without locking, but rely on selected
	 * uses of volatiles to ensure that completed write operations performed by
	 * other threads are noticed. For most purposes, the "count" field, tracking
	 * the number of elements, serves as that volatile variable ensuring
	 * visibility. This is convenient because this field needs to be read in
	 * many read operations anyway:
	 *  - All (unsynchronized) read operations must first read the "count"
	 * field, and should not look at table entries if it is 0.
	 *  - All (synchronized) write operations should write to the "count" field
	 * after structurally changing any bin. The operations must not take any
	 * action that could even momentarily cause a concurrent read operation to
	 * see inconsistent data. This is made easier by the nature of the read
	 * operations in Map. For example, no operation can reveal that the table
	 * has grown but the threshold has not yet been updated, so there are no
	 * atomicity requirements for this with respect to reads.
	 *
	 * As a guide, all critical volatile reads and writes to the count field are
	 * marked in code comments.
	 */

	private static final long serialVersionUID = 2249069246763182397L;

	/**
	 * The number of elements in this segment's region.
	 */
	protected transient volatile int count;

	/**
	 * Number of updates that alter the size of the table. This is used during
	 * bulk-read methods to make sure they see a consistent snapshot: If
	 * modCounts change during a traversal of segments computing size or
	 * checking containsValue, then we might have an inconsistent view of state
	 * so (usually) must retry.
	 */
	protected transient int modCount;

	/**
	 * The table is rehashed when its size exceeds this threshold. (The value of
	 * this field is always (int)(capacity * loadFactor).)
	 */
	protected transient int threshold;

	/**
	 * The per-segment table. Declared as a raw type, casted to HashEntry<K,V>
	 * on each use.
	 */
	protected transient volatile HashEntry<K,V>[] table;

	/**
	 * The load factor for the hash table. Even though this value is same for
	 * all segments, it is replicated to avoid needing links to outer object.
	 *
	 * @serial
	 */
	protected final float loadFactor;

	protected final int maximumCapacity;

	public LockSegment(int initialCapacity, float lf, int maximumCapacity) {
		loadFactor = lf;
		this.maximumCapacity = maximumCapacity;
		setTable(newTable(initialCapacity));
	}

	@SuppressWarnings("unchecked")
	protected HashEntry<K,V>[] newTable(int length) {
		return new HashEntry[length];
	}

	/**
	 * Set table to new HashEntry array. Call only while holding lock or in
	 * constructor.
	 */
	protected void setTable(HashEntry<K,V>[] newTable) {
		threshold = (int) (newTable.length * loadFactor);
		table = newTable;
	}

	/**
	 * Return properly casted first entry of bin for given hash
	 */
	protected HashEntry<K, V> getFirst(int hash) {
		final HashEntry<K, V>[] tab = table;
		return tab[hash & (tab.length - 1)];
	}

	/**
	 * Read value field of an entry under lock. Called if value field ever
	 * appears to be null. This is possible only if a compiler happens to
	 * reorder a HashEntry initialization with its table assignment, which is
	 * legal under memory model but is not known to ever occur.
	 */
	protected V readValueUnderLock(HashEntry<K, V> e) {
		lock();
		try {
			return e.value;
		} finally {
			unlock();
		}
	}

	/* Specialized implementations of map methods */
	public V get(K key, int hash, boolean createIfAbsent, Object... additionalInfo) throws I {
		if(count != 0) { // read-volatile
			HashEntry<K, V> e = getFirst(hash);
			while(e != null) {
				if(e.hash == hash && keyEquals(key, e.key)) {
					V v = e.value;
					if(v != null)
						return v;
					v = readValueUnderLock(e); // recheck
					if(v != null)
						return v;
					else
						break;
				}
				e = e.next;
			}
		}
		return createIfAbsent ? createIfAbsent(key, hash, additionalInfo) : null;
	}

	protected V createIfAbsent(K key, int hash, Object... additionalInfo) throws I {
		lock();
		try {
			int c = count;
			if(c++ > threshold) // ensure capacity
				rehash();
			HashEntry<K,V>[] tab = table;
			int index = hash & (tab.length - 1);
			HashEntry<K, V> first = tab[index];
			HashEntry<K, V> e = first;
			while(e != null && (e.hash != hash || !keyEquals(key, e.key)))
				e = e.next;

			if(e != null) {
				return e.value;
			} else {
				++modCount;
				V value = createValue(key, additionalInfo);
				tab[index] = new HashEntry<K, V>(key, hash, first, value);
				count = c; // write-volatile
				return value;
			}
		} finally {
			unlock();
		}
	}

	protected abstract V createValue(K key, Object... additionalInfo) throws I;

	protected abstract boolean valueEquals(V value1, V value2);

	protected abstract boolean keyEquals(K key1, K key2);

	public boolean containsKey(K key, int hash) {
		if(count != 0) { // read-volatile
			HashEntry<K, V> e = getFirst(hash);
			while(e != null) {
				if(e.hash == hash && keyEquals(key, e.key))
					return true;
				e = e.next;
			}
		}
		return false;
	}

	public boolean containsValue(V value) {
		if(count != 0) { // read-volatile
			HashEntry<K,V>[] tab = table;
			int len = tab.length;
			for(int i = 0; i < len; i++) {
				for(HashEntry<K, V> e = tab[i]; e != null; e = e.next) {
					V v = e.value;
					if(v == null) // recheck
						v = readValueUnderLock(e);
					if(valueEquals(value, v))
						return true;
				}
			}
		}
		return false;
	}

	public V replace(K key, int hash, V oldValue, V newValue) {
		lock();
		try {
			HashEntry<K, V> e = getFirst(hash);
			while(e != null && (e.hash != hash || !keyEquals(key, e.key)))
				e = e.next;

			V replacedValue = null;
			if(e != null && (oldValue == null || valueEquals(oldValue, e.value))) {
				replacedValue = e.value;
				e.value = newValue;
			}
			return replacedValue;
		} finally {
			unlock();
		}
	}

	public V put(K key, int hash, V value, boolean onlyIfAbsent) {
		lock();
		try {
			int c = count;
			if(c++ > threshold) // ensure capacity
				rehash();
			HashEntry<K,V>[] tab = table;
			int index = hash & (tab.length - 1);
			HashEntry<K, V> first = tab[index];
			HashEntry<K, V> e = first;
			while(e != null && (e.hash != hash || !keyEquals(key, e.key)))
				e = e.next;

			V oldValue;
			if(e != null) {
				oldValue = e.value;
				if(!onlyIfAbsent)
					e.value = value;
			} else {
				oldValue = null;
				++modCount;
				tab[index] = new HashEntry<K, V>(key, hash, first, value);
				count = c; // write-volatile
			}
			return oldValue;
		} finally {
			unlock();
		}
	}

	protected void rehash() {
		HashEntry<K,V>[] oldTable = table;
		int oldCapacity = oldTable.length;
		if(oldCapacity >= maximumCapacity)
			return;

		/*
		 * Reclassify nodes in each list to new Map. Because we are using
		 * power-of-two expansion, the elements from each bin must either stay
		 * at same index, or move with a power of two offset. We eliminate
		 * unnecessary node creation by catching cases where old nodes can be
		 * reused because their next fields won't change. Statistically, at the
		 * default threshold, only about one-sixth of them need cloning when a
		 * table doubles. The nodes they replace will be garbage collectable as
		 * soon as they are no longer referenced by any reader thread that may
		 * be in the midst of traversing table right now.
		 */

		HashEntry<K,V>[] newTable = newTable(oldCapacity << 1);
		threshold = (int) (newTable.length * loadFactor);
		int sizeMask = newTable.length - 1;
		for(int i = 0; i < oldCapacity; i++) {
			// We need to guarantee that any existing reads of old Map can
			// proceed. So we cannot yet null out each bin.
			HashEntry<K, V> e = oldTable[i];

			if(e != null) {
				HashEntry<K, V> next = e.next;
				int idx = e.hash & sizeMask;

				// Single node on list
				if(next == null)
					newTable[idx] = e;

				else {
					// Reuse trailing consecutive sequence at same slot
					HashEntry<K, V> lastRun = e;
					int lastIdx = idx;
					for(HashEntry<K, V> last = next; last != null; last = last.next) {
						int k = last.hash & sizeMask;
						if(k != lastIdx) {
							lastIdx = k;
							lastRun = last;
						}
					}
					newTable[lastIdx] = lastRun;

					// Clone all remaining nodes
					for(HashEntry<K, V> p = e; p != lastRun; p = p.next) {
						int k = p.hash & sizeMask;
						HashEntry<K, V> n = newTable[k];
						newTable[k] = new HashEntry<K, V>(p.key, p.hash, n, p.value);
					}
				}
			}
		}
		table = newTable;
	}

	/**
	 * Remove; match on key only if value null, else match both.
	 */
	public V remove(K key, int hash, V value) {
		lock();
		try {
			int c = count - 1;
			HashEntry<K,V>[] tab = table;
			int index = hash & (tab.length - 1);
			HashEntry<K, V> first = tab[index];
			HashEntry<K, V> e = first;
			while(e != null && (e.hash != hash || !keyEquals(key, e.key)))
				e = e.next;

			V oldValue = null;
			if(e != null) {
				V v = e.value;
				if(value == null || valueEquals(value, v)) {
					oldValue = v;
					// All entries following removed node can stay
					// in list, but all preceding ones need to be
					// cloned.
					++modCount;
					HashEntry<K, V> newFirst = e.next;
					for(HashEntry<K, V> p = first; p != e; p = p.next)
						newFirst = new HashEntry<K, V>(p.key, p.hash, newFirst, p.value);
					tab[index] = newFirst;
					count = c; // write-volatile
				}
			}
			return oldValue;
		} finally {
			unlock();
		}
	}

	public void clear() {
		if(count != 0) {
			lock();
			try {
				HashEntry<K,V>[] tab = table;
				for(int i = 0; i < tab.length; i++)
					tab[i] = null;
				++modCount;
				count = 0; // write-volatile
			} finally {
				unlock();
			}
		}
	}

	public int count() {
		return count;
	}

	public Iterator<Map.Entry<K, V>> iterator() {
		if(count != 0) { // read-volatile
			return new EntryIterator(table);
		} else {
			return emptyIterator;
		}
	}
}
