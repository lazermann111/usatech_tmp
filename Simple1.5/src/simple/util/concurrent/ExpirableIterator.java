/**
 * 
 */
package simple.util.concurrent;

import java.util.Iterator;

public interface ExpirableIterator<E> extends Iterator<E> {
	public boolean expire() ;
}