package simple.util.concurrent;

import java.util.Set;

public interface CorrelationControl<E> {
	public Set<E> getLimited();

	public void increment(E corr);

	public void decrement(E corr);

	public int getLimit(E corr);

	public int getCount(E corr);

	public String getCountInfo();
}
