/*
 * Created on Aug 24, 2005
 *
 */
package simple.util.concurrent;

public class CreationException extends Exception {
    private static final long serialVersionUID = -8843317501L;

    public CreationException() {
        super();
    }

    public CreationException(String message) {
        super(message);
    }

    public CreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreationException(Throwable cause) {
        super(cause);
    }

}
