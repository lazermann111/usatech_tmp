package simple.util.concurrent;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * Allows for caching the results of a call based on a set of arguments and returning the cached version if available
 * or executing the specified callable to create the results. Results can be expired and the cache size can be limited
 * (although this max cache size is approximate since it is implemented in each segment and not globally).
 * Many of the internal workings of this class were copied from java.util.concurrent.ConcurrentHashMap.
 *
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public abstract class LockSegmentFutureCache<K,V> extends AbstractSegmentedFutureCache<K,V> {
	protected final AtomicInteger size = new AtomicInteger(0);

    public LockSegmentFutureCache(int maxSize, int initialCapacity, float loadFactor, int concurrencyLevel) {
		super(initialCapacity, loadFactor, concurrencyLevel);
		setMaxSize(maxSize);
	}
    public LockSegmentFutureCache(int initialCapacity, float loadFactor, int concurrencyLevel) {
		super(initialCapacity, loadFactor, concurrencyLevel);
	}

	public int size() {
		return size.get();
	}

	@Override
	protected Segment<K, CacheFuture, RuntimeException> createSegment(int capacity, float loadFactor) {
		return new LockLinkedSegment<K, CacheFuture, RuntimeException>(capacity, loadFactor, MAXIMUM_CAPACITY) {
			private static final long serialVersionUID = -93848140014L;
			@Override
			protected CacheFuture createValue(K key, Object... additionalInfo) {
				return createFuture(key, additionalInfo);
			}

			@Override
			protected int decrementSize() {
				return size.decrementAndGet();
			}

			@Override
			protected int deltaSize(int delta) {
				return size.addAndGet(delta);
			}

			@Override
			protected int getMaxSize() {
				return LockSegmentFutureCache.this.getMaxSize();
			}

			@Override
			protected int incrementSize() {
				return size.incrementAndGet();
			}
			@Override
			protected boolean expire(CacheFuture value) {
				return value.reset();
			}
		};
	}

}
