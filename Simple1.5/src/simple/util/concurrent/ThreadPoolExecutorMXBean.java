package simple.util.concurrent;

public interface ThreadPoolExecutorMXBean {

	public long getTotalProcessingCount();

	public long getTotalLatencyTime();

	public long getTotalProcessingTime();

	public int getSampleSize();

	public Double getAvgLatencyTime();

	public Double getAvgProcessingTime();

	public int getActiveCount();

	public long getCompletedTaskCount();

	public int getCorePoolSize();

	public int getLargestPoolSize();

	public int getMaximumPoolSize();

	public int getPoolSize();

	public long getTaskCount();
}