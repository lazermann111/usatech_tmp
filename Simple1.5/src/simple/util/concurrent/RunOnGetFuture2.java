package simple.util.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/** An implementation of Future that executes the specified callable the first time
 *  the get() method is called. It also allows for cancelling the Callable if the
 *  Callable implements CancellableCallable. In addition, this object may be reset()
 *  so that the Callable will be executed again on the next get() invocation. This
 *  implementation is thread-safe, but attempts to use a ReentrantLock only
 *  when absolutely necessary in order to optimize performance. NOTE: the callable
 *  object passed to the constructor of this class, DOES NOT get nulled out. If you
 *  don't care to reset() this future then clean-up in the callable's call method before
 *  returning the result.
 *
 * NOTE: There is a scenario under which get() will return null. If get() and reset() are called at roughly the same time, the switch in
 * get() may run and find that state = DONE, then the reset() method may update the state to RESETTING and the result to null, which the get() method
 * may then return. The intricacies of thread-safe non-locking processing are seriously complicated, thus the RunOnGetFuture class
 * offers an alternative safer, but potentially slightly slower implementation that locks on each call.
 *
 * NOTE: Some rough tests (simple.test.InitialerTest.testRunOnGetFuture()) revealed that at least in the test case, the RunOnGetFuture implementation
 * was faster than this implementation!
 *
 * @author Brian S. Krug
 *
 * @param <V>
 */
public class RunOnGetFuture2<V> implements ResettableFuture<V> {
	protected final static int CREATED = 0;
	protected final static int CANCELLED = 1;
	protected final static int RUNNING = 2;
	protected final static int DONE = 3;
	protected final static int EXCEPTION = 4;
	protected final static int RESETTING = 5;
	private final ReentrantLock lock = new ReentrantLock();
	protected Callable<V> callable;
	private final AtomicInteger state = new AtomicInteger(CREATED);
	private V result;
	private ExecutionException exception;
	private Condition signal;
	private volatile boolean waiting = false;
	private boolean resetOnException;

	public RunOnGetFuture2(Callable<V> callable) {
		this.callable = callable;
	}

	protected RunOnGetFuture2() {
	}

	/**
	 * @see java.util.concurrent.Future#cancel(boolean)
	 */
	public boolean cancel(boolean mayInterruptIfRunning) {
		switch(state.intValue()) {
			case CREATED:
				if(state.compareAndSet(CREATED, CANCELLED)) {
					return true;
				}
				else return cancel(mayInterruptIfRunning);
			case RUNNING:
				Callable<V> localCallable = callable;//for read consistency
				if(!mayInterruptIfRunning || !(localCallable instanceof CancellableCallable<?>)) return false;
				if(((CancellableCallable<V>)localCallable).cancel() && state.compareAndSet(RUNNING, CANCELLED)) {
					//callable = null; // allow gc - can't do this for resettable
					if(waiting) {
						lock.lock();
						try {
							signal.signalAll();
						} finally {
							waiting = false;
							lock.unlock();
						}
					}
					return true;
				}
			case CANCELLED: case EXCEPTION: case DONE: case RESETTING: default:
				return false;
		}
	}

	/**
	 * @see java.util.concurrent.Future#isCancelled()
	 */
	public boolean isCancelled() {
		return state.intValue() == CANCELLED;
	}

	/**
	 * @see java.util.concurrent.Future#isDone()
	 */
	public boolean isDone() {
		switch(state.intValue()) {
			case CANCELLED:
			case EXCEPTION:
			case DONE:
				return true;
			default:
				return false;
		}
	}

	/**
	 * @see java.util.concurrent.Future#get()
	 */
	public V get() throws InterruptedException, ExecutionException {
		try {
			return get(-1, null);
		} catch(TimeoutException e) {
			throw new IllegalStateException("This should never happen");
		}
	}

	/**
	 * @see java.util.concurrent.Future#get(long, java.util.concurrent.TimeUnit)
	 */
	public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		if(isResetOnException()) {
			int localState = state.intValue();
			// only try once, since reset() is the only thing that will change
			// status from CANCELLED, EXCEPTION, or DONE to something else
			switch(localState) {
				case CANCELLED:// always reset cancelled to created (no cleanup needed)
					state.compareAndSet(localState, CREATED);
					break;
				case EXCEPTION:
					if(state.compareAndSet(localState, RESETTING)) {
						lock.lock();
						try {
							resettingRunResult(result);
							result = null;
							exception = null;
							state.compareAndSet(RESETTING, CREATED);
						} finally {
							lock.unlock();
						}
					}
			}
		}
		switch(state.intValue()) {
			case CREATED:
				if(state.compareAndSet(CREATED, RUNNING)) {
					try {
						V r =  call();
						runComplete(r);
						result = r;
						state.compareAndSet(RUNNING, DONE);
						return r; // Use local variable to prevent race condition with reset
					} catch(Exception e) {
						ExecutionException ee = new ExecutionException(e);
						runFailed(ee);
						exception = ee;
						state.compareAndSet(RUNNING, EXCEPTION);
						throw ee;
					} finally {
						//callable = null; // for gc - can't do this for resettable
						if(waiting) {
							lock.lock();
							try {
								signal.signalAll();
							} finally {
								waiting = false;
								lock.unlock();
							}
						}
					}
				}
			case RUNNING: case RESETTING:
				// wait until done
				lock.lock();
				try {
					if(signal == null)
						signal = lock.newCondition();
					switch(state.intValue()) {
						case RUNNING: case RESETTING:
							waiting = true;
							if(timeout >= 0 && unit != null) {
								if(!signal.await(timeout, unit)) {//timeout
									throw new TimeoutException("Callable not complete after " + timeout + " " + unit.toString());
								}
							} else {
								signal.await();
							}
					}
				} finally {
					//waiting = false; This wasn't here but probably should be  - need to test with it in
					lock.unlock();
				}
				return get(timeout, unit);
			case CANCELLED:
				throw new CancellationException();
			case EXCEPTION:
				throwingRunException(exception);
				throw exception;
			case DONE: default:
				returningRunResult(result);
				return result;
		}
	}

	public boolean reset() {
		int localState = state.intValue();
		// only try once, since reset() is the only thing that will change
		// status from CANCELLED, EXCEPTION, or DONE to something else
		switch(localState) {
			case CREATED:
			case RUNNING:
			case RESETTING:
				return false;
			case CANCELLED:// always reset cancelled to created (no cleanup needed)
				return state.compareAndSet(localState, CREATED);
			case EXCEPTION:
			case DONE:
				if(state.compareAndSet(localState, RESETTING)) {
					lock.lock();
					try {
						resettingRunResult(result);
						result = null;
						exception = null;
						boolean ret = state.compareAndSet(RESETTING, CREATED);
						if(waiting) {
							signal.signalAll();
						}
						return ret;
					} finally {
						waiting = false;
						lock.unlock();
					}
				}
				return false;
		}
		throw new IllegalStateException("State is illegal value '" + localState + "'");
	}

	protected int getState() {
		return state.intValue();
	}

	/** Hook for sub-classes if they wish to know that a run of the callable was successfully completed
	 * @param result
	 *
	 */
	protected void runComplete(V result) {
		//do nothing
	}

	/** Hook for sub-classes if they wish to know that a run of the callable failed
	 * @param e
	 *
	 */
	protected void runFailed(ExecutionException exception) {
		//do nothing
	}

	/** Hook for sub-classes if they wish to know that the run exception was re-thrown from a get() call
	 *
	 */
	protected void throwingRunException(ExecutionException exception) {
		//do nothing
	}
	/** Hook for sub-classes if they wish to know that the run result was returned to another call to get()
	 *
	 */
	protected void returningRunResult(V result) {
		//do nothing
	}
	/** Hook for sub-classes if they wish to perform additional cleanup upon reset
	 *
	 */
	protected void resettingRunResult(V result) {
		//do nothing
	}
	public Callable<V> getCallable() {
		return callable;
	}

	public void setCallable(Callable<V> callable) {
		this.callable = callable;
	}

	protected V call() throws Exception {
		return callable.call();
	}

	public boolean isResetOnException() {
		return resetOnException;
	}

	public void setResetOnException(boolean resetOnException) {
		this.resetOnException = resetOnException;
	}
}
