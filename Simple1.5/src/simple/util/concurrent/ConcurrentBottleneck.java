package simple.util.concurrent;

import simple.util.CollectionUtils;

/** This class allows the specified level of concurrency for the Bottleneck interface.
 *  It is fully thread-safe. NOTE: the number of idle ReentrantLock's can be as high
 *  as 'maxIdle * concurrency'.
 * @author bkrug
 *
 * @param <E>
 */
public class ConcurrentBottleneck<E> implements Bottleneck<E> {
	protected final SingleLockBottleneck<E>[] table;
	
	@SuppressWarnings("unchecked")
	public ConcurrentBottleneck(int concurrency) {
		this.table = new SingleLockBottleneck[concurrency];
		for(int i = 0; i < this.table.length; i++) {
			this.table[i] = new SingleLockBottleneck<E>();
		}
	}
	
	public void lock(E resource) throws InterruptedException {
		table[calcIndex(resource)].lock(resource);
	}
	public void unlock(E resource) {
		table[calcIndex(resource)].unlock(resource);
	}
	protected int calcIndex(E resource) {
		return (CollectionUtils.deepHashCode(resource) & 0x7FFFFFFF) % table.length;
	}

	public int getMaxIdle() {
		return table[0].getMaxIdle();
	}
	public void setMaxIdle(int maxIdle) {
		for(int i = 0; i < table.length; i++) {
			table[i].setMaxIdle(maxIdle);
		}
	}
	public int getConcurrency() {
		return table.length;
	}
}
