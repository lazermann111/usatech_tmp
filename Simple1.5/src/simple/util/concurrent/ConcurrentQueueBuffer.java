/**
 * 
 */
package simple.util.concurrent;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrentQueueBuffer<E> implements QueueBuffer<E>, Iterable<E> {
	protected class Node {
		public Node(E item) {
			this.item = item;
		}

		protected final E item;
		protected volatile Node next;
	}
	protected Node head = new Node(null);
	protected volatile Node tail = head;
    protected final AtomicInteger count = new AtomicInteger(); /** Number of items in the queue */
    protected final ReentrantLock takeLock = new ReentrantLock();
    protected final ReentrantLock putLock = new ReentrantLock();
    protected final Condition notEmpty = takeLock.newCondition();
    protected final Condition notFull = putLock.newCondition();
	protected int capacity;

	protected class InnerIterator implements Iterator<E> {
		protected Node next = head.next;

		@Override
		public boolean hasNext() {
			return next != null;
		}

		@Override
		public E next() {
			E ret = next.item;
			next = next.next;
			return ret;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Modification through the iterator is not supported");
		}
	}

	public ConcurrentQueueBuffer(int capacity) {
		this.capacity = capacity;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) throws InterruptedException {
		lockInterruptibly();
		try {
			this.capacity = capacity;
		} finally {
			unlock();
		}
	}

	public int size() {
		return count.get();
	}

	public E take() throws InterruptedException {
		int c;
		E x;
		takeLock.lockInterruptibly();
		try {
			LOOP: while(true) {
				try {
					while(count.get() == 0)
						notEmpty.await();
				} catch(InterruptedException ie) {
					notEmpty.signal(); // propagate to a non-interrupted thread
					throw ie;
				}
				switch(getReadiness(head.next.item)) {
					case NEVER:
						head = head.next;
						c = count.decrementAndGet();
						break;
					case LAST:
						Node n = head.next;
						while(n.next != null) {
							Readiness r = getReadiness(n.next.item);
							switch(r) {
								case NEVER:
									c = removeNext(n);
									break;
								case READY:
									x = n.next.item;
									c = removeNext(n);
									break LOOP;
							}
							n = n.next;
						}
						// fall-thru: no READY items, use next LAST item
					case READY:
						head = head.next;
						x = head.item;
						c = count.decrementAndGet();
						removedItem(x, c);
						if(c > 0)
							notEmpty.signal();
						break LOOP;
				/* We don't need this functionality right now so don't introduce additional complexity by adding it
				case NOT_YET:
				c = count.get();
				n = head.next;
				Node l = null;
				while(n.next != tail) {
					switch(getReadiness(n.next.item)) {
						case NEVER:
							n.next = n.next.next;
							c = count.decrementAndGet();
							break;
						case READY:
							x = n.next.item;
							n.next = n.next.next;
							c = count.decrementAndGet();
							if(c > 0)
								notEmpty.signal();
							break LOOP;
						case LAST:
							if(l == null)
								l = n;
					}
					n = n.next;
				}
				if(l != null) {
					x = n.next.item;
					n.next = n.next.next;
					c = count.decrementAndGet();
					if(c > 0)
						notEmpty.signal();
					break LOOP;
				}
				// wait until something is ready or new item is added
				break;
				*/
				}
			}
		} finally {
			takeLock.unlock();
		}
		if (c < capacity)
            signalNotFull();
        return x;
	}

	protected void addedItem(E item, int newCount) {
	}

	protected void removedItem(E item, int newCount) {
	}

	protected int removeNext(Node n) throws InterruptedException {
		if(n.next == tail) { // this could change before we execute tail = n so we must lock putLock
			lockInterruptibly();
			try {
				if(n.next == tail)
					tail = n;
			} finally {
				unlock();
			}
		}
		n.next = n.next.next;
		int c = count.decrementAndGet();
		if(n.next != null)
			removedItem(n.next.item, c);
		if(c > 0)
			notEmpty.signal();
		return c;
	}

	protected Readiness getReadiness(E item) {
		return Readiness.READY;
	}

	protected void signalNotFull() {
		// tell pollers to get more
		putLock.lock();
        try {
            notFull.signal();
        } finally {
            putLock.unlock();
        }
	}

	protected void signalNotEmpty() {
        takeLock.lock();
        try {
            notEmpty.signal();
        } finally {
            takeLock.unlock();
        }
    }

	public void put(E item, boolean awaitNotFull) throws InterruptedException {
		lockInterruptibly();
		try {
			if(awaitNotFull)
				awaitNotFullUnderLock();
			putUnderLock(item);
		} finally {
			unlock();
		}
	}

	public void put(Iterator<E> items, boolean awaitNotFull) throws InterruptedException {
		lockInterruptibly();
		try {
			if(awaitNotFull)
				awaitNotFullUnderLock();
			while(items.hasNext())
				putUnderLock(items.next());
		} finally {
			unlock();
		}
	}

	protected void lockInterruptibly() throws InterruptedException {
		putLock.lockInterruptibly();
	}

	protected void unlock() {
		putLock.unlock();
	}

	protected void putUnderLock(E item) {
		tail = tail.next = new Node(item);
        int c = count.getAndIncrement();
		addedItem(item, c + 1);
        if(c == 0)
            signalNotEmpty();
	}

	protected void awaitNotFullUnderLock() throws InterruptedException {
		while(count.get() >= capacity) {
			try {
				notFull.await();
			} catch (InterruptedException ie) {
				notFull.signal(); // propagate to a non-interrupted thread
                throw ie;
            }
		}
	}

	@Override
	public void awaitNotFull() throws InterruptedException {
		lockInterruptibly();
		try {
			awaitNotFullUnderLock();
		} finally {
			unlock();
		}
	}

	public Iterator<E> iterator() {
		return new InnerIterator();
	}
}