package simple.util.concurrent;


/**
 * Sets a max size limit on a cache
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public interface BoundedCache<K,V,I extends Exception> extends Cache<K,V,I> {
    public int getMaxSize() ;
	public void setMaxSize(int maxSize) ;
}
