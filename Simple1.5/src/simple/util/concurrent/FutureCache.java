package simple.util.concurrent;

/**
 * Allows for caching the results of a call based on a set of arguments and returning the cached version if available
 * or executing the specified callable to create the results. Results can be expired and the cache size can be limited
 * (although this max cache size is approximate since it is implemented in each segment and not globally).
 * Many of the internal workings of this class were copied from java.util.concurrent.ConcurrentHashMap.
 *
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public interface FutureCache<K,V,I extends Exception> extends Cache<K,V,I> {

 	public boolean expire(K key) ;

 	public V expireAndGet(K key, Object... additionalInfo) throws I ;

	public long getExpireTime() ;

    public void setExpireTime(long expirationTime) ;

    public int getExpiredCount() ;

	public int getAndResetExpiredCount() ;
}
