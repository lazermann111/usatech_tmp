package simple.util.concurrent;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;

import simple.util.Cycle;

public class ConcurrentReadonlyCycle<E> implements Cycle<E> {
	protected final Collection<E> collection;
	protected Iterator<E> iterator;
	protected final ReentrantLock lock = new ReentrantLock();
	public ConcurrentReadonlyCycle(Collection<E> Collection) {
		this.collection = Collection;
		this.iterator = Collection.iterator();
	}
	
	/* (non-Javadoc)
	 * @see simple.util.concurrent.Cycle#next()
	 */
	public E next() {
		lock.lock();
		try {
			if(!iterator.hasNext()) {
				iterator = collection.iterator();
				if(!iterator.hasNext())
					return null;
			}
			return iterator.next();
		} finally {
			lock.unlock();
		}
	}
	public int add(E obj) {
		throw new UnsupportedOperationException();		
	}
	public boolean remove(int index, E obj) {
		throw new UnsupportedOperationException();
	}
	public E[] drain() {
		throw new UnsupportedOperationException();
	}
	public boolean isEmpty() {
		return collection.isEmpty();
	}
	public int size() {
		return collection.size();
	}
}
