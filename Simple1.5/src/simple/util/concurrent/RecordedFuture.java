package simple.util.concurrent;


public interface RecordedFuture<V> extends ResettableFuture<V> {
	/** Records the last successful run of this Future
	 * @return The system time (in milliseconds past epoch) of
	 * the last successful run of this future.
	 */
	public long getLastRunTime() ;
	
	/** Attempts to reset this future if the given runTime is before
	 *  the last run time of the future. This method ensures an
	 *  atomic check and reset so as to prevent resetting a just run future.
	 * @param runTime
	 * @return if the future was reset
	 */
	public boolean resetIfRunBefore(long runTime) ;
	
}
