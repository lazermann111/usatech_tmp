package simple.util.concurrent;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import simple.bean.ConvertUtils;

public class BasicConcurrentCache<K,V> extends LockSegmentCache<K,V,CacheException> {
	protected final Constructor<V> constructor;
	public BasicConcurrentCache(Constructor<V> constructor) {
		this.constructor = constructor;
	}

	public BasicConcurrentCache(Constructor<V> constructor, int initialCapacity, float loadFactor, int concurrencyLevel) {
		super(initialCapacity, loadFactor, concurrencyLevel);
		this.constructor = constructor;
	}

	@Override
	protected V createValue(K key, Object... additionalInfo) throws CacheException {
		V value;
		try {
			switch(constructor.getParameterTypes().length) {
				case 0:
					value = constructor.newInstance();
					break;
				case 1:
					value = constructor.newInstance(key);
					break;
				case 2:
					if(constructor.isVarArgs() || additionalInfo == null || additionalInfo.length > 1)
						value = constructor.newInstance(key, additionalInfo);
					else
						value = constructor.newInstance(key, additionalInfo[0]);
					break;
				default:
					Object[] params = new Object[constructor.getParameterTypes().length];
					params[0] = key;
					if(additionalInfo != null) {
						System.arraycopy(additionalInfo, 0, params, 1, Math.min(params.length - 1, additionalInfo.length));
					}
					value = constructor.newInstance(params);

			}
		} catch(InstantiationException e) {
			throw new CacheException(e);
		} catch(IllegalArgumentException e) {
			throw new CacheException(e);
		} catch(IllegalAccessException e) {
			throw new CacheException(e);
		} catch(InvocationTargetException e) {
			throw new CacheException(e.getTargetException());
		}
		initialize(value, key, additionalInfo);
		return value;
	}

	protected void initialize(V value, K key, Object[] additionalInfo) {
	}

	@Override
	protected boolean keyEquals(K key1, K key2) {
		return ConvertUtils.areEqual(key1, key2);
	}

	@Override
	protected boolean valueEquals(V value1, V value2) {
		return ConvertUtils.areEqual(value1, value2);
	}
}
