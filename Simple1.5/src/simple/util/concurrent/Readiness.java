package simple.util.concurrent;

public enum Readiness {
	READY, LAST, NEVER
}
