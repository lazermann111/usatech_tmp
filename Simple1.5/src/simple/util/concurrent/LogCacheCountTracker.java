package simple.util.concurrent;

import java.util.concurrent.atomic.AtomicLong;

import simple.io.Log;

public class LogCacheCountTracker implements CacheCountTracker {
	private static final Log log = Log.getLog();
	protected long interval = 60 * 60 * 1000; // 1 hour
	protected final AtomicLong lastRecorded = new AtomicLong(System.currentTimeMillis());
	
	public void cacheAccessed(String cacheName, CountTracker cache) {
		long now = System.currentTimeMillis();
		long last = lastRecorded.get();
		if(now -  last > interval && lastRecorded.compareAndSet(last, now)) {
			StringBuilder sb = new StringBuilder();
			sb.append("Cache '").append(cacheName).append("': ").append(cache.getAndResetHitCount())
				.append(" hits; ").append(cache.getAndResetMissCount()).append(" misses");
			if(cache instanceof FutureCountTracker)
				sb.append("; ").append(((FutureCountTracker)cache).getAndResetExpiredCount()).append(" expired");
			log.info(sb.toString());
		}
	}

	public long getInterval() {
		return interval;
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}
}
