/**
 *
 */
package simple.util.concurrent;

import java.util.Iterator;
import java.util.Map;


public interface Segment<K,V,I extends Exception> {
	public V get(K key, int hash, boolean createIfAbsent, Object... additionalInfo) throws I;
	public V remove(K key, int hash, V value) ;
	public void clear() ;
	public int count() ;
	public boolean containsKey(K key, int hash) ;
	public Iterator<Map.Entry<K,V>> iterator() ;
	public V put(K key, int hash, V value, boolean onlyIfAbsent) ;
	public V replace(K key, int hash, V oldValue, V newValue);
}