package simple.util.concurrent;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import simple.util.CollectionUtils;

/**
 * Allows for caching the results of a call based on a set of arguments and returning the cached version if available
 * or executing the specified callable to create the results. Results can be expired and the cache size can be limited
 * (although this max cache size is approximate since it is implemented in each segment and not globally).
 * Many of the internal workings of this class were copied from java.util.concurrent.ConcurrentHashMap.
 *
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public class ConcurrentLRUMap2<K,V> extends AbstractMap<K, V> implements ConcurrentMap<K, V> {
	protected abstract class AbstractEntrySet<E> extends AbstractSet<E> implements Set<E> {
		protected abstract E convert(Map.Entry<K,V> entry) ;
		protected abstract K toKey(Object o) ;
		@Override
		public Iterator<E> iterator() {
			return new Iterator<E>() {
				protected int index = 0;
				protected Iterator<Map.Entry<K,V>> segmentIterator = cast(segments[0].iterator());
				@SuppressWarnings("unchecked")
				protected Iterator<Map.Entry<K,V>> cast(Iterator<?> iterator) {
					return (Iterator<Map.Entry<K,V>>)iterator;
				}
				protected void findNext() {
					while(index < segments.length - 1 && (segmentIterator == null || !segmentIterator.hasNext())) {
						segmentIterator = cast(segments[++index].iterator());
					}
				}
				public boolean hasNext() {
					findNext();
					return segmentIterator.hasNext();
				}
				public E next() {
					findNext();
					return convert(segmentIterator.next());
				}
				public void remove() {
					segmentIterator.remove();
				}
			};
		}

		@Override
		public int size() {
			return ConcurrentLRUMap2.this.size();
		}

		@Override
		public void clear() {
			ConcurrentLRUMap2.this.clear();
		}

		@Override
		public boolean remove(Object o) {
			K key = toKey(o);
			int hash = hash(key);
			return segmentFor(hash).remove(key, getEntryHash(hash), null) != null;
		}
	}
	protected class KeySet extends AbstractEntrySet<K> {
		@Override
		protected K convert(Map.Entry<K, V> entry) {
			return entry.getKey();
		}
		@Override
		protected K toKey(Object o) {
			return castKey(o);
		}

		@Override
		public boolean contains(Object o) {
			return containsKey(o);
		}
	}
	protected class EntrySet extends AbstractEntrySet<Map.Entry<K, V>> {
		@Override
		protected Map.Entry<K, V> convert(Map.Entry<K, V> entry) {
			return entry;
		}
		@SuppressWarnings("unchecked")
		@Override
		protected K toKey(Object o) {
			return ((Map.Entry<K, V>)o).getKey();
		}
	}
	protected Set<K> keySet;
	protected Set<Map.Entry<K, V>> entrySet;
	protected int maxSize = 100;
	protected final AtomicInteger size = new AtomicInteger(0);
    protected final Queue<K> useQueue = new ConcurrentLinkedQueue<K>();

	/**
     * Mask value for indexing into segments. The upper bits of a
     * key's hash code are used to choose the segment.
     **/
	protected final int segmentMask;

    /**
     * Shift value for indexing within segments.
     **/
	protected final int segmentShift;

    /**
     * The segments, each of which is a specialized hash table
     */
	protected final Segment<K,V,RuntimeException>[] segments;


    /**
     * The default initial number of table slots for this table.
     * Used when not otherwise specified in constructor.
     */
    public static int DEFAULT_INITIAL_CAPACITY = 16;

    /**
     * The maximum capacity, used if a higher value is implicitly
     * specified by either of the constructors with arguments.  MUST
     * be a power of two <= 1<<30 to ensure that entries are indexible
     * using ints.
     */
    protected static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * The default load factor for this table.  Used when not
     * otherwise specified in constructor.
     */
    public static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * The default number of concurrency control segments.
     **/
    public static final int DEFAULT_SEGMENTS = 16;

    /**
     * The maximum number of segments to allow; used to bound
     * constructor arguments.
     */
    protected static final int MAX_SEGMENTS = 1 << 16; // slightly conservative


	public ConcurrentLRUMap2(int maxSize, int initialCapacity, float loadFactor, int concurrencyLevel) {
		if (!(loadFactor > 0) || initialCapacity < 0 || concurrencyLevel <= 0)
            throw new IllegalArgumentException();

        if (concurrencyLevel > MAX_SEGMENTS)
            concurrencyLevel = MAX_SEGMENTS;

        // Find power-of-two sizes best matching arguments
        int sshift = 0;
        int ssize = 1;
        while (ssize < concurrencyLevel) {
            ++sshift;
            ssize <<= 1;
        }
        //segmentShift = 32 - sshift;
        segmentShift = sshift;
        segmentMask = ssize - 1;
        segments = newSegments(ssize);

        if (initialCapacity > MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;
        int c = initialCapacity / ssize;
        if (c * ssize < initialCapacity)
            ++c;
        int cap = 1;
        while (cap < c)
            cap <<= 1;

        for (int i = 0; i < segments.length; ++i)
            segments[i] = createSegment(cap, loadFactor);
        setMaxSize(maxSize);
	}

	@SuppressWarnings("unchecked")
	protected Segment<K,V,RuntimeException>[] newSegments(int length) {
		return new Segment[length];
	}

    public ConcurrentLRUMap2(int initialCapacity, float loadFactor, int concurrencyLevel) {
		this(100,initialCapacity, loadFactor, concurrencyLevel);
	}

    /**
     * Returns a hash code for non-null Object x.
     * Uses the same hash code spreader as most other java.util hash tables.
     * @param x the object serving as a key
     * @return the hash code
     */
    protected int hash(Object x) {
    	return CollectionUtils.deepHashCode(x);
    }

	protected int getSegmentIndex(int hash) {
		return hash & segmentMask; //OLD: (hash >>> segmentShift) & segmentMask;
	}

	protected int getEntryHash(int hash) {
		return hash >>> segmentShift; // OLD: hash;
	}

	/**
     * Returns the segment that should be used for key with given hash
     * @param hash the hash code for the key
     * @return the segment
     */
    protected Segment<K,V,RuntimeException> segmentFor(int hash) {
        return segments[getSegmentIndex(hash)];
    }

    public V remove(Object o) {
		K key = castKey(o);
		int hash = hash(key);
		return segmentFor(hash).remove(key, getEntryHash(hash), null);
	}

	@Override
	public void clear() {
		for(Segment<K,V,RuntimeException> seg : segments) {
			seg.clear();
		}
	}

	public void printCounts() {
		for(int i = 0; i < segments.length; i++) {
			System.out.println("Segment " + (i+1) + " has " + segments[i].count() + " entries");
		}
	}

	@Override
	public Set<K> keySet() {
		if(keySet == null) {
			keySet = new KeySet();
		}
		return keySet;
	}

	@Override
	public int size() {
		return size.get();
	}

	protected Segment<K,V,RuntimeException> createSegment(int capacity, float loadFactor) {
		return new LockSegment<K,V,RuntimeException>(capacity, loadFactor, MAXIMUM_CAPACITY) {
			private static final long serialVersionUID = -93848140014L;
			@Override
			protected V createValue(K key, Object... additionalInfo) {
				return ConcurrentLRUMap2.this.createValue(key, additionalInfo);
			}
			@Override
			protected boolean keyEquals(K key1, K key2) {
				return CollectionUtils.deepEquals(key1, key2);
			}
			@Override
			protected boolean valueEquals(V value1, V value2) {
				return CollectionUtils.deepEquals(value1, value2);
			}
		};
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	protected V createValue(K key, Object... additionalInfo) {
		return null;
	}

	protected boolean expire(V value) {
		return false;
	}

	@SuppressWarnings("unchecked")
	protected K castKey(Object key) {
		return (K)key;
	}
	@SuppressWarnings("unchecked")
	protected V castValue(Object value) {
		return (V)value;
	}

	@Override
	public Set<Map.Entry<K, V>> entrySet() {
		if(entrySet == null) {
			entrySet = new EntrySet();
		}
		return entrySet;
	}

	public V putIfAbsent(K key, V value) {
		int hash = hash(key);
		return segmentFor(hash).put(castKey(key), getEntryHash(hash), value, true);
	}

	public boolean remove(Object key, Object value) {
		int hash = hash(key);
		return segmentFor(hash).remove(castKey(key), getEntryHash(hash), castValue(value)) != null;
	}

	public V replace(K key, V value) {
		int hash = hash(key);
		return segmentFor(hash).replace(castKey(key), getEntryHash(hash), null, value);
	}

	public boolean replace(K key, V oldValue, V newValue) {
		int hash = hash(key);
		return segmentFor(hash).replace(castKey(key), getEntryHash(hash), oldValue, newValue) != null;
	}

	@Override
	public boolean containsKey(Object key) {
		int hash = hash(key);
		return segmentFor(hash).containsKey(castKey(key), getEntryHash(hash));
	}

	@Override
	public V get(Object key) {
		int hash = hash(key);
		return segmentFor(hash).get(castKey(key), getEntryHash(hash), false);
	}

	public V getOrCreate(K key, Object... additionalInfo) {
		int hash = hash(key);
		return segmentFor(hash).get(castKey(key), getEntryHash(hash), true, additionalInfo);
	}

	@Override
	public V put(K key, V value) {
		int hash = hash(key);
		return segmentFor(hash).put(castKey(key), getEntryHash(hash), value, false);
	}
}
