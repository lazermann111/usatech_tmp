package simple.util.concurrent;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Pipe;
import java.nio.channels.Pipe.SinkChannel;
import java.nio.channels.Pipe.SourceChannel;
import java.nio.channels.Selector;

public class TimedPipe {
	protected final Selector sinkSelector;
	protected final Selector sourceSelector;
	protected final SinkChannel sink;
	protected final SourceChannel source;

	public TimedPipe() throws IOException {
		Pipe pipe = Pipe.open();
		sink = pipe.sink();
		source = pipe.source();
		sinkSelector = Selector.open();
		sourceSelector = Selector.open();
		sink.configureBlocking(false);
		source.configureBlocking(false);
		sink.register(sinkSelector, sink.validOps());
		source.register(sourceSelector, source.validOps());
	}

	public int write(ByteBuffer bb, long timeout) throws IOException {
		int tot = 0;
		long target = 0;
		while(true) {
			tot += sink.write(bb);
			if(!bb.hasRemaining())
				return tot;
			// wait for ready
			if(timeout > 0) {
				long now = System.currentTimeMillis();
				if(target == 0)
					target = now + timeout;
				else if(target - now <= 0)
					return tot;
				sinkSelector.select(target - now);
			} else
				sinkSelector.select();
		}
	}

	public int read(ByteBuffer bb, long timeout) throws IOException {
		long target = 0;
		while(true) {
			int r = source.read(bb);
			if(r != 0)
				return r;
			// wait for ready
			if(timeout > 0) {
				long now = System.currentTimeMillis();
				if(target == 0)
					target = now + timeout;
				else if(target - now <= 0)
					return 0;
				sourceSelector.select(target - now);
			} else
				sourceSelector.select();
		}
	}

	public void close() {
		try {
			sink.close();
		} catch(IOException e) {
			// Ignore
		}
		try {
			source.close();
		} catch(IOException e) {
			// Ignore
		}
		try {
			sinkSelector.close();
		} catch(IOException e) {
			// Ignore
		}
		try {
			sourceSelector.close();
		} catch(IOException e) {
			// Ignore
		}
	}
}
