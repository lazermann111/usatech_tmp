package simple.util.concurrent;

import java.util.concurrent.TimeoutException;

public interface Pool<E> {
    /**
     * Obtain an instance from my pool.
     * By contract, clients MUST return
     * the borrowed instance using
     * {@link #returnObject(java.lang.Object) returnObject}
     * or a related method as defined in an implementation
     * or sub-interface.
     * <p>
     * The behaviour of this method when the pool has been exhausted
     * is not specified (although it may be specified by implementations).
     *
     * @return an instance from my pool.
     */
    E borrowObject() throws TimeoutException, CreationException, InterruptedException;

    /**
     * Return an instance to my pool.
     * By contract, <i>obj</i> MUST have been obtained
     * using {@link #borrowObject() borrowObject}
     * or a related method as defined in an implementation
     * or sub-interface.
     *
     * @param obj a {@link #borrowObject borrowed} instance to be returned.
     */
    void returnObject(E obj) ;

    /**
     * Invalidates an object from the pool
     * By contract, <i>obj</i> MUST have been obtained
     * using {@link #borrowObject() borrowObject}
     * or a related method as defined in an implementation
     * or sub-interface.
     * <p>
     * This method should be used when an object that has been borrowed
     * is determined (due to an exception or other problem) to be invalid.
     *
     * @param obj a {@link #borrowObject borrowed} instance to be returned.
     */
    void invalidateObject(E obj) ;

    /**
     * Return the number of instances
     * currently idle in my pool (optional operation).
     * This may be considered an approximation of the number
     * of objects that can be {@link #borrowObject borrowed}
     * without creating any new instances.
     *
     * @return the number of instances currently idle in my pool
     * @throws UnsupportedOperationException if this implementation does not support the operation
     */
    int getNumIdle() ;

    /**
     * Return the number of instances
     * currently borrowed from my pool
     * (optional operation).
     *
     * @return the number of instances currently borrowed in my pool
     * @throws UnsupportedOperationException if this implementation does not support the operation
     */
    int getNumActive() ;

    /**
     * Clears any objects sitting idle in the pool, releasing any
     * associated resources (optional operation).
     *
     * @throws UnsupportedOperationException if this implementation does not support the operation
     */
    void clear() ;

    /**
     * Close this pool, and free any resources associated with it.
     */
    void close() ;
}
