package simple.util.concurrent;


/** This class allows only one thread at a time to receive permission to process
 *  a given resource.
 * @author bkrug
 *
 */
public interface Bottleneck<E> {
	
	public void lock(E resource) throws InterruptedException ;
	
	public void unlock(E resource) ;

	public int getMaxIdle() ;

	public void setMaxIdle(int maxIdle) ;
}
