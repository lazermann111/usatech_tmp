package simple.util.concurrent;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import simple.util.CollectionUtils;

public abstract class SegmentCache<K,V,I extends Exception> implements Cache<K,V,I> {
	protected abstract class AbstractEntrySet<E> extends AbstractSet<E> implements Set<E> {
		protected abstract E convert(Map.Entry<K,V> entry) ;
		protected abstract K toKey(Object o) ;
		@Override
		public Iterator<E> iterator() {
			return new Iterator<E>() {
				protected int index = 0;
				protected Iterator<Map.Entry<K,V>> segmentIterator = cast(segments[0].iterator());
				@SuppressWarnings("unchecked")
				protected Iterator<Map.Entry<K,V>> cast(Iterator<?> iterator) {
					return (Iterator<Map.Entry<K,V>>)iterator;
				}
				protected void findNext() {
					while(index < segments.length - 1 && (segmentIterator == null || !segmentIterator.hasNext())) {
						segmentIterator = cast(segments[++index].iterator());
					}
				}
				public boolean hasNext() {
					findNext();
					return segmentIterator.hasNext();
				}
				public E next() {
					findNext();
					return convert(segmentIterator.next());
				}
				public void remove() {
					segmentIterator.remove();
				}
			};
		}

		@Override
		public int size() {
			return SegmentCache.this.size();
		}

		@Override
		public void clear() {
			SegmentCache.this.clear();
		}

		@Override
		public boolean remove(Object o) {
			return SegmentCache.this.remove(toKey(o)) != null;
		}
	}
	protected class EntrySet extends AbstractEntrySet<Map.Entry<K, V>> {
		@Override
		protected Map.Entry<K, V> convert(Map.Entry<K, V> entry) {
			return entry;
		}
		@Override
		protected K toKey(Object o) {
			return castKey(((Map.Entry<?,?>)o).getKey());
		}

		@Override
		public boolean contains(Object o) {
			K key = toKey(o);
			try {
				return containsKey(key) && valueEquals(getIfPresent(key), castValue(((Map.Entry<?,?>)o).getValue()));
			} catch(Exception e) {
				return false;
			}
		}
	}
	protected class KeySet extends AbstractEntrySet<K> {
		@Override
		protected K convert(Map.Entry<K, V> entry) {
			return entry.getKey();
		}
		@Override
		protected K toKey(Object o) {
			return castKey(o);
		}

		@Override
		public boolean contains(Object o) {
			return containsKey(o);
		}
	}
	protected class ValueCollection extends AbstractCollection<V> {
		@Override
		public Iterator<V> iterator() {
			return new Iterator<V>() {
				protected int index = 0;
				protected Iterator<Map.Entry<K,V>> segmentIterator = cast(segments[0].iterator());
				@SuppressWarnings("unchecked")
				protected Iterator<Map.Entry<K,V>> cast(Iterator<?> iterator) {
					return (Iterator<Map.Entry<K,V>>)iterator;
				}
				protected void findNext() {
					while(index < segments.length - 1 && (segmentIterator == null || !segmentIterator.hasNext())) {
						segmentIterator = cast(segments[++index].iterator());
					}
				}
				public boolean hasNext() {
					findNext();
					return segmentIterator.hasNext();
				}
				public V next() {
					findNext();
					return convert(segmentIterator.next());
				}
				public void remove() {
					segmentIterator.remove();
				}
				protected V convert(Map.Entry<K, V> entry) {
					return entry.getValue();
				}
			};
        }

		@Override
		public int size() {
		    return SegmentCache.this.size();
		}

		@Override
		public boolean contains(Object v) {
			Iterator<V> iter = iterator();
			while(iter.hasNext()) {
				if(valueEquals(iter.next(), castValue(v)))
					return true;
			}
		    return false;
		}
    }

	protected Set<K> keySet;
	protected Set<Map.Entry<K, V>> entrySet;
	protected Collection<V> values;
	/**
     * Mask value for indexing into segments. The upper bits of a
     * key's hash code are used to choose the segment.
     **/
	protected final int segmentMask;

    /**
     * Shift value for indexing within segments.
     **/
	protected final int segmentShift;

    /**
     * The segments, each of which is a specialized hash table
     */
	protected final Segment<K,V,I>[] segments;


    /**
     * The default initial number of table slots for this table.
     * Used when not otherwise specified in constructor.
     */
    public static int DEFAULT_INITIAL_CAPACITY = 16;

    /**
     * The maximum capacity, used if a higher value is implicitly
     * specified by either of the constructors with arguments.  MUST
     * be a power of two <= 1<<30 to ensure that entries are indexible
     * using ints.
     */
    protected static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * The default load factor for this table.  Used when not
     * otherwise specified in constructor.
     */
    public static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * The default number of concurrency control segments.
     **/
    public static final int DEFAULT_SEGMENTS = 16;

    /**
     * The maximum number of segments to allow; used to bound
     * constructor arguments.
     */
    protected static final int MAX_SEGMENTS = 1 << 16; // slightly conservative


	public SegmentCache(int initialCapacity, float loadFactor, int concurrencyLevel) {
		if (!(loadFactor > 0) || initialCapacity < 0 || concurrencyLevel <= 0)
            throw new IllegalArgumentException();

        if (concurrencyLevel > MAX_SEGMENTS)
            concurrencyLevel = MAX_SEGMENTS;

        // Find power-of-two sizes best matching arguments
        int sshift = 0;
        int ssize = 1;
        while (ssize < concurrencyLevel) {
            ++sshift;
            ssize <<= 1;
        }
        //segmentShift = 32 - sshift;
        segmentShift = sshift;
        segmentMask = ssize - 1;
        segments = newSegments(ssize);

        if (initialCapacity > MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;
        int c = initialCapacity / ssize;
        if (c * ssize < initialCapacity)
            ++c;
        int cap = 1;
        while (cap < c)
            cap <<= 1;

        for (int i = 0; i < segments.length; ++i)
            segments[i] = createSegment(cap, loadFactor);
 	}

	@SuppressWarnings("unchecked")
	protected Segment<K,V,I>[] newSegments(int length) {
		return new Segment[length];
	}

    /**
     * Returns a hash code for non-null Object x.
     * Uses the same hash code spreader as most other java.util hash tables.
     * @param x the object serving as a key
     * @return the hash code
     */
    protected int hash(Object x) {
    	return CollectionUtils.deepHashCode(x);
    }

	protected int getSegmentIndex(int hash) {
		return hash & segmentMask; //OLD: (hash >>> segmentShift) & segmentMask;
	}

	protected int getEntryHash(int hash) {
		return hash >>> segmentShift; // OLD: hash;
	}

	/**
     * Returns the segment that should be used for key with given hash
     * @param hash the hash code for the key
     * @return the segment
     */
    protected Segment<K,V,I> segmentFor(int hash) {
        return segments[getSegmentIndex(hash)];
    }

	public V remove(K key) {
		int hash = hash(key);
		return segmentFor(hash).remove(key, getEntryHash(hash), null);
	}

	public void clear() {
		for(Segment<K,V,I> seg : segments) {
			seg.clear();
		}
	}

	public Set<K> keySet() {
		if(keySet == null) {
			keySet = new KeySet();
		}
		return keySet;
	}

	public Set<Map.Entry<K, V>> entrySet() {
		if(entrySet == null) {
			entrySet = new EntrySet();
		}
		return entrySet;
	}

	public int size() {
		int sz = 0;
		for(Segment<K,V,I> seg : segments) {
			sz += seg.count();
		}
		return sz;
	}

	protected abstract Segment<K,V,I> createSegment(int capacity, float loadFactor) ;

	@SuppressWarnings("unchecked")
	protected K castKey(Object key) {
		return (K)key;
	}
	@SuppressWarnings("unchecked")
	protected V castValue(Object value) {
		return (V)value;
	}

	public boolean remove(Object key, Object value) {
		int hash = hash(key);
		return segmentFor(hash).remove(castKey(key), getEntryHash(hash), castValue(value)) != null;
	}

	public V get(K key) {
		int hash = hash(key);
		try {
			return segmentFor(hash).get(castKey(key), getEntryHash(hash), false);
		} catch(Exception e) {
			throw new UndeclaredThrowableException(e);
		}
	}

	public V getIfPresent(K key, Object... additionalInfo) throws I {
		int hash = hash(key);
		return segmentFor(hash).get(castKey(key), getEntryHash(hash), false, additionalInfo);
	}

	public V getOrCreate(K key, Object... additionalInfo) throws I {
		int hash = hash(key);
		return segmentFor(hash).get(castKey(key), getEntryHash(hash), true, additionalInfo);
	}

	public boolean containsKey(Object key) {
		int hash = hash(key);
		return segmentFor(hash).containsKey(castKey(key), getEntryHash(hash));
	}
	public Collection<V> values() {
		if (values == null) {
		    values = new ValueCollection();
		}
		return values;
	}

	protected abstract boolean valueEquals(V value1, V value2) ;

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		Iterator<Entry<K,V>> i = entrySet().iterator();
		if (! i.hasNext())
		    return "{}";

		StringBuilder sb = new StringBuilder();
		sb.append('{');
		for (;;) {
		    Entry<K,V> e = i.next();
		    K key = e.getKey();
		    V value = e.getValue();
		    sb.append(key   == this ? "(this Map)" : key);
		    sb.append('=');
		    sb.append(value == this ? "(this Map)" : value);
		    if (! i.hasNext())
			return sb.append('}').toString();
		    sb.append(", ");
		}
	}

	// *
	public V put(K key, V value) {
		int hash = hash(key);
		return segmentFor(hash).put(castKey(key), getEntryHash(hash), value, false);
	}
	public V replace(K key, V value) {
		int hash = hash(key);
		return segmentFor(hash).replace(castKey(key), getEntryHash(hash), null, value);
	}

	public boolean replace(K key, V oldValue, V newValue) {
		int hash = hash(key);
		return segmentFor(hash).replace(castKey(key), getEntryHash(hash), oldValue, newValue) != null;
	}

	public V putIfAbsent(K key, V value) {
		int hash = hash(key);
		return segmentFor(hash).put(castKey(key), getEntryHash(hash), value, true);
	}
	// */
}
