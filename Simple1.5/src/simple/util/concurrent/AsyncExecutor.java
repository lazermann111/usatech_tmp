package simple.util.concurrent;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;


/** Executes a task in a thread. This implementation is designed so that
 *  a call to complete() must follow each call to submit(). Calling submit()
 *  more than once in a row may incur non-deterministic behavior.
 * @author Brian S. Krug
 *
 * @param <A>
 */
public class AsyncExecutor<A, R> extends Thread  {
	protected A argument;
	protected Task<A, R> task;
	protected R taskResult;
	protected TaskException taskException;
	protected RuntimeException taskRuntimeException;
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition taskSignal = lock.newCondition();
	protected final Condition returnSignal = lock.newCondition();
	protected volatile boolean done = false;
	public AsyncExecutor(String name) {
		super(name);
	}
	public AsyncExecutor(ThreadGroup group, String name) {
		super(group, name);
	}
	@Override
	public void run() {
		lock.lock();
		try {
			while(!done) {
				if(task != null)
					try {
						taskResult = task.perform(argument);
						taskException = null;
					} catch(TaskException e) {
						taskException = e;
						taskRuntimeException = null;
						taskResult = null;
					} catch(RuntimeException e) {
						taskRuntimeException = e;
						taskException = null;
						taskResult = null;
					} finally {
						task = null;
						returnSignal.signalAll();
					}
				try {
					taskSignal.await();
				} catch(InterruptedException e) {
					// ignore
				}
			}
		} finally {
			lock.unlock();
		}
	}
	public void submit(Task<A,R> task) {
		lock.lock();
		try {
			this.task = task;
			taskSignal.signal();
		} finally {
			lock.unlock();
		}
	}
	public R complete() throws TaskException {
		lock.lock();
		try {
			while(task != null && !done)
				try {
					returnSignal.await();
				} catch(InterruptedException e) {
					// ignore
				}
			if(taskException != null)
				throw taskException;
			else if(taskRuntimeException != null)
				throw taskRuntimeException;
			else
				return taskResult;
		} finally {
			taskResult = null;
			taskException = null;
			taskRuntimeException = null;
			lock.unlock();
		}
	}
	public void shutdown() {
		done = true;
		lock.lock();
		try {
			taskSignal.signal();
		} finally {
			lock.unlock();
		}
	}
	public A getArgument() {
		return argument;
	}
	public void setArgument(A argument) {
		this.argument = argument;
	}
}