package simple.util.concurrent;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.collections.list.CursorableLinkedList;
import org.apache.commons.collections.list.CursorableLinkedList.Cursor;

import simple.io.Log;
import simple.lang.Decision;

public abstract class AbstractLockingPool<E> implements BoundedPool<E> {
	private static final Log log = Log.getLog();
	protected final ReentrantLock lock = new ReentrantLock();
	protected final Condition signal = lock.newCondition();
	@SuppressWarnings("serial")
	protected final CursorableLinkedList objects = new CursorableLinkedList() {
		protected void broadcastNodeChanged(Node node) {
			for(int i = 0; i < 20; i++)
				try {
					super.broadcastNodeChanged(node);
					return;
				} catch(ConcurrentModificationException e) {
				}
		}

		protected void broadcastNodeInserted(Node node) {
			for(int i = 0; i < 20; i++)
				try {
					super.broadcastNodeInserted(node);
					return;
				} catch(ConcurrentModificationException e) {
				}
		}

		protected void broadcastNodeRemoved(Node node) {
			for(int i = 0; i < 20; i++)
				try {
					super.broadcastNodeRemoved(node);
					return;
				} catch(ConcurrentModificationException e) {
				}
		}

		protected void registerCursor(Cursor cursor) {
			for(int i = 0; i < 20; i++)
				try {
					super.registerCursor(cursor);
					return;
				} catch(ConcurrentModificationException e) {
				}
		}

		protected void unregisterCursor(Cursor cursor) {
			for(int i = 0; i < 20; i++)
				try {
					super.unregisterCursor(cursor);
					return;
				} catch(ConcurrentModificationException e) {
				}
		}
	};
	protected int maxActive = 3;
	protected int maxIdle = 2;
	protected int numActive = 0;
	protected long maxWait = 0;
	protected boolean tracing = false;
	protected Map<E,Trace> borrowed;
	protected boolean lockDuringDecideEvict = false;
	protected static class Trace {
		protected final long timestamp;
		protected final String threadName;
		protected final WeakReference<Thread> threadRef;
		protected final StackTraceElement[] stackTrace;
		public Trace(Thread thread) {
			timestamp = System.currentTimeMillis();
			threadName = thread.getName();
			if(log.isDebugEnabled()) {
				stackTrace = thread.getStackTrace();
				threadRef = new WeakReference<Thread>(thread);
			} else {
				stackTrace = null;
				threadRef = null;
			}
		}
		/**
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("Borrowed by ").append(threadName).append(" at ").append(new Date(timestamp));
			if(stackTrace != null) {
				sb.append(" originally from ").append(Arrays.toString(stackTrace));
				Thread thread = threadRef.get();
				if(thread != null)
				sb.append(" currently executing ").append(Arrays.toString(thread.getStackTrace()));
			}
			return sb.toString();
		}
	}
	protected abstract E createObject(long timeout) throws CreationException, TimeoutException, InterruptedException ;
	protected abstract void closeObject(E obj) throws Exception ;

	@SuppressWarnings("unchecked")
	protected E poll() {
		return objects.isEmpty() ? null : (E) objects.removeFirst();
	}

	protected void offer(E obj) {
		objects.addLast(obj);
	}
	public E borrowObject() throws TimeoutException, CreationException, InterruptedException {
		long maxWaitMs = getMaxWait();
		long nanosTimeout;
		boolean indefinite;
		if(maxWaitMs <= 0) {
			indefinite = true;
			nanosTimeout = 0;
		} else {
			indefinite = false;
			nanosTimeout = TimeUnit.MILLISECONDS.toNanos(maxWaitMs);
		}
		lock.lockInterruptibly();
		try {
			while(true) {
				E obj = poll();
				if(obj != null) {
					addBorrowed(obj);
					if(log.isDebugEnabled())
						log.debug("Giving out existing object from the pool; " + getNumActive() + " are now active");
					return obj;
				} else if(getMaxActive() > getNumActive() /* + getNumIdle()*/) { // NOTE: getNumIdle() should return 0 here so don't bother getting it
					if(log.isDebugEnabled())
						log.debug("Creating new object for the pool because only " + getNumActive() + " are active");
					numActive++; // to prevent over-creation
					break;
				} else {
					if(log.isDebugEnabled())
						log.debug("Waiting " + (indefinite ? "indefinitely" : "for " + nanosTimeout/1000000 + " milliseconds") + " for an object to be returned to the pool");
					if(indefinite)
						signal.await();
					else {
						nanosTimeout = signal.awaitNanos(nanosTimeout);
						if(nanosTimeout <= 0)
							throw new TimeoutException("Could not obtain an object from the pool in " + maxWaitMs + " milliseconds");
					}
				}
			}
		} finally {
			lock.unlock();
		}
		//create outside of the lock
		E obj;
		try {
			obj = createObject(TimeUnit.NANOSECONDS.toMillis(nanosTimeout));
		} catch(CreationException e) {
			lock.lock();
			try {
				numActive--;
			} finally {
				lock.unlock();
			}
			throw e;
		} catch(TimeoutException e) {
			lock.lock();
			try {
				numActive--;
			} finally {
				lock.unlock();
			}
			throw e;
		} catch(InterruptedException e) {
			lock.lock();
			try {
				numActive--;
			} finally {
				lock.unlock();
			}
			throw e;
		} catch(RuntimeException e) {
			lock.lock();
			try {
				numActive--;
			} finally {
				lock.unlock();
			}
			throw e;
		} catch(Error e) {
			lock.lock();
			try {
				numActive--;
			} finally {
				lock.unlock();
			}
			throw e;
		}
		lock.lock();
		try {
			numActive--; // roll-back over-creation prevention
			addBorrowed(obj);
		} finally {
			lock.unlock();
		}
		return obj;
	}

	public void clear() {
		Set<E> cleared = new LinkedHashSet<E>();
		lock.lock();
		try {
			E obj;
			while((obj = poll()) != null)
				cleared.add(obj);
			if(borrowed != null) {
				Iterator<E> iter = borrowed.keySet().iterator();
				while(iter.hasNext()) {
					cleared.add(iter.next());
					iter.remove();
					numActive--;	
				}
			}
		} finally {
			lock.unlock();
		}
		for(E obj : cleared)
			closeSafely(obj);
	}

	public void close() {
		maxIdle = 0;
		maxActive = 0;
		clear();
	}

	public int getNumActive() {
		return numActive;
	}

	public int getNumIdle() {
		return objects.size();
	}
	
	public boolean addObject() throws CreationException, TimeoutException, InterruptedException {
		if(getMaxActive() <= getNumActive() + getNumIdle()) 
			return false;
		E obj = createObject(getMaxWait());
		lock.lock();
		try {
			if(getMaxActive() > getNumActive() + getNumIdle()) {
				offer(obj);
				signal.signal();
				return true;
			}
		} finally {
			lock.unlock();
		}
		closeSafely(obj);
		return false;
	}
	
	public void invalidateObject(E obj) {
		lock.lock();
		try {
			boolean removed = removeBorrowed(obj);
			if(log.isDebugEnabled()) {
				if(removed)
					log.debug("Removing object from the pool; " + getNumActive() + " are now active");
				else
					log.debug("Object already removed from pool; doing nothing");
			}
		} finally {
			lock.unlock();
		}
		closeSafely(obj);
	}

	public void returnObject(E obj) {
		lock.lock();
		try {
			boolean removed = removeBorrowed(obj);
			if(log.isDebugEnabled()) {
				if(removed)
					log.debug("Receiving object back into the pool; " + getNumActive() + " are now active");
				else
					log.debug("Object already returned to the pool; doing nothing");
			}
			if(removed) {
				if(maxIdle > getNumIdle()) {
					offer(obj);
					signal.signal();
					return; // don't close since we will reuse
				}	
			} else
				return; // don't close since we are not the "owner"
		} finally {
			lock.unlock();
		}
		closeSafely(obj);
	}
	
	@SuppressWarnings("unchecked")
	public int evict(Decision<E> evictDecision, int maxEvictable, int minIdle) {
		// new strategy:
		// 1. Look for an evictable object nicely (keep in pool)
		// 2. Remove it
		// 3. If successful close it and if numIdle < minIdle, add object. If this fails exit.
		int n = 0;
		lock.lock();
		try {
			Cursor cursor = objects.cursor();
			while(cursor.hasNext()) {
				E obj;
				try {
					obj = (E) cursor.next();
				} catch(NoSuchElementException e) {
					return n; // don't continue (we assumably got to the end of the cursor)
				}
				if(obj != null && evictDecision.decide(obj)) {
					int before, after, active;
					before = objects.size();
					cursor.remove();
					after = objects.size();
					active = numActive;
					if(before > after) {
						closeSafely(obj);
						n++;
						for(int i = after; i < minIdle && i + active <= getMaxActive(); i++)
							try {
								addObject();
							} catch(CreationException | TimeoutException | InterruptedException e) {
								return n; // don't continue
							}
						if(n >= maxEvictable)
							return n;
					}
				}
			}
		} finally {
			lock.unlock();
		}
		return n;
	}

	/**
	 * @see simple.util.concurrent.BoundedPool#getMaxActive()
	 */
	public int getMaxActive() {
		return maxActive;
	}
	/**
	 * @see simple.util.concurrent.BoundedPool#setMaxActive(int)
	 */
	public void setMaxActive(int maxActive) {
		int old = this.maxActive;
		this.maxActive = maxActive;
		if(old < maxActive && maxActive > getNumActive()) {
			lock.lock();
			try {
				signal.signalAll();
			} finally {
				lock.unlock();
			}
		}
	}
	/**
	 * @see simple.util.concurrent.BoundedPool#getMaxIdle()
	 */
	public int getMaxIdle() {
		return maxIdle;
	}
	/**
	 * @see simple.util.concurrent.BoundedPool#setMaxIdle(int)
	 */
	public void setMaxIdle(int maxIdle) {
		int old = this.maxIdle;
		this.maxIdle = maxIdle;
		if(old < maxIdle) {
			Collection<E> close = new ArrayList<E>();
			lock.lock();
			try {
				while(maxIdle < getNumIdle()) {
					close.add(poll());
				}
			} finally {
				lock.unlock();
			}
			for(E obj : close)
				closeSafely(obj);
		}
	}
	protected void closeSafely(E obj) {
		try {
			closeObject(obj);
		} catch(Exception e) {
			log.info("Could not close object " + obj, e);
		}
	}
	/**
	 * @see simple.util.concurrent.BoundedPool#getMaxWait()
	 */
	public long getMaxWait() {
		return maxWait;
	}
	/**
	 * @see simple.util.concurrent.BoundedPool#setMaxWait(long)
	 */
	public void setMaxWait(long maxWait) {
		this.maxWait = maxWait;
	}
	public boolean isTracing() {
		return tracing;
	}
	public void setTracing(boolean tracing) {
		this.tracing = tracing;
	}
	public String getTracingInfo() {
		if(!isTracing()) {
			return "<Tracing Not Enabled>";
		} else if(borrowed == null) {
			return "<Tracing Unavailable>";
		} else {
			return borrowed.toString();
		}
	}
	protected void addBorrowed(E obj) {
		getBorrowed().put(obj, isTracing() ? new Trace(Thread.currentThread()) : null);
		numActive++;
	}
	protected boolean removeBorrowed(E obj) {
		Map<E, Trace> b = getBorrowed();
		if(b.containsKey(obj)) {
			b.remove(obj);
			numActive--;
			return true;
		}
		return false;
	}
	protected Map<E,Trace> getBorrowed() {
		if(borrowed == null)
			borrowed = new HashMap<E, Trace>();
			//borrowed = new WeakHashMap<E, Trace>(); //TODO: If we adapt WeakHashMap to notify us of a key being finalized, we could implement abandoned detection
		return borrowed;
	}

	public boolean isLockDuringDecideEvict() {
		return lockDuringDecideEvict;
	}

	public void setLockDuringDecideEvict(boolean lockDuringDecideEvict) {
		this.lockDuringDecideEvict = lockDuringDecideEvict;
	}

	@Override
	public String toString() {
		Map<E, Trace> b = getBorrowed();
		return isTracing() ? b.toString() : b.keySet().toString();
	}
}
