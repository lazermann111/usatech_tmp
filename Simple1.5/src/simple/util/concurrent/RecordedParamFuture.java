/**
 *
 */
package simple.util.concurrent;


/**
 * @author Brian S. Krug
 *
 */
public interface RecordedParamFuture<V> extends RecordedFuture<V>, ParamFuture<V> {

}
