package simple.util.concurrent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class WaitFor {
	
	public static boolean waitForFalse(Object object, String booleanMethodName, long timeoutMs) 
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
	{
		return waitForMatch(object, booleanMethodName, false, timeoutMs);
	}

	public static boolean waitForTrue(Object object, String booleanMethodName, long timeoutMs) 
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
	{
		return waitForMatch(object, booleanMethodName, true, timeoutMs);
	}

	public static boolean waitForMatch(Object object, String booleanMethodName, boolean match, long timeoutMs) 
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
	{
		return waitForMatch(object, booleanMethodName, match, timeoutMs, 10, new Object[0]);
	}

	public static boolean waitForMatch(Object object, String booleanMethodName, boolean match, long timeoutMs, Object... args) 
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
	{
		return waitForMatch(object, booleanMethodName, match, timeoutMs, 10, args);
	}
	
	public static boolean waitForMatch(Object object, String booleanMethodName, boolean match, long timeoutMs, long pollTimeMs, Object... args) 
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
	{
		Class<?>[] argTypes = new Class[args.length];
		for(int i=0; i<args.length; i++) {
			argTypes[i] = args[i].getClass();
		}
	
		Method method = object.getClass().getMethod(booleanMethodName, argTypes);
		if (!method.getReturnType().equals(Boolean.TYPE))
			throw new IllegalArgumentException("method must return boolean");
		
		boolean interrupted = false;
		long st = System.currentTimeMillis();
		while(true) {
			boolean result = (Boolean) method.invoke(object, args);
			if ((result == match) || interrupted)
				return (result == match);
			
			long et = (System.currentTimeMillis() - st);
			if (et > timeoutMs)
				return false;
			
			try {
				Thread.sleep(pollTimeMs);
			} catch (InterruptedException e) {
				interrupted = true;
			}
		}
	}
}
