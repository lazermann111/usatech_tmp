package simple.util.concurrent;

public interface Factory<A,R,I extends Exception> {
	public R create(A argument, Object... additionalInfo) throws I;
}
