package simple.util.concurrent;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** This is a NOT-THREAD SAFE implementation of the Cache abstraction.
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public abstract class NonConcurrentCache<K,V,I extends Exception> implements Cache<K,V,I> {
	protected final Map<K, V> map;
	public NonConcurrentCache() {
		this(new HashMap<K, V>());
	}
	public NonConcurrentCache(int initialCapacity, float loadFactor) {
		this(new HashMap<K, V>(initialCapacity, loadFactor));
	}

	public NonConcurrentCache(Map<K, V> map) {
		this.map = map;
	}

	protected abstract V createValue(K key, Object... additionalInfo) throws I ;

	public void clear() {
		map.clear();
	}
	public V getIfPresent(K key, Object... additionalInfo) throws I {
		return map.get(key);
	}
	public V getOrCreate(K key, Object... additionalInfo) throws I {
		if(map.containsKey(key))
			return map.get(key);
		V value = createValue(key, additionalInfo);
		map.put(key, value);
		return value;
	}
	public Set<K> keySet() {
		return map.keySet();
	}
	public Set<Map.Entry<K, V>> entrySet() {
		return map.entrySet();
	}
	public V remove(K key) {
		return map.remove(key);
	}

	public int size() {
		return map.size();
	}
	public Collection<V> values() {
		return map.values();
	}
	@Override
	public String toString() {
		return map.toString();
	}
}
