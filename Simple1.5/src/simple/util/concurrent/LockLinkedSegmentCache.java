package simple.util.concurrent;

import java.util.concurrent.atomic.AtomicInteger;

import simple.util.CollectionUtils;

public abstract class LockLinkedSegmentCache<K,V,I extends Exception> extends SegmentCache<K,V,I> implements BoundedCache<K, V, I> {
	protected final AtomicInteger size = new AtomicInteger(0);
	protected int maxSize = 100;

    public LockLinkedSegmentCache(int maxSize, int initialCapacity, float loadFactor, int concurrencyLevel) {
		super(initialCapacity, loadFactor, concurrencyLevel);
		setMaxSize(maxSize);
	}
	public LockLinkedSegmentCache() {
		super(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR, DEFAULT_SEGMENTS);
	}
	public LockLinkedSegmentCache(int initialCapacity, float loadFactor, int concurrencyLevel) {
		super(initialCapacity, loadFactor, concurrencyLevel);
	}

	@Override
	protected Segment<K,V,I> createSegment(int capacity, float loadFactor) {
		return new LockLinkedSegment<K,V,I>(capacity, loadFactor, MAXIMUM_CAPACITY) {
			private static final long serialVersionUID = 1324563639141134L;
			@Override
			protected V createValue(K key, Object... additionalInfo) throws I {
				return LockLinkedSegmentCache.this.createValue(key, additionalInfo);
			}

			@Override
			protected int decrementSize() {
				return size.decrementAndGet();
			}

			@Override
			protected int deltaSize(int delta) {
				return size.addAndGet(delta);
			}

			@Override
			protected int getMaxSize() {
				return LockLinkedSegmentCache.this.getMaxSize();
			}

			@Override
			protected int incrementSize() {
				return size.incrementAndGet();
			}
			@Override
			protected boolean expire(V value) {
				return false;
			}
			@Override
			protected boolean keyEquals(K key1, K key2) {
				return LockLinkedSegmentCache.this.keyEquals(key1, key2);
			}

			@Override
			protected boolean valueEquals(V value1, V value2) {
				return LockLinkedSegmentCache.this.valueEquals(value1, value2);
			}
		};
	}

	protected abstract V createValue(K key, Object... additionalInfo) throws I ;

	protected boolean keyEquals(K key1, K key2) {
		return CollectionUtils.deepEquals(key1, key2);
	}

	@Override
	protected boolean valueEquals(V value1, V value2) {
		return CollectionUtils.deepEquals(value1, value2);
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}
	@Override
	public int size() {
		return size.get();
	}

}
