/**
 * 
 */
package simple.util.concurrent;


public interface Task<A, R> {
	public R perform(A argument) throws TaskException;
}