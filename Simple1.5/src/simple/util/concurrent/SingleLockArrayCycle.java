package simple.util.concurrent;

import java.util.NoSuchElementException;
import java.util.concurrent.locks.ReentrantLock;

import simple.util.CollectionUtils;
import simple.util.Cycle;

public class SingleLockArrayCycle<E> implements Cycle<E> {
	protected final ReentrantLock lock = new ReentrantLock();
	protected E[] array;
	protected int position;
	protected int max;
	protected volatile int size;
	protected int open;
	public SingleLockArrayCycle(int initialCapacity, Class<E> elementType) {
		this.array = CollectionUtils.createArray(elementType, initialCapacity);
	}
	
	public E next() {
		E obj;
		lock.lock();
		try {
			if(isEmpty())
				throw new NoSuchElementException();
			do {
				obj = array[position++ % max];
			} while(obj == null) ;
		} finally {
			lock.unlock();
		}
		return obj;
	}
	public E[] drain() {
		lock.lock();
		try {
			E[] ret = CollectionUtils.createArray(array, size);
			for(int i = 0, orig = position; i < ret.length && position - orig < max; ) {
				int index = position++ % max;
				if(array[index] != null) {
					ret[i++] = array[index];
					array[index] = null;
				}
			}
			max = 0;
			size = 0;
			return ret;
		} finally {
			lock.unlock();
		}
	}
	public int add(E obj) {
		if(obj == null)
			throw new NullPointerException("null values not allowed in Cycle");
		lock.lock();
		try {
			size++;
			for(; open < max; open++) {
				if(array[open] == null) {
					array[open] = obj;
					return open++;
				}
			}
			if(array.length <= max) {
				E[] tmp = CollectionUtils.createArray(array, array.length * 2);
				System.arraycopy(array, 0, tmp, 0, max);
				array = tmp;
			}
			max++;
			array[open] = obj;
			return open++;
		} finally {
			lock.unlock();
		}
		
	}
	public boolean remove(int index, E obj) {
		lock.lock();
		try {
			if(array[index] == obj) {
				array[index] = null;
				if(open > index)
					open = index;
				size--;
				return true;
			}
		} finally {
			lock.unlock();
		}
		return false;
	}
	public boolean isEmpty() {
		return size == 0;
	}
	public int size() {
		return size;
	}
}
