package simple.util.concurrent;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public abstract class WeakCache<K,V,I extends Exception> implements Cache<K,V,I> {
	protected final Cache<K,ReferenceWithKey,I> delegate = createDelegate();
	protected final ReferenceQueue<V> referenceQueue = new ReferenceQueue<V>();

	protected class ReferenceWithKey extends WeakReference<V> {
		protected final K key;
		protected V strongRef;
		public ReferenceWithKey(K key, V referent) {
			super(referent, referenceQueue);
			this.key = key;
			this.strongRef = referent;
		}
		protected void free() {
			this.strongRef = null;
		}
		protected boolean isFree() {
			return this.strongRef == null;
		}
	}

	public WeakCache() {
	}

	protected Cache<K, ReferenceWithKey, I> createDelegate() {
		return new LockSegmentCache<K, ReferenceWithKey, I>() {
			@Override
			protected ReferenceWithKey createValue(K key, Object... additionalInfo) throws I {
				return createReference(key, additionalInfo);
			}
			@Override
			protected boolean keyEquals(K key1, K key2) {
				return WeakCache.this.keyEquals(key1, key2);
			}
			@Override
			protected boolean valueEquals(ReferenceWithKey value1, ReferenceWithKey value2) {
				return referenceEquals(value1, value2);
			}
		};
	}
	protected ReferenceWithKey createReference(K key, Object... additionalInfo) throws I {
		return new ReferenceWithKey(key, createValue(key, additionalInfo));
	}
	protected boolean referenceEquals(Reference<V> value1, Reference<V> value2) {
		return valueEquals(value1.get(), value2.get());
	}
	protected abstract V createValue(K key, Object... additionalInfo) throws I ;
	protected abstract boolean keyEquals(K key1, K key2) ;
	protected abstract boolean valueEquals(V value1, V value2) ;

	public void clear() {
		delegate.clear();
	}

	public V getIfPresent(K key, Object... additionalInfo) throws I {
		ReferenceWithKey ref = delegate.getIfPresent(key, additionalInfo);
		V value = ref.get();
		clean();
		ref.free();
		return value;
	}

	public V getOrCreate(K key, Object... additionalInfo) throws I {
		ReferenceWithKey ref = delegate.getOrCreate(key, additionalInfo);
		V value = ref.get();
		clean();
		ref.free();
		return value;
	}

	public void clean() {
		Reference<? extends V> ref;
		while((ref=referenceQueue.poll()) != null) {
			if(ReferenceWithKey.class.isInstance(ref)) {
				@SuppressWarnings("unchecked")
				ReferenceWithKey rwk = (ReferenceWithKey)ref;
				if(rwk.isFree())
					remove(rwk.key);
			}
		}
	}

	public Set<K> keySet() {
		return delegate.keySet();
	}

	public V remove(K key) {
		ReferenceWithKey ref = delegate.remove(key);
		return (ref == null ? null : ref.get());
	}

	public int size() {
		return delegate.size();
	}

	public Collection<V> values() {
		return new AbstractCollection<V>() {
			@Override
			public int size() {
				return delegate.size();
			}
			@Override
			public Iterator<V> iterator() {
				final Iterator<ReferenceWithKey> iter = delegate.values().iterator();
				return new Iterator<V>() {
					public boolean hasNext() {
						return iter.hasNext();
					}
					public V next() {
						return iter.next().get();
					}
					public void remove() {
						iter.remove();
					}
				};
			}
		};
	}
	
	public Set<Map.Entry<K, V>> entrySet() {
		return new AbstractSet<Map.Entry<K, V>>() {
			@Override
			public Iterator<Map.Entry<K, V>> iterator() {
				final Iterator<Map.Entry<K, ReferenceWithKey>> iter = delegate.entrySet().iterator();
				return new Iterator<Map.Entry<K, V>>() {
					public boolean hasNext() {
						return iter.hasNext();
					}
					public Map.Entry<K, V> next() {
						final Map.Entry<K, ReferenceWithKey> entry = iter.next();
						return new Map.Entry<K, V>() {
							public K getKey() {
								return entry.getKey();
							}
							public V getValue() {
								return entry.getValue().get();
							}
							public V setValue(V value) {
								return entry.setValue(new ReferenceWithKey(entry.getKey(), value)).get();
							}		
						};
					}
					public void remove() {
						iter.remove();
					}
				};
			}
			@Override
			public int size() {
				return delegate.size();
			}			
		};
	}
}
