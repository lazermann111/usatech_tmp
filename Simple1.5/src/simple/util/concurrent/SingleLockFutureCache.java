package simple.util.concurrent;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.locks.ReentrantLock;

import simple.util.LRUMap;

/**
 * Allows for caching the results of a call based on a set of arguments and returning the cached version if available
 * or executing the specified callable to create the results. Results can be expired and the cache size can be limited
 * (although this max cache size is approximate since it is implemented in each segment and not globally).
 * Many of the internal workings of this class were copied from java.util.concurrent.ConcurrentHashMap.
 *
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public abstract class SingleLockFutureCache<K,V> extends AbstractFutureCache<K,V> {
	protected final ReentrantLock lock = new ReentrantLock();
	protected final LRUMap<K, CacheFuture> map;

	public SingleLockFutureCache(int initialCapacity, float loadFactor) {
		if (!(loadFactor > 0) || initialCapacity < 0)
            throw new IllegalArgumentException();

		this.map = new LRUMap<K, CacheFuture>(1000, initialCapacity, loadFactor);
 	}

	@Override
	protected CacheFuture getFutureOrCreate(K key, Object... additionalInfo) {
		lock.lock();
		try {
			CacheFuture future = map.get(key);
			if(future == null) {
				future = createFuture(key);
				map.put(key, future);
			}
			return future;
		} finally {
			lock.unlock();
		}
	}
	@Override
	protected CacheFuture getFutureIfPresent(K key, Object... additionalInfo) {
		lock.lock();
		try {
			return map.get(key);
		} finally {
			lock.unlock();
		}
	}
	public V remove(K key) {
		CacheFuture future;
		lock.lock();
		try {
			future = map.remove(key);
		} finally {
			lock.unlock();
		}
		future.cancel(true);
		V old;
		if(!future.isDone())
			old = null;
		else {
			try {
				old = future.get();
			} catch(InterruptedException e) {
				old = null;
			} catch(ExecutionException e) {
				old = null;
			}
			future.reset();
		}
		return old;
	}

	public void clear() {
		lock.lock();
		try {
			map.clear();
		} finally {
			lock.unlock();
		}
	}

	@Override
	public int getMaxSize() {
		return map.getMaxSize();
	}

	@Override
	public void setMaxSize(int maxSize) {
		lock.lock();
		try {
			map.setMaxSize(maxSize);
		} finally {
			lock.unlock();
		}
	}

	public int size() {
		return map.size();
	}

	public Set<K> keySet() {
		return map.keySet(); // this is not thread-safe
	}

	public Set<Entry<K, V>> entrySet() {
		return new AbstractSet<Map.Entry<K, V>>() {
			@Override
			public Iterator<Map.Entry<K, V>> iterator() {
				final Iterator<Map.Entry<K, CacheFuture>> iter = map.entrySet().iterator();
				return new Iterator<Map.Entry<K, V>>() {
					public boolean hasNext() {
						return iter.hasNext();
					}
					public Map.Entry<K, V> next() {
						final Map.Entry<K, CacheFuture> entry = iter.next();
						return new Map.Entry<K, V>() {
							public K getKey() {
								return entry.getKey();
							}
							public V getValue() {
								try {
									return entry.getValue().get();
								} catch(InterruptedException e) {
									return null;
								} catch(ExecutionException e) {
									return null;
								}
							}
							public V setValue(V value) {
								return entry.getValue().set(value);
							}		
						};
					}
					public void remove() {
						iter.remove();
					}
				};
			}
			@Override
			public int size() {
				return map.size();
			}			
		};
	}
	public Collection<V> values() {
		return new AbstractCollection<V>() {
			@Override
			public Iterator<V> iterator() {
				return new Iterator<V>() {
					protected Iterator<CacheFuture> iter = map.values().iterator();
					public boolean hasNext() {
						return iter.hasNext();
					}

					public V next() {
						try {
							return iter.next().get();
						} catch(InterruptedException e) {
							throw new UndeclaredThrowableException(e);
						} catch(ExecutionException e) {
							throw new UndeclaredThrowableException(e);
						}
					}
					public void remove() {
						iter.remove();
					}
				};
			}

			@Override
			public int size() {
				return map.size();
			}
		};
	}
}
