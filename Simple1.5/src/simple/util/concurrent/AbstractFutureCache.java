package simple.util.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Allows for caching the results of a call based on a set of arguments and returning the cached version if available
 * or executing the specified callable to create the results. Results can be expired and the cache size can be limited
 * (although this max cache size is approximate since it is implemented in each segment and not globally).
 * Many of the internal workings of this class were copied from java.util.concurrent.ConcurrentHashMap.
 *
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public abstract class AbstractFutureCache<K,V> implements BoundedCache<K,V,ExecutionException>, FutureCache<K,V,ExecutionException>, FutureCountTracker {
	protected long expireTime = 10 * 60 * 1000; // 10-minutes

	protected long timeout = -1;

	protected int maxSize = 0; //unlimited

	//protected boolean keepUntilComplete = true;
	protected boolean trackingCounts = false;
	protected AtomicInteger missCount;
	protected AtomicInteger hitCount;
	protected AtomicInteger expiredCount;

	//public abstract FutureKeySet<K> keySet() ;

	protected abstract CacheFuture getFutureOrCreate(K key, Object... additionalInfo) ;
	protected abstract CacheFuture getFutureIfPresent(K key, Object... additionalInfo) ;

	protected class CacheFuture extends RecordedRunOnGetFuture<V> implements RecordedFuture<V> {
		protected final K key;
		public CacheFuture(K key) {
			this.key = key;
		}
		@Override
		public V call(Object... additionalInfo) throws Exception {
			return createValue(key, additionalInfo);
		}/*
		@Override
		protected void runComplete() {
			super.runComplete();
			if(keepUntilComplete) checkSize(key, this);
		}
		@Override
		protected void runFailed() {
			super.runFailed();
			if(keepUntilComplete) checkSize(key, this);
		}*/
		@Override
		protected void returningRunResult(V result) {
			if(isTrackingCounts()) {
				hitCount.incrementAndGet();
			}
			super.returningRunResult(result);
		}
		@Override
		protected void throwingRunException(ExecutionException exception) {
			if(isTrackingCounts()) {
				hitCount.incrementAndGet();
			}
			super.throwingRunException(exception);
		}
		protected void resettingRunResult(V result) {
			resetValue(key, result);
		};
	}
	protected CacheFuture createFuture(final K key, final Object... additionalInfo) {
		if(isTrackingCounts()) {
			missCount.incrementAndGet();
		}
		CacheFuture future = new CacheFuture(key);
		return future;
	}
	protected void resetValue(K key, V value) {
		// do nothing - this is to allow subclasses to do something
	}
	/*
	protected void checkSize(K key, RecordedFuture<V> future) {

	}*/
    protected V getValue(CacheFuture futureValue, Object[] additionalInfo) throws ExecutionException {
		if(futureValue == null)
			return null;
    	resetIfStale(futureValue);
    	try {
			return futureValue.get(getTimeout(), TimeUnit.MILLISECONDS, additionalInfo);
		} catch(InterruptedException e) {
			futureValue.reset();
			throw new ExecutionException(e);
		} catch(ExecutionException e) {
			futureValue.reset();
			throw e;
		} catch(TimeoutException e) {
			futureValue.reset();
			throw new ExecutionException(e);
		}
	}

    protected void resetIfStale(CacheFuture futureValue) {
    	if(futureValue.isCancelled())
    		futureValue.resetIfRunBefore(System.currentTimeMillis());
    	else if(getExpireTime() > 0)
    		if(futureValue.resetIfRunBefore(System.currentTimeMillis() - getExpireTime()) && isTrackingCounts()) {
    			expiredCount.incrementAndGet();
    		}
	}

 	public V getOrCreate(K key, Object... additionalInfo) throws ExecutionException {
 		CacheFuture future = getFutureOrCreate(key, additionalInfo);
		return getValue(future, additionalInfo);
	}

 	public V getIfPresent(K key, Object... additionalInfo) throws ExecutionException {
 		CacheFuture future = getFutureIfPresent(key, additionalInfo);
		return getValue(future, additionalInfo);
	}

    public V put(K key, V value) {
		return getFutureOrCreate(key).set(value);
	}

 	public V expireAndGet(K key, Object... additionalInfo) throws ExecutionException {
 		CacheFuture futureValue = getFutureOrCreate(key, additionalInfo);
		if(futureValue.resetIfRunBefore(System.currentTimeMillis()) && isTrackingCounts()) {
			expiredCount.incrementAndGet();
		}
		try {
			return futureValue.get(getTimeout(), TimeUnit.MILLISECONDS, additionalInfo);
		} catch(InterruptedException e) {
			futureValue.reset();
			throw new ExecutionException(e);
		} catch(ExecutionException e) {
			futureValue.reset();
			throw e;
		} catch(TimeoutException e) {
			futureValue.reset();
			throw new ExecutionException(e);
		}
	}

 	public boolean expire(K key) {
 		CacheFuture futureValue = getFutureIfPresent(key);
		if(futureValue == null)
			return false;
		boolean reset = futureValue.resetIfRunBefore(System.currentTimeMillis());
		if(reset && isTrackingCounts()) {
			expiredCount.incrementAndGet();
		}
		return reset;
	}
 	/*
	public Factory<K, Callable<V>> getCallableFactory() {
		return callableFactory;
	}

	public void setCallableFactory(Factory<K, Callable<V>> callableFactory) {
		this.callableFactory = callableFactory;
	}
	*/
	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

    public long getExpireTime() {
		return expireTime;
	}

    public void setExpireTime(long expirationTime) {
		this.expireTime = expirationTime;
	}

	public boolean isTrackingCounts() {
		return trackingCounts;
	}

	public void setTrackingCounts(boolean trackingCounts) {
		boolean oldTrackingCounts = this.trackingCounts;
		this.trackingCounts = trackingCounts;
		if(!oldTrackingCounts && trackingCounts) {
			hitCount = new AtomicInteger();
			missCount = new AtomicInteger();
			expiredCount = new AtomicInteger();
		}
	}

	public int getHitCount() {
		if(!isTrackingCounts())
			throw new IllegalStateException("Not tracking counts");
		return hitCount.get();
	}
	public int getMissCount() {
		if(!isTrackingCounts())
			throw new IllegalStateException("Not tracking counts");
		return missCount.get();
	}
	public int getExpiredCount() {
		if(!isTrackingCounts())
			throw new IllegalStateException("Not tracking counts");
		return expiredCount.get();
	}
	public int getAndResetHitCount() {
		if(!isTrackingCounts())
			throw new IllegalStateException("Not tracking counts");
		return hitCount.getAndSet(0);
	}
	public int getAndResetMissCount() {
		if(!isTrackingCounts())
			throw new IllegalStateException("Not tracking counts");
		return missCount.getAndSet(0);
	}
	public int getAndResetExpiredCount() {
		if(!isTrackingCounts())
			throw new IllegalStateException("Not tracking counts");
		return expiredCount.getAndSet(0);
	}

	protected abstract V createValue(K key, Object... additionalInfo) throws Exception ;

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
}
