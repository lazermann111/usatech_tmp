package simple.util.concurrent;

import java.util.concurrent.Callable;

public class RecordedRunOnGetFuture<V> extends RunOnGetFuture<V> implements RecordedFuture<V> {
	protected long lastRunTime;
	public RecordedRunOnGetFuture(Callable<V> callable) {
		super(callable);
	}
	protected RecordedRunOnGetFuture() {
		super();
	}
	public long getLastRunTime() {
		return lastRunTime;
	}

	@Override
	protected void runComplete(V result) {
		lastRunTime = System.currentTimeMillis();
	}

	public boolean resetIfRunBefore(long runTime) {
		return ((getState() == State.CANCELLED || lastRunTime < runTime) && reset());
	}
}
