package simple.util.concurrent;

import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import simple.io.Log;

/** This implementation just creates objects as it needs to and does not recycle them
 * @author Brian S. Krug
 *
 * @param <E>
 */
public abstract class AbstractCreatingPool<E> implements Pool<E> {
	private static final Log log = Log.getLog();
	protected final AtomicInteger numActive = new AtomicInteger();

	protected abstract E createObject() throws CreationException, InterruptedException ;
	protected abstract void closeObject(E obj) throws Exception ;
	public E borrowObject() throws TimeoutException, CreationException, InterruptedException {
		return createObject();
	}

	public void clear() {
		// do nothing
	}

	public void close() {
		clear();
	}

	public int getNumActive() {
		return numActive.get();
	}

	public int getNumIdle() {
		return 0;
	}

	public void invalidateObject(E obj) {
		numActive.decrementAndGet();
		closeSafely(obj);
	}

	public void returnObject(E obj) {
		numActive.decrementAndGet();
		closeSafely(obj);
	}

	protected void closeSafely(E obj) {
		try {
			closeObject(obj);
		} catch(Exception e) {
			log.info("Could not close object " + obj, e);
		}
	}
}
