package simple.util.concurrent;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class CustomThreadFactory implements ThreadFactory {
	protected final int priority; // Thread.NORM_PRIORITY
	protected final String namePrefix;
	protected final boolean daemon;
	protected final ThreadGroup group;
	protected final AtomicInteger threadNumber = new AtomicInteger(1);
    
	public CustomThreadFactory(String namePrefix, boolean daemon) {
		this(namePrefix, daemon, Thread.NORM_PRIORITY);
	}
	
	public CustomThreadFactory(String namePrefix, boolean daemon, int priority) {
		this(namePrefix, daemon, priority, Thread.currentThread().getThreadGroup());
	}
	
	public CustomThreadFactory(String namePrefix, boolean daemon, int priority, ThreadGroup group) {
		super();
		this.priority = priority;
		this.namePrefix = namePrefix;
		this.daemon = daemon;
		this.group = group;
	}

	public Thread newThread(Runnable r) {
		Thread t = new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), 0);
        t.setDaemon(isDaemon());
        t.setPriority(getPriority());
        return t;
	}

	public int getPriority() {
		return priority;
	}

	public String getNamePrefix() {
		return namePrefix;
	}

	public boolean isDaemon() {
		return daemon;
	}

	public ThreadGroup getGroup() {
		return group;
	}

}
