/*
 * Created on Jul 25, 2005
 *
 */
package simple.util.concurrent;

public class TaskException extends Exception {
	private static final long serialVersionUID = -5542434124100948L;

	public TaskException() {
        super();
    }

    public TaskException(String message) {
        super(message);
    }

    public TaskException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskException(Throwable cause) {
        super(cause);
    }

}
