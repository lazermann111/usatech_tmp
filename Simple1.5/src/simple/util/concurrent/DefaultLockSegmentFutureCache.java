package simple.util.concurrent;

import java.util.concurrent.ExecutionException;



/**
 * Allows for caching the results of a call based on a set of arguments and returning the cached version if available
 * or executing the specified callable to create the results. Results can be expired and the cache size can be limited
 * (although this max cache size is approximate since it is implemented in each segment and not globally).
 * Many of the internal workings of this class were copied from java.util.concurrent.ConcurrentHashMap.
 *
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public class DefaultLockSegmentFutureCache<K,V> extends LockSegmentFutureCache<K,V> implements FutureCacheWithFactory<K,V,ExecutionException>{
	protected Factory<K,V,Exception> valueFactory;

    public DefaultLockSegmentFutureCache(int maxSize, int initialCapacity, float loadFactor, int concurrencyLevel) {
		super(maxSize, initialCapacity, loadFactor, concurrencyLevel);
	}
    public DefaultLockSegmentFutureCache(int initialCapacity, float loadFactor, int concurrencyLevel) {
		super(initialCapacity, loadFactor, concurrencyLevel);
	}

	@Override
	protected V createValue(K key, Object... additionalInfo) throws Exception {
		return getValueFactory().create(key, additionalInfo);
	}
	public Factory<K, V, Exception> getValueFactory() {
		return valueFactory;
	}
	public void setValueFactory(Factory<K, V, Exception> valueFactory) {
		this.valueFactory = valueFactory;
	}
}
