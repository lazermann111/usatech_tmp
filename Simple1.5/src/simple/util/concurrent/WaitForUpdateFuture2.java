package simple.util.concurrent;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/** An implementation of Future that waits until the set() method is called to return from get().
 *  It also allows cancelling, specifying an error, and resetting. This
 *  implementation is thread-safe, locking on an ReentrantLock.
 *
 * @author Brian S. Krug
 *
 * @param <V>
 */
public class WaitForUpdateFuture2<V> implements ResettableFuture<V> {
	protected final static int CREATED = 0;
	protected final static int CANCELLED = 1;
	protected final static int RUNNING = 2;
	protected final static int DONE = 3;
	protected final static int EXCEPTION = 4;
	protected final static int RESETTING = 5;
	private final ReentrantLock lock = new ReentrantLock();
	private final AtomicInteger state = new AtomicInteger(CREATED);
	private V result;
	private ExecutionException exception;
	private Condition signal;
	private volatile boolean waiting = false;
	private boolean resetOnException;

	public WaitForUpdateFuture2() {

	}

	/**
	 * @see java.util.concurrent.Future#cancel(boolean)
	 */
	public boolean cancel(boolean mayInterruptIfRunning) {
		switch(state.intValue()) {
			case CREATED: case RUNNING:
				if(state.compareAndSet(CREATED, CANCELLED)) {
					return true;
				}
				else return cancel(mayInterruptIfRunning);
			default:
				return false;
		}
	}

	/**
	 * @see java.util.concurrent.Future#isCancelled()
	 */
	public boolean isCancelled() {
		return state.intValue() == CANCELLED;
	}

	/**
	 * @see java.util.concurrent.Future#isDone()
	 */
	public boolean isDone() {
		switch(state.intValue()) {
			case CANCELLED:
			case EXCEPTION:
			case DONE:
				return true;
			default:
				return false;
		}
	}

	/**
	 * @see java.util.concurrent.Future#get()
	 */
	public V get() throws InterruptedException, ExecutionException {
		try {
			return get(-1, null);
		} catch(TimeoutException e) {
			throw new IllegalStateException("This should never happen");
		}
	}

	/**
	 * @see java.util.concurrent.Future#get(long, java.util.concurrent.TimeUnit)
	 */
	public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		if(isResetOnException()) {
			int localState = state.intValue();
			// only try once, since reset() is the only thing that will change
			// status from CANCELLED, EXCEPTION, or DONE to something else
			switch(localState) {
				case CANCELLED:// always reset cancelled to created (no cleanup needed)
					state.compareAndSet(localState, CREATED);
					break;
				case EXCEPTION:
					if(state.compareAndSet(localState, RESETTING)) {
						lock.lock();
						try {
							result = null;
							exception = null;
							state.compareAndSet(RESETTING, CREATED);
						} finally {
							lock.unlock();
						}
					}
			}
		}
		switch(state.intValue()) {
			case CANCELLED:
				throw new CancellationException();
			case EXCEPTION:
				throw exception;
			case DONE:
				return result;
			case CREATED: case RUNNING: case RESETTING: default:
				// wait until done
				lock.lock();
				try {
					if(signal == null)
						signal = lock.newCondition();
					waiting = true;
					switch(state.intValue()) {
						case CREATED: case RUNNING: case RESETTING:
							if(timeout >= 0 && unit != null) {
								if(!signal.await(timeout, unit)) {//timeout
									throw new TimeoutException("Result not set after " + timeout + " " + unit.toString());
								}
							} else {
								signal.await();
							}
					}
				} finally {
					waiting = false;
					lock.unlock();
				}
				return get(timeout, unit);
		}
	}

	public boolean reset() {
		int localState = state.intValue();
		// only try once, since reset() is the only thing that will change
		// status from CANCELLED, EXCEPTION, or DONE to something else
		switch(localState) {
			case CREATED:
			case RUNNING:
			case RESETTING:
				return false;
			case CANCELLED:// always reset cancelled to created (no cleanup needed)
				return state.compareAndSet(localState, CREATED);
			case EXCEPTION:
			case DONE:
				if(state.compareAndSet(localState, RESETTING)) {
					lock.lock();
					try {
						result = null;
						exception = null;
						boolean ret = state.compareAndSet(RESETTING, CREATED);
						if(waiting) {
							signal.signalAll();
						}
						return ret;
					} finally {
						lock.unlock();
					}
				}
				return false;
		}
		throw new IllegalStateException("State is illegal value '" + localState + "'");
	}

	protected int getState() {
		return state.intValue();
	}

	public void set(V value) {
		result = value;
		state.set(DONE);
		if(waiting) {
			lock.lock();
			try {
				signal.signalAll();
			} finally {
				lock.unlock();
			}
		}
	}

	public void setException(ExecutionException exception) {
		this.exception = exception;
		state.set(EXCEPTION);
		if(waiting) {
			lock.lock();
			try {
				signal.signalAll();
			} finally {
				lock.unlock();
			}
		}
	}

	public boolean isResetOnException() {
		return resetOnException;
	}

	public void setResetOnException(boolean resetOnException) {
		this.resetOnException = resetOnException;
	}
}
