package simple.util.concurrent;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Turns out this has roughly the same performance as LockSegmentCache and LockSegmentFutureCache
 * 
 * @author bkrug
 * 
 * @param <K>
 * @param <V>
 * @param <I>
 */
public abstract class ConcurrentMapCache<K, V, I extends Exception> implements Cache<K, V, I> {
	protected final ConcurrentMap<K, V> map = new ConcurrentHashMap<K, V>();

	protected abstract V createValue(K key, Object... additionalInfo) throws I;

	@Override
	public V getOrCreate(K key, Object... additionalInfo) throws I {
		V value = map.get(key);
		if(value == null) {
			value = createValue(key, additionalInfo);
			V oldValue = map.putIfAbsent(key, value);
			if(oldValue != null)
				value = oldValue;
		}
		return value;
	}

	@Override
	public V getIfPresent(K key, Object... additionalInfo) throws I {
		return map.get(key);
	}

	@Override
	public V remove(K key) {
		return map.remove(key);
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public Set<K> keySet() {
		return map.keySet();
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return map.entrySet();
	}

	@Override
	public Collection<V> values() {
		return map.values();
	}
}
