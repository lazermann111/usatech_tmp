package simple.util.concurrent;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockingValidity implements Validity {
	protected final ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
	protected boolean valid = true;
	public void invalidate() {
		rwLock.writeLock().lock();
		try {
			valid = false;
		} finally {
			rwLock.writeLock().unlock();
		}
	}
	public boolean lockValid() {
		rwLock.readLock().lock();
		if(valid)
			return true;
		else {
			rwLock.readLock().unlock();
			return false;
		}	
	}
	public void unlockValid() {
		rwLock.readLock().unlock();
	}
}
