package simple.util.concurrent;

import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import simple.util.CollectionUtils;

/**
 * Segments are specialized versions of hash tables.  This
 * subclasses from ReentrantLock opportunistically, just to
 * simplify some locking and avoid separate construction.
 **/
public abstract class LockLinkedSegment<K,V,I extends Exception> extends ReentrantLock implements Segment<K,V,I> {
	/*
     * Segments maintain a table of entry lists that are ALWAYS
     * kept in a consistent state, so can be read without locking.
     * Next fields of nodes are immutable (final).  All list
     * additions are performed at the front of each bin. This
     * makes it easy to check changes, and also fast to traverse.
     * When nodes would otherwise be changed, new nodes are
     * created to replace them. This works well for hash tables
     * since the bin lists tend to be short. (The average length
     * is less than two for the default load factor threshold.)
     *
     */

    private static final long serialVersionUID = 1742546237849503380L;
	protected static class Entry<K,V> implements Map.Entry<K,V> {
		protected final K key;
		protected V value;
		protected final int hash;
		protected Entry<K,V> next;
		protected Entry<K,V> after; // tracks LRU
		protected Entry<K,V> before; // tracks LRU
		//protected boolean deleted = false;
		public Entry(K key, int hash, V value, Entry<K,V> next) {
			this.key = key;
			this.hash = hash;
			this.next = next;
			this.value = value;
		}
		public Entry(Entry<K,V> copy, Entry<K,V> next) {
			this.key = copy.key;
			this.hash = copy.hash;
			this.next = next;
			this.after = copy.after;
			this.before = copy.before;
			if(this.before != null)
				this.before.after = this;
			if(this.after != null)
				this.after.before = this;
			this.value = copy.value;
			//copy.deleted = true;
		}
		public K getKey() {
			return key;
		}
		public V getValue() {
			return value;
		}
		public V setValue(V value) {
			V oldValue = this.value;
			this.value = value;
			return oldValue;
		}
	}
	/**
     * The number of elements in this segment's region.
     **/
	protected transient volatile int count;

    /**
     * Number of updates that alter the size of the table. This is
     * used during bulk-read methods to make sure they see a
     * consistent snapshot: If modCounts change during a traversal
     * of segments computing size or checking containsValue, then
     * we might have an inconsistent view of state so (usually)
     * must retry.
     */
	protected transient int modCount;

    /**
     * The table is rehashed when its size exceeds this threshold.
     * (The value of this field is always (int)(capacity *
     * loadFactor).)
     */
	protected transient int threshold;

    /**
     * The per-segment table. Declared as a raw type, casted
     * to HashEntry<K,V> on each use.
     */
	protected transient volatile Entry<K,V>[] table;

	/**
	 * The least-recently used entry
	 */
	protected Entry<K,V> lru;
	/**
	 * The most-recently used entry
	 */
	protected Entry<K,V> mru;

	protected final float loadFactor;

	protected final int maximumCapacity;
	protected LockLinkedSegment(int initialCapacity, float loadFactor, int maximumCapacity) {
        this.loadFactor = loadFactor;
        this.maximumCapacity = maximumCapacity;
        setTable(newTable(initialCapacity));
    }

    /**
     * Set table to new HashEntry array.
     * Call only while holding lock or in constructor.
     **/
	protected void setTable(Entry<K,V>[] newTable) {
        threshold = (int)(newTable.length * loadFactor);
        table = newTable;
    }

    /**
     * Return properly casted first entry of bin for given hash
     */
	protected Entry<K,V> getFirst(int hash) {
        Entry<K,V>[] tab = table;
        return cast(tab[hash & (tab.length - 1)]);
    }
	@SuppressWarnings("unchecked")
	protected Entry<K,V>[] newTable(int length) {
		return new Entry[length];
	}
	@SuppressWarnings("unchecked")
	protected Entry<K,V> cast(Entry<?,?> e) {
		return (Entry<K,V>)e;
	}
	protected abstract int getMaxSize() ;
	protected abstract int decrementSize() ;
	protected abstract int incrementSize() ;
	protected abstract int deltaSize(int delta) ;
	protected abstract V createValue(K key, Object... additionalInfo) throws I;
	protected abstract boolean expire(V value) ;

	protected boolean matchesEntry(int hash, K key, Entry<K,V> entry) {
		return hash == entry.hash && keyEquals(key, entry.key);
	}

	protected boolean keyEquals(K key1, K key2) {
		return CollectionUtils.deepEquals(key1, key2);
	}

	protected boolean valueEquals(V value1, V value2) {
		return CollectionUtils.deepEquals(value1, value2);
	}

	protected void checkSize(int size) {
		int msz = getMaxSize(); // for read consistency
		if(msz > 0 && size > msz && count > 1) {
			Entry<K,V> e = lru;
			// Remove entry e
			int index = e.hash & (table.length - 1);
			//printState(index, "LRU before");
			Entry<K,V> first = cast(table[index]);
			//assert first != null;

			lru = e.after;
			//assert e.before == null;
			//assert e.deleted == false;
        	lru.before = null;
			e.before = null;
			e.after = null;
        	//e.deleted = true;

			if(first == e) {
				table[index] = e.next;
			} else {
				Entry<K,V> last = first;
				while(last.next != e) last = last.next;
				last.next = e.next;
			} /*else {
				Entry<K,V> newFirst = e.next;
				for(Entry<K,V> p = first; p != e; p = p.next) {
                	assert !p.key.equals(e.key);
                    newFirst = createEntry(p, newFirst);
                    if(lru == p) lru = newFirst;
                    if(mru == p) mru = newFirst;
                }
                table[index] = newFirst;
			}*/
            decrementSize();
            count--;
            //printState(index, "LRU after");
    		//assert lru.deleted == false;
		}
	}

    /**
     * Moves entry to mru and sets links appropriately
     */
	protected void moveToMRU(Entry<K,V> entry) {
		if(mru == null) {
			mru = entry;
			lru = entry;
		} else if(mru != entry) {
			Entry<K,V> prevMru = mru;
			mru = entry;
			if(entry.before != null)
				entry.before.after = entry.after;
			if(entry.after != null)
				entry.after.before = entry.before;
			entry.after = null;
			entry.before = prevMru;
			prevMru.after = entry;
		}
		//checkHashes();
		//checkLinked();
		//assert lru.deleted == false;
    }

	protected V getLockReluctant(K key, int hash, boolean createIfAbsent, Object... additionalInfo) throws I {
		//*
		int mc = modCount;
		Entry<K,V>[] tab = table; // for read-consistency
		int index = hash & (tab.length - 1);
        Entry<K,V> first = cast(tab[index]);
        for(Entry<K,V> e = first; e != null; e = e.next) {
            if(matchesEntry(hash, key, e)) {
            	lock();
            	try {
            		moveToMRU(e); //to make it easy we just lock on each getOrCreate call
            	} finally {
            		unlock();
            	}
            	return e.value;
            }
        }
        if(!createIfAbsent)
        	return null;
		//*/
		// now we check again under the lock and if not found create the future
        lock();
		try {
			//*
			if(mc != modCount) { // recheck
				if(tab != table) { // was re-hashed
					index = hash & (table.length - 1);
	                first = cast(table[index]);
				}
				// check again
				for(Entry<K,V> e = first; e != null; e = e.next) {
	                if(matchesEntry(hash, key, e)) {
	                	return e.value;
	                }
	            }
			}//*/
			/*
			int index = hash & (table.length - 1);
            Entry<K,V> first = cast(table[index]);
            for(Entry<K,V> e = first; e != null; e = e.next) {
                if (e.hash == hash && key.equals(e.key)) {
                	return e;
                }
            }
            */
			index = hash & (table.length - 1);
            first = cast(table[index]);

			if(count + 1 > threshold) {// ensure capacity
                rehash();
                index = hash & (table.length - 1);
                first = cast(table[index]);
			}
			V value = createValue(key, additionalInfo);
			Entry<K,V> e = createEntry(key, hash, value, first);
			//assert table[index] == first;
			table[index] = e;
			count++;
            int size = incrementSize();
            moveToMRU(e); // must do this before checkSize() or internal state will be inconsistent
            checkSize(size);
            modCount++;
            //printState(index, "Create");
       		return e.value;
		} finally {
			unlock();
		}
 	}
	protected V getLockEager(K key, int hash, boolean createIfAbsent, Object... additionalInfo) throws I {
		// now we check again under the lock and if not found create the future
        lock();
		try {
			int index = hash & (table.length - 1);
            Entry<K,V> first = cast(table[index]);
            for(Entry<K,V> e = first; e != null; e = e.next) {
                if (matchesEntry(hash, key, e)) {
                	return e.value;
                }
            }
            if(!createIfAbsent)
            	return null;
			if(count + 1 > threshold) {// ensure capacity
                rehash();
                index = hash & (table.length - 1);
                first = cast(table[index]);
			}
			V value = createValue(key, additionalInfo);
			Entry<K,V> e = createEntry(key, hash, value, first);
			//assert table[index] == first;
			table[index] = e;
			count++;
            int size = incrementSize();
            moveToMRU(e); // must do this before checkSize() or internal state will be inconsistent
            checkSize(size);
            modCount++;
            //printState(index, "Create");
       		return e.value;
		} finally {
			unlock();
		}
 	}
	protected Entry<K,V> createEntry(K key, int hash, V value, Entry<K,V> next) {
		return new Entry<K,V>(key, hash, value, next);
	}
	protected Entry<K, V> createEntry(Entry<K,V> copy, Entry<K,V> next) {
		return new Entry<K, V>(copy, next);
	}
	protected static enum PutAction { PUT, REPLACE, REPLACE_MATCH, IF_ABSENT };

	public V put(K key, int hash, V value, boolean onlyIfAbsent) {
		return put(key, hash, null, value, onlyIfAbsent ? PutAction.IF_ABSENT : PutAction.PUT);
	}
	public V replace(K key, int hash, V value) {
		return put(key, hash, null, value, PutAction.REPLACE);
	}
	public V replace(K key, int hash, V oldValue, V newValue) {
		return put(key, hash, oldValue, newValue, PutAction.REPLACE_MATCH);
	}
	protected V put(K key, int hash, V oldValue, V newValue, PutAction action) {
		// now we check again under the lock and if not found create the future
        lock();
		try {
			int index = hash & (table.length - 1);
            Entry<K,V> first = cast(table[index]);
            for(Entry<K,V> e = first; e != null; e = e.next) {
                if (matchesEntry(hash, key, e)) {
                	switch(action) {
                		case PUT: case REPLACE:
                			return e.setValue(newValue);
                		case REPLACE_MATCH:
                			if(valueEquals(e.getValue(), oldValue))
                				return e.setValue(newValue);
                			else
                				return oldValue == null ? e.getValue() : null;
                		case IF_ABSENT:
                			return e.getValue();
                	}
               }
            }
            switch(action) {
        		case REPLACE: case REPLACE_MATCH:
        			return null;
        	}
			if(count + 1 > threshold) {// ensure capacity
                rehash();
                index = hash & (table.length - 1);
                first = cast(table[index]);
			}
			Entry<K,V> e = createEntry(key, hash, newValue, first);
			//assert table[index] == first;
			table[index] = e;
			count++;
            int size = incrementSize();
            moveToMRU(e); // must do this before checkSize() or internal state will be inconsistent
            checkSize(size);
            modCount++;
            //printState(index, "Create");
       		return null;
		} finally {
			unlock();
		}
 	}
	public boolean containsKey(K key, int hash) {
		lock();
		try {
			int index = hash & (table.length - 1);
            Entry<K,V> first = cast(table[index]);
            for(Entry<K,V> e = first; e != null; e = e.next) {
                if (matchesEntry(hash, key, e)) {
                	return true;
                }
            }
            return false;
		} finally {
			unlock();
		}
	}
	protected void rehash() {
        Entry<K,V>[] oldTable = table;
        int oldCapacity = oldTable.length;
        if (oldCapacity >= maximumCapacity)
            return;

        /*
         * Reclassify nodes in each list to new Map.  Because we are
         * using power-of-two expansion, the elements from each bin
         * must either stay at same index, or move with a power of two
         * offset. We eliminate unnecessary node creation by catching
         * cases where old nodes can be reused because their next
         * fields won't change. Statistically, at the default
         * threshold, only about one-sixth of them need cloning when
         * a table doubles. The nodes they replace will be garbage
         * collectable as soon as they are no longer referenced by any
         * reader thread that may be in the midst of traversing table
         * right now.
         */

        Entry<K,V>[] newTable = newTable(oldCapacity << 1);
        threshold = (int)(newTable.length * loadFactor);
        int sizeMask = newTable.length - 1;
        //printLinked();
        for (int i = 0; i < oldCapacity ; i++) {
            // We need to guarantee that any existing reads of old Map can
            //  proceed. So we cannot yet null out each bin.
            final Entry<K,V> e = cast(oldTable[i]);

            if (e != null) {
            	//printEntries("Old Table " + i +" : ", e);
            	Entry<K,V> low = null;
            	Entry<K,V> high = null;
            	/*int n = 0;
            	for(Entry<K,V> p = e; p != null; p = p.next) {
            		n++;
            	}
            	if(n > 2) {
            		n= n;
            	}*/
            	for(Entry<K,V> p = e; p != null; p = p.next) {
            		int idx = (p.hash & sizeMask);
            		if(idx < oldCapacity) { //low
            			if(low == null) {
            				newTable[idx] = p;
            			} else if(low.next != p) {
            				low.next = p;
            			}
        				low = p;
            		} else {//high
            			if(high == null) {
            				newTable[idx] = p;
            			} else if(high.next != p) {
            				high.next = p;
            			}
        				high = p;
            		}
            	}
            	if(low != null && low.next != null) low.next = null;
            	if(high != null && high.next != null) high.next = null;

            	//printEntries("New Table " + i + " (Low): ", newTable[i]);
            	//printEntries("New Table " + i + " (High): ", newTable[i+oldCapacity]);

            	/*
                Entry<K,V> next = e.next;
                int idx = e.hash & sizeMask;

                //  Single node on list
                if (next == null)
                    newTable[idx] = e;

                else {
                    // Reuse trailing consecutive sequence at same slot
                    Entry<K,V> lastRun = e;
                    int lastIdx = idx;
                    for (Entry<K,V> last = next; last != null; last = last.next) {
                        int k = last.hash & sizeMask;
                        if (k != lastIdx) {
                            lastIdx = k;
                            lastRun = last;
                        }
                    }
                    newTable[lastIdx] = lastRun;

                    // Clone all remaining nodes
                    for (Entry<K,V> p = e; p != lastRun; p = p.next) {
                        int k = p.hash & sizeMask;
                        Entry<K,V> n = cast(newTable[k]);
                        Entry<K,V> c = createEntry(p, n);
                        newTable[k] = c;
                        if(p == lru) lru = c;
                        if(p == mru) mru = c;
                    }
                }*/
            }
            //printState(i, "Rehash");
            //printState((oldCapacity + i)-1, "Rehash");
        }
        table = newTable;
        //printLinked();
    }

	public V get(K key, int hash, boolean createIfAbsent, Object... additionalInfo) throws I {
		return getLockEager(key, hash, createIfAbsent, additionalInfo);
	}

	public int count() {
		return count;
	}
    /**
     * Remove
     */
	public V remove(K key, int hash, V value) {
	    lock();
        try {
            int index = hash & (table.length - 1);
            Entry<K,V> first = cast(table[index]);
            Entry<K,V> e = first;
            while(e != null && !matchesEntry(hash, key, e))
                e = e.next;

            if(e != null && (value == null || valueEquals(e.value, value))) {
            	if(lru == e) lru = e.after;
                if(mru == e) mru = e.before;

            	// Remove from linked list
            	if(e.before != null)
    				e.before.after = e.after;
    			if(e.after != null)
    				e.after.before = e.before;
    			e.before = null;
    			e.after = null;


                // All entries following removed node can stay
                // in list, but all preceding ones need to be cloned.
                ++modCount;
                Entry<K,V> newFirst = e.next;
                for(Entry<K,V> p = first; p != e; p = p.next) {
                    newFirst = createEntry(p, newFirst);
                    if(lru == p) lru = newFirst;
                    if(mru == p) mru = newFirst;
                }
                table[index] = newFirst;
                decrementSize();
                count--;
                //printState(index, "Remove");
                return e.value;
            }
            return null;
        } finally {
            unlock();
        }
    }

	public void clear() {
        lock();
        try {
            for (int i = 0; i < table.length ; i++)
                table[i] = null;
            ++modCount;
            deltaSize(-count);
            count = 0;
        } finally {
            unlock();
        }
    }

	public ExpirableIterator<Map.Entry<K,V>> iterator() {
		return new ExpirableIterator<Map.Entry<K,V>>() {
			protected Entry<K,V> next = lru;
			protected Entry<K,V> prev = null;

			public boolean hasNext() {
				return next != null;
			}
			public Map.Entry<K,V> next() {
				prev = next;
				next = next.after;
				return prev;
			}
			public void remove() {
				LockLinkedSegment.this.remove(prev.key, prev.hash, null);
				prev = null;
			}
			public boolean expire() {
				return LockLinkedSegment.this.expire(prev.value);
			}
		};
	}
	protected void printEntries(String prefix, Entry<K,V> entry) {
		StringBuilder sb = new StringBuilder();
		sb.append(prefix);
		for(Entry<K,V> e = entry; e != null; e = e.next)
			sb.append(e.key).append(" (").append(System.identityHashCode(e)).append(") --> ");
		System.out.println(sb);
	}
	protected void printState(int index, String occasion) {
		StringBuilder sb = new StringBuilder();
		sb.append("Segment ").append(hashCode()).append(" Index ").append(index).append(" for ").append(occasion).append(": ");
		for(Entry<K,V> e = cast(table[index]); e != null; e = e.next)
			sb.append(e.key).append(" (").append(System.identityHashCode(e)).append(") --> ");
		System.out.println(sb);
	}

	protected void printLinked() {
		StringBuilder sb = new StringBuilder();
		sb.append("LRU to MRU: ");
		for(Entry<K,V> e = lru; e != null; e = e.after) {
			//assert e.deleted == false;
			sb.append(e.key).append(" (").append(System.identityHashCode(e)).append(") --> ");
		}
		System.out.println(sb);
	}
	protected void checkLinked() {
		int mask = table.length - 1;
		OUTER: for(Entry<K,V> e = lru; e != null; e = e.after) {
			//assert e.deleted == false;
			int index = e.hash & mask;
			Entry<K,V> first = cast(table[index]);
			for(Entry<K,V> n = first; n != null; n = n.next) {
				if(n == e) continue OUTER;
			}
			System.out.print("BAD ");
			printLinked();
			assert false : "Entry not in table";
		}
	}
	protected void checkHashes() {
		int mask = table.length - 1;
		for(int i = 0; i < table.length; i++) {
			Entry<K,V> first = cast(table[i]);
			for(Entry<K,V> e = first; e != null; e = e.next) {
    			assert (e.hash & mask) == i;
    		}
		}
	}
}