package simple.util.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class ResultFuture<V> implements Future<V> {
	protected V result;
	public boolean cancel(boolean mayInterruptIfRunning) {
		return false;
	}
	public V get() throws InterruptedException, ExecutionException {
		try {
			return get(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
		} catch(TimeoutException e) {
			throw new ExecutionException(e);
		}
	}
	public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		return result = produceResult(timeout, unit);
	}
	public boolean isCancelled() {
		return false;
	}
	public boolean isDone() {
		return result != null;
	}
	protected abstract V produceResult(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException ;
}
