/**
 * 
 */
package simple.util.concurrent;

import java.util.Iterator;

public interface QueueBuffer<E> {
	public int getCapacity();

	public int size();

	public E take() throws InterruptedException;

	public void put(E message, boolean awaitNotFull) throws InterruptedException;

	public void put(Iterator<E> messages, boolean awaitNotFull) throws InterruptedException;

	public void awaitNotFull() throws InterruptedException;

}