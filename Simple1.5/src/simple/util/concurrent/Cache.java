package simple.util.concurrent;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Allows for caching the results of a call based on a set of arguments and returning the cached version if available
 * or executing the specified callable to create the results. Results can be expired and the cache size can be limited
 * (although this max cache size is approximate since it is implemented in each segment and not globally).
 * Many of the internal workings of this class were copied from java.util.concurrent.ConcurrentHashMap.
 *
 * @author Brian S. Krug
 *
 * @param <K> The type of the key
 * @param <V> The type of the value
 * @param <I> The type of exception that is (potentially) thrown
 */
public interface Cache<K,V,I extends Exception>  {

 	public V getOrCreate(K key, Object... additionalInfo) throws I ;

 	public V getIfPresent(K key, Object... additionalInfo) throws I;

 	public V remove(K key) ;

	public int size() ;

	public void clear() ;

	public Set<K> keySet() ;

	public Set<Map.Entry<K, V>> entrySet() ;
	
	public Collection<V> values() ;
}
