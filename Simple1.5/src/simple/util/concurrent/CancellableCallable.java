/**
 * 
 */
package simple.util.concurrent;

import java.util.concurrent.Callable;

public interface CancellableCallable<V> extends Callable<V> {
	public boolean cancel() ;
}