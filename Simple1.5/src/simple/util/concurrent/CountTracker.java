package simple.util.concurrent;


/**
 * Interface for tracking hit and miss counts
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public interface CountTracker  {
    public boolean isTrackingCounts() ;

	public void setTrackingCounts(boolean trackingStats) ;
	
	public int getHitCount() ;
	
	public int getMissCount() ;
	
	public int getAndResetHitCount() ;
	
	public int getAndResetMissCount() ;
}
