package simple.util.concurrent;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/** An implementation of Future that waits until the set() method is called to return from get().
 *  It also allows cancelling, specifying an error, and resetting. This
 *  implementation is thread-safe, locking on an ReentrantLock.
 *
 * @author Brian S. Krug
 *
 * @param <V>
 */
public class WaitForUpdateFuture<V> implements ResettableFuture<V> {
	protected final static int CREATED = 0;
	protected final static int CANCELLED = 1;
	protected final static int RUNNING = 2;
	protected final static int DONE = 3;
	protected final static int EXCEPTION = 4;
	private final ReentrantLock lock = new ReentrantLock();
	private final AtomicInteger state = new AtomicInteger(CREATED);
	private V result;
	private ExecutionException exception;
	private Condition signal;
	private boolean resetOnException;

	public WaitForUpdateFuture() {

	}

	/**
	 * @see java.util.concurrent.Future#cancel(boolean)
	 */
	public boolean cancel(boolean mayInterruptIfRunning) {
		try {
			if(!lock.tryLock(100, TimeUnit.MILLISECONDS))
				return false;
		} catch(InterruptedException e) {
			return false;
		}
		try {
			switch(state.intValue()) {
				case CREATED: case RUNNING:
					state.set(CANCELLED);
					if(signal != null)
						signal.signalAll();
					return true;
				default:
					return false;
			}
		} finally {
			lock.unlock();
		}
	}

	/**
	 * @see java.util.concurrent.Future#isCancelled()
	 */
	public boolean isCancelled() {
		return state.intValue() == CANCELLED;
	}

	/**
	 * @see java.util.concurrent.Future#isDone()
	 */
	public boolean isDone() {
		switch(state.intValue()) {
			case CANCELLED:
			case EXCEPTION:
			case DONE:
				return true;
			default:
				return false;
		}
	}

	public boolean isException() {
		switch(state.intValue()) {
			case EXCEPTION:
				return true;
			default:
				return false;
		}
	}

	/**
	 * @see java.util.concurrent.Future#get()
	 */
	public V get() throws InterruptedException, ExecutionException {
		try {
			return get(-1, null);
		} catch(TimeoutException e) {
			throw new IllegalStateException("This should never happen");
		}
	}

	/**
	 * @see java.util.concurrent.Future#get(long, java.util.concurrent.TimeUnit)
	 */
	public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		if(timeout > 0) {
			if(!lock.tryLock(timeout, unit))
				throw new TimeoutException("Could not acquire lock for this future in " + timeout + " " + unit.toString().toLowerCase());
		} else
			lock.lockInterruptibly();
		try {
			if(isResetOnException()) {
				switch(state.intValue()) {
					case CANCELLED: // always reset cancelled to created (no cleanup needed)
						state.set(CREATED);
						break;
					case EXCEPTION:
						result = null;
						exception = null;
						state.set(CREATED);
						break;
				}
			}
			switch(state.intValue()) {
				case CANCELLED:
					throw new CancellationException();
				case EXCEPTION:
					throw exception;
				case DONE:
					return result;
				case CREATED:
					state.set(RUNNING);
					run();
				case RUNNING: default:
					// wait until done
					if(signal == null)
						signal = lock.newCondition();
					if(timeout >= 0 && unit != null) {
						if(!signal.await(timeout, unit)) {//timeout
							throw new TimeoutException("Result not set after " + timeout + " " + unit.toString());
						}
					} else {
						signal.await();
					}
					return get(timeout, unit);
			}
		} finally {
			lock.unlock();
		}
	}

	@SuppressWarnings("unused")
	protected void run() throws ExecutionException {
	}

	public boolean reset() {
		lock.lock();
		try {
			result = null;
			exception = null;
			state.set(CREATED);
			if(signal != null)
				signal.signalAll();
			return true;
		} finally {
			lock.unlock();
		}
	}

	protected int getState() {
		return state.intValue();
	}

	public void set(V value) {
		lock.lock();
		try {
			result = value;
			state.set(DONE);
			if(signal != null)
				signal.signalAll();
		} finally {
			lock.unlock();
		}
	}

	public void setException(ExecutionException exception) {
		lock.lock();
		try {
			this.exception = exception;
			state.set(EXCEPTION);
			if(signal != null)
				signal.signalAll();
		} finally {
			lock.unlock();
		}
	}

	public boolean isResetOnException() {
		return resetOnException;
	}

	public void setResetOnException(boolean resetOnException) {
		this.resetOnException = resetOnException;
	}
}
