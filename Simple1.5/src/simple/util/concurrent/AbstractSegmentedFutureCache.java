package simple.util.concurrent;

import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import simple.util.CollectionUtils;

/**
 * Allows for caching the results of a call based on a set of arguments and returning the cached version if available
 * or executing the specified callable to create the results. Results can be expired and the cache size can be limited
 * (although this max cache size is approximate since it is implemented in each segment and not globally).
 * Many of the internal workings of this class were copied from java.util.concurrent.ConcurrentHashMap.
 *
 * @author Brian S. Krug
 *
 * @param <K>
 * @param <V>
 */
public abstract class AbstractSegmentedFutureCache<K,V> extends AbstractFutureCache<K,V> {
	protected abstract class AbstractInnerIterator<E> implements Iterator<E> {
		protected int index = 0;
		protected Iterator<Map.Entry<K,CacheFuture>> segmentIterator = cast(segments[0].iterator());
		@SuppressWarnings("unchecked")
		protected Iterator<Map.Entry<K,CacheFuture>> cast(Iterator<?> iterator) {
			return (Iterator<Map.Entry<K,CacheFuture>>)iterator;
		}
		protected void findNext() {
			while(index < segments.length - 1 && (segmentIterator == null || !segmentIterator.hasNext())) {
				segmentIterator = cast(segments[++index].iterator());
			}
		}
		public boolean hasNext() {
			findNext();
			return segmentIterator.hasNext();
		}
		public E next() {
			findNext();
			return convertEntry(segmentIterator.next());	
		}
		public void remove() {
			segmentIterator.remove();
		}
		protected abstract E convertEntry(Map.Entry<K,CacheFuture> entry) ;
	}
	protected class KeySet extends AbstractSet<K> {
		@Override
		public Iterator<K> iterator() {
			return new AbstractInnerIterator<K>() {
				@Override
				protected K convertEntry(Entry<K, CacheFuture> entry) {
					return entry.getKey();
				}
			};
		}

		@Override
		public int size() {
			return AbstractSegmentedFutureCache.this.size();
		}

		@Override
		public void clear() {
			AbstractSegmentedFutureCache.this.clear();
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean contains(Object o) {
			return getFutureIfPresent((K) o) != null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean remove(Object o) {
			return AbstractSegmentedFutureCache.this.remove((K) o) != null;
		}
	}
	protected class EntrySet extends AbstractSet<Map.Entry<K,V>> {
		@Override
		public Iterator<Map.Entry<K,V>> iterator() {
			return new AbstractInnerIterator<Map.Entry<K,V>>() {
				@Override
				protected Map.Entry<K, V> convertEntry(final Entry<K, CacheFuture> entry) {
					return new Map.Entry<K, V>() {
						public K getKey() {
							return entry.getKey();
						}
						public V getValue() {
							CacheFuture future = entry.getValue();
							return cleanupFuture(future);
						}
						public V setValue(V value) {
							V oldValue = getValue();
							entry.getValue().set(value);
							return oldValue;
						}
					};
				}
			};
		}

		@Override
		public int size() {
			return AbstractSegmentedFutureCache.this.size();
		}

		@Override
		public void clear() {
			AbstractSegmentedFutureCache.this.clear();
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean contains(Object o) {
			return getFutureIfPresent(((Map.Entry<K,V>) o).getKey()) != null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean remove(Object o) {
			return AbstractSegmentedFutureCache.this.remove(((Map.Entry<K,V>) o).getKey()) != null;
		}
	}
	protected class ValueCollection extends AbstractCollection<V> {
		@Override
		public Iterator<V> iterator() {
			return new AbstractInnerIterator<V>() {
				@Override
				protected V convertEntry(Entry<K, CacheFuture> entry) {
					CacheFuture future = entry.getValue();
					return cleanupFuture(future);
				}
			};
        }

		@Override
		public int size() {
		    return AbstractSegmentedFutureCache.this.size();
		}

		@Override
		public boolean contains(Object v) {
			Iterator<V> iter = iterator();
			while(iter.hasNext()) {
				if(iter.next().equals(v))
					return true;
			}
		    return false;
		}
    }

	protected Set<K> keySet;
	protected Set<Map.Entry<K,V>> entrySet;
	protected Collection<V> values;
	/**
     * Mask value for indexing into segments. The upper bits of a
     * key's hash code are used to choose the segment.
     **/
	protected final int segmentMask;

    /**
     * Shift value for indexing within segments.
     **/
	protected final int segmentShift;

    /**
     * The segments, each of which is a specialized hash table
     */
	protected final Segment<K,CacheFuture,RuntimeException>[] segments;


    /**
     * The default initial number of table slots for this table.
     * Used when not otherwise specified in constructor.
     */
    public static int DEFAULT_INITIAL_CAPACITY = 16;

    /**
     * The maximum capacity, used if a higher value is implicitly
     * specified by either of the constructors with arguments.  MUST
     * be a power of two <= 1<<30 to ensure that entries are indexible
     * using ints.
     */
    protected static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * The default load factor for this table.  Used when not
     * otherwise specified in constructor.
     */
    public static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * The default number of concurrency control segments.
     **/
    public static final int DEFAULT_SEGMENTS = 16;

    /**
     * The maximum number of segments to allow; used to bound
     * constructor arguments.
     */
    protected static final int MAX_SEGMENTS = 1 << 16; // slightly conservative

    /**
     * Returns a hash code for non-null Object x.
     * Uses the same hash code spreader as most other java.util hash tables.
     * @param x the object serving as a key
     * @return the hash code
     */
    protected int hash(Object x) {
    	return spreadHash(CollectionUtils.deepHashCode(x));
    }

    /**
     * Spreads out the hash codes.
     * Uses the same hash code spreader as most other java.util hash tables.
     * @param x the hash of the object
     * @return the spread hash code
     */
    protected static int spreadHash(int hash){
    	/*
    	hash += ~(hash << 9);
    	hash ^=  (hash >>> 14);
    	hash +=  (hash << 4);
    	hash ^=  (hash >>> 10);
    	//*/
    	//hash = Integer.reverse(hash);
    	/*hash = ((hash << 24) & 0xF000)
    		| ((hash << 8) & 0x0F00)
    		| ((hash >> 8) & 0x00F0)
    		| ((hash >> 24) & 0x000F);
    	//*/
        return hash;
    }

	public AbstractSegmentedFutureCache(int initialCapacity, float loadFactor, int concurrencyLevel) {
		if (!(loadFactor > 0) || initialCapacity < 0 || concurrencyLevel <= 0)
            throw new IllegalArgumentException();

        if (concurrencyLevel > MAX_SEGMENTS)
            concurrencyLevel = MAX_SEGMENTS;

        // Find power-of-two sizes best matching arguments
        int sshift = 0;
        int ssize = 1;
        while (ssize < concurrencyLevel) {
            ++sshift;
            ssize <<= 1;
        }
        //segmentShift = 32 - sshift;
        segmentShift = sshift;
        segmentMask = ssize - 1;
        segments = newSegments(ssize);

        if (initialCapacity > MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;
        int c = initialCapacity / ssize;
        if (c * ssize < initialCapacity)
            ++c;
        int cap = 1;
        while (cap < c)
            cap <<= 1;

        for (int i = 0; i < segments.length; ++i)
            segments[i] = createSegment(cap, loadFactor);
 	}

	@SuppressWarnings("unchecked")
	protected Segment<K,CacheFuture,RuntimeException>[] newSegments(int length) {
		return new Segment[length];
	}

	protected abstract Segment<K,CacheFuture,RuntimeException> createSegment(int capacity, float loadFactor) ;

	protected int getSegmentIndex(int hash) {
		return hash & segmentMask; //OLD: (hash >>> segmentShift) & segmentMask;
	}

	protected int getEntryHash(int hash) {
		return hash >>> segmentShift; // OLD: hash;
	}

	/**
     * Returns the segment that should be used for key with given hash
     * @param hash the hash code for the key
     * @return the segment
     */
    protected Segment<K,CacheFuture,RuntimeException> segmentFor(int hash) {
        return segments[getSegmentIndex(hash)];
    }

    @Override
    protected CacheFuture getFutureOrCreate(K key, Object... additionalInfo) {
		int hash = hash(key);
		return segmentFor(hash).get(key, getEntryHash(hash), true, additionalInfo);
	}
    @Override
    protected CacheFuture getFutureIfPresent(K key, Object... additionalInfo) {
		int hash = hash(key);
		return segmentFor(hash).get(key, getEntryHash(hash), false, additionalInfo);
	}

	protected V cleanupFuture(CacheFuture future) {
		if(future != null && !future.isCancelled()) {
			if(future.isDone())
				try {
					return future.get();
				} catch(InterruptedException e) {
				} catch(ExecutionException e) {
				}
			else
				future.cancel(true);
		}
		return null;
	}

	public V remove(K key) {
		int hash = hash(key);
		CacheFuture future = segmentFor(hash).remove(key, getEntryHash(hash), null);
		return cleanupFuture(future);
	}

	public void clear() {
		for(Segment<K,CacheFuture,RuntimeException> seg : segments) {
			seg.clear();
		}
	}

	public void printCounts() {
		for(int i = 0; i < segments.length; i++) {
			System.out.println("Segment " + (i+1) + " has " + segments[i].count() + " entries");
		}
	}

	public Set<K> keySet() {
		if(keySet == null) {
			keySet = new KeySet();
		}
		return keySet;
	}
	public Set<Entry<K, V>> entrySet() {
		if(entrySet == null) {
			entrySet = new EntrySet();
		}
		return entrySet;
	}
	public Collection<V> values() {
		if (values == null) {
		    values = new ValueCollection();
		}
		return values;
	}
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		Iterator<Entry<K,V>> i = entrySet().iterator();
		if (! i.hasNext())
		    return "{}";

		StringBuilder sb = new StringBuilder();
		sb.append('{');
		for (;;) {
		    Entry<K,V> e = i.next();
		    K key = e.getKey();
		    V value = e.getValue();
		    sb.append(key   == this ? "(this Map)" : key);
		    sb.append('=');
		    sb.append(value == this ? "(this Map)" : value);
		    if (! i.hasNext())
			return sb.append('}').toString();
		    sb.append(", ");
		}
	}
}
