package simple.util.concurrent;

public interface FutureCacheWithFactory<K,V,I extends Exception> extends FutureCache<K,V,I>, FutureCountTracker {
	public Factory<K, V, Exception> getValueFactory() ;
	public void setValueFactory(Factory<K, V, Exception> factory) ;
}
