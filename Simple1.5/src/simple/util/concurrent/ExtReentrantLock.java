/**
 *
 */
package simple.util.concurrent;

import java.util.concurrent.locks.ReentrantLock;

public class ExtReentrantLock extends ReentrantLock {
	private static final long serialVersionUID = -8003633227493748673L;
	public boolean isHeldBy(Thread thread) {
		return getOwner() == thread;
	}
}