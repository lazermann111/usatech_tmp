package simple.util.concurrent;


public interface Validity {
	public void invalidate() ;
	public boolean lockValid() ; 
	public void unlockValid() ;
}
