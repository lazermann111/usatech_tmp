package simple.util.concurrent;

public class ConcurrentQueueBufferFactory<E> implements QueueBufferFactory<E> {
	@Override
	public QueueBuffer<E> create(int capacity) {
		return new ConcurrentQueueBuffer<E>(capacity);
	}
}
