package simple.util.concurrent;

public interface CacheCountTracker {
	public long getInterval() ;
	public void setInterval(long interval) ;
	public void cacheAccessed(String cacheName, CountTracker cache) ;
}
