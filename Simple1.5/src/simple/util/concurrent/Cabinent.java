package simple.util.concurrent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import simple.bean.ConvertUtils;
import simple.io.Log;

public abstract class Cabinent<K, V> {
	private static final Log log = Log.getLog();
	protected final ConcurrentMap<K, CabinentPool> pools = new ConcurrentHashMap<>();
	protected final AtomicReference<CabinentPool> head = new AtomicReference<>();
	protected final AtomicReference<CabinentPool> tail = new AtomicReference<>();
	protected final AtomicInteger count = new AtomicInteger();
	protected long maxWait = 15000L;
	protected boolean tracing;
	protected int maxIdle = 4;
	protected int maxActive = 10;
	protected int maxKeys = 1000;
	protected long maxInactivity = 3600000L;

	protected class CabinentPool extends AbstractLockingPool<V> {
		protected final K key;
		protected final AtomicReference<CabinentPool> prev = new AtomicReference<CabinentPool>();
		protected final AtomicReference<CabinentPool> next = new AtomicReference<CabinentPool>();
		protected volatile long lastActive;

		public CabinentPool(K key) {
			super();
			this.key = key;
		}

		protected void deactivate() {
			// prev.next to next; and next.prev to prev
			CabinentPool formerPrev = prev.getAndSet(null);
			CabinentPool formerNext = next.getAndSet(null);
			if(formerPrev != null)
				formerPrev.next.compareAndSet(this, formerNext);
			else
				head.compareAndSet(this, formerNext);
			if(formerNext != null)
				formerNext.prev.compareAndSet(this, formerPrev);
			else
				tail.compareAndSet(this, formerPrev);
		}

		protected void activate() {
			lastActive = System.currentTimeMillis();
			if(ConvertUtils.areEqual(head.get(), this))
				return;
			deactivate();
			tail.compareAndSet(null, this);
			CabinentPool newNext = head.getAndSet(this);
			if(newNext != null) {
				next.compareAndSet(null, newNext);
				newNext.prev.compareAndSet(null, this);
			}
		}

		public void close() {
			pools.remove(key, this);
			super.close();
			deactivate();
		}
		public long getLastActive() {
			return lastActive;
		}

		public CabinentPool getPrev() {
			return prev.get();
		}

		public CabinentPool getNext() {
			return next.get();
		}

		protected void closeObject(V obj) throws Exception {
			closeItem(obj);
		}

		protected V createObject(long timeout) throws CreationException, TimeoutException, InterruptedException {
			V item;
			try {
				item = createItem(key);
			} catch(Exception e) {
				throw new CreationException(e);
			}
			try {
				configureItem(key, item, true);
			} catch(Exception e) {
				closeSafely(item);
				throw new CreationException(e);
			}
			if(log.isInfoEnabled())
				log.info("Created new item for " + key);

			return item;
		}

		@Override
		public V borrowObject() throws TimeoutException, CreationException, InterruptedException {
			activate();
			int limit = getMaxActive();
			while(true) {
				V item = super.borrowObject();
				try {
					configureItem(key, item, false);
					return item;
				} catch(Exception e) {
					invalidateObject(item);
					if(--limit < 0)
						throw new CreationException(e);
					// failed test. get a new one
					if(log.isInfoEnabled())
						log.info("Item is invalid: " + e.getMessage() + "; Getting a new one");
				}
			}
		}

		@Override
		public String toString() {
			return String.valueOf(key) + " (" + getNumActive() + " active, " + getNumIdle() + " idle)";
		}
	}

	public Cabinent() {
	}

	protected abstract void closeItem(V item) throws Exception;

	protected abstract V createItem(K key) throws Exception;

	protected abstract void configureItem(K key, V item, boolean initial) throws Exception;

	public V getItem(K key) throws CreationException, TimeoutException, InterruptedException {
		CabinentPool newPool;
		CabinentPool pool = pools.get(key);
		if(pool == null) {
			newPool = new CabinentPool(key);
			pool = pools.putIfAbsent(key, newPool);
			if(pool == null) {
				pool = newPool;
				pool.setMaxActive(maxActive);
				pool.setMaxIdle(maxIdle);
				pool.setMaxWait(maxWait);
				pool.setTracing(tracing);
				count.incrementAndGet();
			}
		} else
			newPool = null;
		try {
			return pool.borrowObject();
		} catch(CreationException e) {
			if(newPool != null) {
				count.decrementAndGet();
				newPool.close();
			}
			throw e;
		}
	}

	public void returnItem(K key, V item) {
		CabinentPool pool = pools.get(key);
		if(pool != null)
			pool.returnObject(item);
		else
			closeItemSafely(item);
	}

	protected void closeItemSafely(V item) {
		try {
			closeItem(item);
		} catch(Exception e) {
			log.info("Could not close object " + item + ": " + e.getMessage());
		}
	}

	public void invalidateItem(K key, V item) {
		CabinentPool pool = pools.get(key);
		if(pool != null)
			pool.invalidateObject(item);
		else
			closeItemSafely(item);
	}

	protected int purgeExpired() {
		int c = 0;
		long expireTime;
		if(getMaxInactivity() > 0L)
			expireTime = System.currentTimeMillis() - getMaxInactivity();
		else
			expireTime = 0L;

		for(CabinentPool prev, last = tail.get(); last != null; last = prev) {
			prev = last.getPrev();
			if(last.getLastActive() < expireTime) {
				count.decrementAndGet();
				last.close();
				c++;
			} else if(getMaxKeys() > 0) {
				while(true) {
					int current = count.get();
					if(current <= getMaxKeys())
						return c;
					if(count.compareAndSet(current, current - 1)) {
						last.close();
						c++;
						break;
					}
				}
			} else
				return c;
		}

		return c;
	}

	public ScheduledFuture<?> schedulePurgeJob(ScheduledExecutorService executor, long interval) {
		return executor.scheduleAtFixedRate(new Runnable() {
			public void run() {
				int c = purgeExpired();
				if(c > 0)
					log.info("Purged " + c + " keys because they were expired or over capacity");
			}
		}, interval, interval, TimeUnit.MILLISECONDS);
	}

	public long getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(long maxWait) {
		long old = this.maxWait;
		this.maxWait = maxWait;
		if(old != maxWait) {
			for(CabinentPool pool : pools.values())
				pool.setMaxWait(this.maxWait);
		}
	}

	public boolean isTracing() {
		return tracing;
	}

	public void setTracing(boolean tracing) {
		boolean old = this.tracing;
		this.tracing = tracing;
		if(old != tracing) {
			for(CabinentPool pool : pools.values())
				pool.setTracing(this.tracing);
		}
	}

	public int getMaxIdle() {
		return maxIdle;
	}

	public void setMaxIdle(int maxIdle) {
		int old = maxIdle;
		this.maxIdle = maxIdle;
		if(old != maxIdle) {
			for(CabinentPool pool : pools.values())
				pool.setMaxIdle(this.maxIdle);
		}
	}

	public int getMaxActive() {
		return maxActive;
	}

	public void setMaxActive(int maxActive) {
		int old = maxActive;
		this.maxActive = maxActive;
		if(old != maxActive) {
			for(CabinentPool pool : pools.values())
				pool.setMaxActive(this.maxActive);
		}
	}

	public int getMaxKeys() {
		return maxKeys;
	}

	public void setMaxKeys(int maxKeys) {
		this.maxKeys = maxKeys;
	}

	public long getMaxInactivity() {
		return maxInactivity;
	}

	public void setMaxInactivity(long maxInactivity) {
		this.maxInactivity = maxInactivity;
	}

	public int getNumKeys() {
		return count.get();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(CabinentPool pool = head.get(); pool != null; pool = pool.getNext()) {
			if(sb.length() > 0)
				sb.append(", ");
			sb.append(pool);
		}
		return sb.toString();
	}
	/** The sun.net.ftp.FtpClient does not correctly handle passive mode, so instead use FtpBean (in method ftp2Report)
     *
     */
 /*           try {
                // Make a client connection
                FtpClient ftp = new FtpClient();
                ftp.openServer(host, port);
                try {
                    // login
                    ftp.login(username, password);

                    // Change directory
                    if(remoteDir != null && remoteDir.trim().length() > 0) ftp.cd(remoteDir);

                    // Upload a file
                    OutputStream out = ftp.put(a.filename);
                    out.write(a.contents.getBytes());
                    out.flush();
                    out.close();
                } finally {
                    ftp.closeServer();
                }
            } catch(IOException ioe) {
                log.warn("While ftping file", ioe);
                throw new TransportException(ioe.getMessage());
            }
        */

}
