package simple.util.concurrent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import simple.io.Log;

/** This class allows only one thread at a time to receive permission to process
 *  a given resource. It is fully thread-safe
 * @author bkrug
 *
 */
public class SingleLockBottleneck<E> implements Bottleneck<E> {
	private static final Log log = Log.getLog();
	protected static class ReentrantLockWithCount extends ReentrantLock {
		private static final long serialVersionUID = -6027191326510549336L;
		protected int count;
	}
	protected final Map<E,ReentrantLockWithCount> lockedResources = new HashMap<E, ReentrantLockWithCount>();
	protected final ReentrantLock lock = new ReentrantLock();
	protected int unusedCount = 0;
	protected ReentrantLockWithCount[] unusedLocks = new ReentrantLockWithCount[10];
	
	public SingleLockBottleneck() {
		super();
	}

	public void lock(E resource) throws InterruptedException {
		boolean created = false;
		ReentrantLockWithCount resourceLock;
		lock.lock();
		try {
			resourceLock = lockedResources.get(resource);
			if(resourceLock == null) {
				if(unusedCount > 0)
					resourceLock = unusedLocks[--unusedCount];
				else {
					resourceLock = new ReentrantLockWithCount();
					created = true;
				}
				lockedResources.put(resource, resourceLock);
			}
			resourceLock.count++;
		} finally {
			lock.unlock();
		}
		resourceLock.lockInterruptibly(); // We must lock the resourceLock outside of the main lock so as to avoid deadlock
		if(created && log.isDebugEnabled())
			log.debug("Created new ReentrantLock for " + resource);
	}
	
	public void unlock(E resource) {
		boolean discarded = false;
		lock.lock();
		try {
			ReentrantLockWithCount resourceLock = lockedResources.get(resource);
			if(resourceLock != null) {
				resourceLock.unlock();
				resourceLock.count--;
				if(resourceLock.count <= 0) {
					lockedResources.remove(resource);
					if(unusedCount < unusedLocks.length) {
						unusedLocks[unusedCount++] = resourceLock;
					} else {
						discarded = true;
					}
				}
			}
		} finally {
			lock.unlock();
		}
		if(discarded && log.isInfoEnabled())
			log.info("Discarded ReentrantLock because idle pool is full for " + resource);
	}

	public int getMaxIdle() {
		return unusedLocks.length;
	}

	public void setMaxIdle(int maxIdle) {
		lock.lock();
		try {
			if(maxIdle > unusedLocks.length) {
				ReentrantLockWithCount[] tmp = new ReentrantLockWithCount[maxIdle];
				System.arraycopy(unusedLocks, 0, tmp, 0, unusedLocks.length);
				unusedLocks = tmp;
			} else if(maxIdle < unusedLocks.length) {
				ReentrantLockWithCount[] tmp = new ReentrantLockWithCount[maxIdle];
				System.arraycopy(unusedLocks, 0, tmp, 0, tmp.length);
				unusedLocks = tmp;
				if(unusedCount > unusedLocks.length)
					unusedCount = unusedLocks.length;
			} 
		} finally {
			lock.unlock();
		}
	}
}
