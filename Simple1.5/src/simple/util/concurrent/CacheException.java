/*
 * Created on Jul 25, 2005
 *
 */
package simple.util.concurrent;

public class CacheException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -5545149612348L;

	public CacheException() {
        super();
    }

    public CacheException(String message) {
        super(message);
    }

    public CacheException(String message, Throwable cause) {
        super(message, cause);
    }

    public CacheException(Throwable cause) {
        super(cause);
    }

}
