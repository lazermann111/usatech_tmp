package simple.util.concurrent;

public interface QueueBufferFactory<E> {
	public QueueBuffer<E> create(int capacity);
}
