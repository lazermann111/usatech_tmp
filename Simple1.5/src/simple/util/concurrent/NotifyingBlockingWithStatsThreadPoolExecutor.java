package simple.util.concurrent;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import simple.app.LongTotals;
import simple.app.RollingSample;
import simple.app.Totals;

public class NotifyingBlockingWithStatsThreadPoolExecutor extends NotifyingBlockingThreadPoolExecutor implements ThreadPoolExecutorMXBean {
	// Use a WeakHashMap just in case the afterExecute() method is not called
	protected final Map<Runnable, long[]> runnableTimes = Collections.synchronizedMap(new WeakHashMap<Runnable, long[]>()); // new ConcurrentHashMap<Runnable, long[]>();

	protected static class Stats {
		public final long latency;
		public final long processingTime;

		public Stats(long latency, long processingTime) {
			this.latency = latency;
			this.processingTime = processingTime;
		}
	}

	protected final RollingSample<Stats> sample = new RollingSample<Stats>(new Totals<Stats>() {
		protected final LongTotals latency = new LongTotals();
		protected final LongTotals processingTime = new LongTotals();

		public void replace(Stats oldStats, Stats newStats) {
			latency.replace(oldStats == null ? 0L : oldStats.latency, newStats.latency);
			processingTime.replace(oldStats == null ? 0L : oldStats.processingTime, newStats.processingTime);
		}

		public Stats getSampleTotal() {
			return new Stats(latency.getSampleTotal().longValue(), processingTime.getSampleTotal().longValue());
		}

		public Stats getGrandTotal() {
			return new Stats(latency.getGrandTotal().longValue(), processingTime.getGrandTotal().longValue());
		}
	}, 100);

	public NotifyingBlockingWithStatsThreadPoolExecutor(int poolSize, int queueSize, long keepAliveTime, TimeUnit keepAliveTimeUnit, long maxBlockingTime, TimeUnit maxBlockingTimeUnit, Callable<Boolean> blockingTimeCallback) {
		super(poolSize, queueSize, keepAliveTime, keepAliveTimeUnit, maxBlockingTime, maxBlockingTimeUnit, blockingTimeCallback);
	}

	public NotifyingBlockingWithStatsThreadPoolExecutor(int poolSize, int queueSize, long keepAliveTime, TimeUnit unit) {
		super(poolSize, queueSize, keepAliveTime, unit);
	}

	public NotifyingBlockingWithStatsThreadPoolExecutor(int poolSize, int queueSize, ThreadFactory threadFactory) {
		super(poolSize, queueSize, threadFactory);
	}

	@Override
	public void execute(Runnable task) {
		runnableTimes.put(task, new long[] { System.currentTimeMillis(), 0L });
		boolean okay = false;
		try {
			super.execute(task);
			okay = true;
		} finally {
			if(!okay)
				runnableTimes.remove(task);
		}
	}

	@Override
	protected void beforeExecute(Thread t, Runnable r) {
		super.beforeExecute(t, r);
		long[] times = runnableTimes.get(r);
		if(times != null && times.length > 1)
			times[1] = System.currentTimeMillis();
	}

	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		long end = System.currentTimeMillis();
		try {
			super.afterExecute(r, t);
		} finally {
			long[] times = runnableTimes.remove(r);
			if(times != null && times.length > 1)
				sample.addStats(new Stats(times[1] - times[0], end - times[1]));
		}
	}

	/* (non-Javadoc)
	 * @see simple.util.concurrent.ThreadPoolExecutorMXBean#getTotalProcessingCount()
	 */
	public long getTotalProcessingCount() {
		return sample.getGrandSize();
	}

	/* (non-Javadoc)
	 * @see simple.util.concurrent.ThreadPoolExecutorMXBean#getTotalLatencyTime()
	 */
	public long getTotalLatencyTime() {
		return sample.getGrandTotal().latency;
	}

	/* (non-Javadoc)
	 * @see simple.util.concurrent.ThreadPoolExecutorMXBean#getTotalProcessingTime()
	 */
	public long getTotalProcessingTime() {
		return sample.getGrandTotal().processingTime;
	}

	/* (non-Javadoc)
	 * @see simple.util.concurrent.ThreadPoolExecutorMXBean#getSampleSize()
	 */
	public int getSampleSize() {
		return sample.getSampleSize();
	}

	/* (non-Javadoc)
	 * @see simple.util.concurrent.ThreadPoolExecutorMXBean#getAvgLatencyTime()
	 */
	public Double getAvgLatencyTime() {
		return sample.getSampleTotal().latency / (double) sample.getSampleSize();
	}

	/* (non-Javadoc)
	 * @see simple.util.concurrent.ThreadPoolExecutorMXBean#getAvgProcessingTime()
	 */
	public Double getAvgProcessingTime() {
		return sample.getSampleTotal().processingTime / (double) sample.getSampleSize();
	}
}
