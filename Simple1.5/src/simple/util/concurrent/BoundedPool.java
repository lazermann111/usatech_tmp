/**
 *
 */
package simple.util.concurrent;

import java.util.concurrent.TimeoutException;

import simple.lang.Decision;

/**
 * @author Brian S. Krug
 *
 * @param <E>
 */
public interface BoundedPool<E> extends Pool<E> {

	public int getMaxActive();

	public void setMaxActive(int maxActive);

	public int getMaxIdle();

	public void setMaxIdle(int maxIdle);

	public long getMaxWait();

	public void setMaxWait(long maxWait);
	
	public boolean addObject() throws CreationException, TimeoutException, InterruptedException ;
	
	public int evict(Decision<E> evictDecision, int maxEvictable, int minIdle);

}