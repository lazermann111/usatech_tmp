package simple.util.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/** An implementation of Future that executes the specified callable the first time
 *  the get() method is called. It also allows for cancelling the Callable if the
 *  Callable implements CancellableCallable. In addition, this object may be reset()
 *  so that the Callable will be executed again on the next get() invocation. This
 *  implementation is thread-safe, but since the intricacies of thread-safe non-locking
 *  processing are seriously complicated, this implementation locks on each call which is a
 *  safer, but potentially slightly slower alternative to RunOnGetFuture2. NOTE: the callable
 *  object passed to the constructor of this class, DOES NOT get nulled out. If you
 *  don't care to reset() this future then clean-up in the callable's call method before
 *  returning the result.
 *
 * @author Brian S. Krug
 *
 * @param <V>
 */
public class RunOnGetFuture<V> implements ResettableFuture<V> {
	protected static enum State { CREATED, RUNNING, CANCELLED, DONE, EXCEPTION };
	private final ReentrantLock lock = new ReentrantLock();
	protected Callable<V> callable;
	private State state = State.CREATED;
	private V result;
	private ExecutionException exception;
	private Thread executor;
	private Condition signal;
	private boolean resetOnException;

	public RunOnGetFuture(Callable<V> callable) {
		this.callable = callable;
	}

	protected RunOnGetFuture() {
	}

	/**
	 * @see java.util.concurrent.Future#cancel(boolean)
	 */
	public boolean cancel(boolean mayInterruptIfRunning) {
		lock.lock();
		try {
			switch(state) {
				case CREATED:
					state = State.CANCELLED;
					return true;
				case RUNNING:
					Callable<V> localCallable = callable;//for read consistency
					if(!mayInterruptIfRunning) {
						return false;
					} else if(localCallable instanceof CancellableCallable<?>) {
						return ((CancellableCallable<V>)localCallable).cancel();
					} else if(executor != null && executor != Thread.currentThread()) {
						executor.interrupt();
						return true;
					} else {
						return false;
					}
				case CANCELLED: case EXCEPTION: case DONE: default:
					return false;
			}
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Sets the value and marks Future as done. Returns the old value or null if not set.
	 */
	public V set(V value) {
		lock.lock();
		try {
			switch(state) {
				case RUNNING:
					Callable<V> localCallable = callable;//for read consistency
					if(localCallable instanceof CancellableCallable<?>) {
						((CancellableCallable<V>)localCallable).cancel();
					} else if(executor != null) {
						executor.interrupt();
					}
			}
			V old = result;
			result = value;
			runComplete(result);
			state = State.DONE;
			return old;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * @see java.util.concurrent.Future#isCancelled()
	 */
	public boolean isCancelled() {
		return state == State.CANCELLED;
	}

	/**
	 * @see java.util.concurrent.Future#isDone()
	 */
	public boolean isDone() {
		switch(state) {
			case CANCELLED:
			case EXCEPTION:
			case DONE:
				return true;
			default:
				return false;
		}
	}

	/**
	 * @see java.util.concurrent.Future#get()
	 */
	public V get() throws InterruptedException, ExecutionException {
		try {
			return getInternal(-1, -1, null);
		} catch(TimeoutException e) {
			throw new IllegalStateException("This should never happen");
		}
	}

	/**
	 * @see java.util.concurrent.Future#get(long, java.util.concurrent.TimeUnit)
	 */
	public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		return getInternal(timeout, timeout, unit);
	}

	/**
	 * @see java.util.concurrent.Future#get()
	 */
	public V get(Object... additionalInfo) throws InterruptedException, ExecutionException {
		try {
			return getInternal(-1, -1, null, additionalInfo);
		} catch(TimeoutException e) {
			throw new IllegalStateException("This should never happen");
		}
	}

	/**
	 * @see java.util.concurrent.Future#get(long, java.util.concurrent.TimeUnit)
	 */
	public V get(long timeout, TimeUnit unit, Object... additionalInfo) throws InterruptedException, ExecutionException, TimeoutException {
		return getInternal(timeout, timeout, unit, additionalInfo);
	}

	protected V getInternal(long originalTimeout, long timeout, TimeUnit unit, Object... additionalInfo) throws InterruptedException, ExecutionException, TimeoutException {
		long start;
		if(timeout > 0 && unit != null) {
			start = System.nanoTime();
			if(!lock.tryLock(timeout, unit))
				throw new TimeoutException("Could not acquire lock in " + originalTimeout + " " + unit.toString());
		} else {
			start = 0;
			lock.lock();
		}
		try {
			if(isResetOnException()) {
				switch(state) {
					case CANCELLED: // always reset cancelled to created (no cleanup needed)
						state = State.CREATED;
						break;
					case EXCEPTION:
						resettingRunResult(result);
						result = null;
						exception = null;
						state = State.CREATED;
						break;
				}
			}
			switch(state) {
				case CREATED:
					executor = Thread.currentThread();
					state = State.RUNNING;
					lock.unlock(); //unlock so call can be cancelled
					try {
						try {
							result = call(additionalInfo);
						} catch(Exception e) {
							lock.lock();
							switch(state) {
								case CREATED:
									if(timeout > 0 && unit != null) {
										timeout = unit.toNanos(timeout) - (System.nanoTime() - start);
										if(timeout <= 0)
											throw new TimeoutException("Could not get result in " + originalTimeout + " " + unit.toString());
										timeout = unit.convert(timeout, TimeUnit.NANOSECONDS);
									}
									return getInternal(originalTimeout, timeout, unit, additionalInfo);
								case RUNNING:
								case EXCEPTION:
									exception = new ExecutionException(e);
									runFailed(exception);
									state = State.EXCEPTION;
									throw exception;
								case DONE:
									returningRunResult(result);
									return result;
								case CANCELLED:
									throw new CancellationException();
							}
						} catch(Error e) {
							lock.lock();
							throw e;
						}
						lock.lock();
						runComplete(result);
						state = State.DONE;
						return result;
					} finally {
						executor = null;
						if(signal != null)
							signal.signalAll();
					}
				case RUNNING:
					//wait for signal
					if(signal == null)
						signal = lock.newCondition();
					if(timeout > 0 && unit != null) {
						signal.await(timeout, unit);
						switch(state) {
							case CANCELLED:
								throw new CancellationException();
							case EXCEPTION:
								throwingRunException(exception);
								throw exception;
							case DONE:
								returningRunResult(result);
								return result;
						}
						timeout = unit.toNanos(timeout) - (System.nanoTime() - start);
						if(timeout <= 0)
							throw new TimeoutException("Could not get result in " + originalTimeout + " " + unit.toString());
						timeout = unit.convert(timeout, TimeUnit.NANOSECONDS);
					} else {
						signal.await();
						switch(state) {
							case CANCELLED:
								throw new CancellationException();
							case EXCEPTION:
								throwingRunException(exception);
								throw exception;
							case DONE:
								returningRunResult(result);
								return result;
						}
					}
					return getInternal(originalTimeout, timeout, unit, additionalInfo);
				case CANCELLED:
					throw new CancellationException();
				case EXCEPTION:
					throwingRunException(exception);
					throw exception;
				case DONE: default:
					returningRunResult(result);
					return result;
			}
		} finally {
			lock.unlock();
		}
	}

	public boolean reset() {
		lock.lock();
		try {
			switch(state) {
				case CREATED:
				case RUNNING:
				//case RESETTING:
					return false;
				case CANCELLED:// always reset cancelled to created (no cleanup needed)
					state = State.CREATED;
					return true;
				case EXCEPTION:
				case DONE: default:
					resettingRunResult(result);
					result = null;
					exception = null;
					state = State.CREATED;
					if(signal != null)
						signal.signal();
					return true;
			}
		} finally {
			lock.unlock();
		}
	}

	protected State getState() {
		return state;
	}

	/** Hook for sub-classes if they wish to know that a run of the callable was successfully completed
	 * @param result
	 *
	 */
	protected void runComplete(V result) {
		//do nothing
	}

	/** Hook for sub-classes if they wish to know that a run of the callable failed
	 * @param e
	 *
	 */
	protected void runFailed(ExecutionException exception) {
		//do nothing
	}

	/** Hook for sub-classes if they wish to know that the run exception was re-thrown from a get() call
	 *
	 */
	protected void throwingRunException(ExecutionException exception) {
		//do nothing
	}
	/** Hook for sub-classes if they wish to know that the run result was returned to another call to get()
	 *
	 */
	protected void returningRunResult(V result) {
		//do nothing
	}
	/** Hook for sub-classes if they wish to perform additional cleanup upon reset
	 *
	 */
	protected void resettingRunResult(V result) {
		//do nothing
	}
	public Callable<V> getCallable() {
		return callable;
	}

	public void setCallable(Callable<V> callable) {
		this.callable = callable;
	}

	protected V call(Object... additionalInfo) throws Exception {
		return callable.call();
	}

	public boolean isResetOnException() {
		return resetOnException;
	}

	public void setResetOnException(boolean resetOnException) {
		this.resetOnException = resetOnException;
	}
}
