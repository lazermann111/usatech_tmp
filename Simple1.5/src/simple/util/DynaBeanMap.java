/*
 * CompositeMap.java
 *
 * Created on March 18, 2004, 4:37 PM
 */

package simple.util;

import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.DynaProperty;

import simple.bean.DynaBean;

/**
 *
 * @author  Brian S. Krug
 */
public class DynaBeanMap implements Map<String,Object> {
    protected DynaBean delegate;
    protected Set<String> keySet;
    protected Set<Map.Entry<String,Object>> entrySet;
    protected Collection<Object> values;
    
    protected class DynaBeanKeySet extends AbstractSet<String> implements Set<String> {        
        public boolean contains(Object o) {
            return containsKey(o);
        }
                
        public boolean isEmpty() {
            return DynaBeanMap.this.isEmpty();
        }
        
        public Iterator<String> iterator() {
            return new DynaBeanKeysIterator();
        }
                
        public int size() {
            return DynaBeanMap.this.size();
        }                
    }
    
    protected class DynaBeanEntrySet extends AbstractSet<Map.Entry<String,Object>> implements Set<Map.Entry<String,Object>> {        
        public boolean contains(Object o) {
            return containsKey(o);
        }
                
        public boolean isEmpty() {
            return DynaBeanMap.this.isEmpty();
        }
        
        public Iterator<Map.Entry<String,Object>> iterator() {
            return new DynaBeanEntriesIterator();
        }
                
        public int size() {
            return DynaBeanMap.this.size();
        }
                
    }

    protected class DynaBeanValues extends AbstractCollection<Object> implements Collection<Object> {        
        public boolean contains(Object o) {
            return containsKey(o);
        }
                
        public boolean isEmpty() {
            return DynaBeanMap.this.isEmpty();
        }
        
        public Iterator<Object> iterator() {
            return new DynaBeanValuesIterator();
        }
                
        public int size() {
            return DynaBeanMap.this.size();
        }
                
    }
    
    /** Creates a new instance of CompositeMap */
    public DynaBeanMap(DynaBean delegate) {
        this.delegate = delegate;
    }
            
    public Object get(Object key) {
        return delegate.get((String) key);        
    }
    
    public Object put(String key, Object value) {
        Object old = delegate.get(key);
        delegate.set(key, value);
        return old;      
    }
    
    public void putAll(Map<? extends String,? extends Object> m) {
        throw new UnsupportedOperationException("PutAll is unsupported; use merge() to merge another Map into this composite");
    }
    
    public void clear() {
        throw new UnsupportedOperationException("Clear is unsupported; use unmerge() to remove a Map from this composite");
    }
    
    public boolean containsKey(Object key) {
        return delegate.get((String)key) != null;
    }
    
    /**
    *  Provides a string representation of the entries within the map.
    **/
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append('[');
        for(Map.Entry<String,Object> entry : entrySet()) {
            buf.append(entry.getKey());
            buf.append('=');
            buf.append(entry.getValue());
            buf.append(',');
        }
        buf.setLength(buf.length()-1);
        buf.append(']');

        return buf.toString();
    }
  
    protected class DynaBeanEntry implements Map.Entry<String,Object> {
        protected String key;
        protected DynaBeanEntry(String _key) {
            this.key = _key;
        }
        public String getKey() {
            return key;
        }
        public Object getValue() {
            return delegate.get(key);
        }
        public Object setValue(Object value) {
            Object old = delegate.get(key);
            delegate.set(key, value);
            return old;
        }       
    }
    
    /**
     * Iterates over all the objects in all the underlying maps in this composite.
     * NOTE: this implementation actually violates some of the java.util specification
     * because it will return the same key more than once if it exists in more than one
     * of the underlying maps. To strictly enfore the java.util spec, we must track
     * each returned key or entry and ignore it if it has already been returned.
     */
    protected abstract class DynaBeanIterator<E> implements Iterator<E> { 
        protected DynaProperty[] props = delegate.getDynaClass().getDynaProperties();
        protected int index;
        
        public DynaBeanIterator() {
        }
        public DynaProperty nextProp() {
            return props[index++];
        }
        public boolean hasNext() {
            return index < props.length;
        }
                        
        public void remove() {
            DynaBeanMap.this.remove(props[index-1].getName());
        }        
    }
    
    protected class DynaBeanKeysIterator extends DynaBeanIterator<String> {
        public String next() {
            return nextProp().getName();
        }       
    }
        
    protected class DynaBeanValuesIterator extends DynaBeanIterator<Object> {
        public Object next() {
            return delegate.get(nextProp().getName());
        }       
    }
    
    protected class DynaBeanEntriesIterator extends DynaBeanIterator<Map.Entry<String,Object>> {
        public Map.Entry<String,Object> next() {
            return new DynaBeanEntry(nextProp().getName());
        }
    }
    
    public Object remove(Object o) {
        Object old = delegate.get((String)o);
        delegate.set((String)o, null);
        return old;
    }
        
    public int size() {
        return delegate.getDynaClass().getDynaProperties().length;
    }
    
    public Set<String> keySet() {
        if(keySet == null) keySet = new DynaBeanKeySet();
        return keySet;
    }
    
    public Set<Map.Entry<String,Object>> entrySet() {
        if(entrySet == null) entrySet = new DynaBeanEntrySet();
        return entrySet;
    }
    
    public Collection<Object> values() {
        if(values == null) values = new DynaBeanValues();
        return values;
    }

    /* (non-Javadoc)
     * @see java.util.Map#isEmpty()
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /* (non-Javadoc)
     * @see java.util.Map#containsValue(java.lang.Object)
     */
    public boolean containsValue(Object value) {
        return values().contains(value);
    }
    
}
