/*
 * CompositeMap.java
 *
 * Created on March 18, 2004, 4:37 PM
 */

package simple.util;

import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 *
 * @author  Brian S. Krug
 */
public class CompositeMap<K,V> implements Map<K,V> {
	protected List<Map<K, V>> maps = new ArrayList<Map<K, V>>();
    protected Set<K> keySet;
    protected Set<Map.Entry<K, V>> entrySet;
    protected Collection<V> values;

    protected class CompositeKeySet extends AbstractSet<K> implements Set<K> {
        @Override
		public boolean contains(Object o) {
            return containsKey(o);
        }

        @Override
		public boolean isEmpty() {
            return CompositeMap.this.isEmpty();
        }

        @Override
		public Iterator<K> iterator() {
            return new CompositeKeysIterator();
        }

        @Override
		public int size() {
            return CompositeMap.this.size();
        }
    }

    protected class CompositeEntrySet extends AbstractSet<Map.Entry<K,V>> implements Set<Map.Entry<K,V>> {
        @Override
		public boolean contains(Object o) {
            return containsKey(o);
        }

        @Override
		public boolean isEmpty() {
            return CompositeMap.this.isEmpty();
        }

        @Override
		public Iterator<Map.Entry<K,V>> iterator() {
            return new CompositeEntriesIterator();
        }

        @Override
		public int size() {
            return CompositeMap.this.size();
        }

    }

    protected class CompositeValues extends AbstractCollection<V> implements Collection<V> {
        @Override
		public boolean contains(Object o) {
            return containsKey(o);
        }

        @Override
		public boolean isEmpty() {
            return CompositeMap.this.isEmpty();
        }

        @Override
		public Iterator<V> iterator() {
            return new CompositeValuesIterator();
        }

        @Override
		public int size() {
            return CompositeMap.this.size();
        }

    }

    /** Creates a new instance of CompositeMap */
    public CompositeMap() {
    }

    /** Creates a new instance of CompositeMap */
    public CompositeMap(Map<K,V>... containedMaps) {
        for(int i = 0; i < containedMaps.length; i++) merge(containedMaps[i]);
    }

    public V get(Object key) {
        for(Map<K,V> map : maps) {
			if(map != null && map.containsKey(key))
				return map.get(key);
        }
        return null;
    }

    public void merge(Map<K,V> map) {
        maps.add(map);
    }

	public void merge(int index, Map<K, V> map) {
		maps.add(index, map);
	}
    public void unmerge(Map<K,V> map) {
        for(Iterator<Map<K,V>> iter = maps.iterator(); iter.hasNext(); ) {
            Map<K,V> containedMap = iter.next();
            if(containedMap == map) iter.remove(); // only way to do this is to remove the EXACT same object as passed b/c if sets contain identical items they are "equal" via the equals() method
        }
    }

	public void unmerge(int index) {
		maps.remove(index);
	}

	public void mergeReplace(int index, Map<K, V> map) {
		maps.set(index, map);
	}

    public V put(K key, V value) {
        throw new UnsupportedOperationException("Put is unsupported; use merge() to merge another Map into this composite");
    }

    public void putAll(Map<? extends K, ? extends V> m) {
        throw new UnsupportedOperationException("PutAll is unsupported; use merge() to merge another Map into this composite");
    }

    public void clear() {
        throw new UnsupportedOperationException("Clear is unsupported; use unmerge() to remove a Map from this composite");
    }

    public boolean containsKey(Object key) {
        for(Iterator<?> iter = maps.iterator(); iter.hasNext(); ) {
            Map<?,?> map = (Map<?,?>)iter.next();
			if(map != null && map.containsKey(key))
				return true;
        }
        return false;
    }

    public boolean containsValue(Object key) {
        for(Iterator<?> iter = maps.iterator(); iter.hasNext(); ) {
            Map<?,?> map = (Map<?,?>)iter.next();
			if(map != null && map.containsValue(key))
				return true;
        }
        return false;
    }

    public boolean isEmpty() {
        boolean empty = true;
        for(Iterator<?> iter = maps.iterator(); iter.hasNext(); ) {
            Map<?,?> map = (Map<?,?>)iter.next();
			if(map != null)
				empty &= map.isEmpty();
        }
        return empty;
    }

    /**
    *  Provides a string representation of the entries within the map.
    **/
    @Override
	public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append('[');
        for(Iterator<?> iter = entrySet().iterator(); iter.hasNext();) {
            Map.Entry<?,?> entry = (Map.Entry<?,?>) iter.next();
            buf.append(entry.getKey());
            buf.append('=');
            buf.append(entry.getValue());
            if(iter.hasNext()) {
                buf.append(',');
            }
        }
        buf.append(']');

        return buf.toString();
    }

    /**
     * Iterates over all the objects in all the underlying maps in this composite.
     * Enforces unique elements for the Set interface, by tracking
     * each returned key and ignoring it if it has already been returned.
     */
    protected abstract class CompositeIterator<E> implements Iterator<E> {
        protected Iterator<Map<K,V>> mapsIter =  maps.iterator();
        protected Iterator<Map.Entry<K,V>> entryIter = null;
        protected Map.Entry<K,V> next;
        protected Set<K> returned = new HashSet<K>();

        public CompositeIterator() {
        	moveNext();
        }

        public boolean hasNext() {
    		return next != null;
        }

        protected boolean moveNext() {
        	do {
        		while(entryIter == null || !entryIter.hasNext()) {
        			if(!mapsIter.hasNext()) {
        				next = null;
        				return false;
        			} else {
						Map<K, V> map = mapsIter.next();
						if(map == null)
							continue;
						entryIter = map.entrySet().iterator();
        			}
        		}
        		next = entryIter.next();
        	} while(!accept(next.getKey())) ;
        	return true;
        }

        public E next() {
            if(!hasNext()) throw new NoSuchElementException();
            Map.Entry<K,V> entry = next;
    		moveNext();
    		return convertEntry(entry);
        }

        protected abstract E convertEntry(Map.Entry<K, V> entry) ;

		public void remove() {
            throw new UnsupportedOperationException("Remove is unsupported for the composite; modify the underlying map directly");
        }

		protected boolean accept(K key) {
			return returned.add(key);
		}
    }

    protected class CompositeKeysIterator extends CompositeIterator<K> {
		@Override
		protected K convertEntry(Map.Entry<K, V> entry) {
			return entry.getKey();
		}
     }

    protected class CompositeValuesIterator extends CompositeIterator<V> {
		@Override
		protected V convertEntry(Map.Entry<K, V> entry) {
			return entry.getValue();
		}
    }

    protected class CompositeEntriesIterator extends CompositeIterator<Map.Entry<K, V>> {
		@Override
		protected Map.Entry<K, V> convertEntry(Map.Entry<K, V> entry) {
			return entry;
		}
    }

    public V remove(Object o) {
        throw new UnsupportedOperationException("Remove is unsupported; use unmerge() to remove a Map from this composite");
    }

    public int size() {
        int size = 0;
        for(Iterator<?> iter = maps.iterator(); iter.hasNext(); ) {
            Map<?,?> map = (Map<?,?>)iter.next();
			if(map != null)
				size += map.size();
        }
        return size;
    }

    public Set<K> keySet() {
        if(keySet == null) keySet = new CompositeKeySet();
        return keySet;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if(entrySet == null) entrySet = new CompositeEntrySet();
        return entrySet;
    }

    public Collection<V> values() {
        if(values == null) values = new CompositeValues();
        return values;
    }

}
