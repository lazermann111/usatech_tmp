/**
 *
 */
package simple.util;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import simple.bean.ParsableToString;

/**
 * @author Brian S. Krug
 *
 */
public class OptimizedIntegerRangeSet extends AbstractSet<Integer> implements IntegerRangeSet, ParsableToString {
	protected final List<Integer> ranges = new ArrayList<Integer>();
	protected int size = 0;
	protected int modCount = 0;
	protected final char rangeChar;
	protected final char concatChar;
	public static final char DEFAULT_RANGE_CHAR = '-';
	public static final char DEFAULT_CONCAT_CHAR = '|';

	/**
	 *
	 */
	public OptimizedIntegerRangeSet() {
		this(DEFAULT_RANGE_CHAR, DEFAULT_CONCAT_CHAR);
	}

	public OptimizedIntegerRangeSet(char rangeChar, char concatChar) {
		this.rangeChar = rangeChar;
		this.concatChar = concatChar;
	}

	public int addRanges(String rangeString) throws NumberFormatException {
		if(rangeString == null || (rangeString=rangeString.trim()).length() == 0)
			return 0;
		int start = 0;
		int end;
		int n = 0;
		do {
			end = rangeString.indexOf(concatChar, start);
			if(end < 0)
				end = rangeString.length();
			String sub = rangeString.substring(start, end);
			int pos = sub.indexOf(rangeChar);
			if(pos < 0) {
				int e = Integer.parseInt(sub);
				n += addRange(e, e);
			} else {
				int min = Integer.parseInt(sub.substring(0, pos));
				int max = Integer.parseInt(sub.substring(pos+1));
				n += addRange(min, max);
			}
			start = end + 1;
		} while(end < rangeString.length()) ;
		return n;
	}
	public int addRange(int min, int max) {
		if(min > max)
			throw new IllegalArgumentException("Min is greater than max");
		if(max == Integer.MAX_VALUE)
			throw new IllegalArgumentException("Max value of " + Integer.MAX_VALUE + " is not allowed");
		int minIndex = Collections.binarySearch(ranges, min);
		int maxIndex = Collections.binarySearch(ranges, max + 1);
		int minInsert = insertionPoint(minIndex);
		int maxInsert = insertionPoint(maxIndex);
		int n = 0;

		if((minIndex % 2) == 0) { // min is already in
			if(minInsert == maxInsert) {
				// do nothing - all are already in
				return 0;
			} else {
				int diff = maxInsert - minInsert;
				//remove ranges within the new range
				int i = 0;
				int end = min;
				while(true) {
					if(i++ >= diff)
						break;
					end = ranges.remove(minInsert);
					if(i++ >= diff)
						break;
					int start = ranges.remove(minInsert);
					n += (start - end);
				}
				if((diff % 2) != 0) {
					ranges.add(minInsert, max + 1);
					n += (max - end + 1);
				}
			}
		} else if(minIndex > 0) { //min is at the end of a range - so extend the range, removing any ranges in the new range
			int diff = maxInsert - minInsert;
			//remove ranges within the new range
			int i = 0;
			int end = min;
			while(true) {
				if(i++ >= diff)
					break;
				end = ranges.remove(minInsert);
				if(i++ >= diff)
					break;
				int start = ranges.remove(minInsert);
				n += (start - end);
			}
			if((diff % 2) != 0) {
				ranges.add(minInsert, max + 1);
				n += (max - end + 1);
			}
		} else { // min is not in a current range
			ranges.add(minInsert, min);
			int diff = maxInsert - minInsert;
			//remove ranges within the new range
			int i = 0;
			int start = min;
			while(true) {
				if(i++ >= diff)
					break;
				int end = ranges.remove(minInsert+1);
				n += (end - start);
				if(i++ >= diff)
					break;
				start = ranges.remove(minInsert+1);
			}
			if((diff % 2) == 0) {
				ranges.add(minInsert + 1, max + 1);
				n += (max - start + 1);
			}
		}
		modCount++;
		size += n;
		return n;
	}
	protected int insertionPoint(int index) {
		if(index < 0)
			return -(index + 1);
		else
			return index;
	}
	/**
	 * @see java.util.Set#add(java.lang.Object)
	 */
	@Override
	public boolean add(Integer e) {
		return addRange(e, e) > 0;
	}

	/**
	 * @see java.util.Set#clear()
	 */
	@Override
	public void clear() {
		ranges.clear();
		size = 0;
		modCount++;
	}

	/**
	 * @see java.util.Set#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o) {
		if(o instanceof Integer) {
			int index = Collections.binarySearch(ranges, (Integer) o);
			return (index % 2) == 0;
		}
		return false;
	}

	/**
	 * @see java.util.Set#iterator()
	 */
	@Override
	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {
			protected int expectedModCount = modCount;
			protected int rangeIndex = 0;
			protected int rangeSeq = 0;
			protected boolean removeOkay = false;
			/**
			 * @see java.util.Iterator#hasNext()
			 */
			public boolean hasNext() {
				return (rangeIndex < ranges.size() / 2);
			}
			/**
			 * @see java.util.Iterator#next()
			 */
			public Integer next() {
				if(modCount != expectedModCount)
					throw new ConcurrentModificationException();
				if(rangeIndex * 2 >= ranges.size())
					throw new NoSuchElementException();
				int start = ranges.get(rangeIndex * 2);
				int end = ranges.get(rangeIndex * 2 + 1);
				int n = start + rangeSeq++;
				if(n >= end - 1) {
					rangeIndex++;
					rangeSeq = 0;
				}
				removeOkay = true;
				return n;
			}
			/**
			 * @see java.util.Iterator#remove()
			 */
			public void remove() {
				if(!removeOkay)
					throw new IllegalStateException("next() has not yet been called");
				if(rangeSeq == 0) {
					int end = ranges.get(rangeIndex * 2 - 1);
					int start = ranges.get(rangeIndex * 2 - 2);
					if(start == end - 1) {
						ranges.remove(rangeIndex * 2 - 2);
						ranges.remove(rangeIndex * 2 - 2);
						rangeIndex--;
					} else {
						ranges.set(rangeIndex * 2 - 1, end - 1);
					}
				} else if(rangeSeq == 1) {
					ranges.set(rangeIndex * 2, ranges.get(rangeIndex * 2) + 1);
					rangeSeq--;
				} else {
					int n = ranges.get(rangeIndex * 2) + rangeSeq - 1;
					ranges.add(rangeIndex * 2 + 1, n);
					ranges.add(rangeIndex * 2 + 2, n + 1);
					rangeIndex++;
					rangeSeq = 0;
				}
				expectedModCount++;
				modCount++;
				size--;
				removeOkay = false;
			}
		};
	}

	/**
	 * @see java.util.Set#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o) {
		if(o instanceof Integer) {
			int n = (Integer) o;
			int index = Collections.binarySearch(ranges, n);
			if((index % 2) == 0) { // set contains this integer so remove it
				if(index >= 0) {
					int end = ranges.get(index + 1);
					if(end == n + 1) {
						ranges.remove(index);
						ranges.remove(index);
					} else {
						ranges.set(index, n + 1);
					}
				} else {
					int insert = insertionPoint(index);
					int end = ranges.get(insert);
					if(end == n + 1) {
						ranges.set(insert, n);
					} else {
						ranges.add(insert, n);
						ranges.add(insert + 1, n + 1);
					}
				}
				modCount++;
				size--;
			}
		}
		return false;
	}

	/**
	 * @see java.util.Set#size()
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * @see java.util.AbstractCollection#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Iterator<Integer> iter = ranges.iterator(); iter.hasNext(); ) {
			if(sb.length() > 0)
				sb.append(concatChar);
			int min = iter.next();
			int max = iter.next();
			sb.append(min);
			if(min < max - 1)
				sb.append(rangeChar).append(max-1);
		}
		return sb.toString();
	}

	public char getRangeChar() {
		return rangeChar;
	}

	public char getConcatChar() {
		return concatChar;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(IntegerRangeSet o) {
		int i = size() - o.size();
		if(i != 0)
			return i;
		Iterator<Integer> iter0;
		Iterator<Integer> iter1;
		int factor;
		if(o instanceof OptimizedIntegerRangeSet) {
			List<Integer> oranges = ((OptimizedIntegerRangeSet)o).ranges;
			i = ranges.size() - oranges.size();
			if(i != 0)
				return i;
			iter0 = ranges.iterator();
			iter1 = oranges.iterator();
			factor = -1;
		} else {
			iter0 = iterator();
			iter1 = o.iterator();
			factor = 1;
		}
		int p = 1;
		while(iter0.hasNext()) {
			i = iter0.next() - iter1.next();
			if(i != 0)
				return i * p;
			p *= factor;
		}
		return 0;
	}
}
