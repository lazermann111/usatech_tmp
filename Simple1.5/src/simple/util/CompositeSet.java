/*
 * CompositeSet.java
 *
 * Created on March 22, 2004, 8:55 AM
 */

package simple.util;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 *
 * @author  Brian S. Krug
 */
public class CompositeSet<E> extends AbstractSet<E> implements Set<E> {
    protected List<Set<? extends E>> sets = new ArrayList<Set<? extends E>>();
    protected boolean strict = true; // whether to eliminate dups in iteration and count (size)
    /** Creates a new instance of CompositeSet */
    public CompositeSet() {
        super();
    }

    /** Creates a new instance of CompositeSet */
    public CompositeSet(Set<? extends E>... containedSets) {
        super();
        for(int i = 0; i < containedSets.length; i++) merge(containedSets[i]);
    }

    public void merge(Set<? extends E> set) {
        sets.add(set);
    }

    public void unmerge(Set<?> set) {
        for(Iterator<?> iter = sets.iterator(); iter.hasNext(); ) {
            Set<?> containedSet = (Set<?>)iter.next();
            if(containedSet == set) iter.remove(); // only way to do this is to remove the EXACT same object as passed b/c if sets contain identical items they are "equal" via the equals() method
        }
    }

    @Override
	public boolean add(E o) {
        throw new UnsupportedOperationException("Add is unsupported; use merge() to merge another set into this composite");
    }

    @Override
	public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("Add is unsupported; use merge() to merge another set into this composite");
    }

    @Override
	public void clear() {
        throw new UnsupportedOperationException("Clear is unsupported; use unmerge() to remove a set from this composite");
    }

    @Override
	public boolean contains(Object o) {
        for(Iterator<?> iter = sets.iterator(); iter.hasNext(); ) {
            Set<?> set = (Set<?>)iter.next();
            if(set.contains(o)) return true;
        }
        return false;
    }

    @Override
	public boolean isEmpty() {
        boolean empty = true;
        for(Iterator<?> iter = sets.iterator(); iter.hasNext(); ) {
            Set<?> set = (Set<?>)iter.next();
            empty &= set.isEmpty();
        }
        return empty;
    }

    protected class CompositeIterator implements Iterator<E> {
        protected final ListIterator<Set<? extends E>> setsIter =  sets.listIterator();
        protected Iterator<? extends E> entryIter;
        protected E nextEntry;
        protected boolean hasNext;

        protected CompositeIterator() {
        	entryIter = setsIter.next().iterator();
        	hasNext = true;
        	moveNext();
        }
        public boolean hasNext() {
            return hasNext;
        }

        public E next() {
        	if(!hasNext()) throw new java.util.NoSuchElementException();
            E next = nextEntry;
            moveNext();
            return next;
        }

        public void remove() {
            throw new UnsupportedOperationException("Remove is unsupported for the composite; modify the underlying set directly");
        }

        protected void moveNext() {
			while(hasNext) {
        		if(entryIter.hasNext()) {
        			nextEntry = entryIter.next();
        			//see if it is a dup
					if(!subSetContains(0, setsIter.previousIndex(), nextEntry))
        				break;
        		} else if(setsIter.hasNext()) {
        			entryIter = setsIter.next().iterator();
        		} else {
        			//done!
        			hasNext = false;
        			nextEntry = null;
        		}
        	}
        }
    }

    protected boolean subSetContains(int start, int end, Object object) {
    	for(int i = start; i < end; i++) {
			if(sets.get(i).contains(object))
				return true;
		}
    	return false;
    }
    @Override
	public Iterator<E> iterator() {
        return new CompositeIterator();
    }

    @Override
	public boolean remove(Object o) {
        throw new UnsupportedOperationException("Remove is unsupported; use unmerge() to remove a set from this composite");
    }

    @Override
	public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("RemoveAll is unsupported; use unmerge() to remove a set from this composite");
    }

    @Override
	public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("RetainAll is unsupported; use unmerge() to remove a set from this composite");
    }

    @Override
	public int size() {
        int size = 0;
        for(Iterator<?> iter = iterator(); iter.hasNext(); iter.next()) {
            size++;
        }
        return size;
    }

}
