/*
 * NameValuePair.java
 *
 * Created on December 10, 2001, 4:01 PM
 */

package simple.util;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class NameValuePair {

	/** Holds value of property name. */
	private String name;
	
	/** Holds value of property value. */
	private String value;
	
	/** Creates new NameValuePair */
    public NameValuePair() {
    }

    public NameValuePair(String name, String value) {
	    this.name = name;
	    this.value = value;
    }

    /** Getter for property name.
     * @return Value of property name.
     */
    public String getName() {
	    return name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     */
    public void setName(String name) {
	    this.name = name;
    }
    
    /** Getter for property value.
     * @return Value of property value.
     */
    public String getValue() {
	    return value;
    }
    
    /** Setter for property value.
     * @param value New value of property value.
     */
    public void setValue(String value) {
	    this.value = value;
    }
    
}
