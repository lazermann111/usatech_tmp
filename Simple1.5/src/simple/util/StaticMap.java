/*
 * Created on Nov 4, 2005
 *
 */
package simple.util;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class StaticMap<K,V> extends AbstractMap<K,V> {
    protected K[] keys;
    protected Map.Entry<K,V>[] entries;
    protected Comparator<? super K> comparator;
    protected Set<Map.Entry<K,V>> entrySet = new AbstractSet<Map.Entry<K,V>> () {
        @Override
        public Iterator<Entry<K, V>> iterator() {
            return new Iterator<Entry<K, V>>() {
                protected int index = 0;
                public boolean hasNext() {
                    return index < entries.length;
                }

                public Entry<K, V> next() {
                    return entries[index++];
                }

                public void remove() {
                    throw new UnsupportedOperationException("Map is not edittable");
                }
            };
        }

        @Override
        public int size() {
            return entries.length;
        }
    };
    protected class StaticMapEntry implements Map.Entry<K, V>, Comparable<StaticMapEntry> {
        protected K key;
        protected V value;
        protected StaticMapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
        public K getKey() {
            return key;
        }
        public V getValue() {
            return value;
        }
        public V setValue(V value) {
            V old = this.value;
            this.value = value;
            return old;
        }
        @Override
        public boolean equals(Object obj) {
            StaticMapEntry sme = (StaticMapEntry)obj;
            return compareTo(sme) == 0;
        }
        @Override
        public int hashCode() {
            return key == null ? 0 : key.hashCode();
        }
        public int compareTo(StaticMapEntry o) {
            if(key == null) return (o.key == null ? 0 : -1);
            if(o.key == null) return 1;
            if(comparator == null) return ((Comparable)key).compareTo(o.key);
            return comparator.compare(key, o.key);
        }
    }
    /** Creates an unmodifable Map backed by the given arrays. This static method is
     * used to ensure that elements are comparable.
     * 
     * @param <T>
     * @param elements
     * @return
     */
    public static <K1 extends Comparable,V1> StaticMap<K1, V1> create(K1[] keys, V1[] values) {
        return new StaticMap<K1,V1>(keys, values, null);
    }
    /** Creates an unmodifable Map backed by the given arrays. 
     * 
     * @param <T>
     * @param elements
     * @param comparator
     * @return
     */
    public static <K1 extends Comparable,V1> StaticMap<K1, V1> create(Comparator<? super K1> comparator, K1[] keys, V1[] values) {
        return new StaticMap<K1,V1>(keys, values, comparator);
    }
    
    protected StaticMap(K[] keys, V[] values, Comparator<? super K> comparator) {
        super();
        if(comparator == null && !Comparable.class.isAssignableFrom(keys.getClass().getComponentType()))
            throw new IllegalArgumentException("Keys must implement java.util.Comparable, unless a comparator is provided");
        entries = new Map.Entry[keys.length];
        for(int i = 0; i < keys.length; i++) {
            entries[i] = new StaticMapEntry(keys[i], values[i]);
        }
        this.comparator = comparator;
        Arrays.sort(entries);
        this.keys = keys.clone();
        Arrays.sort(this.keys, comparator);
    }

    @Override
    public Set<Map.Entry<K,V>> entrySet() {
        return entrySet;
    }

    public V get(Object key) {
        int index = Arrays.binarySearch(keys, (K)key, comparator);
        if(index < 0) return null;
        return entries[index].getValue();
    }
}
