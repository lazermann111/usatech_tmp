/*
 * Created on Dec 7, 2005
 *
 */
package simple.results;

public interface ResultsCreator {
    public Results createResults() throws ResultsCreationException ;
}
