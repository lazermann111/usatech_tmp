/*
 * Created on Sep 8, 2005
 *
 */
package simple.results;

import java.text.Format;

import simple.bean.ConvertUtils;
import simple.results.Results.Aggregate;
import simple.util.ArrayComparable;

public class MultiColumnResultsReader implements ResultsReader {
	protected final String[] displayColumns;
	protected final String[] sortColumns;
	protected final Format displayFormat;
	protected final String lastGroupedColumn;
	protected final Aggregate[] aggregates;
    public MultiColumnResultsReader(String[] displayColumns, String[] sortColumns, Format displayFormat) {
        this(displayColumns, sortColumns, displayFormat, null);
    }
    
    public MultiColumnResultsReader(String[] displayColumns, String[] sortColumns, Format displayFormat,Aggregate[] aggregates) {
        super();
        this.displayColumns = displayColumns;
        this.sortColumns = sortColumns;
        this.displayFormat = displayFormat;
		if(displayColumns.length > 0)
			this.lastGroupedColumn = displayColumns[displayColumns.length - 1];
		else if(sortColumns.length > 0)
			this.lastGroupedColumn = sortColumns[sortColumns.length - 1];
		else
			this.lastGroupedColumn = null;
        this.aggregates=aggregates;
    }

    public String getDisplayValue(Results results) {
        Object[] o = new Object[displayColumns.length];
        for(int i = 0; i < displayColumns.length; i++){
        	if(aggregates[i]==null){
        		o[i] = results.getValue(displayColumns[i]);
        	}else{
        		String[] gns = results.getGroupNames();
        		String lastGroup;
        		if(gns == null || gns.length == 0)
        			lastGroup = null;
        		else
        			lastGroup = gns[gns.length-1];
        		o[i] = results.getAggregate(displayColumns[i], lastGroup, aggregates[i]);
        	}
        }
        return ConvertUtils.formatObject(o, displayFormat);
    }

    public Comparable<?> getSortValue(Results results) {
        Comparable<?>[] o = new Comparable[sortColumns.length];
        for(int i = 0; i < sortColumns.length; i++){
        	if(aggregates[i]==null){
        		o[i] = (Comparable<?>)results.getValue(sortColumns[i]);
        	}else{
        		String[] gns = results.getGroupNames();
        		String lastGroup;
        		if(gns == null || gns.length == 0)
        			lastGroup = null;
        		else
        			lastGroup = gns[gns.length-1];
        		o[i] = (Comparable<?>)results.getAggregate(sortColumns[i], lastGroup, aggregates[i]);
        	}
        }
        return new ArrayComparable(o);
    }

    public boolean isGroupBeginning(Results results) {
        return results.isGroupBeginning(lastGroupedColumn);
    }

    public boolean isGroupEnding(Results results) {
        return results.isGroupEnding(lastGroupedColumn);
    }

    public void addGroups(Results results) {
        for(String sc : sortColumns) results.addGroup(sc);
        for(String dc : displayColumns) results.addGroup(dc);        
    }
    
    public void trackAggregates(Results results) {
    	for(int i = 0; i < displayColumns.length; i++) {
    		if(aggregates[i]!=null){
    			results.trackAggregate(displayColumns[i], aggregates[i]);
    		}
        }
    	for(int i = 0; i < sortColumns.length; i++) {
    		if(aggregates[i]!=null){
    			results.trackAggregate(sortColumns[i], aggregates[i]);
    		}
        }
	}

	public Aggregate[] getAggregates() {
		return aggregates;
	}
}
