/*
 * ResultsIterator.java
 *
 * Created on January 17, 2003, 2:01 PM
 */

package simple.results;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class ResultsIterator implements Iterator<Map<String,Object>> {
    protected Results results;
    protected String[] columnNames;
    
    /** Creates new ResultsIterator */
    public ResultsIterator(Results results, int index) {
        this.results = results;
        results.setRow(index);
        columnNames = new String[results.getColumnCount()];
        for(int i = 0; i < columnNames.length; i++) columnNames[i] = results.getColumnName(i+1);
    }

    public boolean hasNext() {
        return !results.isGroupEnding(0);
    }
    
    public Map<String,Object> next() {
        results.next();
        Map<String,Object> m = new LinkedHashMap<String,Object>();
        for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i], results.getValue(columnNames[i]));
        return m;
    }
    
    public void remove() {
        throw new UnsupportedOperationException();
    }
    
}
