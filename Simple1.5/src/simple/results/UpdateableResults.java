package simple.results;

import java.sql.SQLException;

import simple.bean.ConvertException;

public interface UpdateableResults extends Results {
	public void newRow() throws SQLException;
	public void deleteRow() throws SQLException;
	public void updateRow() throws SQLException;
	public void refreshRow() throws SQLException;
	public void setValue(int column, Object value) throws SQLException, ConvertException;
	public void setValue(String columnName, Object value) throws SQLException, ConvertException;
	
}
