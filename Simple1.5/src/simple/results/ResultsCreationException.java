/*
 * Created on Dec 6, 2005
 *
 */
package simple.results;

public class ResultsCreationException extends Exception {
    private static final long serialVersionUID = -1234123490234L;

    public ResultsCreationException() {
        super();
    }

    public ResultsCreationException(String message) {
        super(message);
    }

    public ResultsCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResultsCreationException(Throwable cause) {
        super(cause);
    }

}
