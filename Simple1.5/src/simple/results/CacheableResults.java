/*
 * Created on Dec 6, 2005
 *
 */
package simple.results;

public interface CacheableResults extends Results {
    public ResultsCreator getCachedResultsCreator() throws CachingException;
}
