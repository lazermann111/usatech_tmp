/*
 * ScrollableResults.java
 *
 * Created on January 24, 2003, 12:02 PM
 */

package simple.results;

import java.util.List;
import java.util.Map;

/**
 * This interface defines an object that implements a Results object that allows
 * random access and also presents its data as an unmodifiable List of Maps 
 * where each "row" is a Map in the List. And each "column" is an element in 
 * the Map with the column name as the key.
 *
 * @author  Brian S. Krug
 * @version 
 */
public interface ScrollableResults extends Results, List<Map<String,Object>> {
    public ScrollableResults subResults(int fromRow, int toRow) ;    
    public ScrollableResults subResults(int groupColumn) ;
    public ScrollableResults subResults(String groupColumnName) ;
}

