/*
 * BeanException.java
 *
 * Created on December 30, 2002, 2:16 PM
 */

package simple.results;

/** An exception to indicate a that an object could not be created or an exception
 *  occurred while setting a property on a bean. The underlying
 *  exception can be accessed by getRootCause().
 *
 * @author  Brian S. Krug
 * @version 
 */
public class BeanException extends Exception {
    private static final long serialVersionUID = 8231434150L;
    /**
     * Creates new <code>BeanCreationException</code> without detail message.
     */
    public BeanException(Exception rootCause) {
        super(rootCause);
    }

    /**
     * Creates new <code>BeanCreationException</code> without detail message.
     */
    public BeanException(String msg, Exception rootCause) {
        super(msg, rootCause);
    }


    /**
     * Constructs an <code>BeanCreationException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public BeanException(String msg) {
        super(msg);
    }    
}


