/*
 * Created on Dec 7, 2005
 *
 */
package simple.results;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

import simple.db.Column;
import simple.io.Log;

public class CachedResultsCreator implements ResultsCreator {
    private static final Log log = Log.getLog();
    protected File cacheFile;
    protected Column[] columns;
    protected int compression;
    protected int rows = -1;
    
    public CachedResultsCreator(Column[] columns, File cacheFile, int compression) {
        super();
        this.columns = columns;
        this.cacheFile = cacheFile;
        this.compression = compression;
    }

    public Results createResults() throws ResultsCreationException {
        try {
            log.debug("Creating CachedForwardOnlyResults with File '" + cacheFile.getPath() + "' of " + cacheFile.length() + " bytes and " + getRows() + " rows");
            return new CachedForwardOnlyResults(columns, cacheFile, getRows(), compression);
        } catch(FileNotFoundException e) {
            throw new ResultsCreationException(e);
        } catch(IOException e) {
            throw new ResultsCreationException(e);
        }
    }
    
    public int getRows() {
        return rows;
    }

    /**
     * This class is simply a marker that we've reached the last row
     */
    public static final class EOFMarker implements Serializable {
        private static final long serialVersionUID = -1L;
        public EOFMarker() {}
    }   
}
