/*
 * KeyValueMap.java
 *
 * Created on January 23, 2003, 1:37 PM
 */

package simple.results;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/** This implementation of the Map interface expects a Map of names to indices
 *  and an array of values. The Map of names is used to provide the keySet for
 *  this Map and the value of a Map entry is retrieved by getting the value of
 *  that same key in the names Map and turning that value into an int which is
 *  the index in the values array for that key. This class was designed
 *  specifically to be used by ListArrayResults.
 *
 * @author  Brian S. Krug
 * @version
 */
public class KeyArrayMap extends AbstractMap<String, Object> {
    protected Map<String, Integer> keys;
    protected Object[] values;
    protected Set<Map.Entry<String, Object>> entrySet;
    protected Set<String> keySet;
    
    public KeyArrayMap(Map<String, Integer> keys, Object[] values) {
        this.keys = keys;
        this.values = values;
    }
    
    public Set<Map.Entry<String, Object>> entrySet() {
        if(entrySet == null) {
            entrySet = new AbstractSet<Map.Entry<String, Object>>() {
                public Iterator<Map.Entry<String, Object>> iterator() {
                    return new Iterator<Map.Entry<String, Object>>() {
                        protected Iterator<String> keyIterator = keySet().iterator();
                        public boolean hasNext() { return keyIterator.hasNext(); }                        
                        public Map.Entry<String, Object> next() {
                            return new Map.Entry<String, Object>() {
                                protected String key = keyIterator.next();
                                protected Object value = get(key);
                                public String getKey() { return  key; }
                                public Object getValue() { return value; }
                                public Object setValue(Object obj) { throw new UnsupportedOperationException(); }
                                public int hashCode() {
                                    return (key == null ? 0 : key.hashCode()) + (value == null ? 0 : value.hashCode());
                                }
                                public boolean equals(Object obj) {
                                    return (obj instanceof Map.Entry<?,?>
                                    && areEqual(key, ((Map.Entry<?,?>)obj).getKey())
                                    && areEqual(value, ((Map.Entry<?,?>)obj).getValue()));
                                }
                            };
                        }                        
                        public void remove() { throw new UnsupportedOperationException(); }
                    };
                }
                public int size() { return keySet().size(); }
            };
        }
        return entrySet;
    }
    
    public Object put(String key, Object value) {
        throw new UnsupportedOperationException();
    }
    
    public Object remove(Object key) {
        throw new UnsupportedOperationException();
    }
    
    public Set<String> keySet() {
        return keys.keySet();
    }
    
    public void clear() {
        throw new UnsupportedOperationException();
    }
    
    public Collection<Object> values() {
        return Arrays.asList(values);
    }
    
    public boolean containsKey(Object obj) {
        return keys.containsKey(obj);
    }
    
    public int size() {
        return keys.size();
    }
    
    public boolean containsValue(Object obj) {
        for(int i = 0; i < values.length;i++) {
            if(areEqual(obj,get(i))) return true;
        }
        return false;
    }
    
    public void putAll(Map<? extends String,? extends Object> map) {
        throw new UnsupportedOperationException();
    }
    
    public boolean isEmpty() {
        return size() == 0;
    }
    
    public Object get(Object obj) {
        Object colObj = keys.get(obj);
        if(colObj instanceof Number) {
            int col = ((Number)colObj).intValue();
            if(col >= 0 && col < values.length) return get(col);
        }
        return null;
    }
    
    public Object get(int index) {
        return values[index];
    }
    
    protected boolean areEqual(Object o1, Object o2) {
        if(o1 == null) return (o2 == null);
        try {
            return o1.equals(o2);
        } catch(ClassCastException e) {
            return false;
        }
    }
    
    public Object[] getValues() {
        return values;
    }
}
