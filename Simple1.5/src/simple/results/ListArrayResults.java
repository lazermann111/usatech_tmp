/*
 * ListMapResults.java
 *
 * Created on January 24, 2003, 11:47 AM
 */

package simple.results;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import simple.bean.ConvertException;
import simple.db.Column;
import simple.db.DBHelper;
import simple.io.Log;

/** Implementation of IndexScrollableResults that uses a List of arrays as the
 *  data and an array of Strings as the column names. Caches KeyArrayMap for each
 *  row when requested for the List interface of ScrollableResults.
 *
 * @author  Brian S. Krug
 * @version
 */
public class ListArrayResults extends IndexScrollableResults {
    private static final Log log = Log.getLog();
    protected List<Object[]> rows;
    protected Map<String, Object>[] maps;
    protected int offset;
    protected int length;

    public ListArrayResults(Column[] columns, ResultSet rs, DBHelper dbHelper) throws SQLException, ConvertException {
        this(columns, readResultSet(columns, rs, dbHelper));
    }

    public ListArrayResults(Column[] columns, List<Object[]> data) {
        this(columns, data, 0, data.size());
    }

    @SuppressWarnings("unchecked")
	protected ListArrayResults(Column[] columns, List<Object[]> data, int offset, int length) {
        super(columns);
        this.rows = data;
        this.maps = new Map[rows.size()];
        this.offset = offset;
        this.length = length;
    }

    protected static List<Object[]> readResultSet(Column[] columns, ResultSet rs, DBHelper dbHelper) throws SQLException, ConvertException {
        List<Object[]> data = new ArrayList<Object[]>();
        try {
            while(rs.next()) {
                Object[] values = dbHelper.getValues(rs, columns, false);
                data.add(values);
            }
        } catch(SQLException sqle) {
            log.info("Exception while reading row #" + rs.getRow());
            throw sqle;
        } finally {
			try {
				rs.close();
			} catch(SQLException e) {
			}
        }
        return data;
    }

    public Map<String, Object> get(int index) {
        index += offset;
        if(maps[index] == null)
            maps[index] = new KeyArrayMap(columnNamesMap, rows.get(index));
        return maps[index];
    }

    public int size() {
        return length;
    }

    protected Object[] getValuesAtRow(int row) throws IndexOutOfBoundsException {
        return rows.get((row-1) + offset);
    }

    public boolean isValidRow(int row) {
        return row > 0 && row <= size();
    }

    public ScrollableResults subResults(int fromRow, int toRow) {
        ListArrayResults lar = (ListArrayResults)this.clone();
        lar.offset = fromRow - 1;
        lar.length = toRow - fromRow;
        lar.currentRow = 0;
        lar.groupValues = null;
        return lar;
    }
	
    public void close() {
        super.close();
        rows = null; //allow gc of data (list of arrays)
        maps = null;
    }

}
