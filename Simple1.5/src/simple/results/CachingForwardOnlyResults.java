/*
 * Created on Dec 6, 2005
 *
 */
package simple.results;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import simple.db.Column;
import simple.db.DBHelper;
import simple.io.CacheFile;
import simple.io.Log;
import simple.results.CachedResultsCreator.EOFMarker;
import simple.security.SecurityUtils;

public class CachingForwardOnlyResults extends ForwardOnlyResults implements CacheableResults {
    private static final Log log = Log.getLog();
    protected ObjectOutputStream cachingStream;
    protected ObjectInputStream cachedStream;
    protected Exception exception;
    protected File cacheFile;
    protected transient ResultsCreator resultsCreator;
    protected final BlockingQueue<Object[]> queue = new LinkedBlockingQueue<Object[]>(); //Using a blocking queue yields about the same time, but WAY less CPU
    protected final static Object[] DONE = new Object[0];
    protected final Lock cacheLock = new ReentrantLock();
    protected final Condition cacheSignal = cacheLock.newCondition();
    protected int rows = -1;
    protected int compression = 1;

    public CachingForwardOnlyResults(Column[] columns, ResultSet rs, DBHelper dbHelper) throws FileNotFoundException, IOException {
        this(columns, rs, dbHelper, new CacheFile(File.createTempFile("cached-results-" + SecurityUtils.getRandomAlphaNumericString(16) + "-", ".dat").getPath()));
    }

    public CachingForwardOnlyResults(Column[] columns, ResultSet rs, DBHelper dbHelper, File cacheFile) throws FileNotFoundException, IOException {
        super(columns, rs, dbHelper);
        this.cacheFile = cacheFile;
        // Performance analysis indicates that the optimal buffer sizes are 512 (the default) for the Deflater and 1024 for the BufferedOutputStream
		OutputStream os = new FileOutputStream(cacheFile);
        if(compression > 0)
            os = new DeflaterOutputStream(os, new Deflater(compression), 512);
        this.cachingStream = new ObjectOutputStream(new BufferedOutputStream(os, 1024));
        startCachingThread();
    }

    @Override
    protected Object[] readNextRow() {
		if(cachedStream == null)
    		return readNextRowCaching();
    	else
    		return readNextRowCached();
    }

    protected Object[] readNextRowCaching() {
		Object[] data = super.readNextRow(false);
        if(data == null) { // we are done
        	queue.offer(DONE);
        } else
        	queue.offer(data);
        return data;
    }

    @Override
    protected void resetRows() {
    	try {
			awaitCachingStream();
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
    	closeCachedStream();
		try {
			cachedStream = CachedForwardOnlyResults.createCachedStream(cacheFile, compression);
		} catch(IOException e) {
			throw new UnsupportedOperationException("Could not read from file '" + cacheFile.getAbsolutePath() + "' to reset rows for Results object");
		}
    }

    protected Object[] readNextRowCached() {
        try {
            if(lastReadRow >= rows) return null;
            Object o = cachedStream.readUnshared();
            try {
                Object[] row = (Object[]) o;
                if(lastReadRow - 1 == rows) {
                    log.debug("Reached end of file");
                    closeCachedStream();
                    atEnd = true;
                }
                return row;
            } catch(ClassCastException e) {
                if(o instanceof EOFMarker) {
                    log.debug("Read end of file marker at row " + lastReadRow + " while preset size = " + rows);
                    closeCachedStream();
                    atEnd = true;
                    return null;
                } else
                    throw e;
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        } catch(ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
	protected void closeCachedStream() {
		if(cachedStream != null)
			try {
				cachedStream.close();
			} catch(IOException e) {
				log.warn("Could not close input stream", e);
			}
    	cachedStream = null;
	}
    public ResultsCreator getCachedResultsCreator() throws CachingException {
        if(resultsCreator == null) {
            log.debug("Creating creator");
            resultsCreator = new CachedResultsCreator(columns, cacheFile, compression) {
                @Override
                public Results createResults() throws ResultsCreationException {
                	try {
						awaitCachingStream();
					} catch(Exception e) {
						throw new ResultsCreationException("While writing to file", e);
					}
                    return super.createResults();
                }

                @Override
                public int getRows() {
                    return lastReadRow;
                }
            };
        }
        return resultsCreator;
    }

    protected void startCachingThread() {
		Thread thread = new Thread("CachingThread-" + cacheLock.hashCode()) {
            @Override
            public void run() {
                try {
                    for(int i = 0; true; i++) {
                        Object[] data;
						try {
							data = queue.take();
						} catch(InterruptedException e1) {
							continue;
						}
                        if(data == DONE) {
                        	log.debug("Wrote " + i + " rows to cache file");
                            try {
                                cachingStream.writeUnshared(new CachedResultsCreator.EOFMarker()); // mark the end
                            } catch(IOException e) {
                                exception = e;
                                return;
                            }
                            break;
                        } else if(data != null) {
                        	Thread.yield();
                            try {
                                cachingStream.writeUnshared(data);
                            } catch(IOException e) {
                                exception = e;
                                break;
                            }
                        } else {
                        	Thread.yield();
                        }
                    }
                } catch(RuntimeException e) {
                    log.warn("While Writing to file", e);
                    exception = e;
                } finally {
                    Thread.yield();
                    log.debug("Flushing and closing cache stream");
                    try {
                        cachingStream.flush();
                        cachingStream.close();
                    } catch(IOException e) {
                        exception = e;
                    }
                    log.debug("Signalling cacheSignal");
                    cacheLock.lock();
                    try {
						rows = lastReadRow;
                        cachingStream = null;
                        cacheSignal.signalAll();
                    } finally {
                    	cacheLock.unlock();
                    }
                    log.debug("Exiting Run method");
                }
            }
        };
        thread.setDaemon(true);
        thread.setPriority(3);
        thread.start();
    }

    protected void awaitCachingStream() throws Exception {
    	if(rows == -1)
        	readAll();
        cacheLock.lockInterruptibly();
        try {
            if(cachingStream != null) {
                log.debug("Awaiting cache signal");
                cacheSignal.await();
            }
        } finally {
        	cacheLock.unlock();
        }
        if(exception != null)
            throw exception;
    }
    @Override
    public void close() {
        // ensure that we have read this all the way
        readAll();
        super.close();
        closeCachedStream();
        this.cacheFile = null;
    }

}
