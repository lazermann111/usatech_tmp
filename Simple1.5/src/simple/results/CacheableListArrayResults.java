/*
 * Created on Dec 9, 2005
 *
 */
package simple.results;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import simple.bean.ConvertException;
import simple.db.Column;
import simple.db.DBHelper;
import simple.io.CacheFile;
import simple.security.SecurityUtils;

public class CacheableListArrayResults extends ListArrayResults implements CacheableResults {
    protected int compression = 1;
    protected transient ResultsCreator resultsCreator;

    public CacheableListArrayResults(Column[] columns, ResultSet rs, DBHelper dbHelper) throws SQLException, ConvertException {
        super(columns, rs, dbHelper);
    }

    public ResultsCreator getCachedResultsCreator() throws CachingException {
        if(resultsCreator == null) {
            try {
                File file = new CacheFile(File.createTempFile("cached-results-" + SecurityUtils.getRandomAlphaNumericString(16) + "-", ".dat").getPath());
                resultsCreator = new LazyCachedResultsCreator(rows, columns, file, compression);
            } catch(IOException e) {
                throw new CachingException("While creating cache file", e);
            }
        }
        return resultsCreator;
    }
}
