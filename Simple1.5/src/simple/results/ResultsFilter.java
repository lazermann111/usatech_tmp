package simple.results;

public interface ResultsFilter {
	public boolean isValidRow(int row, Object[] data, Results results) ;
}
