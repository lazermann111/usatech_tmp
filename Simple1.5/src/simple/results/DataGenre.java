/*
 * Created on Sep 30, 2005
 *
 */
package simple.results;

public enum DataGenre {
    STRING, NUMBER, DATE, ARRAY, OTHER
}
