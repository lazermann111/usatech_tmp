/*
 * Created on Sep 8, 2005
 *
 */
package simple.results;

import simple.results.Results.Aggregate;


public interface ResultsReader {
    public String getDisplayValue(Results results);
    public Comparable<?> getSortValue(Results results);
    public boolean isGroupBeginning(Results results);
    public boolean isGroupEnding(Results results);
    public void addGroups(Results results);
    public void trackAggregates(Results results);
	public Aggregate[] getAggregates();
}