package simple.results;

import java.util.List;
import java.util.Map;

import simple.db.Column;
import simple.util.CollectionUtils;

public abstract class ListResults<R> extends IndexScrollableResults {
	protected Map<String, Object>[] maps;
	protected int offset;
	protected int length;
	protected final List<R> rows;

	public ListResults(Column[] columns, List<R> rows) {
		this(columns, rows, 0, rows.size());
	}

	public ListResults(Column[] columns, List<R> rows, int offset, int length) {
		super(columns);
		this.rows = rows;
		this.length = length;
		this.offset = offset;
	}

	@Override
	public ScrollableResults subResults(int fromRow, int toRow) {
		ListResults<R> sub = (ListResults<R>) this.clone();
		sub.offset = fromRow - 1;
		sub.length = toRow - fromRow;
		sub.currentRow = 0;
		sub.groupValues = null;
		return sub;
	}

	@Override
	public boolean isValidRow(int row) {
		return row > 0 && row <= size();
	}

	@Override
	public int size() {
		return length;
	}

	@Override
	protected Object[] getValuesAtRow(int row) throws IndexOutOfBoundsException {
		return toValueArray(rows.get((row - 1) + offset));
	}

	@Override
	public Map<String, Object> get(int index) {
		index += offset;
		if(maps == null)
			maps = CollectionUtils.genericize(new Map[length]);
		if(maps[index] == null)
			maps[index] = toValueMap(rows.get(index));
		return maps[index];
	}

	protected abstract Object[] toValueArray(R rowData);

	protected abstract Map<String, Object> toValueMap(R rowData);
}
