/*
 * Created on Oct 24, 2005
 *
 */
package simple.results;

import java.sql.ResultSet;
import java.sql.SQLException;

import simple.bean.ConvertException;
import simple.db.Column;
import simple.db.DBHelper;
import simple.io.Log;

public class ForwardOnlyResults extends AccumulatingResults {
    private static final Log log = Log.getLog();
    protected ResultSet rs;
    protected DBHelper dbHelper;
    public ForwardOnlyResults(Column[] columns, ResultSet rs, DBHelper dbHelper) {
        super(columns);
        this.rs = rs;
        this.dbHelper = dbHelper;
    }

    @Override
    protected Object[] readNextRow() {
		return readNextRow(true);
	}

	protected Object[] readNextRow(boolean connected) {
        try {
            if(rs.next()) {
				return dbHelper.getValues(rs, columns, connected);
            }
        } catch(SQLException e) {
            throw new IllegalStateException("Could not advance ResultSet " + rs, e);
        } catch(ConvertException e) {
        	throw new IllegalArgumentException("Could not convert column", e);
		}
        closeResultSet();
        return null;
    }

    protected void closeResultSet() {
    	if(rs != null) {
    		try {
				rs.getStatement().close();
	        } catch(SQLException e) {
	            log.debug("Could not close resultset; continuing anyway", e);
	        } finally {
	        	rs = null;
	        }
    	}
    }
    @Override
    public void close() {
    	closeResultSet();
    	dbHelper = null;
        super.close();
    }
}
