/*
 * Created on Sep 8, 2005
 *
 */
package simple.results;

import java.text.Format;

import simple.bean.ConvertUtils;
import simple.results.Results.Aggregate;

public class SingleColumnResultsReader implements ResultsReader {
	protected final String displayColumn;
	protected final String sortColumn;
	protected final Format displayFormat;
	protected final Aggregate aggregate;
	protected final Aggregate[] aggregates;
    public SingleColumnResultsReader(String displayColumn, String sortColumn, Format displayFormat) {
        this(displayColumn, sortColumn, displayFormat, null);
    }
    
    public SingleColumnResultsReader(String displayColumn, String sortColumn, Format displayFormat, Aggregate aggregate) {
        super();
        this.displayColumn = displayColumn;
        this.sortColumn = sortColumn;
        this.displayFormat = displayFormat;
        this.aggregate=aggregate;
		this.aggregates = new Aggregate[] { aggregate };
    }

    public String getDisplayValue(Results results) {
    	if(aggregate==null){
    		return ConvertUtils.formatObject(results.getValue(displayColumn), displayFormat);
    	}else{
    		String[] gns = results.getGroupNames();
    		String lastGroup;
    		if(gns == null || gns.length == 0)
    			lastGroup = null;
    		else
    			lastGroup = gns[gns.length-1];
    		return ConvertUtils.formatObject(results.getAggregate(displayColumn, lastGroup, aggregate), displayFormat);
    	}
    }

    public Comparable<?> getSortValue(Results results) {
    	if(aggregate==null){
    		return (Comparable<?>)results.getValue(sortColumn);
    	}else{
    		String[] gns = results.getGroupNames();
    		String lastGroup;
    		if(gns == null || gns.length == 0)
    			lastGroup = null;
    		else
    			lastGroup = gns[gns.length-1];
    		return (Comparable<?>)results.getAggregate(sortColumn, lastGroup, aggregate);
    	}
    }
    public void trackAggregates(Results results) {
    	if(aggregate != null){
			results.trackAggregate(displayColumn, aggregate);
			results.trackAggregate(sortColumn, aggregate);
    	}
	}
    public boolean isGroupBeginning(Results results) {
        return results.isGroupBeginning(displayColumn);
    }

    public boolean isGroupEnding(Results results) {
        return results.isGroupEnding(displayColumn);
    }

    public void addGroups(Results results) {
        results.addGroup(sortColumn);
        results.addGroup(displayColumn);   
    }

	public Aggregate[] getAggregates() {
		return aggregates;
	}
}
