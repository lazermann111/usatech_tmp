/*
 * AbstractResults.java
 *
 * Created on October 11, 2001, 9:56 AM
 */

package simple.results;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.Collection;
import java.util.Locale;

import org.apache.commons.beanutils.DynaProperty;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;

/** Provides a default implementation for some of Results' methods. It
 *  uses the abstract method getColumnIndex to implement each method
 *  that takes a String as the Column Name to use its column index pair.
 *  For example, getValue(String) is defined in terms of getValue(int).
 *
 * @author  Brian S. Krug
 * @version
 */
public abstract class AbstractResults extends simple.bean.AbstractDynaBean implements Results {
    protected DynaProperty[] dynaProperties;
    protected String[] formats;
	protected Locale locale;
	protected int[] uniqueColumnIndexes;

    /** Creates new AbstractResults */
    public AbstractResults() {}

    public Class<?> getColumnClass(String columnName) {
        return getColumnClass(getColumnIndex(columnName));
    }

    public Object getValue(String columnName) {
        return getValue(getColumnIndex(columnName));
    }

    public <T> T getValue(int column, Class<T> type) throws ConvertException {
        return ConvertUtils.convert(type, getValue(column));
    }

    public <T> T getValue(String columnName, Class<T> type) throws ConvertException {
        return getValue(getColumnIndex(columnName), type);
    }

    public Object getAggregate(String columnName, String groupColumnName, Aggregate aggregateType) {
        return getAggregate(getColumnIndex(columnName), getColumnIndex(groupColumnName), aggregateType);
    }

    public <T> T getAggregate(int column, int groupColumn, Aggregate aggregateType, Class<T> type) throws ConvertException {
        return ConvertUtils.convert(type, getAggregate(column, groupColumn, aggregateType));
    }

    public <T> T getAggregate(String columnName, String groupColumnName, Aggregate aggregateType, Class<T> type) throws ConvertException {
        return getAggregate(getColumnIndex(columnName), getColumnIndex(groupColumnName), aggregateType, type);
    }

    public Object getAggregate(String columnName, String groupColumnName, ResultsFilter filter, Aggregate aggregateType) {
        return getAggregate(getColumnIndex(columnName), getColumnIndex(groupColumnName), filter, aggregateType);
    }

    public Object getAggregate(int column, int groupColumn, Aggregate aggregateType) {
        return getAggregate(column, groupColumn, null, aggregateType);
    }

    public <T> T getAggregate(int column, int groupColumn, ResultsFilter filter, Aggregate aggregateType, Class<T> type) throws ConvertException {
        return ConvertUtils.convert(type, getAggregate(column, groupColumn, filter, aggregateType));
    }

    public <T> T getAggregate(String columnName, String groupColumnName, ResultsFilter filter, Aggregate aggregateType, Class<T> type) throws ConvertException {
        return getAggregate(getColumnIndex(columnName), getColumnIndex(groupColumnName), filter, aggregateType, type);
    }

    public void trackAggregate(String columnName) {
        trackAggregate(getColumnIndex(columnName));
    }

    public void trackAggregate(String columnName, Aggregate... aggregates) {
        trackAggregate(getColumnIndex(columnName), aggregates);
    }

    public void trackAggregate(int column, Aggregate... aggregates) {
        trackAggregate(column, null, aggregates);
    }

    public void trackAggregate(String columnName, ResultsFilter filter, Aggregate... aggregates) {
        trackAggregate(getColumnIndex(columnName), filter, aggregates);
    }

	public void trackAggregate(int column, ResultsFilter filter, Aggregate... aggregates) {
		rawTrackAggregate(column, filter, aggregates);
		int uci = getUniqueColumnIndexFor(column);
		if(uci > 0)
			rawTrackAggregate(uci, filter, Aggregate.COUNT, Aggregate.DISTINCT);
	}

	protected abstract void rawTrackAggregate(int column, ResultsFilter filter, Aggregate... aggregates);

    public Aggregate[] getAggregatesTracked(String columnName) {
        return getAggregatesTracked(getColumnIndex(columnName));
    }

    public void addGroup(String groupColumnName) {
        addGroup(getColumnIndex(groupColumnName));
    }
    public String[] getGroupNames() {
        int[] groups = getGroups();
        String[] groupNames = new String[groups.length];
        for(int i = 0; i < groups.length; i++)
            groupNames[i] = getColumnName(groups[i]);
        return groupNames;
    }
    public boolean isGroupEnding(String groupColumnName) {
        return isGroupEnding(getColumnIndex(groupColumnName));
    }

    public boolean isGroupBeginning(String groupColumnName) {
        return isGroupBeginning(getColumnIndex(groupColumnName));
    }

    public Object[] getGroupValues(String groupColumnName) {
        return getGroupValues(getColumnIndex(groupColumnName));
    }

    public int getGroupings(String columnName, String groupColumnName) {
        return getGroupings(getColumnIndex(columnName), getColumnIndex(groupColumnName));
    }

    public Results nextSubResult(String groupColumnName) {
        return nextSubResult(getColumnIndex(groupColumnName));
    }

    public java.math.BigDecimal getPercent(String valueColumnName, String wholeColumnName, Aggregate aggregateType) {
        return getPercent(getColumnIndex(valueColumnName), getColumnIndex(wholeColumnName), aggregateType);
    }

    public java.math.BigDecimal getPercent(String valueColumnName, String valueGroupColumnName, String wholeColumnName, Aggregate aggregateType) {
        return getPercent(getColumnIndex(valueColumnName), getColumnIndex(valueGroupColumnName), getColumnIndex(wholeColumnName), aggregateType);
    }

    public java.math.BigDecimal getPercent(int valueColumn, int wholeColumn, Aggregate aggregateType) {
        return getPercent(valueColumn, 0, wholeColumn, aggregateType);
    }

    public java.math.BigDecimal getPercent(int valueColumn, int valueGroupColumn, int wholeColumn, Aggregate aggregateType) {
        Object tmp = getAggregate(valueColumn,wholeColumn,aggregateType);
        java.math.BigDecimal whole;
        if(tmp == null) return null;
        else if(tmp instanceof java.math.BigDecimal) whole = (java.math.BigDecimal) tmp;
        else whole = new java.math.BigDecimal(tmp.toString());
        if(whole.signum() == 0) return whole;

        if(valueGroupColumn == 0) tmp = getValue(valueColumn);
        else tmp = getAggregate(valueColumn,valueGroupColumn,aggregateType);
        java.math.BigDecimal part;
        if(tmp == null) return null;
        else if(tmp instanceof java.math.BigDecimal) part = (java.math.BigDecimal) tmp;
        else part = new java.math.BigDecimal(tmp.toString());

        return part.divide(whole, 10, java.math.BigDecimal.ROUND_HALF_UP);
    }

    @Override
	public Results clone() {
        try { return (Results)super.clone(); } catch(CloneNotSupportedException cnse) { return null; }
    }

    // for interface DynaBean
    @Override
	protected void setDynaProperty(String name, Object value) {
        throw new UnsupportedOperationException("Results does not support the set method");
    }

    @Override
	protected boolean hasDynaProperty(String name) {
        try {
            return getColumnIndex(name) > 0;
        } catch(NullPointerException npe) {
            return false;
		} catch(IllegalArgumentException e) {
			return false;
        }
    }

    @Override
	protected Object getDynaProperty(String name) {
        try {
            return getValue(name);
        } catch(NullPointerException npe) {
            return null;
        }
    }

    @Override
	protected String getDynaClassName() {
        return getClass().getName();
    }

    public String getFormattedAggregate(int column, int groupColumn, Aggregate aggregateType) {
        return format(getAggregate(column, groupColumn, aggregateType), column);
    }

    public String getFormattedAggregate(String columnName, String groupColumnName, Aggregate aggregateType) {
        return getFormattedAggregate(getColumnIndex(columnName), getColumnIndex(groupColumnName), aggregateType);
    }

    public String getFormattedAggregate(int column, int groupColumn, ResultsFilter filter, Aggregate aggregateType) {
        return format(getAggregate(column, groupColumn, filter, aggregateType), column);
    }

    public String getFormattedAggregate(String columnName, String groupColumnName, ResultsFilter filter, Aggregate aggregateType) {
        return getFormattedAggregate(getColumnIndex(columnName), getColumnIndex(groupColumnName), filter, aggregateType);
    }

    public String[] getFormattedGroupValues(int groupColumn) {
        Object[] gvs = getGroupValues(groupColumn);
        String[] fgvs = new String[gvs.length];
        for(int i = 0; i < gvs.length; i++)
            fgvs[i] = format(gvs[i], groupColumn);
        return fgvs;
    }

    public String[] getFormattedGroupValues(String groupColumnName) {
        return getFormattedGroupValues(getColumnIndex(groupColumnName));
    }

    public String getFormattedValue(int column) {
        return format(getValue(column), column);
    }

    public String getFormattedValue(int column, String format) {
      return format(getValue(column), format);
    }

    protected String format(Object value, int column) {
    	return ConvertUtils.formatObject(value, getFormat(column), getLocale());
    }

    protected String format(Object value, String format) {
    	return ConvertUtils.formatObject(value, format, getLocale());
    }

    public String[] getFormats() {
        return formats;
    }

    public String getFormat(int column) {
        if(formats == null || column < 1 || formats.length < column) return null;
        else return formats[column-1];
    }

    public String getFormat(String columnName) {
        return getFormat(getColumnIndex(columnName));
    }

    public void setFormats(String[] formats) {
        this.formats = formats;
    }

    public void setFormat(String columnName, String format) {
        setFormat(getColumnIndex(columnName), format);
    }

    public void setFormat(int column, String format) {
		if(column < 1)
			return;
		if(formats == null) {
			if(format == null)
				return;
			formats = new String[Math.max(getColumnCount(), column)];
		} else if(column > formats.length) {
			if(format == null)
				return;
			String[] tmp = new String[column];
            System.arraycopy(formats, 0, tmp, 0, formats.length);
            formats = tmp;
        }
        formats[column-1] = format;
    }

    public String getFormattedValue(String columnName) {
        return getFormattedValue(getColumnIndex(columnName));
    }

    public String getFormattedValue(String columnName, String format) {
      return getFormattedValue(getColumnIndex(columnName), format);
    }

    @Override
	protected DynaProperty[] getDynaClassProperties() {
        if(dynaProperties == null) {
            dynaProperties = new DynaProperty[getColumnCount()];
            for(int i = 0; i < dynaProperties.length; i++)
                dynaProperties[i] = new DynaProperty(getColumnName(i+1), getColumnClass(i+1));
        }
        return dynaProperties;
    }

    public void fillBean(Object bean) throws BeanException {
        int k = 0;
        try {
            for(; k < getColumnCount(); k++)
                ReflectionUtils.setProperty(bean, getColumnName(k+1), getValue(k+1), true);
        } catch(IntrospectionException e) {
            throw new BeanException("While setting property, '" + getColumnName(k+1) + "' on bean '" + bean + "'", e);
        } catch (IllegalAccessException e) {
            throw new BeanException("While setting property, '" + getColumnName(k+1) + "' on bean '" + bean + "'", e);
        } catch (InvocationTargetException e) {
            throw new BeanException("While setting property, '" + getColumnName(k+1) + "' on bean '" + bean + "'", e);
        } catch (InstantiationException e) {
            throw new BeanException("While setting property, '" + getColumnName(k+1) + "' on bean '" + bean + "'", e);
        } catch (ConvertException e) {
            throw new BeanException("While setting property, '" + getColumnName(k+1) + "' on bean '" + bean + "'", e);
        } catch(ParseException e) {
            throw new BeanException("While setting property, '" + getColumnName(k+1) + "' on bean '" + bean + "'", e);
		}
    }

    public static BigDecimal calcSum(BigDecimal sum, Number value) {
        if(sum == null) sum = new BigDecimal("0");
        BigDecimal bd;
        if(value instanceof BigDecimal) {
            bd = (BigDecimal)value;
        } else  if(value instanceof java.math.BigInteger) {
            bd = new BigDecimal((java.math.BigInteger)value);
        } else  if(value instanceof Double || value instanceof Float) {
            bd = new BigDecimal(value.doubleValue());
        } else {
            bd = new BigDecimal(value.toString());
        }
        return sum.add(bd);
    }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected static Comparable calcMax(Comparable max, Comparable value) {
        if(max == null) return value;
        //if(value == null) return max;
        if(max.compareTo(value) < 0) return value;
        return max;
    }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected static Comparable calcMin(Comparable min, Comparable value) {
        if(min == null) return value;
        //if(value == null) return min;
        if(min.compareTo(value) > 0) return value;
        return min;
    }

    protected static void addValue(Collection<Object> collection, Object value) {
    	if(ConvertUtils.isCollectionType(value.getClass())) {
    		try {
				collection.addAll(ConvertUtils.asCollection(value));
				return;
			} catch(ConvertException e) {
			}
    	}
    	collection.add(value);
    }

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Object getAggregate(int columnIndex, int groupColumnIndex, ResultsFilter filter, Aggregate aggregate) {
		Object value = getRawAggregate(columnIndex, groupColumnIndex, filter, aggregate);
		int uniqueColumnIndex;
		if(value instanceof Number && needsDoubleCountProtection(aggregate) && (uniqueColumnIndex = getUniqueColumnIndexFor(columnIndex)) > 0) {
			Object mult = getRawAggregate(uniqueColumnIndex, groupColumnIndex, filter, Aggregate.DISTINCT);
			Object div = getRawAggregate(uniqueColumnIndex, groupColumnIndex, filter, Aggregate.COUNT);
			try {
				if(value instanceof BigDecimal) {
					value = calcAggAsBigDecimal(value, mult, div);
				} else if(value instanceof BigInteger) {
					value = calcAggAsLong(value, mult, div);
				} else if(value instanceof Long || value instanceof Integer || value instanceof Short || value instanceof Byte) {
					value = calcAggAsLong(value, mult, div);
				} else if(value instanceof Double || value instanceof Float) {
					value = calcAggAsDouble(value, mult, div);
				} else {
					value = calcAggAsBigDecimal(value, mult, div);
				}
			} catch(ConvertException e) {
				throw new UndeclaredThrowableException(e);
			}
		}
		return value;
	}

	protected abstract Object getRawAggregate(int columnIndex, int groupColumnIndex, ResultsFilter filter, Aggregate aggregate);

	public int getUniqueColumnIndexFor(int columnIndex) {
		if(uniqueColumnIndexes == null || uniqueColumnIndexes.length < columnIndex)
			return 0;
		return uniqueColumnIndexes[columnIndex - 1];
	}

	public void setUniqueColumnIndexFor(int columnIndex, int uniqueColumnIndex) {
		if(columnIndex < 1)
			return;
		if(uniqueColumnIndexes == null) {
			if(uniqueColumnIndex < 1)
				return;
			uniqueColumnIndexes = new int[Math.max(getColumnCount(), columnIndex)];
		} else if(columnIndex > uniqueColumnIndexes.length) {
			if(uniqueColumnIndex < 1)
				return;
			int[] tmp = new int[columnIndex];
			System.arraycopy(uniqueColumnIndexes, 0, tmp, 0, uniqueColumnIndexes.length);
			uniqueColumnIndexes = tmp;
		}
		uniqueColumnIndexes[columnIndex - 1] = uniqueColumnIndex;
		if(uniqueColumnIndex < 1)
			return;
		trackAggregate(uniqueColumnIndex, Aggregate.COUNT, Aggregate.DISTINCT);
	}

	public String getUniqueColumnNameFor(String columnName) {
		int uci = getUniqueColumnIndexFor(getColumnIndex(columnName));
		return uci < 1 ? null : getColumnName(uci);
	}

	public void setUniqueColumnNameFor(String columnName, String uniqueColumnName) {
		int ci = getColumnIndex(columnName);
		int uci = uniqueColumnName == null ? 0 : getColumnIndex(uniqueColumnName);
		setUniqueColumnIndexFor(ci, uci);
	}

	protected BigDecimal calcAggAsBigDecimal(Object value, Object mult, Object div) throws ConvertException {
		BigDecimal valueBd = ConvertUtils.convert(BigDecimal.class, value);
		if(valueBd == null || valueBd.signum() == 0)
			return valueBd;
		BigDecimal multBd = ConvertUtils.convert(BigDecimal.class, mult);
		BigDecimal divBd = ConvertUtils.convert(BigDecimal.class, div);
		if(multBd == null || multBd.signum() == 0 || divBd == null || divBd.signum() == 0)
			return null;
		return valueBd.multiply(multBd).divide(divBd, new MathContext(valueBd.precision(), RoundingMode.HALF_UP));
	}

	protected BigInteger calcAggAsBigInteger(Object value, Object mult, Object div) throws ConvertException {
		BigInteger valueBd = ConvertUtils.convert(BigInteger.class, value);
		if(valueBd == null || valueBd.signum() == 0)
			return valueBd;
		BigInteger multBd = ConvertUtils.convert(BigInteger.class, mult);
		BigInteger divBd = ConvertUtils.convert(BigInteger.class, div);
		if(multBd == null || multBd.signum() == 0 || divBd == null || divBd.signum() == 0)
			return null;
		return valueBd.multiply(multBd).divide(divBd);
	}

	protected Long calcAggAsLong(Object value, Object mult, Object div) throws ConvertException {
		Long valueLong = ConvertUtils.convert(Long.class, value);
		if(valueLong == null || valueLong == 0)
			return valueLong;
		Long multLong = ConvertUtils.convert(Long.class, mult);
		Long divLong = ConvertUtils.convert(Long.class, div);
		if(multLong == null || multLong == 0 || divLong == null || divLong == 0)
			return null;
		return valueLong * multLong / divLong;
	}

	protected Double calcAggAsDouble(Object value, Object mult, Object div) throws ConvertException {
		Double valueDouble = ConvertUtils.convert(Double.class, value);
		if(valueDouble == null || valueDouble == 0.0)
			return valueDouble;
		Double multDouble = ConvertUtils.convert(Double.class, mult);
		Double divDouble = ConvertUtils.convert(Double.class, div);
		if(multDouble == null || multDouble == 0.0 || divDouble == null || divDouble == 0.0)
			return null;
		return valueDouble * multDouble / divDouble;
	}

	protected boolean needsDoubleCountProtection(Aggregate aggregate) {
		switch(aggregate) {
			case COUNT:
			case SUM:
				return true;
			default:
				return false;
		}
	}
}
