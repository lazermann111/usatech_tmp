/*
 * Created on Dec 9, 2005
 *
 */
package simple.results;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import simple.db.Column;
import simple.io.Log;

public class LazyCachedResultsCreator extends CachedResultsCreator {
    private static final Log log = Log.getLog();
    protected Exception exception;
    protected final Lock lock = new ReentrantLock();
    protected final Condition signal = lock.newCondition();
    protected volatile boolean cachingComplete = false;
    protected volatile boolean awaitingComplete = false;
    public LazyCachedResultsCreator(Collection<Object[]> data, Column[] columns, File cacheFile, int compression) {
        super(columns, cacheFile, compression);
        rows = data.size();
        startCachingThread(data);
    }

    @Override
    public Results createResults() throws ResultsCreationException {
        //if(!cachingComplete) {
            lock.lock();
            try {
                if(!cachingComplete) {
                    awaitingComplete = true;
                    signal.await();
                }
            } catch(InterruptedException e) {
                throw new ResultsCreationException("Interrupted", e);
            } finally {
                lock.unlock();
            }
        //}
        return super.createResults();
    }
    
    protected void startCachingThread(final Collection<Object[]> data) {
        Thread thread = new Thread("CachingThread-" + hashCode()) {       
            @Override
            public void run() {
                try {
                    if(!awaitingComplete) Thread.yield();
                    OutputStream os;
                    if(compression > 0)
                        os = new DeflaterOutputStream(new FileOutputStream(cacheFile), new Deflater(compression), 8192);
                    else
                        os = new BufferedOutputStream(new FileOutputStream(cacheFile), 8192);
                    ObjectOutputStream cacheStream = new ObjectOutputStream(os);
                    for(Object[] row : data) {
                        if(!awaitingComplete) Thread.yield();
                        cacheStream.writeObject(row);
                    }
                    if(!awaitingComplete) Thread.yield();
                    log.debug("Wrote " + data.size() + " rows to cache file");                
                    cacheStream.writeObject(new CachedResultsCreator.EOFMarker()); // mark the end
                    log.debug("Flushing and closing cache stream");
                    cacheStream.flush();
                    cacheStream.close();
               } catch(RuntimeException e) {
                    log.warn("While Writing to file", e);
               } catch(IOException e) {
                   exception = e;
               } finally {
                    log.debug("Signalling cacheSignal");
                    cachingComplete = true;
                    lock.lock();
                    try {
                        signal.signalAll();
                    } finally {
                        lock.unlock();
                    }
                    log.debug("Exiting Run method");                    
                }
            }        
        };
        thread.setDaemon(true);
        thread.setPriority(3);
        thread.start();   
    }

}
