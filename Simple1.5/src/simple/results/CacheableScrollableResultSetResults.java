/*
 * Created on Dec 9, 2005
 *
 */
package simple.results;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

import simple.db.Column;
import simple.db.DBHelper;
import simple.io.CacheFile;
import simple.security.SecurityUtils;

public class CacheableScrollableResultSetResults extends ScrollableResultSetResults implements CacheableResults {
    protected int compression = 1;
    
    public CacheableScrollableResultSetResults(Column[] columns, ResultSet rs, DBHelper dbHelper) {
        super(columns, rs, dbHelper);
    }
    
    public ResultsCreator getCachedResultsCreator() throws CachingException {
        if(resultsCreator == null) {
            try {
                File file = new CacheFile(File.createTempFile("cached-results-" + SecurityUtils.getRandomAlphaNumericString(16) + "-", ".dat").getPath());
                Collection<Object[]> rows = new AbstractCollection<Object[]>() {
                    @Override
                    public Iterator<Object[]> iterator() {
                        return new Iterator<Object[]>() {
                            protected int index = 0;
                            public boolean hasNext() {
                                return index < size();
                            }
                            public Object[] next() {
                                return getMapAt(index++).getValues();
                            }
                            public void remove() {
                                throw new UnsupportedOperationException("Remove() not supported");
                            }
                        };
                    }

                    @Override
                    public int size() {
                        return CacheableScrollableResultSetResults.this.size();
                    }                    
                };
                resultsCreator = new LazyCachedResultsCreator(rows, columns, file, compression);
            } catch(IOException e) {
                throw new CachingException("While creating cache file", e);
            }
        }
        return resultsCreator;
    }
}
