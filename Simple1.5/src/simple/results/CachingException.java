/*
 * Created on Dec 6, 2005
 *
 */
package simple.results;

public class CachingException extends Exception {
    private static final long serialVersionUID = 119872034L;

    public CachingException() {
        super();
    }

    public CachingException(String message) {
        super(message);
    }

    public CachingException(String message, Throwable cause) {
        super(message, cause);
    }

    public CachingException(Throwable cause) {
        super(cause);
    }

}
