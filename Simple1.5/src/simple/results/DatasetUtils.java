package simple.results;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.BinaryStream;
import simple.io.ByteInput;
import simple.io.ByteOutput;
import simple.text.StringUtils;

public class DatasetUtils {
	protected static final byte[] VERSION_1 = new byte[] { 0x77, 0x01 };
	protected static final Charset ENCODING = Charset.forName("UTF-8");
	protected static final Map<Class<?>,SortedSet<DataType>> classToDataType = new HashMap<Class<?>,SortedSet<DataType>>();
	protected static final Comparator<? super DataType> dataTypeComparator = new Comparator<DataType>() {
		public int compare(DataType o1, DataType o2) {
			return o1.getLength() - o2.getLength();
		}		
	};
	public static long MAX_SMALL_LENGTH = 1L << (8 * 1);
	public static long MAX_MEDIUM_LENGTH = 1L << (8 * 2);
	public static long MAX_LARGE_LENGTH = 1L << (8 * 4);
	protected static int BUFFER_SIZE = 1024;
	
	protected static enum DataType {
		NULL(void.class, 0), BOOLEAN(boolean.class, 0), BYTE(byte.class, 0), SHORT(short.class, 0), INTEGER(int.class, 0), LONG(long.class, 0), FLOAT(float.class, 0), DOUBLE(double.class, 0),
		SMALL_STRING(String.class, 1), MEDIUM_STRING(String.class, 2), LARGE_STRING(String.class, 4), 
		SMALL_BYTE_ARRAY(byte[].class, 1), MEDIUM_BYTE_ARRAY(byte[].class, 2), LARGE_BYTE_ARRAY(byte[].class, 4), 
		SMALL_BIG_INTEGER(BigInteger.class, 1), MEDIUM_BIG_INTEGER(BigInteger.class, 2), LARGE_BIG_INTEGER(BigInteger.class, 4), 
		SMALL_BIG_DECIMAL(BigDecimal.class, 1), MEDIUM_BIG_DECIMAL(BigDecimal.class, 2), LARGE_BIG_DECIMAL(BigDecimal.class, 4), 
		DATE(Date.class, 0), CALENDAR(Calendar.class, 0), SQL_DATE(java.sql.Date.class, 0), SQL_TIME(java.sql.Time.class, 0), SQL_TIMESTAMP(java.sql.Timestamp.class, 0),
		SMALL_CLASS(Class.class, 1), MEDIUM_CLASS(Class.class, 2), ENUM(Enum.class, 0),
		SMALL_ARRAY(Object[].class, 1), MEDIUM_ARRAY(Object[].class, 2), LARGE_ARRAY(Object[].class, 4),
		OBJECT(Object.class, 0);
		private final Class<?> dataClass;
		private final byte length; /* in bytes */
		private DataType(Class<?> dataClass, int length) {
			this.dataClass = dataClass;
			this.length = (byte)length;
			SortedSet<DataType> dts = classToDataType.get(dataClass);
			if(dts == null) {
				dts = new TreeSet<DataType>(dataTypeComparator);
				classToDataType.put(dataClass, dts);
				if(dataClass.isPrimitive())
					classToDataType.put(ConvertUtils.convertToWrapperClass(dataClass), dts);
			}
			dts.add(this);	
		}
		public Class<?> getDataClass() {
			return dataClass;
		}
		public byte getLength() {
			return length;
		}
	}
	
	/** Writes a dataset to a ByteOutput and returns the number of rows written
	 * @param output
	 * @param results
	 * @return
	 * @throws IOException
	 */
	public static int writeDataset(ByteOutput output, Results results) throws IOException {
		return writeDataset(output, results, results.getColumnCount(), null);
	}

	public static int writeDataset(ByteOutput output, Results results, Runnable afterRow) throws IOException {
		return writeDataset(output, results, results.getColumnCount(), afterRow);
	}

	public static int writeDataset(ByteOutput output, Results results, int columnCount) throws IOException {
		return writeDataset(output, results, columnCount, null);
	}

	public static int writeDataset(ByteOutput output, Results results, int columnCount, Runnable afterRow) throws IOException {
		output.write(VERSION_1);
		output.writeShort((short)columnCount);
		for(int i = 1; i <= columnCount; i++) {
			writeSmallString(output, results.getColumnName(i));
		}
		int rows = 0;
		while(results.next()) {
			output.writeByte(1);
			for(int i = 1; i <= columnCount; i++) {
				writeObject(output, results.getValue(i));
			}
			rows++;
			if(afterRow != null)
				afterRow.run();
		}
		output.writeByte(-1);
		return rows;
	}
	
	/** Writes the header of dataset to a ByteOutput
	 * @param output
	 * @param columnNames
	 * @return
	 * @throws IOException
	 */
	public static void writeHeader(ByteOutput output, String[] columnNames) throws IOException {
		output.write(VERSION_1);
		int cnt = columnNames.length;
		output.writeShort((short)cnt);
		for(String columnName : columnNames) {
			writeSmallString(output, columnName);
		}
	}
	
	/** Writes a row of a dataset to a ByteOutput
	 * @param output
	 * @param values
	 * @return
	 * @throws IOException
	 */
	public static void writeRow(ByteOutput output, Object[] values) throws IOException {
		output.writeByte(1);
		for(Object value : values) {
			writeObject(output, value);
		}
	}
	/** Writes the footer of a dataset to a ByteOutput
	 * @param output
	 * @throws IOException
	 */
	public static void writeFooter(ByteOutput output) throws IOException {
		output.writeByte((byte) -1);
	}
	
	public static void writeObject(ByteOutput output, Object value) throws IOException {
		if(value == null)
			writeNullData(output);
		else {
			Class<?> clazz = value.getClass();
			DataType dataType = DataType.OBJECT; // this must be first so that DataType class is initialized and classToDataType is populated
			SortedSet<DataType> dts = classToDataType.get(clazz);
			if(dts != null) {
				for(DataType dt : dts) {
					if(supportsLength(dt, value)) {
						dataType = dt;
						break;
					}
				}
			}
			output.writeByte(dataType.ordinal());
			try {
				switch(dataType) {
					case NULL:
						break;
					case BOOLEAN:
						output.writeByte(ConvertUtils.getBoolean(value) ? 1 : -1);
						break;
					case BYTE:
						output.writeByte(ConvertUtils.getByte(value));
						break;
					case SHORT:
						output.writeShort(ConvertUtils.getShort(value));
						break;
					case INTEGER:
						output.writeInt(ConvertUtils.getInt(value));
						break;
					case LONG:
						output.writeLong(ConvertUtils.getLong(value));
						break;
					case FLOAT:
						output.writeFloat(ConvertUtils.getFloat(value));
						break;
					case DOUBLE:
						output.writeDouble(ConvertUtils.getDouble(value));
						break;
					case SMALL_STRING:
						writeSmallString(output, ConvertUtils.getString(value, true));
						break;
					case MEDIUM_STRING:
						writeMediumString(output, ConvertUtils.getString(value, true));
						break;
					case LARGE_STRING:
						writeLargeString(output, ConvertUtils.getString(value, true));
						break;
					case DATE: case SQL_DATE: case SQL_TIME: case SQL_TIMESTAMP:
						output.writeLong(ConvertUtils.getLong(value));
						break;
					case CALENDAR:
						Calendar cal = ConvertUtils.convert(Calendar.class, value);
						output.writeLong(cal.getTimeInMillis());
						writeSmallString(output, cal.getTimeZone().getID());
						break;
					case SMALL_BIG_INTEGER:
						writeSmallByteArray(output, ConvertUtils.convert(BigInteger.class, value).toByteArray());
						break;
					case MEDIUM_BIG_INTEGER:
						writeMediumByteArray(output, ConvertUtils.convert(BigInteger.class, value).toByteArray());
						break;
					case LARGE_BIG_INTEGER:
						writeLargeByteArray(output, ConvertUtils.convert(BigInteger.class, value).toByteArray());
						break;
					case SMALL_BIG_DECIMAL:
						BigDecimal bd = ConvertUtils.convert(BigDecimal.class, value);
						writeSmallByteArray(output, bd.unscaledValue().toByteArray());
						output.writeInt(bd.scale());
						break;
					case MEDIUM_BIG_DECIMAL:
						bd = ConvertUtils.convert(BigDecimal.class, value);
						writeMediumByteArray(output, bd.unscaledValue().toByteArray());
						output.writeInt(bd.scale());
						break;
					case LARGE_BIG_DECIMAL:
						bd = ConvertUtils.convert(BigDecimal.class, value);
						writeLargeByteArray(output, bd.unscaledValue().toByteArray());
						output.writeInt(bd.scale());
						break;
					case SMALL_BYTE_ARRAY:
						writeSmallByteArray(output, ConvertUtils.convert(byte[].class, value));
						break;						
					case MEDIUM_BYTE_ARRAY:
						writeMediumByteArray(output, ConvertUtils.convert(byte[].class, value));
						break;						
					case LARGE_BYTE_ARRAY:
						writeMediumByteArray(output, ConvertUtils.convert(byte[].class, value));
						break;	
					case SMALL_CLASS:
						writeSmallString(output, ConvertUtils.convert(Class.class, value).getName());
						break;
					case MEDIUM_CLASS:
						writeMediumString(output, ConvertUtils.convert(Class.class, value).getName());
						break;
					case SMALL_ARRAY:
						Object[] array = ConvertUtils.convert(Object[].class, value);
						output.writeByte(array.length);
						for(Object a : array)
							writeObject(output, a);
						break;						
					case MEDIUM_ARRAY:
						array = ConvertUtils.convert(Object[].class, value);
						output.writeShort(array.length);
						for(Object a : array)
							writeObject(output, a);
						break;						
					case LARGE_ARRAY:
						array = ConvertUtils.convert(Object[].class, value);
						output.writeInt(array.length);
						for(Object a : array)
							writeObject(output, a);
						break;	
					case ENUM:
						writeMediumString(output, clazz.getName());
						output.writeInt(ConvertUtils.getInt(value));
					case OBJECT:
						writeMediumString(output, clazz.getName());
						writeLargeString(output, ConvertUtils.getString(value, false));
						break;
				}
			} catch(ConvertException e) {
				throw new IOException("Could not convert data", e);
			}
		}
	}

	public static boolean supportsLength(DataType dataType, Object value) {
		if(dataType.getLength() <= 0)
			return true;
		long len = 1L << (8 * dataType.getLength());
		try {
			if(dataType.getDataClass().equals(String.class)) 
				return len > ConvertUtils.getString(value, true).length();
			if(dataType.getDataClass().equals(byte[].class)) 
				return len> ConvertUtils.convert(byte[].class, value).length;
			if(dataType.getDataClass().equals(BigInteger.class)) 
				return len > Math.ceil((ConvertUtils.convert(BigInteger.class, value).bitLength() + 1) / 8.0);
			if(dataType.getDataClass().equals(BigDecimal.class)) 
				return len > Math.ceil((ConvertUtils.convert(BigDecimal.class, value).unscaledValue().bitLength() + 1) / 8.0);
			if(dataType.getDataClass().equals(Class.class)) 
				return len > ConvertUtils.convert(Class.class, value).getName().length();
			if(dataType.getDataClass().equals(Object[].class)) 
				return len> ConvertUtils.convert(Object[].class, value).length;					
		} catch(ConvertException e) {
			//ignore - will probably be thrown again in later processing
		}
		return false;
	}

	public static void writeNullData(ByteOutput output) throws IOException {
		output.writeByte(DataType.NULL.ordinal());
	}

	public static void writeSmallString(ByteOutput output, String value) throws IOException {
		if(value == null || value.length() == 0) {
			output.writeByte(0);
		} else if(value.length() > MAX_SMALL_LENGTH) {
			throw new IOException("Length of string is " + value.length() + "; max supported is " + MAX_SMALL_LENGTH);
		} else {
			byte[] bytes = value.getBytes(ENCODING);
			output.writeByte(bytes.length);
			output.write(bytes);
		}
	}
	public static void writeMediumString(ByteOutput output, String value) throws IOException {
		if(value == null || value.length() == 0) {
			output.writeShort(0);
		} else if(value.length() > MAX_MEDIUM_LENGTH) {
			throw new IOException("Length of string is " + value.length() + "; max supported is " + MAX_MEDIUM_LENGTH);
		} else {
			byte[] bytes = value.getBytes(ENCODING);
			output.writeShort(bytes.length);
			output.write(bytes);
		}
	}
	public static void writeLargeString(ByteOutput output, String value) throws IOException {
		if(value == null || value.length() == 0) {
			output.writeInt(0);
		} else if(value.length() > MAX_LARGE_LENGTH) {
			throw new IOException("Length of string is " + value.length() + "; max supported is " + MAX_LARGE_LENGTH);
		} else {
			byte[] bytes = value.getBytes(ENCODING);
			output.writeInt(bytes.length);
			output.write(bytes);
		}
	}
	public static void writeSmallByteArray(ByteOutput output, byte[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeByte(0);
		} else if(value.length > MAX_SMALL_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + MAX_SMALL_LENGTH);
		} else {
			output.writeByte(value.length);
			output.write(value);
		}
	}
	public static void writeMediumByteArray(ByteOutput output, byte[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeShort(0);
		} else if(value.length > MAX_MEDIUM_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + MAX_MEDIUM_LENGTH);
		} else {
			output.writeShort(value.length);
			output.write(value);
		}
	}
	public static void writeLargeByteArray(ByteOutput output, byte[] value) throws IOException {
		if(value == null || value.length == 0) {
			output.writeInt(0);
		} else if(value.length > MAX_LARGE_LENGTH) {
			throw new IOException("Length of byte array is " + value.length + "; max supported is " + MAX_LARGE_LENGTH);
		} else {
			output.writeInt(value.length);
			output.write(value);
		}
	}
	public static void writeSmallByteArray(ByteOutput output, BinaryStream value) throws IOException {
		long len;
		if(value == null || (len=value.getLength()) == 0L) {
			output.writeByte(0);
		} else if(len > MAX_SMALL_LENGTH) {
			throw new IOException("Length of BinaryStream is " + len + "; max supported is " + MAX_SMALL_LENGTH);
		} else {
			output.writeByte((int)len);
			copyInto(output, value);
		}
	}
	public static void writeMediumByteArray(ByteOutput output, BinaryStream value) throws IOException {
		long len;
		if(value == null || (len=value.getLength()) == 0L) {
			output.writeShort(0);
		} else if(len > MAX_MEDIUM_LENGTH) {
			throw new IOException("Length of BinaryStream is " + len + "; max supported is " + MAX_MEDIUM_LENGTH);
		} else {
			output.writeShort((int)len);
			copyInto(output, value);
		}
	}
	public static void writeLargeByteArray(ByteOutput output, BinaryStream value) throws IOException {
		long len;
		if(value == null || (len=value.getLength()) == 0L) {
			output.writeInt(0);
		} else if(len > MAX_LARGE_LENGTH) {
			throw new IOException("Length of BinaryStream is " + len + "; max supported is " + MAX_LARGE_LENGTH);
		} else {
			output.writeInt((int)len);
			copyInto(output, value);
		}
	}
	
	protected static void copyInto(ByteOutput output, BinaryStream binaryStream) throws IOException {
		binaryStream.copyInto(output);
	}
	
	public static <I extends Exception> int parseDataset(ByteInput input, DatasetHandler<I> handler) throws IOException, I {
		byte[] version = new byte[VERSION_1.length];
		input.readBytes(version, 0, version.length);
		if(Arrays.equals(version, VERSION_1)) {
			int cnt = input.readShort();
			String[] columnNames = new String[cnt];
			for(int i = 0; i < cnt; i++) {
				columnNames[i] = readSmallString(input);
			}
			handler.handleDatasetStart(columnNames);
			int rows = 0;
			while(true) {
				byte r = input.readByte();
				switch(r) {
					case 1:
						handler.handleRowStart();
						for(int i = 0; i < cnt; i++) {
							Object value = readObject(input);
							handler.handleValue(columnNames[i], value);
						}
						handler.handleRowEnd();
						rows++;
						break;
					case -1:
						handler.handleDatasetEnd();
						return rows;
					default:
						throw new IOException("Invalid row prefix " + r);
				}
			}
		} else {
			throw new IOException("Invalid byte header or unsupported version '" + StringUtils.toHex(version) + "'");
		}		
	}
	
	public static <I extends Exception> int parseAndMergeDatasets(DatasetHandler<I> handler, ByteInput... inputs) throws IOException, I {
		//setup
		byte[][] versions = new byte[inputs.length][];
		String[][] columnNames = new String[inputs.length][];
		List<String> mergedList = new ArrayList<String>();
		for(int i = 0; i < inputs.length; i++) {
			versions[i] = new byte[VERSION_1.length];
			inputs[i].readBytes(versions[i], 0, versions[i].length);
			if(Arrays.equals(versions[i], VERSION_1)) {
				int cnt = inputs[i].readShort();
				columnNames[i] = new String[cnt];
				for(int k = 0; k < cnt; k++) {
					String cn = readSmallString(inputs[i]);
					columnNames[i][k] = cn;
					mergedList.add(cn);
				}
				
			} else {
				throw new IOException("Invalid byte header or unsupported version '" + StringUtils.toHex(versions[i]) + "'");
			}
		}
		
		//header
		String[] mergedColumnNames = mergedList.toArray(new String[mergedList.size()]);
		handler.handleDatasetStart(mergedColumnNames);
		
		// rows
		int rows = 0;
		boolean[] done = new boolean[inputs.length];
		boolean allDone = false;
		while(!allDone) {
			allDone = true;
			for(int i = 0; i < inputs.length; i++) {
				if(!done[i]) {
					byte r = inputs[i].readByte();
					switch(r) {
						case 1:
							if(allDone) {
								handler.handleRowStart();
								allDone = false;
								rows++;
							}
							for(int k = 0; k < columnNames[i].length; k++) {
								Object value = readObject(inputs[i]);
								handler.handleValue(columnNames[i][k], value);
							}
							break;
						case -1:
							done[i] = true;
							break;
						default:
							throw new IOException("Invalid row prefix " + r);
					}
				}
			}
			if(!allDone)
				handler.handleRowEnd();
		}
		handler.handleDatasetEnd();
		return rows;	
	}
	
	public static Object readObject(ByteInput input) throws IOException {
		int ordinal = input.readByte() & 0xFF;
		DataType dataType;
		try {
			dataType = DataType.values()[ordinal];
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new IOException("Invalid data type ordinal " + ordinal, e);
		}
		try {
			switch(dataType) {
				case NULL:
					return null;
				case BOOLEAN:
					return input.readByte() == 1;
				case BYTE:
					return input.readByte();
				case SHORT:
					return input.readShort();
				case INTEGER:
					return input.readInt();
				case LONG:
					return input.readLong();
				case FLOAT:
					return input.readFloat();
				case DOUBLE:
					return input.readDouble();
				case SMALL_STRING:
					return readSmallString(input);
				case MEDIUM_STRING:
					return readMediumString(input);
				case LARGE_STRING:
					return readLargeString(input);
				case DATE: 
					return new Date(input.readLong());
				case SQL_DATE: 
					return new java.sql.Date(input.readLong());
				case SQL_TIME: 
					return new java.sql.Time(input.readLong());
				case SQL_TIMESTAMP:
					return new java.sql.Timestamp(input.readLong());
				case CALENDAR:
					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(input.readLong());
					cal.setTimeZone(TimeZone.getTimeZone(readSmallString(input)));
					return cal;
				case SMALL_BIG_INTEGER:
					return new BigInteger(readSmallByteArray(input));
				case MEDIUM_BIG_INTEGER:
					return new BigInteger(readMediumByteArray(input));
				case LARGE_BIG_INTEGER:
					return new BigInteger(readLargeByteArray(input));
				case SMALL_BIG_DECIMAL:
					return new BigDecimal(new BigInteger(readSmallByteArray(input)), input.readInt());
				case MEDIUM_BIG_DECIMAL:
					return new BigDecimal(new BigInteger(readMediumByteArray(input)), input.readInt());
				case LARGE_BIG_DECIMAL:
					return new BigDecimal(new BigInteger(readLargeByteArray(input)), input.readInt());
				case SMALL_BYTE_ARRAY:
					return readSmallByteArray(input);						
				case MEDIUM_BYTE_ARRAY:
					return readMediumByteArray(input);						
				case LARGE_BYTE_ARRAY:
					return readLargeByteArray(input);						
				case SMALL_CLASS:
					String className = readSmallString(input);
					try {
						return Class.forName(className);
					} catch(ClassNotFoundException e) {
						throw new IOException("Could not find class '" + className + "'", e);
					}
				case MEDIUM_CLASS:
					className = readMediumString(input);
					try {
						return Class.forName(className);
					} catch(ClassNotFoundException e) {
						throw new IOException("Could not find class '" + className + "'", e);
					}
				case SMALL_ARRAY:
					int n = input.readByte();
					Object[] array = new Object[n];
					for(int i = 0; i < n; i++)
						array[i] = readObject(input);
					return array;						
				case MEDIUM_ARRAY:
					n = input.readShort();
					array = new Object[n];
					for(int i = 0; i < n; i++)
						array[i] = readObject(input);
					return array;						
				case LARGE_ARRAY:
					n = input.readInt();
					array = new Object[n];
					for(int i = 0; i < n; i++)
						array[i] = readObject(input);
					return array;						
				case ENUM:
					className = readMediumString(input);
					Class<?> clazz;
					try {
						clazz = Class.forName(className);
					} catch(ClassNotFoundException e) {
						throw new IOException("Could not find class '" + className + "'", e);
					}
					return ConvertUtils.convert(clazz, input.readInt());
				case OBJECT:
					className = readMediumString(input);
					try {
						clazz = Class.forName(className);
					} catch(ClassNotFoundException e) {
						throw new IOException("Could not find class '" + className + "'", e);
					}
					return ConvertUtils.convert(clazz, readLargeString(input));
				default:
					throw new IOException("DataType '" + dataType + "' not supported");
			}
		} catch(ConvertException e) {
			throw new IOException("Could not convert data", e);
		}
	}

	public static String readSmallString(ByteInput input) throws UnsupportedEncodingException, IOException {
		byte[] bytes = readSmallByteArray(input);
		return bytes.length == 0 ? null : new String(bytes, ENCODING);
	}
	public static byte[] readSmallByteArray(ByteInput input) throws IOException {
		int len = input.readUnsignedByte();
		byte[] bytes = new byte[len];
		input.readBytes(bytes, 0, len);
		return bytes;
	}
	public static String readMediumString(ByteInput input) throws UnsupportedEncodingException, IOException {
		byte[] bytes = readMediumByteArray(input);
		return bytes.length == 0 ? null : new String(bytes, ENCODING);
	}
	public static byte[] readMediumByteArray(ByteInput input) throws IOException {
		int len = input.readUnsignedShort();
		byte[] bytes = new byte[len];
		input.readBytes(bytes, 0, len);
		return bytes;
	}
	public static String readLargeString(ByteInput input) throws UnsupportedEncodingException, IOException {
		byte[] bytes = readLargeByteArray(input);
		return bytes.length == 0 ? null : new String(bytes, ENCODING);
	}
	public static byte[] readLargeByteArray(ByteInput input) throws IOException {
		int len = (int)input.readUnsignedInt();
		byte[] bytes = new byte[len];
		input.readBytes(bytes, 0, len);
		return bytes;
	}

}
