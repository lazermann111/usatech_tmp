package simple.results;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import simple.io.Log;

public class DatasetHandlerSelectorFacade<E extends Exception> implements DatasetHandler<E> {
	private final static Log log = Log.getLog();
	
	private final String selectorColumnLabel;
	private final Map<String, DatasetHandler<? extends E>> handlers = new HashMap<>();
	private final DatasetHandler<? extends E> defaultHandler;
	
	private String currentSelectorValue;
	private DatasetHandler<? extends E> currentHandler;
	private String[] columnNames;
	private final Set<DatasetHandler<? extends E>> started = new LinkedHashSet<>();
	private long row = 0L;
	
	public DatasetHandlerSelectorFacade(String selectorColumnLabel) {
		this(selectorColumnLabel, null);
	}

	public DatasetHandlerSelectorFacade(String selectorColumnLabel, DatasetHandler<? extends E> defaultHandler) {
		this.selectorColumnLabel = selectorColumnLabel;
		this.defaultHandler = defaultHandler;
	}
	public String getSelectorColumnLabel() {
		return selectorColumnLabel;
	}
	
	public void addHandler(String selectorValue, DatasetHandler<? extends E> handler) {
		handlers.put(selectorValue, handler);
	}
	
	public void removeHandler(String selectorValue) {
		handlers.remove(selectorValue);
	}
	
	public DatasetHandler<? extends E> getHandler(String selectorValue) {
		DatasetHandler<? extends E> handler = handlers.get(selectorValue);
		return handler == null ? defaultHandler : handler;
	}
	
	public Map<String, DatasetHandler<? extends E>> getHandler() {
		return handlers;
	}
	
	@Override
	public void handleDatasetStart(String[] columnNames) throws E {
		this.columnNames = columnNames;
	}

	@Override
	public void handleRowStart() throws E {
		row++;
	}

	@Override
	public void handleValue(String columnName, Object value) throws E {
		if(columnName.equals(selectorColumnLabel)) {
			String selectorValue = value.toString();
			if(!selectorValue.equals(currentSelectorValue)) {
				DatasetHandler<? extends E> handler = getHandler(selectorValue);
				if(handler != null) {
					currentHandler = handler;
					if(started.add(handler))
						handler.handleDatasetStart(columnNames);
					currentHandler.handleRowStart();
				} else {
					currentHandler = null;
					log.debug("No DatasetHandler set for selector value {0}", selectorValue);
				}
				currentSelectorValue = selectorValue;
			} else if(currentHandler != null)
				currentHandler.handleRowStart();
		}
		if(currentHandler != null)
			currentHandler.handleValue(columnName, value);
	}

	@Override
	public void handleRowEnd() throws E {
		if (currentHandler != null)
			currentHandler.handleRowEnd();
	}

	@Override
	public void handleDatasetEnd() throws E {
		Iterator<DatasetHandler<? extends E>> iter = started.iterator();
		while(iter.hasNext()) {
			iter.next().handleDatasetEnd();
			iter.remove();
		}
	}

	public long getRow() {
		return row;
	}
}
