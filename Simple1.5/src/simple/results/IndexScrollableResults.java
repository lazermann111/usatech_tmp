/*
 * IndexScrollableResults.java
 *
 * Created on October 11, 2001, 11:04 AM
 */

package simple.results;

import java.beans.IntrospectionException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.Column;
import simple.util.CollectionUtils;

/** This abstract class implements many of the methods of ScrollableResults
 *  using indexed access to the column and its values. The implementations
 *  of the aggregate and grouping methods are all handled by this class.
 *
 * @author  Brian S. Krug
 * @version
 */
public abstract class IndexScrollableResults extends ColumnAbstractResults implements ScrollableResults {
	protected final List<Integer> order = new ArrayList<Integer>(); // holds the group precedence
    protected Object[][] groupValues; //holds a columns key to a set of values for each grouped column
    protected int currentRow = 0;
	protected final Aggregate[][] trackedAggregates; // this is simply to hold the aggregates requested,
    // it is not used except to implement the getAggregatesTracked() methods
    protected interface AggrCalc {
        public void eval(Object value) ;
        public Object getValue() ;
    }

    protected class MinAggrCalc implements AggrCalc {
        protected Comparable<?> min = null;
        public MinAggrCalc() {
        }
        public void eval(Object value) {
            if(value instanceof Comparable<?>)
                min = calcMin(min, (Comparable<?>)value);
        }
        public Object getValue() {
            return min;
        }
    }

    protected class MaxAggrCalc implements AggrCalc {
        protected Comparable<?> max = null;
        public MaxAggrCalc() {
        }
        public void eval(Object value) {
            if(value instanceof Comparable<?>)
                max = calcMax(max, (Comparable<?>)value);
        }
        public Object getValue() {
            return max;
        }
    }

    protected class SumAggrCalc implements AggrCalc {
        protected BigDecimal sum = null;
        public SumAggrCalc() {
        }
        public void eval(Object value) {
            if(value instanceof Number) {
                sum = calcSum(sum, (Number)value);
            }
        }
        public Object getValue() {
            return sum;
        }
    }

    protected class CountAggrCalc implements AggrCalc {
        protected int count;
        public CountAggrCalc() {
        }
        public void eval(Object value) {
            if(value != null) count++;
        }
        public Object getValue() {
            return new Integer(count);
        }
    }

    protected class AvgAggrCalc implements AggrCalc {
        protected BigDecimal sum = null;
        protected int count;
        public AvgAggrCalc() {
        }
        public void eval(Object value) {
            if(value instanceof Number) {
                sum = calcSum(sum, (Number)value);
                count++;
            }
        }
        public Object getValue() {
            if(sum == null) return null;
            return sum.divide(new BigDecimal("" + count), sum.scale() + 4, BigDecimal.ROUND_HALF_UP);
        }
    }

    protected class DistinctAggrCalc implements AggrCalc {
		protected Set<Object> distinct = new LinkedHashSet<Object>();
        public DistinctAggrCalc() {
        }
        public void eval(Object value) {
        	checkDistinctNotNull(distinct, value);
        }
        public Object getValue() {
            return distinct.size();
        }
    }

    protected class SetAggrCalc implements AggrCalc {
		protected Set<Object> distinct = new LinkedHashSet<Object>();
        public SetAggrCalc() {
        }
        public void eval(Object value) {
        	checkDistinctNotNull(distinct, value);
        }
        public Object getValue() {
            return distinct;
        }
    }

    protected class ArrayAggrCalc implements AggrCalc {
        protected List<Object> list = new ArrayList<Object>();
        public ArrayAggrCalc() {
        }
        public void eval(Object value) {
        	addValue(list, value);
        }
        public Object getValue() {
            return list.toArray();
        }
    }

    public IndexScrollableResults(Column[] columns) {
		super();
        trackedAggregates = new Aggregate[columns.length][];
		setColumns(columns, false);
    }

    public abstract boolean isValidRow(int row) ;

    public abstract int size() ;

    protected abstract Object[] getValuesAtRow(int row) throws IndexOutOfBoundsException ;

    public abstract Map<String,Object> get(int index) ;

    protected AggrCalc getAggrCalc(Aggregate aggregateType) {
        switch(aggregateType) {
            case MAX:
                return new MaxAggrCalc();
            case MIN:
                return new MinAggrCalc();
            case SUM:
                return new SumAggrCalc();
            case AVG:
                return new AvgAggrCalc();
            case COUNT:
                return new CountAggrCalc();
            case DISTINCT:
                return new DistinctAggrCalc();
            case SET:
                return new SetAggrCalc();
            case ARRAY:
                return new ArrayAggrCalc();
        }
        return null;
    }

	protected Object getRawAggregate(int column, int groupColumn, ResultsFilter filter, Aggregate aggregateType) {
    	Object[] values;
        switch(aggregateType) {
            case FIRST: {
                //go back to beginning of group and then forward until non-null
                int i = currentRow;
                if(i <= 1) {
                	if(!isValidRow(1))
                		return null;
                	i = 1;
                } else {
	                for(; i > 0; i--) {
	                    if(isGroupBeginning(groupColumn,i)) break;
	                }
                }
                if(isValidRow(i)) {
                    do {
	                	values = getValuesAtRow(i);
	                	if(filter == null || filter.isValidRow(i, values, this)) {
		                    if(values[column-1] != null) return values[column-1];
	                	}
	                } while(!isGroupEnding(groupColumn, i++)) ;
                }
                return null;
            }
            case LAST: {
                // go to ending of group and then backwords until non-null
                int i = currentRow;
                if(i <= 1) {
                	if(!isValidRow(1))
                		return null;
                	i = 1;
                } else if(!isValidRow(i)) {
            		i--;
            		while(i > 0 && !isValidRow(i)) i--;
            		if(i == 0) return null;
            	}
                while(!isGroupEnding(groupColumn,i)) i++;
            	do {
                	values = getValuesAtRow(i);
                	if(filter == null || filter.isValidRow(i, values, this)) {
	                    if(values[column-1] != null) return values[column-1];
                	}
                } while(!isGroupBeginning(groupColumn, i--)) ;
                return null;
            }
            case ARRAY: {// order is important; must start at first row
            	// find start row
                AggrCalc ac = getAggrCalc(aggregateType);
                int i = currentRow;
                if(i <= 1) {
                	if(!isValidRow(1))
                		return new Object[0];
                	i = 1;
                } else {
	                for(; i > 0; i--) {
	                    if(isGroupBeginning(groupColumn,i)) break;
	                }
                }
            	//calc aggs from start to current
                for(; i <= currentRow && isValidRow(i); i++) {
                	values = getValuesAtRow(i);
                	if(filter == null || filter.isValidRow(i, values, this))
    	                ac.eval(values[column-1]);
                }

                //continue calculating aggregate from current row forward
                if(!isGroupEnding(groupColumn, i-1)) {
                    for(; isValidRow(i); i++) {
                    	values = getValuesAtRow(i);
                    	if(filter == null || filter.isValidRow(i, values, this))
        	                ac.eval(values[column-1]);
                        if(isGroupEnding(groupColumn,i)) break;
                    }
                }

                //return the correct aggregate calculation
                return ac.getValue();
            }
        }
        //calculate aggregate from current row back
        AggrCalc ac = getAggrCalc(aggregateType);
        int i = currentRow;
        if(i <= 1) {
        	if(!isValidRow(1)) {
        		switch(aggregateType) {
        			case COUNT: case DISTINCT:
        				return 0;
        			case SET:
        				return Collections.EMPTY_SET;
        			default:
        				return null;
        		}
        	}
        	i = 1;
        } else if(!isValidRow(i)) {
        	i--;
        	while(i > 0 && !isValidRow(i)) i--;
        }

        for(; i > 0; i--) {
        	values = getValuesAtRow(i);
        	if(filter == null || filter.isValidRow(i, values, this))
                ac.eval(values[column-1]);
            if(isGroupBeginning(groupColumn,i)) break;
        }

        //continue calculating aggregate from current row forward
        i = currentRow;
        if(i <= 1) {
        	i = 1;
        }
        if(!isGroupEnding(groupColumn, i)) {
        	i++;
            for(; isValidRow(i); i++) {
            	values = getValuesAtRow(i);
            	if(filter == null || filter.isValidRow(i, values, this))
	                ac.eval(values[column-1]);
                if(isGroupEnding(groupColumn,i)) break;
            }
        }

        //return the correct aggregate calculation
        return ac.getValue();
    }

    public boolean next() {
        return isValidRow(++currentRow);
    }

    /** Implements Results interface.
     */
    public Object getValue(int column) {
        return getValueAtRow(column,currentRow);
    }

    protected Object getValueAtRow(int column, int row) throws IndexOutOfBoundsException {
    	return getValuesAtRow(row)[column-1];
    }


    public int getRow() {
        return currentRow;
    }

    public void addGroup(int groupColumn) {
        Integer key = new Integer(groupColumn);
        order.remove(key);
        order.add(key);
    }
    public int[] getGroups() {
        return ConvertUtils.toIntArray(order);
    }
    public boolean isGroupBeginning(int groupColumn) {
        return isGroupBeginning(groupColumn, currentRow);
    }

    protected boolean isGroupBeginning(int groupColumn, int row) {
        try {
            if(row == 1 && isValidRow(1)) return true;
            if(groupColumn == 0) return false;
            if(!ConvertUtils.areEqual(getValueAtRow(groupColumn, row),getValueAtRow(groupColumn, row - 1))) return true;
            for(int i = 0; i < order.size(); i++) {
                int index = order.get(i);
                if(groupColumn == index) break;
                if(!ConvertUtils.areEqual(getValueAtRow(index, row),getValueAtRow(index, row - 1))) return true;
            }
        } catch(IndexOutOfBoundsException e) {
            return false;
        }
        return false;
    }

    public boolean isGroupEnding(int groupColumn) {
        return  isGroupEnding(groupColumn, currentRow);
    }

    protected boolean isGroupEnding(int groupColumn, int row) {
        try {
            if(row == 0) return !isValidRow(row+1);
            if(!isValidRow(row)) return false;
            if(!isValidRow(row+1)) return true;
            if(groupColumn == 0) return false;
            if(!ConvertUtils.areEqual(getValueAtRow(groupColumn, row),getValueAtRow(groupColumn, row + 1))) return true;
            for(int i = 0; i < order.size(); i++) {
                int index = order.get(i);
                if(groupColumn == index) break;
                if(!ConvertUtils.areEqual(getValueAtRow(index, row),getValueAtRow(index, row + 1))) return true;
            }
        } catch(IndexOutOfBoundsException e) {
            return false;
        }
        return false;
    }

    public Object[] getGroupValues(int groupColumn) {
        if(groupValues == null) groupValues = new Object[getColumnCount() + 1][];
        if(groupValues[groupColumn] == null) {
            Set<Object> set = new TreeSet<Object>(new Comparator<Object>() {
				@SuppressWarnings({ "unchecked", "rawtypes" })
				public int compare(Object o1, Object o2) {
                    if(o1 instanceof Comparable && o2 instanceof Comparable) {
                        return ((Comparable) o1).compareTo(o2);
                    } else if(o1 == null) {
                        return (o2 == null ? 0 : 1);
                    } else if(o2 == null) {
                        return -1;
                    } else {
                        return o1.toString().compareTo(o2.toString());
                    }
                }
            }); // use sorted set so that getGroupValues() results are sorted
            for(int i = 1; isValidRow(i); i++) set.add(getValueAtRow(groupColumn,i));
            groupValues[groupColumn] = set.toArray();
        }
        return groupValues[groupColumn];
    }

    public int getGroupings(int column, int groupColumn) {
        if(!isGroupBeginning(groupColumn)) throw new IllegalStateException("Specified Group is not beginning");
        int cnt = 1;
        for(int row = getRow(); !isGroupEnding(groupColumn, row); row++) {
            if(isGroupEnding(column,row)) cnt++;
        }
        return cnt;
    }

    /** Notifies the Results Object that it must track the aggregates of specified column. This method or the method of the same
     * name with a String parameter MUST be called prior to calling getFormattedAggregate() or getAggregate(). If this method is called it
     * it must be called before next() is called.
     * @param column Index of the column whose aggregate values must be tracked
     */
    public void trackAggregate(int column) {
        trackAggregate(column, Aggregate.values());
    }

    /**
     * @see simple.results.Results#trackAggregate(int, simple.results.Results.Aggregate[])
     */
	protected void rawTrackAggregate(int column, ResultsFilter filter, Aggregate... aggregates) {
        // don't need to do anything
        //store for return by getAggregatesTracked()
    	if(filter == null) {
    		if(trackedAggregates[column-1] != null)
    			aggregates = CollectionUtils.iterativeUnion(trackedAggregates[column-1], aggregates);
    		trackedAggregates[column-1] = aggregates;
    	}
    }

    public Aggregate[] getAggregatesTracked(int column) {
        return trackedAggregates[column-1];
    }

    /** Sets the row number; 1 is the first row, etc.
     * @param row The new row
     */
    public void setRow(int row) {
        if(row >= 0) currentRow = row;
        else currentRow = Math.max(0, size() + row + 1);
    }

	public <T> T[] toBeanArray(Class<T> beanClass) throws BeanException {
        T[] beans = (T[])Array.newInstance(beanClass, size());
        fillBeanArray(beans, beanClass);
        return beans;
    }

    protected void fillBeanArray(Object[] beans, Class<?> beanClass) throws BeanException {
        for(int i = 0; i < beans.length; i++) {
            try {
                beans[i] = beanClass.newInstance();
                for(int k = 0; k < getColumnCount(); k++)
                    ReflectionUtils.setProperty(beans[i],getColumnName(k+1),getValueAtRow(k+1,i+1),true);
            } catch(IntrospectionException e) {
                throw new BeanException("While creating bean of class, " + beanClass.getName(),e);
            } catch (InstantiationException e) {
                throw new BeanException("While creating bean of class, " + beanClass.getName(),e);
            } catch (IllegalAccessException e) {
                throw new BeanException("While creating bean of class, " + beanClass.getName(),e);
            } catch (IndexOutOfBoundsException e) {
                throw new BeanException("While creating bean of class, " + beanClass.getName(),e);
            } catch (InvocationTargetException e) {
                throw new BeanException("While creating bean of class, " + beanClass.getName(),e);
            } catch (ConvertException e) {
                throw new BeanException("While creating bean of class, " + beanClass.getName(),e);
            } catch(ParseException e) {
                throw new BeanException("While creating bean of class, " + beanClass.getName(),e);
			}
        }
    }

    public ScrollableResults nextSubResult(int groupColumn) {
        if(!next()) return null;
        while(!isGroupBeginning(groupColumn)) if(!next()) return null;
        int from = getRow();
        while(!isGroupEnding(groupColumn)) if(!next()) break;
        int to = getRow();
        if(isValidRow(to)) to++; // subResults(from, to) is inclusive on from and exclusive on to
        simple.io.Log.getLog().debug("Creating Sub Result from " + from + " to " + to);
        return subResults(from, to);
    }

    public ScrollableResults subResults(int groupColumn) {
        int from = getRow();
        if(from == 0) from++;
        if(!isValidRow(from)) return null;
        for(; from > 1; from--) { // if from == 1 && isValidRow(from), then isGroupBeginning(groupColumn, from) is always true
            if(isGroupBeginning(groupColumn, from)) break;
        }
        int to = getRow();
        if(to == 0) to++;
        for(; isValidRow(to); to++) {
            if(isGroupEnding(groupColumn, to)) break;
        }
        to++; // subResults(from, to) is inclusive on from and exclusive on to
        return subResults(from, to);
    }

    public ScrollableResults subResults(String groupColumnName) {
        return subResults(getColumnIndex(groupColumnName));
    }

    //List methods
    public void clear() {
        throw new UnsupportedOperationException();
    }

    public List<Map<String,Object>> subList(int fromIndex, int toIndex) {
        return subResults(fromIndex+1, toIndex+1);
    }

    public ListIterator<Map<String,Object>> listIterator() {
        return listIterator(0);
    }

    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> T[] toArray(T[] obj) {
    	try {
        	if(obj == null) obj = (T[])new Map[size()];
        	Class<? extends T> compType = CollectionUtils.getComponentType(obj);
            if(obj.length < size()) obj = CollectionUtils.createArray(compType, size());
            if(compType == Object.class) compType = Map.class.asSubclass(compType);
            if(Map.class.isAssignableFrom(compType)) {
                for(int i = 0; i < size(); i++) {
                    Map m = get(i);
                    if(compType.isInstance(m)) Array.set(obj,i,m);
                    else {
                        if(obj[i] == null) obj[i] = compType.newInstance();
                        ((Map<?,?>)obj[i]).putAll(m);
                    }
                }
            } else if(Collection.class.isAssignableFrom(compType)) {
                for(int i = 0; i < size(); i++) {
                    Collection c = ((Map) get(i)).values();
                    if(compType.isInstance(c)) Array.set(obj,i,c);
                    else {
                        if(obj[i] == null) obj[i] = compType.newInstance();
                        ((Collection<?>)obj[i]).addAll(c);
                    }
                }
            } else if(compType.isArray()) {
                for(int i = 0; i < size(); i++) {
                    if(obj[i] == null || Array.getLength(obj[i]) < getColumnCount())
                        obj[i] = compType.cast(Array.newInstance(compType.getComponentType(),getColumnCount()));
                    for(int k = 0; k < getColumnCount(); k++) {
                        Array.set(obj[i],k,getValue(k));
                    }
                }
            } else {
                fillBeanArray(obj,compType);
            }
        } catch (BeanException e) {
            throw new IllegalArgumentException("Could not set bean properties", e);
        } catch (InstantiationException e) {
            throw new IllegalArgumentException("Could not create array", e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Could not create array", e);
        }
        return obj;
    }

    public boolean addAll(int index, Collection<? extends Map<String,Object>> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public Map<String,Object> set(int index, Map<String,Object> obj) {
        throw new UnsupportedOperationException();
    }

    public ListIterator<Map<String,Object>> listIterator(final int index) {
        return new ListIterator<Map<String,Object>>() {
            protected int cursor = index;
            public boolean hasNext() { return isValidRow(cursor+1); } //rows are 1-based
            public Map<String,Object> next() { return get(cursor++); } // get is zero-based
            public void remove() { throw new UnsupportedOperationException(); }
            public boolean hasPrevious() { return cursor > 0; }
            public Map<String,Object> previous() { return get(--cursor); }
            public int nextIndex() { return cursor; }
            public int previousIndex() { return cursor-1; }
            public void set(Map<String,Object> o) { throw new UnsupportedOperationException(); }
            public void add(Map<String,Object> o) { throw new UnsupportedOperationException(); }
        };
    }

    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean add(Map<String,Object> obj) {
        throw new UnsupportedOperationException();
    }

    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    public Map<String,Object> remove(int param) {
        throw new UnsupportedOperationException();
    }

    public int indexOf(Object obj) {
        if(!(obj instanceof Map<?,?>)) return -1;
        for(int i = 0; isValidRow(i+1); i++) if(ConvertUtils.areEqual(get(i), obj)) return i;
        return -1;
    }

    public boolean isEmpty() {
        return isValidRow(1);
    }

    public void add(int index, Map<String,Object> obj) {
        throw new UnsupportedOperationException();
    }

    public int lastIndexOf(Object obj) {
        if(!(obj instanceof Map<?,?>)) return -1;
        for(int i = size()-1; i >= 0; i--) if(ConvertUtils.areEqual(get(i), obj)) return i;
        return -1;
    }

    public boolean addAll(Collection<? extends Map<String,Object>> collection) {
        throw new UnsupportedOperationException();
    }

    public Iterator<Map<String,Object>> iterator() {
        return listIterator();
    }

    public Object[] toArray() {
        return toArray(null);
    }

    public boolean containsAll(Collection<?> collection) {
        for(Iterator<?> iter = collection.iterator(); iter.hasNext(); ) {
            if(!contains(iter.next())) return false;
        }
        return true;
    }
	
    public void close() {
        groupValues = null;
		order.clear();
    }

	@Override
	public Map<String, Object> getValuesMap() {
		return get(getRow() - 1);
	}

	@Override
	public int getRowCount() {
		return size();
	}
}
