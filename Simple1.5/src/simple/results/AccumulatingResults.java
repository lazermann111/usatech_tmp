/*
 * Created on Oct 24, 2005
 *
 */
package simple.results;

import java.beans.IntrospectionException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.db.Column;
import simple.io.Log;
import simple.util.CollectionUtils;
import simple.util.FilteredIterator;

public abstract class AccumulatingResults extends ColumnAbstractResults implements Map<String, Object> {
    private static final Log log = Log.getLog();
	protected final Map<Integer, SortedSet<Object>> groupValuesSets; // need to retain add order
	protected final Map<Tracking, Map<Integer, Accumulator>> aggregateCache; // first by agg column, then by group column
    protected int currentRow = 0;
    protected int lastReadRow = 0;
    protected boolean atEnd = false;
    protected static class Entry {
        public Entry(Object[] data, int row) {
            this.data = data;
            this.row = row;
        }
        protected Entry next;
        protected Object[] data;
        protected int row;
    }
    protected static class Tracking {
    	protected int trackedColumn;
    	protected ResultsFilter filter;
    	protected transient String text;
		public Tracking(int trackedColumn, ResultsFilter filter) {
			super();
			this.trackedColumn = trackedColumn;
			this.filter = filter;
		}
		public ResultsFilter getFilter() {
			return filter;
		}
		public int getTrackedColumn() {
			return trackedColumn;
		}
		@Override
		public boolean equals(Object obj) {
			Tracking t = (Tracking)obj;
			return trackedColumn == t.trackedColumn  && ConvertUtils.areEqual(filter, t.filter);
		}
		@Override
		public int hashCode() {
			return trackedColumn + (filter == null ? 0 : filter.hashCode());
		}
		@Override
		public String toString() {
			if(text == null)
				text = "Tracking Column " + trackedColumn + (filter == null ? "" : " with " + filter.toString());
			return text;
		}
    }
    protected Entry previousEntry;
    protected Entry lastEntry;
    protected int lastAddedGroup = 0;
    protected int groupBeginningColumn = -2;

    protected static final Iterator<Object> emptyIterator = new Iterator<Object>() {
        public boolean hasNext() {
            return false;
        }
        public Object next() {
            throw new NoSuchElementException();
        }
        public void remove() {
            throw new UnsupportedOperationException();
        }
    };

    protected static Aggregate[] getDefaultAggregates() {
        return new Aggregate[] {Aggregate.SUM, Aggregate.COUNT, Aggregate.AVG, Aggregate.MAX, Aggregate.MIN};
    }
    protected static interface Accumulator {
        public void eval(Object value, Entry entry) ;
        public Object getValue(Aggregate aggregateType) ;
        public Aggregate[] getAggregates() ;
        public void setAggregates(Aggregate[] aggregates) ;
        public GroupAccumulator getParent() ;
        public void setParent(GroupAccumulator a) ;
        public void reset() ;
    }
    protected class FilteredAccumulator implements Accumulator{
    	protected Accumulator delegate;
    	protected ResultsFilter filter;
		public FilteredAccumulator(Accumulator delegate, ResultsFilter filter) {
			super();
			this.delegate = delegate;
			this.filter = filter;
		}
		public void eval(Object value, Entry entry) {
			if(filter.isValidRow(entry.row, entry.data, AccumulatingResults.this))
				delegate.eval(value, entry);
		}
		public Aggregate[] getAggregates() {
			return delegate.getAggregates();
		}
		public GroupAccumulator getParent() {
			return delegate.getParent();
		}
		public Object getValue(Aggregate aggregateType) {
			return delegate.getValue(aggregateType);
		}
		public void reset() {
			delegate.reset();
		}
		public void setAggregates(Aggregate[] aggregates) {
			delegate.setAggregates(aggregates);
		}
		public void setParent(GroupAccumulator a) {
			delegate.setParent(a);
		}
    }
    protected static abstract class AbstractAccumulator implements Accumulator {
        protected Object first = null;
        protected Object last = null;
        protected Comparable<?> min = null;
        protected Comparable<?> max = null;
        protected BigDecimal sum = null;
        protected long count;
        protected Set<Object> distinct;
        protected List<Object> array;
        protected Aggregate[] aggregates;
        protected GroupAccumulator parent;
        protected boolean supports(Aggregate aggregateType) {
            return Arrays.binarySearch(aggregates, aggregateType) >= 0;
        }
        public Aggregate[] getAggregates() {
            return aggregates;
        }
        public void setAggregates(Aggregate[] aggregates) {
            Set<Aggregate> aggSet = new TreeSet<Aggregate>();
            for(Aggregate a : (aggregates == null ? getDefaultAggregates() : aggregates))
                if(a != null) aggSet.add(a);

            //AVG requires SUM and COUNT
            if(aggSet.contains(Aggregate.AVG)) {
                aggSet.add(Aggregate.SUM);
                aggSet.add(Aggregate.COUNT);
			} else {
				if(aggSet.contains(Aggregate.FIRST))
					aggSet.add(Aggregate.COUNT);
				if(!aggSet.contains(Aggregate.AVG) && aggSet.contains(Aggregate.COUNT) && aggSet.contains(Aggregate.SUM))
					aggSet.add(Aggregate.AVG); // COUNT + SUM implies AVG
            }
            //SET requires DISTINCT
            if(aggSet.contains(Aggregate.SET)) {
                aggSet.add(Aggregate.DISTINCT);
			} else if(aggSet.contains(Aggregate.DISTINCT))
				aggSet.add(Aggregate.SET); // DISTINCT implies SET

            this.aggregates = aggSet.toArray(new Aggregate[aggSet.size()]);

            //NOTE: Moved this from DetailAggregator to here
			if(supports(Aggregate.DISTINCT) && distinct == null)
				distinct = new LinkedHashSet<Object>();
			if(supports(Aggregate.ARRAY) && array == null)
				array = new ArrayList<Object>();
            //if(log.isDebugEnabled())
                //log.debug("Aggregates = " + aggSet);
        }
        public GroupAccumulator getParent() {
            return parent;
        }
        public void setParent(GroupAccumulator parent) {
            this.parent = parent;
        }
        public void reset() {
            first = null;
            last = null;
            min = null;
            max = null;
            sum = null;
            count = 0;
            if(distinct != null) distinct.clear();
            if(array != null) array.clear();
        }
    }
    
    protected static class DetailAccumulator extends AbstractAccumulator {
        public DetailAccumulator(Aggregate[] aggregates) {
            setAggregates(aggregates);
        }
        
        public void eval(Object value, Entry entry) {
            if(value instanceof Number) {
                for(Aggregate a : aggregates) {
                    switch(a) {
                        case SUM: sum = calcSum(sum, (Number)value); break;
                        case COUNT: count++; break;
                        case MAX: max = calcMax(max, (Comparable<?>)value); break;
                        case MIN: min = calcMin(min, (Comparable<?>)value); break;
                        case DISTINCT: 
                        	checkDistinctNotNull(distinct, value);
                        	break;
                        case ARRAY: addValue(array, value); break;
                        case LAST: last = value; break;
                        case FIRST: if(first == null) first = value; break;
                    }
                }
            } else if(value instanceof Comparable<?>) {
                for(Aggregate a : aggregates) {
                    switch(a) {
                        case COUNT: count++; break;
                        case MAX: max = calcMax(max, (Comparable<?>)value); break;
                        case MIN: min = calcMin(min, (Comparable<?>)value); break;
                        case DISTINCT: 
                        	checkDistinctNotNull(distinct, value);
                        	break;
                        case ARRAY: addValue(array, value); break;
                        case LAST: last = value; break;
                        case FIRST: if(first == null) first = value; break;
                    }
                }
            } else if(value != null) {
                for(Aggregate a : aggregates) {
                    switch(a) {
                        case COUNT: count++; break;
                        case DISTINCT: 
                        	checkDistinctNotNull(distinct, value);
                        	break;
                        case ARRAY: addValue(array, value); break;
                        case LAST: last = value; break;
                        case FIRST: if(first == null) first = value; break;
                    }
                }
            }
        }
        public Object getValue(Aggregate aggregateType) {
            if(!supports(aggregateType)) return null; // not supported
            switch(aggregateType) {
                case MAX:
                    return max;
                case MIN:
                    return min;
                case SUM:
                    return sum;
                case AVG:
                    if(sum == null) return null;
                    return sum.divide(new BigDecimal("" + count), sum.scale() + 4, BigDecimal.ROUND_HALF_UP);
                case COUNT:
                    return count;
                case DISTINCT:
                    return distinct.size();
                case SET:
                    return distinct;
                case ARRAY:
                    return array;
                case LAST:
                    return last;
                case FIRST:
                    return first;
            }
            return null;
        }
    }
    protected static class GroupAccumulator extends AbstractAccumulator {
        protected Accumulator child;
        public GroupAccumulator(Aggregate[] aggregates) {
            setAggregates(aggregates);
        }
        public Accumulator getChild() {
            return child;
        }
        public void setChild(Accumulator a) {
            child = a;
            a.setParent(this);
        }
        public void eval(Object value, Entry entry) {
            child.eval(value, entry);
        }
        public Object getValue(Aggregate aggregateType) {
            if(!supports(aggregateType)) return null; // not supported
            switch(aggregateType) {
                case MAX: {
                    Comparable<?> cmax = (Comparable<?>) child.getValue(aggregateType);
                    if(cmax == null) return max;
                    else return calcMax(max, cmax);
                }
                case MIN: {
                    Comparable<?> cmin = (Comparable<?>) child.getValue(aggregateType);
                    if(cmin == null) return min;
                    else return calcMin(min, cmin);
                }
                case SUM: {
                    Number n = (Number) child.getValue(aggregateType);
                    if(sum == null)
                        return n;
                    else if(n != null)
                        return calcSum(sum, n);
                    else
                        return sum;
                }
                case AVG: {
                    Number n = (Number) child.getValue(Aggregate.SUM);
                    if(n != null) {
                        Number cnt = (Number) child.getValue(Aggregate.COUNT);
                        BigDecimal tmp = calcSum(sum, n);
                        return tmp.divide(new BigDecimal("" + (count + cnt.longValue())), tmp.scale() + 4, BigDecimal.ROUND_HALF_UP);
                    } else
                        return sum.divide(new BigDecimal("" + count), sum.scale() + 4, BigDecimal.ROUND_HALF_UP);
                }
                case COUNT: {
                    Number cnt = (Number) child.getValue(Aggregate.COUNT);
                    if(cnt != null)
                        return count + cnt.longValue();
                    else
                        return count;
                }
                case DISTINCT: {
					Set<Object> set = new LinkedHashSet<Object>(distinct);
                    addDistinctValues(set);
                    return set.size();
                }
                case SET: {
					Set<Object> set = new LinkedHashSet<Object>(distinct);
                    addDistinctValues(set);
                    return set;
                }
                case ARRAY: {
                    List<Object> retArray = new ArrayList<Object>(array);
                    retArray.addAll(asCollection(child.getValue(aggregateType)));
                    return retArray;
                }
                case FIRST: {
                    if(first != null) return first;
                    return child.getValue(aggregateType);
                }
                case LAST: {
                    Object clast = child.getValue(aggregateType);
                    if(clast != null) return clast;
                    return last;
                }
            }
            return null;
        }
        protected void addDistinctValues(Set<Object> set) {
            if(child instanceof AbstractAccumulator) {
                set.addAll(((AbstractAccumulator)child).distinct);
                if(child instanceof GroupAccumulator)
                    ((GroupAccumulator)child).addDistinctValues(set);
            } else {
				Collection<Object> coll = asCollection(child.getValue(Aggregate.SET));
				if(coll != null)
					set.addAll(coll);
            }
        }
        public void absorbAndResetChild() {
            for(Aggregate a : aggregates) {
                switch(a) {
                    case MAX: {
                        Comparable<?> cmax = (Comparable<?>) child.getValue(a);
                        if(cmax != null) max = calcMax(max, cmax);
                        break;
                    }
                    case MIN: {
                        Comparable<?> cmin = (Comparable<?>) child.getValue(a);
                        if(cmin != null) min = calcMin(min, cmin);
                        break;
                    }
                    case SUM: {
                        BigDecimal csum = (BigDecimal) child.getValue(a);
                        if(sum == null)
                            sum = csum;
                        else if(csum != null)
                            sum = calcSum(sum, csum);
                        break;
                    }
                    case COUNT: {
                        Number cnt = (Number) child.getValue(a);
                        if(cnt != null)
                            count += cnt.longValue();
                        break;
                    }
                    case DISTINCT: {
                        addDistinctValues(distinct);
                        break;
                    }
                    case ARRAY: {
                        array.addAll(asCollection(child.getValue(a)));
                        break;
                    }
                    case FIRST: {
                        if(first == null)
                            first = child.getValue(a);
                        break;
                    }
                    case LAST: {
                        Object clast = child.getValue(a);
                        if(clast != null)
                            last = clast;
                        break;
                    }
                }
            }
            child.reset();
        }
        @Override
        public void reset() {
            child.reset();
            super.reset();
        }
    }
    public AccumulatingResults(Column[] columns) {
		super();
		groupValuesSets = new LinkedHashMap<Integer, SortedSet<Object>>(); // need to retain add order
		aggregateCache = new HashMap<Tracking, Map<Integer, Accumulator>>();
		setColumns(columns, false);
        previousEntry = new Entry(null, -1);
        previousEntry.next = new Entry(null, 0);
        lastEntry = previousEntry.next;
    }

    public void addGroup(int groupColumn) {
        boolean isNew = (null == groupValuesSets.put(groupColumn, new TreeSet<Object>(new Comparator<Object>() {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public int compare(Object o1, Object o2) {
                if(o1 instanceof Comparable && o2 instanceof Comparable) {
                    return ((Comparable) o1).compareTo(o2);
                } else if(o1 == null) {
                    return (o2 == null ? 0 : 1);
                } else if(o2 == null) {
                    return -1;
                } else {
                    return o1.toString().compareTo(o2.toString());
                }
            }
        })));
        if(isNew) {
	        //add accumulators
	        for(Map.Entry<Tracking, Map<Integer, Accumulator>> aggrEntry : aggregateCache.entrySet()) {
	            //log.debug("Created Accumulator for groupColumn " + groupColumn);
	            Accumulator a = aggrEntry.getValue().get(lastAddedGroup);
	            GroupAccumulator ga = new GroupAccumulator(a.getAggregates());
	            if(a.getParent() != null) a.getParent().setChild(ga);
	            ga.setChild(a);
	            aggrEntry.getValue().put(groupColumn, a);
	            aggrEntry.getValue().put(lastAddedGroup, ga);
	        }
	        lastAddedGroup = groupColumn;
	        if(log.isDebugEnabled())
	        	checkAggrCache();
        }
    }

    protected void checkAggrCache() {
    	Set<Integer> keys = null;
    	for(Map<Integer, Accumulator> aggrMap : aggregateCache.values()) {
    		if(keys == null)
    			keys = aggrMap.keySet();
    		else if(keys.size() != aggrMap.size()) {
    			log.debug("AggregateCache has non-matching maps! One is for " + aggrMap.keySet().toString() + " and the other for " + keys.toString(), new Throwable());
    			return;
    		}
    	}
    	if(keys != null)
    		log.debug("All maps in AggregateCache have " + keys.size() + " entries");
    }

    public int[] getGroups() {
        return ConvertUtils.toIntArray(groupValuesSets.keySet());
    }

	protected Object getRawAggregate(int column, int groupColumn, ResultsFilter filter, Aggregate aggregateType) {
        //log.debug("Aggregate " + aggregateType + " requested for column " + column);
    	Tracking tracking = new Tracking(column, filter);
        Map<Integer,Accumulator> aggrMap = aggregateCache.get(tracking);
        if(aggrMap == null) {
            log.warn("Aggregate Accumulator not found for column " + column + (filter != null ? " with filter " + filter : "")
                    + "; current totals = " + aggregateCache.keySet());
            return null;
        }
        Accumulator acc = aggrMap.get(groupColumn);
        if(acc == null) {
            log.warn("Aggregate Accumulator not found for group column " + groupColumn
                    + "; current groups = " + groupValuesSets.keySet());
            return null;
        }
        //find end of group
        Iterator<Object> iter = forwardIterator(column, groupColumn, filter);
        switch(aggregateType) {
            case MAX: {
                Comparable<?> max = (Comparable<?>)acc.getValue(aggregateType);
                while(iter.hasNext()) {
                    Object value = iter.next();
                    if(value instanceof Comparable<?>)
                        max = calcMax(max, (Comparable<?>)value);
                }
                return max;
            }
            case MIN: {
                Comparable<?> min = (Comparable<?>)acc.getValue(aggregateType);
                while(iter.hasNext()) {
                    Object value = iter.next();
                    if(value instanceof Comparable<?>)
                        min = calcMin(min, (Comparable<?>)value);
                }
                return min;
            }
            case SUM: {
                BigDecimal sum = (BigDecimal)acc.getValue(aggregateType);
                while(iter.hasNext()) {
                    Object value = iter.next();
                    if(value instanceof Number) {
                        sum = calcSum(sum, (Number)value);
                    }
                }
                return sum;
            }
            case AVG: {
                BigDecimal sum = (BigDecimal)acc.getValue(Aggregate.SUM);
                Number tmp = (Number)acc.getValue(Aggregate.COUNT);
                long count = tmp == null ? 0 : tmp.longValue();
                while(iter.hasNext()) {
                    Object value = iter.next();
                    if(value instanceof Number) {
                        sum = calcSum(sum, (Number)value);
                        count++;
                    }
                }
                if(sum == null) return null;
                return sum.divide(new BigDecimal("" + count), sum.scale() + 4, BigDecimal.ROUND_HALF_UP);
            }
            case COUNT: {
            	Number tmp = (Number)acc.getValue(aggregateType);
                long count = tmp == null ? 0 : tmp.longValue();
                while(iter.hasNext()) {
                    Object value = iter.next();
                    if(value != null) {
                        count++;
                    }
                }
                return count;
            }
            case DISTINCT: {
				Set<Object> set = new LinkedHashSet<Object>();
                if(acc instanceof AbstractAccumulator) {
                	set.addAll(((AbstractAccumulator)acc).distinct);
                	if(acc instanceof GroupAccumulator) {
                        ((GroupAccumulator)acc).addDistinctValues(set);
                    }
                } else {
                	set.addAll(asCollection(acc.getValue(Aggregate.SET)));
                }
                while(iter.hasNext()) {
                	Object value=iter.next();
                	checkDistinctNotNull(set, value);
                }
                return set.size();
            }
            case SET: {
				Set<Object> set = new LinkedHashSet<Object>();
                if(acc instanceof AbstractAccumulator) {
                	set.addAll(((AbstractAccumulator)acc).distinct);
                	if(acc instanceof GroupAccumulator) {
                        ((GroupAccumulator)acc).addDistinctValues(set);
                    }
                } else {
                	set.addAll(asCollection(acc.getValue(Aggregate.SET)));
                }
                while(iter.hasNext()) {
                	Object value=iter.next();
                	checkDistinctNotNull(set, value);
                }
                return set;
            }
            case ARRAY: {
                List<Object> list = new ArrayList<Object>();
                list.addAll(asCollection(acc.getValue(aggregateType)));
                while(iter.hasNext()) {
                    addValue(list, iter.next());
                }
                return list.toArray();
            }
            case FIRST: {
                Object first = acc.getValue(aggregateType);
                if(first != null) return first;
                while(iter.hasNext()) {
                    Object value = iter.next();
                    if(value != null)
                        return value;
                }
                return null;
            }
            case LAST: {
                Object last = null;
                while(iter.hasNext()) {
                    Object value = iter.next();
                    if(value != null)
                        last = value;
                }
                return last != null ? last : acc.getValue(aggregateType);
            }
        }

        return null;
    }
    protected static Collection<Object> asCollection(Object coll) {
    	return CollectionUtils.uncheckedCollection((Collection<?>)coll, Object.class);
    }
    protected Iterator<Object> forwardIterator(final int column, final int groupColumn, final ResultsFilter filter) {
    	if(isGroupEnding(groupColumn)) {
    		return emptyIterator;
    	}
    	if(filter == null)
        	return new Iterator<Object>() {
                protected Entry iterPreviousEntry = previousEntry.next;
                protected boolean more = true;
                public boolean hasNext() {
                    return more;
                }
                public Object next() {
                    Object data = iterPreviousEntry.next.data[column-1];
                    iterPreviousEntry = iterPreviousEntry.next;
                    more = iterPreviousEntry != null && !isGroupChanging(iterPreviousEntry, groupColumn);
                    return data;
                }
                public void remove() {
                }
            };
        else
        	return new FilteredIterator<Object,Entry>(new Iterator<Entry>() {
			            protected Entry iterPreviousEntry = previousEntry.next;
			            protected boolean more = true;
			            public boolean hasNext() {
			                return more;
			            }
			            public Entry next() {
			            	Entry entry = iterPreviousEntry.next;
			                iterPreviousEntry = iterPreviousEntry.next;
			                more = iterPreviousEntry != null && !isGroupChanging(iterPreviousEntry, groupColumn);
			                return entry;
			            }
			            public void remove() {
			            }
			        }) {
				@Override
				protected boolean accept(Entry element) {
					return filter.isValidRow(element.row, element.data, AccumulatingResults.this);
				}
				@Override
				protected Object convert(Entry element) {
					return element.data[column-1];
				}
        	};
    }

    protected boolean isGroupChanging(Entry entry, int groupColumn) {
        if(entry.next == null && !processNextRow()) return true; // the entire results is ending
        if(groupColumn == 0) return false;
        if(!groupValuesSets.containsKey(groupColumn)) {
            log.warn("isGroupChanging() requested for column " + groupColumn + " when this column was never specified as a group", new Throwable());
            return false; // this column was not specified as a group so it is never changing
        }
        for(int gc : groupValuesSets.keySet()) {
            if(!ConvertUtils.areEqual(entry.data[gc-1], entry.next.data[gc-1])) return true;
            if(gc == groupColumn) break;
        }
        return false;
    }

    public int getGroupings(int column, int groupColumn) {
        return 1;//TODO: implement this???
    }

    public Object[] getGroupValues(int groupColumn) {
        readAll();
        SortedSet<Object> ss = groupValuesSets.get(groupColumn);
        if(ss == null) return null;
        return ss.toArray();
    }

    protected void readAll() {
        while(processNextRow()) ;
    }

    public boolean next() {
        while(lastReadRow <= currentRow) {
            if(!processNextRow()) {// end of results
                if(previousEntry.next != null) {
                    // move to next entry
                    currentRow++;
                    previousEntry = previousEntry.next;
                }
                return false;
            }
        }
        //move to next entry
        currentRow++;
        previousEntry = previousEntry.next;

        //Find the first (top-most) group that is beginning and sets the groupBeginningColumn appropriately
        groupBeginningColumn = -1;
        if(previousEntry.row == 0) {
            groupBeginningColumn = 0;
        } else {
            for(Integer groupColumn : groupValuesSets.keySet()) {
                if(isGroupChanging(previousEntry, groupColumn)) {
                    groupBeginningColumn = groupColumn;
                    break;
                }
            }
        }
        if(groupBeginningColumn > 0) {
            //reset accumulators
            for(Map.Entry<Tracking,Map<Integer, Accumulator>> aggrEntry : aggregateCache.entrySet()) {
                //log.debug("Column " + groupBeginningColumn + " changed");
                Accumulator a = aggrEntry.getValue().get(groupBeginningColumn);
                try {
	                if(a.getParent() != null) a.getParent().absorbAndResetChild();
	                else a.reset();
                } catch(RuntimeException e) {
                    log.warn("While aggregating column " + groupBeginningColumn, e);
                    throw e;
                }
            }
        }
        //add to accumulators
        for(Map.Entry<Tracking, Map<Integer, Accumulator>> pair : aggregateCache.entrySet()) {
            Object data = getValue(pair.getKey().getTrackedColumn());
            Accumulator acc = pair.getValue().get(lastAddedGroup);
            acc.eval(data, previousEntry.next);
        }
        return true;
    }

    protected boolean processNextRow() {
        if(atEnd) return false;
        Object[] data = readNextRow();
        if(data == null) {// end of results
            atEnd = true;
            return false;
        } else {
            lastReadRow++;
            Entry e = new Entry(data, lastReadRow);
            lastEntry.next = e;
            lastEntry = e;
            //add to group values set
            for(Map.Entry<Integer, SortedSet<Object>> entry: groupValuesSets.entrySet()) {
                entry.getValue().add(data[entry.getKey()-1]);
            }
            return true;
        }
    }

    protected abstract Object[] readNextRow() ;

    public void trackAggregate(int column) {
        trackAggregate(column, getDefaultAggregates());
    }

    /**
     * @see simple.results.Results#trackAggregate(int, simple.results.Results.Aggregate[])
     */
	protected void rawTrackAggregate(int column, ResultsFilter filter, Aggregate... aggregates) {
    	Tracking tracking = new Tracking(column, filter);
        Map<Integer, Accumulator> aggrMap = aggregateCache.get(tracking);
        if(aggrMap == null) {
	        aggrMap = new HashMap<Integer, Accumulator>();
	        aggregateCache.put(tracking, aggrMap);
        } else {
        	Accumulator lastAcc = aggrMap.get(lastAddedGroup);
        	aggregates = CollectionUtils.iterativeUnion(lastAcc.getAggregates(), aggregates);
        	if(aggregates.length == lastAcc.getAggregates().length)
        		return;
        }
        Accumulator a = new DetailAccumulator(aggregates);
        if(filter != null)
        	a = new FilteredAccumulator(a, filter);
        if(lastAddedGroup == 0)
            aggrMap.put(0, a);
        else {
            GroupAccumulator parent = new GroupAccumulator(a.getAggregates());
            aggrMap.put(0, parent);
            for(Integer groupColumn : groupValuesSets.keySet()) {
                if(groupColumn == lastAddedGroup) {
                    parent.setChild(a);
                    aggrMap.put(groupColumn, a);
                    break;
                } else {
                    GroupAccumulator ga = new GroupAccumulator(a.getAggregates());
                    parent.setChild(ga);
                    aggrMap.put(groupColumn, ga);
                    parent = ga;
                }
            }
        }

        if(log.isDebugEnabled())
        	checkAggrCache();

        //evaluate current row if valid
        if(previousEntry != null && previousEntry.next != null && previousEntry.next.data != null)
        	a.eval(previousEntry.next.data[column-1], previousEntry.next);
    }

    public Aggregate[] getAggregatesTracked(int column) {
        Map<Integer, Accumulator> aggrMap = aggregateCache.get(column);
        if(aggrMap != null)
            return aggrMap.get(0).getAggregates();
        return null;
    }

    public void close() {
        //log.debug("Results closed", new Throwable());
		aggregateCache.clear();
		groupValuesSets.clear();
        previousEntry = null;
        lastEntry = null;
    }

    public int getRow() {
        return currentRow;
    }

	@Override
	public int getRowCount() {
		readAll();
		return lastReadRow;
	}
    public Object getValue(int column) {
        try {
            return previousEntry.next.data[column-1];
        } catch(NullPointerException e) {
            if(previousEntry == null) log.warn("Invalid state found: previousEntry is null; currentRow=" + currentRow + "; lastReadRow=" + lastReadRow);
            else if(previousEntry.next == null) log.warn("Invalid state found: previousEntry.next is null; currentRow=" + currentRow + "; lastReadRow=" + lastReadRow);
            else if(previousEntry.next.data == null) log.warn("Invalid state found: previousEntry.next.data is null; currentRow=" + currentRow + "; lastReadRow=" + lastReadRow);
            throw e;
        }
    }

    public boolean isGroupBeginning(int groupColumn) {
        if(groupBeginningColumn == -1) return false;
        if(groupBeginningColumn == 0) return true;
        if(groupBeginningColumn == groupColumn) return true;
        if(groupColumn == 0) return false;
        for(Integer column : groupValuesSets.keySet()) {
            if(column == groupBeginningColumn) return true;
            if(column == groupColumn) return false;
        }
        //this should never happen
        log.warn("Should not reach this point", new Throwable());
        return false;
    }

    public boolean isGroupEnding(int groupColumn) {
        //need to read if previousEntry.next == null
        if(previousEntry.next == null && !processNextRow()) return true; // end of results
        if(previousEntry.row == -1) {//beginning of results
        	if(previousEntry.next.next == null && !processNextRow()) return true; // end of results
        	return false;
        }
        return isGroupChanging(previousEntry.next, groupColumn);
    }

    public Results nextSubResult(int groupColumn) {
        if(!next()) return null;
        while(!isGroupBeginning(groupColumn)) if(!next()) return null;
        Entry entry = previousEntry;
        int rows = 1;
        while(!isGroupEnding(groupColumn)) if(!next()) break; else rows++;
		final int rowCount = rows;
        AccumulatingResults subResults = new AccumulatingResults(columns) {
			@Override
            protected Object[] readNextRow() {
                return null;
			}

			@Override
			public int getRowCount() {
				return rowCount;
            }
        };
        subResults.currentRow = 0;
        subResults.lastReadRow = rows; // this is so next() correct reports false when sub results is exhausted
        subResults.atEnd = true;
        subResults.previousEntry = new Entry(null, -1);
        subResults.previousEntry.next = new Entry(null, 0);
        subResults.previousEntry.next.next = entry.next;
        subResults.lastEntry = previousEntry.next;
        for(int gc : groupValuesSets.keySet())
            subResults.addGroup(gc);
        for(Map.Entry<Tracking, Map<Integer, Accumulator>> ac : aggregateCache.entrySet())
            subResults.trackAggregate(ac.getKey().getTrackedColumn(), ac.getKey().getFilter(), ac.getValue().get(0).getAggregates());
        return subResults;
    }

    public void setRow(int row) {
        if(row < currentRow) {
        	resetRows();
        	resetFields();
        }
        currentRow = row;
    }

    /** Resets the rows so that {@link #readNextRow()} returns the first row in the results
     *  This implementation just throws an IllegalArgumentException. Subclasses may over ride
     *  to support backwards navigation (though normally such navigation is costly as it
     *  requires resetting to the first row and then stepping through to the requested row)
     *  @throws UnsupportedOperationException if backwards navigation is not supported
     */
    protected void resetRows() {
    	throw new UnsupportedOperationException("This is a forward-only Results implementation - cannot set the row backwards");
    }

    protected void resetFields() {
    	lastReadRow = 0;
        atEnd = false;
        groupBeginningColumn = -2;
        previousEntry = new Entry(null, -1);
        previousEntry.next = new Entry(null, 0);
        lastEntry = previousEntry.next;

        for(SortedSet<Object> values : groupValuesSets.values())
        	values.clear();

        for(Map<Integer,Accumulator> map : aggregateCache.values())
        	for(Accumulator acc : map.values())
        		acc.reset();
    }

	public <T> T[] toBeanArray(Class<T> beanClass) throws BeanException {
        readAll();
        int len = 2 + lastReadRow - Math.max(currentRow, 2);
        T[] array = (T[])Array.newInstance(beanClass, len);
        Entry entry;
        if(previousEntry.data == null) {
            entry = previousEntry.next;
        } else {
            entry = previousEntry;
        }
        for(int i = 0; entry != null; entry = entry.next, i++) {
            try {
                array[i] = beanClass.newInstance();
            } catch(InstantiationException e) {
                throw new BeanException("While creating bean '" + beanClass.getName() + "'", e);
            } catch(IllegalAccessException e) {
                throw new BeanException("While creating bean '" + beanClass.getName() + "'", e);
            }
            int k = 1;
            try {
                for(; k <= getColumnCount(); k++)
                    ReflectionUtils.setProperty(array[i], getColumnName(k), getValue(k), true);
            } catch(IntrospectionException e) {
                throw new BeanException("While setting property, '" + getColumnName(k) + "' on bean '" + array[i] + "'", e);
            } catch (IllegalAccessException e) {
                throw new BeanException("While setting property, '" + getColumnName(k) + "' on bean '" + array[i] + "'", e);
            } catch (InvocationTargetException e) {
                throw new BeanException("While setting property, '" + getColumnName(k) + "' on bean '" + array[i] + "'", e);
            } catch (InstantiationException e) {
                throw new BeanException("While setting property, '" + getColumnName(k) + "' on bean '" + array[i] + "'", e);
            } catch (ConvertException e) {
                throw new BeanException("While setting property, '" + getColumnName(k) + "' on bean '" + array[i] + "'", e);
            } catch(ParseException e) {
                throw new BeanException("While setting property, '" + getColumnName(k) + "' on bean '" + array[i] + "'", e);
            }
        }
        return array;
    }

	@Override
	public Object remove(Object key) {
		throw new UnsupportedOperationException("Results objects are not modifiable");
	}

	@Override
	public Map<String, Object> getValuesMap() {
		return this;
	}

	protected int mapHashCode() {
		if(atEnd)
			return 0;
		int h = 0;
		for(int index = 1; index <= getColumnCount(); index++) {
			final String key = getColumnName(index);
			final Object value = getValue(index);
			h += (key == null ? 0 : key.hashCode()) ^ (value == null ? 0 : value.hashCode());
		}
		return h;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getName()).append('@').append(Integer.toHexString(hashCode())).append(' ');
		if(currentRow == 0)
			sb.append("<beginning of results>");
		else if(atEnd)
			sb.append("<end of results>");
		else {
			sb.append('{');
			for(int index = 1; index <= getColumnCount(); index++) {
				if(index > 1)
					sb.append(", ");
				final String key = getColumnName(index);
				final Object value = getValue(index);
				sb.append(key);
				sb.append('=');
				sb.append(value == this ? "(this Map)" : value);
			}
			sb.append('}');
		}
		return sb.toString();
	}
}
