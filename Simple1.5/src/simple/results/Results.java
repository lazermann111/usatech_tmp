/*
 * Results.java
 *
 * Created on October 11, 2001, 9:48 AM
 */

package simple.results;

import java.io.Closeable;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import simple.bean.ConvertException;
import simple.bean.DynaBean;

/** Provides easy access and calculations for sets of two-dimensional data. Its primary use is for formatting row, column data into
 *  some sort of report. The data is iterated over once from the first row to the last row. While the current row is valid, the values of each 
 *  column in that row as well as aggregate calculations and other grouping information are available.
 *
 *  @author  Brian S. Krug
 * @version
 */
public interface Results extends DynaBean, Cloneable, Closeable {
    public static enum Aggregate {SUM, AVG, MAX, MIN, COUNT, DISTINCT, ARRAY, FIRST, LAST, SET }
		
	/** Retrieves the column index of the column with the specified columnName.
     *  The first column has an index of 1.
	 *  @param columnName Name of the column
	 *  @return column index
	 */
	public int getColumnIndex(String columnName);
	/** Retrieves the name of the column at the specified index. The first column has an index of 1. 
	 *  @param column Index of the column
	 *  @return The name of the column
	 */
	public String getColumnName(int column);
	/** Retrieves the object in the specified column at the current row. The first column is 1.
	 *  @param column Index of the column
	 *  @return Value in column <CODE>column</CODE>.
	 */
	public Object getValue(int column);
	/** Retrieves the object in the specified column at the current row.
	 *  @param columnName Name of the column
	 *  @return Value in column <CODE>columnName</CODE>.
	 */
	public Object getValue(String columnName);
	/** Retrieves the object in the specified column at the current row converted to the requested class.
     *  The first column is 1.
	 *  @param column Index of the column
	 *  @return Value in column <CODE>column</CODE>.
	 */
    public <T> T getValue(int column, Class<T> type) throws ConvertException ;    
    /** Retrieves the object in the specified column at the current row converted to the requested class.
     *  @param columnName Name of the column
     *  @return Value in column <CODE>columnName</CODE>.
     */
    public <T> T getValue(String columnName, Class<T> type) throws ConvertException ;   
    /** Retrieves the object in the specified column at the current row formatted as specified for this column. The first column is 1.
     *  @param column Index of the column
     *  @return Value in column <CODE>column</CODE>.
     */
	public String getFormattedValue(int column);
	/** Retrieves the object in the specified column at the current row formatted as specified for this column.
	 *  @param columnName Name of the column
	 *  @return Value in column <CODE>columnName</CODE>.
	 */
	public String getFormattedValue(String columnName);
	/** Retrieves the specified aggregate of the specified column. The <CODE>groupColumn</CODE> specifies which rows to include
	 *  in the calculation. All rows in sequence that equal the object at the current row in column <CODE>groupColumn</CODE> are
	 *  included in the aggregate calculation. Use 0 as the <CODE>groupColumn</CODE> to retrieve the aggregate of a column over 
	 *  the whole result set value.
	 *  @param column Index of the column
	 *  @param groupColumn Index of the group column
	 *  @return Aggregate of the values in the column <CODE>column</CODE>.
	 */
	public String getFormattedValue(int column, String format);
	/** Retrieves the object in the specified column at the current row formatted as specified for this column.
	 *  @param columnName Name of the column
	 *  @return Value in column <CODE>columnName</CODE>.
	 */
	public String getFormattedValue(String columnName, String format);
	/** Retrieves the specified aggregate of the specified column. The <CODE>groupColumn</CODE> specifies which rows to include
	 *  in the calculation. All rows in sequence that equal the object at the current row in column <CODE>groupColumn</CODE> are
	 *  included in the aggregate calculation. Use 0 as the <CODE>groupColumn</CODE> to retrieve the aggregate of a column over 
	 *  the whole result set value.
	 *  @param column Index of the column
	 *  @param groupColumn Index of the group column
	 *  @return Aggregate of the values in the column <CODE>column</CODE>.
	 */
	public Object getAggregate(int column, int groupColumn, Aggregate aggregateType);
	/** Retrieves the specified aggregate of the specified column. The <CODE>groupColumnName</CODE> specifies which rows to include
	 *  in the calculation. All rows in sequence that equal the object at the current row in column <CODE>groupColumnName</CODE> are
	 *  included in the aggregate calculation. Use 0 as the <CODE>groupColumnName</CODE> to retrieve the aggregate of a column over 
	 *  the whole result set value.
	 *  @param columnName Name of the column used to calculate the aggregate
	 *  @param groupColumnName Name of the group column
	 *  @return Aggregate of the values in the column <CODE>columnName</CODE>.
	 */
	public Object getAggregate(String columnName, String groupColumnName, Aggregate aggregateType);
	/** Retrieves the specified aggregate of the specified column converted to the request class. 
     *  The <CODE>groupColumn</CODE> specifies which rows to include
     *  in the calculation. All rows in sequence that equal the object at the 
     *  current row in column <CODE>groupColumn</CODE> are
     *  included in the aggregate calculation. Use 0 as the <CODE>groupColumn</CODE> to 
     *  retrieve the aggregate of a column over 
     *  the whole result set value.
	 *  @param column Index of the column used to calculate the aggregate
	 *  @param groupColumn Index of the group column
     *  @param aggregateType The type of aggregation to perform
     *  @param type The class to which the aggregate is converted
	 *  @return Aggregate of the values in the column <CODE>columnName</CODE>.
	 */
    public <T> T getAggregate(int column, int groupColumn, Aggregate aggregateType, Class<T> type) throws ConvertException ;    
    /** Retrieves the specified aggregate of the specified column converted to the request class. 
     *  The <CODE>groupColumnName</CODE> specifies which rows to include
     *  in the calculation. All rows in sequence that equal the object at the 
     *  current row in column <CODE>groupColumnName</CODE> are
     *  included in the aggregate calculation. Use 0 as the <CODE>groupColumnName</CODE> 
     *  to retrieve the aggregate of a column over 
     *  the whole result set value.
     *  @param columnName Name of the column used to calculate the aggregate
     *  @param groupColumnName Name of the group column
     *  @param aggregateType The type of aggregation to perform
     *  @param type The class to which the aggregate is converted
     *  @return Aggregate of the values in the column <CODE>columnName</CODE>.
     */
    public <T> T getAggregate(String columnName, String groupColumnName, Aggregate aggregateType, Class<T> type) throws ConvertException ;   
    /** Retrieves the specified aggregate of the specified column. 
     *  The <CODE>groupColumnName</CODE> specifies which rows to include
     *  in the calculation. All rows in sequence that equal the object at the 
     *  current row in column <CODE>groupColumnName</CODE> are
     *  included in the aggregate calculation. Use 0 as the <CODE>groupColumnName</CODE> 
     *  to retrieve the aggregate of a column over 
     *  the whole result set value.
     *  @param column Index of the column used to calculate the aggregate
	 *  @param groupColumn Index of the group column
     *  @param filter The ResultsFilter to use
     *  @param aggregateType The type of aggregation to perform
     *  @param type The class to which the aggregate is converted
     *  @return Aggregate of the values in the column <CODE>columnName</CODE>.
     */    
    public Object getAggregate(int column, int groupColumn, ResultsFilter filter, Aggregate aggregateType) ;
    /** Retrieves the specified aggregate of the specified column. 
     *  The <CODE>groupColumnName</CODE> specifies which rows to include
     *  in the calculation. All rows in sequence that equal the object at the 
     *  current row in column <CODE>groupColumnName</CODE> are
     *  included in the aggregate calculation. Use 0 as the <CODE>groupColumnName</CODE> 
     *  to retrieve the aggregate of a column over 
     *  the whole result set value.
     *  @param columnName Name of the column used to calculate the aggregate
     *  @param groupColumnName Name of the group column
     *  @param filter The ResultsFilter to use
     *  @param aggregateType The type of aggregation to perform
     *  @param type The class to which the aggregate is converted
     *  @return Aggregate of the values in the column <CODE>columnName</CODE>.
     */
    public Object getAggregate(String columnName, String groupColumnName, ResultsFilter filter, Aggregate aggregateType) ;
    /** Retrieves the specified aggregate of the specified column converted to the request class. 
     *  The <CODE>groupColumnName</CODE> specifies which rows to include
     *  in the calculation. All rows in sequence that equal the object at the 
     *  current row in column <CODE>groupColumnName</CODE> are
     *  included in the aggregate calculation. Use 0 as the <CODE>groupColumnName</CODE> 
     *  to retrieve the aggregate of a column over 
     *  the whole result set value.
     *  @param column Index of the column used to calculate the aggregate
	 *  @param groupColumn Index of the group column
     *  @param filter The ResultsFilter to use
     *  @param aggregateType The type of aggregation to perform
     *  @param type The class to which the aggregate is converted
     *  @return Aggregate of the values in the column <CODE>columnName</CODE>.
     */    
    public <T> T getAggregate(int column, int groupColumn, ResultsFilter filter, Aggregate aggregateType, Class<T> type) throws ConvertException ;
    /** Retrieves the specified aggregate of the specified column converted to the request class. 
     *  The <CODE>groupColumnName</CODE> specifies which rows to include
     *  in the calculation. All rows in sequence that equal the object at the 
     *  current row in column <CODE>groupColumnName</CODE> are
     *  included in the aggregate calculation. Use 0 as the <CODE>groupColumnName</CODE> 
     *  to retrieve the aggregate of a column over 
     *  the whole result set value.
     *  @param columnName Name of the column used to calculate the aggregate
     *  @param groupColumnName Name of the group column
     *  @param filter The ResultsFilter to use
     *  @param aggregateType The type of aggregation to perform
     *  @param type The class to which the aggregate is converted
     *  @return Aggregate of the values in the column <CODE>columnName</CODE>.
     */
    public <T> T getAggregate(String columnName, String groupColumnName, ResultsFilter filter, Aggregate aggregateType, Class<T> type) throws ConvertException ;
    /** Retrieves the specified aggregate of the specified column formatted as specified for the column. The <CODE>groupColumnName</CODE> specifies which rows to include
     *  in the calculation. All rows in sequence that equal the object at the current row in column <CODE>groupColumnName</CODE> are
     *  included in the aggregate calculation. Use 0 as the <CODE>groupColumnName</CODE> to retrieve the aggregate of a column over 
     *  the whole result set value.
     *  @param columnName Name of the column used to calculate the aggregate
     *  @param groupColumnName Name of the group column
     *  @param aggregateType The type of aggregation to perform
     *  @return Aggregate of the values in the column <CODE>columnName</CODE>.
     */
    public String getFormattedAggregate(String columnName, String groupColumnName, Aggregate aggregateType);
	/** Retrieves the specified aggregate of the specified column formatted as specified for the column. The <CODE>groupColumn</CODE> specifies which rows to include
	 *  in the calculation. All rows in sequence that equal the object at the current row in column <CODE>groupColumn</CODE> are
	 *  included in the aggregate calculation. Use 0 as the <CODE>groupColumn</CODE> to retrieve the aggregate of a column over 
	 *  the whole result set value.
	 *  @param column Index of the column
	 *  @param groupColumn Index of the group column
	 *  @param aggregateType The type of aggregation to perform
     *  @return Aggregate of the values in the column <CODE>column</CODE>.
	 */
	public String getFormattedAggregate(int column, int groupColumn, Aggregate aggregateType);
    /** Retrieves the specified aggregate of the specified column formatted as specified for the column. The <CODE>groupColumnName</CODE> specifies which rows to include
     *  in the calculation. All rows in sequence that equal the object at the current row in column <CODE>groupColumnName</CODE> are
     *  included in the aggregate calculation. Use 0 as the <CODE>groupColumnName</CODE> to retrieve the aggregate of a column over 
     *  the whole result set value.
     *  @param columnName Name of the column used to calculate the aggregate
     *  @param groupColumnName Name of the group column
     *  @param filter The ResultsFilter to use
     *  @param aggregateType The type of aggregation to perform
     *  @return Aggregate of the values in the column <CODE>columnName</CODE>.
     */
    public String getFormattedAggregate(String columnName, String groupColumnName, ResultsFilter filter, Aggregate aggregateType);
	/** Retrieves the specified aggregate of the specified column formatted as specified for the column. The <CODE>groupColumn</CODE> specifies which rows to include
	 *  in the calculation. All rows in sequence that equal the object at the current row in column <CODE>groupColumn</CODE> are
	 *  included in the aggregate calculation. Use 0 as the <CODE>groupColumn</CODE> to retrieve the aggregate of a column over 
	 *  the whole result set value.
	 *  @param column Index of the column
	 *  @param groupColumn Index of the group column
	 *  @param filter The ResultsFilter to use
     *  @param aggregateType The type of aggregation to perform
     *  @return Aggregate of the values in the column <CODE>column</CODE>.
	 */
	public String getFormattedAggregate(int column, int groupColumn, ResultsFilter filter, Aggregate aggregateType);
    /** Retrieves the aggregates (if any) being tracked at the specified column.
     *  @param column Index of the column of interest
     *  @return an array of aggregates being tracked or null if none are tracked
     */ 
    public Aggregate[] getAggregatesTracked(int column);
    /** Retrieves the aggregates (if any) being tracked at the specified column.
     *  @param columnName Name of the column of interest
     *  @return an array of aggregates being tracked or null if none are tracked
     */ 
    public Aggregate[] getAggregatesTracked(String columnName);
    /** Notifies the Results Object that it must track the aggregates of specified column. This method or the method of the same
	 *  name with a String parameter MUST be called prior to calling getFormattedAggregate() or getAggregate(). If this method is called it
	 *  it must be called before next() is called.
	 *  @param column Index of the column whose aggregate values must be tracked
	 */	
	public void trackAggregate(int column);
    /** Notifies the Results Object that it must track the specified aggregates of specified column. This method or the method of the same
     *  name with a String parameter MUST be called prior to calling getFormattedAggregate() or getAggregate(). If this method is called it
     *  it must be called before next() is called.
     *  @param column Index of the column whose aggregate values must be tracked
     *  @param aggregates Array of aggregates that are to be tracked
     */ 
    public void trackAggregate(int column, Aggregate... aggregates);
    /** Notifies the Results Object that it must track the specified aggregates of specified column using the given ResultsFilter.
     *  This method or the method of the same
     *  name with a String parameter MUST be called prior to calling getFormattedAggregate() or getAggregate(). 
     *  Rows prior to the current row may be ignored if this method is called after next()
     *  @param column Index of the column whose aggregate values must be tracked
     *  @param fitler The ResultsFilter that weeds out unwanted rows
     *  @param aggregates Array of aggregates that are to be tracked
     */ 
    public void trackAggregate(int column, ResultsFilter filter, Aggregate... aggregates) ;       
    /** Notifies the Results Object that it must track the aggregates of specified column. This method or the method of the same
     *  name with a int parameter MUST be called prior to calling getFormattedAggregate() or getAggregate(). If this method is called,
     *  it must be called before next() is called.
     *  @param columnName Name of the column whose aggregate values must be tracked
     */ 
    public void trackAggregate(String columnName);
    /** Notifies the Results Object that it must track the specified aggregates of specified column. This method or the method of the same
	 *  name with a int parameter MUST be called prior to calling getFormattedAggregate() or getAggregate(). If this method is called,
	 *  it must be called before next() is called.
	 *  @param columnName Name of the column whose aggregate values must be tracked
	 *  @param aggregates Array of aggregates that are to be tracked
     */	
	public void trackAggregate(String columnName, Aggregate... aggregates);
    /** Notifies the Results Object that it must track the specified aggregates of specified column using the given ResultsFilter.
     *  This method or the method of the same
     *  name with a String parameter MUST be called prior to calling getFormattedAggregate() or getAggregate(). 
     *  Rows prior to the current row may be ignored if this method is called after next()
     *  @param columnName Name of the column whose aggregate values must be tracked
     *  @param fitler The ResultsFilter that weeds out unwanted rows
     *  @param aggregates Array of aggregates that are to be tracked
     */ 
    public void trackAggregate(String columnName, ResultsFilter filter, Aggregate... aggregates) ;
	/** Notifies the Results Object that the specified column is sorted and its grouping must be tracked. This method or the method of the same
	 *  name with a String parameter MUST be called prior to calling isGroupBeginning() or isGroupEnding() or getGroupValues() or
	 *  getFormattedGroupValues() or getGroupings(). If this method is called, it must be called before next() is called. The order in which
	 *  groups are added should match the order in which they are sorted. The first column added as a group is assumed to group on the
	 *  whole row set. The next column added is assumed to group within the first group column. And so on.
	 *  @param groupColumn Index of the column whose groupings must be tracked
	 */	
	public void addGroup(int groupColumn);
	/** Notifies the Results Object that the specified column is sorted and its grouping must be tracked. This method or the method of the same
	 *  name with a String parameter MUST be called prior to calling isGroupBeginning() or isGroupEnding() or getGroupValues() or
	 *  getGroupings(). If this method is called, it must be called before next() is called. The order in which
	 *  groups are added should match the order in which they are sorted. The first column added as a group is assumed to group on the
	 *  whole row set. The next column added is assumed to group within the first group column. And so on.
	 *  @param groupColumnName Name of the column whose groupings must be tracked
	 */	
	public void addGroup(String groupColumnName);
    /** Returns the list of column indexes that this results object is grouping by
     * @return
     */
    public int[] getGroups() ;
    /** Returns the list of column names that this results object is grouping by
     * @return
     */
    public String[] getGroupNames() ;
	/** Returns whether a group of same values is beginning at the current row for the specified column. If the value of the specified
	 *  column at the current  row does not equal the value in the previous row <tt>TRUE</tt> is returned. Also, if any group added
	 *  before the specified group is beginning then <tt>TRUE</tt>.This method returns inconsistent results if addGroup() for the
	 *  specified column has not been previously called.
	 *  @param groupColumn Index of the column to be tested for a new group 
	 *  @return Whether the value in the current row of the column <CODE>groupColumn</CODE> is beginning a new group for that column.
	 */		
	public boolean isGroupBeginning(int groupColumn);
	/** Returns whether a group of same values is beginning at the current row for the specified column. If the value of the specified
	 *  column at the current  row does not equal the value in the previous row <tt>TRUE</tt> is returned. Also, if any group added
	 *  before the specified group is beginning then <tt>TRUE</tt>.This method returns inconsistent results if addGroup() for the
	 *  specified column has not been previously called.
	 *  @param groupColumnName Name of the column to be tested for a new group 
	 *  @return Whether the value in the current row of the column <CODE>groupColumnName</CODE> is beginning a new group for that column.
	 */		
	public boolean isGroupBeginning(String groupColumnName);
	
	/**
	 * Returns whether a group of same values is ending at the current row for the specified column. If the value of the specified
	 * column at the current row does not equal the value in the next row <tt>TRUE</tt> is returned. Also, if any group added
	 * before the specified group is ending then <tt>TRUE</tt>.This method returns inconsistent results if addGroup() for the
	 * specified column has not been previously called. When the current row is 0 this will return <tt>TRUE</tt> only when there are no rows in the Results.
	 * 
	 * @param groupColumn
	 *            Index of the column to be tested for the end of the current group
	 * @return Whether the value in the current row of the column <CODE>groupColumn</CODE> is ending a new group for that column.
	 */		
	public boolean isGroupEnding(int groupColumn);
	/** Returns whether a group of same values is ending at the current row for the specified column. If the value of the specified
	 *  column at the current  row does not equal the value in the next row <tt>TRUE</tt> is returned. Also, if any group added
	 *  before the specified group is ending then <tt>TRUE</tt>.This method returns inconsistent results if addGroup() for the
	 *  specified column has not been previously called.
	 *  @param groupColumnName Name of the column to be tested for the end of the current group 
	 *  @return Whether the value in the current row of the column <CODE>groupColumnName</CODE> is ending a new group for that column.
	 */		
	public boolean isGroupEnding(String groupColumnName);
	/**  Moves to the next row and returns true if the row is valid
	 *  @return Whether the new current row is valid.
	 */
	public boolean next();
    /**  Gathers all the rows in the next group as specified by the groupColumn
     *   and returns a sub Results that contains those rows. Then, sets the current
     *   row on the original Results object to the last row in the sub
     *   Results object being returned.
     *  @param groupColumn The index of the column of which the returned sub Results
     *  object represents one value
     *  @return The next sub Result or null if no more sub Results exist.
     */
    public Results nextSubResult(int groupColumn);
    /**  Gathers all the rows in the next group as specified by the groupColumnName
     *   and returns a sub Results that contains those rows. Then, sets the current
     *   row on the original Results object to the last row in the sub
     *   Results object being returned.
     *  @param groupColumnName The name of the column of which the returned sub Results
     *  object represents one value
     *  @return The next sub Result or null if no more sub Results exist.
     */
    public Results nextSubResult(String groupColumnName);
    /** Returns the row number; 1 is the first row, etc.
	 *  @return The current row number.
	 */
	public int getRow();
	/** Sets the row number; 1 is the first row, etc.
	 *  @param row The new row
	 */
	public void setRow(int row);
	/** Returns the number of columns in this Results.
	 *  @return The number of columns.
	 */	
	public int getColumnCount();
	/** Retrieves the class of the objects returned from the specified column 
	 *  @param column Index of the column
	 *  @return The class of objects in this column
	 */
	public Class<?> getColumnClass(int column);
	/** Retrieves the class of the objects returned from the specified column 
	 *  @param columnName Name of the column
	 *  @return The class of objects in this column
	 */
	public Class<?> getColumnClass(String columnName) ;
	/** Retrieves an array of all unique values found in the specified column. The specified column must be added as a group
	 *  prior to calling this method.
	 *  @param groupColumn Index of the column
	 *  @return An array of unique objects in this column
	 */	
	public Object[] getGroupValues(int groupColumn);
	/** Retrieves an array of all unique values found in the specified column. The specified column must be added as a group
	 *  prior to calling this method.
	 *  @param groupColumnName Name of the column
	 *  @return An array of unique objects in this column
	 */	
	public Object[] getGroupValues(String groupColumnName);
	/** Retrieves an array of all unique values found in the specified column formatted as specified. The specified column must be added as a group
	 *  prior to calling this method.
	 *  @param groupColumn Index of the column
	 *  @return An array of unique objects in this column
	 */	
	public String[] getFormattedGroupValues(int groupColumn);
	/** Retrieves an array of all unique values found in the specified column formatted as specified. The specified column must be added as a group
	 *  prior to calling this method.
	 *  @param groupColumnName Name of the column
	 *  @return An array of unique objects in this column
	 */	
	public String[] getFormattedGroupValues(String groupColumnName);
	/** Retrieves the number of times the specified column changes groups within the current group specified by <tt>groupColumn</tt>.
	 *  This method must be called when isGroupBeginning() returns <tt>TRUE</tt> for the specified group column.The specified group column 
	 *  must be added as a group prior to calling this method.
	 *  @param column Index of the column
	 *  @param groupColumn Index of the group column
	 *  @return Number of rows for which isGroupBeginning() returns <tt>TRUE</tt> for the specified column and isGroupEnding() 
	 *  returns <tt>FALSE</tt> for the specified group column
	 */	
	public int getGroupings(int column, int groupColumn) ;
	/** Retrieves the number of times the specified column changes groups within the current group specified by <tt>groupColumnName</tt>.
	 *  This method must be called when isGroupBeginning() returns <tt>TRUE</tt> for the specified group column.The specified group column 
	 *  must be added as a group prior to calling this method.
	 *  @param columnName Name of the column
	 *  @param groupColumnName Name of the group column
	 *  @return Number of rows for which isGroupBeginning() returns <tt>TRUE</tt> for the specified column and isGroupEnding() 
	 *  returns <tt>FALSE</tt> for the specified group column
	 */	
	public int getGroupings(String columnName, String groupColumnName) ;
	/** Retrieves the percent as a decimal that equals the current value in the specified column divided by the aggregate of the specified column over the whole column group.
	 *  @param valueColumnName Name of the column whose percent is calculated
	 *  @param wholeColumnName Name of the group column whose aggregate represents the 100% value
	 *  @param aggregateType Type of aggregate used to calculate the 100% value
	 *  @return Ratio of the value in the specified column to the aggregate value of that column over the specified whole column group
	 */
	public java.math.BigDecimal getPercent(String valueColumnName, String wholeColumnName, Aggregate aggregateType) ;
	/** Retrieves the percent as a decimal that equals the aggregate value of the specified column over the specified value group column
	 *  divided by the aggregate of the specified column over the whole column group.
	 *  @param valueColumnName Name of the column whose percent is calculated
	 *  @param valueGroupColumnName Name of the group column over which the dividend value is calculated
	 *  @param wholeColumnName Name of the group column whose aggregate represents the 100% value
	 *  @param aggregateType Type of aggregate used to calculate the 100% value
	 *  @return Ratio of the aggregate value of the specified value column over the specified value group column to the aggregate value 
	 *  of that column over the specified whole column group
	 */
	public java.math.BigDecimal getPercent(String valueColumnName, String valueGroupColumnName, String wholeColumnName, Aggregate aggregateType) ;
	/** Retrieves the percent as a decimal that equals the current value in the specified column divided by the aggregate of the specified 
	 *  column over the whole column group.
	 *  @param valueColumn Index of the column whose percent is calculated
	 *  @param wholeColumn Index of the group column whose aggregate represents the 100% value
	 *  @param aggregateType Type of aggregate used to calculate the 100% value
	 *  @return Ratio of the value in the specified column to the aggregate value of that column over the specified whole column group
	 */
	public java.math.BigDecimal getPercent(int valueColumn, int wholeColumn, Aggregate aggregateType) ;
	/** Retrieves the percent as a decimal that equals the aggregate value of the specified column over the specified value group column
	 *  divided by the aggregate of the specified column over the whole column group.
	 *  @param valueColumn Index of the column whose percent is calculated
	 *  @param valueGroupColumn Index of the group column over which the dividend value is calculated
	 *  @param wholeColumn Index of the group column whose aggregate represents the 100% value
	 *  @param aggregateType Type of aggregate used to calculate the 100% value
	 *  @return Ratio of the aggregate value of the specified value column over the specified value group column to the aggregate value 
	 *  of that column over the specified whole column group
	 */
	public java.math.BigDecimal getPercent(int valueColumn, int valueGroupColumn, int wholeColumn, Aggregate aggregateType) ;
    /** Turns the results into a list of the specified bean setting any properties on the
     *  bean to the value in the corresponding Results column.
     */
    public <T> T[] toBeanArray(Class<T> beanClass) throws BeanException ;  
    
    /** Sets any properties on the
     *  bean to the value in the corresponding Results column at the current row.
     *  @param bean The bean to fill (See ReflectionUtils.setProperty method)
     */
    public void fillBean(Object bean) throws BeanException; 
    
    /** Returns the format String for this column or null if no format is specified. See
     *  ConvertUtils.getFormat for details on how the format String is translated into
     *  a java.text.Format object
     * @param column The index of the column for which the format is being retrieved
     * @return The format String
     */
    public String getFormat(int column);
    /** Returns the format Strings for each column or null if no format is specified in an array. See
     *  ConvertUtils.getFormat for details on how the format String is translated into
     *  a java.text.Format object
     * @return An array of the format Strings of each column
     */
    public String[] getFormats();
    /** Sets the format String for this column. See
     *  ConvertUtils.getFormat for details on how the format String is translated into
     *  a java.text.Format object
     * @param column The index of the column for which the format is being set
     * @param format The format String
     */
    public void setFormat(int column, String format);
    /** Sets the format String for this column. See
     *  ConvertUtils.getFormat for details on how the format String is translated into
     *  a java.text.Format object
     * @param columnName The name of the column for which the format is being set
     * @param format The format String
     */
    public void setFormat(String columnName, String format);
    /** Sets the format Strings for each column. See
     *  ConvertUtils.getFormat for details on how the format String is translated into
     *  a java.text.Format object
     * @param formats An array of the format Strings for each column
     */
    public void setFormats(String[] formats);
    
    /** Creates a copy of this Results object. The new Results object should have its current row set to zero.
     * @return The copy of this object.
     */
    public Results clone() ;
    
    /** Closes the results (allows any necessary clean-up)
     * 
     */
    public void close() ;

	/**
	 * Provides a Map implementation of the values of this Results object. The column names are the map keys and the current data values are the map values.
	 * 
	 * @return
	 */
	public Map<String, Object> getValuesMap();

	/**
	 * Returns the column names as a Set
	 * 
	 * @return
	 */
	public Set<String> keySet();

	/**
	 * Returns the number of rows in this results object. This may require reading the entire resultset so
	 * use this sparingly.
	 * 
	 * @return
	 */
	public int getRowCount();

	/**
	 * Gets the locale used for formatting values
	 * 
	 * @return The locale used for formatting values
	 */
	public Locale getLocale();

	/**
	 * Sets the locale used for formatting values
	 * 
	 * @param locale
	 */
	public void setLocale(Locale locale);

	/**
	 * Returns the index of the column that is unique for the given column in order to protect against multiple count or sum of the same row
	 * 
	 * @param columnIndex
	 * @return
	 */
	public int getUniqueColumnIndexFor(int columnIndex);

	/**
	 * Sets the index of the column that is unique for the given column in order to protect against multiple count or sum of the same row
	 * 
	 * @param columnIndex
	 * @param uniqueColumnIndex
	 */
	public void setUniqueColumnIndexFor(int columnIndex, int uniqueColumnIndex);

	/**
	 * Returns the name of the column that is unique for the given column in order to protect against multiple count or sum of the same row
	 * 
	 * @param columnName
	 * @return
	 */
	public String getUniqueColumnNameFor(String columnName);

	/**
	 * Sets the name of the column that is unique for the given column in order to protect against multiple count or sum of the same row
	 * 
	 * @param columnName
	 * @param uniqueColumnName
	 */
	public void setUniqueColumnNameFor(String columnName, String uniqueColumnName);

}

