/*
 * ResultsStack.java
 *
 * Created on May 4, 2004, 8:07 AM
 */

package simple.results;

/**
 *
 * @author  Brian S. Krug
 */
public class ResultsStack {
    protected static class Entry {
        public Results results;
        public Entry parent;
    }
    protected Entry top = null;
    
    /** Creates a new instance of ResultsStack */
    public ResultsStack() {
    }
    
    public void add(Results results) {
        if(top == null) 
            top = new Entry();
        else {
            Entry newTop = new Entry();
            newTop.parent = top;
            top = newTop;
        }
        top.results = results;
    }
    
    public void pop() {
        top = top.parent;
    }
    
    public boolean next() {
        return top.results.next();
    }
    
    public Object getValue(String name) {
        for(Entry entry = top; entry != null; entry = entry.parent) {
            if(entry.results.getColumnIndex(name) > 0) return entry.results.getValue(name);
        }
        throw new IllegalArgumentException("Could not find column '" + name + "' in any of the results");
    }
}
