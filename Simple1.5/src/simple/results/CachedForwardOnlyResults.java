/*
 * Created on Dec 6, 2005
 *
 */
package simple.results;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import simple.db.Column;
import simple.io.Log;
import simple.results.CachedResultsCreator.EOFMarker;

public class CachedForwardOnlyResults extends AccumulatingResults {
    private static final Log log = Log.getLog();
    protected InputStream resettableStream;
    protected ObjectInputStream cachedStream;
    protected IOException exception;
    protected File cacheFile; // hold onto this so it's not gc'ed and the file deleted
    protected final int rows;
    protected int compression;

    public CachedForwardOnlyResults(Column[] columns, File cacheFile, int rows) throws FileNotFoundException, IOException {
        this(columns, cacheFile, rows, 0);
    }

    public CachedForwardOnlyResults(Column[] columns, File cacheFile, int rows, int compression) throws FileNotFoundException, IOException {
        this(columns, createCachedStream(cacheFile, compression), rows);
        this.cacheFile = cacheFile;
        this.compression = compression;
    }

    public CachedForwardOnlyResults(Column[] columns, InputStream cacheStream, int rows) throws IOException {
        this(columns, new ObjectInputStream(cacheStream), rows);
        if(cacheStream.markSupported()) {
        	cacheStream.mark(8192);
        	resettableStream = cacheStream;
        }
    }

    public CachedForwardOnlyResults(Column[] columns, ObjectInputStream cacheStream, int rows) {
        super(columns);
        this.cachedStream = cacheStream;
        this.rows = rows;
    }

    @Override
    protected Object[] readNextRow() {
        try {
            if(rows >= 0 && lastReadRow >= rows) return null;
            Object o = cachedStream.readUnshared();
            try {
                Object[] row = (Object[]) o;
                if(lastReadRow - 1 == rows && rows >= 0) {
                    log.debug("Reached end of file");
                    closeCachedStream();
                    atEnd = true;
                }
                return row;
            } catch(ClassCastException e) {
                if(o instanceof EOFMarker) {
                    log.debug("Read end of file marker at row " + lastReadRow + " while preset size = " + rows);
                    closeCachedStream();
                    atEnd = true;
                    return null;
                } else
                    throw e;
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        } catch(ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void resetRows() {
    	closeCachedStream();
    	if(cacheFile != null) {
    		try {
				cachedStream = createCachedStream(cacheFile, compression);
			} catch(IOException e) {
				throw new UnsupportedOperationException("Could not read from file '" + cacheFile.getAbsolutePath() + "' to reset rows for Results object");
			}
    	} else if(resettableStream != null) {
    		try {
				resettableStream.reset();
	        	cachedStream = new ObjectInputStream(resettableStream);
			} catch(IOException e) {
				throw new UnsupportedOperationException("Could not reset input stream to reset rows for Results object");
			}
    	} else {
        	super.resetRows();
    	}
    }

    public static ObjectInputStream createCachedStream(File cacheFile, int compression) throws IOException {
		// Performance analysis indicates that the optimal buffer sizes are 512 (the default) for the Inflater and 1024 for the BufferedInputStream
		InputStream in = new FileInputStream(cacheFile);
		if(compression > 0)
			in = new InflaterInputStream(in, new Inflater(), 512);
		in = new BufferedInputStream(in, 1024);
		return new ObjectInputStream(in);
	}

	@Override
    public void close() {
    	super.close();
    	closeCachedStream();
    	resettableStream = null;
        cacheFile = null;
    }
	protected void closeCachedStream() {
		if(cachedStream != null)
			try {
				cachedStream.close();
			} catch(IOException e) {
				log.warn("Could not close input stream", e);
			}
    	cachedStream = null;
	}
}
