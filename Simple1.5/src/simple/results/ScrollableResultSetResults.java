/*
 * ScrollableResultSetResults.java
 *
 * Created on January 24, 2003, 11:47 AM
 */

package simple.results;

import java.lang.reflect.UndeclaredThrowableException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import simple.bean.ConvertException;
import simple.db.Column;
import simple.db.DBHelper;
import simple.util.SlidingArray;

/** Implementation of IndexScrollableResults that uses a scrollable ResultSet as the
 *  data and an array of Strings as the column names. Caches KeyArrayMap objects
 *  for each row when requested it is requested. The cache is a sliding window
 *  of these KeyArray Map objects. When a row is request beyond the scope of the window,
 *  some of objects in the KeyArrayMap are removed from the cache so the new ones can
 *  be added.
 *
 * @author  Brian S. Krug
 * @version
 */
public class ScrollableResultSetResults extends IndexScrollableResults {
	protected final ResultSet rs;
    protected SlidingArray<KeyArrayMap> maps;
    protected int allocateSize = 256;
    protected int size = -1;
	protected final DBHelper dbHelper;
	protected final ReentrantLock lock = new ReentrantLock(); // make this thread-safe
    protected int offset;
    protected int length;
    protected transient ResultsCreator resultsCreator;

    /** Creates new ScrollableResultSetResults */
    public ScrollableResultSetResults(Column[] columns, ResultSet rs, DBHelper dbHelper) {
        this(columns, rs, dbHelper, 0, -1);
    }

    /** Creates new ScrollableResultSetResults */
    protected ScrollableResultSetResults(Column[] columns, ResultSet rs, DBHelper dbHelper, int offset, int length) {
        super(columns);
        this.rs = rs;
        this.maps = new SlidingArray<KeyArrayMap>(allocateSize, KeyArrayMap.class);
        this.dbHelper = dbHelper;
        this.offset = offset;
        this.length = length;
    }

    public Map<String,Object> get(int index) {
        return getMapAt(index);
    }

    public KeyArrayMap getMapAt(int index) {
        index += offset;
        KeyArrayMap kam;
        try {
            kam = maps.get(index);
            if(kam != null) return kam;
        } catch(ArrayIndexOutOfBoundsException e) {
            // do nothing
        }
        lock.lock();
        try {
            if(rs.absolute(index+1)) {
                Object[] values = dbHelper.getValues(rs, columns, true);
                kam = new KeyArrayMap(columnNamesMap, values);
                maps.set(index, kam);
            } else {
                return null;
            }
        } catch(SQLException e) {
            handleException(e);
            return null; // this statement is never run because handleException throws a runtime exception
        } catch(ConvertException e) {
            handleException(e);
            return null; // this statement is never run because handleException throws a runtime exception
		} finally {
            lock.unlock();
        }
        return kam;
    }

    public int size() {
        // warning: this requires scrolling to the last row in the result set - could be costly
        if(size == -1) {
            lock.lock();
            try {
                if((length > -1 && rs.absolute(offset + length)) || rs.last())
                    size = Math.max(0, rs.getRow() - offset);
                else
                    size = 0;
            } catch(SQLException e) {
                handleException(e);
            } finally {
                lock.unlock();
            }
        }
        return size;
    }

    protected Object[] getValuesAtRow(int row) throws IndexOutOfBoundsException {
        return getMapAt(row-1).getValues();
    }

    protected void handleException(SQLException e) {
        throw new UndeclaredThrowableException(e, "SQL Exception while accessing ResultSet");
    }
    protected void handleException(ConvertException e) {
        throw new UndeclaredThrowableException(e, "Convert Exception while accessing ResultSet");
    }
    public boolean isValidRow(int row) {
        if(row < 1) return false;
        if(size > -1) return row <= size;
        if(length > -1 && row - offset > length) return false;
        lock.lock();
        try {
            return rs.absolute(row + offset);
        } catch(SQLException e) {
            return false;
        } finally {
            lock.unlock();
        }
    }

    public ScrollableResults subResults(int fromRow, int toRow) {
        ScrollableResultSetResults srsr = (ScrollableResultSetResults)this.clone();
        srsr.offset = fromRow - 1;
        srsr.length = toRow - fromRow;
        srsr.currentRow = 0;
        srsr.groupValues = null;
        return null;
    }

    public void close() {
        try {
			rs.getStatement().close();
        } catch(SQLException e) {
            // ignore
        }
    }
}
