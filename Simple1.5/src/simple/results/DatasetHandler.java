package simple.results;

public interface DatasetHandler<I extends Exception> {
	public void handleDatasetStart(String[] columnNames) throws I ;
	public void handleRowStart() throws I ;
	public void handleValue(String columnName, Object value) throws I ;
	public void handleRowEnd() throws I ;
	public void handleDatasetEnd() throws I ;
}
