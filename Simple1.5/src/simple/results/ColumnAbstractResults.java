/*
 * Created on May 5, 2005
 *
 */
package simple.results;

import java.util.AbstractList;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Column;
import simple.util.CollectionUtils;

public abstract class ColumnAbstractResults extends AbstractResults {
    protected Column[] columns;
    protected Map<String, Integer> columnNamesMap;
	protected Set<String> keySet;
	protected Collection<Object> values;
	protected Set<Map.Entry<String, Object>> entrySet;

    public ColumnAbstractResults(Column[] columns) {
        this(columns, false);
    }

    public ColumnAbstractResults(Column[] columns, boolean caseSensitive) {
        super();
		setColumns(columns, caseSensitive);
	}

	protected ColumnAbstractResults() {
		super();
	}

	protected void setColumns(Column[] columns, boolean caseSensitive) {
		this.columns = columns;
        Comparator<String> comp;
        if(caseSensitive) comp = new java.util.Comparator<String>() {
            public int compare(String o1, String o2) {
                if(o1 == null) {
                    if(o2 == null) return 0;
                    return -1;
                } else if(o2 == null) {
                    return 1;
                } else {
                    return o1.compareTo(o2);
                }
            }
        };
        else comp = new java.util.Comparator<String>() {
            public int compare(String o1, String o2) {
                if(o1 == null) {
                    if(o2 == null) return 0;
                    return -1;
                } else if(o2 == null) {
                    return 1;
                } else {
                    return o1.compareToIgnoreCase(o2);
                }
            }
        };
        this.columnNamesMap = new java.util.TreeMap<String, Integer>(comp);
        String[] columnFormats = new String[columns.length];
        for(int i = 0; i < columns.length; i++) {
            columnNamesMap.put(columns[i].getPropertyName(), new Integer(i));
            columnFormats[i] = columns[i].getFormat();
			if(columns[i].getUniqueColumn() != null)
				setUniqueColumnIndexFor(i + 1, 1 + CollectionUtils.iterativeSearch(columns, columns[i].getUniqueColumn()));
        }
        setFormats(columnFormats);
    }
    /**
     * @see simple.results.Results#getColumnClass(int)
     */
    public Class<?> getColumnClass(int column) {
        return columns[column-1].getColumnClass();
    }
    /**
     * @see simple.results.Results#getColumnIndex(java.lang.String)
     */
    public int getColumnIndex(String columnName) {
        if(columnName == null) return 0; //allow an intentional reference to the null column
        Integer i = columnNamesMap.get(columnName);
        if(i == null) //return -1;//XXX: can I do this??
            throw new IllegalArgumentException("The column name '" + columnName +"' does not exist in this resultset.");
        return i.intValue() + 1;
    }
    /**
     * @see simple.results.Results#getColumnName(int)
     */
    public String getColumnName(int column) {
        return columns[column-1].getPropertyName();
    }
    /**
     * @see simple.results.Results#getColumnCount()
     */
    public int getColumnCount() {
        return columns.length;
    }

	@Override
	protected boolean hasDynaProperty(String name) {
		return columnNamesMap.containsKey(name);
	}

	public void clear() {
		throw new UnsupportedOperationException("Results objects are not modifiable");
	}

	public boolean containsKey(Object key) {
		return columnNamesMap.containsKey(key);
	}

	public boolean containsValue(Object value) {
		for(int i = 1; i <= getColumnCount(); i++)
			if(ConvertUtils.areEqual(value, getValue(i)))
				return true;
		return false;
	}

	public Set<Map.Entry<String, Object>> entrySet() {
		if(entrySet == null)
			entrySet = new AbstractSet<Map.Entry<String, Object>>() {
				@Override
				public Iterator<Map.Entry<String, Object>> iterator() {
					return new Iterator<Entry<String, Object>>() {
						protected int index = 0;

						@Override
						public boolean hasNext() {
							return index < getColumnCount();
						}

						@Override
						public Map.Entry<String, Object> next() {
							index++;
							final String key = getColumnName(index);
							final Object value = getValue(index);
							return new Map.Entry<String, Object>() {
								@Override
								public String getKey() {
									return key;
								}

								@Override
								public Object getValue() {
									return value;
								}

								@Override
								public Object setValue(Object value) {
									throw new UnsupportedOperationException("Results objects are not modifiable");
								}
							};
						}

						@Override
						public void remove() {
							throw new UnsupportedOperationException("Results objects are not modifiable");
						}
					};
				}

				@Override
				public int size() {
					return getColumnCount();
				}
			};
		return entrySet;
	}

	public Object get(Object key) {
		return get((String) key);
	}

	public boolean isEmpty() {
		return size() > 0;
	}

	public Set<String> keySet() {
		if(keySet == null)
			keySet = Collections.unmodifiableSet(columnNamesMap.keySet());
		return keySet;
	}

	public Object put(String key, Object value) {
		throw new UnsupportedOperationException("Results objects are not modifiable");
	}

	public void putAll(Map<? extends String, ? extends Object> m) {
		throw new UnsupportedOperationException("Results objects are not modifiable");
	}

	public int size() {
		return getColumnCount();
	}

	public Collection<Object> values() {
		if(values == null)
			values = new AbstractList<Object>() {
				@Override
				public Object get(int index) {
					return getValue(index + 1);
				}

				@Override
				public int size() {
					return getColumnCount();
				}
			};
		return values;
	}
	
	protected static void checkDistinctNotNull(Set<Object> aSet, Object value){
    	if(value!=null){
    		if(ConvertUtils.isCollectionType(value.getClass())){
    			Collection c=null;
    			try{
    				c=ConvertUtils.asCollection(value);
    			} catch(ConvertException e) {}
    			if(c!=null){
	    			for(Object eachValue:c){
						if(eachValue!=null){
							addValue(aSet, eachValue); 
						}
					}
					return;
    			}
    		}
    		addValue(aSet, value); 
    	}
    }
}
