/*
 * ScrollableResultSetResults.java
 *
 * Created on January 24, 2003, 11:47 AM
 */

package simple.results;

import java.lang.reflect.UndeclaredThrowableException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.Column;
import simple.db.DBHelper;

/** Implementation of UpdateableResults that uses an updateable ResultSet as the
 *  data and an array of Strings as the column names. This class is NOT thread safe.
 *
 * @author  Brian S. Krug
 * @version
 */
public class UpdateableResultSetResults extends IndexScrollableResults implements UpdateableResults {
	protected final ResultSet rs;
    protected int size = -1;
	protected final DBHelper dbHelper;
    protected int offset;
    protected int length;
    protected Object[] currentRowValues;
    protected class RowValuesMap implements Map<String, Object> {
        protected Set<Map.Entry<String, Object>> entrySet;
        protected final int row;
        public RowValuesMap(int row) {
            this.row = row;
        }

        public Set<Map.Entry<String, Object>> entrySet() {
            if(entrySet == null) {
                entrySet = new AbstractSet<Map.Entry<String, Object>>() {
                    public Iterator<Map.Entry<String, Object>> iterator() {
                        return new Iterator<Map.Entry<String, Object>>() {
                            protected Iterator<String> keyIterator = keySet().iterator();
                            public boolean hasNext() { return keyIterator.hasNext(); }
                            public Map.Entry<String, Object> next() {
                                return new Map.Entry<String, Object>() {
                                    protected String key = keyIterator.next();
                                    protected Object value = get(key);
                                    public String getKey() { return  key; }
                                    public Object getValue() { return value; }
                                    public Object setValue(Object obj) { throw new UnsupportedOperationException(); }
                                    public int hashCode() {
                                        return (key == null ? 0 : key.hashCode()) + (value == null ? 0 : value.hashCode());
                                    }
                                    public boolean equals(Object obj) {
                                        return (obj instanceof Map.Entry<?,?>
                                        && ConvertUtils.areEqual(key, ((Map.Entry<?,?>)obj).getKey())
                                        && ConvertUtils.areEqual(value, ((Map.Entry<?,?>)obj).getValue()));
                                    }
                                };
                            }
                            public void remove() { throw new UnsupportedOperationException(); }
                        };
                    }
                    public int size() { return keySet().size(); }
                };
            }
            return entrySet;
        }

        public Object put(String key, Object value) {
            throw new UnsupportedOperationException();
        }

        public Object remove(Object key) {
            return put((String)key, null);
        }

        public Set<String> keySet() {
            return columnNamesMap.keySet();
        }

        public void clear() {
            throw new UnsupportedOperationException();
        }

        public Collection<Object> values() {
            return Arrays.asList(getValuesAtRow(row));
        }

        public boolean containsKey(Object obj) {
            return columnNamesMap.containsKey(obj);
        }

        public int size() {
            return columnNamesMap.size();
        }

        public boolean containsValue(Object obj) {
        	Object[] values = getValuesAtRow(row);
            for(int i = 0; i < values.length;i++) {
                if(ConvertUtils.areEqual(obj,get(i))) return true;
            }
            return false;
        }

        public void putAll(Map<? extends String, ? extends Object> map) {
            for(Map.Entry<? extends String, ? extends Object> entry : map.entrySet()) {
            	put(entry.getKey(), entry.getValue());
            }
        }

        public boolean isEmpty() {
            return size() == 0;
        }

        public Object get(Object key) {
        	Integer column = columnNamesMap.get(key);
        	if(column == null) return null;
        	Object[] values = getValuesAtRow(row);
            return values[column];
        }
    }
    /** Creates new ScrollableResultSetResults */
    public UpdateableResultSetResults(Column[] columns, ResultSet rs, DBHelper dbHelper) {
        this(columns, rs, dbHelper, 0, -1);
    }

    /** Creates new ScrollableResultSetResults */
    protected UpdateableResultSetResults(Column[] columns, ResultSet rs, DBHelper dbHelper, int offset, int length) {
        super(columns);
        this.rs = rs;
        this.dbHelper = dbHelper;
        this.offset = offset;
        this.length = length;
    }

    public Map<String,Object> get(int index) {
        return getMapAt(index);
    }

    public Map<String,Object> getMapAt(int index) {
        return new RowValuesMap(index + 1);
    }

    public int size() {
        // warning: this requires scrolling to the last row in the result set - could be costly
        if(size == -1) {
            try {
                if((length > -1 && rs.absolute(offset + length)) || rs.last())
                    size = Math.max(0, rs.getRow() - offset);
                else
                    size = 0;
            } catch(SQLException e) {
                handleException(e);
            }
        }
        return size;
    }

    protected Object[] getValuesAtRow(int row) throws IndexOutOfBoundsException {
    	try {
    		if(currentRow == row && currentRowValues != null)
    			return currentRowValues;
    		if(row == -1) {
    			rs.moveToInsertRow();
    		} else if(!rs.absolute(row + offset)) {
    			return null;
    		}
			Object[] values = dbHelper.getValues(rs, columns, true);
        	if(currentRow == row)
        		currentRowValues = values;
        	return values;
    	} catch(SQLException e) {
            handleException(e);
            return null; // this statement is never run because handleException throws a runtime exception
        } catch(ConvertException e) {
            handleException(e);
            return null; // this statement is never run because handleException throws a runtime exception
		}
    }

    protected void handleException(SQLException e) {
        throw new UndeclaredThrowableException(e, "SQL Exception while accessing ResultSet");
    }
    protected void handleException(ConvertException e) {
        throw new UndeclaredThrowableException(e, "Convert Exception while accessing ResultSet");
    }

    public boolean isValidRow(int row) {
    	if(row == -1) return true;
        if(row < 1) return false;
        if(size > -1) return row <= size;
        if(length > -1 && row - offset > length) return false;
        try {
            return rs.absolute(row + offset);
        } catch(SQLException e) {
            return false;
        }
    }

    public ScrollableResults subResults(int fromRow, int toRow) {
        UpdateableResultSetResults srsr = (UpdateableResultSetResults)this.clone();
        srsr.offset = fromRow - 1;
        srsr.length = toRow - fromRow;
        srsr.currentRow = 0;
        srsr.groupValues = null;
        srsr.currentRowValues = null;
        return srsr;
    }

    public void close() {
        try {
			rs.getStatement().close();
        } catch(SQLException e) {
            // ignore
        }
    }
    @Override
    public boolean next() {
    	currentRowValues = null;
    	if(currentRow == -1)
			try {
				rs.moveToCurrentRow();
			} catch(SQLException e) {
				handleException(e);
			}
    	return super.next();
    }

	public void deleteRow() throws SQLException {
		rs.deleteRow();
		if(size > -1) size--;
		currentRowValues = null;
	}

	public void newRow() throws SQLException {
		rs.cancelRowUpdates();
		rs.moveToInsertRow();
		currentRow = -1;
		currentRowValues = null;
	}

	public void refreshRow() throws SQLException {
		rs.cancelRowUpdates();
		if(currentRow != -1)
			rs.refreshRow();
		currentRowValues = null;
	}

	public void updateRow() throws SQLException {
		if(currentRow == -1) {
			rs.insertRow();
			if(size > -1) size++;
		} else {
			rs.updateRow();
		}
	}
	public void setValue(int column, Object value) throws SQLException, ConvertException {
		dbHelper.setValue(rs, columns[column-1], value);
		if(currentRowValues != null) currentRowValues[column-1] = value;
	}

	public void setValue(String columnName, Object value) throws SQLException, ConvertException {
		setValue(getColumnIndex(columnName), value);
	}
}
