package simple.ftp;

import simple.io.Log;

import com.usatech.ftps.FtpsBean;

public class SimpleFtpsBean extends FtpsBean {
	private static final Log log = Log.getLog();
    protected void logStatus(String message) {
    	if(log.isDebugEnabled())
    		log.debug(message);
    }
    protected void logCommand(String cmd, String param) {
    	if(log.isDebugEnabled())
    		log.debug("Send command \"" + cmd + param + "\"");
    }
    protected void logResponse(String reply) {
    	if(log.isDebugEnabled())
    		log.debug("Receive reply \"" + reply + "\"");
    }
}
