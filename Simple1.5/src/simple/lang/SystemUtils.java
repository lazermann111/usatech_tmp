/*
 * SystemUtils.java
 *
 * Created on June 17, 2004, 11:03 AM
 */

package simple.lang;

import java.io.IOException;
import java.lang.management.LockInfo;
import java.lang.management.ManagementFactory;
import java.lang.management.MonitorInfo;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.bean.CachedGetter;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.DynaBean;
import simple.bean.Getter;
import simple.bean.GetterSetterDynaBean;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.text.StringUtils;

/** Contains a set of system-related utility methods
 *
 * @author  Brian S. Krug
 */
public class SystemUtils {
	protected static final DynaBean systemInfo = createSystemInfoDynaBean();
	protected static ConfigSource versionSrc;
	protected static String version;
	protected final static Initializer<IOException> versionInitializer = new Initializer<IOException>() {
		@Override
		protected void doInitialize() throws IOException {
			URL url = Thread.currentThread().getContextClassLoader().getResource("version.txt");
			if(url == null)
				return;
			versionSrc = ConfigSource.createConfigSource(url);
			versionSrc.registerLoader(new Loader() {
				@Override
				public void load(ConfigSource src) throws LoadingException {
					try {
						version = IOUtils.readFully(src.getInputStream()).trim();
					} catch(IOException e) {
						throw new LoadingException(e);
					}
				}
			}, 5000, 0);
		}

		@Override
		protected void doReset() {
			if(versionSrc == null)
				versionSrc.registerLoader(null, 0, 0);
			versionSrc = null;
		}
	};
	@SuppressWarnings("rawtypes")
	protected static DynaBean createSystemInfoDynaBean() {
		GetterSetterDynaBean bean = new GetterSetterDynaBean(SystemUtils.class.getName() + ".SystemInfo");
		bean.addDynaProperty("totalMemory", Long.class, new Getter<Long>() {
			public Long get() {
				return Runtime.getRuntime().totalMemory();
			}
			public Class<Long> getType() {
				return Long.class;
			}
		}, null);
		bean.addDynaProperty("availableProcessors", Integer.class, new Getter<Integer>() {
			public Integer get() {
				return Runtime.getRuntime().availableProcessors();
			}
			public Class<Integer> getType() {
				return Integer.class;
			}
		}, null);
		bean.addDynaProperty("freeMemory", Long.class, new Getter<Long>() {
			public Long get() {
				return Runtime.getRuntime().freeMemory();
			}
			public Class<Long> getType() {
				return Long.class;
			}
		}, null);
		bean.addDynaProperty("maxMemory", Long.class, new Getter<Long>() {
			public Long get() {
				return Runtime.getRuntime().maxMemory();
			}
			public Class<Long> getType() {
				return Long.class;
			}
		}, null);
		bean.addDynaProperty("env", Map.class, new CachedGetter<Map>() {
			@Override
			protected Map<?,?> initial() {
				return System.getenv();
			}
			public Class<Map> getType() {
				return Map.class;
			}
		}, null);
		bean.addDynaProperty("properties", Properties.class, new Getter<Properties>() {
			public Properties get() {
				return System.getProperties(); //these can be changed so do not use CachedGetter
			}
			public Class<Properties> getType() {
				return Properties.class;
			}
		}, null);
		bean.addDynaProperty("hostAddress", InetAddress.class, new CachedGetter<InetAddress>() {
			@Override
			protected InetAddress initial() {
				try {
					return java.net.Inet4Address.getLocalHost();
				} catch(UnknownHostException e) {
					return null;
				}
			}
			public Class<InetAddress> getType() {
				return InetAddress.class;
			}
		}, null);
		bean.addDynaProperty("processId", String.class, new CachedGetter<String>() {
			@Override
			protected String initial() {
				String vmId = ManagementFactory.getRuntimeMXBean().getName();
				Pattern pattern = Pattern.compile("(\\d+)@.*");
				Matcher matcher = pattern.matcher(vmId);
				if(matcher.matches()) {
					return matcher.group(1);
				}
				return vmId;
			}
			public Class<String> getType() {
				return String.class;
			}
		}, null);

		return bean;
	}
    /** Creates a new instance of SystemUtils */
    public SystemUtils() {
    }

    public static DynaBean getSystemInfo() {
    	return systemInfo;
    }
    public static String getJavaVersion() {
        return System.getProperty("java.version");
    }

    public static boolean isJavaVersionAtLeast(int major, int minor) {
        String[] parts = StringUtils.split(getJavaVersion(), '.');
        int p0 = Integer.parseInt(parts[0]);
        if(p0 > major) return true;
        if(p0 < major) return false;
        int p1 = Integer.parseInt(parts[1]);
        if(p1 >= minor) return true;
        return false;
    }

    public static String getNewLine() {
        return System.getProperty("line.separator", "\n");
    }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object greatestConvert(Comparable c1, Object c2) throws ConvertException {
    	if(c1 == null) return c2;
    	if(c2 == null) return c1;
    	if(c1.compareTo(ConvertUtils.convert(c1.getClass(), c2)) < 0) return c2;
    	return c1;
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object leastConvert(Comparable c1, Object c2) throws ConvertException {
    	if(c1 == null) return c2;
    	if(c2 == null) return c1;
    	if(c1.compareTo(ConvertUtils.convert(c1.getClass(), c2)) > 0) return c2;
    	return c1;
    }

	public static <T extends Comparable<T>> T least(T... possibles) {
		T least = null;
		for(T t : possibles)
			if(t != null && (least == null || t.compareTo(least) < 0))
				least = t;
		return least;
	}

	public static <T extends Comparable<T>> T greatest(T... possibles) {
		T least = null;
		for(T t : possibles)
			if(t != null && (least == null || t.compareTo(least) > 0))
				least = t;
		return least;
	}

    public static <T> T nvl(T... objects) {
    	if(objects != null) {
    		for(T o : objects)
    			if(o != null && (!(o instanceof String) || ((String)o).trim().length() > 0)) return o;
    	}
    	return null;
    }

	public static long trunc(long orig, long base) {
		return (orig / base) * base;
	}

    public static String trim(String string) {
    	if(string == null) return null;
    	return string.trim();
    }

    public static int spreadHash(int h) {
    	h = h * 33554431;
        h += ~(h << 9);
        h ^=  (h >>> 14);
        h +=  (h << 4);
        h ^=  (h >>> 10);
        return h;
    }

    //  use name instance of compare of Class because classes could have been loaded from different ClassLoaders
    public static Set<Thread> getThreadsOfClass(String threadClassName) {
    	Set<Thread> threads = new HashSet<Thread>();
    	ThreadGroup tg = Thread.currentThread().getThreadGroup();
    	int num = tg.activeCount();
    	Thread[] all = new Thread[num];
    	Thread.enumerate(all);
    	for(Thread t : all) {
    		if(t.getClass().getName().equals(threadClassName))
    			threads.add(t);
    	}
    	return threads;
    }

    public static <T extends Thread> Set<T> getThreadsOfClass(Class<T> threadClass) {
    	Set<T> threads = new HashSet<T>();
    	ThreadGroup tg = Thread.currentThread().getThreadGroup();
    	int num = tg.activeCount();
    	Thread[] all = new Thread[num];
    	Thread.enumerate(all);
    	for(Thread t : all) {
    		if(threadClass.isInstance(t))
    			threads.add(threadClass.cast(t));
    	}
    	return threads;
    }

    public static String getClassLoaderDetails(ClassLoader classLoader) {
    	StringBuilder sb = new StringBuilder();
    	sb.append(classLoader.getClass().getName());
    	if(classLoader instanceof URLClassLoader) {
    		sb.append(" with urls=");
    		URL[] urls = ((URLClassLoader)classLoader).getURLs();
    		sb.append(Arrays.toString(urls));
    	}
    	return sb.toString();
    }

    public static int addHashCode(int hashCode, Object object) {
    	hashCode = hashCode * 31;
    	if(object != null) {
    		if(object instanceof Object[])
    			hashCode = addHashCodes(hashCode, (Object[]) object);
    		else if(object instanceof Iterable<?>)
    			hashCode = addHashCodes(hashCode, (Iterable<?>) object);
    		else if (object instanceof byte[])
            	hashCode += Arrays.hashCode((byte[]) object);
            else if (object instanceof short[])
                hashCode += Arrays.hashCode((short[]) object);
            else if (object instanceof int[])
                hashCode += Arrays.hashCode((int[]) object);
            else if (object instanceof long[])
                hashCode += Arrays.hashCode((long[]) object);
            else if (object instanceof char[])
                hashCode += Arrays.hashCode((char[]) object);
            else if (object instanceof float[])
                hashCode += Arrays.hashCode((float[]) object);
            else if (object instanceof double[])
                hashCode += Arrays.hashCode((double[]) object);
            else if (object instanceof boolean[])
                hashCode += Arrays.hashCode((boolean[]) object);
            else if(object instanceof Enum<?>)
                hashCode += ((Enum<?>) object).ordinal();
            else if(object instanceof Class<?>)
                hashCode += ((Class<?>) object).getName().hashCode();
            else
            	hashCode += object.hashCode();
    	}
    	return hashCode;
    }

	public static int addHashCodes(int hashCode, Object... array) {
    	hashCode = hashCode * 31;
    	if(array != null) {
    		for(int i = 0; i < array.length; i++)
    			hashCode = addHashCode(hashCode, array[i]);
    	}
    	return hashCode;
    }
    public static int addHashCodes(int hashCode, Iterable<?> iterable) {
    	hashCode = hashCode * 31;
    	if(iterable != null) {
			for(Object o : iterable)
				hashCode = addHashCode(hashCode, o);
    	}
    	return hashCode;
    }
    public static boolean sameThrowable(Throwable t1, Throwable t2) {
    	if(t1 == null)
    		return t2 == null;
    	else if(t2 == null)
    		return false;
    	else 
    		return (t1.getClass().equals(t2.getClass()) && ConvertUtils.areEqual(t1.getMessage(), t2.getMessage()) && sameThrowable(t1.getCause(), t2.getCause()));
    }

	public static Throwable getRootCause(Throwable throwable) {
		if(throwable.getCause() != null)
			return getRootCause(throwable.getCause());
		return throwable;
	}

	public static long updateIfGreater(AtomicLong current, long update) {
		long c = current.get();
		if(update <= c)
			return c;
		if(current.compareAndSet(c, update))
			return c;
		return updateIfGreater(current, update);
	}

	public static long updateIfLesser(AtomicLong current, long update) {
		long c = current.get();
		if(update >= c)
			return c;
		if(current.compareAndSet(c, update))
			return c;
		return updateIfLesser(current, update);
	}

	public static String getApplicationVersion() throws InterruptedException, IOException {
		versionInitializer.initialize();
		if(versionSrc == null)
			return null;
		try {
			versionSrc.reload();
		} catch(LoadingException e) {
			if(e.getCause() instanceof IOException)
				throw (IOException) e.getCause();
			throw new IOException(e);
		}
		return version;
	}

	public static String getApplicationVersionSafely() {
		try {
			return getApplicationVersion();
		} catch(InterruptedException e) {
			return null;
		} catch(IOException e) {
			return null;
		}
	}

	protected static ThreadMXBean tmbean;
	protected static String INDENT = "\t";

	public static void dumpThreads(Appendable out, boolean lockedMonitors, boolean lockedSynchronizers) throws IOException {
		if(tmbean == null)
			tmbean = ManagementFactory.getThreadMXBean();
		out.append("----------- Full Java thread dump -------------").append(getNewLine());
		ThreadInfo[] tinfos = tmbean.dumpAllThreads(lockedMonitors, lockedSynchronizers);
		for(ThreadInfo ti : tinfos) {
			printThreadInfo(ti, out);
			LockInfo[] syncs = ti.getLockedSynchronizers();
			printLockInfo(syncs, out);
			out.append(getNewLine());
		}
	}

	protected static void printThreadInfo(ThreadInfo ti, Appendable out) throws IOException {
		out.append('"').append(ti.getThreadName()).append('"').append(" Id=").append(String.valueOf(ti.getThreadId())).append(" in ");
		if(ti.getThreadState() != null)
			out.append(ti.getThreadState().toString());
		else
			out.append("UNKNOWN");
		if(ti.getLockName() != null)
			out.append(" on lock=").append(ti.getLockName());
		if(ti.isSuspended())
			out.append(" (suspended)");
		if(ti.isInNative())
			out.append(" (running in native)");
		out.append(getNewLine());
		if(ti.getLockOwnerName() != null)
			out.append(INDENT).append(" owned by ").append(ti.getLockOwnerName()).append(" Id=").append(String.valueOf(ti.getLockOwnerId())).append(getNewLine());
		StackTraceElement[] stacktrace = ti.getStackTrace();
		MonitorInfo[] monitors = ti.getLockedMonitors();
		for(int i = 0; i < stacktrace.length; i++) {
			StackTraceElement ste = stacktrace[i];
			out.append(INDENT).append("at ").append(ste.toString()).append(getNewLine());

			for(MonitorInfo mi : monitors) {
				if(mi.getLockedStackDepth() == i)
					out.append(INDENT).append("  - locked ").append(mi.toString()).append(getNewLine());
			}
		}
		out.append(getNewLine());
	}

	protected static void printLockInfo(LockInfo[] locks, Appendable out) throws IOException {
		out.append(INDENT).append("Locked synchronizers: count = ").append(String.valueOf(locks.length)).append(getNewLine());
		for(LockInfo li : locks)
			out.append(INDENT).append("  - ").append(String.valueOf(li)).append(getNewLine());
	}
}
