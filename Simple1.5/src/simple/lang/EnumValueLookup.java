/**
 *
 */
package simple.lang;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import simple.bean.ConvertUtils;
import simple.bean.Converter;
import simple.bean.Enumerator;
import simple.bean.Formatter;
import simple.io.Log;
import simple.util.CaseInsensitiveComparator;
import simple.util.CollectionUtils;

/**
 * @author Brian S. Krug
 *
 */
public class EnumValueLookup<E extends Enum<?>, V> implements Converter<E>, Formatter<E> {
	private static final Log log = Log.getLog();
	private final Map<V, E> values;
	private final Map<E, V> keys;
	private final Class<E> enumClass;
	private final Class<V> lookupClass;
	private final Map<String, E> nameMap;

	@SuppressWarnings("unchecked")
	public EnumValueLookup(final Class<E> enumClass) throws IllegalArgumentException {
		this.enumClass = enumClass;
		final Method getter;
		try {
			getter = enumClass.getMethod("getValue");
		} catch(SecurityException e) {
			throw new IllegalArgumentException("Class " + enumClass.getName() + " does not contain an accessible 'getValue' method", e);
		} catch(NoSuchMethodException e) {
			throw new IllegalArgumentException("Class " + enumClass.getName() + " does not contain an accessible 'getValue' method", e);
		}
		lookupClass = (Class<V>)getter.getReturnType();
		Map<E, V> keys = new HashMap<E, V>();
		Map<String, E> nameMap = new TreeMap<String, E>(new CaseInsensitiveComparator<String>());
		Map<V, E> values;
		if(lookupClass.isArray())
			values = new TreeMap<>(CollectionUtils.getComparator(lookupClass));
		else
			values = new HashMap<V, E>();
		E[] enums = getEnumConstants(enumClass);
		for(E en : enums) {
			V value;
			try {
				value = (V)getter.invoke(en);
			} catch(IllegalAccessException e) {
				log.warn("This should not happen", e);
				continue;
			} catch(InvocationTargetException e) {
				log.warn("Error while getting value", e);
				continue;
			}
			E dup = values.put(value, en);
			if(dup != null)
				log.error("Duplicate value for " + enumClass.getName() + " " + dup + " and " + en);
			keys.put(en, value);
			nameMap.put(en.toString(), en);
		}
		this.keys = Collections.unmodifiableMap(keys);
		this.values = Collections.unmodifiableMap(values);
		this.nameMap = Collections.unmodifiableMap(nameMap);
		//register a converter for this enum
		ConvertUtils.register(this);
		if(Number.class.isAssignableFrom(ConvertUtils.convertToWrapperClass(lookupClass))) {
			final Class<? extends Number> targetClass = ConvertUtils.convertToWrapperClass(lookupClass).asSubclass(Number.class);
			ConvertUtils.registerEnumerator(new Enumerator<E>() {
				public Class<E> getSourceClass() {
					return enumClass;
				}
				public Number enumerate(E object) {
					try {
						return (Number)getter.invoke(object);
					} catch(IllegalAccessException e) {
						log.warn("This should not happen", e);
						return null;
					} catch(InvocationTargetException e) {
						log.warn("Error while getting value", e);
						return null;
					}
				}

				@Override
				public Class<? extends Number> getTargetClass() {
					return targetClass;
				}
			});
		}
	}
	public E getByValue(V value) throws InvalidValueException {
        E e = values.get(value);
    	if(e == null)
    		throw new InvalidValueException(value);
        return e;
    }
	/**
	 * @see simple.bean.Converter#convert(java.lang.Object)
	 */
	public E convert(Object object) throws Exception {
		if(lookupClass.isInstance(object)) {
			V value = lookupClass.cast(object);
			E e = values.get(value);
	    	if(e != null)
	    		return e;
    		e = nameMap.get(ConvertUtils.getStringSafely(object));
	    	if(e != null)
	    		return e;
	    	throw new InvalidValueException(value);
		} else {
			E en = nameMap.get(ConvertUtils.getStringSafely(object));
			if(en != null)
				return en;
			return getByValue(ConvertUtils.convert(lookupClass, object));
		}
	}

	/**
	 * @see simple.bean.Formatter#format(java.lang.Object)
	 */
	public String format(E object) {
		if(object == null)
			return null;
		return ConvertUtils.getStringSafely(keys.get(object));
	}

	/**
	 * @see simple.bean.Converter#getTargetClass()
	 */
	public Class<E> getTargetClass() {
		return enumClass;
	}

	@SuppressWarnings("unchecked")
	public static <T extends Enum<?>> T[] getEnumConstants(Class<T> enumClass) {
		try {
			return (T[])enumClass.getMethod("values").invoke(null);
		} catch(IllegalArgumentException e) {
			log.error("This should never happen", e);
		} catch(SecurityException e) {
			log.error("This should never happen", e);
		} catch(IllegalAccessException e) {
			log.error("This should never happen", e);
		} catch(InvocationTargetException e) {
			log.error("This should never happen", e);
		} catch(NoSuchMethodException e) {
			log.error("This should never happen", e);
		}
		return null;
	}

	public Set<V> getAllValues() {
		return values.keySet();
	}

	public Map<V, E> getAllMapped() {
		return values;
	}
}
