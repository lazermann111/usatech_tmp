/**
 *
 */
package simple.lang;

/**
 * @author Brian S. Krug
 *
 */
public class EnumStringValueLookup<E extends Enum<?>> extends EnumValueLookup<E, String> {

	/**
	 * @param enumClass
	 * @throws IllegalArgumentException
	 */
	public EnumStringValueLookup(Class<E> enumClass) throws IllegalArgumentException {
		super(enumClass);
	}

	public E getByValueSafely(String value) {
		try {
			return getByValue(value);
		} catch(InvalidValueException e) {
			return null;
		}
	}
}
