/**
 *
 */
package simple.lang;

/**
 * @author Brian S. Krug
 *
 */
public class EnumCharValueLookup<E extends Enum<?>> extends EnumValueLookup<E, Character> {

	/**
	 * @param enumClass
	 * @throws IllegalArgumentException
	 */
	public EnumCharValueLookup(Class<E> enumClass) throws IllegalArgumentException {
		super(enumClass);
	}

	public E getByValue(char value) throws InvalidValueException {
		return getByValue(new Character(value));
	}
	public E getByValueSafely(char value) {
		try {
			return getByValue(new Character(value));
		} catch(InvalidValueException e) {
			return null;
		}
	}
}
