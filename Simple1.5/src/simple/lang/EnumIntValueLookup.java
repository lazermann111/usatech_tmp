/**
 *
 */
package simple.lang;

/**
 * @author Brian S. Krug
 *
 */
public class EnumIntValueLookup<E extends Enum<?>> extends EnumValueLookup<E, Integer> {

	/**
	 * @param enumClass
	 * @throws IllegalArgumentException
	 */
	public EnumIntValueLookup(Class<E> enumClass) throws IllegalArgumentException {
		super(enumClass);
	}

	public E getByValue(int value) throws InvalidIntValueException {
		try {
			return getByValue(new Integer(value));
		} catch(InvalidValueException e) {
			throw new InvalidIntValueException(value);
		}
	}
	public E getByValueSafely(int value) {
		try {
			return getByValue(new Integer(value));
		} catch(InvalidValueException e) {
			return null;
		}
	}
}
