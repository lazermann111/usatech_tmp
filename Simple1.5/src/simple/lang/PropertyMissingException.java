package simple.lang;

public class PropertyMissingException extends Exception {
	private static final long serialVersionUID = -198208000316365L;

	public PropertyMissingException(String message) {
		super(message);
	}

	public PropertyMissingException(Throwable cause) {
		super(cause);
	}

	public PropertyMissingException(String message, Throwable cause) {
		super(message, cause);
	}

}
