/**
 *
 */
package simple.lang;

/**
 * @author Brian S. Krug
 *
 */
public class EnumByteValueLookup<E extends Enum<?>> extends EnumValueLookup<E, Byte> {

	/**
	 * @param enumClass
	 * @throws IllegalArgumentException
	 */
	public EnumByteValueLookup(Class<E> enumClass) throws IllegalArgumentException {
		super(enumClass);
	}

	public E getByValue(byte value) throws InvalidByteValueException {
		try {
			return getByValue(new Byte(value));
		} catch(InvalidValueException e) {
			throw new InvalidByteValueException(value);
		}
	}
	public E getByValueSafely(byte value) {
		try {
			return getByValue(new Byte(value));
		} catch(InvalidValueException e) {
			return null;
		}
	}
}
