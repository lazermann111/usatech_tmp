/**
 * 
 */
package simple.lang;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import simple.io.Log;
import simple.util.concurrent.CustomThreadFactory;

/**
 * The initializer class ensures that initialization is completed
 * in a multithreaded environment for a method that invokes
 * .initialize. 
 * 
 * Contrast this with Parallel Initializer which guarantees
 * only that once you call initialize, that at some point in the 
 * future, the initialization will be complete.
 * 
 * A startInitialize method is provided that begins the initialization
 * The initialize will block until initialization is complete
 *    
 * @author phorsfield
 *
 */
public abstract class ParallelInitializer<I extends Exception> extends Initializer<I> {
	private Log log = Log.getLog();

	// -- inheritance based API
	protected abstract void doParallelInitialize() throws I;
	protected abstract void doParallelReset();

	// -- concurrency primitives 
	private static final Executor DEFAULT_EXECUTOR = 
			Executors.newCachedThreadPool(
					new CustomThreadFactory("PI-Parallel-Initializer", false) );
	protected Semaphore shutdownSemaphore = new Semaphore(0);
	protected CountDownLatch initializeSucceeded = new CountDownLatch(1);
	protected CountDownLatch resetSucceeded = new CountDownLatch(0);
	
	// -- configuration
	protected long initCycleTimeMillis = 60000 /*1 minute */;
	protected long resetBlockMillis = 30000 /* 30 seconds block on reset (system shutdown in 1 minute). */;

	// -- runtime
	private Executor executor;
		
	protected Runnable initThread = new Runnable() {

		@Override
		public void run() {
			
			Exception lastLoggedException = null;
			
			while(true)
			{
				if(shutdownSemaphore.tryAcquire(1))
				{
					resetSucceeded.countDown();
					return; // fail to start if we've already been told to shut down.
				}
				try {
					doParallelInitialize();
					initializeSucceeded.countDown();					
					break;
				}
				catch(Exception e)
				{
					if(lastLoggedException == null || !(lastLoggedException.getMessage().equals(e.getMessage())))
					{
						lastLoggedException = e;
						log.info("Exception during initialization, next attempt in " + ParallelInitializer.this.initCycleTimeMillis + "ms (initCycleTimeMillis).", lastLoggedException);
					}
					try {
						Thread.sleep(getInitCycleTimeMillis());
					} catch (InterruptedException e1) {
						// cycle immediately
					}
				}
			}
						
		} 
		
	};
	
	protected Runnable resetThread = new Runnable() {

		@Override
		public void run() {
			while(true)
			{
				try {
					shutdownSemaphore.acquire(1); /* we will spend the majority of our time waiting here */
					break;
				} catch (InterruptedException e) {
					log.warn("Interrupted; not shutting down cycling initializer", e);
					return ;
				}
			}
						
			doParallelReset();
			
			initializeSucceeded = new CountDownLatch(1);
			resetSucceeded.countDown();
		} 
	};

	// -- Overridden implementation of Initializer for parallel operation
	@Override
	protected final void doInitialize() throws I {
		shutdownSemaphore = new Semaphore(0);		
		initializeSucceeded = new CountDownLatch(1);
		resetSucceeded = new CountDownLatch(1);
		executor.execute(initThread);
	}

	@Override
	protected final void doReset() {
		shutdownSemaphore.release(1);			
		executor.execute(resetThread);

		// parallel reset must block to avoid racing against init.		
		if(resetBlockMillis > 0)
		{
			try {
				resetSucceeded.await(resetBlockMillis, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {
				log.warn("Interrupted while blocking waiting for initializer reset.", e);
			}
		}
		else
		{
			log.warn("Parallel Initializer should not be initialized with no blocking," +
					" or shutdown may not complete before initialize is called again.");
			
		}
	}

	// -- Constructors
	/**
	 * 
	 * @param cycleTimeMillis number of milliseconds between attempts to perform an initialization
	 */
	public ParallelInitializer(long cycleTimeMillis)
	{
		this.initCycleTimeMillis = cycleTimeMillis;
		this.executor = DEFAULT_EXECUTOR;
	}

	public ParallelInitializer()
	{
		// takes the default one minute cycle time, and no blocking on initialize.
		this.executor = DEFAULT_EXECUTOR;
	}

	/**
	 * @param executor thread pool used to fire executions
	 * @param cycleTimeMillis number of milliseconds between attempts to perform an initialization
	 */
	public ParallelInitializer(long cycleTimeMillis, Executor executor)
	{
		this.initCycleTimeMillis = cycleTimeMillis;
		this.executor = executor;
	}

	/**
	 * @param executor thread pool used to fire executions
	 */
	public ParallelInitializer(Executor executor)
	{
		// takes the default one minute cycle time, and no blocking on initialize.
		this.executor = executor;
	}

	// -- Parallel State Queries
	public boolean hasReset() throws InterruptedException {
		return resetSucceeded.await(0, TimeUnit.MILLISECONDS);
	}

	public boolean waitForReset(long timeout, TimeUnit timeUnit) throws InterruptedException {
		return resetSucceeded.await(timeout, timeUnit);
	}

	public boolean hasInitialized() throws InterruptedException {
		return initializeSucceeded.await(0, TimeUnit.MILLISECONDS);
	}

	public boolean waitForInitialize(long timeout, TimeUnit timeUnit) throws InterruptedException {
		return initializeSucceeded.await(timeout, timeUnit);
	}
	
	// -- Getters and Setters
	/**
	 * @return the number of milliseconds to wait for doParallelReset to be complete.
	 */
	public long getResetBlockMillis() {
		return resetBlockMillis;
	}
	/**
	 * @param resetBlockMillis the number of milliseconds to wait for doParallelReset to be complete.
	 * 							0 means that the process will NOT block
	 */
	public void setResetBlockMillis(long resetBlockMillis) {
		this.resetBlockMillis = resetBlockMillis;
	}
	/**
	 * @return the number of milliseconds between attempts to call doParallelInitialize();
	 */
	public long getInitCycleTimeMillis() {
		return initCycleTimeMillis;
	}

	/**
	 * @param initCycleTimeMillis the number of milliseconds between attempts to invoke doParallelInitialize 
	 */
	public void setInitCycleTimeMillis(long initCycleTimeMillis) {
		this.initCycleTimeMillis = initCycleTimeMillis;
	}
	
}
