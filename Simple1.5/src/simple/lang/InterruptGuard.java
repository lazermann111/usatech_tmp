package simple.lang;

public interface InterruptGuard {
	public boolean getInterruptible() ;
	
	/**
	 * @param interruptible
	 * @return The old value
	 */
	public boolean setInterruptible(boolean interruptible) ;
}
