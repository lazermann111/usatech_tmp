package simple.lang;

public class InvalidStringValueException extends Exception {
    private static final long serialVersionUID = 3016034343224118966L;
    protected final String value;

    public InvalidStringValueException(String value) {
        this.value = value;
    }

    public InvalidStringValueException(String message, String value) {
        super(message);
        this.value = value;
    }

    public InvalidStringValueException(Throwable cause, String value) {
        super(cause);
        this.value = value;
    }

    public InvalidStringValueException(String message, Throwable cause, String value) {
        super(message, cause);
        this.value = value;
    }
    /**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}
