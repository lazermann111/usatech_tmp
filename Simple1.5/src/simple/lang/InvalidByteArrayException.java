/**
 *
 */
package simple.lang;

/**
 * @author Brian S. Krug
 *
 */
public class InvalidByteArrayException extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = 4431549345670739214L;
	protected final byte[] value;
	/**
	 *
	 */
	public InvalidByteArrayException(byte[] value) {
		this.value = value;
	}

	/**
	 * @param message
	 */
	public InvalidByteArrayException(String message, byte[] value) {
		super(message);
		this.value = value;
	}

	/**
	 * @param cause
	 */
	public InvalidByteArrayException(Throwable cause, byte[] value) {
		super(cause);
		this.value = value;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidByteArrayException(String message, Throwable cause, byte[] value) {
		super(message, cause);
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public byte[] getValue() {
		return value;
	}
}
