package simple.lang;

public interface Decision<A> {
	public boolean decide(A argument) ;
}
