package simple.lang;

public class InvalidIntValueException extends Exception {
    private static final long serialVersionUID = 3016034343224118966L;
    protected final int value;

    public InvalidIntValueException(int value) {
        this.value = value;
    }

    public InvalidIntValueException(String message, int value) {
        super(message);
        this.value = value;
    }

    public InvalidIntValueException(Throwable cause, int value) {
        super(cause);
        this.value = value;
    }

    public InvalidIntValueException(String message, Throwable cause, int value) {
        super(message, cause);
        this.value = value;
    }
    /**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}
}
