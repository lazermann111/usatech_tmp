/**
 *
 */
package simple.lang;

/**
 * @author Brian S. Krug
 *
 */
public class InvalidByteValueException extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = 4431549050920739214L;
	protected final byte value;
	/**
	 *
	 */
	public InvalidByteValueException(byte value) {
		this.value = value;
	}

	/**
	 * @param message
	 */
	public InvalidByteValueException(String message, byte value) {
		super(message);
		this.value = value;
	}

	/**
	 * @param cause
	 */
	public InvalidByteValueException(Throwable cause, byte value) {
		super(cause);
		this.value = value;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidByteValueException(String message, Throwable cause, byte value) {
		super(message, cause);
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public byte getValue() {
		return value;
	}
}
