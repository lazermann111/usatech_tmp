package simple.lang;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Initializer<I extends Exception> {
	protected final static int CREATED = 0;
	protected final static int CANCELLED = 1;
	protected final static int RUNNING = 2;
	protected final static int DONE = 3;
	protected final static int EXCEPTION = 4;
	protected final static int RESETTING = 5;
	private final ReentrantLock lock = new ReentrantLock();
	private final AtomicInteger state = new AtomicInteger(CREATED);
	private Throwable exception;
	private Condition signal;
	private volatile boolean waiting = false;

	protected boolean isResetOnException() {
		return true;
	}

	protected boolean isResetOnCancel() {
		return true;
	}

	protected int getState() {
		return state.get();
	}
	/**
	 * @see java.util.concurrent.Future#cancel(boolean)
	 */
	public boolean cancel(boolean mayInterruptIfRunning) {
		int localState = state.intValue();
		switch(localState) {
			case CREATED:
				if(state.compareAndSet(CREATED, CANCELLED)) {
					return true;
				} else
					return cancel(mayInterruptIfRunning);
			case RUNNING:
				if(!mayInterruptIfRunning) return false;
				doCancel(); // After this call initiziliation may be half complete so we must call doReset() before returning
				return callReset(CANCELLED);
			case CANCELLED: case EXCEPTION: case DONE: case RESETTING: default:
				return false;
		}
	}

	/**
	 * @see java.util.concurrent.Future#isCancelled()
	 */
	public boolean isCancelled() {
		return state.intValue() == CANCELLED;
	}

	/**
	 * @see java.util.concurrent.Future#isDone()
	 */
	public boolean isDone() {
		switch(state.intValue()) {
			case CANCELLED:
			case EXCEPTION:
			case DONE:
				return true;
			default:
				return false;
		}
	}

	public boolean isSuccess() {
		return state.intValue() == DONE;
	}

	public boolean isRunning() {
		switch(state.intValue()) {
			case RUNNING:
			case RESETTING:
				return true;
			default:
				return false;
		}
	}
	/**
	 * @see java.util.concurrent.Future#get()
	 */
	public boolean initialize() throws InterruptedException, I {
		try {
			return initialize(-1, null);
		} catch(TimeoutException e) {
			throw new IllegalStateException("This should never happen");
		}
	}

	/**
	 * Returns <code>true</code> if initialization occurred and was not canceled mid-stream
	 * 
	 * @see java.util.concurrent.Future#get(long, java.util.concurrent.TimeUnit)
	 */
	public boolean initialize(long timeout, TimeUnit unit) throws InterruptedException, I, TimeoutException {
		return initialize(timeout, unit, isResetOnException(), isResetOnCancel());
	}

	protected boolean initialize(long timeout, TimeUnit unit, boolean resetOnException, boolean resetOnCancel) throws InterruptedException, I, TimeoutException {
		switch(state.intValue()) {
			case CREATED:
				if(state.compareAndSet(CREATED, RUNNING)) {
					try {
						doInitialize();
						if(state.compareAndSet(RUNNING, DONE))// it may have changed to CANCELLED
							return true;
					} catch(Throwable e) {
						exception = e;
						state.compareAndSet(RUNNING, EXCEPTION);
						throwCastException();
					} finally {
						//callable = null; // for gc - can't do this for resettable
						if(waiting) {
							lock.lock();
							try {
								signal.signalAll();
							} finally {
								waiting = false;
								lock.unlock();
							}
						}
					}
				}
				return initialize(timeout, unit, resetOnException, resetOnCancel);
			case RUNNING: case RESETTING:
				// wait until done
				lock.lock();
				try {
					if(signal == null)
						signal = lock.newCondition();
					long remaining = timeout;
					while(state.intValue() == RUNNING || state.intValue() == RESETTING) {
						waiting = true;
						if(timeout >= 0 && unit != null) {
							long start = System.currentTimeMillis();
							if(!signal.await(remaining, unit)) {//timeout
								if(state.intValue() == CANCELLED) throw new CancellationException();
								else throw new TimeoutException("Initialization not complete after " + timeout + " " + unit.toString());
							}
							remaining -= unit.convert(System.currentTimeMillis() - start, TimeUnit.MILLISECONDS);
							if(remaining <= 0) {
								if(state.intValue() == CANCELLED) throw new CancellationException();
								else throw new TimeoutException("Initialization not complete after " + timeout + " " + unit.toString());
							}
						} else {
							signal.await();
						}
					}
				} finally {
					lock.unlock();
				}
				return initialize(timeout, unit, resetOnException, resetOnCancel);
			case CANCELLED:
				if(!resetOnCancel)
					throw new CancellationException();
				reset();
				return initialize(timeout, unit, false, false);
			case EXCEPTION:
				if(!resetOnException)
					throwCastException();
				reset();
				return initialize(timeout, unit, false, false);
			case DONE: default:
				return false;
		}
	}
	@SuppressWarnings("unchecked")
	protected void throwCastException() throws I, RuntimeException, Error {
		if(exception == null)
			return;
		if(exception instanceof RuntimeException)
			throw (RuntimeException) exception;
		if(exception instanceof Error)
			throw (Error) exception;
		throw (I) exception;
	}
	protected abstract void doInitialize() throws I;
	protected abstract void doReset() ;

	/**
	 * This allows sub-classes to stop initialization mid-stream; The default implementation does nothing.
	 * 
	 */
	protected void doCancel() {

	}

	public boolean reset() {
		return callReset(CREATED);
	}

	protected boolean callReset(int targetState) {
		int localState = state.intValue();
		// only try once, since reset() is the only thing that will change
		// status from CANCELLED, EXCEPTION, or DONE to something else
		switch(localState) {
			case CREATED:
			case RESETTING:
				return false;
			case CANCELLED:
				return targetState != localState && state.compareAndSet(localState, targetState);
			case RUNNING:
				// wait until done, then call reset again
				lock.lock();
				try {
					if(signal == null)
						signal = lock.newCondition();
					while(state.intValue() == RUNNING) {
						waiting = true;
						try {
							signal.await();
						} catch(InterruptedException e) {
							// Ignore
						}
					}
				} finally {
					lock.unlock();
				}
				return callReset(targetState);
			case EXCEPTION:
			case DONE:
				if(state.compareAndSet(localState, RESETTING)) {
					lock.lock();
					try {
						exception = null;
						doReset();
						state.compareAndSet(RESETTING, targetState);
						return true;
					} finally {
						if(waiting)
							signal.signalAll();
						waiting = false;
						lock.unlock();
					}
				}
				return false;
			default:
				throw new IllegalStateException("State is illegal value '" + localState + "'");
		}
	}
}
