/**
 *
 */
package simple.lang;

/**
 * @author Brian S. Krug
 *
 */
public class InvalidValueException extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = 4431549050920739214L;
	protected final Object value;
	/**
	 *
	 */
	public InvalidValueException(Object value) {
		this.value = value;
	}

	/**
	 * @param message
	 */
	public InvalidValueException(String message, Object value) {
		super(message);
		this.value = value;
	}

	/**
	 * @param cause
	 */
	public InvalidValueException(Throwable cause, Object value) {
		super(cause);
		this.value = value;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidValueException(String message, Throwable cause, Object value) {
		super(message, cause);
		this.value = value;
	}

	public Object getValue() {
		return value;
	}
}
