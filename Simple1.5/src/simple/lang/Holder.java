/*
 * Created on Dec 21, 2005
 *
 */
package simple.lang;

public class Holder<E> {
    protected E value;
    public Holder() {
    }
    public E getValue() {
        return value;
    }
    public void setValue(E value) {
        this.value = value;
    }
}
