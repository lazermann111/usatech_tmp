package simple.xml;

import java.io.InputStream;
import java.io.Reader;

import org.w3c.dom.ls.LSInput;

public class LSInputImpl implements LSInput {
    
    protected InputStream in;
    
    protected String uri;
    
    /** Holds value of property baseURI. */
    private String baseURI;
    
    /** Holds value of property byteStream. */
    private InputStream byteStream;
    
    /** Holds value of property certifiedText. */
    private boolean certifiedText;
    
    /** Holds value of property characterStream. */
    private Reader characterStream;
    
    /** Holds value of property encoding. */
    private String encoding;
    
    /** Holds value of property publicId. */
    private String publicId;
    
    /** Holds value of property stringData. */
    private String stringData;
    
    /** Holds value of property systemId. */
    private String systemId;
    
    public LSInputImpl() {
    }
    
    /** Getter for property baseURI.
     * @return Value of property baseURI.
     *
     */
    public String getBaseURI() {
        return this.baseURI;
    }
    
    /** Setter for property baseURI.
     * @param baseURI New value of property baseURI.
     *
     */
    public void setBaseURI(String baseURI) {
        this.baseURI = baseURI;
    }
    
    /** Getter for property byteStream.
     * @return Value of property byteStream.
     *
     */
    public java.io.InputStream getByteStream() {
        return this.byteStream;
    }
    
    /** Setter for property byteStream.
     * @param byteStream New value of property byteStream.
     *
     */
    public void setByteStream(InputStream byteStream) {
        this.byteStream = byteStream;
    }
    
    /** Getter for property certifiedText.
     * @return Value of property certifiedText.
     *
     */
    public boolean getCertifiedText() {
        return this.certifiedText;
    }
    
    /** Setter for property certifiedText.
     * @param certifiedText New value of property certifiedText.
     *
     */
    public void setCertifiedText(boolean certifiedText) {
        this.certifiedText = certifiedText;
    }
    
    /** Getter for property characterStream.
     * @return Value of property characterStream.
     *
     */
    public java.io.Reader getCharacterStream() {
        return this.characterStream;
    }
    
    /** Setter for property characterStream.
     * @param characterStream New value of property characterStream.
     *
     */
    public void setCharacterStream(Reader characterStream) {
        this.characterStream = characterStream;
    }
    
    /** Getter for property encoding.
     * @return Value of property encoding.
     *
     */
    public String getEncoding() {
        return this.encoding;
    }
    
    /** Setter for property encoding.
     * @param encoding New value of property encoding.
     *
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
    
    /** Getter for property publicId.
     * @return Value of property publicId.
     *
     */
    public String getPublicId() {
        return this.publicId;
    }
    
    /** Setter for property publicId.
     * @param publicId New value of property publicId.
     *
     */
    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }
    
    /** Getter for property stringData.
     * @return Value of property stringData.
     *
     */
    public String getStringData() {
        return this.stringData;
    }
    
    /** Setter for property stringData.
     * @param stringData New value of property stringData.
     *
     */
    public void setStringData(String stringData) {
        this.stringData = stringData;
    }
    
    /** Getter for property systemId.
     * @return Value of property systemId.
     *
     */
    public String getSystemId() {
        return this.systemId;
    }
    
    /** Setter for property systemId.
     * @param systemId New value of property systemId.
     *
     */
    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }
        
}

