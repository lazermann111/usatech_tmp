/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import java.io.IOException;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import simple.bean.ConvertUtils;
import simple.xml.AbstractXMLReader;

/**
 * @author bkrug
 *
 */
public class AggregatingAdapter2 extends AbstractXMLReader<AggregatingInputSource> {
	protected static final String BASE_ELEM = "base";
	protected static final String SOURCE_ELEM = "source";
	protected static final String[] SOURCE_ATTS = new String[] {"name"};
	protected static final String PARAMETERS_ELEM = "parameters";
	protected static final String PARAMETER_ELEM = "parameter";
	protected static final String[] PARAMETER_ATTS = new String[] {"name", "value"};
	public static final String NAMESPACE = "http://simple/aggregating/2.0";

    /**
     *
     */
    public AggregatingAdapter2() {
    }

    @Override
	public void generateEvents(AggregatingInputSource inputSource) throws IOException, SAXException {
        Source[] sources = inputSource.getSources();
        String[] xpaths = inputSource.getXpaths();
        Map<String,?> parameters;
        if(inputSource instanceof AggregatingInputSource2) {
        	parameters = ((AggregatingInputSource2)inputSource).getParameters();
        } else {
        	parameters = null;
        }
        super.startDocument();
        startPrefixMapping(null, NAMESPACE);
        startElement(BASE_ELEM);
        if(parameters != null) {
        	startElement(PARAMETERS_ELEM);
        	for(Map.Entry<String, ?> entry : parameters.entrySet()) {
        		startElement(PARAMETER_ELEM, PARAMETER_ATTS, entry.getKey(), ConvertUtils.getStringSafely(entry.getValue()));
        		endElement(PARAMETER_ELEM);
        	}
        	endElement(PARAMETERS_ELEM);
        }
        for(int i = 0; i < sources.length; i++) {
        	startElement(SOURCE_ELEM, SOURCE_ATTS, (xpaths != null && xpaths.length > i ? xpaths[i] : null));
            if(sources[i] instanceof SAXSource) {
                processSAXSource((SAXSource) sources[i]);
            } else if(sources[i] instanceof DOMSource) {
                try {
                    TransformerFactory.newInstance().newTransformer().transform(sources[i], new SAXResult(this));
                } catch (TransformerConfigurationException e) {
                    throw new SAXException(e);
                } catch (TransformerException e) {
                    throw new SAXException(e);
                }
            } else if(sources[i] instanceof StreamSource) {
                processSAXSource(new SAXSource(new InputSource(((StreamSource)sources[i]).getInputStream())));
            }

            endElement(SOURCE_ELEM);
        }
        endElement(BASE_ELEM);
        super.endDocument();
    }

    protected void processSAXSource(SAXSource saxSrc) throws IOException, SAXException {
        XMLReader xmlReader = saxSrc.getXMLReader();
        xmlReader.setContentHandler(this);
        xmlReader.setDTDHandler(getDTDHandler());
        xmlReader.setEntityResolver(getEntityResolver());
        xmlReader.setErrorHandler(getErrorHandler());
        for(String feature : features)
            xmlReader.setFeature(feature, true);
        for(Map.Entry<String,Object> entry : properties.entrySet()) {
            xmlReader.setProperty(entry.getKey(), entry.getValue());
        }
        xmlReader.parse(saxSrc.getInputSource());
    }

    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#endDocument()
     */
    @Override
	public void endDocument() throws SAXException {
        //ignore it!!! (because we are aggregating documents and should only have one doc end and one doc start);
    }

    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#startDocument()
     */
    @Override
	public void startDocument() throws SAXException {
        //ignore it!!! (because we are aggregating documents and should only have one doc end and one doc start);
    }

    @Override
    protected String getNameSpaceURI() {
        return NAMESPACE;
    }
}
