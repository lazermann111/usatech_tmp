package simple.xml.sax;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import simple.io.Log;

public class LoggingErrorHandler implements ErrorHandler {
	private static final Log log = Log.getLog();
	public void error(SAXParseException exception) throws SAXException {
		if(log.isErrorEnabled()) {
			log.error(getMessage(exception), exception);
		}
	}

	public void fatalError(SAXParseException exception) throws SAXException {
		if(log.isFatalEnabled()) {
			log.fatal(getMessage(exception), exception);
		}
	}

	public void warning(SAXParseException exception) throws SAXException {
		if(log.isWarnEnabled()) {
			log.warn(getMessage(exception), exception);
		}
	}

	protected String getMessage(SAXParseException exception) {
		StringBuilder sb = new StringBuilder();
		buildMessage(sb, exception);
		return sb.toString();
	}
	
	protected void buildMessage(StringBuilder sb, SAXParseException exception) {
		sb.append("Parsing error in ").append(exception.getSystemId()).append(" at line ")
			.append(exception.getLineNumber()).append(" and column ").append(exception.getColumnNumber());	
	}
	
}
