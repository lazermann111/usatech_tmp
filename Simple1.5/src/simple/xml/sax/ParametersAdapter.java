/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.xml.AbstractXMLReader;

/**
 * @author bkrug
 *
 */
public class ParametersAdapter extends AbstractXMLReader<ParametersInputSource> {
    private static final Log log = Log.getLog();
    public static final String NAMESPACE = "http://simple/xml/parameters/1.0";
    protected Collection<String> ignore;
    
    public ParametersAdapter() {
        this.ignore = Collections.emptySet();
    }    

    public ParametersAdapter(Collection<String> ignore) {
    	if(ignore == null)
    		this.ignore = Collections.emptySet();
    	else
    		this.ignore = ignore;
    }
    
    public void generateEvents(ParametersInputSource inputSource) throws SAXException {
        startDocument();
        startPrefixMapping(null, NAMESPACE);
        String[] attNames = new String[] {"name", "value"};
        startElement("parameters", EMPTY_ATTS);
        for(Map.Entry<String,Object> entry : inputSource.getParameters().entrySet()) {
            if(ignore.contains(entry.getKey())) continue;
            try {
                startElement("parameter", attNames, entry.getKey(), ConvertUtils.convert(String.class, entry.getValue()));
                endElement("parameter");
            } catch(ConvertException e) {
                log.warn("Could not convert parameter '" + entry.getKey() + "' to a string: " + entry.getValue());
            }
        }
        endElement("parameters");
        endDocument();
    }

    @Override
    protected String getNameSpaceURI() {
        return NAMESPACE;
    }   
}
