/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.Format;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.text.StringUtils;
import simple.util.PrimitiveArrayList;
import simple.xml.AbstractXMLReader;

/**
 * @author bkrug
 *
 */
public class BeanAdapter0 extends AbstractXMLReader<BeanInputSource> {
    protected static final Set<Class<?>> primitiveClasses = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
        String.class,
        Number.class,
        BigDecimal.class,
        BigInteger.class,
        Long.class,
        Integer.class,
        Short.class,
        Byte.class,
        Character.class,
        Double.class,
        Float.class,
        java.util.Date.class,
        java.sql.Date.class,
        java.sql.Time.class,
        java.sql.Timestamp.class,       
        Class.class,       
        Boolean.class,       
        StringBuffer.class,
        StringBuilder.class,
        Format.class
    }));
    
    protected Set<String> EXCLUDE_PROPS = new HashSet<String>();
    protected String NS = "http://simple/bean/1.0";
    protected String CLASS_ATT = "class";
    protected String TO_STRING_ATT = "string-value";
    protected String ID_ATT = "id";
    protected String REFID_ATT = "refId";
    protected String POSITION_ATT = "position";
    protected String MAP_ENTRY_ELEM = "entry";
    protected String MAP_KEY_ELEM = "key";
    protected String MAP_VALUE_ELEM = "value";
    
    /**
     * 
     */
    public BeanAdapter0() {
        EXCLUDE_PROPS.add("class");
    }
    
    public void generateEvents(BeanInputSource inputSource) throws SAXException {
        startDocument();
        startPrefixMapping(null, NS);
        processBean(inputSource.getBean(), inputSource.getBaseTagName(), new HashMap<Object,Long>(), new AtomicLong());
        endDocument();
    }
    
    protected void processBean(Object bean, String beanTagName, Map<Object, Long> processed, AtomicLong lastId) throws SAXException {
        Class<?> vc = (bean == null ? null : bean.getClass());
        AttributesImpl atts = new AttributesImpl();
        atts.addAttribute(NS, CLASS_ATT, CLASS_ATT, "CDATA", (vc == null ? "" : vc.getName()));
        if(vc == null) {
            startElement(NS, beanTagName, beanTagName, atts);
        } else if(Enum.class.isAssignableFrom(vc)) {
            atts.addAttribute(NS, POSITION_ATT, POSITION_ATT, "CDATA", "" + (((Enum<?>)bean).ordinal() + 1));
            startElement(NS, beanTagName, beanTagName, atts);
            String s;
            try {
                s = ConvertUtils.convert(String.class, bean);
            } catch(ConvertException e) {
                s = bean.toString();
            }
            characters(s);
        } else if(isPrimitiveClass(vc)) {
            startElement(NS, beanTagName, beanTagName, atts);
            String s;
            try {
                s = ConvertUtils.convert(String.class, bean);
            } catch(ConvertException e) {
                s = bean.toString();
            }
            characters(s);
        } else {
            Long refId = processed.get(bean);
            if(refId != null) {
	        	//recursive 
	            atts.addAttribute(NS, REFID_ATT, REFID_ATT, "CDATA", "" + refId);
	            startElement(NS, beanTagName, beanTagName, atts);
            } else {
                processed.put(bean, lastId.incrementAndGet());               
                atts.addAttribute(NS, ID_ATT, ID_ATT, "CDATA", "" + lastId);
                if(Collection.class.isAssignableFrom(vc) || vc.isArray()) {
                	startElement(NS, beanTagName, beanTagName, atts);
                    String listName = StringUtils.singular(beanTagName);
                    //TODO: Make sure parent object does not have another property that is the same as what we'll use
	                if(listName.equals(beanTagName)) listName = listName + "-item";
	                Collection<?> coll;                   
	                if(bean instanceof Collection<?>) coll = (Collection<?>) bean;
	                else coll = new PrimitiveArrayList(bean);     
	                for(Iterator<?> iter = coll.iterator(); iter.hasNext();) {
	                    processBean(iter.next(), listName, processed, lastId);
	                }   
                } else if(Map.class.isAssignableFrom(vc)) {
                	startElement(NS, beanTagName, beanTagName, atts);
                    Map<?,?> map = (Map<?,?>)bean;
                    for(Map.Entry<?,?> entry : map.entrySet()) {
                        startElement(NS, MAP_ENTRY_ELEM, MAP_ENTRY_ELEM, EMPTY_ATTS);
                        Class<?> keyClass = (entry.getKey() != null ? entry.getKey().getClass() : null);
                        Class<?> valueClass = (entry.getKey() != null ? entry.getKey().getClass() : null);
                        String keyTagName = MAP_KEY_ELEM;
                        if(keyClass != null && (Collection.class.isAssignableFrom(keyClass) || keyClass.isArray())) {
                            keyTagName += "s";
                        }
                        processBean(entry.getKey(), keyTagName, processed, lastId);
                        String valueTagName = MAP_VALUE_ELEM;
                        if(valueClass != null && (Collection.class.isAssignableFrom(valueClass) || valueClass.isArray())) {
                            valueTagName += "s";
                        }
                        processBean(entry.getValue(), valueTagName, processed, lastId);
                        endElement(NS, MAP_ENTRY_ELEM, MAP_ENTRY_ELEM);  
                    }
                } else {
                	atts.addAttribute(NS, TO_STRING_ATT, TO_STRING_ATT, "CDATA", bean.toString());
                	startElement(NS, beanTagName, beanTagName, atts);
                    
	                Map<String,?> propMap;
                    try {
                        propMap = ReflectionUtils.toPropertyMap(bean, EXCLUDE_PROPS);
                    } catch (Exception e) {
                        throw new SAXException(e);
                    }
                    for(Map.Entry<String,?> entry : propMap.entrySet()) {
		                String propName = entry.getKey();
		                Object value;
		                if(bean instanceof Throwable && propName.equals("stackTrace")) {
		                    value = StringUtils.exceptionToString((Throwable)bean);
		                } else {
		                    value = entry.getValue();
		                }
		                processBean(value, propName, processed, lastId);   
		            }
	            }
                processed.remove(bean);                
            }            
        } 
        endElement(NS, beanTagName, beanTagName);
        
    }
   
    protected boolean isPrimitiveClass(Class<?> cls) {
        // this is strange but we don't need to print the string representation of Object (since it is meaningless)
        // also this makes the recursion on superclasses work
        if(cls == null || Object.class.equals(cls)) return false; 
        if(primitiveClasses.contains(ConvertUtils.convertToWrapperClass(cls))) return true;
        return isPrimitiveClass(cls.getSuperclass());
    }

    @Override
    protected String getNameSpaceURI() {
        return NS;
    }
}
