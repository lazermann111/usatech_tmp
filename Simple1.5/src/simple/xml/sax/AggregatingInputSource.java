/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import javax.xml.transform.Source;

import org.xml.sax.InputSource;

/**
 * @author bkrug
 *
 */
public class AggregatingInputSource extends InputSource {
    protected final Source[] sources;
    protected final String[] xpaths;
    protected final String[] namespaces;
    
    /**
     * 
     */
    public AggregatingInputSource(Source[] sources, String[] xpaths, String[] namespaces) {
        this.sources = sources;
        this.xpaths = xpaths;
        this.namespaces = namespaces;
    }

    /**
     * @return Returns the sources.
     */
    public Source[] getSources() {
        return sources;
    }
    
    /**
     * @return Returns the xpaths.
     */
    public String[] getXpaths() {
        return xpaths;
    }

	public String[] getNamespaces() {
		return namespaces;
	}
}
