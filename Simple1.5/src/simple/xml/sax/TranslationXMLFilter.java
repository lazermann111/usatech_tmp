/*
 * Created on Feb 1, 2006
 *
 */
package simple.xml.sax;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerFactory;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.translator.Translator;

public class TranslationXMLFilter extends ExtensionsXMLFilter {
    protected int translating = 0;
    protected String namespaceURI1 = "http://simple/translate/1.0";
    protected String namespaceURI2 = "http://simple/translate/2.0";
    protected String elementName = "translate";
    protected Translator translator;
    protected Element currentTranslateElement;
    protected class Element {
    	public String namespaceURI;
    	public String localName;
    	public String qName;
    	public Attributes atts;
    	public final List<Object> contents = new ArrayList<Object>();
    	public Element parent;
    }
    public TranslationXMLFilter(TransformerFactory factory, Translator translator) {
        super(factory);
        this.translator = translator;
    }

    public TranslationXMLFilter(XMLReader parent, TransformerFactory factory, Translator translator) {
        super(parent, factory);
        this.translator = translator;
    }

    @Override
	public void characters(char[] ch, int start, int length) throws SAXException {
        if(translating > 0) {
            currentTranslateElement.contents.add(new String(ch, start, length));
        } else {
            super.characters(ch, start, length);
        }
    }

    @Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        if(translating > 0) {
        	Element e = new Element();
        	e.namespaceURI = namespaceURI;
        	e.localName = e.localName;
        	e.qName = qName;
        	e.atts = new AttributesImpl(atts); // must copy these b/c atts are sometimes reused!
        	e.parent = currentTranslateElement;
        	currentTranslateElement.contents.add(e);
        	currentTranslateElement = e;
    	} else if((translating=isTranslateElement(namespaceURI, localName)) > 0) {
        	Element e = new Element();
        	e.namespaceURI = namespaceURI;
        	e.localName = e.localName;
        	e.qName = qName;
        	e.atts = new AttributesImpl(atts); // must copy these b/c atts are sometimes reused!
        	currentTranslateElement = e;
        } else {
        	super.startElement(namespaceURI, localName, qName, atts);
        }
    }

    @Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
    	if(translating > 0) {
    		if(currentTranslateElement.parent == null) {
    			translate(currentTranslateElement);
        		currentTranslateElement = null;
        		translating = 0;
    		} else {
    			currentTranslateElement = currentTranslateElement.parent;
    		}
    	} else {
    		super.endElement(namespaceURI, localName, qName);
	    }
    }

    protected int isTranslateElement(String namespaceURI, String localName) {
        if(localName.equals(getElementName())) {
        	if(namespaceURI1.equals(namespaceURI)) return 1;
        	if(namespaceURI2.equals(namespaceURI)) return 2;
        }
        return 0;
    }

    protected void translate(Element elem) throws SAXException {
    	switch(translating) {
	    	case 1:
	    		//compile all adjacent character sections and translate each
	    		String def = elem.atts.getValue("default");
	    		compileAndTranslateV1(elem.contents, def);
	    		break;
	    	case 2:
	    		String key = elem.atts.getValue("key");
	    		boolean markup;
	    		try {
					markup = ConvertUtils.getBoolean(elem.atts.getValue("is-markup"), false);
				} catch (ConvertException e) {
					throw new SAXException("Could not convert 'is-markup' attribute to a boolean", e);
				}
				String x = translator.translate(key, (String)null);
				if(x == null) {
					write(elem.contents);
				} else if(markup) {
					//parse
					XMLReader reader;
					try {
						reader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
					} catch (ParserConfigurationException e) {
						throw new SAXException(e);
					}
					if(getContentHandler() != null) {
						final ContentHandler delegate = getContentHandler();
						reader.setContentHandler(new ContentHandler() {
							protected int depth = 0;
							public void characters(char[] ch, int start, int length) throws SAXException {
								delegate.characters(ch, start, length);
							}

							public void endDocument() throws SAXException {
							}

							public void endElement(String uri, String localName, String qName) throws SAXException {
								if(--depth > 0) delegate.endElement(uri, localName, qName);
							}

							public void endPrefixMapping(String prefix) throws SAXException {
								delegate.endPrefixMapping(prefix);
							}

							public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
								delegate.ignorableWhitespace(ch, start, length);
							}

							public void processingInstruction(String target, String data) throws SAXException {
								delegate.processingInstruction(target, data);
							}

							public void setDocumentLocator(Locator locator) {
								delegate.setDocumentLocator(locator);
							}

							public void skippedEntity(String name) throws SAXException {
								delegate.skippedEntity(name);
							}

							public void startDocument() throws SAXException {
							}

							public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
								if(depth++ > 0)
									delegate.startElement(uri, localName, qName, atts);
							}

							public void startPrefixMapping(String prefix, String uri) throws SAXException {
								delegate.startPrefixMapping(prefix, uri);
							}
						});
						x = "<x>" + x + "</x>";
					}
					if(getDTDHandler() != null) reader.setDTDHandler(getDTDHandler());
					if(getEntityResolver() != null) reader.setEntityResolver(getEntityResolver());
					if(getErrorHandler() != null) reader.setErrorHandler(getErrorHandler());
					try {
						reader.parse(new InputSource(new StringReader(x)));
					} catch (IOException e) {
						throw new SAXException(e);
					}
				} else {
					super.characters(x.toCharArray(), 0, x.length());
				}
	    		break;
    	}
    }

    protected void write(List<Object> contents) throws SAXException {
    	for(Object o : contents) {
			if(o instanceof Element) {
				Element elem = (Element)o;
				super.startElement(elem.namespaceURI, elem.localName, elem.qName, elem.atts);
				write(elem.contents);
				super.endElement(elem.namespaceURI, elem.localName, elem.qName);
			} else {
				String x = o.toString();
				super.characters(x.toCharArray(), 0, x.length());
			}
		}
	}

	protected void compileAndTranslateV1(List<Object> contents, String defaultValue) throws SAXException {
    	String hold = null;
		for(Object o : contents) {
			if(o instanceof Element) {
				translateV1(hold, defaultValue);
				hold = null;
				defaultValue = null; // only use default once
				Element elem = (Element)o;
				super.startElement(elem.namespaceURI, elem.localName, elem.qName, elem.atts);
				compileAndTranslateV1(elem.contents, null);
				super.endElement(elem.namespaceURI, elem.localName, elem.qName);
			} else if(hold == null) {
				hold = o.toString();
			} else {
				hold = hold + o.toString();
			}
		}
		translateV1(hold, defaultValue);
	}

    protected void translateV1(String value, String defaultValue) throws SAXException {
    	if(value != null && value.length() > 0) {
			String x = translator.translate(value, defaultValue == null ? value : defaultValue);
            super.characters(x.toCharArray(), 0, x.length());
		}
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }
}
