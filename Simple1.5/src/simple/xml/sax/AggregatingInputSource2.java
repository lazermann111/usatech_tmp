/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import java.util.Map;

import javax.xml.transform.Source;

/**
 * @author bkrug
 *
 */
public class AggregatingInputSource2 extends AggregatingInputSource {
    protected Map<String,?> parameters;

    /**
     *
     */
    public AggregatingInputSource2(Source[] sources, String[] xpaths, String[] namespaces, Map<String,?> parameters) {
        super(sources, xpaths, namespaces);
        this.parameters = parameters;
    }

	public Map<String, ?> getParameters() {
		return parameters;
	}
}
