/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import org.xml.sax.InputSource;

import simple.results.Results;

/**
 * @author bkrug
 *
 */
public class ResultsInputSource extends InputSource {
    protected final Results results;
    protected final boolean metadataShown;

    /**
     *
     */
    public ResultsInputSource(Results results) {
        this(results, false);
    }

    /**
     *
     */
    public ResultsInputSource(Results results, boolean metadataShown) {
        this.results = results;
    	this.metadataShown = metadataShown;
    }
    /**
     * @return Returns the results.
     */
    public Results getResults() {
        return results;
    }

	public boolean isMetadataShown() {
		return metadataShown;
	}
}
