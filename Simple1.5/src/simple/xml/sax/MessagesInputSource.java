package simple.xml.sax;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;

import simple.text.MessageFormat;
import simple.text.StringUtils;
import simple.translator.Translator;

public class MessagesInputSource extends PlaybackInputSource {
	protected static final String NAMESPACE_URI = "http://simple/messages/1.0";
	protected static final SAXParserFactory factory = SAXParserFactory.newInstance();
	protected final Translator translator;
	protected final List<Message> messages = new ArrayList<Message>();
	protected final List<Message> messagesUnmod = Collections.unmodifiableList(messages);

	public static class Message {
		protected final String text;
		protected final String type;
		protected final boolean unescaped;

		protected Message(String text, String type, boolean unescaped) {
			this.text = text;
			this.type = type;
			this.unescaped = unescaped;
		}

		public String getType() {
			return type;
		}

		public String toHtml() {
			return isUnescaped() ? text : StringUtils.prepareHTML(text);
		}

		public boolean isUnescaped() {
			return unescaped;
		}
	}
	public MessagesInputSource(Translator translator) {
		this.translator = translator;
	}

	public void addMessage(String type, String messageKey, Object... parameters) {
		addMessage(type, messageKey, messageKey, parameters);
	}

	public void addMessage(String type, String messageKey, String defaultText, Object... parameters) {
		String text;
		if(translator != null) {
			text = translator.translate(messageKey, defaultText, parameters);
		} else {
			text = new MessageFormat(messageKey).format(parameters);
		}

		AttributesImpl atts = new AttributesImpl();
		atts.addAttribute(NAMESPACE_URI, "type", "type", "CDATA", type);
		startElementNoCopy(NAMESPACE_URI, "message", "message", atts);
		characters(text.toCharArray(), 0, text.length());
		endElement(NAMESPACE_URI, "message", "message");
		messages.add(new Message(text, type, false));
	}

	public void addHtmlMessage(String type, String messageKey, Object... parameters) throws IOException, SAXException, ParserConfigurationException {
		addHtmlMessage(type, messageKey, messageKey, parameters);
	}

	public void addHtmlMessage(String type, String messageKey, String defaultHtml, Object... parameters) throws IOException, SAXException, ParserConfigurationException {
		String html;
		if(translator != null) {
			html = translator.translate(messageKey, defaultHtml, parameters);
		} else {
			html = new MessageFormat(messageKey).format(parameters);
		}
		
		AttributesImpl atts = new AttributesImpl();
		atts.addAttribute(NAMESPACE_URI, "type", "type", "CDATA", type);
		startElementNoCopy(NAMESPACE_URI, "message", "message", atts);
		// parse html
		SAXParser parser = factory.newSAXParser();
		XMLReader xr = parser.getXMLReader();
		xr.setContentHandler(this);
		xr.parse(new InputSource(new StringReader(html)));
		endElement(NAMESPACE_URI, "message", "message");
		messages.add(new Message(html, type, true));
	}


	public int getMessageCount() {
		return messages.size();
	}

	public void clear() {
		playbacks.clear();
		messages.clear();
	}

	public List<Message> getMessages() {
		return messagesUnmod;
	}
}
