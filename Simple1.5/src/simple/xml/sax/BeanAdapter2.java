/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import java.awt.Color;
import java.awt.Dimension;
import java.lang.reflect.InvocationTargetException;
import java.text.Format;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.bean.ReflectionUtils.BeanProperty;
import simple.sql.Expression;
import simple.text.StringUtils;
import simple.util.PrimitiveArrayList;
import simple.xml.AbstractXMLReader;

/**
 * @author bkrug
 *
 */
public class BeanAdapter2 extends AbstractXMLReader<BeanInputSource> {
    protected static final Set<Class<?>> simpleClasses = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
	        Number.class,
            Character.class,
            java.util.Date.class,
            Class.class,
            Boolean.class,
            CharSequence.class,
            Format.class,
            Expression.class,
            Color.class,
            Dimension.class,
            Locale.class
    }));

    protected String NS = "http://simple/bean/2.0";
    protected String CLASS_ATT = "class";
    protected String ID_ATT = "id";
    protected String REFID_ATT = "refId";
    protected String INDEX_ATT = "index";
    protected String MAP_ENTRY_ELEM = "entry";
    protected String MAP_KEY_ELEM = "key";
    protected String MAP_VALUE_ELEM = "value";
    protected String BEAN_ELEM = "bean";
    protected String PROPERTY_ELEM = "property";
    protected String ELEMENT_ELEM = "element";
    protected String NAME_ATT = "name";
    protected String DECLARED_ATT = "declaredClass";

    protected static boolean isSimpleType(Class<?> cls) {
        // this is strange but we don't need to print the string representation of Object (since it is meaningless)
        // also this makes the recursion on superclasses work
        if(cls == null || Object.class.equals(cls)) return false;
        if(simpleClasses.contains(cls)) return true;
        Class<?>[] ifs = cls.getInterfaces();
        if(ifs != null)
        	for(Class<?> i : ifs)
        		if(simpleClasses.contains(i))
        			return true;
        return isSimpleType(cls.getSuperclass());
    }

    /**
     *
     */
    public BeanAdapter2() {
    }

    @Override
	public void generateEvents(BeanInputSource inputSource) throws SAXException {
        startDocument();
        startPrefixMapping(null, NS);
        processBean(new AttributesImpl(), inputSource.getBean(), BEAN_ELEM, new HashMap<Object,Long>(), new AtomicLong());
        endDocument();
    }

    protected void processBean(AttributesImpl atts, Object bean, String beanTagName, Map<Object, Long> processed, AtomicLong lastId) throws SAXException {
        Class<?> vc = (bean == null ? null : bean.getClass());
        atts.addAttribute(NS, CLASS_ATT, CLASS_ATT, "CDATA", (vc == null ? "" : vc.getName()));
        if(vc == null) {
            startElement(NS, beanTagName, beanTagName, atts);
        } else if(Enum.class.isAssignableFrom(vc)) {
            atts.addAttribute(NS, INDEX_ATT, INDEX_ATT, "CDATA", String.valueOf(((Enum<?>)bean).ordinal()));
            startElement(NS, beanTagName, beanTagName, atts);
            String s = ConvertUtils.getStringSafely(bean);
            characters(s);
        } else if(isSimpleType(vc)) {
            startElement(NS, beanTagName, beanTagName, atts);
            String s = ConvertUtils.getStringSafely(bean);
            characters(s);
        } else {
            Long refId = processed.get(bean);
            if(refId != null) {
	        	//recursive
	            atts.addAttribute(NS, REFID_ATT, REFID_ATT, "CDATA", String.valueOf(refId));
	            startElement(NS, beanTagName, beanTagName, atts);
            } else {
                processed.put(bean, lastId.incrementAndGet());
                atts.addAttribute(NS, ID_ATT, ID_ATT, "CDATA", String.valueOf(lastId));
                if(Collection.class.isAssignableFrom(vc) || vc.isArray()) {
                	startElement(NS, beanTagName, beanTagName, atts);
                    Collection<?> coll;
	                if(bean instanceof Collection<?>) coll = (Collection<?>) bean;
	                else coll = new PrimitiveArrayList(bean);
	                for(Object subBean : coll) {
	                	atts.clear();
	                    processBean(atts, subBean, ELEMENT_ELEM, processed, lastId);
	                }
                } else if(Map.class.isAssignableFrom(vc)) {
                	startElement(NS, beanTagName, beanTagName, atts);
                    Map<?,?> map = (Map<?,?>)bean;
                    for(Map.Entry<?,?> entry : map.entrySet()) {
                        startElement(NS, MAP_ENTRY_ELEM, MAP_ENTRY_ELEM, EMPTY_ATTS);
                        atts.clear();
	                    processBean(atts, entry.getKey(), MAP_KEY_ELEM, processed, lastId);
	                    atts.clear();
	                    processBean(atts, entry.getValue(), MAP_VALUE_ELEM, processed, lastId);
                        endElement(NS, MAP_ENTRY_ELEM, MAP_ENTRY_ELEM);
                    }
                } else {
                	startElement(NS, beanTagName, beanTagName, atts);

	                Collection<BeanProperty> properties;
                    try {
                    	properties = ReflectionUtils.getBeanProperties(vc);
                    } catch (Exception e) {
                        throw new SAXException(e);
                    }
                    for(BeanProperty bp : properties) {
                    	if(!bp.isReadable() || bp.getName().equals("class"))
                    		continue;
		                Object value;
		                if(bean instanceof Throwable && bp.getName().equals("stackTrace")) {
		                    value = StringUtils.exceptionToString((Throwable)bean);
		                } else {
		                    try {
								value = bp.getValue(bean);
							} catch(IllegalArgumentException e) {
								throw new SAXException(e);
							} catch(IllegalAccessException e) {
								throw new SAXException(e);
							} catch(InvocationTargetException e) {
								throw new SAXException(e);
							}
		                }
		                atts.clear();
	                    atts.addAttribute(NS, NAME_ATT, NAME_ATT, "CDATA", bp.getName());
	                    atts.addAttribute(NS, DECLARED_ATT, DECLARED_ATT, "CDATA", bp.getType().getName());
	                    processBean(atts, value, PROPERTY_ELEM, processed, lastId);
		            }
	            }
                processed.remove(bean);
            }
        }
        endElement(NS, beanTagName, beanTagName);

    }

    @Override
    protected String getNameSpaceURI() {
        return NS;
    }

}
