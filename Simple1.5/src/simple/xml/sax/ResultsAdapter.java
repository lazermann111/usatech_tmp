/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import java.util.LinkedHashSet;
import java.util.Set;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.text.StringUtils;
import simple.xml.AbstractXMLReader;

/**
 * @author bkrug
 *
 */
public class ResultsAdapter extends AbstractXMLReader<ResultsInputSource> {
    public static final String NAMESPACE = "http://simple/results/1.0";
    protected static final String RESULTS_ELEMENT = "results";
    protected static final String ROW_ELEMENT = "row";
    protected static final String VALUE_ATT = "value";
    protected static final String GROUP_ELEMENT = "group";
    protected static final String TOTAL_ELEMENT = "total";
    protected static final String COLUMN_ELEMENT = "column";
    protected static final String[] COLUMN_ATTS = new String[] {"name", "class"};
	protected boolean groupingEnabled = true;

    /**
     *
     */
    public ResultsAdapter() {
    }

    @Override
	public void generateEvents(ResultsInputSource inputSource) throws SAXException {
        Results results = inputSource.getResults();
        startDocument();
        startPrefixMapping("", NAMESPACE);
        startElement(RESULTS_ELEMENT, EMPTY_ATTS);
        Aggregate[][] aggregates = new Aggregate[results.getColumnCount() + 1][];
        boolean totals = false;
        Set<String> columnNameSet = new LinkedHashSet<String>(); // retain order
        for(int i = 0; i < results.getColumnCount(); i++) {
        	String columnName = results.getColumnName(i+1);
        	if(inputSource.isMetadataShown()) {
        		Class<?> columnClass = results.getColumnClass(i+1);
                startElement(COLUMN_ELEMENT, COLUMN_ATTS, columnName, columnClass == null ? null : columnClass.getName());
                endElement(COLUMN_ELEMENT);
        	}
        	columnNameSet.add(columnName);
            aggregates[i] = results.getAggregatesTracked(i+1);
            if(aggregates[i] != null && aggregates[i].length > 0)
                totals = true;
        }
		String[] groupNames = isGroupingEnabled() ? results.getGroupNames() : StringUtils.EMPTY_ARRAY;
        for(String gn : groupNames) {
            columnNameSet.remove(gn);
        }
        while(results.next()) {
            for(String gn : groupNames) {
                if(results.isGroupBeginning(gn)) {
                    startElement(GROUP_ELEMENT, EMPTY_ATTS);
                    Object obj = results.getValue(gn);
                    String text = results.getFormattedValue(gn);
                    String value = (obj == null ? "" : obj.toString());
                    AttributesImpl atts = new AttributesImpl();
                    atts.addAttribute(NAMESPACE, VALUE_ATT, VALUE_ATT, "CDATA", value);
                    startElement(gn, atts);
                    characters(text);
                    endElement(gn);
                }
            }
            startElement(ROW_ELEMENT, EMPTY_ATTS);
            for(String cn : columnNameSet) {
	            Object obj = results.getValue(cn);
	            String text = results.getFormattedValue(cn);
	            String value = (obj == null ? "" : obj.toString());
	            AttributesImpl atts = new AttributesImpl();
	            atts.addAttribute(NAMESPACE, VALUE_ATT, VALUE_ATT, "CDATA", value);
	            startElement(cn, atts);
	            characters(text);
	            endElement(cn);
	        }
            endElement(ROW_ELEMENT);
            for(String gn : groupNames) {
                if(results.isGroupEnding(gn)) {
                    if(totals) {
                        startElement(TOTAL_ELEMENT, EMPTY_ATTS);
                        for(int i = 0; i < aggregates.length; i++) {
                            if(aggregates[i] != null) {
                                String cn = results.getColumnName(i+1);
                                startElement(cn, EMPTY_ATTS);
                                for(Aggregate a : aggregates[i]) {
                                    Object obj = results.getAggregate(cn, gn, a);
                                    String text = results.getFormattedAggregate(cn, gn, a);
                                    String value = (obj == null ? "" : obj.toString());
                                    AttributesImpl atts = new AttributesImpl();
                                    atts.addAttribute(NAMESPACE, VALUE_ATT, VALUE_ATT, "CDATA", value);
                                    startElement(a.toString(), atts);
                                    characters(text);
                                    endElement(a.toString());
                                }
                                endElement(cn);
                            }
                        }
                        endElement(TOTAL_ELEMENT);
                    }
                    endElement(GROUP_ELEMENT);
                }
            }
        }
        endElement(RESULTS_ELEMENT);
        endDocument();
    }

    @Override
    protected String getNameSpaceURI() {
        return NAMESPACE;
    }

	public boolean isGroupingEnabled() {
		return groupingEnabled;
	}

	public void setGroupingEnabled(boolean groupingEnabled) {
		this.groupingEnabled = groupingEnabled;
	}
}
