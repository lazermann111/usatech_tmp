/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import java.util.Map;

import org.xml.sax.InputSource;

/**
 * @author bkrug
 *
 */
public class ParametersInputSource extends InputSource {
    protected Map<String,Object> parameters;
    
    /**
     * 
     */
    public ParametersInputSource(Map<String,Object> parameters) {
        this.parameters = parameters;
    }

    /**
     * @return Returns the parameters.
     */
    public Map<String, Object> getParameters() {
        return parameters;
    }

}
