/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.xml.AbstractXMLReader;
import simple.xml.XMLBuilder;

/**
 * @author bkrug
 *
 */
public class BeanAdapter1 extends AbstractXMLReader<BeanInputSource> {
    protected String NS = "http://simple/bean/1.0";
    protected final XMLBuilder builder;
    
    public BeanAdapter1() {
    	builder = new XMLBuilder(this);
    }
    
    public void generateEvents(BeanInputSource inputSource) throws SAXException {
    	builder.setNamespace(getNameSpaceURI());
    	try {
			builder.put(inputSource.getBaseTagName(), inputSource.getBean());
		} catch(ConvertException e) {
			throw new SAXException("Conversion error", e);
		}
    }
    
    @Override
    protected String getNameSpaceURI() {
        return NS;
    }
}
