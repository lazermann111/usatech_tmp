/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import java.util.LinkedHashSet;
import java.util.Set;

import org.xml.sax.SAXException;

import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.xml.AbstractXMLReader;

/**
 * @author bkrug
 *
 */
public class ResultsAdapter2 extends AbstractXMLReader<ResultsInputSource> {
    public static final String NAMESPACE = "http://simple/results/2.0";
    protected static final String RESULTS_ELEMENT = "results";
    //protected static final String COLUMNS_ELEMENT = "columns";
    protected static final String COLUMN_ELEMENT = "column";
    //protected static final String ROWS_ELEMENT = "rows";
    protected static final String ROW_ELEMENT = "row";
    protected static final String DATA_ELEMENT = "data";
    protected static final String TOTAL_ELEMENT = "total";
    protected static final String GROUP_ELEMENT = "group";

    protected static final String[] COLUMN_ATTS = new String[] {"index", "name", "class"};
    protected static final String[] DATA_ATTS = new String[] {"value", "columnIndex", "columnName", "class"};
    protected static final String[] TOTAL_ATTS = new String[] {"type", "value", "columnIndex", "columnName", "class"};

    /**
     *
     */
    public ResultsAdapter2() {
    }

    @Override
	public void generateEvents(ResultsInputSource inputSource) throws SAXException {
        Results results = inputSource.getResults();
        startDocument();
        startPrefixMapping("", NAMESPACE);
        startElement(RESULTS_ELEMENT);
        Aggregate[][] aggregates = new Aggregate[results.getColumnCount() + 1][];
        boolean totals = false;
        Set<String> columnNameSet = new LinkedHashSet<String>(); // retain order
        //startElement(COLUMNS_ELEMENT);
        for(int i = 0; i < results.getColumnCount(); i++) {
        	String name = results.getColumnName(i+1);
            columnNameSet.add(name);
            aggregates[i] = results.getAggregatesTracked(i+1);
            if(aggregates[i] != null && aggregates[i].length > 0)
                totals = true;
            startElement(COLUMN_ELEMENT, COLUMN_ATTS, String.valueOf(i+1), name, results.getColumnClass(i+1).getName());
            endElement(COLUMN_ELEMENT);
        }
        //endElement(COLUMNS_ELEMENT);
        String[] groupNames = results.getGroupNames();
        for(String gn : groupNames) {
            columnNameSet.remove(gn);
        }
        while(results.next()) {
            for(String gn : groupNames) {
                if(results.isGroupBeginning(gn)) {
                    startElement(GROUP_ELEMENT);
                    Object obj = results.getValue(gn);
                    String text = results.getFormattedValue(gn);
                    String value = (obj == null ? "" : obj.toString());
                    startElement(DATA_ELEMENT, DATA_ATTS, value, String.valueOf(results.getColumnIndex(gn)), gn, obj == null ? "" : obj.getClass().getName());
                    characters(text);
                    endElement(DATA_ELEMENT);
                }
            }
            startElement(ROW_ELEMENT);
            for(String cn : columnNameSet) {
	            Object obj = results.getValue(cn);
	            String text = results.getFormattedValue(cn);
	            String value = (obj == null ? "" : obj.toString());
	            startElement(DATA_ELEMENT, DATA_ATTS, value, String.valueOf(results.getColumnIndex(cn)), cn, obj == null ? "" : obj.getClass().getName());
                characters(text);
                endElement(DATA_ELEMENT);
	        }
            endElement(ROW_ELEMENT);
            for(String gn : groupNames) {
                if(results.isGroupEnding(gn)) {
                    if(totals) {
                        //startElement(TOTAL_ELEMENT, EMPTY_ATTS);
                        for(int i = 0; i < aggregates.length; i++) {
                            if(aggregates[i] != null) {
                                String cn = results.getColumnName(i+1);
                                for(Aggregate a : aggregates[i]) {
                                    Object obj = results.getAggregate(cn, gn, a);
                                    String text = results.getFormattedAggregate(cn, gn, a);
                                    String value = (obj == null ? "" : obj.toString());
                                    startElement(TOTAL_ELEMENT, TOTAL_ATTS, a.toString(), value, String.valueOf(results.getColumnIndex(cn)), cn, obj == null ? "" : obj.getClass().getName());
                                    characters(text);
                                    endElement(TOTAL_ELEMENT);
                                }
                            }
                        }
                        //endElement(TOTAL_ELEMENT);
                    }
                    endElement(GROUP_ELEMENT);
                }
            }
        }
        endElement(RESULTS_ELEMENT);
        endDocument();
    }

    @Override
    protected String getNameSpaceURI() {
        return NAMESPACE;
    }
}
