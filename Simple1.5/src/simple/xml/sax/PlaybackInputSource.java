package simple.xml.sax;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.AttributesImpl;

import simple.xml.EmptyAttributes;
import simple.xml.ExtendedContentHandler;

public class PlaybackInputSource extends InputSource implements ExtendedContentHandler {
	protected interface Playback {
		public void invoke(ContentHandler contentHandler) throws SAXException;
	}
	protected final List<Playback> playbacks = new ArrayList<Playback>();
	
	public PlaybackInputSource() {
		super();
	}

	@Override
	public void comment(final String comment) throws SAXException {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				if(contentHandler instanceof LexicalHandler)
					((LexicalHandler) contentHandler).comment(comment.toCharArray(), 0, comment.length());
			}
		});
	}

	public void characters(final char[] ch, final int start, final int length) {
		final char[] copy = new char[length];
		System.arraycopy(ch, start, copy, 0, length);
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.characters(copy, 0, length);
			}			
		});
	}

	public void endDocument() {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.endDocument();
			}			
		});
	}

	public void endElement(final String uri, final String localName, final String qName) {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.endElement(uri, localName, qName);
			}			
		});
	}

	public void endPrefixMapping(final String prefix) {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.endPrefixMapping(prefix);
			}			
		});
	}

	public void ignorableWhitespace(final char[] ch, final int start, final int length) {
		final char[] copy = new char[length];
		System.arraycopy(ch, start, copy, 0, length);
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.ignorableWhitespace(copy, 0, length);
			}			
		});
	}

	public void processingInstruction(final String target, final String data) {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.processingInstruction(target, data);
			}			
		});
	}

	public void setDocumentLocator(final Locator locator) {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) {
				contentHandler.setDocumentLocator(locator);
			}			
		});
	}

	public void skippedEntity(final String name) {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.skippedEntity(name);
			}			
		});
	}

	public void startDocument() {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.startDocument();
			}			
		});
	}
	/** Records a startElement event, but does not copy the atts. Use this method ONLY if you never make changes to the atts object
	 *  after you call this method.
	 * @param uri
	 * @param localName
	 * @param qName
	 * @param atts
	 * @throws SAXException
	 */
	public void startElementNoCopy(final String uri, final String localName, final String qName, final Attributes atts) {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.startElement(uri, localName, qName, atts);
			}			
		});
	}
	
	public void startElement(String uri, String localName, String qName, Attributes atts) {
		startElementNoCopy(uri, localName, qName, new AttributesImpl(atts));
	}

	public void startPrefixMapping(final String prefix, final String uri) {
		playbacks.add(new Playback() {
			public void invoke(ContentHandler contentHandler) throws SAXException {
				contentHandler.startPrefixMapping(prefix, uri);
			}			
		});
	}

	public void playback(ContentHandler contentHandler) throws SAXException {
		for(Playback pb : playbacks) {
			pb.invoke(contentHandler);
		}
	}
	
	//convenience methods
	protected static final Attributes EMPTY_ATTS = new EmptyAttributes();
    
	protected String nameSpaceURI ;
	protected String prefix ;
    
	public void characters(String ch) {
        if(ch != null) characters(ch.toCharArray(), 0, ch.length());
    }
	public void startElement(String localName) {
        startElement(getNameSpaceURI(), localName, getQName(localName), EMPTY_ATTS);
    }
	public void startElement(String localName, Map<String,String> attributes) {
        AttributesImpl atts = new AttributesImpl();
        for(Map.Entry<String, String> entry : attributes.entrySet()) {
            atts.addAttribute(getNameSpaceURI(), entry.getKey(), getQName(entry.getKey()), "CDATA", entry.getValue());       
        }
        startElement(getNameSpaceURI(), localName, getQName(localName), atts);
    }
	public void startElement(String localName, String[] attributeNames, String... attributeValues) {
        AttributesImpl atts = new AttributesImpl();
        for(int i = 0; i < attributeNames.length && i < attributeValues.length; i++) {
            atts.addAttribute(getNameSpaceURI(), attributeNames[i], getQName(attributeNames[i]), "CDATA", attributeValues[i]);       
        }
        startElement(getNameSpaceURI(), localName, getQName(localName), atts);
    }
	public void startElement(String localName, Attributes atts) {
        startElement(getNameSpaceURI(), localName, getQName(localName), atts);
    }
	public void endElement(String localName) {
        endElement(getNameSpaceURI(), localName, getQName(localName));
    }
    protected String getQName(String localName) {
        if(getPrefix() != null && getPrefix().length() > 0) 
            return getPrefix() + ":" + localName;
        else 
            return localName;            
    }

	public String getNameSpaceURI() {
		return nameSpaceURI;
	}

	public void setNameSpaceURI(String nameSpaceURI) {
		this.nameSpaceURI = nameSpaceURI;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	public int getCommandCount() {
		return playbacks.size();
	}
}
