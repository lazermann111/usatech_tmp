/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import simple.text.StringUtils;
import simple.xml.AbstractXMLReader;

/**
 * @author bkrug
 *
 */
public class AggregatingAdapter extends AbstractXMLReader<AggregatingInputSource> {
    protected static final char[] XPATH_DELIMS = "/".toCharArray();
    public static final String NAMESPACE = "http://simple/aggregating/1.0";
    protected String defaultNamespace;
    
    /**
     * 
     */
    public AggregatingAdapter() {
    }

    public void generateEvents(AggregatingInputSource inputSource) throws IOException, SAXException {
        Source[] sources = inputSource.getSources();
        String[] xpaths = inputSource.getXpaths();
        String[] namespaces = inputSource.getNamespaces();
        String ROOT_ELEMENT = "base";
        super.startDocument();
        startPrefixMapping(null, NAMESPACE);
        startElement(NAMESPACE, ROOT_ELEMENT, ROOT_ELEMENT, EMPTY_ATTS); 
        List<String> elemList = new ArrayList<String>();
        for(int i = 0; i < sources.length; i++) {
            if(xpaths != null && xpaths.length > i) {
            	String[] splits = StringUtils.split(xpaths[i], XPATH_DELIMS, false);
                for(int k = 0; k < splits.length; k++) {
                    String tagname = splits[k].trim(); //RegexUtils.substitute("\\s", splits[k].trim(), "-", null);
                    if(tagname.length() > 0) elemList.add(tagname);
                    startElement(NAMESPACE, tagname, tagname, EMPTY_ATTS); 
                }
            }
            if(namespaces != null && namespaces.length > i && namespaces[i] != null) {
        		defaultNamespace = namespaces[i];
        	} else
        		defaultNamespace = null;
            if(sources[i] instanceof SAXSource) {
                processSAXSource((SAXSource) sources[i]);               
            } else if(sources[i] instanceof DOMSource) {
                try {
                    TransformerFactory.newInstance().newTransformer().transform(sources[i], new SAXResult(this));
                } catch (TransformerConfigurationException e) {
                    throw new SAXException(e);
                } catch (TransformerException e) {
                    throw new SAXException(e);
                }
            } else if(sources[i] instanceof StreamSource) {
                processSAXSource(new SAXSource(new InputSource(((StreamSource)sources[i]).getInputStream())));
            }
            defaultNamespace = null;    
            for(int k = 0; k < elemList.size(); k++) {
                String tagname = elemList.get(k);
                endElement(NAMESPACE, tagname, tagname);
            }
            elemList.clear();            
        }
        endElement(NAMESPACE, ROOT_ELEMENT, ROOT_ELEMENT);
        super.endDocument();
    }
    
    protected void processSAXSource(SAXSource saxSrc) throws IOException, SAXException {
        XMLReader xmlReader = saxSrc.getXMLReader();
        if(xmlReader == null)
        	xmlReader = XMLReaderFactory.createXMLReader();
        xmlReader.setContentHandler(this);
        xmlReader.setDTDHandler(this);
        xmlReader.setEntityResolver(this);
        xmlReader.setErrorHandler(this);
        xmlReader.setProperty("http://xml.org/sax/properties/lexical-handler", this);
        for(String feature : features)
            xmlReader.setFeature(feature, true);
        for(Map.Entry<String,Object> entry : properties.entrySet()) {
            xmlReader.setProperty(entry.getKey(), entry.getValue());                   
        }
        xmlReader.parse(saxSrc.getInputSource());
    }
    
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#endDocument()
     */
    public void endDocument() throws SAXException {
        //ignore it!!! (because we are aggregating documents and should only have one doc end and one doc start);
    }
    
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#startDocument()
     */
    public void startDocument() throws SAXException {
        //ignore it!!! (because we are aggregating documents and should only have one doc end and one doc start);
    }

    @Override
    protected String getNameSpaceURI() {
        return NAMESPACE;
    }
    
    @Override
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
    	super.endElement(((namespaceURI == null || namespaceURI.length() == 0) && defaultNamespace != null ? defaultNamespace : namespaceURI), localName, qName);
    }
    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
    	super.startElement(((namespaceURI == null || namespaceURI.length() == 0) && defaultNamespace != null ? defaultNamespace : namespaceURI), localName, qName, atts);
    }  
}
