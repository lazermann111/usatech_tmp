/*
 * Created on Feb 1, 2006
 *
 */
package simple.xml.sax;

import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.TransformerFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.XMLFilterImpl;

import simple.io.Log;

/** This class maps the java extension namespaces and custom output methods for various TransformerFactory 
 * implementations, allowing the xsl document to remain generic.
 * @author bkrug
 *
 */
public class ExtensionsXMLFilter extends XMLFilterImpl {
    protected static final String outputURI = "http://www.w3.org/1999/XSL/Transform";
    protected static final String outputElementName = "output";
    protected static final String methodAttributeName = "method";
    protected static class FactoryImplType { 
        protected String namespaceURI;
        protected String javaURI;
        protected String methodValue;
        protected Attributes outputAtts;
        protected int methodIndex;
        public FactoryImplType(String namespaceURI, Attributes outputAtts) {
            this.namespaceURI = namespaceURI;
            this.javaURI = namespaceURI + "/java"; 
            this.outputAtts = outputAtts;
            this.methodIndex = outputAtts.getIndex(methodAttributeName);
        }
    }
    protected static final Map<String,FactoryImplType> factoryImplTypes = new HashMap<String, FactoryImplType>();
    static {
        AttributesImpl ai = new AttributesImpl();
        ai.addAttribute(null, "xmlns:jclark-java", "xmlns:jclark-java", "CDATA", "http://www.jclark.com/xt/java");
        ai.addAttribute(null, methodAttributeName, methodAttributeName, "CDATA", "jclark-java:simple.xml.serializer.XHTMLOutputHandler");
        registerFactoryImpl("com.jclark.xsl.trax.TransformerFactoryImpl", "http://www.jclark.com/xt", ai);
        ai = new AttributesImpl();
        ai.addAttribute("http://xml.apache.org/xalan", "content-handler", "xalan:content-handler", "CDATA", "simple.xml.serializer.ToXHTMLStream");
        registerFactoryImpl("org.apache.xalan.processor.TransformerFactoryImpl", "http://xml.apache.org/xalan", ai);
        ai = new AttributesImpl();
        /* NOTE: XSLTC in Xalan does not support custom content handlers*/
        //ai.addAttribute("http://xml.apache.org/xalan", "content-handler", "xalan:content-handler", "CDATA", "simple.xml.serializer.ToXHTMLStream");
        registerFactoryImpl("com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl", "http://xml.apache.org/xalan", ai);
        ai = new AttributesImpl();
        /* NOTE: XSLTC in Xalan does not support custom content handlers*/
        //ai.addAttribute("http://xml.apache.org/xalan", "content-handler", "xalan:content-handler", "CDATA", "simple.xml.serializer.ToXHTMLStream");
        registerFactoryImpl("org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl", "http://xml.apache.org/xalan", ai);
    }
    /**
     * @param factoryClassName
     * @param baseURI
     * @param outputAttributes
     */
    public static void registerFactoryImpl(String factoryClassName, String baseURI, Attributes outputAttributes) {
        factoryImplTypes.put(factoryClassName, new FactoryImplType(baseURI, outputAttributes));
    }

    
    protected String javaURI = "http://simple/xml/extensions/java";
    protected TransformerFactory factory;
    protected FactoryImplType factoryImplType;
    
    public ExtensionsXMLFilter(TransformerFactory factory) {
        super();
        setFactory(factory);
    }

    public ExtensionsXMLFilter(XMLReader parent, TransformerFactory factory) {
        super(parent);
        setFactory(factory);
    }
    
    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        if(outputURI.equals(namespaceURI) && outputElementName.equals(localName)) {
            int index = atts.getIndex(methodAttributeName);
            if(index > -1) {
                String value = atts.getValue(index);
                if("xhtml".equalsIgnoreCase(value)) {
                    FactoryImplType fit = getFactoryImplType();
                    if(fit != null) {
                        AttributesImpl ai;
                        if(atts instanceof AttributesImpl) {
                            ai = (AttributesImpl)atts;
                        } else {
                            ai = new AttributesImpl(atts);
                        }                       
                        for(int i = 0; i < fit.outputAtts.getLength(); i++) {
                            if(fit.methodIndex == i) {
                                ai.setValue(index, fit.outputAtts.getValue(fit.methodIndex));                            
                            } else {
                                ai.addAttribute(fit.outputAtts.getURI(i), fit.outputAtts.getLocalName(i), fit.outputAtts.getQName(i), fit.outputAtts.getType(i), fit.outputAtts.getValue(i));
                            }
                        }
                        atts = ai;                            
                    }
                }
            }
        }
        super.startElement(namespaceURI, localName, qName, atts);
    }
    
    /**
     * @return
     */
    protected FactoryImplType getFactoryImplType() {
        return factoryImplType;
    }

    /**
     * This method maps from the java URI to the appropriate factory-specific uri. 
     * NOTE: We may need to also map the namespace of each element and each attribute,
     * but for now we do not.
     */
    @Override
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        if(uri.startsWith(javaURI)) {
            FactoryImplType fit = getFactoryImplType();
            if(fit != null) {
                Log.getLog().debug("Changing '" + uri + "' to '" + fit.javaURI + uri.substring(javaURI.length()) + "'");
                uri = fit.javaURI + uri.substring(javaURI.length());
            }
        }
        super.startPrefixMapping(prefix, uri);
    }

    protected TransformerFactory getFactory() {
        return factory;
    }

    protected void setFactory(TransformerFactory factory) {
        this.factory = factory;
        factoryImplType = factoryImplTypes.get(factory.getClass().getName());
        if(factoryImplType == null) Log.getLog().warn("Could not find factory type for '" + factory.getClass().getName() + "'");
    }
}
