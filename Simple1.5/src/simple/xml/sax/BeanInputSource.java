/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import org.xml.sax.InputSource;

/**
 * @author bkrug
 *
 */
public class BeanInputSource extends InputSource {
    protected Object bean;
    protected String baseTagName;
    /**
     * 
     */
    public BeanInputSource(Object bean, String baseTagName) {
        this.bean = bean;
        this.baseTagName = baseTagName;
    }

    public Object getBean() {
        return bean;
    }
    public String getBaseTagName() {
        return baseTagName;
    }
}
