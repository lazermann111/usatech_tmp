/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml.sax;

import org.xml.sax.SAXException;

import simple.xml.AbstractXMLReader;

/**
 * @author bkrug
 *
 */
public class PlaybackAdapter extends AbstractXMLReader<PlaybackInputSource> {   
    public PlaybackAdapter() {
    }    

    public void generateEvents(PlaybackInputSource inputSource) throws SAXException {
        inputSource.playback(this);
    }

    @Override
    protected String getNameSpaceURI() {
        return null;
    }   
}
