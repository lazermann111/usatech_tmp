package simple.xml.sax;

import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.SAXParseException;

public class ContextLoggingErrorHandler extends LoggingErrorHandler {
	protected class ContextKeeperReader extends Reader {
		protected final Reader delegate;
		protected final List<Integer> linePositions = new ArrayList<Integer>();
		protected final char[] buffer;
		protected int bufferStartPosition = 0;
		protected int bufferLength = 0;
		protected int markPosition = -1;
		protected int position = 0;
		protected boolean onCR = false;
		public ContextKeeperReader(Reader delegate, int bufferSize) {
			super();
			this.delegate = delegate;
			this.buffer = new char[bufferSize*2];
		}

		public void close() throws IOException {
			delegate.close();
		}

		public boolean equals(Object obj) {
			return delegate.equals(obj);
		}

		public int hashCode() {
			return delegate.hashCode();
		}

		public void mark(int readAheadLimit) throws IOException {
			delegate.mark(readAheadLimit);
		}

		public boolean markSupported() {
			return delegate.markSupported();
		}

		protected void recordChar(char c) {
			position++;
			switch(c) {
				case '\r':
					linePositions.add(position);
					onCR = true;
					break;
				case '\n':
					if(onCR) {
						onCR = false;
						linePositions.set(linePositions.size() - 1, position);						
					} else {
						linePositions.add(position);
					}
					break;
				default:
					onCR = false;
			}
		}
		public int read() throws IOException {
			int r = delegate.read();
			if(r < 0)
				return r;
			recordChar((char)r);	
			if(bufferLength == buffer.length) {
				int n = buffer.length / 2;
				System.arraycopy(buffer, n, buffer, 0, buffer.length - n);
				bufferStartPosition += n;
				bufferLength =  buffer.length - n;
			}
			buffer[bufferLength++] = (char)r;
			return r;
		}

		public int read(char[] cbuf, int off, int len) throws IOException {
			int r = delegate.read(cbuf, off, len);
			if(r < 0)
				return r;
			for(int i = 0; i < r; i++)
				recordChar(cbuf[off + i]);
			int n = buffer.length / 2;
			if(r > n) {
				bufferStartPosition += bufferLength;
				bufferLength = Math.min(r, buffer.length);
				bufferStartPosition += (r - bufferLength);
				System.arraycopy(cbuf, off + r - bufferLength, buffer, 0, bufferLength);	
			} else {
				if(bufferLength + r > buffer.length) {
					System.arraycopy(buffer, n, buffer, 0, buffer.length - n);
					bufferStartPosition += n;
					bufferLength =  buffer.length - n;
				}
				System.arraycopy(cbuf, off, buffer, bufferLength, r);
				bufferLength += r;
			}			
			return r;
		}

		public boolean ready() throws IOException {
			return delegate.ready();
		}

		public void reset() throws IOException {
			delegate.reset();
			//rollback entries in linePositions
			int i = Collections.binarySearch(linePositions, markPosition);
			linePositions.subList(Math.abs(i + 1), linePositions.size()).clear();
			position = markPosition;
			bufferStartPosition = position;
			bufferLength = 0;
		}

		public long skip(long n) throws IOException {
			return delegate.skip(n);
		}

		public String toString() {
			return delegate.toString();
		}
		public String getContext(int lineNumber, int columnNumber) {
			if(lineNumber <= 0 || lineNumber > linePositions.size() + 1)
				return "[Unknown]";
			int pos;
			if(lineNumber == 1) {
				pos = 0;
			} else {
				pos = linePositions.get(lineNumber-2);
			}
			pos += columnNumber;
			if(pos < bufferStartPosition)
				return "[Buffer too small]";
			else {
				return new String(buffer, 0, pos - bufferStartPosition);
			}
		}
	}
	protected Map<URI,ContextKeeperReader> contexts = new HashMap<URI,ContextKeeperReader>();
	@Override
	protected void buildMessage(StringBuilder sb, SAXParseException exception) {
		super.buildMessage(sb, exception);
		if(exception.getSystemId() != null) {
			URI systemIdUri;
			try {
				systemIdUri = new URI(exception.getSystemId());
			} catch(URISyntaxException e) {
				return;
			}			
			ContextKeeperReader ckr = contexts.get(systemIdUri);
			if(ckr != null) {
				sb.append(" at text '").append(ckr.getContext(exception.getLineNumber(), exception.getColumnNumber())).append("*'");
			}
		}	
	}
	public Reader registerContext(URI systemId, Reader reader, int bufferSize) {
		ContextKeeperReader ckr = new ContextKeeperReader(reader, bufferSize);
		contexts.put(systemId, ckr);
		return ckr;
	}
	public void clearContexts() {
		contexts.clear();
	}
}
