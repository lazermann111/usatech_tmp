/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml;

import java.beans.IntrospectionException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;

import simple.bean.ConvertException;

/** Warning: This class is not thread safe since it holds the ContentHandler, DTDHandler, etc.
 * @author bkrug
 *
 */
public abstract class AbstractXMLReader<IS extends InputSource> extends DelegatingXMLHandler implements XMLReader {
    protected Set<String> features = new HashSet<String>();
    protected Map<String,Object> properties = new HashMap<String,Object>();

    /** Throws SAXException because this method is not supported
     * @see org.xml.sax.XMLReader#parse(java.lang.String)
     */
    public void parse(String systemId) throws IOException, SAXException {
        throw new SAXException("Method not supported");
    }

    /**
     * @see org.xml.sax.XMLReader#parse(org.xml.sax.InputSource)
     */
    @SuppressWarnings("unchecked")
	public void parse(InputSource input) throws IOException, SAXException {
    	IS inputSource;
    	try {
    		inputSource = (IS)input;
    	} catch(ClassCastException e) {
            throw new SAXException("InputSource of class, " + input.getClass().getName() + ", not supported");
        }
        try {
            generateEvents(inputSource);
        } catch(RuntimeException e) {
            if(e.getClass().getName().endsWith(".StopException")) {
                throw e;// allow this to pass through
            }
            throw new SAXException("While generating events for " + input, e);
        }
    }

    public abstract void generateEvents(IS inputSource) throws IOException, SAXException ;

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#getFeature(java.lang.String)
     */
    public boolean getFeature(String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        return features.contains(name);
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#setFeature(java.lang.String, boolean)
     */
    public void setFeature(String name, boolean value) throws SAXNotRecognizedException,
            SAXNotSupportedException {
        if(value) features.add(name);
        else features.remove(name);
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#getProperty(java.lang.String)
     */
    public Object getProperty(String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        return properties.get(name);
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#setProperty(java.lang.String, java.lang.Object)
     */
    public void setProperty(String name, Object value) throws SAXNotRecognizedException,
            SAXNotSupportedException {
        properties.put(name, value);
    }

    public String toXML(IS input) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, SAXException, ParseException, IOException {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	ContentHandler orig = getContentHandler();
    	setContentHandler(XMLBuilder.createToXmlStreamHandler(out));
    	parse(input);
    	setContentHandler(orig);
    	return out.toString();
    }
}
