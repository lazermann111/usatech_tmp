/*
 * Created on Nov 7, 2005
 *
 */
package simple.xml.serializer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Vector;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.sun.org.apache.xml.internal.serializer.DOMSerializer;
import com.sun.org.apache.xml.internal.serializer.NamespaceMappings;
import com.sun.org.apache.xml.internal.serializer.SerializationHandler;
import com.sun.org.apache.xml.internal.serializer.TreeWalker;

public abstract class ToXHTMLStream extends XHTMLHandler implements SerializationHandler {
    protected OutputStream output;
    protected NamespaceMappings mappings;
    protected SourceLocator sourceLocator;
    /**
     * @return Returns the output.
     */
    public OutputStream getOutputStream() {
        return output;
    }
    /**
     * @param output The output to set.
     */
    public void setOutputStream(OutputStream output) {
        this.output = output;
        setWriter(new OutputStreamWriter(output));
    }

    /**
     * @param mappings The mappings to set.
     */
    public void setNamespaceMappings(NamespaceMappings mappings) {
        this.mappings = mappings;
    }
    @Override
    protected void warn(String message, Throwable exception) {
        ErrorListener el = getErrorListener();
        if(el != null) try {
            el.warning(new TransformerException(message, getSourceLocator(), exception));
        } catch(TransformerException e1) {
        } else {
            super.warn(message, exception);
        }
    }

    @Override
    protected void error(String message, Throwable exception) {
        ErrorListener el = getErrorListener();
        if(el != null) try {
            el.error(new TransformerException(message, getSourceLocator(), exception));
        } catch(TransformerException e1) {
        } else {
            super.error(message, exception);
        }
    }
    @Override
    protected void fatal(String message, Throwable exception) {
        ErrorListener el = getErrorListener();
        if(el != null) try {
            el.fatalError(new TransformerException(message, getSourceLocator(), exception));
        } catch(TransformerException e1) {
        } else {
            super.fatal(message, exception);
        }
    }
    protected void warning(String message, Throwable exception) {
        warn(message, exception);
    }
    protected void fatalError(String message, Throwable exception) {
        fatal(message, exception);
    }

    @Override
	protected ErrorListener getErrorListener() {
        Transformer tran = getTransformer();
        if(transformer != null) return tran.getErrorListener();
        return null;
    }

    public void setContentHandler(ContentHandler ch) {
        //
    }
    public void close() {
        //
    }
    public void serialize(Node node) throws IOException {
        try {
            TreeWalker walker = new TreeWalker(this);
            walker.traverse(node);
        } catch(SAXException se) {
            throw new RuntimeException(se);
        }
    }

    public void flushPending() throws SAXException {
        // do nothing
    }
    public void setDTDEntityExpansion(boolean expand) {
        // do nothing
    }
    public void addAttribute(String uri, String localName, String rawName, String type, String value, boolean isXSLAttribute) throws SAXException {
        // for now we will not care if the attributes are unique or not
        addUniqueAttribute(rawName, value, 0);
    }
    public void addAttributes(Attributes atts) throws SAXException {
        for(int i = 0; i < atts.getLength(); i++) {
            addAttribute(atts.getURI(i), atts.getLocalName(i), atts.getQName(i), atts.getType(i), atts.getValue(i));
        }
    }
    public void addAttribute(String qName, String value) {
        try {
            addAttribute(null, null, qName, null, value, false);
        } catch(SAXException e) {
            warning("", e);
        }
    }
    public void characters(String chars) throws SAXException {
        characters(chars.toCharArray(), 0, chars.length());
    }
    public void characters(Node node) throws SAXException {
        characters(node.getNodeValue());
    }
    public void endElement(String elemName) throws SAXException {
        // Note: I believe the name is the qname.
        //warn("endElement without prefix or namespaceURI is not supported", new Throwable());
        endElement(null, null, elemName);
    }
    public void startElement(String uri, String localName, String qName) throws SAXException {
        startElement(uri, localName, qName, null);
    }
    public void startElement(String qName) throws SAXException {
        startElement(null, null, qName);
    }
    public void namespaceAfterStartElement(String uri, String prefix) throws SAXException {
        // TODO: this implementation is not truly accurate
        startPrefixMapping(prefix, uri);
    }
    public boolean startPrefixMapping(String prefix, String uri, boolean shouldFlush) throws SAXException {
        boolean already = prefixToUri.containsKey(prefix);
        startPrefixMapping(prefix, uri);
        return already;
    }
    public void entityReference(String entityName) throws SAXException {
        // do nothing
    }
    public NamespaceMappings getNamespaceMappings() {
        return mappings;
    }
    @Override
	public String getPrefix(String uri) {
        try {
            return super.getPrefix(uri);
        } catch(SAXException e) {
            warn("Prefix not found", e);
            return "";
        }
    }
    public String getNamespaceURI(String name, boolean isElement) {
        return null; //TODO: not sure what to do here
    }
    public String getNamespaceURIFromPrefix(String prefix) {
        try {
            return getNamespaceURI(prefix);
        } catch(SAXException e) {
            warn("Namespace not found", e);
            return "";
        }
    }

    public void addUniqueAttribute(String qName, String value, int flags) throws SAXException {
        if(state != TagState.OPEN_START_TAG) {
            throw new SAXException("Already closed the start tag; cannot add attributes");
        }
		try {
			write(' ');
			write(qName);
			write("=\"");
			char[] chars = value.toCharArray();
			spacePending = false;
			escapeCharacters(chars, 0, chars.length, false);
			write("\"");
		} catch(IOException e) {
			throw new SAXException(e);
		}
    }

    public void addXSLAttribute(String qName, String value, String uri) {
        String localName;
        int p = qName.indexOf(':');
        if(p < 0) {
            localName = qName;
        } else {
            localName = qName.substring(p+1);
        }
        try {
            addAttribute(uri, localName, qName, null, value, false);
        } catch(SAXException e) {
            warning("", e);
        }
    }

    public void addAttribute(String uri, String localName, String rawName, String type, String value) throws SAXException {
        addAttribute(uri, localName, rawName, type, value, false);
    }

    public void comment(String comment) throws SAXException {
        comment(comment.toCharArray(), 0, comment.length());
    }

	public void setCdataSectionElements(Vector URI_and_localNames) {
        // ignore this
    }
    public void elementDecl(String name, String model) throws SAXException {
        // Do nothing

    }
    public void attributeDecl(String eName, String aName, String type, String mode, String value) throws SAXException {
        // Do nothing

    }
    public void internalEntityDecl(String name, String value) throws SAXException {
        // Do nothing

    }
    public void externalEntityDecl(String name, String publicId, String systemId) throws SAXException {
        // Do nothing

    }
    public void notationDecl(String name, String publicId, String systemId) throws SAXException {
        // Do nothing

    }
    public void unparsedEntityDecl(String name, String publicId, String systemId, String notationName) throws SAXException {
        // Do nothing

    }
    public void warning(SAXParseException exception) throws SAXException {
        warn("", exception);
    }
    public void error(SAXParseException exception) throws SAXException {
        error("", exception);
    }
    public void fatalError(SAXParseException exception) throws SAXException {
        fatalError("", exception);
    }
    public ContentHandler asContentHandler() throws IOException {
        return this;
    }
    public DOMSerializer asDOMSerializer() throws IOException {
        return this;
    }

    public boolean getOmitXMLDeclaration() {
        return omitXMLDeclaration;
    }

	public void setIsStandalone(boolean b) {
		// not sure what to do here
	}
}
