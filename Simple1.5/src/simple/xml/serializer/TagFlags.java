package simple.xml.serializer;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum TagFlags {
	TAG_FLAG_NON_EMPTY(1),
    TAG_FLAG_NON_ESCAPING(2),
    TAG_FLAG_ADD_NBSP(4),
    TAG_FLAG_PRESERVE_SPACE(8),
    TAG_FLAG_USER_SET_ESCAPING(16),
    TAG_FLAG_NEEDS_SESSION_TOKEN_INPUT(32),
    TAG_FLAG_NEEDS_SESSION_TOKEN_PARAM(64),
    TAG_FLAG_FORM_CONTROL(128),
    TAG_FLAG_STYLE_ESCAPING(256),
    TAG_FLAG_SCRIPT_ESCAPING(512),
    ;

	private final int value;
	private TagFlags(int value) {
        this.value = value;
	}
	public int getValue() {
		return value;
	}

	public boolean isSet(int flags) {
		return (flags & value) == value;
	}
	public int set(int flags) {
		return (flags | value);
	}
	public int unset(int flags) {
		return (flags & ~value);
	}
    protected final static EnumIntValueLookup<TagFlags> lookup = new EnumIntValueLookup<TagFlags>(TagFlags.class);
    public static TagFlags getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
    
}
