/*
 * Created on Mar 6, 2006
 *
 */
package simple.xml.serializer;

import java.io.IOException;
import java.util.Properties;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import com.jclark.xsl.sax.CommentHandler;
import com.jclark.xsl.sax.Destination;
import com.jclark.xsl.sax2.OutputContentHandler;
import com.jclark.xsl.sax2.RawCharactersHandler;

/**
 * @author bkrug
 *
 */
public class XHTMLOutputHandler extends XHTMLHandler implements OutputContentHandler, RawCharactersHandler, CommentHandler {
    /**
     *
     */
    public XHTMLOutputHandler() {
        super();
    }

    /**
     * @see com.jclark.xsl.sax2.OutputContentHandler#init(com.jclark.xsl.sax.Destination, java.util.Properties)
     */
    public ContentHandler init(Destination dest, Properties outputMethodProperties) throws SAXException,
            IOException {
        setWriter(dest.getWriter(getMediaType(), getEncoding()));
        setOutputFormat(outputMethodProperties);
        return this;
    }

    @Override
    protected void error(String message, Throwable exception) {
        ErrorListener el = getErrorListener();
        if(el != null) try {
            el.error(new TransformerException(message, getSourceLocator(), exception));
        } catch(TransformerException e1) {
        } else {
            super.error(message, exception);
        }
    }

    @Override
    protected void fatal(String message, Throwable exception) {
        ErrorListener el = getErrorListener();
        if(el != null) try {
            el.fatalError(new TransformerException(message, getSourceLocator(), exception));
        } catch(TransformerException e1) {
        } else {
            super.fatal(message, exception);
        }
    }

    @Override
    protected void warn(String message, Throwable exception) {
        ErrorListener el = getErrorListener();
        if(el != null) try {
            el.warning(new TransformerException(message, getSourceLocator(), exception));
        } catch(TransformerException e1) {
        } else {
            super.warn(message, exception);
        }
    }

	public void rawCharacters(String chars) throws SAXException {
		if(chars != null && chars.length() > 0)
			try {
				switch(state) {
					case ATTR_VALUE:
						write('"');
						// fall-through
					case OPEN_START_TAG:
						write('>');
					case EMPTY_TAG:
						state = TagState.HAS_CONTENT;
						break;
				}
				write(chars);
			} catch(IOException e) {
				throw new SAXException(e);
			}
	}

	public void comment(String comment) throws SAXException {
		comment(comment.toCharArray(), 0, comment.length());
	}
}
