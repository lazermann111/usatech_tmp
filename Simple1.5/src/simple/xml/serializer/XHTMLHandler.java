/*
 * Created on Jun 2, 2005
 *
 */
package simple.xml.serializer;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.Transformer;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;
import simple.xml.AbstractContentHandler;
import simple.xml.Serializer;

import com.sun.org.apache.xml.internal.serializer.OutputPropertiesFactory;

public class XHTMLHandler extends AbstractContentHandler implements Serializer {
	private static final Log log = Log.getLog();
    protected static final Collection<String> xhtmlNamespaces = Collections.singleton(
            "http://www.w3.org/1999/xhtml"
        );
    protected static final char[] NBSP = "&nbsp;".toCharArray();
    protected static final Pattern useOldDocTypePattern = Pattern.compile(".* MSIE [5]\\..*");
	protected static String defaultEncoding = "ISO-8859-1";
    
    public static final String OMIT_DTD = "omit-dtd";
    public static final String SESSION_TOKEN = "session-token";
    protected static final Map<String,Integer> tagFlagsMap = new HashMap<String,Integer>();
    protected static final Map<String,String> defaultPrefixToUri = new HashMap<String, String>();
    protected static final Map<String,String> defaultUriToPrefix = new HashMap<String, String>();
    protected static final char[] SPACES = new char[48];
    protected static enum TagState { NO_TAG, OPEN_START_TAG, EMPTY_TAG, HAS_CONTENT, HAS_CHILDREN, ATTR_VALUE };
    protected static String defaultEntitiesFile = "simple/xml/serializer/HTMLEntities"; //"org/apache/xml/serializer/HTMLEntities";
    protected static char[][] defaultEntities;
    protected static final Pattern attributePattern = Pattern.compile("\\s*([A-za-z](?:\\w|-|:)*)[=]([\"'])(.*?)\\2\\s*");
    protected static final Pattern urlEscapePattern = Pattern.compile("[^\\w\\-.!~*'()\\\\/#&?;:,=]");
    protected static final Pattern hrefParsePattern = Pattern.compile("(?:([^:]+):)?([^?#]*)(?:(\\?)([^#]*))?(?:(\\#.*))?");
    protected static final char[] EMPTY_CHARS = new char[0];
    protected static final char[] digits = { '0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9' };
	protected static final char[] START_CDATA_IN_COMMENT_CHARS = "/*<![CDATA[*/\n".toCharArray();
	protected static final char[] END_CDATA_IN_COMMENT_CHARS = "/*]]>*/".toCharArray();
	protected static final char[] CDATA_END_CHARS = "]]>".toCharArray();
	protected static final char[] SCRIPT_GT_ESCAPE_CHARS = "\\u003E".toCharArray();

    static {
        Arrays.fill(SPACES, ' ');
        addDefaultPrefixMapping("xml", "http://www.w3.org/XML/1998/namespace");
        addDefaultPrefixMapping("xmlns", "http://www.w3.org/2000/xmlns/");
        addDefaultPrefixMapping("", xhtmlNamespaces.iterator().next());
        loadTagFlags("simple/xml/serializer/XHTMLTagFlags", tagFlagsMap);
    }

    protected String doctypeName = "html";
    protected String doctypePublic = "-//W3C//DTD XHTML 1.0 Transitional//EN";
    protected String doctypeSystem = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd";

    protected boolean indent = false;
    protected Locator locator;
    protected boolean spaceBeforeClose = true;
    protected TagState state = TagState.NO_TAG;
    protected int[] tagFlagStack = new int[128];
    protected int depth = 0;
    protected int indentAmount = 4;
    protected String newline = "\n";
    protected char[][] entities;
    protected int lastSpace = -1;
    protected boolean spacePending = false;
    protected boolean metaTagsWritten = false;
    protected boolean newlinePending = false;
    
    protected final Map<String,String> prefixToUri = new HashMap<String, String>();
    protected final Map<String,String> uriToPrefix = new HashMap<String, String>();
    protected Writer writer;
	protected final Writer publicWriter = new Writer() {
		public int hashCode() {
			return writer.hashCode();
		}

		public void write(int c) throws IOException {
			flushBuffer();
			writer.write(c);
		}

		public void write(char[] cbuf) throws IOException {
			flushBuffer();
			writer.write(cbuf);
		}

		public void write(char[] cbuf, int off, int len) throws IOException {
			flushBuffer();
			writer.write(cbuf, off, len);
		}

		public void write(String str) throws IOException {
			flushBuffer();
			writer.write(str);
		}

		public boolean equals(Object obj) {
			return writer.equals(obj);
		}

		public void write(String str, int off, int len) throws IOException {
			flushBuffer();
			writer.write(str, off, len);
		}

		public Writer append(CharSequence csq) throws IOException {
			flushBuffer();
			writer.append(csq);
			return this;
		}

		public Writer append(CharSequence csq, int start, int end) throws IOException {
			flushBuffer();
			writer.append(csq, start, end);
			return this;
		}

		public Writer append(char c) throws IOException {
			flushBuffer();
			writer.append(c);
			return this;
		}

		public void flush() throws IOException {
			writer.flush();
		}

		public void close() throws IOException {
			writer.close();
		}

		public String toString() {
			return writer.toString();
		}
	};
    protected char[] buffer = new char[512];
    protected int offset;

    //new fields
	protected String encoding = "ISO-8859-1"; // "UTF-8"; to be consistent with database
    protected String mediaType = "text/html";
    protected boolean omitXMLDeclaration = true;
    protected boolean omitDTD = false;
    protected Properties outputFormat = new Properties();
    protected String standalone;
    protected Transformer transformer;
    protected String version = "1.0";
    protected SourceLocator sourceLocator;
    protected String sessionToken;

    public XHTMLHandler() {
        loadEntities(defaultEntitiesFile);
    }

    protected void loadEntities(String entitiesFile) {
        if(entitiesFile.equalsIgnoreCase(defaultEntitiesFile)) {
            if(defaultEntities == null) {
                defaultEntities = createEntities(defaultEntitiesFile);
            }
            entities = defaultEntities;
        } else {
            entities = createEntities(entitiesFile);
        }
    }

	protected static void loadTagFlags(String tagFlagsFile, Map<String,Integer> tagFlags) {
		Properties props = new Properties();
		InputStream in = null;
		try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            if(cl == null) {
                cl = XHTMLHandler.class.getClassLoader();
                if(cl == null) cl= ClassLoader.getSystemClassLoader();
            }
            in = cl.getResourceAsStream(tagFlagsFile + ".properties");
            if(in == null) throw new IOException("File '" + tagFlagsFile + ".properties' not found");
            props.load(in);

            for(Map.Entry<Object, Object> entry : props.entrySet()) {
            	int flag = 0;
            	for(String s : StringUtils.split((String)entry.getValue(), '|')) {
            		TagFlags tf = ConvertUtils.convert(TagFlags.class, s.trim());
            		flag |= tf.getValue();
            	}
            	tagFlags.put((String)entry.getKey(), flag);
            }
			log.info("Loaded " + tagFlags.size() + " tag flags from '" + tagFlagsFile + ".properties'");
        } catch(IOException e) {
            log.warn("Could not load tag flags", e);
            tagFlags.put("script", TagFlags.TAG_FLAG_NON_EMPTY.getValue() | TagFlags.TAG_FLAG_NON_ESCAPING.getValue());
            tagFlags.put("iframe", TagFlags.TAG_FLAG_NON_EMPTY.getValue() | TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_PARAM.getValue());
            tagFlags.put("frame", TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_PARAM.getValue());
            tagFlags.put("td", TagFlags.TAG_FLAG_ADD_NBSP.getValue());
            tagFlags.put("th", TagFlags.TAG_FLAG_ADD_NBSP.getValue());
            tagFlags.put("pre", TagFlags.TAG_FLAG_NON_ESCAPING.getValue());
            tagFlags.put("style", TagFlags.TAG_FLAG_NON_EMPTY.getValue() | TagFlags.TAG_FLAG_NON_ESCAPING.getValue());
            tagFlags.put("p", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("div", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("span", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("font", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("center", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("select", TagFlags.TAG_FLAG_NON_EMPTY.getValue() | TagFlags.TAG_FLAG_FORM_CONTROL.getValue());
            tagFlags.put("input", TagFlags.TAG_FLAG_FORM_CONTROL.getValue());
            tagFlags.put("textarea", TagFlags.TAG_FLAG_FORM_CONTROL.getValue());
            tagFlags.put("form", TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_INPUT.getValue());
        } catch(ConvertException e) {
        	log.warn("Could not load tag flags", e);
            tagFlags.put("script", TagFlags.TAG_FLAG_NON_EMPTY.getValue() | TagFlags.TAG_FLAG_NON_ESCAPING.getValue());
            tagFlags.put("iframe", TagFlags.TAG_FLAG_NON_EMPTY.getValue() | TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_PARAM.getValue());
            tagFlags.put("frame", TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_PARAM.getValue());
            tagFlags.put("td", TagFlags.TAG_FLAG_ADD_NBSP.getValue());
            tagFlags.put("th", TagFlags.TAG_FLAG_ADD_NBSP.getValue());
            tagFlags.put("pre", TagFlags.TAG_FLAG_NON_ESCAPING.getValue());
            tagFlags.put("style", TagFlags.TAG_FLAG_NON_EMPTY.getValue() | TagFlags.TAG_FLAG_NON_ESCAPING.getValue());
            tagFlags.put("p", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("div", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("span", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("font", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("center", TagFlags.TAG_FLAG_NON_EMPTY.getValue());
            tagFlags.put("select", TagFlags.TAG_FLAG_NON_EMPTY.getValue() | TagFlags.TAG_FLAG_FORM_CONTROL.getValue());
            tagFlags.put("input", TagFlags.TAG_FLAG_FORM_CONTROL.getValue());
            tagFlags.put("textarea", TagFlags.TAG_FLAG_FORM_CONTROL.getValue());
            tagFlags.put("form", TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_INPUT.getValue());
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {}
		}
	}

    protected char[][] createEntities(String entitiesFile) {
        Properties props = new Properties();
        try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            if(cl == null) {
                cl = getClass().getClassLoader();
                if(cl == null) cl= ClassLoader.getSystemClassLoader();
            }
            InputStream in = cl.getResourceAsStream(entitiesFile + ".properties");
            if(in == null) throw new IOException("File '" + entitiesFile + ".properties' not found");
            props.load(in);
            debug("Loaded entites from '" + entitiesFile + ".properties'");
        } catch(IOException e) {
            warn("Could not load entities", e);
            props.setProperty("quot", "34");
            props.setProperty("amp", "38");
            props.setProperty("lt", "60");
            props.setProperty("gt", "62");
            props.setProperty("nbsp", "160");
        }
        char[][] entities = new char[256][];
        for(Map.Entry<Object,Object> entry : props.entrySet()) {
            addEntity(entities, entry.getKey().toString(), Integer.parseInt(entry.getValue().toString()));
        }
        return entities;
    }
    protected void addEntity(char[][] entities, String name, int ch) {
        if(ch >= entities.length) {
            warn("Supplemental characters (those >= " + entities.length + ") are not supported; entity '" + name + "' will be ignored");
        } else {
        	entities[ch] = new char[2+name.length()];
            entities[ch][0] = '&';
            name.getChars(0, name.length(), entities[ch], 1);
            entities[ch][1+name.length()] = ';';
        }
    }

	@Override
	protected String getNameSpaceURI() {
		return defaultPrefixToUri.get("");
	}

	@Override
	protected String getPrefix() {
		return "";
	}
	/**
	 * @param namespaceURI
	 * @param localName
	 * @param qname
	 * @param value
	 *            The value as a Reader. It is recommended to use a BufferedReader to improve performance as chars are read one by one.
	 * @throws SAXException
	 */
	public Writer addAttributeByStream(String namespaceURI, String localName, String qname) throws SAXException {
		if(state != TagState.OPEN_START_TAG)
			throw new SAXException("Cannot add attribute; an element is not started or already has content");
		if(qname == null)
			qname = getQName(namespaceURI, localName);
		try {
			write(' ');
			write(qname);
			write("=\"");
		} catch(IOException e) {
			throw new SAXException(e);
		}
		state = TagState.ATTR_VALUE;
		return new Writer() {
			@Override
			public void close() throws IOException {
				if(state != TagState.ATTR_VALUE)
					throw new IOException("An attribute is not open for writing");
				XHTMLHandler.this.write("\"");
				state = TagState.OPEN_START_TAG;
			}

			@Override
			public void write(char[] cbuf, int off, int len) throws IOException {
				if(state != TagState.ATTR_VALUE)
					throw new IOException("An attribute is not open for writing");
				final int end = off + len;
				for(int i = off; i < end; i++)
					writeAttributeChar(cbuf[i]);
			}

			@Override
			public void write(int c) throws IOException {
				if(state != TagState.ATTR_VALUE)
					throw new IOException("An attribute is not open for writing");
				writeAttributeChar((char) c);
			}

			@Override
			public void flush() throws IOException {
				flushBuffer();
			}
		};
	}

	public void addAttribute(String namespaceURI, String localName, String qname, String value) throws SAXException {
		if(state != TagState.OPEN_START_TAG)
			throw new SAXException("Cannot add attribute; an element is not started or already has content");
		if(qname == null)
			qname = getQName(namespaceURI, localName);
		try {
			write(' ');
			write(qname);
			write("=\"");
			if(value != null && value.length() > 0)
				writeAttributeValue(value);
			write("\"");
		} catch(IOException e) {
			throw new SAXException(e);
		}
	}

	protected String getQName(String namespaceURI, String localName) throws SAXException {
		String prefix = getPrefix(namespaceURI);
		if(prefix.length() > 0)
			return prefix + ":" + localName;
		return localName;
	}

    /**
     * @see org.xml.sax.ContentHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
     */
	public void startElement(String namespaceURI, String localName, String qname, Attributes atts) throws SAXException {
        if(localName == null) {
			int p = qname.indexOf(':');
            if(p < 0) {
                namespaceURI = getNamespaceURI("");
				localName = qname;
            } else {
				namespaceURI = getNamespaceURI(qname.substring(0, p));
				localName = qname.substring(p + 1);
            }
		} else if(qname == null)
			qname = getQName(namespaceURI, localName);
		try {
			switch(state) {
				case NO_TAG:
					writeDTD(getDoctypeName(), getDoctypePublic(), getDoctypeSystem());
					break;
				case ATTR_VALUE:
					write('"');
					// fall-through
				case OPEN_START_TAG:
					write('>');
					break;
			}
		} catch(IOException e) {
			throw new SAXException(e);
		}

        int tf = getTagFlags(namespaceURI, localName);
        if(getSessionToken() != null) {
	        if(TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_PARAM.isSet(tf)) {
	        	int index = getAttributeIndex(atts, namespaceURI, getUrlAttribute(localName));
	        	if(index >= 0) {
		        	String href = atts.getValue(index);
		        	if(href != null && href.trim().length() > 0) {
		        		Matcher matcher = hrefParsePattern.matcher(href);
		        		if(matcher.matches()) {
		        			String protocol = matcher.group(1);
		        			if(protocol == null || "https".equalsIgnoreCase(protocol) || "http".equalsIgnoreCase(protocol)) {
		        				String query = matcher.group(4);
		        				if(query == null || query.trim().length() == 0 
		        						|| (!query.startsWith(SESSION_TOKEN + '=') && query.indexOf("&" + SESSION_TOKEN + '=') < 0)) {
		        					//reconstruct url and add sessionToken
		        					StringBuilder sb = new StringBuilder(href);
		        					if(matcher.start(4) >= 0)
		        						sb.insert(matcher.start(4), SESSION_TOKEN + '=' + StringUtils.prepareURLPart(getSessionToken()) + '&');
		        					else if(matcher.start(3) >= 0) 
		        						sb.insert(matcher.start(3) + 1, SESSION_TOKEN + '=' + StringUtils.prepareURLPart(getSessionToken()));
		        					else if(matcher.start(5) >= 0)
		        						sb.insert(matcher.start(5), '?' + SESSION_TOKEN + '=' + StringUtils.prepareURLPart(getSessionToken()));
		        					else
		        						sb.append('?' + SESSION_TOKEN + '=' + StringUtils.prepareURLPart(getSessionToken()));
		        					AttributesImpl ai = new AttributesImpl(atts);
		        					ai.setValue(index, sb.toString());
		        					atts = ai;
		        				}
		        			}
		        		}
		        	} 
	        	}
	        }
			if(TagFlags.TAG_FLAG_FORM_CONTROL.isSet(tf) && SESSION_TOKEN.equals(getAttributeValue(atts, namespaceURI, "name")) && !StringUtils.isBlank(getAttributeValue(atts, namespaceURI, "value"))) {
	        	for(int i = depth - 1; i >= 0; i--) { 
	        		if(TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_INPUT.isSet(tagFlagStack[i])) {
	        			tagFlagStack[i] = TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_INPUT.unset(tagFlagStack[i]);
	        			break;
	        		}
	        	}
	        }
        }
		writeStartTag(qname, atts);
        state = TagState.OPEN_START_TAG;
        setTagFlag(depth, tf);
        depth++;
        spacePending = false;
        newlinePending = true;
    }
    
    protected String getUrlAttribute(String localElementName) {
    	if("a".equalsIgnoreCase(localElementName))
			return "href";
    	else
    		return "src";
    }
    protected String getAttributeValue(Attributes atts, String namespaceURI, String localName) {
    	for(int i = 0; i < atts.getLength(); i++) {
    		if(localName.equals(atts.getLocalName(i))) {
    			String ns = atts.getURI(i);
    			if(ns == null || ns.length() == 0 || namespaceURI == null || namespaceURI.equals(ns))
    				return atts.getValue(i);
    		}
    	}
    	return null;
    }
    protected int getAttributeIndex(Attributes atts, String namespaceURI, String localName) {
    	for(int i = 0; i < atts.getLength(); i++) {
    		if(localName.equals(atts.getLocalName(i))) {
    			String ns = atts.getURI(i);
    			if(ns == null || ns.length() == 0 || namespaceURI == null || namespaceURI.equals(ns))
    				return i;
    		}
    	}
    	return -1;
    }
    protected void setTagFlag(int index, int value) {
        try {
            tagFlagStack[index] = value;
        } catch(ArrayIndexOutOfBoundsException e) {
            int[] tmp = new int[index + 17];
            System.arraycopy(tagFlagStack, 0, tmp, 0, tagFlagStack.length);
            tmp[index] = value;
            tagFlagStack = tmp;
        }
    }
    protected int getTagFlag(int index) {
        try {
            return tagFlagStack[index];
        } catch(ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }
    protected void writeStartTag(String name, Attributes atts) throws SAXException {
		try {
			if(newlinePending && !TagFlags.TAG_FLAG_PRESERVE_SPACE.isSet(getTagFlag(depth - 1))) {
				write(newline);
				writeIndent();
			}
			write('<');
			write(name);
			if(state == TagState.NO_TAG && !StringUtils.isBlank(getNameSpaceURI()) && getIndex(atts, "xmlns") < 0) {
				write(" xmlns=\"");
				write(getNameSpaceURI());
				write('\"');
			}
			writeAttributes(atts);
		} catch(IOException e) {
			throw new SAXException(e);
		}
    }

	protected int getIndex(Attributes atts, String qName) {
		int len = atts.getLength();
		for(int i = 0; i < len; i++) {
			if(qName.equals(atts.getQName(i))) {
				return i;
			}
		}
		return -1;
	}

    protected void writeAttributes(Attributes atts) throws SAXException {
		if(atts != null)
			for(int i = 0; i < atts.getLength(); i++) {
				try {
					write(' ');
					String qname = atts.getQName(i);
					if(qname == null)
						qname = getQName(atts.getURI(i), atts.getLocalName(i));
					write(qname);
					write("=\"");
					String value = atts.getValue(i);
					if(value != null && value.length() > 0)
						writeAttributeValue(value);
					write("\"");
				} catch(IOException e) {
					throw new SAXException(e);
				}
			}
    }

	protected void writeAttributeValue(String value) throws IOException {
		for(int i = 0; i < value.length(); i++)
			writeAttributeChar(value.charAt(i));
	}

	protected void writeAttributeValue(Reader value) throws IOException {
		int ch;
		while((ch = value.read()) >= 0)
			writeAttributeChar((char) ch);
	}

	protected void writeAttributeChar(char ch) throws IOException {
		char[] out;
		if(ch < 32) {
			writeAmpPoundEntity(ch);
		} else if(ch < 128) {
			if(ch >= entities.length || (out = entities[ch]) == null)
				write(ch);
			else
				write(out);
		} else if(ch < entities.length) {
			out = entities[ch];
			if(out != null)
				write(out);
			else
				writeAmpPoundEntity(ch);
		} else
			writeAmpPoundEntity(ch);
	}

	protected void writeAttributeValue(char[] chars, int start, int length) throws IOException {
    	int lastPos = start;
        int end = start + length;
        for(int i = start; i < end; i++) {
            char ch = chars[i];
            char[] out;
            if(ch < 32) {
            	if(lastPos < i) {
                    write(chars, lastPos, i - lastPos);
                }
                lastPos = i+1;
                writeAmpPoundEntity(ch);
            } else if(ch < 128) {
                try {
                    out = entities[ch];
                } catch(ArrayIndexOutOfBoundsException e) {
                    out = null;
                }
                if(out != null) {
                	if(lastPos < i) {
                        write(chars, lastPos, i - lastPos);
                    }
                    lastPos = i+1;
                	write(out);
                }
            } else {
            	try {
                    out = entities[ch];
                } catch(ArrayIndexOutOfBoundsException e) {
                    out = null;
                }
            	if(lastPos < i) {
                    write(chars, lastPos, i - lastPos);
                }
                lastPos = i+1;
                if(out != null) {
                	write(out);
                } else {
                	writeAmpPoundEntity(ch);
                }
            }
        }
        if(lastPos < end) {
            write(chars, lastPos, end - lastPos);
        }
    }
    protected void writeDTD(String name, String publicId, String systemId) throws SAXException {
		try {
			if(!omitDTD && ((name != null && (name = name.trim()).length() > 0) || (publicId != null && (publicId = publicId.trim()).length() > 0) || (systemId != null && (systemId = systemId.trim()).length() > 0))) {
				write("<!DOCTYPE");
				if(name != null && (name = name.trim()).length() > 0) {
					write(" ");
					write(name);
				}
				boolean systemOnly = true;
				if(publicId != null && (publicId = publicId.trim()).length() > 0) {
					write(" PUBLIC \"");
					write(publicId);
					write('"');
					systemOnly = false;
				}
				if(systemId != null && (systemId = systemId.trim()).length() > 0) {
					if(systemOnly)
						write(" SYSTEM");
					write(" \"");
					write(systemId);
					write('"');
				}
				write('>');
			}
		} catch(IOException e) {
			throw new SAXException(e);
		}
    }

    protected void writeIndent() throws SAXException {
        if(!indent) return;
		try {
			// calculate indent
			int n = depth * indentAmount;
			for(; n > SPACES.length; n -= SPACES.length) {
				write(SPACES);
			}
			if(n > 0)
				write(SPACES, 0, n);
		} catch(IOException e) {
			throw new SAXException(e);
		}
    }

    protected String getPrefix(String namespaceURI) throws SAXException {
        String prefix = uriToPrefix.get(namespaceURI);
        if(prefix != null) return prefix;
        prefix = defaultUriToPrefix.get(namespaceURI);
        if(prefix != null) return prefix;
        //log.warn("Namespace '" + namespaceURI + "' was not defined");
        //return "{" + namespaceURI + "}";
        throw new SAXException("Namespace '" + namespaceURI + "' was not defined");
    }

    protected String getNamespaceURI(String prefix) throws SAXException {
        String uri = prefixToUri.get(prefix);
        if(uri != null) return uri;
        uri = defaultPrefixToUri.get(prefix);
        if(uri != null) return uri;
        //log.warn("Invalid prefix '" + prefix + "' specified");
        //return prefix;
        throw new SAXException("Invalid prefix '" + prefix + "' specified");
    }

    /** Track whether characters were written or not
     * @see org.apache.xml.serializer.ToStream#characters(char[], int, int)
     */
    public void characters(char[] chars, int start, int length) throws SAXException {
		if(length > 0)
			try {
				int tf = getTagFlag(depth - 1);
				switch(state) {
					case ATTR_VALUE:
						write('"');
						// fall-through
					case OPEN_START_TAG:
						write('>');
						// fall-through
					case EMPTY_TAG:
						state = TagState.HAS_CONTENT;
						if(TagFlags.TAG_FLAG_STYLE_ESCAPING.isSet(tf))
							write(START_CDATA_IN_COMMENT_CHARS);
						else if(TagFlags.TAG_FLAG_SCRIPT_ESCAPING.isSet(tf))
							write(START_CDATA_IN_COMMENT_CHARS);
						break;
				}
				newlinePending = false;
				if(TagFlags.TAG_FLAG_STYLE_ESCAPING.isSet(tf))
					styleEscapeCharacters(chars, start, length);
				else if(TagFlags.TAG_FLAG_SCRIPT_ESCAPING.isSet(tf))
					scriptEscapeCharacters(chars, start, length);
				else if(TagFlags.TAG_FLAG_NON_ESCAPING.isSet(tf))
					write(chars, start, length);
				else
					escapeCharacters(chars, start, length, TagFlags.TAG_FLAG_PRESERVE_SPACE.isSet(tf));
			} catch(IOException e) {
				throw new SAXException(e);
			}
    }


	protected void styleEscapeCharacters(char[] chars, int start, int length) throws IOException {
		int lastPos = start;
		while(true) {
			int nextPos = StringUtils.indexOfSequence(chars, lastPos, length + start - lastPos, CDATA_END_CHARS);
			if(nextPos < 0) {
				write(chars, lastPos, length + start - lastPos);
				return;
			}
			write(chars, lastPos, nextPos - lastPos);
			write(CDATA_END_CHARS, 0, 2);
			write(END_CDATA_IN_COMMENT_CHARS);
			write(START_CDATA_IN_COMMENT_CHARS);
			write(CDATA_END_CHARS, 2, 1);
			lastPos = nextPos + CDATA_END_CHARS.length;
		}
	}

	protected void scriptEscapeCharacters(char[] chars, int start, int length) throws IOException {
		int lastPos = start;
		while(true) {
			int nextPos = StringUtils.indexOfSequence(chars, lastPos, length + start - lastPos, CDATA_END_CHARS);
			if(nextPos < 0) {
				write(chars, lastPos, length + start - lastPos);
				return;
			}
			write(chars, lastPos, nextPos - lastPos);
			write(CDATA_END_CHARS, 0, 2);
			write(SCRIPT_GT_ESCAPE_CHARS);
			lastPos = nextPos + CDATA_END_CHARS.length;
		}
	}
    protected void escapeCharacters(char[] chars, int start, int length, boolean preserveSpace) throws SAXException {
        int lastPos = start;
        int end = start + length;
		try {
			for(int i = start; i < end; i++) {
				char ch = chars[i];
				char[] out;
				if(ch <= 32) {
					if(Character.isWhitespace(ch)) {
						if(!preserveSpace) {
							if(lastPos < i) {
								if(spacePending) {
									write(' ');
								}
								write(chars, lastPos, i - lastPos);
							}
							spacePending = true;
							lastPos = i + 1;
						}
					} else {
						if(spacePending) {
							write(' ');
							spacePending = false;
						}
						if(lastPos < i) {
							write(chars, lastPos, i - lastPos);
						}
						lastPos = i + 1;
						writeAmpPoundEntity(ch);
					}
				} else if(ch < 128) {
					try {
						out = entities[ch];
					} catch(ArrayIndexOutOfBoundsException e) {
						out = null;
					}
					if(out != null) {
						if(spacePending) {
							write(' ');
							spacePending = false;
						}
	                    if(lastPos < i) {
	                        write(chars, lastPos, i - lastPos);
	                    }
	                    lastPos = i+1;
						write(out);
					}
				} else {
					try {
						out = entities[ch];
					} catch(ArrayIndexOutOfBoundsException e) {
						out = null;
					}
					if(spacePending) {
						write(' ');
						spacePending = false;
					}
					if(lastPos < i) {
						write(chars, lastPos, i - lastPos);
					}
					lastPos = i + 1;
					if(out == null) {
						writeAmpPoundEntity(ch);
					} else {
						write(out);
					}
				}
			}
			if(lastPos < end) {
				if(spacePending) {
					write(' ');
					spacePending = false;
				}
				write(chars, lastPos, end - lastPos);
			}
		} catch(IOException e) {
			throw new SAXException(e);
		}
    }

	protected final static int[] sizeTable = { 9, 99, 999, 9999, 99999, 999999, 9999999, 99999999, 999999999, Integer.MAX_VALUE };

	// Requires positive x
	public static int stringSize(int x) {
		for(int i = 0;; i++)
			if(x <= sizeTable[i])
				return i + 1;
	}

	protected void writeAmpPoundEntity(char ch) throws IOException {
		int length = stringSize(ch);
    	if(3 + length + offset > buffer.length) {
            flushBuffer();
        }
    	buffer[offset++] = '&';
    	buffer[offset++] = '#';

        int q,r,i;
        i = ch;
    	for (int p = offset + length - 1;p >= offset; p--) {
            q = (i * 52429) >>> (16+3);
            r = i - ((q << 3) + (q << 1));  // r = i-(q*10) ...
            buffer[p] = digits[r];
            i = q;
            //if (i == 0) break;
        }
    	offset += length;
    	buffer[offset++] = ';';
    }

    protected void writeMetaTags() throws SAXException {
    	char[] mt = getChars(getMediaType());
    	if(mt.length > 0) {
			try {
				switch(state) {
					case ATTR_VALUE:
						write('"');
						// fall-through
					case OPEN_START_TAG:
						write('>');
				}
				write(newline);
				writeIndent();
				write("<meta http-equiv=\"Content-Type\" content=\"");
				escapeCharacters(mt, 0, mt.length, false);
				char[] enc = getChars(getEncoding());
				if(enc.length > 0) {
					write(";charset=");
					escapeCharacters(enc, 0, enc.length, false);
				}
				write("\" />");
				state = TagState.HAS_CHILDREN;
			} catch(IOException e) {
				throw new SAXException(e);
    		}
    	}
    }

    protected void addSessionTokenInput(String namespaceURI, String token) throws SAXException {
    	String prefix = getPrefix(namespaceURI);
    	if(prefix != null && prefix.length() > 0)
    		prefix += ':';
    	AttributesImpl atts = new AttributesImpl();
        atts.addAttribute(namespaceURI, "type", prefix + "type", "CDATA", "hidden");       
        atts.addAttribute(namespaceURI, "name", prefix + "name", "CDATA", SESSION_TOKEN);       
        atts.addAttribute(namespaceURI, "value", prefix + "value", "CDATA", token);       
        startElement(namespaceURI, "input", prefix + "input", atts);
        endElement(namespaceURI, "input", prefix + "input");
    }
    
    public void endElement(String namespaceURI, String localName, String name) throws SAXException {
        if(localName == null) {
            int p = name.indexOf(':');
            if(p < 0) {
                namespaceURI = "";
                localName = name;
            } else {
                namespaceURI = getNamespaceURI(name.substring(0, p));
                localName = name.substring(p+1);
            }
        } else if(name == null) {
            String prefix = getPrefix(namespaceURI);
            if(prefix.length() > 0) name = prefix + ":" + localName;
            else name = localName;
        }
        if(!metaTagsWritten && isHTMLTag("head", namespaceURI, localName)) {
        	writeMetaTags();
        	metaTagsWritten = false;
        } 
		int tf = getTagFlag(depth - 1);
        String token = getSessionToken();
        if(TagFlags.TAG_FLAG_NEEDS_SESSION_TOKEN_INPUT.isSet(tf) && token != null) {
        	addSessionTokenInput(namespaceURI, token);
        }
		try {
			switch(state) {
				case ATTR_VALUE:
					write('"');
					// fall-through
				case OPEN_START_TAG:
					if(TagFlags.TAG_FLAG_NON_EMPTY.isSet(tf)) {
						write('>');
						writeEndTag(name);
					} else if(TagFlags.TAG_FLAG_ADD_NBSP.isSet(tf)) {
						write('>');
						writeContentForEmptyElement(name);
						writeEndTag(name);
					} else {
						if(spaceBeforeClose)
							write(' ');
						write("/>");
					}
					break;
				case HAS_CHILDREN:
					if(!TagFlags.TAG_FLAG_PRESERVE_SPACE.isSet(tf)) {
						write(newline);
						writeIndent();
					}
				case EMPTY_TAG:
				case HAS_CONTENT:
					if(TagFlags.TAG_FLAG_STYLE_ESCAPING.isSet(tf))
						write(END_CDATA_IN_COMMENT_CHARS);
					else if(TagFlags.TAG_FLAG_SCRIPT_ESCAPING.isSet(tf))
						write(END_CDATA_IN_COMMENT_CHARS);
					writeEndTag(name);
					break;
			}
		} catch(IOException e) {
			throw new SAXException(e);
		}
        newlinePending = true;
        spacePending = false; //!TagFlags.TAG_FLAG_PRESERVE_SPACE.isSet(tf) && state != TagState.HAS_CHILDREN;
        state = TagState.HAS_CHILDREN;
        depth--;
    }

	protected void writeContentForEmptyElement(String tagName) throws SAXException {
		try {
			write(NBSP, 0, NBSP.length);
		} catch(IOException e) {
			throw new SAXException(e);
		}
	}

    protected void writeEndTag(String name) throws SAXException {
		try {
			write("</");
			write(name);
			write('>');
		} catch(IOException e) {
			throw new SAXException(e);
		}
    }

    protected boolean isHTMLTag(String tag, String namespaceURI, String localName) {
        return xhtmlNamespaces.contains(namespaceURI) && tag.equalsIgnoreCase(localName);
    }

    protected boolean isHTMLTag(Collection<String> tags, String namespaceURI, String localName) {
        return xhtmlNamespaces.contains(namespaceURI) && tags.contains(localName);
    }

    protected int getTagFlags(String namespaceURI, String localName) {
        if(!xhtmlNamespaces.contains(namespaceURI)) return 0;
        //trying to optimize this - that's why it's ugly
        /*
        if("script".equalsIgnoreCase(localName)) return TAG_FLAG_NON_EMPTY | TAG_FLAG_NON_ESCAPING;
        if("iframe".equalsIgnoreCase(localName)) return TAG_FLAG_NON_EMPTY;
        if("td".equalsIgnoreCase(localName)) return TAG_FLAG_ADD_NBSP;
        if("th".equalsIgnoreCase(localName)) return TAG_FLAG_ADD_NBSP;
        return 0;
        /*/
        Integer tagFlags = tagFlagsMap.get(localName.toLowerCase());
        return tagFlags == null ? 0 : tagFlags;
        //*/
    }

    public void processingInstruction(String target, String data) throws SAXException {
    	// parse data as attribute1="value1" attribute2="value2"
    	Matcher matcher = attributePattern.matcher(data);
        Map<String,String> attributes = new HashMap<String, String>();
        while(matcher.lookingAt()) {
            attributes.put(matcher.group(1), matcher.group(3));
            matcher.region(matcher.end(), data.length());
        }
        if(matcher.regionStart() < matcher.regionEnd()) {
        	if(attributes.isEmpty())
        	    warn("data '" + data + "' in processing instruction '" + target + "' did not match expected pattern");
        	else
        		warn("extra data '" + data.substring(matcher.end()) + "' in processing instruction '" + target + "'");
        }
        if("client".equalsIgnoreCase(target)) {
            String userAgent = attributes.get("user-agent");
            if(userAgent != null && useOldDocTypePattern.matcher(userAgent).matches()) {
                // this corrects IE's inconsistency in handling percentage height/width of iframes
            	// but vastly changes other display layouts
                setDoctypePublic("-//W3C//DTD HTML 4.01 Transitional//EN");
                setDoctypeSystem(null);
            }
        } else if("output".equalsIgnoreCase(target)) {
        	String preserveSpace = attributes.get("preserve-space");
        	if(preserveSpace != null && (preserveSpace=preserveSpace.trim()).length() > 0) {
            	boolean value = (preserveSpace.equalsIgnoreCase("YES") || preserveSpace.equalsIgnoreCase("TRUE"));
            	int tf = getTagFlag(depth-1);
            	if(value) tf = tf | TagFlags.TAG_FLAG_PRESERVE_SPACE.getValue();
            	else tf = tf & ~TagFlags.TAG_FLAG_PRESERVE_SPACE.getValue();
            	setTagFlag(depth-1, tf);
        	}
        	String omitDtd = attributes.get("omit-dtd");
        	if(omitDtd != null && (omitDtd=omitDtd.trim()).length() > 0) {
            	boolean value = (omitDtd.equalsIgnoreCase("YES") || omitDtd.equalsIgnoreCase("TRUE"));
            	setOmitDTD(value);
        	}
        } else if("javax.xml.transform.disable-output-escaping".equalsIgnoreCase(target)) {
        	setEscaping(false);
        } else if("javax.xml.transform.enable-output-escaping".equalsIgnoreCase(target)) {
        	setEscaping(true);
        }
    }

    public void setDocumentLocator(Locator locator) {
        //debug("Locator " + locator + " was provided");
        this.locator = locator;
    }

    public void startDocument() throws SAXException {
    	char[] ver;
        if(isOmitXMLDeclaration()) {
        	// do nothing
        } else if((ver=getChars(getVersion())).length > 0) {
			try {
				write("<?xml version=\"");
				escapeCharacters(ver, 0, ver.length, false);// write(getVersion());
				write("\" ");
				char[] enc = getChars(getEncoding());
				if(enc.length > 0) {
					write("encoding=\"");
					escapeCharacters(enc, 0, enc.length, false);// write(getEncoding());
					write("\" ?>");
					write(getNewline());
				}
			} catch(IOException e) {
				throw new SAXException(e);
			}
        }
    }

    public void endDocument() throws SAXException {
        try {
            write(newline);
            flushBuffer();
            writer.flush();
        } catch(IOException e) {
            throw new SAXException(e);
		} finally {
			reset();
        }
    }

    protected static void addDefaultPrefixMapping(String prefix, String uri) {
        defaultPrefixToUri.put(prefix, uri);
        defaultUriToPrefix.put(uri, prefix);
    }

    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        //debug("Start prefix mapping '" + prefix + "' = " + uri);
        prefixToUri.put(prefix, uri);
        uriToPrefix.put(uri, prefix);
    }

    public void endPrefixMapping(String prefix) throws SAXException {
        //debug("End prefix mapping '" + prefix + "'");
        String uri = prefixToUri.remove(prefix);
        uriToPrefix.remove(uri);
    }

    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        // do nothing
    	spacePending = true;
    }

    public void skippedEntity(String name) throws SAXException {
        // do nothing
    }

//START: LexicalHandler methods (these are not called by xalan or xsltc)
    public void startDTD(String name, String publicId, String systemId) throws SAXException {
        //debug("DTD started '" + name + "'");
    }

    public void endDTD() throws SAXException {

    }

    public void startEntity(String name) throws SAXException {

    }

    public void endEntity(String name) throws SAXException {

    }

    public void startCDATA() throws SAXException {

    }

    public void endCDATA() throws SAXException {

    }

    public void comment(char[] ch, int start, int length) throws SAXException {
		try {
			// close element if necessary
			switch(state) {
				case ATTR_VALUE:
					write('"');
					// fall-through
				case OPEN_START_TAG:
					write('>');
					state = TagState.EMPTY_TAG;
			}
			if(newlinePending) {
				write(newline);
				writeIndent();
			}
			write("<!--");
			spacePending = false;
			for(int i = start; i < start + length; i++) {
				if(ch[i] == '-' && i > start && ch[i - 1] == '-')
					write("&#45;");
				else
					write(ch[i]);
			}
			write("-->");
		} catch(IOException e) {
			throw new SAXException(e);
		}
    }
//  END: LexicalHandler methods

    /**
     * @return Returns the writer.
     */
    public Writer getWriter() {
		return publicWriter;
    }

    /**
     * @param writer The writer to set.
     */
    public void setWriter(Writer writer) {
        this.writer = writer;
    }

    protected static char[] getChars(String s) {
    	if(s != null && (s=s.trim()).length() > 0) return s.toCharArray();
    	return EMPTY_CHARS;
    }

	protected void write(char ch) throws IOException {
        if(offset >= buffer.length) {
            flushBuffer();
        }
        buffer[offset++] = ch;
    }

	protected void flushBuffer() throws IOException {
		if(offset > 0) {
			writer.write(buffer, 0, offset);
			offset = 0;
        }
    }

	protected void write(String string) throws IOException {
        write(string, 0, string.length());
    }

	protected void write(String string, int start, int length) throws IOException {
		if(length >= buffer.length / 2) {
			// it's probably worth just writing this straight to the stream
			// instead of System.arraycopy
			flushBuffer();
			writer.write(string);
		} else {
			if(length + offset > buffer.length) {
                flushBuffer();
            }
			string.getChars(start, start + length, buffer, offset);
			offset += length;
        }
    }

	protected void write(char[] ch) throws IOException {
        write(ch, 0, ch.length);
    }

	protected void write(char[] ch, int start, int length) throws IOException {
        try {
			if(length >= buffer.length / 2) {
                // it's probably worth just writing this straight to the stream
                // instead of System.arraycopy
                flushBuffer();
                writer.write(ch, start, length);
            } else {
                if(length + offset > buffer.length) {
                    flushBuffer();
                }
                System.arraycopy(ch, start, buffer, offset, length);
                offset += length;
            }
        } catch(ArrayIndexOutOfBoundsException e) {
            warn("ArrayIndexOutOfBounds when start=" + start + "; length=" +length + "; char-length=" + ch.length + "; offset=" + offset);
            throw e;
        }
    }

    protected void log(String severity, String message, Throwable exception) {
        System.out.println("" + new java.util.Date() + " " + severity + " - " + message);
        if(exception != null) exception.printStackTrace();
    }

    protected void fatal(String message, Throwable exception) {
        log("FATAL", message, exception);
    }

    protected void fatal(String message) {
        fatal(message, null);
    }

    protected void error(String message, Throwable exception) {
        log("ERROR", message, exception);
    }

    protected void error(String message) {
        error(message, null);
    }

    protected void warn(String message, Throwable exception) {
        log("WARN", message, exception);
    }

    protected void warn(String message) {
        warn(message, null);
    }

    protected void info(String message, Throwable exception) {
        log("INFO", message, exception);
    }

    protected void info(String message) {
        info(message, null);
    }

    protected void debug(String message, Throwable exception) {
        log("DEBUG", message, exception);
    }

    protected void debug(String message) {
        debug(message, null);
    }

    /**
     * @return Returns the indentAmount.
     */
    public int getIndentAmount() {
        return indentAmount;
    }

    /**
     * @param indentAmount The indentAmount to set.
     */
    public void setIndentAmount(int indentAmount) {
        this.indentAmount = indentAmount;
    }

    /**
     * @return Returns the indent.
     */
    public boolean getIndent() {
        return indent;
    }

    /**
     * @param indent The indent to set.
     */
    public void setIndent(boolean indent) {
        this.indent = indent;
    }

    /**
     * @return Returns the doctypeName.
     */
    public String getDoctypeName() {
        return doctypeName;
    }

    /**
     * @return Returns the doctypePublic.
     */
    public String getDoctypePublic() {
        return doctypePublic;
    }

    /**
     * @param doctypePublic The doctypePublic to set.
     */
    public void setDoctypePublic(String doctypePublic) {
        this.doctypePublic = doctypePublic;
    }

    /**
     * @return Returns the doctypeSystem.
     */
    public String getDoctypeSystem() {
        return doctypeSystem;
    }

    /**
     * @param doctypeSystem The doctypeSystem to set.
     */
    public void setDoctypeSystem(String doctypeSystem) {
        this.doctypeSystem = doctypeSystem;
    }

    public void setDoctype(String system, String pub) {
        setDoctypePublic(pub);
        setDoctypeSystem(system);
    }

    /**
     * @return Returns the newline.
     */
    public String getNewline() {
        return newline;
    }

    /**
     * @param newline The newline to set.
     */
    public void setNewline(String newline) {
        this.newline = newline;
    }

    protected ErrorListener getErrorListener() {
        Transformer tran = getTransformer();
        if(transformer != null) return tran.getErrorListener();
        return null;
    }

    // output properties
    /**
     * @return Returns the outputFormat.
     */
    public Properties getOutputFormat() {
        return outputFormat;
    }

    /**
     * @param outputFormat The outputFormat to set.
     */
    public void setOutputFormat(Properties outputFormat) {
        this.outputFormat = outputFormat;
        //outputFormat.getProperty(OutputKeys.CDATA_SECTION_ELEMENTS);
        //setCdataSectionElements(OutputKeys.CDATA_SECTION_ELEMENTS, format);
        Integer n = getIntegerProp(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT, 4);
        if(n != null) {
            if(n < 0) {
            	warn("Indent Amount '" + n + "' is less than 0 - it will be ignored");
            	setIndentAmount(0);
            } else
            	setIndentAmount(n);
        }

        Boolean b;
        b = getBooleanProp(OutputKeys.INDENT, false);
        if(b != null) {
            setIndent(b);
        }

        b = getBooleanProp(OutputKeys.OMIT_XML_DECLARATION, true);
        if(b != null) {
            setOmitXMLDeclaration(b);
        }

        b = getBooleanProp(OMIT_DTD, false);
        if(b != null) {
            setOmitDTD(b);
        }

        String s;
        s = getStringProp(OutputKeys.DOCTYPE_SYSTEM, "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd");
        if(s != null) setDoctypeSystem(s);
        s = getStringProp(OutputKeys.DOCTYPE_PUBLIC, "-//W3C//DTD XHTML 1.0 Transitional//EN");
        if(s != null) setDoctypePublic(s);

        s = getStringProp(OutputKeys.STANDALONE, null);
        setStandalone(s);

        s = getStringProp(OutputKeys.MEDIA_TYPE, "text/html");
        if(s != null) setMediaType(s);

        s = getStringProp(OutputKeys.VERSION, "1.0");
        if(s != null) setVersion(s);

		s = getStringProp(OutputKeys.ENCODING, getDefaultEncoding());
        if(s != null) setEncoding(s);
        /*
        s = getStringProp(OutputPropertiesFactory.S_KEY_LINE_SEPARATOR);
        if(s != null) NEWLINE = s;
        */

        s = getStringProp(OutputPropertiesFactory.S_KEY_ENTITIES, null);
        if(s != null) loadEntities(s);
        
        s = getStringProp("omp:" + SESSION_TOKEN, null);
        if(s != null) setSessionToken(s);
    }

    protected String getStringProp(String key, String defaultValue) {
        //NOTE: at one time we ignored the default properties. I do not remember why, but I believe it
        // was necessary for the xalan transformer (com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl)
    	//String tmp = (String)outputFormat.get(key); // ignore the default properties);
        //return tmp;
    	return outputFormat.getProperty(key, defaultValue);
    }

    protected Integer getIntegerProp(String key, Integer defaultValue) {
        String tmp = getStringProp(key, null);
        if(tmp != null) {
            try {
                return Integer.parseInt(tmp);
            } catch(NumberFormatException e) {
                warn("Output Format for '" + key + "' is '" + tmp + "' which is not a number", e);
            }
        }
        return defaultValue;
    }

    protected Boolean getBooleanProp(String key, Boolean defaultValue) {
        String tmp = getStringProp(key, null);
        if(tmp != null) {
            if(tmp.equalsIgnoreCase("YES") || tmp.equalsIgnoreCase("TRUE"))return true;
            else if(tmp.equalsIgnoreCase("NO") || tmp.equalsIgnoreCase("FALSE")) return false;
            warn("Output Format for '" + key + "' is '" + tmp + "' which is not a boolean");
        }
        return defaultValue;
    }

    /**
     * @return Returns the encoding.
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * @param encoding The encoding to set.
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    /**
     * @return Returns the mediaType.
     */
    public String getMediaType() {
        return mediaType;
    }

    /**
     * @param mediaType The mediaType to set.
     */
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    /**
     * @return Returns the omitXMLDeclaration.
     */
    public boolean isOmitXMLDeclaration() {
        return omitXMLDeclaration;
    }

    /**
     * @param omitXMLDeclaration The omitXMLDeclaration to set.
     */
    public void setOmitXMLDeclaration(boolean omitXMLDeclaration) {
        this.omitXMLDeclaration = omitXMLDeclaration;
    }

    /**
     * @return Returns the standalone.
     */
    public String getStandalone() {
        return standalone;
    }

    /**
     * @param standalone The standalone to set.
     */
    public void setStandalone(String standalone) {
        this.standalone = standalone;
    }

    /**
     * @return Returns the transformer.
     */
    public Transformer getTransformer() {
        return transformer;
    }

    /**
     * @param transformer The transformer to set.
     */
    public void setTransformer(Transformer transformer) {
        this.transformer = transformer;
    }

    /**
     * @return Returns the version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version The version to set.
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return Returns the sourceLocator.
     */
    public SourceLocator getSourceLocator() {
        return sourceLocator;
    }

    /**
     * @param sourceLocator The sourceLocator to set.
     */
    public void setSourceLocator(SourceLocator sourceLocator) {
        this.sourceLocator = sourceLocator;
    }

    public boolean reset() {
        state = TagState.NO_TAG;
        depth = 0;
        prefixToUri.clear();
        uriToPrefix.clear();
        offset = 0;
        metaTagsWritten = false;
        //should these be reset too?
        /*
        outputFormat = new Properties();
        transformer = null;
        sourceLocator = null;
        //*/
        return true;
    }

	public boolean isOmitDTD() {
		return omitDTD;
	}

	public void setOmitDTD(boolean omitDTD) {
		this.omitDTD = omitDTD;
	}

	public boolean setEscaping(boolean escape) {
        int tf = tagFlagStack[depth-1];
        final boolean old = !TagFlags.TAG_FLAG_NON_ESCAPING.isSet(tf);
        if(old != escape && (!escape || TagFlags.TAG_FLAG_NON_ESCAPING.isSet(tf))) {
            tagFlagStack[depth-1] = (tf ^ TagFlags.TAG_FLAG_NON_ESCAPING.getValue()) ^  TagFlags.TAG_FLAG_USER_SET_ESCAPING.getValue();
        }
        return old;
    }

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public static String getDefaultEncoding() {
		return defaultEncoding;
	}

	public static void setDefaultEncoding(String defaultEncoding) {
		XHTMLHandler.defaultEncoding = defaultEncoding;
	}
}
