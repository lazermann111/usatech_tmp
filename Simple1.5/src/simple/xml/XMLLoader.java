/*
 * XMLLoader.java
 *
 * Created on October 17, 2003, 10:16 AM
 */

package simple.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;

/** This class defines a mechanism for loading XML into Java Objects. Each implementation
 *  processes the XML differently.
 *
 * @author  Brian S. Krug
 */
public abstract class XMLLoader<E> {
    private static final simple.io.Log log = simple.io.Log.getLog();

    private static final String INLINE_DTD_NOT_ALLOWED = "Inline DTD is not allowed";
    protected XMLExtraNodes xmlExtraNodes;

    /**
     * Holds value of property validating.
     */
    private javax.xml.parsers.SAXParserFactory factory;

    /** Creates a new instance of XMLLoader */
    public XMLLoader() {
    	 factory = javax.xml.parsers.SAXParserFactory.newInstance();
    	 setValidating(true);
    }

    /** Loads the XML from the file with the specified path
     * and returns a represenation of it
     * @param filePath The path where the file to parse is located
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @return The "root" object of the implementation's representation of the XML
     */
    public E load(String filePath) throws IOException, SAXException, ParserConfigurationException {
        InputStream in = getClass().getClassLoader().getResourceAsStream(filePath);
        if(in == null) in = ClassLoader.getSystemResourceAsStream(filePath);
        if(in == null) throw new IOException("Couldn't locate " + filePath);
        return load(in);
    }

    /** Loads the XML specified in the parameter and returns a represenation of it
     * @param xml The XML to parse
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @return The "root" object of the implementation's representation of the XML
     */
    public E loadFromString(String xml) throws IOException, SAXException, ParserConfigurationException {
        return load(new java.io.ByteArrayInputStream(xml.getBytes()));
    }

    /** Loads the XML from the given InputStream
     * and returns a represenation of it
     * @param in The InputStream containing XML to parse
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @return The "root" object of the implementation's representation of the XML
     */
    public E load(InputStream in) throws IOException, SAXException, ParserConfigurationException {
    	return load(new InputSource(in));
    }

    private String [] DTD_URI_START_WHITE_LIST = {
    	"http://www.w3.org/",
    	"http://db.apache.org/",
    	"http://java.sun.com/",
    	"http://jakarta.apache.org/",
    	//"http://masqmail.cx/",
    	"http://www.usatech.com/",
    	"/usr/",
    	"http://www.bea.com/",
    	"amqp.dtd"
    };

    public E load(InputSource in) throws IOException, SAXException, ParserConfigurationException {
        log.info("Processing xml stream");
        XMLHandler xh = new XMLHandler();
        SAXParser parser = factory.newSAXParser();
        if(getXmlExtraNodes() != null)
        	parser.setProperty("http://xml.org/sax/properties/lexical-handler", xh);
        parser.getXMLReader().setProperty("http://xml.org/sax/properties/declaration-handler", xh);
    	parser.parse(in, xh);
    	log.info("Processed XML stream: " + in);
        return xh.getRoot();
    }

    protected abstract E createObject(String tag, DocumentLocation location) throws XMLLoadingException ;

    protected abstract void applyAttribute(E o, String name, String value) throws XMLLoadingException ;

    protected abstract void addTo(E parent, E o, String tag) throws XMLLoadingException ;

    protected abstract void addText(E o, String text) throws XMLLoadingException ;

    protected abstract void finishObject(E o) throws XMLLoadingException ;

    /**
     * Getter for property validating.
     * @return Value of property validating.
     */
    public boolean isValidating() {
        return factory.isValidating();
    }

    /**
     * Setter for property validating.
     * @param validating New value of property validating.
     */
    public void setValidating(boolean validating) {
        factory.setValidating(validating);
        try {
			factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", validating);
		} catch(SAXNotRecognizedException e) {
			log.warn("Could not set 'http://apache.org/xml/features/nonvalidating/load-external-dtd'", e);
		} catch(SAXNotSupportedException e) {
			log.warn("Could not set 'http://apache.org/xml/features/nonvalidating/load-external-dtd'", e);
		} catch(ParserConfigurationException e) {
			log.warn("Could not set 'http://apache.org/xml/features/nonvalidating/load-external-dtd'", e);
		}
    }
    private static final Pattern whitespacePattern = Pattern.compile("\\s+");
    protected static class StackObject<E> {
    	public final E createdObject;
    	public final DocumentLocation location;
    	public StackObject(E createdObject, DocumentLocation location) {
    		this.createdObject = createdObject;
    		this.location = location;
    	}
    }
    protected class XMLHandler extends org.xml.sax.helpers.DefaultHandler implements LexicalHandler, DeclHandler {
        protected E root;
        protected LinkedList<StackObject<E>> stack = new LinkedList<StackObject<E>>();
        protected Locator locator;
        protected boolean resolvedEntity;
        protected boolean startedElement;
        protected XMLHandler() {}

        public void setDocumentLocator(Locator locator) {
    		this.locator = locator;
        }
        public void startElement(String uri, String localName, String qName, org.xml.sax.Attributes attributes) throws SAXException {
        	log.debug("startElement: uri=["+uri+"], localNane="+localName+", qName="+qName);
        	startedElement = true;
            try {
                //create new element
                String tag = localName == null || localName.length() == 0 ? qName : localName;
                DocumentLocation location;
                if(locator != null) {
                	location = new DocumentLocation();
                	location.setPublicId(locator.getPublicId());
                	location.setSystemId(locator.getSystemId());
                	location.setStartLine(locator.getLineNumber());
                	location.setStartColumn(locator.getColumnNumber());
                } else {
                	location = null;
                }
                E o = createObject(tag, location);
            	if(getXmlExtraNodes() != null) {
            		getXmlExtraNodes().associateExtraNodes(o);
            	}
                //set properties from attributes
                for(int i = 0; i < attributes.getLength(); i++) {
                    String attrName = attributes.getLocalName(i);
                    applyAttribute(o, attrName == null || attrName.length() == 0 ? attributes.getQName(i) : attrName, attributes.getValue(i));
                }
                //add to parent
                if(stack.isEmpty()) root = o;
                else addTo(stack.getLast().createdObject, o, tag);
                //add to stack
                stack.add(new StackObject<E>(o,location));
            } catch(XMLLoadingException e) {
                throw new SAXException(e);
            }
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            String s = new String(ch, start, length);
            if(getXmlExtraNodes() != null && whitespacePattern.matcher(s).matches()) {
            	//add whitespace
            	getXmlExtraNodes().addWhitespace(s);
            }
            try {
                addText(stack.getLast().createdObject, s);
            } catch(XMLLoadingException e) {
                throw new SAXException(e);
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            try {
            	StackObject<E> so = stack.removeLast();
            	if(locator != null && so.location != null) {
            		so.location.setEndLine(locator.getLineNumber());
            		so.location.setEndColumn(locator.getColumnNumber());
            	}
                finishObject(so.createdObject);
            } catch(java.util.NoSuchElementException e) {
                throw new SAXException("Too many end elements", e);
            } catch(XMLLoadingException e) {
                throw new SAXException(e);
            }
        }

        public E getRoot() {
            return root;
        }
        @Override
        public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        	if(getXmlExtraNodes() != null) {
        		getXmlExtraNodes().addWhitespace(new String(ch, start, length));
        	}
        }
		public void comment(char[] ch, int start, int length) throws SAXException {
        	if(getXmlExtraNodes() != null) {
        		getXmlExtraNodes().addComment(new String(ch, start, length));
        	}
		}
		public void endCDATA() throws SAXException {
		}
		public void endDTD() throws SAXException {
		}
		public void endEntity(String name) throws SAXException {
		}
		public void startCDATA() throws SAXException {
		}
		public void startDTD(String name, String publicId, String systemId) throws SAXException {
		}
		public void startEntity(String name) throws SAXException {
		}
		public InputSource resolveEntity (String publicId, String systemId)
				throws IOException, SAXException
		{
			log.debug("resolveEntity: publicId=["+publicId+"], system="+systemId);
			if (resolvedEntity && !startedElement)
					throw new SAXException(XMLLoader.INLINE_DTD_NOT_ALLOWED);
			if (systemId == null || systemId.isEmpty()) {
				resolvedEntity = true;
				return null;
			}

			for (String s : DTD_URI_START_WHITE_LIST) {
				if (systemId.startsWith(s)) {
					resolvedEntity = true;
					return null;
				}
			}
			return new InputSource(new StringReader(""));
		}
    public void notationDecl (String name, String publicId, String systemId)
        throws SAXException
    {
			log.debug("notationDecl: name=["+name+"], publicId=["+publicId+"], system="+systemId);
			if (!resolvedEntity)
				throw new SAXException(XMLLoader.INLINE_DTD_NOT_ALLOWED);
    }
    public void unparsedEntityDecl (String name, String publicId,
        String systemId, String notationName)
		throws SAXException
		{
			log.debug("unparsedEntityDecl: name=["+name+"], publicId=["+publicId+"], system=["+systemId+"], nnname="+notationName);
			if (!resolvedEntity)
				throw new SAXException(XMLLoader.INLINE_DTD_NOT_ALLOWED);
		}
    public void elementDecl (String name, String model) throws SAXException {
			log.debug("elementDecl: name=["+name+"], model="+model);
			if (!resolvedEntity)
				throw new SAXException(XMLLoader.INLINE_DTD_NOT_ALLOWED);
    }
    public void attributeDecl (String eName,
                                        String aName,
                                        String type,
                                        String mode,
                                        String value)
        throws SAXException {
			log.debug("attributeecl: eName=["+eName+"], aName=["+aName+"], type=["+type+"], mode=["+mode+"], value="+value);
			if (!resolvedEntity)
				throw new SAXException(XMLLoader.INLINE_DTD_NOT_ALLOWED);
    }
    public void internalEntityDecl (String name, String value) throws SAXException {
			log.debug("internalEntityDecl: name=["+name+"], value="+value);
			if (!resolvedEntity)
				throw new SAXException(XMLLoader.INLINE_DTD_NOT_ALLOWED);
    }
    public void externalEntityDecl (String name, String publicId, String systemId) throws SAXException {
			log.debug("notationDecl: name=["+name+"], publicId=["+publicId+"], system="+systemId);
			if (!resolvedEntity)
				throw new SAXException(XMLLoader.INLINE_DTD_NOT_ALLOWED);
    }

    }

	public XMLExtraNodes getXmlExtraNodes() {
		return xmlExtraNodes;
	}

	public void setXmlExtraNodes(XMLExtraNodes xmlExtraNodes) {
		this.xmlExtraNodes = xmlExtraNodes;
	}
}
