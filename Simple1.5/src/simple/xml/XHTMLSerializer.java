/*
 * Created on Nov 7, 2005
 *
 */
package simple.xml;


import simple.io.Log;
import simple.xml.serializer.XHTMLHandler;

public class XHTMLSerializer extends XHTMLHandler {
    private static final Log log = Log.getLog();

    @Override
    protected void fatal(String message, Throwable e) {
        log.fatal(message, e);
    }
    @Override
    protected void error(String message, Throwable e) {
        log.error(message, e);
    }
    @Override
    protected void warn(String message, Throwable e) {
        log.warn(message, e);
    }
    @Override
    protected void info(String message, Throwable e) {
        log.info(message, e);
    }
    @Override
    protected void debug(String message, Throwable e) {
        log.debug(message, e);
    }   
}
