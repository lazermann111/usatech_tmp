package simple.xml;

import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public interface ExtendedContentHandler extends ContentHandler {

	public void characters(String ch) throws SAXException;

	public void comment(String ch) throws SAXException;

	public void startElement(String localName) throws SAXException;

	public void startElement(String localName, Map<String, String> attributes) throws SAXException;

	public void startElement(String localName, String[] attributeNames, String... attributeValues)
			throws SAXException;

	public void startElement(String localName, Attributes atts) throws SAXException;

	public void endElement(String localName) throws SAXException;

}