package simple.xml;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;

public class XMLExtraNodes {
	protected final Map<Object,List<ExtraNode>> extraNodeMap = new IdentityHashMap<Object, List<ExtraNode>>();
	protected List<ExtraNode> pendingNodes;
	
	public static interface ExtraNode {
		public void write(ContentHandler ch) throws SAXException;
	}
	protected static class WhitespaceExtraNode implements ExtraNode {
		protected String whitespace;
		public WhitespaceExtraNode(String whitespace) {
			this.whitespace = whitespace;
		}
		public void write(ContentHandler ch) throws SAXException {
			ch.ignorableWhitespace(whitespace.toCharArray(), 0, whitespace.length());
		}
	}
	protected static class CommentExtraNode implements ExtraNode {
		protected String comment;
		public CommentExtraNode(String comment) {
			this.comment = comment;
		}
		public void write(ContentHandler ch) throws SAXException {
			if(ch instanceof LexicalHandler) {
				((LexicalHandler)ch).comment(comment.toCharArray(), 0, comment.length());
			}
		}
	}
	public void replaceAssociation(Object old, Object replacement) {
		List<ExtraNode> nodes = extraNodeMap.remove(old);
		if(nodes != null)
			extraNodeMap.put(replacement, nodes);
	}
	
	public void associateExtraNodes(Object object) {
		if(pendingNodes != null && !pendingNodes.isEmpty()) {
			///*
			ExtraNode en = pendingNodes.get(pendingNodes.size()-1); //pendingNodes.get(0);
			if(en instanceof WhitespaceExtraNode) {
				trimLastNewline((WhitespaceExtraNode)en);
			}//*/
			extraNodeMap.put(object, pendingNodes);
			pendingNodes = null;
		}
	}
	private static final Pattern lastNewlinePattern = Pattern.compile("(\\s*?(?:\\n\\r|\\r\\n|\\n|\\r))[^\\n\\r]*(?:\\n\\r|\\r\\n|\\n|\\r)([^\\n\\r]*)");
	//private static final Pattern firstNewlinePattern = Pattern.compile("(?:[^\\n\\r]*(?:\\n\\r|\\r\\n|\\n|\\r)){2}(\\s*?)");
    
	protected void trimLastNewline(WhitespaceExtraNode node) {
		Matcher matcher = lastNewlinePattern.matcher(node.whitespace);
		if(matcher.matches()) {
			node.whitespace = matcher.group(1) + matcher.group(2);
		}		
	}

	public void addComment(String comment) {
		getPendingNodes().add(new CommentExtraNode(comment));
	}
	public void addWhitespace(String whitespace) {
		getPendingNodes().add(new WhitespaceExtraNode(whitespace));	
	}
	
	protected List<ExtraNode> getPendingNodes() {
		if(pendingNodes == null)
			pendingNodes = new ArrayList<ExtraNode>();
		return pendingNodes;
	}
	
	public void writeExtraNodes(Object object, ContentHandler ch) throws SAXException {
		List<ExtraNode> nodes = extraNodeMap.get(object);
		if(nodes != null)
			for(ExtraNode node : nodes) {
				node.write(ch);
			}
	}
}
