/*
 * Created on Jan 14, 2005
 *
 */
package simple.xml;

import java.io.IOException;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ext.LexicalHandler;

/** Warning: This class is not thread safe since it holds the ContentHandler, DTDHandler, etc.
 * @author bkrug
 *
 */
public abstract class DelegatingXMLHandler extends AbstractContentHandler implements ContentHandler, DTDHandler, EntityResolver, ErrorHandler, LexicalHandler {
    protected ContentHandler contentHandler;
    protected DTDHandler dtdHandler;
    protected EntityResolver entityResolver;
    protected ErrorHandler errorHandler;
    protected LexicalHandler lexicalHandler;
    protected String prefix = null;

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#getContentHandler()
     */
    public ContentHandler getContentHandler() {
        return contentHandler;
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#setContentHandler(org.xml.sax.ContentHandler)
     */
    public void setContentHandler(ContentHandler handler) {
        this.contentHandler = handler;
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#getDTDHandler()
     */
    public DTDHandler getDTDHandler() {
        return dtdHandler;
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#setDTDHandler(org.xml.sax.DTDHandler)
     */
    public void setDTDHandler(DTDHandler handler) {
        this.dtdHandler = handler;
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#getEntityResolver()
     */
    public EntityResolver getEntityResolver() {
        return entityResolver;
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#setEntityResolver(org.xml.sax.EntityResolver)
     */
    public void setEntityResolver(EntityResolver resolver) {
        this.entityResolver = resolver;
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#getErrorHandler()
     */
    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    /* (non-Javadoc)
     * @see org.xml.sax.XMLReader#setErrorHandler(org.xml.sax.ErrorHandler)
     */
    public void setErrorHandler(ErrorHandler handler) {
        this.errorHandler = handler;
    }

    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#characters(char[], int, int)
     */
    public void characters(char[] ch, int start, int length) throws SAXException {
        if(contentHandler != null) contentHandler.characters(ch, start, length);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#endDocument()
     */
    public void endDocument() throws SAXException {
        if(contentHandler != null) contentHandler.endDocument();
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
     */
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        if(contentHandler != null) contentHandler.endElement(namespaceURI, localName, qName);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#endPrefixMapping(java.lang.String)
     */
    public void endPrefixMapping(String prefix) throws SAXException {
        if(contentHandler != null) contentHandler.endPrefixMapping(prefix == null ? "" : prefix);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#ignorableWhitespace(char[], int, int)
     */
    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        if(contentHandler != null) contentHandler.ignorableWhitespace(ch, start, length);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#processingInstruction(java.lang.String, java.lang.String)
     */
    public void processingInstruction(String target, String data) throws SAXException {
        if(contentHandler != null) contentHandler.processingInstruction(target, data);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#setDocumentLocator(org.xml.sax.Locator)
     */
    public void setDocumentLocator(Locator locator) {
        if(contentHandler != null) contentHandler.setDocumentLocator(locator);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#skippedEntity(java.lang.String)
     */
    public void skippedEntity(String name) throws SAXException {
        if(contentHandler != null) contentHandler.skippedEntity(name);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#startDocument()
     */
    public void startDocument() throws SAXException {
        if(contentHandler != null) contentHandler.startDocument();
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
     */
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
            throws SAXException {
        if(contentHandler != null) contentHandler.startElement(namespaceURI, localName, qName, atts);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ContentHandler#startPrefixMapping(java.lang.String, java.lang.String)
     */
    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        if(prefix == null) prefix = "";
        if(contentHandler != null) contentHandler.startPrefixMapping(prefix, uri);
        if(uri.equalsIgnoreCase(getNameSpaceURI())) this.prefix = prefix;
    }
    /* (non-Javadoc)
     * @see org.xml.sax.DTDHandler#notationDecl(java.lang.String, java.lang.String, java.lang.String)
     */
    public void notationDecl(String name, String publicId, String systemId) throws SAXException {
        if(dtdHandler != null) dtdHandler.notationDecl(name, publicId, systemId);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.DTDHandler#unparsedEntityDecl(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public void unparsedEntityDecl(String name, String publicId, String systemId, String notationName)
            throws SAXException {
        if(dtdHandler != null) dtdHandler.unparsedEntityDecl(name, publicId, systemId, notationName);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String, java.lang.String)
     */
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
        if(entityResolver != null) return entityResolver.resolveEntity(publicId, systemId);
        return null;
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
     */
    public void error(SAXParseException exception) throws SAXException {
        if(errorHandler != null) errorHandler.error(exception);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
     */
    public void fatalError(SAXParseException exception) throws SAXException {
        if(errorHandler != null) errorHandler.fatalError(exception);
    }
    /* (non-Javadoc)
     * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
     */
    public void warning(SAXParseException exception) throws SAXException {
        if(errorHandler != null) errorHandler.warning(exception);
    }

	@Override
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public LexicalHandler getLexicalHandler() {
		return lexicalHandler;
	}

	public void setLexicalHandler(LexicalHandler lexicalHandler) {
		this.lexicalHandler = lexicalHandler;
	}

	public void comment(char[] ch, int start, int length) throws SAXException {
		if(lexicalHandler != null) lexicalHandler.comment(ch, start, length);
	}

	public void endCDATA() throws SAXException {
		if(lexicalHandler != null) lexicalHandler.endCDATA();
	}

	public void endDTD() throws SAXException {
		if(lexicalHandler != null) lexicalHandler.endDTD();
	}

	public void endEntity(String name) throws SAXException {
		if(lexicalHandler != null) lexicalHandler.endEntity(name);
	}

	public void startCDATA() throws SAXException {
		if(lexicalHandler != null) lexicalHandler.startCDATA();
	}

	public void startDTD(String name, String publicId, String systemId) throws SAXException {
		if(lexicalHandler != null) lexicalHandler.startDTD(name, publicId, systemId);
	}

	public void startEntity(String name) throws SAXException {
		if(lexicalHandler != null) lexicalHandler.startEntity(name);
	}
}
