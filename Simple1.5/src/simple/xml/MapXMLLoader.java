/*
 * MapXMLLoader.java
 *
 * Created on October 17, 2003, 10:16 AM
 */

package simple.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/** Loads XML into a heirarchy of <CODE>ParentMap</CODE> objects. An xml element's
 * attributes (its name and value) are loaded as <CODE>java.util.Map</CODE> entries (i.e. - key / value
 * pairs). Sub-elements are accessed using the <CODE>getChildren()</CODE> method of
 * each <CODE>ParentMap</CODE> object. The tag of an element is entered into the
 * Map using the <CODE>tagKey</CODE> field as the entry key.
 * The character body content of an element is entered into the
 * Map using the <CODE>textKey</CODE> field as the entry key. Content broken by sub-elements is concatenated together.
 * The object returned from the load methods is
 * a <CODE>ParentMap</CODE> object that represents the root XML element.
 * @author Brian S. Krug
 */
public class MapXMLLoader extends XMLLoader<MapXMLLoader.ParentMap> {
     /** Holds value of property tagKey. */
     private final String tagKey;

     /** Holds value of property textKey. */
     private final String textKey;

     /** Creates a new MapXMLLoader. Uses "tag" as the tagKey and "text" as the textKey */
     public MapXMLLoader() {
         this(null, null);
     }

     /** Creates a new MapXMLLoader
      * @param tagKey The string used as the key for entering the name of the tag into the
      * <CODE>ParentMap</CODE> object.
      * @param textKey The string used as the key for entering the character body content of an XML element into the
      * <CODE>ParentMap</CODE> object. Content broken by sub-elements is concatenated together.
      */
     public MapXMLLoader(String tagKey, String textKey) {
         this.tagKey = tagKey;
         this.textKey = textKey;
    }

    @Override
	protected ParentMap createObject(String tag, DocumentLocation location) throws XMLLoadingException {
        ParentMap m = new ParentMap();
        m.setLocation(location);
        m.setTag(tag);
        if(getTagKey() != null) m.put(getTagKey(), tag);
        return m;
    }

    @Override
	protected void applyAttribute(ParentMap o, String name, String value) throws XMLLoadingException {
        o.put(name, value);
    }

    @Override
	protected void addTo(ParentMap parent, ParentMap o, String tag) throws XMLLoadingException {
        parent.addChild(o);
    }

    @Override
	protected void addText(ParentMap m, String text) throws XMLLoadingException {
        String value = m.getText();
        if(value == null) value = text;
        else value = value + text;
        m.setText(value);
        if(getTextKey() != null) m.put(getTextKey(), value);
    }

    @Override
	protected void finishObject(ParentMap o) throws XMLLoadingException {
    }

    /** Getter for property tagKey.
     * @return Value of property tagKey.
     *
     */
    public String getTagKey() {
        return this.tagKey;
    }


    /** Getter for property textKey.
     * @return Value of property textKey.
     *
     */
    public String getTextKey() {
        return this.textKey;
    }

    /** Extension of <CODE>java.util.Map</CODE> that allows adding and retrieving of child
     * objects.
     */
	public static class ParentMap extends LinkedHashMap<String, String> {
        private static final long serialVersionUID = -22348471207L;
		protected String text;
        protected String tag;
        protected Map<String, List<ParentMap>> childrenMap = new HashMap<String, List<ParentMap>>();
        protected List<ParentMap> childrenList = new ArrayList<ParentMap>();
        protected DocumentLocation location;
        protected ParentMap parentNode;

        /** Adds a child object to this ParentMap
         * @param child The object to add as a child
         */
        public void addChild(ParentMap child) {
            childrenList.add(child);
            String tag = child.getTag();
            List<ParentMap> maps = childrenMap.get(tag);
            if(maps == null) {
                maps = new ArrayList<ParentMap>();
                childrenMap.put(tag, maps);
            }
            maps.add(child);
            child.parentNode = this;
        }
        /** Returns all the children in this ParentMap
         * @return The children of this ParentMap as an array of ParentMaps
         */
        public ParentMap[] getChildren() {
            return childrenList.toArray(new ParentMap[childrenList.size()]);
        }
        /** Returns all the children in this ParentMap
         * @return The children of this ParentMap as an array of ParentMaps
         */
        public ParentMap[] getChildren(String tag) {
            List<ParentMap> maps = childrenMap.get(tag);
            if(maps == null) return new ParentMap[0];
            else return maps.toArray(new ParentMap[maps.size()]);
        }
        public String getTag() {
            return tag;
        }
        public void setTag(String tag) {
            this.tag = tag;
        }
        public String getText() {
            return text;
        }
        public void setText(String text) {
            this.text = text;
        }
		public DocumentLocation getLocation() {
			return location;
		}
		public void setLocation(DocumentLocation location) {
			this.location = location;
		}
		public ParentMap getParentNode() {
			return parentNode;
		}

    }
}
