/*
 * Validator.java
 *
 * Created on February 26, 2004, 5:32 PM
 */

package simple.xml;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 *
 * @author  Brian S. Krug
 */
public class XSDValidator {

    /** Creates a new instance of Validator */
    public XSDValidator() {
    }

    public static void validateXML(InputStream xml, ErrorHandler handler) throws ParserConfigurationException, SAXException, IOException {
        validateXML_netbeans(xml, handler);
    }

    protected static void validateXML_oracle(InputStream xml, ErrorHandler handler) {
        // oracle's way
        /*
        oracle.xml.parser.v2.SAXParser saxParser = new oracle.xml.parser.v2.SAXParser();
        saxParser.setValidationMode(oracle.xml.parser.v2.XMLParser.SCHEMA_VALIDATION);
        saxParser.setXMLSchema(SchemaUrl);
        Validator handler = new Validator();
        saxParser.setErrorHandler(handler);
        saxParser.parse(XmlDocumentUrl);
        */
    }
    protected static void validateXML_netbeans(InputStream xml, ErrorHandler handler) throws ParserConfigurationException, SAXException, IOException {
        //from netbeans
    	SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setValidating(true);
        SAXParser parser = factory.newSAXParser();
        XMLReader reader = parser.getXMLReader();
        reader.setFeature("http://xml.org/sax/features/validation", true);
        reader.setFeature("http://apache.org/xml/features/validation/schema", true);
        reader.setFeature("http://apache.org/xml/features/validation/schema-full-checking", true);
        //EntityResolver res = createEntityResolver();
        //reader.setEntityResolver(new VerboseEntityResolver(res));
        reader.setErrorHandler(handler);
        //reader.setContentHandler(handler);
        reader.parse(new InputSource(xml));
    }
    public static void validateSchema(Source xml, Source xsd) throws SAXException, IOException {
    	// Create a SchemaFactory capable of understanding WXS schemas.
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

        // Load a WXS schema, represented by a Schema instance.
        Schema schema = factory.newSchema(xsd);

        // Create a Validator object, which can be used to validate an instance document.
        Validator validator = schema.newValidator();

        // Validate the DOM tree.
        validator.validate(xml);
    }
}
