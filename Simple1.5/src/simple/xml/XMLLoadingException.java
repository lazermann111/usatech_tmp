/*
 * Created on Jul 25, 2005
 *
 */
package simple.xml;

public class XMLLoadingException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -9345149612348L;

	public XMLLoadingException() {
        super();
    }

    public XMLLoadingException(String message) {
        super(message);
    }

    public XMLLoadingException(String message, Throwable cause) {
        super(message, cause);
    }

    public XMLLoadingException(Throwable cause) {
        super(cause);
    }

}
