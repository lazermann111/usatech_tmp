package simple.xml;

import org.xml.sax.helpers.AttributesImpl;

public class AppendableAttributes extends AttributesImpl {
	protected final String defaultUri;
	protected final String defaultPrefix;
	protected static final String defaultType = "CDATA";
	public AppendableAttributes() {
		this(null, null);
	}
	public AppendableAttributes(String defaultUri, String defaultPrefix) {
		this.defaultUri = defaultUri;
		this.defaultPrefix = defaultPrefix != null && (defaultPrefix = defaultPrefix.trim()).isEmpty() ? null : defaultPrefix;
	}
	public AppendableAttributes add(String localName, String value) {
		addAttribute(defaultUri, localName, defaultPrefix == null ? localName : defaultPrefix + ':' + localName, defaultType, value);
		return this;
	}

	public AppendableAttributes addPairs(final String... pairs) {
		if(pairs == null)
			return this;
		if(pairs.length % 2 != 0)
			throw new IllegalArgumentException("Number of arguments must be even");
		for(int i = 0; i < pairs.length - 1; i += 2)
			add(pairs[i], pairs[i + 1]);
		return this;
	}
}
