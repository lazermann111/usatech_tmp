/*
 * Created on Jan 13, 2006
 *
 */
package simple.xml;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XPathEvaluator {
    protected Document document;
    protected XPath xpath;
    protected static DocumentBuilder documentBuilder; 
    protected static XPathFactory xpathFactory = XPathFactory.newInstance(); 
    public XPathEvaluator(Document document) {
        super();
        this.document = document;
        this.xpath = xpathFactory.newXPath();
    }
    public XPathEvaluator(InputSource inputSource) throws SAXException, IOException, ParserConfigurationException {
        this(getDocumentBuilder().parse(inputSource));
    }
    public XPathEvaluator(InputStream inputStream) throws SAXException, IOException, ParserConfigurationException {
        this(getDocumentBuilder().parse(inputStream));
    }

	public static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
        if(documentBuilder == null) documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        return documentBuilder;
    }

	public static XPathFactory getXPathFactory() {
		return xpathFactory;
	}
    public String evaluateAsString(String xpathExpression) throws XPathExpressionException {
        return xpath.evaluate(xpathExpression, document);
    }
    public boolean evaluateAsBoolean(String xpathExpression) throws XPathExpressionException {
        return (Boolean)xpath.evaluate(xpathExpression, document, XPathConstants.BOOLEAN);
    }
    public Node evaluateAsNode(String xpathExpression) throws XPathExpressionException {
        return (Node)xpath.evaluate(xpathExpression, document, XPathConstants.NODE);
    }
    public NodeList evaluateAsNodeList(String xpathExpression) throws XPathExpressionException {
        return (NodeList)xpath.evaluate(xpathExpression, document, XPathConstants.NODESET);
    }
    public double evaluateAsNumber(String xpathExpression) throws XPathExpressionException {
        return (Double)xpath.evaluate(xpathExpression, document, XPathConstants.NUMBER);
    }
}
