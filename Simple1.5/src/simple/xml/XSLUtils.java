/*
 * Created on Mar 8, 2006
 *
 */
package simple.xml;

import java.math.BigDecimal;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author bkrug
 *
 */
public class XSLUtils {
    private XSLUtils() {
    }/*
    //ExprParser (XT) - for XPATH expression parsing
    public static class XTNodeWrapper implements Node {
        protected com.jclark.xsl.om.Node delegate;
        public XTNodeWrapper(com.jclark.xsl.om.Node delegate) {
            this.delegate = delegate;
        }
        public String getNodeName() {
            return delegate.getName().;
        }
        public String getNodeValue() throws DOMException {
            // TODO Auto-generated method stub
            return null;
        }
        public void setNodeValue(String nodeValue) throws DOMException {
            // TODO Auto-generated method stub
            
        }
        public short getNodeType() {
            // TODO Auto-generated method stub
            return 0;
        }
        public Node getParentNode() {
            // TODO Auto-generated method stub
            return null;
        }
        public NodeList getChildNodes() {
            // TODO Auto-generated method stub
            return null;
        }
        public Node getFirstChild() {
            // TODO Auto-generated method stub
            return null;
        }
        public Node getLastChild() {
            // TODO Auto-generated method stub
            return null;
        }
        public Node getPreviousSibling() {
            // TODO Auto-generated method stub
            return null;
        }
        public Node getNextSibling() {
            // TODO Auto-generated method stub
            return null;
        }
        public NamedNodeMap getAttributes() {
            // TODO Auto-generated method stub
            return null;
        }
        public Document getOwnerDocument() {
            // TODO Auto-generated method stub
            return null;
        }
        public Node insertBefore(Node newChild, Node refChild) throws DOMException {
            // TODO Auto-generated method stub
            return null;
        }
        public Node replaceChild(Node newChild, Node oldChild) throws DOMException {
            // TODO Auto-generated method stub
            return null;
        }
        public Node removeChild(Node oldChild) throws DOMException {
            // TODO Auto-generated method stub
            return null;
        }
        public Node appendChild(Node newChild) throws DOMException {
            // TODO Auto-generated method stub
            return null;
        }
        public boolean hasChildNodes() {
            // TODO Auto-generated method stub
            return false;
        }
        public Node cloneNode(boolean deep) {
            // TODO Auto-generated method stub
            return null;
        }
        public void normalize() {
            // TODO Auto-generated method stub
            
        }
        public boolean isSupported(String feature, String version) {
            // TODO Auto-generated method stub
            return false;
        }
        public String getNamespaceURI() {
            // TODO Auto-generated method stub
            return null;
        }
        public String getPrefix() {
            // TODO Auto-generated method stub
            return null;
        }
        public void setPrefix(String prefix) throws DOMException {
            // TODO Auto-generated method stub
            
        }
        public String getLocalName() {
            // TODO Auto-generated method stub
            return null;
        }
        public boolean hasAttributes() {
            // TODO Auto-generated method stub
            return false;
        }
        public String getBaseURI() {
            // TODO Auto-generated method stub
            return null;
        }
        public short compareDocumentPosition(Node other) throws DOMException {
            // TODO Auto-generated method stub
            return 0;
        }
        public String getTextContent() throws DOMException {
            // TODO Auto-generated method stub
            return null;
        }
        public void setTextContent(String textContent) throws DOMException {
            // TODO Auto-generated method stub
            
        }
        public boolean isSameNode(Node other) {
            // TODO Auto-generated method stub
            return false;
        }
        public String lookupPrefix(String namespaceURI) {
            // TODO Auto-generated method stub
            return null;
        }
        public boolean isDefaultNamespace(String namespaceURI) {
            // TODO Auto-generated method stub
            return false;
        }
        public String lookupNamespaceURI(String prefix) {
            // TODO Auto-generated method stub
            return null;
        }
        public boolean isEqualNode(Node arg) {
            // TODO Auto-generated method stub
            return false;
        }
        public Object getFeature(String feature, String version) {
            // TODO Auto-generated method stub
            return null;
        }
        public Object setUserData(String key, Object data, UserDataHandler handler) {
            // TODO Auto-generated method stub
            return null;
        }
        public Object getUserData(String key) {
            // TODO Auto-generated method stub
            return null;
        }
    }
    public static Node max(NodeIterator nodes, String expr) throws XPathExpressionException {
        return maxNumber(nodes, expr);
    }
    
    public static Node maxNumber(NodeIterator nodes, String expr) throws XPathExpressionException {
        XPathExpression xpath = XPathFactory.newInstance().newXPath().compile(expr);
        Node maxNode = null;
        BigDecimal maxNum = null;
        while(true) {
            com.jclark.xsl.om.Node n = nodes.next();
            if(n == null) break;
            n.
        }
        for(int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);
            String s = toString(n,xpath);
            if(s != null) {
                BigDecimal num = new BigDecimal(s);
                if(maxNum == null) {
                    if(num != null) {
                        maxNode = n;
                        maxNum = num;
                    }
                } else if(num != null && maxNum.compareTo(num) < 0) {
                    maxNode = n;
                    maxNum = num;
                }
            }
        }
        return maxNode;
    }
*/
    public static Node max(NodeList nodes, String expr) throws XPathExpressionException {
        return maxNumber(nodes, expr);
    }
    
    public static Node max(NodeList nodes, String expr, String type) throws XPathExpressionException {
        if("text".equalsIgnoreCase(type.trim())) {
            return maxString(nodes, expr);
        } else if("number".equalsIgnoreCase(type.trim())) {
            return maxNumber(nodes, expr);
        } else {
            return maxString(nodes, expr);
        }
    }
    
    public static Node maxString(NodeList nodes, String expr) throws XPathExpressionException {
        XPathExpression xpath = XPathFactory.newInstance().newXPath().compile(expr);
        Node maxNode = null;
        String maxString = null;
        for(int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);
            String s = toString(n,xpath);
            if(s != null) {
                if(maxString == null) {
                    maxNode = n;
                    maxString = s;
				} else if(maxString.compareTo(s) < 0) {
					maxNode = n;
					maxString = s;
                }
            }
        }
        return maxNode;
    }

    public static Node maxNumber(NodeList nodes, String expr) throws XPathExpressionException {
        XPathExpression xpath = XPathFactory.newInstance().newXPath().compile(expr);
        Node maxNode = null;
        BigDecimal maxNum = null;
        for(int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);
            String s = toString(n,xpath);
            if(s != null) {
                BigDecimal num = new BigDecimal(s);
                if(maxNum == null) {
                    maxNode = n;
                    maxNum = num;
				} else if(maxNum.compareTo(num) < 0) {
					maxNode = n;
					maxNum = num;
                }
            }
        }
        return maxNode;
    }

    protected static String toString(Node n, XPathExpression xpath) throws XPathExpressionException {
        String s = xpath.evaluate(n);
        /*String s;
        if(n instanceof Element) {
            s = ((Element)n).getTextContent();
        } else {
            s = n.getNodeValue();
        }*/
        if(s == null) return null;
        s = s.trim();
        if(s.length() == 0) return null;
        return s;
    }
    
    public static boolean isNull(Object o) {
        return o == null;
    }

	public static boolean isSubclass(String subClassName, String superClassName) throws ClassNotFoundException {
		if(subClassName == null || (subClassName = subClassName.trim()).isEmpty())
			return false;
		if(superClassName == null || (superClassName = superClassName.trim()).isEmpty())
			return true;
		if(subClassName.equals(superClassName))
			return true;
		Class<?> c0 = Class.forName(subClassName);
		Class<?> cc = Class.forName(superClassName);
		return cc.isAssignableFrom(c0);
	}
}
