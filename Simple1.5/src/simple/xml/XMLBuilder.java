/*
 * Created on Sep 30, 2005
 *
 */
package simple.xml;

import java.beans.IntrospectionException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.text.Format;
import java.text.ParseException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.DeflaterOutputStream;

import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.AttributesImpl;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.sql.Expression;
import simple.text.StringUtils;

public class XMLBuilder {
    protected static final Class<?>[] defaultSimpleTypes = new Class[] {
            Number.class,
            Character.class,
            java.util.Date.class,
            Class.class,
            Boolean.class,
            CharSequence.class,
            Format.class,
            Expression.class,
            Reader.class
    	};
    protected static final String WRITE_XML_METHOD_NAME = "writeXML";
    protected static final Class<?>[] WRITE_XML_METHOD_PARAMS = new Class[] {String.class, XMLBuilder.class};
    protected static final Attributes EMPTY_ATTS = new EmptyAttributes();
    protected static final String[] SIMPLE_ATTS = new String[] {"class"};
    protected static final String[] COMPLEX_ATTS = new String[] {"class", "id"};
    protected static final String[] ENUM_ATTS = new String[] {"class", "position"};
    protected static final String[] REF_ATTS = new String[] {"refId"};
    protected static class RecursionDetect {
        protected Map<Object,Long> processed;
        protected AtomicLong lastId;
    }
    protected final Set<Class<?>> simpleInterfaces = new HashSet<Class<?>>();
    protected final Set<Class<?>> simpleClasses = new HashSet<Class<?>>();
    protected final ThreadLocal<RecursionDetect> recursionDetection = new ThreadLocal<RecursionDetect>() {
		@Override
		protected RecursionDetect initialValue() {
			RecursionDetect rd = new RecursionDetect();
            rd.processed = new LinkedHashMap<Object,Long>();
            rd.lastId = new AtomicLong();
			return rd;
		}

    };
    protected ContentHandler contentHandler;
    protected String namespace = null;
    protected int depth = 0;
	protected boolean started = false;
    protected XMLExtraNodes xmlExtraNodes;
	protected Map<String, String> attMap;

    protected static ContentHandler getToXmlStream() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    	// Use of reflection avoids dependency on java1.5 or xalan jars if this constructor is never used
		try {
    		return (ContentHandler)Class.forName("com.sun.org.apache.xml.internal.serializer.ToXMLStream").newInstance();
    	} catch(InstantiationException e) {
    	} catch(IllegalAccessException e) {
		} catch(ClassNotFoundException e) {
		}
    	return (ContentHandler)Class.forName("org.apache.xml.serializer.ToXMLStream").newInstance();
    }

    public static ContentHandler createToXmlStreamHandler(OutputStream xml) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, ParseException {
    	//ToXMLStream txs = new ToXMLStream(); //very convenient to use this but does make us dependent on having xalan jar
        //txs.setOutputStream(xml);
        ContentHandler txs = getToXmlStream();
    	ReflectionUtils.setProperty(txs, "outputStream", xml, false);
        return txs;
    }

    public static ContentHandler createToXmlStreamHandler(Writer xml) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, ParseException {
    	//ToXMLStream txs = new ToXMLStream(); //very convenient to use this but does make us dependent on having xalan jar
        //txs.setOutputStream(xml);
        ContentHandler txs = getToXmlStream();
    	ReflectionUtils.setProperty(txs, "writer", xml, false);
        return txs;
    }
    public static void serializeNode(Node node, OutputStream xml) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, ParseException, SecurityException, IllegalArgumentException, NoSuchMethodException {
    	ReflectionUtils.invokeMethod(createToXmlStreamHandler(xml), "serialize", node);       
    }

    public static void serializeNode(Node node, Writer xml) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, ParseException, SecurityException, IllegalArgumentException, NoSuchMethodException {
    	ReflectionUtils.invokeMethod(createToXmlStreamHandler(xml), "serialize", node); 
    }
    public XMLBuilder(OutputStream xml) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, ParseException {
    	this(createToXmlStreamHandler(xml));
    }
    public XMLBuilder(Writer xml) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, ParseException {
    	this(createToXmlStreamHandler(xml));
    }
    public XMLBuilder(ContentHandler contentHandler) {
        this.contentHandler = contentHandler;
        registerDefaultSimpleTypes();
    }

	public boolean getIndent() throws IntrospectionException, IllegalAccessException, InvocationTargetException, ParseException {
		return ConvertUtils.getBooleanSafely(ReflectionUtils.getProperty(contentHandler, "indent"), false);
	}

	public void setIndent(boolean indent) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
		ReflectionUtils.setProperty(contentHandler, "indent", indent, false);
	}

    protected void registerDefaultSimpleTypes() {
    	for(Class<?> cls : defaultSimpleTypes)
    		registerSimpleType(cls);
	}

	public void write(Object obj) throws SAXException, ConvertException {
        if(obj != null) {
        	contentHandler.startDocument();
        	put(null, obj);
        	contentHandler.endDocument();
        }
    }

    public void put(String name, Object value) throws ConvertException, SAXException {
        RecursionDetect rd = recursionDetection.get();
        long startId = rd.lastId.get();
        try {
        	put(name, value, rd.processed, rd.lastId);
        } finally {
	        if(startId == 0)
	        	rd.lastId.set(startId);
        }
    }

    protected void put(String name, Object value, Map<Object,Long> processed, AtomicLong lastId) throws ConvertException, SAXException {
        if(value == null) return;
        String tagName = (name == null ? value.getClass().getSimpleName() : name);
        writeExtraNodes(value);
        if(isSimpleType(value.getClass())){
            elementStart(tagName, SIMPLE_ATTS, value.getClass().getName());
            elementData(value);
            elementEnd(tagName);
        } else if(Enum.class.isAssignableFrom(value.getClass())) {
            elementStart(tagName, ENUM_ATTS, value.getClass().getName(), String.valueOf(((Enum<?>)value).ordinal() + 1));
            elementData(ConvertUtils.convert(String.class, value));
            elementEnd(tagName);
        } else {
            Long refId = processed.get(value);
            if(refId != null) {
                elementStart(tagName, REF_ATTS, String.valueOf(refId));
                elementEnd(tagName);
            } else {
                //check for writeXML method
                processed.put(value, lastId.incrementAndGet());
                try {
                    if(!invokeWriteXML(name, value, processed, lastId)) {
                    	elementStart(tagName, COMPLEX_ATTS, value.getClass().getName(), lastId.toString());
                        if(ConvertUtils.isIterableType(value.getClass())) {
                        	String listName = StringUtils.singular(tagName);
		                    if(listName.equals(tagName)) listName = listName + "-item";
			                for(Object e : ConvertUtils.asIterable(value))
			                	put(listName, e, processed, lastId);
                        } else if(Map.class.isAssignableFrom(value.getClass())){
                            // not sure what to do here - for now we use <entry><key/><value/></entry>
                            for(Map.Entry<?,?> e : ((Map<?,?>)value).entrySet() ) {
								elementStart("entry", (Map<String, String>) null, null);
                                put("key", e.getKey(), processed, lastId);
                                put("value", e.getValue(), processed, lastId);
                                elementEnd("entry");
                            }
                        } else {
                            try {
                                Set<String> propNames = ReflectionUtils.getPropertyNames(value);
                                if(propNames != null) {
                                    for(String n : propNames) {
                                    	if(!n.equals("class")) {
	                                        if(value instanceof Throwable && n.equals("stackTrace")) {
	                                            put(n, StringUtils.exceptionToString((Throwable)value), processed, lastId);
	                                        } else {
	                                            put(n, ReflectionUtils.getProperty(value, n), processed, lastId);
	                                        }
                                    	}
                                    }
                                }
                            } catch(IntrospectionException e) {
                                throw new ConvertException("Could not read properties of object", String.class, value, e);
                            } catch (IllegalAccessException e) {
                                throw new ConvertException("Could not read properties of object", String.class, value, e);
                            } catch (InvocationTargetException e) {
                                throw new ConvertException("Could not read properties of object", String.class, value, e);
                            } catch(ParseException e) {
                                throw new ConvertException("Could not read properties of object", String.class, value, e);
							}
                        }
                        elementEnd(tagName);
                    }
                } finally {
                    processed.remove(value); // we only care about infinite recursion
                }
            }
        }
    }
    protected void writeExtraNodes(Object object) throws SAXException {
    	if(getXmlExtraNodes() != null) {
    		getXmlExtraNodes().writeExtraNodes(object, contentHandler);
    	}
    }
    protected boolean invokeWriteXML(String name, Object obj, Map<Object, Long> processed, AtomicLong lastId) throws ConvertException {
        try {
            Method method = obj.getClass().getMethod(WRITE_XML_METHOD_NAME, WRITE_XML_METHOD_PARAMS);
            method.invoke(obj, new Object[] {name, this});
            return true;
        } catch (SecurityException e) {
            throw new ConvertException("Could not use " + WRITE_XML_METHOD_NAME + " method on '" + obj + "'", String.class, obj, e);
        } catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
            throw new ConvertException("While invoking " + WRITE_XML_METHOD_NAME + " method on '" + obj + "'", String.class, obj, e);
        } catch (IllegalAccessException e) {
            throw new ConvertException("While invoking " + WRITE_XML_METHOD_NAME + " method on '" + obj + "'", String.class, obj, e);
        } catch (InvocationTargetException e) {
            throw new ConvertException("While invoking " + WRITE_XML_METHOD_NAME + " method on '" + obj + "'", String.class, obj, e);
        }
        return false;
    }

    public void elementData(Object data) throws SAXException, ConvertException {
    	if(data instanceof Reader) {
    		elementData((Reader)data);
    	} else if(data instanceof CharBuffer) {
    		elementData((CharBuffer)data);
    	} else {
    		elementData(ConvertUtils.convert(String.class, data));
    	}
    }
    public void elementData(String data) throws SAXException {
		if(depth == 0 && !started) {
			contentHandler.startDocument();
			started = true;
		}
		char[] ch = data.toCharArray();
        contentHandler.characters(ch, 0, ch.length);
    }

    public void elementData(CharBuffer data) throws SAXException {
		if(depth == 0 && !started) {
			contentHandler.startDocument();
			started = true;
		}
    	if(data.hasArray()) {
    		contentHandler.characters(data.array(), data.arrayOffset(), data.arrayOffset() + data.limit());
    	} else {
    		int origPos = data.position();
    		char[] buffer = new char[Math.min(512, data.limit())];
        	data.position(0);
    		int len = Math.min(buffer.length, data.remaining());
			while(len > 0) {
				data.get(buffer, 0, len);
				contentHandler.characters(buffer, 0, len);
				len = Math.min(buffer.length, data.remaining());
			}
			data.position(origPos);
    	}
    }
    public void elementData(Reader data) throws SAXException {
		if(depth == 0 && !started) {
			contentHandler.startDocument();
			started = true;
		}
    	char[] buffer = new char[512];
    	int r;
    	try {
			while((r=data.read(buffer)) >= 0) {
				 contentHandler.characters(buffer, 0, r);
			}
		} catch(IOException e) {
			throw new SAXException(e);
		}
    }

    protected boolean isSimpleType(Class<?> cls) {
        // this is strange but we don't need to print the string representation of Object (since it is meaningless)
        // also this makes the recursion on superclasses work
        if(cls == null || Object.class.equals(cls)) return false;
        if(simpleClasses.contains(cls)) return true;
        Class<?>[] ifs = cls.getInterfaces();
        if(ifs != null)
        	for(Class<?> i : ifs)
        		if(simpleInterfaces.contains(i))
        			return true;
        return isSimpleType(cls.getSuperclass());
    }

    protected void registerSimpleType(Class<?> cls) {
    	if(cls.isInterface())
    		simpleInterfaces.add(cls);
    	else
    		simpleClasses.add(cls);
    }

	public void attributeForNext(String name, String value) {
		if(attMap == null)
			attMap = new LinkedHashMap<String, String>();
		attMap.put(name, value);
	}

	public void elementStart(String name) throws SAXException {
		elementStart(name, attMap);
	}

	public void elementStart(String name, Set<String> attributeOrder) throws SAXException {
		elementStart(name, attMap, attributeOrder);
	}

	public void element(String name) throws SAXException {
		element(name, null);
	}

	public void element(String name, Set<String> attributeOrder) throws SAXException {
		elementStart(name, attMap, attributeOrder);
		elementEnd(name);
	}

	public void comment(String commentText) throws SAXException {
		if(contentHandler instanceof LexicalHandler && commentText != null) {
			if(depth == 0 && !started) {
				contentHandler.startDocument();
				started = true;
			}
			((LexicalHandler) contentHandler).comment(commentText.toCharArray(), 0, commentText.length());
		}
	}
    public void elementStart(String name, Map<String, String> attributes) throws SAXException {
		elementStart(name, attributes, null);
	}

	protected void elementStart(String name, Map<String, String> attributes, Set<String> attributeOrder) throws SAXException {
        Attributes atts;
		if(attributes == null || attributes.isEmpty()) {
            atts = EMPTY_ATTS;
        } else {
            AttributesImpl attsImpl = new AttributesImpl();
            atts = attsImpl;
			if(attributeOrder != null) {
				int missing = 0;
				for(String key : attributeOrder) {
					String value = attributes.get(key);
					if(value == null) {
						if(attributes.containsKey(key))
							attsImpl.addAttribute(getNamespace(), key, key, "CDATA", "");
						else
							missing++;
					} else
						attsImpl.addAttribute(getNamespace(), key, key, "CDATA", value);
				}
				if(attributes.size() > attributeOrder.size() - missing)
					for(Map.Entry<String, String> e : attributes.entrySet())
						if(!attributeOrder.contains(e.getKey()))
							attsImpl.addAttribute(getNamespace(), e.getKey(), e.getKey(), "CDATA", (e.getValue() == null ? "" : e.getValue()));
			} else
				for(Map.Entry<String, String> e : attributes.entrySet())
					attsImpl.addAttribute(getNamespace(), e.getKey(), e.getKey(), "CDATA", (e.getValue() == null ? "" : e.getValue()));
        }
        startElement(name, atts);
    }

    public void elementStart(String name, String[] attributeNames, String... attributeValues) throws SAXException {
        Attributes atts;
        if(attributeNames == null || attributeNames.length == 0) {
            atts = EMPTY_ATTS;
        } else {
            AttributesImpl attsImpl = new AttributesImpl();
            atts = attsImpl;
            for(int i = 0; i < attributeNames.length; i++) {
                if(attributeValues != null && attributeValues.length > i && attributeValues[i] != null)
                    attsImpl.addAttribute(getNamespace(), attributeNames[i], attributeNames[i], "CDATA", attributeValues[i]);
            }
        }
        startElement(name, atts);
    }

    protected void startElement(String name, Attributes atts) throws SAXException {
        if(depth++ == 0) {
			if(!started) {
				contentHandler.startDocument();
				started = true;
			}
        	if(getNamespace() != null)
        		contentHandler.startPrefixMapping(null, getNamespace());
        }
        contentHandler.startElement(getNamespace(), name, name, atts);
		if(attMap != null)
			attMap.clear();
    }

    public void elementEnd(String name) throws SAXException {
        contentHandler.endElement(getNamespace(), name, name);
        if(--depth == 0) contentHandler.endDocument();
    }

    public static String toXML_old(Object obj) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, SAXException, ParseException {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	XMLBuilder xmlBuilder = new XMLBuilder(out);
    	xmlBuilder.write(obj);
    	return out.toString();
    }

    public static String toXML(Object obj) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, SAXException, ParseException {
    	StringWriter out = new StringWriter();
    	XMLBuilder xmlBuilder = new XMLBuilder(out);
    	xmlBuilder.write(obj);
    	return out.toString();
    }

    public static byte[] toCompressedXML(Object obj) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, SAXException, ParseException, IOException {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	DeflaterOutputStream dos = new DeflaterOutputStream(out);
		XMLBuilder xmlBuilder = new XMLBuilder(dos);
    	xmlBuilder.write(obj);
    	dos.close();
    	return out.toByteArray();
    }

    public static byte[] toCompressedXML(Object obj, Charset charset) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, SAXException, ParseException, IOException {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	DeflaterOutputStream dos = new DeflaterOutputStream(out);
		XMLBuilder xmlBuilder = new XMLBuilder(new OutputStreamWriter(dos, charset));
    	xmlBuilder.write(obj);
    	dos.close();
    	return out.toByteArray();
    }

    public long getCurrentId() {
    	return recursionDetection.get().lastId.longValue();
    }

    public long incrementCurrentId() {
    	return recursionDetection.get().lastId.incrementAndGet();
    }

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public XMLExtraNodes getXmlExtraNodes() {
		return xmlExtraNodes;
	}

	public void setXmlExtraNodes(XMLExtraNodes xmlExtraNodes) {
		this.xmlExtraNodes = xmlExtraNodes;
	}

	public ContentHandler getContentHandler() {
		return contentHandler;
	}
}
