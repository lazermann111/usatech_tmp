package simple.xml;

import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import simple.text.StringUtils;

public class AttributesMap implements Map<String, String> {
	protected final Attributes attributes;
	protected Set<String> keySet;
	protected Collection<String> values;
	protected Set<Map.Entry<String, String>> entrySet;

	protected abstract class BaseIterator<E> implements Iterator<E> {
		protected boolean canRemove = false;
		protected int index = 0;

		protected abstract E getElement(int index);

		@Override
		public boolean hasNext() {
			return index < attributes.getLength();
		}

		@Override
		public E next() {
			E element = getElement(index);
			index++;
			canRemove = true;
			return element;
		}

		@Override
		public void remove() {
			if(!canRemove)
				throw new IllegalStateException();
			removeIndex(--index);
			canRemove = false;
		}
	}

	public AttributesMap(Attributes attributes) {
		super();
		this.attributes = attributes;
	}

	protected String getUri(String prefix) {
		return null;
	}

	protected String getPrefix(String uri) {
		return null;
	}

	protected int getIndex(String key) {
		if(key == null)
			return -1;
		int pos = key.indexOf(':');
		String prefix;
		String localName;
		if(pos < 0) {
			prefix = "";
			localName = key;
		} else {
			prefix = key.substring(0, pos);
			localName = key.substring(pos + 1);
		}
		String uri = getUri(prefix);
		int index = attributes.getIndex(uri == null ? "" : uri, localName);
		if(index < 0)
			index = attributes.getIndex(key);
		return index;
	}

	protected String getKey(int index) {
		String qName = attributes.getQName(index);
		if(qName != null)
			return qName;
		String localName = attributes.getLocalName(index);
		String uri = attributes.getURI(index);
		if(StringUtils.isBlank(uri))
			return localName;
		String prefix = getPrefix(uri);
		if(StringUtils.isBlank(prefix))
			return localName;
		return prefix + ':' + localName;
	}

	protected void removeIndex(int index) {
		if(!(attributes instanceof AttributesImpl))
			throw new UnsupportedOperationException("Not modifiable");
		((AttributesImpl) attributes).removeAttribute(index);
	}

	protected void setValueAtIndex(int index, String value) {
		if(!(attributes instanceof AttributesImpl))
			throw new UnsupportedOperationException("Not modifiable");
		((AttributesImpl) attributes).setValue(index, value);
	}
	
	protected void add(String key, String value) {
		if(!(attributes instanceof AttributesImpl))
			throw new UnsupportedOperationException("Not modifiable");
		if(key == null)
			throw new IllegalArgumentException("Null keys not supported");
		int pos = key.indexOf(':');
		String prefix;
		String localName;
		if(pos < 0) {
			prefix = "";
			localName = key;
		} else {
			prefix = key.substring(0, pos);
			localName = key.substring(pos + 1);
		}
		String uri = getUri(prefix);
		((AttributesImpl) attributes).addAttribute(uri == null ? "" : uri, localName, key, "CDATA", value);
	}

	@Override
	public Set<Map.Entry<String, String>> entrySet() {
		if(entrySet == null)
			entrySet = new AbstractSet<Map.Entry<String, String>>() {
				@Override
				public Iterator<Map.Entry<String, String>> iterator() {
					return new BaseIterator<Map.Entry<String, String>>() {
						@Override
						protected java.util.Map.Entry<String, String> getElement(int index) {
							return new AbstractMap.SimpleEntry<String, String>(getKey(index), attributes.getValue(index)) {
								private static final long serialVersionUID = -5393590252200L;

								@Override
								public String setValue(String value) {
									String old = super.setValue(value);
									int index = getIndex(getKey());
									if(index >= 0)
										setValueAtIndex(index, value);
									return old;
								}
							};
						}
					};
				}

				@Override
				public int size() {
					return AttributesMap.this.size();
				}

				@Override
				public boolean contains(Object o) {
					if(!(o instanceof Map.Entry))
						return false;
					Map.Entry<?, ?> entry = (Map.Entry<?, ?>) o;
					if(!(entry.getValue() instanceof String))
						return false;
					String value = get(entry.getKey());
					return value != null && value.equals(entry.getValue());
				}
			};
		return entrySet;
	}

	@Override
	public int size() {
		return attributes.getLength();
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean containsKey(Object key) {
		return key instanceof String && getIndex((String) key) >= 0;
	}

	@Override
	public boolean containsValue(Object value) {
		return values().contains(value);
	}

	@Override
	public String get(Object key) {
		if(!(key instanceof String))
			return null;
		int index = getIndex((String) key);
		if(index < 0)
			return null;
		return attributes.getValue(index);
	}

	@Override
	public String put(String key, String value) {
		int index = getIndex(key);
		if(index < 0) {
			add(key, value);
			return null;
		}
		String old = attributes.getValue(index);
		setValueAtIndex(index, value);
		return old;
	}

	@Override
	public String remove(Object key) {
		if(!(key instanceof String))
			return null;
		int index = getIndex((String) key);
		if(index < 0)
			return null;
		String old = attributes.getValue(index);
		removeIndex(index);
		return old;
	}

	@Override
	public void putAll(Map<? extends String, ? extends String> m) {
		for(Map.Entry<? extends String, ? extends String> entry : m.entrySet())
			put(entry.getKey(), entry.getValue());
	}

	@Override
	public void clear() {
		if(!(attributes instanceof AttributesImpl))
			throw new UnsupportedOperationException("Not modifiable");
		((AttributesImpl) attributes).clear();
	}

	@Override
	public Set<String> keySet() {
		if(keySet == null)
			keySet = new AbstractSet<String>() {
				@Override
				public Iterator<String> iterator() {
					return new BaseIterator<String>() {
						@Override
						protected String getElement(int index) {
							return getKey(index);
						}
					};
				}

				@Override
				public int size() {
					return AttributesMap.this.size();
				}

				@Override
				public boolean contains(Object o) {
					return containsKey(o);
				}
			};
		return keySet;
	}

	@Override
	public Collection<String> values() {
		if(values == null)
			values = new AbstractCollection<String>() {
				@Override
				public Iterator<String> iterator() {
					return new BaseIterator<String>() {
						@Override
						protected String getElement(int index) {
							return attributes.getValue(index);
						}
					};
				}

				@Override
				public int size() {
					return AttributesMap.this.size();
				}
			};
		return values;
	}

}