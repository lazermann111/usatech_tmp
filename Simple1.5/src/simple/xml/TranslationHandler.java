/*
 * Created on Jan 23, 2006
 *
 */
package simple.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.sax.TransformerHandler;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;

import simple.translator.Translator;

public class TranslationHandler implements ContentHandler, DTDHandler, LexicalHandler {
    protected TransformerHandler transformerHandler;
    protected int translatingDepth = 0;
    protected String namespaceURI = "http://simple/translate/1.0";
    protected String elementName = "translate";
    protected Translator translator;
    protected String hold;
    protected List<String> defStack = new ArrayList<String>();
    
    public TranslationHandler(TransformerHandler transformerHandler, Translator translator) {
        super();
        this.transformerHandler = transformerHandler;
        this.translator = translator;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if(translatingDepth > 0) {
            translate(ch, start, length);
        } else {
            transformerHandler.characters(ch, start, length);
        }
    }

    public void comment(char[] ch, int start, int length) throws SAXException {
        transformerHandler.comment(ch, start, length);
    }

    public void endCDATA() throws SAXException {
        transformerHandler.endCDATA();
    }

    public void endDocument() throws SAXException {
        transformerHandler.endDocument();
    }

    public void endDTD() throws SAXException {
        transformerHandler.endDTD();
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(translatingDepth > 0) {
            translatingDepth--;
            if(translatingDepth == 0) {
            	finishTranslation();
            	defStack.remove(defStack.size()-1);
            }
        }
        transformerHandler.endElement(uri, localName, qName);
    }

    public void endEntity(String name) throws SAXException {
        transformerHandler.endEntity(name);
    }

    public void endPrefixMapping(String prefix) throws SAXException {
        transformerHandler.endPrefixMapping(prefix);
    }

    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        transformerHandler.ignorableWhitespace(ch, start, length);
    }

    public void notationDecl(String name, String publicId, String systemId) throws SAXException {
        transformerHandler.notationDecl(name, publicId, systemId);
    }

    public void processingInstruction(String target, String data) throws SAXException {
        transformerHandler.processingInstruction(target, data);
    }

    public void setDocumentLocator(Locator locator) {
        transformerHandler.setDocumentLocator(locator);
    }

    public void skippedEntity(String name) throws SAXException {
        transformerHandler.skippedEntity(name);
    }

    public void startCDATA() throws SAXException {
        transformerHandler.startCDATA();
    }

    public void startDocument() throws SAXException {
        transformerHandler.startDocument();
    }

    public void startDTD(String name, String publicId, String systemId) throws SAXException {
        transformerHandler.startDTD(name, publicId, systemId);
    }

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        if(translatingDepth > 0) 
            translatingDepth++;
        else if(isTranslateElement(namespaceURI, localName)) {
        	defStack.add(atts.getValue("default"));
            translatingDepth = 1;
        }
        transformerHandler.startElement(namespaceURI, localName, qName, atts);
    }
   
    public void startEntity(String name) throws SAXException {
        transformerHandler.startEntity(name);
    }

    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        transformerHandler.startPrefixMapping(prefix, uri);
    }

    public void unparsedEntityDecl(String name, String publicId, String systemId, String notationName) throws SAXException {
        transformerHandler.unparsedEntityDecl(name, publicId, systemId, notationName);
    }
    
    protected boolean isTranslateElement(String namespaceURI, String localName) {
        return namespaceURI.equals(getNamespaceURI()) && localName.equals(getElementName());
    }

    protected void translate(char[] ch, int start, int length) {
        String s = new String(ch, start, length);
        if(hold == null) hold = s;
        else hold = hold + s;
    }

    protected void finishTranslation() throws SAXException {
        if(hold.length() > 0) {
        	String def = defStack.get(defStack.size()-1);
            String x = translator.translate(hold, def == null ? hold : def);
            characters(x.toCharArray(), 0, x.length()) ;
            hold = null;
        }
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getNamespaceURI() {
        return namespaceURI;
    }

    public void setNamespaceURI(String namespaceURI) {
        this.namespaceURI = namespaceURI;
    }
}
