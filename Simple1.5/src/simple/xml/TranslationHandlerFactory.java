/*
 * Created on Jan 23, 2006
 *
 */
package simple.xml;

import java.util.Locale;

import javax.xml.transform.Result;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.TransformerHandler;

import simple.app.ServiceException;
import simple.translator.TranslatorFactory;

public class TranslationHandlerFactory {
    protected static TranslationHandlerFactory defaultFactory = new TranslationHandlerFactory();
    protected TranslationHandlerFactory() {
        super();
    }

	public TranslationHandler newTranslationHandler(TransformerHandler transformerHandler, Locale locale, String context) throws ServiceException {
        return new TranslationHandler(transformerHandler, TranslatorFactory.getDefaultFactory().getTranslator(locale, context));
    }
    
    public Result newTranslatedResult(Result result, Locale locale, String context) throws TransformerConfigurationException {
        TransformerHandler handler = TemplatesLoader.getInstance(null).newTransformerHandler();
        handler.setResult(result);
		try {
			return new SAXResult(newTranslationHandler(handler, locale, context));
		} catch(ServiceException e) {
			throw new TransformerConfigurationException(e);
		}
    }

    public static TranslationHandlerFactory getDefaultFactory() {
        return defaultFactory;
    }

    public static void setDefaultFactory(TranslationHandlerFactory defaultFactory) {
        TranslationHandlerFactory.defaultFactory = defaultFactory;
    }

}
