/*
 * Created on Nov 3, 2005
 *
 */
package simple.xml;

import java.io.Writer;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;

public interface Serializer extends ExtendedContentHandler, LexicalHandler {
    public Writer getWriter() ;
    public void setWriter(Writer writer) ;

	public Writer addAttributeByStream(String namespaceURI, String localName, String qname) throws SAXException;
	public void addAttribute(String namespaceURI, String localName, String qname, String value) throws SAXException;

	public void putElement(String localName) throws SAXException;

	public void putElement(String localName, String content) throws SAXException;

	public void putElement(String localName, String content, String[] attributeNames, String... attributeValues) throws SAXException;

	public void putElement(String localName, char[] content, String[] attributeNames, String... attributeValues) throws SAXException;

	public void putElement(String localName, String content, Map<String, String> attributes) throws SAXException;

	public void putElement(String localName, Attributes attributes, String content) throws SAXException;

	public void putElement(String localName, Attributes attributes) throws SAXException;
}
