/*
 * ListOfStringList.java
 *
 * Created on April 23, 2004, 11:48 AM
 */

package simple.xml;

import com.sun.org.apache.xerces.internal.xs.StringList;

/** Wraps org.apache.xerces.xs.StringList in a java.util.List
 *
 * @author  Brian S. Krug
 */
public class ListOfStringList extends java.util.AbstractList<String> {
    protected StringList stringList;
    /** Creates a new instance of ListOfStringList */
    public ListOfStringList(StringList stringList) {
        this.stringList = stringList;
    }
        
    public String get(int index) {
        return stringList.item(index);
    }
    
    public boolean contains(Object o) {
        if(o instanceof String) return stringList.contains((String)o);
        return false;
    }
            
    public int size() {
        return stringList.getLength();
    }
       
}
