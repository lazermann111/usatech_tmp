/*
 * Created on Jan 19, 2005
 *
 */
package simple.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.translator.Translator;
import simple.util.MapBackedSet;
import simple.util.concurrent.Cache;
import simple.util.concurrent.LockSegmentCache;
import simple.xml.sax.ExtensionsXMLFilter;
import simple.xml.sax.TranslationXMLFilter;

/**
 * @author bkrug
 *
 */
public class TemplatesLoader implements URIResolver {
    private static final Log log = Log.getLog();
    protected static final ErrorListener logErrorListener = new ErrorListener() {
        public void warning(TransformerException exception) throws TransformerException {
            log.info("During Transformation (WARNING) at " + getXSLLocation(exception), exception);
        }
        public void error(TransformerException exception) throws TransformerException {
            log.warn("During Transformation (ERROR) at " + getXSLLocation(exception), exception);
        }
        public void fatalError(TransformerException exception) throws TransformerException {
            log.error("During Transformation (FATAL) at " + getXSLLocation(exception), exception);
        }
    };
    protected static final Cache<Translator,TemplatesLoader,RuntimeException> instances = new LockSegmentCache<Translator,TemplatesLoader,RuntimeException>(100, 0.75f, 16) {
		@Override
		protected TemplatesLoader createValue(Translator key, Object... additionalInfo) {
			return new TemplatesLoader(key);
		}
		@Override
		protected boolean keyEquals(Translator key1, Translator key2) {
			return key1 == key2;//We can use this because Translators are non-duplicable
//			return ConvertUtils.areEqual(key1, key2);
		}
		@Override
		protected boolean valueEquals(TemplatesLoader value1, TemplatesLoader value2) {
			return value1 == value2; // not important
		}
    };
    protected static final Map<String,Map<String,Object>> factoryClassAttributes = new HashMap<String, Map<String,Object>>();
    protected static final Cache<Object, BundledConfigSource, IOException> configSourceMap = new LockSegmentCache<Object, BundledConfigSource, IOException>(100, 0.75f, 16) {
		@Override
		protected BundledConfigSource createValue(Object key, Object... additionalInfo) throws IOException {
			BundledConfigSource src;
			if(key instanceof URL)
				src = new BundledConfigSource(ConfigSource.createConfigSource((URL)key));
			else if(key instanceof String)
				src = new BundledConfigSource(ConfigSource.createConfigSource((String)key));
			else if(key instanceof File)
				src = new BundledConfigSource(ConfigSource.createConfigSource((File)key));
			else
				throw new ClassCastException("Source is " + key.getClass().getName() + " but may only be URL, File, or String");
			long minInterval = -1;
			if(additionalInfo != null && additionalInfo.length > 0)
				try {
					minInterval = ConvertUtils.getLong(additionalInfo[0], -1);
				} catch(ConvertException e) {
					log.info("Could not convert minInterval", e);
				}
            src.registerLoader(loader, minInterval, 0);
			return src;
		}
		@Override
		protected boolean keyEquals(Object key1, Object key2) {
			return key1.equals(key2);//Should never be null
		}
		@Override
		protected boolean valueEquals(BundledConfigSource value1, BundledConfigSource value2) {
			return value1 == value2; // not important
		}
    };
    protected static final Loader loader = new Loader() {
        public void load(ConfigSource src) throws LoadingException {
            //simply blank out the old so that new is reloaded on getTemplates()
            for(TemplatesLoader tl : instances.values())
                tl.templatesMap.remove(src);
        }
    };

    protected Templates identityTemplates;
    protected TransformerFactory factory;
    protected Translator translator;
    protected final Map<BundledConfigSource, Templates> templatesMap = new HashMap<BundledConfigSource, Templates>();


    public static String getXSLLocation(Throwable e) {
        if(e instanceof TransformerException) {
            TransformerException te = (TransformerException)e;
            if(te.getLocator() != null)
                return te.getLocationAsString();
        } else if(e instanceof SAXException) {
            Throwable cause = ((SAXException)e).getException();
            if(cause != null)
                return getXSLLocation(cause);
        }
        Throwable cause = e.getCause();
        if(cause != null)
            return getXSLLocation(cause);
        return "unknown";
    }

    public static TemplatesLoader getInstance(Translator translator) {
			return instances.getOrCreate(translator);
    }

    protected TemplatesLoader(Translator translator) {
        this.translator = translator;
        setFactory(TransformerFactory.newInstance());
    }

    protected BundledConfigSource getConfigSource(URL url, Long refreshInterval) throws IOException {
		return configSourceMap.getOrCreate(url, refreshInterval);
    }
    protected BundledConfigSource getConfigSource(File file, Long refreshInterval) throws IOException {
		return configSourceMap.getOrCreate(file, refreshInterval);
    }
    protected BundledConfigSource getConfigSource(String path, Long refreshInterval) throws IOException {
		return configSourceMap.getOrCreate(path, refreshInterval);
    }

    protected void prepareTransformer(Transformer t) {
        t.setErrorListener(getErrorListener());
        // t.setURIResolver(this);
    }

    public Transformer newTransformer() throws TransformerConfigurationException {
    	Transformer t;
        try {
        	t = factory.newTransformer();
        } catch(TransformerConfigurationException e) {
        	if(identityTemplates == null) {
        		identityTemplates = factory.newTemplates(new StreamSource(new StringReader(
        				"<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\"><xsl:output method=\"xml\"/><xsl:template match=\"/\"><xsl:copy-of select=\".\"/></xsl:template></xsl:stylesheet>"
        		)));
        	}
        	t = identityTemplates.newTransformer();
        }
        prepareTransformer(t);
        return t;
    }

    public Transformer newTransformer(URL xslURL) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformer(getConfigSource(xslURL, null));
    }

    public Transformer newTransformer(String xslPath) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformer(getConfigSource(xslPath, null));
    }

    public Transformer newTransformer(File xslFile) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformer(getConfigSource(xslFile, null));
    }

    public Transformer newTransformer(URL xslURL, long refreshInterval) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformer(getConfigSource(xslURL, refreshInterval));
    }

    public Transformer newTransformer(String xslPath, long refreshInterval) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformer(getConfigSource(xslPath, refreshInterval));
    }

    public Transformer newTransformer(File xslFile, long refreshInterval) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformer(getConfigSource(xslFile, refreshInterval));
    }

    protected Transformer newTransformer(BundledConfigSource src) throws TransformerConfigurationException, IOException, LoadingException {
        Transformer t = getTemplates(src).newTransformer();
        prepareTransformer(t);
        return t;
    }

    public Templates getTemplates(URL xslURL) throws TransformerConfigurationException, IOException, LoadingException {
        return getTemplates(getConfigSource(xslURL, null));
    }

    public Templates getTemplates(String xslPath) throws TransformerConfigurationException, IOException, LoadingException {
        return getTemplates(getConfigSource(xslPath, null));
    }

    public Templates getTemplates(File xslFile) throws TransformerConfigurationException, IOException, LoadingException {
        return getTemplates(getConfigSource(xslFile, null));
    }

    public Templates getTemplates(URL xslURL, long refreshInterval) throws TransformerConfigurationException, IOException, LoadingException {
        return getTemplates(getConfigSource(xslURL, refreshInterval));
    }

    public Templates getTemplates(String xslPath, long refreshInterval) throws TransformerConfigurationException, IOException, LoadingException {
        return getTemplates(getConfigSource(xslPath, refreshInterval));
    }

    public Templates getTemplates(File xslFile, long refreshInterval) throws TransformerConfigurationException, IOException, LoadingException {
        return getTemplates(getConfigSource(xslFile, refreshInterval));
    }

    protected Templates getTemplates(BundledConfigSource src) throws TransformerConfigurationException, IOException, LoadingException {
        src.reload();
        //reload any dependents
        Templates tmpl = templatesMap.get(src);
        if(tmpl == null) {
            src.lock();
            try {
                tmpl = templatesMap.get(src);
                if(tmpl == null) {
                    log.debug("Creating new Templates for " + src);
                    src.dependents.clear();
                    String uri = src.getUri();
                    InputStream in = src.getInputStream();
                    try {
                    	tmpl = factory.newTemplates(createSource(uri, in));
                    } finally {
                    	try { in.close(); } catch(IOException e) { log.info("Could not close main InputStream for '" + uri + "'"); }// this cleans up the input stream
                    	if(src.localDependentIS.initialized)
                    		for(InputStream in1 : src.localDependentIS.get())
                    			try { in1.close(); } catch(IOException e) { log.info("Could not close child InputStream for '" + uri + "'"); }
                    }
                    templatesMap.put(src, tmpl);
                }
            } catch(SAXException e) {
                throw new TransformerConfigurationException("Could not create XMLReader", e);
            } finally {
                src.unlock();
            }
        }
        return tmpl;
    }

    protected Source createSource(String uri, InputStream in) throws SAXException {
        if(translator == null) {
            //return new StreamSource(src.getInputStream(), src.toString());
            InputSource inputSource = new InputSource(in);
            inputSource.setSystemId(uri);
            XMLFilter filter = new ExtensionsXMLFilter(XMLReaderFactory.createXMLReader(), factory);
            return new SAXSource(filter, inputSource);
        } else {
            InputSource inputSource = new InputSource(in);
            inputSource.setSystemId(uri);
            XMLFilter filter = new TranslationXMLFilter(XMLReaderFactory.createXMLReader(), factory, translator);
            return new SAXSource(filter, inputSource);
        }
    }

    /**
     * @see javax.xml.transform.URIResolver#resolve(java.lang.String, java.lang.String)
     */
    public Source resolve(String href, String base) throws TransformerException {
        log.debug("Resolving HREF='" + href + "' with BASE='" + base + "'");
        try {
            URL baseURL = new URL(base);
            BundledConfigSource childSrc = getConfigSource(new URL(baseURL, href), -1L); // never reload dependents as they will be reloaded when base is reloaded
            BundledConfigSource baseSrc = configSourceMap.getIfPresent(baseURL);
            if(baseSrc == null) baseSrc = configSourceMap.getIfPresent(base);
            String uri = childSrc.getUri();
            InputStream in = childSrc.getInputStream();
            if(baseSrc != null) {
                baseSrc.dependents.add(childSrc);
                baseSrc.localDependentIS.get().add(in);
            }
            return createSource(uri,in);
        } catch (IOException e) {
            throw new TransformerException(e);
        } catch(SAXException e) {
            throw new TransformerException(e);
        }
    }
    protected static class LocalDependentIS extends ThreadLocal<Set<InputStream>> {
    	protected boolean initialized = false;
    	@Override
		protected Set<InputStream> initialValue() {
    		Set<InputStream> set = new MapBackedSet<InputStream>(new WeakHashMap<InputStream, Object>());
    		initialized = true;
    		return set;
    	}
    }
    /**
     * Implements ConfigSource with a bundle
     */
    protected static class BundledConfigSource extends ConfigSource {
        protected ConfigSource mainSource;
        protected Set<ConfigSource> dependents = new HashSet<ConfigSource>();
        protected final LocalDependentIS localDependentIS = new LocalDependentIS();

        public BundledConfigSource(ConfigSource mainSource) {
            super(mainSource.getUri(), false);
            this.mainSource = mainSource;
        }
        @Override
		public java.io.InputStream getInputStream() throws IOException {
            return mainSource.getInputStream();
        }
        @Override
		public long lastModified() {
            long l = mainSource.lastModified();
            for(Iterator<ConfigSource> iter = dependents.iterator(); iter.hasNext(); ) {
                ConfigSource cs = iter.next();
                l = Math.max(l, cs.lastModified());
            }
            return l;
        }
        @Override
		public String toString() {
            return mainSource.toString();
        }

		public void lock() {
			lock.lock();
		}
		public void unlock() {
			lock.unlock();
		}

		@Override
		protected OutputStream getRawOutputStream() throws IOException {
			return null;
		}

		@Override
		public OutputStream getOutputStream() throws IOException {
			return mainSource.getOutputStream();
		}
    }

    /**
     * @see javax.xml.transform.TransformerFactory#getAttribute(java.lang.String)
     */
    public Object getAttribute(String name) {
        return factory.getAttribute(name);
    }

    /**
     * @see javax.xml.transform.TransformerFactory#getErrorListener()
     */
    public ErrorListener getErrorListener() {
        return factory.getErrorListener();
    }

    /**
     * @see javax.xml.transform.TransformerFactory#getFeature(java.lang.String)
     */
    public boolean getFeature(String name) {
        return factory.getFeature(name);
    }

    /**
     * @see javax.xml.transform.TransformerFactory#setAttribute(java.lang.String, java.lang.Object)
     */
    public void setAttribute(String name, Object value) {
        factory.setAttribute(name, value);
    }

    /**
     * @see javax.xml.transform.TransformerFactory#setErrorListener(javax.xml.transform.ErrorListener)
     */
    public void setErrorListener(ErrorListener listener) {
        factory.setErrorListener(listener);
    }

    /**
     * @see javax.xml.transform.TransformerFactory#setFeature(java.lang.String, boolean)
     */
    public void setFeature(String name, boolean value) throws TransformerConfigurationException {
        factory.setFeature(name, value);
    }

    public XMLFilter chainTransformation(URL xslURL, XMLReader original) throws TransformerConfigurationException, IOException, LoadingException {
        XMLFilter filter = ((SAXTransformerFactory)factory).newXMLFilter(getTemplates(xslURL));
        filter.setParent(original);
        return filter;
    }

    public void chainTransformation(URL xslURL, SAXSource saxSource) throws TransformerConfigurationException, IOException, LoadingException {
        XMLFilter filter = chainTransformation(xslURL, saxSource.getXMLReader());
        saxSource.setXMLReader(filter);
    }

    public XMLFilter chainTransformation(File xslFile, XMLReader original) throws TransformerConfigurationException, IOException, LoadingException {
        XMLFilter filter = ((SAXTransformerFactory)factory).newXMLFilter(getTemplates(xslFile));
        filter.setParent(original);
        return filter;
    }

    public void chainTransformation(File xslFile, SAXSource saxSource) throws TransformerConfigurationException, IOException, LoadingException {
        XMLFilter filter = chainTransformation(xslFile, saxSource.getXMLReader());
        saxSource.setXMLReader(filter);
    }

    public XMLFilter chainTransformation(String xslPath, XMLReader original) throws TransformerConfigurationException, IOException, LoadingException {
        XMLFilter filter = ((SAXTransformerFactory)factory).newXMLFilter(getTemplates(xslPath));
        if(original != null) filter.setParent(original);
        return filter;
    }

    public void chainTransformation(String xslPath, SAXSource saxSource) throws TransformerConfigurationException, IOException, LoadingException {
        XMLFilter filter = chainTransformation(xslPath, saxSource.getXMLReader());
        saxSource.setXMLReader(filter);
    }

    public TransformerHandler newTransformerHandler(URL xslURL) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformerHandler(getConfigSource(xslURL, null));
    }

    public TransformerHandler newTransformerHandler(String xslPath) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformerHandler(getConfigSource(xslPath, null));
    }

    public TransformerHandler newTransformerHandler(File xslFile) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformerHandler(getConfigSource(xslFile, null));
    }

    public TransformerHandler newTransformerHandler(URL xslURL, long refreshInterval) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformerHandler(getConfigSource(xslURL, refreshInterval));
    }

    public TransformerHandler newTransformerHandler(String xslPath, long refreshInterval) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformerHandler(getConfigSource(xslPath, refreshInterval));
    }

    public TransformerHandler newTransformerHandler(File xslFile, long refreshInterval) throws TransformerConfigurationException, IOException, LoadingException {
        return newTransformerHandler(getConfigSource(xslFile, refreshInterval));
    }

    protected TransformerHandler newTransformerHandler(BundledConfigSource src) throws TransformerConfigurationException, IOException, LoadingException {
        return ((SAXTransformerFactory)factory).newTransformerHandler(getTemplates(src));
    }

    public TransformerHandler newTransformerHandler() throws TransformerConfigurationException {
        return ((SAXTransformerFactory)factory).newTransformerHandler();
    }

    public Class<? extends TransformerFactory> getFactoryClass() {
        return factory.getClass();
    }

    public static Object setFactoryClassAttribute(String factoryClassName, String name, Object value) {
        Map<String,Object> atts = factoryClassAttributes.get(factoryClassName);
        if(atts == null) {
            atts = new HashMap<String, Object>();
            factoryClassAttributes.put(factoryClassName, atts);
        }
        return atts.put(name, value);
    }

    public TransformerFactory getFactory() {
        return factory;
    }

    public void setFactory(TransformerFactory factory) {
        this.factory = factory;
        factory.setURIResolver(this);
        factory.setErrorListener(logErrorListener);
        Map<String,Object> atts = factoryClassAttributes.get(factory.getClass().getName());
        if(atts != null) {
            for(Map.Entry<String,Object> a : atts.entrySet()) {
                factory.setAttribute(a.getKey(), a.getValue());
            }
        }
    }
}
