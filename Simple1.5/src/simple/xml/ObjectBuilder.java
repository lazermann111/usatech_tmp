/*
 * Created on Sep 30, 2005
 *
 */
package simple.xml;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.bean.ReflectionUtils.RealMethodComparable;
import simple.io.Log;
import simple.util.ExcludingSet;
import simple.xml.MapXMLLoader.ParentMap;

public class ObjectBuilder {
    private static final Log log = Log.getLog();
    protected static final String READ_XML_METHOD_NAME = "readXML";
    protected static final Class<?>[] READ_XML_METHOD_PARAMS = new Class[] {ObjectBuilder.class};
    protected ParentMap currentNode;
    protected int position;
    protected XMLExtraNodes xmlExtraNodes;
    protected final ObjectBuilder parentBuilder;
	protected final Set<String> excludedNames;
	protected Set<String> attributeNames;

    public ObjectBuilder(InputStream xml) throws IOException, SAXException, ParserConfigurationException {
        this(xml, null);
    }
    public ObjectBuilder(InputStream xml, XMLExtraNodes xmlExtraNodes) throws IOException, SAXException, ParserConfigurationException {
    	this(new InputSource(xml), xmlExtraNodes);
    }
    public ObjectBuilder(Reader xml) throws IOException, SAXException, ParserConfigurationException {
        this(xml, null);
    }
    public ObjectBuilder(Reader xml, XMLExtraNodes xmlExtraNodes) throws IOException, SAXException, ParserConfigurationException {
    	this(new InputSource(xml), xmlExtraNodes);
    }
    public ObjectBuilder(InputSource xml, XMLExtraNodes xmlExtraNodes) throws IOException, SAXException, ParserConfigurationException {
       this.xmlExtraNodes = xmlExtraNodes;
       MapXMLLoader xmlLoader = new MapXMLLoader();
       xmlLoader.setXmlExtraNodes(xmlExtraNodes);
       this.currentNode = xmlLoader.load(xml);
       this.position = 1;
       this.parentBuilder = null;
		this.excludedNames = new HashSet<String>();
		this.excludedNames.add(xmlLoader.getTagKey());
		this.excludedNames.add(xmlLoader.getTextKey());
    }

    protected ObjectBuilder(ParentMap currentNode, XMLExtraNodes xmlExtraNodes, int position, ObjectBuilder parentBuilder) {
        this.currentNode = currentNode;
    	this.xmlExtraNodes = xmlExtraNodes;
        this.position = position;
        this.parentBuilder = parentBuilder;
		this.excludedNames = parentBuilder.excludedNames;
    }
    //TODO: Write a method that reads successfully what XMLBuilder.put() writes

    public <T> T read(Class<T> type, Object... arguments) throws ConvertException {
    	T obj = doRead(type, arguments);
    	updateXmlExtraNodes(obj);
    	return obj;
    }
    public void updateXmlExtraNodes(Object obj) {
    	if(getXmlExtraNodes() != null && obj != null)
    		getXmlExtraNodes().replaceAssociation(getCurrentNode(), obj);
	}
    protected <T> T doRead(Class<T> type, Object... arguments) throws ConvertException {
        try {
            Class<?>[] paramTypes;
            Object[] paramValues;
            if(arguments == null || arguments.length == 0) {
                paramTypes = READ_XML_METHOD_PARAMS;
                paramValues = new Object[] {this};
            } else {
                paramTypes = new Class[READ_XML_METHOD_PARAMS.length + arguments.length];
                paramValues = new Object[READ_XML_METHOD_PARAMS.length + arguments.length];
                System.arraycopy(READ_XML_METHOD_PARAMS, 0, paramTypes, 0, READ_XML_METHOD_PARAMS.length);
                paramValues[0] = this;
                for(int i = 0; i < arguments.length; i++) {
                    paramTypes[i + READ_XML_METHOD_PARAMS.length] = (arguments[i] == null ? null : arguments[i].getClass());
                    paramValues[i + READ_XML_METHOD_PARAMS.length] = arguments[i];
                }
            }
            ReflectionUtils.MethodComparable mc = ReflectionUtils.findMethodComparable(type, READ_XML_METHOD_NAME, paramTypes);
            if(mc instanceof RealMethodComparable) {
                Method method = ((RealMethodComparable)mc).getMethod(); //type.getMethod(READ_XML_METHOD_NAME, paramTypes);
                if(Modifier.isStatic(method.getModifiers())) {
                    return type.cast(method.invoke(null, paramValues));
                } else {
                    T obj = type.newInstance();
                    method.invoke(obj, paramValues);
                    return obj;
                }
            }
        } catch (SecurityException e) {
            throw new ConvertException("Could not use " + READ_XML_METHOD_NAME + " method on '" + type.getName() + "'", type, currentNode, e);
        //} catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
            throw new ConvertException("While invoking " + READ_XML_METHOD_NAME + " method on '" + type.getName() + "'", type, currentNode, e);
        } catch (IllegalAccessException e) {
            throw new ConvertException("While invoking " + READ_XML_METHOD_NAME + " method on '" + type.getName() + "'", type, currentNode, e);
        } catch (InvocationTargetException e) {
            throw new ConvertException("While invoking " + READ_XML_METHOD_NAME + " method on '" + type.getName() + "'", type, currentNode, e);
        } catch (InstantiationException e) {
            throw new ConvertException("While creating a new '" + type.getName() + "'", type, currentNode, e);
        }
        try {
            T obj = type.newInstance();
            //use bean-property default
            readTo(obj);
            return obj;
        } catch (InstantiationException e) {
            throw new ConvertException("While creating a new '" + type.getName() + "'", type, currentNode, e);
        } catch (IllegalAccessException e) {
            throw new ConvertException("While creating a new '" + type.getName() + "'", type, currentNode, e);
        } catch (IntrospectionException e) {
            throw new ConvertException("While setting properties on '" + type.getName() + "'", type, currentNode, e);
        } catch (InvocationTargetException e) {
            throw new ConvertException("While setting properties on '" + type.getName() + "'", type, currentNode, e);
        } catch(ParseException e) {
            throw new ConvertException("While setting properties on '" + type.getName() + "'", type, currentNode, e);
		}
    }

    public void readTo(Object obj) throws IntrospectionException, IllegalAccessException, InvocationTargetException, InstantiationException, ConvertException, ParseException {
        Set<String> propNames = ReflectionUtils.getPropertyNames(obj);
        if(propNames != null) {
            for(String name : propNames) {
                Object value = getValue(name);
                if(value != null){
                	if(value instanceof String[]&&((String[])value).length==1){
                		String strValue=((String[])value)[0];
                		if(strValue!=null){
                			ReflectionUtils.setProperty(obj, name, strValue, true);
                		}
                	}else{
                		ReflectionUtils.setProperty(obj, name, value, true);
                	}
                }
            }
        }
    }

    public <T> T get(String name, Class<T> type) {
        try {
            return ConvertUtils.convert(type, getValue(name));
        } catch (ConvertException e) {
            log.debug("While converting value at '" + name + "' to " + type.getName(), e);
        }
        return null;
    }

    public <T> T getAttr(String name, Class<T> type) {
        try {
            return ConvertUtils.convert(type, getAttrValue(name));
        } catch (ConvertException e) {
            log.debug("While converting value at '" + name + "' to " + type.getName(), e);
        }
        return null;
    }

    public <T> T getNode(String name, Class<T> type) {
        try {
            return ConvertUtils.convert(type, getNodeValues(name));
        } catch (ConvertException e) {
            log.debug("While converting value at '" + name + "' to " + type.getName(), e);
        }
        return null;
    }

    public Object getValue(String name) {
        //try map & then simple children (i.e. - attributes or sub elements)
        String value = getAttrValue(name);
        if(value != null) return value;
        else return getNodeValues(name);
    }

    public String getAttrValue(String name) {
        return getCurrentNode().get(name);
    }

    public String[] getNodeValues(String name) {
        ParentMap[] maps = getCurrentNode().getChildren(name);
        if(maps == null || maps.length == 0) return null;
        String[] values = new String[maps.length];
        for(int i = 0; i < maps.length; i++) {
            values[i] = maps[i].getText();
        }
        return values;
    }

	public String getConcatenatedNodeValues(String name, String separator) {
		ParentMap[] maps = getCurrentNode().getChildren(name);
		if(maps == null)
			return null;
		switch(maps.length) {
			case 0:
				return null;
			case 1:
				return maps[0].getText();
			default:
				StringBuilder sb = new StringBuilder();
				sb.append(maps[0].getText());
				for(int i = 1; i < maps.length; i++) {
					if(separator != null)
						sb.append(separator);
					sb.append(maps[i].getText());
				}
				return sb.toString();
		}
	}

    public String getTag() {
        return getCurrentNode().getTag();
    }
    public String getText() {
        return getCurrentNode().getText();
    }

    protected ParentMap getCurrentNode() {
        return currentNode;
    }

    public ObjectBuilder getParentNode() {
    	return parentBuilder;
    }
    public ObjectBuilder[] getSubNodes() {
        return makeSubNodes(getCurrentNode().getChildren());
    }

    private ObjectBuilder[] makeSubNodes(ParentMap[] children) {
        if(children == null) return null;
        ObjectBuilder[] obs = new ObjectBuilder[children.length];
        for(int i = 0; i < children.length; i++)
            obs[i] = new ObjectBuilder(children[i], getXmlExtraNodes(), i+1, this);
        return obs;
    }

    public ObjectBuilder[] getSubNodes(String name) {
        return makeSubNodes(getCurrentNode().getChildren(name));
    }

    /**
     * @return Returns the position.
     */
    public int getPosition() {
        return position;
    }

    @Override
	public String toString() {
        return getCurrentNode().toString(); //TODO: for now - this is easy
    }

	public XMLExtraNodes getXmlExtraNodes() {
		return xmlExtraNodes;
	}

	public void setXmlExtraNodes(XMLExtraNodes xmlExtraNodes) {
		this.xmlExtraNodes = xmlExtraNodes;
	}

	public DocumentLocation getCurrentLocation() {
		return getCurrentNode().getLocation();
	}

	public Set<String> getAttributeNames() {
		if(attributeNames == null)
			attributeNames = new ExcludingSet<String>(currentNode.keySet(), excludedNames);
		return attributeNames;
	}
}
