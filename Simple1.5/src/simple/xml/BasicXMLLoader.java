/*
 * MapXMLLoader.java
 *
 * Created on October 17, 2003, 10:16 AM
 */

package simple.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/** Loads XML into a heirarchy of <CODE>Entity</CODE> objects. An xml element's
 * attributes (its name and value) are loaded as property values.
 * Sub-elements are accessed using the <CODE>getChildren()</CODE> method of
 * each <CODE>Entity</CODE> object. Content broken by sub-elements is concatenated together.
 * The object returned from the load methods is
 * a <CODE>Entity</CODE> object that represents the root XML element.
 * @author Brian S. Krug
 */
public class BasicXMLLoader extends XMLLoader<BasicXMLLoader.Entity> {
     public BasicXMLLoader() {
    	 super();
     }

    protected Entity createObject(String tag, DocumentLocation location) throws XMLLoadingException {
    	Entity m = new Entity();
        m.setTag(tag);
        return m;
    }

    protected void applyAttribute(Entity o, String name, String value) throws XMLLoadingException {
        o.setProperty(name, value);
    }

    protected void addTo(Entity parent, Entity o, String tag) throws XMLLoadingException {
        parent.addChild(o);
    }

    protected void addText(Entity m, String text) throws XMLLoadingException {
    	String value = m.getContent();
        if(value == null) value = text;
        else value = value + text;
        m.setContent(value);
    }

    protected void finishObject(Entity o) throws XMLLoadingException {
    }

    /** Extension of <CODE>java.util.Map</CODE> that allows adding and retrieving of child
     * objects.
     */
    public static class Entity {
        protected String content;
        protected String tag;
        protected Map<String, String> properties = new HashMap<String, String>();
        protected Map<String, List<Entity>> childrenMap = new HashMap<String, List<Entity>>();
        protected List<Entity> childrenList = new ArrayList<Entity>();
        /** Adds a child object to this Entity
         * @param child The object to add as a child
         */
        public void addChild(Entity child) {
            childrenList.add(child);
            String tag = child.getTag();
            List<Entity> maps = childrenMap.get(tag);
            if(maps == null) {
                maps = new ArrayList<Entity>();
                childrenMap.put(tag, maps);
            }
            maps.add(child);
        }
        /** Returns all the children in this Entity
         * @return The children of this Entity
         */
        public List<Entity> getChildren() {
            return childrenList;
        }
        /** Returns all the children in this Entity
         * @return The children of this Entity
         */
        public List<Entity> getChildren(String tag) {
        	List<Entity> children = childrenMap.get(tag);
        	if(children == null)
        		children = Collections.emptyList();
            return children;
        }
        public String getTag() {
            return tag;
        }
        public void setTag(String tag) {
            this.tag = tag;
        }
        public String getContent() {
            return content;
        }
        public void setContent(String content) {
            this.content = content;
        }
        public String getProperty(String name) {
        	return properties.get(name);
        }
        public void setProperty(String name, String value) {
        	properties.put(name, value);
        }
		public Map<String, String> getProperties() {
			return properties;
		}
		//convenience methods
		public Entity getChild(String tag) {
			List<Entity> children = getChildren(tag);
			return children.size() > 0 ? children.get(0) : null;
		}
		public String getChildContent(String tag) {
			Entity child = getChild(tag);
			return child == null ? null : child.getContent();
		}
    }
}
