/*
 * Created on Jan 25, 2005
 *
 */
package simple.xml.dom;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

/**
 * @author bkrug
 *
 */
public class BaseTextNode extends AbstractNode implements Text {
    protected String value;
    /**
     * @param name
     */
    public BaseTextNode(String value) {
        super("#text");
        this.value = value;
    }

    /**
     * @param name
     * @param parentNode
     * @param indexInParent
     */
    public BaseTextNode(String value, Node parentNode, int indexInParent) {
        super("#text", parentNode, indexInParent);
        this.value = value;
   }

    /* (non-Javadoc)
     * @see org.w3c.dom.Text#splitText(int)
     */
    public Text splitText(int offset) throws DOMException {
        String s = getData();
        Text tn = new BaseTextNode(s.substring(offset), parentNode, (parentNode == null ? 1 : parentNode.getChildNodes().getLength() + 1));
        if(parentNode != null) parentNode.appendChild(tn);
        setData(s.substring(0, offset));
        return tn;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.CharacterData#getLength()
     */
    public int getLength() {
        return getData().length();
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.CharacterData#deleteData(int, int)
     */
    public void deleteData(int offset, int count) throws DOMException {
        replaceData(offset, count, "");
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.CharacterData#getData()
     */
    public String getData() throws DOMException {
        return value;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.CharacterData#substringData(int, int)
     */
    public String substringData(int offset, int count) throws DOMException {
        return getData().substring(offset, offset + count);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.CharacterData#replaceData(int, int, java.lang.String)
     */
    public void replaceData(int offset, int count, String arg) throws DOMException {
        String s = getData();
        setData(s.substring(0, offset) + (arg == null ? "" : arg) + s.substring(offset + count));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.CharacterData#insertData(int, java.lang.String)
     */
    public void insertData(int offset, String arg) throws DOMException {
        replaceData(offset, 0, arg);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.CharacterData#appendData(java.lang.String)
     */
    public void appendData(String arg) throws DOMException {
        setData(getData() + (arg == null ? "" : arg));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.CharacterData#setData(java.lang.String)
     */
    public void setData(String data) throws DOMException {
        value = data;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeType()
     */
    public short getNodeType() {
        return TEXT_NODE;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#cloneNode(boolean)
     */
    public Node cloneNode(boolean deep) {
        return new BaseTextNode(value, parentNode, indexInParent);
    }
    
    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeValue()
     */
    public String getNodeValue() throws DOMException {
        return getData();
    }

    public boolean isElementContentWhitespace() {
        return getNodeValue().trim().length() == 0;
    }

    public String getWholeText() {
        return getNodeValue();
    }

    public Text replaceWholeText(String content) throws DOMException {
        Text text = (Text) cloneNode(true);
        setData(content);
        return text;
    }

    public String getTextContent() throws DOMException {
        return getNodeValue();
    }

    public void setTextContent(String textContent) throws DOMException {
        setData(textContent);
    }

}
