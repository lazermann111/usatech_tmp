/*
 * BigDecimalConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.xml.dom;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.sql.Blob;
import java.sql.Clob;

import javax.xml.transform.sax.SAXSource;

import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import simple.bean.Converter;
import simple.bean.Formatter;
import simple.xml.XMLBuilder;
import simple.xml.XMLUtils;

/**
 * Converts objects to a Node (xml)
 * @author Brian S. Krug
 */
public class DOMNodeConverter implements Converter<Node>, Formatter<Node> {

    /** Creates new DOMNodeConverter */
    public DOMNodeConverter() {
    }

    /**
     * Converts objects to Node (xml)
     * @param object The converted object
     * @throws Exception If cannot convert from the given object into a Node
     * @return A Node represenation of the object
     */    
    public Node convert(Object object) throws Exception {
        if(object == null) 
        	return null;        
        if(object instanceof String) {
        	return XMLUtils.parseXML((String)object);
        } else if(object instanceof InputStream) {
        	return XMLUtils.getDOMNode(new SAXSource(new InputSource((InputStream)object)));
        } else if(object instanceof InputStream) {
        	return XMLUtils.getDOMNode(new SAXSource(new InputSource((InputStream)object)));
        } else if(object instanceof Reader) {
        	return XMLUtils.getDOMNode(new SAXSource(new InputSource((Reader)object)));
        } else if(object instanceof InputSource) {
        	return XMLUtils.getDOMNode(new SAXSource((InputSource)object));
        } else if(object instanceof byte[]) {
        	return XMLUtils.getDOMNode(new SAXSource(new InputSource(new ByteArrayInputStream((byte[])object))));
        } else if(object instanceof char[]) {
        	return XMLUtils.parseXML(new String((char[])object));
        } else if(object instanceof Blob) {
        	return XMLUtils.getDOMNode(new SAXSource(new InputSource(((Blob)object).getBinaryStream())));
        } else if(object instanceof Clob) {
        	return XMLUtils.getDOMNode(new SAXSource(new InputSource(((Clob)object).getAsciiStream())));
        } else if(object instanceof File) {
        	return XMLUtils.getDOMNode(new SAXSource(new InputSource(new FileInputStream((File)object))));
        }

        String name = object.getClass().getSimpleName().replace("[]", "-array");
        return new PropertyElementNode(object, name.substring(0, 1).toLowerCase() + name.substring(1));
    }    
    
    /**
     * Returns Node as the target class
     * @return org.w3c.dom.Node
     */    
    public Class<Node> getTargetClass() {
        return Node.class;
    }

	public String format(Node object) {
		StringWriter xml = new StringWriter();
		try {
			XMLBuilder.serializeNode(object, xml);
		} catch(Exception e) {
			return object.toString();
		}
		return xml.toString();
	}
    
}
