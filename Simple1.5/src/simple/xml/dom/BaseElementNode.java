/*
 * Created on Jan 25, 2005
 *
 */
package simple.xml.dom;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.TypeInfo;

/**
 * @author bkrug
 *
 */
public class BaseElementNode extends AbstractNode implements Element {

    /**
     * @param name
     */
    public BaseElementNode(String namespaceURI, String name) {
        super(namespaceURI, name);
    }

    /**
     * @param name
     * @param parentNode
     * @param indexInParent
     */
    public BaseElementNode(String namespaceURI, String name, Node parentNode, int indexInParent) {
        super(namespaceURI, name, parentNode, indexInParent);
    }

    /**
     * @param name
     */
    public BaseElementNode(String name) {
        super(name);
    }

    /**
     * @param name
     * @param parentNode
     * @param indexInParent
     */
    public BaseElementNode(String name, Node parentNode, int indexInParent) {
        super(name, parentNode, indexInParent);
    }
    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getTagName()
     */
    public String getTagName() {
        return getNodeName();
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getAttributeNode(java.lang.String)
     */
    public Attr getAttributeNode(String name) {
        NamedNodeMap nnm = getAttributes();
        if(nnm == null) return null;
        return (Attr)nnm.getNamedItem(name);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#hasAttribute(java.lang.String)
     */
    public boolean hasAttribute(String name) {
        return getAttributeNode(name) != null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getAttribute(java.lang.String)
     */
    public String getAttribute(String name) {
        Attr attr = getAttributeNode(name);
        return (attr == null ? null : attr.getValue());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#setAttribute(java.lang.String, java.lang.String)
     */
    public void setAttribute(String name, String value) throws DOMException {
        Attr attr = getAttributeNode(name);
        if(attr != null) attr.setValue(value);
        else {
            NamedNodeMap nnm = getAttributes();
            if(nnm == null) throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "This node does not support adding attributes to it");
            nnm.setNamedItem(new BaseAttributeNode(name, value, this));
        }
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#setAttributeNode(org.w3c.dom.Attr)
     */
    public Attr setAttributeNode(Attr newAttr) throws DOMException {
        NamedNodeMap nnm = getAttributes();
        if(nnm == null) throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "This node does not support adding attributes to it");
        return (Attr)nnm.setNamedItem(newAttr);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#removeAttribute(java.lang.String)
     */
    public void removeAttribute(String name) throws DOMException {
        NamedNodeMap nnm = getAttributes();
        if(nnm == null) throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "This node does not support adding attributes to it");
        nnm.removeNamedItem(name);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#removeAttributeNode(org.w3c.dom.Attr)
     */
    public Attr removeAttributeNode(Attr oldAttr) throws DOMException {
        NamedNodeMap nnm = getAttributes();
        if(nnm == null) throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "This node does not support adding attributes to it");
        return (Attr)nnm.removeNamedItem(oldAttr.getName());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getAttributeNodeNS(java.lang.String, java.lang.String)
     */
    public Attr getAttributeNodeNS(String namespaceURI, String localName) {
        NamedNodeMap nnm = getAttributes();
        if(nnm == null) return null;
        return (Attr)nnm.getNamedItemNS(namespaceURI, localName);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#hasAttributeNS(java.lang.String, java.lang.String)
     */
    public boolean hasAttributeNS(String namespaceURI, String localName) {
        return getAttributeNodeNS(namespaceURI, name) != null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getAttributeNS(java.lang.String, java.lang.String)
     */
    public String getAttributeNS(String namespaceURI, String localName) {
        Attr attr = getAttributeNodeNS(namespaceURI, name);
        return (attr == null ? null : attr.getValue());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#setAttributeNS(java.lang.String, java.lang.String, java.lang.String)
     */
    public void setAttributeNS(String namespaceURI, String qualifiedName, String value) throws DOMException {
        Attr attr = getAttributeNodeNS(namespaceURI, name);
        if(attr != null) attr.setValue(value);
        else {
            NamedNodeMap nnm = getAttributes();
            if(nnm == null) throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "This node does not support adding attributes to it");
            nnm.setNamedItemNS(new BaseAttributeNode(namespaceURI, name, value, this));
        }
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#setAttributeNodeNS(org.w3c.dom.Attr)
     */
    public Attr setAttributeNodeNS(Attr newAttr) throws DOMException {
        NamedNodeMap nnm = getAttributes();
        if(nnm == null) throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "This node does not support adding attributes to it");
        return (Attr)nnm.setNamedItemNS(newAttr);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#removeAttributeNS(java.lang.String, java.lang.String)
     */
    public void removeAttributeNS(String namespaceURI, String localName) throws DOMException {
        NamedNodeMap nnm = getAttributes();
        if(nnm == null) throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, "This node does not support adding attributes to it");
        nnm.removeNamedItemNS(namespaceURI, name);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getElementsByTagName(java.lang.String)
     */
    public NodeList getElementsByTagName(String name) {
        NodeList nl = getChildNodes();
        if(nl == null) return null;
        List<Element> elements = new ArrayList<Element>();
        for(int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            if(n instanceof Element && 
                    ("*".equals(name) || n.getNodeName().equals(name))) {
                elements.add((Element)n);             
            }
        }
        return new BaseNodeList(elements);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Element#getElementsByTagNameNS(java.lang.String, java.lang.String)
     */
    public NodeList getElementsByTagNameNS(String namespaceURI, String localName) {
        NodeList nl = getChildNodes();
        if(nl == null) return null;
        List<Element> elements = new ArrayList<Element>();
        for(int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            if(n instanceof Element && 
                    ("*".equals(localName) || n.getLocalName().equals(localName)) && 
                    ("*".equals(namespaceURI) || n.getNamespaceURI().equals(namespaceURI))) {
                elements.add((Element)n);             
            }
        }
        return new BaseNodeList(elements);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeType()
     */
    public short getNodeType() {
        return ELEMENT_NODE;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#cloneNode(boolean)
     */
    public Node cloneNode(boolean deep) {
        return new BaseElementNode(namespaceURI, name, parentNode, indexInParent);
    }

    public TypeInfo getSchemaTypeInfo() {
        return null;
    }

    public void setIdAttribute(String name, boolean isId) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

    public void setIdAttributeNS(String namespaceURI, String localName, boolean isId) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

    public void setIdAttributeNode(Attr idAttr, boolean isId) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

    public String getTextContent() throws DOMException {
        String tc = "";
        NodeList nl = getChildNodes();
        for(int i = 0; i < nl.getLength(); i++) {
            String s = nl.item(i).getTextContent();
            if(s != null) tc += s;
        }
        return tc;
    }

    public void setTextContent(String textContent) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

}
