/*
 * Created on Jan 26, 2005
 *
 */
package simple.xml.dom;

import org.w3c.dom.DOMException;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author bkrug
 *
 */
public class BaseDocumentFragment extends AbstractNode implements DocumentFragment {
    protected NodeList childNodes;
    /**
     * @param name
     */
    public BaseDocumentFragment(NodeList childNodes) {
        super("#document-fragment");
        this.childNodes = childNodes;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeType()
     */
    public short getNodeType() {
        return DOCUMENT_FRAGMENT_NODE;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#cloneNode(boolean)
     */
    public Node cloneNode(boolean deep) {
        return new BaseDocumentFragment(childNodes);
    }

    public NodeList getChildNodes() {
        return childNodes;
    }

    public String getTextContent() throws DOMException {
        String tc = "";
        NodeList nl = getChildNodes();
        for(int i = 0; i < nl.getLength(); i++) {
            String s = nl.item(i).getTextContent();
            if(s != null) tc += s;
        }
        return tc;
    }

    public void setTextContent(String textContent) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }
    
}
