/*
 * Created on Jan 25, 2005
 *
 */
package simple.xml.dom;

import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author bkrug
 *
 */
public class BaseNodeList implements NodeList {
    protected List<? extends Node> nodes;
    /**
     * 
     */
    public BaseNodeList(List<? extends Node> nodes) {
        super();
        this.nodes = nodes;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NodeList#getLength()
     */
    public int getLength() {
        return nodes.size();
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NodeList#item(int)
     */
    public Node item(int index) {
        if(index < 0 || index >= nodes.size()) return null;
        return nodes.get(index);
    }

}
