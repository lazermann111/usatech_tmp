/*
 * Created on Jan 25, 2005
 *
 */
package simple.xml.dom;

import java.beans.IntrospectionException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;

import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.text.StringUtils;

/** 
 * Makes an bean look like an XML document fragment by presenting its properties as elements.
 * NOTE: Currently read-only
 * @author bkrug
 *
 */
public class PropertyElementNode extends BaseElementNode {
    private static final Log log = Log.getLog();
    protected boolean exceptionAsValue = false; //true;
    protected Object bean;
    protected boolean initialized = false;
    protected Class<?> valueClass;
    protected static final Set<Class<?>> primitiveClasses = new HashSet<Class<?>>();
    static {
        primitiveClasses.add(String.class);
        primitiveClasses.add(Number.class);
        primitiveClasses.add(BigDecimal.class);
        primitiveClasses.add(BigInteger.class);
        primitiveClasses.add(Long.class);
        primitiveClasses.add(Integer.class);
        primitiveClasses.add(Short.class);
        primitiveClasses.add(Byte.class);
        primitiveClasses.add(Character.class);
        primitiveClasses.add(Double.class);
        primitiveClasses.add(Float.class);
        primitiveClasses.add(java.util.Date.class);
        primitiveClasses.add(java.sql.Date.class);
        primitiveClasses.add(java.sql.Time.class);
        primitiveClasses.add(java.sql.Timestamp.class);       
        primitiveClasses.add(Class.class);       
        primitiveClasses.add(Boolean.class);       
        primitiveClasses.add(StringBuffer.class);       
        primitiveClasses.add(StringBuilder.class);       
    }
    
    public PropertyElementNode(Object bean, String name) {
        this(bean, name, null, 0);
    }

    public PropertyElementNode(Object bean, String name, Node parentNode, int indexInParent) {
        super(name, parentNode, indexInParent);
        this.bean = bean;
    }

    public PropertyElementNode(Object bean, String namespaceURI, String name) {
        this(bean, name, namespaceURI, null, 0);
    }

    public PropertyElementNode(Object bean, String namespaceURI, String name, Node parentNode, int indexInParent) {
        super(namespaceURI, name, parentNode, indexInParent);
        this.bean = bean;
    }
    
    protected Class<?> getValueClass() throws DOMException {
        initialize();
        return valueClass;
    }
    
    /* (non-Javadoc)
     * @see org.w3c.dom.Node#cloneNode(boolean)
     */
    public Node cloneNode(boolean deep) {
        return new PropertyElementNode(bean, namespaceURI, name, parentNode, indexInParent);
    }

    protected NodeList createChildNodes() throws DOMException {
        initialize();
        return childNodes;
    }    
    
    /**
     * @param value
     * @return
     */
    protected Node getRecursingNode(Object value) {
        if(value == null) return null;
        if(value.equals(bean)) return getParentNode();
        for(Node n = getParentNode(); n != null; n = n.getParentNode()) {
            if(n instanceof PropertyElementNode
                    && value.equals(((PropertyElementNode)n).bean)) return n.getParentNode();
        }
        return null;
    }

    protected NamedNodeMap createAttributes() throws DOMException {
        initialize();
        return attributes;
    }
    
    protected void initialize() throws DOMException {
        if(initialized) return;
        try {
            log.debug("Initializing '" + getPathName(this) + "'");
            Object value = getValue();
            List<Node> list = new ArrayList<Node>();
            Class<?> vc;
            if(value == null) vc = null;
            else if(value instanceof ServletRequest) vc = ServletRequest.class;
            else if(value instanceof ServletConfig) vc = ServletConfig.class;
            else if(value instanceof ServletContext) vc = ServletContext.class;
            else vc = value.getClass();           
            NamedNodeMap nnm = new BaseNamedNodeMap();
            nnm.setNamedItem(new BaseAttributeNode("class", null) {
                public String getValue() throws DOMException {
                    Class<?> vc = getValueClass();
                    return vc == null ? "" : vc.getName();
                }
            });
            log.debug("ValueClass=" + (vc == null ? "NULL" : vc.getName()));
            if(vc == null || primitiveClasses.contains(ConvertUtils.convertToWrapperClass(vc))) {
                Node valueNode = new BaseTextNode(ConvertUtils.getString(value, ""), this, 0) {
    	            public String getData() throws DOMException {
    	                log.debug("Getting value of text node '" + getPathName(this) + "'");
    	                return getValueAsString();
    	            }
                    public void setData(String data) throws DOMException {
                        setValueFromString(data);
                    }
                };
                list.add(valueNode);
            } else {
                //check for recursion
                Node recursingNode = getRecursingNode(value);
                if(recursingNode != null) {
                    log.debug("Node is recursing");
                    nnm.setNamedItem(new BaseAttributeNode("ref", getPathName(recursingNode), this));
                } else if(StackTraceElement[].class.isAssignableFrom(vc) && bean instanceof Throwable) {                    
                    Node valueNode = new BaseTextNode(StringUtils.exceptionToString((Throwable)bean), this, 0);
                    list.add(valueNode);                    
                } else if(Collection.class.isAssignableFrom(vc) || vc.isArray()) {
                    String listName = getTagName();
                    if(listName.endsWith("ies")) listName = listName.substring(0, listName.length()-3) + "y";
                    else if(listName.endsWith("IES")) listName = listName.substring(0, listName.length()-3) + "Y";
                    else if(listName.toLowerCase().endsWith("es")) listName = listName.substring(0, listName.length()-2);
                    else if(listName.toLowerCase().endsWith("s")) listName = listName.substring(0, listName.length()-1);
                    else listName = listName + "-item";
                    int size;                   
                    if(value instanceof Collection<?>) size = ((Collection<?>) value).size();
                    else size = Array.getLength(value);
                    for(int i = 0; i < size; i++) {
                        list.add(new PropertyElementNode(value, namespaceURI, listName, this, i) {
                            protected void setValueFromString(String data) throws DOMException {
                                try {
                                    ReflectionUtils.setIndexedProperty(bean, "", indexInParent, data, true);
                                    valueChanged();
                                } catch (Exception e) {
                                    log.debug("While setting property value for " + bean + " at " + indexInParent, e);
                                    //throw new DOMException(DOMException.INVALID_ACCESS_ERR, e.getMessage());
                                }                
                            }                        
                            protected Object getValue() throws DOMException {
                                try {
                                    log.debug("Getting value of indexed property of " + name + " at " + indexInParent + " from " + bean);
                                    return ReflectionUtils.getIndexedProperty(bean, "", indexInParent, true);
                                } catch (Exception e) {
                                    log.debug("While getting property value for " + bean + " at " + indexInParent, e);
                                    return null;
                                    //throw new DOMException(DOMException.INVALID_ACCESS_ERR, e.getMessage());
                                }                
                            }
                        });
                    }   
                    log.debug("Node is list with " + size + " entries");
                } else {
                    try {
                        Iterator<String> iter = ReflectionUtils.getPropertyNames(value).iterator();                    
                        for(int i = 0; iter.hasNext(); i++) {
                            String propName = iter.next();
                            if(!propName.equals("class"))
                                list.add(new PropertyElementNode(value, namespaceURI, propName, this, i));
                        }
                        log.debug("Node is bean with " + list.size() + " properties");
                    } catch (IntrospectionException e) {
                        log.debug("While getting property names for " + value, e);
                        //throw new DOMException(DOMException.INVALID_ACCESS_ERR, e.getMessage());
                    }                          
                }
            }
            valueClass = vc;
            attributes = nnm;
            childNodes = new BaseNodeList(list);
            initialized = true;
        } catch (Exception e) {
            log.debug("While initializing " + bean, e);
            throw new DOMException(DOMException.INVALID_ACCESS_ERR, e.getMessage());
        }        
    }
    
    protected void setValueFromString(String data) throws DOMException {
        try {
            ReflectionUtils.setProperty(bean, name, data, true);
            valueChanged();
        } catch (Exception e) {
            throw new DOMException(DOMException.INVALID_ACCESS_ERR, e.getMessage());
        }                
    }
    
    protected String getValueAsString() throws DOMException {
        try {
            return ConvertUtils.getString(getValue(), "");
        } catch (Exception e) {
            throw new DOMException(DOMException.INVALID_ACCESS_ERR, e.getMessage());
        }                
    }
    
    protected Object getValue() throws DOMException {
        try {
            return ReflectionUtils.getProperty(bean, name);
        } catch (Exception e) {
            if(exceptionAsValue) return e;
            else {
                log.warn("While getting property " + name + " on " + bean);
                //throw new DOMException(DOMException.INVALID_ACCESS_ERR, e.getMessage());
                return null;
            }
        }                
    }
    
    protected void valueChanged() {
        initialized = false;
        childNodes = null;
        attributes = null;
    }
}
