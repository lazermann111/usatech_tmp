/*
 * Created on Jan 25, 2005
 *
 */
package simple.xml.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.TypeInfo;

/** 
 * Base read-only attribute
 * @author bkrug
 *
 */
public class BaseAttributeNode extends AbstractNode implements Attr {
    protected String value;
    protected Element parentElement;
    
    public BaseAttributeNode(String name, String value) {
        this(null, name, value, null);
    }

    public BaseAttributeNode(String name, String value, Element parentElement) {
        this(null, name, value, parentElement);
    }

    public BaseAttributeNode(String namespaceURI, String name, String value) {
        this(namespaceURI, name, value, null);
    }

    public BaseAttributeNode(String namespaceURI, String name, String value, Element parentElement) {
        super(namespaceURI, name, null, 0);
        this.value = value;
        this.parentElement = parentElement;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeType()
     */
    public short getNodeType() {
        return ATTRIBUTE_NODE;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#cloneNode(boolean)
     */
    public Node cloneNode(boolean deep) {
        return new BaseAttributeNode(name, value, parentElement);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Attr#getSpecified()
     */
    public boolean getSpecified() {
        return value != null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Attr#getName()
     */
    public String getName() {
        return getNodeName();
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Attr#getValue()
     */
    public String getValue() {
        return value;
    }
    
	/* (non-Javadoc)
	 * @see org.w3c.dom.Node#getNodeValue()
	 */
	public String getNodeValue() throws DOMException {
	    return getValue();
	}
	
    /* (non-Javadoc)
     * @see org.w3c.dom.Attr#setValue(java.lang.String)
     */
    public void setValue(String value) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }
    
	/* (non-Javadoc)
	 * @see org.w3c.dom.Node#setNodeValue(java.lang.String)
	 */
	public void setNodeValue(String nodeValue) throws DOMException {
	    setValue(nodeValue);
	}
	
    /* (non-Javadoc)
     * @see org.w3c.dom.Attr#getOwnerElement()
     */
    public Element getOwnerElement() {
        return parentElement;
    }

    public TypeInfo getSchemaTypeInfo() {
        return null;
    }

    public boolean isId() {
        return false;
    }

    public String getTextContent() throws DOMException {
        return null;
    }

    public void setTextContent(String textContent) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);        
    }
}
