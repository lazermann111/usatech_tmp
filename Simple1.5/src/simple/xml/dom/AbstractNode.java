/*
 * Created on Jan 25, 2005
 *
 */
package simple.xml.dom;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.UserDataHandler;

//import simple.io.Log;

/** 
 * Base class for BeanNode and PropertyNode
 * @author bkrug
 *
 */
public abstract class AbstractNode implements Node {
    //private static final Log log = Log.getLog();
    protected String namespaceURI;
    protected String name;
    protected Node parentNode;
    protected int indexInParent;
    protected NodeList childNodes;
    protected NamedNodeMap attributes;
    protected Map<String, Object> userData = new HashMap<String, Object>();
    protected int documentPosition = -1;
    /**
     * 
     */
    public AbstractNode(String name) {
        this(name, null, 0);
    }

    public AbstractNode(String name, Node parentNode, int indexInParent) {
        this(null, name, parentNode, indexInParent);
    }
    
    public AbstractNode(String namespaceURI, String name) {
        this(namespaceURI, name, null, 0);
    }

    public AbstractNode(String namespaceURI, String name, Node parentNode, int indexInParent) {
        this.namespaceURI = namespaceURI;
        this.name = name;
        this.parentNode = parentNode;
        this.indexInParent = indexInParent;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#normalize()
     */
    public void normalize() {
        //don't need to do anything
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#hasAttributes()
     */
    public boolean hasAttributes() {
        return getAttributes().getLength() > 0;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#hasChildNodes()
     */
    public boolean hasChildNodes() {
        return getChildNodes().getLength() > 0;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getLocalName()
     */
    public String getLocalName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNamespaceURI()
     */
    public String getNamespaceURI() {
        return namespaceURI;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeName()
     */
    public String getNodeName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeValue()
     */
    public String getNodeValue() throws DOMException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getPrefix()
     */
    public String getPrefix() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#setNodeValue(java.lang.String)
     */
    public void setNodeValue(String nodeValue) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#setPrefix(java.lang.String)
     */
    public void setPrefix(String prefix) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getOwnerDocument()
     */
    public Document getOwnerDocument() {
        return (parentNode == null ? null : parentNode.getOwnerDocument());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getAttributes()
     */
    public NamedNodeMap getAttributes() {
        if(attributes == null) attributes = createAttributes();
        return attributes;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getFirstChild()
     */
    public Node getFirstChild() {
        NodeList nl = getChildNodes();
        return (nl.getLength() > 0 ? getChildNodes().item(0) : null);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getLastChild()
     */
    public Node getLastChild() {
        NodeList nl = getChildNodes();
        return (nl.getLength() > 0 ? getChildNodes().item(nl.getLength()) : null);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNextSibling()
     */
    public Node getNextSibling() {
        return (getParentNode() == null ? null : getParentNode().getChildNodes().item(indexInParent+1));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getParentNode()
     */
    public Node getParentNode() {
        return parentNode;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getPreviousSibling()
     */
    public Node getPreviousSibling() {
        return (getParentNode() == null ? null : getParentNode().getChildNodes().item(indexInParent-1));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getChildNodes()
     */
    public NodeList getChildNodes() {
        if(childNodes == null) childNodes = createChildNodes();
        return childNodes;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#isSupported(java.lang.String, java.lang.String)
     */
    public boolean isSupported(String feature, String version) {
        return false;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#appendChild(org.w3c.dom.Node)
     */
    public Node appendChild(Node newChild) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#removeChild(org.w3c.dom.Node)
     */
    public Node removeChild(Node oldChild) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#insertBefore(org.w3c.dom.Node, org.w3c.dom.Node)
     */
    public Node insertBefore(Node newChild, Node refChild) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#replaceChild(org.w3c.dom.Node, org.w3c.dom.Node)
     */
    public Node replaceChild(Node newChild, Node oldChild) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }

    /**
     * @return
     */
    protected NamedNodeMap createAttributes() throws DOMException {
        return new BaseNamedNodeMap();
    }
    
    protected NodeList createChildNodes() throws DOMException {
    	List<? extends Node> nodes = Collections.emptyList();
        return new BaseNodeList(nodes);
    }
    
    /** Returns a string representing the xpath of the specified node
     * @param nNode
     * @return
     */
    public static String getPathName(Node node) {
        String path = "";
        for(Node n = node; n != null; n = n.getParentNode()) {
            switch(n.getNodeType()) {
            	case Node.ATTRIBUTE_NODE: 
            	    path = "@" + n.getNodeName() + path;
            	    break;
            	case Node.ELEMENT_NODE: 
            	    path = "/" + n.getNodeName() + path;
            	    break;
            }
        }
        return path;
    }

    protected int getDocumentPosition() {
        if(documentPosition == -1) {
            Node pn = getParentNode();
            if(pn == null) {
                documentPosition = 1;
            } else {
                Node ps = getPreviousSibling();
                if(ps == null) {
                    if(pn instanceof AbstractNode) {
                        documentPosition = ((AbstractNode)pn).getDocumentPosition() + 1;
                    } else {
                        documentPosition = 0;
                    }
                } else if(ps instanceof AbstractNode) {
                    documentPosition = ((AbstractNode)ps).getDocumentPosition() + 1;
                } else {
                    documentPosition = 0;
                }
            }
        }
        return documentPosition;
    }
    /* (non-Javadoc)
     * @see org.w3c.dom.Node#compareDocumentPosition(org.w3c.dom.Node)
     */
    public short compareDocumentPosition(Node other) throws DOMException {
        if(other instanceof AbstractNode) {
            return (short)(getDocumentPosition() - ((AbstractNode)other).getDocumentPosition());
        } else {
            return 0;
        }
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getBaseURI()
     */
    public String getBaseURI() {
        Document doc = getOwnerDocument();
        return (doc == null ? null : doc.getBaseURI());
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getFeature(java.lang.String, java.lang.String)
     */
    public Object getFeature(String feature, String version) {
        Document doc = getOwnerDocument();
        return (doc == null ? null : doc.getFeature(feature, version));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getUserData(java.lang.String)
     */
    public Object getUserData(String key) {
        return userData.get(key);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#isDefaultNamespace(java.lang.String)
     */
    public boolean isDefaultNamespace(String namespaceURI) {
        Document doc = getOwnerDocument();
        return (doc == null ? false : doc.isDefaultNamespace(namespaceURI));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#isEqualNode(org.w3c.dom.Node)
     */
    public boolean isEqualNode(Node arg) {
        return equals(arg);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#isSameNode(org.w3c.dom.Node)
     */
    public boolean isSameNode(Node other) {
        return equals(other);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#lookupNamespaceURI(java.lang.String)
     */
    public String lookupNamespaceURI(String prefix) {
        Document doc = getOwnerDocument();
        return (doc == null ? null : doc.lookupNamespaceURI(prefix));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#lookupPrefix(java.lang.String)
     */
    public String lookupPrefix(String namespaceURI) {
        Document doc = getOwnerDocument();
        return (doc == null ? null : doc.lookupPrefix(namespaceURI));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#setUserData(java.lang.String, java.lang.Object, org.w3c.dom.UserDataHandler)
     */
    public Object setUserData(String key, Object data, UserDataHandler handler) {
        return userData.put(key, data);
    }

}
