/*
 * Created on Jan 25, 2005
 *
 */
package simple.xml.dom;

import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import simple.util.IndexedMap;

/**
 * @author bkrug
 *
 */
public class BaseNamedNodeMap implements NamedNodeMap {
    protected IndexedMap namedMap = new IndexedMap();
    
    /**
     * 
     */
    public BaseNamedNodeMap() {
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NamedNodeMap#getNamedItem(java.lang.String)
     */
    public Node getNamedItem(String name) {
        return (Node)namedMap.get(name);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NamedNodeMap#removeNamedItem(java.lang.String)
     */
    public Node removeNamedItem(String name) throws DOMException {
        Node n = (Node) namedMap.remove(name);
        if(n != null) {          
            namedMap.remove(getNSKey(n.getNamespaceURI(), n.getLocalName()));           
        }
        return n;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NamedNodeMap#setNamedItem(org.w3c.dom.Node)
     */
    public Node setNamedItem(Node newNode) throws DOMException {
        Node n = (Node) namedMap.put(newNode.getNodeName(), newNode);
        namedMap.put(getNSKey(newNode.getNamespaceURI(), newNode.getLocalName()), newNode);
        return n;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NamedNodeMap#setNamedItemNS(org.w3c.dom.Node)
     */
    public Node setNamedItemNS(Node newNode) throws DOMException {
        Node n = (Node) namedMap.put(getNSKey(newNode.getNamespaceURI(), newNode.getLocalName()), newNode);
        namedMap.put(newNode.getNodeName(), newNode);
        return n;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NamedNodeMap#getNamedItemNS(java.lang.String, java.lang.String)
     */
    public Node getNamedItemNS(String namespaceURI, String localName) {
        return (Node)namedMap.get(getNSKey(namespaceURI, localName));
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NamedNodeMap#removeNamedItemNS(java.lang.String, java.lang.String)
     */
    public Node removeNamedItemNS(String namespaceURI, String localName) throws DOMException {
        Node n = (Node) namedMap.remove(getNSKey(namespaceURI, localName));
        if(n != null) {          
            namedMap.remove(n.getNodeName());           
        }
        return n;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NamedNodeMap#getLength()
     */
    public int getLength() {
        return namedMap.size() / 2;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.NamedNodeMap#item(int)
     */
    public Node item(int index) {
        return (Node)namedMap.getValue(index/2);
    }

    protected Object getNSKey(String namespaceURI, String localName) {
        return "" + namespaceURI + ":" + localName;
    }
}
