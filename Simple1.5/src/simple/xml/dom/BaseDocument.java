/*
 * Created on Jan 26, 2005
 *
 */
package simple.xml.dom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.EntityReference;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

/**
 * @author bkrug
 *
 */
public class BaseDocument extends AbstractNode implements Document {
    protected Element rootNode;
    protected String documentURI;
    protected boolean strictErrorChecking;
    protected String xmlVersion;
    protected boolean xmlStandalone;
    
    public BaseDocument() {
        super("#document");
    }

    
    /**
     * @param name
     */
    public BaseDocument(Element rootNode) {
        this();
        this.rootNode = rootNode;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#getImplementation()
     */
    public DOMImplementation getImplementation() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createDocumentFragment()
     */
    public DocumentFragment createDocumentFragment() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#getDoctype()
     */
    public DocumentType getDoctype() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#getDocumentElement()
     */
    public Element getDocumentElement() {
        return rootNode;
    }

    public void setDocumentElement(Element rootNode) {
        this.rootNode = rootNode;
        childNodes = null;
    }
    
    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createAttribute(java.lang.String)
     */
    public Attr createAttribute(String name) throws DOMException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createCDATASection(java.lang.String)
     */
    public CDATASection createCDATASection(String data) throws DOMException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createComment(java.lang.String)
     */
    public Comment createComment(String data) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createElement(java.lang.String)
     */
    public Element createElement(String tagName) throws DOMException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#getElementById(java.lang.String)
     */
    public Element getElementById(String elementId) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createEntityReference(java.lang.String)
     */
    public EntityReference createEntityReference(String name) throws DOMException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#importNode(org.w3c.dom.Node, boolean)
     */
    public Node importNode(Node importedNode, boolean deep) throws DOMException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#getElementsByTagName(java.lang.String)
     */
    public NodeList getElementsByTagName(String tagname) {
        NodeList nl = getChildNodes();
        if(nl == null) return null;
        List<Element> elements = new ArrayList<Element>();
        for(int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            if(n instanceof Element && 
                    ("*".equals(name) || n.getNodeName().equals(name))) {
                elements.add((Element)n);             
            }
        }
        return new BaseNodeList(elements);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createTextNode(java.lang.String)
     */
    public Text createTextNode(String data) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createAttributeNS(java.lang.String, java.lang.String)
     */
    public Attr createAttributeNS(String namespaceURI, String qualifiedName) throws DOMException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createElementNS(java.lang.String, java.lang.String)
     */
    public Element createElementNS(String namespaceURI, String qualifiedName) throws DOMException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#getElementsByTagNameNS(java.lang.String, java.lang.String)
     */
    public NodeList getElementsByTagNameNS(String namespaceURI, String localName) {
        NodeList nl = getChildNodes();
        if(nl == null) return null;
        List<Element> elements = new ArrayList<Element>();
        for(int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            if(n instanceof Element && 
                    ("*".equals(localName) || n.getLocalName().equals(localName)) && 
                    ("*".equals(namespaceURI) || n.getNamespaceURI().equals(namespaceURI))) {
                elements.add((Element)n);             
            }
        }
        return new BaseNodeList(elements);
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Document#createProcessingInstruction(java.lang.String, java.lang.String)
     */
    public ProcessingInstruction createProcessingInstruction(String target, String data) throws DOMException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#getNodeType()
     */
    public short getNodeType() {
        return DOCUMENT_NODE;
    }

    /* (non-Javadoc)
     * @see org.w3c.dom.Node#cloneNode(boolean)
     */
    public Node cloneNode(boolean deep) {
        return new BaseDocument(rootNode);
    }

    protected NodeList createChildNodes() throws DOMException {
        return new BaseNodeList(Collections.singletonList(rootNode));
    }


    public String getInputEncoding() {
        return null;
    }


    public String getXmlEncoding() {
        return null;
    }


    public boolean getXmlStandalone() {
        return xmlStandalone;
    }


    public void setXmlStandalone(boolean xmlStandalone) throws DOMException {
        this.xmlStandalone = xmlStandalone;
    }


    public String getXmlVersion() {
        return xmlVersion;
    }


    public void setXmlVersion(String xmlVersion) throws DOMException {
        this.xmlVersion = xmlVersion;
    }


    public boolean getStrictErrorChecking() {
        return strictErrorChecking;
    }


    public void setStrictErrorChecking(boolean strictErrorChecking) {
        this.strictErrorChecking = strictErrorChecking;
    }


    public String getDocumentURI() {
        return documentURI;
    }


    public void setDocumentURI(String documentURI) {
        this.documentURI = documentURI;      
    }


    public Node adoptNode(Node source) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }


    public DOMConfiguration getDomConfig() {
        return null;
    }


    public void normalizeDocument() {
    }


    public Node renameNode(Node n, String namespaceURI, String qualifiedName) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }


    public String getTextContent() throws DOMException {
        String tc = "";
        NodeList nl = getChildNodes();
        for(int i = 0; i < nl.getLength(); i++) {
            String s = nl.item(i).getTextContent();
            if(s != null) tc += s;
        }
        return tc;
    }


    public void setTextContent(String textContent) throws DOMException {
        throw new DOMException(DOMException.NO_MODIFICATION_ALLOWED_ERR, null);
    }
}
