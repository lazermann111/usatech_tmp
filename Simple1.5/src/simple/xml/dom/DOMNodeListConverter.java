/*
 * BigDecimalConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.xml.dom;

import java.io.StringWriter;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import simple.bean.Converter;
import simple.bean.Formatter;
import simple.xml.XMLBuilder;

/**
 * Converts objects to a NodeList (xml)
 * @author Brian S. Krug
 */
public class DOMNodeListConverter implements Converter<NodeList>, Formatter<NodeList> {
	protected final DOMNodeConverter delegate = new DOMNodeConverter();
    /** Creates new DOMNodeListConverter */
    public DOMNodeListConverter() {
    }

    /**
     * Converts objects to NodeList (xml)
     * @param object The converted object
     * @throws Exception If cannot convert from the given object into a NodeList
     * @return A NodeList represenation of the object
     */    
    public NodeList convert(Object object) throws Exception {
        Node node = delegate.convert(object);
        return node == null ? null : node.getChildNodes();
    }    
    
    /**
     * Returns NodeList as the target class
     * @return org.w3c.dom.NodeList
     */    
    public Class<NodeList> getTargetClass() {
        return NodeList.class;
    }

	public String format(NodeList object) {
		StringWriter xml = new StringWriter();
		try {
			XMLBuilder.serializeNode(new BaseDocumentFragment(object), xml);
		} catch(Exception e) {
			return object.toString();
		}
		return xml.toString();
	}
    
}
