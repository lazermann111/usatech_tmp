/*
 * Created on Jan 25, 2005
 *
 */
package simple.xml.dom;

import org.w3c.dom.Node;

/**
 * @author bkrug
 *
 */
public class IndexedPropertyElementNode extends PropertyElementNode {

    /**
     * @param bean
     * @param name
     */
    public IndexedPropertyElementNode(Object bean, String name) {
        super(bean, name);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param bean
     * @param name
     * @param parentNode
     * @param indexInParent
     */
    public IndexedPropertyElementNode(Object bean, String name, Node parentNode, int indexInParent) {
        super(bean, name, parentNode, indexInParent);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param bean
     * @param namespaceURI
     * @param name
     */
    public IndexedPropertyElementNode(Object bean, String namespaceURI, String name) {
        super(bean, namespaceURI, name);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param bean
     * @param namespaceURI
     * @param name
     * @param parentNode
     * @param indexInParent
     */
    public IndexedPropertyElementNode(Object bean, String namespaceURI, String name, Node parentNode,
            int indexInParent) {
        super(bean, namespaceURI, name, parentNode, indexInParent);
        // TODO Auto-generated constructor stub
    }

}
