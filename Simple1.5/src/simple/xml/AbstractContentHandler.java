package simple.xml;

import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.AttributesImpl;

public abstract class AbstractContentHandler implements ExtendedContentHandler, LexicalHandler {
	public static final Attributes EMPTY_ATTS = new EmptyAttributes();
    
	protected abstract String getNameSpaceURI() ;
	protected abstract String getPrefix() ;
    
	@Override
	public void comment(String ch) throws SAXException {
		if(ch != null)
			comment(ch.toCharArray(), 0, ch.length());
	}

	/**
	 * @see simple.xml.ExtendedContentHandler#characters(java.lang.String)
	 */
	public void characters(String ch) throws SAXException {
		if(ch != null)
			characters(ch.toCharArray(), 0, ch.length());
    }

	protected void characters(char[] characters) throws SAXException {
		if(characters != null)
			characters(characters, 0, characters.length);
	}
	/**
	 * @see simple.xml.ExtendedContentHandler#startElement(java.lang.String)
	 */
	public void startElement(String localName) throws SAXException {
        startElement(getNameSpaceURI(), localName, getQName(localName), EMPTY_ATTS);
    }
	/**
	 * @see simple.xml.ExtendedContentHandler#startElement(java.lang.String, java.util.Map)
	 */
	public void startElement(String localName, Map<String,String> attributes) throws SAXException {
        AttributesImpl atts = new AttributesImpl();
        for(Map.Entry<String, String> entry : attributes.entrySet()) {
            atts.addAttribute(getNameSpaceURI(), entry.getKey(), getQName(entry.getKey()), "CDATA", entry.getValue());       
        }
        startElement(getNameSpaceURI(), localName, getQName(localName), atts);
    }
	/**
	 * @see simple.xml.ExtendedContentHandler#startElement(java.lang.String, java.lang.String[], java.lang.String)
	 */
	public void startElement(String localName, String[] attributeNames, String... attributeValues) throws SAXException {
        AttributesImpl atts = new AttributesImpl();
        for(int i = 0; i < attributeNames.length && i < attributeValues.length; i++) {
            atts.addAttribute(getNameSpaceURI(), attributeNames[i], getQName(attributeNames[i]), "CDATA", attributeValues[i]);       
        }
        startElement(getNameSpaceURI(), localName, getQName(localName), atts);
    }
	/**
	 * @see simple.xml.ExtendedContentHandler#startElement(java.lang.String, org.xml.sax.Attributes)
	 */
	public void startElement(String localName, Attributes atts) throws SAXException {
        startElement(getNameSpaceURI(), localName, getQName(localName), atts);
    }
	/**
	 * @see simple.xml.ExtendedContentHandler#endElement(java.lang.String)
	 */
	public void endElement(String localName) throws SAXException {
        endElement(getNameSpaceURI(), localName, getQName(localName));
    }
    protected String getQName(String localName) {
        if(getPrefix() != null && getPrefix().length() > 0) 
            return getPrefix() + ":" + localName;
        else 
            return localName;            
    }

	public void putElement(String localName) throws SAXException {
		startElement(localName);
		endElement(localName);
	}

	public void putElement(String localName, String content) throws SAXException {
		startElement(localName);
		if(content != null && content.length() > 0)
			characters(content);
		endElement(localName);
	}

	public void putElement(String localName, String content, String[] attributeNames, String... attributeValues) throws SAXException {
		startElement(localName, attributeNames, attributeValues);
		if(content != null && content.length() > 0)
			characters(content);
		endElement(localName);
	}

	public void putElement(String localName, char[] content, String[] attributeNames, String... attributeValues) throws SAXException {
		startElement(localName, attributeNames, attributeValues);
		if(content != null && content.length > 0)
			characters(content);
		endElement(localName);
	}

	public void putElement(String localName, String content, Map<String, String> attributes) throws SAXException {
		startElement(localName, attributes);
		if(content != null && content.length() > 0)
			characters(content);
		endElement(localName);
	}

	public void putElement(String localName, Attributes attributes, String content) throws SAXException {
		startElement(localName, attributes);
		if(content != null && content.length() > 0)
			characters(content);
		endElement(localName);
	}

	public void putElement(String localName, Attributes attributes) throws SAXException {
		putElement(localName, attributes, null);
	}
}
