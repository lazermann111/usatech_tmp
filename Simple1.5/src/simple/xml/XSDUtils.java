package simple.xml;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Locale;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;

public class XSDUtils {
	public static final Format DATE_TIME_TYPE_FORMAT = new XsdDateTimeFormat();
	public static final Format DATE_TYPE_FORMAT = new XsdDateFormat();
	public static final Format TIME_TYPE_FORMAT = new XsdTimeFormat();

	protected static class XsdDateTimeFormat extends Format {
		private static final long serialVersionUID = -7025953811680208608L;

		@Override
		public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition fieldPosition) {
			if(obj != null) {
				try {
					appendFormat(toAppendTo, ConvertUtils.convert(Calendar.class, obj));
				} catch(ConvertException e) {
					throw new IllegalArgumentException("Cannot format given Object as a Date");
				}
			}
			return toAppendTo;
		}

		protected void appendFormat(StringBuffer toAppendTo, Calendar cal) {
			appendDate(toAppendTo, cal);
			toAppendTo.append('T');
			appendTime(toAppendTo, cal);
		}

		protected void appendDate(StringBuffer toAppendTo, Calendar cal) {
			appendField(toAppendTo, cal, Calendar.YEAR, 4, 0);
			toAppendTo.append('-');
			appendField(toAppendTo, cal, Calendar.MONTH, 2, 1);
			toAppendTo.append('-');
			appendField(toAppendTo, cal, Calendar.DAY_OF_MONTH, 2, 0);
		}

		protected void appendTime(StringBuffer toAppendTo, Calendar cal) {
			appendField(toAppendTo, cal, Calendar.HOUR_OF_DAY, 2, 0);
			toAppendTo.append(':');
			appendField(toAppendTo, cal, Calendar.MINUTE, 2, 0);
			toAppendTo.append(':');
			appendField(toAppendTo, cal, Calendar.SECOND, 2, 0);
			int tzOffsetMin = cal.getTimeZone().getOffset(cal.getTimeInMillis()) / (1000 * 60);
			if(tzOffsetMin == 0) {
				toAppendTo.append('Z');
				return;
			}
			if(tzOffsetMin > 0)
				toAppendTo.append('+');
			else {
				tzOffsetMin = -tzOffsetMin;
				toAppendTo.append('-');
			}
			StringUtils.appendPadded(toAppendTo, String.valueOf(tzOffsetMin / 60), '0', 2, Justification.LEFT);
			toAppendTo.append(':');
			StringUtils.appendPadded(toAppendTo, String.valueOf(tzOffsetMin % 60), '0', 2, Justification.LEFT);
		}

		protected void appendField(StringBuffer toAppendTo, Calendar cal, int field, int length, int adjust) {
			StringUtils.appendPadded(toAppendTo, String.valueOf(cal.get(field) + adjust), '0', length, Justification.LEFT);
		}

		@Override
		public Object parseObject(String source, ParsePosition pos) {
			// TODO Auto-generated method stub
			return null;
		}

	}
	protected static class XsdDateFormat extends XsdDateTimeFormat {
		private static final long serialVersionUID = -3454218876661731379L;

		@Override
		protected void appendFormat(StringBuffer toAppendTo, Calendar cal) {
			appendDate(toAppendTo, cal);
		}
	}

	protected static class XsdTimeFormat extends XsdDateTimeFormat {
		private static final long serialVersionUID = 3454218876661731379L;

		@Override
		protected void appendFormat(StringBuffer toAppendTo, Calendar cal) {
			appendTime(toAppendTo, cal);
		}
	}

	public static String toLanguage(Locale locale) {
		if(locale == null || StringUtils.isBlank(locale.getLanguage()))
			return "";
		String country = locale.getCountry();
		if(StringUtils.isBlank(country))
			return locale.getLanguage();
		return new StringBuilder().append(locale.getLanguage()).append('-').append(country).toString();
	}
}
