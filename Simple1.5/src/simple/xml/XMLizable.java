/*
 * Created on Sep 30, 2005
 *
 */
package simple.xml;
/**
 * This interface marks a class as able to be read from and written to XML.
 * If the class implements a static or instance readXML(ObjectBuilder builder) method, then
 * that method is used to create objects of that class from XML, otherwise a 
 * default bean property
 * based method is used. Likewise, if a class implements an instance 
 * writeXML(String tagName, XMLBuilder builder) method, then
 * that method is used to convert objects of that class into XML, otherwise a 
 * default bean property-based method is used.
 * @author bkrug
 *
 */
public interface XMLizable {

}
