/*
 * BeanXMLLoader.java
 *
 * Created on October 17, 2003, 10:16 AM
 */

package simple.xml;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ReflectionUtils;

/** Loads the XML into a heirarchy of beans according to the tag to class mapping
 * which is configured using the <CODE>registerTagClass()</CODE> method.
 * @author Brian S. Krug
 */
public class BeanXMLLoader extends XMLLoader<Object> {
    protected Map<String,Class<?>> tagClassMap = new java.util.HashMap<String,Class<?>>();
    protected boolean ignoreAddTextErrors = false;
    protected boolean ignoreAddChildErrors = false;

    /** Creates a new instance of XMLLoader */
    public BeanXMLLoader() {
    }

    /** Registers the specified tag with the given class. Whenever this tag is
     * encountered an new object of the specified class is created.
     * @param tag The xml tag to associate with
     * @param cls The class that represents xml elements with the specified tag
     */
    public void registerTagClass(String tag, Class<?> cls) {
        tagClassMap.put(tag, cls);
    }

    protected Object createObject(String tag, DocumentLocation location) throws XMLLoadingException {
        Class<?> cls = tagClassMap.get(tag);
        if(cls == null) throw new XMLLoadingException("No class defined for tag, '" + tag + "'");
        try {
            return cls.newInstance();
        } catch (InstantiationException e) {
            throw new XMLLoadingException(e);
        } catch (IllegalAccessException e) {
            throw new XMLLoadingException(e);
        }
    }

    protected void applyAttribute(Object o, String name, String value) throws XMLLoadingException {
        try {
            simple.bean.ReflectionUtils.setProperty(o, name, value, true);
        } catch (IntrospectionException e) {
            throw new XMLLoadingException("Could set property '" + name + "' on " + o, e);
        } catch (IllegalAccessException e) {
            throw new XMLLoadingException("Could set property '" + name + "' on " + o, e);
        } catch (InvocationTargetException e) {
            throw new XMLLoadingException("Could set property '" + name + "' on " + o, e);
        } catch (InstantiationException e) {
            throw new XMLLoadingException("Could set property '" + name + "' on " + o, e);
        } catch (ConvertException e) {
            throw new XMLLoadingException("Could set property '" + name + "' on " + o, e);
        } catch(ParseException e) {
            throw new XMLLoadingException("Could set property '" + name + "' on " + o, e);
		}
    }

    protected void addTo(Object parent, Object o, String tag) throws XMLLoadingException {
        try {
            ReflectionUtils.invokeMethod(parent, "add", new Object[] {o});
        } catch(SecurityException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        } catch (IllegalArgumentException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        } catch (NoSuchMethodException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        } catch (IllegalAccessException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        } catch (InvocationTargetException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        }
    }

    protected void addText(Object o, String text) throws XMLLoadingException {
        try {
            ReflectionUtils.invokeMethod(o, "add", new Object[] {text});
        } catch(SecurityException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        } catch (IllegalArgumentException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        } catch (NoSuchMethodException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        } catch (IllegalAccessException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        } catch (InvocationTargetException e) {
            if(!ignoreAddTextErrors) throw new XMLLoadingException("Parent class, " + o.getClass() + ", does not have an 'add' method", e);
        }
    }

    protected void finishObject(Object o) throws XMLLoadingException {
    }

    public boolean isIgnoreAddChildErrors() {
        return ignoreAddChildErrors;
    }

    public void setIgnoreAddChildErrors(boolean ignoreAddChildErrors) {
        this.ignoreAddChildErrors = ignoreAddChildErrors;
    }

    public boolean isIgnoreAddTextErrors() {
        return ignoreAddTextErrors;
    }

    public void setIgnoreAddTextErrors(boolean ignoreAddTextErrors) {
        this.ignoreAddTextErrors = ignoreAddTextErrors;
    }

}
