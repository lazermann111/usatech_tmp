/*
 * Created on Sep 20, 2005
 *
 */
package simple.xml;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.sax.SAXSource;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import simple.io.Log;
import simple.xml.sax.BeanAdapter1;
import simple.xml.sax.BeanInputSource;

import com.sun.org.apache.xml.internal.utils.DOMBuilder;

public class XMLUtils {
    protected static final DocumentBuilderFactory builder = DocumentBuilderFactory.newInstance();
    protected static final BeanAdapter1 beanAdapter = new BeanAdapter1();
    static {
		try {
			builder.setFeature("http://xml.org/sax/features/external-general-entities", false);
		} catch(ParserConfigurationException e) {
			Log.getLog().warn("Could not turn off external entity resolution", e);
		}
    }


    public static Node parseXML(String s) throws SAXException, IOException, ParserConfigurationException {
        Document doc = builder.newDocumentBuilder().parse(new InputSource(new StringReader(s)));
        return doc.getDocumentElement();
    }

    public static Node getDOMNode(SAXSource xmlSource) throws ParserConfigurationException, IOException, SAXException {
        Document doc = builder.newDocumentBuilder().newDocument();
        DOMBuilder handler = new DOMBuilder(doc);
        XMLReader xmlReader = xmlSource.getXMLReader();
        if(xmlReader == null)
        	xmlReader = XMLReaderFactory.createXMLReader();
        xmlReader.setContentHandler(handler);
        xmlReader.parse(xmlSource.getInputSource());
        return doc.getDocumentElement();
    }

    public static Node getDOMNode(Object bean) throws ParserConfigurationException, IOException, SAXException {
        String tagName = bean.getClass().getSimpleName().replace("[]", "");
        tagName = tagName.substring(0,1).toLowerCase() + tagName.substring(1);
        return getDOMNode(new SAXSource(beanAdapter, new BeanInputSource(bean, tagName)));
    }
}
