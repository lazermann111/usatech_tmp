/*
 * MapXMLLoader.java
 *
 * Created on October 17, 2003, 10:16 AM
 */

package simple.xml;

import java.util.List;
import java.util.Map;

/** Loads XML into a heirarchy of <CODE>java.util.Map</CODE> objects. An xml element's
 * attributes (its name and value) are loaded as <CODE>java.util.Map</CODE> entries (i.e. - key / value
 * pairs). Sub-elements added into a <CODE>java.util.List</CODE> which is stored in
 * the <CODE>java.util.Map</CODE> using the tag
 * name of sub-element prepended by the value in the <CODE>prefix</CODE> property
 * and appended by the value in the <CODE>suffix</CODE> property.
 * The tag of an element is entered into the
 * Map using the <CODE>tagKey</CODE> field as the entry key.
 * The character body content of an element is entered into the
 * Map using the <CODE>textKey</CODE> field as the entry key. Content broken by sub-elements is concatenated together.
 * The object returned from the load methods is
 * a <CODE>java.util.Map</CODE> object that represents the root XML element using <CODE>java.util.List</CODE> objects
 * to represent sub-elements. Each type of sub-element (i.e. - each different element tag) is stored in a separate list.
 * All sub-elements are stored in the <CODE>java.util.Map</CODE> object using the key entry "*".
 *
 * @author Brian S. Krug
 */
public class MapListXMLLoader extends XMLLoader<Map<String,Object>> {
     /** Holds value of property prefix. */
     private String prefix;

     /** Holds value of property suffix. */
     private String suffix;

     /** Holds value of property tagKey. */
     private String tagKey;

     /** Holds value of property textKey. */
     private String textKey;

	/** Creates a new MapListXMLLoader. Uses "" (empty String) as the prefix, "s" as the suffix, "tag" as the tagKey and "" as the textKey */
     public MapListXMLLoader() {
		this("", "s", "tag", "");
     }

     /** Creates a new instance of MapListXMLLoader
      * @param prefix The String prepended to a sub-element's tag that is used as the key entry for
      * the list of same tag name sub-elements.
      * @param suffix The String appended to a sub-element's tag that is used as the key entry for
      * the list of same tag name sub-elements.
      * @param tagKey The string used as the key for entering the name of the tag into the
      * Map object.
      * @param textKey The string used as the key for entering the character body content of an XML element into the
      * Map object. Content broken by sub-elements is concatenated together.
      */
     public MapListXMLLoader(String prefix, String suffix, String tagKey, String textKey) {
         this.prefix = (prefix == null ? "" : prefix);
         this.suffix = (suffix == null ? "" : suffix);
         this.tagKey = tagKey;
         this.textKey = textKey;
    }

    protected Map<String,Object> createObject(String tag, DocumentLocation location) throws XMLLoadingException {
    	Map<String,Object> m = new java.util.HashMap<String,Object>();
        m.put(getTagKey(), tag);
        return m;
    }

    protected void applyAttribute(Map<String,Object> o, String name, String value) throws XMLLoadingException {
        o.put(name, value);
    }

    protected void addTo(Map<String,Object> parent, Map<String,Object> o, String tag) throws XMLLoadingException {
        String key = prefix + tag + suffix;
        List<Map<String,Object>> list = asListOfMaps((List<?>)parent.get(key));
        if(list == null) {
            list = new java.util.LinkedList<Map<String,Object>>();
            parent.put(key, list);
        }
        list.add(o);
        // now add to all list
        key = "*";
        list = asListOfMaps((List<?>)parent.get(key));
        if(list == null) {
            list = new java.util.LinkedList<Map<String,Object>>();
            parent.put(key, list);
        }
        list.add(o);
    }
    @SuppressWarnings("unchecked")
	protected List<Map<String,Object>> asListOfMaps(List<?> list) {
    	return (List<Map<String,Object>>)list;
    }
    protected void addText(Map<String,Object> o, String text) throws XMLLoadingException {
		StringBuilder existing = (StringBuilder) o.get(getTextKey());
		if(existing == null) {
			existing = new StringBuilder();
			o.put(getTextKey(), existing);
		}
		existing.append(text);
    }

    protected void finishObject(Map<String,Object> o) throws XMLLoadingException {
    }

    /** Getter for property suffix.
     * @return Value of property suffix.
     *
     */
    public String getSuffix() {
        return this.suffix;
    }

    /** Getter for property prefix.
     * @return Value of property prefix.
     *
     */
    public String getPrefix() {
        return this.prefix;
    }

    /** Getter for property tagKey.
     * @return Value of property tagKey.
     *
     */
    public String getTagKey() {
        return this.tagKey;
    }

    /** Getter for property textKey.
     * @return Value of property textKey.
     *
     */
    public String getTextKey() {
        return this.textKey;
    }

}
