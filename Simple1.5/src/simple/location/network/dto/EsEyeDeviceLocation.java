package simple.location.network.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Device network location info DTO
 */
public class EsEyeDeviceLocation extends NetworkDeviceLocation {
    private static final long serialVersionUID = -1298809822699755967L;

    @SerializedName("status")
    private EsEyeStatus status = null;
    @SerializedName("request_id")
    private Integer requestId = null;

    public EsEyeDeviceLocation() {
    }

    public EsEyeStatus getStatus() {
        return status;
    }

    public void setStatus(EsEyeStatus status) {
        this.status = status;
    }

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }
}
