package simple.location.network.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Device network location info DTO
 */
public class NetworkDeviceLocation implements Serializable {
    private static final long serialVersionUID = 2191497236943761282L;

    @SerializedName("iccid")
    private Long iccid = null;
    @SerializedName("msisdn")
    private Long msisdn = null;
    @SerializedName("lat")
    private Float latitude = null;
    @SerializedName("lon")
    private Float longitude = null;
    @SerializedName("countryname")
    private String countryName = null;
    @SerializedName("countrycode")
    private String countryCode = null;
    @SerializedName("timezone")
    private String timezone = null;

    public NetworkDeviceLocation() {
    }

    public Long getIccid() {
        return iccid;
    }

    public void setIccid(Long iccid) {
        this.iccid = iccid;
    }

    public Long getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(Long msisdn) {
        this.msisdn = msisdn;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public String toString() {
        return "NetworkDeviceLocation{" +
                "iccid=" + iccid +
                ", msisdn=" + msisdn +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", countryName='" + countryName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", timezone='" + timezone + '\'' +
                '}';
    }
}
