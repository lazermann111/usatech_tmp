package simple.location.network.api;

public interface Request {
    void setParam(String name, String value);
    String convertToString();
}
