package simple.location.network.api;

public interface Credentials {
    void prepareRequest(Request request);
}
