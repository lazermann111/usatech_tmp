package simple.location.network.exception;

/**
 * Error while accessing the data
 */
public class NetDeviceLocationException extends RuntimeException {
    private static final long serialVersionUID = -8734813823760481305L;
    public static final String ERR_CODE_ACCESS_DENIED = "USAT-100";
    public static final String ERR_CODE_NETWORK_PROBLEM = "USAT-200";
    public static final String ERR_CODE_BAD_HTTP_RESPONSE = "USAT-300";
    public static final String ERR_CODE_UNEXPECTED_API_RESPONSE = "USAT-400";
    public static final String ERR_CODE_INTERNAL_ERROR = "USAT-10000";

    private final String errorCode;

    public NetDeviceLocationException(String errorCode) {
        this.errorCode = errorCode;
    }

    public NetDeviceLocationException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public NetDeviceLocationException(String message, Throwable cause, String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public NetDeviceLocationException(Throwable cause, String errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public NetDeviceLocationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
