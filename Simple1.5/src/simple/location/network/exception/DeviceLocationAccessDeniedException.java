package simple.location.network.exception;

/**
 * Error while accessing the data
 */
public class DeviceLocationAccessDeniedException extends NetDeviceLocationException {
    private static final long serialVersionUID = -8734813823760481306L;

    public DeviceLocationAccessDeniedException() {
        super(ERR_CODE_ACCESS_DENIED);
    }

    public DeviceLocationAccessDeniedException(String message) {
        super(message, ERR_CODE_ACCESS_DENIED);
    }

    public DeviceLocationAccessDeniedException(String message, Throwable cause) {
        super(message, cause, ERR_CODE_ACCESS_DENIED);
    }

    public DeviceLocationAccessDeniedException(Throwable cause) {
        super(cause, ERR_CODE_ACCESS_DENIED);
    }

    public DeviceLocationAccessDeniedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, ERR_CODE_ACCESS_DENIED);
    }
}
