package simple.location.network.impl;

import simple.location.network.api.Credentials;
import simple.location.network.api.Request;

public class UsernamePasswordCredentials implements Credentials {
    private String username = null;
    private String password = null;

    @Override
    public void prepareRequest(Request request) {
        request.setParam("username", username);
        request.setParam("password", password);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
