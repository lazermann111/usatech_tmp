package simple.location.network.impl;

import com.google.gson.Gson;
import simple.location.network.api.Request;
import java.util.HashMap;
import java.util.Map;

public class JsonRequest implements Request {
    final Map<String, String> params = new HashMap<>();

    @Override
    public void setParam(String name, String value) {
        params.put(name, value);
    }

    @Override
    public String convertToString() {
        Gson gson = new Gson();
        return gson.toJson(params);
    }
}
