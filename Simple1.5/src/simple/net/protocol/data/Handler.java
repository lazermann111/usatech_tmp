/*
 * Handler.java
 *
 * Created on February 25, 2004, 4:35 PM
 */

package simple.net.protocol.data;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.io.Base64DecodingInputStream;

/**
 *
 * @author  Brian S. Krug
 */
public class Handler extends java.net.URLStreamHandler {
    protected static final Pattern pattern = Pattern.compile("([^;]*)[;]([^,]*)[,](.*)", Pattern.DOTALL);
    public static class DataURLConnection extends URLConnection {
        protected String contentType;
        protected String data;
        
        protected DataURLConnection(URL url, String contentType, String data) {
            super(url);
            this.contentType = contentType;
            this.data = data;
            connected = true;
        }

        @Override
        public void connect() throws IOException {
            // nothing to do
        }
        /*
        @Override
        public Object getContent() throws IOException {
            return Base64.decode(data);
        }
        
        @Override
        public Object getContent(Class[] classes) throws IOException {
            for(Class cl : classes)
                if(String.class.isAssignableFrom(cl)) 
                    return new String((byte[])getContent());
                else if(byte[].class.isAssignableFrom(cl))
                    return getContent();
            return null;
        }
        */
        @Override
        public int getContentLength() {
            return (int)Math.ceil(data.length() * 3.0 / 4.0);
        }

        @Override
        public String getContentType() {
            return contentType;
        }

        @Override
        public String getHeaderField(int n) {
            return getHeaderField(getHeaderFieldKey(n));
        }

        @Override
        public String getHeaderField(String name) {
            if("content-type".equals(name)) return contentType;
            return super.getHeaderField(name);
        }

        @Override
        public String getHeaderFieldKey(int n) {
            switch(n) {
                case 0: return "content-type";
            }
            return super.getHeaderFieldKey(n);
        }

        @Override
        public Map<String, List<String>> getHeaderFields() {
            return Collections.singletonMap("content-type", Collections.singletonList(contentType));
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return new Base64DecodingInputStream(new ByteArrayInputStream(data.getBytes()));
        }
        
    }
    /** Creates a new instance of Handler */
    public Handler() {
    }
    
    protected URLConnection openConnection(URL url) throws IOException {
        if(url == null) throw new FileNotFoundException("Provided URL is null");
        Matcher matcher = pattern.matcher(url.getFile());
        if(!matcher.matches()) 
            throw new MalformedURLException("Invalid format for data protocol; should be data:[content-type];[encoding],[data]");
        String contentType = matcher.group(1);
        String encoding = matcher.group(2);
        String data = matcher.group(3);
        if(!"base64".equalsIgnoreCase(encoding))
            throw new IOException("'" + encoding + "' encoding is currently not supported; use 'base64'");
        return  new DataURLConnection(url, contentType, data);
    }
}
