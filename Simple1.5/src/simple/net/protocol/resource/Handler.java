/*
 * Handler.java
 *
 * Created on February 25, 2004, 4:35 PM
 */

package simple.net.protocol.resource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author  Brian S. Krug
 */
public class Handler extends java.net.URLStreamHandler {
    
    /** Creates a new instance of Handler */
    public Handler() {
    }
    
    protected URLConnection openConnection(URL url) throws IOException {
        if(url == null) throw new FileNotFoundException("Provided URL is null");
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        if(cl == null) {
            cl = getClass().getClassLoader();
            if(cl == null) cl= ClassLoader.getSystemClassLoader();
        }
        String name = url.getFile();
        int start = 0;
        while(name.charAt(start) == '/') start++;
        if(start > 0) name = name.substring(start);
        URL realURL = cl.getResource(name);
        if(realURL == null) throw new FileNotFoundException("Could not find resource '" + name + "' in the classpath");
        return realURL.openConnection();
    }
    
}
