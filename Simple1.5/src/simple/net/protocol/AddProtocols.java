/*
 * AddProtocols.java
 *
 * Created on May 26, 2004, 10:36 AM
 */

package simple.net.protocol;

import java.net.URL;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicBoolean;

import simple.io.Log;

/** Adds the package of this class to the list of protocol handler packages for processing URL protocols
 *
 * @author  Brian S. Krug
 */
public class AddProtocols {
	protected static final AtomicBoolean initialized = new AtomicBoolean();
	public static final String SYSTEM_PROPRETY_PROTOCOL_PKGS = "java.protocol.handler.pkgs";
    protected static final String PACKAGE_NAME = AddProtocols.class.getPackage().getName();
    private static final class LazyHolder {
    	protected static final Log log = Log.getLog();
    }
    public static void run() {
    	if(initialized.compareAndSet(false, true)) {
    		setSystemProperty();
        /*
        log.debug("My ClassLoader = " + formatClassLoaderString(AddProtocols.class.getClassLoader()));
        log.debug("URL's ClassLoader = " + formatClassLoaderString(URL.class.getClassLoader()));
        boolean sameClassLoader = AddProtocols.class.getClassLoader() == ClassLoader.getSystemClassLoader();
        try {
            for(ClassLoader cl = URL.class.getClassLoader(); !sameClassLoader && cl != null; cl = cl.getParent()) {
                sameClassLoader = AddProtocols.class.getClassLoader() == cl;
            }
        } catch(SecurityException e) {
            // leave sameClassLoader as false
        } catch(RuntimeException e) {
            // leave sameClassLoader as false
        }
        if(!sameClassLoader) setURLStreamHandlerFactory();
        */
    	}
    }

    protected static void setSystemProperty() {
        String pkgs = System.getProperty(SYSTEM_PROPRETY_PROTOCOL_PKGS);
        if(pkgs == null || pkgs.trim().length() == 0) pkgs = PACKAGE_NAME;
        else if(!("|" + pkgs + "|").contains("|" + PACKAGE_NAME + "|")) pkgs = PACKAGE_NAME + "|" + pkgs;
        else {
        	LazyHolder.log.debug("protocol handler package already set: " + pkgs);
            return;
        }
        LazyHolder.log.debug("Setting protocol handler package to: " + pkgs);
        System.setProperty(SYSTEM_PROPRETY_PROTOCOL_PKGS, pkgs);
    }

    protected static String formatClassLoaderString(ClassLoader cl) {
        String s = "";
        for(; cl != null; cl = cl.getParent()) {
            s += cl + "-->";
        }
        return s;
    }
    protected static void setURLStreamHandlerFactory() {
        try {
            // basically this is copied from URL.getURLStreamHandler() but this finds the handlers using
            // the classloader of this(AddProtocols) class rather than the loader of the URL class
            URL.setURLStreamHandlerFactory(new URLStreamHandlerFactory() {
                protected final Map<String, URLStreamHandler> cache = new HashMap<String, URLStreamHandler>();
                public URLStreamHandler createURLStreamHandler(String protocol) {
                    URLStreamHandler handler = cache.get(protocol);
                    if(handler == null) {
                        String pkgs = System.getProperty(SYSTEM_PROPRETY_PROTOCOL_PKGS);
                        if(pkgs == null || pkgs.trim().length() == 0) pkgs = "";
                        else pkgs += "|";
                        pkgs += "sun.net.www.protocol";
                        StringTokenizer tokens =  new StringTokenizer(pkgs, "|");
                        while(handler == null && tokens.hasMoreTokens()) {
                            String packagePrefix = tokens.nextToken().trim();
                            try {
                                String clsName = packagePrefix + "." + protocol + ".Handler";
                                Class<?> cls = findClass(clsName);
                                if (cls != null) {
                                    handler = (URLStreamHandler)cls.newInstance();
                                }
                            } catch (Exception e) {
                            // any number of exceptions can get thrown here
                            }
                        }
                    }
                    return handler;
                }
            });
            LazyHolder.log.debug("Set url stream handler factory to custom factory");
        } catch(Error e) {
            //ignore -- already been set
        	LazyHolder.log.debug("url stream handler factory already set");
        }
    }

    protected static Class<?> findClass(String name) throws ClassNotFoundException {
        Class<?> cls = null;
        try {
            cls = Class.forName(name);
        } catch (ClassNotFoundException e) {
            ClassLoader cl = ClassLoader.getSystemClassLoader();
            if (cl != null) {
                cls = cl.loadClass(name);
            }
        }
        return cls;
    }
}
