package simple.net.http;

import simple.io.Log;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Set;

/**
 * HTTP Client to post requests and receive response over the java default URL Connection that is pooled itself
 *
 * @author dlozenko
 */
public class UrlConnectionHttpClient {
    private static final Log log = Log.getLog();
    private final String baseUrl;
    private volatile int notFailHttpCode = 0;

    public UrlConnectionHttpClient(@Nonnull String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void setNotFailHttpCode(int notFailHttpCode) {
        this.notFailHttpCode = notFailHttpCode;
    }

    /**
     * Do POST JSON request and returns JSON response
     *
     * @param httpMethodName HTTP method name. Ie. POST, GET, DELETE,...
     * @param apiCallName    function name on remote. For example login
     * @param body           Request body
     * @param cookies        Cookies to be set to HTTP request
     * @param headers        Headers to be added to request
     *
     * @return               Response as string
     * @throws HttpClientException  on unsuccessful call
     */
    @Nonnull
    private String process(@Nonnull String httpMethodName, @Nonnull String apiCallName, @Nullable String body, @Nullable String cookies, @Nullable Map<String, String> headers) throws HttpClientException {
        log.info("STARTING API CALL: " + httpMethodName + ": " + baseUrl + apiCallName +
                ", BODY: " + (body == null ? "<absent>" : body) +
                ", COOKIES: " + (cookies == null ? "<absent>" : cookies) +
                ", HEADERS = " + (headers == null ? "<absent>" : headers.toString()));
        StringBuilder rv = new StringBuilder("");
        HttpURLConnection conn;
        BufferedReader br = null;
        try {
            URL url = new URL(baseUrl + apiCallName);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod(httpMethodName);
            if (headers == null) {
                conn.setRequestProperty("Content-Type", "application/json");
                if (cookies != null) {
                    conn.setRequestProperty("Cookie", cookies);
                }
            } else {
                Set<String> headerKeys = headers.keySet();
                for (String headerKey : headerKeys) {
                    conn.setRequestProperty(headerKey, headers.get(headerKey));
                }
                if (!headers.containsKey("Content-Type")) {
                    conn.setRequestProperty("Content-Type", "application/json");
                }
                if (cookies != null && !headers.containsKey("Cookie")) {
                    conn.setRequestProperty("Cookie", cookies);
                }
            }
            OutputStream os = conn.getOutputStream();
            os.write(body == null ? "".getBytes() : body.getBytes());
            os.flush();
            if (conn.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
            	br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            } else {
            	br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
            }
            String output;
            while ((output = br.readLine()) != null) {
                if (rv.length() > 0) {
                    rv.append("\n");
                }
                rv.append(output);
            }
            if (conn.getResponseCode() != notFailHttpCode && (conn.getResponseCode() < 200 || conn.getResponseCode() > 207)) {
                throw new HttpClientException("Not successful HTTP response. CODE : " + conn.getResponseCode() +
                        ", MESSAGE: " + conn.getResponseMessage() + ", BODY: " + rv.toString());
            }
        } catch (IOException e) {
            throw new HttpClientException("I/O problem while connecting to API", e);
        }
        finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ignored) {
                }
            }
            // Commented to allow Java to use connection pooling when call to openConnection(..)
            //if (conn != null) {
            //conn.disconnect();
            //}
        }
        log.info("FINISHED API CALL: " + httpMethodName + ": " + baseUrl + apiCallName + ", RESULT: " + rv.toString());
        return rv.toString();
    }

    /**
     * Do POST JSON request and returns JSON response
     *
     * @param apiCallName    function name on remote. For example login
     * @param body           Request body
     * @param cookies        Cookies to be set to HTTP request
     *
     * @return               Response as string
     * @throws HttpClientException  on unsuccessful call
     */
    @Nonnull
    public String post(@Nonnull String apiCallName, @Nullable String body, @Nullable String cookies) throws HttpClientException {
        return process("POST", apiCallName, body, cookies, null);
    }

    /**
     * Do POST JSON request and returns JSON response
     *
     * @param apiCallName    function name on remote. For example login
     * @param body           Request body
     * @param headers        Headers to be added to request
     *
     * @return               Response as string
     * @throws HttpClientException  on unsuccessful call
     */
    @Nonnull
    public String post(@Nonnull String apiCallName, @Nullable String body, @Nullable Map<String, String> headers) throws HttpClientException {
        return process("POST", apiCallName, body, null, headers);
    }

    /**
     * Do POST JSON request and returns JSON response
     *
     * @param apiCallName    function name on remote. For example login
     * @param headers        Headers to be added to request
     *
     * @return               Response as string
     * @throws HttpClientException  on unsuccessful call
     */
    @Nonnull
    public String post(@Nonnull String apiCallName, @Nullable Map<String, String> headers) throws HttpClientException {
        return process("POST", apiCallName, null, null, headers);
    }

    /**
     * Do POST JSON request and returns JSON response
     *
     * @param apiCallName    function name on remote. For example login
     * @param body           Request body
     *
     * @return               Response as string
     * @throws HttpClientException  on unsuccessful call
     */
    @Nonnull
    public String post(@Nonnull String apiCallName, @Nullable String body) throws HttpClientException {
        return process("POST", apiCallName, body, null, null);
    }

    /**
     * Do POST JSON request and returns JSON response
     *
     * @param apiCallName    function name on remote. For example login
     *
     * @return               Response as string
     * @throws HttpClientException  on unsuccessful call
     */
    @Nonnull
    public String post(@Nonnull String apiCallName) throws HttpClientException {
        return process("POST", apiCallName, null, null, null);
    }

    /**
     * Do DELETE request and returns JSON response
     *
     * @param apiCallName    function name on remote. For example login
     * @param headers        Headers to be added to request
     *
     * @return               Response as string
     * @throws HttpClientException  on unsuccessful call
     */
    @Nonnull
    public String delete(@Nonnull String apiCallName, @Nullable Map<String, String> headers) throws HttpClientException {
        return process("DELETE", apiCallName, null, null, headers);
    }
}
