package simple.net.http;

/**
 * Inform about unsuccessful network API call
 *
 * @author dlozenko
 */
public class HttpClientException extends Exception {
    private static final long serialVersionUID = -769255551182573756L;

    public HttpClientException() {
    }

    public HttpClientException(String message) {
        super(message);
    }

    public HttpClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpClientException(Throwable cause) {
        super(cause);
    }

    public HttpClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
