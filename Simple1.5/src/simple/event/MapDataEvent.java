package simple.event;

public class MapDataEvent extends java.util.EventObject {
	private static final long serialVersionUID = 2034592095205L;
	private java.lang.Object fieldKey = new Object();
	private java.lang.Object fieldValue = new Object();
	private java.lang.Object fieldOldValue = new Object();
	private int fieldId;

	public static final int KEY_ADDED = 2001;
	public static final int KEY_REMOVED = 2002;
	public static final int VALUE_REPLACED = 2003;
	public static final int KEY_MODIFIED = 2004;
	public static final int VALUE_MODIFIED = 2005;
/**
 * MapDataEvent constructor comment.
 * @param source java.lang.Object
 */
public MapDataEvent(Object source, int id, Object key, Object value, Object oldValue) {
	super(source);
	fieldId = id;
	fieldKey = key;
	fieldValue = value;
	fieldOldValue = oldValue;
}
/**
 * Gets the id property (int) value.
 * @return The id property value.
 */
public int getId() {
	return fieldId;
}
/**
 * Gets the key property (java.lang.Object) value.
 * @return The key property value.
 */
public java.lang.Object getKey() {
	return fieldKey;
}
/**
 * Gets the oldValue property (java.lang.Object) value.
 * @return The oldValue property value.
 */
public java.lang.Object getOldValue() {
	return fieldOldValue;
}
/**
 * Gets the value property (java.lang.Object) value.
 * @return The value property value.
 */
public java.lang.Object getValue() {
	return fieldValue;
}
/**
 * Gets the value property (java.lang.Object) value.
 * @return The value property value.
 */
public void setValue(Object value) {
	fieldValue = value;
}
}
