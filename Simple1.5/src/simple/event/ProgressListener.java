package simple.event;

import java.util.EventListener;

public interface ProgressListener extends EventListener {
	public void progressFinish(ProgressEvent event) ;
	public void progressStart(ProgressEvent event) ;
	public void progressUpdate(ProgressEvent event) ;
}
