package simple.event;

public interface TaskListener {
	public void taskStarted(String taskName, String taskDetails) ;
	public void taskEnded(String taskName) ;	
	public void taskRan(String taskName, String taskDetails, long startTime, long endTime) ;	
	public void flush() ;
}
