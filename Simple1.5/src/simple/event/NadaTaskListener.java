package simple.event;

public class NadaTaskListener implements TaskListener {
	@Override
	public void taskStarted(String taskName, String taskDetails) {
	}

	@Override
	public void taskEnded(String taskName) {
	}

	@Override
	public void taskRan(String taskName, String taskDetails, long startTime, long endTime) {
	}

	@Override
	public void flush() {
	}
}
