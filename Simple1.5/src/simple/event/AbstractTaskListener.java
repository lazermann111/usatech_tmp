package simple.event;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractTaskListener implements TaskListener {
	protected int taskTimeout = 60*60*1000;
	protected static class Task {
		public long threadId;
		public String threadName;
		public String taskName;
		public String taskDetails;
		public long startTime;
	}
	protected class ThreadInfo {
		protected final long threadId;
		protected final String threadName;
		protected ThreadInfo(long threadId, String threadName) {
			this.threadId = threadId;
			this.threadName = threadName;
		}
		protected final Map<String,Task> runningTasks = new LinkedHashMap<String, Task>(100, 0.75f, true) {
			private static final long serialVersionUID = -948001234L;

			@Override
			protected boolean removeEldestEntry(Map.Entry<String, Task> eldest) {
				long start = eldest.getValue().startTime;
				return start + taskTimeout > System.currentTimeMillis();
			}
		};
	}
	
	protected final ThreadLocal<ThreadInfo> threadInfoLocal = new ThreadLocal<ThreadInfo>() {
		@Override
		protected ThreadInfo initialValue() {
			Thread t = Thread.currentThread();
			return new ThreadInfo(t.getId(), t.getName());
		}
	};
	
	public AbstractTaskListener() {
		super();
	}

	public void taskStarted(String taskName, String taskDetails) {
		ThreadInfo ti = threadInfoLocal.get();
		Task task = new Task();
		task.threadId = ti.threadId;
		task.threadName = ti.threadName;
		task.taskName = taskName;
		task.taskDetails = taskDetails;
		ti.runningTasks.put(task.taskName, task);		
		task.startTime = System.currentTimeMillis();
	}
	
	public void taskEnded(String taskName) {
		long endTime = System.currentTimeMillis();
		ThreadInfo ti = threadInfoLocal.get();
		Task task = ti.runningTasks.remove(taskName);
		taskRan(task.taskName, task.taskDetails, task.threadId, task.threadName, task.startTime, endTime);
	}

	public void taskRan(String taskName, String taskDetails, long startTime, long endTime) {
		Thread t = Thread.currentThread();
		taskRan(taskName, taskDetails, t.getId(), t.getName(), startTime, endTime);
	}

	protected abstract void taskRan(String taskName, String taskDetails, long threadId, String threadName, long startTime, long endTime) ;
}
