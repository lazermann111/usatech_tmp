package simple.event;

import simple.db.BasicBatchRecorder;

public class ExecuteCallTaskListener extends AbstractTaskListener {
	protected final BasicBatchRecorder<TaskInputs> recorder = new BasicBatchRecorder<TaskInputs>();
	protected String appName;
	protected class TaskInputs {
    	protected String taskName;
    	protected String taskDetails;
    	protected long threadId;
    	protected String threadName;
    	protected long startTime;
    	protected long endTime;
    	public TaskInputs() {
    	}
		public long getEndTime() {
			return endTime;
		}
		public long getStartTime() {
			return startTime;
		}
		public String getTaskDetails() {
			return taskDetails;
		}
		public String getTaskName() {
			return taskName;
		}
		public long getThreadId() {
			return threadId;
		}
		public String getThreadName() {
			return threadName;
		}
		public void setEndTime(long endTime) {
			this.endTime = endTime;
		}
		public void setStartTime(long startTime) {
			this.startTime = startTime;
		}
		public void setTaskDetails(String taskDetails) {
			this.taskDetails = taskDetails;
		}
		public void setTaskName(String taskName) {
			this.taskName = taskName;
		}
		public void setThreadId(long threadId) {
			this.threadId = threadId;
		}
		public void setThreadName(String threadName) {
			this.threadName = threadName;
		}
		public String getAppName() {
			return appName;
		}
    }

	public ExecuteCallTaskListener() {
	}

	public ExecuteCallTaskListener(String callId, int batchSize, boolean asynchronous) {
		super();
		setCallId(callId);
		setBatchSize(batchSize);
		setAsynchronous(asynchronous);
	}


	@Override
	protected void taskRan(String taskName, String taskDetails, long threadId, String threadName, long startTime, long endTime) {
		TaskInputs taskInputs = new TaskInputs();
		taskInputs.setEndTime(endTime);
		taskInputs.setStartTime(startTime);
		taskInputs.setTaskDetails(taskDetails);
		taskInputs.setTaskName(taskName);
		taskInputs.setThreadId(threadId);
		taskInputs.setThreadName(threadName);
		recorder.record(taskInputs);
    }

	@Override
	protected void finalize() throws Throwable {
		recorder.finish();
		super.finalize();
	}

	public void flush() {
		recorder.flush();
	}

	public int getBatchSize() {
		return recorder.getBatchSize();
	}

	public String getCallId() {
		return recorder.getCallId();
	}

	public int getPollFrequency() {
		return recorder.getPollFrequency();
	}
	public boolean isUseThreadForExecute() {
		return isAsynchronous();
	}
	public void setUseThreadForExecute(boolean useThreadForExecute) {
		setAsynchronous(useThreadForExecute);
	}
	public boolean isAsynchronous() {
		return recorder.isAsynchronous();
	}

	public void setAsynchronous(boolean asynchronous) {
		recorder.setAsynchronous(asynchronous);
	}

	public void setBatchSize(int batchSize) {
		recorder.setBatchSize(batchSize);
	}

	public void setCallId(String callId) {
		recorder.setCallId(callId);
	}

	public void setPollFrequency(int pollFrequency) {
		recorder.setPollFrequency(pollFrequency);
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}
}
