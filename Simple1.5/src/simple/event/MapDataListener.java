package simple.event;

public interface MapDataListener extends java.util.EventListener {
/**
 * Insert the method's description here.
 * Creation date: (6/13/01 10:31:43 AM)
 * @param event simple.event.MapDataEvent
 */
void keyAdded(MapDataEvent event);
/**
 * Insert the method's description here.
 * Creation date: (6/13/01 10:31:43 AM)
 * @param event simple.event.MapDataEvent
 */
void keyModified(MapDataEvent event);
/**
 * Insert the method's description here.
 * Creation date: (6/13/01 10:31:43 AM)
 * @param event simple.event.MapDataEvent
 */
void keyRemoved(MapDataEvent event);
/**
 * Insert the method's description here.
 * Creation date: (6/13/01 10:31:43 AM)
 * @param event simple.event.MapDataEvent
 */
void valueModified(MapDataEvent event);
/**
 * Insert the method's description here.
 * Creation date: (6/13/01 10:31:43 AM)
 * @param event simple.event.MapDataEvent
 */
void valueReplaced(MapDataEvent event);
}
