package simple.event;

import simple.io.Log;
import simple.io.logging.SimpleBridge;

public class LoggingTaskListener extends AbstractTaskListener {
	private static final Log log = Log.getLog();
	protected int logLevel = SimpleBridge.INFO;

	@Override
	public void flush() {
	}

	@Override
	protected void taskRan(String taskName, String taskDetails, long threadId, String threadName, long startTime, long endTime) {
		StringBuilder sb = new StringBuilder();
		sb.append(taskName).append(" - ").append(taskDetails).append("[in ").append(endTime - startTime).append(" ms]");
		log(sb.toString());
	}

	protected void log(String message) {
		if(getLogLevel() >= SimpleBridge.FATAL)
			log.fatal(message);
		else if(getLogLevel() >= SimpleBridge.ERROR)
			log.error(message);
		else if(getLogLevel() >= SimpleBridge.WARN)
			log.warn(message);
		else if(getLogLevel() >= SimpleBridge.INFO)
			log.info(message);
		else if(getLogLevel() >= SimpleBridge.DEBUG)
			log.debug(message);
		else if(getLogLevel() >= SimpleBridge.TRACE)
			log.trace(message);
	}

	public int getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(int logLevel) {
		this.logLevel = logLevel;
	}
}
