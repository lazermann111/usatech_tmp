/**
 *
 */
package simple.event;

import java.util.EventListener;

/**
 * @author Brian S. Krug
 *
 */
public interface RunnableListener extends EventListener {
	public void runCompleted() ;
	public void runFailed(Throwable throwable) ;
}
