package simple.event;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import simple.io.Log;

public class LogTimingTaskListener implements TaskListener {
	protected final Log log;
	protected static interface Timing {
		public void setStartTime(long startTime);

		public void setEndTime(long endTime);

		public long getAverageTime();

		public long getTotalTime();

		public int getCount();

		public void clear();
	}

	protected static class SimpleTiming implements Timing {
		protected long totalTime;
		protected long startTime;
		protected int count;
		protected boolean within;
		@Override
		public void setStartTime(long startTime) {
			this.startTime = startTime;
			within = true;
		}

		@Override
		public void setEndTime(long endTime) {
			if(within) {
				long diff = endTime - startTime;
				within = false;
				if(diff < 0)
					return;
				count++;
				totalTime += diff;

			}
		}

		@Override
		public long getAverageTime() {
			return getTotalTime() / getCount();
		}

		@Override
		public void clear() {
			totalTime = 0L;
			count = 0;
			within = false;
		}

		@Override
		public long getTotalTime() {
			return totalTime;
		}

		@Override
		public int getCount() {
			return count;
		}

	}
	protected final Map<String, Timing> timings;

	public LogTimingTaskListener(Log log) {
		this(log, new LinkedHashMap<String, Timing>());
	}

	public LogTimingTaskListener(Log log, Map<String, Timing> timings) {
		this.log = log;
		this.timings = timings;
	}

	protected Timing getOrCreateTiming(String taskName) {
		Timing timing = timings.get(taskName);
		if(timing == null) {
			timing = new SimpleTiming();
			timings.put(taskName, timing);
		}
		return timing;
	}
	@Override
	public void taskStarted(String taskName, String taskDetails) {
		Timing timing = getOrCreateTiming(taskName);
		timing.setStartTime(System.nanoTime());
	}

	@Override
	public void taskEnded(String taskName) {
		Timing timing = getOrCreateTiming(taskName);
		timing.setEndTime(System.nanoTime());
	}

	@Override
	public void taskRan(String taskName, String taskDetails, long startTime, long endTime) {
		Timing timing = getOrCreateTiming(taskName);
		timing.setStartTime(TimeUnit.MILLISECONDS.toNanos(startTime));
		timing.setEndTime(TimeUnit.MILLISECONDS.toNanos(endTime));
	}

	@Override
	public void flush() {
		for(Map.Entry<String,Timing> entry : timings.entrySet()) {
			log.info("Timing of '" + entry.getKey() + "' over " + entry.getValue().getCount() + " iterations: " + entry.getValue().getTotalTime() + " ns");
			entry.getValue().clear();
		}
	}

}
