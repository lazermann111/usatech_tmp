package simple.event;

public class ProgressEvent {
	protected Float statusPercent;
	protected int statusProgress;
	protected String statusDescription;
	public ProgressEvent() {
	}
	public ProgressEvent(int statusProgress, Float statusPercent, String statusDescription) {
		super();
		this.statusProgress = statusProgress;
		this.statusPercent = statusPercent;
		this.statusDescription = statusDescription;
	}
	public Float getStatusPercent() {
		return statusPercent;
	}
	public void setStatusPercent(Float statusPercent) {
		this.statusPercent = statusPercent;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public int getStatusProgress() {
		return statusProgress;
	}
	public void setStatusProgress(int statusProgress) {
		this.statusProgress = statusProgress;
	}	
}
