package simple.event;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import simple.text.StringUtils;

public class WriteCSVTaskListener extends AbstractTaskListener {
	protected static final String HEADER_LINE = "Task,Details,Thread,Start Time,Duration";
	protected PrintWriter out;

	public WriteCSVTaskListener() {
	}
	public WriteCSVTaskListener(String fileName) throws FileNotFoundException {
		this(new PrintWriter(fileName), true);
	}

	public WriteCSVTaskListener(PrintWriter out) {
		this(out, true);
	}

	public WriteCSVTaskListener(PrintWriter out, boolean header) {
		super();
		this.out = out;
		if(header) {
			out.println(HEADER_LINE);
		}
	}

	@Override
	protected void taskRan(String taskName, String taskDetails, long threadId, String threadName, long startTime, long endTime) {
		StringBuilder sb = new StringBuilder();
		sb.append('"').append(StringUtils.escapeCSVValue(taskName)).append("\",\"")
			.append(StringUtils.escapeCSVValue(taskDetails)).append("\",\"")
			.append(StringUtils.escapeCSVValue(threadName)).append("\",")
			.append(startTime).append(',').append(endTime-startTime);
		out.println(sb);
	}

	public void flush() {
		out.flush();
	}

	public PrintWriter getOut() {
		return out;
	}

	public void setOut(PrintWriter out) {
		this.out = out;
	}

	public void setFileName(String fileName) throws FileNotFoundException {
		setOut(new PrintWriter(fileName));
	}
}
