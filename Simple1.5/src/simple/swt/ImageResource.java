package simple.swt;

import java.awt.Image;
import java.io.IOException;
import java.util.Map;

import simple.bean.ConvertUtils;
import simple.bean.ReflectionUtils;

/**
 * File:     	  <class>
 * Package:  <package>
 * Author:      Brian S. Krug
 * Description:
 *
 *
 * Modified By     Date     Reason/Action
 *   -----------     ----     ------
 */
public class ImageResource {
    private Image image = null;
    private java.lang.String fieldImagePath = null;
    private static Map<String,Image> loadedImages = new java.util.HashMap<String,Image>(100);
    private static Map<Image,String> imageToPath = new java.util.HashMap<Image,String>(100);
    
    static {
        ImageConverter ic = new ImageConverter();
        ConvertUtils.register(ic);
        //ConvertUtils.register(ic, ImageResource.class);
    }
    /**
     * ImageResource constructor comment.
     */
    public ImageResource() {
        super();
    }
    /**
     * ImageResource constructor comment.
     */
    public ImageResource(String imagePath) {
        super();
        fieldImagePath = imagePath;
    }
    /**
     * Insert the method's description here.
     * Creation date: (7/20/00 11:01:26 AM)
     * @return java.awt.Image
     */
    public Image getImage() throws IOException {
        if (image == null) {
            image = getImageForPath(getImagePath());
        }
        return image;
    }
    /**
     * Insert the method's description here.
     * Creation date: (7/20/00 11:01:26 AM)
     * @return java.awt.Image
     */
    public static Image getImageForPath(String path) throws IOException {
        Image img = null;
        if (path != null && path.length() > 0) {
            img = loadedImages.get(path);
            if(img == null) {
                        /*
                                Image img = null;
        if (cache != null) {
            if ((img = (Image) cache.get(name)) != null) {
                return img;
            }
        }
                         
        URLClassLoader urlLoader = (URLClassLoader)cmp.getClass().getClassLoader();
        URL fileLoc = urlLoader.findResource("images/" + name);
        img = cmp.getToolkit().createImage(fileLoc);
                         
        MediaTracker tracker = new MediaTracker(cmp);
        tracker.addImage(img, 0);
        try {
            tracker.waitForID(0);
            if (tracker.isErrorAny()) {
                System.out.println("Error loading image " + name);
            }
        } catch (Exception ex) { ex.printStackTrace(); }
        return img;
    }
                         
                         */
            	java.net.URL url = null;
            	ClassLoader cl = Thread.currentThread().getContextClassLoader();
            	if(cl != null)
            		url = cl.getResource(path);
                if(url == null) {
                	url = ImageResource.class.getClassLoader().getResource(path);
                	if(url == null) {
                		url = ClassLoader.getSystemResource(path);
                		if(url == null) throw new IOException("Could not find image at path '" + path + "'");			
                    }
                }
                img = java.awt.Toolkit.getDefaultToolkit().createImage(url);
                loadedImages.put(path,img);
                imageToPath.put(img,path);
            }
        }
        return img;
    }
    /**
     * Gets the imagePath property (java.lang.String) value.
     * @return The imagePath property value.
     * @see #setImagePath
     */
    public java.lang.String getImagePath() {
        return fieldImagePath;
    }
    /**
     * Gets the imagePath property (java.lang.String) value.
     * @return The imagePath property value.
     * @see #setImagePath
     */
    public static java.lang.String getPathForImage(java.awt.Image image) {
        if(image == null) return null;
        return imageToPath.get(image);
    }
    /**
     * Sets the imagePath property (java.lang.String) value.
     * @param imagePath The new value for the property.
     * @see #getImagePath
     */
    public void setImagePath(java.lang.String imagePath) {
        if(image != null && (imagePath == null || (imagePath != null && imagePath.equalsIgnoreCase(fieldImagePath)))) {
            image = null;
        }
        fieldImagePath = imagePath;
    }
    /**
     * Returns a String that represents the value of this object.
     * @return a string representation of the receiver
     */
    public String toString() {
        return getImagePath();
    }
}
