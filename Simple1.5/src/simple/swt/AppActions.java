/*
 * AppActions.java
 *
 * Created on June 23, 2003, 2:04 PM
 */

package simple.swt;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import simple.bean.ConvertUtils;

/**
 * Provides one place to register and retrieve application actions
 *
 * @author  Brian S. Krug
 */
public class AppActions {
    protected static final Map actions = new HashMap();
    protected static final Map components = new HashMap();
    protected static final Map groups = new HashMap();
    protected static final java.beans.PropertyChangeListener commandChangeListener = 
        new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                if(evt.getSource() instanceof AbstractButton) {
                    AbstractButton but = (AbstractButton) evt.getSource();
                    if(evt.getPropertyName().equals("actionCommand")) {
                        actionCommandChanged((String)evt.getOldValue(), 
                            (String)evt.getNewValue(), but);
                    }
                } else if(evt.getSource() instanceof Action) {
                    Action a = (Action) evt.getSource();
                    if(evt.getPropertyName().equals(Action.ACTION_COMMAND_KEY)) {
                        actionKeyChanged(evt.getOldValue(), evt.getNewValue(), a);
                    } else if(evt.getPropertyName().equals(AppAction.ACTION_GROUP)) {
                        actionGroupChanged(evt.getOldValue(), evt.getNewValue(), a);
                    }
                } 
            }
    };
    
    /** Creates a new instance of AppActions */
    protected AppActions() {}
        
    public static void addAction(String command, Action action) {
        action.putValue(Action.ACTION_COMMAND_KEY, command);
        addAction(action);
    }
    
    public static void addAction(Action action) {  
        //lets assume the action's command key will not change
        action.addPropertyChangeListener(commandChangeListener);
        putAction(action);
        addActionToGroup(action);
    }
    
    protected static void putAction(Action action) {
        Object command = action.getValue(Action.ACTION_COMMAND_KEY);
        actions.put(command, action);
        //notify dependent components
        Collection comps = (Collection)components.get(command);
        if(comps == null) return;
        for(Iterator iter = comps.iterator(); iter.hasNext(); ) {
            AbstractButton button = (AbstractButton)iter.next();
            if(!ConvertUtils.areEqual(button.getAction(), action))
                button.setAction(action);
        }              
    }
    
    protected static void addActionToGroup(Action action) {
        Object group = action.getValue(AppAction.ACTION_GROUP);
        if(group != null) {
            Collection members = (Collection)groups.get(group);
            if(members == null) {
                members = new java.util.HashSet();
                groups.put(group, members);
            }
            members.add(action);
        }
    }
    
    public static void addButton(AbstractButton button) {
        putButton(button);
        //listen for changes
        button.addPropertyChangeListener(commandChangeListener);
    }
    
    protected static void putButton(AbstractButton button) {
        String command = button.getActionCommand();
        Collection comps = (Collection)components.get(command);
        if(comps == null) {
            comps = new simple.util.LinkedHashSet();
            components.put(command, comps);
        }
        comps.add(button);
        Action action = getAction(command);
        if(!ConvertUtils.areEqual(button.getAction(), action))
            button.setAction(action);
    }
    
    protected static void actionCommandChanged(String oldCommand, String newCommand, AbstractButton button) {
       if(!ConvertUtils.areEqual(newCommand,button.getActionCommand())) {
           button.setActionCommand(newCommand);
       }
       if(!ConvertUtils.areEqual(oldCommand,button.getActionCommand())) {
           Collection comps = (Collection)components.get(oldCommand);
           if(comps != null) comps.remove(button);
           putButton(button);         
       }
    }
    
    protected static void actionGroupChanged(Object oldGroup, Object newGroup, Action action) {
        if(!ConvertUtils.areEqual(oldGroup,newGroup)) {
           Collection members = (Collection)groups.get(oldGroup);
           if(members != null) members.remove(action);
           addActionToGroup(action);
        }
    }
    
    protected static void actionKeyChanged(Object oldCommand, Object newCommand, Action action) {
       if(!ConvertUtils.areEqual(oldCommand,newCommand)) {
           actions.remove(oldCommand);       
           Collection comps = (Collection)components.remove(oldCommand);
           if(comps != null) {
                for(Iterator iter = comps.iterator(); iter.hasNext(); ) {
                    AbstractButton button = (AbstractButton)iter.next();
                    if(ConvertUtils.areEqual(button.getActionCommand(), oldCommand))
                        button.setAction(null);
                    else
                        button.setAction(getAction(button.getActionCommand()));
                }  
           }
           putAction(action);         
       }
    }
    
    public static Action getAction(Object command) {
        return (Action) actions.get(command);
    }
    
    public static javax.swing.JButton createButton() {
        return new AppButton();
    }
    
    public static JMenu createMenu(String label, Object[] commands, boolean tearOff) {
        JMenu menu = new JMenu(label, tearOff);
        for(int i = 0; i < commands.length; i++) {
            if(commands[i] != null) {
                javax.swing.JMenuItem mi = new AppMenuItem();
                addButton(mi);
                mi.setActionCommand(asString(commands[i]));
                menu.add(mi);
            } else menu.addSeparator();
        }
        return menu;
    }

    public static void addMenu(JMenuBar bar, String label, Object[] commands, boolean tearOff) {
        bar.add(createMenu(label, commands, tearOff));
    }
    
    public static void addMenu(javax.swing.JFrame frame, String label, Object[] commands, boolean tearOff) {
        JMenuBar bar = frame.getJMenuBar();
        if(bar == null) {
            bar = new JMenuBar();
            frame.setJMenuBar(bar);
        }
        addMenu(bar,label, commands, tearOff); 
    }
    
    public static void checkEnabled(Object command) {
        Action action = getAction(command);
        if(action instanceof AppAction) ((AppAction)action).checkEnabled();
    }
    
    public static void checkGroupEnabled(Object group) {
        Collection members = (Collection)groups.get(group);
        if(members != null) {
            for(Iterator iter = members.iterator(); iter.hasNext(); ) {
                Action action = (Action)iter.next();
                if(action instanceof AppAction) ((AppAction)action).checkEnabled();
            }
        }
    }

    public static void fireAction(Object source, Object command) {
        Action action = getAction(command);
        if(action != null) action.actionPerformed(
            new java.awt.event.ActionEvent(source, java.awt.event.ActionEvent.ACTION_PERFORMED, asString(command)));
    }
    
    protected static String asString(Object command) {
        return (command == null ? null : command.toString());
    }
}
