/**
 * 
 */
package simple.swt;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;

public class FixedDefaultCellEditor extends DefaultCellEditor {
	private static final long serialVersionUID = 247214071549384L;

	public FixedDefaultCellEditor(final JComboBox comboBox) {
		super(comboBox);
		delegate = new EditorDelegate() {
			private static final long serialVersionUID = 21411114071549384L;
			public void setValue(Object value) {
				comboBox.setSelectedItem(value);
				comboBox.getEditor().setItem(value);
			}

			public Object getCellEditorValue() {
				return comboBox.getSelectedItem();
			}

			public boolean shouldSelectCell(EventObject anEvent) { 
				if (anEvent instanceof MouseEvent) { 
					MouseEvent e = (MouseEvent)anEvent;
					return e.getID() != MouseEvent.MOUSE_DRAGGED;
				}
				return true;
			}

			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getActionCommand().equals("comboBoxEdited") && comboBox.isEditable()) {
					comboBox.getModel().setSelectedItem(comboBox.getEditor().getItem());
					comboBox.setPopupVisible(false);
				}
				super.actionPerformed(e);
			}	        
		};		
	}		

}