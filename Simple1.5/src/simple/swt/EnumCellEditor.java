package simple.swt;

import javax.swing.JComboBox;
import javax.swing.JTable;

public class EnumCellEditor extends FixedDefaultCellEditor {
	private static final long serialVersionUID = 10928498400L;

	public <T extends Enum<?>> EnumCellEditor(Class<T> enumClass) {
		super(createComboBox(enumClass));
	}
	public static <T extends Enum<?>> JComboBox createComboBox(Class<T> enumClass) {
		JComboBox comboBox = new JComboBox(enumClass.getEnumConstants());
		//comboBox.setPreferredSize(new Dimension(31, 20));
		//comboBox.setModel(getColumnsModel());
		//comboBox.setEditable(false);
		//UIManager.installTypeAhead(columnComboBox, comboBoxComparator);
		return comboBox;
	}

	public static <T extends Enum<?>> void registerEnumEditor(JTable table, Class<T> enumClass) {
		table.setDefaultEditor(enumClass, new EnumCellEditor(enumClass));
	}
}
