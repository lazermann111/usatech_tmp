package simple.swt;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;

/**
 * Implementation of a table model that relies on a list of beans. The properties
 * of the beans define the columns of the table.
 */
public class BeanTableModel extends AbstractTableModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 731090132845L;
	private static final Log log = Log.getLog();
	protected PropertyDescriptor[] columnDescriptors = null;
	protected Class<?> beanClass = null;
    protected static final Object[] EMPTY_ARGS = new Object[0];
	protected ListModel listModel = null;
	protected ListDataListener listDataListener = null;
	protected String[] columns = null;
	protected boolean[] columnsEditable = null;
    
    /**
     * BeanTableModel constructor comment.
     */
	public BeanTableModel(Class<?> beanClass, ListModel listModel, boolean editable, String... columns) {
		this();
		setBeanClass(beanClass);
		setListModel(listModel);
		if(columns != null)
			setColumns(columns);
		Arrays.fill(getColumnsEditable(), editable);
	}

	public BeanTableModel() {
        super();
    }
    /**
     *  Returns the class
     */
    protected Object convert(Object value, int columnIndex) throws ConvertException {
        if(value == null) return value;
        Class<?> clazz = getColumnClass(columnIndex);
        return ConvertUtils.convert(clazz,value);
    }
    
    /**
	 * Insert the method's description here. Creation date: (1/16/01 2:59:03 PM)
	 * 
	 * @return Class
	 */
	public Class<?> getBeanClass() {
        return beanClass;
    }
    
    /**
     *  Returns the class
     */
	public Class<?> getColumnClass(int columnIndex) {
		PropertyDescriptor pd = getColumnDescriptors()[columnIndex];
        if(pd == null) return String.class; //so that it finds a cellrenderer
		Class<?> clazz = pd.getPropertyType();
		return ConvertUtils.convertToWrapperClass(clazz);
    }
    
    /**
     * getColumnCount method comment.
     */
    public int getColumnCount() {
        if(getColumnDescriptors() == null ) return 0;
        return getColumnDescriptors().length;
    }
    
    /**
     * Insert the method's description here.
     * Creation date: (1/16/01 2:51:06 PM)
     * @return PropertyDescriptor[]
     */
	protected PropertyDescriptor[] getColumnDescriptors() {
        if(columnDescriptors == null && getBeanClass() != null) {
            //determine the columns from the properties of this object
			PropertyDescriptor[] cds;
            try {
                cds = java.beans.Introspector.getBeanInfo(getBeanClass()).getPropertyDescriptors();
            } catch(java.beans.IntrospectionException inse) {
                return null;
            }
            
            //sort list alphabetically
			Comparator<Object> c = new Comparator<Object>() {
                public int compare(Object o1, Object o2) {
					String s1 = (o1 instanceof PropertyDescriptor ? ((PropertyDescriptor) o1).getName() : String.valueOf(o1));
					String s2 = (o2 instanceof PropertyDescriptor ? ((PropertyDescriptor) o2).getName() : String.valueOf(o2));
					return s1.compareToIgnoreCase(s2);
                }
            };
			Arrays.sort(cds, c);
            
            String[] cols = getColumns();
            if(cols == null) { //return all properties as columns
                columnDescriptors = cds;
				cols = new String[cds.length];
				for(int i = 0; i < cds.length; i++)
					cols[i] = StringUtils.capitalizeFirst(cds[i].getDisplayName());
				columns = cols;
            } else {
				columnDescriptors = new PropertyDescriptor[cols.length];
                for (int i = 0; i < cols.length; i++){
                    int index =	java.util.Arrays.binarySearch(cds,cols[i],c);
                    if(index < 0) { //not found
                        columnDescriptors[i] = null;
                    } else {
                        columnDescriptors[i] = cds[index];
                    }
                }
            }
        }
        return columnDescriptors;
    }
    
    /**
     * Insert the method's description here.
     * Creation date: (1/16/01 2:51:06 PM)
     * @return String
     */
    public String getColumnName(int columnIndex) {
		getColumnDescriptors();
		return columns[columnIndex];
    }
    
	/**
	 * Gets the columns property (String[]) value.
	 * 
	 * @return The columns property value.
	 * @see #setColumns
	 */
	public String[] getColumns() {
        return columns;
    }
    /**
     * Insert the method's description here.
     * Creation date: (1/16/01 2:51:06 PM)
     * @return boolean[]
     */
    public boolean[] getColumnsEditable() {
		if(columnsEditable == null)
			columnsEditable = new boolean[getColumnCount()];
        return columnsEditable;
    }
    /**
     * Gets the listDataListener property (javax.swing.event.ListDataListener) value.
     * @return The listDataListener property value.
     */
	protected ListDataListener getListDataListener() {
        if(listDataListener == null) {
			listDataListener = new ListDataListener() {
				public void contentsChanged(ListDataEvent e) {
                    int low = Math.min(e.getIndex0(),e.getIndex1());
                    int high = Math.max(e.getIndex0(),e.getIndex1());
                    fireTableRowsUpdated(low, high);
                }

				public void intervalAdded(ListDataEvent e) {
                    int low = Math.min(e.getIndex0(),e.getIndex1());
                    int high = Math.max(e.getIndex0(),e.getIndex1());
                    fireTableRowsInserted(low, high);
                }

				public void intervalRemoved(ListDataEvent e) {
                    int low = Math.min(e.getIndex0(),e.getIndex1());
                    int high = Math.max(e.getIndex0(),e.getIndex1());
                    fireTableRowsDeleted(low, high);
                }
            };
        }
        return listDataListener;
    }
    /**
     * Gets the listModel property (javax.swing.ListModel) value.
     * @return The listModel property value.
     * @see #setListModel
     */
	public ListModel getListModel() {
        return listModel;
    }
    /**
     * getRowCount method comment.
     */
    public int getRowCount() {
        if(getListModel() == null) return 0;
        return getListModel().getSize();
    }
    /**
     * getValueAt method comment.
     */
    public Object getValueAt(int rowIndex, int columnIndex) {
		PropertyDescriptor pd = getColumnDescriptors()[columnIndex];
        if(pd == null) return null;
        Object retVal = null;
        try {
            retVal = pd.getReadMethod().invoke(getListModel().getElementAt(rowIndex), EMPTY_ARGS);
        } catch(IllegalArgumentException e1) {
        } catch(IllegalAccessException e2) {
		} catch(InvocationTargetException e3) {
        }
        return retVal;
    }
    
    /**
     *  Returns true if that property has a write method and the column is editable
     */
    public boolean isCellEditable(int rowIndex, int columnIndex) {
		PropertyDescriptor pd = getColumnDescriptors()[columnIndex];
        if(pd == null || pd.getWriteMethod() == null) return false;
        boolean[] b = getColumnsEditable();
        if(b.length > columnIndex && !b[columnIndex]) return false;
        return true;
    }
    
    /**
	 * Insert the method's description here. Creation date: (1/16/01 2:59:03 PM)
	 * 
	 * @param newBeanClass
	 *            Class
	 */
	public void setBeanClass(Class<?> newBeanClass) {
        beanClass = newBeanClass;
        columnDescriptors = null;
        fireTableStructureChanged();
    }
    
    /**
     * Insert the method's description here.
     * Creation date: (1/16/01 2:51:06 PM)
     * @return Boolean[]
     */
    public void setColumnEditable(int column, boolean editable) {
        if(getColumnsEditable().length <= column) {
            boolean[] tmp = new boolean[column+1];
            System.arraycopy(getColumnsEditable(),0,tmp,0,tmp.length);
            setColumnsEditable(tmp);
        }
        getColumnsEditable()[column] = editable;
    }
    
    /**
	 * Sets the columns property (String[]) value.
	 * 
	 * @param columns
	 *            The new value for the property.
	 * @see #getColumns
	 */
	public void setColumns(String[] newColumns) {
        String[] oldValue = columns;
        columns = newColumns;
        boolean changed = (oldValue != columns);
        if(oldValue != null && oldValue.equals(columns)) changed = false;
        if(changed) {
            columnDescriptors = null;
            fireTableStructureChanged();
        }
    }
    
    /**
     * Insert the method's description here.
     * Creation date: (1/16/01 2:51:06 PM)
     * @return boolean[]
     */
    public void setColumnsEditable(boolean[] editable) {
        columnsEditable = editable;
    }
    /**
     * Sets the listModel property (javax.swing.ListModel) value.
     * @param listModel The new value for the property.
     * @see #getListModel
     */
    public void setListModel(javax.swing.ListModel newListModel) {
        if(listModel != null) listModel.removeListDataListener(getListDataListener());
        listModel = newListModel;
        if(listModel != null) listModel.addListDataListener(getListDataListener());
        fireTableDataChanged();
    }
    /**
     *  Sets the value of the property.
     */
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        try {
			Method m = getColumnDescriptors()[columnIndex].getWriteMethod();
            Object o = getListModel().getElementAt(rowIndex);
            if(m != null && o != null) {
                aValue = convert(aValue,columnIndex);
                m.invoke(o, new Object[] {aValue});
                fireTableCellUpdated(rowIndex,columnIndex);
            }
        } catch(ConvertException ce) {
            log.warn("Value's class=" + aValue.getClass().getName(), ce);
        } catch(IllegalArgumentException e1) {
            log.warn("Value's class=" + aValue.getClass().getName(), e1);
        } catch(IllegalAccessException e2) {
            log.warn("While setting value on column " + columnIndex,e2);
		} catch(InvocationTargetException e3) {
            log.warn("While setting value on column " + columnIndex,e3);
        }
    }
}
