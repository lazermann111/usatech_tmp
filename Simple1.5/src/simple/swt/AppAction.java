/*
 * AppAction.java
 *
 * Created on June 23, 2003, 4:18 PM
 */

package simple.swt;

import javax.swing.KeyStroke;

/**
 *
 * @author  Brian S. Krug
 */
public abstract class AppAction extends javax.swing.AbstractAction {
    public static final String ACTION_GROUP = "ACTION_GROUP";
    protected static final String PROP_PREFIX = "simple.swt.AppAction.";
    protected static final String PROP_SUFFIX_NAME = ".name";
    protected static final String PROP_SUFFIX_DESC = ".desc";
    protected static final String PROP_SUFFIX_ACCELERATOR = ".shortcut";
    
    public AppAction(java.util.ResourceBundle bundle, String commandKey) {
        this(bundle, commandKey, null);
    }
    
    public AppAction(java.util.ResourceBundle bundle, String commandKey, Object actionGroup) {
        this(bundle.getString(PROP_PREFIX+commandKey+PROP_SUFFIX_NAME),
            bundle.getString(PROP_PREFIX+commandKey+PROP_SUFFIX_DESC),
            commandKey,
            bundle.getString(PROP_PREFIX+commandKey+PROP_SUFFIX_ACCELERATOR),
            actionGroup);
    }
    
    public AppAction(java.util.Properties properties, String commandKey) {
        this(properties, commandKey, null);
    }
    
    public AppAction(java.util.Properties properties, String commandKey, Object actionGroup) {
        this(properties.getProperty(PROP_PREFIX+commandKey+PROP_SUFFIX_NAME),
            properties.getProperty(PROP_PREFIX+commandKey+PROP_SUFFIX_DESC),
            commandKey,
            properties.getProperty(PROP_PREFIX+commandKey+PROP_SUFFIX_ACCELERATOR),
            actionGroup);
    }
    
    public AppAction(String name, String shortDesc, String commandKey, char acceleratorChar) {
        this(name, shortDesc, commandKey, KeyStroke.getKeyStroke(acceleratorChar));
    }
    
    public AppAction(String name, String shortDesc, String commandKey, 
            char acceleratorChar, Object actionGroup) {
        this(name, shortDesc, commandKey, KeyStroke.getKeyStroke(acceleratorChar), actionGroup);
    }
    
    public AppAction(String name, String shortDesc, String commandKey, String acceleratorString) {
        this(name, shortDesc, commandKey, KeyStroke.getKeyStroke(acceleratorString));
    }

    public AppAction(String name, String shortDesc, String commandKey, 
            String acceleratorString, Object actionGroup) {
        this(name, shortDesc, commandKey, KeyStroke.getKeyStroke(acceleratorString), actionGroup);
    }
    
    public AppAction(String name, String shortDesc, String commandKey, 
            KeyStroke acceleratorKey) {
        this(name, shortDesc, commandKey, acceleratorKey, null);
    }
    
    public AppAction(String name, String shortDesc, String commandKey, 
            KeyStroke acceleratorKey, Object actionGroup) {
        putValue(NAME, name);
        putValue(SHORT_DESCRIPTION, shortDesc);
        putValue(ACTION_COMMAND_KEY , commandKey);
        putValue(ACCELERATOR_KEY, acceleratorKey);
        putValue(ACTION_GROUP, actionGroup);
        AppActions.addAction(this);
    }
    
    public void checkEnabled() {
        setEnabled(canPerform());
    }
    
    protected abstract boolean canPerform() ;
}
