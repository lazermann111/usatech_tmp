package simple.swt;

import java.util.List;

import simple.util.TrackableList;

/** An implementation of the <CODE>java.util.List</CODE> interface that tracks any
 * additions or removals from itself and also tracks any property changes
 * in its elements (if they have an addPropertyChangeListener() method and a
 * removePropertyChangeListener() method).
 */
public class TrackableListModel<E> extends TrackableList<E> implements GenericMutableListModel<E> {
    public TrackableListModel() {
        super();
    }
    public TrackableListModel(int initialCapacity) {
        super(initialCapacity);
    }
    public TrackableListModel(List<E> list) {
        super(list);
    }
}
