/*
 * AppJButton.java
 *
 * Created on June 23, 2003, 2:50 PM
 */

package simple.swt;

/**
 * Like its ancestor but with support for tracking changes to the Action Command
 * added.
 *
 * @author  Brian S. Krug
 */
public class AppButton extends javax.swing.JButton {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3876660609925L;

	/** Creates a new instance of AppJButton */
    public AppButton() {
        super();
        AppActions.addButton(this);
    }
    
    public void setActionCommand(String newCommand) {
        String oldCommand = getActionCommand();
        super.setActionCommand(newCommand);
        if(!simple.bean.ConvertUtils.areEqual(oldCommand, newCommand))
            firePropertyChange("actionCommand", oldCommand, newCommand);
    }
}
