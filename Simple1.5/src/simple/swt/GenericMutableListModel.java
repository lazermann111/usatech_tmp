package simple.swt;

public interface GenericMutableListModel<E> extends GenericListModel<E> {
	public void setElementAt(int index, E element) ;
}
