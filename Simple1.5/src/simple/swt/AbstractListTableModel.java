package simple.swt;

import javax.swing.AbstractListModel;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;

import simple.bean.ConvertUtils;
import simple.util.TrackableList;

/**
 * Implementation of a table model that relies on a trackable list.
 */
public abstract class AbstractListTableModel<E> extends AbstractTableModel {
	private static final long serialVersionUID = 967339693398980512L;
	protected GenericListModel<E> listModel;
    protected final ListDataListener listDataListener = new ListDataListener() {
        public void contentsChanged(ListDataEvent e) {
            int low = Math.min(e.getIndex0(),e.getIndex1());
            int high = Math.max(e.getIndex0(),e.getIndex1());
            fireTableRowsUpdated(low, high);
        }
        public void intervalAdded(ListDataEvent e) {
            int low = Math.min(e.getIndex0(),e.getIndex1());
            int high = Math.max(e.getIndex0(),e.getIndex1());
            fireTableRowsInserted(low, high);
        }
        public void intervalRemoved(ListDataEvent e) {
            int low = Math.min(e.getIndex0(),e.getIndex1());
            int high = Math.max(e.getIndex0(),e.getIndex1());
            fireTableRowsDeleted(low, high);
        }
    };
    protected String[] columnNames;
    protected Class<?>[] columnClasses;
    protected boolean[] columnEditable;
    
    /**
     * BeanTableModel constructor comment.
     */
    public AbstractListTableModel(String[] columnNames, Class<?>[] columnClasses, boolean[] columnEditable) {
        super();
        setColumnNames(columnNames);
        setColumnClasses(columnClasses);
        setColumnEditable(columnEditable);
    }
    public Class<?>[] getColumnClasses() {
        return columnClasses;
    }
    public void setColumnClasses(Class<?>[] columnClasses) {
        this.columnClasses = columnClasses;
    }
    public String[] getColumnNames() {
        return columnNames;
    }
    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
    }
    
    public boolean[] getColumnEditable() {
        return columnEditable;
    }
    public void setColumnEditable(boolean[] columnEditable) {
        this.columnEditable = columnEditable;
    }
    
    /**
     *  Returns the class
     */
    public Class<?> getColumnClass(int columnIndex) {
        if(columnClasses != null && columnIndex < columnClasses.length) 
            return columnClasses[columnIndex]; 
        return Object.class;
    }
    
    /**
     * getColumnCount method comment.
     */
    public int getColumnCount() {
        return columnNames.length;
    }
    
    /**
     * Insert the method's description here.
     * Creation date: (1/16/01 2:51:06 PM)
     * @return String
     */
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    /**
     * Gets the listModel property (javax.swing.ListModel) value.
     * @return The listModel property value.
     * @see #setListModel
     */
    public GenericListModel<E> getListModel() {
        return listModel;
    }
    
    /**
     * getRowCount method comment.
     */
    public int getRowCount() {
        if(getListModel() == null) return 0;
        return getListModel().getSize();
    }
    
    /**
     *  Returns true if that property has a write method and the column is editable
     */
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if(columnEditable != null && columnIndex < columnEditable.length) 
            return columnEditable[columnIndex]; 
        return false;
    }
        
   /**
     * Sets the listModel property (javax.swing.ListModel) value.
     * @param listModel The new value for the property.
     * @see #getListModel
     */
	@SuppressWarnings("unchecked")
	public void setListModel(final ListModel<E> newListModel) {
    	if(newListModel instanceof GenericListModel)
			setListModelInternal((GenericListModel<E>) newListModel);
    	else {
    		setListModelInternal(new GenericListModel<E>() {
				public E getElementAt(int index) {
					return newListModel.getElementAt(index);
				}
				public void addListDataListener(ListDataListener l) {
					newListModel.addListDataListener(l);
				}
				public int getSize() {
					return newListModel.getSize();
				}
				public void removeListDataListener(ListDataListener l) {
					newListModel.removeListDataListener(l);
				}   			
    		});
    	}
    }
    /**
     * Sets the listModel property (javax.swing.ListModel) value.
     * @param listModel The new value for the property.
     * @see #getListModel
     */
    public void setListModel(GenericListModel<E> newListModel) {
    	setListModelInternal(newListModel);
    }
    protected void setListModelInternal(GenericListModel<E> newListModel) {
        if(listModel != null) listModel.removeListDataListener(listDataListener);
        listModel = newListModel;
        if(listModel != null) listModel.addListDataListener(listDataListener);
        fireTableDataChanged();
    }
    /**
     * getValueAt method comment.
     */
    public Object getValueAt(int rowIndex, int columnIndex) {
        return getValueAt(listModel.getElementAt(rowIndex), columnIndex);
    }
    
    protected abstract Object getValueAt(E elementAt, int columnIndex) ;
    
    /**
     *  Sets the value of the property.
     */
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        E element = listModel.getElementAt(rowIndex);
        Object old = getValueAt(element, columnIndex);
        if(ConvertUtils.areEqual(value, old)) return; // Avoid unnecessary notifications
        setValueAt(value, element, columnIndex);
        fireChangedEvents(rowIndex, columnIndex);
    }
    
    protected void fireChangedEvents(int rowIndex, int columnIndex) {
    	if(listModel instanceof AbstractListModel) {
            ListDataEvent event = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, rowIndex, rowIndex);
			for(ListDataListener l : ((AbstractListModel<?>) listModel).getListeners(ListDataListener.class)) {
               l.contentsChanged(event);
            }           
        } else if(listModel instanceof TrackableList) {
			((TrackableList<?>) listModel).fireElementChanged(rowIndex);
        }
    }
    
    protected abstract void setValueAt(Object value, E elementAt, int columnIndex) ;
    
}
