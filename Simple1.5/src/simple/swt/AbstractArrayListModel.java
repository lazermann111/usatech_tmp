package simple.swt;


public abstract class AbstractArrayListModel<E> extends AbstractGenericListModel<E> implements GenericMutableListModel<E> {
	public void setElementAt(int index, E element) {
		E[] array = getArray();
		if(array == null) throw new IllegalStateException("Array is null");
		if(index >= array.length) {
			if(element != null) {
				//expand array
				E[] tmp = newArray(array.length+1);
				System.arraycopy(array, 0, tmp, 0, array.length);
				tmp[array.length] = element;
				setArray(tmp);
			}
		} else {
			if(element == null) {
				// shrink array
				E[] tmp = newArray(array.length-1);
				System.arraycopy(array, 0, tmp, 0, index);
				System.arraycopy(array, index+1, tmp, index, tmp.length-index);
				setArray(tmp);
			} else {
				array[index] = element;
			}
		}
	}
	public E getElementAt(int index) {
		if(getArray() == null || index >= getArray().length) return null;
		return getArray()[index];
	}

	public int getSize() {
		if(getArray() == null) return 0;
		return getArray().length + 1;
	}

	protected abstract E[] newArray(int length) ;
	protected abstract E[] getArray() ;
	protected abstract void setArray(E[] array) ;
}
