package simple.swt;

import simple.results.ScrollableResults;

/**
 * Implementation of a table model that relies on a Results object. The columns in the results object define the columns of the table.
 */
public class ResultsTableModel extends javax.swing.table.AbstractTableModel {
    private static final long serialVersionUID = 1722090132845L;
	protected ScrollableResults results;
    protected javax.swing.event.ListDataListener listDataListener = null;

    /**
     * BeanTableModel constructor comment.
     */
    public ResultsTableModel() {
        super();
    }
    public ResultsTableModel(ScrollableResults results) {
        super();
        setResults(results);
    }
    /**
     *  Returns the class
     */
    public Class<?> getColumnClass(int columnIndex) {
        Class clazz = results.getColumnClass(columnIndex+1);
        if(clazz.isPrimitive()) {
            if(clazz == boolean.class)
                clazz = Boolean.class;
            else if(clazz == byte.class)
                clazz = Byte.class;
            else if(clazz == char.class)
                clazz = Character.class;
            else if(clazz == double.class)
                clazz = Double.class;
            else if(clazz == float.class)
                clazz = Float.class;
            else if(clazz == int.class)
                clazz = Integer.class;
            else if(clazz == long.class)
                clazz = Long.class;
            else if(clazz == short.class)
                clazz = Short.class;
        }
        return clazz;
    }

    /**
     * getColumnCount method comment.
     */
    public int getColumnCount() {
        if(results == null ) return 0;
        return results.getColumnCount();
    }

    /**
     * Insert the method's description here.
     * Creation date: (1/16/01 2:51:06 PM)
     * @return String
     */
    public String getColumnName(int columnIndex) {
        return results.getColumnName(columnIndex+1);
    }

    /**
     * Gets the listDataListener property (javax.swing.event.ListDataListener) value.
     * @return The listDataListener property value.
     */
    protected javax.swing.event.ListDataListener getListDataListener() {
        if(listDataListener == null) {
            listDataListener = new javax.swing.event.ListDataListener() {
                public void contentsChanged(javax.swing.event.ListDataEvent e) {
                    int low = Math.min(e.getIndex0(),e.getIndex1());
                    int high = Math.max(e.getIndex0(),e.getIndex1());
                    fireTableRowsUpdated(low, high);
                }
                public void intervalAdded(javax.swing.event.ListDataEvent e) {
                    int low = Math.min(e.getIndex0(),e.getIndex1());
                    int high = Math.max(e.getIndex0(),e.getIndex1());
                    fireTableRowsInserted(low, high);
                }
                public void intervalRemoved(javax.swing.event.ListDataEvent e) {
                    int low = Math.min(e.getIndex0(),e.getIndex1());
                    int high = Math.max(e.getIndex0(),e.getIndex1());
                    fireTableRowsDeleted(low, high);
                }
            };
        }
        return listDataListener;
    }

    /**
     * getRowCount method comment.
     */
    public int getRowCount() {
        if(results == null) return 0;
        return results.size();
    }
    /**
     * getValueAt method comment.
     */
    public Object getValueAt(int rowIndex, int columnIndex) {
        results.setRow(rowIndex+1);
        return results.getValue(columnIndex+1);
    }

	public ScrollableResults getResults() {
		return results;
	}
	public void setResults(ScrollableResults results) {
		this.results = results;
		fireTableDataChanged();
	}
}
