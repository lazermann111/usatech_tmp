/*
 * Created on Dec 27, 2005
 *
 */
package simple.swt;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import simple.bean.ConvertUtils;

public class ChangeableComboBoxModel<E> extends AbstractListModel implements ComboBoxModel {
    private static final long serialVersionUID = 8948194114L;
	protected E selectedItem;
    protected E[] items;
    public ChangeableComboBoxModel() {
    }
    public int getSize() {
        return items == null ? 0 : items.length;
    }

    public E getElementAt(int index) {
        return items[index];
    }

    public E getSelectedItem() {
        return selectedItem;
    }

    @SuppressWarnings("unchecked")
	public void setSelectedItem(Object selectedItem) {
    	if(!ConvertUtils.areEqual(selectedItem, this.selectedItem)) {
	        this.selectedItem = (E)selectedItem;
	        fireContentsChanged(this, -1, -1);
    	}
    }

    public E[] getItems() {
        return items;
    }

    public void setItems(E[] items) {
        int oldSize = getSize();
        this.items = items;
        setSelectedItem(null);
        fireContentsChanged(this, 0, Math.max(oldSize, getSize()));
    }
}