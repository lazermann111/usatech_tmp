/*
 * ImageConverter.java
 *
 * Created on January 28, 2003, 2:57 PM
 */

package simple.swt;

import java.awt.Image;
import java.io.IOException;

import simple.bean.Converter;
import simple.bean.Formatter;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class ImageConverter implements Converter<Image>, Formatter<Image> {

    /** Creates new ImageConverter */
    public ImageConverter() {
    }

    public Image convert(Object object) {
        try {
            if(object instanceof ImageResource) return ((ImageResource)object).getImage();
            if(object != null) return ImageResource.getImageForPath(object.toString());
        } catch(IOException e) {
        }
        return null;
    }    
    /*
    public Object convert(Class clazz, Object object) {
        try {
            if(Image.class == clazz) {
                return ImageResource.getImageForPath(object.toString());
            }           
            if(ImageResource.class == clazz) {
                if(object instanceof Image) object = ImageResource.getPathForImage((Image)object);
                ImageResource ir = new ImageResource();
                ir.setImagePath(object.toString());
                return ir;
            }
        } catch(IOException e) {
        }
        return null;
    }    
    */
    public String format(Image object) {
    	return ImageResource.getPathForImage(object);
    }
    
    public String format(ImageResource object) {
        return object.getImagePath();
    }

	/**
	 * @see simple.bean.Converter#getTargetClass()
	 */
	public Class<Image> getTargetClass() {
		return Image.class;
	}
    
}
