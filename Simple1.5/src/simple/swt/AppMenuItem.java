/*
 * AppJMenuItem.java
 *
 * Created on June 23, 2003, 2:50 PM
 */

package simple.swt;

/**
 * Like its ancestor but with support for tracking changes to the Action Command
 * added.
 *
 * @author  Brian S. Krug
 */
public class AppMenuItem extends javax.swing.JMenuItem {
    /**
	 * 
	 */
	private static final long serialVersionUID = -23934781471L;

	/** Creates a new instance of AppJMenuItem */
    public AppMenuItem() {
        super();
        AppActions.addButton(this);
    }
    
    public void setActionCommand(String newCommand) {
        String oldCommand = getActionCommand();
        super.setActionCommand(newCommand);
        if(!simple.bean.ConvertUtils.areEqual(oldCommand, newCommand))
            firePropertyChange("actionCommand", oldCommand, newCommand);
    }
}
