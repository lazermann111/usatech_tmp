package simple.swt;

import java.util.Comparator;
import java.util.List;

import simple.util.SortedTrackableList;

/** An implementation of the <CODE>java.util.List</CODE> interface that tracks any
 * additions or removals from itself and also tracks any property changes
 * in its elements (if they have an addPropertyChangeListener() method and a
 * removePropertyChangeListener() method).
 */
public class SortedTrackableListModel<E> extends SortedTrackableList<E> implements GenericMutableListModel<E> {

	public SortedTrackableListModel(boolean allowDups, Comparator<E> comparator) {
		super(allowDups, comparator);
	}

	public SortedTrackableListModel(boolean allowDups) {
		super(allowDups);
	}

	public SortedTrackableListModel(int initialCapacity, boolean allowDups, Comparator<E> comparator) {
		super(initialCapacity, allowDups, comparator);
	}

	public SortedTrackableListModel(int initialCapacity, boolean allowDups) {
		super(initialCapacity, allowDups);
	}

	public SortedTrackableListModel(List<E> list, boolean allowDups, Comparator<E> comparator) {
		super(list, allowDups, comparator);
	}

	public SortedTrackableListModel(List<E> list, boolean allowDups) {
		super(list, allowDups);
	}

}
