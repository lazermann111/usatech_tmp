package simple.swt;

import javax.swing.ListModel;

public interface GenericListModel<E> extends ListModel {
	public E getElementAt(int index);
}
