package simple.swt;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import simple.util.sheet.SheetUtils.RowIterator;

public class SheetTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 91480147821640L;
	protected final List<Object[]> cache = new ArrayList<Object[]>();
	protected int columnCount = 0;
	public SheetTableModel() {
	}

	public int getColumnCount() {
		return columnCount;
	}

	public int getRowCount() {
		return cache.size();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Object[] cells = cache.get(rowIndex);
		return columnIndex < cells.length ? cells[columnIndex] : null;
	}

	/*
	public void setRowValueIterator(RowValuesIterator rvIter) {
		cache.clear();
		columnCount = 0;
		if(rvIter.hasNext()) {
			Map<String,Object> rowValues = rvIter.next();
			Object[] columnNames = rowValues.keySet().toArray();
			cache.add(columnNames);

			cache.add(rowValues.values().toArray());
			while(rvIter.hasNext())
				cache.add(rvIter.next().values().toArray());
			columnCount = columnNames.length;

		}
	}*/

	public void setRowIterator(RowIterator rowIter) {
		cache.clear();
		columnCount = 0;
		while(rowIter.hasNext()) {
			Object[] cells = rowIter.next().toArray();
			cache.add(cells);
			if(cells.length > columnCount)
				columnCount = cells.length;
		}
	}
}
