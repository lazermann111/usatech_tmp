/*
 * ColumnWidthAdjuster.java
 *
 * Created on August 30, 2004, 10:59 AM
 */

package simple.swt;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 *
 * @author  BrianK
 */
public class ColumnWidthAdjuster implements TableModelListener {
    protected JTable table;
    
    /**
     * Holds value of property maxWidth.
     */
    private int maxWidth;
    
    /** Creates a new instance of ColumnWidthAdjuster */
    public ColumnWidthAdjuster(JTable table) {
        this.table = table;
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    }
    
    public void tableChanged(javax.swing.event.TableModelEvent e) {
        // calculate new preferred size based on data
        if(e.getColumn() == TableModelEvent.ALL_COLUMNS) {
            for(int i = 0; i < table.getModel().getColumnCount(); i++) adjustColumn(i);
        } else
            adjustColumn(e.getColumn());
    }
    
    protected void adjustColumn(int col) {
        double maxWidth = 0.0;
        for(int i = 0; i < table.getModel().getRowCount(); i++) {
            double w = table.getCellRenderer(i, col).getTableCellRendererComponent(
                table, table.getModel().getValueAt(i, col),true, true, i, col).getPreferredSize().getWidth();
            if(w > maxWidth) maxWidth = w;
        }
        if(maxWidth > getMaxWidth() && getMaxWidth() > 0) maxWidth = getMaxWidth();
        table.getColumnModel().getColumn(col).setPreferredWidth(5 + (int)maxWidth);   
    }
    
    /**
     * Getter for property maxWidth.
     * @return Value of property maxWidth.
     */
    public int getMaxWidth() {
        return this.maxWidth;
    }
    
    /**
     * Setter for property maxWidth.
     * @param maxWidth New value of property maxWidth.
     */
    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }
    
}
