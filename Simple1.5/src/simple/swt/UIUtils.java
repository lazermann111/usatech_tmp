package simple.swt;

import java.awt.Button;
import java.awt.Dialog;
import java.awt.Dialog.ModalityType;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.atomic.AtomicReference;

public class UIUtils {
	public static String promptForPassword(String prompt, String title, Window parentWindow) {
		Frame hiddenFrame;
		if(parentWindow == null)
			parentWindow = hiddenFrame = new Frame("Input Requested");
		else
			hiddenFrame = null;
		if(title == null)
			title = "Input Requested";
		final AtomicReference<String> valueHolder = new AtomicReference<>();
		final Dialog dialog = new Dialog(parentWindow, title, ModalityType.APPLICATION_MODAL);
		dialog.setLocation(100, 100);
		dialog.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				valueHolder.set(null);
				dialog.dispose();
			}
		});
		Panel buttonPanel = new Panel();
		Label label = new Label(prompt);
		final TextField textField = new TextField(16);
		textField.setEchoChar('*');
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				valueHolder.set(textField.getText());
				dialog.dispose();
			}
		});

		Button okButton = new Button("Ok");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				valueHolder.set(textField.getText());
				dialog.dispose();
			}
		});

		Button cancelButton = new Button("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				valueHolder.set(null);
				dialog.dispose();
			}
		});

		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);

		dialog.add(label, "North");
		dialog.add(textField, "Center");
		dialog.add(buttonPanel, "South");
		dialog.pack();

		dialog.setVisible(true);
		if(hiddenFrame != null)
			hiddenFrame.dispose();
		return valueHolder.get();
	}
}
