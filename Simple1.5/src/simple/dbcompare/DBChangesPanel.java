/*
 * Created on Oct 20, 2004
 *
 */
package simple.dbcompare;

import java.awt.BorderLayout;

import javax.swing.AbstractListModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import simple.swt.BeanTableModel;
/**
 * @author bkrug
 *
 */
public class DBChangesPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 90234016874981L;
	private DBChanges.Change[] changes = null;
	private JTable jTable = null;
	private JScrollPane jScrollPane = null;
	private BeanTableModel beanTableModel = null;  //  @jve:decl-index=0:visual-constraint="398,76"
	private ChangeListModel listModel = null;   //  @jve:decl-index=0:
	/**
	 * This is the default constructor
	 */
	public DBChangesPanel() {
		super();
		initialize();
	}
	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private  void initialize() {
		this.setLayout(new BorderLayout());
		this.setSize(300,200);
		this.add(getJScrollPane(), java.awt.BorderLayout.NORTH);
	}
	/**
	 * This method initializes jTable	
	 * 	
	 * @return javax.swing.JTable	
	 */    
	private JTable getJTable() {
		if (jTable == null) {
			jTable = new JTable();
			jTable.setModel(getBeanTableModel());
		}
		return jTable;
	}
	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */    
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getJTable());
		}
		return jScrollPane;
	}
	/**
	 * This method initializes beanTableModel	
	 * 	
	 * @return simple.swt.BeanTableModel	
	 */    
	private BeanTableModel getBeanTableModel() {
		if (beanTableModel == null) {
			beanTableModel = new BeanTableModel();
			beanTableModel.setBeanClass(DBChanges.Change.class);
			beanTableModel.setListModel(getListModel());
		}
		return beanTableModel;
	}
	/**
	 * @return Returns the changes.
	 */
	public DBChanges.Change[] getChanges() {
		return changes;
	}
	/**
	 * @param changes The changes to set.
	 */
	public void setChanges(DBChanges.Change[] changes) {
		this.changes = changes;
		getListModel().fireContentsChanged();
	}
	
	protected class ChangeListModel extends AbstractListModel {
		/**
		 * 
		 */
		private static final long serialVersionUID = -54101515100L;
		public int getSize() {
			return (changes == null ? 0 : changes.length);
		}
		public Object getElementAt(int index) {
			return (changes == null ? null : changes[index]);
		}
		public void fireContentsChanged() {
			super.fireContentsChanged(DBChangesPanel.this,0,getSize());
		}
	}
	
	/**
	 * This method initializes listModel	
	 * 	
	 * @return javax.swing.ListModel	
	 */    
	private ChangeListModel getListModel() {
		if (listModel == null) {
			listModel = new ChangeListModel();
		}
		return listModel;
	}
}


