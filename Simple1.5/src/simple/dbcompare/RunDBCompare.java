/*
 * Created on Oct 20, 2004
 *
 */
package simple.dbcompare;

import java.awt.Window;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import simple.app.ServiceException;
import simple.io.GuiInteraction;
import simple.io.Interaction;
import simple.io.Log;
import simple.text.StringUtils;

/**
 * @author bkrug
 *
 */
public class RunDBCompare {
	private static final Log log = Log.getLog();
	private static final String version = "1.4";

	public static void main(String[] args) throws ServiceException {
		Interaction interaction = new GuiInteraction((Window) null);
		char[] pwd1 = interaction.readPassword("Enter the password for db #1");
		char[] pwd2 = interaction.readPassword("Enter the password for db #2");
		new RunDBCompare().execute(
				// "jdbc:postgresql://devmst11:5432/mq?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "ADMIN_1",
				// "jdbc:postgresql://devmst01:5432/mq?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "ADMIN_1",
						"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb012.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))",
						"SYSTEM",
						new String(pwd1),
						//"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.120)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.121)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.122)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.220)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.221)(PORT=1535))(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.71.222)(PORT=1535)))(CONNECT_DATA=(SERVICE_NAME=usaprd_db.world)))",
						"jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb011.usatech.com)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV03.WORLD)))",
						"SYSTEM", new String(pwd2),
				new oracle.jdbc.driver.OracleDriver(), new String[] { "DEVICE" },
				// new HashSet<String>(Arrays.asList(new String[] { "TYPE", "TABLE", "VIEW", "GRANT", "TRIGGER", "INDEX", "CONSTRAINT", "FUNCTION" })),
				new HashSet<String>(Arrays.asList(new String[] { "CONSTRAINT" })),
				null);
		/*
		new RunDBCompare().execute(
				// "jdbc:postgresql://devmst11:5432/mq?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "ADMIN_1",
				// "jdbc:postgresql://devmst01:5432/mq?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "ADMIN_1",
				"jdbc:postgresql://usamst25.trooper.usatech.com:5432/mq?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "",
				"jdbc:postgresql://usamst24:5432/mq?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "",
				new org.postgresql.Driver(), new String[] { "mq" },
				// new HashSet<String>(Arrays.asList(new String[] { "TYPE", "TABLE", "VIEW", "GRANT", "TRIGGER", "INDEX", "CONSTRAINT", "FUNCTION" })),
				new HashSet<String>(Arrays.asList(new String[] { "FUNCTION" })),
				"C:\\Users\\bkrug\\Documents\\db_changes_mq_usamst24_to_usamst25");
		new RunDBCompare().execute(
				// "jdbc:postgresql://devmst11:5432/mq?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "ADMIN_1",
				// "jdbc:postgresql://devmst01:5432/mq?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "ADMIN_1",
				"jdbc:postgresql://usamst25.trooper.usatech.com:5432/filerepo?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "",
				"jdbc:postgresql://usamst24:5432/filerepo?ssl=true&amp;connectTimeout=5&amp;tcpKeepAlive=true", "admin_1", "",
				new org.postgresql.Driver(), new String[] { "file_repo" },
				// new HashSet<String>(Arrays.asList(new String[] { "TYPE", "TABLE", "VIEW", "GRANT", "TRIGGER", "INDEX", "CONSTRAINT", "FUNCTION" })),
				new HashSet<String>(Arrays.asList(new String[] { "TABLE", "FUNCTION" })),
				"C:\\Users\\bkrug\\Documents\\db_changes_filerepo_usamst24_to_usamst25"); //*/
	}


	public void execute(String url1, String username1, String password1, String url2, String username2, String password2, Driver driver, String[] schemas, Set<String> objectTypes, String baseOutputPath) throws ServiceException {
		log.debug("DBCompare v." + version);
    	if(url1 != null) {
            if(username1 == null)
				throw new ServiceException("User Name 1 attribute must be set!");
	        if(password1 == null)
				throw new ServiceException("Password 1 attribute must be set!");
    	}
    	//if(url1 == null)
		// throw new ServiceException("Url 1 attribute must be set!", getLocation());
        if(username2 == null)
			throw new ServiceException("User Name 2 attribute must be set!");
        if(password2 == null)
			throw new ServiceException("Password 2 attribute must be set!");
        if(url2 == null)
			throw new ServiceException("Url 2 attribute must be set!");
        Properties info = new Properties();
        Connection conn1 = null;
        if(url1 != null) {
			log.info("connecting to " + url1);
			info.put("user", username1);
			info.put("password", password1);

	        try {
				conn1 = driver.connect(url1, info);
	        } catch(SQLException sqle) {
	            sqle.printStackTrace();
				throw new ServiceException("Could not connect to " + url1, sqle);
	        }
	        if(conn1 == null)
				throw new ServiceException("No suitable Driver for " + url1);
        }
		log.info("connecting to " + url2);
		info.put("user", username2);
		info.put("password", password2);
        Connection conn2;
        try {
			conn2 = driver.connect(url2, info);
        } catch(SQLException sqle) {
            sqle.printStackTrace();
			throw new ServiceException("Could not connect to " + url2, sqle);
        }
        if(conn2 == null)
			throw new ServiceException("No suitable Driver for " + url2);

        // now the real meat
        DBChanges.Change[] changes;
        try {
        	//entity = DBCompare.compare(conn1, conn2, getSchema());
			changes = DBChanges.findChanges(conn1, conn2, schemas, objectTypes);
        } catch(SQLException sqle) {
        	sqle.printStackTrace();
			throw new ServiceException("Error while comparing:", sqle);
        } catch(Exception e) {
        	e.printStackTrace();
			throw new ServiceException("Error while comparing:", e);
        } finally {
        	try { if(conn1 != null) conn1.close(); } catch(SQLException sqle1) {}
        	try { conn2.close(); } catch(SQLException sqle1) {}
        }
		if(!StringUtils.isBlank(baseOutputPath)) {
		try {
			java.io.PrintStream out = new java.io.PrintStream(new java.io.FileOutputStream(baseOutputPath + ".sql"));
			for(int i = 0; i < changes.length; i++) {
				try {
					out.println(changes[i].getSQL());
				} catch(Exception e) {
					System.out.println("!!ERROR: on " + changes[i]);
					e.printStackTrace();
				}
			}
			out.flush();
			out.close();
		} catch(IOException ioe) {
			ioe.printStackTrace();
			throw new ServiceException("Could not write to output file '" + baseOutputPath + ".sql'", ioe);
		} catch(Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error while comparing:", e);
		}
		try {
			java.io.PrintStream out = new java.io.PrintStream(new java.io.FileOutputStream(baseOutputPath + ".txt"));
			for(int i = 0; i < changes.length; i++) {
				try {
					final char SEP = '\t';
					out.print(changes[i].getEntityName());
					out.print(SEP);
					out.print(changes[i].getChangeType());
					out.print(SEP);
					out.print(simple.text.RegexUtils.substitute("(\\n\\r?)|(\\r)", changes[i].getSQL(), " ", null));
					out.println();
				} catch(Exception e) {
					System.out.println("!!ERROR: on " + changes[i]);
					e.printStackTrace();
				}
			}
			out.flush();
			out.close();
		} catch(IOException ioe) {
			ioe.printStackTrace();
			throw new ServiceException("Could not write to output file '" + baseOutputPath + ".txt'", ioe);
		} catch(Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error while comparing:", e);
        }
		}
    }
}
