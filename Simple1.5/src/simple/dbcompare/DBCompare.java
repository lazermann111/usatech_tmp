/*
 * DBCompare.java
 *
 * Created on January 31, 2003, 4:13 PM
 */

package simple.dbcompare;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author  Brian S. Krug
 * @version 
 */
public class DBCompare {
    
    protected final static String[] order = new String[] {"TABLE_SCHEM", "TABLE_NAME", "COLUMN_NAME"};
    protected final static String[] check = new String[] {"DATA_TYPE","TYPE_NAME","COLUMN_SIZE",
        "DECIMAL_DIGITS","NUM_PREC_RADIX","NULLABLE","COLUMN_DEF"};
        protected final static Comparator columnComparator = new Comparator() {
            public int compare(Object o1, Object o2) {
                return compareUpTo(o1,o2,order.length-1);                
            }
        };
    public static final String CHANGE_TYPE_DELETED = "Deleted";
    public static final String CHANGE_TYPE_ADDED = "Added";
    public static final String CHANGE_TYPE_MODIFIED = "Changed";
        
    public static class Entity {
        public String levelName;
        public Map props;
        public Object value;
        public List msgs;
        public List subs;
        public String changeType;
        public String toString() {
            return (value == null ? "" : value.toString()) + " - " + (changeType == null ? "Children Modified" : changeType);
        }
    }
    /*String TABLE_CAT;
        String TABLE_SCHEM;
        String TABLE_NAME;
        String COLUMN_NAME;
        short DATA_TYPE; 
        String TYPE_NAME;
        int COLUMN_SIZE;
        int DECIMAL_DIGITS; 
        int NUM_PREC_RADIX;
        int NULLABLE; 
        int COLUMN_DEF;
        */
    
    /** Creates new DBCompare */
    public DBCompare() {
    }

    public static Entity compare(Connection con1, Connection con2, String schema) throws SQLException {
        DatabaseMetaData md1 = con1.getMetaData();
        DatabaseMetaData md2 = con2.getMetaData();
        Entity diffs = new Entity();
        compareColumns(diffs, md1, md2, schema, null, null);
        return diffs;        
    }
    
    public static void compareColumns(Entity diffs, DatabaseMetaData md1, DatabaseMetaData md2, String schemaPattern, String tableNamePattern, String columnNamePattern) 
    throws SQLException {
        ResultSet rs1 = md1.getColumns(null, schemaPattern, tableNamePattern, columnNamePattern);
        ResultSet rs2 = md2.getColumns(null, schemaPattern, tableNamePattern, columnNamePattern);
        List cols1 = new ArrayList();
        List cols2 = new ArrayList();
        try {
            ResultSetMetaData rsmd = rs1.getMetaData();
            String[] columnNames = new String[rsmd.getColumnCount()];
            for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
            while(rs1.next()) {
                Map m = new HashMap();
                for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs1.getObject(i+1));
                cols1.add(m);
            }
            while(rs2.next()) {
                Map m = new HashMap();
                for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs2.getObject(i+1));
                cols2.add(m);
            }
        } finally {
            rs1.close();
            rs2.close();
            rs1 = null;
            rs2 = null;
        }
        Map[] columns1 = (Map[])cols1.toArray(new Map[cols1.size()]);
        Map[] columns2 = (Map[])cols2.toArray(new Map[cols2.size()]);
        Arrays.sort(columns1, columnComparator);
        Arrays.sort(columns2, columnComparator);
        int i1 = 0;
        int i2 = 0;
        while(i1 < columns1.length || i2 < columns2.length) {
            boolean skipCheck = false;
            //check for missing schemas, tables, columns
            for(int k = 0; k < order.length; k++) {
                int r;
                if(i1 == columns1.length) r = 1;
                else if(i2 == columns2.length) r = -1;
                else {
                    Comparable c1 = (Comparable)columns1[i1].get(order[k]);
                    Comparable c2 = (Comparable)columns2[i2].get(order[k]);
                    //trace("Checking " + c1 + " against " + c2 + " (" + order[k] + ")");
                    r = c1.compareTo(c2);
                    if(r != 0) trace("NO MATCH: " + c1 + " vs " + c2);
                }
                if(r < 0) {
                    String s = "MISSING from Database 2";
                    //figure out if previous it better match
                    if(i2 > 0) {
                        int o = 0;
                        for(; o < order.length-1; o++) {
                            if(!columns1[i1].get(order[o]).equals(columns2[i2-1].get(order[o]))) break;
                        }    
                        if(o > k) k = o;
                    }
                    Object[] ents = new Object[k+1];
                    for(int p = 0; p < ents.length; p++) ents[p] = columns1[i1].get(order[p]);
                    addDiff(diffs, ents, s, CHANGE_TYPE_DELETED);
                    /*String s = "Database 2 is missing " + order[k] + " " + c1;
                    for(int p = k-1; p >= 0; p--) s += " in " + order[p] + " " + columns1[i1].get(order[p]);
                    diffs.add(s);*/
                    int old = i1;
                    i1++;
                    while(i1 < columns1.length && compareUpTo(columns1[old], columns1[i1], k) == 0) i1++;
                    skipCheck = true;
                    break;
                } else if( r > 0) {
                    String s = "MISSING from Database 1";
                    //figure out if previous it better match
                    if(i1 > 0) {
                        int o = 0;
                        for(; o < order.length-1; o++) {
                            if(!columns2[i2].get(order[o]).equals(columns1[i1-1].get(order[o]))) break;
                        }    
                        if(o > k) k = o;
                    }
                    Object[] ents = new Object[k+1];
                    for(int p = 0; p < ents.length; p++) ents[p] = columns2[i2].get(order[p]);
                    addDiff(diffs, ents, s, CHANGE_TYPE_ADDED);
                    /*String s = "Database 1 is missing " + order[k] + " " + c2;
                    for(int p = k-1; p >= 0; p--) s += " in " + order[p] + " " + columns2[i2].get(order[p]);
                    diffs.add(s);*/
                    int old = i2;
                    i2++;
                    while(i2 < columns1.length && compareUpTo(columns2[old], columns2[i2], k) == 0) i2++;
                    skipCheck = true;
                    break;
                }
            }
            if(skipCheck) continue;
            //check attributes of this column
            for(int k = 0; k < check.length; k++) {
                Object o1 = columns1[i1].get(check[k]);
                Object o2 = columns2[i2].get(check[k]);
                if(!areEqual(o1,o2,false)) {
                	//String s = "Different " + check[k] + " (" + o1 + "(" + (o1 == null ? "NULL" : o1.getClass().getName()) + ") vs " + o2 + "(" + (o2 == null ? "NULL" : o2.getClass().getName()) + "))";
                    String s = "Different " + check[k] + " (" + o1 + " vs " + o2 + ")";
                    Object[] ents = new Object[order.length];
                    for(int p = 0; p < ents.length; p++) ents[p] = columns1[i1].get(order[p]);
                    addDiff(diffs, ents, s, CHANGE_TYPE_MODIFIED);
                    /*String s = "Different " + check[k] + " (" + o1 + " vs " + o2 + ") for ";
                    for(int p = order.length-1; p >= 0; p--) {
                        if(p < order.length-1) s += " in ";
                        s += order[p] + " " + columns1[i1].get(order[p]);
                    }
                    diffs.add(s);*/
                }
            }
            i1++;
            i2++;
        }
    }
    
    protected static boolean areEqual(Object o1, Object o2) {
        return areEqual(o1,o2,true);
    }
    
    protected static boolean areEqual(Object o1, Object o2, boolean strict) {
        if(o1 == null) return (o2 == null);
        try {
        	if(!strict) {
	        	if(o1 instanceof String) o1 = ((String)o1).trim().toUpperCase();
	        	if(o2 instanceof String) o2 = ((String)o2).trim().toUpperCase();
        	}
        	return o1.equals(o2);
        } catch(ClassCastException e) {
            return false;
        }
    }
    
    protected static void trace(String s) {
        System.out.println(s);
    }
    
    protected static int compareUpTo(Object o1, Object o2, int index) {
        for(int i = 0; i < index+1; i++) {
            Comparable c1 = (Comparable)((Map)o1).get(order[i]);
            Comparable c2 = (Comparable)((Map)o2).get(order[i]);
            int r = c1.compareTo(c2);
            if(r != 0) return r;
        }
        return 0;                
    }
    
    protected static void addDiff(Entity top, Object[] ents, String msg, String changeType) {
        Entity curr = top;
        for(int i = 0; i < ents.length; i++) {
            Entity next = null;
            if(curr.subs == null) curr.subs = new LinkedList();
            else if(!top.subs.isEmpty()) {
                next = (Entity) curr.subs.get(curr.subs.size()-1);
                if(!ents[i].equals(next.value)) next = null;
            }
            if(next == null) {
                next = new Entity();
                next.value = ents[i];
                next.levelName = order[i];
                curr.subs.add(next);
            }
            curr = next;
        }
        if(curr.msgs == null) curr.msgs = new LinkedList();
        curr.msgs.add(msg);
        curr.changeType = changeType;
    }
}
