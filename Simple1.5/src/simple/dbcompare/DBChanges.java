/*
 * DBCompare.java
 *
 * Created on January 31, 2003, 4:13 PM
 */

package simple.dbcompare;

import java.sql.Array;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.RegexUtils;
import simple.util.CaseInsensitiveHashMap;

/**
 *
 * @author  Brian S. Krug
 * @version
 */
public class DBChanges {
	private static final Log log = Log.getLog();
	public static final String CHANGE_TYPE_DELETED = "Deleted";
    public static final String CHANGE_TYPE_ADDED = "Added";
    public static final String CHANGE_TYPE_MODIFIED = "Changed";
    public static final String NEWLINE = System.getProperty("line.separator", "\n");
    public static final String COMMAND_TERMINATOR = ";";
    private static final Comparator columnComparator = new Comparator() {
    	public int compare(Object o1, Object o2) {
    		if(o1 instanceof Map && o2 instanceof Map) {
    			Map m1 = (Map)o1;
    			Map m2 = (Map)o2;
				Comparable id1 = (Comparable) m1.get("COLUMN_INDEX");
				Comparable id2 = (Comparable) m2.get("COLUMN_INDEX");
    			if(id1 != null) {
    				if(id2 == null) return 1;
    				else return id1.compareTo(id2);
    			} else if(id2 != null) return -1;
    			Comparable nm1 = (Comparable)m1.get("COLUMN_NAME");
    			Comparable nm2 = (Comparable)m2.get("COLUMN_NAME");
    			if(nm1 != null) {
    				if(nm2 == null) return 1;
    				else return nm1.compareTo(nm2);
    			} else if(nm2 != null) return -1;
    		}
    		return 0;
    	}
    };

    public static interface Change {
    	public String getChangeType();
    	public String getSQL();
    	public String getEntityName() ;
    	public String getSubEntityName() ;
    }

    public static abstract class ColumnsObjectChange implements Change {
		protected final Map[] columns;
		protected final Map attributes;
		protected final String type;
    	public ColumnsObjectChange(Map attrs, Map[] changedColumns, String objectType) {
			this.attributes = attrs;
			this.columns = changedColumns;
            Arrays.sort(columns, columnComparator);
			this.type = objectType;
    	}
    	public String getEntityName() {
    		String owner = (String)attributes.get("OWNER");
    		String name = (String)attributes.get("NAME");
    		return (owner == null || owner.trim().length() == 0 ? "" : owner + ".")	+ name;
    	}
    	public String getSubEntityName() {
    		String owner = (String)attributes.get("SUB_OWNER");
    		String name = (String)attributes.get("SUB_NAME");
    		return (owner == null || owner.trim().length() == 0 ? "" : owner + ".")	+ name;
    	}
    	public String getObjectType() {
    		return type;
    	}
    	@Override
		public String toString() {
    		return getEntityName();
    	}
    	protected String getDropSQL() {
    		return "DROP " + getObjectType() + " " + getEntityName() + COMMAND_TERMINATOR + NEWLINE;
    	}
    	protected String getDropIndexSQL() {
			return "DROP INDEX " + getSubEntityName() + COMMAND_TERMINATOR + NEWLINE;
    	}
    	protected String getDropConstraintSQL() {
    		return "ALTER TABLE " + getEntityName() + " DROP CONSTRAINT " + getSubEntityName() + COMMAND_TERMINATOR + NEWLINE;
    	}
    	protected String getDropGrantSQL() {
    		String s = "";
    		for(int i = 0; i < columns.length; i++) {
    			s += "REVOKE " + columns[i].get("COLUMN_NAME") + " ON " + getEntityName() + " FROM " + (String)attributes.get("GRANTEE") + COMMAND_TERMINATOR + NEWLINE;
    		}
    		return s + NEWLINE;
    	}
    	protected String getCreateGrantSQL() {
    		String s = "";
    		for(int i = 0; i < columns.length; i++) {
    			s += "GRANT " + columns[i].get("COLUMN_NAME") + " ON " + getEntityName() + " TO " + (String)attributes.get("GRANTEE");
    			if("YES".equalsIgnoreCase((String)attributes.get("GRANTABLE")))
    				s += " WITH GRANT OPTION";
    			s += COMMAND_TERMINATOR + NEWLINE;
    		}
    		return s + NEWLINE;
    	}
    	protected String getCreateConstraintSQL() {
    		String constraintType = (String)attributes.get("CONSTRAINT_TYPE");
    		String newname = (String)attributes.get("NEW_NAME");
    		if(newname.length() > 30) {
    			StringBuilder sb = new StringBuilder(newname);
    			for(int p = 3; p < sb.length() && sb.length() > 30; p++) {
    				int next = sb.indexOf("_", p);
    				if(p == -1) sb.setLength(30);
    				sb.delete(++p, next);
    			}
    			newname = sb.toString();
    		}
    		String s = "ALTER TABLE " + getEntityName() +
			" ADD CONSTRAINT " + newname + " ";
			if(constraintType.equalsIgnoreCase("P")) {
				s += "PRIMARY KEY";
			} else if(constraintType.equalsIgnoreCase("R")) {
				s += "FOREIGN KEY";
			}
			s += " (";
			for(int i = 0; i < columns.length; i++) {
    			if(i > 0) s += "," + NEWLINE;
    			s += "\t" + columns[i].get("COLUMN_NAME");
    		}
			if(constraintType.equalsIgnoreCase("R")) {
				s += ") REFERENCES ";
				if(columns.length < 1) {
					 s += " ??? NO COLUMNS ???";
				} else {
					s += columns[0].get("REF_TABLE_OWNER") + "." + columns[0].get("REF_TABLE_NAME") + "(";
				}
				for(int i = 0; i < columns.length; i++) {
        			if(i > 0) s += "," + NEWLINE;
					s += "\t" + columns[i].get("REF_COLUMN_NAME");
        		}
			}
    		s += ")" + NEWLINE + COMMAND_TERMINATOR + NEWLINE;
			return s;
    	}
    	protected String getCreateTableSQL() {
			String s = "CREATE TABLE " + getEntityName() + "(" + NEWLINE;
			for(int i = 0; i < columns.length; i++) {
    			if(i > 0) s += "," + NEWLINE;
				s += "\t" + formatColumnSQL(columns[i], false);
    		}
    		s += ")" + NEWLINE + COMMAND_TERMINATOR + NEWLINE;
    		return s;
    	}
    	protected String getCreateIndexSQL() {
    		String s = "CREATE ";
    		String ct = (String)attributes.get("INDEX_TYPE");
    		if(ct != null && ct.trim().length() > 0 ) s += ct + " ";
    		s += "INDEX " + getSubEntityName()
			+ " ON " + getEntityName() + "(" + NEWLINE;
			for(int i = 0; i < columns.length; i++) {
				if(i > 0) s += "," + NEWLINE;
				s += "\t" + columns[i].get("COLUMN_NAME");
			}
			s += ")";
			Number compress = (Number)attributes.get("COMPRESS");
			if(compress != null && compress.intValue() > 0) s += NEWLINE + "\tCOMPRESS " + compress;
			String otherAttrs = (String) attributes.get("OTHER");
			if(otherAttrs != null && otherAttrs.length() > 0)
				s += NEWLINE + otherAttrs;
			s += NEWLINE + COMMAND_TERMINATOR + NEWLINE;
			return s;
    	}
    	protected String getAlterTableSQL() {
    		String s = "ALTER TABLE " + getEntityName();
			for(int i = 0; i < columns.length; i++) {
				s += NEWLINE + "\t" + columns[i].get("CHANGE_TYPE") + "(" + formatColumnSQL(columns[i], true) + ")";
    		}
    		s += NEWLINE + COMMAND_TERMINATOR + NEWLINE;
    		return s;
    	}
    }

    public static class ColumnsObjectDelete extends ColumnsObjectChange {
		public ColumnsObjectDelete(Map attrs, Map[] changedColumns, String type) {
			super(attrs, changedColumns, type);
		}
		public String getChangeType() {
    		return getObjectType() + " DELETED";
    	}
    	public String getSQL() {
    		if(type.equalsIgnoreCase("CONSTRAINT")) {
    			return getDropConstraintSQL();
    		} else if(type.equalsIgnoreCase("INDEX")) {
    			return getDropIndexSQL();
    		} else if(type.equalsIgnoreCase("GRANT")) {
    			return getDropGrantSQL();
    		} else {
    			return getDropSQL();
    		}
    	}
    }

    public static class ColumnsObjectAdd extends ColumnsObjectChange {
		public ColumnsObjectAdd(Map attrs, Map[] changedColumns, String type) {
			super(attrs, changedColumns, type);
		}
		public String getChangeType() {
    		return getObjectType() + " ADDED";
    	}
    	public String getSQL() {
    		if(type.equalsIgnoreCase("CONSTRAINT")) {
    			return getCreateConstraintSQL();
    		} else if(type.equalsIgnoreCase("TABLE")) {
    			return getCreateTableSQL();
    		} else if(type.equalsIgnoreCase("INDEX")) {
    			return getCreateIndexSQL();
    		} else if(type.equalsIgnoreCase("GRANT")) {
    			return getCreateGrantSQL();
    		}
    		return null;
    	}
    }

    public static class ColumnsObjectModify extends ColumnsObjectChange {
		public ColumnsObjectModify(Map attrs, Map[] changedColumns, String type) {
			super(attrs, changedColumns, type);
		}
		public String getChangeType() {
    		return getObjectType() + " CHANGED";
    	}
    	public String getSQL() {
    		if(type.equalsIgnoreCase("CONSTRAINT")) {
    			return getDropConstraintSQL() + getCreateConstraintSQL();
    		} else if(type.equalsIgnoreCase("TABLE")) {
    			return getAlterTableSQL();
    		} else if(type.equalsIgnoreCase("INDEX")) {
    			return getDropIndexSQL() + getCreateIndexSQL();
    		} else if(type.equalsIgnoreCase("GRANT")) {
    			return getDropGrantSQL() + getCreateGrantSQL();
    		}
    		return null;
    	}
    }

    public static abstract class SourceObjectChange implements Change {
    	protected String text;
    	protected Map attributes;
    	protected String type;
    	public SourceObjectChange(Map attrs, String sourceText, String objectType) {
    		attributes = attrs;
    		text = sourceText;
    		type = objectType;
    	}
    	public String getEntityName() {
    		String owner = (String)attributes.get("OWNER");
    		String name = (String)attributes.get("NAME");
    		return (owner == null || owner.trim().length() == 0 ? "" : owner + ".")	+ name;
    	}
    	public String getSubEntityName() {
    		String owner = (String)attributes.get("SUB_OWNER");
    		String name = (String)attributes.get("SUB_NAME");
    		return (owner == null || owner.trim().length() == 0 ? "" : owner + ".")	+ name;
    	}
    	public String getObjectType() {
    		return type;
    	}
    	public String getSourceText() {
    		return text;
    	}
    	@Override
		public String toString() {
    		return getEntityName();
    	}
    	protected String getPreSQL() {
    	    return ""; //"ALTER SESSION SET CURRENT_SCHEMA = " + getSchemaName() + ";" + NEWLINE;
    	}
    	protected String getDropSQL() {
    		if(getObjectType().equalsIgnoreCase("SYNONYM")) {
				return "DROP " + getObjectType() + " " + getSubEntityName() + COMMAND_TERMINATOR + NEWLINE;
    		} else {
				return "DROP " + getObjectType() + " " + getEntityName() + COMMAND_TERMINATOR + NEWLINE;
    		}
    	}
    	protected String getCreateSQL() {
    	    String s = getPreSQL();
    		if(getObjectType().equalsIgnoreCase("SEQUENCE")) {
				s += "CREATE SEQUENCE " + getEntityName() + COMMAND_TERMINATOR + NEWLINE;
    		} else if(getObjectType().equalsIgnoreCase("SYNONYM")) {
    			s += "CREATE SYNONYM " + getSubEntityName() + NEWLINE + "\tFOR " +
			 	getEntityName();
    			String dbLink = (String)attributes.get("DB_LINK");
    			if(dbLink != null && dbLink.trim().length() > 0) {
    				s += "@" + dbLink;
    			}
    			s += NEWLINE + COMMAND_TERMINATOR + NEWLINE;
    			return s;
    		} else if(getObjectType().equalsIgnoreCase("VIEW")) {
    		    s += "CREATE OR REPLACE FORCE " + getSourceText() + NEWLINE + "/" + NEWLINE;
    		} else {
    		    s += "CREATE OR REPLACE " + getSourceText() + NEWLINE + "/" + NEWLINE;
    		}
    		return s;
    	}
    }

    public static class SourceObjectDelete extends SourceObjectChange {
		public SourceObjectDelete(Map attrs, String sourceText, String type) {
			super(attrs, sourceText, type);
		}
		public String getChangeType() {
    		return getObjectType() + " DELETED";
    	}
    	public String getSQL() {
    		return getDropSQL();
    	}
    }

    public static class SourceObjectAdd extends SourceObjectChange {
		public SourceObjectAdd(Map attrs, String sourceText, String type) {
			super(attrs, sourceText, type);
		}
		public String getChangeType() {
    		return getObjectType() + " ADDED";
    	}
    	public String getSQL() {
    		return getCreateSQL();
    	}
    }

    public static class SourceObjectModify extends SourceObjectChange {
		public SourceObjectModify(Map attrs, String sourceText, String type) {
			super(attrs, sourceText, type);
		}
		public String getChangeType() {
    		return getObjectType() + " CHANGED";
    	}
    	public String getSQL() {
    		return getCreateSQL();
    	}
    }

    public static abstract class ColumnsAndSourceObjectChange extends ColumnsObjectChange {
    	protected String text;
    	public ColumnsAndSourceObjectChange(Map attrs, Map[] changedColumns, String sourceText, String objectType) {
    		super(attrs, changedColumns, objectType);
    		text = sourceText;
    	}
    	protected String getCreateViewSQL() {
			String s = "CREATE OR REPLACE FORCE VIEW " + getEntityName() + "(" + NEWLINE;
    		for(int i = 0; i < columns.length; i++) {
    			if(i > 0) s += "," + NEWLINE;
    			s += "\t" + columns[i].get("COLUMN_NAME");
    		}
    		s += ")" + NEWLINE + " AS " + NEWLINE + getSourceText() + NEWLINE + "/" + NEWLINE;
    		return s;
    	}
     	public String getSourceText() {
    		return text;
    	}
    }

    public static class ColumnsAndSourceObjectDelete extends ColumnsAndSourceObjectChange {
		public ColumnsAndSourceObjectDelete(Map attrs, Map[] changedColumns, String sourceText, String type) {
			super(attrs, changedColumns, sourceText, type);
		}
		public String getChangeType() {
    		return getObjectType() + " DELETED";
    	}
    	public String getSQL() {
    		return getDropSQL();
    	}
    }
    public static class ColumnsAndSourceObjectAdd extends ColumnsAndSourceObjectChange {
		public ColumnsAndSourceObjectAdd(Map attrs, Map[] changedColumns, String sourceText, String type) {
			super(attrs, changedColumns, sourceText, type);
		}
		public String getChangeType() {
    		return getObjectType() + " ADDED";
    	}
    	public String getSQL() {
    		return getCreateViewSQL();
    	}
    }
    public static class ColumnsAndSourceObjectModify extends ColumnsAndSourceObjectChange {
		public ColumnsAndSourceObjectModify(Map attrs, Map[] changedColumns, String sourceText, String type) {
			super(attrs, changedColumns, sourceText, type);
		}
		public String getChangeType() {
    		return getObjectType() + " CHANGED";
    	}
    	public String getSQL() {
    		return getCreateViewSQL();
    	}
    }

    /** Creates new DBCompare */
    public DBChanges() {
    }
	protected static final java.util.Set NO_PRECISION_TYPES = new java.util.HashSet(java.util.Arrays.asList(new String[] {
			"DATE", "LONG", "LONG RAW", "TIMESTAMP"
	}));
	protected static final java.util.Set IGNORE_OBJECTS = new java.util.HashSet(java.util.Arrays.asList(new String[] {
			"MICROSOFTDTPROPERTIES",
			"MICROSOFTSEQDTPROPERTIES",
			"MICROSOFT_PK_DTPROPERTIES",
			"DT_ADDUSEROBJECT",
			"DT_DROPUSEROBJECTBYID",
			"DT_SETPROPERTYBYID",
			"PLAN_TABLE"
	}));
    protected static String formatColumnSQL(Map attributes, boolean modify) {
    	String ct = (String) attributes.get("CHANGE_TYPE");
    	String s = "" + attributes.get("COLUMN_NAME");
    	if(ct == null || !ct.equalsIgnoreCase("DROP")) {
    		String dataType = (String)attributes.get("TYPE_NAME");
    		s += " " + dataType;
    		if(!NO_PRECISION_TYPES.contains(dataType)) {
		    	Number dd = (Number)attributes.get("DECIMAL_DIGITS");
		    	Number prec = (Number)attributes.get("NUM_PREC_RADIX");
		    	if(prec != null) {
		    		s += "(" + prec;
		    		if(dd != null && dd.intValue() > 0) s += "," + dd;
		    		s += ")";
		    	}
    		}
	    	Number nullable = (Number)attributes.get("NULLABLE");
	    	String def = (String)attributes.get("COLUMN_DEF");
	    	if(def != null && def.trim().length() > 0 && !def.equalsIgnoreCase("NULL")) {
	    		s += " DEFAULT " + def.trim();
	    	} else if(modify) {
	    		s += " DEFAULT NULL";
	    	}
	    	if(nullable.intValue() == 0)
	    		s += " NOT NULL";// not nullable
	    	else if(modify)
	    		s += " NULL";
    	}
    	return s;
    }

    public static Change[] findChanges(Connection con1, Connection con2, String[] schemas, java.util.Set types) throws SQLException {
        DatabaseMetaData md1 = (con1 == null ? null : con1.getMetaData());
        DatabaseMetaData md2 = (con2 == null ? null : con2.getMetaData());
        //Entity diffs = new Entity();
        List changes = new ArrayList();
        java.util.Set removedObjects = new java.util.HashSet();
        //compareColumns(diffs, changes, md1, md2, schema);
        //compareProcedures(changes, md1, md2, schema, null);
        if(types != null) for(java.util.Iterator iter = types.iterator(); iter.hasNext(); ) types.add(((String)iter.next()).trim().toUpperCase());
        for(int i = 0; i < objectTypes.length; i++) {
        	if(types == null || types.contains(objectTypes[i]))
        		compareObjects(changes, md1, md2, schemas, objectTypes[i], removedObjects);
        }
        return (Change[])changes.toArray(new Change[changes.size()]);
    }

    public static String getTableType(DatabaseMetaData md, String schema, String table) throws SQLException {
    	ResultSet rs = md.getTables(null, schema, table, null);
    	if(rs.next()) {
    		return rs.getString("TABLE_TYPE");
    	}
    	return null;
    }

    protected static boolean areEqual(Object o1, Object o2) {
        return areEqual(o1,o2,true);
    }

    protected static boolean areEqual(Object o1, Object o2, boolean strict) {
        if(o1 == null) return (o2 == null);
        try {
        	if(!strict) {
	        	if(o1 instanceof String) o1 = ((String)o1).trim().toUpperCase();
	        	if(o2 instanceof String) o2 = ((String)o2).trim().toUpperCase();
        	}
        	return o1.equals(o2);
        } catch(ClassCastException e) {
            return false;
        }
    }
    /*
     * Compares ignoring extra white-space
     */
    protected static boolean isSourceSame(String source1, String source2) {
    	if(source1 == null) return source2 == null;
    	source1 = RegexUtils.substitute("\\s+", source1.trim(), " ", null);
    	source2 = RegexUtils.substitute("\\s+", source2.trim(), " ", null);
		int i = 0;
		int len = Math.min(source1.length(), source2.length());
		for(; i < len; i++) {
			char ch1 = Character.toUpperCase(source1.charAt(i));
			char ch2 = Character.toUpperCase(source2.charAt(i));
			if(ch1 != ch2) {
				System.out.println("Sources do not match at character " + (i + 1) + ":\n\t" + source1 + "\nvs:\n\t" + source2);
				return false;
			}
		}
		if(source1.length() != source2.length()) {
			System.out.println("Sources do not match at character " + len + ":\n\t" + source1 + "\nvs:\n\t" + source2);
			return false;
		}
		return true;
    }

    protected static void trace(String s) {
        System.out.println(s);
    }

    protected static int compareUpTo(Object o1, Object o2, String[] order, int index) {
        for(int i = 0; i < index+1; i++) {
            Comparable c1 = (Comparable)((Map)o1).get(order[i]);
            Comparable c2 = (Comparable)((Map)o2).get(order[i]);
            int r = c1.compareTo(c2);
            if(r != 0) return r;
        }
        return 0;
    }

    private static Map[] getColumns(DatabaseMetaData md, Map attributes, String type) throws SQLException {
    	String sql = getColumnsSQL(md.getDatabaseProductName(), type);
    	if(sql == null) return null;
    	System.out.println("SQL --> " + sql);
    	PreparedStatement ps = md.getConnection().prepareStatement(sql);
    	try {
    		String owner = (String)attributes.get("SUB_OWNER");
    		if(owner == null) owner = (String)attributes.get("OWNER");
    		String name = (String)attributes.get("SUB_NAME");
    		if(name == null)
    			name = (String)attributes.get("NAME");
	    	ps.setString(1,owner);
	    	ps.setString(2,name);
	    	ResultSet rs = ps.executeQuery();
	    	ResultSetMetaData rsmd = rs.getMetaData();
	        String[] columnNames = new String[rsmd.getColumnCount()];
            for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
            List cols = new ArrayList();
            while(rs.next()) {
				Map<String, Object> m = new CaseInsensitiveHashMap<Object>();
                for(int i = 0; i < columnNames.length; i++) {
                	Object val =  rs.getObject(i+1);
                	if(val instanceof String) {
                		val = ((String)val).trim();
                		if("COLUMN_DEF".equalsIgnoreCase(columnNames[i]) && "NULL".equalsIgnoreCase(val.toString()))
                    		val = null;
                	}
                	m.put(columnNames[i], val);
                }
                cols.add(m);
            }
	        return (Map[])cols.toArray(new Map[cols.size()]);
    	} finally {
    		ps.close();
    	}
    }

    /*
    private static ResultSet getColumns(DatabaseMetaData md, String schemaPattern) throws SQLException {
    	String sql = getColumnsSQL(md.getDatabaseProductName());
    	if(sql == null) return null;
    	System.out.println("SQL --> " + sql);
    	PreparedStatement ps = md.getConnection().prepareStatement(sql);
    	ps.setString(1,schemaPattern);
    	ResultSet rs = ps.executeQuery();
    	return rs;
    }*/

    private static String getColumnsSQL(String dbProduct, String type) {
		if("ORACLE".equalsIgnoreCase(dbProduct)) {
			if(type.equalsIgnoreCase("TABLE")) {
				return "SELECT C.COLUMN_NAME, NULL DATA_TYPE, C.DATA_TYPE TYPE_NAME, " +
						"C.DATA_LENGTH COLUMN_SIZE, C.DATA_SCALE DECIMAL_DIGITS, " +
						"CASE WHEN C.DATA_TYPE = 'NUMBER' THEN C.DATA_PRECISION ELSE NVL(C.DATA_PRECISION, C.DATA_LENGTH) END NUM_PREC_RADIX, " +
						"CASE WHEN C.NULLABLE = 'Y' THEN 1 WHEN C.NULLABLE = 'N' THEN 0 ELSE 2 END NULLABLE, " +
						"C.DATA_DEFAULT COLUMN_DEF, COLUMN_ID COLUMN_INDEX " +
						"FROM ALL_TAB_COLUMNS C " +
						"WHERE C.OWNER LIKE ? " +
						"AND C.TABLE_NAME LIKE ? " +
						"ORDER BY C.COLUMN_NAME";
			} else if(type.equalsIgnoreCase("VIEW")) {
				return "SELECT C.COLUMN_NAME, C.COLUMN_ID COLUMN_INDEX " +
						"FROM ALL_TAB_COLUMNS C " +
						"WHERE C.OWNER LIKE ? " +
						"AND C.TABLE_NAME LIKE ? " +
						"ORDER BY C.COLUMN_ID";
			} else if(type.equalsIgnoreCase("CONSTRAINT")) {
				return "SELECT C1.COLUMN_NAME, O.R_OWNER REF_TABLE_OWNER, " +
						"C2.TABLE_NAME REF_TABLE_NAME, C2.COLUMN_NAME REF_COLUMN_NAME, C1.POSITION COLUMN_INDEX " +
						"FROM ALL_CONS_COLUMNS C1, ALL_CONSTRAINTS O, ALL_CONS_COLUMNS C2 " +
						"WHERE O.OWNER = ? AND O.CONSTRAINT_NAME = ? " +
						"AND C1.OWNER = O.OWNER " +
						"AND C1.CONSTRAINT_NAME = O.CONSTRAINT_NAME " +
						"AND O.R_OWNER = C2.OWNER (+) " +
						"AND O.R_CONSTRAINT_NAME = C2.CONSTRAINT_NAME (+) " +
						"AND C1.POSITION = NVL(C2.POSITION, C1.POSITION) " +
						"ORDER BY C1.POSITION, C1.COLUMN_NAME";
			} else if(type.equalsIgnoreCase("INDEX")) {
				return "SELECT NVL(DBADMIN.GET_LONG('SYS.COL$', 'DEFAULT$', C.ROWID), I.COLUMN_NAME) COLUMN_NAME, I.DESCEND, I.COLUMN_POSITION COLUMN_INDEX " +
						"FROM ALL_IND_COLUMNS I " +
						"LEFT OUTER JOIN (SYS.COL$ C " +
						"JOIN SYS.ICOL$ IC ON IC.BO# = C.OBJ# AND IC.INTCOL# = C.INTCOL# " +
						"JOIN SYS.OBJ$ IDX ON IC.OBJ# = IDX.OBJ# " +
						"JOIN SYS.USER$ IO ON IO.USER# = IDX.OWNER# " +
						") ON I.COLUMN_NAME LIKE 'SYS_%$' AND I.INDEX_NAME = IDX.NAME AND I.INDEX_OWNER = IO.NAME AND I.COLUMN_POSITION = IC.POS# " + 
						"WHERE I.INDEX_OWNER = ? " +
						"AND I.INDEX_NAME = ? " +
						"ORDER BY I.COLUMN_POSITION, 1";
			} else if(type.equalsIgnoreCase("GRANT")) {
				return "SELECT PRIVILEGE COLUMN_NAME, GRANTABLE" +
						"FROM DBA_TAB_PRIVS " +
						"WHERE OWNER = ? " +
						"AND TABLE_NAME = ? " +
						"ORDER BY PRIVILEGE";
			}
		} else if("POSTGRESQL".equalsIgnoreCase(dbProduct)) {
			return "SELECT a.attname as COLUMN_NAME, NULL as DATA_TYPE, t.typname as TYPE_NAME, a.attlen as COLUMN_SIZE, 0 as DECIMAL_DIGITS, 0 as NUM_PREC_RADIX, "
					+ "CASE WHEN a.attnotnull THEN 0 ELSE 2 END as NULLABLE, ad.adsrc as COLUMN_DEF, a.attnum as COLUMN_INDEX "
					+ "FROM pg_attribute a join pg_class c on a.attrelid = c.oid join pg_namespace n on c.relnamespace = n.oid "
					+ "join pg_type t on a.atttypid = t.oid left outer join pg_attrdef ad on a.attrelid = ad.adrelid and a.attnum = ad.adnum"
					+ " WHERE n.nspname = ? AND c.relname = ? "
					+ "ORDER BY a.attname";
    	}
    	return null;
    }

    //--- This is a new way of doing it
    private static String[] objectTypes = new String[] {
    		"SEQUENCE", "TABLE", "TYPE", "FUNCTION", "SYNONYM", "PROCEDURE",
			"PACKAGE BODY", "PACKAGE", "VIEW", "CONSTRAINT",
			"INDEX", "TRIGGER"
    };
    private static java.util.Set columnObjectTypeSet = new java.util.HashSet();
    private static java.util.Set sourceObjectTypeSet = new java.util.HashSet();
    static {
    	columnObjectTypeSet.add("TABLE");
    	columnObjectTypeSet.add("VIEW");
    	columnObjectTypeSet.add("CONSTRAINT");
    	columnObjectTypeSet.add("INDEX");
    	columnObjectTypeSet.add("GRANT");

    	sourceObjectTypeSet.add("TYPE");
    	sourceObjectTypeSet.add("FUNCTION");
    	sourceObjectTypeSet.add("PROCEDURE");
    	sourceObjectTypeSet.add("PACKAGE BODY");
    	sourceObjectTypeSet.add("PACKAGE");
    	sourceObjectTypeSet.add("VIEW");
    	sourceObjectTypeSet.add("TRIGGER");
    }

    private static int getTypeIndex(String type) {
    	for(int i = 0; i < objectTypes.length; i++) {
    		if(objectTypes[i].equalsIgnoreCase(type)) return i;
    	}
    	return -1;
    }
    private static String formatIgnoreList() {
    	String s = "";
    	for(java.util.Iterator iter = IGNORE_OBJECTS.iterator(); iter.hasNext(); ) {
    		String name = (String)iter.next();
    		s += "'" + name + "'";
    		if(iter.hasNext()) s += ",";
    	}
    	return s;
    }
    private static String getObjectsSQL(String dbProduct, int dbMajorVersion, int dbMinorVersion, String type) {
    	boolean oracle10 = "ORACLE".equalsIgnoreCase(dbProduct) && dbMajorVersion >= 10;
		if("ORACLE".equalsIgnoreCase(dbProduct)) {
    	if(type.equalsIgnoreCase("VIEW")) {
    		return "SELECT OWNER, VIEW_NAME NAME FROM ALL_VIEWS " +
    				"WHERE OWNER LIKE ? " +
					"AND VIEW_NAME NOT IN(" + formatIgnoreList() + ") " +
    				"ORDER BY OWNER, VIEW_NAME";
    	} else if(type.equalsIgnoreCase("CONSTRAINT")) {
    		return "SELECT CONSTRAINT_TYPE, OWNER, TABLE_NAME NAME, " +
    				"CASE WHEN CONSTRAINT_TYPE = 'P' THEN 'PK_' || TABLE_NAME " +
    				"WHEN CONSTRAINT_TYPE = 'R' THEN 'FK_' || TABLE_NAME || '_' || " +
    				"ADHERE(CAST(COLLECT(COLUMN_NAME) AS VARCHAR2_TABLE), '_') ELSE CONSTRAINT_NAME END NEW_NAME, CONSTRAINT_NAME SUB_NAME " +
    				"FROM (SELECT O.CONSTRAINT_TYPE, O.OWNER, O.TABLE_NAME, O.CONSTRAINT_NAME, C.COLUMN_NAME " +
    				"FROM ALL_CONSTRAINTS O " +
    				"JOIN ALL_CONS_COLUMNS C ON C.CONSTRAINT_NAME = O.CONSTRAINT_NAME AND C.OWNER = O.OWNER " +
    				"WHERE O.CONSTRAINT_TYPE IN('P', 'R') " +
					"AND O.TABLE_NAME NOT LIKE 'BIN$%==$0' " +
					"AND O.CONSTRAINT_NAME NOT LIKE 'BIN$%==$0' " +
					"AND O.TABLE_NAME NOT IN(" + formatIgnoreList() + ") " +
    				"AND O.OWNER LIKE ? " +
    				"ORDER BY O.CONSTRAINT_TYPE, O.OWNER, O.TABLE_NAME, O.CONSTRAINT_NAME, C.POSITION) " +
    				"GROUP BY CONSTRAINT_TYPE, OWNER, TABLE_NAME, CONSTRAINT_NAME " +
    				"ORDER BY CONSTRAINT_TYPE, OWNER, TABLE_NAME, NEW_NAME";
    	} else if(type.equalsIgnoreCase("INDEX")) {
    		return "SELECT I.OWNER SUB_OWNER, I.INDEX_NAME SUB_NAME, I.TABLE_OWNER OWNER, I.TABLE_NAME NAME, " +
    				"CASE WHEN I.INDEX_TYPE LIKE '%BITMAP' THEN 'BITMAP' " +
    				"WHEN I.UNIQUENESS = 'UNIQUE' THEN 'UNIQUE' " +
    				"ELSE '' END INDEX_TYPE, I.PREFIX_LENGTH \"COMPRESS\", " +
    				"CASE WHEN PARTITIONED = 'YES' THEN 'LOCAL' END OTHER " +
    				"FROM ALL_INDEXES I " +
    				"LEFT OUTER JOIN ALL_CONSTRAINTS C ON C.CONSTRAINT_TYPE IN('P', 'U') AND C.INDEX_NAME = I.INDEX_NAME AND NVL(C.INDEX_OWNER, C.OWNER) = I.OWNER " +
    				"WHERE I.TABLE_OWNER = ? " +
					(oracle10 ? "AND I.DROPPED = 'NO' " : "") +
					"AND I.TABLE_NAME NOT IN(" + formatIgnoreList() + ") " +
					"AND (C.INDEX_NAME IS NULL OR C.INDEX_NAME NOT LIKE 'SYS_C%')" +
					"ORDER BY I.OWNER, I.INDEX_NAME, I.TABLE_OWNER, I.TABLE_NAME";
    	} else if(type.equalsIgnoreCase("TABLE")) {
    		return "SELECT OWNER, TABLE_NAME, TABLE_NAME NAME " +
					"FROM ALL_TABLES " +
					"WHERE OWNER LIKE ? " +
					"AND TABLE_NAME NOT IN(" + formatIgnoreList() + ") " +
					(oracle10 ? "AND DROPPED = 'NO' " : "") +
					"ORDER BY OWNER, TABLE_NAME";
    	} else if(type.equalsIgnoreCase("SEQUENCE")) {
    		return "SELECT SEQUENCE_OWNER OWNER, SEQUENCE_NAME NAME " +
    				"FROM ALL_SEQUENCES WHERE SEQUENCE_OWNER = ? " +
					"AND SEQUENCE_NAME NOT IN(" + formatIgnoreList() + ") " +
    				"ORDER BY SEQUENCE_OWNER, SEQUENCE_NAME";
    	} else if(type.equalsIgnoreCase("SYNONYM")) {
    		return "SELECT OWNER SUB_OWNER, SYNONYM_NAME SUB_NAME, TABLE_OWNER OWNER, TABLE_NAME NAME, DB_LINK " +
    				"FROM ALL_SYNONYMS WHERE OWNER = ? " +
					"AND SYNONYM_NAME NOT IN(" + formatIgnoreList() + ") " +
    				"ORDER BY OWNER, SYNONYM_NAME, TABLE_OWNER, TABLE_NAME, DB_LINK";
    	} else if(type.equalsIgnoreCase("TRIGGER")) {
    		return "SELECT OWNER, TRIGGER_NAME NAME " +
    				"FROM ALL_TRIGGERS WHERE OWNER = ? " +
					"ORDER BY OWNER, TRIGGER_NAME";
    	} else if(type.equalsIgnoreCase("GRANT")) {
    		return "SELECT OWNER, TABLE_NAME NAME, GRANTEE" +
    				"FROM DBA_TAB_PRIVS " +
    				"WHERE OWNER = ? " +
    				"ORDER BY OWNER, TABLE_NAME, GRANTEE";
    	} else {
    		return "SELECT DISTINCT OWNER, NAME FROM ALL_SOURCE " +
					"WHERE OWNER LIKE ? " +
					"AND TYPE = '" + type + "' " +
					"AND NAME NOT IN(" + formatIgnoreList() + ") " +
					(oracle10 ? "AND NAME NOT LIKE 'BIN$%' AND NAME NOT LIKE 'SYS%==' " : "") +
					"ORDER BY OWNER, NAME";
    	}
		} else if("POSTGRESQL".equalsIgnoreCase(dbProduct)) {
			if(type.equalsIgnoreCase("VIEW")) {
				return "SELECT schemaname as OWNER, viewname as NAME FROM pg_views " +
						"WHERE schemaname LIKE ? " +
						"ORDER BY viewname, viewname";
				/*
				} else if(type.equalsIgnoreCase("CONSTRAINT")) {
				return "SELECT CONSTRAINT_TYPE, OWNER, TABLE_NAME NAME, " +
						"CASE WHEN CONSTRAINT_TYPE = 'P' THEN 'PK_' || TABLE_NAME " +
						"WHEN CONSTRAINT_TYPE = 'R' THEN 'FK_' || TABLE_NAME || '_' || " +
						"(SELECT COLUMN_NAME FROM ALL_CONS_COLUMNS C " +
						"WHERE C.CONSTRAINT_NAME = O.CONSTRAINT_NAME AND C.OWNER = O.OWNER " +
						"AND ROWNUM = 1) " +
						"ELSE CONSTRAINT_NAME END NEW_NAME, CONSTRAINT_NAME SUB_NAME " +
						"FROM ALL_CONSTRAINTS O " +
						"WHERE CONSTRAINT_TYPE IN('P', 'R') " +
						"AND TABLE_NAME NOT LIKE 'BIN$%==$0' " +
						"AND CONSTRAINT_NAME NOT LIKE 'BIN$%==$0' " +
						"AND TABLE_NAME NOT IN(" + formatIgnoreList() + ") " +
						"AND OWNER LIKE ? " +
						"ORDER BY CONSTRAINT_TYPE, OWNER, TABLE_NAME, NEW_NAME";*/
			} else if(type.equalsIgnoreCase("INDEX")) {
				return "SELECT schemaname as SUB_OWNER, indexname as SUB_NAME, schemaname as OWNER, tablename as NAME, " +
						"'' INDEX_TYPE, 0 \"COMPRESS\" " +
						"FROM pg_indexes " +
						"WHERE schemaname LIKE ? " +
						"ORDER BY schemaname, indexname, schemaname, tablename";
			} else if(type.equalsIgnoreCase("TABLE")) {
				return "SELECT schemaname as OWNER, tablename as TABLE_NAME, tablename as NAME " +
						"FROM pg_tables " +
						"WHERE schemaname LIKE ? " +
						"ORDER BY schemaname, tablename";
				/*} else if(type.equalsIgnoreCase("TRIGGER")) {
					return "SELECT OWNER, TRIGGER_NAME NAME " +
							"FROM ALL_TRIGGERS WHERE OWNER = ? " +
							"ORDER BY OWNER, TRIGGER_NAME";
				} else if(type.equalsIgnoreCase("GRANT")) {
					return "SELECT OWNER, TABLE_NAME NAME, GRANTEE" +
							"FROM DBA_TAB_PRIVS " +
							"WHERE OWNER = ? " +
							"ORDER BY OWNER, TABLE_NAME, GRANTEE";*/
			} else if(type.equalsIgnoreCase("FUNCTION")) {
				return "SELECT nspname as OWNER, proname as NAME, proargtypes::oid[] as ARGUMENT_TYPES, proargnames as ARGUMENT_NAMES " +
						"FROM pg_proc p " +
						"join pg_namespace n on p.pronamespace = n.oid " +
						"WHERE nspname LIKE ? " +
						"ORDER BY nspname, proname, proargtypes::oid[]";
			}
		}
		return null;
    }

    private static String getObjectTextSQL(String dbProduct, int dbMajorVersion, int dbMinorVersion, String type) {
        boolean oracle10 = "ORACLE".equalsIgnoreCase(dbProduct) && dbMajorVersion >= 10;
		if("ORACLE".equalsIgnoreCase(dbProduct)) {
			if(type.equalsIgnoreCase("VIEW")) {
				return "SELECT TEXT, 1 FROM ALL_VIEWS " +
						"WHERE OWNER = ? AND VIEW_NAME = ?";
			} else if(type.equalsIgnoreCase("TRIGGER")) {
				/*return "SELECT TEXT, LINE FROM (" +
						"SELECT OWNER, TRIGGER_NAME, 'TRIGGER ' || DESCRIPTION TEXT, 1 LINE FROM ALL_TRIGGERS " +
						"UNION ALL SELECT OWNER, TRIGGER_NAME, TRIGGER_BODY, 2 FROM ALL_TRIGGERS) A " +
				"WHERE OWNER = ? AND TRIGGER_NAME = ?";*/
				/*return "SELECT TRIGGER_BODY, 1, 'TRIGGER ' || DESCRIPTION || CHR(13) HEADER " +
					   "FROM ALL_TRIGGERS " +
					   "WHERE OWNER = ? AND TRIGGER_NAME = ?";*/
				return "SELECT TRIGGER_BODY, 1, 'TRIGGER ' || OWNER || '.' || TRIGGER_NAME || CHR(13) "
						+ "|| DECODE(TRIGGER_TYPE, 'BEFORE EACH ROW', 'BEFORE', 'AFTER EACH ROW', 'AFTER', 'BEFORE STATEMENT', 'BEFORE', 'AFTER STATEMENT', 'AFTER', 'INSTEAD OF', 'INSTEAD OF', '***UNKNOWN: ' ||  TRIGGER_TYPE)"
						+ "|| ' ' || TRIGGERING_EVENT || ' ON ' || TABLE_OWNER || '.' || TABLE_NAME  || CHR(13) || REFERENCING_NAMES  || CHR(13)"
						+ "|| DECODE(TRIGGER_TYPE, 'BEFORE EACH ROW', 'FOR EACH ROW', 'AFTER EACH ROW', 'FOR EACH ROW', 'INSTEAD OF', 'FOR EACH ROW', '')  || CHR(13) HEADER "
						+ "FROM ALL_TRIGGERS "
						+ "WHERE OWNER = ? AND TRIGGER_NAME = ?";
			} else {
				return "SELECT DECODE(LINE, 1, REGEXP_REPLACE(TEXT, '(\\\"?' || OWNER || '\\\"?\\.)?\\\"?' || REGEXP_REPLACE(NAME, '([^A-Za-z_])', '\\1') || '\\\"?', OWNER || '.' || NAME, 1, 0, 'i'), TEXT) TEXT, LINE FROM ALL_SOURCE " +
						"WHERE OWNER = ? AND NAME = ? AND TYPE = '" + type +
						"' ORDER BY LINE";
			}
		} else if("POSTGRESQL".equalsIgnoreCase(dbProduct)) {
			if(type.equalsIgnoreCase("VIEW")) {
				return "SELECT definition, 1 FROM pg_views " +
						"WHERE schemaname = ? AND viewname = ?";
				/*} else if(type.equalsIgnoreCase("TRIGGER")) {
					return "SELECT OWNER, TRIGGER_NAME NAME " +
							"FROM ALL_TRIGGERS WHERE OWNER = ? " +
							"ORDER BY OWNER, TRIGGER_NAME";*/
			} else if(type.equalsIgnoreCase("FUNCTION")) {
				return "SELECT prosrc, 1 " +
						"FROM pg_proc p " +
						"join pg_namespace n on p.pronamespace = n.oid " +
						"WHERE nspname = ? AND proname = ? AND proargtypes::oid[] = ?";
			}
		}
		return null;
    }

    private static String getObjectText(DatabaseMetaData md, Map attributes, String type) throws SQLException {
    	String sql = getObjectTextSQL(md.getDatabaseProductName(), md.getDatabaseMajorVersion(), md.getDatabaseMinorVersion(), type);
    	if(sql == null) return null;
    	System.out.println("SQL --> " + sql);
    	java.sql.PreparedStatement ps = md.getConnection().prepareStatement(sql);
    	String s = "";
    	try {
	    	ps.setString(1,(String)attributes.get("OWNER"));
	    	ps.setString(2,(String)attributes.get("NAME"));
			if(ps.getParameterMetaData().getParameterCount() > 2) {
				ps.setObject(3, attributes.get("ARGUMENT_TYPES"));
			}
	    	ResultSet rs = ps.executeQuery();
	    	try {
		    	while(rs.next()) {
		    	    String txt = rs.getString(1);
		    	    if(rs.getMetaData().getColumnCount() > 2) {
		    	        s += rs.getString(3);
		    	    }
		    	    s += txt;
		    	}
	    	} finally {
	    		rs.close();
	    	}
    	} finally {
    		ps.close();
    	}
    	//convert other forms of newline into this system's new lines
    	s = RegexUtils.substitute("(\\n\\r?)|(\\r)", s, NEWLINE, null);
    	return s;
    }
    private static ResultSet getObjects(DatabaseMetaData md, String schemaPattern, String type) throws SQLException {
    	String sql = getObjectsSQL(md.getDatabaseProductName(), md.getDatabaseMajorVersion(), md.getDatabaseMinorVersion(), type);
    	if(sql == null) return null;
    	System.out.println("SQL --> " + sql);
    	PreparedStatement ps = md.getConnection().prepareStatement(sql);
    	ps.setString(1,schemaPattern);
    	ResultSet rs = ps.executeQuery();
    	return rs;
    }

    public static void compareObjects(List changes, DatabaseMetaData md1, DatabaseMetaData md2, String[] schemas, String type, java.util.Set removedObjects) throws SQLException {
        for(int i = 0; i < schemas.length; i++) compareObjects(changes, md1, md2, schemas[i], type, removedObjects);
    }

	protected static class ArraySortable implements Comparable {
		protected final Comparable[] array;

		public ArraySortable(Comparable[] array) {
			this.array = array;
		}

		public int compareTo(Object o) {
			Comparable[] other;
			if(o instanceof ArraySortable) {
				other = ((ArraySortable) o).array;
			} else
				try {
					other = ConvertUtils.convert(array.getClass(), o);
				} catch(ConvertException e) {
					throw new ClassCastException("Cannot convert " + o + " to " + array.getClass().getName());
				}
			if(other == array)
				return 0;
			for(int k = 0; k < array.length; k++) {
				if(array[k] == null) {
					if(k < other.length && other[k] != null)
						return -1;
				} else if(k >= other.length || other[k] == null)
					return 1;
				int d = array[k].compareTo(other[k]);
				if(d != 0)
					return d;
			}
			return 0;
		}
	}

	public static String getObjectText(DatabaseMetaData md, String type, String owner, String name, String arguments) throws SQLException {
		Map<String, String> attributes = new HashMap<String, String>();
		attributes.put("OWNER", owner);
		attributes.put("NAME", name);
		attributes.put("ARGUMENT_TYPES", arguments);

		return getObjectText(md, attributes, type);
	}
    public static void compareObjects(List changes, DatabaseMetaData md1, DatabaseMetaData md2, String schemaPattern, String type, java.util.Set removedObjects) throws SQLException {
        List list1 = new ArrayList();
        List list2 = new ArrayList();
        String[] columnNames = null;
		if(md1 != null) {
    		ResultSet rs1 = getObjects(md1, schemaPattern, type);
    		if(rs1 == null) {
            	System.out.println("Can not compare " + type + " because no result set exists for these");
            	return;
            }
    		try {
            	ResultSetMetaData rsmd = rs1.getMetaData();
            	columnNames = new String[rsmd.getColumnCount()];
                for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
                while(rs1.next()) {
					Map<String, Object> m = new CaseInsensitiveHashMap<Object>();
                    for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs1.getObject(i+1));
                    list1.add(m);
                }
    		} finally {
    			rs1.getStatement().close();
    			rs1.close();
    		}
    	}
    	if(md2 != null) {
    		ResultSet rs2 = getObjects(md2, schemaPattern, type);
    		if(rs2 == null) {
            	System.out.println("Can not compare " + type + " because no result set exists for these");
            	return;
            }
    		try {
                if(columnNames == null) {
                	ResultSetMetaData rsmd = rs2.getMetaData();
                	columnNames = new String[rsmd.getColumnCount()];
                    for(int i = 0; i < columnNames.length; i++) columnNames[i] = rsmd.getColumnName(i+1);
                }
                while(rs2.next()) {
					Map<String, Object> m = new CaseInsensitiveHashMap<Object>();
                    for(int i = 0; i < columnNames.length; i++) m.put(columnNames[i],rs2.getObject(i+1));
                    list2.add(m);
                }
    		} finally {
    			rs2.getStatement().close();
    			rs2.close();
    		}
    	}
        Map[] objects1 = (Map[])list1.toArray(new Map[list1.size()]);
        Map[] objects2 = (Map[])list2.toArray(new Map[list2.size()]);
        //hack for constraint real name
        int compareLength = columnNames.length;
		int identityLength = columnNames.length;
        if(type.equalsIgnoreCase("CONSTRAINT")) {
        	compareLength = 4;
			identityLength = 4;
		} else if(type.equalsIgnoreCase("INDEX")) {
			identityLength = 2;
		} else if(type.equalsIgnoreCase("SYNONYM")) {
			identityLength = 2;
        }
        int i1 = 0;
        int i2 = 0;
        while(i1 < objects1.length || i2 < objects2.length) {
            //check for missing object
			int r;
			if(i1 == objects1.length)
				r = 1;
			else if(i2 == objects2.length)
				r = -1;
			else
				r = compare(objects1[i1], objects2[i2], columnNames, 0, identityLength);

            boolean hasColumns = columnObjectTypeSet.contains(type);
            boolean hasSource = sourceObjectTypeSet.contains(type);
            if(r < 0) {
            	trace("MISSING from Database 2: " + objects1[i1]);
                Change change = null;
                if(hasColumns && hasSource) {
                	Map[] columns = getColumns(md1, objects1[i1], type);
	                String text = getObjectText(md1, objects1[i1], type);
                	change = new ColumnsAndSourceObjectDelete(objects1[i1], columns, text, type);
                } else if(hasColumns) {
                	Map[] columns = getColumns(md1, objects1[i1], type);
                	change = new ColumnsObjectDelete(objects1[i1], columns, type);
                } else if(hasSource) { //has source
	                String text = getObjectText(md1, objects1[i1], type);
	                change = new SourceObjectDelete(objects1[i1], text, type);
                }
                if(change != null && !(change instanceof ColumnsObjectDelete
                			&& (type.equalsIgnoreCase("CONSTRAINT")
                					|| type.equalsIgnoreCase("INDEX"))
							&& removedObjects.contains(((ColumnsObjectDelete)change).getEntityName()))) {
                	changes.add(change);
                	removedObjects.add(change.getEntityName());
                }
                i1++;
            } else if(r > 0) {
            	trace("MISSING from Database 1: " + objects2[i2]);
                Change change = null;
                if(hasColumns && hasSource) {
                	Map[] columns = getColumns(md2, objects2[i2], type);
	                String text = getObjectText(md2, objects2[i2], type);
                	change = new ColumnsAndSourceObjectAdd(objects2[i2], columns, text, type);
                } else if(hasColumns) {
                	Map[] columns = getColumns(md2, objects2[i2], type);
                	change = new ColumnsObjectAdd(objects2[i2], columns, type);
                } else { //has source
	                String text = getObjectText(md2, objects2[i2], type);
	                change = new SourceObjectAdd(objects2[i2], text, type);
                }
                if(change != null) changes.add(change);
                i2++;
            } else { // exists in both so compare source  text of both
				boolean altered = (0 != compare(objects1[i1], objects2[i2], columnNames, identityLength, compareLength));
            	 if(hasColumns && hasSource) {
                	Map[] columns1 = getColumns(md1, objects1[i1], type);
                	Map[] columns2 = getColumns(md2, objects2[i2], type);
                	//compare columns
                	Map[] changedColumns = getChangedColumns(columns1, columns2);
                	String text1 = getObjectText(md1, objects1[i1], type);
                    String text2 = getObjectText(md2, objects2[i2], type);
					if(altered || changedColumns.length > 0 || !isSourceSame(text1, text2)) {
                		changes.add(new ColumnsAndSourceObjectModify(objects2[i2], columns2, text2, type));
                	}
                 } else if(hasColumns) {
                	Map[] columns1 = getColumns(md1, objects1[i1], type);
                	Map[] columns2 = getColumns(md2, objects2[i2], type);
                	//compare columns
                	Map[] changedColumns = getChangedColumns(columns1, columns2);
					if(altered || changedColumns.length > 0) {
                		if(!type.equalsIgnoreCase("TABLE")) changedColumns = columns2;
                    	changes.add(new ColumnsObjectModify(objects2[i2], changedColumns, type));
                	}
                } else { //has source
                	String text1 = getObjectText(md1, objects1[i1], type);
                    String text2 = getObjectText(md2, objects2[i2], type);
					if(altered || !isSourceSame(text1, text2)) {
                		trace("UNEQUAL: \n" + text1 + "\nAND:\n" + text2);
                        changes.add(new SourceObjectModify(objects2[i2], text2, type));
                	}
            	}
                i1++;
                i2++;
            }
        }
    }

	protected static int compare(Map object1, Map object2, String[] columnNames, int start, int end) throws SQLException {
		for(int k = start; k < end; k++) {
			Object col1 = object1.get(columnNames[k]);
			Object col2 = object2.get(columnNames[k]);
			Comparable c1;
			Comparable c2;
			if(col1 instanceof Comparable || col1 == null)
				c1 = (Comparable) col1;
			else if(col1 instanceof Array) {
				try {
					c1 = new ArraySortable(ConvertUtils.convert(Comparable[].class, ((Array) col1).getArray()));
				} catch(ConvertException e) {
					throw new SQLException(e);
				}
			} else {
				log.warn("Could not convert object " + col1 + " (" + col1.getClass().getName() + ") to a Comparable");
				c1 = null;
			}
			if(col2 instanceof Comparable || col2 == null)
				c2 = (Comparable) col2;
			else if(col2 instanceof Array) {
				try {
					c2 = new ArraySortable(ConvertUtils.convert(Comparable[].class, ((Array) col2).getArray()));
				} catch(ConvertException e) {
					throw new SQLException(e);
				}
			} else {
				log.warn("Could not convert object " + col2 + " (" + col2.getClass().getName() + ") to a Comparable");
				c2 = null;
			}

			// trace("Checking " + c1 + " against " + c2 + " (" + procOrder[k] + ")");
			int r;
			if(c1 == null) {
				if(c2 == null)
					r = 0;
				else
					r = 1;
			} else if(c2 == null) {
				r = -1;
			} else {
				r = c1.compareTo(c2);
			}
			if(r != 0) {
				trace("Found difference: " + object1 + " vs " + object2);
				trace("NO MATCH: " + c1 + " vs " + c2);
				return r;
			}
		}
		return 0;
	}
    private static Map[] getChangedColumns(Map[] columns1, Map[] columns2) {
        List changedColumns = new ArrayList();
    	int i1 = 0;
        int i2 = 0;
        while(i1 < columns1.length || i2 < columns2.length) {
            //check for missing object
            int r = 0;
            if(i1 == columns1.length) r = 1;
            else if(i2 == columns2.length) r = -1;
            else {
            	String name1 = (String) columns1[i1].get("COLUMN_NAME");
            	String name2 = (String) columns2[i2].get("COLUMN_NAME");
            	r = name1.compareToIgnoreCase(name2);
                if(r != 0) {
                	trace("NO MATCH: " + name1 + " vs " + name2);
                }
            }
            if(r < 0) {
                String s = "Column MISSING from Database 2";
                Map m = new HashMap(columns1[i1]);
                m.put("CHANGE_TYPE", "DROP");
                changedColumns.add(m);
                i1++;
            } else if(r > 0) {
                String s = "Column MISSING from Database 1";
                Map m = new HashMap(columns2[i2]);
                m.put("CHANGE_TYPE", "ADD");
                changedColumns.add(m);
                i2++;
            } else { // exists in both so compare attributes of both
            	for(java.util.Iterator iter = columns1[i1].keySet().iterator(); iter.hasNext(); ) {
            		String key = (String)iter.next();
					if(!key.equalsIgnoreCase("COLUMN_NAME") && !key.equalsIgnoreCase("COLUMN_INDEX")) {
	                    Comparable c1 = (Comparable)columns1[i1].get(key);
	                    Comparable c2 = (Comparable)columns2[i2].get(key);
	                    //trace("Checking " + c1 + " against " + c2 + " (" + procOrder[k] + ")");
	                    if(c1 == null) {
	                    	if(c2 == null) r = 0;
	                    	else r = 1;
	                    } else if(c2 == null) {
	                    	r = -1;
	                    } else {
	                    	r = c1.compareTo(c2);
	                    }
	                    if(r != 0) {
	                    	trace("Found difference: " + columns1[i1] + " vs " + columns2[i2]);
	                    	trace("NO MATCH: " + c1 + " vs " + c2);
	                    	break;
	                    }
            		}
            	}
            	if(r != 0) {
	            	Map m = new HashMap(columns2[i2]);
	                m.put("CHANGE_TYPE", "MODIFY");
	                changedColumns.add(m);
            	}
                i1++;
                i2++;
            }
        }
        return (Map[])changedColumns.toArray(new Map[changedColumns.size()]);
    }
	/*
	    public void getChangedRows(List changes, DatabaseMetaData md1, DatabaseMetaData md2, String schema, String table, String filter) {

	    }*/
}
