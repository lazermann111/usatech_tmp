/*
 * Created on Oct 20, 2004
 *
 */
package simple.dbcompare;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Properties;

import org.apache.tools.ant.BuildException;

import simple.text.StringUtils;

/**
 * @author bkrug
 *
 */
public class DBCompareTask extends org.apache.tools.ant.Task {
	private static final String version = "1.4";
	private org.apache.tools.ant.types.Path classpath;
	private String driver;
	private String url1;
	private String url2;
	private String username1;
	private String username2;
	private String password1;
	private String password2;
	private String schemas;
	private java.io.File outputFile;
	private java.io.File reportFile;
	private java.util.Set objectTypes = null;

	/**
	 * @return Returns the schemas.
	 */
	public String[] getSchemas() {
		return StringUtils.split(schemas, ",;".toCharArray(), false);
	}
	/**
	 * @param schema The schema to set.
	 */
	public void setSchemas(String schemas) {
		this.schemas = schemas;
	}
	public static void main(String[] args) {

	}

    private  Driver getDriver() throws BuildException {
	    if(driver == null)
	        throw new BuildException("Driver attribute must be set!", getLocation());
	    Driver driverInstance = null;
	    try {
	        Class dc;
	        if(classpath != null) {
	        	log("Loading " + driver + " using AntClassLoader with classpath " + classpath, 3);
	        	org.apache.tools.ant.AntClassLoader loader = getProject().createClassLoader(classpath);
	            dc = loader.loadClass(driver);
	        } else {
	            log("Loading " + driver + " using system loader.", 3);
	            dc = Class.forName(driver);
	        }
	        driverInstance = (Driver)dc.newInstance();
	    } catch(ClassNotFoundException e) {
	        throw new BuildException("Class Not Found: JDBC driver " + driver + " could not be loaded", getLocation());
	    } catch(IllegalAccessException e) {
	        throw new BuildException("Illegal Access: JDBC driver " + driver + " could not be loaded", getLocation());
	    } catch(InstantiationException e) {
	        throw new BuildException("Instantiation Exception: JDBC driver " + driver + " could not be loaded", getLocation());
	    }
	    return driverInstance;
	}

    @Override
	public void execute() throws BuildException {
    	log("DBCompare v." + version, 2);
    	if(url1 != null) {
            if(username1 == null)
	            throw new BuildException("User Name 1 attribute must be set!", getLocation());
	        if(password1 == null)
	            throw new BuildException("Password 1 attribute must be set!", getLocation());
    	}
    	//if(url1 == null)
        //  throw new BuildException("Url 1 attribute must be set!", getLocation());
        if(username2 == null)
            throw new BuildException("User Name 2 attribute must be set!", getLocation());
        if(password2 == null)
            throw new BuildException("Password 2 attribute must be set!", getLocation());
        if(url2 == null)
            throw new BuildException("Url 2 attribute must be set!", getLocation());
        Driver d = getDriver();
        Properties info = new Properties();
        Connection conn1 = null;
        if(url1 != null) {
	        log("connecting to " + getUrl1(), 3);
	        info.put("user", getUsername1());
	        info.put("password", getPassword1());

	        try {
	        	conn1 = d.connect(getUrl1(), info);
	        } catch(SQLException sqle) {
	            sqle.printStackTrace();
	        	throw new BuildException("Could not connect to " + getUrl1(), sqle);
	        }
	        if(conn1 == null)
	            throw new BuildException("No suitable Driver for " + getUrl1());
        }
        log("connecting to " + getUrl2(), 3);
        info.put("user", getUsername2());
        info.put("password", getPassword2());
        Connection conn2;
        try {
        	conn2 = d.connect(getUrl2(), info);
        } catch(SQLException sqle) {
            sqle.printStackTrace();
        	throw new BuildException("Could not connect to " + getUrl2(), sqle);
        }
        if(conn2 == null)
            throw new BuildException("No suitable Driver for " + getUrl2());

        // now the real meat
        DBCompare.Entity entity;
        DBChanges.Change[] changes;
        try {
        	//entity = DBCompare.compare(conn1, conn2, getSchema());
        	changes = DBChanges.findChanges(conn1, conn2, getSchemas(), getObjectTypes());
        } catch(SQLException sqle) {
        	sqle.printStackTrace();
            throw new BuildException("Error while comparing:", sqle);
        } catch(Exception e) {
        	e.printStackTrace();
            throw new BuildException("Error while comparing:", e);
        } finally {
        	try { if(conn1 != null) conn1.close(); } catch(SQLException sqle1) {}
        	try { conn2.close(); } catch(SQLException sqle1) {}
        }
        /*
        DBComparePanel panel = new DBComparePanel();
        panel.setEntity(entity);
        JFrame frame = new JFrame("DB Compare");
        frame.getContentPane().add(panel);
        frame.show();
        */
        // the alternate way
        if(getOutputFile() != null) {
        	try {
                java.io.PrintStream out = new java.io.PrintStream(new java.io.FileOutputStream(getOutputFile()));
        		for(int i = 0; i < changes.length; i++) {
        			try {
        				out.println(changes[i].getSQL());
        			} catch(Exception e) {
        				System.out.println("!!ERROR: on " + changes[i]);
                    	e.printStackTrace();
        			}
        		}
        		out.flush();
        		out.close();
        	} catch(IOException ioe) {
        		ioe.printStackTrace();
        		throw new BuildException("Could not write to output file '" + getOutputFile() + "'", ioe);
        	} catch(Exception e) {
            	e.printStackTrace();
                throw new BuildException("Error while comparing:", e);
            }
        }
        if(getReportFile() != null) {
        	try {
                java.io.PrintStream out = new java.io.PrintStream(new java.io.FileOutputStream(getReportFile()));
				Object[] data = new Object[] { "Object", "Change", "SQL" };
				StringUtils.writeCSVLine(data, out);
        		for(int i = 0; i < changes.length; i++) {
        			try {
						data[0] = changes[i].getEntityName();
						data[1] = changes[i].getChangeType();
						data[2] = changes[i].getSQL();
						StringUtils.writeCSVLine(data, out);
        			} catch(Exception e) {
        				System.out.println("!!ERROR: on " + changes[i]);
                    	e.printStackTrace();
        			}
        		}
        		out.flush();
        		out.close();
        	} catch(IOException ioe) {
        		ioe.printStackTrace();
        		throw new BuildException("Could not write to output file '" + getOutputFile() + "'", ioe);
        	} catch(Exception e) {
            	e.printStackTrace();
                throw new BuildException("Error while comparing:", e);
            }
	    } /*else {
	        DBChangesPanel panel2 = new DBChangesPanel();
	        panel2.setChanges(changes);
	        JFrame frame2 = new JFrame("DB Changes");
	        frame2.getContentPane().add(panel2);
	        frame2.show();
	    }*/


    }

	/**
	 * @return Returns the classpath.
	 */
	public org.apache.tools.ant.types.Path getClasspath() {
		return classpath;
	}
	/**
	 * @param classpath The classpath to set.
	 */
	public void setClasspath(org.apache.tools.ant.types.Path classpath) {
		this.classpath = classpath;
	}
	/**
	 * @param driver The driver to set.
	 */
	public void setDriver(String driver) {
		this.driver = driver;
	}
	/**
	 * @return Returns the password1.
	 */
	public String getPassword1() {
		return password1;
	}
	/**
	 * @param password1 The password1 to set.
	 */
	public void setPassword1(String password1) {
		this.password1 = password1;
	}
	/**
	 * @return Returns the password2.
	 */
	public String getPassword2() {
		return password2;
	}
	/**
	 * @param password2 The password2 to set.
	 */
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	/**
	 * @return Returns the url1.
	 */
	public String getUrl1() {
		return url1;
	}
	/**
	 * @param url1 The url1 to set.
	 */
	public void setUrl1(String url1) {
		this.url1 = url1;
	}
	/**
	 * @return Returns the url2.
	 */
	public String getUrl2() {
		return url2;
	}
	/**
	 * @param url2 The url2 to set.
	 */
	public void setUrl2(String url2) {
		this.url2 = url2;
	}
	/**
	 * @return Returns the username1.
	 */
	public String getUsername1() {
		return username1;
	}
	/**
	 * @param username1 The username1 to set.
	 */
	public void setUsername1(String username1) {
		this.username1 = username1;
	}
	/**
	 * @return Returns the username2.
	 */
	public String getUsername2() {
		return username2;
	}
	/**
	 * @param username2 The username2 to set.
	 */
	public void setUsername2(String username2) {
		this.username2 = username2;
	}
	/**
	 * @return Returns the outputFile.
	 */
	public java.io.File getOutputFile() {
		return outputFile;
	}
	/**
	 * @param outputFile The outputFile to set.
	 */
	public void setOutputFile(java.io.File outputFile) {
		this.outputFile = outputFile;
	}
	/**
	 * @return Returns the objectTypes.
	 */
	public java.util.Set getObjectTypes() {
		return objectTypes;
	}
	/**
	 * @param objectTypes The objectTypes to set.
	 */
	public void setObjectTypes(String typesList) {
		this.objectTypes = new HashSet(java.util.Arrays.asList(simple.text.StringUtils.split(typesList, ",;:".toCharArray(), false)));
	}
	/**
	 * @return Returns the reportFile.
	 */
	public java.io.File getReportFile() {
		return reportFile;
	}
	/**
	 * @param reportFile The reportFile to set.
	 */
	public void setReportFile(java.io.File reportFile) {
		this.reportFile = reportFile;
	}
}
