/*
 * GeoLookupUtility.java
 *
 * Created on May 14, 2004, 12:37 PM
 */

package simple.tools;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.util.URIUtil;

import simple.bean.ReflectionUtils;
import simple.io.Log;

/**
 *
 * @author  Brian S. Krug
 */
public class GeoInfoLookup {
	private static final Log log = Log.getLog();

    public static class GeoInfo {
		private String postalCd;
		private String countryCd;
        /**
         * Holds value of property areaCode.
         */
        private String areaCode = "";

        /**
         * Holds value of property city.
         */
        private String city = "";

        /**
         * Holds value of property stateName.
         */
        private String stateName = "";

        /**
         * Holds value of property timeZone.
         */
        private String timeZone = "";

        /**
         * Holds value of property metroArea.
         */
        private String metroArea = "";

        /**
         * Holds value of property primaryMetroArea.
         */
        private String primaryMetroArea = "";

        /**
         * Holds value of property stateAbbr.
         */
        private String stateAbbr = "";

        /**
         * Holds value of property counties.
         */
        private County[] counties;


        public void addCounty(String name, String fips, String percent) {
            if(counties == null) counties = new County[1];
            else {
                County[] tmp = new County[counties.length+1];
                System.arraycopy(counties, 0, tmp, 0, counties.length);
                counties = tmp;
            }
            County c = new County();
            c.setName(name);
            c.setFips(fips);
            if(percent == null || percent.trim().length() == 0) c.setPercent(1);
            else try {
                c.setPercent(Float.parseFloat(percent));
            } catch(NumberFormatException nfe) {
                c.setPercent(1);
            }
            counties[counties.length-1] = c;
        }
        /**
         * Getter for property areaCode.
         * @return Value of property areaCode.
         */
        public String getAreaCode() {
            return this.areaCode;
        }

        /**
         * Setter for property areaCode.
         * @param areaCode New value of property areaCode.
         */
        public void setAreaCode(String areaCode) {
            this.areaCode = areaCode;
        }

        /**
         * Getter for property city.
         * @return Value of property city.
         */
        public String getCity() {
            return this.city;
        }

        /**
         * Setter for property city.
         * @param city New value of property city.
         */
        public void setCity(String city) {
            this.city = city;
        }

        /**
         * Getter for property state.
         * @return Value of property state.
         */
        public String getStateName() {
            return this.stateName;
        }

        /**
         * Setter for property state.
         * @param state New value of property state.
         */
        public void setStateName(String stateName) {
            this.stateName = stateName;
        }

        /**
         * Getter for property timeZone.
         * @return Value of property timeZone.
         */
        public String getTimeZone() {
            return this.timeZone;
        }

        /**
         * Setter for property timeZone.
         * @param timeZone New value of property timeZone.
         */
        public void setTimeZone(String timeZone) {
            this.timeZone = timeZone;
        }

        /**
         * Getter for property metroArea.
         * @return Value of property metroArea.
         */
        public String getMetroArea() {
            return this.metroArea;
        }

        /**
         * Setter for property metroArea.
         * @param metroArea New value of property metroArea.
         */
        public void setMetroArea(String metroArea) {
            this.metroArea = metroArea;
        }

        /**
         * Getter for property primaryMetroArea.
         * @return Value of property primaryMetroArea.
         */
        public String getPrimaryMetroArea() {
            return this.primaryMetroArea;
        }

        /**
         * Setter for property primaryMetroArea.
         * @param primaryMetroArea New value of property primaryMetroArea.
         */
        public void setPrimaryMetroArea(String primaryMetroArea) {
            this.primaryMetroArea = primaryMetroArea;
        }

        /**
         * Getter for property stateAbbr.
         * @return Value of property stateAbbr.
         */
        public String getStateAbbr() {
            return this.stateAbbr;
        }

        /**
         * Setter for property stateAbbr.
         * @param stateAbbr New value of property stateAbbr.
         */
        public void setStateAbbr(String stateAbbr) {
            this.stateAbbr = stateAbbr;
        }

        /**
         * Getter for property counties.
         * @return Value of property counties.
         */
        public County[] getCounties() {
            return this.counties;
        }

        /**
         * Setter for property counties.
         * @param counties New value of property counties.
         */
        public void setCounties(County[] counties) {
            this.counties = counties;
        }

		public String getPostalCd() {
			return postalCd;
		}

		public void setPostalCd(String postalCd) {
			this.postalCd = postalCd;
		}

		public String getCountryCd() {
			return countryCd;
		}

		public void setCountryCd(String countryCd) {
			this.countryCd = countryCd;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append('{');
			if(postalCd != null)
				sb.append("PostalCd=").append(postalCd).append("; ");
			if(countryCd != null)
				sb.append("CountryCd=").append(countryCd).append("; ");
			if(city != null)
				sb.append("City=").append(city).append("; ");
			if(stateName != null)
				sb.append("StateName=").append(stateName).append("; ");
			if(stateAbbr != null)
				sb.append("StateAbbr=").append(stateAbbr).append("; ");
			if(areaCode != null)
				sb.append("AreaCode=").append(areaCode).append("; ");
			if(timeZone != null)
				sb.append("TimeZone=").append(timeZone).append("; ");
			if(primaryMetroArea != null)
				sb.append("PrimaryMetroArea=").append(primaryMetroArea).append("; ");
			if(metroArea != null)
				sb.append("MetroArea=").append(metroArea).append("; ");
			if(counties != null && counties.length > 0)
				sb.append("Counties=").append(Arrays.toString(counties)).append("; ");
			sb.setLength(sb.length() - 2);
			sb.append('}');
			return sb.toString();
		}
    }
    public static class County {

        /**
         * Holds value of property name.
         */
        private String name = "";

        /**
         * Holds value of property fips.
         */
        private String fips = "";

        /**
         * Holds value of property percent.
         */
        private float percent;

        /**
         * Getter for property fips.
         * @return Value of property fips.
         */
        public String getFips() {
            return this.fips;
        }

        /**
         * Getter for property county.
         * @return Value of property county.
         */
        public String getName() {
            return this.name;
        }

        /**
         * Setter for property county.
         * @param county New value of property county.
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Setter for property fips.
         * @param fips New value of property fips.
         */
        public void setFips(String fips) {
            this.fips = fips;
        }

        /**
         * Getter for property percent.
         * @return Value of property percent.
         */
        public float getPercent() {
            return this.percent;
        }

        /**
         * Setter for property percent.
         * @param percent New value of property percent.
         */
        public void setPercent(float percent) {
            this.percent = percent;
        }

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			if(name != null)
				sb.append(name);
			if(fips != null) {
				sb.append(" (").append(fips);
				if(percent > 0.0f)
					sb.append(" - ").append(percent / 100).append("%)");
			} else if(percent > 0.0f)
				sb.append(" (").append(percent / 100).append("%)");
			return sb.toString();
		}
    }
    protected static class LookupService {
    	protected final String serviceUrl;
		protected final String prepareUrl;
		protected final Map<Pattern, String> preparePatterns;
    	protected final String postalParamName;
    	protected final Map<Pattern,String> writeMethodPatterns = new HashMap<Pattern,String>();

		public LookupService(String serviceUrl, String postalParamName, String prepareUrl, Map<Pattern, String> preparePatterns) {
        	this.serviceUrl = serviceUrl;
			this.postalParamName = postalParamName;
			this.prepareUrl = prepareUrl;
			this.preparePatterns = preparePatterns;
        }
		public LookupService(String serviceUrl, String postalParamName) {
			this(serviceUrl, postalParamName, null, null);
		}
    }
    protected HttpClient httpClient = new HttpClient();
    protected static final Map<String, LookupService> lookupServices = new HashMap<String, LookupService>();
	protected static final boolean useUSPS = false;
	protected static final String MD_US_SERVICE_URL = "http://www.melissadata.com/Lookups/ZipCityPhone.asp?submit=Search";
	protected static final String USPS_US_SERVICE_URL = "https://tools.usps.com/go/ZipLookupResultsAction!input.action?resultMode=2";
	// protected static final String CA_SERVICE_URL =
	// "http://www.canadapost.ca/cpotools/apps/fpc/business/findAnAddress?execution=e2s1&fpcFindAnAddress%3AfindAnAddress%3Afpc_biz_common_fa_find.x=57&fpcFindAnAddress%3AfindAnAddress%3Afpc_biz_common_fa_find.y=10&autoScroll=0%2C0&fpcFindAnAddress%3AfindAnAddress_SUBMIT=1";
	protected static final String CA_SERVICE_PREPARE_URL = "http://www.canadapost.ca/cpotools/apps/fpc/personal/findAnAddress?execution=e1s1";
	protected static final String CA_SERVICE_URL = "http://www.canadapost.ca/cpotools/apps/fpc/personal/findAnAddress?execution=e2s1&fpcFindAnAddress%3AreverseSearch_SUBMIT=1";
	// <input type="hidden" name="javax.faces.ViewState" id="javax.faces.ViewState"
	// value="v1Dxx9hSieZwMVPzZZHAPSkIfrSZM1jbO4mNTTN2U0GpxoAFjzFhwKHSEgcAunn2YyUiHf4E5tIMuqNSqP+dRXlnUu5pPC6ZorcMnR6eUbwBfVsEfhJYRBewHFHleWOFgmTmtqpxws1JfGmss6QW+w==">
	protected static final Pattern CA_PREPARE_PATTERN = Pattern.compile("<input[^>]* name=\"javax.faces.ViewState\"[^>]* value=\"([^\"]+)\"[^>]*>");
	protected static final Map<Pattern, String> patterns = new java.util.HashMap<Pattern, String>();
    /**
     * Holds value of property stealthMode.
     */
    private boolean stealthMode;

    static {
		if(useUSPS) {
			setLookupService("US", USPS_US_SERVICE_URL, "postalCode");
			try {
				addInfoPattern("US", "<div id=\"result-cities\">\\s*<h3>The <u>preferred</u> city in <span class=\"zip\">[^<]+</span> is...</h3>\\s*<p class=\"std-address\">([^<]+) [A-Z]{2}</p>", "setCity");
				addInfoPattern("US", "<div id=\"result-cities\">\\s*<h3>The <u>preferred</u> city in <span class=\"zip\">[^<]+</span> is...</h3>\\s*<p class=\"std-address\">[^<]+ ([A-Z]{2})</p>", "setStateAbbr");
			} catch(NoSuchMethodException e) {
				log.warn("While initializing GeoInfoLookup for country 'US'", e);
			}
		} else {
			setLookupService("US", MD_US_SERVICE_URL, "InData");
			try {
				addInfoPattern("US", "<tr[^>]*><td[^>]*>Time Zone[^<]*</td><td[^>]*><b>([\\w]+(?:[\\s]+[\\w]+)*)", "setTimeZone");
				addInfoPattern("US", "<tr[^>]*><td[^>]*>Area Code</td><td[^>]*><b>(\\d{3})", "setAreaCode");
				addInfoPattern("US", "<tr[^>]*><td[^>]*>City</td><td[^>]*><b>([\\w]+(?:[\\s]+[\\w]+)*)", "setCity");
				addInfoPattern("US", "<tr[^>]*><td[^>]*>USPS Preferred City Name</td><td[^>]*><b>(\\w+(?:\\s+\\w+)*)", "setCity");
				addInfoPattern("US", "<tr[^>]*><td[^>]*>Metro Area [(]Code[)]</td><td[^>]*><b>([\\w\\s\\,\\-]+)", "setMetroArea");
				addInfoPattern("US", "<tr[^>]*><td[^>]*>Primary Metro Area [(]Code[)]</td><td[^>]*><b>([\\w\\s\\,\\-]+)", "setPrimaryMetroArea");
				addInfoPattern("US", "<tr[^>]*><td[^>]*>State</td><td[^>]*><b>[^(]*[(](\\w+)[)]", "setStateAbbr");
				addInfoPattern("US", "<tr[^>]*><td[^>]*>State</td><td[^>]*><b>([\\w\\s]+)", "setStateName");
				addInfoPattern("US", "<tr[^>]*><td[^>]*>County Name[^<]*</td><td[^>]*><b>([\\w]+(?:[\\s]+[\\w]+)*).*(\\d{5}).*(?:(\\d{1,2}[.]\\d)[%])?.*</td>", "addCounty");
				// addInfoPattern("CA", "\"listPostalCodeResult:fpcResultsTable:tbody_element\"[^>]*>\\s*<tr[^>]*>(?:\\s*<td[^>]*>.*</td\\s*>){5}\\s*<td[^>]*>\\s*(.*)\\s*</td\\s*>", "setCity");
				// addInfoPattern("CA", "\"listPostalCodeResult:fpcResultsTable:tbody_element\"[^>]*>\\s*<tr[^>]*>(?:\\s*<td[^>]*>.*</td\\s*>){6}\\s*<td[^>]*>\\s*(.*)\\s*</td\\s*>", "setStateAbbr");
			} catch(NoSuchMethodException e) {
				log.warn("While initializing GeoInfoLookup for country 'US'", e);
			}
    	}
		// XXX: This doesn't work. Not sure why - website is fairly complex with redirects, etc needed to get the info
		setLookupService("CA", CA_SERVICE_URL, "postalCode", CA_SERVICE_PREPARE_URL, Collections.singletonMap(CA_PREPARE_PATTERN, "javax.faces.ViewState"));
		try {
			addInfoPattern("CA", "\"listPostalCodeResult:fpcResultsTable:tbody_element\"[^>]*>\\s*<tr[^>]*>(?:\\s*<td[^>]*>.*</td\\s*>){5}\\s*<td[^>]*>\\s*(.*)\\s*</td\\s*>", "setCity");
			addInfoPattern("CA", "\"listPostalCodeResult:fpcResultsTable:tbody_element\"[^>]*>\\s*<tr[^>]*>(?:\\s*<td[^>]*>.*</td\\s*>){6}\\s*<td[^>]*>\\s*(.*)\\s*</td\\s*>", "setStateAbbr");
		} catch(NoSuchMethodException e) {
			log.warn("While initializing GeoInfoLookup for country 'CA'", e);
		}
    }

	public static void setLookupService(String countryCd, String serviceUrl, String postalParamName) {
		setLookupService(countryCd, serviceUrl, postalParamName, null, null);
	}

	public static void setLookupService(String countryCd, String serviceUrl, String postalParamName, String prepareUrl, Map<Pattern, String> preparePatterns) {
		LookupService ls = new LookupService(serviceUrl, postalParamName, prepareUrl, preparePatterns);
    	lookupServices.put(countryCd, ls);
    }
    public static void addInfoPattern(String countryCd, String pattern, String writeMethodName) throws NoSuchElementException, NoSuchMethodException {
    	addInfoPattern(countryCd, Pattern.compile(pattern), writeMethodName);
    }
    public static void addInfoPattern(String countryCd, Pattern pattern, String writeMethodName) throws NoSuchElementException, NoSuchMethodException {
		Class<?>[] pt = new Class[pattern.matcher("").groupCount()];
    	if(!ReflectionUtils.hasMethod(GeoInfo.class, writeMethodName, pt))
    		throw new NoSuchMethodException("Could not find method '" + writeMethodName + "' on " + GeoInfo.class.getName() + " with " + pt.length + " parameters");
    	LookupService ls = lookupServices.get(countryCd);
    	if(ls == null)
    		throw new NoSuchElementException("A Lookup Service for Country '" + countryCd + "' was not yet added");
    	ls.writeMethodPatterns.put(pattern, writeMethodName);
    }
    /** Creates a new instance of GeoLookupUtility */
    public GeoInfoLookup() {
    }

    public GeoInfo lookupGeoInfo(String postalCd, String countryCd) throws NoSuchElementException, HttpException, IOException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
    	LookupService ls = lookupServices.get(countryCd);
    	if(ls == null)
    		throw new NoSuchElementException("A Lookup Service for Country '" + countryCd + "' was not found");
		GetMethod method = new GetMethod() {
            protected void addRequestHeaders(org.apache.commons.httpclient.HttpState state, org.apache.commons.httpclient.HttpConnection conn)
            throws java.io.IOException, org.apache.commons.httpclient.HttpException {
                //do nothing if in stealth mode
                if(!stealthMode) super.addRequestHeaders(state, conn);
            }
        };
		StringBuilder queryString = new StringBuilder();
		if(ls.prepareUrl != null) {
			method.setURI(new URI(ls.prepareUrl, true, "UTF-8"));
			httpClient.executeMethod(method);
			String html = method.getResponseBodyAsString();
			if(ls.preparePatterns != null)
				for(Map.Entry<Pattern, String> e : ls.preparePatterns.entrySet()) {
					Pattern p = e.getKey();
					Matcher m = p.matcher(html);
					if(m.find()) {
						if(queryString.length() > 0)
							queryString.append('&');
						// XXX: We should decode m.group(1) as CDATA
						queryString.append(URIUtil.encodeWithinQuery(e.getValue())).append('=').append(URIUtil.encodeWithinQuery(m.group(1)));
					}
				}
		}
		method.setURI(new URI(ls.serviceUrl, true, "UTF-8"));
		if(queryString.length() > 0)
			queryString.append('&');
		String current = method.getQueryString();
		if(current != null)
			queryString.append(current).append('&');
		queryString.append(URIUtil.encodeWithinQuery(ls.postalParamName)).append('=').append(URIUtil.encodeWithinQuery(postalCd));
		method.setQueryString(queryString.toString());
		httpClient.executeMethod(method);
		String html = method.getResponseBodyAsString();

        GeoInfo gi = new GeoInfo();
		gi.setPostalCd(postalCd);
		gi.setCountryCd(countryCd);
        for(Map.Entry<Pattern, String> e : ls.writeMethodPatterns.entrySet()) {
            Pattern p = e.getKey();
            Matcher m = p.matcher(html);
            boolean matched = false;
            while(m.find()) {
                matched = true;
                Object[] params = new Object[m.groupCount()];
                for(int i = 0; i < params.length; i++) params[i] = m.group(i+1);
                ReflectionUtils.invokeMethod(gi, e.getValue(), params);
            }
            if(!matched) {
                //System.out.println("Could not match pattern '" + p.pattern() + "' " /*+ "in:\n" + html*/);
            }
        }
        return gi;
    }

    /**
     * Getter for property stealthMode.
     * @return Value of property stealthMode.
     */
    public boolean isStealthMode() {
        return this.stealthMode;
    }

    /**
     * Setter for property stealthMode.
     * @param stealthMode New value of property stealthMode.
     */
    public void setStealthMode(boolean stealthMode) {
        this.stealthMode = stealthMode;
    }

}
