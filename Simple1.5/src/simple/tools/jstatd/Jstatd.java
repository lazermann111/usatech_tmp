/*
 * Copyright (c) 2004, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package simple.tools.jstatd;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Application providing remote access to the jvmstat instrumentation
 * exported by local Java Virtual Machine processes. Remote access is
 * provided through an RMI interface.
 * 
 * @author Brian S. Krug (copied from Open JDK 1.7_6)
 * @author Brian Doherty
 * @since 1.5
 */
public class Jstatd {
	private static Registry registry;
	private static int port = -1;
	private static int listenPort = -1;
	private static boolean startRegistry = true;
	private static int defaultServerPort = -1;

	protected static void printUsage() {
		System.err.println("usage: jstatd [-nr] [-p port] [-n rminame] [-l listenport] [-s serverport]");
	}

	protected static void bind(String name, RemoteHostImpl remoteHost) throws RemoteException, MalformedURLException, Exception {
		try {
			Naming.rebind(name, remoteHost);
		} catch(java.rmi.ConnectException e) {
			/* either the registry is not running or we cannot contact it.
			 * start an internal registry if requested.
			 */
			if(startRegistry && registry == null) {
				int localport = (port < 0) ? Registry.REGISTRY_PORT : port;
				System.out.println("Creating registry on port=" + localport);
				registry = LocateRegistry.createRegistry(localport);
				bind(name, remoteHost);
			} else {
				System.out.println("Could not contact registry\n" + e.getMessage());
				e.printStackTrace();
			}
		} catch(RemoteException e) {
			System.err.println("Could not bind " + name + " to RMI Registry");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String rminame = null;
		int argc = 0;
		for(; (argc < args.length) && (args[argc].startsWith("-")); argc++) {
			String arg = args[argc];

			if(arg.compareTo("-nr") == 0) {
				startRegistry = false;
			} else if(arg.startsWith("-p")) {
				if(arg.compareTo("-p") != 0) {
					port = Integer.parseInt(arg.substring(2));
				} else {
					argc++;
					if(argc >= args.length) {
						printUsage();
						System.exit(1);
					}
					port = Integer.parseInt(args[argc]);
				}
			} else if(arg.startsWith("-n")) {
				if(arg.compareTo("-n") != 0) {
					rminame = arg.substring(2);
				} else {
					argc++;
					if(argc >= args.length) {
						printUsage();
						System.exit(1);
					}
					rminame = args[argc];
				}
			} else if(arg.startsWith("-l")) {
				if(arg.compareTo("-l") != 0) {
					listenPort = Integer.parseInt(arg.substring(2));
				} else {
					argc++;
					if(argc >= args.length) {
						printUsage();
						System.exit(1);
					}
					listenPort = Integer.parseInt(args[argc]);
				}
			} else if(arg.startsWith("-s")) {
				if(arg.compareTo("-s") != 0) {
					defaultServerPort = Integer.parseInt(arg.substring(2));
				} else {
					argc++;
					if(argc >= args.length) {
						printUsage();
						System.exit(1);
					}
					defaultServerPort = Integer.parseInt(args[argc]);
				}
			} else {
				printUsage();
				System.exit(1);
			}
		}

		if(argc < args.length) {
			printUsage();
			System.exit(1);
		}

		if(System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}

		if(port <= 0)
			port = Registry.REGISTRY_PORT;
		if(listenPort < 0)
			listenPort = port + 1;
		if(defaultServerPort < 0)
			defaultServerPort = listenPort + 1;
		if(rminame == null)
			rminame = "JStatRemoteHost";
		String name = new StringBuilder().append("//:").append(port).append("/").append(rminame).toString();
		System.out.println("Starting up jstatd with port=" + port + "; createRegistry=" + startRegistry + "; listenPort=" + listenPort + "; rminame=" + rminame);
		try {
			// use 1.5.0 dynamically generated subs.
			System.setProperty("java.rmi.server.ignoreSubClasses", "true");
			RemoteHostImpl remoteHost = new RemoteHostImpl(defaultServerPort);
			UnicastRemoteObject.exportObject(remoteHost, listenPort);
			bind(name, remoteHost);
		} catch(MalformedURLException e) {
			System.out.println("Bad RMI server name: " + rminame);
			System.exit(1);
		} catch(java.rmi.ConnectException e) {
			// could not attach to or create a registry
			System.out.println("Could not contact RMI registry\n" + e.getMessage());
			System.exit(1);
		} catch(Exception e) {
			System.out.println("Could not create remote object\n" + e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}
}
