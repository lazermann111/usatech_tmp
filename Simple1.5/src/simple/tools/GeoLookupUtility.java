/*
 * GeoLookupUtility.java
 *
 * Created on May 14, 2004, 12:37 PM
 */

package simple.tools;

import java.awt.Window;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.db.Call.BatchUpdate;
import simple.db.DataLayerMgr;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.io.GuiInteraction;
import simple.io.Interaction;
import simple.io.LoadingException;
import simple.io.Log;
import simple.results.Results;
import simple.text.StringUtils;
import simple.tools.GeoInfoLookup.GeoInfo;

/**
 *
 * @author  Brian S. Krug
 */
public class GeoLookupUtility extends BaseWithConfig {
	private static final Log log = Log.getLog();
    protected static final String NEWLINE = System.getProperty("line.separator", "\n");

	public static class LookupKey {
		protected String postalCd;
		protected String countryCd;

		public LookupKey() {
			super();
		}
		public LookupKey(String postalCd, String countryCd) {
			super();
			this.postalCd = postalCd;
			this.countryCd = countryCd;
		}

		public String getPostalCd() {
			return postalCd;
		}

		public void setPostalCd(String postalCd) {
			this.postalCd = postalCd;
		}

		public String getCountryCd() {
			return countryCd;
		}

		public void setCountryCd(String countryCd) {
			this.countryCd = countryCd;
		}
	}
    /** Creates a new instance of GeoLookupUtility */
    public GeoLookupUtility() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
		new GeoLookupUtility().run(args);
    }

	protected void registerDefaultCommandLineArguments() {
		super.registerDefaultCommandLineArguments();
		registerCommandLineSwitch('q', "query", true, true, "query", "The Call Id of the query to retrieve postal codes that need lookup");
		registerCommandLineSwitch('u', "update", true, true, "update", "The Call Id of the update to set postal code info");
		registerCommandLineSwitch('d', "data-layer-file", true, true, "simple.db.DataLayer.files", "The path to the data layer file");
		registerCommandLineSwitch('i', "input", true, true, "input", "The path of the input file");
		registerCommandLineSwitch('o', "output", true, true, "output", "The path of the output file");
		registerCommandLineSwitch('s', "stealth", true, false, "stealth", "Send the http lookup request without headers");
		registerCommandLineSwitch('b', "batch-size", true, true, "batchSize", "Size of the update batch");
		registerCommandLineSwitch('P', "prompt-for-password", true, false, "promptForPassword", "prompt user for password");
	}

	@Override
	protected void execute(Map<String, Object> argMap, Configuration config) {
		boolean stealth = ConvertUtils.getBooleanSafely(config.getProperty("stealth"), false);
		GeoInfoLookup gilu = new GeoInfoLookup();
		gilu.setStealthMode(stealth);
		String query = ConvertUtils.getStringSafely(config.getProperty("query"), null);
		String update = ConvertUtils.getStringSafely(config.getProperty("update"), null);
		if(!StringUtils.isBlank(query) && !StringUtils.isBlank(update)) {
			try {
				boolean prompt = ConvertUtils.getBooleanSafely(config.getProperty("promptForPassword"), false);
				if(prompt) {
					Interaction interaction = new GuiInteraction((Window) null);
					Configuration dataSourceConfig = config.subset(DATA_SOURCE_FACTORY_PROPERTIES_SUBCONFIG);
					final String suffix = ".username";
					Map<String, Object> additions = new HashMap<String, Object>();
					for(Iterator<String> iter = dataSourceConfig.getKeys(); iter.hasNext();) {
						String key = iter.next();
						if(key.length() > suffix.length() && key.regionMatches(true, key.length() - suffix.length(), suffix, 0, suffix.length())) {
							String dsn = key.substring(0, key.length() - suffix.length());
							char[] password = interaction.readPassword("Enter password for '" + dsn + "' data source:");
							additions.put(dsn + ".password", new MaskedString(new String(password)));
						}
					}
					for(Map.Entry<String, Object> entry : additions.entrySet())
						dataSourceConfig.setProperty(entry.getKey(), entry.getValue());
				}
				configureDataSourceFactory(config, null);
				String dataLayerFile = ConvertUtils.getStringSafely(config.getProperty("simple.db.DataLayer.files"), "data-layer.xml");
				log.debug("Loading the file '" + dataLayerFile + "' into the DataLayer");
				try {
					ConfigLoader.loadConfig(dataLayerFile);
				} catch(ConfigException e) {
					throw new ServiceException("Could not load data-layer file, '" + dataLayerFile + "'", e);
				} catch(IOException e) {
					throw new ServiceException("Could not load data-layer file, '" + dataLayerFile + "'", e);
				} catch(LoadingException e) {
					throw new ServiceException("Could not load data-layer file, '" + dataLayerFile + "'", e);
				}
				final int batchSize = ConvertUtils.getIntSafely(config.getProperty("batchSize"), 100);
				final Results results = DataLayerMgr.executeQuery(query, null);
				final BatchUpdate batch = DataLayerMgr.createBatchUpdate(update, false, 0);
				Connection conn = DataLayerMgr.getConnectionForCall(update);
				try {
					while(results.next()) {
						String postalCd = results.getValue("postalCd", String.class);
						String countryCd = results.getValue("countryCd", String.class);
						if(!StringUtils.isBlank(postalCd) && !StringUtils.isBlank(countryCd)) {
							GeoInfo gi = gilu.lookupGeoInfo(postalCd, countryCd);
							if(StringUtils.isBlank(gi.getCity())) {
								log.warn("Postal " + postalCd + " (" + countryCd + ") not found at row #" + results.getRow());
							} else {
								log.info("Adding update to batch for postal " + postalCd + " (" + countryCd + ") at row #" + results.getRow());
								batch.addBatch(gi);
								if(batchSize > 0 && batch.getPendingCount() > batchSize) {
									batch.executeBatch(conn);
									conn.commit();
									log.info("Committed postal update batch");
								}
							}
						} else
							log.warn("PostalCd or CountryCd was blank for row #" + results.getRow());
					}
					if(batch.getPendingCount() > 0) {
						batch.executeBatch(conn);
						conn.commit();
						log.info("Committed postal update batch");
					}
				} finally {
					conn.close();
				}
			} catch(Exception e) {
				finishAndExit("Could not lookup and update postal codes", 301, e);
			}
		} else {
			String input = ConvertUtils.getStringSafely(config.getProperty("input"), null);
			String output = ConvertUtils.getStringSafely(config.getProperty("output"), null);
			if(!StringUtils.isBlank(input) && !StringUtils.isBlank(output)) {
				try {
					String SEP = "\t";
					RandomAccessFile raf = new RandomAccessFile(output, "rw");
					String line = raf.readLine();
					String lastZip = null;
					if(line != null && line.startsWith("Zip")) {
						Pattern pattern = Pattern.compile("(\\d{5,10})" + SEP + ".+");
						for(line = raf.readLine(); line != null; line = raf.readLine()) {
							Matcher match = pattern.matcher(line);
							if(match.matches()) {
								lastZip = match.group(1);
							} else {
								// back up to beginning of line
								raf.seek(raf.getFilePointer() - line.length());
								break;
							}
						}
					} else {
						raf.writeBytes("Zip");
						raf.writeBytes(SEP);
						raf.writeBytes("AreaCode");
						raf.writeBytes(SEP);
						raf.writeBytes("TimeZone");
						raf.writeBytes(SEP);
						raf.writeBytes("City");
						raf.writeBytes(SEP);
						raf.writeBytes("StateName");
						raf.writeBytes(SEP);
						raf.writeBytes("StateAbbr");
						raf.writeBytes(SEP);
						raf.writeBytes("County");
						raf.writeBytes(SEP);
						raf.writeBytes("FIPS");
						raf.writeBytes(NEWLINE);
					}
					int cnt = 0;
					boolean skip = (lastZip != null);
					BufferedReader in = new BufferedReader(new java.io.FileReader(input));
					for(line = in.readLine(); line != null; line = in.readLine()) {
						if(++cnt % 20 == 0)
							System.out.println("Processed " + cnt + " Zips ending with '" + line + "'");
						if(skip) {
							if(lastZip.equals(line)) {
								skip = false;
								System.out.println("Done skipping Zips begin retrieving data.");
							}
							continue;
						}
						raf.writeBytes(line);
						raf.writeBytes(SEP);
						GeoInfo gi = gilu.lookupGeoInfo(line, "US");
						raf.writeBytes(gi.getAreaCode());
						raf.writeBytes(SEP);
						raf.writeBytes(gi.getTimeZone());
						raf.writeBytes(SEP);
						raf.writeBytes(gi.getCity());
						raf.writeBytes(SEP);
						raf.writeBytes(gi.getStateName());
						raf.writeBytes(SEP);
						raf.writeBytes(gi.getStateAbbr());
						raf.writeBytes(SEP);
						String s = "";
						for(int i = 0; gi.getCounties() != null && i < gi.getCounties().length; i++) {
							if(i > 0) {
								s += ",";
								raf.writeBytes(",");
							}
							s += gi.getCounties()[i].getFips();
							raf.writeBytes(gi.getCounties()[i].getName());
							if(gi.getCounties().length > 1)
								raf.writeBytes(" (" + gi.getCounties()[i].getPercent() + "%)");
						}
						raf.writeBytes(SEP);
						raf.writeBytes(s);
						raf.writeBytes(NEWLINE);
					}
					in.close();
					raf.close();
				} catch(Exception e) {
					finishAndExit("Could not lookup and update postal codes", 301, e);
				}
			} else {
				finishAndExit("You must provide either 'query' and 'update' parameters or 'input' and 'output' parameters", 200, true, null);
				return;
			}
		}

	}
}
