package simple.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SecureHash {
	protected static final Pattern ALG_STRING_PATTERN = Pattern.compile("\\s*([^/]+?)\\s*/\\s*(\\d+)\\s*");
	public static final String CSRNG_ALGORITHM = "SHA1PRNG";
	public static final String CSRNG_PROVIDER = "SUN";
	public static final String HASH_ALGORITHM = "SHA-256";
	public static final int HASH_ITERATION_COUNT = 1000;
	public static final String HASH_ALGORITHM_ITERS = HASH_ALGORITHM + "/" + HASH_ITERATION_COUNT;
	public static final int SALT_LENGTH = 32;
	public static final String CHARSET = "UTF-8";

	public static byte[] getHash(byte[] data, byte[] salt) throws NoSuchAlgorithmException {
		return getHash(data, salt, HASH_ALGORITHM, HASH_ITERATION_COUNT);
	}

	public static byte[] getHash(byte[] data, byte[] salt, String algorithmString) throws NoSuchAlgorithmException {
		Matcher matcher = ALG_STRING_PATTERN.matcher(algorithmString);
		String algorithm;
		int additionalIterations;
		if(matcher.matches()) {
			algorithm = matcher.group(1);
			additionalIterations = Integer.parseInt(matcher.group(2));
		} else {
			algorithm = algorithmString;
			additionalIterations = 0;
		}
		return getHash(data, salt, algorithm, additionalIterations);
	}

	public static byte[] getHash(byte[] data, byte[] salt, String algorithm, int additionalIterations) throws NoSuchAlgorithmException {
		if (data == null)
			return null;
		MessageDigest digest = MessageDigest.getInstance(algorithm);
		digest.reset();
		if(salt != null)
			digest.update(salt);
		byte[] input = digest.digest(data);
        // key stretching by hashing multiple times
		for(int i = 0; i < additionalIterations; i++) {
			digest.reset();
			input = digest.digest(input);
		}
		return input;
   }
	
	public static byte[] getUnsaltedHash(byte[] data) throws NoSuchAlgorithmException {
		if (data == null)
			return null;
		return getUnsaltedHash(data, HASH_ALGORITHM);
	}

	public static byte[] getUnsaltedHash(byte[] data, String algorithm) throws NoSuchAlgorithmException {
		if (data == null)
			return null;
		MessageDigest digest = MessageDigest.getInstance(algorithm);
		digest.reset();
		return digest.digest(data);
	}
	
	public static byte[] getHash(String data, byte[] salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		return getHash(data.getBytes(CHARSET), salt);
	}
	
	public static byte[] getSalt(SecureRandom random, int length) throws NoSuchAlgorithmException, NoSuchProviderException {
		if (random == null)
			random = SecureRandom.getInstance(CSRNG_ALGORITHM, CSRNG_PROVIDER);
		byte salt[] = new byte[length];
	    random.nextBytes(salt);
	    return salt;
	}
	
	public static byte[] getSalt(int length) throws NoSuchAlgorithmException, NoSuchProviderException {
		return getSalt(null, length);
	}

	public static byte[] getSalt() throws NoSuchAlgorithmException, NoSuchProviderException {
		return getSalt(SALT_LENGTH);
	}	
}
