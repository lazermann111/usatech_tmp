package simple.security;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;

public class CompositeConfiguration extends Configuration {
	protected final Set<Configuration> delegates = new LinkedHashSet<Configuration>();

	@Override
	public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
		for(Configuration conf : delegates) {
			AppConfigurationEntry[] entries = conf.getAppConfigurationEntry(name);
			if(entries != null && entries.length > 0)
				return entries;
		}
		return null;
	}

	@Override
	public void refresh() {
		for(Configuration conf : delegates)
			conf.refresh();
	}

	public void addConfiguration(Configuration conf) {
		delegates.add(conf);
	}

	public void removeConfiguration(Configuration conf) {
		delegates.remove(conf);
	}
}
