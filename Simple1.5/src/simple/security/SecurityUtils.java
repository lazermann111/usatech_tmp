/**
 *
 */
package simple.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.Calendar;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.security.auth.login.Configuration;

import com.sun.security.auth.login.ConfigFile;

import simple.bean.ConvertUtils;

/**
 * @author Brian S. Krug
 *
 */
public class SecurityUtils {
	// NOTE: Add Log introduces too many dependencies to allow this class to be used by USATJMXAgent
	//private static final Log log = Log.getLog();
	protected static KeyStoreConfig defaultKeyStoreConfig;
	protected static KeyStoreConfig defaultTrustStoreConfig;
	protected static KeyStore defaultKeyStore;
	protected static KeyStore defaultTrustStore;
	
	public static final String CSRNG_ALGORITHM = "SHA1PRNG";
	public static final String CSRNG_PROVIDER = "SUN";
	
	protected static int minLength = 12;
	private static final String VALID_PWD_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$^*+";
	private static final String VALID_PWD_UPPERCASE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String VALID_PWD_LOWERCASE_CHARS = "abcdefghijklmnopqrstuvwxyz";
	private static final String VALID_PWD_PUNCTUATION_CHARS = "!@#$^*+";
	
	// protected static final Pattern TRACK_DATA_FIXUP_PATTERN = Pattern.compile("([^0-9A-Za-z]*)([0-9A-Za-z]{2})([*@]+)([0-9A-Za-z]{4})($|[^0-9A-Za-z].*)");
	protected static final Pattern TRACK_DATA_FIXUP_PATTERN = Pattern.compile("[^0-9A-Za-z]*([0-9A-Za-z*]+)(?:$|\\=([0-9A-Za-z*]+))[^0-9A-Za-z]*");
	
	public static KeyStoreConfig getDefaultKeyStoreConfig() {
		if(defaultKeyStoreConfig == null) {
			defaultKeyStoreConfig = new KeyStoreConfig("javax.net.ssl.keyStore", getProperties());
		}
		return defaultKeyStoreConfig;
	}

	public static KeyStoreConfig getDefaultTrustStoreConfig() {
		if(defaultTrustStoreConfig == null) {
			defaultTrustStoreConfig = new KeyStoreConfig("javax.net.ssl.trustStore", getProperties(),
					System.getProperty("java.home") + File.separator + "lib" + File.separator + "security" + File.separator + "jssecacerts",
					System.getProperty("java.home") + File.separator + "lib" + File.separator + "security" + File.separator + "cacerts");
		}
		return defaultTrustStoreConfig;
	}
	public static KeyStore getDefaultKeyStore() throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException {
		if(defaultKeyStore == null) {
			defaultKeyStore = getKeyStore(getDefaultKeyStoreConfig());
		}
		return defaultKeyStore;
	}

	public static KeyStore getDefaultTrustStore() throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException {
		if(defaultTrustStore == null) {
			defaultTrustStore = getKeyStore(getDefaultTrustStoreConfig());
		}
		return defaultTrustStore;
	}
	public static KeyStore getKeyStore(KeyStoreConfig keyStoreConfig) throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException {
		return getKeyStore(keyStoreConfig.getInputStream(), keyStoreConfig.getType(), keyStoreConfig.getProvider(), keyStoreConfig.getPassword());
	}
	public static KeyStore getKeyStore(InputStream stream, String type, String provider, char[] password) throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException {
		KeyStore keystore;
		try {
			if(provider == null || (provider=provider.trim()).length() == 0)
				keystore = KeyStore.getInstance(type);
			else
				keystore = KeyStore.getInstance(type, provider);
			keystore.load(stream, password);
		} finally {
			if(stream != null)
				stream.close();
		}
		return keystore;
	}

	/*
	 * Use the static field on a static inner class trick to synchronize so that only one copy of properties is created
	 */
	protected static class PropertiesHolder {
		protected static final Properties properties = createProperties();

		protected static Properties createProperties() {
			Properties properties = new Properties(System.getProperties());
			String fileName = properties.getProperty("simple.security.config.file", "security.properties");
			InputStream in = SecurityUtils.class.getClassLoader().getResourceAsStream(fileName);
			if(in == null) {
				File file = new File(fileName);
				if(file.canRead()) {
					try {
						in = new FileInputStream(file);
					} catch(FileNotFoundException e) {
						System.err.println("Could not read Simple security properties file '"+ file.getAbsolutePath() + "'");
						e.printStackTrace();					
					}
				}
			}
			if(in != null) {
				try {
					properties.load(in);
					System.out.println("Read Simple security properties from file '" + fileName + "'");
				} catch(IOException e) {
					System.err.println("Error while reading Simple security properties file '"+ fileName + "'");
					e.printStackTrace();
				} finally {
					try {
						in.close();
					} catch (IOException e) {
						System.err.println("Error while closing Simple security properties file '"+ fileName + "'");
						e.printStackTrace();
					}
				}
			} else
				System.out.println("Read Simple security properties from system properties because file '" + fileName + "' does not exist");
			return properties;
		}
	}

	/*
	 * This is copied to SimpleProvider to improve dependencies
	 */
	public static Properties getProperties() {
		return PropertiesHolder.properties;
	}

	public static void setJAASConfigFile(String filePath) throws IOException {
		Configuration.setConfiguration(createFileConfig(filePath));
	}

	public static void setAdditionalJAASConfigFiles(String... filePaths) throws IOException {
		CompositeConfiguration cc = new CompositeConfiguration();
		try {
			cc.addConfiguration(Configuration.getConfiguration());
		} catch(SecurityException e) {
			// log.warn("Could not load default configuration; Ignoring and continuing", e);
		}
		for(String filePath : filePaths)
			cc.addConfiguration(createFileConfig(filePath));
		Configuration.setConfiguration(cc);
	}
	
	public static Configuration createFileConfig(String configFilePath) throws IOException {
		try {
			return new ConfigFile(new URI("file", configFilePath, null));
		} catch(URISyntaxException e) {
			throw new IOException(e);
		}
	}
	public static String getRandomString(SecureRandom random, String chars, int stringLength) throws NoSuchAlgorithmException, NoSuchProviderException {
		if (random == null)
			random = SecureRandom.getInstance(CSRNG_ALGORITHM, CSRNG_PROVIDER);
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<stringLength; i++) {
			int index = (int)(random.nextDouble() * chars.length());
			sb.append(chars.substring(index, index + 1));
		}
		return sb.toString();
	}
	
	public static String getRandomString(String chars, int stringLength) throws NoSuchAlgorithmException, NoSuchProviderException {
		return getRandomString(null, chars, stringLength);
	}
	
	public static SecureRandom getSecureRandom() throws NoSuchAlgorithmException, NoSuchProviderException {
		return SecureRandom.getInstance(CSRNG_ALGORITHM, CSRNG_PROVIDER);
	}
	
	public static String getRandomPassword(int passwordLength) throws NoSuchAlgorithmException, NoSuchProviderException {
		// Use characters that can be easily typed. Avoid characters that can be mistaken for each other such as o, O and 0, 1, l, L, i and I
		return getRandomString("abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789!@#$^*+", passwordLength);
	}
	
	public static String getRandomPasscode() throws NoSuchAlgorithmException, NoSuchProviderException {
		return getRandomString("0123456789", 8);
	}
	
	public static String getRandomAlphaNumericString(int stringLength) {
		try {
			// Avoid characters that can be mistaken for each other such as o, 0, 1, l, i
			return getRandomString("abcdefghjkmnpqrstuvwxyz23456789", stringLength);
		} catch (Exception e) {
			return "";
		}
	}
	
	//Generates secure password, which has  at least 12 characters and contain 1 uppercase letter, 1 lowercase letter, and 1 number or punctuation. 
	public static String generatePassword() {
		String password;
		StringBuilder sb = new StringBuilder(minLength);
		SecureRandom rnd;
		try {
			rnd = getSecureRandom();
		} catch (Exception e) {
			rnd = new SecureRandom();
		}
		
		for (int i=0; i<minLength-3; i++){
			sb.append(VALID_PWD_CHARS.charAt(rnd.nextInt(VALID_PWD_CHARS.length())));
		}
		
		password = sb.toString();
		char value; 
		int at;
		//add upper case letter
		value = VALID_PWD_UPPERCASE_CHARS.charAt(rnd.nextInt(VALID_PWD_UPPERCASE_CHARS.length()));
		at = rnd.nextInt(password.length());
		password = password.substring(0, at) + value + password.substring(at, password.length()); 
		//add lower case letter
		value = VALID_PWD_LOWERCASE_CHARS.charAt(rnd.nextInt(VALID_PWD_LOWERCASE_CHARS.length()));
		at = rnd.nextInt(password.length());
		password = password.substring(0, at) + value + password.substring(at, password.length());
		//add punctuation
		value = VALID_PWD_PUNCTUATION_CHARS.charAt(rnd.nextInt(VALID_PWD_PUNCTUATION_CHARS.length()));
		at = rnd.nextInt(password.length());
		password = password.substring(0, at) + value + password.substring(at, password.length());
				
		return password;
	}
	
	public static String generateCard(String prefix, int len) {
		return generateCard(prefix, len, null);
	}

	public static String generateCard(SecureRandom rnd, String prefix, int len, Integer addMonths) {
		SecureRandom rand = rnd;
		if (rand == null) {
			try {			
				rand = getSecureRandom();
			} catch (Exception e) {
				rand = new SecureRandom();
			}
		}
		if(len < 1) {
			len = 15 + rand.nextInt(5);
		}
		StringBuilder sb = new StringBuilder(";");
		if(prefix != null)
			sb.append(prefix.trim());
		for(int i = sb.length(); i < 2; i++) {
			char ch = (char)('0' + rand.nextInt(10));
			sb.append(ch);
		}
		for(int i = sb.length(); i < len - 3; i++) {
			sb.append('*');
		}
		for(int i = 0; i < 4; i++) {
			char ch = (char)('0' + rand.nextInt(10));
			sb.append(ch);
		}
		sb.append('=');
		Integer months = addMonths;
		if (months == null)
			months = rand.nextInt(120);
		Calendar cal = Calendar.getInstance();
		if(months != 0)
			cal.add(Calendar.MONTH, months);
		sb.append(ConvertUtils.getFormat("DATE", "yyMM").format(cal.getTime()));
		for(int i = 0; i < 16; i++) {
			sb.append('*');
		}
		sb.append('?');
		return sb.toString();
	}
	
	public static String generateCard(String prefix, int len, Integer addMonths) {
		return generateCard(null, prefix, len, addMonths);
	}
	
	public static String fillInTrackData(SecureRandom rnd, String trackData) {
		Matcher matcher = TRACK_DATA_FIXUP_PATTERN.matcher(trackData);
		if(matcher.matches()) {
			SecureRandom rand = rnd;
			if (rand == null) {
				try {
					rand = getSecureRandom();
				} catch (Exception e) {
					rand = new SecureRandom();
				}
			}
			StringBuilder sb = new StringBuilder(trackData);
			int unknown = -1;
			for(int i = matcher.start(1); i < matcher.end(1); i++) {
				char ch = sb.charAt(i);
				if(ch == '*') {
					if(unknown == -1) {
						unknown = i;
						continue;
					}
					ch = (char) ('0' + rand.nextInt(10));
					sb.setCharAt(i, ch);
				}
			}
			sb.setCharAt(unknown, (char) ('0' + calcDigit(sb.substring(matcher.start(1), matcher.end(1)), unknown - matcher.start(1))));

			// do extra track info
			String extra = matcher.group(2);
			if(extra != null && extra.length() > 0) {
				int dt = (extra.length() >= 6 ? 0 : 4);
				for(int i = matcher.start(2); i < matcher.end(2) - 2; i++) {
					char ch = sb.charAt(i);
					if(ch == '*') {
						switch(dt) {
							case 3:
								ch = '9';
								dt++;
								break;
							case 2:
								ch = '0';
								dt++;
								break;
							case 1:
								ch = '8';
								dt++;
								break;
							case 0:
								ch = '1';
								dt++;
								break;
							case 4:
							default:
								ch = (char) ('0' + rand.nextInt(10));
								break;
						}
						sb.setCharAt(i, ch);
					}
				}
				// mod97
				int mod97 = mod97(sb.substring(matcher.start(1), matcher.end(2) - 2).replace('=', '0'));
				sb.setCharAt(matcher.end(2) - 2, (char) ('0' + ((mod97 / 10) % 10)));
				sb.setCharAt(matcher.end(2) - 1, (char) ('0' + (mod97 % 10)));
			}			
			return sb.toString();
		}
		return trackData;
	}
	
	public static String fillInTrackData(String trackData) {
		return fillInTrackData(null, trackData);
	}
	
	public static int calcDigit(String accountCd, int index) {
		int[] a = new int[accountCd.length()];
		for(int i = 0; i < a.length; i++)
			a[i] = Character.getNumericValue(accountCd.charAt(i));

		int len = a.length;
		int checksum = 0;
		int tmp;

		for(int i = (len % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i] * 2;
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}
		for(int i = ((len + 1) % 2); i < len; i += 2) {
			if(i == index)
				continue;
			tmp = a[i];
			while(tmp > 9)
				tmp = (tmp % 10) + (tmp / 10);
			checksum += tmp;
		}

		checksum = (10 - (checksum % 10)) % 10;
		if(((len - index) % 2) == 0)
			return (checksum % 2) == 0 ? checksum / 2 : (9 + checksum) / 2;
		return checksum;
	}
	
	private static final BigInteger bi97 = BigInteger.valueOf(97);
	private static final BigInteger bi98 = BigInteger.valueOf(98);
	private static final BigInteger bi100 = BigInteger.valueOf(100);
	
	public static int mod97(String s) {
		BigInteger i = new BigInteger(s);
		return bi98.subtract(i.multiply(bi100).mod(bi97)).mod(bi97).intValue();
	}	
}
