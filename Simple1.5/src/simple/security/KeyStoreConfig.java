package simple.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.Properties;

public class KeyStoreConfig {
	protected final char[] password;
	protected final String type;
	protected final String provider;
	protected final File file;
	public KeyStoreConfig(String type, String provider, String path, char[] password, String... fallbackPaths) {
		super();
		this.password = password;
		this.type = type;
		this.provider = provider;
		if("NONE".equals(path)) {
			this.file = null;
		} else if(path != null && (path=path.trim()).length() > 0) {
			File file = new File(path);
			if(file.exists()) {
				this.file = file;
			} else {
				this.file = null;
			}
		} else if(fallbackPaths != null && fallbackPaths.length > 0) {
			File file = null;
			for(String fp : fallbackPaths) {
				File tryFile = new File(fp);
				if(tryFile.exists()) {
					file = tryFile;
					break;
				}
			}
			this.file = file;
		} else {
			this.file = null;
		}
	}
	public KeyStoreConfig(String type, String provider, String path, String password, String... fallbackPaths) {
		this(type, provider, path, password == null ? null : password.toCharArray(), fallbackPaths);
	}
	public KeyStoreConfig(String prefix, Properties properties, String... fallbackPaths) {
		this(properties.getProperty(prefix + "Type", KeyStore.getDefaultType()),
				properties.getProperty(prefix + "Provider"),
				properties.getProperty(prefix),
				properties.getProperty(prefix + "Password"),
				fallbackPaths);
	}
	public char[] getPassword() {
		return password;
	}
	public String getType() {
		return type;
	}
	public String getProvider() {
		return provider;
	}
	public File getFile() {
		return file;
	}
	public InputStream getInputStream() throws FileNotFoundException {
		return this.file == null ? null : new FileInputStream(file);
	}
}
