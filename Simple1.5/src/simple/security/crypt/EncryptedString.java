/**
 *
 */
package simple.security.crypt;

import java.nio.charset.Charset;

import javax.crypto.KeyGenerator;

/**
 * @author Brian S. Krug
 *
 */
public class EncryptedString extends AbstractEncryptedObject<String> {
	protected Charset encoding;
	protected final boolean originallyEncrypted;

	public EncryptedString(byte[] encrypted, byte[] encryptedKey, String cipherName, String secondaryCipherName, String keyAlias, Charset encoding) {
		super(encrypted, encryptedKey, cipherName, secondaryCipherName, keyAlias);
		this.encoding = encoding;
		this.originallyEncrypted = true;
	}
	public EncryptedString(String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAlias, String unencrypted, Charset encoding) {
		super(cipherName, secondaryCipherName, keyGenerator, keyAlias, unencrypted);
		this.encoding = encoding;
		this.originallyEncrypted = false;
	}
	/**
	 * @see simple.security.crypt.AbstractEncryptedObject#fromBytes(byte[])
	 */
	@Override
	protected String fromBytes(byte[] bytes) {
		return new String(bytes, encoding);
	}
	/**
	 * @see simple.security.crypt.AbstractEncryptedObject#toBytes(java.lang.Object)
	 */
	@Override
	protected byte[] toBytes(String object) {
		return object.getBytes(encoding);
	}
	public Charset getEncoding() {
		return encoding;
	}
	public void setEncoding(Charset encoding) {
		Charset old = this.encoding;
		this.encoding = encoding;
		if(!old.equals(encoding)) {
			if(originallyEncrypted)
				unencrypted = null;
			else
				encrypted = null;
		}
	}
}
