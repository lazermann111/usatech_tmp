/**
 *
 */
package simple.security.crypt;

import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.PublicKey;
import java.util.SortedSet;

/**
 * @author Brian S. Krug
 *
 */
public interface KeySupply {
	public PublicKey getPublicKey(String alias) throws GeneralSecurityException ;
	public Key getKey(String alias) throws GeneralSecurityException ;
	public SortedSet<String> getAliases() throws GeneralSecurityException ;
}
