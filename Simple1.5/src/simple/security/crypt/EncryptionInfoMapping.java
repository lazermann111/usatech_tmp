/**
 *
 */
package simple.security.crypt;

import java.util.HashMap;
import java.util.Map;

import simple.util.CollectionUtils;

/**
 * @author Brian S. Krug
 *
 */
public class EncryptionInfoMapping {
	protected final Map<String, EncryptionInfo> encryptionInfoMapping = new HashMap<String, EncryptionInfo>();
	public EncryptionInfo getEncryptionInfo(String queuePrefix) {
		return encryptionInfoMapping.get(queuePrefix);
	}

	public void setEncryptionInfo(String queuePrefix, EncryptionInfo encryptionInfo) {
		encryptionInfoMapping.put(queuePrefix, encryptionInfo);
	}
	public EncryptionInfo findEncryptionInfo(String queueKey) {
		String key = CollectionUtils.bestPrefixMatch(encryptionInfoMapping.keySet(), queueKey);
		return encryptionInfoMapping.get(key);
	}
}
