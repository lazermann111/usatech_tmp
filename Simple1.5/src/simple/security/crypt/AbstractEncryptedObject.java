/**
 *
 */
package simple.security.crypt;

import java.security.GeneralSecurityException;
import java.security.PublicKey;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.Convertible;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractEncryptedObject<U> implements Convertible {
	protected final String cipherName;
	protected final String secondaryCipherName;
	protected final String keyAlias;
	protected final KeyGenerator keyGenerator;
	protected U unencrypted;
	protected byte[] encrypted;
	protected byte[] encryptedKey;
	public AbstractEncryptedObject(String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAlias, U unencrypted) {
		this(unencrypted, cipherName, secondaryCipherName, keyGenerator, keyAlias, null, null);
	}
	public AbstractEncryptedObject(byte[] encrypted, byte[] encryptedKey, String cipherName, String secondaryCipherName, String keyAlias) {
		this(null, cipherName, secondaryCipherName, null, keyAlias, encrypted, encryptedKey);
	}
	private AbstractEncryptedObject(U unencrypted, String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAlias, byte[] encrypted, byte[] encryptedKey) {
		this.unencrypted = unencrypted;
		this.cipherName = cipherName;
		this.keyAlias = keyAlias;
		this.encrypted = encrypted;
		this.encryptedKey = (encryptedKey == null || encryptedKey.length == 0 ? null : encryptedKey);
		this.secondaryCipherName = (secondaryCipherName == null || (secondaryCipherName=secondaryCipherName.trim()).length() == 0 ? null : secondaryCipherName);
		this.keyGenerator = keyGenerator;
	}
	public U getUnencrypted() throws GeneralSecurityException {
		if(unencrypted == null) {
			byte[] unencryptedBytes;
			if(secondaryCipherName != null && encryptedKey != null) {
				// first decrypt the key
				byte[] unencryptedKey = CryptUtils.decrypt(cipherName, keyAlias, encryptedKey);
				unencryptedBytes = CryptUtils.decrypt(secondaryCipherName, new SecretKeySpec(unencryptedKey, StringUtils.substringBefore(secondaryCipherName, "/")), encrypted);
			} else {
				unencryptedBytes = CryptUtils.decrypt(cipherName, keyAlias, encrypted);
			}
			unencrypted = fromBytes(unencryptedBytes);
		}
		return unencrypted;
	}
	protected abstract byte[] toBytes(U object) ;
	protected abstract U fromBytes(byte[] bytes) ;

	public String getCipherName() {
		return cipherName;
	}
	public String getKeyAlias() {
		return keyAlias;
	}
	public String getSecondaryCipherName() {
		return secondaryCipherName;
	}
	public byte[] getEncryptedKey() {
		return encryptedKey;
	}
	public byte[] getEncrypted() throws GeneralSecurityException {
		if(encrypted == null) {
			PublicKey key = CryptUtils.getDefaultTrustSupply().getPublicKey(keyAlias);
			int max = CryptUtils.getMaxDataSize(cipherName, key);
			byte[] unencryptedBytes = toBytes(unencrypted);
			if(max > 0 && unencryptedBytes.length > max && secondaryCipherName != null) {
				SecretKey skey = keyGenerator.generateKey();
				encryptedKey = CryptUtils.encrypt(cipherName, keyAlias, skey.getEncoded());
				encrypted = CryptUtils.encrypt(secondaryCipherName, skey, unencryptedBytes);
			} else {
				encrypted = CryptUtils.encrypt(cipherName, keyAlias, unencryptedBytes);
			}
		}
		return encrypted;
	}
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof AbstractEncryptedObject<?>))
			return false;
		AbstractEncryptedObject<?> eb = (AbstractEncryptedObject<?>) obj;
		try {
			return ConvertUtils.areEqual(getUnencrypted(), eb.getUnencrypted());
		} catch(GeneralSecurityException e) {
			return false;
			/*
			return encrypted != null && eb.encrypted != null && cipherName.equals(eb.cipherName) && keyAlias.equals(eb.keyAlias) && Arrays.equals(encrypted, eb.encrypted)
				&& ConvertUtils.areEqual(secondaryCipherName, eb.secondaryCipherName) && ConvertUtils.areEqual(encryptedKey, eb.encryptedKey);*/
		}
	}
	@Override
	public int hashCode() {
		return encrypted.hashCode();
	}
	@Override
	public String toString() {
		return "ENCRYPTED";
	}
	public <Target> Target convert(Class<Target> toClass) throws ConvertException {
		try {
			return ConvertUtils.convert(toClass, getUnencrypted());
		} catch(GeneralSecurityException e) {
			throw new ConvertException("Could not decrypt value", toClass, this, e);
		}
	}
}
