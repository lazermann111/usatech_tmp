/**
 *
 */
package simple.security.crypt;

import javax.crypto.KeyGenerator;

/**
 * @author Brian S. Krug
 *
 */
public class EncryptedBytes extends AbstractEncryptedObject<byte[]> {
	public EncryptedBytes(byte[] encrypted, byte[] encryptedKey, String cipherName, String secondaryCipherName, String keyAlias) {
		super(encrypted, encryptedKey, cipherName, secondaryCipherName, keyAlias);
	}

	public EncryptedBytes(String cipherName, String secondaryCipherName, KeyGenerator keyGenerator, String keyAlias, byte[] unencrypted) {
		super(cipherName, secondaryCipherName, keyGenerator, keyAlias, unencrypted);
	}

	/**
	 * @see simple.security.crypt.AbstractEncryptedObject#fromBytes(byte[])
	 */
	@Override
	protected byte[] fromBytes(byte[] bytes) {
		return bytes;
	}
	/**
	 * @see simple.security.crypt.AbstractEncryptedObject#toBytes(java.lang.Object)
	 */
	@Override
	protected byte[] toBytes(byte[] object) {
		return object;
	}
}
