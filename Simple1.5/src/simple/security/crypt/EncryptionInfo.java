/**
 *
 */
package simple.security.crypt;

import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;

import simple.bean.ConvertUtils;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class EncryptionInfo {
	protected String keyAliasPrefix;
	protected String cipherName;
	protected String secondaryCipherName;
	protected int keySize;
	protected KeyGenerator keyGenerator;

	public KeyGenerator getKeyGenerator() {
		return keyGenerator;
	}
	public String getCipherName() {
		return cipherName;
	}
	public void setCipherName(String cipherName) {
		if(cipherName == null || (cipherName=cipherName.trim()).length() == 0)
			this.cipherName = null;
		else
			this.cipherName = cipherName;
	}
	public String getKeyAliasPrefix() {
		return keyAliasPrefix;
	}
	public void setKeyAliasPrefix(String keyAliasPrefix) {
		if(keyAliasPrefix == null || (keyAliasPrefix=keyAliasPrefix.trim()).length() == 0)
			this.keyAliasPrefix = null;
		else
			this.keyAliasPrefix = keyAliasPrefix;
	}
	public String getSecondaryCipherName() {
		return secondaryCipherName;
	}
	public void setSecondaryCipherName(String secondaryCipherName) throws NoSuchAlgorithmException {
		if(secondaryCipherName == null || (secondaryCipherName=secondaryCipherName.trim()).length() == 0)
			secondaryCipherName = null;
		String old = this.secondaryCipherName;
		if(!ConvertUtils.areEqual(old, secondaryCipherName)) {
			if(secondaryCipherName == null)
				keyGenerator = null;
			else {
				keyGenerator = KeyGenerator.getInstance(StringUtils.substringBefore(secondaryCipherName, "/"));
				if(getKeySize() > 0)
					keyGenerator.init(getKeySize());
			}
		}
		this.secondaryCipherName = secondaryCipherName;
	}
	public int getKeySize() {
		return keySize;
	}
	public void setKeySize(int keySize) {
		this.keySize = keySize;
		if(keyGenerator != null && keySize > 0)
			keyGenerator.init(keySize);
	}
}
