package simple.security.crypt;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStore.ProtectionParameter;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.RSAKey;
import java.security.interfaces.RSAPrivateKey;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.crypto.Cipher;

import simple.io.Base64;
import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;
import simple.io.Log;
import simple.security.KeyStoreConfig;
import simple.security.SecurityUtils;
import simple.lang.sun.misc.BASE64Encoder;
import simple.text.StringUtils;
import simple.util.CollectionUtils;
import sun.security.pkcs.PKCS8Key;
import sun.security.pkcs10.PKCS10;
import sun.security.rsa.RSACore;
import sun.security.rsa.RSAKeyFactory;
import sun.security.util.DerValue;
import sun.security.x509.X500Name;

public class CryptUtils {

	public static interface CopyEntryFilter {
		/**
		 * Returns the targetAlias to use when copying keystore entries from one keystore to another
		 * 
		 * @param sourceAlias
		 * @param sourceEntry
		 * @return The alias to use in the target keystore or <code>null</code> is the entry should not be copied
		 */
		public String getTargetAlias(String sourceAlias, KeyStore.Entry sourceEntry);
	}

	public static final CopyEntryFilter CERTIFICATES_ONLY_ENTRY_FILTER = new CopyEntryFilter() {
		@Override
		public String getTargetAlias(String sourceAlias, Entry sourceEntry) {
			return (sourceEntry instanceof KeyStore.TrustedCertificateEntry ? sourceAlias : null);
		}
	};

	public static class MapCopyEntryFilter implements CopyEntryFilter {
		protected final Map<String, String> aliases = new HashMap<String, String>();

		public void addAlias(String sourceAlias, String targetAlias) {
			aliases.put(sourceAlias, targetAlias);
		}

		@Override
		public String getTargetAlias(String sourceAlias, Entry sourceEntry) {
			return aliases.get(sourceAlias);
		}
	}
	public static class ReloadingKeySupply implements KeySupply, Loader {
		protected final ConfigSource src;
		protected final KeyStore keystore;
		protected final char[] password;
		protected SortedSet<String> aliases;
		public ReloadingKeySupply(ConfigSource src, KeyStore keystore, char[] password) throws GeneralSecurityException {
			this.src = src;
			this.keystore = keystore;
			this.password = password;
			if(this.src != null) {
				this.src.registerLoader(this, 1000, 0);
				reload();
			} else {
				try {
					keystore.load(null, password);
				} catch(IOException e) {
					throw new GeneralSecurityException(e);
				}
			}
		}
		public ReloadingKeySupply(ConfigSource src, String type, String provider, char[] password) throws GeneralSecurityException {
			this(src, (provider == null || (provider=provider.trim()).length() == 0 ? KeyStore.getInstance(type) : KeyStore.getInstance(type, provider)), password);
		}
		/**
		 * @see simple.io.Loader#load(simple.io.ConfigSource)
		 */
		public void load(ConfigSource src) throws LoadingException {
			try {
				InputStream stream = src.getInputStream();
				try {
					keystore.load(stream, password);
				} finally {
					stream.close();
				}
			} catch(NoSuchAlgorithmException e) {
				throw new LoadingException(e);
			} catch(CertificateException e) {
				throw new LoadingException(e);
			} catch(IOException e) {
				throw new LoadingException(e);
			}
		}
		/**
		 * @see simple.security.crypt.KeySupply#getKey(java.lang.String)
		 */
		public Key getKey(String alias) throws GeneralSecurityException {
			if(!keystore.containsAlias(alias) && !reload())
				return null;
			Key key = keystore.getKey(alias, password);
			if(key instanceof PrivateKey)
				return key;
			return null;
		}
		/**
		 * @see simple.security.crypt.KeySupply#getPublicKey(java.lang.String)
		 */
		public PublicKey getPublicKey(String alias) throws GeneralSecurityException {
			if(!keystore.containsAlias(alias) && !reload())
				return null;
			Certificate cert = keystore.getCertificate(alias);
			if(cert == null)
				return null;
			return cert.getPublicKey();
		}
	
		public SortedSet<String> getAliases() throws GeneralSecurityException {
			if(reload() || aliases == null) {
				Enumeration<String> en = keystore.aliases();
				SortedSet<String> newAliases = new TreeSet<String>();
				while(en.hasMoreElements())
					newAliases.add(en.nextElement());
				aliases = Collections.unmodifiableSortedSet(newAliases);
			}
			return aliases;
		}
		protected boolean reload() throws GeneralSecurityException {
			try {
				return src.reload();
			} catch(LoadingException e) {
				Throwable cause = e.getCause();
				if(cause instanceof NoSuchAlgorithmException)
					throw (NoSuchAlgorithmException)cause;
				if(cause instanceof CertificateException)
					throw (CertificateException)cause;
				if(cause instanceof IOException)
					throw new GeneralSecurityException(cause);
				throw new GeneralSecurityException(e);
			}
		}

		@Override
		public String toString() {
			return "Reloading Key Supply from " + src.toString();
		}
	}

	public static final Log log = Log.getLog();
	protected static KeySupply defaultKeySupply;
	protected static KeySupply defaultTrustSupply;

	/**
	 * @param keyAliasPrefix
	 * @return
	 * @throws GeneralSecurityException
	 */
	public static String findLatestPublicKeyAlias(String keyAliasPrefix) throws GeneralSecurityException {
		SortedSet<String> aliases = getDefaultTrustSupply().getAliases();
		return CollectionUtils.lastWithPrefix(aliases, keyAliasPrefix);
	}

	public static KeySupply getDefaultKeySupply() throws GeneralSecurityException {
		if(defaultKeySupply == null) {
			defaultKeySupply = getKeySupply(SecurityUtils.getDefaultKeyStoreConfig());
		}
		return defaultKeySupply;
	}

	public static KeySupply getDefaultTrustSupply() throws GeneralSecurityException {
		if(defaultTrustSupply == null) {
			defaultTrustSupply = getKeySupply(SecurityUtils.getDefaultTrustStoreConfig());
		}
		return defaultTrustSupply;
	}

	public static byte[] encrypt(String cipherName, String keyAlias, byte[] unencrypted) throws GeneralSecurityException {
		PublicKey key =	CryptUtils.getDefaultTrustSupply().getPublicKey(keyAlias);
		if(key == null)
			throw new GeneralSecurityException("Public key '" + keyAlias + "' not found in keystore");
		return encrypt(cipherName, key, unencrypted);
	}

	public static byte[] encrypt(String cipherName, Key key, byte[] unencrypted) throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(cipherName);
		cipher.init(Cipher.ENCRYPT_MODE, key);
	    return cipher.doFinal(unencrypted);
	}

	public static byte[] encrypt(String cipherName, String keyAlias, byte[] unencrypted, int offset, int length) throws GeneralSecurityException {
		PublicKey key =	CryptUtils.getDefaultTrustSupply().getPublicKey(keyAlias);
		if(key == null)
			throw new GeneralSecurityException("Public key '" + keyAlias + "' not found in keystore");
		return encrypt(cipherName, key, unencrypted, offset, length);
	}

	public static byte[] encrypt(String cipherName, Key key, byte[] unencrypted, int offset, int length) throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(cipherName);
		cipher.init(Cipher.ENCRYPT_MODE, key);
	    return cipher.doFinal(unencrypted, offset, length);
	}

	public static byte[] decrypt(String cipherName, String keyAlias, byte[] encrypted) throws GeneralSecurityException {
		Key key = CryptUtils.getDefaultKeySupply().getKey(keyAlias);
		if(key == null)
			throw new GeneralSecurityException("Key '" + keyAlias + "' not found in keystore");
		return decrypt(cipherName, key, encrypted);
	}

	public static byte[] decrypt(String cipherName, Key key, byte[] encrypted) throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(cipherName);
		cipher.init(Cipher.DECRYPT_MODE, key);
	    return cipher.doFinal(encrypted);
	}

	public static byte[] decrypt(String cipherName, String keyAlias, byte[] encrypted, int offset, int length) throws GeneralSecurityException {
		Key key = CryptUtils.getDefaultKeySupply().getKey(keyAlias);
		if(key == null)
			throw new GeneralSecurityException("Key '" + keyAlias + "' not found in keystore");
		return decrypt(cipherName, key, encrypted, offset, length);
	}

	public static byte[] decrypt(String cipherName, Key key, byte[] encrypted, int offset, int length) throws GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(cipherName);
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(encrypted, offset, length);
	}

	public static void writePEMKey(String outputType, byte[] encoded, Appendable appendTo) throws IOException {
		String b64 = Base64.encodeBytes(encoded, false);
		appendTo.append("-----BEGIN ");
		appendTo.append(outputType);
		appendTo.append("-----\n");
		appendTo.append(b64);
		appendTo.append("\n-----END ");
		appendTo.append(outputType);
		appendTo.append("-----\n");
	}

	public static void writeCertificate(Certificate cert, Appendable appendTo) throws CertificateEncodingException, IOException {
		writePEMKey("CERTIFICATE", cert.getEncoded(), appendTo);
	}

	public static void writePrivateKey(PrivateKey key, Appendable appendTo) throws IOException {
		String outputType;
		if(key instanceof DSAPrivateKey)
			outputType = "DSA PRIVATE KEY";
		else if(key instanceof RSAPrivateKey)
			outputType = "RSA PRIVATE KEY";
		else 
			outputType = "PRIVATE KEY";
		writePEMKey(outputType, key.getEncoded(), appendTo);
	}

	public static KeySupply getKeySupply(KeyStoreConfig keyStoreConfig) throws GeneralSecurityException {
		return getKeySupply(keyStoreConfig.getFile() == null ? null : ConfigSource.createConfigSource(keyStoreConfig.getFile()), 
				keyStoreConfig.getType(), keyStoreConfig.getProvider(), keyStoreConfig.getPassword());
	}
	
	public static KeySupply getKeySupply(ConfigSource src, String type, String provider, char[] password) throws GeneralSecurityException {
		return new ReloadingKeySupply(src, type, provider, password);
	}

	public static int getMaxDataSize(String algorithm, Key key) throws InvalidKeyException, NoSuchAlgorithmException {
		String[] toks = StringUtils.split(algorithm, '/');
		if(toks == null || toks.length == 0)
			return -1;
		if("RSA".equalsIgnoreCase(toks[0])) {
			RSAKey rsaKey = RSAKeyFactory.toRSAKey(key);
			int n = RSACore.getByteLength(rsaKey.getModulus());
		    if(toks.length < 3 || "PKCS1Padding".equalsIgnoreCase(toks[2])) {
	            return n - 11;
	        } else if("NoPadding".equalsIgnoreCase(toks[2])) {
	            return n;
	        } else if ("oaeppadding".equalsIgnoreCase(toks[2])) {
	        	return n - 42;//n - 2 - 2 * hashLength, hashLength = 20;
	        } else {
	        	String lowerPadding = toks[2].toLowerCase(Locale.ENGLISH);
	        	if(lowerPadding.startsWith("oaepwith") && lowerPadding.endsWith("andmgf1padding")) {
	            	String oaepHashAlgorithm = toks[2].substring(8, toks[2].length() - 14);
	            	int hashLength = MessageDigest.getInstance(oaepHashAlgorithm).getDigestLength();
	            	return n - 2 - 2 * hashLength;
		        }
	        }
		}
		return -1;
	}

	public static KeyPair generateCSR(String algorithm, int keyBits, String dName, Appendable csr) throws NoSuchAlgorithmException, InvalidKeyException, IOException, CertificateException, SignatureException {
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance(algorithm);
		keyGen.initialize(keyBits);
		KeyPair pair = keyGen.generateKeyPair();
		extractCSR(pair, dName, csr);
		return pair;
	}
	public static void extractCSR(KeyPair pair, String dName, Appendable csr) throws NoSuchAlgorithmException, InvalidKeyException, IOException, CertificateException, SignatureException {
		extractCSR(pair, "SHA256", dName, csr);
	}

	public static void extractCSR(KeyPair pair, String hashAlg, String dName, Appendable csr) throws NoSuchAlgorithmException, InvalidKeyException, IOException, CertificateException, SignatureException {
		String sigAlgName;
		if(pair.getPrivate() instanceof DSAPrivateKey) {
			sigAlgName = hashAlg + "withDSA";
		} else if(pair.getPrivate() instanceof RSAPrivateKey) {
			sigAlgName = hashAlg + "withRSA";
		} else if(pair.getPrivate() instanceof ECPrivateKey) {
			sigAlgName = hashAlg + "withECDSA";
		} else {
			throw new SecurityException("Algorithm '" + pair.getPrivate().getAlgorithm() + "' is not supported");
		}
		PKCS10 request = new PKCS10(pair.getPublic());
		Signature signature = Signature.getInstance(sigAlgName);
		signature.initSign(pair.getPrivate());
		X500Name subject = new X500Name(dName);
		request.encodeAndSign(subject, signature);
		csr.append("-----BEGIN NEW CERTIFICATE REQUEST-----\n");
		BASE64Encoder encoder = new BASE64Encoder();
		csr.append(encoder.encodeBuffer(request.getEncoded()));
		csr.append("-----END NEW CERTIFICATE REQUEST-----\n");
	}

	public static String extractCSR(KeyStore keystore, String alias, char[] password, String dName) throws NoSuchAlgorithmException, InvalidKeyException, IOException, CertificateException, SignatureException, UnrecoverableKeyException, KeyStoreException {
		StringBuilder csr = new StringBuilder();
		extractCSR(keystore, alias, password, dName, csr);
		return csr.toString();
	}

	public static KeyPair extractCSR(KeyStore keystore, String alias, char[] password, String dName, Appendable csr) throws NoSuchAlgorithmException, InvalidKeyException, IOException, CertificateException, SignatureException, UnrecoverableKeyException, KeyStoreException {
		PrivateKey privateKey = (PrivateKey) keystore.getKey(alias, password);
		Certificate certificate = keystore.getCertificate(alias);
		if(privateKey == null || certificate == null)
			throw new InvalidKeyException("The keystore did not contain both the private and public key for alias '" + alias + "'");
		KeyPair pair = new KeyPair(certificate.getPublicKey(), privateKey);
		extractCSR(pair, dName, csr);
		return pair;
	}

	public static void importCertificateChain(KeyStore keyStore, String alias, KeyPair pair, InputStream csrReply, char[] password) throws CertificateException, KeyStoreException {
		Collection<? extends Certificate> certs = CertificateFactory.getInstance("X509").generateCertificates(csrReply);
		if(certs.isEmpty())
            throw new SecurityException("Reply has no certificates");
        keyStore.setKeyEntry(alias, pair.getPrivate(), password, certs.toArray(new Certificate[certs.size()]));
	}
	public static Collection<? extends Certificate> readCertificateChain(InputStream certificateStream) throws CertificateException {
		return CertificateFactory.getInstance("X509").generateCertificates(certificateStream);
	}
	public static void importTrustedCertificate(KeyStore keyStore, String alias, InputStream certStream) throws CertificateException, KeyStoreException {
		Certificate cert = CertificateFactory.getInstance("X509").generateCertificate(certStream);
		keyStore.setCertificateEntry(alias, cert);
	}

	public static int copyKeystoreEntries(KeyStore source, char[] sourcePassword, KeyStore target, char[] targetPassword, CopyEntryFilter filter) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException {
		return copyKeystoreEntries(source.aliases(), source, sourcePassword, target, targetPassword, filter);
	}

	public static boolean copyKeystoreEntry(String alias, KeyStore source, char[] sourcePassword, KeyStore target, char[] targetPassword, CopyEntryFilter filter) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException {
		return copyKeystoreEntries(CollectionUtils.singletonEnumeration(alias), source, sourcePassword, target, targetPassword, filter) > 0;
	}

	public static int copyKeystoreEntries(Enumeration<String> aliases, KeyStore source, char[] sourcePassword, KeyStore target, char[] targetPassword, CopyEntryFilter filter) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException {
		return copyKeystoreEntries(aliases, source, sourcePassword, target, targetPassword, filter, false);
	}

	public static int copyKeystoreEntries(Enumeration<String> aliases, KeyStore source, char[] sourcePassword, KeyStore target, char[] targetPassword, CopyEntryFilter filter, boolean removeMissingEntries) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException {
		ProtectionParameter sourceProt = (sourcePassword == null ? null : new KeyStore.PasswordProtection(sourcePassword));
		ProtectionParameter targetProt = (targetPassword == null ? null : new KeyStore.PasswordProtection(targetPassword));
		int cnt = 0;
		while(aliases.hasMoreElements()) {
			String sourceAlias = aliases.nextElement();
			KeyStore.Entry entry;
			if(source.isCertificateEntry(sourceAlias))
				entry = source.getEntry(sourceAlias, null);
			else
				entry = source.getEntry(sourceAlias, sourceProt);
			KeyStore.Entry copyEntry;
			String targetAlias;
			if(filter == null) {
				copyEntry = entry;
				targetAlias = sourceAlias;
			} else if((targetAlias = filter.getTargetAlias(sourceAlias, entry)) != null) {
				copyEntry = entry;
			} else if(entry instanceof KeyStore.PrivateKeyEntry) {
				Certificate cert = ((KeyStore.PrivateKeyEntry) entry).getCertificate();
				if(cert == null)
					continue;
				KeyStore.Entry certEntry = new KeyStore.TrustedCertificateEntry(cert);
				if((targetAlias = filter.getTargetAlias(sourceAlias, certEntry)) != null)
					copyEntry = certEntry;
				else
					continue;
			} else
				continue;

			KeyStore.Entry old;
			if(target.isKeyEntry(targetAlias))
				old = target.getEntry(targetAlias, targetProt);
			else if(target.isCertificateEntry(targetAlias))
				old = target.getEntry(targetAlias, null);
			else
				old = null;
			if(old != null && keystoreEntriesEqual(old, copyEntry))
				continue;
			if(copyEntry != null)
				target.setEntry(targetAlias, copyEntry, (copyEntry instanceof KeyStore.TrustedCertificateEntry ? null : targetProt));
			else if(removeMissingEntries)
				target.deleteEntry(targetAlias);
			else
				continue;
			cnt++;
		}
		return cnt;
	}

	public static boolean keystoreEntriesEqual(KeyStore.Entry entry1, KeyStore.Entry entry2) {
		if(entry1 instanceof KeyStore.PrivateKeyEntry && entry2 instanceof KeyStore.PrivateKeyEntry) {
			KeyStore.PrivateKeyEntry pkEntry1 = (KeyStore.PrivateKeyEntry) entry1;
			KeyStore.PrivateKeyEntry pkEntry2 = (KeyStore.PrivateKeyEntry) entry2;
			return pkEntry1.getPrivateKey().equals(pkEntry2.getPrivateKey()) && pkEntry1.getCertificate().equals(pkEntry2.getCertificate());
		}
		if(entry1 instanceof KeyStore.SecretKeyEntry && entry2 instanceof KeyStore.SecretKeyEntry) {
			KeyStore.SecretKeyEntry skEntry1 = (KeyStore.SecretKeyEntry) entry1;
			KeyStore.SecretKeyEntry skEntry2 = (KeyStore.SecretKeyEntry) entry2;
			return skEntry1.getSecretKey().equals(skEntry2.getSecretKey());
		}
		if(entry1 instanceof KeyStore.TrustedCertificateEntry && entry2 instanceof KeyStore.TrustedCertificateEntry) {
			KeyStore.TrustedCertificateEntry certEntry1 = (KeyStore.TrustedCertificateEntry) entry1;
			KeyStore.TrustedCertificateEntry certEntry2 = (KeyStore.TrustedCertificateEntry) entry2;
			return certEntry1.getTrustedCertificate().equals(certEntry2.getTrustedCertificate());
		}
		return false;
	}

	public static void importUnencryptedPKCS8DERPrivateKey(KeyStore keyStore, String alias, InputStream keyStream, char[] keyPassword, InputStream certStream) throws CertificateException, KeyStoreException, IOException {
		// sun.security - pre-decrypt with openssl, or upgrade to not-yet-commons-ssl
		PKCS8Key pk = PKCS8Key.parse(new DerValue(keyStream));
		Certificate x509 = CertificateFactory.getInstance("X509").generateCertificate(certStream);
		keyStore.setKeyEntry(alias, pk, keyPassword, new Certificate[] { x509 });
	}
}
