/**
 *
 */
package simple.security.provider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLDecoder;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;

import com.sun.org.apache.bcel.internal.classfile.ClassFormatException;
import com.sun.org.apache.bcel.internal.classfile.ClassParser;
import com.sun.org.apache.bcel.internal.classfile.JavaClass;
import com.sun.org.apache.bcel.internal.generic.ArrayType;
import com.sun.org.apache.bcel.internal.generic.BasicType;
import com.sun.org.apache.bcel.internal.generic.BranchInstruction;
import com.sun.org.apache.bcel.internal.generic.ClassGen;
import com.sun.org.apache.bcel.internal.generic.ClassGenException;
import com.sun.org.apache.bcel.internal.generic.ConstantPoolGen;
import com.sun.org.apache.bcel.internal.generic.InstructionFactory;
import com.sun.org.apache.bcel.internal.generic.InstructionHandle;
import com.sun.org.apache.bcel.internal.generic.InstructionList;
import com.sun.org.apache.bcel.internal.generic.MethodGen;
import com.sun.org.apache.bcel.internal.generic.ObjectType;
import com.sun.org.apache.bcel.internal.generic.PUSH;
import com.sun.org.apache.bcel.internal.generic.Type;

import simple.lang.com.sun.org.apache.bcel.internal.Constants;
import simple.lang.com.sun.org.apache.bcel.internal.generic.InstructionConstants;
import sun.security.jca.GetInstance;
import sun.security.jca.Providers;

/**
 * @author Brian S. Krug
 *
 */
public abstract class SimpleProvider extends java.security.Provider {
	private static final long serialVersionUID = 1411354484655170518L;
	public static final Class<?>[] EMPTY_PARAM_TYPES = new Class<?>[0];
	protected static final Type CLASS_TYPE = new ObjectType(Class.class.getName());
	protected static final boolean DEBUG = false;
	protected static class DynamicClassLoader extends ClassLoader {
		protected final Map<String,Class<?>> definedClasses = new HashMap<String, Class<?>>();
		public DynamicClassLoader() {
			super(SimpleProvider.class.getClassLoader());
		}
		public Class<?> defineClass(ClassGen classGen) {
			byte[] bytes = classGen.getJavaClass().getBytes();
			Class<?> c = defineClass(classGen.getClassName(), bytes, 0, bytes.length);
			definedClasses.put(classGen.getClassName(), c);
			return c;
		}

		public Class<?> redefineClass(String className, boolean includeDependeeClasses) throws ClassNotFoundException {
			Class<?> newClass = definedClasses.get(className);
			if(newClass != null)
				return newClass;
			return redefineClass(Class.forName(className), includeDependeeClasses);
		}

		public Class<?> redefineClass(String className, InputStream in, boolean includeDependeeClasses) throws ClassNotFoundException {
			Class<?> newClass = definedClasses.get(className);
			if(newClass != null)
				return newClass;
			if(!includeDependeeClasses)
				return internalRedefineClass(className, in);
			return internalRedefineClass(Class.forName(className), in, includeDependeeClasses);
		}

		public Class<?> redefineClass(Class<?> origClass, boolean includeDependeeClasses) throws ClassNotFoundException {
			return internalRedefineClass(origClass, null, includeDependeeClasses);
		}

		public void redefineNonPublicDependees(Class<?> origClass) throws ClassNotFoundException {
			if(origClass.getSuperclass() != null)
				possiblyRedefineDependee(origClass.getSuperclass());
			for(Class<?> interfaceClass : origClass.getInterfaces())
				possiblyRedefineDependee(interfaceClass);
		}

		protected static final Pattern anonClassNamePattern = Pattern.compile(".*\\$(\\d+)");

		protected void possiblyRedefineDependee(Class<?> dependeeClass) throws ClassNotFoundException {
			if(!Modifier.isPublic(dependeeClass.getModifiers()) && !dependeeClass.equals(Object.class) && !dependeeClass.getName().startsWith("java.") && !dependeeClass.getName().startsWith("javax."))
				redefineClass(dependeeClass, true);
		}

		protected Class<?> internalRedefineClass(Class<?> origClass, InputStream in, boolean includeDependeeClasses) throws ClassNotFoundException {
			String className = origClass.getName();
			Class<?> newClass = definedClasses.get(className);
			if(newClass != null)
				return newClass;
			if(includeDependeeClasses)
				redefineNonPublicDependees(origClass);
			if(in == null) {
				StringBuilder sb = new StringBuilder();
				sb.append(className, className.lastIndexOf('.') + 1, className.length()).append(".class");
				in = origClass.getResourceAsStream(sb.toString());
			}
			newClass = internalRedefineClass(className, in);
			redefineInnerClasses(origClass, includeDependeeClasses);
			return newClass;
		}

		public void redefineInnerClasses(Class<?> origClass, boolean includeDependeeClasses) throws ClassNotFoundException {
			Class<?>[] innerClasses = origClass.getDeclaredClasses();
			int maxAnonIndex = 0;
			for(Class<?> ic : innerClasses) {
				redefineClass(ic, true);
				Matcher matcher = anonClassNamePattern.matcher(ic.getName());
				if(matcher.matches()) {
					int anonIndex = Integer.parseInt(matcher.group(1));
					if(anonIndex > maxAnonIndex)
						maxAnonIndex = anonIndex;
				}
			}
			while(true) {
				Class<?> anonClass;
				try {
					anonClass = Class.forName(origClass.getName() + '$' + ++maxAnonIndex);
				} catch(ClassNotFoundException e) {
					break;
				}
				redefineClass(anonClass, true);
			}
		}

		protected Class<?> internalRedefineClass(String className, InputStream in) throws ClassNotFoundException {
			byte[] bytes = new byte[8192];
			int tot = 0;
			int r;
			try {
				while((r = in.read(bytes, tot, bytes.length - tot)) >= 0) {
					tot += r;
					if(tot >= bytes.length) {
						byte[] tmp = new byte[bytes.length + 8192];
						System.arraycopy(bytes, 0, tmp, 0, bytes.length);
						bytes = tmp;
					}
				}
			} catch(IOException e) {
				throw new ClassNotFoundException("Could not load class '" + className + "'", e);
			}
			Class<?> newClass = defineClass(className, bytes, 0, tot);
			definedClasses.put(className, newClass);
			if(DEBUG)
				System.out.println("=== Redefined '" + className + "' ====");
			return newClass;
		}

		/**
		 * @see java.lang.ClassLoader#loadClass(java.lang.String)
		 */
		@Override
		public Class<?> loadClass(String name) throws ClassNotFoundException {
			if(DEBUG) System.out.println("Loading class '" + name + "'");
			Class<?> c = definedClasses.get(name);
			if(c != null) {
				if(DEBUG) System.out.println("Returning custom defined class '" + name + "'");
				return c;
			}
			try {
				c = super.loadClass(name);
				if(DEBUG) System.out.println("Returning standard class '" + name + "'");
				return c;
			} catch(ClassNotFoundException e) {
				if(DEBUG) {
					System.out.println("Could not load class '" + name + "':");
					e.printStackTrace(System.out);
				}
				throw e;
			}
		}
	}
	protected static final DynamicClassLoader dynamicClassLoader = new DynamicClassLoader();

	public static class ServiceSpec<I> {
		protected final String serviceType;
		protected final String algorithm;
		protected final Class<I> serviceClass;
		protected final Class<? extends I> implClass;
		public ServiceSpec(String serviceType, String algorithm, Class<I> serviceClass, Class<? extends I> implClass) {
			super();
			this.serviceType = serviceType;
			this.algorithm = algorithm;
			this.serviceClass = serviceClass;
			this.implClass = implClass;
		}
		public String getServiceType() {
			return serviceType;
		}
		public String getAlgorithm() {
			return algorithm;
		}
		public Class<I> getServiceClass() {
			return serviceClass;
		}
		public Class<? extends I> getImplClass() {
			return implClass;
		}
	}

	protected String info;
	protected SimpleProvider(String name) {
		super(name, 1.0, "Simple Provider");
	}

	protected void register(ServiceSpec<?>[] serviceSpecs) {
		StringBuilder sb = new StringBuilder("Simple Provider (");
		boolean first = true;
		for(ServiceSpec<?> ss : serviceSpecs) {
			//create class
			Class<?> instClass;
			if(ss.getServiceClass() == null)
				instClass = ss.getImplClass();
			else
				instClass = makeServiceClass(getClass(), ss.getServiceType(), ss.getAlgorithm(), ss.getImplClass(), ss.getServiceClass());
			put(ss.getServiceType() + '.' + ss.getAlgorithm(), instClass.getName());
			if(first)
				first = false;
			else
				sb.append(", ");
			sb.append(ss.getServiceType());
		}
		sb.append(')');
		info = sb.toString();
	}

	/**
	 * @param string
	 * @param implClass
	 * @param serviceClass
	 * @return
	 */
	protected Class<?> makeServiceClass(Class<? extends SimpleProvider> providerClass, String type, String algorithm, Class<?> superClass, Class<?> serviceClass) {
		String className = providerClass.getName() + '_' + type + '_' + algorithm;
		String[] interfaceClassNames;
		if(serviceClass.isAssignableFrom(superClass)) {
			interfaceClassNames = null;
		} else if(serviceClass.isInterface()) {
			interfaceClassNames = new String[] { serviceClass.getName() };
		} else {
			throw new IllegalArgumentException("Implementation class '" + superClass.getName() + "' does not extend '" + serviceClass.getName());
		}
		ClassGen gen = new ClassGen(className, superClass.getName(), "<generated>", Constants.ACC_PUBLIC | Constants.ACC_SUPER, interfaceClassNames);
		InstructionFactory factory = new InstructionFactory(gen);
		ConstantPoolGen cp = gen.getConstantPool();
		InstructionList il = new InstructionList();

		il.append(InstructionConstants.THIS); // Push `this'

		il.append(factory.createConstant(providerClass.getName()));
		il.append(factory.createConstant(serviceClass.getName()));
		//il.append(new LDC(cp.addClass(providerClass.getName())));
		//il.append(new LDC(cp.addClass(serviceClass.getName())));

		il.append(factory.createConstant(type));
		il.append(factory.createConstant(algorithm));
		il.append(factory.createInvoke(SimpleProvider.class.getName(), "getDelegate", Type.OBJECT, new Type[] {
			Type.STRING, //CLASS_TYPE,
			Type.STRING, //CLASS_TYPE,
			Type.STRING,
			Type.STRING
			}, Constants.INVOKESTATIC));
		Type serviceType = new ObjectType(serviceClass.getName());
		il.append(factory.createCast(Type.OBJECT, serviceType));
		il.append(factory.createInvoke(superClass.getName(), "<init>", Type.VOID, new Type[] { serviceType }, Constants.INVOKESPECIAL));
		il.append(InstructionConstants.RETURN);
	    MethodGen mg = new MethodGen(Constants.ACC_PUBLIC, Type.VOID, Type.NO_ARGS, null, "<init>", className, il, cp);
	    mg.setMaxStack(5);
	    gen.addMethod(mg.getMethod());
	    return dynamicClassLoader.defineClass(gen);
	}

	protected static Class<?> makeProviderClass(String name) {
		String className = SimpleProvider.class.getName() + '_' + name;
		ClassGen gen = new ClassGen(className, SimpleProvider.class.getName(), "<generated>", Constants.ACC_PUBLIC | Constants.ACC_SUPER, null);
		ConstantPoolGen cp = gen.getConstantPool();
		InstructionFactory factory = new InstructionFactory(gen);
		InstructionList il = new InstructionList();

		il.append(InstructionConstants.THIS); // Push `this'
		il.append(factory.createConstant(name));
		il.append(factory.createInvoke(SimpleProvider.class.getName(), "<init>", Type.VOID, new Type[] { Type.STRING }, Constants.INVOKESPECIAL));
		il.append(InstructionConstants.RETURN);
	    MethodGen mg = new MethodGen(Constants.ACC_PUBLIC, Type.VOID, Type.NO_ARGS, null, "<init>", className, il, cp);
	    mg.setMaxStack(2);
	    gen.addMethod(mg.getMethod());
	    return dynamicClassLoader.defineClass(gen);
	}

	public void enforceCipherSuitePreference() throws ClassNotFoundException, NoSuchAlgorithmException {
		double javaVersion = Double.parseDouble(System.getProperty("java.specification.version"));
		if(javaVersion >= 1.8) {
			SSLContext.getDefault().getDefaultSSLParameters().setUseCipherSuitesOrder(true);
		} else {
			String pkg = javaVersion >= 1.7 ? "sun.security.ssl." : "com.sun.net.ssl.internal.ssl.";
			Class<?> serverHandshakerClass = Class.forName(pkg + "ServerHandshaker");
			dynamicClassLoader.redefineNonPublicDependees(serverHandshakerClass);
			ClassGen gen = getClassGen(serverHandshakerClass);
			com.sun.org.apache.bcel.internal.classfile.Method method = findMethod(gen, "chooseCipherSuite", Class.forName(pkg + "HandshakeMessage$ClientHello"));
			ConstantPoolGen cp = gen.getConstantPool();
			InstructionFactory factory = new InstructionFactory(gen);
			InstructionList il = createChooseCipherIL(factory);
			MethodGen mg = new MethodGen(method, gen.getClassName(), cp);
			mg.setInstructionList(il);
			mg.setMaxStack();
			mg.setMaxLocals();
			gen.replaceMethod(method, mg.getMethod());
			il.dispose();
			dynamicClassLoader.defineClass(gen);
			dynamicClassLoader.redefineInnerClasses(serverHandshakerClass, true);
			loadPackage(serverHandshakerClass);
			put("SSLContext" + '.' + "TLS", pkg + "SSLContextImpl");
		}
	}

	/* Below got invalid jump error - I don't know why
	protected static InstructionList createChooseCipherIL_hand(InstructionFactory factory) {
		String className = factory.getClassGen().getClassName();
		int pos = className.lastIndexOf('.');
		String pkg = className.substring(0, pos + 1);
		InstructionList il = new InstructionList();
		il.append(factory.createPrintln("********* Choosing Cipher Suite Using Server Preference **************"));
		il.append(factory.createNew(HashSet.class.getName()));
		il.append(InstructionConstants.DUP);
		il.append(InstructionConstants.ALOAD_1);
		Type cipherSuiteListType = new ObjectType(pkg + "CipherSuiteList");
		il.append(factory.createInvoke(pkg + "HandshakeMessage$ClientHello", "getCipherSuites", cipherSuiteListType, Type.NO_ARGS, Constants.INVOKEVIRTUAL));
		InvokeInstruction cslCollectionInvoke = factory.createInvoke(pkg + "CipherSuiteList", "collection", Type.getType(Collection.class), Type.NO_ARGS, Constants.INVOKEVIRTUAL);
		il.append(cslCollectionInvoke);
		il.append(factory.createInvoke(HashSet.class.getName(), "<init>", Type.VOID, Type.NO_ARGS, Constants.INVOKESPECIAL));
		il.append(InstructionConstants.ASTORE_2);
		il.append(InstructionConstants.ALOAD_0);
		il.append(factory.createGetField(className, "enabledCipherSuites", cipherSuiteListType));
		il.append(cslCollectionInvoke);
		il.append(factory.createInvoke(Collection.class.getName(), "iterator", Type.getType(Iterator.class), Type.NO_ARGS, Constants.INVOKEINTERFACE));
		il.append(InstructionConstants.BASTORE);
		BranchInstruction goto106 = InstructionFactory.createBranchInstruction(Constants.GOTO, null);
		BranchInstruction ifeq106 = InstructionFactory.createBranchInstruction(Constants.IFEQ, null);
		BranchInstruction ifacmpeq106 = InstructionFactory.createBranchInstruction(Constants.IF_ACMPEQ, null);
		il.append(goto106);
		InstructionHandle ih32 = il.append(InstructionConstants.BALOAD);
		il.append(factory.createInvoke(Iterator.class.getName(), "next", Type.OBJECT, Type.NO_ARGS, Constants.INVOKEINTERFACE));
		ObjectType cipherSuiteType = new ObjectType(pkg + "CipherSuite");
		il.append(factory.createCheckCast(cipherSuiteType));
		il.append(new ASTORE(3));
		il.append(new ALOAD(3));
		il.append(factory.createInvoke(pkg + "CipherSuite", "isNegotiable", Type.BOOLEAN, Type.NO_ARGS, Constants.INVOKEVIRTUAL));
		il.append(ifeq106);
		il.append(InstructionConstants.ALOAD_2);
		il.append(new ALOAD(3));
		il.append(factory.createInvoke(Set.class.getName(), "contains", Type.BOOLEAN, new Type[] { Type.OBJECT }, Constants.INVOKEINTERFACE));
		il.append(ifeq106);
		il.append(InstructionConstants.ALOAD_0);
		il.append(factory.createGetField(className, "doClientAuth", Type.BYTE));
		il.append(InstructionConstants.ICONST_2);

		BranchInstruction ificmpne94 = InstructionFactory.createBranchInstruction(Constants.IF_ICMPNE, null);
		il.append(ificmpne94);
		il.append(new ALOAD(3));
		ObjectType keyExchangeType = new ObjectType(pkg + "CipherSuite$KeyExchange");
		il.append(factory.createGetField(pkg + "CipherSuite", "keyExchange", keyExchangeType));
		il.append(factory.createGetStatic(pkg + "CipherSuite$KeyExchange", "K_DH_ANON", keyExchangeType));
		il.append(ifacmpeq106);
		il.append(new ALOAD(3));
		il.append(factory.createGetField(pkg + "CipherSuite", "keyExchange", keyExchangeType));
		il.append(factory.createGetStatic(pkg + "CipherSuite$KeyExchange", "K_ECDH_ANON", keyExchangeType));
		il.append(ifacmpeq106);
		InstructionHandle ih94 = il.append(InstructionConstants.ALOAD_0);
		ificmpne94.setTarget(ih94);

		il.append(new ALOAD(3));
		il.append(factory.createInvoke(className, "trySetCipherSuite", Type.BOOLEAN, new Type[] { cipherSuiteType }, Constants.INVOKESPECIAL));
		il.append(ifeq106);
		il.append(InstructionConstants.RETURN);

		InstructionHandle ih106 = il.append(InstructionConstants.BALOAD);
		goto106.setTarget(ih106);
		ifeq106.setTarget(ih106);
		ifacmpeq106.setTarget(ih106);

		il.append(factory.createInvoke(Iterator.class.getName(), "hasNext", Type.BOOLEAN, Type.NO_ARGS, Constants.INVOKEINTERFACE));
		il.append(new IFNE(ih32));
		il.append(InstructionConstants.ALOAD_0);
		il.append(new BIPUSH((byte) 40));
		il.append(new LDC(factory.getConstantPool().addString("no cipher suites in common")));
		il.append(factory.createInvoke(className, "fatalSE", Type.VOID, new Type[] { Type.BYTE, Type.STRING }, Constants.INVOKEVIRTUAL));
		il.append(InstructionConstants.RETURN);
		
		return il;
	}
	*/
	protected static InstructionList createChooseCipherIL(InstructionFactory factory) {
		String className = factory.getClassGen().getClassName();
		int pos = className.lastIndexOf('.');
		String pkg = className.substring(0, pos + 1);
		InstructionList il = new InstructionList();
		// il.append(factory.createPrintln("********* Choosing Cipher Suite Using Server Preference **************"));
		il.append(factory.createNew("java.util.HashSet"));
		il.append(InstructionConstants.DUP);
		il.append(InstructionFactory.createLoad(Type.OBJECT, 1));
		il.append(factory.createInvoke(pkg + "HandshakeMessage$ClientHello", "getCipherSuites", new ObjectType(pkg + "CipherSuiteList"), Type.NO_ARGS, Constants.INVOKEVIRTUAL));
		il.append(factory.createInvoke(pkg + "CipherSuiteList", "collection", new ObjectType("java.util.Collection"), Type.NO_ARGS, Constants.INVOKEVIRTUAL));
		il.append(factory.createInvoke("java.util.HashSet", "<init>", Type.VOID, new Type[] { new ObjectType("java.util.Collection") }, Constants.INVOKESPECIAL));
		il.append(InstructionFactory.createStore(Type.OBJECT, 2));
		il.append(InstructionFactory.createLoad(Type.OBJECT, 0));
		il.append(factory.createFieldAccess(pkg + "ServerHandshaker", "enabledCipherSuites", new ObjectType(pkg + "CipherSuiteList"), Constants.GETFIELD));
		il.append(factory.createInvoke(pkg + "CipherSuiteList", "collection", new ObjectType("java.util.Collection"), Type.NO_ARGS, Constants.INVOKEVIRTUAL));
		il.append(factory.createInvoke("java.util.Collection", "iterator", new ObjectType("java.util.Iterator"), Type.NO_ARGS, Constants.INVOKEINTERFACE));
		il.append(InstructionFactory.createStore(Type.OBJECT, 4));
		BranchInstruction goto_29 = InstructionFactory.createBranchInstruction(Constants.GOTO, null);
		il.append(goto_29);
		InstructionHandle ih_32 = il.append(InstructionFactory.createLoad(Type.OBJECT, 4));
		il.append(factory.createInvoke("java.util.Iterator", "next", Type.OBJECT, Type.NO_ARGS, Constants.INVOKEINTERFACE));
		il.append(factory.createCheckCast(new ObjectType(pkg + "CipherSuite")));
		il.append(InstructionFactory.createStore(Type.OBJECT, 3));
		il.append(InstructionFactory.createLoad(Type.OBJECT, 3));
		il.append(factory.createInvoke(pkg + "CipherSuite", "isNegotiable", Type.BOOLEAN, Type.NO_ARGS, Constants.INVOKEVIRTUAL));
		BranchInstruction ifeq_47 = InstructionFactory.createBranchInstruction(Constants.IFEQ, null);
		il.append(ifeq_47);
		il.append(InstructionFactory.createLoad(Type.OBJECT, 2));
		il.append(InstructionFactory.createLoad(Type.OBJECT, 3));
		il.append(factory.createInvoke("java.util.Set", "contains", Type.BOOLEAN, new Type[] { Type.OBJECT }, Constants.INVOKEINTERFACE));
		BranchInstruction ifne_57 = InstructionFactory.createBranchInstruction(Constants.IFNE, null);
		il.append(ifne_57);
		BranchInstruction goto_60 = InstructionFactory.createBranchInstruction(Constants.GOTO, null);
		il.append(goto_60);
		InstructionHandle ih_63 = il.append(InstructionFactory.createLoad(Type.OBJECT, 0));
		il.append(factory.createFieldAccess(pkg + "ServerHandshaker", "doClientAuth", Type.BYTE, Constants.GETFIELD));
		il.append(new PUSH(factory.getConstantPool(), 2));
		BranchInstruction if_icmpne_68 = InstructionFactory.createBranchInstruction(Constants.IF_ICMPNE, null);
		il.append(if_icmpne_68);
		il.append(InstructionFactory.createLoad(Type.OBJECT, 3));
		il.append(factory.createFieldAccess(pkg + "CipherSuite", "keyExchange", new ObjectType(pkg + "CipherSuite$KeyExchange"), Constants.GETFIELD));
		il.append(factory.createFieldAccess(pkg + "CipherSuite$KeyExchange", "K_DH_ANON", new ObjectType(pkg + "CipherSuite$KeyExchange"), Constants.GETSTATIC));
		BranchInstruction if_acmpeq_78 = InstructionFactory.createBranchInstruction(Constants.IF_ACMPEQ, null);
		il.append(if_acmpeq_78);
		il.append(InstructionFactory.createLoad(Type.OBJECT, 3));
		il.append(factory.createFieldAccess(pkg + "CipherSuite", "keyExchange", new ObjectType(pkg + "CipherSuite$KeyExchange"), Constants.GETFIELD));
		il.append(factory.createFieldAccess(pkg + "CipherSuite$KeyExchange", "K_ECDH_ANON", new ObjectType(pkg + "CipherSuite$KeyExchange"), Constants.GETSTATIC));
		BranchInstruction if_acmpne_88 = InstructionFactory.createBranchInstruction(Constants.IF_ACMPNE, null);
		il.append(if_acmpne_88);
		BranchInstruction goto_91 = InstructionFactory.createBranchInstruction(Constants.GOTO, null);
		il.append(goto_91);
		InstructionHandle ih_94 = il.append(InstructionFactory.createLoad(Type.OBJECT, 0));
		il.append(InstructionFactory.createLoad(Type.OBJECT, 3));
		il.append(factory.createInvoke(pkg + "ServerHandshaker", "trySetCipherSuite", Type.BOOLEAN, new Type[] { new ObjectType(pkg + "CipherSuite") }, Constants.INVOKESPECIAL));
		BranchInstruction ifne_99 = InstructionFactory.createBranchInstruction(Constants.IFNE, null);
		il.append(ifne_99);
		BranchInstruction goto_102 = InstructionFactory.createBranchInstruction(Constants.GOTO, null);
		il.append(goto_102);
		InstructionHandle ih_105 = il.append(InstructionFactory.createReturn(Type.VOID));
		InstructionHandle ih_106 = il.append(InstructionFactory.createLoad(Type.OBJECT, 4));
		il.append(factory.createInvoke("java.util.Iterator", "hasNext", Type.BOOLEAN, Type.NO_ARGS, Constants.INVOKEINTERFACE));
		BranchInstruction ifne_113 = InstructionFactory.createBranchInstruction(Constants.IFNE, ih_32);
		il.append(ifne_113);
		il.append(InstructionFactory.createLoad(Type.OBJECT, 0));
		il.append(new PUSH(factory.getConstantPool(), 40));
		il.append(new PUSH(factory.getConstantPool(), "no cipher suites in common"));
		il.append(factory.createInvoke(pkg + "ServerHandshaker", "fatalSE", Type.VOID, new Type[] { Type.INT, Type.STRING }, Constants.INVOKESPECIAL));
		il.append(InstructionFactory.createReturn(Type.VOID));
		goto_29.setTarget(ih_106);
		ifeq_47.setTarget(ih_106);
		ifne_57.setTarget(ih_63);
		goto_60.setTarget(ih_106);
		if_icmpne_68.setTarget(ih_94);
		if_acmpeq_78.setTarget(ih_106);
		if_acmpne_88.setTarget(ih_94);
		goto_91.setTarget(ih_106);
		ifne_99.setTarget(ih_105);
		goto_102.setTarget(ih_106);
		return il;
	}
	protected static void loadPackage(Class<?> sample) throws ClassNotFoundException {
		URL sampleUrl = sample.getResource(sample.getSimpleName() + ".class");
		String packageName = sample.getPackage().getName();
		if("jar".equals(sampleUrl.getProtocol())) {
			String jarLoc = sampleUrl.getFile();
			if(jarLoc != null && jarLoc.startsWith("file:")) {
				int p = jarLoc.indexOf('!', 5);
				if(p < 0)
					p = jarLoc.length();
				String jarPath;
				try {
					jarPath = URLDecoder.decode(jarLoc.substring(5, p), "UTF-8");
				} catch(UnsupportedEncodingException e) {
					throw new ClassNotFoundException("Could not decode url", e);
				}
				JarFile jarFile;
				try {
					jarFile = new JarFile(jarPath);
				} catch(IOException e) {
					throw new ClassNotFoundException("Could not read jar file (" + jarPath + ") that contains class '" + sample.getName() + "'", e);
				}
				for(Enumeration<JarEntry> en = jarFile.entries(); en.hasMoreElements();) {
					JarEntry je = en.nextElement();
					String entryClassName = je.getName();
					if(!entryClassName.endsWith(".class") || entryClassName.indexOf('$') >= 0)
						continue;
					entryClassName = entryClassName.substring(0, entryClassName.length() - 6).replace('/', '.').replace('\\', '.');
					if(entryClassName.startsWith(packageName)) {
						try {
							dynamicClassLoader.redefineClass(entryClassName, jarFile.getInputStream(je), true);
						} catch(IOException e) {
							throw new ClassNotFoundException("Could not read jar entry " + je.getName() + " from '" + jarPath + "'", e);
						}
					}
				}
			}
		} else if("file".equals(sampleUrl.getProtocol())) {
			File file = new File(sampleUrl.getFile());
			if(file.getParentFile() != null) {
				File[] classFiles = file.listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						return pathname.isFile() && pathname.getName().endsWith(".class") && pathname.getName().indexOf('$') < 0;
					}
				});
				for(File cf : classFiles) {
					try {
						dynamicClassLoader.redefineClass(cf.getName().substring(0, cf.getName().length() - 6), new FileInputStream(cf), true);
					} catch(IOException e) {
						throw new ClassNotFoundException("Could not read class file " + cf.getAbsolutePath(), e);
					}
				}
			}
		}
	}

	protected static void loadClasses(String... classNames) throws ClassNotFoundException {
		for(String cn : classNames) {
			dynamicClassLoader.redefineClass(cn, true);
		}
	}

	public static ClassGen getClassGen(String className) throws ClassNotFoundException {
		return getClassGen(Class.forName(className));
	}

	public static ClassGen getClassGen(Class<?> baseClass) throws ClassNotFoundException {
		// JavaClass javaclass = Repository.getRepository().loadClass(className);
		String path = new StringBuilder(baseClass.getName().length() + 7).append('/').append(baseClass.getName().replace('.', '/')).append(".class").toString();
		InputStream in = baseClass.getResourceAsStream(path);
		if(in == null)
			throw new ClassNotFoundException("Could not find class file for " + baseClass.getSimpleName() + ".class");
		ClassParser parser = new ClassParser(in, baseClass.getName());
		JavaClass javaclass;
		try {
			javaclass = parser.parse();
		} catch(ClassFormatException e) {
			throw new ClassNotFoundException("Could not parse class", e);
		} catch(IOException e) {
			throw new ClassNotFoundException("Could not parse class", e);
		}

		return new ClassGen(javaclass);
	}
	protected static com.sun.org.apache.bcel.internal.classfile.Method findMethod(ClassGen gen, String methodName, Class<?>... paramTypes) throws ClassNotFoundException {
		OUTER: for(com.sun.org.apache.bcel.internal.classfile.Method method : gen.getMethods()) {
			if(!method.getName().equals(methodName))
				continue;
			if(paramTypes == null)
				return method;
			Type[] types = method.getArgumentTypes();
			if(paramTypes.length != types.length)
				continue;
			for(int i = 0; i < paramTypes.length; i++) {
				if(paramTypes[i] == null)
					continue;
				Class<?> definedParamType = getClassFromType(types[i]);
				if(!definedParamType.isAssignableFrom(paramTypes[i]))
					continue OUTER;
			}
			return method;
		}
		return null;
	}

	protected static Class<?> getClassFromType(Type type) throws ClassNotFoundException {
		if(type instanceof ObjectType) {
			return Class.forName(((ObjectType) type).getClassName());
		} else if(type instanceof ArrayType) {
			return Class.forName(type.getSignature());
		} else if(type instanceof BasicType) {
			switch(type.getType()) {
				case Constants.T_VOID:
					return void.class;
				case Constants.T_BOOLEAN:
					return boolean.class;
				case Constants.T_BYTE:
					return byte.class;
				case Constants.T_SHORT:
					return short.class;
				case Constants.T_CHAR:
					return char.class;
				case Constants.T_INT:
					return int.class;
				case Constants.T_LONG:
					return long.class;
				case Constants.T_DOUBLE:
					return double.class;
				case Constants.T_FLOAT:
					return float.class;
				default:
					throw new ClassGenException("Invalid type: " + type);
			}
		}
		return null;
	}
	public static <C> C getDelegate(Class<? extends SimpleProvider> providerClass, Class<C> clazz, String type, String algorithm) throws NoSuchAlgorithmException, ClassCastException {
		List<java.security.Provider> providers = Providers.getProviderList().providers();
		int i = 0;
		for(;i < providers.size(); i++) {
			if(providers.get(i).getClass().equals(providerClass))
				break;
		}
		if(i >= providers.size())
			throw new NoSuchAlgorithmException("This provider is not in the provider list");
		for(i++; i < providers.size(); i++) {
			Service service = providers.get(i).getService(type, algorithm);
			if(service != null) {
				return clazz.cast(GetInstance.getInstance(service, clazz).impl);
			}
		}
		throw new NoSuchAlgorithmException("No other providers implementing '" + algorithm + "' were found after this one");
	}

	public static Object getDelegate(String providerClassName, String serviceClassName, String type, String algorithm) throws NoSuchAlgorithmException, ClassCastException, ClassNotFoundException {
		return getDelegate(dynamicClassLoader.loadClass(providerClassName).asSubclass(SimpleProvider.class), Class.forName(serviceClassName), type, algorithm);
	}

	public static SimpleProvider createProvider(String name) throws InstantiationException, IllegalAccessException, ClassCastException {
		Class<?> providerClass = makeProviderClass(name);
		SimpleProvider provider = (SimpleProvider)providerClass.newInstance();
		return provider;
	}

	public static SimpleProvider createProvider(String name, InputStream serviceSpecData) throws InstantiationException, IllegalAccessException, ClassNotFoundException, ClassCastException, IOException {
		return createProvider(name, new BufferedReader(new InputStreamReader(serviceSpecData)));
	}

	public static SimpleProvider createProvider(String name, File serviceSpecFile) throws InstantiationException, IllegalAccessException, ClassNotFoundException, ClassCastException, IOException {
		return createProvider(name, new BufferedReader(new FileReader(serviceSpecFile)));
	}

	public static SimpleProvider createProvider(String name, String serviceSpecFile) throws InstantiationException, IllegalAccessException, ClassNotFoundException, ClassCastException, IOException {
		File file = new File(serviceSpecFile);
		if(!file.exists()) {
			ClassLoader cl = SimpleProvider.class.getClassLoader();
			if(cl == null)
				cl = ClassLoader.getSystemClassLoader();
			InputStream in = cl.getResourceAsStream(serviceSpecFile);
			return createProvider(name, in);
		}
		return createProvider(name, new BufferedReader(new FileReader(file)));
	}

	public static SimpleProvider createProvider(String name, BufferedReader serviceSpecData) throws InstantiationException, IllegalAccessException, ClassNotFoundException, ClassCastException, IOException {
		SimpleProvider provider = createProvider(name);
		provider.registerServiceSpecs(serviceSpecData);
		return provider;
	}

	public void registerServiceSpecs(InputStream serviceSpecData) throws ClassNotFoundException, ClassCastException, IOException {
		registerServiceSpecs(new BufferedReader(new InputStreamReader(serviceSpecData)));
	}

	public void registerServiceSpecs(File serviceSpecFile) throws ClassNotFoundException, ClassCastException, IOException {
		registerServiceSpecs(new BufferedReader(new FileReader(serviceSpecFile)));
	}
	public void registerServiceSpecs(String serviceSpecFile) throws ClassNotFoundException, ClassCastException, IOException {
		File file = new File(serviceSpecFile);
		if(!file.exists()) {
			ClassLoader cl = SimpleProvider.class.getClassLoader();
			if(cl == null)
				cl = ClassLoader.getSystemClassLoader();
			InputStream in = cl.getResourceAsStream(serviceSpecFile);
			registerServiceSpecs(in);
		} else {
			registerServiceSpecs(new BufferedReader(new FileReader(file)));
		}
	}
	public void registerServiceSpecs(BufferedReader serviceSpecData) throws ClassNotFoundException, ClassCastException, IOException {
		List<ServiceSpec<?>> specs = new ArrayList<ServiceSpec<?>>();
		String line;
		int n = 1;
		while((line=serviceSpecData.readLine()) != null) {
			if((line=line.trim()).length() > 0) {
				StringTokenizer tok = new StringTokenizer(line, "|,;");
				int c =  tok.countTokens();
				if(c != 4)
					throw new IOException("Invalid format in line " + n + ": four values separated by '|', ';', or ',' are required and " + c + " were provided");
				String serviceType = tok.nextToken();
				String algorithm = tok.nextToken();
				Class<?> serviceClass = Class.forName(tok.nextToken());
				Class<?> implClass = Class.forName(tok.nextToken());
				specs.add(createServiceSpec(serviceType, algorithm, serviceClass, implClass));
			}
			n++;
		}
		register(specs.toArray(new ServiceSpec<?>[specs.size()]));
	}

	/**
	 * @param serviceType
	 * @param algorithm
	 * @param serviceClass
	 * @param asSubclass
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected static ServiceSpec<?> createServiceSpec(String serviceType, String algorithm, Class<?> serviceClass, Class<?> implClass) throws ClassCastException {
		return new ServiceSpec(serviceType, algorithm, serviceClass, implClass.asSubclass(serviceClass));
	}
	@Override
	public String getInfo() {
		return info;
	}
	
	public static Object findAndExecuteMethod(Object subject, String methodName, Class<?>[] paramTypes, Object... paramValues) throws NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Method method = findMethod(subject.getClass(), methodName, paramTypes);
		boolean acc = method.isAccessible();
		if(!acc)
			method.setAccessible(true);
		try {
			return method.invoke(subject, paramValues);
		} finally {
			if(!acc)
				method.setAccessible(false);
		}

	}

	public static Method findMethod(Class<?> clazz, String methodName, Class<?>... paramTypes) throws NoSuchMethodException {
		try {
			return clazz.getDeclaredMethod(methodName, paramTypes);
		} catch(NoSuchMethodException e) {
			Class<?> c = clazz.getSuperclass();
			while(c != null)
				try {
					return c.getDeclaredMethod(methodName, paramTypes);
				} catch(NoSuchMethodException e0) {
					c = c.getSuperclass();
				}
			throw e;
		}
	}

	protected static Properties properties = null;

	/*
	 * This is copied from SecurityUtils to improve dependencies
	 */
	public static Properties getProperties() {
		if(properties == null) {
			properties = new Properties(System.getProperties());
			String fileName = properties.getProperty("simple.security.config.file", "security.properties");
			InputStream in = SimpleProvider.class.getClassLoader().getResourceAsStream(fileName);
			if(in == null) {
				File file = new File(fileName);
				if(file.canRead()) {
					try {
						in = new FileInputStream(file);
					} catch(FileNotFoundException e) {
						System.err.println("Could not read Simple security properties file '" + file.getAbsolutePath() + "'");
						e.printStackTrace();
					}
				}
			}
			if(in != null) {
				try {
					properties.load(in);
				} catch(IOException e) {
					System.err.println("Error while reading Simple security properties file '" + fileName + "'");
					e.printStackTrace();
				}
			}
		}
		return properties;
	}
}
