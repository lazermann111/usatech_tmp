package simple.security.provider.ssl;

import java.security.KeyStore;

import javax.net.ssl.KeyManager;

public interface KeyStoreAware extends KeyManager {

	public void registerKeyStore(KeyStore ks);

}
