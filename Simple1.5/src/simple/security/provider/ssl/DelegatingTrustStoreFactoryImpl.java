package simple.security.provider.ssl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactorySpi;
import javax.net.ssl.X509TrustManager;

import simple.security.KeyStoreConfig;
import simple.security.SecurityUtils;
import simple.security.provider.SimpleProvider;

/**
 * To use this add the following to "ServiceSpecs.lst" file:
 * <code>TrustManagerFactory,SunX509,javax.net.ssl.TrustManagerFactorySpi,simple.security.ssl.DelegatingTrustStoreFactoryImpl
 * TrustManagerFactory,PKIX,javax.net.ssl.TrustManagerFactorySpi,simple.security.ssl.DelegatingTrustStoreFactoryImpl
 * </code>
 * @author bkrug
 *
 */
public class DelegatingTrustStoreFactoryImpl extends TrustManagerFactorySpi {
	protected final TrustManagerFactorySpi delegate;
	protected TrustManager[] trustManagers;

	public DelegatingTrustStoreFactoryImpl(TrustManagerFactorySpi delegate) {
		this.delegate = delegate;
	}

	@Override
	protected TrustManager[] engineGetTrustManagers() {
		return trustManagers;
	}

	@Override
	protected void engineInit(KeyStore ks) throws KeyStoreException {
		try {
			KeyStoreConfig ksc = new KeyStoreConfig("javax.net.ssl.trustStore", SimpleProvider.getProperties());
			if (ksc.getFile() == null)
				return;
			ks = SecurityUtils.getKeyStore(ksc);
		} catch(NoSuchProviderException e) {
			throw new KeyStoreException("Could not load truststore", e);
		} catch(NoSuchAlgorithmException e) {
			throw new KeyStoreException("Could not load truststore", e);
		} catch(CertificateException e) {
			throw new KeyStoreException("Could not load truststore", e);
		} catch(IOException e) {
			throw new KeyStoreException("Could not load truststore", e);
		}
		try {
			internalEngineInit(new Class[] { KeyStore.class }, ks);
		} catch(IllegalArgumentException e) {
			throw new KeyStoreException("Could not initialize", e);
		} catch(SecurityException e) {
			throw new KeyStoreException("Could not initialize", e);
		} catch(IllegalAccessException e) {
			throw new KeyStoreException("Could not initialize", e);
		} catch(InvocationTargetException e) {
			if(e.getCause() instanceof KeyStoreException)
				throw (KeyStoreException) e.getCause();
			throw new KeyStoreException("Could not initialize", e);
		} catch(NoSuchMethodException e) {
			throw new KeyStoreException("Could not initialize", e);
		}
	}

	@Override
	protected void engineInit(ManagerFactoryParameters spec) throws InvalidAlgorithmParameterException {
		try {
			internalEngineInit(new Class[] { ManagerFactoryParameters.class }, spec);
		} catch(IllegalArgumentException e) {
			throw new InvalidAlgorithmParameterException("Could not initialize", e);
		} catch(SecurityException e) {
			throw new InvalidAlgorithmParameterException("Could not initialize", e);
		} catch(IllegalAccessException e) {
			throw new InvalidAlgorithmParameterException("Could not initialize", e);
		} catch(InvocationTargetException e) {
			if(e.getCause() instanceof InvalidAlgorithmParameterException)
				throw (InvalidAlgorithmParameterException) e.getCause();
			throw new InvalidAlgorithmParameterException("Could not initialize", e);
		} catch(NoSuchMethodException e) {
			throw new InvalidAlgorithmParameterException("Could not initialize", e);
		}
	}	
	
	protected void internalEngineInit(Class<?>[] paramTypes, Object... parameters) throws IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		SimpleProvider.findAndExecuteMethod(delegate, "engineInit", paramTypes, parameters);
		if(isTrustManagerAcceptAny())
			trustManagers = new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
		else
			trustManagers = (TrustManager[]) SimpleProvider.findAndExecuteMethod(delegate, "engineGetTrustManagers", SimpleProvider.EMPTY_PARAM_TYPES);
	}

	protected boolean isTrustManagerAcceptAny() {
		String value = SimpleProvider.getProperties().getProperty("javax.net.ssl.TrustManager.acceptAny");
		if(value == null || (value = value.trim()).isEmpty())
			return false;
		return "true".equalsIgnoreCase(value);
	}
}
