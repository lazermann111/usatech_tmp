/**
 *
 */
package simple.security.provider.ssl;

import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.X509ExtendedKeyManager;
import javax.net.ssl.X509KeyManager;

/**
 * X509KeyManager which allows selection of a specific keypair and certificate
 * chain (identified by their keystore alias name) to be used by the server to
 * authenticate itself to SSL clients.
 * 
 * @author Jan Luehe (adapted from Apache Tomcat), Brian S. Krug
 */
public class SelectedAliasKeyManager extends X509ExtendedKeyManager implements X509KeyManager {
	public static final String ALIAS_NONE_STRING = "-";
	private final X509KeyManager delegate;
	private final String serverKeyAlias;
	private final String defaultClientKeyAlias;
	private final Map<Pattern,String> clientKeyAliasMapping;

	protected static Map<Pattern, String> convertToPatternMap(String s) {
		if(s == null)
			return null;
		if((s = s.trim()).length() == 0)
			return Collections.emptyMap();
		if(s.startsWith("{") && s.endsWith("}")) {
			s = s.substring(1, s.length() - 1);
		}
		Map<Pattern, String> map = new LinkedHashMap<Pattern, String>();
		int start = 0;
		String key = null;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			switch(ch) {
				case '\\': // escape next char
					sb.append(s, start, i);
					sb.append(s, i + 1, i + 2);
					start = i + 2;
					i++;
					break;
				case '=':
					if(key != null)
						throw new IllegalArgumentException("Found extra '=' at character " + (i + 1) + " of '" + s + "'");
					sb.append(s, start, i);
					key = sb.toString();
					sb.setLength(0);
					start = i + 1;
					break;
				case ',':
				case ';':
					sb.append(s, start, i);
					String value;
					if(key == null) {
						key = sb.toString();
						value = null;
					} else {
						value = sb.toString();
					}
					map.put(Pattern.compile(key), value);
					sb.setLength(0);
					start = i + 1;
					key = null;
					break;
				case ' ':
					if(key == null && sb.length() == 0)
						start = i + 1;
					break;
			}
		}
		sb.append(s, start, s.length());
		if(key != null) {
			map.put(Pattern.compile(key), sb.toString());
		} else if(sb.length() > 0) {
			map.put(Pattern.compile(sb.toString()), null);
		}
		return map;
	}

	public SelectedAliasKeyManager(X509KeyManager mgr, Properties properties) {
		this(mgr, properties.getProperty("javax.net.ssl.serverKeyAlias"), properties.getProperty("javax.net.ssl.clientKeyAlias"), convertToPatternMap(properties.getProperty("javax.net.ssl.clientKeyAliasMap")));
	}

	/**
	 * Constructor.
	 *
	 * @param mgr
	 *            The X509KeyManager used as a delegate
	 * @param serverKeyAlias
	 *            The alias name of the server's keypair and supporting
	 *            certificate chain
	 */
	public SelectedAliasKeyManager(X509KeyManager mgr, String serverKeyAlias, String defaultClientKeyAlias) {
		this(mgr, serverKeyAlias, defaultClientKeyAlias, null);
	}
	/**
	 * Constructor.
	 *
	 * @param mgr
	 *            The X509KeyManager used as a delegate
	 * @param serverKeyAlias
	 *            The alias name of the server's keypair and supporting
	 *            certificate chain
	 */
	public SelectedAliasKeyManager(X509KeyManager mgr, String serverKeyAlias, String defaultClientKeyAlias, Map<Pattern,String> clientKeyAliasMapping) {
		this.delegate = mgr;
		this.serverKeyAlias = serverKeyAlias;
		this.defaultClientKeyAlias = defaultClientKeyAlias;
		this.clientKeyAliasMapping = clientKeyAliasMapping;
	}

	/**
	 * Choose an alias to authenticate the client side of a secure socket, given
	 * the public key type and the list of certificate issuer authorities
	 * recognized by the peer (if any).
	 *
	 * @param keyType
	 *            The key algorithm type name(s), ordered with the
	 *            most-preferred key type first
	 * @param issuers
	 *            The list of acceptable CA issuer subject names, or null if it
	 *            does not matter which issuers are used
	 * @param socket
	 *            The socket to be used for this connection. This parameter can
	 *            be null, in which case this method will return the most
	 *            generic alias to use
	 *
	 * @return The alias name for the desired key, or null if there are no
	 *         matches
	 */
	public String chooseClientAlias(String[] keyType, Principal[] issuers, Socket socket) {
		if(clientKeyAliasMapping != null && !clientKeyAliasMapping.isEmpty()) {
			int port = socket.getPort();
			String host = socket.getInetAddress().getHostName() + ':' + port;
			String ip = socket.getInetAddress().getHostAddress() + ':' + port;
			for(Map.Entry<Pattern, String> entry : clientKeyAliasMapping.entrySet()) {
				if(entry.getKey() != null && (entry.getKey().matcher(host).matches() || entry.getKey().matcher(ip).matches()))
					return entry.getValue();
			}
		}
		return defaultClientKeyAlias != null ? ALIAS_NONE_STRING.equals(defaultClientKeyAlias) ? null : defaultClientKeyAlias : delegate.chooseClientAlias(keyType, issuers, socket);
	}

	/**
	 * Returns this key manager's server key alias that was provided in the
	 * constructor.
	 *
	 * @param keyType
	 *            The key algorithm type name (ignored)
	 * @param issuers
	 *            The list of acceptable CA issuer subject names, or null if it
	 *            does not matter which issuers are used (ignored)
	 * @param socket
	 *            The socket to be used for this connection. This parameter can
	 *            be null, in which case this method will return the most
	 *            generic alias to use (ignored)
	 *
	 * @return Alias name for the desired key
	 */
	public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {
		return serverKeyAlias != null ? ALIAS_NONE_STRING.equals(serverKeyAlias) ? null : serverKeyAlias : delegate.chooseServerAlias(keyType, issuers, socket);
	}

	/**
	 * Returns the certificate chain associated with the given alias.
	 *
	 * @param alias
	 *            The alias name
	 *
	 * @return Certificate chain (ordered with the user's certificate first and
	 *         the root certificate authority last), or null if the alias can't
	 *         be found
	 */
	public X509Certificate[] getCertificateChain(String alias) {
		return delegate.getCertificateChain(alias);
	}

	/**
	 * Get the matching aliases for authenticating the client side of a secure
	 * socket, given the public key type and the list of certificate issuer
	 * authorities recognized by the peer (if any).
	 *
	 * @param keyType
	 *            The key algorithm type name
	 * @param issuers
	 *            The list of acceptable CA issuer subject names, or null if it
	 *            does not matter which issuers are used
	 *
	 * @return Array of the matching alias names, or null if there were no
	 *         matches
	 */
	public String[] getClientAliases(String keyType, Principal[] issuers) {
		return delegate.getClientAliases(keyType, issuers);
	}

	/**
	 * Get the matching aliases for authenticating the server side of a secure
	 * socket, given the public key type and the list of certificate issuer
	 * authorities recognized by the peer (if any).
	 *
	 * @param keyType
	 *            The key algorithm type name
	 * @param issuers
	 *            The list of acceptable CA issuer subject names, or null if it
	 *            does not matter which issuers are used
	 *
	 * @return Array of the matching alias names, or null if there were no
	 *         matches
	 */
	public String[] getServerAliases(String keyType, Principal[] issuers) {
		return delegate.getServerAliases(keyType, issuers);
	}

	/**
	 * Returns the key associated with the given alias.
	 *
	 * @param alias
	 *            The alias name
	 *
	 * @return The requested key, or null if the alias can't be found
	 */
	public PrivateKey getPrivateKey(String alias) {
		return delegate.getPrivateKey(alias);
	}

	@Override
	public String chooseEngineClientAlias(String[] keyType, Principal[] issuers, SSLEngine engine) {
		if(clientKeyAliasMapping != null && !clientKeyAliasMapping.isEmpty()) {
			int port = engine.getPeerPort();
			String host = engine.getPeerHost() + ':' + port;
			String ip;
			InetAddress ia;
			try {
				ia = InetAddress.getByName(engine.getPeerHost());
				host = engine.getPeerHost() + ':' + port;
				ip = ia.getHostAddress() + ':' + port;
			} catch(UnknownHostException e) {
				ip = "<UNKNOWN>:" + port;
			}
			for(Map.Entry<Pattern, String> entry : clientKeyAliasMapping.entrySet()) {
				if(entry.getKey() != null && (entry.getKey().matcher(host).matches() || entry.getKey().matcher(ip).matches()))
					return entry.getValue();
			}
		}
		return defaultClientKeyAlias != null ? ALIAS_NONE_STRING.equals(defaultClientKeyAlias) ? null : defaultClientKeyAlias : super.chooseEngineClientAlias(keyType, issuers, engine);
	}

	@Override
	public String chooseEngineServerAlias(String keyType, Principal[] issuers, SSLEngine engine) {
		return serverKeyAlias != null ? ALIAS_NONE_STRING.equals(serverKeyAlias) ? null : serverKeyAlias : super.chooseEngineServerAlias(keyType, issuers, engine);
	}
}
