/**
 *
 */
package simple.security.provider.ssl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Properties;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactorySpi;
import javax.net.ssl.ManagerFactoryParameters;

import simple.security.KeyStoreConfig;
import simple.security.SecurityUtils;
import simple.security.provider.SimpleProvider;


/**
 * @author Brian S. Krug
 *
 */
public class DelegatingKeyManagerFactoryImpl extends KeyManagerFactorySpi {
	protected final KeyManagerFactorySpi delegate;
	protected KeyManager[] keyManagers;

	/**
	 *
	 */
	public DelegatingKeyManagerFactoryImpl(KeyManagerFactorySpi delegate) {
		this.delegate = delegate;
	}

	/**
	 * @see javax.net.ssl.KeyManagerFactorySpi#engineGetKeyManagers()
	 */
	@Override
	protected KeyManager[] engineGetKeyManagers() {
		return keyManagers;
	}

	/**
	 * @see javax.net.ssl.KeyManagerFactorySpi#engineInit(javax.net.ssl.ManagerFactoryParameters)
	 */
	@Override
	protected void engineInit(ManagerFactoryParameters spec) throws InvalidAlgorithmParameterException {
		try {
			internalEngineInit(null, new Class[] { ManagerFactoryParameters.class }, spec);
		} catch(IllegalArgumentException | SecurityException | IllegalAccessException | ClassNotFoundException | ClassCastException | NoSuchMethodException e) {
			throw new InvalidAlgorithmParameterException("Could not initialize", e);
		} catch(InvocationTargetException | InstantiationException e) {
			if(e.getCause() instanceof InvalidAlgorithmParameterException)
				throw (InvalidAlgorithmParameterException) e.getCause();
			throw new InvalidAlgorithmParameterException("Could not initialize", e);
		}
	}

	/**
	 * @see javax.net.ssl.KeyManagerFactorySpi#engineInit(java.security.KeyStore, char[])
	 */
	@Override
	protected void engineInit(KeyStore ks, char[] password) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
		try {
			KeyStoreConfig ksc = new KeyStoreConfig("javax.net.ssl.keyStore", SimpleProvider.getProperties());
			if(ksc.getFile() == null && ksc.getType() == null)
				return;
			ks = SecurityUtils.getKeyStore(ksc);
			password = ksc.getPassword();
		} catch(NoSuchProviderException e) {
			throw new KeyStoreException("Could not load truststore", e);
		} catch(NoSuchAlgorithmException e) {
			throw new KeyStoreException("Could not load truststore", e);
		} catch(CertificateException e) {
			throw new KeyStoreException("Could not load truststore", e);
		} catch(IOException e) {
			throw new KeyStoreException("Could not load truststore", e);
		}
		try {
			internalEngineInit(ks, new Class[] { KeyStore.class, char[].class }, ks, password);
		} catch(IllegalArgumentException | SecurityException | IllegalAccessException | ClassNotFoundException | ClassCastException | NoSuchMethodException e) {
			throw new KeyStoreException("Could not initialize", e);
		} catch(InvocationTargetException | InstantiationException e) {
			if(e.getCause() instanceof KeyStoreException)
				throw (KeyStoreException) e.getCause();
			if(e.getCause() instanceof NoSuchAlgorithmException)
				throw (NoSuchAlgorithmException) e.getCause();
			if(e.getCause() instanceof UnrecoverableKeyException)
				throw (UnrecoverableKeyException) e.getCause();
			throw new KeyStoreException("Could not initialize", e);
		}
	}

	protected void internalEngineInit(KeyStore ks, Class<?>[] paramTypes, Object... parameters) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException, ClassNotFoundException, ClassCastException, InstantiationException {
		//invoke delegate methods
		SimpleProvider.findAndExecuteMethod(delegate, "engineInit", paramTypes, parameters);
		KeyManager[] kms = (KeyManager[])SimpleProvider.findAndExecuteMethod(delegate, "engineGetKeyManagers", SimpleProvider.EMPTY_PARAM_TYPES);
		Properties properties = SimpleProvider.getProperties();
		String delegatingKeyManagerClassName = properties.getProperty("javax.net.ssl.keyManagerClass", "simple.security.provider.ssl.SelectedAliasKeyManager");
		Class<? extends KeyManager> delegatingKeyManagerClass = Class.forName(delegatingKeyManagerClassName).asSubclass(KeyManager.class);
		Constructor<?> bestConstructor = null;
		Object[] params = null;
		int delegateIndex = -1;
		Class<?> delegateType = null;
		for(Constructor<?> con : delegatingKeyManagerClass.getConstructors()) {
			Class<?>[] conParamTypes = con.getParameterTypes();
			switch(conParamTypes.length) {
				case 0:
					if(bestConstructor == null) {
						bestConstructor = con;
						params = new Object[0];
					}
					break;
				case 1:
					if(KeyManager.class.isAssignableFrom(conParamTypes[0])) {
						if(bestConstructor == null || params.length < 1 || delegateType == null || (params.length == 1 && conParamTypes[0].isAssignableFrom(delegateType))) {
							bestConstructor = con;
							params = new Object[1];
							delegateIndex = 0;
							delegateType = conParamTypes[0];
						}
					} else if(conParamTypes[0].isAssignableFrom(Properties.class)) {
						if(bestConstructor == null || params.length < 1) {
							bestConstructor = con;
							params = new Object[1];
							params[0] = properties;

						}
					}
					break;
				case 2:
					if(KeyManager.class.isAssignableFrom(conParamTypes[0]) && conParamTypes[1].isAssignableFrom(Properties.class)) {
						if(bestConstructor == null || params.length < 2 || delegateType == null || conParamTypes[0].isAssignableFrom(delegateType)) {
							bestConstructor = con;
							params = new Object[2];
							params[1] = properties;
							delegateIndex = 0;
							delegateType = conParamTypes[0];
						}
					} else if(KeyManager.class.isAssignableFrom(conParamTypes[1]) && conParamTypes[0].isAssignableFrom(Properties.class)) {
						if(bestConstructor == null || params.length < 2 || delegateType == null || conParamTypes[1].isAssignableFrom(delegateType)) {
							bestConstructor = con;
							params = new Object[2];
							params[0] = properties;
							delegateIndex = 1;
							delegateType = conParamTypes[1];
						}
					}
					break;
			}
		}
		if(bestConstructor == null)
			throw new IllegalArgumentException("Could not find a valid constructor for " + delegatingKeyManagerClass.getName());
		for(int i = 0; i < kms.length; i++) {
			if(delegateIndex >= 0 && !delegateType.isInstance(kms[i]))
				continue;
			if(delegateIndex >= 0)
				params[delegateIndex] = kms[i];
			kms[i] = (KeyManager) bestConstructor.newInstance(params);
			if(ks != null && kms[i] instanceof KeyStoreAware)
				((KeyStoreAware) kms[i]).registerKeyStore(ks);
		}
		keyManagers = kms;
	}
}
