package simple.security;

import simple.lang.InvalidStringValueException;

public class PasswordExpirationException extends InvalidStringValueException {
	private static final long serialVersionUID = -2511625827625115489L;

	public PasswordExpirationException(String message, String value) {
		super(message, value);
	}

	public PasswordExpirationException(String message, Throwable cause, String value) {
		super(message, cause, value);
	}

	public PasswordExpirationException(String value) {
		super(value);
	}

	public PasswordExpirationException(Throwable cause, String value) {
		super(cause, value);
	}

}
