package simple.service.modem.service;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.io.Log;
import simple.service.modem.service.dto.DeviceModemState;
import simple.service.modem.service.dto.DeviceOperationResult;
import simple.service.modem.service.dto.ModemState;
import simple.service.modem.service.exception.ModemServiceException;

import java.sql.SQLException;
import java.util.Date;

/**
 * Processor for processing modem status API calls and save results into USAPDB database
 *
 * @author dlozenko
 */
public class ModemStatusDbResultProcessor implements ResultProcessor<DeviceOperationResult> {
    static final Log log = Log.getLog();
    DeviceModemStatusDao deviceModemStatusDao = new DeviceModemStatusDaoImpl();

    @Override
    public void process(DeviceOperationResult response) throws ModemServiceException {
        if (response == null) {
            throw new IllegalStateException("Null response could not be processed!");
        }
        log.info("Got operation result to process: " + response.getOperationStatus());
        switch (response.getOperationStatus()) {
            case COMPLETED:
                updateDeviceState(response);
                break;
            case POSTPONED:
                updateStatusAwaitingState(response);
                break;
            case ERROR:
                updateErrorState(response);
                break;
            default:
                throw new IllegalStateException("Unknown operation result status: " + response.getOperationStatus().name());
        }
    }

    void updateDeviceState(DeviceOperationResult response) throws ModemServiceException {
        try {
        	  DeviceModemState status = null;
	        	if (response.getRequestId() != null) {
	        		status = completeByRequestId(response);
	        	} else {
              status = deviceModemStatusDao.getDeviceModemStatus(response.getDevice().getSerialCode());
	            boolean hasStatusRequest = status != null;
	            if (hasStatusRequest && (status.getRequestId() == null || status.getStatusRequestedTs() == null)) {
	                // Drop obsolete results which already processed
	                return;
	            }
	            if (!hasStatusRequest) {
	                status = new DeviceModemState();
	            }
	            status.setModemSerialCode(response.getDevice().getSerialCode());
	            status.setRequestId(null);
	            status.setLastModemStatusCd(response.getModemState().getTargetStatus().getStatus());
	            status.setRequestedStatusCd(null);
	            status.setStatusUpdatedTs(new Date());
	            status.setStatusRequestedTs(null);
	            status.setErrorCode(null);
	            status.setErrorMessage(null);
	            if (hasStatusRequest) {
	                deviceModemStatusDao.updateDeviceModemStatus(status);
	            } else {
	                deviceModemStatusDao.insertDeviceModemStatus(status);
	            }
	        	}
	        	updateDeviceSettings(status);
        } catch (Exception e) {
        		if (response.getDevice() != null) {
        			throw new ModemServiceException("Could not updateDeviceState", e, response.getDevice().getProviderName());
        		} else {
        			throw new ModemServiceException("Could not updateDeviceState", e, null);
        		}
        }
    }

		private void updateDeviceSettings(DeviceModemState status)
				throws SQLException, DataLayerException, ConvertException {
			Long deviceId = deviceModemStatusDao.findDeviceIdBySerialCode(status.getModemSerialCode());
			if (deviceId != null) {
				deviceModemStatusDao.updateModemStatusInDeviceSettings(deviceId, status.getLastModemStatusCd(), "ModemStatusDbResultProcessor");
			} else {
				log.error("Could not find a device ID for serial code <%s>", status.getModemSerialCode());
			}
		}

		private DeviceModemState completeByRequestId(DeviceOperationResult response) throws SQLException, DataLayerException, ConvertException, ModemServiceException {
			String requestId = deviceModemStatusDao.findRequestIdByModemSerialCode(response.getDevice().getSerialCode());
			if (requestId == null || !requestId.equals(response.getRequestId())) {
					log.warn(String.format("Could not updateDeviceState because found requestId (%s) does not match the given requestId (%s)",
							requestId, response.getRequestId()));
					throw new ModemServiceException(String.format(
							"Could not updateDeviceState because found requestId (%s) does not match the given requestId (%s)",
							requestId, response.getRequestId()), null, null);
			}
			DeviceModemState status = deviceModemStatusDao.getDeviceModemStatus(response.getDevice().getSerialCode());
			if (status == null) {
				status = new DeviceModemState();
			}
			if (response.getModemState() != null) {
					status.setLastModemStatusCd(response.getModemState().getTargetStatus().getStatus());
			}
			status.setRequestId(response.getRequestId());
			status.setStatusUpdatedTs(new Date());
			status.setStatusRequestedTs(null);
			status.setErrorCode(response.getErrorCode());
			status.setErrorMessage(response.getErrorString());
			if (response.getErrorCode() == null) {
				status.setRequestedStatusCd(null);
			}
			status.setModemSerialCode(response.getDevice().getSerialCode());
			deviceModemStatusDao.updateDeviceModemStatusByRequestId(status);
			return status;
		}

    void updateStatusAwaitingState(DeviceOperationResult response) throws ModemServiceException {
    	  try {
    	      DeviceModemState status = deviceModemStatusDao.getDeviceModemStatus(response.getDevice().getSerialCode());
    	      status.setRequestId(response.getRequestId());
    	      deviceModemStatusDao.updateDeviceModemStatus(status);
    	  } catch (Exception e) {
    	  	  throw new ModemServiceException("Could not updateStatusAwaitingState", e, response.getDevice().getProviderName());
    	  }
    }

    void updateErrorState(DeviceOperationResult response) throws ModemServiceException {
        try {
        		DeviceModemState status = deviceModemStatusDao.getDeviceModemStatus(response.getDevice().getSerialCode());
            boolean hasStatusRequest = status != null;
            if (hasStatusRequest && (status.getRequestId() == null || status.getStatusRequestedTs() == null)) {
                // Drop obsolete results which already processed
                return;
            }
            if (!hasStatusRequest) {
                status = new DeviceModemState();
            }
            status.setModemSerialCode(response.getDevice().getSerialCode());
						status.setLastModemStatusCd(
								response.getModemState() == null ? (status.getLastModemStatusCd() == null ? ModemState.UNKNOWN.getStatus() : status.getLastModemStatusCd()) : response.getModemState().getStatus());
            status.setStatusUpdatedTs(new Date());
            status.setRequestId(null);
            if (response.getErrorString() == null) {
		            String[] errCodeMsg = response.getErrorMessage() == null ?
		                    new String[]{"NO_CODE:NO MESSAGE"} :
		                    response.getErrorMessage().split(":");
		            String code = errCodeMsg.length > 1 ? errCodeMsg[0] : "NO_CODE";
		            String msg = errCodeMsg.length > 1 ? errCodeMsg[1] : errCodeMsg[0];
		            status.setErrorCode("ERR-" + code);
		            status.setErrorMessage(msg);
            } else {
            	// The implementation for Verizon
            	status.setErrorCode(response.getErrorCode());
            	status.setErrorMessage(response.getErrorString());
            }
            if (hasStatusRequest) {
                deviceModemStatusDao.updateDeviceModemStatus(status);
            } else {
                deviceModemStatusDao.insertDeviceModemStatus(status);
            }
            updateDeviceSettings(status);
        } catch (Exception e) {
            throw new ModemServiceException("Could not updateErrorState", e, response.getDevice().getProviderName());
        }
    }
}
