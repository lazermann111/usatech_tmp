package simple.service.modem.service;

import simple.service.modem.service.exception.ModemServiceException;

/**
 * Results processor for ModemAPI callbacks.
 * In generic it process input service-specific result class and do some work with it.
 */
public interface ResultProcessor<T> {
    void process(T response) throws ModemServiceException;
}
