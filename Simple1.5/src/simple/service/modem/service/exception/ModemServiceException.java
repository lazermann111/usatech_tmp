package simple.service.modem.service.exception;

/**
 * Error occurred while processing modem API call.
 */
public class ModemServiceException extends Exception {
    private static final long serialVersionUID = 8810629131441233381L;

    private String apiProviderName;

    public ModemServiceException(String apiProviderName) {
        this.apiProviderName = apiProviderName;
    }

    public ModemServiceException(String message, String apiProviderName) {
        super(message);
        this.apiProviderName = apiProviderName;
    }

    public ModemServiceException(String message, Throwable cause, String apiProviderName) {
        super(message, cause);
        this.apiProviderName = apiProviderName;
    }

    public ModemServiceException(Throwable cause, String apiProviderName) {
        super(cause);
        this.apiProviderName = apiProviderName;
    }

    public ModemServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String apiProviderName) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.apiProviderName = apiProviderName;
    }
}
