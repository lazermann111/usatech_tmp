package simple.service.modem.service;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.service.dto.DeviceModemState;
import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.*;

/**
 * DAO IMPL for DEVICE.DEVICE_MODEM_STATUS table
 *
 * @author dlozenko
 */
public class DeviceModemStatusDaoImpl implements DeviceModemStatusDao {
    @Override @Nonnull
    public List<DeviceModemState> getLatestActivatedDevicesStatuses(boolean pending, boolean activated, boolean failed) throws SQLException, DataLayerException, ConvertException {
        List<DeviceModemState> rv = new ArrayList<>();
        Map<String, Object> params = new HashMap<>();
        params.put("include_pending", pending ? 1 : 0);
        params.put("include_activated", activated ? 1 : 0);
        params.put("include_failed", failed ? 1 : 0);
        Results result = DataLayerMgr.executeQuery("GET_DEVICE_MODEMS_STATUSES", params, false);
        while (result.next()) {
            DeviceModemState dev = new DeviceModemState();
            dev.setModemSerialCode(result.getValue("modem_serial_cd", String.class));
            dev.setRequestId(result.getValue("request_id", String.class));
            dev.setLastModemStatusCd(result.getValue("last_modem_status_cd", String.class));
            dev.setRequestedStatusCd(result.getValue("requested_status_cd", String.class));
            dev.setStatusUpdatedTs(result.getValue("status_updated_ts", Date.class));
            dev.setStatusRequestedTs(result.getValue("status_requested_ts", Date.class));
            dev.setErrorCode(result.getValue("error_code", String.class));
            dev.setErrorMessage(result.getValue("error_message", String.class));
            dev.setProvider(ModemApiProvider.fromString(result.getValue("provider", String.class)));
            dev.setCreatedTs(result.getValue("created_ts", Date.class));
            rv.add(dev);
        }
        return rv;
    }

    @Override
    public DeviceModemState getDeviceModemStatus(String modemSerialCode) throws SQLException, DataLayerException, ConvertException {
        Results result = DataLayerMgr.executeQuery("GET_DEVICE_MODEM_STATUS", new Object[]{modemSerialCode}, false);
        if (result.next()) {
            DeviceModemState rv = new DeviceModemState();
            rv.setModemSerialCode(result.getValue("modem_serial_cd", String.class));
            rv.setRequestId(result.getValue("request_id", String.class));
            rv.setLastModemStatusCd(result.getValue("last_modem_status_cd", String.class));
            rv.setRequestedStatusCd(result.getValue("requested_status_cd", String.class));
            rv.setStatusUpdatedTs(result.getValue("status_updated_ts", Date.class));
            rv.setStatusRequestedTs(result.getValue("status_requested_ts", Date.class));
            rv.setErrorCode(result.getValue("error_code", String.class));
            rv.setErrorMessage(result.getValue("error_message", String.class));
            rv.setProvider(ModemApiProvider.fromString(result.getValue("provider", String.class)));
            rv.setCreatedTs(result.getValue("created_ts", Date.class));
            return rv;
        }
        return null;
    }

    @Override
    public void updateDeviceModemStatus(DeviceModemState state) throws SQLException, DataLayerException {
        Map<String,Object> params = new HashMap<>();
        params.put("last_modem_status_cd", state.getLastModemStatusCd());
        params.put("status_updated_ts", state.getStatusUpdatedTs());
        params.put("requested_status_cd", state.getRequestedStatusCd());
        params.put("status_requested_ts", state.getStatusRequestedTs());
        params.put("request_id", state.getRequestId());
        params.put("error_code", state.getErrorCode());
        params.put("error_message", state.getErrorMessage());
        params.put("provider", state.getProvider() == null ? null : state.getProvider().name().toLowerCase());
        params.put("modem_serial_cd", state.getModemSerialCode());
        DataLayerMgr.executeUpdate("UPDATE_DEVICE_MODEM_STATUS", params, true);
    }

    @Override
    public void updateDeviceModemStatusByRequestId(DeviceModemState state) throws SQLException, DataLayerException {
        Map<String,Object> params = new HashMap<>();
        params.put("modem_serial_cd", state.getModemSerialCode());
        params.put("last_modem_status_cd", state.getLastModemStatusCd());
        params.put("status_updated_ts", state.getStatusUpdatedTs());
        params.put("requested_status_cd", state.getRequestedStatusCd());
        params.put("status_requested_ts", state.getStatusRequestedTs());
        params.put("request_id_to_set", state.getRequestId());
        params.put("error_code", state.getErrorCode());
        params.put("error_message", state.getErrorMessage());
        params.put("request_id", state.getRequestId());
        DataLayerMgr.executeUpdate("UPDATE_DEVICE_MODEM_STATUS_BY_REQUEST_ID", params, true);
    }

    @Override
    public void insertDeviceModemStatus(DeviceModemState state) throws SQLException, DataLayerException {
        Map<String,Object> params = new HashMap<>();
        params.put("modem_serial_cd", state.getModemSerialCode());
        params.put("last_modem_status_cd", state.getLastModemStatusCd());
        params.put("status_updated_ts", state.getStatusUpdatedTs());
        params.put("requested_status_cd", state.getRequestedStatusCd());
        params.put("status_requested_ts", state.getStatusRequestedTs());
        params.put("request_id", state.getRequestId());
        params.put("error_code", state.getErrorCode());
        params.put("error_message", state.getErrorMessage());
        params.put("provider", state.getProvider() == null ? null : state.getProvider().name().toLowerCase());
        DataLayerMgr.executeUpdate("INSERT_DEVICE_MODEM_STATUS", params, true);
    }

    @Override
    public Long findDeviceIdBySerialCode(String serialCode) throws SQLException, DataLayerException, ConvertException {
        // for testing, please not commit
        //if (1 == 2 - 1) {
        //    return 19452L;
        //}
        Results result = DataLayerMgr.executeQuery("GET_DEVICE_ID_BY_MODEM_ICCID", new Object[]{serialCode}, false);
        if (result.next()) {
            return result.getValue("device_id", long.class);
        }
        return null;
    }

    @Override
    public String findRequestIdByModemSerialCode(String modemSerialCode) throws SQLException, DataLayerException, ConvertException {
        Results result = DataLayerMgr.executeQuery("GET_REQUEST_ID_BY_DEVICE_ID", new Object[]{modemSerialCode}, false);
        if (result.next()) {
            return result.getValue("request_id", String.class);
        }
        return null;
    }

    @Override
    public String findModemSerialCodeByDeviceId(long deviceId) throws SQLException, DataLayerException, ConvertException {
        Results result = DataLayerMgr.executeQuery("GET_MODEM_ICCID_BY_DEVICE_ID", new Object[]{deviceId}, false);
        if (result.next()) {
            return result.getValue("modem_iccid", String.class);
        }
        return null;
    }

    @Override
    public void updateModemStatusInDeviceSettings(long deviceId, String status, String changedBy) throws SQLException, DataLayerException {
        Map<String, Object> params = new HashMap<>();
        params.put("deviceId", deviceId);
        params.put("deviceSettingParameterCd", "DEVICE_MODEM_STATUS");
        params.put("deviceSettingValue",
                    status != null && "ADSN".contains(status)
                    ? status :
                    null);
        params.put("fileOrder", null);
        params.put("changedBy", changedBy);
        DataLayerMgr.executeCall("UPSERT_DEVICE_SETTING", params, true);
    }
}
