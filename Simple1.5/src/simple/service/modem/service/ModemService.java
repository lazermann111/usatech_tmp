package simple.service.modem.service;

import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.service.dto.DeviceOperationResult;
import simple.service.modem.service.dto.USATDevice;
import simple.service.modem.service.exception.ModemServiceException;
import javax.annotation.Nonnull;

/**
 * Interface to work with external modem API
 */
public interface ModemService {
    @Nonnull
    DeviceOperationResult[] activate(@Nonnull USATDevice[] devices) throws ModemServiceException;

    @Nonnull
    DeviceOperationResult[] deactivate(@Nonnull USATDevice[] devices) throws ModemServiceException;

    @Nonnull
    DeviceOperationResult[] suspend(@Nonnull USATDevice[] devices) throws ModemServiceException;

    @Nonnull
    DeviceOperationResult[] resume(@Nonnull USATDevice[] devices) throws ModemServiceException;

    @Nonnull
    DeviceOperationResult[] getCurrentState(@Nonnull USATDevice[] devices) throws ModemServiceException;

    boolean isSuitableDevice(@Nonnull USATDevice device);

    ModemApiProvider getProviderType();
}
