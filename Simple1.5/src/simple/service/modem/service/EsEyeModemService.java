package simple.service.modem.service;

import simple.service.modem.dao.eseye.EsEyeModemApi;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivateResult;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivateSimStatus;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivatedSim;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivationStatus;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import simple.service.modem.dao.eseye.dto.error.EsEyeErrorCode;
import simple.service.modem.dao.eseye.dto.error.EsEyeErrorStatus;
import simple.service.modem.dao.eseye.dto.error.EsEyeSimError;
import simple.service.modem.dao.eseye.dto.getsims.*;
import simple.service.modem.dao.eseye.dto.login.EsEyeLoginResult;
import simple.service.modem.dao.eseye.dto.suspend.EsEyeSuspendResult;
import simple.service.modem.dao.eseye.dto.terminate.EsEyeTerminateResult;
import simple.service.modem.dao.eseye.dto.unsuspend.EsEyeUnSuspendResult;
import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.dao.generic.exception.UnavailableException;
import simple.service.modem.service.dto.DeviceOperationResult;
import simple.service.modem.service.dto.USATDevice;
import simple.service.modem.service.dto.ModemState;
import simple.service.modem.service.dto.OperationStatus;
import simple.service.modem.service.exception.ModemServiceException;
import javax.annotation.Nonnull;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implementation of EsEye modem service
 *
 * @author dlozenko
 */
public class EsEyeModemService implements ModemService {
    private final String username;
    private final String password;
    private final String portfolioId;
    private volatile EsEyeModemApi dao;
    private volatile String cookies;
    private final Lock loginLock = new ReentrantLock();

    public EsEyeModemService(@Nonnull String username, @Nonnull String password, @Nonnull String portfolioId) {
        this.username = username;
        this.password = password;
        this.portfolioId = portfolioId;
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] activate(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            EsEyeActivatedSim[] sims = new EsEyeActivatedSim[devices.length];
            int i = 0;
            for (USATDevice device : devices) {
                sims[i] = new EsEyeActivatedSim();
                sims[i].setFriendlyName(device.getName());
                sims[i].setIccid(device.getIccid());
                sims[i].setImei(device.getImei());
                sims[i].setTariffId(device.getServicePlan());
                i++;
            }
            EsEyeActivateResult res = dao.activateSims(sims, cookies);
            if (!isApiCallStatusSuccess(res.getStatus(), res.getSimErrors())) {
                throwWithFailedState(res.getStatus(), "activateSims");
            }
            int resultsLen = (res.getSimStatuses() == null ? 0 : res.getSimStatuses().length) +
                             (res.getSimErrors() == null ? 0 : res.getSimErrors().length);
            DeviceOperationResult[] rv = new DeviceOperationResult[resultsLen];
            i = 0;
            if (res.getSimStatuses() != null) {
                for (EsEyeActivateSimStatus status : res.getSimStatuses()) {
                    rv[i] = new DeviceOperationResult();
                    rv[i].setOperationStatus(OperationStatus.COMPLETED);
                    rv[i].setResult(status.getIccid());
                    USATDevice dev = new USATDevice();
                    dev.setIccid(status.getIccid());
                    dev.setSerialCode(status.getIccid());
                    rv[i].setDevice(dev);
                    rv[i].setModemState(status.getStatus() == EsEyeActivationStatus.OK_AUTO ? ModemState.ACTIVE : ModemState.PENDING_ACTIVATION);
                    i++;
                }
            }
            if (hasFailedDevices(res.getSimErrors())) {
                for (EsEyeSimError esEyeSimError : res.getSimErrors()) {
                    rv[i] = new DeviceOperationResult();
                    rv[i].setOperationStatus(OperationStatus.ERROR);
                    rv[i].setResult(esEyeSimError.getErrorMessage());
                    USATDevice dev = new USATDevice();
                    dev.setIccid(esEyeSimError.getIccid());
                    dev.setSerialCode(esEyeSimError.getIccid());
                    rv[i].setDevice(dev);
                    rv[i].setModemState(ModemState.UNKNOWN);
                    i++;
                }
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        }
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] deactivate(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            String[] sims = new String[devices.length];
            int i = 0;
            for (USATDevice device : devices) {
                sims[i] = device.getIccid();
                i++;
            }
            EsEyeTerminateResult res = dao.terminateSims(sims, cookies);
            if (!isApiCallStatusSuccess(res.getStatus(), res.getSimErrors())) {
                throwWithFailedState(res.getStatus(), "terminateSims");
            }
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            i = 0;
            for (String sim : sims) {
                if (isErrorSim(res.getSimErrors(), sim)) {
                    continue;
                }
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.COMPLETED);
                rv[i].setResult(sim);
                USATDevice dev = new USATDevice();
                dev.setIccid(sim);
                dev.setSerialCode(sim);
                rv[i].setDevice(dev);
                rv[i].setModemState(ModemState.DEACTIVATED);
                i++;
            }
            if (hasFailedDevices(res.getSimErrors())) {
                for (EsEyeSimError esEyeSimError : res.getSimErrors()) {
                    rv[i] = new DeviceOperationResult();
                    rv[i].setOperationStatus(OperationStatus.ERROR);
                    rv[i].setResult(esEyeSimError.getErrorMessage());
                    USATDevice dev = new USATDevice();
                    dev.setIccid(esEyeSimError.getIccid());
                    dev.setSerialCode(esEyeSimError.getIccid());
                    rv[i].setDevice(dev);
                    rv[i].setModemState(ModemState.UNKNOWN);
                    i++;
                }
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        }
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] suspend(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            String[] sims = new String[devices.length];
            int i = 0;
            for (USATDevice device : devices) {
                sims[i] = device.getIccid();
                i++;
            }
            EsEyeSuspendResult res = dao.suspendSims(sims, cookies);
            if (!isApiCallStatusSuccess(res.getStatus(), res.getSimErrors())) {
                throwWithFailedState(res.getStatus(), "suspendSims");
            }
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            i = 0;
            for (String sim : sims) {
                if (isErrorSim(res.getSimErrors(), sim)) {
                    continue;
                }
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.COMPLETED);
                rv[i].setResult(sim);
                USATDevice dev = new USATDevice();
                dev.setIccid(sim);
                dev.setSerialCode(sim);
                rv[i].setDevice(dev);
                rv[i].setModemState(ModemState.SUSPENDED);
                i++;
            }
            if (hasFailedDevices(res.getSimErrors())) {
                for (EsEyeSimError esEyeSimError : res.getSimErrors()) {
                    rv[i] = new DeviceOperationResult();
                    rv[i].setOperationStatus(OperationStatus.ERROR);
                    rv[i].setResult(esEyeSimError.getErrorMessage());
                    USATDevice dev = new USATDevice();
                    dev.setIccid(esEyeSimError.getIccid());
                    dev.setSerialCode(esEyeSimError.getIccid());
                    rv[i].setDevice(dev);
                    rv[i].setModemState(ModemState.UNKNOWN);
                    i++;
                }
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        }
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] resume(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            String[] sims = new String[devices.length];
            int i = 0;
            for (USATDevice device : devices) {
                sims[i] = device.getIccid();
                i++;
            }
            EsEyeUnSuspendResult res = dao.unSuspendSims(sims, cookies);
            if (!isApiCallStatusSuccess(res.getStatus(), res.getSimErrors())) {
                throwWithFailedState(res.getStatus(), "unSuspendSims");
            }
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            i = 0;
            for (String sim : sims) {
                if (isErrorSim(res.getSimErrors(), sim)) {
                    continue;
                }
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.COMPLETED);
                rv[i].setResult(sim);
                USATDevice dev = new USATDevice();
                dev.setIccid(sim);
                dev.setSerialCode(sim);
                rv[i].setDevice(dev);
                rv[i].setModemState(ModemState.ACTIVE);
                i++;
            }
            if (hasFailedDevices(res.getSimErrors())) {
                for (EsEyeSimError esEyeSimError : res.getSimErrors()) {
                    rv[i] = new DeviceOperationResult();
                    rv[i].setOperationStatus(OperationStatus.ERROR);
                    rv[i].setResult(esEyeSimError.getErrorMessage());
                    USATDevice dev = new USATDevice();
                    dev.setIccid(esEyeSimError.getIccid());
                    dev.setSerialCode(esEyeSimError.getIccid());
                    rv[i].setDevice(dev);
                    rv[i].setModemState(ModemState.UNKNOWN);
                    i++;
                }
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        }
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] getCurrentState(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            int i = 0;
            for (USATDevice dev : devices) {
                EsEyeSearchCriteria searchCriteria = new EsEyeSearchCriteria();
                searchCriteria.setTariffId(dev.getServicePlan());
                searchCriteria.setMatchString(dev.getIccid());
                searchCriteria.setMatchFields(EsEyeModemIdFields.ICCID);
                searchCriteria.setMatchType(EsEyeStrMatchType.MATCHES);
                EsEyeGetSimsResult res = dao.getSims(searchCriteria, EsEyeModemIdFields.ICCID, null, null, cookies);
                if (!isApiCallStatusSuccess(res.getStatus(), null)) {
                    throwWithFailedState(res.getStatus(), "getSims");
                }
                EsEyeSim[] sims = res.getSims();
                if (sims.length != 1) {
                    throw new ModemServiceException("Too many devices returned. Requested: 1" +
                            ", returned: " + sims.length);
                }
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.COMPLETED);
                rv[i].setResult(sims[0].getIccid());
                USATDevice d = new USATDevice();
                d.setIccid(sims[0].getIccid());
                d.setSerialCode(sims[0].getIccid());
                d.setName(sims[0].getFriendlyName());
                rv[i].setDevice(d);
                rv[i].setModemState(fromEsEyeState(sims[0].getStatus()));
                i++;
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        }
    }

    @Override
    public boolean isSuitableDevice(@Nonnull USATDevice device) {
        return device.getProviderName() != null && device.getProviderName().toLowerCase().contains("eseye");
    }

    public EsEyeModemApi getDao() {
        return dao;
    }

    public void setDao(EsEyeModemApi api) {
        this.dao = api;
    }

    private void loginIfNeed() throws UnavailableException, ModemServiceException {
        loginLock.lock();
        try {
            if (cookies == null) {
                EsEyeLoginResult res = dao.login(username, password, portfolioId);
                if (!isApiCallStatusSuccess(res.getStatus(), null)) {
                    throwWithFailedState(res.getStatus(), "login");
                }
                cookies = res.getCookie();
            }
        } finally {
            loginLock.unlock();
        }
    }

    void throwWithFailedState(EsEyeApiCallStatus status, String callName) throws ModemServiceException {
        if (status.getErrorCode() == EsEyeErrorCode.UNKNOWN_USER_PASSWORD_1 ||
            status.getErrorCode() == EsEyeErrorCode.UNKNOWN_USER_PASSWORD_2 ||
            status.getErrorCode() == EsEyeErrorCode.INVALID_PASSWORD ||
            status.getErrorCode() == EsEyeErrorCode.PASSWORDS_DONT_MATCH ||
            status.getErrorCode() == EsEyeErrorCode.PERMISSION_DENIED) {
            cookies = null;
        }
        throw new ModemServiceException("API Call '" + callName + "' failed: " + buildErrorMessage(status) +
                ", RAW ERROR: " + status.getErrorMessage(), getApiProviderName());
    }

    boolean isApiCallStatusSuccess(EsEyeApiCallStatus status, EsEyeSimError[] simErrors) {
        return status == null ||
               status.getStatus() == EsEyeErrorStatus.SUCCESS ||
                // I know this is bad, but EsEye API may mark call as failed even call itself is ok, but has failed devices in batch.
                // They should not mark whole call as failed when some devices has failed state. As workaround we will
                // force such calls to become success to continue normal processing for other devices in batch
               hasFailedDevices(simErrors);
    }

    boolean hasFailedDevices(EsEyeSimError[] simErrors) {
        return simErrors != null && simErrors.length > 0;
    }

    boolean isErrorSim(EsEyeSimError[] errorSims, String testedSim) {
        if (errorSims == null) {
            return false;
        }
        for (EsEyeSimError errorSim : errorSims) {
            if (errorSim.getIccid() != null && errorSim.getIccid().equalsIgnoreCase(testedSim)) {
                return true;
            }
        }
        return false;
    }

    private String buildErrorMessage(EsEyeApiCallStatus status) {
        if (status.getErrorCode() == null || status.getErrorCode().getErrorCode() == null) {
            if (status.getErrorMessage() == null) {
                return "NO_CODE:No error code or message provided by EsEye";
            } else {
                return "NO_CODE:" + status.getErrorMessage();
            }
        }
        return status.getErrorCode().getErrorCode() + ":" + status.getErrorCode().getErrorDescription();
    }

    private String getApiProviderName() {
        return dao.getClass().getName();
    }

    private ModemState fromEsEyeState(EsEyeSimState esEyeSimState) {
        if (esEyeSimState == null) {
            return ModemState.UNKNOWN;
        }
        switch (esEyeSimState) {
            case AVAILABLE:
                return ModemState.PENDING_ACTIVATION;
            case PROVISIONED:
                return ModemState.ACTIVE;
            case SUSPENDED:
                return ModemState.SUSPENDED;
            case TERMINATED:
            case UNPROVISIONED:
            case PENDING_DEPROVISION:
                return ModemState.DEACTIVATED;
        }
        return ModemState.UNKNOWN;
    }

    @Override
    public ModemApiProvider getProviderType() {
        return ModemApiProvider.ESEYE;
    }
}
