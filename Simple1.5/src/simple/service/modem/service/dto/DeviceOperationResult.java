package simple.service.modem.service.dto;

import java.io.Serializable;

/**
 * Result of modem command execution
 */
public class DeviceOperationResult implements Serializable {
    private static final long serialVersionUID = 1260198434211300899L;

    private OperationStatus operationStatus;
    private String result;
    private String requestId;
    private USATDevice device;
    private ModemState modemState;
    private String errorCode;
    private String errorString;

    public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

		public String getErrorString() {
			return errorString;
		}

		public void setErrorString(String errorString) {
			this.errorString = errorString;
		}

		public DeviceOperationResult() {
    }

    public USATDevice getDevice() {
        return device;
    }

    public void setDevice(USATDevice device) {
        this.device = device;
    }

    public ModemState getModemState() {
        return modemState;
    }

    public void setModemState(ModemState modemState) {
        this.modemState = modemState;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(OperationStatus status) {
        this.operationStatus = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return operationStatus == OperationStatus.COMPLETED || operationStatus == OperationStatus.POSTPONED;
    }

    public String getScheduledRequestId() {
        if (operationStatus == OperationStatus.POSTPONED) {
            return result;
        }
        return null;
    }

    public String getErrorMessage() {
        if (operationStatus == OperationStatus.ERROR) {
            return result;
        }
        return null;
    }

		public String getRequestId() {
			return requestId;
		}

		public void setRequestId(String requestId) {
			this.requestId = requestId;
		}
}
