package simple.service.modem.service.dto;

/**
 * Call status of modem API
 */
public enum OperationStatus {
    /**
     * Call completed, see result for details
     */
    COMPLETED,

    /**
     * Call accepted by remote server, but result will ready a bit later.
     * Listen results queue for getting your result. See request id in result.
     */
    POSTPONED,

    /**
     * Error occurred while executing this call. See error message in result.
     */
    ERROR
}
