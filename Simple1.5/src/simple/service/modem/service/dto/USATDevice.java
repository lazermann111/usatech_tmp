package simple.service.modem.service.dto;

import java.io.Serializable;

/**
 * Device that is prepared to modem activation.
 */
public class USATDevice implements Serializable {
    private static final long serialVersionUID = -8564746511854095804L;

    private Long deviceId;
    private String name;
    private String iccid;
    private String imei;
    private String esn;
    private String meid;
    private String servicePlan;
    private String zipCode;
    private String accountName;
    private String providerName;
    private String ownerEmail;
    private String currentStatus;
    private String requestedStatus;
    private String mdn;
    private boolean withSim;
    private String serialCode;

    public USATDevice() {
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getRequestedStatus() {
        return requestedStatus;
    }

    public void setRequestedStatus(String requestedStatus) {
        this.requestedStatus = requestedStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getEsn() {
        return esn;
    }

    public void setEsn(String esn) {
        this.esn = esn;
    }

    public String getMeid() {
        return meid;
    }

    public void setMeid(String meid) {
        this.meid = meid;
    }

    public String getServicePlan() {
        return servicePlan;
    }

    public void setServicePlan(String servicePlan) {
        this.servicePlan = servicePlan;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getMdn() {
        return mdn;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

	public boolean hasIccIdAndProvider() {
        return iccid != null && providerName != null && iccid.trim().length() > 0 && providerName.trim().length() > 0;
    }

		public boolean hasSerialNumberAndProvider() {
			return hasSerialCode() || hasProvider();
		}
	
		private boolean hasProvider() {
			return providerName != null && providerName.trim().length() > 0;
		}
	
		private boolean hasSerialCode() {
			return (hasIccid() || hasMeid()) && serialCode != null && serialCode.trim().length() > 0;
		}
	
		private boolean hasMeid() {
			return meid != null && meid.trim().length() > 0;
		}
	
		private boolean hasIccid() {
			return iccid != null && iccid.trim().length() > 0;
		}
	
		public boolean isWithSim() {
			return withSim;
		}
	
		public void setWithSim(boolean withSim) {
			this.withSim = withSim;
		}
	
		public String getSerialCode() {
			return serialCode;
		}
	
		public void setSerialCode(String serialCode) {
			this.serialCode = serialCode;
		}
	
		@Override
    public String toString() {
        return "USATDevice{" +
                "deviceId='" + deviceId + '\'' +
                ", name='" + name + '\'' +
                ", currentStatus='" + currentStatus + '\'' +
                ", requestedStatus='" + requestedStatus + '\'' +
                ", iccid='" + iccid + '\'' +
                ", imei='" + imei + '\'' +
                ", esn='" + esn + '\'' +
                ", meid='" + meid + '\'' +
                ", servicePlan='" + servicePlan + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", accountName='" + accountName + '\'' +
                ", providerName='" + providerName + '\'' +
                ", ownerEmail='" + ownerEmail + '\'' +
                ", mdn='" + mdn + '\'' +
                ", serialCode='" + serialCode + '\'' +
                '}';
    }

}
