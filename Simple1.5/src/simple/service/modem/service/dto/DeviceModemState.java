package simple.service.modem.service.dto;

import simple.service.modem.dao.generic.dto.ModemApiProvider;
import java.io.Serializable;
import java.util.Date;

/**
 * DTO to map USAPDB table DEVICE.DEVICE_MODEM_STATUS
 *
 * @author dlozenko
 */
public class DeviceModemState implements Serializable{
    private static final long serialVersionUID = 2709140031273353147L;

    private String modemSerialCode;
    private String lastModemStatusCd;
    private Date statusUpdatedTs;
    private String requestedStatusCd;
    private Date statusRequestedTs;
    private String requestId;
    private String errorCode;
    private String errorMessage;
    private ModemApiProvider provider;
    private Date createdTs;

    public DeviceModemState() {
    }

    public String getModemSerialCode() {
        return modemSerialCode;
    }

    public void setModemSerialCode(String modemSerialCode) {
        this.modemSerialCode = modemSerialCode;
    }

    public String getLastModemStatusCd() {
        return lastModemStatusCd;
    }

    public void setLastModemStatusCd(String lastModemStatusCd) {
        this.lastModemStatusCd = lastModemStatusCd;
    }

    public Date getStatusUpdatedTs() {
        return statusUpdatedTs;
    }

    public void setStatusUpdatedTs(Date statusUpdatedTs) {
        this.statusUpdatedTs = statusUpdatedTs;
    }

    public String getRequestedStatusCd() {
        return requestedStatusCd;
    }

    public void setRequestedStatusCd(String requestedStatusCd) {
        this.requestedStatusCd = requestedStatusCd;
    }

    public Date getStatusRequestedTs() {
        return statusRequestedTs;
    }

    public void setStatusRequestedTs(Date statusRequestedTs) {
        this.statusRequestedTs = statusRequestedTs;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ModemApiProvider getProvider() {
        return provider;
    }

    public void setProvider(ModemApiProvider provider) {
        this.provider = provider;
    }

    public Date getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(Date createdTs) {
        this.createdTs = createdTs;
    }
}
