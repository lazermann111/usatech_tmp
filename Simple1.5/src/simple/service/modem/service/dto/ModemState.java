package simple.service.modem.service.dto;

/**
 * Verizon device kind
 *
 * @author dlozenko
 */
public enum ModemState {
    PENDING_ACTIVATION("N"),
    ACTIVE("A"),
    PENDING_SUSPEND("s"),
    SUSPENDED("S"),
    PENDING_RESUME("a"),
    PENDING_DEACTIVATION("d"),
    DEACTIVATED("D"),
    UNKNOWN("U");

    private String status;

    ModemState(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public ModemState getTargetStatus() {
        return fromStringStatus(status);
    }

    public static ModemState fromStringStatus(String statusStr) {
        if (statusStr == null) {
            return null;
        }
        if (statusStr.toLowerCase().equals(statusStr)) {
            // Pending status map to target status because we don't have them in our DB
            // I.e. Pending deactivation will mapped to DEACTIVATED
            return fromStringStatus(statusStr.toUpperCase());
        }
        ModemState[] values = ModemState.values();
        for (ModemState value : values) {
            if (value.getStatus().equals(statusStr)) {
                return value;
            }
        }
        return null;
    }
}
