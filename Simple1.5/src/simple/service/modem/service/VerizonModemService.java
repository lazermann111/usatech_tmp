package simple.service.modem.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.Nonnull;

import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.dao.generic.exception.UnavailableException;
import simple.service.modem.dao.verizon.VerizonModemApi;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceId;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceKind;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorCode;
import simple.service.modem.dao.verizon.dto.list.VerizonDevice;
import simple.service.modem.dao.verizon.dto.list.VerizonDeviceState;
import simple.service.modem.dao.verizon.dto.list.VerizonListResult;
import simple.service.modem.dao.verizon.dto.login.VerizonToken;
import simple.service.modem.dao.verizon.exception.VerizonApiException;
import simple.service.modem.service.dto.DeviceOperationResult;
import simple.service.modem.service.dto.ModemState;
import simple.service.modem.service.dto.OperationStatus;
import simple.service.modem.service.dto.USATDevice;
import simple.service.modem.service.exception.ModemServiceException;

/**
 * Implementation of Verizon modem service
 */
public class VerizonModemService implements ModemService {
    private final String username;
    private final String password;
    private final String appKey;
    private final String appSecret;
    private final String billingAccountName;
    private final String callbackUrl;
    private final String verizonCDMAIPPool;
    private final String verizonLTEIPPool;
    private volatile VerizonModemApi dao;
    private volatile VerizonToken token;
    private final Lock loginLock = new ReentrantLock();

    public VerizonModemService(String username, String password, String appKey, String appSecret, String billingAccountName, String callbackUrl, String verizonCDMAIPPool, String verizonLTEIPPool) {
        this.username = username;
        this.password = password;
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.billingAccountName = billingAccountName;
        this.callbackUrl = callbackUrl;
				this.verizonCDMAIPPool = verizonCDMAIPPool;
				this.verizonLTEIPPool = verizonLTEIPPool;
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] activate(@Nonnull USATDevice[] devices) throws ModemServiceException {
    		// Group
      	Map<String, List<USATDevice>> devicesGroupedByZips = new HashMap<>();
      	for (USATDevice device : devices) {
      			String zip = device.getZipCode();
      			List<USATDevice> devicesWithTheSameZip = devicesGroupedByZips.get(zip);
      			if (devicesWithTheSameZip == null) {
      					devicesWithTheSameZip = new ArrayList<>();
      					devicesGroupedByZips.put(zip, devicesWithTheSameZip);
      			}
      			devicesWithTheSameZip.add(device);
      	}
      	
      	// Activate devices and make output
      	List<DeviceOperationResult> results = new ArrayList<>();
      	for (String zip : devicesGroupedByZips.keySet()) {
      			List<USATDevice> list = devicesGroupedByZips.get(zip);
      			int i = 0;
      			USATDevice[] devicesToActivate = new USATDevice[list.size()];
      			for (USATDevice device : list) {
      				devicesToActivate[i++] = device;
      			}
      			DeviceOperationResult[] result = activateWithOneZip(devicesToActivate);
      			for (DeviceOperationResult r : result) {
      					results.add(r);
      			}
      	}
      	DeviceOperationResult[] ret = new DeviceOperationResult[results.size()];
      	int i = 0;
      	for (DeviceOperationResult result : results) {
      		ret[i++] = result;
      	}
      	return ret;
    }
    
    @Nonnull
    private DeviceOperationResult[] activateWithOneZip(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            VerizonDevice[] sims = new VerizonDevice[devices.length];
            int i;
            for (i = 0; i < devices.length; i++) {
                sims[i] = new VerizonDevice();
                if (devices[i].isWithSim()) {
                  sims[i].setDeviceIds(new VerizonDeviceId[2]);
                	VerizonDeviceId deviceId = new VerizonDeviceId();
                  deviceId.setKind(VerizonDeviceKind.ICCID);
                  deviceId.setId(devices[i].getIccid());
                  sims[i].getDeviceIds()[0] = deviceId;
                	deviceId = new VerizonDeviceId();
                  deviceId.setKind(VerizonDeviceKind.IMEI);
                  deviceId.setId(devices[i].getImei());
                  sims[i].getDeviceIds()[1] = deviceId;
                } else {
                  sims[i].setDeviceIds(new VerizonDeviceId[1]);
                	VerizonDeviceId deviceId = new VerizonDeviceId();
                  deviceId.setKind(VerizonDeviceKind.MEID);
                  deviceId.setId(devices[i].getMeid());
                  sims[i].getDeviceIds()[0] = deviceId;
                }
            }
            String pool = devices[0].isWithSim() ? verizonLTEIPPool : verizonCDMAIPPool;
            String res = dao.activate(sims, billingAccountName, devices[0].getServicePlan(), devices[0].getZipCode(), pool, token);
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            for (i = 0; i < devices.length; i++) {
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.POSTPONED);
                rv[i].setRequestId(res);
                rv[i].setModemState(ModemState.PENDING_ACTIVATION);
                rv[i].setDevice(devices[i]);
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        } catch (VerizonApiException e) {
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            for (int i = 0; i < devices.length; i++) {
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.ERROR);
                rv[i].setResult(e.getMessage());
                rv[i].setDevice(devices[i]);
                if (e.getErrorResponse() != null) {
                	rv[i].setErrorCode(e.getErrorResponse().getErrorCode());
                	rv[i].setErrorString(e.getErrorResponse().getErrorMessage());
                }
            }
            return rv;
        }
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] deactivate(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            VerizonDeviceId[] sims = new VerizonDeviceId[devices.length];
            int i;
            for (i = 0; i < devices.length; i++) {
                sims[i] = new VerizonDeviceId();
                if (devices[i].getIccid() != null) {
                    sims[i].setKind(VerizonDeviceKind.ICCID);
                    sims[i].setId(devices[i].getIccid());
                } else if (devices[i].getMeid() != null) {
                    sims[i].setKind(VerizonDeviceKind.MEID);
                    sims[i].setId(devices[i].getMeid());
                } else if (devices[i].getImei() != null) {
                    sims[i].setKind(VerizonDeviceKind.IMEI);
                    sims[i].setId(devices[i].getImei());
                } else {
                    throw new IllegalArgumentException("Device doesn't have ICCID, MEID or IMEI");
                }
            }
            String res = dao.deactivate(sims, null, null, token);
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            for (i = 0; i < devices.length; i++) {
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.POSTPONED);
                rv[i].setRequestId(res);
                rv[i].setModemState(ModemState.PENDING_DEACTIVATION);
                rv[i].setDevice(devices[i]);
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        } catch (VerizonApiException e) {
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            for (int i = 0; i < devices.length; i++) {
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.ERROR);
                rv[i].setResult(e.getMessage());
                rv[i].setDevice(devices[i]);
                if (e.getErrorResponse() != null) {
                	rv[i].setErrorCode(e.getErrorResponse().getErrorCode());
                	rv[i].setErrorString(e.getErrorResponse().getErrorMessage());
                }
            }
            return rv;
        }
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] suspend(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            VerizonDeviceId[] sims = new VerizonDeviceId[devices.length];
            int i;
            for (i = 0; i < devices.length; i++) {
                sims[i] = new VerizonDeviceId();
                if (devices[i].getIccid() != null) {
                    sims[i].setKind(VerizonDeviceKind.ICCID);
                    sims[i].setId(devices[i].getIccid());
                } else if (devices[i].getMeid() != null) {
                    sims[i].setKind(VerizonDeviceKind.MEID);
                    sims[i].setId(devices[i].getMeid());
                } else if (devices[i].getImei() != null) {
                    sims[i].setKind(VerizonDeviceKind.IMEI);
                    sims[i].setId(devices[i].getImei());
                } else {
                    throw new IllegalArgumentException("Device doesn't have ICCID, MEID or IMEI");
                }
            }
            String res = dao.suspend(sims, null, null, token);
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            for (i = 0; i < devices.length; i++) {
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.POSTPONED);
                rv[i].setRequestId(res);
                rv[i].setModemState(ModemState.PENDING_SUSPEND);
                rv[i].setDevice(devices[i]);
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        } catch (VerizonApiException e) {
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            for (int i = 0; i < devices.length; i++) {
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.ERROR);
                rv[i].setResult(e.getMessage());
                rv[i].setDevice(devices[i]);
                if (e.getErrorResponse() != null) {
                	rv[i].setErrorCode(e.getErrorResponse().getErrorCode());
                	rv[i].setErrorString(e.getErrorResponse().getErrorMessage());
                }
            }
            return rv;
        }
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] resume(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            VerizonDeviceId[] sims = new VerizonDeviceId[devices.length];
            int i;
            for (i = 0; i < devices.length; i++) {
                sims[i] = new VerizonDeviceId();
                if (devices[i].getIccid() != null) {
                    sims[i].setKind(VerizonDeviceKind.ICCID);
                    sims[i].setId(devices[i].getIccid());
                } else if (devices[i].getMeid() != null) {
                    sims[i].setKind(VerizonDeviceKind.MEID);
                    sims[i].setId(devices[i].getMeid());
                } else if (devices[i].getImei() != null) {
                    sims[i].setKind(VerizonDeviceKind.IMEI);
                    sims[i].setId(devices[i].getImei());
                } else {
                    throw new IllegalArgumentException("Device doesn't have ICCID, MEID or IMEI");
                }
            }
            String res = dao.restore(sims, null, null, token);
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            for (i = 0; i < devices.length; i++) {
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.POSTPONED);
                rv[i].setRequestId(res);
                rv[i].setModemState(ModemState.PENDING_RESUME);
                rv[i].setDevice(devices[i]);
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        } catch (VerizonApiException e) {
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            for (int i = 0; i < devices.length; i++) {
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.ERROR);
                rv[i].setResult(e.getMessage());
                rv[i].setDevice(devices[i]);
                if (e.getErrorResponse() != null) {
                	rv[i].setErrorCode(e.getErrorResponse().getErrorCode());
                	rv[i].setErrorString(e.getErrorResponse().getErrorMessage());
                }
            }
            return rv;
        }
    }

    @Nonnull
    @Override
    public DeviceOperationResult[] getCurrentState(@Nonnull USATDevice[] devices) throws ModemServiceException {
        try {
            loginIfNeed();
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            int i = 0;
            for (USATDevice dev : devices) {
                VerizonDeviceId vDev = new VerizonDeviceId();
                if (dev.getIccid() != null) {
                    vDev.setKind(VerizonDeviceKind.ICCID);
                    vDev.setId(dev.getIccid());
                } else if (dev.getMeid() != null) {
                    vDev.setKind(VerizonDeviceKind.MEID);
                    vDev.setId(dev.getMeid());
                } else if (dev.getImei() != null) {
                    vDev.setKind(VerizonDeviceKind.IMEI);
                    vDev.setId(dev.getImei());
                } else {
                    throw new IllegalArgumentException("Device doesn't have ICCID, MEID or IMEI");
                }
                VerizonListResult res = dao.list(vDev, null, null, token);
                VerizonDevice[] sims = res.getDevices();
                if (sims.length != 1) {
                    throw new ModemServiceException("Too many devices returned. Requested: 1" +
                            ", returned: " + sims.length);
                }
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.COMPLETED);
                VerizonDeviceId[] dIds = sims[0].getDeviceIds();
                String id = null;
                VerizonDeviceKind idKind = null;
                for (VerizonDeviceId dId : dIds) {
                    if (dId.getKind() == VerizonDeviceKind.ICCID ||
                        dId.getKind() == VerizonDeviceKind.MEID ||
                        dId.getKind() == VerizonDeviceKind.IMEI) {
                        id = dId.getId();
                        idKind = dId.getKind();
                    }
                    if (dId.getKind() == VerizonDeviceKind.ICCID) {
                        break; // But prefer ICCID as ID
                    }
                }
                if (id == null) {
                    throw new VerizonApiException(VerizonErrorCode.REQUEST_FAILED_UNEXPECTED, "Invalid device ID returned. Device: " + Arrays.toString(dIds), null);
                }
                rv[i].setResult(id);
                USATDevice d = new USATDevice();
                if (idKind == VerizonDeviceKind.ICCID) {
                    d.setIccid(id);
                    d.setSerialCode(id);
                } else if (idKind == VerizonDeviceKind.MEID) {
                    d.setMeid(id);
                    d.setSerialCode(id);
                } else if (idKind == VerizonDeviceKind.IMEI) {
                    d.setImei(id);
                }
                d.setAccountName(sims[0].getAccountName());
                d.setServicePlan(sims[0].getCarrierInfos()[0].getServicePlan());
                rv[i].setDevice(d);
                rv[i].setModemState(fromVerizonState(sims[0].getCarrierInfos()[0].getState()));
                i++;
            }
            return rv;
        } catch (UnavailableException e) {
            throw new ModemServiceException(e, getApiProviderName());
        } catch (VerizonApiException e) {
            DeviceOperationResult[] rv = new DeviceOperationResult[devices.length];
            for (int i = 0; i < devices.length; i++) {
                rv[i] = new DeviceOperationResult();
                rv[i].setOperationStatus(OperationStatus.ERROR);
                rv[i].setResult(e.getMessage());
                rv[i].setModemState(ModemState.UNKNOWN);
                rv[i].setDevice(devices[i]);
                if (e.getErrorResponse() != null) {
                	rv[i].setErrorCode(e.getErrorResponse().getErrorCode());
                	rv[i].setErrorString(e.getErrorResponse().getErrorMessage());
                }
            }
            return rv;
        }
    }

    @Override
    public boolean isSuitableDevice(@Nonnull USATDevice device) {
        return device.getProviderName() != null && device.getProviderName().toLowerCase().contains("verizon");
    }

    public VerizonModemApi getDao() {
        return dao;
    }

    public void setDao(VerizonModemApi dao) {
        this.dao = dao;
    }

    private void loginIfNeed() throws UnavailableException, ModemServiceException {
        loginLock.lock();
        try {
            String apiToken = dao.obtainApiToken(appKey, appSecret);
            token = dao.login(username, password, apiToken);
        } catch (VerizonApiException e) {
            throw new ModemServiceException(e, getApiProviderName());
        } finally {
            loginLock.unlock();
        }
    }

    private String getApiProviderName() {
        return dao.getClass().getName();
    }

    @Override
    public ModemApiProvider getProviderType() {
        return ModemApiProvider.VERIZON;
    }

    private ModemState fromVerizonState(VerizonDeviceState simState) {
        if (simState == null) {
            return ModemState.UNKNOWN;
        }
        switch (simState) {
            case PRE_ACTIVATE:
            case PENDING_ACTIVATION:
                return ModemState.PENDING_ACTIVATION;
            case ACTIVE:
                return ModemState.ACTIVE;
            case SUSPEND:
            case PENDING_RESUME:
            case PENDING_SUSPEND:
                return ModemState.SUSPENDED;
            case DEACTIVE:
            case PENDING_DEACTIVATION:
                return ModemState.DEACTIVATED;
        }
        return ModemState.UNKNOWN;
    }
}
