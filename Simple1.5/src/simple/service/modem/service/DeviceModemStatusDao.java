package simple.service.modem.service;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.service.modem.service.dto.DeviceModemState;
import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.List;

/**
 * DAO for DEVICE.DEVICE_MODEM_STATUS table
 *
 * @author dlozenko
 */
public interface DeviceModemStatusDao {
    @Nonnull List<DeviceModemState> getLatestActivatedDevicesStatuses(boolean pending, boolean activated, boolean failed) throws SQLException, DataLayerException, ConvertException;
    DeviceModemState getDeviceModemStatus(String modemSerialCode) throws SQLException, DataLayerException, ConvertException;
    void updateDeviceModemStatus(DeviceModemState state) throws SQLException, DataLayerException;
    void updateDeviceModemStatusByRequestId(DeviceModemState state) throws SQLException, DataLayerException;
    void insertDeviceModemStatus(DeviceModemState state) throws SQLException, DataLayerException;
    Long findDeviceIdBySerialCode(String iccid) throws SQLException, DataLayerException, ConvertException;
		String findRequestIdByModemSerialCode(String modemSerialCode) throws SQLException, DataLayerException, ConvertException;
    String findModemSerialCodeByDeviceId(long deviceId) throws SQLException, DataLayerException, ConvertException;
    void updateModemStatusInDeviceSettings(long deviceId, String status, String changedBy) throws SQLException, DataLayerException;
}
