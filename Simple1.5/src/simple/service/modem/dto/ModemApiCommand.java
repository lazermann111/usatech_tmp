package simple.service.modem.dto;

/**
 * Commands which can be executed on Modem API
 */
public enum ModemApiCommand {
    ACTIVATE,
    DEACTIVATE,
    SUSPEND,
    RESUME,
    GET_STATE
}
