package simple.service.modem.dao.eseye;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import simple.net.http.UrlConnectionHttpClient;
import simple.net.http.HttpClientException;
import simple.rest.gson.GsonAnnotationExclusionStrategy;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivateResult;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivateSimStatus;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivatedSim;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import simple.service.modem.dao.eseye.dto.error.EsEyeSimError;
import simple.service.modem.dao.eseye.dto.getsims.EsEyeGetSimsResult;
import simple.service.modem.dao.eseye.dto.getsims.EsEyeModemIdFields;
import simple.service.modem.dao.eseye.dto.getsims.EsEyeSearchCriteria;
import simple.service.modem.dao.eseye.dto.login.EsEyeLoginResult;
import simple.service.modem.dao.eseye.dto.suspend.EsEyeSuspendResult;
import simple.service.modem.dao.eseye.dto.terminate.EsEyeTerminateResult;
import simple.service.modem.dao.eseye.dto.unsuspend.EsEyeUnSuspendResult;
import simple.service.modem.dao.eseye.gson.SimErrorsDeserializer;
import simple.service.modem.dao.eseye.gson.SimStatusesDeserializer;
import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.dao.generic.exception.UnavailableException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of EsEye Modem API
 *
 * @author dlozenko
 */
public class EsEyeModemApiImpl implements EsEyeModemApi {
    public static final String BASE_API_URL = "https://siam.eseye.com/Japi/Tigrillo/";
    private final UrlConnectionHttpClient httpClient;
    private final Gson gson;

    public EsEyeModemApiImpl() {
        httpClient = new UrlConnectionHttpClient(BASE_API_URL);
        gson = new GsonBuilder()
                .serializeNulls()
                .setExclusionStrategies(new GsonAnnotationExclusionStrategy())
                .registerTypeAdapter(EsEyeActivateSimStatus[].class, new SimStatusesDeserializer())
                .registerTypeAdapter(EsEyeSimError[].class, new SimErrorsDeserializer())
                .create();
    }

    @Nonnull
    @Override
    public EsEyeLoginResult login(@Nonnull String user, @Nonnull String password, @Nonnull String portfolioId) throws UnavailableException {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("username", user);
            params.put("password", password);
            params.put("portfolioID", portfolioId);
            String requestJsonStr = gson.toJson(params);
            String responseJsonStr = httpClient.post("login", requestJsonStr);
            EsEyeLoginResult rv = gson.fromJson(responseJsonStr, EsEyeLoginResult.class);
            rv.setCookie("PHPSESSID=" + rv.getCookie());
            return rv;
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to login on EsEye", e, ModemApiProvider.ESEYE);
        }
    }

    @Nonnull
    @Override
    public EsEyeApiCallStatus logout(@Nonnull String cookies) throws UnavailableException {
        try {
            String responseJsonStr = httpClient.post("logout", null, cookies);
            return gson.fromJson(responseJsonStr, EsEyeApiCallStatus.class);
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to logout from EsEye", e, ModemApiProvider.ESEYE);
        }
    }

    @Nonnull
    @Override
    public EsEyeGetSimsResult getSims(@Nullable EsEyeSearchCriteria searchCriteria,
                                      @Nullable EsEyeModemIdFields sortOrder,
                                      @Nullable Integer startRec,
                                      @Nullable Integer numRecs,
                                      @Nonnull String cookies) throws UnavailableException {
        if (searchCriteria == null) {
            searchCriteria = new EsEyeSearchCriteria();
        }
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("searchCriteria", searchCriteria);
            params.put("sortOrder", sortOrder);
            params.put("startRec", startRec);
            params.put("numRecs", numRecs);
            String requestJsonStr = gson.toJson(params);
            String responseJsonStr = httpClient.post("getSIMs", requestJsonStr, cookies);
            return gson.fromJson(responseJsonStr, EsEyeGetSimsResult.class);
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to logout from EsEye", e, ModemApiProvider.ESEYE);
        }
    }

    @Nonnull
    @Override
    public EsEyeActivateResult activateSims(@Nonnull EsEyeActivatedSim[] sims, @Nonnull String cookies) throws UnavailableException {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("sims", sims);
            params.put("tariffID", sims[0].getTariffId());
            params.put("group", sims[0].getGroup());
            params.put("device", sims[0].getDevice());
            String requestJsonStr = gson.toJson(params);
            String responseJsonStr = httpClient.post("activateSIMs", requestJsonStr, cookies);
            return gson.fromJson(responseJsonStr, EsEyeActivateResult.class);
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to logout from EsEye", e, ModemApiProvider.ESEYE);
        }
    }

    @Nonnull
    @Override
    public EsEyeTerminateResult terminateSims(@Nonnull String[] simICCIDs, @Nonnull String cookies) throws UnavailableException {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("sims", simICCIDs);
            String requestJsonStr = gson.toJson(params);
            String responseJsonStr = httpClient.post("terminateSIMs", requestJsonStr, cookies);
            return gson.fromJson(responseJsonStr, EsEyeTerminateResult.class);
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to logout from EsEye", e, ModemApiProvider.ESEYE);
        }
    }

    @Nonnull
    @Override
    public EsEyeSuspendResult suspendSims(@Nonnull String[] simICCIDs, @Nonnull String cookies) throws UnavailableException {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("sims", simICCIDs);
            String requestJsonStr = gson.toJson(params);
            String responseJsonStr = httpClient.post("suspendSIMs", requestJsonStr, cookies);
            return gson.fromJson(responseJsonStr, EsEyeSuspendResult.class);
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to logout from EsEye", e, ModemApiProvider.ESEYE);
        }
    }

    @Nonnull
    @Override
    public EsEyeUnSuspendResult unSuspendSims(@Nonnull String[] simICCIDs, @Nonnull String cookies) throws UnavailableException {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("sims", simICCIDs);
            String requestJsonStr = gson.toJson(params);
            String responseJsonStr = httpClient.post("unsuspendSIMs", requestJsonStr, cookies);
            return gson.fromJson(responseJsonStr, EsEyeUnSuspendResult.class);
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to logout from EsEye", e, ModemApiProvider.ESEYE);
        }
    }
}
