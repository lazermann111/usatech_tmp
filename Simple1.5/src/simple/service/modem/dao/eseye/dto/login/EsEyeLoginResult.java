package simple.service.modem.dao.eseye.dto.login;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import java.io.Serializable;

/**
 * EsEye login result
 *
 * @author dlozenko
 */
public class EsEyeLoginResult implements Serializable {
    private static final long serialVersionUID = -4437692839408934459L;

    @SerializedName("cookie")
    private String cookie;
    @SerializedName("permissions")
    private String permissions;
    @SerializedName("canActivate")
    private String canActivate;
    @SerializedName("status")
    private EsEyeApiCallStatus status;

    public EsEyeLoginResult() {
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public String canActivate() {
        return canActivate;
    }

    public void setCanActivate(String canActivate) {
        this.canActivate = canActivate;
    }

    public EsEyeApiCallStatus getStatus() {
        return status;
    }

    public void setStatus(EsEyeApiCallStatus status) {
        this.status = status;
    }
}
