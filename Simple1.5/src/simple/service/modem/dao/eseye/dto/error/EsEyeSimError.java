package simple.service.modem.dao.eseye.dto.error;

import java.io.Serializable;

/**
 * SIM error for EsEye activated SIM
 *
 * @author dlozenko
 */
public class EsEyeSimError implements Serializable {
    private static final long serialVersionUID = 3281149753897232456L;

    private String iccid;
    private String errorMessage;

    public EsEyeSimError() {
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
