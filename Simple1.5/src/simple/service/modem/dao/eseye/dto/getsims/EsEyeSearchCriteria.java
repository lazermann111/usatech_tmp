package simple.service.modem.dao.eseye.dto.getsims;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * EsEye SIM search criteria
 *
 * @author dlozenko
 */
public class EsEyeSearchCriteria implements Serializable {
    private static final long serialVersionUID = -7411560039683340365L;

    @SerializedName("state")
    private EsEyeSimState state;
    @SerializedName("group")
    private String group;
    @SerializedName("tariff")
    private String tariffId;
    @SerializedName("matchString")
    private String matchString;
    @SerializedName("matchType")
    private EsEyeStrMatchType matchType;
    @SerializedName("matchFields")
    private EsEyeModemIdFields matchFields;

    public EsEyeSearchCriteria() {
    }

    public EsEyeSimState getState() {
        return state;
    }

    public void setState(EsEyeSimState state) {
        this.state = state;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getMatchString() {
        return matchString;
    }

    public void setMatchString(String matchString) {
        this.matchString = matchString;
    }

    public EsEyeStrMatchType getMatchType() {
        return matchType;
    }

    public void setMatchType(EsEyeStrMatchType matchType) {
        this.matchType = matchType;
    }

    public EsEyeModemIdFields getMatchFields() {
        return matchFields;
    }

    public void setMatchFields(EsEyeModemIdFields matchFields) {
        this.matchFields = matchFields;
    }
}
