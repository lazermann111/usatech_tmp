package simple.service.modem.dao.eseye.dto.error;

import com.google.gson.annotations.SerializedName;

/**
 * EsEye API call error code
 *
 * @author dlozenko
 */
public enum EsEyeErrorCode {
    @SerializedName("E0000")
    UNKNOWN_USER_PASSWORD_1("E0000", "Unknown username or password"),
    @SerializedName("F0000")
    NO_RESPONSE("F0000", "No response provided"),
    @SerializedName("E0001")
    BAD_JSON("E0001", "Invalid input - Syntax error, malformed JSON"),
    @SerializedName("E0002")
    BAD_FUNCTION("E0002", "Function not recognized"),
    @SerializedName("E0003")
    BAD_API_VERSION("E0003", "API version not recognized"),
    @SerializedName("E0004")
    MISSING_PARAMETER("E0004", "Missing parameter"),
    @SerializedName("E0006")
    PERMISSION_DENIED("E0006", "Permission denied"),
    @SerializedName("E0100")
    UNKNOWN_USER_PASSWORD_2("E0100", "Unknown username or password"),
    @SerializedName("E0116")
    INVALID_PASSWORD("E0116", "Invalid password"),
    @SerializedName("E0126")
    PASSWORDS_DONT_MATCH("E0126", "Passwords don't match"),
    @SerializedName("E0132")
    INVALID_TARIFF("E0132", "Invalid tariff selected"),
    @SerializedName("E0133")
    INVALID_DEVICE_TYPE("E0133", "Invalid device type selected"),
    @SerializedName("E0137")
    INVALID_SIM_NUM("E0137", "Invalid SIM no. provided"),
    @SerializedName("E0139")
    CANT_CHANGE_SIM_STATE("E0139", "SIM cannot change state: SIM is pending termination"),
    @SerializedName("E0140")
    INVALID_SIM_STATE_REQUESTED("E0140", "Invalid state requested"),
    @SerializedName("E0142")
    CANNOT_ACTIVATE_SIM("E0142", "Cannot activate SIM via this route");

    private String errorCode;
    private String errorDesc;

    EsEyeErrorCode(String errorCode, String errorDesc) {
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorDescription() {
        return errorDesc;
    }

    public static EsEyeErrorCode fromStringError(String errorStr) {
        EsEyeErrorCode[] values = EsEyeErrorCode.values();
        for (EsEyeErrorCode value : values) {
            if (value.getErrorCode().equalsIgnoreCase(errorStr)) {
                return value;
            }
        }
        return null;
    }
}
