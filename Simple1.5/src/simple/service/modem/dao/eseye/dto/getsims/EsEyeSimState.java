package simple.service.modem.dao.eseye.dto.getsims;

import com.google.gson.annotations.SerializedName;

/**
 * EsEye SIM card state
 *
 * @author dlozenko
 */
public enum EsEyeSimState {
    @SerializedName("available")
    AVAILABLE("available"),
    @SerializedName("suspended")
    SUSPENDED("suspended"),
    @SerializedName("terminated")
    TERMINATED("terminated"),
    @SerializedName("provisioned")
    PROVISIONED("provisioned"),
    @SerializedName("pendingDeprovision")
    PENDING_DEPROVISION("pendingDeprovision"),
    @SerializedName("unprovisioned")
    UNPROVISIONED("unprovisioned");

    private String state;

    EsEyeSimState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public static EsEyeSimState fromString(String str) {
        EsEyeSimState[] values = EsEyeSimState.values();
        for (EsEyeSimState value : values) {
            if (value.getState().equalsIgnoreCase(str)) {
                return value;
            }
        }
        return null;
    }
}
