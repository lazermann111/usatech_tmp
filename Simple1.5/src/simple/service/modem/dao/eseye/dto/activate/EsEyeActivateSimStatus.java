package simple.service.modem.dao.eseye.dto.activate;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * SIM status for EsEye activated SIM
 *
 * @author dlozenko
 */
public class EsEyeActivateSimStatus implements Serializable {
    private static final long serialVersionUID = 1461341656502928165L;

    @SerializedName("ICCID")
    private String iccid;
    @SerializedName("status")
    private EsEyeActivationStatus status;

    public EsEyeActivateSimStatus() {
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public EsEyeActivationStatus getStatus() {
        return status;
    }

    public void setStatus(EsEyeActivationStatus status) {
        this.status = status;
    }
}
