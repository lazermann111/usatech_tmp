package simple.service.modem.dao.eseye.dto.terminate;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.eseye.dto.error.EsEyeSimError;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import java.io.Serializable;

/**
 * Terminate result for EsEye SIM
 *
 * @author dlozenko
 */
public class EsEyeTerminateResult implements Serializable {
    private static final long serialVersionUID = 57745530827707955L;

    @SerializedName("simStatuses")
    private EsEyeSimError[] simErrors;
    @SerializedName("status")
    EsEyeApiCallStatus status;

    public EsEyeTerminateResult() {
    }

    public EsEyeSimError[] getSimErrors() {
        return simErrors;
    }

    public void setSimErrors(EsEyeSimError[] simErrors) {
        this.simErrors = simErrors;
    }

    public EsEyeApiCallStatus getStatus() {
        return status;
    }

    public void setStatus(EsEyeApiCallStatus status) {
        this.status = status;
    }
}
