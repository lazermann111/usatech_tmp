package simple.service.modem.dao.eseye.dto.unsuspend;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.eseye.dto.error.EsEyeSimError;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import java.io.Serializable;

/**
 * Un Suspedion result for EsEye SIM
 */
public class EsEyeUnSuspendResult implements Serializable {
    private static final long serialVersionUID = -2375164680149766636L;

    @SerializedName("simStatuses")
    private EsEyeSimError[] simErrors;
    @SerializedName("status")
    EsEyeApiCallStatus status;

    public EsEyeUnSuspendResult() {
    }

    public EsEyeSimError[] getSimErrors() {
        return simErrors;
    }

    public void setSimErrors(EsEyeSimError[] simErrors) {
        this.simErrors = simErrors;
    }

    public EsEyeApiCallStatus getStatus() {
        return status;
    }

    public void setStatus(EsEyeApiCallStatus status) {
        this.status = status;
    }
}
