package simple.service.modem.dao.eseye.dto.login;

import java.util.ArrayList;
import java.util.List;

/**
 * EsEye user permission
 */
public enum EsEyePermission {
    FINANCE("F"),
    ADMIN("A"),
    STANDARD_USER("U"),
    LOGISTICS("L"),
    TECHNICAL("T"),
    ENGINEERING("E");

    private String permissionChar;

    EsEyePermission(String permissionChar) {
        this.permissionChar = permissionChar;
    }

    public String getPermissionChar() {
        return permissionChar;
    }

    public static EsEyePermission fromPermissionChar(String permissionChar) {
        EsEyePermission[] values = EsEyePermission.values();
        for (EsEyePermission value : values) {
            if (value.getPermissionChar().equalsIgnoreCase(permissionChar)) {
                return value;
            }
        }
        return null;
    }

    public static EsEyePermission[] fromPermissionChars(String permissionChars) {
        List<EsEyePermission> rv = new ArrayList<>();
        char[] permCharsArr = permissionChars.toCharArray();
        for (char permChar : permCharsArr) {
            rv.add(fromPermissionChar(String.valueOf(permChar)));
        }
        return rv.toArray(new EsEyePermission[rv.size()]);
    }
}
