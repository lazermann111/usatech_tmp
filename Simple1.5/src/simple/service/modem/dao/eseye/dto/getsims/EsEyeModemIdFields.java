package simple.service.modem.dao.eseye.dto.getsims;

import com.google.gson.annotations.SerializedName;

/**
 * Match type for EsEye strings
 *
 * @author dlozenko
 */
public enum EsEyeModemIdFields {
    @SerializedName("F")
    FRIENDLY_NAME("F"),
    @SerializedName("M")
    MSISDN("M"),
    @SerializedName("S")
    IMSI("S"),
    @SerializedName("I")
    ICCID("I"),
    @SerializedName("E")
    IMEI("E"),
    @SerializedName("A")
    IP_ADDRESS("A");

    private String matchField;

    EsEyeModemIdFields(String matchField) {
        this.matchField = matchField;
    }

    public String getMatchField() {
        return matchField;
    }

    public static EsEyeModemIdFields fromString(String str) {
        EsEyeModemIdFields[] values = EsEyeModemIdFields.values();
        for (EsEyeModemIdFields value : values) {
            if (value.getMatchField().equalsIgnoreCase(str)) {
                return value;
            }
        }
        return null;
    }
}
