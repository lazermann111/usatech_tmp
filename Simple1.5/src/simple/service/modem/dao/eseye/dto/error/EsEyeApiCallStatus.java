package simple.service.modem.dao.eseye.dto.error;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Error status for any API call of EsEye modem API
 *
 * @author dlozenko
 */
public class EsEyeApiCallStatus implements Serializable {
    private static final long serialVersionUID = -5732002016036058810L;

    @SerializedName("status")
    private EsEyeErrorStatus status;
    @SerializedName(value = "errorCode", alternate = {"code"})
    private EsEyeErrorCode errorCode;
    @SerializedName(value = "errorMessage", alternate = {"message"})
    private String errorMessage;

    public EsEyeApiCallStatus() {
    }

    public EsEyeErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(EsEyeErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public EsEyeErrorStatus getStatus() {
        return status;
    }

    public void setStatus(EsEyeErrorStatus status) {
        this.status = status;
    }
}
