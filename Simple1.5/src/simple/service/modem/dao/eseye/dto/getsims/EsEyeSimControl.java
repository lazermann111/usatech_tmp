package simple.service.modem.dao.eseye.dto.getsims;

/**
 * Whether the SIM card can be sent a control message.
 */
public enum EsEyeSimControl {
    SMS("S"),
    USSD("U"),
    USSD_AND_SMS("US");

    private String controls;

    EsEyeSimControl(String controls) {
        this.controls = controls;
    }

    public String getControls() {
        return controls;
    }

    public static EsEyeSimControl fromString(String str) {
        EsEyeSimControl[] values = EsEyeSimControl.values();
        for (EsEyeSimControl value : values) {
            if (value.getControls().equalsIgnoreCase(str)) {
                return value;
            }
        }
        return null;
    }
}
