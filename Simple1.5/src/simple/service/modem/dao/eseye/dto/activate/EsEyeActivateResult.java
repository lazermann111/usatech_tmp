package simple.service.modem.dao.eseye.dto.activate;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.eseye.dto.error.EsEyeSimError;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import java.io.Serializable;

/**
 * Activation result for EsEye SIM
 *
 * @author dlozenko
 */
public class EsEyeActivateResult implements Serializable {
    private static final long serialVersionUID = 1456487892115984645L;

    @SerializedName("simStatuses")
    private EsEyeActivateSimStatus[] simStatuses;
    @SerializedName("simErrors")
    private EsEyeSimError[] simErrors;
    @SerializedName("status")
    EsEyeApiCallStatus status;

    public EsEyeActivateResult() {
    }

    public EsEyeActivateSimStatus[] getSimStatuses() {
        return simStatuses;
    }

    public void setSimStatuses(EsEyeActivateSimStatus[] simStatuses) {
        this.simStatuses = simStatuses;
    }

    public EsEyeSimError[] getSimErrors() {
        return simErrors;
    }

    public void setSimErrors(EsEyeSimError[] simErrors) {
        this.simErrors = simErrors;
    }

    public EsEyeApiCallStatus getStatus() {
        return status;
    }

    public void setStatus(EsEyeApiCallStatus status) {
        this.status = status;
    }
}
