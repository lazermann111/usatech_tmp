package simple.service.modem.dao.eseye.dto.error;

import com.google.gson.annotations.SerializedName;

/**
 * EsEye execution error status
 *
 * @author dlozenko
 */
public enum EsEyeErrorStatus {
    @SerializedName("OK")
    SUCCESS("OK"),
    @SerializedName("ERR")
    ERROR("ERR");

    private String errorStatus;

    EsEyeErrorStatus(String errorStatus) {
        this.errorStatus = errorStatus;
    }

    public String getErrorStatus() {
        return errorStatus;
    }

    public static EsEyeErrorStatus fromStringStatus(String statusStr) {
        EsEyeErrorStatus[] values = EsEyeErrorStatus.values();
        for (EsEyeErrorStatus value : values) {
            if (value.getErrorStatus().equalsIgnoreCase(statusStr)) {
                return value;
            }
        }
        return null;
    }
}
