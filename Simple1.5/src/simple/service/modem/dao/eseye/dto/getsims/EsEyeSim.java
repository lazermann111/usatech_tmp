package simple.service.modem.dao.eseye.dto.getsims;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * EsEye SIM card
 *
 * @author dlozenko
 */
public class EsEyeSim implements Serializable {
    private static final long serialVersionUID = 473695267690605025L;

    @SerializedName("MSISDN")
    private Long msisdn;
    @SerializedName("friendlyName")
    private String friendlyName;
    @SerializedName("ICCID")
    private String iccid;
    @SerializedName("ipAddress")
    private String ipAddress;
    @SerializedName("group")
    private String group;
    @SerializedName("status")
    private EsEyeSimState status;
    @SerializedName("alert")
    private String alert;
    @SerializedName("controls")
    private EsEyeSimControl controls;
    @SerializedName("mappedMSISDN")
    private String mappedMsisdn;

    public EsEyeSim() {
    }

    public Long getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(Long msisdn) {
        this.msisdn = msisdn;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public EsEyeSimState getStatus() {
        return status;
    }

    public void setStatus(EsEyeSimState status) {
        this.status = status;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public EsEyeSimControl getControls() {
        return controls;
    }

    public void setControls(EsEyeSimControl controls) {
        this.controls = controls;
    }

    public String getMappedMsisdn() {
        return mappedMsisdn;
    }

    public void setMappedMsisdn(String mappedMsisdn) {
        this.mappedMsisdn = mappedMsisdn;
    }
}
