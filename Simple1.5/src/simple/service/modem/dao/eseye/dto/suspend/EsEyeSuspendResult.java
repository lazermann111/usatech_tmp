package simple.service.modem.dao.eseye.dto.suspend;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.eseye.dto.error.EsEyeSimError;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import java.io.Serializable;

/**
 * Suspension result for EsEye SIM
 *
 * @author dlozenko
 */
public class EsEyeSuspendResult implements Serializable {
    private static final long serialVersionUID = -3427971636844207031L;

    @SerializedName("simStatuses")
    private EsEyeSimError[] simErrors;
    @SerializedName("status")
    EsEyeApiCallStatus status;

    public EsEyeSuspendResult() {
    }

    public EsEyeSimError[] getSimErrors() {
        return simErrors;
    }

    public void setSimErrors(EsEyeSimError[] simErrors) {
        this.simErrors = simErrors;
    }

    public EsEyeApiCallStatus getStatus() {
        return status;
    }

    public void setStatus(EsEyeApiCallStatus status) {
        this.status = status;
    }
}
