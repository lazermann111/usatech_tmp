package simple.service.modem.dao.eseye.dto.getsims;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import java.io.Serializable;

/**
 * Result of EsEye GetSims API cal
 *
 * @author dlozenko
 */
public class EsEyeGetSimsResult implements Serializable {
    private static final long serialVersionUID = 6531416824325133666L;

    @SerializedName("totRecs")
    private Integer totalRecs;
    @SerializedName("sims")
    private EsEyeSim[] sims;
    @SerializedName("status")
    private EsEyeApiCallStatus status;

    public EsEyeGetSimsResult() {
    }

    public Integer getTotalRecs() {
        return totalRecs;
    }

    public void setTotalRecs(Integer totalRecs) {
        this.totalRecs = totalRecs;
    }

    public EsEyeSim[] getSims() {
        return sims;
    }

    public void setSims(EsEyeSim[] sims) {
        this.sims = sims;
    }

    public EsEyeApiCallStatus getStatus() {
        return status;
    }

    public void setStatus(EsEyeApiCallStatus status) {
        this.status = status;
    }
}
