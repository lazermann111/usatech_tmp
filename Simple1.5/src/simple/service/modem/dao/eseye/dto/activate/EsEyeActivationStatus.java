package simple.service.modem.dao.eseye.dto.activate;

/**
 * Status enum of EsEye SIM activation
 *
 * @author dlozenko
 */
public enum EsEyeActivationStatus {
    OK_MANUAL("OK"),
    OK_AUTO("AUTO");

    private String status;

    EsEyeActivationStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public static EsEyeActivationStatus fromStringStatus(String statusStr) {
        EsEyeActivationStatus[] values = EsEyeActivationStatus.values();
        for (EsEyeActivationStatus value : values) {
            if (value.getStatus().equalsIgnoreCase(statusStr)) {
                return value;
            }
        }
        return null;
    }
}
