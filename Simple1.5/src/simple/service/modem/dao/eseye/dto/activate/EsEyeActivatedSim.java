package simple.service.modem.dao.eseye.dto.activate;

import com.google.gson.annotations.SerializedName;
import simple.rest.gson.Exclude;
import java.io.Serializable;

/**
 * EsEye SIM to be activated
 *
 * @author dlozenko
 */
public class EsEyeActivatedSim implements Serializable {
    private static final long serialVersionUID = 2177124044162596691L;

    @SerializedName("ICCID")
    private String iccid;
    @SerializedName("friendlyName")
    private String friendlyName;
    @SerializedName("IMEI")
    private String imei;
    @Exclude
    private String tariffId;
    @Exclude
    private String group;
    @Exclude
    private String device;

    public EsEyeActivatedSim() {
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
