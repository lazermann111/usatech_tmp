package simple.service.modem.dao.eseye.dto.getsims;

import com.google.gson.annotations.SerializedName;

/**
 * Match type for EsEye strings
 *
 * @author dlozenko
 */
public enum EsEyeStrMatchType {
    @SerializedName("S")
    STARTS_WITH("S"),
    @SerializedName("E")
    ENDS_WITH("E"),
    @SerializedName("C")
    CONTAINS("C"),
    @SerializedName("M")
    MATCHES("M");

    private String matchType;

    EsEyeStrMatchType(String matchType) {
        this.matchType = matchType;
    }

    public String getMatchType() {
        return matchType;
    }

    public static EsEyeStrMatchType fromString(String str) {
        EsEyeStrMatchType[] values = EsEyeStrMatchType.values();
        for (EsEyeStrMatchType value : values) {
            if (value.getMatchType().equalsIgnoreCase(str)) {
                return value;
            }
        }
        return null;
    }
}
