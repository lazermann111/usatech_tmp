package simple.service.modem.dao.eseye.gson;

import com.google.gson.*;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivateSimStatus;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * GSON deserializer for EsEye sim statuses
 *
 * @author dlozenko
 */
public class SimStatusesDeserializer implements JsonDeserializer<EsEyeActivateSimStatus[]> {
    @Override
    public EsEyeActivateSimStatus[] deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        if (jsonObject == null) {
            return null;
        }
        Set<Map.Entry<String,JsonElement>> entities = jsonObject.entrySet();
        if (entities == null || entities.size() == 0) {
            return null;
        }
        List<EsEyeActivateSimStatus> rv = new ArrayList<>();
        for (Map.Entry<String,JsonElement> entry : entities) {
            EsEyeActivateSimStatus esEyeActivateSimStatus = jsonDeserializationContext.deserialize(entry.getValue(), EsEyeActivateSimStatus.class);
            rv.add(esEyeActivateSimStatus);
        }
        return rv.toArray(new EsEyeActivateSimStatus[rv.size()]);
    }
}
