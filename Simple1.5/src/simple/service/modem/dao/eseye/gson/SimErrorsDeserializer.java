package simple.service.modem.dao.eseye.gson;

import com.google.gson.*;
import simple.service.modem.dao.eseye.dto.error.EsEyeSimError;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * GSON deserializer for EsEye sim statuses
 *
 * @author dlozenko
 */
public class SimErrorsDeserializer implements JsonDeserializer<EsEyeSimError[]> {
    @Override
    public EsEyeSimError[] deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        if (jsonElement == null) {
            return null;
        }
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        if (jsonObject == null) {
            return null;
        }
        Set<Map.Entry<String,JsonElement>> entities = jsonObject.entrySet();
        if (entities == null || entities.size() == 0) {
            return null;
        }
        List<EsEyeSimError> rv = new ArrayList<>();
        for (Map.Entry<String,JsonElement> entry : entities) {
            EsEyeSimError simError = new EsEyeSimError();
            simError.setIccid(entry.getKey());
            simError.setErrorMessage(entry.getValue().getAsString());
            rv.add(simError);
        }
        return rv.toArray(new EsEyeSimError[rv.size()]);
    }
}
