package simple.service.modem.dao.eseye;

import simple.service.modem.dao.eseye.dto.activate.EsEyeActivateResult;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivateSimStatus;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivatedSim;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivationStatus;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import simple.service.modem.dao.eseye.dto.error.EsEyeErrorCode;
import simple.service.modem.dao.eseye.dto.error.EsEyeErrorStatus;
import simple.service.modem.dao.eseye.dto.error.EsEyeSimError;
import simple.service.modem.dao.eseye.dto.getsims.*;
import simple.service.modem.dao.eseye.dto.login.EsEyeLoginResult;
import simple.service.modem.dao.eseye.dto.login.EsEyePermission;
import simple.service.modem.dao.eseye.dto.suspend.EsEyeSuspendResult;
import simple.service.modem.dao.eseye.dto.terminate.EsEyeTerminateResult;
import simple.service.modem.dao.eseye.dto.unsuspend.EsEyeUnSuspendResult;
import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.dao.generic.exception.UnavailableException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Stub realisation of EsEye modem API
 *
 * @author dlozenko
 */
public class EsEyeModemApiStub implements EsEyeModemApi {
    public static final String STUB_COOKIE = "STUB-COOKIE";
    public static final String STUB_ICCID = "STUB-ICCID";

    @Override
    @Nonnull
    public EsEyeLoginResult login(@Nonnull String user, @Nonnull String password, @Nonnull String portfolioId) throws UnavailableException {
        EsEyeLoginResult rv = new EsEyeLoginResult();
        if ("unavailable".equals(user)) {
            throwNetError();
        } else if ("wrong".equals(user)) {
            rv.setStatus(buildCallStatus(false, EsEyeErrorCode.INVALID_PASSWORD, "STUB: Unknown user/password pair."));
        } else if ("correct".equals(user)) {
            rv.setStatus(buildCallStatus(true, null, null));
            rv.setCanActivate("yes");
            rv.setCookie(STUB_COOKIE);
            rv.setPermissions(EsEyePermission.STANDARD_USER.getPermissionChar());
        } else {
            // Unexpected error
            rv.setStatus(buildCallStatus(false, EsEyeErrorCode.BAD_API_VERSION, "STUB: This API has obsolete, please upgrade your app to use API v.2.0"));
        }
        return rv;
    }

    @Override
    @Nonnull
    public EsEyeApiCallStatus logout(@Nonnull String cookies) throws UnavailableException {
        if (STUB_COOKIE.equals(cookies)) {
            return buildCallStatus(true, null, null);
        } else if (cookies == null) {
            throwNetError();
        }
        return buildCallStatus(false, EsEyeErrorCode.PERMISSION_DENIED, "STUB: Invalid session.");
    }

    @Override
    @Nonnull
    public EsEyeGetSimsResult getSims(@Nullable EsEyeSearchCriteria searchCriteria,
                                      @Nullable EsEyeModemIdFields sortOrder,
                                      @Nullable Integer startRec,
                                      @Nullable Integer numRecs,
                                      @Nonnull String cookies) throws UnavailableException {
        EsEyeGetSimsResult rv = new EsEyeGetSimsResult();
        rv.setStatus(buildCallStatus(true, null, null));
        rv.setTotalRecs(3);
        EsEyeSim[] sims = new EsEyeSim[3];
        sims[0] = new EsEyeSim();
        sims[0].setIccid(STUB_ICCID + "_1");
        sims[0].setStatus(EsEyeSimState.PROVISIONED);
        sims[1] = new EsEyeSim();
        sims[1].setIccid(STUB_ICCID + "_2");
        sims[1].setStatus(EsEyeSimState.SUSPENDED);
        sims[2] = new EsEyeSim();
        sims[2].setIccid(STUB_ICCID + "_3");
        sims[2].setStatus(EsEyeSimState.TERMINATED);
        rv.setSims(sims);
        return rv;
    }

    @Override
    @Nonnull
    public EsEyeActivateResult activateSims(@Nonnull EsEyeActivatedSim[] sims, @Nonnull String cookies) throws UnavailableException {
        EsEyeApiCallStatus authResult = checkCookies(cookies);
        EsEyeActivateResult rv = new EsEyeActivateResult();
        rv.setStatus(authResult);
        if (authResult.getStatus() == EsEyeErrorStatus.ERROR) {
            return rv;
        }
        authResult.setStatus(EsEyeErrorStatus.ERROR); //Emulate EsEye behavior if some sim card was not activated
        authResult.setErrorMessage(null);
        authResult.setErrorCode(null);
        EsEyeSimError[] simErrors = new EsEyeSimError[1];
        simErrors[0] = new EsEyeSimError();
        simErrors[0].setIccid(STUB_ICCID + "_1");
        simErrors[0].setErrorMessage("STUB: Bad iccid");
        rv.setSimErrors(simErrors);
        EsEyeActivateSimStatus simStatuses[] = new EsEyeActivateSimStatus[2];
        simStatuses[0] = new EsEyeActivateSimStatus();
        simStatuses[0].setIccid(STUB_ICCID + "_2");
        simStatuses[0].setStatus(EsEyeActivationStatus.OK_MANUAL);
        simStatuses[1] = new EsEyeActivateSimStatus();
        simStatuses[1].setIccid(STUB_ICCID + "_3");
        simStatuses[1].setStatus(EsEyeActivationStatus.OK_AUTO);
        rv.setSimStatuses(simStatuses);
        return rv;
    }

    @Override
    @Nonnull
    public EsEyeTerminateResult terminateSims(@Nonnull String[] simICCIDs, @Nonnull String cookies) throws UnavailableException {
        EsEyeApiCallStatus authResult = checkCookies(cookies);
        EsEyeTerminateResult rv = new EsEyeTerminateResult();
        rv.setStatus(authResult);
        return rv;
    }

    @Override
    @Nonnull
    public EsEyeSuspendResult suspendSims(@Nonnull String[] simICCIDs, @Nonnull String cookies) throws UnavailableException {
        EsEyeApiCallStatus authResult = checkCookies(cookies);
        EsEyeSuspendResult rv = new EsEyeSuspendResult();
        rv.setStatus(authResult);
        return rv;
    }

    @Override
    @Nonnull
    public EsEyeUnSuspendResult unSuspendSims(@Nonnull String[] simICCIDs, @Nonnull String cookies) throws UnavailableException {
        EsEyeApiCallStatus authResult = checkCookies(cookies);
        EsEyeUnSuspendResult rv = new EsEyeUnSuspendResult();
        rv.setStatus(authResult);
        return rv;
    }

    EsEyeApiCallStatus buildCallStatus(boolean isOk, EsEyeErrorCode code, String message) {
        EsEyeApiCallStatus rv = new EsEyeApiCallStatus();
        if (isOk) {
            rv.setStatus(EsEyeErrorStatus.SUCCESS);
        } else {
            rv.setStatus(EsEyeErrorStatus.ERROR);
            rv.setErrorCode(code);
            rv.setErrorMessage(message);
        }
        return rv;
    }

    void throwNetError() throws UnavailableException {
        throw new UnavailableException("STUB: Service unavailable, network fault.", ModemApiProvider.ESEYE);
    }

    EsEyeApiCallStatus checkCookies(String cookies) throws UnavailableException {
        if (STUB_COOKIE.equals(cookies)) {
            return buildCallStatus(true, null, null);
        } else if (cookies == null) {
            throwNetError();
        }
        return buildCallStatus(false, EsEyeErrorCode.PERMISSION_DENIED, "STUB: Invalid session.");
    }
}
