package simple.service.modem.dao.eseye;

import simple.service.modem.dao.eseye.dto.activate.EsEyeActivateResult;
import simple.service.modem.dao.eseye.dto.activate.EsEyeActivatedSim;
import simple.service.modem.dao.eseye.dto.error.EsEyeApiCallStatus;
import simple.service.modem.dao.eseye.dto.getsims.*;
import simple.service.modem.dao.eseye.dto.login.EsEyeLoginResult;
import simple.service.modem.dao.eseye.dto.suspend.EsEyeSuspendResult;
import simple.service.modem.dao.eseye.dto.terminate.EsEyeTerminateResult;
import simple.service.modem.dao.eseye.dto.unsuspend.EsEyeUnSuspendResult;
import simple.service.modem.dao.generic.exception.UnavailableException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Public interface of EsEye SIM modem
 *
 * @author dlozenko
 */
public interface EsEyeModemApi {
    @Nonnull
    EsEyeLoginResult login(@Nonnull String user,
                           @Nonnull String password,
                           @Nonnull String portfolioId) throws UnavailableException;

    @Nonnull
    EsEyeApiCallStatus logout(@Nonnull String cookies) throws UnavailableException;

    @Nonnull
    EsEyeGetSimsResult getSims(@Nullable EsEyeSearchCriteria searchCriteria,
                               @Nullable EsEyeModemIdFields sortOrder,
                               @Nullable Integer startRec,
                               @Nullable Integer numRecs,
                               @Nonnull String cookies) throws UnavailableException;

    @Nonnull
    EsEyeActivateResult activateSims(@Nonnull EsEyeActivatedSim[] sims,
                                     @Nonnull String cookies) throws UnavailableException;

    @Nonnull
    EsEyeTerminateResult terminateSims(@Nonnull String[] simICCIDs,
                                       @Nonnull String cookies) throws UnavailableException;

    @Nonnull
    EsEyeSuspendResult suspendSims(@Nonnull String[] simICCIDs,
                                   @Nonnull String cookies) throws UnavailableException;

    @Nonnull
    EsEyeUnSuspendResult unSuspendSims(@Nonnull String[] simICCIDs,
                                       @Nonnull String cookies) throws UnavailableException;
}
