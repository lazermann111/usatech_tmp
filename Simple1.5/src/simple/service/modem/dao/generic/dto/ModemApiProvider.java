package simple.service.modem.dao.generic.dto;

import javax.annotation.Nullable;

/**
 * Modem API provider
 *
 * @author dlozenko
 */
public enum ModemApiProvider {
    ESEYE, VERIZON;

    @Nullable
    public static ModemApiProvider fromString(@Nullable String str) {
        if (str == null) {
            return null;
        }
        str = str.toLowerCase();
        for (ModemApiProvider val : values()) {
            if (str.contains(val.name().toLowerCase())) {
                return val;
            }
        }
        return null;
    }
}
