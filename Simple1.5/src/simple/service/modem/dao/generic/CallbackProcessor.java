package simple.service.modem.dao.generic;

import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Callback processor for processing input results from async services API.
 * In generic it process input HTTP request and return service-specific callback result class.
 */
public interface CallbackProcessor<T> { // T = VerizonCallbackResult ?
    @Nonnull
    T process(@Nonnull HttpServletRequest req,
              @Nonnull HttpServletResponse resp) throws ServletException, IOException;
}
