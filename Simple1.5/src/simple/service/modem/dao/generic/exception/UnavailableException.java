package simple.service.modem.dao.generic.exception;

import simple.service.modem.dao.generic.dto.ModemApiProvider;

/**
 * Inform about service unavailable error while accessing modem status API.
 * This may happen because of problems with internet access or if target service is down.
 *
 * @author dlozenko
 */
public class UnavailableException extends Exception {
    private static final long serialVersionUID = -1109393425918008430L;

    private final ModemApiProvider provider;

    public UnavailableException(ModemApiProvider provider) {
        this.provider = provider;
    }

    public UnavailableException(String message, ModemApiProvider provider) {
        super(message);
        this.provider = provider;
    }

    public UnavailableException(String message, Throwable cause, ModemApiProvider provider) {
        super(message, cause);
        this.provider = provider;
    }

    public UnavailableException(Throwable cause, ModemApiProvider provider) {
        super(cause);
        this.provider = provider;
    }

    public UnavailableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ModemApiProvider provider) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.provider = provider;
    }

    public ModemApiProvider getProvider() {
        return provider;
    }
}
