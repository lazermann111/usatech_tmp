package simple.service.modem.dao.verizon.dto.callback;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorResponse;

/**
 * Result for register callback API call
 */
public class VerizonRegisterCallbackResult extends VerizonErrorResponse {
    private static final long serialVersionUID = 717592427892054564L;

    @SerializedName("accountName")
    private String accountName;

    @SerializedName("serviceName")
    private VerizonCallbackService serviceName;

    public VerizonRegisterCallbackResult() {
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public VerizonCallbackService getServiceName() {
        return serviceName;
    }

    public void setServiceName(VerizonCallbackService serviceName) {
        this.serviceName = serviceName;
    }
}
