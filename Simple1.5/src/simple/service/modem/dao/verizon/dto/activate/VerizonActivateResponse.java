package simple.service.modem.dao.verizon.dto.activate;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.verizon.dto.list.VerizonDeviceState;
import simple.service.modem.dao.verizon.dto.login.VerizonDeviceCredentials;
import java.io.Serializable;

/**
 * Callback response for Verizon activate API call
 */
public class VerizonActivateResponse implements Serializable {
    private static final long serialVersionUID = -1803376668593279802L;

    @SerializedName("deviceIds")
    private VerizonDeviceId[] deviceIds;

    @SerializedName("ipAddress")
    private String ipAddress;

    @SerializedName("state")
    private VerizonDeviceState state;

    @SerializedName("servicePlan")
    private String servicePlan;

    @SerializedName("featureCodes")
    private String[] featureCodes;

    @SerializedName("deviceCredential")
    private VerizonDeviceCredentials deviceCredentials;

    public VerizonActivateResponse() {
    }

    public VerizonDeviceId[] getDeviceIds() {
        return deviceIds;
    }

    public void setDeviceIds(VerizonDeviceId[] deviceIds) {
        this.deviceIds = deviceIds;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public VerizonDeviceState getState() {
        return state;
    }

    public void setState(VerizonDeviceState state) {
        this.state = state;
    }

    public String getServicePlan() {
        return servicePlan;
    }

    public void setServicePlan(String servicePlan) {
        this.servicePlan = servicePlan;
    }

    public String[] getFeatureCodes() {
        return featureCodes;
    }

    public void setFeatureCodes(String[] featureCodes) {
        this.featureCodes = featureCodes;
    }

    public VerizonDeviceCredentials getDeviceCredentials() {
        return deviceCredentials;
    }

    public void setDeviceCredentials(VerizonDeviceCredentials deviceCredentials) {
        this.deviceCredentials = deviceCredentials;
    }
}
