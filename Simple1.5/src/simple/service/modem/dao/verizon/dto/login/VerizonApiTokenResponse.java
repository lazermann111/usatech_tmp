package simple.service.modem.dao.verizon.dto.login;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorResponse;

/**
 * Verizon API token response
 */
public class VerizonApiTokenResponse extends VerizonErrorResponse {
    private static final long serialVersionUID = 772108175981219160L;

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("scope")
    private String scope;

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("expires_in")
    private Integer expiresIn;

    public VerizonApiTokenResponse() {
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }
}
