package simple.service.modem.dao.verizon.dto.callback;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceId;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorResponse;

/**
 * Verizon callback result
 */
public class VerizonCallbackResult extends VerizonErrorResponse {
    private static final long serialVersionUID = 1337464266170174402L;

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("requestId")
    private String requestId;

    @SerializedName("deviceIds")
    private VerizonDeviceId[] deviceIds;

    @SerializedName("deviceResponse")
    private VerizonDeviceResponse deviceResponse;
    
    @SerializedName("faultResponse")
    private VerizonFaultResponse faultResponse;

    @SerializedName("comment")
    private String comment;

    public VerizonCallbackResult() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public VerizonDeviceId[] getDeviceIds() {
        return deviceIds;
    }

    public void setDeviceIds(VerizonDeviceId[] deviceIds) {
        this.deviceIds = deviceIds;
    }

    public VerizonDeviceResponse getDeviceResponse() {
        return deviceResponse;
    }

    public void setDeviceResponse(VerizonDeviceResponse deviceResponse) {
        this.deviceResponse = deviceResponse;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

		public VerizonFaultResponse getFaultResponse() {
			return faultResponse;
		}

		public void setFaultResponse(VerizonFaultResponse faultResponse) {
			this.faultResponse = faultResponse;
		}
}
