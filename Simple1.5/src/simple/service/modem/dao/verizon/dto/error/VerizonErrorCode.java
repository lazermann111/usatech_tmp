package simple.service.modem.dao.verizon.dto.error;

/**
 * Error code that describe error in Verizon API call.
 */
public enum VerizonErrorCode {
    // Note: some error codes not included due not needed for our tasks
    INPUT_INVALID_CHAR("INPUT_INVALID.Request.InvalidCharacter"),
    INPUT_INVALID_REQ("INPUT_INVALID.RequestUndefined"),
    REQUEST_FAILED_UNEXPECTED("REQUEST_FAILED.UnexpectedError"),
    DESERIALIZATION_FAILED("DeserializationFailed"),

    REQUEST_FAILED_SESSION_TOKEN_EXPIRED("REQUEST_FAILED.SessionToken.Expired"),
    REQUEST_FAILED_SESSION_TOKEN_FORMAT("REQUEST_FAILED.SessionToken.Format"),
    INPUT_INVALID_SESSION_TOKEN("INPUT_INVALID.SessionToken.Invalid"),

    INPUT_INVALID_ACCOUNT_NOT_DEFINED("INPUT_INVALID.Account.NotDefined"),
    REQUEST_FAILED_ACCOUNT_NAME("REQUEST_FAILED.AccountName"),
    REQUEST_FAILED_ACCOUNT_NOT_FOUND("REQUEST_FAILED.AccountNotFound"),
    REQUEST_FAILED_NO_ACCOUNTS("REQUEST_FAILED.NoAccounts"),
    REQUEST_FAILED_MULTIPLY_ACCOUNTS("REQUEST_FAILED.MultipleAccounts"),

    INPUT_INVALID_NO_LIST("INPUT_INVALID.Nolist of devicesOrDeviceFilters"),
    INPUT_INVALID_BOTH_LIST("INPUT_INVALID.Bothlist of devicesAndDeviceFiltersDefined"),
    INPUT_INVALID_DEV_KIND_INVALID("INPUT_INVALID.Device.Kind.Invalid"),
    INPUT_INVALID_DEV_KIND_NOT_DEFINED("INPUT_INVALID.Device.Kind.NotDefined"),
    INPUT_INVALID_NO_DEV_ID_IN_COLLECTION("INPUT_INVALID.NoDeviceIdentifierInCollection"),
    INPUT_INVALID_DEV_ID_NOT_DEFINED("INPUT_INVALID.Device.Identifier.NotDefined"),
    INPUT_INVALID_DEV_ID_INVALID("INPUT_INVALID.Device.Identifier.Invalid"),
    INPUT_INVALID_DUPLICATE_DEV_KIND_IN_COLLECTION("INPUT_INVALID.DuplicateDeviceKindInCollection"),
    REQUEST_FAILED_DEV_NOT_FOUND("REQUEST_FAILED.DeviceNotFound"),
    INPUT_INVALID_LIST("INPUT_INVALID.list of devicesAccountNameAndDeviceGroupDefined"),
    INPUT_INVALID_NOLIST("INPUT_INVALID.Nolist of devicesAccountNameOrGroup"),
    INPUT_INVALID_DEV_ID_NULL("INPUT_INVALID.Device.Identifier.Null"),

    INPUT_INVALID_CUSTOM_FIELD_INVALID_NAME("INPUT_INVALID.CustomField.InvalidName"),
    INPUT_INVALID_CUSTOM_FIELD_NOT_DEFINED("INPUT_INVALID.CustomField.NotDefined"),
    INPUT_INVALID_CUSTOM_FIELD_TOO_LONG("INPUT_INVALID.CustomField.TooLong"),
    INPUT_INVALID_CUSTOM_FIELD_INVALID_CHAR("INPUT_INVALID.CustomFieldValue.InvalidCharacter"),

    INPUT_INVALID_SERVICE_NAME_NOT_ALPHANUM("INPUT_INVALID.ServiceName.NotAlphanumeric"),
    INPUT_INVALID_SERVICE_NAME_INVALID("INPUT_INVALID.ServiceName.Invalid"),
    INPUT_INVALID_SERVICE_NAME_IS_EMPTY("INPUT_INVALID.ServiceName.IsEmpty"),
    INPUT_INVALID_URL_INVALID("INPUT_INVALID.Url.Invalid"),
    REQUEST_FAILED_CALLBACK_EXISTS("REQUEST_FAILED.CallbackExists"),
    REQUEST_FAILED_CALLBACK_NOT_REGISTERED("REQUEST_FAILED.Callback.NotRegistered"),

    INPUT_INVALID_LOGIN_BLANK_USERNAME("INPUT_INVALID.Login.BlankUsername"),
    INPUT_INVALID_LOGIN_BLANK_PASSWORD("INPUT_INVALID.Login.BlankPassword"),
    INPUT_INVALID_LOGIN_INVALID("INPUT_INVALID.Login.Invalid"),
    INPUT_INVALID_LOGIN_USER_LOCKED("INPUT_INVALID.Login.UserLocked"),
    INPUT_INVALID_USER_REQUIRED_ROLE("REQUEST_FAILED.User.RequiredRole"),
    INPUT_INVALID_PASSWORD_INVALID("INPUT_INVALID.Password.Incorrect");

    private String errorCode;

    VerizonErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public static VerizonErrorCode fromStringError(String errorStr) {
        VerizonErrorCode[] values = VerizonErrorCode.values();
        for (VerizonErrorCode value : values) {
            if (value.getErrorCode().equalsIgnoreCase(errorStr)) {
                return value;
            }
        }
        return REQUEST_FAILED_UNEXPECTED;
    }
}
