package simple.service.modem.dao.verizon.dto.activate;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Verizon device description
 */
public class VerizonDeviceId implements Serializable {
    private static final long serialVersionUID = 4248519491628939544L;

    @SerializedName("kind")
    private VerizonDeviceKind kind;

    @SerializedName("id")
    private String id;

    public VerizonDeviceId() {
    }

    public VerizonDeviceKind getKind() {
        return kind;
    }

    public void setKind(VerizonDeviceKind kind) {
        this.kind = kind;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
