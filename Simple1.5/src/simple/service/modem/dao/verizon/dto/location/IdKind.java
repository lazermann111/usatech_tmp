package simple.service.modem.dao.verizon.dto.location;

import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceKind;

public enum IdKind {
	imei("VLTE Verizon Modem", VerizonDeviceKind.IMEI),
	meid("CDMA Verizon Modem", VerizonDeviceKind.MEID);

	private String typeDesc;
	private VerizonDeviceKind vdKind;

	private IdKind(String typeDesc, VerizonDeviceKind vdKind) {
		this.typeDesc = typeDesc;
		this.vdKind = vdKind;
	}

	public String getTypeDesc() {
		return typeDesc;
	}

	public VerizonDeviceKind getVerizonDeviceKind() {
		return vdKind;
	}

	public String checkDeviceId(String hostSerial, String hostLabel) {
		String testDeviceId = null;
		switch (this) {
			case imei:
				testDeviceId = hostLabel;
				break;
			case meid:
				testDeviceId = hostSerial;
				break;
		}
		if (testDeviceId == null)
			return null;
		testDeviceId = testDeviceId.trim();
		switch (this) {
			case imei:
				if (testDeviceId.matches("35[0-9]{13}"))
					return testDeviceId;
				break;
			case meid:
				if (testDeviceId.matches("A10000[0-9A-F]{8}"))
					return testDeviceId;
				if (testDeviceId.matches("A10000,[0-9A-F]{8}"))
					return "A10000" + testDeviceId.substring(7);
				break;
		}
		return null;
	}

	public String checkMdn(String testMdn) {
		if (testMdn == null)
			return null;
		switch (this) {
			case imei:
				if (testMdn.matches("\\+1[0-9]{10}"))
					return testMdn.substring(2);
				break;
			case meid:
				if (testMdn.matches("[0-9]{10}"))
					return testMdn;
				if (testMdn.matches(",[0-9]{10},129"))
					return testMdn.substring(1, 11);
				if (testMdn.matches("[0-9]{10}\""))
					return testMdn.substring(0, 10);
				break;
		}
		return null;
	}

	public static IdKind getValueOf(String typeDesc) {
		for (IdKind kindValue: IdKind.values()) {
			if (kindValue.getTypeDesc().equals(typeDesc))
				return kindValue;
		}
		return null;
	}
}