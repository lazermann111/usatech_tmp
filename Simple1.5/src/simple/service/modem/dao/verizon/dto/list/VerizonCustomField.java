package simple.service.modem.dao.verizon.dto.list;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Custom field of Verizon API
 */
public class VerizonCustomField implements Serializable {
    private static final long serialVersionUID = 3393372858525524233L;

    @SerializedName("key")
    private String key;

    @SerializedName("value")
    private String value;

    public VerizonCustomField() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
