package simple.service.modem.dao.verizon.dto.suspend;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Verizon async callback response on resume device command
 */
public class VerizonResumeResponse implements Serializable {
    private static final long serialVersionUID = -1240350263642849338L;

    @SerializedName("restored")
    private boolean restored;

    public VerizonResumeResponse() {
    }

    public boolean isRestored() {
        return restored;
    }

    public void setRestored(boolean restored) {
        this.restored = restored;
    }
}
