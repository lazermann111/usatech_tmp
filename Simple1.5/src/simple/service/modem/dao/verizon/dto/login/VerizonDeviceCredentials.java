package simple.service.modem.dao.verizon.dto.login;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Verizon device credentials
 */
public class VerizonDeviceCredentials implements Serializable {
    private static final long serialVersionUID = 3845238077037993395L;

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    public VerizonDeviceCredentials() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
