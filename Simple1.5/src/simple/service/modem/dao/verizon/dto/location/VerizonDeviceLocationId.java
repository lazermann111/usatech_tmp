package simple.service.modem.dao.verizon.dto.location;

import com.google.gson.annotations.SerializedName;

import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceKind;

import java.io.Serializable;

/**
 * Verizon device description
 */
public class VerizonDeviceLocationId implements Serializable {
	private static final long serialVersionUID = 7459156359590981604L;

	@SerializedName("kind")
	private VerizonDeviceKind kind;

	@SerializedName("id")
	private String id;

	@SerializedName("mdn")
	private String mdn;

	public VerizonDeviceLocationId() {
	}

	public VerizonDeviceLocationId(VerizonDeviceKind kind, String id, String mdn) {
		super();
		this.kind = kind;
		this.id = id;
		this.mdn = mdn;
	}

	public VerizonDeviceKind getKind() {
		return kind;
	}

	public void setKind(VerizonDeviceKind kind) {
		this.kind = kind;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMdn() {
		return mdn;
	}

	public void setMdn(String mdn) {
		this.mdn = mdn;
	}

	@Override
	public String toString() {
		return "VerizonDeviceLocationId [kind=" + kind + ", id=" + id + ", mdn=" + mdn + "]";
	}

}
