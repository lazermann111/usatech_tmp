package simple.service.modem.dao.verizon.dto.location;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class VerizonPositionError implements Serializable {
	private static final long serialVersionUID = 5624317587436401695L;

	@SerializedName("type")
	private String type;

	@SerializedName("info")
	private String info;

	@SerializedName("time")
	private String time;

	@SerializedName("utcoffset")
	private String utcOffset;

	@Override
	public String toString() {
		return "VerizonPositionError [type=" + type + ", info=" + info + ", time=" + time + ", utcOffset=" + utcOffset + "]";
	}

}
