package simple.service.modem.dao.verizon.dto.activate;

import com.google.gson.annotations.SerializedName;

/**
 * Verizon device kind
 */
public enum VerizonDeviceKind {
    @SerializedName("esn")
    ESN("esn"),

    @SerializedName("meid")
    MEID("meid"),

    @SerializedName("imei")
    IMEI("imei"),

    @SerializedName("iccId")
    ICCID("iccId");

    private String kind;

    VerizonDeviceKind(String kind) {
        this.kind = kind;
    }

    public String getKind() {
        return kind;
    }

    public static VerizonDeviceKind fromString(String str) {
        VerizonDeviceKind[] values = VerizonDeviceKind.values();
        for (VerizonDeviceKind value : values) {
            if (value.getKind().equalsIgnoreCase(str)) {
                return value;
            }
        }
        return null;
    }
}
