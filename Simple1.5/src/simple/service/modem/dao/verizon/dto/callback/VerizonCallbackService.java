package simple.service.modem.dao.verizon.dto.callback;

import com.google.gson.annotations.SerializedName;

/**
 * Callback service name to register callbacks for
 */
public enum VerizonCallbackService {
    @SerializedName("CarrierService")
    CARRIER_SERVICE("CarrierService"),

    @SerializedName("DeviceUsage")
    DEVICE_USAGE("DeviceUsage"),

    @SerializedName("DevicePRLInformation")
    DEVICE_PRL_INFORMATION("DevicePRLInformation"),

    @SerializedName("SMSDeliveryConfirmation")
    SMS_DELIVERY_CONFIRMATION("SMSDeliveryConfirmation"),

    @SerializedName("EnhancedConnectivityService")
    ENHANCED_CONNECTIVITY_SERVICE("EnhancedConnectivityService");

    private String kind;

    VerizonCallbackService(String kind) {
        this.kind = kind;
    }

    public String getKind() {
        return kind;
    }

    public static VerizonCallbackService fromString(String str) {
        VerizonCallbackService[] values = VerizonCallbackService.values();
        for (VerizonCallbackService value : values) {
            if (value.getKind().equalsIgnoreCase(str)) {
                return value;
            }
        }
        return null;
    }
}
