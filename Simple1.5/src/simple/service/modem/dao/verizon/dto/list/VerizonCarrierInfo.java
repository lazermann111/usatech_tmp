package simple.service.modem.dao.verizon.dto.list;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Verizon carrier information
 */
public class VerizonCarrierInfo implements Serializable {
    private static final long serialVersionUID = 1460375807616556061L;

    @SerializedName("carrierName")
    private String carrierName;

    @SerializedName("servicePlan")
    private String servicePlan;

    @SerializedName("state")
    private VerizonDeviceState state;

    public VerizonCarrierInfo() {
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getServicePlan() {
        return servicePlan;
    }

    public void setServicePlan(String servicePlan) {
        this.servicePlan = servicePlan;
    }

    public VerizonDeviceState getState() {
        return state;
    }

    public void setState(VerizonDeviceState state) {
        this.state = state;
    }
}
