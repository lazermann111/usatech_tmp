package simple.service.modem.dao.verizon.dto.suspend;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Verizon suspend response
 */
public class VerizonSuspendResponse implements Serializable {
    private static final long serialVersionUID = -3362611669515464163L;

    @SerializedName("expectedResumeDate")
    private String expectedResumeDate;

    @SerializedName("maxSuspendDaysAllowed")
    private int maxSuspendDaysAllowed;

    @SerializedName("numDaysSuspendedLast12Months")
    private int numDaysSuspendedLast12Months;

    @SerializedName("numDaysSuspendAllowedCurrent12Months")
    private int numDaysSuspendAllowedCurrent12Months;

    public VerizonSuspendResponse() {
    }

    public String getExpectedResumeDate() {
        return expectedResumeDate;
    }

    public void setExpectedResumeDate(String expectedResumeDate) {
        this.expectedResumeDate = expectedResumeDate;
    }

    public int getMaxSuspendDaysAllowed() {
        return maxSuspendDaysAllowed;
    }

    public void setMaxSuspendDaysAllowed(int maxSuspendDaysAllowed) {
        this.maxSuspendDaysAllowed = maxSuspendDaysAllowed;
    }

    public int getNumDaysSuspendedLast12Months() {
        return numDaysSuspendedLast12Months;
    }

    public void setNumDaysSuspendedLast12Months(int numDaysSuspendedLast12Months) {
        this.numDaysSuspendedLast12Months = numDaysSuspendedLast12Months;
    }

    public int getNumDaysSuspendAllowedCurrent12Months() {
        return numDaysSuspendAllowedCurrent12Months;
    }

    public void setNumDaysSuspendAllowedCurrent12Months(int numDaysSuspendAllowedCurrent12Months) {
        this.numDaysSuspendAllowedCurrent12Months = numDaysSuspendAllowedCurrent12Months;
    }
}
