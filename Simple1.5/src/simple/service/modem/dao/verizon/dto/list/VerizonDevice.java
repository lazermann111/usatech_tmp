package simple.service.modem.dao.verizon.dto.list;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceId;
import java.io.Serializable;
import java.util.Date;

/**
 * Verizon device description returned from list API call
 */
public class VerizonDevice implements Serializable {
    private static final long serialVersionUID = 7631941760635638925L;

    @SerializedName("accountName")
    private String accountName;

    @SerializedName("billingCycleEndDate")
    private String billingCycleEndDate;

    @SerializedName("carrierInformations")
    private VerizonCarrierInfo[] carrierInfos;

    @SerializedName("connected")
    private Boolean connected;

    @SerializedName("createdAt")
    private Date createdAt;

    @SerializedName("customFields")
    private VerizonCustomField[] customFields;

    @SerializedName("deviceIds")
    private VerizonDeviceId[] deviceIds;

    @SerializedName("groupNames")
    private String[] groupNames;

    @SerializedName("ipAddress")
    private String ipAddress;

    @SerializedName("lastActivationBy")
    private String lastActivationBy;

    @SerializedName("lastActivationDate")
    private String lastActivationDate;

    @SerializedName("lastConnectionDate")
    private String lastConnectionDate;

    @SerializedName("extendedAttributes")
    private VerizonCustomField[] extendedAttributes;

    public VerizonDevice() {
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBillingCycleEndDate() {
        return billingCycleEndDate;
    }

    public void setBillingCycleEndDate(String billingCycleEndDate) {
        this.billingCycleEndDate = billingCycleEndDate;
    }

    public VerizonCarrierInfo[] getCarrierInfos() {
        return carrierInfos;
    }

    public void setCarrierInfos(VerizonCarrierInfo[] carrierInfos) {
        this.carrierInfos = carrierInfos;
    }

    public Boolean getConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public VerizonCustomField[] getCustomFields() {
        return customFields;
    }

    public void setCustomFields(VerizonCustomField[] customFields) {
        this.customFields = customFields;
    }

    public VerizonDeviceId[] getDeviceIds() {
        return deviceIds;
    }

    public void setDeviceIds(VerizonDeviceId[] deviceIds) {
        this.deviceIds = deviceIds;
    }

    public String[] getGroupNames() {
        return groupNames;
    }

    public void setGroupNames(String[] groupNames) {
        this.groupNames = groupNames;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLastActivationBy() {
        return lastActivationBy;
    }

    public void setLastActivationBy(String lastActivationBy) {
        this.lastActivationBy = lastActivationBy;
    }

    public String getLastActivationDate() {
        return lastActivationDate;
    }

    public void setLastActivationDate(String lastActivationDate) {
        this.lastActivationDate = lastActivationDate;
    }

    public String getLastConnectionDate() {
        return lastConnectionDate;
    }

    public void setLastConnectionDate(String lastConnectionDate) {
        this.lastConnectionDate = lastConnectionDate;
    }

    public VerizonCustomField[] getExtendedAttributes() {
        return extendedAttributes;
    }

    public void setExtendedAttributes(VerizonCustomField[] extendedAttributes) {
        this.extendedAttributes = extendedAttributes;
    }
}
