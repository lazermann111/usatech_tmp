package simple.service.modem.dao.verizon.dto.location;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class VerizonPositionData implements Serializable {
	private static final long serialVersionUID = 5608846977139802659L;

	@SerializedName("x")
	private String x;

	@SerializedName("y")
	private String y;

	@SerializedName("radius")
	private String radius;

	@SerializedName("qos")
	private String qos;

	@SerializedName("time")
	private String time;

	@SerializedName("utcoffset")
	private String utcOffset;

	public String getX() {
		return x;
	}

	public String getY() {
		return y;
	}

	public String getRadius() {
		return radius;
	}

	public String getQos() {
		return qos;
	}

	public String getTime() {
		return time;
	}

	public String getUtcOffset() {
		return utcOffset;
	}

}
