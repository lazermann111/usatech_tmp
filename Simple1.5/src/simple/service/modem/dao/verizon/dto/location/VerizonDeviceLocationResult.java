package simple.service.modem.dao.verizon.dto.location;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class VerizonDeviceLocationResult implements Serializable {
	private static final long serialVersionUID = 4992349578260718261L;

	@SerializedName("msid")
	private String msid;

	@SerializedName("pd")
	private VerizonPositionData positionData;

	@SerializedName("error")
	private VerizonPositionError positionError;

	public String getMsid() {
		return msid;
	}

	public VerizonPositionData getPositionData() {
		return positionData;
	}

	public VerizonPositionError getPositionError() {
		return positionError;
	}

}
