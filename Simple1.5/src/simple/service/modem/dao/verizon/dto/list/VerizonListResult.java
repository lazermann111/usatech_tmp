package simple.service.modem.dao.verizon.dto.list;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorResponse;

/**
 * Result to be returned from Verizon list API call
 */
public class VerizonListResult extends VerizonErrorResponse {
    private static final long serialVersionUID = 5319960905510171224L;

    @SerializedName("devices")
    private VerizonDevice[] devices;

    @SerializedName("hasMoreData")
    private boolean hasMoreData;

    public VerizonListResult() {
    }

    public VerizonDevice[] getDevices() {
        return devices;
    }

    public void setDevices(VerizonDevice[] devices) {
        this.devices = devices;
    }

    public boolean hasMoreData() {
        return hasMoreData;
    }

    public void setHasMoreData(boolean hasMoreData) {
        this.hasMoreData = hasMoreData;
    }
}
