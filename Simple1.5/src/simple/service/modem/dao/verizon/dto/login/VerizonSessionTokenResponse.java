package simple.service.modem.dao.verizon.dto.login;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorResponse;

/**
 * Verizon session token response
 */
public class VerizonSessionTokenResponse extends VerizonErrorResponse {
    private static final long serialVersionUID = 6769423207882396651L;

    @SerializedName("sessionToken")
    private String sessionToken;

    public VerizonSessionTokenResponse() {
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }
}
