package simple.service.modem.dao.verizon.dto.activate;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Response on Verizon deactivate async callback
 */
public class VerizonDeactivateResponse implements Serializable {
    private static final long serialVersionUID = -7433636310477633526L;

    @SerializedName("deactivated")
    private boolean deactivated;

    public VerizonDeactivateResponse() {
    }

    public boolean isDeactivated() {
        return deactivated;
    }

    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }
}
