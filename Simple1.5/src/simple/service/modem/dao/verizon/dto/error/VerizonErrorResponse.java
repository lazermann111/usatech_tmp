package simple.service.modem.dao.verizon.dto.error;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Verizon error code/message response
 */
public class VerizonErrorResponse implements Serializable {
    private static final long serialVersionUID = 7595100172928588341L;

    @SerializedName("errorCode")
    private String errorCode;

    @SerializedName("errorMessage")
    private String errorMessage;

    public VerizonErrorResponse() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
