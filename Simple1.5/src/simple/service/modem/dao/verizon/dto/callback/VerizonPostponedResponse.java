package simple.service.modem.dao.verizon.dto.callback;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorResponse;

/**
 * Verizon postponed response with request ID
 */
public class VerizonPostponedResponse extends VerizonErrorResponse {
    private static final long serialVersionUID = -1823808532649360511L;

    @SerializedName("requestId")
    private String requestId;

    public VerizonPostponedResponse() {
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
