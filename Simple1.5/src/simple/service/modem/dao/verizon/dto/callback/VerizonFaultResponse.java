package simple.service.modem.dao.verizon.dto.callback;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class VerizonFaultResponse implements Serializable {

	private static final long serialVersionUID = -2744042546751219772L;
	
	@SerializedName ("detail")
	private String detail;
	@SerializedName ("faultactor")
	private String faultActor;
	@SerializedName ("faultcode")
	private String faultCode;
	@SerializedName ("faultstring")
	private String faultString;
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getFaultActor() {
		return faultActor;
	}
	public void setFaultActor(String faultActor) {
		this.faultActor = faultActor;
	}
	public String getFaultCode() {
		return faultCode;
	}
	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}
	public String getFaultString() {
		return faultString;
	}
	public void setFaultString(String faultString) {
		this.faultString = faultString;
	}

}
