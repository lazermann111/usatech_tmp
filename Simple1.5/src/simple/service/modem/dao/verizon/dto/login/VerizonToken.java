package simple.service.modem.dao.verizon.dto.login;

import java.io.Serializable;

/**
 * Verizon auth token
 */
public class VerizonToken implements Serializable {
    private static final long serialVersionUID = 5763194304287130787L;

    private String apiToken;
    private String sessionToken;

    public VerizonToken() {
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }
}
