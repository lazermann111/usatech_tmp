package simple.service.modem.dao.verizon.dto.list;

import com.google.gson.annotations.SerializedName;

/**
 * State of the Verizon device
 */
public enum VerizonDeviceState {
    @SerializedName("pre-active")
    PRE_ACTIVATE("pre-active"),

    @SerializedName("active")
    ACTIVE("active"),

    @SerializedName("deactive")
    DEACTIVE("deactive"),

    @SerializedName("suspend")
    SUSPEND("suspend"),

    @SerializedName("pending resume")
    PENDING_RESUME("pending resume"),

    @SerializedName("pending MDN change")
    PENDING_MDN_CHANGE("pending MDN change"),

    @SerializedName("pending PRL Update")
    PENDING_PRL_UPDATE("pending PRL Update"),

    @SerializedName("pending preactive")
    PENDING_PREACTIVE("pending preactive"),

    @SerializedName("pending activation")
    PENDING_ACTIVATION("pending activation"),

    @SerializedName("pending deactivation")
    PENDING_DEACTIVATION("pending deactivation"),

    @SerializedName("pending suspend")
    PENDING_SUSPEND("pending suspend"),

    @SerializedName("pending service plan change")
    PENDING_SERVICE_PLAN_CHANGE("pending service plan change"),

    @SerializedName("pending ESN / MEID change")
    PENDING_ESN_MEID_CHANGE("pending ESN / MEID change"),

    @SerializedName("pending account update")
    PENDING_ACCOUNT_UPDATE("pending account update");

    private String state;

    VerizonDeviceState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public static VerizonDeviceState fromString(String str) {
        VerizonDeviceState[] values = VerizonDeviceState.values();
        for (VerizonDeviceState value : values) {
            if (value.getState().equalsIgnoreCase(str)) {
                return value;
            }
        }
        return null;
    }
}
