package simple.service.modem.dao.verizon.dto.callback;

import com.google.gson.annotations.SerializedName;
import simple.service.modem.dao.verizon.dto.activate.VerizonActivateResponse;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeactivateResponse;
import simple.service.modem.dao.verizon.dto.suspend.VerizonResumeResponse;
import simple.service.modem.dao.verizon.dto.suspend.VerizonSuspendResponse;
import java.io.Serializable;

/**
 * Async callback response object contained one or many results
 */
public class VerizonDeviceResponse implements Serializable {
    private static final long serialVersionUID = 4012379780252991605L;

    @SerializedName("activateResponse")
    private VerizonActivateResponse activateResponse;

    @SerializedName("deactivateResponse")
    private VerizonDeactivateResponse deactivateResponse;

    @SerializedName("suspendResponse")
    private VerizonSuspendResponse suspendResponse;

    @SerializedName("restoreResponse")
    private VerizonResumeResponse resumeResponse;

    public VerizonDeviceResponse() {
    }

    public VerizonActivateResponse getActivateResponse() {
        return activateResponse;
    }

    public void setActivateResponse(VerizonActivateResponse activateResponse) {
        this.activateResponse = activateResponse;
    }

    public VerizonDeactivateResponse getDeactivateResponse() {
        return deactivateResponse;
    }

    public void setDeactivateResponse(VerizonDeactivateResponse deactivateResponse) {
        this.deactivateResponse = deactivateResponse;
    }

    public VerizonSuspendResponse getSuspendResponse() {
        return suspendResponse;
    }

    public void setSuspendResponse(VerizonSuspendResponse suspendResponse) {
        this.suspendResponse = suspendResponse;
    }

    public VerizonResumeResponse getResumeResponse() {
        return resumeResponse;
    }

    public void setResumeResponse(VerizonResumeResponse resumeResponse) {
        this.resumeResponse = resumeResponse;
    }

    public boolean hasActivateResponse() {
        return activateResponse != null;
    }

    public boolean hasDeactivateResponse() {
        return deactivateResponse != null;
    }

    public boolean hasSuspendResponse() {
        return suspendResponse != null;
    }

    public boolean hasResumeResponse() {
        return resumeResponse != null;
    }
}
