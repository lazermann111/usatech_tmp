package simple.service.modem.dao.verizon;

import simple.service.modem.dao.generic.CallbackProcessor;
import simple.service.modem.dao.verizon.dto.activate.VerizonActivateResponse;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeactivateResponse;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceId;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceKind;
import simple.service.modem.dao.verizon.dto.callback.VerizonCallbackResult;
import simple.service.modem.dao.verizon.dto.callback.VerizonDeviceResponse;
import simple.service.modem.dao.verizon.dto.list.VerizonDeviceState;
import simple.service.modem.dao.verizon.dto.login.VerizonDeviceCredentials;
import simple.service.modem.dao.verizon.dto.suspend.VerizonResumeResponse;
import simple.service.modem.dao.verizon.dto.suspend.VerizonSuspendResponse;
import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Result callback STUB processor for Verizon API
 */
public class VerizonCallbackProcessorStub implements CallbackProcessor<VerizonCallbackResult> {
    public static final String CALLBACK_ID = "VRZ-STUB-CALLBACK";
    public static final String DEVICE_ID = "VRZ-STUB-DEVICE-ID";

    @Override
    @Nonnull
    public VerizonCallbackResult process(@Nonnull HttpServletRequest req,
                                         @Nonnull HttpServletResponse resp) throws ServletException, IOException {
        VerizonCallbackResult rv = new VerizonCallbackResult();
        //VerizonCallbackId callbackId = new VerizonCallbackId();
        //callbackId.setRequestId(CALLBACK_ID);
        //callbackId.setUsername("vasia");
        //callbackId.setPassword("qwerty");
        //rv.setCallbackId(callbackId);
        rv.setComment("Hello Stub!");
        VerizonDeviceId[] deviceIds = new VerizonDeviceId[2];
        deviceIds[0] = new VerizonDeviceId();
        deviceIds[0].setId(DEVICE_ID + "-1");
        deviceIds[0].setKind(VerizonDeviceKind.ICCID);
        deviceIds[1] = new VerizonDeviceId();
        deviceIds[1].setId(DEVICE_ID + "-2");
        deviceIds[1].setKind(VerizonDeviceKind.IMEI);
        rv.setDeviceIds(deviceIds);
        VerizonDeviceResponse vdr = new VerizonDeviceResponse();
        VerizonActivateResponse var = new VerizonActivateResponse();
        VerizonDeviceCredentials credentials = new VerizonDeviceCredentials();
        credentials.setUsername("vasia-stub");
        credentials.setPassword("QsWtEuRb");
        var.setDeviceCredentials(credentials);
        var.setDeviceIds(deviceIds);
        var.setFeatureCodes(null);
        var.setIpAddress("8.8.8.8");
        var.setServicePlan("STUB-PLAN");
        var.setState(VerizonDeviceState.ACTIVE);
        vdr.setActivateResponse(var);
        VerizonDeactivateResponse vder = new VerizonDeactivateResponse();
        vder.setDeactivated(true);
        vdr.setDeactivateResponse(vder);
        VerizonResumeResponse vrr = new VerizonResumeResponse();
        vrr.setRestored(true);
        vdr.setResumeResponse(vrr);
        VerizonSuspendResponse vsr = new VerizonSuspendResponse();
        vsr.setExpectedResumeDate("03.03.2017");
        vsr.setMaxSuspendDaysAllowed(90);
        vsr.setNumDaysSuspendAllowedCurrent12Months(60);
        vsr.setNumDaysSuspendedLast12Months(32);
        vdr.setSuspendResponse(vsr);
        rv.setDeviceResponse(vdr);
        return rv;
    }
}
