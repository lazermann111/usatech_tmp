package simple.service.modem.dao.verizon;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import simple.io.Base64;
import simple.io.Log;
import simple.net.http.HttpClientException;
import simple.net.http.UrlConnectionHttpClient;
import simple.rest.gson.EmptyCheckTypeAdapterFactory;
import simple.rest.gson.GsonAnnotationExclusionStrategy;
import simple.service.modem.dao.generic.dto.ModemApiProvider;
import simple.service.modem.dao.generic.exception.UnavailableException;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceId;
import simple.service.modem.dao.verizon.dto.callback.VerizonCallbackService;
import simple.service.modem.dao.verizon.dto.callback.VerizonPostponedResponse;
import simple.service.modem.dao.verizon.dto.callback.VerizonRegisterCallbackResult;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorCode;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorResponse;
import simple.service.modem.dao.verizon.dto.list.VerizonDevice;
import simple.service.modem.dao.verizon.dto.list.VerizonListResult;
import simple.service.modem.dao.verizon.dto.location.VerizonDeviceLocationId;
import simple.service.modem.dao.verizon.dto.location.VerizonDeviceLocationResult;
import simple.service.modem.dao.verizon.dto.login.VerizonApiTokenResponse;
import simple.service.modem.dao.verizon.dto.login.VerizonSessionTokenResponse;
import simple.service.modem.dao.verizon.dto.login.VerizonToken;
import simple.service.modem.dao.verizon.exception.VerizonApiException;

/**
 * Implementation of Verizon Modem API
 */
public class VerizonModemApiImpl implements VerizonModemApi {
    static final Log log = Log.getLog();
    public static final String BASE_API_URL = "https://thingspace.verizon.com/api/";
    public static final String LOCATIONS_PATH = "loc/v1/locations";
    private final UrlConnectionHttpClient httpClient;
    private final Gson gson;
    private final Gson gsonLoc;

    public VerizonModemApiImpl() {
        httpClient = new UrlConnectionHttpClient(BASE_API_URL);
        httpClient.setNotFailHttpCode(400);
        gson = new GsonBuilder()
            .setExclusionStrategies(new GsonAnnotationExclusionStrategy())
            .create();
        gsonLoc = new GsonBuilder()
            .registerTypeAdapterFactory(new EmptyCheckTypeAdapterFactory())
            .setExclusionStrategies(new GsonAnnotationExclusionStrategy())
            .create();
    }

    @Nonnull
    @Override
    public String obtainApiToken(@Nonnull String appKey, @Nonnull String appSecret) throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            String encoded64Secrets = Base64.encodeString(appKey + ":" + appSecret, true);
            headers.put("Authorization", "Basic " + encoded64Secrets);
            headers.put("Content-Type", "application/x-www-form-urlencoded");
            headers.put("Accept", "application/json");
            log.info("Obtaining API token...");
            String responseJsonStr = httpClient.post("ts/v1/oauth2/token?grant_type=client_credentials", headers);
            log.info("Got this: " + responseJsonStr);
            VerizonApiTokenResponse response = gson.fromJson(responseJsonStr, VerizonApiTokenResponse.class);
            if (response == null || response.getAccessToken() == null) {
                throw new VerizonApiException("Unexpected response: " + responseJsonStr,
                        VerizonErrorCode.REQUEST_FAILED_UNEXPECTED,
                        BASE_API_URL + "ts/v1/oauth2/token?grant_type=client_credentials", response);
            }
            return response.getAccessToken();
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to obtain API key from Verizon. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public VerizonToken login(@Nonnull String user, @Nonnull String password, @Nonnull String apiToken) throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + apiToken);
            headers.put("Content-Type", "application/json");
            headers.put("Accept", "application/json");
            Map<String, String> params = new HashMap<>();
            params.put("username", user);
            params.put("password", password);
            String requestJsonStr = gson.toJson(params);
            log.info("Logging in: " + requestJsonStr);
            String responseJsonStr = httpClient.post("m2m/v1/session/login", requestJsonStr, headers);
            log.info("Got this: " + responseJsonStr);
            VerizonSessionTokenResponse response = gson.fromJson(responseJsonStr, VerizonSessionTokenResponse.class);
            if (response.getSessionToken() == null || response.getErrorCode() != null) {
                throw new VerizonApiException("Login failed. Response: " + responseJsonStr,
                        VerizonErrorCode.fromStringError(response.getErrorCode()),
                        BASE_API_URL + "m2m/v1/session/login", response);
            }
            VerizonToken rv = new VerizonToken();
            rv.setApiToken(apiToken);
            rv.setSessionToken(response.getSessionToken());
            return rv;
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to login to Verizon. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Override
    public void logout(@Nonnull VerizonToken token) throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token.getApiToken());
            headers.put("VZ-M2M-Token", token.getSessionToken());
            headers.put("Content-Type", "application/x-www-form-urlencoded");
            headers.put("Accept", "application/json");
            String responseJsonStr = httpClient.post("m2m/v1/session/logout", headers);
            VerizonSessionTokenResponse response = gson.fromJson(responseJsonStr, VerizonSessionTokenResponse.class);
            if (response.getSessionToken() == null || response.getErrorCode() != null) {
                throw new VerizonApiException("Logout failed. Response: " + responseJsonStr,
                        VerizonErrorCode.fromStringError(response.getErrorCode()),
                        BASE_API_URL + "m2m/v1/session/logout", response);
            }
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to logout from Verizon. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public String activate(@Nonnull VerizonDevice[] devices, @Nullable String accountName, @Nonnull String servicePlan, @Nonnull String mdnZipCode, @Nonnull String ipPoolName, @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token.getApiToken());
            headers.put("VZ-M2M-Token", token.getSessionToken());
            headers.put("Content-Type", "application/json");
            headers.put("Accept", "application/json");
            
            Map<String, Object> params = new HashMap<>();
            params.put("devices", devices);
            params.put("accountName", accountName);
            params.put("servicePlan", servicePlan);
            params.put("mdnZipCode", mdnZipCode);
            params.put("carrierIpPoolName", ipPoolName);
            
            String requestJsonStr = gson.toJson(params);
            log.info("Activating: " + requestJsonStr);
            String responseJsonStr = httpClient.post("m2m/v1/devices/actions/activate", requestJsonStr, headers);
            log.info("Got this: " + responseJsonStr);
            VerizonPostponedResponse response = gson.fromJson(responseJsonStr, VerizonPostponedResponse.class);
            if (response.getRequestId() == null || response.getErrorCode() != null) {
                throw new VerizonApiException("Activate failed. Response: " + responseJsonStr,
                        VerizonErrorCode.fromStringError(response.getErrorCode()),
                        BASE_API_URL + "m2m/v1/devices/actions/activate", response);
            }
            return response.getRequestId();
        } catch (HttpClientException e) {
        		log.error("Failed to activate Verizon devices", e);
            throw new UnavailableException("Failed to activate Verizon devices. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public String suspend(@Nullable VerizonDeviceId[] devices, @Nullable String accountName, @Nullable String servicePlan, @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token.getApiToken());
            headers.put("VZ-M2M-Token", token.getSessionToken());
            headers.put("Content-Type", "application/json");
            headers.put("Accept", "application/json");
            
            Map<String, Object> params = new HashMap<>();
            Object[] deviceIds = new Object[devices.length];
            for (int i=0; i < devices.length; i++) {
	              Map<String, Object> innerDeviceIds = new HashMap<>();
	              innerDeviceIds.put("deviceIds", new Object[]{devices[i]});
            		deviceIds[i] = innerDeviceIds;
            }
            params.put("devices", deviceIds);
            params.put("accountName", accountName);
            params.put("servicePlan", servicePlan);
            String requestJsonStr = gson.toJson(params);
            log.info("Suspending: " + requestJsonStr);
            String responseJsonStr = httpClient.post("m2m/v1/devices/actions/suspend", requestJsonStr, headers);
            log.info("Got this: " + responseJsonStr);
            VerizonPostponedResponse response = gson.fromJson(responseJsonStr, VerizonPostponedResponse.class);
            if (response.getRequestId() == null || response.getErrorCode() != null) {
                throw new VerizonApiException("Suspend failed. Response: " + responseJsonStr,
                        VerizonErrorCode.fromStringError(response.getErrorCode()),
                        BASE_API_URL + "m2m/v1/devices/actions/suspend", response);
            }
            return response.getRequestId();
        } catch (HttpClientException e) {
        		log.error("Failed to suspend Verizon devices", e);
            throw new UnavailableException("Failed to suspend Verizon devices. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public String restore(@Nullable VerizonDeviceId[] devices, @Nullable String accountName, @Nullable String servicePlan, @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token.getApiToken());
            headers.put("VZ-M2M-Token", token.getSessionToken());
            headers.put("Content-Type", "application/json");
            headers.put("Accept", "application/json");
            Map<String, Object> params = new HashMap<>();
            Object[] deviceIds = new Object[devices.length];
            for (int i=0; i < devices.length; i++) {
	              Map<String, Object> innerDeviceIds = new HashMap<>();
	              innerDeviceIds.put("deviceIds", new Object[]{devices[i]});
            		deviceIds[i] = innerDeviceIds;
            }
            params.put("devices", deviceIds);
            params.put("accountName", accountName);
            params.put("servicePlan", servicePlan);
            String requestJsonStr = gson.toJson(params);
            log.info("Restoring: " + requestJsonStr);
            String responseJsonStr = httpClient.post("m2m/v1/devices/actions/restore", requestJsonStr, headers);
            log.info("Got this: " + responseJsonStr);
            VerizonPostponedResponse response = gson.fromJson(responseJsonStr, VerizonPostponedResponse.class);
            if (response.getRequestId() == null || response.getErrorCode() != null) {
                throw new VerizonApiException("Restore failed. Response: " + responseJsonStr,
                        VerizonErrorCode.fromStringError(response.getErrorCode()),
                        BASE_API_URL + "m2m/v1/devices/actions/restore", response);
            }
            return response.getRequestId();
        } catch (HttpClientException e) {
        	  log.error("Failed to restore Verizon devices", e);
            throw new UnavailableException("Failed to restore Verizon devices. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public String deactivate(@Nullable VerizonDeviceId[] devices, @Nullable String accountName, @Nullable String servicePlan, @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token.getApiToken());
            headers.put("VZ-M2M-Token", token.getSessionToken());
            headers.put("Content-Type", "application/json");
            headers.put("Accept", "application/json");
            Map<String, Object> params = new HashMap<>();
            Object[] deviceIds = new Object[devices.length];
            for (int i=0; i < devices.length; i++) {
	              Map<String, Object> innerDeviceIds = new HashMap<>();
	              innerDeviceIds.put("deviceIds", new Object[]{devices[i]});
            		deviceIds[i] = innerDeviceIds;
            }
            params.put("devices", deviceIds);
            params.put("reasonCode", "FF");
            params.put("accountName", accountName);
            params.put("servicePlan", servicePlan);
            params.put("etfWaiver", true);
            String requestJsonStr = gson.toJson(params);
            log.info("Deactivating: " + requestJsonStr);
            String responseJsonStr = httpClient.post("m2m/v1/devices/actions/deactivate", requestJsonStr, headers);
            log.info("Got this: " + responseJsonStr);
            VerizonPostponedResponse response = gson.fromJson(responseJsonStr, VerizonPostponedResponse.class);
            if (response.getRequestId() == null || response.getErrorCode() != null) {
                throw new VerizonApiException("Deactivate failed. Response: " + responseJsonStr,
                        VerizonErrorCode.fromStringError(response.getErrorCode()),
                        BASE_API_URL + "m2m/v1/devices/actions/deactivate", response);
            }
            return response.getRequestId();
        } catch (HttpClientException e) {
        		log.error("Failed to deactivate Verizon devices", e);
            throw new UnavailableException("Failed to deactivate Verizon devices. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public VerizonListResult list(@Nullable VerizonDeviceId device, @Nullable String accountName, @Nullable String servicePlan, @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token.getApiToken());
            headers.put("VZ-M2M-Token", token.getSessionToken());
            headers.put("Content-Type", "application/json");
            headers.put("Accept", "application/json");
            Map<String, Object> params = new HashMap<>();
            params.put("deviceId", device);
            params.put("accountName", accountName);
            params.put("servicePlan", servicePlan);
            String requestJsonStr = gson.toJson(params);
            log.info("Listing: " + requestJsonStr);
            String responseJsonStr = httpClient.post("m2m/v1/devices/actions/list", requestJsonStr, headers);
            log.info("Got this: " + responseJsonStr);
            VerizonListResult response = gson.fromJson(responseJsonStr, VerizonListResult.class);
            if (response.getErrorCode() != null) {
                throw new VerizonApiException("List failed. Response: " + responseJsonStr,
                        VerizonErrorCode.fromStringError(response.getErrorCode()),
                        BASE_API_URL + "m2m/v1/devices/actions/list", response);
            }
            return response;
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to list Verizon devices. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public VerizonRegisterCallbackResult registerCallback(@Nonnull String accountName,
                                                          @Nonnull VerizonCallbackService name,
                                                          @Nonnull String url,
                                                          @Nullable String username,
                                                          @Nullable String password,
                                                          @Nonnull VerizonToken token)
            throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token.getApiToken());
            headers.put("VZ-M2M-Token", token.getSessionToken());
            headers.put("Content-Type", "application/json");
            headers.put("Accept", "application/json");
            Map<String, Object> params = new HashMap<>();
            params.put("name", name);
            params.put("url", url);
            params.put("username", username);
            params.put("password", password);
            String requestJsonStr = gson.toJson(params);
            String responseJsonStr = httpClient.post("m2m/v1/callbacks/" + accountName, requestJsonStr, headers);
            VerizonRegisterCallbackResult response = gson.fromJson(responseJsonStr, VerizonRegisterCallbackResult.class);
            if (response.getErrorCode() != null) {
                throw new VerizonApiException("registerCallback() failed. Response: " + responseJsonStr,
                        VerizonErrorCode.fromStringError(response.getErrorCode()),
                        BASE_API_URL + "m2m/v1/callbacks/" + accountName, response);
            }
            return response;
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to registerCallback() for Verizon. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public VerizonRegisterCallbackResult unRegisterCallback(@Nonnull String accountName,
                                                            @Nonnull VerizonCallbackService name,
                                                            @Nonnull VerizonToken token)
            throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token.getApiToken());
            headers.put("VZ-M2M-Token", token.getSessionToken());
            headers.put("Content-Type", "application/x-www-form-urlencoded");
            headers.put("Accept", "application/json");
            String responseJsonStr = httpClient.delete("m2m/v1/callbacks/" + accountName + "/name/" + name.getKind(), headers);
            VerizonRegisterCallbackResult response = gson.fromJson(responseJsonStr, VerizonRegisterCallbackResult.class);
            if (response.getErrorCode() != null) {
                throw new VerizonApiException("unRegisterCallback() failed. Response: " + responseJsonStr,
                        VerizonErrorCode.fromStringError(response.getErrorCode()),
                        BASE_API_URL + "m2m/v1/callbacks/" + accountName + "/name/" + name.getKind(), response);
            }
            return response;
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to unRegisterCallback() for Verizon. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public VerizonDeviceLocationResult[] location(@Nonnull VerizonDeviceLocationId[] devices,
                                                  @Nonnull String accountName,
                                                  String accuracyMode,
                                                  String cacheMode,
                                                  @Nonnull VerizonToken token)
            throws VerizonApiException, UnavailableException {
        try {
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization", "Bearer " + token.getApiToken());
            headers.put("VZ-M2M-Token", token.getSessionToken());
            headers.put("Content-Type", "application/json");
            headers.put("Accept", "application/json");
            Map<String, Object> params = new HashMap<>();
            params.put("accountName", accountName);
            params.put("accuracyMode", accuracyMode);
            params.put("cacheMode", cacheMode);
            params.put("deviceList", devices);
            String requestJsonStr = gsonLoc.toJson(params);
            String responseJsonStr = httpClient.post(LOCATIONS_PATH, requestJsonStr, headers);
            switch (responseJsonStr.charAt(0)) {
            case '{':
                VerizonErrorResponse response = gsonLoc.fromJson(responseJsonStr, VerizonErrorResponse.class);
                if (response.getErrorCode() != null) {
                    throw new VerizonApiException("Locations failed. Response: " + responseJsonStr,
                            VerizonErrorCode.fromStringError(response.getErrorCode()),
                            BASE_API_URL + LOCATIONS_PATH, response);
                }
                throw new VerizonApiException("Unexpected locations response: " + responseJsonStr,
                        VerizonErrorCode.REQUEST_FAILED_UNEXPECTED,
                        BASE_API_URL + LOCATIONS_PATH, response);
            case '[':
                VerizonDeviceLocationResult[] locResponse = gsonLoc.fromJson(responseJsonStr, VerizonDeviceLocationResult[].class);
                return locResponse;
            default:
                throw new VerizonApiException("Unexpected locations response: " + responseJsonStr,
                        VerizonErrorCode.REQUEST_FAILED_UNEXPECTED,
                        BASE_API_URL + LOCATIONS_PATH, null);
            }
        } catch (HttpClientException e) {
            throw new UnavailableException("Failed to locate Verizon devices. Msg: " + e.getMessage(), e,
                    ModemApiProvider.VERIZON);
        }
    }

    @Nonnull
    @Override
    public VerizonDeviceLocationResult[] location(@Nonnull VerizonDeviceLocationId device,
                                                  @Nonnull String accountName,
                                                  String accuracyMode,
                                                  String cacheMode,
                                                  @Nonnull VerizonToken token)
            throws VerizonApiException, UnavailableException {
        VerizonDeviceLocationId[] devices = new VerizonDeviceLocationId[1];
        devices[0] = device;
        return location(devices, accountName, accuracyMode, cacheMode, token);
    }
}
