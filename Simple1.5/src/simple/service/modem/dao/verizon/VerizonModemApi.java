package simple.service.modem.dao.verizon;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import simple.service.modem.dao.generic.exception.UnavailableException;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceId;
import simple.service.modem.dao.verizon.dto.callback.VerizonCallbackService;
import simple.service.modem.dao.verizon.dto.callback.VerizonRegisterCallbackResult;
import simple.service.modem.dao.verizon.dto.list.VerizonDevice;
import simple.service.modem.dao.verizon.dto.list.VerizonListResult;
import simple.service.modem.dao.verizon.dto.location.VerizonDeviceLocationId;
import simple.service.modem.dao.verizon.dto.location.VerizonDeviceLocationResult;
import simple.service.modem.dao.verizon.dto.login.VerizonToken;
import simple.service.modem.dao.verizon.exception.VerizonApiException;

/**
 * Public interface of Verizon modem
 */
public interface VerizonModemApi {
    @Nonnull
    String obtainApiToken(@Nonnull String appKey,
                          @Nonnull String appSecret) throws VerizonApiException, UnavailableException;

    @Nonnull
    VerizonToken login(@Nonnull String user,
                       @Nonnull String password,
                       @Nonnull String apiToken) throws VerizonApiException, UnavailableException;

    void logout(@Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

    @Nonnull
    String activate(@Nonnull VerizonDevice[] devices,
                    @Nullable String accountName,
                    @Nonnull String servicePlan,
                    @Nonnull String mdnZipCode,
                    @Nonnull String ipPoolName,
                    @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

    @Nonnull
    String suspend(@Nullable VerizonDeviceId[] devices,
                   @Nullable String accountName,
                   @Nullable String servicePlan,
                   @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

    @Nonnull
    String restore(@Nullable VerizonDeviceId[] devices,
                   @Nullable String accountName,
                   @Nullable String servicePlan,
                   @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

    @Nonnull
    String deactivate(@Nullable VerizonDeviceId[] devices,
                      @Nullable String accountName,
                      @Nullable String servicePlan,
                      @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

    @Nonnull
    VerizonListResult list(@Nullable VerizonDeviceId device,
                           @Nullable String accountName,
                           @Nullable String servicePlan,
                           @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

    @Nonnull
    VerizonRegisterCallbackResult registerCallback(@Nonnull String accountName,
                                                   @Nonnull VerizonCallbackService name,
                                                   @Nonnull String url,
                                                   @Nullable String username,
                                                   @Nullable String password,
                                                   @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

    @Nonnull
    VerizonRegisterCallbackResult unRegisterCallback(@Nonnull String accountName,
                                                     @Nonnull VerizonCallbackService name,
                                                     @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

    @Nonnull
    VerizonDeviceLocationResult[] location(@Nonnull VerizonDeviceLocationId[] devices,
                           @Nonnull  String accountName,
                           @Nullable String accuracyMode,
                           @Nullable String cacheMode,
                           @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

    @Nonnull
    VerizonDeviceLocationResult[] location(@Nonnull VerizonDeviceLocationId device,
                           @Nonnull  String accountName,
                           @Nullable String accuracyMode,
                           @Nullable String cacheMode,
                           @Nonnull VerizonToken token) throws VerizonApiException, UnavailableException;

}
