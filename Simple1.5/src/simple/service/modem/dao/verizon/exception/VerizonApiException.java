package simple.service.modem.dao.verizon.exception;

import simple.service.modem.dao.verizon.dto.error.VerizonErrorCode;
import simple.service.modem.dao.verizon.dto.error.VerizonErrorResponse;

/**
 * Generic exception for all Verizon Modem API calls.
 * It informs about not critical(i.e. expected) errors during API call.
 */
public class VerizonApiException extends Exception {
    private static final long serialVersionUID = 2151327492945465319L;

    private VerizonErrorCode errorCode;
    private String errorUrl;
    private VerizonErrorResponse errorResponse;

    public VerizonApiException(VerizonErrorCode errorCode, String errorUrl, VerizonErrorResponse errorResponse) {
        this.errorCode = errorCode;
        this.errorUrl = errorUrl;
        this.errorResponse = errorResponse;
    }

    public VerizonApiException(String message, VerizonErrorCode errorCode, String errorUrl, VerizonErrorResponse errorResponse) {
        super(message);
        this.errorCode = errorCode;
        this.errorUrl = errorUrl;
        this.errorResponse = errorResponse;
    }

    public VerizonApiException(String message, Throwable cause, VerizonErrorCode errorCode, String errorUrl, VerizonErrorResponse errorResponse) {
        super(message, cause);
        this.errorCode = errorCode;
        this.errorUrl = errorUrl;
        this.errorResponse = errorResponse;
    }

    public VerizonApiException(Throwable cause, VerizonErrorCode errorCode, String errorUrl, VerizonErrorResponse errorResponse) {
        super(cause);
        this.errorCode = errorCode;
        this.errorUrl = errorUrl;
        this.errorResponse = errorResponse;
    }

    public VerizonApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, VerizonErrorCode errorCode, String errorUrl, VerizonErrorResponse errorResponse) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
        this.errorUrl = errorUrl;
        this.errorResponse = errorResponse;
    }
    
    public VerizonErrorResponse getErrorResponse() {
    	return this.errorResponse;
    }
}
