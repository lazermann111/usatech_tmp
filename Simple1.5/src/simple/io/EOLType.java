package simple.io;

import simple.bean.ConvertUtils;

public enum EOLType {
	NONE(""), CR("\r"), LF("\n"), CRLF("\r\n"), EOF(null);
	private final String eol;

	private EOLType(String eol) {
		this.eol = eol;
	}

	public String getEol() {
		return eol;
	}

	public static EOLType getEOLType(String eol) {
		for(EOLType eolType : values())
			if(ConvertUtils.areEqual(eolType.getEol(), eol))
				return eolType;
		return null;
	}
}