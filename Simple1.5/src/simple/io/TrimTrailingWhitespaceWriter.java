package simple.io;

import java.io.Writer;

public class TrimTrailingWhitespaceWriter extends TrimTrailingWriter {

	public TrimTrailingWhitespaceWriter(Writer out) {
		super(out);
	}

	@Override
	protected boolean trimIfTrailing(char ch) {
		return Character.isWhitespace(ch);
	}

}
