/**
 *
 */
package simple.io;

import java.io.Flushable;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

/**
 * @author Brian S. Krug
 *
 */
public class EncodingAppendable implements Appendable, Flushable {
	protected final CharsetEncoder encoder;
	protected final ByteBuffer target;
	protected final char[] oneCharArray = new char[1];

	public EncodingAppendable(CharsetEncoder encoder, ByteBuffer target) {
		super();
		this.encoder = encoder;
		this.target = target;
	}

	public EncodingAppendable(Charset charset, ByteBuffer target) {
		this(charset.newEncoder(), target);
	}

	/**
	 * @see java.lang.Appendable#append(java.lang.CharSequence)
	 */
	public Appendable append(CharSequence csq) {
		encoder.encode(CharBuffer.wrap(csq), target, false);
		return this;
	}

	/**
	 * @see java.lang.Appendable#append(char)
	 */
	public Appendable append(char c) {
		oneCharArray[0] = c;
		encoder.encode(CharBuffer.wrap(oneCharArray), target, false);
		return this;
	}

	/**
	 * @see java.lang.Appendable#append(java.lang.CharSequence, int, int)
	 */
	public Appendable append(CharSequence csq, int start, int end) {
		encoder.encode(CharBuffer.wrap(csq, start, end), target, false);
		return this;
	}

	/**
	 * @see java.io.Flushable#flush()
	 */
	public void flush() {
		encoder.flush(target);
	}
}
