package simple.io;

import java.io.IOException;
import java.io.InputStream;

import simple.text.StringUtils;

public class InputStreamByteInput extends AbstractByteInput {
	protected final InputStream in;
	protected int resetLimit = 4096;
	protected String hexString;
	public InputStreamByteInput(InputStream in) {
		super();
		this.in = in;
		if(in.markSupported())
			in.mark(resetLimit);
	}

	public int length() throws IOException {
		return position + in.available(); // not the best way but its what we've got
	}

	public boolean isResettable() {
		return in.markSupported() && resetLimit >= position;
	}

	@Override
	public void reset() throws IOException {
		if(resetLimit < position) throw new IOException("Reset not supported for input lengths > " + resetLimit);
		in.reset();
		super.reset();
	}

	@Override
	public String toString() {
		if(hexString == null) {
			try {
				hexString = constructHexString();
			} catch(IOException e) {
				return "<EXCEPTION>";
			}
		}
		return hexString;
	}

	protected String constructHexString() throws IOException {
		int length = length();
		if(length == 0) return "<EMPTY>";
		if(length > 0 && length < resetLimit && in.markSupported()) {
			in.reset();
			byte[] buf;
			if(readBuffer.length < length) {
				buf = new byte[length];
			} else
				buf = readBuffer;
			int len = in.read(buf, 0, length);
			String s = StringUtils.toHex(buf, 0, len);
			in.reset();
			in.skip(position);
			return s;
		} else
			return "<INDETERMINANT>";

	}

	@Override
	protected int read() throws IOException {
		return in.read();
	}

	@Override
	protected int read(byte[] buf, int offset, int length) throws IOException {
		return in.read(buf, offset, length);
	}
}
