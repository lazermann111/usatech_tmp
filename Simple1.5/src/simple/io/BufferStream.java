package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public interface BufferStream extends BinaryStream {
	public int size();

	/** Returns an OutputStream to write the data access by the InputStream
	 * @return The OutputStream
	 */
	public OutputStream getOutputStream() throws IOException ;

	public void clear() throws IOException ;

	/** Copies bytes from this BufferStream to the ByteBuffer. The number of bytes copied is return and
	 *  will be the smaller of byteBuffer.remaining() or (BufferStream.size() - offset)
	 * @param bb
	 * @param offset The offset in the BufferStream from which to start copying bytes
	 * @return
	 */
	public int putInto(ByteBuffer byteBuffer, int offset) throws IOException ;

	public long copyInto(OutputStream output) throws IOException;

}