/*
 * App.java
 *
 * Created on January 7, 2002, 12:44 PM
 */

package simple.io;

/**
 * Manages properties for an application by looking in the classpath for a
 * properties file named the same as the application name.
 *
 * @author  Brian S. Krug
 * @version
 */
import java.io.IOException;
import java.util.Properties;

public class App {
    protected static final java.util.Map<String,Properties> cache = new java.util.HashMap<String,Properties>();
    protected static Properties master = null;
    protected static String masterAppName = null;
    public static interface PropertiesLoader {
    	public Properties loadProperties(String appName) throws IOException ;
    }
    protected static PropertiesLoader defaultPropertiesLoader  = new PropertiesLoader() {
    	/**
         * Loads the properties from a properties file for the named application.
         */
        public Properties loadProperties(String appName) throws IOException {
            Properties props = new java.util.Properties();
            if(appName != null) {
                //try everything possible to find the file
                final String filename = appName.replace('.', java.io.File.separatorChar) + ".properties";
                java.io.InputStream is = App.class.getClassLoader().getResourceAsStream(filename);
                if(is== null) {
                    is = ClassLoader.getSystemResourceAsStream(filename);
                }
                if(is == null) {
                    System.err.println("*** Could not find properties file, '" + filename + "' in the classpath '" + System.getProperty("java.class.path") + "'");
                    java.io.File f = new java.io.File("").getAbsoluteFile();
                    java.io.File[] files = findFile(filename,f);
                                /*if(files.length == 0) {
                                        while(f.getParentFile() != null) f = f.getParentFile();
                                        files = findFile(filename,f);
                                }*/
                    if(files.length == 0) {
                    	System.err.println("*** Could not find properties file, '" + filename + "' in '" + f.getAbsolutePath() + "'");
                    } else {
	                    System.out.println("***Using properties file, '" + files[0].getAbsolutePath() + "'");
	                    is = new java.io.FileInputStream(files[0]);
                    }
                }
                if(is != null) {
                    props.load(is);
                    props.setProperty("alert_from", appName);
                    //Log.loadProps(props);
                }
            }
            return props;
        }
    };
    protected static PropertiesLoader propertiesLoader = defaultPropertiesLoader;
    private App() {
    }

    /**
     * Returns the application name for this VM
     */
    public static String getMasterAppName() {
        return masterAppName;
    }

    /**
     * Sets the application name for this VM
     */
    public static void setMasterAppName(String masterAppName) {
        App.masterAppName = masterAppName;
        master = null;
    }

    /**
     * Returns properties from a properties file for the named application
     */
    public static Properties getProperties(String appName) throws IOException {
        return getProperties(appName, false);
    }

    /**
     * Returns properties from a properties file for the named application. Uses
     * and sets the master app's properties if standAlone is false
     */
    public static Properties getProperties(String appName, boolean standAlone) throws IOException {
        Properties p = null;
        if(standAlone) {
            p = cache.get(appName);
            if(p != null) return p;
        } else if(master != null) return master;
        return reloadProperties(appName, standAlone);
    }

    /**
     * Reloads the properties for the given application name
     */
    public static Properties reloadProperties(String appName) throws IOException {
        return reloadProperties(appName, false);
    }

    /**
     * Reloads the properties from a properties file for the named application. Uses
     * and sets the master app's properties if standAlone is false
     */
    public static Properties reloadProperties(String appName, boolean standAlone) throws IOException {
        if(!standAlone && masterAppName != null) appName = masterAppName;
        Properties p = getPropertiesLoader().loadProperties(appName);
        if(standAlone) cache.put(appName, p);
        else {
            master = p;
            masterAppName = appName;
        }
        return p;
    }

    /**
     * Finds all files that matches the name in the directory tree of the specified
     * directory
     */
    public static java.io.File[] findFile(final String filename, java.io.File dir) throws IOException {
        java.io.File[] retVal = new java.io.File[0];
        java.io.FilenameFilter filefilter = new java.io.FilenameFilter() {
            public boolean accept(java.io.File dir,String name) { return name.equalsIgnoreCase(filename);}
        };
        java.io.FileFilter dirfilter = new java.io.FileFilter() {
            public boolean accept(java.io.File pathname) { return pathname.isDirectory();}
        };

        java.io.File[] files = dir.listFiles(filefilter);
        if(files != null && files.length > 0) {
            java.io.File[] tmp = new java.io.File[retVal.length + files.length];
            System.arraycopy(retVal,0,tmp,0,retVal.length);
            System.arraycopy(files,0,tmp,retVal.length,files.length);
            retVal = tmp;
        }
        java.io.File[] dirs = dir.listFiles(dirfilter);
        if(dirs != null) {
            for(int i = 0; i < dirs.length; i++) {
                files = findFile(filename,dirs[i]);
                if(files != null &&files.length > 0) {
                    java.io.File[] tmp = new java.io.File[retVal.length + files.length];
                    System.arraycopy(retVal,0,tmp,0,retVal.length);
                    System.arraycopy(files,0,tmp,retVal.length,files.length);
                    retVal = tmp;
                }
            }
        }

        return retVal;
    }

	public static PropertiesLoader getPropertiesLoader() {
		return propertiesLoader;
	}

	public static void setPropertiesLoader(PropertiesLoader propertiesLoader) {
		if(propertiesLoader == null)
			App.propertiesLoader = defaultPropertiesLoader;
		else
			App.propertiesLoader = propertiesLoader;
	}


}
