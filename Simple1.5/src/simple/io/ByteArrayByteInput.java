package simple.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;

import simple.text.StringUtils;

public class ByteArrayByteInput implements ByteInput {
	protected final byte[] bytes;
	protected int position;
	protected String hexString;
	public ByteArrayByteInput(byte[] bytes) {
		super();
		this.bytes = bytes;
	}

	public boolean isResettable() {
		return true;
	}

	/**
	 * @see simple.io.ByteInput#readBytes(int)
	 */
	public String readBytes(int length) throws IOException {
		if(position + length > bytes.length)
			throw new EOFException();
		String s = new String(bytes, position, length);
		position += length;
		return s;
	}

	/**
	 * @see simple.io.ByteInput#readChars(int)
	 */
	public String readChars(int length) throws IOException {
		char[] buf = new char[length];
		for(int i = 0; i < length; i++)
			buf[i] = readChar();
		return new String(buf, 0, length);
	}

	/**
	 * @see simple.io.ByteInput#readChar()
	 */
	public char readChar() throws IOException {
		return (char)readUnsignedShort();
    }

	/**
	 * @see simple.io.ByteInput#readByte()
	 */
	public byte readByte() throws IOException {
		if(position + 1 > bytes.length)
			throw new EOFException();
		return bytes[position++];
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedByte()
	 */
	public int readUnsignedByte() throws IOException {
		return readByte() & 0xff;
	}

	/**
	 * @see simple.io.ByteInput#readShort()
	 */
	public short readShort() throws IOException {
		return (short)readUnsignedShort();
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedShort()
	 */
	public int readUnsignedShort() throws IOException {
		if(position + 2 > bytes.length)
			throw new EOFException();
		return (((bytes[position++]& 0xff) << 8) + ((bytes[position++]& 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readByteAsChar()
	 */
	public char readByteAsChar() throws IOException {
		return (char) readUnsignedByte();
	}

	/**
	 * @see simple.io.ByteInput#readInt()
	 */
	public int readInt() throws IOException {
		if(position + 4 > bytes.length)
			throw new EOFException();
		return (((bytes[position++]& 0xff) << 24) + ((bytes[position++]& 0xff) << 16) + ((bytes[position++]& 0xff) << 8) + ((bytes[position++]& 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedInt()
	 */
	public long readUnsignedInt() throws IOException {
		if(position + 4 > bytes.length)
			throw new EOFException();
		return (((long) (bytes[position++] & 0xff) << 24) + ((bytes[position++]& 0xff) << 16) + ((bytes[position++]& 0xff) << 8) + ((bytes[position++]& 0xff) << 0));
	}


	/**
	 * @see simple.io.ByteInput#readLong()
	 */
	public long readLong() throws IOException {
		if(position + 8 > bytes.length)
			throw new EOFException();
		return (((long) bytes[position++] << 56) + ((long) (bytes[position++] & 0xff) << 48)
				+ ((long) (bytes[position++] & 0xff) << 40) + ((long) (bytes[position++] & 0xff) << 32)
				+ ((long) (bytes[position++] & 0xff) << 24) + ((bytes[position++] & 0xff) << 16)
				+ ((bytes[position++] & 0xff) << 8) + ((bytes[position++] & 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readFloat()
	 */
	public float readFloat() throws IOException {
		return Float.intBitsToFloat(readInt());
	}

	/**
	 * @see simple.io.ByteInput#readDouble()
	 */
	public double readDouble() throws IOException {
		return Double.longBitsToDouble(readLong());
	}

	public int length() throws IOException {
		return bytes.length;
	}

	public int position() {
		return position;
	}
	public void reset() throws IOException {
		position = 0;
	}

	@Override
	public String toString() {
		if(hexString == null) {
			hexString = constructHexString();
		}
		return hexString;
	}

	protected String constructHexString() {
		if(bytes.length == 0) return "<EMPTY>";
		return StringUtils.toHex(bytes);
	}
	public int copyInto(OutputStream output) throws IOException {
		output.write(bytes);
		output.flush();
		return bytes.length;
	}

	@Override
	public int copyInto(ByteOutput output) throws IOException {
		output.write(bytes);
		output.flush();
		return bytes.length;
	}
	public void readBytes(byte[] buffer, int offset, int length) throws IOException {
		if(length > bytes.length - position)
			throw new EOFException();
		System.arraycopy(bytes, position, buffer, offset, length);
		position += length;
	}
}
