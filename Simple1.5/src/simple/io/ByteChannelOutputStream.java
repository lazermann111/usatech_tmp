/*
 * Created on Dec 8, 2005
 *
 */
package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class ByteChannelOutputStream extends OutputStream {
    protected WritableByteChannel channel;
    protected ByteBuffer bufferOne;
    protected ByteBuffer lastBuffer;
    protected byte[] lastArray;
    public ByteChannelOutputStream(WritableByteChannel channel) {
        super();
        this.channel = channel;
    }

    @Override
    public void write(int b) throws IOException {
    	if(bufferOne == null)
    		bufferOne = ByteBuffer.allocate(1);
    	bufferOne.rewind();
        bufferOne.put(0, (byte)b);
		while(bufferOne.hasRemaining())
			channel.write(bufferOne);
    }

    @Override
    public void write(byte[] b, int offset, int length) throws IOException {
    	ByteBuffer bb;
        if(b == lastArray) {
        	bb = lastBuffer;
        	bb.position(offset);
        	bb.limit(offset + length);
        } else {
        	bb =  ByteBuffer.wrap(b, offset, length);
        	lastArray = b;
        	lastBuffer = bb;
        }
		while(bb.hasRemaining())
			channel.write(bb);
    }

    @Override
    public void close() throws IOException {
    	//channel.close(); should this outputstream have control over this?
    	channel = null;
    }
}
