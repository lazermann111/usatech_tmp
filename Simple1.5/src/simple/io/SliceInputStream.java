package simple.io;

import java.io.IOException;
import java.io.InputStream;

public class SliceInputStream extends InputStream {
	protected final InputStream delegate;
	protected long pos = -1;
	protected long endPos = -1;
	protected long markPos = -1;

	public SliceInputStream(InputStream delegate, long offset, long length) throws IOException {
		super();
		this.delegate = delegate;
		this.pos = offset;
		this.endPos = offset + length;
		if(offset > 0)
			delegate.skip(offset);
	}

	@Override
	public int read() throws IOException {
		if(pos >= endPos) {
			return -1;
		}
		int r = delegate.read();
		if(r >= 0)
			pos++;
		return r;
	}

	@Override
	public int available() throws IOException {
		return Math.max((int)(endPos - pos), delegate.available());
	}

	@Override
	public void close() throws IOException {
		delegate.close();
	}

	@Override
	public void mark(int readlimit) {
		delegate.mark(readlimit);
		markPos = pos;
	}

	@Override
	public boolean markSupported() {
		return delegate.markSupported();
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if(pos >= endPos) {
			return -1;
		}
		int r = delegate.read(b, off, Math.max((int)(endPos-pos), len));
		if(r >= 0)
			pos += r;
		return r;
	}

	@Override
	public void reset() throws IOException {
		delegate.reset();
		pos = markPos;
	}

	@Override
	public long skip(long n) throws IOException {
		long s = delegate.skip(n);
		pos += s;
		return s;
	}
}
