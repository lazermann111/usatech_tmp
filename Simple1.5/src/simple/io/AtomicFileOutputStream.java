package simple.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AtomicFileOutputStream extends FileOutputStream implements Abortable {
	protected final File file;
	protected final File temp;
	protected static File createTemp(File orig, boolean copy) throws IOException {
		File temp = File.createTempFile(orig.getName() + '-', ".tmp", orig.getParentFile());
		if(copy && orig.exists())
			IOUtils.copy(orig, temp);
		return temp;
	}

	protected AtomicFileOutputStream(File file, File temp, boolean append) throws IOException {
		super(temp, append);
		this.file = file;
		this.temp = temp;

	}
	public AtomicFileOutputStream(File file, boolean append) throws IOException {
		this(file, createTemp(file, append), append);
	}

	public AtomicFileOutputStream(File file) throws IOException {
		this(file, false);
	}

	public AtomicFileOutputStream(String file, boolean append) throws IOException {
		this(new File(file), append);
	}

	public AtomicFileOutputStream(String file) throws IOException {
		this(file, false);
	}

	@Override
	public void close() throws IOException {
		super.close();
		if(!temp.renameTo(file))
			throw new IOException("Could not copy temp file to '" + file.getAbsolutePath() + "'");
	}

	@Override
	public void abort() throws IOException {
		try {
			super.close();
		} finally {
			if(!temp.delete())
				throw new IOException("Could not delete temp file '" + temp.getAbsolutePath() + "'");
		}
	}
}
