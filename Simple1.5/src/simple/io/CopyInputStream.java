package simple.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CopyInputStream extends FilterInputStream {
	protected OutputStream out;
	public CopyInputStream(InputStream in, OutputStream out) {
		super(in);
		this.out = out;
	}
	@Override
	public int read() throws IOException {
		int ch = super.read();
		if(ch >= 0) out.write(ch);
		return ch;
	}
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int count = super.read(b, off, len);
		if(count > 0) out.write(b, off, count);
		return count;
	}
}
