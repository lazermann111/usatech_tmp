package simple.io.versioning;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.text.StringUtils;

public class GitClient {
	private static final Log log = Log.getLog();
	protected String gitRootDir;
	protected String gitHost;
	protected static final char NEWLINE = '\n';
	protected final JSch ssh = new JSch();
	protected Session session;

	public static class GitResult extends InputStream {
		protected final Session session;
		protected final InputStream in;
		protected final long fileSize;
		protected long pos;

		public GitResult(Session session, InputStream in, long fileSize) {
			super();
			this.session = session;
			this.in = in;
			this.fileSize = fileSize;
		}

		public void close() {
			try {
				session.disconnect();
				;
			} finally {
				pos = Long.MAX_VALUE;
			}
		}

		@Override
		public int available() throws IOException {
			if (pos == Long.MAX_VALUE)
				return 0;
			long remaining = fileSize - pos;
			if (remaining > Integer.MAX_VALUE)
				return Integer.MAX_VALUE;
			return (int) remaining;
		}

		protected void readAfterContent() throws IOException {
			String line;
			StringBuilder message = new StringBuilder();
			while ((line = readLine(in)) != null) {
				if ("ok".equals(line)) {
					pos++;
					return;
				} else if (line.startsWith("error")) {
					throw new IOException("Could not pull file from CVS because: " + message.toString());
				} else if (line.startsWith("M ") || line.startsWith("E ")) {
					message.append(StringUtils.substringAfter(line, " "));
				}
			}
		}

		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			if (pos < fileSize) {
				int n = in.read(b, off, Math.min(available(), len));
				if (n < 0) {
					if (pos != fileSize)
						throw new IOException("InputStream content does not match expected byte count");
					readAfterContent();
				} else
					pos += n;
				return n;
			}
			if (pos == fileSize) {
				readAfterContent();
			}
			return -1;
		}

		@Override
		public long skip(long n) throws IOException {
			if (pos >= fileSize)
				return -1;
			n = in.skip(Math.min(fileSize - pos, n));
			if (n > 0)
				pos += n;
			return n;
		}

		@Override
		public int read() throws IOException {
			if (pos < fileSize) {
				int n = in.read();
				if (n < 0) {
					if (pos != fileSize)
						throw new IOException("InputStream content does not match expected byte count");
					readAfterContent();
				} else
					pos++;
				return n;
			}
			if (pos == fileSize) {
				readAfterContent();
			}
			return -1;
		}
	};

	public GitClient() throws JSchException {
		ssh.setKnownHosts(System.getProperty("user.home", ".") + "/knownhosts.lst");
	}

	public GitClient(String gitHost, String gitRootDir) throws JSchException {
		this();
		this.gitHost = gitHost;
		this.gitRootDir = gitRootDir;

	}

	public boolean isConnected() throws JSchException {
		return session.isConnected();
	}

	public void connect(String username, String password) throws IOException, JSchException {
		if (session == null || !session.isConnected()) {
			session = ssh.getSession(username, gitHost);
			session.setPassword(password);
			session.connect();
		}
	}

	public void close() throws JSchException {
		session.disconnect();
	}

	public InputStream export(String path, String revision) throws IOException, InterruptedException, JSchException {
		if (!isConnected())
			throw new IOException("GitSession is not connected. Call connect() first");
		
		long fileSize = getFileSize(path, revision);

		String command = "cd " + gitRootDir + " && exec git archive " + revision + " " + path + " | tar -xO " + path;

		Channel channel = session.openChannel("exec");
		((ChannelExec) channel).setCommand(command);

		channel.setInputStream(null);
		InputStream in = new DataInputStream(channel.getInputStream());
		channel.connect();

		while (channel.getExitStatus() == -1) {
			if (in.available() > 0) {
				log.info("Pulling file " + path + " (" + revision + ") from Git of " + fileSize + " bytes");
				return new GitResult(session, in, fileSize);
			}
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				log.error(e);
			}
		}

		throw new IOException("End of stream not expected");
	}

	private long getFileSize(String path, String revision) throws IOException, JSchException {
		String cmdResult = executeCmd(
				"cd " + gitRootDir + " && exec git rev-list -1 " + revision + " -- " + path + "  | sed -e 's/$/:"
						+ StringUtils.escape(path, "/".toCharArray(), '\\') + "/g' | git cat-file --batch-check",
				"");
		log.debug(cmdResult);
		long fileSize;
		try {
			fileSize = ConvertUtils.getLong(cmdResult.split("\\s")[2]);
		} catch (ConvertException e) {
			throw new IOException("Could not parse file size", e);
		}
		return fileSize;
	}

	protected String executeCmd(String cmd, String path, String... arguments) throws IOException, JSchException {
		if (!isConnected())
			throw new IOException("GitSession is not connected. Call connect() first");
		log.debug(cmd);
		StringBuilder message = new StringBuilder();
		StringBuilder cmds = new StringBuilder(cmd);
		if (arguments != null)
			for (String a : arguments) {
				if (!StringUtils.isBlank(a))
					cmds.append(a).append(" ");
			}

		Channel channel = session.openChannel("exec");
		((ChannelExec) channel).setCommand(cmds.toString());

		channel.setInputStream(null);

		((ChannelExec) channel).setErrStream(System.err);

		InputStream in = new DataInputStream(channel.getInputStream());
		channel.connect();

		byte[] tmp = new byte[1024];
		while (true) {
			while (in.available() > 0) {
				int i = in.read(tmp, 0, 1024);
				if (i < 0)
					break;
				message.append(new String(tmp, 0, i));
			}
			if (channel.isClosed()) {
				if (in.available() > 0)
					continue;
				log.debug("exit-status: " + channel.getExitStatus());
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ee) {
			}
		}
		channel.disconnect();

		return message.toString().trim();
	}

	protected static String readLine(InputStream in) throws IOException {
		StringBuilder line = new StringBuilder(512);
		while (true) {
			int b = in.read();
			if (b == -1) {
				if (line.length() == 0)
					return null;
				return line.toString();
			}
			char ch = (char) b;
			if (ch == NEWLINE)
				return line.toString();
			line.append(ch);
		}
	}

	public String getGitRootDir() {
		return gitRootDir;
	}

	public String getGitHost() {
		return gitHost;
	}

	public void setGitHost(String gitHost) {
		this.gitHost = gitHost;
	}

	public void setGitRootDir(String gitRootDir) {
		this.gitRootDir = gitRootDir;
	}
}
