package simple.io.versioning;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.io.resource.sftp.SftpUtils;
import simple.text.StringUtils;

import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.session.SessionChannelClient;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.InvalidHostFileException;

public class CvsClient {
	private static final Log log = Log.getLog();
	protected String cvsRootDir;
	protected String cvsHost;
	protected static final char NEWLINE = '\n';
	protected final SshClient ssh = new SshClient();
	protected HostKeyVerification hostKeyVerification;

	public static class CvsResult extends InputStream {
		protected final SessionChannelClient sshSession;
		protected final InputStream in;
		protected final long fileSize;
		protected long pos;

		public CvsResult(SessionChannelClient sshSession, InputStream in, long fileSize) {
			super();
			this.sshSession = sshSession;
			this.in = in;
			this.fileSize = fileSize;
		}

		public void close() {
			try {
				sshSession.close();
			} catch(IOException e) {
				log.warn("Could not close cvs session", e);
			} finally {
				pos = Long.MAX_VALUE;
			}
		}
		@Override
		public int available() throws IOException {
			if(pos == Long.MAX_VALUE)
				return 0;
			long remaining = fileSize - pos;
			if(remaining > Integer.MAX_VALUE)
				return Integer.MAX_VALUE;
			return (int) remaining;
		}

		protected void readAfterContent() throws IOException {
			String line;
			StringBuilder message = new StringBuilder();
			while((line = readLine(in)) != null) {
				if("ok".equals(line)) {
					pos++;
					return;
				} else if(line.startsWith("error")) {
					throw new IOException("Could not pull file from CVS because: " + message.toString());
				} else if(line.startsWith("M ") || line.startsWith("E ")) {
					message.append(StringUtils.substringAfter(line, " "));
				}
			}
		}
		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			if(pos < fileSize) {
				int n = in.read(b, off, Math.min(available(), len));
				if(n < 0) {
					if(pos != fileSize)
						throw new IOException("InputStream content does not match expected byte count");
					readAfterContent();
				} else
					pos += n;
				return n;
			}
			if(pos == fileSize) {
				readAfterContent();
			}
			return -1;
		}

		@Override
		public long skip(long n) throws IOException {
			if(pos >= fileSize)
				return -1;
			n = in.skip(Math.min(fileSize - pos, n));
			if(n > 0)
				pos += n;
			return n;
		}
		@Override
		public int read() throws IOException {
			if(pos < fileSize) {
				int n = in.read();
				if(n < 0) {
					if(pos != fileSize)
						throw new IOException("InputStream content does not match expected byte count");
					readAfterContent();
				} else
					pos++;
				return n;
			}
			if(pos == fileSize) {
				readAfterContent();
			}
			return -1;
		}
	};

	public CvsClient() {
	}

	public CvsClient(String cvsHost, String cvsRootDir) {
		this();
		this.cvsHost = cvsHost;
		this.cvsRootDir = cvsRootDir;
	}
	
	public boolean isConnected() {
		return ssh.isConnected() && ssh.isAuthenticated();
	}

	public void connect(String username, String password) throws IOException {
		if(!ssh.isConnected())
			ssh.connect(cvsHost, getHostKeyVerification());

		boolean okay = false;
		try {
			if(!ssh.isAuthenticated())
				SftpUtils.authenticate(ssh, username, password);
			okay = true;
		} finally {
			if(!okay)
				close();
		}
	}

	public void close() {
		ssh.disconnect();
	}
		
	public InputStream export(String path, String revision) throws IOException {
		if(!isConnected())
			throw new IOException("CvsSession is not connected. Call connect() first");
		SessionChannelClient sshSession = ssh.openSessionChannel();
		if(!sshSession.executeCommand("cvs server"))
			throw new IOException("Could not start cvs server on remote host");
		InputStream in = new DataInputStream(sshSession.getInputStream());
		OutputStream out = new DataOutputStream(sshSession.getOutputStream());
		Writer writer = new OutputStreamWriter(out);
		writer.append("Root ").append(getCvsRootDir()).append(NEWLINE)
			.append("Argument -r").append(revision).append(NEWLINE)
			.append("Argument ").append(path).append(NEWLINE)
			.append("export").append(NEWLINE)
			.flush();
		StringBuilder message = new StringBuilder();
		String line;
		while((line = readLine(in)) != null) {
			if(line.startsWith("Updated ")) {
				readLine(in);
				readLine(in);
				readLine(in);
				long fileSize;
				try {
					fileSize = ConvertUtils.getLong(readLine(in));
				} catch(ConvertException e) {
					throw new IOException("Could not parse file size", e);
				}
				log.info("Pulling file " + path + " (" + revision + ") from CVS of " + fileSize + " bytes");
				return new CvsResult(sshSession, in, fileSize);
			} else if("ok".equals(line)) {
				// it shouldn't really get here
				throw new IOException("ok was read but not expected");
			} else if(line.startsWith("error")) {
				throw new IOException("Could not pull file " + path + " (" + revision + ") from CVS because: " + message.toString());
			} else if(line.startsWith("M ") || line.startsWith("E ")) {
				message.append(StringUtils.substringAfter(line, " "));
			}
		}
		throw new IOException("End of stream not expected");
	}
	
	public void tag(String path, String revision, String tag) throws IOException {
		executeCmd("rtag", path, "-F", "-r" + revision, tag);
	}    
	
	public String historyOfCommits(String path) throws IOException {
		return executeCmd("history", path, "-xARM");
	}

	protected String executeCmd(String cmd, String path, String... arguments) throws IOException {
		if(!isConnected())
			throw new IOException("CvsSession is not connected. Call connect() first");
		SessionChannelClient sshSession = ssh.openSessionChannel();
		if(!sshSession.executeCommand("cvs server"))
			throw new IOException("Could not start cvs server on remote host");
		InputStream in = new DataInputStream(sshSession.getInputStream());
		OutputStream out = new DataOutputStream(sshSession.getOutputStream());
		Writer writer = new OutputStreamWriter(out);
		writer.append("Root ").append(getCvsRootDir()).append(NEWLINE);
		if(arguments != null)
			for(String a : arguments) {
				if(!StringUtils.isBlank(a))
					writer.append("Argument ").append(a).append(NEWLINE);
			}
		writer.append("Argument ").append(path).append(NEWLINE).append(cmd).append(NEWLINE).flush();
		StringBuilder message = new StringBuilder();
		String line;
		while((line = readLine(in)) != null) {
			if(line.startsWith("Updated ")) {
				throw new IOException("'Updated ...' was read but not expected");
			} else if("ok".equals(line)) {
				return message.toString().trim();
			} else if(line.startsWith("error")) {
				throw new IOException("Could not execute '" + cmd + "' on " + path + " because: " + message.toString().trim());
			} else if(line.startsWith("M ") || line.startsWith("E ")) {
				message.append(line, 2, line.length()).append(NEWLINE);
			}
		}
		throw new IOException("End of stream not expected");
	}

	protected static String readLine(InputStream in) throws IOException {
		StringBuilder line = new StringBuilder(512);
		while(true) {
			int b = in.read();
			if(b == -1) {
				if(line.length() == 0)
					return null;
				return line.toString();
			}
			char ch = (char) b;
			if(ch == NEWLINE)
				return line.toString();
			line.append(ch);
		}
	}

	public String getCvsRootDir() {
		return cvsRootDir;
	}

	public HostKeyVerification getHostKeyVerification() throws InvalidHostFileException {
		if(hostKeyVerification == null)
			hostKeyVerification = SftpUtils.createHostKeyVerification(System.getProperty("user.home", ".") + "/knownhosts.lst");
		return hostKeyVerification;
	}

	public void setHostKeyVerification(HostKeyVerification hostKeyVerification) {
		this.hostKeyVerification = hostKeyVerification;
	}

	public String getCvsHost() {
		return cvsHost;
	}

	public void setCvsHost(String cvsHost) {
		this.cvsHost = cvsHost;
	}

	public void setCvsRootDir(String cvsRootDir) {
		this.cvsRootDir = cvsRootDir;
	}
}
