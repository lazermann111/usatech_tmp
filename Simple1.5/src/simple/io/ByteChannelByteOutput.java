/**
 *
 */
package simple.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

/**
 * @author Brian S. Krug
 *
 */
public class ByteChannelByteOutput implements ByteOutput {
	protected final WritableByteChannel byteChannel;
	protected final ByteBuffer buffer;
	
	public ByteChannelByteOutput(WritableByteChannel byteChannel, int bufferSize) {
		this.byteChannel = byteChannel;
		this.buffer = ByteBuffer.allocate(bufferSize);
	}

	protected void ensureCapacity(int additional) throws IOException {
		if(buffer.remaining() < additional) {
			flush();
			if(buffer.remaining() < additional)
				throw new IOException("Buffer too small; " + additional + " needed");
		}
	}	
    /**
	 * @see simple.io.ByteOutput#writeBytes(java.lang.String)
	 */
	public void writeBytes(String s) throws IOException {
		writeBytes(s, s.length());
	}
	/**
	 * @see simple.io.ByteOutput#writeChars(java.lang.String)
	 */
	public void writeChars(String s) throws IOException {
		writeChars(s, s.length());
	}
	/**
	 * @see simple.io.ByteOutput#writeByte(int)
	 */
    public void writeByte(int b) throws IOException {
    	ensureCapacity(1);
    	buffer.put((byte) b);
    }

	/**
	 * @see simple.io.ByteOutput#writeShort(int)
	 */
    public void writeShort(int s) throws IOException {
    	ensureCapacity(2);
    	buffer.put((byte)((s >>> 8) & 0xFF));
    	buffer.put((byte)((s >>> 0) & 0xFF));
    }

    /**
	 * @see simple.io.ByteOutput#writeChar(int)
	 */
    public void writeChar(int c) throws IOException {
    	ensureCapacity(2);
    	buffer.put((byte)((c >>> 8) & 0xFF));
    	buffer.put((byte)((c >>> 0) & 0xFF));
    }

    /**
	 * @see simple.io.ByteOutput#writeInt(int)
	 */
    public void writeInt(int i) throws IOException {
    	ensureCapacity(4);
    	buffer.put((byte)((i >>> 24) & 0xFF));
    	buffer.put((byte)((i >>> 16) & 0xFF));
    	buffer.put((byte)((i >>>  8) & 0xFF));
    	buffer.put((byte)((i >>>  0) & 0xFF));
    }

    /**
	 * @see simple.io.ByteOutput#writeLong(long)
	 */
    public void writeLong(long l) throws IOException {
    	ensureCapacity(8);
    	buffer.put((byte)(l >>> 56));
        buffer.put((byte)(l >>> 48));
        buffer.put((byte)(l >>> 40));
        buffer.put((byte)(l >>> 32));
        buffer.put((byte)(l >>> 24));
        buffer.put((byte)(l >>> 16));
        buffer.put((byte)(l >>>  8));
        buffer.put((byte)(l >>>  0));
    }

    /**
	 * @see simple.io.ByteOutput#writeFloat(float)
	 */
    public void writeFloat(float f) throws IOException {
    	writeInt(Float.floatToIntBits(f));
    }

    /**
	 * @see simple.io.ByteOutput#writeDouble(double)
	 */
    public void writeDouble(double d) throws IOException {
    	writeLong(Double.doubleToLongBits(d));
    }

    /**
	 * @see simple.io.ByteOutput#writeBytes(java.lang.String, int)
	 */
    public void writeBytes(String s, int length) throws IOException {
		writeBytes(s, 0, length);
	}

	/**
	 * @see simple.io.ByteOutput#writeBytes(java.lang.String, int, int)
	 */
	public void writeBytes(String s, int offset, int length) throws IOException {
		byte[] bytes;
		if(offset == 0 && length == s.length())
			bytes = s.getBytes();
		else
			bytes = s.substring(offset, offset + length).getBytes();
		write(bytes);
	}

    /**
	 * @see simple.io.ByteOutput#writeChars(java.lang.String, int)
	 */
    public void writeChars(String s, int length) throws IOException {
        ensureCapacity(length * 2);
    	for (int i = 0 ; i < length ; i++) {
            int v = s.charAt(i);
            buffer.put((byte)((v >>> 8) & 0xFF));
            buffer.put((byte)((v >>> 0) & 0xFF));
        }
    }

    public void write(byte[] bytes) throws IOException {
    	write(bytes, 0, bytes.length);
    }

    public void write(byte[] bytes, int offset, int length) throws IOException {
    	if(buffer.remaining() < length) {
			flush();
			if(buffer.remaining() < length) {
				ByteBuffer tmpBuffer = ByteBuffer.wrap(bytes, offset, length);
				do {
					byteChannel.write(tmpBuffer);
				} while(tmpBuffer.hasRemaining());
				return;
			} 
		} 
    	buffer.put(bytes, offset, length);
    }
    
    public void write(ByteBuffer buffer) throws IOException {
		flush();
		do {
			byteChannel.write(buffer);
		} while(buffer.hasRemaining());
    }

    public void flush() throws IOException {
		buffer.flip();
		try {
			while(buffer.hasRemaining())
				byteChannel.write(buffer);
		} finally {
			buffer.compact();
		}
    }
}
