/*
 * Created on Dec 7, 2005
 *
 */
package simple.io;

import java.io.File;
import java.net.URI;

public class CacheFile extends File {
    private static final long serialVersionUID = -6673410124L;

    public CacheFile(String pathname) {
        super(pathname);
        deleteOnExit();
    }

    public CacheFile(String parent, String child) {
        super(parent, child);
        deleteOnExit();
    }

    public CacheFile(File parent, String child) {
        super(parent, child);
        deleteOnExit();
    }

    public CacheFile(URI uri) {
        super(uri);
        deleteOnExit();
    }

    @Override
    protected void finalize() throws Throwable {
        // delete file
        delete();
        //System.out.println("Deleting file '" + getPath() + "'");
        super.finalize();
    }
}
