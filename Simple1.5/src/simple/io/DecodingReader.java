package simple.io;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;

/** This implementation is NOT thread-safe.
 * @author Brian S. Krug
 *
 */
public abstract class DecodingReader extends Reader {
	protected CharBuffer charBufferOne;
	protected CharsetDecoder decoder;

	public DecodingReader(CharsetDecoder decoder) {
        super();
        setDecoder(decoder);
    }

	@Override
	public int read(char[] cbuf, int offset, int length) throws IOException {
		return read(CharBuffer.wrap(cbuf, offset, length));
	}

	protected void initCharBufferOne() {
		if(charBufferOne == null)
			charBufferOne = CharBuffer.allocate(1);
	}
	@Override
	public int read() throws IOException {
		initCharBufferOne();
		charBufferOne.clear();
		int read = read(charBufferOne);
		if(read == -1)
			return -1;
		return charBufferOne.get(0);
	}

	public CharsetDecoder getDecoder() {
		return decoder;
	}

	public void setDecoder(CharsetDecoder decoder) {
		if(decoder == null) throw new NullPointerException("Decoder cannot be null");
		//we may wish to warn if buffer.position() > 0
		this.decoder = decoder;
	}
}
