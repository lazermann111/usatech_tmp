/*
 * Created on Sep 27, 2005
 *
 */
package simple.io;

import java.io.IOException;
import java.io.InputStream;

public class Base64DecodingInputStream extends AbstractBase64DecodingInputStream {
    protected InputStream in;
    public Base64DecodingInputStream(InputStream in) {
        this.in = in;
    }

	@Override
	protected int get() throws IOException {
		return in.read();
    }
    @Override
	protected int remaining() throws IOException {
		return in.available();
    }
    @Override
    public void close() throws IOException {
        in.close();
    }       
}