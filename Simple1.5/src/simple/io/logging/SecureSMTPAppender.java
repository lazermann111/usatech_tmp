package simple.io.logging;

import java.util.Date;

import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Layout;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.net.SMTPAppender;
import org.apache.log4j.spi.LoggingEvent;

public class SecureSMTPAppender extends SMTPAppender {
	// prevent CRLF injection
	protected void sendBuffer() {

	    // Note: this code already owns the monitor for this
	    // appender. This frees us from needing to synchronize on 'cb'.
	    try {
	      MimeBodyPart part = new MimeBodyPart();

	      StringBuilder sbuf = new StringBuilder();
	      String t = layout.getHeader();
	      if(t != null)
		sbuf.append(t);
	      int len =  cb.length();
	      for(int i = 0; i < len; i++) {
		//sbuf.append(MimeUtility.encodeText(layout.format(cb.get())));
		LoggingEvent event = cb.get();
		String formatted = layout.format(event);
		if (formatted != null) {
			sbuf.append(formatted.replace("\r", "").replace("\n", ""));
			sbuf.append(Layout.LINE_SEP);
		}
		if(layout.ignoresThrowable()) {
		  String[] s = event.getThrowableStrRep();
		  if (s != null) {
		    for(int j = 0; j < s.length; j++) {
		    	if (s[j] != null) {
		    		sbuf.append(s[j].replace("\r", "").replace("\n", ""));
		    		sbuf.append(Layout.LINE_SEP);
		    	}
		    }
		  }
		}
	      }
	      t = layout.getFooter();
	      if(t != null)
	    	  sbuf.append(t);
	      part.setContent(sbuf.toString(), layout.getContentType());

	      Multipart mp = new MimeMultipart();
	      mp.addBodyPart(part);
	      msg.setContent(mp);

	      msg.setSentDate(new Date());
	      Transport.send(msg);
	    } catch(Exception e) {
	      LogLog.error("Error occured while sending e-mail notification.", e);
	    }
	}
}
