/**
 * 
 */
package simple.io.logging.avalon;

public class SimpleAvalonLogger implements org.apache.avalon.framework.logger.Logger {
    protected simple.io.Log delegate;
    public SimpleAvalonLogger(simple.io.Log _delegate) {
        delegate = _delegate;
    }
    
    public void debug(String str) {
        delegate.debug(str);
    }
    
    public void debug(String str, Throwable throwable) {
        delegate.debug(str, throwable);
    }
    
    public void error(String str) {
        delegate.error(str);
    }
    
    public void error(String str, Throwable throwable) {
        delegate.error(str, throwable);
    }
    
    public void fatalError(String str) {
        delegate.fatal(str);
    }
    
    public void fatalError(String str, Throwable throwable) {
        delegate.fatal(str, throwable);
    }
    
    public org.apache.avalon.framework.logger.Logger getChildLogger(String str) {
        return null;
    }
    
    public void info(String str) {
        delegate.info(str);
    }
    
    public void info(String str, Throwable throwable) {
        delegate.info(str, throwable);
    }
    
    public boolean isDebugEnabled() {
        return delegate.isDebugEnabled();
    }
    
    public boolean isErrorEnabled() {
        return delegate.isErrorEnabled();
    }
    
    public boolean isFatalErrorEnabled() {
        return delegate.isFatalEnabled();
    }
    
    public boolean isInfoEnabled() {
        return delegate.isInfoEnabled();
    }
    
    public boolean isWarnEnabled() {
        return delegate.isWarnEnabled();
    }
    
    public void warn(String str) {
        delegate.warn(str);
    }
    
    public void warn(String str, Throwable throwable) {
        delegate.warn(str, throwable);
    }        
}