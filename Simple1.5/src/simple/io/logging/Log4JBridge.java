/*
 * Created on Jun 27, 2005
 *
 */
package simple.io.logging;

import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LocationInfo;
import org.apache.log4j.spi.LoggingEvent;

import simple.io.Log;

public class Log4JBridge extends AbstractBridge {
    private static final String FQCN = Log.class.getName();
    protected static class SimpleLoggingEvent extends LoggingEvent {
        private static final long serialVersionUID = -19014505525235L;
        public SimpleLoggingEvent(Category logger, int level, Object message, Throwable throwable) {
            super(FQCN, logger, Level.toLevel(level), message, throwable);
        }

        public LocationInfo getLocationInformation() {
            //TODO: implement using Thread.getStackTrace()
            return super.getLocationInformation();
        }
    }

    protected class Log4JLog extends AbstractLog {
        protected Logger delegate;
        public Log4JLog(Logger delegate) {
            this.delegate = delegate;
        }
        @Override
        protected int getEffectiveLevel() {
            return delegate.getEffectiveLevel().toInt();
        }

        @Override
        protected void logMessage(int level, Object msg, Throwable exception) {
            delegate.callAppenders(new SimpleLoggingEvent(delegate, level, msg, exception));
        }

        @Override
        protected int getFatalLevel() {
            return Level.FATAL_INT;
        }

        @Override
        protected int getErrorLevel() {
            return Level.ERROR_INT;
        }

        @Override
        protected int getWarnLevel() {
            return Level.WARN_INT;
        }

        @Override
        protected int getInfoLevel() {
            return Level.INFO_INT;
        }

        @Override
        protected int getDebugLevel() {
            return Level.DEBUG_INT;
        }

        @Override
        protected int getTraceLevel() {
            return Level.TRACE_INT;
        }
    }
    protected boolean locationInfo = true;

    public Log4JBridge() {
        super();
    }

    /**
     * @return Returns the locationInfo.
     */
    public boolean isLocationInfo() {
        return locationInfo;
    }

    /**
     * @param locationInfo The locationInfo to set.
     */
    public void setLocationInfo(boolean locationInfo) {
        this.locationInfo = locationInfo;
    }

    public void finish() {
        //NOTE: We can't do this or it will remove all appenders and there is not easy way to tell Log4J to auto-config itself again
    	//LogManager.shutdown();
    }

    @Override
    protected Log createLog(String name) {
        return new Log4JLog(Logger.getLogger(name));
    }
}
