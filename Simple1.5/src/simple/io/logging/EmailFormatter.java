/*
 * EmailFormatter.java
 *
 * Created on January 14, 2004, 4:07 PM
 */

package simple.io.logging;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.MessageFormat;
import java.util.Map;

import simple.io.Log;
import simple.mail.EmailInfo;

/** Creates an EmailInfo object from a LogRecord. Each piece of the EmailInfo (to, from, subject, body)
 * is formatted using the MessageFormat pattern specified in the logger.properties file. 
 * To specify a pattern, create a property in the logger.properties file named "simple.io.logger.EmailFormatter.<PIECE>",
 * where <PIECE> is one of "bcc", "cc", "contentType", "from", "subject", "to", or "body"
 *
 * @author  Brian S. Krug
 */
public class EmailFormatter {
    protected Map<String,Object> formatMap = new java.util.HashMap<String,Object>();
    protected Map<String,String> defaultMap = new java.util.HashMap<String,String>();
    protected String PROPERTY_PREFIX = "simple.io.logger.EmailFormatter.";
    /** Creates a new instance of EmailFormatter */
    public EmailFormatter() {
        init();
    }
    
    protected void init() {
        //setup defaults
        defaultMap.put("bcc", null);
        defaultMap.put("cc", null);
        defaultMap.put("contentType","text/plain");
        defaultMap.put("from","APPLICATION EMAILER");
        defaultMap.put("subject","Log Message");
        defaultMap.put("to",null);
        defaultMap.put("body", SimpleFormatter.DEFAULT_PATTERN);        
    }
    
    public EmailInfo createEmailInfo(Object params) {
        if(Log.checkProperties()) formatMap.clear(); // reset the cache so that next time we go back to Log for the property value
        EmailInfo info = new EmailInfo();
        info.setBcc(getProperty("bcc", params));
        info.setCc(getProperty("cc", params));
        info.setContentType(getProperty("contentType", params));
        info.setFrom(getProperty("from", params));
        info.setSubject(getProperty("subject", params));
        info.setTo(getProperty("to", params));
        info.setBody(getProperty("body", params));
        return info;
    }
    
    public static String format(EmailInfo info) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(info);
        return baos.toString();
    }
    
    public static EmailInfo parse(String s) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(s.getBytes());
        ObjectInputStream ois = new ObjectInputStream(bais);
        return (EmailInfo) ois.readObject();
    }
    
    protected String getProperty(String name, Object params) {
        Object o = formatMap.get(name);
        if(o instanceof MessageFormat) return ((MessageFormat)o).format(params);
        else if(o instanceof String) return (String)o;
        else {
            String value = Log.getProperty(PROPERTY_PREFIX + name);
            if(value == null) value = defaultMap.get(name);
            if(value != null && simple.text.RegexUtils.matches(".*\\{\\d\\}.*", value, null)) { // its a message format
                MessageFormat mf = new MessageFormat(value);
                formatMap.put(name, mf);
                return mf.format(params);
            } else {
                formatMap.put(name, value);
                return value;
            }
        }
    }
    
    public String format(Object logMessage) {
        try {
            return format(createEmailInfo(logMessage));
        } catch(IOException ioe) {
            return "EXCEPTION: " + ioe.toString();
        }
    }
    
}
