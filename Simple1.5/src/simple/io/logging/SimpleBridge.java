/*
 * Created on Jun 27, 2005
 *
 */
package simple.io.logging;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.ref.WeakReference;
import java.text.FieldPosition;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;
import simple.lang.SystemUtils;
import simple.text.FileSizeFormat;
import simple.text.MessageFormat;

public class SimpleBridge extends AbstractBridge {
    public static final String DEFAULT_PATTERN = "{date,date,HH:mm:ss:SSSS} ({threadName},{threadPriority},{threadGroupName}) [{sourceClassName}.{sourceMethodName}({sourceFileName}:{sourceLineNumber})] {levelName}: {message}{exception}";    
    public static final int FATAL = 1000; 
    public static final int ERROR = 900;
    public static final int WARN = 800;
    public static final int INFO = 700;
    public static final int DEBUG = 600;
    public static final int TRACE = 500;
    public static final int ALL = 0;
    protected static final int[] levels = new int[]{ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL};
    protected static final Map<Integer,String> levelNames = new HashMap<Integer,String>();
    protected static final String NEWLINE = SystemUtils.getNewLine();    
    protected static final AtomicLong sequence = new AtomicLong(1);
    protected static int defaultLevel = DEBUG;
    protected MessageFormat format;
    protected final StringBuffer stringBuffer = new StringBuffer(1024);
    protected final FieldPosition pos = new FieldPosition(0);
    protected Writer writer;
    protected int maxSize;
    protected int maxSeconds;
    protected int maxFiles;
    protected String filePrefix;
    protected String fileSuffix;
    protected long lastRoll = System.currentTimeMillis();
    protected long bytesWritten; 
    protected boolean checkForRoll = false;
    protected boolean threadDifferentiated = false;
    protected final ReentrantLock lock = new ReentrantLock();
    
    static {
        levelNames.put(new Integer(FATAL), "FATAL");
        levelNames.put(new Integer(ERROR), "ERROR");
        levelNames.put(new Integer(WARN), "WARN");
        levelNames.put(new Integer(INFO), "INFO");
        levelNames.put(new Integer(DEBUG), "DEBUG");
        levelNames.put(new Integer(TRACE), "TRACE");     
        levelNames.put(new Integer(ALL), "ALL");     
    }
    
    protected class SimpleLog extends AbstractLog {
        protected final String identifier;
        protected String callerClassName;
        protected int effectiveLevel;
        protected boolean locationInfo;
        
        public SimpleLog(String identifier) {
            this.identifier = identifier;
            try {
                callerClassName = Class.forName(identifier).getName();
            } catch(Throwable e) {                
            }
            effectiveLevel = getLogLevel(identifier); 
            locationInfo = isLocationInfo(identifier);
        }
        @Override
        protected int getEffectiveLevel() {
            return effectiveLevel;
        }
        
        @Override
        protected void logMessage(int level, Object msg, Throwable exception) {
            SimpleMessage sm = new SimpleMessage(identifier, level, msg, exception, locationInfo ? callerClassName : null);
            printMessage(sm);            
        }

        @Override
        protected int getFatalLevel() {
            return FATAL;
        }

        @Override
        protected int getErrorLevel() {
            return ERROR;
        }

        @Override
        protected int getWarnLevel() {
            return WARN;
        }

        @Override
        protected int getInfoLevel() {
            return INFO;
        }

        @Override
        protected int getDebugLevel() {
            return DEBUG;
        }

        @Override
        protected int getTraceLevel() {
            return DEBUG;
        }   
    }
    protected static class LogConfigInfo {
    	public int effectiveLevel;
    	public boolean locationInfo;
		public LogConfigInfo(int effectiveLevel, boolean locationInfo) {
			super();
			this.effectiveLevel = effectiveLevel;
			this.locationInfo = locationInfo;
		}
    }
    protected class SimpleThreadDiffLog extends AbstractLog {
    	protected final String identifier;
    	protected final ThreadLocal<LogConfigInfo> configLocal;
        protected String callerClassName;
        
        public SimpleThreadDiffLog(final String identifier) {
        	this.identifier = identifier;
            try {
                callerClassName = Class.forName(identifier).getName();
            } catch(Throwable e) {                
            }
            configLocal = new ThreadLocal<LogConfigInfo>() {
            	@Override
            	protected LogConfigInfo initialValue() {
            		String threadName = Thread.currentThread().getName();            		
            		return new LogConfigInfo(getLogLevel(identifier, threadName), isLocationInfo(identifier, threadName));
            	}
            };
        }
        @Override
        protected int getEffectiveLevel() {
            return configLocal.get().effectiveLevel;
        }
        
        @Override
        protected void logMessage(int level, Object msg, Throwable exception) {
        	LogConfigInfo config = configLocal.get();
            SimpleMessage sm = new SimpleMessage(identifier, level, msg, exception, config.locationInfo ? callerClassName : null);
            printMessage(sm);            
        }

        @Override
        protected int getFatalLevel() {
            return FATAL;
        }

        @Override
        protected int getErrorLevel() {
            return ERROR;
        }

        @Override
        protected int getWarnLevel() {
            return WARN;
        }

        @Override
        protected int getInfoLevel() {
            return INFO;
        }

        @Override
        protected int getDebugLevel() {
            return DEBUG;
        }

        @Override
        protected int getTraceLevel() {
            return DEBUG;
        }   
    }

    protected static class MessageFormatter extends MessageFormat {       
        private static final long serialVersionUID = 984505525235L;

		public MessageFormatter(String pattern) {
            super(pattern);
        }

        @Override
        protected Object getParameter(Object bean, String parameter) {
            if(parameter == null) return null;
            SimpleMessage sm = (SimpleMessage)bean;
            if("date".equals(parameter)) return sm.getDate();
            if("exception".equals(parameter)) return sm.getException();
            if("level".equals(parameter)) return sm.getLevel();
            if("levelName".equals(parameter)) return sm.getLevelName();
            if("logger".equals(parameter)) return sm.getLogger();
            if("message".equals(parameter)) return sm.getMessage();
            if("sequence".equals(parameter)) return sm.getSequence();
            if("sourceClassName".equals(parameter)) return sm.getSourceClassName();
            if("sourceFileName".equals(parameter)) return sm.getSourceFileName();
            if("sourceLineNumber".equals(parameter)) return sm.getSourceLineNumber();
            if("sourceMethodName".equals(parameter)) return sm.getSourceMethodName();
            if("threadGroupName".equals(parameter)) return sm.getThreadGroupName();
            if("threadName".equals(parameter)) return sm.getThreadName();
            if("threadPriority".equals(parameter)) return sm.getThreadPriority();
            return "<" + parameter +">";
        }        
    }
    
    public SimpleBridge() {
        super();
        readProperties();
    }
    
    public void finish() {
        try { writer.flush(); } catch(IOException e) {}
    }

    protected Log createLog(String name) {
        return isThreadDifferentiated() ? new SimpleThreadDiffLog(name) : new SimpleLog(name);
    }
    
    protected static Long getNextSequence() {
        return new Long(sequence.getAndIncrement());
    }
    
    protected void updateCheckForRoll() {
        checkForRoll = (filePrefix != null || fileSuffix != null) && (maxSize > 0 || maxSeconds > 0);
    }
    
    protected void printMessage(SimpleMessage message) {
        //format the message
        lock.lock(); // print one message at a time
        try {
            //Using StringBuffer is faster
            stringBuffer.setLength(0);
            format.format(message, stringBuffer, pos);
            String s = stringBuffer.toString();
            if(checkForRoll) {
                // check file size and date to see if we need to "roll"
                if(maxSize > 0) {
                    if(bytesWritten >= maxSize) {
                        writer = createWriter(rollFile());
                        bytesWritten = s.length() + NEWLINE.length();
                    } else {
                        bytesWritten += s.length() + NEWLINE.length();
                    }
                } else if(maxSeconds > 0) {
                    if(lastRoll + maxSeconds < System.currentTimeMillis()) {
                        //roll by date
                        writer = createWriter(rollFile());
                        lastRoll = System.currentTimeMillis();
                    }
                }
            }
            
            //print the message
            try {
                writer.write(s);
                writer.write(NEWLINE);
                writer.flush();
            } catch(IOException e) {
                Log.handleLogError("Could not write to stream", e);
            }
        } finally {
            lock.unlock();
        }
    }
    
    protected Writer createWriter(OutputStream out) {
        if(out == null) return writer;
        return new OutputStreamWriter(out);
    }

    protected OutputStream rollFile() {
        String prefix = (filePrefix != null ? filePrefix : "");
        String suffix = (fileSuffix != null ? fileSuffix : "");       
        File goal = new File(prefix + suffix);
        if(goal.exists()) {
            int maxIndex = 0;
            if(maxFiles > 0) {
                for(maxIndex = maxFiles-1; maxIndex > 0; maxIndex--) {
                    File file = new File(prefix + "." + maxIndex + suffix);
                    if(file.exists()) break;
                }
            } else {
                String[] filenames = goal.getParentFile().list();
                for(String fn : filenames) {
                    if(fn.startsWith(prefix + ".") && fn.endsWith(suffix)) {
                        try {
                            int index = Integer.parseInt(fn.substring(prefix.length() + 1, fn.length() - suffix.length()));
                            if(index > maxIndex) maxIndex = index;
                        } catch(NumberFormatException e) {                           
                        }
                    }
                }
            } 
            if(maxIndex > 0) {
                File file = new File(prefix + "." + maxIndex + suffix);
                if(maxFiles > 0 && maxIndex >= maxFiles-1) {
                    file.delete();
                } else {
                    file.renameTo(new File(prefix + "." + (maxIndex+1) + suffix));
                }
                for(int i = maxIndex-1; i > 0; i--) {
                    file = new File(prefix + "." + i + suffix);
                    if(file.exists()) file.renameTo(new File(prefix + "." + (i+1) + suffix));
                }
            }
            goal.renameTo(new File(prefix + ".1" + suffix));
            goal = new File(prefix + suffix);
        }
        
        try {
            OutputStream os = new FileOutputStream(goal);
            return os;
        } catch(FileNotFoundException e) {
            Log.handleLogError("Could not roll stream", e);
            return null;
        }
    }

    /**
     * Reloads properties if they've changed and resets appropriate variables
     */
    protected void checkProperties() {
        try {
            if(Log.checkProperties()) {
                lock.lockInterruptibly();  // block until condition holds
                try {
                    readProperties();
                } finally {
                    lock.unlock();
                }
            }
        } catch(RuntimeException e) {
            Log.handleLogError("Could not reload properties", e);
        } catch(InterruptedException e) {
        	Log.handleLogError("Could not reload properties", e);
		}
    }
    
    /**
     * Resets appropriate variables based on properties
     */
    protected void readProperties() {
        // blank out log levels
        for(WeakReference<Log> wr : logs.values()) {
            Log l = wr.get();
            if(l instanceof SimpleLog) {
                SimpleLog sl = (SimpleLog)l;
                sl.effectiveLevel = getLogLevel(sl.identifier);
                sl.locationInfo = isLocationInfo(sl.identifier);
            }
        }
        
        // see if format has changed
        //if(formatter == null) formatter = new SimpleFormatter();
        String pattern = getProperty(getClass().getName() + ".pattern");
        if(pattern == null || pattern.trim().length() == 0 ) {
            pattern = DEFAULT_PATTERN;
        }
        if(format == null || !pattern.equals(format.toPattern())) {
            format = new MessageFormatter(pattern);
        }
        
        // see it file has changed
        String prefix = getProperty(getClass().getName()+ ".filePrefix");
        if(prefix == null || prefix.trim().length() == 0) {
            prefix = getProperty(getClass().getName()+ ".file");
        }
        if(this.filePrefix != null && writer != null && !this.filePrefix.equals(prefix)) {
            try {
                writer.flush();
                writer.close();
            } catch (IOException e) {
            }
        }        
        if(prefix != null && prefix.trim().length() > 0) {
            String suffix = getProperty(getClass().getName()+ ".fileSuffix");
            if(suffix == null) suffix = "";
            setFilePrefix(prefix);
            setFileSuffix(suffix);
            lock.lock();
            try {
                writer = createWriter(new FileOutputStream(prefix + suffix));
            } catch(java.io.FileNotFoundException fnfe) {
                Log.handleLogError("!!! Could not find the log file: " + prefix + suffix, fnfe);
            } finally {
                lock.unlock();
            }                        
        } else { // use standard out
            setFilePrefix(null);
            setFileSuffix(null);
            lock.lock();
            try {
                writer = createWriter(System.out);
            } finally {
                lock.unlock();
            }  
        } 
        try {
            String max = getProperty(getClass().getName()+ ".maxSeconds");
            if(max != null) setMaxSeconds(Integer.parseInt(max));
        } catch(NumberFormatException e) {}
        try {
            String max = getProperty(getClass().getName()+ ".maxSize");
            if(max != null) setMaxSize(FileSizeFormat.parse(max).intValue());
        } catch(NumberFormatException e) {}
        try {
            String max = getProperty(getClass().getName()+ ".maxFiles");
            if(max != null) setMaxSeconds(Integer.parseInt(max));
        } catch(NumberFormatException e) {}
        try {
            String threadDiff = getProperty(getClass().getName()+ ".threadDiff");
            if(threadDiff != null) setThreadDifferentiated(ConvertUtils.getBoolean(threadDiff));
        } catch(ConvertException e) {}
        
        Log.handleLogError("--- BEGIN LOGGING (pattern=" + format.toPattern() + ") ---", null);
    }
    
    protected String getPropertyFor(String identifier, String property) {
        String tmp = null;
        StringBuilder sb = new StringBuilder(identifier);
        while(true) {
            tmp = getProperty(sb.toString() + "." + property);
            if(tmp != null || sb.length() == 0) break;
            int p = Math.max(sb.toString().lastIndexOf("."), sb.toString().lastIndexOf("$"));
            if(p < 0) p = 0;
            sb.setLength(p);
        }
        return tmp;
    }

    protected String getPropertyFor(String identifier, String threadName, String property) {
        String tmp = null;
        StringBuilder sb = new StringBuilder(identifier);
        while(true) {
            tmp = getProperty(sb.toString() + "[" + threadName + "]." + property);
            if(tmp != null) return tmp;
            if(sb.length() == 0) break;
            int p = Math.max(sb.toString().lastIndexOf("."), sb.toString().lastIndexOf("$"));
            if(p < 0) p = 0;
            sb.setLength(p);
        }
        return getPropertyFor(identifier, property);
    }
    
    public static String getLevelName(int level) {
        return levelNames.get(level);
    }
    
    /**
     *  Returns the log level specified in the properties for the given identifier
     */
    protected int getLogLevel(String identifier) {
        String logLevel = getPropertyFor(identifier, "level");
        try {
            return ConvertUtils.getInt(logLevel, defaultLevel);
        } catch (ConvertException e1) {
            for(Map.Entry<Integer, String> entry: levelNames.entrySet()) {
                if(entry.getValue().equalsIgnoreCase(logLevel)) {
                    return entry.getKey();
                }
            }
        }
        return defaultLevel;
    }
    /**
     *  Returns the log level specified in the properties for the given identifier
     */
    protected int getLogLevel(String identifier, String threadName) {
        String logLevel = getPropertyFor(identifier, threadName, "level");
        try {
            return ConvertUtils.getInt(logLevel, defaultLevel);
        } catch (ConvertException e1) {
            for(Map.Entry<Integer, String> entry: levelNames.entrySet()) {
                if(entry.getValue().equalsIgnoreCase(logLevel)) {
                    return entry.getKey();
                }
            }
        }
        return defaultLevel;
    } 
    
    protected boolean isLocationInfo(String identifier) {
        try {
            return ConvertUtils.getBoolean(getPropertyFor(identifier, "locationInfo"), true);
        } catch (ConvertException e) {
            return false;
        }
    }
    
    protected boolean isLocationInfo(String identifier, String threadName) {
        try {
            return ConvertUtils.getBoolean(getPropertyFor(identifier, threadName, "locationInfo"), true);
        } catch (ConvertException e) {
            return false;
        }
    } 
    
    public String getProperty(String key) {
        return Log.getProperty(key);
    }

    /**
     * @return Returns the maxFiles.
     */
    public int getMaxFiles() {
        return maxFiles;
    }

    /**
     * @param maxFiles The maxFiles to set.
     */
    public void setMaxFiles(int maxFiles) {
        this.maxFiles = maxFiles;
    }

    /**
     * @return Returns the maxSeconds.
     */
    public int getMaxSeconds() {
        return maxSeconds;
    }

    /**
     * @param maxSeconds The maxSeconds to set.
     */
    public void setMaxSeconds(int maxSeconds) {
        this.maxSeconds = maxSeconds;
        updateCheckForRoll();
    }

    /**
     * @return Returns the maxSize.
     */
    public int getMaxSize() {
        return maxSize;
    }

    /**
     * @param maxSize The maxSize to set.
     */
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
        updateCheckForRoll();
    }

    /**
     * @return Returns the filePrefix.
     */
    public String getFilePrefix() {
        return filePrefix;
    }

    /**
     * @param filePrefix The filePrefix to set.
     */
    public void setFilePrefix(String filePrefix) {
        this.filePrefix = filePrefix;
        updateCheckForRoll();
    }

    /**
     * @return Returns the fileSuffix.
     */
    public String getFileSuffix() {
        return fileSuffix;
    }

    /**
     * @param fileSuffix The fileSuffix to set.
     */
    public void setFileSuffix(String fileSuffix) {
        this.fileSuffix = fileSuffix;
        updateCheckForRoll();
    }

	public boolean isThreadDifferentiated() {
		return threadDifferentiated;
	}

	public void setThreadDifferentiated(boolean threadDifferentiated) {
		this.threadDifferentiated = threadDifferentiated;
	}

}
