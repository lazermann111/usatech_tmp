/*
 * Created on Jun 27, 2005
 *
 */
package simple.io.logging;

import simple.io.Log;

public interface Bridge {
    public Log getLog(String name) ;
    public void finish() ;
}
