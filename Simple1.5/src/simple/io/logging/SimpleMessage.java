/*
 * Created on Aug 17, 2005
 *
 */
package simple.io.logging;

import java.util.Date;

import simple.bean.ReflectionUtils;
import simple.lang.SystemUtils;

public class SimpleMessage {
    protected static final String NEWLINE = SystemUtils.getNewLine();    
    protected String identifier;
    protected Throwable exception;
    protected int level;
    protected Object message;        
    protected String callerClassName;
    
    protected StackTraceElement source;
    protected Thread thread;
    
    public SimpleMessage(String identifier, int level, Object message, Throwable exception, String callerClassName) {
        this.identifier = identifier;
        this.level = level;
        this.message = message;
        this.exception = exception;
        this.callerClassName = callerClassName;
    }
    
    public Date getDate() {
        return new Date(System.currentTimeMillis());
    }
    
    public String getThreadName() {
        return getThread().getName();
    }
    
    public String getLogger() {
        return identifier;
    }
    
    protected StackTraceElement getSource() {
        if(source == null && callerClassName != null) source = ReflectionUtils.getCallerStackElement(callerClassName);
        return source;
    }
    
    protected Thread getThread() {
        if(thread == null) thread = Thread.currentThread();
        return thread;
    }
    
    public String getSourceClassName() {
        StackTraceElement ste = getSource();
        if(ste != null) return ste.getClassName();
        else return identifier;
    }
    
    public String getSourceMethodName() {
        StackTraceElement ste = getSource();
        if(ste != null) return ste.getMethodName();
        else return "";
    }
    
    public String getSourceFileName() {
        StackTraceElement ste = getSource();
        if(ste != null) return ste.getFileName();
        else return "";
    }
    
    public Integer getSourceLineNumber() {
        StackTraceElement ste = getSource();
        if(ste != null) return ste.getLineNumber();
        else return null;
    }

    public int getLevel() {
        return level;
    }
    
    public Object getMessage() {
        return message;
    }
    
    public String getException() {
        return (exception == null ? "" : NEWLINE + simple.text.StringUtils.exceptionToString(exception));
    }
    
    public Long getSequence() {
        return SimpleBridge.getNextSequence(); 
    }
    
    public String getLevelName() {
        return SimpleBridge.getLevelName(level);
    }
    
    public int getThreadPriority() {
        return getThread().getPriority();
    }
    
    public String getThreadGroupName() {
        ThreadGroup tg = getThread().getThreadGroup();
        return (tg == null ? "" : tg.getName());
    }      
}