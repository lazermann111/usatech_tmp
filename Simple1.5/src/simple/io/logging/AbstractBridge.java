/*
 * Created on Jun 27, 2005
 *
 */
package simple.io.logging;

import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import simple.io.Log;

public abstract class AbstractBridge implements Bridge {
    protected final ConcurrentMap<String, WeakReference<Log>> logs = new ConcurrentHashMap<String, WeakReference<Log>>();
    
    public AbstractBridge() {
    }
    
    public Log getLog(String name) {
    	WeakReference<Log> wr = logs.get(name);
        Log log;
        for(;;) {
	    	if(wr != null && (log=wr.get()) != null)
	        	return log;
	        log = createLog(name);
	        WeakReference<Log> wrNew = new WeakReference<Log>(log);
	        if(wr == null) {
	        	wr = logs.putIfAbsent(name, wrNew);
	        	if(wr == null) return log;
	        } else if(logs.replace(name, wr, wrNew)) 
	        	return log;
	        else
	        	wr = logs.get(name);
    	}
    }
    
    protected abstract Log createLog(String name) ;
  
}
