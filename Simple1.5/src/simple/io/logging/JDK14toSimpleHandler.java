package simple.io.logging;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.regex.Pattern;

import simple.io.Log;

public class JDK14toSimpleHandler extends Handler {
	protected static final Pattern messageFormatPattern = Pattern.compile("\\{\\d\\}");
	@Override
	public void publish(LogRecord record) {
		Log log = Log.getLog(record.getLoggerName());
		if(record.getLevel().intValue() >= Level.SEVERE.intValue()) {
			if(log.isFatalEnabled())
				log.fatal(toMessage(record), record.getThrown());
		} else if(record.getLevel().intValue() >= Level.WARNING.intValue()) {
			if(log.isErrorEnabled())
				log.error(toMessage(record), record.getThrown());
		} else if(record.getLevel().intValue() >= Level.INFO.intValue()) {
			if(log.isWarnEnabled())
				log.warn(toMessage(record), record.getThrown());
		} else if(record.getLevel().intValue() >= Level.FINE.intValue()) {
			if(log.isInfoEnabled())
				log.info(toMessage(record), record.getThrown());
		} else if(record.getLevel().intValue() >= Level.FINER.intValue()) {
			if(log.isDebugEnabled())
				log.debug(toMessage(record), record.getThrown());
		} else {
			if(log.isTraceEnabled())
				log.trace(toMessage(record), record.getThrown());
		}
	}

	private Object toMessage(LogRecord record) {
		String message = record.getMessage();
		// Format message
		Object parameters[] = record.getParameters();
		if(parameters != null && parameters.length != 0 && messageFormatPattern.matcher(message).find()) {
			try {
				message = MessageFormat.format(message, parameters);
			} catch(Exception ex) {
				// ignore Exception
			}
		}
		return message;
	}

	@Override
	public void flush() {
	}

	@Override
	public void close() throws SecurityException {
		Log.finish();
	}

	public static void install() throws SecurityException, IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("handlers=").append(JDK14toSimpleHandler.class.getName()).append("\n.level=FINER");
		// NOTE: If we need lowest level logging we must use FINEST
		// send stream
		LogManager.getLogManager().readConfiguration(new ByteArrayInputStream(sb.toString().getBytes()));
	}
}
