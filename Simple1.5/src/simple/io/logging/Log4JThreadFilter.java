package simple.io.logging;

import java.util.regex.Pattern;

import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;


public class Log4JThreadFilter extends Filter {
	protected Pattern threadNamePattern;
	protected boolean include = false;
	@Override
	public int decide(LoggingEvent event) {
		if(threadNamePattern == null || threadNamePattern.matcher(event.getThreadName()).matches() == isInclude())
			return Filter.NEUTRAL;
		else
			return Filter.DENY;
	}
	public String getThreadNamePattern() {
		return threadNamePattern == null ? null : threadNamePattern.pattern();
	}
	public void setThreadNamePattern(String threadNamePattern) {
		this.threadNamePattern = Pattern.compile(threadNamePattern.trim());
	}
	public boolean isInclude() {
		return include;
	}
	public void setInclude(boolean include) {
		this.include = include;
	}
}
