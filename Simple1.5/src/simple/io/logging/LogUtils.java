package simple.io.logging;

import org.apache.log4j.Layout;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.helpers.QuietWriter;
import org.apache.log4j.spi.LoggingEvent;

public class LogUtils {	
	// prevent CRLF injection
	protected static void writeToLog(WriterAppender wa, QuietWriter qw, Layout layout, LoggingEvent event) {
	    String formatted = layout.format(event);
	    if (formatted != null) {
	    	qw.write(formatted.replace("\r", "").replace("\n", ""));
	    	qw.write(Layout.LINE_SEP);
	    }
	
	    if(layout.ignoresThrowable()) {
	    	String[] s = event.getThrowableStrRep();
	    	if (s != null) {
	    		int len = s.length;
	    		for(int i = 0; i < len; i++) {
	    			if (s[i] != null) {
	    				qw.write(s[i].replace("\r", "").replace("\n", ""));
	    				qw.write(Layout.LINE_SEP);
	    			}
	    		}
	      	}
	    }
	
	    if(wa.getImmediateFlush()) {
	    	qw.flush();
	    }
	}
}
