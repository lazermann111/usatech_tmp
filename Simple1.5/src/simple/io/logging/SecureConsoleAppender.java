package simple.io.logging;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.spi.LoggingEvent;

public class SecureConsoleAppender extends ConsoleAppender {
	// prevent CRLF injection
	protected void subAppend(LoggingEvent event) {
		LogUtils.writeToLog(this, this.qw, layout, event);
	}
}
