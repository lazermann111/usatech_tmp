/**
 *
 */
package simple.io.logging;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.FileWatchdog;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.Configurator;
import org.apache.log4j.spi.LoggerRepository;
import org.apache.log4j.xml.DOMConfigurator;

import simple.io.ConfigSource;
import simple.io.Loader;
import simple.io.LoadingException;

/**
 * @author Brian S. Krug
 *
 */
public class Log4JWatchingConfigurator implements Configurator {
	protected static final String PROPERTY_WATCH_DELAY = "log4j.watchDelay";
	protected long delay;
	protected DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss' - '");

	/**
	 *
	 */
	public Log4JWatchingConfigurator() {
		delay = Long.getLong(PROPERTY_WATCH_DELAY, FileWatchdog.DEFAULT_DELAY);
	}

	/**
	 * @see org.apache.log4j.spi.Configurator#doConfigure(java.net.URL,
	 *      org.apache.log4j.spi.LoggerRepository)
	 */
	public void doConfigure(final URL url, final LoggerRepository repository) {
		final Configurator delegate;
		String filename = url.getFile();
		if(filename != null && filename.endsWith(".xml")) {
			delegate = new DOMConfigurator();
		} else {
			delegate = new PropertyConfigurator();
		}
		ConfigSource src;
		try {
			src = ConfigSource.createConfigSource(url, true);
		} catch(IOException e) {
			LogLog.error(formatTime() + "Could not read log4j config file '" + url + "'", e);
			return;
		}
		src.registerLoader(new Loader() {
			public void load(ConfigSource src) throws LoadingException {
				delegate.doConfigure(url, repository);
				LogLog.warn(formatTime() + "Successfully reloaded log4j config file '" + url + "'");
			}
		}, 0, delay);
		try {
			src.reload();
		} catch(LoadingException e) {
			LogLog.error(formatTime() + "Error while loading log4j config file '" + url + "'", e);
		}
	}

	protected String formatTime() {
		return timeFormat.format(new Date());
	}
}
