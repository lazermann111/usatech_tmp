/*
 * Created on Jun 28, 2005
 *
 */
package simple.io.logging;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogConfigurationException;
import org.apache.commons.logging.LogFactory;

public class SimpleLogFactory extends LogFactory {
    protected Map<String, Object> attributes = new HashMap<String, Object>();
    protected ConcurrentMap<String, WeakReference<Log>> logs = new ConcurrentHashMap<String, WeakReference<Log>>();
    
    public SimpleLogFactory() {
        super();
        //simple.io.Log.handleLogError("~~Using SimpleLogFactory~~", null);
    }

    @Override
    public Object getAttribute(String name) {
        return attributes.get(name);
    }

    @Override
    public String[] getAttributeNames() {
        return attributes.keySet().toArray(new String[attributes.size()]);
    }

	@Override
    public Log getInstance(Class clazz) throws LogConfigurationException {
        return getInstance(clazz.getName());
    }

    @Override
    public Log getInstance(String name) throws LogConfigurationException {
    	WeakReference<Log> wr = logs.get(name);
        Log log;
        for(;;) {
	    	if(wr != null && (log=wr.get()) != null)
	        	return log;
	        log = createLog(name);
	        WeakReference<Log> wrNew = new WeakReference<Log>(log);
	        if(wr == null) {
	        	wr = logs.putIfAbsent(name, wrNew);
	        	if(wr == null) return log;
	        } else if(logs.replace(name, wr, wrNew)) 
	        	return log;
	        else
	        	wr = logs.get(name);
    	}
    }

    protected Log createLog(String name) {
        return new ApacheLog(simple.io.Log.getLog(name));
    }

    @Override
    public void release() {
        //simple.io.Log.finish();
    }

    @Override
    public void removeAttribute(String name) {
        attributes.remove(name);
    }

    @Override
    public void setAttribute(String name, Object value) {
        attributes.put(name, value);
    }    
}
