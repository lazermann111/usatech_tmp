/*
 * SimpleFormatter.java
 *
 * Created on January 8, 2004, 4:35 PM
 */

package simple.io.logging;

import java.text.FieldPosition;
import java.text.Format;

/** Provides Format formatting for a LogRecord based on the pattern specified
 * in the logger.properties file. 
 * To specify a pattern, create a property in the logger.properties file named 
 * "simple.io.logger.SimpleFormatter.pattern".
 *
 * @author  Brian S. Krug
 */
public class SimpleFormatter {
    /** The default pattern for the message */
    public static final String DEFAULT_PATTERN = "{date,date,HH:mm:ss:SSSS} ({threadName},{threadPriority},{threadGroupName}) [{sourceClassName}.{sourceMethodName}({sourceFileName}:{sourceLineNumber})] {levelName}: {message}{exception}";
    /** Holds value of property pattern. */
    private String pattern;    
    /** Holds value of property format. */
    private Format format;
    
    private StringBuffer sb = new StringBuffer(512);
    protected FieldPosition pos = new FieldPosition(0);
    
    /** Creates a new instance of SimpleFormatter */
    public SimpleFormatter() {
        super();
    }
    
    public String format(Object logMessage) {
        sb.setLength(0);
        getFormat().format(logMessage, sb, pos);  
        return sb.toString();
    }
    
    /** Getter for property pattern.
     * @return Value of property pattern.
     *
     */
    public String getPattern() {
        if(pattern == null || pattern.trim().length() == 0) {
            //pattern = simple.io.Log.getProperty("simple.io.logging.SimpleFormatter.pattern");
            if(pattern == null || pattern.trim().length() == 0) pattern = DEFAULT_PATTERN;
        }
        return this.pattern;
    }
    
    /** Setter for property pattern.
     * @param pattern New value of property pattern.
     *
     */
    public void setPattern(String pattern) {
        if(this.pattern == null || !this.pattern.equals(pattern)) {
            this.pattern = pattern;
            format = null;
        }        
    }
    
    /** Getter for property format.
     * @return Value of property format.
     *
     */
    protected Format getFormat() {
        if(format == null) format = new simple.text.MessageFormat(getPattern());
        return this.format;
    }
        
    protected void logPropertiesChanged() {
        setPattern(null); // clear pattern
    }
    
}
