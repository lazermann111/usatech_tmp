package simple.io.logging;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Layout;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.helpers.CountingQuietWriter;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.LoggingEvent;

public class SecureRollingFileAppender extends RollingFileAppender {	
	private long nextRollover = 0;
	
	public // synchronization not necessary since doAppend is already synched
	  void rollOver() {
	    File target;
	    File file;

	    if (qw != null) {
	        long size = ((CountingQuietWriter) qw).getCount();
	        LogLog.debug("rolling over count=" + size);
	        //   if operation fails, do not roll again until
	        //      maxFileSize more bytes are written
	        nextRollover = size + maxFileSize;
	    }
	    LogLog.debug("maxBackupIndex="+maxBackupIndex);

	    boolean renameSucceeded = true;
	    // If maxBackups <= 0, then there is no file renaming to be done.
	    if(maxBackupIndex > 0) {
	      // Delete the oldest file, to keep Windows happy.
	      file = new File(fileName + '.' + maxBackupIndex);
	      if (file.exists())
	       renameSucceeded = file.delete();

	      // Map {(maxBackupIndex - 1), ..., 2, 1} to {maxBackupIndex, ..., 3, 2}
	      for (int i = maxBackupIndex - 1; i >= 1 && renameSucceeded; i--) {
		file = new File(fileName + "." + i);
		if (file.exists()) {
		  target = new File(fileName + '.' + (i + 1));
		  LogLog.debug("Renaming file " + file + " to " + target);
		  renameSucceeded = file.renameTo(target);
		}
	      }

	    if(renameSucceeded) {
	      // Rename fileName to fileName.1
	      target = new File(fileName + "." + 1);

	      this.closeFile(); // keep windows happy.

	      file = new File(fileName);
	      LogLog.debug("Renaming file " + file + " to " + target);
	      renameSucceeded = file.renameTo(target);
	      //
	      //   if file rename failed, reopen file with append = true
	      //
	      if (!renameSucceeded) {
	          try {
	            this.setFile(fileName, true, bufferedIO, bufferSize);
	          }
	          catch(IOException e) {
	            LogLog.error("setFile("+fileName+", true) call failed.", e);
	          }
	      }
	    }
	    }

	    //
	    //   if all renames were successful, then
	    //
	    if (renameSucceeded) {
	    try {
	      // This will also close the file. This is OK since multiple
	      // close operations are safe.
	      this.setFile(fileName, false, bufferedIO, bufferSize);
	      nextRollover = 0;
	    }
	    catch(IOException e) {
	      LogLog.error("setFile("+fileName+", false) call failed.", e);
	    }
	    }
	}
	
	// prevent CRLF injection
	protected void subAppend(LoggingEvent event) {
		String formatted = layout.format(event);
		if (formatted != null) {
			qw.write(formatted.replace("\r", "").replace("\n", ""));
			qw.write(Layout.LINE_SEP);
		}

	    if(layout.ignoresThrowable()) {
	    	String[] s = event.getThrowableStrRep();
	    	if (s != null) {
	    		int len = s.length;
	    		for(int i = 0; i < len; i++) {
	    			if (s[i] != null) {
	    				qw.write(s[i].replace("\r", "").replace("\n", ""));
	    				qw.write(Layout.LINE_SEP);
	    			}
	    		}
	    	}
	    }

	    if(immediateFlush) {
	    	qw.flush();
	    }
		
	    if(fileName != null && qw != null) {
	        long size = ((CountingQuietWriter) qw).getCount();
	        if (size >= maxFileSize && size >= nextRollover) {
	            rollOver();
	        }
	    }
	}
}
