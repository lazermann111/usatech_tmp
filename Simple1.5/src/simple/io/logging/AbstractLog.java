package simple.io.logging;

import simple.io.Log;
import simple.text.MessageFormat;

/**
 * Provides asynchronous logging for an application as well as email notification
 * if required. 
 *
 * @author    Brian S. Krug
 */
public abstract class AbstractLog extends Log {
    /*
     * Creates a new Log instance with the specified identifier
     */
    public AbstractLog() {
    }
              
    /**
     *  Actually formats and adds a log message to the output queue. Does nothing
     *  if the specified level is not being logged
     */
    protected void log(int level, Object msg, Throwable exception) {
        if(shouldLog(level)) {
			logMessage(level, decorateMessage(msg), exception);
        }
    }
    
	protected void log(int level, String format, Object... args) {
		if(shouldLog(level)) {
			Throwable t = null;
			String msg;
			if(args != null && args.length > 0) {
				for(int i = 0; i < args.length; i++) {
					if(args[i] instanceof Throwable) {
						t = (Throwable) args[i];
						break;
					}
				}
				msg = MessageFormat.format(format, args);
			} else
				msg = format;
			try {
				logMessage(level, decorateMessage(msg), t);
			} catch(IllegalArgumentException e) {
				logMessage(level, format + " (LOG FORMAT ERROR)", e);
				e.printStackTrace();
			}
		}
	}
    
	protected Object decorateMessage(Object message) {
		return message;
	}

    /**
     * Converts the exception to a string and appends it to the message. Then,
     * calls the two argument version of the log() method.
     */
    protected void log(int level, Object msg) {
        log(level, msg, null);
    }
            
    /**
     * Tests whether the given log level is enabled for this log
     */
    protected boolean shouldLog(int level) {
        return level >= getEffectiveLevel();
    }
    
    /**
     * Reloads properties if they've changed and resets appropriate variables
     */
    protected abstract int getEffectiveLevel() ; 
    protected abstract void logMessage(int level, Object msg, Throwable exception) ; 
    
    /**
     * Logs a message at the TRACE level
     */
    public void trace(Object msg) {
        log(getTraceLevel(), msg);
    }
    
    /**StringUtils.exceptionToString(new Throwable());
     * Logs a message at the TRACE level
     */
    public void trace(Object msg, Throwable throwable) {
        log(getTraceLevel(), msg, throwable);
    }
    
    public void trace(String format, Object... args) {
    	log(getTraceLevel(), format, args);
    }
    
    /**
     * Logs a message at the DEBUG level
     */
    public void debug(Object msg) {
        log(getDebugLevel(),msg);
    }
    
    /**
     * Logs a message at the DEBUG level
     */
    public void debug(Object msg, Throwable throwable) {
        log(getDebugLevel(), msg, throwable);
    }
        
    public void debug(String format, Object... args) {
    	log(getDebugLevel(), format, args);
    }

    /**
     * Logs a message at the INFO level
     */
    public void info(Object msg) {
        log(getInfoLevel(), msg);
    }
    
    /**
     * Logs a message at the INFO level
     */
    public void info(Object msg, Throwable throwable) {
        log(getInfoLevel(), msg, throwable);
    }
    
    public void info(String format, Object... args) {
    	log(getInfoLevel(), format, args);
    }
    
    /**
     * Logs a message at the WARN level
     */
    public void warn(Object msg) {
        log(getWarnLevel(), msg);
    }

    /**
     * Logs a message at the WARN level
     */
    public void warn(Object msg, Throwable throwable) {
        log(getWarnLevel(), msg, throwable);
    }
    
    public void warn(String format, Object... args) {
    	log(getWarnLevel(), format, args);
    }
    
    /**
     * Logs a message at the ERROR level
     */
    public void error(Object msg) {
        log(getErrorLevel(), msg);
    }
    
    /**
     * Logs a message at the ERROR level
     */
    public void error(Object msg, Throwable throwable) {
        log(getErrorLevel(), msg, throwable);
    }
    
    public void error(String format, Object... args) {
    	log(getErrorLevel(), format, args);
    }
    
    /**
     * Logs a message at the FATAL level
     */
    public void fatal(Object msg) {
        log(getFatalLevel(), msg);
    }
    
    /**
     * Logs a message at the FATAL level
     */
    public void fatal(Object msg, Throwable throwable) {
        log(getFatalLevel(), msg, throwable);
    }
    
    public void fatal(String format, Object... args) {
    	log(getFatalLevel(), format, args);
    }
        
    /**
     * Returns whether logging is enabled at the TRACE level
     */
    public boolean isTraceEnabled() {
        return shouldLog(getTraceLevel());
    }
    
    /**
     * Returns whether logging is enabled at the DEBUG level
     */
    public boolean isDebugEnabled() {
        return shouldLog(getDebugLevel());
    }
    
    /**
     * Returns whether logging is enabled at the INFO level
     */
    public boolean isInfoEnabled() {
        return shouldLog(getInfoLevel());
    }

    /**
     * Returns whether logging is enabled at the WARN level
     */
    public boolean isWarnEnabled() {
        return shouldLog(getWarnLevel());
    }

    /**
     * Returns whether logging is enabled at the ERROR level
     */
    public boolean isErrorEnabled() {
        return shouldLog(getErrorLevel());
    }
    
    /**
     * Returns whether logging is enabled at the FATAL level
     */
    public boolean isFatalEnabled() {
        return shouldLog(getFatalLevel());
    }
    
    protected abstract int getFatalLevel() ;
    protected abstract int getErrorLevel() ;
    protected abstract int getWarnLevel() ;
    protected abstract int getInfoLevel() ;
    protected abstract int getDebugLevel() ;
    protected abstract int getTraceLevel() ;
}
