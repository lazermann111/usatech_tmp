package simple.io.logging;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import simple.bean.ReflectionUtils;
import simple.mail.QueuedEmailer;
/**
 * Provides asynchronous emailing for an application 
 *
 * @author    Brian S. Krug
 */
public class EmailQueueHandler extends Handler {
    protected final QueuedEmailer emailer = new QueuedEmailer();
    protected final EmailFormatter formatter = new EmailFormatter();
    
    /*
     * Creates a new PrintQueueHandler instance
     */
    public EmailQueueHandler() {
    }
                
    public void close() throws SecurityException {
        emailer.finish();
    }
    
    public void flush() {
        emailer.finish();
    }
    
    public void publish(LogRecord record) { 
        Map<String, Object> params = new HashMap<String, Object>();
        JDK14Bridge.gatherParameters(params, ReflectionUtils.getPrevCallerStackElement(Logger.class), record.getLoggerName(), record.getLevel().intValue(), record.getMessage(), record.getThrown());
        //Off-load all formatting to queue so that formatting does not run in main thread and take up processing power
        emailer.queueMessage(new LoggerEmailInfoCreator(params, formatter));
    }
        
    protected static class LoggerEmailInfoCreator implements QueuedEmailer.EmailInfoCreator {
        protected Object logMessage;
        protected EmailFormatter formatter;
        public LoggerEmailInfoCreator(Object _logMessage, EmailFormatter _formatter) {
            logMessage = _logMessage;
            formatter = _formatter;
        }
        public String toString() {
            return formatter.format(logMessage);            
        }        
        public simple.mail.EmailInfo createEmailInfo() throws java.io.IOException {
            return formatter.createEmailInfo(logMessage);
        }        
    }
}
