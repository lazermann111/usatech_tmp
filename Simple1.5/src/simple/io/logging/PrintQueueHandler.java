package simple.io.logging;

import java.beans.IntrospectionException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import simple.bean.ReflectionUtils;
import simple.io.Log;
import simple.io.QueuedOutput;

/**
 * Provides asynchronous logging for an application 
 *
 * @author    Brian S. Krug
 */
public class PrintQueueHandler extends Handler {
    protected final QueuedOutput output = new QueuedOutput();
    protected SimpleFormatter formatter = new SimpleFormatter();
    protected final ReentrantLock lock = new ReentrantLock();
    protected String logFile;
    
    /*
     * Creates a new PrintQueueHandler instance
     */
    public PrintQueueHandler() {
        checkProperties();
    }
        
    public void close() throws SecurityException {
        output.finish();
    }
    
    public void flush() {
        output.finish();
    }
    
    public void publish(LogRecord record) { 
        Map<String, Object> params = new HashMap<String, Object>();
        try {
            JDK14Bridge.gatherParameters(params, ReflectionUtils.getPrevCallerStackElement(Logger.class), record.getLoggerName(), record.getLevel().intValue(), record.getMessage(), record.getThrown());
            //Off-load all formatting to queue so that formatting does not run in main thread and take up processing power
            output.queueMessage(new LazilyFormattedMessage(params, formatter));
        } catch(RuntimeException e) {// we must do something
            String m = "!!! Could not format message: ";
            try { 
                m += simple.bean.ReflectionUtils.getProperty(params, "message"); 
            } catch(IntrospectionException e2) {
            } catch (IllegalAccessException e2) {
            } catch (InvocationTargetException e2) {
            } catch(ParseException e2) {
			}
            output.queueMessage(m + "\n" + simple.text.StringUtils.exceptionToString(e));
            Log.handleLogError(m, e);
        }

    }
    
    /**
     * Reloads properties if they've changed and resets appropriate variables
     */
    protected void checkProperties() {
        try {
            if(Log.checkProperties()) {
                lock.lockInterruptibly();  // block until condition holds
                try {
                    // see if format has changed
                    String pattern = getProperty(formatter.getClass().getName() + ".pattern");
                    if(pattern == null || pattern.trim().length() == 0 ) {
                        if(!formatter.getPattern().equals(SimpleFormatter.DEFAULT_PATTERN))
                            formatter.setPattern(SimpleFormatter.DEFAULT_PATTERN);
                    } else if(!formatter.getPattern().equals(pattern.trim())) {
                        formatter.setPattern(pattern.trim());
                    }
                    
                    //see it file has changed
                    String newLogFile = getProperty(getClass().getName()+ ".file");
                    if(newLogFile == null || logFile.trim().length() == 0) { // use standard out
                        if(output.getOut() != System.out) output.setOut(System.out);
                        logFile = newLogFile;
                    } else try {
                        if(logFile == null || !newLogFile.equals(logFile)) {
                            output.setOut(new PrintStream(new FileOutputStream(newLogFile), true));
                            logFile = newLogFile;
                        }                        
                    } catch(java.io.FileNotFoundException fnfe) {
                        Log.handleLogError("!!! Could not find the log file: " + newLogFile, fnfe);
                    }
                    output.queueMessage("--- BEGIN LOGGING (pattern=" + formatter.getPattern() + ") ---\n");
                } finally {
                    lock.unlock();
                }
            }
        } catch(RuntimeException e) {
            Log.handleLogError("Could not reload properties", e);
        } catch(InterruptedException e) {
        	Log.handleLogError("Could not reload properties", e);
		}
    }

    public String getProperty(String key) {
        return Log.getProperty(key);
    }
    
    protected static class LazilyFormattedMessage {
        protected Object logMessage;
        protected SimpleFormatter formatter;
        public LazilyFormattedMessage(Object _logMessage, SimpleFormatter _formatter) {
            logMessage = _logMessage;
            formatter = _formatter;
        }
        public String toString() {
            return formatter.format(logMessage);            
        }
    }
}
