package simple.io.logging;

import org.apache.log4j.FileAppender;
import org.apache.log4j.spi.LoggingEvent;

public class SecureFileAppender extends FileAppender {
	// prevent CRLF injection
	protected void subAppend(LoggingEvent event) {
		LogUtils.writeToLog(this, this.qw, layout, event);
	}
}
