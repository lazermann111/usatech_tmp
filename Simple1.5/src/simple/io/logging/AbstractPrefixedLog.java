/**
 *
 */
package simple.io.logging;

import simple.io.Log;
import simple.text.MessageFormat;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractPrefixedLog extends Log {
	protected final Log delegate;

	public AbstractPrefixedLog(Log delegate) {
		super();
		this.delegate = delegate;
	}

	protected abstract Object prefixMessage(Object message) ;
	
	@Override
	public void debug(Object message, Throwable throwable) {
		delegate.debug(prefixMessage(message), throwable);
	}

	@Override
	public void debug(Object message) {
		delegate.debug(prefixMessage(message));
	}
	
	@Override
	public void debug(String format, Object... args) {
		Throwable t = null;
		String msg;
		if(args != null && args.length > 0) {
			for(int i = 0; i < args.length; i++) {
				if(args[i] instanceof Throwable) {
					t = (Throwable) args[i];
					break;
				}
			}
			msg = MessageFormat.format(format, args);
		} else
			msg = format;
		debug(msg, t);
	}

	@Override
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	@Override
	public void error(Object message, Throwable throwable) {
		delegate.error(prefixMessage(message), throwable);
	}

	@Override
	public void error(Object message) {
		delegate.error(prefixMessage(message));
	}

	@Override
	public void error(String format, Object... args) {
		Throwable t = null;
		String msg;
		if(args != null && args.length > 0) {
			for(int i = 0; i < args.length; i++) {
				if(args[i] instanceof Throwable) {
					t = (Throwable) args[i];
					break;
				}
			}
			msg = MessageFormat.format(format, args);
		} else
			msg = format;
		error(msg, t);
	}

	@Override
	public void fatal(Object message, Throwable throwable) {
		delegate.fatal(prefixMessage(message), throwable);
	}

	@Override
	public void fatal(Object message) {
		delegate.fatal(prefixMessage(message));
	}

	@Override
	public void fatal(String format, Object... args) {
		Throwable t = null;
		String msg;
		if(args != null && args.length > 0) {
			for(int i = 0; i < args.length; i++) {
				if(args[i] instanceof Throwable) {
					t = (Throwable) args[i];
					break;
				}
			}
			msg = MessageFormat.format(format, args);
		} else
			msg = format;
		fatal(msg, t);
	}
	
	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

	@Override
	public void info(Object message, Throwable throwable) {
		delegate.info(prefixMessage(message), throwable);
	}

	@Override
	public void info(Object message) {
		delegate.info(prefixMessage(message));
	}
	
	@Override
	public void info(String format, Object... args) {
		Throwable t = null;
		String msg;
		if(args != null && args.length > 0) {
			for(int i = 0; i < args.length; i++) {
				if(args[i] instanceof Throwable) {
					t = (Throwable) args[i];
					break;
				}
			}
			msg = MessageFormat.format(format, args);
		} else
			msg = format;
		info(msg, t);
	}
	
	@Override
	public boolean isDebugEnabled() {
		return delegate.isDebugEnabled();
	}

	@Override
	public boolean isErrorEnabled() {
		return delegate.isErrorEnabled();
	}

	@Override
	public boolean isFatalEnabled() {
		return delegate.isFatalEnabled();
	}

	@Override
	public boolean isInfoEnabled() {
		return delegate.isInfoEnabled();
	}

	@Override
	public boolean isTraceEnabled() {
		return delegate.isTraceEnabled();
	}

	@Override
	public boolean isWarnEnabled() {
		return delegate.isWarnEnabled();
	}

	@Override
	public String toString() {
		return delegate.toString();
	}

	@Override
	public void trace(Object message, Throwable throwable) {
		delegate.trace(prefixMessage(message), throwable);
	}

	@Override
	public void trace(Object message) {
		delegate.trace(prefixMessage(message));
	}

	@Override
	public void trace(String format, Object... args) {
		Throwable t = null;
		String msg;
		if(args != null && args.length > 0) {
			for(int i = 0; i < args.length; i++) {
				if(args[i] instanceof Throwable) {
					t = (Throwable) args[i];
					break;
				}
			}
			msg = MessageFormat.format(format, args);
		} else
			msg = format;
		trace(msg, t);
	}

	@Override
	public void warn(Object message, Throwable throwable) {
		delegate.warn(prefixMessage(message), throwable);
	}

	@Override
	public void warn(Object message) {
		delegate.warn(prefixMessage(message));
	}
	
	@Override
	public void warn(String format, Object... args) {
		Throwable t = null;
		String msg;
		if(args != null && args.length > 0) {
			for(int i = 0; i < args.length; i++) {
				if(args[i] instanceof Throwable) {
					t = (Throwable) args[i];
					break;
				}
			}
			msg = MessageFormat.format(format, args);
		} else
			msg = format;
		warn(msg, t);
	}

}
