/*
 * Created on Jun 27, 2005
 *
 */
package simple.io.logging;

import simple.io.Log;
/**
 * This log does nothing. It is used as the initial bridge for the Log class before initialization of the real
 * bridge, so that if the real bridge depends on any class that calls Log.getLog(), a circular dependency
 * does not occur. 
 *
 * @author bkrug
 *
 */
public class NothingBridge implements Bridge {
    protected static final Log nothingLog = new AbstractLog() {        
        @Override
        protected int getEffectiveLevel() {
            return Integer.MAX_VALUE;
        }
        
        @Override
        protected void logMessage(int level, Object msg, Throwable exception) {
        }

        @Override
        protected int getFatalLevel() {
            return 0;
        }

        @Override
        protected int getErrorLevel() {
            return 0;
        }

        @Override
        protected int getWarnLevel() {
            return 0;
        }

        @Override
        protected int getInfoLevel() {
            return 0;
        }

        @Override
        protected int getDebugLevel() {
            return 0;
        }

        @Override
        protected int getTraceLevel() {
            return 0;
        }   
    };
   
    public NothingBridge() {
        super();
    }
    
    public void finish() {
    }

    public Log getLog(String name) {
        return nothingLog;
    }
 
}
