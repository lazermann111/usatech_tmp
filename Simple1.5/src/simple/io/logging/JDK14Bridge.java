/*
 * Created on Jun 27, 2005
 *
 */
package simple.io.logging;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import simple.bean.ReflectionUtils;
import simple.io.Log;

public class JDK14Bridge extends AbstractBridge {
    protected class JDK14Log extends AbstractLog {
        protected Logger delegate;
        public JDK14Log(Logger delegate) {
            this.delegate = delegate;
        }
        @Override
        protected int getEffectiveLevel() {
            return delegate.getLevel().intValue();
        }

        @Override
        protected void logMessage(int level, Object msg, Throwable exception) {
            LogRecord record = new LogRecord(getLevelObject(level), msg.toString());
            record.setThrown(exception);
            if(locationInfo) {
                StackTraceElement ste = ReflectionUtils.getPrevCallerStackElement(AbstractLog.class);
                record.setSourceClassName(ste.getClassName());
                record.setSourceMethodName(ste.getMethodName());
            }
            delegate.log(record);         
        }

        @Override
        protected int getFatalLevel() {
            return Level.ALL.intValue();
        }

        @Override
        protected int getErrorLevel() {
            return Level.SEVERE.intValue();
        }

        @Override
        protected int getWarnLevel() {
            return Level.WARNING.intValue();
        }

        @Override
        protected int getInfoLevel() {
            return Level.INFO.intValue();
        }

        @Override
        protected int getDebugLevel() {
            return Level.FINE.intValue();
        }

        @Override
        protected int getTraceLevel() {
            return Level.FINER.intValue();
        }   
    }
    protected boolean locationInfo = true;
    
    public JDK14Bridge() {
        super();
    }
    
    /**
     * @return Returns the locationInfo.
     */
    public boolean isLocationInfo() {
        return locationInfo;
    }

    /**
     * @param locationInfo The locationInfo to set.
     */
    public void setLocationInfo(boolean locationInfo) {
        this.locationInfo = locationInfo;
    }
    
    @Override
    protected Log createLog(String name) {
        return new JDK14Log(Logger.getLogger(name));
    }
    
    protected static final Map<Integer, Level> levels = new HashMap<Integer, Level>();
    
    public Level getLevelObject(int level) {
        Level l = levels.get(level);
        if(l == null) {
            l = Level.parse("" + level);
            levels.put(level, l);
        }
        return l;
    }

    public static Map<String, Object> gatherParameters(Map<String, Object> params, StackTraceElement source, String identifier, int level, Object message, Throwable exception) {
        putParameter("date", 0, new java.util.Date(), params); //0
        Thread thread = Thread.currentThread();
        ThreadGroup group = thread.getThreadGroup();
        int priority = thread.getPriority();
        putParameter("thread", 1, (thread.getName() == null ? "<UNKNOWN>" : thread.getName()) + "," + priority 
            + (group == null || group.getName() == null ? "" : "," + group.getName()), params);//1
        putParameter("logger", 2, identifier, params); //2    
        putParameter("source", 3, source, params); //3
        putParameter("exceptionObject", 4, exception, params); //4
        putParameter("sequence", 5, getNextSequence(), params); // 5
        putParameter("levelName", 6, SimpleBridge.getLevelName(level), params); //6
        putParameter("message", 7, message, params);//7
        putParameter("exception", 8, (exception == null ? "" : "\n" + simple.text.StringUtils.exceptionToString(exception)), params);//8
        putParameter("threadPriority", 9, new Integer(priority), params);//9
        putParameter("level", 10, new Integer(level), params); //10
        putParameter("threadName", 11, thread.getName(), params);
        putParameter("threadGroup", 12, (group == null ? null : group.getName()), params);
        return params;
    }
    protected static void putParameter(String name, int index, Object value, Map<String, Object> params) {
        params.put(name, value);
        params.put("" + index, value);
    }
    protected static final AtomicLong sequence = new AtomicLong(1);
    protected static Long getNextSequence() {
        return new Long(sequence.getAndIncrement());
    }

    public void finish() {
        
    }
}
