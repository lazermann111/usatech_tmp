package simple.io.logging;

import simple.io.Log;

public class PrefixedLog extends AbstractPrefixedLog 
{
	protected String prefix;

	public PrefixedLog(Log delegate, String prefix)
	{
		super(delegate);
		this.prefix = prefix;
	}

	@Override
	protected Object prefixMessage(Object message)
	{
		return prefix + message;
	}

	public String getPrefix()
	{
		return prefix;
	}

	public void setPrefix(String prefix)
	{
		this.prefix = prefix;
	}
}
