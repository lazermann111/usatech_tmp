/*
 * ApacheLog.java
 *
 * Created on June 16, 2004, 3:01 PM
 */

package simple.io.logging;

import simple.io.Log;

/**
 *
 * @author  Brian S. Krug
 */
public class ApacheLog implements org.apache.commons.logging.Log {
    protected Log delegate;
    /** Creates a new instance of ApacheLog */
    public ApacheLog(Log delegate) {
        super();
        this.delegate = delegate;
    }

    public void debug(Object message) {
        delegate.debug(message);
    }

    public void debug(Object message, Throwable throwable) {
        delegate.debug(message, throwable);
    }

    public void error(Object message) {
        delegate.error(message);
    }

    public void error(Object message, Throwable throwable) {
        delegate.error(message, throwable);
    }

    public void fatal(Object message) {
        delegate.fatal(message);
    }

    public void fatal(Object message, Throwable throwable) {
        delegate.fatal(message, throwable);
    }

    public void info(Object message) {
        delegate.info(message);
    }

    public void info(Object message, Throwable throwable) {
        delegate.info(message, throwable);
    }

    public boolean isDebugEnabled() {
        return delegate.isDebugEnabled();
    }

    public boolean isErrorEnabled() {
        return delegate.isErrorEnabled();
    }

    public boolean isFatalEnabled() {
        return delegate.isFatalEnabled();
    }

    public boolean isInfoEnabled() {
        return delegate.isInfoEnabled();
    }

    public boolean isTraceEnabled() {
        return delegate.isTraceEnabled();
    }

    public boolean isWarnEnabled() {
        return delegate.isWarnEnabled();
    }

    public void trace(Object message) {
        delegate.trace(message);
    }

    public void trace(Object message, Throwable throwable) {
        delegate.trace(message, throwable);
    }

    public void warn(Object message) {
        delegate.warn(message);
    }

    public void warn(Object message, Throwable throwable) {
        delegate.warn(message, throwable);
    }  
}
