/*
 * Created on Feb 11, 2005
 *
 */
package simple.io.logging;

import java.util.HashMap;
import java.util.Map;

import simple.io.QueuedOutput;
import simple.text.StringUtils;

/** Tracks the time it takes to do various tasks
 * @author bkrug
 *
 */
public class PerformanceTracker {
    protected static final char QUOTE = '"';
    protected static final char[] QUOTE_ARR = new char[] {QUOTE};
    protected static final char COMMA = ',';
    protected QueuedOutput output;
    protected static PerformanceTracker instance;
    protected class Task {
        protected String key;
        protected String description;
        protected long start;
    }
    protected Map<String,Task> runningTasks = new HashMap<String,Task>();
    public static PerformanceTracker getDefaultInstance() {
        if(instance == null) instance = new PerformanceTracker();
        return instance;
    }
    public PerformanceTracker() {
        
    }
    
    public void startTask(String key, String description) {
        Task t = new Task();
        t.key = key;
        t.description = description;
        runningTasks.put(key, t);
        t.start = System.currentTimeMillis(); // do this last so it is most accurate
    }
    
    public void endTask(String key) {
        long end = System.currentTimeMillis();
        Task t = runningTasks.get(key);
        if(t != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(QUOTE);
            sb.append(StringUtils.escape(t.key, QUOTE_ARR, QUOTE));
            sb.append(QUOTE);
            sb.append(COMMA);
            sb.append(QUOTE);
            sb.append(StringUtils.escape(t.description, QUOTE_ARR, QUOTE));
            sb.append(QUOTE);
            sb.append(COMMA);
            sb.append(end - t.start);
            getOutput().queueMessage(sb);
        }
    }
    
    public QueuedOutput getOutput() {
        if(output == null) {
            output = new QueuedOutput();
            String file = "task-times.log";
            //output.setOut(System.out);
            try {
                output.setOut(new java.io.PrintStream(new java.io.FileOutputStream(file), true));
            } catch(java.io.FileNotFoundException fnfe) {
                System.out.println("!!! Could not find the file: " + file);
                fnfe.printStackTrace();
                output.setOut(System.out);
            }
        }
        return output;
    }
    
    public void setOutput(QueuedOutput output) {
        this.output = output;
    }
}
