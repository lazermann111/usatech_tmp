/**
 *
 */
package simple.io;

import java.io.File;
import java.io.PrintWriter;
import java.util.concurrent.Future;

import simple.event.ProgressListener;

public interface Interaction {
	public String readLine(String fmt, Object ... args) ;
	public Future<File> browseForFile(String caption, String baseDir, String fmt, Object ... args);
	public char[] readPassword(String fmt, Object ... args) ;
	public void printf(String format, Object ... args) ;
	public ProgressListener createProgressMeter() ; 
	public PrintWriter getWriter() ;
	public Future<String> getLineFuture(String fmt, Object ... args) ;
	public Future<char[]> getPasswordFuture(String fmt, Object ... args) ;

	public int startSection(String sectionDescription);

	public void endSection(int sectionId, boolean success, String msg);

	public void clear();
}