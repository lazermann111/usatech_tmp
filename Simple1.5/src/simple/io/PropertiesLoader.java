/*
 * PropertiesLoader.java
 *
 * Created on January 19, 2004, 9:06 AM
 */

package simple.io;

import java.io.IOException;
import java.util.Properties;

/**  Implementation of the Loader class that loads the InputStream from the ConfigSource
 * into the Properties object provided in the constructor
 *
 * @author  Brian S. Krug
 */
public class PropertiesLoader implements Loader {
    protected Properties properties;
    
    /** Creates a new instance of PropertiesLoader */
    public PropertiesLoader(Properties properties) {
        this.properties = properties;
    }
    
    public void load(ConfigSource src) throws LoadingException {
        // create a new properties set in case an error occurs
        Properties newProps = new Properties();
        try {
            newProps.load(src.getInputStream());
        } catch(IOException ioe) {
            throw new LoadingException("Could not read Properties input stream", ioe);
        }
        // now load them into the real properties object
        synchronized(properties) { // enables thread-safe reloads
            properties.clear();
            properties.putAll(newProps);
        }
    }
    
}
