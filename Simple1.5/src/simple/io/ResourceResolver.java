/*
 * Created on Feb 15, 2005
 *
 */
package simple.io;

import java.io.IOException;
import java.net.URL;

/**
 * @author bkrug
 *
 */
public interface ResourceResolver {
    public URL getResourceURL(String path) throws IOException ;
}
