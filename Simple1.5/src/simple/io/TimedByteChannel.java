package simple.io;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public class TimedByteChannel implements ByteChannel {
	private static final Log log = Log.getLog();
	protected final SocketChannel delegate;
	protected int readTimeout;
	protected int writeTimeout;
	protected final Selector readSelector;
	protected final Selector writeSelector;
	protected final SelectionKey readSelectionKey;
	protected final SelectionKey writeSelectionKey;
	protected final String socketInfo;
	public TimedByteChannel(SocketChannel delegate, int readTimeout, int writeTimeout) throws IOException {
		delegate.configureBlocking(false);
		this.delegate = delegate;
		this.readTimeout = readTimeout;
		this.writeTimeout = writeTimeout;
		this.readSelector = delegate.provider().openSelector();
		this.writeSelector = delegate.provider().openSelector();
		this.readSelectionKey = delegate.register(readSelector, SelectionKey.OP_READ);
		this.writeSelectionKey = delegate.register(writeSelector, SelectionKey.OP_WRITE);
		Socket socket = delegate.socket();
		if(socket == null)
			socketInfo = "CLOSED SOCKET";
		else
			socketInfo = socket.getRemoteSocketAddress().toString() + " from port " + socket.getLocalPort();
		log.info("Opened selectors and keys for connection to " + socketInfo);
	}

	@Override
	public int read(ByteBuffer dst) throws IOException {
		// NOTE: we assume that delegate is still blocking and noone else is using it
		// Copied from sun.nio.ch.SocketAdaptor and adapted
		// Implement timeout with a selector
		if(!dst.hasRemaining())
			return 0;
		int n;
		if((n = delegate.read(dst)) != 0)
			return n;
		try {
			long to = getReadTimeout();
			final long expire;
			if(to > 0)
				expire = System.currentTimeMillis() + to;
			else
				expire = 0;
			for(;;) {
				if(!delegate.isOpen())
					throw new ClosedChannelException();
				int ns = readSelector.select(to);
				if(ns > 0 && readSelectionKey.isReadable()) {
					if((n = delegate.read(dst)) != 0)
						return n;
				}
				readSelector.selectedKeys().remove(readSelectionKey);
				if(expire > 0) {
					long now = System.currentTimeMillis();
					if(now >= expire)
						throw new SocketTimeoutException();
					to = expire - now;
				}
			}
		} finally {
			readSelector.selectNow(); // Flush cancelled keys
		}
	}

	@Override
	public boolean isOpen() {
		return delegate.isOpen();
	}

	@Override
	public void close() throws IOException {
		log.info("Closing selectors and keys for connection to " + socketInfo);
		try {
			readSelector.close();
		} finally {
			try {
				writeSelector.close();
			} finally {
				delegate.close();
			}
		}
	}

	@Override
	public int write(ByteBuffer src) throws IOException {
		// NOTE: we assume that delegate is still blocking and noone else is using it
		// Implement timeout with a selector
		if(!src.hasRemaining())
			return 0;
		int n = delegate.write(src);
		if(n < 0 || (n > 0 && !src.hasRemaining()))
			return n;
		int total = n;
		try {
			long to = getWriteTimeout();
			final long expire;
			if(to > 0)
				expire = System.currentTimeMillis() + to;
			else
				expire = 0;
			for(;;) {
				if(!delegate.isOpen())
					throw new ClosedChannelException();
				int ns = writeSelector.select(to);
				if(ns > 0 && writeSelectionKey.isWritable()) {
					n = delegate.write(src);
					if(n < 0) {
						if(total > 0)
							return total;
						return n;
					}
					total += n;
					if(n > 0 && !src.hasRemaining())
						return total;
				}
				writeSelector.selectedKeys().remove(writeSelectionKey);
				if(expire > 0) {
					long now = System.currentTimeMillis();
					if(now >= expire)
						throw new SocketTimeoutException();
					to = expire - now;
				}
			}
		} finally {
			writeSelector.selectNow(); // Flush cancelled keys
		}
	}

	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(int readTimeout) throws IOException {
		if(readTimeout < 0)
			throw new IOException("Timeout must be greater than or equal to zero");
		this.readTimeout = readTimeout;
		this.readSelector.wakeup();
	}

	public int getWriteTimeout() {
		return writeTimeout;
	}

	public void setWriteTimeout(int writeTimeout) throws IOException {
		if(writeTimeout < 0)
			throw new IOException("Timeout must be greater than or equal to zero");
		this.writeTimeout = writeTimeout;
		this.writeSelector.wakeup();
	}
}
