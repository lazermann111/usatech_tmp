package simple.io;

import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import simple.bean.ConvertUtils;
import simple.db.helpers.PostgresHelper;
import simple.text.StringUtils;

public abstract class PGTextOutputter {
	protected static final char DEFAULT_EOL = '\n';
	protected static final char DEFAULT_DELIMITER = '\t';
	protected static final char[] DEFAULT_NULL_CHARS = "\\N".toCharArray();

	protected char eol = DEFAULT_EOL;
	protected char delimiter = DEFAULT_DELIMITER;
	protected char[] nullChars = DEFAULT_NULL_CHARS;
	protected DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZZ");
	protected static final char SLASH = '\\';
	protected static final char X = 'x';
	protected char TRUE_CHAR = 't';
	protected char FALSE_CHAR = 'f';

	protected String[] columnNames;
	protected String tableName;
	protected Writer writer;

	public PGTextOutputter() {
	}

	public int writeData() throws IOException {
		if(writer == null)
			throw new IllegalStateException("writer property is null");
		int tot = 0;
		Object[] data;
		while((data = nextRow()) != null) {
			for(int i = 0; i < data.length; i++) {
				if(i > 0) {
					writer.write(delimiter);
					tot++;
				}
				tot += writeEscapedData(data[i]);
			}
			writer.write(eol);
			tot++;
		}
		writer.flush();
		return tot;
	}

	protected int writeEscapedData(Object o) throws IOException {
		if(o == null) {
			writer.write(nullChars);
			return nullChars.length;
		}
		if(o instanceof byte[]) {
			writer.write(SLASH);
			writer.write(SLASH);
			writer.write(X);
			byte[] bytes = (byte[]) o;
			for(int i = 0; i < bytes.length; i++) {
				writer.write(StringUtils.hexDigit1(bytes[i]));
				writer.write(StringUtils.hexDigit2(bytes[i]));
			}
			return 3 + (bytes.length * 2);
		}
		if(o instanceof Boolean) {
			writer.write(((Boolean) o).booleanValue() ? TRUE_CHAR : FALSE_CHAR);
			return 1;
		}
		if(o instanceof Number) {
			String s = o.toString();
			writer.write(s);
			return s.length();
		}
		if(o instanceof Date) {
			String s = dateFormat.format(o);
			return writeEscaped(s);
		}
		if(ConvertUtils.isCollectionType(o.getClass())) {
			return writeEscaped(PostgresHelper.arrayToString(o));
		}
		return writeEscaped(o.toString());
	}

	protected int writeEscaped(String s) throws IOException {
		if(s.isEmpty())
			return 0;
		int tot = 0;
		for(int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			switch(ch) {
				default:
					if(ch != delimiter && ch != eol)
						break;
				case '\n':
				case '\r':
				case SLASH:
					writer.write(SLASH);
					tot++;
					break;
			}
			writer.write(ch);
			tot++;
		}
		return tot;
	}

	protected abstract Object[] nextRow();

	public String getCopySql(Charset encoding) {
		StringBuilder sb = new StringBuilder();
		sb.append("COPY ").append(tableName);
		if(columnNames != null && columnNames.length > 0) {
			sb.append('(');
			for(int i = 0; i < columnNames.length; i++) {
				if(i > 0)
					sb.append(',');
				sb.append(columnNames[i]);
			}
			sb.append(')');			
		}
		sb.append(" FROM STDIN (FORMAT TEXT");
		if(delimiter != DEFAULT_DELIMITER) {
			sb.append(", DELIMITER '");
			appendPGEscaped(delimiter, sb);
			sb.append('\'');
		}
		if(!Arrays.equals(nullChars, DEFAULT_NULL_CHARS)) {
			sb.append(", NULL '");
			for(char ch : nullChars)
				appendPGEscaped(ch, sb);
			sb.append('\'');
		}
		if(encoding != null) {
			sb.append(", ENCODING '").append(encoding.name()).append('\'');
		}

		sb.append(')');
		return sb.toString();
	}

	protected void appendPGEscaped(char ch, StringBuilder sb) {
		switch(ch) {
			case '\b':
				sb.append("\\b");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '\\':
				sb.append("\\\\");
				break;
			case 0:
				sb.append("\\x00");
				break;
			default:
				sb.append(ch);
		}
	}

	public char getEol() {
		return eol;
	}

	public void setEol(char eol) {
		this.eol = eol;
	}

	public char getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(char delimiter) {
		this.delimiter = delimiter;
	}

	public char[] getNullChars() {
		return nullChars;
	}

	public void setNullChars(char[] nullChars) {
		this.nullChars = nullChars;
	}

	public DateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(DateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String[] getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Writer getWriter() {
		return writer;
	}

	public void setWriter(Writer writer) {
		this.writer = writer;
	}
}
