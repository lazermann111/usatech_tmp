/*
 * Created on Dec 8, 2005
 *
 */
package simple.io;

import java.io.IOException;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;

/** This implementation is NOT thread-safe.
 * @author Brian S. Krug
 *
 */
public class ByteChannelWriter extends Writer {
	protected CharsetEncoder encoder;
    protected final WritableByteChannel channel;
    protected final ByteBuffer buffer;
    protected final CharBuffer charBufferOne = CharBuffer.allocate(1);

    public ByteChannelWriter(WritableByteChannel channel, CharsetEncoder encoder) {
    	this(channel, encoder, 1024);
    }

    public ByteChannelWriter(WritableByteChannel channel, CharsetEncoder encoder, int bufferSize) {
        this(channel, encoder, ByteBuffer.allocate(bufferSize));
    }

    protected ByteChannelWriter(WritableByteChannel channel, CharsetEncoder encoder, ByteBuffer buffer) {
        super();
        this.channel = channel;
        this.buffer = buffer;
        this.encoder = encoder;
    }

    @Override
    public void flush() throws IOException {
    	charBufferOne.position(charBufferOne.limit());
    	CoderResult result = encoder.encode(charBufferOne, buffer, true);
    	while(result.isOverflow()) {
    		flushBuffer();
    		result = encoder.encode(charBufferOne, buffer, true);
    	}
    	result = encoder.flush(buffer);
		boolean okay = false;
		try {
			flushBuffer();
			okay = true;
		} finally {
			if(okay && result.isOverflow()) {
				flush();
			} else {
				encoder.reset();
			}
    	}
    }

    protected void flushBuffer() throws IOException {
    	buffer.flip();
		while(buffer.hasRemaining())
			channel.write(buffer);
        buffer.clear();
    }

    @Override
    public void write(char[] cbuf, int offset, int length) throws IOException {
    	write(CharBuffer.wrap(cbuf, offset, length));
    }

    @Override
    public void write(String str, int off, int len) throws IOException {
    	write(CharBuffer.wrap(str, off, len));
    }

    @Override
	public void write(int c) throws IOException {
		charBufferOne.put(0, (char) c);
		charBufferOne.position(0);
    	write(charBufferOne);
    }

    protected int write(CharBuffer charBuffer) throws IOException {
    	int written = -buffer.position();
    	CoderResult result = encoder.encode(charBuffer, buffer, false);
    	written += buffer.position();
    	while(result.isOverflow()) {
    		flushBuffer();
    		result = encoder.encode(charBuffer, buffer, false);
    		written += buffer.position();
    	}
    	return written;
    }

	@Override
	public void close() throws IOException {
		flush();
		channel.close();
	}

	public CharsetEncoder getEncoder() {
		return encoder;
	}

	public void setEncoder(CharsetEncoder encoder) throws IOException {
		if(encoder == null) throw new NullPointerException("Encoder cannot be null");
		flush();
		this.encoder = encoder;
	}
}
