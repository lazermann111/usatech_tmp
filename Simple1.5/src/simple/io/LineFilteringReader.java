package simple.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class LineFilteringReader extends Reader {
	protected final BufferedReader delegate;
	protected char[] line;
	protected int pos = -1;
	protected final char[] newline;

	public LineFilteringReader(BufferedReader delegate, String newline) {
		super();
		this.delegate = delegate;
		this.newline = newline.toCharArray();
	}

	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		int tot = 0;
		while(tot < len) {
			if(pos == -1 || pos >= line.length + newline.length) {
				String str = delegate.readLine();
				if(str == null) {
					if(tot > 0)
						return tot;
					return -1;
				}
				line = filter(str).toCharArray();
				pos = 0;
			}
			if(pos < line.length) {
				int r = Math.min(len - tot, line.length - pos);
				System.arraycopy(line, pos, cbuf, off + tot, r);
				pos += r;
				tot += r;
			} else if(pos < line.length + newline.length) {
				int r = Math.min(len - tot, line.length + newline.length - pos);
				System.arraycopy(newline, pos - line.length, cbuf, off + tot, r);
				pos += r;
				tot += r;
			} else {
				pos = -1;
			}
		}
		return tot;
	}

	protected String filter(String line) {
		return line;
	}

	@Override
	public void mark(int readAheadLimit) throws IOException {
		delegate.mark(readAheadLimit);
	}

	@Override
	public boolean markSupported() {
		return delegate.markSupported();
	}

	@Override
	public boolean ready() throws IOException {
		return delegate.ready();
	}

	@Override
	public void reset() throws IOException {
		delegate.reset();
	}

	@Override
	public long skip(long n) throws IOException {
		return delegate.skip(n);
	}

	@Override
	public void close() throws IOException {
		delegate.close();
	}

}
