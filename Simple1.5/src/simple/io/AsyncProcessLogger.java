package simple.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import simple.util.concurrent.CustomThreadFactory;

public class AsyncProcessLogger {
	public static enum LogLevel {
		NONE {
			@Override
			public void log(Log log, Object message, Throwable throwable) {
				// do nothing
			}

			@Override
			public boolean shouldLog(Log log) {
				return false;
			}
		},
		TRACE {
			@Override
			public void log(Log log, Object message, Throwable throwable) {
				log.trace(message, throwable);
			}

			@Override
			public boolean shouldLog(Log log) {
				return log.isTraceEnabled();
			}
		},
		DEBUG {
			@Override
			public void log(Log log, Object message, Throwable throwable) {
				log.debug(message, throwable);
			}

			@Override
			public boolean shouldLog(Log log) {
				return log.isDebugEnabled();
			}
		},
		INFO {
			@Override
			public void log(Log log, Object message, Throwable throwable) {
				log.info(message, throwable);
			}

			@Override
			public boolean shouldLog(Log log) {
				return log.isInfoEnabled();
			}
		},
		WARN {
			@Override
			public void log(Log log, Object message, Throwable throwable) {
				log.warn(message, throwable);
			}

			@Override
			public boolean shouldLog(Log log) {
				return log.isWarnEnabled();
			}
		},
		ERROR {
			@Override
			public void log(Log log, Object message, Throwable throwable) {
				log.error(message, throwable);
			}

			@Override
			public boolean shouldLog(Log log) {
				return log.isErrorEnabled();
			}
		},
		FATAL {
			@Override
			public void log(Log log, Object message, Throwable throwable) {
				log.fatal(message, throwable);
			}

			@Override
			public boolean shouldLog(Log log) {
				return log.isFatalEnabled();
			}
		};

		public abstract void log(Log log, Object message, Throwable throwable);

		public abstract boolean shouldLog(Log log);
	}

	protected static class Loader {
		public static Executor executor = new ThreadPoolExecutor(0, 1000, 5000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new CustomThreadFactory("AsyncProcessLogger-", true));
	}

	public static void logProcessStreams(final Process process, final String processName, final LogLevel errorLogLevel, final LogLevel outputLogLevel) {
		logProcessStreams(process, Log.getLog("/" + processName + "/"), Loader.executor, errorLogLevel, outputLogLevel);
	}

	public static void logProcessStreams(final Process process, final Log log, final Executor executor, final LogLevel errorLogLevel, final LogLevel outputLogLevel) {
		if(errorLogLevel != LogLevel.NONE)
			executor.execute(new Runnable() {
				@Override
				public void run() {
					logStream(process.getErrorStream(), errorLogLevel, log);
				}
			});
		if(outputLogLevel != LogLevel.NONE)
			executor.execute(new Runnable() {
				@Override
				public void run() {
					logStream(process.getInputStream(), outputLogLevel, log);
				}
			});
	}

	protected static void logStream(InputStream stream, LogLevel logLevel, Log log) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		String line;
		try {
			while((line = reader.readLine()) != null) {
				if(logLevel.shouldLog(log))
					logLevel.log(log, line, null);
			}
		} catch(IOException e) {
			if(logLevel.shouldLog(log))
				logLevel.log(log, "Could not read from stream", e);
		}
	}

}
