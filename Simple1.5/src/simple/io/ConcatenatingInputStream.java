/*
 * Created on Mar 15, 2006
 *
 */
package simple.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author bkrug
 *
 */
public class ConcatenatingInputStream extends InputStream {
    protected final InputStream[] inputStreams;
    protected int index;
    protected int resetIndex;

    /**
     *
     */
    public ConcatenatingInputStream(InputStream... inputStreams) {
        super();
        this.inputStreams = inputStreams;
    }

    @Override
    public boolean markSupported() {
    	for(int i = 0; i < inputStreams.length; i++) {
            if(!inputStreams[i].markSupported())
            	return false;
        }
    	return true;
    }

    @Override
    public void mark(int readlimit) {
    	resetIndex = index;
    	for(int i = resetIndex; i < inputStreams.length; i++) {
            inputStreams[i].mark(readlimit);
        }
    }

    @Override
    public void reset() throws IOException {
    	for(int i = inputStreams.length - 1; i >= resetIndex; i--) {
            inputStreams[i].reset();
        }
    	index = resetIndex;
    }

    /**
     * @see java.io.InputStream#read()
     */
    @Override
    public int read() throws IOException {
        if(index < inputStreams.length) {
            int ret = inputStreams[index].read();
            if(ret == -1) {
                index++;
                return read();
            } else {
            	return ret;
            }
        }
        return -1;
    }

    @Override
    public int available() throws IOException {
        int available = 0;
        for(int i = index; i < inputStreams.length; i++) {
            available += inputStreams[i].available();
        }
        return available;
    }

    @Override
    public void close() throws IOException {
        for(int i = 0; i < inputStreams.length; i++) {
            inputStreams[i].close();
        }
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if(len == 0) return 0;
        int read = 0;
        while(read < len && index < inputStreams.length) {
            int r = inputStreams[index].read(b, off + read, len - read);
            if(r == -1) index++;
            else read += r;
        }
        return read > 0 ? read : -1;
    }

    public int getCurrentIndex() {
        return index;
    }

    public void setCurrentIndex(int index) {
    	if(index < 0 || index >= inputStreams.length)
    		throw new IndexOutOfBoundsException("Index '" + index + "' is not valid");
    	this.index = index;
    }

    @Override
    public long skip(long n) throws IOException {
    	if(n == 0) return 0;
        long skipped = 0;
        while(skipped < n && index < inputStreams.length) {
            long sk = inputStreams[index].skip(n - skipped);
            if(sk == -1) index++;
            else skipped += sk;
 		}
        return skipped > 0 ? skipped : -1;
    }
}
