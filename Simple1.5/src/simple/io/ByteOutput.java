package simple.io;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface ByteOutput {

	/**
	 * Writes out the string to the underlying output stream as a
	 * sequence of bytes. Each character in the string is written out, in
	 * sequence, by discarding its high eight bits.
	 *
	 * @param      s   a string of bytes to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeBytes(String s) throws IOException;

	/**
	 * Writes a string to the underlying output stream as a sequence of
	 * characters. Each character is written to the data output stream as
	 * if by the <code>writeChar</code> method. If no exception is
	 * thrown, the counter <code>written</code> is incremented by twice
	 * the length of <code>s</code>.
	 *
	 * @param      s   a <code>String</code> value to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.DataOutputStream#writeChar(int)
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeChars(String s) throws IOException;

	/**
	 * Writes out a <code>byte</code> to the underlying output stream as
	 * a 1-byte value.
	 *
	 * @param      b   a <code>byte</code> value to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeByte(int b) throws IOException;

	/**
	 * Writes a <code>short</code> to the underlying output stream as two
	 * bytes, high byte first.
	 *
	 * @param      s   a <code>short</code> to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeShort(int s) throws IOException;

	/**
	 * Writes a <code>char</code> to the underlying output stream as a
	 * 2-byte value, high byte first.
	 *
	 * @param      v   a <code>char</code> value to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeChar(int c) throws IOException;

	/**
	 * Writes an <code>int</code> to the underlying output stream as four
	 * bytes, high byte first.
	 *
	 * @param      v   an <code>int</code> to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeInt(int i) throws IOException;

	/**
	 * Writes a <code>long</code> to the underlying output stream as eight
	 * bytes, high byte first.
	 *
	 * @param      l   a <code>long</code> to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeLong(long l) throws IOException;

	/**
	 * Converts the float argument to an <code>int</code> using the
	 * <code>floatToIntBits</code> method in class <code>Float</code>,
	 * and then writes that <code>int</code> value to the underlying
	 * output stream as a 4-byte quantity, high byte first.
	 *
	 * @param      f   a <code>float</code> value to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 * @see        java.lang.Float#floatToIntBits(float)
	 */
	public void writeFloat(float f) throws IOException;

	/**
	 * Converts the double argument to a <code>long</code> using the
	 * <code>doubleToLongBits</code> method in class <code>Double</code>,
	 * and then writes that <code>long</code> value to the underlying
	 * output stream as an 8-byte quantity, high byte first.
	 *
	 * @param      d   a <code>double</code> value to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 * @see        java.lang.Double#doubleToLongBits(double)
	 */
	public void writeDouble(double d) throws IOException;

	/**
	 * Writes out the string to the underlying output stream as a
	 * sequence of bytes. Each character in the string is written out, in
	 * sequence, by discarding its high eight bits.
	 *
	 * @param      s   a string of bytes to be written.
	 * @param      length The number of bytes to write
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeBytes(String s, int length) throws IOException;

	/**
	 * Writes out the string to the underlying output stream as a
	 * sequence of bytes. Each character in the string is written out, in
	 * sequence, by discarding its high eight bits.
	 *
	 * @param      s   a string of bytes to be written.
	 * @param      offset
	 * @param      length The number of bytes to write
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeBytes(String s, int offset, int length) throws IOException;

	/**
	 * Writes out the array of bytes to the underlying output stream.
	 *
	 * @param      bytes   the bytes to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void write(byte[] bytes) throws IOException;

	/**
	 * Writes out the array of bytes to the underlying output stream.
	 *
	 * @param      bytes   the bytes to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void write(ByteBuffer buffer) throws IOException;

	/**
	 * Writes out the array of bytes to the underlying output stream.
	 *
	 * @param      bytes the bytes to be written.
	 * @param      offset
	 * @param      length The number of bytes to write
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.FilterOutputStream#out
	 */
	public void write(byte[] bytes, int offset, int length) throws IOException;
	/**
	 * Writes a string to the underlying output stream as a sequence of
	 * characters. Each character is written to the data output stream as
	 * if by the <code>writeChar</code> method. If no exception is
	 * thrown, the counter <code>written</code> is incremented by twice
	 * the length of <code>s</code>.
	 *
	 * @param      s   a <code>String</code> value to be written.
	 * @exception  IOException  if an I/O error occurs.
	 * @see        java.io.DataOutputStream#writeChar(int)
	 * @see        java.io.FilterOutputStream#out
	 */
	public void writeChars(String s, int length) throws IOException;

	public void flush() throws IOException ;
}