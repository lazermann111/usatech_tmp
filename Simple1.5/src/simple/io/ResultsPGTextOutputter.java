package simple.io;

import simple.results.Results;

public class ResultsPGTextOutputter extends PGTextOutputter {
	protected final Results results;
	protected final Object[] cache;

	public ResultsPGTextOutputter(Results results) {
		super();
		this.results = results;
		this.cache = new Object[results.getColumnCount()];
	}

	@Override
	protected Object[] nextRow() {
		if(!results.next())
			return null;
		for(int i = 0; i < cache.length; i++)
			cache[i] = results.getValue(i + 1);
		return cache;
	}

}
