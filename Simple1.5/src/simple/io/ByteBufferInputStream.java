package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/** This implementation is NOT thread-safe.
 * @author Brian S. Krug
 *
 */
public class ByteBufferInputStream extends InputStream {
	protected final ByteBuffer buffer;

	public ByteBufferInputStream(ByteBuffer buffer) {
        super();
        this.buffer = buffer;
    }

	@Override
	public int available() throws IOException {
		return buffer.remaining();
	}

	@Override
	public void mark(int readlimit) {
		buffer.mark();
	}
	@Override
	public boolean markSupported() {
		return true;
	}
	@Override
	public void reset() throws IOException {
		buffer.reset();
	}
	@Override
	public long skip(long n) throws IOException {
		int ret = Math.min(buffer.remaining(), (int)n);
		buffer.position(ret + buffer.position());
		return ret;
	}
	@Override
	public int read(byte[] b, int offset, int length) throws IOException {
		if(buffer.remaining() == 0)
			return -1;
		int len = Math.min(length, buffer.remaining());
		buffer.get(b, offset, len);
		return len;
	}

	@Override
	public int read() throws IOException {
		if(buffer.remaining() == 0)
			return -1;
		return buffer.get();
	}
}
