package simple.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;

public abstract class AbstractByteInput implements ByteInput {
	protected final byte readBuffer[] = new byte[1024];
	protected int position;
	protected AbstractByteInput() {
		super();
	}

	protected abstract int read() throws IOException ;
	protected abstract int read(byte[] buf, int offset, int length) throws IOException ;

	protected int readAndCheck() throws IOException {
		int ch = read();
		if(ch < 0)
			throw new EOFException();
		position++;
		return ch;
	}

	protected void readAndCheck(byte[] buf, int offset, int length) throws IOException {
		if(length < 1)
			return;
		while(true) {
			int len = read(buf, offset, length);
			if(len < 0)
				throw new EOFException();
			if(len > 0)
				position += len;
			if(len < length) {
				offset += len;
				length -= len;
			} else
				return;
		} 
	}

	protected byte[] readAndCheck(int length, boolean useBuffer) throws IOException {
		byte[] ret;
		if(!useBuffer || readBuffer.length < length) {
			if(length > length() - position)
				throw new EOFException();
			ret = new byte[length];
		} else
			ret = readBuffer;
		readAndCheck(ret, 0, length);
		return ret;
	}

	public void reset() throws IOException {
		position = 0;
	}

	/**
	 * @see simple.io.ByteInput#readBytes(int)
	 */
	public String readBytes(int length) throws IOException {
		return new String(readAndCheck(length, true), 0, length);
	}

	/**
	 * @see simple.io.ByteInput#readChars(int)
	 */
	public String readChars(int length) throws IOException {
		if(length * 2 > length() - position)
			throw new EOFException();
		char[] buf = new char[length];
		//TODO: could be more efficient by using byte array
		for(int i = 0; i < length; i++)
			buf[i] = readChar();
		return new String(buf, 0, length);
	}

	/**
	 * @see simple.io.ByteInput#readChar()
	 */
	public char readChar() throws IOException {
		return (char)readUnsignedShort();
    }

	/**
	 * @see simple.io.ByteInput#readByte()
	 */
	public byte readByte() throws IOException {
		return (byte) readAndCheck();
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedByte()
	 */
	public int readUnsignedByte() throws IOException {
		return readAndCheck();
	}

	/**
	 * @see simple.io.ByteInput#readShort()
	 */
	public short readShort() throws IOException {
		return (short)readUnsignedShort();
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedShort()
	 */
	public int readUnsignedShort() throws IOException {
		byte[] bytes = readAndCheck(2, true);
		return (((bytes[0]& 0xff) << 8) + ((bytes[1]& 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readByteAsChar()
	 */
	public char readByteAsChar() throws IOException {
		return (char) readUnsignedByte();
	}

	/**
	 * @see simple.io.ByteInput#readInt()
	 */
	public int readInt() throws IOException {
		byte[] bytes = readAndCheck(4, true);
		return (((bytes[0]& 0xff) << 24) + ((bytes[1]& 0xff) << 16) + ((bytes[2]& 0xff) << 8) + ((bytes[3]& 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedInt()
	 */
	public long readUnsignedInt() throws IOException {
		byte[] bytes = readAndCheck(4, true);
		return (((long) (bytes[0] & 0xff) << 24) + ((bytes[1]& 0xff) << 16) + ((bytes[2]& 0xff) << 8) + ((bytes[3]& 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readLong()
	 */
	public long readLong() throws IOException {
		byte[] bytes = readAndCheck(8, true);
		return (((long) bytes[0] << 56) + ((long) (bytes[1] & 255) << 48)
				+ ((long) (bytes[2] & 255) << 40) + ((long) (bytes[3] & 255) << 32)
				+ ((long) (bytes[4] & 255) << 24) + ((bytes[5] & 255) << 16)
				+ ((bytes[6] & 255) << 8) + ((bytes[7] & 255) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readFloat()
	 */
	public float readFloat() throws IOException {
		return Float.intBitsToFloat(readInt());
	}

	/**
	 * @see simple.io.ByteInput#readDouble()
	 */
	public double readDouble() throws IOException {
		return Double.longBitsToDouble(readLong());
	}


	public void readBytes(byte[] buffer, int offset, int length) throws IOException {
		readAndCheck(buffer, offset, length);
	}

	public int position() {
		return position;
	}

	public int copyInto(OutputStream output) throws IOException {
		int tot = 0;
		int rem;
        for(int len = 0; (rem = Math.min(readBuffer.length, length() - position())) > 0 && (len=read(readBuffer, 0, rem)) >= 0; tot += len)
        	output.write(readBuffer, 0, len);
        output.flush();
        return tot;
	}

	public int copyInto(ByteOutput output) throws IOException {
		int tot = 0;
		int rem;
        for(int len = 0; (rem = Math.min(readBuffer.length, length() - position())) > 0 && (len=read(readBuffer, 0, rem)) >= 0; tot += len)
        	output.write(readBuffer, 0, len);
        output.flush();
        return tot;
	}
}
