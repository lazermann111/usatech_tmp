/**
 *
 */
package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class ToHexReader extends Reader {
	protected final InputStream in;
	protected char leftover;
	protected boolean hasLeftover = false;
	/**
	 *
	 */
	public ToHexReader(InputStream in) {
		this.in = in;
	}

	/**
	 * @see java.io.Reader#close()
	 */
	@Override
	public void close() throws IOException {
		in.close();
	}

	/**
	 * @see java.io.Reader#read(char[], int, int)
	 */
	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		int l = len / 2;
		if((len % 2) == 1 && !hasLeftover)
			l++;
		int orig = off;
		if(hasLeftover) {
			cbuf[off++] = leftover;
		}
		hasLeftover = false;
		if(l > 0) {
			byte[] bytes = new byte[l];
			int r = in.read(bytes);
			if(r <= 0)
				return r;
			if(r > len / 2) {
				cbuf[off + len - 1] = StringUtils.hexDigit1(bytes[r-1]);
				leftover = StringUtils.hexDigit2(bytes[r-1]);
				hasLeftover = true;
				orig--;
				r--;
			}
			for(int i = 0; i < r; i++) {
				cbuf[off++] = StringUtils.hexDigit1(bytes[i]);
				cbuf[off++] = StringUtils.hexDigit2(bytes[i]);
			}
		}

		return off-orig;
	}
	/**
	 * @see java.io.Reader#mark(int)
	 */
	@Override
	public void mark(int readAheadLimit) throws IOException {
		in.mark(1 + readAheadLimit / 2);
	}

	/**
	 * @see java.io.Reader#markSupported()
	 */
	@Override
	public boolean markSupported() {
		return in.markSupported();
	}
	/**
	 * @see java.io.Reader#read()
	 */
	@Override
	public int read() throws IOException {
		if(hasLeftover) {
			hasLeftover = false;
			return leftover;
		} else {
			int r = in.read();
			if(r <= 0)
				return r;
			hasLeftover = true;
			leftover = StringUtils.hexDigit2((byte)r);
			return StringUtils.hexDigit1((byte)r);
		}
	}

	/**
	 * @see java.io.Reader#ready()
	 */
	@Override
	public boolean ready() throws IOException {
		return hasLeftover || in.available() > 0;
	}
	/**
	 * @see java.io.Reader#reset()
	 */
	@Override
	public void reset() throws IOException {
		in.reset();
	}
	/**
	 * @see java.io.Reader#skip(long)
	 */
	@Override
	public long skip(long n) throws IOException {
		return in.skip(n / 2);
	}
}
