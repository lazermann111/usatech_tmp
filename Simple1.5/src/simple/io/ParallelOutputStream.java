/*
 * PassThroughOutputStream.java
 *
 * Created on December 8, 2003, 9:12 AM
 */

package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;

import simple.lang.SystemUtils;


/**
 * Writes to several OutputStreams in parallel. The write() methods of this class are NOT Thread-safe.
 * @author  Brian S. Krug
 */
public class ParallelOutputStream extends OutputStream {
	private static Log log = Log.getLog();
	protected final ExecutorService executor;
	protected final byte[] oneByte = new byte[1];

	protected volatile byte[] pending;
	protected volatile int pendingOffset;
	protected volatile int pendingLength;

	protected final Collection<WriteTask> writeTasks = new ArrayList<WriteTask>();
    protected class WriteTask extends ReentrantLock implements Callable<Integer>  {
		private static final long serialVersionUID = 1238089513343706149L;
		protected final OutputStream out;
    	public WriteTask(OutputStream out) {
    		this.out = out;
    	}
		public Integer call() throws IOException {
			lock();
			try {
				if(pending == null) {
					out.flush();
					return 0;
				} else {
					out.write(pending, pendingOffset, pendingLength);
					return pendingLength;
				}
			} finally {
				unlock();
			}
		}
		public void close() throws IOException {
			lock();
			try {
				out.close();
			} finally {
				unlock();
			}
		}

		public Thread getOwner() {
			return super.getOwner();
		}
		@Override
		public int hashCode() {
			return out.hashCode();
		}
		@Override
		public boolean equals(Object obj) {
			return obj instanceof WriteTask && out.equals(((WriteTask)obj).out);
		}
	};

    public ParallelOutputStream(ExecutorService executor) {
		super();
		this.executor = executor;
	}

    public void addOutputStream(OutputStream out) {
    	if(out == null)
			throw new NullPointerException();
    	writeTasks.add(new WriteTask(out));
    }

    public void removeOutputStream(OutputStream out) {
    	if(out == null)
			throw new NullPointerException();
    	writeTasks.remove(new WriteTask(out));
    }

    @Override
	public void write(int b) throws IOException {
    	oneByte[0] = (byte)b;
    	pending = oneByte;
    	pendingOffset = 0;
    	pendingLength = 1;
    	executeAll();
    }

    @Override
	public void close() throws IOException {
    	IOException exception = null;
        for(WriteTask writeTask : writeTasks)
        	try {
        		writeTask.close();
        	} catch(IOException e) {
        		exception = e;
        	}
        if(exception != null)
        	throw exception;
    }

    @Override
	public void flush() throws IOException {
		if(pending != null) {
			pending = null;
			executeAll();
		}
    }

    @Override
	public void write(byte[] b, int off, int len) throws IOException {
		if(b == null)
			throw new NullPointerException();
    	pending = b;
    	pendingOffset = off;
    	pendingLength = len;
    	executeAll();
    }
    protected int executeAll() throws IOException {
		List<Future<Integer>> results = new ArrayList<Future<Integer>>(writeTasks.size());
		StackTraceElement[][] stacks = null;
		Integer allN = null;
    	try {
			boolean done = false;
			try {
				for(WriteTask t : writeTasks)
					results.add(executor.submit(t));
				for(Future<Integer> r : results) {
					try {
						Integer n = r.get();
						if(allN == null)
							allN = n;
						else if(!allN.equals(n))
							throw new IOException("Inconsistent results from write operation on multiple OutputStreams");
					} catch(ExecutionException e) {
						Throwable cause = e.getCause();
						if(cause instanceof IOException)
							throw (IOException) cause;
						else
							throw new IOException(cause);
					}
				}
				done = true;
			} finally {
				if(!done) {
					stacks = new StackTraceElement[writeTasks.size()][];
					int i = 0;
					for(WriteTask t : writeTasks) {
						Thread executor = t.getOwner();
						if(executor != null)
							stacks[i++] = executor.getStackTrace();
					}
					for(Future<Integer> r : results)
						r.cancel(true);
				}
	    	}
	    	return allN == null ? 0 : allN;
    	} catch(InterruptedException e) {
			StringBuilder sb = new StringBuilder();
			sb.append("Parallel ");
			if(pending == null)
				sb.append("flushes");
			else
				sb.append("writes of ").append(pending.length).append(" bytes");
			sb.append(" were interrupted:\n");
			int n = 0;
			for(Future<Integer> r : results) {
				if(r.isCancelled()) {
					sb.append("\tStream ").append(++n).append(" was cancelled\n");
					if(stacks != null && stacks[n - 1] != null) {
						for(StackTraceElement ste : stacks[n - 1])
							sb.append("\tat ").append(ste.toString()).append('\n');
					}
				} else if(!r.isDone()) {
					sb.append("\tStream ").append(++n).append(" could not be cancelled and is still running\n");
					if(stacks != null && stacks[n - 1] != null) {
						for(StackTraceElement ste : stacks[n - 1])
							sb.append("\tat ").append(ste.toString()).append('\n');
					}
				} else {
					try {
						allN = r.get();
						sb.append("\tStream ").append(++n).append(" succeeded with result ").append(allN).append("\n");
					} catch(ExecutionException e1) {
						sb.append("\tStream ").append(++n).append(" failed with: ").append(SystemUtils.getRootCause(e1).getMessage()).append("\n");
					} catch(InterruptedException e1) {
						sb.append("\tStream ").append(++n).append(" was interrupted while getting result\n");
					} catch(Throwable e1) {
						sb.append("\tStream ").append(++n).append(" throw an error: ").append(SystemUtils.getRootCause(e1).getMessage()).append(" while getting result\n");
					}
				}
			}
			log.warn(sb.toString());
			if(pending == null)
				pending = oneByte; // so that next call to flush will re-attempt
    		throw new IOException(e);
		}
    }
}
