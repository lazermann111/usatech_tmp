package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;

public class ReaderInputStream extends InputStream {
	protected final Reader delegate;
	protected final CharsetEncoder charsetEncoder;
	protected final CharBuffer charBuffer;
	protected final ByteBuffer byteBuffer;
	protected boolean done;

	public ReaderInputStream(Reader reader) {
		this(reader, Charset.defaultCharset(), 1024);
	}

	public ReaderInputStream(Reader reader, Charset encoding, int bufferSize) {
		super();
		if(reader == null)
			throw new NullPointerException("Reader may not be null");
		if(encoding == null)
			throw new NullPointerException("Encoding may not be null");
		this.delegate = reader;
		this.charsetEncoder = encoding.newEncoder();
		this.byteBuffer = ByteBuffer.allocate(bufferSize);
		this.charBuffer = CharBuffer.allocate(Math.max(16, (int) Math.ceil(bufferSize / charsetEncoder.averageBytesPerChar())));
		byteBuffer.limit(0);
		charBuffer.limit(0);
	}

	@Override
	public int read() throws IOException {
		if(byteBuffer.hasRemaining())
			return byteBuffer.get();
		if(done)
			return -1;
		fill();
		if(byteBuffer.hasRemaining())
			return byteBuffer.get();

		return -1;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if(byteBuffer.hasRemaining()) {
			int r = Math.min(len, byteBuffer.remaining());
			byteBuffer.get(b, off, r);
			return r;
		}
		if(done)
			return -1;
		fill();
		if(byteBuffer.hasRemaining()) {
			int r = Math.min(len, byteBuffer.remaining());
			byteBuffer.get(b, off, r);
			return r;
		}

		return -1;
	}

	protected void fill() throws IOException {
		byteBuffer.compact();
		while(true) {
			CoderResult result = charsetEncoder.encode(charBuffer, byteBuffer, false);
			if(result.isError())
				throw new IOException("Invalid input bytes");
			if(result.isOverflow())
				break;
			if(result.isUnderflow()) {
				charBuffer.compact();
				int r = delegate.read(charBuffer);
				if(r == -1) {
					done = true;
					break;
				}
				charBuffer.flip();
			}
		}
		byteBuffer.flip();
	}

	@Override
	public int available() throws IOException {
		if(byteBuffer.hasRemaining())
			return byteBuffer.remaining();
		if(delegate.ready())
			return 1;
		return 0;
	}

	public void close() throws IOException {
		delegate.close();
		done = true;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof ReaderInputStream && ((ReaderInputStream) obj).delegate.equals(delegate));
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return delegate.hashCode();
	}
}
