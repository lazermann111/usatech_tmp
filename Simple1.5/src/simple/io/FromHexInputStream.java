package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

public class FromHexInputStream extends InputStream {
	protected final Reader delegate;
	protected final char[] TWO_BUFFER = new char[2];
	
	public FromHexInputStream(Reader delegate) {
		super();
		this.delegate = delegate;
	}

	@Override
	public int read() throws IOException {
		int r = delegate.read(TWO_BUFFER);
		switch(r) {
			case -1: case 0:
				return r;
			case 1:
				while(true) {
					r = delegate.read(TWO_BUFFER, 1, 1);
					switch(r) {
						case 1:
							return ByteArrayUtils.hexValue(TWO_BUFFER[0], TWO_BUFFER[1]);
						case 0:
							break; // keep trying
						case -1:
							return -1;
						default:
							throw new IOException("Invalid result returned from java.io.Reader.read() for " + delegate);
					}
				}
			case 2:
				return ByteArrayUtils.hexValue(TWO_BUFFER[0], TWO_BUFFER[1]);
			default:
				throw new IOException("Invalid result returned from java.io.Reader.read() for " + delegate);	
		}
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if(len == 1) {
			int r = read();
			if(r >= 0)
				return r;
			else {
				b[off] = (byte) r;
				return 1;
			}
		}
		char[] buffer = new char[len*2];
		int r = delegate.read(buffer);
		if(r < 1) {
			return r;
		} else if((r % 2) == 1) {
			int t = (r + 1) / 2;
			int orig = r;
			while(true) {
				r = delegate.read(buffer, orig, 1);
				switch(r) {
					case 1:
						for(int i = 0; i < t; i++) {
							b[off + i] = ByteArrayUtils.hexValue(buffer[i * 2], buffer[i * 2 + 1]);
						}
						return t;
					case 0:
						break; // keep trying
					case -1:
						return -1;
					default:
						throw new IOException("Invalid result returned from java.io.Reader.read() for " + delegate);
				}
			}
		} else {
			int t = r / 2;
			for(int i = 0; i < t; i++) {
				b[off + i] = ByteArrayUtils.hexValue(buffer[i * 2], buffer[i * 2 + 1]);
			}
			return t;
		}
	}
}
