package simple.io;

import java.io.IOException;
import java.util.Properties;

import simple.bean.ReflectionUtils;
import simple.io.logging.Bridge;
import simple.io.logging.JDK14toSimpleHandler;
import simple.io.logging.NothingBridge;
import simple.io.logging.SimpleBridge;
import simple.net.protocol.AddProtocols;

/**
 * Provides logging for an application. Will perform asynchronous logging
 * unless simple.logging.async is set to false or off in the logging.properties
 * file. This will use Log4J if it is available and otherwise will
 * use the simple.io.logging.SimpleBridge to perform logging.
 * To use this with commons logging (from apache/jakarta/commons), set
 * either the system property or the commons logging property "org.apache.commons.logging.Log"
 * to "simple.io.logging.SimpleLogFactory".
 *
 * @author    Brian S. Krug
 */
public abstract class Log {
    protected static Bridge bridge = new NothingBridge();
    protected static enum State { NOT_INITIALIZED, INITIALIZING, INITIALIZED, REINITIALIZING };
    protected static State state = State.NOT_INITIALIZED;

    protected static Loader propertiesLoader = new Loader() {
        public void load(ConfigSource src) throws LoadingException {
            try {
                properties.clear();
                properties.load(src.getInputStream());
				if("true".equalsIgnoreCase(properties.getProperty("manageJDKLogging", "true").trim()))
					JDK14toSimpleHandler.install();
			} catch(IOException e) {
				throw new LoadingException("Could not read Log properties input stream", e);
			} catch(SecurityException e) {
				throw new LoadingException("Could not install JDK14toSimpleHandler", e);
            }
        }
    };
    protected static ConfigSource propertySource;
    protected static final Properties properties = new Properties(getSystemProperties());
    protected static final String addProtocolsClassName = AddProtocols.class.getName();

    /*
     * Returns the Log instance with the class name of the calling class as the identifier
     */
    public static Log getLog() {
        StackTraceElement ste = ReflectionUtils.getPrevCallerStackElement(Log.class);
        String id = (ste == null ? "" : ste.getClassName());
        return getLog(id);
    }

    private static Properties getSystemProperties() {
		try {
			return System.getProperties();
		} catch(SecurityException e) {
			handleLogError("Not allowed to get system properties", e);
			return null;
		}
	}

	/*
     * Returns the Log instance for the given identifier
     */
    public static Log getLog(String identifier) {
    	if(!addProtocolsClassName.equals(identifier))
    		AddProtocols.run();
        return getBridge().getLog(identifier);
    }

    protected static void initialize() {
    	try {
    		propertySource = ConfigSource.createConfigSource("logging.properties", true);
            propertySource.registerLoader(propertiesLoader, 15 * 1000 /* 15 seconds*/, 0);
            propertySource.reload();
        } catch(Throwable t) {
        	Throwable cause = t;
			while(cause.getCause() != null && cause.getCause() != cause)
				cause = cause.getCause();
            handleLogError("Could not load Log Config file 'logging.properties': " + cause.getMessage(), null);
        }
        bridge = createBridge();
    }
    protected static Bridge getBridge() {
    	if(state == null) state = State.NOT_INITIALIZED;
    	switch(state) {
    		case NOT_INITIALIZED:
    			state = State.INITIALIZING;
    			initialize();
    			state = State.INITIALIZED;
    			break;
    	}
    	return bridge;
    }

    private static Bridge createBridge() {
        //handleLogError("Creating bridge from: " + properties, null);
        String bridgeClassName = getProperty("simple.io.logging.bridge");
        if(bridgeClassName != null)
            try {
                return (Bridge)Class.forName(bridgeClassName).newInstance();
            } catch(Throwable t) {
                handleLogError("Could not create specified Bridge class '" + bridgeClassName + "'", t);
            }
        try {
            return createLog4JBridge();
        } catch(Throwable t) {
            handleLogError("Could not create Log4JBridge class", t);
        }
        try {
        	return new SimpleBridge();
        } catch(Throwable t) {
            handleLogError("Could not create SimpleBridge class", t);
    	}
        throw new RuntimeException("Could not create a bridge for the logging system");
    }

    public static void handleLogError(String msg, Throwable t) {
        if("true".equalsIgnoreCase(System.getProperty("simple.io.Log.debug"))) {
	    	System.out.println("~~~LOG: " + msg);
	        if(t != null) t.printStackTrace(System.out);
        }
    }

    private static Bridge createLog4JBridge() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        loadClass("org.apache.log4j.Logger"); // ensure log4j is available
        return (Bridge)loadClass("simple.io.logging.Log4JBridge").newInstance();
    }

    protected static Class<?> loadClass(String className) throws ClassNotFoundException {
        return Class.forName(className);
        //return Log.class.getClassLoader().loadClass(className);
    }

    public static String getProperty(String key) {
    	String value = properties.getProperty(key);
    	if(value == null)
    		try {
    			value = System.getProperty(key);
    		} catch(SecurityException e) {
    			handleLogError("Not allowed to read property '" + key + "'", e);
    		}
        return value;
    }
    /**
     * Returns true if properties have changed since last read
     * @return
     */
    public static boolean checkProperties() {
        try {
            return propertySource != null && propertySource.reload();
        } catch(Throwable t) {
            return false;
        }
    }
    public abstract void debug(Object message);
    public abstract void debug(Object message, Throwable throwable);
    public abstract void debug(String format, Object... args);
    public abstract void error(Object message);
    public abstract void error(Object message, Throwable throwable);
    public abstract void error(String format, Object... args);
    public abstract void fatal(Object message);
    public abstract void fatal(Object message, Throwable throwable);
    public abstract void fatal(String format, Object... args);
    public abstract void info(Object message);
    public abstract void info(Object message, Throwable throwable);
    public abstract void info(String format, Object... args);
    public abstract void trace(Object message);
    public abstract void trace(Object message, Throwable throwable);
    public abstract void trace(String format, Object... args);
    public abstract void warn(Object message);
    public abstract void warn(Object message, Throwable throwable);
    public abstract void warn(String format, Object... args);
    public abstract boolean isDebugEnabled();
    public abstract boolean isErrorEnabled();
    public abstract boolean isFatalEnabled();
    public abstract boolean isInfoEnabled();
    public abstract boolean isTraceEnabled();
    public abstract boolean isWarnEnabled();

    public static void finish() {
        bridge.finish();
    }
}
