/**
 *
 */
package simple.io;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import simple.bean.ConvertUtils;
import simple.event.ProgressEvent;
import simple.event.ProgressListener;
import simple.text.StringUtils;
import simple.util.concurrent.RunOnGetFuture;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractTextInteraction extends AbstractSectionInteraction<Void> {
	public File readFile(final String caption, final String baseDir, final String fmt, final Object... args) {
		return new File(readLine(fmt, args));
	}

	public int[] readChoice(String fmt, String[] options, boolean multiple, int[] defaultChoices, Object... args) {
		printf(fmt, args);
		for(int i = 1; i <= options.length; i++) {
			printf("\t" + i + ". " + options[i - 1]);
		}
		String choice = readLine("");
		if(choice == null || (choice = choice.trim()).length() == 0)
			return defaultChoices;
		if(multiple) {
			String[] ss = StringUtils.split(choice, ";, ".toCharArray(), false);
			int[] result = new int[ss.length];
			for(int i = 0; i < ss.length; i++)
				result[i] = ConvertUtils.getIntSafely(ss[i], -1);
			return result;
		}
		return new int[ConvertUtils.getIntSafely(choice, -1)];
	}

	
	public ProgressListener createProgressMeter() {
		return new ProgressListener() {			
			public void progressUpdate(ProgressEvent event) {
				getWriter().append('.');
			}			
			public void progressStart(ProgressEvent event) {
				getWriter().append(event.getStatusDescription()).append(": ");
			}			
			public void progressFinish(ProgressEvent event) {
				getWriter().append("Complete\n");
			}
		};
	}

	public Future<String> getLineFuture(final String fmt, final Object... args) {
		return new RunOnGetFuture<String>(new Callable<String>() {
			public String call() throws Exception {
				return readLine(fmt, args);
			}
		});
	}
	public Future<char[]> getPasswordFuture(final String fmt, final Object ... args) {
		return new RunOnGetFuture<char[]>(new Callable<char[]>() {
			public char[] call() throws Exception {
				return readPassword(fmt, args);
			}
		});
	}

	@Override
	public Future<File> browseForFile(final String caption, final String baseDir, final String fmt, final Object... args) {
		return getFileFuture(caption, baseDir, fmt, args);
	}

	public Future<File> getFileFuture(final String caption, final String baseDir, final String fmt, final Object... args) {
		return new RunOnGetFuture<File>(new Callable<File>() {
			public File call() throws Exception {
				return readFile(caption, baseDir, fmt, args);
			}
		});
	}

	public Future<int[]> getChoiceFuture(final String fmt, final String[] options, final boolean multiple, final int[] defaultChoices, final Object... args) {
		return new RunOnGetFuture<int[]>(new Callable<int[]>() {
			public int[] call() throws Exception {
				return readChoice(fmt, options, multiple, defaultChoices, args);
			}
		});
	}

	public void clear() {
		for(int i = 0; i < 20; i++)
			getWriter().println();
	}

	@Override
	protected void doSectionEnd(Section section, boolean success, String msg) {
		writeSectionBreak(section, success ? "COMPLETE" : "FAILURE", msg);
	}

	@Override
	protected Void doSectionStart(Section section) {
		writeSectionBreak(section, "START", null);
		return null;
	}

	public String readLineDefault(String fmt, String defaultValue, Object... args) {
		String value = readLine(fmt, args);
		return StringUtils.isBlank(value) ? defaultValue : value;
	}

	public Future<String> getLineFutureDefault(final String fmt, final String defaultValue, final Object... args) {
		return new RunOnGetFuture<String>(new Callable<String>() {
			public String call() throws Exception {
				return readLine(fmt, args);
			}
		});
	}
}
