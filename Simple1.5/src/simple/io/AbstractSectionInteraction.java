package simple.io;

import java.io.PrintWriter;
import java.util.Random;

import simple.text.StringUtils;

public abstract class AbstractSectionInteraction<S> implements Interaction {
	protected final Random random = new Random();
	protected final static String NEWLINE = System.getProperty("line.separator", "\n");

	protected class Section {
		public final int depth;
		public final int id = random.nextInt();
		public final String desc;
		public final S data;
		public Section child = null;
		public final Section parent;

		public Section(String desc) {
			this.desc = desc;
			this.parent = tailSection;
			if(parent != null) {
				this.depth = parent.depth + 1;
				parent.child = this;
				this.data = doSectionStart(this);
			} else {
				this.depth = 0;
				this.data = null;
			}
			tailSection = this;
		}

		public void remove(boolean success, String msg) {
			doSectionEnd(this, success, msg);
			tailSection = this.parent;
			this.parent.child = null;
		}
	}

	protected final Section headSection = new Section(null);
	protected Section tailSection = headSection;

	@Override
	public int startSection(String sectionDescription) {
		Section section = new Section(sectionDescription);
		return section.id;
	}

	public void endSection(int sectionId, boolean success, String msg) {
		// TODO: what should we do when we can't find that section?
		if(findSection(sectionId) == null)
			return;
		for(Section section = tailSection; section != null; section = section.parent) {
			section.remove(success, msg);
			if(section.id == sectionId)
				break;
		}
	}

	protected Section findSection(int sectionId) {
		for(Section section = tailSection; section != null; section = section.parent) {
			if(section.id == sectionId)
				return section;
		}
		return null;
	}

	protected abstract S doSectionStart(Section section);

	protected abstract void doSectionEnd(Section section, boolean success, String msg);

	protected void writeSectionBreak(Section section, String type, String msg) {
		PrintWriter writer = getWriter();
		int len = type.length();
		if(!StringUtils.isBlank(section.desc))
			len += 1 + section.desc.length();
		int n;
		if(len + 12 > 60) {
			n = 5;
			len = len + 12;
		} else {
			n = (60 - len) / 2;
			len = 60;
		}
		for(int i = 0; i < n; i++)
			writer.write('#');
		writer.write(' ');
		writer.write(type);
		writer.write(' ');
		if(!StringUtils.isBlank(section.desc)) {
			writer.write(section.desc);
			writer.write(' ');
		}
		for(int i = 0; i < n; i++)
			writer.write('#');
		writer.write(NEWLINE);
		if(!StringUtils.isBlank(msg)) {
			for(int i = 0; i < msg.length(); i += len - 4) {
				writer.write('#');
				writer.write(' ');
				writer.write(msg.substring(i, len - 4));
				for(int k = msg.length() - i; k < 56; k++)
					writer.write(' ');
				writer.write(' ');
				writer.write('#');
				writer.write(NEWLINE);
			}
			for(int i = 0; i < len; i++)
				writer.write('#');
			writer.write(NEWLINE);
		}
	}
}
