package simple.io;

import java.io.IOException;
import java.io.InputStream;

public class ByteInputBinaryStream implements BinaryStream {
	protected final ByteInput content;
	
	public ByteInputBinaryStream(ByteInput content) {
		this.content = content;
	}

	public InputStream getInputStream() {
		try {
			content.reset();
		} catch(IOException e) {
			//ignore
		}
		return new ByteInputInputStream(content);
	}

	public long getLength() {
		try {
			return content.length();
		} catch(IOException e) {
			return -1;
		}
	}

	public long copyInto(ByteOutput output) throws IOException {
		return content.copyInto(output);
	}
}
