package simple.io;

import java.io.IOException;

public interface Abortable {
	public void abort() throws IOException;
}
