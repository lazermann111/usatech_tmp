package simple.io;

import java.io.IOException;
import java.io.OutputStream;

public class StrictOutputStreamByteOutput extends OutputStreamByteOutput {

	public StrictOutputStreamByteOutput(OutputStream out) {
		super(out);
	}
	/**
	 * @see simple.io.ByteOutput#writeByte(int)
	 */
    @Override
	public void writeByte(int b) throws IOException {
    	if((b & 0xFFFFFF00) != 0)
    		throw new IOException("Invalid byte '" + b + "'");
		super.writeByte(b);
    }

    /**
	 * @see simple.io.ByteOutput#writeShort(int)
	 */
    @Override
	public void writeShort(int s) throws IOException {
    	if((s & 0xFFFF0000) != 0)
    		throw new IOException("Invalid short '" + s + "'");
		super.writeShort(s);
    }

    /**
	 * @see simple.io.ByteOutput#writeChar(int)
	 */
    @Override
	public void writeChar(int c) throws IOException {
    	if((c & 0xFFFF0000) != 0)
    		throw new IOException("Invalid char '" + c + "'");
    	super.writeChar(c);
    }
}
