package simple.io;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import simple.util.MapBackedSet;
/**
 * A queue of objects that are written to a PrintStream asynchronously.
 *
 * @author  Brian S. Krug
 * @version
 */
public class QueuedOutput extends java.io.OutputStream {
    protected PrintThread printThread;
    private static java.util.Set<QueuedOutput> outputs = new MapBackedSet<QueuedOutput>(new java.util.WeakHashMap<QueuedOutput, Object>());
    protected static final Object SIGNAL = new Object();
    protected static final Object FINISH = new Object();
    protected static class PrintThread extends Thread {
    	protected BlockingQueue<Object> queue = null;
    	protected java.io.PrintStream out = null;
    	protected final ReentrantLock lock = new ReentrantLock();
    	protected final Condition signal = lock.newCondition();
    	public void run() {
            try {
                while(true) {
                    yield();
                    Object m = queue.take();
                    if(m == SIGNAL) break;
                    if(m == FINISH) return;
                    try	{
                        out.println(m);
                    } catch(NullPointerException e) {
                    	//do nothing
                    } catch (Throwable e) {
                        System.out.println("ERROR while printing message");
                        e.printStackTrace();
                    }
                }
                lock.lock();
                try {
                	signal.signalAll();
                } finally {
                	lock.unlock();
                }
            } catch(InterruptedException ie) {
                // do nothing just end
            }
        }
    };
    /**
     * Creates a new QueuedOutput
     */
    public QueuedOutput() {
        super();
        init();
    }
    /**
     * Adds a message to the printThread queue
     */
    public void queueMessage(Object message) {
        if(message != null) {
        	printThread.queue.offer(message);
        }
    }
    
    /**
     * Makes sure all pending messages are written
     */
    public void finish() {
    	printThread.lock.lock();
    	try {
    		printThread.queue.offer(SIGNAL);
    		printThread.signal.await();
    	} catch(InterruptedException e) {
			//do nothing;
		} finally {
			printThread.lock.unlock();
	    }
        //System.out.println("Ending Output, " + this);
    }
    
    /**
     * Makes sure all pending messages are written
     */
    public static void finishAll() {
        for(QueuedOutput qo : outputs) qo.finish();
    }

    /**
     * Returns the thread that actually writes the messages
     * @return Thread
     */
    protected Thread createPrintThread() {
    	PrintThread t = new PrintThread();
        t.setName("Print Thread");
        t.setDaemon(true);
        t.setPriority(3);
        t.start();
        return t;
    }
    
    /**
     * Returns the PrintStream to which messages are written
     * @return java.io.PrintStream
     */
    public java.io.PrintStream getOut() {
        return printThread.out;
    }
    
    /**
     * Performs initialization of the Queue and printing thread
     */
    protected void init() {
        outputs.add(this);
        printThread = new PrintThread();
        printThread.setName("Print Thread");
        printThread.setDaemon(true);
        printThread.setPriority(3);
        printThread.queue = new LinkedBlockingQueue<Object>();
        printThread.out = System.out;
        printThread.start();
    }
    
    /**
     * Sets the PrintStream to which messages are written. System.out by
     * default.
     * @param newOut java.io.PrintStream
     */
    public void setOut(java.io.PrintStream newOut) {
    	printThread.out = newOut;
    }
        
    /**
     * Implementation of the OutputStream write method that puts the bytes into
     * the queue to be written
     */
    public void write(byte b[], int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if ((off < 0) || (off > b.length) || (len < 0) ||
        ((off + len) > b.length) || ((off + len) < 0)) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return;
        }
        queueMessage(new String(b,off,len));
    }
    
    /**
     * Implementation of the OutputStream write method that puts the byte into
     * the queue to be written
     */
    public void write(int b) throws IOException {
        queueMessage(new Character((char)b));
    }
    
    /**
     * Flushes the underlying PrintSteam
     */
    public void flush() throws IOException {
    	printThread.out.flush();
    }
    
    @Override
    protected void finalize() throws Throwable {
    	printThread.queue.offer(FINISH);
    	super.finalize();
    }
}
