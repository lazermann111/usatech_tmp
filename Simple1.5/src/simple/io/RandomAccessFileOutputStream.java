package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

/** Implementation of OutputStream around a RandomAccessFile.
 * This implementation is NOT thread-safe.
 *
 * @author Brian S. Krug
 *
 */public class RandomAccessFileOutputStream extends OutputStream {
	protected final RandomAccessFile raf;
	public RandomAccessFileOutputStream(RandomAccessFile raf) {
		this.raf = raf;
	}
	@Override
	public void close() throws IOException {
		raf.close();
	}
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof RandomAccessFileOutputStream
				&& raf.equals(((RandomAccessFileOutputStream)obj).raf));
	}
	@Override
	public int hashCode() {
		return raf.hashCode();
	}
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		raf.write(b, off, len);
	}
	@Override
	public void write(byte[] b) throws IOException {
		raf.write(b);
	}
	@Override
	public void write(int b) throws IOException {
		raf.write(b);
	}
}
