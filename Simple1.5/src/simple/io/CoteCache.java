package simple.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.NotEnoughRowsException;
import simple.lang.SystemUtils;
import simple.results.BeanException;
import simple.sql.ByteArrayBlob;
import simple.util.concurrent.BoundedCache;
import simple.util.concurrent.LockLinkedSegmentCache;

public class CoteCache {
	public static class ItemClosedException extends Exception {
		private static final long serialVersionUID = -3686464775005376943L;

		public ItemClosedException() {
			super();
		}

		public ItemClosedException(String message, Throwable cause) {
			super(message, cause);
		}

		public ItemClosedException(String message) {
			super(message);
		}

		public ItemClosedException(Throwable cause) {
			super(cause);
		}
		
	}
	public static interface Item {
		public long getLength();

		public long getLastModified();

		public void writeTo(OutputStream out) throws IOException, ItemClosedException;

		public String getContentType();

		public void close();
	}

	public static interface Retriever {
		public Item getItem(String fileName) throws IOException;
	}

	protected final BoundedCache<ItemKey, Item, IOException> itemCache = new LockLinkedSegmentCache<ItemKey, Item, IOException>(1024, 16, 0.75f, 4) {
		protected Item createValue(ItemKey key, Object... additionalInfo) throws IOException {
			return retrieveItem(key);
		}
	};
	
	protected File workingDirectory;
	protected int maxInMemoryLength = 0;
	protected static class ItemKey {
		protected final String subdomainUrl;
		protected final String subdirectory;
		protected final String fileName;
		protected final Object keyCd;

		public ItemKey(String subdomainUrl, String subdirectory, String fileName, Object keyCd) {
			super();
			this.subdomainUrl = subdomainUrl;
			this.subdirectory = subdirectory;
			this.fileName = fileName;
			this.keyCd = keyCd;
		}
		public boolean equals(Object obj) {
			if(!(obj instanceof ItemKey))
				return false;
			ItemKey ik = (ItemKey)obj;
			return ConvertUtils.areEqual(subdomainUrl, ik.subdomainUrl) && ConvertUtils.areEqual(subdirectory, ik.subdirectory) && ConvertUtils.areEqual(fileName, ik.fileName) && ConvertUtils.areEqual(keyCd, ik.keyCd);
		}
		public int hashCode() {
			return SystemUtils.addHashCodes(0, subdomainUrl, subdirectory, fileName, keyCd);
		}

		public Object getKeyCd() {
			return keyCd;
		}

		public String getSubdomainUrl() {
			return subdomainUrl;
		}

		public String getSubdirectory() {
			return subdirectory;
		}

		public String getFileName() {
			return fileName;
		}
	}

	public class BasicItem implements Item {
		protected final long length;
		protected final long lastModified;
		protected final String contentType;
		protected final BinaryStream content;
		protected final Runnable closer;

		public BasicItem(long length, String contentType, long lastModified, BinaryStream content, Runnable closer) {
			super();
			this.length = length;
			this.lastModified = lastModified;
			this.contentType = contentType;
			this.content = content;
			this.closer = closer;
		}

		@Override
		public void writeTo(OutputStream out) throws IOException, ItemClosedException {
			InputStream in;
			try {
				in = content.getInputStream();
			} catch(IOException e) {
				close();
				checkWorkingDirectory();
				throw new ItemClosedException(e);
			}
			try {
				IOUtils.copy(in, out);
			} finally {
				in.close();
			}
		}

		public long getLength() {
			return length;
		}

		public long getLastModified() {
			return lastModified;
		}

		public String getContentType() {
			return contentType;
		}

		public void close() {
			if(closer != null)
				closer.run();
		}
	}
	protected String retrieveCallId = "GET_COTE_ITEM";

	public CoteCache() {
		super();
		setWorkingDirectory(new File(System.getProperty("java.io.tmpdir"), "__web_working_" + System.currentTimeMillis()));
	}

	protected void checkWorkingDirectory() {
		File dir = getWorkingDirectory();
		if(!dir.exists())
			makeTmpDir(dir);
	}

	public Item getItem(String subdomainUrl, String subdirectory, String fileName, Object keyCd) throws IOException {
		return itemCache.getOrCreate(new ItemKey(subdomainUrl, subdirectory, fileName, keyCd));
	}

	public Retriever createRetriever(final String subdomainUrl, final String subdirectory, final Object keyCd) {
		return new Retriever() {
			public Item getItem(String fileName) throws IOException {
				return CoteCache.this.getItem(subdomainUrl, subdirectory, fileName, keyCd);
			}
		};
	}

	protected Item retrieveItem(final ItemKey key) throws IOException {
		Map<String, Object> params = new HashMap<String, Object>(8);
		params.put("subdomainUrl", key.getSubdomainUrl());
		params.put("subdirectory", key.getSubdirectory());
		params.put("fileName", key.getFileName());
		params.put("key", key.getKeyCd());
		boolean dbOkay = false;
		Connection conn;
		try {
			conn = DataLayerMgr.getConnectionForCall(getRetrieveCallId());
		} catch(SQLException e) {
			throw new IOException(e);
		} catch(DataLayerException e) {
			throw new IOException(e);
		}
		try {
			try {
				DataLayerMgr.selectInto(conn, getRetrieveCallId(), params);
			} catch(NotEnoughRowsException e) {
				return null;
			} catch(SQLException e) {
				throw new IOException(e);
			} catch(DataLayerException e) {
				throw new IOException(e);
			} catch(BeanException e) {
				throw new IOException(e);
			}
			try {
				long length = ConvertUtils.getLong(params.get("length"), -1L);
				Object content = params.get("content");
				String contentType = ConvertUtils.getString(params.get("contentType"), true);
				long lastModified = ConvertUtils.getLong(params.get("lastModified"));
				Blob blob;
				if(content instanceof Blob)
					blob = (Blob) content;
				else if(content instanceof byte[])
					blob = new ByteArrayBlob((byte[]) content);
				else
					blob = ConvertUtils.convert(Blob.class, content);
				if(length < 0)
					length = blob.length();
				BinaryStream stream;
				Runnable closer;
				if(length > getMaxInMemoryLength()) {
					final File file = createWorkingFile(key.getSubdirectory(), key.getFileName(), key.getKeyCd());
					boolean okay = false;
					try {
						InputStream in = blob.getBinaryStream();
						try {
							FileOutputStream out = new FileOutputStream(file);
							try {
								IOUtils.copy(in, out);
							} finally {
								out.close();
							}
						} finally {
							in.close();
						}
						stream = new FileBinaryStream(file);
						closer = new Runnable() {
							public void run() {
								itemCache.remove(key);
								file.delete();
							}
						};
						okay = true;
					} finally {
						if(!okay)
							file.delete();
					}
				} else {
					stream = new ByteArrayBinaryStream(blob.getBytes(1L, (int) length));
					closer = new Runnable() {
						public void run() {
							itemCache.remove(key);
						}
					};
				}
				Item item = new BasicItem(length, contentType, lastModified, stream, closer);
				dbOkay = true;
				conn.commit();
				return item;
			} catch(ConvertException e) {
				throw new IOException(e);
			} catch(IOException e) {
				throw new IOException(e);
			} catch(SQLException e) {
				throw new IOException(e);
			}
		} finally {
			if(!dbOkay)
				DbUtils.rollbackSafely(conn);
			DbUtils.closeSafely(conn);
		}
	}

	protected File createWorkingFile(String subdirectory, String fileName, Object keyCd) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("cote_");
		if(subdirectory != null)
			sb.append(subdirectory.replaceAll("\\W", "_")).append('_');
		if(fileName != null)
			sb.append(fileName.replaceAll("\\W", "_")).append('_');
		if(keyCd != null)
			sb.append(keyCd).append('_');
		File file = File.createTempFile(sb.toString(), null, getWorkingDirectory());
		file.deleteOnExit();
		return file;
	}

	public void clearCache() {
		for(Iterator<Item> iter = itemCache.values().iterator(); iter.hasNext();) {
			iter.next().close();
			iter.remove();
		}
		itemCache.clear();
	}

	public String getRetrieveCallId() {
		return retrieveCallId;
	}

	public void setRetrieveCallId(String retrieveCallId) {
		this.retrieveCallId = retrieveCallId;
	}

	public File getWorkingDirectory() {
		return workingDirectory;
	}

	public void setWorkingDirectory(File workingDirectory) {
		if(workingDirectory != null && !ConvertUtils.areEqual(workingDirectory, this.workingDirectory))
			makeTmpDir(workingDirectory);
		this.workingDirectory = workingDirectory;
	}

	protected static void makeTmpDir(File dir) {
		if(dir.getParentFile() != null)
			makeTmpDir(dir.getParentFile());
		if(dir.mkdir())
			dir.deleteOnExit();
	}

	public int getMaxInMemoryLength() {
		return maxInMemoryLength;
	}

	public void setMaxInMemoryLength(int maxInMemoryLength) {
		this.maxInMemoryLength = maxInMemoryLength;
	}

	public int getMaxCacheSize() {
		return itemCache.getMaxSize();
	}

	public void setMaxCacheSize(int maxSize) {
		itemCache.setMaxSize(maxSize);
	}
}
