package simple.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;

/** This implementation is NOT thread-safe.
 * @author Brian S. Krug
 *
 */
public class ByteChannelReader extends DecodingReader {
	protected final ReadableByteChannel channel;
	protected ByteBuffer buffer;
	protected int minRemaining;
	public ByteChannelReader(ReadableByteChannel channel, CharsetDecoder decoder, int bufferSize) {
        super(decoder);
        this.channel = channel;
        this.buffer = ByteBuffer.allocate(bufferSize);
    }

	@Override
	public void close() throws IOException {
		buffer = null;
	}

	@Override
	public int read(CharBuffer charBuffer) throws IOException {
		buffer.compact();
		int read;
		if(buffer.position() < minRemaining) {
			read = channel.read(buffer);
		} else {// we can avoid reading from the channel
			read = 0;
		}
		buffer.flip();

		int start = charBuffer.position();
		if(read == -1) {
			decoder.decode(buffer, charBuffer, true);
			decoder.flush(charBuffer);
			decoder.reset();
			if(start == charBuffer.position())
				return -1;
		} else {
			CoderResult result = decoder.decode(buffer, charBuffer, false);
			if(result.isOverflow()) {
				//could read more
			}
		}
		return charBuffer.position() - start;
	}

	@Override
	public void setDecoder(CharsetDecoder decoder) {
		super.setDecoder(decoder);
		minRemaining = 4 /*Add an extra or two*/ + (int)Math.ceil(1.0 / decoder.averageCharsPerByte());
	}

	@Override
	public boolean ready() throws IOException {
		return buffer.remaining() > minRemaining;
	}
}
