/*
 * ExtensionFileFilter.java
 *
 * Created on December 8, 2003, 11:54 AM
 */

package simple.io;

/**
 *
 * @author  Brian S. Krug
 */
public class ExtensionFileFilter extends javax.swing.filechooser.FileFilter implements java.io.FileFilter, java.io.FilenameFilter {
    protected java.util.Set<String> extensionSet = new java.util.HashSet<String>();
    protected String description;

    public ExtensionFileFilter(String typeName, String... extensions) {
    	this(extensions, typeName);
    }
    /** Creates a new instance of ExtensionFileFilter */
    public ExtensionFileFilter(String[] extensions, String typeName) {
        description = typeName + " (";
        for(int i = 0; i < extensions.length; i++) {
            if(i > 0) description += ", ";
            extensionSet.add(extensions[i]);
            description += "*." + extensions[i];
        }
        description += ")";
    }

    public boolean accept(java.io.File f) {
        if(f.isDirectory()) return true; // allow navigation throughout the folder tree
        return accept(f.getParentFile(), f.getName());
    }

    public boolean accept(java.io.File dir, String name) {
        int p = name.lastIndexOf('.');
        String ext = (p >= 0 ? name.substring(p+1) : "");
        return extensionSet.contains(ext);
    }

    public String getDescription() {
        return description;
    }
}
