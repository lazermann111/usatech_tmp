/*
 * Created on Sep 27, 2005
 *
 */
package simple.io;

import static simple.io.Base64Constants.BAD_ENCODING;
import static simple.io.Base64Constants.DECODABET;
import static simple.io.Base64Constants.EQUALS_SIGN_ENC;
import static simple.io.Base64Constants.white_SPACE_ENC;

import java.io.IOException;
import java.io.InputStream;

public abstract class AbstractBase64DecodingInputStream extends InputStream {
    protected byte[] b4 = new byte[4];
    protected int position = 0;

    public int read() throws IOException {
        if(position == -1) return -1;
        while(true) {
			int b = get();
            if (b < 0) { position = -1; return -1; }
            b4[position] = DECODABET[b & 0x7f];
            switch(b4[position]) {
                case white_SPACE_ENC: break; //continue LOOP;
                case EQUALS_SIGN_ENC: position = -1; return -1; //end of stream reached
                case BAD_ENCODING: throw new IOException("Invalid byte '" + b + "'");
                default:
                    switch(position) {
                        case 0: position = 1; break; //continue LOOP
                        case 1: position = 2; return ((b4[0] & 0x3F) << 2) | ((b4[1] & 0x30) >> 4); // pos = 1;
                        case 2: position = 3; return ((b4[1] & 0x0F) << 4) | ((b4[2] & 0x3C) >> 2); // pos = 2;
                        case 3: position = 0; return ((b4[2] & 0x03) << 6) | ((b4[3] & 0x3F) >> 0); // pos = 3;
                        default: return -1;
                    }
            }           
        }
    }

	protected abstract int get() throws IOException;

	protected abstract int remaining() throws IOException;

    @Override
    public int available() throws IOException {
		return (int) (remaining() * 3.0 / 4.0);
    }       
}