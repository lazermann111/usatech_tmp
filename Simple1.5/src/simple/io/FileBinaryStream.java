/**
 *
 */
package simple.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Brian S. Krug
 *
 */
public class FileBinaryStream implements BinaryStream {
	protected final File file;

	public FileBinaryStream(File file) {
		super();
		this.file = file;
	}

	/**
	 * @see simple.io.BinaryStream#getLength()
	 */
	public long getLength() {
		return file.length();
	}

	/**
	 * @throws FileNotFoundException
	 * @see simple.io.BinaryStream#getReader()
	 */
	public InputStream getInputStream() throws FileNotFoundException {
		return new FileInputStream(file);
	}

	public long copyInto(ByteOutput output) throws IOException {
		return IOUtils.copy(getInputStream(), output, 0L, getLength());
	}
}
