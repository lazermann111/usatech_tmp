package simple.io.server;


public interface NIOServerMXBean {
	public int getNumActive() ;

	public int getNumIdle() ;

	public int getNumConnected() ;
	
	public int getMaxConnections() ;
	
	public void setMaxConnections(int maxConnections);

	public String getConnectionInfo() ;
}
