package simple.io.server;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.util.concurrent.atomic.AtomicInteger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import simple.io.Log;
import simple.text.StringUtils;

public class TrackingJMXByteChannel implements ByteChannel, ByteChannelMXBean {
	private static final Log log = Log.getLog();
	protected final ByteChannel delegate;
	protected ObjectName jmxObjectName;
	protected final String sourceKey;
	protected final AtomicInteger bytesRead = new AtomicInteger();
	protected final AtomicInteger bytesWritten = new AtomicInteger();
	
	public TrackingJMXByteChannel(ByteChannel delegate, String jmxNamePrefix, String sourceKey) {
		this.delegate = delegate;
		this.sourceKey = sourceKey;
		StringBuilder jmxName = new StringBuilder();
		jmxName.append(jmxNamePrefix).append(",Channel=").append(StringUtils.prepareJMXNameValue(sourceKey));
		try {
			ObjectName jmxObjectName = new ObjectName(jmxName.toString());
			ManagementFactory.getPlatformMBeanServer().registerMBean(this, jmxObjectName);
			this.jmxObjectName = jmxObjectName;
		} catch(InstanceAlreadyExistsException e) {
			log.warn("Could not register ByteChannel '" + sourceKey + "' in JMX", e);
		} catch(MBeanRegistrationException e) {
			log.warn("Could not register ByteChannel '" + sourceKey + "' in JMX", e);
		} catch(NotCompliantMBeanException e) {
			log.warn("Could not register ByteChannel '" + sourceKey + "' in JMX", e);
		} catch(MalformedObjectNameException e) {
			log.warn("Could not register ByteChannel '" + sourceKey + "' in JMX", e);
		} catch(NullPointerException e) {
			log.warn("Could not register ByteChannel '" + sourceKey + "' in JMX", e);
		}
	}

	public void close() throws IOException {
		try {
			delegate.close();
		} finally {
			if(jmxObjectName != null) {
				try {
					ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmxObjectName);
				} catch(InstanceNotFoundException e) {
					log.warn("Could not unregister ByteChannel '" + sourceKey + "' in JMX", e);
				} catch(MBeanRegistrationException e) {
					log.warn("Could not unregister ByteChannel '" + sourceKey + "' in JMX", e);
				} catch(NullPointerException e) {
					log.warn("Could not unregister ByteChannel '" + sourceKey + "' in JMX", e);
				}
				jmxObjectName = null;
			}
		}

	}

	public boolean isOpen() {
		return delegate.isOpen();
	}

	public int read(ByteBuffer dst) throws IOException {
		int n = delegate.read(dst);
		if(n > 0)
			bytesRead.addAndGet(n);		
		return n;
	}

	public int write(ByteBuffer src) throws IOException {
		int n = delegate.write(src);
		if(n > 0)
			bytesWritten.addAndGet(n);		
		return n;
	}
	
	public int getBytesRead() {
		return bytesRead.get();
	}
	
	public int getBytesWritten() {
		return bytesWritten.get();
	}

	public String toString() {
		return getClass().getName() + " on " + delegate;
	}
}
