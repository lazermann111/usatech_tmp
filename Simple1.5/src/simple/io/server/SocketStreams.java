package simple.io.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.ReentrantLock;

import javax.net.ServerSocketFactory;

public class SocketStreams {
	protected final ReentrantLock lock = new ReentrantLock();
	protected final ServerSocket serverSocket;
	protected Socket socket;
	protected final InputStream inputStream = new InputStream() {
		@Override
		public int read() throws IOException {
			lockInterruptibly();
			try {
				InputStream in = getSocketInputStream();
				while(true) {
					try {
						int r = in.read();
						if(r > 0) return r;
						in = newSocketInputStream();
					} catch(IOException e) {
						in = newSocketInputStream();
					}
				}
			} finally {
				lock.unlock();
			}
		}
		@Override
		public int available() throws IOException {
			lockInterruptibly();
			try {
				InputStream in = getSocketInputStream();
				while(true)
					try {
						return in.available();
					} catch(IOException e) {
						in = newSocketInputStream();
					}
			} finally {
				lock.unlock();
			}
		}
		@Override
		public void close() throws IOException {
			lock.lock();
			try {
				if(socket != null) socket.close();
			} finally {
				lock.unlock();
			}
		}
		@Override
		public void mark(int readlimit) {
			if(!lockInterruptiblySafely())
				return;
			try {
				try {
					getSocketInputStream().mark(readlimit);
				} catch(IOException e) {
				}
			} finally {
				lock.unlock();
			}
		}
		@Override
		public boolean markSupported() {
			if(!lockInterruptiblySafely())
				return false;
			try {
				try {
					return getSocketInputStream().markSupported();
				} catch(IOException e) {
					return false;
				}
			} finally {
				lock.unlock();
			}
		}
		@Override
		public int read(byte[] b) throws IOException {
			lockInterruptibly();
			try {
				InputStream in = getSocketInputStream();
				while(true) {
					try {
						int r = in.read(b);
						if(r > 0) return r;
						in = newSocketInputStream();
					} catch(IOException e) {
						in = newSocketInputStream();
					}
				}
			} finally {
				lock.unlock();
			}
		}
		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			lockInterruptibly();
			try {
				InputStream in = getSocketInputStream();
				while(true) {
					try {
						int r = in.read(b, off, len);
						if(r > 0) return r;
						in = newSocketInputStream();
					} catch(IOException e) {
						in = newSocketInputStream();
					}
				}
			} finally {
				lock.unlock();
			}
		}
		@Override
		public void reset() throws IOException {
			lockInterruptibly();
			try {
				getSocketInputStream().reset();
			} finally {
				lock.unlock();
			}
		}
		@Override
		public long skip(long n) throws IOException {
			lockInterruptibly();
			try {
				InputStream in = getSocketInputStream();
				while(true)
					try {
						return in.skip(n);
					} catch(IOException e) {
						in = newSocketInputStream();
					}
			} finally {
				lock.unlock();
			}
		}
	};

	protected final OutputStream outputStream = new OutputStream() {
		@Override
		public void write(int b) throws IOException {
			lockInterruptibly();
			try {
				getSocketOutputStream().write(b);
			} finally {
				lock.unlock();
			}
		}
		@Override
		public void write(byte[] b) throws IOException {
			lockInterruptibly();
			try {
				getSocketOutputStream().write(b);
			} finally {
				lock.unlock();
			}
		}
		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			lockInterruptibly();
			try {
				getSocketOutputStream().write(b, off, len);
			} finally {
				lock.unlock();
			}
		}
		@Override
		public void flush() throws IOException {
			lockInterruptibly();
			try {
				getSocketOutputStream().flush();
			} finally {
				lock.unlock();
			}
		}
		@Override
		public void close() throws IOException {
			lock.lock();
			try {
				if(socket != null) socket.close();
			} finally {
				lock.unlock();
			}
		}
	};

	public SocketStreams(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}
	public SocketStreams(int port) throws IOException {
		this(ServerSocketFactory.getDefault().createServerSocket(port));
	}
	public int getPort() {
		return serverSocket.getLocalPort();
	}
	protected void lockInterruptibly() throws IOException {
		try {
			lock.lockInterruptibly();
		} catch(InterruptedException e) {
			throw new IOException(e);
		}
	}
	protected boolean lockInterruptiblySafely() {
		try {
			lock.lockInterruptibly();
		} catch(InterruptedException e) {
			return false;
		}
		return true;
	}
	protected InputStream getSocketInputStream() throws IOException {
		if(socket == null|| socket.isClosed()) {
			socket = serverSocket.accept();
		}
		return socket.getInputStream();
	}

	protected InputStream newSocketInputStream() throws IOException {
		if(socket != null) socket.close();
		socket = serverSocket.accept();
		return socket.getInputStream();
	}

	protected OutputStream getSocketOutputStream() throws IOException {
		if(socket == null || socket.isClosed()) {
			socket = serverSocket.accept();
		}
		return socket.getOutputStream();
	}

	protected OutputStream newSocketOutputStream() throws IOException {
		if(socket != null) socket.close();
		socket = serverSocket.accept();
		return socket.getOutputStream();
	}

	public InputStream getInputStream() {
		return inputStream;
	}
	public OutputStream getOutputStream() {
		return outputStream;
	}
	public void close() throws IOException {
		lock.lock();
		try {
			if(socket != null) socket.close();
			serverSocket.close();
		} finally {
			lock.unlock();
		}
	}
}
