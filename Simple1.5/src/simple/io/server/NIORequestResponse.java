/**
 *
 */
package simple.io.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.Socket;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Map;

public interface NIORequestResponse extends InputInterpretter {
	public Reader getReader() throws IOException;
	public Writer getWriter() throws IOException;
	public PrintWriter getPrintWriter() throws IOException;
	public InputStream getInputStream() throws IOException;
	public OutputStream getOutputStream() throws IOException;
	public CharsetDecoder getReaderCharsetDecoder() ;
	public CharsetEncoder getWriterCharsetEncoder() ;
	public void setReaderCharsetDecoder(CharsetDecoder decoder) throws IOException;
	public void setWriterCharsetEncoder(CharsetEncoder encoder) throws IOException;
	public Socket getSocket() ;
	public Map<String,Object> getSessionAttributes() ;
}