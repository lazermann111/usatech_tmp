package simple.io.server;

import java.io.IOException;
import java.nio.channels.ReadableByteChannel;

public interface InputInterpretter {
		public SocketReadResult read(ReadableByteChannel channel) throws IOException ;

		public boolean prepareCurrent() throws IOException ;

		public boolean prepareNext() throws IOException ;

		public void flushOutput() ;

		public void close(boolean clientInitiated) ;
}
