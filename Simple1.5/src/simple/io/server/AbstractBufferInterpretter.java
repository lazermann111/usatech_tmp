package simple.io.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

import simple.io.Log;

/** Implements some of the details of InputInterpretter using a ByteBuffer
 * (and a readonly copy) to hold the data read from the socket.
 * @author Brian S. Krug
 *
 */
public abstract class AbstractBufferInterpretter implements InputInterpretter {
	private static final Log log = Log.getLog();
	protected final ByteBuffer buffer;
	protected final ByteBuffer bufferReadOnly;
	protected int next = 0;
	protected int last = 0;

	public AbstractBufferInterpretter(int inputBufferSize) {
		super();
		this.buffer = ByteBuffer.allocate(inputBufferSize);
		this.bufferReadOnly = buffer.duplicate();
		bufferReadOnly.limit(0);
	}

	protected abstract int findEOR(int newBytes) throws IOException ;

	public SocketReadResult read(ReadableByteChannel channel) throws IOException {
		if(last < bufferReadOnly.limit()) {
			if(log.isDebugEnabled())
				log.debug("Looking for end-of-record from " + bufferReadOnly.position() + " - " + bufferReadOnly.limit() + " in " + (bufferReadOnly.limit() - last) + " bytes");
			next = findEOR(bufferReadOnly.limit() - last);
			if(next >= 0) {
				if(log.isDebugEnabled())
					log.debug("Found end-of-record at " + next + "; buffer is now " + bufferReadOnly.position() + " - " + bufferReadOnly.limit());
				last = next;
				return SocketReadResult.RETURN;
			} else {
				last = bufferReadOnly.limit();
			}
		}
		int read = channel.read(buffer);
		if(read == -1) { //End of Stream
			return SocketReadResult.CLOSED;
		} else if(read > 0) {
			// last = buffer.position();
			// determine if end of request has been reached
			bufferReadOnly.limit(buffer.position());
			if(log.isDebugEnabled())
				log.debug("Read " + read + " bytes from channel; looking for end-of-record from " + bufferReadOnly.position() + " - " + bufferReadOnly.limit());

			next = findEOR(read);

			//if eor then :
			//1. Create Request
			//2. Save remaining bytes / chars for next request on this channel
			//3. Remove key from selection,
			if(next >= 0) {
				if(log.isDebugEnabled())
					log.debug("Found end-of-record at " + next + "; buffer is now " + bufferReadOnly.position() + " - " + bufferReadOnly.limit());

				last = next;
				return SocketReadResult.RETURN;
			} else if(!buffer.hasRemaining()) { //request input is too long!
				if(log.isInfoEnabled())
					log.info("Request Input is longer than buffer");
				return handleBufferOverflow();
			} else {
				last = bufferReadOnly.limit();
				if(log.isDebugEnabled())
					log.debug("Waiting for more data; last byte read at position " + last);
			}
		}
		return SocketReadResult.CONTINUE;
	}

	@SuppressWarnings("unused")
	protected SocketReadResult handleBufferOverflow() throws IOException {
		return SocketReadResult.CANCEL;
	}

	public boolean prepareNext() {
		//Save remaining bytes for next request on this channel
		buffer.limit(buffer.position()); // this must be first or buffer will throw IllegalArgumentException b/c new position > old limit
		buffer.position(next);
		buffer.compact();
		bufferReadOnly.limit(buffer.position());
		bufferReadOnly.position(0);
		last = 0;
		return bufferReadOnly.hasRemaining();
	}
	public void close(boolean clientInitiated) {
		next = 0;
		buffer.position(0);
	}
}