package simple.io.server;

import simple.app.SelfProcessor;


public interface SelfProcessingInterpretter extends InputInterpretter, SelfProcessor {
	
}
