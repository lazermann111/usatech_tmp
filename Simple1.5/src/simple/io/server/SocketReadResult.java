/**
 *
 */
package simple.io.server;

public enum SocketReadResult {CONTINUE, CANCEL, RETURN, CLOSED }