package simple.io.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.WritableByteChannel;

import simple.io.Log;

public class CopyingJMXByteChannel extends TrackingJMXByteChannel {
	private static final Log log = Log.getLog();
	protected final WritableByteChannel inCopyChannel;
	protected final WritableByteChannel outCopyChannel;
	
	public CopyingJMXByteChannel(ByteChannel delegate, String jmxNamePrefix, String sourceKey) {
		super(delegate, jmxNamePrefix, sourceKey);
		inCopyChannel = createCopyChannel("IN-" + sourceKey.replaceAll("[^\\w.-]", "_"));
		outCopyChannel = createCopyChannel("OUT-" + sourceKey.replaceAll("[^\\w.-]", "_"));
	}

	protected WritableByteChannel createCopyChannel(String tag) {
		try {
			File tmpDir = new File("./tmp");
			tmpDir.mkdir();
			File tmpFile = new File(tmpDir, "SocketChannelCopy-" + tag + ".dmp");
			// File tmpFile = File.createTempFile("SocketChannelCopy-" + tag + "-", ".dmp");
			// tmpFile.deleteOnExit();
			log.info("Creating copy file " + tmpFile.getAbsolutePath());
			return new FileOutputStream(tmpFile).getChannel();
		} catch(IOException e) {
			log.warn("Could not setup copy file", e);
			return null;
		}
	}

	public void close() throws IOException {
		super.close();
		inCopyChannel.close();
		outCopyChannel.close();
	}

	public int read(ByteBuffer dst) throws IOException {
		int n = super.read(dst);
		if(n > 0) {
			int limit = dst.limit();
			int pos = dst.position();
			dst.position(pos - n);
			dst.limit(pos);
			while(dst.hasRemaining())
				inCopyChannel.write(dst);
			dst.limit(limit);
			dst.position(pos);
		}
		return n;
	}

	public int write(ByteBuffer src) throws IOException {
		int n = super.write(src);
		if(n > 0) {
			int limit = src.limit();
			int pos = src.position();
			src.position(pos - n);
			src.limit(pos);
			while(src.hasRemaining())
				outCopyChannel.write(src);
			src.limit(limit);
			src.position(pos);
		}
		return n;
	}
}
