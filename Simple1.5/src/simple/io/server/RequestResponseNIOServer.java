package simple.io.server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.HashMap;
import java.util.Map;

import simple.io.ByteBufferInputStream;
import simple.io.ByteBufferReader;
import simple.io.ByteChannelOutputStream;
import simple.io.ByteChannelReader;
import simple.io.ByteChannelWriter;
import simple.io.ConcatenatingDecodingReader;
import simple.io.ConcatenatingInputStream;
import simple.io.DecodingReader;
import simple.io.Log;
import simple.io.RandomAccessFileInputStream;
import simple.lang.Initializer;

public class RequestResponseNIOServer extends NIOServer<NIORequestResponse> {
	private static final Log log = Log.getLog();
	protected int inputBufferSize = 1024;
	protected long maxRequestSize = 1024;
	protected int outputBufferSize = 1024;
	protected Charset charset;
	protected String[] eorMarkers;
	protected byte[][] eorByteMarkers;

	protected class DefaultNIORequestResponse extends AbstractBufferInterpretter implements NIORequestResponse {
		protected InputStream input;
		protected OutputStream output;
		protected CharsetDecoder decoder = getCharset().newDecoder();
		protected CharsetEncoder encoder = getCharset().newEncoder();
		protected DecodingReader reader;
		protected PrintWriter printWriter;
		protected ByteChannelWriter writer;
		protected Map<String,Object> attributes;
		protected final Socket socket;
		protected final ByteChannel byteChannel;
		protected final Initializer<RuntimeException> attributeInitializer = new Initializer<RuntimeException>() {
			@Override
			protected void doInitialize() {
				attributes = new HashMap<String, Object>();
			}
			@Override
			protected void doReset() {
				attributes = null;
			}
		};
		public DefaultNIORequestResponse(Socket socket, ByteChannel byteChannel) {
			super(getInputBufferSize());
			this.socket = socket;
			this.byteChannel = byteChannel;
		}
		protected void complete() {
			returnObject(this);
		}
		public Socket getSocket() {
			return socket;
		}
		public Map<String, Object> getSessionAttributes() {
			try {
				attributeInitializer.initialize();
			} catch(InterruptedException e) {
				return null; //this should not happen
			}
			return attributes;
		}
		public Reader getReader() {
			if(reader == null)
				reader = createBufferReader();
			return reader;
		}
		protected DecodingReader createBufferReader() {
			return new ByteBufferReader(bufferReadOnly, getReaderCharsetDecoder());
		}
		public Writer getWriter() {
			if(writer == null)
				writer = new ByteChannelWriter(byteChannel, getWriterCharsetEncoder(), getOutputBufferSize()) {
					@Override
					public void close() throws IOException {
						complete();
					}
				};
			return writer;
		}
		public InputStream getInputStream() {
			if(input == null) {
				input = createBufferInputStream();
			}
			return input;
		}
		protected InputStream createBufferInputStream() {
			return new ByteBufferInputStream(bufferReadOnly);
		}
		public OutputStream getOutputStream() {
			if(output == null)
				output = new ByteChannelOutputStream(byteChannel) {
					@Override
					public void close() throws IOException {
						complete();
					}
				};
			return output;
		}
		public CharsetDecoder getReaderCharsetDecoder() {
			return decoder;
		}
		public void setReaderCharsetDecoder(CharsetDecoder readerCharsetDecoder) {
			if(reader != null)
				reader.setDecoder(readerCharsetDecoder);
			decoder = readerCharsetDecoder;
		}
		public CharsetEncoder getWriterCharsetEncoder() {
			return encoder;
		}
		public void setWriterCharsetEncoder(CharsetEncoder writerCharsetEncoder) throws IOException {
			if(writer != null)
				writer.setEncoder(writerCharsetEncoder);
			encoder = writerCharsetEncoder;
		}

		@Override
		protected int findEOR(int newBytes) {
			return findEORPattern(bufferReadOnly, newBytes);
		}

		public boolean prepareCurrent() throws IOException {
			if(log.isDebugEnabled())
				log.debug("Received request from " + socket.getInetAddress() + " of " + buffer.remaining() + " bytes");
			if(!getReaderCharsetDecoder().charset().equals(getCharset())) {
				setReaderCharsetDecoder(getCharset().newDecoder());
			}
			if(!getWriterCharsetEncoder().charset().equals(getCharset())) {
				setWriterCharsetEncoder(getCharset().newEncoder());
			}
			return true;
		}

		@Override
		protected SocketReadResult handleBufferOverflow() throws IOException {
			if(log.isInfoEnabled())
				log.info("Request input from " + socket.getInetAddress() + " is longer than " + getMaxRequestSize() + " and cannot be processed. Closing the connection.");
			return super.handleBufferOverflow();
		}

		public void flushOutput() {
			if(writer != null)
				try {
					writer.flush();
				} catch(IOException e) {
					log.info("Could not flush writer", e);
				}
		}

		public PrintWriter getPrintWriter() throws IOException {
			if(printWriter == null)
				printWriter = new PrintWriter(getWriter()) {
				@Override
				public void close() {
					complete();
				}
			};
			return printWriter;
		}
	}

	public class FileAndBufferNIORequestResponse extends DefaultNIORequestResponse {
		protected final File inputOverflowFile;
		protected final RandomAccessFile inputOverflowRAF;
		protected final FileChannel inputOverflowChannel;

		public FileAndBufferNIORequestResponse(Socket socket, ByteChannel byteChannel) throws IOException {
			super(socket, byteChannel);
			long overflowSize = getMaxRequestSize() - getInputBufferSize();
			if(overflowSize > 0) {
				inputOverflowFile = File.createTempFile("tmp_server_request_", ".dat");
				inputOverflowFile.deleteOnExit();
				inputOverflowRAF = new RandomAccessFile(inputOverflowFile, "rw");
				inputOverflowChannel = inputOverflowRAF.getChannel();
			} else {
				inputOverflowFile = null;
				inputOverflowRAF = null;
				inputOverflowChannel = null;
			}
		}
		@Override
		public void complete() {
			super.complete();
			closeFile();
		}
		protected void closeFile() {
			if(inputOverflowFile != null) {
				inputOverflowFile.delete();
				try {
					inputOverflowRAF.close();
				} catch(IOException e) {
					log.info("Could not close RandomAccessFile", e);
				}
				try {
					inputOverflowChannel.close();
				} catch(IOException e) {
					log.info("Could not close RandomAccessFile", e);
				}
			}
		}
		@Override
		public InputStream getInputStream() {
			if(input == null) {
				if(inputOverflowRAF != null) {
					input = new ConcatenatingInputStream(
							new InputStream[] {
									new RandomAccessFileInputStream(inputOverflowRAF),
									createBufferInputStream()
							});
				} else {
					input = createBufferInputStream();
				}
			}

			return input;
		}

		@Override
		public Reader getReader() {
			if(reader == null) {
				if(inputOverflowChannel != null)
					reader = new ConcatenatingDecodingReader(
							new DecodingReader[] {
									new ByteChannelReader(inputOverflowChannel, getReaderCharsetDecoder(), getInputBufferSize()),
									createBufferReader()
							},  getReaderCharsetDecoder());
				else
					reader = createBufferReader();
			}
			return reader;
		}
		@Override
		public boolean prepareNext() {
			boolean hasNext = super.prepareNext();
			if(inputOverflowRAF != null)
				try {
					inputOverflowRAF.setLength(0); // this will set filePointer to 0 also
				} catch(IOException e) {
					log.warn("Could not set length of input buffer file to zero", e);

					// close the socketChannel but don't decrement connections b/c the complete() method
					// that called this method should do that
					try {
						byteChannel.close(); //this will cancel the channel in the selector
					} catch(IOException e1) {
						log.info("Could not close socketChannel", e1);
					}
					closeFile();
				}
			//???: Do we need to call the following as well?
			//inputOverflowChannel.truncate(0);
			return hasNext;
		}
		@Override
		protected SocketReadResult handleBufferOverflow() throws IOException {
			if(bufferReadOnly.remaining() + inputOverflowRAF.length() > getMaxRequestSize()) {
				return super.handleBufferOverflow();
			}
			//copy buffer to file
			while(bufferReadOnly.hasRemaining())
				inputOverflowChannel.write(bufferReadOnly);
			//clear buffer
			buffer.clear();
			bufferReadOnly.limit(0);
			last = 0;
			// try again
			return read(byteChannel);

		}
		@Override
		public boolean prepareCurrent() throws IOException {
			super.prepareCurrent();
			if(input instanceof ConcatenatingInputStream) {
				((ConcatenatingInputStream)input).setCurrentIndex(0);
			}
			if(reader instanceof ConcatenatingDecodingReader) {
				((ConcatenatingDecodingReader)reader).setCurrentIndex(0);
			}
			//position file pointer
			if(inputOverflowRAF != null)
				inputOverflowRAF.seek(0);
			if(inputOverflowChannel != null)
				inputOverflowChannel.position(0);
			return true;
		}
	}

	public RequestResponseNIOServer(int port) {
		this(new InetSocketAddress(port));
	}

	public RequestResponseNIOServer(InetSocketAddress socketAddress) {
		super(socketAddress);
		setEorMarkers(new String[] {"\n","\r"});
		setCharset(Charset.defaultCharset());
	}
	public RequestResponseNIOServer() {
		setEorMarkers(new String[] {"\n","\r"});
		setCharset(Charset.defaultCharset());
	}

	@Override
	protected NIORequestResponse createInterpretter(Socket socket, ByteChannel byteChannel) throws IOException {
		if(getInputBufferSize() < getMaxRequestSize())
			return new FileAndBufferNIORequestResponse(socket, byteChannel);
		else
			return new DefaultNIORequestResponse(socket, byteChannel);
	}

	/** Finds the end of the request, sets the buffer's position to the first character in the buffer
	 *  of the request data and limit to one more than last character of the request data
	 *  and returns the position in the buffer that marks the start of the next request.
	 * @param buffer
	 * @param read The number of new bytes to consider
	 * @return The position in the buffer of that marks the start of the next request. Or -1 if the
	 * end of the request was not found
	 */
	protected int findEORPattern(ByteBuffer buffer, int read) {
		int last = buffer.limit();
		for(int i = buffer.limit() - read; i < last; i++) {
			for(byte[] seq : getEorByteMarkers())
				if(byteRegionMatches(buffer, i, seq)) {
					if(i == buffer.position()) {
						buffer.position(i + seq.length);
						i += seq.length - 1; // so that i is incremented by seq.length after hitting loop header
						break;
					} else {
						buffer.limit(i);
						return i + seq.length;
					}
				}
		}
		return -1;
	}

	protected byte[][] getEorByteMarkers() {
		if(eorByteMarkers == null) {
			String[] markers = getEorMarkers();
			Charset charset = getCharset();
			byte[][] tmp = new byte[markers.length][];
			for(int i = 0; i < markers.length; i++)
				tmp[i] = charset.encode(markers[i]).array();
			eorByteMarkers = tmp;
		}
		return eorByteMarkers;
	}

	protected static boolean byteRegionMatches(ByteBuffer buffer, int start, byte[] match) {
		if(buffer.limit() < start + match.length)
			return false;
		for(int i = 0; i < match.length; i++) {
			if(buffer.get(i+start) != match[i])
				return false;
		}
		return true;
	}

	public int getInputBufferSize() {
		return inputBufferSize;
	}

	public void setInputBufferSize(int inputBufferSize) {
		this.inputBufferSize = inputBufferSize;
	}

	public long getMaxRequestSize() {
		return maxRequestSize;
	}

	public void setMaxRequestSize(long maxRequestSize) {
		this.maxRequestSize = maxRequestSize;
	}

	public int getOutputBufferSize() {
		return outputBufferSize;
	}

	public void setOutputBufferSize(int outputBufferSize) {
		this.outputBufferSize = outputBufferSize;
	}

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
		eorByteMarkers = null;
	}

	public String[] getEorMarkers() {
		return eorMarkers;
	}

	public void setEorMarkers(String[] eorMarkers) {
		this.eorMarkers = eorMarkers;
		eorByteMarkers = null;
	}
}
