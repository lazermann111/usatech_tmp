package simple.io.server;

public interface ByteChannelMXBean {
	public int getBytesRead() ;

	public int getBytesWritten() ;
}
