/*
 * PassThroughOutputStream.java
 *
 * Created on December 8, 2003, 9:12 AM
 */

package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author  Brian S. Krug
 */
public class PassThroughOutputStream extends OutputStream {
    protected Set<OutputStream> delegates = new LinkedHashSet<OutputStream>();
    
    /** Creates a new instance of PassThroughOutputStream */
    public PassThroughOutputStream() {
    }
    
    public void addWriter(Writer writer) {
        addOutputStream(new WriterOutputStream(writer));
    }
    public void removeWriter(Writer writer) {
        removeOutputStream(new WriterOutputStream(writer));
    }
    public void addOutputStream(OutputStream os) {
        delegates.add(os);
    }
    
    public void removeOutputStream(OutputStream os) {
        delegates.remove(os);
    }
    
    public void write(int b) throws IOException {
        for(OutputStream os : delegates) os.write(b);
    }
    
    public void close() throws IOException {
        for(OutputStream os : delegates) os.close();
    }
    
    public void flush() throws IOException {
        for(OutputStream os : delegates) os.flush();
    }
    
    public void write(byte[] b) throws IOException {
        for(OutputStream os : delegates) os.write(b);
    }
    
    public void write(byte[] b, int off, int len) throws IOException {
        for(OutputStream os : delegates) os.write(b, off, len);
    }
}
