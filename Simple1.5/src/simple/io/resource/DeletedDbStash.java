package simple.io.resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import simple.io.Compression;

public class DeletedDbStash implements DbStash {
	protected final String fileId;
	
	public DeletedDbStash(String fileId) {
		this.fileId = fileId;
	}
	public boolean canRead() {
		return false;
	}
	public boolean canWrite() {
		return false;
	}
	public void close() {
	}
	public boolean delete() {
		return false;
	}
	public String getAbsolutePath() {
		return null;
	}
	public String getContentType() {
		return null;
	}
	public String getFilename() {
		return null;
	}
	public InputStream getInputStream() throws IOException {
		return null;
	}
	public long getLength() {
		return 0;
	}
	public OutputStream getOutputStream() throws IOException {
		return null;
	}
	public OutputStream getUnbufferedOutputStream() throws IOException {
		return null;
	}
	public boolean isDirectory() {
		return false;
	}
	public boolean isFile() {
		return false;
	}
	public boolean isOpen() {
		return false;
	}
	public void setContentType(String contentType) {
	}
	public void setLength(long length) {
	}
	public String getKey() {
		return fileId;
	}
	public Compression getCompression() {
		return null;
	}
}
