/**
 * 
 */
package simple.io.resource;

import simple.io.Compression;

public interface DbStash extends Stash {
	public String getKey() ;
	public Compression getCompression() ;
}