package simple.io.resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Stash {
	public boolean canRead();
	public boolean canWrite();
	public void close() ;
	public String getAbsolutePath() ;
	public String getFilename();
	public boolean isDirectory();
	public boolean isFile();
	public boolean isOpen();
	public long getLength() ;
	public void setLength(long length) ;
	public OutputStream getOutputStream() throws IOException ;
	public InputStream getInputStream() throws IOException ;
	public OutputStream getUnbufferedOutputStream() throws IOException ;
	public boolean delete() ;
	public String getContentType() ;
	public void setContentType(String contentType) ;
}
