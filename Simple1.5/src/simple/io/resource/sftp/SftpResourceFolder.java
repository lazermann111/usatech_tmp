package simple.io.resource.sftp;

import java.text.ParseException;
import java.util.ListIterator;

import simple.io.resource.Repository;
import simple.util.SingletonListIterator;

/**
 * This class represents a sftp resource, it can be used to read and write to a
 * sftp file.
 *
 * @author yhe
 *
 */
public class SftpResourceFolder extends AbstractSftpResourceFolder {
	protected final SftpRepository repository = new SftpRepository();
	protected final Controller controller = new Controller(repository);

	public SftpResourceFolder() {
		super();
	}

	public SftpResourceFolder(String uri) throws ParseException{
		super();
		repository.setUri(uri);
	}
	public SftpResourceFolder(String host, String username, String password, String path) {
		super();
		setHost(host);
		setUsername(username);
		setPassword(password);
		setPath(path);
		super.setReplicas(1);
	}

	public String getHost() {
		return repository.getHost();
	}

	public void setHost(String host) {
		repository.setHost(host);
	}

	public String getUsername() {
		return repository.getUsername();
	}

	public void setUsername(String username) {
		repository.setUsername(username);
	}

	public String getPassword() {
		return repository.getPassword();
	}

	public void setPassword(String password) {
		repository.setPassword(password);
	}

	public String getPath() {
		return repository.getPath();
	}

	public void setPath(String path) {
		repository.setPath(path);
	}

	@Override
	protected ListIterator<Controller> getControlIterator(Repository[] firstRepos) {
		return new SingletonListIterator<Controller>(controller);
	}

	/**
	 * @see simple.io.resource.sftp.AbstractSftpResourceFolder#setReplicas(int)
	 */
	@Override
	public void setReplicas(int replicas) {
		// do nothing
	}

	public void release() {
		controller.close();
	}
}
