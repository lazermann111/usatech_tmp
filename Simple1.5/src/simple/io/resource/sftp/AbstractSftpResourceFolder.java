package simple.io.resource.sftp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.UndeclaredThrowableException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.activation.FileTypeMap;

import simple.io.Compression;
import simple.io.Log;
import simple.io.resource.AbstractResourceFolder;
import simple.io.resource.Repository;
import simple.io.resource.Resource;
import simple.io.resource.ResourceMode;
import simple.io.resource.ResourceUtils;
import simple.io.resource.ResourceUtils.ResourceKey;
import simple.io.resource.Stash;
import simple.io.resource.sftp.SftpUtils.PrivateKeyType;
import simple.text.StringUtils;
import simple.util.concurrent.AbstractLockingPool;
import simple.util.concurrent.CreationException;

import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.configuration.SshConnectionProperties;
import com.sshtools.j2ssh.io.UnsignedInteger32;
import com.sshtools.j2ssh.sftp.FileAttributes;
import com.sshtools.j2ssh.sftp.SftpFile;
import com.sshtools.j2ssh.sftp.SftpFileInputStream;
import com.sshtools.j2ssh.sftp.SftpFileOutputStream;
import com.sshtools.j2ssh.sftp.SftpSubsystemClient;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.InvalidHostFileException;

/**
 * This class represents a sftp resource, it can be used to read and write to a
 * sftp file to multiple locations.
 *
 * @author yhe, bkrug
 *
 */
public abstract class AbstractSftpResourceFolder extends AbstractResourceFolder {
	private static final Log log = Log.getLog();
	protected final ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 5, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	
	protected class ControllerItem {
		protected final SftpRepository repository;
		protected final SshClient sshClient;
		protected SftpSubsystemClient subClient;
		public ControllerItem(SftpRepository repository, int connectTimeout, int socketTimeout) {
			this.sshClient = new SshClient();
			this.sshClient.setConnectTimeout(connectTimeout);
			this.sshClient.setSocketTimeout(socketTimeout);
			this.repository = repository;
		}
		public SftpSubsystemClient getSubClient() throws IOException {
			if(subClient == null)
				subClient = sshClient.openSftpChannel();
			return subClient;
		}
		public void close() throws IOException {
			if(subClient != null && subClient.isOpen())
				subClient.close();
			subClient = null;
			if(sshClient.isConnected())
				sshClient.disconnect();
		}
		public void ensureOpen() throws IOException {
			if(!sshClient.isConnected()) {
				connectSshClient(sshClient, repository);
				subClient = null;// re-create on getSubClient()
			} else if(subClient != null && subClient.isClosed())
				subClient = null; // re-create on getSubClient()
		}
		public boolean isClosed() {
			return !sshClient.isConnected() || (subClient != null && subClient.isClosed());
		}
	}
	// We will need n SshClients per host b/c each SshClient uses one socket and operations on a socket are single-threaded
	protected class Controller extends AbstractLockingPool<ControllerItem> {
		protected final SftpRepository repository;

		public Controller(SftpRepository repository) {
			super();
			this.repository = repository;
			setMaxActive(AbstractSftpResourceFolder.this.getConcurrency());
			setMaxIdle(AbstractSftpResourceFolder.this.getReplicas());
			setMaxWait(AbstractSftpResourceFolder.this.getMaxWait());
			setTracing(AbstractSftpResourceFolder.this.isTracing());
		}

		@Override
		protected ControllerItem createObject(long timeout) throws CreationException {
			ControllerItem item = new ControllerItem(repository, getConnectTimeout(), getSocketTimeout());
			if(log.isInfoEnabled())
				log.info("Created new " + item + " for " + repository);
			return item;
		}
		@Override
		protected void closeObject(ControllerItem obj) throws Exception {
			obj.close();
		}
		@Override
		public ControllerItem borrowObject() throws TimeoutException, CreationException,
				InterruptedException {
			ControllerItem item = super.borrowObject();
			try {
				item.ensureOpen();
			} catch(IOException e) {
				invalidateObject(item);
				throw new CreationException(e);
			}
			return item;
		}
		public ControllerItem borrowItem() throws TimeoutException, InterruptedException, IOException {
			try {
				return borrowObject();
			} catch(CreationException e) {
				if(e.getCause() instanceof IOException)
					throw (IOException)e.getCause();
				if(e.getCause() instanceof InterruptedException)
					throw (InterruptedException)e.getCause();
				throw new UndeclaredThrowableException(e);
			}
		}
		public String getFullPath(String path) {
			if(repository.getPath() == null || repository.getPath().length() == 0)
				return path;
			else if(path == null)
				return repository.getPath() + '/';
			else if(path.charAt(0) == '/' || repository.getPath().charAt(repository.getPath().length()-1) == '/')
				return repository.getPath() + path;
			else
				return repository.getPath() + '/' + path;

		}

		public SftpRepository getRepository() {
			return repository;
		}
	}

	protected static final SftpRepository[] EMPTY_REPOS = new SftpRepository[0];
	protected static final Controller[] EMPTY_CONTROLS = new Controller[0];
	protected static final FileAttributes defaultFileAttributes = new FileAttributes();
	static {
		defaultFileAttributes.setPermissions("rw-------");
	}
	protected static HostKeyVerification hostKeyVerification;

	protected void connectSshClient(SshClient sshClient, SftpRepository repository) throws IOException {
		HostKeyVerification hostKeyVerification = getHostKeyVerification();
		SshConnectionProperties sshProperties = new SshConnectionProperties();
		sshProperties.setHost(repository.getHost());
		sshProperties.setKeepAlive(isKeepAlive());
		sshClient.connect(sshProperties, hostKeyVerification);
		SftpUtils.authenticate(sshClient, repository.getUsername(), repository.getPassword(), repository.getPrivateKeyBytes(), repository.getPassphrase(), PrivateKeyType.FORMATTED);
	}

	protected static HostKeyVerification getHostKeyVerification() throws InvalidHostFileException {
		if(hostKeyVerification == null) {
			hostKeyVerification = SftpUtils.createHostKeyVerification(System.getProperty("knownhosts.file"));
		}
		return hostKeyVerification;
	}

	protected FileAttributes initialFileAttributes = createFileAttributes();
	protected int maxChildrenPerFolder = 0;
	protected int connectTimeout = 5000;
	protected int socketTimeout = 5000;
	protected boolean keepAlive = true;
	protected long maxWait;
	protected boolean tracing = false;
	protected int concurrency = 3;
	protected int replicas = 1;
	protected String[] subFolderNames = new String[] {"_"};
	protected String compressedSuffix = ".gz";

	public AbstractSftpResourceFolder() {
		final ThreadGroup group = Thread.currentThread().getThreadGroup();
        final AtomicInteger threadNumber = new AtomicInteger(1);
        final String namePrefix = "SftpControllerThread-";
        executor.setThreadFactory(new ThreadFactory() {       
	        public Thread newThread(Runnable r) {
	            Thread t = new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), 0);
	            t.setDaemon(true);
	            t.setPriority(Thread.NORM_PRIORITY);
	            return t;
	        }
	    });
	}
	protected abstract ListIterator<Controller> getControlIterator(Repository[] firstRepos) ;

	protected static FileAttributes createFileAttributes() {
		FileAttributes fileAttributes = new FileAttributes();
		fileAttributes.setPermissions("rw-------");
		return fileAttributes;
	}

	public int getReplicas() {
		return replicas;
	}

	public void setReplicas(int replicas) {
		this.replicas = replicas;
		for(ListIterator<Controller> iter = getControlIterator(null); iter.hasNext(); )
			iter.next().setMaxIdle(replicas);
	}

	public int getConcurrency() {
		return concurrency;
	}

	public void setConcurrency(int concurrency) {
		this.concurrency = concurrency;
		for(ListIterator<Controller> iter = getControlIterator(null); iter.hasNext(); )
			iter.next().setMaxActive(concurrency);
	}

	public Resource getResource(String key, ResourceMode mode) throws IOException {
		if((key=key.trim()).length() == 0)
			throw new IOException("Blank key is not valid");
		String compressSuffix = getCompressedSuffix();
		if(mode == ResourceMode.CREATE) {
			key = ResourceUtils.convertToKey(key);
			if(isCompressing() && compressSuffix != null)
				key += compressSuffix;
			else
				compressSuffix = null;
		}
		ResourceKey resKey;
		try {
			resKey = ResourceUtils.parseKey(key, compressSuffix);
		} catch(ParseException e) {
			IOException ioe = new IOException();
			ioe.initCause(e);
			throw ioe;
		}
		ListIterator<Controller> controlIterator;
		if(resKey.getRepositories() != null && resKey.getRepositories().length > 0) {
			controlIterator = getControlIterator(resKey.getRepositories());
		} else {
			controlIterator = getControlIterator(null);
		}
		if(!controlIterator.hasNext())
			throw new IOException("No Repositories are configured for this ResourceFolder");

		// get the files
		int required;
		int flags;
		switch(mode) {
			case READ:
				required = 1;
				flags = SftpSubsystemClient.OPEN_READ;
				break;
			case CREATE:
				required = getReplicas();
				flags = SftpSubsystemClient.OPEN_CREATE | SftpSubsystemClient.OPEN_EXCLUSIVE | SftpSubsystemClient.OPEN_WRITE | SftpSubsystemClient.OPEN_READ;
				break;
			case UPDATE:
				required = getReplicas();
				flags = SftpSubsystemClient.OPEN_READ | SftpSubsystemClient.OPEN_WRITE;
				break;
			default:
				throw new IOException("Invalid Mode " + mode);
		}

		Stash[] stashes = new Stash[required];
		boolean timedOut = false;
		IOException connectException = null;
		SftpRepository failedRepository = null;
		int count = 0;
		if(log.isInfoEnabled())
			log.info("Constructing resource '" + resKey.getPath() + "' from " + (resKey.getRepositories() != null && resKey.getRepositories().length > 0 ? Arrays.toString(resKey.getRepositories()) : "any") + " for " + mode);
		StringBuilder keyBuilder = new StringBuilder();
		//TODO: the following for loop contains a complicated logic flow (as evidenced by the various continue, break, finally statements)
		try {
			while(controlIterator.hasNext()) {
				Controller con = controlIterator.next();
				if(con != null && con.getMaxActive() > 0) {
					ControllerItem item;
					try {
						item = con.borrowItem();
					} catch(IOException e) {
						log.warn("Could not connect to '" + con.getRepository() + "': " + e.getMessage());
						connectException = e;
						failedRepository = con.getRepository();
						continue;
					} catch(TimeoutException e) {
						log.info("Repository '" + con.getRepository() + "' with " + con.getNumActive() + " of " + con.getMaxActive() + " active items and " + con.getNumIdle() + " idle items is too busy" + (isTracing() ? ": " + con.getTracingInfo() : ""));
						timedOut = true;
						failedRepository = con.getRepository();
						continue;
					} catch(InterruptedException e) {
						log.info("Interrupted; throwing exception", e);
						throw new IOException("Interrupted while connecting to repository");
					}
					if(log.isInfoEnabled())
						log.info("Using repository '" + con.getRepository() + "'");
					SftpFile file = null;
					try {
						if(mode == ResourceMode.CREATE) {
							String fullPath;
							try {
								if(count == 0) { // Only allow first repository to change key
									fullPath = checkdirs(con.getFullPath(null), resKey, item.getSubClient(), getMaxChildrenPerFolder(), getSubFolderNames());
								} else {
									fullPath = con.getFullPath(resKey.getPath());
									mkdirs(fullPath, item.getSubClient());
								}
							} catch(IOException e) {
								if(item.isClosed()) {//e.getMessage().startsWith("The channel is closed")) {
									if(log.isInfoEnabled())
										log.info("Could not create directories for '" + resKey.getPath() + "' on '" + con.getRepository() + "'; trying again", e);
									controlIterator.previous(); // return the client and try again
								} else {
									log.warn("Could not create directories for '" + resKey.getPath() + "' on '" + con.getRepository() + "'", e);
								}
								continue;
							} catch(ParseException e) {
								IOException ioe = new IOException();
								ioe.initCause(e);
								throw ioe;
							}
							do {
								try {
									file = item.getSubClient().openFile(fullPath, flags, defaultFileAttributes);
									if(keyBuilder.length() > 0)
										keyBuilder.append(',');
									else
										keyBuilder.append('[');
									keyBuilder.append(con.getRepository().getHost());
									keyBuilder.append(':');
									keyBuilder.append(con.getRepository().getPath());
									break;
								} catch(IOException e) {
									// for the case when fullPath already exists
									if(e.getMessage().equals("Failure")) {
										resKey.nextPath();
										if(controlIterator.hasPrevious()) {
											// go back to start
											do { controlIterator.previous(); } while(controlIterator.hasPrevious());
											count = 0;
											keyBuilder.setLength(0);
											break;
										}
									} else if(item.isClosed()) {//e.getMessage().startsWith("The channel is closed")) {
										controlIterator.previous(); // return the client and try again
										break;
									} else {
										log.warn("Could not create file '" + resKey.getPath() + "' on '" + con.getRepository() + "'", e);
										break;
									}
								}
							} while(true);
						} else {
							try {
								file = item.getSubClient().openFile(con.getFullPath(resKey.getPath()), flags);
							} catch(IOException e) {
								if(item.isClosed()) {//e.getMessage().startsWith("The channel is closed")) {
									controlIterator.previous(); // return the client and try again
									break;
								} else {
									log.warn("Could not open file '" + resKey.getPath() + "' on '" + con.getRepository() + "': " + e.getMessage());
								}
							}
						}
					} finally {
						if(file == null) // hold onto sub until File is closed
							con.returnObject(item);
					}
					if(file != null) {
						stashes[count] = new ControllerStash(file, con, item, (mode == ResourceMode.CREATE ? initialFileAttributes : null));
						if(++count >= required)
							break;
					}
				}
			}
			if(count < required) {
				if(connectException != null) {
					IOException ioe = new IOException("Could not connect to repository '" + failedRepository + "'");
					ioe.initCause(connectException);
					throw ioe;
				} else if(timedOut)
					throw new IOException("Repositories were too busy");
				else if(count == 0)
					throw new IOException("Could not find the file '" + resKey.getPath() + "' on any repository");
				else
					throw new IOException("Could only connect to " + count + " repositories, but " + required + " are required to " + mode + " the file '" + resKey.getPath() + "'");
			}

			//update key as needed
			if(mode == ResourceMode.CREATE) {
				key = keyBuilder.append(']').append(resKey.getPath()).toString();
			}
			if(log.isInfoEnabled())
				log.info("Constructed resource '" + key + "' for " + mode);
			return createResource(stashes, executor, key, resKey.getFileName(), resKey.hasOptionalSuffix() ? Compression.ZLIB : null);
		} catch(IOException e) {
			//close all ResourceFiles
			for(int i = 0; i < count; i++) {
				stashes[i].close();
			}
			throw e;
		} catch(RuntimeException e) {
			//close all ResourceFiles
			for(int i = 0; i < count; i++) {
				stashes[i].close();
			}
			throw e;
		}
	}

	public boolean isAvailable(ResourceMode mode) {
		ListIterator<Controller> controlIterator = getControlIterator(null);
		int required;
		switch(mode) {
			case READ:
				required = 1;
				break;
			case CREATE:
				required = getReplicas();
				break;
			case UPDATE:
				required = getReplicas();
				break;
			default:
				log.warn("Invalid Mode " + mode);
				return false;
		}

		int count = 0;
		while(controlIterator.hasNext()) {
			Controller con = controlIterator.next();
			if(con != null && con.getMaxActive() > 0) {
				ControllerItem item;
				try {
					item = con.borrowItem();
				} catch(IOException e) {
					log.info("Could not connect to '" + con.getRepository() + "': " + e.getMessage());
					continue;
				} catch(TimeoutException e) {
					log.info("Repository '" + con.getRepository() + "' is too busy");
					continue;
				} catch(InterruptedException e) {
					log.info("Interrupted; retrying", e);
					controlIterator.previous();
					continue;
				}
				con.returnObject(item);
				if(++count >= required)
					return true;
			}
		}
		return false;
	}
	protected class ControllerStash implements Stash {
		protected final SftpFile file;
		protected final Controller controller;
		protected final ControllerItem item;
		protected final FileAttributes fileAttributesOnClose;
		protected long size = -1;
		protected String contentType;
		public ControllerStash(SftpFile file, Controller controller, ControllerItem item, FileAttributes fileAttributesOnClose) {
			super();
			this.file = file;
			this.controller = controller;
			this.item = item;
			this.fileAttributesOnClose = fileAttributesOnClose;
			this.contentType = FileTypeMap.getDefaultFileTypeMap().getContentType(file.getFilename());
		}
		public boolean canRead() {
			return file.canRead();
		}
		public boolean canWrite() {
			return file.canWrite();
		}
		public void close() {
			if(fileAttributesOnClose != null) {
				try {
					item.getSubClient().setAttributes(file, fileAttributesOnClose);
				} catch(IOException e) {
					log.info("Could not set file attributes on '" + file.getAbsolutePath() + "' to "
							+ (fileAttributesOnClose.getPermissions() == null ? "" : fileAttributesOnClose.getPermissionsString() + "\t")
							+ (fileAttributesOnClose.getUID() == null && fileAttributesOnClose.getGID() == null ? "" :
								(fileAttributesOnClose.getUID() == null ? "_" : fileAttributesOnClose.getUID())
								+ "\t" + (fileAttributesOnClose.getGID() == null ? "_" : fileAttributesOnClose.getGID())), e);
				}
			}
			if(log.isInfoEnabled())
				log.info("Closing file '" + file.getAbsolutePath() + "' on " + controller.getRepository().getHost() + " and returning item to pool");
			try {
				file.close();
			} catch(IOException e) {
				log.info("Could not close file '" + file.getAbsolutePath() + "' on " + controller.getRepository().getHost(), e);
			}
			controller.returnObject(item);
		}
		public String getAbsolutePath() {
			return file.getAbsolutePath();
		}
		public String getFilename() {
			return file.getFilename();
		}
		public InputStream getInputStream() throws IOException {
			return new BufferedInputStream(new SftpFileInputStream(file), getInputBufferSize());
		}
		public OutputStream getOutputStream() throws IOException {
			return new BufferedOutputStream(getUnbufferedOutputStream(), getOutputBufferSize());
		}
		public OutputStream getUnbufferedOutputStream() throws IOException {
			if(!canWrite())
				throw new IOException("File is not writable");
			return new SftpFileOutputStream(file) {
				@Override
				public void close() throws IOException {
					super.close();
					//set size
					size = super.position.longValue();
				}
			};
		}
		public boolean isDirectory() {
			return file.isDirectory();
		}
		public boolean isFile() {
			return file.isFile();
		}
		public boolean isOpen() {
			return file.isOpen();
		}
		public long getLength() {
			return size == -1 ? file.getAttributes().getSize().longValue() : size;
		}
		/** TODO: We don't store the original file length, so compressed files will not have the correct length
		 * @see simple.io.resource.Stash#setLength(long)
		 */
		public void setLength(long length) {
			this.size = length;
		}
		public boolean delete() {
			try {
				file.delete();
				return true;
			} catch(IOException e) {
				log.debug("Could not delete file " + file.getAbsolutePath(), e);
				return false;
			}
		}
		public String getContentType() {
			return contentType;
		}
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
	}
	protected void mkdirs(String fullPath, SftpSubsystemClient client) throws IOException {
		if(fullPath.charAt(0) != '/') {
			fullPath = client.getDefaultDirectory() + '/' + fullPath;
		}
		for(int start = 0, end = 0; start < fullPath.length(); start = end + 1) {
            end = fullPath.indexOf('/',start);
            if(end < 0)
            	break;
            else if(end > start) { // no need to check "...//" on second slash
	            String dir = fullPath.substring(0, end);
	            try {
	            	client.getAttributes(dir);
	            } catch (IOException ex) {
	            	client.makeDirectory(dir);
	            	client.changePermissions(dir, 0022 ^ 0777); //default_permissions ^ umask
	            }
            }
        }
	}
	protected String checkdirs(String basePath, ResourceKey resKey, SftpSubsystemClient client, int maxChildren, String[] subDirectories) throws IOException, ParseException {
		if(basePath.charAt(0) != '/') {
			basePath = client.getDefaultDirectory() + '/' + basePath;
		}
		String dir = null;
		for(int start = 0, end = 0; start < basePath.length(); start = end + 1) {
            end = basePath.indexOf('/',start);
            if(end < 0)
            	end = basePath.length();
            if(end > start) { // no need to check "...//" on second slash
	            dir = basePath.substring(0, end);
	            try {
	            	client.getAttributes(dir);
	            } catch (IOException ex) {
	            	client.makeDirectory(dir);
	            	client.changePermissions(dir, 0022 ^ 0777); //default_permissions ^ umask
	            }
            }
        }
		basePath = dir;
		String path = resKey.getPath();
		if(path.charAt(0) != '/') {
			path = '/' + path;
		}
		for(int start = 0, end = 0; start < path.length(); start = end + 1) {
            end = path.indexOf('/',start);
            if(end < 0) {
            	if(maxChildren > 1 && subDirectories != null && subDirectories.length > 0) {
            		if(maxChildren < 2 + subDirectories.length)
            			maxChildren = 2 + subDirectories.length;
            		SftpFile file = findDirectoryWithOpenings(dir, client, maxChildren, subDirectories);
            		String filePath = file.getAbsolutePath();
            		if(!filePath.startsWith(dir))
            			throw new IOException("Resultant file '" + filePath + "' does not start with base directory '" + dir + "'");
            		if(filePath.length() > dir.length()) {
            			resKey.insertSubDirectory(filePath.substring(dir.length()));
	            		path = resKey.getPath();
	            		if(path.charAt(0) != '/') {
	            			path = '/' + path;
	            		}
            		}
            	}
            	break;
            } else if(end > start) { // no need to check "...//" on second slash
	            dir = basePath + path.substring(0, end);
	            try {
	            	client.getAttributes(dir);
	            } catch (IOException ex) {
	            	client.makeDirectory(dir);
	            	client.changePermissions(dir, 0022 ^ 0777); //default_permissions ^ umask
	            }
            }
        }
		return basePath + path;
	}
	protected SftpFile searchSubDirForOpenings(String baseDir, SftpSubsystemClient client, int maxChildren, String[] subDirectories) throws IOException {
		List<SftpFile> children = new ArrayList<SftpFile>();
		SftpFile file = client.openDirectory(baseDir);
		int n = client.listChildren(file, children);
		//NOTE: listChildren includes "." and ".." so we are really checking (n-2)+1 against maxChildren
		if(n - 2 + subDirectories.length < maxChildren)
			return file;

		Map<String, SftpFile> names = new HashMap<String, SftpFile>();
		for(SftpFile f : children) {
			names.put(f.getFilename(), f);
		}
		int lastUnfound = -1;
		for(int i = subDirectories.length - 1; i >= 0; i--) {
			subDirectories[i] = StringUtils.trimChars(subDirectories[i], ResourceUtils.DIR_SEPARATORS);
			file = names.get(subDirectories[i]);
			if(file != null) {
				if(file.isDirectory()) {
				children.clear();
        		n = client.listChildren(file, children);
        		if(n - 2 + subDirectories.length < maxChildren)
        			return file;
				}
        	} else
        		lastUnfound = i;

		}
		//No directory openings found, check if we can create a new one
		if(lastUnfound >= 0) {
			String dir = baseDir + '/' + subDirectories[lastUnfound];
			try {
				file = client.openDirectory(dir);
			} catch (IOException ex) {
	        	client.makeDirectory(dir);
	        	client.changePermissions(dir, 0022 ^ 0777); //default_permissions ^ umask
	        	file = client.openDirectory(dir);
	        }
			return file;
		}
		return null;
	}
	protected SftpFile findDirectoryWithOpenings(String baseDir, SftpSubsystemClient client, int maxChildren, String[] subDirectories) throws IOException {
		for(int depth = 0; depth < 16; depth++) {
			SftpFile file = searchDirectoryForOpenings(baseDir, client, maxChildren, subDirectories, depth);
			if(file != null)
				return file;
		}
		throw new IOException("Could not find a directory opening even to a depth of 16 directories from '" + baseDir + "'");
	}
	protected SftpFile searchDirectoryForOpenings(String baseDir, SftpSubsystemClient client, int maxChildren, String[] subDirectories, int depth) throws IOException {
		if(depth == 0) {
			return searchSubDirForOpenings(baseDir, client, maxChildren, subDirectories);
		}
		for(int i = 0; i < subDirectories.length; i++) {
			SftpFile file = searchDirectoryForOpenings(baseDir + '/' + subDirectories[i], client, maxChildren, subDirectories, depth-1);
			if(file != null)
				return file;
		}
		return null;
	}

	public long getInitialFileUID() {
		return initialFileAttributes == null ? 0 : initialFileAttributes.getUID().longValue();
	}
	public void setInitialFileUID(long uid) {
		if(uid == 0) {
			if(initialFileAttributes != null)
				initialFileAttributes.setUID(null);
		} else {
			if(initialFileAttributes == null)
				initialFileAttributes = new FileAttributes();
			initialFileAttributes.setUID(new UnsignedInteger32(uid));
		}
	}
	public long getInitialFileGID() {
		return initialFileAttributes == null ? 0 : initialFileAttributes.getGID().longValue();
	}
	public void setInitialFileGID(long gid) {
		if(gid == 0) {
			if(initialFileAttributes != null)
				initialFileAttributes.setGID(null);
		} else {
			if(initialFileAttributes == null)
				initialFileAttributes = new FileAttributes();
			initialFileAttributes.setGID(new UnsignedInteger32(gid));
		}
	}
	public String getInitialFilePermissions() {
		return initialFileAttributes == null ? null : initialFileAttributes.getPermissionsString();
	}
	public void setInitialFilePermissions(String permissions) {
		if(permissions == null) {
			if(initialFileAttributes != null)
				initialFileAttributes.setPermissions((UnsignedInteger32)null);
		} else {
			if(initialFileAttributes == null)
				initialFileAttributes = new FileAttributes();
			initialFileAttributes.setPermissions(permissions);
		}
	}
	public int getMaxChildrenPerFolder() {
		return maxChildrenPerFolder;
	}

	public void setMaxChildrenPerFolder(int maxChildrenPerFolder) {
		this.maxChildrenPerFolder = maxChildrenPerFolder;
	}

	public String[] getSubFolderNames() {
		return subFolderNames;
	}

	public void setSubFolderNames(String[] subFolderNames) {
		this.subFolderNames = subFolderNames;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public long getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(long maxWait) {
		this.maxWait = maxWait;
		for(ListIterator<Controller> iter = getControlIterator(null); iter.hasNext(); )
			iter.next().setMaxWait(maxWait);
	}

	protected boolean isKeepAlive() {
		return keepAlive;
	}

	protected void setKeepAlive(boolean keepAlive) {
		this.keepAlive = keepAlive;
	}

	public boolean isTracing() {
		return tracing;
	}

	public void setTracing(boolean tracing) {
		this.tracing = tracing;
		for(ListIterator<Controller> iter = getControlIterator(null); iter.hasNext(); )
			iter.next().setTracing(tracing);
	}

	public String getCompressedSuffix() {
		return compressedSuffix;
	}
	public void setCompressedSuffix(String compressedSuffix) {
		if(compressedSuffix == null || (compressedSuffix=compressedSuffix.trim()).length() == 0)
			this.compressedSuffix = null;
		else if(compressedSuffix.startsWith("."))
			this.compressedSuffix = compressedSuffix;
		else
			this.compressedSuffix = "." + compressedSuffix;
	}
}
