/**
 *
 */
package simple.io.resource.sftp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import simple.io.Interaction;
import simple.io.Log;

import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolException;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;
import com.sshtools.j2ssh.authentication.KBIAuthenticationClient;
import com.sshtools.j2ssh.authentication.KBIPrompt;
import com.sshtools.j2ssh.authentication.KBIRequestHandler;
import com.sshtools.j2ssh.authentication.PasswordAuthenticationClient;
import com.sshtools.j2ssh.authentication.PublicKeyAuthenticationClient;
import com.sshtools.j2ssh.authentication.SshAuthenticationClient;
import com.sshtools.j2ssh.authentication.SshAuthenticationPrompt;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.IgnoreHostKeyVerification;
import com.sshtools.j2ssh.transport.InvalidHostFileException;
import com.sshtools.j2ssh.transport.publickey.SshPrivateKey;
import com.sshtools.j2ssh.transport.publickey.SshPrivateKeyFile;
import com.sshtools.j2ssh.transport.publickey.dsa.SshDssPrivateKey;
import com.sshtools.j2ssh.transport.publickey.rsa.SshRsaPrivateKey;

/**
 * @author Brian S. Krug
 *
 */
public class SftpUtils {
	private static final Log log = Log.getLog();
	public static enum PrivateKeyType { DSA, RSA, FORMATTED };

	public static void authenticate(SshClient sshClient, String username, String password) throws IOException {
		authenticate(sshClient, username, password, null, null, null);
	}

	public static void authenticate(SshClient sshClient, String username, String password, byte[] formattedPrivateKey, String passphrase, byte[] dsaPrivateKey, byte[] rsaPrivateKey) throws IOException {
		byte[] privateKey;
		PrivateKeyType type;
		if(formattedPrivateKey != null && formattedPrivateKey.length > 0) {
			privateKey = formattedPrivateKey;
			type = PrivateKeyType.FORMATTED;
		} else if(dsaPrivateKey != null && dsaPrivateKey.length > 0) {
			privateKey = dsaPrivateKey;
			type = PrivateKeyType.DSA;
		} else if(rsaPrivateKey != null && rsaPrivateKey.length > 0) {
			privateKey = rsaPrivateKey;
			type = PrivateKeyType.RSA;
		} else {
			privateKey = null;
			type = null;
		}
		authenticate(sshClient, username, password, privateKey, passphrase, type);
	}

	public static void authenticate(SshClient sshClient, String username, String password, byte[] privateKey, String passphrase, PrivateKeyType privateKeyType) throws IOException {
		authenticate(sshClient, username, password, privateKey, passphrase, privateKeyType, null);
	}

	public static void authenticate(SshClient sshClient, String username, String password, byte[] privateKey, String passphrase, PrivateKeyType privateKeyType, Interaction interaction) throws IOException {
		List<?> methods = sshClient.getAvailableAuthMethods(username);
		List<SshAuthenticationClient> auths = new ArrayList<SshAuthenticationClient>(3/*methods.size()*/);
		if(privateKeyType != null && privateKey != null && privateKey.length > 0) {
    		SshPrivateKey key;
    		switch(privateKeyType) {
    			case FORMATTED:
    				key = SshPrivateKeyFile.parse(privateKey).toPrivateKey(passphrase);
    				break;
    			case DSA:
    				key = new SshDssPrivateKey(privateKey);
    				break;
    			case RSA:
    				key = new SshRsaPrivateKey(privateKey);
    				break;
    			default:
    				throw new IllegalArgumentException("Invalid PrivateKeyType " + privateKeyType);
    		}

			auths.add(createPKAuth(username, key));
    	}
		if(username != null && (password != null || interaction != null)) {
			if(methods.contains("password"))
				auths.add(createPasswordAuth(username, password, interaction, sshClient.getConnectionProperties().getHost()));
			else if(methods.contains("keyboard-interactive"))
				auths.add(createKBIAuth(username, password, interaction, sshClient.getConnectionProperties().getHost()));
		}

        // Try the authentication
		authenticate(sshClient, auths.toArray(new SshAuthenticationClient[auths.size()]));
	}

	protected static SshAuthenticationClient createKBIAuth(final String username, final String password, final Interaction interaction, final String hostname) {
		// Create a keyboard interactive authentication instance
		final KBIAuthenticationClient kbiAuth = new KBIAuthenticationClient();
		kbiAuth.setUsername(username);
		kbiAuth.setKBIRequestHandler(new KBIRequestHandler() {
			@Override
			public void showPrompts(String name, String instruction, KBIPrompt[] prompts) {
				switch(prompts.length) {
					case 1:
						if(password == null && interaction != null) {
							String username = kbiAuth.getUsername();
							if(username == null || username.length() == 0)
								username = interaction.readLine("Username on %1$s:", hostname);
							if(username == null || username.length() == 0)
								return;
							kbiAuth.setUsername(username);
							char[] enteredPassword = interaction.readPassword("Password for %1$s on %2$s:", username, hostname);
							if(enteredPassword == null || enteredPassword.length == 0)
								return;
							prompts[0].setResponse(new String(enteredPassword));
						} else
							prompts[0].setResponse(password);
						break;
				}
			}
		});
		return kbiAuth;
	}

	protected static SshAuthenticationClient createPasswordAuth(String username, String password, final Interaction interaction, final String hostname) {
		// Create a password authentication instance
		final PasswordAuthenticationClient passAuth = new PasswordAuthenticationClient();
        passAuth.setUsername(username);
        passAuth.setPassword(password);
		try {
			passAuth.setAuthenticationPrompt(new SshAuthenticationPrompt() {
				@Override
				public boolean showPrompt(SshAuthenticationClient instance) throws AuthenticationProtocolException {
					if(interaction != null) {
						String username = passAuth.getUsername();
						if(username == null || username.length() == 0)
							username = interaction.readLine("Username on %1$s:", hostname);
						if(username == null || username.length() == 0)
							return false;
						passAuth.setUsername(username);
						char[] enteredPassword = interaction.readPassword("Password for %1$s on %2$s:", username, hostname);
						if(enteredPassword == null || enteredPassword.length == 0)
							return false;
						passAuth.setPassword(new String(enteredPassword));
					}
					return true;
				}
			});
		} catch(AuthenticationProtocolException e) {
			log.warn("Could not set password prompt; continuing anyway", e);
		}
        return passAuth;
	}

	protected static SshAuthenticationClient createPKAuth(String username, SshPrivateKey key) {
		PublicKeyAuthenticationClient pkAuth = new PublicKeyAuthenticationClient();
		pkAuth.setUsername(username);
		pkAuth.setKey(key);
		return pkAuth;
	}

	public static HostKeyVerification createHostKeyVerification(String knownHostsFile) throws InvalidHostFileException {
		if(knownHostsFile == null || (knownHostsFile = knownHostsFile.trim()).length() == 0) {
			return new IgnoreHostKeyVerification();
		} else {
			return new AutoKnownHostsKeyVerification(knownHostsFile);
		}
	}

	public static void authenticate(SshClient sshClient, SshAuthenticationClient[] auths)	throws IOException {
		for(int i = 0; i < auths.length; i++) {
			int result;
			try {
				result = sshClient.authenticate(auths[i]);
			} catch(IOException e) {
				IOException ioe = new IOException("While authenticating");
				ioe.initCause(e);
				throw ioe;
			}
			// Evaluate the result
			switch(result) {
				case AuthenticationProtocolState.COMPLETE:
					return;
				case AuthenticationProtocolState.CANCELLED:
					throw new IOException("Authorization was cancelled");
				case AuthenticationProtocolState.FAILED:
					throw new IOException("Authorization failed");
				case AuthenticationProtocolState.PARTIAL:
					break;
				default:
					throw new IOException("Authorization not complete");
			}
		}
		throw new IOException("Authorization could not complete");
	}

	public static void authenticate(SshClient sshClient, String username, String hostname, Interaction interaction) throws IOException {
		List<?> methods = sshClient.getAvailableAuthMethods(username);
		SshAuthenticationClient auth = null;
		for(Object method : methods) {
			auth = createAuthenticationClient(hostname, (String) method, interaction);
			if(auth != null)
				break;
		}
		if(auth == null)
			throw new IOException("None of the auth methods " + methods + " is supported");
		auth.setUsername(username);
		// Create a password authentication instance
		int result;
		try {
			result = sshClient.authenticate(auth);
		} catch(IOException e) {
			IOException ioe = new IOException("While authenticating");
			ioe.initCause(e);
			throw ioe;
		} catch(RuntimeException e) {
			log.warn("Could not log on", e);
			throw e;
		} catch(Error e) {
			log.warn("Could not log on", e);
			throw e;
		}
		// Evaluate the result
		switch(result) {
			case AuthenticationProtocolState.COMPLETE:
				return;
			case AuthenticationProtocolState.CANCELLED:
				throw new IOException("Authorization was cancelled");
			case AuthenticationProtocolState.FAILED:
				throw new IOException("Authorization failed");
			case AuthenticationProtocolState.PARTIAL:
				throw new IOException("Authorization could not complete");
			default:
				throw new IOException("Authorization not complete");
		}
	}

	protected static SshAuthenticationClient createAuthenticationClient(final String hostname, String method, final Interaction interaction) throws AuthenticationProtocolException {
		if("password".equals(method)) {
			final PasswordAuthenticationClient passAuth = new PasswordAuthenticationClient();
			passAuth.setAuthenticationPrompt(new SshAuthenticationPrompt() {
				@Override
				public boolean showPrompt(SshAuthenticationClient instance) throws AuthenticationProtocolException {
					String username = passAuth.getUsername();
					if(username == null || username.length() == 0)
						username = interaction.readLine("Username on %1$s:", hostname);
					if(username == null || username.length() == 0)
						return false;
					passAuth.setUsername(username);
					char[] password = interaction.readPassword("Password for %1$s on %2$s:", username, hostname);
					if(password == null || password.length == 0)
						return false;
					passAuth.setPassword(new String(password));
					return true;
				}
			});
			return passAuth;
		} else if("keyboard-interactive".equals(method)) {
			final KBIAuthenticationClient kbiAuth = new KBIAuthenticationClient();
			kbiAuth.setKBIRequestHandler(new KBIRequestHandler() {
				@Override
				public void showPrompts(String name, String instruction, KBIPrompt[] prompts) {
					for(KBIPrompt prompt : prompts) {
						String username = kbiAuth.getUsername();
						StringBuilder sb = new StringBuilder();
						if(name != null && name.length() > 0)
							sb.append("[ ").append(name).append(" ] - ");
						if(instruction != null && instruction.length() > 0)
							sb.append(instruction).append(": ");
						sb.append(prompt.getPrompt());
						if(username != null && username.length() > 0)
							sb.append(" for ").append(username);
						sb.append(" on ").append(hostname);
						String value;
						if(prompt.echo()) {
							value = interaction.readLine(sb.toString());
							if(value == null || value.length() == 0)
								return;
						} else {
							char[] cvalue = interaction.readPassword(sb.toString());
							if(cvalue == null || cvalue.length == 0)
								return;
							value = new String(cvalue);
						}
						prompt.setResponse(value);
					}
				}
			});
			return kbiAuth;
		}
		return null;
	}
}
