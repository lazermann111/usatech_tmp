package simple.io.resource.sftp;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import simple.io.Log;
import simple.io.resource.Repository;
import simple.util.ArrayListIterator;

/**
 * This class represents a sftp resource, it can be used to read and write to a
 * sftp file to multiple locations.
 *
 * @author yhe, bkrug
 *
 */
public class ReplicatedSftpResourceFolder extends AbstractSftpResourceFolder {
	private static final Log log = Log.getLog();

	protected final Map<SftpRepository,Controller> repoControlMap = new ConcurrentHashMap<SftpRepository,Controller>();
	protected Controller[] controllers;
	protected int replicas = 2;
	protected int concurrency = 5;
	protected final AtomicInteger roundRobin = new AtomicInteger();

	public ReplicatedSftpResourceFolder() {
	}

	public void initialize() throws IOException {
		if(getReplicas() > repoControlMap.size())
			throw new IOException("Not enough repositories configured to support " + getReplicas() + " replicas");
	}
	@Override
	public int getReplicas() {
		return replicas;
	}

	public void setReplicas(int replicas) {
		this.replicas = replicas;
	}

	public SftpRepository[] getRepositories() {
		return repoControlMap.keySet().toArray(EMPTY_REPOS); //use empty array so that if size() varies while doing array it does not affect the output
	}

	public void setRepositoryList(String[] repositoryList) throws ParseException {
		SftpRepository[] repos;
		if(repositoryList == null) {
			repos = null;
		} else {
			repos = new SftpRepository[repositoryList.length];
			for(int i = 0; i < repositoryList.length; i++)
				repos[i] = new SftpRepository(repositoryList[i]);
		}
		setRepositories(repos);
	}
	public void setRepositories(SftpRepository[] repositories) {
		if(repositories == null || repositories.length == 0) {
			//remove all
			Iterator<Map.Entry<SftpRepository,Controller>> iter = repoControlMap.entrySet().iterator();
			while(iter.hasNext()) {
				iter.next().getValue().close();
				iter.remove();
			}
		} else {
			Set<SftpRepository> retain = new HashSet<SftpRepository>();
			for(SftpRepository r : repositories) {
				if(!repoControlMap.containsKey(r))
					repoControlMap.put(r, new Controller(r));
				retain.add(r);
			}
			Iterator<Map.Entry<SftpRepository,Controller>> iter = repoControlMap.entrySet().iterator();
			while(iter.hasNext()) {
				Map.Entry<SftpRepository,Controller> entry = iter.next();
				if(!retain.contains(entry.getKey())) {
					entry.getValue().close();
					iter.remove();
				}
			}
		}
		controllers = repoControlMap.values().toArray(EMPTY_CONTROLS);//use empty array so that if size() varies while doing array it does not affect the output
	}

	@Override
	public int getConcurrency() {
		return concurrency;
	}

	public void setConcurrency(int concurrency) {
		this.concurrency = concurrency;
	}

	@Override
	protected ListIterator<Controller> getControlIterator(Repository[] firstRepos) {
		Controller[] controllers = this.controllers; //protect against mid-stream changes
		int start = roundRobin.getAndIncrement();
		if(start < 0)
			start = (controllers.length - 1) + ((start + 1) % controllers.length);
    	else
    		start = start % controllers.length;
		if(firstRepos != null && firstRepos.length > 0) {
			Set<Controller> tmp = new LinkedHashSet<Controller>();
			for(Repository repo : firstRepos) {
				Controller con = repoControlMap.get(repo);
				if(con == null)
					log.debug("Repository '" + repo + "' was removed");
				else
					tmp.add(con);

			}
			int end = start + controllers.length;
			for(int i = start; i < end; i++)
				tmp.add(controllers[i%controllers.length]);

			controllers = tmp.toArray(new Controller[tmp.size()]);
			start = 0;
		}
		return new ArrayListIterator<Controller>(controllers, start);
	}
	public void release() {
		setRepositories(null);
	}
}
