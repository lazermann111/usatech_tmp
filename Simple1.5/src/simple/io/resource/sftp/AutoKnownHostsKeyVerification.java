package simple.io.resource.sftp;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import simple.io.Log;

import com.sshtools.j2ssh.transport.AbstractKnownHostsKeyVerification;
import com.sshtools.j2ssh.transport.InvalidHostFileException;
import com.sshtools.j2ssh.transport.TransportProtocolException;
import com.sshtools.j2ssh.transport.publickey.SshPublicKey;

/**
 * Provides automatic host key verification, always allowing new hosts, but requiring existing
 * hosts to use the same public key that they used before. Also, wraps a lock mechanism around
 * any methods that access the knownhosts Map so as to be thread-safe.
 *
 */
public class AutoKnownHostsKeyVerification extends AbstractKnownHostsKeyVerification {
	private static final Log log = Log.getLog();
	
	public AutoKnownHostsKeyVerification(String knownhosts) throws InvalidHostFileException {
		super(knownhosts);
	}

	public void onHostKeyMismatch(String host, SshPublicKey allowedHostKey, SshPublicKey actualHostKey) throws TransportProtocolException {
		throw new TransportProtocolException("Host '" + host + "' public key does not match the one previously used.");
	}

	public void onUnknownHost(String host, SshPublicKey key) throws TransportProtocolException {
		log.debug("Adding host '" + host + "' to the list of known hosts");
		allowHost(host, key, true);
	}

	protected Map<?,?> createAllowedHostsMap() {
		return new ConcurrentHashMap<Object,Object>();
	}
}
