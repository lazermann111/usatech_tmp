/**
 *
 */
package simple.io.resource.sftp;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.io.resource.Repository;

public class SftpRepository extends Repository {
	protected static final Pattern repoParser = Pattern.compile("sftp\\:([^:@]+)(?:\\:([^:@]*)(?:\\:([^:@]*)(?:\\:([^@]+))?)?)?@([^:@]+)\\:(.+)"); // user[:password[:passphrase[:privateKeyFile]]]@host:/directory/path
	protected static final long MAX_PRIVATE_KEY_FILE_SIZE = 65536;
	protected String username;
	protected String password;
	protected String passphrase;
	protected File privateKeyFile;

	public SftpRepository(String uri) throws ParseException {
		super();
		setUri(uri);
	}
	public SftpRepository(String host, String username, String password, String path) {
		super(host, path);
		this.username = username;
		this.password = password;
	}
	public SftpRepository() {
		super();
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUri() {
		StringBuilder sb = new StringBuilder().append("sftp:");
		if(username != null)
			sb.append(username);
		if(password != null || passphrase != null || privateKeyFile != null) {
			sb.append(':');
			if(password != null)
				sb.append(password);
			if(passphrase != null || privateKeyFile != null) {
				sb.append(':');
				if(passphrase != null)
					sb.append(passphrase);
				if(privateKeyFile != null)
					sb.append(':').append(privateKeyFile.getAbsolutePath());
			}
		}
		sb.append('@');
		if(host != null)
			sb.append(host);
		sb.append(':');
		if(path != null)
			sb.append(path);
		return sb.toString();
	}
	public void setUri(String uri) throws ParseException {
		Matcher matcher = repoParser.matcher(uri);
		if(!matcher.matches())
			throw new ParseException("Repository uri is not in a valid format. Please use 'sftp:username[:password[:passphrase[:privateKeyFile]]]@host:/directory/path'", 0);
		username = matcher.group(1);
		password = matcher.group(2);
		host = matcher.group(5);
		path = matcher.group(6);
		passphrase = matcher.group(3);
		String fn = matcher.group(4);
		privateKeyFile = (fn == null || (fn=fn.trim()).length() == 0 ? null : new File(fn));
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder().append("sftp:");
		if(username != null)
			sb.append(username);
		sb.append('@');
		if(host != null)
			sb.append(host);
		sb.append(':');
		if(path != null)
			sb.append(path);
		return sb.toString();
	}
	public String getPassphrase() {
		return passphrase;
	}
	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}
	public File getPrivateKeyFile() {
		return privateKeyFile;
	}
	public void setPrivateKeyFile(File privateKeyFile) {
		this.privateKeyFile = privateKeyFile;
	}
	/**
	 * @return
	 * @throws IOException
	 */
	public byte[] getPrivateKeyBytes() throws IOException {
		File file = getPrivateKeyFile();
		if(file == null)
			return null;
		if(!file.exists())
			throw new IOException("Private Key File '" + file.getAbsolutePath() + "' does not exist");
		if(!file.canRead())
			throw new IOException("Private Key File '" + file.getAbsolutePath() + "' is not readable");
		RandomAccessFile raf = new RandomAccessFile(file, "r");
		long length = raf.length();
		if(length > MAX_PRIVATE_KEY_FILE_SIZE)
			throw new IOException("Private Key File '" + file.getAbsolutePath() + "' is too large. Actual size = " + length + "; max size = " + MAX_PRIVATE_KEY_FILE_SIZE);
		byte[] bytes = new byte[(int)length];
		raf.read(bytes);
		return bytes;
	}
}