package simple.io.resource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import simple.lang.SystemUtils;
import simple.text.StringUtils;
import simple.util.CollectionUtils;

public class ResourceUtils {
	public static final char[] DIR_SEPARATORS = "\\/".toCharArray();
	protected static final Pattern keyParser = Pattern.compile("(?:\\[((?:[\\w.-]*(?:\\:[\\\\/\\w+*.%-]*)?[,;])*[\\w.-]*(?:\\:[\\\\/\\w+*.%-]*)?)\\])?(([\\\\/\\w+*.%-]+?)(\\.\\w+)?(?:\\((\\d+)\\)(\\.\\w+)?)?)");
	protected static final Pattern subDirPattern = Pattern.compile("[\\\\/\\w+*.%-]+");
	protected static final Random random = new Random();
	public static class ResourceKey {
		protected Repository[] repositories;
		protected String path;
		protected String directory;
		protected String prefix;
		protected String suffix;
		protected String ordinal;
		protected String fileName;
		protected String optionalSuffix;
		public boolean hasOptionalSuffix() {
			return optionalSuffix.length() > 0;
		}
		public String getFileName() {
			if(fileName == null) {
				int pos = prefix.lastIndexOf('/');
				if(pos < 0)
					fileName = prefix + suffix;
				else
					fileName = prefix.substring(pos+1) + suffix;
				try {
					fileName = URLDecoder.decode(fileName, "UTF-8");
				} catch(UnsupportedEncodingException e) {
					throw new RuntimeException("System does not have UTF-8 encoding", e);
				}
			}
			return fileName;
		}
		public void nextPath() {
			ordinal = "(" + (random.nextInt() & 0xffff) + ')';
			buildPath();
		}
		protected void buildPath() {
			path = directory + prefix + ordinal + suffix + optionalSuffix;
		}
		public Repository[] getRepositories() {
			return repositories;
		}
		public String getPath() {
			return path;
		}
		public void insertSubDirectory(String subDirectory) throws ParseException {
			if(!subDirPattern.matcher(subDirectory).matches())
				throw new ParseException("Invalid character in sub directory", 0);
			directory = directory + StringUtils.trimChars(fixBackSlash(subDirectory), DIR_SEPARATORS) + '/';
			buildPath();
		}
		@Override
		public String toString() {
			if(repositories != null && repositories.length > 0) {
				StringBuilder sb = new StringBuilder();
				sb.append('[');
				for(Repository r : repositories) {
					if(r.host != null)
						sb.append(r.host);
					sb.append(':');
					if(path != null)
						sb.append(r.path);
					sb.append(',');
				}
				sb.setLength(sb.length() - 1);
				sb.append(']');
				return sb.append(path).toString();
			} else
				return path;
		}
	}
	/** Determines whether the given string is a valid key for use in {@link ResourceFolder#getResource(String, String, ResourceMode)}.
	 * This method tests the format of the string to determine if it can be parsed as a key for a resource.
	 * @param key the string to test
	 * @return <code>true</code> if and only if the given key is formatted correctly
	 */
	public static boolean isValidKey(String key) {
		Matcher matcher = keyParser.matcher(key);
		return matcher.matches();
	}
	public static ResourceKey parseKey(String key, String optionalSuffix) throws ParseException {
		if(optionalSuffix != null && optionalSuffix.length() > 0 && key.endsWith(optionalSuffix))
			key = key.substring(0, key.length() - optionalSuffix.length());
		else
			optionalSuffix = "";
		Matcher matcher = keyParser.matcher(key);
		if(!matcher.matches())
			throw new ParseException("Invalid key '" + key + "'", 0);
		ResourceKey resKey = new ResourceKey();
		String repos = matcher.group(1);
		if(repos != null && (repos=repos.trim()).length() > 0) {
			String[] repoArr = StringUtils.split(repos, StringUtils.STANDARD_DELIMS, false);
			resKey.repositories = new Repository[repoArr.length];
			for(int i = 0; i < repoArr.length; i++) {
				int pos = repoArr[i].indexOf(':');
				String repoHost, repoPath;
				if(pos >= 0) {
					repoHost = repoArr[i].substring(0, pos);
					repoPath = fixBackSlash(repoArr[i].substring(pos+1));
				} else {
					repoHost = repoArr[i];
					repoPath = "";
				}
				resKey.repositories[i] = new Repository(repoHost, repoPath);
			}
		}
		String tmp;
		resKey.path = fixBackSlash(matcher.group(2)) + optionalSuffix;
		tmp = fixBackSlash(matcher.group(3));
		int pos = tmp.lastIndexOf('/');
		if(pos < 0) {
			resKey.prefix = tmp;
			resKey.directory = "";
		} else {
			resKey.prefix = tmp.substring(pos+1);
			resKey.directory = tmp.substring(0, pos+1);
		}
		resKey.suffix = fixBackSlash(SystemUtils.nvl(matcher.group(4), matcher.group(6)));
		tmp = matcher.group(5);
		if(tmp != null && tmp.length() > 0)
			resKey.ordinal = '(' + tmp + ')';
		else
			resKey.ordinal = "";
		resKey.optionalSuffix = optionalSuffix;
		return resKey;
	}
	public static String fixBackSlash(String path) {
		if(path == null)
			return "";
		return path.replace('\\', '/');
	}
	public static String getAbsolutePath(String directory, String path) {
		if(path.charAt(0) == '/' || directory == null || directory.length() == 0)
			return path;
		else {
			if(directory.charAt(directory.length()-1) == '/')
				return directory + path;
			else
				return directory + '/' + path;
		}
	}
	public static String convertToKey(String fileName) {
		String[] parts = StringUtils.split(fileName, DIR_SEPARATORS, false);
		StringBuilder sb = new StringBuilder();
		if(CollectionUtils.iterativeSearch(DIR_SEPARATORS, fileName.charAt(0)) >= 0)
			sb.append('/');
		for(String p : parts)
			try {
				sb.append(URLEncoder.encode(p, "UTF-8")).append('/');
			} catch(UnsupportedEncodingException e) {
				throw new RuntimeException("System does not have UTF-8 encoding", e);
			}
		return sb.substring(0, sb.length() - 1);
	}
}
