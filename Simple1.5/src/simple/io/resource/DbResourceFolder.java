/**
 *
 */
package simple.io.resource;

import java.io.IOException;

/**
 * @author Brian S. Krug
 *
 */
public class DbResourceFolder extends AbstractDbResourceFolder {
	protected String dataSourceName;
	/**
	 * @see simple.io.resource.ResourceFolder#getResource(java.lang.String, simple.io.resource.ResourceMode)
	 */
	public Resource getResource(String key, ResourceMode mode) throws IOException {
		DbStash stash = getStash(key, mode);
		if(stash == null)
			throw new IOException("Resource '" + key + "' not found");
		return new SingleStashResource(stash, stash.getKey(), null, stash.getCompression());
	}

	protected DbStash getStash(String key, ResourceMode mode) throws IOException {
		switch(mode) {
			case CREATE:
				return createFile(generateFileId(), key, getDataSourceName(), getCompression());
			case READ:
				return selectFile(key, true, getDataSourceName());
			case UPDATE:
				return selectFile(key, false, getDataSourceName());
			 case DELETE:
				 DbStashImpl stash = selectFile(key, false, getDataSourceName());
				 if(stash != null)
					 return stash;
				 else
					 return new DeletedDbStash(key);
		}
		return null;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
}
