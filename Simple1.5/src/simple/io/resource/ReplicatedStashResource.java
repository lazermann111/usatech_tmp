package simple.io.resource;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;

import simple.io.Compression;
import simple.io.ParallelOutputStream;
import simple.io.PassThroughOutputStream;

public class ReplicatedStashResource extends SingleStashResource {
	protected final Stash[] stashes;
	protected final ExecutorService executor;
	protected int outputBufferSize;
	protected boolean parallel = false;

	public ReplicatedStashResource(Stash[] stashes, ExecutorService executor, String key, String fileName) {
		this(stashes, executor, key, fileName, 8192);
	}
	public ReplicatedStashResource(Stash[] stashes, ExecutorService executor, String key, String fileName, int outputBufferSize) {
		this(stashes, executor, key, fileName, outputBufferSize, false);
	}
	public ReplicatedStashResource(Stash[] stashes, ExecutorService executor, String key, String fileName, int outputBufferSize, boolean parallel) {
		this(stashes, executor, key, fileName, outputBufferSize, parallel, null);
	}

	public ReplicatedStashResource(Stash[] stashes,ExecutorService executor, String key, String fileName, int outputBufferSize, boolean parallel, Compression compression) {
		super(stashes[0], key, fileName, compression);
		this.stashes = stashes;
		this.executor = executor;
		this.outputBufferSize = outputBufferSize;
		this.parallel = parallel;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		OutputStream out;
		if(isParallel()) {
			if(executor == null)
				throw new IOException("No Executor Service was provided; cannot use parallel write");
			ParallelOutputStream ptos = new ParallelOutputStream(executor);
			for(int i = 0; i < stashes.length; i++) {
				ptos.addOutputStream(stashes[i].getUnbufferedOutputStream());
			}
			out = ptos;
		} else {
			PassThroughOutputStream ptos = new PassThroughOutputStream();
			for(Stash stash : stashes)
				ptos.addOutputStream(stash.getUnbufferedOutputStream());
			out = ptos;
		}
		out = new BufferedOutputStream(out, getOutputBufferSize()) {
			@Override
			public void close() throws IOException {
				flush(); // DO NOT ignore exceptions as FilterOutputStream does
				out.close();
			}
		};
		Compression c = compression;
		if(c != null)
			out = c.getCompressingOutputStream(out);
		return new ResourceOutputStream(out) {
			@Override
			public void flush() throws IOException {
				super.flush();
				for(Stash s : stashes)
					s.setLength(length);
			}
		};
	}

	@Override
	public void release() {
		closeAbandonedOutputs();
		for(Stash stash : stashes)
			stash.close();
	}

	/**
	 * @see simple.io.resource.SingleStashResource#setContentType(java.lang.String)
	 */
	@Override
	public void setContentType(String contentType) {
		for(Stash stash : stashes)
			stash.setContentType(contentType);
	}

	public int getOutputBufferSize() {
		return outputBufferSize;
	}

	public void setOutputBufferSize(int outputBufferSize) {
		this.outputBufferSize = outputBufferSize;
	}
	public boolean isParallel() {
		return parallel;
	}
	public void setParallel(boolean parallel) {
		this.parallel = parallel;
	}
	@Override
	public boolean delete() {
		boolean success = true;
		for(Stash stash : stashes)
			if(!stash.delete())
				success = false;
		return success;
	}
}
