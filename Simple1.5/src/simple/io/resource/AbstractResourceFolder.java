package simple.io.resource;

import java.util.concurrent.ExecutorService;

import simple.io.Compression;


/**
 * Base class for common ResourceFolder methods.
 *
 * @author yhe, bkrug
 *
 */
public abstract class AbstractResourceFolder implements ResourceFolder {
	protected int inputBufferSize = 8192;
	protected int outputBufferSize = 8192;
	protected boolean parallelOutputStream = true;
	protected boolean compressing;

	protected Resource createResource(Stash[] stashes, ExecutorService executor, String key, String fileName, Compression compression) {
		if(stashes.length == 1)
			return new SingleStashResource(stashes[0], key, fileName, compression);
		else
			return new ReplicatedStashResource(stashes, executor, key, fileName, getOutputBufferSize(), isParallelOutputStream(), compression);
	}

	public int getInputBufferSize() {
		return inputBufferSize;
	}
	public void setInputBufferSize(int inputBufferSize) {
		this.inputBufferSize = inputBufferSize;
	}
	public int getOutputBufferSize() {
		return outputBufferSize;
	}
	public void setOutputBufferSize(int outputBufferSize) {
		this.outputBufferSize = outputBufferSize;
	}
	public boolean isParallelOutputStream() {
		return parallelOutputStream;
	}
	public void setParallelOutputStream(boolean parallelOutputStream) {
		this.parallelOutputStream = parallelOutputStream;
	}
	public boolean isCompressing() {
		return compressing;
	}
	public void setCompressing(boolean compressing) {
		this.compressing = compressing;
	}
	public Compression getCompression() {
		return isCompressing() ? Compression.ZLIB : null;
	}
}
