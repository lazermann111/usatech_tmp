/**
 *
 */
package simple.io.resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

import javax.activation.FileTypeMap;

import org.postgresql.PGConnection;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DBUnwrap;
import simple.db.DataLayer;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DataSourceNotFoundException;
import simple.db.DbUtils;
import simple.io.AbstractBufferedInputStream;
import simple.io.AbstractBufferedOutputStream;
import simple.io.Compression;
import simple.io.Log;
import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public abstract class AbstractDbResourceFolder extends AbstractResourceFolder {
	private static final Log log = Log.getLog();
	protected final static String FILE_ID_PREFIX = ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':';
	protected final static AtomicLong fileIDGenerator = new AtomicLong(Double.doubleToLongBits(Math.random()));

	protected String insertCallId;
	protected String selectCallId;
	protected String selectForUpdateCallId;
	protected String updateCallId;
	protected String deleteCallId;
	protected DataLayer dataLayer = DataLayerMgr.getGlobalDataLayer();
	protected int maxInMemoryBuffer = 8192;
	
	public class DbStashImpl implements DbStash {
		protected final String fileId;
		protected final String filePath;
		protected final String dataSourceName;
		protected boolean exists;
		protected boolean updatePending;
		protected final boolean readOnly;
		protected long dataLength = 0;
		protected long fileLength;
		protected String contentType;
		protected String fileName;
		protected final long loid;
		protected final Compression compression;
		protected final ReentrantLock lock = new ReentrantLock();

		public DbStashImpl(String fileId, String filePath, String contentType, long fileLength, String dataSourceName, long loid, boolean readOnly, Compression compression) {
			this.fileId = fileId;
			this.filePath = filePath;
			this.exists = true;
			this.dataSourceName = dataSourceName;
			this.readOnly = readOnly;
			this.contentType = contentType;
			this.compression = compression;
			this.fileLength = fileLength;
			this.loid = loid;
		}

		/**
		 * @see simple.io.resource.Resource#delete()
		 */
		public boolean delete() {
			lock.lock();
			try {
				if(!exists)
					return true; // already deleted
				if(readOnly)
					return false;
				try {
					deleteFile(dataSourceName, fileId);
					exists = false;
					return true;
				} catch(SQLException e) {
					log.warn("Could not delete file id " + fileId, e);
				} catch(DataLayerException e) {
					log.warn("Could not delete file id " + fileId, e);
				}
				return false;
			} finally {
				lock.unlock();
			}
		}

		/**
		 * @see simple.io.resource.Resource#getKey()
		 */
		public String getKey() {
			return fileId;
		}

		/**
		 * @see simple.io.resource.Resource#getLength()
		 */
		public long getLength() {
			return fileLength;
		}

		/**
		 * @see simple.io.resource.Resource#getPath()
		 */
		public String getAbsolutePath() {
			return filePath;
		}

		/**
		 * @see simple.io.resource.Resource#isReadable()
		 */
		public boolean canRead() {
			return true;
		}

		/**
		 * @see simple.io.resource.Resource#isWritable()
		 */
		public boolean canWrite() {
			return !readOnly;
		}

		/**
		 * @see simple.io.resource.Resource#release()
		 */
		public void close() {
			lock.lock();
			try {
				if(!readOnly) {
					updateAttributes();
				}
			} catch(IOException e) {
				log.warn("Could not update attributes of file " + fileId, e);
			} finally {
				lock.unlock();
			}
		}

		protected void updateAttributes() throws IOException {
			if(updatePending) {
				try {
					if(exists)
						updateFile(dataSourceName, fileId, filePath, contentType, fileLength);
					updatePending = false;
				} catch(SQLException e) {
					throw new IOException("Could not update file id " + fileId, e);
				} catch(DataLayerException e) {
					throw new IOException("Could not update file id " + fileId, e);
				}
			}
		}
		/**
		 * @see javax.activation.DataSource#getInputStream()
		 */
		public InputStream getInputStream() throws IOException {
			// NOTE: Since fileLength is the decompressed size this is a very rough calculation
			int size;
			if(fileLength / 2 >= getMaxInMemoryBuffer())
				size = getMaxInMemoryBuffer();
			else if(fileLength > 512)
				size = (int)(fileLength * 1.5);
			else 
				size = 512;
			return new AbstractBufferedInputStream(size) {
				protected int offset = 0;
				@Override
				protected int fill(byte[] bytes, int off, int len) throws IOException {
					int r = obtainFileContent(bytes, off, len, offset);
					if(r > 0) {
						offset += r;
					}
					return r;
				}
			};
		}

		/**
		 * @see javax.activation.DataSource#getName()
		 */
		public String getFilename() {
			if(fileName == null) {
				int pos = StringUtils.lastIndexOf(filePath, new char[] {'/', '\\'});
				if(pos < 0)
					fileName = filePath;
				else
					fileName = filePath.substring(pos + 1);
			}
			return fileName;
		}

		/**
		 * @see javax.activation.DataSource#getContentType()
		 */
		public String getContentType() {
			return contentType;
		}
		/**
		 * @see simple.io.resource.Resource#setContentType(java.lang.String)
		 */
		public void setContentType(String contentType) {
			lock.lock();
			try {
				if(!ConvertUtils.areEqual(this.contentType, contentType))
					updatePending = true;
				this.contentType = contentType;
			} finally {
				lock.unlock();
			}
		}
		/**
		 * @see simple.io.resource.Stash#getOutputStream()
		 */
		public OutputStream getOutputStream() throws IOException {
			return getUnbufferedOutputStream();
		}
		/**
		 * @see javax.activation.DataSource#getOutputStream()
		 */
		public OutputStream getUnbufferedOutputStream() throws IOException {
			if(readOnly)
				throw new IOException("Resource is read-only");
			this.dataLength = 0;
			return new AbstractBufferedOutputStream(getMaxInMemoryBuffer()) {
				@Override
				protected void commit(byte[] b, int off, int len) throws IOException {
					if(exists)
						updateFileContent(b, off, len);
				}
			};
		}
		protected void updateFileContent(byte[] bytes, int byteOffset, int len) throws IOException {
			writeFileContent(this, bytes, byteOffset, len, this.dataLength);
			this.dataLength += len;
		}
		protected int obtainFileContent(byte[] bytes, int byteOffset, int len, int contentOffset) throws IOException {
			return readFileContent(this, bytes, byteOffset, len, contentOffset);
		}
		/**
		 * @see simple.io.resource.Stash#isDirectory()
		 */
		public boolean isDirectory() {
			return false;
		}
		/**
		 * @see simple.io.resource.Stash#isFile()
		 */
		public boolean isFile() {
			return exists;
		}
		/**
		 * @see simple.io.resource.Stash#isOpen()
		 */
		public boolean isOpen() {
			return exists;
		}

		public Compression getCompression() {
			return compression;
		}

		public void setLength(long length) {
			lock.lock();
			try {
				if(!ConvertUtils.areEqual(this.fileLength, length))
					updatePending = true;
				this.fileLength = length;
			} finally {
				lock.unlock();
			}
		}
	}

	protected DbStashImpl createFile(String fileId, String fileName, String dataSourceName, Compression compression) throws IOException {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("fileId", fileId);
		params.put("fileName", fileName);
		String contentType = FileTypeMap.getDefaultFileTypeMap().getContentType(fileName);
		params.put("contentType", contentType);
		params.put("compression", compression);
		if(log.isDebugEnabled())
			log.debug("Creating resource " + fileId + " in " + dataSourceName);
		try {
			dataLayer.executeCall(dataSourceName, getInsertCallId(), params, params, true, null);
		} catch(SQLException e) {
			throw new IOException(e);
		} catch(DataLayerException e) {
			throw new IOException(e);
		}
		long loid;
		try {
			loid = ConvertUtils.getLong(params.get("fileContentId"));
		} catch(ConvertException e) {
			throw new IOException(e);
		}
		if(log.isDebugEnabled())
			log.debug("Successfully created resource " + fileId + " in " + dataSourceName);
		return new DbStashImpl(fileId, fileName, contentType, 0, dataSourceName, loid, false, compression);
	}

	/**
	 * @return
	 */
	protected String generateFileId() {
		return FILE_ID_PREFIX + Long.toHexString(fileIDGenerator.getAndIncrement()).toUpperCase();
	}

	protected DbStashImpl selectFile(String fileId, boolean readOnly, String dataSourceName) throws IOException {
		String callId = getSelectCallId();
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("fileId", fileId);
		try {
			dataLayer.executeCall(dataSourceName, callId, params, params, true, null);
		} catch(SQLException e) {
			throw new IOException(e);
		} catch(DataLayerException e) {
			throw new IOException(e);
		}
		String fileName;
		String contentType;
		long fileContentId;
		Compression fileCompression;
		long fileLength;
		try {
			fileName = ConvertUtils.getString(params.get("fileName"), false);
			if(fileName == null) // doesn't exist
				return null;
			contentType = ConvertUtils.getString(params.get("contentType"), false);
			fileContentId = ConvertUtils.getLong(params.get("fileContentId"));
			fileCompression = ConvertUtils.convert(Compression.class, params.get("compression"));
			fileLength = ConvertUtils.getLong(params.get("fileLength"));
		} catch(ConvertException e) {
			throw new IOException(e);
		}
		return new DbStashImpl(fileId, fileName, contentType, fileLength, dataSourceName, fileContentId, readOnly, fileCompression);
	}

	//This method is Postgresql specific
	protected void writeFileContent(DbStashImpl stash, byte[] bytes, int byteOffset, int length, long contentOffset) throws IOException {
		if(contentOffset > Integer.MAX_VALUE)
			throw new IOException("File size is too big. Maximum is " + Integer.MAX_VALUE);
		try {
			Connection conn = dataLayer.getConnection(stash.dataSourceName, false);
			try {
				PGConnection pgConn = (PGConnection)DBUnwrap.getRealConnection(conn);
				LargeObjectManager lom = pgConn.getLargeObjectAPI();
				LargeObject lo = lom.open(stash.loid, LargeObjectManager.WRITE);
				try {
					try {
						if(contentOffset > 0)
							lo.seek((int)contentOffset);
						lo.write(bytes, byteOffset, length);
					} finally {
						lo.close(); //NOTE: Order is important. lo.close() must be before conn.commit() as commit invalidates the lo
					}
					if(log.isDebugEnabled())
						log.debug("Wrote " + length + " bytes to database large object at offset " + contentOffset);
					Map<String,Object> params = new HashMap<String,Object>();
					params.put("fileId", stash.fileId);
					params.put("fileName", stash.filePath);
					params.put("contentType", stash.contentType);
					params.put("fileLength", stash.fileLength);
					dataLayer.executeCall(conn, getUpdateCallId(), params);	
					conn.commit();
					stash.updatePending = false;
				} catch(DataLayerException e) {
					DbUtils.rollbackSafely(conn);
					throw new IOException(e);
				} catch(SQLException e) {
					DbUtils.rollbackSafely(conn);
					throw new IOException(e);
				}
			} finally {
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException e) {
			throw new IOException(e);
		} catch(DataSourceNotFoundException e) {
			throw new IOException(e);
		}
	}

	//This method is Postgresql specific
	protected int readFileContent(DbStashImpl stash, byte[] bytes, int byteOffset, int length, long contentOffset) throws IOException {
		int read;
		try {
			Connection conn = dataLayer.getConnection(stash.dataSourceName, false);
			try {
				PGConnection pgConn = (PGConnection)DBUnwrap.getRealConnection(conn);
				LargeObjectManager lom = pgConn.getLargeObjectAPI();
				LargeObject lo = lom.open(stash.loid, LargeObjectManager.READ);
				try {
					try {
						if(contentOffset > Integer.MAX_VALUE)
							throw new IOException("Offset " + contentOffset + " exceeds the max " + Integer.MAX_VALUE);
						if(contentOffset > 0)
							lo.seek((int)contentOffset);
						read = lo.read(bytes, byteOffset, length);
						if(read == 0)
							throw new SQLException("Read " + read + " bytes from Large Object " + stash.loid + " when trying to read " + length + " at position " + contentOffset);
						else if(log.isDebugEnabled())
							log.debug("Read " + read + " bytes from Large Object " + stash.loid + " when trying to read " + length + " at position " + contentOffset);
					} finally {
						try {
							lo.close();
						} catch(SQLException e) {
							log.warn("Could not close Large Object " + stash.loid, e);
						}
					}
					conn.commit();
				} catch(SQLException e) {
					DbUtils.rollbackSafely(conn);
					throw new IOException(e);
				}
			} finally {
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException e) {
			throw new IOException(e);
		} catch(DataSourceNotFoundException e) {
			throw new IOException(e);
		}
		return read;
	}
	/**
	 * @param fileId
	 * @param fileName
	 * @param contentType
	 * @throws DataLayerException
	 * @throws SQLException
	 */
	protected void updateFile(String dataSourceName, String fileId, String fileName, String contentType, long length) throws SQLException, DataLayerException {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("fileId", fileId);
		params.put("fileName", fileName);
		params.put("contentType", contentType);
		params.put("fileLength", length);
		dataLayer.executeCall(dataSourceName, getUpdateCallId(), params, null, true, null);		
	}

	/**
	 * @param fileId
	 * @throws DataLayerException
	 * @throws SQLException
	 */
	protected void deleteFile(String dataSourceName, String fileId) throws SQLException, DataLayerException {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("fileId", fileId);
		dataLayer.executeCall(dataSourceName, getDeleteCallId(), params, null, true, null);
	}

	/**
	 * @see simple.io.resource.ResourceFolder#isAvailable(simple.io.resource.ResourceMode)
	 */
	public boolean isAvailable(ResourceMode mode) {
		return true;  // all modes supported
	}

	/**
	 * @see simple.io.resource.ResourceFolder#release()
	 */
	public void release() {
	}

	public String getInsertCallId() {
		return insertCallId;
	}

	public void setInsertCallId(String insertCallId) {
		this.insertCallId = insertCallId;
	}

	public String getSelectCallId() {
		return selectCallId;
	}

	public void setSelectCallId(String selectCallId) {
		this.selectCallId = selectCallId;
	}

	public String getUpdateCallId() {
		return updateCallId;
	}

	public void setUpdateCallId(String updateCallId) {
		this.updateCallId = updateCallId;
	}

	public String getDeleteCallId() {
		return deleteCallId;
	}

	public void setDeleteCallId(String deleteCallId) {
		this.deleteCallId = deleteCallId;
	}

	public String getSelectForUpdateCallId() {
		return selectForUpdateCallId;
	}

	public void setSelectForUpdateCallId(String selectForUpdateCallId) {
		this.selectForUpdateCallId = selectForUpdateCallId;
	}

	public DataLayer getDataLayer() {
		return dataLayer;
	}

	public void setDataLayer(DataLayer dataLayer) {
		if(dataLayer == null)
			this.dataLayer = DataLayerMgr.getGlobalDataLayer();
		else
			this.dataLayer = dataLayer;
	}

	public int getMaxInMemoryBuffer() {
		return maxInMemoryBuffer;
	}

	public void setMaxInMemoryBuffer(int maxInMemoryBuffer) {
		this.maxInMemoryBuffer = maxInMemoryBuffer;
	}

}
