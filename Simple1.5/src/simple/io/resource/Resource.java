/**
 *
 */
package simple.io.resource;


import javax.activation.DataSource;

/** Represents one "file" in a file repository system. (This "file" may actually be replicated in several different places based on
 * the implementation of the {@link ResourceFolder} and its settings that created this object.)
 * This interface extends {@link javax.activation.DataSource} to enable the use of it in many contexts.
 *
 * @author Brian S. Krug
 *
 */
public interface Resource extends DataSource {
	/** Whether this resource can be read via {@link javax.activation.DataSource#getInputStream()}.
	 * @return <code>true</code>, if this resource is readable
	 */
	public boolean isReadable() ;
	/** Whether this resource can be updated via {@link javax.activation.DataSource#getOutputStream()}.
	 * @return <code>true</code>, if this resource is writable
	 */
	public boolean isWritable() ;
	/** Whether the underlying "file" exists.
	 * @return <code>true</code>, if this resource exists in the repository system
	 */
	public boolean exists();
	/** The path of the underlying "file".
	 * @return The path of the resource in the repository system
	 */
	public String getPath() ;
	/** The number of bytes of the underlying "file". Reading from {@link javax.activation.DataSource#getInputStream()}
	 * should return this many bytes in total.
	 * @return The size in bytes of the resource in the repository system
	 */
	public long getLength();
	/** Updates the contentType of the resource as obtained by {@link javax.activation.DataSource#getContentType()}
	 * @param contentType the new content type of the resource
	 */
	public void setContentType(String contentType);
	/** The unique identifier of this resource in the repository system. Two resources with the same key will have the same content
	 * @return the key of the resource
	 */
	public String getKey();
	/** Updates the name of the resource as obtained by {@link javax.activation.DataSource#getName()}
	 * @param fileName the new name of the resource
	 */
	public void setName(String fileName);
	/** Releases any system resources used by this object
	 *
	 */
	public void release();

	/** Attempts to deletes this file from from the repository system.
	 *
	 */
	public boolean delete();
}