package simple.io.resource.httpclient;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.httpclient.methods.multipart.PartSource;

import simple.io.resource.Resource;


public class ResourcePartSource implements PartSource {
	protected final Resource resource;
	public ResourcePartSource(Resource resource) {
		this.resource = resource;
	}
	public InputStream createInputStream() throws IOException {
		return resource.getInputStream();
	}

	public String getFileName() {
		return resource.getName();
	}
	public long getLength() {
		return resource.getLength();
	}
}
