package simple.io.resource;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Random;

import simple.io.resource.ResourceUtils.ResourceKey;

/**
 * This class represents a local file resource, it can be used to read and write
 * to a local file.
 *
 * @author yhe, bkrug
 *
 */
public class FileResourceFolder implements ResourceFolder {
	protected static final Random random = new Random();
	protected String path;
	protected File baseDir;

	public FileResourceFolder(String path) {
		super();
		setPath(path);
	}

	public Resource getResource(String key, ResourceMode mode) throws IOException {
		if((key=key.trim()).length() == 0)
			throw new IOException("Blank key is not valid");
		if(mode == ResourceMode.CREATE) {
			key = ResourceUtils.convertToKey(key);
		}
		ResourceKey resKey;
		try {
			resKey = ResourceUtils.parseKey(key, null);
		} catch(ParseException e) {
			IOException ioe = new IOException();
			ioe.initCause(e);
			throw ioe;
		}
		File file;
		switch(mode) {
			case CREATE:
				file = new File(baseDir, resKey.getPath());
				file.getParentFile().mkdirs();
				if(!file.createNewFile()) {
					do {
						resKey.nextPath();
						file = new File(baseDir, resKey.getPath());
					} while(!file.createNewFile());
				}
				break;
			case READ: case UPDATE:
				file = new File(baseDir, resKey.getPath());
				break;
			default:
				throw new IOException("Invalid Mode " + mode);
		}
		return new FileResource(file, resKey.getPath(), resKey.getFileName());
	}
	public boolean isAvailable(ResourceMode mode) {
		File f = baseDir;
		if(f == null)
			return false;
		f.mkdirs();
		return f.exists();
	}
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
		this.baseDir = new File(path);
	}
	public void release() {
		baseDir = null;
	}
}
