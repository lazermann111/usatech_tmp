package simple.io.resource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ByteArrayResource implements Resource {
	protected final byte[] bytes;
	protected String fileName;
	protected String contentType;
	protected String key;

	public ByteArrayResource(byte[] bytes, String key, String fileName){
		this.bytes = bytes;
		this.key = key;
		this.fileName = fileName;
	}

	public boolean exists() {
		return true;
	}

	public String getPath() {
		return "<in memory byte array>";
	}

	public boolean isReadable() {
		return true;
	}
	public boolean isWritable() {
		return false;
	}
	public String getContentType() {
		return contentType;
	}

	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(bytes);
	}

	public void setName(String fileName) {
		this.fileName = fileName;
	}

	public String getName() {
		return fileName;
	}

	public OutputStream getOutputStream() throws IOException {
		throw new IOException("Write is not supported");
	}

	public long getLength() {
		return bytes.length;
	}
	@Override
	public String toString() {
		return bytes.length + " bytes";
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getKey(){
		return key;
	}
	public void release(){
		//do nothing;
	}
	public boolean delete() {
		return true;
	}
}
