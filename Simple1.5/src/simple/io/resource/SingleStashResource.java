package simple.io.resource;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import simple.io.Compression;
import simple.io.Log;
import simple.util.MapBackedSet;

public class SingleStashResource implements Resource {
	private static final Log log = Log.getLog();
	protected final Stash stash;
	protected String fileName;
	protected final String key;
	protected Compression compression;
	protected final Set<ResourceOutputStream> outputs = new MapBackedSet<ResourceOutputStream>(new ConcurrentHashMap<ResourceOutputStream,Object>());
	protected class ResourceOutputStream extends FilterOutputStream {
		protected long length = 0;
		protected ResourceOutputStream(OutputStream out) {
			super(out);
			outputs.add(this);
			if(stash.getLength() > 0)
				log.warn("Resource '" + getKey() + "' formerly of length " + stash.getLength() + " is being overwritten", new IOException());
		}
		protected void closeDelegate() throws IOException {
			out.close();
		}
		@Override
		public void close() throws IOException {
			flush();
			outputs.remove(this);
			out.close();
		}
		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			out.write(b, off, len);
			length += len;
		}
		@Override
		public void write(int b) throws IOException {
			super.write(b);
			length++;
		}
		@Override
		public void flush() throws IOException {
			super.flush();
			stash.setLength(length);
		}
	}

	public SingleStashResource(Stash stash, String key, String fileName) {
		this(stash, key, fileName, null);
	}

	public SingleStashResource(Stash stash, String key, String fileName, Compression compression) {
		this.stash = stash;
		this.key = key;
		this.fileName = fileName;
		this.compression = compression;
	}

	public boolean exists() {
		return stash.isFile() || stash.isDirectory();
	}

	public String getPath() {
		return stash.getAbsolutePath();
	}

	public boolean isReadable() {
		return stash.canRead();
	}
	public boolean isWritable() {
		return stash.canWrite();
	}
	public String getContentType() {
		return stash.getContentType();
	}

	public InputStream getInputStream() throws IOException {
		InputStream in = stash.getInputStream();
		Compression c = compression;
		if(c != null)
			in = c.getDecompressingInputStream(in);
		return in;
	}

	public void setName(String fileName) {
		this.fileName = fileName;
	}

	public String getName() {
		if(fileName == null) {
			return stash.getFilename();
		} else {
			return fileName;
		}
	}

	public OutputStream getOutputStream() throws IOException {
		OutputStream out = stash.getOutputStream();
		Compression c = compression;
		if(c != null)
			out = c.getCompressingOutputStream(out);
		return new ResourceOutputStream(out);
	}

	public long getLength() {
		return stash.getLength();
	}

	@Override
	public String toString() {
		return getKey() + '=' + getName();
	}

	public void setContentType(String contentType) {
		stash.setContentType(contentType);
	}

	public String getKey() {
		return key;
	}

	protected void closeAbandonedOutputs() {
		for(Iterator<ResourceOutputStream> iter = outputs.iterator(); iter.hasNext(); ) {
			ResourceOutputStream out = iter.next();
			log.debug("Closing abandoned output stream (" + out + ") on " + this);
			iter.remove();
			try {
				out.closeDelegate();
			} catch(IOException e) {
				log.info("Could not close output stream of stash", e);
			}
		}
	}

	public void release() {
		closeAbandonedOutputs();
		stash.close();
	}
	public boolean delete() {
		return stash.delete();
	}

	protected Compression getCompression() {
		return compression;
	}

	protected void setCompression(Compression compression) {
		this.compression = compression;
	}
}
