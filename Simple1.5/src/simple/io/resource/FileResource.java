package simple.io.resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileResource implements Resource {
	protected final File file;
	protected String fileName;
	protected String contentType;
	protected String key;

	public FileResource(String path) {
		this.file = new File(path);
	}

	public FileResource(File file, String key, String fileName){
		this.file = file;
		this.key=key;
		this.fileName=fileName;
	}

	public boolean exists() {
		return file.exists();
	}

	public String getPath() {
		return file.getAbsolutePath();
	}

	public boolean isReadable() {
		return file.canRead();
	}
	public boolean isWritable() {
		return file.canWrite();
	}
	public String getContentType() {
		return contentType;
	}

	public InputStream getInputStream() throws IOException {
		return new FileInputStream(file);
	}

	public void setName(String fileName) {
		this.fileName = fileName;
	}

	public String getName() {
		if(fileName==null){
			return file.getName();
		}else{
			return fileName;
		}
	}

	public OutputStream getOutputStream() throws IOException {
		return new FileOutputStream(file);
	}

	public long getLength() {
		return file.length();
	}
	@Override
	public String toString() {
		return "file:" + getPath();
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getKey(){
		return key;
	}
	public void release(){
		//do nothing;
	}
	public boolean delete() {
		return file.delete();
	}
}
