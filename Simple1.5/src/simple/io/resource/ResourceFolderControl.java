package simple.io.resource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.apache.commons.configuration.Configuration;

import simple.app.BaseWithConfig;
import simple.app.BasicCommandArgument;
import simple.app.Command;
import simple.app.CommandArgument;
import simple.app.CommandLineInterfaceWithConfig;
import simple.app.ServiceException;
import simple.app.AbstractCommandManager.AbstractCommand;
import simple.io.IOUtils;

public class ResourceFolderControl extends CommandLineInterfaceWithConfig<ResourceFolder> {
	protected static abstract class ResourceFolderControlCommand extends AbstractCommand<ResourceFolder> {
		public ResourceFolderControlCommand(String key, String description, CommandArgument[] commandArguments) {
			super(key, description, commandArguments);
		}
	}
	protected static class RetrieveCommand extends ResourceFolderControlCommand {
		public RetrieveCommand() {
			super("retrieve", "Retrieves a resource from the ResourceFolder and writes it to a file or standard out", new CommandArgument[]{
					new BasicCommandArgument("resourceKey", String.class, "The unique resource key of the resource to retrieve", false),
					new BasicCommandArgument("outputFile", String.class, "The file to which the resource is copied; if not provided standard out is used", true),				
			});
		}
		
		public boolean executeCommand(ResourceFolder context, PrintWriter out, Object[] arguments) {
			OutputStream writeTo;
			String fileName;
			String writeToDesc;
			boolean close;
			try {
				if(arguments.length < 2 || arguments[1] == null || (fileName=((String)arguments[1]).trim()).length() == 0 || fileName.equals("-")) {
					writeTo = System.out;
					close = false;
					writeToDesc = "standard out";
				} else {
					File file = new File(fileName);
					writeTo = new FileOutputStream(file);
					close = true;
					writeToDesc = file.getCanonicalPath();
				}
				String resourceKey = ((String)arguments[0]).trim();
				Resource resource = context.getResource(resourceKey, ResourceMode.READ);
				long n = IOUtils.copy(resource.getInputStream(), writeTo);
				out.println("Wrote " + n + " bytes from resource '" + resourceKey + "' to " + writeToDesc);
				if(close)
					out.close();
			} catch(IOException e) {
				out.println("Could not copy resource");
				e.printStackTrace(out);
			}
			return true;
		}
	}
	
	public static void main(String[] args) {
		new ResourceFolderControl().run(args);
	}
	/**
	 * @see simple.app.CommandLineInterfaceWithConfig#createContext(org.apache.commons.configuration.Configuration, java.io.BufferedReader, java.io.PrintStream)
	 */
	@Override
	protected ResourceFolder createContext(Configuration config, BufferedReader in, PrintStream out) {
		try {
			configureDataSourceFactory(config, null);
		} catch(ServiceException e) {
			finishAndExit("Could not configure data source factory", 500, e);
			return null;
		}
		try {
			configureDataLayer(config, 0);
		} catch(ServiceException e) {
			finishAndExit("Could not configure data layer", 501, e);
			return null;
		}
		try {
			return BaseWithConfig.configure(ResourceFolder.class, config.subset(ResourceFolder.class.getName()));
		} catch(ServiceException e) {
			finishAndExit("Could not configure resource folder", 600, e);
			return null;
		}
	}
	/**
	 * @see simple.app.CommandLineInterfaceWithConfig#getCommands(org.apache.commons.configuration.Configuration)
	 */
	@Override
	protected Command<ResourceFolder>[] getCommands(Configuration config) {
		return new ResourceFolderControlCommand[] {
			new RetrieveCommand()
		};
	}
}
