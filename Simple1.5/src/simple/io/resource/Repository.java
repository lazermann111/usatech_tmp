/**
 *
 */
package simple.io.resource;

import simple.bean.ConvertUtils;

public class Repository {
	protected String host;
	protected String path;

	public Repository(String host, String path) {
		this.host = host;
		this.path = path;
	}
	public Repository() {
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(host != null)
			sb.append(host);
		sb.append(':');
		if(path != null)
			sb.append(path);
		return sb.toString();
	}
	@Override
	public int hashCode() {
		return (host == null ? 0 : host.hashCode()) ^ (path == null ? 0 : path.hashCode());
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Repository) {
			Repository repo = (Repository)obj;
			return ConvertUtils.areEqual(host, repo.host) && ConvertUtils.areEqual(path, repo.path);
		} else
			return false;
	}
}