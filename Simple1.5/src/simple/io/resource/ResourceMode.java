/**
 *
 */
package simple.io.resource;

/** The mode of the Resource. CREATE will create a new unique resource, UPDATE will get an existing resource as writable,
 * and READ will get an existing resource as readable.
 * @author Brian S. Krug
 *
 */
public enum ResourceMode {CREATE, UPDATE, READ, DELETE }