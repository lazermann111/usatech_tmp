/**
 *
 */
package simple.io.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import simple.io.Compression;
import simple.io.Log;
import simple.text.StringUtils;
import simple.util.ArrayListIterator;
import simple.util.concurrent.CustomThreadFactory;

/**
 * @author Brian S. Krug
 *
 */
public class ReplicatedDbResourceFolder extends AbstractDbResourceFolder {
	private static final Log log = Log.getLog();
	protected int replicas = 2;
	protected String[] dataSourceNames;
	protected final AtomicInteger roundRobin = new AtomicInteger();
	protected final ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 5, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(),
			new CustomThreadFactory("DbResourceWriterThread-", true, Thread.NORM_PRIORITY));
	
	protected static class DbResourceKey {
		protected final String[] dataSourceNames;
		protected final String keyId;
		public DbResourceKey(String key) {
			key = key.trim();
			if(key.charAt(0) == '[') {
				int pos = key.indexOf(']', 1);
				if(pos < 0) {
					keyId = key;
					dataSourceNames = null;
				} else {
					keyId = key.substring(pos+1);
					dataSourceNames = StringUtils.split(key.substring(1, pos), StringUtils.STANDARD_DELIMS, false);
				}
			} else {
				keyId = key;
				dataSourceNames = null;
			}
		}
		public String[] getDataSourceNames() {
			return dataSourceNames;
		}
		public String getKeyId() {
			return keyId;
		}
	}
	public ReplicatedDbResourceFolder() {
	}
	/**
	 * @see simple.io.resource.ResourceFolder#getResource(java.lang.String, simple.io.resource.ResourceMode)
	 */
	public Resource getResource(String key, ResourceMode mode) throws IOException {
		switch(mode) {
			case CREATE: {
				ListIterator<String> dsnIterator = getDataSourceNameIterator(null);
				int required = getReplicas();
				DbStashImpl[] stashes = new DbStashImpl[required];
				int count = 0;
				String fileId = generateFileId();
				IOException lastException = null;
				StringBuilder sb = new StringBuilder();
				sb.append('[');
				Compression compression = getCompression();
				boolean okay = false;
				try {
					while(count < required) {
						if(dsnIterator.hasNext()) {
							try {
								String dsn = dsnIterator.next();
								DbStashImpl stash = createFile(fileId, key, dsn, compression);
								if(stash != null) {
									if(count > 0)
										sb.append(',');
									sb.append(dsn);
									stashes[count++] = stash;
								}
							} catch(IOException e) {
								lastException = e;
							}
						} else if(lastException != null)
							throw new IOException("Resource '" + fileId + "' for '" + key + "' only created in " + count + " data sources, but " + required + " are required", lastException);
						else
							throw new IOException("Resource '" + fileId + "' for '" + key + "' only created in " + count + " data sources, but " + required + " are required");
					}
					okay = true;
				} finally {
					if(!okay) {
						for(int i = 0; i < count; i++) {
							if(stashes[i] != null)
								stashes[i].close();
						}
					}
				}
				sb.append(']').append(fileId);
				if(required > 1) {
					boolean parallel = isParallelOutputStream();
					return new ReplicatedStashResource(stashes, executor, sb.toString(), null, getOutputBufferSize(), parallel, stashes[0].getCompression());
				} else {
					return new SingleStashResource(stashes[0], sb.toString(), null, stashes[0].getCompression());
				}
			}
			case READ: {
				DbResourceKey rk = new DbResourceKey(key);
				ListIterator<String> dsnIterator = getDataSourceNameIterator(rk.getDataSourceNames());
				IOException lastException = null;
				while(dsnIterator.hasNext()) {
					try {
						DbStashImpl stash = selectFile(rk.getKeyId(), true, dsnIterator.next());
						if(stash != null)
							return new SingleStashResource(stash, key, null, stash.getCompression());
					} catch(IOException e) {
						lastException = e;
					}
				}
				if(lastException != null)
					throw new IOException("Resource '" + key + "' not retrieved from any data source", lastException);
				else
					throw new IOException("Resource '" + key + "' not found in any data source");
			}
			case UPDATE: {
				DbResourceKey rk = new DbResourceKey(key);
				String[] dsns = rk.getDataSourceNames();
				DbStashImpl[] stashes = new DbStashImpl[dsns.length];
				boolean okay = false;
				try {
					for(int i = 0; i < dsns.length; i++) {
						DbStashImpl stash = selectFile(rk.getKeyId(), false, dsns[i]);
						if(stash != null) {
							//Ensure that compression is the same on all
							if(i > 0 && stash.getCompression() != stashes[0].getCompression())
								throw new IOException("Compression on '" + key + "' in '" + dsns[i] + "' is not the same as in the other databases");
							stashes[i] = stash;
						} else 
							throw new IOException("Resource '" + rk.getKeyId() + "' for '" + key + "' is not found in '" + dsns[i] + "' data source");
					}
					okay = true;
				} finally {
					if(!okay) {
						for(int i = 0; i < stashes.length; i++) {
							if(stashes[i] != null)
								stashes[i].close();
						}
					}
				}
				if(stashes.length > 1) {
					return new ReplicatedStashResource(stashes, executor, key, null, getOutputBufferSize(), isParallelOutputStream(), stashes[0].getCompression());
				} else {
					return new SingleStashResource(stashes[0], key, null, stashes[0].getCompression());
				}
			}
			case DELETE: {
				DbResourceKey rk = new DbResourceKey(key);
				String[] dsns = rk.getDataSourceNames();
				List<DbStashImpl> stashes = new ArrayList<DbStashImpl>(dsns.length);
				boolean okay = false;
				try {
					for(int i = 0; i < dsns.length; i++) {
						DbStashImpl stash = selectFile(rk.getKeyId(), false, dsns[i]);
						if(stash != null) {
							//Ensure that compression is the same on all
							if(!stashes.isEmpty() && stash.getCompression() != stashes.get(0).getCompression())
								throw new IOException("Compression on '" + key + "' in '" + dsns[i] + "' is not the same as in the other databases");
							stashes.add(stash);
						} else 
							log.info("Resource '" + rk.getKeyId() + "' for '" + key + "' is not found in '" + dsns[i] + "' data source; ignoring");
					}
					okay = true;
				} finally {
					if(!okay) {
						for(DbStashImpl stash : stashes) {
							if(stash != null)
								stash.close();
						}
					}
				}
				switch(stashes.size()) {
					case 1:
						return new SingleStashResource(stashes.get(0), key, null, stashes.get(0).getCompression());
					case 0:
						return new SingleStashResource(new DeletedDbStash(key), key, null, null);
					default:
						return new ReplicatedStashResource(stashes.toArray(new DbStashImpl[stashes.size()]), executor, key, null, getOutputBufferSize(), isParallelOutputStream(), stashes.get(0).getCompression());
				}
			}
		}
		return null;
	}

	protected ListIterator<String> getDataSourceNameIterator(String[] firstDataSourceNames) {
		String[] dataSourceNames = this.dataSourceNames; //protect against mid-stream changes
		int start = roundRobin.getAndIncrement();
		if(start < 0)
			start = (dataSourceNames.length - 1) + ((start + 1) % dataSourceNames.length);
    	else
    		start = start % dataSourceNames.length;
		if(firstDataSourceNames != null && firstDataSourceNames.length > 0) {
			Set<String> tmp = new LinkedHashSet<String>();
			for(String dsn : firstDataSourceNames) {
				tmp.add(dsn);

			}
			int end = start + dataSourceNames.length;
			for(int i = start; i < end; i++)
				tmp.add(dataSourceNames[i%dataSourceNames.length]);

			dataSourceNames = tmp.toArray(new String[tmp.size()]);
			start = 0;
		}
		return new ArrayListIterator<String>(dataSourceNames, start);
	}
	public int getReplicas() {
		return replicas;
	}

	public void setReplicas(int replicas) {
		this.replicas = replicas;
	}

	public String[] getDataSourceNames() {
		return dataSourceNames.clone();
	}

	public void setDataSourceNames(String[] dataSourceNames) {
		if(dataSourceNames != null) {
			List<String> tmp = new ArrayList<String>(dataSourceNames.length);
			for(String dsn : dataSourceNames)
				if(dsn != null && (dsn=dsn.trim()).length() > 0)
					tmp.add(dsn);
			dataSourceNames = tmp.toArray(new String[dataSourceNames.length]);
		}

		this.dataSourceNames = dataSourceNames;
	}

	public int getConcurrency() {
		return executor.getMaximumPoolSize();
	}

	public void setConcurrency(int concurrency) {
		executor.setMaximumPoolSize(concurrency);
	}

	public int getMaxIdle() {
		return executor.getCorePoolSize();
	}

	public void setMaxIdle(int maxIdle) {
		executor.setCorePoolSize(maxIdle);
	}
	public long getKeepAliveTime() {
		return executor.getKeepAliveTime(TimeUnit.MILLISECONDS);
	}
	public void setKeepAliveTime(long time) {
		executor.setKeepAliveTime(time, TimeUnit.MILLISECONDS);
	}

}
