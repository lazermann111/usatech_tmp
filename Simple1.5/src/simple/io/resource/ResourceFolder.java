package simple.io.resource;

/** Represents a file repository to which files may be written and from which files may be read
 * @author Brian S. Krug
 *
 */
public interface ResourceFolder {
	/** Retrieves a {@link Resource} from the file repository with the given key and possibly sets the resource's
	 * file name to the given name. Typical usage is:
	 * <pre>
	 * ResourceFolder folder = ...;
	 * Resource res = folder.getResource("path/filename.ext", ResourceMode.CREATE);
	 * OutputStream out = res.getOutputStream();
	 * &#47;&#47; write to the output stream
	 * out.flush();
	 * String key = res.getKey();
	 * res.release();
	 * Resource res2 = folder.getResource(key, ResourceMode.READ);
	 * InputStream in = res2.getInputStream();
	 * &#47;&#47; read from the input stream
	 * res2.release();
	 * </pre>
	 * @param key The unique identifier for the file. For CREATE mode, this may be a file-path-like string. The ResourceFolder will
	 * sanitize and ensure uniqueness of the given key and the Resource instance returned will have the transformed version of the key
	 * in its {@link Resource#getKey()} method. The original key will determine the name of the Resource (in {@link Resource#getName()}).
	 * For READ and UPDATE modes, this key must be formatted as:
	 * <pre> ( '[' ( host-name ':' host-path ',' | ';' ) * host-name ':' host-path ']' ) ? file-path ( '(' ordinal ')' ) ? ( '.' file-extension ) ?</pre>
	 * where
	 * <code>host-name</code> is an IP address or hostname, <br/>
	 * <code>host-path</code> is a series of 0 to n URLEncoded directory names separated by '/' or '\',<br/>
	 * <code>file-path</code> is a series of 0 to n URLEncoded directory names followed by 1 URLEncoded file name,<br/>
	 * <code>ordinal</code> is a series of 1 to n digits, and <br/>
	 * <code>file-extension</code> is a series of 1 to n alpha-numeric characters, hyphens or underscores<br/>
	 *
	 * Keys can be tested for validity with {@link ResourceUtils#isValidKey(String)}.
	 * @param mode The mode in which to open the Resource
	 * @return a Resource as specified
	 * @throws java.io.IOException
	 */
	public Resource getResource(String key, ResourceMode mode) throws java.io.IOException;

	/** Releases any system resources used by this object, such as connections to other systems
	 *
	 */
	public void release() ;

	/** Determines if this ResourceFolder is available for the given mode and
	 *  returns the result
	 * @param mode
	 * @return
	 */
	public boolean isAvailable(ResourceMode mode) ;
}
