/*
 * IOUtils.java
 *
 * Created on January 7, 2002, 2:16 PM
 */

package simple.io;

/**
 * Utility methods for io functions
 *
 * @author  Brian S. Krug
 * @version
 */
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Writer;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.text.StringUtils;
import simple.util.concurrent.Cache;

public class IOUtils {
	private static final Log log = Log.getLog();
    private static final int BUFFER_SIZE = 1024;
    protected static final Pattern uriAddPattern = Pattern.compile("([^?#]+)(?:(\\?)([^#]*))?(?:(\\#.*))?"); // groups: 1=path; 2=? or empty; 3=query string 4=# or empty
	protected static final Pattern uriRemovePattern = Pattern.compile("([^?#]+)\\?((?:[^&#]*&)*)t=\\d+((?:&[^&#]*)*)(\\#.*)?"); // groups: 1=path; 2=query string 1; 3=query string 2; 4=# and fragment
	protected static final String[] executableSuffixes = gatherExecutableSuffixes();
	protected static final char[] DIRECTORY_SEPARATORS = "/\\".toCharArray();
	public static final byte[] EMPTY_BYTES = new byte[0];

    private IOUtils() {
    }

	public static String getFileName(String filePath) {
		return StringUtils.substringAfterLast(filePath, DIRECTORY_SEPARATORS);
	}

	public static String getDirectory(String filePath) {
		return StringUtils.substringBeforeLast(filePath, DIRECTORY_SEPARATORS);
	}

	protected static String[] gatherExecutableSuffixes() {
		String os = System.getProperty("os.name");
		if(os == null)
			return null;
		if(os.startsWith("Window")) {
			String pathExt = new ProcessBuilder().environment().get("PATHEXT");
			if(pathExt == null)
				return new String[] { ".exe", ".bat" };
			return pathExt.split(";");
		}
		return new String[] { ".sh" };
	}

	public static File findExecutableFile(File baseFile) throws IOException {
		if(baseFile.exists() && baseFile.canExecute())
			return baseFile.getCanonicalFile();
		if(executableSuffixes == null) {
			if(baseFile.exists())
				throw new FileNotFoundException("File '" + baseFile.getAbsolutePath() + "' is not executable");
			throw new FileNotFoundException("Could not find executable file '" + baseFile.getAbsolutePath() + "'");
		}
		for(String ext : executableSuffixes) {
			File file = new File(baseFile.getAbsolutePath().replace(".", "") + ext);
			if(file.exists() /*&& file.canExecute()   todo uncomment*/)
				return file.getCanonicalFile();
		}
		throw new FileNotFoundException("Could not find executable file '" + baseFile.getAbsolutePath() + "' with extensions " + Arrays.toString(executableSuffixes));
	}

    public static String addLastModifiedToFile(String originalPath, String filePath, Cache<String, ConfigSource, RuntimeException> fileCache) {
		long lastModified = 0;
		if (isFilePermitted(filePath)) {
			final File file = new File(filePath);
			if (fileCache == null)
				lastModified = file.lastModified();
			else {
				ConfigSource src = fileCache.getOrCreate(filePath);
				if (src == null && log.isInfoEnabled())
					log.info("Could not find file '" + safeCanonicalPath(file) + "' for path '" + originalPath
							+ "' to add last modified");
				lastModified = (src == null ? 0 : src.lastModified());
			}
		}
		String paramName = "t";
		Matcher matcher = uriAddPattern.matcher(originalPath);
		if(matcher.matches()) {
			StringBuilder sb = new StringBuilder();
			sb.append(matcher.group(1));
			if(matcher.group(2) == null) {
				sb.append('?').append(paramName).append("=").append(lastModified);
			} else {
				if(matcher.group(3) != null && matcher.group(3).length() > 0)
					sb.append('?').append(matcher.group(3)).append('&');
				sb.append(paramName).append("=").append(lastModified);
			}
			if(matcher.group(4) != null)
				sb.append(matcher.group(4));
			return sb.toString();
		} else
			return originalPath;
    }

	private static boolean isFilePermitted(String filePath) {
		List<String> permittedExtensions = Arrays.asList(".jsp", ".css", ".js");
		for (String extension : permittedExtensions) {
			if (filePath.endsWith(extension)) {
				return true;
			}
		}
		return false;
	}

	public static String removeLastModifiedFromUri(String uri) {
		Matcher matcher = uriRemovePattern.matcher(uri);
		if(!matcher.matches())
			return uri;
		StringBuilder sb = new StringBuilder();
		sb.append(matcher.group(1));
		String group2 = matcher.group(2);
		String group3 = matcher.group(3);
		if(!StringUtils.isBlank(group2)) {
			sb.append('?').append(group2, 0, group2.length() - 1);
			if(!StringUtils.isBlank(group3))
				sb.append(group3);
		} else if(!StringUtils.isBlank(group3)) {
			sb.append('?').append(group3, 1, group3.length());
		}
		if(matcher.group(4) != null)
			sb.append(matcher.group(4));
		return sb.toString();
	}

    public static long readForLength(InputStream in) throws IOException {
    	//NOTE: It looks like several implementations return 0 from skip when at the end of input
    	// Unfortunately, different implementations handle skip() differently
    	if(in instanceof FileInputStream) {
    		return ((FileInputStream)in).getChannel().size();
    	}
        long total = 0;
        long n = Math.max(in.available(), Integer.MAX_VALUE);
        while(true) {
        	long s = in.skip(n);
        	if(s < 0)
        		return total;
        	if(s == 0) {
        		if(in.available() == 0)
        			return total;
        		n = n / 2;
        		if(n <= 0)
        			throw new IOException("Could not skip to end of InputStream to calculate its length");
        	} else {
        		total += s;
        	}
        }     
    }
    /**
     *  Reads an entire input stream and returns it as a String
     */
    public static String readFully(InputStream is) throws IOException {
        return readFully(is, new byte[BUFFER_SIZE]);
    }
    /**
     *  Reads an entire input stream and returns it as a String
     */
    public static String readFully(InputStream is, byte[] buffer) throws IOException {
		BufferedInputStream buf = (is instanceof BufferedInputStream ? (BufferedInputStream) is : new BufferedInputStream(is));
        int read = 0;
        int count = 0;
        while((read = buf.read(buffer, count, buffer.length - count)) > -1) {
            count += read;
            if(count >= buffer.length * 0.8) {
                byte[] nb = new byte[buffer.length + BUFFER_SIZE];
                System.arraycopy(buffer,0,nb,0,count);
                buffer = nb;
            }
        }
        return new String(buffer, 0, count);
    }

    /**
     *  Reads an entire reader and returns it as a String
     */
	public static String readFully(Reader r) throws IOException {
		BufferedReader buf = null;
		char[] c = new char[BUFFER_SIZE];
		int read = 0;
		int count = 0;
		try {
			buf = (r instanceof BufferedReader ? (BufferedReader)r : new BufferedReader(r));
			while ((read = buf.read(c, count, c.length - count)) > -1) {
				count += read;
				if (count >= c.length * 0.8) {
					char[] nc = new char[c.length + BUFFER_SIZE];
					System.arraycopy(c, 0, nc, 0, count);
					c = nc;
				}
			}
			return new String(c, 0, count);
		} finally {
			if (buf != null)
				buf.close();
		}
	}

    /**
	 * Reads an entire reader and returns it as a String
	 */
	public static String readFully(Reader r, int maxLength) throws IOException {
		BufferedReader buf = null;
		char[] c = new char[Math.min(BUFFER_SIZE, maxLength)];
		int read = 0;
		int count = 0;
		try {
			buf = (r instanceof BufferedReader ? (BufferedReader)r : new BufferedReader(r));
			while (count < maxLength && (read = buf.read(c, count, Math.min(c.length - count, maxLength - count))) > -1) {
				count += read;
				if (count >= c.length * 0.8 && c.length < maxLength) {
					char[] nc = new char[c.length + BUFFER_SIZE];
					System.arraycopy(c, 0, nc, 0, count);
					c = nc;
				}
			}
			return new String(c, 0, count);
		} finally {
			if (buf != null)
				buf.close();
		}
	}

	/**
	 * Reads an entire reader and returns it as a String using a CharBuffer
	 */
    public static String readFullyNIO(Reader r) throws IOException {
        CharBuffer charBuffer = CharBuffer.allocate(BUFFER_SIZE);
		while(r.read(charBuffer) > -1) {
            if(!charBuffer.hasRemaining()) {
            	CharBuffer ncb = CharBuffer.allocate(charBuffer.capacity() + BUFFER_SIZE);
            	charBuffer.rewind();
                ncb.put(charBuffer);
                charBuffer = ncb;
            }
        }
        charBuffer.flip();
        return charBuffer.toString();
    }

    /**
     *  Reads an entire input stream into an outputstream in chuncks of bytes
     *  bufferSize in number
     */
    public static long readInto(InputStream in, OutputStream out, int bufferSize) throws IOException {
        if(bufferSize < 1) bufferSize = BUFFER_SIZE;
        byte[] b = new byte[bufferSize];
        long tot = 0;
        for(int len = 0; (len=in.read(b)) >= 0; tot += len) out.write(b, 0, len);
        out.flush();
        return tot;
    }

    /**
     *  Reads an entire reader into a writer in chuncks of chars
     *  bufferSize in number
     */
    public static long readInto(Reader in, Writer out, int bufferSize) throws IOException {
        if(bufferSize < 1) bufferSize = BUFFER_SIZE;
        char[] b = new char[bufferSize];
        long tot = 0;
        for(int len = 0; (len=in.read(b)) >= 0; tot += len) out.write(b, 0, len);
        out.flush();
        return tot;
    }

    /**
     *  Reads an entire reader into a writer in chuncks of chars
     *  bufferSize in number
     */
    public static int readInto(Reader in, StringBuilder out, int bufferSize) throws IOException {
        if(bufferSize < 1) bufferSize = BUFFER_SIZE;
        char[] b = new char[bufferSize];
        int tot = 0;
        for(int len = 0; (len=in.read(b)) >= 0; tot += len) out.append(b, 0, len);
        return tot;
    }

    /**
     *  Reads an entire reader into a writer in chuncks of chars
     *  bufferSize in number
     */
    public static int readInto(Reader in, StringBuffer out, int bufferSize) throws IOException {
        if(bufferSize < 1) bufferSize = BUFFER_SIZE;
        char[] b = new char[bufferSize];
        int tot = 0;
        for(int len = 0; (len=in.read(b)) >= 0; tot += len) out.append(b, 0, len);
        return tot;
    }

    /**
	 * Reads an entire input stream into an ByteBuffer
	 * bufferSize in number
	 */
	public static long readInto(InputStream in, ByteBuffer out, int bufferSize) throws IOException {
		long tot = 0;
		if(out.hasArray()) {
			byte[] b = out.array();
			int off = out.position() + out.arrayOffset();
			for(int len = 0; (len = in.read(b, off, out.remaining())) >= 0; tot += len) {
				off += len;
				out.position(out.position() + len);
				if(len == 0 && out.remaining() == 0)
					throw new BufferOverflowException();
			}
		} else {
			if(bufferSize < 1)
				bufferSize = BUFFER_SIZE;
			byte[] b = new byte[bufferSize];
			for(int len = 0; (len = in.read(b)) >= 0; tot += len)
				out.put(b, 0, len);
		}
		return tot;
	}

	/**
	 * Returns whether the given parent is a parent directory of the child file
	 */
    public static boolean isParentOf(File parent, File child) {
        for(child = child.getParentFile(); child != null; child = child.getParentFile()) if(child.equals(parent)) return true;
        return false;
    }

    /**
     *  Returns whether the given parent path is a parent directory of the child path
     */
    public static boolean isParentOf(String parent, String child) {
        return isParentOf(new File(parent), new File(child));
    }

    /**
     *  Returns that portion of the fullPath that is NOT equivalent to the fromPath.
     *  For example, if fullPath is "/foo/bar/src/logs/file.txt" and fromPath
     *  is "/foo/bar" this method would return "/src/logs/file.txt".
     */
    public static String getRelativePathFrom(String fullPath, String fromPath) throws IOException {
        fullPath = new File(fullPath).getCanonicalPath();
        fromPath = new File(fromPath).getCanonicalPath();
        if(fullPath.startsWith(fromPath)) return fullPath.substring(fromPath.length());
        else throw new IOException("Path '" + fromPath + "' is not part of '" + fullPath + "'");
    }

	public static String getFullPath(String directory, String filePath) {
		if(StringUtils.isBlank(directory))
			return filePath;
		if(StringUtils.isBlank(filePath)) {
			switch(directory.charAt(directory.length() - 1)) {
				case '/':
				case '\\':
					return directory;
				default:
					return directory + '/';
			}
		}
		StringBuilder sb = new StringBuilder(directory);
		switch(directory.charAt(directory.length() - 1)) {
			case '/':
			case '\\':
				switch(filePath.charAt(0)) {
					case '/':
					case '\\':
						return sb.append(filePath, 1, filePath.length()).toString();
					default:
						return sb.append(filePath).toString();
				}
			default:
				switch(filePath.charAt(0)) {
					case '/':
					case '\\':
						return sb.append(filePath).toString();
					default:
						return sb.append('/').append(filePath).toString();
				}
		}
	}

    /**
     * Writes the data from the contents object to the specifed file
     *
     * @param fileName
     * @param contents
     * @throws IOException
     */
    public static void saveToFile(String fileName, InputStream contents) throws IOException {
        readInto(contents, new FileOutputStream(fileName), BUFFER_SIZE);
    }

    /** Copies the contents of the specified InputStream into the specified
     * OutputStream
     * @param in The InputStream
     * @param out The OutputStream
     * @throws IOException If an exception occurs reading or writing to the streams
     * @return The total number of bytes written into the OutputStream
     */
    public static long copy(InputStream in, OutputStream out) throws IOException {
        return readInto(in, out, BUFFER_SIZE);
    }

	public static long copy(InputStream in, ByteOutput out, long offset, long length) throws IOException {
		return readInto(in, out, offset, length, BUFFER_SIZE);
	}

	public static long readInto(InputStream in, ByteOutput out, long offset, long length, int bufferSize) throws IOException {
		byte[] b = new byte[(int) Math.min(length, bufferSize)];
		if(offset > 0)
			in.skip(offset);
		long tot = 0;
		for(int len = 0; tot < length - offset && (len = in.read(b)) >= 0; tot += len)
			out.write(b, 0, len);
		return tot;
	}

    /** Copies the contents of the specified Reader into the specified
     * Writer
     * @param in The InputStream
     * @param out The OutputStream
     * @throws IOException If an exception occurs reading or writing to the streams
     * @return The total number of bytes written into the OutputStream
     */
    public static long copy(Reader in, Writer out) throws IOException {
        return readInto(in, out, BUFFER_SIZE);
    }

    /** Copies the contents of the specified Reader into the specified
     * Writer
     * @param in The InputStream
     * @param out The OutputStream
     * @throws IOException If an exception occurs reading or writing to the streams
     * @return The total number of bytes written into the OutputStream
     */
    public static int copy(Reader in, StringBuilder out) throws IOException {
        return readInto(in, out, BUFFER_SIZE);
    }

    /** Copies the contents of the specified Reader into the specified
     * Writer
     * @param in The InputStream
     * @param out The OutputStream
     * @throws IOException If an exception occurs reading or writing to the streams
     * @return The total number of bytes written into the OutputStream
     */
    public static int copy(Reader in, StringBuffer out) throws IOException {
        return readInto(in, out, BUFFER_SIZE);
    }

    public static int firstDifference(File file1, File file2) throws IOException {
    	FileInputStream fis1 = new FileInputStream(file1);
		try {
			FileInputStream fis2 = new FileInputStream(file2);
			try {
				byte[] buf1 = new byte[256];
				byte[] buf2 = new byte[256];
				int offset = 0;
				while(fis1.available() > 0 && fis2.available() > 0) {
					int r1 = fis1.read(buf1);
					int r2 = fis2.read(buf2);
					if(r1 != r2)
						return offset + Math.min(0, Math.min(r1, r2));
					for(int i = 0; i < r1; i++) {
						if(buf1[i] != buf2[i])
							return offset + i;
					}
					offset += r1;
				}
			} finally {
				fis2.close();
			}
		} finally {
			fis1.close();
    	}
    	return -1;
    }

	public static int firstDifferenceIgnoreWhitespace(File file1, File file2) throws IOException {
		BufferedInputStream is1 = new BufferedInputStream(new FileInputStream(file1));
		try {
			BufferedInputStream is2 = new BufferedInputStream(new FileInputStream(file2));
			try {
				int offset = 0;
				while(is1.available() > 0 && is2.available() > 0) {
					boolean ws1 = false;
					int r1;
					while(true) {
						r1 = is1.read();
						if(r1 >= 0 && Character.isWhitespace((char) r1))
							ws1 = true;
						else
							break;
					}
		
					boolean ws2 = false;
					int r2;
					while(true) {
						r2 = is2.read();
						offset++;
						if(r2 >= 0 && Character.isWhitespace((char) r2))
							ws2 = true;
						else
							break;
					}
					if(ws1 != ws2)
						return offset - 1;
					if(r1 != r2)
						return offset;
				}
			} finally {
				is2.close();
			}
		} finally {
			is1.close();
		}
		return -1;
	}
    /**
     * Searches for byte sequences. The
     * source is the byte array being searched, and the target
     * is the byte array being searched for.
     *
     * @param   source       the bytes being searched.
     * @param   sourceOffset offset of the source bytes.
     * @param   sourceCount  count of the source bytes.
     * @param   target       the bytes being searched for.
     * @param   targetOffset offset of the target bytes.
     * @param   targetCount  count of the target bytes.
     */
    public static int indexOf(byte[] source, int sourceOffset, int sourceCount, byte[] target, int targetOffset, int targetCount) {
		byte first  = target[targetOffset];
		int max = sourceOffset + (sourceCount - targetCount);

		for(int i = sourceOffset; i <= max; i++) {
			/* Look for first character. */
			if(source[i] != first) {
				while(++i <= max && source[i] != first)
					;
			}

			/* Found first character, now look at the rest of v2 */
			if(i <= max) {
				int j = i + 1;
				int end = j + targetCount - 1;
				for(int k = targetOffset + 1; j < end && source[j] == target[k]; j++, k++)
					;

				if(j == end) {
					/* Found whole string. */
					return i - sourceOffset;
				}
			}
		}
		return -1;
	}

    protected static final Pattern inetSocketAddressPattern = Pattern.compile("([^:/]*)(?:\\/(\\d{1,3}(?:\\.\\d{1,3}){3,5}))?:(\\d{1,5})");
	protected static final Pattern inetAddressPattern = Pattern.compile("([^:/]*)(?:\\/(\\d{1,3}(?:\\.\\d{1,3}){3,5}))?");

    public static boolean isInetAddressSame(String addressString, InetSocketAddress address) {
		return isInetAddressSame(addressString, address, true);
	}

	public static boolean isInetAddressSame(String addressString, InetSocketAddress address, boolean full) {
		Matcher matcher = inetSocketAddressPattern.matcher(addressString.trim());
    	if(matcher.matches()) {
    		int port = Integer.parseInt(matcher.group(3));
    		String ip = matcher.group(2);
    		String hostname = matcher.group(1).trim();
    		if(port == address.getPort()) {
				return isInetAddressSame(ip, hostname, address.getAddress(), full);
    		}
    	}
    	return false;
	}

	public static boolean isInetAddressSame(String addressString, InetAddress address, boolean full) {
		Matcher matcher = inetAddressPattern.matcher(addressString.trim());
		if(matcher.matches()) {
			String ip = matcher.group(2);
			String hostname = matcher.group(1).trim();
			return isInetAddressSame(ip, hostname, address, full);
		}
		return false;
	}

	protected static boolean isInetAddressSame(String ip, String hostname, InetAddress address, boolean full) {
		if((!StringUtils.isBlank(hostname) && hostname.equals("localhost")) || (!StringUtils.isBlank(ip) && (ip.equals("0.0.0.0") || ip.equals("127.0.0.1")))) {
			return address == null || address.isLoopbackAddress() || "0.0.0.0".equals(address.getHostAddress()) || "127.0.0.1".equals(address.getHostAddress());
		}
		if(address == null)
			return false;
		if(!StringUtils.isBlank(ip)) {
			if(ConvertUtils.areEqual(address.getHostAddress(), ip))
				return true;
			if(!full)
				return false;
		}
		// getHostName() can cause host name lookup, so check it second
		InetAddress[] others = null;
		if(!StringUtils.isBlank(hostname)) {
			try {
				others = InetAddress.getAllByName(hostname);
			} catch(UnknownHostException e) {
				return false;
			}
			if(others != null)
				for(InetAddress other : others)
					if(ConvertUtils.areEqual(address.getHostAddress(), other.getHostAddress()))
						return true;
		}
		if(!full)
			return false;
		InetAddress ia2;
		if(ip != null) {
			String[] ipParts = StringUtils.split(ip, '.');
			byte[] ipAddr = new byte[ipParts.length];
			for(int i = 0; i < ipParts.length; i++)
				ipAddr[i] = Byte.parseByte(ipParts[i]);
			try {
				ia2 = InetAddress.getByAddress(ipAddr);
			} catch(UnknownHostException e) {
				return false;
			}
			return address.getCanonicalHostName().equals(ia2.getCanonicalHostName());
		} else if(others != null) {
			for(InetAddress other : others)
				if(address.getCanonicalHostName().equals(other.getCanonicalHostName()))
					return true;
		}
		return false;
	}

	public static InetAddress parseInetAddress(String addressString) throws ConvertException, UnknownHostException {
		if(addressString == null)
			return null;
		Matcher matcher = inetAddressPattern.matcher(addressString.trim());
		if(matcher.matches()) {
			String ip = matcher.group(2);
			String hostname = matcher.group(1).trim();
			if(ip == null) {
				return InetAddress.getByName(hostname);
			} else {
				InetAddress ia = InetAddress.getByName(ip);
				return InetAddress.getByAddress(hostname, ia.getAddress());
			}
		} else
			throw new ConvertException(InetAddress.class, addressString.trim());
	}

    public static InetSocketAddress parseInetSocketAddress(String addressString) throws ConvertException, UnknownHostException {
    	if(addressString == null)
    		return null;
	    Matcher matcher = inetSocketAddressPattern.matcher(addressString.trim());
		if(matcher.matches()) {
			int port = Integer.parseInt(matcher.group(3));
			String ip = matcher.group(2);
			String hostname = matcher.group(1).trim();
			if(ip == null) {
				return new InetSocketAddress(hostname, port);
			} else {
				InetAddress ia = InetAddress.getByName(ip);
				return new InetSocketAddress(InetAddress.getByAddress(hostname, ia.getAddress()), port);
			}
		} else {
			throw new ConvertException(InetSocketAddress.class, addressString.trim());
		}
	}
    public static byte[] compressByteArray(byte[] input) throws IOException{
    	if(input == null)
    		return null;
    	Deflater compressor = new Deflater();
        compressor.setLevel(Deflater.BEST_COMPRESSION);
        compressor.setInput(input);
        compressor.finish();
        byte[] buf = new byte[input.length + 16];
        int total = 0;
        while(!compressor.finished()) {
        	int len = buf.length - total;
        	if(len < 1)
        		throw new IOException("Could not fit compression into " + buf.length + " bytes");
        	int count = compressor.deflate(buf, total, len);
        	total += count;
        }

        byte newbuf[] = new byte[total];
    	System.arraycopy(buf, 0, newbuf, 0, total);
    	return newbuf;
    }

    public static byte[] decompressByteArray(byte[] input) throws DataFormatException, IOException{
    	if(input == null)
    		return null;
    	Inflater decompressor = new Inflater();
        decompressor.setInput(input);
        ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);
        byte[] buf = new byte[1024];
        while (!decompressor.finished()) {
            int count = decompressor.inflate(buf);
            bos.write(buf, 0, count);
        }
        bos.close();
        return bos.toByteArray();
    }

    public static long zip(File source, OutputStream out) throws IOException {
    	ZipOutputStream zip = new ZipOutputStream(out);
    	long tot = 0;
    	byte[] buffer = new byte[BUFFER_SIZE];
    	if(source.isDirectory()) {
        	for(File f : source.listFiles()) {
	    		tot += addFileToZip(zip, f, "", buffer);
	    	}
    	} else {
    		ZipEntry zipEntry = new ZipEntry(source.getName());
    		zipEntry.setTime(source.lastModified());
    		zip.putNextEntry(zipEntry);
    		RandomAccessFile raf = new RandomAccessFile(source, "r");
    		try {
	    		for(int len = 0; (len=raf.read(buffer)) >= 0; tot += len)
	            	zip.write(buffer, 0, len);
    		} finally {
    			raf.close();
    		}
            zip.closeEntry();
        }
		zip.finish();
		zip.flush();
		return tot;
    }
    public static long unzip(File targetDir, InputStream in) throws IOException {
    	if(!targetDir.exists()) {
    		targetDir.mkdirs();
    		if(!targetDir.exists())
    			throw new IOException("Could not create target directory '" + targetDir.getAbsolutePath() + "'");
    	} else if(!targetDir.isDirectory())
    		throw new IOException("Target '" + targetDir.getAbsolutePath() + "' is NOT a directory");
    	else if(!targetDir.canWrite())
    		throw new IOException("Target '" + targetDir.getAbsolutePath() + "' is NOT writable");

    	ZipInputStream zip = new ZipInputStream(in);
    	long tot = 0;
    	byte[] buffer = new byte[BUFFER_SIZE];
    	ZipEntry zipEntry;
    	while((zipEntry=zip.getNextEntry()) != null) {
    		File file = new File(targetDir, zipEntry.getName());
			if(zipEntry.isDirectory()) {
				file.mkdirs();
    			if(!file.exists())
        			throw new IOException("Could not create directory '" + file.getAbsolutePath() + "'");
    		} else {
    			File dir = file.getParentFile();
    			if(dir != null)
    				dir.mkdirs();
    			if(!dir.exists())
        			throw new IOException("Could not create directory '" + dir.getAbsolutePath() + "'");
    			RandomAccessFile raf = new RandomAccessFile(file, "rw");
    			raf.setLength(0);
        		try {
        			for(int len = 0; (len=zip.read(buffer)) >= 0; tot += len)
	                	raf.write(buffer, 0, len);
	    		} finally {
	    			raf.close();
	    		}
    		}
			if(zipEntry.getTime() >= 0)
				file.setLastModified(zipEntry.getTime());
    	}
		return tot;
    }
    protected static long addFileToZip(ZipOutputStream zip, File file, String prefix, byte[] buffer) throws IOException {
    	if(file.isDirectory()) {
    		String name = prefix + file.getName() + "/";
    		ZipEntry zipEntry = new ZipEntry(name);
    		zipEntry.setTime(file.lastModified());
    		zip.putNextEntry(zipEntry);
    		zip.closeEntry();
    		long tot = 0;
    		for(File f : file.listFiles()) {
        		tot += addFileToZip(zip, f, name, buffer);
        	}
    		return tot;
    	} else {
    		ZipEntry zipEntry = new ZipEntry(prefix + file.getName());
    		zipEntry.setTime(file.lastModified());
    		zip.putNextEntry(zipEntry);
    		long tot = 0;
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			try {
				for(int len = 0; (len = raf.read(buffer)) >= 0; tot += len)
					zip.write(buffer, 0, len);
			} finally {
				raf.close();
			}
            zip.closeEntry();
            return tot;
    	}
    }

	public static boolean moveOverrideFile(File src, File dest) {
		if(src.renameTo(dest))
			return true;
		if(dest.exists()) {
			if(!dest.canWrite())
				return false;
			if(dest.exists()) {
				dest.delete();
				if(src.renameTo(dest))
					return true;
			}
		}
		try {
			FileInputStream in = new FileInputStream(src);
			try {
				FileOutputStream out = new FileOutputStream(dest);
				try {
					IOUtils.copy(in, out);
				} finally {
					out.close();
				}
			} finally {
				in.close();
			}
		} catch(IOException e) {
			log.info("Could not move file " + src.getAbsolutePath() + " on to " + dest.getAbsolutePath());
			return false;
		}
		src.delete();
		return true;
	}

	protected static class DeleteDirectoryHolder extends Thread {
		protected static final Deque<Path> directories = createDeque();

		protected static Deque<Path> createDeque() {
			Runtime.getRuntime().addShutdownHook(new DeleteDirectoryHolder());
			return new ConcurrentLinkedDeque<>();
		}

		@Override
		public void run() {
			Path directory;
			while((directory = DeleteDirectoryHolder.directories.poll()) != null)
				try {
					deleteDirectory(directory);
				} catch(IOException e) {
					log.info("Could not delete directory '" + directory + "': " + e.getMessage());
				}
		}
	}

	public static void deleteDirectoryOnExit(Path directory) {
		DeleteDirectoryHolder.directories.offer(directory);
	}

	public static void deleteDirectory(Path directory) throws IOException {
		Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}
    public static boolean deleteContents(File directory) {
    	if(directory.isDirectory()) {
        	for(File f : directory.listFiles()) {
	    		if(!f.delete())
	    			return false;
	    	}
        	return true;
    	}
    	return false;
	}

	public static String safeCanonicalPath(File baseDirectory) {
		if(baseDirectory == null)
			return null;
		try {
			return baseDirectory.getCanonicalPath();
		} catch(IOException e) {
			return baseDirectory.getAbsolutePath();
		}
	}

	public static String getServerIP(String hostName) throws UnknownHostException {
		return InetAddress.getByName(hostName).getHostAddress();
	}

	public interface Replacement {
		long getStart();

		long getEnd();

		byte[] getBytes();
	}

	/**
	 * Replaces sections of a RandomAccessFile
	 * 
	 * @param raf
	 * @param replacements
	 *            Ordered list of replacements
	 * @throws IOException
	 */
	public static void replaceRandomAccessBytes(RandomAccessFile raf, Iterable<Replacement> replacements) throws IOException {
		for(Replacement replacement : replacements) {
			long start = replacement.getStart();
			long end = replacement.getEnd();
			byte[] newBytes = replacement.getBytes();
			replaceRandomAccessBytes(raf, start, end, newBytes);
		}
		/*
		long totalDiff = 0;
		for(Replacement replacement : replacements) {
			long start = replacement.getStart();
			long end = replacement.getEnd();
			byte[] newBytes = replacement.getBytes();
			int len = (newBytes == null ? 0 : newBytes.length);
			long diff = len + start - end;
			if(diff == 0) { // easy case - simple replace
				if(len == 0)
					return;
				raf.seek(start);
				raf.write(newBytes);
			} else if(diff < 0) {
				// write bytes then copy down
				if(len > 0) {
					raf.seek(start);
					raf.write(newBytes);
				}
				raf.seek(end);
				byte[] tmp = new byte[BUFFER_SIZE];
				int r;
				while((r = raf.read(tmp)) >= 0) {
					if(r > 0) {
						raf.seek(end + diff);
						raf.write(tmp, 0, r);
						end += r;
						raf.seek(end);
					}
				}
				raf.setLength(raf.length() + diff);
			} else {
				// extend and copy bytes then insert
				long stop = end;
				end = raf.length();
				raf.setLength(end + diff);
				byte[] tmp = new byte[BUFFER_SIZE];
				int l;
				do {
					if(end - stop < tmp.length) {
						l = (int) (end - stop);
					} else {
						l = tmp.length;
					}
					end -= l;
					raf.seek(end);
					int r;
					int o = 0;
					while(o < l) {
						r = raf.read(tmp, o, l - o);
						if(r < 0)
							throw new IOException("Could not read file at position " + raf.getFilePointer());
						o += r;
					}
					raf.seek(end + diff);
					raf.write(tmp, 0, l);
				} while(end > stop);
				if(len > 0) {
					raf.seek(start);
					raf.write(newBytes);
				}
			}
		}*/
	}
	public static void replaceRandomAccessBytes(RandomAccessFile raf, long start, long end, byte[] newBytes) throws IOException {
		int len = (newBytes == null ? 0 : newBytes.length);
		long diff = len + start - end;
		if(diff == 0) { // easy case - simple replace
			if(len == 0)
				return;
			raf.seek(start);
			raf.write(newBytes);
		} else if(diff < 0) {
			// write bytes then copy down
			if(len > 0) {
				raf.seek(start);
				raf.write(newBytes);
			}
			raf.seek(end);
			byte[] tmp = new byte[BUFFER_SIZE];
			int r;
			while((r = raf.read(tmp)) >= 0) {
				if(r > 0) {
					raf.seek(end + diff);
					raf.write(tmp, 0, r);
					end += r;
					raf.seek(end);
				}
			}
			raf.setLength(raf.length() + diff);
		} else {
			// extend and copy bytes then insert
			long stop = end;
			end = raf.length();
			raf.setLength(end + diff);
			byte[] tmp = new byte[BUFFER_SIZE];
			int l;
			do {
				if(end - stop < tmp.length) {
					l = (int) (end - stop);
				} else {
					l = tmp.length;
				}
				end -= l;
				raf.seek(end);
				int r;
				int o = 0;
				while(o < l) {
					r = raf.read(tmp, o, l - o);
					if(r < 0)
						throw new IOException("Could not read file at position " + raf.getFilePointer());
					o += r;
				}
				raf.seek(end + diff);
				raf.write(tmp, 0, l);
			} while(end > stop);
			if(len > 0) {
				raf.seek(start);
				raf.write(newBytes);
			}
		}
	}

	public static long copy(File from, File to) throws IOException {
		OutputStream out = new FileOutputStream(to);
		try {
			InputStream in = new FileInputStream(from);
			try {
				return copy(in, out);
			} finally {
				in.close();
			}
		} finally {
			out.close();
		}
	}
}
