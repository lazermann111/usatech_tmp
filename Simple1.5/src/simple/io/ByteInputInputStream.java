package simple.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

public class ByteInputInputStream extends InputStream {
	protected final ByteInput delegate;
	protected int mark = -1;
	public ByteInputInputStream(ByteInput delegate) {
		super();
		this.delegate = delegate;
	}

	@Override
	public int read() throws IOException {
		try {
			return delegate.readByte();
		} catch(EOFException e) {
			return -1;
		}
	}
	@Override
	public int available() throws IOException {
		return delegate.length() - delegate.position();
	}
	@Override
	public void close() throws IOException {
		// do nothing
	}
	@Override
	public void mark(int readlimit) {
		mark = delegate.position();
	}
	@Override
	public boolean markSupported() {
		return delegate.isResettable();
	}
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if(len < 1)
			throw new IndexOutOfBoundsException();
		int r = Math.min(len, Math.max(available(), 1));
		try {
			delegate.readBytes(b, off, r);
		} catch(EOFException e) {
			return -1;
		}
		return r;
	}
	@Override
	public void reset() throws IOException {
		if(mark < 0)
			throw new IOException("Mark not set");
		delegate.reset();
		while(delegate.position() < mark)
			delegate.readByte();
	}
	@Override
	public long skip(long n) throws IOException {
		long i = 0;
		for(; i < n; i++)
			try {
				delegate.readByte();
			} catch(EOFException e) {
				break;
			}
		return i;
	}
}
