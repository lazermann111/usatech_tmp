package simple.io;

import java.nio.ByteBuffer;
import java.util.zip.CRC32;

import sun.security.jca.JCAUtil;

public class CRCByteChecker implements ByteChecker<Long> {
	protected final CRC32 crc32 = new CRC32();
	protected byte[] tempArray;

	@Override
	public void update(ByteBuffer buffer) {
		if(buffer.hasRemaining() == false) {
			return;
		}
		if(buffer.hasArray()) {
			byte[] b = buffer.array();
			int ofs = buffer.arrayOffset();
			int pos = buffer.position();
			int lim = buffer.limit();
			crc32.update(b, ofs + pos, lim - pos);
			buffer.position(lim);
		} else {
			int len = buffer.remaining();
			int n = JCAUtil.getTempArraySize(len);
			if((tempArray == null) || (n > tempArray.length)) {
				tempArray = new byte[n];
			}
			while(len > 0) {
				int chunk = Math.min(len, tempArray.length);
				buffer.get(tempArray, 0, chunk);
				crc32.update(tempArray, 0, chunk);
				len -= chunk;
			}
		}
	}

	@Override
	public void update(byte[] bytes, int offset, int len) {
		crc32.update(bytes, offset, len);
	}

	@Override
	public void reset() {
		crc32.reset();
	}

	@Override
	public Long getCheckValue() {
		return crc32.getValue();
	}
}
