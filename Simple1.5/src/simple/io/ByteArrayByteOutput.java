/**
 *
 */
package simple.io;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * @author Brian S. Krug
 *
 */
public class ByteArrayByteOutput implements ByteOutput {
	protected byte[] bytes;
	protected int position = 0;

	/**
	 *
	 */
	public ByteArrayByteOutput(int initialCapacity) {
		if(initialCapacity > 9)
			bytes = new byte[initialCapacity];
		else
			bytes = new byte[10];
	}

	public byte[] getBytes() {
		return bytes;
	}
	public int getPosition() {
		return position;
	}

    /**
	 * @param add
	 */
	protected void ensureCapacity(int add) {
		if(position + add >= bytes.length) {
			byte[] tmp = new byte[(bytes.length + add) * 2];
			System.arraycopy(bytes, 0, tmp, 0, position);
			bytes = tmp;
		}
	}
	/**
	 * @see simple.io.ByteOutput#writeBytes(java.lang.String)
	 */
	public void writeBytes(String s) throws IOException {
		writeBytes(s, s.length());
	}
	/**
	 * @see simple.io.ByteOutput#writeChars(java.lang.String)
	 */
	public void writeChars(String s) throws IOException {
		writeChars(s, s.length());
	}
	/**
	 * @see simple.io.ByteOutput#writeByte(int)
	 */
    public void writeByte(int b) throws IOException {
    	ensureCapacity(1);
    	bytes[position++] = (byte) b;
    }

	/**
	 * @see simple.io.ByteOutput#writeShort(int)
	 */
    public void writeShort(int s) throws IOException {
    	ensureCapacity(2);
    	bytes[position++] = (byte)((s >>> 8) & 0xFF);
    	bytes[position++] = (byte)((s >>> 0) & 0xFF);
    }

    /**
	 * @see simple.io.ByteOutput#writeChar(int)
	 */
    public void writeChar(int c) throws IOException {
    	ensureCapacity(2);
    	bytes[position++] = (byte)((c >>> 8) & 0xFF);
    	bytes[position++] = (byte)((c >>> 0) & 0xFF);
    }

    /**
	 * @see simple.io.ByteOutput#writeInt(int)
	 */
    public void writeInt(int i) throws IOException {
    	ensureCapacity(4);
    	bytes[position++] = (byte)((i >>> 24) & 0xFF);
        bytes[position++] = (byte)((i >>> 16) & 0xFF);
        bytes[position++] = (byte)((i >>>  8) & 0xFF);
        bytes[position++] = (byte)((i >>>  0) & 0xFF);
    }

    /**
	 * @see simple.io.ByteOutput#writeLong(long)
	 */
    public void writeLong(long l) throws IOException {
    	ensureCapacity(8);
    	bytes[position++] = (byte)(l >>> 56);
        bytes[position++] = (byte)(l >>> 48);
        bytes[position++] = (byte)(l >>> 40);
        bytes[position++] = (byte)(l >>> 32);
        bytes[position++] = (byte)(l >>> 24);
        bytes[position++] = (byte)(l >>> 16);
        bytes[position++] = (byte)(l >>>  8);
        bytes[position++] = (byte)(l >>>  0);
    }

    /**
	 * @see simple.io.ByteOutput#writeFloat(float)
	 */
    public void writeFloat(float f) throws IOException {
    	writeInt(Float.floatToIntBits(f));
    }

    /**
	 * @see simple.io.ByteOutput#writeDouble(double)
	 */
    public void writeDouble(double d) throws IOException {
    	writeLong(Double.doubleToLongBits(d));
    }

    /**
	 * @see simple.io.ByteOutput#writeBytes(java.lang.String, int)
	 */
    public void writeBytes(String s, int length) throws IOException {
		writeBytes(s, 0, length);
	}

	/**
	 * @see simple.io.ByteOutput#writeBytes(java.lang.String, int, int)
	 */
	public void writeBytes(String s, int offset, int length) throws IOException {
		byte[] bytes;
		if(offset == 0 && length == s.length())
			bytes = s.getBytes();
		else
			bytes = s.substring(offset, offset + length).getBytes();
		write(bytes);
	}

    /**
	 * @see simple.io.ByteOutput#writeChars(java.lang.String, int)
	 */
    public void writeChars(String s, int length) throws IOException {
        ensureCapacity(length * 2);
    	for (int i = 0 ; i < length ; i++) {
            int v = s.charAt(i);
            bytes[position++] = (byte)((v >>> 8) & 0xFF);
            bytes[position++] = (byte)((v >>> 0) & 0xFF);
        }
    }

    public void write(byte[] bytes) throws IOException {
    	write(bytes, 0, bytes.length);
    }

    public void write(byte[] bytes, int offset, int length) throws IOException {
    	ensureCapacity(length);
    	System.arraycopy(bytes, offset, this.bytes, position, length);
    	position += length;
    }
    public void write(ByteBuffer buffer) throws IOException {
		int length = buffer.remaining();
		ensureCapacity(length);
		buffer.get(bytes, position, length);
		position += length;
    }

    public void flush() throws IOException {
    	// Do nothing
    }
}
