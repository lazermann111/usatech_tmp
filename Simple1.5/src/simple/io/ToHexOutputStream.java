/**
 *
 */
package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

import simple.text.StringUtils;

/**
 * @author Brian S. Krug
 *
 */
public class ToHexOutputStream extends OutputStream {
	protected final Writer delegate;

	public ToHexOutputStream(Writer delegate) {
		this.delegate = delegate;
	}

	/**
	 * @see java.io.OutputStream#write(int)
	 */
	@Override
	public void write(int b) throws IOException {
		byte b0 = (byte)b;
		delegate.write(StringUtils.hexDigit1(b0));
		delegate.write(StringUtils.hexDigit2(b0));
	}
	/**
	 * @see java.io.OutputStream#flush()
	 */
	@Override
	public void flush() throws IOException {
		delegate.flush();
	}
}
