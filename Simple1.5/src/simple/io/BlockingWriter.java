package simple.io;

import java.io.IOException;
import java.io.Writer;
import java.util.concurrent.locks.ReentrantLock;

public abstract class BlockingWriter extends Writer {
	protected final ReentrantLock stateLock = new ReentrantLock();
	protected Writer delegate;

	public BlockingWriter() {
	}

	protected abstract Writer openDelegate() throws IOException;

	protected abstract void prepareDelegate() throws IOException;

	public void open() throws IOException {
		stateLock.lock();
		boolean okay = false;
		try {
			if(delegate == null)
				delegate = openDelegate();
			else
				prepareDelegate();
			okay = true;
		} finally {
			if(!okay)
				close();
		}
	}
	@Override
	public void close() {
		stateLock.unlock();
	}

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
		checkOpen();
		try {
			delegate.write(cbuf, off, len);
		} catch(IOException e) {
			handleError();
			throw e;
		}
	}

	protected void checkOpen() throws IOException {
		if(!stateLock.isHeldByCurrentThread())
			throw new IOException("Writer is not opened by this thread");
	}

	@Override
	public void flush() throws IOException {
		checkOpen();
		try {
			delegate.flush();
		} catch(IOException e) {
			handleError();
			throw e;
		}
	}

	public void write(int c) throws IOException {
		checkOpen();
		try {
			delegate.write(c);
		} catch(IOException e) {
			handleError();
			throw e;
		}
	}

	public void write(String str, int off, int len) throws IOException {
		checkOpen();
		try {
			delegate.write(str, off, len);
		} catch(IOException e) {
			handleError();
			throw e;
		}
	}

	public void reset() {
		if(!stateLock.isHeldByCurrentThread())
			stateLock.lock();
		try {
			if(delegate != null) {
				closeDelegate();
				delegate = null;
			}
		} finally {
			close();
		}
	}

	protected void closeDelegate() {
		try {
			delegate.close();
		} catch(IOException e) {
			// ignore
		}
	}

	protected void handleError() {
		if(delegate != null) {
			closeDelegate();
			delegate = null;
		}
	}
}