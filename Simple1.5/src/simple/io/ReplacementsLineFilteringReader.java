package simple.io;

import java.io.BufferedReader;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplacementsLineFilteringReader extends LineFilteringReader {
	protected final Map<Pattern, String> replacements;

	public ReplacementsLineFilteringReader(BufferedReader delegate, String newline, Map<Pattern, String> replacements) {
		super(delegate, newline);
		this.replacements = replacements;
	}

	@Override
	protected String filter(String line) {
		for(Map.Entry<Pattern, String> entry : replacements.entrySet()) {
			Matcher matcher = entry.getKey().matcher(line);
			line = matcher.replaceAll(entry.getValue());
		}
		return line;
	}
}
