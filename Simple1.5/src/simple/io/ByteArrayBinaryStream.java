package simple.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteArrayBinaryStream implements BinaryStream {
	protected final byte[] content;
	
	public ByteArrayBinaryStream(byte[] content) {
		this.content = content;
	}

	public InputStream getInputStream() {
		return new ByteArrayInputStream(content);
	}

	public long getLength() {
		return content.length;
	}

	public long copyInto(ByteOutput output) throws IOException {
		output.write(content);
		return content.length;
    }
}
