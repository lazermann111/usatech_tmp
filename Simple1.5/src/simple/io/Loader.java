/*
 * Loader.java
 *
 * Created on December 23, 2003, 1:04 PM
 */

package simple.io;

/**
 *
 * @author  Brian S. Krug
 */
public interface Loader {
    /** Loads the specified ConfigSource, modifying the loadKeys as necessary.
     *
     */
    public void load(ConfigSource src) throws LoadingException ; 
}
