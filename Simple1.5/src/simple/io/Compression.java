/**
 *
 */
package simple.io;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import simple.lang.EnumValueLookup;
import simple.lang.InvalidValueException;

/**
 * @author Brian S. Krug
 *
 */
public enum Compression {
	NONE(null) {
		@Override
		public InputStream getDecompressingInputStream(InputStream in) {
			return in;
		}
		@Override
		public OutputStream getCompressingOutputStream(OutputStream out) {
			return out;
		}
	},
	ZLIB("ZLIB") {
		@Override
		public InputStream getDecompressingInputStream(InputStream in) {
			return new InflaterInputStream(in);
		}
		@Override
		public OutputStream getCompressingOutputStream(OutputStream out) {
			return new DeflaterOutputStream(out);
		}
	}
	;
	private final String value;
	private Compression(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	public abstract InputStream getDecompressingInputStream(InputStream in) ;
	public abstract OutputStream getCompressingOutputStream(OutputStream out) ;
	protected final static EnumValueLookup<Compression, String> lookup = new EnumValueLookup<Compression, String>(Compression.class);
    public static Compression getByValue(String value) throws InvalidValueException {
    	return lookup.getByValue(value);
    }

}
