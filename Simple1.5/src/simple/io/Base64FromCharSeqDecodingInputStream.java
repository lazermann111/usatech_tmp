/*
 * Created on Sep 27, 2005
 *
 */
package simple.io;


public class Base64FromCharSeqDecodingInputStream extends AbstractBase64DecodingInputStream {
	protected CharSequence in;
	protected int r = 0;

	public Base64FromCharSeqDecodingInputStream(CharSequence in) {
        this.in = in;
    }

	@Override
	protected int get() {
		if(r >= in.length())
			return -1;
		return in.charAt(r++);
	}
    @Override
	protected int remaining() {
		return in.length() - r;
	}
}