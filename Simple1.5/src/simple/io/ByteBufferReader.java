package simple.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;

/** This implementation is NOT thread-safe.
 * @author Brian S. Krug
 *
 */
public class ByteBufferReader extends DecodingReader {
	protected final ByteBuffer buffer;

	public ByteBufferReader(ByteBuffer buffer, CharsetDecoder decoder) {
        super(decoder);
        this.buffer = buffer;
    }

	@Override
	public void close() throws IOException {
	}

	/** Fills the buffer with new bytes to read and sets the buffer's position and limit
	 *  as appropriate. The default implementation is
	 * to do nothing and return -1
	 * @return The number of new bytes obtained or -1 if none remain.
	 * @throws IOException
	 */
	protected int fillBuffer() throws IOException {
		return -1;
	}

	@Override
	public int read(CharBuffer charBuffer) throws IOException {
		int start = charBuffer.position();
		if(!buffer.hasRemaining()) {
			int rem = fillBuffer();
			if(rem == 0)
				return 0;
			if(rem < 0) {
				decoder.decode(buffer, charBuffer, true);
				decoder.flush(charBuffer);
				decoder.reset();
				if(start == charBuffer.position())
					return -1;
				else
					return charBuffer.position() - start;
			}
		}
		decoder.decode(buffer, charBuffer, false);
		return charBuffer.position() - start;
	}

	@Override
	public boolean ready() throws IOException {
		return buffer.hasRemaining();
	}
}
