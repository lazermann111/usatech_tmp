/*
 * Created on May 6, 2005
 *
 */
package simple.io;

public interface Base64Constants {
    public final static byte EQUALS_SIGN = (byte) '=';
    public final static byte[] ALPHABET = {
        (byte) 'A', (byte) 'B', (byte) 'C', (byte) 'D', (byte) 'E', (byte) 'F',
        (byte) 'G', (byte) 'H', (byte) 'I', (byte) 'J', (byte) 'K', (byte) 'L',
        (byte) 'M', (byte) 'N', (byte) 'O', (byte) 'P', (byte) 'Q', (byte) 'R',
        (byte) 'S', (byte) 'T', (byte) 'U', (byte) 'V', (byte) 'W', (byte) 'X',
        (byte) 'Y', (byte) 'Z', (byte) 'a', (byte) 'b', (byte) 'c', (byte) 'd',
        (byte) 'e', (byte) 'f', (byte) 'g', (byte) 'h', (byte) 'i', (byte) 'j',
        (byte) 'k', (byte) 'l', (byte) 'm', (byte) 'n', (byte) 'o', (byte) 'p',
        (byte) 'q', (byte) 'r', (byte) 's', (byte) 't', (byte) 'u', (byte) 'v',
        (byte) 'w', (byte) 'x', (byte) 'y', (byte) 'z', (byte) '0', (byte) '1',
        (byte) '2', (byte) '3', (byte) '4', (byte) '5', (byte) '6', (byte) '7',
        (byte) '8', (byte) '9', (byte) '+', (byte) '/'
    };
    public final static byte[] DECODABET = {
        // char codes 0 - 17 (including <tab> <LF> and <CR>)
        -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9,
        // char codes 18 - 35 (including <space>)
        -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9,
        // char codes 36 - 42 (punctuation marks)
        -9, -9, -9, -9, -9, -9, -9, 
        // char codes 43 - 47 (including "+" and "/" )
        62, -9, -9, -9, 63, 
        // char codes 48 - 64 (Numbers 0 - 9, and "=")
        52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 
        // char codes 65 - 90 (Letters 'A' through 'Z')
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 
        // char codes 91-9
        -9, -9, -9, -9, -9, -9,
        // char codes 97 - 122 (Letters 'a' through 'z')
        26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,
        // char codes 123 - 127
        -9, -9, -9, -9
    };
    public final static byte BAD_ENCODING = -9;

    // Indicates error in encoding
    public final static byte white_SPACE_ENC = -5;

    // Indicates white space in encoding
    public final static byte EQUALS_SIGN_ENC = -1;
}
