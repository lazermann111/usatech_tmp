/*
 * BufferReaderWriter.java
 *
 * Created on December 12, 2003, 9:27 AM
 */

package simple.io;

import java.io.IOException;
import java.io.Writer;
import java.nio.CharBuffer;

import simple.db.CharacterStream;

/**
 * This class loads into a char buffer from its Writer and reads from the same char buffer using its Reader.
 * WARNING: this class is not thread-safe. If you access it from more than one thread you must synchronize calls made against
 * this class.
 *
 * @author  Brian S. Krug
 */
public interface BufferReaderWriter extends CharacterStream {
	public int size();

	/**
	 * Returns an Writer to write the data access by the Reader
	 * 
	 * @return The Writer
	 */
	public Writer getWriter();

	public void clear() throws IOException;

	/**
	 * Copies bytes from this BufferReaderWriter to the CharBuffer. The number of bytes copied is return and
	 * will be the smaller of charBuffer.remaining() or (BufferReaderWriter.size() - offset)
	 * 
	 * @param bb
	 * @param offset
	 *            The offset in the BufferReaderWriter from which to start copying bytes
	 * @return
	 */
	public int putInto(CharBuffer charBuffer, int offset) throws IOException;

	public long copyInto(Writer output) throws IOException;

}
