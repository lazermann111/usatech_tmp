/**
 *
 */
package simple.io;

import java.io.Console;
import java.io.PrintWriter;

/**
 * @author Brian S. Krug
 *
 */
public class ConsoleInteraction extends AbstractTextInteraction {
	protected final Console console;

	public ConsoleInteraction(Console console) {
		this.console = console;
	}

	/**
	 * @see simple.io.Interaction#printf(java.lang.String, java.lang.Object[])
	 */
	public void printf(String format, Object... args) {
		console.printf(format, args);
	}

	/**
	 * @see simple.io.Interaction#readLine(java.lang.String, java.lang.Object[])
	 */
	public String readLine(String fmt, Object... args) {
		return console.readLine(fmt, args);
	}

	/**
	 * @see simple.io.Interaction#readPassword(java.lang.String, java.lang.Object[])
	 */
	public char[] readPassword(String fmt, Object... args) {
		return console.readPassword(fmt, args);
	}


	/**
	 * @see simple.io.Interaction#getWriter()
	 */
	public PrintWriter getWriter() {
		return console.writer();
	}
}
