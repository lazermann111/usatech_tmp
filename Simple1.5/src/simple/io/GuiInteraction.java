/**
 *
 */
package simple.io;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog.ModalityType;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.JTextComponent;

import simple.event.ProgressEvent;
import simple.event.ProgressListener;
import simple.text.StringUtils;

import com.sun.java.swing.plaf.windows.WindowsTreeUI;

/**
 * @author Brian S. Krug
 *
 */
public class GuiInteraction extends AbstractSectionInteraction<JComponent> {
	private static final Log log = Log.getLog();
	protected static final Icon expandedIcon = new WindowsTreeUI.ExpandedIcon();
	protected static final Icon collapsedIcon = new WindowsTreeUI.CollapsedIcon();
	protected static Icon completeIcon;
	protected static Icon failIcon;
	static {
		try {
			completeIcon = new ImageIcon(new URL("resource:resources/complete_icon.png"));
		} catch(MalformedURLException e) {
			log.warn("Could not get complete icon", e);
		}
		try {
			failIcon = new ImageIcon(new URL("resource:resources/fail_icon.png"));
		} catch(MalformedURLException e) {
			log.warn("Could not get fail icon", e);
		}
	}

	protected final StringBuilder appendable = new StringBuilder();
	protected final Formatter formatter = new Formatter(appendable);
	protected final Container parent;
	protected JPanel messagePanel;
	protected JComponent currentSection;
	protected JScrollPane scrollPane;
	protected final Runnable shower;
	protected Color background = Color.WHITE;
	protected final StringWriter innerWriter = new StringWriter();
	protected final PrintWriter writer = new PrintWriter(innerWriter) {
		@Override
		public void flush() {
			if(innerWriter.getBuffer().length() > 0) {
				addMessage(innerWriter.toString());
				innerWriter.getBuffer().setLength(0);
			}
		}		
	};
	protected class PromptFuture {
		protected final JTextField field;
		protected final CountDownLatch done = new CountDownLatch(1);	
		protected volatile boolean canceled = false;
		public PromptFuture(JTextField textField) {
			this.field = textField;
			field.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					field.setEnabled(false);
					done.countDown();
				}
			});
			addMessageComponent(field);
		}
		public boolean cancel(boolean mayInterruptIfRunning) {
			if(done.getCount() <= 0)
				return false;
			field.setEnabled(false);
			canceled = true;
			done.countDown();
			return true;
		}
		public String getText() throws InterruptedException {
			try {
				done.await();
			} finally {
				field.setEnabled(false);
			}
			return canceled ? null : field.getText();
		}
		public String getText(long timeout, TimeUnit unit) throws InterruptedException {
			try {
				done.await(timeout, unit);
			} finally {
				field.setEnabled(false);
			}
			return canceled ? null : field.getText();
		}
		public boolean isCancelled() {
			return canceled;
		}
		public boolean isDone() {
			return done.getCount() <= 0;
		}
	}
	protected class StringPromptFuture extends PromptFuture implements Future<String> {
		public StringPromptFuture(JTextField textField) {
			super(textField);
		}	
		public String get() throws InterruptedException, ExecutionException {
			return getText();
		}
		public String get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
			return getText(timeout, unit);
		}
	}
	protected class FilePromptFuture extends PromptFuture implements Future<File> {
		private File chosenFile = null;
		private File defaultFile;
		private boolean fileChosen = false;

		public void setChosenFile(File chosenFile) {
			this.chosenFile = chosenFile;
			fileChosen = true;
		}
		public FilePromptFuture(JTextField textField) {
			super(textField);
			this.defaultFile = null;
		}	
		public FilePromptFuture(JTextField textField, String defaultFileName) {
			super(textField);
			this.defaultFile = new File(defaultFileName);
		}	
		public FilePromptFuture(JTextField textField, File defaultFile) {
			super(textField);
			this.defaultFile = defaultFile;
		}	
		public File get() throws InterruptedException, ExecutionException {
			String fn = getText();
			return fileChosen ? chosenFile : (StringUtils.isBlank(fn) ? defaultFile : new File(fn));
		}
		public File get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
			String fn = getText(timeout, unit);
			return fileChosen ? chosenFile : (StringUtils.isBlank(fn) ? defaultFile : new File(fn));
		}
	}	
	protected class BrowseFilePromptFuture extends FilePromptFuture implements Future<File>, MouseListener {
		
		private String caption;
		private File baseDir;
		private FileSystemView view;
		
		public BrowseFilePromptFuture(String caption, File baseDir, JTextField textField) {
			super(textField);
			this.baseDir = baseDir;
			this.caption = caption;
			textField.addMouseListener(this);
		}

		public BrowseFilePromptFuture(String caption, File baseDir, JTextField textField, FileSystemView view) {
			super(textField);
			this.baseDir = baseDir;
			this.caption = caption;
			this.view = view;
			textField.addMouseListener(this);
		}

		@Override
		public void mouseClicked(MouseEvent click) {
			if(click.getClickCount() > 0)
			{				
				JFileChooser chooser = view != null ? new JFileChooser(view) : new JFileChooser(baseDir);
			    chooser.setDialogTitle(caption);
			    String current = BrowseFilePromptFuture.this.field.getText();
				if(StringUtils.isBlank(current))
			    {
			    	chooser.setSelectedFile(baseDir);
			    }
			    else
			    {
			    	chooser.setSelectedFile(new File(current));
			    }
			    boolean foundDecentFileOrAbort = false;			    
			    
			    while(!foundDecentFileOrAbort)
			    {
				    int returnVal = chooser.showOpenDialog(SwingUtilities.getRootPane(BrowseFilePromptFuture.this.field));
				    if(returnVal == JFileChooser.APPROVE_OPTION) {
				    	if(view != null || (chooser.getSelectedFile() != null && chooser.getSelectedFile().exists()))
				    	{
					    	BrowseFilePromptFuture.this.field.setText(chooser.getSelectedFile().getAbsolutePath());
					    	for(ActionListener action : BrowseFilePromptFuture.this.field.getActionListeners())
					    	{
					    		action.actionPerformed(new ActionEvent(BrowseFilePromptFuture.this.field, ActionEvent.ACTION_PERFORMED, ""));
					    	}
					    	setChosenFile(chooser.getSelectedFile());
					    	foundDecentFileOrAbort = true;					    	
				    	}
				    	else
				    	{
				    		GuiInteraction.this.addMessage("Cannot read file " + chooser.getSelectedFile().getAbsolutePath());
				    	}
				    }
				    else 
				    {
				    	foundDecentFileOrAbort = true;				    	
				    }
			    }
			}
		}

		@Override public void mouseEntered(MouseEvent e) {;}
		@Override public void mouseExited(MouseEvent e) {;}		
		@Override public void mousePressed(MouseEvent e) {;}
		@Override public void mouseReleased(MouseEvent e) {;}
	}
	protected class CharArrayPromptFuture extends PromptFuture implements Future<char[]> {
		public CharArrayPromptFuture(JTextField textField) {
			super(textField);
		}
		public char[] get() throws InterruptedException, ExecutionException {
			String text = getText();
			return (text == null ? null : text.toCharArray());
		}
		public char[] get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
			String text = getText(timeout, unit);
			return (text == null ? null : text.toCharArray());
		}
	}

	protected class ChoiceFuture implements Future<int[]> {
		protected final JPanel panel;
		protected final JButton button;
		protected final List<JToggleButton> checks;
		protected final boolean multiple;
		protected final CountDownLatch done = new CountDownLatch(1);
		protected volatile boolean canceled = false;

		public ChoiceFuture(String prompt, String[] options, boolean multiple, int[] defaultChoices) {
			this.panel = new JPanel();
			this.multiple = multiple;
			BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
			panel.setLayout(layout);
			checks = new ArrayList<JToggleButton>();
			if(multiple) {
				final JCheckBox checkAll = new JCheckBox(prompt);
				checkAll.addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent e) {
						for(JToggleButton check : checks)
							check.setSelected(checkAll.isSelected());
					}
				});
				checkAll.setAlignmentX(Component.LEFT_ALIGNMENT);
				panel.add(checkAll);
			} else {
				JLabel label = new JLabel(prompt);
				label.setAlignmentX(Component.LEFT_ALIGNMENT);
				panel.add(label);
			}
			JSeparator sep = new JSeparator();
			panel.add(sep);
			for(String option : options) {
				JToggleButton comp = (multiple ? new JCheckBox(option) : new JRadioButton(option));
				comp.setAlignmentX(Component.LEFT_ALIGNMENT);
				panel.add(comp);
				checks.add(comp);
			}
			if(!multiple) {
				ButtonGroup group = new ButtonGroup();
				for(JToggleButton comp : checks)
					group.add(comp);
			}
			if(defaultChoices != null)
				for(int c : defaultChoices)
					if(c > 0 && c <= checks.size()) {
						checks.get(c - 1).setSelected(true);
						if(!multiple)
							break;
					}
			button = new JButton("Submit");
			button.setAlignmentX(Component.CENTER_ALIGNMENT);
			panel.add(button);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					button.setEnabled(false);
					done.countDown();
				}
			});
			addMessageComponent(panel);
		}

		public boolean cancel(boolean mayInterruptIfRunning) {
			if(done.getCount() <= 0)
				return false;
			button.setEnabled(false);
			canceled = true;
			done.countDown();
			return true;
		}

		public int[] get() throws InterruptedException {
			try {
				done.await();
			} finally {
				button.setEnabled(false);
			}
			return canceled ? null : gatherChoices();
		}

		public int[] get(long timeout, TimeUnit unit) throws InterruptedException {
			try {
				done.await(timeout, unit);
			} finally {
				button.setEnabled(false);
			}
			return canceled ? null : gatherChoices();
		}

		protected int[] gatherChoices() {
			if(multiple) {
				int cnt = 0;
				for(JToggleButton check : checks)
					if(check.isSelected())
						cnt++;
				int[] result = new int[cnt];
				cnt = 0;
				int i = 1;
				for(JToggleButton check : checks) {
					if(check.isSelected())
						result[cnt++] = i;
					i++;
				}
				return result;
			} else {
				int i = 1;
				for(JToggleButton check : checks) {
					if(check.isSelected())
						return new int[] { i };
					i++;
				}
				return new int[] { 0 };
			}
		}
		public boolean isCancelled() {
			return canceled;
		}

		public boolean isDone() {
			return done.getCount() <= 0;
		}
	}
	public GuiInteraction(Window parentWindow) {
		final JDialog dialog = new JDialog(parentWindow, ModalityType.APPLICATION_MODAL);
		dialog.setSize(600, 400);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				System.exit(0);
			}
		});
		
		this.shower = new Runnable() {
			public void run() {
				final CountDownLatch shown = new CountDownLatch(1);
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						dialog.setVisible(true);
					}
				});	
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						shown.countDown();
					}
				});	
				try {
					shown.await();
				} catch(InterruptedException e) {
				}
			}
		};		
		this.parent = dialog.getContentPane();
	}
	public GuiInteraction(final JComponent parent) {
		this.parent = parent;
		this.shower = new Runnable() {
			public void run() {
				parent.validate();
			}
		};
	}

	protected JPanel getMessagePanel() {
		if(messagePanel == null) {
			messagePanel = new JPanel();
			messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.Y_AXIS));
			JPanel scrollPanel = new JPanel(new BorderLayout());
			scrollPanel.add(messagePanel, BorderLayout.NORTH);
			scrollPanel.add(new JLabel(" "), BorderLayout.CENTER);
			scrollPanel.setBackground(background);
			scrollPanel.setOpaque(true);
			scrollPane = new JScrollPane(scrollPanel);
			scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.getVerticalScrollBar().setUnitIncrement(20);
			scrollPane.getVerticalScrollBar().setBlockIncrement(400);
			scrollPane.setAlignmentX(JScrollPane.LEFT_ALIGNMENT);

			this.parent.add(scrollPane);
			this.currentSection = messagePanel;
			if(this.shower != null)
				this.shower.run();
		}
		return messagePanel;
	}

	protected JTextComponent createWrappedLabel(String text) {
		JTextArea label = new JTextArea(text);
		label.setEditable(false);
		label.setLineWrap(true);
		label.setWrapStyleWord(true);
		return label;
	}

	protected JTextComponent addMessage(String text) {
		JTextComponent label = createWrappedLabel(text);
		addMessageComponent(label);	
		return label;
	}

	protected void addMessageComponent(JComponent comp) {
		getMessagePanel();
		comp.setAlignmentX(Component.LEFT_ALIGNMENT);
		currentSection.add(comp);
		comp.requestFocus();
		scrollPane.validate();
		JScrollBar sb = scrollPane.getVerticalScrollBar();
		sb.setValue(sb.getMaximum());
	}

	public Future<String> getLineFuture(String fmt, Object ... args) {
		addMessage(format(fmt, args));
		return new StringPromptFuture(new JTextField());
	}

	public Future<String> getLineFutureDefault(String fmt, String defaultValue, Object... args) {
		addMessage(format(fmt, args));
		return new StringPromptFuture(new JTextField(defaultValue));
	}
	public Future<char[]> getPasswordFuture(String fmt, Object ... args) {
		addMessage(format(fmt, args));
		return new CharArrayPromptFuture(new JPasswordField(null));		
	}
	protected String format(String format, Object... args) {
		appendable.setLength(0);
		formatter.format(format, args);
		return appendable.toString();
	}
	/**
	 * @see simple.io.Interaction#printf(java.lang.String, java.lang.Object[])
	 */
	public void printf(String format, Object... args) {
		addMessage(format(format, args));
	}

	/**
	 * @see simple.io.Interaction#readLine(java.lang.String, java.lang.Object[])
	 */
	public String readLine(String fmt, Object... args) {
		try {
			return getLineFuture(fmt, args).get();
		} catch(InterruptedException e) {
			return null;
		} catch(ExecutionException e) {
			return null;
		}
	}
	
	/**
	 * @see simple.io.Interaction#readLine(java.lang.String, java.lang.Object[])
	 */
	public String readLineDefault(String fmt, String defaultValue, Object... args) {
		try {
			return getLineFuture(fmt, args).get();
		} catch(InterruptedException e) {
			return null;
		} catch(ExecutionException e) {
			return null;
		}
	}

	/**
	 * @see simple.io.Interaction#readPassword(java.lang.String, java.lang.Object[])
	 */
	public char[] readPassword(String fmt, Object... args) {
		try {
			return getPasswordFuture(fmt, args).get();
		} catch(InterruptedException e) {
			return null;
		} catch(ExecutionException e) {
			return null;
		}
	}
	
	/**
	 * @see simple.io.Interaction#getWriter()
	 */
	public PrintWriter getWriter() {
		return writer;
	}
	
	public ProgressListener createProgressMeter() {
		final JTextComponent textBox = addMessage("");
		return new ProgressListener() {			
			protected long start;
			public void progressUpdate(ProgressEvent event) {
				StringBuilder sb = new StringBuilder();
				sb.append(event.getStatusDescription()).append(':');
				if(event.getStatusProgress() >= 0)
					sb.append(' ').append(event.getStatusProgress());
				if(event.getStatusPercent() != null)
					sb.append(" (").append(event.getStatusPercent()).append(" %");
				textBox.setText(sb.toString());
			}			
			public void progressStart(ProgressEvent event) {
				start = System.currentTimeMillis();
				StringBuilder sb = new StringBuilder();
				sb.append(event.getStatusDescription()).append(':');
				if(event.getStatusProgress() >= 0)
					sb.append(' ').append(event.getStatusProgress());
				if(event.getStatusPercent() != null)
					sb.append(" (").append(event.getStatusPercent()).append(" %");
				textBox.setText(sb.toString());
			}			
			public void progressFinish(ProgressEvent event) {
				long end = System.currentTimeMillis();
				StringBuilder sb = new StringBuilder();
				sb.append(event.getStatusDescription()).append(": Complete [").append(end-start).append(" ms]");
				textBox.setText(sb.toString());
			}
		};
	}

	@Override
	public Future<File> browseForFile(final String caption,final String baseDir,final String fmt, final Object... args) {
		return getFileFuture(caption, baseDir, fmt, args);
	}
	
	public Future<File> getFileFuture(final String caption, final String baseDir, final String fmt, final Object... args) {
		addMessage(format(fmt, args));
		return new BrowseFilePromptFuture(caption, new File(baseDir), new JTextField(null));
	}
	
	public Future<File> getFileFuture(final String caption, final String baseDir, final FileSystemView view, final String fmt, final Object... args) {
		addMessage(format(fmt, args));
		return new BrowseFilePromptFuture(caption, new File(baseDir), new JTextField(null), view);
	}

	@Override
	public void clear() {
		getMessagePanel().removeAll();
	}

	protected JComponent doSectionStart(Section section) {
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());
		final JToggleButton header = new JToggleButton(section.desc); // Buttons always clip the text when too long; need to use a different component.
		header.setSelectedIcon(expandedIcon);
		header.setIcon(collapsedIcon);
		header.setSelected(true);
		if(section.depth > 1)
			header.setHorizontalAlignment(SwingConstants.LEFT);

		container.add(header, BorderLayout.NORTH);
		final JPanel detail = new JPanel();
		detail.setLayout(new BoxLayout(detail, BoxLayout.Y_AXIS));
		container.add(detail, BorderLayout.CENTER);
		header.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent e) {
				detail.setVisible(header.isSelected());
			}
		});
		addMessageComponent(container);
		currentSection = detail;
		
		return header;
	}

	protected void doSectionEnd(Section section, boolean success, String msg) {
		JToggleButton tb = ((JToggleButton) section.data);
		tb.setSelectedIcon(success ? completeIcon : failIcon);
		tb.setIcon(success ? completeIcon : failIcon);
		if(!StringUtils.isBlank(msg))
			tb.setText((StringUtils.isBlank(section.desc) ? "" : section.desc) + "...  " + msg);
		tb.setSelected(false);
		currentSection = (JComponent) section.data.getParent().getParent();
	}

	public File readFile(String caption, String baseDir, String fmt, Object... args) {
		try {
			return getFileFuture(caption, baseDir, fmt, args).get();
		} catch(InterruptedException e) {
			return null;
		} catch(ExecutionException e) {
			return null;
		}
	}

	public int[] readChoice(String fmt, String[] options, boolean multiple, int[] defaultChoices, Object... args) {
		try {
			return getChoiceFuture(fmt, options, multiple, defaultChoices, args).get();
		} catch(InterruptedException e) {
			return null;
		} catch(ExecutionException e) {
			return null;
		}
	}

	public Future<int[]> getChoiceFuture(String fmt, String[] options, boolean multiple, int[] defaultChoices, Object... args) {
		return new ChoiceFuture(format(fmt, args), options, multiple, defaultChoices);
	}

}
