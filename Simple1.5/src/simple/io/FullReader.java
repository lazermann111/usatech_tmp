/**
 *
 */
package simple.io;

import java.io.IOException;
import java.io.Reader;

/**
 * @author Brian S. Krug
 *
 */
public class FullReader extends Reader {
	protected final Reader delegate;
	
	public FullReader(Reader delegate){
		this.delegate=delegate;
	}

	@Override
	public void close() throws IOException {
		delegate.close();
	}


	@Override
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}


	@Override
	public int hashCode() {
		return delegate.hashCode();
	}


	@Override
	public void mark(int readAheadLimit) throws IOException {
		delegate.mark(readAheadLimit);
	}


	@Override
	public boolean markSupported() {
		return delegate.markSupported();
	}


	@Override
	public int read() throws IOException {
		return delegate.read();
	}


	@Override
	public boolean ready() throws IOException {
		return delegate.ready();
	}


	@Override
	public void reset() throws IOException {
		delegate.reset();
	}


	@Override
	public long skip(long n) throws IOException {
		return delegate.skip(n);
	}


	@Override
	public String toString() {
		return delegate.toString();
	}


	/**
	 * @see java.io.Reader#read(char[], int, int)
	 */
	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		int r;
		int tot = 0;
		while(tot < len && (r = delegate.read(cbuf, off + tot, len - tot)) >= 0) {
			tot += r;
		}
		return tot;
	}
}
