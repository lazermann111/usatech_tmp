package simple.io;

import java.io.IOException;
import java.io.OutputStream;

public abstract class AbstractBufferedOutputStream extends OutputStream {
	protected final byte[] buffer;
	protected int count;
	 
	protected AbstractBufferedOutputStream() {
		this(8192);
	}
	protected AbstractBufferedOutputStream(int size) {
		this.buffer = new byte[size];
	}
	@Override
	public void write(int b) throws IOException {
		if (count >= buffer.length) {
		    flushBuffer();
		}
		buffer[count++] = (byte)b;
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		if(len > buffer.length / 2) {
			flushBuffer();
			commit(b, off, len);
		} else {
			if(len > buffer.length - count)  
				flushBuffer();
			System.arraycopy(b, off, buffer, count, len);
			count += len;
		}
	}
	
	@Override
	public void flush() throws IOException {
		flushBuffer();
	}
	
	protected void flushBuffer() throws IOException {
        if (count > 0) {
        	commit(buffer, 0, count);
		    count = 0;
        }
    }
	
	protected abstract void commit(byte[] b, int off, int len) throws IOException ;
}
