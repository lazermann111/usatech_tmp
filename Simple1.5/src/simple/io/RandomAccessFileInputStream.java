package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

/** Implementation of InputStream around a RandomAccessFile.
 * This implementation is NOT thread-safe.
 *
 * @author Brian S. Krug
 *
 */
public class RandomAccessFileInputStream extends InputStream {
	protected final RandomAccessFile raf;
	protected long mark;
	protected long readlimit = -1;
	public RandomAccessFileInputStream(RandomAccessFile raf) {
		this.raf = raf;
	}
	@Override
	public int available() throws IOException {
		long available = raf.length() - raf.getFilePointer();
		if(available > Integer.MAX_VALUE) return Integer.MAX_VALUE;
		return (int)available;
	}
	@Override
	public void close() throws IOException {
		raf.close();
	}
	@Override
	public int hashCode() {
		return raf.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof RandomAccessFileInputStream
				&& raf.equals(((RandomAccessFileInputStream)obj).raf));
	}
	@Override
	public void mark(int readlimit) {
		try {
			mark = raf.getFilePointer();
		} catch(IOException e) {
			//throw new RuntimeException(e);
		}
		this.readlimit = readlimit;
	}
	@Override
	public boolean markSupported() {
		return true;
	}
	@Override
	public int read() throws IOException {
		return raf.read();
	}
	@Override
	public int read(byte[] b) throws IOException {
		return raf.read(b);
	}
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return raf.read(b, off, len);
	}
	@Override
	public void reset() throws IOException {
		raf.seek(mark);
	}
	@Override
	public long skip(long n) throws IOException {
		long skipped = Math.min(n, raf.length() - raf.getFilePointer());
		if(skipped > 0) raf.seek(raf.getFilePointer() + skipped);
		return skipped;
	}

}
