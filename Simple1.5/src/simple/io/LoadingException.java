/*
 * Created on Jan 19, 2005
 *
 */
package simple.io;

/**
 * @author bkrug
 *
 */
public class LoadingException extends Exception {
	private static final long serialVersionUID = 516161617L;

	/**
     * 
     */
    public LoadingException() {
        super();
    }

    /**
     * @param message
     */
    public LoadingException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public LoadingException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public LoadingException(String message, Throwable cause) {
        super(message, cause);
    }

}
