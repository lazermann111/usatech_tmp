/**
 *
 */
package simple.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Brian S. Krug
 *
 */
public interface BinaryStream {
	public InputStream getInputStream() throws IOException ;
	public long getLength() ;
	public long copyInto(ByteOutput output) throws IOException;
}
