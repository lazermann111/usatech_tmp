/*
 * Created on Mar 15, 2006
 *
 */
package simple.io;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;

/**
 * @author bkrug
 *
 */
public class ConcatenatingReader extends Reader {
    protected Reader[] readers;
    protected int index;
    /**
     *
     */
    public ConcatenatingReader(Reader[] readers) {
        super();
        this.readers = readers;
    }

    /**
     * @see java.io.Reader#read()
     */
    @Override
    public int read() throws IOException {
        if(index < readers.length) {
            int ret = readers[index].read();
            if(ret == -1) {
                index++;
                return read();
            }
        }
        return -1;
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        if(len == 0) return 0;
        /*int read = 0;
        while(read < len && index < readers.length) {
            int r = readers[index].read(cbuf, off + read, len - read);
            if(r == -1) index++;
            else read += r;
        }
        return read > 0 ? read : -1;
        */
        //This way (Only one read per reader) so that ready() is accurate
        int read = -1;
        while(index < readers.length && (read=readers[index].read(cbuf, off, len)) == -1)
        	index++;
        return read;
    }

    @Override
    public int read(CharBuffer target) throws IOException {
    	if(!target.hasRemaining()) return 0;
    	int read = -1;
        while(index < readers.length && (read=readers[index].read(target)) == -1)
        	index++;
        return read;
    }

    @Override
    public boolean ready() throws IOException {
    	return index < readers.length && readers[index].ready();
    }

    @Override
    public void close() throws IOException {
        for(int i = 0; i < readers.length; i++) {
        	readers[i].close();
        }
    }

    public int getCurrentIndex() {
        return index;
    }

    public void setCurrentIndex(int index) {
    	if(index < 0 || index >= readers.length)
    		throw new IndexOutOfBoundsException("Index '" + index + "' is not valid");
    	this.index = index;
    }

    @Override
    public long skip(long n) throws IOException {
    	if(n == 0) return 0;
        long skipped = 0;
        while(skipped < n && index < readers.length) {
            long sk = readers[index].skip(n - skipped);
            if(sk == -1) index++;
            else skipped += sk;
        }
        return skipped > 0 ? skipped : -1;
    }
}
