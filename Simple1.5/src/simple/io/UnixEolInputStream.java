package simple.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class UnixEolInputStream extends FilterInputStream {
	protected static final byte CR = (byte)'\r';
	protected static final byte LF = (byte)'\n';
	
	protected int prevByte = -1;
	public UnixEolInputStream(InputStream in) {
		super(in);
	}
	@Override
	public int read() throws IOException {
		int b = super.read();
		switch(b) {
			case LF:
				if(prevByte == CR) {
					prevByte = -1;
					return read();
				} else {
					prevByte = b;
					return LF;
				}
			case CR:
				if(prevByte == LF) {
					prevByte = -1;
					return read();
				} else {
					prevByte = b;
					return LF;
				}
			default:		
				prevByte = -1;
				return b;
		}
	}
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int r = super.read(b, off, len);
		if(r < 0)
			return r;
		for(int i = off; i < off + r; i++) {
			switch(b[i]) {
				case LF:
					if(prevByte == CR) {
						prevByte = -1;
						System.arraycopy(b, i + 1, b, i, r - i - 1);
						r--;
						i--;
					} else {
						prevByte = b[i];
					}
					break;		
				case CR:
					if(prevByte == LF) {
						prevByte = -1;
						System.arraycopy(b, i + 1, b, i, r - i - 1);
						r--;
						i--;
					} else {
						prevByte = b[i];
						b[i] = LF;
					}
					break;
				default:		
					prevByte = -1;
			}
		}
		return r;
	}
}
