/**
 *
 */
package simple.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Formatter;

/**
 * @author Brian S. Krug
 *
 */
public class InOutInteraction extends AbstractTextInteraction {
	protected final Formatter formatter;
	protected final BufferedReader reader;
	protected final PrintWriter writer;

	public InOutInteraction() {
		this(System.out, System.in);
	}
	public InOutInteraction(OutputStream out, InputStream in) {
		this(new PrintWriter(out), new BufferedReader(new InputStreamReader(in)));
	}
	public InOutInteraction(PrintStream out, InputStream in) {
		this(new PrintWriter(out), new BufferedReader(new InputStreamReader(in)));
	}

	public InOutInteraction(PrintWriter writer, BufferedReader reader) {
		super();
		this.formatter = new Formatter(writer);
		this.reader = reader;
		this.writer = writer;
	}

	/**
	 * @see simple.io.Interaction#printf(java.lang.String, java.lang.Object[])
	 */
	public void printf(String fmt, Object... args) {
		formatter.format(fmt, args);
		writer.flush();
	}

	/**
	 * @see simple.io.Interaction#readLine(java.lang.String, java.lang.Object[])
	 */
	public String readLine(String fmt, Object... args) {
		printf(fmt, args);
		try {
			return reader.readLine();
		} catch(IOException e) {
			return null;
		}
	}

	/**
	 * @see simple.io.Interaction#readPassword(java.lang.String, java.lang.Object[])
	 */
	public char[] readPassword(String fmt, Object... args) {
		printf(fmt, args);
		try {
			String s = reader.readLine();
			return s == null ? null : s.toCharArray();
		} catch(IOException e) {
			return null;
		}
	}

	/**
	 * @see simple.io.Interaction#getWriter()
	 */
	public PrintWriter getWriter() {
		return writer;
	}
}
