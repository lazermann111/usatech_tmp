package simple.io;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
/**
 * This class represents an InputStream to read one or more files in compressed data format.
 * Usage is as follows:
 * 	<pre>
 * 		FileInputStream file1=new FileInputStream("C:\\public\\temp\\test.csv");
 *   	FileInputStream file2=new FileInputStream("C:\\public\\temp\\test.html");
 *   	FileInputStream file3=new FileInputStream("C:\\public\\temp\\test.txt");
 *   	ZipEntry[] entryItems=new ZipEntry[3];
 *   	entryItems[0]=new ZipEntry("test.csv");
 *   	entryItems[1]=new ZipEntry("test.html");
 *   	entryItems[2]=new ZipEntry("test.txt");
 *   	FileInputStream[] inputStreams=new FileInputStream[3];
 *   	inputStreams[0]=file1;
 *   	inputStreams[1]=file2;
 *   	inputStreams[2]=file3;
 *   	ZippingInputStream zIsArray=new ZippingInputStream(entryItems, inputStreams);
 *   	IOUtils.copy(zIsArray, new FileOutputStream("C:\\public\\temp\\testArray.zip"));
 *   
 *   	or you can use addEntryItem to add a ZipEntry with its InputStream.
 *   	
 *   	FileInputStream singleFile=new FileInputStream("C:\\public\\temp\\test.csv");
 *   	ZippingInputStream zIs=new ZippingInputStream();
 *   	zIs.addEntryItem(new ZipEntry("test.html"), singleFile);
 *   	IOUtils.copy(zIs, new FileOutputStream("C:\\public\\temp\\test.zip"));
 *   	
 *   </pre>
 * @author yhe
 *
 */
public class ZippingInputStream extends InputStream {
	
	protected static final long LOCSIG = 0x04034b50L;	// "PK\003\004"
	protected static final long EXTSIG = 0x08074b50L;	// "PK\007\008"
	protected static final long CENSIG = 0x02014b50L;	// "PK\001\002"
	protected static final long ENDSIG = 0x06054b50L;	// "PK\005\006"
	protected static final int VERSION = 20;	
	protected static final int FLAG=8;
	protected static final int METHOD=ZipEntry.DEFLATED;
	
	protected ArrayList<EntryItem> entryItems;
	protected long totalSizeSoFar=0;
    /** Temporary read buffer. */
    private byte[] rbuf = new byte[1];
    protected boolean reachEnd;
    protected long dosTime; //dos time
    protected int currentIndex=0;
	protected boolean cenFinished=false;
	protected ByteArrayBuffer finalFooterBuffer=new ByteArrayBuffer();

    public ZippingInputStream(ZipEntry[] _entry, InputStream[] _in) {
    	this();
        if (_entry.length != _in.length)
            throw new IllegalArgumentException("ZipEntry size is different from InputStream size. It is illegal.");
        for(int i=0; i<_entry.length; i++){
        	entryItems.add(new EntryItem(_entry[i], _in[i]));
        }
    }
    
    public ZippingInputStream(ZipEntry _entry, InputStream _in) {
    	this();
        entryItems.add(new EntryItem(_entry, _in));
    }
    
    public ZippingInputStream() {
    	dosTime=javaToDosTime(System.currentTimeMillis());
    	entryItems=new ArrayList<EntryItem>();
    }
    
    public void addEntryItem(ZipEntry _entry, InputStream _in) throws IllegalArgumentException{
    	if(cenFinished){
    		throw new IllegalArgumentException("Zip central directory (CEN) header is written. It is illegal to add entry item now.");
    	}
    	entryItems.add(new EntryItem(_entry, _in));
    }

    public void close() throws IOException {
    	for(int i=0; i<entryItems.size(); i++){
        	entryItems.get(i).close();
        }
    }

    public int read() throws IOException {
        // Read a single byte of compressed data
        int len = read(rbuf, 0, 1);
        if (len <= 0)
            return -1;
        return (rbuf[0] & 0xFF);
    }
    /**
     * Inner class that facilitates the reading of header and footer and final footer portion of the zip.
     * @author yhe
     *
     */
    protected class ByteArrayBuffer{
    	int bufferOffset;
    	byte[] buffer;
    	public ByteArrayBuffer(){
    		bufferOffset=0;
    	}
    	public void setBuffer(byte[] buffer){
    		this.buffer=buffer;
    	}
    	
    	public byte[] getBuffer(){
    		return buffer;
    	}
    	
    	public void setBufferOffset(int bufferOffset){
    		this.bufferOffset=bufferOffset;
    	}
    	
    	public int getBufferOffset(){
    		return bufferOffset;
    	}
    	
    	public void addBufferOffset(int len){
    		this.bufferOffset+=len;
    	}
    	public int getUnreadLen(){
    		return buffer.length-bufferOffset;
    	}
    	public boolean bufferFinished(){
    		if(buffer==null){
    			return false;
    		}
    		else{
    			return bufferOffset==buffer.length;
    		}
    	}
    	
    }
    /**
     * Inner class that represents a ZipEntry with its InputStream.
     * @author yhe
     *
     */
    protected class EntryItem{
    	private void ensureOpen() throws IOException {
            if (in == null) {
                throw new IOException("Stream closed");
            }
        }
    	public void close() throws IOException {
            if (in != null) {
                try {
                    in.close();
                } finally {
                    in = null;
                }
            }
        }
    	protected ZipEntry entry;
    	protected long entryOffset;
    	protected DeflaterInputStream in;
    	protected ByteArrayBuffer headerBuffer=new ByteArrayBuffer();
    	protected ByteArrayBuffer footerBuffer=new ByteArrayBuffer();
    	
    	public EntryItem(ZipEntry _entry, InputStream _in){
    		// Sanity checks
            if (_entry == null)
                throw new NullPointerException("Null ZipEntry.");
            if (_in == null)
                throw new NullPointerException("Null inputstream.");

            // Initialize
            in= new DeflaterInputStream(_in, new Deflater(Deflater.DEFAULT_COMPRESSION, true));
            entry=_entry;
    	}
    	public ZipEntry getEntry(){
    		return entry;
    	}
    	public long getEntryOffset(){
    		return entryOffset;
    	}
    	public int read(byte[] b, int off, int len) throws IOException {
            ensureOpen();
            if (b == null) {
                throw new NullPointerException("Null buffer for read.");
            } else if (off < 0 || len < 0 || len > b.length - off) {
                throw new IndexOutOfBoundsException();
            } else if (len == 0) {
                return 0;
            }
            int cnt = 0;
            if(!headerBuffer.bufferFinished()){
    	        if(headerBuffer.getBuffer()==null){
    	        	//read the header out
    	        	entryOffset=totalSizeSoFar;
    	        	headerBuffer.setBuffer(writeLOC(entry));
    	        }
    	        while(len>0){
    		        int readHeaderLen=readBuffer(headerBuffer, b, off, len);
    		        if(readHeaderLen>0){
    		        	cnt +=readHeaderLen;
    		        	off += readHeaderLen;
    		        	len-=readHeaderLen;
    		        }
    		        else{
    		        	break;
    		        }
    	        }
            }
            while (len > 0&&in.available()==1){
    	            int n=in.read(b, off, len);
    	            if(n>0){
    		            cnt += n;
    		            off += n;
    		            len -= n;
    	            }
    	            else
    	            {
    	            	break;
    	            }
            }
            
            while(len>0&&!footerBuffer.bufferFinished()){
                if(footerBuffer.getBuffer()==null){
                	entry.setSize(in.getBytesRead());
                	entry.setCompressedSize(in.getBytesWritten());
                	entry.setCrc(in.getCrcValue());
                	footerBuffer.setBuffer(writeEXT(entry)) ;
    	        	totalSizeSoFar += entry.getCompressedSize();
    	        }
	    	    int readFooterLen=readBuffer(footerBuffer, b, off, len);
	    		if(readFooterLen>0){
	    			cnt+=readFooterLen;
	    			off+=readFooterLen;
	    			len-=readFooterLen;
	    		}
	    		else{
	    		    break;
	    		}
            }
            
            if (cnt == 0 && footerBuffer.bufferFinished()) {
            	currentIndex++;
            	cnt=-1;
            }
            return cnt;
        }
    	/**
    	 * Logic taken from ZipOutputStream.java 
    	 * @param e
    	 * @return
    	 * @throws IOException
    	 */
    	public byte[] writeLOC(ZipEntry e) throws IOException {
    		ByteArrayOutputStream headerOut=new ByteArrayOutputStream();
        	writeInt(headerOut, LOCSIG);	    // LOC header signature
        	writeShort(headerOut,VERSION);      // VERSION needed to extract
        	writeShort(headerOut,FLAG);         // general purpose bit FLAG
        	writeShort(headerOut,METHOD);       // compression METHOD
        	writeInt(headerOut,dosTime);           // last modification time
        	writeInt(headerOut,0);
        	writeInt(headerOut,0);
        	writeInt(headerOut,0);
        	byte[] nameBytes = getUTF8Bytes(e.getName());
        	writeShort(headerOut,nameBytes.length);
        	byte[] extra=e.getExtra();
        	writeShort(headerOut,extra != null ? extra.length : 0);
        	writeBytes(headerOut,nameBytes, 0, nameBytes.length);
        	if (extra != null) {
        	    writeBytes(headerOut,extra, 0, extra.length);
        	}
        	return headerOut.toByteArray();
        }
    	/*
    	 * * Logic taken from ZipOutputStream.java 
         * Writes extra data descriptor (EXT) for specified entry.
         */
        public byte[] writeEXT(ZipEntry e) throws IOException {
        	ByteArrayOutputStream footerOut=new ByteArrayOutputStream();
    		writeInt(footerOut, EXTSIG);	    // EXT header signature
    		writeInt(footerOut,e.getCrc());	    // crc-32
    		writeInt(footerOut,e.getCompressedSize());	    // compressed size
    		writeInt(footerOut,e.getSize());	    // uncompressed size
    		return footerOut.toByteArray();
        }
    }
    /**
     * Reads compressed data into a byte array.
     *
     * @param b buffer into which the data is read
     * @param off starting offset of the data within {@code b}
     * @param len maximum number of compressed bytes to read into {@code b}
     * @return the actual number of bytes read, or -1 if the end of the
     * uncompressed input stream is reached
     * @throws IndexOutOfBoundsException  if {@code len} > {@code b.length -
     * off}
     * @throws IOException if an I/O error occurs or if this input stream is
     * already closed
     */
    public int read(byte[] b, int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException("Null buffer for read");
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }
        int cnt = 0;
        //read all entryItems
        while (len > 0&&currentIndex<entryItems.size()){
	            int n=entryItems.get(currentIndex).read(b, off, len);
	            if(n>0){
		            cnt += n;
		            off += n;
		            len -= n;
	            }
        }
        
        while(len>0&&!finalFooterBuffer.bufferFinished()){
            if(finalFooterBuffer.getBuffer()==null){
            	ByteArrayOutputStream finalOut= new ByteArrayOutputStream();
	        	long offsetCEN = totalSizeSoFar;
	        	for(int i=0; i<entryItems.size(); i++){
	        	    writeCEN(finalOut, entryItems.get(i).getEntry(), entryItems.get(i).getEntryOffset());
	        	}
	        	writeEND(finalOut,offsetCEN, totalSizeSoFar-offsetCEN);
	        	finalFooterBuffer.setBuffer(finalOut.toByteArray());
	        }
           
	        int readFinalLen=readBuffer(finalFooterBuffer, b, off, len);
		    if(readFinalLen>0){
			    cnt+=readFinalLen;
			    off+=readFinalLen;
			    len-=readFinalLen;
		    }
		    else{
		        break;
		    }
        }
        
        if (cnt == 0 && finalFooterBuffer.bufferFinished()) {
        	reachEnd = true;
        	cnt=-1;
        }
        return cnt;
    }

    public int readBuffer(ByteArrayBuffer buffer, byte[] b, int off, int len){
    	if(buffer.bufferFinished()){
    		return -1;
    	}
    	else {
    		int unreadLen=buffer.getUnreadLen();
    		if(unreadLen>=len){
	    		System.arraycopy(buffer.getBuffer(), buffer.getBufferOffset(), b, off, len);
	    		buffer.addBufferOffset(len);
	    		return len;
	    	}
	    	else{
	    		System.arraycopy(buffer.getBuffer(), buffer.getBufferOffset(), b, off, unreadLen);
	    		buffer.addBufferOffset(unreadLen);
	    		return unreadLen;
	    	}
    	}
    }
    /**
     * Logic taken from ZipOutputStream.java 
     * @param finalOut
     * @param e
     * @param entryOffset
     * @throws IOException
     */
    protected void writeCEN(ByteArrayOutputStream finalOut, ZipEntry e, long entryOffset) throws IOException {
    	writeInt(finalOut,CENSIG);	    // CEN header signature
    	writeShort(finalOut,VERSION);	    // VERSION made by
    	writeShort(finalOut,VERSION);	    // VERSION needed to extract
    	writeShort(finalOut,FLAG);	    // general purpose bit FLAG
    	writeShort(finalOut,METHOD);	    // compression METHOD
    	writeInt(finalOut,dosTime);	    // last modification time
    	writeInt(finalOut,e.getCrc());	    // crc-32
    	writeInt(finalOut,e.getCompressedSize());	    // compressed size
    	writeInt(finalOut,e.getSize());	    // uncompressed size
    	byte[] nameBytes = getUTF8Bytes(e.getName());
    	writeShort(finalOut,nameBytes.length);
    	byte[] extra=e.getExtra();
    	writeShort(finalOut, extra != null ? extra.length : 0);
    	byte[] commentBytes;
    	if (e.getComment() != null) {
    	    commentBytes = getUTF8Bytes(e.getComment());
    	    writeShort(finalOut,commentBytes.length);
    	} else {
    	    commentBytes = null;
    	    writeShort(finalOut,0);
    	}
    	writeShort(finalOut,0);		    // starting disk number
    	writeShort(finalOut,0);		    // internal file attributes (unused)
    	writeInt(finalOut,0);		    // external file attributes (unused)
    	writeInt(finalOut,entryOffset);	    // relative offset of local header
    	writeBytes(finalOut,nameBytes, 0, nameBytes.length);
    	if (extra != null) {
    	    writeBytes(finalOut,extra, 0, extra.length);
    	}
    	if (commentBytes != null) {
    	    writeBytes(finalOut,commentBytes, 0, commentBytes.length);
    	}
    	cenFinished=true;
    }
    
    /*
     * Logic taken from ZipOutputStream.java 
     * Writes end of central directory (END) header.
     */
    protected void writeEND(ByteArrayOutputStream finalOut, long off, long len) throws IOException {
		writeInt(finalOut,ENDSIG);	    // END record signature
		writeShort(finalOut,0);		    // number of this disk
		writeShort(finalOut,0);		    // central directory start disk
		writeShort(finalOut,entryItems.size()); // number of directory entries on disk
		writeShort(finalOut,entryItems.size()); // total number of directory entries
		writeInt(finalOut,len);		    // length of central directory
		writeInt(finalOut,off);		    // offset of central directory
		writeShort(finalOut,0);// zip file comment
    }
    
    /*
     * Logic taken from ZipOutputStream.java 
     * Writes a 16-bit short to the output stream in little-endian byte order.
     */
    protected void writeShort(OutputStream out, int v) throws IOException {
		out.write((v >>> 0) & 0xff);
		out.write((v >>> 8) & 0xff);
		totalSizeSoFar+=2;
    }

    /*
     * Logic taken from ZipOutputStream.java 
     * Writes a 32-bit int to the output stream in little-endian byte order.
     */
    protected void writeInt(OutputStream out, long v) throws IOException {
		out.write((int)((v >>>  0) & 0xff));
		out.write((int)((v >>>  8) & 0xff));
		out.write((int)((v >>> 16) & 0xff));
		out.write((int)((v >>> 24) & 0xff));
		totalSizeSoFar+=4;
    }

    /*
     * Logic taken from ZipOutputStream.java 
     * Writes an array of bytes to the output stream.
     */
    protected void writeBytes(OutputStream out,byte[] b, int off, int len) throws IOException {
    	out.write(b, off, len);
    	totalSizeSoFar+=len;
    }

    /**
     * Returns 0 after EOF has been reached, otherwise always return 1.
     * <p>
     * Programs should not count on this METHOD to return the actual number
     * of bytes that could be read without blocking
     * @return zero after the end of the underlying input stream has been
     * reached, otherwise always returns 1
     * @throws IOException if an I/O error occurs or if this stream is
     * already closed
     */
    public int available() throws IOException {
        if (reachEnd) {
            return 0;
        }
        return 1;
    }

    /**
     * Always returns {@code false} because this input stream does not support
     * the {@link #mark mark()} and {@link #reset reset()} METHODs.
     *
     * @return false, always
     */
    public boolean markSupported() {
        return false;
    }

    /**
     * <i>This operation is not supported</i>.
     *
     * @param limit maximum bytes that can be read before invalidating the position marker
     */
    public void mark(int limit) {
        // Operation not supported
    }

    /**
     * <i>This operation is not supported</i>.
     *
     * @throws IOException always thrown
     */
    public void reset() throws IOException {
        throw new IOException("mark/reset not supported");
    }
    /**
     * Method taken from JDK ZipEntry.java original a private method
     * @param time
     * @return
     */
    protected static long javaToDosTime(long time) {
    	Date d = new Date(time);
    	int year = d.getYear() + 1900;
    	if (year < 1980) {
    	    return (1 << 21) | (1 << 16);
    	}
    	return (year - 1980) << 25 | (d.getMonth() + 1) << 21 |
                   d.getDate() << 16 | d.getHours() << 11 | d.getMinutes() << 5 |
                   d.getSeconds() >> 1;
    }
    /**
     * Method taken from JDK ZipOutputStream.java original a private method
     * @param s
     * @return
     */
    protected static byte[] getUTF8Bytes(String s) {
		char[] c = s.toCharArray();
		int len = c.length;
		// Count the number of encoded bytes...
		int count = 0;
		for (int i = 0; i < len; i++) {
		    int ch = c[i];
		    if (ch <= 0x7f) {
			count++;
		    } else if (ch <= 0x7ff) {
			count += 2;
		    } else {
			count += 3;
		    }
		}
		// Now return the encoded bytes...
		byte[] b = new byte[count];
		int off = 0;
		for (int i = 0; i < len; i++) {
		    int ch = c[i];
		    if (ch <= 0x7f) {
			b[off++] = (byte)ch;
		    } else if (ch <= 0x7ff) {
			b[off++] = (byte)((ch >> 6) | 0xc0);
			b[off++] = (byte)((ch & 0x3f) | 0x80);
		    } else {
			b[off++] = (byte)((ch >> 12) | 0xe0);
			b[off++] = (byte)(((ch >> 6) & 0x3f) | 0x80);
			b[off++] = (byte)((ch & 0x3f) | 0x80);
		    }
		}
		return b;
    }
    
    public static void main(String args[]) throws Exception{
    	FileInputStream file1=new FileInputStream("C:\\public\\temp\\test.csv");
    	FileInputStream file2=new FileInputStream("C:\\public\\temp\\test.html");
    	FileInputStream file3=new FileInputStream("C:\\public\\temp\\test.txt");
    	ZipEntry[] entryItems=new ZipEntry[3];
    	entryItems[0]=new ZipEntry("test.csv");
    	entryItems[1]=new ZipEntry("test.html");
    	entryItems[2]=new ZipEntry("test.txt");
    	FileInputStream[] inputStreams=new FileInputStream[3];
    	inputStreams[0]=file1;
    	inputStreams[1]=file2;
    	inputStreams[2]=file3;
    	
    	FileInputStream singleFile=new FileInputStream("C:\\public\\temp\\test.csv");
    	ZippingInputStream zIs=new ZippingInputStream();
    	zIs.addEntryItem(new ZipEntry("test.html"), singleFile);
    	IOUtils.copy(zIs, new FileOutputStream("C:\\public\\temp\\test.zip"));
    	
    	//use array
    	ZippingInputStream zIsArray=new ZippingInputStream(entryItems, inputStreams);
    	IOUtils.copy(zIsArray, new FileOutputStream("C:\\public\\temp\\testArray.zip"));
    	
    	FileInputStream afile2=new FileInputStream("C:\\public\\temp\\test.html");
    	ZipOutputStream zipOut =
	         new ZipOutputStream(new FileOutputStream("C:\\public\\temp\\test2.zip"));
		zipOut.putNextEntry(new ZipEntry("test.html"));
		IOUtils.copy(afile2, zipOut);
		zipOut.closeEntry();
		zipOut.close();
    }
}
