package simple.io;

import java.io.IOException;
import java.io.InputStream;

public class StartAtPrefixInputStream extends InputStream {
	protected final InputStream delegate;
	protected byte[] buffer;
	protected int bufferPos = -1;
	protected int bufferEndPos = -1;

	public StartAtPrefixInputStream(InputStream delegate, String prefix) throws IOException {
		this(delegate,prefix.getBytes());
	}
	public StartAtPrefixInputStream(InputStream delegate, byte[] prefixBytes) throws IOException {
		super();
		this.delegate = delegate;
		buffer = new byte[prefixBytes.length + 256];
		if(delegate.markSupported()) {
			delegate.mark(buffer.length);
			int offset = 0;
			while(true) {
				int len = delegate.read(buffer, offset, buffer.length);
				if(len == -1)
					throw new IOException("Prefix '" + new String(prefixBytes) + "' not found in stream");
				int pos = IOUtils.indexOf(buffer, 0, offset + len, prefixBytes, 0, prefixBytes.length);
				if(pos != -1) {
					delegate.reset();
					delegate.skip(pos);
					break;
				} else if(offset + len > prefixBytes.length) {
					delegate.reset();
					delegate.skip(offset + len - prefixBytes.length);
					delegate.mark(buffer.length);
					offset = 0;
				} else {
					offset += len;
				}
			}
		} else {
			int offset = 0;
			while(true) {
				int len = delegate.read(buffer, offset, buffer.length - offset);
				if(len == -1)
					throw new IOException("Prefix '" + new String(prefixBytes) + "' not found in stream");
				int pos = IOUtils.indexOf(buffer, 0, offset + len, prefixBytes, 0, prefixBytes.length);
				if(pos != -1) {
					bufferPos = pos;
					bufferEndPos = offset + len;
					break;
				} else if(offset + len > prefixBytes.length) {
					System.arraycopy(buffer, offset + len - prefixBytes.length, buffer, 0, prefixBytes.length);
					offset = prefixBytes.length;
				} else {
					offset = len;
				}
			}
		}
	}

	@Override
	public int read() throws IOException {
		if(bufferPos < bufferEndPos) {
			return buffer[bufferPos++];
		}
		return delegate.read();
	}

	@Override
	public int available() throws IOException {
		return bufferEndPos - bufferPos + delegate.available();
	}

	@Override
	public void close() throws IOException {
		delegate.close();
	}

	@Override
	public void mark(int readlimit) {
		delegate.mark(readlimit);
	}

	@Override
	public boolean markSupported() {
		return delegate.markSupported();
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int fromBuffer;
		if(bufferPos < bufferEndPos) {
			fromBuffer = Math.min(bufferEndPos - bufferPos, len);
			System.arraycopy(buffer, bufferPos, b, off, fromBuffer);
			bufferPos += fromBuffer;
			if(fromBuffer < len) {
				off += fromBuffer;
				len -= fromBuffer;
			} else {
				return fromBuffer;
			}
		} else {
			fromBuffer = 0;
		}
		int fromStream = delegate.read(b, off, len);
		if(fromBuffer == 0)
			return fromStream;
		if(fromStream < 0)
			return fromBuffer;
		return fromBuffer + fromStream;
	}

	@Override
	public void reset() throws IOException {
		delegate.reset();
	}

	@Override
	public long skip(long n) throws IOException {
		return delegate.skip(n);
	}
}
