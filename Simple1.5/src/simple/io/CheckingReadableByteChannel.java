package simple.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.atomic.AtomicLong;

public class CheckingReadableByteChannel implements ReadableByteChannel {
	protected final ReadableByteChannel delegate;
	protected final AtomicLong count = new AtomicLong();
	protected final ByteChecker checker;

	public CheckingReadableByteChannel(ReadableByteChannel delegate, ByteChecker checker) {
		this.delegate = delegate;
		this.checker = checker;
	}

	public void close() throws IOException {
		delegate.close();
	}

	public int read(ByteBuffer dst) throws IOException {
		int n = delegate.read(dst);
		if(n > 0)
			count.addAndGet(n);
		if(n > 0) {
			int limit = dst.limit();
			int pos = dst.position();
			dst.position(pos - n);
			dst.limit(pos);
			while(dst.hasRemaining())
				update(dst);
			dst.limit(limit);
			dst.position(pos);
		}
		return n;
	}

	@Override
	public boolean isOpen() {
		return delegate.isOpen();
	}

	protected void update(ByteBuffer src) {
		checker.update(src);
	}

	public long getCount() {
		return count.longValue();
	}

	public Object getCheckValue() {
		return checker.getCheckValue();
	}

	public void reset() {
		count.set(0L);
		checker.reset();
	}
}
