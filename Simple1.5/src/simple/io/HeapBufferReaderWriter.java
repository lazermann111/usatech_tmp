package simple.io;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;

/**
 * This class loads into a char buffer from its Writer and reads from the same char buffer using its Reader.
 * WARNING: this class is not thread-safe. If you access it from more than one thread you must synchronize calls made against
 * this class.
 * 
 * @author Brian S. Krug
 */
public class HeapBufferReaderWriter implements BufferReaderWriter {
	protected char[] buffer;
	protected int count = 0;
	protected final int blockSize;
	protected BufferReader in;
	protected final Writer out = new BufferWriter();
	protected final boolean resettable;
	protected int disgarded = 0;

	public HeapBufferReaderWriter(boolean resettable) {
		this(1024, 32, resettable);
	}

    public HeapBufferReaderWriter() {
		this(1024, 32, false);
    }

    public HeapBufferReaderWriter(int blockSize, int capacity) {
		this(blockSize, capacity, false);
    }
    /** Creates a new instance of BufferStream
     * @param blockSize The number of bytes read before that section of the buffer is disgarded
     * @param capacity The initial capacity of the buffer
     */
	public HeapBufferReaderWriter(int blockSize, int capacity, boolean resettable) {
        this.blockSize = blockSize;
		buffer = new char[capacity];
		this.resettable = resettable;
		if(!resettable)
			in = new BufferReader();
    }

    /**
     * @see simple.io.BinaryStream#getLength()
     */
    public long getLength() {
    	return size();
    }

    public int size() {
    	return count;
    }

	/**
	 * Returns an Reader to access the data written into the Writer
	 * 
	 * @return The Reader
	 * @see simple.io.BinaryStream#getReader()
	 */
	public Reader getReader() {
		if(resettable)
			return new BufferReader();
        return in;
    }

	/**
	 * Returns an Writer to write the data access by the Reader
	 * 
	 * @return The Writer
	 */
	public Writer getWriter() {
		return out;
    }

    public void clear() {
		count = 0;
		disgarded = 0;
		if(!resettable)
			in.pos = 0;
    }

	/**
	 * Copies bytes from this BufferReaderWriter to the CharBuffer. The number of bytes copied is return and
	 * will be the smaller of charBuffer.remaining() or (BufferReaderWriter.size() - offset)
	 * 
	 * @param bb
	 * @param offset
	 *            The offset in the BufferReaderWriter from which to start copying bytes
	 * @return
	 */
	public int putInto(CharBuffer charBuffer, int offset) {
		int n = Math.min(charBuffer.remaining(), count - offset);
		charBuffer.put(buffer, offset, n);
    	return n;
    }

	public long copyInto(Writer output) throws IOException {
		output.write(buffer, 0, count);
		return count;
	}

    protected void ensureCapacity(int additional) {
		if(additional + count - disgarded > buffer.length) {
			if(!resettable && blockSize > 0 && in.pos - disgarded > blockSize) {
				System.arraycopy(buffer, in.pos - disgarded, buffer, 0, count - in.pos);
				disgarded += in.pos;
				if(additional + count - disgarded <= buffer.length)
					return;
			}

			int newlen = Math.max(buffer.length << 1, additional + count - disgarded);
			char tmp[] = new char[newlen];
			System.arraycopy(buffer, 0, tmp, 0, count - disgarded);
	    	buffer = tmp;
		}
    }

	protected class BufferReader extends Reader {
		protected int pos = disgarded;
		protected int mark;
		protected boolean done() {
			return pos >= count;
		}

		public int available() {
            return count - pos;
        }

        @Override
		public void mark(int readlimit) {
			mark = pos;
        }

        @Override
		public boolean markSupported() {
			return resettable;
        }

        @Override
		public void reset() throws IOException {
			if(!resettable)
				throw new IOException("mark/reset not supported for non-resettable BufferStream");
			pos = mark;
        }

        @Override
		public int read() throws IOException {
            if(done()) return -1;
			char b = buffer[pos++ - disgarded];
            return b & 0xFF;
        }

        @Override
		public int read(char[] b, int off, int len) throws IOException {
            if (off < 0 || len < 0  /*||off > count || off + len > count */) {
                throw new IndexOutOfBoundsException();
            } else if (len == 0) {
                return 0;
            }

            if (done()) return -1;
            if (pos + len > count) len = count - pos;
            if (len <= 0) return 0;
			System.arraycopy(buffer, pos - disgarded, b, off, len);
            pos += len;
            return len;
        }

        @Override
		public long skip(long n) throws IOException {
            if (pos + n > count) n = count - pos;
            if (n < 0)  return 0;
            pos += n;
            return n;
        }

		@Override
		public void close() throws IOException {
		}
    }

	protected class BufferWriter extends Writer {
        @Override
		public void write(int b) throws IOException {
            ensureCapacity(1);
			buffer[count++ - disgarded] = (char) b;
        }

        @Override
		public void write(char[] b, int off, int len) throws IOException {
            if ((off < 0) || (off > b.length) || (len < 0) ||
            ((off + len) > b.length) || ((off + len) < 0)) {
                throw new IndexOutOfBoundsException();
            } else if (len == 0) {
                return;
            }
            ensureCapacity(len);
			System.arraycopy(b, off, buffer, count - disgarded, len);
            count += len;
        }

		@Override
		public void flush() throws IOException {
		}

		@Override
		public void close() throws IOException {
		}
    }
}
