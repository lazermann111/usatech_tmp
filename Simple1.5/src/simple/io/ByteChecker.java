package simple.io;

import java.nio.ByteBuffer;

public interface ByteChecker<V> {
	public void update(ByteBuffer src);

	public void update(byte[] bytes, int offset, int len);

	public void reset();

	public V getCheckValue();
}
