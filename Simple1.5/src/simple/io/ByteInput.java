package simple.io;

import java.io.IOException;
import java.io.OutputStream;

/** Interface for reading data
 * @author Brian S. Krug
 *
 */
public interface ByteInput {
	/** Whether or not this ByteInput may be reset by calling the {@link #reset()} method.
	 * @return <code>true</code>, if and only if this implementation supports calls to reset()
	 */
	public boolean isResettable() ;
	/** Read the specified number of bytes and into the provided buffer. Reads at most <code>length</code> bytes.
	 * @param buffer
	 * @param offset
	 * @param length
	 * @return The number of bytes read
	 * @throws IOException
	 */
	public void readBytes(byte[] buffer, int offset, int length) throws IOException;

	/** Read the specified number of bytes and return it as a String. Reads <code>length</code> bytes.
	 * @param length
	 * @return A String containing those bytes read
	 * @throws IOException
	 */
	public String readBytes(int length) throws IOException;

	/** Read the specified number of chars (2 bytes) and return it as a String. Reads <code>length * 2</code> bytes.
	 * @param length
	 * @return A String containing those chars read
	 * @throws IOException
	 */
	public String readChars(int length) throws IOException;

	/** Read 2 bytes and convert to a char. Reads 2 bytes.
	 * @return A char representing the next two bytes
	 * @throws IOException
	 */
	public char readChar() throws IOException;

	/** Read 1 byte and return it. Reads 1 bytes.
	 * @return The next byte
	 * @throws IOException
	 */
	public byte readByte() throws IOException;

	/** Read 1 byte and return it unsigned as an int. Reads 1 bytes.
	 * @return The next byte unsigned as an int
	 * @throws IOException
	 */
	public int readUnsignedByte() throws IOException;

	/** Read 2 bytes and return them as a short. Reads 2 bytes.
	 * @return The next two bytes as a short
	 * @throws IOException
	 */
	public short readShort() throws IOException;

	/** Read 2 bytes and return them unsigned as an int. Reads 2 bytes.
	 * @return The next two bytes unsigned as an int
	 * @throws IOException
	 */
	public int readUnsignedShort() throws IOException;

	/** Read 1 byte and return it unsigned as a char. Reads 1 bytes.
	 * @return The next byte unsigned as a char
	 * @throws IOException
	 */
	public char readByteAsChar() throws IOException;

	/** Read 4 bytes and return them as an int. Reads 4 bytes.
	 * @return The next four bytes as an int
	 * @throws IOException
	 */
	public int readInt() throws IOException;

	/** Read 4 bytes and return them unsigned as a long. Reads 4 bytes.
	 * @return The next four bytes unsigned as a long
	 * @throws IOException
	 */
	public long readUnsignedInt() throws IOException;

	/** Read 8 bytes and return them as a long. Reads 8 bytes.
	 * @return The next eight bytes as a long
	 * @throws IOException
	 */
	public long readLong() throws IOException;

	/** Read 4 bytes and return them as a float using Float.intBitsToFloat() method. Reads 4 bytes.
	 * @return The next four bytes as a float
	 * @throws IOException
	 */
	public float readFloat() throws IOException;

	/** Read 8 bytes and return them as a double using Double.longBitsToDouble() method. Reads 8 bytes.
	 * @return The next eight bytes as a double
	 * @throws IOException
	 */
	public double readDouble() throws IOException;

	/** Returns the length of the input stream from the first byte to the last
	 * @return Number of bytes that can be read
	 * @throws IOException
	 */
	public int length() throws IOException;

	/** Sets the position of the input back to the first byte.
	 * @throws IOException
	 */
	public void reset() throws IOException;

	/** Returns the number of bytes read thus far.
	 * @return
	 */
	public int position();

	/** Copies the bytes from the input to the output
	 * @param output
	 * @return The total number of bytes copied
	 * @throws IOException
	 */
	public int copyInto(OutputStream output) throws IOException ;

	/**
	 * Copies the bytes from the input to the output
	 * 
	 * @param output
	 * @return The total number of bytes copied
	 * @throws IOException
	 */
	public int copyInto(ByteOutput output) throws IOException;
}