package simple.io;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DigestByteChecker implements ByteChecker<byte[]> {
	protected final MessageDigest messageDigest;
	protected byte[] value;

	public DigestByteChecker(String algorithm) throws NoSuchAlgorithmException {
		this(MessageDigest.getInstance(algorithm));
	}

	public DigestByteChecker(MessageDigest messageDigest) {
		this.messageDigest = messageDigest;
	}

	@Override
	public void update(ByteBuffer buffer) {
		messageDigest.update(buffer);
	}

	@Override
	public void update(byte[] bytes, int offset, int len) {
		messageDigest.update(bytes, offset, len);
	}

	@Override
	public void reset() {
		messageDigest.reset();
		value = null;
	}

	@Override
	public byte[] getCheckValue() {
		if(value == null)
			value = messageDigest.digest();
		return value;
	}
}
