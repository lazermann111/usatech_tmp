package simple.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

import simple.text.StringUtils;

public class TLVParser {
	public static final Pattern TRACK2_PATTERN = Pattern.compile("^;?([0-9]+(?:=[0-9]*)*)(?:\\?(?:[\\x00-\\xFF])?)?\\x00*$");
	
	public static interface ValueHandler {
		public void handle(byte[] key, byte[] value, boolean mask, boolean encryption) throws IOException;
	}

	public void parse(byte[] source, int offset, int length, ValueHandler valueHandler) throws IOException {
		int limit = Math.min(source.length, offset + length);
		handle(source, offset, limit, valueHandler);
	}

	protected void handle(byte[] source, int offset, int limit, ValueHandler valueHandler) throws IOException {
		while(offset < limit) {
			offset = handleTag(source, offset, limit, valueHandler);
		}
	}

	public static int handleTag(byte[] source, int offset, int limit, ValueHandler valueHandler) throws IOException {
		int start = offset;
		if((source[offset++] & 0x1F) == 0x1F)
			while(true) {
				if(offset >= limit)
					throw new IOException("Unfinished tag '" + StringUtils.toHex(source, start, limit) + "' at position " + (start + 1) + "; need at least one more byte");
				if((source[offset++] & 0x80) != 0x80)
					break;
			}
		if(offset >= limit)
			throw new IOException("No bytes left for length of tag '" + StringUtils.toHex(source, start, limit) + "' at position " + (offset + 1) + "; need at least one more byte");

		int end = offset;
		byte[] key = new byte[offset - start];
		System.arraycopy(source, start, key, 0, key.length);
		boolean mask = ((source[offset] & 0xA0) == 0xA0);
		boolean encryption = ((source[offset] & 0xC0) == 0xC0);
		int len;
		if((source[offset] & 0x80) == 0x80) {
			int lengthBytes = (source[offset++] & 0x1F);
			if(lengthBytes > limit - offset)
				throw new IOException("Not enough bytes at position " + (offset + 1) + "; need " + (lengthBytes - limit + offset) + " more bytes for key " + StringUtils.toHex(key));

			len = 0;
			for(int l = 0; l < lengthBytes; l++)
				len = len * 256 + (source[offset++] & 0xFF);
		} else
			len = (source[offset++] & 0x7F);
		if(len > limit - offset)
			throw new IOException("Invalid length " + len + " at position " + (end + 1) + "; need " + (len - limit + offset) + " more bytes for key " + StringUtils.toHex(key));
		byte[] value = new byte[len];
		System.arraycopy(source, offset, value, 0, value.length);
		offset += value.length;
		valueHandler.handle(key, value, mask, encryption);
		return offset;
	}

	public void parse(byte[] source, int offset, int length, final Map<byte[], byte[]> addTo) throws IOException {
		parse(source, offset, length, new ValueHandler() {
			@Override
			public void handle(byte[] key, byte[] value, boolean mask, boolean encryption) throws IOException {
				addTo.put(key, value);
			}
		});
	}
	
	public void parseData(byte[] source, int offset, int length, final Map<String, byte[]> addTo) throws IOException {
		parse(source, offset, length, new ValueHandler() {
			@Override
			public void handle(byte[] key, byte[] value, boolean mask, boolean encryption) throws IOException {
				addTo.put(StringUtils.toHex(key), value);
			}
		});
	}	

	public Map<byte[], byte[]> parse(byte[] source, int offset, int length) throws IOException {
		Map<byte[], byte[]> map = new TreeMap<>(new Comparator<byte[]>() {
			@Override
			public int compare(byte[] o1, byte[] o2) {
				int min = Math.min(o1.length, o2.length);
				for(int i = 0; i < min; i++) {
					int c = o1[i] - o2[i];
					if(c != 0)
						return c;
				}
				return o1.length - o2.length;
			}
		});
		parse(source, offset, length, map);
		return map;
	}
	
	public Map<String, byte[]> parseData(byte[] source, int offset, int length) throws IOException {
		Map<String, byte[]> map = new HashMap<String, byte[]>();
		parseData(source, offset, length, map);
		return map;
	}	
	
	public static void formatTagValue(ByteBuffer buffer, byte [] tag, byte [] value) {
		buffer.put(tag);
		if (value.length > 127) {
			int numBytesNeededForLength = 0;
			int lengthCalc = value.length;
			buffer.mark();
			buffer.position(buffer.position() + 1);
			while (lengthCalc > 0) {
				byte lenByte = (byte)(lengthCalc & 0xff);
				buffer.put(lenByte);
				lengthCalc = lengthCalc >> 8;
				numBytesNeededForLength++;
			}
			int firstValueBytePos = buffer.position();
			buffer.reset();
			byte firstByte = (byte)(0x80 | numBytesNeededForLength);
			buffer.put(firstByte);
			buffer.position(firstValueBytePos);
		} else {
			buffer.put((byte)value.length);
		}
		buffer.put(value);
	}
	
	public static void formatTagValueToHex(StringBuilder buffer, byte [] tag, byte [] value) {
		StringUtils.appendHex(buffer, tag);
		if (value.length > 127) {
			int numBytesNeededForLength = 0;
			int lengthCalc = value.length;
			StringBuilder lengthSb = new StringBuilder(2);
			while (lengthCalc > 0) {
				byte lenByte = (byte)(lengthCalc & 0xff);
				StringUtils.appendHex(lengthSb, lenByte);
				lengthCalc = lengthCalc >> 8;
				numBytesNeededForLength++;
			}
			byte firstByte = (byte)(0x80 | numBytesNeededForLength);
			StringUtils.appendHex(buffer, firstByte);
			buffer.append(lengthSb);
		} else {
			StringUtils.appendHex(buffer, (byte)value.length);
		}
		StringUtils.appendHex(buffer, value);
	}
	
}
