/*
 * SourceFile.java
 *
 * Created on December 23, 2003, 12:51 PM
 */

package simple.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.xml.sax.InputSource;

import simple.lang.SystemUtils;

/** Provides loading and reloading capabilities by recording when a file or url was last loaded (read),
 *  and whether it has changed since.
 *
 * @author  Brian S. Krug
 */
public abstract class ConfigSource {
    protected Log log;
    protected long lastLoaded = Long.MIN_VALUE;
    protected long lastChecked = Long.MIN_VALUE;
    protected Set<Object> loadKeys = new java.util.HashSet<Object>();
    protected long minInterval;
    protected final ReentrantLock lock = new ReentrantLock();
	protected final AtomicInteger updating = new AtomicInteger();

    /** Holds value of property loader. */
    private Loader loader;

    /**
     * This class periodically calls reload()
     */
    protected class Reloader extends simple.util.IntervalThread {
        public Reloader() {
            super("Reloader of '" + ConfigSource.this.toString() + "'");
        }
        @Override
		protected void action() {
            try {
                reload();
            } catch(LoadingException e) {
                if(!silent) log.warn("Could not reload '" + ConfigSource.this.toString() + "'", e);
            } catch(RuntimeException e) {
                if(!silent) log.warn("Could not reload '" + ConfigSource.this.toString() + "'", e);
            }
        }
    }

    protected Reloader reloader;
    protected final boolean silent;
    protected final String uri;

    protected ConfigSource(String uri, boolean silent) {
    	this.uri = uri;
        this.silent = silent;
        if(!silent) log = Log.getLog();
    }

    /** Creates a new <code>ConfigSource</code> instance by converting the given
     * pathname string into a URL or File. Messages are logged when this method is used.
     *
     * @param   pathname  A pathname string
     * @throws  NullPointerException
     *          If the <code>pathname</code> argument is <code>null</code>
     *
     */
    public static ConfigSource createConfigSource(String pathname) throws IOException {
        return createConfigSource(pathname, pathname, null, false);
    }

    /** Creates a new <code>ConfigSource</code> instance by converting the given
     * pathname string into a URL or File.
     *
     * @param   pathname  A pathname string
     * @param   silent  Whether to log messages or not (the Log class uses this
     *  so as to avoid infinite recursion.
     * @returns The new ConfigSource object
     * @throws  NullPointerException
     *          If the <code>pathname</code> argument is <code>null</code>
     *
     */
    public static ConfigSource createConfigSource(String pathname, boolean silent) throws IOException {
        return createConfigSource(pathname, pathname, null, silent);
    }

    protected static final Pattern urlProtocolPattern = Pattern.compile("\\s*([a-zA-Z]{2,}):");

	public static String getProtocol(String uri) {
		Matcher matcher = urlProtocolPattern.matcher(uri);
		if(matcher.lookingAt()) {
			return matcher.group(1);
		}
		return null;
	}
    protected static ConfigSource createConfigSource(String uri, String pathname, URL url, boolean silent) throws IOException {
    	String protocol;
    	String remainder;
    	if(url != null) {
    		protocol = url.getProtocol();
    		int len = 0;
    		if(url.getFile() != null && url.getFile().length() > 0)
    			len += url.getFile().length();
    		if(url.getAuthority() != null && url.getAuthority().length() > 0)
    			len += 2+url.getAuthority().length();
    		if(url.getRef() != null && url.getRef().length() > 0)
    			len += 1+url.getRef().length();

    		StringBuilder sb = new StringBuilder(len);
    		if(url.getAuthority() != null && url.getAuthority().length() > 0)
    			sb.append("//").append(url.getAuthority());
    		if(url.getFile() != null && url.getFile().length() > 0)
    			sb.append(url.getFile());
    		if(url.getRef() != null && url.getRef().length() > 0)
    			sb.append("#").append(url.getRef());

    		remainder = sb.toString();
    	} else {
    		Matcher matcher = urlProtocolPattern.matcher(pathname);
    		if(matcher.lookingAt()) {
    			protocol = matcher.group(1);
    			remainder = pathname.substring(matcher.end());
    		} else {
    			protocol = "resource";
    			remainder = pathname;
    		}
    	}
    	if("resource".equalsIgnoreCase(protocol) || "systemresource".equalsIgnoreCase(protocol)) {
    		return createConfigSource(uri, remainder, createUrl(remainder), silent);
    	} else if("file".equalsIgnoreCase(protocol)) {
            return new FileConfigSource(new File(decodeFilePath(remainder)), uri, silent);
    	} else if("jar".equalsIgnoreCase(protocol)) {
    		int pos = remainder.indexOf('!');
    		if(pos == -1)
    		    throw new MalformedURLException("no ! found in url spec:" + remainder);
    		if(remainder.length() <= pos + 1)
        		throw new IOException("no entry name specified");
    		URL jarURL = new URL(remainder.substring(0, pos));
    		String host;
    		if(jarURL.getProtocol().equalsIgnoreCase("file") && (
    				(host= jarURL.getHost()) == null || host.equals("") || host.equals("~") || host.equalsIgnoreCase("localhost"))) {
    			JarFile jarFile = new JarFile(decodeFilePath(remainder.substring(5, pos)));
        		String entryName = decodeJarPath(remainder.substring(pos+1));
        		JarEntry jarEntry = jarFile.getJarEntry(entryName);
        		return new ZipEntryConfigSource(jarFile, jarEntry, uri, silent);
    		} else {
    			return new UrlConfigSource(url == null ? new URL(pathname) : url, uri, silent);
    		}
    	} else { // some other protocol
            return new UrlConfigSource(url == null ? new URL(pathname) : url, uri, silent);
        }
    }
    @SuppressWarnings("deprecation")
	protected static String decodeFilePath(String filePath) {
    	return URLDecoder.decode(filePath);
    }
    //I'm not sure this is done different from URLDecoder, but this is copied from the JARURLConnection
	public static String decodeJarPath(String entryPath) {
		StringBuilder stringbuilder = new StringBuilder();
        char c;
        for(int i = 0; i < entryPath.length(); stringbuilder.append(c)) {
            c = entryPath.charAt(i);
            if(c != '%') {
                i++;
                continue;
            }
            try  {
                c = (char)Integer.parseInt(entryPath.substring(i + 1, i + 3), 16);
                i += 3;
                if((c & 0x80) == 0)
                    continue;
                switch(c >> 4)
                {
                case 12: // '\f'
                case 13: // '\r'
                    char c1 = (char)Integer.parseInt(entryPath.substring(i + 1, i + 3), 16);
                    i += 3;
                    c = (char)((c & 0x1f) << 6 | c1 & 0x3f);
                    break;

                case 14: // '\016'
                    char c2 = (char)Integer.parseInt(entryPath.substring(i + 1, i + 3), 16);
                    i += 3;
                    char c3 = (char)Integer.parseInt(entryPath.substring(i + 1, i + 3), 16);
                    i += 3;
                    c = (char)((c & 0xf) << 12 | (c2 & 0x3f) << 6 | c3 & 0x3f);
                    break;

                default:
                    throw new IllegalArgumentException();
                }
            }
            catch(NumberFormatException numberformatexception)
            {
                throw new IllegalArgumentException();
            }
        }
        //BSK - Zip does not find entry if it starts with '/' so remove if found
        return (stringbuilder.length() > 0 && stringbuilder.charAt(0) == '/' ? stringbuilder.substring(1) : stringbuilder.toString());
	}

    /** Creates a new <code>ConfigSource</code> instance using the specified
     *  InputStream as the data source. Messages are logged when this method is used.
     *
     * @param   in  The InputStream
     * @returns The new ConfigSource object
     * @throws  NullPointerException
     *          If the <code>in</code> argument is <code>null</code>
     */
    public static ConfigSource createConfigSource(InputStream in) {
        return new InputStreamConfigSource(in, null, false);
    }

    /** Creates a new <code>ConfigSource</code> instance using the specified
     *  InputStream as the data source
     * @param   in  The InputStream
     * @param   silent  Whether to log messages or not (the Log class uses this
     *  so as to avoid infinite recursion.
     * @returns The new ConfigSource object
     * @throws  NullPointerException
     *          If the <code>in</code> argument is <code>null</code>
     */
    public static ConfigSource createConfigSource(InputStream in, boolean silent) {
        return new InputStreamConfigSource(in, null, silent);
    }

    /** Creates a new <code>ConfigSource</code> instance using the specified
     *  InputStream as the data source. Messages are logged when this method is used.
     *
     * @param   file  The File
     * @returns The new ConfigSource object
     * @throws  NullPointerException
     *          If the <code>in</code> argument is <code>null</code>
     */
    public static ConfigSource createConfigSource(File file) {
        return new FileConfigSource(file, file.toURI().toString(), false);
    }

    /** Creates a new <code>ConfigSource</code> instance using the specified
     *  InputStream as the data source
     * @param   file  The File
     * @param   silent  Whether to log messages or not (the Log class uses this
     *  so as to avoid infinite recursion.
     * @returns The new ConfigSource object
     * @throws  NullPointerException
     *          If the <code>in</code> argument is <code>null</code>
     */
    public static ConfigSource createConfigSource(File file, boolean silent) {
        return new FileConfigSource(file, file.toURI().toString(), silent);
    }

    /** Creates a new <code>ConfigSource</code> instance using the specified
     *  URL as the data source
     * @param   url  The URL
     * @throws IOException
     * @returns The new ConfigSource object
     * @throws  NullPointerException
     *          If the <code>URL</code> argument is <code>null</code>
     */
    public static ConfigSource createConfigSource(URL url) throws IOException {
        return createConfigSource(url.toString(), null, url, false);
    }

    /** Creates a new <code>ConfigSource</code> instance using the specified
     *  URL as the data source
     * @param   url  The URL
     * @param   silent  Whether to log messages or not (the Log class uses this
     *  so as to avoid infinite recursion.
     * @throws IOException
     * @returns The new ConfigSource object
     * @throws  NullPointerException
     *          If the <code>URL</code> argument is <code>null</code>
     */
    public static ConfigSource createConfigSource(URL url, boolean silent) throws IOException {
        return createConfigSource(url.toString(), null, url, silent);
    }

    /** Creates a new <code>ConfigSource</code> instance using the specified
     *  byte array as the data source. Messages are logged when this method is used.
     * @param   data  The byte array of source data
     * @returns The new ConfigSource object
     * @throws  NullPointerException
     *          If the <code>data</code> argument is <code>null</code>
     */
    public static ConfigSource createConfigSource(byte[] data) {
        data.getClass(); //ensure not null
        return new ByteArrayConfigSource(data, null, false);
    }

    /** Creates a new <code>ConfigSource</code> instance using the specified
     *  byte array as the data source
     * @param   data  The byte array of source data
     * @param   silent  Whether to log messages or not (the Log class uses this
     *  so as to avoid infinite recursion.
     * @returns The new ConfigSource object
     * @throws  NullPointerException
     *          If the <code>data</code> argument is <code>null</code>
     */
    public static ConfigSource createConfigSource(byte[] data, boolean silent) {
        data.getClass(); //ensure not null
        return new ByteArrayConfigSource(data, null, silent);
    }

    protected static URL createUrl(String pathname) throws IOException {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        if(cl == null) {
            cl = ConfigSource.class.getClassLoader();
            if(cl == null) cl= ClassLoader.getSystemClassLoader();
        }
        int start = 0;
        while(start < pathname.length() && pathname.charAt(start) == '/') start++;
        if(start > 0) pathname = pathname.substring(start);
        URL url = cl.getResource(pathname);
        if(url == null) url = ClassLoader.getSystemResource(pathname);
        if(url == null) throw new IOException("Couldn't locate '" + pathname + "' as a resource in the system from " + SystemUtils.getClassLoaderDetails(cl));
        return url;
    }

    /** Returns the last time this file was loaded
     * @return The time in milleseconds of the last load
     */
    public long lastLoaded() {
        return lastLoaded;
    }

    /** Sets the last time this file was loaded
     * @param lastLoaded The time in milleseconds of the last load
     */
    public void setLastLoaded(long lastLoaded) {
        this.lastLoaded = lastLoaded;
    }

    /** Determines whether the file has changed since last loaded
     * @return Whether the file has changed since the last load
     */
    public boolean hasChanged() {
        long mod = lastModified();
        if(mod == 0) return true; // some error has occurred
        return lastLoaded() < mod;
    }

    /** Adds a key to the loadKeys Set. This set can be used to track which components
     * are loaded by this SourceFile.
     * @param key The key object
     */
    public void addLoadKey(Object key) {
        loadKeys.add(key);
    }

    /** Removes a key from the loadKeys Set. This set can be used to track which components
     * are loaded by this SourceFile.
     * @param key
     */
    public void removeLoadKey(Object key) {
        loadKeys.remove(key);
    }

    /** Removes all keys in the loadKeys Set. This set can be used to track which components
     * are loaded by this SourceFile.
     */
    public void clearLoadKeys() {
        loadKeys.clear();
    }

    /** Retreives the loadKeys Set. This set can be used to track which components
     * are loaded by this SourceFile.
     * @return The loadKeys Set
     */
    public Set<Object> getLoadKeys() {
        return loadKeys;
    }

    /** Getter for property loader.
     * @return Value of property loader.
     *
     */
    public Loader getLoader() {
        return this.loader;
    }

    /** Registers the specified loader to load this file whenever reload() is called or the maxInterval has passed
     * and the minInterval has passed and the file has changed. This method is not thread safe as it is expected
     * to be used during initialization and not during normal processing.
     * @param loader The loader to use for loading this file
     * @param minInterval The minimum number of milleseconds between loads
     * @param maxInterval The number of milleseconds between secheduled calls to the <code>reload()</code>
     * method. Use 0 to disable scheduled reloading.
     */
    public void registerLoader(Loader loader, long minInterval, long maxInterval) {
        this.loader = loader;
        if(maxInterval > 0) {
            //create a thread to call reload
            if(reloader == null) {
                reloader = new Reloader();
                reloader.setInterval(maxInterval);
                reloader.start();
            } else {
                reloader.setInterval(maxInterval);
            }
        } else if(reloader != null) {
            reloader.finish();
            reloader = null;
        }
        this.minInterval = minInterval;
    }

    /** If the minInterval has passed since the last load and the file has changed then this method calls
     * the load() method on the registered Loader.
     * @throws LoadingException
     * @return Whether the file was reloaded or not
     */
    public boolean reload() throws LoadingException {
        //if(!silent) log.debug("Getting last modified for " + loader);
		if(loader != null && updating.get() == 0) {
			long mod;
			long time = System.currentTimeMillis();
			if(lastLoaded() == Long.MIN_VALUE) { // its never been loaded
        		if(!silent && log.isDebugEnabled())
        			log.debug("Initial load for " + getClass().getName() + " of '" + toString() + "'");
				mod = 0;
			} else if(minInterval != 0 && lastChecked + minInterval >= time)
				return false;
			if(!silent && log.isDebugEnabled())
				log.debug("Getting last modified for " + getClass().getName() + " of '" + toString() + "'");
			mod = lastModified();
			if(!silent && log.isDebugEnabled())
				log.debug("Checking reload for " + getClass().getName() + " of '" + toString() + "'; lastLoaded=" + lastLoaded() + "; lastModified=" + mod/*, new Throwable()*/);
			lock.lock(); // only synchronize if we must & block until condition holds
			try {
				lastChecked = time;
				if(updating.get() == 0 && (mod == 0 || lastLoaded() < mod)) {
                    if(!silent && log.isDebugEnabled())
                    	log.debug("Reloading using " + loader);
                    loader.load(this);
					lastLoaded = System.currentTimeMillis(); // b/c load() may take a while
                    lastChecked = lastLoaded;
                    return true;
                }
			} finally {
				lock.unlock();
			}
        }
        return false;
    }

    public abstract long lastModified() ;
    public abstract InputStream getInputStream() throws IOException ;

	protected abstract OutputStream getRawOutputStream() throws IOException;

	@SuppressWarnings("resource")
	public OutputStream getOutputStream() throws IOException {
		OutputStream out = getRawOutputStream();
		return out instanceof Abortable ? new AbortableLockingOutputStream(out, (Abortable) out) : new LockingOutputStream(out);
	}

	protected void startUpdating() {
    	lock.lock(); // this is so we wait until any reload is complete
		try {
			updating.incrementAndGet();
		} finally {
			lock.unlock();
		}
	}

	protected void endUpdating() {
		lock.lock(); // this is so we wait until any reload is complete
		try {
			long time = System.currentTimeMillis();
			if(time > lastLoaded)
				lastLoaded = time; // we assume the app has handled the changes it is saving
			updating.decrementAndGet();
		} finally {
			lock.unlock();
		}
	}
    public String getUri() {
    	return uri;
    }
    @Override
    public String toString() {
        return getUri();
    }
    public InputSource getInputSource() throws IOException {
        InputSource is = new InputSource(getInputStream());
        String uri = getUri();
        if(uri != null) is.setSystemId(uri);
        return is;
    }

    /**
     *  Implements ConfigSource using a file
     */
    protected static class FileConfigSource extends ConfigSource {
        protected final File file;
        /** Creates a new <code>ConfigSource</code> instance using the specified file as the info source
         */
        public FileConfigSource(File file, String uri, boolean silent) {
            super(uri, silent);
            this.file = file;
        }
        @Override
		public InputStream getInputStream() throws IOException {
            return new java.io.FileInputStream(file);
        }
        @Override
		public long lastModified() {
            return file.lastModified();
        }
        @Override
        public boolean equals(Object obj) {
            if(this == obj) return true;
            if(obj instanceof FileConfigSource) {
                FileConfigSource fcs = (FileConfigSource)obj;
                return file.equals(fcs.file);
            }
            return false;
        }
        @Override
        public int hashCode() {
            return file.hashCode();
        }

		@Override
		protected OutputStream getRawOutputStream() throws IOException {
			return new AtomicFileOutputStream(file);
		}
    }
    /**
     *  Implements ConfigSource using a jar file entry
     */
    protected static class ZipEntryConfigSource extends ConfigSource {
        protected final ZipFile zipFile;
        protected final ZipEntry zipEntry;
        /** Creates a new <code>ConfigSource</code> instance using the specified file as the info source
         */
        public ZipEntryConfigSource(ZipFile zipFile, ZipEntry zipEntry, String uri, boolean silent) {
            super(uri, silent);
            this.zipFile = zipFile;
            this.zipEntry = zipEntry;
        }
        @Override
		public InputStream getInputStream() throws IOException {
            return zipFile.getInputStream(zipEntry);
        }
        @Override
		public long lastModified() {
            return zipEntry.getTime();
        }
        @Override
        public boolean equals(Object obj) {
            if(this == obj) return true;
            if(obj instanceof ZipEntryConfigSource) {
            	ZipEntryConfigSource zcs = (ZipEntryConfigSource)obj;
                return zipFile.equals(zcs.zipFile) && zipEntry.equals(zcs.zipEntry);
            }
            return false;
        }
        @Override
        public int hashCode() {
            return zipFile.hashCode() ^ zipEntry.hashCode();
        }

		@Override
		protected OutputStream getRawOutputStream() throws IOException {
			throw new IOException("Updating not supported");
		}
    }

    /**
     * Implements ConfigSource using a URL
     */
    protected static class UrlConfigSource extends ConfigSource {
        protected final URL url;
        public UrlConfigSource(URL url, String uri, boolean silent) {
            super(uri, silent);
            this.url = url;
        }
        @Override
		public java.io.InputStream getInputStream() throws IOException {
            //return new LoggedInputStream(url.openStream());
            return url.openStream();
        }
        @Override
		public long lastModified() {
            try {
            	URLConnection conn = url.openConnection();
            	return conn.getLastModified();
            } catch (IOException e) {
                log.debug("Exception getting Last Modified for " + url.getClass().getName() + " '" + url, e);
                return 0;
            } catch(RuntimeException e) {
                log.debug("Exception getting Last Modified for " + url.getClass().getName() + " '" + url, e);
                return 0;
            }
        }
        @Override
        public boolean equals(Object obj) {
            if(this == obj) return true;
            if(obj instanceof UrlConfigSource) {
                UrlConfigSource ucs = (UrlConfigSource)obj;
                return url.equals(ucs.url);
            }
            return false;
        }
        @Override
        public int hashCode() {
            return url.hashCode();
        }

		@Override
		protected OutputStream getRawOutputStream() throws IOException {
			return url.openConnection().getOutputStream();
		}
    }

    /**
     *  Implements ConfigSource using an InputStream
     */
    protected static class InputStreamConfigSource extends ConfigSource {
        protected final InputStream in;
        protected long lastModified = System.currentTimeMillis();
        /** Creates a new <code>ConfigSource</code> instance using the specified InputStream as the info source
         */
        public InputStreamConfigSource(InputStream in, String uri, boolean silent) {
            super(uri, silent);
            this.in = in;
        }
        @Override
		public InputStream getInputStream() throws IOException {
            return in;
        }
        @Override
		public long lastModified() {
            try {
                if(in.available() > 0) lastModified = System.currentTimeMillis();
            } catch(IOException ioe) {
                if(!silent) log.warn("Could not read from InputStream of ConfigSource", ioe);
            }
            return lastModified;
        }
        @Override
		public String toString() {
            return in.toString();
        }
        @Override
        public boolean equals(Object obj) {
            if(this == obj) return true;
            if(obj instanceof InputStreamConfigSource) {
                InputStreamConfigSource ics = (InputStreamConfigSource)obj;
                return in.equals(ics.in);
            }
            return false;
        }
        @Override
        public int hashCode() {
            return in.hashCode();
        }

		@Override
		protected OutputStream getRawOutputStream() throws IOException {
			throw new IOException("Updating not supported");
		}
    }

    /**
     *  Implements ConfigSource using a byte array as the data
     */
    protected static class ByteArrayConfigSource extends ConfigSource {
        protected byte[] data;
        protected long lastModified = System.currentTimeMillis();
        /** Creates a new <code>ConfigSource</code> instance using the specified byte array as the info source
         */
        public ByteArrayConfigSource(byte[] data, String uri, boolean silent) {
            super(uri, silent);
            this.data = data;
        }
        @Override
		public InputStream getInputStream() throws IOException {
            return new java.io.ByteArrayInputStream(data);
        }
        @Override
		public long lastModified() {
            return lastModified;
        }
        @Override
		public String toString() {
            return new String(data);
        }
        @Override
        public boolean equals(Object obj) {
            if(this == obj) return true;
            if(obj instanceof ByteArrayConfigSource) {
                ByteArrayConfigSource bcs = (ByteArrayConfigSource)obj;
                return Arrays.equals(data, bcs.data);
            }
            return false;
        }
        @Override
        public int hashCode() {
            return Arrays.hashCode(data);
        }

		@Override
		protected OutputStream getRawOutputStream() throws IOException {
			throw new IOException("Updating not supported");
		}
    }

    protected static class LoggedInputStream extends java.io.FilterInputStream {
        protected Log log = Log.getLog();
        protected int total = 0;
        public LoggedInputStream(InputStream in) {
            super(in);
        }

        @Override
		public int read() throws IOException {
            try {
                int r = super.read();
                if(r == -1) logRead(null, 0, r);
                else logRead(new byte[]{(byte)r}, 0, 1);
                return r;
            } catch(IOException ioe) {
                log.warn("While reading", ioe);
                throw ioe;
            }
        }

        @Override
		public int read(byte[] b) throws IOException {
            try {
                int r = super.read(b);
                logRead(b, 0, r);
                return r;
            } catch(IOException ioe) {
                log.warn("While reading", ioe);
                throw ioe;
            }
       }

        @Override
		public int read(byte[] b, int off, int len) throws IOException {
            try {
                int r = super.read(b, off, len);
                logRead(b, off, r);
                return r;
            } catch(IOException ioe) {
                log.warn("While reading", ioe);
                throw ioe;
            }
        }

        protected void logRead(byte[] b, int off, int count) {
            if(count == -1) {
                log.debug("END OF STREAM! Total bytes read: " + total);
            } else {
                total += count;
                log.debug("Read: '" + new String(b, off, count) + "' (" + count + " byte(s); Total bytes read: " + total + ")");
            }
        }
    }

	protected class LockingOutputStream extends OutputStream {
		protected final OutputStream delegate;

		public LockingOutputStream(OutputStream delegate) {
			this.delegate = delegate;
		}

		@Override
		public void write(int b) throws IOException {
			startUpdating();
			delegate.write(b);
		}

		@Override
		public void close() throws IOException {
			try {
				delegate.close();
			} finally {
				endUpdating();
			}
		}

		@Override
		public void flush() throws IOException {
			delegate.flush();
		}

		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			startUpdating();
			delegate.write(b, off, len);
		}
	}
	
	protected class AbortableLockingOutputStream extends LockingOutputStream implements Abortable {
		protected final Abortable abortable;

		public AbortableLockingOutputStream(OutputStream delegate, Abortable abortable) {
			super(delegate);
			this.abortable = abortable;
		}

		@Override
		public void abort() throws IOException {
			try {
				abortable.abort();
			} finally {
				endUpdating();
			}
		}
	}
}
