package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class OutputStreamByteOutput implements ByteOutput {
	protected final OutputStream out;
	protected final byte writeBuffer[] = new byte[8];

	public OutputStreamByteOutput(OutputStream out) {
		super();
		this.out = out;
	}
	/**
	 * @see simple.io.ByteOutput#writeBytes(java.lang.String)
	 */
	public void writeBytes(String s) throws IOException {
		writeBytes(s, s.length());
	}
	/**
	 * @see simple.io.ByteOutput#writeChars(java.lang.String)
	 */
	public void writeChars(String s) throws IOException {
		writeChars(s, s.length());
	}
	/**
	 * @see simple.io.ByteOutput#writeByte(int)
	 */
    public void writeByte(int b) throws IOException {
	out.write(b);
    }

    /**
	 * @see simple.io.ByteOutput#writeShort(int)
	 */
    public void writeShort(int s) throws IOException {
        out.write((s >>> 8) & 0xFF);
        out.write((s >>> 0) & 0xFF);
    }

    /**
	 * @see simple.io.ByteOutput#writeChar(int)
	 */
    public void writeChar(int c) throws IOException {
        out.write((c >>> 8) & 0xFF);
        out.write((c >>> 0) & 0xFF);
    }

    /**
	 * @see simple.io.ByteOutput#writeInt(int)
	 */
    public void writeInt(int i) throws IOException {
        out.write((i >>> 24) & 0xFF);
        out.write((i >>> 16) & 0xFF);
        out.write((i >>>  8) & 0xFF);
        out.write((i >>>  0) & 0xFF);
    }

    /**
	 * @see simple.io.ByteOutput#writeLong(long)
	 */
    public void writeLong(long l) throws IOException {
        writeBuffer[0] = (byte)(l >>> 56);
        writeBuffer[1] = (byte)(l >>> 48);
        writeBuffer[2] = (byte)(l >>> 40);
        writeBuffer[3] = (byte)(l >>> 32);
        writeBuffer[4] = (byte)(l >>> 24);
        writeBuffer[5] = (byte)(l >>> 16);
        writeBuffer[6] = (byte)(l >>>  8);
        writeBuffer[7] = (byte)(l >>>  0);
        out.write(writeBuffer, 0, 8);
	}

    /**
	 * @see simple.io.ByteOutput#writeFloat(float)
	 */
    public void writeFloat(float f) throws IOException {
    	writeInt(Float.floatToIntBits(f));
    }

    /**
	 * @see simple.io.ByteOutput#writeDouble(double)
	 */
    public void writeDouble(double d) throws IOException {
    	writeLong(Double.doubleToLongBits(d));
    }

    /**
	 * @see simple.io.ByteOutput#writeBytes(java.lang.String, int)
	 */
    public void writeBytes(String s, int length) throws IOException {
		writeBytes(s, 0, length);
	}

	/**
	 * @see simple.io.ByteOutput#writeBytes(java.lang.String, int, int)
	 */
	public void writeBytes(String s, int offset, int length) throws IOException {
		for (int i = offset ; i < length ; i++) {
		    out.write((byte)s.charAt(i));
		}
	}

    /**
	 * @see simple.io.ByteOutput#writeChars(java.lang.String, int)
	 */
    public void writeChars(String s, int length) throws IOException {
        for (int i = 0 ; i < length ; i++) {
            int v = s.charAt(i);
            out.write((v >>> 8) & 0xFF);
            out.write((v >>> 0) & 0xFF);
        }
    }

    public void write(byte[] bytes) throws IOException {
    	out.write(bytes);
    }

    public void write(byte[] bytes, int offset, int length) throws IOException {
    	out.write(bytes, offset, length);
    }
    public void write(ByteBuffer buffer) throws IOException {
    	if(buffer.hasArray()) {
    		write(buffer.array(), buffer.arrayOffset() + buffer.position(), buffer.remaining());
    		buffer.position(buffer.limit());
    	} else {
    		byte[] tmp = new byte[buffer.remaining()];
    		buffer.get(tmp);
    		write(tmp);
    	}

    }
    public void flush() throws IOException {
    	out.flush();
    }
}
