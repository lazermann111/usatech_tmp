package simple.io;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;

/** Will hold back trailing characters and not pass them through to the underlying writer. This class is NOT thread-safe.
 * @author Brian S. Krug
 *
 */
public abstract class TrimTrailingWriter extends FilterWriter {
	/**
     * Temporary buffer used to hold writes of strings and single characters
     */
    protected char[] writeBuffer;

    /**
     * Size of writeBuffer, must be >= 1
     */
    protected final int writeBufferSize = 1024;

    protected int writeBufferPos = 0;
    
	public TrimTrailingWriter(Writer out) {
		super(out);
	}
	
	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
		int i = off + len;
		for(; i > off; i--) {
			if(!trimIfTrailing(cbuf[i-1])) {
				if(writeBufferPos > 0) {
					out.write(writeBuffer, 0, writeBufferPos);
					writeBufferPos = 0;
				}
				out.write(cbuf, off, i - off);
				break;
			}
		}
		if(i < off + len) {
			if (writeBuffer == null){
		    	writeBuffer = new char[writeBufferSize];
		    }
			int n = off + len - i;
			System.arraycopy(cbuf, i, writeBuffer, writeBufferPos, n);
		    writeBufferPos += n;
		}
	}

	@Override
	public void write(int c) throws IOException {
		char ch = (char)c;
		if(!trimIfTrailing(ch)) {
			if(writeBufferPos > 0) {
				out.write(writeBuffer, 0, writeBufferPos);
				writeBufferPos = 0;
			}
			out.write(c);
		} else {
		    if (writeBuffer == null){
		    	writeBuffer = new char[writeBufferSize];
		    }
		    writeBuffer[writeBufferPos++] = ch;
		}
	}
	protected boolean checkAndSave(char ch) throws IOException {
		if(!trimIfTrailing(ch)) {
			if(writeBufferPos > 0) {
				out.write(writeBuffer, 0, writeBufferPos);
				writeBufferPos = 0;
			}
			return false;
		}
	    if (writeBuffer == null){
	    	writeBuffer = new char[writeBufferSize];
	    }
	    writeBuffer[writeBufferPos++] = ch;
	    return true;
	}
	protected abstract boolean trimIfTrailing(char ch) ;
}
