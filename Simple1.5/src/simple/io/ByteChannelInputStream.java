package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

/** This implementation is NOT thread-safe.
 * @author Brian S. Krug
 *
 */
public class ByteChannelInputStream extends InputStream {
	protected final ReadableByteChannel channel;
	protected ByteBuffer bufferOne;
    protected ByteBuffer lastBuffer;
    protected byte[] lastArray;

	public ByteChannelInputStream(ReadableByteChannel channel) {
        super();
        this.channel = channel;
    }

	@Override
	public int read(byte[] b, int offset, int length) throws IOException {
		ByteBuffer bb;
        if(b == lastArray) {
        	bb = lastBuffer;
        	bb.position(offset);
        	bb.limit(offset + length);
        } else {
        	bb =  ByteBuffer.wrap(b, offset, length);
        	lastArray = b;
        	lastBuffer = bb;
        }
        return channel.read(bb);
	}

	@Override
	public int read() throws IOException {
		if(bufferOne == null)
    		bufferOne = ByteBuffer.allocate(1);
    	bufferOne.rewind();
    	channel.read(bufferOne);
        return bufferOne.get(0);
	}
}
