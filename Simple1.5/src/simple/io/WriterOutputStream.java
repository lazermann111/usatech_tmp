/*
 * Created on Sep 13, 2005
 *
 */
package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;

public class WriterOutputStream extends OutputStream {
	protected final Writer delegate;
	protected final CharsetDecoder charsetDecoder;
	protected final CharBuffer charBuffer;
	protected final ByteBuffer byteBuffer;

    public WriterOutputStream(Writer writer) {
        this(writer, true);
    }

    public WriterOutputStream(Writer writer, boolean simpleConversion) {
		this(writer, simpleConversion ? null : Charset.defaultCharset(), 1024);
	}

	public WriterOutputStream(Writer writer, Charset encoding, int bufferSize) {
        super();
		if(writer == null)
			throw new NullPointerException("Writer may not be null");
        this.delegate = writer;
		if(encoding != null) {
			this.charsetDecoder = encoding.newDecoder();
			this.byteBuffer = ByteBuffer.allocate(Math.max(16, (int) (bufferSize * charsetDecoder.averageCharsPerByte())));
			this.charBuffer = CharBuffer.allocate(bufferSize);
		} else {
			this.charsetDecoder = null;
			this.byteBuffer = null;
			this.charBuffer = null;
		}
    }

    public void write(int b) throws IOException {
        if(charsetDecoder == null)
            delegate.write(b);
        else {
			if(byteBuffer.remaining() < 1)
				flushBuffer();
			byteBuffer.put((byte) b);
        }
    }
    
	protected void flushBuffer() throws IOException {
		byteBuffer.flip();
		while(true) {
			CoderResult result = charsetDecoder.decode(byteBuffer, charBuffer, false);
			if(result.isError())
				throw new IOException("Invalid input bytes");
			byteBuffer.compact();
			// write what was decoded
			charBuffer.flip();
			delegate.write(charBuffer.array(), charBuffer.arrayOffset() + charBuffer.position(), charBuffer.arrayOffset() + charBuffer.limit());
			charBuffer.clear();
			if(!result.isOverflow())
				break;
		}

	}

	public void close() throws IOException {
        delegate.close();
    }
    
    public void flush() throws IOException {
		if(charsetDecoder != null)
			flushBuffer();
        delegate.flush();
    }
    
    public void write(byte[] b, int off, int len) throws IOException {
		if(charsetDecoder == null) {
            char[] ch = new char[len];
            for(int i = 0; i < len; i++)
                ch[i] = (char)b[i + off];
            delegate.write(ch);
		} else {
			while(len > 0) {
				if(byteBuffer.remaining() < len)
					flushBuffer();
				int r = byteBuffer.remaining();
				int w = Math.min(r, len);
				byteBuffer.put(b, off, w);
				off += w;
				len -= w;
			}
		}
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof WriterOutputStream && ((WriterOutputStream)obj).delegate.equals(delegate));
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return delegate.hashCode();
    }
    
    
}
