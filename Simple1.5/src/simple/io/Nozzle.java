/*
 * Created on Apr 18, 2005
 *
 */
package simple.io;

import java.io.OutputStream;
import java.io.Writer;

public interface Nozzle {
    public int fill(OutputStream out);
    public int fill(Writer out);
    
}
