package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * This class loads into a byte buffer from its OutputStream and reads from the same byte buffer using its InputStream.
 * WARNING: this class is not thread-safe. If you access it from more than one thread you must synchronize calls made against
 * this class.
 *
 * @author  Brian S. Krug
 */
public class HeapBufferStream implements BufferStream {
    protected byte[] buffer;
	protected int count = 0;
	protected final int blockSize;
	protected BufferInputStream in;
	protected final OutputStream out = new BufferOutputStream();
	protected final boolean resettable;
	protected int disgarded = 0;

	public HeapBufferStream(boolean resettable) {
		this(1024, 32, resettable);
	}

    public HeapBufferStream() {
		this(1024, 32, false);
    }

    public HeapBufferStream(int blockSize, int capacity) {
		this(blockSize, capacity, false);
    }
    /** Creates a new instance of BufferStream
     * @param blockSize The number of bytes read before that section of the buffer is disgarded
     * @param capacity The initial capacity of the buffer
     */
	public HeapBufferStream(int blockSize, int capacity, boolean resettable) {
        this.blockSize = blockSize;
        buffer = new byte[capacity];
		this.resettable = resettable;
		if(!resettable)
			in = new BufferInputStream();
    }

    /**
     * @see simple.io.BinaryStream#getLength()
     */
    public long getLength() {
    	return size();
    }

    public int size() {
    	return count;
    }

    /** Returns an InputStream to access the data written into the OutputStream
     * @return The InputStream
     * @see simple.io.BinaryStream#getInputStream()
     */
    public InputStream getInputStream() {
		if(resettable)
			return new BufferInputStream();
        return in;
    }

    /** Returns an OutputStream to write the data access by the InputStream
     * @return The OutputStream
     */
    public OutputStream getOutputStream() {
		return out;
    }

    public void clear() {
		count = 0;
		disgarded = 0;
		if(!resettable)
			in.pos = 0;
    }

	/**
	 * Copies bytes from this BufferStream to the ByteBuffer. The number of bytes copied is returned and
	 * will be the smaller of byteBuffer.remaining() or (BufferStream.size() - offset)
	 * 
	 * @param bb
	 * @param offset
	 *            The offset in the BufferStream from which to start copying bytes
	 * @return
	 */
    public int putInto(ByteBuffer byteBuffer, int offset) {
    	int n = Math.min(byteBuffer.remaining(), count - offset);
    	byteBuffer.put(buffer, offset, n);
    	return n;
    }

	public long copyInto(ByteOutput output) throws IOException {
		output.write(buffer, 0, count);
		return count;
    }

	public long copyInto(OutputStream output) throws IOException {
		output.write(buffer, 0, count);
		return count;
	}

    protected void ensureCapacity(int additional) {
		if(additional + count - disgarded > buffer.length) {
			if(!resettable && blockSize > 0 && in.pos - disgarded > blockSize) {
				System.arraycopy(buffer, in.pos - disgarded, buffer, 0, count - in.pos);
				disgarded += in.pos;
				if(additional + count - disgarded <= buffer.length)
					return;
			}

			int newlen = Math.max(buffer.length << 1, additional + count - disgarded);
            byte tmp[] = new byte[newlen];
			System.arraycopy(buffer, 0, tmp, 0, count - disgarded);
	    	buffer = tmp;
		}
    }

    protected class BufferInputStream extends InputStream {
		protected int pos = disgarded;
		protected int mark;
		protected boolean done() {
			return pos >= count;
		}

		@Override
		public int available() throws IOException {
            return count - pos;
        }

        @Override
		public void mark(int readlimit) {
			mark = pos;
        }

        @Override
		public boolean markSupported() {
			return resettable;
        }

        @Override
		public void reset() throws IOException {
			if(!resettable)
				throw new IOException("mark/reset not supported for non-resettable BufferStream");
			pos = mark;
        }

        @Override
		public int read() throws IOException {
            if(done()) return -1;
			byte b = buffer[pos++ - disgarded];
            return b & 0xFF;
        }

        @Override
		public int read(byte[] b, int off, int len) throws IOException {
            if (off < 0 || len < 0  /*||off > count || off + len > count */) {
                throw new IndexOutOfBoundsException();
            } else if (len == 0) {
                return 0;
            }

            if (done()) return -1;
            if (pos + len > count) len = count - pos;
            if (len <= 0) return 0;
			System.arraycopy(buffer, pos - disgarded, b, off, len);
            pos += len;
            return len;
        }

        @Override
		public long skip(long n) throws IOException {
            if (pos + n > count) n = count - pos;
            if (n < 0)  return 0;
            pos += n;
            return n;
        }
    }

    protected class BufferOutputStream extends OutputStream {
        @Override
		public void write(int b) throws IOException {
            ensureCapacity(1);
			buffer[count++ - disgarded] = (byte) b;
        }

        @Override
		public void write(byte[] b, int off, int len) throws IOException {
            if ((off < 0) || (off > b.length) || (len < 0) ||
            ((off + len) > b.length) || ((off + len) < 0)) {
                throw new IndexOutOfBoundsException();
            } else if (len == 0) {
                return;
            }
            ensureCapacity(len);
			System.arraycopy(b, off, buffer, count - disgarded, len);
            count += len;
        }
    }
}
