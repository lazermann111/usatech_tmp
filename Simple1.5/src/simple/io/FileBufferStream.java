/*
 * BufferStream.java
 *
 * Created on December 12, 2003, 9:27 AM
 */

package simple.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * This class loads into a temp file from its OutputStream and reads from the same file using its InputStream.
 * WARNING: this class is not thread-safe. If you access it from more than one thread you must synchronize calls made against
 * this class.
 *
 * @author  Brian S. Krug
 */
public class FileBufferStream implements BufferStream {
    protected final File file;
    protected FileInputStream in;
    protected FileOutputStream out;
    
    /** Creates a new instance of BufferStream 
     * @throws IOException */
    public FileBufferStream() throws IOException {
        this.file = File.createTempFile("file-buffer-stream-", ".tmp");
        this.file.deleteOnExit();
    }

   public long getLength() {
    	return file.length();
    }

    public int size() {
    	return (int) getLength();
    }

    public FileInputStream getInputStream() throws FileNotFoundException {
        if(in == null)
        	in = new FileInputStream(file);
    	return in;
    }

    public FileOutputStream getOutputStream() throws IOException {
        if(out == null)
        	out = new FileOutputStream(file);
    	return out;
    }
    
    public void clear() throws IOException {
    	if(out != null)
    		out.close();
    	if(in != null)
    		in.close();
    	new FileOutputStream(file).close();
    	out = null;
    	in = null;
    }

    public int putInto(ByteBuffer byteBuffer, int offset) throws IOException {
    	FileChannel fc = getInputStream().getChannel();
    	return fc.read(byteBuffer, offset);
    }

	public long copyInto(ByteOutput output) throws IOException {
		return IOUtils.copy(getInputStream(), output, 0L, getLength());
    }

	public long copyInto(OutputStream output) throws IOException {
		return IOUtils.copy(getInputStream(), output);
	}
}
