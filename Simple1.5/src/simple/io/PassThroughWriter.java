/*
 * PassThroughOutputStream.java
 *
 * Created on December 8, 2003, 9:12 AM
 */

package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author  Brian S. Krug
 */
public class PassThroughWriter extends Writer {
    protected Set<Writer> delegates = new LinkedHashSet<Writer>();
    protected Map<OutputStream,Writer> osToWriterMap = new HashMap<OutputStream,Writer>();
    /** Creates a new instance of PassThroughOutputStream */
    public PassThroughWriter(Writer... writers) {
    	if(writers != null)
    		for(Writer writer : writers)
    			addWriter(writer);
    }
    
    public void addOutputStream(OutputStream os) {
        Writer writer = new OutputStreamWriter(os);
        osToWriterMap.put(os, writer);
        delegates.add(writer);
    }
    
    public void removeOutputStream(OutputStream os) {
        Writer writer = osToWriterMap.get(os);
        if(writer != null) delegates.remove(writer);
    }
    
    public void addWriter(Writer writer) {
        delegates.add(writer);
    }
    
    public void removeWriter(Writer writer) {
        delegates.remove(writer);
    }
    
    @Override
    public void close() throws IOException {
        for(Writer writer : delegates) writer.close();
    }
    
    @Override
    public void flush() throws IOException {
        for(Writer writer : delegates) writer.flush();
    }
    
    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        for(Writer writer : delegates) writer.write(cbuf, off, len);
    }
}
