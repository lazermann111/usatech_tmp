package simple.io;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class CopyReader extends FilterReader {
	protected Writer out;
	public CopyReader(Reader in, Writer out) {
		super(in);
		this.out = out;
	}
	@Override
	public int read() throws IOException {
		int ch = super.read();
		if(ch >= 0) out.write(ch);
		return ch;
	}
	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		int count = super.read(cbuf, off, len);
		if(count > 0) out.write(cbuf, off, count);
		return count;
	}
}
