package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataSource;

public class BufferStreamDataSource implements DataSource {
	protected final String name;
	protected final String contentType;
	protected final BufferStream bufferStream;

	public BufferStreamDataSource(String name, String contentType) {
		this(name, contentType, new HeapBufferStream());
	}

	public BufferStreamDataSource(String name, String contentType, BufferStream bufferStream) {
		super();
		this.name = name;
		this.contentType = contentType;
		this.bufferStream = bufferStream;
	}

	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return bufferStream.getInputStream();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return bufferStream.getOutputStream();
	}
}
