package simple.io;

import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnmappableCharacterException;

public class ByteArrayUtils {
	public static int hexValue(char ch) throws IllegalArgumentException {
		switch(ch) {
			case '0': return 0;
			case '1': return 1;
			case '2': return 2;
			case '3': return 3;
			case '4': return 4;
			case '5': return 5;
			case '6': return 6;
			case '7': return 7;
			case '8': return 8;
			case '9': return 9;
			case 'A': case 'a': return 10;
			case 'B': case 'b': return 11;
			case 'C': case 'c': return 12;
			case 'D': case 'd': return 13;
			case 'E': case 'e': return 14;
			case 'F': case 'f': return 15;
			default:
				throw new IllegalArgumentException("Character '" + ch + "' (" + (int)ch + ") is not a hex character");
		}
	}

	public static byte[] fromBCD(byte[] bcdData) {
		byte[] result = new byte[bcdData.length * 2];
		// simply add 0x30 to each half byte
		for(int i = 0; i < bcdData.length; i++) {
			result[i * 2] = (byte) (((bcdData[i] & 0xF0) >> 4) | 0x30);
			result[i * 2 + 1] = (byte) ((bcdData[i] & 0x0F) | 0x30);
		}
		return result;
	}

	public static byte[] toBCD(byte[] bytes) throws UnmappableCharacterException {
		if((bytes.length % 2) == 1)
			throw new UnmappableCharacterException(bytes.length);
		byte[] result = new byte[bytes.length / 2];
		// simply add 0x30 to each half byte
		for(int i = 0; i < result.length; i++) {
			if((bytes[i * 2] & 0x30) != 0x30)
				throw new UnmappableCharacterException(i * 2);
			if((bytes[i * 2 + 1] & 0x30) != 0x30)
				throw new UnmappableCharacterException(i * 2 + 1);
			result[i] = (byte) (((bytes[i * 2] & 0x0F) << 4) | (bytes[i * 2 + 1] & 0x0F));
		}
		return result;
	}
	
	public static boolean isBCDSeparator(byte[] track2) {
		if (track2 == null)
			return false;
		for (byte b: track2) {
			if (b == (byte)0xd2)
				return true;
		}
		return false;
	}

	public static byte hexValue(char ch1, char ch2) throws IllegalArgumentException {
		return (byte)(16 * hexValue(ch1) + hexValue(ch2));
	}

	public static byte[] fromHex(String hex) throws IllegalArgumentException {
		if(hex == null)
			return null;
		byte[] bytes = new byte[hex.length()/2];
		for(int i = 0; i < bytes.length; i++)
			bytes[i] = hexValue(hex.charAt(i * 2), hex.charAt(i * 2 + 1));
		return bytes;
	}

	public static long readUnsignedInt(byte[] bytes, int offset) throws IOException {
		if(offset + 4 > bytes.length)
			throw new EOFException();
		return (((long) (bytes[offset] & 0xff) << 24)
				+ ((bytes[offset+1]& 0xff) << 16)
				+ ((bytes[offset+2]& 0xff) << 8)
				+ ((bytes[offset+3]& 0xff) << 0));
	}

	public static String readByteString(byte[] bytes, int offset, int length) throws IOException {
		if(offset + length > bytes.length)
			throw new EOFException();
		return new String(bytes, offset, length);
	}
	public static String readByteString(byte[] bytes, int offset, int length, Charset charset) throws IOException {
		if(offset + length > bytes.length)
			throw new EOFException();
		return new String(bytes, offset, length, charset);
	}

	public static String readByteLengthString(byte[] bytes, int offset, Charset charset) throws IOException {
		int length = readUnsignedByte(bytes, offset);
		return readByteString(bytes, offset+1, length, charset);
	}
	public static String readShortLengthString(byte[] bytes, int offset, Charset charset) throws IOException {
		int length = readUnsignedShort(bytes, offset);
		return readByteString(bytes, offset+2, length, charset);
	}
	public static String readCharString(byte[] bytes, int offset, int length) throws IOException {
		char[] buf = new char[length];
		for(int i = 0; i < length; i++)
			buf[i] = readChar(bytes, offset);
		return new String(buf, 0, length);
	}

	public static char readChar(byte[] bytes, int offset) throws IOException {
		return (char)readUnsignedShort(bytes, offset);
    }

	public static byte readByte(byte[] bytes, int offset) throws IOException {
		if(offset + 1 > bytes.length)
			throw new EOFException();
		return bytes[offset];
	}

	public static int readUnsignedByte(byte[] bytes, int offset) throws IOException {
		return readByte(bytes, offset) & 0xff;
	}

	public static short readShort(byte[] bytes, int offset) throws IOException {
		return (short)readUnsignedShort(bytes, offset);
	}

	public static int readUnsignedShort(byte[] bytes, int offset) throws IOException {
		if(offset + 2 > bytes.length)
			throw new EOFException();
		return (((bytes[offset]& 0xff) << 8) + ((bytes[offset+1]& 0xff) << 0));
	}

	public static char readByteAsChar(byte[] bytes, int offset) throws IOException {
		return (char) readUnsignedByte(bytes, offset);
	}

	public static int readInt(byte[] bytes, int offset) throws IOException {
		if(offset + 4 > bytes.length)
			throw new EOFException();
		return (((bytes[offset]& 0xff) << 24)
				+ ((bytes[offset+1]& 0xff) << 16)
				+ ((bytes[offset+2]& 0xff) << 8)
				+ ((bytes[offset+3]& 0xff) << 0));
	}

	public static long readLong(byte[] bytes, int offset) throws IOException {
		if(offset + 8 > bytes.length)
			throw new EOFException();
		return (((long) bytes[offset] << 56)
				+ ((long) (bytes[offset+1] & 0xff) << 48)
				+ ((long) (bytes[offset+2] & 0xff) << 40)
				+ ((long) (bytes[offset+3] & 0xff) << 32)
				+ ((long) (bytes[offset+4] & 0xff) << 24)
				+ ((bytes[offset+5] & 0xff) << 16)
				+ ((bytes[offset+6] & 0xff) << 8)
				+ ((bytes[offset+7] & 0xff) << 0));
	}

	public static float readFloat(byte[] bytes, int offset) throws IOException {
		return Float.intBitsToFloat(readInt(bytes, offset));
	}

	public static double readDouble(byte[] bytes, int offset) throws IOException {
		return Double.longBitsToDouble(readLong(bytes, offset));
	}

	/** Writes the given number as a two-byte big-endian value at the given offset
	 * @param bytes
	 * @param offset
	 * @param i
	 */
	public static void writeUnsignedShort(byte[] bytes, int offset, int i) {
		if(i < 0x0000 || i > 0xFFFF)
			throw new IllegalArgumentException("Unsigned Short must be between 0 and 65535, not " + i);
		bytes[offset]   = (byte)(i >> 8);
		bytes[offset+1] = (byte)(i >> 0);

	}

	/**
	 * Writes the given number as a four-byte big-endian value at the given offset
	 * 
	 * @param bytes
	 * @param offset
	 * @param i
	 */
	public static void writeUnsignedLong(byte[] bytes, int offset, long i) {
		if(i < 0x00000000 || i > 0xFFFFFFFFL)
			throw new IllegalArgumentException("Unsigned Long must be between 0 and 4294967295, not " + i);
		bytes[offset]   = (byte)(i >> 24);
		bytes[offset+1] = (byte)(i >> 16);
		bytes[offset+2] = (byte)(i >> 8);
		bytes[offset+3] = (byte)(i >> 0);
	}

	/**
	 * Writes the given number as an eight-byte big-endian value at the given offset
	 * 
	 * @param bytes
	 * @param offset
	 * @param i
	 */
	public static void writeLong(byte[] bytes, int offset, long i) {
		bytes[offset] = (byte) (i >> 56);
		bytes[offset + 1] = (byte) (i >> 48);
		bytes[offset + 2] = (byte) (i >> 40);
		bytes[offset + 3] = (byte) (i >> 32);
		bytes[offset + 4] = (byte) (i >> 24);
		bytes[offset + 5] = (byte) (i >> 16);
		bytes[offset + 6] = (byte) (i >> 8);
		bytes[offset + 7] = (byte) (i >> 0);
	}

	/**
	 * @param bytes
	 * @param offset
	 * @param s
	 */
	@SuppressWarnings("deprecation")
	public static void writeString(byte[] bytes, int offset, String s) {
		s.getBytes(0, s.length(), bytes, 7);
	}
	public static int indexOf(byte[] bytes, byte[] find) {
		return indexOf(bytes, 0, bytes.length, find, 0, find.length);
	}
	public static int indexOf(byte[] bytes, int offset, int length, byte[] find) {
		return indexOf(bytes, offset, length, find, 0, find.length);
	}
	
	public static int indexOf(byte[] bytes, int offset, int length, byte[] find, int findOffset, int findLength) {
		int k = findOffset;
		for(int i = offset; i < offset + length; i++) {
			if(bytes[i] == find[k]) {
				if(++k >= findLength)
					return i;
			} else
				k = 0;
		}
		return -k - 1;
	}

	public static boolean equals(byte[] array1, int offset1, byte[] array2, int offset2, int length) {
		for(int i = 0; i < length; i++)
			if(array1[offset1 + i] != array2[offset2 + i])
				return false;
		return true;
	}
}
