/**
 *
 */
package simple.io;

/**
 * @author Brian S. Krug
 *
 */
public class UUEncode {
	public static byte[] encode(byte[] bytes) {
		if(bytes == null || bytes.length == 0)
			return IOUtils.EMPTY_BYTES;
		int len = (bytes.length % 3 == 0 ? 0 : 1) + (4 * bytes.length / 3);
		byte[] result = new byte[len];
		int k = 0;
		for(int i = 0; i < bytes.length; i+=3) {
			result[k++] = (byte)(0x20 + ((bytes[i]>>2)  & 0x3F));
			if(i + 1 < bytes.length) {
				result[k++] = (byte)(0x20 + (((bytes[i] <<4) | (((bytes[i+1] >>4) & 0x0F))) & 0x3F));
				if(i + 2 < bytes.length) {
					result[k++] = (byte)(0x20 + ((((bytes[i+1] <<2)) | (((bytes[i+2] >>6) & 0x03))) & 0x3F));
					result[k++] = (byte)(0x20 + ((bytes[i+2]) & 0x3F));
				} else {
					result[k++] = (byte)(0x20 + ((bytes[i+1] <<2)  & 0x3F));
				}
			} else {
				result[k++] = (byte)(0x20+ ((bytes[i] <<4) & 0x3F));
			}
		}
		for(; k < result.length; k++) {
			result[k] ^= result[k];
		}

		return result;
	}
	
	/*
	public static byte[] encodeAlt(byte[] bytes) {
		if(bytes == null || bytes.length == 0)
			return IOUtils.EMPTY_BYTES;
		int len = (bytes.length % 3 == 0 ? 0 : 1) + (4 * bytes.length / 3);
		byte[] result = new byte[len];
		int k = 0;
		for(int i = 0; i < bytes.length; i+=3) {
			byte byte0 = bytes[i];
			byte byte1 = (i + 1 < bytes.length ? bytes[i+1] : 1);
			byte byte2 = (i + 2 < bytes.length ? bytes[i+2] : 1);
			result[k++] = (byte)(0x20 + (byte0 >>> 2 & 63));
			result[k++] = (byte)(0x20 + (byte0 << 4 & 48 | byte1 >>> 4 & 15));
			result[k++] = (byte)(0x20 + (byte1 << 2 & 60 | byte2 >>> 6 & 3));
			result[k++] = (byte)(0x20 + (byte2 & 63));
		}
		return result;
	}
	*/
	public static byte[] decode(byte[] bytes) {
		byte[] result = new byte[bytes.length * 3 / 4];
		byte val = 0;
		for(int i = 0, k = 0; i < bytes.length; i++) {
			byte info = (byte)(bytes[i] - 32);
			switch (i%4) {
				case 0:
					val = (byte) ((info & 0x3F) << 2);
					break;
				case 1:
					val |= (byte) ((info >> 4) & 0x03);
					result[k++] = val;
					val = (byte) ((info & 0x0f) <<4);
					break;
				case 2:
					val |= (byte) ((info >> 2) & 0x0f);
					result[k++] = val;
					val = (byte) ((info & 0x03) << 6);
					break;
				case 3:
					val |= (byte) (info & 0x3F);
					result[k++] = val;
					break;
			}

		}
		return result;
	}
}
