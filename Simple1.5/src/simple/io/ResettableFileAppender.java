package simple.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import simple.bean.ConvertUtils;

public class ResettableFileAppender extends BlockingWriter {
	protected File file;

	public ResettableFileAppender() {
	}

	@Override
	protected Writer openDelegate() throws IOException {
		if(file == null)
			throw new IOException("File not set");
		return new FileWriter(file, true);
	}

	@Override
	protected void prepareDelegate() throws IOException {
	}
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		File old = this.file;
		this.file = file;
		if(!ConvertUtils.areEqual(old, file))
			reset();
	}
}