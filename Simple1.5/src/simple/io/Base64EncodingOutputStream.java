package simple.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

/**
 * Copied from batik 1.6 and then adapted
 * 
 * @author bkrug
 * 
 */
public class Base64EncodingOutputStream extends OutputStream {
	/** This array maps the 6 bit values to their characters */
	private final static char pem_array[] = {
			// 0 1 2 3 4 5 6 7
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', // 0
			'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', // 1
			'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', // 2
			'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', // 3
			'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', // 4
			'o', 'p', 'q', 'r', 's', 't', 'u', 'v', // 5
			'w', 'x', 'y', 'z', '0', '1', '2', '3', // 6
			'4', '5', '6', '7', '8', '9', '+', '/' // 7
	};

	protected final byte[] atom = new byte[3];
	protected int atomLen = 0;
	protected final char[] encodeBuf = new char[4];
	protected int lineLen = 0;

	protected final Writer writer;
	protected final String newline;

	public Base64EncodingOutputStream(Writer writer, String newline) {
		this.writer = writer;
		this.newline = newline;
	}

	public void finish() throws IOException {
		encodeAtom();
		writer.flush();
	}

	public void close() throws IOException {
		encodeAtom();
		writer.flush();
		writer.close();
	}

	/**
	 * This can't really flush out output since that may generate
	 * '=' chars which would indicate the end of the stream.
	 * Instead we flush the writer. You can only be sure all output is
	 * writen by closing this stream.
	 */
	public void flush() throws IOException {
		writer.flush();
	}

	public void write(int b) throws IOException {
		atom[atomLen++] = (byte) b;
		if(atomLen == 3)
			encodeAtom();
	}

	public void write(byte[] data) throws IOException {
		encodeFromArray(data, 0, data.length);
	}

	public void write(byte[] data, int off, int len) throws IOException {
		encodeFromArray(data, off, len);
	}

	/**
	 * enocodeAtom - Take three bytes of input and encode it as 4
	 * printable characters. Note that if the length in len is less
	 * than three is encodes either one or two '=' signs to indicate
	 * padding characters.
	 */
	void encodeAtom() throws IOException {
		byte a, b, c;

		switch(atomLen) {
			case 0:
				return;
			case 1:
				a = atom[0];
				encodeBuf[0] = pem_array[((a >>> 2) & 0x3F)];
				encodeBuf[1] = pem_array[((a << 4) & 0x30)];
				encodeBuf[2] = encodeBuf[3] = '=';
				break;
			case 2:
				a = atom[0];
				b = atom[1];
				encodeBuf[0] = pem_array[((a >>> 2) & 0x3F)];
				encodeBuf[1] = pem_array[(((a << 4) & 0x30) | ((b >>> 4) & 0x0F))];
				encodeBuf[2] = pem_array[((b << 2) & 0x3C)];
				encodeBuf[3] = '=';
				break;
			default:
				a = atom[0];
				b = atom[1];
				c = atom[2];
				encodeBuf[0] = pem_array[((a >>> 2) & 0x3F)];
				encodeBuf[1] = pem_array[(((a << 4) & 0x30) | ((b >>> 4) & 0x0F))];
				encodeBuf[2] = pem_array[(((b << 2) & 0x3C) | ((c >>> 6) & 0x03))];
				encodeBuf[3] = pem_array[c & 0x3F];
		}
		if(newline != null && !newline.isEmpty() && lineLen == 64) {
			writer.write(newline);
			lineLen = 0;
		}
		writer.write(encodeBuf);

		lineLen += 4;
		atomLen = 0;
	}

	/**
	 * enocodeAtom - Take three bytes of input and encode it as 4
	 * printable characters. Note that if the length in len is less
	 * than three is encodes either one or two '=' signs to indicate
	 * padding characters.
	 */
	void encodeFromArray(byte[] data, int offset, int len) throws IOException {
		byte a, b, c;
		if(len == 0)
			return;

		// System.out.println("atomLen: " + atomLen +
		// " len: " + len +
		// " offset:  " + offset);

		if(atomLen != 0) {
			switch(atomLen) {
				case 1:
					atom[1] = data[offset++];
					len--;
					atomLen++;
					if(len == 0)
						return;
					atom[2] = data[offset++];
					len--;
					atomLen++;
					break;
				case 2:
					atom[2] = data[offset++];
					len--;
					atomLen++;
					break;
				default:
			}
			encodeAtom();
		}

		while(len >= 3) {
			a = data[offset++];
			b = data[offset++];
			c = data[offset++];

			encodeBuf[0] = pem_array[((a >>> 2) & 0x3F)];
			encodeBuf[1] = pem_array[(((a << 4) & 0x30) | ((b >>> 4) & 0x0F))];
			encodeBuf[2] = pem_array[(((b << 2) & 0x3C) | ((c >>> 6) & 0x03))];
			encodeBuf[3] = pem_array[c & 0x3F];
			writer.write(encodeBuf);

			lineLen += 4;
			if(newline != null && !newline.isEmpty() && lineLen == 64) {
				writer.write(newline);
				lineLen = 0;
			}

			len -= 3;
		}

		switch(len) {
			case 1:
				atom[0] = data[offset];
				break;
			case 2:
				atom[0] = data[offset];
				atom[1] = data[offset + 1];
				break;
			default:
		}
		atomLen = len;
	}
}
