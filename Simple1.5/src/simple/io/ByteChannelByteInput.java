package simple.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class ByteChannelByteInput implements ByteInput {
	protected final ReadableByteChannel byteChannel;
	protected final byte[] bytes;
	protected final int bytesOffset;
	protected final ByteBuffer buffer;
	protected String hexString;
	protected int discarded = 0;
	
	public ByteChannelByteInput(ReadableByteChannel byteChannel, int bufferSize) {
		super();
		if(bufferSize < 8)
			bufferSize = 8; // so ensureAvailable always returns true for length <= 8
		this.byteChannel = byteChannel;
		this.bytes = new byte[bufferSize];
		this.buffer = ByteBuffer.wrap(bytes);
		this.bytesOffset = 0;
		buffer.limit(0);
	}

	public ByteChannelByteInput(ReadableByteChannel byteChannel, ByteBuffer buffer, byte[] bytes, int bytesOffset) {
		super();
		this.byteChannel = byteChannel;
		this.buffer = buffer;
		this.bytes = bytes;
		this.bytesOffset = bytesOffset;
		buffer.limit(0);
	}

	public int length() throws IOException {
		return discarded + buffer.limit() + (byteChannel.isOpen() ? 1 : 0); // not the best way but its what we've got
	}

	public int available() {
		return buffer.remaining();
	}

	public boolean isResettable() {
		return true;
	}

	public void reset() throws IOException {
		if(discarded > 0) 
			throw new IOException("Reset not supported for input lengths > " + buffer.capacity());
		buffer.rewind();
	}

	@Override
	public String toString() {
		if(hexString == null) {
			try {
				hexString = constructHexString();
			} catch(IOException e) {
				return "<EXCEPTION>";
			}
		}
		return hexString;
	}

	/**
	 * @throws IOException  
	 */
	protected String constructHexString() throws IOException {
		return "<INDETERMINANT>";

	}
	
	protected boolean ensureAvailable(int length) throws IOException {
		if(buffer.capacity() < length) {
			if(length <= 8)
				throw new IOException("ensureAvailable returned false for length = " + length);
			return false;
		}
		if(buffer.remaining() < length) {
			int readPos;
			if(buffer.capacity() - buffer.position() < length) {
				//discard bytes read and read more
				discarded += buffer.position();
				buffer.compact();
				readPos = 0;
			} else {
				//read more
				readPos = buffer.position();
				buffer.position(buffer.limit());
				buffer.limit(buffer.capacity());
			}
			try {
				do {
					int r = byteChannel.read(buffer);
					if(r < 0)
						throw new EOFException();
				} while(buffer.position() - readPos < length);
			} finally {
				buffer.limit(buffer.position());
				buffer.position(readPos);
			}
		}
		return true;
	}
	
	protected void advance(int length) {
		buffer.position(buffer.position() + length);
	}
	
	protected void readTo(byte[] tmp, int offset, int length) throws IOException {
		int len = buffer.remaining();
		if(len > 0)
			buffer.get(tmp, offset, len);
		discarded += buffer.position();
		buffer.position(0);
		buffer.limit(0);
		ByteBuffer tmpBuffer = ByteBuffer.wrap(tmp, offset + len, length - len);
		while(tmpBuffer.hasRemaining()) {
			int r = byteChannel.read(tmpBuffer);
			if(r < 0)
				throw new EOFException();
			discarded += r;
		}
		
	}

	/**
	 * @see simple.io.ByteInput#readBytes(int)
	 */
	public String readBytes(int length) throws IOException {
		if(ensureAvailable(length)) {
			String s = new String(bytes, buffer.position() + bytesOffset, length);
			advance(length);
			return s;
		} else {
			byte[] tmp = new byte[length];
			readTo(tmp, 0, length);
			return new String(tmp);
		}	
	}

	/**
	 * @see simple.io.ByteInput#readChars(int)
	 */
	public String readChars(int length) throws IOException {
		final char[] chars = new char[length];
		final byte[] orig;
		final int offset;
		if(ensureAvailable(length*2)) {
			orig = bytes;
			offset = buffer.position() + bytesOffset;
			advance(length * 2);
		} else {
			orig = new byte[length*2];
			readTo(orig, 0, length*2);
			offset = 0;
		}
		for(int i = 0; i < length; i++)
			chars[i] = (char)(((orig[i * 2 + offset]& 0xff) << 8) + ((orig[i * 2 + offset + 1]& 0xff) << 0));
		return new String(chars, 0, length);
	}

	/**
	 * @see simple.io.ByteInput#readChar()
	 */
	public char readChar() throws IOException {
		return (char)readUnsignedShort();
    }

	/**
	 * @see simple.io.ByteInput#readByte()
	 */
	public byte readByte() throws IOException {
		ensureAvailable(1);
		return buffer.get();
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedByte()
	 */
	public int readUnsignedByte() throws IOException {
		return readByte() & 0xff;
	}

	/**
	 * @see simple.io.ByteInput#readShort()
	 */
	public short readShort() throws IOException {
		return (short)readUnsignedShort();
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedShort()
	 */
	public int readUnsignedShort() throws IOException {
		ensureAvailable(2);
		return (((buffer.get() & 0xff) << 8) + ((buffer.get() & 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readByteAsChar()
	 */
	public char readByteAsChar() throws IOException {
		return (char) readUnsignedByte();
	}

	/**
	 * @see simple.io.ByteInput#readInt()
	 */
	public int readInt() throws IOException {
		ensureAvailable(4);
		return (((buffer.get()& 0xff) << 24) + ((buffer.get()& 0xff) << 16) + ((buffer.get()& 0xff) << 8) + ((buffer.get()& 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedInt()
	 */
	public long readUnsignedInt() throws IOException {
		ensureAvailable(4);
		return (((long) (buffer.get() & 0xff) << 24) + ((buffer.get()& 0xff) << 16) + ((buffer.get()& 0xff) << 8) + ((buffer.get()& 0xff) << 0));
	}


	/**
	 * @see simple.io.ByteInput#readLong()
	 */
	public long readLong() throws IOException {
		ensureAvailable(8);
		return (((long) buffer.get() << 56) + ((long) (buffer.get() & 0xff) << 48)
				+ ((long) (buffer.get() & 0xff) << 40) + ((long) (buffer.get() & 0xff) << 32)
				+ ((long) (buffer.get() & 0xff) << 24) + ((buffer.get() & 0xff) << 16)
				+ ((buffer.get() & 0xff) << 8) + ((buffer.get() & 0xff) << 0));
	}
	
	public void readBytes(byte[] bytes, int offset, int length) throws IOException {
		if(ensureAvailable(length)) {
			buffer.get(bytes, offset, length);
		} else {
			readTo(bytes, offset, length);
		}
	}
	
	/**
	 * @see simple.io.ByteInput#readFloat()
	 */
	public float readFloat() throws IOException {
		return Float.intBitsToFloat(readInt());
	}

	/**
	 * @see simple.io.ByteInput#readDouble()
	 */
	public double readDouble() throws IOException {
		return Double.longBitsToDouble(readLong());
	}

	public int position() {
		return discarded + buffer.position();
	}
	
	public int copyInto(OutputStream output) throws IOException {
		if(buffer.hasArray()) {
			output.write(buffer.array(), buffer.arrayOffset(), buffer.limit());
			output.flush();
			return buffer.limit();
		} else {
			ByteBuffer copyBuffer = buffer.asReadOnlyBuffer();
			copyBuffer.position(0);
			return (int)IOUtils.copy(new ByteBufferInputStream(copyBuffer), output);
		}
	}

	@Override
	public int copyInto(ByteOutput output) throws IOException {
		ByteBuffer copyBuffer = buffer.asReadOnlyBuffer();
		copyBuffer.position(0);
		output.write(copyBuffer);
		output.flush();
		return copyBuffer.position();
	}
}
