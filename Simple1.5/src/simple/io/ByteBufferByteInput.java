package simple.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import simple.text.StringUtils;

public class ByteBufferByteInput implements ByteInput {
	protected final ByteBuffer buffer;
	protected String hexString;
	public ByteBufferByteInput(ByteBuffer buffer) {
		super();
		this.buffer = buffer;
	}

	public boolean isResettable() {
		return true;
	}

	/**
	 * @see simple.io.ByteInput#readBytes(int)
	 */
	public String readBytes(int length) throws IOException {
		if(buffer.remaining() < length )
			throw new EOFException();
		String s;
		if(buffer.hasArray()) {
			s = new String(buffer.array(), buffer.arrayOffset() + buffer.position(), length);
			buffer.position(buffer.position() + length);
		} else {
			byte[] bytes = new byte[length];
			buffer.get(bytes);
			s = new String(bytes);
		}
		return s;
	}

	/**
	 * @see simple.io.ByteInput#readChars(int)
	 */
	public String readChars(int length) throws IOException {
		char[] buf = new char[length];
		for(int i = 0; i < length; i++)
			buf[i] = readChar();
		return new String(buf, 0, length);
	}

	/**
	 * @see simple.io.ByteInput#readChar()
	 */
	public char readChar() throws IOException {
		return (char)readUnsignedShort();
    }

	/**
	 * @see simple.io.ByteInput#readByte()
	 */
	public byte readByte() throws IOException {
		if(buffer.remaining() <  1)
			throw new EOFException();
		return buffer.get();
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedByte()
	 */
	public int readUnsignedByte() throws IOException {
		return readByte() & 0xff;
	}

	/**
	 * @see simple.io.ByteInput#readShort()
	 */
	public short readShort() throws IOException {
		return (short)readUnsignedShort();
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedShort()
	 */
	public int readUnsignedShort() throws IOException {
		if(buffer.remaining() < 2)
			throw new EOFException();
		return (((buffer.get() & 0xff) << 8) + ((buffer.get() & 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readByteAsChar()
	 */
	public char readByteAsChar() throws IOException {
		return (char) readUnsignedByte();
	}

	/**
	 * @see simple.io.ByteInput#readInt()
	 */
	public int readInt() throws IOException {
		if(buffer.remaining() < 4)
			throw new EOFException();
		return (((buffer.get()& 0xff) << 24) + ((buffer.get()& 0xff) << 16) + ((buffer.get()& 0xff) << 8) + ((buffer.get()& 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readUnsignedInt()
	 */
	public long readUnsignedInt() throws IOException {
		if(buffer.remaining() < 4)
			throw new EOFException();
		return (((long) (buffer.get() & 0xff) << 24) + ((buffer.get()& 0xff) << 16) + ((buffer.get()& 0xff) << 8) + ((buffer.get()& 0xff) << 0));
	}


	/**
	 * @see simple.io.ByteInput#readLong()
	 */
	public long readLong() throws IOException {
		if(buffer.remaining() < 8)
			throw new EOFException();
		return (((long) buffer.get() << 56) + ((long) (buffer.get() & 0xff) << 48)
				+ ((long) (buffer.get() & 0xff) << 40) + ((long) (buffer.get() & 0xff) << 32)
				+ ((long) (buffer.get() & 0xff) << 24) + ((buffer.get() & 0xff) << 16)
				+ ((buffer.get() & 0xff) << 8) + ((buffer.get() & 0xff) << 0));
	}

	/**
	 * @see simple.io.ByteInput#readFloat()
	 */
	public float readFloat() throws IOException {
		return Float.intBitsToFloat(readInt());
	}

	/**
	 * @see simple.io.ByteInput#readDouble()
	 */
	public double readDouble() throws IOException {
		return Double.longBitsToDouble(readLong());
	}

	public int length() {
		return buffer.limit();
	}

	public int position() {
		return buffer.position();
	}
	public void reset() throws IOException {
		buffer.position(0);
	}

	@Override
	public String toString() {
		if(hexString == null) {
			hexString = constructHexString();
		}
		return hexString;
	}

	protected String constructHexString() {
		if(length() == 0) return "<EMPTY>";
		return StringUtils.toHex(buffer, 0, buffer.limit());
	}

	public int copyInto(OutputStream output) throws IOException {
		if(buffer.hasArray()) {
			output.write(buffer.array(), buffer.arrayOffset(), buffer.limit());
			output.flush();
			return buffer.limit();
		} else {
			ByteBuffer copyBuffer = buffer.asReadOnlyBuffer();
			copyBuffer.position(0);
			return (int)IOUtils.copy(new ByteBufferInputStream(copyBuffer), output);
		}
	}

	@Override
	public int copyInto(ByteOutput output) throws IOException {
		ByteBuffer copyBuffer = buffer.asReadOnlyBuffer();
		copyBuffer.position(0);
		output.write(copyBuffer);
		output.flush();
		return copyBuffer.position();
	}

	public void readBytes(byte[] bytes, int offset, int length) throws IOException {
		if(buffer.remaining() < bytes.length)
			throw new EOFException();
		buffer.get(bytes, offset, length);
	}
}
