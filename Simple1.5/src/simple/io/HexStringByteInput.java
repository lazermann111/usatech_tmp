package simple.io;

import java.io.IOException;

public class HexStringByteInput extends AbstractByteInput {
	protected final String hex;
	public HexStringByteInput(String hex) {
		super();
		this.hex = hex;
	}

	public boolean isResettable() {
		return true;
	}

	public int length() throws IOException {
		return hex.length() / 2;
	}

	@Override
	protected int read(byte[] buffer, int offset, int length) throws IOException {
		int len = Math.min(length, length() - position());
		for(int i = 0; i < len; i++) {
			try {
				buffer[offset+i] = ByteArrayUtils.hexValue(hex.charAt((position + i) * 2), hex.charAt((position + i) * 2 + 1));
			} catch(IllegalArgumentException e) {
				throw new IOException(e.getMessage());
			}
		}
		return len;
	}

	@Override
	protected int read() throws IOException {
		return ByteArrayUtils.hexValue(hex.charAt(position * 2), hex.charAt(position * 2 + 1)) & 0xFF;
	}

	@Override
	public String toString() {
		if(hex.length() == 0)
			return "<EMPTY>";
		return hex;
	}
}
