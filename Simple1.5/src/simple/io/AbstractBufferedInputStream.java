package simple.io;

import java.io.IOException;
import java.io.InputStream;

public abstract class AbstractBufferedInputStream extends InputStream {
	protected final byte[] buffer;
	protected int count;
    protected int pos;
    protected int markpos = -1;
    protected int marklimit; 
    
	protected AbstractBufferedInputStream() {
		this(8192);
	}
	protected AbstractBufferedInputStream(int size) {
		this.buffer = new byte[size];
	}
	@Override
	public int read() throws IOException {
		if(pos >= count) {
			int r = fill(buffer, 0, buffer.length);
			if(r <= 0)
				return -1;
			pos = 0;
			count = r;
        }
        return buffer[pos++] & 0xff;
	}
	
	@Override
	public int available() throws IOException {
		if(pos >= count) {
			/*
			int r = fill(buffer, 0, buffer.length);
			if(r <= 0)
				return 0;
			pos = 0;
			count = r;*/
			return 0;
		}
		return count - pos;
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if(pos >= count) {
			if(len > buffer.length / 2) {
				return fill(b, off, len);
			} else {
				int r = fill(buffer, 0, buffer.length);
				if(r <= 0)
					return -1;
				pos = 0;
				count = r;
			}
		}
		int r = Math.min(len, count - pos);
		System.arraycopy(buffer, pos, b, off, r);
		pos += r;
		return r;
	}

	protected abstract int fill(byte[] bytes, int off, int len) throws IOException ;
}
