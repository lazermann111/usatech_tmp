/**
 *
 */
package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;

/**
 * @author Brian S. Krug
 *
 */
public class EncodingInputStream extends InputStream {
	protected final CharBuffer buffer;
	protected final Reader reader;
	protected final CharsetEncoder encoder;
	protected final byte[] ONE_BYTE = new byte[1];
	protected boolean atEnd = false;

	public EncodingInputStream(Reader reader) {
		this(reader, Charset.defaultCharset().newEncoder(), 1024);
	}

	public EncodingInputStream(Reader reader, CharsetEncoder encoder, int bufferSize) {
		super();
		this.buffer = CharBuffer.allocate(bufferSize);
		this.reader = reader;
		this.encoder = encoder;
		buffer.flip();
	}

	/**
	 * @see java.io.InputStream#close()
	 */
	@Override
	public void close() throws IOException {
		reader.close();
	}

	/**
	 * @see java.io.InputStream#mark(int)
	 */
	@Override
	public void mark(int readlimit) {
		try {
			reader.mark(readlimit);
		} catch(IOException e) {
			// do nothing
		}
	}

	/**
	 * @see java.io.InputStream#markSupported()
	 */
	@Override
	public boolean markSupported() {
		return reader.markSupported();
	}
	/**
	 * @see java.io.InputStream#reset()
	 */
	@Override
	public void reset() throws IOException {
		reader.reset();
	}
	/**
	 * @see java.io.InputStream#skip(long)
	 */
	@Override
	public long skip(long n) throws IOException {
		return reader.skip(n);
	}
	/**
	 * @see java.io.InputStream#read(byte[], int, int)
	 */
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if(atEnd)
			return -1;
		ByteBuffer out = ByteBuffer.wrap(b, off, len);
		if(buffer.hasRemaining()) {
			// we have left over - use it
			CoderResult result = encoder.encode(buffer, out, false);
			if(result.isOverflow()) {
				return len - out.remaining();
			} else if(result.isError()) {
				result.throwException();
			}
		}
		buffer.compact();
		while(reader.read(buffer) >= 0) {
			buffer.flip();
			CoderResult result = encoder.encode(buffer, out, false);
			if(result.isOverflow()) {
				return len - out.remaining();
			} else if(result.isError()) {
				result.throwException();
			}
			buffer.compact();
		}
		buffer.flip();
		CoderResult result = encoder.encode(buffer, out, true);
		if(result.isOverflow()) {
			return len - out.remaining();
		} else if(result.isError()) {
			result.throwException();
		}
		encoder.flush(out);
		atEnd = true;
		int total = len - out.remaining();
		return total == 0 ? -1 : total;
	}
	/**
	 * @see java.io.InputStream#read()
	 */
	@Override
	public int read() throws IOException {
		int r = read(ONE_BYTE);
		return r > 0 ? ONE_BYTE[0] : r;
	}

}
