package simple.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.xml.sax.InputSource;

/**
 * Implements ConfigSource with a bundle
 */
public class BundledConfigSource extends ConfigSource {
    protected List<ConfigSource> configSources;
    public BundledConfigSource(List<ConfigSource> configSources) {
        super(null, false);
        this.configSources = configSources;
    }
    public ConcatenatingInputStream getInputStream() throws IOException {
        InputStream[] ins = new InputStream[configSources.size()];
        for(int i = 0; i < ins.length; i++) {
            ins[i] = configSources.get(i).getInputStream();
        }
        return new ConcatenatingInputStream(ins);
    }
    public InputSource getInputSource() throws IOException {
        final ConcatenatingInputStream cis = getInputStream();
        InputSource is = new InputSource(cis) {
            @Override
            public String getSystemId() {
                return configSources.get(cis.getCurrentIndex()).getUri();
            }
        };
        return is;
    }
    public long lastModified() {
        long l = 0;
        for(ConfigSource cs : configSources) {
            l = Math.max(l, cs.lastModified());
        }
        return l;
    }
    public String toString() {
        return configSources.toString();
    }
    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj instanceof BundledConfigSource) {
            BundledConfigSource cs = (BundledConfigSource)obj;
            return configSources.equals(cs.configSources);
        }
        return false;
    }
    @Override
    public int hashCode() {
        return configSources.hashCode();
    }
    @Override
    public String getUri() {
        String uri = "";
        for(ConfigSource cs : configSources) {
            if(uri.length() > 0) uri += ";";
            uri += cs.getUri();
        }
        return uri;
    }

	@Override
	protected OutputStream getRawOutputStream() throws IOException {
		throw new IOException("Updating not supported");
	}
}