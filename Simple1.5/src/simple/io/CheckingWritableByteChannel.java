package simple.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.atomic.AtomicLong;

public class CheckingWritableByteChannel implements WritableByteChannel {
	protected final WritableByteChannel delegate;
	protected final AtomicLong count = new AtomicLong();
	protected final ByteChecker checker;

	public CheckingWritableByteChannel(WritableByteChannel delegate, ByteChecker checker) {
		this.delegate = delegate;
		this.checker = checker;
	}

	public void close() throws IOException {
		delegate.close();
	}

	public int write(ByteBuffer src) throws IOException {
		int n = delegate.write(src);
		if(n > 0)
			count.addAndGet(n);
		if(n > 0) {
			int limit = src.limit();
			int pos = src.position();
			src.position(pos - n);
			src.limit(pos);
			while(src.hasRemaining())
				update(src);
			src.limit(limit);
			src.position(pos);
		}
		return n;
	}

	@Override
	public boolean isOpen() {
		return delegate.isOpen();
	}

	protected void update(ByteBuffer src) {
		checker.update(src);
	}

	public long getCount() {
		return count.longValue();
	}

	public Object getCheckValue() {
		return checker.getCheckValue();
	}

	public void reset() {
		count.set(0L);
		checker.reset();
	}
}
