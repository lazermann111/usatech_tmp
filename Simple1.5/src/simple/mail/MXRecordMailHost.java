package simple.mail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;

import org.xbill.DNS.Lookup;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.Record;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

import simple.io.Log;

import com.sun.mail.smtp.SMTPTransport;

public class MXRecordMailHost implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6217956078781679402L;
	private static final Log log = simple.io.Log.getLog();
	/**
	 * 09282012 Ying:
	 * This implementation is originally from aspirin project aspirin-5b3211d https://github.com/masukomi/aspirin
	 * With slight modification
	 * 
	 * <p>This method gives back the host name(s) where we can send the email. 
	 * It is copied from it's original place in RemoteDelivery object.</p>
	 * 
	 * <p>First time we ask DNS to find MX record(s) of a domain name. If no MX 
	 * records are found, we check the upper level domains (if exists). At last 
	 * we try to get the domain A record, because the MX server could be same as 
	 * the normal domain handler server. If only upper level domain has MX 
	 * record then we append the A record of original hostname (if exists) as 
	 * first element of record collection. If none of these tries are 
	 * successful, we give back an empty collection.</p>
	 * 
	 * Special Thanks to Tim Motika (tmotika at ionami dot com) for 
	 * his reworking of this method.
	 * 
	 * @param hostName We search the associated MX server of this hostname.
	 * @return Collection of URLName objects. If no MX server found, then it 
	 * gives back an empty collection.
	 * 
	 */
	
	public static List<String> getMXRecordsForHost(String hostName) {

		ArrayList<String> recordsColl = null;
		try {
			boolean foundOriginalMX = true;
			Record[] records = new Lookup(hostName, Type.MX).run();
			
			/*
			 * Sometimes we should send an email to a subdomain which does not 
			 * have own MX record and MX server. At this point we should find an 
			 * upper level domain and server where we can deliver our email.
			 *  
			 * Example: subA.subB.domain.name has not own MX record and 
			 * subB.domain.name is the mail exchange master of the subA domain 
			 * too.
			 */
			if( records == null || records.length == 0 )
			{
				foundOriginalMX = false;
				String upperLevelHostName = hostName;
				while(		records == null &&
							upperLevelHostName.indexOf(".") != upperLevelHostName.lastIndexOf(".") &&
							upperLevelHostName.lastIndexOf(".") != -1
					)
				{
					upperLevelHostName = upperLevelHostName.substring(upperLevelHostName.indexOf(".")+1);
					records = new Lookup(upperLevelHostName, Type.MX).run();
				}
			}

            if( records != null )
            {
            	// Sort in MX priority (higher number is lower priority)
                Arrays.sort(records, new Comparator<Record>() {
                    @Override
                    public int compare(Record arg0, Record arg1) {
                        return ((MXRecord)arg0).getPriority()-((MXRecord)arg1).getPriority();
                    }
                });
                // Create records collection
                recordsColl = new ArrayList<String>(records.length);
                for (int i = 0; i < records.length; i++)
				{ 
					MXRecord mx = (MXRecord) records[i];
					String targetString = mx.getTarget().toString();
					recordsColl.add(targetString.substring(0, targetString.length() - 1));
				}
            }else
            {
            	foundOriginalMX = false;
            	recordsColl = new ArrayList<String>();
            }
            
            /*
             * If we found no MX record for the original hostname (the upper 
             * level domains does not matter), then we add the original domain 
             * name (identified with an A record) to the record collection, 
             * because the mail exchange server could be the main server too.
			 * 
			 * We append the A record to the first place of the record 
			 * collection, because the standard says if no MX record found then 
			 * we should to try send email to the server identified by the A 
			 * record.
             */
			if( !foundOriginalMX )
			{
				Record[] recordsTypeA = new Lookup(hostName, Type.A).run();
				if (recordsTypeA != null && recordsTypeA.length > 0)
				{
					recordsColl.add(0, hostName);
				}
			}

		} catch (TextParseException e) {
			log.error("Failed to get MX record for host '"+hostName+"'.", e);
		}

		return recordsColl;
	}
	
	public static String getHostname(String emailAddress) throws IllegalArgumentException{
		String parts[] = emailAddress.split("@");
	    if (parts.length != 2) throw new IllegalArgumentException("Failed to get hostname from emailAddress:" + emailAddress);
	    return parts[1];
	}
	
	public static List<String> getMXRecordMailhostFromEmailAddress(String emailAddress){
		String hostname=getHostname(emailAddress);
		return getMXRecordsForHost(hostname);
	}

	public static enum AddressVerification {
		SYNTAX, MX_RECORD, MX_CONNECT, FULL
	};

	public static void verifyAddress(InternetAddress emailAddress, AddressVerification verification) throws MessagingException {
		emailAddress.validate();
		if(verification == AddressVerification.SYNTAX)
			return;
		String hostName = getHostname(emailAddress.getAddress());
		List<String> mxrs = getMXRecordsForHost(hostName);
		if(mxrs.isEmpty())
			throw new MessagingException("No MX Records found for domain '" + hostName + "'");
		if(verification == AddressVerification.MX_RECORD)
			return;
		Properties props = new Properties();
		Transport transport;
		MessagingException connectException = null;
		for(String mxr : mxrs) {
			log.info("Using mailhost=" + mxr + " to verify " + emailAddress.getAddress());
			props.put("mail.smtp.host", mxr);
			Session session = Session.getInstance(props, null);
			transport = session.getTransport(emailAddress);
			try {
				try {
					transport.connect();
				} catch(MessagingException e) {
					if(connectException == null)
						connectException = e;
					continue;
				}
				if(verification == AddressVerification.MX_CONNECT || !(transport instanceof SMTPTransport))
					return;
				int response = ((SMTPTransport) transport).simpleCommand("VRFY " + emailAddress.getAddress());
				switch(response) {
					case 250: // good!
						return;
					case 502: // not implemented
						return;
					default:
						throw new MessagingException("Mail Server could not verify '" + emailAddress.getAddress() + "' (" + response + ")");
				}
			} finally {
				transport.close();
			}
		}
		throw connectException;
	}
}
