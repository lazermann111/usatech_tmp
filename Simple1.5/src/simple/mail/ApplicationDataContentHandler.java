package simple.mail;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;

import javax.activation.DataContentHandler;
import javax.activation.DataSource;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.IOUtils;
import simple.io.Log;

public class ApplicationDataContentHandler implements DataContentHandler {
	private static final Log log = Log.getLog();
	protected static DataFlavor[] dataFlavors;
	
	public ApplicationDataContentHandler() {
		super();
	}

	public DataFlavor[] getTransferDataFlavors() {
		if(dataFlavors == null) {
			try {
				dataFlavors = new DataFlavor[] {
							new DataFlavor("application/*")
						};
			} catch (ClassNotFoundException e) {
				log.warn("Could not create data flavors", e);
			}
		}
		return dataFlavors;
	}

	public Object getTransferData(DataFlavor dataFlavor, DataSource dataSource)
			throws UnsupportedFlavorException, IOException {
		return dataSource.getInputStream();
	}

	public Object getContent(DataSource dataSource) throws IOException {
		return dataSource.getInputStream();
	}

	public void writeTo(Object obj, String mimeType, OutputStream out)
			throws IOException {
		if(obj instanceof InputStream) {
			IOUtils.copy((InputStream)obj, out);
		} else if(obj instanceof Reader) {
			IOUtils.copy((Reader)obj, new OutputStreamWriter(out));			
		} else if(obj instanceof byte[]) {
			out.write((byte[])obj);
		} else if(obj instanceof String) {
			out.write(((String)obj).getBytes());
		} else {
			log.warn("Converting object of class " + obj.getClass() + " to string inorder to write to output stream");
			try {
				out.write(ConvertUtils.convert(String.class, obj).getBytes());
			} catch (ConvertException e) {
				IOException ioe = new IOException("Could not convert to a string");
				ioe.initCause(e);
				throw ioe;
			}
		}
	}

}
