/*
 * Emailer.java
 *
 * Created on November 16, 2001, 5:28 PM
 */

package simple.mail;

/**
 * Utility class to create Email Objects using the properties set in this class
 *
 * @author  Brian Krug
 * @version
 */
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;

public class Emailer implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3902478841L;
	protected transient Session session = null;
	protected Properties props = new Properties();
	
	/** Creates new Emailer */
	public Emailer() {
		super();
	}
	
	public Emailer(Properties props) {
		super();
		loadProperties(props);
	}
	
	public Email createEmail() throws MessagingException {
		return new Email(getSession());
	}
	
	public void loadProperties(Properties props) {
		if(props != null) {
			this.props = props;
			session = null;
		}
	}
	
	protected Session getSession() {
		if(session == null) session = Session.getInstance(props, null);
		return session;		
	}
	
	public void setMailHost(String mailHost){
		props.put("mail.smtp.host", mailHost);
	}
	
	public String getMailHost(){
		return props.getProperty("mail.smtp.host");
	}

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}
	
}
