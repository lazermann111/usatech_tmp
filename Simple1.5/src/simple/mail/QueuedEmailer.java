package simple.mail;

import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A queue of emails that are mailed asynchronously.
 *
 * @author  Brian S. Krug
 */
public class QueuedEmailer extends Emailer {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3247412499914L;
	private BlockingQueue<Object> queue = null;
    private Thread emailThread = null;
    private static Set<QueuedEmailer> emailers = new java.util.HashSet<QueuedEmailer>();
    protected static final Object FINISH = new Object();
    private final ReentrantLock lock = new ReentrantLock();
    /** Allows an object to delay creation of the EmailInfo until
     * it is absolutely needed for the sendMessage method
     */
    public static interface EmailInfoCreator {
        public EmailInfo createEmailInfo() throws IOException;
    }
    
    /**
     * Creates a new QueuedEmailer 
     */
    public QueuedEmailer() {
        super();
        init();
    }
    
    /**
     * Creates a new QueuedEmailer 
     */
    public QueuedEmailer(Properties props) {
        super(props);
        init();
    }


    /**
     * Adds a message to the emailThread queue
     */
    public void queueMessage(Object message) {
        if(message != null) {
            queue.offer(message);
            getEmailThread(); // ensure that emailThread is started
        }
    }
    
    /**
     * Makes sure all pending messages are written
     */
    public void finish() {
        if(emailThread != null) {
            lock.lock();  // block until condition holds
            try {
                if(emailThread != null) {
	                queue.offer(FINISH);
	                try {
	                    emailThread.join();
	                } catch(InterruptedException e) {}
	                emailThread = null;
	            }
            } finally {
                lock.unlock();
            }
        }
        //System.out.println("Ending Output, " + this);
    }
    
    /**
     * Makes sure all pending messages are written
     */
    public static void finishAll() {
        for(QueuedEmailer qe : emailers)
        	qe.finish();
    }
    
    /**
     * Returns the thread that actually writes the messages
     * @return Thread
     */
    protected Thread getEmailThread() {
    	//FIXME:This is a broken pattern
        if(emailThread == null) {
            lock.lock();  // block until condition holds
            try {
                if(emailThread == null) { // double-check now that we have the lock
                    emailThread = new Thread() {
                        public void run() {
                            try {
    	                        while(true) {
	                                yield();
	                                Object m = queue.take();
	                                if(m == FINISH) break;
	                                sendMessage(m);
	                            }
                            } catch(InterruptedException ie) {
                                // do nothing just end
                            }
                        }
                    };
                    emailThread.setName("Email Thread");
                    emailThread.setDaemon(true);
                    emailThread.setPriority(3);
                    emailThread.start();
                }
            } finally {
                lock.unlock();
            }
        }
        return emailThread;
    }
    
    /**
     * Performs initialization of the Queue and printing thread
     */
    protected void init() {
        emailers.add(this);
        queue = new LinkedBlockingQueue<Object>();
    }
    
    /**
     * Emails the message
     */
    protected void sendMessage(Object message) {
        try	{
            convertToEmail(message).send(getSession());
        } catch (Throwable e) {
        }
    }
    
    protected EmailInfo convertToEmail(Object message) throws IOException, ClassNotFoundException {
        if(message instanceof EmailInfo) return (EmailInfo)message;
        if(message instanceof EmailInfoCreator) return ((EmailInfoCreator)message).createEmailInfo();
        /*if(message instanceof Map) {
            EmailInfo info = new EmailInfo();
            info.setBcc(bcc*/
        return simple.io.logging.EmailFormatter.parse(message.toString());
    }
        
}
