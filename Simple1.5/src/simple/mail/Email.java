/*
 * Email.java
 *
 * Created on November 16, 2001, 5:28 PM
 */

package simple.mail;

/**
 * Convenience Class that simplifies email message creation, configuration and
 * sending.
 *
 * @author  Brian S. Krug
 * @version
 */
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import simple.io.IOUtils;
import simple.text.RegexUtils;

public class Email {
    protected Session session;
    protected MimeMultipart multipart;
    protected MimeBodyPart mainBody;
    protected MimeMessage message;
    
    /** Creates new Email */
    public Email(Session session) throws MessagingException {
        message = new javax.mail.internet.MimeMessage(session);
        multipart = new MimeMultipart();
        message.setContent(multipart);
        mainBody = new MimeBodyPart();
        multipart.addBodyPart(mainBody);        
    }
    /**
     * Sends the email message
     */
    public void send() throws MessagingException {
        message.setSentDate(new java.util.Date());
        message.saveChanges();
        Transport.send(message);
    }
    
    public void send(Address address) throws MessagingException {
    	if (address == null){
    	    throw new SendFailedException("No recipient address");
    	}
    	Address[] addArray=new Address[1];
    	addArray[0]=address;
        message.setSentDate(new java.util.Date());
        message.saveChanges();
        Transport.send(message, addArray);
    }
    
    /**
     * Parses the recipients into an array of Strings
     */
    public static String[] parseRecips(String recips) {
		if(recips == null)
			return null;
		if(recips.indexOf('<') != -1) {
        	// do not split on spaces, if we're using this form
			String[] addrs = RegexUtils.split("[\\x00-\\x1f;,\\x0A]", recips, null);
	        return addrs;
		} else {
			String[] addrs = RegexUtils.split("[\\x00-\\x20:;,\\xA0]", recips, null);// "[^\\w.@-]" -- too liberal
			return addrs;
		}
    }
    
    /** Getter for property to.
     * @return Value of property to.
     */
    public String getTo() throws MessagingException {
        return InternetAddress.toString(message.getRecipients(Message.RecipientType.TO));
    }
    
    /** Sets the recipient list
     * @param to The new recipient list
     */
    public void setTo(String to) throws MessagingException {
        setTo(parseRecips(to));
    }
    
    /** Sets the recipient list
     * @param to The new recipient list
     */
    public void setTo(String[] to) throws MessagingException {    	
    	message.setRecipients(Message.RecipientType.TO, (InternetAddress[])null);
    	
    	addTo(to);
    }
    
    /** Adds the given String to the recipient list.
     * @param to the list of recipients to add
     */
    public void addTo(String to) throws MessagingException {
        addTo(parseRecips(to));
    }
    
    /** Adds the given String array to the recipient list.
     * @param to the list of recipients to add
     */
    public void addTo(String[] to) throws MessagingException {
    	if(to != null) {
            for(String toAddr : to) 
            {
            	message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddr));
            }
        }
    }
    
    /** Adds the given String to the copy list.
     * @param to the list of copies to add
     */
    public void addCc(String cc) throws MessagingException {
        addCc(parseRecips(cc));
    }
    
    /** Adds the given String array to the copy list.
     * @param to the list of copies to add
     */
    public void addCc(String[] cc) throws MessagingException {
    	if(cc != null) {
            for(String ccAddr : cc) 
            {
            	message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccAddr));
            }
        }
    }
    
    /** Adds the given String to the blind copy list.
     * @param to the list of copies to add
     */
    public void addBcc(String bcc) throws MessagingException {
        addBcc(parseRecips(bcc));
    }
    
    /** Adds the given String array to the blind copy list.
     * @param to the list of copies to add
     */
    public void addBcc(String[] bcc) throws MessagingException {
    	if(bcc != null) {
            for(String bccAddr : bcc) 
            {
            	message.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(bccAddr));
            }
        }
    }
    
    /** Returns the copy to list
     */
    public String getCc() throws MessagingException {
        return InternetAddress.toString(message.getRecipients(Message.RecipientType.CC));
    }
    
    /** Sets the copy to list
     * @param cc The new copy to list
     */
    public void setCc(String cc) throws MessagingException {
        setCc(parseRecips(cc));
    }
    
    /** Sets the copy to list
     * @param cc The new copy to list
     */
    public void setCc(String[] cc) throws MessagingException {
    	message.setRecipients(Message.RecipientType.CC, (InternetAddress[])null);
    	
    	addCc(cc);
    }
    
    /** Returns the blind copy to list
     */
    public String getBcc() throws MessagingException {
        return InternetAddress.toString(message.getRecipients(Message.RecipientType.BCC));
    }
    
    /** Sets the blind copy to list
     * @param cc The new blind copy to list
     */
    public void setBcc(String bcc) throws MessagingException {
        setBcc(parseRecips(bcc));
    }
    
    /** Sets the blind copy to list
     * @param cc The new blind copy to list
     */
    public void setBcc(String[] bcc) throws MessagingException {
    	message.setRecipients(Message.RecipientType.BCC, (InternetAddress[])null);
    	
    	addBcc(bcc);
    }
    
    /** Getter for property from.
     * @return Value of property from.
     */
    public String getFrom() throws MessagingException {
        return InternetAddress.toString(message.getFrom());
    }
    
    /** Setter for property from.
     * @param from New value of property from.
     */
    public void setFrom(String from) throws MessagingException {
        message.setFrom(from== null ? null : new InternetAddress(from));
    }
    
    public void addFrom(String from) throws MessagingException {
        message.addFrom(from== null ? null : new InternetAddress[] {new InternetAddress(from)});
    }
    
    /** Getter for property subject.
     * @return Value of property subject.
     */
    public String getSubject() throws MessagingException {
        return message.getSubject();
    }
    
    /** Setter for property subject.
     * @param subject New value of property subject.
     */
    public void setSubject(String subject) throws MessagingException {
        message.setSubject(subject);
    }
    
    /** Getter for property body.
     * @return Value of property body.
     */
    public String getBody() throws MessagingException, java.io.IOException {
        return mainBody.getContent().toString();
    }
    
    /** Setter for property body.
     * @param body New value of property body.
     */
    public void setBody(String body) throws MessagingException {
        mainBody.setText(body);
    }
    
    public void setBodyHtml(String body) throws MessagingException {
        mainBody.setText(body, null, "html" );
    }
    
    public void addAttachment(java.io.File file, String contentType) throws MessagingException {
        try {
            addAttachment(IOUtils.readFully(new java.io.FileInputStream(file)), contentType, file.getName());
        } catch(java.io.FileNotFoundException fnfe) {
            throw new MessagingException("",fnfe);
        } catch(java.io.IOException ioe) {
            throw new MessagingException("",ioe);
        }
    }
    
    public void addAttachment(Object content, String contentType) throws MessagingException {
    	addAttachment(content, contentType, null);
    }
    
    public void addAttachment(Object content, String contentType, String filename) throws MessagingException {
    	addAttachment(content, contentType, filename, contentType.startsWith("text/")  && filename == null ? "7bit" : "base64");
    }
    
    /** Adds an attachments.
     */
    public void addAttachment(Object content, String contentType, String filename, String encoding) throws MessagingException {
        MimeBodyPart bd = new MimeBodyPart();
        bd.setContent(content,contentType);
        if(filename != null) bd.setFileName(filename);
        if(encoding != null) bd.setHeader("Content-Transfer-Encoding", encoding);
        multipart.addBodyPart(bd);
    }
    /** Adds an attachments.
     */
    public void addAttachment(DataSource dataSource) throws MessagingException {
        addAttachment(dataSource, null);
    }
    /** Adds an attachments.
     */
    public void addAttachment(DataSource dataSource, String encoding) throws MessagingException {
        MimeBodyPart bd = new MimeBodyPart();
        bd.setDataHandler(new DataHandler(dataSource));
        if(dataSource.getName() != null) bd.setFileName(dataSource.getName());
        if(encoding != null) bd.setHeader("Content-Transfer-Encoding", encoding);
        multipart.addBodyPart(bd);
    }
    /** Setter for property attachements.
     * @param attachements New value of property attachements.
     */
    public void clearAttachments() throws MessagingException {
        while(multipart.getCount() > 1) multipart.removeBodyPart(1);
    }
    
    public void clearAll() throws MessagingException {
        setSubject(null);
        setTo((String[])null);
        setCc((String[])null);
        setBcc((String[])null);
        setFrom(null);
        setBody(null);
        clearAttachments();
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getName());
        sb.append(": [Subject=");
        try {
            sb.append(getSubject());
        } catch(MessagingException me) {
            sb.append("#ERROR#");
        }
        sb.append("; To=");
        try {
            sb.append(getTo());
        } catch(MessagingException me) {
            sb.append("#ERROR#");
        }
        sb.append("; Cc=");
        try {
            sb.append(getCc());
        } catch(MessagingException me) {
            sb.append("#ERROR#");
        }
        sb.append("; Bcc=");
        try {
            sb.append(getBcc());
        } catch(MessagingException me) {
            sb.append("#ERROR#");
        }
        sb.append("; From=");
        try {
            sb.append(getFrom());
        } catch(MessagingException me) {
            sb.append("#ERROR#");
        }
        sb.append("; Body=");
        try {
            sb.append(getBody());
        } catch(MessagingException me) {
            sb.append("#ERROR#");
        } catch(java.io.IOException ioe) {
            sb.append("#ERROR#");
        }
        sb.append("; Attachment Count=");
        try {
            sb.append(multipart.getCount()-1);
        } catch(MessagingException me) {
            sb.append("#ERROR#");
        }
        sb.append("]");
        return sb.toString();
    }
}