/*
 * Email.java
 *
 * Created on November 16, 2001, 5:28 PM
 */

package simple.mail;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.HeaderTokenizer;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import simple.bean.ConvertUtils;
import simple.text.StringUtils;

public class EmailInfo implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6612340124121656L;
	protected Collection<MimeBodyPart> attachmentList = new LinkedHashSet<MimeBodyPart>();
    protected Collection<String> bccList = new LinkedHashSet<String>();
    protected Collection<String> ccList = new LinkedHashSet<String>();
    protected Collection<String> toList = new LinkedHashSet<String>();
    protected String from;
    protected Object body;
    protected Object subject;    
	protected String contentType = "text/plain";
	protected String charset = null;
    
    /** Creates new EmailInfo */
    public EmailInfo() {        
    }
    
    /**
     * Sends the email message
     */
	public void send(Session session, Address... addresses) throws MessagingException {
        //set up message
        MimeMessage message = new MimeMessage(session);
        MimeMultipart multipart = new MimeMultipart();
        message.setContent(multipart);
        MimeBodyPart mainBody = new MimeBodyPart();
        multipart.addBodyPart(mainBody);
        
        //add other stuff
        message.setRecipients(Message.RecipientType.TO, toAddresses(toList));
        message.setRecipients(Message.RecipientType.CC, toAddresses(ccList));
        message.setRecipients(Message.RecipientType.BCC, toAddresses(bccList));
        if(from != null) message.setFrom(new InternetAddress(from));
        if(subject != null) message.setSubject(subject.toString());
		if(body != null) {
			if(body instanceof Multipart)
				mainBody.setContent((Multipart) body);
			if(body instanceof DataSource)
				mainBody.setDataHandler(new DataHandler((DataSource) body));
			else {
				String bodyText = ConvertUtils.getStringSafely(body);
				StringBuilder sb = new StringBuilder();
				if(!StringUtils.isBlank(contentType))
					sb.append(contentType);
				else
					sb.append("text/plain");
				sb.append("; charset=");
				if(charset == null) {
					if(!isAscii(bodyText))
						sb.append(MimeUtility.quote(MimeUtility.mimeCharset(MimeUtility.getDefaultJavaCharset()), HeaderTokenizer.MIME));
					else
						sb.append("us-ascii");
				} else
					sb.append(MimeUtility.quote(charset, HeaderTokenizer.MIME));
				mainBody.setContent(bodyText, sb.toString());
			}
		}
        
        //attachments
		for(MimeBodyPart bp : attachmentList) {
            multipart.addBodyPart(bp);
        }
        
        //send
        message.setSentDate(new java.util.Date());
		message.saveChanges();
		if(addresses == null || addresses.length == 0)
			addresses = message.getAllRecipients();
		Transport.send(message, addresses);
    }
    
	public static boolean isAscii(String s) {
		for(int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if(ch >= 0177 || (ch < 040 && ch != '\r' && ch != '\n' && ch != '\t'))
				return false;
		}
		return true;
	}
    /**
     * Parses the recipients into an array of Strings
     */
    public static String[] parseRecips(String recips) {
		return StringUtils.splitQuotesAnywhere(recips, ",;", "\"", false);
    }
    
    public static InternetAddress[] toAddresses(Collection<String> recipList) throws AddressException {
        InternetAddress[] addresses = null;
        if(recipList != null) {
			int cnt = 0;
			for(String recip : recipList)
				if(!StringUtils.isBlank(recip))
					cnt++;
			addresses = new InternetAddress[cnt];
			cnt = 0;
			for(String recip : recipList)
				if(!StringUtils.isBlank(recip))
					addresses[cnt++] = new InternetAddress(recip.trim());
        }
        return addresses;
    }
    
    /** Adds the given String to the recipient list.
     * @param to the list of recipients to add
     */
    public void addTo(String to) {
        addTo(parseRecips(to));
    }
    
    /** Adds the given String array to the recipient list.
     * @param to the list of recipients to add
     */
    public void addTo(String[] to) {
        if(to != null) toList.addAll(Arrays.asList(to));
    }
    
    /** Getter for property to.
     * @return Value of property to.
     */
    public String getTo() {
        return StringUtils.join(toList, ",");
    }
    
    /** Sets the recipient list
     * @param to The new recipient list
     */
    public void setTo(String to) {
        setTo(parseRecips(to));
    }
    
    /** Sets the recipient list
     * @param to The new recipient list
     */
    public void setTo(String[] to) {
        toList.clear();
        addTo(to);
    }
    
    /** Adds the given String to the copy list.
     * @param to the list of copies to add
     */
    public void addCc(String cc) {
        addCc(parseRecips(cc));
    }
    
    /** Adds the given String array to the copy list.
     * @param to the list of copies to add
     */
    public void addCc(String[] cc) {
        if(cc != null) ccList.addAll(Arrays.asList(cc));
    }
    
    /** Returns the copy to list
     */
    public String getCc() {
        return StringUtils.join(ccList, ",");
    }
    
    /** Sets the copy to list
     * @param cc The new copy to list
     */
    public void setCc(String cc) {
        setCc(parseRecips(cc));
    }
    
    /** Sets the copy to list
     * @param cc The new copy to list
     */
    public void setCc(String[] cc) {
        ccList.clear();
        addCc(cc);
    }
    
    /** Adds the given String to the blind copy list.
     * @param to the list of copies to add
     */
    public void addBcc(String bcc) {
        addBcc(parseRecips(bcc));
    }
    
    /** Adds the given String array to the blind copy list.
     * @param to the list of copies to add
     */
    public void addBcc(String[] bcc) {
        if(bcc != null) bccList.addAll(Arrays.asList(bcc));
    }
    
    /** Returns the blind copy to list
     */
    public String getBcc() {
        return StringUtils.join(bccList, ",");
    }
    
    /** Sets the blind copy to list
     * @param cc The new blind copy to list
     */
    public void setBcc(String bcc) {
        setBcc(parseRecips(bcc));
    }
    
    /** Sets the blind copy to list
     * @param cc The new blind copy to list
     */
    public void setBcc(String[] bcc) {
        bccList.clear();
        addBcc(bcc);
    }
    
    /** Getter for property from.
     * @return Value of property from.
     */
    public String getFrom() {
        return from;
    }
    
    /** Setter for property from.
     * @param from New value of property from.
     */
    public void setFrom(String from) {
        this.from = from;
    }
        
    /** Getter for property subject.
     * @return Value of property subject.
     */
    public Object getSubject() {
        return subject;
    }
    
    /** Setter for property subject.
     * @param subject New value of property subject.
     */
    public void setSubject(Object subject) {
        this.subject = subject;
    }
    
    /** Getter for property body.
     * @return Value of property body.
     */
    public Object getBody() {
        return body;
    }
    
    /** Setter for property body.
     * @param body New value of property body.
     */
    public void setBody(Object body) {
        this.body = body;
    }

	public void addAttachment(Object content, String contentType, String filename, String encoding) throws MessagingException {
		MimeBodyPart bd = new MimeBodyPart();
		bd.setContent(content, contentType);
		if(filename != null)
			bd.setFileName(filename);
		if(encoding != null)
			bd.setHeader("Content-Transfer-Encoding", encoding);
		attachmentList.add(bd);
	}

	/**
	 * Adds an attachments.
	 */
	public void addAttachment(DataSource dataSource) throws MessagingException {
		addAttachment(dataSource, null);
    }
    
	/**
	 * Adds an attachments.
	 */
	public void addAttachment(DataSource dataSource, String encoding) throws MessagingException {
		MimeBodyPart bd = new MimeBodyPart();
		bd.setDataHandler(new DataHandler(dataSource));
		if(dataSource.getName() != null)
			bd.setFileName(dataSource.getName());
		if(encoding != null)
			bd.setHeader("Content-Transfer-Encoding", encoding);
		attachmentList.add(bd);
    }
    
    /** Setter for property attachements.
     * @param attachements New value of property attachements.
     */
    public void clearAttachments() {
        attachmentList.clear();
    }
    
    public void clearAll() {
        setSubject(null);
        setTo((String[])null);
        setCc((String[])null);
        setBcc((String[])null);
        setFrom(null);
        setBody(null);
        clearAttachments();
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getName());
        sb.append(": [Subject=");
        sb.append(getSubject());
        sb.append("; To=");
        sb.append(getTo());
        sb.append("; Cc=");
        sb.append(getCc());
        sb.append("; Bcc=");
        sb.append(getBcc());
        sb.append("; From=");
        sb.append(getFrom());
        sb.append("; Body=");
        sb.append(getBody());
        sb.append("; Attachment Count=");
        sb.append(attachmentList.size());
        sb.append("]");
        return sb.toString();
    }
    
    /** Getter for property contentType.
     * @return Value of property contentType.
     *
     */
    public String getContentType() {
        return this.contentType;
    }
    
    /** Setter for property contentType.
     * @param contentType New value of property contentType.
     *
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}
    
}
