/*
 * ResultsCategoryDataset.java
 *
 * Created on December 29, 2003, 9:06 AM
 */

package simple.chart;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jfree.data.category.IntervalCategoryDataset;

/** Uses multiple values to represent the series - rather than a cluster.
 * @author Brian S. Krug
 */
public class ResultsMultiSeriesDataset extends ResultsDataset implements IntervalCategoryDataset {
    private static final long serialVersionUID = -234932043214L;
	protected static final String[] CLUSTER_NAMES = new String[] {"category"}; // column = category; row = series
    protected Comparable[] seriesKeys = null;
    /** Creates a new instance of ResultsCategoryDataset */
    public ResultsMultiSeriesDataset() {
        super(CLUSTER_NAMES);
    }
    
    public int getColumnCount() {
        return getClusterKeyCount(CLUSTER_NAMES[0]);
    }
    
    public int getColumnIndex(Comparable key) {
        return getClusterKeyIndex(CLUSTER_NAMES[0], key);
    }
    
    public Comparable getColumnKey(int category) {
        return getClusterKey(CLUSTER_NAMES[0], category);
    }
    
    public java.util.List getColumnKeys() {
        return Arrays.asList(getCluster(CLUSTER_NAMES[0]).getKeys());
    }
    
    protected static String[] suffixes = new String[] {"-start","-end"};
    protected Comparable[] getSeriesKeys() {
    	if(seriesKeys == null) {
    		/*
        	int per = 0;
        	if(valueTypeExists("0-end")) per++;
        	if(valueTypeExists("0-start")) per++;
        	if(valueTypeExists("0")) per++;
        	
            seriesKeys = new Integer[valueColumns.size() / per];
    		for(int i = 0; i < seriesKeys.length; i++)
    			seriesKeys[i] = new Integer(i);
    			*/
    		Set<Comparable> keys = new LinkedHashSet<Comparable>();
    		//if valuesType i exists use it else try each suffix and then create a new virtual ValueKey
    		OUTER: for(int i = 0; true; i++) {
    			ValueKey vk = valueColumns.get("" + i);
    			if(vk != null) {//good
    				keys.add(vk);
    			} else {
    				for(String suf : suffixes) {
    					vk = valueColumns.get("" + i + suf);
    					if(vk != null) {
    						keys.add(new ValueKey(vk.type, vk.label, -1, null));
    						continue OUTER;
    					}
        			}
    				break; //not found - all done
    			}
    		}
        		
    		seriesKeys = keys.toArray(new Comparable[keys.size()]);
    	}
    	return seriesKeys;
    }
    
    public int getRowCount() {
    	return getSeriesKeys().length;
    }
    
    public int getRowIndex(Comparable key) {
        return ((Number)key).intValue();
    }
    
    public Comparable getRowKey(int series) {
        return getSeriesKeys()[series];
    }
    
    public java.util.List getRowKeys() {
        return Arrays.asList(getSeriesKeys());
    }
    
    public Number getValue(Comparable series, Comparable category) {
        return getDataValue(CLUSTER_NAMES, getKeys(category), getValueType(series, null));        
    }
    
    public Number getValue(int series, int category) {
        return getValue(getRowKey(series), getColumnKey(category));
    }
    
    public Number getEndValue(Comparable series, Comparable category) {
    	return getValue(series, category, "end");
    }
    
    public Number getEndValue(int series, int category) {
        return getEndValue(getRowKey(series), getColumnKey(category));
    }
    
    public Number getStartValue(Comparable series, Comparable category) {
    	return getValue(series, category, "start");
    }
    
    protected Number getValue(Comparable series, Comparable category, String subtype) {
        String valueType = getValueType(series, subtype);
        if(valueTypeExists(valueType))
            return getDataValue(CLUSTER_NAMES, getKeys(category), valueType);        
        else
            return getValue(series, category);
    }
    
    public Number getStartValue(int series, int category) {
        return getStartValue(getRowKey(series), getColumnKey(category));
    }
    
    protected Comparable[] getKeys(Comparable category) {
        return new Comparable[] {
            category,
        };
    }
    protected String getValueType(Comparable series, String subtype) {
    	String s;
    	if(series instanceof ValueKey) {
    		s = ((ValueKey)series).type;
    	} else {
    		s = series.toString();
    	}
    	if(subtype == null || subtype.trim().length() == 0)
    		return s;
    	else
    		return s + "-" + subtype;
    }
}
