package simple.chart;

import java.math.BigDecimal;

import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.xy.IntervalXYDataset;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

public class ResultsStackedXYDataset extends ResultsXYZDataset implements IntervalXYDataset {
	private static final long serialVersionUID = 5018142975627942389L;
	protected double barWidth = 100.0;
	
	public ResultsStackedXYDataset() {
		super();
	}

	protected double asDouble(Number n) {
		return n == null ? Double.NaN : n.doubleValue();
	}
	
	protected int signum(Number n) {
		if(n == null)
			return 0;
		if(n instanceof BigDecimal)
			return ((BigDecimal)n).signum();
		if(n instanceof Long || n instanceof Integer || n instanceof Short || n instanceof Byte)
			return Long.signum(n.longValue());
		return (int)Math.signum(n.doubleValue());
	}
	
	protected Number add(Number n1, Number n2, int signum) {
		if(n1 == null)
			return n2;
		if(n2 == null)
			return n1;
		if(!n1.getClass().equals(n2)) {
			BigDecimal bd1;
			try {
				bd1 = ConvertUtils.convert(BigDecimal.class, n1);
			} catch (ConvertException e) {
				bd1 = new BigDecimal(n1.doubleValue());
			}
			BigDecimal bd2;
			try {
				bd2 = ConvertUtils.convert(BigDecimal.class, n2);
			} catch (ConvertException e) {
				bd2 = new BigDecimal(n2.doubleValue());
			}
			if(signum * bd2.signum() == -1)
				return bd1;
			return bd1.add(bd2);
		}
		if(n1 instanceof Long || n1 instanceof Integer || n1 instanceof Short || n1 instanceof Byte) {
			long l1 = n1.longValue();
			long l2 = n2.longValue();
			if(signum * Long.signum(l2) == -1)
				return n1;
			return l1 + l2;	
		}
		double d1 = n1.doubleValue();
		double d2 = n2.doubleValue();
		if(Double.isNaN(d1))
			return n2;
		if(Double.isNaN(d2))
			return n1;
		if(signum * Math.signum(d2) == -1.0)
			return n1;
		return d1 + d2;			
	}
	
	/**
     * Returns the bar width.
     *
     * @return The bar width.
     *
     * @see #setBarWidth(double)
     * @since 1.0.4
     */
    public double getBarWidth() {
        return this.barWidth;
    }

    /**
     * Sets the bar width and sends a {@link DatasetChangeEvent} to all
     * registered listeners.
     *
     * @param barWidth  the bar width.
     *
     * @see #getBarWidth()
     * @since 1.0.4
     */
    public void setBarWidth(double barWidth) {
        this.barWidth = barWidth;
        notifyListeners(new DatasetChangeEvent(this, this));
    }
	@Override
	public Number getStartX(int series, int item) {
		Number n = getX(series, item);
		if(n != null)
			n = add(n, -this.barWidth / 2.0, 0);
		return n;
	}

	@Override
	public double getStartXValue(int series, int item) {
		return asDouble(getStartX(series, item));		
	}

	@Override
	public Number getEndX(int series, int item) {
		Number n = getX(series, item);
		if(n != null)
			n = add(n, this.barWidth / 2.0, 0);
		return n;
	}

	@Override
	public double getEndXValue(int series, int item) {
		return asDouble(getEndX(series, item));
	}

	@Override
	public Number getStartY(int series, int item) {
		if(series == 0)
			return 0.0;	
		Number n = getY(series, item);
		if(n == null)
			return Double.NaN;
		int signum = signum(n);
		switch(signum) {
			case 0:
				return 0.0;
			case 1:
				n = 0.0;
				//fall-through
			default:
				for(int i = 0; i < series; i++)
					n = add(n, getY(i, item), signum);
		}
		return n;
	}

	@Override
	public double getStartYValue(int series, int item) {
		return asDouble(getStartY(series, item));
	}

	@Override
	public Number getEndY(int series, int item) {
		Number n = getY(series, item);
		if(n == null)
			return Double.NaN;
		int signum = signum(n);
		switch(signum) {
			case 0:
				return 0.0;
			case -1:
				n = 0.0;
				//fall-through
			default:
				for(int i = 0; i < series; i++)
					n = add(n, getY(i, item), signum);
		}
		return n;
	}

	@Override
	public double getEndYValue(int series, int item) {
		return asDouble(getEndY(series, item));
	}
	
	// We override to fix the issue when no value is provided for the given X
	public Number getX(int series, int item) {
		Cluster c = getCluster(CLUSTER_NAMES[1]);
        if(c == null) {
            //log.debug("Could not find a cluster named, '" + CLUSTER_NAMES[1] + "'");
            return null;
        }
		ClusterKey key = c.getKeys()[item];
        return ConvertUtils.convertSafely(Number.class, key.getOrderValue(), null);
    }
}
