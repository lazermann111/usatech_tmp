/*
 * Created on Sep 30, 2005
 *
 */
package simple.chart;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.general.Dataset;
import org.jfree.util.TableOrder;
import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.chart.GenericChartType.GenericAxisType;
import simple.io.Log;
import simple.results.DataGenre;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;

/**
 * @author Brian S. Krug
 *
 */
public class ChartFactory {
    private static final Log log = Log.getLog();
    protected static Map<String, ChartType> chartTypes = new HashMap<String, ChartType>();
    protected static class LazyHolder {
    	protected static boolean loaded = loadChartTypes();
    }
    protected static boolean loaded = false;
    public static Collection<ChartType> getAvailableChartTypes() {
    	if(LazyHolder.loaded)
    		return new ArrayList<ChartType>(chartTypes.values());
    	else
    		return Collections.emptyList();
    }
    public static ChartType getChartType(String chartTypeName) {
    	if(LazyHolder.loaded)
    		return chartTypes.get(chartTypeName);
    	else
    		return null;
    }
    
    protected static boolean loadChartTypes() {
        return loadChartTypesFromXML();
    }

    protected static void loadChartTypes(InputStream in) throws IOException, SAXException, ParserConfigurationException, ConvertException {
        ObjectBuilder builder = new ObjectBuilder(in);
        for(ObjectBuilder ob : builder.getSubNodes("chart-type")) {
            addChartType(ob.read(GenericChartType.class));
        }
    }
    
    protected static boolean loadChartTypesFromXML() {
        try {
            loadChartTypes(ChartFactory.class.getResourceAsStream("chart-types.xml"));
            return true;
        } catch (IOException e) {
            log.warn("Could not load chart types", e);
        } catch (SAXException e) {
            log.warn("Could not load chart types", e);
        } catch (ParserConfigurationException e) {
            log.warn("Could not load chart types", e);
        } catch (ConvertException e) {
            log.warn("Could not load chart types", e);
        }
        return false;
    }
    
    /** This method is NOT thread-safe.
     * @param ct The ChartType to add
     */
    public static void addChartType(ChartType ct) {
        chartTypes.put(ct.getName(), ct);        
    }
    
    protected static void loadChartTypesFromReflection() {
//      figure out which ResultsDatasets we have and which dataset
        // interfaces they implement
		Map<Class<?>, Class<? extends ResultsDataset>> datasetMap = new HashMap<Class<?>, Class<? extends ResultsDataset>>();
		Map<Class<?>, String[]> clustersMap = new HashMap<Class<?>, String[]>();
        Collection<Class<? extends ResultsDataset>> datasetClasses = new ArrayList<Class<? extends ResultsDataset>>(2);
        datasetClasses.add(ResultsCategoryDataset.class);
        datasetClasses.add(ResultsXYZDataset.class);
        for(Class<? extends ResultsDataset> dsc : datasetClasses) {
            try {
				List<Class<?>> list = new ArrayList<Class<?>>();
                gatherInterfaces(dsc, list);
				for(Iterator<Class<?>> iter = list.iterator(); iter.hasNext();) {
					Class<?> c = iter.next();
                    if (Dataset.class.isAssignableFrom(c))
                        datasetMap.put(c, dsc);
                }
                String[] clusters = ((ResultsDataset) dsc.newInstance()).getClusterOrder();
                clustersMap.put(dsc, clusters);
            } catch (SecurityException e) {
                log.warn("Could not create ResultsDataset '" + dsc.getName() + "'", e);
            } catch (InstantiationException e) {
                log.warn("Could not create ResultsDataset '" + dsc.getName() + "'", e);
            } catch (IllegalAccessException e) {
                log.warn("Could not create for ResultsDataset '" + dsc.getName() + "'", e);
            }
        }
        // Figure out which methods are available and then ensure we have a
        // ResultsDatasets that implements the necessary dataset
        Method[] methods = org.jfree.chart.ChartFactory.class.getMethods();
        OUTER: for (int i = 0; i < methods.length; i++) {
            if (methods[i].getName().startsWith("create")
                    && JFreeChart.class.isAssignableFrom(methods[i].getReturnType())) {
				Class<?>[] pts = methods[i].getParameterTypes();
                //MethodParameter[] params = new MethodParameter[pts.length];
                Class<? extends ResultsDataset> rdClass = null;
                String[] clusters = null;
                String[] values = null;
				for(Class<?> cls : pts) {
                    if(Dataset.class.isAssignableFrom(cls)) {
                        rdClass = datasetMap.get(cls);
                        if(rdClass == null) continue OUTER;
                        clusters = clustersMap.get(rdClass);
                        if(clusters == null) throw new IllegalArgumentException("No clusters found for " + rdClass.getName());
                        Set<String> valueSet = new TreeSet<String>();
                        for(Method m : cls.getMethods()) {
							Class<?> rt = ConvertUtils.convertToWrapperClass(m.getReturnType());
                            if (rt == null || rt.equals(Void.class))
                                continue;
                            log.debug("Checking method '" + m.getName() + "' with return type=" + rt.getName());
                            if (Number.class.isAssignableFrom(rt)
                                    && m.getName().startsWith("get")
                                    && m.getParameterTypes().length == clusters.length
                                    && m.getName().endsWith("Value")) {
                                // this is one so add it
                                String s = m.getName().substring(3, m.getName().length() - 5);
                                valueSet.add(s.length() == 0 ? s : s.substring(0, 1).toLowerCase() + s.substring(1));
                            }
                        }
                        values = valueSet.toArray(new String[valueSet.size()]);
                        break;
                    }
                }
                
                List<String> args = new ArrayList<String>();
                int b = 0;
                for (int k = 0; k < pts.length; k++) {
                    if (String.class.equals(pts[k])) {
                        switch (k) {
                        case 0:
                            args.add("title");
                            break;
                        case 1:
                            args.add(clusters[0] + "Label");
                            break;
                        case 2:
                            args.add((values[0].trim().length() > 0 ? values[0] + "Value" : "value") + "Label");
                            break;
                        default:
                            log.warn("Cannot determine what the " + i + "th paramter represents for method '" + methods[i].getName() + "'.");
                        }
                    } else if (Boolean.class.equals(ConvertUtils.convertToWrapperClass(pts[k]))) {
                        switch (b++) {
                        case 0:
                            args.add("legend");
                            break;
                        case 1:
                            args.add("tooltips");
                            break;
                        case 2:
                            args.add("urls");
                            break;
                        default:
                            log.warn("Cannot determine what the " + i + "th paramter represents for method '" + methods[i].getName() + "'.");
                        }
                    } else if (PlotOrientation.class.isAssignableFrom(pts[k])) {
                        args.add("orientation");
                    } else if (TableOrder.class.isAssignableFrom(pts[k])) {
                        args.add("tableorder");                        
                    } else if (Dataset.class.isAssignableFrom(pts[k])) {
                        args.add("dataset");                        
                    } else {
                        args.add("UNKNOWN");
                        log.warn("Cannot determine what the " + i + "th paramter represents for method '" + methods[i].getName() + "'.");
                    }
                }
                // create the chartType and add it to the collection
                GenericChartType gct = new GenericChartType(
                        methods[i].getName().substring(6),
                        rdClass,
                        methods[i],
                        args.toArray(new String[args.size()]));
                for(int k = 0; k < clusters.length; k++) {
                    gct.addAxis(
                            clusters[k], 
                            new DataGenre[] {DataGenre.STRING, DataGenre.DATE}, 
                            false, 
                            clusters[k],
                            null,
                            k == clusters.length-1 ? GenericAxisType.CLUSTER_LEGEND : GenericAxisType.CLUSTER);                    
                }
                for(int k = 0; k < values.length; k++) {
                    gct.addAxis(
                            (values[k].trim().length() > 0 ? values[k] + "Value" : "value"), 
                            new DataGenre[] {DataGenre.NUMBER}, 
                            true, 
                            null,
                            values[k],
                            GenericAxisType.VALUE);                  
                }
                chartTypes.put(gct.getName(), gct);
                
                log.debug("Adding chart type " + gct.getName() + " with clusters=" + StringUtils.toStringList(clusters)
                            + " and values=" +  StringUtils.toStringList(values));
            }
        }
    }

	protected static void gatherInterfaces(Class<?> baseClass, List<Class<?>> list) {
        list.add(baseClass);
		Class<?>[] interfaces = baseClass.getInterfaces();
        for (int i = 0; i < interfaces.length; i++) {
            gatherInterfaces(interfaces[i], list);
        }
        if (baseClass.getSuperclass() != null)
            gatherInterfaces(baseClass.getSuperclass(), list);
    }

    public static void main(String[] args) throws Throwable {
        // load from reflections and output to xml
        String file = "C:/TEMP/ChartTypes.xml";
        OutputStream out = new FileOutputStream(file);
        XMLBuilder builder = new XMLBuilder(out);
        //loadChartTypesFromReflection();
        loadChartTypesFromXML();
		builder.elementStart("chart-types");
        for(ChartType ct : chartTypes.values()) {
            builder.write(ct);
        }
        builder.elementEnd("chart-types");
        out.flush();
        out.close();
        Log.finish();
    }
}
