/*
 * ResultsCategoryDataset.java
 *
 * Created on December 29, 2003, 9:06 AM
 */

package simple.chart;

import java.util.Arrays;

import org.jfree.data.category.IntervalCategoryDataset;

/** Builds on ResultsDataset to implement the CategoryDataset methods.
 * @author Brian S. Krug
 */
public class ResultsCategoryDataset extends ResultsDataset implements IntervalCategoryDataset {
    private static final long serialVersionUID = 910941234L;
	protected static final String[] CLUSTER_NAMES = new String[] {"category", "series"}; // column = category; row = series
    
    /** Creates a new instance of ResultsCategoryDataset */
    public ResultsCategoryDataset() {
        super(CLUSTER_NAMES);
    }
    
    public int getColumnCount() {
        return getClusterKeyCount(CLUSTER_NAMES[0]);
    }
    
    public int getColumnIndex(Comparable key) {
        return getClusterKeyIndex(CLUSTER_NAMES[0], key);
    }
    
    public Comparable getColumnKey(int category) {
        return getClusterKey(CLUSTER_NAMES[0], category);
    }
    
    public java.util.List getColumnKeys() {
        return Arrays.asList(getCluster(CLUSTER_NAMES[0]).getKeys());
    }
    
    public int getRowCount() {
        return getClusterKeyCount(CLUSTER_NAMES[1]);
    }
    
    public int getRowIndex(Comparable key) {
        return getClusterKeyIndex(CLUSTER_NAMES[1], key);
    }
    
    public Comparable getRowKey(int series) {
        return getClusterKey(CLUSTER_NAMES[1], series);
    }
    
    public java.util.List getRowKeys() {
        return Arrays.asList(getCluster(CLUSTER_NAMES[1]).getKeys());
    }
    
    public Number getValue(Comparable series, Comparable category) {
        return getDataValue(CLUSTER_NAMES, getKeys(series, category), "");        
    }
    
    public Number getValue(int series, int category) {
        return getValue(getRowKey(series), getColumnKey(category));
    }
    
    public Number getEndValue(Comparable series, Comparable category) {
        if(valueTypeExists("end"))
            return getDataValue(CLUSTER_NAMES, getKeys(series, category), "end");        
        else
            return getValue(series, category);
    }
    
    public Number getEndValue(int series, int category) {
        return getEndValue(getRowKey(series), getColumnKey(category));
    }
    
    public Number getStartValue(Comparable series, Comparable category) {
        if(valueTypeExists("start"))
            return getDataValue(CLUSTER_NAMES, getKeys(series, category), "start"); 
        else
            return getValue(series, category);
    }
    
    public Number getStartValue(int series, int category) {
        return getStartValue(getRowKey(series), getColumnKey(category));
    }
    
    protected Comparable[] getKeys(Comparable series, Comparable category) {
        return new Comparable[] {
            category,
            series
        };
    }
}
