/*
 * Created on Sep 23, 2005
 *
 */
package simple.chart;

import java.text.Format;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;

import simple.results.Results;
import simple.results.ResultsReader;

public interface ChartMaker {
    public interface AxisData {
        public String getLabel() ;
        public ResultsReader getReader() ;
        public Format getFormat() ;
		public void setFormat(Format format) ;
		public void setLabel(String label) ;
		public void setReader(ResultsReader reader) ;
    }
    
    public String getTitle() ;
    public void setTitle(String title) ;
    
    public AxisData getAxisData(String axisName) ;
    
    /**
     * @param axisName
     * @param axisData
     * @throws IllegalArgumentException If axisName does not exist in the chart type
     */
    public void setAxisData(String axisName, AxisData axisData) throws IllegalArgumentException;
    /**
     * @param axisName
     * @param label
     * @param reader
     * @param format
     * @throws IllegalArgumentException If axisName does not exist in the chart type
     */
    public void setAxisData(String axisName, String label, ResultsReader reader, Format format) throws IllegalArgumentException;
    /**
     * @param axisName
     * @throws IllegalArgumentException If axisName does not exist in the chart type
     */
    public void removeAxisData(String axisName) throws IllegalArgumentException;
    
    public PlotOrientation getOrientation() ;
    public void setOrientation(PlotOrientation orientation) ;
    
    public boolean isTooltips() ;
    public void setTooltips(boolean tooltips) ;
    
    public boolean isUrls() ;
    public void setUrls(boolean urls) ;

    public Object getProperty(String name) ;
    public void setProperty(String name, Object value) ;
    
    public JFreeChart makeChart(Results results) throws ChartCreationException;
}