package simple.chart;

import java.util.concurrent.TimeUnit;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.urls.StandardXYURLGenerator;
import org.jfree.chart.util.ParamChecks;
import org.jfree.data.xy.XYBarDataset;
import org.jfree.data.xy.XYDataset;


public class ExtraChartTypeFactory {
	/*
	public static JFreeChart createDualAxisBarLineChart() {
	CategoryDataset categorydataset = createDataset1();
    JFreeChart jfreechart = ChartFactory.createBarChart3D("Dual Axis Chart", "Category", "Value", categorydataset, PlotOrientation.VERTICAL, true, true, false);
    jfreechart.setBackgroundPaint(new Color(204, 255, 204));
    CategoryPlot categoryplot = jfreechart.getCategoryPlot();
    categoryplot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
    categoryplot.setRangeAxisLocation(AxisLocation.TOP_OR_LEFT);
    CategoryItemRenderer categoryitemrenderer = categoryplot.getRenderer();
    categoryitemrenderer.setSeriesPaint(0, Color.red);
    categoryitemrenderer.setSeriesPaint(1, Color.yellow);
    categoryitemrenderer.setSeriesPaint(2, Color.green);
    CategoryDataset categorydataset1 = createDataset2();
    NumberAxis3D numberaxis3d = new NumberAxis3D("Secondary");
    categoryplot.setRangeAxis(1, numberaxis3d);
    categoryplot.setDataset(1, categorydataset1);
    categoryplot.mapDatasetToRangeAxis(1, 1);
    LineRenderer3D linerenderer3d = new LineRenderer3D();
    linerenderer3d.setSeriesPaint(0, Color.blue);
    categoryplot.setRenderer(1, linerenderer3d);
    categoryplot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
    return jfreechart;
	}
*/
	public static JFreeChart createXYOverlayBarChart(String title, String xAxisLabel, String yAxisLabel, XYDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls) {
		ParamChecks.nullNotPermitted(orientation, "orientation");
		ValueAxis domainAxis = new DateAxis(xAxisLabel);
		ValueAxis valueAxis = new NumberAxis(yAxisLabel);

		XYBarRenderer renderer = new XYBarRenderer(0.15);
		if(tooltips) {
			XYToolTipGenerator tt = StandardXYToolTipGenerator.getTimeSeriesInstance();
			renderer.setBaseToolTipGenerator(tt);
		}
		if(urls) {
			renderer.setURLGenerator(new StandardXYURLGenerator());
		}

		XYPlot plot = new XYPlot(new XYBarDataset(dataset, TimeUnit.DAYS.toMillis(1)), domainAxis, valueAxis, renderer);
		plot.setOrientation(orientation);

		JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, legend);
		return chart;
	}
	public static JFreeChart createXYStackedBarChart(String title, String xAxisLabel, String yAxisLabel, ResultsStackedXYDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls) {
		ParamChecks.nullNotPermitted(orientation, "orientation");
		ValueAxis domainAxis = new DateAxis(xAxisLabel);
		ValueAxis valueAxis = new NumberAxis(yAxisLabel);

		XYBarRenderer renderer = new XYBarRenderer(0.15);
		renderer.setUseYInterval(true);
		if(tooltips) {
			XYToolTipGenerator tt = StandardXYToolTipGenerator.getTimeSeriesInstance();
			renderer.setBaseToolTipGenerator(tt);
		}
		if(urls) {
			renderer.setURLGenerator(new StandardXYURLGenerator());
		}
		dataset.setBarWidth(TimeUnit.DAYS.toMillis(1));
		XYPlot plot = new XYPlot(dataset, domainAxis, valueAxis, renderer);
		plot.setOrientation(orientation);

		JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, legend);
		return chart;
	}
}
