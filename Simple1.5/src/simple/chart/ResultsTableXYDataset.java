package simple.chart;

import org.jfree.data.xy.TableXYDataset;

public class ResultsTableXYDataset extends ResultsXYZDataset implements TableXYDataset {
	private static final long serialVersionUID = 5695540084464830498L;

	public ResultsTableXYDataset() {

	}

	@Override
	public int getItemCount() {
		return getSeriesCount();
	}

}
