/*
 * Created on Sep 30, 2005
 *
 */
package simple.chart;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import simple.results.DataGenre;


public abstract class AbstractChartType implements ChartType {
    protected String name;
    protected Map<String,Axis> axes = new LinkedHashMap<String,Axis>(); //Must keep order of addition
    protected Collection<Axis> safeAxes = Collections.unmodifiableCollection(axes.values());
    
    public static class BasicAxis implements Axis {
        protected String name;
        protected DataGenre[] acceptedGenre;
        protected boolean required;
        public BasicAxis(String name, DataGenre[] acceptedGenre, boolean required) {
            this.name = name;
            this.acceptedGenre = acceptedGenre;
            this.required = required;
        }
        public DataGenre[] getAcceptedGenre() {
            return acceptedGenre;
        }
        public String getName() {
            return name;
        }
        public boolean isRequired() {
            return required;
        }
    }
    
    public AbstractChartType(String name) {
        this.name = name;
    }

    public void addAxis(String name, DataGenre[] acceptedGenre, boolean required) {
        addAxis(new BasicAxis(name, acceptedGenre, required));
    }
    
    public void addAxis(Axis axis) {
        axes.put(axis.getName(), axis);
    }
    
    public String getName() {
        return name;
    }

    public Collection<Axis> getAxes() {
        return safeAxes;
    }

    public Axis getAxis(String axisName) {
        return axes.get(axisName);
    }
}
