/*
 * Created on Sep 26, 2005
 *
 */
package simple.chart;

import java.text.Format;
import java.util.HashMap;
import java.util.Map;

import org.jfree.chart.plot.PlotOrientation;

import simple.results.ResultsReader;

public abstract class AbstractChartMaker implements ChartMaker {
    public class BasicAxisData implements AxisData {
        protected ResultsReader reader;
        protected String label;
        protected Format format;
        public BasicAxisData(ResultsReader reader, String label, Format format) {
            this.reader = reader;
            this.label = label;
            this.format = format;
        }
        public String getLabel() {
            return label;
        }
        public ResultsReader getReader() {
            return reader;
        }
		public Format getFormat() {
			return format;
		}
		public void setFormat(Format format) {
			this.format = format;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public void setReader(ResultsReader reader) {
			this.reader = reader;
		}
    }
    protected String title;
    protected PlotOrientation orientation = PlotOrientation.VERTICAL;
    protected boolean tooltips = true;
    protected boolean urls = true;
    protected Map<String, AxisData> axesData = new HashMap<String, AxisData>();;
    protected ChartType chartType;
    protected Map<String,Object> properties = new HashMap<String, Object>();
    
    public AbstractChartMaker(ChartType chartType) {
        this.chartType = chartType;
    }

    /**
     * @return Returns the orientation.
     */
    public PlotOrientation getOrientation() {
        return orientation;
    }

    /**
     * @param orientation The orientation to set.
     */
    public void setOrientation(PlotOrientation orientation) {
        this.orientation = orientation;
    }

    /**
     * @return Returns the title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return Returns the tooltips.
     */
    public boolean isTooltips() {
        return tooltips;
    }

    /**
     * @param tooltips The tooltips to set.
     */
    public void setTooltips(boolean tooltips) {
        this.tooltips = tooltips;
    }

    /**
     * @return Returns the urls.
     */
    public boolean isUrls() {
        return urls;
    }

    /**
     * @param urls The urls to set.
     */
    public void setUrls(boolean urls) {
        this.urls = urls;
    }
    
    /**
     * @see simple.chart.ChartMaker#setAxisData(java.lang.String, java.lang.String, simple.results.ResultsReader)
     */
    public void setAxisData(String axisName, String label, ResultsReader reader, Format format) throws IllegalArgumentException {
        setAxisData(axisName, new BasicAxisData(reader, label, format));
    }

    /**
     * @see simple.chart.ChartMaker#removeAxisData(java.lang.String)
     */
    public void removeAxisData(String axisName) throws IllegalArgumentException {
        setAxisData(axisName, null);
    }

    /**
     * @see simple.chart.ChartMaker#setAxisData(java.lang.String, simple.chart.ChartMaker.AxisData)
     */
    public void setAxisData(String axisName, AxisData axisData) throws IllegalArgumentException {
        if(chartType.getAxis(axisName) != null)
            axesData.put(axisName, axisData);
        else
            throw new IllegalArgumentException("Axis '" + axisName + "' does not exist in this Chart Type");
    }

    public AxisData getAxisData(String axisName) {
        return axesData.get(axisName);
    }
    
    public Object getProperty(String name) {
    	return properties.get(name);
    }
    
    public void setProperty(String name, Object value) {
    	properties.put(name, value);
    }
}
